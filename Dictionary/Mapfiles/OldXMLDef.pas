// !!! A C H T U N G !!! Automatisch generierte Datei. �nderungen gehen verloren.
// Generiert: 26.02.2008

(* -------------------------------------------------------------------------------
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: XMLDef.pas
| Projectpart...: MillMaster Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Konstanten f�r den Zugriff auf die einzelenen Elemente des MM Schamas
| Info..........: Dieses File wurde automatisch vom Schematool erzeugt. �nderungen von Hand gehen verloren
| Develop.system: Windows 2000
| Target.system.: Windows NT/2000/XP
| Compiler/Tools: Delphi
|------------------------------------------------------------------------------ *)
unit XMLDef;

interface

uses
  MSXML2_TLB;

// Typendefinitionen (Automatisch generiert. Nicht �ndern.)
type
  TSwitch = (swOff, // = 0
             swOn, // = 1
             swExt_SPE);// TSwitch

  TSensingHead = (shNone, // = 0
                  shTK830, // = 1
                  shTK840, // = 2
                  shTK850, // = 3
                  shTK870, // = 4
                  shTK880, // = 5
                  shTK930F, // = 6
                  shTK940BD, // = 7
                  shTK930H, // = 8
                  shTK940F, // = 9
                  shTK930S, // = 10
                  shTKZenit, // = 11
                  shTKZenitF, // = 12
                  shTKZenitFP, // = 13
                  shTKZenitC, // = 14
                  shTKZenitCF, // = 15
                  shTKZenitCFP);// TSensingHead

  TSWOption = (swoNone, // = 0
               swoBase, // = 1
               swoQuality, // = 2
               swoImperfection, // = 3
               swoLabPack);// TSWOption

  TWinderType = (wtNone, // = 0
                 wtAC238, // = 1
                 wtESPERO, // = 2
                 wtMUR_IND_INV, // = 3
                 wtAC238_SYS, // = 4
                 wtAWE_SS, // = 5
                 wtWSC_C_CONER, // = 6
                 wtAC338_PLUS, // = 7
                 wtORION, // = 8
                 wtAC338_SPECTRA, // = 9
                 wtORION_SPECTRA, // = 10
                 wtMUR_SPECTRA, // = 11
                 wtESPER_SPECTRA, // = 12
                 wtAWE_SS_SPECTRA, // = 13
                 wtAC238_SPECTRA, // = 14
                 wtMUR_NEW_SPECTRA, // = 15
                 wtAC338_SPECTRA_PLUS, // = 16
                 wtAC338_COLOR, // = 17
                 wtxxORION_COLOR, // = 18
                 wtxxAPRO_COLOR, // = 19
                 wtxxMURATA_COLOR, // = 20
                 wtxxESPERO_COLOR, // = 21
                 wtxxAC238_COLOR, // = 22
                 wtxxAWE_SS_COLOR, // = 23
                 wtDEMO_WINDER);// TWinderType

  TYarnUnit = (yuNone, // = 0
               yuNm, // = 1
               yuNeB, // = 2
               yutex, // = 3
               yuTd, // = 4
               yuNeL, // = 5
               yuNeK, // = 6
               yuNeW, // = 7
               yuTs, // = 8
               yuNcC);// TYarnUnit

  TLengthWindowMode = (lwmNone, // = 0
                       lwmFirst, // = 1
                       lwmLast, // = 2
                       lwmCone);// TLengthWindowMode

  TAWEClass = (aweNone, // = 0
               awePlus, // = 1
               aweSpectra, // = 2
               aweSpectraPlus, // = 3
               aweZenit, // = 4
               aweZenitL2);// TAWEClass

  TSensingHeadClass = (shcNone, // = 0
                       shc8x, // = 1
                       shc9xFS, // = 2
                       shc9xH, // = 3
                       shc9xBD, // = 4
                       shcZenit, // = 5
                       shcZenitF, // = 6
                       shcZenitFP, // = 7
                       shcZenitC, // = 8
                       shcZenitCF, // = 9
                       shcZenitCFP);// TSensingHeadClass

  TFrontType = (ftNone, // = 0
                ftZE80, // = 1
                ftZE800, // = 2
                ftZE900, // = 3
                ftZE80i, // = 4
                ftZE800i, // = 5
                ftZE900i, // = 6
                ftInf68K, // = 7
                ftInfPPC, // = 8
                ftInfPPCAC5, // = 9
                ftZEUnknown);// TFrontType


// Konstanten Arrays (Automatisch generiert. Nicht �ndern.)
const
  cSwitchNames: array[TSwitch] of string = ('swOff', // = 0
                                            'swOn', // = 1
                                            'swExt_SPE');//   cSwitchNames

  cSensingHeadNames: array[TSensingHead] of string = ('shNone', // = 0
                                                      'shTK830', // = 1
                                                      'shTK840', // = 2
                                                      'shTK850', // = 3
                                                      'shTK870', // = 4
                                                      'shTK880', // = 5
                                                      'shTK930F', // = 6
                                                      'shTK940BD', // = 7
                                                      'shTK930H', // = 8
                                                      'shTK940F', // = 9
                                                      'shTK930S', // = 10
                                                      'shTKZenit', // = 11
                                                      'shTKZenitF', // = 12
                                                      'shTKZenitFP', // = 13
                                                      'shTKZenitC', // = 14
                                                      'shTKZenitCF', // = 15
                                                      'shTKZenitCFP');//   cSensingHeadNames

  cSWOptionNames: array[TSWOption] of string = ('swoNone', // = 0
                                                'swoBase', // = 1
                                                'swoQuality', // = 2
                                                'swoImperfection', // = 3
                                                'swoLabPack');//   cSWOptionNames

  cWinderTypeNames: array[TWinderType] of string = ('wtNone', // = 0
                                                    'wtAC238', // = 1
                                                    'wtESPERO', // = 2
                                                    'wtMUR_IND_INV', // = 3
                                                    'wtAC238_SYS', // = 4
                                                    'wtAWE_SS', // = 5
                                                    'wtWSC_C_CONER', // = 6
                                                    'wtAC338_PLUS', // = 7
                                                    'wtORION', // = 8
                                                    'wtAC338_SPECTRA', // = 9
                                                    'wtORION_SPECTRA', // = 10
                                                    'wtMUR_SPECTRA', // = 11
                                                    'wtESPER_SPECTRA', // = 12
                                                    'wtAWE_SS_SPECTRA', // = 13
                                                    'wtAC238_SPECTRA', // = 14
                                                    'wtMUR_NEW_SPECTRA', // = 15
                                                    'wtAC338_SPECTRA_PLUS', // = 16
                                                    'wtAC338_COLOR', // = 17
                                                    'wtxxORION_COLOR', // = 18
                                                    'wtxxAPRO_COLOR', // = 19
                                                    'wtxxMURATA_COLOR', // = 20
                                                    'wtxxESPERO_COLOR', // = 21
                                                    'wtxxAC238_COLOR', // = 22
                                                    'wtxxAWE_SS_COLOR', // = 23
                                                    'wtDEMO_WINDER');//   cWinderTypeNames

  cYarnUnitNames: array[TYarnUnit] of string = ('yuNone', // = 0
                                                'yuNm', // = 1
                                                'yuNeB', // = 2
                                                'yutex', // = 3
                                                'yuTd', // = 4
                                                'yuNeL', // = 5
                                                'yuNeK', // = 6
                                                'yuNeW', // = 7
                                                'yuTs', // = 8
                                                'yuNcC');//   cYarnUnitNames

  cLengthWindowModeNames: array[TLengthWindowMode] of string = ('lwmNone', // = 0
                                                                'lwmFirst', // = 1
                                                                'lwmLast', // = 2
                                                                'lwmCone');//   cLengthWindowModeNames

  cAWEClassNames: array[TAWEClass] of string = ('aweNone', // = 0
                                                'awePlus', // = 1
                                                'aweSpectra', // = 2
                                                'aweSpectraPlus', // = 3
                                                'aweZenit', // = 4
                                                'aweZenitL2');//   cAWEClassNames

  cSensingHeadClassNames: array[TSensingHeadClass] of string = ('shcNone', // = 0
                                                                'shc8x', // = 1
                                                                'shc9xFS', // = 2
                                                                'shc9xH', // = 3
                                                                'shc9xBD', // = 4
                                                                'shcZenit', // = 5
                                                                'shcZenitF', // = 6
                                                                'shcZenitFP', // = 7
                                                                'shcZenitC', // = 8
                                                                'shcZenitCF', // = 9
                                                                'shcZenitCFP');//   cSensingHeadClassNames

  cFrontTypeNames: array[TFrontType] of string = ('ftNone', // = 0
                                                  'ftZE80', // = 1
                                                  'ftZE800', // = 2
                                                  'ftZE900', // = 3
                                                  'ftZE80i', // = 4
                                                  'ftZE800i', // = 5
                                                  'ftZE900i', // = 6
                                                  'ftInf68K', // = 7
                                                  'ftInfPPC', // = 8
                                                  'ftInfPPCAC5', // = 9
                                                  'ftZEUnknown');//   cFrontTypeNames


// Element Namen und XPath-Konstanten f�r die Selektionspfade
const
  // Element Namen
  cA0Name                           = 'A0';// Automatisch generiert. Nicht �ndern.
  cA10Name                          = 'A10';// Automatisch generiert. Nicht �ndern.
  cA11Name                          = 'A11';// Automatisch generiert. Nicht �ndern.
  cA12Name                          = 'A12';// Automatisch generiert. Nicht �ndern.
  cA13Name                          = 'A13';// Automatisch generiert. Nicht �ndern.
  cA14Name                          = 'A14';// Automatisch generiert. Nicht �ndern.
  cA15Name                          = 'A15';// Automatisch generiert. Nicht �ndern.
  cA1Name                           = 'A1';// Automatisch generiert. Nicht �ndern.
  cA2Name                           = 'A2';// Automatisch generiert. Nicht �ndern.
  cA3Name                           = 'A3';// Automatisch generiert. Nicht �ndern.
  cA4Name                           = 'A4';// Automatisch generiert. Nicht �ndern.
  cA5Name                           = 'A5';// Automatisch generiert. Nicht �ndern.
  cA6Name                           = 'A6';// Automatisch generiert. Nicht �ndern.
  cA7Name                           = 'A7';// Automatisch generiert. Nicht �ndern.
  cA8Name                           = 'A8';// Automatisch generiert. Nicht �ndern.
  cA9Name                           = 'A9';// Automatisch generiert. Nicht �ndern.
  cAdditionalName                   = 'Additional';// Automatisch generiert. Nicht �ndern.
  cAdjustName                       = 'Adjust';// Automatisch generiert. Nicht �ndern.
  cALZerolRefAdjMovementName        = 'ALZerolRefAdjMovement';// Automatisch generiert. Nicht �ndern.
  cALZeroRefFixDarkName             = 'ALZeroRefFixDark';// Automatisch generiert. Nicht �ndern.
  cALZeroRefSensCorrectionName      = 'ALZeroRefSensCorrection';// Automatisch generiert. Nicht �ndern.
  cAssignModeName                   = 'AssignMode';// Automatisch generiert. Nicht �ndern.
  cAWEMachTypeName                  = 'AWEMachType';// Automatisch generiert. Nicht �ndern.
  cB0Name                           = 'B0';// Automatisch generiert. Nicht �ndern.
  cB10Name                          = 'B10';// Automatisch generiert. Nicht �ndern.
  cB11Name                          = 'B11';// Automatisch generiert. Nicht �ndern.
  cB12Name                          = 'B12';// Automatisch generiert. Nicht �ndern.
  cB13Name                          = 'B13';// Automatisch generiert. Nicht �ndern.
  cB14Name                          = 'B14';// Automatisch generiert. Nicht �ndern.
  cB15Name                          = 'B15';// Automatisch generiert. Nicht �ndern.
  cB1Name                           = 'B1';// Automatisch generiert. Nicht �ndern.
  cB2Name                           = 'B2';// Automatisch generiert. Nicht �ndern.
  cB3Name                           = 'B3';// Automatisch generiert. Nicht �ndern.
  cB4Name                           = 'B4';// Automatisch generiert. Nicht �ndern.
  cB5Name                           = 'B5';// Automatisch generiert. Nicht �ndern.
  cB6Name                           = 'B6';// Automatisch generiert. Nicht �ndern.
  cB7Name                           = 'B7';// Automatisch generiert. Nicht �ndern.
  cB8Name                           = 'B8';// Automatisch generiert. Nicht �ndern.
  cB9Name                           = 'B9';// Automatisch generiert. Nicht �ndern.
  cBasicName                        = 'Basic';// Automatisch generiert. Nicht �ndern.
  cBitLowFactorName                 = 'BitLowFactor';// Automatisch generiert. Nicht �ndern.
  cBlockAtAlarmName                 = 'BlockAtAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtClusterAlarmName          = 'BlockAtClusterAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtCutFailureName            = 'BlockAtCutFailure';// Automatisch generiert. Nicht �ndern.
  cBlockAtFAlarmName                = 'BlockAtFAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtFClusterAlarmName         = 'BlockAtFClusterAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtSFIAlarmName              = 'BlockAtSFIAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtShortCountAlarmName       = 'BlockAtShortCountAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockAtYarnCountAlarmName        = 'BlockAtYarnCountAlarm';// Automatisch generiert. Nicht �ndern.
  cBlockByUpExceptionName           = 'BlockByUpException';// Automatisch generiert. Nicht �ndern.
  cBrightName                       = 'Bright';// Automatisch generiert. Nicht �ndern.
  cBunchMonitorName                 = 'BunchMonitor';// Automatisch generiert. Nicht �ndern.
  cC0Name                           = 'C0';// Automatisch generiert. Nicht �ndern.
  cC10Name                          = 'C10';// Automatisch generiert. Nicht �ndern.
  cC11Name                          = 'C11';// Automatisch generiert. Nicht �ndern.
  cC12Name                          = 'C12';// Automatisch generiert. Nicht �ndern.
  cC13Name                          = 'C13';// Automatisch generiert. Nicht �ndern.
  cC14Name                          = 'C14';// Automatisch generiert. Nicht �ndern.
  cC15Name                          = 'C15';// Automatisch generiert. Nicht �ndern.
  cC1Name                           = 'C1';// Automatisch generiert. Nicht �ndern.
  cC2Name                           = 'C2';// Automatisch generiert. Nicht �ndern.
  cC3Name                           = 'C3';// Automatisch generiert. Nicht �ndern.
  cC4Name                           = 'C4';// Automatisch generiert. Nicht �ndern.
  cC5Name                           = 'C5';// Automatisch generiert. Nicht �ndern.
  cC6Name                           = 'C6';// Automatisch generiert. Nicht �ndern.
  cC7Name                           = 'C7';// Automatisch generiert. Nicht �ndern.
  cC8Name                           = 'C8';// Automatisch generiert. Nicht �ndern.
  cC9Name                           = 'C9';// Automatisch generiert. Nicht �ndern.
  cChannelName                      = 'Channel';// Automatisch generiert. Nicht �ndern.
  cCheckLengthName                  = 'CheckLength';// Automatisch generiert. Nicht �ndern.
  cClassBlockIntervalName           = 'ClassBlockInterval';// Automatisch generiert. Nicht �ndern.
  cClassificationName               = 'Classification';// Automatisch generiert. Nicht �ndern.
  cClusterName                      = 'Cluster';// Automatisch generiert. Nicht �ndern.
  cConeChangeConditionActiveName    = 'ConeChangeConditionActive';// Automatisch generiert. Nicht �ndern.
  cConfigCodeAName                  = 'ConfigCodeA';// Automatisch generiert. Nicht �ndern.
  cConfigCodeBName                  = 'ConfigCodeB';// Automatisch generiert. Nicht �ndern.
  cConfigCodeCName                  = 'ConfigCodeC';// Automatisch generiert. Nicht �ndern.
  cConfigCodeDName                  = 'ConfigCodeD';// Automatisch generiert. Nicht �ndern.
  cConfigCodeName                   = 'ConfigCode';// Automatisch generiert. Nicht �ndern.
  cConfigName                       = 'Config';// Automatisch generiert. Nicht �ndern.
  cCountName                        = 'Count';// Automatisch generiert. Nicht �ndern.
  cCutAtYarnBreakName               = 'CutAtYarnBreak';// Automatisch generiert. Nicht �ndern.
  cCutBeforeAdjustName              = 'CutBeforeAdjust';// Automatisch generiert. Nicht �ndern.
  cCutDelayAdditionalAName          = 'CutDelayAdditionalA';// Automatisch generiert. Nicht �ndern.
  cCutDelayAdditionalBName          = 'CutDelayAdditionalB';// Automatisch generiert. Nicht �ndern.
  cCutRetriesName                   = 'CutRetries';// Automatisch generiert. Nicht �ndern.
  cD0Name                           = 'D0';// Automatisch generiert. Nicht �ndern.
  cD10Name                          = 'D10';// Automatisch generiert. Nicht �ndern.
  cD11Name                          = 'D11';// Automatisch generiert. Nicht �ndern.
  cD12Name                          = 'D12';// Automatisch generiert. Nicht �ndern.
  cD13Name                          = 'D13';// Automatisch generiert. Nicht �ndern.
  cD14Name                          = 'D14';// Automatisch generiert. Nicht �ndern.
  cD15Name                          = 'D15';// Automatisch generiert. Nicht �ndern.
  cD1Name                           = 'D1';// Automatisch generiert. Nicht �ndern.
  cD2Name                           = 'D2';// Automatisch generiert. Nicht �ndern.
  cD3Name                           = 'D3';// Automatisch generiert. Nicht �ndern.
  cD4Name                           = 'D4';// Automatisch generiert. Nicht �ndern.
  cD5Name                           = 'D5';// Automatisch generiert. Nicht �ndern.
  cD6Name                           = 'D6';// Automatisch generiert. Nicht �ndern.
  cD7Name                           = 'D7';// Automatisch generiert. Nicht �ndern.
  cD8Name                           = 'D8';// Automatisch generiert. Nicht �ndern.
  cD9Name                           = 'D9';// Automatisch generiert. Nicht �ndern.
  cDarkName                         = 'Dark';// Automatisch generiert. Nicht �ndern.
  cDebugName                        = 'Debug';// Automatisch generiert. Nicht �ndern.
  cDefectsName                      = 'Defects';// Automatisch generiert. Nicht �ndern.
  cDeltaName                        = 'Delta';// Automatisch generiert. Nicht �ndern.
  cDFSInverseName                   = 'DFSInverse';// Automatisch generiert. Nicht �ndern.
  cDFSName                          = 'DFS';// Automatisch generiert. Nicht �ndern.
  cDFSSensitivity1Name              = 'DFSSensitivity1';// Automatisch generiert. Nicht �ndern.
  cDFSSensitivity2Name              = 'DFSSensitivity2';// Automatisch generiert. Nicht �ndern.
  cDiameterControlIntervalName      = 'DiameterControlInterval';// Automatisch generiert. Nicht �ndern.
  cDiameterControllIntervalStepName = 'DiameterControllIntervalStep';// Automatisch generiert. Nicht �ndern.
  cDiaName                          = 'Dia';// Automatisch generiert. Nicht �ndern.
  cDriftCompensationName            = 'DriftCompensation';// Automatisch generiert. Nicht �ndern.
  cDrumCorrectionName               = 'DrumCorrection';// Automatisch generiert. Nicht �ndern.
  cExtendedInterfaceMurataName      = 'ExtendedInterfaceMurata';// Automatisch generiert. Nicht �ndern.
  cExtraName                        = 'Extra';// Automatisch generiert. Nicht �ndern.
  cFAdjustAfterAlarmName            = 'FAdjustAfterAlarm';// Automatisch generiert. Nicht �ndern.
  cFAdjustAtOfflimitName            = 'FAdjustAtOfflimit';// Automatisch generiert. Nicht �ndern.
  cFClassCoarseYarnName             = 'FClassCoarseYarn';// Automatisch generiert. Nicht �ndern.
  cFClearingDuringSpliceName        = 'FClearingDuringSplice';// Automatisch generiert. Nicht �ndern.
  cFill0Name                        = 'Fill0';// Automatisch generiert. Nicht �ndern.
  cFill1Name                        = 'Fill1';// Automatisch generiert. Nicht �ndern.
  cFill2Name                        = 'Fill2';// Automatisch generiert. Nicht �ndern.
  cFill3Name                        = 'Fill3';// Automatisch generiert. Nicht �ndern.
  cFill4Name                        = 'Fill4';// Automatisch generiert. Nicht �ndern.
  cFill5Name                        = 'Fill5';// Automatisch generiert. Nicht �ndern.
  cFill6Name                        = 'Fill6';// Automatisch generiert. Nicht �ndern.
  cFill7Name                        = 'Fill7';// Automatisch generiert. Nicht �ndern.
  cFill8Name                        = 'Fill8';// Automatisch generiert. Nicht �ndern.
  cFill9Name                        = 'Fill9';// Automatisch generiert. Nicht �ndern.
  cFillName                         = 'Fill';// Automatisch generiert. Nicht �ndern.
  cFModeBDName                      = 'FModeBD';// Automatisch generiert. Nicht �ndern.
  cFName                            = 'F';// Automatisch generiert. Nicht �ndern.
  cForeignName                      = 'Foreign';// Automatisch generiert. Nicht �ndern.
  cFP_ClassClearingName             = 'FP_ClassClearing';// Automatisch generiert. Nicht �ndern.
  cFP_ClusterName                   = 'FP_Cluster';// Automatisch generiert. Nicht �ndern.
  cFP_Data_DiameterName             = 'FP_Data_Diameter';// Automatisch generiert. Nicht �ndern.
  cFP_Data_FName                    = 'FP_Data_F';// Automatisch generiert. Nicht �ndern.
  cFP_Data_IPIName                  = 'FP_Data_IPI';// Automatisch generiert. Nicht �ndern.
  cFP_Data_SFIName                  = 'FP_Data_SFI';// Automatisch generiert. Nicht �ndern.
  cFP_Data_ShiftName                = 'FP_Data_Shift';// Automatisch generiert. Nicht �ndern.
  cFP_Data_SpliceName               = 'FP_Data_Splice';// Automatisch generiert. Nicht �ndern.
  cFP_ExtendedClusterName           = 'FP_ExtendedCluster';// Automatisch generiert. Nicht �ndern.
  cFP_FClusterBrightName            = 'FP_FClusterBright';// Automatisch generiert. Nicht �ndern.
  cFP_FClusterDarkName              = 'FP_FClusterDark';// Automatisch generiert. Nicht �ndern.
  cFP_FSpectraBDName                = 'FP_FSpectraBD';// Automatisch generiert. Nicht �ndern.
  cFP_FSpectraHighName              = 'FP_FSpectraHigh';// Automatisch generiert. Nicht �ndern.
  cFP_FSpectraNormalName            = 'FP_FSpectraNormal';// Automatisch generiert. Nicht �ndern.
  cFP_FZenitBrightName              = 'FP_FZenitBright';// Automatisch generiert. Nicht �ndern.
  cFP_FZenitDarkName                = 'FP_FZenitDark';// Automatisch generiert. Nicht �ndern.
  cFP_PName                         = 'FP_P';// Automatisch generiert. Nicht �ndern.
  cFP_SFIName                       = 'FP_SFI';// Automatisch generiert. Nicht �ndern.
  cFP_ShortCountName                = 'FP_ShortCount';// Automatisch generiert. Nicht �ndern.
  cFP_ShortCountRepetitionName      = 'FP_ShortCountRepetition';// Automatisch generiert. Nicht �ndern.
  cFP_SpeedSimulationName           = 'FP_SpeedSimulation';// Automatisch generiert. Nicht �ndern.
  cFP_SpliceCheckName               = 'FP_SpliceCheck';// Automatisch generiert. Nicht �ndern.
  cFP_SpliceClassClearingName       = 'FP_SpliceClassClearing';// Automatisch generiert. Nicht �ndern.
  cFP_UpperYarnChannelName          = 'FP_UpperYarnChannel';// Automatisch generiert. Nicht �ndern.
  cFP_VCVName                       = 'FP_VCV';// Automatisch generiert. Nicht �ndern.
  cFP_YarnCountRepetitionName       = 'FP_YarnCountRepetition';// Automatisch generiert. Nicht �ndern.
  cFP_ZenitCName                    = 'FP_ZenitC';// Automatisch generiert. Nicht �ndern.
  cFP_ZenitName                     = 'FP_Zenit';// Automatisch generiert. Nicht �ndern.
  cFPatternName                     = 'FPattern';// Automatisch generiert. Nicht �ndern.
  cFrontTypeName                    = 'FrontType';// Automatisch generiert. Nicht �ndern.
  cFSensorActiveName                = 'FSensorActive';// Automatisch generiert. Nicht �ndern.
  cFullSysInfoName                  = 'FullSysInfo';// Automatisch generiert. Nicht �ndern.
  cFWSFSOutName                     = 'FWSFSOut';// Automatisch generiert. Nicht �ndern.
  cGroupName                        = 'Group';// Automatisch generiert. Nicht �ndern.
  cHeadstockRightName               = 'HeadstockRight';// Automatisch generiert. Nicht �ndern.
  cinopSettingsName                 = 'inopSettings';// Automatisch generiert. Nicht �ndern.
  cio_ClassClearingName             = 'io_ClassClearing';// Automatisch generiert. Nicht �ndern.
  cio_ClusterLockName               = 'io_ClusterLock';// Automatisch generiert. Nicht �ndern.
  cio_ClusterRepName                = 'io_ClusterRep';// Automatisch generiert. Nicht �ndern.
  cio_CountLengthName               = 'io_CountLength';// Automatisch generiert. Nicht �ndern.
  cio_CountLockName                 = 'io_CountLock';// Automatisch generiert. Nicht �ndern.
  cio_CutFailedLockName             = 'io_CutFailedLock';// Automatisch generiert. Nicht �ndern.
  cio_CutRetriesName                = 'io_CutRetries';// Automatisch generiert. Nicht �ndern.
  cio_ExtMurataName                 = 'io_ExtMurata';// Automatisch generiert. Nicht �ndern.
  cio_FaultClusterName              = 'io_FaultCluster';// Automatisch generiert. Nicht �ndern.
  cio_FfAdjustAlarmName             = 'io_FfAdjustAlarm';// Automatisch generiert. Nicht �ndern.
  cio_FfAdjustOfflimitName          = 'io_FfAdjustOfflimit';// Automatisch generiert. Nicht �ndern.
  cio_FfClSpliceCheckName           = 'io_FfClSpliceCheck';// Automatisch generiert. Nicht �ndern.
  cio_FfClusterLockName             = 'io_FfClusterLock';// Automatisch generiert. Nicht �ndern.
  cio_FfClusterMonitorName          = 'io_FfClusterMonitor';// Automatisch generiert. Nicht �ndern.
  cio_FfClusterRepName              = 'io_FfClusterRep';// Automatisch generiert. Nicht �ndern.
  cio_FfMonitorName                 = 'io_FfMonitor';// Automatisch generiert. Nicht �ndern.
  cio_FfSensorActiveName            = 'io_FfSensorActive';// Automatisch generiert. Nicht �ndern.
  cio_FfSensorTypName               = 'io_FfSensorTyp';// Automatisch generiert. Nicht �ndern.
  cio_FfStartupRepName              = 'io_FfStartupRep';// Automatisch generiert. Nicht �ndern.
  cio_HeadstockName                 = 'io_Headstock';// Automatisch generiert. Nicht �ndern.
  cio_Inop1Name                     = 'io_Inop1';// Automatisch generiert. Nicht �ndern.
  cio_Inop2Name                     = 'io_Inop2';// Automatisch generiert. Nicht �ndern.
  cio_LongThinClusterName           = 'io_LongThinCluster';// Automatisch generiert. Nicht �ndern.
  cio_MinusDiaDiffName              = 'io_MinusDiaDiff';// Automatisch generiert. Nicht �ndern.
  cio_MMMachConfigName              = 'io_MMMachConfig';// Automatisch generiert. Nicht �ndern.
  cio_OffCountRepName               = 'io_OffCountRep';// Automatisch generiert. Nicht �ndern.
  cio_OnePulsUmdrehungName          = 'io_OnePulsUmdrehung';// Automatisch generiert. Nicht �ndern.
  cio_SfiLockName                   = 'io_SfiLock';// Automatisch generiert. Nicht �ndern.
  cio_SfiName                       = 'io_Sfi';// Automatisch generiert. Nicht �ndern.
  cio_SfiRepName                    = 'io_SfiRep';// Automatisch generiert. Nicht �ndern.
  cio_ShortCountName                = 'io_ShortCount';// Automatisch generiert. Nicht �ndern.
  cio_ShortSfiName                  = 'io_ShortSfi';// Automatisch generiert. Nicht �ndern.
  cio_SpeedName                     = 'io_Speed';// Automatisch generiert. Nicht �ndern.
  cio_SpeedRampName                 = 'io_SpeedRamp';// Automatisch generiert. Nicht �ndern.
  cio_SpliceCheckName               = 'io_SpliceCheck';// Automatisch generiert. Nicht �ndern.
  cio_UpperYarnCheckName            = 'io_UpperYarnCheck';// Automatisch generiert. Nicht �ndern.
  cIPICalculatedOnMachineName       = 'IPICalculatedOnMachine';// Automatisch generiert. Nicht �ndern.
  cIPILimitName                     = 'IPILimit';// Automatisch generiert. Nicht �ndern.
  cKnifeCutOffPowerHighName         = 'KnifeCutOffPowerHigh';// Automatisch generiert. Nicht �ndern.
  cKnifeMonitorName                 = 'KnifeMonitor';// Automatisch generiert. Nicht �ndern.
  cLengthModeName                   = 'LengthMode';// Automatisch generiert. Nicht �ndern.
  cLengthName                       = 'Length';// Automatisch generiert. Nicht �ndern.
  cLengthWindowName                 = 'LengthWindow';// Automatisch generiert. Nicht �ndern.
  cLimitLowFactorName               = 'LimitLowFactor';// Automatisch generiert. Nicht �ndern.
  cLimitName                        = 'Limit';// Automatisch generiert. Nicht �ndern.
  cLongName                         = 'Long';// Automatisch generiert. Nicht �ndern.
  cLongSpliceCheckLengthName        = 'LongSpliceCheckLength';// Automatisch generiert. Nicht �ndern.
  cLongStopDefName                  = 'LongStopDef';// Automatisch generiert. Nicht �ndern.
  cLowerName                        = 'Lower';// Automatisch generiert. Nicht �ndern.
  cLTGBreakBlockOnName              = 'LTGBreakBlockOn';// Automatisch generiert. Nicht �ndern.
  cMachineName                      = 'Machine';// Automatisch generiert. Nicht �ndern.
  cMachNameName                     = 'MachName';// Automatisch generiert. Nicht �ndern.
  cMachTypeName                     = 'MachType';// Automatisch generiert. Nicht �ndern.
  cMaintenanceName                  = 'Maintenance';// Automatisch generiert. Nicht �ndern.
  cModeName                         = 'Mode';// Automatisch generiert. Nicht �ndern.
  cNegDiaDiffName                   = 'NegDiaDiff';// Automatisch generiert. Nicht �ndern.
  cNepsName                         = 'Neps';// Automatisch generiert. Nicht �ndern.
  cNotMMName                        = 'NotMM';// Automatisch generiert. Nicht �ndern.
  cNrOfGroupsName                   = 'NrOfGroups';// Automatisch generiert. Nicht �ndern.
  cNrOfSectionsName                 = 'NrOfSections';// Automatisch generiert. Nicht �ndern.
  cNrOfSpindlesName                 = 'NrOfSpindles';// Automatisch generiert. Nicht �ndern.
  cNSLTName                         = 'NSLT';// Automatisch generiert. Nicht �ndern.
  cNullCutGeneralName               = 'NullCutGeneral';// Automatisch generiert. Nicht �ndern.
  cNullCutSFSName                   = 'NullCutSFS';// Automatisch generiert. Nicht �ndern.
  cObsLengthName                    = 'ObsLength';// Automatisch generiert. Nicht �ndern.
  cPClearingDuringSpliceName        = 'PClearingDuringSplice';// Automatisch generiert. Nicht �ndern.
  cPilotSpindlesName                = 'PilotSpindles';// Automatisch generiert. Nicht �ndern.
  cPName                            = 'P';// Automatisch generiert. Nicht �ndern.
  cPosDiaDiffName                   = 'PosDiaDiff';// Automatisch generiert. Nicht �ndern.
  cPReductionName                   = 'PReduction';// Automatisch generiert. Nicht �ndern.
  cProdGrpIDName                    = 'ProdGrpID';// Automatisch generiert. Nicht �ndern.
  cReductionName                    = 'Reduction';// Automatisch generiert. Nicht �ndern.
  cReductionSwitchName              = 'ReductionSwitch';// Automatisch generiert. Nicht �ndern.
  cReferenceName                    = 'Reference';// Automatisch generiert. Nicht �ndern.
  cRefLengthName                    = 'RefLength';// Automatisch generiert. Nicht �ndern.
  cRefTypeName                      = 'RefType';// Automatisch generiert. Nicht �ndern.
  cReinTech__Name                   = 'ReinTech__';// Automatisch generiert. Nicht �ndern.
  cReinVerName                      = 'ReinVer';// Automatisch generiert. Nicht �ndern.
  cRemoveYarnAfterAdjustName        = 'RemoveYarnAfterAdjust';// Automatisch generiert. Nicht �ndern.
  cRepetitionName                   = 'Repetition';// Automatisch generiert. Nicht �ndern.
  cRequest__Name                    = 'Request__';// Automatisch generiert. Nicht �ndern.
  cSensingHeadName                  = 'SensingHead';// Automatisch generiert. Nicht �ndern.
  cSeparateSpliceCheckSettingsName  = 'SeparateSpliceCheckSettings';// Automatisch generiert. Nicht �ndern.
  cSFIName                          = 'SFI';// Automatisch generiert. Nicht �ndern.
  cSFSImpulseblockCtrlName          = 'SFSImpulseblockCtrl';// Automatisch generiert. Nicht �ndern.
  cSFSSensitivityName               = 'SFSSensitivity';// Automatisch generiert. Nicht �ndern.
  cSHControllerSpeedAName           = 'SHControllerSpeedA';// Automatisch generiert. Nicht �ndern.
  cSHControllerSpeedBName           = 'SHControllerSpeedB';// Automatisch generiert. Nicht �ndern.
  cShortCountName                   = 'ShortCount';// Automatisch generiert. Nicht �ndern.
  cShortName                        = 'Short';// Automatisch generiert. Nicht �ndern.
  cShortSFIDName                    = 'ShortSFID';// Automatisch generiert. Nicht �ndern.
  cSHZeroCutName                    = 'SHZeroCut';// Automatisch generiert. Nicht �ndern.
  cSmallMinusName                   = 'SmallMinus';// Automatisch generiert. Nicht �ndern.
  cSmallPlusName                    = 'SmallPlus';// Automatisch generiert. Nicht �ndern.
  cSpeedName                        = 'Speed';// Automatisch generiert. Nicht �ndern.
  cSpeedRampName                    = 'SpeedRamp';// Automatisch generiert. Nicht �ndern.
  cSpeedSimulationName              = 'SpeedSimulation';// Automatisch generiert. Nicht �ndern.
  cSpindleFromName                  = 'SpindleFrom';// Automatisch generiert. Nicht �ndern.
  cSpindleToName                    = 'SpindleTo';// Automatisch generiert. Nicht �ndern.
  cSpliceClassificationName         = 'SpliceClassification';// Automatisch generiert. Nicht �ndern.
  cSpliceName                       = 'Splice';// Automatisch generiert. Nicht �ndern.
  cStartupRepetitionName            = 'StartupRepetition';// Automatisch generiert. Nicht �ndern.
  cStartupRepetitionSwitchName      = 'StartupRepetitionSwitch';// Automatisch generiert. Nicht �ndern.
  cSuckOffLastCutOnlyName           = 'SuckOffLastCutOnly';// Automatisch generiert. Nicht �ndern.
  cSwitchName                       = 'Switch';// Automatisch generiert. Nicht �ndern.
  cSWOptionName                     = 'SWOption';// Automatisch generiert. Nicht �ndern.
  cTestmodeName                     = 'Testmode';// Automatisch generiert. Nicht �ndern.
  cThickName                        = 'Thick';// Automatisch generiert. Nicht �ndern.
  cThinName                         = 'Thin';// Automatisch generiert. Nicht �ndern.
  cThreadCountName                  = 'ThreadCount';// Automatisch generiert. Nicht �ndern.
  cUpperName                        = 'Upper';// Automatisch generiert. Nicht �ndern.
  cUpperYarnCheckName               = 'UpperYarnCheck';// Automatisch generiert. Nicht �ndern.
  cUpperYarnName                    = 'UpperYarn';// Automatisch generiert. Nicht �ndern.
  cValuesName                       = 'Values';// Automatisch generiert. Nicht �ndern.
  cVCVName                          = 'VCV';// Automatisch generiert. Nicht �ndern.
  cYarnCountName                    = 'YarnCount';// Automatisch generiert. Nicht �ndern.
  cYarnName                         = 'Yarn';// Automatisch generiert. Nicht �ndern.
  cYarnUnitName                     = 'YarnUnit';// Automatisch generiert. Nicht �ndern.
  cYMSettingName                    = 'YMSetting';// Automatisch generiert. Nicht �ndern.
  cYMSwVersionName                  = 'YMSwVersion';// Automatisch generiert. Nicht �ndern.
  cYMTypeName                       = 'YMType';// Automatisch generiert. Nicht �ndern.
  cZeroTestName                     = 'ZeroTest';// Automatisch generiert. Nicht �ndern.

  // XPath
  cXPYMSettingNode                    = '/LoepfeBody/YMSetting';// Automatisch generiert. Nicht �ndern.
  cXPBasicNode                        = '/LoepfeBody/YMSetting/Basic';// Automatisch generiert. Nicht �ndern.
  cXPChannelNode                      = '/LoepfeBody/YMSetting/Basic/Channel';// Automatisch generiert. Nicht �ndern.
  cXPChannelNepsNode                  = '/LoepfeBody/YMSetting/Basic/Channel/Neps';// Automatisch generiert. Nicht �ndern.
  cXPChannelNepsSwitchItem            = '/LoepfeBody/YMSetting/Basic/Channel/Neps/Switch';// Automatisch generiert. Nicht �ndern.
  cXPChannelNepsDiaItem               = '/LoepfeBody/YMSetting/Basic/Channel/Neps/Dia';// Automatisch generiert. Nicht �ndern.
  cXPChannelShortNode                 = '/LoepfeBody/YMSetting/Basic/Channel/Short';// Automatisch generiert. Nicht �ndern.
  cXPChannelShortSwitchItem           = '/LoepfeBody/YMSetting/Basic/Channel/Short/Switch';// Automatisch generiert. Nicht �ndern.
  cXPChannelShortDiaItem              = '/LoepfeBody/YMSetting/Basic/Channel/Short/Dia';// Automatisch generiert. Nicht �ndern.
  cXPChannelShortLengthItem           = '/LoepfeBody/YMSetting/Basic/Channel/Short/Length';// Automatisch generiert. Nicht �ndern.
  cXPChannelLongNode                  = '/LoepfeBody/YMSetting/Basic/Channel/Long';// Automatisch generiert. Nicht �ndern.
  cXPChannelLongSwitchItem            = '/LoepfeBody/YMSetting/Basic/Channel/Long/Switch';// Automatisch generiert. Nicht �ndern.
  cXPChannelLongDiaItem               = '/LoepfeBody/YMSetting/Basic/Channel/Long/Dia';// Automatisch generiert. Nicht �ndern.
  cXPChannelLongLengthItem            = '/LoepfeBody/YMSetting/Basic/Channel/Long/Length';// Automatisch generiert. Nicht �ndern.
  cXPChannelThinNode                  = '/LoepfeBody/YMSetting/Basic/Channel/Thin';// Automatisch generiert. Nicht �ndern.
  cXPChannelThinSwitchItem            = '/LoepfeBody/YMSetting/Basic/Channel/Thin/Switch';// Automatisch generiert. Nicht �ndern.
  cXPChannelThinDiaItem               = '/LoepfeBody/YMSetting/Basic/Channel/Thin/Dia';// Automatisch generiert. Nicht �ndern.
  cXPChannelThinLengthItem            = '/LoepfeBody/YMSetting/Basic/Channel/Thin/Length';// Automatisch generiert. Nicht �ndern.
  cXPChannelSpliceNode                = '/LoepfeBody/YMSetting/Basic/Channel/Splice';// Automatisch generiert. Nicht �ndern.
  cXPChannelSpliceSwitchItem          = '/LoepfeBody/YMSetting/Basic/Channel/Splice/Switch';// Automatisch generiert. Nicht �ndern.
  cXPChannelSpliceLengthItem          = '/LoepfeBody/YMSetting/Basic/Channel/Splice/Length';// Automatisch generiert. Nicht �ndern.
  cXPNSLTNode                         = '/LoepfeBody/YMSetting/Basic/Channel/NSLT';// Automatisch generiert. Nicht �ndern.
  cXPChannelNSLTSwitchItem            = '/LoepfeBody/YMSetting/Basic/Channel/NSLT/Switch';// Automatisch generiert. Nicht �ndern.
  cXPNSLTStartupRepetitionItem        = '/LoepfeBody/YMSetting/Basic/Channel/NSLT/StartupRepetition';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingBasicClassificationItem = '/LoepfeBody/YMSetting/Basic/Classification';// Automatisch generiert. Nicht �ndern.
  cXPBasicSpliceNode                  = '/LoepfeBody/YMSetting/Basic/Splice';// Automatisch generiert. Nicht �ndern.
  cXPSpliceNepsNode                   = '/LoepfeBody/YMSetting/Basic/Splice/Neps';// Automatisch generiert. Nicht �ndern.
  cXPSpliceNepsSwitchItem             = '/LoepfeBody/YMSetting/Basic/Splice/Neps/Switch';// Automatisch generiert. Nicht �ndern.
  cXPSpliceNepsDiaItem                = '/LoepfeBody/YMSetting/Basic/Splice/Neps/Dia';// Automatisch generiert. Nicht �ndern.
  cXPSpliceShortNode                  = '/LoepfeBody/YMSetting/Basic/Splice/Short';// Automatisch generiert. Nicht �ndern.
  cXPSpliceShortSwitchItem            = '/LoepfeBody/YMSetting/Basic/Splice/Short/Switch';// Automatisch generiert. Nicht �ndern.
  cXPSpliceShortDiaItem               = '/LoepfeBody/YMSetting/Basic/Splice/Short/Dia';// Automatisch generiert. Nicht �ndern.
  cXPSpliceShortLengthItem            = '/LoepfeBody/YMSetting/Basic/Splice/Short/Length';// Automatisch generiert. Nicht �ndern.
  cXPSpliceLongNode                   = '/LoepfeBody/YMSetting/Basic/Splice/Long';// Automatisch generiert. Nicht �ndern.
  cXPSpliceLongSwitchItem             = '/LoepfeBody/YMSetting/Basic/Splice/Long/Switch';// Automatisch generiert. Nicht �ndern.
  cXPSpliceLongDiaItem                = '/LoepfeBody/YMSetting/Basic/Splice/Long/Dia';// Automatisch generiert. Nicht �ndern.
  cXPSpliceLongLengthItem             = '/LoepfeBody/YMSetting/Basic/Splice/Long/Length';// Automatisch generiert. Nicht �ndern.
  cXPSpliceThinNode                   = '/LoepfeBody/YMSetting/Basic/Splice/Thin';// Automatisch generiert. Nicht �ndern.
  cXPSpliceThinSwitchItem             = '/LoepfeBody/YMSetting/Basic/Splice/Thin/Switch';// Automatisch generiert. Nicht �ndern.
  cXPSpliceThinDiaItem                = '/LoepfeBody/YMSetting/Basic/Splice/Thin/Dia';// Automatisch generiert. Nicht �ndern.
  cXPSpliceThinLengthItem             = '/LoepfeBody/YMSetting/Basic/Splice/Thin/Length';// Automatisch generiert. Nicht �ndern.
  cXPUpperYarnNode                    = '/LoepfeBody/YMSetting/Basic/Splice/UpperYarn';// Automatisch generiert. Nicht �ndern.
  cXPSpliceUpperYarnSwitchItem        = '/LoepfeBody/YMSetting/Basic/Splice/UpperYarn/Switch';// Automatisch generiert. Nicht �ndern.
  cXPSpliceUpperYarnDiaItem           = '/LoepfeBody/YMSetting/Basic/Splice/UpperYarn/Dia';// Automatisch generiert. Nicht �ndern.
  cXPSeparateSpliceCheckSettingsItem  = '/LoepfeBody/YMSetting/Basic/Splice/SeparateSpliceCheckSettings';// Automatisch generiert. Nicht �ndern.
  cXPSpliceClassificationItem         = '/LoepfeBody/YMSetting/Basic/SpliceClassification';// Automatisch generiert. Nicht �ndern.
  cXPBasicClusterNode                 = '/LoepfeBody/YMSetting/Basic/Cluster';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortNode                 = '/LoepfeBody/YMSetting/Basic/Cluster/Short';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortSwitchItem           = '/LoepfeBody/YMSetting/Basic/Cluster/Short/Switch';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortDiaItem              = '/LoepfeBody/YMSetting/Basic/Cluster/Short/Dia';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortLengthItem           = '/LoepfeBody/YMSetting/Basic/Cluster/Short/Length';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortObsLengthItem        = '/LoepfeBody/YMSetting/Basic/Cluster/Short/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortDefectsItem          = '/LoepfeBody/YMSetting/Basic/Cluster/Short/Defects';// Automatisch generiert. Nicht �ndern.
  cXPClusterShortRepetitionItem       = '/LoepfeBody/YMSetting/Basic/Cluster/Short/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongNode                  = '/LoepfeBody/YMSetting/Basic/Cluster/Long';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongSwitchItem            = '/LoepfeBody/YMSetting/Basic/Cluster/Long/Switch';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongDiaItem               = '/LoepfeBody/YMSetting/Basic/Cluster/Long/Dia';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongLengthItem            = '/LoepfeBody/YMSetting/Basic/Cluster/Long/Length';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongObsLengthItem         = '/LoepfeBody/YMSetting/Basic/Cluster/Long/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongDefectsItem           = '/LoepfeBody/YMSetting/Basic/Cluster/Long/Defects';// Automatisch generiert. Nicht �ndern.
  cXPClusterLongRepetitionItem        = '/LoepfeBody/YMSetting/Basic/Cluster/Long/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinNode                  = '/LoepfeBody/YMSetting/Basic/Cluster/Thin';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinSwitchItem            = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/Switch';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinDiaItem               = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/Dia';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinLengthItem            = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/Length';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinObsLengthItem         = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinDefectsItem           = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/Defects';// Automatisch generiert. Nicht �ndern.
  cXPClusterThinRepetitionItem        = '/LoepfeBody/YMSetting/Basic/Cluster/Thin/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPForeignNode                      = '/LoepfeBody/YMSetting/Foreign';// Automatisch generiert. Nicht �ndern.
  cXPFNode                            = '/LoepfeBody/YMSetting/Foreign/F';// Automatisch generiert. Nicht �ndern.
  cXPDarkNode                         = '/LoepfeBody/YMSetting/Foreign/F/Dark';// Automatisch generiert. Nicht �ndern.
  cXPFDarkClassificationItem          = '/LoepfeBody/YMSetting/Foreign/F/Dark/Classification';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterNode                  = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterSwitchItem            = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster/Switch';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterObsLengthItem         = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterDefectsItem           = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster/Defects';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterRepetitionItem        = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPDarkClusterClassificationItem    = '/LoepfeBody/YMSetting/Foreign/F/Dark/Cluster/Classification';// Automatisch generiert. Nicht �ndern.
  cXPBrightNode                       = '/LoepfeBody/YMSetting/Foreign/F/Bright';// Automatisch generiert. Nicht �ndern.
  cXPFBrightClassificationItem        = '/LoepfeBody/YMSetting/Foreign/F/Bright/Classification';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterNode                = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterSwitchItem          = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster/Switch';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterObsLengthItem       = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterDefectsItem         = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster/Defects';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterRepetitionItem      = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPBrightClusterClassificationItem  = '/LoepfeBody/YMSetting/Foreign/F/Bright/Cluster/Classification';// Automatisch generiert. Nicht �ndern.
  cXPFStartupRepetitionSwitchItem     = '/LoepfeBody/YMSetting/Foreign/F/StartupRepetitionSwitch';// Automatisch generiert. Nicht �ndern.
  cXPFStartupRepetitionItem           = '/LoepfeBody/YMSetting/Foreign/F/StartupRepetition';// Automatisch generiert. Nicht �ndern.
  cXPFModeBDItem                      = '/LoepfeBody/YMSetting/Foreign/F/FModeBD';// Automatisch generiert. Nicht �ndern.
  cXPPNode                            = '/LoepfeBody/YMSetting/Foreign/P';// Automatisch generiert. Nicht �ndern.
  cXPForeignPSwitchItem               = '/LoepfeBody/YMSetting/Foreign/P/Switch';// Automatisch generiert. Nicht �ndern.
  cXPLimitItem                        = '/LoepfeBody/YMSetting/Foreign/P/Limit';// Automatisch generiert. Nicht �ndern.
  cXPRefLengthItem                    = '/LoepfeBody/YMSetting/Foreign/P/RefLength';// Automatisch generiert. Nicht �ndern.
  cXPPStartupRepetitionSwitchItem     = '/LoepfeBody/YMSetting/Foreign/P/StartupRepetitionSwitch';// Automatisch generiert. Nicht �ndern.
  cXPPStartupRepetitionItem           = '/LoepfeBody/YMSetting/Foreign/P/StartupRepetition';// Automatisch generiert. Nicht �ndern.
  cXPLimitLowFactorItem               = '/LoepfeBody/YMSetting/Foreign/P/LimitLowFactor';// Automatisch generiert. Nicht �ndern.
  cXPBitLowFactorItem                 = '/LoepfeBody/YMSetting/Foreign/P/BitLowFactor';// Automatisch generiert. Nicht �ndern.
  cXPPReductionItem                   = '/LoepfeBody/YMSetting/Foreign/P/PReduction';// Automatisch generiert. Nicht �ndern.
  cXPSFINode                          = '/LoepfeBody/YMSetting/SFI';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingSFISwitchItem           = '/LoepfeBody/YMSetting/SFI/Switch';// Automatisch generiert. Nicht �ndern.
  cXPRefTypeItem                      = '/LoepfeBody/YMSetting/SFI/RefType';// Automatisch generiert. Nicht �ndern.
  cXPReferenceItem                    = '/LoepfeBody/YMSetting/SFI/Reference';// Automatisch generiert. Nicht �ndern.
  cXPSFIUpperItem                     = '/LoepfeBody/YMSetting/SFI/Upper';// Automatisch generiert. Nicht �ndern.
  cXPSFILowerItem                     = '/LoepfeBody/YMSetting/SFI/Lower';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingSFIRepetitionItem       = '/LoepfeBody/YMSetting/SFI/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPVCVNode                          = '/LoepfeBody/YMSetting/VCV';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingVCVSwitchItem           = '/LoepfeBody/YMSetting/VCV/Switch';// Automatisch generiert. Nicht �ndern.
  cXPVCVUpperItem                     = '/LoepfeBody/YMSetting/VCV/Upper';// Automatisch generiert. Nicht �ndern.
  cXPVCVLowerItem                     = '/LoepfeBody/YMSetting/VCV/Lower';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingVCVLengthItem           = '/LoepfeBody/YMSetting/VCV/Length';// Automatisch generiert. Nicht �ndern.
  cXPYMSettingVCVRepetitionItem       = '/LoepfeBody/YMSetting/VCV/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPIPILimitNode                     = '/LoepfeBody/YMSetting/IPILimit';// Automatisch generiert. Nicht �ndern.
  cXPNepsItem                         = '/LoepfeBody/YMSetting/IPILimit/Neps';// Automatisch generiert. Nicht �ndern.
  cXPThickItem                        = '/LoepfeBody/YMSetting/IPILimit/Thick';// Automatisch generiert. Nicht �ndern.
  cXPSmallPlusItem                    = '/LoepfeBody/YMSetting/IPILimit/SmallPlus';// Automatisch generiert. Nicht �ndern.
  cXPSmallMinusItem                   = '/LoepfeBody/YMSetting/IPILimit/SmallMinus';// Automatisch generiert. Nicht �ndern.
  cXPThinItem                         = '/LoepfeBody/YMSetting/IPILimit/Thin';// Automatisch generiert. Nicht �ndern.
  cXPAdditionalNode                   = '/LoepfeBody/YMSetting/Additional';// Automatisch generiert. Nicht �ndern.
  cXPYarnNode                         = '/LoepfeBody/YMSetting/Additional/Yarn';// Automatisch generiert. Nicht �ndern.
  cXPYarnCountItem                    = '/LoepfeBody/YMSetting/Additional/Yarn/YarnCount';// Automatisch generiert. Nicht �ndern.
  cXPYarnUnitItem                     = '/LoepfeBody/YMSetting/Additional/Yarn/YarnUnit';// Automatisch generiert. Nicht �ndern.
  cXPThreadCountItem                  = '/LoepfeBody/YMSetting/Additional/Yarn/ThreadCount';// Automatisch generiert. Nicht �ndern.
  cXPCountNode                        = '/LoepfeBody/YMSetting/Additional/Yarn/Count';// Automatisch generiert. Nicht �ndern.
  cXPYarnCountSwitchItem              = '/LoepfeBody/YMSetting/Additional/Yarn/Count/Switch';// Automatisch generiert. Nicht �ndern.
  cXPCountPosDiaDiffItem              = '/LoepfeBody/YMSetting/Additional/Yarn/Count/PosDiaDiff';// Automatisch generiert. Nicht �ndern.
  cXPCountNegDiaDiffItem              = '/LoepfeBody/YMSetting/Additional/Yarn/Count/NegDiaDiff';// Automatisch generiert. Nicht �ndern.
  cXPYarnCountRepetitionItem          = '/LoepfeBody/YMSetting/Additional/Yarn/Count/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPYarnCountObsLengthItem           = '/LoepfeBody/YMSetting/Additional/Yarn/Count/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPShortCountNode                   = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount';// Automatisch generiert. Nicht �ndern.
  cXPYarnShortCountSwitchItem         = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount/Switch';// Automatisch generiert. Nicht �ndern.
  cXPShortCountPosDiaDiffItem         = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount/PosDiaDiff';// Automatisch generiert. Nicht �ndern.
  cXPShortCountNegDiaDiffItem         = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount/NegDiaDiff';// Automatisch generiert. Nicht �ndern.
  cXPYarnShortCountRepetitionItem     = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount/Repetition';// Automatisch generiert. Nicht �ndern.
  cXPYarnShortCountObsLengthItem      = '/LoepfeBody/YMSetting/Additional/Yarn/ShortCount/ObsLength';// Automatisch generiert. Nicht �ndern.
  cXPAdjustNode                       = '/LoepfeBody/YMSetting/Additional/Adjust';// Automatisch generiert. Nicht �ndern.
  cXPReductionSwitchItem              = '/LoepfeBody/YMSetting/Additional/Adjust/ReductionSwitch';// Automatisch generiert. Nicht �ndern.
  cXPReductionItem                    = '/LoepfeBody/YMSetting/Additional/Adjust/Reduction';// Automatisch generiert. Nicht �ndern.
  cXPModeItem                         = '/LoepfeBody/YMSetting/Additional/Adjust/Mode';// Automatisch generiert. Nicht �ndern.
  cXPMaintenanceNode                  = '/LoepfeBody/YMSetting/Maintenance';// Automatisch generiert. Nicht �ndern.
  cXPMaintenanceConfigCodeNode        = '/LoepfeBody/YMSetting/Maintenance/ConfigCode';// Automatisch generiert. Nicht �ndern.
  cXPFSensorActiveItem                = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/FSensorActive';// Automatisch generiert. Nicht �ndern.
  cXPDFSSensitivity1Item              = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/DFSSensitivity1';// Automatisch generiert. Nicht �ndern.
  cXPDFSSensitivity2Item              = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/DFSSensitivity2';// Automatisch generiert. Nicht �ndern.
  cXPSFSSensitivityItem               = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/SFSSensitivity';// Automatisch generiert. Nicht �ndern.
  cXPFClearingDuringSpliceItem        = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/FClearingDuringSplice';// Automatisch generiert. Nicht �ndern.
  cXPPClearingDuringSpliceItem        = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/PClearingDuringSplice';// Automatisch generiert. Nicht �ndern.
  cXPBunchMonitorItem                 = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/BunchMonitor';// Automatisch generiert. Nicht �ndern.
  cXPFAdjustAtOfflimitItem            = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/FAdjustAtOfflimit';// Automatisch generiert. Nicht �ndern.
  cXPFAdjustAfterAlarmItem            = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/FAdjustAfterAlarm';// Automatisch generiert. Nicht �ndern.
  cXPCutAtYarnBreakItem               = '/LoepfeBody/YMSetting/Maintenance/ConfigCode/CutAtYarnBreak';// Automatisch generiert. Nicht �ndern.
  cXPDrumCorrectionItem               = '/LoepfeBody/YMSetting/Maintenance/DrumCorrection';// Automatisch generiert. Nicht �ndern.
  cXPConfigNode                       = '/LoepfeBody/Config';// Automatisch generiert. Nicht �ndern.
  cXPGroupNode                        = '/LoepfeBody/Config/Group';// Automatisch generiert. Nicht �ndern.
  cXPReinVerItem                      = '/LoepfeBody/Config/Group/ReinVer';// Automatisch generiert. Nicht �ndern.
  cXPSensingHeadItem                  = '/LoepfeBody/Config/Group/SensingHead';// Automatisch generiert. Nicht �ndern.
  cXPCutRetriesItem                   = '/LoepfeBody/Config/Group/CutRetries';// Automatisch generiert. Nicht �ndern.
  cXPAssignModeItem                   = '/LoepfeBody/Config/Group/AssignMode';// Automatisch generiert. Nicht �ndern.
  cXPProdGrpIDItem                    = '/LoepfeBody/Config/Group/ProdGrpID';// Automatisch generiert. Nicht �ndern.
  cXPPilotSpindlesItem                = '/LoepfeBody/Config/Group/PilotSpindles';// Automatisch generiert. Nicht �ndern.
  cXPGroupItem                        = '/LoepfeBody/Config/Group/Group';// Automatisch generiert. Nicht �ndern.
  cXPSpindleFromItem                  = '/LoepfeBody/Config/Group/SpindleFrom';// Automatisch generiert. Nicht �ndern.
  cXPSpindleToItem                    = '/LoepfeBody/Config/Group/SpindleTo';// Automatisch generiert. Nicht �ndern.
  cXPSpeedRampItem                    = '/LoepfeBody/Config/Group/SpeedRamp';// Automatisch generiert. Nicht �ndern.
  cXPSpeedItem                        = '/LoepfeBody/Config/Group/Speed';// Automatisch generiert. Nicht �ndern.
  cXPFPatternNode                     = '/LoepfeBody/Config/Group/FPattern';// Automatisch generiert. Nicht �ndern.
  cXPFP_ClassClearingItem             = '/LoepfeBody/Config/Group/FPattern/FP_ClassClearing';// Automatisch generiert. Nicht �ndern.
  cXPFP_SpliceCheckItem               = '/LoepfeBody/Config/Group/FPattern/FP_SpliceCheck';// Automatisch generiert. Nicht �ndern.
  cXPFP_ShortCountItem                = '/LoepfeBody/Config/Group/FPattern/FP_ShortCount';// Automatisch generiert. Nicht �ndern.
  cXPFP_UpperYarnChannelItem          = '/LoepfeBody/Config/Group/FPattern/FP_UpperYarnChannel';// Automatisch generiert. Nicht �ndern.
  cXPFP_ClusterItem                   = '/LoepfeBody/Config/Group/FPattern/FP_Cluster';// Automatisch generiert. Nicht �ndern.
  cXPFP_ExtendedClusterItem           = '/LoepfeBody/Config/Group/FPattern/FP_ExtendedCluster';// Automatisch generiert. Nicht �ndern.
  cXPFP_SFIItem                       = '/LoepfeBody/Config/Group/FPattern/FP_SFI';// Automatisch generiert. Nicht �ndern.
  cXPFP_PItem                         = '/LoepfeBody/Config/Group/FPattern/FP_P';// Automatisch generiert. Nicht �ndern.
  cXPFP_SpeedSimulationItem           = '/LoepfeBody/Config/Group/FPattern/FP_SpeedSimulation';// Automatisch generiert. Nicht �ndern.
  cXPFP_ZenitItem                     = '/LoepfeBody/Config/Group/FPattern/FP_Zenit';// Automatisch generiert. Nicht �ndern.
  cXPFP_FSpectraNormalItem            = '/LoepfeBody/Config/Group/FPattern/FP_FSpectraNormal';// Automatisch generiert. Nicht �ndern.
  cXPFP_FSpectraHighItem              = '/LoepfeBody/Config/Group/FPattern/FP_FSpectraHigh';// Automatisch generiert. Nicht �ndern.
  cXPFP_FSpectraBDItem                = '/LoepfeBody/Config/Group/FPattern/FP_FSpectraBD';// Automatisch generiert. Nicht �ndern.
  cXPFP_FZenitDarkItem                = '/LoepfeBody/Config/Group/FPattern/FP_FZenitDark';// Automatisch generiert. Nicht �ndern.
  cXPFP_FZenitBrightItem              = '/LoepfeBody/Config/Group/FPattern/FP_FZenitBright';// Automatisch generiert. Nicht �ndern.
  cXPFP_FClusterDarkItem              = '/LoepfeBody/Config/Group/FPattern/FP_FClusterDark';// Automatisch generiert. Nicht �ndern.
  cXPFP_FClusterBrightItem            = '/LoepfeBody/Config/Group/FPattern/FP_FClusterBright';// Automatisch generiert. Nicht �ndern.
  cXPFP_VCVItem                       = '/LoepfeBody/Config/Group/FPattern/FP_VCV';// Automatisch generiert. Nicht �ndern.
  cXPFP_SpliceClassClearingItem       = '/LoepfeBody/Config/Group/FPattern/FP_SpliceClassClearing';// Automatisch generiert. Nicht �ndern.
  cXPFP_ZenitCItem                    = '/LoepfeBody/Config/Group/FPattern/FP_ZenitC';// Automatisch generiert. Nicht �ndern.
  cXPFP_YarnCountRepetitionItem       = '/LoepfeBody/Config/Group/FPattern/FP_YarnCountRepetition';// Automatisch generiert. Nicht �ndern.
  cXPFP_ShortCountRepetitionItem      = '/LoepfeBody/Config/Group/FPattern/FP_ShortCountRepetition';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_DiameterItem             = '/LoepfeBody/Config/Group/FPattern/FP_Data_Diameter';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_SpliceItem               = '/LoepfeBody/Config/Group/FPattern/FP_Data_Splice';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_FItem                    = '/LoepfeBody/Config/Group/FPattern/FP_Data_F';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_SFIItem                  = '/LoepfeBody/Config/Group/FPattern/FP_Data_SFI';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_IPIItem                  = '/LoepfeBody/Config/Group/FPattern/FP_Data_IPI';// Automatisch generiert. Nicht �ndern.
  cXPFP_Data_ShiftItem                = '/LoepfeBody/Config/Group/FPattern/FP_Data_Shift';// Automatisch generiert. Nicht �ndern.
  cXPGroupConfigCodeNode              = '/LoepfeBody/Config/Group/ConfigCode';// Automatisch generiert. Nicht �ndern.
  cXPCutBeforeAdjustItem              = '/LoepfeBody/Config/Group/ConfigCode/CutBeforeAdjust';// Automatisch generiert. Nicht �ndern.
  cXPRemoveYarnAfterAdjustItem        = '/LoepfeBody/Config/Group/ConfigCode/RemoveYarnAfterAdjust';// Automatisch generiert. Nicht �ndern.
  cXPZeroTestItem                     = '/LoepfeBody/Config/Group/ConfigCode/ZeroTest';// Automatisch generiert. Nicht �ndern.
  cXPDriftCompensationItem            = '/LoepfeBody/Config/Group/ConfigCode/DriftCompensation';// Automatisch generiert. Nicht �ndern.
  cXPKnifeMonitorItem                 = '/LoepfeBody/Config/Group/ConfigCode/KnifeMonitor';// Automatisch generiert. Nicht �ndern.
  cXPSpeedSimulationItem              = '/LoepfeBody/Config/Group/ConfigCode/SpeedSimulation';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtAlarmItem                 = '/LoepfeBody/Config/Group/ConfigCode/BlockAtAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtCutFailureItem            = '/LoepfeBody/Config/Group/ConfigCode/BlockAtCutFailure';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtClusterAlarmItem          = '/LoepfeBody/Config/Group/ConfigCode/BlockAtClusterAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtYarnCountAlarmItem        = '/LoepfeBody/Config/Group/ConfigCode/BlockAtYarnCountAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockByUpExceptionItem           = '/LoepfeBody/Config/Group/ConfigCode/BlockByUpException';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtSFIAlarmItem              = '/LoepfeBody/Config/Group/ConfigCode/BlockAtSFIAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtFAlarmItem                = '/LoepfeBody/Config/Group/ConfigCode/BlockAtFAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtFClusterAlarmItem         = '/LoepfeBody/Config/Group/ConfigCode/BlockAtFClusterAlarm';// Automatisch generiert. Nicht �ndern.
  cXPBlockAtShortCountAlarmItem       = '/LoepfeBody/Config/Group/ConfigCode/BlockAtShortCountAlarm';// Automatisch generiert. Nicht �ndern.
  cXPSuckOffLastCutOnlyItem           = '/LoepfeBody/Config/Group/ConfigCode/SuckOffLastCutOnly';// Automatisch generiert. Nicht �ndern.
  cXPShortSFIDItem                    = '/LoepfeBody/Config/Group/ConfigCode/ShortSFID';// Automatisch generiert. Nicht �ndern.
  cXPSHZeroCutItem                    = '/LoepfeBody/Config/Group/ConfigCode/SHZeroCut';// Automatisch generiert. Nicht �ndern.
  cXPLongSpliceCheckLengthItem        = '/LoepfeBody/Config/Group/ConfigCode/LongSpliceCheckLength';// Automatisch generiert. Nicht �ndern.
  cXPFClassCoarseYarnItem             = '/LoepfeBody/Config/Group/ConfigCode/FClassCoarseYarn';// Automatisch generiert. Nicht �ndern.
  cXPTestmodeItem                     = '/LoepfeBody/Config/Group/ConfigCode/Testmode';// Automatisch generiert. Nicht �ndern.
  cXPFullSysInfoItem                  = '/LoepfeBody/Config/Group/ConfigCode/FullSysInfo';// Automatisch generiert. Nicht �ndern.
  cXPSHControllerSpeedAItem           = '/LoepfeBody/Config/Group/ConfigCode/SHControllerSpeedA';// Automatisch generiert. Nicht �ndern.
  cXPSHControllerSpeedBItem           = '/LoepfeBody/Config/Group/ConfigCode/SHControllerSpeedB';// Automatisch generiert. Nicht �ndern.
  cXPDFSInverseItem                   = '/LoepfeBody/Config/Group/ConfigCode/DFSInverse';// Automatisch generiert. Nicht �ndern.
  cXPNullCutSFSItem                   = '/LoepfeBody/Config/Group/ConfigCode/NullCutSFS';// Automatisch generiert. Nicht �ndern.
  cXPNullCutGeneralItem               = '/LoepfeBody/Config/Group/ConfigCode/NullCutGeneral';// Automatisch generiert. Nicht �ndern.
  cXPALZerolRefAdjMovementItem        = '/LoepfeBody/Config/Group/ConfigCode/ALZerolRefAdjMovement';// Automatisch generiert. Nicht �ndern.
  cXPALZeroRefSensCorrectionItem      = '/LoepfeBody/Config/Group/ConfigCode/ALZeroRefSensCorrection';// Automatisch generiert. Nicht �ndern.
  cXPALZeroRefFixDarkItem             = '/LoepfeBody/Config/Group/ConfigCode/ALZeroRefFixDark';// Automatisch generiert. Nicht �ndern.
  cXPLTGBreakBlockOnItem              = '/LoepfeBody/Config/Group/ConfigCode/LTGBreakBlockOn';// Automatisch generiert. Nicht �ndern.
  cXPFWSFSOutItem                     = '/LoepfeBody/Config/Group/ConfigCode/FWSFSOut';// Automatisch generiert. Nicht �ndern.
  cXPCutDelayAdditionalAItem          = '/LoepfeBody/Config/Group/ConfigCode/CutDelayAdditionalA';// Automatisch generiert. Nicht �ndern.
  cXPCutDelayAdditionalBItem          = '/LoepfeBody/Config/Group/ConfigCode/CutDelayAdditionalB';// Automatisch generiert. Nicht �ndern.
  cXPSFSImpulseblockCtrlItem          = '/LoepfeBody/Config/Group/ConfigCode/SFSImpulseblockCtrl';// Automatisch generiert. Nicht �ndern.
  cXPExtraNode                        = '/LoepfeBody/Config/Group/ConfigCode/Extra';// Automatisch generiert. Nicht �ndern.
  cXPA0Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A0';// Automatisch generiert. Nicht �ndern.
  cXPA1Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A1';// Automatisch generiert. Nicht �ndern.
  cXPA2Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A2';// Automatisch generiert. Nicht �ndern.
  cXPA3Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A3';// Automatisch generiert. Nicht �ndern.
  cXPA4Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A4';// Automatisch generiert. Nicht �ndern.
  cXPA5Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A5';// Automatisch generiert. Nicht �ndern.
  cXPA6Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A6';// Automatisch generiert. Nicht �ndern.
  cXPA7Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A7';// Automatisch generiert. Nicht �ndern.
  cXPA8Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A8';// Automatisch generiert. Nicht �ndern.
  cXPA9Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/A9';// Automatisch generiert. Nicht �ndern.
  cXPA10Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A10';// Automatisch generiert. Nicht �ndern.
  cXPA11Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A11';// Automatisch generiert. Nicht �ndern.
  cXPA12Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A12';// Automatisch generiert. Nicht �ndern.
  cXPA13Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A13';// Automatisch generiert. Nicht �ndern.
  cXPA14Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A14';// Automatisch generiert. Nicht �ndern.
  cXPA15Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/A15';// Automatisch generiert. Nicht �ndern.
  cXPB0Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B0';// Automatisch generiert. Nicht �ndern.
  cXPB1Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B1';// Automatisch generiert. Nicht �ndern.
  cXPB2Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B2';// Automatisch generiert. Nicht �ndern.
  cXPB3Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B3';// Automatisch generiert. Nicht �ndern.
  cXPB4Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B4';// Automatisch generiert. Nicht �ndern.
  cXPB5Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B5';// Automatisch generiert. Nicht �ndern.
  cXPB6Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B6';// Automatisch generiert. Nicht �ndern.
  cXPB7Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B7';// Automatisch generiert. Nicht �ndern.
  cXPB8Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B8';// Automatisch generiert. Nicht �ndern.
  cXPB9Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/B9';// Automatisch generiert. Nicht �ndern.
  cXPB10Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B10';// Automatisch generiert. Nicht �ndern.
  cXPB11Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B11';// Automatisch generiert. Nicht �ndern.
  cXPB12Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B12';// Automatisch generiert. Nicht �ndern.
  cXPB13Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B13';// Automatisch generiert. Nicht �ndern.
  cXPB14Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B14';// Automatisch generiert. Nicht �ndern.
  cXPB15Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/B15';// Automatisch generiert. Nicht �ndern.
  cXPC0Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C0';// Automatisch generiert. Nicht �ndern.
  cXPC1Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C1';// Automatisch generiert. Nicht �ndern.
  cXPC2Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C2';// Automatisch generiert. Nicht �ndern.
  cXPC3Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C3';// Automatisch generiert. Nicht �ndern.
  cXPC4Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C4';// Automatisch generiert. Nicht �ndern.
  cXPC5Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C5';// Automatisch generiert. Nicht �ndern.
  cXPC6Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C6';// Automatisch generiert. Nicht �ndern.
  cXPC7Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C7';// Automatisch generiert. Nicht �ndern.
  cXPC8Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C8';// Automatisch generiert. Nicht �ndern.
  cXPC9Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/C9';// Automatisch generiert. Nicht �ndern.
  cXPC10Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C10';// Automatisch generiert. Nicht �ndern.
  cXPC11Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C11';// Automatisch generiert. Nicht �ndern.
  cXPC12Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C12';// Automatisch generiert. Nicht �ndern.
  cXPC13Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C13';// Automatisch generiert. Nicht �ndern.
  cXPC14Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C14';// Automatisch generiert. Nicht �ndern.
  cXPC15Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/C15';// Automatisch generiert. Nicht �ndern.
  cXPD0Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D0';// Automatisch generiert. Nicht �ndern.
  cXPD1Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D1';// Automatisch generiert. Nicht �ndern.
  cXPD2Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D2';// Automatisch generiert. Nicht �ndern.
  cXPD3Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D3';// Automatisch generiert. Nicht �ndern.
  cXPD4Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D4';// Automatisch generiert. Nicht �ndern.
  cXPD5Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D5';// Automatisch generiert. Nicht �ndern.
  cXPD6Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D6';// Automatisch generiert. Nicht �ndern.
  cXPD7Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D7';// Automatisch generiert. Nicht �ndern.
  cXPD8Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D8';// Automatisch generiert. Nicht �ndern.
  cXPD9Item                           = '/LoepfeBody/Config/Group/ConfigCode/Extra/D9';// Automatisch generiert. Nicht �ndern.
  cXPD10Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D10';// Automatisch generiert. Nicht �ndern.
  cXPD11Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D11';// Automatisch generiert. Nicht �ndern.
  cXPD12Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D12';// Automatisch generiert. Nicht �ndern.
  cXPD13Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D13';// Automatisch generiert. Nicht �ndern.
  cXPD14Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D14';// Automatisch generiert. Nicht �ndern.
  cXPD15Item                          = '/LoepfeBody/Config/Group/ConfigCode/Extra/D15';// Automatisch generiert. Nicht �ndern.
  cXPValuesNode                       = '/LoepfeBody/Config/Group/ConfigCode/Values';// Automatisch generiert. Nicht �ndern.
  cXPConfigCodeAItem                  = '/LoepfeBody/Config/Group/ConfigCode/Values/ConfigCodeA';// Automatisch generiert. Nicht �ndern.
  cXPConfigCodeBItem                  = '/LoepfeBody/Config/Group/ConfigCode/Values/ConfigCodeB';// Automatisch generiert. Nicht �ndern.
  cXPConfigCodeCItem                  = '/LoepfeBody/Config/Group/ConfigCode/Values/ConfigCodeC';// Automatisch generiert. Nicht �ndern.
  cXPConfigCodeDItem                  = '/LoepfeBody/Config/Group/ConfigCode/Values/ConfigCodeD';// Automatisch generiert. Nicht �ndern.
  cXPDFSNode                          = '/LoepfeBody/Config/Group/DFS';// Automatisch generiert. Nicht �ndern.
  cXPGroupDFSSwitchItem               = '/LoepfeBody/Config/Group/DFS/Switch';// Automatisch generiert. Nicht �ndern.
  cXPDeltaItem                        = '/LoepfeBody/Config/Group/DFS/Delta';// Automatisch generiert. Nicht �ndern.
  cXPClassBlockIntervalItem           = '/LoepfeBody/Config/Group/ClassBlockInterval';// Automatisch generiert. Nicht �ndern.
  cXPDiameterControlIntervalItem      = '/LoepfeBody/Config/Group/DiameterControlInterval';// Automatisch generiert. Nicht �ndern.
  cXPDiameterControllIntervalStepItem = '/LoepfeBody/Config/Group/DiameterControllIntervalStep';// Automatisch generiert. Nicht �ndern.
  cXPMachineNode                      = '/LoepfeBody/Config/Machine';// Automatisch generiert. Nicht �ndern.
  cXPFrontTypeItem                    = '/LoepfeBody/Config/Machine/FrontType';// Automatisch generiert. Nicht �ndern.
  cXPYMSwVersionItem                  = '/LoepfeBody/Config/Machine/YMSwVersion';// Automatisch generiert. Nicht �ndern.
  cXPSWOptionItem                     = '/LoepfeBody/Config/Machine/SWOption';// Automatisch generiert. Nicht �ndern.
  cXPAWEMachTypeItem                  = '/LoepfeBody/Config/Machine/AWEMachType';// Automatisch generiert. Nicht �ndern.
  cXPYMTypeItem                       = '/LoepfeBody/Config/Machine/YMType';// Automatisch generiert. Nicht �ndern.
  cXPMachTypeItem                     = '/LoepfeBody/Config/Machine/MachType';// Automatisch generiert. Nicht �ndern.
  cXPNrOfSpindlesItem                 = '/LoepfeBody/Config/Machine/NrOfSpindles';// Automatisch generiert. Nicht �ndern.
  cXPNrOfSectionsItem                 = '/LoepfeBody/Config/Machine/NrOfSections';// Automatisch generiert. Nicht �ndern.
  cXPNrOfGroupsItem                   = '/LoepfeBody/Config/Machine/NrOfGroups';// Automatisch generiert. Nicht �ndern.
  cXPCheckLengthItem                  = '/LoepfeBody/Config/Machine/CheckLength';// Automatisch generiert. Nicht �ndern.
  cXPLongStopDefItem                  = '/LoepfeBody/Config/Machine/LongStopDef';// Automatisch generiert. Nicht �ndern.
  cXPMachNameItem                     = '/LoepfeBody/Config/Machine/MachName';// Automatisch generiert. Nicht �ndern.
  cXPLengthWindowItem                 = '/LoepfeBody/Config/Machine/LengthWindow';// Automatisch generiert. Nicht �ndern.
  cXPLengthModeItem                   = '/LoepfeBody/Config/Machine/LengthMode';// Automatisch generiert. Nicht �ndern.
  cXPIPICalculatedOnMachineItem       = '/LoepfeBody/Config/Machine/IPICalculatedOnMachine';// Automatisch generiert. Nicht �ndern.
  cXPMachineConfigCodeNode            = '/LoepfeBody/Config/Machine/ConfigCode';// Automatisch generiert. Nicht �ndern.
  cXPHeadstockRightItem               = '/LoepfeBody/Config/Machine/ConfigCode/HeadstockRight';// Automatisch generiert. Nicht �ndern.
  cXPUpperYarnCheckItem               = '/LoepfeBody/Config/Machine/ConfigCode/UpperYarnCheck';// Automatisch generiert. Nicht �ndern.
  cXPExtendedInterfaceMurataItem      = '/LoepfeBody/Config/Machine/ConfigCode/ExtendedInterfaceMurata';// Automatisch generiert. Nicht �ndern.
  cXPConeChangeConditionActiveItem    = '/LoepfeBody/Config/Machine/ConfigCode/ConeChangeConditionActive';// Automatisch generiert. Nicht �ndern.
  cXPKnifeCutOffPowerHighItem         = '/LoepfeBody/Config/Machine/ConfigCode/KnifeCutOffPowerHigh';// Automatisch generiert. Nicht �ndern.
  cXPNotMMNode                        = '/LoepfeBody/NotMM';// Automatisch generiert. Nicht �ndern.
  cXPRequest__Item                    = '/LoepfeBody/NotMM/Request__';// Automatisch generiert. Nicht �ndern.
  cXPReinTech__Item                   = '/LoepfeBody/NotMM/ReinTech__';// Automatisch generiert. Nicht �ndern.
  cXPFillNode                         = '/LoepfeBody/NotMM/Fill';// Automatisch generiert. Nicht �ndern.
  cXPFill0Item                        = '/LoepfeBody/NotMM/Fill/Fill0';// Automatisch generiert. Nicht �ndern.
  cXPFill1Item                        = '/LoepfeBody/NotMM/Fill/Fill1';// Automatisch generiert. Nicht �ndern.
  cXPFill2Item                        = '/LoepfeBody/NotMM/Fill/Fill2';// Automatisch generiert. Nicht �ndern.
  cXPFill3Item                        = '/LoepfeBody/NotMM/Fill/Fill3';// Automatisch generiert. Nicht �ndern.
  cXPFill4Item                        = '/LoepfeBody/NotMM/Fill/Fill4';// Automatisch generiert. Nicht �ndern.
  cXPFill5Item                        = '/LoepfeBody/NotMM/Fill/Fill5';// Automatisch generiert. Nicht �ndern.
  cXPFill6Item                        = '/LoepfeBody/NotMM/Fill/Fill6';// Automatisch generiert. Nicht �ndern.
  cXPFill7Item                        = '/LoepfeBody/NotMM/Fill/Fill7';// Automatisch generiert. Nicht �ndern.
  cXPFill8Item                        = '/LoepfeBody/NotMM/Fill/Fill8';// Automatisch generiert. Nicht �ndern.
  cXPFill9Item                        = '/LoepfeBody/NotMM/Fill/Fill9';// Automatisch generiert. Nicht �ndern.
  cXPDebugNode                        = '/LoepfeBody/Debug';// Automatisch generiert. Nicht �ndern.
  cXPinopSettingsNode                 = '/LoepfeBody/Debug/inopSettings';// Automatisch generiert. Nicht �ndern.
  cXPio_ClassClearingItem             = '/LoepfeBody/Debug/inopSettings/io_ClassClearing';// Automatisch generiert. Nicht �ndern.
  cXPio_SpliceCheckItem               = '/LoepfeBody/Debug/inopSettings/io_SpliceCheck';// Automatisch generiert. Nicht �ndern.
  cXPio_UpperYarnCheckItem            = '/LoepfeBody/Debug/inopSettings/io_UpperYarnCheck';// Automatisch generiert. Nicht �ndern.
  cXPio_SpeedItem                     = '/LoepfeBody/Debug/inopSettings/io_Speed';// Automatisch generiert. Nicht �ndern.
  cXPio_SpeedRampItem                 = '/LoepfeBody/Debug/inopSettings/io_SpeedRamp';// Automatisch generiert. Nicht �ndern.
  cXPio_CutRetriesItem                = '/LoepfeBody/Debug/inopSettings/io_CutRetries';// Automatisch generiert. Nicht �ndern.
  cXPio_HeadstockItem                 = '/LoepfeBody/Debug/inopSettings/io_Headstock';// Automatisch generiert. Nicht �ndern.
  cXPio_CutFailedLockItem             = '/LoepfeBody/Debug/inopSettings/io_CutFailedLock';// Automatisch generiert. Nicht �ndern.
  cXPio_ExtMurataItem                 = '/LoepfeBody/Debug/inopSettings/io_ExtMurata';// Automatisch generiert. Nicht �ndern.
  cXPio_OnePulsUmdrehungItem          = '/LoepfeBody/Debug/inopSettings/io_OnePulsUmdrehung';// Automatisch generiert. Nicht �ndern.
  cXPio_MMMachConfigItem              = '/LoepfeBody/Debug/inopSettings/io_MMMachConfig';// Automatisch generiert. Nicht �ndern.
  cXPio_FaultClusterItem              = '/LoepfeBody/Debug/inopSettings/io_FaultCluster';// Automatisch generiert. Nicht �ndern.
  cXPio_ClusterRepItem                = '/LoepfeBody/Debug/inopSettings/io_ClusterRep';// Automatisch generiert. Nicht �ndern.
  cXPio_ClusterLockItem               = '/LoepfeBody/Debug/inopSettings/io_ClusterLock';// Automatisch generiert. Nicht �ndern.
  cXPio_MinusDiaDiffItem              = '/LoepfeBody/Debug/inopSettings/io_MinusDiaDiff';// Automatisch generiert. Nicht �ndern.
  cXPio_CountLengthItem               = '/LoepfeBody/Debug/inopSettings/io_CountLength';// Automatisch generiert. Nicht �ndern.
  cXPio_OffCountRepItem               = '/LoepfeBody/Debug/inopSettings/io_OffCountRep';// Automatisch generiert. Nicht �ndern.
  cXPio_CountLockItem                 = '/LoepfeBody/Debug/inopSettings/io_CountLock';// Automatisch generiert. Nicht �ndern.
  cXPio_ShortCountItem                = '/LoepfeBody/Debug/inopSettings/io_ShortCount';// Automatisch generiert. Nicht �ndern.
  cXPio_LongThinClusterItem           = '/LoepfeBody/Debug/inopSettings/io_LongThinCluster';// Automatisch generiert. Nicht �ndern.
  cXPio_SfiItem                       = '/LoepfeBody/Debug/inopSettings/io_Sfi';// Automatisch generiert. Nicht �ndern.
  cXPio_SfiRepItem                    = '/LoepfeBody/Debug/inopSettings/io_SfiRep';// Automatisch generiert. Nicht �ndern.
  cXPio_SfiLockItem                   = '/LoepfeBody/Debug/inopSettings/io_SfiLock';// Automatisch generiert. Nicht �ndern.
  cXPio_ShortSfiItem                  = '/LoepfeBody/Debug/inopSettings/io_ShortSfi';// Automatisch generiert. Nicht �ndern.
  cXPio_FfMonitorItem                 = '/LoepfeBody/Debug/inopSettings/io_FfMonitor';// Automatisch generiert. Nicht �ndern.
  cXPio_FfClusterMonitorItem          = '/LoepfeBody/Debug/inopSettings/io_FfClusterMonitor';// Automatisch generiert. Nicht �ndern.
  cXPio_FfSensorActiveItem            = '/LoepfeBody/Debug/inopSettings/io_FfSensorActive';// Automatisch generiert. Nicht �ndern.
  cXPio_FfClSpliceCheckItem           = '/LoepfeBody/Debug/inopSettings/io_FfClSpliceCheck';// Automatisch generiert. Nicht �ndern.
  cXPio_FfAdjustOfflimitItem          = '/LoepfeBody/Debug/inopSettings/io_FfAdjustOfflimit';// Automatisch generiert. Nicht �ndern.
  cXPio_FfStartupRepItem              = '/LoepfeBody/Debug/inopSettings/io_FfStartupRep';// Automatisch generiert. Nicht �ndern.
  cXPio_FfAdjustAlarmItem             = '/LoepfeBody/Debug/inopSettings/io_FfAdjustAlarm';// Automatisch generiert. Nicht �ndern.
  cXPio_FfSensorTypItem               = '/LoepfeBody/Debug/inopSettings/io_FfSensorTyp';// Automatisch generiert. Nicht �ndern.
  cXPio_FfClusterRepItem              = '/LoepfeBody/Debug/inopSettings/io_FfClusterRep';// Automatisch generiert. Nicht �ndern.
  cXPio_FfClusterLockItem             = '/LoepfeBody/Debug/inopSettings/io_FfClusterLock';// Automatisch generiert. Nicht �ndern.
  cXPio_Inop1Item                     = '/LoepfeBody/Debug/inopSettings/io_Inop1';// Automatisch generiert. Nicht �ndern.
  cXPio_Inop2Item                     = '/LoepfeBody/Debug/inopSettings/io_Inop2';// Automatisch generiert. Nicht �ndern.

  // Zugriffsfunktionen f�r die oben definierten Enumerationen

  //Generiert ein FPattern Element (alle FPs auf false)
  function CreateDefaultFPattern: IXMLDOMElement;


  // Zugriffsmethode f�r den Typ TSwitch
  function GetSwitchValue(aLiteral: string): TSwitch;
  // Zugriffsmethode f�r den Typ TSensingHead
  function GetSensingHeadValue(aLiteral: string): TSensingHead;
  // Zugriffsmethode f�r den Typ TSWOption
  function GetSWOptionValue(aLiteral: string): TSWOption;
  // Zugriffsmethode f�r den Typ TWinderType
  function GetWinderTypeValue(aLiteral: string): TWinderType;
  // Zugriffsmethode f�r den Typ TYarnUnit
  function GetYarnUnitValue(aLiteral: string): TYarnUnit;
  // Zugriffsmethode f�r den Typ TLengthWindowMode
  function GetLengthWindowModeValue(aLiteral: string): TLengthWindowMode;
  // Zugriffsmethode f�r den Typ TAWEClass
  function GetAWEClassValue(aLiteral: string): TAWEClass;
  // Zugriffsmethode f�r den Typ TSensingHeadClass
  function GetSensingHeadClassValue(aLiteral: string): TSensingHeadClass;
  // Zugriffsmethode f�r den Typ TFrontType
  function GetFrontTypeValue(aLiteral: string): TFrontType;


  // Gibt den Namen eines Typs zur�ck
  // Zugriffsmethode f�r den Typ TSwitch
  function GetSwitchName(aValue: TSwitch): string;
  // Zugriffsmethode f�r den Typ TSensingHead
  function GetSensingHeadName(aValue: TSensingHead): string;
  // Zugriffsmethode f�r den Typ TSWOption
  function GetSWOptionName(aValue: TSWOption): string;
  // Zugriffsmethode f�r den Typ TWinderType
  function GetWinderTypeName(aValue: TWinderType): string;
  // Zugriffsmethode f�r den Typ TYarnUnit
  function GetYarnUnitName(aValue: TYarnUnit): string;
  // Zugriffsmethode f�r den Typ TLengthWindowMode
  function GetLengthWindowModeName(aValue: TLengthWindowMode): string;
  // Zugriffsmethode f�r den Typ TAWEClass
  function GetAWEClassName(aValue: TAWEClass): string;
  // Zugriffsmethode f�r den Typ TSensingHeadClass
  function GetSensingHeadClassName(aValue: TSensingHeadClass): string;
  // Zugriffsmethode f�r den Typ TFrontType
  function GetFrontTypeName(aValue: TFrontType): string;

implementation

uses
  typinfo, XMLGlobal;
(*-----------------------------------------------------------
  Generiert ein FPattern Element (alle FPs auf false)
-------------------------------------------------------------*) 
function CreateDefaultFPattern: IXMLDOMElement;
var
  xDOM: DOMDocumentMM;
  
  procedure AppendElement(aDOM: DOMDocumentMM; aParentElement: IXMLDOMElement; aElementName: string; aValue: Variant);
  var
    xElement: IXMLDOMElement;
  begin
    xElement := aDOM.CreateElement(aElementName);
    xElement.Text := aValue;
    aParentElement.AppendChild(xElement);
  end;// procedure AppendElement(...)
  
begin
  result := nil;
  
  xDOM := DOMDocumentMMCreate;
  if assigned(xDOM) then begin
    result := xDOM.CreateElement(cFPatternName);
    AppendElement(xDOM, result, cFP_ClassClearingName, false);
    AppendElement(xDOM, result, cFP_SpliceCheckName, false);
    AppendElement(xDOM, result, cFP_ShortCountName, false);
    AppendElement(xDOM, result, cFP_UpperYarnChannelName, false);
    AppendElement(xDOM, result, cFP_ClusterName, false);
    AppendElement(xDOM, result, cFP_ExtendedClusterName, false);
    AppendElement(xDOM, result, cFP_SFIName, false);
    AppendElement(xDOM, result, cFP_PName, false);
    AppendElement(xDOM, result, cFP_SpeedSimulationName, false);
    AppendElement(xDOM, result, cFP_ZenitName, false);
    AppendElement(xDOM, result, cFP_FSpectraNormalName, false);
    AppendElement(xDOM, result, cFP_FSpectraHighName, false);
    AppendElement(xDOM, result, cFP_FSpectraBDName, false);
    AppendElement(xDOM, result, cFP_FZenitDarkName, false);
    AppendElement(xDOM, result, cFP_FZenitBrightName, false);
    AppendElement(xDOM, result, cFP_FClusterDarkName, false);
    AppendElement(xDOM, result, cFP_FClusterBrightName, false);
    AppendElement(xDOM, result, cFP_VCVName, false);
    AppendElement(xDOM, result, cFP_SpliceClassClearingName, false);
    AppendElement(xDOM, result, cFP_ZenitCName, false);
    AppendElement(xDOM, result, cFP_YarnCountRepetitionName, false);
    AppendElement(xDOM, result, cFP_ShortCountRepetitionName, false);
    AppendElement(xDOM, result, cFP_Data_DiameterName, false);
    AppendElement(xDOM, result, cFP_Data_SpliceName, false);
    AppendElement(xDOM, result, cFP_Data_FName, false);
    AppendElement(xDOM, result, cFP_Data_SFIName, false);
    AppendElement(xDOM, result, cFP_Data_IPIName, false);
    AppendElement(xDOM, result, cFP_Data_ShiftName, false);
  end;// if assigned(xDOM) then begin
end;// CreateDefaultFPattern

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSwitch
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetSwitchValue(aLiteral: string): TSwitch;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TSwitch), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TSwitch))) then
    result := Low(TSwitch)
  else
    result := TSwitch(xEnumValue);
end;// function GetSwitchValue(aLiteral: string): TSwitch;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSensingHead
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetSensingHeadValue(aLiteral: string): TSensingHead;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TSensingHead), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TSensingHead))) then
    result := Low(TSensingHead)
  else
    result := TSensingHead(xEnumValue);
end;// function GetSensingHeadValue(aLiteral: string): TSensingHead;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSWOption
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetSWOptionValue(aLiteral: string): TSWOption;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TSWOption), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TSWOption))) then
    result := Low(TSWOption)
  else
    result := TSWOption(xEnumValue);
end;// function GetSWOptionValue(aLiteral: string): TSWOption;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TWinderType
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetWinderTypeValue(aLiteral: string): TWinderType;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TWinderType), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TWinderType))) then
    result := Low(TWinderType)
  else
    result := TWinderType(xEnumValue);
end;// function GetWinderTypeValue(aLiteral: string): TWinderType;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TYarnUnit
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetYarnUnitValue(aLiteral: string): TYarnUnit;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TYarnUnit), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TYarnUnit))) then
    result := Low(TYarnUnit)
  else
    result := TYarnUnit(xEnumValue);
end;// function GetYarnUnitValue(aLiteral: string): TYarnUnit;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TLengthWindowMode
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetLengthWindowModeValue(aLiteral: string): TLengthWindowMode;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TLengthWindowMode), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TLengthWindowMode))) then
    result := Low(TLengthWindowMode)
  else
    result := TLengthWindowMode(xEnumValue);
end;// function GetLengthWindowModeValue(aLiteral: string): TLengthWindowMode;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TAWEClass
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetAWEClassValue(aLiteral: string): TAWEClass;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TAWEClass), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TAWEClass))) then
    result := Low(TAWEClass)
  else
    result := TAWEClass(xEnumValue);
end;// function GetAWEClassValue(aLiteral: string): TAWEClass;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSensingHeadClass
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetSensingHeadClassValue(aLiteral: string): TSensingHeadClass;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TSensingHeadClass), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TSensingHeadClass))) then
    result := Low(TSensingHeadClass)
  else
    result := TSensingHeadClass(xEnumValue);
end;// function GetSensingHeadClassValue(aLiteral: string): TSensingHeadClass;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TFrontType
    Aufruf f�r TSwitch:
    GetSwitchValue('swOn')
-------------------------------------------------------------*) 
function GetFrontTypeValue(aLiteral: string): TFrontType;
var
  xEnumValue: integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TFrontType), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TFrontType))) then
    result := Low(TFrontType)
  else
    result := TFrontType(xEnumValue);
end;// function GetFrontTypeValue(aLiteral: string): TFrontType;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSwitch
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetSwitchName(aValue: TSwitch): string;
begin
  result := GetEnumName(TypeInfo(TSwitch), ord(aValue));
end;// function GetSwitchName(aValue: TSwitch): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSensingHead
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetSensingHeadName(aValue: TSensingHead): string;
begin
  result := GetEnumName(TypeInfo(TSensingHead), ord(aValue));
end;// function GetSensingHeadName(aValue: TSensingHead): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSWOption
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetSWOptionName(aValue: TSWOption): string;
begin
  result := GetEnumName(TypeInfo(TSWOption), ord(aValue));
end;// function GetSWOptionName(aValue: TSWOption): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TWinderType
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetWinderTypeName(aValue: TWinderType): string;
begin
  result := GetEnumName(TypeInfo(TWinderType), ord(aValue));
end;// function GetWinderTypeName(aValue: TWinderType): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TYarnUnit
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetYarnUnitName(aValue: TYarnUnit): string;
begin
  result := GetEnumName(TypeInfo(TYarnUnit), ord(aValue));
end;// function GetYarnUnitName(aValue: TYarnUnit): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TLengthWindowMode
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetLengthWindowModeName(aValue: TLengthWindowMode): string;
begin
  result := GetEnumName(TypeInfo(TLengthWindowMode), ord(aValue));
end;// function GetLengthWindowModeName(aValue: TLengthWindowMode): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TAWEClass
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetAWEClassName(aValue: TAWEClass): string;
begin
  result := GetEnumName(TypeInfo(TAWEClass), ord(aValue));
end;// function GetAWEClassName(aValue: TAWEClass): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TSensingHeadClass
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetSensingHeadClassName(aValue: TSensingHeadClass): string;
begin
  result := GetEnumName(TypeInfo(TSensingHeadClass), ord(aValue));
end;// function GetSensingHeadClassName(aValue: TSensingHeadClass): string;

(*-----------------------------------------------------------
  Zugriffsmethode f�r den Typ TFrontType
    Aufruf f�r TSwitch:
    GetSwitchName(swOn)
-------------------------------------------------------------*) 
function GetFrontTypeName(aValue: TFrontType): string;
begin
  result := GetEnumName(TypeInfo(TFrontType), ord(aValue));
end;// function GetFrontTypeName(aValue: TFrontType): string;



end.

