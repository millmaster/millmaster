(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_shift.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Schichtkalender bearbeiten
| Info..........: -
| Develop.system: Windows NT 4.0 SP3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 20.09.99 | 0.03 | PW  | TmmToolbar, less buttons, dropdwon menues
| 24.09.99 | 0.04 | PW  | rename c_shift_dauer => c_shift_length
| 14.03.00 | 1.00 | Wss | EnableActions changed to work with MMSecurityControl
| 20.04.00 | 1.00 | Wss | Ctrl+S added to acSave
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.02 |      | LOK | - Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
|          |      |     | - Beim Editieren des Schichtmusters wird das Default-Schichtmuster angew�hlt
|          |      |     | - Beim �bernehmen des Schichtmusters wird das Default-Schichtmuster angew�hlt
| 27.02.04 | 1.00 | Wss | In SaveQuery_OK wird per THackGrid der EditorMode ausgeschalten, um ein
                          unbeabsichtigtes schreiben einer editierten Zeit in der Vergangenheit zu verhindern
| 18.06.04 | 1.00 | SDo | Neue Kopf- & Fusszeile im Print-Layout hinzugefuegt -> Func. PrintHeaderFooter()
|                       | Aenderungen am Grid-Ausdruck, Col-Width neu setzen Func. PrintShiftCalendar()
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_shift;
interface
uses
  Windows, Messages, CommCtrl, SysUtils, Classes, Graphics, Controls, Forms, BaseForm, Dialogs, StdCtrls,
  Nwcombo, ComCtrls, ToolWin, Grids, DBGrids, Db, ExtCtrls, Menus, ShiftCalendars,
  ActnList, 
  IvDictio, IvMulti, IvEMulti, ImgList, mmTimer,
  mmActionList, mmTranslator, mmPopupMenu, mmTabControl, mmPanel, mmToolBar,
  Buttons, mmSpeedButton, MMSecurity, u_dmShiftCalendar, mmADODataSet, mmADOCommand, LoepfeGlobal,
  AdoDBAccess;

type
  TEditShiftDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bEditShiftCalendar: TToolButton;
    bPrint: TToolButton;
    ToolButton8: TToolButton;
    bViewDefaulShift: TToolButton;
    bViewShiftPattern: TToolButton;
    ToolButton10: TToolButton;
    cobShiftCal: TNWComboBox;
    ToolButton14: TToolButton;
    bHelp: TToolButton;
    bEditTeam: TToolButton;
    ToolButton4: TToolButton;
    bOptions: TToolButton;
    ToolButton3: TToolButton;
    bSaveChanges: TToolButton;
    ToolButton5: TToolButton;

    MainPanel: TmmPanel;
    kMonthTab: TmmTabControl;
    Panel1: TmmPanel;
    kspGrid: TShiftCalendarGrid;

    mmActionList: TmmActionList;
    acClose: TAction;
    acEditTeam: TAction;
    acEditShiftCalendar: TAction;
    acEditShiftPattern: TAction;
    acEditDefaultShifts: TAction;
    acEditOptions: TAction;
    acPrintPage: TAction;
    acPrintRange: TAction;
    acSaveChanges: TAction;
    acCopyDefaultShift: TAction;
    acCopyShiftPattern: TAction;
    acHelp: TAction;
    acPrint: TAction;
    acSecurity: TAction;

    RefreshTimer: TmmTimer;

    pmPrint: TmmPopupMenu;
    miPrintPage: TMenuItem;
    miPrintRange: TMenuItem;

    pmShiftPattern: TmmPopupMenu;
    miEditShiftPattern: TMenuItem;
    miCopyShiftPattern: TMenuItem;

    pmDefaultShift: TmmPopupMenu;
    miEditDefaultShifts: TMenuItem;
    miCopyDefaultShift: TMenuItem;
    MMSecurityControl: TMMSecurityControl;
    mmSpeedButton1: TmmSpeedButton;
    mmTranslator: TmmTranslator;

    procedure FormCreate(Sender: TObject);
    procedure kMonthTabChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cobShiftCalChange(Sender: TObject);
    procedure RefreshTimerTimer(Sender: TObject);
    procedure kMonthTabChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acCloseExecute(Sender: TObject);
    procedure acEditTeamExecute(Sender: TObject);
    procedure acEditShiftCalendarExecute(Sender: TObject);
    procedure acEditShiftPatternExecute(Sender: TObject);
    procedure acEditDefaultShiftsExecute(Sender: TObject);
    procedure acEditOptionsExecute(Sender: TObject);
    procedure acPrintPageExecute(Sender: TObject);
    procedure acShowWeekendExecute(Sender: TObject);
    procedure acShowTeamExecute(Sender: TObject);
    procedure acSaveChangesExecute(Sender: TObject);
    procedure acCopyDefaultShiftExecute(Sender: TObject);
    procedure acCopyShiftPatternExecute(Sender: TObject);
    procedure acPrintRangeExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure UpdateFormCaption;
    procedure UpdateTabs;
    procedure InitTabs;
    function GetMaxCopyDate: tdatetime;
    function GetCurrentTabID: longint;
    function SelectedYear: word;
    function SelectedMonth: word;
    procedure Fill_cobShiftCal;
    function WriteDataToDB: boolean;
    function SaveQuery_OK: boolean;
    procedure InitShiftCalendar(aShiftGrid: TShiftCalendarGrid);
    procedure PrintShiftCalendar;
    procedure EnableActions;
    procedure TextToTranslate;
  public
    procedure Init;
    procedure TranslateText;            //override;
  end;                                  //TEditShiftDlg

var
  EditShiftDlg: TEditShiftDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs, Printers,
  u_common_lib, u_common_global, u_main, u_lib, u_global, u_edit_shiftcalendar, u_edit_defaultshift,
  u_edit_team, u_edit_exceptdays, u_edit_options, u_edit_shiftpattern, u_progress, u_copy_shiftpattern,
  u_copy_defaultshift, u_printgrid, ADODB_TLB;

{$R *.DFM}

resourcestring
  cCaptionStr1 = '(50)Schichtkalender:  [%s - %s %d]'; //ivlm
  cCaptionStr2 = '(60)Standardschichten fuer %s uebernehmen'; //ivlm
  cCaptionStr3 = '(60)Schichtmuster fuer %s uebernehmen'; //ivlm
  cProgressStr1 = '(60)Standardschichten werden uebernommen...'; //ivlm
  cProgressStr2 = '(60)Schichtmuster wird uebernommen...'; //ivlm
  cPrintTitleStr = '(30)Schichtkalender:  [%s]'; //ivlm
  cMsgStr1 = '(255)Der Schichtplan wurde geaendert. Moechten Sie Ihre Eingaben sichern ?'; //ivlm
  cMsgStr2 = '(255)Moechten Sie die Standardschichten fuer den gewaehlten Zeitraum jetzt uebernehmen ?'; //ivlm
  cMsgStr3a = '(255)Die Schichtdauer der Schicht %d vom %s wird durch das Einfuegen der Standardschichten zu gering / zu gross.'; //ivlm
  cMsgStr3b = '(255)Aendern Sie den Beginn dieser Schicht und wiederholen Sie die Aktion nochmals.'; //ivlm
  cMsgStr4 = '(255)Moechten Sie das Schichtmuster fuer den gewaehlten Zeitraum jetzt uebernehmen ?'; //ivlm


//-----------------------------------------------------------------------------
procedure TEditShiftDlg.InitShiftCalendar(aShiftGrid: TShiftCalendarGrid);
begin
  with aShiftGrid.CalendarColors, gProgOptions do begin
    DayTextColor := ShiftCalendarColors[ciDay, cpText];
    DayColor := ShiftCalendarColors[ciDay, cpBackground];
    OffDayTextColor := ShiftCalendarColors[ciOffDay, cpText];
    OffDayColor := ShiftCalendarColors[ciOffDay, cpBackground];
    MarkTextColor := ShiftCalendarColors[ciMark, cpText];
    MarkColor := ShiftCalendarColors[ciMark, cpBackground];
    TimeTextColor := ShiftCalendarColors[ciTime, cpText];
    TimeColor := ShiftCalendarColors[ciTime, cpBackground];
    TitleTextColor := ShiftCalendarColors[ciTitle, cpText];
    TitleColor := ShiftCalendarColors[ciTitle, cpBackground];
    WeekEndTextColor := ShiftCalendarColors[ciWeekEnd, cpText];
    WeekEndColor := ShiftCalendarColors[ciWeekEnd, cpBackground];
  end;                                  //with aShiftGrid.CalendarColors, gProgOptions do

  with aShiftGrid do begin
    MaxShiftsPerDay := gProgOptions.ShiftsPerDay;
    if gProgOptions.ShowMarks then MarkFields := [mfTimeDifferent]
    else MarkFields := [];
    MarkChar := gProgOptions.MarkChar[1];
    WeekEndDays := GetWeekEndDays(gProgOptions.WeekEndDays);
    ShowTeamColors := gProgOptions.ShowTeamColors;
    ShowWeekEndDays := gProgOptions.ShowShiftWeekEnds;
  end;                                  //with aShiftGrid do
end;                                    //procedure TEditShiftDlg.InitShiftCalendar
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.Init;
begin
  InitShiftCalendar(kspGrid);
end;                                    //procedure TEditShiftDlg.Init
//-----------------------------------------------------------------------------
function TEditShiftDlg.GetMaxCopyDate: tdatetime;
var
  y, m, d: word;

begin
  decodedate(AddMonth(date, cMaxShiftMonths), y, m, d);
  result := EncodeDate(y, m, DaysPerMonth(y, m));
end;                                    //function TEditShiftDlg.GetMaxCopyDate
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.UpdateFormCaption;
begin
  Caption := format({LTrans{}(cCaptionStr1), [cobShiftCal.GetItemText, GetLongMonthNames(SelectedMonth), SelectedYear]);
end;                                    //TEditShiftDlg.UpdateFormCaption
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.UpdateTabs;
var
  i: integer;
  xMonth,
    xYear: word;
  xYearMonth: longint;

begin
  for i := 0 to kMonthTab.Tabs.Count - 1 do begin
    xYearMonth := Longint(kMonthTab.Tabs.Objects[i]);
    xMonth := xYearMonth mod 100;
    xYear := xYearMonth div 100;
    kMonthTab.Tabs[i] := GetShortMonthNames(xMonth) + ' ' + lz(xYear mod 100, 2);
  end;                                  //for i := 0 to kMonthTab.Tabs.Count -1 do
end;                                    //procedure TEditShiftDlg.UpdateTabs
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.InitTabs;
var
  i,
    d, m, y: word;
  xMonth: integer;

begin
  kMonthTab.Tabs.Clear;
  for xMonth := -cMaxShiftMonths to cMaxShiftMonths do begin
    decodedate(AddMonth(date, xMonth), y, m, d);
    kMonthTab.Tabs.AddObject('', TObject(y * 100 + m));
  end;                                  //for xMonth := -cMaxShiftMonths to cMaxShiftMonths do

  UpdateTabs;

  i := kMonthTab.Tabs.IndexOfObject(TObject(GetCurrentYear * 100 + GetCurrentMonth));
  kMonthTab.TabIndex := i;
end;                                    //procedure TEditShiftDlg.InitTabs
//-----------------------------------------------------------------------------
function TEditShiftDlg.GetCurrentTabID: longint;
begin
  with kMonthTab do
    result := Longint(Tabs.Objects[TabIndex]);
end;                                    //function TEditShiftDlg.GetCurrentTabID
//-----------------------------------------------------------------------------
function TEditShiftDlg.SelectedYear: word;
begin
  result := GetCurrentTabID div 100;
end;                                    //function TEditShiftDlg.SelectedYear
//-----------------------------------------------------------------------------
function TEditShiftDlg.SelectedMonth: word;
begin
  result := GetCurrentTabID mod 100;
end;                                    //function TEditShiftDlg.SelectedMonth
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.kMonthTabChange(Sender: TObject);
begin
  kspGrid.Month := SelectedMonth;
  kspGrid.Year := SelectedYear;
  kspGrid.CalendarID := cobShiftCal.GetItemID;
  UpdateFormCaption;

  kspGrid.ReadShiftData;
end;                                    //procedure TEditShiftDlg.kMonthTabChange
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.kMonthTabChanging(Sender: TObject; var AllowChange: Boolean);
begin
  AllowChange := SaveQuery_OK;
end;                                    //procedure TEditShiftDlg.kMonthTabChanging
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.FormCreate(Sender: TObject);
begin
  HelpContext                    := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender.htm');
  acEditShiftPattern.HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Neues-Schichtmuster.htm');
  TextToTranslate;

  RefreshTimer.Enabled := false;

  with kspGrid do begin
    DatabaseConnection := dmShiftCalendar.conDefault;
    MaxShiftsPerDay := cMaxShiftsPerDay;
    ReadTeamColors;
  end;                                  //with kspGrid do

  Init;

  Fill_cobShiftCal;
  cobShiftCal.ItemIndex := 0;
  cobShiftCal.OnChange := cobShiftCalChange;
  kspGrid.CalendarID := cobShiftCal.GetItemID;

  InitTabs;
  cobShiftCalChange(Sender);
  kspGrid.CalcFirstEditCell;
 // HelpContext := hcEditShift;

{$IFDEF EnableRefreshTimer}
  RefreshTimer.Interval := cRefreshInterval * 1000; //convert to ms
  RefreshTimer.Enabled := true;
{$ENDIF}

  //EnableActions => in cobShiftCalChange
end;                                    //procedure TEditShiftDlg.FormCreate
//------------------------------------------------------------------------------
procedure TEditShiftDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.FormShow(Sender: TObject);
begin
  Screen.Cursor := crDefault;
end;                                    //procedure TEditShiftDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.Fill_cobShiftCal;
begin
  cobShiftCal.Items.Clear;
  with TmmAdoDataSet.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT c_shiftcal_id, c_shiftcal_name FROM t_shiftcal';
    Open;
    while not Eof do begin
      cobShiftCal.AddObj(FieldByName('c_shiftcal_name').AsString, FieldByName('c_shiftcal_id').AsInteger);
      Next;
    end;                              //while not Eof do
  finally
    free;
  end;// with TmmAdoDataSet.Create(nil)
end;                                    //procedure TEditExceptDaysDlg.Fill_cobShiftCal
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.cobShiftCalChange(Sender: TObject);
begin
  if not SaveQuery_OK then begin
    cobShiftCal.OnChange := nil;
    try
      cobShiftCal.SetItemID(kspGrid.CalendarID);
    finally
      cobShiftCal.OnChange := cobShiftCalChange;
    end;                                //try..finally
    Exit;
  end;                                  //if not SaveQuery_OK then

  kMonthTabChange(nil);
  EnableActions;
end;                                    //procedure TEditShiftDlg.cobShiftCalChange
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.RefreshTimerTimer(Sender: TObject);
begin
  kspGrid.ReadTeamColors;
  kspGrid.ReadDefaultShifts;
  kspGrid.ReadOffDays;
  kspGrid.Refresh;
end;                                    //procedure TEditShiftDlg.RefreshTimerTimer
//-----------------------------------------------------------------------------
function TEditShiftDlg.WriteDataToDB: boolean;
begin
  result := false;
  dmShiftCalendar.conDefault.BeginTrans;
  try
    result := kspGrid.WriteShiftData;
    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e:Exception do begin
      dmShiftCalendar.conDefault.RollbackTrans;
      WriteDebugLog('[TEditShiftDlg.WriteDataToDB]', e.Message);
    end;// on e:Exception do begin
  end;                                  //try..except
end;                                    //function TEditShiftDlg.WriteDataToDB
//-----------------------------------------------------------------------------
type
  THackGrid = class(TmmCustomCalendar);
  
function TEditShiftDlg.SaveQuery_OK: boolean;
  //..........................................................
  function SaveQuery: TModalResult;
  begin
    result := MMMessageDlg(cMsgStr1, mtConfirmation, [mbYes, mbNo, mbCancel], 0, Self);
  end;                                  //function SaveQuery
  //..........................................................
begin                                   //main
  Result := true;
  if kspGrid.IsDifferent then begin
    case SaveQuery of
      mrYes: begin
          THackGrid(kspGrid).EditorMode := False;
          Result := WriteDataToDB;
        end;
      mrCancel: result := false;
      mrNo: THackGrid(kspGrid).EditorMode := False;
    end;                                //case SaveQuery of
  end;                                  //if kspGrid.IsDifferent  then
end;                                    //function TEditShiftDlg.SaveQuery_OK
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := SaveQuery_OK;
end;                                    //procedure TEditShiftDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end;                                    //procedure TEditShiftDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acEditTeamExecute(Sender: TObject);
begin
// with TEditTeamDlg.Create(Application) do
  with TEditTeamDlg.Create(Self) do
    ShowModal;
  EnableActions;
end;                                    //procedure TEditShiftDlg.ActionEditTeamExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acEditShiftCalendarExecute(Sender: TObject);
var
  xID: longint;

begin
  if not SaveQuery_OK then exit;

// with TEditShiftCalendarDlg.Create(Application) do
  with TEditShiftCalendarDlg.Create(Self) do
    ShowModal;

  kspGrid.ReadDefaultShifts;
  kspGrid.Refresh;
  cobShiftCal.OnChange := nil;
  try
    xID := cobShiftCal.GetItemID;
    Fill_cobShiftCal;
    if cobShiftCal.GetItemIDPos(xID) > -1 then cobShiftCal.SetItemID(xID) //record deleted ??
    else cobShiftCal.ItemIndex := 0;
  finally
    kMonthTabChange(Sender);
    cobShiftCal.OnChange := cobShiftCalChange;
    UpdateFormCaption;
    EnableActions;
  end;                                  //try..finally
end;                                    //procedure TEditShiftDlg.ActionEditShiftCalendarExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acEditShiftPatternExecute(Sender: TObject);
begin
// with TEditShiftPatternDlg.Create(Application) do
  with TEditShiftPatternDlg.Create(Self) do begin
    SetPatternIDFromShiftID(cobShiftCal.GetItemID);
    ShowModal;
  end;// with TEditShiftPatternDlg.Create(Self) do begin
  EnableActions;
end;                                    //procedure TEditShiftDlg.ActionEditShiftPatternExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acEditDefaultShiftsExecute(Sender: TObject);
var
  i: integer;

begin
  i := cobShiftCal.GetItemID;
// with TEditDefaultShiftDlg.Create(Application) do
  with TEditDefaultShiftDlg.Create(Self) do begin
    SetKalenderID(i);
    ShowModal;
  end;                                  //with TEditDefaultShiftDlg.Create(Application) do
  EnableActions;
end;                                    //procedure TEditShiftDlg.ActionEditDefaultShiftsExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acEditOptionsExecute(Sender: TObject);
begin
// with TEditOptionsDlg.Create(Application) do
  with TEditOptionsDlg.Create(Self) do begin
    SetVisibleTabs([0, 2]);
    ShowModal;
  end;                                  //with TEditOptionsDlg.Create(Application) do
  Init;
end;                                    //procedure TEditShiftDlg.ActionEditOptionsExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.PrintShiftCalendar;
var
  xCol,
  xRow: integer;
  xPG: TGridPrint;
  cInfo: rCellInfoT;
  xShiftGrid: TShiftCalendarGrid;
  xNumber: integer;
  i: longint;
  xShiftsPerDay : Integer;
begin
  Update;

  xShiftGrid := TShiftCalendarGrid.Create(Self);
  xPG := TGridPrint.Create;
  try
    with xShiftGrid do begin
      InitShiftCalendar(xShiftGrid);

   //DatabaseName := gDataBaseName;
      DatabaseConnection := dmShiftCalendar.conDefault;
      MaxShiftsPerDay := gProgOptions.ShiftsPerDay;
      ReadTeamColors;
      ShowTeamColors := kspGrid.ShowTeamColors;
      ShowWeekEndDays := kspGrid.ShowWeekEndDays;
    end;                                //with xShiftGrid do


    xPG.InitPrinterInfo;

    xPG.Rows := 17;
    xPG.LeftMM := 10;
    xPG.LeftMM := 0;

{$IFDEF PrintMaxShifts}
    xPG.Cols := 15;
    xPG.Cols := 5 + (2 * gProgOptions.ShiftsPerDay);  //SDO
    xShiftsPerDay := gProgOptions.ShiftsPerDay;

{
    for xCol := 1 to xPG.Cols do
      case xCol of
        1, 9: xPG.SetColWidth(xCol, 13);
        2..6, 10..14: xPG.SetColWidth(xCol, 14);
        7, 15: xPG.SetColWidth(xCol, 4);
        8: xPG.SetColWidth(xCol, 7);
      end;
}

    //Breite der Col's setzen
    for xCol := 1 to xPG.Cols do begin    //SDO
       //Datum & Schichten Col
       if (xCol <= xShiftsPerDay + 1) or
          (xCol > xShiftsPerDay + 3 )and  (xCol < xPG.Cols ) then xPG.SetColWidth(xCol, 14)
       else
          //x & Leer Col
          xPG.SetColWidth(xCol, 4);


   end;
                                 //case i of
{$ELSE}
    xPG.Cols := 5 + (2 * gProgOptions.ShiftsPerDay);
    for xCol := 1 to xPG.Cols do begin
      if (xCol = 1) or (xCol = (4 + gProgOptions.ShiftsPerDay)) then xPG.SetColWidth(xCol, 13);
      if (xCol > 1) and (xCol <= (gProgOptions.ShiftsPerDay + 1)) then xPG.SetColWidth(xCol, 14);
      if (xCol >= (5 + gProgOptions.ShiftsPerDay)) and (xCol <= (4 + 2 * gProgOptions.ShiftsPerDay)) then xPG.SetColWidth(xCol, 14);
      if (xCol = (2 + gProgOptions.ShiftsPerDay)) or (xCol = (5 + 2 * gProgOptions.ShiftsPerDay)) then xPG.SetColWidth(xCol, 4);
      if xCol = (3 + gProgOptions.ShiftsPerDay) then xPG.SetColWidth(xCol, 7);
    end;                                //for xCol := 1 to xPG.Cols do
{$ENDIF}

    for xRow := 1 to xPG.Rows do
      xPG.SetRowHeight(xRow, 6);

    xNumber := 0;

    Printer.BeginDoc;
    for i := gPrintOptions.FromYearMonth to gPrintOptions.ToYearMonth do begin
      inc(xNumber);
      xShiftGrid.Month := GetMonthFromYM(i);
      xShiftGrid.Year := GetYearFromYM(i);
      xShiftGrid.CalendarID := cobShiftCal.GetItemID;
      xShiftGrid.ReadShiftData;

      if odd(xNumber) then begin
        if xNumber > 1 then Printer.NewPage;
        xPG.TopMM := 35;
        xPG.MoveTo(xPG.LeftMM, 20);
        xPG.LineTo(xPG.PInfo.xMM - xPG.LeftMM, 20);
        (*
        xPG.SetFont(clBlack, [fsBold], 90);
        xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taCenter, taBottom, format({LTrans{}(cPrintTitleStr), [cobShiftCal.GetItemText]));
        xPG.SetFont(clBlack, [], 70);
//Wss     xPG.AlignTextOut(xPG.LeftMM,0,xPG.PInfo.xMM -xPG.LeftMM,19,taRight,taBottom,formatdatetime_('dd/mm/yyyy  hh:nn',now));
        xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taRight, taBottom, formatdatetime(ShortDateFormat + '  hh:nn', now));
        //xPG.SetFont(clBlack, [fsBold], 120);
        //xPG.TextOut(xPG.LeftMM, xPG.PInfo.yMM - 15, 'LOEPFE MILLMASTER');
        *)
      end                               //if odd(xNumber) then
      else xPG.TopMM := 155;

      xPG.SetFont(clBlack, [fsUnderline], 90);
      xPG.AlignTextOut(xPG.LeftMM, xPG.TopMM - 15, xPG.GetGridWidth + xPG.LeftMM, xPG.TopMM - 3,
        taCenter, taBottom, format('%s %d', [GetLongMonthNames(xShiftGrid.Month), xShiftGrid.Year]));

    //xPG.AlignTextOut(xPG.LeftMM,xPG.TopMM -15,xPG.PInfo.xMM -xPG.LeftMM,xPG.TopMM -3,taCenter,taBottom,format('%s %d',[GetLongMonthNames(xShiftGrid.Month),xShiftGrid.Year]));

    for xCol := 1 to xPG.Cols do
        for xRow := 1 to xPG.Rows do begin
          cInfo := xShiftGrid.GetCellInfoAt(xCol - 1, xRow - 1);
          if not gPrintOptions.PrintColors then begin
            cInfo.fColor := clBlack;
            cInfo.bColor := clWhite;
            if (xCol = 1) or (xRow = 1) then cInfo.bColor := cLightSilver;
          end;                          //if not gPrintOptions.PrintColors then
          xPG.SetFont(cInfo.fColor, cInfo.fStyle, 70);
          xPG.SetBrush(cInfo.bColor, bsSolid);
          xPG.FillCellText(xCol, xRow, taCenter, taCenter, cInfo.cString, true);
        end;                            //for xRow := 1 to xPG.Rows do
    end;


    PrintHeaderFooter( Printer.Canvas ,Format( cPrintTitleStr, [cobShiftCal.GetItemText]) );
                               //for i := gPrintOptions.FromYearMonth to gPrintOptions.ToYearMonth do
    Printer.EndDoc;
  finally
    xPG.Free;
    xShiftGrid.Free;
  end;                                  //try..finally
end;                                    //procedure TEditShiftDlg.PrintShiftCalendar
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acPrintPageExecute(Sender: TObject);
//var xClearerSettingsRec : TClearerSettingsRec;
begin
  if not WriteDataToDB then exit;
//  if not GetPrinterOptions(poPortrait, [poPrintColors], Self) then exit;
  if not GetPrintOptions(poPortrait, Self, gPrintOptions.PrintColors) then exit;

  gPrintOptions.FromYearMonth := SetYearMonth(SelectedYear, SelectedMonth);
  gPrintOptions.ToYearMonth := gPrintOptions.FromYearMonth;
  PrintShiftCalendar;
end;                                    //procedure TEditShiftDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acPrintRangeExecute(Sender: TObject);
begin
  if not WriteDataToDB then exit;
  if GetPrinterOptions(poPortrait, [poPrintColors, poShiftCalRange], Self) then PrintShiftCalendar;
end;                                    //procedure TEditShiftDlg.ActionPrintRangeExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acShowWeekendExecute(Sender: TObject);
begin
  if kspGrid.ShowTeamColors then begin
    kspGrid.ShowTeamColors := false;
    kspGrid.ShowWeekEndDays := true;
  end                                   //if kspGrid.ShowTeamColors then
  else kspGrid.ShowWeekEndDays := not kspGrid.ShowWeekEndDays;
end;                                    //procedure TEditShiftDlg.ActionShowWeekendExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acShowTeamExecute(Sender: TObject);
begin
  kspGrid.ShowTeamColors := not kspGrid.ShowTeamColors;
end;                                    //procedure TEditShiftDlg.ActionShowTeamExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acSaveChangesExecute(Sender: TObject);
begin
  WriteDataToDB;
end;                                    //procedure TEditShiftDlg.ActionSaveChangesExecute
//-----------------------------------------------------------------------------
{}
// NativeADO
procedure TEditShiftDlg.acCopyDefaultShiftExecute(Sender: TObject);
var
  xFound,
    xOK: boolean;
  xDay,
    xShift,
    xShiftDuration: integer;
  xShiftID: longint;
  xQuery: TmmADODataSet;
  xCommand: TmmADOCommand;
  xDateFrom,
    xShiftStart,
    xMaxDate: tdatetime;
  xDSRec: rDefaultShiftT;
  xCopyRec: rCopyShiftT;
  xCheckBegin,
    xCheckEnd: rShiftCheckT;
  xProgressForm: TProgressForm;
  xDB: TAdoDBAccess;
  xInsertQuery: TNativeAdoQuery;
  xUpdateQuery: TNativeAdoQuery;
  xDeleteQuery: TNativeAdoQuery;
  xSelectQuery: TNativeAdoQuery;
  xQueryText : string;
begin
  if not SaveQuery_OK then exit;

  xMaxDate := GetMaxCopyDate;
  xDateFrom := EncodeDate(SelectedYear, SelectedMonth, 1);
  if xDateFrom < (Date + 1) then xDateFrom := (Date + 1);

 //1. select default shifts and period
// with TCopyDefaultShiftDlg.Create(Application) do
  with TCopyDefaultShiftDlg.Create(Self) do begin
    dtFrom.MinDate := date + 1;
    dtFrom.Date := xDateFrom;
    dtFrom.MaxDate := xMaxDate;
    dtTo.MinDate := date + 1;
    dtTo.Date := EncodeDate(SelectedYear, SelectedMonth, DaysPerMonth(SelectedYear, SelectedMonth));
    dtTo.MaxDate := xMaxDate;
    Caption := format((cCaptionStr2), [cobShiftCal.GetItemText]);
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then xCopyRec := gCPRec;
  end;                                  //with CopyDefaultShiftDlg.Create(Application) do

  if not xOK then exit;
  if MMMessageDlg((cMsgStr2), mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then exit;

  Update;


 //2. read default shift data
  fillchar(xDSRec, sizeof(rDefaultShiftT), 0);
  xQuery := TmmADODataSet.Create(Application);
  xCommand := nil;
  try
    xQuery.ConnectionString := GetDefaultConnectionString;
    xCommand := TmmADOCommand.Create(Application);
    xCommand.ConnectionString := GetDefaultConnectionString;
    xQuery.CommandText := 'SELECT * FROM t_default_shift';
    xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
    xQuery.CommandText := xQuery.CommandText + 'ORDER BY c_default_shift_id';
    xQuery.Open;

    while not xQuery.Eof do begin
      xDay := xQuery.FieldByName('c_default_shift_id').AsInteger div 10;
      xShift := xQuery.FieldByName('c_default_shift_id').AsInteger mod 10;
      xDSRec.StartTime[xDay, xShift] := trim(xQuery.FieldByName('c_start_time').AsString);
      xQuery.Next;
    end;                                //while not xQuery.Eof do

    xQuery.Close;
    if xCopyRec.Variante = 1 then xCopyRec.Period.toDate := xMaxDate;


  //3. check shift duration of last record before "begin of copy period"
    xCheckBegin := kspGrid.GetShiftRecordData(xCopyRec.Period.fromDate, 1, sdPrev);
    if xCheckBegin.ShiftID > 0 then begin
      xDay := bDOW(DayOfWeek(xCopyRec.Period.fromDate));
      xShiftStart := xCopyRec.Period.fromDate + GetTimeFromStr(xDSRec.StartTime[xDay, 1]);
      xCheckBegin.Duration := round((xShiftStart - xCheckBegin.StartTime) * cMinPerDay);
      if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then begin
        MessageBeep(0);
        MMMessageDlg(format(cMsgStr3a + ' ' + cMsgStr3b, [xCheckBegin.Shift, formatdatetime(ShortDateFormat, xCheckBegin.StartTime)]), mtError, [mbOK], 0, Self);
        Exit;
      end;                              //if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then
    end;                                //if xCheckBegin.ShiftID > 0 then


  //4. check shift duration of first record after "end of copy period"
    xCheckEnd := kspGrid.GetShiftRecordData(xCopyRec.Period.toDate + 1, 1, sdNone);
    if xCheckEnd.ShiftID > 0 then begin
      xDay := bDOW(DayOfWeek(xCopyRec.Period.toDate));
      for xShift := cMaxShiftsPerDay downto 1 do
        if xDSRec.StartTime[xDay, xShift] <> '' then break;

      xShiftStart := xCopyRec.Period.toDate + GetTimeFromStr(xDSRec.StartTime[xDay, xShift]);
      xCheckEnd.Duration := round((xCheckEnd.StartTime - xShiftStart) * cMinPerDay);
      if (xCheckEnd.Duration < cMinShiftDuration) or (xCheckEnd.Duration > cMaxShiftDuration) then begin
        MessageBeep(0);
        MMMessageDlg(Format(cMsgStr2, [xShift, formatdatetime(ShortDateFormat, xShiftStart)]), mtError, [mbOK], 0, Self);
        Exit;
      end;                              //if (xCheckEnd.Duration < cMinShiftDuration) or (xCheckEnd.Duration > cMaxShiftDuration) then
    end;                                //if xCheckEnd.ShiftID > 0 then


  //5. write default shift data into t_shift
    Application.CreateForm(TProgressForm, xProgressForm);
    xProgressForm.Init((cProgressStr1), round(xCopyRec.Period.toDate - xCopyRec.Period.fromDate) + 1, 1);


    xDB := TAdoDBAccess.Create(4,ADODB_TLB._Connection(dmShiftCalendar.conDefault.ConnectionObject));
    try
      xDB.Init;
      xDB.StartTransaction;
      try
        xUpdateQuery := xDB.Query[cPrimaryQuery];
        xQueryText := 'UPDATE t_shift';
        xQueryText := xQueryText + ' SET c_shift_start = :shift_start,';
        xQueryText := xQueryText + ' c_shift_length = :c_shift_length';
        xQueryText := xQueryText + ' WHERE c_shiftcal_id = :c_shiftcal_id';
        xQueryText := xQueryText + ' AND c_shift_id = :c_shift_id';
        xUpdateQuery.SQL.Text := xQueryText;

        xInsertQuery := xDB.Query[cSecondaryQuery];
        xQueryText := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_shift_in_day,c_shift_start,c_shift_length)';
        xQueryText := xQueryText + ' VALUES(:shift_id,:shiftcal_id,:shift_in_day,:shift_start,:shift_length)';
        xInsertQuery.SQL.Text := xQueryText;


        xDeleteQuery := xDB.Query[cThirdQuery];
        xQueryText := 'DELETE FROM t_shift';
        xQueryText := xQueryText + ' WHERE c_shiftcal_id = :c_shiftcal_id';
        xQueryText := xQueryText + ' AND c_shift_id = :c_shift_id';
        xDeleteQuery.SQL.Text := xQueryText;

        xSelectQuery := xDB.Query[cThirdQuery + 1];
        xQueryText := 'SELECT COUNT(*) AS anzahl FROM t_shift';
        xQueryText := xQueryText + ' WHERE c_shiftcal_id = :c_shiftcal_id';
        xQueryText := xQueryText + ' AND c_shift_id = :c_shift_id';
        xSelectQuery.SQL.Text := xQueryText;

        while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do begin
          xProgressForm.StepIt;

          xDay := bDOW(DayOfWeek(xCopyRec.Period.fromDate));

          for xShift := 1 to cMaxShiftsPerDay do begin
            xShiftID := GetShiftID(xCopyRec.Period.fromDate, xShift);
            if xDSRec.StartTime[xDay, xShift] = '' then begin
              xDeleteQuery.ParamByName('c_shiftcal_id').AsInteger := cobShiftCal.GetItemID;
              xDeleteQuery.ParamByName('c_shift_id').AsInteger := xShiftID;
              xDeleteQuery.ExecSQL;
            end //if xDSRec.StartTime[xDay,sShift] = '' then
            else begin
              xSelectQuery.ParamByName('c_shiftcal_id').AsInteger := cobShiftCal.GetItemID;
              xSelectQuery.ParamByName('c_shift_id').AsInteger := xShiftID;
              xSelectQuery.Open;
              xFound := xSelectQuery.FieldByName('anzahl').AsInteger > 0;
              xSelectQuery.Close;

              xShiftStart := xCopyRec.Period.fromDate + GetTimeFromStr(xDSRec.StartTime[xDay, xShift]);
              xShiftDuration := GetDS_ShiftDuration(xDSRec, xDay, xShift);

              if xFound then begin
                xUpdateQuery.ParamByname('c_shift_length').asInteger := xShiftDuration;
                xUpdateQuery.ParamByname('c_shiftcal_id').asInteger := cobShiftCal.GetItemID;
                xUpdateQuery.ParamByname('c_shift_id').asInteger := xShiftID;

                xUpdateQuery.ParamByname('shift_start').asDateTime := xShiftStart;
                xUpdateQuery.ExecSQL;
              end //if xFound then
              else begin
                xInsertQuery.ParamByname('shift_id').AsInteger := xShiftID;
                xInsertQuery.ParamByname('shiftcal_id').asInteger := cobShiftCal.GetItemID;
                xInsertQuery.ParamByname('shift_in_day').AsInteger := xShift;
                xInsertQuery.ParamByname('shift_start').asDateTime := xShiftStart;
                xInsertQuery.ParamByname('shift_length').asInteger := xShiftDuration;
                xInsertQuery.ExecSQL;
              end;//if xFound then
            end;//else if xDSRec.StartTime[xDay,sShift] = '' then
          end;//for xShift := 1 to cMaxShiftsPerDay do

          xCopyRec.Period.fromDate := xCopyRec.Period.fromDate + 1;
        end;//while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do

     //update shift duration of last record before "begin of copy period"
        if xCheckBegin.ShiftID > 0 then begin
          xQueryText := 'UPDATE t_shift';
          xQueryText := xQueryText + format(' SET c_shift_length = %d', [xCheckBegin.Duration]);
          xQueryText := xQueryText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
          xQueryText := xQueryText + format(' AND c_shift_id = %d', [xCheckBegin.ShiftID]);
          xUpdateQuery.SQL.Text := xQueryText;
          xUpdateQuery.ExecSQL;
        end;//if xCheckBegin.ShiftID > 0 then

     //update shift duration of first record after "end of copy period"
        xShiftDuration := xCheckEnd.Duration;
        xCheckEnd := kspGrid.GetShiftRecordData(xCopyRec.Period.toDate + 1, 1, sdPrev);
        xQueryText := 'UPDATE t_shift';
        xQueryText := xQueryText + format(' SET c_shift_length = %d', [xShiftDuration]);
        xQueryText := xQueryText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
        xQueryText := xQueryText + format(' AND c_shift_id = %d', [xCheckEnd.ShiftID]);
        xUpdateQuery.SQL.Text := xQueryText;
        xUpdateQuery.ExecSQL;

        xDB.Commit;
      except
        on e:Exception do begin
          WriteDebugLog('[TEditShiftDlg.acCopyDefaultShiftExecute]', e.Message);
          xDB.Rollback;
        end;// on e:Exception do begin
      end;//try..except
    finally
      xDB.Free;
    end;// try finally

  //6. update screen
    kspGrid.ReadShiftData;
  finally
    xProgressForm.Free;
    xQuery.Free;
    xCommand.Free;
  end;//try..finally
end;//procedure TEditShiftDlg.acCopyDefaultShiftExecute
{}
{
// ADOI Express 1 : 1 Umbau von BDE
procedure TEditShiftDlg.acCopyDefaultShiftExecute(Sender: TObject);
var
  xFound,
    xOK: boolean;
  xDay,
    xShift,
    xShiftDuration: integer;
  xShiftID: longint;
  xQuery: TmmADODataSet;
  xCommand: TmmADOCommand;
  xDateFrom,
    xShiftStart,
    xMaxDate: tdatetime;
  xDSRec: rDefaultShiftT;
  xCopyRec: rCopyShiftT;
  xCheckBegin,
    xCheckEnd: rShiftCheckT;
  xProgressForm: TProgressForm;

begin
  if not SaveQuery_OK then exit;

  xMaxDate := GetMaxCopyDate;
  xDateFrom := EncodeDate(SelectedYear, SelectedMonth, 1);
  if xDateFrom < (Date + 1) then xDateFrom := (Date + 1);

 //1. select default shifts and period
// with TCopyDefaultShiftDlg.Create(Application) do
  with TCopyDefaultShiftDlg.Create(Self) do begin
    dtFrom.MinDate := date + 1;
    dtFrom.Date := xDateFrom;
    dtFrom.MaxDate := xMaxDate;
    dtTo.MinDate := date + 1;
    dtTo.Date := EncodeDate(SelectedYear, SelectedMonth, DaysPerMonth(SelectedYear, SelectedMonth));
    dtTo.MaxDate := xMaxDate;
    Caption := format((cCaptionStr2), [cobShiftCal.GetItemText]);
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then xCopyRec := gCPRec;
  end;                                  //with CopyDefaultShiftDlg.Create(Application) do

  if not xOK then exit;
  if MMMessageDlg((cMsgStr2), mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then exit;

  Update;


 //2. read default shift data
  fillchar(xDSRec, sizeof(rDefaultShiftT), 0);
  xQuery := TmmADODataSet.Create(Application);
  xCommand := nil;
  try
    xQuery.ConnectionString := GetDefaultConnectionString;
    xCommand := TmmADOCommand.Create(Application);
    xCommand.ConnectionString := GetDefaultConnectionString;
    xQuery.CommandText := 'SELECT * FROM t_default_shift';
    xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
    xQuery.CommandText := xQuery.CommandText + 'ORDER BY c_default_shift_id';
    xQuery.Open;

    while not xQuery.Eof do begin
      xDay := xQuery.FieldByName('c_default_shift_id').AsInteger div 10;
      xShift := xQuery.FieldByName('c_default_shift_id').AsInteger mod 10;
      xDSRec.StartTime[xDay, xShift] := trim(xQuery.FieldByName('c_start_time').AsString);
      xQuery.Next;
    end;                                //while not xQuery.Eof do

    xQuery.Close;
    if xCopyRec.Variante = 1 then xCopyRec.Period.toDate := xMaxDate;


  //3. check shift duration of last record before "begin of copy period"
    xCheckBegin := kspGrid.GetShiftRecordData(xCopyRec.Period.fromDate, 1, sdPrev);
    if xCheckBegin.ShiftID > 0 then begin
      xDay := bDOW(DayOfWeek(xCopyRec.Period.fromDate));
      xShiftStart := xCopyRec.Period.fromDate + GetTimeFromStr(xDSRec.StartTime[xDay, 1]);
      xCheckBegin.Duration := round((xShiftStart - xCheckBegin.StartTime) * cMinPerDay);
      if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then begin
        MessageBeep(0);
//    MessageDlg(format((cMsgStr3),[xCheckBegin.Shift,formatdatetime_('dd/mm/yyyy',xCheckBegin.StartTime)]),mtError,[mbOK],0);
//    MMMessageDlg(format((cMsgStr3),[xCheckBegin.Shift,formatdatetime(ShortDateFormat, xCheckBegin.StartTime)]),mtError,[mbOK],0, Self);
        MMMessageDlg(format(cMsgStr3a + ' ' + cMsgStr3b, [xCheckBegin.Shift, formatdatetime(ShortDateFormat, xCheckBegin.StartTime)]), mtError, [mbOK], 0, Self);
        Exit;
      end;                              //if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then
    end;                                //if xCheckBegin.ShiftID > 0 then


  //4. check shift duration of first record after "end of copy period"
    xCheckEnd := kspGrid.GetShiftRecordData(xCopyRec.Period.toDate + 1, 1, sdNone);
    if xCheckEnd.ShiftID > 0 then begin
      xDay := bDOW(DayOfWeek(xCopyRec.Period.toDate));
      for xShift := cMaxShiftsPerDay downto 1 do
        if xDSRec.StartTime[xDay, xShift] <> '' then break;

      xShiftStart := xCopyRec.Period.toDate + GetTimeFromStr(xDSRec.StartTime[xDay, xShift]);
      xCheckEnd.Duration := round((xCheckEnd.StartTime - xShiftStart) * cMinPerDay);
      if (xCheckEnd.Duration < cMinShiftDuration) or (xCheckEnd.Duration > cMaxShiftDuration) then begin
        MessageBeep(0);
//Wss    MessageDlg(format(cMsgStr2,[xShift,formatdatetime_('dd/mm/yyyy',xShiftStart)]),mtError,[mbOK],0);
        MMMessageDlg(Format(cMsgStr2, [xShift, formatdatetime(ShortDateFormat, xShiftStart)]), mtError, [mbOK], 0, Self);
        Exit;
      end;                              //if (xCheckEnd.Duration < cMinShiftDuration) or (xCheckEnd.Duration > cMaxShiftDuration) then
    end;                                //if xCheckEnd.ShiftID > 0 then


  //5. write default shift data into t_shift
    Application.CreateForm(TProgressForm, xProgressForm);
    xProgressForm.Init((cProgressStr1), round(xCopyRec.Period.toDate - xCopyRec.Period.fromDate) + 1, 1);
    dmShiftCalendar.conDefault.BeginTrans;
    try
      while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do begin
        xProgressForm.StepIt;

        xDay := bDOW(DayOfWeek(xCopyRec.Period.fromDate));

        for xShift := 1 to cMaxShiftsPerDay do begin
          xShiftID := GetShiftID(xCopyRec.Period.fromDate, xShift);
          if xDSRec.StartTime[xDay, xShift] = '' then begin
            xCommand.CommandText := 'DELETE FROM t_shift';
            xCommand.CommandText := xCommand.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
            xCommand.CommandText := xCommand.CommandText + format(' AND c_shift_id = %d', [xShiftID]);
            xCommand.Execute;
          end                           //if xDSRec.StartTime[xDay,sShift] = '' then
          else begin
            xQuery.CommandText := 'SELECT COUNT(*) AS anzahl FROM t_shift';
            xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
            xQuery.CommandText := xQuery.CommandText + format(' AND c_shift_id = %d', [xShiftID]);
            xQuery.Open;
            xFound := xQuery.FieldByName('anzahl').AsInteger > 0;
            xQuery.Active := false;

            xShiftStart := xCopyRec.Period.fromDate + GetTimeFromStr(xDSRec.StartTime[xDay, xShift]);
            xShiftDuration := GetDS_ShiftDuration(xDSRec, xDay, xShift);

            if xFound then begin
              xCommand.CommandText := 'UPDATE t_shift';
              xCommand.CommandText := xCommand.CommandText + ' SET c_shift_start = :shift_start,';
              xCommand.CommandText := xCommand.CommandText + format(' c_shift_length = %d', [xShiftDuration]);
              xCommand.CommandText := xCommand.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
              xCommand.CommandText := xCommand.CommandText + format('AND c_shift_id = %d', [xShiftID]);
              xCommand.Parameters.ParamByname('shift_start').DataType := ftDateTime;
              xCommand.Parameters.ParamByname('shift_start').value := xShiftStart;
              xCommand.Execute;
            end                         //if xFound then
            else begin
              xCommand.CommandText := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_shift_in_day,c_shift_start,c_shift_length)';
              xCommand.CommandText := xCommand.CommandText + ' VALUES(:shift_id,:shiftcal_id,:shift_in_day,:shift_start,:shift_length)';
              xCommand.Parameters.ParamByname('shift_id').value := xShiftID;
              xCommand.Parameters.ParamByname('shiftcal_id').value := cobShiftCal.GetItemID;
              xCommand.Parameters.ParamByname('shift_in_day').value := xShift;
              xCommand.Parameters.ParamByname('shift_start').DataType := ftDateTime;
              xCommand.Parameters.ParamByname('shift_start').value := xShiftStart;
              xCommand.Parameters.ParamByname('shift_length').value := xShiftDuration;
              xCommand.Execute;
            end;                        //if xFound then
          end;                          //else if xDSRec.StartTime[xDay,sShift] = '' then
        end;                            //for xShift := 1 to cMaxShiftsPerDay do

        xCopyRec.Period.fromDate := xCopyRec.Period.fromDate + 1;
      end;                              //while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do

   //update shift duration of last record before "begin of copy period"
      if xCheckBegin.ShiftID > 0 then begin
        xCommand.CommandText := 'UPDATE t_shift';
        xCommand.CommandText := xCommand.CommandText + format(' SET c_shift_length = %d', [xCheckBegin.Duration]);
        xCommand.CommandText := xCommand.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
        xCommand.CommandText := xCommand.CommandText + format(' AND c_shift_id = %d', [xCheckBegin.ShiftID]);
        xCommand.Execute;
      end;                              //if xCheckBegin.ShiftID > 0 then

   //update shift duration of first record after "end of copy period"
      xShiftDuration := xCheckEnd.Duration;
      xCheckEnd := kspGrid.GetShiftRecordData(xCopyRec.Period.toDate + 1, 1, sdPrev);
      xCommand.CommandText := 'UPDATE t_shift';
      xCommand.CommandText := xCommand.CommandText + format(' SET c_shift_length = %d', [xShiftDuration]);
      xCommand.CommandText := xCommand.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
      xCommand.CommandText := xCommand.CommandText + format(' AND c_shift_id = %d', [xCheckEnd.ShiftID]);
      xCommand.Execute;

      dmShiftCalendar.conDefault.CommitTrans;
    except
      dmShiftCalendar.conDefault.RollbackTrans;
    end;                                //try..except


  //6. update screen
    kspGrid.ReadShiftData;
  finally
    xProgressForm.Free;
    xQuery.Free;
    xCommand.Free;
  end;                                  //try..finally
end;                                    //procedure TEditShiftDlg.ActionCopyDefaultShiftExecute
{}
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acCopyShiftPatternExecute(Sender: TObject);
var
  xCopyRec: rCopyShiftT;
  xOK: boolean;
  xPatSize: integer;                    // pattern period
  xDay,
    xShift: integer;
  xQuery: TmmADODataSet;
  xCommand: TmmADOCommand;
  xSPRec: rShiftPatternT;
  xDateFrom,
    xMaxDate: tdatetime;
  xProgressForm: TProgressForm;

begin
  if not SaveQuery_OK then exit;

  xMaxDate := GetMaxCopyDate;
  xDateFrom := EncodeDate(SelectedYear, SelectedMonth, 1);
  if xDateFrom < (Date + 1) then xDateFrom := Date + 1;

 //1. select pattern and period
// with TCopyShiftPatternDlg.Create(Application) do
  with TCopyShiftPatternDlg.Create(Self) do begin
    dtFrom.MinDate := date + 1;
    dtFrom.Date := xDateFrom;
    dtFrom.MaxDate := xMaxDate;
    dtTo.MinDate := date + 1;
    dtTo.Date := EncodeDate(SelectedYear, SelectedMonth, DaysPerMonth(SelectedYear, SelectedMonth));
    dtTo.MaxDate := xMaxDate;
    Caption := format({LTrans{}(cCaptionStr3), [cobShiftCal.GetItemText]);
    SetPatternIDFromShift(cobShiftCal.GetItemID);
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then xCopyRec := gCPRec;
  end;                                  //with CopyShiftPatternDlg.Create(Application) do

  if not xOK then exit;
  if MMMessageDlg({LTrans{}(cMsgStr4), mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then exit;

  Update;

 //2. read selected shift pattern data
  xPatSize := 1;
  fillchar(xSPRec, sizeof(rShiftPatternT), 0);
  xQuery := TmmADODataSet.Create(Application);
  xCommand := nil;
  try
    xQuery.Connection := dmShiftCalendar.conDefault;
    xCommand := TmmADOCommand.Create(Application);
    xCommand.Connection := dmShiftCalendar.conDefault;

    xQuery.CommandText := 'SELECT * FROM t_shift_pattern';
    xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shift_pattern_id = %d', [xCopyRec.PatternID]);
    xQuery.CommandText := xQuery.CommandText + 'ORDER BY c_shift_in_id';
    xQuery.Open;
    while not xQuery.Eof do begin
      xDay := xQuery.FieldByName('c_shift_in_id').AsInteger div 10;
      xShift := xQuery.FieldByName('c_shift_in_id').AsInteger mod 10;
      xSPRec.Team[xDay, xShift] := xQuery.FieldByName('c_team_id').AsString;
      if xDay > xPatSize then xPatSize := xDay;
      xQuery.Next;
    end;                                //while not xQuery.Eof do

    xQuery.Close;

    case xCopyRec.Variante of
      1: xCopyRec.Period.toDate := xMaxDate;
      2: begin
          xCopyRec.Period.toDate := xCopyRec.Period.fromDate + xPatSize * xCopyRec.Cycles;
          if xCopyRec.Period.toDate > xMaxdate then xCopyRec.Period.toDate := xMaxdate;
        end;                            //2
    end;                                //case xCopyRec.Variante of


  //3. write shift pattern data into t_shift
    Application.CreateForm(TProgressForm, xProgressForm);
    xProgressForm.Init({LTrans{}(cProgressStr2), round(xCopyRec.Period.toDate - xCopyRec.Period.fromDate) + 1, 1);
    dmShiftCalendar.conDefault.BeginTrans;
    try
      xCommand.CommandText := 'UPDATE t_shift';
      xCommand.CommandText := xCommand.CommandText + ' SET c_team_id = :team_id';
      xCommand.CommandText := xCommand.CommandText + format(' WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID]);
      xCommand.CommandText := xCommand.CommandText + ' AND c_shift_id = :shift_id';

      xDay := 1;
      while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do begin
        xProgressForm.StepIt;

        for xShift := 1 to cMaxShiftsPerDay do begin
          xCommand.Parameters.ParamByname('shift_id').value := GetShiftID(xCopyRec.Period.fromDate, xShift);
          xCommand.Parameters.ParamByname('team_id').value := xSPRec.Team[xDay, xShift];
          xCommand.Execute;
        end;                            //for xShift := 1 to cMaxShiftsPerDay do

        xCopyRec.Period.fromDate := xCopyRec.Period.fromDate + 1;
        inc(xDay);
        if xDay > xPatSize then xDay := 1;
      end;                              //while xCopyRec.Period.fromDate <= xCopyRec.Period.toDate do

      dmShiftCalendar.conDefault.CommitTrans;
    except
      dmShiftCalendar.conDefault.RollbackTrans;
    end;                                //try..except

  //4. update screen
    kspGrid.ReadShiftData;
  finally
    xProgressForm.Free;
    xQuery.Free;
    xCommand.Free;
  end;                                  //try..finally
end;                                    //procedure TEditShiftDlg.ActionCopyShiftPatternExecute
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.acHelpExecute(Sender: TObject);
{
var
 i   : Integer;
 Msg : TNMUpDown;
}

begin
  Application.HelpCommand(0, HelpContext);
 {
 FillChar(Msg,SizeOf(Msg),0);
 with Msg do
 begin
  hdr.hwndFrom := kMonthTab.Handle;
  hdr.idFrom := kMonthTab.Handle;
  hdr.code := UDN_DELTAPOS;
  iPos := kMonthTab.TabIndex;
  iDelta := 5;
 end; //with Msg do
 i := SendMessage(kMonthTab.Handle,WM_NOTIFY,kMonthTab.Handle,Longint(@Msg));
 }
end;                                    //procedure TEditShiftDlg.ActionHelpContentExecute
//-----------------------------------------------------------------------------
{}
procedure TEditShiftDlg.EnableActions;
begin
  acEditTeam.Enabled := MMSecurityControl.CanEnabled(acEditTeam) and
    (GetRecordCount('t_department', '') > 0);
  acEditShiftCalendar.Enabled := MMSecurityControl.CanEnabled(acEditShiftCalendar) and
    ((GetRecordCount('t_shift_pattern_name', '') * GetRecordCount('t_factory_calendar', '')) > 0);
  acEditShiftPattern.Enabled := MMSecurityControl.CanEnabled(acEditShiftPattern) and
    (GetRecordCount('t_team', '') > 0);
  acEditDefaultShifts.Enabled := MMSecurityControl.CanEnabled(acEditDefaultShifts) and
    (GetRecordCount('t_shiftcal', '') > 0);
  acCopyDefaultShift.Enabled := MMSecurityControl.CanEnabled(acCopyDefaultShift) and
    (GetRecordCount('t_default_shift', format('WHERE c_shiftcal_id = %d', [cobShiftCal.GetItemID])) > 0);
  acCopyShiftPattern.Enabled := MMSecurityControl.CanEnabled(acCopyShiftPattern) and
    ((GetRecordCount('t_shift_pattern_name', '') * GetRecordCount('t_shiftcal', '')) > 0);

  cobShiftCal.Enabled := MMSecurityControl.CanEnabled(cobShiftCal) and
    (GetRecordCount('t_shiftcal', '') > 0);

  acSaveChanges.Enabled := MMSecurityControl.CanEnabled(acSaveChanges);
  kspGrid.Enabled := acSaveChanges.Enabled;

end;                                    //procedure TEditShiftDlg.EnableActions
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.TextToTranslate;
begin
  kspGrid.Refresh;
  UpdateFormCaption;
  UpdateTabs;
end;                                    //procedure TEditShiftDlg.TextToTranslate
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.TranslateText;
begin
  TextToTranslate;
// inherited;
end;                                    //procedure TEditShiftDlg.TranslateText
//-----------------------------------------------------------------------------
procedure TEditShiftDlg.mmTranslatorLanguageChange(Sender: TObject);
begin
  kspGrid.Refresh;
  UpdateFormCaption;
  UpdateTabs;
end;                                    //procedure TEditShiftDlg.mmTranslatorLanguageChange
//------------------------------------------------------------------------------
procedure TEditShiftDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
end.                                    //u_edit_shift.pas

