inherited EditTeamDlg: TEditTeamDlg
  Left = 605
  Top = 105
  Width = 417
  Height = 227
  BorderIcons = [biSystemMenu]
  Caption = '(50)Schichtgruppen bearbeiten'
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 409
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bPrint: TToolButton
      Left = 34
      Top = 0
      Action = acPrint
    end
    object ToolButton8: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object DBNavigator1: TmmDBNavigator
      Left = 65
      Top = 0
      Width = 92
      Height = 22
      DataSource = srcTeam
      VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object ToolButton14: TToolButton
      Left = 157
      Top = 0
      Width = 8
      ImageIndex = 8
      Style = tbsDivider
    end
    object bHelp: TToolButton
      Left = 165
      Top = 0
      Action = acHelp
    end
  end
  object mDBGrid: TmmDBGrid
    Left = 0
    Top = 26
    Width = 409
    Height = 174
    Align = alClient
    DataSource = srcTeam
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = mDBGridCellClick
    OnDrawColumnCell = mDBGridDrawColumnCell
    OnEditButtonClick = mDBGridEditButtonClick
    OnKeyPress = mDBGridKeyPress
    Columns = <
      item
        Expanded = False
        FieldName = 'c_team_id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_team_name'
        Visible = True
      end
      item
        ButtonStyle = cbsEllipsis
        Expanded = False
        FieldName = 'c_team_color'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'calcDepartment'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_department_id'
        Visible = False
      end>
  end
  object srcTeam: TmmDataSource
    DataSet = tabTeam
    Left = 48
    Top = 72
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 88
    Top = 72
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
  end
  object queDepartment: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    CursorType = ctStatic
    CommandText = 
      'SELECT c_department_id, c_department_name  FROM t_department ORD' +
      'ER BY c_department_name'
    Parameters = <>
    Left = 8
    Top = 112
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 136
    Top = 72
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Hints'
        0)
      (
        '*'
        'DisplayLabel'
        0))
  end
  object tabTeam: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    BeforePost = tabTeamBeforePost
    AfterPost = tabTeamAfterPost
    BeforeDelete = tabTeamBeforeDelete
    OnNewRecord = tabTeamNewRecord
    CommandText = 'dbo.t_team'
    CommandType = cmdTable
    Parameters = <>
    Left = 8
    Top = 72
    object tabTeamc_team_id: TStringField
      DisplayLabel = '(20)Gruppe'
      DisplayWidth = 1
      FieldName = 'c_team_id'
      Required = True
      FixedChar = True
      Size = 1
    end
    object tabTeamc_team_name: TStringField
      DisplayLabel = '(20)Bezeichnung'
      DisplayWidth = 20
      FieldName = 'c_team_name'
    end
    object tabTeamc_team_color: TIntegerField
      DisplayLabel = '(20)Farbe'
      DisplayWidth = 10
      FieldName = 'c_team_color'
      Required = True
    end
    object calcDepartment: TStringField
      DisplayLabel = '(20)Abteilung'
      FieldKind = fkLookup
      FieldName = 'calcDepartment'
      LookupDataSet = queDepartment
      LookupKeyFields = 'c_department_id'
      LookupResultField = 'c_department_name'
      KeyFields = 'c_department_id'
      Lookup = True
    end
    object tabTeamc_department_id: TSmallintField
      DisplayLabel = '(20)Abteilung'
      DisplayWidth = 10
      FieldName = 'c_department_id'
      Required = True
      Visible = False
    end
  end
  object mColorDialog: TmmColorDialog
    Ctl3D = True
    Left = 136
    Top = 112
  end
end
