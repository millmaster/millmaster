(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_factorycalendar.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Erstellen von Betriebskalendern.
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.2002       LOK  | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_factorycalendar;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, DBCtrls,
  ComCtrls, ToolWin, Db, Grids, DBGrids, ActnList, mmActionList, mmDataSource,
  mmDBGrid, mmToolBar, IvDictio, IvAMulti, IvBinDic, IvMulti, IvEMulti, BASEFORM,
  mmTranslator, mmDBNavigator, u_dmShiftCalendar, ADODB, mmADODataSet, mmADOCommand, LoepfeGlobal;

type
  TEditFactoryCalendarDlg = class(TmmForm)
    sourceFactoryCal: TmmDataSource;
    tabFactoryCal: TmmADODataSet;
    tabFactoryCalC_FACTORY_CALENDAR_ID: TSmallintField;
    tabFactoryCalC_FACTORY_CALENDAR_NAME: TStringField;
    tabFactoryCalC_REMARK: TStringField;

    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bPrint: TToolButton;
    ToolButton8: TToolButton;
    bHelp: TToolButton;
    ToolButton14: TToolButton;
    DBNavigator1: TmmDBNavigator;
    DBGrid1: TmmDBGrid;

    ActionList1: TmmActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;

    procedure tabFactoryCalBeforeDelete(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tabFactoryCalAfterDeletePost(DataSet: TDataSet);
    procedure tabFactoryCalNewRecord(DataSet: TDataSet);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure tabFactoryCalBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
  public
  end; //EditFactoryCalendarDlg

var
  EditFactoryCalendarDlg: TEditFactoryCalendarDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,  MMHtmlHelp,

  mmDialogs, Printers,
  u_common_lib, u_common_global, u_main, u_global, u_lib, u_report_factorycalendar,
  mmEventLog;

{$R *.DFM}

resourcestring
  cMsgStr1 = '(255)Betriebskalender ist noch mit Schichtkalendern verbunden. Loeschen nicht moeglich !'; //ivlm
  cMsgStr2 = '(255)Soll der Kalender mit der Bezeichnung "%s" geloescht werden ?'; //ivlm

procedure TEditFactoryCalendarDlg.tabFactoryCalBeforeDelete(DataSet: TDataSet);
begin
  if IsLinkedMsg('T_SHIFTCAL', 'C_FACTORY_CALENDAR_ID', {LTrans{}(cMsgStr1), tabFactoryCalC_FACTORY_CALENDAR_ID.Value, Self) then
    SysUtils.Abort;
//  if not DeleteRecord(format(LTrans(cMsgStr2),[tabFactoryCalC_FACTORY_CALENDAR_NAME.Value])) then
  if MMMessageDlg(Format({LTrans{}(cMsgStr2), [tabFactoryCalC_FACTORY_CALENDAR_NAME.Value]),
    mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then
    SysUtils.Abort;

  //should be deleted by db-trigger
  dmShiftCalendar.conDefault.BeginTrans;
  try
    with TmmAdoCommand.Create(nil) do try
      ConnectionString := GetDefaultConnectionString;
      CommandText := Format('DELETE FROM t_except_days WHERE c_factory_calendar_id = %d', [tabFactoryCalC_FACTORY_CALENDAR_ID.Value]);
      Execute;
    finally
      free;
    end; // with TmmAdoCommand.Create(nil) do try

    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e: Exception do begin
      WriteDebugLog('[TEditFactoryCalendarDlg.tabFactoryCalBeforeDelete]', e.Message);
      dmShiftCalendar.conDefault.RollbackTrans;
      SysUtils.Abort;
    end; // on e:Exception do begin
  end; //try..except
end; //procedure TEditFactoryCalendarDlg.tabFactoryCalBeforeDelete
{-----------------------------------------------------------------------------}
procedure TEditFactoryCalendarDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalendernamen_bearbeiten.htm');
  tabFactoryCal.Open;
// HelpContext := hcEditFactoryCalendar;
end; //procedure TEditFactoryCalendarDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := PostRecord(tabFactoryCal);
end; //procedure TEditFactoryCalendarDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.tabFactoryCalAfterDeletePost(DataSet: TDataSet);
begin
  tabFactoryCal.Refresh;
end; //procedure TEditFactoryCalendarDlg.tabFactoryCalAfterDeletePost
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.tabFactoryCalNewRecord(DataSet: TDataSet);
var
  xNewID: longint;

begin
  xNewID := MakeUniqueID(1, tabFactoryCal.CommandText, tabFactoryCalC_FACTORY_CALENDAR_ID.FieldName, Self);
  if xNewID < 0 then SysUtils.Abort;
  tabFactoryCalC_FACTORY_CALENDAR_ID.Value := xNewID;
end; //procedure TEditFactoryCalendarDlg.tabFactoryCalNewRecord
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditFactoryCalendarDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.acPrintExecute(Sender: TObject);
begin
//  if not GetPrinterOptions(poPortrait, [], Self) then exit;
  if not GetPrintOptions(poPortrait, Self, gPrintOptions.PrintColors) then exit;
  PrintQuickReport(TReportFactoryCalendarForm, ReportFactoryCalendarForm);
end; //procedure TEditFactoryCalendarDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditFactoryCalendarDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.tabFactoryCalBeforePost(DataSet: TDataSet);
begin
  if not DBFieldOk(tabFactoryCalC_FACTORY_CALENDAR_NAME, DBGrid1.Columns[0].Title.Caption, 0, 0, True, Self) then SysUtils.Abort;
  if not DBFieldOk(tabFactoryCalC_REMARK, DBGrid1.Columns[1].Title.Caption, 0, 0, True, Self) then SysUtils.Abort;
end; //procedure TEditFactoryCalendarDlg.tabFactoryCalBeforePost
//-----------------------------------------------------------------------------
procedure TEditFactoryCalendarDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditFactoryCalendarDlg.FormClose

procedure TEditFactoryCalendarDlg.DBGrid1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_DELETE) and (ssCtrl in Shift) then
    Key := 0;
end;

end. //u_edit_shiftcalendar.pas

