unit shiftcalendar_TLB;

// ************************************************************************ //
// WARNUNG                                                                    
// -------                                                                    
// Die in dieser Datei deklarierten Typen wurden aus Daten einer Typbibliothek
// generiert. Wenn diese Typbibliothek explizit oder indirekt (ueber eine     
// andere Typbibliothek) reimportiert wird oder wenn die Anweisung            
// 'Aktualisieren' im Typbibliotheks-Editor waehrend des Bearbeitens der     
// Typbibliothek aktiviert ist, wird der Inhalt dieser neu generiert und alle 
// manuell vorgenommenen Aenderungen gehen verloren.                           
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88  $
// Datei generiert am 21.01.2000 16:03:23 aus der unten beschriebenen Typbibliothek.

// *************************************************************************//
// HINWEIS:                                                                   
// Eintraege, die von $IFDEF_LIVE_SERVER_AT_DESIGN_TIME ueberwacht sind,        
// werden von Eigenschaften verwendet, die Objekte zurueckgeben, die           
// explizit ueber einen Funktionsaufruf vor dem Zugriff durch die              
// Eigenschaft erzeugt werden muessen. Diese Eintraege wurden deaktiviert,      
// um eine unbeabsichtigte Nutzung aus dem Objektinspektor heraus zu          
// verhindern. Sie koennen sie aktivieren, indem Sie                           
// LIVE_SERVER_AT_DESIGN_TIME definieren oder durch selektives Entfernen aus  
// den $IFDEF-Bloecken. Wie auch immer, diese Eintraege muessen                  
// weiterhin programmiererisch ueber eine Methode der passenden CoClass        
// erzeugt werden, bevor sie verwendet werden koennen.                         
// ************************************************************************ //
// Typbib: D:\DPROG_32\Loepfe\shiftcalendar\shiftcalendar.tlb (1)
// IID\LCID: {9A183149-CFF2-11D3-9ECB-0000861C3DB7}\0
// Hilfedatei: 
// AbhLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// In dieser Typbibliothek deklarierte GUIDS . Es werden folgende         
// Praefixe verwendet:                                                     
//   Typbibliotheken     : LIBID_xxxx                                     
//   CoClasses           : CLASS_xxxx                                     
//   DISPInterfaces      : DIID_xxxx                                      
//   Non-DISP interfaces : IID_xxxx                                       
// *********************************************************************//
const
  // Haupt- und Nebenversionen der Typbibliothek
  shiftcalendarMajorVersion = 1;
  shiftcalendarMinorVersion = 0;

  LIBID_shiftcalendar: TGUID = '{9A183149-CFF2-11D3-9ECB-0000861C3DB7}';

  IID_IShiftCalendarServer: TGUID = '{9A18314A-CFF2-11D3-9ECB-0000861C3DB7}';
  CLASS_ShiftCalendarServer: TGUID = '{9A18314C-CFF2-11D3-9ECB-0000861C3DB7}';
type

// *********************************************************************//
// Forward-Deklaration von in der Typbibliothek definierten Typen         
// *********************************************************************//
  IShiftCalendarServer = interface;
  IShiftCalendarServerDisp = dispinterface;

// *********************************************************************//
// Deklaration von in der Typbibliothek definierten CoClasses             
// (HINWEIS: Hier wird jede CoClass zu ihrer Standardschnittstelle        
// zugewiesen)                                                            
// *********************************************************************//
  ShiftCalendarServer = IShiftCalendarServer;


// *********************************************************************//
// Schnittstelle: IShiftCalendarServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9A18314A-CFF2-11D3-9ECB-0000861C3DB7}
// *********************************************************************//
  IShiftCalendarServer = interface(IDispatch)
    ['{9A18314A-CFF2-11D3-9ECB-0000861C3DB7}']
  end;

// *********************************************************************//
// DispIntf:  IShiftCalendarServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {9A18314A-CFF2-11D3-9ECB-0000861C3DB7}
// *********************************************************************//
  IShiftCalendarServerDisp = dispinterface
    ['{9A18314A-CFF2-11D3-9ECB-0000861C3DB7}']
  end;

// *********************************************************************//
// Die Klasse CoShiftCalendarServer stellt eine Methode Create und CreateRemote zur      
// Verfuegung, um Instanzen der Standardschnittstelle IShiftCalendarServer, dargestellt von
// CoClass ShiftCalendarServer, zu erzeugen. Diese Funktionen koennen                     
// von einem Client verwendet werden, der die CoClasses automatisieren    
// moechte, die von dieser Typbibliothek dargestellt werden.               
// *********************************************************************//
  CoShiftCalendarServer = class
    class function Create: IShiftCalendarServer;
    class function CreateRemote(const MachineName: string): IShiftCalendarServer;
  end;


// *********************************************************************//
// OLE-Server-Proxy-Klassendeklaration
// Server-Objekt    : TShiftCalendarServer
// Hilfe-String     : ShiftCalendarServer Objekt
// Standardschnittstelle: IShiftCalendarServer
// Def. Intf. DISP? : No
// Ereignisschnittstelle: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TShiftCalendarServerProperties= class;
{$ENDIF}
  TShiftCalendarServer = class(TOleServer)
  private
    FIntf:        IShiftCalendarServer;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TShiftCalendarServerProperties;
    function      GetServerProperties: TShiftCalendarServerProperties;
{$ENDIF}
    function      GetDefaultInterface: IShiftCalendarServer;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IShiftCalendarServer);
    procedure Disconnect; override;
    property  DefaultInterface: IShiftCalendarServer read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TShiftCalendarServerProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE-Server-Properties-Proxy-Klasse
// Server-Objekt    : TShiftCalendarServer
// (Dieses Objekt wird vom Eigenschaftsinspektor der IDE verwendet,
//  um die Eigenschaften dieses Servers zu bearbeiten)
// *********************************************************************//
 TShiftCalendarServerProperties = class(TPersistent)
  private
    FServer:    TShiftCalendarServer;
    function    GetDefaultInterface: IShiftCalendarServer;
    constructor Create(AServer: TShiftCalendarServer);
  protected
  public
    property DefaultInterface: IShiftCalendarServer read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoShiftCalendarServer.Create: IShiftCalendarServer;
begin
  Result := CreateComObject(CLASS_ShiftCalendarServer) as IShiftCalendarServer;
end;

class function CoShiftCalendarServer.CreateRemote(const MachineName: string): IShiftCalendarServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_ShiftCalendarServer) as IShiftCalendarServer;
end;

procedure TShiftCalendarServer.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{9A18314C-CFF2-11D3-9ECB-0000861C3DB7}';
    IntfIID:   '{9A18314A-CFF2-11D3-9ECB-0000861C3DB7}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TShiftCalendarServer.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IShiftCalendarServer;
  end;
end;

procedure TShiftCalendarServer.ConnectTo(svrIntf: IShiftCalendarServer);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TShiftCalendarServer.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TShiftCalendarServer.GetDefaultInterface: IShiftCalendarServer;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface ist NULL. Die Komponente ist nicht mit dem Server verbunden. Sie muessen vor dieser Operation ''Connect'' oder ''ConnectTo'' aufrufen');
  Result := FIntf;
end;

constructor TShiftCalendarServer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TShiftCalendarServerProperties.Create(Self);
{$ENDIF}
end;

destructor TShiftCalendarServer.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TShiftCalendarServer.GetServerProperties: TShiftCalendarServerProperties;
begin
  Result := FProps;
end;
{$ENDIF}

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TShiftCalendarServerProperties.Create(AServer: TShiftCalendarServer);
begin
  inherited Create;
  FServer := AServer;
end;

function TShiftCalendarServerProperties.GetDefaultInterface: IShiftCalendarServer;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents('Servers',[TShiftCalendarServer]);
end;

end.
