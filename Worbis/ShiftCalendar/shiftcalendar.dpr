program shiftcalendar;
 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  mmCS,
  LoepfeGlobal,
  Forms,
  BaseForm in '..\..\..\LoepfeShares\Templates\BaseForm.pas' {mmForm},
  BaseApplMain in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  BaseApplAboutBox in '..\..\..\LoepfeShares\Templates\BASEAPPLABOUTBOX.pas' {BaseApplAboutBoxForm},
  u_MAIN in 'u_MAIN.pas' {frmMain},
  u_lib in 'u_lib.pas',
  u_global in 'u_global.pas',
  u_edit_factorycalendar in 'u_edit_factorycalendar.pas' {EditFactoryCalendarDlg},
  u_edit_shiftcalendar in 'u_edit_shiftcalendar.pas' {EditShiftCalendarDlg},
  u_edit_defaultshift in 'u_edit_defaultshift.pas' {EditDefaultShiftDlg},
  u_progress in 'u_progress.pas' {ProgressForm},
  u_edit_team in 'u_edit_team.pas' {EditTeamDlg},
  u_edit_shiftpattern in 'u_edit_shiftpattern.pas' {EditShiftPatternDlg},
  u_copy_shiftpattern in 'u_copy_shiftpattern.pas' {CopyShiftPatternDlg},
  u_copy_defaultshift in 'u_copy_defaultshift.pas' {CopyDefaultShiftDlg},
  u_readwrite_options in 'u_readwrite_options.pas',
  u_report_machines in 'u_report_machines.pas' {ReportMachinesForm},
  u_report_teams in 'u_report_teams.pas' {ReportTeamsForm},
  u_report_shiftcalendar in 'u_report_shiftcalendar.pas' {ReportShiftCalendarForm},
  u_printgrid in 'u_printgrid.pas',
  u_common_lib in '..\common\u_common_lib.pas',
  u_common_global in '..\common\u_common_global.pas',
  u_set_factorycal_range in 'u_set_factorycal_range.pas' {SetFactoryCalRangeDlg},
  u_report_factorycalendar in 'u_report_factorycalendar.pas' {ReportFactoryCalendarForm},
  u_edit_options in 'u_edit_options.pas' {EditOptionsDlg},
  u_edit_shift in 'u_edit_shift.pas' {EditShiftDlg},
  u_edit_exceptdays in 'u_edit_exceptdays.pas' {EditExceptDaysDlg},
  shiftcalendar_TLB in 'shiftcalendar_TLB.pas',
  ShiftCalendarIMPL in 'ShiftCalendarIMPL.pas' {ShiftCalendarServer: CoClass},
  u_set_printoptions in 'u_set_printoptions.pas' {SetPrintOptionsDlg},
  mmCommonLib in '..\..\Common\mmCommonLib.pas',
  u_dmShiftCalendar in 'u_dmShiftCalendar.pas' {dmShiftCalendar: TDataModule};

{$R *.TLB}

{$R *.RES}
{$R 'Version.res'}

begin
  gApplicationName := 'Shiftcalendar';
  CodeSite.Enabled := CodeSiteEnabled;
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  Application.Initialize;
  Application.Title := 'Shiftcalendar';
  Application.CreateForm(TdmShiftCalendar, dmShiftCalendar);
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
