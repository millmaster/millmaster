(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_report_teams.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Quickreport Schichtgruppen
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 28.10.02 |      | LOK | Umbau ADO
| 15.12.04 |      | SDo | Print auch in B&W
|=========================================================================================*)

{$i symbols.inc}

unit u_report_teams;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,Db,
 Qrctrls,QuickRpt,ExtCtrls, IvDictio, IvMulti, IvEMulti,
 mmTranslator, mmQRDBText, mmQRShape, mmQRSysData, mmQRLabel, mmQRBand,
 mmQuickRep, BaseForm, u_dmShiftCalendar, ADODB, mmADODataSet, mmQRImage;

type
 TReportTeamsForm = class(TmmForm)
    QuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitel: TmmQRLabel;
    laPrintDate: TmmQRLabel;
    QRSysData2: TmmQRSysData;
    ColumnHeaderBand1: TmmQRBand;
    laGruppe: TmmQRLabel;
    laBezeichnung: TmmQRLabel;
    laAbteilung: TmmQRLabel;
    QRShape1: TmmQRShape;
    QRShape3: TmmQRShape;
    DetailBand1: TmmQRBand;
    QRDBText1: TmmQRDBText;
    QRDBText2: TmmQRDBText;
    QRDBText3: TmmQRDBText;
    QRShape2: TmmQRShape;
    QRShape4: TmmQRShape;
    QRShape5: TmmQRShape;
    QRShape6: TmmQRShape;
    laFarbe: TmmQRLabel;
    shFarbe: TmmQRShape;
    queTeam: TmmADODataSet;
    mmTranslator: TmmTranslator;
    qbFooter: TQRBand;
    qlPageValue: TmmQRSysData;
    mmQRSysData1: TmmQRSysData;
    mmQRImage1: TmmQRImage;
    qlCompanyName: TmmQRLabel;

  procedure queTeamAfterScroll(DataSet: TDataSet);
  procedure FormCreate(Sender: TObject);
  procedure QuickRep1AfterPrint(Sender: TObject);
  private
  public
 end; //TReportTeamsForm

var
 ReportTeamsForm: TReportTeamsForm;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 Printers,u_global, SettingsReader;

{$R *.DFM}

procedure TReportTeamsForm.queTeamAfterScroll(DataSet: TDataSet);
begin
 if gPrintOptions.PrintColors = FALSE then
    shFarbe.Brush.Color := clWhite
 else
   shFarbe.Brush.Color := queTeam.FieldByName('C_TEAM_COLOR').AsInteger;
end; //procedure TReportTeamsForm.teamQAfterScroll
//-----------------------------------------------------------------------------
procedure TReportTeamsForm.FormCreate(Sender: TObject);
begin
 qlPageValue.Text := Translate(qlPageValue.Text) + ' ';

 try
   qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
 except
   qlCompanyName.Caption :='';
 end;

 queTeam.Open;

 //if gPrintOptions.PrintColors = FALSE then begin
    ColumnHeaderBand1.Color := cLightSilver;
 //end;

 QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
 //QuickRep1.PrintBackground;
 QuickRep1.Print;
end; //procedure TReportTeamsForm.FormCreate
//-----------------------------------------------------------------------------
procedure TReportTeamsForm.QuickRep1AfterPrint(Sender: TObject);
begin
 Release;
end; //procedure TReportTeamsForm.QuickRep1AfterPrint

end. //u_report_teams.pas
 
