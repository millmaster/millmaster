(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: ShiftCalendarIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP5
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.07.2001 2.01  Wss | implementation changed to use WrapperForm
| 27.02.2004 2.01  Wss | Floor integration entfernt, da /unregserver weiterhin funktionieren
                         soll, darf die COM-Erweiterung nicht einfach entfernt werden.
|=========================================================================================*)
unit ShiftCalendarIMPL;

interface

uses
  ComObj, ActiveX, shiftcalendar_TLB, StdVcl, LoepfePluginIMPL, LOEPFELIBRARY_TLB;

type
  TShiftCalendarServer = class(TLoepfePlugin, IShiftCalendarServer)
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R 'Toolbar.res'}

uses
  mmMBCS,
  ComServ, Forms, BASEFORM, u_edit_exceptdays, u_edit_shift;

{
type
  TCommandID = (ciFactoryCalendar, ciShiftCalendar);

const
  cFloorMenuDef: Array[0..2] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Kalender'; MenuHint: '(*)Kalender'; MenuStBar:'(*)Kalender'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciFactoryCalendar); SubMenu: 0; MenuBmpName: 'FACTORYCALENDAR';
     MenuText: '(*)Betriebskalender'; MenuHint:'(*)Betriebskalender'; MenuStBar: '(*)Betriebskalender'; //ivlm
     ToolbarText: 'Betriebskalender';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciShiftCalendar); SubMenu: 0; MenuBmpName: 'SHIFTCALENDAR';
     MenuText: '(*)Schichtkalender'; MenuHint: '(*)Schichtkalender'; MenuStBar: '(*)Schichtkalender'; //ivlm
     ToolbarText: 'Schichtkalender';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)
  );
{}

//------------------------------------------------------------------------------
// TShiftCalendar
//------------------------------------------------------------------------------
{}
function TShiftCalendarServer.ExecuteCommand: SYSINT;
{
var
  xShiftForm: TEditShiftDlg;
  xExceptForm: TEditExceptDaysDlg;
  xWrap: TmmForm;
{}
begin
  Result := 0;
{
  case TCommandID(CommandIndex) of
    ciFactoryCalendar: begin
        if GetWrappedDataForm(TEditExceptDaysDlg, TForm(xExceptForm), xWrap) then begin
          Result := xWrap.Handle;
        end;
      end;
    ciShiftCalendar: begin
        if GetWrappedDataForm(TEditShiftDlg, TForm(xShiftForm), xWrap) then begin
          Result := xWrap.Handle;
        end;
      end;
  else
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TShiftCalendarServer.Initialize;
begin
  inherited Initialize;
//  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------

initialization
  InitializeObjectFactory(TShiftCalendarServer, Class_ShiftCalendarServer);
end.
