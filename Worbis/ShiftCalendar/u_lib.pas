(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_lib.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Allgemeine Funktionen und Prozeduren
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 17.11.98 | 0.00 | PW  | Added function MakeUniqueID
| 05.12.98 | 0.01 | PW  | Using MM Components
|=========================================================================================*)

{$i symbols.inc}

unit u_lib;
interface
uses
// Windows, SysUtils, Classes, Graphics, Forms,

 Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,

 Buttons,DB,DBCtrls,Tabs,StdCtrls, WinTypes,
 u_global,u_common_global,
 Mask,ComCtrls,Grids,ShiftCalendars,Printers;

 //function GetWindowOpenMode(woMode: eWindowOpenModeT): eWindowOpenModeT;
 function GetWeekEndDays(aWeekEndDays: integer): sWeekendDayT;
 procedure PrintQuickReport(fClass: TFormClass; var f);
 function GetPrinterOptions(aDefaultOrientation: TPrinterOrientation; aVariante: sPrintOptionsT; aOwner: TComponent): boolean;

 procedure PrintHeaderFooter(aCanvas: TCanvas; aTitle: String);

resourcestring
  cPage     = '(*)Seite'; //ivlm


//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 ExtCtrls,WinProcs,DbiProcs,DbiTypes,DbiErrs,Spin,FileCtrl,Menus,
 u_common_lib,u_set_printoptions,u_main, IvDictio, SettingsReader ;

{$R MMLogoReport.res}

{function GetWindowOpenMode(woMode: eWindowOpenModeT): eWindowOpenModeT;
begin
 if woMode = woModal then result := woModal
                     else result := woSingle;
end; //function GetWindowOpenMode}
//-----------------------------------------------------------------------------
function GetWeekEndDays(aWeekEndDays: integer): sWeekendDayT;
var
 xDay : eWeekendDayT;
 xBit : integer;

begin
 result := [];
 for xDay := wdSunday to wdSaturday do
 begin
  xBit := round(ex(2,ord(xDay)));
  if ((aWeekEndDays or xBit) = aWeekEndDays) then result := result + [xDay];
 end; //for xDay := wdSunday to wdSaturday do
end; //function GetWeekEndDays
//-----------------------------------------------------------------------------
procedure PrintQuickReport(fClass: TFormClass; var f);
begin
 Application.CreateForm(fClass,f);
end; //procedure PrintQuickReport
//-----------------------------------------------------------------------------
function GetPrinterOptions(aDefaultOrientation: TPrinterOrientation; aVariante: sPrintOptionsT; aOwner: TComponent): boolean;
var
  xDlg: TSetPrintOptionsDlg;
begin
 Printer.Orientation := aDefaultOrientation;
 if Assigned(aOwner) and (aOwner is TWinControl) then
   xDlg := TSetPrintOptionsDlg.Create(aOwner, aDefaultOrientation)
 else
   xDlg := TSetPrintOptionsDlg.Create(Application, aDefaultOrientation);

 with xDlg do begin
   Variante := aVariante;
   ShowModal;
   Result := ModalResult = mrOK;
 end; //with TSetPrintOptionsDlg.Create(Application) do
end; //function GetPrinterOptions
//-----------------------------------------------------------------------------
procedure PrintHeaderFooter(aCanvas: TCanvas; aTitle: String);
var xBMP : Graphics.TBitmap;
    xSrcRect: TRect;
    xDestRect: TRect;

    xDPIPrinter, xDPIScreen, xFactor: integer;
    xPos, yPos, xNewWidth, xNewHeight : Integer;
    xPrntPixelHeight, xPrntPixelWidth : Integer;
    xText :String;
    xRes : THandle;
const
  cLogoMaxWidth = 282;
  cLogoMaxHight = 25;
begin

  xDPIPrinter := GetDeviceCaps(Printer.handle, LOGPIXELSX );  //DPI Drucker z.B. 600 dpi
  xDPIScreen  := GetDeviceCaps(GetDC(0), LOGPIXELSX );        //DPI Scree z.B. 72 / 96 dpi
  xFactor     := xDPIPrinter DIV xDPIScreen;

  xPrntPixelHeight := GetDeviceCaps(Printer.handle, VERTRES) - (20 * xFactor);
  xPrntPixelWidth  := GetDeviceCaps(Printer.handle, HORZRES) - (20 * xFactor);

  aCanvas.Font.Color := clBlack;
  aCanvas.Brush.Color:= clWhite;

  //Header
  //++++++

  //Position Titel
  aCanvas.Font.Size := 14;
  aCanvas.Font.Style := [fsBold];

  xText := Translate(aTitle);
  xPos := 0;
  yPos := 10 * xFactor;
  aCanvas.TextOut( xPos , yPos, xText );

  try
    xText := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    xText := '';
  end;

  aCanvas.Font.Size := 10;
  aCanvas.Font.Style := [];
  xPos := xPrntPixelWidth - aCanvas.TextWidth(xText);
  yPos := 13 * xFactor;
  aCanvas.TextOut( xPos , yPos, xText );



  //Footer
  //++++++
  try
    xRes := FindResource( HInstance, 'MMLogoReport.res', 'BitMap' );
    xBMP := Graphics.TBitmap.Create;

    xBMP.Transparent := TRUE;
    xBMP.TransparentMode := tmAuto;

    xBMP.LoadFromResourceName(xRes, 'MMLogoReport');

    xBMP.TransParentColor := xBMP.canvas.pixels[1,1];

    //xBMP.Height := imLogo.Picture.Bitmap.Height;
    //xBMP.Width  := imLogo.Picture.Bitmap.Width;

    //Logo size
    xNewWidth   := (xFactor * cLogoMaxWidth);
    xNewHeight  := (xFactor * cLogoMaxHight);


    xPos := 0;
    yPos := xPrntPixelHeight - xNewHeight ;

    xSrcRect  := Rect(0, 0, xBMP.Width, xBMP.Height);
    xDestRect := Rect(xPos, yPos, xNewWidth + xPos, xNewHeight + yPos);

    aCanvas.CopyRect(xDestRect, xBMP.Canvas, xSrcRect);
  finally
    xBMP.Free;
  end;



  //Position Linie & zeichen
  yPos := xPrntPixelHeight - xFactor * cLogoMaxHight - (2 * xFactor);
  aCanvas.Pen.Width:= 1 * xFactor + 2;
  aCanvas.Pen.Style:= psSolid;
  aCanvas.Pen.Color:= clBlack;
  aCanvas.MoveTo(0, yPos);
  aCanvas.LineTo(xPrntPixelWidth, yPos);


  //Position Datum
  aCanvas.Font.Size := 10;
  aCanvas.Font.Style := [];
  aCanvas.Font.Color := clBlack;
  xText := DateTimeToStr(Now);

  yPos := xPrntPixelHeight - ((xFactor * cLogoMaxHight) Div 2) - (aCanvas.TextHeight(xText) DIV 2);
  xPos := xPrntPixelWidth - aCanvas.TextWidth(xText);
  aCanvas.TextOut( xPos , yPos, xText );

  //Position Seiten Nr.
  xText := cPage + '1' ;
  xPos := (xPrntPixelWidth Div 2)  - (aCanvas.TextWidth(xText) DIV 2);
  aCanvas.TextOut( xPos , yPos, xText );
end;


end. //u_lib.pas

