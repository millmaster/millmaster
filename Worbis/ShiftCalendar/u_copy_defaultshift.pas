(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_copy_defaultshift.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Auswahl-Dialog um Standardschichten zu kopieren. 2 Moeglichkeiten
|                 1. Std.schichten werden in einen gewaehlten Zeitraum hinein kopiert
|                 2. Std.schichten werden ab einem gewaehlten Zeitpunkt bis zum Ende
|                    des Datenbank-Bereiches kopiert
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 29.10.99 | 0.03 | PW  | TToolButton/TToolBar replaced by TButton
|===========================================================================================*)

{$I symbols.inc}

unit u_copy_defaultshift;
interface
uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, ToolWin, DB, Nwcombo, ActnList, mmActionList, mmRadioButton, mmDateTimePicker,
  mmBevel, mmLabel, mmGroupBox, mmPanel, mmToolBar, IvDictio, IvMulti, IvEMulti,
  mmTranslator, BaseForm, mmButton;

type
  TCopyDefaultShiftDlg = class(TmmForm)

    MainPanel: TmmPanel;
    gbPeriod: TmmGroupBox;
    laFrom: TmmLabel;
    laTo: TmmLabel;
    dtFrom: TmmDateTimePicker;
    dtTo: TmmDateTimePicker;
    rbPeriod: TmmRadioButton;
    rbFillToEnd: TmmRadioButton;
    Bevel1: TmmBevel;

    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure rbClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  public
    gCPRec: rCopyShiftT;
  end; //TCopyShiftPatternDlg

var
  CopyDefaultShiftDlg: TCopyDefaultShiftDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,  MMHtmlHelp,

  mmDialogs,
  u_common_lib, u_common_global, u_lib, u_main;

{$R *.DFM}

resourcestring
  cMsgStr1 = '(35)Zeitraum von > Zeitraum bis ?'; //ivde

procedure TCopyDefaultShiftDlg.rbClick(Sender: TObject);
begin
  dtTo.Enabled := TRadioButton(Sender).Tag = 0;
  laTo.Font.Color := GetEnabledFontColor(dtTo.Enabled);
  gCPRec.Variante := TRadioButton(Sender).Tag;
end; //procedure TCopyShiftPatternDlg.rbClick
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    CanClose := dtFrom.Date < dtTo.Date;
    if not CanClose then MMMessageDlg({LTrans{}(cMsgStr1), mtError, [mbOK], 0, Self);
    if CanClose then begin
      gCPRec.Period.fromDate := dtFrom.Date;
      gCPRec.Period.toDate := dtTo.Date;
    end; //if CanClose then
  end //if ModalResult = mrOK then
  else
    CanClose := True;
end; //procedure TCopyShiftPatternDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-kopieren.htm');
  rbClick(rbPeriod);
end; //procedure TCopyShiftPatternDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.acOKExecute(Sender: TObject);
begin
  ModalResult := mrOK;
end; //procedure TCopyDefaultShiftDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end; //procedure TCopyDefaultShiftDlg.acCancelExecute
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TCopyDefaultShiftDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TCopyDefaultShiftDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TCopyDefaultShiftDlg.FormClose

end. //u_copy_defaultshift.pas

