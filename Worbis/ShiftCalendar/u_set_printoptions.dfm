object SetPrintOptionsDlg: TSetPrintOptionsDlg
  Left = 551
  Top = 197
  BorderStyle = bsDialog
  Caption = '(60)Druckoptionen fuer Ausdruck'
  ClientHeight = 245
  ClientWidth = 390
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 390
    Height = 210
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gbCurrentPrinter: TmmGroupBox
      Left = 5
      Top = 5
      Width = 380
      Height = 90
      Caption = '(60)Aktueller Drucker'
      TabOrder = 0
      object laPrintername: TmmLabel
        Left = 10
        Top = 20
        Width = 82
        Height = 13
        Caption = '(20)Druckername'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laOrientation: TmmLabel
        Left = 10
        Top = 40
        Width = 77
        Height = 13
        Caption = '(20)Seitenformat'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laCopies: TmmLabel
        Left = 10
        Top = 60
        Width = 82
        Height = 13
        Caption = '(20)Kopienanzahl'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laPrinternameValue: TmmLabel
        Left = 100
        Top = 20
        Width = 98
        Height = 13
        Caption = '(20)Druckername'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laOrientationValue: TmmLabel
        Left = 100
        Top = 40
        Width = 94
        Height = 13
        Caption = '(20)Seitenformat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laCopiesValue: TmmLabel
        Left = 100
        Top = 60
        Width = 99
        Height = 13
        Caption = '(20)Kopienanzahl'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bPrinter: TmmButton
        Left = 295
        Top = 15
        Width = 75
        Height = 25
        Action = acPrinter
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object gbAdvancedSettings: TmmGroupBox
      Left = 5
      Top = 100
      Width = 380
      Height = 50
      Caption = '(60)Erweiterte Druck-Einstellungen'
      TabOrder = 1
      object cbPrintColors: TmmCheckBox
        Left = 10
        Top = 20
        Width = 280
        Height = 17
        Caption = '(60)Ausdruck in Farbe bzw. Graustufen'
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object gbShiftCalRange: TmmGroupBox
      Left = 5
      Top = 155
      Width = 380
      Height = 50
      Caption = '(60)Druckbereich festlegen'
      TabOrder = 2
      object Label1: TmmLabel
        Left = 180
        Top = 20
        Width = 20
        Height = 21
        Alignment = taCenter
        AutoSize = False
        Caption = '-'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Layout = tlCenter
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object comFromDate: TNWComboBox
        Left = 10
        Top = 20
        Width = 170
        Height = 21
        Style = csDropDownList
        DropDownCount = 12
        ItemHeight = 13
        TabOrder = 0
      end
      object comToDate: TNWComboBox
        Left = 200
        Top = 20
        Width = 170
        Height = 21
        Style = csDropDownList
        DropDownCount = 12
        ItemHeight = 13
        TabOrder = 1
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 210
    Width = 390
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 150
      Top = 5
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 230
      Top = 5
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 310
      Top = 5
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 293
    Top = 79
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0))
  end
  object mmPrinterSetupDialog: TmmPrinterSetupDialog
    Left = 333
    Top = 79
  end
  object mmActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 293
    Top = 111
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Eingaben uebernehmen, Dialog beenden'
      ImageIndex = 34
      ShortCut = 16397
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 35
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acPrinter: TAction
      Caption = '(9)&Drucker...'
      Hint = '(*)Druckereinstellungen aufrufen...'
      ImageIndex = 43
      OnExecute = acPrinterExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
  end
end
