(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_printgrid.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Routinen zum Ausdruck von Gittern
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
|=========================================================================================*)

{$i symbols.inc}

unit u_printgrid;
interface
uses
 Graphics,Windows,SysUtils,Printers;

const
 MaxCols = 255;
 MaxRows = 255;

type
 TAlignmentItem = (taLeft,taCenter,taRight,taTop,taBottom);

 TPrinterInfoRecord = record
  xMM,
  yMM,
  xPix,
  yPix,
  xPixInch,
  yPixInch,
  xPhysPix,
  yPhysPix,
  xOffset,
  yOffset    : longint;
  xPixMM,
  yPixMM     : double;
 end; //TPrinterInfoRecord

 TGridPrint = class(TObject)
  private
   FCols: integer;
   FLeft: integer;
   FLeftMM: double;
   FRows: integer;
   FTop: integer;
   FTopMM: double;
   FRowHeight: array[1..MaxRows] of integer;
   FColWidth: array[1..MaxCols] of integer;
   procedure SetCols(Value: integer);
   procedure SetLeftMM(Value: double);
   procedure SetRows(Value: integer);
   procedure SetTopMM(Value: double);
  protected
  public
   PInfo: TPrinterInfoRecord;
   constructor Create;
   destructor Destroy; override;
   function GetColPos(aCol: integer): integer;
   function GetFontHeight(fHeight: integer): integer;
   function GetGridWidth: double; //mm
   function GetGridHeight: double; //mm
   function GetRowPos(aRow: integer): integer;
   function GetRect(aCol,aRow: integer): TRect;
   function GetXPix(xMM: double): integer;
   function GetYPix(yMM: double): integer;
   procedure AlignTextOut(x1mm,y1mm,x2mm,y2mm: double; xAlign,yAlign: TAlignmentItem; s: string);
   procedure CellText(aCol,aRow: integer; xAlign,yAlign: TAlignmentItem; s: string);
   procedure FillCell(aCol,aRow: integer);
   procedure FillCellText(aCol,aRow: integer; xAlign,yAlign: TAlignmentItem; s: string; DrawText: boolean);
   procedure InitPrinterInfo;
   procedure LineTo(xMM,yMM: double);
   procedure MoveTo(xMM,yMM: double);
   procedure PrintCellText(r: TRect; xAlign,yAlign: TAlignmentItem; s: string);
   procedure SetColWidth(aCol: integer; ColMM: double);
   procedure SetBrush(aColor: TColor; aStyle: TBrushStyle);
   procedure SetPen(aColor: TColor; aStyle: TPenStyle; aWidth: integer);
   procedure SetFont(aColor: TColor; aStyle: TFontStyles; aHeight: integer);
   procedure SetFontName(aName: TFontName);
   procedure SetRowHeight(aRow: integer; RowMM: double);
   procedure TextOut(xMM,yMM: double; const Text: string);
   property Cols: integer read FCols write SetCols default 0;
   property LeftMM: double read FLeftMM write SetLeftMM;
   property Rows: integer read FRows write SetRows default 0;
   property TopMM: double read FTopMM write SetTopMM;
 end; //TGridPrint

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

constructor TGridPrint.Create;
begin
 inherited Create;

 InitPrinterInfo;

 FLeftMM := 0;
 FTopMM := 0;
 FCols := 0;
 FRows := 0;
 FLeft := 0;
 FTop := 0;

 SetPen(clBlack,psSolid,1);
 SetBrush(clWhite,bsSolid);
 SetFontname('Arial');
end; //constructor TGridPrint.Create
//-----------------------------------------------------------------------------
destructor TGridPrint.Destroy;
begin
 inherited Destroy;
end; //destructor TGridPrint.Destroy
//-----------------------------------------------------------------------------
procedure TGridPrint.InitPrinterInfo;
begin
 with PInfo do
 begin
  xMM := GetDeviceCaps(Printer.Handle,HORZSIZE);
  yMM := GetDeviceCaps(Printer.Handle,VERTSIZE);
  xPix := GetDeviceCaps(Printer.Handle,HORZRES);
  yPix := GetDeviceCaps(Printer.Handle,VERTRES);
  xPixInch := GetDeviceCaps(Printer.Handle,LOGPIXELSX);
  yPixInch := GetDeviceCaps(Printer.Handle,LOGPIXELSY);
  xPhysPix := GetDeviceCaps(Printer.Handle,PHYSICALWIDTH);
  yPhysPix := GetDeviceCaps(Printer.Handle,PHYSICALHEIGHT);
  xOffset := GetDeviceCaps(Printer.Handle,PHYSICALOFFSETX);
  yOffset := GetDeviceCaps(Printer.Handle,PHYSICALOFFSETY);
  xPixMM := xPix /xMM;
  yPixMM := yPix /yMM;
 end; //with PInfo do
end; //procedure TGridPrint.InitPrinterInfo
//-----------------------------------------------------------------------------
procedure TGridPrint.SetCols(Value: integer);
begin
 if FCols <> Value then FCols := Value;
 if FCols < 0 then FCols := 0;
 if FCols > MaxCols then FCols := MaxCols;
end; //procedure TGridPrint.SetCols
//-----------------------------------------------------------------------------
procedure TGridPrint.SetLeftMM(Value: double);
begin
 if FLeftMM <> Value then FLeftMM := Value;
 FLeft := GetXPix(FLeftMM);
end; //procedure TGridPrint.SetLeftMM
//-----------------------------------------------------------------------------
procedure TGridPrint.SetRows(Value: integer);
begin
 if FRows <> Value then FRows := Value;
 if FRows < 0 then FRows := 0;
 if FRows > MaxRows then FRows:= MaxRows;
end; //procedure TGridPrint.SetRows
//-----------------------------------------------------------------------------
procedure TGridPrint.SetTopMM(Value: double);
begin
 if FTopMM <> Value then FTopMM := Value;
 FTop := GetXPix(FTopMM);
end; //procedure TGridPrint.SetTop
//-----------------------------------------------------------------------------
function TGridPrint.GetXPix(xMM: double): integer;
begin
 result := round(xMM *PInfo.xPixMM);
end; //function TGridPrint.GetXPix
//-----------------------------------------------------------------------------
function TGridPrint.GetYPix(yMM: double): integer;
begin
 result := round(yMM *PInfo.yPixMM);
end; //function TGridPrint.GetYPix
//-----------------------------------------------------------------------------
procedure TGridPrint.SetColWidth(aCol: integer; ColMM: double);
begin
 FColWidth[aCol] := GetXPix(ColMM);
end; //procedure SetColWidth
//-----------------------------------------------------------------------------
procedure TGridPrint.SetRowHeight(aRow: integer; RowMM: double);
begin
 FRowHeight[aRow] := GetYPix(RowMM);
end; //procedure TGridPrint.SetRowHeight
//-----------------------------------------------------------------------------
procedure TGridPrint.MoveTo(xMM,yMM: double);
begin
 Printer.Canvas.MoveTo(GetXPix(xMM),GetYPix(yMM));
end; //procedure TGridPrint.MoveTo
//-----------------------------------------------------------------------------
procedure TGridPrint.LineTo(xMM,yMM: double);
begin
 Printer.Canvas.LineTo(GetXPix(xMM),GetYPix(yMM));
end; //procedure TGridPrint.LineTo
//-----------------------------------------------------------------------------
procedure TGridPrint.TextOut(xMM,yMM: double; const Text: string);
begin
 Printer.Canvas.TextOut(GetXPix(xMM),GetYPix(yMM),Text);
end; //procedure TGridPrint.TextOut
//-----------------------------------------------------------------------------
procedure TGridPrint.AlignTextOut(x1mm,y1mm,x2mm,y2mm: double; xAlign,yAlign: TAlignmentItem; s: string);
var
 r : TRect;

begin
 r.Left := GetXPix(x1mm);
 r.Top := GetYPix(y1mm);
 r.Right := GetXPix(x2mm);
 r.Bottom := GetYPix(y2mm);
 PrintCellText(r,xAlign,yAlign,s);
end; //procedure TGridPrint.AlignTextOut
//-----------------------------------------------------------------------------
function TGridPrint.GetColPos(aCol: integer): integer;
var
 i : integer;

begin
 result := FLeft;
 for i := 1 to (aCol -1) do
  inc(result,FColWidth[i]);
end; //function TGridPrint.GetColPos
//-----------------------------------------------------------------------------
function TGridPrint.GetGridWidth: double;
var
 i : integer;

begin
 result := 0;
 for i := 1 to FCols do
  result := result +(FColWidth[i] /PInfo.xPixMM);
end; //function TGridPrint.GetGridWidth
//-----------------------------------------------------------------------------
function TGridPrint.GetGridHeight: double;
var
 i : integer;

begin
 result := 0;
 for i := 1 to FRows do
  result := result +(FRowHeight[i] /PInfo.yPixMM);
end; //function TGridPrint.GetGridHeight
//-----------------------------------------------------------------------------
function TGridPrint.GetRowPos(aRow: integer): integer;
var
 i : integer;

begin
 result := FTop;
 for i := 1 to (aRow -1) do
  inc(result,FRowHeight[i]);
end; //function TGridPrint.GetRowPos
//-----------------------------------------------------------------------------
function TGridPrint.GetRect(aCol,aRow: integer): TRect;
begin
 result.Left := GetColPos(aCol);
 result.Right := result.Left +FColWidth[aCol];
 result.Top := GetRowPos(aRow);
 result.Bottom := result.Top +FRowHeight[aRow];
end; //function TGridPrint.GetRect
//-----------------------------------------------------------------------------
function TGridPrint.GetFontHeight(fHeight: integer): integer;
const
 cDevelopRes = 600; //dpi

var
 yRes : double;

begin
 yRes := PInfo.yPixInch /cDevelopRes;
 result := round(yRes *fHeight);
end; //function TGridPrint.GetFontHeight
//-----------------------------------------------------------------------------
procedure TGridPrint.SetPen(aColor: TColor; aStyle: TPenStyle; aWidth: integer);
begin
 with Printer.Canvas.Pen do
 begin
  Color := aColor;
  Style := aStyle;
  Width := aWidth;
 end; //with Printer.Canvas.Pen do
end; //procedure TGridPrint.SetPen
//-----------------------------------------------------------------------------
procedure TGridPrint.SetBrush(aColor: TColor; aStyle: TBrushStyle);
begin
 with Printer.Canvas.Brush do
 begin
  Color := aColor;
  Style := aStyle;
 end; //with Printer.Canvas.Brush do
end; //procedure TGridPrint.SetBrush
//-----------------------------------------------------------------------------
procedure TGridPrint.SetFont(aColor: TColor; aStyle: TFontStyles; aHeight: integer);
begin
 with Printer.Canvas.Font do
 begin
  Color := aColor;
  Style := aStyle;
  Height := GetFontHeight(aHeight);
 end; //with Printer.Canvas.Font do
end; //procedure TGridPrint.SetFont
//-----------------------------------------------------------------------------
procedure TGridPrint.SetFontName(aName: TFontName);
begin
 Printer.Canvas.Font.Name := aName;
end; //procedure TGridPrint.SetFontName
//-----------------------------------------------------------------------------
procedure TGridPrint.FillCell(aCol,aRow: integer);
var
 r : TRect;

begin
 with Printer.Canvas do
 begin
  r := GetRect(aCol,aRow);
  Rectangle(r.Left,r.Top,r.Right,r.Bottom);
 end; //with Printer.Canvas do
end; //procedure TGridPrint.FillCell
//-----------------------------------------------------------------------------
procedure TGridPrint.PrintCellText(r: TRect; xAlign,yAlign: TAlignmentItem; s: string);
var
 x,y,
 dy,
 wText,
 hText  : integer;

begin
 with Printer do
 begin
  wText := Canvas.TextWidth(s);
  hText := Canvas.TextHeight(s);

  case xAlign of
   taLeft: x := r.Left +GetXPix(0.1);
   taCenter: begin
    x := r.Right -r.Left;
    x := r.Left +(x -wtext) div 2;
   end; //taCenter
   taRight: x := r.Right -wtext -GetXPix(0.1);
   else x := 0;
  end; //case xAlign of

  dy := r.Bottom -r.Top;

  case yAlign of
   taTop:  y := r.Top +GetXPix(0.1);
   taCenter: y := r.Top +(dy -htext) div 2;
   taBottom: y := r.Bottom -hText -GetXPix(0.1);
   else y := 0;
  end; //case yAlign of

  Canvas.TextOut(x,y,s);
 end; //with Printer do
end; //procedure TGridPrint.CellText
//-----------------------------------------------------------------------------
procedure TGridPrint.CellText(aCol,aRow: integer; xAlign,yAlign: TAlignmentItem; s: string);
begin
 PrintCellText(GetRect(aCol,aRow),xAlign,yAlign,s);
end; //procedure TGridPrint.CellText
//-----------------------------------------------------------------------------
procedure TGridPrint.FillCellText(aCol,aRow: integer; xAlign,yAlign: TAlignmentItem; s: string; DrawText: boolean);
begin
 FillCell(aCol,aRow);
 if DrawText then CellText(aCol,aRow,xAlign,yAlign,s);
end; //procedure TGridPrint.FillCellText

end. //u_printgrid.pas
