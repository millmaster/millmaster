(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_exceptdays.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bearbeiten der Betriebskalender. Arbeitsfreie Tage koennen markiert
|                 werden.
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 14.03.00 | 1.00 | Wss | EnableActions changed to work with MMSecurityControl
| 20.04.00 | 1.00 | Wss | Ctrl+S added to acSaveChanges
| 17.05.00 | 1.00 | Wss | acRangeExecute: Da nicht in der Vergangenheit geaendert werden kann,
                                          werden die Datum auf HEUTE gesetzt
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.2002       LOK  | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_exceptdays;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs, Grids,
  StdCtrls, DB, Buttons, ExtCtrls, Nwcombo, ComCtrls, ToolWin, Menus, ShiftCalendars,
  ActnList, mmMainMenu, mmActionList, mmPopupMenu, mmToolBar, mmPanel,
  BaseForm, ImgList, IvDictio, IvMulti, IvEMulti, mmTranslator,
  MMSecurity, mmSpeedButton, mmADODataset, LoepfeGlobal;

type
  TEditExceptDaysDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bFactoryCal: TToolButton;
    bRange: TToolButton;
    bNext: TToolButton;
    ToolButton8: TToolButton;
    bPrev: TToolButton;
    ToolButton10: TToolButton;
    bPrint: TToolButton;
    bHelp: TToolButton;
    bOptions: TToolButton;
    ToolButton14: TToolButton;
    cobFactoryCal: TNWComboBox;
    bSave: TToolButton;
    ToolButton1: TToolButton;

    MainPanel: TmmPanel;
    JanHeaderPanel: TmmPanel;
    FebHeaderPanel: TmmPanel;
    MarHeaderPanel: TmmPanel;
    AprHeaderPanel: TmmPanel;
    MayHeaderPanel: TmmPanel;
    JunHeaderPanel: TmmPanel;
    JulHeaderPanel: TmmPanel;
    AugHeaderPanel: TmmPanel;
    DecHeaderPanel: TmmPanel;
    NovHeaderPanel: TmmPanel;
    OctHeaderPanel: TmmPanel;
    SepHeaderPanel: TmmPanel;
    fcJan: TFactoryCalendar;
    fcFeb: TFactoryCalendar;
    fcMar: TFactoryCalendar;
    fcApr: TFactoryCalendar;
    fcMay: TFactoryCalendar;
    fcJun: TFactoryCalendar;
    fcJul: TFactoryCalendar;
    fcAug: TFactoryCalendar;
    fcSep: TFactoryCalendar;
    fcOct: TFactoryCalendar;
    fcNov: TFactoryCalendar;
    fcDez: TFactoryCalendar;

    pmYearPlus: TmmPopupMenu;
    miYear_Plus_1: TMenuItem;
    miYear_Plus_2: TMenuItem;
    miYear_Plus_3: TMenuItem;
    miYear_Plus_4: TMenuItem;
    miYear_Plus_5: TMenuItem;

    pmYearMinus: TmmPopupMenu;
    miYear_Minus_1: TMenuItem;
    miYear_Minus_2: TMenuItem;
    miYear_Minus_3: TMenuItem;
    miYear_Minus_4: TMenuItem;
    miYear_Minus_5: TMenuItem;

    ActionList1: TmmActionList;
    acClose: TAction;
    acSaveChanges: TAction;
    acEditFactoryCal: TAction;
    acRange: TAction;
    acEditOptions: TAction;
    acPrint: TAction;
    acPreviousYear: TAction;
    acNextYear: TAction;
    acHelp: TAction;
    acSecurity: TAction;

    mmSpeedButton1: TmmSpeedButton;
    MMSecurityControl: TMMSecurityControl;
    mmTranslator: TmmTranslator;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cobFactoryCalChange(Sender: TObject);
    procedure YearPopUpClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveChangesExecute(Sender: TObject);
    procedure acEditFactoryCalExecute(Sender: TObject);
    procedure acRangeExecute(Sender: TObject);
    procedure acEditOptionsExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acPreviousYearExecute(Sender: TObject);
    procedure acNextYearExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure UpdateFormCaption;
    procedure ReadFromKalender;
    procedure WriteToKalender;
    procedure Fill_cobFactoryCal;
    function SaveQuery_OK: boolean;
    procedure EnableActions;
    procedure TextToTranslate;
  public
    procedure SetBetriebsKalender(aID: longint);
    procedure Init;
    procedure TranslateText; //override;
  end; //TEditExceptDaysDlg

var
  EditExceptDaysDlg: TEditExceptDaysDlg;

//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,  MMHtmlHelp,

  mmDialogs, printers,
  u_common_lib, u_common_global, u_main, u_global, u_lib, u_set_factorycal_range,
  u_edit_options, u_edit_shift, u_edit_factorycalendar, u_printgrid,
  u_dmShiftCalendar;

{$R *.DFM}

resourcestring
  cCaption_Dialog = '(100)Betriebskalender - [%s - %d]'; //ivlm
  cMsgStr1 = '(255)Der Kalender wurde geaendert. Moechten Sie Ihre Eingaben sichern ?'; //ivlm

//------------------------------------------------------------------------------
procedure TEditExceptDaysDlg.Init;
var
  i: integer;
  fCal: TFactoryCalendar;

begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TFactoryCalendar then begin
      fCal := TFactoryCalendar(Components[i]);
      with fCal.CalendarColors, gProgOptions do begin
        DayTextColor := YearCalendarColors[ciDay, cpText];
        DayColor := YearCalendarColors[ciDay, cpBackground];
        OffDayTextColor := YearCalendarColors[ciOffDay, cpText];
        OffDayColor := YearCalendarColors[ciOffDay, cpBackground];
        TitleTextColor := YearCalendarColors[ciTitle, cpText];
        TitleColor := YearCalendarColors[ciTitle, cpBackground];
        WeekEndTextColor := YearCalendarColors[ciWeekEnd, cpText];
        WeekEndColor := YearCalendarColors[ciWeekEnd, cpBackground];
      end; //with fCal.CalendarColors, gProgOptions do

      fCal.StartOfWeek := eWeekendDayT(gProgOptions.StartOfWeek - 1);
      fCal.WeekEndDays := GetWeekEndDays(gProgOptions.WeekEndDays);
      fCal.ShowWeekEndDays := gProgOptions.ShowCalWeekEnds;
    end; //if Components[i] is TFactoryCalendar then
end; //procedure TEditExceptDaysDlg.Init
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.UpdateFormCaption;
begin
  Caption := Format({LTrans{}(cCaption_Dialog), [cobFactoryCal.GetItemText, fcJan.Year]);
end; //TEditExceptDaysDlg.UpdateFormCaption
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.ReadFromKalender;
var
  i: integer;
  fCal: TFactoryCalendar;

begin
  UpdateFormCaption;

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TFactoryCalendar then begin
      fCal := TFactoryCalendar(Components[i]);
      fCal.Month := TFactoryCalendar(Components[i]).Month;
      fCal.Year := fcJan.Year;
      fCal.CalendarID := cobFactoryCal.GetItemID;
      fCal.ReadExceptDays;
    end; //if Components[i] is TFactoryCalendar then

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TMenuItem then
      if pos('miYear_', TMenuItem(Components[i]).Name) = 1 then
        TMenuItem(Components[i]).Caption := inttostr(fcJan.Year + TMenuItem(Components[i]).Tag);
end; //procedure TEditExceptDaysDlg.ReadFromKalender
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.WriteToKalender;
var
  i: integer;

begin
  dmShiftCalendar.conDefault.BeginTrans;
  try
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TFactoryCalendar then
        TFactoryCalendar(Components[i]).WriteExceptDays;
    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e: Exception do begin
      dmShiftCalendar.conDefault.RollbackTrans;
      WriteDebugLog('[TEditExceptDaysDlg.WriteToKalender]', e.Message);
    end; // on e:Exception do begin
  end; //try..except

  for i := 0 to Screen.FormCount - 1 do
    if Screen.Forms[i] is TEditShiftDlg then begin
      TEditShiftDlg(Screen.Forms[i]).kspGrid.ReadOffDays;
      TEditShiftDlg(Screen.Forms[i]).kspGrid.Refresh;
    end; //if Screen.Forms[i] is TEditShiftDlg then
end; //procedure TEditExceptDaysDlg.WriteToKalender
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Betriebskalender\BETRKAL_Fenster_Betriebskalender.htm');
  fcJan.Year := GetCurrentYear;
//  Init;  is already in OnFormShow
  Fill_cobFactoryCal;
  cobFactoryCal.ItemIndex := 0;
  EnableActions;
end; //procedure TEditExceptDaysDlg.FormCreate
//------------------------------------------------------------------------------
procedure TEditExceptDaysDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.FormShow(Sender: TObject);
begin
  ReadFromKalender;
  Init;
  cobFactoryCal.OnChange := cobFactoryCalChange;
  Screen.Cursor := crDefault;
end; //procedure TEditExceptDaysDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.Fill_cobFactoryCal;
begin
  cobFactoryCal.Items.Clear;

  with TmmAdoDataSet.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT c_factory_calendar_id,c_factory_calendar_name FROM t_factory_calendar';
    Open;
    while not Eof do begin
      cobFactoryCal.AddObj(FieldByName('c_factory_calendar_name').Value, FieldByName('c_factory_calendar_id').Value);
      Next;
    end; //while not Eof do
  finally
    free;
  end; // with TmmAdoDataSet.Create(nil)
end; //procedure TEditExceptDaysDlg.Fill_cobFactoryCal
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.cobFactoryCalChange(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  try
    if not SaveQuery_OK then begin
      cobFactoryCal.OnChange := nil;
      try
        cobFactoryCal.SetItemID(fcJan.CalendarID);
      finally
        cobFactoryCal.OnChange := cobFactoryCalChange;
      end; //try..finally
      Exit;
    end; //if not SaveQuery_OK then

    ReadFromKalender;
  finally
    Screen.Cursor := crDefault;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.cobFactoryCalChange
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.SetBetriebsKalender(aID: longint);
begin
  cobFactoryCal.SetItemID(aID);
end; //procedure TEditExceptDaysDlg.SetBetriebsKalender
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.YearPopUpClick(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  try
    if not SaveQuery_OK then exit;
    fcJan.Year := fcJan.Year + TMenuItem(Sender).Tag;
    ReadFromKalender;
  finally
    Screen.Cursor := crDefault;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.YearPopUpClick
//-----------------------------------------------------------------------------
function TEditExceptDaysDlg.SaveQuery_OK: boolean;

  //-------
  function SaveQuery: TModalResult;
  begin
    Result := MMMessageDlg({LTrans{}(cMsgStr1), mtConfirmation, [mbYes, mbNo, mbCancel], 0, Self);
  end; //function SaveQuery
  //-------
  function CalendarEdited: boolean;
  var
    i: integer;

  begin
    Result := False;
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TFactoryCalendar then begin
        Result := TFactoryCalendar(Components[i]).IsDifferent;
        if Result then
          break;
      end;
//        if not result then result := TFactoryCalendar(Components[i]).IsDifferent;
  end; //function CalendarEdited
  //-------

begin //main
  Result := True;
  if CalendarEdited then begin
    case SaveQuery of
      mrYes: WriteToKalender;
      mrCancel: Result := False;
      mrNo: begin
          ReadFromKalender;
          Result := True;
        end; //mrNo
    end; //case SaveQuery of
  end; //if CalendarEdited then
end; //function TEditExceptDaysDlg.SaveQuery_OK
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := SaveQuery_OK;
end; //procedure TEditExceptDaysDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditExceptDaysDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acSaveChangesExecute(Sender: TObject);
begin
  WriteToKalender;
end; //procedure TEditExceptDaysDlg.ActionSaveExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acEditFactoryCalExecute(Sender: TObject);
var
  xID: longint;

begin
  if not SaveQuery_OK then exit;

// with TEditFactoryCalendarDlg.Create(Application) do
  with TEditFactoryCalendarDlg.Create(Self) do
    ShowModal;

  cobFactoryCal.OnChange := nil;
  try
    xID := cobFactoryCal.GetItemID;
    Fill_cobFactoryCal;
    if cobFactoryCal.GetItemIDPos(xID) > -1 then
      cobFactoryCal.SetItemID(xID) //deleted ??
    else
      cobFactoryCal.ItemIndex := 0;
  finally
    cobFactoryCal.OnChange := cobFactoryCalChange;
    ReadFromKalender;
    EnableActions;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.ActionEditFactoryCalExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acRangeExecute(Sender: TObject);
begin
  if not SaveQuery_OK then exit;

// with TSetFactoryCalRangeDlg.Create(Application) do
  with TSetFactoryCalRangeDlg.Create(Self) do begin
{ Da nicht in der Vergangenheit geaendert werden kann, werden die Datum auf HEUTE gesetzt
  dtVon.date := Encodedate(fcJan.Year,1,1);
  dtBis.date := Encodedate(fcJan.Year,1,1);
{}
    dtVon.date := Now;
    dtBis.date := Now;
    SetKalenderID(cobFactoryCal.GetItemID);
    ShowModal;
  end; //with TSetFactoryCalRangeDlg.Create(Application) do
  ReadFromKalender;
end; //procedure TEditExceptDaysDlg.ActionRangeExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acEditOptionsExecute(Sender: TObject);
begin
// with TEditOptionsDlg.Create(Application) do
  with TEditOptionsDlg.Create(Self) do begin
    SetVisibleTabs([1, 2]);
    ShowModal;
  end; //with TEditOptionsDlg.Create(Application) do
end; //procedure TEditExceptDaysDlg.ActionOptionsExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acPrintExecute(Sender: TObject);
const
  cDefColWidth = 6.5; //mm

var
  i,
    xCol,
    xRow: integer;
  xCalWidth: double;
  xPG: TGridPrint;
  cInfo: rCellInfoT;
  fCal: TFactoryCalendar;

  //---------
  function GetTopPos(aMonth: word): integer;
  begin
{
    case aMonth of
      1..4: Result := 35;
      5..8: Result := 80;
    else
      Result := 125;
    end; //case aMonth of
}
    if Printer.Orientation = poPortrait	then  //SDO
       case aMonth of
        1..3   : Result := 35;
        4..6   : Result := 80;
        7..9   : Result := 125;
        10..12 : Result := 170;
      end
    else
       case aMonth of
        1..4: Result := 35;
        5..8: Result := 80;
       else
        Result := 125;
       end;

  end; //function GetTopPos
  //---------
  function GetLeftPos(aMonth: word): integer;
  var xPos : Integer;
  const c_DistancePortrait  = 50;
        c_DistanceLandscape = 60;
        cLeftPosPortrait    = 10;
        cLeftPosLandscape   = 30;
  begin
    {
    case aMonth of
      1, 5, 9: Result := 32;
      2, 6, 10: Result := 92;
      3, 7, 11: Result := 152;
    else
      Result := 212;
    end; //case (aMonth mod 4) of
    }

    if Printer.Orientation = poPortrait	then  //SDO
        case aMonth of
         1, 4, 7, 10 : Result := cLeftPosPortrait;
         2, 5, 8, 11 : Result := cLeftPosPortrait + c_DistancePortrait;
         3, 6, 9, 12 : Result := cLeftPosPortrait + (c_DistancePortrait * 2);
        end
     else
       case aMonth of
         1, 5, 9  : Result := cLeftPosLandscape;
         2, 6, 10 : Result := cLeftPosLandscape + c_DistanceLandscape;
         3, 7, 11 : Result := cLeftPosLandscape + (c_DistanceLandscape * 2);
         4, 8, 12 : Result := cLeftPosLandscape + (c_DistanceLandscape * 3);
       end;
  end; //function GetLeftPos
  //---------

begin //main
//  if not GetPrinterOptions(polandscape, [poPrintColors], Self) then exit;
  if not GetPrintOptions(poPortrait, Self, gPrintOptions.PrintColors) then exit;
  WriteToKalender;

  xPG := TGridPrint.Create;
  try
    xPG.InitPrinterInfo;

    xPG.Cols := cDaysPerWeek;
    xPG.Rows := 7;
    xPG.LeftMM := 10;
    xPG.LeftMM := 0;

    for xCol := 1 to xPG.Cols do
      xPG.SetColWidth(xCol, cDefColWidth);

    for xRow := 1 to xPG.Rows do
      xPG.SetRowHeight(xRow, 5);

    xCalWidth := cDefColWidth * xPG.Cols;

    Printer.BeginDoc;
    xPG.MoveTo(xPG.LeftMM, 20);
{
    xPG.LineTo(xPG.PInfo.xMM - xPG.LeftMM, 20);
    xPG.SetFont(clBlack, [fsBold], 90);
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taCenter, taBottom, caption);

    xPG.SetFont(clBlack, [], 70);
//Wss   xPG.AlignTextOut(xPG.LeftMM,0,xPG.PInfo.xMM -xPG.LeftMM,19,taRight,taBottom,formatdatetime_('dd/mm/yyyy  hh:nn',now));
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taRight, taBottom, formatdatetime(ShortDateFormat + '  hh:nn', now));
    xPG.SetFont(clBlack, [fsBold], 120);
    xPG.TextOut(xPG.LeftMM, xPG.PInfo.yMM - 15, 'LOEPFE MILLMASTER');
}
    for i := 0 to ComponentCount - 1 do
      if Components[i] is TFactoryCalendar then begin
        fCal := TFactoryCalendar(Components[i]);

        xPG.TopMM := GetTopPos(fCal.Month);
        xPG.LeftMM := GetLeftPos(fCal.Month);

        xPG.SetFont(clBlack, [], 90);
        xPG.SetBrush(clWhite, bsSolid);
        xPG.AlignTextOut(xPG.LeftMM, xPG.TopMM - 8, xPG.LeftMM + xCalWidth, xPG.TopMM - 2, taCenter, taBottom, GetLongMonthNames(fCal.Month));

        for xCol := 1 to xPG.Cols do
          for xRow := 1 to xPG.Rows do begin
            cInfo := fCal.GetCellInfoAt(xCol - 1, xRow - 1);
            if not gPrintOptions.PrintColors then begin
              cInfo.fColor := clBlack;
              cInfo.bColor := clWhite;
              if (xRow = 1) then cInfo.bColor := cLightSilver;
            end; //if not gPrintOptions.PrintColors then
            xPG.SetFont(cInfo.fColor, cInfo.fStyle, 80);
            xPG.SetBrush(cInfo.bColor, bsSolid);
            xPG.FillCellText(xCol, xRow, taCenter, taCenter, cInfo.cString, True);
          end; //for xRow := 1 to xPG.Rows do
      end; //if Components[i] is TFactoryCalendar then

    PrintHeaderFooter( Printer.Canvas , Caption );

    Printer.EndDoc;
  finally
    xPG.Free;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acPreviousYearExecute(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  try
    if not SaveQuery_OK then exit;
    fcJan.Year := fcJan.Year - 1;
    ReadFromKalender;
  finally
    Screen.Cursor := crDefault;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.ActionPreviousYearExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acNextYearExecute(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  try
    if not SaveQuery_OK then exit;
    fcJan.Year := fcJan.Year + 1;
    ReadFromKalender;
  finally
    Screen.Cursor := crDefault;
  end; //try..finally
end; //procedure TEditExceptDaysDlg.ActionNextYearExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditExceptDaysDlg.acHelpContentExecute
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.EnableActions;
var
  i: integer;
  xCalNr: Integer;
begin
  xCalNr := GetRecordCount('t_factory_calendar', '');

  acRange.Enabled := MMSecurityControl.CanEnabled(acRange) and (xCalNr > 0);
  acPrint.Enabled := MMSecurityControl.CanEnabled(acPrint) and (xCalNr > 0);
  acPreviousYear.Enabled := MMSecurityControl.CanEnabled(acPreviousYear) and (xCalNr > 0);
  acNextYear.Enabled := MMSecurityControl.CanEnabled(acNextYear) and (xCalNr > 0);
  cobFactoryCal.Enabled := MMSecurityControl.CanEnabled(cobFactoryCal) and (xCalNr > 0);
  acEditFactoryCal.Enabled := MMSecurityControl.CanEnabled(acEditFactoryCal);
  acSaveChanges.Enabled := MMSecurityControl.CanEnabled(acSaveChanges);

  for i := 0 to ComponentCount - 1 do
    if Components[i] is TFactoryCalendar then
      TFactoryCalendar(Components[i]).Enabled := acSaveChanges.Enabled;
{
 xCanEdit := not fcJan.ReadOnly;
 acRange.Enabled := xCanEdit and (GetRecordCount('t_factory_calendar','') > 0);
 acPrint.Enabled := GetRecordCount('t_factory_calendar','') > 0;
 acPreviousYear.Enabled := acPrint.Enabled;
 acNextYear.Enabled := acPrint.Enabled;
 cobFactoryCal.Enabled := acPrint.Enabled;
 acEditFactoryCal.Enabled := xCanEdit;
 acSaveChanges.Enabled := xCanEdit;

 for i := 0 to ComponentCount -1 do
  if Components[i] is TFactoryCalendar then
   TFactoryCalendar(Components[i]).Enabled := acRange.Enabled;
{}
end; //procedure TEditExceptDaysDlg.EnableActions
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.TextToTranslate;
var
  i: integer;

begin
  for i := 0 to ComponentCount - 1 do
    if Components[i] is TFactoryCalendar then
      TFactoryCalendar(Components[i]).Refresh;
  UpdateFormCaption;
end; //procedure TEditExceptDaysDlg.TextToTranslate
//-----------------------------------------------------------------------------
procedure TEditExceptDaysDlg.TranslateText;
begin
  TextToTranslate;
// inherited TranslateText;
end; //procedure TEditExceptDaysDlg.TranslateText
//------------------------------------------------------------------------------
procedure TEditExceptDaysDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
end. //u_edit_exceptdays.pas

