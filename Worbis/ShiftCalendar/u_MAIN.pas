(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_MAIN.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.01.1999 0.00  PW  | Datei erstellt
|            0.01  PW  |
| 01.02.1999 0.02  PW  | Anpassungen an neue MDI-Templates
| 04.03.1999 0.03  Wss | adapted to creating child forms of new template of
| 13.05.99   0.02  PW  | units u_common_global and u_common_lib added
| 11.04.2000 0.03  SDo | Neue Sprachen Span., Port., Ital.
| 28.10.02         LOK | Umbau ADO
| 27.02.04         Wss | Beim Start wird direkt ein Schichtkalender Fenster erzeugt
|=========================================================================================*)

unit u_main;
{$I symbols.inc}

interface
uses
  Windows, Messages, SysUtils, StdCtrls, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, mmLogin, MMSecurity,
  Menus, mmPopupMenu, mmOpenDialog, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, mmMainMenu,
  StdActns, ActnList, mmActionList, ImgList, mmImageList, ComCtrls,
  mmStatusBar, ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, MMMessages,
  IvBinDic, IvDictio, IvMulti, IvEMulti, IvAMulti, IvMlDlgs, mmDictionary, mmTranslator,
  u_global, mmButton, ShiftCalendars, MMHtmlHelp;

type
  TfrmMain = class(TBaseApplMainForm)
    acEditShift: TAction;
    acEditCalendar: TAction;
    miEditShift: TMenuItem;
    miEditCalendar: TMenuItem;
    MMSecurityDB: TMMSecurityDB;
    MMHtmlHelp: TMMHtmlHelp;
    procedure acEditCalendarExecute(Sender: TObject);
    procedure acEditShiftExecute(Sender: TObject);
    procedure acPreViewExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  protected
    procedure InitStandalone; override;
  public
  end; //TMainForm

var
  frmMain: TfrmMain;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  BaseForm, LoepfeGlobal, u_lib, u_readwrite_options, u_edit_exceptdays,
  u_edit_shift, u_common_lib, u_common_global, u_dmShiftCalendar;

{$R *.DFM}

resourcestring
  rsProductName          = '(20)Schichtkalender'; //ivlm
  rsConnectError         = '(45)Keine Verbindung zum Datenbank-Server: %s'; //ivlm

//------------------------------------------------------------------------------
procedure TfrmMain.acEditCalendarExecute(Sender: TObject);
var
  xForm: TEditExceptDaysDlg;
begin
  GetCreateChildForm(TEditExceptDaysDlg, TForm(xForm));
end; //procedure TMainForm.acEditCalendarExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acEditShiftExecute(Sender: TObject);
var
  xForm: TEditShiftDlg;
begin
  GetCreateChildForm(TEditShiftDlg, TForm(xForm));
  xForm.WindowState := wsMaximized;
end; //procedure TMainForm.acEditShiftExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acPreViewExecute(Sender: TObject);
begin
  acNewExecute(Sender);
end; //procedure TMainForm.acPreViewExecute
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  xAliasName: string;
  xUserName,
    xPassword: string;

begin
  HelpContext                := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definition.htm');
  acPrint.HelpContext        := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-drucken.htm');
  acEditCalendar.HelpContext := GetHelpContext('WindingMaster\Betriebskalender\BETRKAL-definition.htm');


  ProductName := rsProductName;
 // if parameters overrides the default settings
  xAliasName := GetParamStr('/a');
  if xAliasName <> '' then gDBAliasNameODBC := xAliasName;

  xUsername := GetParamStr('/u');
  if xUsername <> '' then gDBUsername := xUsername;

  xPassword := GetParamStr('/p');
  if xPassword <> '' then gDBPassword := xPassword;

 // call inherited
  inherited;

 // this application need an open database
  try
    dmShiftCalendar.conDefault.Connected := True;
  except
    MessageDlg(Format(rsConnectError, [dmShiftCalendar.conDefault.ConnectionString]), mtError, [mbOK], 0);
    Application.Terminate;
  end; //try..except

 // fill in gProgOptions
  ReadOptions;

 // fill in gPrintOptions: copy subpart of gProgOptions to gPrintOptions
  fillchar(gPrintOptions, sizeof(gPrintOptions), 0);
  gPrintOptions.PrintColors := gProgOptions.PrintColors;
end; //procedure TMainForm.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  inherited;
  Application.OnHelp := nil;
end; //procedure TMainForm.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmMain.InitStandalone;
begin
  inherited InitStandalone;

  Application.HintPause := 0;

  //Neu, Oeffnen und Separatoren im Menu auf unsichtbar stellen
  acNew.Visible := False;
  acOpen.Visible := False;
  acSave.Visible := False;
  acSaveas.Visible := False;
  acPreView.Visible := False;
  acPrinterSetup.Visible := False;
  acPrint.Visible := False;
  N11.Visible := False;
  N21.Visible := False;
  N31.Visible := False;
  N41.Visible := False;
end; //procedure TMainForm.InitStandalone
//-----------------------------------------------------------------------------
procedure TfrmMain.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;
//------------------------------------------------------------------------------

procedure TfrmMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  with TEditShiftDlg.Create(Self) do
    WindowState := wsMaximized;
end;

end. //u_main.pas

