(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_set_printoptions.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog zum Einstellen von Druckoptionen, wird vor dem Ausdruck von
|                 Berichten, Listen aufgerufen
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
|=========================================================================================*)

{$I symbols.inc}

unit u_set_printoptions;
interface
uses
  printers,
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  ExtCtrls, ActnList, ComCtrls, ToolWin, IvDictio, IvMulti, IvEMulti, IvMlDlgs,
  mmActionList, mmPrinterSetupDialog, mmTranslator, mmPanel, mmToolBar,
  mmUpDown, mmEdit, mmCheckBox, mmLabel, mmGroupBox, BaseForm, mmButton, Nwcombo;

type
  TSetPrintOptionsDlg = class(TmmForm)
    MainPanel: TmmPanel;
    gbCurrentPrinter: TmmGroupBox;
    laPrintername: TmmLabel;
    laOrientation: TmmLabel;
    laCopies: TmmLabel;
    laPrinternameValue: TmmLabel;
    laOrientationValue: TmmLabel;
    laCopiesValue: TmmLabel;
    gbAdvancedSettings: TmmGroupBox;
    cbPrintColors: TmmCheckBox;
    gbShiftCalRange: TmmGroupBox;
    Label1: TmmLabel;

    mmTranslator: TmmTranslator;
    mmPrinterSetupDialog: TmmPrinterSetupDialog;

    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    acPrinter: TAction;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;
    bPrinter: TmmButton;
    comFromDate: TNWComboBox;
    comToDate: TNWComboBox;

    procedure FormShow(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acPrinterExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure Init;
    procedure ReadPrinterInfo;
  public
    Variante: sPrintOptionsT;
    constructor Create(aOwner: TComponent; aDefaultOrientation: TPrinterOrientation); virtual;
  end; //u_set_printoptions

var
  SetPrintOptionsDlg: TSetPrintOptionsDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,
  u_common_lib, u_common_global, u_lib, u_main;

{$R *.DFM}

resourcestring
  cPortraitStr = '(20)Hochformat'; //ivlm
  cLandscapeStr = '(20)Querformat'; //ivlm

procedure TSetPrintOptionsDlg.Init;
var
  i,
    xTop: integer;
  xMonth,
    xYear: word;

begin
  xTop := 100;

  gbAdvancedSettings.Visible := poPrintColors in Variante;
  if gbAdvancedSettings.Visible then begin
    inc(xTop, gbAdvancedSettings.Height + 5);
    cbPrintColors.Checked := gPrintOptions.PrintColors;
  end; //if gbAdvancedSettings.Visible then

  gbShiftCalRange.Visible := poShiftCalRange in Variante;
  if gbShiftCalRange.Visible then begin
    gbShiftCalRange.Top := xTop;
    inc(xTop, gbShiftCalRange.Height + 5);

    comFromDate.Items.Clear;
    comToDate.Items.Clear;
    for i := SetYearMonth(GetCurrentYear, GetCurrentMonth) - 12 to SetYearMonth(GetCurrentYear, GetCurrentMonth) + 12 do begin
      xMonth := GetMonthFromYM(i);
      xYear := GetYearFromYM(i);
      comFromDate.AddObj(Format('%s %d', [GetLongMonthNames(xMonth), xYear]), i);
    end; //for i := SetYearMonth(GetCurrentYear,GetCurrentMonth -12) to SetYearMonth(GetCurrentYear,GetCurrentMonth +12) do
    comToDate.Items.AddStrings(comFromDate.Items);

    if gPrintOptions.FromYearMonth < 1 then begin
      comFromDate.SetItemID(SetYearMonth(GetCurrentYear, 1));
      comToDate.SetItemID(SetYearMonth(GetCurrentYear, 12));
    end //if gPrintOptions.FromMonth < 1 then
    else begin
      comFromDate.SetItemID(gPrintOptions.FromYearMonth);
      comToDate.SetItemID(gPrintOptions.ToYearMonth);
    end; //else if gPrintOptions.FromMonth < 1 then
  end; //if gbShiftCalRange.Visible then

  ClientHeight := xTop + paButtons.Height;
end; //procedure TSetPrintOptionsDlg.Init
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.ReadPrinterInfo;
begin
  with Printer do begin
    laPrinternameValue.Caption := Printers[PrinterIndex];
    case Orientation of
      poLandscape: laOrientationValue.Caption := {LTrans{}(cLandscapeStr);
      poPortrait: laOrientationValue.Caption := {LTrans{}(cPortraitStr);
    end; //case Orientation of

    laCopiesValue.Caption := inttostr(Copies);
  end; //with Printer do
end; //procedure TSetPrintOptionsDlg.ReadPrinterInfo
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.FormShow(Sender: TObject);
begin
  Init;
  ReadPrinterInfo;
end; //procedure TSetPrintOptionsDlg.FormShow
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acOKExecute(Sender: TObject);
begin
  if poPrintColors in Variante then
    gPrintOptions.PrintColors := cbPrintColors.Checked;

  if poShiftCalRange in Variante then begin
    gPrintOptions.FromYearMonth := comFromDate.GetItemID;
    gPrintOptions.ToYearMonth := comToDate.GetItemID;
  end; //if gbShiftCalRange.Visible then

  Close;
  ModalResult := mrOK;
end; //procedure TSetPrintOptionsDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acCancelExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end; //procedure TSetPrintOptionsDlg.ActionCancelExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acPrinterExecute(Sender: TObject);
begin
  mmPrinterSetupDialog.Execute;
  ReadPrinterInfo;
end; //procedure TSetPrintOptionsDlg.ActionPrinterExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TSetPrintOptionsDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end; //procedure TSetPrintOptionsDlg.FormClose
//------------------------------------------------------------------------------
constructor TSetPrintOptionsDlg.Create(aOwner: TComponent; aDefaultOrientation: TPrinterOrientation);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-drucken.htm');
  Printer.Orientation := poPortrait;
end;
//------------------------------------------------------------------------------
end. //u_set_printoptions.pas

