inherited EditShiftCalendarDlg: TEditShiftCalendarDlg
  Left = 295
  Top = 103
  Width = 502
  Height = 221
  BorderIcons = [biSystemMenu]
  Caption = '(60)Schichtkalender bearbeiten'
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 494
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bPrint: TToolButton
      Left = 34
      Top = 0
      Action = acPrint
    end
    object ToolButton8: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object DBNavigator1: TmmDBNavigator
      Left = 65
      Top = 0
      Width = 92
      Height = 22
      DataSource = srcShiftCal
      VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object ToolButton14: TToolButton
      Left = 157
      Top = 0
      Width = 8
      ImageIndex = 8
      Style = tbsDivider
    end
    object bHelp: TToolButton
      Left = 165
      Top = 0
      Action = acHelp
    end
  end
  object mDBGrid: TmmDBGrid
    Left = 0
    Top = 26
    Width = 494
    Height = 168
    Align = alClient
    DataSource = srcShiftCal
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnCellClick = mDBGridCellClick
    Columns = <
      item
        Expanded = False
        FieldName = 'c_shiftcal_name'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Kalender'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Muster'
        Visible = True
      end>
  end
  object tabShiftCal: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    BeforePost = tabShiftCalBeforePost
    AfterPost = tabShiftCalAfterDeletePost
    BeforeDelete = tabShiftCalBeforeDelete
    AfterDelete = tabShiftCalAfterDeletePost
    OnNewRecord = tabShiftCalNewRecord
    CommandText = 't_shiftcal'
    CommandType = cmdTable
    IndexFieldNames = 'C_SHIFTCAL_NAME'
    Parameters = <>
    Left = 16
    Top = 72
    object tabShiftCalc_shiftcal_name: TStringField
      DisplayLabel = '(20)Schichtkalender'
      DisplayWidth = 25
      FieldName = 'c_shiftcal_name'
      Size = 30
    end
    object tabShiftCalKalender: TStringField
      DisplayLabel = '(20)Betriebskalender'
      DisplayWidth = 25
      FieldKind = fkLookup
      FieldName = 'Kalender'
      LookupDataSet = queCALENDAR
      LookupKeyFields = 'c_factory_calendar_id'
      LookupResultField = 'c_factory_calendar_name'
      KeyFields = 'c_factory_calendar_id'
      Size = 40
      Lookup = True
    end
    object tabShiftCalMuster: TStringField
      DisplayLabel = '(40)Standard - Schichtmuster'
      DisplayWidth = 24
      FieldKind = fkLookup
      FieldName = 'Muster'
      LookupDataSet = quePATTERN
      LookupKeyFields = 'c_shift_pattern_id'
      LookupResultField = 'c_shift_pattern_name'
      KeyFields = 'c_default_pattern_id'
      Size = 30
      Lookup = True
    end
    object tabShiftCalc_shiftcal_id: TSmallintField
      DisplayWidth = 10
      FieldName = 'c_shiftcal_id'
      Visible = False
    end
    object tabShiftCalc_default_pattern_id: TSmallintField
      DisplayWidth = 16
      FieldName = 'c_default_pattern_id'
      Visible = False
    end
    object tabShiftCalc_factory_calendar_id: TSmallintField
      DisplayWidth = 18
      FieldName = 'c_factory_calendar_id'
      Visible = False
    end
  end
  object srcShiftCal: TmmDataSource
    DataSet = tabShiftCal
    Left = 56
    Top = 72
  end
  object queDELETE: TmmADOCommand
    Connection = dmShiftCalendar.conDefault
    Parameters = <>
    Left = 96
    Top = 72
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 198
    Top = 10
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
  end
  object queCALENDAR: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    CommandText = 
      'SELECT c_factory_calendar_id,c_factory_calendar_name FROM t_fact' +
      'ory_calendar ORDER BY c_factory_calendar_name'
    Parameters = <>
    Left = 16
    Top = 104
  end
  object quePATTERN: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    CommandText = 
      'SELECT c_shift_pattern_id,c_shift_pattern_name FROM t_shift_patt' +
      'ern_name ORDER BY c_shift_pattern_name'
    Parameters = <>
    Left = 16
    Top = 136
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 208
    Top = 80
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Hints'
        0)
      (
        '*'
        'DisplayLabel'
        0))
  end
  object dsCalendar: TmmDataSource
    DataSet = queCALENDAR
    Left = 56
    Top = 104
  end
end
