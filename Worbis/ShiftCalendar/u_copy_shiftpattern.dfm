inherited CopyShiftPatternDlg: TCopyShiftPatternDlg
  Left = 510
  Top = 220
  ActiveControl = bOK
  BorderStyle = bsDialog
  Caption = 'CopyShiftPatternDlg'
  ClientHeight = 260
  ClientWidth = 270
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 270
    Height = 225
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gbPeriod: TmmGroupBox
      Left = 5
      Top = 5
      Width = 260
      Height = 215
      Caption = '(50)Zeitraum / Schichtmuster'
      TabOrder = 0
      object laFrom: TmmLabel
        Left = 10
        Top = 139
        Width = 130
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '(25)uebernehmen ab'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laTo: TmmLabel
        Left = 10
        Top = 164
        Width = 130
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '(25)uebernehmen bis'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laCycles: TmmLabel
        Left = 10
        Top = 189
        Width = 130
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '(25)Zyklen'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object Bevel1: TmmBevel
        Left = 10
        Top = 125
        Width = 240
        Height = 3
        Shape = bsTopLine
      end
      object Bevel2: TmmBevel
        Left = 10
        Top = 50
        Width = 240
        Height = 3
        Shape = bsTopLine
      end
      object dtFrom: TmmDateTimePicker
        Left = 150
        Top = 135
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 36094
        Time = 36094
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 4
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object dtTo: TmmDateTimePicker
        Left = 150
        Top = 160
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 36094
        Time = 36094
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 5
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object rbPeriod: TmmRadioButton
        Left = 10
        Top = 60
        Width = 240
        Height = 17
        Caption = '(50)Zeitraum angeben (von - bis)'
        Checked = True
        TabOrder = 1
        TabStop = True
        OnClick = rbClick
      end
      object rbFillToEnd: TmmRadioButton
        Tag = 1
        Left = 10
        Top = 80
        Width = 240
        Height = 17
        Caption = '(50)Auffuellen bis Ende Schichtkalender'
        TabOrder = 2
        OnClick = rbClick
      end
      object rbCycles: TmmRadioButton
        Tag = 2
        Left = 10
        Top = 100
        Width = 240
        Height = 17
        Caption = '(50)Anzahl Zyklen angeben'
        TabOrder = 3
        OnClick = rbClick
      end
      object edCycles: TmmEdit
        Left = 150
        Top = 185
        Width = 84
        Height = 21
        Color = clWindow
        ReadOnly = True
        TabOrder = 6
        Text = '1'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object udCycles: TmmUpDown
        Left = 234
        Top = 185
        Width = 15
        Height = 21
        Associate = edCycles
        Min = 1
        Max = 1000
        Position = 1
        TabOrder = 7
        Wrap = False
      end
      object cobPattern: TNWComboBox
        Left = 10
        Top = 20
        Width = 240
        Height = 21
        Hint = '(*)Muster auswaehlen'
        Style = csDropDownList
        DropDownCount = 12
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        Sorted = True
        TabOrder = 0
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 225
    Width = 270
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 30
      Top = 5
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 110
      Top = 5
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 190
      Top = 5
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 210
    Top = 18
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Eingaben uebernehmen, Dialog beenden'
      ImageIndex = 34
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 35
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 213
    Top = 55
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
