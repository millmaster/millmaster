(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_defaultshift.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bearbeiten von Std.schichten.
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using mmDatabase Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 20.04.00 | 1.00 | Wss | Ctrl+S added to acSaveChanges
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.2002       LOK  | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_defaultshift;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Grids, ExtCtrls,
  DBCtrls, ComCtrls, ToolWin, Db, ShiftCalendars, StdCtrls, Nwcombo, Printers, ActnList,
  mmActionList, mmPanel, mmToolBar, IvDictio, IvMulti, IvEMulti, BASEFORM, u_dmShiftCalendar,
  mmTranslator, mmADODataSet, LoepfeGlobal;

type
  TEditDefaultShiftDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    bSave: TToolButton;
    ToolButton8: TToolButton;
    bHelp: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    bPrint: TToolButton;
    cobShiftcal: TNWComboBox;

    MainPanel: TmmPanel;
    dsGrid: TDefaultShiftGrid;

    ActionList1: TmmActionList;
    acClose: TAction;
    acSaveChanges: TAction;
    acPrint: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cobShiftcalChange(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acCloseExecute(Sender: TObject);
    procedure acSaveChangesExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure IvExtendedTranslator1LanguageChange(Sender: TObject);
  private
    function SaveQuery_OK: boolean;
    function WriteToDefaultShift: boolean;
  public
    procedure SetKalenderID(aID: longint);
    procedure Fill_nameCombo;
    procedure Init;
  end; //TEditDefaultShiftDlg

var
  EditDefaultShiftDlg: TEditDefaultShiftDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs,
  u_common_lib, u_common_global, u_main, u_lib, u_global, u_edit_shift, u_printgrid;

{$R *.DFM}

resourcestring
  cDateStr = '(8)Datum'; //ivlm
  cMsgStr1 = '(255)Die Standardschichten wurden geaendert. Moechten Sie Ihre Eingaben sichern ?'; //ivlm
  cCaption_Print = 'Standardschichten - [%s]'; //ivlm

procedure TEditDefaultShiftDlg.Init;
begin
  with dsGrid.CalendarColors, gProgOptions do begin
    DayTextColor := ShiftCalendarColors[ciDay, cpText];
    DayColor := ShiftCalendarColors[ciDay, cpBackground];
    TimeTextColor := ShiftCalendarColors[ciTime, cpText];
    TimeColor := ShiftCalendarColors[ciTime, cpBackground];
    TitleTextColor := ShiftCalendarColors[ciTitle, cpText];
    TitleColor := ShiftCalendarColors[ciTitle, cpBackground];
    WeekEndTextColor := ShiftCalendarColors[ciWeekEnd, cpText];
    WeekEndColor := ShiftCalendarColors[ciWeekEnd, cpBackground];
  end; //with dsGrid.CalendarColors, gProgOptions do

  with dsGrid do begin
    CalendarMessages.DateStr := {LTrans{}(cDateStr);
    MaxShiftsPerDay := gProgOptions.ShiftsPerDay;
    WeekEndDays := GetWeekEndDays(gProgOptions.WeekEndDays);
  end; //with dsGrid do
end; //procedure TEditDefaultShiftDlg.Init
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-bearbeiten.htm');
  dsGrid.ShowWeekEndDays := gProgOptions.ShowShiftWeekEnds;
  Init;
  Fill_nameCombo;
// HelpContext := hcEditDefaultShift;
end; //procedure TEditDefaultShiftDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.Fill_nameCombo;
begin
  with TmmAdoDataSet.Create(nil) do try
    cobShiftcal.OnChange := nil;
    cobShiftcal.Items.Clear;
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT c_shiftcal_id, c_shiftcal_name FROM t_shiftcal';
    Open;
    while not Eof do begin
      cobShiftcal.AddObj(FieldByName('c_shiftcal_name').AsString, FieldByName('c_shiftcal_id').AsInteger);
      Next;
    end; //while not Eof do
  finally
    cobShiftcal.OnChange := cobShiftcalChange;
    free;
  end; // with TmmAdoDataSet.Create(nil)
end; //procedure TEditDefaultShiftDlg.Fill_nameCombo
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.SetKalenderID(aID: longint);
begin
  dsGrid.DatabaseConnection := dmShiftCalendar.conDefault;
  dsGrid.CalendarID := aID;
  dsGrid.ReadShiftData;
  cobShiftcal.SetItemID(aID);
end; //procedure TEditDefaultShiftDlg.SetKalenderID
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.FormShow(Sender: TObject);
begin
  Screen.Cursor := crDefault;
end; //procedure TEditDefaultShiftDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditDefaultShiftDlg.FormClose
//-----------------------------------------------------------------------------
function TEditDefaultShiftDlg.WriteToDefaultShift: boolean;
var
  i: integer;

begin
  Result := False;
  dmShiftCalendar.conDefault.BeginTrans;
  try
    Result := dsGrid.WriteShiftData;
    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e: Exception do begin
      WriteDebugLog('[TEditDefaultShiftDlg.WriteToDefaultShift]', e.Message);
      dmShiftCalendar.conDefault.RollbackTrans;
      exit;
    end; // on e:Exception do begin
  end; //try..except

  for i := 0 to Screen.FormCount - 1 do
    if Screen.Forms[i] is TEditShiftDlg then begin
      TEditShiftDlg(Screen.Forms[i]).kspGrid.ReadDefaultShifts;
      TEditShiftDlg(Screen.Forms[i]).kspGrid.Refresh;
    end; //if Screen.Forms[i] is TEditShiftDlg then
end; //function TEditDefaultShiftDlg.WriteToDefaultShift
//-----------------------------------------------------------------------------
function TEditDefaultShiftDlg.SaveQuery_OK: boolean;

//-------
  function SaveQuery: TModalResult;
  begin
    Result := MMMessageDlg({LTrans{}(cMsgStr1), mtConfirmation, [mbYes, mbNo, mbCancel], 0, Self);
  end; //function SaveQuery
//-------

begin //main
  Result := True;
  if dsGrid.IsDifferent then begin
    case SaveQuery of
      mrYes: Result := WriteToDefaultShift;
      mrCancel: Result := False;
    end; //case SaveQuery of
  end; //if dsGrid.IsDifferent  then
end; //function TEditDefaultShiftDlg.SaveQuery_OK
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.cobShiftcalChange(Sender: TObject);
begin
  if not SaveQuery_OK then begin
    cobShiftcal.OnChange := nil;
    try
      cobShiftcal.SetItemID(dsGrid.CalendarID);
    finally
      cobShiftcal.OnChange := cobShiftcalChange;
    end; //try..finally
    Exit;
  end; //if not SaveQuery_OK then

  SetKalenderID(cobShiftcal.GetItemID);
end; //procedure TEditDefaultShiftDlg.cobShiftcalChange
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := SaveQuery_OK;
end; //procedure TEditDefaultShiftDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditDefaultShiftDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.acSaveChangesExecute(Sender: TObject);
begin
  WriteToDefaultShift;
end; //procedure TEditDefaultShiftDlg.ActionSaveChangesExecute
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.acPrintExecute(Sender: TObject);
var
  xCol,
    xRow: integer;
  xPG: TGridPrint;
  cInfo: rCellInfoT;

begin
//  if not GetPrinterOptions(poPortrait, [poPrintColors], Self) then exit;
  if not GetPrintOptions(poPortrait, Self, gPrintOptions.PrintColors) then exit;

  if not WriteToDefaultShift then exit;

  xPG := TGridPrint.Create;
  try
    xPG.InitPrinterInfo;

    xPG.Cols := dsGrid.MaxShiftsPerDay + 1;
    xPG.Rows := 8;
    xPG.LeftMM := 10;
    xPG.TopMM := 25;

    for xCol := 1 to xPG.Cols do
      case xCol of
        1: xPG.SetColWidth(xCol, 25);
      else
        xPG.SetColWidth(xCol, 15);
      end; //case i of

    for xRow := 1 to 8 do
      xPG.SetRowHeight(xRow, 6);

    Printer.BeginDoc;
    xPG.MoveTo(xPG.LeftMM, 20);
    xPG.LineTo(xPG.PInfo.xMM - xPG.LeftMM, 20);
   (*
    xPG.SetFont(clBlack, [fsBold], 90);
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taCenter, taBottom, Format({LTrans{}(cCaption_Print), [cobShiftcal.GetItemText]));
    xPG.SetFont(clBlack, [], 70);
//Wss   xPG.AlignTextOut(xPG.LeftMM,0,xPG.PInfo.xMM -xPG.LeftMM,19,taRight,taBottom,formatdatetime_('dd/mm/yyyy  hh:nn',now));
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taRight, taBottom, formatdatetime(ShortDateFormat + '  hh:nn', now));
    xPG.SetFont(clBlack, [fsBold], 120);
    xPG.TextOut(xPG.LeftMM, xPG.PInfo.yMM - 15, 'LOEPFE MILLMASTER');
    *)

    PrintHeaderFooter( Printer.Canvas ,Format( cCaption_Print, [cobShiftcal.GetItemText]) );

    for xCol := 1 to xPG.Cols do
      for xRow := 1 to xPG.Rows do begin
        cInfo := dsGrid.GetCellInfoAt(xCol - 1, xRow - 1);
        if not gPrintOptions.PrintColors then begin
            cInfo.fColor := clBlack;
            cInfo.bColor := clWhite;
            if (xCol = 1) or (xRow = 1) then cInfo.bColor := cLightSilver;
        end;                          //if not gPrintOptions.PrintColors then

        xPG.SetFont(cInfo.fColor, cInfo.fStyle, 90);
        xPG.SetBrush(cInfo.bColor, bsSolid);
        xPG.FillCellText(xCol, xRow, taCenter, taCenter, cInfo.cString, True);
      end; //for xRow := 1 to xPG.Rows do
    Printer.EndDoc;
  finally
    xPG.Free;
  end; //try..finally
end; //procedure TEditDefaultShiftDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditDefaultShiftDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TEditDefaultShiftDlg.IvExtendedTranslator1LanguageChange(Sender: TObject);
begin
  dsGrid.Refresh;
end; //procedure TEditDefaultShiftDlg.IvExtendedTranslator1LanguageChange

end. //u_edit_defaultshift.pas

