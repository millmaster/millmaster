inherited EditFactoryCalendarDlg: TEditFactoryCalendarDlg
  Left = 486
  Top = 115
  Width = 488
  Height = 279
  ActiveControl = DBGrid1
  BorderIcons = [biSystemMenu]
  Caption = '(50)Betriebskalender erstellen, bearbeiten'
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 480
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bPrint: TToolButton
      Left = 34
      Top = 0
      Action = acPrint
    end
    object ToolButton8: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object DBNavigator1: TmmDBNavigator
      Left = 65
      Top = 0
      Width = 100
      Height = 22
      DataSource = sourceFactoryCal
      VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object ToolButton14: TToolButton
      Left = 165
      Top = 0
      Width = 8
      ImageIndex = 8
      Style = tbsDivider
    end
    object bHelp: TToolButton
      Left = 173
      Top = 0
      Action = acHelp
    end
  end
  object DBGrid1: TmmDBGrid
    Left = 0
    Top = 26
    Width = 480
    Height = 226
    Align = alClient
    DataSource = sourceFactoryCal
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnKeyDown = DBGrid1KeyDown
    Columns = <
      item
        Expanded = False
        FieldName = 'C_FACTORY_CALENDAR_NAME'
        Title.Caption = '(20)Betriebskalender'
        Width = 174
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'C_REMARK'
        Title.Caption = '(20)Bemerkung'
        Width = 270
        Visible = True
      end>
  end
  object tabFactoryCal: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    BeforePost = tabFactoryCalBeforePost
    AfterPost = tabFactoryCalAfterDeletePost
    BeforeDelete = tabFactoryCalBeforeDelete
    AfterDelete = tabFactoryCalAfterDeletePost
    OnNewRecord = tabFactoryCalNewRecord
    CommandText = 'T_FACTORY_CALENDAR'
    CommandType = cmdTable
    IndexFieldNames = 'C_FACTORY_CALENDAR_NAME'
    Parameters = <>
    Left = 16
    Top = 74
    object tabFactoryCalC_FACTORY_CALENDAR_ID: TSmallintField
      FieldName = 'C_FACTORY_CALENDAR_ID'
      Visible = False
    end
    object tabFactoryCalC_FACTORY_CALENDAR_NAME: TStringField
      FieldName = 'C_FACTORY_CALENDAR_NAME'
      Size = 40
    end
    object tabFactoryCalC_REMARK: TStringField
      FieldName = 'C_REMARK'
      Size = 100
    end
  end
  object sourceFactoryCal: TmmDataSource
    DataSet = tabFactoryCal
    Left = 48
    Top = 73
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 201
    Top = 12
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 48
    Top = 112
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Hints'
        0))
  end
end
