(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_shiftpattern.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Erstellen und bearbeiten von Schichtmustern
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.02 |      | LOK | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 01.11.02 |      | LOK | Mit SetPatternIDFromShiftID() kann das aktuelle Schichtmuster gesetzt werden
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_shiftpattern;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Grids, ShiftCalendars,
  ComCtrls, StdCtrls, ExtCtrls, ToolWin, Db, DBCtrls, ActnList, mmActionList, mmTimer,
  mmDataSource, mmUpDown, mmEdit, mmGroupBox, mmPanel, mmToolBar, Mask, mmDBEdit,
  IvDictio, IvMulti, IvEMulti, BASEFORM, mmTranslator, mmDBNavigator, u_dmShiftCalendar, ADODB,
  mmADODataSet, mmImage;

type
  TEditShiftPatternDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton8: TToolButton;
    bHelp: TToolButton;
    ToolButton1: TToolButton;
    bPrint: TToolButton;
    bShowTeam: TToolButton;
    ToolButton2: TToolButton;
    DBNavigator1: TmmDBNavigator;

    MainPanel: TmmPanel;
    gbPeriod: TmmGroupBox;
    edPeriod: TmmEdit;
    udPeriod: TmmUpDown;
    gbPattern: TmmGroupBox;
    spGrid: TShiftPatternGrid;
    gbPatternName: TmmGroupBox;
    edPatternName: TmmDBEdit;

    sourceShiftPat: TmmDataSource;
    tabShiftPat: TmmADODataSet;
    tabShiftPatC_SHIFT_PATTERN_ID: TSmallintField;
    tabShiftPatC_SHIFT_PATTERN_NAME: TStringField;

    RefreshTimer: TmmTimer;

    ActionList1: TmmActionList;
    acClose: TAction;
    acShowTeam: TAction;
    acPrint: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;
    imLogo: TmmImage;

    procedure udPeriodClick(Sender: TObject; Button: TUDBtnType);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tabShiftPatNewRecord(DataSet: TDataSet);
    procedure tabShiftPatAfterScroll(DataSet: TDataSet);
    procedure tabShiftPatBeforeInsert(DataSet: TDataSet);
    procedure tabShiftPatBeforeDelete(DataSet: TDataSet);
    procedure RefreshTimerTimer(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acCloseExecute(Sender: TObject);
    procedure acShowTeamExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure tabShiftPatAfterPost(DataSet: TDataSet);
    procedure tabShiftPatBeforePost(DataSet: TDataSet);
    procedure tabShiftPatAfterCancel(DataSet: TDataSet);
    procedure spGridChange(Sender: TObject);
    procedure IvExtendedTranslator1LanguageChange(Sender: TObject);
  private
    procedure SetPatternID(aID: longint);
    procedure UpdateFormCaption;
    function WriteDataToDB: boolean;
    procedure EnableActions;
  public
    procedure Init;
    procedure SetPatternIDFromShiftID(aID: longint);
  end; //TEditShiftPatternDlg




var
  EditShiftPatternDlg: TEditShiftPatternDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs, Printers,
  u_common_lib, u_common_global, u_main, u_lib, u_global, u_printgrid,
  LoepfeGlobal, SettingsReader, u_report_factorycalendar;

{$R *.DFM}

resourcestring
  cCaption_Dialog = '(60)Schichtmuster [%s] bearbeiten'; //ivlm
  cDefaultStr = '(30)Standard - not named yet'; //ivlm
  cMsgStr1 = '(255)Schichtmuster ist noch mit Schichtkalender verbunden. Loeschen nicht moeglich !'; //ivlm
  cMsgStr2 = '(255)Letztes Schichtmuster kann nicht geloescht werden !'; //ivlm
  cMsgStr3 = '(255)Soll das Schichtmuster mit der Bezeichnung "%s" geloescht werden ?'; //ivlm
  cTitleStr = '(60)Schichtmuster [%s]'; //ivlm


//------------------------------------------------------------------------------






procedure TEditShiftPatternDlg.Init;
begin
  with spGrid.CalendarColors, gProgOptions do begin
    DayTextColor := ShiftCalendarColors[ciDay, cpText];
    DayColor := ShiftCalendarColors[ciDay, cpBackground];
    TimeTextColor := ShiftCalendarColors[ciTime, cpText];
    TimeColor := ShiftCalendarColors[ciTime, cpBackground];
    TitleTextColor := ShiftCalendarColors[ciTitle, cpText];
    TitleColor := ShiftCalendarColors[ciTitle, cpBackground];
  end; //with dsGrid.CalendarColors, gProgOptions do

  with spGrid do
    MaxShiftsPerDay := gProgOptions.ShiftsPerDay;
     
 {
 xQuery := TmmQuery.Create(Application);
 try
  xQuery.DatabaseName := gDataBaseName;
  xQuery.SQL.Clear;
  xQuery.SQL.Add('SET IDENTITY_INSERT t_shift_pattern_name ON');
  try
   xQuery.ExecSQL;
  except
  end; //try..except
 finally
  xQuery.Free;
 end; //try..finally
 }
end; //procedure TEditShiftPatternDlg.Init
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.UpdateFormCaption;
begin
  Caption := Format({{LTrans{}{}(cCaption_Dialog), [tabShiftPatC_SHIFT_PATTERN_NAME.Value]);
end; //procedure TEditShiftPatternDlg.UpdateFormCaption
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.SetPatternID(aID: longint);
begin
  spGrid.ShiftPatternID := aID;
  spGrid.ReadShiftPatternData;
  udPeriod.Position := spGrid.ShiftPatternPeriod;
  spGrid.Refresh;
  UpdateFormCaption;
end; //procedure TEditShiftPatternDlg.SetPatternID
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.udPeriodClick(Sender: TObject; Button: TUDBtnType);
begin
  if not (tabShiftPat.State in [dsEdit, dsInsert]) then tabShiftPat.Edit;
  spGrid.ShiftPatternPeriod := udPeriod.Position;
end; //procedure TEditShiftPatternDlg.udPeriodClick
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.SetPatternIDFromShiftID(aID: integer);
begin
  if tabShiftPat.Active then begin
    with TmmAdoDataSet.Create(nil) do try
      ConnectionString := GetDefaultConnectionString;
      CommandText := Format('SELECT c_default_pattern_id FROM t_shiftcal WHERE c_shiftcal_id = %d', [aID]);
      Open;
      if not (EOF) then
        tabShiftPat.Locate('c_shift_pattern_id', FieldByName('c_default_pattern_id').AsInteger, []);
      close;
    finally
      free;
    end; // with TmmAdoDataSet.Create(nil)
  end;
end; // procedure TEditShiftPatternDlg.SetTablePatternID(aID: integer);
//-----------------------------------------------------------------------------

procedure TEditShiftPatternDlg.FormCreate(Sender: TObject);
begin

 imLogo.Left := 1000;
 imLogo.Top  := 1000;
 imLogo.Width  := 1;
 imLogo.Height := 1;

  HelpContext        := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-bearbeiten.htm');
  spGrid.HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster.htm');

  tabShiftPat.Active := False;
  RefreshTimer.Enabled := False;

  with spGrid do begin
    DatabaseConnection := dmShiftCalendar.conDefault;
    MaxShiftsPerDay := 5;
    ShowTeamColors := gProgOptions.ShowTeamColors;
    ReadTeamColors;
  end; //with spGrid do

  Init;
  tabShiftPat.Open;
// HelpContext := hcEditShiftPattern;

{$IFDEF EnableRefreshTimer}
  RefreshTimer.Interval := cRefreshInterval * 1000; //convert to ms
  RefreshTimer.Enabled := True;
{$ENDIF}

  EnableActions;
end; //procedure TEditShiftPatternDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.FormShow(Sender: TObject);
begin
  Screen.Cursor := crDefault;
end; //procedure TEditShiftPatternDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditShiftPatternDlg.FormClose
//-----------------------------------------------------------------------------
function TEditShiftPatternDlg.WriteDataToDB: boolean;
begin
  Result := True;

  if spGrid.IsDifferent then
    if spGrid.Validate then begin
      dmShiftCalendar.conDefault.BeginTrans;
      try
        spGrid.WriteShiftPatternData;
        dmShiftCalendar.conDefault.CommitTrans;
      except
        on e: Exception do begin
          WriteDebugLog('[TEditShiftPatternDlg.WriteDataToDB]', e.Message);

          Result := False;
          dmShiftCalendar.conDefault.RollbackTrans;
        end; // on e:Exception do begin
      end; //try..except
    end //if spGrid.Validate then
    else
      Result := False;
end; //function TEditShiftPatternDlg.WriteDataToDB
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatNewRecord(DataSet: TDataSet);
var
  xNewID: longint;

begin
  xNewID := MakeUniqueID(1, tabShiftPat.CommandText, tabShiftPatC_SHIFT_PATTERN_ID.FieldName, Self);
  if xNewID < 0 then SysUtils.Abort;
  tabShiftPatC_SHIFT_PATTERN_NAME.Value := {LTrans{}(cDefaultStr);
  tabShiftPatC_SHIFT_PATTERN_ID.Value := xNewID;

  EnableActions;
end; //procedure TEditShiftPatternDlg.tabShiftPatNewRecord
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatAfterScroll(DataSet: TDataSet);
begin
  SetPatternID(tabShiftPatC_SHIFT_PATTERN_ID.Value);
end; //procedure TEditShiftPatternDlg.tabShiftPatAfterScroll
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatBeforeInsert(DataSet: TDataSet);
begin
  if not WriteDataToDB then SysUtils.Abort;
end; //procedure TEditShiftPatternDlg.tabShiftPatBeforeInsert
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatBeforeDelete(DataSet: TDataSet);
begin
  if IsLinkedMsg('T_SHIFTCAL', 'C_DEFAULT_PATTERN_ID', {LTrans{}(cMsgStr1), tabShiftPatC_SHIFT_PATTERN_ID.Value, Self) then
    SysUtils.Abort;

  if tabShiftPat.RecordCount = 1 then begin
    MessageBeep(0);
    MMMessageDlg({LTrans{}(cMsgStr2), mtInformation, [mbOK], 0, Self);
    SysUtils.Abort;
  end; //if tabShiftPat.RecordCount = 1 then

//  if not DeleteRecord(format({LTrans{}(cMsgStr3),[tabShiftPatC_SHIFT_PATTERN_NAME.Value])) then
  if MMMessageDlg(Format({LTrans{}(cMsgStr3), [tabShiftPatC_SHIFT_PATTERN_NAME.Value]),
    mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then
    SysUtils.Abort;

  dmShiftCalendar.conDefault.BeginTrans;
  try
    spGrid.DeleteShiftPattern(tabShiftPatC_SHIFT_PATTERN_ID.Value);
    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e: Exception do begin
      WriteDebugLog('[TEditShiftPatternDlg.tabShiftPatBeforeDelete]', e.Message);

      dmShiftCalendar.conDefault.RollbackTrans;
      SysUtils.Abort;
    end; // on e:Exception do begin
  end; //try..except
end; //procedure TEditShiftPatternDlg.tabShiftPatBeforeDelete
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.RefreshTimerTimer(Sender: TObject);
begin
  spGrid.ReadTeamColors;
  spGrid.Refresh;
end; //procedure TEditShiftPatternDlg.RefreshTimerTimer
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := WriteDataToDB;
end; //procedure TEditShiftPatternDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditShiftPatternDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.acShowTeamExecute(Sender: TObject);
begin
  spGrid.ShowTeamColors := not spGrid.ShowTeamColors;
end; //procedure TEditShiftPatternDlg.ActionShowTeamExecute
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.acPrintExecute(Sender: TObject);
var
  xCol,
    xRow: integer;
  xPG: TGridPrint;
  cInfo: rCellInfoT;
  xText :string;
begin
//  if not GetPrinterOptions(poLandscape, [poPrintColors], Self) then exit;
  if not GetPrintOptions(poPortrait, Self, gPrintOptions.PrintColors ) then exit;

  if not WriteDataToDB then exit;

  xPG := TGridPrint.Create;
  try
    xPG.InitPrinterInfo;

    xPG.Cols := spGrid.ShiftPatternPeriod + 1;
    xPG.Rows := gProgOptions.ShiftsPerDay + 1;
    xPG.LeftMM := 10;
    xPG.TopMM := 25;

    for xCol := 1 to xPG.Cols do
      case xCol of
        1: xPG.SetColWidth(xCol, 6);
      else
        xPG.SetColWidth(xCol, 4.5);
      end; //case i of

    for xRow := 1 to xPG.Rows do
      xPG.SetRowHeight(xRow, 6);

    Printer.BeginDoc;
    //xPG.MoveTo(xPG.LeftMM, 20);
    //xPG.LineTo(xPG.PInfo.xMM - xPG.LeftMM, 20);
    (*
    xPG.SetFont(clBlack, [fsBold], 90);
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taCenter, taBottom, Format({LTrans{}(cTitleStr), [tabShiftPatC_SHIFT_PATTERN_NAME.Value]));
    xPG.SetFont(clBlack, [], 70);
//Wss   xPG.AlignTextOut(xPG.LeftMM,0,xPG.PInfo.xMM -xPG.LeftMM,19,taRight,taBottom,formatdatetime_('dd/mm/yyyy  hh:nn',now));
    xPG.AlignTextOut(xPG.LeftMM, 0, xPG.PInfo.xMM - xPG.LeftMM, 19, taRight, taBottom, formatdatetime(ShortDateFormat + '  hh:nn', now));



    xPG.SetFont(clBlack, [fsBold], 120);
    xPG.TextOut(xPG.LeftMM, xPG.PInfo.yMM - 15, 'LOEPFE MILLMASTER');
    *)

    xText := Format((cTitleStr), [tabShiftPatC_SHIFT_PATTERN_NAME.Value]);
    PrintHeaderFooter(Printer.Canvas, xText);


    for xCol := 1 to xPG.Cols do
      for xRow := 1 to xPG.Rows do begin
        cInfo := spGrid.GetCellInfoAt(xCol - 1, xRow - 1);
        if not gPrintOptions.PrintColors then begin
            cInfo.fColor := clBlack;
            cInfo.bColor := clWhite;
            if (xCol = 1) or (xRow = 1) then cInfo.bColor := cLightSilver;
        end;

        xPG.SetFont(cInfo.fColor, cInfo.fStyle, 70);
        xPG.SetBrush(cInfo.bColor, bsSolid);
        xPG.FillCellText(xCol, xRow, taCenter, taCenter, cInfo.cString, True);
      end; //for xRow := 1 to xPG.Rows do
    Printer.EndDoc;
  finally
    xPG.Free;
  end; //try..finally
end; //procedure TEditShiftPatternDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditShiftPatternDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.EnableActions;
begin
  acPrint.Enabled := (GetRecordCount('t_shift_pattern_name', '') > 0) or (tabShiftPat.State = dsInsert);
  edPatternName.Enabled := acPrint.Enabled;
  udPeriod.Enabled := acPrint.Enabled;
  edPeriod.Enabled := acPrint.Enabled;
  spGrid.Enabled := acPrint.Enabled;
end; //procedure TEditShiftDlg.EnableActions
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatAfterPost(DataSet: TDataSet);
begin
  EnableActions;
  UpdateFormCaption;
end; //procedure TEditShiftPatternDlg.tabShiftPatAfterPost
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatBeforePost(DataSet: TDataSet);
begin
  if not DBFieldOk(tabShiftPatC_SHIFT_PATTERN_NAME, gbPatternName.Caption, 0, 0, True, Self) then SysUtils.Abort;
  if not WriteDataToDB then SysUtils.Abort;
end; //procedure TEditShiftPatternDlg.tabShiftPatBeforePost
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.tabShiftPatAfterCancel(DataSet: TDataSet);
begin
  tabShiftPatAfterScroll(Dataset);
  EnableActions;
end; //procedure TEditShiftPatternDlg.tabShiftPatAfterCancel
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.spGridChange(Sender: TObject);
begin
  if not (tabShiftPat.State in [dsEdit, dsInsert]) then tabShiftPat.Edit;
end; //procedure TEditShiftPatternDlg.spGridChange
//-----------------------------------------------------------------------------
procedure TEditShiftPatternDlg.IvExtendedTranslator1LanguageChange(Sender: TObject);
begin
  spGrid.refresh;
end; //procedure TEditShiftPatternDlg.IvExtendedTranslator1LanguageChange
//-----------------------------------------------------------------------------


end. //u_edit_shiftpattern.pas

