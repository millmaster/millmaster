inherited SetFactoryCalRangeDlg: TSetFactoryCalRangeDlg
  Left = 687
  Top = 65
  ActiveControl = bOK
  BorderStyle = bsDialog
  Caption = 'SetFactoryCalRangeDlg'
  ClientHeight = 275
  ClientWidth = 310
  ParentFont = False
  Font.Color = clBlack
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 310
    Height = 240
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gbPeriod: TmmGroupBox
      Left = 5
      Top = 5
      Width = 300
      Height = 70
      Anchors = [akLeft, akTop, akRight]
      Caption = '(30)Zeitraum'
      TabOrder = 0
      object laFrom: TmmLabel
        Tag = 10
        Left = 5
        Top = 20
        Width = 60
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '(15)von'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laTo: TmmLabel
        Tag = 11
        Left = 5
        Top = 45
        Width = 60
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = '(15)bis'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object dtVon: TmmDateTimePicker
        Left = 75
        Top = 16
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 36054
        Time = 36054
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object dtBis: TmmDateTimePicker
        Left = 75
        Top = 41
        Width = 100
        Height = 21
        CalAlignment = dtaLeft
        Date = 36054
        Time = 36054
        DateFormat = dfShort
        DateMode = dmComboBox
        Kind = dtkDate
        ParseInput = False
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object rgSelectDay: TmmRadioGroup
      Left = 5
      Top = 80
      Width = 300
      Height = 105
      Anchors = [akLeft, akTop, akRight]
      Caption = '(30)Wochentag'
      Columns = 2
      Items.Strings = (
        '(15)Sonntag'
        '(15)Montag'
        '(15)Dienstag'
        '(15)Mittwoch'
        '(15)Donnerstag'
        '(15)Freitag'
        '(15)Samstag'
        '(20)alle Tage')
      TabOrder = 1
    end
    object rgState: TmmRadioGroup
      Left = 5
      Top = 190
      Width = 300
      Height = 45
      Anchors = [akLeft, akTop, akRight]
      Caption = '(30)Status'
      Columns = 2
      Items.Strings = (
        '(20)Arbeitstag'
        '(20)arbeitsfreier Tag')
      TabOrder = 2
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 240
    Width = 310
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 70
      Top = 5
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 150
      Top = 5
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 230
      Top = 5
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object queDelete: TmmADOCommand
    CommandText = 
      'DELETE FROM t_except_days WHERE c_except_date = :tag AND c_facto' +
      'ry_calendar_id = :kalenderid '
    Connection = dmShiftCalendar.conDefault
    Parameters = <
      item
        Name = 'tag'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'kalenderid'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    Left = 189
    Top = 47
  end
  object queInsert: TmmADOCommand
    CommandText = 
      'INSERT INTO t_except_days(c_factory_calendar_id,c_except_date) V' +
      'ALUES(:KalenderID, :Tag) '
    Connection = dmShiftCalendar.conDefault
    Parameters = <
      item
        Name = 'KalenderID'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'Tag'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    Left = 221
    Top = 47
  end
  object tabFactoryCal: TmmADODataSet
    Connection = dmShiftCalendar.conDefault
    CommandText = 'T_FACTORY_CALENDAR'
    CommandType = cmdTable
    IndexFieldNames = 'C_FACTORY_CALENDAR_ID'
    Parameters = <>
    Left = 189
    Top = 79
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 210
    Top = 18
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Eingaben uebernehmen, Dialog beenden'
      ImageIndex = 34
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 35
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 221
    Top = 79
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
