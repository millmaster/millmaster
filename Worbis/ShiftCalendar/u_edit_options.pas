(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_options.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Optionen fuer Schichtkalender / Betriebskalender bearbeiten
| Info..........: -
| Develop.system: Dell PII 333, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 28.10.02 | 1.00 | LOK | Umbau ADO
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_options;
interface
uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ToolWin, ExtCtrls, StdCtrls, CheckLst, Nwcombo, Grids, ShiftCalendars,
  ActnList, mmActionList, mmColorDialog, mmCheckListBox, mmUpDown, mmEdit,
  mmLabel, mmCheckBox, mmGroupBox, mmPageControl, mmPanel, mmToolBar, IvDictio, IvMulti, IvEMulti,
  IvMlDlgs, mmTranslator, BaseForm, mmButton, mmIvMlDlgs;

type
  TEditOptionsDlg = class(TmmForm)

    MainPanel: TmmPanel;
    PageControl1: TmmPageControl;
    tsAllgemein: TTabSheet;
    tsShiftCalendar: TTabSheet;
    tsFactoryCalendar: TTabSheet;
    gbMarkierung: TmmGroupBox;
    cbShowMarks: TmmCheckBox;
    edMarkChar: TmmEdit;
    gbWochenende: TmmGroupBox;
    cklWeekend: TmmCheckListBox;
    gbWochenstart: TmmGroupBox;
    cobStartday: TNWComboBox;
    gbSchichtfarben: TmmGroupBox;
    laDatum: TmmLabel;
    laArbeitsfrei: TmmLabel;
    laMarkierung: TmmLabel;
    laSchichtzeit: TmmLabel;
    laTitel: TmmLabel;
    laWochenende: TmmLabel;
    laVG: TmmLabel;
    laHG: TmmLabel;
    scPanel00: TmmPanel;
    scPanel01: TmmPanel;
    scPanel10: TmmPanel;
    scPanel20: TmmPanel;
    scPanel21: TmmPanel;
    scPanel30: TmmPanel;
    scPanel31: TmmPanel;
    scPanel11: TmmPanel;
    scPanel40: TmmPanel;
    scPanel41: TmmPanel;
    scPanel50: TmmPanel;
    scPanel51: TmmPanel;
    gbSchichten: TmmGroupBox;
    edShiftsPerDay: TmmEdit;
    udShiftsPerDay: TmmUpDown;
    laMarkierungszeichen: TmmLabel;
    gbKalenderfarbe: TmmGroupBox;
    laDatum1: TmmLabel;
    laArbeitsfrei1: TmmLabel;
    laTitel1: TmmLabel;
    laWochenende1: TmmLabel;
    laVG1: TmmLabel;
    laHG1: TmmLabel;
    ycPanel00: TmmPanel;
    ycPanel01: TmmPanel;
    ycPanel10: TmmPanel;
    ycPanel40: TmmPanel;
    ycPanel41: TmmPanel;
    ycPanel50: TmmPanel;
    ycPanel51: TmmPanel;
    ycPanel11: TmmPanel;
    gbKalenderstart: TmmGroupBox;
    cbShowCalWeekEnds: TmmCheckBox;
    gbTeams: TmmGroupBox;
    cbShowTeamColors: TmmCheckBox;
    gbOthers: TmmGroupBox;
    cbPrintColors: TmmCheckBox;
    cbShowShiftWeekEnds: TmmCheckBox;

    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;
    mmColorDialog: TmmColorDialog;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure FormCreate(Sender: TObject);
    procedure ColorPanelClick(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl1Change(Sender: TObject);
  private
    function GetMinShiftsPerDay: byte;
    procedure ReadFromControls;
    procedure WriteToControls;
    procedure Init;
  public
    procedure SetVisibleTabs(aVisibleTabs: sByteT);
  end; //TEditOptionsDlg

var
  EditOptionsDlg: TEditOptionsDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,  MMHtmlHelp,

  math,
  u_common_lib, u_main, u_lib, u_readwrite_options, u_edit_shift, u_edit_exceptdays, u_edit_defaultshift,
  u_edit_shiftpattern, mmADODataSet, LoepfeGlobal;

{$R *.DFM}

function TEditOptionsDlg.GetMinShiftsPerDay: byte;
var
  xShifts,
    xDefaultShifts: byte;

begin
  with TmmADODataSet.Create(Application) do begin
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT MAX(c_shift_in_day) AS max_shift FROM t_shift';
    Active := True;
    xShifts := FieldByName('max_shift').AsInteger;

    Active := False;
    CommandText := 'SELECT MAX(c_default_shift_id % 10) AS max_shift FROM t_default_shift';
    Active := True;
    xDefaultShifts := FieldByName('max_shift').AsInteger;
    Active := False;

    Result := math.max(xShifts, xDefaultShifts);
    Result := math.max(1, Result);

    Free;
  end; //with TmmQuery.Create(Application) do
end; //function TEditOptionsDlg.GetMinShiftsPerDay
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.SetVisibleTabs(aVisibleTabs: sByteT);
var
  i: byte;
begin
  for i := 0 to PageControl1.PageCount - 1 do
    PageControl1.Pages[i].TabVisible := (i in aVisibleTabs);
end; //procedure TEditOptionsDlg.SetVisibleTabs
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.ReadFromControls;
var
  i, j: integer;
  xDay: eWeekendDayT;
  xCCI: eCalendarColorT;
  xCP: eColorPositionT;
  xComp: TComponent;
  xBit: integer;

begin
  cobStartday.Items.Clear;
  cklWeekend.Items.Clear;

  for i := 1 to cDaysPerWeek do begin
    j := usDOW(i);
    cobStartday.AddObj(GetLongDayNames(i), j);
    cklWeekend.Items.AddObject(GetLongDayNames(i), TObject(j));
  end; //for i := 1 to cMaxDaysPerWeek do

  with gProgOptions do begin
    cbPrintColors.Checked := PrintColors;

    udShiftsPerDay.Min := GetMinShiftsPerDay;
    udShiftsPerDay.Max := cMaxShiftsPerDay;
    udShiftsPerDay.Position := ShiftsPerDay;

    cbShowMarks.Checked := ShowMarks;
    edMarkChar.Text := MarkChar;

    for xDay := wdSunday to wdSaturday do begin
      j := ord(xDay) + 1;
      i := cklWeekend.Items.IndexOfObject(TObject(j));
      xBit := round(ex(2, ord(xDay)));
      cklWeekend.Checked[i] := (WeekEndDays or xBit) = WeekEndDays;
    end; //for xDay := wdSunday to wdSaturday do

    cobStartday.SetItemID(StartOfWeek);
    cbShowShiftWeekEnds.Checked := ShowShiftWeekEnds;
    cbShowTeamColors.Checked := ShowTeamColors;
    cbShowCalWeekEnds.Checked := ShowCalWeekEnds;

    for xCCI := ciDay to ciWeekEnd do
      for xCP := cpText to cpBackground do begin
        xComp := TPanel(FindComponent('scPanel' + inttostr(ord(xCCI)) + inttostr(ord(xCP))));
        TPanel(xComp).Color := ShiftCalendarColors[xCCI, xCP];

        if xCCI in [ciDay, ciOffDay, ciTitle, ciWeekEnd] then begin
          xComp := TPanel(FindComponent('ycPanel' + inttostr(ord(xCCI)) + inttostr(ord(xCP))));
          TPanel(xComp).Color := YearCalendarColors[xCCI, xCP];
        end; //if xCCI in [ciDay,ciOffDay,ciTitle,ciWeekEnd] then
      end; //for xCP := cpText to cpBackground do
  end; //with gProgOptions do
end; //procedure TEditOptionsDlg.ReadFromControls
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.WriteToControls;
var
  i, j: integer;
  xDay: eWeekendDayT;
  xCCI: eCalendarColorT;
  xCP: eColorPositionT;
  xComp: TComponent;
  xBit: integer;

begin
  with gProgOptions do begin
    PrintColors := cbPrintColors.Checked;
    ShiftsPerDay := udShiftsPerDay.Position;

    ShowMarks := cbShowMarks.Checked;
    MarkChar := trim(edMarkChar.Text);
    if MarkChar = '' then MarkChar := #32;

    WeekEndDays := 0;
    for xDay := wdSunday to wdSaturday do begin
      j := ord(xDay) + 1;
      i := cklWeekend.Items.IndexOfObject(TObject(j));
      xBit := round(ex(2, ord(xDay)));
      if cklWeekend.Checked[i] then WeekEndDays := WeekEndDays + xBit;
    end; //for xDay := wdSunday to wdSaturday do

    StartOfWeek := cobStartday.GetItemID;
    ShowShiftWeekEnds := cbShowShiftWeekEnds.Checked;
    ShowCalWeekEnds := cbShowCalWeekEnds.Checked;
    ShowTeamColors := cbShowTeamColors.Checked;

    for xCCI := ciDay to ciWeekEnd do
      for xCP := cpText to cpBackground do begin
        xComp := TPanel(FindComponent('scPanel' + inttostr(ord(xCCI)) + inttostr(ord(xCP))));
        ShiftCalendarColors[xCCI, xCP] := TPanel(xComp).Color;

        if xCCI in [ciDay, ciOffDay, ciTitle, ciWeekEnd] then begin
          xComp := TPanel(FindComponent('ycPanel' + inttostr(ord(xCCI)) + inttostr(ord(xCP))));
          YearCalendarColors[xCCI, xCP] := TPanel(xComp).Color;
        end; //if xCCI in [ciDay,ciOffDay,ciTitle,ciWeekEnd] then
      end; //for xCP := cpText to cpBackground do
  end; //with gProgOptions do

  WriteOptions;
end; //procedure TEditOptionsDlg.WriteToControls
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.Init;
begin
  ReadFromControls;
end; //procedure TEditOptionsDlg.Init
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definition.htm');
  tsShiftCalendar.HelpContext   := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen-Schichtkalender.htm');
  tsFactoryCalendar.HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen.htm');
  tsAllgemein.HelpContext       := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definition.htm');
  Init;
// HelpContext := hcEditOptions;
end; //procedure TEditOptionsDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.ColorPanelClick(Sender: TObject);
begin
  with (Sender as TPanel) do begin
    mmColorDialog.Color := Color;
    if mmColorDialog.Execute then
      Color := mmColorDialog.Color;
  end; //with (Sender as TPanel) do
end; //procedure TEditOptionsDlg.ColorPanelClick
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.acOKExecute(Sender: TObject);
var
  i: integer;

begin
  WriteToControls;
  Close;

  for i := 0 to Screen.FormCount - 1 do begin
    if Screen.Forms[i] is TEditShiftDlg then TEditShiftDlg(Screen.Forms[i]).Init;
    if Screen.Forms[i] is TEditExceptDaysDlg then TEditExceptDaysDlg(Screen.Forms[i]).Init;
    if Screen.Forms[i] is TEditDefaultShiftDlg then TEditDefaultShiftDlg(Screen.Forms[i]).Init;
    if Screen.Forms[i] is TEditShiftPatternDlg then TEditShiftPatternDlg(Screen.Forms[i]).Init;
  end; //for i := 0 to Screen.FormCount -1 do
end; //procedure TEditOptionsDlg.ActionOKExecute
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.acCancelExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditOptionsDlg.ActionCancelExecute
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditOptionsDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditOptionsDlg.FormClose
//-----------------------------------------------------------------------------
procedure TEditOptionsDlg.PageControl1Change(Sender: TObject);
begin
  HelpContext := PageControl1.ActivePage.HelpContext;
end;
//-----------------------------------------------------------------------------
end. //u_edit_options.pas

