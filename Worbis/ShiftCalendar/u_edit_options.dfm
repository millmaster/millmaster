inherited EditOptionsDlg: TEditOptionsDlg
  Left = 581
  ActiveControl = bOK
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '(80)Einstellungen fuer MILLMASTER [Schichtkalender]'
  ClientHeight = 277
  ClientWidth = 462
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 462
    Height = 242
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 0
    object PageControl1: TmmPageControl
      Left = 2
      Top = 2
      Width = 458
      Height = 238
      ActivePage = tsShiftCalendar
      Align = alClient
      HotTrack = True
      TabOrder = 0
      OnChange = PageControl1Change
      object tsShiftCalendar: TTabSheet
        Caption = '(20)Schichtkalender'
        ImageIndex = 8
        object gbMarkierung: TmmGroupBox
          Left = 5
          Top = 60
          Width = 200
          Height = 70
          Caption = '(30)Markierungen'
          TabOrder = 1
          CaptionSpace = True
          object laMarkierungszeichen: TmmLabel
            Left = 40
            Top = 45
            Width = 150
            Height = 13
            AutoSize = False
            Caption = '(30)Markierungs-Zeichen'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbShowMarks: TmmCheckBox
            Left = 10
            Top = 20
            Width = 97
            Height = 17
            Caption = '(30)Anzeigen'
            Checked = True
            State = cbChecked
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object edMarkChar: TmmEdit
            Left = 10
            Top = 42
            Width = 18
            Height = 19
            AutoSize = False
            Color = clWindow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            MaxLength = 1
            ParentFont = False
            TabOrder = 1
            Text = 'W'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
        end
        object gbSchichtfarben: TmmGroupBox
          Left = 215
          Top = 5
          Width = 230
          Height = 200
          Caption = '(30)Farben'
          TabOrder = 3
          CaptionSpace = True
          object laDatum: TmmLabel
            Left = 10
            Top = 35
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Datum'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laArbeitsfrei: TmmLabel
            Left = 10
            Top = 55
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Arbeitsfrei'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laMarkierung: TmmLabel
            Left = 10
            Top = 75
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Markierung'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laSchichtzeit: TmmLabel
            Left = 10
            Top = 95
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Schichtzeiten'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laTitel: TmmLabel
            Left = 10
            Top = 115
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Titel'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laWochenende: TmmLabel
            Left = 10
            Top = 135
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Wochenende'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laVG: TmmLabel
            Left = 105
            Top = 15
            Width = 50
            Height = 13
            Hint = '(*)Textfarbe'
            Alignment = taCenter
            AutoSize = False
            Caption = '(10)Text'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laHG: TmmLabel
            Left = 165
            Top = 15
            Width = 50
            Height = 13
            Hint = '(*)Hintergrundfarbe'
            Alignment = taCenter
            AutoSize = False
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object scPanel00: TmmPanel
            Tag = 10
            Left = 105
            Top = 35
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 16574684
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = ColorPanelClick
          end
          object scPanel01: TmmPanel
            Tag = 11
            Left = 165
            Top = 35
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = ColorPanelClick
          end
          object scPanel10: TmmPanel
            Tag = 20
            Left = 105
            Top = 55
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = ColorPanelClick
          end
          object scPanel20: TmmPanel
            Tag = 30
            Left = 105
            Top = 75
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = ColorPanelClick
          end
          object scPanel21: TmmPanel
            Tag = 31
            Left = 165
            Top = 75
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = ColorPanelClick
          end
          object scPanel30: TmmPanel
            Tag = 40
            Left = 105
            Top = 95
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = ColorPanelClick
          end
          object scPanel31: TmmPanel
            Tag = 41
            Left = 165
            Top = 95
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = ColorPanelClick
          end
          object scPanel11: TmmPanel
            Tag = 21
            Left = 165
            Top = 55
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            OnClick = ColorPanelClick
          end
          object scPanel40: TmmPanel
            Tag = 50
            Left = 105
            Top = 115
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 8
            OnClick = ColorPanelClick
          end
          object scPanel41: TmmPanel
            Tag = 51
            Left = 165
            Top = 115
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
            OnClick = ColorPanelClick
          end
          object scPanel50: TmmPanel
            Tag = 60
            Left = 105
            Top = 135
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 10
            OnClick = ColorPanelClick
          end
          object scPanel51: TmmPanel
            Tag = 61
            Left = 165
            Top = 135
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 11
            OnClick = ColorPanelClick
          end
        end
        object gbSchichten: TmmGroupBox
          Left = 5
          Top = 5
          Width = 200
          Height = 50
          Caption = '(30)Schichten pro Tag'
          TabOrder = 0
          CaptionSpace = True
          object edShiftsPerDay: TmmEdit
            Left = 11
            Top = 20
            Width = 30
            Height = 21
            Color = clWindow
            ReadOnly = True
            TabOrder = 0
            Text = '1'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object udShiftsPerDay: TmmUpDown
            Left = 41
            Top = 20
            Width = 15
            Height = 21
            Associate = edShiftsPerDay
            Min = 1
            Max = 5
            Position = 1
            TabOrder = 1
            Wrap = False
          end
        end
        object gbTeams: TmmGroupBox
          Left = 5
          Top = 135
          Width = 200
          Height = 70
          TabOrder = 2
          object cbShowTeamColors: TmmCheckBox
            Left = 10
            Top = 17
            Width = 180
            Height = 17
            Caption = '(30)Schichtgruppen anzeigen'
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbShowShiftWeekEnds: TmmCheckBox
            Left = 10
            Top = 43
            Width = 180
            Height = 17
            Caption = '(30)Wochenenden anzeigen'
            TabOrder = 1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
      object tsFactoryCalendar: TTabSheet
        Caption = '(20)Betriebskalender'
        ImageIndex = 7
        object gbWochenende: TmmGroupBox
          Left = 5
          Top = 5
          Width = 200
          Height = 120
          Caption = '(30)Wochenend-Tage'
          TabOrder = 0
          CaptionSpace = True
          object cklWeekend: TmmCheckListBox
            Left = 10
            Top = 20
            Width = 121
            Height = 95
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = True
            ItemHeight = 13
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6')
            ParentCtl3D = False
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object gbWochenstart: TmmGroupBox
          Left = 5
          Top = 130
          Width = 200
          Height = 75
          Caption = '(30)Wochenbeginn'
          TabOrder = 1
          object cobStartday: TNWComboBox
            Left = 10
            Top = 20
            Width = 180
            Height = 21
            Style = csDropDownList
            DropDownCount = 7
            ItemHeight = 13
            TabOrder = 0
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6')
          end
        end
        object gbKalenderfarbe: TmmGroupBox
          Left = 215
          Top = 5
          Width = 230
          Height = 120
          Caption = '(30)Farben'
          TabOrder = 2
          CaptionSpace = True
          object laDatum1: TmmLabel
            Left = 10
            Top = 35
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Datum'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laArbeitsfrei1: TmmLabel
            Left = 10
            Top = 55
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Arbeitsfrei'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laTitel1: TmmLabel
            Left = 10
            Top = 75
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Titel'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laWochenende1: TmmLabel
            Left = 10
            Top = 95
            Width = 85
            Height = 13
            AutoSize = False
            Caption = '(20)Wochenende'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laVG1: TmmLabel
            Left = 105
            Top = 15
            Width = 50
            Height = 13
            Hint = '(*)Textfarbe'
            Alignment = taCenter
            AutoSize = False
            Caption = '(10)Text'
            Color = clBtnFace
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laHG1: TmmLabel
            Left = 165
            Top = 15
            Width = 50
            Height = 13
            Hint = '(*)Hintergrundfarbe'
            Alignment = taCenter
            AutoSize = False
            Color = clWhite
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWhite
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentColor = False
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object ycPanel00: TmmPanel
            Tag = 11
            Left = 105
            Top = 35
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Color = 16574684
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = ColorPanelClick
          end
          object ycPanel01: TmmPanel
            Tag = 10
            Left = 165
            Top = 35
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = ColorPanelClick
          end
          object ycPanel10: TmmPanel
            Tag = 21
            Left = 105
            Top = 55
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = ColorPanelClick
          end
          object ycPanel40: TmmPanel
            Tag = 31
            Left = 105
            Top = 75
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = ColorPanelClick
          end
          object ycPanel41: TmmPanel
            Tag = 30
            Left = 165
            Top = 75
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = ColorPanelClick
          end
          object ycPanel50: TmmPanel
            Tag = 41
            Left = 105
            Top = 95
            Width = 50
            Height = 15
            Hint = '(*)Textfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = ColorPanelClick
          end
          object ycPanel51: TmmPanel
            Tag = 40
            Left = 165
            Top = 95
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = ColorPanelClick
          end
          object ycPanel11: TmmPanel
            Tag = 20
            Left = 165
            Top = 55
            Width = 50
            Height = 15
            Hint = '(*)Hintergrundfarbe'
            BevelInner = bvRaised
            BevelOuter = bvLowered
            Ctl3D = False
            ParentCtl3D = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 7
            OnClick = ColorPanelClick
          end
        end
        object gbKalenderstart: TmmGroupBox
          Left = 215
          Top = 130
          Width = 230
          Height = 75
          TabOrder = 3
          object cbShowCalWeekEnds: TmmCheckBox
            Left = 10
            Top = 20
            Width = 180
            Height = 17
            Caption = '(30)Wochenenden anzeigen'
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
      object tsAllgemein: TTabSheet
        Caption = '(20)Allgemein'
        ImageIndex = 17
        object gbOthers: TmmGroupBox
          Left = 5
          Top = 5
          Width = 440
          Height = 200
          Caption = '(60)Sonstige Einstellungen'
          TabOrder = 0
          CaptionSpace = True
          object cbPrintColors: TmmCheckBox
            Left = 10
            Top = 20
            Width = 420
            Height = 17
            Caption = '(60)Ausdruck in Farbe bzw. Graustufen'
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 242
    Width = 462
    Height = 35
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 222
      Top = 5
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 302
      Top = 5
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 382
      Top = 5
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 290
    Top = 10
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Eingaben uebernehmen, Dialog beenden'
      ImageIndex = 34
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 35
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 330
    Top = 10
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0))
  end
  object mmColorDialog: TmmColorDialog
    Ctl3D = True
    Left = 371
    Top = 9
  end
end
