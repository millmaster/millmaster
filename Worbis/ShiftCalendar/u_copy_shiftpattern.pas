(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_copy_shiftpattern.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Auswahl-Dialog um Schichtmuster zu kopieren. 3 Moeglichkeiten
|                 1. Schichtmuster wird in einen gewaehlten Zeitraum hinein kopiert
|                 2. Schichtmuster wird ab einem gewaehlten Zeitpunkt bis zum Ende
|                    des Datenbank-Bereiches kopiert
|                 3. Schichtmuster wird ab einem gewaehlten Zeitpunkt mit einer
|                    gewaehlten Zyklenanzahl (Zyklen) kopiert
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 29.10.99 | 0.03 | PW  | TToolButton/TToolBar replaced by TButton
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.02 |      | LOK | mit SetPatternIDFromShift() kann das aktuelle Schichtmuster gesetzt werden
|===========================================================================================*)

{$I symbols.inc}

unit u_copy_shiftpattern;
interface
uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, ComCtrls,
  StdCtrls, ToolWin, DB, Nwcombo, ActnList, mmActionList, mmUpDown, mmEdit,
  mmRadioButton, mmDateTimePicker, mmBevel, mmLabel, mmGroupBox, mmPanel, mmToolBar,
  IvDictio, IvMulti, IvEMulti, mmTranslator, BaseForm, mmButton, mmADODataset, LoepfeGlobal;

type
  TCopyShiftPatternDlg = class(TmmForm)

    MainPanel: TmmPanel;
    gbPeriod: TmmGroupBox;
    laFrom: TmmLabel;
    laTo: TmmLabel;
    dtFrom: TmmDateTimePicker;
    dtTo: TmmDateTimePicker;
    rbPeriod: TmmRadioButton;
    rbFillToEnd: TmmRadioButton;
    rbCycles: TmmRadioButton;
    edCycles: TmmEdit;
    udCycles: TmmUpDown;
    laCycles: TmmLabel;
    Bevel1: TmmBevel;
    Bevel2: TmmBevel;
    cobPattern: TNWComboBox;

    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    mmTranslator: TmmTranslator;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure rbClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure Fill_cobPattern;
  public
    gCPRec: rCopyShiftT;
    procedure SetPatternIDFromShift(aId: integer);
  end; //TCopyShiftPatternDlg

var
  CopyShiftPatternDlg: TCopyShiftPatternDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,  MMHtmlHelp,

  mmDialogs,
  u_common_lib, u_common_global, u_lib, u_main;

{$R *.DFM}

resourcestring
  cMsgStr1 = 'Zeitraum von > Zeitraum bis ?'; //ivlm

procedure TCopyShiftPatternDlg.rbClick(Sender: TObject);
begin
  edCycles.Enabled := TRadioButton(Sender).Tag = 2;
  udCycles.Enabled := edCycles.Enabled;
  dtTo.Enabled := TRadioButton(Sender).Tag = 0;
  laTo.Font.Color := GetEnabledFontColor(dtTo.Enabled);
  laCycles.Font.Color := GetEnabledFontColor(udCycles.Enabled);
  gCPRec.Variante := TRadioButton(Sender).Tag;
end; //procedure TCopyShiftPatternDlg.rbClick
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if ModalResult = mrOK then begin
    CanClose := dtFrom.Date < dtTo.Date;
    if not CanClose then MMMessageDlg({LTrans{}(cMsgStr1), mtError, [mbOK], 0, Self);
    if CanClose then begin
      gCPRec.PatternID := cobPattern.GetItemID;
      gCPRec.Cycles := udCycles.Position;
      gCPRec.Period.fromDate := dtFrom.Date;
      gCPRec.Period.toDate := dtTo.Date;
    end; //if CanClose then
  end //if ModalResult = mrOK then
  else
    CanClose := True;
end; //procedure TCopyShiftPatternDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.SetPatternIDFromShift(aId: integer);
begin
  with TmmAdoDataSet.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := Format('SELECT c_default_pattern_id FROM t_shiftcal WHERE c_shiftcal_id = %d', [aID]);
    Open;
    if not (EOF) then
      cobPattern.SetItemID(FieldByName('c_default_pattern_id').AsInteger);
    close;
  finally
    free;
  end; // with TmmAdoDataSet.Create(nil)
end; // procedure TCopyShiftPatternDlg.SetPatternItemID(aId: integer);

procedure TCopyShiftPatternDlg.Fill_cobPattern;
begin
  with TmmAdoDataSet.Create(nil) do try
    cobPattern.Items.Clear;
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT c_shift_pattern_id,c_shift_pattern_name FROM t_shift_pattern_name';
    Open;
    while not Eof do begin
      cobPattern.AddObj(FieldByName('c_shift_pattern_name').AsString, FieldByName('c_shift_pattern_id').AsInteger);
      Next;
    end; //while not Eof do
  finally
    free;
  end; // with TmmAdoDataSet.Create(nil)

  cobPattern.ItemIndex := 0;
end; //procedure TCopyShiftPatternDlg.Fill_cobPattern
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-uebernehmen.htm');
  Fill_cobPattern;
  rbClick(rbPeriod);
end; //procedure TCopyShiftPatternDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.acOKExecute(Sender: TObject);
begin
  ModalResult := mrOK;
end; //procedure TCopyShiftPatternDlg.ActionOKExecute
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end; //procedure TCopyShiftPatternDlg.ActionCancelExecute
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TCopyShiftPatternDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TCopyShiftPatternDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TCopyShiftPatternDlg.FormClose

end. //u_copy_shiftpattern.pas

