(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_global.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Typen, Variablen, Konstanten
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
|=========================================================================================*)

{$i symbols.inc}

unit u_global;
interface
uses
 u_common_global,
 Forms,DB,Graphics,WinTypes,WinProcs,Classes,Printers;

const
 //divers
 cRefreshInterval      = 120;  //seconds
 cProgramVersion       = '1.00';
 cCompileDate          = '17.05.1999';

 //Helpsystem, Filename and Context IDs
 hcOffset              = 1000;
 hcGlobalInfo          = hcOffset +1;
 hcEditExceptDays      = hcOffset +2;
 hcEditFactoryCalendar = hcOffset +3;
 hcSetFactoryCalRange  = hcOffset +4;
 hcSetPrintOptions     = hcOffset +5;
 hcAssignShiftCalendar = hcOffset +6;
 hcCopyDefaultShift    = hcOffset +7;
 hcCopyShiftPattern    = hcOffset +8;
 hcEditDefaultShift    = hcOffset +9;
 hcEditOptions         = hcOffset +10;
 hcEditShift           = hcOffset +11;
 hcEditShiftCalendar   = hcOffset +12;
 hcEditShiftPattern    = hcOffset +13;
 hcEditTeam            = hcOffset +14;


 cLightSilver          = $00EBEBEB; // Black & white print Grid-Header



type
 eCalendarColorT  = (ciDay,ciOffDay,ciMark,ciTime,ciTitle,ciWeekEnd);
 eColorPositionT  = (cpText,cpBackground);
 ePrintOptionsT   = (poPrintColors,poShiftCalRange);

 sPrintOptionsT   = set of ePrintOptionsT;

 rDatePeriodT = record
  fromDate,
  toDate    : TDateTime;
 end; //rDatePeriodT

 rCopyShiftT = record
  PatternID,
  Variante,
  Cycles    : longint;
  Period    : rDatePeriodT;
 end; //rCopyShiftT

 rProgrammOptionT = record
  WindowOpenMode     : eWindowOpenModeT;
  ShiftsPerDay       : byte;
  ShiftCalendarColors,
  YearCalendarColors : array[eCalendarColorT,eColorPositionT] of TColor;
  ShowTeamColors,
  ShowShiftWeekEnds,
  ShowCalWeekEnds    : boolean;
  StartOfWeek        : byte;
  WeekEndDays        : integer;
  MarkChar           : s1;
  ShowMarks          : boolean;
  PrintColors        : boolean;
 end; //rProgrammOptionT

 rPrintOptionT = record
  PrintColors   : boolean;
  FromYearMonth,
  ToYearMonth   : longint;
 end; //rPrintOptionT

//-----------------------------------------------------------------------------

var
 gProgOptions  : rProgrammOptionT;
 gPrintOptions : rPrintOptionT;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end. //u_global.pas
