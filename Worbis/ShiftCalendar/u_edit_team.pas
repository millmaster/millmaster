(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_team.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Bearbeiten und erstellen von Schichtgruppen
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 19.05.00 | 0.02 | Wss | IndexFieldName made problems: team records were displayed 3 times
| 28.10.02 |      | LOK | Umbau ADO
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$i symbols.inc}

unit u_edit_team;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,ExtCtrls,DBCtrls,
 ComCtrls,ToolWin,Db,Grids,DBGrids,StdCtrls,Buttons,ActnList,
 mmActionList,mmColorDialog,mmDataSource,mmDBGrid,mmToolBar,
 IvDictio,IvMulti,IvEMulti,mmTranslator,
 Wwdatsrc, Wwdbigrd, Wwdbgrid, wwdblook, Mask, wwdbedit, Wwdotdot, BaseForm,
 mmDBNavigator, mmIvMlDlgs, ADODB, mmADODataSet, u_dmShiftCalendar;

type
 TEditTeamDlg = class(TmmForm)
    queDepartment: TmmADODataSet;
    srcTeam: TmmDataSource;

  ToolBar1: TmmToolBar;
   bClose: TToolButton;
   ToolButton2: TToolButton;
   bPrint: TToolButton;
   ToolButton8: TToolButton;
   bHelp: TToolButton;
   ToolButton14: TToolButton;
    DBNavigator1: TmmDBNavigator;

  ActionList1: TmmActionList;
   acClose: TAction;
   acPrint: TAction;
   acHelp: TAction;

  mmTranslator: TmmTranslator;
    mDBGrid: TmmDBGrid;
    tabTeam: TmmADODataSet;
    tabTeamc_team_id: TStringField;
    tabTeamc_department_id: TSmallintField;
    tabTeamc_team_name: TStringField;
    tabTeamc_team_color: TIntegerField;
    calcDepartment: TStringField;
    mColorDialog: TmmColorDialog;

    procedure tabTeamBeforeDelete(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tabTeamAfterPost(DataSet: TDataSet);
    procedure tabTeamNewRecord(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure tabTeamBeforePost(DataSet: TDataSet);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure mDBGridDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure mDBGridEditButtonClick(Sender: TObject);
    procedure mDBGridCellClick(Column: TColumn);
    procedure mDBGridKeyPress(Sender: TObject; var Key: Char);
  private
   procedure DisplayColorDlg;
  public
 end; //TEditTeamDlg

var
 EditTeamDlg: TEditTeamDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

 mmDialogs, printers,
 u_main,u_global,u_lib,u_progress,u_edit_shift,u_edit_shiftpattern,
 u_common_lib,u_common_global,u_report_teams;

{$R *.DFM}

// resourcestrings will be translated automatically. There is no need of retranslate again wiht Translate()
resourcestring
 cMsgStr1    = '(255)Letzte Gruppe kann nicht geloescht werden !'; //ivlm
 cMsgStr2    = '(255)Soll die Gruppe mit der Bezeichnung "%s" geloescht werden ?'; //ivlm
 cDefaultStr = '(30)Standard - not named yet'; //ivlm

procedure TEditTeamDlg.tabTeamBeforeDelete(DataSet: TDataSet);
begin
 if tabTeam.RecordCount = 1 then
 begin
  MessageBeep(0);
  MMMessageDlg({LTrans{}(cMsgStr1), mtInformation, [mbOK], 0, Self);
  SysUtils.Abort;
 end; //if not CanDelete then

//  if not DeleteRecord(format(LTrans(cMsgStr2),[tabTeamC_TEAM_NAME.Value])) then
  if MMMessageDlg(Format(cMsgStr2,[tabTeamC_TEAM_NAME.Value]),
                  mtConfirmation, [mbYes,mbNo], 0, Self) <> mrYes then
    SysUtils.Abort;
end; //procedure TForm1.nameDBBeforeDelete
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtgruppen-erstellen-bearbeiten.htm');

  if OLEMode then
    mColorDialog.UseInFloor := True;

  tabTeam.Open;
end; //procedure TEditTeamDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := PostRecord(tabTeam);
end; //procedure TEditTeamDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.tabTeamAfterPost(DataSet: TDataSet);
var
 i : integer;

begin
 for i := 0 to Screen.FormCount -1 do
 begin
  if Screen.Forms[i] is TEditShiftDlg then
  begin
   TEditShiftDlg(Screen.Forms[i]).kspGrid.ReadTeamColors;
   TEditShiftDlg(Screen.Forms[i]).kspGrid.Refresh;
  end; //if Screen.Forms[i] is TEditShiftDlg then

  if Screen.Forms[i] is TEditShiftPatternDlg then
  begin
   TEditShiftPatternDlg(Screen.Forms[i]).spGrid.ReadTeamColors;
   TEditShiftPatternDlg(Screen.Forms[i]).spGrid.Refresh;
  end; //if Screen.Forms[i] is TEditShiftDlg then
 end; //for i := 0 to Screen.FormCount -1 do
end; //procedure TEditTeamDlg.tabTeamAfterPost
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.tabTeamNewRecord(DataSet: TDataSet);
begin
 tabTeamC_TEAM_NAME.Value := {LTrans{}(cDefaultStr);
 tabTeamC_TEAM_COLOR.Value := clWindow;
end; //procedure TEditTeamDlg.tabTeamNewRecord
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.FormShow(Sender: TObject);
begin
 Screen.Cursor := crDefault;
end; //procedure TEditTeamDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.tabTeamBeforePost(DataSet: TDataSet);
begin
 if not DBFieldOk(tabTeamC_DEPARTMENT_ID,calcDepartment.DisplayLabel,1,MaxLongInt,true, Self) then
   SysUtils.Abort;
 tabTeamC_TEAM_ID.Value := UpperCase(tabTeamC_TEAM_ID.Value);
end; //procedure TEditTeamDlg.tabTeamBeforePost
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.DisplayColorDlg;
begin
  with mColorDialog do
  try
    Color := tabTeamC_TEAM_COLOR.Value;
    if Execute then begin
      with tabTeam do
      try
        Edit;
        tabTeamC_TEAM_COLOR.AsInteger := Color;
        Post;
      except
        Cancel;
      end;
    end;
  finally
    mDBGrid.EditorMode := False;
  end;
end; //procedure TEditTeamDlg.DisplayColorDlg
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.DBGrid1CellClick(Column: TColumn);
begin
 if Column.FieldName = 'C_TEAM_COLOR' then DisplayColorDlg;
end; //procedure TEditTeamDlg.DBGrid1CellClick
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 inherited;
 Action := caFree;
end; //procedure TEditTeamDlg.FormClose
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditTeamDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.acPrintExecute(Sender: TObject);
begin
//  if not GetPrinterOptions(poPortrait,[], Self) then exit;
  if not GetPrintOptions(poPortrait, Self,  gPrintOptions.PrintColors ) then exit;
  PrintQuickReport(TReportTeamsForm,ReportTeamsForm);
end; //procedure TEditTeamDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditTeamDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditTeamDlg.ActionHelpExecute
//------------------------------------------------------------------------------
procedure TEditTeamDlg.mDBGridDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if DataCol = 2 then begin
    with mDBGrid.Canvas do begin
      Brush.Color := Column.Field.Value;
      FillRect(Rect);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TEditTeamDlg.mDBGridEditButtonClick(Sender: TObject);
begin
  if mDBGrid.SelectedField.Index = 2 then begin
    DisplayColorDlg;
  end;
end;
//------------------------------------------------------------------------------
procedure TEditTeamDlg.mDBGridCellClick(Column: TColumn);
begin
  mDBGrid.EditorMode := False;
  if Column.Index = 3 then
    mDBGrid.EditorMode := True
  else if Column.Index = 2 then
    DisplayColorDlg
  else
    mDBGrid.EditorMode := False;
end;
//------------------------------------------------------------------------------
procedure TEditTeamDlg.mDBGridKeyPress(Sender: TObject; var Key: Char);
begin
 if (mDBGrid.SelectedField = tabTeamC_TEAM_COLOR) and (Key = #13) then begin
   DisplayColorDlg;
   Key := #00;
 end;
end;
//------------------------------------------------------------------------------
end. //u_edit_team.pas

