inherited EditDefaultShiftDlg: TEditDefaultShiftDlg
  Left = 835
  Top = 121
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = '(30)Standardschichten bearbeiten'
  ClientHeight = 213
  ClientWidth = 312
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 312
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 7
      Style = tbsSeparator
    end
    object bSave: TToolButton
      Left = 34
      Top = 0
      Action = acSaveChanges
    end
    object bPrint: TToolButton
      Left = 57
      Top = 0
      Action = acPrint
    end
    object ToolButton8: TToolButton
      Left = 80
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object cobShiftcal: TNWComboBox
      Left = 88
      Top = 0
      Width = 165
      Height = 21
      Hint = '(*)Schichtkalender auswaehlen'
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
    end
    object ToolButton1: TToolButton
      Left = 253
      Top = 0
      Width = 8
      ImageIndex = 7
      Style = tbsSeparator
    end
    object bHelp: TToolButton
      Left = 261
      Top = 0
      Action = acHelp
    end
  end
  object MainPanel: TmmPanel
    Left = 0
    Top = 26
    Width = 312
    Height = 187
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object dsGrid: TDefaultShiftGrid
      Left = 0
      Top = 0
      Width = 312
      Height = 187
      CalendarColors.DayColor = 14737632
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = 14737632
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = 14737632
      CalendarColors.OffDayTextColor = clRed
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 15983582
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      DatabaseConnection = dmShiftCalendar.conDefault
      TabOrder = 0
      Align = alClient
      ColWidths = (
        80
        45
        45
        45
        45
        45)
      RowHeights = (
        24
        22
        22
        22
        22
        22
        22
        22)
    end
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 256
    Top = 58
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acSaveChanges: TAction
      Hint = '(*)Aenderungen jetzt sichern'
      ImageIndex = 3
      ShortCut = 16467
      OnExecute = acSaveChangesExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 216
    Top = 58
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0))
  end
end
