(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_shiftcalendar.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Erstellen von Schichtkalendern, verbinden von Schichtkalendern mit
|                 Betriebskalendern.
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 28.10.02 |      | LOK | Umbau ADO
| 01.11.2002       LOK  | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 18.06.04 |      | SDo | Print-Layout & Print-DLG geaendert -> acPrintExecute
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_shiftcalendar;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls, DBCtrls,
  ComCtrls, ToolWin, Db, Grids, DBGrids, StdCtrls, Buttons, ActnList, mmActionList,
  mmDataSource, mmDBGrid, mmToolBar, IvDictio, IvMulti,
  IvEMulti, BASEFORM, mmTranslator, wwdblook, Wwdatsrc,
  Wwdbigrd, Wwdbgrid, mmDBNavigator, mmComboBox, DBVisualBox,
  mmDBLookupComboBox, u_dmShiftCalendar, ADODB, mmADODataSet, mmADOCommand;

type
  TEditShiftCalendarDlg = class(TmmForm)
    srcShiftCal: TmmDataSource;
    tabShiftCal: TmmADODataSet;
    tabShiftCalKalender: TStringField;
    tabShiftCalMuster: TStringField;
    tabShiftCalc_shiftcal_id: TSmallintField;
    tabShiftCalc_shiftcal_name: TStringField;
    tabShiftCalc_default_pattern_id: TSmallintField;
    tabShiftCalc_factory_calendar_id: TSmallintField;
    queCALENDAR: TmmADODataSet;
    quePATTERN: TmmADODataSet;
    queDELETE: TmmADOCommand;

    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bPrint: TToolButton;
    ToolButton8: TToolButton;
    bHelp: TToolButton;
    ToolButton14: TToolButton;
    DBNavigator1: TmmDBNavigator;

    ActionList1: TmmActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;

    mmTranslator1: TmmTranslator;
    dsCalendar: TmmDataSource;
    mDBGrid: TmmDBGrid;

    procedure tabShiftCalBeforeDelete(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure tabShiftCalAfterDeletePost(DataSet: TDataSet);
    procedure tabShiftCalNewRecord(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure tabShiftCalBeforePost(DataSet: TDataSet);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mDBGridCellClick(Column: TColumn);
  private
  public
  end; //TEditShiftCalendarDlg

var
  EditShiftCalendarDlg: TEditShiftCalendarDlg;

{-----------------------------------------------------------------------------}

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  Printers, mmDialogs,
  u_main, u_global, u_lib, u_edit_defaultshift, u_edit_shiftpattern, u_progress,
  u_common_lib, u_common_global, u_report_shiftcalendar;

{$R *.DFM}

resourcestring
  cMsgStr1 = '(255)Schichtkalender ist noch mit Maschinen verbunden. Loeschen nicht moeglich !'; //ivlm
  cMsgStr2 = '(255)Letzter Schichtkalender kann nicht geloescht werden !'; //ivlm
  cMsgStr3 = '(255)Soll der Kalender mit der Bezeichnung "%s" geloescht werden ?'; //ivlm
  cDefaultStr = '(30)Standard - not named yet'; //ivlm

procedure TEditShiftCalendarDlg.tabShiftCalBeforeDelete(DataSet: TDataSet);
begin
  tabShiftCal.Refresh;

{Wss: t_machine doesn't contain a c_shiftcal_id column anymore -> DO NOT CHECK FOR DELETE
 if IsLinkedMsg('T_MACHINE','C_SHIFTCAL_ID',LTrans(cMsgStr1),tabShiftCalC_SHIFTCAL_ID.Value) then SysUtils.Abort;
{}
  if tabShiftCal.RecordCount = 1 then begin
    MessageBeep(0);
    MMMessageDlg({{LTrans{}{}(cMsgStr2), mtInformation, [mbOK], 0, Self);
    Abort;
  end; //if tabShiftCal.RecordCount = 1 then

// if not DeleteRecord(Format({{LTrans{}{}(cMsgStr3),[tabShiftCalC_SHIFTCAL_NAME.Value])) then
  if MMMessageDlg(Format({{LTrans{}{}(cMsgStr3), [tabShiftCalC_SHIFTCAL_NAME.Value]),
    mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then
    Abort;

 //delete with trigger
  dmShiftCalendar.conDefault.BeginTrans;
  try
    queDELETE.CommandText := Format('DELETE FROM t_shift WHERE c_shiftcal_id = %d', [tabShiftCalC_SHIFTCAL_ID.Value]);
    queDELETE.Execute;

    queDELETE.CommandText := Format('DELETE FROM t_default_shift WHERE c_shiftcal_id = %d', [tabShiftCalC_SHIFTCAL_ID.Value]);
    queDELETE.Execute;

    dmShiftCalendar.conDefault.CommitTrans;
  except
    on e: Exception do begin
      WriteDebugLog('[TEditShiftCalendarDlg.tabShiftCalBeforeDelete]', e.Message);
      dmShiftCalendar.conDefault.RollbackTrans;
      Abort;
    end; // on e:Exception do begin
  end; //try..except
end; //procedure TForm1.tabShiftCalBeforeDelete
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-bearbeiten.htm');
  tabShiftCal.Open;
  Screen.Cursor := crDefault;
// HelpContext := hcEditShiftCalendar;
end; //procedure TEditShiftCalendarDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  CanClose := PostRecord(tabShiftCal);
end; //procedure TEditShiftCalendarDlg.FormCloseQuery
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.tabShiftCalAfterDeletePost(DataSet: TDataSet);
begin
  tabShiftCal.Refresh;
end; //procedure TEditShiftCalendarDlg.tabShiftCalAfterDeletePost
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.tabShiftCalNewRecord(DataSet: TDataSet);
var
  xNewID: longint;

begin
  xNewID := MakeUniqueID(1, tabShiftCal.CommandText, tabShiftCalC_SHIFTCAL_ID.FieldName, Self);
  if xNewID < 0 then SysUtils.Abort;
  tabShiftCalC_SHIFTCAL_ID.Value := xNewID;
  tabShiftCalC_SHIFTCAL_NAME.Value := {{LTrans{} {}(cDefaultStr);
end; //procedure TEditShiftCalendarDlg.tabShiftCalNewRecord
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.FormShow(Sender: TObject);
begin
//  Screen.Cursor := crDefault;
end; //procedure TEditShiftCalendarDlg.FormShow
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.tabShiftCalBeforePost(DataSet: TDataSet);
begin
  if not DBFieldOk(tabShiftCalc_shiftcal_name, '', 0, 0, True, Self) then Abort;
  if not DBFieldOk(tabShiftCalC_FACTORY_CALENDAR_ID, tabShiftCalKalender.DisplayLabel, 1, MaxLongInt, True, Self) then Abort;
  if not DBFieldOk(tabShiftCalC_DEFAULT_PATTERN_ID, tabShiftCalMuster.DisplayLabel, 1, MaxLongInt, True, Self) then Abort;
end; //procedure TEditShiftCalendarDlg.tabShiftCalBeforePost
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEditShiftCalendarDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.acPrintExecute(Sender: TObject);
begin
//  if not GetPrinterOptions(poPortrait, [], Self) then exit;
  if not GetPrintOptions(poPortrait, Self,  gPrintOptions.PrintColors ) then exit;
  PrintQuickReport(TReportShiftCalendarForm, ReportShiftCalendarForm);
end; //procedure TEditShiftCalendarDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEditShiftCalendarDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TEditShiftCalendarDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditShiftCalendarDlg.FormClose

procedure TEditShiftCalendarDlg.mDBGridCellClick(Column: TColumn);
begin
  mDBGrid.EditorMode := (Column.Index in [1, 2]);
end;

end. //u_edit_shiftcalendar.pas

