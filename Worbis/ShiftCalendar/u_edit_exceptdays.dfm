inherited EditExceptDaysDlg: TEditExceptDaysDlg
  Left = 205
  Top = 103
  ActiveControl = cobFactoryCal
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '(*)Betriebskalender'
  ClientHeight = 401
  ClientWidth = 561
  Constraints.MaxHeight = 428
  Constraints.MaxWidth = 569
  ParentFont = False
  Font.Color = clBlack
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000BBBB00000000000BBBB0000000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000B00000BBBB0E00000
    000000BBBB00000000000BBBB0E00BBBB00000BBBB00BBBB00000BBBB0E00BBB
    B0E000BBBB00BBBB0E000BBBB0E00BBBB0E000BBBB00BBBB0E0B0BBBB0E00BBB
    B0E000BBBBB0BBBB0E0BBBBBB0E00BBBB0E0000BBBB0BBBB0E0BBBBBB0E00BBB
    B0E000B0BBB0BBBB0E0BBBBBB0E00BBBB0E00BBB0BB0BBBB0E0BBBBBB0E00BBB
    B0E0BBBBB0B0BBBB0E00BBBBB0E00BBBB00BBBBBBB00BBBB0E000BBBB0E00BBB
    B0BBBBBBBBB0BBBB0E00000000E00BBBBBBBBB0BBBBBBBBB0E00000EEE000BBB
    BBBBB0E0BBBBBBBB0E00000000000BBBBBBB0EEE0BBBBBBB0E00000000000BBB
    BBB0EEE000BBBBBB0E00000000000BBBBB0EEE00000BBBBB0E00000000000BBB
    B0EEE0000000BBBB0E000000000000000EEE0000000000000E000000000000EE
    E0E00000000000EEE0000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81FF03FF80FF01FF807F00FF80
    7F00FF807F00FF806F00FF804700038007000180030000800000008000000080
    0000008000000080000000000000000000000000010000000180000001C00000
    01E0000001FF000801FF001C01FF003E01FF007F01FF80FF81FFC1FFC1FF}
  Position = poScreenCenter
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 26
    Width = 561
    Height = 375
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object JanHeaderPanel: TmmPanel
      Tag = 1001
      Left = 5
      Top = 5
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Januar'
      Color = clSilver
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 12
    end
    object FebHeaderPanel: TmmPanel
      Tag = 1002
      Left = 144
      Top = 5
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Februar'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 13
    end
    object MarHeaderPanel: TmmPanel
      Tag = 1003
      Left = 283
      Top = 5
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Maerz'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 14
    end
    object AprHeaderPanel: TmmPanel
      Tag = 1004
      Left = 422
      Top = 5
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)April'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 15
    end
    object MayHeaderPanel: TmmPanel
      Tag = 1005
      Left = 5
      Top = 130
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Mai'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 16
    end
    object JunHeaderPanel: TmmPanel
      Tag = 1006
      Left = 144
      Top = 130
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Juni'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 17
    end
    object JulHeaderPanel: TmmPanel
      Tag = 1007
      Left = 283
      Top = 130
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Juli'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 18
    end
    object AugHeaderPanel: TmmPanel
      Tag = 1008
      Left = 422
      Top = 130
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)August'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 19
    end
    object DecHeaderPanel: TmmPanel
      Tag = 1012
      Left = 422
      Top = 255
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Dezember'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 20
    end
    object NovHeaderPanel: TmmPanel
      Tag = 1011
      Left = 283
      Top = 255
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)November'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 21
    end
    object OctHeaderPanel: TmmPanel
      Tag = 1010
      Left = 144
      Top = 255
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)Oktober'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 22
    end
    object SepHeaderPanel: TmmPanel
      Tag = 1009
      Left = 5
      Top = 255
      Width = 134
      Height = 15
      BevelOuter = bvNone
      BorderStyle = bsSingle
      Caption = '(15)September'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentCtl3D = False
      ParentFont = False
      TabOrder = 23
    end
    object fcJan: TFactoryCalendar
      Tag = 1
      Left = 5
      Top = 22
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ColWidth = 18
      RowHeight = 13
      Month = 1
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
      ColWidths = (
        18
        18
        18
        18
        18
        18
        18)
    end
    object fcFeb: TFactoryCalendar
      Tag = 2
      Left = 144
      Top = 22
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      ColWidth = 18
      RowHeight = 13
      Month = 2
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcMar: TFactoryCalendar
      Tag = 3
      Left = 283
      Top = 22
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      ColWidth = 18
      RowHeight = 13
      Month = 3
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcApr: TFactoryCalendar
      Tag = 4
      Left = 422
      Top = 22
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      ColWidth = 18
      RowHeight = 13
      Month = 4
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcMay: TFactoryCalendar
      Tag = 5
      Left = 5
      Top = 147
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      ColWidth = 18
      RowHeight = 13
      Month = 5
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcJun: TFactoryCalendar
      Tag = 6
      Left = 144
      Top = 147
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      ColWidth = 18
      RowHeight = 13
      Month = 6
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcJul: TFactoryCalendar
      Tag = 7
      Left = 283
      Top = 147
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 6
      ColWidth = 18
      RowHeight = 13
      Month = 7
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcAug: TFactoryCalendar
      Tag = 8
      Left = 422
      Top = 147
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 7
      ColWidth = 18
      RowHeight = 13
      Month = 8
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcSep: TFactoryCalendar
      Tag = 9
      Left = 5
      Top = 272
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 8
      ColWidth = 18
      RowHeight = 13
      Month = 9
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcOct: TFactoryCalendar
      Tag = 10
      Left = 144
      Top = 272
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 9
      ColWidth = 18
      RowHeight = 13
      Month = 10
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcNov: TFactoryCalendar
      Tag = 11
      Left = 283
      Top = 272
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 10
      ColWidth = 18
      RowHeight = 13
      Month = 11
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
    object fcDez: TFactoryCalendar
      Tag = 12
      Left = 422
      Top = 272
      Width = 134
      Height = 99
      CalendarColors.DayColor = clWhite
      CalendarColors.DayTextColor = clBlack
      CalendarColors.MarkColor = clWhite
      CalendarColors.MarkTextColor = clBlue
      CalendarColors.OffDayColor = clWhite
      CalendarColors.OffDayTextColor = 202
      CalendarColors.TimeColor = clWindow
      CalendarColors.TimeTextColor = clBlack
      CalendarColors.TitleColor = 13224393
      CalendarColors.TitleTextColor = clBlack
      CalendarColors.WeekEndColor = 14811135
      CalendarColors.WeekEndTextColor = clBlack
      CalendarMessages.DateStr = '(8)Datum'
      CalendarMessages.Shift1Str = '(5)S1'
      CalendarMessages.Shift2Str = '(5)S2'
      CalendarMessages.Shift3Str = '(5)S3'
      CalendarMessages.Shift4Str = '(5)S4'
      CalendarMessages.Shift5Str = '(5)S5'
      CalendarMessages.ChangesStr = '(2)X'
      CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
      CalendarMessages.DaysInPastErrorMsg = 
        '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
        'ehr geaendert werden !'
      CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
      CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
      CalendarMessages.ShiftOverlappingErrorMsg = 
        '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
        'cht!'
      CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
      CalendarMessages.MinShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
        'd min.'
      CalendarMessages.MaxShiftDurationErrorMsg = 
        '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
        ' min.'
      CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
      Ctl3D = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 11
      ColWidth = 18
      RowHeight = 13
      Month = 12
      ShowWeekEndTitle = False
      StartOfWeek = wdMonday
      Year = 1998
    end
  end
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 561
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bSave: TToolButton
      Left = 34
      Top = 0
      Action = acSaveChanges
    end
    object ToolButton1: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 7
      Style = tbsSeparator
    end
    object bFactoryCal: TToolButton
      Left = 65
      Top = 0
      Action = acEditFactoryCal
    end
    object bRange: TToolButton
      Left = 88
      Top = 0
      Action = acRange
    end
    object bOptions: TToolButton
      Left = 111
      Top = 0
      Action = acEditOptions
    end
    object bPrint: TToolButton
      Left = 134
      Top = 0
      Action = acPrint
    end
    object ToolButton8: TToolButton
      Left = 157
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object bPrev: TToolButton
      Left = 165
      Top = 0
      Action = acPreviousYear
      DropdownMenu = pmYearMinus
      Grouped = True
      PopupMenu = pmYearMinus
      Style = tbsDropDown
    end
    object bNext: TToolButton
      Left = 201
      Top = 0
      Action = acNextYear
      DropdownMenu = pmYearPlus
      Grouped = True
      Style = tbsDropDown
    end
    object ToolButton10: TToolButton
      Left = 237
      Top = 0
      Width = 8
      ImageIndex = 6
      Style = tbsDivider
    end
    object cobFactoryCal: TNWComboBox
      Left = 245
      Top = 0
      Width = 165
      Height = 21
      Hint = '(*)Betriebskalender auswaehlen'
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
    end
    object ToolButton14: TToolButton
      Left = 410
      Top = 0
      Width = 8
      ImageIndex = 8
      Style = tbsDivider
    end
    object bHelp: TToolButton
      Left = 418
      Top = 0
      Action = acHelp
    end
    object mmSpeedButton1: TmmSpeedButton
      Left = 441
      Top = 0
      Width = 102
      Height = 22
      Action = acSecurity
      Flat = True
      Transparent = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 8
    Top = 32
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object pmYearPlus: TmmPopupMenu
    Left = 82
    Top = 66
    object miYear_Plus_1: TMenuItem
      Tag = 1
      Caption = '1999'
      OnClick = YearPopUpClick
    end
    object miYear_Plus_2: TMenuItem
      Tag = 2
      Caption = '2000'
      OnClick = YearPopUpClick
    end
    object miYear_Plus_3: TMenuItem
      Tag = 3
      Caption = '2001'
      OnClick = YearPopUpClick
    end
    object miYear_Plus_4: TMenuItem
      Tag = 4
      Caption = '2002'
      OnClick = YearPopUpClick
    end
    object miYear_Plus_5: TMenuItem
      Tag = 5
      Caption = '2003'
      OnClick = YearPopUpClick
    end
  end
  object pmYearMinus: TmmPopupMenu
    Left = 42
    Top = 66
    object miYear_Minus_1: TMenuItem
      Tag = -1
      Caption = '1997'
      OnClick = YearPopUpClick
    end
    object miYear_Minus_2: TMenuItem
      Tag = -2
      Caption = '1996'
      OnClick = YearPopUpClick
    end
    object miYear_Minus_3: TMenuItem
      Tag = -3
      Caption = '1995'
      OnClick = YearPopUpClick
    end
    object miYear_Minus_4: TMenuItem
      Tag = -4
      Caption = '1994'
      OnClick = YearPopUpClick
    end
    object miYear_Minus_5: TMenuItem
      Tag = -5
      Caption = '1993'
      OnClick = YearPopUpClick
    end
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 7
    Top = 64
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acSaveChanges: TAction
      Hint = '(*)Aenderungen jetzt sichern'
      ImageIndex = 3
      ShortCut = 16467
      OnExecute = acSaveChangesExecute
    end
    object acEditFactoryCal: TAction
      Hint = '(*)Betriebskalender bearbeiten, erstellen, loeschen'
      ImageIndex = 26
      OnExecute = acEditFactoryCalExecute
    end
    object acRange: TAction
      Hint = '(*)Arbeitstage-Status ueber einen gewaehlten Bereich setzen'
      ImageIndex = 27
      OnExecute = acRangeExecute
    end
    object acEditOptions: TAction
      Hint = '(*)Einstellungen bearbeiten...'
      ImageIndex = 42
      OnExecute = acEditOptionsExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acPreviousYear: TAction
      Hint = '(*)Kalender fuer vorhergehendes Jahr anzeigen'
      ImageIndex = 29
      OnExecute = acPreviousYearExecute
    end
    object acNextYear: TAction
      Hint = '(*)Kalender fuer naechstes Jahr anzeigen'
      ImageIndex = 30
      OnExecute = acNextYearExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      OnExecute = acSecurityExecute
    end
  end
  object MMSecurityControl: TMMSecurityControl
    FormCaption = '(*)Betriebskalender'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 120
    Top = 66
  end
end
