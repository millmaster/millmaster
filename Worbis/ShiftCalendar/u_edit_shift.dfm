inherited EditShiftDlg: TEditShiftDlg
  Left = 353
  Top = 288
  BorderStyle = bsDialog
  Caption = '(*)Schichtkalender'
  ClientHeight = 460
  ClientWidth = 776
  FormStyle = fsMDIChild
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000BBBB00000000000BBBB0000000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000B00000BBBB0E00000
    000000BBBB00000000000BBBB0E00BBBB00000BBBB00BBBB00000BBBB0E00BBB
    B0E000BBBB00BBBB0E000BBBB0E00BBBB0E000BBBB00BBBB0E0B0BBBB0E00BBB
    B0E000BBBBB0BBBB0E0BBBBBB0E00BBBB0E0000BBBB0BBBB0E0BBBBBB0E00BBB
    B0E000B0BBB0BBBB0E0BBBBBB0E00BBBB0E00BBB0BB0BBBB0E0BBBBBB0E00BBB
    B0E0BBBBB0B0BBBB0E00BBBBB0E00BBBB00BBBBBBB00BBBB0E000BBBB0E00BBB
    B0BBBBBBBBB0BBBB0E00000000E00BBBBBBBBB0BBBBBBBBB0E00000EEE000BBB
    BBBBB0E0BBBBBBBB0E00000000000BBBBBBB0EEE0BBBBBBB0E00000000000BBB
    BBB0EEE000BBBBBB0E00000000000BBBBB0EEE00000BBBBB0E00000000000BBB
    B0EEE0000000BBBB0E000000000000000EEE0000000000000E000000000000EE
    E0E00000000000EEE0000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81FF03FF80FF01FF807F00FF80
    7F00FF807F00FF806F00FF804700038007000180030000800000008000000080
    0000008000000080000000000000000000000000010000000180000001C00000
    01E0000001FF000801FF001C01FF003E01FF007F01FF80FF81FFC1FFC1FF}
  Position = poScreenCenter
  PrintScale = poPrintToFit
  Visible = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 776
    Height = 26
    AutoSize = True
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bSaveChanges: TToolButton
      Left = 34
      Top = 0
      Action = acSaveChanges
    end
    object ToolButton5: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bEditShiftCalendar: TToolButton
      Left = 65
      Top = 0
      Action = acEditShiftCalendar
    end
    object ToolButton4: TToolButton
      Left = 88
      Top = 0
      Width = 8
      ImageIndex = 7
      Style = tbsSeparator
    end
    object bEditTeam: TToolButton
      Left = 96
      Top = 0
      Action = acEditTeam
    end
    object bViewDefaulShift: TToolButton
      Left = 119
      Top = 0
      Action = acEditDefaultShifts
      DropdownMenu = pmDefaultShift
      Style = tbsDropDown
    end
    object bViewShiftPattern: TToolButton
      Left = 155
      Top = 0
      Action = acEditShiftPattern
      DropdownMenu = pmShiftPattern
      Style = tbsDropDown
    end
    object ToolButton8: TToolButton
      Left = 191
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object bOptions: TToolButton
      Left = 199
      Top = 0
      Action = acEditOptions
    end
    object ToolButton3: TToolButton
      Left = 222
      Top = 0
      Width = 8
      ImageIndex = 6
      Style = tbsSeparator
    end
    object bPrint: TToolButton
      Left = 230
      Top = 0
      Action = acPrint
      DropdownMenu = pmPrint
      Style = tbsDropDown
    end
    object ToolButton10: TToolButton
      Left = 266
      Top = 0
      Width = 8
      ImageIndex = 6
      Style = tbsDivider
    end
    object cobShiftCal: TNWComboBox
      Left = 274
      Top = 0
      Width = 165
      Height = 21
      Hint = '(*)Schichtkalender auswaehlen'
      Style = csDropDownList
      DropDownCount = 12
      ItemHeight = 13
      Sorted = True
      TabOrder = 0
      OnChange = cobShiftCalChange
    end
    object ToolButton14: TToolButton
      Left = 439
      Top = 0
      Width = 8
      ImageIndex = 8
      Style = tbsDivider
    end
    object bHelp: TToolButton
      Left = 447
      Top = 0
      Action = acHelp
    end
    object mmSpeedButton1: TmmSpeedButton
      Left = 470
      Top = 0
      Width = 102
      Height = 22
      Action = acSecurity
      Flat = True
      Transparent = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object MainPanel: TmmPanel
    Left = 0
    Top = 26
    Width = 776
    Height = 434
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    object kMonthTab: TmmTabControl
      Left = 3
      Top = 3
      Width = 770
      Height = 428
      Align = alClient
      BiDiMode = bdLeftToRight
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      HotTrack = True
      ParentBiDiMode = False
      ParentFont = False
      TabOrder = 0
      Tabs.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6'
        '7'
        '8'
        '9'
        '10'
        '11'
        '12')
      TabIndex = 0
      TabWidth = 50
      OnChange = kMonthTabChange
      OnChanging = kMonthTabChanging
      object Panel1: TmmPanel
        Left = 4
        Top = 24
        Width = 762
        Height = 400
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 3
        TabOrder = 0
        object kspGrid: TShiftCalendarGrid
          Left = 3
          Top = 3
          Width = 756
          Height = 394
          CalendarColors.DayColor = 15724527
          CalendarColors.DayTextColor = clBlack
          CalendarColors.MarkColor = 202
          CalendarColors.MarkTextColor = clYellow
          CalendarColors.OffDayColor = 15724527
          CalendarColors.OffDayTextColor = 202
          CalendarColors.TimeColor = clWindow
          CalendarColors.TimeTextColor = clBlack
          CalendarColors.TitleColor = 15983582
          CalendarColors.TitleTextColor = clBlack
          CalendarColors.WeekEndColor = 14811135
          CalendarColors.WeekEndTextColor = clBlack
          CalendarMessages.DateStr = '(8)Datum'
          CalendarMessages.Shift1Str = '(5)S1'
          CalendarMessages.Shift2Str = '(5)S2'
          CalendarMessages.Shift3Str = '(5)S3'
          CalendarMessages.Shift4Str = '(5)S4'
          CalendarMessages.Shift5Str = '(5)S5'
          CalendarMessages.ChangesStr = '(2)X'
          CalendarMessages.PatternErrMsg = '(255)Schichtgruppe nicht vorhanden !'
          CalendarMessages.DaysInPastErrorMsg = 
            '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht m' +
            'ehr geaendert werden !'
          CalendarMessages.DateShiftStr = '(50)Datum / Schicht: %s'
          CalendarMessages.FirstShiftEmptyErrorMsg = '(255)1. Schicht darf nicht leer sein!'
          CalendarMessages.ShiftOverlappingErrorMsg = 
            '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schi' +
            'cht!'
          CalendarMessages.ShiftGapErrorMsg = '(255)Luecken in Schichtfolge sind nicht erlaubt!'
          CalendarMessages.MinShiftDurationErrorMsg = 
            '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %' +
            'd min.'
          CalendarMessages.MaxShiftDurationErrorMsg = 
            '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d' +
            ' min.'
          CalendarMessages.TeamNotFoundErrorMsg = '(255)Schichtgruppe / Team nicht vorhanden!'
          Ctl3D = False
          DatabaseConnection = dmShiftCalendar.conDefault
          TabOrder = 0
          Month = 3
          Year = 1999
          ColWidths = (
            60
            55
            55
            55
            55
            55
            15
            40
            60
            55
            55
            55
            55
            55
            15)
          RowHeights = (
            24
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22
            22)
        end
      end
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    OnLanguageChange = mmTranslatorLanguageChange
    Left = 40
    Top = 88
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Tabs'
        0))
  end
  object mmActionList: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 632
    Top = 10
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acEditTeam: TAction
      Hint = '(*)Schichtgruppen bearbeiten...'
      ImageIndex = 41
      OnExecute = acEditTeamExecute
    end
    object acEditShiftCalendar: TAction
      Hint = '(*)Schichtkalender erstellen, bearbeiten...'
      ImageIndex = 26
      OnExecute = acEditShiftCalendarExecute
    end
    object acEditShiftPattern: TAction
      Caption = '(*)Schichtmuster bearbeiten...'
      Hint = '(*)Schichtmuster bearbeiten...'
      ImageIndex = 40
      OnExecute = acEditShiftPatternExecute
    end
    object acEditDefaultShifts: TAction
      Caption = '(*)Standardschichten bearbeiten...'
      Hint = '(*)Standardschichten bearbeiten...'
      ImageIndex = 36
      OnExecute = acEditDefaultShiftsExecute
    end
    object acEditOptions: TAction
      Hint = '(*)Einstellungen bearbeiten...'
      ImageIndex = 42
      OnExecute = acEditOptionsExecute
    end
    object acPrint: TAction
      Hint = '(*)Aktuellen Schichtkalender ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintPageExecute
    end
    object acPrintPage: TAction
      Caption = '(*)Aktuellen Schichtkalender ausdrucken...'
      Hint = '(*)Aktuellen Schichtkalender ausdrucken...'
      OnExecute = acPrintPageExecute
    end
    object acPrintRange: TAction
      Caption = '(*)Mehrere Schichtkalender-Monate ausdrucken...'
      Hint = '(*)Mehrere Schichtkalender-Monate ausdrucken...'
      OnExecute = acPrintRangeExecute
    end
    object acSaveChanges: TAction
      Hint = '(*)Aenderungen jetzt sichern'
      ImageIndex = 3
      ShortCut = 16467
      OnExecute = acSaveChangesExecute
    end
    object acCopyDefaultShift: TAction
      Caption = '(*)Standardschichten uebernehmen...'
      Hint = '(*)Standardschichten uebernehmen...'
      ImageIndex = 36
      OnExecute = acCopyDefaultShiftExecute
    end
    object acCopyShiftPattern: TAction
      Caption = '(*)Schicht-Muster uebernehmen...'
      Hint = '(*)Schicht-Muster uebernehmen...'
      ImageIndex = 38
      OnExecute = acCopyShiftPatternExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      OnExecute = acSecurityExecute
    end
  end
  object RefreshTimer: TmmTimer
    Interval = 120000
    OnTimer = RefreshTimerTimer
    Left = 39
    Top = 125
  end
  object pmPrint: TmmPopupMenu
    Left = 127
    Top = 125
    object miPrintPage: TMenuItem
      Action = acPrintPage
    end
    object miPrintRange: TMenuItem
      Action = acPrintRange
    end
  end
  object pmShiftPattern: TmmPopupMenu
    Left = 160
    Top = 125
    object miEditShiftPattern: TMenuItem
      Action = acEditShiftPattern
    end
    object miCopyShiftPattern: TMenuItem
      Action = acCopyShiftPattern
    end
  end
  object pmDefaultShift: TmmPopupMenu
    Left = 192
    Top = 125
    object miEditDefaultShifts: TMenuItem
      Action = acEditDefaultShifts
    end
    object miCopyDefaultShift: TMenuItem
      Action = acCopyDefaultShift
    end
  end
  object MMSecurityControl: TMMSecurityControl
    FormCaption = '(*)Schichtkalender'
    Active = True
    Left = 662
    Top = 12
  end
end
