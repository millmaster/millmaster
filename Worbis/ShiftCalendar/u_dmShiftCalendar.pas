(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_dmShiftCalendar.pas
| Projectpart...: MillMaster Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Optionen fuer Schichtkalender / Betriebskalender bearbeiten
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 29.10.02 | 1.00 | LOK | Datei erstellt
|=========================================================================================*)
unit u_dmShiftCalendar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADOConnection;

type
  TdmShiftCalendar = class(TDataModule)
    conDefault: TmmADOConnection;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmShiftCalendar: TdmShiftCalendar;

implementation

{$R *.DFM}

end.
