(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_readwrite_options.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Vorlaeufig !
|                 Laden und Speichern der Programm-Optionen in der Registry
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 21.09.99 | 0.03 | PW  | Using TMemoINI to save settings in DB
| 28.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_readwrite_options;
interface
uses
 SysUtils,Graphics,Registry;

 procedure WriteOptions;
 procedure ReadOptions;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 MemoINI,mmLib, LoepfeGlobal,
 u_main,u_common_lib,u_common_global,u_global,u_lib;

procedure WriteOptions;
var
 xINI      : TMemoINI;
begin
 with gProgOptions do
 begin
  xINI := TMemoINI.Create;
  xINI.AdoConnectionString := Format(cDefaultConnectionString,[gSQLServerName,gDBName,gDBPassword,mmUsername]);
  xINI.Formname := 'SCHICHTKALENDER_MAIN';
  xINI.Profilename := 'DEFAULT_SET';

  try
   xINI.WriteBool('Common','PrintColors',PrintColors);

   xINI.WriteInteger('ShiftCalendar','ShiftsPerDay',ShiftsPerDay);
   xINI.WriteBool('ShiftCalendar','ShowShiftWeekEnds',ShowShiftWeekEnds);
   xINI.WriteBool('ShiftCalendar','ShowTeamColors',ShowTeamColors);
   xINI.WriteBool('ShiftCalendar','ShowMarks',ShowMarks);
   xINI.WriteString('ShiftCalendar','MarkChar',MarkChar);
   xINI.WriteInteger('ShiftCalendar','DayText',ShiftCalendarColors[ciDay,cpText]);
   xINI.WriteInteger('ShiftCalendar','DayBack',ShiftCalendarColors[ciDay,cpBackground]);
   xINI.WriteInteger('ShiftCalendar','OffDayText',ShiftCalendarColors[ciOffDay,cpText]);
   xINI.WriteInteger('ShiftCalendar','OffDayBack',ShiftCalendarColors[ciOffDay,cpBackground]);
   xINI.WriteInteger('ShiftCalendar','MarkText',ShiftCalendarColors[ciMark,cpText]);
   xINI.WriteInteger('ShiftCalendar','MarkBack',ShiftCalendarColors[ciMark,cpBackground]);
   xINI.WriteInteger('ShiftCalendar','TimeText',ShiftCalendarColors[ciTime,cpText]);
   xINI.WriteInteger('ShiftCalendar','TimeBack',ShiftCalendarColors[ciTime,cpBackground]);
   xINI.WriteInteger('ShiftCalendar','TitleText',ShiftCalendarColors[ciTitle,cpText]);
   xINI.WriteInteger('ShiftCalendar','TitleBack',ShiftCalendarColors[ciTitle,cpBackground]);
   xINI.WriteInteger('ShiftCalendar','WeekEndText',ShiftCalendarColors[ciWeekEnd,cpText]);
   xINI.WriteInteger('ShiftCalendar','WeekEndBack',ShiftCalendarColors[ciWeekEnd,cpBackground]);

   xINI.WriteBool('FactoryCalendar','ShowCalWeekEnds',ShowCalWeekEnds);
   xINI.WriteInteger('FactoryCalendar','StartOfWeek',StartOfWeek);
   xINI.WriteInteger('FactoryCalendar','WeekEndDays',WeekEndDays);
   xINI.WriteInteger('FactoryCalendar','DayText',YearCalendarColors[ciDay,cpText]);
   xINI.WriteInteger('FactoryCalendar','DayBack',YearCalendarColors[ciDay,cpBackground]);
   xINI.WriteInteger('FactoryCalendar','OffDayText',YearCalendarColors[ciOffDay,cpText]);
   xINI.WriteInteger('FactoryCalendar','OffDayBack',YearCalendarColors[ciOffDay,cpBackground] );
   xINI.WriteInteger('FactoryCalendar','MarkText',YearCalendarColors[ciMark,cpText]);
   xINI.WriteInteger('FactoryCalendar','MarkBack',YearCalendarColors[ciMark,cpBackground]);
   xINI.WriteInteger('FactoryCalendar','TimeText',YearCalendarColors[ciTime,cpText]);
   xINI.WriteInteger('FactoryCalendar','TimeBack',YearCalendarColors[ciTime,cpBackground]);
   xINI.WriteInteger('FactoryCalendar','TitleText',YearCalendarColors[ciTitle,cpText]);
   xINI.WriteInteger('FactoryCalendar','TitleBack',YearCalendarColors[ciTitle,cpBackground]);
   xINI.WriteInteger('FactoryCalendar','WeekEndText',YearCalendarColors[ciWeekEnd,cpText]);
   xINI.WriteInteger('FactoryCalendar','WeekEndBack',YearCalendarColors[ciWeekEnd,cpBackground]);

   xINI.WriteSettings;
  finally
   xINI.Destroy;
  end; //try..finally
 end; //with gProgOptions do
end; //procedure WriteOptions
//-----------------------------------------------------------------------------
procedure ReadOptions;
var
 xINI          : TMemoINI;

begin
 with gProgOptions do
 begin
  fillchar(gProgOptions,sizeof(rProgrammOptionT),0);

  xINI := TMemoINI.Create;
  xINI.AdoConnectionString := Format(cDefaultConnectionString,[gSQLServerName,gDBName,gDBPassword,mmUsername]);
  xINI.Formname := 'SCHICHTKALENDER_MAIN';
  xINI.Profilename := 'DEFAULT_SET';

  xINI.ReadSettings;
  try
   PrintColors := xINI.ReadBool('Common','PrintColors',true);

   ShiftsPerDay := xINI.ReadInteger('ShiftCalendar','ShiftsPerDay',5);
   ShowTeamColors := xINI.ReadBool('ShiftCalendar','ShowTeamColors',true);
   ShowShiftWeekEnds := xINI.ReadBool('ShiftCalendar','ShowShiftWeekEnds',true);
   ShowMarks := xINI.ReadBool('ShiftCalendar','ShowMarks',true);
   MarkChar := xINI.ReadString('ShiftCalendar','MarkChar','*');
   ShiftCalendarColors[ciDay,cpText] := xINI.ReadInteger('ShiftCalendar','DayText',clBlack);
   ShiftCalendarColors[ciDay,cpBackground] := xINI.ReadInteger('ShiftCalendar','DayBack',clLightGray1);
   ShiftCalendarColors[ciOffDay,cpText] := xINI.ReadInteger('ShiftCalendar','OffDayText',clRed);
   ShiftCalendarColors[ciOffDay,cpBackground] := xINI.ReadInteger('ShiftCalendar','OffDayBack',clLightGray1);
   ShiftCalendarColors[ciMark,cpText] := xINI.ReadInteger('ShiftCalendar','MarkText',clYellow);
   ShiftCalendarColors[ciMark,cpBackground] := xINI.ReadInteger('ShiftCalendar','MarkBack',clRed);
   ShiftCalendarColors[ciTime,cpText] := xINI.ReadInteger('ShiftCalendar','TimeText',clBlue);
   ShiftCalendarColors[ciTime,cpBackground] := xINI.ReadInteger('ShiftCalendar','TimeBack',clWhite);
   ShiftCalendarColors[ciTitle,cpText] := xINI.ReadInteger('ShiftCalendar','TitleText',clBlack);
   ShiftCalendarColors[ciTitle,cpBackground] := xINI.ReadInteger('ShiftCalendar','TitleBack',clSilver);
   ShiftCalendarColors[ciWeekEnd,cpText] := xINI.ReadInteger('ShiftCalendar','WeekEndText',clBlack);
   ShiftCalendarColors[ciWeekEnd,cpBackground] := xINI.ReadInteger('ShiftCalendar','WeekEndBack',clLightYellow);

   ShowCalWeekEnds := xINI.ReadBool('FactoryCalendar','ShowCalWeekEnds',true);
   StartOfWeek := xINI.ReadInteger('FactoryCalendar','StartOfWeek',2);
   WeekEndDays := xINI.ReadInteger('FactoryCalendar','WeekEndDays',65);
   YearCalendarColors[ciDay,cpText] := xINI.ReadInteger('FactoryCalendar','DayText',clBlack);
   YearCalendarColors[ciDay,cpBackground] := xINI.ReadInteger('FactoryCalendar','DayBack',clWhite);
   YearCalendarColors[ciOffDay,cpText] := xINI.ReadInteger('FactoryCalendar','OffDayText',clRed);
   YearCalendarColors[ciOffDay,cpBackground] := xINI.ReadInteger('FactoryCalendar','OffDayBack',clWhite);
   YearCalendarColors[ciTitle,cpText] := xINI.ReadInteger('FactoryCalendar','TitleText',clBlack);
   YearCalendarColors[ciTitle,cpBackground] := xINI.ReadInteger('FactoryCalendar','TitleBack',clSilver);
   YearCalendarColors[ciWeekEnd,cpText] := xINI.ReadInteger('FactoryCalendar','WeekEndText',clBlack);
   YearCalendarColors[ciWeekEnd,cpBackground] := xINI.ReadInteger('FactoryCalendar','WeekEndBack',clLightYellow);
  finally
   xINI.Destroy;
  end; //try..finally
 end; //with gProgOptions do
end; //procedure ReadOptions

end. //u_readwrite_options
