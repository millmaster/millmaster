(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_set_factorycal_range.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog zum Aendern eines waehlbaren Bereiches / Intervalles im Betriebskalender
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
| 13.05.99 | 0.02 | PW  | units u_common_global and u_common_lib added
| 29.10.99 | 0.03 | PW  | TToolButton/TToolBar replaced by TButton
| 17.05.00 | 0.03 | Wss | acOKExecute: Date format changed to DateToStr()
| 28.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$I symbols.inc}

unit u_set_factorycal_range;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs, ExtCtrls,
  StdCtrls, Buttons, DB, ComCtrls, ToolWin, ActnList, mmActionList,
  mmToolBar, mmRadioGroup, mmDateTimePicker, mmLabel, mmGroupBox, mmPanel,
  IvDictio, IvMulti, IvEMulti, mmTranslator, BaseForm, mmButton, u_dmShiftCalendar,
  ADODB, mmADODataSet, mmADOCommand;

type
  TSetFactoryCalRangeDlg = class(TmmForm)
    MainPanel: TmmPanel;
    gbPeriod: TmmGroupBox;
    laFrom: TmmLabel;
    laTo: TmmLabel;
    dtVon: TmmDateTimePicker;
    dtBis: TmmDateTimePicker;
    rgSelectDay: TmmRadioGroup;
    rgState: TmmRadioGroup;
    queDelete: TmmADOCommand;
    queInsert: TmmADOCommand;
    tabFactoryCal: TmmADODataSet;

    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;

    mmTranslator: TmmTranslator;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    procedure Init;
  public
    procedure SetKalenderID(aID: longint);
  end; //TSetCalendarRangeDlg

var
  SetFactoryCalRangeDlg: TSetFactoryCalRangeDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs,
  u_common_lib, u_common_global, u_main, u_global, u_lib, u_edit_exceptdays;

{$R *.DFM}

var
  gKalenderID: longint;

resourcestring
  cDialog_Caption = '(40)Kalenderbereich [%s] bearbeiten'; //ivlm
  cMsgStr1 = '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht mehr geaendert werden !'; //ivlm
  cMsgStr2 = '(255)Datums-Bereich [%s - %s] fuer Tag(e) [%s] auf Status [%s] setzen ?'; //ivlm

procedure TSetFactoryCalRangeDlg.SetKalenderID(aID: longint);
begin
  gKalenderID := aID;
end; //procedure TSetFactoryCalRangeDlg.SetKalenderID
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.Init;
begin
  tabFactoryCal.Open;
  tabFactoryCal.Locate('C_FACTORY_CALENDAR_ID', gKalenderID, []);
  Caption := Format({LTrans{}(cDialog_Caption), [tabFactoryCal.FieldByName('c_factory_calendar_name').AsString]);
  tabFactoryCal.Close;
end; //procedure TSetFactoryCalRangeDlg.Init
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalenderbereich_bearbeiten.htm');
 //dtVon.date := Encodedate(EditExceptDaysDlg.fcJan.Year,1,1);
 //dtBis.date := Encodedate(EditExceptDaysDlg.fcJan.Year,1,1);
  dtVon.time := 0;
  dtBis.time := 0;
  rgSelectDay.ItemIndex := 0;
  rgState.ItemIndex := 0;
// HelpContext := hcSetFactoryCalRange;
end; //procedure TSetCalendarRangeDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.FormShow(Sender: TObject);
begin
  Init;
  Screen.Cursor := crDefault;
end; //procedure TSetFactoryCalRangeDlg.FormShow
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.acOKExecute(Sender: TObject);
const
  cMaxTage = 730; //max. 2 Jahre aufeinmal setzen...

var
  aTag,
    sTag: integer;
  vDatum,
    bDatum: TDateTime;
  hStr: string;

begin
  vDatum := dtVon.date;
  bDatum := dtBis.date;
  if vDatum > bDatum then ChangeVars(vDatum, bDatum, sizeof(vDatum));
  if vDatum < date then begin
    MMMessageDlg({LTrans{}(cMsgStr1), mtError, [mbOk], 0, Self);
    Exit;
  end; //if vDatum < date then

  aTag := rgSelectDay.ItemIndex;
  sTag := rgState.ItemIndex;

  if (bDatum - vDatum) > cMaxTage then bDatum := vDatum + cMaxTage;

//Wss hstr := format({LTrans{}(cMsgStr2),[formatdatetime_('dd/mm/yy',vDatum),formatdatetime_('dd/mm/yy',bDatum),rgSelectDay.Items[aTag],rgState.Items[sTag]]);
  hstr := Format({LTrans{}(cMsgStr2), [formatdatetime(ShortDateFormat, vDatum), formatdatetime(ShortDateFormat, bDatum), rgSelectDay.Items[aTag], rgState.Items[sTag]]);
  if MMMessageDlg(hStr, mtConfirmation, mbOkCancel, 0, Self) <> mrOk then exit;

  Screen.Cursor := crHourglass;
  try
    if sTag = 0 then begin //status = arbeitstag, d.h. ggf. datensaetze loeschen
      while vDatum <= bDatum do begin
        if (aTag = 7) or ((aTag <> 7) and (DayOfWeek(vDatum) = (aTag + 1))) then begin
          queDelete.Parameters.ParamByName('tag').DataType := ftDateTime;
          queDelete.Parameters.ParamByName('tag').value := vDatum;
          queDelete.Parameters.ParamByName('kalenderid').value := gKalenderID;
          queDelete.Execute;
        end; //if (aTag = 7) or ((aTag <> 7) and (DayOfWeek(vDatum) = (aTag +1))) then
        vDatum := vDatum + 1;
      end; //while vDatum <= bDatum do
    end //if sTag = 0 then
    else begin //status = arbeitsfrei, d.h. ggf. datensaetze einfuegen
      while vDatum <= bDatum do begin
        if (aTag = 7) or ((aTag <> 7) and (DayOfWeek(vDatum) = (aTag + 1))) then begin
          queInsert.Parameters.ParamByName('tag').DataType := ftDateTime;
          queInsert.Parameters.ParamByName('tag').value := vDatum;
          queInsert.Parameters.ParamByName('kalenderid').value := gKalenderID;
          try
            queInsert.Execute;
          except
      //exception occurs if record already exists
          end; //
        end; //if (aTag = 7) or ((aTag <> 7) and (DayOfWeek(vDatum) = (aTag +1))) then
        vDatum := vDatum + 1;
      end; //while vDatum <= bDatum do
    end; //else if sTag = 0 then
  finally
    Screen.Cursor := crDefault;
  end; //try..finally

  Close;
end; //procedure TSetFactoryCalRangeDlg.ActionOKExecute
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.acCancelExecute(Sender: TObject);
begin
  Close;
end; //procedure TSetFactoryCalRangeDlg.ActionCancelExecute
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TSetFactoryCalRangeDlg.ActionHelpExecute
//-----------------------------------------------------------------------------
procedure TSetFactoryCalRangeDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TSetFactoryCalRangeDlg.FormClose

end. //u_set_factorycal_range.pas

