(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_resourcestrings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Strings (fuer Uebersetzung)
| Info..........: -
| Develop.system: Windows NT 4.0 SP 3
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 21. 1.99 | 0.01 | PW  | Aenderungen fuer Multilizer
|=========================================================================================*)

{$i symbols.inc}

unit u_resourcestrings;
interface

resourcestring
 cHint_bOK                   = '(*)Eingaben uebernehmen, Dialog beenden';
 cHint_bCancel               = '(*)Dialog abbrechen';
 cHint_bPrint                = '(*)Bericht, Formular ausdrucken...';
 cHint_bHelp                 = '(*)Hilfetext anzeigen...';
 cHint_bClose                = '(*)Fenster schliessen';
 cHint_bSave                 = '(*)Aenderungen jetzt sichern';
 cHint_bShowTeam             = '(*)Schichtfarben anzeigen';
 cHint_bPrinter              = '(*)Druckereinstellungen aufrufen...';
 cCaption_bPrint             = '&Drucken';
 cCaption_bCancel            = '&Abbrechen';
 cCaption_bPrinter           = 'D&rucker...';
 cCaption_bHelp              = '&Hilfe...';
 cMessage_IsLinked           = 'Datensatz wird noch benoetigt und kann daher nicht geloescht werden !';

 cCaption_ActionClose        = '&Beenden';
 cCaption_ActionEditOptions  = '&Einstellungen...';
 cHint_ActionEditOptions     = '(*)Einstellungen bearbeiten';
 cCaption_ActionAboutDlg     = '&Info...';
 cHint_ActionAboutDlg        = 'Copyright-Informationen anzeigen';
 cCaption_ActionSaveChanges  = '&Speichern...';
 cCaption_ActionHelpContent  = '&Inhaltsverzeichnis...';
 cHint_ActionHelpContent     = 'Inhaltsverzeichnis aufrufen';
 cCaption_ActionHelpSearch   = '&Suchen...';
 cHint_ActionHelpSearch      = 'Nach Stichwort suchen';
 cCaption_ActionHelpUse      = '&Hilfe benutzen...';
 cHint_ActionHelpUse         = 'Informationen zur Benutzung des Hilfesystems anzeigen';
 cCaption_ActionPrint        = '&Drucken...';
 cCaption_ActionPrinterSetup = '&Druckereinstellungen...';

 cCaption_miFile             = '&Datei';
 cCaption_miEdit             = '&Bearbeiten';
 cCaption_miHelp             = '&?';

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end. //u_resourcestrings.pas
