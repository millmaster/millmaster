{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_select_ym_settings.pas
| Projectpart...: Millmaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 21.06.2001  1.00  pw  | file created
| 25.06.2001        Wss | Title of both listbox changed
| 14.08.2001  1.02  Nue | c_head_class inserted.
| 10.10.2002  1.02  LOK | Umbau ADO
| 15.01.2004        Wss | Kontexthilfe f�r Dialog implementiert
| 18.01.2005  2.00  SDo | Umbau & Anpassungen an XML Vers. 5
| 24.06.2008  2.01  Nue | Verbreitern der Listboxen von 200 auf 260
|=========================================================================================*}
unit u_select_ym_settings;

interface

uses
  u_common_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SELECTIONDIALOG, ActnList, mmActionList, Buttons, mmSpeedButton,
  StdCtrls, mmListBox, mmLabel, mmButton, ExtCtrls, mmPanel, Db,
  IvDictio, IvMulti, IvEMulti, mmTranslator, u_dmStyle, ADODB,
  mmADODataSet;

type
  TfrmSelectYMSettings = class(TfrmSelectionDialog)
    qry1: TmmADODataSet;
    mmTranslator: TmmTranslator;
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure lbSourceDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure acHelpExecute(Sender: TObject);
  private
  public
    constructor Create(aOwner: TComponent; aInt: aIntegerT); reintroduce; virtual;
    function GetSelection: aIntegerT;
  end;                                  //TfrmSelectYMSettings

var
  frmSelectYMSettings: TfrmSelectYMSettings;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  u_main, u_common_lib, u_resourcestrings,
  YMParaDef, MMHtmlHelp,                            //Nue:14:8.01
  XMLDef;

//------------------------------------------------------------------------------
constructor TfrmSelectYMSettings.Create(aOwner: TComponent;
  aInt: aIntegerT);
var
  xStr: string;

  function IsAssigned(aID: integer): boolean;
  var
    i: integer;
  begin
    result := false;
    for i := 0 to high(aInt) do begin
      result := aInt[i] = aID;
      if result then break;
    end;                                //for i := 0 to high(aInt) do begin
  end;                                  //function IsAssigned

begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Reinigervorlage_dem_Artikel_zuweisen.htm');

  with qry1 do try
    lbSource.Items.BeginUpdate;
    lbDest.Items.BeginUpdate;
    Open;
    while not Eof do begin
      xStr := Format('%s@%s', [fieldbyname('c_ym_set_name').asstring, cSensingHeadClassNames[TSensingHeadClass(fieldbyname('c_head_class').asinteger)]]);
      if IsAssigned(fieldbyname('c_ym_set_id').asinteger) then
        lbDest.Items.AddObject(xStr, TObject(fieldbyname('c_ym_set_id').asinteger))
      else
        lbSource.Items.AddObject(xStr, TObject(fieldbyname('c_ym_set_id').asinteger));
      Next;
    end;                                //while not Eof do begin
  finally
    Active := false;
    lbSource.Items.EndUpdate;
    lbDest.Items.EndUpdate;
  end;                                  //try..finally
end;                                    //constructor TfrmSelectYMSettings.Create
//-----------------------------------------------------------------------------
function TfrmSelectYMSettings.GetSelection: aIntegerT;
var
  i: integer;
begin
  result := nil;
  if ModalResult = mrOK then begin
    SetLength(result, lbDest.Items.Count);
    for i := 0 to lbDest.Items.Count - 1 do
      result[i] := integer(lbDest.Items.Objects[i]);
  end;                                  //if ModalResult = mrOK then begin
end;                                    //function TfrmSelectYMSettings.GetSelection
//-----------------------------------------------------------------------------
procedure TfrmSelectYMSettings.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  acOK.Enabled := lbDest.Items.Count > 0;
end;                                    //procedure TfrmSelectYMSettings.mmActionListUpdate
//------------------------------------------------------------------------------
procedure TfrmSelectYMSettings.lbSourceDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  xStr1, xStr2: String;
  xPos: Integer;
begin
  with Control as TListBox do begin
    xStr1 := Items.Strings[Index];
    xPos := Pos('@', xStr1);
    if xPos > 0 then begin
      xStr2 := Copy(xStr1, xPos+1, Length(xStr1) - xPos);
      xStr1 := Copy(xStr1, 1, xPos - 1);
    end else
      xStr2 := ' ';
    with Canvas do begin
      if odSelected in State then begin
        Brush.Color := clHighlight;
        Font.Color := clHighlightText;
      end;
      FillRect(Rect);
      TextOut(Rect.Left + 2,   Rect.Top + 0, xStr1);
      TextOut(Rect.Right - 80, Rect.Top + 0, xStr2);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmSelectYMSettings.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

end. //u_select_ym_settings

