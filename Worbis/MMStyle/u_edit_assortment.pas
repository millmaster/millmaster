(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_assortment.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assortment Edit Form
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.00 | 0.00 | PW  | Datei erstellt
| 11.04.01 |      |     | edit/readonly state for mm-dbcontrols implemented
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 15.05.2001       Wss | call edc_assortment_name.SetFocus in AfterInsert event
| 25.06.2001       Wss | Icon for Userfields changed
| 22.06.2001       Wss | Width of user fields changed to 375 instead of 550
| 29.08.2002       Wss | Update of valid functions in mmNavigator moved to OnShow event because
                         at creation time is to early
| 10.10.2002       LOK | Umbau ADO
| 25.10.2002       Wss | Diverse Bugfixes
                         - Filteraktivierung war gegensetzlich von Editmode von Tabelle
                         - Edit/Delete Sortiment in Security hinzugefügt
| 22.11.2002       Wss | Refresh Button entfernt
|=========================================================================================*)

{$i symbols.inc}

{ $define ShowUserGrayed}

unit u_edit_assortment;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, ActnList, ToolWin, ComCtrls, IvDictio, IvMulti, IvEMulti,
  mmTranslator, Menus, mmPopupMenu, ExtCtrls, DBCtrls, mmToolBar, mmActionList,
  mmStatusBar, mmPanel, Db, mmDataSource, StdCtrls,
  mmGroupBox, mmDBNavigator, Mask, mmDBEdit, Buttons, mmSpeedButton,
  mmDBLookupComboBox, mmLabel, Nwcombo, NumCtrl, mmComboBox, wwdbedit,
  {Wwdotdot,} Wwdbcomb, IvMlDlgs, mmColorDialog, mmDBText, mmColorButton,
  MMSecurity, Wwdatsrc{, Wwquery}, mmBevel, Grids, Wwdbigrd, Wwdbgrid{, Wwkeycb},
  mmGraphGlobal, Printers, mmDbValCb, ImgList, u_dmStyle, ADODB,
  mmADODataSet, Wwkeycb;

type
  TfrmEditAssortment = class(TmmForm)
    mmActionList1: TmmActionList;
    acClose: TAction;
    acApplyFilter: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acCopyAssortment: TAction;
    acUserFields: TAction;
    acDeleteAssortment: TAction;
    acSecurity: TAction;
    acEditAssortment: TAction;
    tabAssortment: TmmADODataSet;
    dsAssortment: TmmDataSource;
    tabAssortmentStyleData: TBooleanField;
    tabAssortmentc_assortment_id: TSmallintField;
    tabAssortmentc_assortment_name: TStringField;
    tabAssortmentc_assortment_state: TSmallintField;
    tabAssortmentc_num1: TFloatField;
    tabAssortmentc_num2: TFloatField;
    tabAssortmentc_num3: TFloatField;
    tabAssortmentc_string1: TStringField;
    tabAssortmentc_string2: TStringField;
    tabAssortmentc_string3: TStringField;
    tabAssortmentc_string4: TStringField;
    tabAssortmentc_string5: TStringField;
    tabAssortmentc_string6: TStringField;
    tabAssortmentc_string7: TStringField;
    qry1: TmmADODataSet;
    qrySelect: TmmADODataSet;
    dsSelect: TwwDataSource;
    qrySelectc_assortment_id: TSmallintField;
    qrySelectc_assortment_name: TStringField;
    qrySelectc_assortment_state: TSmallintField;

    MMSecurityControl1: TMMSecurityControl;
    paList: TmmPanel;
    mmToolBar2: TmmToolBar;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton15: TToolButton;
    navSelect: TmmDBNavigator;
    mmPanel2: TmmPanel;
    sbRecordCount: TmmStatusBar;
    paEdit: TmmPanel;
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    laAssortmentname: TmmLabel;
    edc_assortment_name: TmmDBEdit;
    mmGroupBox4: TmmGroupBox;
    laState: TmmLabel;
    mmLabel16: TmmLabel;
    comAssortmentState: TmmDBValComboBox;
    gbUserFields: TmmGroupBox;
    laUserField1: TmmLabel;
    laUserField2: TmmLabel;
    laUserField3: TmmLabel;
    laUserField4: TmmLabel;
    laUserField5: TmmLabel;
    laUserField6: TmmLabel;
    laUserField7: TmmLabel;
    laUserField8: TmmLabel;
    laUserField9: TmmLabel;
    laUserField10: TmmLabel;
    edUserField1: TmmDBEdit;
    edUserField2: TmmDBEdit;
    edUserField3: TmmDBEdit;
    edUserField4: TmmDBEdit;
    edUserField5: TmmDBEdit;
    edUserField6: TmmDBEdit;
    edUserField7: TmmDBEdit;
    edUserField8: TmmDBEdit;
    edUserField9: TmmDBEdit;
    edUserField10: TmmDBEdit;
    mmStatusBar1: TmmStatusBar;
    mmToolBar1: TmmToolBar;
    ToolButton11: TToolButton;
    ToolButton9: TToolButton;
    ToolButton5: TToolButton;
    ToolButton12: TToolButton;
    ToolButton10: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    mmDBNavigator1: TmmDBNavigator;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    gbList: TmmGroupBox;
    mmLabel1: TmmLabel;
    isSearch: TwwIncrementalSearch;
    grSelect: TwwDBGrid;
    ToolButton1: TToolButton;
    acCancelFilter: TAction;
    acSortAssortment: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    mmTranslator: TmmTranslator;
    ilBitmaps: TImageList;
    imgHistorical: TImage;

    procedure acCloseExecute(Sender: TObject);
    procedure acApplyFilterExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acCopyAssortmentExecute(Sender: TObject);
    procedure tabAssortmentBeforePost(DataSet: TDataSet);
    procedure tabAssortmentCalcFields(DataSet: TDataSet);
    procedure tabAssortmentNewRecord(DataSet: TDataSet);
    procedure tabAssortmentAfterScroll(DataSet: TDataSet);
    procedure acUserFieldsExecute(Sender: TObject);
    procedure tabAssortmentBeforeDelete(DataSet: TDataSet);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure acSecurityExecute(Sender: TObject);
    procedure tabAssortmentAfterRefresh(DataSet: TDataSet);
    procedure acSortAssortmentExecute(Sender: TObject);
    procedure acCancelFilterExecute(Sender: TObject);
    procedure qrySelectBeforeScroll(DataSet: TDataSet);
    procedure qrySelectAfterScroll(DataSet: TDataSet);
    procedure tabAssortmentAfterPost(DataSet: TDataSet);
    procedure dsAssortmentStateChange(Sender: TObject);
    procedure grSelectCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure tabAssortmentAfterDelete(DataSet: TDataSet);
    procedure UpdateEditStyleForm;
    procedure isSearchAfterSearch(Sender: TwwIncrementalSearch;
      MatchFound: Boolean);
    procedure grSelectDblClick(Sender: TObject);
    procedure tabAssortmentAfterInsert(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure tabAssortmentAfterOpen(DataSet: TDataSet);
  private
    fChanged: Boolean;
    mSFilter: rStyleFilterT;
    mSortOrder: eSortOrderT;
    mSortField: string;
    mFilterActive: boolean;
    mAssortmentUF: rUserFieldsT;
    mAskForSaving: boolean;
//    procedure SetRequiredFieldColor;
    procedure InitUserFields;
    procedure RunQuery;
    procedure UpdateRecordCountPanel;
    procedure ShowHideUserFields;
    procedure DisplayHint(Sender: TObject);
  protected
  public
    property Changed: Boolean read fChanged;
    constructor Create(aOwner: TComponent); override;
    procedure Init(aAssortmentID: integer);
  end; //TfrmEditAssortment

var
  frmEditAssortment: TfrmEditAssortment;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs,
  u_main, u_common_lib, u_lib, u_style_filter, u_edit_userfields,
  u_resourcestrings, u_print_assortment_form, math, u_edit_style;

{$R *.DFM}

resourcestring
//  cMsg1 = '(*)Ein Sortiment mit vorhandenen Artikeln kann nicht geloescht werden.'; //
  cMsg2 = '(*)Es muss mindestens ein Sortiment in der Datenbank vorhanden sein. Sortiment kann nicht geloescht werden.'; //ivlm
  cMsg3 = '(*)Das Sortiment %s wird jetzt geloescht. Weitermachen?'; //ivlm
  cMsg4 = '(*)Das Default-Sortiment %s kann nicht geloescht werden!'; //ivlm
  cMsg5 = '(*)Ein Sortiment mit vorhandenen Artikeln kann nicht deaktiviert werden.'; //ivlm

constructor TfrmEditAssortment.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Sortimentstamm.htm');
  // use ModalResult to detect if something has changed from outside
  InitUserFields;

  fChanged := False;
  mSortField := 'c_assortment_name';
  mSortOrder := soAscending;
  mAskForSaving := true;

  mSFilter := InitFilter(0);

  GetNavigatorWidth(mmDBNavigator1);

//  tabAssortment.Open;
//  RunQuery;
  (*
  { $ifdef ShowOnlyActive}
  acCancelFilter.Enabled := true;
  { $else}
  acCancelFilter.Enabled := false;
  { $endif}
  *)
//  SetRequiredFieldColor;
  laAssortmentname.Font.Color := cRequiredColor;
  laState.Font.Color          := cRequiredColor;
end; //constructor TfrmEditAssortment.Create
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.Init(aAssortmentID: integer);
begin
  if aAssortmentID > 0 then begin
    if not(qrySelect.Active) then
      RunQuery;
    qrySelect.Locate('c_assortment_id',aAssortmentID,[]);
  end;// if aAssortmentID > 0 then begin
end; //procedure TfrmEditAssortment.Init
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TfrmEditAssortment.acCloseExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acApplyFilterExecute(Sender: TObject);
begin
  if not PostRecord(tabAssortment) then exit;

  with TfrmStyleFilter.Create(Self) do
  try
    Init(2,mSFilter);
    if ShowModal = mrOK then begin
      mSFilter := GetFilterValues;
      RunQuery;
    end;
  finally
    Free;
  end; //with TfrmSearchByName.Create(Application) do begin
end; //procedure TfrmEditAssortment.acApplyFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.DisplayHint(Sender: TObject);
begin
  mmStatusBar1.SimpleText := GetLongHint(Application.Hint);
end; //procedure TfrmEditAssortment.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.FormActivate(Sender: TObject);
begin
  Application.OnHint := DisplayHint;
end; //procedure TfrmEditAssortment.FormActivate
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.FormDestroy(Sender: TObject);
begin
  Application.OnHint := frmMain.DisplayHint;
end; //procedure TfrmEditAssortment.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acPrintExecute(Sender: TObject);
var xBW : Boolean;
begin
  inherited;
  if not PostRecord(tabAssortment) then exit;
  if not GetPrintOptions(xBW, poPortrait, Self) then exit;

  with TfrmPrintAssortmentForm.Create(Self, tabAssortmentc_assortment_id.asinteger) do
    try
      Execute;
    finally
      Free;
    end; //try..finally
end; //procedure TfrmEditAssortment.acPrintExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TfrmEditAssortment.acHelpExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acCopyAssortmentExecute(Sender: TObject);
var
  i,xID      : integer;
  xFieldName : string;

begin
  if not PostRecord(tabAssortment) then exit;
  xID := tabAssortmentc_assortment_id.asinteger;

  tabAssortment.Insert;
  with qry1 do
  try
    CommandText := 'SELECT * FROM t_assortment WHERE c_assortment_id = :id';
    Parameters.ParamByName('id').value := xID;
    Open;

    for i := 0 to FieldCount -1 do begin
      xFieldName := lowercase(Fields[i].FieldName);
      
      if (xFieldName <> 'c_assortment_id') and
         (xFieldName <> 'c_assortment_name') and
         (not Fields[i].IsNull) then
           tabAssortment.FieldByName(xFieldname).Value := FieldByName(xFieldname).Value;
    end; //for i := 0 to FieldCount -1 do begin
  finally
    Close;
    edc_assortment_name.SetFocus;
    tabAssortmentCalcFields(Nil);
    tabAssortmentAfterScroll(Nil);
  end; //try..finally
end; //procedure TfrmEditAssortment.acCopyStyleExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentBeforePost(DataSet: TDataSet);
var
  xID : integer;

begin
  if tabAssortment.State = dsInsert then begin
    xID := MakeUniqueID(2,tabAssortment.CommandText,'c_assortment_id');
    if xID < 0 then SysUtils.Abort;
    tabAssortmentc_assortment_id.Value := xID;
  end; //if tabAssortment.State = dsInsert then begin

  if not DBFieldOk(tabAssortmentc_assortment_name,laAssortmentname.Caption,0,0,true) then SysUtils.Abort;
  tabAssortmentc_assortment_name.value := trim(tabAssortmentc_assortment_name.asstring);

  if (tabAssortmentc_assortment_state.asinteger = ord(ssInactive)) and
     tabAssortmentStyleData.Value then begin
    MessageBeep(0);
    MMMessageDlg(cMsg5, mtError, [mbOK], 0, Self);
    Abort;
  end; //if tabAssortmentStyleData.Value then begin
end; //procedure TfrmEditAssortment.tabAssortmentBeforePost
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentCalcFields(DataSet: TDataSet);
const
  cQry1 = 'WHERE c_assortment_id = %d';
begin
  imgHistorical.Picture.Bitmap := nil;
  tabAssortmentStyleData.AsBoolean := GetRecordCount('t_style',format(cQry1,[tabAssortmentc_assortment_id.asinteger])) > 0;
  if tabAssortmentStyleData.AsBoolean then begin
    ilBitmaps.GetBitmap(2,imgHistorical.Picture.Bitmap);
    // Wenn Sortiment zugeordnete Artikel hat, kann dieses nicht gelöscht werden
    mmDBNavigator1.VisibleButtons := mmDBNavigator1.VisibleButtons -[nbDelete];
  end else begin
    ilBitmaps.GetBitmap(3,imgHistorical.Picture.Bitmap);
    // Bei Freigabe durch Security Delete Funktion ermöglichen aber nur, wenn kein Artikel
    // mit diesem Sortiment verlinkt ist
    if acDeleteAssortment.Enabled and acEditAssortment.Enabled  then
      mmDBNavigator1.VisibleButtons := mmDBNavigator1.VisibleButtons +[nbDelete];
  end;
end; //procedure TfrmEditAssortment.tabAssortmentCalcFields
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentNewRecord(DataSet: TDataSet);
begin
  tabAssortmentc_assortment_state.Value := ord(ssActive);
end; //procedure TfrmEditAssortment.tabAssortmentNewRecord
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentAfterScroll(DataSet: TDataSet);
begin
  ShowHideUserFields;
end; //procedure TfrmEditAssortment.tabAssortmentAfterScroll
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acUserFieldsExecute(Sender: TObject);
var
  xOK : boolean;
  i   : integer;
begin
  if not PostRecord(tabAssortment) then exit;

  with TfrmEditUserFields.Create(Self) do
  try
    mmTranslator.DictionaryName := Self.mmTranslator.DictionaryName;
    Init(2);
    if ShowModal = mrOK then begin
      InitUserFields;
    end;
  finally
    Free;
  end; //with TfrmEditUserFields.Create(Application) do begin
end; //procedure TfrmEditAssortment.acUserFieldsExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentBeforeDelete(DataSet: TDataSet);
begin
  // Defaultsortiment kann nicht gelöscht werden
  if tabAssortmentc_assortment_id.asinteger = 1 then begin
    MessageBeep(0);
    MMMessageDlg(cMsg4, mtError, [mbOK], 0, Self);
    Abort;
  end; //if tabAssortmentc_assortment_id.asinteger = 1 then begin

  // Mindestens 1 Sortiment muss vorhanden sein
  if tabAssortment.RecordCount < 1 then begin
    MessageBeep(0);
    MMMessageDlg(cMsg2, mtError, [mbOK], 0, Self);
    Abort;
  end; //if tabAssortment.RecordCount < 1 then begin

  MessageBeep(0);
  if MMMessageDlg(format(cMsg3, [tabAssortmentc_assortment_name.asstring]), mtConfirmation, [mbYes,mbCancel], 0, Self) <> mrYes then
    Abort;
end; //procedure TfrmEditAssortment.tabAssortmentBeforeDelete
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle,WM_NEXTDLGCTL,0,0);
  end; //if Key = #13 then begin
end; //procedure TfrmEditAssortment.FormKeyPress
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl1.Configure;
end; //procedure TfrmEditAssortment.acSecurityExecute
//-----------------------------------------------------------------------------
{
procedure TfrmEditAssortment.SetRequiredFieldColor;
var
  xColor : TColor;
begin
  xColor := cRequiredColor;
  laAssortmentname.Font.Color := xColor;
  laState.Font.Color := xColor;
end; //procedure TfrmEditAssortment.SetRequiredFieldColor
{}
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.InitUserFields;
var
  i,k : integer;
begin
  mAssortmentUF := ReadUserFields(2);
  tabAssortment.DisableControls;
  try
    if mAssortmentUF.FieldCount > 0 then begin
      for i := 1 to 10 do begin
        with TmmDBEdit(FindComponent('edUserField' +inttostr(i))) do begin
          Visible := False;
          DataField := '';
        end;
      end; //for i := 1 to 10 do begin

      for i := 1 to 10 do begin
        if mAssortmentUF.fields[i].Active then begin
          k := mAssortmentUF.fields[i].Position;

          with TmmDBEdit(FindComponent('edUserField' +inttostr(k))) do begin
            AutoLabel.Control.Caption := mAssortmentUF.fields[i].FieldName;

            {$ifdef ShowUserGrayed}
            Visible := true;
            Color := clBtnFace;
            {$else}
            Color := clWindow;
            {$endif}

            DataField := mAssortmentUF.fields[i].InternalName;
            if mAssortmentUF.Fields[i].FieldType = 0 then Width := 80
                                                     else Width := 375;
          end;
        end; //if mAssortmentUF.fields[i].Active then begin
      end; //for i := 1 to 10 do begin
    end //if mAssortmentUF.FieldCount > 0 then begin
    else begin
      for i := 1 to 10 do
        TmmDBEdit(FindComponent('edUserField' +inttostr(i))).DataField := '';
    end; //else begin
  finally
    tabAssortment.EnableControls;
    ShowHideUserFields;
  end; //try..finally
end; //procedure TfrmEditAssortment.InitUserFields
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentAfterRefresh(DataSet: TDataSet);
begin
  tabAssortmentAfterScroll(Nil);
end; //procedure TfrmEditAssortment.tabAssortmentAfterRefresh
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acSortAssortmentExecute(Sender: TObject);
begin
  if not PostRecord(tabAssortment) then exit;
  if mSortOrder = soAscending then mSortOrder := soDescending
                              else mSortOrder := soAscending;

  case mSortOrder of
    soAscending: acSortAssortment.ImageIndex := 40;
    soDescending: acSortAssortment.ImageIndex := 41;
  end; //case mSortOrder of

  RunQuery;
end; //procedure TfrmEditAssortment.acSortStyleExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.acCancelFilterExecute(Sender: TObject);
begin
  if not PostRecord(tabAssortment) then exit;
  mSFilter := InitFilter(1);
  RunQuery;
end; //procedure TfrmEditAssortment.acCancelFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.RunQuery;
var
  xID : integer;
begin
  with qrySelect do begin
    if Active then xID := tabAssortmentc_assortment_id.AsInteger
              else xID := 0;

    Active := false;
    CommandText := GetAssortmentFilterQuery(1,mSFilter,mSortField,mSortOrder);
    // SQL.SaveToFile('c:\temp\xx.txt');
    Open;

    if xID > 0 then
      Locate('c_assortment_id',xID,[]);
  end; //with qrySelect do begin
  UpdateRecordCountPanel;
end; //procedure TfrmEditAssortment.RunQuery
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.qrySelectBeforeScroll(DataSet: TDataSet);
begin
  if tabAssortment.State in [dsEdit,dsInsert] then
    if tabAssortment.Modified then begin
      if DoSaveRecord(mAskForSaving, Self) then begin
        if not PostRecord(tabAssortment) then Abort;
      end //if DoSaveRecord(mAskForSaving) then begin
      else Abort;
    end //if tabAssortment.Modified then begin
    else tabAssortment.Cancel;
end; //procedure TfrmEditAssortment.qrySelectBeforeScroll
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.qrySelectAfterScroll(DataSet: TDataSet);
begin
  if tabAssortment.Active then
    tabAssortment.Locate('c_assortment_id',qrySelectc_assortment_id.AsInteger,[]);
end; //procedure TfrmEditAssortment.qrySelectAfterScroll
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.UpdateRecordCountPanel;
var
  xFilterCount,
  xTableCount  : integer;
begin
  xFilterCount := -1;
  xTableCount  := -1;
  if not(qrySelect.Active) then
    qrySelect.Active := true;
  xFilterCount := qrySelect.RecordCount;
  if not(tabAssortment.Active) then
    tabAssortment.Active := true;
  xTableCount := tabAssortment.RecordCount;
  sbRecordCount.SimpleText := format(cFilterRecs, [xFilterCount,xTableCount]);
//  sbRecordCount.SimpleText := format(LTrans(cFilterRecs),[xFilterCount,xTableCount]);
  acCancelFilter.Enabled := xFilterCount <> xTableCount;
  mFilterActive := acCancelFilter.Enabled;
end; //procedure TfrmEditAssortment.UpdateRecordCountPanel
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentAfterPost(DataSet: TDataSet);
begin
  RunQuery;
  fChanged := True;
//  UpdateEditStyleForm;
end; //procedure TfrmEditAssortment.tabAssortmentAfterPost
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.dsAssortmentStateChange(Sender: TObject);
var
  xViewMode : boolean;
begin
  xViewMode := not(tabAssortment.State in [dsEdit,dsInsert]);

  acApplyFilter.Enabled    := xViewMode;
  acSortAssortment.Enabled := xViewMode;
  acCancelFilter.Enabled   := xViewMode and mFilterActive;
  isSearch.Enabled         := xViewMode;

  edc_assortment_name.ReadOnly := xViewMode;
  comAssortmentState.ReadOnly  := xViewMode;
  edUserField1.ReadOnly        := xViewMode;
  edUserField2.ReadOnly        := xViewMode;
  edUserField3.ReadOnly        := xViewMode;
  edUserField4.ReadOnly        := xViewMode;
  edUserField5.ReadOnly        := xViewMode;
  edUserField6.ReadOnly        := xViewMode;
  edUserField7.ReadOnly        := xViewMode;
  edUserField8.ReadOnly        := xViewMode;
  edUserField9.ReadOnly        := xViewMode;
  edUserField10.ReadOnly       := xViewMode;

  ShowHideUserFields;
end; //procedure TfrmEditAssortment.dsAssortmentStateChange
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.ShowHideUserFields;
var
  i,
  xLastPos   : integer;
  xEditMode  : boolean;
  xField     : TField;
begin
  tabAssortment.DisableControls;
  try
    if mAssortmentUF.FieldCount > 0 then begin
      xEditMode := tabAssortment.State in [dsEdit,dsInsert];
      xLastPos := -5;

      for i := 1 to 10 do begin
        with TmmDBEdit(FindComponent('edUserField' +inttostr(i))) do begin
          if DataField <> '' then begin
            xField := tabAssortment.FieldByName(DataField);
            try
              Visible := xEditMode or ((not xField.IsNull) and (xField.AsString <> ''));
            except
              Visible := false;
            end;
          {$ifdef ShowUserGrayed}
            if Visible then Color := clWindow
                       else Color := clBtnFace;
          {$else}
            if Visible then begin
              inc(xLastPos, 25);
              Top := xLastPos;
            end; //if xVisible then begin
          {$endif}
          end; //if xDataField <> '' then begin
        end; // with
      end; //for i
    end; //if mAssortmentUF.FieldCount > 0 then begin
  finally
    tabAssortment.EnableControls;
  end; //try..finally
end; //procedure TfrmEditAssortment.ShowHideUserFields
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.grSelectCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if qrySelectc_assortment_state.AsInteger <> 1 then
    AFont.Color := clRed;
end; //procedure TfrmEditAssortment.grSelectCalcCellColors
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentAfterDelete(DataSet: TDataSet);
begin
  qrySelect.First;
  RunQuery;
  UpdateEditStyleForm;
end; //procedure TfrmEditAssortment.tabAssortmentAfterDelete
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.UpdateEditStyleForm;
var
  i : integer;
begin
  for i := 0 to Screen.FormCount -1 do
    if Screen.Forms[i] is TfrmEditStyle then
      ReOpenQuery(TfrmEditStyle(Screen.Forms[i]).qryAssortment);
end; //procedure TfrmEditAssortment.UpdateEditStyleForm
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.isSearchAfterSearch(
  Sender: TwwIncrementalSearch; MatchFound: Boolean);
begin
  qrySelectAfterScroll(qrySelect);
  // tabAssortment.Locate('c_assortment_id',qrySelectc_assortment_id.AsInteger,[]);
end; //procedure TfrmEditAssortment.isSearchAfterSearch
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.grSelectDblClick(Sender: TObject);
begin
  if acEditAssortment.Enabled then
    if not(tabAssortment.State in [dsEdit,dsInsert]) then tabAssortment.Edit;
end; //procedure TfrmEditAssortment.grSelectDblClick
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.tabAssortmentAfterInsert(DataSet: TDataSet);
begin
  edc_assortment_name.SetFocus;
end;
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//-----------------------------------------------------------------------------
procedure TfrmEditAssortment.FormShow(Sender: TObject);
begin
  acCopyAssortment.Enabled := acCopyAssortment.Enabled and acEditAssortment.Enabled;

  if not acEditAssortment.Enabled then
    mmDBNavigator1.VisibleButtons := mmDBNavigator1.VisibleButtons -[nbInsert,nbDelete,nbEdit,nbPost,nbCancel];

  tabAssortment.ReadOnly := not acEditAssortment.Enabled;
  tabAssortment.Open;
end;
//-----------------------------------------------------------------------------

procedure TfrmEditAssortment.tabAssortmentAfterOpen(DataSet: TDataSet);
begin
  RunQuery;
end;

end.

