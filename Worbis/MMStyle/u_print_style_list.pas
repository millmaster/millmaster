(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_print_style_list.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.04.01  0.00   PW  | file created
| 13.07.2001       Wss | ColDistance set to 10 for better view at printout
| 01.10.2003       Wss | Farbe nun auch auf Ausdruck
| 21.06.2004     | SDo | Layout Anpassung
| 25.02.2005     | SDo | Black & White Print
|=========================================================================================*)

{$i symbols.inc}

unit u_print_style_list;

interface

uses
  DB, u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, QRPrintGrid, Qrctrls, mmQRSysData, QuickRpt, mmQRLabel, Printers,
  mmQRBand, ExtCtrls, mmQuickRep, IvDictio, IvMulti, IvEMulti, mmTranslator,
  wwQRPrintDBGrid, mmQRImage;

type
  TfrmPrintStyleList = class(TmmForm)
    mmTranslator: TmmTranslator;
    QuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitle: TmmQRLabel;
    qrbHeader: TmmQRBand;
    qrbDetail: TmmQRBand;
    mPrintDBGrid: TwwQRPrintDBGrid;
    QRBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    qlCompanyName: TmmQRLabel;

    procedure FormCreate(Sender: TObject);
    procedure mPrintDBGridAddQRComponent(Sender: TObject;
      aColIndex: Integer; var aWidth: Integer;
      var aQRComponent: TQRPrintable);
    procedure mPrintDBGridAfterScroll(DataSet: TDataSet;
      aField: TField; aQRPrintable: TQRPrintable);
  private
    mPrintBW: Boolean;
  public
    constructor Create(aOwner: TComponent; aPrintBW: Boolean); reintroduce; virtual;
    procedure Execute;
  end; //TfrmPrintStyleList




var
  frmPrintStyleList: TfrmPrintStyleList;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main, u_lib, u_common_lib, mmLib, u_resourcestrings, u_view_stylelist, wwdbgrid,
  mmQRShape, SettingsReader;

{$R *.DFM}


constructor TfrmPrintStyleList.Create(aOwner: TComponent; aPrintBW: Boolean);
begin
  mPrintBW :=  aPrintBW;
  inherited create(aOwner);
end;
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleList.Execute;
begin
  {$ifdef PrintPreview}
  mPrintDBGrid.Preview;
  {$else}
  mPrintDBGrid.Print;
  {$endif}
end; //procedure TfrmPrintStyleList.Execute
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleList.FormCreate(Sender: TObject);
begin
  inherited;
  QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
  QuickRep1.Page.Orientation := Printer.Orientation;
 

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

end; //procedure TfrmPrintStyleList.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleList.mPrintDBGridAddQRComponent(
  Sender: TObject; aColIndex: Integer; var aWidth: Integer;
  var aQRComponent: TQRPrintable);
begin
  with mPrintDBGrid do begin
    if AnsiSameText(TwwDBGrid(DataGrid).Fields[aColIndex].FieldName, 'c_color') then begin
      aQRComponent := TmmQRShape.Create(Nil);
      dec(aWidth, 10);
    end;
  end;
end;
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleList.mPrintDBGridAfterScroll(DataSet: TDataSet;
  aField: TField; aQRPrintable: TQRPrintable);
begin
  if aQRPrintable is TQRShape then begin
    if not mPrintBW then
       TQRShape(aQRPrintable).Brush.Color := DataSet.FieldByName('c_color').AsInteger
    else
       TQRShape(aQRPrintable).Brush.Color := GetGryScale(DataSet.FieldByName('c_color').AsInteger);
  end;
end;
//-----------------------------------------------------------------------------
end. //u_print_style_list

