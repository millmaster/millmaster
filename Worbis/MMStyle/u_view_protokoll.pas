(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_view_protokoll.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.12.2000 0.00  PW  | Datei erstellt
|
|
|=========================================================================================*)

{$i symbols.inc}

unit u_view_protokoll;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, ActnList, mmActionList, mmButton, ComCtrls,
  mmRichEdit, ExtCtrls, mmPanel, IvMlDlgs, mmSaveDialog, IvDictio, IvMulti,
  IvEMulti, mmTranslator, mmMemo;

type
  TfrmViewProtokoll = class(TmmDialog)
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmButton2: TmmButton;
    mmButton3: TmmButton;
    mmButton1: TmmButton;
    mmActionList1: TmmActionList;
    acSaveAs: TAction;
    acOK: TAction;
    acHelp: TAction;
    mmTranslator1: TmmTranslator;
    mmSaveDialog1: TmmSaveDialog;
    mmMemo1: TmmMemo;
    procedure acOKExecute(Sender: TObject);
    procedure acSaveAsExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
  public
    procedure Init(aLines: TStringList);
  end; //TfrmViewProtokoll

var
  frmViewProtokoll: TfrmViewProtokoll;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main, u_common_lib, u_lib, u_resourcestrings, mmCommonLib;

{$R *.DFM}

procedure TfrmViewProtokoll.acOKExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end; //procedure TfrmViewProtokoll.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmViewProtokoll.acSaveAsExecute(Sender: TObject);
begin
  inherited;
  with mmSaveDialog1 do begin
    if Execute then begin
      if not CreateDirs(FileName, Self) then
        exit;
      mmMemo1.Lines.SaveToFile(Filename);
    end; //if Execute then begin
  end; //if mmSaveDialog1.Execute then begin
end; //procedure TfrmViewProtokoll.acSaveAsExecute
//-----------------------------------------------------------------------------
procedure TfrmViewProtokoll.Init(aLines: TStringList);
begin
  mmMemo1.Lines.Assign(aLines);
end; //procedure TfrmViewProtokoll.Init
//-----------------------------------------------------------------------------
procedure TfrmViewProtokoll.acHelpExecute(Sender: TObject);
begin
  inherited;
end; //procedure TfrmViewProtokoll.acHelpExecute

end. //u_view_protokoll
