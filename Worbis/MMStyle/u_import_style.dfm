inherited frmImportStyle: TfrmImportStyle
  Left = 357
  Top = 111
  Width = 588
  Height = 529
  ActiveControl = edFilename
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = '(*)Artikeldaten importieren'
  Constraints.MaxWidth = 588
  Constraints.MinHeight = 440
  Constraints.MinWidth = 588
  KeyPreview = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object mmStatusBar1: TmmStatusBar
    Left = 0
    Top = 480
    Width = 580
    Height = 22
    Panels = <>
    SimplePanel = True
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 580
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 3
      Top = 0
      Action = acCancel
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 34
      Top = 0
      Action = acOpenDefault
    end
    object ToolButton4: TToolButton
      Left = 57
      Top = 0
      Action = acSaveDefault
    end
    object ToolButton5: TToolButton
      Left = 80
      Top = 0
      Width = 8
      ImageIndex = 3
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 88
      Top = 0
      Action = acImport
    end
    object ToolButton7: TToolButton
      Left = 111
      Top = 0
      Width = 8
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 119
      Top = 0
      Action = acHelp
    end
    object ToolButton9: TToolButton
      Left = 142
      Top = 0
      Action = acSecurity
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 26
    Width = 580
    Height = 454
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 570
      Height = 100
      Caption = '(*)Import-Datei'
      TabOrder = 0
      CaptionSpace = True
      object mmLabel4: TmmLabel
        Left = 10
        Top = 23
        Width = 130
        Height = 13
        AutoSize = False
        Caption = '(30)Dateiname'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton5: TmmSpeedButton
        Left = 536
        Top = 20
        Width = 24
        Height = 22
        Action = acSelectFile
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel5: TmmLabel
        Left = 10
        Top = 48
        Width = 130
        Height = 13
        AutoSize = False
        Caption = '(30)Feld-Trennzeichen'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edFilename: TmmEdit
        Left = 150
        Top = 20
        Width = 382
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object comSeparator: TNWComboBox
        Left = 150
        Top = 45
        Width = 50
        Height = 21
        Style = csDropDownList
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
      end
      object cbColumnTitle: TmmCheckBox
        Left = 10
        Top = 73
        Width = 550
        Height = 17
        Caption = '(*)Erste Zeile enthaelt Feldnamen'
        TabOrder = 2
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object gbFields: TmmGroupBox
      Left = 5
      Top = 110
      Width = 570
      Height = 339
      Anchors = [akLeft, akTop, akBottom]
      Caption = '(*)Import Feldzuweisungen'
      TabOrder = 1
      CaptionSpace = True
      object mmDBGrid1: TmmDBGrid
        Left = 10
        Top = 20
        Width = 550
        Height = 309
        Anchors = [akLeft, akTop, akBottom]
        DataSource = dsFields
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgCancelOnExit]
        ParentFont = False
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'id'
            Visible = False
          end
          item
            Expanded = False
            FieldName = 'fieldname'
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '(*)Feldname'
            Title.Color = clSilver
            Width = 202
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'internalname'
            Visible = False
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'required'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '(*)Mussfeld'
            Width = 75
            Visible = True
          end
          item
            Alignment = taCenter
            Expanded = False
            FieldName = 'keyfield'
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ReadOnly = True
            Title.Alignment = taCenter
            Title.Caption = '(*)Schluesselfeld'
            Width = 85
            Visible = True
          end
          item
            Alignment = taCenter
            Color = 16644338
            Expanded = False
            FieldName = 'position'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Title.Alignment = taCenter
            Width = 90
            Visible = True
          end>
      end
    end
  end
  object mmActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 368
    Top = 56
    object acImport: TAction
      Hint = '(*)Artikeldaten importieren'
      ImageIndex = 38
      OnExecute = acImportExecute
    end
    object acCancel: TAction
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 24
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
    object acSaveDefault: TAction
      Hint = '(*)Einstellungen als Standard festlegen'
      ImageIndex = 3
      OnExecute = acSaveDefaultExecute
    end
    object acOpenDefault: TAction
      Hint = '(*)Standard Export-Einstellungen oeffnen'
      ImageIndex = 1
      OnExecute = acOpenDefaultExecute
    end
    object acSelectFile: TAction
      Hint = '(*)Datei auswaehlen'
      OnExecute = acSelectFileExecute
    end
    object acSecurity: TAction
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 28
      OnExecute = acSecurityExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 400
    Top = 56
    TargetsData = (
      1
      6
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Title'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'SimpleText'
        0))
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Artikeldaten importieren'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 437
    Top = 53
  end
  object dsFields: TmmDataSource
    DataSet = tabFields
    Left = 365
    Top = 238
  end
  object tabFields: TmmADODataSet
    Connection = dmStyle.conDefault
    BeforePost = tabFieldsBeforePost
    CommandText = 'c:\temp\@mm_impdef1.db'
    CommandType = cmdTable
    FieldDefs = <
      item
        Name = 'Fieldname'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Id'
        DataType = ftInteger
      end
      item
        Name = 'Internalname'
        DataType = ftString
        Size = 50
      end
      item
        Name = 'Required'
        DataType = ftBoolean
      end
      item
        Name = 'Keyfield'
        DataType = ftBoolean
      end
      item
        Name = 'Position'
        DataType = ftInteger
      end>
    Parameters = <>
    StoreDefs = True
    Left = 333
    Top = 238
    object tabFieldsid: TIntegerField
      FieldName = 'id'
      Visible = False
    end
    object tabFieldsfieldname: TStringField
      DisplayWidth = 20
      FieldName = 'fieldname'
      Size = 50
    end
    object tabFieldsinternalname: TStringField
      FieldName = 'internalname'
      Visible = False
      Size = 50
    end
    object tabFieldsrequired: TBooleanField
      FieldName = 'required'
      DisplayValues = 'X; '
    end
    object tabFieldskeyfield: TBooleanField
      FieldName = 'keyfield'
      DisplayValues = 'X; '
    end
    object tabFieldsposition: TIntegerField
      DisplayLabel = '(*)Import-Position'
      FieldName = 'position'
    end
  end
  object qry1: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 
      'SELECT COUNT(*) AS '#39'count'#39' FROM t_assortment WHERE UPPER(c_assor' +
      'tment_name) = UPPER(:c_assortment_name) '
    Parameters = <
      item
        Name = 'c_assortment_name'
        Size = -1
        Value = Null
      end>
    Left = 445
    Top = 238
  end
  object tabConnection: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 'dbo.t_assortment'
    CommandType = cmdTable
    Parameters = <>
    Left = 525
    Top = 117
  end
  object mmOpenDialog1: TmmOpenDialog
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 477
    Top = 53
  end
  object qry2: TmmADOCommand
    CommandText = 
      'INSERT INTO t_style_settings(c_style_id,c_ym_set_id) VALUES(:c_s' +
      'tyle_id,1)'
    Connection = dmStyle.conDefault
    Parameters = <
      item
        Name = 'c_style_id'
        Size = -1
        Value = Null
      end>
    Left = 477
    Top = 238
  end
end
