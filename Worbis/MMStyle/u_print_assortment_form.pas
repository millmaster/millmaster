(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_print_assortment_form.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: 
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.12.2000 | 0.00 | PW  | Datei erstellt
| 10.10.2002 | 0.00 | LOK | Umbau ADO
| 21.06.2004 | 1.00 | SDo | Layout Anpassung
|=========================================================================================*)

{$i symbols.inc}

unit u_print_assortment_form;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db,
  Qrctrls, QuickRpt, ExtCtrls, IvDictio, IvMulti, IvEMulti,
  Printers, mmTranslator, mmQRDBText, mmQRShape, mmQRSysData, mmQRLabel,
  mmQRBand, mmQuickRep, BaseForm, u_dmStyle, ADODB, mmADODataSet, mmQRImage;

type
  TfrmPrintAssortmentForm = class(TmmForm)
    QuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitel: TmmQRLabel;
    DetailBand1: TmmQRBand;
    mmTranslator: TmmTranslator;
    mmQRShape1: TmmQRShape;
    mmQRLabel1: TmmQRLabel;
    mmQRDBText1: TmmQRDBText;
    mmQRShape2: TmmQRShape;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel11: TmmQRLabel;
    mmQRDBText6: TmmQRDBText;
    mmQRDBText7: TmmQRDBText;
    shUserFields: TmmQRShape;
    laUserFields: TmmQRLabel;
    laUserField1: TmmQRLabel;
    laUserField2: TmmQRLabel;
    laUserField3: TmmQRLabel;
    laUserField4: TmmQRLabel;
    laUserField5: TmmQRLabel;
    laUserField6: TmmQRLabel;
    laUserField7: TmmQRLabel;
    laUserField8: TmmQRLabel;
    laUserField9: TmmQRLabel;
    laUserField10: TmmQRLabel;
    edUserField1: TmmQRDBText;
    edUserField2: TmmQRDBText;
    edUserField3: TmmQRDBText;
    edUserField4: TmmQRDBText;
    edUserField5: TmmQRDBText;
    edUserField6: TmmQRDBText;
    edUserField7: TmmQRDBText;
    edUserField8: TmmQRDBText;
    edUserField9: TmmQRDBText;
    edUserField10: TmmQRDBText;
    qryAssortment: TmmADODataSet;
    qryAssortmentState: TStringField;
    qryAssortmentStyleData: TBooleanField;
    qryAssortmentc_assortment_id: TSmallintField;
    qryAssortmentc_assortment_name: TStringField;
    qryAssortmentc_assortment_state: TSmallintField;
    qryAssortmentc_num1: TFloatField;
    qryAssortmentc_num2: TFloatField;
    qryAssortmentc_num3: TFloatField;
    qryAssortmentc_string1: TStringField;
    qryAssortmentc_string2: TStringField;
    qryAssortmentc_string3: TStringField;
    qryAssortmentc_string4: TStringField;
    qryAssortmentc_string5: TStringField;
    qryAssortmentc_string6: TStringField;
    qryAssortmentc_string7: TStringField;
    qlCompanyName: TmmQRLabel;
    QRBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;

    procedure qryAssortmentCalcFields(DataSet: TDataSet);
  private
    mAssortmentUF: rUserFieldsT;
    procedure InitUserFields;
  public
    constructor Create(aOwner: TComponent; aAssortmentID: integer); virtual;
    procedure Execute;
  end; //TfrmPrintAssortmentForm

var
  frmPrintAssortmentForm: TfrmPrintAssortmentForm;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main, u_lib, u_common_lib, mmLib, u_resourcestrings, SettingsReader;

{$R *.DFM}

constructor TfrmPrintAssortmentForm.Create(aOwner: TComponent; aAssortmentID: integer);
begin
  inherited create(aOwner);

  mAssortmentUF := ReadUserFields(2);
  InitUserFields;
  QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;

  qryAssortment.Parameters.ParamByName('c_assortment_id').value := aAssortmentID;
  qryAssortment.Open;


  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

end; //procedure TfrmPrintAssortmentForm.Init
//-----------------------------------------------------------------------------
procedure TfrmPrintAssortmentForm.Execute;
begin
  {$ifdef PrintPreview}
  QuickRep1.Preview;
  {$else}
  QuickRep1.Print;
  {$endif}
end; //procedure TfrmPrintAssortmentForm.Execute
//-----------------------------------------------------------------------------
procedure TfrmPrintAssortmentForm.qryAssortmentCalcFields(DataSet: TDataSet);
const
  cQry1 = 'WHERE c_assortment_id = %d';

begin
  inherited;
  qryAssortmentState.value := GetStateStr(qryAssortmentc_assortment_state.AsInteger);
  qryAssortmentStyleData.AsBoolean := GetRecordCount('t_style',format(cQry1,[qryAssortmentc_assortment_id.asinteger])) > 0;
end; //procedure TfrmPrintAssortmentForm.qryStyleCalcFields
//-----------------------------------------------------------------------------
procedure TfrmPrintAssortmentForm.InitUserFields;
var
  i,k : integer;

begin
  if mAssortmentUF.FieldCount > 0 then begin
    laUserFields.Enabled := true;
    shUserFields.Enabled := true;
    shUserFields.Height := (mAssortmentUF.FieldCount *25) +10;

    for i := 1 to 10 do begin
      if mAssortmentUF.fields[i].Active then begin
        k := mAssortmentUF.fields[i].Position;
        TmmQRDBText(FindComponent('edUserField' +inttostr(k))).Enabled := true;
        TmmQRDBText(FindComponent('edUserField' +inttostr(k))).DataField := mAssortmentUF.fields[i].InternalName;
        TmmQRLabel(FindComponent('laUserField' +inttostr(k))).Enabled := true;
        TmmQRLabel(FindComponent('laUserField' +inttostr(k))).Caption := mAssortmentUF.fields[i].FieldName;
      end; //if mAssortmentUF.fields[i].Active then begin
    end; //for i := 1 to 10 do begin
  end; //if mAssortmentUF.FieldCount > 0 then begin
end; //procedure TfrmPrintAssortmentForm.InitUserFields

end. //u_print_assortment_form
