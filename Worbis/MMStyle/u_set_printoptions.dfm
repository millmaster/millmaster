inherited SetPrintOptionsDlg: TSetPrintOptionsDlg
  Left = 459
  Top = 302
  ActiveControl = bOK
  BorderStyle = bsDialog
  Caption = '(*)Druckoptionen fuer Ausdruck'
  ClientHeight = 133
  ClientWidth = 360
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 360
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gbCurrentPrinter: TmmGroupBox
      Left = 5
      Top = 5
      Width = 350
      Height = 90
      Anchors = [akLeft, akTop, akRight]
      Caption = '(60)Aktueller Drucker'
      TabOrder = 0
      CaptionSpace = True
      object laPrintername: TmmLabel
        Left = 10
        Top = 20
        Width = 82
        Height = 13
        Caption = '(20)Druckername'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laOrientation: TmmLabel
        Left = 10
        Top = 40
        Width = 77
        Height = 13
        Caption = '(20)Seitenformat'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laCopies: TmmLabel
        Left = 10
        Top = 60
        Width = 82
        Height = 13
        Caption = '(20)Kopienanzahl'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laPrinternameValue: TmmLabel
        Left = 100
        Top = 20
        Width = 98
        Height = 13
        Caption = '(20)Druckername'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laOrientationValue: TmmLabel
        Left = 100
        Top = 40
        Width = 94
        Height = 13
        Caption = '(20)Seitenformat'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laCopiesValue: TmmLabel
        Left = 100
        Top = 60
        Width = 99
        Height = 13
        Caption = '(20)Kopienanzahl'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bPrinter: TmmButton
        Left = 265
        Top = 15
        Width = 75
        Height = 23
        Action = acPrinter
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 100
    Width = 360
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 120
      Top = 5
      Width = 75
      Height = 23
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 200
      Top = 5
      Width = 75
      Height = 23
      Action = acCancel
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 280
      Top = 5
      Width = 75
      Height = 23
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object PrinterSetupDialog1: TmmPrinterSetupDialog
    Left = 281
    Top = 51
  end
  object ActionList1: TmmActionList
    Left = 316
    Top = 52
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acPrinter: TAction
      Caption = '(12)&Drucker'
      Hint = '(*)Druckereinstellungen aufrufen...'
      OnExecute = acPrinterExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 249
    Top = 52
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
