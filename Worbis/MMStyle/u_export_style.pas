(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_export_style.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
|  8.12.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 | 0.00 | PW  | column "c_opt_speed" removed
| 12.04.01 | 0.00 | PW  | - move columns with double click
|                         - export column title
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 15.05.2001       Wss | Slip added to export list/array in u_lib and query
| 22.06.01         pw  | c_ym_set_id removed
| 21.11.2001       Wss | YarnCount, Meter/Weight calculation handling adapted
| 28.02.2002       Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
| 10.10.2002       LOK | Umbau ADO
| 18.01.2005  2.00 SDo | Umbau & Anpassungen an XML Vers. 5
|=========================================================================================*)

{$I symbols.inc}

unit u_export_style;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, IvDictio, IvMulti, IvEMulti, mmTranslator, ExtCtrls, mmPanel,
  ActnList, mmActionList, StdCtrls, mmLabel, Buttons, mmSpeedButton, FileCtrl,
  Nwcombo, mmGroupBox, mmButton, mmEdit, mmComboBox, IvMlDlgs, mmSaveDialog,
  Db, mmBitBtn, ComCtrls, ToolWin, mmToolBar,
  mmStatusBar, MMSecurity, mmCheckBox, mmDataSource, Grids, DBGrids,
  mmDBGrid, ADODB, mmADODataSet, u_dmStyle;

type
  TfrmExportStyle = class(TmmForm)
    mmTranslator1: TmmTranslator;
    mmActionList1: TmmActionList;
    acExport: TAction;
    acCancel: TAction;
    acHelp: TAction;
    acInsert: TAction;
    acDelete: TAction;
    acMoveUp: TAction;
    acMoveDown: TAction;
    acSaveDefault: TAction;
    acOpenDefault: TAction;
    mmPanel1: TmmPanel;
    gbFields: TmmGroupBox;
    lbAvailable: TNWListBox;
    lbExportFields: TNWListBox;
    bInsert: TmmSpeedButton;
    bDelete: TmmSpeedButton;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmGroupBox1: TmmGroupBox;
    mmLabel3: TmmLabel;
    comDecimal: TNWComboBox;
    edFilename: TmmEdit;
    mmLabel4: TmmLabel;
    mmSpeedButton5: TmmSpeedButton;
    acSelectFile: TAction;
    mmLabel5: TmmLabel;
    comSeparator: TNWComboBox;
    mmSaveDialog1: TmmSaveDialog;
    acSecurity: TAction;
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    mmStatusBar1: TmmStatusBar;
    MMSecurityControl1: TMMSecurityControl;
    qryStyle: TmmADODataSet;
    qryStylec_style_id: TSmallintField;
    qryStylec_assortment_id: TSmallintField;
    qryStylec_style_name: TStringField;
    qryStylec_assortment_name: TStringField;
    qryStylec_nr_of_threads: TSmallintField;
    qryStylec_twist_left: TBooleanField;
    qryStylem_per_bob: TIntegerField;
    qryStylem_per_cone: TIntegerField;
    qryStylec_color: TIntegerField;
    qryStylec_num1: TFloatField;
    qryStylec_num2: TFloatField;
    qryStylec_num3: TFloatField;
    qryStylec_string1: TStringField;
    qryStylec_string2: TStringField;
    qryStylec_string3: TStringField;
    qryStylec_string4: TStringField;
    qryStylec_string5: TStringField;
    qryStylec_string6: TStringField;
    qryStylec_string7: TStringField;
    qryStylec_package_type: TSmallintField;
    qryStylec_style_state: TSmallintField;
    qryStyletwist_direction: TStringField;
    qryStylecalc_CopsWeight: TFloatField;
    qryStylecalc_ConeWeight: TFloatField;
    qryStylepackage_type: TStringField;
    qryStylestyle_state: TStringField;
    qryStylec_mm_per_bob: TIntegerField;
    qryStylec_mm_per_cone: TIntegerField;
    cbColumnTitle: TmmCheckBox;
    qryStylec_Factor_INeps: TFloatField;
    qryStylec_Factor_IThick: TFloatField;
    qryStylec_Factor_IThin: TFloatField;
    qryStylec_Factor_ISmall: TFloatField;
    qryStylec_Factor_SFI: TFloatField;
    qryStylec_slip: TSmallintField;
    qryStylecalc_slip: TFloatField;
    qryStylec_yarncnt: TFloatField;
    qryStylec_twist: TFloatField;

    procedure acCancelExecute(Sender: TObject);
    procedure acInsertExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acMoveUpExecute(Sender: TObject);
    procedure acMoveDownExecute(Sender: TObject);
    procedure acSaveDefaultExecute(Sender: TObject);
    procedure acOpenDefaultExecute(Sender: TObject);
    procedure acSelectFileExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure ItemDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lbAvailableDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lbExportFieldsDragOver(Sender, Source: TObject; X,
      Y: Integer; State: TDragState; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure qryStyleCalcFields(DataSet: TDataSet);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure mmPanel1Resize(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acHelpExecute(Sender: TObject);
  private
    mExportFields: rExportFieldsT;
    procedure MoveSelectedItem(aOldPos, aDropPos: integer);
    procedure EnableActions;
    procedure Update_ExportFields;
    procedure InitEditControls;
    function CreateExportFile: boolean;
    procedure DisplayHint(Sender: TObject);
  public
  end; //TfrmExportStyle

var
  frmExportStyle: TfrmExportStyle;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  MMUGlobal, mmDialogs,
  u_main, u_common_lib, u_lib, u_resourcestrings, u_edit_style, XMLDef;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Datei %s existiert bereits. Soll die bestehende Datei ueberschrieben werden?'; //ivlm
  cMsg3 = '(*)Datei % konnte nicht erstellt werden.'; //ivlm
  cMsg4 = '(*)Fehler beim Schreiben in %s.'; //ivlm
  cMsg5 = '(*)Artikeldaten wurden erfolgreich exportiert.'; //ivlm

procedure TfrmExportStyle.acCancelExecute(Sender: TObject);
begin
//  ModalResult := mrCancel;
  Close;
end; //procedure TfrmExportStyle.acCancelExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acInsertExecute(Sender: TObject);
begin
  ItemDragDrop(lbExportFields, lbAvailable, 0, 0);
end; //procedure TfrmExportStyle.acInsertExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acDeleteExecute(Sender: TObject);
begin
  ItemDragDrop(lbAvailable, lbExportFields, 0, 0);
end; //procedure TfrmExportStyle.acDeleteExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acMoveUpExecute(Sender: TObject);
begin
  if lbExportFields.ItemIndex > 0 then MoveSelectedItem(lbExportFields.ItemIndex, lbExportFields.ItemIndex - 1);
end; //procedure TfrmExportStyle.acMoveUpExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acMoveDownExecute(Sender: TObject);
begin
  if lbExportFields.ItemIndex < (lbExportFields.Items.Count - 1) then MoveSelectedItem(lbExportFields.ItemIndex, lbExportFields.ItemIndex + 1);
end; //procedure TfrmExportStyle.acMoveDownExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.Update_ExportFields;
var
  i, k: integer;

begin
  mExportFields.Fields := nil;
  mExportFields.Filename := trim(edFilename.Text);
  mExportFields.DecimalSeparator := comDecimal.GetItemID;
  mExportFields.FieldSeparator := comSeparator.GetItemID;
  mExportFields.WithFieldNames := cbColumnTitle.Checked;

  SetLength(mExportFields.Fields, lbExportFields.Items.Count);
  for i := 0 to lbExportFields.Items.Count - 1 do begin
    k := lbExportFields.GetItemIDAtPos(i);
    mExportFields.Fields[i] := GetExportFieldByID(k);
  end; //for i := 0 to lbExportFields.Items.Count -1 do begin
end; //procedure TfrmExportStyle.acSaveDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acSaveDefaultExecute(Sender: TObject);
begin
  Update_ExportFields;
  WriteExportFields(mExportFields);
end; //procedure TfrmExportStyle.acSaveDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acOpenDefaultExecute(Sender: TObject);
begin
  InitEditControls;
end; //procedure TfrmExportStyle.acOpenDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acSelectFileExecute(Sender: TObject);
begin
  with mmSaveDialog1 do begin
    FileName := edFilename.Text;
    if Execute then edFilename.Text := FileName;
  end; //with mmSaveDialog1 do begin
end; //procedure TfrmExportStyle.acSelectFileExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acExportExecute(Sender: TObject);
begin
  if not CreateExportFile then exit;
  ModalResult := mrOK;
end; //procedure TfrmExportStyle.acExportExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.ItemDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  xDropPos: integer;
  i, k: integer;
  b: boolean;
  s: string;

begin
  //end of dragging
  if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then begin
    for i := 0 to TNWListBox(Source).Items.Count - 1 do begin
      b := TNWListBox(Source).Selected[i];
      s := TNWListBox(Source).Items[i];
      k := TNWListBox(Source).GetItemIDAtPos(i);

      if b then TNWListBox(Sender).AddObj(s, k);
    end; //for i := 0 to TNWListBox(Source).Items.Count -1 do

    while TNWListBox(Source).SelCount > 0 do
      for i := TNWListBox(Source).Items.Count - 1 downto 0 do begin
        b := TNWListBox(Source).Selected[i];
        if b then TNWListBox(Source).Items.Delete(i);
        if TNWListBox(Source).SelCount = 0 then break;
      end; //for i := TNWListBox(Source).Items.Count -1 downto 0 do

    for i := 0 to TNWListBox(Source).Items.Count - 1 do
      TNWListBox(Source).Selected[i] := False;

    for i := 0 to TNWListBox(Sender).Items.Count - 1 do
      TNWListBox(Sender).Selected[i] := False;

    EnableActions;
  end //if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then
  else begin
    if (Sender = Source) and ((Sender as TNWListBox) = lbExportFields) then begin
      xDropPos := lbExportFields.ItemAtPos(Point(X, Y), False);

      for i := 0 to lbExportFields.Items.Count - 1 do
        if lbExportFields.Selected[i] then Break;

      MoveSelectedItem(i, xDropPos);
    end; //if (Sender = Source) and ((Sender as TNWListBox) = lbExportFields) then begin
  end; //else begin
end; //procedure TfrmExportStyle.ItemDragDrop
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.lbAvailableDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source <> Sender;
end; //procedure TfrmExportStyle.lbAvailableDragOver
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.lbExportFieldsDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  xDropPos: integer;
begin
  if Source = lbExportFields then begin
    xDropPos := lbExportFields.ItemAtPos(Point(X, Y), False);
    Accept := (xDropPos > -1) and
      (xDropPos <> lbExportFields.ItemIndex) and
      (xDropPos < lbExportFields.Items.Count) and
      (not lbExportFields.Selected[xDropPos]);
  end //if Source = lbExportFields then begin
  else
    Accept := Source = lbAvailable;
end; //procedure TfrmExportStyle.lbExportFieldsDragOver
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.MoveSelectedItem(aOldPos, aDropPos: integer);
var
  i: integer;

begin
  lbExportFields.Items.Move(aOldPos, aDropPos);
  for i := 0 to lbExportFields.Items.Count - 1 do
    lbExportFields.Selected[i] := False;
  lbExportFields.Selected[aDropPos] := True;
end; //procedure TfrmExportStyle.MoveSelectedItem
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.EnableActions;
  //............................................................
  procedure SetSelectedItem(aListBox: TNWListBox);
  var
    i: integer;

  begin
    if aListBox.Items.Count > 0 then begin
      i := aListBox.ItemIndex;
      if i > -1 then
        aListBox.Selected[i] := True
      else
        aListBox.Selected[0] := True;
    end; //if aListBox.Items.Count > 0 then
  end; //procedure SetSelectedItem
  //............................................................
begin
  acInsert.Enabled := lbAvailable.Items.Count > 0;
  acDelete.Enabled := lbExportFields.Items.Count > 0;
  acMoveUp.Enabled := lbExportFields.Items.Count > 1;
  acMoveDown.Enabled := acMoveUp.Enabled;
  acExport.Enabled := acDelete.Enabled;
  acSaveDefault.Enabled := acDelete.Enabled;
  SetSelectedItem(lbExportFields);
  SetSelectedItem(lbAvailable);
end; //procedure TfrmExportStyle.EnableActions
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Artikeldaten_exportieren.htm');
  Application.OnHint := DisplayHint;
  gUserFields := ReadUserFields(1);
  InitEditControls;
end; //procedure TfrmExportStyle.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.InitEditControls;
var
  i: integer;
  xExportField: rExportFieldT;
  xUserField: rUserFieldT;
  xShowField: boolean;
begin
  //read default data
  mExportFields := ReadExportFields;

  //init combos
  comDecimal.Clear;
  comDecimal.AddObj('.', 0);
  comDecimal.AddObj(',', 1);
  comDecimal.ItemIndex := mExportFields.DecimalSeparator;

  comSeparator.Clear;
  comSeparator.AddObj(';', 0);
  comSeparator.AddObj('|', 1);
  comSeparator.AddObj('#', 2);
  comSeparator.ItemIndex := mExportFields.FieldSeparator;

  edFilename.Text := mExportFields.Filename;

  cbColumnTitle.Checked := mExportFields.WithFieldNames;

  //init listboxes
  lbAvailable.Clear;
  lbExportFields.Clear;

  for i := 0 to high(mExportFields.Fields) do
    lbExportFields.AddObj(LTrans(mExportFields.Fields[i].FieldName), mExportFields.Fields[i].ID);

  for i := 1 to cMaxExportFields do begin
    xExportField := GetExportFieldByID(i);
    //hide "empty" user fields
    if xExportField.UserField then begin
      xUserField := GetUserFieldByName(xExportField.InternalName);
      xShowField := xUserField.Active;
    end //if xExportField.UserField then begin
    else
      xShowField := True;

    if xShowField then begin
      if lbExportFields.GetItemIDPos(i) < 0 then
        lbAvailable.AddObj(LTrans(xExportField.FieldName), xExportField.ID);
    end; //if xShowField then begin
  end; //for i := 1 to cMaxExportFields do begin

  EnableActions;
end; //procedure TfrmExportStyle.InitEditControls
//-----------------------------------------------------------------------------
function TfrmExportStyle.CreateExportFile: boolean;
var
  xStr: string;
  xTemp: string;
  f: textfile;
  i: integer;
  xField: TField;
begin
  Result := False;
  Update_ExportFields;
  //overwrite
  if FileExists(mExportFields.Filename) then begin
    MessageBeep(0);
    if MMMessageDlg(Format(cMsg1, [mExportFields.Filename]), mtConfirmation, [mbYes, mbCancel], 0, Self) <> mrYes then
      exit;
  end; //if FileExists(mExportFields.Filename) then begin

  //create directory
  if not CreateDirs(mExportFields.Filename, Self) then
    exit;

  //create, open file
  try
    AssignFile(f, mExportFields.Filename);
    System.ReWrite(f);
  except
    MessageBeep(0);
    MMMessageDlg(Format(cMsg3, [mExportFields.Filename]), mtError, [mbOK], 0, Self);
    exit;
  end; //try..except

  //write to file
  try
    //export title
    if mExportFields.WithFieldNames then begin
      xStr := '';
      for i := 0 to high(mExportFields.Fields) do
        xStr := xStr + LTrans(mExportFields.Fields[i].FieldName) + comSeparator.GetItemText;
      system.writeln(f, xStr);
    end; //if mExportFields.WithFieldNames then begin

    //export data
    qryStyle.Open;
    try
      while not qryStyle.Eof do begin
        xStr := '';
        for i := 0 to high(mExportFields.Fields) do begin
          //handle calculated fields
          xField := qryStyle.fieldbyname(mExportFields.Fields[i].InternalName);

          if not xField.IsNull then
            case xField.DataType of
              ftString:
                xStr := xStr + xField.AsString;
              ftInteger, ftSmallInt:
                xStr := xStr + xField.AsString;
              ftFloat: begin
                  xTemp := FormatFloat(TFloatField(xField).DisplayFormat, xField.AsFloat);
                  xTemp := StringReplace(xTemp, DecimalSeparator, comDecimal.GetItemText, [rfReplaceAll]);
                  xStr := xStr + xTemp;
                end;
            end; //case xField.DataType of

          xStr := xStr + comSeparator.GetItemText;
        end; //for i := 0 to high(mExportFields.Fields) do begin

        system.writeln(f, xStr);

        qryStyle.Next;
      end; //while not qryStyle.Eof do begin
      Result := True;
      MMMessageDlg(cMsg5, mtInformation, [mbOK], 0, Self);
    except
      MessageBeep(0);
      MMMessageDlg(Format(cMsg4, [mExportFields.Filename]), mtError, [mbOK], 0, Self);
    end; //try..except
  finally
    CloseFile(f);
  end; //try..finally
end; //procedure TfrmExportStyle.CreateExportFile
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.qryStyleCalcFields(DataSet: TDataSet);
begin
  if qryStylec_twist.AsInteger > 0 then
    qryStyletwist_direction.value := cTwistDirAr[qryStylec_twist_left.asboolean];

  // qryStylestyle_state.value := GetStateStr(qryStylec_style_state.AsInteger);
  qryStylePackage_Type.value := GetPackageTypeStr(qryStylec_package_type.AsInteger);

  if qryStylec_mm_per_bob.IsNull then
    qryStylecalc_CopsWeight.clear
  else
    qryStylecalc_CopsWeight.value := MeterToWeightUnit(wug, yuNm, qryStylec_yarncnt.value, qryStylec_mm_per_bob.AsFloat / 1000);

  if qryStylec_mm_per_cone.IsNull then
    qryStylecalc_ConeWeight.clear
  else
    qryStylecalc_ConeWeight.value := MeterToWeightUnit(wug, yuNm, qryStylec_yarncnt.value, qryStylec_mm_per_cone.AsFloat / 1000);

  // Wss: new added field c_slip in table t_style (factor 1000 = int 1500 -> 1.500
  qryStylecalc_slip.Value := qryStylec_slip.Value / 1000.0;
  if (qryStylecalc_slip.Value < cMinSlip) or (qryStylecalc_slip.Value > cMaxSlip) then
    qryStylecalc_slip.AsString := '';

end; //procedure TfrmExportStyle.qryStyleCalcFields
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl1.Configure;
end; //procedure TfrmExportStyle.acSecurityExecute
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.DisplayHint(Sender: TObject);
begin
  mmStatusBar1.SimpleText := GetLongHint(Application.Hint);
end; //procedure TfrmExportStyle.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.FormDestroy(Sender: TObject);
begin
  mExportFields.Fields := nil;
  Set_DisplayHint;
end; //procedure TfrmExportStyle.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle, WM_NEXTDLGCTL, 0, 0);
  end; //if Key = #13 then begin
end; //procedure TfrmExportStyle.FormKeyPress
//-----------------------------------------------------------------------------
procedure TfrmExportStyle.mmPanel1Resize(Sender: TObject);
var
  xYCenter: integer;
begin
  xYCenter := gbFields.Height div 2;
  bInsert.Top := xYCenter - bInsert.Height - 2;
  bDelete.Top := xYCenter + 2;
end; //procedure TfrmExportStyle.mmPanel1Resize
//------------------------------------------------------------------------------
procedure TfrmExportStyle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
procedure TfrmExportStyle.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

end. //u_export_style

