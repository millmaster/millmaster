inherited frmViewProtokoll: TfrmViewProtokoll
  Left = 423
  Top = 187
  Width = 466
  Height = 449
  ActiveControl = mmMemo1
  BorderStyle = bsSizeable
  Caption = '(*)Import-Protokoll betrachten'
  Constraints.MinHeight = 150
  Constraints.MinWidth = 285
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 458
    Height = 389
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 0
    object mmMemo1: TmmMemo
      Left = 3
      Top = 3
      Width = 452
      Height = 383
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 389
    Width = 458
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object mmButton2: TmmButton
      Left = 290
      Top = 5
      Width = 80
      Height = 23
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton3: TmmButton
      Left = 375
      Top = 5
      Width = 80
      Height = 23
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe...'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton1: TmmButton
      Left = 3
      Top = 5
      Width = 100
      Height = 23
      Action = acSaveAs
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmActionList1: TmmActionList
    Left = 216
    Top = 32
    object acSaveAs: TAction
      Caption = '(10)Speichern unter...'
      Hint = '(*)Protokoll speichern unter'
      OnExecute = acSaveAsExecute
    end
    object acOK: TAction
      Caption = '(15)Schliessen'
      Hint = '(*)Fenster schliessen'
      OnExecute = acOKExecute
    end
    object acHelp: TAction
      Caption = '(15)Hilfe...'
      Hint = '(*)Hilfetext anzeigen'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 248
    Top = 32
    TargetsData = (
      1
      5
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0)
      (
        ''
        'Lines'
        0)
      (
        ''
        'Filter'
        0)
      (
        ''
        'Title'
        0))
  end
  object mmSaveDialog1: TmmSaveDialog
    FileName = 'Protokoll.txt'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Left = 288
    Top = 32
  end
end
