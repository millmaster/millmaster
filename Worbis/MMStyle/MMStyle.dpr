program MMStyle;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  MemCheck,
  Forms,
  LoepfeGlobal,
  u_main in 'u_main.pas' {frmMain},
  u_lib in 'u_lib.pas',
  u_global in 'u_global.pas',
  u_common_global in '..\common\u_common_global.pas',
  u_common_lib in '..\common\u_common_lib.pas',
  u_edit_style in 'u_edit_style.pas' {frmEditStyle},
  u_search_name in 'u_search_name.pas' {frmSearchByName},
  u_style_filter in 'u_style_filter.pas' {frmStyleFilter},
  u_resourcestrings in 'u_resourcestrings.pas',
  u_edit_userfields in 'u_edit_userfields.pas' {frmEditUserFields},
  u_edit_assortment in 'u_edit_assortment.pas' {frmEditAssortment},
  u_view_stylelist in 'u_view_stylelist.pas' {frmViewStyle},
  u_set_printoptions in 'u_set_printoptions.pas' {SetPrintOptionsDlg},
  u_export_style in 'u_export_style.pas' {frmExportStyle},
  u_import_style in 'u_import_style.pas' {frmImportStyle},
  u_print_assortment_form in 'u_print_assortment_form.pas' {frmPrintAssortmentForm},
  u_view_protokoll in 'u_view_protokoll.pas' {frmViewProtokoll},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  u_print_style_list in 'u_print_style_list.pas' {frmPrintStyleList},
  u_save_dialog in 'u_save_dialog.pas' {frmSaveDialog},
  u_progress in '..\common\u_progress.pas' {ProgressForm},
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  MMStyle_TLB in 'MMStyle_TLB.pas',
  MMStyleServerIMPL in 'MMStyleServerIMPL.pas' {MMStyleServer: CoClass},
  SelectionDialog in '..\..\..\LoepfeShares\Templates\SELECTIONDIALOG.pas' {frmSelectionDialog},
  u_select_ym_settings in 'u_select_ym_settings.pas' {frmSelectYMSettings},
  StyleChangerForm in 'StyleChangerForm.pas' {frmStyleChanger},
  u_dmStyle in 'u_dmStyle.pas' {dmStyle: TDataModule};

{$R *.TLB}

{$R *.RES}
{$R Version.res}

begin
  gApplicationName := 'MMStyle';
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}
  Application.Initialize;
  Application.Title := 'MMStyle';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TdmStyle, dmStyle);
  Application.Run;
end. //mmstyle
