inherited frmViewStyle: TfrmViewStyle
  Left = 468
  Top = 306
  Width = 703
  Height = 532
  Caption = '(*)Artikelliste betrachten'
  FormStyle = fsMDIChild
  Position = poDefault
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 26
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 34
      Top = 0
      Action = acSecurity
    end
    object ToolButton12: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 65
      Top = 0
      Action = acSearch
      ImageIndex = 25
    end
    object ToolButton6: TToolButton
      Left = 88
      Top = 0
      Action = acApplyFilter
    end
    object ToolButton13: TToolButton
      Left = 111
      Top = 0
      Action = acCancelFilter
    end
    object ToolButton15: TToolButton
      Left = 134
      Top = 0
      Width = 8
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton14: TToolButton
      Left = 142
      Top = 0
      Action = acEditStyle
    end
    object ToolButton16: TToolButton
      Left = 165
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 173
      Top = 0
      Action = acGridOptions
    end
    object ToolButton17: TToolButton
      Left = 196
      Top = 0
      Action = acOpenProfile
    end
    object ToolButton18: TToolButton
      Left = 219
      Top = 0
      Action = acSaveProfile
    end
    object ToolButton10: TToolButton
      Left = 242
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 250
      Top = 0
      Action = acPrint
    end
    object ToolButton9: TToolButton
      Left = 273
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object navStyle: TmmDBNavigator
      Left = 281
      Top = 0
      Width = 100
      Height = 22
      DataSource = dsStyle
      VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbRefresh]
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object ToolButton7: TToolButton
      Left = 381
      Top = 0
      Width = 8
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 389
      Top = 0
      Action = acHelp
    end
  end
  object wwDBGrid1: TwwDBGrid
    Left = 0
    Top = 26
    Width = 695
    Height = 460
    Selected.Strings = (
      'State'#9'10'#9'(20)Status'
      'c_style_name'#9'15'#9'(20)Artikel-~name'
      'yarncnt'#9'7'#9'(20)Garn-~feinheit'
      'twist'#9'8'#9'(20)Garn-~drehung'
      'ym_settings'#9'20'#9'(20)Reiniger-~einstellungen'
      'c_assortment_name'#9'20'#9'(20)Sortiment'
      'm_per_bob'#9'10'#9'(20)Laenge~auf Cops (m)'
      'm_per_cone'#9'10'#9'(20)Laenge~auf Cone (m)'
      'Package_Type'#9'25'#9'(20)Konentyp'
      'c_color'#9'10'#9'(20)Farbe'
      'c_num1'#9'10'#9'c_num1'
      'c_num2'#9'10'#9'c_num2'
      'c_num3'#9'10'#9'c_num3'
      'c_string1'#9'25'#9'c_string1'
      'c_string2'#9'25'#9'c_string2'
      'c_string3'#9'25'#9'c_string3'
      'c_string4'#9'25'#9'c_string4'
      'c_string5'#9'25'#9'c_string5'
      'c_string6'#9'25'#9'c_string6'
      'c_string7'#9'25'#9'c_string7'
      'calc_slip'#9'10'#9'(12)Schlupf')
    IniAttributes.Delimiter = ';;'
    TitleColor = clBtnFace
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = dsStyle
    KeyOptions = [dgEnterToTab]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint]
    PopupMenu = pmProfile
    TabOrder = 1
    TitleAlignment = taLeftJustify
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    TitleLines = 2
    TitleButtons = True
    OnCalcCellColors = wwDBGrid1CalcCellColors
    OnTitleButtonClick = wwDBGrid1TitleButtonClick
    OnDblClick = wwDBGrid1DblClick
    OnCalcTitleImage = wwDBGrid1CalcTitleImage
    TitleImageList = frmMain.ImageList16x16
  end
  object mmStatusBar1: TmmStatusBar
    Left = 0
    Top = 486
    Width = 695
    Height = 19
    Panels = <
      item
        Width = 200
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 208
    Top = 40
    TargetsData = (
      1
      5
      (
        '*'
        'Text'
        0)
      (
        '*'
        'SimpleText'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Artikelliste betrachten'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 176
    Top = 40
  end
  object mmActionList: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 112
    Top = 40
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acSearch: TAction
      Caption = '(*)Suche ueber Artikelname...'
      Hint = '(*)Artikel ueber Name suchen'
      ImageIndex = 10
      ShortCut = 16462
      OnExecute = acSearchExecute
    end
    object acApplyFilter: TAction
      Caption = '(*)&Filter setzen...'
      Hint = '(*)Filter fuer Artikeltabelle setzen'
      ImageIndex = 29
      ShortCut = 16454
      OnExecute = acApplyFilterExecute
    end
    object acPrint: TAction
      Caption = '(*)Artikelformular drucken...'
      Hint = '(*)Liste mit gewaehlten Artikeln ausdrucken'
      ImageIndex = 2
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen'
      ImageIndex = 4
      ShortCut = 112
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 28
      OnExecute = acSecurityExecute
    end
    object acCancelFilter: TAction
      Caption = '(*)Filter aufheben'
      Hint = '(*)Filter aufheben'
      ImageIndex = 39
      OnExecute = acCancelFilterExecute
    end
    object acEditStyle: TAction
      Hint = '(*)Aktuellen Artikel bearbeiten'
      ImageIndex = 33
      OnExecute = acEditStyleExecute
    end
    object acGridOptions: TAction
      Caption = '(*)Tabellen-Eigenschaften...'
      Hint = '(*)Tabellen-Eigenschaften bearbeiten'
      ImageIndex = 30
      ShortCut = 16468
      OnExecute = acGridOptionsExecute
    end
    object acOpenProfile: TAction
      Caption = '(*)Profil &laden...'
      Hint = '(*)Gespeichertes Profil laden'
      ImageIndex = 1
      OnExecute = acLoadProfileExecute
    end
    object acSaveProfile: TAction
      Caption = '(*)Profil &speichern...'
      Hint = '(*)Einstellungen als Profil speichern'
      ImageIndex = 3
      OnExecute = acSaveProfileExecute
    end
    object acDeleteProfile: TAction
      Caption = '(*)Profil l&oeschen...'
      Hint = '(*)Gespeichertes Profil loeschen'
      ImageIndex = 32
      OnExecute = acDeleteProfileExecute
    end
    object acSaveAsDefault: TAction
      Caption = '(*)Profil als Standard speichern'
      Hint = '(*)Einstellungen als Standard speichern'
      ImageIndex = 31
      OnExecute = acSaveAsDefaultExecute
    end
    object acRefresh: TAction
      Hint = '(*)Daten aktualisieren'
      ShortCut = 116
      OnExecute = acRefreshExecute
    end
  end
  object qryStyle: TmmADODataSet
    Connection = dmStyle.conDefault
    CursorType = ctStatic
    OnCalcFields = qryStyleCalcFields
    CommandText = 
      'SELECT'#13#10's.c_style_id,'#13#10's.c_style_name,'#13#10'/*       RTRIM(CONVERT(C' +
      'HAR(4),s.c_yarncnt /10)) +'#39'/'#39' + CONVERT(CHAR(1),s.c_nr_of_thread' +
      's) AS '#39'yarncnt'#39',*/'#13#10's.c_yarncnt,'#13#10's.c_nr_of_threads,'#13#10's.c_twist,' +
      #13#10's.c_twist_left,'#13#10's.c_style_state,'#13#10'a.c_assortment_name,'#13#10's.c_m' +
      'm_per_bob /1000 AS '#39'c_m_per_bob'#39','#13#10's.c_mm_per_cone /1000 AS '#39'c_m' +
      '_per_cone'#39','#13#10's.c_package_type,'#13#10's.c_color,'#13#10's.c_num1,'#13#10's.c_num2,' +
      #13#10's.c_num3,'#13#10's.c_string1,'#13#10's.c_string2,'#13#10's.c_string3,'#13#10's.c_strin' +
      'g4,'#13#10's.c_string5,'#13#10's.c_string6,'#13#10's.c_string7,'#13#10's.c_Factor_INeps,' +
      #13#10's.c_Factor_IThick,'#13#10's.c_Factor_IThin,'#13#10's.c_Factor_ISmall,'#13#10's.c' +
      '_Factor_SFI,'#13#10's.c_slip'#13#10'FROM t_style s'#13#10'JOIN t_assortment a ON a' +
      '.c_assortment_id = s.c_assortment_id'#13#10'WHERE s.c_style_id IS NOT ' +
      'NULL'#13#10
    Parameters = <>
    Left = 112
    Top = 72
    object qryStylec_style_id: TSmallintField
      Tag = -1
      FieldName = 'c_style_id'
      Visible = False
    end
    object qryStylec_style_name: TStringField
      DisplayLabel = '(20)Artikel-~name'
      DisplayWidth = 15
      FieldName = 'c_style_name'
    end
    object qryStylec_m_per_bob: TIntegerField
      DisplayLabel = '(20)Laenge~auf Cops (m)'
      DisplayWidth = 10
      FieldName = 'c_m_per_bob'
      Visible = False
      DisplayFormat = ',0'
    end
    object qryStylec_assortment_name: TStringField
      DisplayLabel = '(20)Sortiment'
      DisplayWidth = 20
      FieldName = 'c_assortment_name'
    end
    object qryStylec_m_per_cone: TIntegerField
      DisplayLabel = '(20)Laenge~auf Cone (m)'
      DisplayWidth = 10
      FieldName = 'c_m_per_cone'
      Visible = False
      DisplayFormat = ',0'
    end
    object qryStylec_color: TIntegerField
      DisplayLabel = '(20)Farbe'
      DisplayWidth = 10
      FieldName = 'c_color'
    end
    object qryStylec_num1: TFloatField
      DisplayWidth = 10
      FieldName = 'c_num1'
    end
    object qryStylec_num2: TFloatField
      DisplayWidth = 10
      FieldName = 'c_num2'
    end
    object qryStylec_num3: TFloatField
      DisplayWidth = 10
      FieldName = 'c_num3'
    end
    object qryStylec_string1: TStringField
      DisplayWidth = 25
      FieldName = 'c_string1'
      Size = 255
    end
    object qryStylec_string2: TStringField
      DisplayWidth = 25
      FieldName = 'c_string2'
      Size = 255
    end
    object qryStylec_string3: TStringField
      DisplayWidth = 25
      FieldName = 'c_string3'
      Size = 255
    end
    object qryStylec_string4: TStringField
      DisplayWidth = 25
      FieldName = 'c_string4'
      Size = 255
    end
    object qryStylec_string5: TStringField
      DisplayWidth = 25
      FieldName = 'c_string5'
      Size = 255
    end
    object qryStylec_string6: TStringField
      DisplayWidth = 25
      FieldName = 'c_string6'
      Size = 255
    end
    object qryStylec_string7: TStringField
      DisplayWidth = 25
      FieldName = 'c_string7'
      Size = 255
    end
    object qryStylecalc_slip: TFloatField
      DisplayLabel = '(12)Schlupf'
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'calc_slip'
      DisplayFormat = ',0.###'
      Calculated = True
    end
    object qryStylec_slip: TSmallintField
      Tag = -1
      DisplayWidth = 10
      FieldName = 'c_slip'
      Visible = False
    end
    object qryStylec_Factor_INeps: TFloatField
      DisplayLabel = '(20)Faktor~INeps'
      FieldName = 'c_Factor_INeps'
      Visible = False
      DisplayFormat = '0.000'
    end
    object qryStylec_Factor_IThick: TFloatField
      DisplayLabel = '(20)Faktor~IThick'
      FieldName = 'c_Factor_IThick'
      Visible = False
      DisplayFormat = '0.000'
    end
    object qryStylec_Factor_IThin: TFloatField
      DisplayLabel = '(20)Faktor~IThin'
      FieldName = 'c_Factor_IThin'
      Visible = False
      DisplayFormat = '0.000'
    end
    object qryStylec_Factor_ISmall: TFloatField
      DisplayLabel = '(20)Faktor~ISmall'
      FieldName = 'c_Factor_ISmall'
      Visible = False
      DisplayFormat = '0.000'
    end
    object qryStylec_Factor_SFI: TFloatField
      DisplayLabel = '(20)Faktor~SFI'
      FieldName = 'c_Factor_SFI'
      Visible = False
      DisplayFormat = '0.000'
    end
    object qryStylecalc_State: TStringField
      DisplayLabel = '(20)Status'
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'calc_State'
      Visible = False
      Size = 15
      Calculated = True
    end
    object qryStylec_style_state: TSmallintField
      Tag = -1
      DisplayWidth = 10
      FieldName = 'c_style_state'
      Visible = False
    end
    object qryStylecalc_Package_Type: TStringField
      DisplayLabel = '(20)Konentyp'
      DisplayWidth = 25
      FieldKind = fkCalculated
      FieldName = 'calc_Package_Type'
      Visible = False
      Size = 40
      Calculated = True
    end
    object qryStylec_package_type: TSmallintField
      Tag = -1
      DisplayWidth = 13
      FieldName = 'c_package_type'
      Visible = False
    end
    object qryStylecalc_twist: TStringField
      Alignment = taRightJustify
      DisplayLabel = '(20)Garn-~drehung'
      DisplayWidth = 8
      FieldKind = fkCalculated
      FieldName = 'calc_twist'
      Visible = False
      Size = 8
      Calculated = True
    end
    object qryStylec_twist: TFloatField
      Tag = -1
      FieldName = 'c_twist'
      Visible = False
    end
    object qryStylec_twist_left: TBooleanField
      Tag = -1
      DisplayWidth = 8
      FieldName = 'c_twist_left'
      Visible = False
    end
    object qryStylecalc_yarncnt: TStringField
      DisplayLabel = '(20)Garn-~feinheit'
      FieldKind = fkCalculated
      FieldName = 'calc_yarncnt'
      Visible = False
      Calculated = True
    end
    object qryStylec_yarncnt: TFloatField
      Tag = -1
      FieldName = 'c_yarncnt'
      Visible = False
    end
    object qryStylec_nr_of_threads: TSmallintField
      Tag = -1
      FieldName = 'c_nr_of_threads'
      Visible = False
    end
    object qryStylecalc_ym_settings: TStringField
      DisplayLabel = '(20)Reiniger-~einstellungen'
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'calc_ym_settings'
      Visible = False
      Size = 100
      Calculated = True
    end
  end
  object dsStyle: TwwDataSource
    DataSet = qryStyle
    Left = 144
    Top = 72
  end
  object mmGridPos1: TmmGridPos
    MemoINI.AdoConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=mmbde2;Initial Catalog=MM_Windin' +
      'g;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    AlterColors = True
    AlterColumnFormat = True
    AlterColumnPosition = True
    AlterGridOptions = True
    AlterFixedCols = True
    AlterFonts = True
    AlterVisibleCols = True
    Grid = wwDBGrid1
    LookupID = 'VIEW_STYLELIST'
    Left = 24
    Top = 104
  end
  object pmProfile: TmmPopupMenu
    Images = frmMain.ImageList16x16
    Left = 144
    Top = 40
    object miEditGridOptions: TMenuItem
      Action = acGridOptions
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object miLoadProfile: TMenuItem
      Action = acOpenProfile
    end
    object miSaveProfile: TMenuItem
      Action = acSaveProfile
    end
    object miDeleteProfile: TMenuItem
      Action = acDeleteProfile
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miSaveAsDefault: TMenuItem
      Action = acSaveAsDefault
    end
  end
  object qryYMSet: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 
      'SELECT y.c_ym_set_name  FROM t_style_settings ss JOIN t_xml_ym_s' +
      'ettings y ON y.c_ym_set_id = ss.c_ym_set_id WHERE ss.c_style_id ' +
      '= :c_style_id  ORDER BY y.c_ym_set_name'
    Parameters = <
      item
        Name = 'c_style_id'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end>
    Left = 112
    Top = 104
  end
end
