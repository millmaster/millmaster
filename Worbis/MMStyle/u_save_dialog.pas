(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_save_dialog.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Style Edit Form
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.04.01 | 0.00 | PW  | Datei erstellt
|          |      |     |
|          |      |     |
|=========================================================================================*)

{$i symbols.inc}

unit u_save_dialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, ActnList, mmActionList, StdCtrls, mmLabel, mmButton,
  mmCheckBox, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  TfrmSaveDialog = class(TmmDialog)
    cbAsk: TmmCheckBox;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    mmLabel1: TmmLabel;
    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    mmTranslator: TmmTranslator;

    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public

  end; //TfrmSaveDialog

var
  frmSaveDialog: TfrmSaveDialog;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TfrmSaveDialog.acOKExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end; //procedure TmmDialog1.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmSaveDialog.acCancelExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end; //procedure TmmDialog1.acCancelExecute
//-----------------------------------------------------------------------------
procedure TfrmSaveDialog.FormCreate(Sender: TObject);
begin
  inherited;
  MessageBeep(0);
end; //procedure TfrmSaveDialog.FormCreate

end. //u_save_dialog
