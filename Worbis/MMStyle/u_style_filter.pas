(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_style_form.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Define Style Filter
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.11.00 | 0.00 | PW  | Datei erstellt
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 21.03.2002       Wss | Hint in Label/Editbox fuer Artikelname korrigiert: " statt ' verwenden
| 10.10.2002       LOK | Umbau ADO
| 28.10.2002       Wss | - Diverse Bugfixes
                         - Style Filter: Reinigersettings mit Tastkopf ausgeben -> FillComboData()
| 27.06.2003       Wss | YarnUnit f�r YarnCount anzeigen
| 18.01.2005  1.01|SDo | Umbau & Anpassungen an XML Vers. 5
|=========================================================================================*)

{$i symbols.inc}

unit u_style_filter;

interface

uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, IvDictio, IvMulti, IvEMulti, mmTranslator, StdCtrls, mmEdit,
  mmLabel, ActnList, mmActionList, mmButton, NumCtrl, Nwcombo;

type
  TfrmStyleFilter = class(TmmDialog)
    mmTranslator1: TmmTranslator;
    laName: TmmLabel;
    edName: TmmEdit;
    laAssortment: TmmLabel;
    laYarncnt: TmmLabel;
    laThreads: TmmLabel;
    laTwist: TmmLabel;
    taDirection: TmmLabel;
    laState: TmmLabel;
    laYMSettings: TmmLabel;
    comAssortment: TNWComboBox;
    comState: TNWComboBox;
    comThreads: TNWComboBox;
    edYarnCount: TNumEdit;
    comTwistDirection: TNWComboBox;
    edTwist: TNumEdit;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    comYMSettings: TNWComboBox;
    laYarnUnit: TmmLabel;

    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure comYMSettingsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormCreate(Sender: TObject);
  private
    mVariante: byte;
    procedure FillCombos;
    procedure FillComboData(aCombo: TNWComboBox; aSQL: string);
  public
    procedure Init(aVariante: byte; aFilter: rStyleFilterT);
    function GetFilterValues: rStyleFilterT;
  end; //TfrmStyleFilter

var
  frmStyleFilter: TfrmStyleFilter;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, MMHtmlHelp, mmADODataSet, LoepfeGlobal, mmCS,
  u_lib, u_common_lib, u_resourcestrings, YMParaDef, SettingsReader, MMUGlobal,
  XMLDef;

resourcestring
  rsMsg1 = '(20)Artikelname'; //ivlm
  rsMsg2 = '(20)Sortimentname'; //ivlm
  rsMsg3 = '(*)<Alle>'; //ivlm

//-----------------------------------------------------------------------------
procedure TfrmStyleFilter.acOKExecute(Sender: TObject);
begin
  ModalResult := mrOK;
end; //procedure TfrmStyleFilter.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmStyleFilter.acCancelExecute(Sender: TObject);
begin
  ModalResult := mrCancel;
end; //procedure TfrmStyleFilter.acCancelExecute
//-----------------------------------------------------------------------------
function TfrmStyleFilter.GetFilterValues: rStyleFilterT;
begin
  result.Name  := edName.Text;
  result.State := comState.GetItemID;

  if mVariante = 1 then begin
    result.Assortment_ID  := comAssortment.GetItemID;
    result.YarnCount      := edYarnCount.Value;
    result.NoOfThreads    := comThreads.GetItemID;
    result.Twist          := Round(edTwist.Value);
    result.TwistDirection := comTwistDirection.GetItemID;
//    with comYMSettings do
//      result.YMSettings_ID  := Integer(Items.Objects[ItemIndex]);
    result.YMSettings_ID  := comYMSettings.GetItemID;
  end; //if mVariante = 1 then begin
end; //function TfrmStyleFilter.GetFilterValues
//-----------------------------------------------------------------------------
procedure TfrmStyleFilter.Init(aVariante: byte; aFilter: rStyleFilterT);
begin
  mVariante := aVariante;

  FillCombos;

  edName.Text := aFilter.Name;
  comState.SetItemID(aFilter.State);

  case mVariante of
    // Filterdialog f�r Artikel
    1: begin
        laName.Caption := rsMsg1;
        ClientHeight := 223;

        comAssortment.SetItemID(aFilter.Assortment_ID);
        edYarnCount.Value := aFilter.YarnCount;
        comThreads.SetItemID(aFilter.NoOfThreads);
        edTwist.Value := aFilter.Twist;
        comTwistDirection.SetItemID(aFilter.TwistDirection);
        comYMSettings.SetItemID(aFilter.YMSettings_ID);
      end;
    // Filterdialog f�r Sortiment
    2: begin
        laName.Caption := rsMsg2;
        ClientHeight := 108;
      end;
  end; //case mVariante of

  comAssortment.Visible     := mVariante = 1;
  edYarnCount.Visible       := comAssortment.Visible;
  comThreads.Visible        := comAssortment.Visible;
  edTwist.Visible           := comAssortment.Visible;
  comTwistDirection.Visible := comAssortment.Visible;
  comYMSettings.Visible     := comAssortment.Visible;
  laAssortment.Visible      := comAssortment.Visible;
  laYarncnt.Visible         := comAssortment.Visible;
  laYarnUnit.Visible        := comAssortment.Visible;
  laThreads.Visible         := comAssortment.Visible;
  laTwist.Visible           := comAssortment.Visible;
  taDirection.Visible       := comAssortment.Visible;
  laYMSettings.Visible      := comAssortment.Visible;
end; //procedure TfrmStyleFilter.Init
//-----------------------------------------------------------------------------
procedure TfrmStyleFilter.FillCombos;
const
  cQry1 = 'SELECT c_assortment_id AS id, c_assortment_name AS name FROM t_assortment '+
          'ORDER BY c_assortment_name';

  cQry2 = 'SELECT c_ym_set_id AS id, c_ym_set_name AS name, c_head_class FROM t_xml_ym_settings '+
          'WHERE c_template_set = 1 AND c_ym_set_name IS NOT NULL '+
          'AND RTRIM(c_ym_set_name) <> '''' '+
          'ORDER BY c_ym_set_name';
var
  i : integer;
begin
  comState.AddObj(rsMsg3,-1);
  comState.AddObj(cStyleState1,1);
  comState.AddObj(cStyleState2,2);

  if mVariante = 1 then begin
    GetComboData(comAssortment,cQry1);
    FillComboData(comYMSettings,cQry2);

    comThreads.AddObj(rsMsg3, -1);
    for i:=1 to 6 do
      comThreads.AddObj(inttostr(i),i);

    comTwistDirection.AddObj(rsMsg3, -1);
    comTwistDirection.AddObj('S',0);
    comTwistDirection.AddObj('Z',1);
  end; //if mVariante = 1 then begin
end; //procedure TfrmStyleFilter.FillCombos
//-----------------------------------------------------------------------------
procedure TfrmStyleFilter.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle,WM_NEXTDLGCTL,0,0);
  end; //if Key = #13 then begin
end; //procedure TfrmStyleFilter.FormKeyPress
//------------------------------------------------------------------------------
procedure TfrmStyleFilter.FillComboData(aCombo: TNWComboBox; aSQL: string);
var
  xStr: String;
begin
  aCombo.Items.Clear;
  with TmmAdoDataSet.Create(nil) do
  try
    ConnectionString := GetDefaultConnectionString;
    aCombo.AddObj(rsMsg3, -1);
    CommandText := aSQL;
    Open;
    while not Eof do begin
      if mVariante = 1 then
        xStr := Format('%s@%s', [FieldByName('name').AsString, cSensingHeadClassNames[TSensingHeadClass(FieldByName('c_head_class').AsInteger)]])
      else
        xStr := FieldByName('name').AsString;

      aCombo.AddObj(xStr, FieldByName('id').AsInteger);
      Next;
    end; //while not Eof do begin
  finally
    Free;
  end;// with TmmAdoDataSet.Create(nil)
end; //procedure GetComboData.GetComboData
//------------------------------------------------------------------------------
procedure TfrmStyleFilter.comYMSettingsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  xStr1, xStr2: String;
  xPos: Integer;
begin
  with TNWComboBox(Control) do begin
    xStr1 := Items.Strings[Index];
    xPos := Pos('@', xStr1);
    if xPos > 0 then begin
      xStr2 := Copy(xStr1, xPos+1, Length(xStr1) - xPos);
      xStr1 := Copy(xStr1, 1, xPos - 1);
    end else
      xStr2 := ' ';

    with Canvas do begin
      if odSelected in State then begin
        Brush.Color := clHighlight;
        Font.Color := clHighlightText;
      end;
      FillRect(Rect);
      TextOut(Rect.Left + 2,   Rect.Top + 0, xStr1);
      TextOut(Rect.Right - 60, Rect.Top + 0, xStr2);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TfrmStyleFilter.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Filter_setzen.htm');

  laYarnUnit.Caption := cYarnUnitsStr[TYarnUnit(TMMSettingsReader.Instance.Value[cYarnCntUnit])];
end;

end. //u_style_filter

