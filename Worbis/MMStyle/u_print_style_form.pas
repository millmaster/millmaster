(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_print_style_form.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  6.12.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 | 0.00 | PW  | column "c_opt_speed" removed
| 22.06.01 |      | pw  | printing MxN t_style_settings
| 13.07.01 |      | Wss | Bug when printing style form solved -> c_ym_setname field removed from qryStyle
| 16.11.01 |      | Wss | Visibility of imperfection factors depending on global settings
| 19.11.01 |      | Wss | Corrections for YarnCount handlings
| 21.11.2001       Wss | YarnCount, Meter/Weight calculation handling adapted
| 05.12.2001       Wss | YarnUnit added at YarnCount values
| 28.02.2002       Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
| 10.10.2002       LOK | Umbau ADO
| 28.10.2002       Wss | - Link zu t_dw_style_shift entfernt, da diese Tabelle gel�scht wurde
| 21.06.2004| 1.00|SDo | Layout Anpassung
| 18.01.2005  1.01|SDo | Umbau & Anpassungen an XML Vers. 5
| 25.02.2005| 1.01|SDo | Black & White Print
|=========================================================================================*)

{$i symbols.inc}

unit u_print_style_form;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Db,
  Qrctrls, QuickRpt, ExtCtrls, IvDictio, IvMulti, IvEMulti,
  Printers, mmTranslator, mmQRDBText, mmQRShape, mmQRSysData, mmQRLabel,
  mmQRBand, mmQuickRep, BaseForm, mmDataSource, mmQRSubDetail,
  ADODB, mmADODataSet, u_dmStyle, mmQRImage;

type
  TfrmPrintStyleForm = class(TmmForm)
    QuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitel: TmmQRLabel;
    DetailBand1: TmmQRBand;
    mmTranslator: TmmTranslator;
    mmQRShape1: TmmQRShape;
    mmQRLabel1: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRDBText1: TmmQRDBText;
    mmQRDBText3: TmmQRDBText;
    mmQRShape2: TmmQRShape;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel11: TmmQRLabel;
    mmQRLabel12: TmmQRLabel;
    mmQRDBText6: TmmQRDBText;
    mmQRDBText7: TmmQRDBText;
    mmQRDBText8: TmmQRDBText;
    mmQRShape3: TmmQRShape;
    mmQRLabel13: TmmQRLabel;
    mmQRDBText4: TmmQRDBText;
    mmQRLabel14: TmmQRLabel;
    mmQRDBText5: TmmQRDBText;
    mmQRLabel15: TmmQRLabel;
    shStyleColor: TmmQRShape;
    mmQRLabel16: TmmQRLabel;
    mmQRShape6: TmmQRShape;
    mmQRLabel17: TmmQRLabel;
    mmQRDBText11: TmmQRDBText;
    mmQRLabel18: TmmQRLabel;
    mmQRDBText12: TmmQRDBText;
    mmQRLabel19: TmmQRLabel;
    mmQRDBText13: TmmQRDBText;
    shUserFields: TmmQRShape;
    laUserFields: TmmQRLabel;
    laUserField1: TmmQRLabel;
    laUserField2: TmmQRLabel;
    laUserField3: TmmQRLabel;
    laUserField4: TmmQRLabel;
    laUserField5: TmmQRLabel;
    laUserField6: TmmQRLabel;
    laUserField7: TmmQRLabel;
    laUserField8: TmmQRLabel;
    laUserField9: TmmQRLabel;
    laUserField10: TmmQRLabel;
    edUserField1: TmmQRDBText;
    edUserField2: TmmQRDBText;
    edUserField3: TmmQRDBText;
    edUserField4: TmmQRDBText;
    edUserField5: TmmQRDBText;
    edUserField6: TmmQRDBText;
    edUserField7: TmmQRDBText;
    edUserField8: TmmQRDBText;
    edUserField9: TmmQRDBText;
    edUserField10: TmmQRDBText;
    mmQRLabel2: TmmQRLabel;
    mmQRDBText2: TmmQRDBText;
    shImpFactor: TmmQRShape;
    qlImpFactorTitle: TmmQRLabel;
    qlImpFactorNepsStr: TmmQRLabel;
    qlImpFactorNeps: TmmQRDBText;
    qlImpFactorThickStr: TmmQRLabel;
    qlImpFactorThick: TmmQRDBText;
    qlImpFactorThinStr: TmmQRLabel;
    qlImpFactorThin: TmmQRDBText;
    qlImpFactorSmallStr: TmmQRLabel;
    qlImpFactorSmall: TmmQRDBText;
    qlImpFactorSFI: TmmQRDBText;
    qlImpFactorSFIStr: TmmQRLabel;
    mmQRLabel7: TmmQRLabel;
    mmQRDBText10: TmmQRDBText;
    mmQRSubDetail1: TmmQRSubDetail;
    mmQRBand1: TmmQRBand;
    mmQRLabel20: TmmQRLabel;
    mmQRLabel9: TmmQRLabel;
    mmQRDBText14: TmmQRDBText;
    shYMSetColor: TmmQRShape;

    dsStyle: TmmDataSource;
    qryStyle: TmmADODataSet;
    qryStylecalc_State: TStringField;
    qryStylec_style_name: TStringField;
    qryStylecalc_twist: TStringField;
    qryStylec_assortment_name: TStringField;
    qryStylec_m_per_bob: TIntegerField;
    qryStylec_m_per_cone: TIntegerField;
    qryStylecalc_Package_Type: TStringField;
    qryStylec_color: TIntegerField;
    qryStylec_num1: TFloatField;
    qryStylec_num2: TFloatField;
    qryStylec_num3: TFloatField;
    qryStylec_string1: TStringField;
    qryStylec_string2: TStringField;
    qryStylec_string3: TStringField;
    qryStylec_string4: TStringField;
    qryStylec_string5: TStringField;
    qryStylec_string6: TStringField;
    qryStylec_string7: TStringField;
    qryStylec_style_id: TSmallintField;
    qryStylec_style_state: TSmallintField;
    qryStylec_twist_left: TBooleanField;
    qryStylec_package_type: TSmallintField;
    qryStylec_mm_per_bob: TIntegerField;
    qryStylec_mm_per_cone: TIntegerField;
    qryStylecalc_copsgewicht: TFloatField;
    qryStylecalc_konengewicht: TFloatField;
    qryStylecalc_HistoricalData: TBooleanField;
    qryStylecalc_CurrentProdGroup: TBooleanField;
    qryStylec_nr_of_threads: TSmallintField;
    qryStylecalc_ProdPeriod: TStringField;
    qryStylec_Factor_INeps: TFloatField;
    qryStylec_Factor_IThick: TFloatField;
    qryStylec_Factor_IThin: TFloatField;
    qryStylec_Factor_ISmall: TFloatField;
    qryStylec_Factor_SFI: TFloatField;
    qryStylec_slip: TSmallintField;
    qryStylecalc_slip: TFloatField;
    qryYMSet: TmmADODataSet;
    qryYMSetc_ym_set_name: TStringField;
    qryYMSetc_color: TIntegerField;
    qryStylec_twist: TFloatField;
    qryStylecalc_yarncnt: TFloatField;
    qryStylec_yarncnt: TFloatField;
    QRBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData2: TmmQRSysData;
    qlCompanyName: TmmQRLabel;

    procedure qryStyleCalcFields(DataSet: TDataSet);
    procedure DetailBand1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure mmQRSubDetail1BeforePrint(Sender: TQRCustomBand;
      var PrintBand: Boolean);
    procedure QuickRep1BeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure mmQRDBText4Print(sender: TObject; var Value: String);
  private
    mStyleUF: rUserFieldsT;
    mPrintBW: Boolean;
    procedure InitUserFields;
  public
    constructor Create(aOwner: TComponent; aStyleID: integer; aPrintBW: Boolean); reintroduce; virtual;
    procedure Execute;
  end; //TfrmPrintStyleForm

var
  frmPrintStyleForm: TfrmPrintStyleForm;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  MMUGlobal,
  u_main, u_lib, u_common_lib, mmLib, u_resourcestrings, SettingsReader,
  XMLDef;

{$R *.DFM}

constructor TfrmPrintStyleForm.Create(aOwner: TComponent; aStyleID: integer; aPrintBW: Boolean);
begin
  inherited create(aOwner);

  mPrintBW := aPrintBW;

  mStyleUF := ReadUserFields(1);
  InitUserFields;
  QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;

  qryStyle.Parameters.ParamByName('c_style_id').value := aStyleID;
  qryStyle.Open;

  qryYMSet.Parameters.ParamByName('c_style_id').value := aStyleID;
  qryYMSet.Open;
end; //procedure TfrmPrintStyleForm.Init
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleForm.Execute;
begin
  {$ifdef PrintPreview}
  QuickRep1.Preview;
  {$else}
  QuickRep1.Print;
  {$endif}
end; //procedure TfrmPrintStyleForm.Execute
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleForm.qryStyleCalcFields(DataSet: TDataSet);
const
  cQry1 = 'WHERE c_style_id = %d';

begin
  // convert YarnCountUnit to user defined in MMConfiguration
  if qryStylec_yarncnt.AsInteger > 0 then
    qryStylecalc_yarncnt.Value := YarnCountConvert(yuNm, gCountUnit, qryStylec_yarncnt.AsFloat);

  if qryStylec_mm_per_bob.IsNull then
    qryStylecalc_copsgewicht.clear
  else
    qryStylecalc_copsgewicht.value := MeterToWeightUnit(wug, yuNm, qryStylec_yarncnt.value, qryStylec_mm_per_bob.AsFloat / 1000);
//    qryStylecalc_copsgewicht.value := MeterToWeightUnit(wug, gCountUnit, qryStylecalc_yarncnt.value, qryStylec_mm_per_bob.AsFloat / 1000);

  if qryStylec_mm_per_cone.IsNull then
    qryStylecalc_konengewicht.clear
  else
    qryStylecalc_konengewicht.value := MeterToWeightUnit(wug, yuNm, qryStylec_yarncnt.value, qryStylec_mm_per_cone.AsFloat / 1000);
//    qryStylecalc_konengewicht.value := MeterToWeightUnit(wug, gCountUnit, qryStylecalc_yarncnt.value, qryStylec_mm_per_cone.AsFloat / 1000);

  if qryStylec_twist.AsFloat > 0.0 then
    with qryStylec_twist do
      qryStylecalc_twist.value := FormatFloat(DisplayFormat, AsFloat) + ' ' + cTwistDirAr[qryStylec_twist_left.AsBoolean];
{
  if qryStylec_twist.AsInteger > 0 then
    qryStyletwist.value := formatfloat(',0',qryStylec_twist.asinteger)
                          +' ' +cTwistDirAr[qryStylec_twist_left.asboolean];
{}

  qryStylecalc_State.value := GetStateStr(qryStylec_style_state.AsInteger);
  qryStylecalc_Package_Type.value := GetPackageTypeStr(qryStylec_package_type.AsInteger);

  qryStylecalc_HistoricalData.AsBoolean   := GetRecordCount('t_prodgroup',format(cQry1,[qryStylec_style_id.asinteger])) > 0;
  qryStylecalc_CurrentProdGroup.AsBoolean := GetRecordCount('t_prodgroup_state',format(cQry1,[qryStylec_style_id.asinteger])) > 0;

  qryStylecalc_ProdPeriod.asstring := GetProductionPeriod(qryStylec_style_id.asinteger,
                                                          qryStylecalc_HistoricalData.AsBoolean,
                                                          qryStylecalc_CurrentProdGroup.AsBoolean);

  // Wss: new added field c_slip in table t_style (factor 1000 = int 1500 -> 1.500
  qryStylecalc_slip.Value := qryStylec_slip.Value / 1000.0;
  if (qryStylecalc_slip.Value < cMinSlip) or (qryStylecalc_slip.Value > cMaxSlip) then
    qryStylecalc_slip.AsString := '';
end; //procedure TfrmPrintStyleForm.qryStyleCalcFields
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleForm.DetailBand1BeforePrint(Sender: TQRCustomBand;
  var PrintBand: Boolean);
begin
  if not mPrintBW then
     shStyleColor.Brush.Color := qryStylec_color.AsInteger
  else
     shStyleColor.Brush.Color := GetGryScale(qryStylec_color.AsInteger);


end; //procedure TfrmPrintStyleForm.DetailBand1BeforePrint
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleForm.InitUserFields;
var
  i,k : integer;
begin
  DetailBand1.Height := 440;
  if mStyleUF.FieldCount > 0 then begin
    laUserFields.Enabled := true;
    shUserFields.Enabled := true;
    shUserFields.Height := (mStyleUF.FieldCount *25) +10;
    DetailBand1.Height := shUserFields.Top +shUserFields.Height +25;

    for i := 1 to 10 do
      if mStyleUF.fields[i].Active then begin
        k := mStyleUF.fields[i].Position;
        TmmQRDBText(FindComponent('edUserField' +inttostr(k))).Enabled := true;
        TmmQRDBText(FindComponent('edUserField' +inttostr(k))).DataField := mStyleUF.fields[i].InternalName;
        TmmQRLabel(FindComponent('laUserField' +inttostr(k))).Enabled := true;
        TmmQRLabel(FindComponent('laUserField' +inttostr(k))).Caption := mStyleUF.fields[i].FieldName;
      end; //if mStyleUF.fields[i].Active then begin
  end; //if mStyleUF.FieldCount > 0 then begin
end; //procedure TfrmPrintStyleForm.InitUserFields
//-----------------------------------------------------------------------------
procedure TfrmPrintStyleForm.mmQRSubDetail1BeforePrint(
  Sender: TQRCustomBand; var PrintBand: Boolean);
begin
  inherited;
  if not mPrintBW then
     shYMSetColor.Brush.Color := qryYMSetc_color.AsInteger
  else
     shYMSetColor.Brush.Color := GetGryScale(qryYMSetc_color.AsInteger);


end; //procedure TfrmPrintStyleForm.mmQRSubDetail1BeforePrint
//------------------------------------------------------------------------------
procedure TfrmPrintStyleForm.QuickRep1BeforePrint(Sender: TCustomQuickRep;
  var PrintReport: Boolean);
begin
  qlImpFactorTitle.Enabled    := gImpFactorVisible;
  qlImpFactorNepsStr.Enabled  := gImpFactorVisible;
  qlImpFactorNeps.Enabled     := gImpFactorVisible;
  qlImpFactorThickStr.Enabled := gImpFactorVisible;
  qlImpFactorThick.Enabled    := gImpFactorVisible;
  qlImpFactorThinStr.Enabled  := gImpFactorVisible;
  qlImpFactorThin.Enabled     := gImpFactorVisible;
  qlImpFactorSmallStr.Enabled := gImpFactorVisible;
  qlImpFactorSmall.Enabled    := gImpFactorVisible;
  qlImpFactorSFIStr.Enabled   := gImpFactorVisible;
  qlImpFactorSFI.Enabled      := gImpFactorVisible;
  shImpFactor.Enabled         := gImpFactorVisible;


  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

end;
//------------------------------------------------------------------------------
procedure TfrmPrintStyleForm.mmQRDBText4Print(sender: TObject; var Value: String);
begin
  Value := Format('%s/%s %s', [Value, qryStylec_nr_of_threads.AsString, cYarnUnitsStr[gCountUnit]]);
end;

end. //u_print_style_form

