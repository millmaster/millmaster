(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_import_style.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.12.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 | 0.00 | PW  | column "c_opt_speed" removed
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 22.06.01         pw  | - field c_ym_set_id removed
                         - insert default c_ym_set_id into t_style_settings on new record
| 28.02.2002       Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
| 10.10.2002       LOK | Umbau ADO
|=========================================================================================*)

{$I symbols.inc}

unit u_import_style;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, MMSecurity, ComCtrls, mmStatusBar, IvDictio, IvMulti, IvEMulti,
  mmTranslator, ActnList, mmActionList, ToolWin, mmToolBar, IvMlDlgs,
  mmSaveDialog, StdCtrls, mmEdit, Nwcombo, Buttons, mmSpeedButton, mmLabel,
  mmGroupBox, ExtCtrls, mmPanel, Grids, AdvGrid, mmStringGrid, mmListView,
  Db, DBGrids, mmDBGrid, mmDataSource, mmMemTable, mmOpenDialog, mmCheckBox, ADODB, mmADODataSet, u_dmStyle,
  mmADOCommand;

type
  TfrmImportStyle = class(TmmForm)
    mmActionList1: TmmActionList;
    acImport: TAction;
    acCancel: TAction;
    acHelp: TAction;
    acSaveDefault: TAction;
    acOpenDefault: TAction;
    acSelectFile: TAction;
    acSecurity: TAction;
    mmTranslator1: TmmTranslator;
    mmStatusBar1: TmmStatusBar;
    MMSecurityControl1: TMMSecurityControl;
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton9: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    mmLabel4: TmmLabel;
    mmSpeedButton5: TmmSpeedButton;
    mmLabel5: TmmLabel;
    edFilename: TmmEdit;
    comSeparator: TNWComboBox;
    gbFields: TmmGroupBox;
    mmDBGrid1: TmmDBGrid;

    dsFields: TmmDataSource;
    tabFields: TmmADODataSet;
    tabFieldsid: TIntegerField;
    tabFieldsfieldname: TStringField;
    tabFieldsinternalname: TStringField;
    tabFieldsrequired: TBooleanField;
    tabFieldskeyfield: TBooleanField;
    tabFieldsposition: TIntegerField;
    qry1: TmmADODataSet;
    tabConnection: TmmADODataSet;
    mmOpenDialog1: TmmOpenDialog;
    cbColumnTitle: TmmCheckBox;
    qry2: TmmADOCommand;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure acSaveDefaultExecute(Sender: TObject);
    procedure acOpenDefaultExecute(Sender: TObject);
    procedure acSelectFileExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tabFieldsBeforeInsert(DataSet: TDataSet);
    procedure Update_ImportFields;
    procedure tabFieldsBeforePost(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    mImportFields: rExportFieldsT;
    mProtokoll: TStringList;
    procedure DisplayHint(Sender: TObject);
    procedure InitEditControls;
    function DefinitionOK(aMessage: boolean): boolean;
    function ImportStyleData: boolean;
    procedure UpdateAssortments;
    procedure UpdateStyles;
    function GetFieldPosition(aFieldname: string): integer;
    function GetAssortmentID(aLine: string): integer;
    function GetNumericValue(aLine, aFieldname: string): double;
    function GetStringValue(aLine, aFieldname: string): string;
  public
  end; //TfrmImportStyle

var
  frmImportStyle: TfrmImportStyle;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs,
  u_main, u_common_lib, u_lib, u_resourcestrings, mmCommonLib, u_view_protokoll,
  u_progress;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Dem Feld %s muss eine gueltige Position zugewiesen werden!'; //ivlm
  cMsg2 = '(*)Import-Datei %s nicht vorhanden !'; //ivlm
  cMsg3 = '(*)Neues Sortiment %s in Sortiment-Tabelle eingefuegt'; //ivlm
  cMsg4 = '(*)Artikel %s existiert bereits. Soll der Artikel aktualisiert werden?'; //ivlm
  cMsg5 = '(*)Neuer Artikel %s in Artikel-Tabelle eingefuegt'; //ivlm
  cMsg6 = '(*)Artikel %s wurde aktualisiert'; //ivlm
  cMsg7 = '(*)Der Artikelstamm wurde erfolgreich importiert. Moechten Sie das Import-Protokoll sehen?'; //ivlm
  cMsg8 = '(*)Es trat ein Fehler beim Importieren auf. Der Artikelstamm wurde nicht importiert. Moechten Sie das Protokoll sehen?'; //ivlm
  cMsg9 = '(*)Start des Datenimports: %s'; //ivlm
  cMsg10 = '(*)Artikel %s uebersprungen'; //ivlm
  cMsg11 = '(*)Artikelstamm wird importiert...'; //ivlm

procedure TfrmImportStyle.DisplayHint(Sender: TObject);
begin
  mmStatusBar1.SimpleText := GetLongHint(Application.Hint);
end; //procedure TfrmImportStyle.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Artikeldaten_importieren.htm');
  Application.OnHint := DisplayHint;
  gUserFields := ReadUserFields(1);
  mProtokoll := TStringList.Create;

  tabConnection.Open;

  tabFields.CommandText := gTempPath + '@mm_impdef1.db';
{ TODO -oLOK -cUmbau ADO : CreateTable }
//  tabFields.CreateTable;

  InitEditControls;
end; //procedure TfrmImportStyle.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.FormDestroy(Sender: TObject);
begin
  mProtokoll.Free;

  try
    tabFields.Active := False;
{ TODO -oLOK -cUmbau ADO : CreateTable }
//    tabFields.DeleteTable;
  except
  end; //try..except

  mImportFields.Fields := nil;
  Set_DisplayHint;
end; //procedure TfrmImportStyle.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acCancelExecute(Sender: TObject);
begin
//  ModalResult := mrCancel;
  Close;
end; //procedure TfrmImportStyle.acCancelExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TfrmImportStyle.acHelpExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acImportExecute(Sender: TObject);
begin
  if not DefinitionOK(True) then exit;
  Update_ImportFields;
  if not ImportStyleData then exit;
  ModalResult := mrOK;
end; //procedure TfrmImportStyle.acImportExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acSaveDefaultExecute(Sender: TObject);
begin
  if not DefinitionOK(True) then exit;
  Update_ImportFields;
  WriteImportFields(mImportFields);
end; //procedure TfrmImportStyle.acSaveDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acOpenDefaultExecute(Sender: TObject);
begin
  InitEditControls;
end; //procedure TfrmImportStyle.acOpenDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acSelectFileExecute(Sender: TObject);
begin
  with mmOpenDialog1 do begin
    FileName := edFilename.Text;
    if Execute then edFilename.Text := FileName;
  end; //with mmOpenDialog1 do begin
end; //procedure TfrmImportStyle.acSelectFileExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl1.Configure;
end; //procedure TfrmImportStyle.acSecurityExecute
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle, WM_NEXTDLGCTL, 0, 0);
  end; //if TmmDBGrid(Sender) <> mmDBGrid1 then begin
end; //procedure TfrmImportStyle.FormKeyPress
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.InitEditControls;
var
  i, k: integer;
  xImportField: rExportFieldT;
  xShowField: boolean;

begin
  //read default data
  mImportFields := ReadImportFields;

  //init combos
  comSeparator.Clear;
  comSeparator.AddObj(';', 0);
  comSeparator.AddObj('|', 1);
  comSeparator.AddObj('#', 2);
  comSeparator.ItemIndex := mImportFields.FieldSeparator;

  edFilename.Text := mImportFields.Filename;

  cbColumnTitle.Checked := mImportFields.WithFieldNames;

  tabFields.DisableControls;
  try
    //init grid
    if tabFields.Active then begin
      tabFields.Close;
      try
{ TODO -oLOK -cUmbau ADO : CreateTable }
//        tabFields.EmptyTable;
      except
      end; //try..except
    end; //if tabFields.Active then begin

    tabFields.Open;
    tabFields.BeforeInsert := nil;
    tabFields.BeforeDelete := nil;
    tabFields.BeforePost := nil;

    for i := 1 to cMaxExportFields do begin
      xImportField := GetExportFieldByID(i);

      //show only "CanImport" and "User defined" fields
      xShowField := xImportField.CanImport;
      if xImportField.UserField then xShowField := xShowField and GetUserFieldByName(xImportField.InternalName).Active;

      if xShowField then begin
        tabFields.Insert;
        tabFieldsid.AsInteger := xImportField.ID;
        tabFieldsfieldname.AsString := LTrans(xImportField.FieldName);
        tabFieldsinternalname.AsString := xImportField.InternalName;
        tabFieldsrequired.AsBoolean := xImportField.Required;
        tabFieldskeyfield.AsBoolean := xImportField.KeyField;

        //get positions from default profile
        for k := 0 to high(mImportFields.Fields) do
          if mImportFields.Fields[k].ID = xImportField.ID then begin
            tabFieldsposition.AsInteger := mImportFields.Fields[k].Position;
            break;
          end; //if mImportFields.Fields[k].ID = xImportField.ID then begin

        tabFields.Post;
      end; //if xShowField then begin
    end; //for i := 1 to cMaxExportFields do begin

    tabFields.First;
  finally
    mmDBGrid1.SelectedField := tabFieldsposition;
    tabFields.BeforeInsert := tabFieldsBeforeInsert;
    tabFields.BeforeDelete := tabFieldsBeforeInsert;
    tabFields.BeforePost := tabFieldsBeforePost;
    tabFields.EnableControls;
  end; //try..finally
end; //procedure TfrmImportStyle.InitGrid
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.tabFieldsBeforeInsert(DataSet: TDataSet);
begin
  SysUtils.Abort;
end; //procedure TfrmImportStyle.tabFieldsBeforeInsert
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.Update_ImportFields;
var
  xCount,
    xID: integer;

begin
  mImportFields.Fields := nil;
  mImportFields.Filename := trim(edFilename.Text);
  mImportFields.FieldSeparator := comSeparator.GetItemID;
  mImportFields.WithFieldNames := cbColumnTitle.Checked;

  xCount := 0;

  with tabFields do begin
    xID := tabFieldsid.AsInteger;
    try
      DisableControls;
      First;
      while not Eof do begin
        if tabFieldsposition.AsInteger > 0 then begin
          inc(xCount);
          SetLength(mImportFields.Fields, xCount);
          mImportFields.Fields[xCount - 1].ID := tabFieldsid.AsInteger;
          mImportFields.Fields[xCount - 1].Position := tabFieldsposition.AsInteger;
        end; //if tabFieldsposition.AsInteger > 0 then begin
        Next;
      end; //while not Eof do begin
    finally
      Locate('id', xID, []);
      EnableControls;
    end; //try..finally
  end; //with tabFields do begin
end; //procedure TfrmImportStyle.acSaveDefaultExecute
//-----------------------------------------------------------------------------
function TfrmImportStyle.DefinitionOK(aMessage: boolean): boolean;
var
  xID: integer;

begin
  Result := False;
  if not PostRecord(tabFields) then exit;

  xID := tabFieldsid.AsInteger;
  with tabFields do begin
    try
      DisableControls;
      First;
      while not Eof do begin
        if tabFieldsrequired.Value and tabFieldsposition.IsNull then begin
          if aMessage then begin
            MessageBeep(0);
            MMMessageDlg(Format(cMsg1, [tabFieldsfieldname.asstring]), mtError, [mbOK], 0, Self);
          end; //if aMessage then begin
          exit;
        end; //if tabFieldsrequired.Value and tabFieldsposition.IsNull then begin
        Next;
      end; //while not Eof do begin

      Result := True;
    finally
      if Result then tabFields.Locate('id', xID, []);
      EnableControls;
    end; //try..finally
  end; //with tabFields do begin
end; //function TfrmImportStyle.DefinitionOK
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.tabFieldsBeforePost(DataSet: TDataSet);
//var xOK: boolean;

begin
  //if tabFieldsrequired.Value then xOK := DBFieldOk(tabFieldsposition,'',1,999,true)
  //                           else xOK := DBFieldOk(tabFieldsposition,'',1,999,false);
  //if not xOK then Abort;

  if not DBFieldOk(tabFieldsposition, '', 1, 999, False) then Abort;
end; //procedure TfrmImportStyle.tabFieldsBeforePost
//-----------------------------------------------------------------------------
function TfrmImportStyle.GetFieldPosition(aFieldname: string): integer;
var
  i: integer;
  xField: rExportFieldT;

begin
  Result := -1;
  for i := 0 to high(mImportFields.Fields) do begin
    xField := GetExportFieldByID(mImportFields.Fields[i].ID);
    if lowercase(xField.InternalName) = lowercase(aFieldname) then
      Result := mImportFields.Fields[i].Position;
  end; //for i := 0 to high(mImportFields.Fields) do begin
end; //function TfrmImportStyle.GetFieldPosition
//-----------------------------------------------------------------------------
function TfrmImportStyle.GetAssortmentID(aLine: string): integer;
const
  cQry1 = 'SELECT c_assortment_id FROM t_assortment WHERE UPPER(c_assortment_name) = UPPER(:c_assortment_name)';

var
  xPos: integer;
  xName: string;

begin
  Result := 1; //=> default assortment id
  xPos := GetFieldPosition('c_assortment_name');

  if xPos > 0 then begin
    xName := trim(GetSubStr(aLine, comSeparator.GetItemText[1], xPos));
    if xName <> '' then
      with TmmAdoDAtaSet.Create(Application) do try
        Connection := dmStyle.conDefault;
        CommandText := cQry1;
        Parameters.ParamByName('c_assortment_name').value := xName;
        Open;
        Result := fieldbyname('c_assortment_id').AsInteger;
      finally
        Free;
      end; //try..finally
  end //if xPos > 0 then begin
end; //function TfrmImportStyle.GetAssortmentID
//-----------------------------------------------------------------------------
function TfrmImportStyle.GetNumericValue(aLine, aFieldname: string): double;
var
  xCode,
    xPos: integer;
  xStr: string;
  xValue: double;

begin
  Result := -1; //=> indicator for empty (null) fields
  xPos := GetFieldPosition(aFieldname);

  if xPos > 0 then begin
    xStr := trim(GetSubStr(aLine, comSeparator.GetItemText[1], xPos));
    if xStr <> '' then begin
      xStr := ReplaceChar(',', '.', xStr);
      // xStr := ReplaceChar('.',DecimalSeparator,xStr);

      val(xStr, xValue, xCode);
      if xCode = 0 then Result := xValue;
    end; //if xStr <> '' then begin
  end //if xPos > 0 then begin
end; //function TfrmImportStyle.GetNumericValue
//-----------------------------------------------------------------------------
function TfrmImportStyle.GetStringValue(aLine, aFieldname: string): string;
var
  xPos: integer;

begin
  xPos := GetFieldPosition(aFieldname);
  if xPos > 0 then
    Result := trim(GetSubStr(aLine, comSeparator.GetItemText[1], xPos))
  else
    Result := '';
end; //function TfrmImportStyle.GetStringValue
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.UpdateAssortments;
const
  cQry1 = 'SELECT COUNT(*) AS ''count'' FROM t_assortment WHERE UPPER(c_assortment_name) = UPPER(:c_assortment_name)';
  cQry2 = 'INSERT INTO t_assortment(c_assortment_id, c_assortment_name, c_assortment_state) VALUES(:c_assortment_id, :c_assortment_name, :c_assortment_state)';

var
  xID,
    xAssortmentPos: integer;
  xLine,
    xAssortmentName: string;
  f: system.TextFile;

begin
  xAssortmentPos := GetFieldPosition('c_assortment_name');
  if xAssortmentPos < 1 then exit;

  assignfile(f, mImportFields.Filename);
  try
    reset(f);
  except
    exit;
  end; //try..except

  with TmmAdoDAtaSet.Create(Application) do try
    Connection := dmStyle.conDefault;

    //read title
    if cbColumnTitle.Checked then
      if not system.eof(f) then readln(f, xLine);

    // read data
    while not system.eof(f) do begin
      readln(f, xLine);
      xAssortmentName := trim(GetSubStr(xLine, comSeparator.GetItemText[1], xAssortmentPos));
      if xAssortmentName <> '' then begin
        CommandText := cQry1;
        Parameters.ParamByName('c_assortment_name').value := xAssortmentName;
        try
          Active := True;
          if FieldByName('count').AsInteger = 0 then begin
            Active := False;

            xID := MakeUniqueID(2, 't_assortment', 'c_assortment_id');
            if xID > 0 then begin
              with TmmAdoCommand.Create(nil) do try
                Connection := dmStyle.ConDefault;
                CommandText := cQry2;
                Parameters.ParamByName('c_assortment_id').value := xID;
                Parameters.ParamByName('c_assortment_name').value := xAssortmentName;
                Parameters.ParamByName('c_assortment_state').value := ord(ssActive);
                Execute;
              finally
                free;
              end;// with TmmAdoCommand.Create(nil) do try
              mProtokoll.Add(Format(cMsg3, [xAssortmentName]));
//                mProtokoll.Add(format(LTrans(cMsg3),[xAssortmentName]));
            end; //if xID > 0 then begin
          end; //if FieldByName('count').asinteger = 0 then begin
        finally
          Active := False;
        end; //try..finally
      end; //if xAssortmentName <> '' then begin
    end; //while not system.eof(f) do
  finally
    closefile(f);
    Free;
  end; //with TmmAdoDAtaSet.Create(Application) do try
end; // procedure TfrmImportStyle.UpdateAssortments;
//-----------------------------------------------------------------------------
procedure TfrmImportStyle.UpdateStyles;
const
  cQry1 = 'SELECT c_style_id FROM t_style WHERE UPPER(c_style_name) = UPPER(:c_style_name)';
  cQry2 = 'INSERT INTO t_style(c_style_id, c_assortment_id, c_style_name, c_color, c_yarncnt, c_nr_of_threads, c_twist_left, c_style_state) ' +
    'VALUES(:c_style_id, :c_assortment_id, :c_style_name, :c_color, :c_yarncnt, :c_nr_of_threads, :c_twist_left, :c_style_state)';

  //...........................................................
  procedure AddLine(aFieldname: string; aValue: Variant);
  var
    i: integer;
    f: double;
    s: string;
  begin
    case VarType(aValue) of
      varSmallint, varInteger: begin
          i := aValue;
          if i > 0 then qry1.CommandText := qry1.CommandText + Format(', %s = %d', [aFieldname, i]);
        end; //varSmallint,varInteger
      varSingle, varDouble: begin
          f := aValue;
          if f > 0 then begin
            qry1.CommandText := qry1.CommandText + Format(', %s = :%s', [aFieldname, aFieldname]);
            qry1.Parameters.ParamByName(aFieldname).value := f;
          end; //if f > 0 then begin
        end; //varSingle,varDouble
      varString: begin
          s := aValue;
          if s <> '' then begin
            qry1.CommandText := qry1.CommandText + Format(', %s = :%s', [aFieldname, aFieldname]);
            qry1.Parameters.ParamByName(aFieldname).value := s;
          end; //if aValue <> '' then begin
        end; //varString
    end; //case VarType(aValue) of
  end; //procedure AddLine
  //...........................................................
var
  f: system.TextFile;
  xLine: string;
  xUpdate: boolean;
  xTotalRecs,
    xLastMR: integer;
  xData: rImportDataT;
  xNewRec: boolean;
  xProgress: TProgressForm;

begin
  assignfile(f, mImportFields.Filename);
  try
    reset(f);
  except
    exit;
  end; //try..except

  xLastMR := mrNone;
  with qry1 do begin
    xProgress := TProgressForm.Create(Application);

    xTotalRecs := CountImportRecords(mImportFields.Filename);
    if cbColumnTitle.Checked then dec(xTotalRecs);

    xProgress.Init(cMsg11, xTotalRecs, 1);
    xProgress.Hide;
    try
      //read title
      if cbColumnTitle.Checked then
        if not system.eof(f) then readln(f, xLine);

      //read data
      while not system.eof(f) do begin
        readln(f, xLine);

        xProgress.StepIt;
        Application.ProcessMessages;
        Application.ProcessMessages;

        fillchar(xData, sizeof(xdata), 0);

        xData.c_style_name := GetStringValue(xLine, 'c_style_name');
        xData.c_yarncnt := GetNumericValue(xLine, 'c_yarncnt');
//        xData.c_yarncnt := round(GetNumericValue(xLine,'c_yarncnt') *10);

        if (xData.c_style_name <> '') and (xData.c_yarncnt > 0) then begin
          CommandText := cQry1;
          Parameters.ParamByName('c_style_name').value := xData.c_style_name;
          Active := True;
          xData.c_style_id := FieldByName('c_style_id').AsInteger;
          Active := False;

          xData.c_assortment_name := GetStringValue(xLine, 'c_assortment_name');
          xData.c_assortment_id := GetAssortmentID(xLine);
          xData.c_nr_of_threads := round(GetNumericValue(xLine, 'c_nr_of_threads'));
          xData.c_twist := GetNumericValue(xLine, 'c_twist');
          xData.twist_direction := uppercase(GetStringValue(xLine, 'twist_direction'));

          xData.c_mm_per_cone := round(GetNumericValue(xLine, 'm_per_cone') * 1000);
          xData.c_mm_per_bob := round(GetNumericValue(xLine, 'm_per_bob') * 1000);
          xData.c_color := round(GetNumericValue(xLine, 'c_color'));
          xData.c_num1 := round(GetNumericValue(xLine, 'c_num1'));
          xData.c_num2 := round(GetNumericValue(xLine, 'c_num2'));
          xData.c_num3 := round(GetNumericValue(xLine, 'c_num3'));
          xData.c_string1 := GetStringValue(xLine, 'c_string1');
          xData.c_string2 := GetStringValue(xLine, 'c_string2');
          xData.c_string3 := GetStringValue(xLine, 'c_string3');
          xData.c_string4 := GetStringValue(xLine, 'c_string4');
          xData.c_string5 := GetStringValue(xLine, 'c_string5');
          xData.c_string6 := GetStringValue(xLine, 'c_string6');
          xData.c_string7 := GetStringValue(xLine, 'c_string7');
          xData.c_inepsconst := GetNumericValue(xLine, 'c_inepsconst');
          xData.c_ithickconst := GetNumericValue(xLine, 'c_ithickconst');
          xData.c_ithinconst := GetNumericValue(xLine, 'c_ithinconst');
          xData.c_ismallconst := GetNumericValue(xLine, 'c_ismallconst');
          xData.c_sficonst := GetNumericValue(xLine, 'c_sficonst');
          xData.c_style_state := Round(GetNumericValue(xLine, 'c_style_state'));

          //insert style
          xNewRec := xData.c_style_id = 0;
          if xNewRec then begin
            xData.c_style_id := MakeUniqueID(2, 't_style', 'c_style_id');

            CommandText := cQry2;
            Parameters.ParamByName('c_style_id').value := xData.c_style_id;
            Parameters.ParamByName('c_assortment_id').value := xData.c_assortment_id;
            Parameters.ParamByName('c_style_name').value := xData.c_style_name;
            Parameters.ParamByName('c_color').value := clWhite;
            Parameters.ParamByName('c_yarncnt').value := xData.c_yarncnt;
            if xData.c_nr_of_threads < 1 then xData.c_nr_of_threads := 1;
            Parameters.ParamByName('c_nr_of_threads').value := xData.c_nr_of_threads;
            Parameters.ParamByName('c_twist_left').value := 1; //default = Z
            if xData.c_style_state in [ord(ssActive), ord(ssInactive)] then
              Parameters.ParamByName('c_style_state').value := xData.c_style_state
            else
              Parameters.ParamByName('c_style_state').value := ord(ssActive);

            Open;

            qry2.Parameters.ParamByName('c_style_id').value := xData.c_style_id;
            qry2.Execute;

            xUpdate := True;

            mProtokoll.Add(Format(cMsg5, [xData.c_style_name]));
          end //if xNewRec then begin
          else begin
            if not (xLastMR in [mrNoToAll, mrYesToAll]) then begin
              xLastMR := MMMessageDlg(Format(cMsg4, [xData.c_style_name]), mtConfirmation, [mbYes, mbNo, mbYesToAll, mbNoToAll], 0, Self);
              //show progress bar
              if xLastMR in [mrNoToAll, mrYesToAll] then xProgress.Show;
              Application.ProcessMessages;
            end; //if not(xLastMR in [mrNoToAll,mrYesToAll]) then begin
            xUpdate := xLastMR in [mrYes, mrYesToAll];

            if not xUpdate then mProtokoll.Add(Format(cMsg10, [xData.c_style_name]));
          end; //else begin

          if xUpdate then begin
            if not xNewRec then mProtokoll.Add(Format(cMsg6, [xData.c_style_name]));
            CommandText := '';
            CommandText := 'UPDATE t_style SET';
            //dummy due to ', '
            CommandText := CommandText + 'c_style_name = :c_style_name';
            Parameters.ParamByName('c_style_name').value := xData.c_style_name;

            if xData.c_assortment_name <> '' then
              AddLine('c_assortment_id', xData.c_assortment_id);

            AddLine('c_yarncnt', xData.c_yarncnt);
            AddLine('c_nr_of_threads', xData.c_nr_of_threads);
            AddLine('c_twist', xData.c_twist);

            if xData.twist_direction <> '' then
              if xData.twist_direction = 'S' then
                CommandText := CommandText + ', c_twist_left = 0'
              else
                CommandText := CommandText + ', c_twist_left = 1';

            AddLine('c_mm_per_cone', xData.c_mm_per_cone);
            AddLine('c_mm_per_bob', xData.c_mm_per_bob);
            AddLine('c_color', xData.c_color);
            AddLine('c_num1', xData.c_num1);
            AddLine('c_num2', xData.c_num2);
            AddLine('c_num3', xData.c_num3);
            AddLine('c_string1', xData.c_string1);
            AddLine('c_string2', xData.c_string2);
            AddLine('c_string3', xData.c_string3);
            AddLine('c_string4', xData.c_string4);
            AddLine('c_string5', xData.c_string5);
            AddLine('c_string6', xData.c_string6);
            AddLine('c_string7', xData.c_string7);
            AddLine('c_inepsconst', xData.c_inepsconst);
            AddLine('c_ithickconst', xData.c_ithickconst);
            AddLine('c_ithinconst', xData.c_ithinconst);
            AddLine('c_ismallconst', xData.c_ismallconst);
            AddLine('c_sficonst', xData.c_sficonst);
            AddLine('c_style_state', xData.c_style_state);

            CommandText := CommandText + Format('WHERE c_style_id = %d', [xData.c_style_id]);

            // SQL.SaveToFile('c:\temp\xx.txt');

            Open;
          end; //if xUpdate then begin
        end; //if (xStyleName <> '') and (xYarncnt > 0) then begin
      end; //while not system.eof(f) do
    finally
      closefile(f);
      xProgress.Free;
    end; //try..finally
  end; //with qry1 do begin
end; //procedure TfrmImportStyle.UpdateStyles
//-----------------------------------------------------------------------------
function TfrmImportStyle.ImportStyleData: boolean;
var
  xShowLog: boolean;

begin
  Result := False;
  mProtokoll.Clear;

  //importfile must exist
  if not FileExists(mImportFields.Filename) then begin
    MessageBeep(0);
    MMMessageDlg(Format(cMsg2, [mImportFields.Filename]), mtError, [mbOK], 0, Self);
    exit;
  end; //if not FileExists(mImportFields.Filename) then begin

{ TODO -oLOK -cUmbau ADO : Database }
//  if not frmMain.mDatabase.Connected then frmMain.mDatabase.Connected := True;
{ TODO -oLOK -cUmbau ADO : Database }
//  frmMain.mDatabase.StartTransaction;
  try
    //insert new assortments, only if column assortment_name was selected
    mProtokoll.Add(Format(cMsg9, [formatdatetime('dd/mm/yy hh:nn:ss', now)]));
    mProtokoll.Add(replicate('=', 60));
    UpdateAssortments;
    UpdateStyles;

    //mProtokoll.SaveToFile('c:\temp\import_protokoll.txt');

{ TODO -oLOK -cUmbau ADO : Database }
//    frmMain.mDatabase.Commit;
    Result := True;

    MessageBeep(0);
    xShowLog := MMMessageDlg(cMsg7, mtInformation, [mbNo, mbYes], 0, Self) = mrYes;
  except
{ TODO -oLOK -cUmbau ADO : Database }
//    frmMain.mDatabase.Rollback;
    MessageBeep(0);
    xShowLog := MMMessageDlg(cMsg8, mtError, [mbNo, mbYes], 0, Self) = mrYes;
  end; //try..except

  if xShowLog then
    with TfrmViewProtokoll.Create(Application) do begin
      Init(mProtokoll);
      ShowModal;
      Free;
    end; //with TfrmViewProtokoll.Create(Application) do begin
end; //function TfrmImportStyle.ImportStyleData

procedure TfrmImportStyle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end. //u_import_style

