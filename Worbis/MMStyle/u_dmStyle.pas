(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_dmStyle.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Style Datamodul
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.10.2002          LOK | Datei erstellt
|=========================================================================================*)
unit u_dmStyle;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADODataSet, mmADOConnection;

type
  TdmStyle = class(TDataModule)
    conDefault: TmmADOConnection;
    dseQuery: TmmADODataSet;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmStyle: TdmStyle;

implementation

{$R *.DFM}

end.
