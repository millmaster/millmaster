inherited frmExportStyle: TfrmExportStyle
  Left = 466
  Top = 101
  Width = 596
  Height = 554
  ActiveControl = edFilename
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = '(*)Artikeldaten exportieren'
  Constraints.MaxWidth = 596
  Constraints.MinHeight = 440
  Constraints.MinWidth = 596
  KeyPreview = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 26
    Width = 588
    Height = 479
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    OnResize = mmPanel1Resize
    object gbFields: TmmGroupBox
      Left = 5
      Top = 135
      Width = 580
      Height = 339
      Anchors = [akLeft, akTop, akBottom]
      Caption = '(*)Feldauswahl'
      TabOrder = 1
      CaptionSpace = True
      object bInsert: TmmSpeedButton
        Left = 264
        Top = 93
        Width = 24
        Height = 24
        Action = acInsert
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888808800
          88808800088088000080880008808800888088088880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bDelete: TmmSpeedButton
        Left = 264
        Top = 120
        Width = 24
        Height = 24
        Action = acDelete
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
          08808800088080000880880008808880088088880880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton1: TmmSpeedButton
        Left = 546
        Top = 93
        Width = 24
        Height = 24
        Action = acMoveUp
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808888
          88800000000080000080880008808880888088888880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton2: TmmSpeedButton
        Left = 546
        Top = 120
        Width = 24
        Height = 24
        Action = acMoveDown
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808880
          88808800088080000080000000008888888088888880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel1: TmmLabel
        Left = 10
        Top = 20
        Width = 247
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '(*)Vorhandene Felder'
        Color = 11184810
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 294
        Top = 20
        Width = 247
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '(*)Export Felder'
        Color = 11184810
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbAvailable: TNWListBox
        Left = 10
        Top = 33
        Width = 247
        Height = 294
        Anchors = [akLeft, akTop, akBottom]
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        Sorted = True
        TabOrder = 0
        OnDblClick = acInsertExecute
        OnDragDrop = ItemDragDrop
        OnDragOver = lbAvailableDragOver
      end
      object lbExportFields: TNWListBox
        Left = 294
        Top = 33
        Width = 247
        Height = 294
        Anchors = [akLeft, akTop, akBottom]
        DragMode = dmAutomatic
        ItemHeight = 13
        MultiSelect = True
        TabOrder = 1
        OnDblClick = acDeleteExecute
        OnDragDrop = ItemDragDrop
        OnDragOver = lbExportFieldsDragOver
      end
    end
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 580
      Height = 125
      Caption = '(*)Export-Datei'
      TabOrder = 0
      CaptionSpace = True
      object mmLabel3: TmmLabel
        Left = 10
        Top = 48
        Width = 130
        Height = 13
        AutoSize = False
        Caption = '(30)Dezimal-Trennzeichen'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel4: TmmLabel
        Left = 10
        Top = 23
        Width = 130
        Height = 13
        AutoSize = False
        Caption = '(30)Dateiname'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton5: TmmSpeedButton
        Left = 536
        Top = 20
        Width = 24
        Height = 22
        Action = acSelectFile
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel5: TmmLabel
        Left = 10
        Top = 73
        Width = 130
        Height = 13
        AutoSize = False
        Caption = '(30)Feld-Trennzeichen'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object comDecimal: TNWComboBox
        Left = 150
        Top = 45
        Width = 50
        Height = 21
        Style = csDropDownList
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 13
        ParentFont = False
        TabOrder = 1
      end
      object edFilename: TmmEdit
        Left = 150
        Top = 20
        Width = 382
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object comSeparator: TNWComboBox
        Left = 150
        Top = 70
        Width = 50
        Height = 21
        Style = csDropDownList
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ItemHeight = 13
        ParentFont = False
        TabOrder = 2
      end
      object cbColumnTitle: TmmCheckBox
        Left = 10
        Top = 98
        Width = 550
        Height = 17
        Caption = '(*)Feldnamen in erste Zeile exportieren'
        TabOrder = 3
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 588
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 3
      Top = 0
      Action = acCancel
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 34
      Top = 0
      Action = acOpenDefault
    end
    object ToolButton4: TToolButton
      Left = 57
      Top = 0
      Action = acSaveDefault
    end
    object ToolButton5: TToolButton
      Left = 80
      Top = 0
      Width = 8
      ImageIndex = 3
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 88
      Top = 0
      Action = acExport
    end
    object ToolButton7: TToolButton
      Left = 111
      Top = 0
      Width = 8
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton8: TToolButton
      Left = 119
      Top = 0
      Action = acHelp
    end
    object ToolButton9: TToolButton
      Left = 142
      Top = 0
      Action = acSecurity
    end
  end
  object mmStatusBar1: TmmStatusBar
    Left = 0
    Top = 505
    Width = 588
    Height = 22
    Panels = <>
    SimplePanel = True
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 400
    Top = 56
    TargetsData = (
      1
      6
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Title'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'SimpleText'
        0))
  end
  object mmActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 368
    Top = 56
    object acExport: TAction
      Hint = '(*)Artikeldaten exportieren'
      ImageIndex = 35
      OnExecute = acExportExecute
    end
    object acCancel: TAction
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 24
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
    object acInsert: TAction
      Hint = '(*)Markiertes Feld hinzufuegen'
      OnExecute = acInsertExecute
    end
    object acDelete: TAction
      Hint = '(*)Markiertes Feld entfernen'
      OnExecute = acDeleteExecute
    end
    object acMoveUp: TAction
      Hint = '(*)Markiertes Feld nach oben'
      OnExecute = acMoveUpExecute
    end
    object acMoveDown: TAction
      Hint = '(*)Markiertes Feld nach unten'
      OnExecute = acMoveDownExecute
    end
    object acSaveDefault: TAction
      Hint = '(*)Einstellungen als Standard festlegen'
      ImageIndex = 3
      OnExecute = acSaveDefaultExecute
    end
    object acOpenDefault: TAction
      Hint = '(*)Standard Export-Einstellungen oeffnen'
      ImageIndex = 1
      OnExecute = acOpenDefaultExecute
    end
    object acSelectFile: TAction
      Hint = '(*)Datei auswaehlen'
      OnExecute = acSelectFileExecute
    end
    object acSecurity: TAction
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 28
      OnExecute = acSecurityExecute
    end
  end
  object mmSaveDialog1: TmmSaveDialog
    Options = [ofHideReadOnly, ofEnableSizing]
    Left = 453
    Top = 99
  end
  object qryStyle: TmmADODataSet
    Connection = dmStyle.conDefault
    OnCalcFields = qryStyleCalcFields
    CommandText = 
      'SELECT s.c_style_id, s.c_assortment_id,       s.c_style_name, a.' +
      'c_assortment_name,       s.c_yarncnt, s.c_nr_of_threads, s.c_twi' +
      'st, s.c_twist_left,       s.c_mm_per_bob, s.c_mm_per_cone,      ' +
      ' s.c_mm_per_bob /1000 AS '#39'm_per_bob'#39', s.c_mm_per_cone /1000 AS '#39 +
      'm_per_cone'#39',       s.c_color, s.c_num1, s.c_num2, s.c_num3, s.c_' +
      'string1,       s.c_string2, s.c_string3, s.c_string4, s.c_string' +
      '5, s.c_string6,       s.c_string7, s.c_package_type, s.c_style_s' +
      'tate,       s.c_Factor_INeps, s.c_Factor_IThick, s.c_Factor_IThi' +
      'n, s.c_Factor_ISmall,       s.c_Factor_SFI, s.c_slipFROM t_style' +
      ' sJOIN t_assortment a ON a.c_assortment_id = s.c_assortment_idOR' +
      'DER BY s.c_style_name'
    Parameters = <>
    Left = 368
    Top = 104
    object qryStylec_style_id: TSmallintField
      FieldName = 'c_style_id'
    end
    object qryStylec_assortment_id: TSmallintField
      FieldName = 'c_assortment_id'
    end
    object qryStylec_style_name: TStringField
      FieldName = 'c_style_name'
    end
    object qryStylec_assortment_name: TStringField
      FieldName = 'c_assortment_name'
    end
    object qryStylec_nr_of_threads: TSmallintField
      FieldName = 'c_nr_of_threads'
    end
    object qryStylec_twist: TFloatField
      FieldName = 'c_twist'
    end
    object qryStylec_twist_left: TBooleanField
      FieldName = 'c_twist_left'
    end
    object qryStylem_per_bob: TIntegerField
      FieldName = 'm_per_bob'
    end
    object qryStylem_per_cone: TIntegerField
      FieldName = 'm_per_cone'
    end
    object qryStylec_color: TIntegerField
      FieldName = 'c_color'
    end
    object qryStylec_num1: TFloatField
      FieldName = 'c_num1'
      DisplayFormat = ',0.###'
    end
    object qryStylec_num2: TFloatField
      FieldName = 'c_num2'
      DisplayFormat = ',0.###'
    end
    object qryStylec_num3: TFloatField
      FieldName = 'c_num3'
      DisplayFormat = ',0.###'
    end
    object qryStylec_string1: TStringField
      FieldName = 'c_string1'
      Size = 255
    end
    object qryStylec_string2: TStringField
      FieldName = 'c_string2'
      Size = 255
    end
    object qryStylec_string3: TStringField
      FieldName = 'c_string3'
      Size = 255
    end
    object qryStylec_string4: TStringField
      FieldName = 'c_string4'
      Size = 255
    end
    object qryStylec_string5: TStringField
      FieldName = 'c_string5'
      Size = 255
    end
    object qryStylec_string6: TStringField
      FieldName = 'c_string6'
      Size = 255
    end
    object qryStylec_string7: TStringField
      FieldName = 'c_string7'
      Size = 255
    end
    object qryStylec_package_type: TSmallintField
      FieldName = 'c_package_type'
    end
    object qryStylec_style_state: TSmallintField
      FieldName = 'c_style_state'
    end
    object qryStyletwist_direction: TStringField
      FieldKind = fkCalculated
      FieldName = 'twist_direction'
      Size = 1
      Calculated = True
    end
    object qryStylecalc_CopsWeight: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_CopsWeight'
      DisplayFormat = ',0.#'
      Calculated = True
    end
    object qryStylecalc_ConeWeight: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_ConeWeight'
      DisplayFormat = ',0.#'
      Calculated = True
    end
    object qryStylepackage_type: TStringField
      FieldKind = fkCalculated
      FieldName = 'package_type'
      Size = 50
      Calculated = True
    end
    object qryStylestyle_state: TStringField
      FieldKind = fkCalculated
      FieldName = 'style_state'
      Size = 50
      Calculated = True
    end
    object qryStylec_mm_per_bob: TIntegerField
      FieldName = 'c_mm_per_bob'
    end
    object qryStylec_mm_per_cone: TIntegerField
      FieldName = 'c_mm_per_cone'
    end
    object qryStylec_yarncnt: TFloatField
      FieldName = 'c_yarncnt'
    end
    object qryStylec_Factor_INeps: TFloatField
      FieldName = 'c_Factor_INeps'
      DisplayFormat = ',0.###'
    end
    object qryStylec_Factor_IThick: TFloatField
      FieldName = 'c_Factor_IThick'
      DisplayFormat = ',0.###'
    end
    object qryStylec_Factor_IThin: TFloatField
      FieldName = 'c_Factor_IThin'
      DisplayFormat = ',0.###'
    end
    object qryStylec_Factor_ISmall: TFloatField
      FieldName = 'c_Factor_ISmall'
      DisplayFormat = ',0.###'
    end
    object qryStylec_Factor_SFI: TFloatField
      FieldName = 'c_Factor_SFI'
      DisplayFormat = ',0.###'
    end
    object qryStylec_slip: TSmallintField
      FieldName = 'c_slip'
    end
    object qryStylecalc_slip: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_slip'
      DisplayFormat = ',0.###'
      Calculated = True
    end
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Artikeldaten exportieren'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 413
    Top = 101
  end
end
