inherited frmStyleChanger: TfrmStyleChanger
  Left = 204
  Top = 214
  Height = 422
  Caption = '(*)Artikelstamm bearbeiten'
  Position = poOwnerFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MMSelectNavigator: TMMUSBSelectNavigator
    Left = 0
    Top = 0
    Width = 632
    Height = 334
    Align = alClient
    Color = clBtnShadow
    ParentColor = False
    TabOrder = 0
    DoResize = True
    LabelColor = clInfoBk
    MultiSelectClearSet = True
    MultiSelectLot = True
    MultiSelectMach = True
    MultiSelectStyle = True
    MultiSelectTime = True
    OnAfterSelection = MMSelectNavigatorAfterSelection
    ShowSelection = True
    TimeMode = tmShift
    UseClearerSettings = False
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 334
    Width = 632
    Height = 61
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object mmLabel1: TmmLabel
      Left = 24
      Top = 11
      Width = 137
      Height = 13
      Caption = '(*)Verschieben nach Artikel...'
      FocusControl = vbStyle
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object vbStyle: TDBVisualBox
      Left = 24
      Top = 26
      Width = 145
      Height = 21
      Color = clWindow
      ItemHeight = 13
      TabOrder = 0
      Visible = True
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpTop
      Edit = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      DataField = 'c_style_name'
      DataSource = dsStyle
      KeyField = 'c_style_id'
    end
    object bGo: TmmButton
      Left = 192
      Top = 24
      Width = 75
      Height = 25
      Caption = '(10)Start'
      Enabled = False
      TabOrder = 1
      Visible = True
      OnClick = bGoClick
      AutoLabel.LabelPosition = lpLeft
    end
    object mmBitBtn1: TmmBitBtn
      Left = 504
      Top = 24
      Width = 120
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '(10)&Schliessen'
      TabOrder = 2
      Visible = True
      Kind = bkClose
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object slUpdateQuery: TmmVCLStringList
    Strings.Strings = (
      'UPDATE t_prodgroup'
      'SET c_style_id = %d, c_style_name = '#39'%s'#39
      'WHERE c_prod_id in (%s)')
    Left = 8
    Top = 232
  end
  object dsStyle: TmmDataSource
    DataSet = qryStyle
    Left = 272
    Top = 200
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 32
    Top = 40
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Filter'
        0))
  end
  object qryStyle: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 
      'select c_style_id, c_style_name '#13#10'from t_style '#13#10'order by c_styl' +
      'e_name'
    Parameters = <>
    Left = 240
    Top = 200
  end
end
