inherited frmSelectYMSettings: TfrmSelectYMSettings
  Left = 726
  Top = 105
  Caption = '(*)Reinigereinstellungen auswaehlen'
  ClientWidth = 557
  PixelsPerInch = 96
  TextHeight = 16
  inherited mmPanel4: TmmPanel
    Width = 557
    inherited bOK: TmmButton
      Left = 270
    end
    inherited bCancel: TmmButton
      Left = 365
    end
    inherited bHelp: TmmButton
      Left = 460
    end
  end
  inherited gbSelectionList: TmmPanel
    Width = 557
    inherited mmPanel1: TmmPanel
      Width = 260
      inherited mmLabel2: TmmLabel
        Width = 83
        Caption = '(*)Verfuegbar:'
      end
      inherited lbSource: TmmListBox
        Width = 260
        Style = lbOwnerDrawFixed
        OnDrawItem = lbSourceDrawItem
      end
    end
    inherited mmPanel2: TmmPanel
      Left = 262
      inherited bMoveRight: TmmSpeedButton
        Left = 3
      end
      inherited bMoveLeft: TmmSpeedButton
        Left = 3
      end
    end
    inherited mmPanel3: TmmPanel
      Left = 295
      Width = 260
      inherited mmLabel3: TmmLabel
        Width = 93
        Caption = '(*)Ausgewaehlt:'
      end
      inherited lbDest: TmmListBox
        Width = 260
        Style = lbOwnerDrawFixed
        OnDrawItem = lbSourceDrawItem
      end
    end
  end
  inherited mmActionList: TmmActionList
    inherited acHelp: TAction
      OnExecute = acHelpExecute
    end
  end
  object qry1: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 
      'SELECT c_ym_set_id, c_ym_set_name, c_head_class FROM t_xml_ym_se' +
      'ttings WHERE c_template_set = 1 AND c_ym_set_name IS NOT NULL AN' +
      'D RTRIM(c_ym_set_name) <> '#39#39' ORDER BY c_ym_set_name'
    Parameters = <>
    Left = 90
    Top = 66
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 24
    Top = 96
    TargetsData = (
      1
      9
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'SimpleText'
        0)
      (
        '*'
        'Hints'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'DisplayValues'
        0))
  end
end
