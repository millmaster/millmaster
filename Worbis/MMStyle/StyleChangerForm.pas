(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: StyleChangerForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Style Edit Form
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01, Multilizer, InfoPower 3000
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| unbekannt  | 0.00 | -   | Datei erstellt
| 10.10.2002          LOK | Umbau ADO
| 28.10.2002          Wss | In FormDestroy wird MMSelectNavigator.StoppFilling aufgerufen
|=========================================================================================*)
unit StyleChangerForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, IvDictio, IvMulti, IvEMulti, mmTranslator, Db, mmDataSource,
  mmVCLStringList, Buttons, mmBitBtn, StdCtrls,
  mmButton, mmComboBox, DBVisualBox, mmLabel, ExtCtrls, mmPanel,
  MMUSBSelectNavigatorFrame, u_dmStyle, ADODB,
  mmADODataSet, mmAdoCommand;

type
  TfrmStyleChanger = class(TmmForm)
    MMSelectNavigator: TMMUSBSelectNavigator;
    mmPanel1: TmmPanel;
    mmLabel1: TmmLabel;
    vbStyle: TDBVisualBox;
    bGo: TmmButton;
    mmBitBtn1: TmmBitBtn;
    slUpdateQuery: TmmVCLStringList;
    dsStyle: TmmDataSource;
    mmTranslator: TmmTranslator;
    qryStyle: TmmADODataSet;
    procedure bGoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure MMSelectNavigatorAfterSelection(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
  public
  end;

var
  frmStyleChanger: TfrmStyleChanger;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, LoepfeGlobal, mmcs;
{$R *.DFM}
const
  cUpdProdgroup      = 'UPDATE t_prodgroup SET c_style_id = %d, c_style_name = ''%s'' WHERE c_prod_id in (%s)';
  cUpdProdgroupState = 'UPDATE t_prodgroup SET c_style_id = %d WHERE c_prod_id in (%s)';


//------------------------------------------------------------------------------
procedure TfrmStyleChanger.bGoClick(Sender: TObject);
begin
  with TmmAdoCommand.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    try
      CommandText := Format(cUpdProdgroup, [vbStyle.KeyValue, vbStyle.Text, MMSelectNavigator.LotID]);
      Execute;
      CommandText := Format(cUpdProdgroupState, [vbStyle.KeyValue, MMSelectNavigator.LotID]);
      Execute;
      MMSelectNavigator.Reset;
    except
      CodeSite.SendError(GetConnectionErrorMessage(dmStyle.conDefault.Errors));
    end;
  finally
    free;
  end;// with TmmAdoCommand.Create(nil) do try
end;
//------------------------------------------------------------------------------
procedure TfrmStyleChanger.FormShow(Sender: TObject);
begin
  qryStyle.Active := True;
end;
//------------------------------------------------------------------------------
procedure TfrmStyleChanger.MMSelectNavigatorAfterSelection(Sender: TObject);
begin
  bGo.Enabled := (MMSelectNavigator.LotID <> '');
end;
//------------------------------------------------------------------------------
procedure TfrmStyleChanger.FormCreate(Sender: TObject);
begin
  MMSelectNavigator.Init;
end;

procedure TfrmStyleChanger.FormDestroy(Sender: TObject);
begin
  MMSelectNavigator.StoppFilling;
end;

end.
