inherited frmStyleFilter: TfrmStyleFilter
  Left = 246
  Top = 403
  ActiveControl = edName
  Caption = '(*)Artikelsuche ueber Filter'
  ClientHeight = 223
  ClientWidth = 420
  KeyPreview = True
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object laName: TmmLabel
    Left = 5
    Top = 14
    Width = 125
    Height = 13
    Hint = '(*)Platzhalter "*" und "?"'
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Artikelname'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laAssortment: TmmLabel
    Left = 5
    Top = 64
    Width = 125
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Sortiment'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laYarncnt: TmmLabel
    Left = 5
    Top = 99
    Width = 125
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Garnfeinheit'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laThreads: TmmLabel
    Left = 251
    Top = 99
    Width = 90
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Fachung'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laTwist: TmmLabel
    Left = 5
    Top = 124
    Width = 125
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Drehung'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object taDirection: TmmLabel
    Left = 251
    Top = 124
    Width = 90
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Drehrichtung'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laState: TmmLabel
    Left = 5
    Top = 39
    Width = 125
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Status'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laYMSettings: TmmLabel
    Left = 5
    Top = 159
    Width = 125
    Height = 13
    Alignment = taRightJustify
    AutoSize = False
    Caption = '(20)Reinigereinstellungen'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laYarnUnit: TmmLabel
    Left = 218
    Top = 99
    Width = 31
    Height = 13
    AutoSize = False
    Caption = '0'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edName: TmmEdit
    Left = 135
    Top = 10
    Width = 275
    Height = 21
    Hint = '(*)Platzhalter "*" und "?"'
    Color = clWindow
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    Decimals = -1
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
  end
  object comAssortment: TNWComboBox
    Left = 135
    Top = 60
    Width = 275
    Height = 21
    Style = csDropDownList
    DropDownCount = 12
    ItemHeight = 13
    TabOrder = 2
  end
  object comState: TNWComboBox
    Left = 135
    Top = 35
    Width = 97
    Height = 21
    Style = csDropDownList
    DropDownCount = 12
    ItemHeight = 13
    TabOrder = 1
  end
  object comThreads: TNWComboBox
    Left = 344
    Top = 95
    Width = 66
    Height = 21
    Style = csDropDownList
    DropDownCount = 12
    ItemHeight = 13
    TabOrder = 4
  end
  object edYarnCount: TNumEdit
    Left = 135
    Top = 95
    Width = 80
    Height = 21
    Decimals = 2
    Digits = 12
    Masks.PositiveMask = '#,###.#'
    Max = 9999
    NumericType = ntGeneral
    TabOrder = 3
    UseRounding = True
    Validate = False
    ValidateString = 
      '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
      's %s'
  end
  object comTwistDirection: TNWComboBox
    Left = 344
    Top = 120
    Width = 66
    Height = 21
    Style = csDropDownList
    DropDownCount = 12
    ItemHeight = 13
    TabOrder = 6
  end
  object edTwist: TNumEdit
    Left = 135
    Top = 120
    Width = 80
    Height = 21
    Decimals = 2
    Digits = 12
    Masks.PositiveMask = '#,###'
    Max = 9999
    NumericType = ntGeneral
    TabOrder = 5
    UseRounding = True
    Validate = False
    ValidateString = 
      '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
      's %s'
  end
  object mmButton1: TmmButton
    Left = 135
    Top = 190
    Width = 75
    Height = 23
    Action = acOK
    Anchors = [akLeft, akBottom]
    Caption = '(9)OK'
    TabOrder = 8
    TabStop = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 215
    Top = 190
    Width = 75
    Height = 23
    Action = acCancel
    Anchors = [akLeft, akBottom]
    Caption = '(9)Abbrechen'
    TabOrder = 9
    TabStop = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object comYMSettings: TNWComboBox
    Left = 135
    Top = 155
    Width = 275
    Height = 21
    Style = csOwnerDrawFixed
    DropDownCount = 12
    ItemHeight = 15
    TabOrder = 7
    OnDrawItem = comYMSettingsDrawItem
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 312
    Top = 184
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Items'
        0))
  end
  object mmActionList1: TmmActionList
    Left = 344
    Top = 184
    object acOK: TAction
      Caption = '(10)&Uebernehmen'
      Hint = '(*)Suche starten'
      ShortCut = 16397
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(10)&Abbrechen'
      Hint = '(*)Suche abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
  end
end
