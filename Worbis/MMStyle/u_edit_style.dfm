inherited frmEditStyle: TfrmEditStyle
  Left = 252
  Top = 129
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = '(*)Artikelstamm bearbeiten'
  ClientHeight = 715
  ClientWidth = 910
  FormStyle = fsMDIChild
  KeyPreview = True
  Position = poScreenCenter
  Visible = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object paStyleList: TmmPanel
    Left = 0
    Top = 0
    Width = 220
    Height = 715
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object mmToolBar2: TmmToolBar
      Left = 0
      Top = 0
      Width = 220
      Height = 26
      EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
      Flat = True
      Images = frmMain.ImageList16x16
      Indent = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object ToolButton13: TToolButton
        Left = 3
        Top = 0
        Action = acClose
      end
      object ToolButton14: TToolButton
        Left = 26
        Top = 0
        Width = 8
        ImageIndex = 1
        Style = tbsSeparator
      end
      object ToolButton16: TToolButton
        Left = 34
        Top = 0
        Action = acApplyFilter
      end
      object ToolButton17: TToolButton
        Left = 57
        Top = 0
        Action = acCancelFilter
      end
      object ToolButton2: TToolButton
        Left = 80
        Top = 0
        Width = 8
        ImageIndex = 3
        Style = tbsSeparator
      end
      object ToolButton1: TToolButton
        Left = 88
        Top = 0
        Action = acSortStyle
      end
      object ToolButton15: TToolButton
        Left = 111
        Top = 0
        Width = 8
        ImageIndex = 2
        Style = tbsSeparator
      end
      object navSelect: TmmDBNavigator
        Left = 119
        Top = 0
        Width = 92
        Height = 22
        DataSource = dsSelect
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        Flat = True
        Hints.Strings = (
          '(*)Erster Datensatz'
          '(*)Vorgaengiger Datensatz'
          '(*)Naechster Datensatz'
          '(*)Letzter Datensatz'
          '(*)Einfuegen'
          '(*)Loeschen'
          '(*)Bearbeiten'
          '(*)Speichern'
          '(*)Abbrechen'
          '(*)Daten aktualisieren')
        TabOrder = 0
        UseMMHints = True
      end
    end
    object mmPanel2: TmmPanel
      Left = 0
      Top = 26
      Width = 220
      Height = 689
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object gbStyleList: TmmGroupBox
        Left = 0
        Top = 5
        Width = 210
        Height = 657
        Caption = '(*)&Artikelauswahl-Liste'
        TabOrder = 0
        object mmLabel1: TmmLabel
          Left = 10
          Top = 25
          Width = 81
          Height = 13
          Caption = '(20)Artikel Suche'
          FocusControl = isSearch
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object isSearch: TwwIncrementalSearch
          Left = 10
          Top = 40
          Width = 190
          Height = 21
          DataSource = dsSelect
          OnAfterSearch = isSearchAfterSearch
          ShowMatchText = True
          TabOrder = 0
        end
        object grSelect: TwwDBGrid
          Left = 10
          Top = 73
          Width = 190
          Height = 574
          Selected.Strings = (
            'c_style_name'#9'28'#9'(*)Artikelname')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = False
          Anchors = [akLeft, akTop, akBottom]
          DataSource = dsSelect
          KeyOptions = []
          Options = [dgColumnResize, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint]
          ParentColor = True
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          OnCalcCellColors = grSelectCalcCellColors
          OnDblClick = grSelectDblClick
          HideAllLines = True
        end
      end
      object sbRecordCount: TmmStatusBar
        Left = 0
        Top = 667
        Width = 220
        Height = 22
        Panels = <>
        SimplePanel = True
      end
    end
  end
  object paStyleEdit: TmmPanel
    Left = 220
    Top = 0
    Width = 690
    Height = 715
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object mmPanel1: TmmPanel
      Left = 0
      Top = 26
      Width = 690
      Height = 689
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object mmGroupBox1: TmmGroupBox
        Left = 5
        Top = 5
        Width = 370
        Height = 75
        Caption = '(*)&Identifikation'
        TabOrder = 0
        CaptionSpace = True
        object laStylename: TmmLabel
          Left = 5
          Top = 23
          Width = 110
          Height = 13
          Hint = '(*)Eindeutiger Artikelname oder Artikelnummer'
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Artikelname'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laAssortment: TmmLabel
          Left = 5
          Top = 48
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Sortiment'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bEditAssortment: TmmSpeedButton
          Left = 335
          Top = 45
          Width = 24
          Height = 21
          Action = acEditAssortment
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edc_style_name: TmmDBEdit
          Left = 120
          Top = 20
          Width = 210
          Height = 21
          Color = clWindow
          DataField = 'c_style_name'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object comAssortment: TmmDBLookupComboBox
          Left = 120
          Top = 45
          Width = 210
          Height = 21
          Color = clWindow
          DataField = 'c_assortment_id'
          DataSource = dsStyle
          DropDownRows = 15
          DropDownWidth = 400
          KeyField = 'c_assortment_id'
          ListField = 'c_assortment_name'
          ListSource = dsAssortment
          ReadOnly = True
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmGroupBox2: TmmGroupBox
        Left = 5
        Top = 286
        Width = 370
        Height = 104
        Caption = '(*)R&einigereinstellungen'
        TabOrder = 3
        CaptionSpace = True
        object laYMSettings: TmmLabel
          Left = 5
          Top = 23
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Bezeichnung'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bYMSettings: TmmSpeedButton
          Left = 336
          Top = 20
          Width = 24
          Height = 24
          Hint = '(*)Reinigereinstellungen'
          Action = acEditYMSettings
          Glyph.Data = {
            EE000000424DEE000000000000007600000028000000100000000F0000000100
            0400000000007800000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF0088888AA88888
            888888888AA88888888888888AA00088888888888AA08088888888880AA87088
            888888807A878088888888808A899088888888008A880888888880087AA08889
            099080780AA888899FF900770700000999990888078888890990088807888880
            800000000AA88888888888888AA888888888}
          ParentShowHint = False
          ShowHint = True
          Spacing = -1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmSpeedButton1: TmmSpeedButton
          Left = 336
          Top = 48
          Width = 24
          Height = 24
          Action = acSelectYMSettings
          Glyph.Data = {
            36030000424D3603000000000000360000002800000010000000100000000100
            1800000000000003000000000000000000000000000000000000FF00FFFF00FF
            FF00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFF00FFFF000000FF
            00FFFF00FFBF0000BF0000BF0000BF0000BF0000BF0000BF0000FF00FFFF00FF
            FF00FF00000000FFFFFFFFFF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FFFFFF000000000000FF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF
            FF00FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000
            000000FF00FF000000000000FF00FF000000000000FF00FF000000000000FF00
            FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000
            FF00FF000000000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FF000000FF00FF000000FF00FF00000000FFFFFFFFFF000000FF
            00FFFF00FF000000000000000000000000000000FF00FF000000FF00FFFF00FF
            FF00FF000000FFFFFF00FFFF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FFFF00FFFF00FF000000FF00FF00000000FFFF000000000000FF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000FF00FF000000
            FF00FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
            FFFF00FFFF00FF000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF000000
            000000FF00FF000000000000FF00FF000000000000FF00FF000000000000FF00
            FF000000000000000000FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF
            00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00FF}
          ParentShowHint = False
          ShowHint = True
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object lbYMSettings: TmmListBox
          Left = 120
          Top = 20
          Width = 210
          Height = 75
          Color = clInfoBk
          Enabled = True
          ItemHeight = 13
          Sorted = True
          Style = lbOwnerDrawFixed
          TabOrder = 0
          Visible = True
          OnDrawItem = lbYMSettingsDrawItem
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object mmGroupBox3: TmmGroupBox
        Left = 5
        Top = 85
        Width = 370
        Height = 94
        Caption = '(*)&Garndaten'
        TabOrder = 1
        CaptionSpace = True
        object laYarncnt: TmmLabel
          Left = 5
          Top = 19
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Garnfeinheit'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laThreads: TmmLabel
          Left = 232
          Top = 19
          Width = 85
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Fachung'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laColor: TmmLabel
          Left = 5
          Top = 69
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Farbe'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laTwist: TmmLabel
          Left = 5
          Top = 44
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Drehung'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel8: TmmLabel
          Left = 216
          Top = 44
          Width = 101
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Drehrichtung'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel9: TmmLabel
          Left = 216
          Top = 70
          Width = 101
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(12)Schlupf'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laYarnUnit: TmmLabel
          Left = 202
          Top = 19
          Width = 33
          Height = 13
          AutoSize = False
          Caption = '0'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edYarnCnt: TpwDBEdit
          Left = 120
          Top = 16
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'calc_YarnCnt'
          DataSource = dsStyle
          Enabled = True
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnKeyPress = edYarnCntKeyPress
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object comFachung: TmmDBValComboBox
          Left = 322
          Top = 16
          Width = 40
          Height = 21
          Style = csDropDownList
          Color = clWhite
          ItemHeight = 13
          TabOrder = 1
          Visible = True
          Items.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6')
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnly = True
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
          DataField = 'c_nr_of_threads'
          DataSource = dsStyle
          Values.Strings = (
            '1'
            '2'
            '3'
            '4'
            '5'
            '6')
        end
        object edc_twist: TmmDBEdit
          Left = 120
          Top = 41
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_twist'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 2
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object comDrehrichtung: TmmDBValComboBox
          Left = 322
          Top = 41
          Width = 40
          Height = 21
          Style = csDropDownList
          Color = clWhite
          ItemHeight = 13
          TabOrder = 3
          Visible = True
          Items.Strings = (
            'S'
            'Z')
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnly = True
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
          DataField = 'c_twist_left'
          DataSource = dsStyle
          Values.Strings = (
            'F'
            'T')
        end
        object colbColor: TmmColorButton
          Left = 120
          Top = 66
          Width = 80
          Height = 21
          Hint = '(*)Farbe auswaehlen...'
          AutoLabel.LabelPosition = lpLeft
          Caption = '(15)&Mehr...'
          Color = clBlack
          OnChangeColor = colbColorChangeColor
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          TabStop = True
          Visible = True
        end
        object edcalc_slip: TpwDBEdit
          Left = 322
          Top = 66
          Width = 39
          Height = 21
          Color = clWindow
          DataField = 'calc_slip'
          DataSource = dsStyle
          Enabled = True
          ReadOnly = True
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmGroupBox6: TmmGroupBox
        Left = 5
        Top = 182
        Width = 370
        Height = 100
        Caption = '(*)&Kops-, Konendaten'
        TabOrder = 2
        CaptionSpace = True
        object laCopsgewicht: TmmLabel
          Left = 5
          Top = 23
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Kopsgewicht (g)'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laKonengewicht: TmmLabel
          Left = 5
          Top = 48
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Konengewicht (g)'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel14: TmmLabel
          Left = 5
          Top = 73
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Konentyp'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bKopsGewicht: TmmSpeedButton
          Left = 205
          Top = 20
          Width = 24
          Height = 21
          Action = acEditCopsLength
          Glyph.Data = {
            EE000000424DEE0000000000000076000000280000000F0000000F0000000100
            0400000000007800000000000000000000001000000000000000000000008000
            00000080000080800000000080008000800000808000C0C0C00080808000FF00
            000000FF0000FFFF00000000FF00FF00FF0000FFFF00FFFFFF00777777777777
            777077700000000077707708888888880770770FF8F8F8F80770770F00000008
            0770770FF8F8F8F80770770F000000080770770FF8F8F8F80770770F00000008
            0770770FF8F8F8F80770770F000000080770770F000000080770770FFFFFFFF8
            077077700000000077707777777777777770}
          ParentShowHint = False
          ShowHint = True
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bKonenGewicht: TmmSpeedButton
          Left = 205
          Top = 45
          Width = 24
          Height = 21
          Action = acEditConeLength
          Glyph.Data = {
            EE000000424DEE0000000000000076000000280000000F0000000F0000000100
            0400000000007800000000000000000000001000000000000000000000008000
            00000080000080800000000080008000800000808000C0C0C00080808000FF00
            000000FF0000FFFF00000000FF00FF00FF0000FFFF00FFFFFF00777777777777
            777077700000000077707708888888880770770FF8F8F8F80770770F00000008
            0770770FF8F8F8F80770770F000000080770770FF8F8F8F80770770F00000008
            0770770FF8F8F8F80770770F000000080770770F000000080770770FFFFFFFF8
            077077700000000077707777777777777770}
          ParentShowHint = False
          ShowHint = True
          Spacing = -1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object comPackageType: TmmDBValComboBox
          Left = 120
          Top = 70
          Width = 210
          Height = 21
          Style = csDropDownList
          Color = clWhite
          DropDownCount = 12
          ItemHeight = 13
          TabOrder = 2
          Visible = True
          Items.Strings = (
            '(*)Konisch'
            '(*)Zylindrisch'
            '(*)Faerbekonen')
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnly = True
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
          DataField = 'c_package_type'
          DataSource = dsStyle
          Values.Strings = (
            '1'
            '2'
            '3')
        end
        object edcopsgewicht: TpwDBEdit
          Left = 120
          Top = 20
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'calc_copsgewicht'
          DataSource = dsStyle
          Enabled = True
          ReadOnly = True
          TabOrder = 0
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnKeyPress = edcopsgewichtKeyPress
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edkonengewicht: TpwDBEdit
          Left = 120
          Top = 45
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'calc_konengewicht'
          DataSource = dsStyle
          Enabled = True
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
          OnKeyPress = edkonengewichtKeyPress
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmGroupBox4: TmmGroupBox
        Left = 385
        Top = 5
        Width = 300
        Height = 174
        Caption = '(*)&Status'
        TabOrder = 4
        CaptionSpace = True
        object laState: TmmLabel
          Left = 5
          Top = 23
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Status'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel16: TmmLabel
          Left = 5
          Top = 48
          Width = 160
          Height = 13
          Hint = '(*)Sind von dem Artikel historische Daten vorhanden ?'
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Historische Daten'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel17: TmmLabel
          Left = 5
          Top = 73
          Width = 160
          Height = 13
          Hint = '(*)Ist der Artikel in einer aktuellen Produktionsgruppe ?'
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Aktuelle Partie(n) vorhanden'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel2: TmmLabel
          Left = 5
          Top = 98
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Produktions-Zeitraum'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmDBText3: TmmDBText
          Left = 170
          Top = 98
          Width = 120
          Height = 13
          DataField = 'calc_ProdPeriod'
          DataSource = dsStyle
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object imgHistorical: TImage
          Left = 190
          Top = 48
          Width = 16
          Height = 16
          Transparent = True
        end
        object imgCurrent: TImage
          Left = 190
          Top = 73
          Width = 16
          Height = 16
          Transparent = True
        end
        object comStyleState: TmmDBValComboBox
          Left = 170
          Top = 20
          Width = 120
          Height = 21
          Style = csDropDownList
          Color = clWhite
          ItemHeight = 13
          TabOrder = 0
          Visible = True
          Items.Strings = (
            '(20)Aktiv'
            '(20)Inaktiv')
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
          DataField = 'c_style_state'
          DataSource = dsStyle
          Values.Strings = (
            '1'
            '2')
        end
      end
      object gbUserFields: TmmGroupBox
        Left = 5
        Top = 397
        Width = 680
        Height = 265
        Caption = '(*)&Benutzerfelder'
        TabOrder = 6
        CaptionSpace = True
        object laUserField1: TmmLabel
          Left = 6
          Top = 24
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField1'
          FocusControl = edUserField1
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField2: TmmLabel
          Left = 6
          Top = 48
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField2'
          FocusControl = edUserField2
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField3: TmmLabel
          Left = 6
          Top = 72
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField3'
          FocusControl = edUserField3
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField4: TmmLabel
          Left = 6
          Top = 96
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField4'
          FocusControl = edUserField4
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField5: TmmLabel
          Left = 6
          Top = 120
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField5'
          FocusControl = edUserField5
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField6: TmmLabel
          Left = 6
          Top = 145
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField6'
          FocusControl = edUserField6
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField7: TmmLabel
          Left = 6
          Top = 169
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField7'
          FocusControl = edUserField7
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField8: TmmLabel
          Left = 6
          Top = 193
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField8'
          FocusControl = edUserField8
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField9: TmmLabel
          Left = 6
          Top = 217
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField9'
          FocusControl = edUserField9
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField10: TmmLabel
          Left = 6
          Top = 241
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField10'
          FocusControl = edUserField10
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object edUserField1: TmmDBEdit
          Left = 120
          Top = 20
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 0
          Visible = False
          AutoLabel.Control = laUserField1
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField2: TmmDBEdit
          Left = 120
          Top = 44
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 1
          Visible = False
          AutoLabel.Control = laUserField2
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField3: TmmDBEdit
          Left = 120
          Top = 68
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 2
          Visible = False
          AutoLabel.Control = laUserField3
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField4: TmmDBEdit
          Left = 120
          Top = 92
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 3
          Visible = False
          AutoLabel.Control = laUserField4
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField5: TmmDBEdit
          Left = 120
          Top = 116
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 4
          Visible = False
          AutoLabel.Control = laUserField5
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField6: TmmDBEdit
          Left = 120
          Top = 141
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 5
          Visible = False
          AutoLabel.Control = laUserField6
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField7: TmmDBEdit
          Left = 120
          Top = 165
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 6
          Visible = False
          AutoLabel.Control = laUserField7
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField8: TmmDBEdit
          Left = 120
          Top = 189
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 7
          Visible = False
          AutoLabel.Control = laUserField8
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField9: TmmDBEdit
          Left = 120
          Top = 213
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 8
          Visible = False
          AutoLabel.Control = laUserField9
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField10: TmmDBEdit
          Left = 120
          Top = 237
          Width = 550
          Height = 21
          Color = clWindow
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 9
          Visible = False
          AutoLabel.Control = laUserField10
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object gbImpFactors: TmmGroupBox
        Left = 385
        Top = 182
        Width = 300
        Height = 208
        Caption = '(*)Korrekturfaktor fuer Imperfektionen'
        TabOrder = 5
        Visible = False
        CaptionSpace = True
        object mmLabel3: TmmLabel
          Left = 5
          Top = 23
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Faktor INeps'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel4: TmmLabel
          Left = 5
          Top = 73
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Faktor IThick'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel5: TmmLabel
          Left = 5
          Top = 98
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Faktor IThin'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel6: TmmLabel
          Left = 5
          Top = 48
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Faktor ISmall'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel7: TmmLabel
          Left = 5
          Top = 123
          Width = 160
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Faktor SFI'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edc_Factor_INeps: TmmDBEdit
          Left = 170
          Top = 20
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_Factor_INeps'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edc_Factor_IThick: TmmDBEdit
          Left = 170
          Top = 70
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_Factor_IThick'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edc_Factor_IThin: TmmDBEdit
          Left = 170
          Top = 95
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_Factor_IThin'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 2
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edc_Factor_ISmall: TmmDBEdit
          Left = 170
          Top = 45
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_Factor_ISmall'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 3
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edc_Factor_SFI: TmmDBEdit
          Left = 170
          Top = 120
          Width = 80
          Height = 21
          Color = clWindow
          DataField = 'c_Factor_SFI'
          DataSource = dsStyle
          ReadOnly = True
          TabOrder = 4
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmStatusBar1: TmmStatusBar
        Left = 0
        Top = 667
        Width = 690
        Height = 22
        Panels = <>
        SimplePanel = True
      end
    end
    object mmToolBar1: TmmToolBar
      Left = 0
      Top = 0
      Width = 690
      Height = 26
      EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
      Flat = True
      Images = frmMain.ImageList16x16
      Indent = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object ToolButton11: TToolButton
        Left = 3
        Top = 0
        Action = acSecurity
      end
      object ToolButton9: TToolButton
        Left = 26
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton5: TToolButton
        Left = 34
        Top = 0
        Action = acPrint
      end
      object ToolButton12: TToolButton
        Left = 57
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton18: TToolButton
        Left = 65
        Top = 0
        Action = acStyleChanger
      end
      object ToolButton10: TToolButton
        Left = 88
        Top = 0
        Action = acUserFields
      end
      object ToolButton4: TToolButton
        Left = 111
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton6: TToolButton
        Left = 119
        Top = 0
        Action = acCopyStyle
      end
      object tbTemplate: TToolButton
        Left = 142
        Top = 0
        Action = acTemplate
      end
      object ToolButton3: TToolButton
        Left = 165
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object navStyle: TmmDBNavigator
        Left = 173
        Top = 0
        Width = 130
        Height = 22
        DataSource = dsStyle
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Flat = True
        Hints.Strings = (
          '(*)Erster Datensatz'
          '(*)Vorgaengiger Datensatz'
          '(*)Naechster Datensatz'
          '(*)Letzter Datensatz'
          '(*)Einfuegen'
          '(*)Loeschen'
          '(*)Bearbeiten'
          '(*)Speichern'
          '(*)Abbrechen'
          '(*)Daten aktualisieren')
        ConfirmDelete = False
        TabOrder = 0
        UseMMHints = True
      end
      object ToolButton7: TToolButton
        Left = 303
        Top = 0
        Width = 8
        ImageIndex = 4
        Style = tbsSeparator
      end
      object ToolButton8: TToolButton
        Left = 311
        Top = 0
        Action = acHelp
      end
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    OnLanguageChange = mmTranslatorLanguageChange
    Left = 160
    Top = 56
    TargetsData = (
      1
      8
      (
        '*'
        'SimpleText'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'DisplayValues'
        0)
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0)
      (
        '*'
        'Hints.Strings'
        0))
  end
  object mmActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    OnUpdate = mmActionList1Update
    Left = 128
    Top = 56
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = '(*)Artikelformular drucken...'
      Hint = '(*)Stammdaten-Blatt fuer aktuellen Artikel ausdrucken'
      ImageIndex = 2
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
    object acCopyStyle: TAction
      Caption = '(*)Artikeldaten kopieren...'
      Hint = '(*)Neuen Artikel mit den Daten des aktuellen Artikels anlegen'
      ImageIndex = 26
      OnExecute = acCopyStyleExecute
    end
    object acEditAssortment: TAction
      Caption = '...'
      Hint = '(*)Sortiment bearbeiten/hinzufuegen'
      OnExecute = acEditAssortmentExecute
    end
    object acEditYMSettings: TAction
      Hint = '(*)Reinigereinstellungen bearbeiten'
      OnExecute = acEditYMSettingsExecute
    end
    object acUserFields: TAction
      Caption = '(*)&Benutzerfelder...'
      Hint = '(*)Benutzerfelder definieren'
      ImageIndex = 27
      OnExecute = acUserFieldsExecute
    end
    object acEditCopsLength: TAction
      Hint = '(*)Kopslaenge eingeben'
      OnExecute = acEditCopsLengthExecute
    end
    object acEditConeLength: TAction
      Hint = '(*)Konenlaenge eingeben'
      OnExecute = acEditConeLengthExecute
    end
    object acDeleteStyle: TAction
      Hint = '(*)Artikel loeschen'
    end
    object acEditStyle: TAction
      Hint = '(*)Artikel bearbeiten'
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 28
      OnExecute = acSecurityExecute
    end
    object acApplyFilter: TAction
      Caption = '(*)Filter setzen'
      Hint = '(*)Filter fuer Artikeldaten setzen'
      ImageIndex = 29
      ShortCut = 16454
      OnExecute = acApplyFilterExecute
    end
    object acCancelFilter: TAction
      Hint = '(*)Filter aufheben'
      ImageIndex = 39
      OnExecute = acCancelFilterExecute
    end
    object acSortStyle: TAction
      Caption = '(*)Artikeltabelle sortieren'
      Hint = '(*)Artikeltabelle sortieren'
      ImageIndex = 40
      OnExecute = acSortStyleExecute
    end
    object acEditImpFactors: TAction
      Hint = '(*)Umrechnungsfaktoren bearbeiten'
    end
    object acSelectYMSettings: TAction
      Hint = '(*)Reinigereinstellungen auswaehlen'
      ImageIndex = 48
      OnExecute = acSelectYMSettingsExecute
    end
    object acStyleChanger: TAction
      Hint = '(*)Partie/Artikel Zuordung aendern'
      ImageIndex = 42
      OnExecute = acStyleChangerExecute
    end
    object acTemplate: TAction
      Caption = '(*)Vorlagenverwaltung'
      Hint = '(*)Vorlagenverwaltung'
      ImageIndex = 50
      OnExecute = acTemplateExecute
    end
  end
  object tabStyle: TmmADODataSet
    AutoCalcFields = False
    Connection = dmStyle.conDefault
    AfterInsert = tabStyleAfterInsert
    BeforePost = tabStyleBeforePost
    AfterPost = tabStyleAfterPost
    AfterCancel = tabStyleAfterScroll
    BeforeDelete = tabStyleBeforeDelete
    AfterDelete = tabStyleAfterDelete
    AfterScroll = tabStyleAfterScroll
    AfterRefresh = tabStyleAfterRefresh
    OnCalcFields = tabStyleCalcFields
    OnNewRecord = tabStyleNewRecord
    CommandText = 'dbo.t_style'
    CommandType = cmdTable
    IndexFieldNames = 'c_style_id'
    Parameters = <>
    Left = 513
    Top = 400
    object tabStylec_style_id: TSmallintField
      FieldName = 'c_style_id'
    end
    object tabStylec_assortment_id: TSmallintField
      FieldName = 'c_assortment_id'
    end
    object tabStylec_style_name: TStringField
      FieldName = 'c_style_name'
      Size = 50
    end
    object tabStylec_nr_of_threads: TSmallintField
      FieldName = 'c_nr_of_threads'
    end
    object tabStylec_yarncnt: TFloatField
      FieldName = 'c_yarncnt'
      Visible = False
    end
    object tabStylecalc_YarnCnt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_YarnCnt'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
      Calculated = True
    end
    object tabStylec_twist: TFloatField
      FieldName = 'c_twist'
      DisplayFormat = '0.0'
      EditFormat = '0.0'
    end
    object tabStylec_twist_left: TBooleanField
      FieldName = 'c_twist_left'
      DisplayValues = 'T;F'
    end
    object tabStylec_mm_per_bob: TIntegerField
      FieldName = 'c_mm_per_bob'
    end
    object tabStylec_package_type: TSmallintField
      FieldName = 'c_package_type'
    end
    object tabStylec_mm_per_cone: TIntegerField
      FieldName = 'c_mm_per_cone'
    end
    object tabStylec_m_per_spdhour: TIntegerField
      FieldName = 'c_m_per_spdhour'
    end
    object tabStylec_adjust: TSmallintField
      FieldName = 'c_adjust'
    end
    object tabStylec_color: TIntegerField
      FieldName = 'c_color'
    end
    object tabStylec_num1: TFloatField
      FieldName = 'c_num1'
    end
    object tabStylec_num2: TFloatField
      FieldName = 'c_num2'
    end
    object tabStylec_num3: TFloatField
      FieldName = 'c_num3'
    end
    object tabStylec_string1: TStringField
      FieldName = 'c_string1'
      Size = 255
    end
    object tabStylec_string2: TStringField
      FieldName = 'c_string2'
      Size = 255
    end
    object tabStylec_string3: TStringField
      FieldName = 'c_string3'
      Size = 255
    end
    object tabStylec_string4: TStringField
      FieldName = 'c_string4'
      Size = 255
    end
    object tabStylec_string5: TStringField
      FieldName = 'c_string5'
      Size = 255
    end
    object tabStylec_string6: TStringField
      FieldName = 'c_string6'
      Size = 255
    end
    object tabStylec_string7: TStringField
      FieldName = 'c_string7'
      Size = 255
    end
    object tabStylec_style_state: TSmallintField
      FieldName = 'c_style_state'
    end
    object tabStylec_slip: TSmallintField
      FieldName = 'c_slip'
    end
    object tabStylecalc_slip: TFloatField
      DisplayWidth = 5
      FieldKind = fkCalculated
      FieldName = 'calc_slip'
      DisplayFormat = ',0.###'
      EditFormat = '0.0'
      MaxValue = 1.5
      Calculated = True
    end
    object tabStylecalc_copsgewicht: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_copsgewicht'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
      Calculated = True
    end
    object tabStylecalc_konengewicht: TFloatField
      FieldKind = fkCalculated
      FieldName = 'calc_konengewicht'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
      Calculated = True
    end
    object tabStylecalc_HistoricalData: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'calc_HistoricalData'
      DisplayValues = 'Ja;Nein'
      Calculated = True
    end
    object tabStylecalc_CurrentProdGroup: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'calc_CurrentProdGroup'
      DisplayValues = 'Ja;Nein'
      Calculated = True
    end
    object tabStylecalc_ProdPeriod: TStringField
      FieldKind = fkCalculated
      FieldName = 'calc_ProdPeriod'
      Calculated = True
    end
    object tabStylec_Factor_INeps: TFloatField
      FieldName = 'c_Factor_INeps'
      DisplayFormat = '0.00'
    end
    object tabStylec_Factor_IThick: TFloatField
      FieldName = 'c_Factor_IThick'
      DisplayFormat = '0.00'
    end
    object tabStylec_Factor_IThin: TFloatField
      FieldName = 'c_Factor_IThin'
      DisplayFormat = '0.00'
    end
    object tabStylec_Factor_ISmall: TFloatField
      FieldName = 'c_Factor_ISmall'
      DisplayFormat = '0.00'
    end
    object tabStylec_Factor_SFI: TFloatField
      FieldName = 'c_Factor_SFI'
      DisplayFormat = '0.00'
    end
    object tabStylec_max_subID: TIntegerField
      FieldName = 'c_max_subID'
    end
  end
  object dsStyle: TmmDataSource
    AutoEdit = False
    DataSet = tabStyle
    OnStateChange = dsStyleStateChange
    Left = 514
    Top = 431
  end
  object dsAssortment: TmmDataSource
    DataSet = qryAssortment
    Left = 562
    Top = 431
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Artikelstamm bearbeiten'
    Active = True
    Left = 224
    Top = 56
  end
  object qrySelect: TmmADODataSet
    Connection = dmStyle.conDefault
    BeforeScroll = qrySelectBeforeScroll
    AfterScroll = qrySelectAfterScroll
    CommandText = 
      'SELECT c_style_id, c_style_name, c_style_stateFROM t_styleORDER ' +
      'BY c_style_name, c_style_id'
    Parameters = <>
    Left = 64
    Top = 292
    object qrySelectc_style_name: TStringField
      DisplayLabel = '(*)Artikelname'
      DisplayWidth = 28
      FieldName = 'c_style_name'
      Origin = 't_style.c_style_name'
    end
    object qrySelectc_style_id: TSmallintField
      FieldName = 'c_style_id'
      Origin = 't_style.c_style_id'
      Visible = False
    end
    object qrySelectc_style_state: TSmallintField
      FieldName = 'c_style_state'
      Origin = 't_style.c_style_state'
    end
  end
  object dsSelect: TwwDataSource
    DataSet = qrySelect
    Left = 104
    Top = 292
  end
  object ilBitmaps: TImageList
    Left = 192
    Top = 56
    Bitmap = {
      494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000420000004200000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004200
      0000006300000063000042000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      84008484840000000000000000000000000000000000000000000000FF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000420000000063
      0000006300000063000000630000420000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000042000000006300000063
      0000006300000063000000630000006300004200000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004200000000630000006300000063
      000000FF00000063000000630000006300000063000042000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8400000084000000840000008400848484000000840000008400000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000630000006300000063000000FF
      00000000000000FF000000630000006300000063000042000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000840000008400000084000000840000008400000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF00000063000000FF00000000
      0000000000000000000000FF0000006300000063000000630000420000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000000000000000
      000000000000000000000000000000FF00000063000000630000006300004200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000630000006300000063
      0000420000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000006300000063
      0000006300004200000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000063
      0000006300000063000042000000000000000000000000000000000000000000
      FF00000084000000840000008400848484000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000063000000630000006300004200000000000000000000000000FF000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000630000006300004200000000000000000000000000FF000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000630000006300000000000000000000000000000000
      FF000000840000000000000000000000000000000000000000000000FF000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000F3FFCFFFFFFFFFFFE1FF87CFFFFFFFFF
      C0FF8387FFFFEFFD807F8103E7FFC7FF003FC003C3FFC3FB083FE007C1FFE3F7
      1C1FF00FC0FFF1E7BE0FF81FC47FF8CFFF07F81FC63FFC1FFF83F01FC71FFE3F
      FFC1E00FEF8FFC1FFFE0C107FFC7F8CFFFF0C383FFE3E1E7FFF8E7C3FFF3C3F3
      FFFDFFE3FFFFC7FDFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object dseQry1: TmmADODataSet
    Connection = dmStyle.conDefault
    Parameters = <>
    Left = 465
    Top = 400
  end
  object qryAssortment: TmmADODataSet
    Connection = dmStyle.conDefault
    CommandText = 
      'SELECT c_assortment_id, c_assortment_name FROM t_assortment WHER' +
      'E c_assortment_state = 1 /* active */ ORDER BY c_assortment_name'
    Parameters = <>
    Left = 564
    Top = 402
  end
end
