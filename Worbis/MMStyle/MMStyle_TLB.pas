unit MMStyle_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 18.04.2001 08:41:28 from Type Library described below.

// ************************************************************************ //
// Type Lib: \\Wetsrvbde\BDE_Dev\Worbis\programme\Style\MMStyle.tlb (1)
// IID\LCID: {F236A202-33C9-11D5-8494-0050DA46B864}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MMStyleMajorVersion = 1;
  MMStyleMinorVersion = 0;

  LIBID_MMStyle: TGUID = '{F236A202-33C9-11D5-8494-0050DA46B864}';

  IID_IMMStyleServer: TGUID = '{F236A203-33C9-11D5-8494-0050DA46B864}';
  CLASS_MMStyleServer: TGUID = '{F236A205-33C9-11D5-8494-0050DA46B864}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IMMStyleServer = interface;
  IMMStyleServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MMStyleServer = IMMStyleServer;


// *********************************************************************//
// Interface: IMMStyleServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F236A203-33C9-11D5-8494-0050DA46B864}
// *********************************************************************//
  IMMStyleServer = interface(IDispatch)
    ['{F236A203-33C9-11D5-8494-0050DA46B864}']
  end;

// *********************************************************************//
// DispIntf:  IMMStyleServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {F236A203-33C9-11D5-8494-0050DA46B864}
// *********************************************************************//
  IMMStyleServerDisp = dispinterface
    ['{F236A203-33C9-11D5-8494-0050DA46B864}']
  end;

// *********************************************************************//
// The Class CoMMStyleServer provides a Create and CreateRemote method to          
// create instances of the default interface IMMStyleServer exposed by              
// the CoClass MMStyleServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoMMStyleServer = class
    class function Create: IMMStyleServer;
    class function CreateRemote(const MachineName: string): IMMStyleServer;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoMMStyleServer.Create: IMMStyleServer;
begin
  Result := CreateComObject(CLASS_MMStyleServer) as IMMStyleServer;
end;

class function CoMMStyleServer.CreateRemote(const MachineName: string): IMMStyleServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_MMStyleServer) as IMMStyleServer;
end;

end.
