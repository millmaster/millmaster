inherited frmEditUserFields: TfrmEditUserFields
  Left = 413
  Top = 144
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '(*)Benutzerfelder bearbeiten'
  ClientHeight = 278
  ClientWidth = 447
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object paButtons: TmmPanel
    Left = 0
    Top = 245
    Width = 447
    Height = 33
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOK: TmmButton
      Left = 206
      Top = 5
      Width = 75
      Height = 23
      Action = acOK
      Caption = '(9)OK'
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 286
      Top = 5
      Width = 75
      Height = 23
      Action = acCancel
      Caption = '(9)Abbrechen'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton1: TmmButton
      Left = 366
      Top = 5
      Width = 75
      Height = 23
      Action = acHelp
      Caption = '(9)&Hilfe...'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 447
    Height = 245
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object mmSpeedButton3: TmmSpeedButton
      Left = 199
      Top = 97
      Width = 24
      Height = 24
      Action = acInsert
      Glyph.Data = {
        92000000424D9200000000000000760000002800000007000000070000000100
        0400000000001C00000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888808800
        88808800088088000080880008808800888088088880}
      ParentShowHint = False
      ShowHint = True
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmSpeedButton4: TmmSpeedButton
      Left = 199
      Top = 124
      Width = 24
      Height = 24
      Action = acDelete
      Glyph.Data = {
        92000000424D9200000000000000760000002800000007000000070000000100
        0400000000001C00000000000000000000001000000010000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
        08808800088080000880880008808880088088880880}
      ParentShowHint = False
      ShowHint = True
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmGroupBox1: TmmGroupBox
      Left = 230
      Top = 5
      Width = 211
      Height = 235
      Caption = '(*)Benutzerfelder'
      TabOrder = 1
      CaptionSpace = True
      object mmSpeedButton1: TmmSpeedButton
        Left = 181
        Top = 92
        Width = 24
        Height = 23
        Action = acMoveUp
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808888
          88800000000080000080880008808880888088888880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton2: TmmSpeedButton
        Left = 181
        Top = 119
        Width = 24
        Height = 23
        Action = acMoveDown
        Glyph.Data = {
          92000000424D9200000000000000760000002800000007000000070000000100
          0400000000001C00000000000000000000001000000010000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808880
          88808800088080000080000000008888888088888880}
        ParentShowHint = False
        ShowHint = True
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbUserFields: TNWListBox
        Left = 10
        Top = 20
        Width = 167
        Height = 175
        DragMode = dmAutomatic
        ItemHeight = 13
        TabOrder = 0
        OnDblClick = acRenameExecute
        OnDragDrop = ItemDragDrop
        OnDragOver = lbUserFieldsDragOver
      end
      object mmButton2: TmmButton
        Left = 10
        Top = 203
        Width = 110
        Height = 23
        Action = acRename
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmGroupBox2: TmmGroupBox
      Left = 5
      Top = 5
      Width = 188
      Height = 235
      Caption = '(*)Vorhandene Felder'
      TabOrder = 0
      CaptionSpace = True
      object lbAvailable: TNWListBox
        Left = 10
        Top = 20
        Width = 167
        Height = 205
        DragMode = dmAutomatic
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        OnDblClick = acInsertExecute
        OnDragDrop = ItemDragDrop
        OnDragOver = lbAvailableDragOver
      end
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 352
    Top = 40
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object mmActionList1: TmmActionList
    Left = 384
    Top = 40
    object acOK: TAction
      Caption = '(10)&Uebernehmen'
      Hint = '(*)Eingaben uebernehmen'
      ShortCut = 16397
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(10)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(10)Hilfe...'
      Hint = '(*)Hilfetext anzeigen'
      OnExecute = acHelpExecute
    end
    object acInsert: TAction
      Hint = '(*)Markiertes Feld hinzufuegen'
      OnExecute = acInsertExecute
    end
    object acDelete: TAction
      Hint = '(*)Markiertes Feld entfernen'
      OnExecute = acDeleteExecute
    end
    object acMoveUp: TAction
      Hint = '(*)Markiertes Feld nach oben'
      OnExecute = acMoveUpExecute
    end
    object acMoveDown: TAction
      Hint = '(*)Markiertes Feld nach unten'
      OnExecute = acMoveDownExecute
    end
    object acRename: TAction
      Caption = '(15)Umbenennen...'
      Hint = '(*)Feldname umbenennen'
      OnExecute = acRenameExecute
    end
  end
end
