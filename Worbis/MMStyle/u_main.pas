(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_MAIN.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.2000 0.00  PW  | Datei erstellt
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 11.09.2001       Wss | AfterLanguageChange index for help file set to MMHtmlHelp
| 10.10.2002       LOK | Umbau ADO
| 22.11.2002       Wss | Nach Login refresh von SecurityControl
|=========================================================================================*)

{$i symbols.inc}

unit u_main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, Menus, mmMainMenu, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, IvMulti, IvEMulti, mmTranslator, mmLogin, mmPopupMenu,
  mmOpenDialog, IvMlDlgs, mmSaveDialog, mmPrintDialog,
  mmPrinterSetupDialog, StdActns,
  ActnList, mmActionList, ImgList, mmImageList, ComCtrls, mmStatusBar,
  ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, MMSecurity,
  mmAboutDlg, MMHtmlHelp;

type
  TfrmMain = class(TBaseApplMainForm)
    acEditStyle: TAction;
    miEditStyle: TMenuItem;
    MMSecurityDB: TMMSecurityDB;
    MMSecurityControl: TMMSecurityControl;
    acSecurityConfig: TAction;
    Zugriffkontrolle1: TMenuItem;
    acViewStyleList: TAction;
    miViewStyleList: TMenuItem;
    acExport: TAction;
    acImport: TAction;
    acEditAssortment: TAction;
    miEditAssortment: TMenuItem;
    MMHtmlHelp: TMMHtmlHelp;

    procedure FormCreate(Sender: TObject);
    procedure acEditStyleExecute(Sender: TObject);
    procedure acSecurityConfigExecute(Sender: TObject);
    procedure acViewStyleListExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure acEditAssortmentExecute(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
    procedure acLogin1Execute(Sender: TObject);
  private
  public
    procedure DisplayHint(Sender: TObject); override; 
    procedure InitStandalone; override;
  end; //TfrmMain

var
  frmMain: TfrmMain;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, 

  BaseForm, LoepfeGlobal, u_lib,
  u_common_lib, u_common_global, u_global, u_edit_style, u_view_stylelist,
  u_export_style, u_import_style, u_edit_assortment;

{$R *.DFM}

resourcestring
  cConnectError = '(45)Keine Verbindung zum Datenbank-Server: %s'; //ivlm

procedure TfrmMain.InitStandalone;
begin
  inherited InitStandalone;

  Application.HintPause := 0;
  Application.Helpfile := cHelpfilename;

  //Neu, Oeffnen und Separatoren im Menu auf unsichtbar stellen
  acNew.Visible := false;
  acOpen.Visible := false;
  acSave.Visible := false;
  acSaveas.Visible := false;
  acPreView.Visible := false;
  acPrinterSetup.Visible := false;
  acPrint.Visible := false;
  N11.Visible := false;
  N21.Visible := false;
  N31.Visible := false;
  N41.Visible := false;
end; //procedure TfrmMain.InitStandalone
//-----------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
 xAliasName,
 xUserName,
 xPassword  : string;

begin
  HelpContext := GetHelpContext('Allgemein\MM_LabMaster.htm');
  // if parameters overrides the default settings
  xAliasName := GetParamStr('/a');
  if xAliasName <> '' then gDBAliasNameODBC := xAliasName;

  xUsername := GetParamStr('/u');
  if xUsername <> '' then gDBUsername := xUsername;

  xPassword := GetParamStr('/p');
  if xPassword <> '' then gDBPassword := xPassword;

  // call inherited
  inherited;
  Application.OnHint := DisplayHint;

  gTempPath := SetTempPath;

//  gStyleSettings := TStyleSettings.Create;
  ReadStyleSettings;
end; //procedure TfrmMain.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmMain.acEditStyleExecute(Sender: TObject);
var
  xForm: TfrmEditStyle;
begin
  if GetCreateChildForm(TfrmEditStyle,TForm(xForm)) then begin
    xForm.Init(0);
    Height := xForm.Height + 105;
    Width := xForm.Width + 30;
  end;
end; //procedure TfrmMain.acEditStyleFormExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.DisplayHint(Sender: TObject);
begin
  mmStatusBar.SimpleText := GetLongHint(Application.Hint);
end; //procedure TfrmMain.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmMain.acSecurityConfigExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end; //procedure TfrmMain.acSecurityConfigExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acViewStyleListExecute(Sender: TObject);
var
  xForm: TfrmViewStyle;

begin
  if GetCreateChildForm(TfrmViewStyle,TForm(xForm)) then
    //xForm.Init('');
end; //procedure TfrmMain.acViewStyleListExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acExportExecute(Sender: TObject);
begin
  with TfrmExportStyle.Create(Application) do begin
    ShowModal;
//    Free;
  end; //with TfrmExportStyle.Create(Application) do begin
end; //procedure TfrmMain.acExportExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acImportExecute(Sender: TObject);
begin
  with TfrmImportStyle.Create(Application) do begin
    ShowModal;
//    Free;
  end; //with TfrmImportStyle.Create(Application) do begin
end; //procedure TfrmMain.acImportExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acEditAssortmentExecute(Sender: TObject);
begin
  with TfrmEditAssortment.Create(Application) do begin
    Init(0);
    ShowModal;
    Free;
  end; //with TfrmEditAssortment.Create(Application) do begin
end; //procedure TfrmMain.acEditAssortmentExecute
//------------------------------------------------------------------------------
procedure TfrmMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acLogin1Execute(Sender: TObject);
begin
  inherited;  // nicht l�schen, da sonst Login Dialog nicth erscheint
  MMSecurityControl.Refresh;
end;

end. //u_main.pas

