(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMStyleServerIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP5
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.07.2001 2.01  Wss | implementation changed to use WrapperForm 
|=========================================================================================*)
unit MMStyleServerIMPL;

interface

uses
  ComObj, ActiveX, LoepfePluginIMPL, LOEPFELIBRARY_TLB, MMStyle_TLB, StdVcl;

type
  TMMStyleServer = class(TLoepfePlugin, IMMStyleServer)
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R 'Toolbar.res'}

uses
  mmMBCS,

  ComServ, Forms, BaseForm,
  u_edit_style, u_view_stylelist, u_edit_assortment, u_import_style, u_export_style;

type
  TCommandID = (ciEditStyle, ciViewStyleList, ciEditAssortment, ciImportStyle, ciExportStyle);
const
  cFloorMenuDef: Array[0..3] of TFloorMenuRec = (
    // --- Main Menu File ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Artikel'; MenuHint: '(*)Artikel'; MenuStBar:'(*)Artikel'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciEditStyle); SubMenu: 0; MenuBmpName: 'STYLEEDIT';
     MenuText: '(*)Artikel bearbeiten'; MenuHint:'(*)Artikel bearbeiten'; MenuStBar: '(*)Artikel bearbeiten'; //ivlm
     ToolbarText: 'StyleEdit';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciViewStyleList); SubMenu: 0; MenuBmpName: 'STYLELIST';
     MenuText: '(*)Artikelliste'; MenuHint:'(*)Artikelliste'; MenuStBar: '(*)Artikelliste'; //ivlm
     ToolbarText: 'StyleList';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciEditAssortment); SubMenu: 0; MenuBmpName: '';
     MenuText: '(*)Sortiment bearbeiten/hinzufuegen'; MenuHint:'(*)Sortiment bearbeiten/hinzufuegen'; MenuStBar: '(*)Sortiment bearbeiten/hinzufuegen'; //ivlm
     ToolbarText: 'AssortmentEdit';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)

{
    (CommandID: 0; SubMenu: 0; MenuBmpName: '';
     MenuText: '-'; MenuHint:''; MenuStBar: '';
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciImportStyle); SubMenu: 0; MenuBmpName: '';
     MenuText: '(*)Importieren...'; MenuHint:'(*)Artikelstamm importieren'; MenuStBar: '(*)Artikelstamm importieren'; //ivlm
     ToolbarText: 'StyleImport';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll),

    (CommandID: Ord(ciExportStyle); SubMenu: 0; MenuBmpName: '';
     MenuText: '(*)Exportieren...'; MenuHint:'(*)Artikelstamm exportieren'; MenuStBar: '(*)Artikelstamm exportieren'; //ivlm
     ToolbarText: 'StyleExport';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled; MenuAvailable: maAll)
{}
  );

//------------------------------------------------------------------------------
// TMMStyleServer
//------------------------------------------------------------------------------
function TMMStyleServer.ExecuteCommand: SYSINT;
var
  xForm: TmmForm;
  xWrap: TmmForm;
begin
  Result := 0;
  case TCommandID(CommandIndex) of
    ciEditAssortment: begin
        if GetWrappedDataForm(TfrmEditAssortment, TForm(xForm), xWrap) then begin
          Result := xWrap.Handle;
          TFrmEditAssortment(xForm).Init(0);
          xForm.Show;
        end;
      end;

    ciEditStyle: begin
        if GetWrappedDataForm(TfrmEditStyle, TForm(xForm), xWrap) then begin
          Result := xWrap.Handle;
        end;
      end;

    ciViewStyleList: begin
        if GetWrappedDataForm(TfrmViewStyle, TForm(xForm), xWrap) then begin
          Result := xWrap.Handle;
        end;
      end;

    ciImportStyle: begin
        if GetWrappedDataForm(TfrmImportStyle, TForm(xForm), xWrap) then begin
          Result := xWrap.Handle;
          xForm.Show;
        end;
      end;

    ciExportStyle: begin
        if GetWrappedDataForm(TfrmExportStyle, TForm(xForm), xWrap) then begin
          Result := xWrap.Handle;
          xForm.Show;
        end;
      end;
  else
  end;
end;
{
function TMMStyleServer.ExecuteCommand: SYSINT;
var
  xForm: TmmForm;
begin
  Result := 0;
  case TCommandID(CommandIndex) of
    ciEditAssortment: begin
        if GetCreateChildForm(TfrmEditAssortment, TForm(xForm)) then begin
          TFrmEditAssortment(xForm).Init(0);
          xForm.Show;
          Result := xForm.Handle;
        end;
      end;

    ciEditStyle: begin
        if GetCreateChildForm(TfrmEditStyle, TForm(xForm)) then begin
          Result := xForm.Handle;
        end;
      end;

    ciViewStyleList: begin
        if GetCreateChildForm(TfrmViewStyle, TForm(xForm)) then begin
          Result := xForm.Handle;
        end;
      end;

    ciImportStyle: begin
        if GetCreateChildForm(TfrmImportStyle, TForm(xForm)) then begin
          xForm.Show;
          Result := xForm.Handle;
        end;
      end;

    ciExportStyle: begin
        if GetCreateChildForm(TfrmExportStyle, TForm(xForm)) then begin
          xForm.Show;
          Result := xForm.Handle;
        end;
      end;
  else
  end;
end;
{}
//------------------------------------------------------------------------------
procedure TMMStyleServer.Initialize;
begin
  inherited Initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------
initialization
  InitializeObjectFactory(TMMStyleServer, CLASS_MMStyleServer, False);
end.
