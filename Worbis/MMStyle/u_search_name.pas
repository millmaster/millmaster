(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_search_name.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Get values for style search
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.11.00 | 0.00 | PW  | Datei erstellt
|          |      |     |
|=========================================================================================*)

{$i symbols.inc}

unit u_search_name;

interface

uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, mmCheckBox, ActnList, mmActionList, mmEdit,
  mmButton, mmLabel, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  TfrmSearchByName = class(TmmDialog)
    mmTranslator1: TmmTranslator;
    laFieldName: TmmLabel;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    edSearchName: TmmEdit;
    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    cbExactSearch: TmmCheckBox;

    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  public
    procedure Init(aSearch: rStyleSearchT; aFieldName: string);
    function GetSearchValues: rStyleSearchT;
  end; //TfrmSearchByName

var
  frmSearchByName: TfrmSearchByName;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TfrmSearchByName.acOKExecute(Sender: TObject);
begin
  inherited;
  Close;
  ModalResult := mrOK;
end; //procedure TfrmSearchByName.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmSearchByName.acCancelExecute(Sender: TObject);
begin
  inherited;
  Close;
  ModalResult := mrCancel;
end; //procedure TfrmSearchByName.acCancelExecute
//-----------------------------------------------------------------------------
function TfrmSearchByName.GetSearchValues: rStyleSearchT;
begin
  result.SearchName := trim(edSearchName.Text);
  result.ExactSearch := cbExactSearch.Checked;
end; //function TfrmSearchByName.GetSearchValues
//-----------------------------------------------------------------------------
procedure TfrmSearchByName.Init(aSearch: rStyleSearchT; aFieldName: string);
begin
  edSearchName.Text := trim(aSearch.SearchName);
  cbExactSearch.Checked := aSearch.ExactSearch;
  laFieldname.Caption := aFieldName;
end; //constructor TfrmSearchByName.Create
//-----------------------------------------------------------------------------
procedure TfrmSearchByName.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle,WM_NEXTDLGCTL,0,0);
  end; //if Key = #13 then begin
end; //procedure TfrmSearchByName.FormKeyPress

end. //u_search_name
