(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_style.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Style Edit Form
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 |      | PW  | - column "c_opt_speed" removed
|          |      |     | - using state bitmaps instead of text (yes/no)
|          |      |     | - new bitmaps for tmmspeedbuttons
|          |      |     | - enable/disable editing ut-factors
| 10.04.01 |      | PW  | TDBValComboBox replaced by TmmDBValComboBox
|          |      |     | edit controls with state
| 17.04.01 |      | Wss | - acEditYMSettingsExecute: call to RefreshTemplateList framed
                            with try..except. Exception, if template name does not exist in template list.
                          - OnStyleStateChange: buttons added to set Enabled state (buttons renamed)
| 14.05.2001       Wss | gbUTFactors visible set from TMMSettingsReader
| 15.05.2001       Wss | - call to LTrans remarked because resourcestrings hasn't to be translated manually
                         - correction factor for imperfections isn't specialized for UT4 anymore
| 17.05.2001       Wss | In RunQuery the style id from tabStyle is taken (instead of qrySelect)
| 21.06.01         pw  | MxN t_style_settings implemented
                         - c_ym_set_id removed from table t_style
| 25.06.2001       Wss | Icon for Userfields and Clearersettingslist changed
| 10.07.2001       Wss | - c_max_subID for AutoID in Assignments set to 0
                         - on new style lbYMSettings keept clear instead of set DefaultMemoryC
| 16.07.2001       Wss | - use predefined color array for color button from MMUGlobal
| 14.08.2001       Nue | c_head_class inserted.
| 22.08.2001       Wss | call to template form parameter changed to use SetID instead of SetName
| 03.09.2001       Nue | Update_lbYMSettings(nil) after edit YMSettings
| 16.11.2001       Wss | - Twist datatype changed to float
                         - Size of StyleName changed to 50 (value from DB)
| 21.11.2001       Wss | YarnCount, Meter/Weight calculation handling adapted
| 05.12.2001       Wss | YarnUnit added on right side of edYarnCounts
| 18.01.2002       Wss | - SetUTFactorState not used anymore -> Visibility through group box
                         - Prevent deleting of DefaultStyle because it is necessary for
                           MillMaster System basic functionality
| 29.01.2002       Wss | Remove Delete button in navStyle if style has historical data. In this
                         case the user has to set Inactive this style
| 28.02.2002       Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
| 10.10.2002       LOK | Umbau ADO
| 28.10.2002       Wss | - Diverse Bugfixes
                         - Link zu t_dw_style_shift entfernt, da diese Tabelle gel�scht wurde
| 21.11.2002       LOK | Wenn noch kein Template definiert ist, dann kann mit dem SpedButton 'bYMSettings'
                       | direkt ein neues Template erzeugt werden
| 22.11.2002       Wss | Refresh Button entfernt
| 10.07.2003       Wss | Schlupf immer mindestens auf 1.0
| 16.12.2003       Nue | Einbau Templateverwaltung
| 18.02.2004       Wss | - Anpassungen an �bersetzungen
| 21.06.2004       SDO | Neuer Drucker-DLG
| 18.01.2005  2.00 SDo | Umbau & Anpassungen an XML Vers. 5
| 07.07.2006       Wss | Wenn Record im Edit Modus ist, wird beim Schliessen des Fensters
                         nachgefragt (Yes, No, Cancel) -> CanClose()
|=========================================================================================*)

{$I symbols.inc}

unit u_edit_style;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, ActnList, ToolWin, ComCtrls, IvDictio, IvMulti,
  IvEMulti, mmTranslator, Menus, mmPopupMenu, ExtCtrls, DBCtrls, mmToolBar,
  mmActionList, mmStatusBar, mmPanel, Db, mmDataSource,
  StdCtrls, mmGroupBox, mmDBNavigator, Mask, mmDBEdit, Buttons,
  mmSpeedButton, mmDBLookupComboBox, mmLabel, NumCtrl,
  mmComboBox, wwdbedit, Wwdbcomb, IvMlDlgs,
  mmColorDialog, mmDBText, mmColorButton, MMSecurity, Wwdatsrc,
  mmBevel, Grids, Wwdbigrd, Wwdbgrid, mmGraphGlobal,
  TemplateForm, ImgList, Printers, mmDbValCb, pwDBEdit, mmListBox, ADODB,
  mmADODataSet, u_dmStyle, mmADOCommand, Wwkeycb, mmADOTable;

type
  TfrmEditStyle = class(TmmForm)
    mmActionList1: TmmActionList;
    acClose: TAction;
    acApplyFilter: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acCopyStyle: TAction;
    acEditAssortment: TAction;
    acEditYMSettings: TAction;
    acUserFields: TAction;
    acEditCopsLength: TAction;
    acEditConeLength: TAction;
    acDeleteStyle: TAction;
    acSecurity: TAction;
    acEditStyle: TAction;
    acCancelFilter: TAction;
    acSortStyle: TAction;
    acEditImpFactors: TAction;
    tabStyle: TmmADODataSet;
    dsStyle: TmmDataSource;
    tabStylec_style_id: TSmallintField;
    tabStylec_assortment_id: TSmallintField;
    tabStylec_style_name: TStringField;
    tabStylec_nr_of_threads: TSmallintField;
    tabStylec_twist_left: TBooleanField;
    tabStylec_mm_per_bob: TIntegerField;
    tabStylec_package_type: TSmallintField;
    tabStylec_mm_per_cone: TIntegerField;
    tabStylec_m_per_spdhour: TIntegerField;
    tabStylec_adjust: TSmallintField;
    tabStylec_color: TIntegerField;
    tabStylec_num1: TFloatField;
    tabStylec_num2: TFloatField;
    tabStylec_num3: TFloatField;
    tabStylec_string1: TStringField;                                   
    tabStylec_string2: TStringField;
    tabStylec_string3: TStringField;
    tabStylec_string4: TStringField;
    tabStylec_string5: TStringField;
    tabStylec_string6: TStringField;
    tabStylec_string7: TStringField;
    tabStylec_style_state: TSmallintField;
    tabStylecalc_copsgewicht: TFloatField;
    tabStylecalc_konengewicht: TFloatField;
    tabStylecalc_HistoricalData: TBooleanField;
    tabStylecalc_CurrentProdGroup: TBooleanField;
    tabStylecalc_ProdPeriod: TStringField;
    tabStylec_Factor_INeps: TFloatField;
    tabStylec_Factor_IThick: TFloatField;
    tabStylec_Factor_IThin: TFloatField;
    tabStylec_Factor_ISmall: TFloatField;
    tabStylec_Factor_SFI: TFloatField;
    dsAssortment: TmmDataSource;
    qrySelect: TmmADODataSet;
    dsSelect: TwwDataSource;
    qrySelectc_style_id: TSmallintField;
    qrySelectc_style_name: TStringField;
    qrySelectc_style_state: TSmallintField;

    MMSecurityControl1: TMMSecurityControl;
    paStyleList: TmmPanel;
    mmToolBar2: TmmToolBar;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    ToolButton16: TToolButton;
    ToolButton17: TToolButton;
    ToolButton15: TToolButton;
    navSelect: TmmDBNavigator;
    mmPanel2: TmmPanel;
    sbRecordCount: TmmStatusBar;
    paStyleEdit: TmmPanel;
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    laStylename: TmmLabel;
    laAssortment: TmmLabel;
    bEditAssortment: TmmSpeedButton;
    edc_style_name: TmmDBEdit;
    comAssortment: TmmDBLookupComboBox;
    mmGroupBox2: TmmGroupBox;
    laYMSettings: TmmLabel;
    bYMSettings: TmmSpeedButton;
    mmGroupBox3: TmmGroupBox;
    laYarncnt: TmmLabel;
    laThreads: TmmLabel;
    laColor: TmmLabel;
    laTwist: TmmLabel;
    mmLabel8: TmmLabel;
    edYarnCnt: TpwDBEdit;
    comFachung: TmmDBValComboBox;
    edc_twist: TmmDBEdit;
    comDrehrichtung: TmmDBValComboBox;
    colbColor: TmmColorButton;
    mmGroupBox6: TmmGroupBox;
    laCopsgewicht: TmmLabel;
    laKonengewicht: TmmLabel;
    mmLabel14: TmmLabel;
    bKopsGewicht: TmmSpeedButton;
    bKonenGewicht: TmmSpeedButton;
    comPackageType: TmmDBValComboBox;
    edcopsgewicht: TpwDBEdit;
    edkonengewicht: TpwDBEdit;
    mmGroupBox4: TmmGroupBox;
    laState: TmmLabel;
    mmLabel16: TmmLabel;
    mmLabel17: TmmLabel;
    comStyleState: TmmDBValComboBox;
    gbUserFields: TmmGroupBox;
    laUserField1: TmmLabel;
    laUserField2: TmmLabel;
    laUserField3: TmmLabel;
    laUserField4: TmmLabel;
    laUserField5: TmmLabel;
    laUserField6: TmmLabel;
    laUserField7: TmmLabel;
    laUserField8: TmmLabel;
    laUserField9: TmmLabel;
    laUserField10: TmmLabel;
    edUserField1: TmmDBEdit;
    edUserField2: TmmDBEdit;
    edUserField3: TmmDBEdit;
    edUserField4: TmmDBEdit;
    edUserField5: TmmDBEdit;
    edUserField6: TmmDBEdit;
    edUserField7: TmmDBEdit;
    edUserField8: TmmDBEdit;
    edUserField9: TmmDBEdit;
    edUserField10: TmmDBEdit;
    mmStatusBar1: TmmStatusBar;
    mmToolBar1: TmmToolBar;
    ToolButton11: TToolButton;
    ToolButton9: TToolButton;
    ToolButton5: TToolButton;
    ToolButton12: TToolButton;
    ToolButton10: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    navStyle: TmmDBNavigator;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    mmLabel2: TmmLabel;
    mmDBText3: TmmDBText;
    gbStyleList: TmmGroupBox;
    mmLabel1: TmmLabel;
    isSearch: TwwIncrementalSearch;
    grSelect: TwwDBGrid;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    gbImpFactors: TmmGroupBox;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel5: TmmLabel;
    mmLabel6: TmmLabel;
    mmLabel7: TmmLabel;
    edc_Factor_INeps: TmmDBEdit;
    edc_Factor_IThick: TmmDBEdit;
    edc_Factor_IThin: TmmDBEdit;
    edc_Factor_ISmall: TmmDBEdit;
    edc_Factor_SFI: TmmDBEdit;
    ilBitmaps: TImageList;
    imgHistorical: TImage;
    imgCurrent: TImage;
    ToolButton3: TToolButton;
    tabStylec_slip: TSmallintField;
    mmLabel9: TmmLabel;
    tabStylecalc_slip: TFloatField;
    edcalc_slip: TpwDBEdit;
    lbYMSettings: TmmListBox;
    mmSpeedButton1: TmmSpeedButton;
    acSelectYMSettings: TAction;
    mmTranslator: TmmTranslator;
    tabStylec_max_subID: TIntegerField;
    tabStylec_twist: TFloatField;
    acStyleChanger: TAction;
    ToolButton18: TToolButton;
    laYarnUnit: TmmLabel;
    tabStylec_yarncnt: TFloatField;
    tabStylecalc_YarnCnt: TFloatField;
    dseQry1: TmmADODataSet;
    qryAssortment: TmmADODataSet;
    tbTemplate: TToolButton;
    acTemplate: TAction;

    procedure acCloseExecute(Sender: TObject);
    procedure acApplyFilterExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acCopyStyleExecute(Sender: TObject);
    procedure acEditAssortmentExecute(Sender: TObject);
    procedure tabStyleBeforePost(DataSet: TDataSet);
    procedure acEditYMSettingsExecute(Sender: TObject);
    procedure tabStyleCalcFields(DataSet: TDataSet);
    procedure edYarnCntKeyPress(Sender: TObject; var Key: Char);
    procedure tabStyleNewRecord(DataSet: TDataSet);
    procedure tabStyleAfterScroll(DataSet: TDataSet);
    procedure edcopsgewichtKeyPress(Sender: TObject; var Key: Char);
    procedure edkonengewichtKeyPress(Sender: TObject; var Key: Char);
    procedure acUserFieldsExecute(Sender: TObject);
    procedure tabStyleBeforeDelete(DataSet: TDataSet);
    procedure acEditCopsLengthExecute(Sender: TObject);
    procedure acEditConeLengthExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure colbColorChangeColor(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure tabStyleAfterRefresh(DataSet: TDataSet);
    procedure acSortStyleExecute(Sender: TObject);
    procedure acCancelFilterExecute(Sender: TObject);
    procedure qrySelectBeforeScroll(DataSet: TDataSet);
    procedure qrySelectAfterScroll(DataSet: TDataSet);
    procedure tabStyleAfterPost(DataSet: TDataSet);
    procedure dsStyleStateChange(Sender: TObject);
    procedure grSelectCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure tabStyleAfterDelete(DataSet: TDataSet);
    procedure isSearchAfterSearch(Sender: TwwIncrementalSearch;
      MatchFound: Boolean);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure tabStyleAfterInsert(DataSet: TDataSet);
    procedure grSelectDblClick(Sender: TObject);
    procedure acSelectYMSettingsExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acStyleChangerExecute(Sender: TObject);
    procedure lbYMSettingsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure FormShow(Sender: TObject);
    procedure acTemplateExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    mSFilter: rStyleFilterT;
    mSortOrder: eSortOrderT;
    mSortField: string;
    mFilterActive: boolean;
    mStyleUF: rUserFieldsT;
    mAskForSaving: boolean;
    procedure EditYarnLength(Sender: TObject);
    procedure SetRequiredFieldColor;
    procedure InitUserFields;
    procedure RunQuery;
    procedure UpdateRecordCountPanel;
    procedure ShowHideUserFields;
    procedure Read_YMSettings;
    procedure Write_YMSettings;
    procedure Update_lbYMSettings(aInt: aIntegerT);
    function GetYMSettingsIDs: aIntegerT;
  protected
  public
    constructor Create(aOwner: TComponent); override;
    procedure Init(aStyleID: integer);
    procedure DisplayHint(Sender: TObject);
  end; //TfrmEditStyle

var
  frmEditStyle: TfrmEditStyle;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, MMHtmlHelp,
  MMUGlobal, mmDialogs,
  u_main, u_common_lib, u_lib, u_style_filter, u_edit_userfields, u_edit_assortment,
  u_resourcestrings, u_print_style_form, math, u_select_ym_settings,
  YMParaDef, StyleChangerForm, //Nue:14:8.01
  XMLDef, RzCSIntf, BaseGlobal;

{$R *.DFM}

const
  cDefaultStyleColor = clBlue; // clWhite;

resourcestring
//  cMsg1  = '(*)Artikelname muss eindeutig sein.' +#10#13 +'"%s" ist schon vorhanden.';  //
  cMsg2 = '(*)Unbekannter Fehler. Reinigereinstellungen wurden nicht gesichert!'; //ivlm
  cMsg4 = '(*)Ein Artikel mit einer aktuellen Produktionsgruppe kann nicht geloescht werden.'; //ivlm
  cMsg5 = '(*)Es muss mindestens ein Artikel in der Datenbank vorhanden sein. Artikel kann nicht geloescht werden.'; //ivlm
  cMsg6 = '(*)Dieser Artikel kann nicht geloescht werden.'; //ivlm
//  cMsg7  = '(*)Artikel mit allen historischen Daten loeschen?'; //
  cMsg8 = '(*)Der Artikel %s wird jetzt geloescht. Weitermachen?'; //ivlm
  cMsg9 = '(*)Fuer das Feld %s muss ein Wert eingegeben werden!'; //ivlm
  cMsg10 = '(*)Mindestens eine Reinigereinstellung muss vorhanden sein!'; //ivlm
  rsMsg11 = '(*)Das Element "%s" wurde veraendert. Wollen Sie speichern?'; //ivlm

constructor TfrmEditStyle.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Artikelstamm_bearbeiten.htm');
  // use predefined color array
  colbColor.ColorArray := cMMProdGroupColors;
  InitUserFields;
  gbImpFactors.Visible := gImpFactorVisible;

  mSortField := 'c_style_name';
  mSortOrder := soAscending;
  mAskForSaving := True;

  mSFilter := InitFilter(0);

  laYarnUnit.Caption := cYarnUnitsStr[gCountUnit];

//  tabStyle.ReadOnly := not acEditStyle.Enabled;
//  acCopyStyle.Enabled := acCopyStyle.Enabled and acEditStyle.Enabled;
//  acEditCopsLength.Enabled := acEditCopsLength.Enabled and acEditStyle.Enabled;
//  acEditConeLength.Enabled := acEditConeLength.Enabled and acEditStyle.Enabled;
//  // if not allowed to edit style remove associated buttons
//  if not acEditStyle.Enabled then
//    navStyle.VisibleButtons := navStyle.VisibleButtons - [nbInsert, nbDelete, nbEdit, nbPost, nbCancel];

  GetNavigatorWidth(navStyle);

  qryAssortment.Open;
  tabStyle.Open;

  RunQuery;
{$IFDEF ShowOnlyActive}
  acCancelFilter.Enabled := True;
{$ELSE}
  acCancelFilter.Enabled := False;
{$ENDIF}
  SetRequiredFieldColor;
end; //constructor TfrmEditStyle.Create
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.Init(aStyleID: integer);
begin
  if aStyleID > 0 then
    qrySelect.Locate('c_style_id', aStyleID, []);
end; //procedure TfrmEditStyle.Init
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TfrmEditStyle.acCloseExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acApplyFilterExecute(Sender: TObject);
begin
  if not PostRecord(tabStyle) then exit;

  with TfrmStyleFilter.Create(Self) do
  try
    Init(1, mSFilter);
    if ShowModal = mrOK then begin
      mSFilter := GetFilterValues;
      RunQuery;
    end;
  finally
    Free;
  end; //with TfrmSearchByName.Create(Self) do begin
end; //procedure TfrmEditStyle.acApplyFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.DisplayHint(Sender: TObject);
begin
  mmStatusBar1.SimpleText := GetLongHint(Application.Hint);
end; //procedure TfrmEditStyle.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.FormActivate(Sender: TObject);
begin
  Application.OnHint := DisplayHint;
end; //procedure TfrmEditStyle.FormActivate
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.FormDestroy(Sender: TObject);
begin
  Application.OnHint := frmMain.DisplayHint;
end; //procedure TfrmEditStyle.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acPrintExecute(Sender: TObject);
var xBW : Boolean;
begin
  if not PostRecord(tabStyle) then exit;
  if not GetPrintOptions(xBW, poPortrait, Self) then exit;

  with TfrmPrintStyleForm.Create(Self, tabStylec_style_id.asinteger, xBW) do try
    Execute;
  finally
    Free;
  end; //try..finally
end; //procedure TfrmEditStyle.acPrintExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TfrmEditStyle.acHelpExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acCopyStyleExecute(Sender: TObject);
const
  cQry1 = 'SELECT * FROM t_style WHERE c_style_id = :c_style_id';
var
  i, xID: integer;
  xFieldName: string;
  xList: TStringList;
begin
  if not PostRecord(tabStyle) then exit;
  xID := tabStylec_style_id.asinteger;

  xList := TStringList.Create;
  try
    xList.AddStrings(lbYMSettings.Items);
    tabStyle.Insert;

    dseQry1.CommandText := cQry1;
    dseQry1.Parameters.ParamByName('c_style_id').value := xID;
    dseQry1.Open;

    for i := 0 to dseQry1.FieldCount - 1 do begin
      xFieldName := lowercase(dseQry1.Fields[i].FieldName);

      if (xFieldName <> 'c_style_id') and (xFieldName <> 'c_style_name') and
         (not dseQry1.Fields[i].IsNull) and (tabStyle.FindField(xFieldName) <> nil) then
      begin
        tabStyle.FieldByName(xFieldname).Value := dseQry1.FieldByName(xFieldname).Value;
      end;
    end; //for i := 0 to dseQry1.FieldCount -1 do begin
  finally
    dseQry1.Close;
    edc_style_name.SetFocus;
    tabStyleCalcFields(nil);
    tabStyleAfterScroll(nil);
    lbYMSettings.Items.Clear;
    lbYMSettings.Items.AddStrings(xList);
    xList.Free;
  end; //try..finally
end; //procedure TfrmEditStyle.acCopyStyleExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acEditAssortmentExecute(Sender: TObject);
begin
  with TfrmEditAssortment.Create(Self) do try
    Init(tabStylec_assortment_id.asinteger);
    ShowModal;
    if Changed then begin
      qryAssortment.Active := False;
      qryAssortment.Active := True;
    end;
  finally
    Free;
  end; //with TfrmEditAssortment.Create(Self) do begin
  FormActivate(Sender);
end; //procedure TfrmEditStyle.acEditAssortmentExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleBeforePost(DataSet: TDataSet);
var
  xID: integer;
  xInt: Integer;
begin
  // at new style set some default values
  if tabStyle.State = dsInsert then begin
    xID := MakeUniqueID(2, tabStyle.CommandText, 'c_style_id');
    if xID < 0 then
      SysUtils.Abort;
    tabStylec_style_id.Value := xID;
    tabStylec_max_subID.Value := 0;
  end; //if tabStylec_style_id.IsNull then begin

  if not DBFieldOk(tabStylec_style_name, laStylename.Caption, 0, 0, True) then
    SysUtils.Abort;

  tabStylec_style_name.value := trim(tabStylec_style_name.asstring);

  if tabStylec_assortment_id.IsNull then begin
    MMMessageDlg(Format(cMsg9, [laAssortment.caption]), mtError, [mbOK], 0, Self);
    SysUtils.Abort;
  end; //if tabStylec_assortment_id.IsNull then begin

  if not DBFieldOk(tabStylecalc_yarncnt, laYarncnt.Caption, 1, 1000, True) then SysUtils.Abort;
  if not DBFieldOk(tabStylecalc_copsgewicht, laCopsgewicht.Caption, 10, 999, False) then SysUtils.Abort;
  if not DBFieldOk(tabStylecalc_konengewicht, laKonengewicht.Caption, 10, 9999, False) then SysUtils.Abort;

  // convert user defined YarnCountUnit to general YarnCountUnit Nm
  tabStylec_yarncnt.value := YarnCountConvert(gCountUnit, yuNm, tabStylecalc_yarncnt.value);

  if not tabStylecalc_copsgewicht.IsNull then
    tabStylec_mm_per_bob.value := Round(WeightUnitToMeter(wug, yuNm, tabStylec_yarncnt.value, tabStylecalc_copsgewicht.value) * 1000);

  if not tabStylecalc_konengewicht.IsNull then
    tabStylec_mm_per_cone.value := Round(WeightUnitToMeter(wug, yuNm, tabStylec_yarncnt.value, tabStylecalc_konengewicht.AsFloat) * 1000);

  // Wss: new added field c_slip in table t_style (factor 1000 = int 1500 -> 1.500
  if (tabStylecalc_slip.Value >= cMinSlip) and (tabStylecalc_slip.Value <= cMaxSlip) then
    tabStylec_slip.Value := Round(tabStylecalc_slip.AsFloat * 1000.0)
  else
    tabStylec_slip.Value := 1000;

  if lbYMSettings.Items.Count = 0 then begin
    MMMessageDlg(Format(cMsg10, [laAssortment.caption]), mtError, [mbOK], 0, Self);
    SysUtils.Abort;
  end; //if tabStylec_assortment_id.IsNull then begin
end; //procedure TfrmEditStyle.tabStyleBeforePost
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acEditYMSettingsExecute(Sender: TObject);
var
  xID: Integer;
begin
  with TTemplate.Create(Self, dmStyle.conDefault) do begin
    mmTranslator.DictionaryName := Self.mmTranslator.DictionaryName;
    FormStyle := fsNormal;
    Visible := False;

//    Init(not acSelectYMSettings.Enabled);
    try
      if (Sender=TObject(acTemplate)) then
        Init(False) //Set Window TTemplate NOT to ReadOnly
      else
        Init(False, True); //Set Window TTemplate NOT to ReadOnly (only cobTemplate is disabled)
    //Init;
      xID := 0;
      if lbYMSettings.ItemIndex > -1 then
        xID := Integer(lbYMSettings.Items.Objects[lbYMSettings.ItemIndex]);
      RefreshTemplateList(xID);
    except
    end;
    mmTranslator.Translate;
    ShowModal;
    Free;
  end; //with TTemplate.Create(Self,frmMain.mDataBase) do begin

  // neccessary if c_ym_set_name or TK-Class can be changed
  Update_lbYMSettings(nil); //Nue:3.9.01
end; //procedure TfrmEditStyle.acEditYMSettingsExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleCalcFields(DataSet: TDataSet);
const
  cQry1 = 'WHERE c_style_id = %d';
var
  xHasHistoricalData,
  xIsCurrentProdGroup: boolean;
begin
  // convert YarnCountUnit to user defined in MMConfiguration
  tabStylecalc_yarncnt.value := YarnCountConvert(yuNm, gCountUnit, tabStylec_yarncnt.AsFloat);

  if tabStylec_mm_per_bob.IsNull then
    tabStylecalc_copsgewicht.clear
  else
    tabStylecalc_copsgewicht.value := MeterToWeightUnit(wug, yuNm, tabStylec_yarncnt.value, tabStylec_mm_per_bob.AsFloat / 1000);

  if tabStylec_mm_per_cone.IsNull then
    tabStylecalc_konengewicht.clear
  else
    tabStylecalc_konengewicht.value := MeterToWeightUnit(wug, yuNm, tabStylec_yarncnt.value, tabStylec_mm_per_cone.AsFloat / 1000);

  { TODO -oWss -cCheck : Was ist mit den historischen Daten in der Langzeittabelle? }
  xHasHistoricalData  := GetRecordCount('t_prodgroup', Format(cQry1, [tabStylec_style_id.asinteger])) > 0;
  xIsCurrentProdGroup := GetRecordCount('t_prodgroup_state', Format(cQry1, [tabStylec_style_id.asinteger])) > 0;


  tabStylecalc_HistoricalData.AsBoolean := xHasHistoricalData;
  tabStylecalc_CurrentProdGroup.AsBoolean := xIsCurrentProdGroup;

  // Bitmap f�r Historische Daten zeigen
  imgHistorical.Picture.Bitmap := nil;
  if xHasHistoricalData then begin
    ilBitmaps.GetBitmap(2, imgHistorical.Picture.Bitmap);
    // Wenn Artikel Daten hat, darf dieser nicht gel�scht werden -> Inaktiv verwenden
    navStyle.VisibleButtons := navStyle.VisibleButtons - [nbDelete];
  end else begin
    ilBitmaps.GetBitmap(3, imgHistorical.Picture.Bitmap);
    // Bei Freigabe durch Security Delete Funktion erm�glichen
    if acDeleteStyle.Enabled and acEditStyle.Enabled then
      navStyle.VisibleButtons := navStyle.VisibleButtons + [nbDelete];
  end;

  // Hat es laufende Produktionsgruppen unter diesem Artikel?
  imgCurrent.Picture.Bitmap := nil;
  if xIsCurrentProdGroup then
    ilBitmaps.GetBitmap(2, imgCurrent.Picture.Bitmap)
  else
    ilBitmaps.GetBitmap(3, imgCurrent.Picture.Bitmap);

  tabStylecalc_ProdPeriod.asstring := GetProductionPeriod(tabStylec_style_id.asinteger,
                                      tabStylecalc_HistoricalData.AsBoolean,
                                      tabStylecalc_CurrentProdGroup.AsBoolean);

  // Wss: new added field c_slip in table t_style (factor 1000 = int 1500 -> 1.500
  tabStylecalc_slip.Value := tabStylec_slip.Value / 1000.0;
  if (tabStylecalc_slip.Value < cMinSlip) or (tabStylecalc_slip.Value > cMaxSlip) then
    tabStylecalc_slip.Value := 1.0;
//    tabStylecalc_slip.AsString := '';
end; //procedure TfrmEditStyle.tabStyleCalcFields
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.edYarnCntKeyPress(Sender: TObject;
  var Key: Char);
begin
  if not TpwDBEdit(Sender).Enabled then begin
    key := #0;
    exit;
  end; //if not TpwDBEdit(Sender).Enabled then begin

  inherited;
  if tabStyle.State in [dsEdit, dsInsert] then tabStylec_yarncnt.Clear;
end; //procedure TfrmEditStyle.edyarncntKeyPress
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleNewRecord(DataSet: TDataSet);
begin
  tabStylec_color.Value := cDefaultStyleColor;
  tabStylec_style_state.Value := ord(ssActive);
  tabStylec_package_type.value := 1;
  tabStylec_nr_of_threads.value := 1;
end; //procedure TfrmEditStyle.tabStyleNewRecord
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleAfterScroll(DataSet: TDataSet);
begin
  colbColor.Color := tabStylec_color.Value;
  Read_YMSettings;
  ShowHideUserFields;
end; //procedure TfrmEditStyle.tabStyleAfterScroll
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.edcopsgewichtKeyPress(Sender: TObject; var Key: Char);
begin
  if tabStyle.State in [dsEdit, dsInsert] then
    tabStylec_mm_per_bob.Clear;
end; //procedure TfrmEditStyle.edcopsgewichtKeyPress
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.edkonengewichtKeyPress(Sender: TObject; var Key: Char);
begin
  if tabStyle.State in [dsEdit, dsInsert] then
    tabStylec_mm_per_cone.Clear;
end; //procedure TfrmEditStyle.edkonengewichtKeyPress
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acUserFieldsExecute(Sender: TObject);
var
  i: integer;
begin
  if not PostRecord(tabStyle) then
    exit;

  with TfrmEditUserFields.Create(Self) do
  try
    mmTranslator.DictionaryName := Self.mmTranslator.DictionaryName;
    Init(1);
    if ShowModal = mrOK then begin
      InitUserFields;
    end;
  finally
    Free;
  end; //with TfrmEditUserFields.Create(Self) do begin
end; //procedure TfrmEditStyle.acUserFieldsExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleBeforeDelete(DataSet: TDataSet);
begin
  // prevent deleting of DefaultStyle because necessary for WindingMaster basic functionality
  if tabStylec_style_id.Value = 1 then begin
    MessageBeep(0);
    MMMessageDlg(cMsg6, mtError, [mbOK], 0, Self);
    Abort;
  end;

  if tabStylecalc_CurrentProdGroup.Value then begin
    MessageBeep(0);
    MMMessageDlg(cMsg4, mtError, [mbOK], 0, Self);
    Abort;
  end; //if tabStylecalc_CurrentProdGroup.Value then begin

  if tabStyle.RecordCount < 1 then begin
    MessageBeep(0);
    MMMessageDlg(cMsg5, mtError, [mbOK], 0, Self);
    Abort;
  end; //if tabStyle.RecordCount < 1 then begin

  MessageBeep(0);
  if MMMessageDlg(Format(cMsg8, [tabStylec_style_name.asstring]), mtConfirmation, [mbYes, mbCancel], 0, Self) <> mrYes then
    Abort;
end; //procedure TfrmEditStyle.tabStyleBeforeDelete
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acEditCopsLengthExecute(Sender: TObject);
begin
  EditYarnLength(Sender);
  edcopsgewicht.SetFocus;
end; //procedure TfrmEditStyle.acEditCopsLengthExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acEditConeLengthExecute(Sender: TObject);
begin
  EditYarnLength(Sender);
  edkonengewicht.SetFocus;
end; //procedure TfrmEditStyle.acEditConeLengthExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.EditYarnLength(Sender: TObject);
var
  xValue: double;
  xField: TFloatField;

begin
  if Sender = acEditCopsLength then
    xField := tabStylecalc_copsgewicht
  else
    xField := tabStylecalc_konengewicht;

  xValue := xField.asfloat;
  if not SetYarnLength(xValue, tabStylecalc_yarncnt.value, tabStylec_nr_of_threads.value, gCountUnit, Self) then
    exit;
  if not (tabStyle.State in [dsEdit, dsInsert]) then
    exit;

  SetRecordEditMode(tabStyle);
  xField.value := xValue;
end; //procedure TfrmEditStyle.EditYarnLength
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle, WM_NEXTDLGCTL, 0, 0);
  end; //if Key = #13 then begin
end; //procedure TfrmEditStyle.FormKeyPress
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.colbColorChangeColor(Sender: TObject);
begin
  if tabStyle.ReadOnly or not (tabStyle.State in [dsEdit, dsInsert]) then
    colbColor.Color := tabStylec_color.AsInteger
  else
    tabStylec_color.AsInteger := colbColor.Color;
end; //procedure TfrmEditStyle.colbColorChangeColor
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl1.Configure;
end; //procedure TfrmEditStyle.acSecurityExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.SetRequiredFieldColor;
var
  xColor: TColor;

begin
  xColor := cRequiredColor;
  laStylename.Font.Color := xColor;
  laAssortment.Font.Color := xColor;
  laState.Font.Color := xColor;
  laYarncnt.Font.Color := xColor;
  laThreads.Font.Color := xColor;
  laColor.Font.Color := xColor;
  laYMSettings.Font.Color := xColor;
end; //procedure TfrmEditStyle.SetRequiredFieldColor
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.InitUserFields;
var
  i, k: integer;
  xDBEdit: TmmDBEdit;
begin
  mStyleUF := ReadUserFields(1);

  tabStyle.DisableControls;
  try
    if mStyleUF.FieldCount > 0 then begin
      { don't modify form height, gbUserFields always visible
      gbUserFields.Visible := true;
      gbUserFields.Height := 25 +mStyleUF.FieldCount *25;
      ClientHeight := 350 +gbUserFields.Height;
      }

      for i := 1 to 10 do begin
        with TmmDBEdit(FindComponent('edUserField' + inttostr(i))) do begin
          Visible := False;
          DataField := '';
        end;
      end; //for i := 1 to 10 do begin

      for i := 1 to 10 do begin
        if mStyleUF.fields[i].Active then begin
          k := mStyleUF.fields[i].Position;

          with TmmDBEdit(FindComponent('edUserField' + inttostr(k))) do begin
            AutoLabel.Control.Caption := mStyleUF.fields[i].FieldName;

{$IFDEF ShowUserGrayed}
            Visible := True;
            Color := clBtnFace;
{$ELSE}
            Color := clWindow;
{$ENDIF}

            DataField := mStyleUF.fields[i].InternalName;
            if mStyleUF.fields[i].FieldType = 0 then
              Width := 80
            else
              Width := 550;
          end;
        end; //if mStyleUF.fields[i].Active then begin
      end; //for i := 1 to 10 do begin
    end //if mStyleUF.FieldCount > 0 then begin
    else begin
      for i := 1 to 10 do
        TmmDBEdit(FindComponent('edUserField' + inttostr(i))).DataField := '';

      { don't modify form height, gbUserFields always visible
      gbUserFields.Visible := false;
      ClientHeight := 365;
      }
    end; //else begin
  finally
    tabStyle.EnableControls;
    ShowHideUserFields;
  end; //try..finally
end; //procedure TfrmEditStyle.InitUserFields
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleAfterRefresh(DataSet: TDataSet);
begin
  tabStyleAfterScroll(nil);
end; //procedure TfrmEditStyle.tabStyleAfterRefresh
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acSortStyleExecute(Sender: TObject);
begin
  if not PostRecord(tabStyle) then exit;
  if mSortOrder = soAscending then
    mSortOrder := soDescending
  else
    mSortOrder := soAscending;

  case mSortOrder of
    soAscending: acSortStyle.ImageIndex := 40;
    soDescending: acSortStyle.ImageIndex := 41;
  end; //case mSortOrder of

  RunQuery;
end; //procedure TfrmEditStyle.acSortStyleExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acCancelFilterExecute(Sender: TObject);
begin
  if not PostRecord(tabStyle) then exit;
  mSFilter := InitFilter(1);
  RunQuery;
end; //procedure TfrmEditStyle.acCancelFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.RunQuery;
var
  xID: integer;
begin
  with qrySelect do begin
    if Active then
      xID := tabStylec_style_id.AsInteger
    else
      xID := 0;

    Active := False;
    CommandText := GetStyleFilterQuery(1, mSFilter, mSortField, mSortOrder);
    // SQL.SaveToFile('c:\temp\xx.txt');
    Open;

    if xID > 0 then
      Locate('c_style_id', xID, []);
  end; //with qryStyle do begin
  UpdateRecordCountPanel;
end; //procedure TfrmEditStyle.RunQuery
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.qrySelectBeforeScroll(DataSet: TDataSet);
begin
  if tabStyle.State in [dsEdit, dsInsert] then
    if tabStyle.Modified then begin
      if DoSaveRecord(mAskForSaving, Self) then begin
        if not PostRecord(tabStyle) then Abort;
      end //if DoSaveRecord(mAskForSaving) then begin
      else
        Abort;
    end //if tabStyle.Modified then begin
    else
      tabStyle.Cancel;
end; //procedure TfrmEditStyle.qrySelectBeforeScroll
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.qrySelectAfterScroll(DataSet: TDataSet);
begin
  tabStyle.Locate('c_style_id', qrySelectc_style_id.AsInteger, []);
end; //procedure TfrmEditStyle.qrySelectAfterScroll
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.UpdateRecordCountPanel;
var
  xFilterCount,
    xTableCount: integer;
begin
  if not qrySelect.Active then exit;
  xFilterCount := qrySelect.RecordCount;
  xTableCount := tabStyle.RecordCount;
  sbRecordCount.SimpleText := Format(cFilterRecs, [xFilterCount, xTableCount]);
  acCancelFilter.Enabled := xFilterCount <> xTableCount;
  mFilterActive := acCancelFilter.Enabled;
end; //procedure TfrmEditStyle.UpdateRecordCountPanel
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleAfterPost(DataSet: TDataSet);
begin
  Write_YMSettings;
  RunQuery;
end; //procedure TfrmEditStyle.tabStyleAfterPost
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.dsStyleStateChange(Sender: TObject);
var
  xViewMode: boolean;
begin
  xViewMode := not(tabStyle.State in [dsEdit, dsInsert]);
  acApplyFilter.Enabled  := xViewMode;
  acSortStyle.Enabled    := xViewMode;
  acCancelFilter.Enabled := xViewMode and mFilterActive;
  isSearch.Enabled       := xViewMode;

  // group Identifikaition
  edc_style_name.ReadOnly  := xViewMode or (tabStylec_style_id.Value = 1);
  comAssortment.ReadOnly   := xViewMode;
  acEditAssortment.Enabled := not xViewMode;
  // group Garndaten
  edyarncnt.ReadOnly       := xViewMode;
  edc_Twist.ReadOnly       := xViewMode;
  comFachung.ReadOnly      := xViewMode;
  comDrehrichtung.ReadOnly := xViewMode;
  edcalc_slip.ReadOnly     := xViewMode;

//  colbColor.ReadOnly := xViewMode;
  // group Kops-, Konendaten
  edCopsGewicht.ReadOnly   := xViewMode;
  acEditCopsLength.Enabled := not xViewMode;
  edKonenGewicht.ReadOnly  := xViewMode;
  acEditConeLength.Enabled := not xViewMode;
  comPackageType.ReadOnly  := xViewMode;
  // group Reinigereinstellungen
  if xViewMode then
    lbYMSettings.Color := clInfoBk
  else
    lbYMSettings.Color := clWindow;
  acSelectYMSettings.Enabled := (not xViewMode) and MMSecurityControl1.CanEnabled(acSelectYMSettings);
  // group Status
  comStyleState.ReadOnly := xViewMode;
  // group Umrechnungsfaktoren
  edc_Factor_INeps.ReadOnly  := xViewMode;
  edc_Factor_IThick.ReadOnly := xViewMode;
  edc_Factor_IThin.ReadOnly  := xViewMode;
  edc_Factor_ISmall.ReadOnly := xViewMode;
  edc_Factor_SFI.ReadOnly    := xViewMode;
  // group Benutzerfelder
  edUserField1.ReadOnly  := xViewMode;
  edUserField2.ReadOnly  := xViewMode;
  edUserField3.ReadOnly  := xViewMode;
  edUserField4.ReadOnly  := xViewMode;
  edUserField5.ReadOnly  := xViewMode;
  edUserField6.ReadOnly  := xViewMode;
  edUserField7.ReadOnly  := xViewMode;
  edUserField8.ReadOnly  := xViewMode;
  edUserField9.ReadOnly  := xViewMode;
  edUserField10.ReadOnly := xViewMode;
  //editing calculated fields !!!
  tabStylecalc_yarncnt.ReadOnly      := xViewMode;
  tabStylecalc_copsgewicht.ReadOnly  := xViewMode;
  tabStylecalc_konengewicht.ReadOnly := xViewMode;

  ShowHideUserFields;
end; //procedure TfrmEditStyle.dsStyleStateChange
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.ShowHideUserFields;
var
  i, xLastPos: integer;
  xEditMode: boolean;
  xField: TField;
  xEdit: TmmDBEdit;
begin
  tabStyle.DisableControls;
  try
    if mStyleUF.FieldCount > 0 then begin
      xEditMode := tabStyle.State in [dsEdit, dsInsert];
      xLastPos := -5;

      for i := 1 to 10 do begin
        xEdit := TmmDBEdit(FindComponent('edUserField' + inttostr(i)));
        with xEdit do begin
          if DataField <> '' then begin


            xField := tabStyle.FieldByName(DataField);
      { TODO 1 -oSDo -cMMStyle :
Seltsames Verhalten:
Kann DataFiled nicht sofort auslesen -> Access error bei xField.IsNull}
            try
              xEdit.Visible := xEditMode or ((not xField.IsNull) and (xField.AsString <> ''));
            except
              on E: Exception do begin
                   // xEdit.Visible := false;
                   RzCSIntf.CodeSite.SendError('TfrmEditStyle.ShowHideUserFields(), Error in "xField.IsNull".  Error msg : ' + e.Message  );
              end;


            end;



{$IFDEF ShowUserGrayed}
            if Visible then
              Color := clWindow
            else
              Color := clBtnFace;
{$ELSE}

            if Visible then begin
              inc(xLastPos, 25);
              Top := xLastPos;
            end; //if xVisible then begin
{$ENDIF}
          end; //if xDataField <> '' then begin
        end; // with
      end; // for i
    end; //if mStyleUF.FieldCount > 0 then begin
  finally
    tabStyle.EnableControls;
  end; //try..finally
end; //procedure TfrmEditStyle.ShowHideUserFields
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.grSelectCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if qrySelectc_style_state.AsInteger <> 1 then begin
    AFont.Color := clRed;
  end; //if qryStylec_style_state.AsInteger <> 1 then begin
end; //procedure TfrmEditStyle.grSelectCalcCellColors
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleAfterDelete(DataSet: TDataSet);
begin
  qrySelect.First;
  RunQuery;
end; //procedure TfrmEditStyle.tabStyleAfterDelete
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.isSearchAfterSearch(Sender: TwwIncrementalSearch;
  MatchFound: Boolean);
begin
  qrySelectAfterScroll(nil);
end; //procedure TfrmEditStyle.isSearchAfterSearch
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.mmTranslatorLanguageChange(Sender: TObject);
begin
  UpdateRecordCountPanel;
end; //procedure TfrmEditStyle.mmTranslatorLanguageChange
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.tabStyleAfterInsert(DataSet: TDataSet);
begin
  edc_style_name.SetFocus;
end; //procedure TfrmEditStyle.tabStyleAfterInsert
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.grSelectDblClick(Sender: TObject);
begin
  if acEditStyle.Enabled then
    if not (tabStyle.State in [dsEdit, dsInsert]) then tabStyle.Edit;
end; //procedure TfrmEditStyle.grSelectDblClick
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.Read_YMSettings;
const
  cQry2 = 'SELECT y.c_ym_set_name, y.c_ym_set_id, y.c_head_class FROM t_style_settings ss ' +
    'JOIN t_xml_ym_settings y ON y.c_ym_set_id = ss.c_ym_set_id ' +
    'WHERE ss.c_style_id = :c_style_id ';
var
  xStr: string;
begin
  lbYMSettings.Clear;
  with dseQry1 do try
    lbYMSettings.Items.BeginUpdate;

    if tabStylec_style_id.asinteger > 0 then begin
      CommandText := cQry2;
      Parameters.ParamByName('c_style_id').value := tabStylec_style_id.asinteger;
      Open;
      while not Eof do begin
        xStr := Format('%s@%s', [fieldbyname('c_ym_set_name').AsString, cGUISensingHeadClassNames[TSensingHeadClass(fieldbyname('c_head_class').AsInteger)]]);
//        xStr := Format('%s@%s', [fieldbyname('c_ym_set_name').asstring, cSensingHeadClassNames[TSensingHeadClass(fieldbyname('c_head_class').asinteger)]]);
        lbYMSettings.Items.AddObject(xStr, TObject(fieldbyname('c_ym_set_id').asinteger));
        Next;
      end; //while not Eof do begin
    end; //else begin

    lbYMSettings.ItemIndex := 0;
  finally
    Active := False;
    lbYMSettings.Items.EndUpdate;
  end; //try..finally
end; //procedure TfrmEditStyle.Read_YMSettings
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.acSelectYMSettingsExecute(Sender: TObject);
var
  xInt: aIntegerT;
begin
  xInt := GetYMSettingsIDs;
  with TfrmSelectYMSettings.Create(Self, xInt) do
  try
    if ShowModal = mrOK then begin
      xInt := GetSelection;
      SetModifiedState(tabStylec_style_name);
      Update_lbYMSettings(xInt);
    end;
  finally
    Free;
  end; //try..finally
end; //procedure TfrmEditStyle.acSelectYMSettingsExecute
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.Update_lbYMSettings(aInt: aIntegerT);
const
  cQry = 'SELECT c_ym_set_name, c_head_class FROM t_xml_ym_settings WHERE c_ym_set_id = %d';
var
  i: integer;
  xStr: string;
begin
  if aInt = nil then
    aInt := GetYMSettingsIDs;

  lbYMSettings.Items.BeginUpdate;
  lbYMSettings.Clear;
  try
    with dseQry1 do
      for i := 0 to high(aInt) do begin
        CommandText := Format(cQry, [aInt[i]]);
        Active := True;
        xStr := Format('%s@%s', [fieldbyname('c_ym_set_name').asstring, cSensingHeadClassNames[TSensingHeadClass(fieldbyname('c_head_class').asinteger)]]);
        lbYMSettings.Items.AddObject(xStr, TObject(aInt[i]));
        Active := False;
      end; //for i := 0 to high(xInt) do begin

    lbYMSettings.ItemIndex := 0;
  finally
    lbYMSettings.Items.EndUpdate;
  end; //try..finally
end; //procedure TfrmEditStyle.Update_lbYMSettings
//-----------------------------------------------------------------------------
function TfrmEditStyle.GetYMSettingsIDs: aIntegerT;
var
  i: integer;
begin
  Result := nil;
  SetLength(Result, lbYMSettings.Items.Count);
  for i := 0 to lbYMSettings.Items.Count - 1 do
    Result[i] := integer(lbYMSettings.Items.Objects[i]);
end; //function TfrmEditStyle.GetYMSettingsIDs
//-----------------------------------------------------------------------------
procedure TfrmEditStyle.Write_YMSettings;
const
  cQryDelete = 'DELETE FROM t_style_settings WHERE c_style_id = :c_style_id';
  cQryInsert = 'INSERT INTO t_style_settings(c_style_id,c_ym_set_id) VALUES(:c_style_id,:c_ym_set_id)';
var
  i: integer;
begin
  dmStyle.conDefault.BeginTrans;
  try
    with TmmAdoCommand.Create(nil) do
    try
      Connection := dmStyle.conDefault;
      // first delete all items in x-table for Style <> YMSettings
      CommandText := cQryDelete;
      Parameters.ParamByName('c_style_id').value := tabStylec_style_id.asinteger;
      Execute;

      // second insert all new YMSettings associated with active Style
      CommandText := cQryInsert;
      for i := 0 to lbYMSettings.Items.Count - 1 do begin
        Parameters.ParamByName('c_style_id').value := tabStylec_style_id.asinteger;
        Parameters.ParamByName('c_ym_set_id').value := integer(lbYMSettings.Items.Objects[i]);
        Execute;
      end; //for i := 0 to lbYMSettings.Items.Count -1 do begin
    finally
      free;
    end;// with TmmAdoCommand.Create(nil) do try

    dmStyle.conDefault.CommitTrans;
  except
    dmStyle.conDefault.RollbackTrans;
    MessageBeep(0);
    MMMessageDlg(cMsg2, mtError, [mbOK], 0, Self);
  end; //try..except
end; //procedure TfrmEditStyle.Write_YMSettings
//------------------------------------------------------------------------------
procedure TfrmEditStyle.mmActionList1Update(Action: TBasicAction; var Handled: Boolean);
var
  xEnabled: Boolean;
begin
  Handled := True;

  xEnabled := tabStyle.State in [dsEdit, dsInsert];
  // Wenn noch kein Template definiert ist, dann kann direkt ein neues Template erzeugt werden LOK 21.11.2002
  // acEditYMSettings.Enabled := MMSecurityControl1.CanEnabled(acEditYMSettings);

  acEditYMSettings.Enabled := (lbYMSettings.Items.Count > 0) and MMSecurityControl1.CanEnabled(acEditYMSettings);
  acSelectYMSettings.Enabled := xEnabled and MMSecurityControl1.CanEnabled(acSelectYMSettings);
end;
//------------------------------------------------------------------------------
procedure TfrmEditStyle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//------------------------------------------------------------------------------
procedure TfrmEditStyle.acStyleChangerExecute(Sender: TObject);
begin
  with TfrmStyleChanger.Create(Self) do try
    ShowModal;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmEditStyle.lbYMSettingsDrawItem(Control: TWinControl; Index: Integer;
  Rect: TRect; State: TOwnerDrawState);
var
  xStr1, xStr2: string;
  xPos: Integer;
begin
  acEditYMSettings.Enabled := MMSecurityControl1.CanEnabled(acEditYMSettings); //Nue: 15.01.03
  with lbYMSettings do begin
    xStr1 := Items.Strings[Index];
    xPos := Pos('@', xStr1);
    if xPos > 0 then begin
      xStr2 := Copy(xStr1, xPos + 1, Length(xStr1) - xPos);
      xStr1 := Copy(xStr1, 1, xPos - 1);
    end
    else
      xStr2 := ' ';
    with Canvas do begin
      if odSelected in State then begin
        Brush.Color := clHighlight;
        Font.Color := clHighlightText;
      end;
      FillRect(Rect);
      TextOut(Rect.Left + 2, Rect.Top + 0, xStr1);
      TextOut(Rect.Right - 50, Rect.Top + 0, xStr2);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmEditStyle.FormShow(Sender: TObject);
begin
  tabStyle.ReadOnly := not acEditStyle.Enabled;
  acCopyStyle.Enabled := acCopyStyle.Enabled and acEditStyle.Enabled;
  acEditCopsLength.Enabled := acEditCopsLength.Enabled and acEditStyle.Enabled;
  acEditConeLength.Enabled := acEditConeLength.Enabled and acEditStyle.Enabled;
  // if not allowed to edit style remove associated buttons
  if not acEditStyle.Enabled then
    navStyle.VisibleButtons := navStyle.VisibleButtons - [nbInsert, nbDelete, nbEdit, nbPost, nbCancel];
end;

procedure TfrmEditStyle.acTemplateExecute(Sender: TObject);
begin
  acEditYMSettingsExecute(acTemplate);
//  acEditYMSettingsExecute(Sender);
end;

procedure TfrmEditStyle.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  CanClose := not(tabStyle.State = dsEdit);
  if not CanClose then begin
    case MessageDlg(Format(rsMsg11, [edc_style_name.Text]), mtConfirmation, [mbYes, mbNo, mbCancel], 0) of
      mrYes: begin
          tabStyle.Post;
          CanClose := True;
        end;
      mrNo: CanClose := True;
    else
    end;
  end; // if not CanClose
end;

end. //u_edit_style

