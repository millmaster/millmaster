inherited frmEditAssortment: TfrmEditAssortment
  Left = 524
  Top = 121
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = '(*)Sortimentstamm bearbeiten'
  ClientHeight = 416
  ClientWidth = 734
  KeyPreview = True
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object paList: TmmPanel
    Left = 0
    Top = 0
    Width = 220
    Height = 416
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 0
    object mmToolBar2: TmmToolBar
      Left = 0
      Top = 0
      Width = 220
      Height = 26
      EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
      Flat = True
      Images = frmMain.ImageList16x16
      Indent = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object ToolButton13: TToolButton
        Left = 3
        Top = 0
        Action = acClose
      end
      object ToolButton14: TToolButton
        Left = 26
        Top = 0
        Width = 8
        ImageIndex = 1
        Style = tbsSeparator
      end
      object ToolButton16: TToolButton
        Left = 34
        Top = 0
        Action = acApplyFilter
      end
      object ToolButton17: TToolButton
        Left = 57
        Top = 0
        Action = acCancelFilter
      end
      object ToolButton2: TToolButton
        Left = 80
        Top = 0
        Width = 8
        ImageIndex = 3
        Style = tbsSeparator
      end
      object ToolButton1: TToolButton
        Left = 88
        Top = 0
        Action = acSortAssortment
      end
      object ToolButton15: TToolButton
        Left = 111
        Top = 0
        Width = 8
        ImageIndex = 2
        Style = tbsSeparator
      end
      object navSelect: TmmDBNavigator
        Left = 119
        Top = 0
        Width = 92
        Height = 22
        DataSource = dsSelect
        VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast]
        Flat = True
        Hints.Strings = (
          '(*)Erster Datensatz'
          '(*)Vorgaengiger Datensatz'
          '(*)Naechster Datensatz'
          '(*)Letzter Datensatz'
          '(*)Einfuegen'
          '(*)Loeschen'
          '(*)Bearbeiten'
          '(*)Speichern'
          '(*)Abbrechen'
          '(*)Daten aktualisieren')
        TabOrder = 0
        UseMMHints = True
      end
    end
    object mmPanel2: TmmPanel
      Left = 0
      Top = 26
      Width = 220
      Height = 390
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object sbRecordCount: TmmStatusBar
        Left = 0
        Top = 368
        Width = 220
        Height = 22
        Panels = <>
        SimplePanel = True
      end
      object gbList: TmmGroupBox
        Left = 5
        Top = 5
        Width = 210
        Height = 355
        Caption = '(*)&Sortimentauswahl-Liste'
        TabOrder = 0
        CaptionSpace = True
        object mmLabel1: TmmLabel
          Left = 10
          Top = 25
          Width = 96
          Height = 13
          Caption = '(20)Sortiment Suche'
          FocusControl = isSearch
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object isSearch: TwwIncrementalSearch
          Left = 10
          Top = 40
          Width = 190
          Height = 21
          DataSource = dsSelect
          OnAfterSearch = isSearchAfterSearch
          ShowMatchText = True
          TabOrder = 0
        end
        object grSelect: TwwDBGrid
          Left = 10
          Top = 65
          Width = 190
          Height = 280
          Selected.Strings = (
            'c_assortment_name'#9'27'#9'c_assortment_name'#9#9)
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = False
          DataSource = dsSelect
          KeyOptions = []
          Options = [dgColumnResize, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint]
          ParentColor = True
          TabOrder = 1
          TitleAlignment = taLeftJustify
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 1
          TitleButtons = True
          OnCalcCellColors = grSelectCalcCellColors
          OnDblClick = grSelectDblClick
          HideAllLines = True
        end
      end
    end
  end
  object paEdit: TmmPanel
    Left = 220
    Top = 0
    Width = 514
    Height = 416
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object mmPanel1: TmmPanel
      Left = 0
      Top = 26
      Width = 514
      Height = 390
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object mmGroupBox1: TmmGroupBox
        Left = 5
        Top = 5
        Width = 265
        Height = 75
        Caption = '(*)&Identifikation'
        TabOrder = 0
        CaptionSpace = True
        object laAssortmentname: TmmLabel
          Left = 5
          Top = 23
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(20)Sortimentname'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edc_assortment_name: TmmDBEdit
          Left = 120
          Top = 20
          Width = 135
          Height = 21
          Color = clWindow
          DataField = 'c_assortment_name'
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmGroupBox4: TmmGroupBox
        Left = 280
        Top = 5
        Width = 230
        Height = 75
        Caption = '(*)&Status'
        TabOrder = 1
        CaptionSpace = True
        object laState: TmmLabel
          Left = 5
          Top = 23
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Status'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel16: TmmLabel
          Left = 5
          Top = 48
          Width = 110
          Height = 13
          Hint = '(*)Sind von dem Sortiment Artikel vorhanden ?'
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Artikel vorhanden'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object imgHistorical: TImage
          Left = 140
          Top = 48
          Width = 16
          Height = 16
          Transparent = True
        end
        object comAssortmentState: TmmDBValComboBox
          Left = 120
          Top = 20
          Width = 100
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 13
          TabOrder = 0
          Visible = True
          Items.Strings = (
            '(20)Aktiv'
            '(20)Inaktiv')
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnly = True
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
          DataField = 'c_assortment_state'
          DataSource = dsAssortment
          Values.Strings = (
            '1'
            '2')
        end
      end
      object gbUserFields: TmmGroupBox
        Left = 5
        Top = 85
        Width = 505
        Height = 275
        Caption = '(*)Benutzerfelder'
        TabOrder = 2
        CaptionSpace = True
        object laUserField1: TmmLabel
          Left = 6
          Top = 24
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField1'
          FocusControl = edUserField1
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField2: TmmLabel
          Left = 6
          Top = 49
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField2'
          FocusControl = edUserField2
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField3: TmmLabel
          Left = 6
          Top = 74
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField3'
          FocusControl = edUserField3
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField4: TmmLabel
          Left = 6
          Top = 99
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField4'
          FocusControl = edUserField4
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField5: TmmLabel
          Left = 6
          Top = 124
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField5'
          FocusControl = edUserField5
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField6: TmmLabel
          Left = 6
          Top = 149
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField6'
          FocusControl = edUserField6
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField7: TmmLabel
          Left = 6
          Top = 174
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField7'
          FocusControl = edUserField7
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField8: TmmLabel
          Left = 6
          Top = 199
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField8'
          FocusControl = edUserField8
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField9: TmmLabel
          Left = 6
          Top = 224
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField9'
          FocusControl = edUserField9
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laUserField10: TmmLabel
          Left = 6
          Top = 249
          Width = 110
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'laUserField10'
          FocusControl = edUserField10
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object edUserField1: TmmDBEdit
          Left = 120
          Top = 20
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 0
          Visible = False
          AutoLabel.Control = laUserField1
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField2: TmmDBEdit
          Left = 120
          Top = 45
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 1
          Visible = False
          AutoLabel.Control = laUserField2
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField3: TmmDBEdit
          Left = 120
          Top = 70
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 2
          Visible = False
          AutoLabel.Control = laUserField3
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField4: TmmDBEdit
          Left = 120
          Top = 95
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 3
          Visible = False
          AutoLabel.Control = laUserField4
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField5: TmmDBEdit
          Left = 120
          Top = 120
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 4
          Visible = False
          AutoLabel.Control = laUserField5
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField6: TmmDBEdit
          Left = 120
          Top = 145
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 5
          Visible = False
          AutoLabel.Control = laUserField6
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField7: TmmDBEdit
          Left = 120
          Top = 170
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 6
          Visible = False
          AutoLabel.Control = laUserField7
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField8: TmmDBEdit
          Left = 120
          Top = 195
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 7
          Visible = False
          AutoLabel.Control = laUserField8
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField9: TmmDBEdit
          Left = 120
          Top = 220
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 8
          Visible = False
          AutoLabel.Control = laUserField9
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
        object edUserField10: TmmDBEdit
          Left = 120
          Top = 245
          Width = 375
          Height = 21
          Color = clWindow
          DataSource = dsAssortment
          ReadOnly = True
          TabOrder = 9
          Visible = False
          AutoLabel.Control = laUserField10
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          ReadOnlyColor = clInfoBk
          ShowMode = smExtended
        end
      end
      object mmStatusBar1: TmmStatusBar
        Left = 0
        Top = 368
        Width = 514
        Height = 22
        Panels = <>
        SimplePanel = True
      end
    end
    object mmToolBar1: TmmToolBar
      Left = 0
      Top = 0
      Width = 514
      Height = 26
      EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
      Flat = True
      Images = frmMain.ImageList16x16
      Indent = 3
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      object ToolButton11: TToolButton
        Left = 3
        Top = 0
        Action = acSecurity
      end
      object ToolButton9: TToolButton
        Left = 26
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton5: TToolButton
        Left = 34
        Top = 0
        Action = acPrint
      end
      object ToolButton12: TToolButton
        Left = 57
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton10: TToolButton
        Left = 65
        Top = 0
        Action = acUserFields
      end
      object ToolButton3: TToolButton
        Left = 88
        Top = 0
        Hint = '(*)Legende fuer Artikelstamm bearbeiten'
        Caption = '(*)Legende bearbeiten'
        ImageIndex = 43
        Visible = False
      end
      object ToolButton4: TToolButton
        Left = 111
        Top = 0
        Width = 8
        ImageIndex = 5
        Style = tbsSeparator
      end
      object ToolButton6: TToolButton
        Left = 119
        Top = 0
        Action = acCopyAssortment
      end
      object mmDBNavigator1: TmmDBNavigator
        Left = 142
        Top = 0
        Width = 135
        Height = 22
        DataSource = dsAssortment
        VisibleButtons = [nbInsert, nbDelete, nbEdit, nbPost, nbCancel]
        Flat = True
        Hints.Strings = (
          '(*)Erster Datensatz'
          '(*)Vorgaengiger Datensatz'
          '(*)Naechster Datensatz'
          '(*)Letzter Datensatz'
          '(*)Einfuegen'
          '(*)Loeschen'
          '(*)Bearbeiten'
          '(*)Speichern'
          '(*)Abbrechen'
          '(*)Daten aktualisieren')
        ConfirmDelete = False
        TabOrder = 0
        UseMMHints = True
      end
      object ToolButton7: TToolButton
        Left = 277
        Top = 0
        Width = 8
        ImageIndex = 4
        Style = tbsSeparator
      end
      object ToolButton8: TToolButton
        Left = 285
        Top = 0
        Action = acHelp
      end
    end
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Sortimentstamm bearbeiten'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 56
    Top = 104
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 88
    Top = 104
    TargetsData = (
      1
      7
      (
        '*'
        'SimpleText'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'DisplayValues'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object mmActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 24
    Top = 104
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Caption = '(*)Artikelformular drucken...'
      Hint = '(*)Stammdaten-Blatt fuer aktuellen Artikel ausdrucken'
      ImageIndex = 2
      ShortCut = 16464
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen'
      ImageIndex = 4
      ShortCut = 112
      OnExecute = acHelpExecute
    end
    object acCopyAssortment: TAction
      Caption = '(*)Sortimentdaten kopieren...'
      Hint = 
        '(*)Neues Sortiment mit den Daten des aktuellen Sortiments anlege' +
        'n'
      ImageIndex = 26
      OnExecute = acCopyAssortmentExecute
    end
    object acUserFields: TAction
      Caption = '(*)&Benutzerfelder...'
      Hint = '(*)Benutzerfelder definieren'
      ImageIndex = 27
      OnExecute = acUserFieldsExecute
    end
    object acDeleteAssortment: TAction
      Hint = '(*)Sortiment loeschen'
    end
    object acEditAssortment: TAction
      Hint = '(*)Sortiment bearbeiten/hinzufuegen'
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      ImageIndex = 28
      OnExecute = acSecurityExecute
    end
    object acApplyFilter: TAction
      Caption = '(*)Filter setzen'
      Hint = '(*)Filter fuer Sortimentdaten setzen'
      ImageIndex = 29
      ShortCut = 16454
      OnExecute = acApplyFilterExecute
    end
    object acCancelFilter: TAction
      Hint = '(*)Filter aufheben'
      ImageIndex = 39
      OnExecute = acCancelFilterExecute
    end
    object acSortAssortment: TAction
      Caption = '(*)Sortimenttabelle sortieren'
      Hint = '(*)Sortimenttabelle sortieren'
      ImageIndex = 40
      OnExecute = acSortAssortmentExecute
    end
  end
  object tabAssortment: TmmADODataSet
    AutoCalcFields = False
    Connection = dmStyle.conDefault
    AfterOpen = tabAssortmentAfterOpen
    AfterInsert = tabAssortmentAfterInsert
    BeforePost = tabAssortmentBeforePost
    AfterPost = tabAssortmentAfterPost
    AfterCancel = tabAssortmentAfterScroll
    BeforeDelete = tabAssortmentBeforeDelete
    AfterDelete = tabAssortmentAfterDelete
    AfterScroll = tabAssortmentAfterScroll
    AfterRefresh = tabAssortmentAfterRefresh
    OnCalcFields = tabAssortmentCalcFields
    OnNewRecord = tabAssortmentNewRecord
    CommandText = 'dbo.t_assortment'
    CommandType = cmdTable
    IndexFieldNames = 'c_assortment_id'
    Parameters = <>
    Left = 443
    Top = 116
    object tabAssortmentc_assortment_id: TSmallintField
      FieldName = 'c_assortment_id'
    end
    object tabAssortmentc_assortment_name: TStringField
      FieldName = 'c_assortment_name'
    end
    object tabAssortmentc_assortment_state: TSmallintField
      FieldName = 'c_assortment_state'
    end
    object tabAssortmentStyleData: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'StyleData'
      DisplayValues = 'Ja;Nein'
      Calculated = True
    end
    object tabAssortmentc_num1: TFloatField
      FieldName = 'c_num1'
    end
    object tabAssortmentc_num2: TFloatField
      FieldName = 'c_num2'
    end
    object tabAssortmentc_num3: TFloatField
      FieldName = 'c_num3'
    end
    object tabAssortmentc_string1: TStringField
      FieldName = 'c_string1'
      Size = 255
    end
    object tabAssortmentc_string2: TStringField
      FieldName = 'c_string2'
      Size = 255
    end
    object tabAssortmentc_string3: TStringField
      FieldName = 'c_string3'
      Size = 255
    end
    object tabAssortmentc_string4: TStringField
      FieldName = 'c_string4'
      Size = 255
    end
    object tabAssortmentc_string5: TStringField
      FieldName = 'c_string5'
      Size = 255
    end
    object tabAssortmentc_string6: TStringField
      FieldName = 'c_string6'
      Size = 255
    end
    object tabAssortmentc_string7: TStringField
      FieldName = 'c_string7'
      Size = 255
    end
  end
  object dsAssortment: TmmDataSource
    AutoEdit = False
    DataSet = tabAssortment
    OnStateChange = dsAssortmentStateChange
    Left = 475
    Top = 116
  end
  object qry1: TmmADODataSet
    Connection = dmStyle.conDefault
    Parameters = <>
    Left = 445
    Top = 151
  end
  object qrySelect: TmmADODataSet
    Connection = dmStyle.conDefault
    BeforeScroll = qrySelectBeforeScroll
    AfterScroll = qrySelectAfterScroll
    CommandText = 
      'SELECT c_assortment_id, c_assortment_name, c_assortment_stateFRO' +
      'M t_assortmentORDER BY c_assortment_name, c_assortment_id'
    Parameters = <>
    Left = 64
    Top = 292
    object qrySelectc_assortment_name: TStringField
      DisplayWidth = 27
      FieldName = 'c_assortment_name'
      Origin = 't_assortment.c_assortment_name'
    end
    object qrySelectc_assortment_state: TSmallintField
      FieldName = 'c_assortment_state'
      Origin = 't_assortment.c_assortment_state'
      Visible = False
    end
    object qrySelectc_assortment_id: TSmallintField
      FieldName = 'c_assortment_id'
      Origin = 't_assortment.c_assortment_id'
      Visible = False
    end
  end
  object dsSelect: TwwDataSource
    DataSet = qrySelect
    Left = 96
    Top = 292
  end
  object ilBitmaps: TImageList
    Left = 120
    Top = 104
    Bitmap = {
      494C010104000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000420000004200000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000004200
      0000006300000063000042000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      84008484840000000000000000000000000000000000000000000000FF008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000420000000063
      0000006300000063000000630000420000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000042000000006300000063
      0000006300000063000000630000006300004200000000000000000000000000
      000000000000000000000000000000000000000000000000FF00000084000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004200000000630000006300000063
      000000FF00000063000000630000006300000063000042000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      8400000084000000840000008400848484000000840000008400000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000630000006300000063000000FF
      00000000000000FF000000630000006300000063000042000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000840000008400000084000000840000008400000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF00000063000000FF00000000
      0000000000000000000000FF0000006300000063000000630000420000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000000000000000
      000000000000000000000000000000FF00000063000000630000006300004200
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FF000000630000006300000063
      0000420000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FF0000006300000063
      0000006300004200000000000000000000000000000000000000000000000000
      00000000FF000000840000008400000084000000840000008400848484000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000FF00000063
      0000006300000063000042000000000000000000000000000000000000000000
      FF00000084000000840000008400848484000000840000008400000084008484
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000000FF
      00000063000000630000006300004200000000000000000000000000FF000000
      8400000084000000840084848400000000000000FF0000008400000084000000
      8400848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000FF000000630000006300004200000000000000000000000000FF000000
      840000008400848484000000000000000000000000000000FF00000084000000
      8400000084008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000FF000000630000006300000000000000000000000000000000
      FF000000840000000000000000000000000000000000000000000000FF000000
      8400000084000000840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF00000084000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000F3FFCFFFFFFFFFFFE1FF87CFFFFFFFFF
      C0FF8387FFFFEFFD807F8103E7FFC7FF003FC003C3FFC3FB083FE007C1FFE3F7
      1C1FF00FC0FFF1E7BE0FF81FC47FF8CFFF07F81FC63FFC1FFF83F01FC71FFE3F
      FFC1E00FEF8FFC1FFFE0C107FFC7F8CFFFF0C383FFE3E1E7FFF8E7C3FFF3C3F3
      FFFDFFE3FFFFC7FDFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
end
