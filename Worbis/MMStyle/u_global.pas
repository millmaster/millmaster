(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_global.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Typen, Variablen, Konstanten
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 | 0.00 | PW  | column t_style.c_opt_speed removed
| 22.06.01        | pw  | column t_style.c_ym_set_id removed
| 28.02.2002        Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
                         - Datentyp von c_yarncnt in rImportDataT auf Single geaendert
| 10.10.2002        LOK | Umbau ADO
| 15.12.2004        SDo | XML: Replace von TYarnUnits nach TYarnUnit
|=========================================================================================*)

{$i symbols.inc}

unit u_global;
interface
uses
  u_common_global, MMUGlobal, XMLDef,
  Forms, DB, Graphics, WinTypes, WinProcs, Classes, Printers, SysUtils,
  BaseSetup;

const
  //divers
  cProgramVersion       = '0.90';
  cCompileDate          = '22.6.2001';

  cUTFactorKey          = 'UTFactors';

  //Helpsystem, Filename and Context IDs
  cHelpfilename         = 'MMSTYLE.HLP';

  cTwistDirAr           : array[boolean] of char = ('S','Z');

  cRequiredColor        = clNavy;
  cMaxExportFields      = 31;

//-----------------------------------------------------------------------------

type
  eStyleStateT          = (ssNone,ssActive,ssInactive);

  rStyleSearchT = record
    SearchName  : s50;
    ExactSearch : boolean;
  end; //rStyleSearchT

  rStyleFilterT = record
    Name           : s50;
    Description    : string;
    State,
    Assortment_ID  : integer;
    YarnCount      : double;
    NoOfThreads,
    Twist,
    TwistDirection,
    YMSettings_ID  : integer;
  end; //rStyleFilterT

  rUserFieldT = record
    ID           : byte;
    InternalName,
    FieldName    : s50;
    Position,
    FieldType    : byte;   //0= integer, 1= string
    Delete,
    Active       : boolean;
  end; //rUserFieldT

  aUserFieldT = array[1..10] of rUserFieldT;

  rUserFieldsT = record
    FieldCount : byte;
    Fields     : aUserFieldT;
  end; //rUserFieldsT

  rExportFieldT = record
    ID           : byte;
    InternalName : s50;
    UserField,
    CanImport,
    Required,
    KeyField     : boolean;
    FieldName    : s50;
    Position     : integer;
  end; //rExportFieldT

  rExportFieldsT = record
    Filename         : string;
    DecimalSeparator,
    FieldSeparator   : byte;
    WithFieldNames   : boolean;
    Fields           : array of rExportFieldT;
  end; //rExportFieldsT

  rImportDataT = record
    c_style_id,
    c_assortment_id,
    c_nr_of_threads   : integer;
    c_twist,
    c_yarncnt         : Single;
    c_style_name,
    c_assortment_name,
    twist_direction   : string;
    c_style_state,
    c_mm_per_cone,
    c_mm_per_bob,
    c_color,
    c_num1,
    c_num2,
    c_num3            : integer;
    c_string1,
    c_string2,
    c_string3,
    c_string4,
    c_string5,
    c_string6,
    c_string7         : string;
    c_inepsconst,
    c_ithickconst,
    c_ithinconst,
    c_ismallconst,
    c_sficonst        : double;
  end; //rImportDataT


{Wss: class isn't needed anymore -> use TMMSettingsReader
  TStyleSettings = class(TBaseSettingsReader)
  private
  protected
  public
    function GetValue(Name: string): Variant;
    procedure SetValue(aName: string; aData: string);
  published
  end; //TStyleSettings
{}


//-----------------------------------------------------------------------------

var
//  gCountUnit        : byte;
  gCountUnit        : TYarnUnit; //TYarnUnits;
  gUserFields       : rUserFieldsT;
  gImpFactorVisible : boolean;
//  gStyleSettings   : TStyleSettings;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{ TStyleSettings }

{Wss: class isn't needed anymore -> use TMMSettingsReader
function TStyleSettings.GetValue(Name: String): Variant;
const
  cQry = 'SELECT data FROM t_mmuparm '+
         'WHERE Appkey = :Appkey AND AppName = ''MMConfiguration'' ';

begin
  with TDBAccess.Create(1, TRUE) do begin
    Init;
    Query[0].SQL.text := cQry;
    Query[0].ParamByName('appkey').asstring := Name;
    Query[0].Open;
    result := Query[0].FieldByName('Data').asstring;
    Free;
  end; //with TDBAccess.Create(1, TRUE) do begin
end; //function TStyleSettings.GetValue
//-----------------------------------------------------------------------------
procedure TStyleSettings.SetValue(aName,aData: string);
const
  cQry = 'UPDATE t_mmuparm SET data = :data '+
         'WHERE Appkey = :Appkey AND AppName = ''MMConfiguration'' ';

begin
  with TDBAccess.Create(1, TRUE) do begin
    Init;
    Query[0].SQL.text := cQry;
    Query[0].ParamByName('appkey').asstring := aName;
    Query[0].ParamByName('data').asstring := aData;
    Query[0].ExecSQL;
    Free;
  end; //with TDBAccess.Create(1, TRUE) do begin
end; //procedure TStyleSettings.SetValue
{}

end. //u_global
