(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_resourcestrings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Strings
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  1.12.00 | 0.00 | PW  | Datei erstellt
|          | 0.00 | PW  |
|=========================================================================================*)

{$i symbols.inc}

unit u_resourcestrings;

interface

resourcestring
  cStyleState1    = '(*)Aktiv'; //ivlm
  cStyleState2    = '(*)Inaktiv'; //ivlm
  cNumericField   = '(*)Zahlenfeld'; //ivlm
  cStringField    = '(*)Zeichenfeld'; //ivlm
  cStyleName      = '(*)Artikelname'; //ivlm
  cPackageType1   = '(*)Konisch'; //ivlm
  cPackageType2   = '(*)Zylindrisch'; //ivlm
  cPackageType3   = '(*)Faerbekonen'; //ivlm

  cFilterRecs     = '(*)Anzahl Datensaetze: %d von %d'; //ivlm

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end. //u_resourcestrings
