(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_view_stylelist.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Style List
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.11.00 | 0.00 | PW  | Datei erstellt
|  7.03.01 | 0.00 | PW  | column "c_opt_speed" removed
| 10.04.01 | 0.00 | PW  | printing grid with QRPrintGrid
| 14.05.01 |      | Wss | UTFactors visible set from TMMSettingsReader
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 16.05.2001       Wss | c_slip value added
| 21.06.2001       PW  | MxN t_style_settings implemented, multiple settings per article
                         - new calc. field "ym_settings" with t_ym_settings.c_ym_set_name(s)
                         - grid option dgShowCellHint added
| 13.07.2001       Wss | Title alignment set to left orientated
| 28.02.2002       Wss | YarnCount auf Single Datentyp umgestellt (auch auf DB)
| 29.08.2002       Wss | - Format von YarnCount war noch mit %.1g -> 100 = 1E2. Ge�ndert auf
                           FormatFloat Funktion.
                         - DB-Feld c_yarn_count war noch sichtbar
| 10.10.2002       LOK | Umbau ADO
| 22.11.2002       Wss | - Bugs in Sortierungen behoben -> wwDBGrid1TitleButtonClick
                         - Refresh Button enabled plus Action mit F5
| 21.06.2004       SDO | Neuer Drucker-DLG
| 18.01.2005  2.00 SDo | Umbau & Anpassungen an XML Vers. 5
|=========================================================================================*)

{$i symbols.inc}

unit u_view_stylelist;

interface

uses
  u_common_global, u_global, mmGraphGlobal,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, ExtCtrls, DBCtrls, mmDBNavigator, ComCtrls, ToolWin,
  mmToolBar, ActnList, mmActionList, MMSecurity, IvDictio, IvMulti,
  IvEMulti, mmTranslator, Grids, Wwdbgrid, Db, Wwdatsrc,
  mmStatusBar, mmGridPos, Menus, mmPopupMenu, DBGrids,
  mmDBGrid, mmDataSource, Printers, u_dmStyle, ADODB, mmADODataSet,
  Wwdbigrd;

type
  TfrmViewStyle = class(TmmForm)
    MMSecurityControl1: TMMSecurityControl;
    mmActionList: TmmActionList;
    acClose: TAction;
    acSearch: TAction;
    acApplyFilter: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acSecurity: TAction;
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton5: TToolButton;
    ToolButton9: TToolButton;
    navStyle: TmmDBNavigator;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton4: TToolButton;
    ToolButton6: TToolButton;
    wwDBGrid1: TwwDBGrid;
    ToolButton3: TToolButton;
    ToolButton10: TToolButton;
    mmStatusBar1: TmmStatusBar;
    mmGridPos1: TmmGridPos;
    pmProfile: TmmPopupMenu;
    miEditGridOptions: TMenuItem;
    N2: TMenuItem;
    miLoadProfile: TMenuItem;
    miSaveProfile: TMenuItem;
    miDeleteProfile: TMenuItem;
    N1: TMenuItem;
    miSaveAsDefault: TMenuItem;
    ToolButton13: TToolButton;
    ToolButton15: TToolButton;
    acCancelFilter: TAction;
    qryStyle: TmmADODataSet;
    dsStyle: TwwDataSource;
    qryStylec_style_id: TSmallintField;
    qryStylec_style_name: TStringField;
    qryStylec_twist_left: TBooleanField;
    qryStylec_style_state: TSmallintField;
    qryStylecalc_ym_settings: TStringField;
    qryStylec_assortment_name: TStringField;
    qryStylec_m_per_bob: TIntegerField;
    qryStylec_m_per_cone: TIntegerField;
    qryStylec_package_type: TSmallintField;
    qryStylec_color: TIntegerField;
    qryStylec_num1: TFloatField;
    qryStylec_num2: TFloatField;
    qryStylec_num3: TFloatField;
    qryStylec_string1: TStringField;
    qryStylec_string2: TStringField;
    qryStylec_string3: TStringField;
    qryStylec_string4: TStringField;
    qryStylec_string5: TStringField;
    qryStylec_string6: TStringField;
    qryStylec_string7: TStringField;
    qryStylecalc_twist: TStringField;
    qryStylecalc_State: TStringField;
    qryStylecalc_Package_Type: TStringField;
    qryStylec_Factor_INeps: TFloatField;
    qryStylec_Factor_IThick: TFloatField;
    qryStylec_Factor_IThin: TFloatField;
    qryStylec_Factor_ISmall: TFloatField;
    qryStylec_Factor_SFI: TFloatField;
    acEditStyle: TAction;
    ToolButton14: TToolButton;
    ToolButton16: TToolButton;
    qryStylec_slip: TSmallintField;
    qryStylecalc_slip: TFloatField;
    qryYMSet: TmmADODataSet;
    mmTranslator: TmmTranslator;
    qryStylec_twist: TFloatField;
    qryStylec_nr_of_threads: TSmallintField;
    qryStylecalc_yarncnt: TStringField;
    qryStylec_yarncnt: TFloatField;
    acGridOptions: TAction;
    acOpenProfile: TAction;
    acSaveProfile: TAction;
    acDeleteProfile: TAction;
    acSaveAsDefault: TAction;
    ToolButton17: TToolButton;
    ToolButton18: TToolButton;
    acRefresh: TAction;

    procedure FormActivate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acSearchExecute(Sender: TObject);
    procedure acApplyFilterExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acGridOptionsExecute(Sender: TObject);
    procedure qryStyleCalcFields(DataSet: TDataSet);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure acLoadProfileExecute(Sender: TObject);
    procedure acSaveProfileExecute(Sender: TObject);
    procedure acDeleteProfileExecute(Sender: TObject);
    procedure acSaveAsDefaultExecute(Sender: TObject);
    procedure wwDBGrid1TitleButtonClick(Sender: TObject; AFieldName: String);
    procedure wwDBGrid1CalcTitleImage(Sender: TObject; Field: TField;
      var TitleImageAttributes: TwwTitleImageAttributes);
    procedure acCancelFilterExecute(Sender: TObject);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure acEditStyleExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acRefreshExecute(Sender: TObject);
  private
    mSSearch: rStyleSearchT;
    mSFilter: rStyleFilterT;
    mSortOrder: eSortOrderT;
    mSortField: string;
    mStyleUF: rUserFieldsT;
    procedure RunQuery;
    procedure InitUserFields;
    procedure LoadProfile(aUsername, aProfilename: string);
    procedure UpdateRecordCountPanel;
  protected
//    procedure SetOLEMode(const Value: Boolean); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure DisplayHint(Sender: TObject);
  end; //TfrmViewStyle

var
  frmViewStyle: TfrmViewStyle;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  MMUGlobal, RzCSIntf,
  u_main, u_lib, u_search_name, u_style_filter, mmLib, u_resourcestrings,
  u_common_lib, MemoIni, u_print_style_list, u_edit_style, XMLDef;

{$R *.DFM}

constructor TfrmViewStyle.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  acOpenProfile.HelpContext   := GetHelpContext('globale-fenster-und-funktionen\DLG_Profile-bearbeiten.htm');
  acSaveProfile.HelpContext   := acOpenProfile.HelpContext;
  acDeleteProfile.HelpContext := acOpenProfile.HelpContext;


  mSortField := 'c_style_name';
  mSortOrder := soAscending;

  mSSearch.SearchName := '';
  mSSearch.ExactSearch := false;

  mSFilter := InitFilter(0);

  mmGridPos1.MemoINI.Username := mmUsername;
  mmGridPos1.MemoINI.Formname := Classname;

  LoadProfile(mmGridPos1.MemoINI.Username,mmGridPos1.MemoINI.GetDefaultProfile);

  RunQuery;
end; //constructor TfrmViewStyle.Create
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.DisplayHint(Sender: TObject);
begin
  mmStatusBar1.Panels[1].Text := GetLongHint(Application.Hint);
end; //procedure TfrmViewStyle.DisplayHint
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.FormActivate(Sender: TObject);
begin
  Application.OnHint := DisplayHint;
end; //procedure TfrmViewStyle.FormActivate
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.FormDestroy(Sender: TObject);
begin
  Application.OnHint := frmMain.DisplayHint;
end; //procedure TfrmViewStyle.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acCloseExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end; //procedure TfrmViewStyle.acCloseExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acSearchExecute(Sender: TObject);
var
  xSOpt : TLocateOptions;
begin
  with TfrmSearchByName.Create(Self) do
  try
    Init(mSSearch, cStyleName);
    if ShowModal = mrOK then begin
      mSSearch := GetSearchValues;
      xSOpt    := [loCaseInsensitive];
      if not mSSearch.ExactSearch then
        Include(xSOpt, loPartialKey);
      qryStyle.Locate('c_style_name', mSSearch.SearchName, xSOpt);
    end;
  finally
    Free;
  end; //with TfrmSearchByName.Create(Application) do begin
end; //procedure TfrmViewStyle.acSearchExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acApplyFilterExecute(Sender: TObject);
var
  xOK : boolean;
begin
  with TfrmStyleFilter.Create(Self) do begin
    Init(1,mSFilter);
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then mSFilter := GetFilterValues;
    Free;
  end; //with TfrmSearchByName.Create(Application) do begin

  if not xOK then exit;
  RunQuery;
end; //procedure TfrmViewStyle.acApplyFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acPrintExecute(Sender: TObject);
var
  xID : integer;
  xPrintColored : Boolean;
begin
  if not GetPrintOptions(poPortrait, Self, xPrintColored) then exit;

  qryStyle.DisableControls;
  xID := qryStylec_style_id.asinteger;
  qryStyle.First;
  try
    with TfrmPrintStyleList.Create(Self, not xPrintColored) do
      try
        Execute;
      finally
        Free;
      end; //try..finally
  finally
    qryStyle.Locate('c_style_id',xID,[]);
    qryStyle.EnableControls;
  end; //try..finally
end; //procedure TfrmViewStyle.acPrintExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl1.Configure;
end; //procedure TfrmViewStyle.acSecurityExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acGridOptionsExecute(Sender: TObject);
begin
  mmGridPos1.EditSettings;
end; //procedure TfrmViewStyle.acGridOptionsExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.RunQuery;
var
  xID : integer;
begin
  with qryStyle do begin
    if Active then xID := qryStylec_style_id.asinteger
              else xID := 0;

    Active := false;
    CommandText := GetStyleFilterQuery(2,mSFilter,mSortField,mSortOrder);
    CodeSite.SendString('ViewStyle.Query', qryStyle.CommandText);
    // SQL.SaveToFile('c:\temp\xx.txt');
    Open;

    if xID > 0 then
      Locate('c_style_id',xID,[]);
  end; //with qryStyle do begin
  
  UpdateRecordCountPanel;
end; //procedure TfrmViewStyle.RunQuery
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.qryStyleCalcFields(DataSet: TDataSet);
var
  xYarnCount: Single;
begin
  if qryStylec_yarncnt.AsInteger > 0 then begin
    xYarnCount := YarnCountConvert(yuNm, gCountUnit, qryStylec_yarncnt.AsFloat);
    qryStylecalc_yarncnt.Value := Format('%s/%d', [formatfloat(',0.0', xYarnCount), qryStylec_nr_of_threads.Value]);
  end;

  if qryStylec_twist.AsInteger > 0 then
    qryStylecalc_twist.Value := formatfloat(',0',qryStylec_twist.asinteger)
                                + ' ' +cTwistDirAr[qryStylec_twist_left.asboolean];

  qryStylecalc_State.value := GetStateStr(qryStylec_style_state.AsInteger);
  qryStylecalc_Package_Type.value := GetPackageTypeStr(qryStylec_package_type.AsInteger);

  // Wss: new added field c_slip in table t_style (factor 1000 = int 1500 -> 1.500
  qryStylecalc_slip.Value := qryStylec_slip.Value / 1000.0;
  if (qryStylecalc_slip.Value < cMinSlip) or (qryStylecalc_slip.Value > cMaxSlip) then
    qryStylecalc_slip.AsString := '';

  with qryYMSet do
    try
      Parameters.ParamByName('c_style_id').value := qryStylec_style_id.asinteger;
      Active := true;
      qryStylecalc_ym_settings.asstring := '';
      First;
      while not eof do begin
        if qryStylecalc_ym_settings.asstring <> '' then
          qryStylecalc_ym_settings.asstring := qryStylecalc_ym_settings.asstring +', ';
        qryStylecalc_ym_settings.asstring := qryStylecalc_ym_settings.asstring + fieldbyname('c_ym_set_name').asstring;
        Next;
      end; //while not eof do begin
    finally
      Active := false;
    end; //with qryYMSet do begin
end; //procedure TfrmViewStyle.qryStyleCalcFields
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.InitUserFields;
var
  i : integer;
  //.......................................................
  function SetField(aField: TFloatField): TFloatField;
  begin
    result := aField;
    result.Visible := result.Visible and gImpFactorVisible;
    if gImpFactorVisible then result.Tag := 0
                         else result.Tag := -1;
  end; //SetField
  //.......................................................
begin
  mStyleUF := ReadUserFields(1);

  qryStyle.DisableControls;
  try
    for i := 1 to 10 do 
      with qryStyle.fieldbyname(mStyleUF.fields[i].InternalName) do begin
        visible := false;
        tag := -1;
      end; //with qryStyle.fieldbyname(mStyleUF.fields[i].InternalName) do begin

    for i := 1 to 10 do
      if mStyleUF.fields[i].Active then
        with qryStyle.fieldbyname(mStyleUF.fields[i].InternalName) do begin
          Visible := true;
          tag := 0;
          DisplayLabel := mStyleUF.fields[i].FieldName;
        end; //with qryStyle.fieldbyname(mStyleUF.fields[i].InternalName) do begin

    qryStylec_Factor_INeps  := SetField(qryStylec_Factor_INeps);
    qryStylec_Factor_IThick := SetField(qryStylec_Factor_IThick);
    qryStylec_Factor_IThin  := SetField(qryStylec_Factor_IThin);
    qryStylec_Factor_ISmall := SetField(qryStylec_Factor_ISmall);
    qryStylec_Factor_SFI    := SetField(qryStylec_Factor_SFI);
  finally
    qryStyle.EnableControls;
  end; //try..finally
end; //procedure TfrmViewStyle.InitUserFields
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if gdFixed in State then ABrush.Color := clLightYellow
                      else ABrush.Color := TwwDBGrid(Sender).Color;

  if qryStylec_style_state.AsInteger <> ord(ssActive) then begin
    AFont.Color := clRed;
  end; //if qryStylec_style_state.AsInteger <> ord(ssActive) then begin

  if highlight then begin
    AFont.Color := clHighlightText;
    ABrush.Color := clHighlight;
  end; //if highlight then

  if Field = qryStylec_color then begin
    AFont.Color := qryStylec_color.asinteger;
    ABrush.Color := AFont.Color;
  end; //if Field = qryStylec_color then begin
end; //procedure TfrmViewStyle.wwDBGrid1CalcCellColors
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acLoadProfileExecute(Sender: TObject);
var
  xRes : tSelectProfileRecordT;
begin
  xRes := mmGridpos1.MemoINI.SelectProfile;
  if xRes.Profilename <> '' then
    LoadProfile(xRes.Username,xRes.Profilename);
end; //procedure TfrmViewStyle.acLoadProfileExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acSaveProfileExecute(Sender: TObject);
var
  xRes : tSelectProfileRecordT;
begin
  xRes := mmGridPos1.MemoINI.SaveProfile;
  if xRes.Profilename <> '' then begin
    mmGridPos1.MemoINI.Profilename := xRes.Profilename;
    mmGridPos1.WriteSettings;
  end; //if xRes.Profilename <> '' then begin
end; //procedure TfrmViewStyle.acSaveProfileExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acDeleteProfileExecute(Sender: TObject);
begin
  mmGridPos1.MemoINI.DeleteProfileDlg;
end; //procedure TfrmViewStyle.acDeleteProfileExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acSaveAsDefaultExecute(Sender: TObject);
begin
  mmGridPos1.WriteSettings;
  mmGridPos1.MemoINI.SetDefaultProfile;
end; //procedure TfrmViewStyle.acSaveAsDefaultExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.LoadProfile(aUsername, aProfilename: string);
begin
  mmGridPos1.MemoINI.Username := aUsername;
  mmGridPos1.MemoINI.Profilename := aProfilename;
  mmGridPos1.ReadSettings;
  InitUserFields;
  wwDBGrid1.Options := wwDBGrid1.Options +[dgShowCellHint];
end; //procedure TfrmViewStyle.LoadProfile
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.wwDBGrid1TitleButtonClick(Sender: TObject; AFieldName: String);
begin
  AFieldName := lowercase(AFieldName);

  //query can't be sorted by this field
  if AFieldname = 'calc_slip' then
    AFieldname := 'c_slip'
  else if AFieldname = 'calc_yarncnt' then
    AFieldname := 'c_yarncnt'
  else if AFieldname = 'calc_twist' then
    AFieldname := 'c_twist'
  else if AFieldname = 'calc_state' then
    AFieldname := 'c_style_state'
  else if AFieldname = 'calc_package_type' then
    AFieldname := 'c_package_type'
  else if AFieldName = 'calc_ym_settings' then
    Exit;

  if mSortField = AFieldName then begin
    if mSortOrder = soAscending then mSortOrder := soDescending
                                else mSortOrder := soAscending;
  end
  else begin
    mSortField := AFieldName;
    mSortOrder := soAscending;
  end; //if mSortField = AFieldName then begin

  RunQuery;
  wwDBGrid1.SelectedField := qryStyle.fieldbyname(mSortField);
end; //procedure TfrmViewStyle.wwDBGrid1TitleButtonClick
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.wwDBGrid1CalcTitleImage(Sender: TObject;
  Field: TField; var TitleImageAttributes: TwwTitleImageAttributes);
begin
  //query can't be sorted by this field
  if lowercase(Field.FieldName) = mSortField then begin
    if mSortOrder = soAscending then TitleImageAttributes.ImageIndex := 44
                                else TitleImageAttributes.ImageIndex := 45;
  end //if lowercase(Field.FieldName) = mSortField then begin
  else TitleImageAttributes.ImageIndex := -1;

  TitleImageAttributes.Alignment := taRightJustify;
  TitleImageAttributes.Margin := 1;
end; //procedure TfrmViewStyle.wwDBGrid1CalcTitleImage
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acCancelFilterExecute(Sender: TObject);
begin
  mSFilter := InitFilter(1);
  RunQuery;
end; //procedure TfrmViewStyle.acCancelFilterExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.mmTranslatorLanguageChange(Sender: TObject);
begin
  UpdateRecordCountPanel;
end; //procedure TfrmViewStyle.mmTranslatorLanguageChange
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.UpdateRecordCountPanel;
var
  xFilterCount,
  xTableCount   : integer;

begin
  if not qryStyle.Active then
    exit;
  xFilterCount := qryStyle.RecordCount;
  xTableCount := GetRecordCount('t_style','');
  mmStatusBar1.Panels[0].Text := format(cFilterRecs, [xFilterCount,xTableCount]);
  acCancelFilter.Enabled := xFilterCount <> xTableCount;
end; //procedure TfrmViewStyle.UpdateRecordCountPanel
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.wwDBGrid1DblClick(Sender: TObject);
begin
  if acEditStyle.Enabled then
    acEditStyle.Execute;
end; //procedure TfrmViewStyle.wwDBGrid1DblClick
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acEditStyleExecute(Sender: TObject);
var
  xForm: TfrmEditStyle;
begin
  xForm := TfrmEditStyle.Create(Self);
  xForm.Init(qryStylec_style_id.asinteger);
end; //procedure TfrmViewStyle.acEditStyleExecute
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//-----------------------------------------------------------------------------
procedure TfrmViewStyle.acRefreshExecute(Sender: TObject);
begin
  navStyle.DataSource.DataSet.Refresh;
end;
//-----------------------------------------------------------------------------

end. //u_view_stylelist

