(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_user_fields.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  1.12.00   | 0.00 | PW  | Datei erstellt
| 14.05.2001          Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 10.10.2002          LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_edit_userfields;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ActnList, mmActionList, IvDictio, IvMulti, IvEMulti,
  mmTranslator, StdCtrls, Nwcombo, mmButton, ExtCtrls, mmPanel, ComCtrls,
  mmListView, mmLabel, mmEdit, mmGroupBox, Buttons, mmSpeedButton;

type
  TfrmEditUserFields = class(TmmForm)
    mmTranslator1: TmmTranslator;
    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    lbUserFields: TNWListBox;
    mmGroupBox2: TmmGroupBox;
    lbAvailable: TNWListBox;
    mmSpeedButton3: TmmSpeedButton;
    mmSpeedButton4: TmmSpeedButton;
    mmButton1: TmmButton;
    acHelp: TAction;
    acInsert: TAction;
    acDelete: TAction;
    acMoveUp: TAction;
    acMoveDown: TAction;
    mmButton2: TmmButton;
    acRename: TAction;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;

    procedure acOKExecute(Sender: TObject);
    procedure acInsertExecute(Sender: TObject);
    procedure ItemDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acDeleteExecute(Sender: TObject);
    procedure lbAvailableDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure lbUserFieldsDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure acMoveUpExecute(Sender: TObject);
    procedure acMoveDownExecute(Sender: TObject);
    procedure acRenameExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    mVariante: byte;
    mTempFields: rUserFieldsT;
    mDeleted: boolean;
    procedure MoveSelectedItem(aOldPos,aDropPos: integer);
    procedure EnableActions;
    function UpdateTable: boolean;
  public
    procedure Init(aVariante: byte); //1= t_style, 2= t_assortment
  end; //TfrmEditUserFields

var
  frmEditUserFields: TfrmEditUserFields;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs,
  u_main, u_common_lib, u_lib, u_resourcestrings, mmADOCommand, u_dmStyle;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Es wurden Benutzerfelder geloescht. Der Inhalt geloeschter Felder geht ebenfalls verloren.' +#10#13 +'Moechten Sie weitermachen?'; //ivlm
  cMsg2 = '(*)Fehler beim Loeschen der Benutzerfelder.' +#10#13 +'Die Aenderungen konnten nicht gespeichert werden!'; //ivlm
  cMsg3 = '(*)Feldname umbenennen'; //ivlm
  cMsg4 = '(*)Feldname'; //ivlm

//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acOKExecute(Sender: TObject);
begin
  if not UpdateTable then
    ModalResult := mrCancel;
end; //procedure TfrmEditUserFields.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.Init(aVariante: byte); //1= t_style, 2= t_assortment
var
  i : integer;
begin
  mVariante := aVariante;
  mTempFields := ReadUserFields(mVariante);

  SortUserFields(mTempFields);

  mDeleted := false;

  lbUserFields.Clear;
  lbAvailable.Clear;

  for i := 1 to 10 do begin
    mTempFields.Fields[i].Delete := false;
    if mTempFields.Fields[i].Active
      then lbUserFields.AddObj(mTempFields.Fields[i].FieldName,mTempFields.Fields[i].ID)
      else lbAvailable.AddObj(LTrans(mTempFields.Fields[i].FieldName),mTempFields.Fields[i].ID);
  end; //for i := 1 to 10 do begin

  EnableActions;
end; //procedure TfrmEditUserFields.Init
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acInsertExecute(Sender: TObject);
begin
  ItemDragDrop(lbUserFields,lbAvailable,0,0);
end; //procedure TfrmEditUserFields.acInsertExecute
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acDeleteExecute(Sender: TObject);
begin
  ItemDragDrop(lbAvailable,lbUserFields,0,0);
end; //procedure TfrmEditUserFields.acDeleteExecute
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.ItemDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  xDropPos : integer;
  i,k      : integer;
  s        : string;

begin
  //end of dragging
  if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then begin
    i := TNWListBox(Source).ItemIndex;
    s := TNWListBox(Source).Items[i];
    k := TNWListBox(Source).GetItemIDAtPos(i);

    if TNWListBox(Source) = lbUserFields then begin
      //set delete mark => to delete field content
      mDeleted := true;
      mTempFields.Fields[k].Delete := true;
      //reset fieldname
      if k < 4 then s := cNumericField
               else s := cStringField;
{
      if k < 4 then s := LTrans(cNumericField)
               else s := LTrans(cStringField);
{}
    end; //if TNWListBox(Source) = lbUserFields then begin

    TNWListBox(Sender).AddObj(s,k);
    TNWListBox(Source).Items.Delete(i);

    EnableActions;
    //Update_lbAttribut;
  end //if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then
  else begin
    if (Sender = Source) and ((Sender as TNWListBox) = lbUserFields) then begin
      xDropPos := lbUserFields.ItemAtPos(Point(X,Y),false);
      i := lbUserFields.ItemIndex;
      MoveSelectedItem(i,xDropPos);
    end; //if (Sender = Source) and ((Sender as TNWListBox) = lbUserFields) then begin
  end; //else begin
end; //procedure TfrmEditUserFields.ItemDragDrop
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.MoveSelectedItem(aOldPos, aDropPos: integer);
begin
  lbUserFields.Items.Move(aOldPos,aDropPos);
  lbUserFields.ItemIndex := aDropPos;
end; //procedure TfrmEditUserFields.MoveSelectedItem
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.EnableActions;
begin
  acInsert.Enabled := lbAvailable.Items.Count > 0;
  acDelete.Enabled := lbUserFields.Items.Count > 0;
  acMoveUp.Enabled := lbUserFields.Items.Count > 1;
  acMoveDown.Enabled := acMoveUp.Enabled;
  acRename.Enabled := acDelete.Enabled;

  if lbUserFields.Items.Count > 0 then lbUserFields.ItemIndex := 0;
  if lbAvailable.Items.Count > 0 then lbAvailable.ItemIndex := 0;
end; //procedure TmmGridPosEditor.EnableActions
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.lbAvailableDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  inherited;
  Accept := Source <> Sender;
end; //procedure TfrmEditUserFields.lbAvailableDragOver
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.lbUserFieldsDragOver(Sender, Source: TObject;
  X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  xDropPos : integer;
begin
  if Source = lbUserFields then begin
    xDropPos := lbUserFields.ItemAtPos(Point(X,Y),false);
    Accept  := (xDropPos > -1) and
               (xDropPos <> lbUserFields.ItemIndex) and
               (xDropPos < lbUserFields.Items.Count);
  end //if Source = lbUserFields then begin
  else Accept := Source = lbAvailable;
end; //procedure TfrmEditUserFields.lbUserFieldsDragOver
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acMoveUpExecute(Sender: TObject);
begin
  if lbUserFields.ItemIndex > 0 then MoveSelectedItem(lbUserFields.ItemIndex,lbUserFields.ItemIndex -1);
end; //procedure TfrmEditUserFields.acMoveUpExecute
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acMoveDownExecute(Sender: TObject);
begin
  if lbUserFields.ItemIndex < (lbUserFields.Items.Count -1) then MoveSelectedItem(lbUserFields.ItemIndex,lbUserFields.ItemIndex +1);
end; //procedure TfrmEditUserFields.acMoveDownExecute
//-----------------------------------------------------------------------------
function TfrmEditUserFields.UpdateTable: boolean;
const
  cQry1 = 'UPDATE t_style SET %s = null WHERE %s IS NOT NULL';
  cQry2 = 'UPDATE t_assortment SET %s = null WHERE %s IS NOT NULL';

var
  i,xID : integer;
  xStr  : string;
begin
  result := true;

  dmStyle.conDefault.BeginTrans;
  try
    //delete field content of removed userfields
    if mDeleted then begin
      MessageBeep(0);
      if MMMessageDlg(cMsg1, mtConfirmation, [mbYes,mbCancel], 0, Self) <> mrYes then begin
        dmStyle.conDefault.RollbackTrans;
        Exit;
      end;

      for i := 1 to 10 do
        if mTempFields.Fields[i].Delete then begin
          if i < 4 then xStr := 'c_num' +inttostr(i)
                   else xStr := 'c_string' +inttostr(i-3);
          with TmmAdoCommand.Create(nil) do try
            Connection := dmStyle.ConDefault;
            case mVariante of
              1: CommandText := format(cQry1,[xStr,xStr]);
              2: CommandText := format(cQry2,[xStr,xStr]);
            end; //case mVariante of
            Execute;
          finally
            free;
          end;// with TmmAdoCommand.Create(nil) do try
        end; //if mTempFields.Fields[i].Delete then begin
    end; //if mDeleted then begin

    //set active fields
    for i := 0 to lbUserFields.Items.Count -1 do begin
      xID := lbUserFields.GetItemIDAtPos(i);
      mTempFields.Fields[xID].FieldName := lbUserFields.Items[i];
      mTempFields.Fields[xID].Position := i+1;
      mTempFields.Fields[xID].Active := true;
    end; //for i := 0 to lbUserFields.Items.Count -1 do begin

    //set inactive fields
    for i := 0 to lbAvailable.Items.Count -1 do begin
      xID := lbAvailable.GetItemIDAtPos(i);
      mTempFields.Fields[xID].FieldName := lbAvailable.Items[i];
      mTempFields.Fields[xID].Position := 0;
      mTempFields.Fields[xID].Active := false;
    end; //for i := 0 to lbUserFields.Items.Count -1 do begin

    WriteUserFields(mVariante,mTempFields);

    dmStyle.conDefault.CommitTrans;
  except
    dmStyle.conDefault.RollbackTrans;
    result := false;
    MessageBeep(0);
    MMMessageDlg(cMsg2, mtError, [mbOK], 0, Self);
  end; //try..except
end; //function TfrmEditUserFields.UpdateTable
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acRenameExecute(Sender: TObject);
var
  xStr : string;
begin
  xStr := lbUserFields.GetItemText;
  if not InputQuery(cMsg3, cMsg4, xStr) then exit;
//  if not InputQuery(LTrans(cMsg3),LTrans(cMsg4),xStr) then exit;
  if trim(xStr) <> '' then lbUserFields.Items[lbUserFields.ItemIndex] := xStr;
end; //procedure TfrmEditUserFields.acRenameExecute
//-----------------------------------------------------------------------------
procedure TfrmEditUserFields.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TfrmEditUserFields.acHelpExecute

procedure TfrmEditUserFields.acCancelExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmEditUserFields.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('LabMaster\Style\STYLE_Benutzerfelder_definieren.htm');
end;

end. //u_edit_user_fields
