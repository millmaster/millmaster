inherited frmSearchByName: TfrmSearchByName
  Left = 697
  Top = 426
  ActiveControl = edSearchName
  Caption = '(*)Nach Datensatz suchen'
  ClientHeight = 123
  ClientWidth = 320
  KeyPreview = True
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object laFieldName: TmmLabel
    Left = 10
    Top = 10
    Width = 6
    Height = 13
    Caption = '0'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton1: TmmButton
    Left = 82
    Top = 90
    Width = 75
    Height = 23
    Action = acOK
    Caption = '(9)OK'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 162
    Top = 90
    Width = 75
    Height = 23
    Action = acCancel
    Caption = '(9)Abbrechen'
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edSearchName: TmmEdit
    Left = 10
    Top = 25
    Width = 300
    Height = 21
    Color = clWindow
    MaxLength = 50
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object cbExactSearch: TmmCheckBox
    Left = 10
    Top = 60
    Width = 300
    Height = 17
    Hint = '(*)Nach exakter Uebereinstimmung suchen'
    Caption = '(40)Exakte Suche'
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 152
    Top = 8
    TargetsData = (
      1
      2
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0))
  end
  object mmActionList1: TmmActionList
    Left = 120
    Top = 8
    object acOK: TAction
      Caption = '(10)&Uebernehmen'
      Hint = '(*)Suche starten'
      ShortCut = 16397
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(10)&Abbrechen'
      Hint = '(*)Suche abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
  end
end
