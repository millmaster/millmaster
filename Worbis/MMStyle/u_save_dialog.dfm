inherited frmSaveDialog: TfrmSaveDialog
  Left = 429
  Top = 273
  ActiveControl = mmButton1
  Caption = '(*)Aenderungen speichern?'
  ClientHeight = 128
  ClientWidth = 370
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 10
    Top = 10
    Width = 350
    Height = 45
    AutoSize = False
    Caption = 
      '(*)Der aktuelle Datensatz wurde geaendert. Sollen die Aenderunge' +
      'n jetzt gespeichert werden?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    WordWrap = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbAsk: TmmCheckBox
    Left = 10
    Top = 60
    Width = 350
    Height = 17
    Caption = '(*)Nicht mehr nachfragen und Datensaetze automatisch speichern.'
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton1: TmmButton
    Left = 97
    Top = 95
    Width = 85
    Height = 25
    Action = acOK
    Caption = '(9)OK'
    Default = True
    TabOrder = 1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 187
    Top = 95
    Width = 85
    Height = 25
    Action = acCancel
    Caption = '(9)Abbrechen'
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmActionList1: TmmActionList
    Left = 312
    Top = 80
    object acOK: TAction
      Caption = '(12)&Speichern'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      OnExecute = acCancelExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 306
    Top = 26
    TargetsData = (
      1
      2
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0))
  end
end
