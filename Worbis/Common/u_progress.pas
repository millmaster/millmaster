(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_progress.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Fortschrittsbalken in einem eigenen Fenster anzeigen
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 05.12.98 | 0.01 | PW  | Using MM Components
|=========================================================================================*)

{$i symbols.inc}

unit u_progress;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, BASEFORM, mmProgressBar, mmLabel, IvDictio, IvMulti,
  IvEMulti, mmTranslator;

type
  TProgressForm = class(TmmForm)
    ProgressBar1: TmmProgressBar;
    mmTranslator: TmmTranslator;
    laInfo: TmmLabel;

  private
  public
    procedure Init(aInfo: string; aMax,aStep: longint);
    procedure Exit;
    procedure StepIt;
  end; //TProgressForm

var
  ProgressForm: TProgressForm;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TProgressForm.Init(aInfo: string; aMax,aStep: longint);
begin
  ProgressBar1.Max := aMax;
  if aStep = 0 then aStep := 1;
  ProgressBar1.Step := astep;
  ProgressBar1.Position := 0;
  Show;
  laInfo.Caption := aInfo;
  laInfo.Update;
end; //procedure TProgressForm.InitProgressForm
//-----------------------------------------------------------------------------
procedure TProgressForm.Exit;
begin
  ProgressForm.Visible := false;
end; //procedure TProgressForm.Exit
//-----------------------------------------------------------------------------
procedure TProgressForm.StepIt;
begin
  ProgressBar1.StepIt;
end; //procedure TProgressForm.StepIt

end. //u_progress
