(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_set_printoptions.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Dialog zum Einstellen von Druckoptionen, wird vor dem Ausdruck von
|                 Berichten, Listen aufgerufen
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  6.12.00 | 0.00 | PW  | Datei erstellt
| 14.05.2001       Wss | call to LTrans remarked because resourcestrings hasn't to be translated manually
|=========================================================================================*)

{$i symbols.inc}

unit u_set_printoptions;

interface

uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, ActnList, ComCtrls, ToolWin, IvDictio, IvMulti, IvEMulti,
  IvMlDlgs, BASEFORM, mmActionList, mmToolBar, mmCheckBox, mmLabel, mmGroupBox,
  mmPanel, mmTranslator, mmPrinterSetupDialog, mmButton, printers;

type
  TSetPrintOptionsDlg = class(TmmForm)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acPrinter: TAction;
    acHelp: TAction;

    MainPanel: TmmPanel;
    gbCurrentPrinter: TmmGroupBox;
    laPrintername: TmmLabel;
    laOrientation: TmmLabel;
    laCopies: TmmLabel;
    laPrinternameValue: TmmLabel;
    laOrientationValue: TmmLabel;
    laCopiesValue: TmmLabel;
    bPrinter: TmmButton;

    PrinterSetupDialog1: TmmPrinterSetupDialog;
    mmTranslator: TmmTranslator;

    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acPrinterExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    procedure ReadPrinterInfo;
  public
    constructor Create(aOwner: TComponent; aDefaultOrientation: TPrinterOrientation); reintroduce; virtual;
  end; //u_set_printoptions

var
  SetPrintOptionsDlg: TSetPrintOptionsDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_common_global, u_common_lib, u_lib, u_main;

{$R *.DFM}

resourcestring
  cPortraitStr  = '(20)Hochformat'; //ivlm
  cLandscapeStr = '(20)Querformat'; //ivlm

constructor TSetPrintOptionsDlg.Create(aOwner: TComponent;
  aDefaultOrientation: TPrinterOrientation);
begin
  inherited create(aOwner);
  Printer.Orientation := aDefaultOrientation;
  ReadPrinterInfo;
end; //constructor TSetPrintOptionsDlg.Create
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.ReadPrinterInfo;
begin
  with Printer do begin
    laPrinternameValue.Caption := Printers[PrinterIndex];
    case Orientation of
      poLandscape: laOrientationValue.Caption := cLandscapeStr;
      poPortrait: laOrientationValue.Caption  := cPortraitStr;
    end; //case Orientation of

    laCopiesValue.Caption := inttostr(Copies);
  end; //with Printer do
end; //procedure TSetPrintOptionsDlg.ReadPrinterInfo
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acOKExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end; //procedure TSetPrintOptionsDlg.ActionPrintExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acCancelExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end; //procedure TSetPrintOptionsDlg.ActionCancelExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acPrinterExecute(Sender: TObject);
begin
  inherited;
  PrinterSetupDialog1.Execute;
  ReadPrinterInfo;
end; //procedure TSetPrintOptionsDlg.ActionPrinterExecute
//-----------------------------------------------------------------------------
procedure TSetPrintOptionsDlg.acHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, HelpContext)
end; //procedure TSetPrintOptionsDlg.ActionHelpExecute

end. //u_set_printoptions
 
