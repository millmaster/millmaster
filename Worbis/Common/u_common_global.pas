(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_common_global.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Typen, Variablen, Konstanten
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer, InfoPower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.1998 | 0.00 | PW  | Datei erstellt
| 25.02.2005 | 0.01  |SDo | Neue Func. GetGryScale() fuer Black & White Print
|=========================================================================================*)

{$i symbols.inc}

unit u_common_global;
interface
uses
  Forms,DB,Graphics,WinTypes,WinProcs,Classes,Printers;

const
  //divers
  cCaptionHintSeparator = '|';
  cCRLF                 = #13#10;
  cCRLFStr              = '///';
  cBS                   = '\';
  cEpsilon              = 1e-9;
  sTrue                 = 'T';
  sFalse                = 'F';
  clLightGray1          = $00E0E0E0;
  clLightGray2          = $00E5E5E5;
  clLightGray3          = $00D4D4D4;
  clLightYellow         = $00E8FCFF;
  clMarkCol             = $00C9C9C9;

  cMinPerHour           = 60;
  cHoursPerDay          = 24;
  cMinPerDay            = cMinPerHour *cHoursPerDay;
  cWeeksPerYear         = 53;
  cMaxShiftsPerDay      = 5;
  cMaxShiftMonths       = 12;
  cDaysPerWeek          = 7;
  cMonthsPerYear        = 12;
  cMaxDaysPerMonth      = 31;
  cSecPerMin            = 60;

  //cursors
  crDragFolder          = 1;
  crDropFolder          = 2;
  crWaitDelete          = 3;
  crWaitReindex         = 4;
  crWaitArrow           = 5;
  crWaitClose           = 6;
  crRemoteCursor        = 7;
  crLoadCursor          = 8;
  crHandDrag            = 9;
  crDragBar             = 10;
  crWaitPrint           = 11;
  crUpArrow             = 12;
  crFineCross           = 13;

  //db login defaults, used when launching application without parameters
  cDefServerName        = 'WETSRVBDE';
  cDefUserName          = 'SA';
  cDefPassword          = 'QUINCY';
  cDefDBAliasName       = 'MM_WINDINGODBC';


type
  s1  = string[1];
  s3  = string[3];
  s5  = string[5];
  s20 = string[20];
  s30 = string[30];
  s50 = string[50];

  // added from previous file version for shift calendar
  eWindowOpenModeT = (woModal,woSingle,woMulti);

  sByteT    = set of byte;
  aIntegerT = array of integer;

//-----------------------------------------------------------------------------
function GetGryScale(aColor : TColor): TColor;

//-----------------------------------------------------------------------------
var
  gTempPath,
  gDataBaseName : string;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


//-----------------------------------------------------------------------------
function GetGryScale(aColor : TColor): TColor;
 var xRed, xBlue, xGreen, xGray : Byte	;
begin
  xRed := GetRValue(aColor);
  xGreen := GetGValue(aColor);
  xBlue := GetBValue(aColor);

  xGray := Round(0.299 * xRed + 0.587 * xGreen + 0.114 * xBlue);

  if (xRed = xGreen) and ( xGreen = xBlue) then
    Result := aColor
  else  begin
    if xGray < 50 then inc(xGray, 30); //Etwas aufhellen
    Result := RGB(xGray, xGray, xGray);
  end;
end;
//-----------------------------------------------------------------------------


end. //u_common_global.pas
