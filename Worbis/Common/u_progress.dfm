inherited ProgressForm: TProgressForm
  Left = 488
  Top = 195
  BorderIcons = []
  BorderStyle = bsToolWindow
  Caption = '(35)Einen Moment bitte'
  ClientHeight = 45
  ClientWidth = 250
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object laInfo: TmmLabel
    Left = 3
    Top = 8
    Width = 6
    Height = 13
    Caption = '0'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object ProgressBar1: TmmProgressBar
    Left = 5
    Top = 23
    Width = 240
    Height = 16
    Min = 0
    Max = 100
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 208
    TargetsData = (
      1
      2
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0))
  end
end
