(*=============================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_common_lib.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Allgemeine Funktionen und Prozeduren
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW  | Datei erstellt
| 17.11.98 | 0.00 | PW  | Added function MakeUniqueID
| 05.12.98 | 0.01 | PW  | Using MM Components
| 25.11.00 | 0.02 | PW  | new function: ReOpenQuery
| 14.05.2001       Wss  | call to LTrans remarked because resourcestrings hasn't to be translated manually
| 18.08.2001       PW   | new function: CalcPercentage
| 23.08.2001       PW   | GetPrintOptions moved from \style\u_lib
| 10.10.2002       LOK  | Umbau ADO
| 01.11.2002       LOK  | Prozedur um ins DebugLog zu schreiben hinzugefügt (WriteDebugLog)
| 01.11.2002       LOK  | Fehlermeldungen werden ins DebugLog geschrieben, wenn eine Exception auftritt.
| 15.12.2004       SDo  | Funk. GetPrintOptions() mit Param. aPrintColored erweitert
|============================================================================================*)

{$I symbols.inc}

unit u_common_lib;

interface

uses
  u_common_global,
  SysUtils, Buttons, DB, DBCtrls, Classes, Tabs, StdCtrls, Forms, WinTypes,
  Graphics, DBGrids, TabNotBk, Mask, ComCtrls, Controls, Grids,
  ShiftCalendars, Printers, mmCommonLib, mmADODataSet;

procedure incr(var a: double; b: double);
procedure incs(var a: single; b: single);
function ex(aBasis, aExponent: double): double;
function log(aValue: double): double;
function ten(aValue: double): double;
procedure ChangeVars(var a; var b; size: word);
function space(i: byte): string;
function LFillString(s: string; i: byte): string;
function FillString(s: string; c: char; i: byte): string;
function CutString(s: string; i: byte): string;
function ReplaceChar(InChar, OutChar: char; s: string): string;
function CountChar(s: string; c: char): byte;
function DeleteChar(InputStr, DelStr: string): string;
function NotDeleteChar(InputStr, NotDelStr: string): string;
function Replicate(c: char; l: byte): string;
function DaysPerMonth(Year, Month: integer): integer;
function ReplaceString(QuellStr, InputStr: string; Position: byte): string;
function DBFieldOk(F: TField; fstr: string; LLimit, ULimit: double; Required: boolean; aOwner: TWinControl = nil): boolean;
function DeleteRecord(MsgStr: string; aOwner: TWinControl = nil): boolean;
function CheckDriveReady(aDrive: char): boolean;
function PostRecord(t: TDataset): boolean;
procedure SetRecordEditMode(t: TDataset);
function GetCurrentDay: word;
function GetCurrentMonth: word;
function GetCurrentYear: word;
function GetEnabledFontColor(IsEnabled: boolean): TColor;
function GetEnabledFC(Enabled: boolean; EnabledColor, DisabledColor: TColor): TColor;
function GetWeekNo(aDate: TDateTime): byte;
function GetMonth(aDate: TDateTime): word;
function GetYear(aDate: TDateTime): word;
function DateOK(aDate: string; aOwner: TWinControl = nil): boolean;
procedure SetTextDirection(c: TCanvas; Angle: single);
function GetEnvStr(aVarString: string): string;
function lz(value: double; len: byte): string;
function _GetComputerName: string;
function _GetUserName: string;
function GetTimeStr: string;
function SetTempPath: string;
function Dow(d: tdatetime): integer;
function bDOW(dow: byte): byte;
function usDOW(dow: byte): byte;
function GetFirstStr(s: string; SeparatorChar: char): string;
function GetLastStr(s: string; SeparatorChar: char): string;
function ValTimeStr(TimeStr: s5): s5;
function GetTimeFromStr(TimeStr: s5): double;
function AddMonth(StartDate: TDateTime; MonthToAdd: word): TDateTime;
procedure GetLastMonth(var aYear, aMonth: word; aDecMonth: integer);
procedure GetQuarterRange(var aStart, aEnd: word);
function GetParamStr(aKey: string): string;
function GetMonthFromYM(aYearMonth: longint): word;
function GetYearFromYM(aYearMonth: longint): word;
function SetYearMonth(aYear, aMonth: word): longint;
function IsLinked(aTablename, aIndexFieldNames: string; aFindKeyValue: Variant): boolean;
function IsLinkedMsg(aTablename, aIndexFieldNames, aMsgText: string; aFindKeyValue: Variant; aOwner: TWinControl = nil): boolean;
function MakeUniqueID(aFieldType: byte; aTable, aCol: string; aOwner: TWinControl = nil): longint;
function GetRecordCount(aTable, aFilter: string): longint;
function LTrans(aTransString: string): string;
function GetShortMonthNames(aMonth: byte): string;
function GetLongMonthNames(aMonth: byte): string;
function GetShortDayNames(aDay: byte): string;
function GetLongDayNames(aDay: byte): string;
function FormatBool(aFormatStr: string; aBoolValue: boolean): string;
procedure GetNavigatorWidth(n: TDBNavigator);
function Format_hmm(aMinutes: integer): string;
function Format_h(aMinutes: integer): string;
function FloatToNumStr(aValue: extended): string;

function ReOpenQuery(q: TmmAdoDataSet): boolean;
function CreateDirs(aDirs: string; aOwner: TWinControl = nil): boolean;
function CalcPercentage(aValue, aTotal: double): double;
function GetPrintOptions(aDefaultOrientation: TPrinterOrientation; aOwner: TComponent; var aPrintBlackWhite: Boolean): boolean;

 procedure WriteDebugLog(aMsg: string; aDefaultText: string); // LOK 1.11.02
// function DeleteThousandSeparator(s: string): string;
// function Different(var a,b; size: word): boolean;
// function GermanUCase(s: string): string;
// function GermanChr(i: byte): char;
// function GetMemoString(memo: TMemoField): string;
// procedure StopApplication(aMsgStr: string);
// procedure delay(aSeconds: word);

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Dialogs, ExtCtrls, WinProcs, DbiProcs, DbiTypes, DbiErrs, FileCtrl, Menus,
  u_main, MMDialogs, u_set_printoptions, LoepfeGlobal, ADOdb, mmEventLog,
  PrintSettingsTemplateForm, mmPageControl;

resourcestring
  cMsgStr1 = '(80)Eingabe in Feld %s erforderlich'; //ivlm
  cMsgStr2 = '(50)Fehler in Feld %s. Erlaubter Eingabe-Bereich: %s'; //ivlm
  cMsgStr3 = '(60)Datumsformat %s ist ungueltig'; //ivlm
  cErrorMakeUniqueID = '(150)Fehler beim Erzeugen einer eindeutigen ID fuer Tabelle "%s" in Feld "%s"'; //ivlm
  cCreateDirErr = '(*)Verzeichnis %s konnte nicht erstellt werden.'; //ivlm

//  Schreibt eine Meldung ins DebugLog abhängig von DebugConfig
procedure WriteDebugLog(aMsg: string; aDefaultText: string);
begin
  if CodeSiteEnabled then
    WriteToEventLogDebug(aMsg,aDefaultText);
end;// procedure WriteDebugLog(aMsg: string; aDefaultText: string);
//-----------------------------------------------------------------------------

procedure incr(var a: double; b: double);
begin
  a := a + b;
end; //procedure incr
//-----------------------------------------------------------------------------
procedure incs(var a: single; b: single);
begin
  a := a + b;
end; //procedure incs
//-----------------------------------------------------------------------------
function ex(aBasis, aExponent: double): double;
begin
  Result := Exp(aExponent * Ln(aBasis));
end; //function Ex
//-----------------------------------------------------------------------------
function log(aValue: double): double;
begin
  try
    Result := ln(aValue) / ln(10);
  except
    Result := 0;
  end; //try..except
end; //function Log
//-----------------------------------------------------------------------------
function ten(aValue: double): double;
begin
  Result := ex(10, aValue);
end; //function Ten
//-----------------------------------------------------------------------------
procedure ChangeVars(var a; var b; Size: word);
var
  xBuf: array[1..255] of byte;

begin
  move(A, xBuf, Size);
  move(B, A, Size);
  move(xBuf, B, Size);
end; //procedure ChangeVars
//-----------------------------------------------------------------------------
function space(i: byte): string;
begin
  Result := '';
  for i := 1 to i do
    Result := Result + #32;
end; //function space
//-----------------------------------------------------------------------------
function LFillString(s: string; i: byte): string;
const
  c = #32;

var
  k, j: byte;

begin
  Result := s;
  k := length(Result);
  if k >= i then exit;
  j := i - k;
  Result := space(j) + s;
end; //function LFillString
//-----------------------------------------------------------------------------
function FillString(s: string; c: char; i: byte): string;
begin
  for i := (Length(s) + 1) to i do
    s := s + c;
  FillString := s;
end; //function FillString
//-----------------------------------------------------------------------------
function CutString(s: string; i: byte): string;
var
  l: integer;

begin
  l := length(s);
  if l > i then
    Result := copy(s, 1, i)
  else
    Result := fillstring(s, #32, i);
end; //function CutString
//-----------------------------------------------------------------------------
function replacechar(InChar, OutChar: char; s: string): string;
var
  i: byte;

begin
  for i := 1 to Length(s) do
    if (s[i] = InChar) then s[i] := OutChar;
  Result := s;
end; //function replacechar
//-----------------------------------------------------------------------------
function CountChar(s: string; c: char): byte;
begin
  Result := 0;
  while pos(c, s) > 0 do begin
    inc(Result);
    delete(s, pos(c, s), 1);
  end; //while pos(c,s) > 0 do
end; //function CountChar
//-----------------------------------------------------------------------------
function deletechar(InputStr, DelStr: string): string;
var
  c: char;
  i: integer;

begin
  for i := 1 to length(DelStr) do begin
    c := DelStr[i];

    while pos(c, InputStr) > 0 do
      delete(InputStr, pos(c, InputStr), 1);
  end; //for i := 1 to length(DelStr) do

  Result := InputStr;
end; //function deletechar
//-----------------------------------------------------------------------------
function notdeletechar(InputStr, NotDelStr: string): string;
var
  c: char;
  i: integer;

begin
  Result := '';

  for i := 1 to length(InputStr) do begin
    c := InputStr[i];
    if pos(c, NotDelStr) > 0 then
      Result := Result + c;
  end; //for i := 1 to length(InputStr) do
end; //function notdeletechar
//-----------------------------------------------------------------------------
function checkreal(s: string): boolean;
begin
  s := deletechar(s, ThousandSeparator);
  try
    strtofloat(s);
    Result := True;
  except
    Result := False;
  end; //try..except
end; //function checkreal
//-----------------------------------------------------------------------------
function replicate(c: char; l: byte): string;
var
  i: byte;

begin
  Result := '';
  for i := 1 to l do
    Result := Result + c;
end; //function replicate
//-----------------------------------------------------------------------------
function IsLeapYear(Year: integer): boolean;
begin
  Result := (Year mod 4 = 0) and ((Year mod 100 <> 0) or (Year mod 400 = 0));
end; //function IsLeapYear
//-----------------------------------------------------------------------------
function DaysPerMonth(Year, Month: integer): integer;
const
  Days: array[1..12] of byte = (31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);

begin
  Result := Days[Month];
  if (Month = 2) and IsLeapYear(Year) then inc(Result);
end; //function DaysPerMonth
//-----------------------------------------------------------------------------
function ReplaceString(QuellStr, InputStr: string; Position: byte): string;
var
  i: byte;

begin
  if Length(QuellStr) < (Position + length(InputStr) - 1) then
    for i := 1 to (Position + length(InputStr) - 1 - length(QuellStr)) do
      QuellStr := QuellStr + #32;

  dec(Position);
  for i := 1 to Length(InputStr) do
    QuellStr[Position + i] := InputStr[i];
  Result := QuellStr;
end; //function ReplaceString
//-----------------------------------------------------------------------------
function DBFieldOk(F: TField; fstr: string; LLimit, ULimit: double; Required: boolean; aOwner: TWinControl): boolean;
const
  cTStr = ' - ';

var
  xValue: double;
  xStr,
    xRangeStr: string;

begin
  Result := True;

  if fStr = '' then
    xStr := f.DisplayLabel
  else
    xStr := fstr;

 //if F.IsNull then
  if F.AsString = '' then
    if Required then begin
      Result := False;
      MMMessageDlg(Format(cMsgStr1, [xstr]), mtError, [mbOk], 0, aOwner);
      exit;
    end //if Required then
    else
      exit;

  if f.DataType = ftString then exit;

  xValue := F.AsFloat;

  if ((xValue + cEpsilon) < LLimit) or (xValue > (ULimit + cEpsilon)) then Result := False;

  if not Result then begin
    case f.DataType of
      ftTime: xRangeStr := TimeToStr(LLimit) + cTStr + TimeToStr(ULimit);
      ftDate,
//Wss   ftDateTime: xRangeStr := formatdatetime_('d/m/yyyy',LLimit) +cTStr +formatdatetime_('d/m/yyyy',ULimit);
      ftDateTime: xRangeStr := formatdatetime(ShortDateFormat, LLimit) + cTStr + formatdatetime(ShortDateFormat, ULimit);
      ftFloat: xRangeStr := FormatFloat(',0.00', LLimit) + cTStr + FormatFloat(',0.00', ULimit);
      ftSmallint,
        ftInteger,
        ftWord: xRangeStr := FormatFloat(',0', LLimit) + cTStr + FormatFloat(',0', ULimit);
    end; //case f.DataType of

    MMMessageDlg(Format(cMsgStr2, [xStr, xRangeStr]), mtError, [mbOk], 0, aOwner);
  end; //if not result then
end; //function DBFieldOk
//-----------------------------------------------------------------------------
function DeleteRecord(MsgStr: string; aOwner: TWinControl): boolean;
begin
  Result := MMMessageDlg(MsgStr, mtConfirmation, [mbYes, mbNo], 0, aOwner) = mrYes;
end; //function DeleteRecord
//-----------------------------------------------------------------------------
function CheckDriveReady(aDrive: char): boolean;
var
  xErrorMode: word;

begin
  xErrorMode := SetErrorMode(SEM_FailCriticalErrors);
  try
    Result := DiskSize(ord(upcase(aDrive)) - 64) <> -1;
  finally
    SetErrorMode(xErrorMode);
  end; //try..finally
end; //function CheckDriveReady
//-----------------------------------------------------------------------------
function PostRecord(t: TDataset): boolean;
begin
  try
    with t do
      if Active and (State in [dsEdit, dsInsert]) then Post;
    PostRecord := True;
  except
    on e:Exception do begin
      PostRecord := False;
      WriteDebugLog('[u_common_lib.PostRecord]', e.Message);
    end;// on e:Exception do begin
  end; //try..except
end; //function PostRecord
//-----------------------------------------------------------------------------
procedure SetRecordEditMode(t: TDataset);
begin
  with t do begin
    if State in [dsEdit, dsInsert] then exit;
    if RecordCount = 0 then
      Insert
    else
      Edit;
  end; //with t do
end; //procedure SetRecordEditMode
//-----------------------------------------------------------------------------
function GetCurrentDay: word;
var
  xYear, xMonth: word;

begin
  DecodeDate(sysutils.date, xYear, xMonth, Result);
end; //function GetCurrentMonth
//-----------------------------------------------------------------------------
function GetCurrentMonth: word;
var
  xYear, xDay: word;

begin
  DecodeDate(sysutils.date, xYear, Result, xDay);
end; //function GetCurrentMonth
//-----------------------------------------------------------------------------
function GetCurrentYear: word;
var
  xMonth, xDay: word;

begin
  DecodeDate(sysutils.date, Result, xMonth, xDay);
end; //function GetCurrentYear
//-----------------------------------------------------------------------------
function GetEnabledFontColor(IsEnabled: boolean): TColor;
begin
  if IsEnabled then
    Result := clWindowText
  else
    Result := clGray;
end; //function GetEnabledFontColor
//-----------------------------------------------------------------------------
function GetEnabledFC(Enabled: boolean; EnabledColor, DisabledColor: TColor): TColor;
begin
  if Enabled then
    Result := EnabledColor
  else
    Result := DisabledColor;
end; //function GetEnabledFC
//-----------------------------------------------------------------------------
function GetWeekNo(aDate: TDateTime): byte;
const
  caDOW: array[1..7, 1..2] of byte = ((2, 1), (1, 1), (31, 12), (30, 12), (29, 12), (4, 1), (3, 1));
  caDOW_NYD: array[1..7] of byte = (52, 52, 53, 52, 52, 53, 52); //day of week, new years day

var
  xDiffDays: integer;
  hYear,
    xYear, xMonth,
    xDay: word;
  xCheckDate,
    xNewYearDay: TDateTime;

begin
  DecodeDate(aDate, xYear, xMonth, xDay);
  xNewYearDay := EncodeDate(xYear, 1, 1);

  hYear := xYear;
  if DayOfWeek(xNewYearDay) in [3, 4, 5] then dec(hYear);
  xCheckDate := EncodeDate(hyear, caDOW[DayOfWeek(xNewYearDay), 2], caDOW[DayOfWeek(xNewYearDay), 1]);

  xDiffDays := round(aDate - xCheckDate);
  if aDate <= xCheckDate then xDiffDays := -1;

  Result := (xDiffDays div 7) + 1;
  if xDiffDays < 0 then Result := caDOW_NYD[DayOfWeek(xNewYearDay)];

  if xMonth = 12 then
    case xDay of
      31:
        if DayOfWeek(aDate) in [2..4] then Result := 1;
      30:
        if DayOfWeek(aDate) in [2, 3] then Result := 1;
      29:
        if DayOfWeek(aDate) in [2] then Result := 1;
    end; //case xDay of
end; //function GetWeekNo
//-----------------------------------------------------------------------------
function GetMonth(aDate: TDateTime): word;
var
  xYear, xDay: word;

begin
  DecodeDate(aDate, xYear, Result, xDay);
end; //function GetMonth
//-----------------------------------------------------------------------------
function GetYear(aDate: TDateTime): word;
var
  xMonth, xDay: word;

begin
  DecodeDate(aDate, Result, xMonth, xDay);
end; //function GetYear
//-----------------------------------------------------------------------------
function DateOK(aDate: string; aOwner: TWinControl = nil): boolean;
begin
  Result := True;
  try
    StrToDate(aDate);
  except
    Result := False;
    MMMessageDlg(Format(cMsgStr3, [aDate]), mtError, [mbOk], 0, aOwner);
  end; //try..except
end; //function DateOK
//-----------------------------------------------------------------------------
procedure SetTextDirection(c: TCanvas; Angle: single);
var
  xLogRec: TLogFont;

begin
  GetObject(c.Font.Handle, SizeOf(xLogRec), Addr(xLogRec));
  xLogRec.lfEscapement := round(Angle * 10);
  c.Font.Handle := CreateFontIndirect(xLogRec);
end; //procedure SetTextDirection
//-----------------------------------------------------------------------------
function GetEnvStr(aVarString: string): string;
var
  xLen: word;
  xEnvStz: PChar;
  xaNameStz: array[0..180] of char;

begin
  StrPCopy(xaNameStz, aVarString);
  xLen := StrLen(xaNameStz);
  xEnvStz := GetEnvironmentStrings;

  while xEnvStz^ <> #0 do begin
    if (StrLIComp(xEnvStz, xaNameStz, xLen) = 0) and (xEnvStz[xLen] = '=') then begin
      Result := UpperCase(StrPas(xEnvStz + xLen + 1));
      exit;
    end; //if (StrLIComp(xEnvStz,xaNameStz,...

    inc(xEnvStz, StrLen(xEnvStz) + 1);
  end; //while xEnvStz^ <> #0 do

  Result := '';
end; //function GetEnvStr
//-----------------------------------------------------------------------------
function lz(value: double; len: byte): string;
var
  xFrmstr: string;

begin
  xFrmstr := Replicate('0', len);
  Result := formatfloat(xFrmstr, value);
end; //lz
//-----------------------------------------------------------------------------
function _GetComputerName: string;
var
  xaBuf: array[0..255] of char;
  xBuffSize: DWORD;

begin
  xBuffSize := sizeOf(xaBuf);
  GetComputerName(@xaBuf, xBuffSize);
  Result := xaBuf;
end; //function _GetComputerName
//-----------------------------------------------------------------------------
function _GetUserName: string;
var
  xaBuf: array[0..255] of char;
  xBuffSize: DWORD;

begin
  xBuffSize := sizeOf(xaBuf);
  GetUserName(@xaBuf, xBuffSize);
  Result := xaBuf;
end; //function _GetUserName
//-----------------------------------------------------------------------------
function GetTimeStr: string;
begin
  Result := FormatDateTime('hh:nn', now);
end; //function GetTimeStr
//-----------------------------------------------------------------------------
function SetTempPath: string;
const
  cDefTempPath = 'C:\TEMP\';

var
  i: word;
  xaBuf: array[0..255] of char;

begin
  i := sizeof(xaBuf);
  GetTempPath(i, xaBuf);
  if xaBuf = '' then xaBuf := cDefTempPath;
  try
    forcedirectories(xaBuf);
    Result := xaBuf;
    i := Length(Result);
    if Result[i] <> '\' then Result := Result + '\';
  except
    Result := '';
  end; //try..except
end; //function SetTempPath
//-----------------------------------------------------------------------------
function Dow(d: tdatetime): integer;
begin
  Result := DayOfWeek(d) - 1;
  if Result = 0 then Result := 7;
end; //function Dow
//-----------------------------------------------------------------------------
function bDOW(dow: byte): byte;
begin
 //us sonntag = 1. wochentag, nicht ganz biblisch
  if dow = 1 then
    dow := 7
  else
    dec(dow);
  Result := dow;
end; //bDOW
//-----------------------------------------------------------------------------
function usDOW(dow: byte): byte;
begin
  if dow = 7 then
    dow := 1
  else
    inc(dow);
  Result := dow;
end; //usDOW
//-----------------------------------------------------------------------------
function GetFirstStr(s: string; SeparatorChar: char): string;
var
  i: integer;

begin
  i := pos(SeparatorChar, s);
  if i = 0 then
    Result := s
  else
    Result := copy(s, 1, i - 1);
end; //function GetFirstStr
//-----------------------------------------------------------------------------
function GetLastStr(s: string; SeparatorChar: char): string;
var
  i: integer;

begin
  i := pos(SeparatorChar, s);
  if i = 0 then
    Result := s
  else
    Result := copy(s, i + 1, MaxInt);
end; //function GetLastStr
//-----------------------------------------------------------------------------
function GetMinute(s: string): byte;
begin
  s := GetLastStr(s, cTimeSeparator);
  try
    Result := strtoint(s);
  except
    Result := 0;
  end; //try..except
end; //function GetMin
//-----------------------------------------------------------------------------
function GetHour(s: string): byte;
begin
  s := GetFirstStr(s, cTimeSeparator);
  try
    Result := strtoint(s);
  except
    Result := 0;
  end; //try..except
end; //function GetHour
//-----------------------------------------------------------------------------
function ValTimeStr(TimeStr: s5): s5;
var
  xHour,
    xMin: byte;

begin
  Result := trim(TimeStr);
  if (Result <> '') and (Result <> cTimeSeparator) then begin
    xMin := GetMinute(Result);
    xHour := GetHour(Result);
    Result := lz(xHour, 2) + cTimeSeparator + lz(xMin, 2);
  end //if (result <> '') and (result <> cTimeSeparator) then
  else
    Result := ''; //null
end; //function ValTimeStr
//-----------------------------------------------------------------------------
function GetTimeFromStr(TimeStr: s5): double;
begin
  TimeStr := trim(TimeStr);
  if (TimeStr <> '') and (TimeStr <> cTimeSeparator) then begin
    Result := (GetHour(TimeStr) * 60 + GetMinute(TimeStr)) / cMinPerDay;
  end //if (TimeStr <> '') and (TimeStr <> cTimeSeparator) then
  else
    Result := 0;
end; //function GetTimeFromStr
//-----------------------------------------------------------------------------
function AddMonth(StartDate: TDateTime; MonthToAdd: word): TDateTime;
const
  cStartYear = 1900;

var
  xDay,
    xMonth,
    xYear,
    xResult: word;

begin
  DecodeDate(StartDate, xYear, xMonth, xDay);
  xResult := ((xYear - cStartYear) * cMonthsPerYear + xMonth) + MonthToAdd - 1;
  xYear := cStartYear + (xResult div 12);
  xMonth := (xResult mod 12) + 1;
  Result := encodedate(xYear, xMonth, 1);
end; //function AddMonth
//-----------------------------------------------------------------------------
procedure GetLastMonth(var aYear, aMonth: word; aDecMonth: integer);
var
  i: integer;

begin
  for i := 1 to aDecMonth do begin
    dec(aMonth);
    if aMonth = 0 then begin
      aMonth := cMonthsPerYear;
      dec(aYear);
    end; //if m = 0 then
  end; //for i := 1 to decMonth do
end; //procedure GetLastMonth
//-----------------------------------------------------------------------------
procedure GetQuarterRange(var aStart, aEnd: word);
begin
  case GetCurrentMonth of
    1..3: begin
        aStart := 1;
        aEnd := 3;
      end;
    4..6: begin
        aStart := 4;
        aEnd := 6;
      end;
    7..9: begin
        aStart := 7;
        aEnd := 9;
      end;
    10..12: begin
        aStart := 10;
        aEnd := 12;
      end;
  end; //case GetCurrentMonth of
end; //procedure GetQuarterRange
//-----------------------------------------------------------------------------
function GetParamStr(aKey: string): string;
var
  i: integer;
  xStr: string;

begin
  Result := '';
  aKey := uppercase(aKey);
  for i := 1 to ParamCount do begin
    if pos(aKey, UpperCase(ParamStr(i))) = 1 then begin
      xStr := ParamStr(i);
      delete(xStr, 1, length(aKey));
      Result := trim(xStr);
      exit;
    end; //if pos(aKey,UpperCase(ParamStr(i))) = 1 then
  end; //for i := 1 to ParamCount do
end; //function GetParamStr
//-----------------------------------------------------------------------------
function GetMonthFromYM(aYearMonth: longint): word;
begin
  Result := aYearMonth mod 12;
  if Result = 0 then Result := 12;
end; //procedure GetMonthFromYM
//-----------------------------------------------------------------------------
function GetYearFromYM(aYearMonth: longint): word;
var
  xMonth: word;

begin
  xMonth := aYearMonth mod 12;
  Result := aYearMonth div 12;
  if xMonth = 0 then dec(Result);
end; //procedure GetYearFromYM
//-----------------------------------------------------------------------------
function SetYearMonth(aYear, aMonth: word): longint;
begin
  Result := aYear * 12 + aMonth
end; //procedure GetYearFromYM
//-----------------------------------------------------------------------------
function IsLinked(aTablename, aIndexFieldNames: string; aFindKeyValue: Variant): boolean;
begin
  with TmmADODataSet.Create(Application) do try
    ConnectionString := GetDefaultConnectionString;
    CommandType := cmdTable;
    CommandText := aTablename;
    Active := True;
    Result := Locate(aIndexFieldNames, aFindKeyValue, [loCaseInsensitive]);
  finally
    Free;
  end; //try..finally
(*  with TmmTable.Create(Application) do
    try
      DatabaseName := gDataBaseName;
      TableName := aTablename;
      IndexFieldNames := aIndexFieldNames;
      Active := true;
      result := FindKey([aFindKeyValue]);
    finally
      Free;
    end; //try..finally*)
end; //function IsLinked
//-----------------------------------------------------------------------------
function IsLinkedMsg(aTablename, aIndexFieldNames, aMsgText: string; aFindKeyValue: Variant; aOwner: TWinControl): boolean;
begin
  Result := IsLinked(aTablename, aIndexFieldNames, aFindKeyValue);
  if Result then begin
    MessageBeep(0);
    MMMessageDlg(aMsgText, mtWarning, [mbOK], 0, aOwner);
  end; //if result then
end; //function IsLinkedMsg
//-----------------------------------------------------------------------------
//returns -1 on error
function MakeUniqueID(aFieldType: byte; aTable, aCol: string; aOwner: TWinControl): longint;
var
  xDataSet: TmmADODataSet;
  xID,
    xMaxID,
    xLastID: longint;

begin
  Result := -1;
  xMaxID := 0;

  case aFieldType of
    1: xMaxID := High(Byte);
    2: xMaxID := High(SmallInt);
    3: xMaxID := High(LongInt);
  end; //case aFieldType of

  xDataSet := TmmADODataSet.Create(Application);
  try
    with xDataSet do begin
      ConnectionString := GetDefaultConnectionString;
      CommandText := Format('SELECT MAX(%s) AS maxid FROM %s', [aCol, aTable]);
      Active := True;

      if not Eof then
        xLastID := FieldByName('maxid').AsInteger
      else
        xLastID := 0; //increment by 1, see below
      Active := False;

      if xLastID < xMaxID then
        Result := xLastID + 1
      else begin
        CommandText := Format('SELECT %s AS colid FROM %s ORDER BY %s', [aCol, aTable, aCol]);
        Active := True;

        xLastID := FieldByName('colid').AsInteger;

        while not Eof do begin
          xID := FieldByName('colid').AsInteger;
          if (xID - xLastID) > 1 then begin
            Result := xLastID + 1;
            break;
          end //if (xID -xLastID) > 1 then
          else
            xLastID := xID;

          Next;

          if xID > xMaxID then break;
        end; //while not Eof do
      end; //if xLastID < xMaxID then result := xLastID +1
    end; //with xQuery do

    if Result < 0 then begin
      MessageBeep(0);
      MMMessageDlg(Format(cErrorMakeUniqueID, [aTable, aCol]), mtError, [mbOK], 0, aOwner);
    end; //is error
  finally
    xDataSet.Free;
  end; //try..finally
end; //function MakeUniqueID
//-----------------------------------------------------------------------------
function GetRecordCount(aTable, aFilter: string): longint;
begin
  with TmmADODataSet.Create(Application) do
  try
    ConnectionString := GetDefaultConnectionString;
    CommandText := Format('SELECT COUNT(*) AS anzahl FROM %s ', [aTable]);
    if aFilter <> '' then
      CommandText := CommandText + aFilter;
    Active := True;
    Result := FieldByName('anzahl').AsInteger;
  finally
    Free;
  end; //try..finally
end; //function GetRecordCount
//-----------------------------------------------------------------------------
function LTrans(aTransString: string): string;
begin
  Result := frmMain.Dictionary.Translate(aTransString);
end; //function LTrans
//-----------------------------------------------------------------------------
function GetShortMonthNames(aMonth: byte): string;
begin
  case aMonth of
    1: Result := cShortJanuary;
    2: Result := cShortFebruary;
    3: Result := cShortMarch;
    4: Result := cShortApril;
    5: Result := cShortMay;
    6: Result := cShortJune;
    7: Result := cShortJuly;
    8: Result := cShortAugust;
    9: Result := cShortSeptember;
    10: Result := cShortOctober;
    11: Result := cShortNovember;
    12: Result := cShortDecember;
  end; //case aMonth of
end; //function GetShortMonthNames
//-----------------------------------------------------------------------------
function GetLongMonthNames(aMonth: byte): string;
begin
  case aMonth of
    1: Result := cLongJanuary;
    2: Result := cLongFebruary;
    3: Result := cLongMarch;
    4: Result := cLongApril;
    5: Result := cLongMay;
    6: Result := cLongJune;
    7: Result := cLongJuly;
    8: Result := cLongAugust;
    9: Result := cLongSeptember;
    10: Result := cLongOctober;
    11: Result := cLongNovember;
    12: Result := cLongDecember;
  end; //case aMonth of
end; //function GetLongMonthNames
//-----------------------------------------------------------------------------
function GetShortDayNames(aDay: byte): string;
begin
  case aDay of
    1: Result := cShortMonday;
    2: Result := cShortTuesday;
    3: Result := cShortWednesday;
    4: Result := cShortThursday;
    5: Result := cShortFriday;
    6: Result := cShortSaturday;
    7: Result := cShortSunday;
  end; //case aDay of
end; //function GetShortDayNames
//-----------------------------------------------------------------------------
function GetLongDayNames(aDay: byte): string;
begin
  case aDay of
    1: Result := cLongMonday;
    2: Result := cLongTuesday;
    3: Result := cLongWednesday;
    4: Result := cLongThursday;
    5: Result := cLongFriday;
    6: Result := cLongSaturday;
    7: Result := cLongSunday;
  end; //case aDay of
end; //function GetLongDayNames
//-----------------------------------------------------------------------------
function FormatBool(aFormatStr: string; aBoolValue: boolean): string;
begin
  if aBoolValue then
    Result := GetFirstStr(aFormatStr, ';')
  else
    Result := GetLastStr(aFormatStr, ';');
end; //function FormatBool
//-----------------------------------------------------------------------------
procedure GetNavigatorWidth(n: TDBNavigator);
var
  nBtn: TNavigateBtn;
  btnCount: byte;

begin
  btnCount := 0;
  for nBtn := nbFirst to nbRefresh do
    if nBtn in n.VisibleButtons then
      inc(btnCount);
  n.Width := btnCount * 23;
  if n.VisibleButtons = [] then n.Visible := False;
end; //procedure GetNavigatorWidth
//-----------------------------------------------------------------------------
function Format_hmm(aMinutes: integer): string;
var
  xHours,
    xMin: integer;

begin
  xHours := aMinutes div 60;
  xMin := aMinutes mod 60;
  Result := Format('%dh %sm', [xHours, lz(xMin, 2)]);
end; //function Format_hhmm
//-----------------------------------------------------------------------------
function Format_h(aMinutes: integer): string;
begin
  Result := Format('%dh', [round(aMinutes / 60)]);
end; //function Format_h
//-----------------------------------------------------------------------------
function FloatToNumStr(aValue: extended): string;
var
  xSaveDC: char;

begin
  xSaveDC := DecimalSeparator;
  DecimalSeparator := '.';
  try
    try
      Result := FloatToStr(aValue);
    except
      Result := ''
    end; //try..except
  finally
    DecimalSeparator := xSaveDC;
  end; //try..finally
end; //function FloatToNumStr
//-----------------------------------------------------------------------------
function ReOpenQuery(q: TmmADODataSet): boolean;
begin
  Result := True;
  try
    if q.Active then q.Active := False;
    q.Active := True;
  except
    on e:Exception do begin
      Result := False;
      WriteDebugLog('[u_common_lib.ReOpenQuery]', e.Message);
    end;// on e:Exception do begin
  end; //try..except
end; //function ReOpenQuery
//-----------------------------------------------------------------------------
function CreateDirs(aDirs: string; aOwner: TWinControl = nil): boolean;
var
  xStr: string;

begin
  Result := True;
  xStr := ExtractFilePath(aDirs);
  if xStr <> '' then
    if not ForceDirectories(xStr) then begin
      MessageBeep(0);
      MMMessageDlg(Format(cCreateDirErr, [xStr]), mtError, [mbOK], 0, aOwner);
      Result := False;
    end; //if not ForceDirectories(xStr) then begin
end; //function CreateDirs
//-----------------------------------------------------------------------------
function CalcPercentage(aValue, aTotal: double): double;
begin
  if aTotal > 0 then
    Result := aValue / aTotal * 100
  else
    Result := 0;
{
  try
    result := aValue /aTotal *100;
  except
    result := 0;
  end; //try..except
{}
end; //function CalcPercentage
//-----------------------------------------------------------------------------
function GetPrintOptions(aDefaultOrientation: TPrinterOrientation; aOwner: TComponent; var aPrintBlackWhite: Boolean): boolean;
var x : integer;
    xPageControl : TmmPageControl;
begin
  aPrintBlackWhite := FALSE;

  if aOwner = nil then
    aOwner := Application;

  with TfrmPrintSettings.Create(aOwner) do
  try
    ShowClearerSettings := FALSE;

    PrinterSet.Orientation := aDefaultOrientation;

    PrinterSet.OrientationEnable := TRUE;
    PrinterSet.BlackWhiteEnable := TRUE;

    for x:= 0 to aOwner.ComponentCount-1 do begin
      if aOwner.Components[x] is TmmPageControl then begin
        xPageControl := TmmPageControl(aOwner.Components[x]);

        if xPageControl.ActivePage.Name =  'tsTable' then begin
          PrinterSet.BlackWhiteEnable := FALSE;
          PrinterSet.Portrait         := TRUE;
        end;
      end;
    end; // for x

    Result := (ShowModal = mrOK);

    aPrintBlackWhite := PrinterSet.PrintBlackWhite;
  finally
    Free;
  end;
end; //function GetPrintOptions
//------------------------------------------------------------------------------
end. //u_common_lib

