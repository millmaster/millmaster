�
 TSETPRINTOPTIONSDLG 0�  TPF0�TSetPrintOptionsDlgSetPrintOptionsDlgLeft�Top.ActiveControlbOKBorderStylebsDialogCaption(*)Druckoptionen fuer AusdruckClientHeight� ClientWidthhOldCreateOrderPositionpoScreenCenterPixelsPerInch`
TextHeight TmmPanel	MainPanelLeft Top WidthhHeightdAlignalClient
BevelOuterbvNoneTabOrder  TmmGroupBoxgbCurrentPrinterLeftTopWidth^HeightZAnchorsakLeftakTopakRight Caption(60)Aktueller DruckerTabOrder CaptionSpace	 TmmLabellaPrinternameLeft
TopWidthRHeightCaption(20)DruckernameVisible	AutoLabel.LabelPositionlpLeft  TmmLabellaOrientationLeft
Top(WidthMHeightCaption(20)SeitenformatVisible	AutoLabel.LabelPositionlpLeft  TmmLabellaCopiesLeft
Top<WidthRHeightCaption(20)KopienanzahlVisible	AutoLabel.LabelPositionlpLeft  TmmLabellaPrinternameValueLeftdTopWidthbHeightCaption(20)DruckernameFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible	AutoLabel.LabelPositionlpLeft  TmmLabellaOrientationValueLeftdTop(Width^HeightCaption(20)SeitenformatFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible	AutoLabel.LabelPositionlpLeft  TmmLabellaCopiesValueLeftdTop<WidthcHeightCaption(20)KopienanzahlFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.StylefsBold 
ParentFontVisible	AutoLabel.LabelPositionlpLeft  	TmmButtonbPrinterLeft	TopWidthKHeightAction	acPrinterParentShowHintShowHint	TabOrder Visible	AutoLabel.LabelPositionlpLeft    TmmPanel	paButtonsLeft TopdWidthhHeight!AlignalBottom
BevelOuterbvNoneTabOrder 	TmmButtonbOKLeftxTopWidthKHeightActionacOKAnchorsakTopakRight Caption(9)OKDefault	ParentShowHintShowHint	TabOrder Visible	AutoLabel.LabelPositionlpLeft  	TmmButtonbCancelLeft� TopWidthKHeightActionacCancelAnchorsakTopakRight Caption(9)AbbrechenParentShowHintShowHint	TabOrderVisible	AutoLabel.LabelPositionlpLeft  	TmmButtonbHelpLeftTopWidthKHeightActionacHelpAnchorsakTopakRight Caption	(9)&HilfeParentShowHintShowHint	TabOrderVisible	AutoLabel.LabelPositionlpLeft   TmmPrinterSetupDialogPrinterSetupDialog1LeftTop3  TmmActionListActionList1Left<Top4 TActionacOKCaption(12)&OKHint(*)Auswahl uebernehmen	OnExecuteacOKExecute  TActionacCancelCaption(12)&AbbrechenHint(*)Dialog abbrechenShortCut	OnExecuteacCancelExecute  TAction	acPrinterCaption(12)&DruckerHint#(*)Druckereinstellungen aufrufen...	OnExecuteacPrinterExecute  TActionacHelpCaption(12)&Hilfe...Hint(*)Hilfetext anzeigen...ShortCutp	OnExecuteacHelpExecute   TmmTranslatormmTranslatorDictionaryNameDictionary1Left� Top4TargetsData*Hint  *Caption      