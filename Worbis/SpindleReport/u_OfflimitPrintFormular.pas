(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_OfflimitPrintFormular.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 1506.2004   0.00   SDO  | file created
|=============================================================================*)
unit u_OfflimitPrintFormular;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, IvDictio, IvMulti, IvEMulti, mmTranslator, Qrctrls,
  mmQRSysData, mmQRImage, QuickRpt, mmQRLabel, mmQRBand, ExtCtrls,
  mmQuickRep, mmDBGrid, Db, mmDataSource, StdCtrls, Mask, DBCtrls,

  mmADODataSet, mmQRShape;

type
  TOfflimitPrintFormular = class(TmmForm)
    mmQuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitle: TmmQRLabel;
    qlCompanyName: TmmQRLabel;
    PageFooterBand1: TmmQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData1: TmmQRSysData;
    mmTranslator: TmmTranslator;
    qrbDetail: TmmQRBand;
    DataSource: TmmDataSource;
    qrOfflimitSetName: TmmQRLabel;
    mmQRLabel7: TmmQRLabel;
    qlRunningOfMachines: TmmQRLabel;
    mmQRLabel1: TmmQRLabel;
    mmQRLabel2: TmmQRLabel;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    mmQRLabel8: TmmQRLabel;
    mmQRLabel9: TmmQRLabel;
    mmQRLabel10: TmmQRLabel;
    mmQRLabel11: TmmQRLabel;
    mmQRLabel12: TmmQRLabel;
    mmQRLabel13: TmmQRLabel;
    mmQRLabel14: TmmQRLabel;
    mmQRLabel15: TmmQRLabel;
    mmQRLabel16: TmmQRLabel;
    mmQRLabel17: TmmQRLabel;
    mmQRLabel18: TmmQRLabel;
    mmQRLabel19: TmmQRLabel;
    mmQRLabel20: TmmQRLabel;
    mmQRLabel21: TmmQRLabel;
    mmQRLabel22: TmmQRLabel;
    mmQRLabel23: TmmQRLabel;
    mmQRShape1: TmmQRShape;
    mmQRShape2: TmmQRShape;
    mmQRBand1: TmmQRBand;
    mmQRLabel24: TmmQRLabel;
    qrlActive2Value: TmmQRLabel;
    qrlActive1Value: TmmQRLabel;
    qrlBaseValue: TmmQRLabel;
    qrlTimeValue: TmmQRLabel;
    qrlMEff_OG_Abs_Value: TmmQRLabel;
    qrlMEff_UG_Proc_Value: TmmQRLabel;
    qrlLongStopRangeValue: TmmQRLabel;
    qrlLongStopCountValue: TmmQRLabel;
    qrlMEff_OG_Proc_Value: TmmQRLabel;
    qrlMEff_UG_Abs_Value: TmmQRLabel;
    qrl_1_Value: TmmQRLabel;
    qrl_10_Value: TmmQRLabel;
    qrl_11_Value: TmmQRLabel;
    qrl_12_Value: TmmQRLabel;
    qrl_13_Value: TmmQRLabel;
    qrl_14_Value: TmmQRLabel;
    qrl_15_Value: TmmQRLabel;
    qrl_16_Value: TmmQRLabel;
    qrl_20_Value: TmmQRLabel;
    qrl_21_Value: TmmQRLabel;
    qrl_22_Value: TmmQRLabel;
    qrl_23_Value: TmmQRLabel;
    qrl_24_Value: TmmQRLabel;
    qrl_25_Value: TmmQRLabel;
    qrl_26_Value: TmmQRLabel;

    procedure mmQuickRep1NeedData(Sender: TObject; var MoreData: Boolean);
  private
    { Private declarations }
    mDataSet: TmmADODataSet;
    mCounter : Integer;
    mNumOfMachines : String;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent; aDataSet: TmmADODataSet; aTitle: string); reintroduce; virtual;
    procedure Execute;
  end;

var
  OfflimitPrintFormular: TOfflimitPrintFormular;

implementation

{$R *.DFM}




uses SettingsReader, Printers, MMUGlobal;


resourcestring

   rsActiv    = '(*)Aktiv';  //ivlm
   rsInActiv  = '(*)Inaktiv'; //ivlm

{ TfOfflimitPrintList }

constructor TOfflimitPrintFormular.Create(aOwner: TComponent;
  aDataSet: TmmADODataSet; aTitle: string);
begin
 inherited Create(aOwner);
 if aTitle <> '' then
    laTitle.Caption := aTitle;


  mmQuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
  mmQuickRep1.Page.Orientation := Printer.Orientation;

  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;

  mDataSet :=  aDataSet;

  mNumOfMachines:= Translate(qlRunningOfMachines.caption);
  mNumOfMachines:= StringReplace(mNumOfMachines, '"?"~', '%d ', [rfReplaceAll]);


  //Pos. shape setzen

  mmQRShape1.Left  := 0;
  mmQRShape1.Width := qrbDetail.Width;
  mmQRShape1.Top   := mmQRLabel7.Top - 1;

  mmQRShape2.Left  := mmQRLabel15.Left - 10;

  mmQRShape2.Width := qrbDetail.Width - mmQRShape2.Left;
  mmQRShape2.Top   := mmQRLabel16.Top -1 ;

  mCounter := 1;
end;

procedure TOfflimitPrintFormular.Execute;
var
  xLastPos: TBookmark;
begin
  xLastPos := mDataSet.GetBookmark;
  mDataSet.DisableControls;
  mDataSet.First;
//  mmQuickRep1.Prepare;
  mmQuickRep1.Print;

  mDataSet.GotoBookmark(xLastPos);
  mDataSet.FreeBookmark(xLastPos);
  mDataSet.EnableControls;
end;

procedure TOfflimitPrintFormular.mmQuickRep1NeedData(Sender: TObject;
  var MoreData: Boolean);
var xText  : String;
    xValue : Integer;
begin
  inherited;

  with mDataSet do begin
     qrOfflimitSetName.caption:=  FieldByName('c_maoffset_name').AsString;
     qlRunningOfMachines.caption:= Format(mNumOfMachines, [FieldByName('c_number_of_machines').AsInteger]);

     //Allg Info

     if FieldByName('c_OffBorderByFixValues').AsBoolean then
       qrlActive1Value.Caption := rsActiv
     else
       qrlActive1Value.Caption := rsInActiv;

     qrlBaseValue.Caption :=  cLenBaseTxt[FieldByName('c_Base').AsInteger].Text;



     if FieldByName('c_OffBorderByAverage').AsBoolean then
       qrlActive2Value.Caption := rsActiv
     else
       qrlActive2Value.Caption := rsInActiv;

     qrlTimeValue.Caption := FieldByName('c_Average_time').AsString;


    mmQRLabel15.Caption := Format(mmQRLabel15.Caption, [cLenBaseTxt[FieldByName('c_Base').AsInteger].Text]);
    mmQRLabel16.Caption := Format(mmQRLabel16.Caption, [cLenBaseTxt[FieldByName('c_Base').AsInteger].Text]);


    //Grenzwerte

    xText := FieldByName('c_Eff_min').AsString;
    if xText <> '' then
      qrlMEff_UG_Abs_Value.Caption := xText
    else
      qrlMEff_UG_Abs_Value.Caption := '-';

    xText := FieldByName('c_Eff_max').AsString;
    if xText <> '' then
      qrlMEff_OG_Abs_Value.Caption := xText
    else
      qrlMEff_OG_Abs_Value.Caption := '-';

    xText := FieldByName('c_Eff_PC_min').AsString;
    if xText <> '' then
      qrlMEff_UG_Proc_Value.Caption := xText
    else
      qrlMEff_UG_Proc_Value.Caption := '-';

    xText := FieldByName('c_Eff_PC_max').AsString;
    if xText <> '' then
      qrlMEff_OG_Proc_Value.Caption := xText + ' %'
    else
      qrlMEff_OG_Proc_Value.Caption := '-';




    xText := FieldByName('c_tLSt_max').AsString;
    if xText <> '' then
      qrlLongStopRangeValue.Caption := Format( '%d',[FieldByName('c_tLSt_max').AsInteger] )
    else
      qrlLongStopRangeValue.Caption := '-';     

    xText := FieldByName('c_LSt_max').AsString;
    if xText <> '' then
      qrlLongStopCountValue.Caption := Format( '%d',[FieldByName('c_LSt_max').AsInteger] )
    else
      qrlLongStopCountValue.Caption := '-';



    xText := FieldByName('c_CYTot_min').AsString;
    if xText <> '' then
      qrl_1_Value.Caption := Format( '%d',[FieldByName('c_CYTot_min').AsInteger] )
    else
      qrl_1_Value.Caption := '-';

    xText := FieldByName('c_CYTot_max').AsString;
    if xText <> '' then
      qrl_10_Value.Caption := Format( '%d',[FieldByName('c_CYTot_max').AsInteger] )
    else
      qrl_10_Value.Caption := '-';

    xText := FieldByName('c_CSIRO_max').AsString;
    if xText <> '' then
      qrl_11_Value.Caption := Format( '%d',[FieldByName('c_CSIRO_max').AsInteger] )
    else
      qrl_11_Value.Caption := '-';

    xText := FieldByName('c_CSp_max').AsString;
    if xText <> '' then
      qrl_12_Value.Caption := Format( '%d',[FieldByName('c_CSp_max').AsInteger] )
    else
      qrl_12_Value.Caption := '-';

    xText := FieldByName('c_RSp_max').AsString;
    if xText <> '' then
      qrl_13_Value.Caption := Format( '%d',[FieldByName('c_RSp_max').AsInteger] )
    else
      qrl_13_Value.Caption := '-';

    xText := FieldByName('c_CBu_max').AsString;
    if xText <> '' then
      qrl_14_Value.Caption := Format( '%d',[FieldByName('c_CBu_max').AsInteger] )
    else
      qrl_14_Value.Caption := '-';

    xText := FieldByName('c_CUpY_max').AsString;
    if xText <> '' then
      qrl_15_Value.Caption := Format( '%d',[FieldByName('c_CUpY_max').AsInteger] )
    else
      qrl_15_Value.Caption := '-';

    xText := FieldByName('c_YB_max').AsString;
    if xText <> '' then
      qrl_16_Value.Caption := Format( '%d',[FieldByName('c_YB_max').AsInteger] )
    else
      qrl_16_Value.Caption := '-';


    xText := FieldByName('c_CYTot_PC_max').AsString;
    if xText <> '' then
      qrl_20_Value.Caption := xText + ' %'
    else
      qrl_20_Value.Caption := '-';

    xText := FieldByName('c_CSIRO_PC_max').AsString;
    if xText <> '' then
      qrl_21_Value.Caption := xText + ' %'
    else
      qrl_21_Value.Caption := '-';

    xText := FieldByName('c_CSp_PC_max').AsString;
    if xText <> '' then
      qrl_22_Value.Caption := xText + ' %'
    else
      qrl_22_Value.Caption := '-';

    xText := FieldByName('c_RSp_PC_max').AsString;
    if xText <> '' then
      qrl_23_Value.Caption := xText + ' %'
    else
      qrl_23_Value.Caption := '-';

    xText := FieldByName('c_CBu_PC_max').AsString;
    if xText <> '' then
      qrl_24_Value.Caption := xText + ' %'
    else
      qrl_24_Value.Caption := '-';

    xText := FieldByName('c_CUpY_PC_max').AsString;
    if xText <> '' then
      qrl_25_Value.Caption := xText + ' %'
    else
      qrl_25_Value.Caption := '-';

    xText := FieldByName('c_YB_PC_max').AsString;
    if xText <> '' then
      qrl_26_Value.Caption := xText + ' %'
    else
      qrl_26_Value.Caption := '-';



  end;

  qrbDetail.Frame.DrawBottom := odd(mCounter);

  inc(mCounter);

  MoreData := not mDataSet.EOF;

{  if then
     MoreData := False
  else
    MoreData := TRUE;
}

  mDataSet.Next;
end;

end.
