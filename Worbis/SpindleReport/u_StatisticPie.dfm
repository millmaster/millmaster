inherited fStatisticPie: TfStatisticPie
  Left = 369
  Top = 187
  Width = 639
  Height = 460
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSizeToolWin
  Caption = '(*)Kuchen'
  FormStyle = fsStayOnTop
  KeyPreview = True
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object mmChart1: TmmChart
    Left = 0
    Top = 0
    Width = 631
    Height = 393
    AllowPanning = pmNone
    AllowZoom = False
    AnimatedZoom = True
    AnimatedZoomSteps = 10
    BackWall.Brush.Color = clWhite
    BackWall.Brush.Style = bsClear
    BackWall.Pen.Visible = False
    Foot.Alignment = taLeftJustify
    Title.Font.Charset = DEFAULT_CHARSET
    Title.Font.Color = clBlack
    Title.Font.Height = -21
    Title.Font.Name = 'Arial'
    Title.Font.Style = []
    Title.Text.Strings = (
      '0')
    AxisVisible = False
    Chart3DPercent = 40
    ClipPoints = False
    Frame.Visible = False
    LeftAxis.Visible = False
    Legend.Alignment = laBottom
    Legend.ColorWidth = 15
    Legend.HorizMargin = 1
    Legend.ShadowSize = 0
    Legend.TextStyle = ltsRightPercent
    View3DOptions.Elevation = 315
    View3DOptions.Orthogonal = False
    View3DOptions.Perspective = 0
    View3DOptions.Rotation = 360
    View3DOptions.ZoomText = False
    View3DWalls = False
    Align = alClient
    BevelOuter = bvNone
    ParentShowHint = False
    ShowHint = False
    TabOrder = 0
    AutoSize = True
    MouseMode = mmNormal
    object PieSeries: TPieSeries
      Marks.ArrowLength = 12
      Marks.BackColor = 13434879
      Marks.Style = smsLegend
      Marks.Visible = True
      PercentFormat = '##0.# %'
      SeriesColor = clRed
      Title = 'PieSeries'
      ValueFormat = '#,##0.#'
      OtherSlice.Text = 'Other'
      PieValues.DateTime = False
      PieValues.Name = 'Pie'
      PieValues.Multiplier = 1
      PieValues.Order = loNone
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 393
    Width = 631
    Height = 40
    Align = alBottom
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 1
    OnCanResize = mmPanel1CanResize
    object mmBitBtn1: TmmBitBtn
      Left = 510
      Top = 7
      Width = 110
      Height = 27
      Action = acClose
      Cancel = True
      Caption = '(9)Abbrechen'
      TabOrder = 0
      Visible = True
      Glyph.Data = {
        DE010000424DDE01000000000000760000002800000024000000120000000100
        0400000000006801000000000000000000001000000000000000000000000000
        80000080000000808000800000008000800080800000C0C0C000808080000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333333333333333333333333000033338833333333333333333F333333333333
        0000333911833333983333333388F333333F3333000033391118333911833333
        38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
        911118111118333338F3338F833338F3000033333911111111833333338F3338
        3333F8330000333333911111183333333338F333333F83330000333333311111
        8333333333338F3333383333000033333339111183333333333338F333833333
        00003333339111118333333333333833338F3333000033333911181118333333
        33338333338F333300003333911183911183333333383338F338F33300003333
        9118333911183333338F33838F338F33000033333913333391113333338FF833
        38F338F300003333333333333919333333388333338FFF830000333333333333
        3333333333333333333888330000333333333333333333333333333333333333
        0000}
      NumGlyphs = 2
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 20
    Top = 20
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Title'
        0))
  end
  object mmActionList1: TmmActionList
    Left = 72
    Top = 24
    object acClose: TAction
      Caption = '(9)Abbrechen'
      OnExecute = acCloseExecute
    end
  end
end
