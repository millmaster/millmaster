(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_def_offlimit_filter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Auswahl-Dialog fuer Offlimit-Bericht
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 28.04.99 | 0.00 | PW  | Datei erstellt
| 29.10.99 | 0.01 | PW  | TToolButton/TToolBar replaced by TButton
| 29.08.01 | 0.01 | Wss | - Initialize and Ok/Cancel buttons improved
                          - set Focus on edit field
| 18.02.04 | 0.01 | Wss | Italienisch hatte zuwenig Platz
| 18.02.04 | 0.00 | Wss | verwende rsHourShort f�r EditFields PositivMask
|=========================================================================================*)

{$I symbols.inc}

unit u_def_offlimit_filter;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, StdCtrls, mmLabel, mmGroupBox, ExtCtrls, mmPanel, ComCtrls,
  ToolWin, mmToolBar, IvDictio, IvMulti, IvEMulti, ActnList, mmActionList,
  NumCtrl, mmButton, mmCheckBox, mmTranslator, BASEFORM;

type
  TDefOfflimitReportDlg = class(TmmForm)
    ActionList1: TmmActionList;
    acHelp: TAction;
    paOnline: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    Label1: TmmLabel;
    cbOutOfProdSpindles: TmmCheckBox;
    edOnlineOfflimitMin: TNumEdit;
    paOffline: TmmPanel;
    GroupBox1: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    edOfflineOfflimitMin: TNumEdit;
    edOfflimitRatio: TNumEdit;

    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    mmTranslator: TmmTranslator;

    procedure acHelpExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
  public
    procedure InitOnline(aOfflimitTime: integer; aShowOutOfProdSpindles: boolean);
    procedure InitOffline(aOfflimitTime: integer; aOfflimitRatio: double);
  end;                                  //TDefOfflimitReportDlg

var
  DefOfflimitReportDlg: TDefOfflimitReportDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  u_global, u_lib;

{$R *.DFM}
//-----------------------------------------------------------------------------
procedure TDefOfflimitReportDlg.InitOnline(aOfflimitTime: integer; aShowOutOfProdSpindles: boolean);
begin
  paOnline.Visible := True;
  ClientHeight := paOnline.Height + paButtons.Height;
  HelpContext := paOffline.HelpContext;

  edOnlineOfflimitMin.Value := (aOfflimitTime div 60);
  cbOutOfProdSpindles.Checked := aShowOutOfProdSpindles;
end;                                    //procedure TDefOfflimitReportDlg.Init
//-----------------------------------------------------------------------------
procedure TDefOfflimitReportDlg.InitOffline(aOfflimitTime: integer; aOfflimitRatio: double);
begin
  paOffline.Visible := True;
  ClientHeight := paOffline.Height + paButtons.Height;
  HelpContext := paOnline.HelpContext;

  edOfflineOfflimitMin.Value := (aOfflimitTime div 60);
  edOfflimitRatio.Value := aOfflimitRatio;
end;                                    //procedure TDefOfflimitReportDlg.Init
//-----------------------------------------------------------------------------
procedure TDefOfflimitReportDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, Self.HelpContext);
end;                                    //procedure TDefOfflimitReportDlg.acHelpExecute
//------------------------------------------------------------------------------
procedure TDefOfflimitReportDlg.FormShow(Sender: TObject);
begin
  if paOnline.Visible then
    edOnlineOfflimitMin.SetFocus
  else
    edOfflineOfflimitMin.SetFocus;
end;
//------------------------------------------------------------------------------
procedure TDefOfflimitReportDlg.bOKClick(Sender: TObject);
begin
  inherited;
  bOK.SetFocus;                         // force edit field to update its value -> only on OnExit event!
end;

procedure TDefOfflimitReportDlg.FormCreate(Sender: TObject);
begin
  paOffline.HelpContext := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Online-Offlimit-Filter-bearbeiten.htm');
  paOnline.HelpContext  := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Offline-Offlimit-Filter.htm');
  edOnlineOfflimitMin.Masks.PositiveMask := ',0 ' + rsHourShort;
  edOfflineOfflimitMin.Masks.PositiveMask := ',0 ' + rsHourShort;
end;

end. //u_def_offlimit_filter.pas

