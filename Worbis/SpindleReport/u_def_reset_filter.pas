(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_def_reset_filter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: filter definition to reset offlimit spindles
| Info..........: -
| Develop.system: Windows NT 5.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01, Multilizer, Infopower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 16.08.01 | 0.00 | PW  | file created
| 18.02.04 | 0.00 | Wss | verwende rsHourShort f�r EditFields PositivMask
|=========================================================================================*)

{$i symbols.inc}

unit u_def_reset_filter;

interface

uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, StdCtrls, mmLabel, mmGroupBox, mmButton, ExtCtrls,
  mmPanel, IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList,
  mmActionList, NumCtrl, mmComboBox;

type
  TfrmDefResetFilter = class(TmmForm)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    mmTranslator: TmmTranslator;
    mmPanel2: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;
    mmPanel1: TmmPanel;
    GroupBox1: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    edRuntime: TNumEdit;
    edRatio: TNumEdit;
    comRuntime: TmmComboBox;
    comRatio: TmmComboBox;
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure acHelpExecute(Sender: TObject);
  private
  public
    constructor Create(aOwner: TComponent; aResetFilter: rResetFilterT); reintroduce; virtual; 
    function GetFilter: rResetFilterT;
  end; //TfrmDefResetFilter

var
  frmDefResetFilter: TfrmDefResetFilter;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main, u_lib;

{$R *.DFM}

constructor TfrmDefResetFilter.Create(aOwner: TComponent; aResetFilter: rResetFilterT);
begin
  inherited Create(aOwner);

  comRuntime.ItemIndex := ord(aResetFilter.RuntimeCompare);
  comRatio.ItemIndex := ord(aResetFilter.RatioCompare);

  edRuntime.Masks.PositiveMask := ',0 ' + rsHourShort;
  edRuntime.Value              := aResetFilter.RuntimeValue;
  edRatio.Value                := aResetFilter.RatioValue;
end; //constructor TfrmDefResetFilter.Create
//-----------------------------------------------------------------------------
procedure TfrmDefResetFilter.acOKExecute(Sender: TObject);
begin
  inherited;

  if (edRuntime.AsInteger = 0) and (edRatio.AsInteger = 0)
    then ModalResult := mrCancel
    else ModalResult := mrOK;
end; //procedure TfrmDefResetFilter.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmDefResetFilter.acCancelExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end; //procedure TfrmDefResetFilter.acCancelExecute
//-----------------------------------------------------------------------------
function TfrmDefResetFilter.GetFilter: rResetFilterT;
begin
  result.RunTimeValue := edRuntime.AsInteger;
  result.RatioValue := edRatio.AsInteger;
  result.RatioCompare := eCompareT(comRatio.ItemIndex);
  result.RuntimeCompare := eCompareT(comRuntime.ItemIndex);
end; //function TfrmDefResetFilter.GetFilter
//-----------------------------------------------------------------------------
procedure TfrmDefResetFilter.FormKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle,WM_NEXTDLGCTL,0,0);
  end; //if Key = #13 then
end; //procedure TfrmDefResetFilter.FormKeyPress
//-----------------------------------------------------------------------------
procedure TfrmDefResetFilter.acHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, HelpContext)
end; //procedure TfrmDefResetFilter.acHelpExecute

end. //u_def_reset_filter
