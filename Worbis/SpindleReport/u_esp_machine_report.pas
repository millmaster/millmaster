(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_esp_machine_report.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.04.99 | 0.00 | PW  | File created
| 01.10.99 | 0.01 | PW  | mmGraph.TMemoINI to read/write settings
| 11.02.00 | 1.00 | Mg  | PEff = tRu / tWa in qu1.CalcFields
| 23.02.00 | 1.01 | Mg  | TEsp_MachineReportDlg.qu1CalcFields imperfections inserted
| 05.05.00 | 1.02 | PW  | using TmSettingsDialog for grid and graph options
| 14.06.00 | 1.03 | SDo | Aenderungen in Proc. PrepareReport.
|                       | Von "LStxy" sind nur noch "LSt" und "LSt Prod" sichtbar
| 15.06.00 | 1.04 | Mg  | DBGrid1DblClick : Exit wenn Spindle ID = 0
| 07.07.00 | 1.05 | SDo | Save WinPos -> RestoreFormLayout, SaveFormLayout()
|                       | ShowDialog: Graphic-YAxis-param to mParameter
| 07.07.00 | 1.06 | SDo | Imperfections NICHT mal 1000 rechnen   [ qu1CalcFields(DataSet: TDataSet)]
| 08.08.00 | 1.07 | Mg  | RestoreFormLayout in FormCreate and constructor deleted
| 05.09.00 | 1.08 | Mg  | ISmall div 1000
| 12.09.00 | 1.09 | Mg  | Work around
| 06.11.00 | 1.09 | Wss | MemoIni.Applicationname set to gApplicationname to solve 8.3 naming problems
| 18.08.01 | 1.09 | PW  | footer cells with sum and avg values for dbgrid1 added
| 23.08.01 |      | PW  | printing grid using wwQRPrintDBGrid component
| 30.08.01 |      | Wss | - DBGrid1UpdateFooter: if result set is empty calculations for
                            imperfections failed (variant conversion error)
                          - Init: Select machine dialog shows now only machines with valid nodes
| 29.10.01 | 1.10 | Wss | qu1 contained a statement AND pg.c_style_id <> 2 -> removed (??)
| 08.11.01 | 1.11 | Wss | calculation for qu1calc_PCSp %CSp corrected: base is SP and not CYTot
| 12.11.01 | 1.11 | Wss | calculation for qu1calc_SFI corrected
| 22.11.01 | 1.11 | Wss | MemoryLeak: object mParameters was overriden at reading parameters
                          mParameters := TParameters.Create;
                          mParameters := ReadParam(); <-- forbidden !!!!
| 26.11.01 | 1.11 | Wss | base of Bob, Cones values changed to float, without factor 100
| 13.12.01 | 1.15 | Wss | calculation SFI/D and SFI improved/added
| 14.02.02 | 1.16 | Wss | Fehler in Footerline berechung: c_AdjustBase Feld war nicht in Query vorhanden
| 15.03.02 | 1.16 | Wss | qu1, qrySum umgestellt auf Views um Faktor fuer Imperfektionen zu verwenden
| 17.10.02 |      | LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 15.11.02 | 1.16 | Wss | Kleinere Fixes
| 04.12.02 | 1.16 | Wss | In acSettingsExecute() wird gepr�ft, ob eine g�ltige ProdID vorhanden
                          ist (<> 0). Wenn ja, werden die Settings von dieser Partie gezeigt,
                          wenn nein, alle Settings von der Maschine
| 02.10.03 | 1.16 | Wss | - LStProd wird nun im hh:mm Format ausgegeben
                          - LoadProfile: DisplayFormat f�r nLSt wird fix auf '#0' gesetzt
| 25.01.05 | 1.17   SDo | Umbau auf XML (MM-Vers. 5.0) -> in Func. TEsp_MachineReportDlg.acSettingsExecute Erweiterung Machinename
| 18.03.05 | 1.16 | Wss | Maschinenmodell wird nicht mehr ben�tigt -> �ber cMachineTypeArr[c_machine_type] gel�st
| 10.06.05 | 1.16 | Wss | - Spectra+ und Zenit Elemente hinzugef�gt
                          - Korrektur f�r Locks welche immer absolut angezeigt werden
| 18.02.08 | 1.20 | Nue | Einbau von VCV
|=========================================================================================*)

{$I symbols.inc}

unit u_esp_machine_report;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, BaseForm, Dialogs,
  StdCtrls, ComCtrls, ToolWin, Grids, DBGrids, Db, ExtCtrls, Menus, ActnList,
  IvDictio, IvMulti, IvEMulti, ImgList, mmGraphGlobal,
  mmGraph, Wwdatsrc, Wwdbigrd, Wwdbgrid, u_global, mmActionList,
  mmPageControl, mmPanel, mmToolBar, mmGridPos, mmPopupMenu, mmTranslator,
  mmDictionary, MMSecurity, MemoINI, mmLabel, mmLengthUnit, Buttons, mmSpeedButton,
  Printers, u_dmSpindleReport, ADODB, mmADOCommand, mmADODataSet;

type
  TEsp_MachineReportDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bSettings: TToolButton;
    ToolButton10: TToolButton;
    bHelp: TToolButton;
    bPrintRange: TToolButton;
    bEditGridOptions: TToolButton;
    bPeriod: TToolButton;
    bFormat: TToolButton;
    bSort: TToolButton;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    mmSpeedButton1: TmmSpeedButton;
    ToolButton4: TToolButton;

    ActionList1: TmmActionList;
    acClose: TAction;
    acHelp: TAction;
    acSettings: TAction;
    acPeriod: TAction;
    acPrint: TAction;
    acEditGridOptions: TAction;
    acFormat: TAction;
    acSortGraph: TAction;
    acSaveAsDefault: TAction;
    acSaveProfile: TAction;
    acLoadProfile: TAction;
    acDeleteProfile: TAction;
    acSecurity: TAction;

    qu1Source: TwwDataSource;
    qu1: TmmADODataSet;
    qu1c_spindle_id: TSmallintField;
    qu1c_Len: TFloatField;
    qu1c_Wei: TFloatField;
    qu1c_Bob: TFloatField;
    qu1c_Cones: TFloatField;
    qu1calc_Red: TFloatField;
    qu1calc_Sp: TFloatField;
    qu1calc_RSP: TFloatField;
    qu1calc_YB: TFloatField;
    qu1calc_CS: TFloatField;
    qu1calc_CL: TFloatField;
    qu1calc_CT: TFloatField;
    qu1calc_CN: TFloatField;
    qu1calc_CSP: TFloatField;
    qu1calc_CClS: TFloatField;
    qu1calc_UCLS: TFloatField;
    qu1calc_UCLL: TFloatField;
    qu1calc_UCLT: TFloatField;
    qu1calc_COFFCNT: TFloatField;
    qu1calc_CBU: TFloatField;
    qu1calc_CDBU: TFloatField;
    qu1calc_CSYS: TFloatField;
    qu1calc_CUPY: TFloatField;
    qu1calc_CYTOT: TFloatField;
    qu1calc_CSIRO: TFloatField;
    qu1calc_MEff: TFloatField;
    qu1calc_PEff: TFloatField;
    qu1calc_PCSp: TFloatField;
    qu1calc_PRSp: TFloatField;
    qu1calc_npProdGrp: TIntegerField;
    qu1calc_LStTOT: TIntegerField;
    qu1calc_ManSt: TFloatField;
    qu1calc_INeps: TFloatField;
    qu1calc_IThick: TFloatField;
    qu1calc_IThin: TFloatField;
    qu1calc_ISmall: TFloatField;
    qu1calc_I2_4: TFloatField;
    qu1calc_I4_8: TFloatField;
    qu1calc_I8_20: TFloatField;
    qu1calc_I20_70: TFloatField;
    qu1calc_SFI_D: TFloatField;
////////////    qu1calc_CVd: TFloatField;     /////Nue:30.6.08
    qu1calc_CSFI: TFloatField;
    qu1calc_CSIROCl: TFloatField;
    qu1c_UClT: TFloatField;
    qu1c_UClL: TFloatField;
    quMach: TmmADODataSet;
    quMachc_machine_name: TStringField;
    quMachc_nr_of_spindles: TSmallintField;
    quMachc_machine_id: TSmallintField;
    quProd: TmmADODataSet;
    quProdc_prod_id: TIntegerField;
    quProdc_style_id: TSmallintField;
    quProdc_order_id: TSmallintField;
    quProdc_YM_set_id: TSmallintField;
    quProdc_spindle_first: TSmallintField;
    quProdc_spindle_last: TSmallintField;
    quProdc_order_name: TStringField;
    quProdc_order_description: TStringField;
    quProdc_style_name: TStringField;
    quGetProdID: TmmADODataSet;

    mmLengthUnits1: TmmLengthUnits;
    mmGridPos1: TmmGridPos;

    PageControl1: TmmPageControl;
    tsGraph: TTabSheet;
    tsTable: TTabSheet;
    mmGraph1: TmmGraph;
    DBGrid1: TwwDBGrid;
    Panel1: TmmPanel;
    laPeriod: TmmLabel;
    laPeriodValue: TmmLabel;
    laDuration: TmmLabel;
    laDurationValue: TmmLabel;

    pmProfile: TmmPopupMenu;
    miSaveAsDefault: TMenuItem;
    miSaveProfile: TMenuItem;
    N1: TMenuItem;
    miLoadProfile: TMenuItem;
    miDeleteProfile: TMenuItem;
    N2: TMenuItem;
    miEditGridOptions: TMenuItem;

    MMSecurityControl: TMMSecurityControl;
    qrySum: TmmADODataSet;
    mmTranslator: TmmTranslator;
    qu1calc_SFI: TFloatField;
    qu1c_AdjustBase: TFloatField;
    qu1c_INeps: TFloatField;
    qu1c_IThick: TFloatField;
    qu1c_IThin: TFloatField;
    qu1c_ISmall: TFloatField;
    qu1c_LStProd: TFloatField;
    qu1c_rtSpd: TFloatField;
    qu1c_otSpd: TFloatField;
    qu1c_wtSpd: TFloatField;
    qu1c_Red: TFloatField;
    qu1c_tRed: TFloatField;
    qu1c_tYell: TFloatField;
    qu1c_ManSt: TFloatField;
    qu1c_tManSt: TFloatField;
    qu1c_Sp: TFloatField;
    qu1c_RSp: TFloatField;
    qu1c_YB: TFloatField;
    qu1c_LSt: TFloatField;
    qu1c_LStBreak: TFloatField;
    qu1c_LStMat: TFloatField;
    qu1c_LStRev: TFloatField;
    qu1c_LStClean: TFloatField;
    qu1c_LStDef: TFloatField;
    qu1c_CS: TFloatField;
    qu1c_CL: TFloatField;
    qu1c_CT: TFloatField;
    qu1c_CN: TFloatField;
    qu1c_CSp: TFloatField;
    qu1c_CClS: TFloatField;
    qu1c_UClS: TFloatField;
    qu1c_COffCnt: TFloatField;
    qu1c_LckOffCnt: TFloatField;
    qu1c_CBu: TFloatField;
    qu1c_CDBu: TFloatField;
    qu1c_CSys: TFloatField;
    qu1c_LckSys: TFloatField;
    qu1c_CUpY: TFloatField;
    qu1c_CYTot: TFloatField;
    qu1c_CSIRO: TFloatField;
    qu1c_LckSIRO: TFloatField;
    qu1c_I2_4: TFloatField;
    qu1c_I4_8: TFloatField;
    qu1c_I8_20: TFloatField;
    qu1c_I20_70: TFloatField;
    qu1c_Sfi: TFloatField;
    qu1c_SFiCnt: TFloatField;
    qu1c_CSFI: TFloatField;
    qu1c_LckSFI: TFloatField;
    qu1c_LckClS: TFloatField;
    qu1c_LckSIROCl: TFloatField;
    qu1c_CSIROCL: TFloatField;
    qu1c_AdjustBaseCnt: TFloatField;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    quMachc_machine_type: TSmallintField;
    qu1calc_COffCntPlus: TFloatField;
    qu1calc_COffCntMinus: TFloatField;
    qu1calc_CShortOffCnt: TFloatField;
    qu1calc_CShortOffCntPlus: TFloatField;
    qu1calc_CShortOffCntMinus: TFloatField;
    qu1calc_CP: TFloatField;
    qu1calc_CClL: TFloatField;
    qu1calc_CClT: TFloatField;
    qu1c_COffCntPlus: TBCDField;
    qu1c_COffCntMinus: TBCDField;
    qu1c_CShortOffCnt: TBCDField;
    qu1c_CShortOffCntPlus: TBCDField;
    qu1c_CShortOffCntMinus: TBCDField;
    qu1c_CP: TBCDField;
    qu1c_CClL: TBCDField;
    qu1c_CClT: TBCDField;
    qu1c_LckP: TBCDField;
    qu1c_LckClL: TBCDField;
    qu1c_LckClT: TBCDField;
    qu1c_LckShortOffCnt: TBCDField;
    qu1c_LckNSLT: TBCDField;
    qu1calc_CVd: TFloatField;
    qu1c_LckVCV: TFloatField;
    qu1calc_CVCV: TFloatField;
    qu1c_CVCV: TFloatField;  ////////Nue:30.6.08

    procedure acCloseExecute(Sender: TObject);
    procedure acDeleteProfileExecute(Sender: TObject);
    procedure acEditGridOptionsExecute(Sender: TObject);
    procedure acFormatExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acLoadProfileExecute(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSaveAsDefaultExecute(Sender: TObject);
    procedure acSaveProfileExecute(Sender: TObject);
    procedure acSettingsExecute(Sender: TObject);
    procedure acSortGraphExecute(Sender: TObject);
    procedure DBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure mmGraph1GraphDblClick(Sender: TObject; AItem: eDblClickT;
      ARow, APoint: Integer; AStatistik: eStatistikT);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure qu1c_rtSpdGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure qu1CalcFields(DataSet: TDataSet);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure DBGrid1UpdateFooter(Sender: TObject);
    procedure qu1BeforeOpen(DataSet: TDataSet);
    procedure qu1c_LStProdGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    mRepInfo: rESPReportInfoT;
    mPeriod: rPeriodInfoT;
    mParaList: rEspParameterArrayT;
    mParameter: TParameterList;
    procedure GetGridDisplay;
    procedure LoadProfile(aUsername, aProfilename: string);
    procedure ReadData;
    procedure RunQuery;
    procedure SetGraphParameters;
    procedure ShowSpindleDetails;
    procedure TextToTranslate;
    procedure UpdateCaption;
    procedure ShowDialog(aGrid: TwwDBGrid);
    procedure PrepareReport(aDataset: TDataset);
    function GetConvertFactor(aDataset: TDataset): double;
  public
    function Init(aMachines: string; aProdID: integer): Boolean;
  end; //TEsp_MachineReportDlg

var
  Esp_MachineReportDlg: TEsp_MachineReportDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, MMHtmlHelp,

  mmLib, u_main, u_lib, u_common_global, u_resourcestrings, u_common_lib,
  u_reiniger_settings, u_SettingsDialog, u_print_list, u_esp_spindle_report,
  u_select_interval, u_select_prodid, LoepfeGlobal, mmDialogs, SettingsReader,
  MMUGlobal, BaseGlobal;

{$R *.DFM}

resourcestring
  cCaptionStr1 = '(50)Maschinenbericht: [%s   %s - %s]'; //ivlm
 // Mg cGraphTitleStr1     = '(100)Maschine: %s-%s    Auftrag: %s    Artikel: %s'; //ivlm
  cGraphTitleStr1a = '(100)Maschine: %s-%s    Spulstelle: %d-%d'; //ivlm
  cXAxisStr = '(40)Spulstellenpositionen'; //ivlm
  cSelectMachineStr = '(40)Maschine auswaehlen'; //ivlm
  cMachineNotFoundMsg = '(80)Machine "%s" nicht vorhanden !'; //ivlm
  cTotalStr = '(10)Total'; //ivlm

const
  cDateFrmStr = 'd/m/yyyy  hh:nn';
// cRegRootMachineRep  = cRegMillMasterPath + '\Esp_MachineReportDlg';

procedure TEsp_MachineReportDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEsp_MachineReportDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acDeleteProfileExecute(Sender: TObject);
begin
  inherited;
  mmGraph1.MemoINI.DeleteProfileDlg;
end; //procedure TEsp_MachineReportDlg.acDeleteProfileExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acEditGridOptionsExecute(Sender: TObject);
var
  xParameter: TParameterList;
begin
  PrepareReport(qu1);
// mParameter := ReadParam(qu1,mParaList,mmGraph1.Bars.Style); //re-read pos-changes from TDataSet
//  mParameter := ReadParam(qu1, mParaList, mmGraph1); //re-read pos-changes from TDataSet

  xParameter := ReadParam(qu1, mParaList, mmGraph1);
  mParameter.Assign(xParameter);
  xParameter.Free;

//  mParameter.Assign(ReadParam(qu1, mParaList, mmGraph1)); //re-read pos-changes from TDataSet
  ShowDialog(DBGrid1);
end; //procedure TEsp_MachineReportDlg.acEditGridOptionsExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acFormatExecute(Sender: TObject);
begin
  inherited;
  mmLengthUnits1.LengthUnit := mRepInfo.LengthUnit;
  if mmLengthUnits1.Execute then
    if mmLengthUnits1.LengthUnit <> mRepInfo.LengthUnit then begin
      mRepInfo.LengthUnit := mmLengthUnits1.LengthUnit;
      GetGridDisplay;
      SetGraphParameters;
      DBGrid1UpdateFooter(Sender);
    end; //if mmLengthUnits1.LengthUnit <> gRepInfo.LengthUnit then
end; //procedure TEsp_MachineReportDlg.acFormatExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEsp_MachineReportDlg.ActionHelpContentExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acLoadProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;

begin
  xRes := mmGraph1.MemoINI.SelectProfile;
  if xRes.Profilename <> '' then
    LoadProfile(xRes.Username, xRes.Profilename);
end; //procedure TEsp_MachineReportDlg.acLoadProfileExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acPeriodExecute(Sender: TObject);
begin
  inherited;
  if SelectInterval(Self, mPeriod) then begin
    mRepInfo.StartTime := mPeriod.pBegin;
    mRepInfo.EndTime := mPeriod.pEnd;
    RunQuery;
    ReadData;
  end; //if SelectInterval(gPeriod) then
end; //procedure TEsp_MachineReportDlg.acPeriodExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acPrintExecute(Sender: TObject);
var xBlackWhitePrint : Boolean;
begin
  inherited;
  if not GetPrintOptions(poLandscape, Self, xBlackWhitePrint) then exit;

  if PageControl1.ActivePage = tsTable then begin
    with TfrmPrintList.Create(Self, qu1, DBGrid1, Caption) do try
      Execute;
    finally
      Free;
    end; //try..finally
  end //if PageControl1.ActivePage = tsTable then begin
  else
    mmGraph1.Print(10, 20, 0, 0);
end; //procedure TEsp_MachineReportDlg.acPrintExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acSaveAsDefaultExecute(Sender: TObject);
begin
  inherited;
  mmGridPos1.WriteSettings;
  mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGraph1.WriteSettings;
  mmGraph1.MemoINI.SetDefaultProfile;
end; //procedure TEsp_MachineReportDlg.acSaveAsDefaultExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acSaveProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;
  xSerieNr, x: integer;
begin
// inherited;

  xRes := mmGraph1.MemoINI.SaveProfile;
  if xRes.Profilename <> '' then begin
    mmGridPos1.MemoINI.Profilename := xRes.Profilename;
    mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);

    xSerieNr := 0;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then inc(xSerieNr);

    mmGraph1.Data.NumSeries := xSerieNr;

    mmGridPos1.WriteSettings;
    mmGraph1.WriteSettings;
  end; //if xRes.Profilename <> '' then
end; //procedure TEsp_MachineReportDlg.acSaveProfileExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acSettingsExecute(Sender: TObject);
var
  xForm: TReinigerSettingsDlg;
begin
  if GetCreateChildForm(TReinigerSettingsDlg, TForm(xForm), Self) then
    if mRepInfo.ProdGroupID = 0 then
      xForm.Init(dlMachine, mRepInfo.MachineID, mRepInfo.StartTime, mRepInfo.EndTime, quMachc_machine_name.value)
    else
      xForm.Init(dlProdGrp, mRepInfo.ProdGroupID, mRepInfo.StartTime, mRepInfo.EndTime, quMachc_machine_name.value);
end; //procedure TEsp_MachineReportDlg.acSettingsExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acSortGraphExecute(Sender: TObject);
begin
  inherited;
  mmGraph1.SortSeriesDlg;
end; //procedure TEsp_MachineReportDlg.acSortGraphExecute
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.DBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
  if gdFixed in State then
    ABrush.Color := clLightYellow
  else
    ABrush.Color := TwwDBGrid(Sender).Color;

  if highlight then begin
    AFont.Color := clHighlightText;
    ABrush.Color := clHighlight;
  end; //if highlight then
end; //procedure TEsp_MachineReportDlg.DBGrid1CalcCellColors
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.DBGrid1DblClick(Sender: TObject);
begin
  if qu1c_spindle_id.AsInteger <= 0 then exit;
  mRepInfo.Spindle := qu1c_spindle_id.Value;
  ShowSpindleDetails;
end; //procedure TEsp_MachineReportDlg.DBGrid1DblClick
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  mParaList := nil;
end; //procedure TEsp_MachineReportDlg.FormClose
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Spulstellenbericht\SPST_DLG_Maschinenbericht.htm');
  mmGridPos1.MemoINI.Applicationname := gApplicationName;
  mmGridPos1.MemoINI.Formname := Classname;

  mmGraph1.MemoINI.Applicationname := gApplicationName;

  mParameter := TParameterList.Create;
end; //procedure TEsp_MachineReportDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.mmGraph1GraphDblClick(Sender: TObject;
  AItem: eDblClickT; ARow, APoint: Integer; AStatistik: eStatistikT);
begin
  if APoint < 0 then
    Exit;
  mRepInfo.Spindle := round(mmGraph1.Data.GetXValue(aRow, aPoint));
  ShowSpindleDetails;
end; //procedure TEsp_MachineReportDlg.mmGraph1GraphDblClick
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.mmTranslatorLanguageChange(Sender: TObject);
begin
  if not (quMach.Active) then
    quMach.Active := True;

  TextToTranslate;
end; //procedure TEsp_MachineReportDlg.mmTranslatorLanguageChange
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.PageControl1Change(Sender: TObject);
begin
  acSortGraph.Enabled := PageControl1.ActivePage = tsGraph;
end; //procedure TEsp_MachineReportDlg.PageControl1Change
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.qu1c_rtSpdGetText(Sender: TField;
  var Text: string; DisplayText: Boolean);
begin
  Text := Format_hmm(Sender.AsInteger);
end; //procedure TEsp_MachineReportDlg.qu1c_rtSpdGetText
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.qu1CalcFields(DataSet: TDataSet);
var
  xFactor: double;

begin //main
  qu1calc_MEff.AsFloat := CalcPercentage(qu1c_rtSpd.AsFloat, qu1c_otSpd.AsFloat);
  qu1calc_PEff.AsFloat := CalcPercentage(qu1c_rtSpd.AsFloat, qu1c_wtSpd.AsFloat);
  qu1calc_PCSp.AsFloat := CalcPercentage(qu1c_CSp.AsFloat, qu1c_Sp.AsFloat);
  qu1calc_PRSp.AsFloat := CalcPercentage(qu1c_RSp.AsFloat, qu1c_Sp.AsFloat);

  xFactor := GetConvertFactor(qu1);
  qu1.FieldByName('calc_Red').AsFloat       := qu1.FieldByName('c_Red').AsFloat * xFactor;
  qu1.FieldByName('calc_ManSt').AsFloat     := qu1.FieldByName('c_ManSt').AsFloat * xFactor;
  qu1.FieldByName('calc_Sp').AsFloat        := qu1.FieldByName('c_Sp').AsFloat * xFactor;
  qu1.FieldByName('calc_Rsp').AsFloat       := qu1.FieldByName('c_Rsp').AsFloat * xFactor;
  qu1.FieldByName('calc_YB').AsFloat        := qu1.FieldByName('c_YB').AsFloat * xFactor;
  qu1.FieldByName('calc_CS').AsFloat        := qu1.FieldByName('c_CS').AsFloat * xFactor;
  qu1.FieldByName('calc_CL').AsFloat        := qu1.FieldByName('c_CL').AsFloat * xFactor;
  qu1.FieldByName('calc_CT').AsFloat        := qu1.FieldByName('c_CT').AsFloat * xFactor;
  qu1.FieldByName('calc_CN').AsFloat        := qu1.FieldByName('c_CN').AsFloat * xFactor;
  qu1.FieldByName('calc_CSp').AsFloat       := qu1.FieldByName('c_CSp').AsFloat * xFactor;
  qu1.FieldByName('calc_CClS').AsFloat      := qu1.FieldByName('c_CClS').AsFloat * xFactor;
  qu1.FieldByName('calc_CClL').AsFloat      := qu1.FieldByName('c_CClL').AsFloat * xFactor;
  qu1.FieldByName('calc_CClT').AsFloat      := qu1.FieldByName('c_CClT').AsFloat * xFactor;
  qu1.FieldByName('calc_UClS').AsFloat      := qu1.FieldByName('c_UClS').AsFloat * xFactor;
  qu1.FieldByName('calc_UClL').AsFloat      := qu1.FieldByName('c_UClL').AsFloat * xFactor;
  qu1.FieldByName('calc_UClT').AsFloat      := qu1.FieldByName('c_UClT').AsFloat * xFactor;
  qu1.FieldByName('calc_COffCnt').AsFloat           := qu1.FieldByName('c_COffCnt').AsFloat * xFactor;
  qu1.FieldByName('calc_COffCntPlus').AsFloat       := qu1.FieldByName('c_COffCntPlus').AsFloat * xFactor;
  qu1.FieldByName('calc_COffCntMinus').AsFloat      := qu1.FieldByName('c_COffCntMinus').AsFloat * xFactor;
  qu1.FieldByName('calc_CShortOffCnt').AsFloat      := qu1.FieldByName('c_CShortOffCnt').AsFloat * xFactor;
  qu1.FieldByName('calc_CShortOffCntPlus').AsFloat  := qu1.FieldByName('c_CShortOffCntPlus').AsFloat * xFactor;
  qu1.FieldByName('calc_CShortOffCntMinus').AsFloat := qu1.FieldByName('c_CShortOffCntMinus').AsFloat * xFactor;
  qu1.FieldByName('calc_CBu').AsFloat       := qu1.FieldByName('c_CBu').AsFloat * xFactor;
  qu1.FieldByName('calc_CDBu').AsFloat      := qu1.FieldByName('c_CDBu').AsFloat * xFactor;
  qu1.FieldByName('calc_CSys').AsFloat      := qu1.FieldByName('c_CSys').AsFloat * xFactor;
  qu1.FieldByName('calc_CUpY').AsFloat      := qu1.FieldByName('c_CUpY').AsFloat * xFactor;
  qu1.FieldByName('calc_CYTot').AsFloat     := qu1.FieldByName('c_CYTot').AsFloat * xFactor;
  qu1.FieldByName('calc_CSiro').AsFloat     := qu1.FieldByName('c_CSiro').AsFloat * xFactor;
  qu1.FieldByName('calc_CSFi').AsFloat      := qu1.FieldByName('c_CSFi').AsFloat * xFactor;
  qu1.FieldByName('calc_CSiroCl').AsFloat   := qu1.FieldByName('c_CSiroCl').AsFloat * xFactor;
  qu1.FieldByName('calc_CP').AsFloat        := qu1.FieldByName('c_CP').AsFloat * xFactor;
  qu1.FieldByName('calc_CVCV').AsFloat      := qu1.FieldByName('c_CVCV').AsFloat * xFactor;   //Nue:18.02.08
  // Locks sind absolut und nicht L�ngen basiert -> direkt Feld c_Lck... verwenden ohne Berechnung

  qu1calc_SFI_D.Value := CalcSFI_D(qu1c_SFI.AsFloat, qu1c_SFICnt.AsFloat);
  if qu1c_AdjustBaseCnt.AsInteger > 0 then
    qu1calc_SFI.Value := CalcSFI(qu1c_SFI.AsFloat, qu1c_SFICnt.AsFloat, qu1c_AdjustBase.AsFloat / qu1c_AdjustBaseCnt.AsFloat)
  else
    qu1calc_SFI.Value := 0;

  qu1calc_CVd.Value := qu1calc_SFI_D.Value * cVCVFactor;   //Nue:18.02.08

  // Imperfections  sdo
  if qu1c_Len.Value > 0 then begin
    // Diese sind per 1 km
    qu1calc_INeps.Value  := qu1c_INeps.AsFloat / qu1c_Len.Value;
    qu1calc_IThick.Value := qu1c_IThick.AsFloat / qu1c_Len.Value;
    qu1calc_IThin.Value  := qu1c_IThin.AsFloat / qu1c_Len.Value;
    qu1calc_I2_4.Value   := qu1c_I2_4.AsFloat / qu1c_Len.Value;
    qu1calc_I4_8.Value   := qu1c_I4_8.AsFloat / qu1c_Len.Value;
    qu1calc_I8_20.Value  := qu1c_I8_20.AsFloat / qu1c_Len.Value;
    qu1calc_I20_70.Value := qu1c_I20_70.AsFloat / qu1c_Len.Value;
    // Ausname: ISmall per 1m
    qu1calc_ISmall.Value := qu1c_ISmall.AsFloat / qu1c_Len.Value / 1000;
  end;

  qu1calc_npProdGrp.Value := Round(qu1c_wtSpd.Value - qu1c_rtSpd.Value);

  //qu1c_LStProd.Value := qu1c_LStProd.Value Div 60;
  qu1calc_LStTOT.Value := Round(qu1c_LStProd.Value
    + qu1c_LStBreak.Value
    + qu1c_LStMat.Value
    + qu1c_LStRev.Value
    + qu1c_LStClean.Value
    + qu1c_LStDef.Value);
end; //procedure TEsp_MachineReportDlg.qu1CalcFields
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.GetGridDisplay;
var
  xluStr: string;

begin
  xluStr := GetLengthUnitStr(mRepInfo.LengthUnit);

  qu1.DisableControls;
  try
    qu1c_spindle_id.DisplayLabel   := cC_spindle_idStr;
    qu1c_rtSpd.DisplayLabel        := cC_rtSpdStr;
    qu1calc_npProdGrp.DisplayLabel := cCalc_npProdGrpStr;
    qu1calc_MEff.DisplayLabel      := cCalc_MEffStr;
    qu1calc_PEff.DisplayLabel      := cCalc_PEffStr;
    qu1c_Len.DisplayLabel          := cC_LenStr;
    qu1c_Wei.DisplayLabel          := cC_WeiStr;
    qu1c_Bob.DisplayLabel          := cC_BobStr;
    qu1c_Cones.DisplayLabel        := cC_ConesStr;
    qu1calc_CYTOT.DisplayLabel     := Format(cCalc_CYTOTStr, [xluStr]);
    qu1calc_Sp.DisplayLabel        := Format(cCalc_SpStr, [xluStr]);
    qu1calc_YB.DisplayLabel        := Format(cCalc_YBStr, [xluStr]);
    qu1calc_CN.DisplayLabel        := Format(cCalc_CNStr, [xluStr]);
    qu1calc_CS.DisplayLabel        := Format(cCalc_CSStr, [xluStr]);
    qu1calc_CL.DisplayLabel        := Format(cCalc_CLStr, [xluStr]);
    qu1calc_CT.DisplayLabel        := Format(cCalc_CTStr, [xluStr]);
    qu1calc_CSIRO.DisplayLabel     := Format(cCalc_CSIROStr, [xluStr]);
    qu1calc_COFFCNT.DisplayLabel           := Format(cCalc_COFFCNTStr, [xluStr]);
    qu1calc_COFFCNTPlus.DisplayLabel       := Format(cCalc_COFFCNTPlusStr, [xluStr]);
    qu1calc_COFFCNTMinus.DisplayLabel      := Format(cCalc_COFFCNTMinusStr, [xluStr]);
    qu1calc_CShortOFFCNT.DisplayLabel      := Format(cCalc_CShortOFFCNTStr, [xluStr]);
    qu1calc_CShortOFFCNTPlus.DisplayLabel  := Format(cCalc_CShortOFFCNTPlusStr, [xluStr]);
    qu1calc_CShortOFFCNTMinus.DisplayLabel := Format(cCalc_CShortOFFCNTMinusStr, [xluStr]);
    qu1calc_CP.DisplayLabel                := Format(cCalc_CPStr, [xluStr]);
    qu1calc_CCLS.DisplayLabel      := Format(cCalc_CCLSStr, [xluStr]);
    qu1calc_CCLL.DisplayLabel      := Format(cCalc_CCLLStr, [xluStr]);
    qu1calc_CCLT.DisplayLabel      := Format(cCalc_CCLTStr, [xluStr]);
    qu1c_LSt.DisplayLabel          := cC_LStStr;
    qu1c_LStProd.DisplayLabel      := cC_LStProdStr;
    qu1c_LStBreak.DisplayLabel     := cC_LStBreakStr;
    qu1c_LStMat.DisplayLabel       := cC_LStMatStr;
    qu1c_LStRev.DisplayLabel       := cC_LStRevStr;
    qu1c_LStClean.DisplayLabel     := cC_LStCleanStr;
    qu1c_LStDef.DisplayLabel       := cC_LStDefStr;
    qu1calc_LStTOT.DisplayLabel    := cCalc_LStTOTStr;
    qu1calc_CUPY.DisplayLabel      := Format(cCalc_CUPYStr, [xluStr]);
    qu1calc_CBU.DisplayLabel       := Format(cCalc_CBUStr, [xluStr]);
    qu1calc_CSYS.DisplayLabel      := Format(cCalc_CSYSStr, [xluStr]);
    qu1calc_CSP.DisplayLabel       := Format(cCalc_CSPStr, [xluStr]);
    qu1calc_RSP.DisplayLabel       := Format(cCalc_RSPStr, [xluStr]);
    qu1calc_PCSp.DisplayLabel      := cCalc_PCSpStr;
    qu1calc_PRSp.DisplayLabel      := cCalc_PRSpStr;
    qu1c_LckSys.DisplayLabel       := cC_LckSysStr;
    qu1c_LckSIRO.DisplayLabel      := cC_LckSIROStr;
    qu1calc_Red.DisplayLabel       := Format(cCalc_RedStr, [xluStr]);
    qu1calc_ManSt.DisplayLabel     := Format(cCalc_ManStStr, [xluStr]);
    qu1calc_UCLS.DisplayLabel      := Format(cCalc_UCLSStr, [xluStr]);
    qu1calc_UCLL.DisplayLabel      := Format(cCalc_UCLLStr, [xluStr]);
    qu1calc_UCLT.DisplayLabel      := Format(cCalc_UCLTStr, [xluStr]);
    qu1calc_CDBU.DisplayLabel      := Format(cCalc_CDBUStr, [xluStr]);
    qu1c_tRed.DisplayLabel         := cC_tRedStr;
    qu1c_tYell.DisplayLabel        := cC_tYellStr;
    qu1c_tManSt.DisplayLabel       := cC_tManStStr;
    qu1calc_SFI_D.DisplayLabel     := cCalc_SfiDStr;
    qu1calc_CSFI.DisplayLabel      := Format(cCalc_CSFIStr, [xluStr]);
    qu1calc_CSIROCl.DisplayLabel   := Format(cCalc_CSIROClStr, [xluStr]);
//    qu1calc_LckClS.DisplayLabel    := Format(cCalc_LckClSStr, [xluStr]);
//    qu1calc_LckClL.DisplayLabel    := Format(cCalc_LckClLStr, [xluStr]);
//    qu1calc_LckClT.DisplayLabel    := Format(cCalc_LckClTStr, [xluStr]);
//    qu1calc_LckSIROCl.DisplayLabel := Format(cCalc_LckSIROClStr, [xluStr]);
//    qu1calc_LckSFI.DisplayLabel    := Format(cCalc_LckSFIStr, [xluStr]);
//    qu1calc_LckOffCnt.DisplayLabel      := Format(cCalc_LckOffCntStr, [xluStr]);
//    qu1calc_LckShortOffCnt.DisplayLabel := Format(cCalc_LckShortOffCntStr, [xluStr]);
//    qu1calc_LckP.DisplayLabel    := Format(cCalc_LckPStr, [xluStr]);
//    qu1calc_LckNSLT.DisplayLabel := Format(cCalc_LckNSLTStr, [xluStr]);

    qu1c_LckSFI.DisplayLabel         := cc_LckSFIStr;
    qu1c_LckSys.DisplayLabel         := cc_LckSysStr;
    qu1c_LckSIRO.DisplayLabel        := cc_LckSIROStr;
    qu1c_LckSIROCl.DisplayLabel      := cc_LckSIROClStr;
    qu1c_LckOffCnt.DisplayLabel      := cc_LckOffCntStr;
    qu1c_LckShortOffCnt.DisplayLabel := cc_LckShortOffCntStr;
    qu1c_LckP.DisplayLabel           := cc_LckPStr;
    qu1c_LckClS.DisplayLabel         := cc_LckClSStr;
    qu1c_LckClL.DisplayLabel         := cc_LckClLStr;
    qu1c_LckClT.DisplayLabel         := cc_LckClTStr;
    qu1c_LckNSLT.DisplayLabel        := cc_LckNSLTStr;

    qu1calc_INeps.DisplayLabel     := cCalc_INepsStr;
    qu1calc_IThick.DisplayLabel    := cCalc_IThickStr;
    qu1calc_IThin.DisplayLabel     := cCalc_IThinStr;
    qu1calc_ISmall.DisplayLabel    := cCalc_ISmallStr;
    qu1calc_I2_4.DisplayLabel      := cCalc_I2_4Str;
    qu1calc_I4_8.DisplayLabel      := cCalc_I4_8Str;
    qu1calc_I8_20.DisplayLabel     := cCalc_I8_20Str;
    qu1calc_I20_70.DisplayLabel    := cCalc_I20_70Str;

//Nue:18.02.08
    qu1calc_CVd.DisplayLabel       := cCalc_CVdStr;
    qu1calc_CVCV.DisplayLabel      := Format(cCalc_CVCVStr, [xluStr]);
    qu1c_LckVCV.DisplayLabel       := cc_LckVCVStr;

  finally
    qu1.EnableControls;
  end; //try..finally
end; //procedure TEsp_MachineReportDlg.GetGridDisplay
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.LoadProfile(aUsername, aProfilename: string);
var
  xParameter: TParameterList;
begin
  mmGraph1.BeginUpdate;
  mmGridPos1.MemoINI.Username := aUsername;
  mmGridPos1.MemoINI.Profilename := aProfilename;
  mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGridPos1.ReadSettings;
  mmGraph1.ReadSettings;
  mParaList := GetGraphParamList(mmGraph1);
  xParameter := ReadParam(qu1, mParaList, mmGraph1);
  mParameter.Assign(xParameter);
  xParameter.Free;
  // nLSt ist ein Z�hler und wird immer ohne Komma dargestellt
  qu1c_LSt.DisplayFormat := '#0';
  RunQuery;
  SetGraphParameters;
  mmGridPos1.MemoINI.Username := mmUsername;
  mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);

  DBGrid1.Options := DBGrid1.Options + [dgShowFooter, dgShowCellHint] - [dgFooter3DCells];
end; //procedure TEsp_MachineReportDlg.LoadProfile
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.ReadData;
var
  i: integer;
  yValue: tYPointT;
  xFStr: string;
  xSortOrder: eSortOrderT;
  xSortRow: integer;

  function SetYValue(aFieldname: string): tYPointT;
  begin
    Result.IsNull := qu1.FieldByName(aFieldname).IsNull;
    Result.Value := qu1.FieldByName(aFieldname).AsFloat;
  end; //function SetYValue

begin
  with mmGraph1 do begin
    xSortOrder := Data.SortOrder;
    xSortRow := Data.SortRow;

    Data.ClearData;

    qu1.DisableControls;
    try
      qu1.First;
      while not qu1.Eof do begin
        for i := 1 to Data.NumSeries do begin
          xFStr := Series.FieldName[i];
          yValue := SetYValue(xFStr);
          if not yValue.IsNull then
            Data.AddYValue(i, qu1c_spindle_id.AsInteger, yValue.Value, qu1c_spindle_id.asstring);
        end; //for i := 1 to Data.NumSeries do

        qu1.Next;
      end; //while not qu1.Eof do
    finally
      qu1.First;
      qu1.EnableControls;
    end; //try..finally

    Data.SortByRow(xSortRow, xSortOrder);
    TextToTranslate;
    Refresh;
  end; //with mmGraph1 do
end; //procedure TEsp_MachineReportDlg.ReadData
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.RunQuery;
begin
  with qu1 do begin
    Active := False;
    if mRepInfo.ProdGroupID = 0 then
      CommandText := Format(dmSpindleReport.slMachineReport.Text, ['AND c_prod_id <> :ProdGroupID'])
    else
      CommandText := Format(dmSpindleReport.slMachineReport.Text, ['AND c_prod_id = :ProdGroupID']);

    Parameters.ParamByName('MachineID').value := mRepInfo.MachineID;
    Parameters.ParamByName('ProdGroupID').value := mRepInfo.ProdGroupID;
    Parameters.ParamByName('IntervalStart').value := mRepInfo.StartTime;
    Parameters.ParamByName('IntervalEnd').value := mRepInfo.EndTime;

    Active := True;
  end;
end; //procedure TEsp_MachineReportDlg.RunQuery
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.SetGraphParameters;
var
  x,
    xSerieNr: integer;
  xHasLine,
    xHasBar: boolean;

begin
  xHasLine := False;
  xHasBar := False;

  with mmGraph1 do begin
    BeginUpdate;

    Bars.Style := mParameter.BarStyle;
    if Bars.Style = bsSide then
      Bars.Gap := 5
    else
      Bars.Gap := 2;

    xSerieNr := 0;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then inc(xSerieNr);
    Data.NumSeries := xSerieNr;

    xSerieNr := 1;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then begin
        Series.FieldName[xSerieNr]       := mParameter.Items[x].GraphicParam.FieldName;
        Series.Color[xSerieNr]           := mParameter.Items[x].GraphicParam.Color;
        Series.YAxis[xSerieNr]           := eYAxisT(Ord(mParameter.Items[x].GraphicParam.ChartType));
        Series.YScalingType[xSerieNr]    := mParameter.Items[x].GraphicParam.YScalingType;
        Series.ShowStat[xSerieNr]        := mParameter.Items[x].GraphicParam.Statistik;
        Series.Visible[xSerieNr]         := mParameter.Items[x].GraphicParam.ShowGraph;
        Series.LowerLimit[xSerieNr]      := mParameter.Items[x].GraphicParam.LowerLimit;
        Series.UpperLimit[xSerieNr]      := mParameter.Items[x].GraphicParam.UpperLimit;
        Series.UpperLimitColor[xSerieNr] := clRed;
        Series.LowerLimitColor[xSerieNr] := clRed;

        xHasBar := xHasBar or (mParameter.Items[x].GraphicParam.ChartType = ctBar);
        xHasLine := xHasLine or (mParameter.Items[x].GraphicParam.ChartType = ctLine);

        Series.Name[xSerieNr] := GetDisplayLabel(qu1.FieldByName(Series.FieldName[xSerieNr]).DisplayLabel);
        inc(xSerieNr);

      end; //if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and

    LeftYAxis.Visible := xHasLine;
    RightYAxis.Visible := True; //xHasBar;

    if xHasBar and xHasLine then
      HorzGrid.GridType := hgLeftRightAxis
    else if xHasBar then
      HorzGrid.GridType := hgRightAxis
    else if xHasLine then
      HorzGrid.GridType := hgLeftAxis;

    ReadData;
    EndUpdate;
  end; //with mmGraph1 do
end; //procedure TEsp_MachineReportDlg.SetGraphParameters
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.ShowSpindleDetails;
var
  xForm: TEsp_SpindleReportDlg;
begin
  if GetCreateChildForm(TEsp_SpindleReportDlg, TForm(xForm), Self) then begin
    xForm.Init(mRepInfo, mParaList);
    xForm.Parameter.Assign(mParameter); // SDO
    xForm.ShowSettedGraphic; // SDO
    xForm.ShowGraphicOptions := False; // SDO
    xForm.Visible := True;
  end;
end; //procedure TEsp_MachineReportDlg.ShowSpindleDetails
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.TextToTranslate;
var
  i: integer;
  xSpindleFirst: integer;
  xSpindleLast: integer;
begin
  UpdateCaption;

  with mmGraph1 do begin
    BeginUpdate;
    if mRepInfo.ProdGroupID <> 0 then begin
      xSpindleFirst := quProdc_spindle_first.Value; //Mg
      xSpindleLast := quProdc_spindle_last.Value; // Mg
      Bars.MinBarsPerPage := quProdc_spindle_last.Value - quProdc_spindle_first.Value + 1;
    end
    else begin
      Bars.MinBarsPerPage := quMachc_nr_of_spindles.value; //0;
      xSpindleFirst := 1; // Mg
      xSpindleLast := quMachc_nr_of_spindles.Value; // Mg
    end;

    for i := 1 to Data.NumSeries do
      Series.Name[i] := GetDisplayLabel(qu1.FieldByName(Series.FieldName[i]).DisplayLabel);

    LeftYAxis.Title := cLeftAxisStr;
    RightYAxis.Title := cRightAxisStr;
    Titles.Title := Format(cGraphTitleStr1a, [quMachc_machine_name.value,
        cMachineTypeArr[TMachineType(quMachc_machine_type.Value)].Name,
        xSpindleFirst,
        xSpindleLast]);

//    Titles.Title := Format(cGraphTitleStr1a, [quMachc_machine_name.value,
//      quMachc_machine_model.value,
//        xSpindleFirst,
//        xSpindleLast]);

    Titles.SubTitle := Format(cGraphTitleStr2, [formatdatetime(cDateFrmStr, mRepInfo.StartTime),
      formatdatetime(cDateFrmStr, mRepInfo.EndTime),
      GetTimeDiffStr(mRepInfo.StartTime, mRepInfo.EndTime)]);

    XAxis.Title := cXAxisStr;
    EndUpdate;
  end; //with mmGraph1 do

  GetGridDisplay;
end; //procedure TEsp_MachineReportDlg.TextToTranslate
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.UpdateCaption;
begin
  laPeriodValue.Caption := Format('%s - %s', [formatdatetime(cDateFrmStr, mRepInfo.StartTime),
    formatdatetime(cDateFrmStr, mRepInfo.EndTime)]);
  laDurationValue.Caption := GetTimeDiffStr(mRepInfo.StartTime, mRepInfo.EndTime);

  Caption := Format(cCaptionStr1, [quMachc_machine_name.value,
    formatdatetime(cDateFrmStr, mRepInfo.StartTime),
      formatdatetime(cDateFrmStr, mRepInfo.EndTime)]);
end; //procedure TEsp_MachineReportDlg.UpdateCaption
//-----------------------------------------------------------------------------
function TEsp_MachineReportDlg.Init(aMachines: string; aProdID: integer): Boolean;
var
  xSelection: rListSelResultT;
begin
  Result := True;
  PageControl1.ActivePage := tsGraph;
  PageControl1Change(nil);

  quMach.Active := False;
  quProd.Active := False;

  //always start showing all available intervals
//  GetPeriodByIntervals(mRepInfo.StartTime, mRepInfo.EndTime, cDefaultStartupIntervals);
  GetPeriodByIntervals(mRepInfo.StartTime, mRepInfo.EndTime, TMMSettingsReader.Instance.Value[cDefaultSpindleReportTimeRange]);
  mPeriod.pBegin := mRepInfo.StartTime;
  mPeriod.pEnd := mRepInfo.EndTime;
  CodeSite.SendFmtMsg('TimeRange: %s - %s', [DateTimeToStr(mPeriod.pBegin), DateTimeToStr(mPeriod.pEnd)]);

  // if machine selection comes from Floor -> no machine selected = all machines selected
  // aMachines contains ID not Names
  if (Pos(',', aMachines) > 0) or (aMachines = '') then
    xSelection := StandardSelect(cSelectMachineStr, 'c_machine_name', 'c_machine_id', 't_machine m WHERE c_node_id <> ''0''')
  else try
    xSelection.ID := StrToInt(aMachines);
    xSelection.Text := aMachines;
  except
  end;

  if xSelection.Text = '' then begin
    Result := False;
    Close;
    exit;
  end; //if xSelection.Text = '' then

  // Wss: parameter changed to MachineID
  quMach.Parameters.ParamByName('MachineID').value := xSelection.ID;
  quMach.Open;

  if quMach.Eof then begin
    MMMessageDlg(Format(cMachineNotFoundMsg, [xSelection.Text]), mtError, [mbOK], 0, Self);
    Close;
    exit;
  end; //if quMach.Eof then

//  mRepInfo.MachineID   := quMachc_machine_id.asinteger;
  mRepInfo.MachineID := xSelection.ID;
  mRepInfo.ProdGroupID := aProdID;

  with TMMSettingsReader.Instance do
    mRepInfo.LengthUnit := TLenBase(Value[cMMUnit]);

  if aProdID <> 0 then begin
    quProd.Parameters.ParamByName('ProdGroupID').value := mRepInfo.ProdGroupID;
    quProd.Open;
    mRepInfo.MinSpindle := quProdc_spindle_first.Value;
    mRepInfo.MaxSpindle := quProdc_spindle_last.Value;
  end
  else begin
    mRepInfo.MinSpindle := 1;
    mRepInfo.MaxSpindle := quMachc_nr_of_spindles.Value;
  end;

{
 mRepInfo.MinSpindle  := 1;
 mRepInfo.MaxSpindle  := quMachc_nr_of_spindles.Value;
 mRepInfo.MachineID   := quMachc_machine_id.asinteger;
 mRepInfo.ProdGroupID := aProdID;

 with TMMSettingsReader.Create do
 try
   mRepInfo.LengthUnit := TLenBase ( xSettings.value[ SetupMMDefaults.cMMUnit ] );
 finally
   Free;
 end;

 if mRepInfo.ProdGroupID <> 0 then begin
  quProd.ParamByName('ProdGroupID').asinteger := mRepInfo.ProdGroupID;
  quProd.Open;
  mRepInfo.MinSpindle := quProdc_spindle_last.Value;
  mRepInfo.MaxSpindle := quProdc_spindle_first.Value;
 end; //if gRepInfo.ProdGroupID > 0 then
{}

  DBGrid1.TitleColor := cDefaultGridTitleColor;

  mmGridPos1.MemoINI.Username := mmUsername;
  LoadProfile(mmGridPos1.MemoINI.Username, mmGridPos1.MemoINI.GetDefaultProfile);
 // Mg 12.09.00
  GetGridDisplay;
  SetGraphParameters;
end; //procedure TEsp_MachineReportDlg.Init
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.PrepareReport(aDataset: TDataset);
var
  x, xMaxFields: integer;
  xStr: string;

begin
  xMaxFields := aDataSet.FieldCount;
  with aDataSet do
    for x := 0 to xMaxFields - 1 do begin
      xStr := uppercase(Fields[x].FieldName);

      if (xStr = 'C_SPINDLE_ID') then
        Fields[x].Tag := cShowKey
      else if (xStr = 'C_LEN') or
              (xStr = 'C_WEI') or
              (xStr = 'C_BOB') or
              (xStr = 'C_CONES') or
              (xStr = 'C_TRED') or
              (xStr = 'C_TYELL') or
              (xStr = 'C_TMANST') or
              (xStr = 'C_LST') or
//              (xStr = 'CALC_LCKCLS') or
              (xStr = 'CALC_SP') or
              (xStr = 'CALC_RSP') or
              (xStr = 'CALC_YB') or
              (xStr = 'CALC_CS') or
              (xStr = 'CALC_CL') or
              (xStr = 'CALC_CT') or
              (xStr = 'CALC_CN') or
              (xStr = 'CALC_CSP') or
              (xStr = 'CALC_UCLS') or
              (xStr = 'CALC_UCLL') or
              (xStr = 'CALC_UCLT') or
              (xStr = 'CALC_COFFCNT') or
              (xStr = 'CALC_CBU') or
              (xStr = 'CALC_CDBU') or
              (xStr = 'CALC_CSYS') or
              (xStr = 'CALC_CUPY') or
              (xStr = 'CALC_CYTOT') or
              (xStr = 'CALC_CSIRO') or
              (xStr = 'CALC_MEFF') or
              (xStr = 'CALC_PEFF') or
              (xStr = 'CALC_PCSP') or
              (xStr = 'CALC_PRSP') or
              (xStr = 'CALC_NPPRODGRP') or
              (xStr = 'CALC_MANST') or
              (xStr = 'CALC_INEPS') or
              (xStr = 'CALC_ITHICK') or
              (xStr = 'CALC_ITHIN') or
              (xStr = 'CALC_ISMALL') or
              (xStr = 'CALC_I2_4') or
              (xStr = 'CALC_I4_8') or
              (xStr = 'CALC_I8_20') or
              (xStr = 'CALC_I20_70') or
              (xStr = 'C_LSTPROD') or
              (xStr = 'CALC_SFI_D') or
              (xStr = 'CALC_SFI') or
              (xStr = 'CALC_CSFI') or
              (xStr = 'CALC_COFFCNTPLUS') or
              (xStr = 'CALC_COFFCNTMINUS') or
              (xStr = 'CALC_CSHORTOFFCNT') or
              (xStr = 'CALC_CSHORTOFFCNTPLUS') or
              (xStr = 'CALC_CSHORTOFFCNTMINUS') or
              (xStr = 'CALC_CP') or
              (xStr = 'CALC_CCLS') or
              (xStr = 'CALC_CCLL') or
              (xStr = 'CALC_CCLT') or

              //Nue:18.02.08
              (xStr = 'CALC_CVD') or
              (xStr = 'CALC_CVCV') or
              (xStr = 'C_LCKVCV') or

              (xStr = 'C_LCKSYS') or
              (xStr = 'C_LCKSFI') or
              (xStr = 'C_LCKSIRO') or
              (xStr = 'C_LCKSIROCL') or
              (xStr = 'C_LCKOFFCNT') or
              (xStr = 'C_LCKSHORTOFFCNT') or
              (xStr = 'C_LCKP') or
              (xStr = 'C_LCKCLS') or
              (xStr = 'C_LCKCLL') or
              (xStr = 'C_LCKCLT') or
              (xStr = 'C_LCKNSLT') then
        Fields[x].Tag := cShowTableGraphic
      else if (xStr = 'C_TRED') or (xStr = 'CALC_RED') then
        Fields[x].Tag := cShowTable
      else
        Fields[x].Tag := cNonVisible;
//      else if (xStr = 'C_RTSPD') or
//              (xStr = 'C_RED') or
//              (xStr = 'C_SP') or
//              (xStr = 'C_RSP') or
//              (xStr = 'C_YB') or
//              (xStr = 'C_CS') or
//              (xStr = 'C_CL') or
//              (xStr = 'C_CT') or
//              (xStr = 'C_CN') or
//              (xStr = 'C_CSP') or
//              (xStr = 'C_CCL') or
//              (xStr = 'C_UCLS') or
//              (xStr = 'C_UCLL') or
//              (xStr = 'C_UCLT') or
//              (xStr = 'C_COFFCNT') or
//              (xStr = 'C_LCKOFFCNT') or
//              (xStr = 'C_CBU') or
//              (xStr = 'C_CDBU') or
//              (xStr = 'C_CSYS') or
//              (xStr = 'C_CUPY') or
//              (xStr = 'C_CYTOT') or
//              (xStr = 'C_CSIRO') or
//              (xStr = 'C_MANST') or
//              (xStr = 'C_OTSPD') or
//              (xStr = 'C_WTSPD') or
//              (xStr = 'C_INEPS') or
//              (xStr = 'C_ITHICK') or
//              (xStr = 'C_ITHIN') or
//              (xStr = 'C_ISMALL') or
//              (xStr = 'C_I2_4') or
//              (xStr = 'C_I4_8') or
//              (xStr = 'C_I8_20') or
//              (xStr = 'C_I20_70') or
//              (xStr = 'C_SFI') or
//              (xStr = 'C_CSFI') or
//              (xStr = 'C_LCKCLS') or
//              (xStr = 'C_LCKSIROCL') or
//              (xStr = 'C_CSIROCL') or
//              (xStr = 'C_LCKSFI') or
//              (xStr = 'C_SFICNT') or
//              (xStr = 'CALC_LCKSIROCL') or
//              (xStr = 'CALC_CSIROCL') or
//              (xStr = 'CALC_LSTTOT') or
//              (xStr = 'C_LSTBREAK') or
//              (xStr = 'C_LSTMAT') or
//              (xStr = 'C_LSTREV') or
//              (xStr = 'C_LSTCLEAN') then
//        Fields[x].Tag := cNonVisible;
    end; //for x:= 0 to xMaxFields - 1 do
end;
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.ShowDialog(aGrid: TwwDBGrid);
var
  x, xrec: integer;
// xDlg: TmSettingsDialog;
begin
  with TmSettingsDialog.Create(self) do try
    ReportType := rtStatistic;
    Grid := aGrid;
    DataSet := aGrid.DataSource.DataSet;

    // mmGraph1.Data.NumSeries:=
    Parameter := mParameter;

    Parameter.LeftYAxis.Autoscale := mmGraph1.LeftYAxis.AutoScaling;
    Parameter.LeftYAxis.Min := Round(mmGraph1.LeftYAxis.DefaultMinY);
    Parameter.LeftYAxis.Max := Round(mmGraph1.LeftYAxis.DefaultMaxY);

    Parameter.RightYAxis.Autoscale := mmGraph1.RightYAxis.AutoScaling;
    Parameter.RightYAxis.Min := Round(mmGraph1.RightYAxis.DefaultMinY);
    Parameter.RightYAxis.Max := Round(mmGraph1.RightYAxis.DefaultMaxY);

    if ShowModal = mrOK then begin

      mParameter.Assign(Parameter);

      mmGraph1.LeftYAxis.AutoScaling := mParameter.LeftYAxis.Autoscale;
      mmGraph1.LeftYAxis.DefaultMinY := mParameter.LeftYAxis.Min;
      mmGraph1.LeftYAxis.DefaultMaxY := mParameter.LeftYAxis.Max;

      mmGraph1.RightYAxis.AutoScaling := mParameter.RightYAxis.Autoscale;
      mmGraph1.RightYAxis.DefaultMinY := mParameter.RightYAxis.Min;
      mmGraph1.RightYAxis.DefaultMaxY := mParameter.RightYAxis.Max;

      mParaList := nil;
      xrec := 0;
      for x := 0 to mParameter.Count - 1 do
        if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
           mParameter.Items[x].GraphicParam.ShowGraph then begin
          SetLength(mParaList, xRec + 1);
          mParaList[xRec] := mParameter.Items[x].GraphicParam;
          inc(xRec);
        end; //if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and

      RunQuery;
      SetGraphParameters;
    end; //if ShowModal = mrOK then
  finally
    free;
  end; //try..finally
end;
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.FormDestroy(Sender: TObject);
begin
  mParameter.free;
end; //procedure TEsp_MachineReportDlg.FormDestroy
//-----------------------------------------------------------------------------
procedure TEsp_MachineReportDlg.DBGrid1UpdateFooter(Sender: TObject);
var
  xValue,
  xFactor: double;
  xInt: Integer;
begin
  DBGrid1.ColumnByName('c_spindle_id').FooterValue := cTotalStr;

  with qrySum do try
    if mRepInfo.ProdGroupID = 0 then
      CommandText := Format(dmSpindleReport.slMachineSummary.Text, ['AND c_prod_id <> :ProdGroupID'])
    else
      CommandText := Format(dmSpindleReport.slMachineSummary.Text, ['AND c_prod_id = :ProdGroupID']);

    Parameters.ParamByName('MachineID').value := mRepInfo.MachineID;
    Parameters.ParamByName('ProdGroupID').value := mRepInfo.ProdGroupID;
    Parameters.ParamByName('IntervalStart').value := mRepInfo.StartTime;
    Parameters.ParamByName('IntervalEnd').value := mRepInfo.EndTime;
    Active := True;
{
    ParamByName('MachineID').asinteger := mRepInfo.MachineID;
    if mRepInfo.ProdGroupID = 0 then SQL[63] := 'AND sid.c_prod_id <> :ProdGroupID'
    else SQL[63] := 'AND sid.c_prod_id = :ProdGroupID';

    ParamByName('ProdGroupID').asinteger := mRepInfo.ProdGroupID;
    ParamByName('IntervalStart').asdatetime := mRepInfo.StartTime;
    ParamByName('IntervalEnd').asdatetime := mRepInfo.EndTime;
    Active := true;
{}

    DBGrid1.ColumnByName('calc_npProdGrp').FooterValue := Format_hmm(fieldbyname('calc_npProdGrp').AsInteger);
    DBGrid1.ColumnByName('calc_MEff').FooterValue      := formatfloat(qu1calc_MEff.DisplayFormat, CalcPercentage(fieldbyname('c_rtSpd').AsFloat, fieldbyname('c_otSpd').AsFloat));
    DBGrid1.ColumnByName('calc_PEff').FooterValue      := formatfloat(qu1calc_PEff.DisplayFormat, CalcPercentage(fieldbyname('c_rtSpd').AsFloat, fieldbyname('c_wtSpd').AsFloat));
    DBGrid1.ColumnByName('calc_PCSp').FooterValue      := formatfloat(qu1calc_PCSp.DisplayFormat, CalcPercentage(fieldbyname('c_CSp').AsFloat, fieldbyname('c_Sp').AsFloat));
    DBGrid1.ColumnByName('calc_PRSp').FooterValue      := formatfloat(qu1calc_PRSp.DisplayFormat, CalcPercentage(fieldbyname('c_RSp').AsFloat, fieldbyname('c_Sp').AsFloat));

    DBGrid1.ColumnByName('c_rtSpd').FooterValue        := Format_hmm(fieldbyname('c_rtSpd').AsInteger);
    DBGrid1.ColumnByName('c_wtSpd').FooterValue        := Format_hmm(fieldbyname('c_wtSpd').AsInteger);
    DBGrid1.ColumnByName('c_len').FooterValue          := formatfloat(qu1c_Len.DisplayFormat, fieldbyname('c_len').AsFloat);
    DBGrid1.ColumnByName('c_wei').FooterValue          := formatfloat(qu1c_Wei.DisplayFormat, fieldbyname('c_wei').AsFloat);
    DBGrid1.ColumnByName('c_bob').FooterValue          := formatfloat(qu1c_Bob.DisplayFormat, fieldbyname('c_bob').AsFloat);
    DBGrid1.ColumnByName('c_cones').FooterValue        := formatfloat(qu1c_Cones.DisplayFormat, fieldbyname('c_cones').AsFloat);
    DBGrid1.ColumnByName('c_LSt').FooterValue          := formatfloat(qu1c_LSt.DisplayFormat, fieldbyname('c_LSt').AsFloat);
    DBGrid1.ColumnByName('c_LStProd').FooterValue      := Format_hmm(fieldbyname('c_LStProd').AsInteger);
    DBGrid1.ColumnByName('c_LStBreak').FooterValue     := formatfloat(qu1c_LStBreak.DisplayFormat, fieldbyname('c_LStBreak').AsFloat);
    DBGrid1.ColumnByName('c_LStMat').FooterValue       := formatfloat(qu1c_LStMat.DisplayFormat, fieldbyname('c_LStMat').AsFloat);
    DBGrid1.ColumnByName('c_LStRev').FooterValue       := formatfloat(qu1c_LStRev.DisplayFormat, fieldbyname('c_LStRev').AsFloat);
    DBGrid1.ColumnByName('c_LStClean').FooterValue     := formatfloat(qu1c_LStClean.DisplayFormat, fieldbyname('c_LStClean').AsFloat);
    DBGrid1.ColumnByName('c_LStDef').FooterValue       := formatfloat(qu1c_LStDef.DisplayFormat, fieldbyname('c_LStDef').AsFloat);
    DBGrid1.ColumnByName('calc_LStTOT').FooterValue    := formatfloat(qu1calc_LStTOT.DisplayFormat, fieldbyname('calc_LStTOT').AsFloat);
    DBGrid1.ColumnByName('c_tRed').FooterValue         := formatfloat(qu1c_tRed.DisplayFormat, fieldbyname('c_tRed').AsFloat);
    DBGrid1.ColumnByName('c_tYell').FooterValue        := formatfloat(qu1c_tYell.DisplayFormat, fieldbyname('c_tYell').AsFloat);
    DBGrid1.ColumnByName('c_tManSt').FooterValue       := formatfloat(qu1c_tManSt.DisplayFormat, fieldbyname('c_tManSt').AsFloat);

    xFactor := GetConvertFactor(qrySum);
    DBGrid1.ColumnByName('calc_Red').FooterValue       := formatfloat(qu1calc_Red.DisplayFormat, FieldByName('c_Red').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_ManSt').FooterValue     := formatfloat(qu1calc_ManSt.DisplayFormat, FieldByName('c_ManSt').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_Sp').FooterValue        := formatfloat(qu1calc_Sp.DisplayFormat, FieldByName('c_Sp').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_Rsp').FooterValue       := formatfloat(qu1calc_Rsp.DisplayFormat, FieldByName('c_Rsp').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_YB').FooterValue        := formatfloat(qu1calc_YB.DisplayFormat, FieldByName('c_YB').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CS').FooterValue        := formatfloat(qu1calc_CS.DisplayFormat, FieldByName('c_CS').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CL').FooterValue        := formatfloat(qu1calc_CL.DisplayFormat, FieldByName('c_CL').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CT').FooterValue        := formatfloat(qu1calc_CT.DisplayFormat, FieldByName('c_CT').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CN').FooterValue        := formatfloat(qu1calc_CN.DisplayFormat, FieldByName('c_CN').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CSp').FooterValue       := formatfloat(qu1calc_CSp.DisplayFormat, FieldByName('c_CSp').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CClS').FooterValue      := formatfloat(qu1calc_CClS.DisplayFormat, FieldByName('c_CClS').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CClL').FooterValue      := formatfloat(qu1calc_CClL.DisplayFormat, FieldByName('c_CClL').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CClT').FooterValue      := formatfloat(qu1calc_CClT.DisplayFormat, FieldByName('c_CClT').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_UClS').FooterValue      := formatfloat(qu1calc_UClS.DisplayFormat, FieldByName('c_UClS').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_UClL').FooterValue      := formatfloat(qu1calc_UClL.DisplayFormat, FieldByName('c_UClL').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_UClT').FooterValue      := formatfloat(qu1calc_UClT.DisplayFormat, FieldByName('c_UClT').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_COffCnt').FooterValue           := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_COffCnt').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_COffCntPlus').FooterValue       := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_COffCntPlus').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_COffCntMinus').FooterValue      := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_COffCntMinus').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CShortOffCnt').FooterValue      := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_CShortOffCnt').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CShortOffCntPlus').FooterValue  := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_CShortOffCntPlus').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CShortOffCntMinus').FooterValue := formatfloat(qu1calc_COffCnt.DisplayFormat, FieldByName('c_CShortOffCntMinus').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CP').FooterValue          := formatfloat(qu1calc_CN.DisplayFormat, FieldByName('c_CP').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CBu').FooterValue         := formatfloat(qu1calc_CBu.DisplayFormat, FieldByName('c_CBu').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CDBu').FooterValue        := formatfloat(qu1calc_CDBu.DisplayFormat, FieldByName('c_CDBu').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CSys').FooterValue        := formatfloat(qu1calc_CSys.DisplayFormat, FieldByName('c_CSys').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CUpY').FooterValue        := formatfloat(qu1calc_CUpY.DisplayFormat, FieldByName('c_CUpY').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CYTot').FooterValue       := formatfloat(qu1calc_CYTot.DisplayFormat, FieldByName('c_CYTot').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CSiro').FooterValue       := formatfloat(qu1calc_CSiro.DisplayFormat, FieldByName('c_CSiro').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CSFi').FooterValue        := formatfloat(qu1calc_CSFi.DisplayFormat, FieldByName('c_CSFi').AsFloat * xFactor);
    DBGrid1.ColumnByName('calc_CVCV').FooterValue        := formatfloat(qu1calc_CVCV.DisplayFormat, FieldByName('c_CVCV').AsFloat * xFactor);   //Nue:18.02.08
    DBGrid1.ColumnByName('calc_CSiroCl').FooterValue     := formatfloat(qu1calc_CSiroCl.DisplayFormat, FieldByName('c_CSiroCl').AsFloat * xFactor);
    // Locks sind absolut und nicht L�ngen basiert -> direkt Feld c_Lck... verwenden ohne Berechnung
    DBGrid1.ColumnByName('c_LckSys').FooterValue         := formatfloat(qu1c_LckSys.DisplayFormat, fieldbyname('c_LckSys').AsFloat);
    DBGrid1.ColumnByName('c_LckSFI').FooterValue         := formatfloat(qu1c_LckSFI.DisplayFormat, fieldbyname('c_LckSFI').AsFloat);
    DBGrid1.ColumnByName('c_LckVCV').FooterValue         := formatfloat(qu1c_LckVCV.DisplayFormat, FieldByName('c_LckVCV').AsFloat * xFactor);   //Nue:18.02.08
    DBGrid1.ColumnByName('c_LckSIRO').FooterValue        := formatfloat(qu1c_LckSIRO.DisplayFormat, fieldbyname('c_LckSIRO').AsFloat);
    DBGrid1.ColumnByName('c_LckSIROCl').FooterValue      := formatfloat(qu1c_LckSIROCl.DisplayFormat, fieldbyname('c_LckSIROCl').AsFloat);
    DBGrid1.ColumnByName('c_LckOffCnt').FooterValue      := formatfloat(qu1c_LckOffCnt.DisplayFormat, fieldbyname('c_LckOffCnt').AsFloat);
    DBGrid1.ColumnByName('c_LckShortOffCnt').FooterValue := formatfloat(qu1c_LckShortOffCnt.DisplayFormat, fieldbyname('c_LckShortOffCnt').AsFloat);
    DBGrid1.ColumnByName('c_LckP').FooterValue           := formatfloat(qu1c_LckP.DisplayFormat, fieldbyname('c_LckP').AsFloat);
    DBGrid1.ColumnByName('c_LckClS').FooterValue         := formatfloat(qu1c_LckClS.DisplayFormat, fieldbyname('c_LckClS').AsFloat);
    DBGrid1.ColumnByName('c_LckClL').FooterValue         := formatfloat(qu1c_LckClL.DisplayFormat, fieldbyname('c_LckClL').AsFloat);
    DBGrid1.ColumnByName('c_LckClT').FooterValue         := formatfloat(qu1c_LckClT.DisplayFormat, fieldbyname('c_LckClT').AsFloat);
    DBGrid1.ColumnByName('c_LckNSLT').FooterValue        := formatfloat(qu1c_LckNSLT.DisplayFormat, fieldbyname('c_LckNSLT').AsFloat);

//    CodeSite.SendFmtMsg('c_SFI: %f; c_SFICnt: %f', [FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat]);
    xValue := CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsInteger);
    DBGrid1.ColumnByName('calc_SFI_D').FooterValue := formatfloat(qu1calc_SFI_D.DisplayFormat, xValue);

//Nue:18.02.08
    DBGrid1.ColumnByName('calc_CVd').FooterValue    := formatfloat(qu1calc_CVd.DisplayFormat, xValue * cVCVFactor);   //Nue:18.02.08

    if FieldByName('c_AdjustBaseCnt').AsInteger > 0 then
      xValue := CalcSFI(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat, FieldByName('c_AdjustBase').AsFloat / FieldByName('c_AdjustBaseCnt').AsFloat)
    else
      xValue := 0;
    DBGrid1.ColumnByName('calc_SFI').FooterValue := formatfloat(qu1calc_SFI.DisplayFormat, xValue);

    xValue := FieldByName('c_Len').AsFloat;
    if xValue > 0 then begin
      DBGrid1.ColumnByName('calc_INeps').FooterValue := formatfloat(qu1calc_INeps.DisplayFormat, FieldByName('c_INeps').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_IThick').FooterValue := formatfloat(qu1calc_IThick.DisplayFormat, FieldByName('c_IThick').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_IThin').FooterValue := formatfloat(qu1calc_IThin.DisplayFormat, FieldByName('c_IThin').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_I2_4').FooterValue := formatfloat(qu1calc_I2_4.DisplayFormat, FieldByName('c_I2_4').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_I4_8').FooterValue := formatfloat(qu1calc_I4_8.DisplayFormat, FieldByName('c_I4_8').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_I8_20').FooterValue := formatfloat(qu1calc_I8_20.DisplayFormat, FieldByName('c_I8_20').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_I20_70').FooterValue := formatfloat(qu1calc_I20_70.DisplayFormat, FieldByName('c_I20_70').AsFloat / xValue);
      DBGrid1.ColumnByName('calc_ISmall').FooterValue := formatfloat(qu1calc_ISmall.DisplayFormat, FieldByName('c_ISmall').AsFloat / xValue / 1000);
    end; //if xValue > 0 then begin
  finally
    Active := False;
  end; //try..finally
end; //procedure TEsp_MachineReportDlg.DBGrid1UpdateFooter
//-----------------------------------------------------------------------------
function TEsp_MachineReportDlg.GetConvertFactor(aDataset: TDataset): double;
var
  xBaseUnits: rBaseLengthUnitsT;
begin
//  xBaseUnits.Length := aDataset.fieldbyname('c_Len').AsInteger;
  xBaseUnits.Length := aDataset.fieldbyname('c_Len').AsFloat;
  xBaseUnits.Weight := aDataset.fieldbyname('c_Wei').AsFloat;
  xBaseUnits.Bobines := aDataset.fieldbyname('c_Bob').AsFloat;
  xBaseUnits.Cones := aDataset.fieldbyname('c_Cones').AsFloat;
  Result := GetLengthUnitFactor(xBaseUnits, mRepInfo.LengthUnit);
end; //function TEsp_MachineReportDlg.GetConvertFactor

procedure TEsp_MachineReportDlg.qu1BeforeOpen(DataSet: TDataSet);
begin
//  SetDefaultMaskInDisplayFormat(qu1);
end;

procedure TEsp_MachineReportDlg.qu1c_LStProdGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  Text := Format_hmm(Sender.AsInteger);
end;

end. //u_esp_machine_report

