inherited OffLimitEditorDlg: TOffLimitEditorDlg
  Left = 253
  Top = 127
  Width = 868
  Height = 592
  BorderIcons = [biSystemMenu]
  Caption = '(100)Offlimit-Einstellungen bearbeiten'
  Constraints.MinHeight = 400
  Constraints.MinWidth = 671
  KeyPreview = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 860
    Height = 26
    ButtonWidth = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton1: TToolButton
      Left = 29
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bPrint: TToolButton
      Left = 37
      Top = 0
      Action = acPrint
    end
    object ToolButton3: TToolButton
      Left = 63
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object bCopy: TToolButton
      Left = 71
      Top = 0
      Action = acCopy
      Visible = False
    end
    object DBNavigator1: TmmDBNavigator
      Left = 97
      Top = 0
      Width = 200
      Height = 22
      DataSource = srcOfflimitSettings
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object bSecurity: TmmSpeedButton
      Left = 297
      Top = 0
      Width = 102
      Height = 22
      Action = acSecurity
      Flat = True
      Transparent = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object ToolButton4: TToolButton
      Left = 399
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bHelp: TToolButton
      Left = 407
      Top = 0
      Action = acHelp
    end
  end
  object Panel1: TmmPanel
    Left = 0
    Top = 26
    Width = 860
    Height = 539
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 1
    object PageControl: TmmPageControl
      Left = 2
      Top = 2
      Width = 856
      Height = 535
      ActivePage = tsAssignment
      Align = alClient
      HotTrack = True
      TabOrder = 0
      TabStop = False
      OnChange = PageControlChange
      OnChanging = PageControlChanging
      object tsFormular: TTabSheet
        BorderWidth = 2
        Caption = '(20)Formular'
        object gbOfflimitName: TmmGroupBox
          Left = 0
          Top = 0
          Width = 844
          Height = 137
          Align = alTop
          Caption = '(60)Allgemeine Informationen'
          TabOrder = 0
          CaptionSpace = True
          object laOfflimitName: TmmLabel
            Left = 8
            Top = 22
            Width = 186
            Height = 13
            Caption = '(60)Bezeichnung der Offlimit-Einstellung'
            FocusControl = DBEdit1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laC_Average_time: TmmLabel
            Left = 379
            Top = 96
            Width = 210
            Height = 13
            AutoSize = False
            Caption = '(60)Zeitdauer fuer Mittelwertsbildung (min)'
            FocusControl = edAverageTime
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laLenBaseTitle: TmmLabel
            Left = 26
            Top = 96
            Width = 116
            Height = 13
            AutoSize = False
            Caption = '(20)Basierend auf'
            FocusControl = laLenBase
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object bChangeLenBase: TmmSpeedButton
            Left = 248
            Top = 92
            Width = 73
            Height = 21
            Caption = '(*)Aendern'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGreen
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = True
            OnClick = bChangeLenBaseClick
            AutoLabel.LabelPosition = lpLeft
          end
          object laLenBase: TmmLabel
            Left = 144
            Top = 96
            Width = 8
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            Visible = True
            AutoLabel.Control = laLenBaseTitle
            AutoLabel.LabelPosition = lpLeft
          end
          object DBEdit1: TmmDBEdit
            Left = 8
            Top = 37
            Width = 295
            Height = 21
            Color = clWindow
            DataField = 'c_maoffset_name'
            DataSource = srcOfflimitSettings
            TabOrder = 0
            Visible = True
            AutoLabel.Control = laOfflimitName
            AutoLabel.LabelPosition = lpTop
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object DBCheckBox3: TmmDBCheckBox
            Left = 360
            Top = 72
            Width = 300
            Height = 17
            Caption = '(80)Ueberwachung der prozentualen Grenzwerte aktivieren'
            DataField = 'c_OffBorderByAverage'
            DataSource = srcOfflimitSettings
            TabOrder = 1
            ValueChecked = 'Wahr'
            ValueUnchecked = 'Falsch'
          end
          object DBCheckBox2: TmmDBCheckBox
            Left = 8
            Top = 72
            Width = 300
            Height = 17
            Caption = '(80)Ueberwachung der absoluten Grenzwerte aktivieren'
            DataField = 'c_OffBorderByFixValues'
            DataSource = srcOfflimitSettings
            TabOrder = 2
            ValueChecked = 'Wahr'
            ValueUnchecked = 'Falsch'
          end
          object edAverageTime: TmmDBEdit
            Left = 591
            Top = 92
            Width = 80
            Height = 21
            Color = clWindow
            DataField = 'c_Average_time'
            DataSource = srcOfflimitSettings
            TabOrder = 3
            Visible = True
            AutoLabel.Control = laC_Average_time
            AutoLabel.LabelPosition = lpLeft
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
        end
        object GroupBox3: TmmGroupBox
          Left = 0
          Top = 137
          Width = 844
          Height = 543
          Align = alTop
          Caption = '(60)Grenzwerte'
          TabOrder = 1
          CaptionSpace = True
          object paGroup: TmmPanel
            Left = 2
            Top = 15
            Width = 840
            Height = 526
            Align = alClient
            BevelOuter = bvNone
            BorderWidth = 5
            TabOrder = 0
            object ScrollBox1: TmmScrollBox
              Left = 5
              Top = 40
              Width = 830
              Height = 481
              HorzScrollBar.Range = 580
              VertScrollBar.Range = 269
              Align = alClient
              AutoScroll = False
              BorderStyle = bsNone
              TabOrder = 0
              object laMaEff: TmmLabel
                Left = 5
                Top = 9
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Maschinen-Nutzeffekt (%MEff)'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object laCYTot: TmmLabel
                Left = 5
                Top = 132
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Gesamt-Reinigerschnitte'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label10: TmmLabel
                Left = 5
                Top = 157
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)SIRO-Schnitte'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label9: TmmLabel
                Left = 5
                Top = 182
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Spleiss-Schnitte'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label15: TmmLabel
                Left = 5
                Top = 207
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Spleiss-Wiederholungen'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label8: TmmLabel
                Left = 5
                Top = 282
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Fadenbrueche'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label11: TmmLabel
                Left = 5
                Top = 232
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Schlingen-Schnitte'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label13: TmmLabel
                Left = 5
                Top = 257
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Oberfaden-Schnitte'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label16: TmmLabel
                Left = 5
                Top = 37
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Lang-Stop (Dauer/Intervall)'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object Label17: TmmLabel
                Left = 5
                Top = 62
                Width = 220
                Height = 13
                AutoSize = False
                Caption = '(60)Lang-Stop (Anzahl/Intervall)'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object DBEdit4: TmmDBEdit
                Left = 250
                Top = 5
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_Eff_min'
                DataSource = srcOfflimitSettings
                TabOrder = 0
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit5: TmmDBEdit
                Left = 340
                Top = 5
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_Eff_max'
                DataSource = srcOfflimitSettings
                TabOrder = 1
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit6: TmmDBEdit
                Left = 430
                Top = 5
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_Eff_PC_min'
                DataSource = srcOfflimitSettings
                TabOrder = 2
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit7: TmmDBEdit
                Left = 520
                Top = 5
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_Eff_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 3
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit11: TmmDBEdit
                Left = 520
                Top = 128
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CYTot_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 6
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit13: TmmDBEdit
                Left = 520
                Top = 153
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CSIRO_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 8
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit15: TmmDBEdit
                Left = 520
                Top = 178
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CSp_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 10
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit17: TmmDBEdit
                Left = 520
                Top = 203
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_RSp_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 12
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit23: TmmDBEdit
                Left = 520
                Top = 278
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_YB_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 14
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit19: TmmDBEdit
                Left = 520
                Top = 228
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CBu_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 16
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit21: TmmDBEdit
                Left = 520
                Top = 253
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CUpY_PC_max'
                DataSource = srcOfflimitSettings
                TabOrder = 18
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit24: TmmDBEdit
                Left = 340
                Top = 33
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_tLSt_max'
                DataSource = srcOfflimitSettings
                TabOrder = 19
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit26: TmmDBEdit
                Left = 340
                Top = 58
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_LSt_max'
                DataSource = srcOfflimitSettings
                TabOrder = 20
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object Panel2: TmmPanel
                Left = 235
                Top = 86
                Width = 180
                Height = 35
                BevelOuter = bvNone
                Color = 13290186
                TabOrder = 21
                object laLowBorder: TmmLabel
                  Left = 0
                  Top = 0
                  Width = 90
                  Height = 35
                  Align = alLeft
                  Alignment = taCenter
                  AutoSize = False
                  Caption = '(30)untere Grenze (/%s)'
                  Color = clSilver
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                  Layout = tlCenter
                  Visible = True
                  WordWrap = True
                  AutoLabel.LabelPosition = lpLeft
                end
                object laHighBorder: TmmLabel
                  Left = 90
                  Top = 0
                  Width = 90
                  Height = 35
                  Align = alLeft
                  Alignment = taCenter
                  AutoSize = False
                  Caption = '(30)obere Grenze (/%s)'
                  Color = clSilver
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentColor = False
                  ParentFont = False
                  Transparent = True
                  Layout = tlCenter
                  Visible = True
                  WordWrap = True
                  AutoLabel.LabelPosition = lpLeft
                end
              end
              object DBEdit8: TmmDBEdit
                Left = 250
                Top = 128
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CYTot_min'
                DataSource = srcOfflimitSettings
                TabOrder = 4
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit12: TmmDBEdit
                Left = 340
                Top = 153
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CSIRO_max'
                DataSource = srcOfflimitSettings
                TabOrder = 7
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit14: TmmDBEdit
                Left = 340
                Top = 178
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CSp_max'
                DataSource = srcOfflimitSettings
                TabOrder = 9
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit16: TmmDBEdit
                Left = 340
                Top = 203
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_RSp_max'
                DataSource = srcOfflimitSettings
                TabOrder = 11
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit22: TmmDBEdit
                Left = 340
                Top = 278
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_YB_max'
                DataSource = srcOfflimitSettings
                TabOrder = 13
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit18: TmmDBEdit
                Left = 340
                Top = 228
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CBu_max'
                DataSource = srcOfflimitSettings
                TabOrder = 15
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit20: TmmDBEdit
                Left = 340
                Top = 253
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CUpY_max'
                DataSource = srcOfflimitSettings
                TabOrder = 17
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object DBEdit9: TmmDBEdit
                Left = 340
                Top = 128
                Width = 60
                Height = 21
                Color = clWindow
                DataField = 'c_CYTot_max'
                DataSource = srcOfflimitSettings
                TabOrder = 5
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
            end
            object mmPanel1: TmmPanel
              Left = 5
              Top = 5
              Width = 830
              Height = 35
              Align = alTop
              BevelOuter = bvNone
              Color = 13290186
              TabOrder = 1
              object mmLabel1: TmmLabel
                Left = 0
                Top = 0
                Width = 235
                Height = 35
                Align = alLeft
                AutoSize = False
                Caption = '(60)Parameter'
                Color = clSilver
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                Layout = tlCenter
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object mmLabel2: TmmLabel
                Left = 235
                Top = 0
                Width = 90
                Height = 35
                Align = alLeft
                Alignment = taCenter
                AutoSize = False
                Caption = '(30)untere Grenze absolut'
                Color = clSilver
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                Layout = tlCenter
                Visible = True
                WordWrap = True
                AutoLabel.LabelPosition = lpLeft
              end
              object mmLabel3: TmmLabel
                Left = 325
                Top = 0
                Width = 90
                Height = 35
                Align = alLeft
                Alignment = taCenter
                AutoSize = False
                Caption = '(30)obere Grenze absolut'
                Color = clSilver
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                Layout = tlCenter
                Visible = True
                WordWrap = True
                AutoLabel.LabelPosition = lpLeft
              end
              object mmLabel4: TmmLabel
                Left = 415
                Top = 0
                Width = 90
                Height = 35
                Align = alLeft
                Alignment = taCenter
                AutoSize = False
                Caption = '(30)untere Grenze prozentual'
                Color = clSilver
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                Layout = tlCenter
                Visible = True
                WordWrap = True
                AutoLabel.LabelPosition = lpLeft
              end
              object mmLabel5: TmmLabel
                Left = 505
                Top = 0
                Width = 90
                Height = 35
                Align = alLeft
                Alignment = taCenter
                AutoSize = False
                Caption = '(30)obere Grenze prozentual'
                Color = clSilver
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentColor = False
                ParentFont = False
                Transparent = True
                Layout = tlCenter
                Visible = True
                WordWrap = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
          end
        end
      end
      object tsGrid: TTabSheet
        Caption = '(40)Tabellensicht'
        ImageIndex = 1
        object wwDBGrid1: TwwDBGrid
          Left = 0
          Top = 0
          Width = 848
          Height = 507
          Selected.Strings = (
            'c_maoffset_name'#9'40'#9'c_maoffset_name'#9#9
            'c_number_of_machines'#9'8'#9'c_number~of~machines'#9#9
            'c_Average_time'#9'10'#9'c_Average~time'#9#9
            'c_OffBorderByAverage'#9'5'#9'c_OffBorder~ByAverage'#9#9
            'c_OffBorderByFixValues'#9'5'#9'c_OffBorder~ByFixValues'#9#9
            'c_Eff_min'#9'10'#9'c_Eff~min'#9#9
            'c_Eff_max'#9'10'#9'c_Eff~max'#9#9
            'c_tLSt_max'#9'10'#9'c_tLSt~max'#9#9
            'c_LSt_max'#9'10'#9'c_LSt~max'#9#9
            'c_CYTot_min'#9'10'#9'c_CYTot~min'#9#9
            'c_CYTot_max'#9'10'#9'c_CYTot~max'#9#9
            'c_CSIRO_max'#9'10'#9'c_CSIRO~max'#9#9
            'c_CSp_max'#9'10'#9'c_CSp~max'#9#9
            'c_RSp_max'#9'10'#9'c_RSp~max'#9#9
            'c_YB_Max'#9'10'#9'c_YB~Max'#9#9
            'c_CBu_max'#9'10'#9'c_CBu~max'#9#9
            'c_CUpY_max'#9'10'#9'c_CUpY~max'#9#9
            'c_Eff_PC_min'#9'10'#9'c_Eff~PC_min'#9#9
            'c_Eff_PC_max'#9'10'#9'c_Eff~PC_max'#9#9
            'c_CYTot_PC_min'#9'10'#9'c_CYTot~PC_min'#9#9
            'c_CYTot_PC_max'#9'10'#9'c_CYTot~PC_max'#9#9
            'c_CSIRO_PC_max'#9'10'#9'c_CSIRO~PC_max'#9#9
            'c_CSp_PC_max'#9'10'#9'c_CSp~PC_max'#9#9
            'c_RSp_PC_max'#9'10'#9'c_RSp~PC_max'#9#9
            'c_YB_PC_max'#9'10'#9'c_YB~PC_max'#9#9
            'c_CBu_PC_max'#9'10'#9'c_CBu~PC_max'#9#9
            'c_CUpY_PC_max'#9'10'#9'c_CUpY~PC_max'#9#9)
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = srcOfflimitSettings
          KeyOptions = [dgEnterToTab]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis]
          ReadOnly = True
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 3
          TitleButtons = False
          OnCalcCellColors = wwDBGrid1CalcCellColors
          OnDblClick = wwDBGrid1DblClick
        end
      end
      object tsAssignment: TTabSheet
        Caption = '(40)Aktuelle Offlimit-Einstellung'
        ImageIndex = 2
        object dgMaOfflimit: TmmDBGrid
          Left = 0
          Top = 0
          Width = 848
          Height = 507
          Align = alClient
          DataSource = srcMachine
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
      end
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 416
    Top = 24
    TargetsData = (
      1
      12
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'Filter'
        0)
      (
        'TwwDBGrid'
        '*'
        0)
      (
        'TFields'
        '*'
        0)
      (
        '*'
        'Title'
        0)
      (
        '*'
        'DisplayValues'
        0)
      (
        '*'
        'Cells'
        0))
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 384
    Top = 24
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Einstellungen ausdrucken'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acCopy: TAction
      Hint = '(*)Einstellungen kopieren...'
      ImageIndex = 36
      OnExecute = acCopyExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acEditBaseLength: TAction
      Hint = '(60)Basislaenge fuer Mittelwertsbildung (m)'
    end
    object acEditAverageTime: TAction
      Hint = '(60)Zeitdauer fuer Mittelwertsbildung (min)'
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      OnExecute = acSecurityExecute
    end
    object acEditOfflimit: TAction
      Hint = '(*)Offlimit-Einstellungen bearbeiten...'
    end
  end
  object srcOfflimitSettings: TmmDataSource
    DataSet = tabOfflimitSettings
    Left = 440
    Top = 80
  end
  object qryWork: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CommandText = 
      'SELECT COUNT(*) AS c_number_of_machines FROM t_machine WHERE c_m' +
      'aoffset_id = :c_maoffset_id '
    Parameters = <
      item
        Name = 'c_maoffset_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 544
    Top = 24
  end
  object tabMachine: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CursorType = ctStatic
    Filter = 'c_node_id <> 0'
    Filtered = True
    BeforeInsert = tabMachineBeforeDelete
    AfterPost = tabMachineAfterPost
    BeforeDelete = tabMachineBeforeDelete
    CommandText = 't_machine'
    CommandType = cmdTable
    FieldDefs = <>
    IndexFieldNames = 'c_machine_name'
    Parameters = <>
    StoreDefs = True
    Left = 480
    Top = 24
    object tabMachinec_machine_name: TStringField
      DisplayLabel = '(*)Maschine'
      DisplayWidth = 10
      FieldName = 'c_machine_name'
      ReadOnly = True
      Required = True
      Size = 10
    end
    object tabMachinelookMaTyp: TStringField
      DisplayLabel = '(*)Maschinentyp'
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'lookMaType'
      ReadOnly = True
      OnGetText = tabMachinelookMaTypGetText
      Calculated = True
    end
    object tabMachinec_nr_of_spindles: TSmallintField
      Alignment = taLeftJustify
      DisplayLabel = '(*)Anzahl Spulstellen'
      DisplayWidth = 25
      FieldName = 'c_nr_of_spindles'
      ReadOnly = True
      Required = True
    end
    object tabMachinelookMaOfflimitName: TStringField
      DisplayLabel = '(*)Offlimit-Einstellung'
      DisplayWidth = 40
      FieldKind = fkLookup
      FieldName = 'lookMaOfflimit'
      LookupDataSet = tabOfflimitSettings
      LookupKeyFields = 'c_maoffset_id'
      LookupResultField = 'c_maoffset_name'
      KeyFields = 'c_maoffset_id'
      Lookup = True
    end
    object tabMachinec_maoffset_id: TIntegerField
      FieldName = 'c_maoffset_id'
      Required = True
      Visible = False
    end
    object tabMachinec_machine_type: TSmallintField
      FieldName = 'c_machine_type'
      ReadOnly = True
      Required = True
      Visible = False
    end
    object tabMachinec_node_id: TStringField
      FieldName = 'c_node_id'
      Required = True
      Visible = False
      Size = 16
    end
  end
  object srcMachine: TmmDataSource
    DataSet = tabMachine
    Left = 536
    Top = 80
  end
  object MMSecurityControl: TMMSecurityControl
    FormCaption = '(100)Offlimit-Einstellungen bearbeiten'
    Active = True
    Left = 352
    Top = 24
  end
  object tabOfflimitSettings: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CursorType = ctStatic
    BeforeEdit = tabOfflimitSettingsBeforeEdit
    BeforePost = tabOfflimitSettingsBeforePost
    BeforeDelete = tabOfflimitSettingsBeforeDelete
    AfterScroll = tabOfflimitSettingsAfterScroll
    OnCalcFields = tabOfflimitSettingsCalcFields
    OnNewRecord = tabOfflimitSettingsNewRecord
    CommandText = 't_machine_offlimit_settings'
    CommandType = cmdTable
    Parameters = <>
    Left = 448
    Top = 24
    object tabOfflimitSettingsc_maoffset_name: TStringField
      DisplayWidth = 40
      FieldName = 'c_maoffset_name'
      Size = 40
    end
    object tabOfflimitSettingsc_number_of_machines: TIntegerField
      DisplayLabel = 'c_number~of~machines'
      DisplayWidth = 8
      FieldKind = fkCalculated
      FieldName = 'c_number_of_machines'
      Calculated = True
    end
    object tabOfflimitSettingsc_Average_time: TIntegerField
      DisplayLabel = 'c_Average~time'
      DisplayWidth = 10
      FieldName = 'c_Average_time'
      DisplayFormat = ',0 min'
    end
    object tabOfflimitSettingsc_OffBorderByAverage: TBooleanField
      DisplayLabel = 'c_OffBorder~ByAverage'
      DisplayWidth = 5
      FieldName = 'c_OffBorderByAverage'
    end
    object tabOfflimitSettingsc_OffBorderByFixValues: TBooleanField
      DisplayLabel = 'c_OffBorder~ByFixValues'
      DisplayWidth = 5
      FieldName = 'c_OffBorderByFixValues'
    end
    object tabOfflimitSettingsc_Eff_min: TIntegerField
      DisplayLabel = 'c_Eff~min'
      DisplayWidth = 10
      FieldName = 'c_Eff_min'
      DisplayFormat = ',0'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_Eff_max: TIntegerField
      DisplayLabel = 'c_Eff~max'
      DisplayWidth = 10
      FieldName = 'c_Eff_max'
      DisplayFormat = ',0'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_tLSt_max: TIntegerField
      DisplayLabel = 'c_tLSt~max'
      DisplayWidth = 10
      FieldName = 'c_tLSt_max'
      DisplayFormat = ',0'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_LSt_max: TIntegerField
      DisplayLabel = 'c_LSt~max'
      DisplayWidth = 10
      FieldName = 'c_LSt_max'
      DisplayFormat = ',0'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CYTot_min: TFloatField
      DisplayLabel = 'c_CYTot~min'
      DisplayWidth = 10
      FieldName = 'c_CYTot_min'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_CYTot_max: TFloatField
      DisplayLabel = 'c_CYTot~max'
      DisplayWidth = 10
      FieldName = 'c_CYTot_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_CSIRO_max: TFloatField
      DisplayLabel = 'c_CSIRO~max'
      DisplayWidth = 10
      FieldName = 'c_CSIRO_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_CSp_max: TFloatField
      DisplayLabel = 'c_CSp~max'
      DisplayWidth = 10
      FieldName = 'c_CSp_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_RSp_max: TFloatField
      DisplayLabel = 'c_RSp~max'
      DisplayWidth = 10
      FieldName = 'c_RSp_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_YB_Max: TFloatField
      DisplayLabel = 'c_YB~Max'
      DisplayWidth = 10
      FieldName = 'c_YB_Max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_CBu_max: TFloatField
      DisplayLabel = 'c_CBu~max'
      DisplayWidth = 10
      FieldName = 'c_CBu_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_CUpY_max: TFloatField
      DisplayLabel = 'c_CUpY~max'
      DisplayWidth = 10
      FieldName = 'c_CUpY_max'
      DisplayFormat = ',0.0'
      EditFormat = '0.0'
    end
    object tabOfflimitSettingsc_Eff_PC_min: TIntegerField
      DisplayLabel = 'c_Eff~PC_min'
      DisplayWidth = 10
      FieldName = 'c_Eff_PC_min'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_Eff_PC_max: TIntegerField
      DisplayLabel = 'c_Eff~PC_max'
      DisplayWidth = 10
      FieldName = 'c_Eff_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CYTot_PC_minXX: TIntegerField
      DisplayLabel = 'c_CYTot~PC_min'
      DisplayWidth = 10
      FieldName = 'c_CYTot_PC_min'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CYTot_PC_max: TIntegerField
      DisplayLabel = 'c_CYTot~PC_max'
      DisplayWidth = 10
      FieldName = 'c_CYTot_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CSIRO_PC_max: TIntegerField
      DisplayLabel = 'c_CSIRO~PC_max'
      DisplayWidth = 10
      FieldName = 'c_CSIRO_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CSp_PC_max: TIntegerField
      DisplayLabel = 'c_CSp~PC_max'
      DisplayWidth = 10
      FieldName = 'c_CSp_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_RSp_PC_max: TIntegerField
      DisplayLabel = 'c_RSp~PC_max'
      DisplayWidth = 10
      FieldName = 'c_RSp_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_YB_PC_max: TIntegerField
      DisplayLabel = 'c_YB~PC_max'
      DisplayWidth = 10
      FieldName = 'c_YB_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CBu_PC_max: TIntegerField
      DisplayLabel = 'c_CBu~PC_max'
      DisplayWidth = 10
      FieldName = 'c_CBu_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingsc_CUpY_PC_max: TIntegerField
      DisplayLabel = 'c_CUpY~PC_max'
      DisplayWidth = 10
      FieldName = 'c_CUpY_PC_max'
      DisplayFormat = ',0 %'
      EditFormat = '0'
    end
    object tabOfflimitSettingscalc_base: TStringField
      DisplayLabel = '(40)c_base'
      FieldKind = fkCalculated
      FieldName = 'calc_base'
      Visible = False
      Size = 12
      Calculated = True
    end
    object tabOfflimitSettingsc_base: TIntegerField
      DisplayLabel = '0'
      FieldName = 'c_base'
      Visible = False
    end
    object tabOfflimitSettingsc_maoffset_id: TIntegerField
      DisplayLabel = '0'
      DisplayWidth = 5
      FieldName = 'c_maoffset_id'
      Visible = False
    end
    object tabOfflimitSettingsc_ProdGrp_depend: TBooleanField
      FieldName = 'c_ProdGrp_depend'
      Visible = False
    end
  end
  object pmLenBase: TmmPopupMenu
    Left = 328
    Top = 142
    object N100km1: TMenuItem
      Tag = 1
      Caption = '100 km'
      OnClick = pmLenBaseClick
    end
    object N1000km2: TMenuItem
      Tag = 2
      Caption = '1000 km'
      OnClick = pmLenBaseClick
    end
    object N10000yrd1: TMenuItem
      Tag = 3
      Caption = '100.000 yrd'
      OnClick = pmLenBaseClick
    end
    object N1000km1: TMenuItem
      Tag = 4
      Caption = '1.000.000 yrd'
      OnClick = pmLenBaseClick
    end
  end
  object comUpdateHide: TmmADOCommand
    CommandText = 
      'update t_machine_offlimit set'#13#10'  c_update_time = GetDate(), '#13#10'  ' +
      'c_max_online_offlimit_time = 0'#13#10'from t_machine m, t_machine_offl' +
      'imit mao'#13#10'where m.c_maoffset_id = :OfflimitId'#13#10'and m.c_machine_i' +
      'd = mao.c_machine_id'#13#10
    Connection = dmSpindleReport.conDefault
    Parameters = <
      item
        Name = 'OfflimitId'
        Size = -1
        Value = Null
      end>
    Left = 440
    Top = 134
  end
end
