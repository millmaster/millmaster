(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_esp_spindle_report.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.04.99 | 0.00 | PW  | File created
| 22.05.99 | 0.01 | PW  | SFI, SFI/D added
|                         Classification added
| 21.01.00 | 0.02 | PW  | TReportClassMatrix and TReportClearerSettings added
| 11.02.00 | 1.00 | Mg  | PEff = tRu / tWa in qu1.CalcFields
| 08.05.00 | 1.01 | PW  | TmSettingsDialog added
| 04.07.00 | 1.02 | SDO | New property  Parameter and new func.ShowSettedGraphic
|                       | Is using for take over the grapfic parameters from TEsp_MachineRep
| 06.07.00 | 1.03 | SDO | New property ShowGraphicOptions
| 07.07.00 | 1.04 | SDo | Imperfections NICHT mal 1000 rechnen   [ CalcAnteil() ]
| 08.08.00 | 1.05 | Mg  | constructor deleted
| 23.08.00 | 1.06 | Mg  | New components of clearer settings inserted
| 05.09.00 | 1.07 | Mg  | ISmall div 1000
| 12.09.00 | 1.08 | Mg  | Work around
| 06.11.00 | 1.09 | Wss | MemoIni.Applicationname set to gApplicationname to solve 8.3 naming problems
| 03.09.01 | 1.09 | Wss | Show Cut/Defects as default
| 18.09.01 | 1.09 | Wss | Use of property MatrixMode changed to DisplayMode for coarse, fine view
| 29.10.01 | 1.10 | Wss | qu1 contained a statement AND pg.c_style_id <> 2 -> removed (??)
| 08.11.01 | 1.11 | Wss | calculation for qu1calc_PCSp %CSp corrected: base is SP and not CYTot
| 12.11.01 | 1.11 | Wss | calculation for qu1calc_SFI corrected
| 22.11.01 | 1.11 | Wss | MemoryLeak: object mParameters was overriden at reading parameters
                          mParameters := TParameters.Create;
                          mParameters := ReadParam(); <-- forbidden !!!!
| 26.11.01 | 1.11 | Wss | base of Bob, Cones values changed to float, without factor 100
| 13.12.01 | 1.15 | Wss | calculation SFI/D and SFI improved/added
| 26.02.02   1.02   Wss | - QMatix mit TSizingPanel in richtigen Proportionen darstellen
                          - ActiveVisible war nicht gesetzt, wenn Klassiertyp = Garn
| 04.03.02   1.02   Wss | TSizingPanel nicht mehr noetig, da RepQMatrix Komponente Verhaeltnis
                          nun selber abhandelt
| 15.03.02 | 1.16 | Wss | qu1 umgestellt auf Views um Faktor fuer Imperfektionen zu verwenden
| 02.04.02 | 1.16 | Wss | Siro Matrix hatte noch Fine Werte dargestellt als default -> auf Coarse umgestellt
| 21.05.02 | 1.16 | Wss | Uebersetzung fuer SFI Feld wurde immernoch SFI/D genommen
| 18.10.02 |      | LOK | Umbau ADO
| 15.11.02 | 1.16 | Wss | Kleinere Fixes
| 11.09.03 | 1.16 | Wss | Irgendein Bug in der Toolbar bewirkte, dass der Print Knopf ebenfalls
                          ausgeblendet wurde, wenn die SecurityControl auf Hide gesetzt wurde
| 25.03.04 | 1.16 | Wss | Beim Drucken der Klassiermatrix und Settings wird der Form Caption
                          tempor�r in ein Label kopiert, damit beim Print diese Infos ebenfalls
                          mit auf dem Papier sind.
| 18.03.05 | 1.16 | Wss | Maschinenmodell wird nicht mehr ben�tgit
| 10.06.05 | 1.16 | Wss | - Spectra+ und Zenit Elemente hinzugef�gt
                          - Korrektur f�r Locks welche immer absolut angezeigt werden
| 18.02.08 | 1.20 | Nue | VCV added.
| 18.09.08 | 1.21 | Nue | ChannelMatrix.ActiveColor gesetzt wegen Einbau Spliceclassification.
|=========================================================================================*)

{$I symbols.inc}                     

unit u_esp_spindle_report;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, BASEAPPLMAIN,
  BASEFORM, mmGraphGlobal, mmGraph, ComCtrls, ExtCtrls, ActnList, ToolWin, u_global, Db,
  Buttons, Menus, mmLengthUnit, Grids, DBGrids, IvDictio, IvMulti,
  IvEMulti, mmActionList, mmSpeedButton, mmPageControl, mmPanel, mmToolBar,
  mmTranslator, mmPopupMenu, StdCtrls, mmRadioButton, mmGroupBox,
  mmRadioGroup, mmCheckBox, mmButton, MMUGlobal, MMSecurity, QualityMatrixBase,
  mmStringGrid, MemoINI, mmScrollBox, Nwcombo, LoepfeGlobal,
  RepSettingsUnit, Printers, mmComboBox,
  ADODB, mmADODataSet, ClassDataReader, mmLabel, QualityMatrix,
  XMLSettingsModel, VCLXMLSettingsModel;

type
  TEsp_SpindleReportDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    bPrintRange: TToolButton;
    bHelp: TToolButton;
    bGraphOptions: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    bSort: TToolButton;
    bFormat: TToolButton;
    bShowKlassTrend: TToolButton;
    bSelectMode: TToolButton;

    ActionList1: TmmActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acShowPrevSpindle: TAction;
    acShowNextSpindle: TAction;
    acGraphOptions: TAction;
    acSortGraph: TAction;
    acSaveProfile: TAction;
    acLoadProfile: TAction;
    acFormat: TAction;
    acShowKlassTrend: TAction;
    acSelectMode: TAction;
    acDeleteProfile: TAction;
    acSaveAsDefault: TAction;

    mmLengthUnits1: TmmLengthUnits;
    quMach: TmmADODataSet;
    quMachc_machine_name: TStringField;
    quMachc_nr_of_spindles: TSmallintField;
    dseData: TmmADODataSet;
    dseDatac_spindle_id: TSmallintField;
    dseDatacalc_npProdGrp: TIntegerField;
    dseDatacalc_MEff: TFloatField;
    dseDatacalc_PEff: TFloatField;
    dseDatac_Len: TFloatField;
    dseDatac_Wei: TFloatField;
    dseDatac_Bob: TFloatField;
    dseDatac_Cones: TFloatField;
    dseDatacalc_CYTOT: TFloatField;
    dseDatacalc_Sp: TFloatField;
    dseDatacalc_YB: TFloatField;
    dseDatacalc_CN: TFloatField;
    dseDatacalc_CS: TFloatField;
    dseDatacalc_CL: TFloatField;
    dseDatacalc_CT: TFloatField;
    dseDatacalc_CSIRO: TFloatField;
    dseDatacalc_COffCnt: TFloatField;
    dseDatacalc_CClS: TFloatField;
    dseDatacalc_LStTOT: TIntegerField;
    dseDatacalc_CUPY: TFloatField;
    dseDatacalc_CBU: TFloatField;
    dseDatacalc_CSYS: TFloatField;
    dseDatacalc_CSP: TFloatField;
    dseDatacalc_RSP: TFloatField;
    dseDatacalc_PCSp: TFloatField;
    dseDatacalc_PRSp: TFloatField;
    dseDatacalc_Red: TFloatField;
    dseDatacalc_ManSt: TFloatField;
    dseDatacalc_UClS: TFloatField;
    dseDatacalc_UClL: TFloatField;
    dseDatacalc_UClT: TFloatField;
    dseDatacalc_CDBU: TFloatField;
    dseDatac_interval_start: TDateTimeField;
    dseDatac_interval_end: TDateTimeField;

    pmProfile: TmmPopupMenu;
    miGraphOptions: TMenuItem;
    N2: TMenuItem;
    miLoadProfile: TMenuItem;
    miSaveProfile: TMenuItem;
    miDeleteProfile: TMenuItem;
    N1: TMenuItem;
    miSaveAsDefault: TMenuItem;
    bPeriod: TToolButton;
    acPeriod: TAction;
    acSecurity: TAction;
    MMSecurityControl: TMMSecurityControl;
    mmSpeedButton1: TmmSpeedButton;
    dseDatacalc_INeps: TFloatField;
    dseDatacalc_IThick: TFloatField;
    dseDatacalc_IThin: TFloatField;
    dseDatacalc_ISmall: TFloatField;
    dseDatacalc_I2_4: TFloatField;
    dseDatacalc_I4_8: TFloatField;
    dseDatacalc_I8_20: TFloatField;
    dseDatacalc_I20_70: TFloatField;
    dseDatacalc_SFI_D: TFloatField;
    dseDatacalc_CSFI: TFloatField;
    dseDatacalc_CSIROCl: TFloatField;
    dseDatac_UClS: TFloatField;
    dseDatac_UClL: TFloatField;
    dseDatac_UClT: TFloatField;
    mmTranslator: TmmTranslator;
    dseDatac_AdjustBase: TFloatField;
    dseDatacalc_SFI: TFloatField;
    dseDatac_INeps: TFloatField;
    dseDatac_IThick: TFloatField;
    dseDatac_IThin: TFloatField;
    dseDatac_ISmall: TFloatField;
    mCDR: TClassDataReader;
    dseDatac_Red: TBCDField;
    dseDatac_tRed: TBCDField;
    dseDatac_tYell: TBCDField;
    dseDatac_ManSt: TBCDField;
    dseDatac_tManSt: TBCDField;
    dseDatac_rtSpd: TBCDField;
    dseDatac_otSpd: TBCDField;
    dseDatac_wtSpd: TBCDField;
    dseDatac_Sp: TBCDField;
    dseDatac_RSp: TBCDField;
    dseDatac_YB: TBCDField;
    dseDatac_LSt: TBCDField;
    dseDatac_LStProd: TBCDField;
    dseDatac_LStBreak: TBCDField;
    dseDatac_LStMat: TBCDField;
    dseDatac_LStRev: TBCDField;
    dseDatac_LStClean: TBCDField;
    dseDatac_LStDef: TBCDField;
    dseDatac_CS: TBCDField;
    dseDatac_CL: TBCDField;
    dseDatac_CT: TBCDField;
    dseDatac_CN: TBCDField;
    dseDatac_CSp: TBCDField;
    dseDatac_CClS: TBCDField;
    dseDatac_COffCnt: TBCDField;
    dseDatac_LckOffCnt: TBCDField;
    dseDatac_CBu: TBCDField;
    dseDatac_CDBu: TBCDField;
    dseDatac_CSys: TBCDField;
    dseDatac_LckSys: TBCDField;
    dseDatac_CUpY: TBCDField;
    dseDatac_CYTot: TBCDField;
    dseDatac_CSIRO: TBCDField;
    dseDatac_LckSIRO: TBCDField;
    dseDatac_I2_4: TBCDField;
    dseDatac_I4_8: TBCDField;
    dseDatac_I8_20: TBCDField;
    dseDatac_I20_70: TBCDField;
    dseDatac_Sfi: TBCDField;
    dseDatac_SFiCnt: TBCDField;
    dseDatac_CSFI: TBCDField;
    dseDatac_LckSFI: TBCDField;
    dseDatac_LckClS: TBCDField;
    dseDatac_LckSIROCl: TBCDField;
    dseDatac_CSIROCL: TBCDField;
    dseDatac_AdjustBaseCnt: TBCDField;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    laCaption: TmmLabel;
    PageControl1: TmmPageControl;
    tsGraph: TTabSheet;
    mmGraph1: TmmGraph;
    tsClass: TTabSheet;
    Panel2: TmmPanel;
    rgMatrixType: TmmRadioGroup;
    rgEvents: TmmRadioGroup;
    rgKlassierung: TmmRadioGroup;
    pnMatrix: TmmPanel;
    pnSiro: TPanel;
    pnChannel: TPanel;
    tsSettings: TTabSheet;
    RepSettings: TRepSettings;
    SiroMatrix: TQualityMatrix;
    ChannelMatrix: TQualityMatrix;
    mXMLModel: TVCLXMLSettingsModel;
    dseDatac_COffCntPlus: TBCDField;
    dseDatac_COffCntMinus: TBCDField;
    dseDatac_CShortOffCnt: TBCDField;
    dseDatac_CShortOffCntPlus: TBCDField;
    dseDatac_CShortOffCntMinus: TBCDField;
    dseDatac_CP: TBCDField;
    dseDatac_CClL: TBCDField;
    dseDatac_CClT: TBCDField;
    dseDatac_LckP: TBCDField;
    dseDatac_LckClL: TBCDField;
    dseDatac_LckClT: TBCDField;
    dseDatac_LckShortOffCnt: TBCDField;
    dseDatac_LckNSLT: TBCDField;
    dseDatacalc_COffCntPlus: TFloatField;
    dseDatacalc_COffCntMinus: TFloatField;
    dseDatacalc_CShortOffCnt: TFloatField;
    dseDatacalc_CShortOffCntPlus: TFloatField;
    dseDatacalc_CShortOffCntMinus: TFloatField;
    dseDatacalc_CP: TFloatField;
    dseDatacalc_CClL: TFloatField;
    dseDatacalc_CClT: TFloatField;
    dseDatacalc_CVd: TFloatField;
    dseDatacalc_CVCV: TFloatField;
    dseDatac_CVCV: TBCDField;
    dseDatac_LckVCV: TBCDField;

    procedure acCloseExecute(Sender: TObject);
    procedure acDeleteProfileExecute(Sender: TObject);
    procedure acFormatExecute(Sender: TObject);
    procedure acGraphOptionsExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acLoadProfileExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSaveAsDefaultExecute(Sender: TObject);
    procedure acSaveProfileExecute(Sender: TObject);
    procedure acShowPrevNextSpindleExecute(Sender: TObject);
    procedure acSortGraphExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mmTranslatorLanguageChange(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure dseDataCalcFields(DataSet: TDataSet);
    procedure rgMatrixTypeClick(Sender: TObject);
    procedure acPeriodExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure rgEventsClick(Sender: TObject);
    procedure rgKlassierungClick(Sender: TObject);
    procedure pnMatrixResize(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure tsSettingsEnter(Sender: TObject);
  private
//    mLenBase: TLenBase;
    mRepInfo: rESPReportInfoT;
    mParaList: rEspParameterArrayT;
    mPeriod: rPeriodInfoT;
    mParameter: TParameterList;
    mPrinterEnabled : Boolean;

    procedure GetGridDisplay;
    procedure EnableActions;
    procedure GetMatrixData;
    procedure GetClearerSettings;
    procedure LoadProfile(aUsername, aProfilename: string);
    procedure ReadFromDataQuery;
    procedure ResetTabsheetTags;
    procedure PrepareDataQuery;
    procedure SetClassLengthUnits;
    procedure SetGraphParameters;
    procedure TextToTranslate;
    procedure UpdateCaption;
    procedure UpdateCurrentTabSheet;
    procedure PrepareReport(aDataset: TDataset);
    procedure ShowDialog(aDataset: TDataset);
    function GetParameterList: TParameterList; // SDO
    procedure SetParameterList(const Value: TParameterList);
    procedure SetShowGraphicOptions(const Value: Boolean);
    procedure PrintRep(aBlackWhitePrint : Boolean);
  public
    procedure Init(aRepInfo: rESPReportInfoT; aParaList: rEspParameterArrayT);
    property Parameter: TParameterList read GetParameterList write SetParameterList; // SDO
    procedure ShowSettedGraphic; // SDo
    property ShowGraphicOptions: Boolean write SetShowGraphicOptions default True;
  end; //TEsp_SpindleReportDlg

var
  Esp_SpindleReportDlg: TEsp_SpindleReportDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmCS,
  u_main, u_lib, u_common_lib, u_common_global,
  u_SettingsDialog,
  u_resourcestrings, mmLib, QualityMatrixDef, u_dmSpindleReport, XMLGlobal,
  PrintTemplateForm, PrintSettingsTemplateForm;

{$R *.DFM}

resourcestring
  cCaptionStr1    = '(*)Spulstellen-Bericht: [%s-%d   Zeitraum: %s - %s]'; //ivlm
  cGraphTitleStr1 = '(*)Maschine: %s    Spulstelle: %d'; //ivlm

  cReportTitle  = '(*)Reinigereinstellungen'; //ivlm
  cReportTitle1 = '(*)Spulstelle'; //ivlm
const
  cDateFrmStr = 'd/m/yyyy  hh:nn';

//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TEsp_SpindleReportDlg.acCloseExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acDeleteProfileExecute(Sender: TObject);
begin
  mmGraph1.MemoINI.DeleteProfileDlg;
end; //procedure TEsp_SpindleReportDlg.acDeleteProfileExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acFormatExecute(Sender: TObject);
var
  xTmpLU: TLenBase;

begin
  xTmpLU := mRepInfo.LengthUnit;

  mmLengthUnits1.LengthUnitSet := cAllLengthUnits;
  mmLengthUnits1.LengthUnit    := xTmpLU;

  if mmLengthUnits1.Execute then begin
    if mmLengthUnits1.LengthUnit <> xTmpLU then begin
      mRepInfo.LengthUnit := mmLengthUnits1.LengthUnit;
      GetGridDisplay;
      ReadFromDataQuery;

      SetClassLengthUnits;
      GetMatrixData;
    end; //if mmLengthUnits1.LengthUnit <> mRepInfo.LengthUnit then
  end;
end; //procedure TEsp_SpindleReportDlg.acFormatExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acGraphOptionsExecute(Sender: TObject);
var
  xParameter: TParameterList;
begin
  PrepareReport(dseData);

  xParameter := ReadParam(dseData, mParaList, mmGraph1);
  mParameter.Assign(xParameter);
  xParameter.Free;

  ShowDialog(dseData);
end; //procedure TEsp_SpindleReportDlg.acBarsStackedExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TEsp_SpindleReportDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acLoadProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;

begin
  xRes := mmGraph1.MemoINI.SelectProfile;
  if xRes.Profilename <> '' then
    LoadProfile(xRes.Username, xRes.Profilename);
end; //procedure TEsp_SpindleReportDlg.acLoadDefaultExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acPrintExecute(Sender: TObject);
var
  xBlackWhitePrint : Boolean;
begin
  if PageControl1.ActivePage = tsGraph then begin
    if GetPrintOptions(poLandscape, Self, xBlackWhitePrint) then
      mmGraph1.Print(10, 20, 0, 0)
  end else begin
    if GetPrintOptions(poPortrait, Self, xBlackWhitePrint) then
      PrintRep(xBlackWhitePrint);
      
//    try
//      if GetPrintOptions(poPortrait, Self, xBlackWhitePrint) then
//        PrintRep( xBlackWhitePrint );
//    finally
//      laCaption.Visible := False;
//    end;
  end;
end; //procedure TEsp_SpindleReportDlg.acPrintExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acSaveAsDefaultExecute(Sender: TObject);
begin
  mmGraph1.WriteSettings;
  mmGraph1.MemoINI.SetDefaultProfile;
end; //procedure TEsp_SpindleReportDlg.acSaveAsDefaultExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acSaveProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;

begin
  xRes := mmGraph1.MemoINI.SaveProfile;
  if xRes.Profilename <> '' then begin
    mmGraph1.MemoINI.Profilename := xRes.Profilename;
    mmGraph1.WriteSettings;
  end; //if xRes.Profilename <> '' then
end; //procedure TEsp_SpindleReportDlg.acSaveProfileExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acShowPrevNextSpindleExecute(Sender: TObject);
begin
  if TControl(Sender).Tag = -1 then
    CircDec(mRepInfo.Spindle, mRepInfo.MinSpindle, mRepInfo.MaxSpindle)
  else
    CircInc(mRepInfo.Spindle, mRepInfo.MinSpindle, mRepInfo.MaxSpindle);

  ResetTabsheetTags;
  UpdateCurrentTabSheet;
end; //procedure TEsp_SpindleReportDlg.acShowPrevSpindleExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acSortGraphExecute(Sender: TObject);
begin
  mmGraph1.SortSeriesDlg;
end; //procedure TEsp_SpindleReportDlg.acSortGraphExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
  mParaList := nil;
end; //procedure TEsp_SpindleReportDlg.FormClose
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Spulstellenbericht\SPST_DLG_Spulstellenbericht.htm');
//  mLenBase := lb100Km;
//  SetClassLengthUnits;
  mParaList := nil;
  RepSettings.ClassDataReader := mCDR;

  ResetTabsheetTags;

  mmGraph1.MemoINI.Applicationname := gApplicationName;
  mmGraph1.MemoINI.Formname := Classname;

  PrepareReport(dseData);
  PageControl1.ActivePage := tsGraph;
  mParameter := TParameterList.Create;

  mPrinterEnabled := acPrint.Enabled;
end; //procedure TEsp_SpindleReportDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.FormDestroy(Sender: TObject);
begin
  mParameter.Free;
end; //procedure TEsp_SpindleReportDlg.FormDestroy
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.mmTranslatorLanguageChange(Sender: TObject);
begin
//  if not (quMach.Active) then
//    quMach.Active := True;

  TextToTranslate;
end; //procedure TEsp_SpindleReportDlg.mmTranslatorLanguageChange
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.PageControl1Change(Sender: TObject);
begin
  UpdateCurrentTabSheet;
end; //procedure TEsp_SpindleReportDlg.PageControl1Change
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.dseDataCalcFields(DataSet: TDataSet);
var
  xFactor: double;
  xBaseUnits: rBaseLengthUnitsT;
  //..........................................................
  procedure ConvertField(aFStr: s30);
  begin
    dseData.FieldByName('calc_' + aFstr).AsFloat := dseData.FieldByName('c_' + aFstr).AsFloat * xFactor;
  end; //procedure ConvertField
  //..........................................................
  procedure CalcAnteil(resField, f1, f2: TField);
  begin
    try
      if f2.Value <> 0 then
        resField.Value := f1.value / f2.value * 100
      else
        resField.Value := 0;
    except
      resField.Value := 0;
    end; //try..except
  end; //procedure CalcAnteil
  //..........................................................
begin //main
//  xBaseUnits.Length  := dseDatac_Len.AsInteger;
  xBaseUnits.Length  := dseDatac_Len.AsFloat;
  xBaseUnits.Weight  := dseDatac_Wei.AsFloat;
  xBaseUnits.Bobines := dseDatac_Bob.AsFloat;
  xBaseUnits.Cones   := dseDatac_Cones.AsFloat;
  xFactor := GetLengthUnitFactor(xBaseUnits, mRepInfo.LengthUnit);

  ConvertField('RED');
  ConvertField('MANST');
  ConvertField('SP');
  ConvertField('RSP');
  ConvertField('YB');
  ConvertField('CS');
  ConvertField('CL');
  ConvertField('CT');
  ConvertField('CN');
  ConvertField('CSP');
  ConvertField('CCLS');
  ConvertField('CCLL');
  ConvertField('CCLT');
  ConvertField('UCLS');
  ConvertField('UCLL');
  ConvertField('UCLT');
  ConvertField('COFFCNT');
  ConvertField('COFFCNTPlus');
  ConvertField('COFFCNTMinus');
  ConvertField('CShortOFFCNT');
  ConvertField('CShortOFFCNTPlus');
  ConvertField('CShortOFFCNTMinus');
  ConvertField('CBU');
  ConvertField('CDBU');
  ConvertField('CSYS');
  ConvertField('CSFI');
  ConvertField('CVCV');    //Nue:18.02.08
  ConvertField('CUPY');
  ConvertField('CYTOT');
  ConvertField('CSIRO');
  ConvertField('CSIROCL');
  ConvertField('CP');
  // Locks sind absolut und nicht L�ngen basiert -> direkt Feld c_Lck... verwenden ohne Berechnung

  dseDatacalc_SFI_D.Value := CalcSFI_D(dseDatac_SFI.AsFloat, dseDatac_SFICnt.AsFloat);
  if dseDatac_AdjustBaseCnt.AsInteger > 0 then
    dseDatacalc_SFI.Value := CalcSFI(dseDatac_SFI.AsFloat, dseDatac_SFICnt.AsFloat, dseDatac_AdjustBase.AsFloat / dseDatac_AdjustBaseCnt.AsFloat)
  else
    dseDatacalc_SFI.Value := 0;

//Nue:18.02.08
  dseDatacalc_CVd.Value := dseDatacalc_SFI_D.Value * cVCVFactor;


  CalcAnteil(dseDatacalc_MEff, dseDatac_rtSpd, dseDatac_otSpd);
  CalcAnteil(dseDatacalc_PEff, dseDatac_rtSpd, dseDatac_wtSpd); //falsch
  CalcAnteil(dseDatacalc_PCSp, dseDatac_CSp, dseDatac_Sp);
 // CalcAnteil(dseDatacalc_PCSp,dseDatac_CSp,dseDatac_CYTot);
  CalcAnteil(dseDatacalc_PRSp, dseDatac_RSp, dseDatac_Sp);

  // Imperfections
  if dseDatac_Len.Value > 0 then begin
    // Diese sind per 1 km
    dseDatacalc_INeps.Value  := dseDatac_INeps.AsVariant / dseDatac_Len.Value;
    dseDatacalc_IThick.Value := dseDatac_IThick.AsVariant / dseDatac_Len.Value;
    dseDatacalc_IThin.Value  := dseDatac_IThin.AsVariant / dseDatac_Len.Value;
    dseDatacalc_I2_4.Value   := dseDatac_I2_4.AsVariant / dseDatac_Len.Value;
    dseDatacalc_I4_8.Value   := dseDatac_I4_8.AsVariant / dseDatac_Len.Value;
    dseDatacalc_I8_20.Value  := dseDatac_I8_20.AsVariant / dseDatac_Len.Value;
    dseDatacalc_I20_70.Value := dseDatac_I20_70.AsVariant / dseDatac_Len.Value;
    // Ausname: ISmall per 1m
    dseDatacalc_ISmall.Value := dseDatac_ISmall.AsFloat / dseDatac_Len.Value / 1000;
  end;

  dseDatacalc_npProdGrp.Value := Round(dseDatac_wtSpd.Value - dseDatac_rtSpd.Value);

  dseDatacalc_LStTOT.Value := Round(dseDatac_LStProd.Value
    + dseDatac_LStBreak.Value
    + dseDatac_LStMat.Value
    + dseDatac_LStRev.Value
    + dseDatac_LStClean.Value
    + dseDatac_LStDef.Value);
end; //procedure TEsp_SpindleReportDlg.dseDataCalcFields
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.EnableActions;
begin
  acSortGraph.Enabled      := PageControl1.ActivePage = tsGraph;
  acGraphOptions.Enabled   := acSortGraph.Enabled;
  acShowKlassTrend.Enabled := PageControl1.ActivePage = tsClass;
  acSelectMode.Enabled     := acShowKlassTrend.Enabled;
  acPeriod.Enabled         := (PageControl1.ActivePage = tsClass) or (PageControl1.ActivePage = tsSettings);
end; //procedure TEsp_SpindleReportDlg.EnableActions
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.GetMatrixData;
var
  xCutData: Boolean;
  xDefectData: Boolean;
  xShowSettings: Boolean;
begin
  with mCDR.Params do begin
    LenBase    := mRepInfo.LengthUnit;
    TimeFrom   := mPeriod.pBegin;
    TimeTo     := mPeriod.pEnd;
    SpindleIDs := IntToStr(mRepInfo.Spindle);
    MachIDs    := IntToStr(mRepInfo.MachineID);
  end;

  // Handle Settings
  xShowSettings := (mCDR.XMLSettingNames.Count = 1);
  if rgMatrixType.ItemIndex = 0 then begin
    ChannelMatrix.MatrixType     := mtShortLongThin;
    ChannelMatrix.ActiveColor    := clQualityCutOn;     //Nue:18.09.08 Wegen Einbau Spliceclassification
    ChannelMatrix.ActiveVisible  := xShowSettings;
    ChannelMatrix.ChannelVisible := xShowSettings;
    ChannelMatrix.ClusterVisible := xShowSettings;
    ChannelMatrix.SpliceVisible  := False;
  end
  else begin
    ChannelMatrix.MatrixType     := mtSplice;
    ChannelMatrix.ActiveColor    := clSpliceQualityCutOn;    //Nue:18.09.08 Wegen Einbau Spliceclassification
    ChannelMatrix.ActiveVisible  := False;
    ChannelMatrix.ChannelVisible := False;
    ChannelMatrix.ClusterVisible := False;
    ChannelMatrix.SpliceVisible  := xShowSettings;
  end;
  SiroMatrix.ActiveVisible  := xShowSettings;

  if xShowSettings then begin
    mXMLModel.xmlAsString := mCDR.XMLSettings.Items[0]^.XMLData;
    if CodeSite.Enabled then
      CodeSite.SendString('XMLData', FormatXML(mCDR.XMLSettings.Items[0]^.XMLData));
  end else
    mXMLModel.xmlAsString := '';

  // Handle Daten
  xDefectData := (rgEvents.ItemIndex in [0,2]);
  xCutData    := (rgEvents.ItemIndex in [1,2]);
  mCDR.FillValuesToMatrix(xDefectData, xCutData, ChannelMatrix);
  mCDR.FillValuesToMatrix(xDefectData, xCutData, SiroMatrix);

  tsClass.Tag := 1; //1 = no update, 0 = update, in => UpdateCurrentTabSheet

//  with mCDR.Params do begin
//    LenBase    := mRepInfo.LengthUnit;
//    TimeFrom   := mPeriod.pBegin;
//    TimeTo     := mPeriod.pEnd;
//    SpindleIDs := IntToStr(mRepInfo.Spindle);
//    MachIDs    := IntToStr(mRepInfo.MachineID);
//  end;
//  xShowSettings := (mCDR.XMLSettingNames.Count = 1);
//
//  if rgMatrixType.ItemIndex = 1 then begin
//    ChannelMatrix.MatrixType     := mtSplice;
//    ChannelMatrix.ActiveVisible  := False;
//    ChannelMatrix.ChannelVisible := False;
//    ChannelMatrix.ClusterVisible := False;
//    ChannelMatrix.SpliceVisible  := xShowSettings;
//  end
//  else begin
//    ChannelMatrix.MatrixType     := mtShortLongThin;
//    ChannelMatrix.ActiveVisible  := xShowSettings;
//    ChannelMatrix.ChannelVisible := xShowSettings;
//    ChannelMatrix.ClusterVisible := xShowSettings;
//    ChannelMatrix.SpliceVisible  := False;
//  end;
//
//  xDefectData := (rgEvents.ItemIndex in [0,2]);
//  xCutData    := (rgEvents.ItemIndex in [1,2]);
//  mCDR.FillValuesToMatrix(xDefectData, xCutData, ChannelMatrix);
//  mCDR.FillValuesToMatrix(xDefectData, xCutData, SiroMatrix);
//
//  if xShowSettings then begin
//    mXMLModel.xmlAsString := mCDR.XMLSettings.Items[0]^.XMLData;
//    if CodeSite.Enabled then
//      CodeSite.SendString('XMLData', FormatXML(mCDR.XMLSettings.Items[0]^.XMLData));
//  end;
//
//  ChannelMatrix.ActiveVisible  := False;
//  ChannelMatrix.ChannelVisible := False;
//  ChannelMatrix.ClusterVisible := False;
//  ChannelMatrix.SpliceVisible  := False;
//
//  if rgMatrixType.ItemIndex = 1 then begin
//    ChannelMatrix.MatrixType     := mtSplice;
//  end
//  else begin
//    ChannelMatrix.MatrixType     := mtShortLongThin;
//  end;
//
//  SiroMatrix.MatrixType     := mtSiro;
//  SiroMatrix.ActiveVisible  := False;
//  SiroMatrix.ChannelVisible := False;
//  SiroMatrix.ClusterVisible := False;
//  SiroMatrix.SpliceVisible  := False;
//
//  tsClass.Tag := 1; //1 = no update, 0 = update, in => UpdateCurrentTabSheet
end; //procedure TEsp_SpindleReportDlg.GetMatrixData
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.GetClearerSettings;
//var
// i    : integer;
// hstr : string;
// xProdGrpTimeRange : String;
begin
  with mCDR.Params do begin
    LenBase    := mRepInfo.LengthUnit;
    TimeFrom   := mPeriod.pBegin;
    TimeTo     := mPeriod.pEnd;
    SpindleIDs := IntToStr(mRepInfo.Spindle);
    MachIDs    := IntToStr(mRepInfo.MachineID);
  end;

  RepSettings.Open;
end; //procedure TEsp_SpindleReportDlg.GetMatrixData
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.LoadProfile(aUsername, aProfilename: string);
var
  xParameter: TParameterList;
begin
  mmGraph1.BeginUpdate; //=> EndUpdate in TextToTranslate;
  mmGraph1.MemoINI.Username := aUsername;
  mmGraph1.MemoINI.Profilename := aProfilename;
  mmGraph1.ReadSettings;
  mParaList := GetGraphParamList(mmGraph1);

  xParameter := ReadParam(dseData, mParaList, mmGraph1);
  mParameter.Assign(xParameter);
  xParameter.Free;

  SetGraphParameters;
  mmGraph1.MemoINI.Username := mmUsername;
end; //procedure TEsp_SpindleReportDlg.LoadProfile
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.ReadFromDataQuery;
var
  i: integer;
  yValue: tYPointT;
  xFStr: string;
  xSortOrder: eSortOrderT;
  xSortRow: integer;

  function SetYValue(aFieldname: string): tYPointT;
  begin
    Result.IsNull := dseData.FieldByName(aFieldname).IsNull;
    Result.Value := dseData.FieldByName(aFieldname).AsFloat;
  end; //function SetYValue

begin
  with mmGraph1 do begin
    xSortOrder := Data.SortOrder;
    xSortRow := Data.SortRow;

    Data.ClearData;

    dseData.DisableControls;
    try
      dseData.First;
      mRepInfo.StartTime := dseDatac_interval_start.value;
      while not dseData.Eof do begin
        for i := 1 to Data.NumSeries do begin
          xFStr := mParaList[i - 1].FieldName;
          yValue := SetYValue(xFStr);
          if not yValue.IsNull then
            Data.AddYValue(i, dseDatac_interval_start.Value, yValue.Value, formatdatetime('dd/mm hh:nn', dseDatac_interval_start.Value));
        end; //for i := 1 to Data.NumSeries do

        mRepInfo.EndTime := dseDatac_interval_end.value;

        dseData.Next;
      end; //while not dseData.Eof do
    finally
      dseData.EnableControls;
    end; //try..finally

    Data.SortByRow(xSortRow, xSortOrder);
    TextToTranslate;
    Refresh;

    tsGraph.Tag := 1; //1 = no update, 0 = update, in => UpdateCurrentTabSheet
  end; //with mmGraph1 do
end; //procedure TEsp_SpindleReportDlg.ReadFromDataQuery
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.ResetTabsheetTags;
var
  i: integer;
begin
  for i := 0 to PageControl1.PageCount - 1 do
    PageControl1.Pages[i].Tag := 0;
end; //procedure TEsp_SpindleReportDlg.ResetTabsheetTags
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.PrepareDataQuery;
begin
  with dseData do begin
    Active := False;
    if mRepInfo.ProdGroupID = 0 then
      CommandText := Format(dmSpindleReport.slSpindleReport.Text, ['AND c_prod_id <> :ProdGroupID'])
    else
      CommandText := Format(dmSpindleReport.slSpindleReport.Text, ['AND c_prod_id = :ProdGroupID']);

    Parameters.ParamByName('MachineID').value := mRepInfo.MachineID;
    Parameters.ParamByName('SpindleID').value := mRepInfo.Spindle;
    Parameters.ParamByName('ProdGroupID').value := mRepInfo.ProdGroupID;
    Active := True;
  end;
end; //procedure TEsp_SpindleReportDlg.PrepareDataQuery
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.SetClassLengthUnits;
begin
//  ChannelMatrix.LenBase := mRepInfo.LengthUnit;
end; //procedure TEsp_SpindleReportDlg.SetClassLengthUnits
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.SetGraphParameters;
var
  x, xSerieNr: integer;
  xHasLine: Boolean;
  xHasBar: Boolean;

begin
  xHasLine := False;
  xHasBar := False;

  with mmGraph1 do begin
    BeginUpdate;

    Bars.Style := mParameter.BarStyle;
    if Bars.Style = bsSide then
      Bars.Gap := 5
    else
      Bars.Gap := 2;

    xSerieNr := 0;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then inc(xSerieNr);
    Data.NumSeries := xSerieNr;

    xSerieNr := 1;
    for x := 0 to mParameter.Count - 1 do begin
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
         mParameter.Items[x].GraphicParam.ShowGraph and (xSerieNr <= cMaxSeries) then
      begin
        Series.FieldName[xSerieNr]       := mParameter.Items[x].GraphicParam.FieldName;
        Series.Color[xSerieNr]           := mParameter.Items[x].GraphicParam.Color;
        Series.YAxis[xSerieNr]           := eYAxisT(Ord(mParameter.Items[x].GraphicParam.ChartType));
        Series.YScalingType[xSerieNr]    := mParameter.Items[x].GraphicParam.YScalingType;
        Series.ShowStat[xSerieNr]        := mParameter.Items[x].GraphicParam.Statistik;
        Series.Visible[xSerieNr]         := mParameter.Items[x].GraphicParam.ShowGraph;
        Series.LowerLimit[xSerieNr]      := mParameter.Items[x].GraphicParam.LowerLimit;
        Series.UpperLimit[xSerieNr]      := mParameter.Items[x].GraphicParam.UpperLimit;
        Series.UpperLimitColor[xSerieNr] := clRed;
        Series.LowerLimitColor[xSerieNr] := clRed;

        xHasBar               := xHasBar or (mParameter.Items[x].GraphicParam.ChartType = ctBar);
        xHasLine              := xHasLine or (mParameter.Items[x].GraphicParam.ChartType = ctLine);
        Series.Name[xSerieNr] := GetDisplayLabel(dseData.FieldByName(Series.FieldName[xSerieNr]).DisplayLabel);
        inc(xSerieNr);
      end; //if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
    end; // for x

    with RightYAxis do begin
      Visible := True; //xHasBar;
      AutoScaling := mParameter.RightYAxis.Autoscale;
      DefaultMinY := mParameter.RightYAxis.Min;
      DefaultMaxY := mParameter.RightYAxis.Max;
    end;

    with LeftYAxis do begin
      Visible := xHasLine;
      AutoScaling := mParameter.LeftYAxis.Autoscale;
      DefaultMinY := mParameter.LeftYAxis.Min;
      DefaultMaxY := mParameter.LeftYAxis.Max;
    end;

    if xHasBar and xHasLine then
      HorzGrid.GridType := hgLeftRightAxis
    else if xHasBar then
      HorzGrid.GridType := hgRightAxis
    else if xHasLine then
      HorzGrid.GridType := hgLeftAxis;

    ReadFromDataQuery;
    EndUpdate;
  end; //with mmGraph1 do
end; //procedure TEsp_SpindleReportDlg.SetGraphParameters
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.TextToTranslate;
var
  i: integer;

begin
  UpdateCaption;

  GetGridDisplay;

  with mmGraph1 do begin
    BeginUpdate;
    for i := 1 to Data.NumSeries do
      Series.Name[i] := GetDisplayLabel(dseData.FieldByName(Series.FieldName[i]).DisplayLabel);

    LeftYAxis.Title := cLeftAxisStr;
    RightYAxis.Title := cRightAxisStr;
    if quMach.Active then
      Titles.Title := Format(cGraphTitleStr1, [quMachc_machine_name.value, mRepInfo.Spindle]);

    Titles.SubTitle := Format(cGraphTitleStr2, [DateToStr(mRepInfo.StartTime),
      DateToStr(mRepInfo.EndTime),
      GetTimeDiffStr(mRepInfo.StartTime, mRepInfo.EndTime)]);

    XAxis.Title := cXAxisIntStr;
    EndUpdate;
  end; //with mmGraph1 do
end; //procedure TEsp_SpindleReportDlg.TextToTranslate
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.UpdateCaption;
var
  xFrom,
    xTo: tdatetime;

begin
  if PageControl1.ActivePage = tsGraph then begin
    xFrom := mRepInfo.StartTime;
    xTo := mRepInfo.EndTime;
  end //if PageControl1.ActivePage = tsGraph then
  else begin
    xFrom := mPeriod.pBegin;
    xTo := mPeriod.pEnd;
  end; //else if PageControl1.ActivePage = tsGraph then

  if quMach.Active then
    Caption := Format(cCaptionStr1, [quMachc_machine_name.value,
      mRepInfo.Spindle,
      formatdatetime(cDateFrmStr, xFrom),
      formatdatetime(cDateFrmStr, xTo)]);
end; //procedure TEsp_MachineReportDlg.UpdateCaption
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.UpdateCurrentTabSheet;
begin
  EnableActions;
  UpdateCaption;
{
  if PageControl1.ActivePage = tsClass then begin
    GetMatrixData;
  end; //if PageControl1.ActivePage = tsClass then
}
  if PageControl1.ActivePage.Tag = 1 then exit;

  if PageControl1.ActivePage = tsGraph then begin
    PrepareDataQuery;
    SetGraphParameters;
  end; //if PageControl1.ActivePage = tsGraph then

  if PageControl1.ActivePage = tsClass then begin
    GetMatrixData;
  end; //if PageControl1.ActivePage = tsClass then


  if PageControl1.ActivePage = tsSettings then begin
    GetClearerSettings;
  end; //if PageControl1.ActivePage = tsSettings then
end; //procedure TEsp_SpindleReportDlg.UpdateCurrentTabSheet
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.Init(aRepInfo: rESPReportInfoT; aParaList: rEspParameterArrayT);
begin
  mRepInfo := aRepInfo;
  SetClassLengthUnits;

  if aParaList <> nil then
    mParaList := aParaList;

  quMach.Parameters.ParamByName('MachID').Value := mRepInfo.MachineID;
  quMach.Open;

  PrepareDataQuery;

  mmGraph1.MemoINI.Username := mmUsername;
  LoadProfile(mmGraph1.MemoINI.Username, mmGraph1.MemoINI.GetDefaultProfile);

//Wss: after reload the mRepInfo set the mPeriod parameters
  mPeriod.pBegin := mRepInfo.StartTime;
  mPeriod.pEnd := mRepInfo.EndTime;
  CodeSite.SendFmtMsg('TimeRange4: %s - %s', [DateTimeToStr(mPeriod.pBegin), DateTimeToStr(mPeriod.pEnd)]);

  EnableActions;
  GetGridDisplay;
end; //procedure TEsp_SpindleReportDlg.Init
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.rgMatrixTypeClick(Sender: TObject);
begin
  GetMatrixData;
end; //procedure TEsp_SpindleReportDlg.rgMatrixTypeClick
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acPeriodExecute(Sender: TObject);
begin
  if SelectInterval(Self, mPeriod) then begin
    //tag: 1 = no update, 0 = update, in => UpdateCurrentTabSheet
    tsClass.Tag    := 0;
    tsSettings.Tag := 0;
    UpdateCurrentTabSheet;
  end; //if SelectInterval(mPeriod) then
end; //procedure TEsp_SpindleReportDlg.acPeriodExecute
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.PrepareReport(aDataset: TDataset);
var
  x, xMaxFields: integer;
  xStr: string;

begin
  xMaxFields := aDataSet.FieldCount;
  with aDataSet do
    for x:=0 to xMaxFields-1 do begin
      xStr := uppercase(Fields[x].FieldName);
      if (xStr = 'C_LEN') or
          (xStr = 'C_WEI') or
          (xStr = 'C_BOB') or
          (xStr = 'C_CONES') or
          (xStr = 'C_TRED') or
          (xStr = 'C_TYELL') or
          (xStr = 'C_TMANST') or
          (xStr = 'C_LST') or
//          (xStr = 'CALC_LCKCLS') or
          (xStr = 'CALC_SP') or
          (xStr = 'CALC_RSP') or
          (xStr = 'CALC_YB') or
          (xStr = 'CALC_CS') or
          (xStr = 'CALC_CL') or
          (xStr = 'CALC_CT') or
          (xStr = 'CALC_CN') or
          (xStr = 'CALC_CSP') or
          (xStr = 'CALC_UCLS') or
          (xStr = 'CALC_UCLL') or
          (xStr = 'CALC_UCLT') or
          (xStr = 'CALC_CBU') or
          (xStr = 'CALC_CDBU') or
          (xStr = 'CALC_CSYS') or
          (xStr = 'CALC_CUPY') or
          (xStr = 'CALC_CYTOT') or
          (xStr = 'CALC_CSIRO') or
          (xStr = 'CALC_MEFF') or
          (xStr = 'CALC_PEFF') or
          (xStr = 'CALC_PCSP') or
          (xStr = 'CALC_PRSP') or
          (xStr = 'CALC_NPPRODGRP') or
          (xStr = 'C_LSTPROD') or
          (xStr = 'CALC_MANST') or
          (xStr = 'CALC_INEPS') or
          (xStr = 'CALC_ITHICK') or
          (xStr = 'CALC_ITHIN') or
          (xStr = 'CALC_ISMALL') or
          (xStr = 'CALC_I2_4') or
          (xStr = 'CALC_I4_8') or
          (xStr = 'CALC_I8_20') or
          (xStr = 'CALC_I20_70') or
          (xStr = 'CALC_SFI_D') or
          (xStr = 'CALC_SFI') or
          (xStr = 'CALC_CSFI') or
          (xStr = 'CALC_CVD') or   //Nue:18.02.08
          (xStr = 'CALC_CVCV') or   //Nue:18.02.08
//          (xStr = 'CALC_LCKSFI') or
//          (xStr = 'CALC_LCKOFFCNT') or
          (xStr = 'CALC_COFFCNT') or
          (xStr = 'CALC_COFFCNTPLUS') or
          (xStr = 'CALC_COFFCNTMINUS') or
          (xStr = 'CALC_CSHORTOFFCNT') or
          (xStr = 'CALC_CSHORTOFFCNTPLUS') or
          (xStr = 'CALC_CSHORTOFFCNTMINUS') or
          (xStr = 'CALC_CP') or
          (xStr = 'CALC_CCLS') or
          (xStr = 'CALC_CCLL') or
          (xStr = 'CALC_CCLT') or

          (xStr = 'C_LCKSYS') or
          (xStr = 'C_LCKSIRO') or
          (xStr = 'C_LCKSIROCL') or
          (xStr = 'C_LCKSFI') or
          (xStr = 'C_LckVCV') or   //Nue:18.02.08
          (xStr = 'C_LCKP') or
          (xStr = 'C_LCKCLS') or
          (xStr = 'C_LCKCLL') or
          (xStr = 'C_LCKCLT') or
          (xStr = 'C_LCKOFFCNT') or
          (xStr = 'C_LCKSHORTOFFCNT') or
          (xStr = 'C_LCKNSLT') then
        Fields[x].Tag := cShowTableGraphic
      else
        Fields[x].Tag := cNonVisible;
    end; //for x:= 0 to xMaxFields - 1 do
end;
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.ShowDialog(aDataset: TDataset);
var
  x, xrec: integer;
begin
  with TmSettingsDialog.Create(self) do
  try
    ReportType := rtStatistic;
    Grid := nil;
    DataSet := aDataSet;
    Parameter := mParameter;

    if ShowModal = mrOK then begin
      mParameter.Assign(Parameter);

      mParaList := nil;
      xrec := 0;
      for x := 0 to mParameter.Count - 1 do
        if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
          mParameter.Items[x].GraphicParam.ShowGraph then begin
          SetLength(mParaList, xRec + 1);
          mParaList[xRec] := mParameter.Items[x].GraphicParam;
          inc(xRec);
        end; //if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and

      PrepareDataQuery;
      SetGraphParameters;
    end; //if ShowModal = mrOK then
  finally
    free;
  end; //try..finally
end;
//-----------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.GetGridDisplay;
var
  xluStr: string;

begin
  xluStr := GetLengthUnitStr(mRepInfo.LengthUnit);

  dseData.DisableControls;
  try
    dseDatacalc_npProdGrp.DisplayLabel := cCalc_npProdGrpStr;
    dseDatacalc_MEff.DisplayLabel      := cCalc_MEffStr;
    dseDatacalc_PEff.DisplayLabel      := cCalc_PEffStr;
    dseDatac_Len.DisplayLabel          := cC_LenStr;
    dseDatac_Wei.DisplayLabel          := cC_WeiStr;
    dseDatac_Bob.DisplayLabel          := cC_BobStr;
    dseDatac_Cones.DisplayLabel        := cC_ConesStr;
    dseDatacalc_CYTOT.DisplayLabel     := Format(cCalc_CYTOTStr, [xluStr]);
    dseDatacalc_Sp.DisplayLabel        := Format(cCalc_SpStr, [xluStr]);
    dseDatacalc_YB.DisplayLabel        := Format(cCalc_YBStr, [xluStr]);
    dseDatacalc_CN.DisplayLabel        := Format(cCalc_CNStr, [xluStr]);
    dseDatacalc_CS.DisplayLabel        := Format(cCalc_CSStr, [xluStr]);
    dseDatacalc_CL.DisplayLabel        := Format(cCalc_CLStr, [xluStr]);
    dseDatacalc_CT.DisplayLabel        := Format(cCalc_CTStr, [xluStr]);
    dseDatacalc_CSIRO.DisplayLabel     := Format(cCalc_CSIROStr, [xluStr]);

    dseDatacalc_CClS.DisplayLabel              := Format(cCalc_CCLSStr, [xluStr]);
    dseDatacalc_CClL.DisplayLabel              := Format(cCalc_CCLLStr, [xluStr]);
    dseDatacalc_CClT.DisplayLabel              := Format(cCalc_CCLTStr, [xluStr]);
    dseDatacalc_COffCnt.DisplayLabel           := Format(cCalc_COFFCNTStr, [xluStr]);
    dseDatacalc_COffCntPlus.DisplayLabel       := Format(cCalc_COFFCNTPlusStr, [xluStr]);
    dseDatacalc_COffCntMinus.DisplayLabel      := Format(cCalc_COFFCNTMinusStr, [xluStr]);
    dseDatacalc_CShortOffCnt.DisplayLabel      := Format(cCalc_CShortOFFCNTStr, [xluStr]);
    dseDatacalc_CShortOffCntPlus.DisplayLabel  := Format(cCalc_CShortOFFCNTPlusStr, [xluStr]);
    dseDatacalc_CShortOffCntMinus.DisplayLabel := Format(cCalc_CShortOFFCNTMinusStr, [xluStr]);
    dseDatacalc_CP.DisplayLabel                := Format(cCalc_CPStr, [xluStr]);

    dseDatac_LckSFI.DisplayLabel         := cc_LckSFIStr;
    dseDatac_LckVCV.DisplayLabel         := cc_LckVCVStr;   //Nue:18.02.08
    dseDatac_LckSys.DisplayLabel         := cc_LckSysStr;
    dseDatac_LckSIRO.DisplayLabel        := cc_LckSIROStr;
    dseDatac_LckSIROCl.DisplayLabel      := cc_LckSIROClStr;
    dseDatac_LckOffCnt.DisplayLabel      := cc_LckOffCntStr;
    dseDatac_LckShortOffCnt.DisplayLabel := cc_LckShortOffCntStr;
    dseDatac_LckP.DisplayLabel           := cc_LckPStr;
    dseDatac_LckClS.DisplayLabel         := cc_LckClSStr;
    dseDatac_LckClL.DisplayLabel         := cc_LckClLStr;
    dseDatac_LckClT.DisplayLabel         := cc_LckClTStr;
    dseDatac_LckNSLT.DisplayLabel        := cc_LckNSLTStr;
//    dseDatacalc_LckSFI.DisplayLabel         := cc_LckSFIStr;
//    dseDatacalc_LckOffCnt.DisplayLabel      := cc_LckOffCntStr;
//    dseDatacalc_LckShortOffCnt.DisplayLabel := cc_LckShortOffCntStr;
//    dseDatacalc_LckP.DisplayLabel           := cc_LckPStr;
//    dseDatacalc_LckClS.DisplayLabel         := cc_LckClSStr;
//    dseDatacalc_LckClL.DisplayLabel         := cc_LckClLStr;
//    dseDatacalc_LckClT.DisplayLabel         := cc_LckClTStr;
//    dseDatacalc_LckNSLT.DisplayLabel        := cc_LckNSLTStr;

    dseDatac_LSt.DisplayLabel          := cC_LStStr;
    dseDatacalc_LStTOT.DisplayLabel    := cCalc_LStTOTStr;
    dseDatacalc_CUPY.DisplayLabel      := Format(cCalc_CUPYStr, [xluStr]);
    dseDatacalc_CBU.DisplayLabel       := Format(cCalc_CBUStr, [xluStr]);
    dseDatacalc_CSYS.DisplayLabel      := Format(cCalc_CSYSStr, [xluStr]);
    dseDatacalc_CSP.DisplayLabel       := Format(cCalc_CSPStr, [xluStr]);
    dseDatacalc_RSP.DisplayLabel       := Format(cCalc_RSPStr, [xluStr]);
    dseDatacalc_PCSp.DisplayLabel      := cCalc_PCSpStr;
    dseDatacalc_PRSp.DisplayLabel      := cCalc_PRSpStr;
    dseDatac_LStProd.DisplayLabel      := cC_LStProdStr;
    dseDatacalc_ManSt.DisplayLabel     := Format(cCalc_ManStStr, [xluStr]);
    dseDatacalc_UClS.DisplayLabel      := Format(cCalc_UCLSStr, [xluStr]);
    dseDatacalc_UClL.DisplayLabel      := Format(cCalc_UCLLStr, [xluStr]);
    dseDatacalc_UClT.DisplayLabel      := Format(cCalc_UCLTStr, [xluStr]);
    dseDatacalc_CDBU.DisplayLabel      := Format(cCalc_CDBUStr, [xluStr]);
    dseDatac_tRed.DisplayLabel         := cC_tRedStr;
    dseDatac_tYell.DisplayLabel        := cC_tYellStr;
    dseDatac_tManSt.DisplayLabel       := cC_tManStStr;
//    dseDatacalc_Sfi.DisplayLabel     := cCalc_SfiDStr;
    dseDatacalc_CSFI.DisplayLabel      := Format(cCalc_CSFIStr, [xluStr]);
    dseDatacalc_CVCV.DisplayLabel      := Format(cCalc_CVCVStr, [xluStr]);   //Nue:18.02.08
    dseDatacalc_INeps.DisplayLabel     := cCalc_INepsStr;
    dseDatacalc_IThick.DisplayLabel    := cCalc_IThickStr;
    dseDatacalc_IThin.DisplayLabel     := cCalc_IThinStr;
    dseDatacalc_ISmall.DisplayLabel    := cCalc_ISmallStr;
    dseDatacalc_I2_4.DisplayLabel      := cCalc_I2_4Str;
    dseDatacalc_I4_8.DisplayLabel      := cCalc_I4_8Str;
    dseDatacalc_I8_20.DisplayLabel     := cCalc_I8_20Str;
    dseDatacalc_I20_70.DisplayLabel    := cCalc_I20_70Str;
  finally
    dseData.EnableControls;
  end; //try..finally
end; //procedure TEsp_MachineReportDlg.GetGridDisplay
//------------------------------------------------------------------------------
function TEsp_SpindleReportDlg.GetParameterList: TParameterList;
begin
  Result := mParameter;
  SetGraphParameters;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.SetParameterList(const Value: TParameterList);
begin
  mParameter.Assign(Value);
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.ShowSettedGraphic;
var
  x, xrec: integer;
begin
  try
    mParaList := nil;
    xrec := 0;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then begin
        SetLength(mParaList, xRec + 1);
        mParaList[xRec] := mParameter.Items[x].GraphicParam;
        inc(xRec);
      end; //if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and

    PrepareDataQuery;
    SetGraphParameters;
  finally
  end; //try..finally

end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.SetShowGraphicOptions(const Value: Boolean);
begin
  acGraphOptions.Visible := Value;
  acLoadProfile.Visible  := Value;
  acSaveProfile.Visible  := Value;
  // if parameters are inherited from parent form (T_esp_machine_report) to popup menu is available
  if not Value then
    mmGraph1.PopupMenu := nil;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.rgEventsClick(Sender: TObject);
begin
  GetMatrixData;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.rgKlassierungClick(Sender: TObject);
begin
  if rgKlassierung.ItemIndex = 0 then begin
    ChannelMatrix.DisplayMode := dmCalculateSCValues;
    SiroMatrix.DisplayMode := dmCalculateSCValues;
  end
  else begin
    ChannelMatrix.DisplayMode := dmValues;
    SiroMatrix.DisplayMode := dmValues;
  end;
  GetMatrixData;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.pnMatrixResize(Sender: TObject);
begin
  pnChannel.Width := pnMatrix.Width * 2 div 3;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.PrintRep(aBlackWhitePrint : Boolean);
var
  xOldCursor: TCursor;
  xPrnFrm: TfrmPrint;
  xProdGrpTimeRange : String;
  xProdGroup : TProdGroupInfo;
begin
  xOldCursor := Screen.Cursor;

  xPrnFrm := TfrmPrint.Create(self);
  with xPrnFrm do
  try

    mQuickReport.UsePrinterIndex(Printer.PrinterIndex);

    qcbClearerSettings.Enabled := True;

    VCLXMLSettingsModel_Print.xmlAsString := RepSettings.Model.xmlAsString;

    xProdGroup := RepSettings.SelectedSettings;

    if xProdGroup.ProdStart <> ( xProdGroup.ProdEnd )  then
       if Printer.Orientation = poLandscape then
          xProdGrpTimeRange := DateTimeToStr(xProdGroup.ProdStart) + ' - ' + DateTimeToStr(xProdGroup.ProdEnd)
       else
          xProdGrpTimeRange := DateTimeToStr(xProdGroup.ProdStart) + ' - ' + cCRLF + DateTimeToStr(xProdGroup.ProdEnd)
    else
       xProdGrpTimeRange := DateTimeToStr(xProdGroup.ProdStart) + ' -  ...';

    qlTitle.Caption := Format('%s     %s : %d',[cReportTitle, cReportTitle1, mRepInfo.Spindle ]);

    //Header
    qlLot6.Caption        := xProdGroup.ProdName;
    qlTemplate6.Caption   := xProdGroup.YMSetName;
    qlMachine6.Caption    := xProdGroup.MachName;
    qlTimeRange6.WordWrap := FALSE;
    qlTimeRange6.Caption  := xProdGrpTimeRange;

    //Matrix
    with qrmChannel do begin
         ChannelVisible := TRUE;
         SpliceVisible  := TRUE;
         ClusterVisible := TRUE;
         BlackWhite     := aBlackWhitePrint;
    end;

    qrmSiro.BlackWhite := aBlackWhitePrint;

    //Settings Boxen
    with QRXMLSettings_Channel do begin
         Visible := TRUE;
         Enabled := TRUE;
    end;

    with QRXMLSettings_Splice do begin
         Visible := TRUE;
         Enabled := TRUE;
    end;

    with QRXMLSettings_YarnCount do begin
         Visible := TRUE;
         Enabled := TRUE;

         YarnCount := RepSettings.SelectedSettings.YarnCount;
         YarnUnit := mCDR.Params.YarnUnit;
    end;

    with QRXMLSettings_FCluster do begin
         Visible := TRUE;
         Enabled := TRUE;
    end;

    with QRXMLSettings_SFI do begin
         Visible := TRUE;
         Enabled := TRUE;
    end;

    with QRXMLSettings_Cluster do begin
         Visible := TRUE;
         Enabled := TRUE;
    end;

    //Print
    with mQuickReport do
    try
      Screen.Cursor := crHourGlass;
      Prepare;
      Print;
    finally
      QRPrinter.Free;
      QRPrinter := nil;
      Screen.Cursor := xOldCursor;
    end;

  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  inherited;

  if PageControl1.ActivePage = tsClass then begin
     acPrint.Enabled := FALSE;
  end else begin
    acPrint.Enabled := mPrinterEnabled;
  end;
end;
//------------------------------------------------------------------------------
procedure TEsp_SpindleReportDlg.tsSettingsEnter(Sender: TObject);
begin
  inherited;

end;
//------------------------------------------------------------------------------
end. //u_esp_spindle_report.pas

