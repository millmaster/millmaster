(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_edit_offlimit_cause.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Editor for offlimit reason
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.05.99 | 0.00 | PW  | file created
| 20.08.01 | 0.00 | PW  | acResetCause to reset (<> delete) all values of the selected cause
| 23.08.01 |      | PW  | printing grid using wwQRPrintDBGrid component
| 31.08.01 |      | pw  | qryReset modified: "c_op_offcnt = null" added
| 18.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_edit_offlimit_cause;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, Db, Grids, Wwdbigrd, Wwdbgrid, Wwdatsrc,
  ExtCtrls, DBCtrls, ComCtrls, ToolWin, mmToolBar, IvDictio, IvMulti,
  IvEMulti, ActnList, mmActionList, mmTranslator, mmPanel,
  mmDBNavigator, Printers, u_dmSpindleReport, ADODB, mmADODataSet,
  mmADOConnection, mmADOCommand;

type
  TEditOfflimitCauseDlg = class(TmmForm)
    ActionList1: TmmActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acResetCause: TAction;

    mmTranslator1: TmmTranslator;

    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton1: TToolButton;
    bPrint: TToolButton;
    ToolButton3: TToolButton;
    DBNavigator1: TmmDBNavigator;
    ToolButton4: TToolButton;
    bHelp: TToolButton;
    ToolButton2: TToolButton;
    ToolButton5: TToolButton;

    srcOffCause: TwwDataSource;
    tabOffCause: TmmADODataSet;
    tabOffCausec_offcause_id: TIntegerField;
    tabOffCausec_offcause_name: TStringField;
    tabOffCausec_Counter: TIntegerField;

    Panel1: TmmPanel;
    wwDBGrid1: TwwDBGrid;
    qryReset: TmmADOCommand;

    // MMSecurity1: TMMSecurity;

    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure tabOffCauseBeforePost(DataSet: TDataSet);
    procedure acCloseExecute(Sender: TObject);
    procedure tabOffCauseBeforeDelete(DataSet: TDataSet);
    procedure tabOffCauseNewRecord(DataSet: TDataSet);
    procedure acPrintExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acResetCauseExecute(Sender: TObject);
  private
    procedure GetGridDisplay;
  public
  end; //TEditOfflimitCauseDlg

var
  EditOfflimitCauseDlg: TEditOfflimitCauseDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

  mmDialogs, u_global, u_common_global, u_common_lib, u_lib, u_resourcestrings,
  u_print_list;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Soll die Offlimit-Ursache mit der Bezeichnung "%s" geloescht werden ?';       //ivlm
  cMsg2 = '(*)Sollen die zu der Ursache gespeicherten Informationen zurueckgesetzt werden ?'; //ivlm

procedure TEditOfflimitCauseDlg.GetGridDisplay;
begin
  tabOffCause.DisableControls;
  try
    tabOffCausec_offcause_name.DisplayLabel := LTrans(cc_offcause_nameStr);
    tabOffCausec_Counter.DisplayLabel := LTrans(cc_counterStr);
  finally
    tabOffCause.EnableControls;
  end; //try..finally
end; //procedure TEditOfflimitCauseDlg.GetGridDisplay
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end; //procedure TEditOfflimitCauseDlg.FormClose
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Offlimit-Ursachen.htm');
  tabOffCause.IndexFieldNames := 'c_offcause_name';
  GetGridDisplay;

  //x1012
  //tabOffCause.ReadOnly := not MMSecurity1.Check([1]);
  tabOffCause.Open;
  // HelpContext := hcEditOfflimitCauseDlg;
end; //procedure TEditOfflimitCauseDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.tabOffCauseBeforePost(DataSet: TDataSet);
begin
  inherited;
  if not DBFieldOk(tabOffCausec_offcause_name,'',0,0,true, Self) then sysutils.abort;
end; //procedure TEditOfflimitCauseDlg.tabOffCauseBeforePost
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.acCloseExecute(Sender: TObject);
begin
  inherited;
  if not PostRecord(tabOffCause) then exit;
  Close;
end; //procedure TEditOfflimitCauseDlg.acCloseExecute
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.tabOffCauseBeforeDelete(DataSet: TDataSet);
begin
  // if not DeleteRecord(format(LTrans(cMsg1),[tabOffCausec_offcause_name.value])) then
  if MMMessageDlg(Format(LTrans(cMsg1),[tabOffCausec_offcause_name.value]), mtConfirmation, [mbYes,mbNo], 0, Self) <> mrYes then
    SysUtils.Abort;
end; //procedure TEditOfflimitCauseDlg.tabOffCauseBeforeDelete
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.tabOffCauseNewRecord(DataSet: TDataSet);
var
  xID : longint;

begin
  inherited;
  xID := MakeUniqueID(3,tabOffCause.CommandText,'c_offcause_id', Self);
  if not xID > 0 then SysUtils.Abort;
  tabOffCausec_offcause_id.Value := xID;
  wwDBGrid1.EditorMode := true;
end; //procedure TEditOfflimitCauseDlg.tabOffCauseNewRecord
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.acPrintExecute(Sender: TObject);
var xBlackWhitePrint : Boolean;
begin
  inherited;
  if not GetPrintOptions(poPortrait, Self, xBlackWhitePrint) then exit;
  with TfrmPrintList.Create(Self,tabOffCause,wwDBGrid1,Caption) do
    try
      Execute;
    finally
      Free;
    end; //try..finally
end; //procedure TEditOfflimitCauseDlg.acPrintExecute
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;
//-----------------------------------------------------------------------------
procedure TEditOfflimitCauseDlg.acResetCauseExecute(Sender: TObject);
begin
  if not PostRecord(tabOffCause) then
    exit;

  if MMMessageDlg(cMsg2, mtConfirmation, [mbYes,mbNo], 0, Self) <> mrYes then
    exit;

  with qryReset do begin
    Parameters.ParamByName('c_offcause_id').value := tabOffCausec_offcause_id.asinteger;
    Execute;
  end; //with qryReset do begin

  tabOffCause.Refresh;
end; //procedure TEditOfflimitCauseDlg.acResetCauseExecute

end. //u_edit_offlimit_cause.pas
