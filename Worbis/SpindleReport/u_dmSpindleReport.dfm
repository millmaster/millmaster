object dmSpindleReport: TdmSpindleReport
  OldCreateOrder = False
  Left = 416
  Top = 119
  Height = 640
  Width = 870
  object conDefault: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=WETSRVMM5;Use Procedure for Pr' +
      'epare=1;Auto Translate=False;Packet Size=4096;Workstation ID=WET' +
      'PC1148;Use Encryption for Data=False;Tag with column collation w' +
      'hen possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 16
    Top = 8
  end
  object slOnlineOfflimit: TmmVCLStringList
    Strings.Strings = (
      
        'SELECT m.c_machine_name,mo.c_spindle_id,mo.c_update_time, (mo.c_' +
        'max_online_offlimit_time / 60) AS c_max_online_offlimit_time,'
      
        '       mo.c_YB,mo.c_YB_online_time,mo.c_RedL,mo.c_RedL_online_ti' +
        'me,mo.c_Eff,mo.c_Eff_online_time,'
      
        '       mo.c_CSp,mo.c_CSp_online_time,mo.c_RSp,mo.c_RSp_online_ti' +
        'me,mo.c_CBu,mo.c_CBu_online_time,'
      
        '       mo.c_CUpY,mo.c_CUpY_online_time,mo.c_tLSt, mo.c_tLSt_onli' +
        'ne_time,mo.c_LSt,mo.c_LSt_online_time,'
      
        '       mo.c_CSIRO,mo.c_CSIRO_online_time,mo.c_CYTot,mo.c_CYTot_o' +
        'nline_time,mo.c_OutOfProduction,'
      
        '       mo.c_OutOfProduction_online_time,mo.c_maoffaverage_id,mo.' +
        'c_machine_id,m.c_maoffset_id,'
      ''
      '       moavg.c_YB AS c_YB_AVG,'
      '       moavg.c_Red AS c_RedL_AVG,'
      '       moavg.c_Eff AS c_Eff_AVG,'
      '       moavg.c_CSp AS c_CSp_AVG,'
      '       moavg.c_RSp AS c_RSp_AVG,'
      '       moavg.c_CBu AS c_CBu_AVG,'
      '       moavg.c_CUpY AS c_CUpY_AVG,'
      '       (moavg.c_tLSt / 60) AS c_tLSt_AVG,'
      '       moavg.c_LSt AS c_LSt_AVG,'
      '       moavg.c_CSIRO AS c_CSIRO_AVG,'
      '       moavg.c_CYTot AS c_CYTot_AVG,'
      ''
      
        '       moset.c_base,moset.c_Average_time,moset.c_ProdGrp_depend,' +
        'moset.c_OffBorderByAverage,'
      
        '       moset.c_OffBorderByFixValues,moset.c_Eff_PC_max,moset.c_E' +
        'ff_PC_min,moset.c_Eff_max,'
      
        '       moset.c_Eff_min,moset.c_CYTot_PC_max,moset.c_CYTot_PC_min' +
        ',moset.c_CYTot_max,moset.c_CYTot_min,'
      
        '       moset.c_CSIRO_PC_max,moset.c_CSIRO_max,moset.c_CSp_PC_max' +
        ',moset.c_CSp_max,moset.c_RSp_PC_max,'
      
        '       moset.c_RSp_max,moset.c_YB_PC_max,moset.c_YB_Max,moset.c_' +
        'CBu_PC_max,moset.c_CBu_max,'
      
        '       moset.c_CUpY_PC_max,moset.c_CUpY_max,moset.c_tLSt_PC_max,' +
        'moset.c_tLSt_max,moset.c_LSt_PC_max,'
      '       moset.c_LSt_max,moset.c_RedL_PC_max,moset.c_RedL_max'
      ''
      'FROM t_machine_offlimit mo'
      
        'JOIN t_machine_offlimit_average moavg ON moavg.c_maoffaverage_id' +
        ' = mo.c_maoffaverage_id'
      'JOIN t_machine m ON m.c_machine_id = mo.c_machine_id'
      
        'JOIN t_machine_offlimit_settings moset ON moset.c_maoffset_id = ' +
        'm.c_maoffset_id'
      ''
      'WHERE %s'
      'ORDER BY %s'
      ' '
      ' '
      ' ')
    Left = 96
    Top = 8
  end
  object slOfflineOfflimit: TmmVCLStringList
    Strings.Strings = (
      
        'SELECT mo.c_machine_id, m.c_machine_name, mo.c_spindle_id, mo.c_' +
        'offline_offlimit_time/60 c_offline_offlimit_time, mo.c_offline_w' +
        'atchtime/60 c_offline_watchtime,'
      
        '       (100.0 * mo.c_offline_offlimit_time / mo.c_offline_watcht' +
        'ime) as calc_OfflineRatio,'
      
        '       mo.c_YB_offline_time/60 c_YB_offline_time, mo.c_RedL_offl' +
        'ine_time/60 c_RedL_offline_time, mo.c_Eff_offline_time/60 c_Eff_' +
        'offline_time,'
      
        '       mo.c_CSp_offline_time/60 c_CSp_offline_time, mo.c_RSp_off' +
        'line_time/60 c_RSp_offline_time,mo.c_CBu_offline_time/60 c_CBu_o' +
        'ffline_time,'
      
        '       mo.c_CUpY_offline_time/60 c_CUpY_offline_time, mo.c_tLSt_' +
        'offline_time/60 c_tLSt_offline_time, mo.c_LSt_offline_time/60 c_' +
        'LSt_offline_time,'
      
        '       mo.c_CSIRO_offline_time/60 c_CSIRO_offline_time, mo.c_CYT' +
        'ot_offline_time/60 c_CYTot_offline_time,'
      
        '       mo.c_OutOfProduction, mo.c_OutOfProduction_offline_time/6' +
        '0 c_OutOfProduction_offline_time'
      'FROM t_machine_offlimit mo'
      'JOIN t_machine m ON (m.c_machine_id = mo.c_machine_id)'
      'WHERE mo.c_offline_offlimit_time >= :MinOfflimitTime'
      'AND mo.c_offline_watchtime > 0'
      
        'AND (100.0 *mo.c_offline_offlimit_time /mo.c_offline_watchtime) ' +
        '>= :MinOfflineRatio'
      '%s'
      'ORDER BY %s'
      ''
      ' ')
    Left = 184
    Top = 8
  end
  object slMachineReport: TmmVCLStringList
    Strings.Strings = (
      'SELECT '
      'c_spindle_id,       '
      'SUM(c_Len /1000.0) AS c_Len,'
      'SUM(c_Wei /1000.0) AS c_Wei,   '
      'SUM(c_Bob) AS c_Bob,'
      'SUM(c_Cones) AS c_Cones,'
      'SUM(c_Red) AS c_Red,'
      'SUM(c_tRed) AS c_tRed,       '
      'SUM(c_tYell) AS c_tYell,'#9#9
      'SUM(c_ManSt) AS c_ManSt,       '
      'SUM(c_tManSt) AS c_tManSt,       '
      'SUM(c_rtSpd / 60) AS c_rtSpd,       '
      'SUM(c_otSpd / 60) AS c_otSpd,       '
      'SUM(c_wtSpd / 60) AS c_wtSpd,       '
      'SUM(c_Sp) AS c_Sp,       '
      'SUM(c_RSp) AS c_RSp,       '
      'SUM(c_YB) AS c_YB,      '
      'SUM(c_LSt) AS c_LSt,       '
      'SUM(c_LStProd /60) AS c_LStProd,       '
      'SUM(c_LStBreak) AS c_LStBreak,       '
      'SUM(c_LStMat) AS c_LStMat,       '
      'SUM(c_LStRev) AS c_LStRev,       '
      'SUM(c_LStClean) AS c_LStClean,       '
      'SUM(c_LStDef) AS c_LStDef,       '
      'SUM(c_CS) AS c_CS,       '
      'SUM(c_CL) AS c_CL,       '
      'SUM(c_CT) AS c_CT,       '
      'SUM(c_CN) AS c_CN,       '
      'SUM(c_CSp) AS c_CSp,       '
      'SUM(c_CClS) AS c_CClS,'
      'SUM(c_UClS) AS c_UClS,       '
      'SUM(c_UClL) AS c_UClL,       '
      'SUM(c_UClT) AS c_UClT,       '
      'SUM(c_COffCnt) AS c_COffCnt,       '
      'SUM(c_CBu) AS c_CBu,       '
      'SUM(c_CDBu) AS c_CDBu,       '
      'SUM(c_CSys) AS c_CSys,       '
      'SUM(c_LckSys) AS c_LckSys,       '
      'SUM(c_CUpY) AS c_CUpY,       '
      'SUM(c_CYTot) AS c_CYTot,       '
      'SUM(c_CSIRO) AS c_CSIRO,       '
      'SUM(c_INeps) AS c_INeps,       '
      'SUM(c_IThick) AS c_IThick,       '
      'SUM(c_IThin) AS c_IThin,       '
      'SUM(c_ISmall) AS c_ISmall,       '
      'SUM(c_I2_4) AS c_I2_4,       '
      'SUM(c_I4_8) AS c_I4_8,       '
      'SUM(c_I8_20) AS c_I8_20,'
      'SUM(c_I20_70) AS c_I20_70,       '
      'SUM(c_Sfi) AS c_Sfi,       '
      'SUM(c_SfiCnt) AS c_SFiCnt,       '
      'SUM(c_CSFI) AS c_CSFI,       '
      'SUM(c_CVCV) AS c_CVCV,       '
      'SUM(c_LckVCV) AS c_LckVCV,       '
      'SUM(c_LckSFI) AS c_LckSFI,       '
      'SUM(c_LckClS) AS c_LckClS,'
      'SUM(c_LckSIRO) AS c_LckSIRO,       '
      'SUM(c_LckSIROCl) AS c_LckSIROCl, '
      'SUM(c_LckOffCnt) AS c_LckOffCnt,       '
      'SUM(c_CSIROCl) AS c_CSIROCL,       '
      'SUM(c_AdjustBase) AS c_AdjustBase,       '
      'SUM(c_AdjustBaseCnt) AS c_AdjustBaseCnt,'
      'SUM(c_COffCntPlus) AS c_COffCntPlus,       '
      'SUM(c_COffCntMinus) AS c_COffCntMinus,       '
      'SUM(c_CShortOffCnt) AS c_CShortOffCnt,       '
      'SUM(c_CShortOffCntPlus) AS c_CShortOffCntPlus,       '
      'SUM(c_CShortOffCntMinus) AS c_CShortOffCntMinus,       '
      'SUM(c_CP) AS c_CP,       '
      'SUM(c_CClL) AS c_CClL,       '
      'SUM(c_CClT) AS c_CClT,       '
      'SUM(c_LckP) AS c_LckP,       '
      'SUM(c_LckClL) AS c_LckClL,       '
      'SUM(c_LckClT) AS c_LckClT,       '
      'SUM(c_LckShortOffCnt) AS c_LckShortOffCnt,       '
      'SUM(c_LckNSLT) AS c_LckNSLT'
      ''
      'FROM v_production_interval '
      'WHERE c_machine_id = :MachineID '
      'AND c_interval_start >= :IntervalStart '
      'AND c_interval_end <= :IntervalEnd '
      '%s'
      'GROUP BY c_spindle_id '
      'ORDER BY c_spindle_id')
    Left = 104
    Top = 88
  end
  object slSpindleReport: TmmVCLStringList
    Strings.Strings = (
      'SELECT'
      'c_spindle_id,'
      'c_interval_start,'
      'c_interval_end,'
      'SUM(c_Len /1000.0) AS c_Len,'
      'SUM(c_Wei /1000.0) AS c_Wei,'
      'SUM(c_Bob) AS c_Bob,'
      'SUM(c_Cones) AS c_Cones,'
      'SUM(c_Red) AS c_Red,'
      'SUM(c_tRed) AS c_tRed,'
      'SUM(c_tYell) AS c_tYell,'
      'SUM(c_ManSt) AS c_ManSt,'
      'SUM(c_tManSt) AS c_tManSt,'
      'SUM(c_rtSpd / 60) AS c_rtSpd,'
      'SUM(c_otSpd / 60) AS c_otSpd,       '
      'SUM(c_wtSpd / 60) AS c_wtSpd,       '
      'SUM(c_Sp) AS c_Sp,       '
      'SUM(c_RSp) AS c_RSp,       '
      'SUM(c_YB) AS c_YB,       '
      'SUM(c_LSt) AS c_LSt,       '
      'SUM(c_LStProd / 60) AS c_LStProd,       '
      'SUM(c_LStBreak) AS c_LStBreak,       '
      'SUM(c_LStMat) AS c_LStMat,       '
      'SUM(c_LStRev) AS c_LStRev,       '
      'SUM(c_LStClean) AS c_LStClean,       '
      'SUM(c_LStDef) AS c_LStDef,       '
      'SUM(c_CS) AS c_CS,       '
      'SUM(c_CL) AS c_CL,       '
      'SUM(c_CT) AS c_CT,       '
      'SUM(c_CN) AS c_CN,       '
      'SUM(c_CSp) AS c_CSp,       '
      'SUM(c_CClS) AS c_CClS,       '
      'SUM(c_UClS) AS c_UClS,       '
      'SUM(c_UClL) AS c_UClL,       '
      'SUM(c_UClT) AS c_UClT,       '
      'SUM(c_COffCnt) AS c_COffCnt,       '
      'SUM(c_CBu) AS c_CBu,       '
      'SUM(c_CDBu) AS c_CDBu,       '
      'SUM(c_CSys) AS c_CSys,       '
      'SUM(c_LckSys) AS c_LckSys,       '
      'SUM(c_CUpY) AS c_CUpY,       '
      'SUM(c_CYTot) AS c_CYTot,       '
      'SUM(c_CSIRO) AS c_CSIRO,       '
      'SUM(c_INeps) AS c_INeps,       '
      'SUM(c_IThick) AS c_IThick,      '
      'SUM(c_IThin) AS c_IThin,       '
      'SUM(c_ISmall) AS c_ISmall,       '
      'SUM(c_I2_4) AS c_I2_4,       '
      'SUM(c_I4_8) AS c_I4_8,       '
      'SUM(c_I8_20) AS c_I8_20,       '
      'SUM(c_I20_70) AS c_I20_70,       '
      'SUM(c_Sfi ) AS c_Sfi,       '
      'SUM(c_SfiCnt ) AS c_SFiCnt,       '
      'SUM(c_CSFI) AS c_CSFI,       '
      'SUM(c_CVCV) AS c_CVCV,       '
      'SUM(c_LckVCV) AS c_LckVCV,       '
      'SUM(c_LckSFI) AS c_LckSFI,       '
      'SUM(c_LckClS) AS c_LckClS,       '
      'SUM(c_LckSIRO) AS c_LckSIRO,       '
      'SUM(c_LckSIROCl) AS c_LckSIROCl,       '
      'SUM(c_LckOffCnt) AS c_LckOffCnt,       '
      'SUM(c_CSIROCl) AS c_CSIROCL,       '
      'SUM(c_AdjustBase) AS c_AdjustBase,       '
      'SUM(c_AdjustBaseCnt) AS c_AdjustBaseCnt, '
      'SUM(c_COffCntPlus) AS c_COffCntPlus,       '
      'SUM(c_COffCntMinus) AS c_COffCntMinus,       '
      'SUM(c_CShortOffCnt) AS c_CShortOffCnt,       '
      'SUM(c_CShortOffCntPlus) AS c_CShortOffCntPlus,       '
      'SUM(c_CShortOffCntMinus) AS c_CShortOffCntMinus,       '
      'SUM(c_CP) AS c_CP,       '
      'SUM(c_CClL) AS c_CClL,       '
      'SUM(c_CClT) AS c_CClT,       '
      'SUM(c_LckP) AS c_LckP,       '
      'SUM(c_LckClL) AS c_LckClL,       '
      'SUM(c_LckClT) AS c_LckClT,       '
      'SUM(c_LckShortOffCnt) AS c_LckShortOffCnt,       '
      'SUM(c_LckNSLT) AS c_LckNSLT'
      ''
      'FROM v_production_interval '
      'WHERE c_machine_id = :MachineID '
      'AND c_spindle_id = :SpindleID '
      '%s  '
      'GROUP BY  c_spindle_id, c_interval_start, c_interval_end  '
      'ORDER BY c_interval_start'
      '')
    Left = 184
    Top = 88
  end
  object slMachineSummary: TmmVCLStringList
    Strings.Strings = (
      'SELECT '
      'SUM(c_Len /1000.0) AS c_Len,       '
      'SUM(c_Wei /1000.0) AS c_Wei,       '
      'SUM(c_Bob) AS c_Bob,       '
      'SUM(c_Cones) AS c_Cones,       '
      'SUM(c_Red) AS c_Red,       '
      'SUM(c_tRed) AS c_tRed,       '
      'SUM(c_tYell) AS c_tYell,       '
      'SUM(c_ManSt) AS c_ManSt,       '
      'SUM(c_tManSt) AS c_tManSt,       '
      'SUM(c_rtSpd / 60) AS c_rtSpd,       '
      'SUM(c_otSpd / 60) AS c_otSpd,       '
      'SUM(c_wtSpd / 60) AS c_wtSpd,       '
      'SUM(c_wtSpd / 60) - SUM(c_rtSpd / 60) AS calc_npProdGrp,'
      'SUM(c_Sp) AS c_Sp,       '
      'SUM(c_RSp) AS c_RSp,       '
      'SUM(c_YB) AS c_YB,       '
      'SUM(c_LSt) AS c_LSt,       '
      'SUM(c_LStProd /60) AS c_LStProd,       '
      'SUM(c_LStBreak) AS c_LStBreak,       '
      'SUM(c_LStMat) AS c_LStMat,       '
      'SUM(c_LStRev) AS c_LStRev,       '
      'SUM(c_LStClean) AS c_LStClean,       '
      'SUM(c_LStDef) AS c_LStDef,       '
      
        'SUM(c_LStProd /60 +c_LStBreak +c_LStMat +c_LStRev +c_LStClean +c' +
        '_LStDef) AS calc_LStTOT,       '
      'SUM(c_CS) AS c_CS,       '
      'SUM(c_CL) AS c_CL,       '
      'SUM(c_CT) AS c_CT,       '
      'SUM(c_CN) AS c_CN,       '
      'SUM(c_CSp) AS c_CSp,       '
      'SUM(c_CClS) AS c_CClS,       '
      'SUM(c_UClS) AS c_UClS,       '
      'SUM(c_UClL) AS c_UClL,       '
      'SUM(c_UClT) AS c_UClT,       '
      'SUM(c_COffCnt) AS c_COffCnt,       '
      'SUM(c_CBu) AS c_CBu,       '
      'SUM(c_CDBu) AS c_CDBu,       '
      'SUM(c_CSys) AS c_CSys,       '
      'SUM(c_CUpY) AS c_CUpY,       '
      'SUM(c_CYTot) AS c_CYTot,       '
      'SUM(c_CSIRO) AS c_CSIRO,       '
      'SUM(c_INeps) AS c_INeps,       '
      'SUM(c_IThick) AS c_IThick,       '
      'SUM(c_IThin) AS c_IThin,       '
      'SUM(c_ISmall) AS c_ISmall,       '
      'SUM(c_I2_4) AS c_I2_4,       '
      'SUM(c_I4_8) AS c_I4_8,       '
      'SUM(c_I8_20) AS c_I8_20,       '
      'SUM(c_I20_70) AS c_I20_70,       '
      'SUM(c_Sfi) AS c_Sfi,       '
      'SUM(c_SfiCnt) AS c_SFiCnt,       '
      'SUM(c_CSFI) AS c_CSFI,       '
      'SUM(c_CVCV) AS c_CVCV,       '
      'SUM(c_LckVCV) AS c_LckVCV,       '
      'SUM(c_LckSys) AS c_LckSys,       '
      'SUM(c_LckSFI) AS c_LckSFI,       '
      'SUM(c_LckClS) AS c_LckClS,       '
      'SUM(c_LckSIRO) AS c_LckSIRO,       '
      'SUM(c_LckSIROCl) AS c_LckSIROCl,       '
      'SUM(c_LckOffCnt) AS c_LckOffCnt,       '
      'SUM(c_CSIROCl) AS c_CSIROCL,      '
      'SUM(c_AdjustBase) AS c_AdjustBase,       '
      'SUM(c_AdjustBaseCnt) AS c_AdjustBaseCnt,'
      'SUM(c_COffCntPlus) AS c_COffCntPlus,       '
      'SUM(c_COffCntMinus) AS c_COffCntMinus,       '
      'SUM(c_CShortOffCnt) AS c_CShortOffCnt,       '
      'SUM(c_CShortOffCntPlus) AS c_CShortOffCntPlus,       '
      'SUM(c_CShortOffCntMinus) AS c_CShortOffCntMinus,       '
      'SUM(c_CP) AS c_CP,       '
      'SUM(c_CClL) AS c_CClL,       '
      'SUM(c_CClT) AS c_CClT,       '
      'SUM(c_LckP) AS c_LckP,       '
      'SUM(c_LckClL) AS c_LckClL,       '
      'SUM(c_LckClT) AS c_LckClT,       '
      'SUM(c_LckShortOffCnt) AS c_LckShortOffCnt,       '
      'SUM(c_LckNSLT) AS c_LckNSLT'
      ' '
      'FROM v_production_interval '
      'WHERE c_machine_id = :MachineID '
      'AND c_interval_start >= :IntervalStart '
      'AND c_interval_end <= :IntervalEnd '
      '%s')
    Left = 184
    Top = 144
  end
end
