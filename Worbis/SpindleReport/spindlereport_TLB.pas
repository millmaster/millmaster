unit spindlereport_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 09.05.2000 10:38:17 from Type Library described below.

// ************************************************************************ //
// Type Lib: \\Wetsrvbde\BDE_Dev\Worbis\programme\spindlereport\spindlereport.tlb (1)
// IID\LCID: {F4DD6C83-1B76-11D4-B5BC-0050DA4CA88D}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.Tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  spindlereportMajorVersion = 1;
  spindlereportMinorVersion = 0;

  LIBID_spindlereport: TGUID = '{F4DD6C83-1B76-11D4-B5BC-0050DA4CA88D}';

  IID_ISpindleReportServer: TGUID = '{D58647F2-1B7C-11D4-B5BC-0050DA4CA88D}';
  CLASS_SpindleReportServer: TGUID = '{37FEA181-2585-11D4-86C2-00105A55E052}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  ISpindleReportServer = interface;
  ISpindleReportServerDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  SpindleReportServer = ISpindleReportServer;


// *********************************************************************//
// Interface: ISpindleReportServer
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D58647F2-1B7C-11D4-B5BC-0050DA4CA88D}
// *********************************************************************//
  ISpindleReportServer = interface(IDispatch)
    ['{D58647F2-1B7C-11D4-B5BC-0050DA4CA88D}']
  end;

// *********************************************************************//
// DispIntf:  ISpindleReportServerDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D58647F2-1B7C-11D4-B5BC-0050DA4CA88D}
// *********************************************************************//
  ISpindleReportServerDisp = dispinterface
    ['{D58647F2-1B7C-11D4-B5BC-0050DA4CA88D}']
  end;

// *********************************************************************//
// The Class CoSpindleReportServer provides a Create and CreateRemote method to          
// create instances of the default interface ISpindleReportServer exposed by              
// the CoClass SpindleReportServer. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoSpindleReportServer = class
    class function Create: ISpindleReportServer;
    class function CreateRemote(const MachineName: string): ISpindleReportServer;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoSpindleReportServer.Create: ISpindleReportServer;
begin
  Result := CreateComObject(CLASS_SpindleReportServer) as ISpindleReportServer;
end;

class function CoSpindleReportServer.CreateRemote(const MachineName: string): ISpindleReportServer;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_SpindleReportServer) as ISpindleReportServer;
end;

end.
