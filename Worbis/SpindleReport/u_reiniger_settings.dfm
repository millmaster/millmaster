inherited ReinigerSettingsDlg: TReinigerSettingsDlg
  Left = 327
  Top = 275
  Width = 882
  Height = 617
  Caption = '(*)Reinigereinstellungen'
  ParentFont = False
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000BBBB00000000000BBBB0000000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000B00000BBBB0E00000
    000000BBBB00000000000BBBB0E00BBBB00000BBBB00BBBB00000BBBB0E00BBB
    B0E000BBBB00BBBB0E000BBBB0E00BBBB0E000BBBB00BBBB0E0B0BBBB0E00BBB
    B0E000BBBBB0BBBB0E0BBBBBB0E00BBBB0E0000BBBB0BBBB0E0BBBBBB0E00BBB
    B0E000B0BBB0BBBB0E0BBBBBB0E00BBBB0E00BBB0BB0BBBB0E0BBBBBB0E00BBB
    B0E0BBBBB0B0BBBB0E00BBBBB0E00BBBB00BBBBBBB00BBBB0E000BBBB0E00BBB
    B0BBBBBBBBB0BBBB0E00000000E00BBBBBBBBB0BBBBBBBBB0E00000EEE000BBB
    BBBBB0E0BBBBBBBB0E00000000000BBBBBBB0EEE0BBBBBBB0E00000000000BBB
    BBB0EEE000BBBBBB0E00000000000BBBBB0EEE00000BBBBB0E00000000000BBB
    B0EEE0000000BBBB0E000000000000000EEE0000000000000E000000000000EE
    E0E00000000000EEE0000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81FF03FF80FF01FF807F00FF80
    7F00FF807F00FF806F00FF804700038007000180030000800000008000000080
    0000008000000080000000000000000000000000010000000180000001C00000
    01E0000001FF000801FF001C01FF003E01FF007F01FF80FF81FFC1FFC1FF}
  Position = poMainFormCenter
  PrintScale = poPrintToFit
  Visible = True
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 874
    Height = 26
    AutoSize = True
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton1: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bPrintRange: TToolButton
      Left = 34
      Top = 0
      Action = acPrint
    end
    object ToolButton9: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bHelp: TToolButton
      Left = 65
      Top = 0
      Action = acHelp
    end
  end
  object RepSettings: TRepSettings
    Left = 0
    Top = 26
    Width = 874
    Height = 564
    Align = alClient
    TabOrder = 1
    Model = mXMLSettings
  end
  object mXMLSettings: TVCLXMLSettingsModel
    Left = 651
    Top = 326
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 472
    TargetsData = (
      1
      3
      (
        'TReportClearerSettings'
        '*'
        0)
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object ActionList1: TmmActionList
    Images = BaseApplMainForm.ImageList16x16
    Left = 682
    Top = 2
    object acLoadProfile: TAction
      Category = 'Profile Popup'
      Caption = '(*)Profil &laden...'
      Hint = '(*)Gespeichertes Profil laden'
      ImageIndex = 1
    end
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht drucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acSaveProfile: TAction
      Category = 'Profile Popup'
      Caption = '(*)Profil &speichern...'
      Hint = '(*)Einstellungen als Profil speichern'
      ImageIndex = 3
    end
    object acDeleteProfile: TAction
      Category = 'Profile Popup'
      Caption = '(*)Profil l&oeschen...'
      Hint = '(*)Gespeichertes Profil loeschen'
      ImageIndex = 46
    end
    object acSaveAsDefault: TAction
      Category = 'Profile Popup'
      Caption = '(*)Profil als Standard speichern'
      Hint = '(*)Einstellungen als Standard speichern'
      ImageIndex = 55
    end
  end
  object mCDR: TClassDataReader
    Left = 686
    Top = 326
  end
end
