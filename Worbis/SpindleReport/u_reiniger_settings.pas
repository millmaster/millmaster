(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_reiniger_settings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.01.00 | 0.00 | PW  | File created
| 23.08.00 | 1.00 | Mg  | New components of clearer settings inserted
| 12.09.00 | 1.01 | Mg  | Work around
| 10.10.00 | 1.02 | Mg  | RepSettings4 inserted
| 22.10.02 |      | LOK | Umbau ADO
| 04.12.02 |      | Wss | Anpassungen an Init Methode um unterschiedliche Anforderungen zu erf�llen
| 20.02.04 |      | Wss | Hinterlassener Bug behoben von einer Bugsuche
| 15.06.04 |      | SDo | Neuer Print-Report -> Func. PrintRep
| 25.01.05   1.03   SDo | Umbau auf XML (MM-Vers. 5.0)
|=========================================================================================*)

{$I symbols.inc}

unit u_reiniger_settings;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, BASEAPPLMAIN, BASEFORM,
  mmGraphGlobal, mmGraph, ComCtrls, ExtCtrls, ActnList, ToolWin, u_global, Db,
  Buttons, Menus, mmLengthUnit, Grids, DBGrids, IvDictio, IvMulti, IvEMulti, mmActionList, mmSpeedButton,
  mmPageControl, mmPanel, mmToolBar, mmTranslator, mmPopupMenu, Printers,
  StdCtrls, mmRadioButton, mmGroupBox, mmRadioGroup, mmCheckBox, mmButton, ClassDataSuck,
  MMUGlobal, QualityMatrixBase, QualityMatrix, mmStringGrid, MemoINI, mmScrollBox,
  Nwcombo, mmLabel, RepSettingsUnit,
  ClassDataReader, XMLSettingsModel, VCLXMLSettingsModel, SizingPanel,
  YMParaEditBox, EditBox, mmComboBox;

type
  TReinigerSettingsDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton1: TToolButton;
    bPrintRange: TToolButton;
    bHelp: TToolButton;
    ToolButton9: TToolButton;

    ActionList1: TmmActionList;
    acClose: TAction;
    acPrint: TAction;
    acHelp: TAction;
    acSaveProfile: TAction;
    acLoadProfile: TAction;
    acDeleteProfile: TAction;
    acSaveAsDefault: TAction;
    mmTranslator: TmmTranslator;
    mCDR: TClassDataReader;
    mXMLSettings: TVCLXMLSettingsModel;
    RepSettings: TRepSettings;

    procedure acCloseExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure PrintRep;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  protected
  public
    procedure Init(aDataLevel: TDataLevel; aPrimID: integer; aDateFrom, aDateTo: tdatetime; aMachineName: string);
  end; //TReinigerSettingsDlg

var
  ReinigerSettingsDlg: TReinigerSettingsDlg;
//-----------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  u_main, u_lib, u_common_lib, u_common_global, u_resourcestrings, mmLib,
  PrintTemplateForm, PrintSettingsTemplateForm, YMParaDef;

//------------------------------------------------------------------------------
procedure TReinigerSettingsDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TReinigerSettingsDlg.acCloseExecute
//-----------------------------------------------------------------------------
procedure TReinigerSettingsDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TReinigerSettingsDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TReinigerSettingsDlg.acPrintExecute(Sender: TObject);
begin
  //if not GetPrintOptions(poLandscape, Self) then
  //  exit;
  PrintRep;
  //Print;
end; //procedure TReinigerSettingsDlg.acPrintExecute
//-----------------------------------------------------------------------------
procedure TReinigerSettingsDlg.Init(aDataLevel: TDataLevel; aPrimID: integer; aDateFrom, aDateTo: tdatetime; aMachineName: string);
begin
  //ClassDataReader abfuellen
  with mCDR.Params do begin
    TimeMode := tmInterval;
    TimeFrom := aDateFrom;
    TimeTo := aDateTo;
    if aDataLevel = dlMachine then
      MachIDs := IntToSTr(aPrimID)
    else
      ProdIDs := IntToSTr(aPrimID);
  end;
  RepSettings.MachineName := aMachineName;
  RepSettings.Open;
end; //procedure TReinigerSettingsDlg.Init
//------------------------------------------------------------------------------
procedure TReinigerSettingsDlg.PrintRep;
var
//  xYMSettingsByteArr: TYMSettingsByteArr;
  xOldCursor: TCursor;
  xPrnFrm: TfrmPrint;
  xID: Integer;
  xProdGrpTimeRange : String;
begin
  xOldCursor := Screen.Cursor;
  //Print-DLG
  with TfrmPrintSettings.Create(self) do
  try
    if ShowModal = mrOK then begin
      //Report-Form
      xPrnFrm := TfrmPrint.Create(self);
      with xPrnFrm do
      try
        mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);

        qcbClearerSettings.Enabled := True;

        VCLXMLSettingsModel_Print.xmlAsString := RepSettings.Model.xmlAsString;

          //Header
        with RepSettings.SelectedSettings do begin
          if ProdStart <> ( ProdEnd )  then
             if Printer.Orientation = poLandscape then
                xProdGrpTimeRange := DateTimeToStr(ProdStart) + ' - ' + DateTimeToStr(ProdEnd)
             else
                xProdGrpTimeRange := DateTimeToStr(ProdStart) + ' - ' + cCRLF + DateTimeToStr(ProdEnd)
          else
             xProdGrpTimeRange := DateTimeToStr(ProdStart) + ' -  ...';

          qlLot6.Caption       := ProdName;
          qlTemplate6.Caption  := YMSetName;
          qlMachine6.Caption   := MachName;
          qlTimeRange6.Caption := xProdGrpTimeRange;
        end;

        with ShowClearerSettingsSet do begin
//          //Matrix
//          with qrmChannel do begin
//            ChannelVisible := ShowChannel;
//            SpliceVisible := ShowSplice;
//            ClusterVisible := ShowCluster;
//            BlackWhite := PrintBlackWhite;
//          end;
//
          //Matrix
          with qrmChannel do begin
            ChannelVisible := ShowChannel;
            SpliceVisible := False;
            ClusterVisible := ShowCluster;
            BlackWhite := PrintBlackWhite;
          end;

          //SpliceMatrix
          with qrmSplice do begin
            ChannelVisible := False;
            SpliceVisible := ShowSplice;
            ClusterVisible := False;
            BlackWhite := PrintBlackWhite;
          end;

          qrmSiro.BlackWhite := PrintBlackWhite;

          //Settings Boxen
          with QRXMLSettings_Channel do begin
            Visible := ShowChannel;
            Enabled := ShowChannel;
          end;

          //Nue:11.03.08
          with qrmSplice do begin
            Visible := ShowSplice;
            Enabled := ShowSplice;
          end;

          with QRXMLSettings_YarnCount do begin
            Visible := ShowYarnCount;
            Enabled := ShowYarnCount;

            YarnCount := RepSettings.SelectedSettings.YarnCount;
            YarnUnit := mCDR.Params.YarnUnit;
          end;

          with QRXMLSettings_FCluster do begin
            Visible := ShowFFCluster;
            Enabled := ShowFFCluster;
          end;

          with QRXMLSettings_SFI do begin
            Visible := ShowSFI;
            Enabled := ShowSFI;
          end;

          //Nue:11.03.08
          with QRXMLSettings_VCV do begin
            Visible := ShowVCV;
            Enabled := ShowVCV;
          end;

          with QRXMLSettings_Cluster do begin
            Visible := ShowCluster;
            Enabled := ShowCluster;
          end;
        end;

          //Print
        with mQuickReport do
        try
          Screen.Cursor := crHourGlass;
          Prepare;
          Print;
        finally
          QRPrinter.Free;
          QRPrinter := nil;
          Screen.Cursor := xOldCursor;
        end;

      finally
        Free;
      end;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TReinigerSettingsDlg.FormCreate(Sender: TObject);
begin
  RepSettings.ClassDataReader := mCDR;
end;

procedure TReinigerSettingsDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

end.

