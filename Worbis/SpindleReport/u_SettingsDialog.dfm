inherited mSettingsDialog: TmSettingsDialog
  Left = 389
  Top = 119
  ActiveControl = lbShow
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '(*)Berichtseinstellungen festlegen'
  ClientHeight = 434
  ClientWidth = 843
  Constraints.MaxWidth = 851
  Constraints.MinHeight = 461
  Constraints.MinWidth = 851
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel6: TmmPanel
    Left = 0
    Top = 404
    Width = 843
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOk: TmmButton
      Left = 590
      Top = 2
      Width = 80
      Height = 25
      Action = acOK
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 675
      Top = 2
      Width = 80
      Height = 25
      Hint = '(*)Dialog abbrechen'
      Caption = '(9)Abbrechen'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton3: TmmButton
      Left = 760
      Top = 2
      Width = 80
      Height = 25
      Action = acHelp
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object paMain: TmmPanel
    Left = 0
    Top = 0
    Width = 843
    Height = 404
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    object pcSettings: TmmPageControl
      Left = 3
      Top = 3
      Width = 837
      Height = 398
      ActivePage = tsColParam
      Align = alClient
      TabOrder = 0
      OnChange = pcSettingsChange
      object tsColParam: TTabSheet
        Caption = '(*)Spalten auswahl / Parametrisierung'
        ImageIndex = 2
        OnEnter = tsColParamEnter
        OnExit = tsColParamExit
        object mmPanel2: TmmPanel
          Left = 0
          Top = 0
          Width = 425
          Height = 370
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object bMoveTo_lbShow: TmmSpeedButton
            Left = 185
            Top = 109
            Width = 23
            Height = 22
            Action = acMoveTo_lbShow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888808800
              88808800088088000080880008808800888088088880}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object bMoveTo_lbAvailable: TmmSpeedButton
            Left = 185
            Top = 134
            Width = 23
            Height = 22
            Action = acMoveTo_lbAvailable
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
              08808800088080000880880008808880088088880880}
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton9: TmmSpeedButton
            Left = 185
            Top = 169
            Width = 23
            Height = 22
            Action = acMoveAllTo_lbShow
            Glyph.Data = {
              AE000000424DAE0000000000000076000000280000000A000000070000000100
              0400000000003800000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00808880888800
              0000800880088800000080008000880000008000000008000000800080008800
              000080088008880000008088808888000000}
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object bMoveAllTo_lbAvailable: TmmSpeedButton
            Left = 185
            Top = 194
            Width = 23
            Height = 22
            Action = acMoveAllTo_lbAvailable
            Glyph.Data = {
              AE000000424DAE0000000000000076000000280000000A000000070000000100
              0400000000003800000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808880800
              0000888008800800000088000800080000008000000008000000880008000800
              000088800880080000008888088808000000}
            ParentShowHint = False
            ShowHint = True
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object bMoveUp: TmmSpeedButton
            Left = 395
            Top = 139
            Width = 23
            Height = 22
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808888
              88800000000080000080880008808880888088888880}
            ParentShowHint = False
            ShowHint = True
            Visible = False
            OnClick = bMoveUpClick
            AutoLabel.LabelPosition = lpLeft
          end
          object bMoveDown: TmmSpeedButton
            Left = 395
            Top = 164
            Width = 23
            Height = 22
            Glyph.Data = {
              92000000424D9200000000000000760000002800000007000000070000000100
              0400000000001C00000000000000000000001000000010000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888808880
              88808800088080000080000000008888888088888880}
            ParentShowHint = False
            ShowHint = True
            Visible = False
            OnClick = bMoveUpClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mmGroupBox7: TmmGroupBox
            Left = 5
            Top = 5
            Width = 170
            Height = 310
            Anchors = [akLeft, akTop, akBottom]
            Caption = '(25)Vorhandene Spaltenwerte'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            object lbAvailable: TNWListBox
              Left = 10
              Top = 20
              Width = 150
              Height = 280
              Anchors = [akLeft, akTop, akBottom]
              DragMode = dmAutomatic
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ItemHeight = 13
              MultiSelect = True
              ParentFont = False
              Sorted = True
              TabOrder = 0
            end
          end
          object mmGroupBox8: TmmGroupBox
            Left = 217
            Top = 5
            Width = 170
            Height = 310
            Anchors = [akLeft, akTop, akBottom]
            Caption = '(25)In Bericht'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            object lbShow: TmmCheckListBox
              Left = 10
              Top = 20
              Width = 150
              Height = 280
              OnClickCheck = lbShowClickCheck
              Anchors = [akLeft, akTop, akBottom]
              ItemHeight = 13
              TabOrder = 0
              Visible = True
              OnClick = lbShowClick
              AutoLabel.LabelPosition = lpLeft
              MultiSelect = True
            end
          end
          object mmPanel5: TmmGroupBox
            Left = 5
            Top = 325
            Width = 382
            Height = 40
            Anchors = [akLeft, akBottom]
            Caption = '(*)Information'
            Enabled = False
            TabOrder = 2
            object mmCheckBox2: TmmCheckBox
              Left = 15
              Top = 15
              Width = 150
              Height = 17
              Caption = '(*)In Tabelle anzeigen'
              TabOrder = 0
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object mmCheckBox1: TmmCheckBox
              Left = 185
              Top = 15
              Width = 190
              Height = 17
              Caption = '(*)InTabelle und Grafik anzeigen'
              Checked = True
              State = cbChecked
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
        object mmPanel3: TmmPanel
          Left = 425
          Top = 0
          Width = 404
          Height = 370
          Align = alClient
          BevelOuter = bvNone
          BorderWidth = 3
          TabOrder = 1
          object pcParameter: TPageControl
            Left = 3
            Top = 3
            Width = 398
            Height = 364
            ActivePage = tsTabParam
            Align = alClient
            BiDiMode = bdLeftToRight
            HotTrack = True
            MultiLine = True
            ParentBiDiMode = False
            RaggedRight = True
            TabOrder = 0
            object tsGraficParam: TTabSheet
              Caption = '(40)Grafik-Parameter'
              object rgChartType: TRadioGroup
                Left = 5
                Top = 5
                Width = 380
                Height = 40
                Caption = '(40)Grafiktyp'
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  '(35)Liniendiagramm'
                  '(35)Balkendiagramm')
                TabOrder = 0
              end
              object rgYScaling: TmmRadioGroup
                Left = 5
                Top = 55
                Width = 200
                Height = 140
                Caption = '(40)Anzeige-Faktor'
                Columns = 3
                Items.Strings = (
                  '1x'
                  '2x'
                  '5x'
                  '10x'
                  '50x'
                  '100x'
                  '500x'
                  '1000x'
                  '1/2x'
                  '1/10x'
                  '1/20x'
                  '1/100x'
                  '1/200x'
                  '1/1000x'
                  '1/2000x')
                TabOrder = 1
              end
              object gbStatistik: TmmGroupBox
                Left = 215
                Top = 55
                Width = 170
                Height = 140
                Caption = '(40)Anzeige von Statistik-Daten'
                TabOrder = 2
                object clbStatData: TmmCheckListBox
                  Left = 10
                  Top = 20
                  Width = 150
                  Height = 115
                  BorderStyle = bsNone
                  Color = clSilver
                  Ctl3D = False
                  ItemHeight = 16
                  Items.Strings = (
                    '(20)Minima'
                    '(20)Maxima'
                    '(20)Mittelwert'
                    '(20)Standard-Abweichung'
                    '(20)Q90'
                    '(20)Q95'
                    '(20)Q99')
                  ParentCtl3D = False
                  Style = lbOwnerDrawFixed
                  TabOrder = 0
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
              end
              object mmGroupBox13: TmmGroupBox
                Left = 5
                Top = 205
                Width = 200
                Height = 60
                Caption = '(20)Untere Grenze'
                TabOrder = 3
                object mmLabel2: TmmLabel
                  Left = 10
                  Top = 35
                  Width = 87
                  Height = 17
                  AutoSize = False
                  Caption = '(30)Grenzwert'
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
                object cbLowerLimit: TmmCheckBox
                  Left = 10
                  Top = 15
                  Width = 160
                  Height = 17
                  Caption = '(30)Grenze anzeigen'
                  TabOrder = 0
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
                object edLowerLimit: TNumEdit
                  Left = 100
                  Top = 30
                  Width = 60
                  Height = 21
                  Decimals = 2
                  Digits = 12
                  Masks.PositiveMask = '#,##0'
                  NumericType = ntGeneral
                  TabOrder = 1
                  UseRounding = True
                  Validate = False
                  ValidateString = 
                    '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
                    's %s'
                end
              end
              object mmGroupBox12: TmmGroupBox
                Left = 215
                Top = 205
                Width = 170
                Height = 60
                Caption = '(20)Obere Grenze'
                TabOrder = 4
                object mmLabel1: TmmLabel
                  Left = 10
                  Top = 35
                  Width = 87
                  Height = 17
                  AutoSize = False
                  Caption = '(30)Grenzwert'
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
                object cbUpperLimit: TmmCheckBox
                  Left = 10
                  Top = 15
                  Width = 150
                  Height = 17
                  Caption = '(30)Grenze anzeigen'
                  TabOrder = 0
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
                object edUpperLimit: TNumEdit
                  Left = 100
                  Top = 30
                  Width = 60
                  Height = 21
                  Decimals = 2
                  Digits = 12
                  Masks.PositiveMask = '#,##0'
                  NumericType = ntGeneral
                  TabOrder = 1
                  UseRounding = True
                  Validate = False
                  ValidateString = 
                    '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
                    's %s'
                end
              end
              object mmGroupBox11: TmmGroupBox
                Left = 5
                Top = 275
                Width = 380
                Height = 50
                Caption = '(40)Farbauswahl'
                TabOrder = 5
                object paColor1: TmmPanel
                  Left = 10
                  Top = 17
                  Width = 250
                  Height = 25
                  Hint = '(*)Mit Mausklick Farbauswahl oeffnen'
                  BevelInner = bvRaised
                  BevelOuter = bvLowered
                  Color = clBlue
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                  OnClick = acColorPanelExecute
                end
                object mmBitBtn1: TmmBitBtn
                  Left = 270
                  Top = 17
                  Width = 100
                  Height = 25
                  Action = acColorPanel
                  Caption = '(*)Farbe setzen'
                  TabOrder = 1
                  Visible = True
                  AutoLabel.LabelPosition = lpLeft
                end
              end
            end
            object tsTabParam: TTabSheet
              Caption = '(40)Feld-Definition'
              ImageIndex = 1
              object rgAlignment: TmmRadioGroup
                Left = 5
                Top = 5
                Width = 170
                Height = 75
                Caption = '(25)Ausrichtung'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  '(15)links'
                  '(15)zentriert'
                  '(15)rechts')
                ParentFont = False
                TabOrder = 0
              end
              object rgFloat: TmmRadioGroup
                Left = 208
                Top = 10
                Width = 170
                Height = 180
                Caption = '(25)Zahlen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  '1.234.481'
                  '1.234.481,0'
                  '1.234.481,00'
                  '1.234.481,000'
                  '1234481'
                  '1234481,0'
                  '1234481,00'
                  '1234481,000')
                ParentFont = False
                TabOrder = 2
              end
              object rgInteger: TmmRadioGroup
                Left = 5
                Top = 90
                Width = 170
                Height = 55
                Caption = '(25)Zahlen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                Items.Strings = (
                  '1.234.488'
                  '1234488')
                ParentFont = False
                TabOrder = 3
              end
              object rgDate: TmmRadioGroup
                Left = 197
                Top = 40
                Width = 170
                Height = 180
                Caption = '(25)Datum'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
              end
              object rgDateTime: TmmRadioGroup
                Left = 189
                Top = 84
                Width = 170
                Height = 180
                Caption = '(25)Datum/Zeit'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
              end
              object rgTime: TmmRadioGroup
                Left = 11
                Top = 154
                Width = 170
                Height = 55
                Caption = '(25)Zeit'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
              end
            end
          end
        end
      end
      object tsTabFormat: TTabSheet
        Caption = '(*)Allgemeine Tabellen- / Grafikeinstellung'
        OnEnter = tsTabFormatEnter
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 829
          Height = 370
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object gbGridSettings: TmmGroupBox
            Left = 5
            Top = 5
            Width = 495
            Height = 360
            Caption = '(*)Tabelleneinstellung'
            TabOrder = 0
            object gbSchrift: TmmGroupBox
              Left = 10
              Top = 20
              Width = 475
              Height = 90
              Caption = '(40)Schrift'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 0
              object paTitleColor: TmmPanel
                Left = 10
                Top = 20
                Width = 220
                Height = 30
                Hint = '(*)Mit Mausklick Farbauswahl oeffnen'
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = bTitelColorClick
                object laTitleFont: TmmLabel
                  Left = 10
                  Top = 8
                  Width = 38
                  Height = 13
                  Hint = '(*)Mit Mausklick Schriftauswahl oeffnen'
                  Caption = '(30)Titel'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  Visible = True
                  OnClick = bTitelFontClick
                  AutoLabel.LabelPosition = lpLeft
                end
              end
              object bTitelFont: TmmBitBtn
                Tag = 1
                Left = 10
                Top = 55
                Width = 110
                Height = 25
                Hint = '(*)Schrifttyp einstellen'
                Caption = '(20)Schrifttyp setzen'
                TabOrder = 1
                Visible = True
                OnClick = bTitelFontClick
                AutoLabel.LabelPosition = lpLeft
              end
              object bTitelColor: TmmBitBtn
                Tag = 1
                Left = 120
                Top = 55
                Width = 110
                Height = 25
                Hint = '(*)Farbe setzen'
                Caption = '(20)Farbe setzen'
                TabOrder = 2
                Visible = True
                OnClick = bTitelColorClick
                AutoLabel.LabelPosition = lpLeft
              end
              object paColor: TmmPanel
                Left = 245
                Top = 20
                Width = 220
                Height = 30
                Hint = '(*)Mit Mausklick Farbauswahl oeffnen'
                BevelInner = bvRaised
                BevelOuter = bvLowered
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                ParentShowHint = False
                ShowHint = True
                TabOrder = 3
                OnClick = bDataColorClick
                object laFont: TmmLabel
                  Left = 10
                  Top = 8
                  Width = 47
                  Height = 13
                  Hint = '(*)Mit Mausklick Schriftauswahl oeffnen'
                  Caption = '(30)Daten'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clBlack
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  Visible = True
                  OnClick = bDatenFontClick
                  AutoLabel.LabelPosition = lpLeft
                end
              end
              object bDatenFont: TmmBitBtn
                Tag = 2
                Left = 245
                Top = 55
                Width = 110
                Height = 25
                Hint = '(*)Schrifttyp einstellen'
                Caption = '(20)Schrifttyp setzen'
                TabOrder = 4
                Visible = True
                OnClick = bDatenFontClick
                AutoLabel.LabelPosition = lpLeft
              end
              object bDataColor: TmmBitBtn
                Tag = 2
                Left = 355
                Top = 55
                Width = 110
                Height = 25
                Hint = '(*)Farbe setzen'
                Caption = '(20)Farbe setzen'
                TabOrder = 5
                Visible = True
                OnClick = bDataColorClick
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object gbGridOptions: TmmGroupBox
              Left = 10
              Top = 120
              Width = 475
              Height = 170
              Caption = '(40)Anzeige'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object cbRowLines: TmmCheckBox
                Left = 10
                Top = 20
                Width = 400
                Height = 17
                Caption = '(80)Zeilenlinien anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 0
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object cbColLines: TmmCheckBox
                Left = 10
                Top = 40
                Width = 400
                Height = 17
                Caption = '(80)Spaltenlinien anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object cbIndicator: TmmCheckBox
                Left = 10
                Top = 60
                Width = 400
                Height = 17
                Caption = '(80)Spaltenzeiger anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object cbRowSelect: TmmCheckBox
                Left = 10
                Top = 140
                Width = 400
                Height = 17
                Caption = '(80)Ganze Zeile markiert anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                Visible = False
                AutoLabel.LabelPosition = lpLeft
              end
              object cbAlwaysShowSelection: TmmCheckBox
                Left = 10
                Top = 80
                Width = 400
                Height = 17
                Caption = '(80)Markierung permanent anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object cbTitleAlignment: TmmCheckBox
                Left = 10
                Top = 100
                Width = 400
                Height = 17
                Caption = '(80)Spaltentitel zentriert anzeigen'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object cbTrailingEllipsis: TmmCheckBox
                Left = 10
                Top = 120
                Width = 400
                Height = 17
                Caption = '(80)Fortsetzungspunkte (...)'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clBlack
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object gbFixedCols: TmmGroupBox
              Left = 10
              Top = 300
              Width = 475
              Height = 50
              Caption = '(40)Fixierte Spalten'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clBlack
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 2
              object edFixedCols: TmmEdit
                Left = 10
                Top = 20
                Width = 40
                Height = 21
                Color = clWindow
                ReadOnly = True
                TabOrder = 0
                Text = '0'
                Visible = True
                AutoLabel.LabelPosition = lpLeft
                ReadOnlyColor = clInfoBk
                ShowMode = smNormal
              end
              object udFixedCols: TmmUpDown
                Left = 50
                Top = 20
                Width = 15
                Height = 21
                Associate = edFixedCols
                Min = 0
                Max = 10
                Position = 0
                TabOrder = 1
                Wrap = False
              end
            end
          end
          object rgBarStyle: TmmRadioGroup
            Left = 510
            Top = 5
            Width = 314
            Height = 81
            Caption = '(*)Grafikeinstellung'
            ItemIndex = 0
            Items.Strings = (
              '(30)Balken nebeneinander'
              '(30)Balken gestapelt'
              '(30)Balken gestapelt normiert')
            TabOrder = 1
            OnClick = rgBarStyleClick
          end
          object gbGraphicSettings: TmmGroupBox
            Left = 510
            Top = 96
            Width = 314
            Height = 265
            Caption = '(*)Grafikeinstellung Y-Achsen'
            TabOrder = 2
            object gbLine: TmmGroupBox
              Left = 10
              Top = 24
              Width = 295
              Height = 105
              Caption = '(*)Liniengrafik'
              TabOrder = 0
              object laLineMin: TmmLabel
                Left = 15
                Top = 52
                Width = 85
                Height = 13
                Caption = '(*)Vorgabe Y (min)'
                FocusControl = edLineMin
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object laLineMax: TmmLabel
                Left = 15
                Top = 76
                Width = 88
                Height = 13
                Caption = '(*)Vorgabe Y (max)'
                FocusControl = edLineMax
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object chbAutoScaleLine: TmmCheckBox
                Left = 15
                Top = 24
                Width = 97
                Height = 17
                Caption = '(*)Automatisch'
                Checked = True
                State = cbChecked
                TabOrder = 0
                Visible = True
                OnClick = chbAutoScaleLineClick
                AutoLabel.LabelPosition = lpLeft
              end
              object edLineMin: TNumEdit
                Left = 150
                Top = 48
                Width = 121
                Height = 21
                Decimals = 0
                Digits = 12
                Masks.PositiveMask = '#,##0'
                NumericType = ntGeneral
                TabOrder = 1
                UseRounding = True
                Validate = False
                ValidateString = 
                  'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
                  '%s'
              end
              object edLineMax: TNumEdit
                Left = 150
                Top = 72
                Width = 121
                Height = 21
                Decimals = 0
                Digits = 12
                Masks.PositiveMask = '#,##0'
                NumericType = ntGeneral
                TabOrder = 2
                UseRounding = True
                Validate = False
                ValidateString = 
                  'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
                  '%s'
              end
            end
            object gbBar: TmmGroupBox
              Left = 10
              Top = 144
              Width = 295
              Height = 105
              Caption = '(*)Balkengrafik'
              TabOrder = 1
              object laBarMin: TmmLabel
                Left = 15
                Top = 52
                Width = 85
                Height = 13
                Caption = '(*)Vorgabe Y (min)'
                FocusControl = edBarMin
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object laBarMax: TmmLabel
                Left = 15
                Top = 76
                Width = 88
                Height = 13
                Caption = '(*)Vorgabe Y (max)'
                FocusControl = edBarMax
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
              object chbAutoScaleBar: TmmCheckBox
                Left = 15
                Top = 24
                Width = 97
                Height = 17
                Caption = '(*)Automatisch'
                Checked = True
                State = cbChecked
                TabOrder = 0
                Visible = True
                OnClick = chbAutoScaleBarClick
                AutoLabel.LabelPosition = lpLeft
              end
              object edBarMin: TNumEdit
                Left = 150
                Top = 48
                Width = 121
                Height = 21
                Decimals = 0
                Digits = 12
                Masks.PositiveMask = '#,##0'
                NumericType = ntGeneral
                TabOrder = 1
                UseRounding = True
                Validate = False
                ValidateString = 
                  'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
                  '%s'
              end
              object edBarMax: TNumEdit
                Left = 150
                Top = 72
                Width = 121
                Height = 21
                Decimals = 0
                Digits = 12
                Masks.PositiveMask = '#,##0'
                NumericType = ntGeneral
                TabOrder = 2
                UseRounding = True
                Validate = False
                ValidateString = 
                  'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
                  '%s'
              end
            end
          end
        end
      end
      object tsSummary: TTabSheet
        Caption = '(*)Zusammenfassung'
        ImageIndex = 3
        OnExit = tsSummaryExit
        object sgSummary: TmmExtStringGrid
          Left = 0
          Top = 0
          Width = 829
          Height = 370
          Align = alClient
          ColCount = 11
          DefaultRowHeight = 15
          FixedCols = 0
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goThumbTracking]
          TabOrder = 0
          Visible = True
          AutoNumAlign = True
          AutoSize = False
          VAlignment = vtaTop
          EnhTextSize = False
          EnhRowColMove = False
          SortFixedCols = False
          SizeWithForm = False
          Multilinecells = False
          OnGetCellColor = sgSummaryGetCellColor
          OnGetAlignment = sgSummaryGetAlignment
          OnDblClickCell = sgSummaryDblClickCell
          SortDirection = sdAscending
          SortFull = True
          SortAutoFormat = True
          SortShow = False
          EnableGraphics = False
          SortColumn = 0
          HintColor = clInfoBk
          SelectionColor = clHighlight
          SelectionTextColor = clHighlightText
          SelectionRectangle = False
          SelectionRTFKeep = False
          HintShowCells = False
          OleDropTarget = False
          OleDropSource = False
          OleDropRTF = False
          PrintSettings.FooterSize = 0
          PrintSettings.HeaderSize = 0
          PrintSettings.Time = ppNone
          PrintSettings.Date = ppNone
          PrintSettings.DateFormat = 'dd/mm/yyyy'
          PrintSettings.PageNr = ppNone
          PrintSettings.Title = ppNone
          PrintSettings.Font.Charset = DEFAULT_CHARSET
          PrintSettings.Font.Color = clWindowText
          PrintSettings.Font.Height = -11
          PrintSettings.Font.Name = 'MS Sans Serif'
          PrintSettings.Font.Style = []
          PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
          PrintSettings.HeaderFont.Color = clWindowText
          PrintSettings.HeaderFont.Height = -11
          PrintSettings.HeaderFont.Name = 'MS Sans Serif'
          PrintSettings.HeaderFont.Style = []
          PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
          PrintSettings.FooterFont.Color = clWindowText
          PrintSettings.FooterFont.Height = -11
          PrintSettings.FooterFont.Name = 'MS Sans Serif'
          PrintSettings.FooterFont.Style = []
          PrintSettings.Borders = pbNoborder
          PrintSettings.BorderStyle = psSolid
          PrintSettings.Centered = False
          PrintSettings.RepeatFixedRows = False
          PrintSettings.RepeatFixedCols = False
          PrintSettings.LeftSize = 0
          PrintSettings.RightSize = 0
          PrintSettings.ColumnSpacing = 0
          PrintSettings.RowSpacing = 0
          PrintSettings.TitleSpacing = 0
          PrintSettings.Orientation = poPortrait
          PrintSettings.FixedWidth = 0
          PrintSettings.FixedHeight = 0
          PrintSettings.UseFixedHeight = False
          PrintSettings.UseFixedWidth = False
          PrintSettings.FitToPage = fpNever
          PrintSettings.PageNumSep = '/'
          PrintSettings.NoAutoSize = False
          PrintSettings.PrintGraphics = False
          HTMLSettings.Width = 100
          Navigation.AllowInsertRow = False
          Navigation.AllowDeleteRow = False
          Navigation.AdvanceOnEnter = False
          Navigation.AdvanceInsert = False
          Navigation.AutoGotoWhenSorted = False
          Navigation.AutoGotoIncremental = False
          Navigation.AutoComboDropSize = False
          Navigation.AdvanceDirection = adLeftRight
          Navigation.AllowClipboardShortCuts = False
          Navigation.AllowSmartClipboard = False
          Navigation.AllowRTFClipboard = False
          Navigation.AdvanceAuto = False
          Navigation.InsertPosition = pInsertBefore
          Navigation.CursorWalkEditor = False
          Navigation.MoveRowOnSort = False
          Navigation.ImproveMaskSel = False
          Navigation.AlwaysEdit = False
          ColumnSize.Save = False
          ColumnSize.Stretch = True
          ColumnSize.Location = clRegistry
          CellNode.Color = clSilver
          CellNode.NodeType = cnFlat
          CellNode.NodeColor = clBlack
          SizeWhileTyping.Height = False
          SizeWhileTyping.Width = False
          MouseActions.AllSelect = False
          MouseActions.ColSelect = False
          MouseActions.RowSelect = False
          MouseActions.DirectEdit = False
          MouseActions.DisjunctRowSelect = False
          MouseActions.AllColumnSize = False
          MouseActions.CaretPositioning = False
          IntelliPan = ipVertical
          URLColor = clBlack
          URLShow = False
          URLFull = False
          URLEdit = False
          ScrollType = ssNormal
          ScrollColor = clNone
          ScrollWidth = 16
          ScrollProportional = False
          ScrollHints = shNone
          OemConvert = False
          FixedFooters = 0
          FixedRightCols = 0
          FixedColWidth = 14
          FixedRowHeight = 15
          FixedFont.Charset = DEFAULT_CHARSET
          FixedFont.Color = clWindowText
          FixedFont.Height = -11
          FixedFont.Name = 'MS Sans Serif'
          FixedFont.Style = []
          WordWrap = True
          Lookup = False
          LookupCaseSensitive = False
          LookupHistory = False
          BackGround.Top = 0
          BackGround.Left = 0
          BackGround.Display = bdTile
          Hovering = False
          Filter = <>
          FilterActive = False
          AutoLabel.LabelPosition = lpLeft
          ColWidths = (
            14
            14
            14
            14
            14
            14
            14
            14
            14
            14
            658)
          RowHeights = (
            15
            15)
        end
      end
    end
  end
  object ActionList1: TmmActionList
    Left = 705
    Top = 30
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Dialog schliessen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfe aufrufen...'
      ImageIndex = 2
    end
    object acMoveTo_lbShow: TAction
      Hint = '(*)Feld zur Anzeige hinzufuegen'
      OnExecute = acMoveTo_lbShowExecute
    end
    object acMoveTo_lbAvailable: TAction
      Hint = '(*)Feld aus Anzeige entfernen'
      OnExecute = acMoveTo_lbAvailableExecute
    end
    object acMoveAllTo_lbShow: TAction
      Hint = '(*)Alle Felder zur Anzeige hinzufuegen'
      OnExecute = acMoveAllTo_lbShowExecute
    end
    object acMoveAllTo_lbAvailable: TAction
      Hint = '(*)Alle Felder aus Anzeige entfernen'
      OnExecute = acMoveAllTo_lbAvailableExecute
    end
    object acColorPanel: TAction
      Caption = '(*)Farbe setzen'
      Hint = '(*)Farbe setzen'
      OnExecute = acColorPanelExecute
    end
    object acFont: TAction
      Caption = '(*)Schrifttyp'
      Hint = '(*)Schrifttyp einstellen'
      OnExecute = acFontExecute
    end
    object acSetParam: TAction
      OnExecute = acSetParamExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 740
    Top = 30
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Lines'
        0))
  end
  object ColorDialog1: TmmColorDialog
    Ctl3D = True
    Left = 640
    Top = 30
  end
  object FontDialog1: TmmFontDialog
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'System'
    Font.Style = []
    MinFontSize = 0
    MaxFontSize = 0
    Left = 670
    Top = 30
  end
end
