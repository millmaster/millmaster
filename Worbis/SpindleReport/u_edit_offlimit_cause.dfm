object EditOfflimitCauseDlg: TEditOfflimitCauseDlg
  Left = 503
  Top = 60
  Width = 342
  Height = 321
  BorderIcons = [biSystemMenu]
  Caption = '(60)Offlimit-Ursachen bearbeiten'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = True
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 334
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 0
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton1: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 34
      Top = 0
      Action = acResetCause
    end
    object ToolButton5: TToolButton
      Left = 57
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bPrint: TToolButton
      Left = 65
      Top = 0
      Action = acPrint
    end
    object ToolButton3: TToolButton
      Left = 88
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsDivider
    end
    object DBNavigator1: TmmDBNavigator
      Left = 96
      Top = 0
      Width = 92
      Height = 22
      DataSource = srcOffCause
      VisibleButtons = [nbInsert, nbDelete, nbPost, nbCancel]
      Flat = True
      Hints.Strings = (
        '(*)Erster Datensatz'
        '(*)Vorgaengiger Datensatz'
        '(*)Naechster Datensatz'
        '(*)Letzter Datensatz'
        '(*)Einfuegen'
        '(*)Loeschen'
        '(*)Bearbeiten'
        '(*)Speichern'
        '(*)Abbrechen'
        '(*)Daten aktualisieren')
      ConfirmDelete = False
      TabOrder = 0
      UseMMHints = True
    end
    object ToolButton4: TToolButton
      Left = 188
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bHelp: TToolButton
      Left = 196
      Top = 0
      Action = acHelp
    end
  end
  object Panel1: TmmPanel
    Left = 0
    Top = 26
    Width = 334
    Height = 268
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object wwDBGrid1: TwwDBGrid
      Left = 0
      Top = 0
      Width = 334
      Height = 268
      IniAttributes.Delimiter = ';;'
      TitleColor = clBtnFace
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = srcOffCause
      KeyOptions = [dgEnterToTab, dgAllowInsert]
      Options = [dgEditing, dgTitles, dgIndicator, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis]
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      TitleLines = 2
      TitleButtons = False
    end
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    Left = 26
    Top = 130
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      ShortCut = 16397
      OnExecute = acCloseExecute
    end
    object acPrint: TAction
      Hint = '(*)Einstellungen ausdrucken'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acResetCause: TAction
      Hint = '(*)Ursache zuruecksetzen...'
      ImageIndex = 66
      OnExecute = acResetCauseExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 56
    Top = 128
    TargetsData = (
      1
      7
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        '*'
        'Filter'
        0))
  end
  object tabOffCause: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    BeforePost = tabOffCauseBeforePost
    BeforeDelete = tabOffCauseBeforeDelete
    OnNewRecord = tabOffCauseNewRecord
    CommandText = 'dbo.t_offlimit_cause'
    CommandType = cmdTable
    IndexFieldNames = 'c_offcause_name'
    Parameters = <>
    Left = 16
    Top = 66
    object tabOffCausec_offcause_name: TStringField
      DisplayWidth = 37
      FieldName = 'c_offcause_name'
      Required = True
      Size = 40
    end
    object tabOffCausec_Counter: TIntegerField
      DisplayWidth = 11
      FieldName = 'c_Counter'
      ReadOnly = True
      DisplayFormat = ',0'
    end
    object tabOffCausec_offcause_id: TIntegerField
      FieldName = 'c_offcause_id'
      Required = True
      Visible = False
    end
  end
  object srcOffCause: TwwDataSource
    DataSet = tabOffCause
    Left = 48
    Top = 66
  end
  object qryReset: TmmADOCommand
    CommandText = 
      'UPDATE t_offlimit_cause SET c_Eff_dev = null,    c_CYTot_dev = n' +
      'ull,    c_CSIRO_dev = null,    c_CSp_dev = null,    c_RSp_dev = ' +
      'null,    c_YB_dev = null,    c_CBu_dev = null,    c_CUpY_dev = n' +
      'ull,    c_tLSt_dev = null,    c_LSt_dev = null,    c_RedL_dev = ' +
      'null,    c_Counter = null,    c_Eff_offcnt = null,    c_CYTot_of' +
      'fcnt = null,    c_CSIRO_offcnt = null,    c_CSp_offcnt = null,  ' +
      '  c_RSp_offcnt = null,    c_YB_offcnt = null,    c_CBu_offcnt = ' +
      'null,    c_CUpY_offcnt = null,    c_tLSt_offcnt = null,    c_LSt' +
      '_offcnt = null,    c_op_offcnt = null,    c_RedL_offcnt = null W' +
      'HERE c_offcause_id = :c_offcause_id'
    Connection = dmSpindleReport.conDefault
    Parameters = <
      item
        Name = 'c_offcause_id'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Left = 24
    Top = 162
  end
end
