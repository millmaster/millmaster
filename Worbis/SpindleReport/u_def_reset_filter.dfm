inherited frmDefResetFilter: TfrmDefResetFilter
  Left = 510
  Top = 278
  ActiveControl = comRuntime
  BorderStyle = bsDialog
  Caption = '(*)Loesch-Filter fuer Spindeln definieren'
  ClientHeight = 120
  ClientWidth = 375
  KeyPreview = True
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel2: TmmPanel
    Left = 0
    Top = 90
    Width = 375
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOK: TmmButton
      Left = 135
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 215
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 295
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 375
    Height = 90
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object GroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 365
      Height = 80
      Caption = '(30)Filterbedingung'
      TabOrder = 0
      object mmLabel1: TmmLabel
        Left = 10
        Top = 24
        Width = 145
        Height = 13
        AutoSize = False
        Caption = '(30)Laufzeit (h)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 10
        Top = 49
        Width = 145
        Height = 13
        AutoSize = False
        Caption = '(30)Offlimit-Zeit / Laufzeit (%)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edRuntime: TNumEdit
        Left = 275
        Top = 20
        Width = 80
        Height = 21
        Decimals = 0
        Digits = 12
        Masks.PositiveMask = ',0 h'
        Masks.ZeroMask = ' '
        Max = 99999
        NumericType = ntGeneral
        TabOrder = 1
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
      object edRatio: TNumEdit
        Left = 275
        Top = 45
        Width = 80
        Height = 21
        Decimals = 0
        Digits = 12
        Masks.PositiveMask = '0 %'
        Masks.ZeroMask = ' '
        Max = 100
        NumericType = ntGeneral
        TabOrder = 3
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
      object comRuntime: TmmComboBox
        Left = 160
        Top = 20
        Width = 105
        Height = 21
        Style = csDropDownList
        Color = clWindow
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        Items.Strings = (
          '(*)groesser als'
          '(*)kleiner als')
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object comRatio: TmmComboBox
        Left = 160
        Top = 45
        Width = 105
        Height = 21
        Style = csDropDownList
        Color = clWindow
        ItemHeight = 13
        TabOrder = 2
        Visible = True
        Items.Strings = (
          '(*)groesser als'
          '(*)kleiner als')
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
    end
  end
  object ActionList1: TmmActionList
    Left = 98
    Top = 2
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 130
    Top = 3
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
