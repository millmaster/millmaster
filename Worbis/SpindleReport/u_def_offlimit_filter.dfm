inherited DefOfflimitReportDlg: TDefOfflimitReportDlg
  Left = 658
  Top = 369
  ActiveControl = bOK
  BorderStyle = bsDialog
  Caption = '(40)Filterbedingung fuer Offlimit-Bericht'
  ClientHeight = 241
  ClientWidth = 280
  KeyPreview = True
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object paOnline: TmmPanel
    Left = 0
    Top = 0
    Width = 280
    Height = 100
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    Visible = False
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 270
      Height = 90
      Caption = '(30)Filterbedingung'
      TabOrder = 0
      object Label1: TmmLabel
        Left = 10
        Top = 26
        Width = 156
        Height = 13
        Caption = '(30)Anzeigeunterdrueckung (Std)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object cbOutOfProdSpindles: TmmCheckBox
        Left = 10
        Top = 65
        Width = 250
        Height = 17
        Caption = '(40)Stehende Spulstellen anzeigen'
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edOnlineOfflimitMin: TNumEdit
        Left = 200
        Top = 22
        Width = 61
        Height = 21
        Decimals = 0
        Digits = 12
        Masks.PositiveMask = ',0 Std'
        Max = 99999
        NumericType = ntGeneral
        TabOrder = 0
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
    end
  end
  object paOffline: TmmPanel
    Left = 0
    Top = 100
    Width = 280
    Height = 90
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    Visible = False
    object GroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 270
      Height = 80
      Caption = '(30)Filterbedingung'
      TabOrder = 0
      object mmLabel1: TmmLabel
        Left = 10
        Top = 24
        Width = 191
        Height = 13
        AutoSize = False
        Caption = '(30)Anzeigeunterdrueckung (Std)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 10
        Top = 49
        Width = 191
        Height = 13
        AutoSize = False
        Caption = '(30)Offlimit-Zeit / Laufzeit (%)'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edOfflineOfflimitMin: TNumEdit
        Left = 200
        Top = 20
        Width = 61
        Height = 21
        Decimals = 0
        Digits = 12
        Masks.PositiveMask = ',0 Std'
        Max = 99999
        NumericType = ntGeneral
        TabOrder = 0
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
      object edOfflimitRatio: TNumEdit
        Left = 200
        Top = 45
        Width = 61
        Height = 21
        Decimals = 2
        Digits = 12
        Masks.PositiveMask = '0.00 %'
        Max = 100
        NumericType = ntGeneral
        TabOrder = 1
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 211
    Width = 280
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object bOK: TmmButton
      Left = 40
      Top = 3
      Width = 75
      Height = 25
      Hint = '(*)Auswahl uebernehmen'
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ModalResult = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      OnClick = bOKClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 120
      Top = 3
      Width = 75
      Height = 25
      Hint = '(*)Dialog abbrechen'
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ModalResult = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 200
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 154
    Top = 2
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 186
    Top = 3
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
