(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_standard_select.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Select dialog for small tables
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.04.99 | 0.00 | PW  | Datei erstellt
| 18.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_select_prodid;
interface
uses
 u_global,
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 BASEDIALOG,StdCtrls,ComCtrls,ToolWin,mmToolBar,Db,
 IvDictio,IvMulti,IvEMulti,ActnList,mmActionList, Nwcombo,
 mmButton, ExtCtrls, mmPanel, mmTranslator, BASEFORM, ADODB, mmADODataSet;

type
 TSelectProdIDDlg = class(TmmForm)
  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;
   IvExtendedTranslator1: TmmTranslator;
    quSelect: TmmADODataSet;

  lbNames: TNWListBox;
   paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure acHelpExecute(Sender: TObject);
  private
  public
   procedure Init(aRepInfo: rESPReportInfoT);
   function GetSelection: rListSelResultT;
 end; //TSelectProdIDDlg

var
 SelectProdIDDlg: TSelectProdIDDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 u_common_lib,u_common_global;

{$R *.DFM}

procedure TSelectProdIDDlg.acOKExecute(Sender: TObject);
begin
 inherited;
 Close;
 ModalResult := mrOK;
end; //procedure TSelectProdIDDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TSelectProdIDDlg.acCancelExecute(Sender: TObject);
begin
 inherited;
 Close;
 ModalResult := mrCancel;
end; //procedure TSelectProdIDDlg.acCancelExecute
//-----------------------------------------------------------------------------
function TSelectProdIDDlg.GetSelection: rListSelResultT;
begin
 result.ID := lbNames.GetItemID;
 result.Text := lbNames.GetItemText;
end; //function TSelectProdIDDlg.GetSelectedID
//-----------------------------------------------------------------------------
procedure TSelectProdIDDlg.Init(aRepInfo: rESPReportInfoT);
var
 xStr : string;
 
begin
 with quSelect do
 begin
  try
   parameters.ParamByName('MachineID').value := aRepInfo.MachineID;
   parameters.ParamByName('IntervalStart').value := aRepInfo.StartTime;
   parameters.ParamByName('IntervalEnd').value := aRepInfo.EndTime;

   Open;

   while not Eof do
   begin
    xStr := 'Spd: '+lz(FieldByName('c_spindle_first').asinteger,2) +'-'
                   +lz(FieldByName('c_spindle_last').asinteger,2) +'    '
                   +FieldByName('partie_name').asstring;

    lbNames.AddObj(xstr,FieldByName('c_prod_id').asinteger);
    Next;
   end; //while not Eof do
  finally
   acOK.Enabled := lbNames.Items.Count > 0;
   if acOK.Enabled then lbNames.ItemIndex := 0;
   Active := false;
  end; //try..finally
 end; //with quSelect do
end; //procedure TSelectProdIDDlg.Init
//-----------------------------------------------------------------------------
procedure TSelectProdIDDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end; //procedure TSelectProdIDDlg.acHelpExecute
//-----------------------------------------------------------------------------

end. //u_select_prodid.pas
 
