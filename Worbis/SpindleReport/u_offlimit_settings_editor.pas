(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_offlimit_settings_editor.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 25.04.99 | 0.00 | PW  | Datei erstellt
| 31.05.00 | 1.00 | Mg  | Insert und Delete ec.. und Copy Button in DBNavigator ausgeschaltet, bis Maschinenzuordnung der Settings gemacht ist
| 26.07.00 | 1.01 | Mg  | TOffLimitEditorDlg.DBNavigator1BeforeAction and print inserted
| 06.11.00 | 1.09 | Wss | MemoIni.Applicationname set to gApplicationname to solve 8.3 naming problems
| 04.07.01 | 1.10 | Wss | - Problem mit Zuweisung von Offlimits an Maschine behoben -> mmDBGrid verwendet
                          - unnoetige Komponenten entfernt -> wwDBGrid, cobwwDBCombobox, etc.
| 16.08.01 |      | PW  | fields "c_tLSt_PC_max" and "c_LSt_PC_max" removed
                          - fields "tabMachinec_machine_name" and "tabMachinec_nr_of_spindles" set readonly
                          - abort insert (ins, cursor down) and delete (ctrl-del) in dgMaOfflimit
| 29.08.02 |      | Wss | Code from Init() method moved to OnFormShow because of security checks for
                          Dis/Enabled security attributes
| 18.10.02 |      | LOK | Umbau ADO
| 03.07.03 |      | Wss | - �berpr�fung von Min/Max Werten verschoben in BeforePost Event und die
                            automatische Korrektur entfernt. Es werden nur die absoluten Werte gepr�ft
                          - tLSt wird nun mit Intervall�nge gepr�ft
                            tLSt < IntervalLen
                          - nLSt wird nun noch mit der kleinsten LangstopDef Zeit von der DB gepr�ft
                            nLSt < (IntervalLen div MIN(langstopDef))
| 14.06.04 |      | SDO | Neuer Drucker-DLG & Print-Layout, Anpassungen, mmDBGrid [dgMaOfflimit] durch TwwDBGrid ersetzt
|=========================================================================================*)

{$I symbols.inc}

unit u_offlimit_settings_editor;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, ExtCtrls, DBCtrls, ComCtrls, ToolWin, mmToolBar, ActnList, mmActionList,
  IvDictio, IvMulti, IvEMulti, StdCtrls, Mask, Db,
  mmDataSource, wwDialog, Wwlocate, mmScrollBox, mmDBCheckBox,
  mmLabel, mmDBEdit, mmGroupBox, mmPageControl, mmPanel, Grids,
  Wwdbgrid, Wwdatsrc,
  u_global, MMSecurity, mmTranslator, mmDBNavigator, wwdbedit, Wwdotdot,
  Wwdbcomb, DBGrids, mmDBGrid, Buttons, mmSpeedButton, ADODB, mmADODataSet,
  LoepfeGlobal, Wwdbigrd, mmComboBox, mmDbValCb, Menus, mmPopupMenu, mmStringList,
  mmADOCommand;

type
  TOffLimitEditorDlg = class(TmmForm)
    mmTranslator1: TmmTranslator;

    ActionList1: TmmActionList;
    acClose: TAction;
    acCopy: TAction;
    acPrint: TAction;
    acHelp: TAction;

    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton3: TToolButton;
    bCopy: TToolButton;
    bPrint: TToolButton;
    ToolButton4: TToolButton;
    bHelp: TToolButton;
    DBNavigator1: TmmDBNavigator;
    ToolButton1: TToolButton;
    srcOfflimitSettings: TmmDataSource;
    qryWork: TmmADODataSet;

    Panel1: TmmPanel;
    PageControl: TmmPageControl;
    tsFormular: TTabSheet;
    tsGrid: TTabSheet;
    tsAssignment: TTabSheet;
    gbOfflimitName: TmmGroupBox;
    DBEdit1: TmmDBEdit;
    GroupBox3: TmmGroupBox;
    paGroup: TmmPanel;
    ScrollBox1: TmmScrollBox;
    wwDBGrid1: TwwDBGrid;
    tabMachine: TmmADODataSet;
    srcMachine: TmmDataSource;
    tabMachinec_machine_type: TSmallintField;
    tabMachinec_maoffset_id: TIntegerField;
    tabMachinec_machine_name: TStringField;
    tabMachinec_nr_of_spindles: TSmallintField;
    tabMachinelookMaOfflimitName: TStringField;
    tabMachinelookMaTyp: TStringField;
    tabMachinec_node_id: TStringField;
    acEditBaseLength: TAction;
    acEditAverageTime: TAction;
    bSecurity: TmmSpeedButton;
    acSecurity: TAction;
    MMSecurityControl: TMMSecurityControl;
    acEditOfflimit: TAction;
    laOfflimitName: TmmLabel;
    DBCheckBox3: TmmDBCheckBox;
    DBCheckBox2: TmmDBCheckBox;
    laC_Average_time: TmmLabel;
    edAverageTime: TmmDBEdit;
    laMaEff: TmmLabel;
    laCYTot: TmmLabel;
    Label10: TmmLabel;
    Label9: TmmLabel;
    Label15: TmmLabel;
    Label8: TmmLabel;
    Label11: TmmLabel;
    Label13: TmmLabel;
    Label16: TmmLabel;
    Label17: TmmLabel;
    DBEdit4: TmmDBEdit;
    DBEdit5: TmmDBEdit;
    DBEdit6: TmmDBEdit;
    DBEdit7: TmmDBEdit;
    DBEdit11: TmmDBEdit;
    DBEdit13: TmmDBEdit;
    DBEdit15: TmmDBEdit;
    DBEdit17: TmmDBEdit;
    DBEdit23: TmmDBEdit;
    DBEdit19: TmmDBEdit;
    DBEdit21: TmmDBEdit;
    DBEdit24: TmmDBEdit;
    DBEdit26: TmmDBEdit;
    Panel2: TmmPanel;
    laLowBorder: TmmLabel;
    laHighBorder: TmmLabel;
    mmPanel1: TmmPanel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel5: TmmLabel;
    tabOfflimitSettings: TmmADODataSet;
    tabOfflimitSettingsc_maoffset_name: TStringField;
    tabOfflimitSettingsc_Average_time: TIntegerField;
    tabOfflimitSettingsc_OffBorderByAverage: TBooleanField;
    tabOfflimitSettingsc_OffBorderByFixValues: TBooleanField;
    tabOfflimitSettingsc_Eff_PC_max: TIntegerField;
    tabOfflimitSettingsc_Eff_PC_min: TIntegerField;
    tabOfflimitSettingsc_Eff_max: TIntegerField;
    tabOfflimitSettingsc_Eff_min: TIntegerField;
    tabOfflimitSettingsc_CYTot_PC_max: TIntegerField;
    tabOfflimitSettingsc_CYTot_PC_minXX: TIntegerField;
    tabOfflimitSettingsc_CYTot_max: TFloatField;
    tabOfflimitSettingsc_CYTot_min: TFloatField;
    tabOfflimitSettingsc_CSIRO_PC_max: TIntegerField;
    tabOfflimitSettingsc_CSIRO_max: TFloatField;
    tabOfflimitSettingsc_CSp_PC_max: TIntegerField;
    tabOfflimitSettingsc_CSp_max: TFloatField;
    tabOfflimitSettingsc_RSp_PC_max: TIntegerField;
    tabOfflimitSettingsc_RSp_max: TFloatField;
    tabOfflimitSettingsc_YB_PC_max: TIntegerField;
    tabOfflimitSettingsc_YB_Max: TFloatField;
    tabOfflimitSettingsc_CBu_PC_max: TIntegerField;
    tabOfflimitSettingsc_CBu_max: TFloatField;
    tabOfflimitSettingsc_CUpY_PC_max: TIntegerField;
    tabOfflimitSettingsc_CUpY_max: TFloatField;
    tabOfflimitSettingsc_tLSt_max: TIntegerField;
    tabOfflimitSettingsc_LSt_max: TIntegerField;
    tabOfflimitSettingsc_number_of_machines: TIntegerField;
    tabOfflimitSettingsc_maoffset_id: TIntegerField;
    tabOfflimitSettingsc_base: TIntegerField;
    DBEdit8: TmmDBEdit;
    DBEdit12: TmmDBEdit;
    DBEdit14: TmmDBEdit;
    DBEdit16: TmmDBEdit;
    DBEdit22: TmmDBEdit;
    DBEdit18: TmmDBEdit;
    DBEdit20: TmmDBEdit;
    DBEdit9: TmmDBEdit;
    laLenBaseTitle: TmmLabel;
    tabOfflimitSettingscalc_base: TStringField;
    bChangeLenBase: TmmSpeedButton;
    pmLenBase: TmmPopupMenu;
    N100km1: TMenuItem;
    N1000km1: TMenuItem;
    N1000km2: TMenuItem;
    N10000yrd1: TMenuItem;
    laLenBase: TmmLabel;
    comUpdateHide: TmmADOCommand;
    tabOfflimitSettingsc_ProdGrp_depend: TBooleanField;
    dgMaOfflimit: TmmDBGrid;
    procedure acCloseExecute(Sender: TObject);
    procedure tabOfflimitSettingsNewRecord(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure tabOfflimitSettingsBeforeDelete(DataSet: TDataSet);
    procedure tabOfflimitSettingsBeforeEdit(DataSet: TDataSet);
    procedure tabOfflimitSettingsBeforePost(DataSet: TDataSet);
    procedure acCopyExecute(Sender: TObject);
    procedure tabOfflimitSettingsCalcFields(DataSet: TDataSet);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure PageControlChange(Sender: TObject);
    procedure PageControlChanging(Sender: TObject; var AllowChange: Boolean);
    procedure bPrintClick(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure tabMachineAfterPost(DataSet: TDataSet);
    procedure tabMachineBeforeDelete(DataSet: TDataSet);
    procedure acSecurityExecute(Sender: TObject);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cobLenBaseChange(Sender: TObject);
    procedure tabOfflimitSettingsAfterScroll(DataSet: TDataSet);
    procedure bChangeLenBaseClick(Sender: TObject);
    procedure pmLenBaseClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tabMachinelookMaTypGetText(Sender: TField; var Text: String;
      DisplayText: Boolean);
  private
    mID: Integer;
    mDefaultLenBase: TLenBase;
    mCompareSettingsValues: TIniStringList;
    mMaxnLSt: Integer;
    procedure SetTranslations;
    function GetNumberOfMachines: integer;
    procedure UpdateLenBaseLabels;
    function CompareOfflimitSettings: Boolean;
    procedure PrintRapport;
  public
    procedure Init(aID: Longint);
  end;                                  //TOffLimitEditorDlg
//..............................................................................
  TOffSet = class
  private
    fOffSetName: string;
    fOffsetID: integer;
  public
    property OffSetName: string read fOffsetName write fOffsetName;
    property OffsetID: integer read fOffsetID write fOffsetID;
  end;

var
  OffLimitEditorDlg: TOffLimitEditorDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS, MMHtmlHelp, SettingsReader, MMUGlobal, BaseGlobal, mmEventLog,
  u_main, u_lib, mmDialogs, printers,
  u_common_global, u_common_lib, u_resourcestrings, u_print_list, u_OfflimitPrintFormular ,
  xmlMaConfigClasses, XMLDef;


//------------------------------------------------------------------------------
resourcestring
  rsMsgStr1     = '(*)Offlimit-Einstellung ist noch mit Maschinen verbunden. Loeschen nicht moeglich !'; //ivlm
  rsMsgStr2     = '(*)Soll die Offlimit-Einstellung mit der Bezeichnung "%s" geloescht werden ?'; //ivlm
  rsCaptionStr1 = '(60)Offlimit-Einstellung auswaehlen'; //ivlm
  rsHighBorder  = '(30)obere Grenze (/%s)'; //ivlm
  rsLowBorder   = '(30)untere Grenze (/%s)'; //ivlm
  rsSettingsChanged = '(*)Aufgrund der Aenderungen sind dazugehoerige Daten nicht mehr gueltig.~Diese werden ausgeblendet bis zur naechsten Offlimitberechnung.'; // ivlm
  rsMinMaxWrong     = '(*)Die Einstellungen koennen nicht gespeichert werden, da der Min. Wert im Feld "%s" groesser oder gleich dem Max. Wert ist.'; // ivlm
  rsMsgMaxtLStError = '(*)Die Langstopzeit muss kleiner sein als die aktuelle Intervallaenge (%d Minuten)!'; // ivlm
  rsMsgMaxLStError  = '(*)Die Anzahl der Langstops sollte kleiner als %d sein!'; // ivlm
  rsPrintOfflimitSetTitle = '(*)Offlimit-Einstellung';    // ivlm


//------------------------------------------------------------------------------
const
  cCompareValuesCnt = 24;
  cCompareValues: Array[0..cCompareValuesCnt-1] of String = (
    'c_base',
    'c_Average_time',
    'c_Eff_min',
    'c_Eff_max',
    'c_tLSt_max',
    'c_LSt_max',
    'c_CYTot_min',
    'c_CYTot_max',
    'c_CSIRO_max',
    'c_CSp_max',
    'c_RSp_max',
    'c_YB_max',
    'c_CBu_max',
    'c_CUpY_max',
    'c_Eff_PC_min',
    'c_Eff_PC_max',
    'c_CYTot_PC_min',
    'c_CYTot_PC_max',
    'c_CSIRO_PC_max',
    'c_CSp_PC_max',
    'c_RSp_PC_max',
    'c_YB_PC_max',
    'c_CBu_PC_max',
    'c_CUpY_PC_max'
  );
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.SetTranslations;
begin
  tabOfflimitSettings.DisableControls;
  try
    tabOfflimitSettingsc_maoffset_name.DisplayLabel        := cc_maoffset_nameStr;
    tabOfflimitSettingscalc_base.DisplayLabel              := cc_baseStr;
    tabOfflimitSettingsc_number_of_machines.Displaylabel   := cc_number_of_machinesStr;
    tabOfflimitSettingsc_Average_time.Displaylabel         := cc_Average_timeStr;
    tabOfflimitSettingsc_OffBorderByAverage.Displaylabel   := cc_OffBorderByAverageStr;
    tabOfflimitSettingsc_OffBorderByFixValues.Displaylabel := cc_OffBorderByFixValuesStr;
    tabOfflimitSettingsc_Eff_PC_max.Displaylabel           := cc_Eff_PC_maxStr;
    tabOfflimitSettingsc_Eff_PC_min.Displaylabel           := cc_Eff_PC_minStr;
    tabOfflimitSettingsc_Eff_max.Displaylabel              := cc_Eff_maxStr;
    tabOfflimitSettingsc_Eff_min.Displaylabel              := cc_Eff_minStr;
    tabOfflimitSettingsc_CYTot_PC_max.Displaylabel         := cc_CYTot_PC_maxStr;
//    tabOfflimitSettingsc_CYTot_PC_min.Displaylabel         := cc_CYTot_PC_minStr;
    tabOfflimitSettingsc_CSIRO_PC_max.Displaylabel         := cc_CSIRO_PC_maxStr;
    tabOfflimitSettingsc_CSp_PC_max.Displaylabel           := cc_CSp_PC_maxStr;
    tabOfflimitSettingsc_RSp_PC_max.Displaylabel           := cc_RSp_PC_maxStr;
    tabOfflimitSettingsc_YB_PC_max.Displaylabel            := cc_YB_PC_maxStr;
    tabOfflimitSettingsc_CBu_PC_max.Displaylabel           := cc_CBu_PC_maxStr;
    tabOfflimitSettingsc_CUpY_PC_max.Displaylabel          := cc_CUpY_PC_maxStr;
    tabOfflimitSettingsc_tLSt_max.Displaylabel             := cc_tLSt_maxStr;
    tabOfflimitSettingsc_LSt_max.Displaylabel              := cc_LSt_maxStr;

    tabOfflimitSettingsc_CYTot_min.Displaylabel := Format(cc_CYTot_minStr, [rsAbsolut]);
    tabOfflimitSettingsc_CYTot_max.Displaylabel := Format(cc_CYTot_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_CSIRO_max.Displaylabel := Format(cc_CSIRO_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_CSp_max.Displaylabel   := Format(cc_CSp_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_RSp_max.Displaylabel   := Format(cc_RSp_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_CBu_max.Displaylabel   := Format(cc_CBu_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_CUpY_max.Displaylabel  := Format(cc_CUpY_maxStr, [rsAbsolut]);
    tabOfflimitSettingsc_YB_Max.Displaylabel    := Format(cc_YB_MaxStr, [rsAbsolut]);
  finally
    tabOfflimitSettings.EnableControls;
  end;                                 //try..finally
end;                                    //procedure TOffLimitEditorDlg.SetTranslations
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.acCloseExecute(Sender: TObject);
begin
  if PostRecord(tabOfflimitSettings) then
    Close;
end;                                    //procedure TOffLimitEditorDlg.acCloseExecute
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsNewRecord(DataSet: TDataSet);
var
  xID: longint;
begin
  xID := MakeUniqueID(3, tabOfflimitSettings.CommandText, 'c_maoffset_id', Self);
  if xID > 0 then begin
    tabOfflimitSettingsc_maoffset_id.Value := xID;
    tabOfflimitSettingsc_Average_time.value := 480;
    tabOfflimitSettingsc_OffBorderByAverage.value := true;
    tabOfflimitSettingsc_OffBorderByFixValues.value := true;
    tabOfflimitSettingsc_base.value := ord(mDefaultLenBase);
  end else
    SysUtils.Abort;
end; //procedure TOffLimitEditorDlg.tabOfflimitSettingsNewRecord
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Offlimit-Einstellungen.htm');
  mID := 0;
  mCompareSettingsValues := TIniStringList.Create;

  with TMMSettingsReader.Instance do begin
    mDefaultLenBase := TLenBase(Value[cMMUnit]);
    if mDefaultLenBase = lbAbsolute then
      mDefaultLenBase := lb100km;
  end;

  PageControl.ActivePage := tsFormular;
  tabOfflimitSettingsc_Average_time.Visible := MMSecurityControl.CanVisible(acEditAverageTime);
  edAverageTime.Visible := tabOfflimitSettingsc_Average_time.Visible;

//  tabOfflimitSettingsc_base.Visible := MMSecurityControl.CanVisible(acEditBaseLength);
//  edBaseLength.Visible := tabOfflimitSettingsc_base.Visible;
end;                                    //procedure TOffLimitEditorDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.Init(aID: Longint);
const
  cQryMinLongstoptime = 'select MIN(c_longStopDef) MinTime from t_machine';
  cQryAllMachIDs = 'select c_machine_id from t_machine';
var
  xInt: Integer;
  xMintLSt: Integer;
  xMaConfig: TMaConfigReader;
begin
  mID := aID;

  xMintLSt := 0;
  with qryWork do
  try
    Close;
    CommandText := cQryAllMachIDs;
    Open;
    if FindFirst then begin
      xMaConfig := TMaConfigReader.Create;
      try
        while not EOF do begin
          xMaConfig.MachID := FieldByName('c_machine_id').AsInteger;
          if xMaConfig.Available then begin
            xInt := xMaConfig.MachValue[cXPLongStopDefItem];
            // die kleinste LongStopDef herausfinden
            if ((xInt > 0) and (xInt < xMintLSt)) or (xMintLSt = 0) then
              xMintLSt := xInt;
          end;
          Next;
        end;
      finally
        xMaConfig.Free;
      end;
    end; // if FindFirst
    Close;
  except
    on e:Exception do begin
      CodeSite.SendError('GetMinLongstopTime: ' + e.Message);
    end;
  end;

  if xMintLSt = 0 then
    xMintLSt := 5;  // default 5 Minuten als Anhaltspunkt
  mMaxnLSt := TMMSettingsReader.Instance.Value[cIntervalLen] div xMintLSt;

//  mID := aID;
//
////  xMintLSt := 5;
//  with qryWork do
//  try
//    Close;
//    CommandText := cQryMinLongstoptime;
//    Open;
//    xMintLSt := FieldByName('MinTime').AsInteger div 60;
//  except
//    on e:Exception do begin
//      CodeSite.SendError('GetMinLongstopTime: ' + e.Message);
//    end;
//  end;
//  if xMintLSt = 0 then
//    xMintLSt := 5;  // default 5 Minuten als Anhaltspunkt
//  mMaxnLSt := TMMSettingsReader.Instance.Value[cIntervalLen] div xMintLSt;
end; //procedure TOffLimitEditorDlg.Init
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    PostMessage(Handle, WM_NEXTDLGCTL, 0, 0);
  end;
end;
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforeDelete(DataSet: TDataSet);
begin
  if IsLinkedMsg('t_machine', 'c_maoffset_id', rsMsgStr1, tabOfflimitSettingsc_maoffset_id.Value, Self) then SysUtils.abort;
  if MMMessageDlg(Format(rsMsgStr2, [tabOfflimitSettingsc_maoffset_name.value]), mtConfirmation, [mbYes, mbNo], 0, Self) <> mrYes then
    SysUtils.Abort;
end;                                    //procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforeDelete
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforeEdit(DataSet: TDataSet);
var
  i: Integer;
begin
  // Originalwerte in Liste abf�llen, damit diese in BeforePost verlichen werden k�nnen
  mCompareSettingsValues.Clear;
  for i:=0 to cCompareValuesCnt-1 do begin
    with tabOfflimitSettings.FieldByName(cCompareValues[i]) do
      mCompareSettingsValues.Values[cCompareValues[i]] := Text;
  end;
end;
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforePost(DataSet: TDataSet);
var
  xStr: String;
  xnLSt: Integer;
  //...............................................................
  function CheckMinMax(aMinField, aMaxField: TField; aLabelStr: String): Boolean;
  begin
    Result := (aMinField.Value < aMaxField.Value) or (aMaxField.Value = 0) or (aMaxField.IsNull);
    if not Result then
      ShowMessage(Format(rsMinMaxWrong, [aLabelStr]));
  end;
  //...............................................................
begin
  if not(DBFieldOk(tabOfflimitSettingsc_maoffset_name, gbOfflimitName.Caption, 0, 0, true, Self) AND
         DBFieldOk(tabOfflimitSettingsc_Average_time, laC_Average_time.Caption, 1, 1E5, true, Self)) then
    Sysutils.Abort;

  // f�r MEff und CYTot die Min/Max Eingaben pr�fen
  if not CheckMinMax(tabOfflimitSettingsc_Eff_min, tabOfflimitSettingsc_Eff_max, laMaEff.Caption) then
    SysUtils.Abort;
  if not CheckMinMax(tabOfflimitSettingsc_CYTot_min, tabOfflimitSettingsc_CYTot_max, laCYTot.Caption) then
    SysUtils.Abort;
  // tLSt sollte kleiner als Intervall�nge sein
  with tabOfflimitSettingsc_tLSt_max do begin
    if Value >= TMMSettingsReader.Instance.Value[cIntervalLen] then begin
      ShowMessage(Format(rsMsgMaxtLStError, [Integer(TMMSettingsReader.Instance.Value[cIntervalLen])]));
      SysUtils.Abort;
    end;
  end;
  // nLSt sollte kleiner als das Verh�ltnis von Intervall�nge und MIN(LongstopDef) sein
  with tabOfflimitSettingsc_LSt_max do begin
    if Value >= mMaxnLSt then begin
      ShowMessage(Format(rsMsgMaxLStError, [mMaxnLSt]));
      SysUtils.Abort;
    end;
  end;

  tabOfflimitSettingsc_ProdGrp_depend.Value := True;
  if tabOfflimitSettings.State = dsEdit then begin
    // Wenn Werte ge�ndert haben...
    if not CompareOfflimitSettings then begin
      // ...dann nachfragen, ob �bernehmen und die bestehenden Daten auf Hide gesetzt werden
      if MessageDlg(StringReplace(rsSettingsChanged, '~', cCRLF, [rfReplaceAll]), mtConfirmation, mbOKCancel, 0) = mrOK then begin
        with comUpdateHide do
        try
          Parameters.ParamByName('OfflimitID').Value := tabOfflimitSettingsc_maoffset_id.Value;
          Execute;
        except
          on e:Exception do begin
            WriteToEventLog('UpdateAsHide failed. ' + e.Message, 'MachineOfflimitSettings: ', etError);
            SysUtils.Abort;
          end;
        end;
      end else
        SysUtils.Abort;
    end;
  end;
end;                                    //procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforePost
//-----------------------------------------------------------------------------
function TOffLimitEditorDlg.CompareOfflimitSettings: Boolean;
var
  i: Integer;
begin
  Result := True;

  for i:=0 to cCompareValuesCnt-1 do begin
    with tabOfflimitSettings.FieldByName(cCompareValues[i]) do begin
      Result := Result and (mCompareSettingsValues.Values[cCompareValues[i]] = Text);
    end;
  end;
end;                                    //procedure TOffLimitEditorDlg.tabOfflimitSettingsBeforeDelete
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.acCopyExecute(Sender: TObject);
var
  xSelection: rListSelResultT;
  xQuery: TmmAdoDataSet;
  //..........................................................
  procedure SetField(aFieldname: string);
  begin
    if xQuery.FieldByName(aFieldname).IsNull then
      tabOfflimitSettings.FieldByName(aFieldname).Clear
    else
      tabOfflimitSettings.FieldByName(aFieldname).Value := xQuery.FieldByName(aFieldname).Value;
  end;                                  //procedure SetField
  //....................................................
begin
  xSelection := StandardSelect(rsCaptionStr1, 'c_maoffset_name', 'c_maoffset_id', 't_machine_offlimit_settings');
  if xSelection.ID < 0 then exit;

  SetRecordEditMode(tabOfflimitSettings);
  xQuery := TmmAdoDataSet.Create(nil);
  with xQuery do
  try
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT *';
    CommandText := CommandText + ' FROM t_machine_offlimit_settings';
    CommandText := CommandText + format(' WHERE c_maoffset_id = %d', [xSelection.ID]);
    Active := true;

    SetField('c_base');
    SetField('c_Average_time');
    SetField('c_ProdGrp_depend');
    SetField('c_OffBorderByAverage');
    SetField('c_OffBorderByFixValues');
    SetField('c_Eff_PC_max');
    SetField('c_Eff_PC_min');
    SetField('c_Eff_max');
    SetField('c_Eff_min');
    SetField('c_CYTot_PC_max');
    SetField('c_CYTot_PC_min');
    SetField('c_CYTot_max');
    SetField('c_CYTot_min');
    SetField('c_CSIRO_PC_max');
    SetField('c_CSIRO_max');
    SetField('c_CSp_PC_max');
    SetField('c_CSp_max');
    SetField('c_RSp_PC_max');
    SetField('c_RSp_max');
    SetField('c_YB_PC_max');
    SetField('c_YB_Max');
    SetField('c_CBu_PC_max');
    SetField('c_CBu_max');
    SetField('c_CUpY_PC_max');
    SetField('c_CUpY_max');
    SetField('c_tLSt_max');
    SetField('c_LSt_max');

  // SetField('c_tLSt_PC_max');
  // SetField('c_LSt_PC_max');
//    SetField('c_RedL_PC_max');
//    SetField('c_RedL_max');
  finally
    Free;
  end;// with TmmAdoDataSet.Create(nil)
end; //procedure TOffLimitEditorDlg.acCopyExecute
//-----------------------------------------------------------------------------
function TOffLimitEditorDlg.GetNumberOfMachines: integer;
const
  cQryNumberOfMachines = 'SELECT COUNT(*) AS c_number_of_machines FROM t_machine WHERE c_maoffset_id = :c_maoffset_id ';
begin
  Result := 0;
  with qryWork do
  try
    Active := False;
    CommandText := cQryNumberOfMachines;
    Parameters.ParamByName('c_maoffset_id').Value := tabOfflimitSettingsc_maoffset_id.asinteger;
    Active := True;
    Result := FieldByName('c_number_of_machines').AsInteger;
    Active := False;
  except
    on e:Exception do begin
      CodeSite.SendError('GetNumberOfMachines: ' + e.Message);
    end;
  end;                                  //with quCountMach do
end; //function TOffLimitEditorDlg.GetNumberOfMachines
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsCalcFields(DataSet: TDataSet);
begin
  tabOfflimitSettingsc_number_of_machines.Value := GetNumberOfMachines;
  // LenBase als Feld auch noch anpassen
  tabOfflimitSettingscalc_base.Value := cLenBaseTxt[tabOfflimitSettingsc_base.AsInteger].Text;
end; //procedure TOffLimitEditorDlg.tabOfflimitSettingsCalcFields
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.wwDBGrid1CalcCellColors(Sender: TObject; Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
  if gdFixed in State then ABrush.Color := clLightYellow
                      else ABrush.Color := wwDBGrid1.Color;

  if highlight then begin
    AFont.Color := clHighlightText;
    ABrush.Color := clHighlight;
  end;                                  //if highlight then
end; //procedure TOffLimitEditorDlg.wwDBGrid1CalcCellColors
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.PageControlChange(Sender: TObject);
var xBool : Boolean;
begin
  DBNavigator1.VisibleButtons := [nbFirst, nbPrior, nbNext, nbLast];
  xBool := TRUE;
  case PageControl.ActivePage.PageIndex of
    // tsFormular
    0: begin
        DBNavigator1.DataSource := srcOfflimitSettings;
        if not tabOfflimitSettings.ReadOnly then
          DBNavigator1.VisibleButtons := DBNavigator1.VisibleButtons + [nbInsert, nbDelete, nbPost, nbCancel];
      end;
    // tsGrid
    1: begin
         DBNavigator1.DataSource := srcOfflimitSettings;
         xBool := FALSE;
       end;
    // tsAssignment
    2: DBNavigator1.DataSource := srcMachine;
  else
  end;
  acPrint.Enabled := xBool;

  GetNavigatorWidth(DBNavigator1);
end;                                    //procedure TOffLimitEditorDlg.PageControl1Change
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.PageControlChanging(Sender: TObject; var AllowChange: Boolean);
begin
  AllowChange := PostRecord(tabOfflimitSettings) and PostRecord(tabMachine);
end;                                    //procedure TOffLimitEditorDlg.PageControl1Changing
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.bPrintClick(Sender: TObject);
var xBlackWhitePrint : Boolean;
begin
  if GetPrintOptions(poLandscape, Self, xBlackWhitePrint) then
    Print;
end;                                    //procedure TOffLimitEditorDlg.bPrintClick
//-----------------------------------------------------------------------------
procedure TOffLimitEditorDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.acPrintExecute(Sender: TObject);
var xBlackWhitePrint : Boolean;
begin
//  if GetPrintOptions(poLandscape, Self) then
  if GetPrintOptions(poPortrait, Self, xBlackWhitePrint) then
   // Print;
   PrintRapport;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabMachineAfterPost(DataSet: TDataSet);
begin
  tabOfflimitSettings.Refresh;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabMachineBeforeDelete(DataSet: TDataSet);
begin
  Abort;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.wwDBGrid1DblClick(Sender: TObject);
begin
  PageControl.ActivePageIndex := 0;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.FormShow(Sender: TObject);
begin
  wwDBGrid1.TitleColor    := cDefaultGridTitleColor;
//  dgMaOfflimit.FixedColor := cDefaultGridTitleColor;

  SetTranslations;
  with tabOfflimitSettings do
  try
    Active          := False;
    ReadOnly        := not acEditOfflimit.Enabled;
    Active          := True;
    IndexFieldNames := 'c_maoffset_name';
    First;
    if mID > 0 then
      tabOfflimitSettings.locate('c_maoffset_id',mID,[]);
  except
  end;

  with tabMachine do try
    Active          := False;
    ReadOnly        := not acEditOfflimit.Enabled;
    Active          := True;
  except
  end;
  PageControlChange(nil);
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.cobLenBaseChange(Sender: TObject);
begin
{
  laLowBorder.Caption  := Format(rsLowBorder, [cobLenBase.Text]);
  laHighBorder.Caption := Format(rsHighBorder, [cobLenBase.Text]);
{}
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.UpdateLenBaseLabels;
begin
  laLenBase.Caption    := cLenBaseTxt[tabOfflimitSettingsc_base.AsInteger].Text;
  laLowBorder.Caption  := Format(rsLowBorder,  [cLenBaseTxt[tabOfflimitSettingsc_base.AsInteger].Text]);
  laHighBorder.Caption := Format(rsHighBorder, [cLenBaseTxt[tabOfflimitSettingsc_base.AsInteger].Text]);
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.tabOfflimitSettingsAfterScroll(DataSet: TDataSet);
var
  i: Integer;
begin
{
  // Originalwerte in Liste abf�llen, damit diese in BeforePost verlichen werden k�nnen
  mCompareSettingsValues.Clear;
  for i:=0 to cCompareValuesCnt-1 do begin
    with tabOfflimitSettings.FieldByName(cCompareValues[i]) do
//      if not IsNull then
        mCompareSettingsValues.Values[cCompareValues[i]] := Text;
  end;
{}

  // Aktuallisiert einfach den Text per L�ngenbasis
  UpdateLenBaseLabels;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.bChangeLenBaseClick(Sender: TObject);
begin
  with bChangeLenBase do begin
    with ClientToScreen(Point(Parent.Left, Parent.Top)) do
      pmLenBase.Popup(X, Y + Height);
  end;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.pmLenBaseClick(Sender: TObject);
begin
  tabOfflimitSettings.Edit;
  tabOfflimitSettingsc_base.Value := TControl(Sender).Tag;

  laLenBase.Font.Color := clRed;
  UpdateLenBaseLabels;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.FormDestroy(Sender: TObject);
begin
  mCompareSettingsValues.Free;
end;
//------------------------------------------------------------------------------
procedure TOffLimitEditorDlg.PrintRapport;
var xText :string;
begin

  if PageControl.ActivePage = tsAssignment then
     with TfrmPrintList.Create(Self, (tabMachine as TDataSet), dgMaOfflimit, tsAssignment.Caption) do
     try
          SummaryBand1.Enabled := FALSE;
          Execute;
     finally
          Free;
     end  //try..finally
  else if PageControl.ActivePage = tsFormular then begin
     //xText := Format('%s %s',[rsPrintOfflimitSetTitle, tsFormular.Caption ]);
     with TOfflimitPrintFormular.Create(Self, tabOfflimitSettings, '') do try
         execute;
     finally
          Free;
     end  //try..finally
  end;

end;

procedure TOffLimitEditorDlg.tabMachinelookMaTypGetText(Sender: TField;
  var Text: String; DisplayText: Boolean);
begin
  Text := cMachineTypeArr[TMachineType(tabMachinec_machine_type.Value)].Name;
end;

end.

