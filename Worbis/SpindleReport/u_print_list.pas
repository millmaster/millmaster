(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_print_list.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.08.2001   0.00   PW  | file created
| 29.10.2002   0.00   Wss | Summary Band hinzugef�gt und TwwQRPrintDBGrid Komponenten erweitert
| 01.10.2003   0.00   Wss | Printbefehl wird nun �ber PrintGrid Komponente ausgef�hrt
| 02.10.2003   0.00   Wss | Loepfe Logo ge�ndert, Titel f�r Autosize angepasst
| 09.06.2004   0.00   SDO | Neue Fusszeile, Firmenname in Kopfzeile
| 21.03.2005   0.00   Wss | Anpassungen ob ein DBGrid oder wwDBGrid in Create() �bergeben wird
|=========================================================================================*)

{$i symbols.inc}

unit u_print_list;

interface

uses
  u_common_global, u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, QRPrintGrid, Qrctrls, mmQRSysData, QuickRpt, mmQRLabel, Printers,
  mmQRBand, ExtCtrls, mmQuickRep, IvDictio, IvMulti, IvEMulti, mmTranslator,
  wwQRPrintDBGrid, DB, wwDBGrid, mmQRImage, mmDBGrid, grids;

type
  TfrmPrintList = class(TmmForm)
    mmTranslator: TmmTranslator;
    QuickRep1: TmmQuickRep;
    TitleBand1: TmmQRBand;
    laTitle: TmmQRLabel;
    PageFooterBand1: TmmQRBand;
    qrbHeader: TmmQRBand;
    qrbDetail: TmmQRBand;
    SummaryBand1: TQRBand;
    mmQRImage1: TmmQRImage;
    qlPageValue: TmmQRSysData;
    mmQRSysData1: TmmQRSysData;
    qlCompanyName: TmmQRLabel;

    procedure FormCreate(Sender: TObject);
  private
    mDataSet: TDataSet;
    mPrintGrid: TBaseQRPrintGrid;
  public
    constructor Create(aOwner: TComponent; aDataset: TDataSet; aDataGrid: TCustomGrid; aTitle: string); reintroduce; virtual;
    procedure Execute;

  end; //TfrmPrintList

var
  frmPrintList: TfrmPrintList;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  u_main, u_lib, u_common_lib, mmLib, u_resourcestrings, SettingsReader, stdctrls;

//-----------------------------------------------------------------------------
constructor TfrmPrintList.Create(aOwner: TComponent; aDataset: TDataSet; aDataGrid: TCustomGrid; aTitle: string);
begin
  inherited Create(aOwner);
  if aDataGrid is TwwDBGrid then begin
    mPrintGrid := TwwQRPrintDBGrid.Create(Self);
    with TwwQRPrintDBGrid(mPrintGrid) do begin
      SummaryBand.Band    := SummaryBand1;
      SummaryBand.Margin  := 2;
      SummaryBand.VertPos := tlCenter;
    end;
  end
  else begin
    mPrintGrid := TQRPrintDBGrid.Create(Self);
  end;

  with mPrintGrid do begin
    QuickReport        := QuickRep1;
    ColDistance        := 10;
    DataMultiLine      := False;
    FitToWidth         := True;
    LeftMargin         := 2;
    DataGrid           := aDataGrid;
    DetailBand.Band    := qrbDetail;
    DetailBand.Margin  := 2;
    DetailBand.VertPos := tlCenter;
    HeaderBand.Band    := qrbHeader;
    HeaderBand.Margin  := 2;
    HeaderBand.VertPos := tlCenter;
  end;

  QuickRep1.DataSet   := aDataSet;
  mDataSet            := aDataset;
  laTitle.Caption     := aTitle;
end; //constructor TfrmPrintList.Create
//-----------------------------------------------------------------------------
procedure TfrmPrintList.Execute;
var
  xLastPos: TBookmark;
begin
  mDataSet.DisableControls;
  xLastPos := mDataSet.GetBookmark;
  mDataSet.First;
  try
    {$ifdef PrintPreview}
    mPrintGrid.Preview;
    {$else}
    mPrintGrid.Print;
    {$endif}
  finally
    mDataSet.GotoBookmark(xLastPos);
    mDataSet.FreeBookmark(xLastPos);
    mDataSet.EnableControls;
  end; //try..finally
end; //procedure TfrmPrintList.Execute
//-----------------------------------------------------------------------------
procedure TfrmPrintList.FormCreate(Sender: TObject);
begin
  inherited;
  QuickRep1.PrinterSettings.PrinterIndex := Printer.PrinterIndex;
  QuickRep1.Page.Orientation := Printer.Orientation;

  //laTitle.Left := (TitleBand1.Width -laTitle.Width) div 2;
  //sysDate.Left := TitleBand1.Width -sysDate.Width;
  //sysPage.Left := PageFooterBand1.Width -sysPage.Width;


  qlPageValue.Text := Translate( qlPageValue.Text ) + ' ';

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption :='';
  end;
end; //procedure TfrmPrintList.FormCreate



end. //u_print_list

