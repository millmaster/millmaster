(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_standard_select.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Select dialog for small tables
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.04.99 | 0.00 | PW  | Datei erstellt
| 18.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_standard_select;
interface
uses
 u_global,
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 BASEDIALOG,StdCtrls,ComCtrls,ToolWin,mmToolBar,Db,
 IvDictio,IvMulti,IvEMulti,ActnList,mmActionList,Nwcombo,mmButton,
 ExtCtrls,mmPanel,mmTranslator,Buttons, BASEFORM, ADODB, mmADODataSet;

type
 TStandardSelectDlg = class(TmmForm)
  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  mmTranslator1: TmmTranslator;
    quSelect: TmmADODataSet;

  lbNames: TNWListBox;
  paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure FormClose(Sender: TObject; var Action: TCloseAction);
  procedure acHelpExecute(Sender: TObject);
  private
  public
   procedure Init(aCaption,aDisplayfield,aIDField,aTablename: string);
   function GetSelection: rListSelResultT;
 end; //TSelectOffLimitDlg

var
 StandardSelectDlg: TStandardSelectDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf,
  u_common_global;

{$R *.DFM}

procedure TStandardSelectDlg.acOKExecute(Sender: TObject);
begin
 inherited;
 Close;
 ModalResult := mrOK;
end; //procedure TSelectOffLimitDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TStandardSelectDlg.acCancelExecute(Sender: TObject);
begin
 inherited;
 Close;
 ModalResult := mrCancel;
end; //procedure TSelectOffLimitDlg.acCancelExecute
//-----------------------------------------------------------------------------
function TStandardSelectDlg.GetSelection: rListSelResultT;
begin
 result.ID := lbNames.GetItemID;
 result.Text := lbNames.GetItemText;
end; //function TSelectOffLimitDlg.GetSelectedID
//-----------------------------------------------------------------------------
procedure TStandardSelectDlg.Init(aCaption,aDisplayfield,aIDField,aTablename: string);
begin
 Caption := aCaption;

 with quSelect do
 begin
  try
   CommandText := format('SELECT %s,%s',[aDisplayfield,aIDField]);
   CommandText := CommandText + format(' FROM %s',[aTablename]);
   CommandText := CommandText + format(' ORDER BY %s',[aDisplayfield]);
   CodeSite.SendString('TStandardSelectDlg Query', CommandText);
   Open;

   while not Eof do
   begin
    lbNames.AddObj(FieldByName(aDisplayfield).asstring,FieldByName(aIDField).asinteger);
    Next;
   end; //while not Eof do
  finally
   acOK.Enabled := lbNames.Items.Count > 0;
   if acOK.Enabled then lbNames.ItemIndex := 0;
   Active := false;
  end; //try..finally
 end; //with quSelect do
end; //procedure TSelectOffLimitDlg.Init
//-----------------------------------------------------------------------------
procedure TStandardSelectDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 inherited;
 Action := caFree;
end; //procedure TStandardSelectDlg.FormClose
//-----------------------------------------------------------------------------
procedure TStandardSelectDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, Self.HelpContext);
end; //procedure TStandardSelectDlg.acHelpExecute

end. //u_standard_select.pas
