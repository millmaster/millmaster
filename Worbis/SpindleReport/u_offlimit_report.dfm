inherited OffReportDlg: TOffReportDlg
  Left = 325
  Top = 249
  Width = 792
  Height = 626
  BorderIcons = [biSystemMenu]
  Caption = '(100)Maschinen-Offlimit Bericht'
  ParentFont = False
  Font.Color = clBlack
  Icon.Data = {
    0000010001002020100000000000E80200001600000028000000200000004000
    0000010004000000000080020000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    0000000000000000000000000000000000000000000000000000000000000000
    000000BBBB00000000000BBBB0000000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000000000BBBB0E00000
    000000BBBB0E000000000BBBB0E00000000000BBBB0E000B00000BBBB0E00000
    000000BBBB00000000000BBBB0E00BBBB00000BBBB00BBBB00000BBBB0E00BBB
    B0E000BBBB00BBBB0E000BBBB0E00BBBB0E000BBBB00BBBB0E0B0BBBB0E00BBB
    B0E000BBBBB0BBBB0E0BBBBBB0E00BBBB0E0000BBBB0BBBB0E0BBBBBB0E00BBB
    B0E000B0BBB0BBBB0E0BBBBBB0E00BBBB0E00BBB0BB0BBBB0E0BBBBBB0E00BBB
    B0E0BBBBB0B0BBBB0E00BBBBB0E00BBBB00BBBBBBB00BBBB0E000BBBB0E00BBB
    B0BBBBBBBBB0BBBB0E00000000E00BBBBBBBBB0BBBBBBBBB0E00000EEE000BBB
    BBBBB0E0BBBBBBBB0E00000000000BBBBBBB0EEE0BBBBBBB0E00000000000BBB
    BBB0EEE000BBBBBB0E00000000000BBBBB0EEE00000BBBBB0E00000000000BBB
    B0EEE0000000BBBB0E000000000000000EEE0000000000000E000000000000EE
    E0E00000000000EEE0000000000000000000000000000000000000000000FFFF
    FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF81FF03FF80FF01FF807F00FF80
    7F00FF807F00FF806F00FF804700038007000180030000800000008000000080
    0000008000000080000000000000000000000000010000000180000001C00000
    01E0000001FF000801FF001C01FF003E01FF007F01FF80FF81FFC1FFC1FF}
  Position = poScreenCenter
  OnClose = FormClose
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 26
    Width = 784
    Height = 573
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 0
    object PageControl1: TmmPageControl
      Left = 2
      Top = 2
      Width = 780
      Height = 569
      ActivePage = tsStatistic
      Align = alClient
      HotTrack = True
      TabOrder = 0
      OnChange = PageControl1Change
      object tsOnline: TTabSheet
        Caption = '(40)Online'
        object wwDBGrid1: TwwDBGrid
          Left = 0
          Top = 25
          Width = 772
          Height = 340
          ControlType.Strings = (
            'calc_OutOfProduction;ImageIndex;Shrink To Fit')
          Selected.Strings = (
            'c_machine_name'#9'10'#9'c_machine_name'
            'c_spindle_id'#9'5'#9'c_spindle_id'
            'c_update_time'#9'5'#9'c_update_time'
            'c_max_online_offlimit_time'#9'5'#9'c_max_online_offlimit_time'
            'c_Eff'#9'5'#9'c_Eff'
            'c_tLSt'#9'5'#9'c_tLSt'
            'c_LSt'#9'5'#9'c_LSt'
            'c_CYTot'#9'10'#9'c_CYTot'
            'c_CSIRO'#9'10'#9'c_CSIRO'
            'c_CSp'#9'10'#9'c_CSp'
            'c_RSp'#9'10'#9'c_RSp'
            'c_CBu'#9'10'#9'c_CBu'
            'c_CUpY'#9'10'#9'c_CUpY'
            'c_YB'#9'10'#9'c_YB'
            'calc_OutOfProduction'#9'10'#9'calc_OutOfProduction'
            'calc_Offcause1'#9'20'#9'calc_Offcause1'
            'calc_Offcause2'#9'20'#9'calc_Offcause2'
            'calc_Offcause3'#9'20'#9'calc_Offcause3')
          IniAttributes.Delimiter = ';;'
          TitleColor = clBtnFace
          FixedCols = 0
          ShowHorzScrollBar = True
          EditControlOptions = [ecoSearchOwnerForm, ecoDisableDateTimePicker]
          Align = alClient
          Ctl3D = True
          DataSource = srcOnline
          KeyOptions = []
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis]
          ParentCtl3D = False
          PopupMenu = pmProfile
          ReadOnly = True
          TabOrder = 0
          TitleAlignment = taCenter
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clBlack
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          TitleLines = 2
          TitleButtons = True
          OnCalcCellColors = wwDBGrid1CalcCellColors
          OnColWidthChanged = wwDBGrid1ColWidthChanged
          OnTitleButtonClick = wwDBGrid1TitleButtonClick
          OnDblClick = wwDBGrid1DblClick
          OnColumnMoved = wwDBGrid1ColumnMoved
          ImageList = ilCross
        end
        object Panel1: TmmPanel
          Left = 0
          Top = 0
          Width = 772
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object mmLabel1: TmmLabel
            Left = 5
            Top = 5
            Width = 140
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Anzeigeunterdrueckung:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laMinOnlineDuration: TmmLabel
            Left = 150
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel2: TmmLabel
            Left = 205
            Top = 5
            Width = 120
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(25)Anzeige-Status:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laSpindleStatus: TmmLabel
            Left = 330
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel6: TmmLabel
            Left = 525
            Top = 5
            Width = 120
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(25)Anzahl Spulstellen:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laCountOnline: TmmLabel
            Left = 650
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object paOnlineInfo: TmmPanel
          Left = 0
          Top = 365
          Width = 772
          Height = 176
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object tabsOnline: TTabSet
            Left = 0
            Top = 155
            Width = 772
            Height = 21
            Align = alBottom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Tabs.Strings = (
              '(40)Offlimit-Tabelle'
              '(40)Offlimit-Verteilung'
              '(40)Ursachen-Analyse')
            TabIndex = 0
            OnClick = tabsOnlineClick
          end
          object nbOnline: TNotebook
            Left = 0
            Top = 0
            Width = 772
            Height = 155
            Align = alClient
            TabOrder = 1
            object TPage
              Left = 0
              Top = 0
              Caption = '(40)Offlimit-Tabelle'
              object grOfflimits: TwwDBGrid
                Left = 0
                Top = 0
                Width = 772
                Height = 155
                IniAttributes.Delimiter = ';;'
                TitleColor = clBtnFace
                FixedCols = 0
                ShowHorzScrollBar = True
                Align = alClient
                DataSource = dsOfflimits
                KeyOptions = []
                Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis, dgShowCellHint]
                ReadOnly = True
                TabOrder = 0
                TitleAlignment = taCenter
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clBlack
                TitleFont.Height = -11
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                TitleLines = 1
                TitleButtons = False
                OnCalcCellColors = grOfflimitsCalcCellColors
              end
            end
            object TPage
              Left = 0
              Top = 0
              Caption = '(40)Offlimit-Verteilung'
              object Chart1: TmmChart
                Left = 0
                Top = 0
                Width = 772
                Height = 155
                AllowPanning = pmNone
                BackWall.Brush.Color = clWhite
                BackWall.Brush.Style = bsClear
                BackWall.Color = 14869218
                LeftWall.Color = 12171705
                MarginBottom = 2
                MarginLeft = 2
                MarginRight = 2
                MarginTop = 5
                Title.Text.Strings = (
                  'TmmChart')
                Title.Visible = False
                BackColor = 14869218
                BottomAxis.Axis.Width = 1
                BottomAxis.Grid.Visible = False
                Chart3DPercent = 10
                LeftAxis.Axis.Width = 1
                LeftAxis.LabelsSize = 35
                LeftAxis.Title.Caption = '(*)(in Stunden)'
                LeftAxis.TitleSize = 3
                Legend.ShadowSize = 0
                Legend.TopPos = 6
                Legend.Visible = False
                MaxPointsPerPage = 11
                View3D = False
                View3DOptions.Elevation = 315
                View3DOptions.Rotation = 360
                Align = alClient
                BevelOuter = bvNone
                BorderStyle = bsSingle
                Color = clSilver
                TabOrder = 0
                MouseMode = mmNormal
                object Series1: TBarSeries
                  Marks.Arrow.Color = clBlack
                  Marks.Arrow.Visible = False
                  Marks.ArrowLength = 15
                  Marks.BackColor = 12910591
                  Marks.Style = smsPercent
                  Marks.Visible = True
                  PercentFormat = '##0.# %'
                  SeriesColor = 35840
                  BarWidthPercent = 45
                  Dark3D = False
                  XValues.DateTime = False
                  XValues.Name = 'X'
                  XValues.Multiplier = 1
                  XValues.Order = loAscending
                  YValues.DateTime = False
                  YValues.Name = 'Balken'
                  YValues.Multiplier = 1
                  YValues.Order = loNone
                end
              end
            end
            object TPage
              Left = 0
              Top = 0
              Caption = '(40)Ursachen-Analyse'
              object sgOffCause: TStringGrid
                Left = 0
                Top = 0
                Width = 772
                Height = 155
                Align = alClient
                ColCount = 3
                Ctl3D = True
                DefaultColWidth = 180
                DefaultRowHeight = 18
                DefaultDrawing = False
                RowCount = 7
                Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goDrawFocusSelected, goThumbTracking]
                ParentCtl3D = False
                TabOrder = 0
                OnDrawCell = sgOffCauseDrawCell
                ColWidths = (
                  180
                  92
                  180)
                RowHeights = (
                  18
                  18
                  18
                  18
                  18
                  18
                  18)
              end
            end
          end
        end
      end
      object tsStatistic: TTabSheet
        Caption = '(40)Statistik'
        ImageIndex = 1
        object Panel2: TmmPanel
          Left = 0
          Top = 0
          Width = 772
          Height = 25
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object mmLabel4: TmmLabel
            Left = 345
            Top = 5
            Width = 120
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(25)Mindest-Offlimit Zeit:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laMinOfflineDuration: TmmLabel
            Left = 470
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel3: TmmLabel
            Left = 145
            Top = 5
            Width = 140
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Offlimit-Zeit / Laufzeit (%):'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laOfflineRatio: TmmLabel
            Left = 290
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel5: TmmLabel
            Left = 525
            Top = 5
            Width = 120
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(25)Anzahl Spulstellen:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laCountOffline: TmmLabel
            Left = 650
            Top = 5
            Width = 6
            Height = 13
            Caption = '0'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmToolBar1: TmmToolBar
            Left = 0
            Top = 0
            Width = 135
            Height = 25
            Align = alLeft
            EdgeBorders = []
            Images = frmMain.ImageList16x16
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            Wrapable = False
            object ToolButton5: TToolButton
              Left = 0
              Top = 2
              Action = acToggleGridChart
            end
            object bSort: TToolButton
              Left = 23
              Top = 2
              Action = acSort
              DropdownMenu = pmStatistic
              Style = tbsDropDown
            end
            object ToolButton9: TToolButton
              Left = 59
              Top = 2
              Width = 8
              ImageIndex = 64
              Style = tbsSeparator
              Visible = False
            end
            object tbPie: TToolButton
              Left = 67
              Top = 2
              Action = acShowPie
              AllowAllUp = True
              Style = tbsCheck
            end
            object ToolButton6: TToolButton
              Left = 90
              Top = 2
              Action = acResetSpindle
              DropdownMenu = pmReset
              Style = tbsDropDown
            end
          end
        end
        object pnStatisticGrid: TmmPanel
          Left = 0
          Top = 25
          Width = 772
          Height = 200
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 1
          Visible = False
          object wwDBGrid2: TwwDBGrid
            Left = 2
            Top = 2
            Width = 768
            Height = 196
            Selected.Strings = (
              'c_machine_name'#9'10'#9'c_machine_name'#9#9
              'c_spindle_id'#9'5'#9'c_spindle_id'#9#9
              'calc_OfflineRatio'#9'5'#9'calc_OfflineRatio'#9#9
              'c_offline_offlimit_time'#9'5'#9'c_offline_offlimit_time'#9#9
              'c_offline_watchtime'#9'5'#9'c_offline_watchtime'#9#9
              'c_YB_offline_time'#9'5'#9'c_YB_offline_time'#9#9
              'c_RedL_offline_time'#9'5'#9'c_RedL_offline_time'#9#9
              'c_Eff_offline_time'#9'5'#9'c_Eff_offline_time'#9#9
              'c_CSp_offline_time'#9'5'#9'c_CSp_offline_time'#9#9
              'c_RSp_offline_time'#9'5'#9'c_RSp_offline_time'#9#9
              'c_CBu_offline_time'#9'5'#9'c_CBu_offline_time'#9#9
              'c_CUpY_offline_time'#9'5'#9'c_CUpY_offline_time'#9#9
              'c_tLSt_offline_time'#9'5'#9'c_tLSt_offline_time'#9#9
              'c_LSt_offline_time'#9'5'#9'c_LSt_offline_time'#9#9
              'c_CSIRO_offline_time'#9'5'#9'c_CSIRO_offline_time'#9#9
              'c_CYTot_offline_time'#9'5'#9'c_CYTot_offline_time'#9#9
              
                'c_OutOfProduction_offline_time'#9'5'#9'c_OutOfProduction_offline_time'#9 +
                #9)
            IniAttributes.Delimiter = ';;'
            TitleColor = clBtnFace
            OnRowChanged = wwDBGrid2RowChanged
            FixedCols = 0
            ShowHorzScrollBar = True
            EditControlOptions = [ecoSearchOwnerForm, ecoDisableDateTimePicker]
            Align = alClient
            Ctl3D = True
            DataSource = scrOffline
            KeyOptions = []
            Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgTrailingEllipsis]
            ParentCtl3D = False
            PopupMenu = pmProfile
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clBlack
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            TitleLines = 2
            TitleButtons = True
            OnCalcCellColors = wwDBGrid1CalcCellColors
            OnTitleButtonClick = wwDBGrid1TitleButtonClick
            OnDblClick = wwDBGrid2DblClick
          end
        end
        object pnStatisticChart: TmmPanel
          Left = 0
          Top = 264
          Width = 772
          Height = 272
          BevelOuter = bvNone
          BorderWidth = 2
          BorderStyle = bsSingle
          TabOrder = 2
          Visible = False
          object mmGraph1: TmmGraph
            Left = 2
            Top = 2
            Width = 764
            Height = 264
            Align = alClient
            Bars.DefaultBarWidth = 12
            Bars.Gap = 2
            Bars.MinBarWidth = 12
            ChartArea.BackgroundColor = 15066597
            Colors.PanelBackground = clBtnFace
            HorzGrid.Color = clSilver
            HorzGrid.GridType = hgLeftRightAxis
            Margin.Bottom = 5
            Margin.Graph_Legend = 10
            Margin.Left = 5
            Margin.Right = 5
            Margin.Title_Graph = 10
            Margin.Top = 5
            MemoINI.AdoConnectionString = 
              'Provider=SQLOLEDB.1;Data Source=wetsrvmm;Initial Catalog=MM_Wind' +
              'ing;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
            LeftYAxis.BackgroundColor = clBtnFace
            LeftYAxis.BorderStyle = bsNone
            LeftYAxis.Decimals = 1
            LeftYAxis.DefaultMaxY = 100
            LeftYAxis.Font.Charset = DEFAULT_CHARSET
            LeftYAxis.Font.Color = clBlack
            LeftYAxis.Font.Height = -11
            LeftYAxis.Font.Name = 'Arial'
            LeftYAxis.Font.Style = []
            LeftYAxis.Title = '(*)Linien Werte'
            LeftYAxis.TitleMargin = 1
            LeftYAxis.Width = 70
            LeftYAxis.Visible = True
            Legend.BackgroundColor = 14737632
            Legend.BorderStyle = bsSingle
            Legend.Font.Charset = DEFAULT_CHARSET
            Legend.Font.Color = clBlack
            Legend.Font.Height = -11
            Legend.Font.Name = 'Arial'
            Legend.Font.Style = []
            Legend.Position = lpTop
            Legend.Visible = True
            OnGraphDblClick = mmGraph1GraphDblClick
            OnGraphClick = mmGraph11GraphClick
            PopupMenu = pmProfile
            ReadOnly = False
            RightYAxis.BackgroundColor = clBtnFace
            RightYAxis.BorderStyle = bsNone
            RightYAxis.Decimals = 0
            RightYAxis.DefaultMaxY = 10
            RightYAxis.Font.Charset = DEFAULT_CHARSET
            RightYAxis.Font.Color = clBlack
            RightYAxis.Font.Height = -11
            RightYAxis.Font.Name = 'Arial'
            RightYAxis.Font.Style = []
            RightYAxis.Title = '(*)Balken Werte'
            RightYAxis.TitleMargin = 1
            RightYAxis.Visible = True
            RightYAxis.Width = 70
            Selection.Enabled = False
            Selection.Shift = [ssShift]
            Selection.Button = mbLeft
            Titles.Alignment = taCenter
            Titles.BackgroundColor = 14737632
            Titles.BorderStyle = bsSingle
            Titles.Font.Charset = DEFAULT_CHARSET
            Titles.Font.Color = clBlack
            Titles.Font.Height = -11
            Titles.Font.Name = 'Arial'
            Titles.Font.Style = []
            Titles.SubTitleFont.Charset = DEFAULT_CHARSET
            Titles.SubTitleFont.Color = clBlack
            Titles.SubTitleFont.Height = -11
            Titles.SubTitleFont.Name = 'Arial'
            Titles.SubTitleFont.Style = []
            Titles.Visible = False
            VertGrid.Color = clSilver
            XAxis.BackgroundColor = clBtnFace
            XAxis.BorderStyle = bsNone
            XAxis.Font.Charset = DEFAULT_CHARSET
            XAxis.Font.Color = clBlack
            XAxis.Font.Height = -11
            XAxis.Font.Name = 'Arial'
            XAxis.Font.Style = []
            XAxis.Height = 80
            XAxis.XAxisTextAngle = 90
            XAxis.XValueFormat = '0'
            PageHeader.Title = '(100)Maschinen-Offlimit Bericht'
            PageHeader.PrintHeader = True
            PageFooter.Logo.Data = {
              07544269746D61709A930000424D9A9300000000000036040000280000008302
              0000390000000100080000000000648F00000000000000000000000100000001
              0000000000000101010002020200030303000404040005050500060606000707
              070008080800090909000A0A0A000B0B0B000C0C0C000D0D0D000E0E0E000F0F
              0F00101010001111110012121200131313001414140015151500161616001717
              170018181800191919001A1A1A001B1B1B001C1C1C001D1D1D001E1E1E001F1F
              1F00202020002121210022222200232323002424240025252500262626002727
              270028282800292929002A2A2A002B2B2B002C2C2C002D2D2D002E2E2E002F2F
              2F00303030003131310032323200333333003434340035353500363636003737
              370038383800393939003A3A3A003B3B3B003C3C3C003D3D3D003E3E3E003F3F
              3F00404040004141410042424200434343004444440045454500464646004747
              470048484800494949004A4A4A004B4B4B004C4C4C004D4D4D004E4E4E004F4F
              4F00505050005151510052525200535353005454540055555500565656005757
              570058585800595959005A5A5A005B5B5B005C5C5C005D5D5D005E5E5E005F5F
              5F00606060006161610062626200636363006464640065656500666666006767
              670068686800696969006A6A6A006B6B6B006C6C6C006D6D6D006E6E6E006F6F
              6F00707070007171710072727200737373007474740075757500767676007777
              770078787800797979007A7A7A007B7B7B007C7C7C007D7D7D007E7E7E007F7F
              7F00808080008181810082828200838383008484840085858500868686008787
              870088888800898989008A8A8A008B8B8B008C8C8C008D8D8D008E8E8E008F8F
              8F00909090009191910092929200939393009494940095959500969696009797
              970098989800999999009A9A9A009B9B9B009C9C9C009D9D9D009E9E9E009F9F
              9F00A0A0A000A1A1A100A2A2A200A3A3A300A4A4A400A5A5A500A6A6A600A7A7
              A700A8A8A800A9A9A900AAAAAA00ABABAB00ACACAC00ADADAD00AEAEAE00AFAF
              AF00B0B0B000B1B1B100B2B2B200B3B3B300B4B4B400B5B5B500B6B6B600B7B7
              B700B8B8B800B9B9B900BABABA00BBBBBB00BCBCBC00BDBDBD00BEBEBE00BFBF
              BF00C0C0C000C1C1C100C2C2C200C3C3C300C4C4C400C5C5C500C6C6C600C7C7
              C700C8C8C800C9C9C900CACACA00CBCBCB00CCCCCC00CDCDCD00CECECE00CFCF
              CF00D0D0D000D1D1D100D2D2D200D3D3D300D4D4D400D5D5D500D6D6D600D7D7
              D700D8D8D800D9D9D900DADADA00DBDBDB00DCDCDC00DDDDDD00DEDEDE00DFDF
              DF00E0E0E000E1E1E100E2E2E200E3E3E300E4E4E400E5E5E500E6E6E600E7E7
              E700E8E8E800E9E9E900EAEAEA00EBEBEB00ECECEC00EDEDED00EEEEEE00EFEF
              EF00F0F0F000F1F1F100F2F2F200F3F3F300F4F4F400F5F5F500F6F6F600F7F7
              F700F8F8F800F9F9F900FAFAFA00FBFBFB00FCFCFC00FDFDFD00FEFEFE00FFFF
              FF005A4DD0FF624DBDFF8B4CB1C24B85FFFFFFC94D62FDC94D1B194ABBFFFFFF
              BD4C6EFBFFFF874D49464652FEA24C94FFDF4E57F5F97F30152E82F8FFFFFFFF
              C64D64FFD04D75FEFFA84D4CA6FFFFFFFFFFF8644DC6FFFFD04D4C4646469282
              4CB3FFFF8F4C74FBFFB34C77FEFFFF814CB8FF6E4D4A49489CC24D4B464646A3
              FFFFFFFFFC904530474C4572FFEE7029163CA1FFF85D54F2FFFFF45A4BCAB04C
              4B49496EDE5253EDFFFF944C9FFFFFFFFFF75E4DD5FFFFFFFFFFFFFFAE431729
              75F8FFFFB544182465E7FFFD754DDAFFE9544D65F8FFFF8C4CA6FFFFF5554DDB
              FF8F4CA0FFF98130163590FEFFD54E4C49494CD9FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF000900BCE200006CFF540085D0001FFFFFFF89005BFC2300365000
              0BD0FFFF9C0026F9FFFF4C0009181726FE730060FFBC0017FE99000859200085
              FFFFFFFFAA0018FFB80032FFFF3F00007BFFFFFFFFFFF51800AAFFFFB8000016
              18157FA3002EFCF0150093FFFF8D0034FDFFFF420095FF270000000075A40000
              1818158CFFFFFFFF8100043700002C4FFB4E0024580400A5FE3A00BAFFFFD300
              00EB820000000035CE0000E4FFFF60006FFFFFFFFFF30F00C0FFFFFFFFFFFFC0
              0400511F007FFFC90800512D0048FEFE3200C8FF8D000019F5FFFF53007BFFFF
              EF0200CBFF380089FF850012590A0093FFC10000000004CAFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF001300C6B500003FFF5C0084FC1A027D90902F00AACE00
              00E3FF460093FFFFA0002FF9FFFF52008CF5F5F6FF750066FFBE002CFC480066
              FFBC002AF9FFFFFFAD0021FFBB003AFFD809000080FFFFFFFFFFF52100ADFFFF
              BB0022ECF5F5F9FC3100C3960028F2FFFF92003CFDFFFF4A0099FF2F008DE1DF
              EF9E003DF4F5F4FAFFFFFFF926009EFE7F0388F8D60005D2FF81003CFF810046
              91915D0037FF82004FE1E0E7D60409E5FFFF660075FFFFFFFFF31900C3FFFFFF
              FFFFFF72005CFFA9002CFF7C0054FFDB0700D4FF3A00CDFF3D000022F5FFFF5A
              0080FFFFF00C00CDFF3C00A3FC1A00A5FF930037FFC50016D6E0E1F9FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF001300C785000C11F15F0080FF650000000000
              05D9E28B94FFEF2C0090FFFFA0002FF9FFFF520097FFFFFFFF75006CFFB6002A
              FAAC89CEFF930026F8FFFFFFAD0021FFBB003BFF79000C0080FFFFFFFFFFF521
              00ADFFFFBB0024FEFFFFFFFF9F00462C009EFFFFFF92003CFDFFFF4A0099FF2F
              00A4FFFFFF9D0042FFFFFFFFFFFFFFED0B02D6FFC50026FFBA000DF4FFB0001F
              FDCE0400000000006BFF80005CFFFFFFD70409E5FFFF660075FFFFFFFFF61900
              C5FFFFFFFFFFFF54008EFFD33C59FC500080FFFB2800A6FF3A00D4D906140421
              F5FFFF5A0080FFFFF00C00D9FF2D00AAE80207E3FFC3001BFEC7001AF9FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF001300B63F167000B1680080FFA400
              38C5800022F6FFFFFFB42F0004CCFFFFA0002FF9FFFF520089F0F0F5FF750041
              9639086AFEFFFFF16D05007CFFFFFFFFAD0021FFBB003EF92C0D4F0080FFFFFF
              FFFFF52100ADFFFFBB0021E7F0F0FCFFFA3200002DF2FFFFFF92003CFDFFFF4A
              0099FF2F00A0FFFFFF9D003CEFF0F0FEFFFFFFE20707E0FFD80407F3BD000DF0
              FFAD0021F8FD2404B3B40800AFFF80005AFFFFFFD70409E5FFFF660075FFFFFF
              FFC2050096FFFFFFFFFFFF4A00A0FFFFFDFDFD3D0090FFFD3C0097FF3A00D77A
              006D101FF5FFFF5A0080FFFFF00E008276071FE5D2000CF0FFCA000DFAC70019
              F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF001302980B40B900716700
              80FFD70127FF880056FFFFF465000013A5FFFFFFA0002FF9FFFF530000000059
              FF760000000048F1FFFFB726000060F1FFFFFFFFAD0021FFBB0047B700617F00
              80FFFFFFFFFFF52100ADFFFFBB0000000002C9FFFF98000091FFFFFFFF92003C
              FDFFFF4A0099FF2F00A0FFFFFF9D000000000DF0FFFFFFDF0707E0FFDA0507EA
              BE000DF0FFAD0021F6FF5B00C5D70115EFFF80005AFFFFFFD70409E5FFFF6600
              75FFFFFFFF6000002BF5FFFFFFFFFF4900A1FFFFFFFFFD3B0093FFFD3D0094FF
              3A00C52D0AB7091FF5FFFF5A0080FFFFF00F00000013AEFFC9000CF0FFCA000B
              F9C70019F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0013086C0068F1
              083F5A0080FFFC3203D7450091FFFE65000065DFFFFFFFFFA0002FF9FFFF5300
              467B7BAAFF76004BB84C057FFFCE0C0027A8FFFFFFFFFFFFAD0021FFBB00455A
              00BE860080FFFFFFFFFFF52100ADFFFFBB0011777B7DE4FFFF6D000042F9FFFF
              FF92003CFDFFFF4A0099FF2F00A0FFFFFF9D001F7B7B83F7FFFFFFE00707E1FF
              DA0507EBBE000DF0FFAD0021F5FF900087A50046FFFF80005AFFFFFFD70409E5
              FFFF660075FFFFFFEB16062000B3FFFFFFFFFF4900A1FFFFFFFFFD3B0092FFFD
              3C0095FF3A067E0161E3031FF5FFFF5A0080FFFFF00D009A95032BF1CF000CF1
              FFCA000CF9C70019F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00130C
              3B0098FF440E430080FFFF81007A1707DCFFED0E0095FFE2C8EAFFFFA20030FD
              FFFF52009EFFFFFFFF75006BFFB6002FFF810034E7FFCBD5FEFFFFFFAD0021FF
              BB00261219F97D0080FFFFFFFFFFF92200B0FFFFBB0026FFFFFFFFFFD70D0926
              00AFFFFFFF94003DFFFFFF4A0099FF2F00A0FFFFFF9D0045FFFFFFFFFFFFFFEA
              0903D6FFC80009F5BD000DF0FFAD0021F5FFC6005462007EFFFF80005AFFFFFF
              D70409E5FFFF680077FFFFFF8E0045A3004CFEFFFFFFFF53008FFFCF6F94FF4E
              0081FFFB2B00A5FF3C042100BBE6021FF5FFFF5B0082FFFFF00C00D9FF2600B3
              E60307E5FFC4001BFEC70019F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF0016020707D9FF88000A0080FFFFB9001C0432FDFFEC0902D8FF3E01A0F7F3
              99002DEDF3FC530090FBFBFCFF750065FA91002EFF7D005FFFB1043FFEFFFFFF
              AD0021FFBB00000079FF780080FFFFFFFBF3EA1F00A5F3F7BB0022F2FBFBFCFF
              620060BC0035F8F6F38B0039F1F3FC4B0099FF2F00A0FFFFFF9D003FFAFBFBFE
              FFFFFFF62000AEFF8F002DFFBB000DF0FFAD0021F5FFF31D141303C8FFFF8000
              5AFFFFFFD70409E4F4F3610070F3F4FD380098FE2001CEFFFFFFFF6F0061FF94
              003AFF780058FFDF0800D0FF3D000022F7DD021FF4F4F356007AF3F4EF0C00CD
              E81600ADFB1600AAFF990035FFC60019F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF000C000023FAFFB60000007AFFFFEA0D000065FFFFFC4600325300
              00CB55110600001011BB5600132A2A50FF71000B2900006FFFB800055724007B
              FFFFFFFFA90017FFB8000000CAFF72007AFFFFFFAB100F0000081062BA000028
              2A2A9CDC0100BCFF2F00A04E110500001013C14C0095FF26009CFFFFFF990002
              2A2A2ABAFFFFFFFF74001B5A0F0092FFB50002EFFFA90017F5FFFF6700001FF8
              FFFF7A0053FFFFFFD50002D5251100000211269B0000D9FF680066FEFFFFFFBB
              0001541E0087FFC4010056330040FDFF34000080FFD7001BDC20110000031122
              DA090021170018E8FF7C00175A11008BFFC1000FF2FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF0058484881FFFFE84F4848A4FFFFFF7E4848BAFFFFFFDF
              6B1C0339A9FF73424448474242C98C48433F3F61FF9E48443F4985F1FFFEA33D
              061873EDFFFFFFFFC4485FFFCE484869FCFF9E48A4FFFFFFBC42424748444282
              D049463F3F3FA7A54561F8FFA547786B424448464243CE8548B6FF6948BBFFFF
              FFB848453F3F3FC1FFFFFFFFF58326012C8DF8FFCC4851F4FFC4485FF8FFFFBC
              484884FFFFFFA44888FFFFFFE24B51E0514245484542527A4674FEFFCF4958F1
              FFFFFFFFA83B052080F6FFFFAF40061766DFFFFE73484AD6FFE44962E44E4246
              4845424FE35748404162C4FFFFF38125012D8EFBFFD44859F6FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFF6F0FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFEF1F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFCFFFFFFFFFFFFFBFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FAFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDFFFFFFFF
              FFFFFDFDFFFFFFFFFFFDF0F6FFFFFFFFFFFFF1F5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8F0FAFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF00B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7
              B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B6E8FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9E0E1E1E1E1E1DFF1FFFFFF
              FFFFFFF8E2E1E1E1E1E1E0E6FFFFFFFFFFFFFFF5E0E1E1E1E1E1E0E6FEFFFFFF
              FFFFF1DFE1E1E1E1E1E0E3FCFFFFFFFFFFFEE6E0E1E1E1E1E1E1E1E1E1E1E1E1
              E1E1E1E1E1E0E4FEFFFFF4E0E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1DFEB
              FFFFFEC7B5B7B7B7B7B7B4D9FFFFFFFFFFFFEAC1C1C1C1C1C1C0C9FDFFFFFFFF
              FFFFE0BFC1C1C1C1C1C0CCFDFFFFDCBDC1C1C1C1C1C1C8FCFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFE7C0C1C1C1C1C1C1BFDAFFFFFFFFFFFFFFFFE69C6848352B2B2D
              37445D7CB1EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD6BFC1C1C1C1C1C0
              CBFDFFFFFFFFFFFFFFFFFFFFFFF2C3C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1C1
              C1C1C1C0E8FFFFFFFFDFBFC1C1C1C1C1C1C9FBFFFFFFFFFFFFFFEFC2C1C1C1C1
              C1C1C2F3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF007272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727272727272727272727272
              7272727272727272727272727272727272727272727170D2FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D7C808080808079
              C2FFFFFFFFFFFFD47D7F808080807E89F1FFFFFFFFFFFFD37D7F808080807C96
              FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C8080808080808080
              8080808080808080807D8CF9FFFFCE7C80808080808080808080808080808080
              808079ABFFFFFF54000000000000005EFFFFFFFFFFFF9E0001010101010007CB
              FFFFFFFFFFFF9B0001010101010015E9FFFFBE0501010101010005C0FFFFFFFF
              FFFFFFFFFFFFFFFFFFFF8E000101010101010074FFFFFFFFFFFFE86B17000000
              0000000000000000001958BDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6C000101
              0101010013E4FFFFFFFFFFFFFFFFFFFFFFDD0E00010101010101010101010101
              010101010101010085FFFFFFFF93000101010101000CD8FFFFFFFFFFFFFF9B01
              0001010101000AD6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9E7B7F7F
              7F7F7F78C1FFFFFFFFFFFFBB777F7F7F7F7F7E7BD0FFFFFFFFFFFFD37C7E7F7F
              7F7F7B95FCFFFFFFFFFFC1787F7F7F7F7F7D85F2FFFFFFFFFFFB927B7F7F7F7F
              7F7F7F7F7F7F7F7F7F7F7F7F7F7C8BF9FFFFCD7A7F7F7F7F7F7F7F7F7F7F7F7F
              7F7F7F7F7F7F77A9FFFFFF82000000000000003DFCFFFFFFFFFF8C0000000000
              0000006FFFFFFFFFFFFFC90300000000000001C9FFFFFE41000000000000005C
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF7E000000000000000089FFFFFFFFFFD72E00
              0000000000000000000000000000000458E0FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              930000000000000001C5FFFFFFFFFFFFFFFFFFFFFFF629000000000000000000
              0000000000000000000000005CFFFFFFFFB50000000000000000A9FFFFFFFFFF
              FFFF7C0000000000000009DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9F7C808080808079C2FFFFFFFFFFFD9B7B80808080808078B2FFFFFFFFFFFFD3
              7D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C
              80808080808080808080808080808080807D8CF9FFFFCE7B8080808080808080
              8080808080808080808078AAFFFFFFA7000000000000001BF2FFFFFFFFFF8000
              0000000000000012E1FFFFFFFFFFEC1C00000000000000A7FFFFFFAD01000000
              00000009D6FFFFFFFFFFFFFFFFFFFFFFFFFF6B00000000000000009FFFFFFFFF
              EE350000000000000000000000000000000000000025C7FFFFFFFFFFFFFFFFFF
              FFFFFFFFC40400000000000000A3FFFFFFFFFFFFFFFFFFFFFFFF5A0000000000
              000000000000000000000000000000003CFDFFFFFFD60A000000000000007CFF
              FFFFFFFFFFFF7C0000000000000006CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9F7C808080808079C2FFFFFFFFFFEE847E8080808080807B9BFEFFFF
              FFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFF
              FFFB937C80808080808080808080808080808080807D8CF9FFFFCE7B80808080
              808080808080808080808080808078AAFFFFFFC70300000000000005D1FFFFFF
              FFFF7400000000000000000091FFFFFFFFFFFC390000000000000084FFFFFFFA
              330000000000000086FFFFFFFFFFFFFFFFFFFFFFFFFF590000000000000002BE
              FFFFFFFF990000000000000000000000000000000000000000001AD2FFFFFFFF
              FFFFFFFFFFFFFFFFE9180000000000000080FFFFFFFFFFFFFFFFFFFFFFFF8600
              00000000000000000000000000000000000000001DF2FFFFFFF1240000000000
              000059FFFFFFFFFFFFFF8E0000000000000002C4FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFFFFFD87E7F8080808080807E
              87F1FFFFFFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2
              FFFFFFFFFFFB937C80808080808080808080808080808080807D8CF9FFFFCE7B
              80808080808080808080808080808080808078AAFFFFFFE51500000000000000
              9FFFFFFFFFFF660000000000000000002CF3FFFFFFFFFF59000000000000005A
              FFFFFFFF930000000000000032FBFFFFFFFFFFFFFFFFFFFFFFFF480000000000
              000009D3FFFFFFFC430000000000000000000001040000000000000000000035
              F1FFFFFFFFFFFFFFFFFFFFFFFB350000000000000054FFFFFFFFFFFFFFFFFFFF
              FFFFA900000000000000000000000000000000000000000003CCFFFFFFFE4E00
              00000000000038FDFFFFFFFFFFFFB00000000000000000AFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFFFFFC17980808080
              808080807BD0FFFFFFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080
              807E86F2FFFFFFFFFFFB937C8080808080807C797979797979797979797686F9
              FFFFCE7B80808080808080797979797979797979797971A6FFFFFFFA38000000
              0000000074FFFFFFFFFF5A00000000000000000000B1FFFFFFFFFF7E00000000
              0000002BF7FFFFFFEF2300000000000001B0FFFFFFFFFFFFFFFFFFFFFFF73300
              00000000000016E7FFFFFFE71600000000000000001082BBC6B7710A00000000
              0000000088FFFFFFFFFFFFFFFFFFFFFFFF560000000000000025F5FFFFFFFFFF
              FFFFFFFFFFFFC90400000000000000070E0D0D0D0D0D0D0D0D0D0D0D0AA4FFFF
              FFFF7A000000000000001AEDFFFFFFFFFFFFDA0D000000000000009BFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFFFFEA17A
              808080808080808078B2FFFFFFFFFFD37D7F808080807C96FCFFFFFFFFFFC279
              80808080807E86F2FFFFFFFFFFFB937C80808080807CA4C7C7C7C7C7C7C7C7C7
              C7C5CCFCFFFFCE7B80808080807F84C2C7C7C7C7C7C7C7C7C7C7C3D9FFFFFFFF
              650000000000000051FFFFFFFFFF4F000000000000000000004DFEFFFFFFFFAC
              000000000000000CDEFFFFFFFF7D000000000000004BFFFFFFFFFFFFFFFFFFFF
              FFF0220000000000000022F4FFFFFFD3090000000000000009B4FFFFFFFFFFBC
              160000000000000016E5FFFFFFFFFFFFFFFFFFFFFF7A000000000000000ADBFF
              FFFFFFFFFFFFFFFFFFFFE616000000000000005DE3E2E2E2E2E2E2E2E2E2E2E2
              E0EFFFFFFFFFA00000000000000005CAFFFFFFFFFFFFF6280000000000000081
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF007B7B7B7B7B7B7B7B7B7B7A75747474747474747474747474747474747474
              74747474747474747474747474777B7B7B7B7A78757474747474747474747474
              74747474747474747474747474747475787A7B7B7B7B7B757474747474747474
              74747474747474747474747474747474747474747474747474767B7B77747474
              74767B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7974747474757A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7874747474747474747474747474747474747474747474747474
              747474747474747474787B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFF
              FFF2867E80808080808080807C9AFDFFFFFFFFD37D7F808080807C96FCFFFFFF
              FFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808077C4FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFCE7B80808080807E87FEFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF8E0000000000000032FAFFFFFFFE430000000000000000000005CCFF
              FFFFFFD70B00000000000001BDFFFFFFFFE2160000000000000AD1FFFFFFFFFF
              FFFFFFFFFFF2170000000000000035FBFFFFFFCF06000000000000003BFFFFFF
              FFFFFFFF9800000000000000009DFFFFFFFFFFFFFFFFFFFFFFA8000000000000
              0000B9FFFFFFFFFFFFFFFFFFFFFFFB3B000000000000004AFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFC1020000000000000099FFFFFFFFFFFFFF4B00000000
              0000005FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF007B7B7B7B7B7B7B7B7B7A81A5A7A7A7A7A7A7A7A7A7A7A7A7A7A7
              A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A796797B7B777888A4A7A7A7A7A7A7A7
              A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A18678787B7B7A7DA2A7A7A7A7
              A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A79F7C78
              93A7A7A7A79C7C7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7989A7A7A7A7A5807A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B798CA7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7A7
              A7A7A7A7A7A7A7A7A7A7A7A7A78C797B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079
              C2FFFFFFFFDC7E7F80808080808080807E86F0FFFFFFFFD37D7F808080807C96
              FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFB00000000000000014E8FFFFFFFC38000000000000000000
              000073FFFFFFFFF325000000000000009DFFFFFFFFFF66000000000000004593
              9090909090909090927D090000000000000046FFFFFFFFD80D00000000000000
              53FFFFFFFFFFFFFFF628000000000000004CFFFFFFFFFFFFFFFFFFFFFFD50A00
              00000000000099FFFFFFFFFFFFFFFFFFFFFFFF680000000000000028F6FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFE00F0000000000000070FFFFFFFFFFFFFF6D
              0000000000000036FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFDFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCD767A7789D3F6FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4CD83777B7981F1
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFEA7E72C5FFFFFFFFE07C7A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B75A5FFFFFFFFFC8A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B74AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAE747B7B7B7B7B7B7A79D5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C8080
              80808079C2FFFFFFFFC7798080808080808080807F7CD4FFFFFFFFD37D7F8080
              80807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C80808080
              8078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFCE0600000000000003C3FFFFFFF92E0000000000
              0000000000001BEBFFFFFFFE41000000000000007CFFFFFFFFFFC70700000000
              0000000000000000000000000000000000000000000057FFFFFFFFE818000000
              000000004FFFFFFFFFFFFFFFFF5A0000000000000013E9FFFFFFFFFFFFFFFFFF
              FFF01F0000000000000077FFFFFFFFFFFFFFFFFFFFFFFF8D000000000000000E
              E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF62C0000000000000052FFFFFFFF
              FFFFFF910000000000000015EEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFBFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC76768EECFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE585
              787981F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFE97E72C4FFFFFFFFDF7C7A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B75A4FFFFFFFFFA8A787B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAD747B7B7B7B7B7B7A79D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9F7C808080808079C2FFFFFFFFAC798080808080808080808078B5FFFFFFFFD3
              7D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C
              808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEA1A0000000000000094FFFFFFF32400
              0000000000000000000000A1FFFFFFFF61000000000000004EFFFFFFFFFFFF48
              000000000000000000000000000000000000000000000000000068FFFFFFFFF9
              35000000000000003BFDFFFFFFFFFFFFFF700000000000000002C3FFFFFFFFFF
              FFFFFFFFFFFD3D0000000000000049FFFFFFFFFFFFFFFFFFFFFFFFAF00000000
              00000001B7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF580000000000000032
              FBFFFFFFFFFFFFBB0200000000000008D4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC7578
              D7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCB777881EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFE97E72C4FFFFFFFFDE7C7A7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B75A4FFFFFFFFF98A787B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAD747B7B7B7B7B
              7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9F7C808080808079C2FFFFFFF78E7E80808080807F808080807A9DFE
              FFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFF
              FFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080
              807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD41000000000001006BFFFF
              FFEA1A00000000000000000000000039F9FFFFFF880000000000000020F2FFFF
              FFFFFFB303000000000000000000000000000000000000000000000000007AFF
              FFFFFFFF66000000000000001AEFFFFFFFFFFFFFFF580000000000000000A4FF
              FFFFFFFFFFFFFFFFFFFF5F000000000000001BF1FFFFFFFFFFFFFFFFFFFFFFD0
              080000000000000086FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF8400000000
              00000014E8FFFFFFFFFFFFDA0D00000000000000B3FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B78
              8CFAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFCB7289FBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFEC7F7781EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE87E72C4FFFFFFFFDE7C7A7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B75A4FFFFFFFFF98A787B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAD747B
              7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFE3807F8080808081847F8080
              807D88F3FFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2
              FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B
              80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6F000000000000
              004AFFFFFFE21200000000000102000000000000BEFFFFFFB801000000000000
              09D7FFFFFFFFFFFD390000000000000000000000000000000000000000000000
              000093FFFFFFFFFFD9A4A7A7A7A7A7A6ACF5FFFFFFFFFFFFD916000000000000
              000094FFFFFFFFFFFFFFFFFFFFFF850000000000000007D4FFFFFFFFFFFFFFFF
              FFFFFFEC1C0000000000000061FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9
              0000000000000002BDFFFFFFFFFFFFE6120000000000000097FFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B
              7B7B7B788CFAFFFFFFFFBF9A9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F
              9F9F9F9FA091749BFEFFFFFFFBAA9D9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F
              9F9F9F9F9F9BB7FFFFFFFFF98B7681EFFFFFFFFFD2999F9F9F9F9F9F9F9F9F9F
              9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9FA0997C73C4FFFFFFFFDE7C7A7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B75A4FFFFFFFFF98A
              787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74AEFF
              FFFFFFFFC9999F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F
              A089797B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFCD7A808080807D99
              A77B808080807CD3FFFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080
              807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF960000
              00000000002AF7FFFFD60B00000000000C440000000000005CFFFFFFE2120000
              0000000000B6FFFFFFFFFFFF9E00000000000000000000000000000000000000
              000000000000AAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF042000000
              00000000000094FFFFFFFFFFFFFFFFFFFFFFB20000000000000000B4FFFFFFFF
              FFFFFFFFFFFFFFFD430000000000000041FEFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFCA04000000000000008FFFFFFFFFFFFFD4090000000000000087FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B
              7B7B7B7B7B7B7B788CFAFFFFFFFFA36E75757575757575757575757575757575
              7575757575757575757875A0FFFFFFFFF9857275757575757575757575757575
              7575757575757575757097FEFFFFFFFB917581EFFFFFFFFFBE6D757575757575
              757575757575757575757575757575757575757575767B74C4FFFFFFFFDE7C7A
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B75A4FFFF
              FFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B74AEFFFFFFFFFFB26D75757575757575757575757575757575757575757575
              7575757575797B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFFB3788080
              808079B3CC7A8080808078B5FFFFFFD37D7F808080807C96FCFFFFFFFFFFC279
              80808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFB8010000000000000FE0FFFFC70500000000000AAB20000000000009D8FFFF
              F92D0000000000000096FFFFFFFFFFFFF429000000000000010F1B1817171718
              19090000000000000005C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE147
              00000000000000000000A7FFFFFFFFFFFFFFFFFFFFFFDC0D0000000000000093
              FFFFFFFFFFFFFFFFFFFFFFFF700000000000000021F3FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFE815000000000000006AFFFFFFFFFFFF820000000000000000
              90FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFFC1737B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74C4FFFFFF
              FFDE7C7A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              75A4FFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B74AEFFFFFFFFFFB5737B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFFFFA
              937C8080808079C8E7807F8080807B9DFEFFFFD37D7F808080807C96FCFFFFFF
              FFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFD80B00000000000001B4FFFFB600000000000007D96A0000000000
              0081FFFFFF4C000000000000006EFFFFFFFFFFFFFF850000000000000056F1EC
              ECECECECEA3B000000000000000EE0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFBF260000000000000000000004CFFFFFFFFFFFFFFFFFFFFFFFF52700000000
              0000006BFFFFFFFFFFFFFFFFFFFFFFFF970000000000000008D5FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFC3A000000000000004AFFFFFFFFF1A11000000000
              00000000B1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFF
              BF7078787878787878787878787878787878797B7B7B7B7B7B7B7B7B7B7B7B74
              C4FFFFFFFFDD7977787878787878787878787878787878787878787878787878
              7A7B7B7B75A4FFFFFFFFF9877578787878787878787878787878787878797B7B
              7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFB3707878787878787878787878787878
              7878797B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079
              C2FFFFE8817F8080807F81E6FA8B7E8080807E88F3FFFFD37D7F808080807C96
              FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFF2240000000000000085FFFFA600000000000009E0CF08
              00000000001EEBFFFF6F000000000000003EFDFFFFFFFFFFFFE71A0000000000
              0019EDFFFFFFFFFFFB32000000000000001CEEFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFEC790700000000000000000000003CF9FFFFFFFFFFFFFFFFFFFFFFFF48
              0000000000000038FBFFFFFFFFFFFFFFFFFFFFFFB90000000000000000A9FFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6A000000000000001771747360260000
              0000000000000020EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EF
              FFFFFFFFC8838A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A867B7B7B7B7B7B7B7B7B
              7B7B7B74C4FFFFFFFFE28B898A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A8A
              8A8A8A887B757B7B75A4FFFFFFFFFA97878A8A8A8A8A8A8A8A8A8A8A8A8A8A8A
              8A867B7B7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFBD838A8A8A8A8A8A8A8A8A8A
              8A8A8A8A8A8A847B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C8080
              80808079C2FFFFD37C7F8080807D8FFBFFA87A8080807F7CD5FFFFD37D7F8080
              80807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C80808080
              8078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFF500000000000000061FFFF97000000000000
              10E2FF44000000000000A3FFFF990000000000000014ECFFFFFFFFFFFFFF7000
              0000000000008BFFFFFFFFFFF022000000000000002AF8FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFB62F00000000000000000000000000B1FFFFFFFFFFFFFFFFFFFF
              FFFFFF6A0000000000000010E9FFFFFFFFFFFFFFFFFFFFFFD90C000000000000
              005AB6B3B3B3B3B3B3B3B3B3B2BAFDFFFFFFFFFF930000000000000000000000
              0000000100000000000006AFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA774
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFF
              F98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB
              907581EFFFFFFFFFFCF9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9D67A7A7B7B7B
              7B7B7B7B7B7B7B74C4FFFFFFFFFEF9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9F9
              F9F9F9F9F9F9F9F8DC9E757B75A4FFFFFFFFFFFAF9F9F9F9F9F9F9F9F9F9F9F9
              F9F9F9F9F9D67A7A7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFFCF9F9F9F9F9F9F9
              F9F9F9F9F9F9F9F9F9F9C6767A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9F7C808080808079C2FFFFBA78808080807AA4FFFFC6798080808078B6FFFFD3
              7D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C
              808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7D000000000000003FFEFF890000
              0000000018EAFFA30000000000003DFAFFC70400000000000005CDFFFFFFFFFF
              FFFFD80F00000000000035FBFFFFFFFFEA19000000000000003BFDFFFFFFFFFF
              FFFFFFFFFFFFFFFFF275020000000000000000000000000062FEFFFFFFFFFFFF
              FFFFFFFFFFFFFF930000000000000003C9FFFFFFFFFFFFFFFFFFFFFFF3280000
              00000000000000000000000000000000000ADEFFFFFFFFFFB400000000000000
              0000000000000000000000000539B3FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFF
              FFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B759F
              FFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFE
              FFFFFFFB907581EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDB7A
              7A7B7B7B7B7B7B7B7B7B7B74C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFAA67675A4FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFDB7A7A7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCB767A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9F7C808080808079C2FFFE9C7C8080808077B8FFFFDC7E7F8080807B
              9FFFFFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFF
              FFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080
              807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA00000000000000020F5
              FF7B0000000000001DF1FFF323000000000001C5FFE91800000000000000AFFF
              FFFFFFFFFFFFFF5400000000000004C2FFFFFFFFE214000000000000004DFFFF
              FFFFFFFFFFFFFFFFFFFFFFEA4400000000000000000000000000003CF1FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFBE0200000000000000A9FFFFFFFFFFFFFFFFFFFFFF
              FF500000000000000000000000000000000000000000B1FFFFFFFFFFD2080000
              0000000000000000000000000000176BC4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B78
              8CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B759FFFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B769BFEFFFFFFFB907581EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFDC7A7A7B7B7B7B7B7B7B7B7B7B74C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEF8073A4FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFDC7A7A7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCB767A7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF9F7C808080808079C2FFF1867E8080807F7CD4FFFFF1867E
              8080807E8CF6FFD37D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2
              FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B
              80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFBF0200000000
              00000AD8FF6D00000000000023F5FFFF700000000000006DFFFB330000000000
              000090FFFFFFFFFFFFFFFFB90400000000000068FFFFFFFFD80C000000000000
              005DFFFFFFFFFFFFFFFFFFFFFFFFDF3700000000000000000000000100004AE6
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2130000000000000089FFFFFFFFFFFFFF
              FFFFFFFFFF79000000000000000000000000000000000000000084FFFFFFFFFF
              EC1D0000000000000000000000000000000008224E9BF6FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B7B7B7B7B
              7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFFFCF8F8F8F8F8F8F8F8F8F8F8
              F8F8F8F8F8F8D57A7A7B7B7B7B7B7B7B7B7B7B74C4FFFFFFFFFDF8F8F8F8F8F8
              F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8FAFFFFFFFFFE9670A4FFFFFFFFFFF9
              F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8D57A7A7B7B7B7B7B7B7B7B7B74AEFF
              FFFFFFFFFBF8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8F8C6767A7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFDA7E7F8080807E87F1FF
              FFFEA07A808080807ED9FFD37D7F808080807C96FCFFFFFFFFFFC27980808080
              807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00F
              00000000000000AAFF6300000000000036FBFFFFD40D000000000011E3FF5400
              00000000000066FFFFFFFFFFFFFFFFFD3E0000000000001EEEFFFFFFCF060000
              00000000006FFFFFFFFFFFFFFFFFFFFFFFEB3800000000000000000000000000
              057BF6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF92F000000000000005FFFFFFF
              FFFFFFFFFFFFFFFFFF9F00000000000000000000000000000000000000005FFF
              FFFFFFFFFE45000000000000000000000000000000000000000037C5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF007B7B
              7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFFC780878787878787
              87878787878787878787847B7B7B7B7B7B7B7B7B7B7B7B74C4FFFFFFFFE18886
              878787878787878787878787878787878787878783A5FEFFFFFFFD9970A4FFFF
              FFFFF9958587878787878787878787878787878787847B7B7B7B7B7B7B7B7B7B
              7B74AEFFFFFFFFFFBC8087878787878787878787878787878788827B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C2FFC47980808080
              7B99FEFFFFFFC0798080808078B9FFD37D7F808080807C96FCFFFFFFFFFFC279
              80808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFF82F000000000000007DFF5800000000000049FFFFFFFF4F000000000000
              93FF770000000000000033FAFFFFFFFFFFFFFFFF970000000000000098FFFFFF
              C4030000000000000084FFFFFFFFFFFFFFFFFFFFFE6400000000000000000000
              0000003BBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF4F00000000000000
              2FF8FFFFFFFFFFFFFFFFFFFFFFC1010000000000000100000000000000000000
              00003AFCFFFFFFFFFF730000000000000005313333331C000000000000000010
              B2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFFC0707878
              7878787878787878787878787878797B7B7B7B7B7B7B7B7B7B7B7B74C4FFFFFF
              FFDD797778787878787878787878787878787878787878787399FEFFFFFFFD98
              70A4FFFFFFFFF9877578787878787878787878787878787878797B7B7B7B7B7B
              7B7B7B7B7B74AEFFFFFFFFFFB37078787878787878787878787878787878797B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079C3FFA57A
              8080808078AEFFFFFFFFD87D7F8080807BA1FFD47D7F808080807C96FCFFFFFF
              FFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFF5D000000000000005BFF4D00000000000056FFFFFFFFB00100
              000000002AFBA7000000000000000DE5FFFFFFFFFFFFFFFFE71A000000000000
              40FFFFFFB900000000000000009EFFFFFFFFFFFFFFFFFFFFC303000000000000
              00000000001792F4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF72000000
              000000000EE2FFFFFFFFFFFFFFFFFFFFFFE00F00000000000001060A09090909
              09090909090827F1FFFFFFFFFF9A000000000000000CDBFAFAFAEFB026000000
              000000000FCAFFFFFFFFFFFFFFFFFFFFFFFEC47F5D566692DEFFFFFFFFFFFFFF
              FFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B759FFFFFFFFFF98A787B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFFFFFFFB907581EFFFFFFFFF
              C1737B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B74
              C4FFFFFFFFDE7C7A7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769BFEFF
              FFFFFD9870A4FFFFFFFFF98A787B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B74AEFFFFFFFFFFB5737B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808079
              C5FA897D8080808079C8FFFFFFFFEF857F8080807E8BFCD57D7F808080807C96
              FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFF87000000000000003AFB4300000000000061FFFFFF
              FFF930000000000000BADD0800000000000002C5FFFFFFFFFFFFFFFFFF7E0000
              0000000008CCFFFFAD0000000000000000B7FFFFFFFFFFFFFFFFFFFF68000000
              00000000000000005FDBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9B0000000000000002C1FFFFFFFFFFFFFFFFFFFFFFF72F0000000000000043D9
              D8D8D8D8D8D8D8D8D8D8DAFCFFFFFFFFFFBB0100000000000002C0FFFFFFFFFF
              D820000000000000003AF5FFFFFFFFFFFFFFFFFFB9410100000000001062E1FF
              FFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA7747B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B75A0FFFFFFFFF9837074
              7474747474747474747474747474747474747474746E95FEFFFFFFFB917581EF
              FFFFFFFFBD6B7474747474747474747474747474747474747474747474747474
              74757B74C4FFFFFFFFDC75737474747474747474747474747474747474747474
              6E95FEFFFFFFFD9870A4FFFFFFFFF98370747474747474747474747474747474
              7474747474747474747474747B74AEFFFFFFFFFFB16B74747474747474747474
              7474747474747474747474747474747474787B7B7B7B7B7B7B7A79D5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C8080
              80808079C7E77F7F8080807F82E8FFFFFFFFFD9C7B8080807F7DDFD97D7F8080
              80807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C80808080
              8078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAB000000000000001CED39000000000000
              72FFFFFFFFFF8800000000000052F32100000000000000A6FFFFFFFFFFFFFFFF
              FFD90D0000000000006BFFFFA40000000000000007D3FFFFFFFFFFFFFFFFFFF1
              220000000000000000000F9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFC80600000000000000A0FFFFFFFFFFFFFFFFFFFFFFFF5D0000000000
              000031FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDA0D000000000000009BFF
              FFFFFFFFFFA40200000000000000A6FFFFFFFFFFFFFFFF890700012F6772581C
              000023C4FFFFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFFFFFFA774
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B769AFFFFFFFF
              FCB6ABADADADADADADADADADADADADADADADADADADADADADADAAC1FFFFFFFFF8
              897681EFFFFFFFFFD9A8ADADADADADADADADADADADADADADADADADADADADADAD
              ADADADADAEA57C73C4FFFFFFFFEBAEADADADADADADADADADADADADADADADADAD
              ADADADADAAC1FFFFFFFFFD9870A4FFFFFFFFFCB6ABADADADADADADADADADADAD
              ADADADADADADADADADADADADADADAEAA7E73AEFFFFFFFFFFD1A8ADADADADADAD
              ADADADADADADADADADADADADADADADADADADADADAE8E787B7B7B7B7B7B7A79D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9F7C808080808079CAD07A808080807D93FAFFFFFFFFFFBD788080808078BFDA
              7D7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C
              808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCC050000000000000CCF310000
              0000000081FFFFFFFFFFE518000000000007CA480000000000000081FFFFFFFF
              FFFFFFFFFFFF670000000000001FF1FF990000000000000011EAFFFFFFFFFFFF
              FFFFFFD90B00000000000000000EBFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFEB1B000000000000007BFFFFFFFFFFFFFFFFFFFFFFFF8800
              00000000000011E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF3280000000000
              000077FFFFFFFFFFFFFB36000000000000003DFBFFFFFFFFFFFF9000002C99D4
              F8FFF3C570050019D3FFFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B788CFAFFFF
              FFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7888
              FAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFEA7E7881EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFEC7E72C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9770A4FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB8371AEFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAF747B7B7B7B7B
              7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9F7C808080808079C6B3798080808079A8FFFFFFFFFFFFD67D7F8080
              807BA7D37E7F808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFF
              FFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080
              807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE91800000000000003
              A72B0000000000008AFFFFFFFFFFFF64000000000000776A0000000000000053
              FFFFFFFFFFFFFFFFFFFFCA060000000000009AFF8C0000000000000021F3FFFF
              FFFFFFFFFFFFFFD109000000000000000085FFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFC39000000000000004CFFFFFFFFFFFFFFFFFFFF
              FFFFAC0000000000000001BAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5300
              00000000000048FEFFFFFFFFFFFF7C0000000000000005D0FFFFFFFFFFCA0B00
              55A41F1472FF4C16288E1B003DF7FFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B78
              8CFAFFFFFFFFA7747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7A77CFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFC5767981EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFE97E72C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFDF7E73A4FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88371AEFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAD747B
              7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF9F7C80808080807BBA947D8080808078BEFFFFFFFFFFFFEE
              837E8080807E91C97F7F808080807C96FCFFFFFFFFFFC27980808080807E86F2
              FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B
              80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC3D000000
              000000007E2800000000000094FFFFFFFFFFFFCA080000000000226800000000
              00000025F4FFFFFFFFFFFFFFFFFFFD5100000000000042FF8200000000000000
              31F9FFFFFFFFFFFFFFFFFFD90C000000000000001AEEFFFFFFFFFFFFD29E9F9F
              9F9F9F9EABFAFFFFFFFFFFFFFFFFFFFFFF590000000000000020F1FFFFFFFFFF
              FFFFFFFFFFFFCD05000000000000008BFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFF7F000000000000001DEEFFFFFFFFFFFFBA02000000000000009CFFFFFFFF
              FF540020EFB4000065F71C0009F5A90200B1FFFFFFFFFFFFFF007B7B7B7B7B7B
              7B7B7B788CFBFFFFFFFFA8747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7888E4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFDD81797981F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE97E72C5FFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE2897875A4FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF88371AFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFAE747B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9F7C80808080807EA8887F8080807F7FDDFFFFFF
              FFFFFFFD997C8080807F83AE837F808080807C96FCFFFFFFFFFFC27980808080
              807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              6B000000000000004D1C000000000000A0FFFFFFFFFFFFFF440000000000024B
              0C00000000000009DBFFFFFFFFFFFFFFFFFFFFB600000000000009DC7E000000
              0000000042FEFFFFFFFFFFFFFFFFFFED1E0000000000000039FDFFFFFFFFFFFF
              9E0000000000000005DBFFFFFFFFFFFFFFFFFFFFFF7D0000000000000009D7FF
              FFFFFFFFFFFFFFFFFFFFE9180000000000000067FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFA40000000000000006D2FFFFFFFFFFFFDD0F000000000000006F
              FFFFFFFFE3120094FFAA000069FB1E000CEEFF3D006AFFFFFFFFFFFFFF007B7B
              7B7B7B7B7B7B7B788CF8FDFDFDFDA6747B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7783C7F5FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD
              FDFDFDFDFDFDFDFDFDFDFDFCF1C07F777B7981EDFDFDFDFDFDFDFDFDFDFDFDFD
              FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDE67E72C3FDFDFDFDFDFDFD
              FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFCF7E1B181777B75A3FDFD
              FDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDF5
              8371ADFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFD
              FDFDFDFDFDAC747B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C8080808080809283808080807E8A
              F5FFFFFFFFFFFFFFB978808080807E8F8480808080807C96FCFFFFFFFFFFC279
              80808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF90000000000000001A09000000000000A9FFFFFFFFFFFFFF99000000
              0000001B0C00000000000001BDFFFFFFFFFFFFFFFFFFFFF4300000000000007E
              730000000000000053FFFFFFFFFFFFFFFFFFFFFE49000000000000002CFBFFFF
              FFFFFFFFC00100000000000000BBFFFFFFFFFFFFFFFFFFFFFFA4000000000000
              0001BBFFFFFFFFFFFFFFFFFFFFFFFB3A0000000000000049FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFC20200000000000000B5FFFFFFFFFFFFE71500000000
              0000004AFFFFFFFFAF0005D4FFAA000062D0130012EFFF81003DFFFFFFFFFFFF
              FF007B7B7B7B7B7B7B7B7B7A7F9596969696847A7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B787786969696969696969696969696
              969696969696969696969696969696948476797B7B7A7C939696969696969696
              96969696969696969696969696969696969696969696969696917C798A969696
              9696969696969696969696969696969696969696969696969694877C74797B7B
              7A84969696969696969696969696969696969696969696969696969696969696
              969696957D798696969696969696969696969696969696969696969696969696
              969696969696969696857A7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808080807F8080
              80807B9EFEFFFFFFFFFFFFFFD27C808080807F7F8080808080807C96FCFFFFFF
              FFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFB2000000000000000001000000000000B1FFFFFFFFFFFFFF
              EF2200000000010001000000000000009CFFFFFFFFFFFFFFFFFFFFFF9A000000
              0000002E4B0000000000000067FFFFFFFFFFFFFFFFFFFFFF8700000000000000
              0AD6FFFFFFFFFFFFCB06000000000000009FFFFFFFFFFFFFFFFFFFFFFFD20900
              0000000000009AFFFFFFFFFFFFFFFFFFFFFFFF6A0000000000000028F9FFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFE0110000000000000095FFFFFFFFFFFFD80A
              000000000000002DF9FFFFFF8C001EF2FFAA00000B0B000277FEFFB30023FFFF
              FFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7A7776767676797B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A787676767676767676
              7676767676767676767676767676767676767677797B7B7B7B7B7B7776767676
              7676767676767676767676767676767676767676767676767676767676777B7B
              787676767676767676767676767676767676767676767676767676767677787A
              7B7B7B7B7B7A7676767676767676767676767676767676767676767676767676
              76767676767676777B7B79767676767676767676767676767676767676767676
              76767676767676767676767676797B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808080
              80808080808078B4FFFFFFFFFFFFFFFFE9837F8080807F7F8080808080807C96
              FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C808080808078C0FF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFD2080000000000000000000000000000BAFFFFFF
              FFFFFFFFFF74000000000001000000000000000076FFFFFFFFFFFFFFFFFFFFFF
              EA1F0000000000030E0000000000000078FFFFFFFFFFFFFFFFFFFFFFC8040000
              000000000063FFFFFFFFFFFFAF010000000000000093FFFFFFFFFFFFFFFFFFFF
              FFFF260000000000000075FFFFFFFFFFFFFFFFFFFFFFFF91000000000000000C
              E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF731000000000000006DFFFFFFFF
              FFFF7F00000000000000001FF4FFFFFF890027F6FFAA000004000007B8FFFFBD
              001EFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9F7C8080
              808080808080808080807BD0FFFFFFFFFFFFFFFFFB957B808080808080808080
              80807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C80808080
              8078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEE1F0000000000000000000000000003
              C5FFFFFFFFFFFFFFFFD60E0000000000000000000000000048FEFFFFFFFFFFFF
              FFFFFFFFFF8600000000000000000000000000008EFFFFFFFFFFFFFFFFFFFFFF
              FA440000000000000007A7FFFFFFFFFF59000000000000000093FFFFF28D8485
              8585858585831E000000000000002985858585858585848FF4FFFFB400000000
              000000009DE2E0E0E0E0E0E0E0E0E0E0DFE7FFFFFFFFFF5D0000000000000037
              DEE1E0E1D0880B000000000000000030F9FFFFFF9C000EE5FFAA00005BBF1100
              2FF6FF99002EFFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              9F7C80808080808080808080807E85EFFFFFFFFFFFFFFFFFFFB6788080808080
              8080808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFFFFFB937C
              808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080807E87F6
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE4700000000000000000000
              00000007D1FFFFFFFFFFFFFFFFFF50000000000000000000000000001EF0FFFF
              FFFFFFFFFFFFFFFFFFDE1200000000000000000000000000A8FFFFFFFFFFFFFF
              FFFFFFFFFFB60000000000000000015FA8C1B769030000000000000000A0FFFF
              F1180000000000000000000000000000000000000000000000000000CAFFFFD3
              0800000000000000050C0B0B0B0B0B0B0B0B0B0B0B2CF6FFFFFFFF8800000000
              000000000A0B0B0B060000000000000000000054FFFFFFFFD10700B1FFAA0000
              69F31E0008EAFF580058FFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFF9F7C80808080808080808080807C97FDFFFFFFFFFFFFFFFFFFD17B80
              808080808080808080807C96FCFFFFFFFFFFC27980808080807E86F2FFFFFFFF
              FFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B80808080
              807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF73000000000000
              0000000000000009D8FFFFFFFFFFFFFFFFFFB002000000000000000000000000
              07D3FFFFFFFFFFFFFFFFFFFFFFFF6F00000000000000000000000003C2FFFFFF
              FFFFFFFFFFFFFFFFFFFE60000000000000000000000300000000000000000000
              01C6FFFFFE440000000000000000000000000000000000000000000000000000
              9CFFFFEF20000000000000000000000000000000000000000007D7FFFFFFFFAB
              000000000000000000000000000000000000000000000091FFFFFFFFFA380044
              FFB000001521010020FCD2100099FFFFFFFFFFFFFF007B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFF9F7C808080808080808080808079ADFFFFFFFFFFFFFFFFFF
              FFE7827F808080808080808080807C96FCFFFFFFFFFFC27980808080807E86F2
              FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCE7B
              80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9A0000
              00000000000000000000000FE0FFFFFFFFFFFFFFFFFFF9310000000000000000
              0000000000B3FFFFFFFFFFFFFFFFFFFFFFFFD00800000000000000000000000C
              DCFFFFFFFFFFFFFFFFFFFFFFFFFFEB3A00000000000000000000000000000000
              0000000033F5FFFFFF6700000000000000000000000000000000000000000000
              0000000073FFFFFF49000000000000000000000000000000000000000000A8FF
              FFFFFFCC050000000000000000000000000000000000000000000FDBFFFFFFFF
              FFA400008AB1000000000009AEEC36001CE7FFFFFFFFFFFFFF007B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFF9F7C808080808080808080808079C6FFFFFFFFFF
              FFFFFFFFFFFA927C808080808080808080807C96FCFFFFFFFFFFC27980808080
              807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFBB010000000000000000000000001AEAFFFFFFFFFFFFFFFFFFFF8A00000000
              00000000000000000092FFFFFFFFFFFFFFFFFFFFFFFFFF590000000000000000
              00000016F0FFFFFFFFFFFFFFFFFFFFFFFFFFFFE5450000000000000000000000
              0000000000000000B2FFFFFFFF8E000000000000000000000000000000000000
              000000000000000050FFFFFF7600000000000000000000000000000000000000
              00007BFFFFFFFFE9180000000000000000000000000000000000000000008FFF
              FFFFFFFFFFFE5D0000476D8F929095C8C8370002ABFFFFFFFFFFFFFFFF007B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9D7C808080808080808080807F83E6FF
              FFFFFFFFFFFFFFFFFFFFB478808080808080808080807C96FCFFFFFFFFFFC279
              80808080807E86F2FFFFFFFFFFFB937C808080808078C0FFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFCE7B80808080807E87F6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFDC0D00000000000000000000000024F2FFFFFFFFFFFFFFFFFFFFE7
              1B00000000000000000000000069FFFFFFFFFFFFFFFFFFFFFFFFFFC003000000
              0000000000000028F7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF67B120000000000
              00000000000000000000048AFFFFFFFFFFBC0200000000000000000000000000
              0000000000000000000000002FF9FFFF9D000000000000000000000000000000
              00000000000059FFFFFFFFFD3E00000000000000000000000000000000000000
              057EFFFFFFFFFFFFFFFFF35100002A80AEBAA55B07000591FFFFFFFFFFFFFFFF
              FF007B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B
              7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7B7A79D5FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA58487878787878787878787
              8597FAFFFFFFFFFFFFFFFFFFFFFFD18387878787878787878787849CFCFFFFFF
              FFFFC6818787878787868DF3FFFFFFFFFFFB9984878787878780C4FFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFD2848787878787868EF7FFFFFFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFF4350F0F0F0F0F0F0F0F0F0F0F0F38F8FFFFFFFFFFFFFF
              FFFFFFFF6A0F0F0F0F0F0F0F0F0F0F0F0F47FCFFFFFFFFFFFFFFFFFFFFFFFFFB
              470F0F0F0F0F0F0F0F0F0F43FCFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFD965
              160F101010101010101010100F2AB5FFFFFFFFFFFFE41F0F0F0F0F0F0F0F0F0F
              0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F0F22EAFFFFC1110F0F0F0F0F0F0F0F0F0F
              0F0F0F0F0F0F0F0F0F0F46FCFFFFFFFF740F0F0F0F0F0F0F0F0F0F0F0F0F0F10
              10100F46C2FFFFFFFFFFFFFFFFFFFFF973120F0F0F0F0F0F0F18A8FFFFFFFFFF
              FFFFFFFFFF00}
            PageFooter.PrintFooter = True
          end
        end
      end
    end
  end
  object ToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 784
    Height = 26
    EdgeBorders = [ebLeft, ebTop, ebRight, ebBottom]
    Flat = True
    Images = frmMain.ImageList16x16
    Indent = 3
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    object bClose: TToolButton
      Left = 3
      Top = 0
      Action = acClose
    end
    object ToolButton2: TToolButton
      Left = 26
      Top = 0
      Width = 8
      ImageIndex = 1
      Style = tbsDivider
    end
    object bOptions: TToolButton
      Left = 34
      Top = 0
      Action = acEditOfflimits
    end
    object bFilter: TToolButton
      Left = 57
      Top = 0
      Action = acFilter
    end
    object bPrint: TToolButton
      Left = 80
      Top = 0
      Action = acPrint
    end
    object ToolButton11: TToolButton
      Left = 103
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bSetOffCause: TToolButton
      Left = 111
      Top = 0
      Action = acSetOfflimitCause
    end
    object bEditOfflimitCause: TToolButton
      Left = 134
      Top = 0
      Action = acEditOfflimitCause
    end
    object bPatternRecognition: TToolButton
      Left = 157
      Top = 0
      Action = acDefPatternRecognition
    end
    object ToolButton1: TToolButton
      Left = 180
      Top = 0
      Width = 10
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bEditGridOptions: TToolButton
      Left = 190
      Top = 0
      Action = acEditGridOptions
    end
    object ToolButton7: TToolButton
      Left = 213
      Top = 0
      Action = acLoadProfile
    end
    object ToolButton10: TToolButton
      Left = 236
      Top = 0
      Action = acSaveProfile
    end
    object ToolButton4: TToolButton
      Left = 259
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bShowInfoWindow: TToolButton
      Left = 267
      Top = 0
      Action = acShowHideInfoWindow
    end
    object ToolButton3: TToolButton
      Left = 290
      Top = 0
      Width = 10
      Style = tbsSeparator
    end
    object bSecurity: TmmSpeedButton
      Left = 300
      Top = 0
      Width = 102
      Height = 22
      Action = acSecurity
      Flat = True
      Transparent = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object ToolButton8: TToolButton
      Left = 402
      Top = 0
      Width = 8
      ImageIndex = 5
      Style = tbsSeparator
    end
    object bHelp: TToolButton
      Left = 410
      Top = 0
      Action = acHelp
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'Dictionary1'
    OnAfterTranslate = mmTranslatorAfterTranslate
    Left = 216
    Top = 96
    TargetsData = (
      1
      7
      (
        '*'
        'Tabs'
        0)
      (
        '*'
        'Title'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'DisplayLabel'
        0)
      (
        'TwwDBGrid'
        '*'
        0)
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object MMSecurityControl: TMMSecurityControl
    FormCaption = '(100)Maschinen-Offlimit Bericht'
    Active = True
    Left = 451
    Top = 25
  end
  object ActionList1: TmmActionList
    Images = frmMain.ImageList16x16
    OnUpdate = ActionList1Update
    Left = 256
    Top = 192
    object acClose: TAction
      Hint = '(*)Fenster schliessen'
      ImageIndex = 24
      OnExecute = acCloseExecute
    end
    object acEditOfflimits: TAction
      Hint = '(*)Offlimit-Einstellungen bearbeiten...'
      ImageIndex = 59
      ShortCut = 16467
      OnExecute = acEditOfflimitsExecute
    end
    object acFilter: TAction
      Hint = '(*)Filterbedingung fuer Offlimit-Bericht'
      ImageIndex = 58
      ShortCut = 16454
      OnExecute = acFilterExecute
    end
    object acPrint: TAction
      Hint = '(*)Bericht, Formular ausdrucken...'
      ImageIndex = 2
      OnExecute = acPrintExecute
    end
    object acHelp: TAction
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 4
      OnExecute = acHelpExecute
    end
    object acEditOfflimitCause: TAction
      Hint = '(*)Offlimit-Ursachen bearbeiten...'
      ImageIndex = 47
      OnExecute = acEditOfflimitCauseExecute
    end
    object acShowHideInfoWindow: TAction
      Hint = '(*)Info-Fenster zeigen bzw. verbergen'
      ImageIndex = 6
      OnExecute = acShowHideInfoWindowExecute
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle konfigurieren'
      OnExecute = acSecurityExecute
    end
    object acResetSpindle: TAction
      Caption = '(*)Ausgewaehlte Spindel loeschen...'
      Enabled = False
      Hint = '(*)Loescht die aktuell ausgewaehlte Spindel'
      ImageIndex = 63
      OnExecute = acResetSpindleExecute
    end
    object acResetWithFilter: TAction
      Caption = '(*)Spindeln ueber Filter loeschen...'
      Enabled = False
      Hint = '(*)Spindeln ueber Filter loeschen'
      OnExecute = acResetWithFilterExecute
    end
    object acToggleGridChart: TAction
      Caption = '(*)Statistik als Grafik oder Tabelle zeigen'
      Hint = '(*)Zeigt die Statistik als Grafik'
      ImageIndex = 62
      OnExecute = acToggleGridChartExecute
    end
    object acShowPie: TAction
      Caption = '(*)Kuchengrafik ein'
      Hint = '(*)Zeigt die Kuchengrafik'
      ImageIndex = 64
      OnExecute = acShowPieExecute
    end
    object acSort: TAction
      Caption = '(*)Sortiert nach ...'
      Hint = '(*)Sortiert nach Maschine oder nach Fehler'
      ImageIndex = 50
      OnExecute = acSortExecute
    end
    object acSortMachine: TAction
      Caption = '(*)Sortiert nach Maschinen'
      Hint = '(*)Sortiert nach Maschinen'
      OnExecute = acSortMachineExecute
    end
    object acSortMachineError: TAction
      Caption = '(*)Sortiert nach Fehler in Maschinengruppe'
      Hint = '(*)Sortiert nach Fehler in Maschinengruppe'
      OnExecute = acSortMachineExecute
    end
    object acSortError: TAction
      Caption = '(*)Sortiert nach Fehler'
      Hint = '(*)Sortiert nach Fehler'
      OnExecute = acSortMachineExecute
    end
    object acDefPatternRecognition: TAction
      Hint = '(*)Ursachen-Erkennung definieren...'
      ImageIndex = 67
      OnExecute = acDefPatternRecognitionExecute
    end
    object acSetOfflimitCause: TAction
      Caption = '(*)Offlimit-Ursache zuweisen...'
      Hint = '(*)Der ausgewaehlten Spulstelle eine Offlimit-Ursache zuweisen'
      ImageIndex = 65
      OnExecute = acSetOfflimitCauseExecute
    end
    object acEditGridOptions: TAction
      Caption = '(*)Tabellen-Eigenschaften'
      Hint = '(*)Tabellen-Eigenschaften bearbeiten'
      ImageIndex = 42
      OnExecute = acEditGridOptionsExecute
    end
    object acLoadProfile: TAction
      Caption = '(*)Profil &laden...'
      Hint = '(*)Gespeichertes Profil laden'
      ImageIndex = 1
      OnExecute = acLoadProfileExecute
    end
    object acSaveProfile: TAction
      Caption = '(*)Profil &speichern...'
      Hint = '(*)Einstellungen als Profil speichern'
      ImageIndex = 3
      OnExecute = acSaveProfileExecute
    end
    object acDeleteProfile: TAction
      Caption = '(*)Profil l&oeschen...'
      Hint = '(*)Gespeichertes Profil loeschen'
      ImageIndex = 46
      OnExecute = acDeleteProfileExecute
    end
    object acSaveAsDefault: TAction
      Caption = '(*)Profil als Standard speichern'
      Hint = '(*)Einstellungen als Standard speichern'
      ImageIndex = 55
      OnExecute = acSaveAsDefaultExecute
    end
  end
  object quOnline: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CursorType = ctStatic
    BeforeOpen = quOnlineBeforeOpen
    AfterOpen = quOnlineAfterOpen
    AfterScroll = quOnlineAfterScroll
    OnCalcFields = quOnlineCalcFields
    CommandText = 
      'SELECT '#13#10'  m.c_machine_name, mo.c_spindle_id, mo.c_update_time, ' +
      #13#10'  (mo.c_max_online_offlimit_time / 60) AS c_max_online_offlimi' +
      't_time, '#13#10'  mo.c_YB,mo.c_YB_online_time, '#13#10'  mo.c_RedL, mo.c_Red' +
      'L_online_time, mo.c_Eff,mo.c_Eff_online_time,'#13#10'  mo.c_CSp,mo.c_C' +
      'Sp_online_time,mo.c_RSp,mo.c_RSp_online_time,mo.c_CBu,mo.c_CBu_o' +
      'nline_time,'#13#10'  mo.c_CUpY,mo.c_CUpY_online_time,mo.c_tLSt / 60 as' +
      ' c_tLSt, mo.c_tLSt_online_time,'#13#10'  mo.c_LSt,mo.c_LSt_online_time' +
      ', mo.c_CSIRO,mo.c_CSIRO_online_time,mo.c_CYTot,'#13#10'  mo.c_CYTot_on' +
      'line_time,mo.c_OutOfProduction,'#13#10'  mo.c_OutOfProduction_online_t' +
      'ime,mo.c_maoffaverage_id,mo.c_machine_id,m.c_maoffset_id,'#13#10#13#10'   ' +
      '    moavg.c_YB AS c_YB_AVG,'#13#10'       moavg.c_Red AS c_RedL_AVG,'#13#10 +
      '       moavg.c_Eff AS c_Eff_AVG,'#13#10'       moavg.c_CSp AS c_CSp_AV' +
      'G,'#13#10'       moavg.c_RSp AS c_RSp_AVG,'#13#10'       moavg.c_CBu AS c_CB' +
      'u_AVG,'#13#10'       moavg.c_CUpY AS c_CUpY_AVG,'#13#10'       moavg.c_tLSt ' +
      '/ 60 AS c_tLSt_AVG,'#13#10'       moavg.c_LSt AS c_LSt_AVG,'#13#10'       mo' +
      'avg.c_CSIRO AS c_CSIRO_AVG,'#13#10'       moavg.c_CYTot AS c_CYTot_AVG' +
      ','#13#10#13#10'       moset.c_base,moset.c_Average_time,moset.c_ProdGrp_de' +
      'pend,moset.c_OffBorderByAverage,'#13#10'       moset.c_OffBorderByFixV' +
      'alues,moset.c_Eff_PC_max,moset.c_Eff_PC_min,moset.c_Eff_max,'#13#10'  ' +
      '     moset.c_Eff_min,moset.c_CYTot_PC_max,moset.c_CYTot_PC_min,m' +
      'oset.c_CYTot_max,moset.c_CYTot_min,'#13#10'       moset.c_CSIRO_PC_max' +
      ',moset.c_CSIRO_max,moset.c_CSp_PC_max,moset.c_CSp_max,moset.c_RS' +
      'p_PC_max,'#13#10'       moset.c_RSp_max,moset.c_YB_PC_max,moset.c_YB_M' +
      'ax,moset.c_CBu_PC_max,moset.c_CBu_max,'#13#10'       moset.c_CUpY_PC_m' +
      'ax,moset.c_CUpY_max,moset.c_tLSt_PC_max,moset.c_tLSt_max,moset.c' +
      '_LSt_PC_max,'#13#10'       moset.c_LSt_max,moset.c_RedL_PC_max,moset.c' +
      '_RedL_max'#13#10#13#10'FROM t_machine_offlimit mo'#13#10'JOIN t_machine_offlimit' +
      '_average moavg ON moavg.c_maoffaverage_id = mo.c_maoffaverage_id' +
      #13#10'JOIN t_machine m ON m.c_machine_id = mo.c_machine_id'#13#10'JOIN t_m' +
      'achine_offlimit_settings moset ON moset.c_maoffset_id = m.c_maof' +
      'fset_id'#13#10#13#10'WHERE mo.c_max_online_offlimit_time > 0'#13#10'AND mo.c_Out' +
      'OfProduction = 0'#13#10'ORDER BY m.c_machine_name,mo.c_spindle_id'#13#10
    Parameters = <>
    Left = 22
    Top = 100
    object quOnlinec_machine_name: TStringField
      DisplayWidth = 10
      FieldName = 'c_machine_name'
      Origin = 't_machine.c_machine_name'
      Size = 10
    end
    object quOnlinec_spindle_id: TSmallintField
      DisplayWidth = 5
      FieldName = 'c_spindle_id'
      Origin = 't_machine_offlimit.c_spindle_id'
    end
    object quOnlinec_update_time: TDateTimeField
      Alignment = taCenter
      DisplayWidth = 5
      FieldName = 'c_update_time'
      Origin = 't_machine_offlimit.c_update_time'
      DisplayFormat = 'd/m/yyyy hh:nn'
    end
    object quOnlinec_max_online_offlimit_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_max_online_offlimit_time'
      Origin = 't_machine_offlimit.c_max_online_offlimit_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOnlinec_Eff: TIntegerField
      Tag = 1
      DisplayWidth = 5
      FieldName = 'c_Eff'
      Origin = 't_machine_offlimit.c_Eff'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0'
    end
    object quOnlinec_tLSt: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_tLSt'
      ReadOnly = True
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0'
    end
    object quOnlinec_LSt: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_LSt'
      Origin = 't_machine_offlimit.c_LSt'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0'
    end
    object quOnlinec_CYTot: TFloatField
      DisplayWidth = 10
      FieldName = 'c_CYTot'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_CSIRO: TFloatField
      DisplayWidth = 10
      FieldName = 'c_CSIRO'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_CSp: TFloatField
      DisplayWidth = 10
      FieldName = 'c_CSp'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_RSp: TFloatField
      DisplayWidth = 10
      FieldName = 'c_RSp'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_CBu: TFloatField
      DisplayWidth = 10
      FieldName = 'c_CBu'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_CUpY: TFloatField
      DisplayWidth = 10
      FieldName = 'c_CUpY'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinec_YB: TFloatField
      DisplayWidth = 10
      FieldName = 'c_YB'
      OnGetText = OnGetOnlineText
      DisplayFormat = ',0.0'
    end
    object quOnlinecalc_OutOfProduction: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'calc_OutOfProduction'
      Calculated = True
    end
    object quOnlinecalc_Offcause1: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'calc_Offcause1'
      Size = 50
      Calculated = True
    end
    object quOnlinecalc_Offcause2: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'calc_Offcause2'
      Size = 50
      Calculated = True
    end
    object quOnlinecalc_Offcause3: TStringField
      DisplayWidth = 20
      FieldKind = fkCalculated
      FieldName = 'calc_Offcause3'
      Size = 50
      Calculated = True
    end
    object quOnlinec_base: TIntegerField
      FieldName = 'c_base'
      Visible = False
    end
    object quOnlinec_OutOfProduction: TBooleanField
      Alignment = taCenter
      DisplayLabel = '(40)c_OutOfProduction'
      DisplayWidth = 5
      FieldName = 'c_OutOfProduction'
      Origin = 't_machine_offlimit.c_OutOfProduction'
      ReadOnly = True
      Visible = False
    end
    object quOnlinec_OutOfProduction_online_time: TIntegerField
      DisplayLabel = '(40)c_OutOfProduction_online_time'
      DisplayWidth = 5
      FieldName = 'c_OutOfProduction_online_time'
      Origin = 't_machine_offlimit.c_OutOfProduction_online_time'
      Visible = False
      OnGetText = FormatTo_hh_mm
    end
    object quOnlinec_YB_online_time: TIntegerField
      DisplayLabel = '(40)c_YB_online_time'
      DisplayWidth = 10
      FieldName = 'c_YB_online_time'
      Visible = False
    end
    object quOnlinec_Eff_online_time: TIntegerField
      DisplayLabel = '(40)c_Eff_online_time'
      DisplayWidth = 10
      FieldName = 'c_Eff_online_time'
      Visible = False
    end
    object quOnlinec_CSp_online_time: TIntegerField
      DisplayLabel = '(40)c_CSp_online_time'
      DisplayWidth = 10
      FieldName = 'c_CSp_online_time'
      Visible = False
    end
    object quOnlinec_RSp_online_time: TIntegerField
      DisplayLabel = '(40)c_RSp_online_time'
      DisplayWidth = 10
      FieldName = 'c_RSp_online_time'
      Visible = False
    end
    object quOnlinec_CBu_online_time: TIntegerField
      DisplayLabel = '(40)c_CBu_online_time'
      DisplayWidth = 10
      FieldName = 'c_CBu_online_time'
      Visible = False
    end
    object quOnlinec_CUpY_online_time: TIntegerField
      DisplayLabel = '(40)c_CUpY_online_time'
      DisplayWidth = 10
      FieldName = 'c_CUpY_online_time'
      Visible = False
    end
    object quOnlinec_tLSt_online_time: TIntegerField
      DisplayLabel = '(40)c_tLSt_online_time'
      DisplayWidth = 10
      FieldName = 'c_tLSt_online_time'
      Visible = False
    end
    object quOnlinec_LSt_online_time: TIntegerField
      DisplayLabel = '(40)c_LSt_online_time'
      DisplayWidth = 10
      FieldName = 'c_LSt_online_time'
      Visible = False
    end
    object quOnlinec_CSIRO_online_time: TIntegerField
      DisplayLabel = '(40)c_CSIRO_online_time'
      DisplayWidth = 10
      FieldName = 'c_CSIRO_online_time'
      Visible = False
    end
    object quOnlinec_CYTot_online_time: TIntegerField
      DisplayLabel = '(40)c_CYTot_online_time'
      DisplayWidth = 10
      FieldName = 'c_CYTot_online_time'
      Visible = False
    end
    object quOnlinec_maoffaverage_id: TIntegerField
      Tag = -1
      FieldName = 'c_maoffaverage_id'
      Origin = 't_machine_offlimit.c_maoffaverage_id'
      Visible = False
    end
    object quOnlinec_machine_id: TSmallintField
      Tag = -1
      FieldName = 'c_machine_id'
      Origin = 't_machine_offlimit.c_machine_id'
      Visible = False
    end
    object quOnlinec_maoffset_id: TIntegerField
      Tag = -1
      FieldName = 'c_maoffset_id'
      Origin = 't_machine.c_maoffset_id'
      Visible = False
    end
    object quOnlinec_Eff_AVG: TIntegerField
      Tag = -1
      FieldName = 'c_Eff_AVG'
      Visible = False
    end
    object quOnlinec_tLSt_AVG: TIntegerField
      Tag = -1
      FieldName = 'c_tLSt_AVG'
      Visible = False
    end
    object quOnlinec_LSt_AVG: TIntegerField
      Tag = -1
      FieldName = 'c_LSt_AVG'
      Visible = False
    end
    object quOnlinec_CYTot_AVG: TFloatField
      FieldName = 'c_CYTot_AVG'
      Visible = False
    end
    object quOnlinec_CSIRO_AVG: TFloatField
      FieldName = 'c_CSIRO_AVG'
      Visible = False
    end
    object quOnlinec_CSp_AVG: TFloatField
      FieldName = 'c_CSp_AVG'
      Visible = False
    end
    object quOnlinec_RSp_AVG: TFloatField
      FieldName = 'c_RSp_AVG'
      Visible = False
    end
    object quOnlinec_CBu_AVG: TFloatField
      FieldName = 'c_CBu_AVG'
      Visible = False
    end
    object quOnlinec_CUpY_AVG: TFloatField
      FieldName = 'c_CUpY_AVG'
      Visible = False
    end
    object quOnlinec_YB_AVG: TFloatField
      FieldName = 'c_YB_AVG'
      Visible = False
    end
    object quOnlinec_Average_time: TIntegerField
      Tag = -1
      FieldName = 'c_Average_time'
      Visible = False
    end
    object quOnlinec_OffBorderByAverage: TBooleanField
      Tag = -1
      FieldName = 'c_OffBorderByAverage'
      Visible = False
    end
    object quOnlinec_OffBorderByFixValues: TBooleanField
      Tag = -1
      FieldName = 'c_OffBorderByFixValues'
      Visible = False
    end
    object quOnlinec_Eff_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_Eff_PC_max'
      Visible = False
    end
    object quOnlinec_Eff_PC_min: TIntegerField
      Tag = -1
      FieldName = 'c_Eff_PC_min'
      Visible = False
    end
    object quOnlinec_Eff_max: TIntegerField
      Tag = -1
      FieldName = 'c_Eff_max'
      Visible = False
    end
    object quOnlinec_Eff_min: TIntegerField
      Tag = -1
      FieldName = 'c_Eff_min'
      Visible = False
    end
    object quOnlinec_CYTot_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_CYTot_PC_max'
      Visible = False
    end
    object quOnlinec_CYTot_PC_min: TIntegerField
      Tag = -1
      FieldName = 'c_CYTot_PC_min'
      Visible = False
    end
    object quOnlinec_CYTot_max: TFloatField
      Tag = -1
      FieldName = 'c_CYTot_max'
      Visible = False
    end
    object quOnlinec_CYTot_min: TFloatField
      Tag = -1
      FieldName = 'c_CYTot_min'
      Visible = False
    end
    object quOnlinec_CSIRO_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_CSIRO_PC_max'
      Visible = False
    end
    object quOnlinec_CSIRO_max: TFloatField
      Tag = -1
      FieldName = 'c_CSIRO_max'
      Visible = False
    end
    object quOnlinec_CSp_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_CSp_PC_max'
      Visible = False
    end
    object quOnlinec_CSp_max: TFloatField
      Tag = -1
      FieldName = 'c_CSp_max'
      Visible = False
    end
    object quOnlinec_RSp_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_RSp_PC_max'
      Visible = False
    end
    object quOnlinec_RSp_max: TFloatField
      Tag = -1
      FieldName = 'c_RSp_max'
      Visible = False
    end
    object quOnlinec_YB_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_YB_PC_max'
      Visible = False
    end
    object quOnlinec_YB_Max: TFloatField
      Tag = -1
      FieldName = 'c_YB_Max'
      Visible = False
    end
    object quOnlinec_CBu_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_CBu_PC_max'
      Visible = False
    end
    object quOnlinec_CBu_max: TFloatField
      Tag = -1
      FieldName = 'c_CBu_max'
      Visible = False
    end
    object quOnlinec_CUpY_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_CUpY_PC_max'
      Visible = False
    end
    object quOnlinec_CUpY_max: TFloatField
      Tag = -1
      FieldName = 'c_CUpY_max'
      Visible = False
    end
    object quOnlinec_tLSt_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_tLSt_PC_max'
      Visible = False
    end
    object quOnlinec_tLSt_max: TIntegerField
      Tag = -1
      FieldName = 'c_tLSt_max'
      Visible = False
    end
    object quOnlinec_LSt_PC_max: TIntegerField
      Tag = -1
      FieldName = 'c_LSt_PC_max'
      Visible = False
    end
    object quOnlinec_LSt_max: TIntegerField
      Tag = -1
      FieldName = 'c_LSt_max'
      Visible = False
    end
  end
  object srcOnline: TwwDataSource
    DataSet = quOnline
    Left = 54
    Top = 100
  end
  object quOffline: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CursorType = ctStatic
    AfterOpen = quOfflineAfterOpen
    AfterScroll = quOfflineAfterScroll
    CommandText = 
      'SELECT mo.c_machine_id, m.c_machine_name, mo.c_spindle_id, mo.c_' +
      'offline_offlimit_time/60 c_offline_offlimit_time, mo.c_offline_w' +
      'atchtime/60 c_offline_watchtime,'#13#10'       (100.0 * mo.c_offline_o' +
      'fflimit_time / mo.c_offline_watchtime) as calc_OfflineRatio,'#13#10'  ' +
      '     mo.c_YB_offline_time/60 c_YB_offline_time, mo.c_RedL_offlin' +
      'e_time/60 c_RedL_offline_time, mo.c_Eff_offline_time/60 c_Eff_of' +
      'fline_time,'#13#10'       mo.c_CSp_offline_time/60 c_CSp_offline_time,' +
      ' mo.c_RSp_offline_time/60 c_RSp_offline_time,mo.c_CBu_offline_ti' +
      'me/60 c_CBu_offline_time,'#13#10'       mo.c_CUpY_offline_time/60 c_CU' +
      'pY_offline_time, mo.c_tLSt_offline_time/60 c_tLSt_offline_time, ' +
      'mo.c_LSt_offline_time/60 c_LSt_offline_time,'#13#10'       mo.c_CSIRO_' +
      'offline_time/60 c_CSIRO_offline_time, mo.c_CYTot_offline_time/60' +
      ' c_CYTot_offline_time,'#13#10'       mo.c_OutOfProduction, mo.c_OutOfP' +
      'roduction_offline_time/60 c_OutOfProduction_offline_time'#13#10'FROM t' +
      '_machine_offlimit mo'#13#10'JOIN t_machine m ON (m.c_machine_id = mo.c' +
      '_machine_id)'#13#10'WHERE mo.c_offline_offlimit_time >= :MinOfflimitTi' +
      'me'#13#10'AND mo.c_offline_watchtime > 0'#13#10'AND (100.0 *mo.c_offline_off' +
      'limit_time /mo.c_offline_watchtime) >= :MinOfflineRatio'#13#10'ORDER B' +
      'Y m.c_machine_name,mo.c_spindle_id'#13#10
    Parameters = <
      item
        Name = 'MinOfflimitTime'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MinOfflineRatio'
        Attributes = [paSigned, paNullable]
        DataType = ftBCD
        NumericScale = 12
        Precision = 26
        Size = 19
        Value = Null
      end>
    Left = 22
    Top = 148
    object quOfflinec_machine_name: TStringField
      Tag = 10
      Alignment = taCenter
      DisplayWidth = 10
      FieldName = 'c_machine_name'
      Size = 10
    end
    object quOfflinec_spindle_id: TSmallintField
      Tag = 10
      DisplayWidth = 5
      FieldName = 'c_spindle_id'
    end
    object quOfflinecalc_OfflineRatio: TFloatField
      Tag = 10
      DisplayWidth = 5
      FieldName = 'calc_OfflineRatio'
      DisplayFormat = ',0.00'
    end
    object quOfflinec_offline_offlimit_time: TIntegerField
      Tag = 10
      DisplayWidth = 5
      FieldName = 'c_offline_offlimit_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_offline_watchtime: TIntegerField
      Tag = 10
      DisplayWidth = 5
      FieldName = 'c_offline_watchtime'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_YB_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_YB_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_RedL_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_RedL_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_Eff_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_Eff_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_CSp_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_CSp_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_RSp_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_RSp_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_CBu_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_CBu_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_CUpY_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_CUpY_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_tLSt_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_tLSt_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_LSt_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_LSt_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_CSIRO_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_CSIRO_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_CYTot_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_CYTot_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_OutOfProduction_offline_time: TIntegerField
      DisplayWidth = 5
      FieldName = 'c_OutOfProduction_offline_time'
      OnGetText = FormatTo_hh_mm
    end
    object quOfflinec_machine_id: TSmallintField
      Tag = 10
      FieldName = 'c_machine_id'
      Visible = False
    end
  end
  object mmGridPos1: TmmGridPos
    MemoINI.AdoConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=wetsrvmm;Initial Catalog=MM_Wind' +
      'ing;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    AlterColors = True
    AlterColumnFormat = True
    AlterColumnPosition = True
    AlterGridOptions = True
    AlterFixedCols = True
    AlterFonts = True
    AlterVisibleCols = True
    Grid = wwDBGrid1
    LookupID = 'ONLINE_OFFLIMIT'
    Left = 118
    Top = 100
  end
  object scrOffline: TwwDataSource
    DataSet = quOffline
    Left = 54
    Top = 148
  end
  object quProdID: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CommandText = 
      'SELECT int.c_interval_start,sid.c_prod_id,pg.c_spindle_first,pg.' +
      'c_spindle_last FROM t_interval int JOIN t_spindle_interval_data ' +
      'sid ON (sid.c_interval_id = int.c_interval_id) JOIN t_prodgroup ' +
      'pg ON (pg.c_prod_id = sid.c_prod_id) WHERE sid.c_machine_id = :M' +
      'achineID AND sid.c_spindle_id = :SpindleID ORDER BY int.c_interv' +
      'al_start DESC'
    Parameters = <
      item
        Name = 'MachineID'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'SpindleID'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    Left = 22
    Top = 188
  end
  object mmGridPos2: TmmGridPos
    MemoINI.AdoConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=wetsrvmm;Initial Catalog=MM_Wind' +
      'ing;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    AlterColors = True
    AlterColumnFormat = True
    AlterColumnPosition = True
    AlterGridOptions = True
    AlterFixedCols = True
    AlterFonts = True
    AlterVisibleCols = True
    Grid = wwDBGrid2
    LookupID = 'OFFLINE_OFFLIMIT'
    Left = 94
    Top = 148
  end
  object quUpdate: TmmADOCommand
    CommandText = 
      'UPDATE t_machine_offlimit  SET c_offcause_id = :OffCauseID WHERE' +
      ' c_machine_id = :MachineID AND c_spindle_id = :SpindleID '
    Connection = dmSpindleReport.conDefault
    Parameters = <
      item
        Name = 'OffCauseID'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'MachineID'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'SpindleID'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    Left = 54
    Top = 188
  end
  object qryOffCause: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    Parameters = <>
    Left = 22
    Top = 292
  end
  object pmProfile: TmmPopupMenu
    Images = frmMain.ImageList16x16
    Left = 576
    Top = 104
    object miEditGridOptions: TMenuItem
      Action = acEditGridOptions
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object miLoadProfile: TMenuItem
      Action = acLoadProfile
    end
    object miSaveProfile: TMenuItem
      Action = acSaveProfile
    end
    object miDeleteProfile: TMenuItem
      Action = acDeleteProfile
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object miSaveAsDefault: TMenuItem
      Action = acSaveAsDefault
    end
    object N3: TMenuItem
      Caption = '-'
      Visible = False
    end
    object miSetOfflimitCause: TMenuItem
      Action = acSetOfflimitCause
    end
    object miDataPresents: TMenuItem
      Action = acToggleGridChart
      Visible = False
    end
    object miShowPie: TMenuItem
      Action = acShowPie
      Visible = False
    end
    object miSort: TMenuItem
      Action = acSort
      object miSortMachine2: TMenuItem
        Action = acSortMachine
        Checked = True
        Default = True
        GroupIndex = 2
        RadioItem = True
      end
      object miSortMachineError2: TMenuItem
        Action = acSortMachineError
        GroupIndex = 2
        RadioItem = True
      end
      object miSortError2: TMenuItem
        Action = acSortError
        GroupIndex = 2
        RadioItem = True
      end
    end
    object miResetSpindle: TMenuItem
      Action = acResetSpindle
      Visible = False
    end
    object miResetWithFilter: TMenuItem
      Action = acResetWithFilter
    end
  end
  object pmStatistic: TmmPopupMenu
    Left = 488
    Top = 104
    object miSortMachine1: TMenuItem
      Action = acSortMachine
      Checked = True
      Default = True
      GroupIndex = 2
      RadioItem = True
    end
    object miSortMachineError1: TMenuItem
      Action = acSortMachineError
      GroupIndex = 2
      RadioItem = True
    end
    object miSortError1: TMenuItem
      Action = acSortError
      GroupIndex = 2
      RadioItem = True
    end
  end
  object qWork: TmmADODataSet
    Parameters = <>
    Left = 246
    Top = 93
  end
  object pmReset: TmmPopupMenu
    Left = 536
    Top = 104
    object miResetSpindle2: TMenuItem
      Action = acResetSpindle
    end
    object miResetWithFilter2: TMenuItem
      Action = acResetWithFilter
    end
  end
  object tabOfflimits: TmmMemTable
    ReadOnly = False
    DataOrigin = doInternal
    Left = 24
    Top = 336
    object tabOfflimitsc_ID: TIntegerField
      DisplayLabel = '0'
      FieldName = 'c_ID'
      Visible = False
    end
    object tabOfflimitsc_position: TStringField
      Alignment = taRightJustify
      DisplayLabel = '(20)Maschine: %s - %d'
      DisplayWidth = 35
      FieldName = 'c_position'
      Size = 50
    end
    object tabOfflimitsc_YB: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_YB'
      DisplayWidth = 15
      FieldName = 'c_YB'
    end
    object tabOfflimitsc_Eff: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_Eff'
      DisplayWidth = 15
      FieldName = 'c_Eff'
    end
    object tabOfflimitsc_CSp: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_CSp'
      DisplayWidth = 15
      FieldName = 'c_CSp'
    end
    object tabOfflimitsc_RSp: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_RSp'
      DisplayWidth = 15
      FieldName = 'c_RSp'
    end
    object tabOfflimitsc_CBu: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_CBu'
      DisplayWidth = 15
      FieldName = 'c_CBu'
    end
    object tabOfflimitsc_CUpY: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_CUpY'
      DisplayWidth = 15
      FieldName = 'c_CUpY'
    end
    object tabOfflimitsc_tLSt: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_tLSt'
      DisplayWidth = 15
      FieldName = 'c_tLSt'
    end
    object tabOfflimitsc_LSt: TStringField
      Alignment = taCenter
      DisplayLabel = '(30)c_LSt'
      DisplayWidth = 15
      FieldName = 'c_LSt'
    end
    object tabOfflimitsc_CSIRO: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_CSIRO'
      DisplayWidth = 15
      FieldName = 'c_CSIRO'
    end
    object tabOfflimitsc_CYTot: TStringField
      Alignment = taCenter
      DisplayLabel = '(40)c_CYTot'
      DisplayWidth = 15
      FieldName = 'c_CYTot'
    end
  end
  object dsOfflimits: TwwDataSource
    DataSet = tabOfflimits
    Left = 64
    Top = 336
  end
  object mmGridPos3: TmmGridPos
    MemoINI.AdoConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=wetsrvmm;Initial Catalog=MM_Wind' +
      'ing;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    AlterColors = True
    AlterColumnFormat = True
    AlterColumnPosition = True
    AlterGridOptions = True
    AlterFixedCols = True
    AlterFonts = True
    AlterVisibleCols = True
    Grid = grOfflimits
    LookupID = 'OFFLIMIT_INFO'
    Left = 102
    Top = 340
  end
  object ilCross: TmmImageList
    Left = 86
    Top = 100
    Bitmap = {
      494C010105000900040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008400000084000000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008400000084
      0000008400000084000000840000008400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400008400000084
      0000000000000000000000840000008400000084000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008484840000840000000000000000
      0000000000000000000000000000008400000084000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008400000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000084000000840000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000840000008400000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000084
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400008400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000084000000840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000000000000000000000000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      FF000000FF0000000000000000000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      00000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000000000000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF00000000000000000000000000000000000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF0000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000FF000000FF000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF00000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000FF00000000000000FF000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      FF000000FF000000FF000000FF000000000000000000000000000000FF000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF000000000000000000000000000000FF000000
      FF000000FF000000000000000000000000000000000000000000000000000000
      00000000FF000000FF000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000FF000000FF000000
      FF00000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000FF000000FF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000FFFF000000000000
      F9FF000000000000F0FF000000000000F0FF000000000000E07F000000000000
      C07F000000000000843F0000000000001E3F000000000000FE1F000000000000
      FF1F000000000000FF8F000000000000FFC7000000000000FFE3000000000000
      FFF8000000000000FFFF000000000000FFFFFFFFFFFFFFFFFFFFFFFFDFFBBFF9
      FFFFFFFFCFF3CFFBFFFFFFFFE7E7C7F3FFFFF7EFF3CFE3E7FFFFFBDFFB9FF1C7
      FFFFF93FF93FF18FFFFFFC7FFC7FF81FFFFFFE7FFC7FFC3FFFFFFD3FF83FFC3F
      FFFFF99FF19FF81FFFFFF3CFE3CFF087FFFFFFFFCFE3E1C3FFFFFFFF9FF1C7F1
      FFFFFFFFFFFF8FFDFFFFFFFFFFFF3FFF00000000000000000000000000000000
      000000000000}
  end
end
