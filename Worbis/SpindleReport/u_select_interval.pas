(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_select_interval.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Auswahl-Dialog fuer Produktions-Zeitraum
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.04.99 | 0.00 | PW  | Datei erstellt
| 31.01.00 | 1.00 | Mg  | Where in TSelectIntervalDlg.SetPeriod inserted
| 18.10.02 |      | LOK | Umbau ADO
|=========================================================================================*)

{$i symbols.inc}

unit u_select_interval;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 IvDictio,IvMulti,IvEMulti,ActnList,mmActionList,ComCtrls,ToolWin,mmToolBar,
 ExtCtrls,mmPanel,mmDateTimePicker,Db,StdCtrls,mmUpDown,
 ShiftCalendars,
 mmEdit,mmButton,mmLabel,mmGroupBox,u_global,Mask,Nwcombo,
 BASEFORM,Grids, mmTranslator, ADODB, mmADODataSet, u_dmSpindleReport;

type
 TSelectIntervalDlg = class(TmmForm)

  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  mmTranslator1: TmmTranslator;
    quInterval: TmmADODataSet;

  mmPanel1: TmmPanel;
   Panel1: TmmPanel;
   lbIntervals: TNWListBox;
   Panel2: TmmPanel;
   laFrom: TmmLabel;
   laTo: TmmLabel;
   laDuration: TmmLabel;
   paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure lbIntervalsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
  procedure FormClose(Sender: TObject; var Action: TCloseAction);
  procedure acHelpExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
   FPeriod: rPeriodInfoT;
  public
   procedure SetPeriod(value: rPeriodInfoT);
   function GetPeriod: rPeriodInfoT;
 end; //SelectPeriodDlg

var
 SelectIntervalDlg: TSelectIntervalDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,

 u_main,u_common_lib,u_common_global;

{$R *.DFM}

resourcestring
 cMsgStr1 = '(*)Zeitraum-Begin > Zeitraum-Ende ?'; //ivlm

procedure TSelectIntervalDlg.SetPeriod(value: rPeriodInfoT);
var
 xStr         : string;
 xIntDuration : tdatetime;

begin
 FPeriod := value;

 with quInterval do
 begin
  try
   CommandText := 'SELECT * FROM t_interval WHERE c_interval_end < getDate() ORDER BY c_interval_start DESC';
   Open;

   while not Eof do
   begin
    xIntDuration := FieldByName('c_interval_end').asdatetime -FieldByName('c_interval_start').asdatetime;
    xStr := format('%s|%s|%s',[formatdatetime('dd/mm/yyyy  (hh:nn)',FieldByName('c_interval_start').asdatetime),
                               formatdatetime('dd/mm/yyyy  (hh:nn)',FieldByName('c_interval_end').asdatetime),
                               formatdatetime('hh:nn',xIntDuration)]);
    lbIntervals.AddObj(xStr,FieldByName('c_interval_id').value);
    Next;
   end; //while not Eof do
  finally
   acOK.Enabled := lbIntervals.Items.Count > 0;
   if acOK.Enabled then lbIntervals.ItemIndex := 0;
   lbIntervals.Selected[0] := true;
   Active := false;
  end; //try..finally
 end; //with quInterval do
end; //procedure TSelectIntervalDlg.Init
//-----------------------------------------------------------------------------
function TSelectIntervalDlg.GetPeriod: rPeriodInfoT;
var
 i : integer;

begin
 result.pEndID := 0;
 result.pBeginID := 0;

 for i := 0 to lbIntervals.Items.Count -1 do
  if lbIntervals.Selected[i] then
  begin
   if result.pEndID = 0 then result.pEndID := lbIntervals.GetItemIDAtPos(i);
   result.pBeginID := lbIntervals.GetItemIDAtPos(i);
  end; //if lbIntervals.Selected[i] then

 with quInterval do
 begin
  Active := false;
  CommandText := 'SELECT * FROM t_interval WHERE c_interval_id = :id';
  Parameters.ParamByname('id').value := result.pBeginID;
  Active := true;
  result.pBegin := FieldByName('c_interval_start').asdatetime;

  Active := false;
  Parameters.ParamByname('id').value := result.pEndID;
  Active := true;
  result.pEnd := FieldByName('c_interval_end').asdatetime;
  Active := false;
 end; //with quInterval do
end; //function TSelectIntervalDlg.GetPeriod
//-----------------------------------------------------------------------------
procedure TSelectIntervalDlg.acOKExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrOK;
end; //procedure TForm4.acOKExecute
//-----------------------------------------------------------------------------
procedure TSelectIntervalDlg.acCancelExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrCancel;
end; //procedure TForm4.acCancelExecute
//-----------------------------------------------------------------------------
procedure TSelectIntervalDlg.lbIntervalsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
 cFromPos     = 5;
 cToPos       = 155;
 cDurationPos = 305;

var
 xIsSelected  : boolean;
 xStr,
 xFromStr,
 xToStr,
 xDurationStr : string;

begin
 with (Control as TNWListBox).Canvas do
 begin
  xIsSelected := odSelected in state;
  FillRect(Rect);
  if xIsSelected then Font.Color := clHighlightText
                 else Font.Color := clWindowText;

  xStr := (Control as TNWListBox).Items[Index];
  xFromStr := GetShortHint(xStr);
  xStr := GetLongHint(xStr);
  xToStr := GetShortHint(xStr);
  xDurationStr := GetLongHint(xStr);

  TextOut(Rect.Left +cFromPos,Rect.Top,xFromStr);
  TextOut(Rect.Left +cToPos,Rect.Top,xToStr);
  if not xIsSelected then Font.Color := clNavy;
  TextOut(Rect.Left +cDurationPos,Rect.Top,xDurationStr);
 end; //with (Control as TNWListBox).Canvas do
end; //procedure TSelectIntervalDlg.lbIntervalsDrawItem
//-----------------------------------------------------------------------------
procedure TSelectIntervalDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 inherited;
 Action := caFree;
end; //procedure TSelectIntervalDlg.FormClose
//-----------------------------------------------------------------------------
procedure TSelectIntervalDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext)
end; //procedure TSelectIntervalDlg.acHelpExecute
//-----------------------------------------------------------------------------

procedure TSelectIntervalDlg.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('globale-fenster-und-funktionen\DLG_Zeitraum-auswaehlen.htm');
end;

end. //u_select_interval.pas
 
