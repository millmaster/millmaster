(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_set_offlimit_cause.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: dialog to assign offlimit cause
| Info..........: -
| Develop.system: Windows NT 5.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01, Multilizer, Infopower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 22.08.01 | 0.00 | PW  | file created
| 22.10.02 |      | LOK | Umbau ADO
|
|=========================================================================================*)

{$i symbols.inc}

unit u_set_offlimit_cause;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList,
  mmActionList, StdCtrls, mmButton, ExtCtrls, mmPanel, mmLabel,
  mmGroupBox, Db, mmComboBox, mmADODataSet, LoepfeGlobal;

type
  TfrmSetOfflimitCause = class(TmmDialog)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    mmTranslator: TmmTranslator;
    mmPanel2: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;
    acEditOfflimitCause: TAction;
    mmPanel1: TmmPanel;
    mmGroupBox1: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    laMachineName: TmmLabel;
    laSpindleID: TmmLabel;
    comCause: TmmComboBox;
    mmButton1: TmmButton;

    procedure acEditOfflimitCauseExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acOKExecute(Sender: TObject);

  private
    procedure Fill_comCause;
  public
    constructor Create(aOwner: TComponent; aMachineName: string; aSpindleID: integer); reintroduce; virtual;
    function GetCauseID: integer;
  end; //TfrmSetOfflimitCause

var
  frmSetOfflimitCause: TfrmSetOfflimitCause;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main, u_global, u_common_global, u_common_lib, u_lib, u_resourcestrings,
  u_edit_offlimit_cause;

{$R *.DFM}

constructor TfrmSetOfflimitCause.Create(aOwner: TComponent;
  aMachineName: string; aSpindleID: integer);
begin
  inherited create(aOwner);
  laMachineName.Caption := aMachineName;
  laSpindleID.Caption := inttostr(aSpindleID);
  Fill_comCause;
end; //constructor TfrmSetOfflimitCause.Create
//-----------------------------------------------------------------------------
procedure TfrmSetOfflimitCause.acEditOfflimitCauseExecute(Sender: TObject);
begin
  inherited;
  with TEditOfflimitCauseDlg.Create(Self) do
    ShowModal;
  Fill_comCause;
end; //procedure TfrmSetOfflimitCause.acEditOfflimitCauseExecute
//-----------------------------------------------------------------------------
procedure TfrmSetOfflimitCause.Fill_comCause;
const
  cQry = 'SELECT c_offcause_id, c_offcause_name '+
         'FROM t_offlimit_cause '+
         'ORDER BY c_offcause_name';

begin
  with TmmADODataSet.Create(Self) do
    try
      comCause.Items.BeginUpdate;
      comCause.Clear;

      ConnectionString := GetDefaultConnectionString;
      CommandText := cQry;
      Open;
      while not Eof do begin
        comCause.Items.AddObject(FieldByName('c_offcause_name').asstring,
                                 TObject(FieldByName('c_offcause_id').asinteger));
        Next;
      end; //while not Eof do begin
    finally
      comCause.Items.EndUpdate;
      Free;
      acOK.Enabled := comCause.Items.Count > 0;
      if acOK.Enabled then comCause.ItemIndex := 0;
    end; //try..finally
end; //procedure TfrmSetOfflimitCause.Fill_comCause
//-----------------------------------------------------------------------------
procedure TfrmSetOfflimitCause.acCancelExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end; //procedure TfrmSetOfflimitCause.acCancelExecute
//-----------------------------------------------------------------------------
procedure TfrmSetOfflimitCause.acHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, HelpContext)
end; //procedure TfrmSetOfflimitCause.acHelpExecute
//-----------------------------------------------------------------------------
procedure TfrmSetOfflimitCause.acOKExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end; //procedure TfrmSetOfflimitCause.acOKExecute
//-----------------------------------------------------------------------------
function TfrmSetOfflimitCause.GetCauseID: integer;
begin
  if ModalResult = mrOK then result := Longint(comCause.Items.Objects[comCause.ItemIndex])
                        else result := -1;
end; //function TfrmSetOfflimitCause.GetCauseID

end. //u_set_offlimit_cause
