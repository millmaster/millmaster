(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_offlimit_report.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01, Multilizer, InfoPower
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 12.04.99 | 0.00 | PW  | file created
| 09.05.99 | 0.01 | PW  | offlimit cause added
| 05.10.99 | 0.02 | PW  | mmGridpos.TMemoINI to read/write settings
| 15.10.99 | 0.03 | PW  | TProgOptions.MemoINI to read/write settings
| 21.03.00 | 0.04 | SDo | Proc Getcauses added and TwwDBComboDLG to
|                       | comEnterCause.TwwDBCombobox changed
| 22.03.00 |      | SDo | RedLight ausgeschaltet -> // @@RedL
                          Error DIV / 0 ->// @@ErrorDIV0
| 23.03.00 |      | SDo | Uebrarbeitung der Statistik (Folder),
|                       | Error Div / 0 behoben in Fill_sgOffCause und
|                       | comEnterCauseExit
| 10.04.00 |      | SDo | Neuer Settings-Dialog (TmSettingsDialog) implementiert
| 26.04.00 |      | SDo | TGridPos nicht loeschen -> acSaveProfileExecute(),
|                       | acLoadProfileExecute,
|  5.05.00 |      | PW  | Read and Write of graph-settings with new TmSettingsDialog
| 09.05.00 |      | Mg  | cMaxNumberOfHits = 10
| 25.05.00 |      | Wss | Offlimit-Timefilter changed to ">="
| 27.07.00 | 1.00 | Mg  | Refresh of Offcause combobox after new Offlimit cause                      |
| 08.08.00 | 1.01 | Mg  | Constructor deleted
| 23.08.00 | 1.02 | Mg  | quOnlineCalcFields inserted
| 30.08.00 | 1.03 | Mg  | TOffReportDlg.PageControl1Change : Clear of grafik deleted
| 10.10.00 | 1.04 | Mg  | Bug Fixes
| 06.07.01 | 1.04 | Wss | last col in stringgrid sgOffCause removed
| 16.08.01 | 1.05 | PW  | - time format changed from HH:MM to H
                          - default sort order changed from soAscending to soDescending
                            on first click on title button. move cursor on first record after
                            clicking title button
                          - new spindle reset dialog implemented: => TfrmDefResetFilter
                          - show number of online/offline winding positions
                          - y-axis data type of "offlimit-verteilung" changed from minutes to hours
                          - Offlimit stringgrid replaced by wwDBGrid, columns of grOfflimits
                            and wwDBGrid1 are linked
                          - Offlimit cause analysis modified: offlimit occurrence and pattern match
| 22.08.01 | 1.06 | PW  | - set offlimit cause through separate form
                          - comEnterCause removed
                          - EnableActions and PageControl1Change modified
                          - new calc. fields "calc_Offcause 1-3" showing the most likely offlimit causes
                          - printing grids using wwQRPrintDBGrid component
| 29.08.01 | 1.07 | Wss | - FormCreate removed: use Create OR FormCreate, but not both!!
                          - TextToTranslate code moved to After
                          - some translations handling changed -> call of GetFieldNames removed
                          - obsolete components for Offline offlimit removed (TNotebook, chart, panels)
| 31.08.01 | 1.08 | pw  | - Offlimit cause analysis modified:
                          - consider "spindle out of production" => c_op_offcnt
| 11.09.01 | 1.09 | Wss | OnlineOfflimitTime changed to display in hours instead of minutes
| 22.11.01 | 1.10 | Wss | MemoryLeak: object mParameters was overriden at reading parameters
                          mParameters := TParameters.Create;
                          mParameters := ReadParam(); <-- forbidden !!!!
| 29.01.02 | 1.11 | Wss | Fehler bei Filterung der Kollonnen in GetResetQuery behoben. Wenn Offline
                          Spindel geloescht wurde, so wurde auch das Feld c_max_online_offlimit_time
                          geloescht. Filterung der Kollonnen war '_OFFLIMIT_' und somit wurde dieses
                          Feld ebenfalls mitgenommen.
| 02.04.02 | 1.11 | Wss | - Beim aufstarten konnte es vorkommen, dass die untere Offlimituebersichttabelle
                            nicht angezeigt wurde. In FormShow wird nun Update_grOfflimits aufgerufen
| 17.10.02 |      | LOK | Umbau ADO
| 29.10.02 | 1.11 | Wss | tLst auf TFloatField ge�ndert und DisplayFormat = '0' gesetzt (BeforeOpen Event)
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 25.11.2002        Wss | Grids nur noch mit ganzer Zeile Markierung, da mit Sortierung sonst Probleme
| 30.06.2003        Wss | Offlimit Zeichen auf '*' ge�ndert, da '#' vielfach als 'Anzahl' interpretiert wird
| 03.10.2003        Wss | Statistik: Doppelklick in Grid oder Chart zeigt nun auch Kuchengrafik
| 18.02.2004        Wss | Fehlende �bersetzungen korrigiert
| 19.02.2004        Wss | Filtrierung nach Machinen wenn von Floor aufgestartet wird -> Init(), RunQuery()
| 07.07.2006        Wss | In SortBars() fehlte noch ein String Parameter in Format()
|=============================================================================*)

{$I symbols.inc}

unit u_offlimit_report;

interface

uses
  u_global,
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms,
  Dialogs, Grids, StdCtrls, DB, Buttons, ExtCtrls, Nwcombo, ComCtrls,
  ToolWin, Menus, ActnList, mmMainMenu, mmActionList, mmPopupMenu, mmToolBar,
  mmPanel, ImgList, IvDictio, IvMulti, IvEMulti,
  Wwdbigrd, ShiftCalendars, Wwdbgrid, Wwdatsrc, NumCtrl, mmLengthUnit,
  mmGraphGlobal, mmGridPos, mmLabel, TeEngine, Series, TeeProcs, Chart, Tabs,
  mmGraph, Mask, wwdbedit, Wwdotdot, mmTranslator, MMSecurity,
  mmStringGrid, mmPageControl, AdvGrid, mmChart, mmSpeedButton, Wwdbcomb,
  DBGrids, mmDataSource, mmListBox, mmRadioGroup, u_StatisticPie,
  u_SettingsDialog, u_def_reset_filter, mmMemTable, u_print_list,
  BASEFORM, u_dmSpindleReport, ADODB, mmADOCommand, mmADODataSet,
  LoepfeGlobal, mmImageList, mmVCLStringList;

type
  TOffReportDlg = class(TmmForm)
    ToolBar1: TmmToolBar;
    bClose: TToolButton;
    ToolButton2: TToolButton;
    bPrint: TToolButton;
    bHelp: TToolButton;
    bOptions: TToolButton;
    bFilter: TToolButton;
    bEditOfflimitCause: TToolButton;
    bShowInfoWindow: TToolButton;
    bEditGridOptions: TToolButton;
    ToolButton4: TToolButton;

    ActionList1: TmmActionList;
    acClose: TAction;
    acEditOfflimits: TAction;
    acHelp: TAction;
    acPrint: TAction;
    acFilter: TAction;
    acEditOfflimitCause: TAction;
    acShowHideInfoWindow: TAction;

    pmProfile: TmmPopupMenu;
    miEditGridOptions: TMenuItem;
    N2: TMenuItem;
    miLoadProfile: TMenuItem;
    miSaveProfile: TMenuItem;
    miDeleteProfile: TMenuItem;
    N1: TMenuItem;
    miSaveAsDefault: TMenuItem;

    MainPanel: TmmPanel;
    PageControl1: TmmPageControl;
    tsOnline: TTabSheet;
    tsStatistic: TTabSheet;
    wwDBGrid1: TwwDBGrid;
    Panel1: TmmPanel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    laMinOnlineDuration: TmmLabel;
    laSpindleStatus: TmmLabel;
    Panel2: TmmPanel;
    mmLabel4: TmmLabel;
    laMinOfflineDuration: TmmLabel;
    mmLabel3: TmmLabel;
    laOfflineRatio: TmmLabel;

    mmGridPos1: TmmGridPos;
    mmGridPos2: TmmGridPos;

    scrOffline: TwwDataSource;
    quOffline: TmmADODataSet;
    quOfflinec_machine_name: TStringField;
    quOfflinec_spindle_id: TSmallintField;
    quOfflinec_offline_offlimit_time: TIntegerField;
    quOfflinec_offline_watchtime: TIntegerField;
    quOfflinecalc_OfflineRatio: TFloatField;
    quOfflinec_YB_offline_time: TIntegerField;
    quOfflinec_RedL_offline_time: TIntegerField;
    quOfflinec_Eff_offline_time: TIntegerField;
    quOfflinec_CSp_offline_time: TIntegerField;
    quOfflinec_RSp_offline_time: TIntegerField;
    quOfflinec_CBu_offline_time: TIntegerField;
    quOfflinec_CUpY_offline_time: TIntegerField;
    quOfflinec_tLSt_offline_time: TIntegerField;
    quOfflinec_LSt_offline_time: TIntegerField;
    quOfflinec_CSIRO_offline_time: TIntegerField;
    quOfflinec_CYTot_offline_time: TIntegerField;
    quOfflinec_OutOfProduction_offline_time: TIntegerField;
    quOfflinec_machine_id: TSmallintField;

    srcOnline: TwwDataSource;
    quOnline: TmmADODataSet;
    quOnlinec_machine_name: TStringField;
    quOnlinec_spindle_id: TSmallintField;
    quOnlinec_update_time: TDateTimeField;
    quOnlinec_max_online_offlimit_time: TIntegerField;
    quOnlinec_Eff: TIntegerField;
    quOnlinec_LSt: TIntegerField;
    quOnlinec_OutOfProduction: TBooleanField;
    quOnlinec_OutOfProduction_online_time: TIntegerField;
    quOnlinec_maoffaverage_id: TIntegerField;
    quOnlinec_machine_id: TSmallintField;
    quOnlinec_maoffset_id: TIntegerField;
    quOnlinec_Eff_AVG: TIntegerField;
    quOnlinec_tLSt_AVG: TIntegerField;
    quOnlinec_LSt_AVG: TIntegerField;
    quOnlinec_Average_time: TIntegerField;
    quOnlinec_OffBorderByAverage: TBooleanField;
    quOnlinec_OffBorderByFixValues: TBooleanField;
    quOnlinec_Eff_PC_max: TIntegerField;
    quOnlinec_Eff_PC_min: TIntegerField;
    quOnlinec_Eff_max: TIntegerField;
    quOnlinec_Eff_min: TIntegerField;
    quOnlinec_CYTot_PC_max: TIntegerField;
    quOnlinec_CYTot_PC_min: TIntegerField;
    quOnlinec_CYTot_max: TFloatField;
    quOnlinec_CYTot_min: TFloatField;
    quOnlinec_CSIRO_PC_max: TIntegerField;
    quOnlinec_CSIRO_max: TFloatField;
    quOnlinec_CSp_PC_max: TIntegerField;
    quOnlinec_CSp_max: TFloatField;
    quOnlinec_RSp_PC_max: TIntegerField;
    quOnlinec_RSp_max: TFloatField;
    quOnlinec_YB_PC_max: TIntegerField;
    quOnlinec_YB_Max: TFloatField;
    quOnlinec_CBu_PC_max: TIntegerField;
    quOnlinec_CBu_max: TFloatField;
    quOnlinec_CUpY_PC_max: TIntegerField;
    quOnlinec_CUpY_max: TFloatField;
    quOnlinec_tLSt_PC_max: TIntegerField;
    quOnlinec_tLSt_max: TIntegerField;
    quOnlinec_LSt_PC_max: TIntegerField;
    quOnlinec_LSt_max: TIntegerField;
    quOnlinec_YB_online_time: TIntegerField;
    quOnlinec_Eff_online_time: TIntegerField;
    quOnlinec_CSp_online_time: TIntegerField;
    quOnlinec_RSp_online_time: TIntegerField;
    quOnlinec_CBu_online_time: TIntegerField;
    quOnlinec_CUpY_online_time: TIntegerField;
    quOnlinec_tLSt_online_time: TIntegerField;
    quOnlinec_LSt_online_time: TIntegerField;
    quOnlinec_CSIRO_online_time: TIntegerField;
    quOnlinec_CYTot_online_time: TIntegerField;
    quOnlinecalc_OutOfProduction: TIntegerField;
    quOnlinecalc_Offcause1: TStringField;
    quOnlinecalc_Offcause2: TStringField;
    quOnlinecalc_Offcause3: TStringField;

    dsOfflimits: TwwDataSource;
    tabOfflimits: TmmMemTable;
    tabOfflimitsc_ID: TIntegerField;
    tabOfflimitsc_position: TStringField;
    tabOfflimitsc_YB: TStringField;
    tabOfflimitsc_Eff: TStringField;
    tabOfflimitsc_CSp: TStringField;
    tabOfflimitsc_RSp: TStringField;
    tabOfflimitsc_CBu: TStringField;
    tabOfflimitsc_CUpY: TStringField;
    tabOfflimitsc_tLSt: TStringField;
    tabOfflimitsc_LSt: TStringField;
    tabOfflimitsc_CSIRO: TStringField;
    tabOfflimitsc_CYTot: TStringField;
    quProdID: TmmADODataSet;
    quUpdate: TmmADOCommand;
    qryOffCause: TmmADODataSet;
    paOnlineInfo: TmmPanel;
    tabsOnline: TTabSet;
    nbOnline: TNotebook;
    Chart1: TmmChart;
    Series1: TBarSeries;
    sgOffCause: TStringGrid;
    MMSecurityControl: TMMSecurityControl;
    bSecurity: TmmSpeedButton;
    acSecurity: TAction;
    ToolButton3: TToolButton;
    acResetSpindle: TAction;
    acToggleGridChart: TAction;
    pnStatisticGrid: TmmPanel;
    wwDBGrid2: TwwDBGrid;
    mmToolBar1: TmmToolBar;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    bSort: TToolButton;
    pmStatistic: TmmPopupMenu;
    miSortMachine1: TMenuItem;
    miSortMachineError1: TMenuItem;
    tbPie: TToolButton;
    qWork: TmmADODataSet;
    acSortMachine: TAction;
    acSortMachineError: TAction;
    acShowPie: TAction;
    acSort: TAction;
    N3: TMenuItem;
    miDataPresents: TMenuItem;
    miShowPie: TMenuItem;
    miSort: TMenuItem;
    miSortMachine2: TMenuItem;
    miSortMachineError2: TMenuItem;
    miResetSpindle: TMenuItem;
    ToolButton9: TToolButton;
    ToolButton1: TToolButton;
    miSortError2: TMenuItem;
    acSortError: TAction;
    miSortError1: TMenuItem;
    ToolButton8: TToolButton;
    acResetWithFilter: TAction;
    miResetWithFilter: TMenuItem;
    pmReset: TmmPopupMenu;
    miResetSpindle2: TMenuItem;
    miResetWithFilter2: TMenuItem;
    mmLabel5: TmmLabel;
    laCountOffline: TmmLabel;
    mmLabel6: TmmLabel;
    laCountOnline: TmmLabel;
    grOfflimits: TwwDBGrid;
    mmGridPos3: TmmGridPos;
    acDefPatternRecognition: TAction;
    bPatternRecognition: TToolButton;
    ToolButton11: TToolButton;
    acSetOfflimitCause: TAction;
    bSetOffCause: TToolButton;
    miSetOfflimitCause: TMenuItem;
    mmTranslator: TmmTranslator;
    pnStatisticChart: TmmPanel;
    mmGraph1: TmmGraph;
    ilCross: TmmImageList;
    quOnlinec_tLSt: TIntegerField;
    acEditGridOptions: TAction;
    acLoadProfile: TAction;
    acSaveProfile: TAction;
    acDeleteProfile: TAction;
    acSaveAsDefault: TAction;
    ToolButton7: TToolButton;
    ToolButton10: TToolButton;
    quOnlinec_base: TIntegerField;
    quOnlinec_YB: TFloatField;
    quOnlinec_CSp: TFloatField;
    quOnlinec_RSp: TFloatField;
    quOnlinec_CBu: TFloatField;
    quOnlinec_CUpY: TFloatField;
    quOnlinec_CSIRO: TFloatField;
    quOnlinec_CYTot: TFloatField;
    quOnlinec_YB_AVG: TFloatField;
    quOnlinec_CSp_AVG: TFloatField;
    quOnlinec_RSp_AVG: TFloatField;
    quOnlinec_CBu_AVG: TFloatField;
    quOnlinec_CUpY_AVG: TFloatField;
    quOnlinec_CSIRO_AVG: TFloatField;
    quOnlinec_CYTot_AVG: TFloatField;

    procedure acCloseExecute(Sender: TObject);
    procedure acDeleteProfileExecute(Sender: TObject);
    procedure acEditGridOptionsExecute(Sender: TObject);
    procedure acEditOfflimitCauseExecute(Sender: TObject);
    procedure acEditOfflimitsExecute(Sender: TObject);
    procedure acFilterExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure acLoadProfileExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acSaveAsDefaultExecute(Sender: TObject);
    procedure acSaveProfileExecute(Sender: TObject);
    procedure acShowHideInfoWindowExecute(Sender: TObject);
    procedure FormatTo_hh_mm(Sender: TField; var Text: string; DisplayText: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure quOfflineAfterScroll(DataSet: TDataSet);
    procedure quOnlineAfterScroll(DataSet: TDataSet);
    procedure sgOffCauseDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure wwDBGrid1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure wwDBGrid1DblClick(Sender: TObject);
    procedure wwDBGrid1TitleButtonClick(Sender: TObject; AFieldName: string);
    procedure tabsOnlineClick(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acToggleGridChartExecute(Sender: TObject); // Sdo
    procedure mmGraph11GraphClick(Sender: TObject; Button: TMouseButton; // Sdo
      Shift: TShiftState; ARow, APoint: Integer);
    procedure acShowPieExecute(Sender: TObject); // Sdo
    procedure acResetSpindleExecute(Sender: TObject); // Sdo
    procedure acSortMachineExecute(Sender: TObject); // Sdo
    procedure bSortClick(Sender: TObject); // Sdo
    procedure wwDBGrid2RowChanged(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure quOnlineCalcFields(DataSet: TDataSet);
    procedure acResetWithFilterExecute(Sender: TObject);
    procedure quOfflineAfterOpen(DataSet: TDataSet);
    procedure quOnlineAfterOpen(DataSet: TDataSet);
    procedure grOfflimitsCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure wwDBGrid1ColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure wwDBGrid1ColWidthChanged(Sender: TObject; Column: Integer);
    procedure acDefPatternRecognitionExecute(Sender: TObject);
    procedure acSetOfflimitCauseExecute(Sender: TObject);
    procedure OnGetOnlineText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure mmTranslatorAfterTranslate(translator: TIvCustomTranslator);
    procedure acSortExecute(Sender: TObject);
    procedure quOnlineBeforeOpen(DataSet: TDataSet);
    procedure wwDBGrid2DblClick(Sender: TObject);
    procedure mmGraph1GraphDblClick(Sender: TObject; AItem: eDblClickT;
      ARow, APoint: Integer; AStatistik: eStatistikT);
  private
    mCreating:boolean;       // LOK True w�rend Create
    mFloorMachines: String;       // F�r das Filtrieren per Machine wenn von Floor gestartet
    mOnlineSortOrder: string;
    mOfflineSortOrder: string;
    mSortOrder: eSortOrderT;
    mOfflimits: TOfflimits;
    mProgOptions: TProgOptions;
    mShowStatisticChart: Boolean; // SDo
    mStatisticPie: TfStatisticPie; // SDo
    mParaList: rEspParameterArrayT; // SDo
    mParameter: TParameterList; // SDO
    mOfflimitCauses: TOfflimitCauses;
    procedure EnableActions;
    procedure Fill_sgOffCause;
    procedure Fill_grOfflimits;
    procedure GetFieldNames;
    procedure GetOfflimitAllSpindles;
    procedure GetOnlineChartData;
    procedure Init_StringGrids;
    procedure LoadProfile(aUsername, aProfilename: string);
    procedure ReadOnlineOfflimits(aOfflimits: TOfflimits);
    procedure RunQuery(aSavePos: boolean);
    procedure ShowStatisticGraphic; // Sdo
    procedure ShowPieValues; //(aStatisticPie: TfStatisticPie); // Sdo
    procedure SortBars(aAction: TAction); // Sdo
    procedure SortCauses(var aData: array of rOffCausesT; iLo, iHi: integer);
    procedure UpdateCaptions;
    procedure UpdateInfoWindows;
    function ShowDialog(aGrid: TwwDBGrid; aShowTabGrid: boolean = true): Boolean; // SDo
    procedure PrepareOfflineReport(aGrid: TwwDBGrid); // SDo
    procedure PrepareOnlineReport(aGrid: TwwDBGrid); // SDo
    function GetResetQuery: string;
    procedure Init_tabOfflimits;
    procedure Translate_tabOfflimits;
    procedure Update_grOfflimits;
    procedure AssignOfflimitCause(aCauseID: integer);
//    function GetQuOnlineQuery: string;
//    function GetQuOfflineQuery: string;
  public
    constructor Create(aOwner: TComponent); override;
    procedure Init(aMachines: string = '');
    procedure WndProc(var Message: TMessage); override; // SDo
  end; //TOffReportDlg

const
  cPieFormClose = WM_USER + 1;
  cPieToggle = WM_USER + 2;
  cNoDataAvailable = '(15)keine Daten'; //ivlm

var
  OffReportDlg: TOffReportDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, MMHtmlHelp, SettingsReader,
  printers, u_resourcestrings, MemoINI, mmLib, u_common_lib,
  u_common_global, u_main, u_lib, u_offlimit_settings_editor, MMUGlobal,
  u_def_offlimit_filter, u_esp_spindle_report, u_edit_offlimit_cause,
  mmDialogs, u_def_pattern_recognition, u_set_offlimit_cause ;

{$R *.DFM}

resourcestring
  cShowAllSpindles = '(40)alle Spulstellen'; //ivlm
  cHideOutOfProdpindles = '(40)stehende Spulstellen ausblenden'; //ivlm
  cLowerFixedStr = '(40)Unterer Grenzwert (absolut)'; //ivlm
  cUpperFixedStr = '(40)Oberer Grenzwert (absolut)'; //ivlm
  cLowerRelativeStr = '(40)Unterer Grenzwert (prozent.)'; //ivlm
  cUpperRelativeStr = '(40)Oberer Grenzwert (prozent.)'; //ivlm
  cSpindelDataStr = '(40)Spulstellendaten pro %s'; //ivlm
//  cSpindelDataStr = '(40)Spulstellendaten pro %d km'; //ivl
  cMeanDataStr = '(40)Mittelwerte ueber %d min'; //ivlm
  cMachineSpdInfoStr = '(20)Maschine: %s - %d'; //ivlm
  cRuntimeStr = '(25)Laufzeit'; //ivlm
  cOfflimitStr = '(25)Offlimit'; //ivlm
  cOffcauseNameStr = '(30)Offlimit-Ursachen'; //ivlm
  cOffcauseCountStr = '(15)Haeufigkeit'; //ivlm
  cHitRatioStr = '(20)Vorhersage%'; //ivlm
  cSelectTitleStr = '(30)Ursache auswaehlen'; //ivlm
  cShowGraphicHint = '(*)Zeigt die Statistik als Grafik'; //ivlm
  cShowTabHint = '(*)Zeigt die Statistik als Tabelle'; //ivlm
  cShowGraphicCaption = '(*)Grafik'; //ivlm
  cShowTabelleCaption = '(*)Tabelle'; //ivlm
  cResetSpindle = '(*)Wollen Sie die Spindel %s wirklich loeschen ?'; //ivlm
  cResetFilter = '(*)Wollen Sie die Spindeln wirklich loeschen ?'; //ivlm
  cPieOn = '(*)Kuchengrafik ein'; //ivlm
  cPieOff = '(*)Kuchengrafik aus'; //ivlm

  TabOptionsCaption = '(*)Tabellen-Eigenschaften'; //ivlm
  TabOptionsHint = '(*)Tabellen-Eigenschaften bearbeiten'; //ivlm
  GraphOptionsCaption = '(*)Graphik-Optionen...'; //ivlm
  GraphOptionsHint = '(*)Graphik-Optionen...'; //ivlm

const
  cDefaultSortOrder = 'm.c_machine_name, mo.c_spindle_id';
//-----------------------------------------------------------------------------
procedure TOffReportDlg.acCloseExecute(Sender: TObject);
begin
  Close;
end; //procedure TOffReportDlg.ActionCloseExecute
//-----------------------------------------------------------------------------
procedure TOffReportDlg.acDeleteProfileExecute(Sender: TObject);
begin
  mmGridPos1.MemoINI.DeleteProfileDlg;
end; //procedure TOffReportDlg.acDeleteProfileExecute
//-----------------------------------------------------------------------------
procedure TOffReportDlg.acEditGridOptionsExecute(Sender: TObject);
begin
  acEditGridOptions.Caption := TabOptionsCaption;
  acEditGridOptions.Hint := TabOptionsHint;

  if PageControl1.ActivePage = tsOnline then begin
    PrepareOnlineReport(wwDBGrid1);
    if ShowDialog(wwDBGrid1, false) then begin
      acSort.Enabled := False;
      Update_grOfflimits;
    end;
  end else begin
    // Statistikgrafik Mode
    mParameter.Clear;
    PrepareOfflineReport(wwDBGrid2);

    if ShowDialog(wwDBGrid2) then begin
      acSort.Enabled := (mmGraph1.Data.NumSeries > 1);
      if acSort.Enabled then begin
        if acSortMachine.Checked then
          acSortMachine.Execute
        else if acSortError.Checked and acSortError.Enabled then
          acSortError.Execute
        else if acSortMachineError.Checked then
          acSortMachineError.Execute;
      end;
    end;
  end;
end; //procedure TOffReportDlg.acEditGridOptionsExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acEditOfflimitCauseExecute(Sender: TObject);
begin
  with TEditOfflimitCauseDlg.Create(Self) do
    ShowModal;

  mOfflimitCauses.ReadOfflimitCauses;
  RunQuery(True);
end; //procedure TOffReportDlg.acEditOfflimitCauseExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acEditOfflimitsExecute(Sender: TObject);
begin
  with TOffLimitEditorDlg.Create(Self) do
  try
    Init(quOnlinec_maoffset_id.AsInteger);
    if ShowModal = mrOK then
      RunQuery(False);
  finally
    Free;
  end; //with TOffLimitEditorDlg.Create(Self) do
end; //procedure TOffReportDlg.acEditOfflimitsExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acFilterExecute(Sender: TObject);
var
  xOk: boolean;
begin
  try
  //Wss  Application.CreateForm(TDefOfflimitReportDlg,DefOfflimitReportDlg);
    DefOfflimitReportDlg := TDefOfflimitReportDlg.Create(Self);
    if PageControl1.ActivePage = tsOnline then
      DefOfflimitReportDlg.InitOnline(mProgOptions.OnlineTimeLimit, mProgOptions.OnlineShowOutOfProdSpindles)
    else
      DefOfflimitReportDlg.InitOffline(mProgOptions.OfflineTimeLimit, mProgOptions.OfflineRatio);

    xOk := (DefOfflimitReportDlg.ShowModal = mrOK);
    if xOK then begin
      if PageControl1.ActivePage = tsOnline then begin
        mProgOptions.OnlineTimeLimit := DefOfflimitReportDlg.edOnlineOfflimitMin.AsInteger * 60;
        mProgOptions.OnlineShowOutOfProdSpindles := DefOfflimitReportDlg.cbOutOfProdSpindles.Checked;
      end
      else begin
        mProgOptions.OfflineTimeLimit := DefOfflimitReportDlg.edOfflineOfflimitMin.AsInteger * 60;
        mProgOptions.OfflineRatio := DefOfflimitReportDlg.edOfflimitRatio.Value;
      end; //if PageControl1.ActivePage = tsOnline then
    end; //if xOK then
  finally
    DefOfflimitReportDlg.Free;
  end; //try..finaly

  UpdateCaptions;
  if xOK then
    RunQuery(True);
end; //procedure TOffReportDlg.acFilterExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, PageControl1.ActivePage.HelpContext);
end; //procedure TOffReportDlg.acHelpContentExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acLoadProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;
begin
  xRes := mmGridpos1.MemoINI.SelectProfile;
  if xRes.Profilename <> '' then
    LoadProfile(xRes.Username, xRes.Profilename);
end; //procedure TOffReportDlg.acLoadProfileExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acPrintExecute(Sender: TObject);
var
  xCursor: TCursor;
  xBlackWhitePrint : Boolean;
begin
  if not GetPrintOptions(poLandscape, Self, xBlackWhitePrint) then exit;

  xCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  dsOfflimits.DataSet.DisableControls;

  if PageControl1.ActivePage = tsOnline then begin
    with TfrmPrintList.Create(Self, quOnline, wwDBGrid1, Caption) do try
      Execute;
    finally
      Free;
    end; //try..finally
  end //if PageControl1.ActivePage = tsOnline then begin
  else begin
    if not mShowStatisticChart then begin
      with TfrmPrintList.Create(Self, quOffline, wwDBGrid2, Caption) do try
        Execute;
      finally
        Free;
      end; //try..finally
    end
    else
      mmGraph1.Print(10, 20, 0, 0)
  end; //else begin

  dsOfflimits.DataSet.EnableControls;
  Screen.Cursor := xCursor;
end; //procedure TOffReportDlg.acPrintExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acSaveAsDefaultExecute(Sender: TObject);
begin
  mmGridPos1.WriteSettings;
  mmGridPos2.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGridPos3.MemoINI.Assign(mmGridPos1.MemoINI);
  mProgOptions.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGridPos2.WriteSettings;
  mmGridPos3.WriteSettings;
  mProgOptions.WriteSettings;
  mmGraph1.WriteSettings;
  mmGridPos1.MemoINI.SetDefaultProfile;
end; //procedure TOffReportDlg.acSaveAsDefaultExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acSaveProfileExecute(Sender: TObject);
var
  xRes: tSelectProfileRecordT;
begin
  xRes := mmGridPos1.MemoINI.SaveProfile;
  if xRes.Profilename <> '' then begin
    mmGridPos1.MemoINI.Profilename := xRes.Profilename;
    mmGridPos2.MemoINI.Assign(mmGridPos1.MemoINI);
    mmGridPos3.MemoINI.Assign(mmGridPos1.MemoINI);
    mProgOptions.MemoINI.Assign(mmGridPos1.MemoINI);
    mmGraph1.MemoINI.Assign(mmGridPos1.MemoINI);
    mmGridPos1.WriteSettings;
    mmGridPos2.WriteSettings;
    mmGridPos3.WriteSettings;
    mProgOptions.WriteSettings;
    mmGraph1.WriteSettings;
  end; //if xRes.Profilename <> '' then
end; //procedure TOffReportDlg.acSaveProfileExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acShowHideInfoWindowExecute(Sender: TObject);
begin
  mProgOptions.ShowOfflimitInfoWindow := not mProgOptions.ShowOfflimitInfoWindow;
  UpdateInfoWindows;
end; //procedure TOffReportDlg.acShowHideInfoWindowExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.FormatTo_hh_mm(Sender: TField; var Text: string; DisplayText: Boolean);
begin
//  Text := Format_h(Sender.AsInteger);
  if Sender.AsInteger = 0 then
    Text := '-'
  else
    Text := Format('%d', [Sender.AsInteger]);
//    Text := Format('%d',[Round(Sender.AsInteger / 60)]);
end; //procedure TOffReportDlg.FormatTo_hh_mm
//-----------------------------------------------------------------------------

procedure TOffReportDlg.FormDestroy(Sender: TObject);
begin
  mProgOptions.Free;
  mOfflimits.Free;
  mOfflimitCauses.Free;
  mParameter.free;
end; //procedure TOffReportDlg.FormDestroy
//-----------------------------------------------------------------------------

procedure TOffReportDlg.FormShow(Sender: TObject);
begin
  EnterMethod('FormShow');
  Screen.Cursor := crDefault;
  Update_grOfflimits;
  Translate_tabOfflimits;
end; //procedure TOffReportDlg.FormShow
//-----------------------------------------------------------------------------

procedure TOffReportDlg.mmTranslatorAfterTranslate(translator: TIvCustomTranslator);
var
  i: integer;
begin
  EnterMethod('mmTranslatorAfterTranslate');
  GetFieldNames;
  UpdateCaptions;
  Init_StringGrids;
  Translate_tabOfflimits;

  with mmGraph1 do begin
    BeginUpdate;
    for i := 1 to Data.NumSeries do
      Series.Name[i] := GetDisplayLabel(quOffline.FieldByName(Series.FieldName[i]).DisplayLabel);
    LeftYAxis.Title := cLeftAxisStr;
    RightYAxis.Title := cRightAxisStr;
    EndUpdate;
  end; //with mmGraph1 do
end;
//-----------------------------------------------------------------------------

procedure TOffReportDlg.PageControl1Change(Sender: TObject);
begin
  if (PageControl1.ActivePage = tsStatistic) and (quOffline.Active = False) then begin
    RunQuery(True);
  end;
  EnableActions;
end; //procedure TOffReportDlg.PageControl1Change
//-----------------------------------------------------------------------------

procedure TOffReportDlg.quOfflineAfterScroll(DataSet: TDataSet);
begin
  // updated linked informations only if not disabled -> disabled for printout
  if not quOffline.ControlsDisabled then begin
    ShowPieValues;
  end;
end; //procedure TOffReportDlg.quOfflineAfterScroll
//-----------------------------------------------------------------------------

procedure TOffReportDlg.quOnlineAfterScroll(DataSet: TDataSet);
begin
  if not(mCreating) then begin
    EnterMethod('quOnlineAfterScroll');
    ReadOnlineOfflimits(mOfflimits);
    // updated linked informations only if not disabled -> disabled for printout
    if not quOnline.ControlsDisabled then begin
      case nbOnline.PageIndex of
        0: Fill_grOfflimits;
        1: GetOnlineChartData;
        2: Fill_sgOffCause;
      end; //case nbOnline.PageIndex of
    end;
  end;//
end; //procedure TOffReportDlg.quOnlineAfterScroll
//-----------------------------------------------------------------------------

procedure TOffReportDlg.sgOffCauseDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  IsFixedCell: boolean;
  hrow, wcol,
    wtext, htext,
    txpos, typos: integer;
  bColor,
    fColor: TColor;
  t: string;

begin
  IsFixedCell := gdFixed in State;

  sgOffCause.Canvas.Font.Size := 8;
  sgOffCause.Canvas.Font.Style := [];

  t := sgOffCause.Cells[ACol, ARow];
  hrow := sgOffCause.RowHeights[ARow];
  wcol := sgOffCause.ColWidths[ACol];
  wtext := sgOffCause.Canvas.TextWidth(t);
  htext := sgOffCause.Canvas.TextHeight(t);
  typos := (hrow - htext) div 2;

  if not IsFixedCell then begin
    bColor := clWindow;
    fColor := clWindowText;

    sgOffCause.Canvas.Brush.Color := bColor;
    sgOffCause.Canvas.Font.Color := fColor;

    txpos := (wcol - wtext) div 2;
    sgOffCause.Canvas.TextRect(rect, rect.left + txpos, rect.top + typos, t);
  end //if not IsFixedCell then
  else begin
    if ARow = 0 then
      bColor := cDefaultGridTitleColor
    else
      bColor := cFixedColColor;

    sgOffCause.Canvas.Brush.Color := bColor;
    sgOffCause.Canvas.FillRect(Rect);
    sgOffCause.Canvas.Font.Color := clBlack;

    if (ARow < 2) and (ACol > 0) then
      txpos := (wcol - wtext) div 2
    else
      txpos := (wcol - wtext - 10);

    sgOffCause.Canvas.TextRect(rect, rect.left + txpos, rect.top + typos, t);
  end; //else if not IsFixedCell then
end; //procedure TOffReportDlg.sgOffCauseDrawCell
//-----------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid1CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
var
  xData: eOfflimitDataT;
  xOfflimits: TOfflimits;
begin
  if gdFixed in State then
    ABrush.Color := clLightYellow
  else
    ABrush.Color := TwwDBGrid(Sender).Color;

  xData := mOfflimits.Fields.TypeByFieldname(Field.Fieldname);
  if xData in [otYB..otCYTot] then begin
    xOfflimits := TOfflimits.Create;
    try
      if quOnlinec_spindle_id.AsInteger = 29 then begin
        sleep(1);
      end;
      ReadOnlineOfflimits(xOfflimits);
      if cOfflimitTypes * xOfflimits.LimitFaults[xData] <> [] then begin
        AFont.Color := clRed;
//        Field.AsString := '#' + Field.AsString;
      end;
    finally
      xOfflimits.Free;
    end; //try..finally
  end; //if xData in [otYB..otCYTot] then begin

  if highlight then begin
    AFont.Color := clHighlightText;
    ABrush.Color := clHighlight;
  end; //if highlight then
end; //procedure TOffReportDlg.wwDBGrid1CalcCellColors
//-----------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid1DblClick(Sender: TObject);
var
  xForm: TEsp_SpindleReportDlg;
  xRepInfo: rESPReportInfoT;

begin
{$IFDEF demo}
  if quOnlinec_machine_id.Value <> 1 then begin
    MMMessageDlg('demo-data available only for mach.no. 1', mtinformation, [mbok], 0, Self);
    exit;
  end; //if quOnlinec_machine_id.Value <> 1 then
{$ENDIF}

  if not quOnline.IsEmpty then begin
    xRepInfo.MachineID := quOnlinec_machine_id.Value;
    xRepInfo.Spindle := quOnlinec_spindle_id.Value;
    xRepInfo.LengthUnit := lb100Km;

    //get prod_id of current spindle
    with quProdID do begin
      parameters.ParambyName('MachineID').value := xRepInfo.MachineID;
      parameters.ParambyName('SpindleID').value := xRepInfo.Spindle;

      Active := True;
      xRepInfo.ProdGroupID := fieldbyname('c_prod_id').asinteger;
      xRepInfo.MinSpindle := fieldbyname('c_spindle_first').asinteger;
      xRepInfo.MaxSpindle := fieldbyname('c_spindle_last').asinteger;
      Active := False;
    end; //with quProdID do

    //always start showing all available intervals
    //Wss: because of OLE Automation I have to know if it has to be a free form for ChildForm
    if GetCreateChildForm(TEsp_SpindleReportDlg, TForm(xForm), Self) then begin
      xForm.Init(xRepInfo, nil);
      xForm.ShowGraphicOptions := True; // SDO
      xForm.Visible := True;
    end;
  end;
end; //procedure TOffReportDlg.wwDBGrid1DblClick
//-----------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid1TitleButtonClick(Sender: TObject; AFieldName: string);
var
  xLastOrder: string;
  xSortStr: string;
begin
  EnterMethod('wwDBGrid1TitleButtonClick');
  AFieldName := lowercase(AFieldName);

  if (AFieldName = 'calc_offcause1') or
    (AFieldName = 'calc_offcause2') or
    (AFieldName = 'calc_offcause3') then exit;

  if PageControl1.ActivePage = tsOnline then
    xLastOrder := mOnlineSortOrder
  else
    xLastOrder := mOfflineSortOrder;

  if AFieldName = 'c_machine_name' then
    xSortStr := cDefaultSortOrder
  else if AFieldName = 'c_spindle_id' then
    xSortStr := 'mo.c_spindle_id, m.c_machine_name'
  else if AFieldName = 'calc_outofproduction' then
    xSortStr := 'mo.c_outofproduction,' + cDefaultSortOrder
  else if AFieldName = 'calc_offlineratio' then
    xSortStr := 'calc_offlineratio,' + cDefaultSortOrder
  else
    xSortStr := 'mo.' + AFieldName + ',' + cDefaultSortOrder;

  if xLastOrder = xSortStr then begin
    if mSortOrder = soDescending then
      mSortOrder := soAscending
    else
      mSortOrder := soDescending;
  end //if xLastOrder = xSortStr then
  else
    mSortOrder := soDescending; //soAscending;

  if PageControl1.ActivePage = tsOnline then
    mOnlineSortOrder := xSortStr
  else
    mOfflineSortOrder := xSortStr;

  RunQuery(False);
end; //procedure TOffReportDlg.wwDBGrid1TitleButtonClick
//-----------------------------------------------------------------------------

procedure TOffReportDlg.tabsOnlineClick(Sender: TObject);
begin
  inherited;
  nbOnline.PageIndex := tabsOnline.TabIndex;

  case nbOnline.PageIndex of
    0: Fill_grOfflimits;
    1: GetOnlineChartData;
    2: Fill_sgOffCause;
  end; //case nbOnline.PageIndex of
end; //procedure TOffReportDlg.tabsOnlineClick
//-----------------------------------------------------------------------------

procedure TOffReportDlg.EnableActions;
begin
  acEditOfflimitCause.Enabled := PageControl1.ActivePage = tsOnline;
  acDefPatternRecognition.Enabled := acEditOfflimitCause.Enabled;

  if (PageControl1.ActivePage = tsStatistic) then begin
    acToggleGridChart.Enabled := True;
    mShowStatisticChart := True;
    pnStatisticGrid.Align := alClient;
    pnStatisticChart.Align := alClient;

    ShowStatisticGraphic;

    acToggleGridChart.Hint := cShowTabHint;
    acToggleGridChart.Caption := acToggleGridChart.Hint; //Translate(cShowTabelleCaption);
    acShowPie.Caption := cPieOn;
    acShowPie.Hint := acShowPie.Caption;
    acShowHideInfoWindow.Enabled := False;

    acEditGridOptions.Caption := GraphOptionsCaption;
    acEditGridOptions.Hint := GraphOptionsHint;
  end
  else begin
    acToggleGridChart.Enabled := False;
    mmGraph1.UnselectBar;

    FreeAndNil(mStatisticPie);
    acShowPie.Checked := False;

    acShowHideInfoWindow.Enabled := True;
    acEditGridOptions.Caption := TabOptionsCaption;
    acEditGridOptions.Hint := TabOptionsHint;
  end;

  acToggleGridChart.Visible := PageControl1.ActivePage = tsStatistic;
  acSort.Visible := acToggleGridChart.Visible;
  acShowPie.Visible := acToggleGridChart.Visible;
  acResetSpindle.Visible := acToggleGridChart.Visible;
  acResetWithFilter.Visible := acToggleGridChart.Visible;
  acSetOfflimitCause.Visible := PageControl1.ActivePage = tsOnline;

  N3.Visible := acToggleGridChart.Visible;
end; //procedure TOffReportDlg.EnableActions
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Fill_sgOffCause;
var
  i, j: integer;
  xCauses: aOffCausesT;

begin
  //1. read causes
  xCauses := mOfflimitCauses.GetCauses(mOfflimits);

  //2. reset stringgrid
  j := 0;
  sgOffCause.RowCount := 2;
  sgOffCause.cells[0, 1] := '';
  sgOffCause.cells[1, 1] := '';
  sgOffCause.cells[2, 1] := '';

  //3. fill grid
  for i := 0 to high(xCauses) do begin
    if (i < mProgOptions.OfflimitPattern.MaxCauses) and
      (xCauses[i].TotalHitrate >= mProgOptions.OfflimitPattern.MinHitrate) then
      if xCauses[i].cntNumber > 0 then begin
        inc(j);
        sgOffCause.cells[0, j] := xCauses[i].Name;
        sgOffCause.cells[1, j] := formatfloat(',0', xCauses[i].cntNumber);
        sgOffCause.cells[2, j] := formatfloat('0%', xCauses[i].TotalHitrate);
        sgOffCause.RowCount := j + 1;
      end; //if xCauses[i].cntNumber > 0 then begin
  end; //for i := 0 to high(xCauses) do begin
end; //procedure TOffReportDlg.Fill_sgOffCause

(*

procedure TOffReportDlg.Fill_sgOffCause;
const
  cMaxNumberOfHits = 10; //=> sizeof(eOfflimitDataT)

var
  xFieldname : string;
  xData      : eOfflimitDataT;
  i,j        : integer;
  xHitRate,
  xMin,
  xMax,
  xDev,
  xField,
  xField_avg : double;
  xCauses    : array of rOffCausesT;

begin
  with qryOffCause do begin
    //1. read t_offlimit_cause
    First;

    //2. count hits
    i := 0;
    xCauses := nil;
    try
      while not eof do begin
        inc(i);
        SetLength(xCauses,i);

        j := i-1;
        fillchar(xCauses[j],sizeof(rOffCausesT),0);

        xCauses[j].Name := FieldByName('c_offcause_name').asstring;
        xCauses[j].cntNumber := FieldByName('c_counter').asinteger;

        for xData := otYB to otCYTot do begin
          xFieldname := mOfflimits.Fields.FieldnameByType(xData);
          inc(xCauses[j].offcntNumber,FieldByName(xFieldname +'_offcnt').asinteger);
        end; //for xData := otYB to otCYTot do begin

        for xData := otYB to otCYTot do begin
          xFieldname := mOfflimits.Fields.FieldnameByType(xData);

          //2a. pattern analysis
          if not FieldByName(xFieldname +'_min').IsNull then begin
            xMin := FieldByName(xFieldname +'_min').asfloat -cEpsilon;
            xMax := FieldByName(xFieldname +'_max').asfloat +cEpsilon;

            if xMax < xMin then ChangeVars(xMax,xMin,sizeof(xMin));

            if not mOfflimits.IsNull[oiSpindle,xData] then
              if abs(mOfflimits.Value[oiMean,xData]) > cEpsilon then begin
                xDev := (mOfflimits.Value[oiSpindle,xData] /mOfflimits.Value[oiMean,xData] -1) *100;
                if (xDev <= xMax) and (xDev >= xMin) then
                  incr(xCauses[j].PatternHitrate, 1 /cMaxNumberOfHits);
              end; //if abs(mOfflimits.Value[oiMean,xData]) > cEpsilon then begin
          end; //if not FieldByName(xFieldname +'_min').IsNull then

          //2b. occurrence analysis
          if (cOfflimitTypes * mOfflimits.LimitFaults[xData] <> []) and //is offlimit
             (xCauses[j].offcntNumber > 0) then
            incr(xCauses[j].OccurrenceHitrate, FieldByName(xFieldname +'_offcnt').asinteger /xCauses[j].offcntNumber);
        end; //for xData := otYB to otCYTot do

        //2c. calc weight: occurrence � pattern
        xCauses[j].TotalHitrate := (mProgOptions.OfflimitPattern.Weight *xCauses[j].PatternHitrate)
                                  +((100 -mProgOptions.OfflimitPattern.Weight) *xCauses[j].OccurrenceHitrate);

        Next;
      end; //while not eof do

      //3. reset stringgrid
      j := 0;
      sgOffCause.RowCount := 2;
      sgOffCause.cells[0,1] := '';
      sgOffCause.cells[1,1] := '';
      sgOffCause.cells[2,1] := '';

      //4. sort by TotalHitrate and write to stringgrid
      if high(xCauses) > 0 then SortCauses(xCauses,low(xCauses),high(xCauses));

      for i := 0 to high(xCauses) do
        if (i < mProgOptions.OfflimitPattern.MaxCauses) and
           (xCauses[i].TotalHitrate >= mProgOptions.OfflimitPattern.MinHitrate) then
          if xCauses[i].cntNumber > 0 then begin
            inc(j);
            sgOffCause.cells[0,j] := xCauses[i].Name;
            sgOffCause.cells[1,j] := formatfloat(',0',xCauses[i].cntNumber);
            sgOffCause.cells[2,j] := formatfloat('0%',xCauses[i].TotalHitrate);
            sgOffCause.RowCount := j+1;
          end; //if xCauses[i].cntNumber > 0 then begin
    finally
      xCauses := nil;
    end; //try..finally
  end; //with qryOffCause do
end; //procedure TOffReportDlg.Fill_sgOffCause

*)
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Fill_grOfflimits;
var
  xInfo: eOfflimitInfoT;
  xData: eOfflimitDataT;
  xFieldName: string;

//----------
  procedure ModifyRec(aID: integer; aFieldname: string; aValue: string);
  begin
    with tabOfflimits do begin
      Locate('c_id', aID, []);
      Edit;
      fieldbyname(aFieldname).asstring := aValue;
      Post;
    end; //with tabOfflimits do begin
  end; //procedure ModifyRec
//----------

begin
  with tabOfflimits do
    // at printing this dataset is disabled and no information has to be updated
    if not ControlsDisabled then try
      DisableControls;

      tabOfflimitsc_position.DisplayLabel := format(cMachineSpdInfoStr, [quOnlinec_machine_name.AsString, quOnlinec_spindle_id.AsInteger]);
{ TODO : LenBase anpassen }
      // Unterschiedliche Maschinen k�nnen unterschiedliche Settings haben -> Text noch neu setzen
      ModifyRec(1, 'c_position', Format(cSpindelDataStr, [IvDictio.Translate(cLenBaseTxt[quOnlinec_base.AsInteger].Text)]) + ' ');
      ModifyRec(2, 'c_position', Format(cMeanDataStr, [quOnlinec_Average_time.AsInteger]) + ' ');

      for xData := otYB to otCYTot do
        for xInfo := oiSpindle to oiUpperRelativePC do
          ModifyRec(ord(xInfo) + 1, mOfflimits.Fields.FieldnameByType(xData), '');

      for xData := otYB to otCYTot do begin
        xFieldName := mOfflimits.Fields.FieldnameByType(xData);
        ModifyRec(1, xFieldName, mOfflimits.AsString(',0.0', oiSpindle, xData));
        ModifyRec(2, xFieldName, mOfflimits.AsString(',0.0', oiMean, xData));
        ModifyRec(3, xFieldName, mOfflimits.AsString(',0.0', oiLowerFixedLimit, xData));
        ModifyRec(4, xFieldName, mOfflimits.AsString(',0.0', oiUpperFixedLimit, xData));

        if not (mOfflimits.IsNull[oiLowerRelativeLimit, xData] and mOfflimits.IsNull[oiLowerRelativePC, xData]) then
          ModifyRec(5, xFieldName, mOfflimits.AsString(',0.0', oiLowerRelativeLimit, xData) + ' (' + mOfflimits.AsString(',0%', oiLowerRelativePC, xData) + ')');

        if not (mOfflimits.IsNull[oiUpperRelativeLimit, xData] and mOfflimits.IsNull[oiUpperRelativePC, xData]) then
          ModifyRec(6, xFieldName, mOfflimits.AsString(',0.0', oiUpperRelativeLimit, xData) + ' (' + mOfflimits.AsString(',0%', oiUpperRelativePC, xData) + ')');
      end; //for xData := otYB to otCYTot do
    finally
      EnableControls;
      grOfflimits.Invalidate;
    end; //try..finally
end; //procedure TOffReportDlg.Fill_grOfflimits
//-----------------------------------------------------------------------------

procedure TOffReportDlg.GetFieldNames;
begin
  // Online strings
  quOnlinec_machine_name.DisplayLabel := (cc_machine_nameStr);
  quOnlinec_spindle_id.DisplayLabel := (cc_spindle_idStr);
  quOnlinec_update_time.DisplayLabel := (cc_update_timeStr);
  quOnlinec_max_online_offlimit_time.DisplayLabel := (cc_max_online_offlimit_timeStr);
  quOnlinec_YB.DisplayLabel := (cc_YBStr);
  quOnlinec_YB_online_time.DisplayLabel := (cc_YB_online_timeStr);
  quOnlinec_Eff.DisplayLabel := (cc_EffStr);
  quOnlinec_Eff_online_time.DisplayLabel := (cc_Eff_online_timeStr);
  quOnlinec_CSp.DisplayLabel := (cc_CSpStr);
  quOnlinec_CSp_online_time.DisplayLabel := (cc_CSp_online_timeStr);
  quOnlinec_RSp.DisplayLabel := (cc_RSpStr);
  quOnlinec_RSp_online_time.DisplayLabel := (cc_RSp_online_timeStr);
  quOnlinec_CBu.DisplayLabel := (cc_CBuStr);
  quOnlinec_CBu_online_time.DisplayLabel := (cc_CBu_online_timeStr);
  quOnlinec_CUpY.DisplayLabel := (cc_CUpYStr);
  quOnlinec_CUpY_online_time.DisplayLabel := (cc_CUpY_online_timeStr);
  quOnlinec_tLSt.DisplayLabel := (cc_tLStStr);
  quOnlinec_tLSt_online_time.DisplayLabel := (cc_tLSt_online_timeStr);
  quOnlinec_LSt.DisplayLabel := (cc_LStStr);
  quOnlinec_LSt_online_time.DisplayLabel := (cc_LSt_online_timeStr);
  quOnlinec_CSIRO.DisplayLabel := (cc_CSIROStr);
  quOnlinec_CSIRO_online_time.DisplayLabel := (cc_CSIRO_online_timeStr);
  quOnlinec_CYTot.DisplayLabel := (cc_CYTotStr);
  quOnlinec_CYTot_online_time.DisplayLabel := (cc_CYTot_online_timeStr);
  quOnlinec_OutOfProduction.DisplayLabel := (cc_OutOfProductionStr);
  quOnlinecalc_OutOfProduction.DisplayLabel := (cc_OutOfProductionStr);
  quOnlinec_OutOfProduction_online_time.DisplayLabel := (cc_OutOfProduction_online_timeStr);
  quOnlinecalc_offcause1.DisplayLabel := (ccalc_offcause1Str);
  quOnlinecalc_offcause2.DisplayLabel := (ccalc_offcause2Str);
  quOnlinecalc_offcause3.DisplayLabel := (ccalc_offcause3Str);

  // Offline strings
  quOfflinec_machine_name.DisplayLabel := (cc_machine_nameStr);
  quOfflinec_spindle_id.DisplayLabel := (cc_spindle_idStr);
  quOfflinecalc_OfflineRatio.DisplayLabel := (ccalc_OfflineRatioStr);
  quOfflinec_offline_offlimit_time.DisplayLabel := (cc_offline_offlimit_timeStr);
  quOfflinec_offline_watchtime.DisplayLabel := (cc_offline_watchtimeStr);
  quOfflinec_YB_offline_time.DisplayLabel := (cc_YB_offline_timeStr);
  quOfflinec_RedL_offline_time.DisplayLabel := (cc_RedL_offline_timeStr);
  quOfflinec_Eff_offline_time.DisplayLabel := (cc_Eff_offline_timeStr);
  quOfflinec_CSp_offline_time.DisplayLabel := (cc_CSp_offline_timeStr);
  quOfflinec_RSp_offline_time.DisplayLabel := (cc_RSp_offline_timeStr);
  quOfflinec_CBu_offline_time.DisplayLabel := (cc_CBu_offline_timeStr);
  quOfflinec_CUpY_offline_time.DisplayLabel := (cc_CUpY_offline_timeStr);
  quOfflinec_tLSt_offline_time.DisplayLabel := (cc_tLSt_offline_timeStr);
  quOfflinec_LSt_offline_time.DisplayLabel := (cc_LSt_offline_timeStr);
  quOfflinec_CSIRO_offline_time.DisplayLabel := (cc_CSIRO_offline_timeStr);
  quOfflinec_CYTot_offline_time.DisplayLabel := (cc_CYTot_offline_timeStr);
  quOfflinec_OutOfProduction_offline_time.DisplayLabel := (cc_OutOfProduction_offline_timeStr);
end; //procedure TOffReportDlg.GetFieldNames
//-----------------------------------------------------------------------------

procedure TOffReportDlg.GetOfflimitAllSpindles;
var
  xLabel: string;
  x, xPos: longint;
  xSerieNr,
    xValue,
    xMaxValue: integer;
  xHasLine,
    xHasBar: boolean;
  xParameter: TParameterList;
begin
// GetFieldNames;

  xHasLine := False;
  xHasBar := False;

  acSortError.Enabled := False;

  quOffline.DisableControls;
  quOffline.AfterScroll := nil;
  quOffline.First;
  try

    mmGraph1.Data.ClearData;
    mmGraph1.Bars.Style := mParameter.BarStyle;

//  mParameter := u_lib.ReadParam(quOffline,mParaList,mmGraph1.Bars.Style);
//  mParameter := u_lib.ReadParam(quOffline,mParaList,mmGraph1);

    xParameter := ReadParam(quOffline, mParaList, mmGraph1);
    mParameter.Assign(xParameter);
    xParameter.Free;

    if mmGraph1.Bars.Style = bsSide then
      mmGraph1.Bars.Gap := 5
    else
      mmGraph1.Bars.Gap := 2;

    mmGraph1.BeginUpdate;

    xSerieNr := 0;
    for x := 0 to mParameter.Count - 1 do
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then inc(xSerieNr);
    mmGraph1.Data.NumSeries := xSerieNr;

    xSerieNr := 1;
    for x := 0 to mParameter.Count - 1 do begin
      if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
        mParameter.Items[x].GraphicParam.ShowGraph then
        with mmGraph1 do begin
        //Serien
          Data.NumSeries := xSerieNr;

        //worbis
          Series.FieldName[xSerieNr] := mParameter.Items[x].GraphicParam.FieldName;

          Series.Color[xSerieNr] := mParameter.Items[x].GraphicParam.Color;
          Series.YAxis[xSerieNr] := eYAxisT(Ord(mParameter.Items[x].GraphicParam.ChartType));
          Series.YScalingType[xSerieNr] := mParameter.Items[x].GraphicParam.YScalingType;
          Series.ShowStat[xSerieNr] := mParameter.Items[x].GraphicParam.Statistik;
          Series.Visible[xSerieNr] := mParameter.Items[x].GraphicParam.ShowGraph;
          Series.LowerLimit[xSerieNr] := mParameter.Items[x].GraphicParam.LowerLimit;
          Series.UpperLimit[xSerieNr] := mParameter.Items[x].GraphicParam.UpperLimit;
          Series.UpperLimitColor[xSerieNr] := clRed;
          Series.LowerLimitColor[xSerieNr] := clRed;

          xHasBar := xHasBar or (mParameter.Items[x].GraphicParam.ChartType = ctBar);
          xHasLine := xHasLine or (mParameter.Items[x].GraphicParam.ChartType = ctLine);

        //Daten zu den Serien
          xPos := 1;
          xMaxValue := 0;
          quOffline.First;
          while not quOffline.Eof do begin

            xLabel := quOfflinec_machine_name.value + '-' + quOfflinec_spindle_id.AsString;
            xValue := quOffline.FieldByName(mParameter.Items[x].ColumnParam.FieldName).AsInteger;

            if xValue > xMaxValue then xMaxValue := xValue;

            mmGraph1.Data.AddYValue(xSerieNr, xPos, xValue, xLabel);

            inc(xPos);
            quOffline.Next;
          end; //while not quOffline.Eof do

       // if xMaxValue > 0 then
          //Series.Name[xSerieNr] := mParameter.Items[x].ColumnParam.DisplayLabel;
          Series.Name[xSerieNr] := GetDisplayLabel(quOffline.FieldByName(Series.FieldName[xSerieNr]).DisplayLabel);

        //else
        //  Series.Name[xSerieNr] := mParameter.Items[x].ColumnParam.DisplayLabel + '   [ ' +
        //                          cNoDataAvailable + ' ]';

          if mParameter.Items[x].ColumnParam.FieldName = 'c_offline_offlimit_time' then
            acSortError.Enabled := True;

          inc(xSerieNr);
        end; // end with
    end; // for

    mmGraph1.LeftYAxis.Visible := xHasLine;
    mmGraph1.RightYAxis.Visible := True; //xHasBar;

    if xHasBar and xHasLine then
      mmGraph1.HorzGrid.GridType := hgLeftRightAxis
    else if xHasBar then
      mmGraph1.HorzGrid.GridType := hgRightAxis
    else if xHasLine then
      mmGraph1.HorzGrid.GridType := hgLeftAxis;

    mmGraph1.EndUpdate;
  finally
    quOffline.First;
    quOffline.AfterScroll := quOfflineAfterScroll;
    quOffline.EnableControls;
  end; //try..finally
end; //procedure TOffReportDlg.GetOfflimitAllSpindles
//-----------------------------------------------------------------------------

procedure TOffReportDlg.GetOnlineChartData;

//------------
  procedure AddBar(aDataType: eOfflimitDataT; aValue: integer; aItemStr: string);
  var
    bColor: TColor;

  begin
    if cOfflimitTypes * mOfflimits.LimitFaults[aDataType] <> [] then
      bColor := clRed
    else
      bColor := clTeeColor;
    Series1.Add(aValue, aItemStr, bColor);
  end; //procedure AddBar
//------------

begin
  Series1.Clear;
  AddBar(otYB, round(quOnlinec_YB_online_time.AsInteger / 60), quOnlinec_YB.DisplayLabel);
  AddBar(otEff, round(quOnlinec_Eff_online_time.AsInteger / 60), quOnlinec_Eff.DisplayLabel);
  AddBar(otCSp, round(quOnlinec_CSp_online_time.AsInteger / 60), quOnlinec_CSp.DisplayLabel);
  AddBar(otRSp, round(quOnlinec_RSp_online_time.AsInteger / 60), quOnlinec_RSp.DisplayLabel);
  AddBar(otCBu, round(quOnlinec_CBu_online_time.AsInteger / 60), quOnlinec_CBu.DisplayLabel);
  AddBar(otCUpY, round(quOnlinec_CUpY_online_time.AsInteger / 60), quOnlinec_CUpY.DisplayLabel);
  AddBar(ottLSt, round(quOnlinec_tLSt_online_time.AsInteger / 60), quOnlinec_tLSt.DisplayLabel);
  AddBar(otLSt, round(quOnlinec_LSt_online_time.AsInteger / 60), quOnlinec_LSt.DisplayLabel);
  AddBar(otCSIRO, round(quOnlinec_CSIRO_online_time.AsInteger / 60), quOnlinec_CSIRO.DisplayLabel);
  AddBar(otCYTot, round(quOnlinec_CYTot_online_time.AsInteger / 60), quOnlinec_CYTot.DisplayLabel);

  //AddBar(otRed,round(quOnlinec_RedL_online_time.AsInteger/60),quOnlinec_RedL.DisplayLabel);  // @@RedL
end; //procedure TOffReportDlg.GetOnlineChartData
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Init_StringGrids;
begin
  with sgOffCause do begin
    ColWidths[0] := 180;
    ColWidths[1] := 100;
    ColWidths[2] := 100; // removed: wish from Kast, 6.7.01 100;

    Cells[0, 0] := cOffcauseNameStr;
    Cells[1, 0] := cOffcauseCountStr;
    Cells[2, 0] := cHitRatioStr;
  end; //with sgOffCause do
end; //procedure TOffReportDlg.Init_StringGrids
//-----------------------------------------------------------------------------

procedure TOffReportDlg.LoadProfile(aUsername, aProfilename: string);
var
  xParameter: TParameterList;
begin
  mmGridPos1.MemoINI.Username := aUsername;
  mmGridPos1.MemoINI.Profilename := aProfilename;
  mmGridPos2.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGridPos3.MemoINI.Assign(mmGridPos1.MemoINI);
  mmGraph1.MemoIni.Assign(mmGridPos1.MemoINI);
  mProgOptions.MemoIni.Assign(mmGridPos1.MemoINI);

  mmGridPos1.ReadSettings;
  mmGridPos2.ReadSettings;
  mmGridPos3.ReadSettings;
  mmGraph1.ReadSettings;
  mProgOptions.ReadSettings;

  wwDBGrid1.Options := wwDBGrid1.Options - [Wwdbigrd.dgEditing];

  mParaList := GetGraphParamList(mmGraph1);
  // mParameter := ReadParam(quOffline,mParaList,mmGraph1.Bars.Style);
//  mParameter := ReadParam(quOffline,mParaList,mmGraph1);

  xParameter := ReadParam(quOffline, mParaList, mmGraph1);
  mParameter.Assign(xParameter);
  xParameter.Free;

  UpdateInfoWindows;
  UpdateCaptions;

  mOfflimitCauses.OfflimitPattern := mProgOptions.OfflimitPattern;

  RunQuery(True);
  Update_grOfflimits;
end; //procedure TOffReportDlg.LoadProfile
//-----------------------------------------------------------------------------

procedure TOffReportDlg.ReadOnlineOfflimits(aOfflimits: TOfflimits);
begin
  aOfflimits.Clear;

  aOfflimits.SetData(oiSpindle, otYB, quOnlinec_YB);
  aOfflimits.SetData(oiSpindle, otEff, quOnlinec_Eff);
  aOfflimits.SetData(oiSpindle, otCSp, quOnlinec_CSp);
  aOfflimits.SetData(oiSpindle, otRSp, quOnlinec_RSp);
  aOfflimits.SetData(oiSpindle, otCBu, quOnlinec_CBu);
  aOfflimits.SetData(oiSpindle, otCUpY, quOnlinec_CUpY);
  aOfflimits.SetData(oiSpindle, ottLSt, quOnlinec_tLSt);
  aOfflimits.SetData(oiSpindle, otLSt, quOnlinec_LSt);
  aOfflimits.SetData(oiSpindle, otCSIRO, quOnlinec_CSIRO);
  aOfflimits.SetData(oiSpindle, otCYTot, quOnlinec_CYTot);

  aOfflimits.SetData(oiMean, otYB, quOnlinec_YB_AVG);
  aOfflimits.SetData(oiMean, otEff, quOnlinec_Eff_AVG);
  aOfflimits.SetData(oiMean, otCSp, quOnlinec_CSp_AVG);
  aOfflimits.SetData(oiMean, otRSp, quOnlinec_RSp_AVG);
  aOfflimits.SetData(oiMean, otCBu, quOnlinec_CBu_AVG);
  aOfflimits.SetData(oiMean, otCUpY, quOnlinec_CUpY_AVG);
  aOfflimits.SetData(oiMean, ottLSt, quOnlinec_tLSt_AVG);
  aOfflimits.SetData(oiMean, otLSt, quOnlinec_LSt_AVG);
  aOfflimits.SetData(oiMean, otCSIRO, quOnlinec_CSIRO_AVG);
  aOfflimits.SetData(oiMean, otCYTot, quOnlinec_CYTot_AVG);

  if quOnlinec_OffBorderByAverage.Value then begin
    aOfflimits.SetData(oiLowerRelativePC, otEff, quOnlinec_Eff_PC_min);
    aOfflimits.SetData(oiLowerRelativePC, otCYTot, quOnlinec_CYTot_PC_min);

    aOfflimits.SetData(oiUpperRelativePC, otYB, quOnlinec_YB_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otEff, quOnlinec_Eff_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otCSp, quOnlinec_CSp_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otRSp, quOnlinec_RSp_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otCBu, quOnlinec_CBu_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otCUpY, quOnlinec_CUpY_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, ottLSt, quOnlinec_tLSt_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otLSt, quOnlinec_LSt_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otCSIRO, quOnlinec_CSIRO_PC_max);
    aOfflimits.SetData(oiUpperRelativePC, otCYTot, quOnlinec_CYTot_PC_max);
  end; //if quOnlinec_OffBorderByAverage.Value then

  if quOnlinec_OffBorderByFixValues.Value then begin
    aOfflimits.SetData(oiLowerFixedLimit, otEff, quOnlinec_Eff_min);
    aOfflimits.SetData(oiLowerFixedLimit, otCYTot, quOnlinec_CYTot_min);

    aOfflimits.SetData(oiUpperFixedLimit, otYB, quOnlinec_YB_max);
    aOfflimits.SetData(oiUpperFixedLimit, otEff, quOnlinec_Eff_max);
    aOfflimits.SetData(oiUpperFixedLimit, otCSp, quOnlinec_CSp_max);
    aOfflimits.SetData(oiUpperFixedLimit, otRSp, quOnlinec_RSp_max);
    aOfflimits.SetData(oiUpperFixedLimit, otCBu, quOnlinec_CBu_max);
    aOfflimits.SetData(oiUpperFixedLimit, otCUpY, quOnlinec_CUpY_max);
    aOfflimits.SetData(oiUpperFixedLimit, ottLSt, quOnlinec_tLSt_max);
    aOfflimits.SetData(oiUpperFixedLimit, otLSt, quOnlinec_LSt_max);
    aOfflimits.SetData(oiUpperFixedLimit, otCSIRO, quOnlinec_CSIRO_max);
    aOfflimits.SetData(oiUpperFixedLimit, otCYTot, quOnlinec_CYTot_max);
  end; //if quOnlinec_OffBorderByFixValues.Value then

  aOfflimits.OutOfProduction := quOnlinec_OutOfProduction.asboolean;

  aOfflimits.Calculate;
end; //procedure TOffReportDlg.ReadOnlineOfflimits
//-----------------------------------------------------------------------------

procedure TOffReportDlg.RunQuery(aSavePos: boolean);
var
  xOrderWhere: String;
  xStr1: String;
  xMachineID: Integer;
  xSpindleID: Integer;
  xWhereList: TStringList;
begin
  xMachineID := 0;
  xSpindleID := 0;
  xWhereList := TStringList.Create;
  try
    if PageControl1.ActivePage = tsOnline then begin
      with quOnline do
      try
        // Datensatz nur merken, wenn Dataset offen
        if Active then begin
          xMachineID := quOnlinec_machine_id.asinteger;
          xSpindleID := quOnlinec_spindle_id.asinteger;
        end;// if Active then begin
        Active := False;

        // diese Where Kondition kommt direkt nach 'WHERE ' -> daher kein AND
        xWhereList.Add(format('mo.c_max_online_offlimit_time >= %d', [mProgOptions.OnlineTimeLimit]));

        if mProgOptions.OnlineShowOutOfProdSpindles then
          xWhereList.Add('AND mo.c_OutOfProduction >= 0')
        else
          xWhereList.Add('AND mo.c_OutOfProduction = 0');

        if mFloorMachines <> '' then
          xWhereList.Add(Format('AND m.c_machine_id in (%s)', [mFloorMachines]));

        xOrderWhere := GetFirstStr(mOnlineSortOrder, ',');
        xStr1       := GetLastStr(mOnlineSortOrder, ',');
        if mSortOrder = soDescending then
          xOrderWhere := xOrderWhere + ' DESC,'
        else
          xOrderWhere := xOrderWhere + ',';
        xOrderWhere := xOrderWhere + xStr1;

        quOnline.CommandText := Format(dmSpindleReport.slOnlineOfflimit.Text, [xWhereList.Text, xOrderWhere]);
        CodeSite.SendString('quOnline.CommandText', quOnline.CommandText);

        Active := True;
        if aSavePos then
          quOnline.Locate('c_machine_id;c_spindle_id', VarArrayOf([xMachineID, xSpindleID]), [])
        else
          quOnline.First;

        //if quOnline.Locate('c_spindle_id',xSpindleID,[]) then messagebeep(0);
      finally
        tabsOnlineClick(nil); //xxx
      end; //try..finally
    end //if PageControl1.ActivePage = tsOnline then
    else begin
      // tsOffline
      with quOffline do
      try
        // Datensatz nur merken, wenn Dataset offen
        if Active then begin
          xMachineID := quOfflinec_machine_id.asinteger;
          xSpindleID := quOfflinec_spindle_id.asinteger;
        end;// if Active then begin
        Active := False;

        if mFloorMachines <> '' then
          xWhereList.Add(Format('AND m.c_machine_id in (%s)', [mFloorMachines]));

        xOrderWhere := GetFirstStr(mOfflineSortOrder, ',');
        xStr1       := GetLastStr(mOfflineSortOrder, ',');
        if mSortOrder = soDescending then
          xOrderWhere := xOrderWhere + ' DESC,'
        else
          xOrderWhere := xOrderWhere + ',';
        xOrderWhere := xOrderWhere + xStr1;

        CommandText := Format(dmSpindleReport.slOfflineOfflimit.Text, [xWhereList.Text, xOrderWhere]);
        CodeSite.SendString('quOffline.CommandText', quOffline.CommandText);
        Parameters.ParamByName('MinOfflimitTime').value := mProgOptions.OfflineTimeLimit;
        Parameters.ParamByName('MinOfflineRatio').value := mProgOptions.OfflineRatio;

        Active := True;
      finally
        GetOfflimitAllSpindles;
        if aSavePos then
          quOffline.Locate('c_machine_id;c_spindle_id', VarArrayOf([xMachineID, xSpindleID]), [])
        else
          quOffline.First;
      end; //try..finally
    end; //else if PageControl1.ActivePage = tsOnline then
  finally
    xWhereList.Free;
  end;
end; //procedure TOffReportDlg.RunQuery
//-----------------------------------------------------------------------------

procedure TOffReportDlg.SortCauses(var aData: array of rOffCausesT; iLo, iHi: integer);

//-----------
  function GetValue(aPos: integer): double;
  begin
    Result := aData[aPos].TotalHitrate;
  end; //function GetValue
//-----------

var
  xLo, xHi: integer;
  xMid: double;
  xTmp: rOffCausesT;

begin
  xLo := iLo;
  xHi := iHi;
  xMid := GetValue((xLo + xHi) div 2);

  repeat
    while GetValue(xLo) > xMid do
      inc(xLo);
    while GetValue(xHi) < xMid do
      dec(xHi);

    if xLo <= xHi then begin
      move(aData[xLo], xTmp, sizeof(rOffCausesT));
      move(aData[xHi], aData[xLo], sizeof(rOffCausesT));
      move(xTmp, aData[xHi], sizeof(rOffCausesT));
      inc(xLo);
      dec(xHi);
    end; //if xLo <= xHi then
  until xLo > xHi;

  if xHi > iLo then SortCauses(aData, iLo, xHi);
  if xLo < iHi then SortCauses(aData, xLo, iHi);
end; //procedure SortCauses
//-----------------------------------------------------------------------------

procedure TOffReportDlg.UpdateCaptions;
begin
  laMinOnlineDuration.Caption := Format_h(mProgOptions.OnlineTimeLimit);
  if mProgOptions.OnlineShowOutOfProdSpindles then
    laSpindleStatus.Caption := cShowAllSpindles
  else
    laSpindleStatus.Caption := cHideOutOfProdpindles;
  laMinOfflineDuration.Caption := Format_h(mProgOptions.OfflineTimeLimit);
  laOfflineRatio.Caption := formatfloat('0.00 %', mProgOptions.OfflineRatio);
end; //procedure TOffReportDlg.UpdateCaptions
//-----------------------------------------------------------------------------

procedure TOffReportDlg.UpdateInfoWindows;
begin
  paOnlineInfo.Visible := mProgOptions.ShowOfflimitInfoWindow;
end; //procedure TOffReportDlg.UpdateInfoWindows
//-----------------------------------------------------------------------------

constructor TOffReportDlg.Create(aOwner: TComponent);
begin
  mCreating := true;

  mProgOptions := TProgOptions.Create(Self);
  mStatisticPie := Nil;
  inherited Create(aOwner);

  tsOnline.HelpContext        := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Fenster_Online_Offlimit.htm');
  tsStatistic.HelpContext     := GetHelpContext('WindingMaster\Offlimit\OFFL_DLG_Fenster_Offline_Offlimit.htm');
  acLoadProfile.HelpContext   := GetHelpContext('globale-fenster-und-funktionen\DLG_Profile-bearbeiten.htm');
  acSaveProfile.HelpContext   := acLoadProfile.HelpContext;
  acDeleteProfile.HelpContext := acLoadProfile.HelpContext;

  mFloorMachines := '';

  PageControl1.ActivePage := tsOnline;
  wwDBGrid1.TitleColor    := cDefaultGridTitleColor;
  wwDBGrid2.TitleColor    := cDefaultGridTitleColor;
  grOfflimits.TitleColor  := cDefaultGridTitleColor;

  mOnlineSortOrder  := cDefaultSortOrder;
  mOfflineSortOrder := cDefaultSortOrder;
  mSortOrder        := soAscending;

  mOfflimits      := TOfflimits.Create;
  mOfflimitCauses := TOfflimitCauses.Create;
  mParameter      := TParameterList.Create;

  EnableActions;
  mCreating := false;
end; //constructor TOffReportDlg.Create
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Init(aMachines: string = '');
begin
//  EnterMethod('Init');
  mmGridPos1.MemoINI.Username := mmUsername;
  mmGridPos1.MemoINI.Formname := Classname;
  mFloorMachines              := aMachines;
  Init_tabOfflimits;
  LoadProfile(mmGridPos1.MemoINI.Username, mmGridPos1.MemoINI.GetDefaultProfile);
end; //procedure TOffReportDlg.Init
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//-----------------------------------------------------------------------------

// Ab hier SDo
//------------------------------------------------------------------------------

procedure TOffReportDlg.acToggleGridChartExecute(Sender: TObject);
begin
  mShowStatisticChart := not mShowStatisticChart;
  ShowStatisticGraphic;
end; //procedure TOffReportDlg.acDataPresentsExecute
//------------------------------------------------------------------------------

procedure TOffReportDlg.ShowStatisticGraphic;
begin
// paOfflineInfo.Visible   := aValue;
// wwDBGrid2.Visible := pStatistic_Tab.Visible;
  pnStatisticGrid.Visible := not mShowStatisticChart;
  pnStatisticChart.Visible := mShowStatisticChart;
  if mShowStatisticChart then begin
    //Zeige Tabelle Icon ist aber im Grafikmode
    acToggleGridChart.ImageIndex := 62;
    acToggleGridChart.Hint := cShowTabHint;
    //acToggleGridChart.Caption := Translate(cShowTabelleCaption);
    acResetSpindle.enabled := mmGraph1.Selection.Enabled;
  end
  else begin
   // Zeigt Grafik Icon ist aber im Tabellenmode
    acToggleGridChart.ImageIndex := 61;
    acToggleGridChart.Hint := cShowGraphicHint;
   //acToggleGridChart.Caption := Translate(cShowGraphicCaption);
    acResetSpindle.enabled := True;
  end;
  acResetWithFilter.enabled := acResetSpindle.enabled;
  acToggleGridChart.Caption := acToggleGridChart.Hint;
end; //procedure TOffReportDlg.ShowStatisticGraphic
//------------------------------------------------------------------------------

procedure TOffReportDlg.mmGraph11GraphClick(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; ARow, APoint: Integer);
begin
  // Angeklickter Balken fuer Kuchengrafik und Reset ermitteln
  if APoint <> -1 then begin
    if (not mmGraph1.Selection.Enabled) or // Balken nicht markiert
      ((Button = mmGraph1.Selection.Button) and // Balken markiert & PopupMenue
      (Shift = mmGraph1.Selection.Shift)) then begin

      quOffline.First;
      quOffline.MoveBy(aPoint);

       // Daten fuer Kuchengrafik ermitteln und anzeigen
      ShowPieValues;
    end;

    // Bar Select fuer reset
    if ((Button = mmGraph1.Selection.Button) and
      (Shift = mmGraph1.Selection.Shift)) or // Bar Select
      (Button = mbRight) and mmGraph1.Selection.Enabled then // PopupMenu & Bar select
      mmGraph1.Selection.Enabled := True
    else
      mmGraph1.Selection.Enabled := False;

    acResetSpindle.Enabled := mmGraph1.Selection.Enabled;
    acResetWithFilter.enabled := acResetSpindle.enabled;
    mmGraph1.Repaint;

    if Assigned(mStatisticPie) and acShowPie.Checked then
      mStatisticPie.Show;
  end;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.SortBars(aAction: TAction);
var
  xOrderWhere: String;
  xStr1: String;
  xMachineID: Integer;
  xSpindleID: Integer;
  xWhereList: TStringList;
begin
  xMachineID := 0;
  xSpindleID := 0;
  xWhereList := TStringList.Create;
  with quOffline do
  try
    // Datensatz nur merken, wenn Dataset offen
    if Active then begin
      xMachineID := quOfflinec_machine_id.asinteger;
      xSpindleID := quOfflinec_spindle_id.asinteger;
    end;// if Active then begin
    Active := False;

    if mFloorMachines <> '' then
      xWhereList.Add(Format('AND m.c_machine_id in (%s)', [mFloorMachines]));

    xOrderWhere := GetFirstStr(mOfflineSortOrder, ',');
    if mSortOrder = soDescending then
      xOrderWhere := xOrderWhere + ' DESC,'
    else
      xOrderWhere := xOrderWhere + ',';

    if aAction = acSortMachine then
      xStr1 := GetLastStr(mOfflineSortOrder, ',')
    else if aAction = acSortMachineError then
      xStr1 := 'mo.c_offline_offlimit_time'
    else if aAction = acSortError then begin
      xOrderWhere := '';
      xStr1       := 'mo.c_offline_offlimit_time';
    end else
      xStr1 := GetLastStr(mOfflineSortOrder, ',');

    xOrderWhere := xOrderWhere + xStr1;
    CommandText := Format(dmSpindleReport.slOfflineOfflimit.Text, [xWhereList.Text, xOrderWhere]);
    Parameters.ParamByName('MinOfflimitTime').value := mProgOptions.OfflineTimeLimit;
    Parameters.ParamByName('MinOfflineRatio').value := mProgOptions.OfflineRatio;

    Active := True;
  finally
    //tabsOfflineClick(nil);
    GetOfflimitAllSpindles;
    quOffline.Locate('c_machine_id;c_spindle_id', VarArrayOf([xMachineID, xSpindleID]), []);
  end; //try..finally
 //end; //else if PageControl1.ActivePage = tsOnline then

// GetFieldNames;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.ShowPieValues; //(aStatisticPie: TfStatisticPie);
var
  xTitle: string;
  xSerieID, xValue: Integer;
  xSerie: string;
begin
  if Assigned(mStatisticPie) then
    if mStatisticPie.Visible then begin
      mStatisticPie.PieSeries.Clear;
      mStatisticPie.mmChart1.Title.Text.Clear;

      // Titel fuer Kuchengrafik
      xTitle := quOfflinec_machine_name.value + '-' + quOfflinec_spindle_id.asstring;
      mStatisticPie.mmChart1.Title.Text.Add(xTitle);

      // Werte fuer Kuchengrafik
      for xSerieID := 0 to mParameter.Count - 1 do begin
        if (mParameter.Items[xSerieID].ColumnParam.Tag = cShowTableGraphic) and
           (mParameter.Items[xSerieID].ColumnParam.FieldName <> 'c_offline_offlimit_time') and
           (mParameter.Items[xSerieID].ColumnParam.FieldName <> 'calc_OfflineRatio') then begin
          xSerie := mParameter.Items[xSerieID].ColumnParam.FieldName;
          xValue := quOffline.FieldByName(xSerie).asInteger;

          if xValue > 0 then
            mStatisticPie.PieSeries.Add(xValue, mParameter.Items[xSerieID].ColumnParam.DisplayLabel,
              mParameter.Items[xSerieID].GraphicParam.Color);
        end; // end if mParameter
      end;
    end; // if Assigned
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.acShowPieExecute(Sender: TObject);
begin
  acShowPie.Checked := not acShowPie.Checked;

  // Button ist gedrueckt, nun Kuchengrafik erstellen
  if acShowPie.Checked then begin
    if not Assigned(mStatisticPie) then
      mStatisticPie := TfStatisticPie.Create(self);

    mStatisticPie.Visible := True;
    acShowPie.Caption := cPieOff;
    ShowPieValues;
  end
  else begin
//    mStatisticPie.Visible := False;
    FreeAndNil(mStatisticPie);
    acShowPie.Caption := cPieOn;
  end;
  acShowPie.Hint := acShowPie.Caption;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.acResetSpindleExecute(Sender: TObject);
var
  xMaName: string;

begin
  xMaName := quOfflinec_machine_name.AsString + ' - ' + quOfflinec_spindle_id.asstring;

  if MMMessageDlg(Format(cResetSpindle, [xMaName]), mtConfirmation, [mbYes, mbNo], 0, Self) = mrNo then begin
    mmGraph1.UnselectBar;
    if mShowStatisticChart then
      acResetSpindle.Enabled := False;
    exit;
  end; //if MMMessageDlg(Format(Translate(...

  with TmmAdoCommand.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := GetResetQuery;
    parameters.ParamByName('c_machine_id').value := quOfflinec_machine_id.AsInteger;
    parameters.ParamByName('c_spindle_id').value := quOfflinec_spindle_id.AsInteger;
    Execute;
  finally
    free;
  end;// with TmmAdoCommand.Create(nil) do try

  RunQuery(True);
  mmGraph1.UnselectBar;
  if mShowStatisticChart then
    acResetSpindle.Enabled := False;
end; //procedure TOffReportDlg.acResetSpindleExecute
//------------------------------------------------------------------------------

procedure TOffReportDlg.acSortExecute(Sender: TObject);
begin
// do not remove -> action used for enable/disable functionality
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.acSortMachineExecute(Sender: TObject);
begin
  mmGraph1.UnselectBar;
  (Sender as TAction).Checked := True;
  SortBars(Sender as TAction);
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.bSortClick(Sender: TObject);
begin
  (Sender as TToolButton).CheckMenuDropdown;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.WndProc(var Message: TMessage);
begin
  case Message.Msg of
    cPieFormClose: acShowPie.Execute;
  else
    inherited WndProc(Message);
  end;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid2RowChanged(Sender: TObject);
begin
  // Daten fuer Kuchengrafik ermitteln und anzeigen
  ShowPieValues;
end;
//------------------------------------------------------------------------------

procedure TOffReportDlg.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  acSetOfflimitCause.Enabled := not quOnline.IsEmpty;
  acShowPie.Enabled := (mParameter.Count <> 0);
end;
//------------------------------------------------------------------------------

function TOffReportDlg.ShowDialog(aGrid: TwwDBGrid; aShowTabGrid: boolean): Boolean;
var
  x, xrec: integer;
  xDummyPara: rEspParameterArrayT;
begin
  Result := False;
  with TmSettingsDialog.Create(self) do
  try
    ReportType := rtStatistic;
    Grid := aGrid;
    DataSet := aGrid.DataSource.DataSet;

    //re-read pos-changes from TDataSet
    xDummyPara := nil;
    Parameter.clear;
    if PageControl1.ActivePage = tsStatistic then
       //Parameter := u_lib.ReadParam(quOffline,mParaList,mmGraph1.Bars.Style)
      Parameter := u_lib.ReadParam(quOffline, mParaList, mmGraph1)
    else
      Parameter := u_lib.ReadParam(quOnline, xDummyPara, mmGraph1);
       //Parameter := u_lib.ReadParam(quOnline,xDummyPara,mmGraph1.Bars.Style);

    if not(aShowTabGrid) then
      mmPanel5.visible := false;

//    Result := (ShowModal = mrOK);
//    if Result then begin
    if ShowModal = mrOK then begin
      mParameter.Assign(Parameter);

      xrec := 0;
       // Grafikparameter in mParaList schreiben
      if PageControl1.ActivePage = tsStatistic then begin
        //mParaList := nil;
        for x := 0 to mParameter.Count - 1 do
          if (mParameter.Items[x].ColumnParam.Tag = cShowTableGraphic) and
            mParameter.Items[x].GraphicParam.ShowGraph then begin
            SetLength(mParaList, xRec + 1);
            mParaList[xRec] := mParameter.Items[x].GraphicParam;
            inc(xRec);
          end;

        mmGraph1.LeftYAxis.AutoScaling := Parameter.LeftYAxis.Autoscale;
        mmGraph1.LeftYAxis.DefaultMinY := Parameter.LeftYAxis.Min;
        mmGraph1.LeftYAxis.DefaultMaxY := Parameter.LeftYAxis.Max;

        mmGraph1.RightYAxis.AutoScaling := Parameter.RightYAxis.Autoscale;
        mmGraph1.RightYAxis.DefaultMinY := Parameter.RightYAxis.Min;
        mmGraph1.RightYAxis.DefaultMaxY := Parameter.RightYAxis.Max;

      end; //if PageControl1.ActivePage = tsStatistic then

      RunQuery(True);
      Result := True;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Dieser Filter ist fuer die Statistikanzeige
//******************************************************************************

procedure TOffReportDlg.PrepareOfflineReport(aGrid: TwwDBGrid);
var
  x, xMaxFields: integer;
  xStr: string;
begin
  xMaxFields := aGrid.DataSource.DataSet.FieldCount;
  with aGrid.DataSource.DataSet do
    for x := 0 to xMaxFields - 1 do begin
      xStr := uppercase(Fields[x].FieldName);
      // In Tab. und Graphic auswaehlbar
      if (xStr = 'C_YB_OFFLINE_TIME') or
        (xStr = 'C_EFF_OFFLINE_TIME') or
        (xStr = 'C_CSP_OFFLINE_TIME') or
        (xStr = 'C_RSP_OFFLINE_TIME') or
        (xStr = 'C_CBU_OFFLINE_TIME') or
        (xStr = 'C_CUPY_OFFLINE_TIME') or
        (xStr = 'C_TLST_OFFLINE_TIME') or
        (xStr = 'C_LST_OFFLINE_TIME') or
        (xStr = 'C_CSIRO_OFFLINE_TIME') or
        (xStr = 'C_CYTOT_OFFLINE_TIME') or
        (xStr = 'CALC_OFFLINERATIO') or
        (xStr = 'C_OFFLINE_OFFLIMIT_TIME') then
        Fields[x].Tag := cShowTableGraphic
      // Unsichtbare Spalten
      else if (xStr = 'C_MACHINE_ID') or
        (xStr = 'C_REDL_OFFLINE_TIME') then
        Fields[x].Tag := cNonVisible
      else if (xStr = 'C_MACHINE_NAME') or // bis hier SDo
        (xStr = 'C_SPINDLE_ID') then
        Fields[x].Tag := cShowKey
      else
        // Nur in Tab. sichtbar
        Fields[x].Tag := cShowTable;
    end; //for x:= 0 to xMaxFields - 1 do
end;
//-----------------------------------------------------------------------------

//******************************************************************************
// Dieser Filter ist fuer die Onlineanzeige
//******************************************************************************

procedure TOffReportDlg.PrepareOnlineReport(aGrid: TwwDBGrid);
var
  x, xMaxFields: integer;
  xStr: string;
begin
  xMaxFields := aGrid.DataSource.DataSet.FieldCount;
  with aGrid.DataSource.DataSet do
    for x := 0 to xMaxFields - 1 do begin
      xStr := uppercase(Fields[x].FieldName);
     // Nur in Tab. sichtbar
      if (xStr = 'CALC_OUTOFPRODUCTION') or
        (xStr = 'C_EFF') or
        (xStr = 'C_CSP') or
        (xStr = 'C_CYTOT') or
        (xStr = 'C_UPDATE_TIME') or
        (xStr = 'C_RSP') or
        (xStr = 'C_CBU') or
        (xStr = 'C_CUPY') or
        (xStr = 'C_CSIRO') or
        (xStr = 'C_LST') or
        (xStr = 'C_TLST') or
        (xStr = 'CALC_OFFCAUSE1') or
        (xStr = 'CALC_OFFCAUSE2') or
        (xStr = 'CALC_OFFCAUSE3') or
        (xStr = 'C_YB') or
        (xStr = 'C_MAX_ONLINE_OFFLIMIT_TIME') then
        Fields[x].Tag := cShowTable
           // nicht loeschbar
      else if (xStr = 'C_MACHINE_NAME') or
        (xStr = 'C_SPINDLE_ID') then
        Fields[x].Tag := cShowKey
      else
                // unsichtbar
        Fields[x].Tag := cNonVisible;
    end; //for x:= 0 to xMaxFields - 1 do
end;
// bis hier SDo
//-----------------------------------------------------------------------------

procedure TOffReportDlg.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;
//-----------------------------------------------------------------------------

procedure TOffReportDlg.quOnlineCalcFields(DataSet: TDataSet);
var
  i: integer;
  xStr: string;
  xCauses: aOffCausesT;
  xOfflimits: TOfflimits;
begin
  xCauses := Nil;
  if not(mCreating) then begin
    if quOnlinec_OutOfProduction.AsBoolean then
      quOnlinecalc_OutOfProduction.AsInteger := 1
    else
      quOnlinecalc_OutOfProduction.AsInteger := 0;

    // Automatisch ermittelte Offlimit Ursachen per ID als Text ausgeben
    xOfflimits := TOfflimits.Create;
    try
      ReadOnlineOfflimits(xOfflimits);
      xCauses := mOfflimitCauses.GetCauses(xOfflimits);
      for i := 0 to 2 do begin
        if high(xCauses) >= i then
          if (xCauses[i].TotalHitrate >= mProgOptions.OfflimitPattern.MinHitrate) then
            if xCauses[i].cntNumber > 0 then begin
              xStr := format('%s  (%d%s)', [xCauses[i].Name, round(xCauses[i].TotalHitrate), '%']);
              quOnline.fieldbyname('calc_Offcause' + inttostr(i + 1)).asstring := xStr;
            end; //if xCauses[i].cntNumber > 0 then begin
      end; // for i
    finally
      xOfflimits.Free;
    end; //try..finally
  end;// if not(mCreating) then begin
end; //procedure TOffReportDlg.quOnlineCalcFields
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acResetWithFilterExecute(Sender: TObject);
const
  cQry1 = 'SELECT c_machine_id, c_spindle_id FROM t_machine_offlimit ' +
          'WHERE c_spindle_id > 0 ';
var
  xOK: boolean;
  xStr: string;
  //......................................................
  function GetCompStr(aComp: eCompareT): string;
  begin
    case aComp of
      cmGreater: Result := '>';
      cmSmaller: Result := '<';
    else
    end; //case aComp of
  end; //function GetCompStr
  //......................................................
begin
  with TfrmDefResetFilter.Create(Self, mProgOptions.ResetFilter) do try
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then mProgOptions.ResetFilter := GetFilter;
  finally
    Free;
  end; //try..finally

  if not xOK then exit;

  if MMMessageDlg(cResetFilter, mtConfirmation, [mbYes, mbAbort], 0, Self) <> mrYes then exit;

  with TmmADODataSet.Create(Self) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := cQry1;
    if mProgOptions.ResetFilter.RunTimeValue > 0 then begin
      xStr := 'AND c_offline_watchtime %s %d';
      CommandText := CommandText + format(xStr, [GetCompStr(mProgOptions.ResetFilter.RuntimeCompare),
        mProgOptions.ResetFilter.RunTimeValue * 60]);
    end; //if mProgOptions.ResetFilter.RunTimeValue > 0 then begin

    if mProgOptions.ResetFilter.RatioValue > 0 then begin
      xStr := 'AND c_offline_offlimit_time %s (c_offline_watchtime /100.0 *%d)';
      CommandText := CommandText + format(xStr, [GetCompStr(mProgOptions.ResetFilter.RatioCompare),
        mProgOptions.ResetFilter.RatioValue]);
    end; //if mProgOptions.ResetFilter.RatioValue > 0 then begin

      // SQL.SaveToFile('c:\temp\xx.txt');
    Open;

    with TmmAdoCommand.Create(nil) do try
      ConnectionString := GetDefaultConnectionString;
      CommandText := GetResetQuery;
      while not Eof do begin
        parameters.ParamByName('c_machine_id').value := fieldbyname('c_machine_id').asinteger;
        parameters.ParamByName('c_spindle_id').value := fieldbyname('c_spindle_id').asinteger;
        Execute;

        Next;
      end; //while not Eof do begin
    finally
      free;
    end;// with TmmAdoCommand.Create(nil) do try
  finally
    Free;
    RunQuery(True);
  end; //try..finally
end; //procedure TOffReportDlg.acResetWithFilterExecute
//-----------------------------------------------------------------------------
function TOffReportDlg.GetResetQuery: string;
const
  cQry1 = 'SELECT * FROM t_machine_offlimit WHERE c_spindle_id = 0';
  cQry2 = 'UPDATE t_machine_offlimit SET %s ' +
          'WHERE c_spindle_id = :c_spindle_id ' +
          'AND c_machine_id = :c_machine_id';
var
  x: integer;
  xSQLTxt: string;
  xTabList,
  xFieldNames: TStringList;
begin
  xTabList := TStringList.Create;
  xFieldNames := TStringList.Create;
  xFieldNames.Sorted := True;
  xFieldNames.Duplicates := dupIgnore;

  // Alle Attribute der Tab. t_machine_offlimit ermitteln, welche in den
  // Attributen _offline_ und _offlimit_ enthalten sind
  with TmmAdoDataSet.Create(Self) do try
    ConnectionString := GetDefaultConnectionString;
    CommandText := cQry1;
    Open;

    GetFieldNames(xTabList);

      //Filter von _offline_ und _offlimit_
    for x := 0 to xTabList.Count - 1 do begin
      // dieses Feld darf nicht zurueckgesetzt werden, obwohl es dem Filter entspricht
      if CompareText('c_max_online_offlimit_time', xTabList.Strings[x]) <> 0 then begin
        if pos('_OFFLINE_', uppercase(xTabList.Strings[x])) > 0 then
          xFieldNames.Add(xTabList.Strings[x]);
        if pos('_OFFLIMIT_', uppercase(xTabList.Strings[x])) > 0 then
          xFieldNames.Add(xTabList.Strings[x]);
      end;
    end; //for x := 0 to xTabList.Count-1 do begin
    CodeSite.SendStringList('GetResetQuery', xFieldNames);

    for x := 0 to xFieldNames.Count - 1 do
      xSQLTxt := xSQLTxt + Format(' %s = 0,', [xFieldNames.Strings[x]]);

    system.Delete(xSQLTxt, Length(xSQLTxt), 1); // minus letztes Komma
    Result := format(cQry2, [xSQLTxt]);
    CodeSite.SendString('GetResetQuery.cQry1', Result);
  finally
    Free;
    xTabList.free;
    xFieldNames.free;
  end; //try..finally
end; //function TOffReportDlg.GetResetQuery
//-----------------------------------------------------------------------------

procedure TOffReportDlg.quOfflineAfterOpen(DataSet: TDataSet);
begin
  // Zeilenmarkierung fix einschalten
  wwDBGrid2.Options := wwDBGrid2.Options + [Wwdbigrd.dgRowSelect];
  laCountOffline.Caption := formatfloat(',0', quOffline.RecordCount);
  // ??!! After open query the DisplayNames are reset -> set back to translatet strings
  // !! only for query object quOffline !!
  GetFieldNames;
end; //procedure TOffReportDlg.quOfflineAfterOpen
//-----------------------------------------------------------------------------

procedure TOffReportDlg.quOnlineAfterOpen(DataSet: TDataSet);
begin
  // Zeilenmarkierung fix einschalten
  wwDBGrid1.Options := wwDBGrid1.Options + [Wwdbigrd.dgRowSelect];
  laCountOnline.Caption := formatfloat(',0', quOnline.RecordCount);
  CodeSite.SendString('quOnline.SQL', quOnline.CommandText);
end; //procedure TOffReportDlg.quOnlineAfterOpen
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Init_tabOfflimits;
var
  xOffInfo: eOfflimitInfoT;
begin
  with tabOfflimits do try
    DisableControls;
    if not Active then Open;

    // Datenzeilen mit ID einf�gen
    for xOffInfo := oiSpindle to oiUpperrelativeLimit do begin
      Append;
      tabOfflimitsc_ID.asinteger := ord(xOffInfo) + 1;
      Post;
    end; //for xOffInfo := oiSpindle to oiUpperrelativeLimit do begin

    First;
  finally
    EnableControls;
  end; //try..finally
end; //procedure TOffReportDlg.Init_tabOfflimits
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Translate_tabOfflimits;
  //......................................................
  procedure ModifyRec(aID: integer; aPosition: string);
  begin
    with tabOfflimits do begin
      Locate('c_id', aID, []);
      Edit;
      tabOfflimitsc_position.asstring := aPosition + ' ';
      Post;
    end; //with tabOfflimits do begin
  end; //procedure AppendRec
  //......................................................
begin
  with tabOfflimits do
    if Active then
    try
      DisableControls;
      First;

      ModifyRec(1, Format(cSpindelDataStr, [IvDictio.Translate(cLenBaseTxt[quOnlinec_base.AsInteger].Text)]));
      ModifyRec(2, format(cMeanDataStr, [quOnlinec_Average_time.AsInteger]));
      ModifyRec(3, cLowerFixedStr);
      ModifyRec(4, cUpperFixedStr);
      ModifyRec(5, cLowerRelativeStr);
      ModifyRec(6, cUpperRelativeStr);

      First;
        // fill in Header lines
      tabOfflimitsc_position.DisplayLabel := format(cMachineSpdInfoStr, [quOnlinec_machine_name.AsString, quOnlinec_spindle_id.AsInteger]);
    finally
      EnableControls;
      grOfflimits.Invalidate;
    end; //try..finally
end; //procedure TOffReportDlg.Translate_tabOfflimits
//-----------------------------------------------------------------------------

procedure TOffReportDlg.grOfflimitsCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
var
  xInfo: eOfflimitInfoT;
  xData: eOfflimitDataT;
begin
  if gdFixed in State then ABrush.Color := clLightYellow
                      else ABrush.Color := TwwDBGrid(Sender).Color;

  xInfo := eOfflimitInfoT(tabOfflimitsc_ID.asinteger - 1);
  ReadOnlineOfflimits(mOfflimits);
  xData := mOfflimits.Fields.TypeByFieldname(field.fieldname);
  if xData in [otYB..otCYTot] then begin
    if xInfo in mOfflimits.LimitFaults[xData] then begin
      ABrush.Color := clRed;
      AFont.Color := clWhite;
    end; //if xInfo in mOfflimits.LimitFaults[xData] then
  end; //if xData in [otYB..otCYTot] then begin

  if highlight then begin
    AFont.Color := clHighlightText;
    ABrush.Color := clHighlight;
  end; //if highlight then
end; //procedure TOffReportDlg.wwDBGrid3CalcCellColors
//-----------------------------------------------------------------------------

procedure TOffReportDlg.Update_grOfflimits;
var
  i, xIndex: integer;
  xData: eOfflimitDataT;
  xFieldName: string;
begin
  grOfflimits.TitleColor := wwDBGrid1.TitleColor;
  grOfflimits.Color := wwDBGrid1.Color;
  grOfflimits.TitleFont.Assign(wwDBGrid1.TitleFont);
  grOfflimits.Font.Assign(wwDBGrid1.Font);

  grOfflimits.Options := wwDBGrid1.Options;

  quOnline.DisableControls;
  tabOfflimits.DisableControls;
  try
    for xData := otYB to otCYTot do
      tabOfflimits.FieldByName(mOfflimits.Fields.FieldnameByType(xdata)).Visible := False;

    if Wwdbigrd.dgIndicator in wwDBGrid1.Options then
      xIndex := 1
    else
      xIndex := 0;
    tabOfflimitsc_position.Index := xIndex;

    for i := 0 to wwDBGrid1.FieldCount - 1 do begin
      xFieldName := wwDBGrid1.Fields[i].FieldName;
      xData := mOfflimits.Fields.TypeByFieldname(xFieldName);
      if xData in [otYB..otCYTot] then begin
        inc(xIndex);
        tabOfflimits.FieldByName(xFieldName).Visible := True;
        tabOfflimits.FieldByName(xFieldName).Index := xIndex;
        tabOfflimits.FieldByName(xFieldName).DisplayWidth := wwDBGrid1.Fields[i].DisplayWidth;
      end; //if xData in [otYB..otCYTot] then begin
    end; //for i := 0 to wwDBGrid1.FieldCount -1 do begin
  finally
    quOnline.EnableControls;
    tabOfflimits.EnableControls;
  end; //try..finally
end; //procedure TOffReportDlg.Update_grOfflimits
//-----------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid1ColumnMoved(Sender: TObject; FromIndex, ToIndex: Integer);
begin
  Update_grOfflimits;
end; //procedure TOffReportDlg.wwDBGrid1ColumnMoved
//-----------------------------------------------------------------------------

procedure TOffReportDlg.wwDBGrid1ColWidthChanged(Sender: TObject; Column: Integer);
begin
  Update_grOfflimits;
end; //procedure TOffReportDlg.wwDBGrid1ColWidthChanged
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acDefPatternRecognitionExecute(Sender: TObject);
var
  xOK: boolean;
begin
  with TfrmDefPatternRecognition.Create(Self, mProgOptions.OfflimitPattern) do try
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then mProgOptions.OfflimitPattern := GetOfflimitPattern;
  finally
    Free;
  end; //try..finally

  if not xOK then exit;

  mOfflimitCauses.OfflimitPattern := mProgOptions.OfflimitPattern;
  Fill_sgOffCause;
  RunQuery(True);
end; //procedure TOffReportDlg.acDefPatternRecognitionExecute
//-----------------------------------------------------------------------------

procedure TOffReportDlg.acSetOfflimitCauseExecute(Sender: TObject);
var
  xCauseID: integer;
begin
  xCauseID := -1;
  with TfrmSetOfflimitCause.Create(Self, quOnlinec_machine_name.asstring, quOnlinec_spindle_id.asinteger) do try
    if ShowModal = mrOK then
      AssignOfflimitCause(GetCauseID);
//    xCauseID := GetCauseID;
  finally
    Free;
  end; //try..finally

//  if xCauseID > -1 then
//    AssignOfflimitCause(xCauseID);
end; //procedure TOffReportDlg.acSetOfflimitCauseExecute
//-----------------------------------------------------------------------------

//******************************************************************************
// Errechnet die Abweichung in % vom Mittelwert sowie einen Wert fuer den
// Lerneffekt und legt diese Werte in die Tab. t_offlimit_cause ab
// Es wird auch die Anzahl der Merkmale im Offlimit gespeichert fuer eine
// fallbasierte Ursachen-Analyse
//******************************************************************************

procedure TOffReportDlg.AssignOfflimitCause(aCauseID: integer);
const
  cQry1 = 'UPDATE t_machine_offlimit ' +
    'SET c_offcause_id = %d ' +
    'WHERE c_machine_id = %d ' +
    'AND c_spindle_id = %d';

  cQry2 = 'SELECT * FROM t_offlimit_cause ' +
    'WHERE c_offcause_id = %d';
var
  xData: eOfflimitDataT;
  xAvgDev: array[eOfflimitDataT] of double;
  xOffCnt: array[eOfflimitDataT] of integer;
  xCounter: integer;
  xNumStr,
    xFieldname: string;
  xDev: double;
  xField,
    xField_avg: Real;
begin
  dmSpindleReport.conDefault.BeginTrans;
  try
    fillchar(xAvgDev, sizeof(xAvgDev), 0);
    with quUpdate do begin
//      Database.StartTransaction;
      //2. set cause-id in t_machine_offlimit
      CommandText := format(cQry1, [aCauseID, quOnlinec_machine_id.value, quOnlinec_spindle_id.value]);
      CodeSite.SendString('SetOffCause: quUpdate', CommandText);
      Execute;

      with TmmAdoDataSet.Create(nil) do try
        ConnectionString := GetDefaultConnectionString;
        //3. get cause-values
        CommandText := format(cQry2, [aCauseID]);
        Active := true;

        xCounter := FieldByName('c_Counter').asinteger;
        xOffCnt[otOP] := FieldByName('c_op_offcnt').asinteger;

        //4. calculate new cause-values
        for xData := otYB to otCYTot do begin
          xFieldname := Uppercase(mOfflimits.Fields.FieldnameByType(xData));
          xAvgDev[xData] := FieldByName(xFieldname + '_dev').asfloat;
          xOffCnt[xData] := FieldByName(xFieldname + '_offcnt').asinteger;

          if not quOnline.fieldbyname(xFieldname).isnull then begin
            //calculate deviation against avg.value
            try
              xField := quOnline.fieldbyname(xFieldname).Asfloat;
              xField_avg := quOnline.fieldbyname(xFieldname + '_avg').Asfloat;

              // @@ErrorDIV0
              if xField_avg <> 0 then begin
                // Abweichung in % vom Mittelwert
                xDev := (xField / xField_avg - 1) * 100;
                // Formel fuer den Lerneffekt
                xDev := (xAvgDev[xData] * xCounter + xDev) / (xCounter + 1);
                xAvgDev[xData] := xDev;
              end; //if xField_avg <> 0 then begin
            except
              // skip error
            end; //try..except

            if cOfflimitTypes * mOfflimits.LimitFaults[xData] <> [] then inc(xOffCnt[xData]);

          end; //if not quOnline.fieldbyname(xFieldname).isnull then
        end; //for xData := otYB to otCYTot do

        if quOnlinec_OutOfProduction.asboolean then inc(xOffCnt[otOP]);

        inc(xCounter);

        //5. write new cause-values
        Active := False;
      finally
        free;
      end;// with TmmAdoDataSet.Create(nil)

      CommandText := 'UPDATE t_offlimit_cause SET';

      for xData := otYB to otCYTot do begin
        xFieldname := mOfflimits.Fields.FieldnameByType(xData);
        xNumStr := FloatToNumStr(xAvgDev[xData]);
        CommandText := CommandText + format(' %s = %s,', [xFieldname + '_dev', xNumStr]);

        xNumStr := inttostr(xOffCnt[xData]);
        CommandText := CommandText + format(' %s = %s,', [xFieldname + '_offcnt', xNumStr]);
      end; //for xData := otYB to otCYTot do

      CommandText := CommandText + format(' c_op_offcnt = %d,', [xOffCnt[otOP]]);
      CommandText := CommandText + format(' c_Counter = %d', [xCounter]);
      CommandText := CommandText + format(' WHERE c_offcause_id = %d', [aCauseID]);

      // SQL.savetofile('c:\temp\xx.txt');

      Execute;
    end; //with quUpdate do

    dmSpindleReport.conDefault.CommitTrans;
  except
    dmSpindleReport.conDefault.RollbackTrans;
  end; //try..except

  mOfflimitCauses.ReadOfflimitCauses;
  RunQuery(True);
end; //procedure TOffReportDlg.AssignOfflimitCause
//------------------------------------------------------------------------------

procedure TOffReportDlg.OnGetOnlineText(Sender: TField; var Text: string; DisplayText: Boolean);
var
  xData: eOfflimitDataT;
  //................................
  // Zu jedem Datenfeld geh�rt auch ein XX_online_time Feld. Ist die Zeit > 0
  // dann ist dieser Wert im Offlimit und wird markiert.
//  function IsInOfflimit: Boolean;
//  begin
//    Result := False;
//    with quOnline do
//    try
//      Result := (FieldByName(Sender.FieldName + '_online_time').AsInteger > 0);
//    except
//    end;
//  end;
  //................................
begin
  if Sender.AsInteger = 0 then
    Text := '-'
  else begin
    Text := FormatFloat(TNumericField(Sender).DisplayFormat, Sender.AsFloat);
    xData := mOfflimits.Fields.TypeByFieldname(Sender.Fieldname);
    if xData in [otYB..otCYTot] then
    try
      ReadOnlineOfflimits(mOfflimits);
      if cOfflimitTypes * mOfflimits.LimitFaults[xData] <> [] then begin
        Text := '* ' + Text;
      end;
    finally
    end; //if xData in [otYB..otCYTot] then begin
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TOffReportDlg.quOnlineBeforeOpen(DataSet: TDataSet);
begin
  // Wenn vom Profil her keine Formatierung erfolgt, dann default setzten.
  // Vorhergehende Version war ein Integer Feld, da war keine Formatierung n�tig.
  if quOnlinec_tLSt.DisplayFormat = '' then
    quOnlinec_tLSt.DisplayFormat := '0';
end;

procedure TOffReportDlg.wwDBGrid2DblClick(Sender: TObject);
begin
  if not Assigned(mStatisticPie) then
    acShowPie.Execute;
end;

procedure TOffReportDlg.mmGraph1GraphDblClick(Sender: TObject;
  AItem: eDblClickT; ARow, APoint: Integer; AStatistik: eStatistikT);
begin
  if APoint <> -1 then
    if (not mmGraph1.Selection.Enabled) and (not Assigned(mStatisticPie)) then begin
      acShowPie.Execute;
    end;
end;

end. //u_offlimit_report


