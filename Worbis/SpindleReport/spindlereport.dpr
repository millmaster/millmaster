program spindlereport;
 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  mmCS,
  Forms,
  LoepfeGlobal,
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  BaseApplMain in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  u_MAIN in 'u_MAIN.pas' {frmMain},
  u_lib in 'u_lib.pas',
  u_global in 'u_global.pas',
  u_offlimit_report in 'u_offlimit_report.pas' {OffReportDlg},
  u_set_printoptions in '..\common\u_set_printoptions.pas' {SetPrintOptionsDlg},
  u_esp_machine_report in 'u_esp_machine_report.pas' {Esp_MachineReportDlg},
  u_common_lib in '..\common\u_common_lib.pas',
  u_common_global in '..\common\u_common_global.pas',
  u_esp_spindle_report in 'u_esp_spindle_report.pas' {Esp_SpindleReportDlg},
  u_select_interval in 'u_select_interval.pas' {SelectIntervalDlg},
  u_resourcestrings in 'u_resourcestrings.pas',
  u_offlimit_settings_editor in 'u_offlimit_settings_editor.pas' {OffLimitEditorDlg},
  u_def_offlimit_filter in 'u_def_offlimit_filter.pas' {DefOfflimitReportDlg},
  u_edit_offlimit_cause in 'u_edit_offlimit_cause.pas' {EditOfflimitCauseDlg},
  u_standard_select in 'u_standard_select.pas' {StandardSelectDlg},
  u_select_prodid in 'u_select_prodid.pas' {SelectProdIDDlg},
  u_reiniger_settings in 'u_reiniger_settings.pas' {ReinigerSettingsDlg},
  u_StatisticPie in 'u_StatisticPie.pas' {fStatisticPie},
  u_SettingsDialog in 'u_SettingsDialog.pas' {mSettingsDialog},
  SpindleReportIMPL in 'SpindleReportIMPL.pas',
  u_def_reset_filter in 'u_def_reset_filter.pas' {frmDefResetFilter},
  u_def_pattern_recognition in 'u_def_pattern_recognition.pas' {frmDefPatternRecognition},
  u_set_offlimit_cause in 'u_set_offlimit_cause.pas' {frmSetOfflimitCause},
  u_print_list in 'u_print_list.pas' {frmPrintList},
  u_dmSpindleReport in 'u_dmSpindleReport.pas' {dmSpindleReport: TDataModule},
  u_OfflimitPrintFormular in 'u_OfflimitPrintFormular.pas' {OfflimitPrintFormular},
  RepSettingsUnit in '..\..\Common\RepSettingsUnit.pas' {RepSettings: TFrame},
  PrintTemplateForm in '..\..\Common\PrintTemplateForm.pas' {frmPrint};

{$R *.TLB}

{$R *.RES}
{$R 'Version.res'}

begin
  gApplicationName := 'Spindlereport';
  CodeSite.Enabled := CodeSiteEnabled;
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}

  Application.Initialize;
  Application.Title := 'Spindlereport';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TdmSpindleReport, dmSpindleReport);
  Application.Run;
end. //spindlereport

