(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_MAIN.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  5.04.99 | 0.00 | PW  | Datei erstellt
|          | 0.01 | PW  |
| 11.09.01 | 0.00 | Wss | Bei AfterLanguageChange Index fuer Helpdatei an MMHtmlHelp setzen
| 17.10.02 |      | LOK | Umbau ADO
| 03.10.03 | 0.00 | Wss | Refresh Security in UpdateSecurity 
|=========================================================================================*)

{$I symbols.inc}

unit u_main;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEAPPLMAIN, mmLogin, MMSecurity, IvDictio, IvMulti,
  IvEMulti, IvAMulti, IvBinDic, Menus, mmPopupMenu, mmOpenDialog, mmSaveDialog,
  mmPrintDialog, mmPrinterSetupDialog, Db,
  mmMainMenu, StdActns, ActnList, mmActionList, ImgList, mmImageList, ComCtrls,
  mmStatusBar, ToolWin, mmToolBar, mmAnimate, ExtCtrls, mmPanel, MMMessages,
  StdCtrls, mmDictionary, mmTranslator, IvMlDlgs, MMHtmlHelp;

type
  TfrmMain = class(TBaseApplMainForm)
    acEspReport: TAction;
    acOffReport: TAction;
    miEspReport: TMenuItem;
    miOffReport: TMenuItem;
    acSecurityConfig: TAction;
    Benutzerrechte1: TMenuItem;
    MMSecurityDB: TMMSecurityDB;
    MMHtmlHelp: TMMHtmlHelp;
    MMSecurityControl: TMMSecurityControl;
    procedure acEspReportExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acOffReportExecute(Sender: TObject);
    procedure acPreViewExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure acSecurityConfigExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure mDictionaryAfterLangChange(Sender: TObject);
  private
  protected
    procedure InitStandalone; override;
    procedure UpdateSecurity; override;
  public
  end; //TfrmMain

var
  frmMain: TfrmMain;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  BaseForm, LoepfeGlobal, mmDialogs,
  MMEventLog,
  u_common_global, u_common_lib,
  u_global,
  u_lib, u_esp_machine_report, u_offlimit_report, u_dmSpindleReport;

{$R *.DFM}

resourcestring
  rsProductName = '(*)Spulstellen- und Maschinen Offlimit Bericht'; //ivlm

//-----------------------------------------------------------------------------
procedure TfrmMain.acEspReportExecute(Sender: TObject);
var
  xForm: TEsp_MachineReportDlg;
begin

  if GetCreateChildForm(TEsp_MachineReportDlg, TForm(xForm), Self) then begin
    xForm.Init('', 0);
    xForm.Visible := True;
  end;
end; //procedure TMainForm.acEspReportExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.acOffReportExecute(Sender: TObject);
var
  xForm: TOffReportDlg;
begin
  if GetCreateChildForm(TOffReportDlg, TForm(xForm), Self) then begin
    xForm.Init;
    xForm.Visible := True;
  end;
end; //procedure TMainForm.acOffReportExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  acEspReport.HelpContext := GetHelpContext('WindingMaster\Spulstellenbericht\SPST_Spulstellenbericht.htm');
  
  gTempPath   := SetTempPath;
  ProductName := rsProductName;
end; //procedure TMainForm.FormCreate
//-----------------------------------------------------------------------------
procedure TfrmMain.InitStandalone;
begin
  inherited InitStandalone;

  Application.HintPause := 0;
  Application.Helpfile := cHelpfilename;

  //Neu, Oeffnen und Separatoren im Menu auf unsichtbar stellen
  acNew.Visible := False;
  acOpen.Visible := False;
  acSave.Visible := False;
  acSaveas.Visible := False;
  acPreView.Visible := False;
  acPrinterSetup.Visible := False;
  acPrint.Visible := False;
  N11.Visible := False;
  N21.Visible := False;
  N31.Visible := False;
  N41.Visible := False;
end; //procedure TMainForm.InitStandalone
//-----------------------------------------------------------------------------
procedure TfrmMain.acPreViewExecute(Sender: TObject);
begin
  acNewExecute(Sender);
end; //procedure TMainForm.acPreViewExecute
//-----------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  Application.OnHelp := nil;
  inherited;
end; //procedure TMainForm.FormDestroy
//-----------------------------------------------------------------------------
procedure TfrmMain.acSecurityConfigExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mDictionaryAfterLangChange(Sender: TObject);
begin
  MMHtmlHelp.LoepfeIndex := mDictionary.LoepfeIndex;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.UpdateSecurity;
begin
  MMSecurityControl.Refresh;
end;
//------------------------------------------------------------------------------
end. //u_main

