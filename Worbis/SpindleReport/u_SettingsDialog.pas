(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_SettingsDialog.pas
| Projectpart...: Parameter-Dialog fuer die Spulerei und Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01, Multilizer
|-------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis. | Reason
|-------------------------------------------------------------------------------
| 10.04.2000 | 0.00 | SDo  | File created
|  5.05.2000 | 0.01 | PW   | Form-Layout angepasst,
|                          | Typ-Definitionen => in u_global.pas
|                          | Border-Style = bsSizeable
|                          | dataSet property added for using the dialog without a twwdbgrid component
| 03.07.00   | 1.02 | SDO  | Defaultfarben werden in u_lib.pas (ReadParam) eingebaut
|                          | Definition der Farben befinden sich in mmGraphGlobal.pas
|                          | In SH_ColsDragDrop Defaultfarben setezn
| 06.07.00   | 1.03 | SDO  | Uebergabe der Grafik-YAchsen
| 25.11.02   | 1.03 | Wss  | - cbRowSelect ausgeblendet, da es Probleme mit dem Grid geben kann, wenn
                               NICHT die ganze Zeile markiert ist
                             - Scrollbug in Checklistbox behoben -> Focus auf falschen Control beim 1. Anzeigen   
| 30.06.03   | 1.03 | Wss  | ListIndex Bug beim entfernen von Elementen behoben
|=============================================================================*)
unit u_SettingsDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, StdCtrls, CheckLst, mmCheckListBox, ToolWin, ComCtrls,
  mmPageControl, mmButton, ExtCtrls, mmPanel, Buttons, mmSpeedButton,
  Nwcombo, NumCtrl, mmCheckBox, mmLabel, mmRadioGroup, mmGroupBox,
  mmComboBox, mmUpDown, mmEdit, IvMlDlgs, mmFontDialog, mmColorDialog,
  IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList, mmActionList, Grids,
  AdvGrid, mmList, DB,

  Wwdbgrid, Wwdbigrd, u_global, u_common_lib, mmGraphGlobal, mmBitBtn,
  mmBevel,
  u_lib, Mask, mmMaskEdit, mmIvMlDlgs, mmExtStringGrid;

type
  TmSettingsDialog = class(TmmForm)
    mmPanel6: TmmPanel;
    ActionList1: TmmActionList;
    acOK: TAction;
    acHelp: TAction;
    acMoveTo_lbShow: TAction;
    acMoveTo_lbAvailable: TAction;
    acMoveAllTo_lbShow: TAction;
    acMoveAllTo_lbAvailable: TAction;
    mmTranslator1: TmmTranslator;
    ColorDialog1: TmmColorDialog;
    FontDialog1: TmmFontDialog;
    acColorPanel: TAction;
    acFont: TAction;
    acSetParam: TAction;
    paMain: TmmPanel;
    pcSettings: TmmPageControl;
    tsTabFormat: TTabSheet;
    Panel3: TPanel;
    gbGridSettings: TmmGroupBox;
    gbSchrift: TmmGroupBox;
    paTitleColor: TmmPanel;
    laTitleFont: TmmLabel;
    bTitelFont: TmmBitBtn;
    bTitelColor: TmmBitBtn;
    paColor: TmmPanel;
    laFont: TmmLabel;
    bDatenFont: TmmBitBtn;
    bDataColor: TmmBitBtn;
    gbGridOptions: TmmGroupBox;
    cbRowLines: TmmCheckBox;
    cbColLines: TmmCheckBox;
    cbIndicator: TmmCheckBox;
    cbRowSelect: TmmCheckBox;
    cbAlwaysShowSelection: TmmCheckBox;
    cbTitleAlignment: TmmCheckBox;
    cbTrailingEllipsis: TmmCheckBox;
    gbFixedCols: TmmGroupBox;
    edFixedCols: TmmEdit;
    udFixedCols: TmmUpDown;
    tsColParam: TTabSheet;
    mmPanel2: TmmPanel;
    bMoveTo_lbShow: TmmSpeedButton;
    bMoveTo_lbAvailable: TmmSpeedButton;
    mmSpeedButton9: TmmSpeedButton;
    bMoveAllTo_lbAvailable: TmmSpeedButton;
    bMoveUp: TmmSpeedButton;
    bMoveDown: TmmSpeedButton;
    mmGroupBox7: TmmGroupBox;
    lbAvailable: TNWListBox;
    mmGroupBox8: TmmGroupBox;
    lbShow: TmmCheckListBox;
    mmPanel5: TmmGroupBox;
    mmCheckBox2: TmmCheckBox;
    mmCheckBox1: TmmCheckBox;
    mmPanel3: TmmPanel;
    pcParameter: TPageControl;
    tsTabParam: TTabSheet;
    rgAlignment: TmmRadioGroup;
    rgFloat: TmmRadioGroup;
    rgInteger: TmmRadioGroup;
    rgDate: TmmRadioGroup;
    rgDateTime: TmmRadioGroup;
    rgTime: TmmRadioGroup;
    tsGraficParam: TTabSheet;
    tsSummary: TTabSheet;
    sgSummary: TmmExtStringGrid;
    rgBarStyle: TmmRadioGroup;
    bOk: TmmButton;
    bCancel: TmmButton;
    mmButton3: TmmButton;
    rgChartType: TRadioGroup;
    rgYScaling: TmmRadioGroup;
    gbStatistik: TmmGroupBox;
    clbStatData: TmmCheckListBox;
    mmGroupBox13: TmmGroupBox;
    mmLabel2: TmmLabel;
    cbLowerLimit: TmmCheckBox;
    edLowerLimit: TNumEdit;
    mmGroupBox12: TmmGroupBox;
    mmLabel1: TmmLabel;
    cbUpperLimit: TmmCheckBox;
    edUpperLimit: TNumEdit;
    mmGroupBox11: TmmGroupBox;
    paColor1: TmmPanel;
    mmBitBtn1: TmmBitBtn;
    gbGraphicSettings: TmmGroupBox;
    gbLine: TmmGroupBox;
    gbBar: TmmGroupBox;
    chbAutoScaleLine: TmmCheckBox;
    laLineMin: TmmLabel;
    laLineMax: TmmLabel;
    edLineMin: TNumEdit;
    edLineMax: TNumEdit;
    chbAutoScaleBar: TmmCheckBox;
    laBarMin: TmmLabel;
    laBarMax: TmmLabel;
    edBarMin: TNumEdit; //TmmEdit;
    edBarMax: TNumEdit;

    // Actions
    procedure acColorPanelExecute(Sender: TObject);
    procedure acFontExecute(Sender: TObject);
    procedure acMoveAllTo_lbShowExecute(Sender: TObject);
    procedure acMoveAllTo_lbAvailableExecute(Sender: TObject);
    procedure acMoveTo_lbShowExecute(Sender: TObject);
    procedure acMoveTo_lbAvailableExecute(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acSetParamExecute(Sender: TObject);

    // Forms
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormDestroy(Sender: TObject);

    // Listbox
    procedure lbShowClick(Sender: TObject);
    procedure lbShowClickCheck(Sender: TObject);

    // TabSheets
    procedure pcSettingsChange(Sender: TObject);
    procedure tsColParamExit(Sender: TObject);
    procedure tsColParamEnter(Sender: TObject);
    procedure tsSummaryExit(Sender: TObject);

    // RadioGroup
    procedure rgYScalingClick(Sender: TObject);

    // Grid
    procedure sgSummaryGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure sgSummaryDblClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure sgSummaryGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var AAlignment: TAlignment);

    // Button
    procedure bMoveUpClick(Sender: TObject);
    procedure bTitelFontClick(Sender: TObject);
    procedure bDatenFontClick(Sender: TObject);
    procedure bTitelColorClick(Sender: TObject);
    procedure bDataColorClick(Sender: TObject);
    procedure rgChartTypeClick(Sender: TObject);
    procedure rgBarStyleClick(Sender: TObject);
    procedure chbAutoScaleLineClick(Sender: TObject);
    procedure chbAutoScaleBarClick(Sender: TObject);
    procedure tsTabFormatEnter(Sender: TObject);

  private
    { Private declarations }
    FParameter: TParameterList;
    FGrid: TwwDBGrid;
    FDataSet: TDataSet;
    mOldShowListIndex: Integer;
    FReportType: TReportType;

    // Properies Proc.
    function GetParameter: TParameterList;
    procedure SetParameter(Value: TParameterList);

    function GetDataSet: TDataSet;
    procedure SetDataSet(const Value: TDataSet);

    function GetGrid: TwwDBGrid;
    procedure SetGrid(const Value: TwwDBGrid);
    procedure SetReportType(const Value: TReportType);

    procedure Init;
    procedure ReadParam;
    procedure SetParam(aItemIndex: integer);
    procedure ShowParam(aItemIndex: integer);
    procedure SetControls;
    procedure SetAllParamsBeforeClose;

    procedure MoveAllItems(aSource, aTarget: TNWListBox);
    procedure SH_ColsDragDrop(aSource, aTarget: TObject);
    procedure MoveSelectedItem(aOldPos, aDropPos: integer);

    procedure EnableActions;

    procedure SetLabelPos(l: TLabel);

    procedure Update_VisibleColumns;
    function GetClickedParameterID(aItemIndex: integer): integer;

    procedure WriteParamToSummary;

    procedure SetGridFont(aButton: TmmBitBtn);
    procedure SetGridColor(aButton: TmmBitBtn);
    procedure AfterItemMove;
  public
    { Public declarations }
    property Parameter: TParameterList read GetParameter write SetParameter;
    property Grid: TwwDBGrid read GetGrid write SetGrid;
    property DataSet: TDataSet read GetDataSet write SetDataSet;
    property ReportType: TReportType read FReportType write SetReportType;
  end;

var
  mSettingsDialog: TmSettingsDialog;

resourcestring
  cLineStr = '(10)Linien'; //ivlm
  cBarStr = '(10)Balken'; //ivlm
  cDescriptionStr = '(25)gr_Description'; //ivlm
  cChartTypeStr = '(20)gr_ChartType'; //ivlm
  cColorStr = '(10)gr_Color'; //ivlm
  cYScalingStr = '(20)gr_YScaling'; //ivlm
  cStatistikStr = '(20)gr_Statistik'; //ivlm
  cLowerLimitStr = '(20)gr_LowerLimit'; //ivlm
  cUpperLimitStr = '(20)gr_UpperLimit'; //ivlm
  cShowChartStr = '(20)gr_ShowChart'; //ivlm
  cColumnAlign = '(20)Spaltenausrichtung'; //ivlm
  cShowInTable = '(20)in Tab. anzeigen'; // ivlm
  cColumnFormat = '(20)Spaltenformat'; // ivlm

implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}
uses
  mmMBCS,
  u_common_global, Consts;

const cStatName: array[stMin..stQ99] of s3 = ('Min', 'Max', 'X', 's', 'q90', 'q95', 'q99');

{ TmSettingsDialog }

procedure TmSettingsDialog.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  inherited;
  if ModalResult = mrOK then SetAllParamsBeforeClose;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.FormDestroy(Sender: TObject);
begin
  FParameter.Free;
  inherited;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.MoveAllItems(aSource, aTarget: TNWListBox);
var
  xStart,
    i: integer;
begin
 //prevent moving all cols to lbAvailable, there should allways be one visible col in the grid

  aSource.Selected[0] := False;
  if aSource = TNWListBox(lbShow) then
    xStart := 1
  else
    xStart := 0;
  aSource.Items.BeginUpdate;
  for i := xStart to aSource.Items.Count - 1 do
    aSource.Selected[i] := True;

  aSource.Items.EndUpdate;

  SH_ColsDragDrop(aSource, aTarget);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SH_ColsDragDrop(aSource, aTarget: TObject);
var
// xDropPos   : integer;
  i, k: integer;
  b: boolean;
  s: string;
  xFieldName: string;
// xParamID,
  xres, x, xIndex: integer;
begin

 // lbShow      -> TmmCheckListBox
 // lbAvailable -> TNWListBox

 // von lbAvailable nach lbShow
  if (aSource is TNWListBox) and (aTarget is TmmCheckListBox) then begin
    // Items in lbShow einfuegen
    for i := 0 to TNWListBox(aSource).Items.Count - 1 do begin
      b := TNWListBox(aSource).Selected[i];
      s := TNWListBox(aSource).Items[i];
      k := TNWListBox(aSource).GetItemIDAtPos(i);
      if b then begin
        TmmCheckListBox(aTarget).Items.AddObject(s, TObject(k));
      end;
    end;

    // Ausgewaehlte Items in lbAvailable loeschen
    while TNWListBox(aSource).SelCount > 0 do
      for i := TNWListBox(aSource).Items.Count - 1 downto 0 do begin
        b := TNWListBox(aSource).Selected[i];
        if b then TNWListBox(aSource).Items.Delete(i);
        if TNWListBox(aSource).SelCount = 0 then break;
      end; //for i := TNWListBox(Source).Items.Count -1 downto 0 do
  end
  else begin
     // von lbShow nach lbAvailable
    // Items in lbAvailable einfuegen
    i := TmmCheckListBox(aSource).Items.Count;
    while i > 0 do begin
      dec(i);
      b := TmmCheckListBox(aSource).Selected[i];
      s := TmmCheckListBox(aSource).Items[i];
      k := integer(TmmCheckListBox(aSource).Items.Objects[i]);

      if b and (FParameter.Items[k].ColumnParam.Tag <> cShowKey) then begin
        TNWListBox(aTarget).AddObj(s, k);
             // Ausgewaehlte Items in lbShow loeschen
        TmmCheckListBox(aSource).Items.Delete(i);
        FParameter.Items[k].SelectedColumn := False;
        FParameter.Items[k].ColumnParam.Visible := False;
        xFieldName := FParameter.Items[k].ColumnParam.FieldName;

             //lbShow.ItemIndex := i-2;
        with FParameter.Items[k].GraphicParam do begin
          x := -1;
                 // Defaultfarbe ermitteln und setzen
          repeat
            inc(x);
            xres := StrComp(PChar(Uppercase(xFieldName)),
              PChar(Uppercase(cDefaultColor[x].Fieldname)));
          until (xres = 0) or (x >= High(cDefaultColor));

          Color := cDefaultColor[x].Color;
          ChartType := ctBar;
          LowerLimit := 0;
          UpperLimit := 0;
          YScalingType := ysNone;
          Statistik := [];
          ShowGraph := False;
        end;
      end;
    end;
    // Wenn nur noch Key's lbShow vorhanden sind, dann lbShow.ItemIndex:=0
    xIndex := 0;
    for x := 0 to lbShow.Items.Count - 1 do begin
      k := integer(TmmCheckListBox(aSource).Items.Objects[x]);
      if (FParameter.Items[k].ColumnParam.Tag = cShowKey) then inc(xIndex);
    end;
    if xIndex = lbShow.Items.Count then begin
      lbShow.ItemIndex := 0;
      mOldShowListIndex := 0;
    end;
  end;

  EnableActions;
  Update_VisibleColumns;
  lbShow.OnClick(nil);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acMoveAllTo_lbShowExecute(Sender: TObject);
begin
  MoveAllItems(lbAvailable, TNWListBox(lbShow));
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acMoveAllTo_lbAvailableExecute(Sender: TObject);
var xBool: Boolean;
begin
  MoveAllItems(TNWListBox(lbShow), lbAvailable);

  xBool := lbShow.MultiSelect;
  lbShow.MultiSelect := False;
  if lbShow.Items.Count > 0 then begin
    lbShow.ItemIndex := 0;
    lbShow.MultiSelect := xBool;
    lbShow.Selected[0];
  end;
  AfterItemMove;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acMoveTo_lbShowExecute(Sender: TObject);
begin
  SH_ColsDragDrop(lbAvailable, lbShow);
  if lbShow.Items.Count = 1 then begin
    SetParam(0);
    ShowParam(0);
  end;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acMoveTo_lbAvailableExecute(Sender: TObject);
var x: integer;
begin
// if lbShow.Items.Count = 1 then exit;

  SH_ColsDragDrop(lbShow, lbAvailable);
  x := lbShow.ItemIndex;
  if x > 0 then lbShow.ItemIndex := x;
  AfterItemMove;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.Init;
begin
  with sgSummary do begin
    ColWidths[0] := 120;
    RowHeights[0] := DefaultRowHeight shl 1;
    Cells[0, 0] := Translate(cDescriptionStr);
    Cells[1, 0] := Translate(cChartTypeStr);
    Cells[2, 0] := Translate(cColorStr);
    Cells[3, 0] := Translate(cStatistikStr);
    Cells[4, 0] := Translate(cYScalingStr);
    Cells[5, 0] := Translate(cLowerLimitStr);
    Cells[6, 0] := Translate(cUpperLimitStr);
    Cells[7, 0] := Translate(cShowChartStr);

    Cells[8, 0] := Translate(cShowInTable);
    Cells[9, 0] := Translate(cColumnAlign);
    Cells[10, 0] := Translate(cColumnFormat);
  end; //with sgPARAM do

  rgTime.Items.Clear;
  rgTime.Items.Add(formatdatetime('hh:nn', time));
  rgTime.Items.Add(formatdatetime('hh:nn:ss', time));

  rgDate.Items.Clear;
  rgDate.Items.Add(formatdatetime('d/m/yy', date));
  rgDate.Items.Add(formatdatetime('dd/mm/yy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mm/yy', date));
  rgDate.Items.Add(formatdatetime('d/m/yyyy', date));
  rgDate.Items.Add(formatdatetime('dd/mm/yyyy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mm/yyyy', date));
  rgDate.Items.Add(formatdatetime('dd/mmm/yyyy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mmm/yyyy', date));

  rgDateTime.Items.Clear;
  rgDateTime.Items.Add(formatdatetime('d/m/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mm/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mm/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('d/m/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mmm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mmm/yyyy hh:nn', now));
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.FormCreate(Sender: TObject);
begin
  inherited;
  FReportType := rtStatistic;
  init;
  FParameter := TParameterList.Create;
  sgSummary.ClearNormalCells;
  mOldShowListIndex := 0;
  chbAutoScaleLine.OnClick(nil);
  chbAutoScaleBar.OnClick(nil);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acOKExecute(Sender: TObject);
begin
  inherited;
 // perform an OnExit if a Edit field was active to update its value
  bOk.SetFocus;

  FParameter.LeftYAxis.Autoscale := chbAutoScaleLine.Checked;
  FParameter.LeftYAxis.Min := Round(edLineMin.Value);
  FParameter.LeftYAxis.Max := Round(edLineMax.Value);

  FParameter.RightYAxis.Autoscale := chbAutoScaleBar.Checked;
  FParameter.RightYAxis.Min := Round(edBarMin.Value);
  FParameter.RightYAxis.Max := Round(edBarMax.Value);

end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetControls;
var
  i: integer;
begin

  with Grid do
  // Grid eigenschaften anzeigen
    if Grid <> nil then begin
      laTitleFont.Font.Name := TitleFont.Name;
      laTitleFont.Font.Size := TitleFont.Size;
      laTitleFont.Font.Color := TitleFont.Color;
      laTitleFont.Font.Style := TitleFont.Style;

      laFont.Font.Name := Font.Name;
      laFont.Font.Size := Font.Size;
      laFont.Font.Color := Font.Color;
      laFont.Font.Style := Font.Style;

      paTitleColor.Color := TitleColor;
      paColor.Color := Color;

      cbRowLines.Checked := dgRowLines in Options;
      cbColLines.Checked := dgColLines in Options;
      cbIndicator.Checked := dgIndicator in Options;
//      cbRowSelect.Checked := dgRowSelect in Options;
      cbAlwaysShowSelection.Checked := dgAlwaysShowSelection in Options;
      cbTrailingEllipsis.Checked := dgTrailingEllipsis in Options;
      cbTitleAlignment.Checked := TitleAlignment = taCenter;
    end; //if Grid <> nil then

  SetLabelPos(laTitleFont);
  SetLabelPos(laFont);

  lbShow.Items.Clear;
  lbAvailable.Items.Clear;

  with FParameter do
    for i := 0 to Count - 1 do
      if Items[i].ColumnParam.Tag > cNonVisible then
        if (Items[i].ColumnParam.Visible) or
          (Items[i].ColumnParam.Tag = cShowKey) then begin
          lbShow.Items.AddObject(Items[i].ColumnParam.DisplayLabel, TObject(Items[i].ColumnParam.ID));
          lbShow.Checked[lbShow.Items.Count - 1] := items[i].GraphicParam.ShowGraph;
        end
        else
          lbAvailable.AddObj(Items[i].ColumnParam.DisplayLabel, Items[i].ColumnParam.ID);

  if Grid <> nil then
    udFixedCols.Position := Grid.FixedCols;

  if lbShow.Items.Count > 0 then lbShow.ItemIndex := 0;
  EnableActions;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetParameter(Value: TParameterList);
begin
  FParameter.Assign(Value);
end;
//------------------------------------------------------------------------------
function TmSettingsDialog.GetGrid: TwwDBGrid;
begin
  Result := FGrid;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetGrid(const Value: TwwDBGrid);
begin
  if value <> nil then FGrid := value;
end;
//-----------------------------------------------------------------------------
function TmSettingsDialog.GetDataSet: TDataSet;
begin
  Result := FDataSet;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetDataSet(const Value: TDataSet);
begin
  if value <> nil then FDataSet := value;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.FormShow(Sender: TObject);
begin
  inherited;
  ReadParam;
  SetControls;

  if lbShow.Items.Count > 0 then begin
    lbShow.ItemIndex := 0;
    ShowParam(lbShow.ItemIndex);
    Update_VisibleColumns;
  end;
  WriteParamToSummary;
end;
//------------------------------------------------------------------------------
// Liest Daten aus dem DataSet und aus dem ParameterRecord
procedure TmSettingsDialog.ReadParam;
//var
// xDS           : TDataSet;
// i, xParamID   : integer;
// xParm         : pParamRec;
begin
 //if FLookupID = '' then exit;
 {if FGrid = nil then exit;
 if FGrid.DataSource.DataSet = nil then exit;
 }

  if FDataSet = nil then exit;
// xDS := FDataSet;

 //xParamID := xDS.FieldCount;

  rgBarStyle.ItemIndex := Ord(FParameter.BarStyle);

  chbAutoScaleLine.Checked := FParameter.LeftYAxis.Autoscale;
  edLineMin.Value := FParameter.LeftYAxis.Min;
  edLineMax.Value := FParameter.LeftYAxis.Max;

  chbAutoScaleBar.Checked := FParameter.RightYAxis.Autoscale;
  edBarMin.Value := FParameter.RightYAxis.Min;
  edBarMax.Value := FParameter.RightYAxis.Max;

 {
 if FParameter.Count <= 0 then begin

// if FParameter.Count >= 0 then begin
     xDS.DisableControls;
     try
       xParamID := 0;
       for i := 0 to xDS.FieldCount -1 do begin
        if xDs.Fields[i].Tag > cNonVisible then begin
        new(xParm);
        FillChar(xParm^.ColumnParam, SizeOf(xParm^.ColumnParam), 0);
        FillChar(xParm^.GraphicParam, SizeOf(xParm^.GraphicParam), 0);

        with xParm^.ColumnParam do begin
          ID := xParamID;
          FieldName := xDS.Fields[i].FieldName;
          DisplayLabel := GetDisplayLabel(xDS.Fields[i].DisplayLabel);
          DataType := xDS.Fields[i].DataType;
          Index := xDS.Fields[i].Index;
          DisplayWidth := xDS.Fields[i].DisplayWidth;
          Alignment := xDS.Fields[i].Alignment;
          DisplayFormat := GetDisplayFormatStr(xDS.Fields[i]);
          Tag :=  xDS.Fields.Fields[i].Tag;
          Visible := xDS.Fields[i].Visible; //and (xDS.Fields[i].Tag <> cFieldHideID);
        end;

        with xParm^.GraphicParam do begin
          ID:=i;
          FieldName:= xParm^.ColumnParam.FieldName;
          FilterID:=-2;
          Color:= clRed;//clBlue;
          ChartType:= ctBar;
          LowerLimit:=0;
          UpperLimit:=0;
          YScalingType:= ysNone;
          Statistik:=[];
          ShowGraph := FALSE;
        end;
        inc(xParamID);
        FParameter.Add(xParm);
        Dispose(xParm);
       end;
       end; //for i := 0 to xDS.FieldCount -1 do
     finally
       xDS.EnableControls;
     end; //try..finally
 end;
 }
end; //procedure TmSettingsDialog.ReadFromDataset
//------------------------------------------------------------------------------
function TmSettingsDialog.GetParameter: TParameterList;
begin
  Result := FParameter;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetLabelPos(l: TLabel);
begin
  l.Left := (l.Parent.Width - l.Width) div 2;
  l.Top := (l.Parent.Height - l.Height) div 2;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acColorPanelExecute(Sender: TObject);
begin
  ColorDialog1.Color := paColor1.Color;
  if ColorDialog1.Execute then
    paColor1.Color := ColorDialog1.Color;

  SetParam(lbShow.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acFontExecute(Sender: TObject);
begin
{
if TmmBitBtn(Sender) = bTitelFont then begin
    FontDialog1.Font := laTitleFont.Font;
    if FontDialog1.Execute then
       laTitleFont.Font:= FontDialog1.Font;
    SetLabelPos(laTitleFont);
 end else begin
   FontDialog1.Font := laFont.Font;
   if FontDialog1.Execute then
       laFont.Font:= FontDialog1.Font;
    SetLabelPos(laFont);
 end;
  //TLabel(Sender).Font := FontDialog1.Font;
 //SetLabelPos(TLabel(Sender));
}
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetAllParamsBeforeClose;
var
// xIndex,
// xID,
  j, x: integer;
// xParam    : integer;
  xDS: TDataSet;
  xDispStr,
    xEditStr: string;
  xField: TField;
// xDistance : Integer;

// xDataSet  : TDataSet;
begin
  FParameter.BarStyle := eBarStyleT(rgBarStyle.ItemIndex);

 // neue Einstellungen fuer das Grid setzen
  with Grid do
    if Grid <> nil then begin
      TitleFont.Name := laTitleFont.Font.Name;
      TitleFont.Size := laTitleFont.Font.Size;
      TitleFont.Color := laTitleFont.Font.Color;
      TitleFont.Style := laTitleFont.Font.Style;

      Font.Name := laFont.Font.Name;
      Font.Size := laFont.Font.Size;
      Font.Color := laFont.Font.Color;
      Font.Style := laFont.Font.Style;

      TitleColor := paTitleColor.Color;
      Color := paColor.Color;

      Options := Options - [dgRowLines, dgColLines, dgIndicator, dgRowSelect, dgAlwaysShowSelection, dgTrailingEllipsis];
      if cbRowLines.Checked then Options := Options + [dgRowLines];
      if cbColLines.Checked then Options := Options + [dgColLines];
      if cbIndicator.Checked then Options := Options + [dgIndicator];
//      if cbRowSelect.Checked then Options := Options + [dgRowSelect];
      if cbAlwaysShowSelection.Checked then Options := Options + [dgAlwaysShowSelection];
      if cbTrailingEllipsis.Checked then Options := Options + [dgTrailingEllipsis];

      if cbTitleAlignment.Checked then
        TitleAlignment := taCenter
      else
        TitleAlignment := taLeftJustify;

      FixedCols := udFixedCols.Position;
    end; //if Grid <> nil then

 //
 {if Grid.DataSource.DataSet = nil then exit;
 xDS := Grid.DataSource.DataSet;}

  if DataSet = nil then exit;
  xDS := DataSet;

  xDS.DisableControls;
  try
  // Alle Spalten auf unsichtbar setzen
    for x := 0 to xDS.FieldCount - 1 do
      xDS.Fields.Fields[x].Visible := False;

    for x := 0 to FParameter.Count - 1 do
    // Nur fuer ausgewaehlte Saplten (aus lbShow) anzeigen
      if FParameter.Items[x].SelectedColumn then begin
       // Tab.Mode
        xField := xDS.FieldByName(FParameter.Items[x].ColumnParam.FieldName);
        xField.Index := FParameter.Items[x].ColumnParam.Index;
        xField.DisplayWidth := FParameter.Items[x].ColumnParam.DisplayWidth;
        xField.Alignment := FParameter.Items[x].ColumnParam.Alignment;
        xField.Tag := FParameter.Items[x].ColumnParam.Tag;
        xField.Visible := FParameter.Items[x].ColumnParam.Visible;

        xDispStr := FParameter.Items[x].ColumnParam.DisplayFormat;
        xEditStr := deletechar(FParameter.Items[x].ColumnParam.DisplayFormat, #44);

        case xField.DataType of
          ftSmallint, ftInteger,
            ftFloat, ftWord,
            ftAutoInc, ftLargeInt: begin
              TNumericField(xField).DisplayFormat := xDispStr;
              xEditStr := '';
              for j := 1 to length(xDispStr) do
                if xDispStr[j] in ['.', '0'] then xEditStr := xEditStr + xDispStr[j];
              TNumericField(xField).EditFormat := xEditStr;
            end; //ftSmallint,ftInteger,ftFloat,ftWord,ftAutoInc,ftLargeInt
          ftDateTime, ftDate,
            ftTime: TDateTimeField(xField).DisplayFormat := xDispStr;
        end; //case xField.DataType of
      end; //END if

 {
  // Dataset wird nach lbShow sortiert
  for x:= 0 to lbShow.Items.Count - 1 do
     for xParam:= 0 to xDS.FieldCount-1 do
         if xDS.Fields.Fields[xParam].DisplayName = lbShow.Items.Strings[x] then begin
            xDistance := - xParam - x;
            xDS.MoveBy(xDistance);
            xDS.UpdateCursorPos;
            break;
         end;

  }

  finally
    xDS.EnableControls;
  end; //try..finally
// xDataSet.Free;

end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.lbShowClickCheck(Sender: TObject);
var
  xID: Integer;
begin
  xID := GetClickedParameterID(lbShow.ItemIndex);
  //Tabelle
  if (FParameter.Items[xID].ColumnParam.Tag = cShowTable) or
     (FParameter.Items[xID].ColumnParam.Tag = cShowKey) then begin
    FParameter.items[xID].GraphicParam.ShowGraph := False;
    lbShow.Checked[lbShow.ItemIndex] := False;
    pcParameter.ActivePage := tsTabParam;
    tsGraficParam.TabVisible := False;
  end
  else begin
     // Grafik
    FParameter.items[xID].GraphicParam.ShowGraph := lbShow.Checked[lbShow.ItemIndex];
    tsGraficParam.TabVisible := True;
  end;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.lbShowClick(Sender: TObject);
var
  xID: integer;
begin
  xID := GetClickedParameterID(lbShow.ItemIndex);
  if (FParameter.Items[xID].ColumnParam.Tag = cShowTable) or
    (FParameter.Items[xID].ColumnParam.Tag = cShowKey) then begin
    pcParameter.ActivePage := tsTabParam;
    tsGraficParam.TabVisible := False;
    pcParameter.ActivePage := tsTabParam;
  end
  else begin
     // Grafik
    tsGraficParam.TabVisible := True;
    pcParameter.ActivePage := tsGraficParam;
  end;

  if ((lbShow.ItemIndex = lbShow.Items.Count - 1) and (mOldShowListIndex > lbShow.ItemIndex)) OR
      (mOldShowListIndex > lbShow.Items.Count-1) then
    mOldShowListIndex := lbShow.ItemIndex;

  if mOldShowListIndex <> lbShow.ItemIndex then begin
    SetParam(mOldShowListIndex);
    clbStatData.ItemIndex := -1;
    mOldShowListIndex := lbShow.ItemIndex;
    ShowParam(lbShow.ItemIndex);
  end
  else
    ShowParam(lbShow.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.EnableActions;
//-----------
  procedure SetSelectedItem(aListBox: TNWListBox);
  var
    i: integer;
  begin
    if aListBox.Items.Count > 0 then begin
      i := aListBox.ItemIndex;
      if i > -1 then
        aListBox.Selected[i] := True
      else
        aListBox.Selected[0] := True;
    end; //if aListBox.Items.Count > 0 then
  end; //procedure SetSelectedItem
//-----------

begin //main
  acOK.Enabled := lbShow.Items.Count > 0;

  acMoveTo_lbAvailable.Enabled := (lbShow.Items.Count > 1);
  acMoveAllTo_lbAvailable.Enabled := acMoveTo_lbAvailable.Enabled;

  acMoveTo_lbShow.Enabled := (lbAvailable.Items.Count > 0);
  acMoveAllTo_lbShow.Enabled := acMoveTo_lbShow.Enabled;

  bMoveUp.Enabled := (lbShow.Items.Count >= 1);
  bMoveDown.Enabled := bMoveUp.Enabled;

  SetSelectedItem(lbAvailable);
  SetSelectedItem(TNWListBox(lbShow));

  gbGridSettings.Enabled := Grid <> nil;
 //only to display the enabled font color
  laTitleFont.Enabled := gbGridSettings.Enabled;
  laFont.Enabled := gbGridSettings.Enabled;
  bTitelFont.Enabled := gbGridSettings.Enabled;
  bTitelColor.Enabled := gbGridSettings.Enabled;
  bDatenFont.Enabled := gbGridSettings.Enabled;
  bDataColor.Enabled := gbGridSettings.Enabled;
  cbRowLines.Enabled := gbGridSettings.Enabled;
  cbColLines.Enabled := gbGridSettings.Enabled;
  cbIndicator.Enabled := gbGridSettings.Enabled;
//  cbRowSelect.Enabled := gbGridSettings.Enabled;
  cbAlwaysShowSelection.Enabled := gbGridSettings.Enabled;
  cbTitleAlignment.Enabled := gbGridSettings.Enabled;
  cbTrailingEllipsis.Enabled := gbGridSettings.Enabled;
  udFixedCols.Enabled := gbGridSettings.Enabled;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.Update_VisibleColumns;
var
  i, //j,
    xID: integer;
begin

 // Alle Grafik-Cols auf unsichtbar
  for i := 0 to FParameter.Count - 1 do begin
    FParameter.Items[i].ColumnParam.Visible := False;
    FParameter.Items[i].SelectedColumn := False;
  end;

 // Ausgewaehlte Items auf visible setzen (Grafik)
  for i := 0 to lbShow.Items.Count - 1 do begin
     //xID := GetClickedParameterID(i);
    xID := integer(lbShow.Items.Objects[i]);
    FParameter.Items[xID].ColumnParam.Visible := True;
    FParameter.Items[xID].SelectedColumn := True;

    if lbShow.Checked[i] then
      FParameter.items[xID].GraphicParam.ShowGraph := True
    else
      FParameter.items[xID].GraphicParam.ShowGraph := False;
  end;

  if lbShow.Items.Count = 0 then begin
    pcParameter.Enabled := False;
    paColor1.Color := clBlue;
    lbShow.Clear;
    mOldShowListIndex := 0;
    rgYScaling.ItemIndex := 0;
    rgChartType.ItemIndex := 0; // Bar
    cbUpperLimit.Checked := False;
    cbLowerLimit.Checked := False;
    edUpperLimit.Value := 0;
    edLowerLimit.Value := 0;
    clbStatData.ItemIndex := -1;
    for i := 0 to clbStatData.Items.Count - 1 do
      clbStatData.Checked[i] := False;

    for i := 0 to lbAvailable.items.count - 1 do
      lbAvailable.Selected[i] := False;
    lbAvailable.Selected[lbAvailable.ItemIndex] := True;
  end
  else
    pcParameter.Enabled := True;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.ShowParam(aItemIndex: integer);
var
  xID: integer;
  rgIndex: integer;
  xStat: eStatistikT;
  xPos: integer;

begin

  xID := GetClickedParameterID(aItemIndex);

  if (FParameter.Items[xID].ColumnParam.Tag = cShowTable) or
    (FParameter.Items[xID].ColumnParam.Tag = cShowKey) then begin
    pcParameter.ActivePage := tsTabParam;
    tsGraficParam.TabVisible := False;
  end
  else begin
    tsGraficParam.TabVisible := True;
  end;

 //Tabelle
  with FParameter.Items[xID].ColumnParam do begin
    xPos := rgAlignment.Top + rgAlignment.Height + 10;

    rgInteger.Visible := DataType in [ftSmallint, ftInteger, ftWord, ftAutoInc, ftLargeInt];
    rgFloat.Visible := DataType = ftFloat;
    rgTime.Visible := DataType = ftTime;
    rgDate.Visible := DataType = ftDate;
    rgDateTime.Visible := DataType = ftDateTime;

    case Alignment of
      taLeftJustify: rgAlignment.ItemIndex := 0;
      taCenter: rgAlignment.ItemIndex := 1;
      taRightJustify: rgAlignment.ItemIndex := 2;
    end; //case FWork.Columns[i].ALignment of

    if rgInteger.Visible then begin
      rgInteger.top := xPos;
      rgInteger.Left := rgAlignment.Left;
      rgIndex := 0;
      if DisplayFormat = '0' then rgIndex := 1;
      rgInteger.ItemIndex := rgIndex;
       //if (DisplayFormat = '') and (rgIndex = 0) then DisplayFormat:= '0.0';
    end; //if rgInteger.Visible then

    if rgFloat.Visible then begin
      rgFloat.top := xPos;
      rgFloat.left := rgAlignment.Left;
      rgIndex := 0;
      if DisplayFormat = ',0.0' then rgIndex := 1;
      if DisplayFormat = ',0.00' then rgIndex := 2;
      if DisplayFormat = ',0.000' then rgIndex := 3;
      if DisplayFormat = '0' then rgIndex := 4;
      if DisplayFormat = '0.0' then rgIndex := 5;
      if DisplayFormat = '0.00' then rgIndex := 6;
      if DisplayFormat = '0.000' then rgIndex := 7;
      rgFloat.ItemIndex := rgIndex;
     // if (DisplayFormat = '') and (rgIndex = 0) then DisplayFormat:= '0.0';
    end; //if rgFloat.Visible then

    if rgTime.Visible then begin
      rgTime.top := xPos;
      rgTime.left := rgAlignment.Left;
      rgIndex := 0;
      if DisplayFormat = 'hh:nn:ss' then rgIndex := 1;
      rgTime.ItemIndex := rgIndex;
    end; //if rgTime.Visible then

    if rgDate.Visible then begin
      rgDate.top := xPos;
      rgDate.left := rgAlignment.Left;
      rgIndex := 0;
      if DisplayFormat = 'dd/mm/yy' then rgIndex := 1;
      if DisplayFormat = 'ddd dd/mm/yy' then rgIndex := 2;
      if DisplayFormat = 'd/m/yyyy' then rgIndex := 3;
      if DisplayFormat = 'dd/mm/yyyy' then rgIndex := 4;
      if DisplayFormat = 'ddd dd/mm/yyyy' then rgIndex := 5;
      if DisplayFormat = 'dd/mmm/yyyy' then rgIndex := 6;
      if DisplayFormat = 'ddd dd/mmm/yyyy' then rgIndex := 7;
      rgDate.ItemIndex := rgIndex;
    end; //if rgDate.Visible then

    if rgDateTime.Visible then begin
      rgDateTime.top := xPos;
      rgDateTime.left := rgAlignment.Left;
      rgIndex := 0;
      if DisplayFormat = 'dd/mm/yy hh:nn' then rgIndex := 1;
      if DisplayFormat = 'ddd dd/mm/yy hh:nn' then rgIndex := 2;
      if DisplayFormat = 'd/m/yyyy hh:nn' then rgIndex := 3;
      if DisplayFormat = 'dd/mm/yyyy hh:nn' then rgIndex := 4;
      if DisplayFormat = 'ddd dd/mm/yyyy hh:nn' then rgIndex := 5;
      if DisplayFormat = 'dd/mmm/yyyy hh:nn' then rgIndex := 6;
      if DisplayFormat = 'ddd dd/mmm/yyyy hh:nn' then rgIndex := 7;
      rgDateTime.ItemIndex := rgIndex;
    end; //if rgDateTime.Visible then
  end;

  with FParameter.Items[xID].GraphicParam do begin
     // Parameter einstellungen

    rgYScaling.ItemIndex := ord(YScalingType);
    rgChartType.ItemIndex := ord(ChartType);

    paColor1.Color := Color;
    for xStat := stMin to stQ99 do
      clbStatData.Checked[ord(xStat)] := xStat in Statistik;

    cbLowerLimit.Checked := stLowerLimit in Statistik;
    edLowerLimit.Value := LowerLimit;

    cbUpperLimit.Checked := stUpperLimit in Statistik;
    edUpperLimit.Value := UpperLimit;

    lbShow.Checked[aItemIndex] := ShowGraph;
  end;
end; // END ShowParam()
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetParam(aItemIndex: integer);
var
  xID: integer;
  xStat: eStatistikT;
begin

  if lbShow.items.count <= 0 then exit;

  xID := GetClickedParameterID(aItemIndex);

 //Tabellenparameter
  with FParameter.Items[xID].ColumnParam do begin

    case rgAlignment.ItemIndex of
      0: Alignment := taLeftJustify;
      1: Alignment := taCenter;
      2: Alignment := taRightJustify;
    end;

    if rgInteger.Visible then
      case rgInteger.ItemIndex of
        0: DisplayFormat := ',0';
        1: DisplayFormat := '0';
      end; //case rgInteger.ItemIndex 0f

    if rgFloat.Visible then
      case rgFloat.ItemIndex of
        0: DisplayFormat := ',0';
        1: DisplayFormat := ',0.0';
        2: DisplayFormat := ',0.00';
        3: DisplayFormat := ',0.000';
        4: DisplayFormat := '0';
        5: DisplayFormat := '0.0';
        6: DisplayFormat := '0.00';
        7: DisplayFormat := '0.000';
      end; //case rgFloat.ItemIndex 0f

    if rgTime.Visible then
      case rgTime.ItemIndex of
        0: DisplayFormat := 'hh:nn';
        1: DisplayFormat := 'hh:nn:ss';
      end; //case rgTime.ItemIndex 0f

    if rgDate.Visible then
      case rgDate.ItemIndex of
        0: DisplayFormat := 'd/m/yy';
        1: DisplayFormat := 'dd/mm/yy';
        2: DisplayFormat := 'ddd dd/mm/yy';
        3: DisplayFormat := 'd/m/yyyy';
        4: DisplayFormat := 'dd/mm/yyyy';
        5: DisplayFormat := 'ddd dd/mm/yyyy';
        6: DisplayFormat := 'dd/mmm/yyyy';
        7: DisplayFormat := 'ddd dd/mmm/yyyy';
      end; //case rgDate.ItemIndex 0f

    if rgDateTime.Visible then
      case rgDateTime.ItemIndex of
        0: DisplayFormat := 'd/m/yy hh:nn';
        1: DisplayFormat := 'dd/mm/yy hh:nn';
        2: DisplayFormat := 'ddd dd/mm/yy hh:nn';
        3: DisplayFormat := 'd/m/yyyy hh:nn';
        4: DisplayFormat := 'dd/mm/yyyy hh:nn';
        5: DisplayFormat := 'ddd dd/mm/yyyy hh:nn';
        6: DisplayFormat := 'dd/mmm/yyyy hh:nn';
        7: DisplayFormat := 'ddd dd/mmm/yyyy hh:nn';
      end; //case rgDateTime.ItemIndex 0f
 {}
  end;

 // Grafikparmeter
  with FParameter.Items[xID].GraphicParam do begin
     // Parameter einstellungen

    YScalingType := eYScalingTypeT(rgYScaling.ItemIndex);

    ChartType := eChartTypeT(rgChartType.ItemIndex);

    Color := paColor1.Color;

    Statistik := Statistik - [stMin, stMax, stMean, stStdDev, stQ90, stQ95, stQ99];
    for xStat := stMin to stQ99 do
      if clbStatData.Checked[ord(xStat)] then Statistik := Statistik + [xStat];

    LowerLimit := edLowerLimit.Value;
    UpperLimit := edUpperLimit.Value;
    ShowGraph := lbShow.Checked[aItemIndex];

    if cbUpperLimit.Checked then
      Statistik := Statistik + [stUpperLimit]
    else
      Statistik := Statistik - [stUpperLimit];

    if cbLowerLimit.Checked then
      Statistik := Statistik + [stLowerLimit]
    else
      Statistik := Statistik - [stLowerLimit];
  end;
end; // END SetParam()
//------------------------------------------------------------------------------
procedure TmSettingsDialog.rgYScalingClick(Sender: TObject);
begin
  SetParam(lbShow.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.acSetParamExecute(Sender: TObject);
begin
  SetParam(lbShow.ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.WriteParamToSummary;
var x, xRow, xID: integer;
  xStat: eStatistikT;
begin

  with sgSummary do begin

    ClearNormalCells;
    RowCount := 2;

    AutoSizeCol(1);
    AutoSizeCol(2);
    AutoSizeCol(3);
    AutoSizeCol(4);
    AutoSizeCol(5);
    AutoSizeCol(6);
    AutoSizeCol(7);
    AutoSizeCol(8);
    AutoSizeCol(9);
    AutoSizeCol(10);

    xRow := 1;

    for x := 0 to lbShow.Items.Count - 1 do begin
      xID := GetClickedParameterID(x);
      cells[0, xRow] := FParameter.Items[xID].ColumnParam.DisplayLabel;

       //-> Die Farbe wird in sgSummaryGetCellColor dargestellt

       //Grafiktyp
      if FParameter.Items[xID].GraphicParam.ShowGraph then
        case FParameter.Items[xID].GraphicParam.ChartType of
          ctLine: Cells[1, xRow] := LTrans(cLineStr);
          ctBar: Cells[1, xRow] := LTrans(cBarStr);
        end; //case FParaList[pID].ChartType of

       // Statistik
      for xStat := stMin to stQ99 do
        if xStat in FParameter.Items[xID].GraphicParam.Statistik then
          if Cells[3, xRow] = '' then
            Cells[3, xRow] := cStatName[xStat]
          else
            Cells[3, xRow] := Cells[3, xRow] + ', ' + cStatName[xStat];

       // Skalierung  -> nur fuer Grafik
      if FParameter.Items[xID].ColumnParam.Tag = cShowTableGraphic then
        Cells[4, xRow] := mmGraphGlobal.cYScalingStr[FParameter.Items[xID].GraphicParam.YScalingType];

       //Unterer Grenzwert
      if stLowerLimit in FParameter.Items[xID].GraphicParam.Statistik then
        Cells[5, xRow] := formatfloat(',0.0', FParameter.Items[xID].GraphicParam.LowerLimit);

       // Oberer Grenzwert
      if stUpperLimit in FParameter.Items[xID].GraphicParam.Statistik then
        Cells[6, xRow] := formatfloat(',0.0', FParameter.Items[xID].GraphicParam.UpperLimit);

       // in Grafik anzeigen
      Cells[7, xRow] := FormatBool('X; ', FParameter.Items[xID].GraphicParam.ShowGraph);

       // Tab. Ausrichtung
      case FParameter.Items[xID].ColumnParam.Alignment of
        taLeftJustify: Cells[9, xRow] := rgAlignment.Items.Strings[0]; // ItemIndex := 0;
        taCenter: Cells[9, xRow] := rgAlignment.Items.Strings[1]; //ItemIndex := 1;
        taRightJustify: Cells[9, xRow] := rgAlignment.Items.Strings[2]; //ItemIndex := 2;
      end;

       // in Tab. anzeigen
      Cells[8, xRow] := 'X';

       // Tab. Format
      Cells[10, xRow] := FParameter.Items[xID].ColumnParam.DisplayFormat;

      RowCount := sgSummary.RowCount + 1; // neue Zeile
      inc(xRow);
    end; // End for

    if RowCount > 2 then RowCount := RowCount - 1;

    if lbShow.Items.Count > 0 then Row := lbShow.ItemIndex + 1;
  end; // End with
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.sgSummaryGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
var
  xID: integer;
begin
  if lbShow.items.count <= 0 then exit; //pw
  if ARow <= 0 then exit;

  xID := GetClickedParameterID(ARow - 1);

  AFont.Color := clBlack;
  ABrush.Style := bsSolid;

  if ARow < sgSummary.FixedRows then begin
    ABrush.Color := clLightGray1;
  end
  else if ACol = 0 then
    ABrush.Color := clLightYellow
  else
    ABrush.Color := clWindow;

 // Spaltenfarbe anzeigen
  if (ACol = 2) and (ARow > 0) then begin
    ABrush.Style := bsSolid;
    if (FParameter.items[xID].ColumnParam.Tag = cShowTableGraphic) { and
     (FParameter.items[xID].GraphicParam.ShowGraph)}then
      ABrush.Color := FParameter.items[xID].GraphicParam.Color;
    if (sgSummary.RowCount = 2) and (sgSummary.Cells[0, 1] = '') then ABrush.Color := clWindow;
  end;

  if gdSelected in AState then begin
    if (ACol = 2) and (ABrush.Color = clHighlight) then
      ABrush.Color := clHighlight + clWhite
    else
      ABrush.Color := clHighlight;

    AFont.Color := clHighlightText;
  end; //if gdSelected in AState then
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.sgSummaryDblClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
  if (Arow = 0) then exit;
  if lbShow.items.count <= 0 then exit; //pw

  pcSettings.ActivePage := tsColParam;
  lbShow.Selected[lbShow.ItemIndex] := False;
  lbShow.ItemIndex := Arow - 1;
  lbShow.Selected[lbShow.ItemIndex] := True;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.sgSummaryGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var AAlignment: TAlignment);
begin
  if (ACol = 7) or (ACol = 8) then AAlignment := taCenter;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.tsColParamEnter(Sender: TObject);
begin
  if lbShow.Items.Count <= 0 then exit;
  lbShow.Selected[lbShow.ItemIndex] := True;
  mOldShowListIndex := lbShow.ItemIndex;
  ShowParam(lbShow.ItemIndex);
  Update_VisibleColumns;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.tsColParamExit(Sender: TObject);
begin
  if mOldShowListIndex <> lbShow.ItemIndex then begin
    SetParam(mOldShowListIndex);
    clbStatData.ItemIndex := -1;
    mOldShowListIndex := lbShow.ItemIndex;
  end
  else begin
    SetParam(lbShow.ItemIndex);
    clbStatData.ItemIndex := -1;
  end;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.tsSummaryExit(Sender: TObject);
var x: integer;
begin
  if sgSummary.Cells[0, 1] <> '' then begin
    for x := 0 to lbShow.Items.Count - 1 do
      lbShow.Selected[x] := False;
    lbShow.ItemIndex := sgSummary.Row - 1;
  end;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.pcSettingsChange(Sender: TObject);
begin
  if pcSettings.ActivePage = tsSummary then WriteParamToSummary;
end;
//------------------------------------------------------------------------------
function TmSettingsDialog.GetClickedParameterID(aItemIndex: integer): integer;
var
  i, xID: integer;
begin //??????????
  Result := -1;
  if (aItemIndex < 0) or (lbShow.Items.Count <= 0) then
    Exit;

  xID := integer(lbShow.Items.Objects[aItemIndex]);
  for i := 0 to FParameter.Count - 1 do
    if FParameter.Items[i].ColumnParam.ID = xID then begin
      Result := FParameter.Items[i].ColumnParam.ID;
      break;
    end
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetReportType(const Value: TReportType);
begin
  FReportType := Value;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.MoveSelectedItem(aOldPos, aDropPos: integer);
var
  i: integer;
begin
  lbShow.Items.Move(aOldPos, aDropPos);

  for i := 0 to lbShow.Items.Count - 1 do
    lbShow.Selected[i] := False;

  lbShow.Selected[aDropPos] := True;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.bMoveUpClick(Sender: TObject);
var
  xFirst,
    xDropPos,
    i: integer;
begin

 //only move 1 selected item (the first one)
  xFirst := -1;
  for i := 0 to lbShow.Items.Count - 1 do
    if lbShow.Selected[i] then begin
      xFirst := i;
      Break;
    end; //if lbShow.Selected[i] then

  if xFirst = -1 then exit;

  xDropPos := xFirst;
  if Sender = bMoveUp then begin
    if xFirst > 0 then xDropPos := xFirst - 1;
  end //if Sender = bMoveUp then
  else begin
    if xFirst < (lbShow.Items.Count - 1) then xDropPos := xFirst + 1;
  end; //else if Sender = bMoveUp then

  MoveSelectedItem(xFirst, xDropPos);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetGridColor(aButton: TmmBitBtn);
begin

  if aButton = bTitelColor then begin
    ColorDialog1.Color := paTitleColor.Color;
    if ColorDialog1.Execute then
      paTitleColor.Color := ColorDialog1.Color;
  end
  else begin
    ColorDialog1.Color := paColor.Color;
    if ColorDialog1.Execute then
      paColor.Color := ColorDialog1.Color;
  end;

{
 ColorDialog1.Color := TPanel(Sender).Color;
 if ColorDialog1.Execute then
  TPanel(Sender).Color := ColorDialog1.Color;
  SetParam(lbShow.ItemIndex);
}
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.SetGridFont(aButton: TmmBitBtn);
begin
  if aButton = bTitelFont then begin
    FontDialog1.Font := laTitleFont.Font;
    if FontDialog1.Execute then
      laTitleFont.Font := FontDialog1.Font;
    SetLabelPos(laTitleFont);
  end
  else begin
    FontDialog1.Font := laFont.Font;
    if FontDialog1.Execute then
      laFont.Font := FontDialog1.Font;
    SetLabelPos(laFont);
  end;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.bTitelFontClick(Sender: TObject);
begin
  SetGridFont(bTitelFont);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.bDatenFontClick(Sender: TObject);
begin
  SetGridFont(bDatenFont);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.bTitelColorClick(Sender: TObject);
begin
  SetGridColor(bTitelColor);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.bDataColorClick(Sender: TObject);
begin
  //SetGridColor( TmmBitBtn(Sender) );
  SetGridColor(bDataColor);
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.rgChartTypeClick(Sender: TObject);
begin

  if rgChartType.ItemIndex = 0 then
    rgBarStyle.Visible := False
  else
    rgBarStyle.Visible := True;

end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.AfterItemMove;
var xBool: Boolean;
begin
  xBool := lbAvailable.MultiSelect;
  lbAvailable.MultiSelect := False;
  if lbAvailable.Items.Count > 0 then
    lbAvailable.ItemIndex := 0;
  lbAvailable.MultiSelect := xBool;
  if lbAvailable.Items.Count > 0 then
    lbAvailable.Selected[0];
  lbAvailable.setfocus;
  mOldShowListIndex := 0;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.rgBarStyleClick(Sender: TObject);
var xEnabled: Boolean;
  xColor: TColor;
begin
  if rgBarStyle.ItemIndex = 0 then begin
    xEnabled := True;
    xColor := clWindow;
  end
  else begin
    xEnabled := False;
    xColor := clBtnFace;
  end;

  chbAutoScaleLine.enabled := xEnabled;
  edLineMin.enabled := xEnabled;
  edLineMax.enabled := xEnabled;
  edLineMin.Color := xColor;
  edLineMax.Color := xColor;

  chbAutoScaleBar.enabled := xEnabled;
  edBarMin.enabled := xEnabled;
  edBarMax.enabled := xEnabled;
  edBarMin.Color := xColor;
  edBarMax.Color := xColor;

end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.chbAutoScaleLineClick(Sender: TObject);
var xColor: TColor;
begin
  if not chbAutoScaleLine.Checked then begin
    edLineMin.Enabled := True;
    edLineMax.Enabled := True;
    xColor := clWindow;
  end
  else begin
    edLineMin.Enabled := False;
    edLineMax.Enabled := False;
    xColor := clBtnFace;
  end;

  edLineMin.Color := xColor;
  edLineMax.Color := xColor;

  laLineMin.Enabled := not chbAutoScaleLine.Checked;
  laLineMax.Enabled := not chbAutoScaleLine.Checked;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.chbAutoScaleBarClick(Sender: TObject);
var xColor: TColor;
begin

  if not chbAutoScaleBar.Checked then begin
    edBarMin.Enabled := True;
    edBarMax.Enabled := True;
    xColor := clWindow;
  end
  else begin
    edBarMin.Enabled := False;
    edBarMax.Enabled := False;
    xColor := clBtnFace;
  end;

  edBarMin.Color := xColor;
  edBarMax.Color := xColor;

  laBarMin.Enabled := not chbAutoScaleBar.Checked;
  laBarMax.Enabled := not chbAutoScaleBar.Checked;
end;
//------------------------------------------------------------------------------
procedure TmSettingsDialog.tsTabFormatEnter(Sender: TObject);
begin
  chbAutoScaleLine.OnClick(nil);
  chbAutoScaleBar.OnClick(nil);
end;

end.

