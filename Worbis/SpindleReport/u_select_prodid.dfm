inherited SelectProdIDDlg: TSelectProdIDDlg
  Left = 437
  Top = 205
  Width = 348
  Height = 339
  Caption = '(40)Produktionsgruppe auswaehlen'
  Constraints.MinWidth = 252
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object lbNames: TNWListBox
    Left = 0
    Top = 0
    Width = 340
    Height = 282
    Align = alClient
    ExtendedSelect = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 0
    OnDblClick = acOKExecute
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 282
    Width = 340
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 100
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 180
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 260
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 10
    Top = 58
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object IvExtendedTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 40
    Top = 56
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0))
  end
  object quSelect: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CommandText = 
      'SELECT DISTINCT sid.c_prod_id,               pg.partie_name,    ' +
      '            pg.c_spindle_first,                pg.c_spindle_last' +
      ' FROM t_spindle_interval_data sid JOIN t_interval int ON (int.c_' +
      'interval_id = sid.c_interval_id) JOIN t_prodgroup pg ON (pg.c_pr' +
      'od_id = sid.c_prod_id) WHERE sid.c_machine_id = :MachineID AND i' +
      'nt.c_interval_start >= :IntervalStart AND int.c_interval_end <= ' +
      ':IntervalEnd ORDER BY pg.c_spindle_first,pg.partie_name'
    Parameters = <
      item
        Name = 'MachineID'
        Attributes = [paSigned]
        DataType = ftSmallint
        Precision = 5
        Size = 2
        Value = Null
      end
      item
        Name = 'IntervalStart'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end
      item
        Name = 'IntervalEnd'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    Left = 82
    Top = 58
  end
end
