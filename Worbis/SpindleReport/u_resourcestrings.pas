(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_resourcestrings.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale strings
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|  5. 4.99 | 0.00 | PW  | Datei erstellt
| 23.02.00 | 1.00 | Mg  | Imperfections inserted
| 10.06.05 | 1.16 | Wss | - Spectra+ und Zenit Elemente hinzugef�gt
| 18.02.08 | 1.20 | Nue | VCV added.
|=========================================================================================*)

{$i symbols.inc}

unit u_resourcestrings;
interface

resourcestring
 //graph items
 cLeftAxisStr                      = '(15)Linien';                                         //ivlm
 cRightAxisStr                     = '(15)Balken';                                         //ivlm
 cXAxisIntStr                      = '(40)Zeit (Intervalle)';                              //ivlm
 cGraphTitleStr2                   = '(100)Zeitraum: %s - %s   (%s)';                      //ivlm

 //fieldnames
 //t_spindle_intervall_data...
 cc_spindle_idStr                  = '(30)c_spindle_id';                                   //ivlm
 cc_rtSpdStr                       = '(30)c_rtSpd';                                        //ivlm
 ccalc_npProdGrpStr                = '(30)calc_npProdGrp';                                 //ivlm
 ccalc_MEffStr                     = '(30)calc_MEff';                                      //ivlm
 ccalc_PEffStr                     = '(30)calc_PEff';                                      //ivlm
 cc_LenStr                         = '(30)c_Len';                                          //ivlm
 cc_WeiStr                         = '(30)c_Wei';                                          //ivlm
 cc_BobStr                         = '(30)c_Bob';                                          //ivlm
 cc_ConesStr                       = '(30)c_Cones';                                        //ivlm
 ccalc_CYTOTStr                    = '(30)calc_CYTOT';                                     //ivlm
 ccalc_SpStr                       = '(30)calc_Sp';                                        //ivlm
 ccalc_YBStr                       = '(30)calc_YB';                                        //ivlm
 ccalc_CNStr                       = '(30)calc_CN';                                        //ivlm
 ccalc_CSStr                       = '(30)calc_CS';                                        //ivlm
 ccalc_CLStr                       = '(30)calc_CL';                                        //ivlm
 ccalc_CTStr                       = '(30)calc_CT';                                        //ivlm
 ccalc_CSIROStr                    = '(30)calc_CSIRO';                                     //ivlm
 ccalc_COFFCNTStr                  = '(30)calc_COFFCNT';                                   //ivlm
 ccalc_COFFCNTPlusStr              = '(30)calc_COFFCNTPlus';                               //ivlm
 ccalc_COFFCNTMinusStr             = '(30)calc_COFFCNTMinus';                              //ivlm
 ccalc_CShortOFFCNTStr             = '(30)calc_CShortOFFCNT';                              //ivlm
 ccalc_CShortOFFCNTPlusStr         = '(30)calc_CShortOFFCNTPlus';                          //ivlm
 ccalc_CShortOFFCNTMinusStr        = '(30)calc_CShortOFFCNTMinus';                         //ivlm
 ccalc_CPStr                       = '(30)calc_CP';                                        //ivlm
 ccalc_CCLSStr                     = '(30)calc_CCLS';                                      //ivlm
 ccalc_CClLStr                     = '(30)calc_CClL';                                      //ivlm
 ccalc_CClTStr                     = '(30)calc_CClT';                                      //ivlm
// ccalc_LckPStr                     = '(30)calc_LckP';
// ccalc_LckClLStr                   = '(30)calc_LckClL';
// ccalc_LckClTStr                   = '(30)calc_LckClT';
// ccalc_LckShortOffCntStr           = '(30)calc_LckShortOffCnt';
// ccalc_LckNSLTStr                  = '(30)calc_NSLT';

 cc_LckSIROClStr                   = '(30)c_LckSIROCl';                                 //ivlm
 cc_LckSFIStr                      = '(30)c_LckSFI';                                    //ivlm
 cc_LckSysStr                      = '(30)c_LckSys';                                       //ivlm
 cc_LckSIROStr                     = '(30)c_LckSIRO';                                      //ivlm
 cc_LckOffCntStr                   = '(30)c_LckOffCnt';                                    //ivlm
 cc_LckShortOffCntStr              = '(30)c_LckShortOffCnt';                               //ivlm
 cc_LckPStr                        = '(30)c_LckP';                                      //ivlm
 cc_LckClSStr                      = '(30)c_LckClS';                                    //ivlm
 cc_LckClLStr                      = '(30)c_LckClL';                                    //ivlm
 cc_LckClTStr                      = '(30)c_LckClT';                                    //ivlm
 cc_LckNSLTStr                     = '(30)c_LckNSLT';                                   //ivlm

//Nue:18.02.08
 cc_LckVCVStr                      = '(30)c_LckVCV';                                    //ivlm

 cc_LStStr                         = '(30)c_LSt';                                          //ivlm
 cc_LStProdStr                     = '(30)c_LStProd';                                      //ivlm
 cc_LStBreakStr                    = '(30)c_LStBreak';                                     //ivlm
 cc_LStMatStr                      = '(30)c_LStMat';                                       //ivlm
 cc_LStRevStr                      = '(30)c_LStRev';                                       //ivlm
 cc_LStCleanStr                    = '(30)c_LStClean';                                     //ivlm
 cc_LStDefStr                      = '(30)c_LStDef';                                       //ivlm
 ccalc_LStTOTStr                   = '(30)calc_LStTOT';                                    //ivlm
 ccalc_CUPYStr                     = '(30)calc_CUPY';                                      //ivlm
 ccalc_CBUStr                      = '(30)calc_CBU';                                       //ivlm
 ccalc_CSYSStr                     = '(30)calc_CSYS';                                      //ivlm
 ccalc_CSPStr                      = '(30)calc_CSP';                                       //ivlm
 ccalc_RSPStr                      = '(30)calc_RSP';                                       //ivlm
 ccalc_PCSpStr                     = '(30)calc_PCSp';                                      //ivlm
 ccalc_PRSpStr                     = '(30)calc_PRSp';                                      //ivlm
 ccalc_RedStr                      = '(30)calc_Red';                                       //ivlm
 ccalc_ManStStr                    = '(30)calc_ManSt';                                     //ivlm
 ccalc_UCLSStr                     = '(30)calc_UCLS';                                      //ivlm
 ccalc_UCLLStr                     = '(30)calc_UCLL';                                      //ivlm
 ccalc_UCLTStr                     = '(30)calc_UCLT';                                      //ivlm
 ccalc_CDBUStr                     = '(30)calc_CDBU';                                      //ivlm
 cc_tRedStr                        = '(30)c_tRed';                                         //ivlm
 cc_tYellStr                       = '(30)c_tYell';                                        //ivlm
 cc_tManStStr                      = '(30)c_tManSt';                                       //ivlm
 cc_order_nameStr                  = '(30)c_order_name';                                   //ivlm
 cc_order_descriptionStr           = '(30)c_order_description';                            //ivlm
 cc_style_nameStr                  = '(30)c_style_name';                                   //ivlm
 cc_yarncntStr                     = '(30)c_yarncnt';                                      //ivlm
 ccalc_SfiDStr                     = '(30)calc_SfiD';                                      //ivlm
 ccalc_CSFIStr                     = '(30)calc_CSFI';                                      //ivlm
 ccalc_CSIROClStr                  = '(30)calc_CSIROCl';                                   //ivlm
// ccalc_LckClSStr                   = '(30)calc_LckClS';
// ccalc_LckSIROClStr                = '(30)calc_LckSIROCl';
// ccalc_LckSFIStr                   = '(30)calc_LckSFI';
// ccalc_LckOffCntStr                = '(30)calc_LckOffCnt';
 ccalc_INepsStr                    = '(30)calc_INeps';                                     //ivlm
 ccalc_IThickStr                   = '(30)calc_IThick';                                    //ivlm
 ccalc_IThinStr                    = '(30)calc_IThin';                                     //ivlm
 ccalc_ISmallStr                   = '(30)calc_ISmall';                                    //ivlm
 ccalc_I2_4Str                     = '(30)calc_I2_4';                                      //ivlm
 ccalc_I4_8Str                     = '(30)calc_I4_8';                                      //ivlm
 ccalc_I8_20Str                    = '(30)calc_I8_20';                                     //ivlm
 ccalc_I20_70Str                   = '(30)calc_I20_70';                                    //ivlm

//Nue:18.02.08
 ccalc_CVdStr                      = '(30)calc_CVd';                                       //ivlm
 ccalc_CVCVStr                     = '(30)calc_CVCV';                                      //ivlm

 //t_prodgroup
 cpartie_nameStr                   = '(30)partie_name';                                    //ivlm
 cc_spindle_firstStr               = '(30)c_spindle_first';                                //ivlm
 cc_spindle_lastStr                = '(30)c_spindle_last';                                 //ivlm
 cc_prod_startStr                  = '(30)c_prod_start';                                   //ivlm
 cc_prod_endStr                    = '(30)c_prod_end';                                     //ivlm

 //t_ym_settings
 cc_YM_set_idStr                   = '(40)c_YM_set_id';                                    //ivlm
 cc_YM_set_nameStr                 = '(40)c_YM_set_name';                                  //ivlm
 cc_DNStr                          = '(40)c_DN';                                           //ivlm
 cstat_noppenStr                   = '(40)stat_noppen';                                    //ivlm
 cdurch_kurzStr                    = '(40)durch_kurz';                                     //ivlm
 cstat_kurzStr                     = '(40)stat_kurz';                                      //ivlm
 claenge_kurzStr                   = '(40)laenge_kurz';                                    //ivlm
 cdurch_langStr                    = '(40)durch_lang';                                     //ivlm
 cstat_langStr                     = '(40)stat_lang';                                      //ivlm
 claenge_langStr                   = '(40)laenge_lang';                                    //ivlm
 cdurch_duennStr                   = '(40)durch_duenn';                                    //ivlm
 cstat_duennStr                    = '(40)stat_duenn';                                     //ivlm
 claenge_duennStr                  = '(40)laenge_duenn';                                   //ivlm
 claenge_spleissStr                = '(40)laenge_spleiss';                                 //ivlm
 cstat_spleissStr                  = '(40)stat_spleiss';                                   //ivlm
 cstat_spliceStr                   = '(40)stat_splice';                                    //ivlm
 csdurch_noppenStr                 = '(40)sdurch_noppen';                                  //ivlm
 csstat_noppenStr                  = '(40)sstat_noppen';                                   //ivlm
 csdurch_kurzStr                   = '(40)sdurch_kurz';                                    //ivlm
 csstat_kurzStr                    = '(40)sstat_kurz';                                     //ivlm
 cslaenge_kurzStr                  = '(40)slaenge_kurz';                                   //ivlm
 csdurch_langStr                   = '(40)sdurch_lang';                                    //ivlm
 csstat_langStr                    = '(40)sstat_lang';                                     //ivlm
 cslaenge_langStr                  = '(40)slaenge_lang';                                   //ivlm
 csdurch_duennStr                  = '(40)sdurch_duenn';                                   //ivlm
 csstat_duennStr                   = '(40)sstat_duenn';                                    //ivlm
 cslaenge_duennStr                 = '(40)slaenge_duenn';                                  //ivlm
 cupper_yarnStr                    = '(40)upper_yarn';                                     //ivlm
 cfs_abweichStr                    = '(40)fs_abweich';                                     //ivlm
 cfs_max_anzStr                    = '(40)fs_max_anz';                                     //ivlm
 cfs_laengeStr                     = '(40)fs_laenge';                                      //ivlm
 cnr_abweichStr                    = '(40)nr_abweich';                                     //ivlm
 cnr_startup_repStr                = '(40)nr_startup_rep';                                 //ivlm
 cklassier_klassStr                = '(40)klassier_klass';                                 //ivlm
 cstat_upper_yarnStr               = '(40)stat_upper_yarn';                                //ivlm
 cscheck_lengthStr                 = '(40)scheck_length';                                  //ivlm
 cconfigAStr                       = '(40)configA';                                        //ivlm
 cconfigBStr                       = '(40)configB';                                        //ivlm
 csiro_klassStr                    = '(40)siro_klass';                                     //ivlm
 cstartupOffliStr                  = '(40)startupOffli';                                   //ivlm
 cstartupRepStr                    = '(40)startupRep';                                     //ivlm

 //t_machine_offlimit_settings
 cc_maoffset_nameStr               = '(40)c_maoffset_name';                                //ivlm
 cc_baseStr                        = '(40)c_base';                                         //ivlm
 cc_Average_timeStr                = '(40)c_Average_time';                                 //ivlm
 cc_ProdGrp_dependStr              = '(40)c_ProdGrp_depend';                               //ivlm
 cc_OffBorderByAverageStr          = '(40)c_OffBorderByAverage';                           //ivlm
 cc_OffBorderByFixValuesStr        = '(40)c_OffBorderByFixValues';                         //ivlm
 cc_Eff_PC_maxStr                  = '(40)c_Eff_PC_max';                                   //ivlm
 cc_Eff_PC_minStr                  = '(40)c_Eff_PC_min';                                   //ivlm
 cc_Eff_maxStr                     = '(40)c_Eff_max';                                      //ivlm
 cc_Eff_minStr                     = '(40)c_Eff_min';                                      //ivlm
 cc_CYTot_PC_maxStr                = '(40)c_CYTot_PC_max';                                 //ivlm
 cc_CYTot_PC_minStr                = '(40)c_CYTot_PC_min';                                 //ivlm
 cc_CYTot_maxStr                   = '(40)c_CYTot_max';                                    //ivlm
 cc_CYTot_minStr                   = '(40)c_CYTot_min';                                    //ivlm
 cc_CSIRO_PC_maxStr                = '(40)c_CSIRO_PC_max';                                 //ivlm
 cc_CSIRO_maxStr                   = '(40)c_CSIRO_max';                                    //ivlm
 cc_CSp_PC_maxStr                  = '(40)c_CSp_PC_max';                                   //ivlm
 cc_CSp_maxStr                     = '(40)c_CSp_max';                                      //ivlm
 cc_RSp_PC_maxStr                  = '(40)c_RSp_PC_max';                                   //ivlm
 cc_RSp_maxStr                     = '(40)c_RSp_max';                                      //ivlm
 cc_YB_PC_maxStr                   = '(40)c_YB_PC_max';                                    //ivlm
 cc_YB_MaxStr                      = '(40)c_YB_Max';                                       //ivlm
 cc_CBu_PC_maxStr                  = '(40)c_CBu_PC_max';                                   //ivlm
 cc_CBu_maxStr                     = '(40)c_CBu_max';                                      //ivlm
 cc_CUpY_PC_maxStr                 = '(40)c_CUpY_PC_max';                                  //ivlm
 cc_CUpY_maxStr                    = '(40)c_CUpY_max';                                     //ivlm
 cc_tLSt_PC_maxStr                 = '(40)c_tLSt_PC_max';                                  //ivlm
 cc_tLSt_maxStr                    = '(40)c_tLSt_max';                                     //ivlm
 cc_LSt_PC_maxStr                  = '(40)c_LSt_PC_max';                                   //ivlm
 cc_LSt_maxStr                     = '(40)c_LSt_max';                                      //ivlm
 cc_RedL_PC_maxStr                 = '(40)c_RedL_PC_max';                                  //ivlm
 cc_RedL_maxStr                    = '(40)c_RedL_max';                                     //ivlm
 cc_number_of_machinesStr          = '(40)c_number_of_machines';                           //ivlm
 rsAbsolut                         = '(12)Absolut'; // ivlm  Wird f�r Tabellenansicht ben�tigt

 //t_machine
 cc_machine_nameStr                = '(40)c_machine_name';                                 //ivlm
 cc_nr_of_spindlesStr              = '(40)c_nr_of_spindles';                               //ivlm
 cc_machine_modelStr               = '(40)c_machine_model';                                //ivlm

 //t_machine_offlimit
 cc_update_timeStr                 = '(40)c_update_time';                                  //ivlm
 cc_max_online_offlimit_timeStr    = '(40)c_max_online_offlimit_time';                     //ivlm
 cc_YBStr                          = '(40)c_YB';                                           //ivlm
 cc_YB_online_timeStr              = '(40)c_YB_online_time';                               //ivlm
 cc_YB_offline_timeStr             = '(40)c_YB_offline_time';                              //ivlm
 cc_RedLStr                        = '(40)c_RedL';                                         //ivlm
 cc_RedL_online_timeStr            = '(40)c_RedL_online_time';                             //ivlm
 cc_RedL_offline_timeStr           = '(40)c_RedL_offline_time';                            //ivlm
 cc_EffStr                         = '(40)c_Eff';                                          //ivlm
 cc_Eff_online_timeStr             = '(40)c_Eff_online_time';                              //ivlm
 cc_Eff_offline_timeStr            = '(40)c_Eff_offline_time';                             //ivlm
 cc_CSpStr                         = '(40)c_CSp';                                          //ivlm
 cc_CSp_online_timeStr             = '(40)c_CSp_online_time';                              //ivlm
 cc_CSp_offline_timeStr            = '(40)c_CSp_offline_time';                             //ivlm
 cc_RSpStr                         = '(40)c_RSp';                                          //ivlm
 cc_RSp_online_timeStr             = '(40)c_RSp_online_time';                              //ivlm
 cc_RSp_offline_timeStr            = '(40)c_RSp_offline_time';                             //ivlm
 cc_CBuStr                         = '(40)c_CBu';                                          //ivlm
 cc_CBu_online_timeStr             = '(40)c_CBu_online_time';                              //ivlm
 cc_CBu_offline_timeStr            = '(40)c_CBu_offline_time';                             //ivlm
 cc_CUpYStr                        = '(40)c_CUpY';                                         //ivlm
 cc_CUpY_online_timeStr            = '(40)c_CUpY_online_time';                             //ivlm
 cc_CUpY_offline_timeStr           = '(40)c_CUpY_offline_time';                            //ivlm
 cc_tLStStr                        = '(40)c_tLSt';                                         //ivlm
 cc_tLSt_online_timeStr            = '(40)c_tLSt_online_time';                             //ivlm
 cc_LSt_online_timeStr             = '(40)c_LSt_online_time';                              //ivlm
 cc_tLSt_offline_timeStr           = '(40)c_tLSt_offline_time';                            //ivlm
 cc_LSt_offline_timeStr            = '(40)c_LSt_offline_time';                             //ivlm
 cc_CSIROStr                       = '(40)c_CSIRO';                                        //ivlm
 cc_CSIRO_online_timeStr           = '(40)c_CSIRO_online_time';                            //ivlm
 cc_CSIRO_offline_timeStr          = '(40)c_CSIRO_offline_time';                           //ivlm
 cc_CYTotStr                       = '(40)c_CYTot';                                        //ivlm
 cc_CYTot_online_timeStr           = '(40)c_CYTot_online_time';                            //ivlm
 cc_CYTot_offline_timeStr          = '(40)c_CYTot_offline_time';                           //ivlm
 cc_OutOfProduction_online_timeStr = '(40)c_OutOfProduction_online_time';                  //ivlm
 cc_OutOfProduction_offline_timeStr = '(40)c_OutOfProduction_offline_time';                //ivlm
 cc_OutOfProductionStr             = '(40)c_OutOfProduction';                              //ivlm
 cc_offline_offlimit_timeStr       = '(40)c_offline_offlimit_time';                        //ivlm
 cc_offline_watchtimeStr           = '(40)c_offline_watchtime';                            //ivlm
 ccalc_OfflineRatioStr             = '(40)calc_OfflineRatio';                              //ivlm
 ccalc_offcause1Str                = '(40)calc_offcause1';                                 //ivlm
 ccalc_offcause2Str                = '(40)calc_offcause2';                                 //ivlm
 ccalc_offcause3Str                = '(40)calc_offcause3';                                 //ivlm

 //t__offlimit_cause
 cc_offcause_nameStr               = '(40)c_offcause_name';                                //ivlm
 cc_counterStr                     = '(40)c_counter';                                      //ivlm

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
                                                                                 
end. //u_resourcestrings.pas
