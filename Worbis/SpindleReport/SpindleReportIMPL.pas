(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SpindleReportIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP5
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date       Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.07.2001 2.01  Wss | implementation changed to use WrapperForm 
| 19.02.2004       Wss | Filtrierung nach Machinen wenn von Floor aufgestartet wird -> Init(), RunQuery()
|=========================================================================================*)
unit SpindleReportIMPL;

interface

uses
  ComObj, ActiveX, SpindleReport_TLB, LoepfePluginIMPL, LOEPFELIBRARY_TLB;

type
  TSpindleReportServer = class(TLoepfePlugin, ISpindleReportServer)
  protected
  public
    function ExecuteCommand: SYSINT; override;
    procedure Initialize; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
//------------------------------------------------------------------------------
uses
  mmMBCS,
 ComServ,SysUtils,u_offlimit_report,u_esp_machine_report, Forms, BaseForm;
{$R 'Toolbar.res'}
//------------------------------------------------------------------------------
type
  TCommandID = (ciEspReport,ciOfflimitReport);

const
  cFloorMenuDef: Array[0..1] of TFloorMenuRec = (
    // --- Main Menu File ---
  (CommandID: Ord(ciEspReport); SubMenu: 0; MenuBmpName: 'SPINDLEREPORT';
   MenuText: '(*)Spulstellendetail'; MenuHint: '(*)Spulstellendetail'; MenuStBar: '(*)Spulstellendetail';  //ivlm
   ToolbarText: 'Einzelspindel';
   MenuGroup: mgMain; MenuTyp: mtTrend; MenuState: msEnabled; MenuAvailable: maAll),

  (CommandID: Ord(ciOfflimitReport); SubMenu: 0; MenuBmpName: 'OFFLIMIT';
   MenuText: '(*)Maschinenofflimit'; MenuHint: '(*)Maschinenofflimit'; MenuStBar: '(*)Maschinenofflimit';  //ivlm
   ToolbarText: 'Offlimit';
   MenuGroup: mgMain; MenuTyp: mtTrend; MenuState: msEnabled; MenuAvailable: maAll)
  );

//------------------------------------------------------------------------------
// TSpindleReportServer
//------------------------------------------------------------------------------
function TSpindleReportServer.ExecuteCommand: SYSINT;
var
  xEspForm: TEsp_MachineReportDlg;
  xOffForm: TOffReportDlg;
  xWrap: TmmForm;
begin
  Result := 0;
  case TCommandID(CommandIndex) of
    ciEspReport: begin
        if GetWrappedDataForm(TEsp_MachineReportDlg, TForm(xEspForm), xWrap) then begin
          if xEspForm.Init(SelectedMachines, ProdID) then begin
            xEspForm.Visible := True;
            Result := xWrap.Handle;
          end;
        end;
      end;
    ciOfflimitReport: begin
        if GetWrappedDataForm(TOffReportDlg, TForm(xOffForm), xWrap) then begin
          xOffForm.Init(SelectedMachines);
          xOffForm.Visible := True;
          Result := xWrap.Handle;
        end;
      end;
  end;
end;
{
function TSpindleReportServer.ExecuteCommand: SYSINT;
var
  xEspForm: TEsp_MachineReportDlg;
  xOffForm: TOffReportDlg;
begin
  Result := 0;
  case TCommandID(CommandIndex) of
    ciEspReport: begin
        if GetCreateChildForm(TEsp_MachineReportDlg, TForm(xEspForm)) then begin
          xEspForm.Init(SelectedMachines, ProdID);
          Result := xEspForm.Handle;
        end;
      end;
    ciOfflimitReport: begin
        if GetCreateChildForm(TOffReportDlg, TForm(xOffForm)) then begin
          xOffForm.Init;
          Result := xOffForm.Handle;
        end;
      end;
  end;
end;
{}
//------------------------------------------------------------------------------
procedure TSpindleReportServer.Initialize;
begin
  Inherited Initialize;
  InitMenu(cFloorMenuDef);
end;
//------------------------------------------------------------------------------
initialization
  InitializeObjectFactory(TSpindleReportServer, Class_SpindleReportServer);
end.
