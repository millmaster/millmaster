(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_def_pattern_recognition.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: definitions for offlimit cause recognition
| Info..........: -
| Develop.system: Windows NT 5.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01, Multilizer, Infopower
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 21.08.01 | 0.00 | PW  | file created
| 26.04.04 | 0.00 | Wss | Sonderzeichen beim Text Bandbreite wegen Chinesisch entfernt
|=========================================================================================*)

{$i symbols.inc}

unit u_def_pattern_recognition;

interface

uses
  u_global,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, ComCtrls, mmTrackBar, StdCtrls, mmButton, mmLabel, mmPanel,
  mmGroupBox, ExtCtrls, IvDictio, IvMulti, IvEMulti, mmTranslator, ActnList,
  mmActionList, mmUpDown, NumCtrl, mmEdit;

type
  TfrmDefPatternRecognition = class(TmmDialog)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    mmTranslator: TmmTranslator;
    mmPanel1: TmmPanel;
    GroupBox1: TmmGroupBox;
    mmPanel2: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;
    tbWeight: TmmTrackBar;
    mmGroupBox1: TmmGroupBox;
    tbBandwidth: TmmTrackBar;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    laWeightLeft: TmmLabel;
    laWeightRight: TmmLabel;
    laBandWidth: TmmLabel;
    mmGroupBox2: TmmGroupBox;
    mmLabel9: TmmLabel;
    mmLabel10: TmmLabel;
    edMinHitrate: TmmEdit;
    edMaxCauses: TmmEdit;
    udMinHitrate: TmmUpDown;
    udMaxCauses: TmmUpDown;

    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure tbWeightChange(Sender: TObject);
    procedure tbBandwidthChange(Sender: TObject);
    procedure udMinHitrateClick(Sender: TObject; Button: TUDBtnType);
    procedure FormShow(Sender: TObject);
  private
  public
    constructor Create(aOwner: TComponent; aOfflimitPattern: rOfflimitPatternT); reintroduce; virtual;
    function GetOfflimitPattern: rOfflimitPatternT;
  end; //TfrmDefPatternRecognition

var
  frmDefPatternRecognition: TfrmDefPatternRecognition;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_main;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Bandbreite  %d'; // ivlm

constructor TfrmDefPatternRecognition.Create(aOwner: TComponent;
  aOfflimitPattern: rOfflimitPatternT);
begin
  inherited create(aOwner);
  tbWeight.Position := aOfflimitPattern.Weight;
  tbBandwidth.Position := aOfflimitPattern.Bandwidth;
  udMinHitrate.Position := aOfflimitPattern.MinHitrate;
  udMaxCauses.Position := aOfflimitPattern.MaxCauses;
end; //constructor TfrmDefPatternRecognition.Create
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.acOKExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrOK;
end; //procedure TfrmDefPatternRecognition.acOKExecute
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.acCancelExecute(Sender: TObject);
begin
  inherited;
  ModalResult := mrCancel;
end; //procedure TfrmDefPatternRecognition.acCancelExecute
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.acHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, HelpContext)
end; //procedure TfrmDefPatternRecognition.acHelpExecute
//-----------------------------------------------------------------------------
function TfrmDefPatternRecognition.GetOfflimitPattern: rOfflimitPatternT;
begin
  result.BandWidth := tbBandwidth.Position;
  result.Weight := tbWeight.Position;
  result.MinHitrate := udMinHitrate.Position;
  result.MaxCauses := udMaxCauses.Position;
end; //function TfrmDefPatternRecognition.GetOfflimitPattern
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.tbWeightChange(Sender: TObject);
begin
  inherited;
  laWeightLeft.Caption := inttostr(100 -tbWeight.Position) +'%';
  laWeightRight.Caption := inttostr(tbWeight.Position) +'%';
end; //procedure TfrmDefPatternRecognition.tbWeightChange
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.tbBandwidthChange(Sender: TObject);
begin
  inherited;
  laBandWidth.Caption := format(cMsg1,[tbBandwidth.Position])+'%';
end; //procedure TfrmDefPatternRecognition.tbBandwidthChange
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.udMinHitrateClick(Sender: TObject;
  Button: TUDBtnType);
begin
  inherited;
  edMinHitrate.Text := formatfloat(',0 %',udMinHitrate.Position);
end; //procedure TfrmDefPatternRecognition.udMinHitrateClick
//-----------------------------------------------------------------------------
procedure TfrmDefPatternRecognition.FormShow(Sender: TObject);
begin
  inherited;
  tbWeightChange(nil);
  tbBandwidthChange(nil);
  udMinHitrateClick(nil,btNext);
end; //procedure TfrmDefPatternRecognition.FormShow

end. //u_def_pattern_recognition
