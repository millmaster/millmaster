(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_dmSpindleReport.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale strings
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.06.05 | 1.16 | Wss | - Spectra+ und Zenit Elemente hinzugefügt
|=========================================================================================*)
unit u_dmSpindleReport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADODataSet, mmADOConnection, mmVCLStringList;

type
  TdmSpindleReport = class(TDataModule)
    conDefault: TmmADOConnection;
    slOnlineOfflimit: TmmVCLStringList;
    slOfflineOfflimit: TmmVCLStringList;
    slMachineReport: TmmVCLStringList;
    slSpindleReport: TmmVCLStringList;
    slMachineSummary: TmmVCLStringList;
  private
  public
  end;

var
  dmSpindleReport: TdmSpindleReport;

implementation

{$R *.DFM}

end.
