inherited StandardSelectDlg: TStandardSelectDlg
  Left = 522
  Top = 117
  Width = 307
  Height = 316
  Constraints.MinWidth = 252
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object lbNames: TNWListBox
    Left = 0
    Top = 0
    Width = 299
    Height = 259
    Align = alClient
    ExtendedSelect = False
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    ItemHeight = 15
    ParentFont = False
    TabOrder = 0
    OnDblClick = acOKExecute
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 259
    Width = 299
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 59
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 139
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 219
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 10
    Top = 58
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 40
    Top = 56
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0))
  end
  object quSelect: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    CommandText = 'SELECT name,id FROM table ORDER BY name '
    Parameters = <>
    Left = 82
    Top = 58
  end
end
