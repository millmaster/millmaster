inherited frmSetOfflimitCause: TfrmSetOfflimitCause
  Left = 563
  Top = 275
  Caption = '(*)Offlimit Ursache zuweisen'
  ClientHeight = 160
  ClientWidth = 335
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel2: TmmPanel
    Left = 0
    Top = 130
    Width = 335
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object bOK: TmmButton
      Left = 95
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 175
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 255
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 335
    Height = 130
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 325
      Height = 120
      Caption = '(*)Auswahl'
      TabOrder = 0
      object mmLabel1: TmmLabel
        Left = 10
        Top = 20
        Width = 56
        Height = 13
        Caption = '(*)Maschine'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 10
        Top = 40
        Width = 55
        Height = 13
        Caption = '(*)Spulstelle'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel3: TmmLabel
        Left = 10
        Top = 70
        Width = 84
        Height = 13
        Caption = '(*)Offlimit-Ursache'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laMachineName: TmmLabel
        Left = 110
        Top = 20
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laSpindleID: TmmLabel
        Left = 110
        Top = 40
        Width = 8
        Height = 13
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object comCause: TmmComboBox
        Left = 10
        Top = 85
        Width = 200
        Height = 21
        Style = csDropDownList
        Color = clWindow
        DropDownCount = 12
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object mmButton1: TmmButton
        Left = 220
        Top = 84
        Width = 95
        Height = 23
        Action = acEditOfflimitCause
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object ActionList1: TmmActionList
    Left = 258
    Top = 18
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
    object acEditOfflimitCause: TAction
      Caption = '(15)Bearbeiten...'
      Hint = '(*)Offlimit-Ursachen bearbeiten'
      OnExecute = acEditOfflimitCauseExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 290
    Top = 19
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Filter'
        0))
  end
end
