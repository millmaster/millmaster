inherited frmDefPatternRecognition: TfrmDefPatternRecognition
  Left = 510
  Top = 209
  Caption = '(*)Offlimit-Ursachenerkennung definieren'
  ClientHeight = 340
  ClientWidth = 410
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 410
    Height = 310
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object GroupBox1: TmmGroupBox
      Left = 5
      Top = 5
      Width = 400
      Height = 110
      Caption = '(*)Gewichtung Offlimit-Ursachen / Muster'
      TabOrder = 0
      CaptionSpace = True
      object mmLabel1: TmmLabel
        Left = 10
        Top = 60
        Width = 130
        Height = 40
        Alignment = taCenter
        AutoSize = False
        Caption = '(*)Gewichtung auf Offlimit-Ursachen'
        Visible = True
        WordWrap = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 260
        Top = 60
        Width = 130
        Height = 40
        Alignment = taCenter
        AutoSize = False
        Caption = '(*)Gewichtung auf Ursachenmuster'
        Visible = True
        WordWrap = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laWeightLeft: TmmLabel
        Left = 15
        Top = 30
        Width = 55
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laWeightRight: TmmLabel
        Left = 330
        Top = 30
        Width = 55
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object tbWeight: TmmTrackBar
        Left = 75
        Top = 25
        Width = 250
        Height = 30
        Cursor = crHandPoint
        Max = 100
        Orientation = trHorizontal
        PageSize = 10
        Frequency = 25
        Position = 80
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBoth
        TickStyle = tsNone
        Visible = True
        OnChange = tbWeightChange
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 120
      Width = 400
      Height = 90
      Caption = '(*)Musteruebereinstimmung'
      TabOrder = 1
      CaptionSpace = True
      object laBandWidth: TmmLabel
        Left = 10
        Top = 60
        Width = 380
        Height = 13
        Alignment = taCenter
        AutoSize = False
        Caption = '0'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object tbBandwidth: TmmTrackBar
        Left = 75
        Top = 25
        Width = 250
        Height = 30
        Cursor = crHandPoint
        Max = 100
        Orientation = trHorizontal
        PageSize = 10
        Frequency = 25
        Position = 30
        SelEnd = 0
        SelStart = 0
        TabOrder = 0
        TickMarks = tmBoth
        TickStyle = tsNone
        Visible = True
        OnChange = tbBandwidthChange
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmGroupBox2: TmmGroupBox
      Left = 5
      Top = 215
      Width = 400
      Height = 90
      Caption = '(*)Anzeigeunterdrueckung'
      TabOrder = 2
      CaptionSpace = True
      object mmLabel9: TmmLabel
        Left = 10
        Top = 20
        Width = 195
        Height = 26
        AutoSize = False
        Caption = '(*)Offlimit-Ursache nur anzeigen wenn Vorhersage groesser als'
        Visible = True
        WordWrap = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel10: TmmLabel
        Left = 10
        Top = 55
        Width = 195
        Height = 26
        AutoSize = False
        Caption = '(*)Anzahl der maximal anzuzeigenden Offlimit-Ursachen'
        Visible = True
        WordWrap = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edMinHitrate: TmmEdit
        Left = 215
        Top = 23
        Width = 50
        Height = 21
        Color = clWindow
        ReadOnly = True
        TabOrder = 0
        Text = '0'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edMaxCauses: TmmEdit
        Left = 215
        Top = 58
        Width = 50
        Height = 21
        Color = clWindow
        ReadOnly = True
        TabOrder = 1
        Text = '0'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object udMinHitrate: TmmUpDown
        Left = 265
        Top = 23
        Width = 16
        Height = 21
        Associate = edMinHitrate
        Min = 0
        Position = 0
        TabOrder = 2
        Wrap = True
        OnClick = udMinHitrateClick
      end
      object udMaxCauses: TmmUpDown
        Left = 265
        Top = 58
        Width = 16
        Height = 21
        Associate = edMaxCauses
        Min = 0
        Max = 999
        Position = 0
        TabOrder = 3
        Wrap = True
      end
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 310
    Width = 410
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 170
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 250
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 330
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 330
    Top = 2
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 362
    Top = 3
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
