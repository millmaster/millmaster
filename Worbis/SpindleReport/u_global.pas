(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_global.pas
| Projectpart...: MillMaster NT Spulerei, Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Typen, Variablen, Konstanten
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.04.98 | 0.00 | PW  | Datei erstellt
| 12.10.99 | 0.01 | PW  | Prog Options with MemoINI to save options in database
| 22.03.00 |      | sdo | RedLight ausgeschaltet -> // @@RedL
| 06.04.00 |      | sdo | neuer Recoerd fuer SettingsDLG erstellt (TParamRec, Class TParameterList)
| 16.08.01 |      | PW  | - TProgOptions modified:
|                           ResetFilter, OffPatternBandwidth, OffPatternWeight added
| 29.08.01 |      | Wss | Create query in TOfflimitCause.Create changed to Nil owner
| 31.08.01 |      | pw  | TOfflimitCauses.GetCauses modified
                          - consider "spindle out of production"
                          - act. number of offlimit positions / total number of offlimit positions
| 03.09.01 |      | Wss | GetCauses: division by zero handled with if check instead of try..except
| 29.10.02 |      | Wss | Procedure SetDefaultMaskInDisplayFormat() hinzugef�gt
| 22.10.02 |      | LOK | Umbau ADO
| 18.02.08 | 1.10 | Nue | Einbau VCV MM5.04
|=========================================================================================*)

{$I symbols.inc}

unit u_global;

interface

uses
  u_common_global, mmGraphGlobal, mmLengthUnit, mmGridPos, mmList, Forms,
  SysUtils, DB, Graphics, WinTypes, WinProcs, Classes, Printers, MemoINI,
  MMUGlobal, LoepfeGlobal, mmAdoQuery, mmADODataSet;

const
  // Umrechnungsfaktoren f�r die Anzeige der Daten in der gew�nschten LenBase
  cBaseFactor: Array[lb100km..lb1000000Y] of Double = (1.0, 10.0, cMToYd, 10*cMToYd);

const
 //divers
  cNonVisible = 0;
  cShowTable = 10;
  cShowTableGraphic = 20;
  cShowKey = 30;

  cRefreshInterval = 120;               //seconds
  cProgramVersion = '1.00';
  cCompileDate = '22.05.1999';
  cMaxEspParameter = 69;     //Nue:18.02.08 von 67 auf 69
  cDefaultStartupIntervals = 10;
  cListSep = '|';

  cDefaultGridTitleColor = clLightGray3;
  cFixedColColor = clLightYellow;

  cBaseKey = 'Software\Loepfe\MMSpindleReport';

 //Helpsystem, Filename and Context IDs
  cHelpfilename = 'MMEspReport.HLP';
  hcOffset = 1000;
  hcGlobalInfo = hcOffset + 1;
  hcEsp_MachineReport = hcOffset + 2;
  hcOfflimit_Report = hcOffset + 3;
  hcSetPrintOptions = hcOffset + 4;
  hcDefOfflimitReportDlg = hcOffset + 5;
  hcEditEspParameterDlg = hcOffset + 6;
  hcEditInsertEspParameterDlg = hcOffset + 7;
  hcEditOfflimitCauseDlg = hcOffset + 8;
  hcEsp_SpindleReportDlg = hcOffset + 9;
  hcExtendedDateSelectDlg = hcOffset + 10;
  hcOffLimitEditorDlg = hcOffset + 11;
  hcOpenProfilDlg = hcOffset + 12;
  hcSaveProfilDlg = hcOffset + 13;
  hcSelectIntervalDlg = hcOffset + 14;
  hcSelectPeriodDlg = hcOffset + 15;
  hcStandardSelectDlg = hcOffset + 16;
  hcViewYMSettingsDlg = hcOffset + 17;

//-----------------------------------------------------------------------------

type
  rListSelResultT = record
    ID: integer;
    Text: string;
  end;                                  //rListSelResultT

  rShiftInfoT = record
    ShiftNo: byte;
    ShiftBegin,
      ShiftEnd: tdatetime;
  end;                                  //rShiftInfoT

  rPeriodInfoT = record
    ID: integer;
    pBegin,
      pEnd: tdatetime;
    pBeginID,
      pEndID: integer;
  end;                                  //rPeriodInfoT

  rEspParameterT = record
    ID,
      FilterID: integer;
    FieldName: s30;
    ShowInReport: boolean;
    LowerLimit,
      UpperLimit: double;
    Statistik: sStatistikT;
    ShowGraph: boolean;
    ChartType: eChartTypeT;
    Color: TColor;
    YScalingType: eYScalingTypeT;
  end;                                  //rEspParameterT

  rBaseLengthUnitsT = record
    Length,
      Weight,
      Bobines,
      Cones: double;
  end;                                  //rBaseLengthUnitsT

  rEspParameterArrayT = array of rEspParameterT;
  prEspParameterArrayT = ^rEspParameterArrayT;

  rESPReportInfoT = record
    LengthUnit: TLenBase;
    ProdGroupID,
      MachineID,
      Spindle: integer;
    StartTime,
      EndTime: tdatetime;
    MinSpindle,
      MaxSpindle: integer;
  end;                                  //rESPReportInfoT

  rOffCausesT = record
    Name: string[40];
    offcntNumber,
      cntNumber: integer;
    PatternHitrate,
      OccurrenceHitrate,
      TotalHitrate: double;
  end;                                  //rOffCausesT

  aOffCausesT = array of rOffCausesT;

  eUserModeT = (umEdit, umView);

  eOfflimitInfoT = (oiSpindle, oiMean,
    oiLowerFixedLimit, oiUpperFixedLimit,
    oiLowerRelativeLimit, oiUpperrelativeLimit,
    oiLowerRelativePC, oiUpperRelativePC);
  sOfflimitInfoT = set of eOfflimitInfoT;
  eOfflimitDataT = (otYB, otEff, otCSp, otRSp, otCBu, otCUpY, ottLSt, otLSt, otCSIRO, otCYTot, otOP, otNone); // @@RedL
  sOfflimitDataT = set of eOfflimitDataT;

  eGrphicTypeT = (gtLine, gtBar, gtBarSide, gtBarStacked, gtBarStacked100);

  eCompareT = (cmGreater, cmSmaller);

  rResetFilterT = record
    RunTimeValue,
      RatioValue: integer;
    RatioCompare,
      RuntimeCompare: eCompareT;
  end;                                  //tResetFilterT

  rOfflimitPatternT = record
    Weight,
      BandWidth,
      MinHitrate,
      MaxCauses: integer
  end;                                  //rOfflimitPatternT


  TOfflimitFields = class(TPersistent)
  private
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function TypeByFieldname(aFieldname: string): eOfflimitDataT;
    function FieldnameByType(aType: eOfflimitDataT): string;
  end;                                  //TOfflimitFields

  TOfflimits = class(TPersistent)
  private
    FFields: TOfflimitFields;
    FIsNull: array[eOfflimitInfoT, eOfflimitDataT] of boolean;
    FLimitFaults: array[eOfflimitDataT] of sOfflimitInfoT;
    FValue: array[eOfflimitInfoT, eOfflimitDataT] of double;
    FOutOfProduction: boolean;
    procedure SetValue(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aValue: double);
    function GetValue(aInfo: eOfflimitInfoT; aData: eOfflimitDataT): double;
    procedure SetIsNull(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aValue: boolean);
    function GetIsNull(aInfo: eOfflimitInfoT; aData: eOfflimitDataT): boolean;
    procedure SetLimitFaults(aData: eOfflimitDataT; aValue: sOfflimitInfoT);
    function GetLimitFaults(aData: eOfflimitDataT): sOfflimitInfoT;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function AsString(aFormat: string; aInfo: eOfflimitInfoT; aData: eOfflimitDataT): string;
    procedure Clear;
    procedure Calculate;
    procedure SetData(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aField: TField);
    property Value[aInfo: eOfflimitInfoT; aData: eOfflimitDataT]: double read GetValue write SetValue;
    property IsNull[aInfo: eOfflimitInfoT; aData: eOfflimitDataT]: boolean read GetIsNull write SetIsNull;
    property LimitFaults[aData: eOfflimitDataT]: sOfflimitInfoT read GetLimitFaults write SetLimitFaults;
  published
    property Fields: TOfflimitFields read FFields write FFields;
    property OutOfProduction: boolean read FOutOfProduction write FOutOfProduction;
  end;                                  //TOfflimits

  TProgOptions = class(TBaseMemoINIObject)
  private
    FShowOfflimitInfoWindow: boolean;
    FOnlineTimeLimit: integer;
    FOnlineShowOutOfProdSpindles: boolean;
    FOfflineTimeLimit: integer;
    FOfflineRatio: double;
    FResetFilter: rResetFilterT;
    FOfflimitPattern: rOfflimitPatternT;
  protected
  public
   // constructor Create; overload;
    constructor Create(aOwner: TComponent); overload;
    destructor Destroy; override;
    procedure ReadSettings;
    procedure WriteSettings;
    property ShowOfflimitInfoWindow: boolean read FShowOfflimitInfoWindow write FShowOfflimitInfoWindow;
    property OnlineTimeLimit: integer read FOnlineTimeLimit write fOnlineTimeLimit;
    property OnlineShowOutOfProdSpindles: boolean read FOnlineShowOutOfProdSpindles write FOnlineShowOutOfProdSpindles;
    property OfflineTimeLimit: integer read FOfflineTimeLimit write FOfflineTimeLimit;
    property OfflineRatio: double read FOfflineRatio write FOfflineRatio;
    property ResetFilter: rResetFilterT read FResetFilter write FResetFilter;
    property OfflimitPattern: rOfflimitPatternT read FOfflimitPattern write FOfflimitPattern;
  end;                                  //TProgOptions

  TReportType = (rtStatistic, rtOfflimit, rtOnline, rtMachine);

  TParamRec = record
    SelectedColumn: Boolean;
    GraphicParam: rEspParameterT;
    ColumnParam: tColumnRecordT;
  end;                                  //TParamRec

  PParamRec = ^TParamRec;

  TScaleRec = record
    Autoscale: Boolean;
    Min: Longint;
    Max: Longint;
  end;

  pScaleRec = ^TScaleRec;

  pLeftYAxis = pScaleRec;
  pRightYAxis = pScaleRec;

  TParameterList = class(TmmList)
  private
    FBarStyle: eBarStyleT;

    FLeftYAxis: pLeftYAxis;
    FRightYAxis: pRightYAxis;

    function GetItems(aIndex: Integer): PParamRec;

    procedure SetBarStyle(const Value: eBarStyleT);
    function GetBarStyle: eBarStyleT;
  protected
  public
    property Items[aIndex: Integer]: PParamRec read GetItems;
    property BarStyle: eBarStyleT read GetBarStyle write SetBarStyle;

    property LeftYAxis: pLeftYAxis read FLeftYAxis write FLeftYAxis;
    property RightYAxis: pLeftYAxis read FRightYAxis write FRightYAxis;
    constructor Create; virtual;
    destructor Destroy; override;
    procedure Clear; override;
    procedure Assign(aSource: TObject); override;
  end;                                  //TParameterList


  TOfflimitCauses = class(TPersistent)
  private
    FQry: TmmADOQuery;
    FFields: TOfflimitFields;
    FOfflimitPattern: rOfflimitPatternT;
    procedure SetOfflimitPattern(Value: rOfflimitPatternT);
  protected
    Causes: aOffCausesT;
    procedure SortCauses;
  public
    constructor Create;
    destructor Destroy; override;
    function GetCauses(aOfflimits: TOfflimits): aOffCausesT;
    procedure ReadOfflimitCauses;
  published
    property Fields: TOfflimitFields read FFields write FFields;
    property OfflimitPattern: rOfflimitPatternT read FOfflimitPattern write SetOfflimitPattern;
  end;                                  //TOfflimitCauses



//-----------------------------------------------------------------------------
//procedure SetDefaultMaskInDisplayFormat(aDataSet: TmmADODataSet);
//-----------------------------------------------------------------------------

const
  cOfflimitTypes = [oiLowerFixedLimit, oiUpperFixedLimit, oiLowerRelativeLimit, oiUpperrelativeLimit];

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  u_common_lib;

//------------------------------------------------------------------------------
// Diese Prozedure wird vor dem �ffnen des DataSet aufgerufen, um bei Float Feldern
// ein Default DisplayFormat zu setzen. N�tig, weil die Profile kein Defaultwert
// enthalten und urspr�nglich die Datenfelder als Integer Typ ausgelesen wurden.
{
procedure SetDefaultMaskInDisplayFormat(aDataSet: TmmADODataSet);
var
  i: Integer;
begin
  with aDataSet do begin
    for i:=0 to Fields.Count-1 do begin
      if Fields[i] is TFloatField then
        with TFloatField(Fields[i]) do
          if DisplayFormat = '' then
            DisplayFormat := ',0.0';
    end;
  end;
end;
{}
//------------------------------------------------------------------------------
// TOfflimitFields
//------------------------------------------------------------------------------
constructor TOfflimitFields.Create;
begin
  inherited create;
end;                                    //constructor TOfflimitFields.Create
//-----------------------------------------------------------------------------
destructor TOfflimitFields.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TOfflimitFields.Destroy
//-----------------------------------------------------------------------------
function TOfflimitFields.FieldnameByType(aType: eOfflimitDataT): string;
begin
  if aType = otYB then result := 'c_yb'
  else if aType = otEff then result := 'c_eff'
  else if aType = otCSp then result := 'c_csp'
  else if aType = otRSp then result := 'c_rsp'
  else if aType = otCBu then result := 'c_cbu'
  else if aType = otCUpY then result := 'c_cupy'
  else if aType = ottLSt then result := 'c_tlst'
  else if aType = otLSt then result := 'c_lst'
  else if aType = otCSIRO then result := 'c_csiro'
  else if aType = otCYTot then result := 'c_cytot'
  else if aType = otOP then result := 'c_op'
  else result := '';
end;                                    //function TOfflimitFields.FieldnameByType
//-----------------------------------------------------------------------------
function TOfflimitFields.TypeByFieldname(aFieldname: string): eOfflimitDataT;
begin
  aFieldname := UpperCase(aFieldname);
  if aFieldname = 'C_YB' then result := otYB
  else if aFieldname = 'C_EFF' then result := otEff
  else if aFieldname = 'C_CSP' then result := otCSp
  else if aFieldname = 'C_RSP' then result := otRSp
  else if aFieldname = 'C_CBU' then result := otCBu
  else if aFieldname = 'C_CUPY' then result := otCUpY
  else if aFieldname = 'C_TLST' then result := ottLSt
  else if aFieldname = 'C_LST' then result := otLSt
  else if aFieldname = 'C_CSIRO' then result := otCSIRO
  else if aFieldname = 'C_CYTOT' then result := otCYTot
  else if aFieldname = 'C_OP' then result := otOP
  else result := otNone;
end;                                    //function TOfflimitFields.TypeByFieldname


{ TOfflimits }

constructor TOfflimits.Create;
begin
  inherited create;
  fillchar(FValue, sizeof(FValue), 0);
  fillchar(FIsNull, sizeof(FIsNull), 1);
  FFields := TOfflimitFields.Create;
end;                                    //constructor TOfflimits.Create
//-----------------------------------------------------------------------------
destructor TOfflimits.Destroy;
begin
  FFields.Free;
  inherited Destroy;
end;                                    //destructor TOfflimits.Destroy
//-----------------------------------------------------------------------------
procedure TOfflimits.SetValue(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aValue: double);
begin
  FValue[aInfo, aData] := aValue;
  SetIsNull(aInfo, aData, false);
end;                                    //procedure TOfflimits.SetValue
//-----------------------------------------------------------------------------
function TOfflimits.GetValue(aInfo: eOfflimitInfoT; aData: eOfflimitDataT): double;
begin
  result := FValue[aInfo, aData];
end;                                    //function TOfflimits.GetValue
//-----------------------------------------------------------------------------
procedure TOfflimits.SetIsNull(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aValue: boolean);
begin
  FIsNull[aInfo, aData] := aValue;
end;                                    //procedure TOfflimits.SetIsNull
//-----------------------------------------------------------------------------
function TOfflimits.GetIsNull(aInfo: eOfflimitInfoT; aData: eOfflimitDataT): boolean;
begin
  result := FIsNull[aInfo, aData];
end;                                    //function TOfflimits.GetIsNull
//-----------------------------------------------------------------------------
procedure TOfflimits.SetData(aInfo: eOfflimitInfoT; aData: eOfflimitDataT; aField: TField);
begin
  if not aField.IsNull then Value[aInfo, aData] := aField.Value;
end;                                    //procedure TOfflimits.SetData
//-----------------------------------------------------------------------------
procedure TOfflimits.Clear;
begin
  fillchar(FValue, sizeof(FValue), 0);
  fillchar(FIsNull, sizeof(FIsNull), 1);
end;                                    //procedure TOfflimits.Clear
//-----------------------------------------------------------------------------
function TOfflimits.AsString(aFormat: string; aInfo: eOfflimitInfoT; aData: eOfflimitDataT): string;
begin
  if FIsNull[aInfo, aData] then result := ''
  else result := formatfloat(aFormat, FValue[aInfo, aData]);
end;                                    //function TOfflimits.AsString
//-----------------------------------------------------------------------------
procedure TOfflimits.SetLimitFaults(aData: eOfflimitDataT; aValue: sOfflimitInfoT);
begin
  FLimitFaults[aData] := aValue;
end;                                    //procedure TOfflimits.ASetLimitFaults
//-----------------------------------------------------------------------------
function TOfflimits.GetLimitFaults(aData: eOfflimitDataT): sOfflimitInfoT;
begin
  result := FLimitFaults[aData];
end;                                    //function TOfflimits.AGetLimitFaults
//-----------------------------------------------------------------------------
procedure TOfflimits.Calculate;
var
  xData: eOfflimitDataT;

begin
  for xData := otYB to otOP do
    FLimitFaults[xData] := [];

  if FOutOfProduction then LimitFaults[otOP] := [oiLowerFixedLimit];

  for xData := otYB to otCYTot do begin
    if not FIsNull[oiMean, xData] then begin
      if not FIsNull[oiLowerRelativePC, xData] then
        Value[oiLowerRelativeLimit, xData] := (100 - FValue[oiLowerRelativePC, xData]) / 100 * FValue[oiMean, xData];

      if not FIsNull[oiUpperRelativePC, xData] then
        Value[oiUpperRelativeLimit, xData] := (100 + FValue[oiUpperRelativePC, xData]) / 100 * FValue[oiMean, xData];
    end;                                //if not FIsNull[oiMean,xData] then

    if (not FIsNull[oiSpindle, xData]) and (not FIsNull[oiLowerFixedLimit, xData]) then
      if FValue[oiSpindle, xData] < FValue[oiLowerFixedLimit, xData] then
        FLimitFaults[xData] := FLimitFaults[xData] + [oiLowerFixedLimit];

    if (not FIsNull[oiSpindle, xData]) and (not FIsNull[oiLowerRelativeLimit, xData]) then
      if FValue[oiSpindle, xData] < FValue[oiLowerRelativeLimit, xData] then
        FLimitFaults[xData] := FLimitFaults[xData] + [oiLowerRelativeLimit];

    if (not FIsNull[oiSpindle, xData]) and (not FIsNull[oiUpperFixedLimit, xData]) then
      if FValue[oiSpindle, xData] > FValue[oiUpperFixedLimit, xData] then
        FLimitFaults[xData] := FLimitFaults[xData] + [oiUpperFixedLimit];

    if (not FIsNull[oiSpindle, xData]) and (not FIsNull[oiUpperRelativeLimit, xData]) then
      if FValue[oiSpindle, xData] > FValue[oiUpperRelativeLimit, xData] then
        FLimitFaults[xData] := FLimitFaults[xData] + [oiUpperRelativeLimit];
  end;                                  //for xData := otYB to otCYTot do
end;                                    //procedure TOfflimits.Calculate


{ TProgOptions }

constructor TProgOptions.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FShowOfflimitInfoWindow := true;
  FOnlineTimeLimit := 120;
  FOnlineShowOutOfProdSpindles := true;
  FOfflineTimeLimit := 120;
  FOfflineRatio := 1.0;
  FOfflimitPattern.MaxCauses := 6;
  FOfflimitPattern.MinHitrate := 50;
  FOfflimitPattern.Weight := 50;
  FOfflimitPattern.BandWidth := 20;
  FResetFilter.RunTimeValue := 0;
  FResetFilter.RatioValue := 0;
  FResetFilter.RatioCompare := cmGreater;
  FResetFilter.RuntimeCompare := cmGreater;
end;                                    //constructor TProgOptions.Create
//-----------------------------------------------------------------------------
destructor TProgOptions.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TProgOptions.Destroy
//-----------------------------------------------------------------------------
procedure TProgOptions.ReadSettings;
begin
  MemoIni.ReadSettings;
  FShowOfflimitInfoWindow := MemoIni.ReadBool('ProgOptions.Offlimit', 'ShowOfflimitInfoWindow', true);
  FOnlineTimeLimit := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OnlineTimeLimit', 120);
  FOnlineShowOutOfProdSpindles := MemoIni.ReadBool('ProgOptions.Offlimit', 'OnlineShowOutOfProdSpindles', true);
  FOfflineTimeLimit := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OfflineTimeLimit', 120);
  FOfflineRatio := MemoIni.ReadFloat('ProgOptions.Offlimit', 'OfflineRatio', 1);

  FOfflimitPattern.MaxCauses := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OfflimitPattern.MaxCauses', 6);
  FOfflimitPattern.MinHitRate := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OfflimitPattern.MinHitrate', 50);
  FOfflimitPattern.BandWidth := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OfflimitPattern.Bandwidth', 20);
  FOfflimitPattern.Weight := MemoIni.ReadInteger('ProgOptions.Offlimit', 'OfflimitPattern.Weight', 50);

  FResetFilter.RunTimeValue := MemoIni.ReadInteger('ProgOptions.Offlimit', 'ResetFilter.RunTimeValue', 0);
  FResetFilter.RatioValue := MemoIni.ReadInteger('ProgOptions.Offlimit', 'ResetFilter.RatioValue', 0);
  FResetFilter.RatioCompare := eCompareT(MemoIni.ReadInteger('ProgOptions.Offlimit', 'ResetFilter.RatioCompare', ord(cmGreater)));
  FResetFilter.RuntimeCompare := eCompareT(MemoIni.ReadInteger('ProgOptions.Offlimit', 'ResetFilter.RuntimeCompare', ord(cmGreater)))
end;                                    //procedure TProgOptions.ReadSettings
//-----------------------------------------------------------------------------
procedure TProgOptions.WriteSettings;
begin
  MemoIni.ReadSettings;
  try
    MemoIni.EraseSection('ProgOptions.Offlimit');
  except
  end;                                  //try..except

  MemoIni.WriteBool('ProgOptions.Offlimit', 'ShowOfflimitInfoWindow', FShowOfflimitInfoWindow);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OnlineTimeLimit', FOnlineTimeLimit);
  MemoIni.WriteBool('ProgOptions.Offlimit', 'OnlineShowOutOfProdSpindles', FOnlineShowOutOfProdSpindles);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OfflineTimeLimit', FOfflineTimeLimit);
  MemoIni.WriteFloat('ProgOptions.Offlimit', 'OfflineRatio', FOfflineRatio);

  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OfflimitPattern.MaxCauses', FOfflimitPattern.MaxCauses);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OfflimitPattern.MinHitrate', FOfflimitPattern.MinHitrate);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OfflimitPattern.Bandwidth', FOfflimitPattern.BandWidth);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'OfflimitPattern.Weight', FOfflimitPattern.Weight);

  MemoIni.WriteInteger('ProgOptions.Offlimit', 'ResetFilter.RunTimeValue', FResetFilter.RunTimeValue);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'ResetFilter.RatioValue', FResetFilter.RatioValue);
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'ResetFilter.RatioCompare', ord(ResetFilter.RatioCompare));
  MemoIni.WriteInteger('ProgOptions.Offlimit', 'ResetFilter.RuntimeCompare', ord(FResetFilter.RuntimeCompare));
  MemoIni.WriteSettings;
end;                                    //TProgOptions.WriteSettings


{ TParameterList }

constructor TParameterList.Create;
begin
  inherited Create;
  FBarStyle := bsSide;
  New(FLeftYAxis);
  New(FRightYAxis);
end;                                    //constructor TParameterList.Create
//------------------------------------------------------------------------------
procedure TParameterList.Clear;
begin
  while Count > 0 do begin
    Dispose(Items[0]);
    Delete(0);
  end;                                  //while Count > 0 do begin
  inherited Clear;
end;                                    //procedure TParameterList.Clear
//------------------------------------------------------------------------------
function TParameterList.GetItems(aIndex: Integer): pParamRec;
begin
  Result := inherited Items[aIndex];
end;                                    //function TParameterList.GetItems
//------------------------------------------------------------------------------
procedure TParameterList.Assign(aSource: TObject);
var xParm: PParamRec;
  xSrc: TParameterList;
  i: Integer;
begin
  if aSource is TParameterList then begin
    Clear;
    xSrc := aSource as TParameterList;
    if Assigned(xSrc) then begin
      for i := 0 to xSrc.Count - 1 do begin
        new(xParm);
        xParm^ := xSrc.Items[i]^;
        Add(xParm);
      end;

      FBarStyle    := xSrc.BarStyle;
      FLeftYAxis^  := xSrc.LeftYAxis^;
      FRightYAxis^ := xSrc.RightYAxis^;
    end;
  end else
    inherited Assign(aSource);
end;                                    //procedure TParameterList.Assign
//------------------------------------------------------------------------------
procedure TParameterList.SetBarStyle(const Value: eBarStyleT);
begin
  FBarStyle := Value;
end;                                    //procedure TParameterList.SetBarStyle
//------------------------------------------------------------------------------
function TParameterList.GetBarStyle: eBarStyleT;
begin
  Result := FBarStyle;
end;                                    //function TParameterList.GetBarStyle
//------------------------------------------------------------------------------
destructor TParameterList.Destroy;
begin
  Dispose(FLeftYAxis);
  Dispose(FRightYAxis);
  inherited Destroy;
end;


{ TOfflimitCauses }

constructor TOfflimitCauses.Create;
begin
  inherited create;
  FQry := TmmADOQuery.Create(nil);         // never use Application for as owner -> problems in destructor
  FQry.ConnectionString := GetDefaultConnectionString;
  FFields := TOfflimitFields.Create;
end;                                    //constructor TOfflimitCauses.Create
//-----------------------------------------------------------------------------
destructor TOfflimitCauses.Destroy;
begin
  FQry.Free;
  FFields.Free;
  inherited Destroy;
end;                                    //destructor TOfflimitCauses.Destroy
//-----------------------------------------------------------------------------
function TOfflimitCauses.GetCauses(aOfflimits: TOfflimits): aOffCausesT;
const
  cMaxNumberOfHits = 10;

var
  xData: eOfflimitDataT;
  xFieldname: string;
  i, j: integer;
  xDev,
    xMin,
    xMax: double;
  xHitOffCnt,                           // number of offlimit hits
    xTotOffCnt: integer;                // total number of offlimits
  xHit_TotFactor: double;               // factor: xHitOffCnt / xTotOffCnt
  xCauseOfflimits: sOfflimitDataT;      // cause offlimits

begin
  with FQry do begin
    First;

    i := 0;
    Causes := nil;
    while not eof do begin
      inc(i);
      SetLength(Causes, i);

      xTotOffCnt := 0;
      xHitOffCnt := 0;
      xCauseOfflimits := [];

      j := i - 1;
      fillchar(Causes[j], sizeof(rOffCausesT), 0);

      Causes[j].Name := FieldByName('c_offcause_name').asstring;
      Causes[j].cntNumber := FieldByName('c_counter').asinteger;

      // for xData := otYB to otCYTot do begin
      //otOP
      for xData := otYB to otOP do begin
        xFieldname := self.Fields.FieldnameByType(xData);
        inc(Causes[j].offcntNumber, FieldByName(xFieldname + '_offcnt').asinteger);

        if FieldByName(xFieldname + '_offcnt').asinteger > 0 then
          include(xCauseOfflimits, xData);
      end;                              //for xData := otYB to otCYTot do begin

      //for xData := otYB to otCYTot do begin
      //otOP
      for xData := otYB to otOP do begin
        xFieldname := self.Fields.FieldnameByType(xData);

        //2a. pattern analysis
        if xData <> otOP then begin
          if not FieldByName(xFieldname + '_min').IsNull then begin
            xMin := FieldByName(xFieldname + '_min').asfloat - cEpsilon;
            xMax := FieldByName(xFieldname + '_max').asfloat + cEpsilon;

            if xMax < xMin then ChangeVars(xMax, xMin, sizeof(xMin));

            if not aOfflimits.IsNull[oiSpindle, xData] then
              if abs(aOfflimits.Value[oiMean, xData]) > cEpsilon then begin
                xDev := (aOfflimits.Value[oiSpindle, xData] / aOfflimits.Value[oiMean, xData] - 1) * 100;
                if (xDev <= xMax) and (xDev >= xMin) then
                  incr(Causes[j].PatternHitrate, 1 / cMaxNumberOfHits);
              end;                      //if abs(mOfflimits.Value[oiMean,xData]) > cEpsilon then begin
          end;                          //if not FieldByName(xFieldname +'_min').IsNull then
        end;                            //if xData <> otOP then begin

        //2b. occurrence analysis
        if (cOfflimitTypes * aOfflimits.LimitFaults[xData] <> []) then begin //is offlimit
          if xData in xCauseOfflimits then inc(xHitOffCnt);
          inc(xTotOffCnt);

          if (Causes[j].offcntNumber > 0) then
            incr(Causes[j].OccurrenceHitrate, FieldByName(xFieldname + '_offcnt').asinteger / Causes[j].offcntNumber);
        end;                            //if (cOfflimitTypes * aOfflimits.LimitFaults[xData] <> []) then begin
      end;                              //for xData := otYB to otCYTot do

      //2c. calc weight: occurrence � pattern
      if xTotOffCnt > 0 then
        xHit_TotFactor := xHitOffCnt / xTotOffCnt
      else
        xHit_TotFactor := 0;
{
      try
        xHit_TotFactor := xHitOffCnt /xTotOffCnt;
      except
        xHit_TotFactor := 0;
      end; //try..except
{}

      Causes[j].TotalHitrate := (FOfflimitPattern.Weight * Causes[j].PatternHitrate)
        + ((100 - FOfflimitPattern.Weight) * Causes[j].OccurrenceHitrate) * xHit_TotFactor;

      Next;
    end;                                //while not eof do

    SortCauses;
  end;                                  //with FQry do begin

  result := Causes;
end;                                    //function TOfflimitCauses.GetCauses
//-----------------------------------------------------------------------------
procedure TOfflimitCauses.ReadOfflimitCauses;
var
  xData: eOfflimitDataT;
  xFieldname: string;

begin
  with FQry do begin
    Active := false;
    SQL.Clear;
    SQL.Add('SELECT');

    for xData := otYB to otCYTot do begin
      xFieldname := FFields.FieldnameByType(xData);
      SQL.Add(format('%s_dev *(100 -%d) /100 AS %s_min,', [xFieldname, FOfflimitPattern.BandWidth, xFieldname]));
      SQL.Add(format('%s_dev *(100 +%d) /100 AS %s_max,', [xFieldname, FOfflimitPattern.BandWidth, xFieldname]));
      SQL.Add(format('%s_offcnt,', [xFieldname]));
    end;                                //for xData := otYB to otCYTot do

    SQL.Add('c_op_offcnt, c_offcause_name, c_counter');
    SQL.Add('FROM t_offlimit_cause');
    Open;
  end;                                  //with FQry do begin
end;                                    //procedure TOfflimitCauses.ReadOfflimitCauses
//-----------------------------------------------------------------------------
procedure TOfflimitCauses.SetOfflimitPattern(Value: rOfflimitPatternT);
begin
  FOfflimitPattern := Value;
  ReadOfflimitCauses;
end;                                    //procedure TOfflimitCauses.SetOfflimitPattern
//-----------------------------------------------------------------------------
procedure TOfflimitCauses.SortCauses;

  //-----------
  function GetValue(aPos: integer): double;
  begin
    result := Causes[aPos].TotalHitrate;
  end;                                  //function GetValue

  //-----------

  procedure DoSort(iLo, iHi: integer);

  var
    xLo, xHi: integer;
    xMid: double;
    xTmp: rOffCausesT;

  begin
    xLo := iLo;
    xHi := iHi;
    xMid := GetValue((xLo + xHi) div 2);

    repeat
      while GetValue(xLo) > xMid do inc(xLo);
      while GetValue(xHi) < xMid do dec(xHi);

      if xLo <= xHi then begin
        move(Causes[xLo], xTmp, sizeof(rOffCausesT));
        move(Causes[xHi], Causes[xLo], sizeof(rOffCausesT));
        move(xTmp, Causes[xHi], sizeof(rOffCausesT));
        inc(xLo);
        dec(xHi);
      end;                              //if xLo <= xHi then
    until xLo > xHi;

    if xHi > iLo then DoSort(iLo, xHi);
    if xLo < iHi then DoSort(xLo, iHi);
  end;                                  //procedure DoSort

  //-----------

begin
  if high(Causes) > 0 then DoSort(low(Causes), high(Causes));
end;                                    //procedure TOfflimitCauses.SortCauses

end.                                    //u_global

