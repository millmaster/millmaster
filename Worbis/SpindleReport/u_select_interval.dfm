inherited SelectIntervalDlg: TSelectIntervalDlg
  Left = 434
  Top = 215
  Width = 410
  Height = 322
  BorderIcons = [biSystemMenu]
  Caption = '(60)Zeitraum auswaehlen'
  Constraints.MaxWidth = 410
  Constraints.MinWidth = 410
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 402
    Height = 265
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TmmPanel
      Left = 0
      Top = 0
      Width = 402
      Height = 265
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object lbIntervals: TNWListBox
        Left = 0
        Top = 20
        Width = 402
        Height = 245
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        ItemHeight = 15
        MultiSelect = True
        ParentFont = False
        Style = lbOwnerDrawFixed
        TabOrder = 0
        OnDblClick = acOKExecute
        OnDrawItem = lbIntervalsDrawItem
      end
      object Panel2: TmmPanel
        Left = 0
        Top = 0
        Width = 402
        Height = 20
        Align = alTop
        BevelOuter = bvNone
        BorderStyle = bsSingle
        TabOrder = 1
        object laFrom: TmmLabel
          Left = 5
          Top = 2
          Width = 36
          Height = 13
          Caption = '(20)von'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laTo: TmmLabel
          Left = 155
          Top = 2
          Width = 31
          Height = 13
          Caption = '(20)bis'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laDuration: TmmLabel
          Left = 305
          Top = 2
          Width = 87
          Height = 13
          Caption = '(20)Dauer (hh:mm)'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 265
    Width = 402
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 162
      Top = 3
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 242
      Top = 3
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 322
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 10
    Top = 58
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 40
    Top = 56
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0))
  end
  object quInterval: TmmADODataSet
    Connection = dmSpindleReport.conDefault
    Parameters = <>
    Left = 82
    Top = 58
  end
end
