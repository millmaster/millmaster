(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_StatisticPie.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Show a pie
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 23.03.00 |      | SDo | file created
| 04.04.2002      | Wss | WindowPos nun DesktopCenter
| 04.04.2002      | Wss | - WindowPos nun ScreenCenter
                          - SendMessage durch PostMessage ausgetauscht
|=============================================================================*)

unit u_StatisticPie;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEFORM, TeEngine, Series, ExtCtrls, TeeProcs, Chart, mmChart, IvDictio,
  IvMulti, IvEMulti, mmTranslator, StdCtrls, Buttons, mmBitBtn, mmPanel,
  ActnList, mmActionList;

type
  TfStatisticPie = class(TmmForm)
    mmChart1: TmmChart;
    PieSeries : TPieSeries;
    mmTranslator1: TmmTranslator;
    mmPanel1: TmmPanel;
    mmBitBtn1: TmmBitBtn;
    mmActionList1: TmmActionList;
    acClose: TAction;
    procedure acCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mmPanel1CanResize(Sender: TObject; var NewWidth,
      NewHeight: Integer; var Resize: Boolean);
  private
    { Private declarations }
    mButtonOffset : Word;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
  end;

var
  fStatisticPie: TfStatisticPie;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 u_offlimit_report;
{$R *.DFM}

//------------------------------------------------------------------------------
constructor TfStatisticPie.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mButtonOffset := mmPanel1.width -  mmBitBtn1.Left - mmBitBtn1.Width;
end;
//------------------------------------------------------------------------------
procedure TfStatisticPie.acCloseExecute(Sender: TObject);
//var xAction: TCloseAction ;
begin
  Close;
end;

procedure TfStatisticPie.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  PostMessage((Owner as TWinControl).Handle, cPieFormClose, 0, 0);
end;


procedure TfStatisticPie.mmPanel1CanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  mmBitBtn1.left := NewWidth - mmBitBtn1.width - mButtonOffset;
end;

end.
