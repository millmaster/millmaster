(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_lib.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Allgemeine Funktionen und Prozeduren
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 05.04.99 | 0.00 | PW  | File created
| 21.05.99 | 0.01 | PW  | SFI and SFI/D added
| 15.09.99 | 0.02 | PW  |
| 10.02.00 | 1.00 | Mg  | GetPeriodByIntervals : where in Query inserted
| 23.02.00 | 1.01 | Mg  | Imperfections in GetEspParameter enabled
|  5.05.00 | 1.02 | PW  | func. ReadParam fuer neuen Dialog in u_settingdialog.pas
| 15.06.00 | 1.03 | Mg  | GetLengthUnitFactor : check fuer div 0 eingebaut
| 03.07.00 | 1.04 | SDO | In Func. ReadParam Defaultfarben eingebaut
| 03.07.00 | 1.04 | SDO | Definition der Farben befindet sich in mmGraphGlobal.pas
| 23.08.01 |      | PW  | PrintOptionsDlg removed, replaced by \common_units\u_common_lib.GetPrintOptions
| 18.02.04 |      | Wss | RessourceString rsHourShort hinzugefügt
| 18.02.08 | 1.10 | Nue | Einbau VCV MM5.04
|=========================================================================================*)

{$I symbols.inc}

unit u_lib;
interface
uses
  u_common_global, u_global, mmLengthUnit, mmGraph, memoini, mmGridpos, mmGraphGlobal,
  Controls, Classes, Graphics, SysUtils, Forms, Dialogs, Buttons, DB, MMUGlobal,
  LoepfeGlobal, mmADODataSet;
//------------------------------------------------------------------------------
resourcestring
  c1000KmStr = '(20)1000KmUnit'; //ivlm
  c100KmStr = '(20)100KmUnit'; //ivlm
  c1000KyrdStr = '(20)1000KyrdUnit'; //ivlm
  c100KyrdStr = '(20)100KyrdUnit'; //ivlm
  c1KgStr = '(20)1kgUnit'; //ivlm
  c1LbsStr = '(20)1LbsUnit'; //ivlm
  cAbsoluteStr = '(20)AbsoluteUnit'; //ivlm
  cConeStr = '(20)ConeUnit'; //ivlm
  cBobineStr = '(20)BobinUnit'; //ivlm
  rsHourShort = '(5)Std'; // ivlm
//------------------------------------------------------------------------------
function GetEspParameter(pID: integer): rEspParameterT;
function GetEspParameterName(pID: integer): string;
function GetSubStr(aStr: string; aDelimiter: char; aPos: byte): string;
function GetTimeDiffStr(aFrom, aTo: tdatetime): string;
 // function SelectPeriod(var aPeriod: rPeriodInfoT; aShiftCalID: integer): boolean;
function SelectInterval(aOwner: TWinControl; var aPeriod: rPeriodInfoT): boolean;
function GetLengthUnitFactor(aBaseUnits: rBaseLengthUnitsT; aLengthUnit: TLenBase): double;
function GetLengthUnitStr(aLengthUnit: TLenBase): string;
function StandardSelect(aCaption, aDisplayfield, aIDField, aTablename: string): rListSelResultT;
procedure GetPeriodByIntervals(var aFrom, aTo: tdatetime; aIntervals: integer);
function GetGraphParamList(aGraph: TmmGraph): rEspParameterArrayT;

function GetDisplayLabel(aDisplayLabel: string): string;
function GetDisplayFormatStr(aField: TField): string;
// function ReadParam(aDataset: TDataSet; aParaList: rEspParameterArrayT; aBarStyle: eBarStyleT): TParameterList;
function ReadParam(aDataset: TDataSet; aParaList: rEspParameterArrayT; aGraphic: TmmGraph): TParameterList;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 //Registry,
  mmLib,
 //u_select_period,
  u_main, u_common_lib,
  u_select_interval, u_standard_select, u_set_printoptions;

function GetEspParameter(pID: integer): rEspParameterT;
const
  xArray: array[1..cMaxEspParameter] of rEspParameterT =
    ((ID: 1; FilterID: 0; FieldName: 'C_LEN'), //iv lm
    (ID: 2; FilterID: 0; FieldName: 'C_WEI'), //iv lm
    (ID: 3; FilterID: 0; FieldName: 'C_BOB'), //iv lm
    (ID: 4; FilterID: 0; FieldName: 'C_CONES'), //iv lm
    (ID: 5; FilterID: - 1; FieldName: 'CALC_RED'), //iv lm
    (ID: 6; FilterID: - 1; FieldName: 'C_TRED'), //iv lm
    (ID: 7; FilterID: 0; FieldName: 'C_TYELL'), //iv lm
    (ID: 8; FilterID: 0; FieldName: 'CALC_MANST'), //iv lm
    (ID: 9; FilterID: 0; FieldName: 'C_TMANST'), //iv lm
    (ID: 10; FilterID: - 1; FieldName: 'C_RTSPD'), //iv lm
    (ID: 11; FilterID: - 1; FieldName: 'C_OTSPD'), //iv lm
    (ID: 12; FilterID: - 1; FieldName: 'C_WTSPD'), //iv lm
    (ID: 13; FilterID: 0; FieldName: 'CALC_SP'), //iv lm
    (ID: 14; FilterID: 0; FieldName: 'CALC_RSP'), //iv lm
    (ID: 15; FilterID: 0; FieldName: 'CALC_YB'), //iv lm
    (ID: 16; FilterID: - 1; FieldName: 'C_OTPRODGRP'), //iv lm
    (ID: 17; FilterID: - 1; FieldName: 'C_WTPRODGRP'), //iv lm
    (ID: 18; FilterID: 0; FieldName: 'C_LST'), //iv lm
    (ID: 19; FilterID: - 1; FieldName: 'C_LSTPROD'), //iv lm
    (ID: 20; FilterID: - 1; FieldName: 'C_LSTBREAK'), //iv lm
    (ID: 21; FilterID: - 1; FieldName: 'C_LSTMAT'), //iv lm
    (ID: 22; FilterID: - 1; FieldName: 'C_LSTREV'), //iv lm
    (ID: 23; FilterID: - 1; FieldName: 'C_LSTCLEAN'), //iv lm
    (ID: 24; FilterID: - 1; FieldName: 'C_LSTDEF'), //iv lm
    (ID: 25; FilterID: 0; FieldName: 'CALC_CS'), //iv lm
    (ID: 26; FilterID: 0; FieldName: 'CALC_CL'), //iv lm
    (ID: 27; FilterID: 0; FieldName: 'CALC_CT'), //iv lm
    (ID: 28; FilterID: 0; FieldName: 'CALC_CN'), //iv lm
    (ID: 29; FilterID: 0; FieldName: 'CALC_CSP'), //iv lm
    (ID: 30; FilterID: 0; FieldName: 'CALC_CCL'), //iv lm
    (ID: 31; FilterID: 0; FieldName: 'CALC_UCLS'), //iv lm
    (ID: 32; FilterID: 0; FieldName: 'CALC_UCLL'), //iv lm
    (ID: 33; FilterID: 0; FieldName: 'CALC_UCLT'), //iv lm
    (ID: 34; FilterID: 0; FieldName: 'CALC_COFFCNT'), //iv lm
    (ID: 35; FilterID: 0; FieldName: 'C_LCKOFFCNT'), //iv lm
    (ID: 36; FilterID: 0; FieldName: 'CALC_CBU'), //iv lm
    (ID: 37; FilterID: 0; FieldName: 'CALC_CDBU'), //iv lm
    (ID: 38; FilterID: 0; FieldName: 'CALC_CSYS'), //iv lm
    (ID: 39; FilterID: 0; FieldName: 'C_LCKSYS'), //iv lm
    (ID: 40; FilterID: 0; FieldName: 'CALC_CUPY'), //iv lm
    (ID: 41; FilterID: 0; FieldName: 'CALC_CYTOT'), //iv lm
    (ID: 42; FilterID: 0; FieldName: 'CALC_CSIRO'), //iv lm
    (ID: 43; FilterID: 0; FieldName: 'C_LCKSIRO'), //iv lm
    (ID: 44; FilterID: - 1; FieldName: 'C_CLASSCUT_ID'), //iv lm
    (ID: 45; FilterID: - 1; FieldName: 'C_CLASSUNCUT_ID'), //iv lm
    (ID: 46; FilterID: - 1; FieldName: 'C_SPLICECUT_ID'), //iv lm
    (ID: 47; FilterID: - 1; FieldName: 'C_SPLICEUNCUT_ID'), //iv lm
    (ID: 48; FilterID: - 1; FieldName: 'C_SIROCUTUNCUT_ID'), //iv lm
    (ID: 49; FilterID: 0; FieldName: 'CALC_INEPS'), //iv lm
    (ID: 50; FilterID: 0; FieldName: 'CALC_ITHICK'), //iv lm
    (ID: 51; FilterID: 0; FieldName: 'CALC_ITHIN'), //iv lm
    (ID: 52; FilterID: 0; FieldName: 'CALC_ISMALL'), //iv lm
    (ID: 53; FilterID: 0; FieldName: 'CALC_I2_4'), //iv lm
    (ID: 54; FilterID: 0; FieldName: 'CALC_I4_8'), //iv lm
    (ID: 55; FilterID: 0; FieldName: 'CALC_I8_20'), //iv lm
    (ID: 56; FilterID: 0; FieldName: 'CALC_I20_70'), //iv lm
    (ID: 57; FilterID: 0; FieldName: 'CALC_MEFF'), //iv lm
    (ID: 58; FilterID: 0; FieldName: 'CALC_PEFF'), //iv lm
    (ID: 59; FilterID: 0; FieldName: 'CALC_PCSP'), //iv lm
    (ID: 60; FilterID: 0; FieldName: 'CALC_PRSP'), //iv lm
    (ID: 61; FilterID: 0; FieldName: 'CALC_LSTTOT'), //iv lm
    (ID: 62; FilterID: 0; FieldName: 'CALC_SFI'), //iv lm
    (ID: 63; FilterID: - 1; FieldName: 'CALC_LCKCL'), //iv lm
    (ID: 64; FilterID: - 1; FieldName: 'CALC_LCKSIROCL'), //iv lm
    (ID: 65; FilterID: - 1; FieldName: 'CALC_CSIROCL'), //iv lm
    (ID: 66; FilterID: - 1; FieldName: 'CALC_LCKSFI'), //iv lm
    (ID: 67; FilterID: - 1; FieldName: 'CALC_LCKOFFCNT'), //iv lm
    (ID: 68; FilterID: 0; FieldName: 'CALC_CVCV'), //iv lm
    (ID: 69; FilterID: - 1; FieldName: 'CALC_LCKVCV') //iv lm
    );

begin
  Result.ID := xArray[pID].ID;
  Result.FilterID := xArray[pID].FilterID;
  Result.FieldName := xArray[pID].FieldName;
  Result.LowerLimit := 0;
  Result.UpperLimit := 0;
  Result.Statistik := [];
  Result.ShowGraph := True;
  Result.ChartType := ctBar;
  Result.Color := clBlue;
  Result.YScalingType := ysNone;
end; //function GetEspParameter
//-----------------------------------------------------------------------------
function GetEspParameterName(pID: integer): string;
var
  xParameter: rEspParameterT;
begin
  xParameter := GetEspParameter(pID);
  Result := LTrans(xParameter.FieldName);
end; //function GetEspParameterName
//-----------------------------------------------------------------------------
function GetSubStr(aStr: string; aDelimiter: char; aPos: byte): string;
var
  xMaxC,
    i, j: integer;
  sArray: array of string;

begin
  Result := '';
  xMaxC := 0;

  for i := 1 to length(aStr) do
    if aStr[i] = aDelimiter then inc(xMaxC);

  if aPos > xMaxC then exit;
  SetLength(sArray, xMaxC);

  i := pos(aDelimiter, aStr);
  j := 0;

  while (i > 0) and (j < xMaxC) do begin
    sArray[j] := copy(aStr, 1, i - 1);
    delete(aStr, 1, i);
    i := pos(aDelimiter, aStr);
    inc(j);
  end; //while (i > 0) and (j < (xMaxC -1))  do

  Result := trim(sArray[aPos - 1]);

  sArray := nil;
end; //function GetSubStr
//-----------------------------------------------------------------------------
function GetTimeDiffStr(aFrom, aTo: tdatetime): string;
var
  xDiff: tdatetime;
  xVZ: string;
  xDays,
    xHrs,
    xMins: integer;

begin
  xDiff := aTo - aFrom;

  if xDiff < 0 then
    xVZ := '-'
  else
    xVZ := '';
  xDiff := abs(xDiff);

  xDays := trunc(xDiff);
  xMins := round(frac(xDiff) * cHoursPerDay * cMinsPerHour);

  xHrs := xMins div cMinsPerHour;
  xMins := xMins - (xHrs * cMinsPerHour);

  Result := Format('%s%dd %sh:%sm', [xVZ, xDays, lz(xHrs, 2), lz(xMins, 2)]);
end; //function GetTimeDiffStr
//-----------------------------------------------------------------------------
(*
function SelectPeriod(var aPeriod: rPeriodInfoT; aShiftCalID: integer): boolean;
begin
 with TSelectPeriodDlg.Create(Application) do
 begin
  Init(aPeriod,aShiftCalID);
  ShowModal;
  result := ModalResult = mrOK;
  if result then
  begin
   aPeriod.PBegin := dtFrom.Date;
   aPeriod.pEnd := dtTo.Date;
  end; //if ModalResult = mrOK then
  Free;
 end; //with TSelectPeriodDlg.Create(Application) do
end; //function SelectPeriod
*)
//-----------------------------------------------------------------------------
function SelectInterval(aOwner: TWinControl; var aPeriod: rPeriodInfoT): boolean;
begin
  with TSelectIntervalDlg.Create(aOwner) do begin
    SetPeriod(aPeriod);
    ShowModal;
    Result := ModalResult = mrOK;
    if Result then aPeriod := GetPeriod;
    Free;
  end; //with TSelectIntervalDlg.Create(Application) do
end; //function SelectInterval
//-----------------------------------------------------------------------------
function GetLengthUnitFactor(aBaseUnits: rBaseLengthUnitsT; aLengthUnit: TLenBase): double;
begin
  try
  // Check of zero
    case aLengthUnit of
      lb1000Km, lb100Km, lb1000000y, lb100000y: begin
          if aBaseUnits.Length = 0 then begin
            Result := 0;
            Exit;
          end;
        end;
  { Mg
  lu1Kg,lu1Lbs : begin
      if aBaseUnits.Weight = 0 then begin
        result := 0;
        Exit;
      end;
    end;
  luCone : begin
      if aBaseUnits.Cones = 0 then begin
        result := 0;
        Exit;
      end;
    end;
  luBobine : begin
      if aBaseUnits.Bobines = 0 then begin
        result := 0;
        Exit;
      end;
    end;
  }
    end;
    case aLengthUnit of
      lb1000Km: Result := 1000 / aBaseUnits.Length;
      lb100Km: Result := 100 / aBaseUnits.Length;
      lb1000000y: Result := 914.4 / aBaseUnits.Length;
      lb100000y: Result := 91.44 / aBaseUnits.Length;
   //lu1Kg: result := 1 /aBaseUnits.Weight;
   //lu1Lbs: result := 0.4536 /aBaseUnits.Weight;
      lbAbsolute: Result := 1;
   //luCone: result := 1 /aBaseUnits.Cones;
   //luBobine: result := 1 /aBaseUnits.Bobines;
    else
      Result := 0;
    end; //case aLengthUnit of
  except
    Result := 0;
  end; //try..except
end; //GetLengthUnitFactor
//-----------------------------------------------------------------------------
function GetLengthUnitStr(aLengthUnit: TLenBase): string;
begin
  case aLengthUnit of
    lb1000Km: Result := c1000KmStr;
    lb100Km: Result := c100KmStr;
    lb1000000y: Result := c1000KyrdStr;
    lb100000y: Result := c100KyrdStr;
  //lu1Kg: result := c1KgStr;
  //lu1Lbs: result := c1LbsStr;
    lbAbsolute: Result := cAbsoluteStr;
  //luCone: result := cConeStr;
  //luBobine: result := cBobineStr;
  end; //case aLengthUnit of

//wss: not needed because of ressourcestring result := LTrans(result);
end; //function GetLengthUnitStr
//-----------------------------------------------------------------------------
function StandardSelect(aCaption, aDisplayfield, aIDField, aTablename: string): rListSelResultT;
var
  xOK: boolean;

begin
  Result.ID := -1;
  Result.Text := '';

  with TStandardSelectDlg.Create(Application) do begin
    Init(aCaption, aDisplayfield, aIDField, aTablename);
    ShowModal;
    xOK := ModalResult = mrOK;
    if xOK then Result := GetSelection;
    Free;
  end; //with TStandardSelectDlg.Create(Application) do
end; //function StandardSelect
//-----------------------------------------------------------------------------
procedure GetPeriodByIntervals(var aFrom, aTo: tdatetime; aIntervals: integer);
var
  xCounter: integer;
begin
  with TmmADODataSet.Create(Nil) do
  try
    ConnectionString := GetDefaultConnectionString;
    CommandText := 'SELECT * FROM t_interval ';
    CommandText := CommandText + ' where c_interval_end < getDate() ';
    CommandText := CommandText + ' ORDER BY c_interval_end DESC';
    Open;
    //there must be minimum one interval in the database
    if FindFirst then begin
      xCounter := 1;
      aFrom := fieldbyname('c_interval_start').asdatetime;
      aTo   := fieldbyname('c_interval_end').asdatetime;

      while (not Eof) and (xCounter <= aIntervals) do begin
        inc(xCounter);
        aFrom := fieldbyname('c_interval_start').asdatetime;
        Next;
      end; //while (not Eof) and (xCounter < aIntervals) do
    end;
  finally
    Free;
  end; //try..finally
end; //procedure GetPeriodByIntervals
//-----------------------------------------------------------------------------
function GetGraphParamList(aGraph: TmmGraph): rEspParameterArrayT;
var
  i: integer;

begin
  with aGraph do begin
    Result := nil;
    if Data.NumSeries = 0 then exit;

    SetLength(Result, Data.NumSeries);

    for i := 1 to Data.NumSeries do begin
      Result[i - 1] := GetEspParameter(Series.ID[i]);
      Result[i - 1].FieldName := Series.FieldName[i];
      Result[i - 1].Color := Series.Color[i];
      Result[i - 1].YScalingType := Series.YScalingType[i];
      Result[i - 1].Statistik := Series.ShowStat[i];
      Result[i - 1].ShowGraph := Series.Visible[i];
      Result[i - 1].ChartType := eChartTypeT(ord(Series.YAxis[i]));
      Result[i - 1].LowerLimit := Series.LowerLimit[i];
      Result[i - 1].UpperLimit := Series.UpperLimit[i];
    end; //for i := 1 to Data.NumSeries do
  end; //with aGraph do
end; //function GetGraphParamList
//-----------------------------------------------------------------------------
function GetDisplayLabel(aDisplayLabel: string): string;
const
  cWordWrapChar = '~'; //#126 do not modify

var
  i: integer;

begin
  i := pos(cWordWrapChar, aDisplayLabel);
  while i > 0 do begin
    aDisplayLabel[i] := #32;
    i := pos(cWordWrapChar, aDisplayLabel);
  end; //while i > 0 do

  i := pos('- ', aDisplayLabel);
  while i > 0 do begin
    if i > 0 then system.delete(aDisplayLabel, i, 2);
    i := pos('- ', aDisplayLabel);
  end; //while i > 0 do

  Result := aDisplayLabel;
end; //function GetDisplayLabel
//-----------------------------------------------------------------------------
function GetDisplayFormatStr(aField: TField): string;
begin
  case aField.DataType of
    ftFloat: begin
        Result := TNumericField(aField).DisplayFormat;
        if Result = '' then
          Result := ',0.0';
      end;
    ftSmallint, ftInteger, ftWord, ftAutoInc, ftLargeInt:
      Result := TNumericField(aField).DisplayFormat;
    ftDate, ftTime, ftDateTime:
      Result := TDateTimeField(aField).DisplayFormat;
  else
    Result := '';
  end; //case aField.DataType of
end; //function GetDisplayFormatStr
//-----------------------------------------------------------------------------
function ReadParam(aDataset: TDataSet; aParaList: rEspParameterArrayT; aGraphic: TmmGraph): TParameterList;
var
  i, k, x, xres,
    xParamID: integer;
  xParm: pParamRec;
// xPara    : TParameterList;
begin
  Result := TParameterList.Create;
  Result.Clear;
// result.BarStyle := aBarStyle;
  Result.BarStyle := aGraphic.Bars.Style;

  with Result do begin
    LeftYAxis.Autoscale := aGraphic.LeftYAxis.AutoScaling;
    LeftYAxis.Min := Round(aGraphic.LeftYAxis.DefaultMinY);
    LeftYAxis.Max := Round(aGraphic.LeftYAxis.DefaultMaxY);
  end;

  with Result do begin
    RightYAxis.Autoscale := aGraphic.RightYAxis.AutoScaling;
    RightYAxis.Min := Round(aGraphic.RightYAxis.DefaultMinY);
    RightYAxis.Max := Round(aGraphic.RightYAxis.DefaultMaxY);
  end;

  aDataset.DisableControls;

  try
    xParamID := 0;
    for i := 0 to aDataset.FieldCount - 1 do begin
      if aDataset.Fields[i].Tag > cNonVisible then begin
        new(xParm);
        FillChar(xParm^.ColumnParam, SizeOf(xParm^.ColumnParam), 0);
        FillChar(xParm^.GraphicParam, SizeOf(xParm^.GraphicParam), 0);
        with xParm^.ColumnParam do begin
          ID := xParamID;
          FieldName := aDataset.Fields[i].FieldName;
          DisplayLabel := GetDisplayLabel(aDataset.Fields[i].DisplayLabel);
          DataType := aDataset.Fields[i].DataType;
          Index := aDataset.Fields[i].Index;
          DisplayWidth := aDataset.Fields[i].DisplayWidth;
          Alignment := aDataset.Fields[i].Alignment;
          DisplayFormat := GetDisplayFormatStr(aDataset.Fields[i]);
          Tag := aDataset.Fields.Fields[i].Tag;
          Visible := aDataset.Fields[i].Visible and (aDataset.Fields[i].Tag <> cFieldHideID);
        end;

        with xParm^.GraphicParam do begin
          ID := i;
          FieldName := xParm^.ColumnParam.FieldName;
          FilterID := -2;
          Color := clBlue;

          x := -1;
//    xres:= -1;
    // Defaultfarbe ermitteln
          repeat
            inc(x);
            xres := StrComp(PChar(Uppercase(FieldName)),
              PChar(Uppercase(cDefaultColor[x].Fieldname)));
          until (xres = 0) or (x >= High(cDefaultColor));

          Color := cDefaultColor[x].Color;

          ChartType := ctBar;
          LowerLimit := 0;
          UpperLimit := 0;
          YScalingType := ysNone;
          Statistik := [];
          ShowGraph := False;

          for k := 0 to high(aParaList) do
            if uppercase(aParaList[k].FieldName) = uppercase(FieldName) then begin
              Color := aParaList[k].Color;
              YScalingType := aParaList[k].YScalingType;
              Statistik := aParaList[k].Statistik;
              ShowGraph := True;
              ChartType := aParaList[k].ChartType;
              LowerLimit := aParaList[k].LowerLimit;
              UpperLimit := aParaList[k].UpperLimit;
            end; //for i := 1 to Data.NumSeries do
        end;

        inc(xParamID);
        Result.Add(xParm);
      end; //if aDataset.Fields[i].Tag > cNonVisible then
    end; //for i := 0 to xDS.FieldCount -1 do
  finally
    aDataset.EnableControls;
  end;
end; //function ReadParam

end. //u_lib.pas

