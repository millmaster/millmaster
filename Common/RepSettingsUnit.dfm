object RepSettings: TRepSettings
  Left = 0
  Top = 0
  Width = 858
  Height = 494
  TabOrder = 0
  object pnBaseTop: TmmPanel
    Left = 0
    Top = 0
    Width = 858
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object cobSettings: TmmComboBox
      Left = 0
      Top = 16
      Width = 858
      Height = 21
      Style = csOwnerDrawFixed
      Anchors = [akLeft, akTop, akRight]
      Color = clWindow
      ItemHeight = 15
      TabOrder = 0
      Visible = True
      OnChange = cobSettingsChange
      OnDrawItem = cobSettingsDrawItem
      AutoLabel.LabelPosition = lpLeft
      Edit = False
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object mmPanel2: TmmPanel
      Left = 0
      Top = 0
      Width = 858
      Height = 15
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object laTimeRange: TmmLabel
        Left = 550
        Top = 0
        Width = 308
        Height = 15
        Align = alClient
        AutoSize = False
        Caption = '(30)Zeitraum'
        Layout = tlBottom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laLot: TmmLabel
        Left = 0
        Top = 0
        Width = 350
        Height = 15
        Align = alLeft
        AutoSize = False
        Caption = '(*)Partie'
        Layout = tlBottom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laTemplate: TmmLabel
        Left = 350
        Top = 0
        Width = 100
        Height = 15
        Align = alLeft
        AutoSize = False
        Caption = '(*)Vorlage'
        Layout = tlBottom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laMachine: TmmLabel
        Left = 450
        Top = 0
        Width = 100
        Height = 15
        Align = alLeft
        AutoSize = False
        Caption = '(*)Maschine'
        Layout = tlBottom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object pnSettings: TmmPanel
    Left = 0
    Top = 41
    Width = 858
    Height = 453
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object mmPanel1: TmmPanel
      Left = 0
      Top = 0
      Width = 401
      Height = 453
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object pnBottom: TmmPanel
        Left = 0
        Top = 304
        Width = 401
        Height = 149
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object mFaultCluster: TFaultClusterEditBox
          Left = 0
          Top = 0
          Width = 262
          Height = 104
          Caption = '(11)Fehlerschwarm'
          TabOrder = 0
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
        object mSFI: TSFIEditBox
          Left = 264
          Top = 0
          Width = 136
          Height = 74
          Caption = '(11)SFI/D'
          TabOrder = 1
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
        object mVCV: TVCVEditBox
          Left = 264
          Top = 73
          Width = 136
          Height = 74
          Caption = '(11)VCV'
          TabOrder = 2
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
      end
      object spQMatrix: TSizingPanel
        Left = 0
        Top = 0
        Width = 401
        Height = 304
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 2
        Control = mQMatrix
        object mSpliceQMatrix: TQualityMatrix
          Left = 2
          Top = 2
          Width = 395
          Height = 304
          Color = clWhite
          LastCutColor = clLime
          LastCutField = 0
          LastCutMode = lcNone
          MatrixSubType = mstNone
          MatrixType = mtSplice
          ActiveColor = clFuchsia
          ActiveVisible = True
          Anchors = [akLeft, akTop, akRight]
          ChannelColor = clRed
          ChannelStyle = psSolid
          ChannelVisible = False
          ClusterColor = clPurple
          ClusterStyle = psSolid
          ClusterVisible = False
          CutsColor = clRed
          DefectsColor = clBlack
          DisableMessage = 'Quality Matrix disabled'
          DisplayMode = dmValues
          DotsColor = clBlack
          Enabled = True
          InactiveColor = 9502719
          MatrixMode = mmDisplayData
          SpliceColor = clBlue
          SpliceStyle = psSolid
          SpliceVisible = True
          SubFieldX = 0
          SubFieldY = 0
          ZeroLimit = 0.01
        end
        object mQMatrix: TQualityMatrix
          Left = 2
          Top = 2
          Width = 389
          Height = 300
          Color = clWhite
          LastCutColor = clLime
          LastCutField = 0
          LastCutMode = lcNone
          MatrixSubType = mstNone
          MatrixType = mtShortLongThin
          ActiveColor = clAqua
          ActiveVisible = True
          Anchors = [akLeft, akTop, akRight]
          ChannelColor = clRed
          ChannelStyle = psSolid
          ChannelVisible = True
          ClusterColor = clPurple
          ClusterStyle = psSolid
          ClusterVisible = True
          CutsColor = clRed
          DefectsColor = clBlack
          DisableMessage = 'Quality Matrix disabled'
          DisplayMode = dmValues
          DotsColor = clBlack
          Enabled = True
          InactiveColor = 9502719
          MatrixMode = mmDisplayData
          SpliceColor = clBlue
          SpliceStyle = psSolid
          SpliceVisible = False
          SubFieldX = 0
          SubFieldY = 0
          ZeroLimit = 0.01
        end
        object mCurveSelection: TmmPanel
          Left = 264
          Top = 8
          Width = 135
          Height = 63
          BevelOuter = bvNone
          TabOrder = 2
          object mcbSpliceCurve: TmmCheckBox
            Left = 6
            Top = 38
            Width = 123
            Height = 17
            Caption = '(13)Spleiss Kurve'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            Visible = True
            OnClick = mcbSpliceCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mcbChannelCurve: TmmCheckBox
            Left = 6
            Top = 6
            Width = 123
            Height = 17
            Caption = '(13)Kanal Kurve'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 0
            Visible = True
            OnClick = mcbChannelCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mcbClusterCurve: TmmCheckBox
            Left = 6
            Top = 22
            Width = 123
            Height = 17
            Caption = '(13)Fehlerschwarm'
            Checked = True
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            State = cbChecked
            TabOrder = 1
            Visible = True
            OnClick = mcbClusterCurveClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
    end
    object mmPanel3: TmmPanel
      Left = 401
      Top = 0
      Width = 457
      Height = 453
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object mmPanel6: TmmPanel
        Left = 223
        Top = 0
        Width = 234
        Height = 453
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object SizingPanel1: TSizingPanel
          Left = 0
          Top = 0
          Width = 234
          Height = 309
          Align = alClient
          BevelOuter = bvNone
          Control = mFMatrix
          object mFMatrix: TQualityMatrix
            Left = 0
            Top = 0
            Width = 234
            Height = 180
            Color = clWhite
            LastCutColor = clLime
            LastCutField = 0
            LastCutMode = lcNone
            MatrixSubType = mstSiroF
            MatrixType = mtSiro
            ActiveColor = clAqua
            ActiveVisible = True
            Anchors = [akLeft, akTop, akRight]
            ChannelColor = clSilver
            ChannelStyle = psSolid
            ChannelVisible = False
            ClusterColor = clSilver
            ClusterStyle = psSolid
            ClusterVisible = False
            CutsColor = clRed
            DefectsColor = clBlack
            DisableMessage = 'Quality Matrix disabled'
            DisplayMode = dmValues
            DotsColor = clBlack
            Enabled = True
            InactiveColor = 9502719
            MatrixMode = mmDisplayData
            SpliceColor = clSilver
            SpliceStyle = psSolid
            SpliceVisible = False
            SubFieldX = 0
            SubFieldY = 0
            ZeroLimit = 0.01
          end
        end
        object mmPanel5: TmmPanel
          Left = 0
          Top = 309
          Width = 234
          Height = 144
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object mFCluster: TFFClusterEditBox
            Left = 1
            Top = 0
            Width = 136
            Height = 54
            Caption = '(*)Fremdfasern'
            TabOrder = 0
            CaptionSpace = True
            Printable = True
            ReadOnly = False
          end
          object mPSettings: TPEditBox
            Left = 1
            Top = 90
            Width = 142
            Height = 54
            Caption = '(11)P Einstellungen'
            TabOrder = 1
            CaptionSpace = True
            Printable = True
            ReadOnly = False
          end
        end
      end
      object mmPanel4: TmmPanel
        Left = 0
        Top = 0
        Width = 223
        Height = 453
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object RepetitionEditBox1: TRepetitionEditBox
          Left = 90
          Top = 0
          Width = 131
          Height = 259
          Caption = '(12)Wiederholung'
          TabOrder = 3
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
        object mChannel: TChannelEditBox
          Left = 2
          Top = 0
          Width = 88
          Height = 144
          Caption = '(8)Kanal'
          TabOrder = 0
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
        object mSplice: TSpliceEditBox
          Left = 2
          Top = 144
          Width = 88
          Height = 164
          Caption = '(8)Spleiss'
          TabOrder = 1
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
        object mYarnCount: TYarnCountEditBox
          Left = 2
          Top = 309
          Width = 220
          Height = 144
          Anchors = [akLeft, akBottom]
          Caption = '(11)Garnnummer'
          TabOrder = 2
          CaptionSpace = True
          Printable = True
          ReadOnly = False
        end
      end
    end
  end
end
