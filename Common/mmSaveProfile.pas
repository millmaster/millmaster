(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmSaveProfile.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 03.10.99 | 0.01 | PW  | File created
|
|=========================================================================================*)

unit mmSaveProfile;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MemoINI;

type
  TmmSaveProfile = class(TComponent)
  private
    FMemoINI: TMemoINI;
    FSelection: tSelectProfileRecordT;
  protected
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
    function Execute: boolean;
  published
    property Selection: tSelectProfileRecordT read FSelection write FSelection;
  end;                                  //TmmSaveProfile

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmSaveProfileDlg;

constructor TmmSaveProfile.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
end;                                    //constructor TmmSaveProfile.Create
//-----------------------------------------------------------------------------
destructor TmmSaveProfile.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TmmSaveProfile.Destroy
//-----------------------------------------------------------------------------
function TmmSaveProfile.Execute: boolean;
begin
  if Owner is TWinControl then
    mmSaveProfileEditor := TmmSaveProfileEditor.Create(Owner, FMemoINI)
  else
    mmSaveProfileEditor := TmmSaveProfileEditor.Create(Application, FMemoINI);

  try
    result := mmSaveProfileEditor.ShowModal = IDOk;
    if result then
      Selection := mmSaveProfileEditor.GetSelectedProfile;
  finally
    mmSaveProfileEditor.Free;
  end;                                  //try..finally
end;                                    //function TmmSaveProfile.Execute

end. //mmSaveProfile.pas

