(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AskAssignForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 08.05.2000  1.01  Mg  | Help inserted
| 06.11.2001  1.02  Nue | distinct inserted in FillCbProdGrpName.
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 22.11.2002        LOK | Default auf OK-Button gelegt
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 27.08.2003  1.10  Nue | Ueberarbeitung Assignment.
| 20.01.2004  1.11  Nue | Ueberarbeitung Security.
| 29.01.2004  1.12  Nue | (NOT(xSettingsNav.SpdRangeList.AssMachine.AWEMachType=amtAC338) added in bOKClick.
| 13.10.2005  1.13  Nue | Addings because of upload from machine at FormShow for FixSpdRange machines.
|=============================================================================*)
unit AskAssignForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmLabel, Mask, mmMaskEdit,
  IvMlDlgs, mmColorDialog, ExtCtrls, mmImage, mmColorButton, IvDictio,
  IvMulti, IvEMulti, mmTranslator, MMUGlobal, mmComboBox, Db,
  MMSecurity,
  AssignComp, mmButton, ADODB, mmADODataSet, mmADOConnection, mmADOCommand,
  SpindleFieldsFrame, //Added on 12.11.01 Nue
  YMParaDef, SettingsFrame, //Added on 06.05.03 Nue
  DetailInfoForm, fcButton, fcImgBtn, fcShapeBtn, mmDialogs,  //Added on 12.06.03 Nue
  StyleUserFields, ActnList, mmActionList, ComCtrls, ImgList, mmImageList,
  mmTabControl;

const
  cProdNameLength = 40;
  ASKASSIGN_WND_REG_STR = 'askassign_wnd_reg_str';
  INIT_FOCUS = 1;

  cTabIndGroup = 0;
  cTabIndMemory = 1;

resourcestring
  rsOverstart             = '(20)(Existiert)'; //ivlm
  rsNotSameTKInRange = '(*)Der gewaehlte Spindelbereich (%d-%d) enthaelt verschiedene Tastkoepfe! (%s) Die gewuenschte Partie kann daher nicht gestartet werden!'; //ivlm
  rsSpdAlreadyAssigned = '(*)Der gewaehlte Spindelbereich (%d-%d), (oder ein Teil davon) ist bereits auf einer anderen Maschinengruppe aktiv! Wollen sie diesen Spindelbereich trotzdem auf Maschinengruppe %d starten?'; //ivlm
  rsSmallerThenProdGrp = '(*)Der gewaehlte Spindelbereich (%d-%d) ist kleiner als der einer bereits bestehenden Maschinengruppe! Wollen sie diesen Spindelbereich trotzdem auf Maschinengruppe %d starten?'; //ivlm
  rsGroups = '(20)Gruppen'; //ivlm
  rsMemories = '(30)Artikelspeicher'; //ivlm

//
//  rsBadSpdRangeMsg = '(*)Der von Ihnen gewaehlte Spulstellenbereich kann nicht zugeordnet werden.'; // ivlm
//  rsAssMachineOfflineMsg = '(*)Zuordnung von Reiniger Einstellungen ist zurzeit nicht moeglich, weil die Maschine keine Verbindung zum MillMaster besitzt.'; // ivlm
//  rsAssignFailedMsg = '(*)Die Zuordnung der Reiniger Einstellungen konnte nicht durchgefuehrt werden.'; // ivlm
//  rsAssignTimeoutMsg = '(*)Das MillMaster Server System ist beschaeftigt. Bitte versuchen Sie die Einstellungen erneut zuzuordnen.'; // ivlm
//  rsAssignProdGrpBusy = '(*)Von den gewaehlten Spulstellen werden zur Zeit Daten akquiriert. Bitte versuchen Sie die Einstellungen erneut zuzuordnen.'; // ivlm
//  rsMachineOffline1 = '(*)Zuordnung von Reiniger Einstellungen nicht moeglich. Grund :'; //ivlm
//  rsMachineOffline2 = '(*)Die Maschine ist ausgeschaltet, nicht am Netzwerk angeschlossen oder an der Maschine werden Einstellungen geaendert.'; // ivlm
//  rsBadMachineGrpMsg = '(*)Die Gruppen Nummer ist bereits in produktion, bitte andere waehlen.'; //ivlm
//  rsGrpNrAlreadyInUse = '(*)Die von Ihnen gewaehlte Gruppen Nummer ist bereits in Produktion. Bitte andere waehlen.'; //ivlm

type
  TAskAssign = class(TDialogBottom)
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    lMachine: TmmLabel;
    mmColorButton1: TmmColorButton;
    mmTranslator1: TmmTranslator;
    bHelp: TmmButton;
    Panel1: TPanel;
    cbProdGrpName: TmmComboBox;
    MMSecurityControl: TMMSecurityControl;
    lStyleName: TmmLabel;
    lStyle: TmmLabel;
    conDefault: TmmADOConnection;
    dseQuery1: TmmADODataSet;
    mmLabel7: TmmLabel;
    lTkName: TmmLabel;
    bAddInfo: TfcShapeBtn;
    mmActionList: TmmActionList;
    acSecurity: TAction;
    acSlip: TAction;
    bAccess: TmmButton;
    acYarnCount: TAction;
    acSpindlerange: TAction;
    acMaGrp: TAction;
    imlAskAssign: TmmImageList;
    tbcGroupMemory: TmmTabControl;
    lOverstart: TmmLabel;
    mSpindleFields: TSpindleFields;
    procedure FormShow(Sender: TObject);
    procedure mmColorButton1ChangeColor(Sender: TObject);
    procedure bHelpClick(Sender: TObject);
    procedure cbProdGrpNameChange(Sender: TObject);
    procedure bAddInfoClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bOKClick(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure acSecurityClick(Sender: TObject);
    procedure MMSecurityControlAfterSecurityCheck(Sender: TObject);
    procedure tbcGroupMemoryChange(Sender: TObject);
  private
    fMachineName: string;
//    fSpindleFirst: string;
//    fSpindleLast: string;
//    fMachGrpNr: string;
    fStyleName: string; //12.11.01
    fYMSetName: string; //12.11.01
    fProdGrpColor: TColor;
    fMachGrpNrOverstart: boolean;
    fWishedSettings: TSettings;
    fStyleID: Integer;  //Nue:03.06.03
//    fStyleInfo: TStringList;   //Nue:03.06.03
    mDetailInfo: TfrmDetailInfo;
    mIntWindowMsg: DWord;
    mOrgCursor: TCursor;
//    mStyleInfo: TStyleInfo;
//    mStyleInfoGot: Boolean;
    procedure SetProdGrpName(aName: string);
    procedure SetProdGrpColor(aColor: TColor);
    function GetProdGrpName: string;
    procedure SpindleFieldsOnChanged(Sender: TObject);
  public
    constructor Create(aOwner: TComponent); override;
    property ProdGrpColor: TColor read fProdGrpColor write SetProdGrpColor;
    property MachineName: string read fMachineName write fMachineName;
//    property SpindleFirst: string read fSpindleFirst write fSpindleFirst;
//    property SpindleLast: string read fSpindleLast write fSpindleLast;
//    property MachGrpNr: string read fMachGrpNr write fMachGrpNr;
    property MachGrpNrOverstart: boolean read fMachGrpNrOverstart write fMachGrpNrOverstart;
    property ProdGrpName: string read GetProdGrpName write SetProdGrpName;
    property StyleName: string read fStyleName write fStyleName;
    property StyleID: Integer read fStyleID write fStyleID;    //Nue:03.06.03
//    property StyleInfo: TStringList read fStyleInfo write fStyleInfo;    //Nue:03.06.03
    property YMSetName: string read fYMSetName write fYMSetName;
    property WishedSettings: TSettings read fWishedSettings write fWishedSettings;
    function FillCbProdGrpName(aReadOnly: Boolean): Boolean;
    function GetStyleSubID(aStyleID: Integer): Integer;
    procedure WndProc(var msg: TMessage); override;
//    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
  end;
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, MMHtmlHelp,
  AssignForm,  //Nue:8.4.03
  SettingsNavFrame, LoepfeGlobal, //Nue:19.6.03
  BaseGlobal, XMLDef, typinfo, mmCS, XMLGlobal;
//------------------------------------------------------------------------------
constructor TAskAssign.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  // create my own window message
  mIntWindowMsg := RegisterWindowMessage(ASKASSIGN_WND_REG_STR);

  mmColorButton1.ColorArray := cMMProdGroupColors;
  HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Reinigereinstellungen_bestaetigen.htm');
  mSpindleFields.OnChanged := SpindleFieldsOnChanged;

  //Organisieren der tbcGroupMemory
  tbcGroupMemory.Tabs.Add(rsGroups);
  tbcGroupMemory.Tabs.Add(rsMemories);

end;

//------------------------------------------------------------------------------
procedure TAskAssign.SetProdGrpName(aName: string);
begin
//    edProdGrpName.Text := aName;
  cbProdGrpName.Text := aName
end;
//------------------------------------------------------------------------------
procedure TAskAssign.SetProdGrpColor(aColor: TColor);
begin
  if (aColor = cDefaultStyleIDColor) then
      // do not display system colors
    fProdGrpColor := clBlue
  else
    fProdGrpColor := aColor;
end;
//------------------------------------------------------------------------------
function TAskAssign.GetProdGrpName: string;
begin
  Result := Trim(cbProdGrpName.Text);
end;
//------------------------------------------------------------------------------

function TAskAssign.FillCbProdGrpName(aReadOnly: Boolean): Boolean;
begin
  cbProdGrpName.Clear;
  if aReadOnly then begin
    cbProdGrpName.Style := csSimple;
    cbProdGrpName.Enabled := False
  end
  else begin
    cbProdGrpName.Style := csDropDown;
    cbProdGrpName.Enabled := True;
    try
      with dseQuery1 do begin
        Close;
//          SQL.Text := 'select * from t_prodgroup where datediff(day,c_prod_start,getdate())<20 order by c_prod_start desc';
        CommandText := 'select distinct(c_prod_name) from t_prodgroup order by c_prod_name';
        Open;
        while not EOF do begin // ADO Conform
          cbProdGrpName.Items.Append(FieldByName('c_prod_name').AsString);
          Next;
        end;
      end;
    except
      on e: Exception do begin
        raise Exception.Create('TAskAssign.FillCbProdGrpName failed. ' + e.Message);
      end;
    end;
  end;
  Result := True;
end;
//------------------------------------------------------------------------------

function TAskAssign.GetStyleSubID(aStyleID: Integer): Integer;
begin
//  Result := 0;
  try
//      mmQuery.Database.StartTransaction;
    with TmmADOCommand.Create(nil) do
    try
      Connection := dseQuery1.Connection;
      CommandText := 'Update t_style set c_max_subID=(c_max_subID+1) where c_style_id=:c_style_id ';
      Parameters.ParamByName('c_style_id').value := aStyleID;
      Execute;
    finally
      Free;
    end; // with TmmADOCommand.Create(nil)do begin

    with dseQuery1 do begin
      Close;
(*        CommandText := 'Update t_style set c_max_subID=(c_max_subID+1) where c_style_id=:c_style_id ';
        Parameters.ParamByName ( 'c_style_id' ).value := aStyleID;
        Open;*)
      CommandText := 'Select c_max_subID from t_style where c_style_id=:c_style_id ';
      Parameters.ParamByName('c_style_id').value := aStyleID;
      Open;
      Result := FieldByName('c_max_subID').AsInteger;
    end; //with
//      mmQuery.Database.Commit;
  except
    on e: Exception do begin
//        mmQuery.Database.Rollback;
      raise Exception.Create('TAskAssign.GetStyleSubID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TAskAssign.FormShow(Sender: TObject);
  var
    i : Integer;
begin
  inherited;

  lMachine.Caption := fMachineName;
//  lSpindles.Caption := fSpindleFirst + ' - ' + fSpindleLast;
//  lMachieGrpNr.Caption := fMachGrpNr;
  lTkName.Caption := cGUISensingHeadClassNames[fWishedSettings.ActSensingHeadClass];

    //Nue:13.11.01
  if fStyleName <> '' then begin
    lStyle.Caption := cStyle;
    lStyleName.Caption := fStyleName;
  end
  else begin
    lStyle.Caption := cTemplate;
    lStyleName.Caption := fYMSetName;
  end;

  //Visualisierung des AdditionalInfo Buttons (abh�ngig, ob �berhaupt zus. ArtikelInfo vorhanden sind)
  if fStyleID<>0 then try
    if not Assigned(mDetailInfo) then begin
      mDetailInfo := TfrmDetailInfo.Create(Self, fStyleID, fStyleName);
      bAddInfo.Visible := mDetailInfo.InfoIsAvailable;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TAskAssign.TfrmDetailInfo.Create failed. ' + e.Message);
    end;
  end;

  mmColorButton1.Color := fProdGrpColor;
  if fMachGrpNrOverstart then
    lOverstart.Caption := rsOverstart
  else
    lOverstart.Caption := '';

  if (mSpindleFields.SpdRangeList.SettingsNavObject as TSettingsNav).SpdRangeList.AssMachine.NetTyp <> ntLX then begin
    if tbcGroupMemory.Tabs.Count = 2 then begin //Verhindern der Anzeige des Memories-Tabs
      tbcGroupMemory.Tabs.Delete(cTabIndMemory);
    end;
  end;

  //Organisieren der tbcGroupMemory
  mSpindleFields.labMemories.Visible := False;
  mSpindleFields.cobMemories.Visible := False;
  tbcGroupMemory.TabIndex := cTabIndGroup; //Groups
  tbcGroupMemory.TabWidth := ((tbcGroupMemory.Width-5) div tbcGroupMemory.Tabs.Count); //Tabs �ber die ganze Breite des TabControls verteilen

  //Disablen von gewissen Feldern (weil zuerst noch ein Upload f�r FixSpdRanges-Maschinen) abgewartet werden muss. Nue:13.10.05
  if ((mSpindleFields.SpdRangeList.SettingsNavObject as TSettingsNav).SpdRangeList.AssMachine.FixSpdRanges) then begin
    mOrgCursor := Screen.Cursor;
    Screen.Cursor := crAppStart;
    bOK.Enabled := False;
    bCancel.Enabled := False;
    mSpindleFields.edSlip.Enabled := False;
    mSpindleFields.edPilot.Enabled := False;

    mSpindleFields.cobGrpNr.Enabled := False;
    mSpindleFields.edYarnCnt.Enabled := False;
    tbcGroupMemory.Enabled := False;
  end;

  bAccess.Enabled := MMSecurityControl.CanEnabled(acSecurity); //Nue:27.1.04

//    if NOT((TMMSettingsReader.Instance.IsPackageLabMaster) or (TMMSettingsReader.Instance.IsPackageDispoMaster)) then begin
//     lStyle.Enabled := False;
//     lStyleName.Enabled := False;
//    end;

  //Speichert die urspr�nglich �bergebene MaGrpNr zur�ck, unabh�ngig von den Aenderungen in cobGrpNr
  mSpindleFields.RestoreMaGrpNr;
  mSpindleFields.cobGrpNrChange(Self);

  //Original Cursor speichern
  mOrgCursor := Self.Cursor;

  //Meldung an eigene Windowsqueue um erst dort den Focus zu setzen
  PostMessage(Self.Handle, mIntWindowMsg, INIT_FOCUS, 0);
end;
//------------------------------------------------------------------------------
procedure TAskAssign.mmColorButton1ChangeColor(Sender: TObject);
begin
  inherited;
  fProdGrpColor := mmColorButton1.Color;
end;
//------------------------------------------------------------------------------
procedure TAskAssign.bHelpClick(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, self.HelpContext);
end;
//------------------------------------------------------------------------------
procedure TAskAssign.cbProdGrpNameChange(Sender: TObject);
begin
//  inherited;
  if StrLen(PChar(cbProdGrpName.Text)) >= cProdNameLength then begin
    Beep;
{
    xStr := cbProdGrpName.Text;
    xStr[cProdNameLength] := Char(0);
    cbProdGrpName.Text := xStr;
    cbProdGrpName.Cursor.
{}
//    cbProdGrpName.Text.
  end;
end;
//------------------------------------------------------------------------------

procedure TAskAssign.bAddInfoClick(Sender: TObject);
begin
//Holen der AdditionalInfo des Artikels
  if fStyleID<>0 then try
    if not Assigned(mDetailInfo) then begin
      mDetailInfo := TfrmDetailInfo.Create(Self, fStyleID, fStyleName);
    end;

    mDetailInfo.Show;  //Anzeigen DetailStyleInfo
  except
    on e: Exception do begin
      raise Exception.Create('TAskAssign.bAddInfoClick failed. ' + e.Message);
    end;
  end;


end;
//------------------------------------------------------------------------------

procedure TAskAssign.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//  mStyleInfoGot := False;
  if Assigned(mDetailInfo) then
    FreeAndNil(mDetailInfo);
end;
//------------------------------------------------------------------------------
//procedure TAskAssign.UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
//var
//  x: integer;
//  xProdID: integer;
//begin
//  try
//EnterMethod('TSettingsNav.UpdateComponent');
//    if csDesigning in ComponentState then exit;
//
//    if Assigned(aSpdRange) and (aSpdRange is TProdGrp)
//codesite.SendMsg('TSettingsNav.UpdateComponent: (7) acMachine.Enabled := aSpdRange.IsAbleToGetMachineSettings');
//    end;
//  finally
//      // LOK (7.11.2002): TSettingsNav wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
//      // ==> Hier wieder einblenden
//    visible := True;
/////    (Owner as TAssign).Settings1.UpdateComponent(aSpdRange,aMachSettings,aMiscInfo); //////////////////////////////
//  end;
//end;
//------------------------------------------------------------------------------

procedure TAskAssign.bOKClick(Sender: TObject);
var
  xSettingsNav: TSettingsNav;
  xTmpStr: string;
  xCount: Integer;
  xMaxGrps : integer;

begin
  mSpindleFields.SpdRangeList.GettingSettingsFromMachine := False; //R�ckstellen des GettingSettingsFromMachine Status Nue:15.11.05

  xSettingsNav := (mSpindleFields.SpdRangeList.SettingsNavObject as TSettingsNav);

  if NOT(xSettingsNav.SpdRangeList.CheckUserValues) then EXIT; //Nue:17.07.03

  with xSettingsNav.SpdRangeList.ProdGrpList do begin
    if xSettingsNav.SpdRangeList.AssMachine.MachineType = mtAC338 then
      xMaxGrps := cWSCSpdGroupLimit
    else
      xMaxGrps := cZESpdGroupLimit;
//      case xSettingsNav.SpdRangeList.AssMachine.FrontType of
//        ftInf68K, ftInfPPC: xMaxGrps := cWSCSpdGroupLimit;
//      else
//        xMaxGrps := cZESpdGroupLimit;
//      end;

//DONE wss: Gruppenindexierung richtig behandelt in while und Abbruchkriterium? -> 0-basiert !!
      //Suchen bereits belegter Spindlen
      xCount:= 0;
      while xCount < (xMaxGrps-1) do begin
        //Check ob Object �berhaupt g�ltig
        if (xCount>=Count) then begin
          xCount :=xMaxGrps-1;   //Abbruchkriterium
        end
        //Check ob an dieser Position zugewiesene ProdGrp ist
        //Diesen CHECK unbedingt ZUERST abhandeln.
        else if  NOT(Objects[xCount] is TProdGrp) then begin
          if (xCount>=Count) then
            xCount :=xMaxGrps-1;   //Abbruchkriterium
        end
        //Nur Check auf gleichen Spindelbereich und zugewiesene MaGrps
        else if (xCount>=Count) OR
                ((mSpindleFields.SpindleFirst = TProdGrp(Objects[xCount]).SpindleFirst) and    //Check ob gleich Gruppe und gleicher Spindlerange
                 (mSpindleFields.SpindleLast = TProdGrp(Objects[xCount]).SpindleLast) and
//NUE1                 (mSpindleFields.cobGrpNr.ItemIndex = TProdGrp(Objects[xCount]).GrpNr))  then begin
                 ((StrToInt(mSpindleFields.cobGrpNr.Items[mSpindleFields.cobGrpNr.ItemIndex])-1) = TProdGrp(Objects[xCount]).GrpNr))  then begin
            xCount :=xMaxGrps-1;   //Abbruchkriterium
        end
        //Nur Check auf gleichen Spindelbereich und ungleiche MaGrp
        else if (xSettingsNav.SpdRangeList.AssMachine.NetTyp <> ntWSC) AND
                ((mSpindleFields.SpindleFirst = TProdGrp(Objects[xCount]).SpindleFirst) and    //Check ob gleich Gruppe und gleicher Spindlerange
                 (mSpindleFields.SpindleLast = TProdGrp(Objects[xCount]).SpindleLast) and
//NUE1                 (mSpindleFields.cobGrpNr.ItemIndex <> TProdGrp(Objects[xCount]).GrpNr)) then begin
                 ((StrToInt(mSpindleFields.cobGrpNr.Items[mSpindleFields.cobGrpNr.ItemIndex])-1) <> TProdGrp(Objects[xCount]).GrpNr))  then begin
          if MMMessageDlg(Format(rsSpdAlreadyAssigned,[mSpindleFields.SpindleFirst,mSpindleFields.SpindleLast,
                                  StrToInt(mSpindleFields.cobGrpNr.Items[mSpindleFields.cobGrpNr.ItemIndex])]),
               mtWarning, [mbOk, mbCancel], 0) <> mrOk then
            EXIT
          else
            xCount :=xMaxGrps-1;   //Abbruchkriterium
        end
        //Check Range kleiner  (bestehende MaGrps)
        else if (xSettingsNav.SpdRangeList.AssMachine.NetTyp <> ntWSC) AND
                ((mSpindleFields.SpindleFirst >= TProdGrp(Objects[xCount]).SpindleFirst) and
                 (mSpindleFields.SpindleLast <= TProdGrp(Objects[xCount]).SpindleLast)) then begin
          if MMMessageDlg(Format(rsSmallerThenProdGrp,[mSpindleFields.SpindleFirst,mSpindleFields.SpindleLast,
                                  StrToInt(mSpindleFields.cobGrpNr.Items[mSpindleFields.cobGrpNr.ItemIndex])]),
               mtWarning, [mbOk, mbCancel], 0) <> mrOk then
            EXIT
          else
            xCount :=xMaxGrps-1;   //Abbruchkriterium
        end
        //Check und WarnungsMessage, wenn Spindlebereich eine MaGrp. �berschreitet. (bestehende MaGrps)
        else if (xSettingsNav.SpdRangeList.AssMachine.NetTyp <> ntWSC) AND
                NOT((mSpindleFields.SpindleFirst > TProdGrp(Objects[xCount]).SpindleLast) or
                     (mSpindleFields.SpindleLast < TProdGrp(Objects[xCount]).SpindleFirst)) then begin
          if MMMessageDlg(Format(rsSpdAlreadyAssigned,[mSpindleFields.SpindleFirst,mSpindleFields.SpindleLast,
                                  StrToInt(mSpindleFields.cobGrpNr.Items[mSpindleFields.cobGrpNr.ItemIndex])]),
               mtWarning, [mbOk, mbCancel], 0) <> mrOk then
            EXIT
          else
            xCount :=xMaxGrps-1;   //Abbruchkriterium
        end; //else if
        INC(xCount);
      end; //while
  end; //with


  codesite.SendFmtMsg('TAskAssignForm.bOKClick: %s',[FormatXml(xSettingsNav.SpdRangeList.AssMachine.MaConfigReader.DOM.xml)]);

  //Check unterschiedliche Tastk�pfe im Zielbereich
  if not mSpindleFields.Settings.CheckEqualTKClassInRange(mSpindleFields.SpindleFirst, mSpindleFields.SpindleLast) then begin
//    xCount := 1;
    xTmpStr := '';
    with xSettingsNav.SpdRangeList.AssMachine.MaConfigReader do begin
//      while (GroupValueDef[xCount,cXPSpindleFromItem,1] <> 0) and (GroupValueDef[xCount,cXPSpindleFromItem,1] <> $FF) do begin
      for xCount := 0 to GroupCount-1 do
        xTmpStr := Format('%s %s-%s: %s, ', [xTmpStr, GroupValueDef[xCount,cXPSpindleFromItem,1], GroupValueDef[xCount,cXPSpindleToItem,1],
                                             cGUISensingHeadNames[GetSensingHeadValue(GroupValueDef[xCount, cXPSensingHeadItem, GetEnumName(TypeInfo(TSensingHead),1)])]]);
    end; //with
    WarningMsg(Format(rsNotSameTKInRange, [mSpindleFields.SpindleFirst, mSpindleFields.SpindleLast, xTmpStr]),Self);
    Exit;
  end;

  //Check unterschiedliche Tastk�pfe der Vorlage und des gew�nschten Zielbereichs
  //  ausser bei Upload von Maschine und Download auf ein Memory
  if not ((xSettingsNav.ActSourceType=stMaMemory) or (tbcGroupMemory.TabIndex=cTabIndMemory))  then begin
    if not mSpindleFields.Settings.CheckEqualTKClassInRangeFromProdGrp(mSpindleFields.SpindleFirst, mSpindleFields.SpindleLast,
      (xSettingsNav.ActCobChooserItem as TBaseSetIDItem).HeadClass) then begin
      WarningMsg(rsNotSameTKforProdGrp, Self);
      Exit;
    end;
  end;
  ModalResult := mrOk;
  //  WarningMsg(aMsg: string; aOwner: TWinControl);
end;

procedure TAskAssign.SpindleFieldsOnChanged(Sender: TObject);
begin
  with mSpindleFields do begin
    if FixSpdRange then begin
      if cobGrpNr.Enabled then begin
        bOK.Enabled := True; //Set OK enabled if cobGrpNr.Enabled: Used because of upload from machine at FormShow for FixSpdRange machines Nue:13.10.05
        bCancel.Enabled := True; //Set Cancel enabled if cobGrpNr.Enabled: Used because of upload from machine at FormShow for FixSpdRange machines Nue:13.10.05
        edSlip.Enabled := True;  //Set Slip enabled if cobGrpNr.Enabled: Used because of upload from machine at FormShow for FixSpdRange machines Nue:13.10.05
        edPilot.Enabled := True;  //Set PilotSpindles enabled if cobGrpNr.Enabled: Used because of upload from machine at FormShow for FixSpdRange machines Nue:13.10.05
        tbcGroupMemory.Enabled := True;  //Nue:3.10.06
        Screen.Cursor := mOrgCursor;
      end;
      lOverstart.Caption := rsOverstart;
      if FixSpdRangeInProd then
        AssignedMark := 'A'
      else
        AssignedMark := '';
    end
    else if (SpdRangeList.CheckGrpNr(cobGrpNr.ItemIndex)) then begin
      lOverstart.Caption := rsOverstart;
      AssignedMark := 'A';
    end
    else begin //Noch nicht zugewiesene ProdGrp
      lOverstart.Caption := '';
      AssignedMark := '';
    end;
    EnableNetDependentFields; //Z.Z. Handling des YarnCnt-Fields
  end;
end;
//------------------------------------------------------------------------------

procedure TAskAssign.bCancelClick(Sender: TObject);
begin
  inherited;
  mSpindleFields.SpdRangeList.GettingSettingsFromMachine := False; //Nue:15.11.05
  ModalResult := mrCancel;
end;
//------------------------------------------------------------------------------

procedure TAskAssign.acSecurityClick(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;

//------------------------------------------------------------------------------

procedure TAskAssign.MMSecurityControlAfterSecurityCheck(Sender: TObject);
begin
  mSpindleFields.edSlip.Enabled := acSlip.Enabled;
  mSpindleFields.edSlip.Visible := acSlip.Visible;
  mSpindleFields.edYarnCnt.Enabled := acYarnCount.Enabled;
  mSpindleFields.edYarnCnt.Visible := acYarnCount.Visible;
  mSpindleFields.edFirst.Enabled := acSpindlerange.Enabled and {wegen FixSpdRange}mSpindleFields.edFirst.Enabled;
  mSpindleFields.edFirst.Visible := acSpindlerange.Visible;
  mSpindleFields.udFirst.Enabled := acSpindlerange.Enabled and {wegen FixSpdRange}mSpindleFields.udFirst.Enabled;
  mSpindleFields.udFirst.Visible := acSpindlerange.Visible;

  mSpindleFields.edLast.Enabled := acSpindlerange.Enabled and {wegen FixSpdRange}mSpindleFields.edLast.Enabled;
  mSpindleFields.edLast.Visible := acSpindlerange.Visible;
  mSpindleFields.udLast.Enabled := acSpindlerange.Enabled and {wegen FixSpdRange}mSpindleFields.udLast.Enabled;
  mSpindleFields.udLast.Visible := acSpindlerange.Visible;
  mSpindleFields.edPilot.Enabled := acSpindlerange.Enabled;
  mSpindleFields.edPilot.Visible := acSpindlerange.Visible;
  mSpindleFields.cobGrpNr.Enabled := acMaGrp.Enabled;
  mSpindleFields.cobGrpNr.Visible := acMaGrp.Visible;
end;

//------------------------------------------------------------------------------
procedure TAskAssign.WndProc(var msg: TMessage);
begin
  if msg.msg = mIntWindowMsg then begin
    case msg.wParam of
      INIT_FOCUS: begin
        if cbProdGrpName.Enabled then
          cbProdGrpName.SetFocus
        else
          if bOk.Enabled then
          bOk.SetFocus;
      end
    else
    end;
  end
  else begin
    inherited WndProc(msg);
  end;  
end;

//------------------------------------------------------------------------------

procedure TAskAssign.tbcGroupMemoryChange(Sender: TObject);
begin
  inherited;
  if (tbcGroupMemory.TabIndex=cTabIndMemory) then begin
    mSpindleFields.edExit(Sender);   //Verhindern dass leere Eingabefelder auftreten Nue:6.2.07
    mSpindleFields.laFirst.Visible := False;
    mSpindleFields.laLast.Visible := False;
    mSpindleFields.laPilot.Visible := False;
    mSpindleFields.laSpindleRange.Visible := False;
    mSpindleFields.laGrpNr.Visible := False;
    mSpindleFields.cobGrpNr.Visible := False;
    mSpindleFields.laAssign.Visible := False;
    mSpindleFields.laYarnCount.Visible := False;
    mSpindleFields.laSlip.Visible := False;
    mSpindleFields.laThreads.Visible := False;
    mSpindleFields.laSections.Visible := False;
    mSpindleFields.edLast.Visible := False;
    mSpindleFields.edPilot.Visible := False;
    mSpindleFields.edFirst.Visible := False;
    mSpindleFields.edYarnCnt.Visible := False;
    mSpindleFields.edSlip.Visible := False;
    mSpindleFields.edThreadCnt.Visible := False;
    mSpindleFields.udFirst.Visible := False;
    mSpindleFields.udLast.Visible := False;
    mSpindleFields.udPilot.Visible := False;
    lOverstart.Visible := False;
    mSpindleFields.labMemories.Visible := True;
    mSpindleFields.cobMemories.Visible := True;
  end
  else if (tbcGroupMemory.TabIndex=cTabIndGroup) then begin
    mSpindleFields.edExit(Sender);   //Verhindern dass leere Eingabefelder auftreten Nue:6.2.07
    mSpindleFields.labMemories.Visible := False;
    mSpindleFields.cobMemories.Visible := False;
    mSpindleFields.laGrpNr.Visible := True;
    MMSecurityControlAfterSecurityCheck(Sender);
    SpindleFieldsOnChanged(Self); //Nue:19.12.06
  end; //else if
end;

end.

