(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmCommonLib.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: div. functions + procedures
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.1999  0.01  PW  |
| 16.10.1999  0.01  PW  | function IsAdmin{istrator}
| 10.11.1999  0.01  Wss | IsAdmin{istrator} corrected to functions in NetAPI32MM
| 01.11.2002  0.01  LOK | Prozedur um ins DebugLog zu schreiben hinzugefügt (WriteDebugLog)
|=========================================================================================*)

unit mmCommonLib;
interface
uses
 SysUtils,Classes, NetAPI32MM;

const
 cListDelimiterChar     = ';';

resourcestring
 cShortMonday           = '(2)Mo'; //ivlm
 cShortTuesday          = '(2)Di'; //ivlm
 cShortWednesday        = '(2)Mi'; //ivlm
 cShortThursday         = '(2)Do'; //ivlm
 cShortFriday           = '(2)Fr'; //ivlm
 cShortSaturday         = '(2)Sa'; //ivlm
 cShortSunday           = '(2)So'; //ivlm

 cLongMonday            = '(15)Montag'; //ivlm
 cLongTuesday           = '(15)Dienstag'; //ivlm
 cLongWednesday         = '(15)Mittwoch'; //ivlm
 cLongThursday          = '(15)Donnerstag'; //ivlm
 cLongFriday            = '(15)Freitag'; //ivlm
 cLongSaturday          = '(15)Samstag'; //ivlm
 cLongSunday            = '(15)Sonntag'; //ivlm

 cShortJanuary          = '(5)Jan'; //ivlm
 cShortFebruary         = '(5)Feb'; //ivlm
 cShortMarch            = '(5)Maer'; //ivlm
 cShortApril            = '(5)Apr'; //ivlm
 cShortMay              = '(5)Mai'; //ivlm
 cShortJune             = '(5)Jun'; //ivlm
 cShortJuly             = '(5)Jul'; //ivlm
 cShortAugust           = '(5)Aug'; //ivlm
 cShortSeptember        = '(5)Sep'; //ivlm
 cShortOctober          = '(5)Okt'; //ivlm
 cShortNovember         = '(5)Nov'; //ivlm
 cShortDecember         = '(5)Dez'; //ivlm

 cLongJanuary           = '(15)Januar'; //ivlm
 cLongFebruary          = '(15)Februar'; //ivlm
 cLongMarch             = '(15)Maerz'; //ivlm
 cLongApril             = '(15)April'; //ivlm
 cLongMay               = '(15)Mai'; //ivlm
 cLongJune              = '(15)Juni'; //ivlm
 cLongJuly              = '(15)Juli'; //ivlm
 cLongAugust            = '(15)August'; //ivlm
 cLongSeptember         = '(15)September'; //ivlm
 cLongOctober           = '(15)Oktober'; //ivlm
 cLongNovember          = '(15)November'; //ivlm
 cLongDecember          = '(15)Dezember'; //ivlm


 procedure AddMonth(var aYear,aMonth: word; aMonthsToAdd: word);
 function AnsiPos(const substr,s: string): integer;
 function deletechar(aInputStr,aDelStr: string): string;
 function lz(value: double; len: byte): string;
 function GetShortDayNames(aDOW: byte): string;
 function GetLongDayNames(aDOW: byte): string;
 function GetShortMonthNames(aMonth: byte): string;
 function GetLongMonthNames(aMonth: byte): string;
 function GetSubStr(aStr: string; aDelimiter: char; aPos: byte): string;
 function GetFirstStr(s: string; SeparatorChar: char): string;
 function GetLastStr(s: string; SeparatorChar: char): string;
 function IsAdmin(aUserName: string): boolean;
 function IsLeapYear(aYear: integer): boolean;
 function DaysPerMonth(aYear,aMonth: integer): integer;
 function bDOW(dow: byte): byte; //from us to bible order
 function usDOW(dow: byte): byte; //from bible to us order

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure AddMonth(var aYear,aMonth: word; aMonthsToAdd: word);
begin
 if (aMonth +aMonthsToAdd) > 12 then
 begin
  inc(aYear);
  aMonth := aMonth +aMonthsToAdd -12;
 end //if (aMonth +aMonthsToAdd) > 12 then
 else inc(aMonth,aMonthsToAdd);
end; //procedure AddMonth
//-----------------------------------------------------------------------------
function AnsiPos(const substr,s: string): integer;
begin
 result := system.pos(substr,s);
end; //function AnsiPos
//-----------------------------------------------------------------------------
function deletechar(aInputStr,aDelStr: string): string;
var
 c : char;
 i : integer;

begin
 for i := 1 to length(aDelStr) do
 begin
  c := aDelStr[i];

  while pos(c,aInputStr) > 0 do
   delete(aInputStr,pos(c,aInputStr),1);
 end; //for i := 1 to length(aDelStr) do

 result := aInputStr;
end; //function deletechar
//-----------------------------------------------------------------------------
function lz(value: double; len: byte): string;
var
 i       : integer;
 xFrmstr : string;

begin
 xFrmstr := '';
 for i := 1 to len do
  xFrmstr := xFrmstr +'0';
 result := formatfloat(xFrmstr,value);
end; //function lz
//-----------------------------------------------------------------------------
function GetSubStr(aStr: string; aDelimiter: char; aPos: byte): string;
var
 xMaxC,
 i,j    : integer;
 sArray : array of string;

begin
 result := '';
 xMaxC := 0;

 for i := 1 to length(aStr) do
  if aStr[i] = aDelimiter then inc(xMaxC);

 if aPos > xMaxC then exit;
 SetLength(sArray,xMaxC);

 i := pos(aDelimiter,aStr);
 j := 0;

 while (i > 0) and (j < xMaxC) do
 begin
  sArray[j] := copy(aStr,1,i-1);
  delete(aStr,1,i);
  i := pos(aDelimiter,aStr);
  inc(j);
 end; //while (i > 0) and (j < (xMaxC -1))  do

 result := trim(sArray[aPos-1]);

 sArray := nil;
end; //function GetSubStr
//-----------------------------------------------------------------------------
function GetShortDayNames(aDOW: byte): string;
begin
 case aDOW of
  1: result := cShortMonday;
  2: result := cShortTuesday;
  3: result := cShortWednesday;
  4: result := cShortThursday;
  5: result := cShortFriday;
  6: result := cShortSaturday;
  7: result := cShortSunday;
 end; //case aDOW of
end; //function GetShortDayNames
//-----------------------------------------------------------------------------
function GetLongDayNames(aDOW: byte): string;
begin
 case aDOW of
  1: result := cLongMonday;
  2: result := cLongTuesday;
  3: result := cLongWednesday;
  4: result := cLongThursday;
  5: result := cLongFriday;
  6: result := cLongSaturday;
  7: result := cLongSunday;
 end; //case aDOW of
end; //function GetLongDayNames
//-----------------------------------------------------------------------------
function GetShortMonthNames(aMonth: byte): string;
begin
 case aMonth of
   1: result := cShortJanuary;
   2: result := cShortFebruary;
   3: result := cShortMarch;
   4: result := cShortApril;
   5: result := cShortMay;
   6: result := cShortJune;
   7: result := cShortJuly;
   8: result := cShortAugust;
   9: result := cShortSeptember;
  10: result := cShortOctober;
  11: result := cShortNovember;
  12: result := cShortDecember;
 end; //case aMonth of
end; //function GetShortMonthNames
//-----------------------------------------------------------------------------
function GetLongMonthNames(aMonth: byte): string;
begin
 case aMonth of
   1: result := cLongJanuary;
   2: result := cLongFebruary;
   3: result := cLongMarch;
   4: result := cLongApril;
   5: result := cLongMay;
   6: result := cLongJune;
   7: result := cLongJuly;
   8: result := cLongAugust;
   9: result := cLongSeptember;
  10: result := cLongOctober;
  11: result := cLongNovember;
  12: result := cLongDecember;
 end; //case aMonth of
end; //function GetLongMonthNames
//-----------------------------------------------------------------------------
function GetFirstStr(s: string; SeparatorChar: char): string;
var
 i : integer;

begin
 i := pos(SeparatorChar,s);
 if i = 0 then result := s
          else result := copy(s,1,i -1);
end; //function GetFirstStr
//-----------------------------------------------------------------------------
function GetLastStr(s: string; SeparatorChar: char): string;
var
 i : integer;

begin
 i := pos(SeparatorChar,s);
 if i = 0 then result := s
          else result := copy(s,i +1,MaxInt);
end; //function GetLastStr
//-----------------------------------------------------------------------------
function IsAdmin(aUserName: string): boolean;
var
 xList       : TStringList;
 xServerName : string;

begin
  Result := False;

  xList := TStringList.Create;
  //sList := GetServerName(mmUsername,gMMHost); //wetsrvbde
  GetServerName(aUserName,cSV_TYPE_SQLSERVER, xList);
  try
    if xList.Count = 0 then exit;
    xServerName := trim(xList[0]);
    if pos('\\',xServerName) = 1 then system.delete(xServerName,1,2); //will be "re"-added automatically in GetUserAccountInfo

    GetUserAccountInfo(aUserName,xServerName, xList);
    if xList.Count > 3 then
     result := xList[3] = inttostr(cUSER_PRIV_ADMIN)
  finally
    xList.free;
  end; //try..finally
end; //function IsAdmin
//-----------------------------------------------------------------------------
function IsLeapYear(aYear: integer): boolean;
begin
 result := (aYear mod 4 = 0) and ((aYear mod 100 <> 0) or (aYear mod 400 = 0));
end; //function IsLeapYear
//-----------------------------------------------------------------------------
function DaysPerMonth(aYear,aMonth: integer): integer;
const
 cDaysInMonth: array[1..12] of integer = (31,28,31,30,31,30,31,31,30,31,30,31);

begin
 result := cDaysInMonth[aMonth];
 if (aMonth = 2) and IsLeapYear(aYear) then inc(result);
end; //function DaysPerMonth
//-----------------------------------------------------------------------------
function bDOW(dow: byte): byte; //from us to bible order
begin
 //us sunday = 1st day of week
 if dow = 1 then dow := 7
            else dec(dow);
 result := dow;
end; //function bDOW
//-----------------------------------------------------------------------------
function usDOW(dow: byte): byte; //from bible to us order
begin
 if dow = 7 then dow := 1
            else inc(dow);
 result := dow;
end; //function usDOW

end. //mmCommonLib.pas
