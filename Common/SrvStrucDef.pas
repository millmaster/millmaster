unit SrvStrucDef;

interface

uses
  Windows;


const
  SS_MAJOR_VERSION   = 7;
  SS_MINOR_VERSION   = 00;
  SS_LEVEL_VERSION   = 0000;
  SS_MINIMUM_VERSION = '7.00.00.0000';
  ODS_VERSION	     = ((SS_MAJOR_VERSION shl 24) or (SS_MINOR_VERSION shl 16));

  // Macros -- return codes
  XP_NOERROR     = 0;
  XP_ERROR       = 1;

  // values of status parameter to srv_senddone
  // Also: values for symbol parameters to srv_symbol when type = SRV_DONE
  SRV_DONE_FINAL	= $0000;
  SRV_DONE_MORE		= $0001;
  SRV_DONE_ERROR	= $0002;
  SRV_DONE_COUNT	= $0010;
  SRV_DONE_RPC_IN_BATCH = $0080;

const
  SRV_TDS_BIGVARCHAR     = $A7;


  SRVBIGVARCHAR = SRV_TDS_BIGVARCHAR;

const
  cMSSQLLibrary = 'opends60.dll';  // in MSSQL7\Binn directory

  // Sizes of fields in loginrec
  SRV_NULLTERM  = -1;   // Length to indicate string is null terminated
  SRV_MAXNAME   = 30;	// maximum login names
  SRV_PKTLEN    = 6;    // maximum size of packet size string
  SRV_HOSTIDLEN	= 8;	// maximum size of host id field
//  SRV_MAXREM  = 255; // maximum length of remote password name
//  SRV_MAXFILENAME = 64; // maximum size of filename

  DBPROGNLEN = 10;
  SRV_NUMINPUTBUFS = 2;

// user defined data types
type
  DBInt       = Integer;
  DBChar      = PChar;
  DBUSMALLINT = Word;

type
  // forward declaration of pointer of record
  PSRV_PROC = ^TSRV_PROC;

  // SRV_INPUTBUF
  // This structure maintains the read information when
  // pre-reading packets.
  PSRV_INPUTBUF = ^TSRV_INPUTBUF;
  TSRV_INPUTBUF = record
    pBuffer: PByte;
    ioStatus: LongWord;
    ioRecvCnt: LongWord;
    pNext: PSRV_INPUTBUF; // struct srv_inputbuf * pNext;
  end; // SRV_INPUTBUF;

  // SRV_COMPORT_QUEUE
  // Queue entry for completion port requests.
  TSRV_COMPORT_QUEUE = record
    Flink: Pointer;  // struct srv_comport_queue*	Flink;
    Blink: Pointer;  // struct srv_comport_queue*	Blink;
  end; // SRV_COMPORT_QUEUE;

  PSRV_HANDLER = ^TSRV_HANDLER;
  TSRV_HANDLER = record
    previous: Pointer; // struct srv_handler * previous;
    next: Pointer;     // struct srv_handler * next;
    last: Pointer;     // struct srv_handler * last;
    handler: Pointer;  // int (* handler)(void *);
  end; // SRV_HANDLER;

  PSRV_PEVENTS = ^TSRV_PEVENTS;
  TSRV_PEVENTS = record
    srv_connect: PSRV_HANDLER;
    srv_disconnect: PSRV_HANDLER;
    srv_rpc: PSRV_HANDLER;
    srv_language: PSRV_HANDLER;
    srv_start: PSRV_HANDLER;
    srv_stop: PSRV_HANDLER;
    srv_sleep: PSRV_HANDLER;
    srv_restart: PSRV_HANDLER;
    srv_transmgr: PSRV_HANDLER;
    srv_oledb: PSRV_HANDLER;
  end; // SRV_PEVENTS

  PSRV_LISTHEAD = ^TSRV_LISTHEAD;
  TSRV_LISTHEAD = record
    Flink: Pointer; // struct srv_listentry * Flink;
    Blink: Pointer; // struct srv_listentry * Blink;
    NumListEntries: Integer;
    NumReadRequest: Integer;
    MaxQueueLength: Integer;
    ListLock: LongInt;
    ListEvent: THandle;
  end; // SRV_LISTHEAD, *PSRV_LISTHEAD;

  PSRV_LISTENTRY = ^TSRV_LISTENTRY;
  TSRV_LISTENTRY = record
    Flink: Pointer; // struct srv_listentry * Flink;
    Blink: Pointer; // struct srv_listentry * Blink;
    pSrvProc: PSRV_PROC; // struct srv_proc      * pSrvProc;
  end; // SRV_LISTENTRY, *PSRV_LISTENTRY;

  TTRANSLATION_INFO = record
    flag: Boolean;                      // data translation flag
    int2: Pointer; // void (*int2)(short*);           // 2-byte integer swap function
    int4: Pointer; // void (*int4)(long*);            // 4-byte integer swap function
    recvflt: Pointer; // void (*recvflt)(float*);        // receive 8-byte float translation function
    sendflt: Pointer; // void (*sendflt)(float*);        // send 8-byte float translation function
    recvflt4: Pointer; // void (*recvflt4)(float*);       // receive 4-byte float translation function
    sendflt4: Pointer; // void (*sendflt4)(float*);       // send 4-byte float translation function
    recvchar: Pointer; // void (*recvchar)(char *, int);  // ASCII translation function
    sendchar: Pointer; // void (*sendchar)(char *, int);  // ASCII translation function
    twoint4: Pointer; // void (*twoint4)(char *);        // 2 4-byte integer swap function
    twoint2: Pointer; // void (*twoint2)(char *);        // 2 2-byte interger swap function
  end; // TRANSLATION_INFO;

  PSRV_SUBCHANNEL = ^TSRV_SUBCHANNEL;
  TSRV_SUBCHANNEL = record
    srvproc: Pointer; //struct srv_proc * srvproc;
    threadHDL: THandle;
    threadID: THandle;  // THREAD
    ss_handle: THandle;
    cs_handle: THandle;
    hEvent: THandle;
    status: Boolean;
    site_srvproc: Pointer; //struct srv_proc * site_srvproc;
    index: LongWord;
    master_list: Pointer; //struct srv_proc ** master_list;
  end; // SRV_SUBCHANNEL

  // SRV_RPCp: RPC parameter information
  PSRV_RPC = ^TSRV_RPC;
  TSRV_RPC = record
    len: Byte;          // length of RPC parameter name
    rpc_name: PByte;     // pointer to the RPC parameter name
    status: Byte;       // return status, 1 = passed by reference
    user_type: LongWord;    // User-defined data type
    typ: Byte;         // data type
    maxlen: LongWord;       // maximum possible data length
    actual_len: LongWord;   // actual data length
    value: Pointer;        // the actual data
  end; // SRV_RPC;

  // SRV_COLDESC: column description array (used by srv_describe & srv_sendrow)
  PSRV_COLDESC = ^TSRV_COLDESC;
  TSRV_COLDESC = record
    otype: LongWord;        // output data type
    olen: LongWord;         // length
    itype: LongWord;        // input data type
    ilen: LongWord;         // length
    data: PByte;		// data buffer address
    user_type: DBInt;		// user-defined data type
    precision: Byte; 	// precision
    scale: Byte;		// scale
  end; // SRV_COLDESC;

  TSRV_LOGINREC = record
    lhostname: Array[1..SRV_MAXNAME] of Byte;    // name of host or generic
    lhostnlen: Byte;         // length of lhostname
    lusername: Array[1..SRV_MAXNAME] of Byte;    // name of user
    lusernlen: Byte;         // length of lusername
    lpw: Array[1..SRV_MAXNAME] of Byte;  // password (plaintext)
    lpwnlen: Byte;           // length of lpw
    lhostproc: Array[1..SRV_HOSTIDLEN] of Byte;	 // host process identification
    lunused: Array[1..SRV_MAXNAME-SRV_HOSTIDLEN - 6] of Byte;	 // unused
    lapptype: Array[1..6] of Byte;	    // Application specific.
    lhplen: Byte;            // length of host process id
    lint2: Byte;             // type of int2 on this host
    lint4: Byte;             // type of int4 on this host
    lchar: Byte;             // type of char
    lflt: Byte;              // type of float
    ldate: Byte;             // type of datetime
    lusedb: Byte;            // notify on exec of use db cmd
    ldmpld: Byte;            // disallow use of dump/load and bulk insert
    linterface: Byte;        // SQL interface type
    ltype: Byte;             // type of network connection
    spare: Array[1..7] of Byte;          // NOTE: Apparently used by System-10
    lappname: Array[1..SRV_MAXNAME] of Byte; // application name
    lappnlen: Byte;          // length of appl name
    lservname: Array[1..SRV_MAXNAME] of Byte;    // name of server
    lservnlen: Byte;         // length of lservname
    lrempw: Array[1..$ff] of Byte;      // passwords for remote servers
    lrempwlen: Byte;         // length of lrempw
    ltds: Array[1..4] of Byte;           // tds version
    lprogname: Array[1..DBPROGNLEN] of Byte; // client program name
    lprognlen: Byte;         // length of client program name
    lprogvers: Array[1..4] of Byte;      // client program version
    lnoshort: Byte;          // NEW: auto convert of short datatypes
    lflt4: Byte;             // NEW: type of flt4 on this host
    ldate4: Byte;            // NEW: type of date4 on this host
    llanguage: Array[1..SRV_MAXNAME] of Byte;    // NEW: initial language
    llanglen: Byte;          // NEW: length of language
    lsetlang: Byte;          // NEW: notify on language change
    slhier: SmallInt;           // NEW: security label hierarchy
    slcomp: Array[1..8] of Byte;         // NEW: security components
    slspare: SmallInt;          // NEW: security label spare
    slrole: Byte;            // NEW: security login role
    lcharset: Array[1..SRV_MAXNAME] of Byte; // character set name (unused)
    lcharsetlen: Byte;       // length of character set (unused)
    lsetcharset: Byte;       // notify on character set change (unused)
    lpacketsize: Array[1..SRV_PKTLEN] of Byte; // length of TDS packets
    lpacketsizelen: Byte;    // length of lpacketsize
    ldummy: Array[1..3] of Byte;         // NEW: pad to longword
  end;  // TSRV_LOGINREC;

  TSRV_IO = packed record
    server_ep: Pointer;       // server side endpoint
{$ifdef BRIDGE}
    client_ep: Pointer;       // client side endpoint
    fCSerror: Boolean;        // flag indicate client-side error
{$endif}
    cs_sub_handle: THandle;  // client-side subchannel local handle
    outbuff: PByte;        // send: start of send buffer
    p_outbuff: PByte;      // send: next place in buffer
    n_outbuff: Word;      // send: room left in send buffer
    inbuff: PByte;         // recv: start of read buffer
    p_inbuff: PByte;       // recv: next place in buffer
    n_inbuff: Word;       // recv: room left in read buffer
    SQLspanned: Integer;     // flag indicating that the SQL command
                                //  has spanned more that 1 network buffer.
    cbflag: Integer;	  				// flag indicating that a client buffer is
                                //  available.
    channel: DBUSMALLINT;        // io packet subchannel
    packet: Byte;         // io packet number
    window: Byte;         // io packet window size
    ioEvent: THandle;        // io event handle
    ioOverlapped: OVERLAPPED;   // io overlapped structure.
    pNextInBuf: PSRV_INPUTBUF;  // current io input buffer index
    ioInputBuffers: Array[1..SRV_NUMINPUTBUFS] of TSRV_INPUTBUF; // io input buffers
    ss_sub_handle: THandle;  // server-side subchannel local handle
    packetsize: Word;	    // TDS packet size
  end; // TSRV_IO

{$MINENUMSIZE 4}
  TFlags = (flLocalPost, flMadeBoundCall);
{$MINENUMSIZE 1}

  // SRV_PROC:   This is the main connection structure
  // PSRV_PROC is defined above as pre-declaration
  TSRV_PROC = packed record
    tdsversion: WORD;   // version of tds detected from client
                                //   0x3400 = 3.4, 0x4000 = 4.0
    status: WORD;       // status of this SRV_PROC
    srvio: TSRV_IO;        // I/O structure of srvproc
    login: TSRV_LOGINREC;        // login record received from the client
    langbuff: Pointer;     // pointer to language buffer
    langlen: LongWord;      // length of language buffer
    event: Integer;        // event variable
    server: Pointer;       // pointer to associated SRV_SERVER structure
    threadstack: PChar;  // stack pointer of event thread stack
    threadID: THandle;     // THREAD: thead ID associated with this SRV_PROC
    threadHDL: THandle;    // thread handle for resume and suspend
    iowakeup: THandle;     // event handle to wait on for overlapped io
                                // and more. Passed on to SQL Server and netlibs.
    exited: THandle;       // semaphore indicating that thread
                                //  associated with this SRV_PROC has exited
    rowsent: DBInt;      // # of rows sent to client
    coldescp: PSRV_COLDESC;     // pointer to column description array
    coldescno: DBUSMALLINT;    // count of column descriptions
    colnamep: PByte;     // pointer to column name list
    colnamelen: Word;   // length of column name list
    userdata: Pointer;     // pointer to user's private data area
    event_data: Pointer;   // pointer to event data area
    serverlen: Byte;    // length of server name
    Pad1: Byte;
    servername: PByte;   // name of server

    //  RPC info
    //
    rpc_active: Byte;          // flag indicating active RPC (TRUE=active)
    rpc_server_len: Byte;      // length of RPC server name
    rpc_server: PByte;          // name of RPC server
    rpc_database_len: Byte;    // length of RPC database name
    Pad2: Byte;
    rpc_database: PByte;        // name of RPC database
    rpc_owner_len: Byte;       // length of RPC owner name
    Pad3: Byte;
    rpc_owner: PByte;           // name of RPC owner
    rpc_proc_len: Byte;        // length of RPC or stored procecedure name
    Pad4: Byte;
    rpc_proc_name: PByte;       // name of RPC or stored procedure
    rpc_proc_number: LongWord;     // number of RPC "procedure_name;number"
    rpc_linenumber: LongWord;      // line number batch error occurred on.
    rpc_options: Word;         // recompile option flag (bit 0)
    rpc_num_params: Word;      // number of RPC parameters
    rpc_params: Array[0..0] of PSRV_RPC;          // array of pointers to each RPC paramater

    // Return information for non-remote procedure call client command.
    // This information is provided by the function srv_returnval().
    // flag indicating active non-RPC values (TRUE = active)
    //
    non_rpc_active: Byte;
    Pad5: Byte;

    // number of non-RPC parameters
    //
    non_rpc_num_params: Word;

    // array of pointers to each non-RPC paramater
    //
    non_rpc_params: Array[0..0] of PSRV_RPC;

    // temporary work buffer
    //
    temp_buffer: Array[1..100] of Char;

    // array of subchannel structures
    //
    subprocs: Array[0..0] of PSRV_SUBCHANNEL;

    // Data Translation information
    //
    translation_info: TTRANSLATION_INFO;
    IOListEntry: TSRV_LISTENTRY;      // struct srv_listentry IOListEntry;
    CommandListEnry: TSRV_LISTENTRY;  // struct srv_listentry CommandListEntry;
    pNetListHead: PSRV_LISTHEAD;
    bNewPacket: Boolean;
    Pad6: Array[1..3] of Byte;
    StatusCrit: LongInt;	// critical section to check attentions
    serverdata: Pointer;    // private data area used ONLY by SQL Server
    subchannel: PSRV_SUBCHANNEL;

    // Pre && Post Handler pointers
    //
    pre_events: PSRV_PEVENTS;
    post_events: PSRV_PEVENTS;

    // current position in language buffer used by processRPC
	//
    p_langbuff: Pointer;

    fSecureLogin: Boolean;
    Pad7: Array[1..3] of Byte;

    // Set by the server when making an outbound call to an XP.
    fInExtendedProc: Boolean;
    Pad8: Array[1..3] of Byte;

    // If TRUE indicates the current buffer is from
    // srv_post_completion_queue and the buffer should be
    // deallocated when the current work is completed.  If
    // FALSE the current buffer came from the network and a
    // new read should be posted on the net.
    //
    flags: Array[1..4] of Byte;
//    flags: set of TFlags;
//    unsigned    fLocalPost:1;

    // If TRUE an XP associated with this srvproc made a call back into
    // the server on a bound connection. This flag is used to allow the
    // calling session to wait for logout processing of the XP to complete
    //
//    unsigned	fMadeBoundCall:1;

    // Filler to align after flags above.
    //
//    unsigned	uFill1:30;

    // List of requests posted using srv_post_completion_queue. Entries are
    // dynamically allocated inside srv_post_completion_queue and enqueued
    // in netIOCompletionRoutine. After the request has been processed the
    // entry is removed from this queue and deallocated.
    //
    // NOTE: Must hold SRV_PROC spinlock while altering queue.
    //
    comport_queue: TSRV_COMPORT_QUEUE;

    // Data pointers reserved for the Starfighter team
    //
    pSF1: Pointer;    // Currently in use by SQLTrace
    pSF2: Pointer;    // Reserved

    // Pre and Post handler semephores used during handler instals
    // and deinstals.
    //
    hPreHandlerMutex: THandle;
    hPostHandlerMutex: THandle;
    bSAxp: Boolean;		// Used by XP's to determine if running as SA
    Pad9: Array[1..3] of Byte;
  end;  // TSRV_PROC


function srv_rpcparams(aSrvProc: PSRV_PROC): Integer; cdecl; external cMSSQLLibrary;

function srv_paraminfo(pSrvProc: PSRV_PROC; n: Integer; Typ: PByte; MaxLen: PLongInt;
                       ActualLen: PLongInt; Pad1: Integer; var Bool: Boolean): Integer; cdecl; external cMSSQLLibrary;

function srv_paramset(srvproc: PSRV_PROC; n: Integer; data: Pointer; len: Integer): Integer; cdecl; external cMSSQLLibrary;

function srv_describe(srvproc: PSRV_PROC;
		      colnumber: Integer;
		      columnname: DBChar;
		      namelen: Integer;
		      desttype: DBInt;
		      destlen: DBInt;
		      srctype: DBInt;
		      srclen: DBInt;
		      srcdata: Pointer ): Integer; cdecl; external cMSSQLLibrary;

function srv_sendrow(srvproc: PSRV_PROC ): Integer; cdecl; external cMSSQLLibrary;

function srv_senddone(srvproc: PSRV_PROC;
		      status: DBUSMALLINT;
		      curcmd: DBUSMALLINT;
		      count:  DBInt): Integer; cdecl; external cMSSQLLibrary;

(**
//
//  Define Other External API's
//
function srv_alloc(size: DBInt): Pointer;

function srv_bmove(from, to: Pointer; count: DBInt): Integer;

function srv_bzero(location: Pointer; count: DBInt): Integer;

function srv_config(config: PSRV_CONFIG; option: DBInt; value: DBChar; valuelen: Integer): Integer;

SRV_CONFIG *
function srv_config_alloc( void );

function srv_convert( SRV_PROC * srvproc,
			int	       srctype,
			void	 * src,
			DBInt	       srclen,
			int	       desttype,
			void	 * dest,
			DBInt	       destlen ): Integer;

	(* SRVAPI srv_errhandle( int (CDECL *handler)
				 ( SRV_SERVER * server,
				   SRV_PROC *   srvproc,
				   int		    srverror,
				   BYTE 	    severity,
				   BYTE 	    state,
				   int		    oserrnum,
				   DBChar *     errtext,
				   int		    errtextlen,
				   DBChar *     oserrtext,
				   int		    oserrtextlen) ))
				 ( SRV_SERVER * server,
				   SRV_PROC *   srvproc,
				   int		    srverror,
				   BYTE 	    severity,
				   BYTE 	    state,
				   int		    oserrnum,
				   DBChar *     errtext,
				   int		    errtextlen,
				   DBChar *     oserrtext,
				   int		    oserrtextlen ): Integer;

function srv_event( SRV_PROC * srvproc,
		      int	     event,
		      BYTE     * data ): Integer;

function srv_free( void * ptr ): Integer;

void *
function srv_getuserdata( SRV_PROC * srvproc);

function srv_getbindtoken( SRV_PROC * srvproc, char * token_buf ): Integer;

function srv_getdtcxact( SRV_PROC * srvproc, void** ppv): Integer;

    (* SRVAPI srv_handle( SRV_SERVER * server,
			      DBInt	       event,
				  int (CDECL *handler)(void * ) ))
			      (void * ): Integer;

function srv_impersonate_client( SRV_PROC * srvproc ): Integer;

SRV_SERVER *
function srv_init( SRV_CONFIG * config,
		     DBChar	* connectname,
		     int	      namelen );

BOOL
function srv_iodead( SRV_PROC * srvproc );

long
function srv_langcpy( SRV_PROC *	srvproc,
			long		start,
			long		nbytes,
			DBChar	 *	buffer );

long
function srv_langlen( SRV_PROC * srvproc );

void *
function srv_langptr( SRV_PROC *srvproc );

function srv_log( SRV_SERVER * server,
		    BOOL	     datestamp,
		    DBChar     * msg,
		    int 	     msglen ): Integer;

void *
function srv_paramdata( SRV_PROC * srvproc,
			  int		 n );

function srv_paramlen( SRV_PROC * srvproc,
			 int		n ): Integer;

function srv_parammaxlen( SRV_PROC * srvproc,
			    int 	   n ): Integer;

DBChar *
function srv_paramname( SRV_PROC * srvproc,
			  int		 n,
			  int	   * len );

function srv_paramnumber( SRV_PROC * srvproc,
			    DBChar   * name,
			    int 	   len ): Integer;

function srv_paramstatus( SRV_PROC * srvproc,
			    int 	   n ): Integer;

function srv_paramtype( SRV_PROC * srvproc,
			  int		 n ): Integer;


DBChar *
function srv_pfield( SRV_PROC * srvproc,
		       int	      field,
		       int	* len );

function srv_returnval( SRV_PROC * srvproc,
			  DBChar   * valuename,
			  int		 len,
			  BYTE		 status,
			  DBInt 	 type,
			  DBInt 	 maxlen,
			  DBInt 	 datalen,
			  void	   * value ): Integer;

function srv_revert_to_self( SRV_PROC * srvproc ): Integer;

DBChar *
function srv_rpcdb( SRV_PROC * srvproc,
		      int      * len );

DBChar *
function srv_rpcname( SRV_PROC * srvproc,
			int	 * len );

function srv_rpcnumber( SRV_PROC * srvproc ): Integer;

DBUSMALLINT
function srv_rpcoptions( SRV_PROC * srvproc );

DBChar *
function srv_rpcowner( SRV_PROC * srvproc,
			 int	  * len );

function srv_rpcparams( SRV_PROC * srvproc ): Integer;

function srv_run( SRV_SERVER * server ): Integer;

function srv_sendmsg( SRV_PROC * srvproc,
			int	       msgtype,
			DBInt	       msgnum,
			DBTINYINT      msgclass,
			DBTINYINT      state,
			DBChar	 * rpcname,
			int	       rpcnamelen,
			DBUSMALLINT    linenum,
			DBChar	 * message,
			int	       msglen ): Integer;

function srv_sendstatus( SRV_PROC * srvproc,
			   DBInt	  status ): Integer;

function srv_setcoldata( SRV_PROC * srvproc,
			   int		  column,
			   void     * data): Integer;

function srv_setcollen( SRV_PROC * srvproc,
			  int		 column,
			  int	 len ): Integer;

function srv_setuserdata( SRV_PROC * srvproc,
			    void *	   ptr): Integer;

function srv_setutype( SRV_PROC * srvproc,
			 int		column,
			 DBInt		usertype ): Integer;

DBChar *
function srv_sfield( SRV_SERVER * server,
		       int		field,
		       int	  * len );
DBChar *
function srv_symbol( int	 type,
		       int	 symbol,
		       int * len );

function srv_tdsversion( SRV_PROC * srvproc ): Integer;

BOOL
function srv_willconvert( int srctype,
			    int desttype );
function srv_writebuf( SRV_PROC * srvproc,
             void * ptr,
             WORD count ): Integer;

function srv_get_text( SRV_PROC * srvproc,
			 long * outlen): Integer;

void
function srv_ackattention( SRV_PROC * srvproc);

function srv_terminatethread( SRV_PROC * srvproc): Integer;

function srv_sendstatistics( SRV_PROC * srvproc): Integer;

function srv_clearstatistics( SRV_PROC * srvproc): Integer;

function srv_setevent( SRV_SERVER * server, int event): Integer;

function srv_message_handler(SRV_PROC * srvproc, int errornum, BYTE severity,
		BYTE state, int oserrnum, char *errtext, int errtextlen,
		char *oserrtext, int oserrtextlen): Integer;

function srv_pre_handle(SRV_SERVER * server,
			     SRV_PROC * srvproc,
			     DBInt event,
				 int (CDECL *handler)(void * ),
			     BOOL remove): Integer;
function srv_post_handle(SRV_SERVER * server,
			    SRV_PROC * srvproc,
			    DBInt event,
				int (CDECL *handler)(void * ),
			    BOOL remove): Integer;

function srv_ansi_sendmsg( SRV_PROC * srvproc,
			int	       msgtype,
			DBInt	       msgnum,
			DBTINYINT      msgclass,
			DBTINYINT      state,
			DBChar	 * rpcname,
			int	       rpcnamelen,
			DBUSMALLINT    linenum,
			DBChar	 * message,
			int	       msglen ): Integer;

function srv_post_completion_queue( SRV_PROC * srvproc,
			DBChar	 * inbuf,
			int	 inbuflen ): Integer;
(**)
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
end.
