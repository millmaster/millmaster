unit regWorbis;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R mmGraph.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  InfoPowr,  // loaded from ThirdParty directory
  mmGraph,
  mmGridPos,
  mmLengthUnit,
  ShiftCalendars,
  NWCombo;

//------------------------------------------------------------------------------
procedure MMRegister;
begin
  InfoPowr.Register;
  // Unit mmGraph
  RegisterComponents('Worbis', [TmmGraph]);
  // Unit mmGridPos
  RegisterComponents('Worbis', [TmmGridPos]);
  // Unit mmLengthUnit
  RegisterComponents('Worbis', [TmmLengthUnits]);
  // Unit ShiftCalendar
  RegisterComponents('Worbis', [TShiftCalendarGrid,
                                TDefaultShiftGrid,
                                TShiftPatternGrid,
                                TAppendDefaultShifts,
                                TFactoryCalendar]);
  // Unit NWCombo
  RegisterComponents('Worbis', [TNWComboBox]);
end;
//------------------------------------------------------------------------------
end.
