(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AskAssignForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 08.05.2000  1.01  Mg  | Help inserted
| 06.11.2001  1.02  Nue | distinct inserted in FillCbProdGrpName.
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
|=============================================================================*)
unit AskAssignForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmLabel, Mask, mmMaskEdit,
  IvMlDlgs, mmColorDialog, ExtCtrls, mmImage, mmColorButton, IvDictio,
  IvMulti, IvEMulti, mmTranslator, MMUGlobal, mmComboBox, Db,
  MMSecurity,
  AssignComp, mmButton, ADODB, mmADODataSet, mmADOConnection, mmADOCommand,
  MaGrpNrFrame, SpindleFieldsFrame;   //Added on 12.11.01 Nue

const
  cProdNameLength = 40;
resourceString
  cOverstart = '(20)(Existiert)'; //ivlm

type
  TAskAssign = class(TDialogBottom)
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    lMachine: TmmLabel;
    mmLabel3: TmmLabel;
    lSpindles: TmmLabel;
    mmLabel4: TmmLabel;
    mmImage1: TmmImage;
    mmColorButton1: TmmColorButton;
    mmLabel5: TmmLabel;
    mmTranslator1: TmmTranslator;
    bHelp: TmmButton;
    mmLabel6: TmmLabel;
    lMachieGrpNr: TmmLabel;
    Panel1: TPanel;
    lOverstart: TmmLabel;
    cbProdGrpName: TmmComboBox;
    MMSecurityControl1: TMMSecurityControl;
    mmImage2: TmmImage;
    lStyleName: TmmLabel;
    lStyle: TmmLabel;
    conDefault: TmmADOConnection;
    dseQuery1: TmmADODataSet;
    SpindleFields1: TSpindleFields;
    MaGrpNr1: TMaGrpNr;
    procedure FormShow(Sender: TObject);
    procedure mmColorButton1ChangeColor(Sender: TObject);
    procedure bHelpClick(Sender: TObject);
    procedure cbProdGrpNameChange(Sender: TObject);
  private
    fMachineName  : String;
    fSpindleFirst : String;
    fSpindleLast  : String;
    fMachGrpNr    : String;
    fStyleName    : String;  //12.11.01
    fYMSetName    : String;  //12.11.01
    fProdGrpColor : TColor;
    fMachGrpNrOverstart : boolean;
    procedure SetProdGrpName ( aName : String );
    procedure SetProdGrpColor ( aColor : TColor );
    function  GetProdGrpName :String;
  public
    constructor Create ( aOwner : TComponent ); override;
    property ProdGrpColor : TColor read fProdGrpColor write SetProdGrpColor;
    property MachineName : String read fMachineName write fMachineName;
    property SpindleFirst : String read fSpindleFirst write fSpindleFirst;
    property SpindleLast : String read fSpindleLast write fSpindleLast;
    property MachGrpNr : String read fMachGrpNr write fMachGrpNr;
    property MachGrpNrOverstart : boolean read fMachGrpNrOverstart write fMachGrpNrOverstart;
    property ProdGrpName : String read GetProdGrpName write SetProdGrpName;
    property StyleName : String read fStyleName write fStyleName;
    property YMSetName : String read fYMSetName write fYMSetName;
    function FillCbProdGrpName(aReadOnly: Boolean): Boolean;
    function GetStyleSubID(aStyleID: Integer): Integer;
  end;
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp;
{$R *.DFM}
  constructor TAskAssign.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Reinigereinstellungen_bestaetigen.htm');
    mmColorButton1.ColorArray := cMMProdGroupColors;
  end;
//------------------------------------------------------------------------------
  procedure TAskAssign.SetProdGrpName ( aName : String );
  begin
//    edProdGrpName.Text := aName;
    cbProdGrpName.Text := aName
  end;
//------------------------------------------------------------------------------
  procedure TAskAssign.SetProdGrpColor ( aColor : TColor );
  begin
    if ( aColor = cDefaultStyleIDColor ) then
      // do not display system colors
      fProdGrpColor := clBlue
    else
      fProdGrpColor := aColor;
  end;
//------------------------------------------------------------------------------
  function TAskAssign.GetProdGrpName:String;
  begin
    Result := Trim(cbProdGrpName.Text);
  end;
//------------------------------------------------------------------------------

  function TAskAssign.FillCbProdGrpName(aReadOnly: Boolean): Boolean;
  begin
    cbProdGrpName.Clear;
    if aReadOnly then begin
      cbProdGrpName.Style :=csSimple;
      cbProdGrpName.Enabled := False
    end
    else begin
      cbProdGrpName.Style :=csDropDown;
      cbProdGrpName.Enabled := True;
      try
        with dseQuery1 do begin
          Close;
//          SQL.Text := 'select * from t_prodgroup where datediff(day,c_prod_start,getdate())<20 order by c_prod_start desc';
          CommandText := 'select distinct(c_prod_name) from t_prodgroup order by c_prod_name';
          Open;
          while not EOF do begin  // ADO Conform
            cbProdGrpName.Items.Append(FieldByName ( 'c_prod_name' ).AsString);
            Next;
          end;
        end;
      except
        on e:Exception do begin
          raise Exception.Create ( 'TAskAssign.FillCbProdGrpName failed. ' + e.Message );
        end;
      end;
    end;
    Result := True;
  end;
//------------------------------------------------------------------------------

  function TAskAssign.GetStyleSubID(aStyleID: Integer):Integer;
  begin
    try
//      mmQuery.Database.StartTransaction;
      with TmmADOCommand.Create(nil)do begin
        try
          Connection := dseQuery1.Connection;
          CommandText := 'Update t_style set c_max_subID=(c_max_subID+1) where c_style_id=:c_style_id ';
          Parameters.ParamByName ( 'c_style_id' ).value := aStyleID;
          Execute;
        finally
          Free;
        end;// try finally
      end;// with TmmADOCommand.Create(nil)do begin

      with dseQuery1 do begin
        Close;
(*        CommandText := 'Update t_style set c_max_subID=(c_max_subID+1) where c_style_id=:c_style_id ';
        Parameters.ParamByName ( 'c_style_id' ).value := aStyleID;
        Open;*)
        CommandText := 'Select c_max_subID from t_style where c_style_id=:c_style_id ';
        Parameters.ParamByName ( 'c_style_id' ).value := aStyleID;
        Open;
        Result := FieldByName ( 'c_max_subID' ).AsInteger;
      end; //with
//      mmQuery.Database.Commit;
    except
      on e:Exception do begin
//        mmQuery.Database.Rollback;
        Result := 0;
        raise Exception.Create ( 'TAskAssign.GetStyleSubID failed. ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------

  procedure TAskAssign.FormShow(Sender: TObject);
  begin
    inherited;
    lMachine.Caption     := fMachineName;
    lSpindles.Caption    := fSpindleFirst + ' - ' + fSpindleLast;
    lMachieGrpNr.Caption := fMachGrpNr;

    //Nue:13.11.01
    if fStyleName<>'' then begin
      lStyle.Caption := cStyle;
      lStyleName.Caption   := fStyleName;
    end
    else begin
      lStyle.Caption := cTemplate;
      lStyleName.Caption := fYMSetName;
    end;



    mmColorButton1.Color := fProdGrpColor;
    if fMachGrpNrOverstart then
      lOverstart.Caption := cOverstart
    else
      lOverstart.Caption := '';

//    if NOT((gMMSetup.IsPackageLabMaster) or (gMMSetup.IsPackageDispoMaster)) then begin
//     lStyle.Enabled := False;
//     lStyleName.Enabled := False;
//    end;
    if cbProdGrpName.Enabled then
      cbProdGrpName.SetFocus
    else
      bOk.SetFocus;  
  end;
//------------------------------------------------------------------------------
  procedure TAskAssign.mmColorButton1ChangeColor(Sender: TObject);
  begin
    inherited;
    fProdGrpColor := mmColorButton1.Color;
  end;
//------------------------------------------------------------------------------
  procedure TAskAssign.bHelpClick(Sender: TObject);
  begin
    inherited;
    Application.HelpCommand( 0, self.HelpContext );
  end;
//------------------------------------------------------------------------------
procedure TAskAssign.cbProdGrpNameChange(Sender: TObject);
var
 xStr : string;
begin
//  inherited;
  if StrLen(PChar(cbProdGrpName.Text))>=cProdNameLength then begin
    Beep;
{
    xStr := cbProdGrpName.Text;
    xStr[cProdNameLength] := Char(0);
    cbProdGrpName.Text := xStr;
    cbProdGrpName.Cursor.
{}
//    cbProdGrpName.Text.
  end;
end;

end.
