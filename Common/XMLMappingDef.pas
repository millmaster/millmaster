{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MappingDef.pas
| Projectpart...: MillMaster 2000
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains common data types and definitions for XML parts in Millmaster
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.04.2004  1.00  Nue | File created
| 14.07.2004        Nue | Erweiterungen: cMapNames, ptIgnore
| 28.10.2004        Nue | Erweiterungen: cLoepfeConverterVersion
|==========================================================================================}
unit XMLMappingDef;          

interface

uses
  XMLGlobal;

const
  cCutFlag               = 'C';
  cUncutFlag             = 'U';

  cTrueXML               = 'true';
  cFalseXML              = 'false';
  cXMLLanguage           = 'xml';
  cLoepfeProlog          = 'version="1.0" encoding="UTF-8"';
  cLoepfeTextComment     = 'Gebr&#252;der Loepfe AG, Switzerland, Copyright &#169; 2004'#13#10'';
  cLoepfeVersion         = 'LoepfeSoftwareVersion';
  cLoepfeSoftwareAttr    = 'MapFile';
  cLoepfeConverterVersion= 'ConverterVersion';
  cConverterProlog       = 'MillMaster';
  cPIConverter           = cLoepfeSoftwareAttr + '="%s" ' + cLoepfeConverterVersion + '="%s"';
  cLoepfeAdditionalInfo  = 'Additional LOEPFE Info: Creation date:';
//  cLoepfeConverterVersion= 'MMXMLConverter version:';

  (* Name des speziellen Millmaster Mapfiles.
     Diese Mapfile wird nie erzeugt, sondern gilt als globales Mapfile dessen
     einstellungen (im Moment nur Events) f�r alle anderen Mapfiles gelten. *)
  cMillmasterMap = 'millmaster';

  (* Bezeichnet in einer Enumeration den Wert der f�r alle nicht vertretenen Werte benutzt werden soll.
     Dies Betrifft eine Enummeration in der 255 Off bedeutet und alle andern WErte On. *)
  cEnumOthers = 'others';

(*---------------------------------------------------------
  MAP File
 ----------------------------------------------------------*)
  //XML Elemente
  cEleLoepfeBody          = 'LoepfeBody';
  cEleYMSetting           = 'YMSetting';
  cEleMaConfig            = 'MaConfig';
  cEleMapMachine          = 'MapMachine';

//  cEleMapYMSetting        = 'MapYMSetting';
//  cEleMapMaConfig         = 'MapMaConfig';
  cEleEvent               = 'event';

  //XML Attribute
    //Loepfe global
    cAtrStructSize          = 'structSize';
    //Loepfe:map
    cAtrVersion             = 'version';
    cAtrOffset              = 'offset';
    cAtrType                = 'type';
    cAtrDefault             = 'def';
    cAtrCount               = 'count';
    cAtrDirection           = 'dir';
    cAtrDescription         = 'desc';

    //Loepfe:convert
    cAtrConvert             = 'conv';
    cAtrParameter           = 'para';

    //Loepfe:facet
    cAtrMinIncl             = 'minIncl';
    cAtrMinExcl             = 'minExcl';
    cAtrMaxIncl             = 'maxIncl';
    cAtrMaxExcl             = 'maxExcl';

    //Loepfe:bitfield
    cAtrBitOffset           = 'bitOffset';
    cAtrBitCount            = 'bitCount';

    // Loepfe:ConfigCode
    // Legt fest, dass das Element einen ConfigCode repr�sentiert
    cAtrIsConfigCode = 'IsConfigCode';
    // Inhalt: 'A', 'B', 'C' oder 'D'
    cAtrConfigCode   = 'configCode';
    // Bitnummer im entsprechenden ConfigCode
    cAtrBitNr        = 'bitNr';

    //Loepfe:event
    cAtrElement             = 'element';
    cAtrMode                = 'mode';
    cAtrDeleteFlag          = 'del';

    //Loepfe:enum
    cAtrEnum                = 'enum';

  //MapSections
//  cMapYMSetting   = 0;
//  cMapMaConfig    = 1;

type
  TMapSection = (msYMSetting, msMaConfig, msMachine);
//  TMapSection = cMapYMSetting..cMapMaConfig;

 (*---------------------------------------------------------
  XML MM Schema
 ----------------------------------------------------------*)
const
  // Name des annotation Elements (mit Namespace)
  cNSAnnotation    = cSNS + 'annotation';
  // Name des appinfo Elements (mit Namespace)
  cNSAppinfo       = cSNS + 'appinfo';
  // Name des documentation Elements (mit Namespace)
  cNSDocumentation = cSNS + 'documentation';

  // Name des Elements f�r die Enumerationen
  cEleEnum = cLNS + cAtrEnum;
    // name einer Enummeration oder eines Elements
    cAtrEnumName  = 'name';
    // Liste der mit den einzelnen Werten
    cAtrEnumValue = 'value';

  // Element f�r die mapping Informationen
  cEleMap = cLNS + 'map';
    // MapID
    cAtrMapVersion = cAtrVersion;
    // Gr�sse der Bin�rstruktur
    cAtrMapStructSize = cAtrStructSize;
    // Beschreibung (nur LoepfeBody)
    cAtrMapDescription = 'desc';
    // Kategorie (nur LoepfeBody)
    cAtrMapCategory = 'category';
    // Offset
    cAtrMapOffset = cAtrOffset;
    // Datentyp
    cAtrMapType = cAtrType;
    // Defaultwert
    cAtrMapDefault = 'default';
    // Anzahl Wiederholungen (Datentyp char)
    cAtrMapCount = cAtrCount;
    // Gibt die Konvertierungsrichtung an (nur Upload oder Up/Download)
    cAtrMapDirection = cAtrDirection;
    // Delphi Konstante (wird gebraucht um f�r jedes Element eine Konstante mit dem XPath zu extrahieren
    cAtrMapDelphiConstName = 'ConstName';
    // Flags um das Element zu Kennzeichnen
    cAtrMapFlag = 'Flag';

  // Convert Element f�r die Konvertierungslogik
  cEleConvert = cLNS + 'convert';
    // Modus f�r die Berechnung (Formula, UC, ..)
    cAtrConvertMode = 'mode';
    // Parameter Attribut
    cAtrConvertPara = cAtrParameter;

  // Wertbereiche
  cEleFacet = cLNS + 'facet';
    // Untermenge der XML Facets
    cAtrFacetName = 'name';
    cAtrFacetValue = 'value';

  // Bitfelder
  cEleBitfield = cLNS + 'bitfield';
    (* Offset an dem das Bitfeld beginnt. Der Offset bezieht sich immer auf den Datentyp
       maximale Offset f�r ein Byte = 7, f�r ein Word = 15 usw. *)
    cAtrBitfieldBitOffset = cAtrBitOffset;
    // Anzahl der Bits f�r das entsprechende Bitfield
    cAtrBitfieldBitCount = cAtrBitCount;

  // ConfigCode
  cEleConfigCode = cLNS + cAtrIsConfigCode;
    // Legt fest, dass das Element einen ConfigCode repr�sentiert
    cAtrConfigCodeIsConfigCode = cAtrIsConfigCode;
    // Inhalt: 'A', 'B', 'C' oder 'D'
    cAtrConfigCodeConfigCode   = cAtrConfigCode;
    // Bitnummer im entsprechenden ConfigCode
    cAtrConfigCodeBitNr        = cAtrBitNr;
    // True, wenn der ConfigCode invertiert werden muss (wird nur im Defaults MMXML gebraucht)
    cAtrConfigCodeInverted     = 'ConfigCodeInverted';

  // Events
  cEleEventSchema = cLNS + 'event';
    // XPath der ein Element selektiert
    cAtrEventElement    = cAtrElement;
    // Modus interner oder externer Event
    cAtrEventMode       = cAtrMode;
    // Richtung in der der Event angewendet wird
    cAtrEventDirection  = cAtrDirection;
    // L�schflag. Das Element wird nach der Konvertierung (Bin-->XML) gel�scht
    cAtrEventDeleteFlag = cAtrDeleteFlag;

  // XPath
    // Pfad zur appinfo eines Elements
    cXPathAppinfo = cNSAnnotation + '/' + cNSAppinfo;
    // Findet die Documentation vom Element appinfo aus
    cXPathDocumentation = cNSAnnotation + '/' + cNSDocumentation;
    // Selektiert ein Map Element ausgehend vom appinfo Element
    cXPathNamedMap = 'loepfe:map[@version=''%s'']';

(*---------------------------------------------------------
  MMXML Handling
 ----------------------------------------------------------*)
  cMMXMLDummy      = '<?xml version="1.0" encoding="UTF-8"?><LoepfeBody/>';
  cXMLNameSettings = 'Settings';
  cXMLNameFPattern = 'FPattern';

type
  TDataType = (dtNone,
               dtChar,
               dtByte,
               dtShortint,
               dtWord,
               dtSmallint,
               dtDWord,
               dtLongint,
               dtSingle,
               dtDouble,
               dtUCBit,
               dtUCChar,
               dtUTF16,
               dtIgnore   //dtIgnore: Datentyp wird ignoriert (nur f�r das Mapfile relevant)
               ); // TDataType

  TBasicTypeNames = Array[TDataType] of string;
  TBasicTypeDimensions = Array[TDataType] of integer;

const
  cConvertNone    = 0;
  cConvertFormula = 1;
  cConvertNot     = 2;

  cOperatorNone   = 0;
  cOperatorAdd    = 1;
  cOperatorMul    = 2;

  cFacetNone      = 0;
  cFacetMinIncl   = 1;
  cFacetMinExcl   = 2;
  cFacetMaxIncl   = 3;
  cFacetMaxExcl   = 4;

  cDirBoth        = 0;
  cDirUp          = 1;
  cDirDown        = 2;

  cEventInt       = 0;
  cEventExt       = 1;

const
  cBasicTypeNames: TBasicTypeNames = ('', 'char', 'byte', 'shortint', 'word', 'smallint', 'dword',
                                      'longint', 'single', 'double', 'ucbit', 'ucchar', 'UTF16', 'ignore');
  cBasicTypeDimensions: TBasicTypeDimensions = (0, 1, 1, 1, 2, 2, 4, 4, 4, 8, 1, 1, 2, 0);

  cFunctionNames  : Array[cConvertNone..cConvertNot] of String = ('', 'formula', 'not');
  cOperatorNames  : Array[cOperatorNone..cOperatorMul] of String = ('', 'add', 'mul');
  cFacetNames     : Array[cFacetNone..cFacetMaxExcl] of String = ('', 'minIncl', 'minExcl', 'maxIncl', 'maxExcl');
  cEventNames     : Array[cEventInt..cEventExt] of String = ('int', 'ext');
  cDirNames       : Array[cDirBoth..cDirDown] of String = ('both', 'up', 'down');
  // Selektionspfade zur entsprechenden Map Sektion
  cMapSectionNames: Array[TMapSection] of String = ('/'+cEleLoepfeBody+'/'+cEleYMSetting, '/'+cEleLoepfeBody+'/'+cEleMaConfig, '/'+cEleLoepfeBody+'/'+ cEleMapMachine);
//  cMapNames     : Array[TMapSection] of String = ('/'+cEleLoepfeBody+'/'+cEleMapYMSetting, '/'+cEleLoepfeBody+'/'+cEleMapMaConfig);

implementation

end.

