(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FixBorderFrame
| Projectpart...: MillMaster Spulerei
| Subpart.......: Berechnungsmethoden
| Process(es)...: -
| Description...: GUI f�r die Berechnungsmethode 'FixBorder'
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows NT/2000/XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 16.12.2003  1.00  Lok | Initial release
|=============================================================================*)
unit FixBorderFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CalcMethodBaseFrame, StdCtrls, mmLineLabel, mmEdit, mmLabel, CalcMethods,
  IvDictio, IvMulti, IvEMulti, mmTranslator, LoepfeGlobal, SettingsReader, ClassDef;

type
  (*: Klasse:        TfrmFixBorderFrame
      Vorg�nger:     TfrmCalcMethodBaseFrame
      Kategorie:     No category
      Kurzbeschrieb: GUI f�r die Settings f�r die FixBorder Methode 
      Beschreibung:  
                     - *)
  //1 GUI f�r die Settings f�r die FixBorder Methode 
  TfrmFixBorderFrame = class(TfrmCalcMethodBaseFrame)
    edLimitValue: TmmEdit;
    laLimitValueUnit: TmmLabel;
    mmLabel1: TmmLabel;
  protected
    //1 F�gt die Methoden Settings zur String Liste hinzu 
    procedure GetInternalMethodSettings; override;
    //1 Zeigt die Methodensettings im GUI an 
    procedure SetInternalMethodSettings; override;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent; aDataItem: TBaseDataItem); overload; override;
  end;
  

implementation

{$R *.DFM}

(*:
  Klasse:        TfrmFixBorderFrame
 *  Vorg�nger:     TfrmCalcMethodBaseFrame
 *  Kategorie:     No category
 *  Kurzbeschrieb: GUI f�r die Settings f�r die FixBorder Methode 
 *  Beschreibung:  - 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmFixBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner, aDataItem)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmFixBorderFrame.Create(aOwner: TComponent; aDataItem: TBaseDataItem);
begin
  mCalcMethodType := cmFix;

//: ----------------------------------------------
  inherited Create(aOwner, aDataItem);
end;// TfrmFixBorderFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInternalMethodSettings
 *  Klasse:           TfrmFixBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�gt die Methoden Settings zur String Liste hinzu
 *  Beschreibung:     
                      Die StringListe wird in der Basisklasse verwaltet.
 --------------------------------------------------------------------*)
procedure TfrmFixBorderFrame.GetInternalMethodSettings;
begin
  inherited GetInternalMethodSettings;

//: ----------------------------------------------
  mMethodParam.Values[cFixBorderLimitValue] := MMFloatToStr(edLimitValue.AsFloat);
end;// TfrmFixBorderFrame.GetInternalMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetInternalMethodSettings
 *  Klasse:           TfrmFixBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zeigt die Methodensettings im GUI an
 *  Beschreibung:     
                      Die StringListe wird in der Basisklasse verwaltet.
 --------------------------------------------------------------------*)
procedure TfrmFixBorderFrame.SetInternalMethodSettings;
var
  xLenBase: TLenBase;
  xCalcMode: Integer;
begin
  inherited SetInternalMethodSettings;

//: ----------------------------------------------
  (* String  in einen Float verwandeln um den
     L�nderspezifischen Dezimaltrenner in den String einzuf�gen *)
  edLimitValue.AsFloat := MMStrToFloat(mMethodParam.ValueDef(cFixBorderLimitValue, 0));
  
  // Initialisieren
  xLenBase := lb100KM;
  
  if assigned(mDataItem) then
    xCalcMode := mDataItem.CalcMode
  else
    // Kalkulationsmodus aus dem String lesen
    xCalcMode := mMethodParam.ValueDef(cCalcMode, MAXINT);
  
  // Die L�ngenbasis ist nur relevant, wenn der CalcMode = 0 ist (> = L�ngenbasis in Metern; < 0 = Ohne Einheit)
  if xCalcMode = 0 then begin
    // Wenn eine L�ngenbasis definiert ist, diese nehmen, sonst die globale L�ngenbasis holen
    if (mMethodParam.IndexOfName(cLenBase) >= 0) then begin
      xLenBase := mMethodParam.Values[cLenBase];
    end else begin
      with TMMSettingsReader.Instance do begin
        // Initialisieren soweit notwendig
        if init then
          xLenBase := TLenBase(Value[cMMUnit]);
      end;// with TMMSettingsReader.Instance do begin
    end;// if (mMethodParam.IndexOfName(cLenBase) >= 0) then begin
    laLimitValueUnit.Caption := GetLenBaseString(xLenBase);
  end else begin
    laLimitValueUnit.Caption := GetLenBaseString(xCalcMode);
  end;// if xCalcMode = 0 then begin
end;// TfrmFixBorderFrame.SetInternalMethodSettings cat:No category

initialization
  RegisterClass(TfrmFixBorderFrame);

end.


