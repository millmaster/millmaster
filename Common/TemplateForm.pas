(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: TemplateForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 04.01.2000  1.00  Mg  | Projekt started
| 08.05.2000  1.01  Mg  | Help Button inserted
| 08.07.2000  1.02  Mg  | Copy button nur enabled wenn Vorlagen vorhanden
| 12.07.2000  1.03  Mg  | Print inserted
| 17.04.2001  1.04  Wss | RefreshTemplateList moved to Public because of MMStyle access
| 15.05.2001  1.05  Wss | - ReadOnly Parameter in Init methode added to prevent of editing
                          - CheckActionState implemented for Enabling/Disabling action states
| 15.08.2001  1.06  Nue | Handling of c_head_class added. cobTemplatesDrawItem added.
| 03.09.2001  1.07  Nue | Refresh after Save added.
| 04.09.2001  1.08  Nue | CheckChanged added and within MMSecurityControl1.CanEnabled(acSave) added.
| 10.09.2001  1.09  Wss | RestoreFormLayout called in Create
| 17.12.2001  1.10  Wss | Icons changed
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 02.12.2002  1.11  Nue | Settings1.CurveSelection := not mReadOnly;  added.
| 12.03.2003  1.12  Nue | ChannelVisibility added in acPrintExecute. (Bug fix!)
| 10.12.2003  1.13  Nue | L�schen Typ TTemplateItem der Typ wird von AssignComp verwendet.(Nue)
|                       | SaveSettingsAsTemplate(aColor: TColor; aItem: TBaseItem) und Templatehandling im Zu-
|                       | sammenhang mit Artikel (TSaveTemplateDLGForm) eingef�gt.
| 02.02.2004  1.14  Nue | Anpassungen, in acNew.. , wenn noch kein Template auf DB ist.
| 04.02.2004  1.15  Nue | Verhindern, dass DefaultMemoryC gel�scht werden kann.
| 07.06.2004  1.15  Nue | caFree nicht nur im else-Fall in FormClose (sonst bleibt Assignments im OLE-Mode bestehen.
| 07.06.2004  1.15  SDo | Zuweisung Property PrintBlackWhite von Matrix, Func. acPrintExecute()
| 23.12.2004  2.00  Nue | Modifikationen wegen Umbau auf XML V5.0.
| 18.01.2005  2.01  SDo | Printouts Anpassungen an XML Vers. 5.0
| 05.07.2006  2.02  Nue | P-Channel und YarnCount visible geschaltet.
| 12.08.2008  2.03  Nue | Kleine Aenderung wegen Spleissklassierung in acTempChangedExecute.
|=============================================================================*)
unit TemplateForm;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IvDictio, IvMulti, IvEMulti, mmTranslator,
  ExtCtrls, mmPanel, ToolWin, ComCtrls, mmToolBar, ActnList,
  mmActionList, Buttons, mmSpeedButton, Db,
  StdCtrls, mmComboBox, AssignComp, mmColorButton, MMSecurity, MMUGlobal, LoepfeGlobal,
  YMParaDef, BASEFORM, YMSettingsGUI, mmAdoConnection, ADODB, mmADODataSet, //Nue:14.8.01
  SaveTemplateDLG, XMLSettingsModel, VCLXMLSettingsModel, SettingsFrame;  //Nue: 15.12.03

resourcestring
  cSaveQuestionMsg =
    '(*)Wollen Sie die geaenderte Vorlage speichern?'; // ivlm

  cDeleteQuestionMsg =
    '(*)Soll %s %s geloescht werden?'; //ivlm

  cSaveFailed =
    '(*)Speichern von Vorlage misslungen.'; // ivlm

  cDeleteFailed =
    '(*)Loeschen von Vorlage misslungen.'; // ivlm

  cCopyFailed =
    '(*)Kopieren von Vorlage misslungen.'; // ivlm

  cNewFailed =
    '(*)Anlegen einer neuen Vorlage misslungen.'; // ivlm

  cExistingReferences =
    '(*)Die Vorlage %s %s kann nicht geloescht werden! %d Artikel referenzieren noch darauf.'; //ivlm

//  cSaveNotAllowedMsg =
//    '(*)Die Vorlage %s %s kann nicht gespeichert werden! Es besteht bereits eine Vorlage mit gleichem Namen und gleicher Tastkopf-Klasse.'; //ivlm
//
type
//..............................................................................
//  TTemplateItem = class(TObject)
//  private
//    fSettingsID: integer;
//    fName: string;
//    fColor: TColor;
//    fHeadClass: TSensingHeadClass; //Nue:14.8.01
//  public
//    property SettingsID: integer read fSettingsID write fSettingsID;
//    property Name: string read fName write fName;
//    property Color: TColor read fColor write fColor;
//    property HeadClass: TSensingHeadClass read fHeadClass write fHeadClass; //Nue 13.8.01
//  end;
//..............................................................................
  TTemplate = class(TmmForm)
    mmToolBar1: TmmToolBar;
    mmSpeedButton1: TmmSpeedButton;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acNew: TAction;
    acSave: TAction;
    acDelete: TAction;
    acImport: TAction;
    bEditSettings: TmmSpeedButton;
    bRename: TmmSpeedButton;
    bSave: TmmSpeedButton;
    bDelete: TmmSpeedButton;
    bImport: TmmSpeedButton;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    cobTemplates: TmmComboBox;
    ToolButton3: TToolButton;
    acNext: TAction;
    acPrev: TAction;
    bPrev: TmmSpeedButton;
    bNext: TmmSpeedButton;
    acTempChanged: TAction;
    acDataChanged: TAction;
    bColor: TmmColorButton;
    ToolButton4: TToolButton;
    MMSecurityControl1: TMMSecurityControl;
    ToolButton5: TToolButton;
    mmSpeedButton2: TmmSpeedButton;
    acSecurity: TAction;
    acHelp: TAction;
    mmSpeedButton3: TmmSpeedButton;
    ToolButton6: TToolButton;
    acPrint: TAction;
    mmSpeedButton4: TmmSpeedButton;
    ToolButton7: TToolButton;
    Settings1: TSettings;
    mmTranslator: TmmTranslator;
    acEditSettings: TAction;
    acRename: TAction;
    pBackColor: TmmPanel;
    pColor: TmmPanel;
    bNew: TmmSpeedButton;
    VCLXMLSettingsModel1: TVCLXMLSettingsModel;
    procedure acExitExecute(Sender: TObject);
    procedure acEditSettingsExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure acNextExecute(Sender: TObject);
    procedure acPrevExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure acTempChangedExecute(Sender: TObject);
    procedure acDataChangedExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure acPrintExecute(Sender: TObject);
    procedure cobTemplatesDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure acRenameExecute(Sender: TObject);
    procedure acNewExecute(Sender: TObject);
    procedure VCLXMLSettingsModel1Changed(Sender: TObject);
  private
    mDisablecobChooser: Boolean; //Nue:13.1.04
    mReadOnly: Boolean;
    mTemplateChanged: boolean;
    mFirst: boolean;
    mLast: boolean;
    mActTemplate: TTemplateItem;
    mIsDefaultSet: Boolean;  //Nue:4.2.04
    dseQuery: TmmAdoDataSet;

    procedure CheckActionState;
    procedure NavChanged;
    procedure Nav(aNext: boolean);
    procedure ToScreen;
    procedure OnChange(aSender: TUserChanges; aChanged: boolean);
    procedure CheckChanged; //Nue: 4.9.01
    procedure ModifySettings(aColor: TColor; aItem: TBaseItem; aCallType: TCallType; aVisualType: TTemplateType);
  public
    constructor Create(aOwner: TComponent; aDataBase: TmmAdoConnection); reintroduce; virtual;
    procedure Init(aReadOnly: Boolean = False; aDisablecobChooser: Boolean= False);
    procedure RefreshTemplateList(aTempName: string); overload;
    procedure RefreshTemplateList(aTempID: Integer); overload;
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, MMHtmlHelp,

  Printers, BaseGlobal, PrintTemplateForm, PrintSettingsTemplateForm, XMLDef, YMBasicProdParaBox;

const
  cSettingsHeight = 570;
  cSettingsWidth = 1020;
const
  cGetAllTemplates =
    'select c_YM_set_id, c_YM_set_name, c_color ' +
    ',c_head_class ' + //Nue:14.08.01
    'from t_xml_ym_settings ' +
    'where c_template_set = 1 ' +
    'order by c_ym_set_name, c_head_class';
//------------------------------------------------------------------------------
  cQryCheckTemplateReferences =
    'select anz=count(*) from t_style_settings where c_ym_set_id=:c_ym_set_id ';

////------------------------------------------------------------------------------
//  cCheckTemplateNameAndTKClassAvailability =
//    'select c_YM_set_name from t_ym_settings ' +
//    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name and c_head_class =:c_head_class and c_YM_set_id <>:c_YM_set_id ';
 { TTemplate }
//------------------------------------------------------------------------------
constructor TTemplate.Create(aOwner: TComponent; aDataBase: TmmAdoConnection);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_Fenster_Vorlagenverwaltung.htm');
  dseQuery := TmmADODataSet.Create(self);
//    Width                     := 1020;
//    ClientHeight              := 647;
//    Position                  := poScreenCenter;
  Width := cSettingsWidth;
  ClientHeight := mmToolbar1.Height + cSettingsHeight;
  if OLEMode then
    RestoreFormLayout;

    (* Wird die Vorlagenverwaltung von der Floor gestartet (als Com Server)
       muss eine eigene Connection hergestellt werden. Da als Owner der Frame
       angegeben ist, wird die Connection von TTemplate wieder freigegeben. *)
  if aDataBase = nil then begin
    aDataBase := TmmADOConnection.Create(self);
    aDataBase.ConnectionString := GetDefaultConnectionString;
  end; // if aDataBase = nil then begin

  dseQuery.Connection := aDataBase;
  Settings1.GUIMode := gmTemplate;
  Settings1.Query := dseQuery;
  Settings1.OnValueChanged := OnChange;
  bColor.ColorArray := cMMProdGroupColors;

  mReadOnly := False;
  mTemplateChanged := False;
  mActTemplate := nil;
  mIsDefaultSet := False;
  mFirst := True;
  mLast := True;

  Settings1.ReadOnly := True;
  bEditSettings.Enabled := MMSecurityControl1.CanEnabled(acEditSettings);

end;
//------------------------------------------------------------------------------
procedure TTemplate.Init(aReadOnly: Boolean; aDisablecobChooser: Boolean);
begin
  try
    mDisablecobChooser := aDisablecobChooser;
    mReadOnly := aReadOnly;
    if Assigned(dseQuery.Connection) then
      RefreshTemplateList('');

    bEditSettings.Enabled := MMSecurityControl1.CanEnabled(acEditSettings) and not mReadOnly;
//    Settings1.ReadOnly := mReadOnly;
    Settings1.CurveSelection := not mReadOnly; //Nue:02.12.02
    CheckActionState;
  except
    on e: Exception do begin
      SystemErrorMsg_('TTemplate.FormShow failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.CheckChanged;
begin
// Nicht weiter ben�tigt, weil Ver�nderungen neu im Fenster SaveTemplateDLG abgehandelt werden. Nue:7.1.04
//  if MMSecurityControl1.CanEnabled(acSave) and mTemplateChanged then begin
//    if MessageDlg(cSaveQuestionMsg, mtConfirmation, [mbYes, mbNo], 0) = mrYes then
//      acSaveExecute(self);
//  end;
end;

//------------------------------------------------------------------------------

procedure TTemplate.RefreshTemplateList(aTempName: string);
var xTempItem: TTemplateItem;
begin
  try
    cobTemplates.Clear;
    with dseQuery do begin
      Close;
      CommandText := cGetAllTemplates;
      Open;
      while (not EOF) do begin // ADO Conform
        xTempItem := TTemplateItem.Create(FieldByName('c_ym_set_id').AsInteger);   //10.12.03 (Nue)
//        xTempItem := TTemplateItem.Create;
//        xTempItem.SettingsID := FieldByName('c_ym_set_id').AsInteger;
        xTempItem.Name := FieldByName('c_ym_set_name').AsString;
        xTempItem.Color := FieldByName('c_color').AsInteger;
        xTempItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger); //Nue:13.08.01
        cobTemplates.Items.AddObject(xTempItem.Name, xTempItem);
        Next;
      end;
      if cobTemplates.items.Count > 0 then begin
        if aTempName <> '' then
          cobTemplates.ItemIndex := cobTemplates.Items.IndexOf(aTempName)
        else
          cobTemplates.ItemIndex := 0;
        ToScreen;
      end
      else begin
        Settings1.New;
      end;
    end;
//      NavChanged;
  except
    on e: Exception do begin

      raise Exception.Create('TTemplate.RefreshTemplateList failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.RefreshTemplateList(aTempID: Integer);
var
  xTempItem: TTemplateItem;
  xStr: string;
begin
  try
    cobTemplates.Clear;
    xStr := '';
    with dseQuery do begin
      Close;
      CommandText := cGetAllTemplates;
      Open;
      while (not EOF) do begin // ADO Conform
        xTempItem := TTemplateItem.Create(FieldByName('c_ym_set_id').AsInteger);  //10.12.03 (Nue)
//        xTempItem := TTemplateItem.Create;
//        xTempItem.SettingsID := FieldByName('c_ym_set_id').AsInteger;
        xTempItem.Name := FieldByName('c_ym_set_name').AsString;
        xTempItem.Color := FieldByName('c_color').AsInteger;
        xTempItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger); //Nue:13.08.01
        cobTemplates.Items.AddObject(xTempItem.Name, xTempItem);
//        if aTempID = xTempItem.SettingsID then
        if aTempID = xTempItem.SetID then    //10.12.03 (Nue)
          xStr := xTempItem.Name;

        Next;
      end;
      if cobTemplates.items.Count > 0 then begin
        if xStr <> '' then
          cobTemplates.ItemIndex := cobTemplates.Items.IndexOf(xStr)
        else
          cobTemplates.ItemIndex := 0;
        ToScreen;
      end
      else begin
        Settings1.New;
      end;
    end;
//      NavChanged;
  except
    on e: Exception do begin
      raise Exception.Create('TTemplate.RefreshTemplateList failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.NavChanged;
begin
  mFirst := True;
  mLast := True;
  if cobTemplates.Items.Count <> 0 then begin
    if cobTemplates.ItemIndex > 0 then
      mFirst := False;
    if cobTemplates.ItemIndex < (cobTemplates.Items.Count - 1) then
      mLast := False;
  end;
{Wss: moved to CheckActionState
    acPrev.Enabled := not mFirst;
    acNext.Enabled := not mLast;
{}
end;
//------------------------------------------------------------------------------
procedure TTemplate.Nav(aNext: boolean);
begin
  if aNext then begin
    if cobTemplates.ItemIndex < cobTemplates.Items.Count - 1 then
      cobTemplates.ItemIndex := cobTemplates.ItemIndex + 1;
  end
  else begin
    if cobTemplates.ItemIndex > 0 then
      cobTemplates.ItemIndex := cobTemplates.ItemIndex - 1;
  end;
  ToScreen;
end;
//------------------------------------------------------------------------------
procedure TTemplate.ToScreen;
begin
  CheckChanged;
  mActTemplate := (cobTemplates.Items.Objects[cobTemplates.ItemIndex] as TTemplateItem);

  if mActTemplate.SetID=1 then  //Nue:4.2.04
    mIsDefaultSet := True
  else
    mIsDefaultSet := False;

    //Selektierter Spindlerange 0 setzten um Template zu signalisieren Nue:9.5.01
//NUE1  Settings1.SpindleFirst := 0;
//NUE1  Settings1.SpindleLast := 0;

  Settings1.LoadFromDB(mActTemplate.SetID);

  Settings1.YarnCnt := 0;
  Settings1.Speed := 0;
  Settings1.SpeedRamp := 0;
  Settings1.PilotSpindles := 0;

  bColor.Color := mActTemplate.Color;
  pColor.Color := mActTemplate.Color; //Nue:17.12.03
  mTemplateChanged := False;
end;
//------------------------------------------------------------------------------
procedure TTemplate.OnChange(aSender: TUserChanges; aChanged: boolean);
begin
  acDataChangedExecute(self);
end;
//------------------------------------------------------------------------------
procedure TTemplate.acExitExecute(Sender: TObject);
begin
  inherited;
  CheckChanged;
  Close;
end;
//------------------------------------------------------------------------------
procedure TTemplate.ModifySettings(aColor: TColor; aItem: TBaseItem; aCallType: TCallType; aVisualType: TTemplateType);
var
  xIndex: Integer;
  xStr: string;
begin
  try
    xStr := '';
    xIndex := cobTemplates.ItemIndex;
    xStr := Settings1.SaveSettingsAsTemplate(aColor, aItem, aCallType, aVisualType);
    RefreshTemplateList(xStr);  //Neu
    if (xStr='') and ({Nue:2.2.04}xIndex>=0) then begin
      cobTemplates.ItemIndex := xIndex;
      ToScreen;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TTemplate.ModifySettings' + e.Message);
    end;
  end;

end;
//------------------------------------------------------------------------------


procedure TTemplate.acEditSettingsExecute(Sender: TObject);
begin
  //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  Settings1.ReadOnly := not bEditSettings.Down;
//  inherited;
//  try
//    CheckChanged;
//
//    Settings1.New;
//    mTemplateChanged := False; //Nue: 0.5.01
//    RefreshTemplateList(Settings1.SaveSettingsAsTemplate(0));
//  except
//    on e: Exception do begin
//      SystemErrorMsg_(cNewFailed + e.Message);
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acRenameExecute(Sender: TObject);
begin
  inherited;
  try
    ModifySettings(mActTemplate.Color, mActTemplate, tcTemplate, ttRenameTemplateOnly);
  except
    on e: Exception do begin
      SystemErrorMsg_(cSaveFailed + e.Message);
    end;
  end;

end;


//------------------------------------------------------------------------------

procedure TTemplate.acNewExecute(Sender: TObject);
var xTemplate: TTemplateItem;
begin
  inherited;
  try
    if NOT Assigned(mActTemplate) then begin //Nue:2.2.04
      xTemplate := TTemplateItem.Create(0);
      ModifySettings(xTemplate.Color, xTemplate, tcTemplate, ttNewTemplateOnly);
    end
    else begin
      ModifySettings(mActTemplate.Color, mActTemplate, tcTemplate, ttNewTemplateOnly);
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_(cSaveFailed + e.Message);
    end;
  end;

end;
//------------------------------------------------------------------------------
procedure TTemplate.acSaveExecute(Sender: TObject);
begin
  inherited;
    // Save changes
  try

////Nue:15.8.01
//    with dseQuery do begin
//      Close;
//      CommandText := cCheckTemplateNameAndTKClassAvailability;
//      Parameters.ParamByName('c_YM_set_name').value := mActTemplate.Name;
//      Parameters.ParamByName('c_head_class').value := ORD(mActTemplate.HeadClass);
//      Parameters.ParamByName('c_YM_set_id').value := mActTemplate.SetID;
//      Open;
//      if not EOF then begin // ADO Conform
//        if MessageDlg(cSaveNotAllowedMsg, mtWarning, [mbOk], 0) = mrOk then
//          Exit;
//      end;
//    end;
//
//    mActTemplate.Color := bColor.Color;
//    Settings1.Update(mActTemplate.SetID);
//
//
////    Settings1.UpdateColor(mActTemplate.SetID, bColor.Color);   //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@2
//    mTemplateChanged := False;



      //Refresh List and focus on the modified item  Nue:3.9.01
    mTemplateChanged := False;  //Neu
    ModifySettings(mActTemplate.Color, mActTemplate, tcTemplate, SaveTemplateDLG.ttNone);

  except
    on e: Exception do begin
      SystemErrorMsg_(cSaveFailed + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acDeleteExecute(Sender: TObject);
begin
  inherited;
    // Delete Template
  try
    dseQuery.Close;
    dseQuery.CommandText := cQryCheckTemplateReferences;
    dseQuery.Parameters.ParamByName('c_ym_set_id').value := mActTemplate.SetID;
    dseQuery.Open;
    if not dseQuery.EOF then // ADO Conform
      if (dseQuery.FieldByName('anz').AsInteger <> 0) then
        if MessageDlg(Format(cExistingReferences, [mActTemplate.Name, cGUISensingHeadClassNames[mActTemplate.HeadClass], dseQuery.FieldByName('anz').AsInteger]), mtWarning, [mbOk], 0) = mrOk then
          EXIT;

    if MessageDlg(Format(cDeleteQuestionMsg, [mActTemplate.Name, cGUISensingHeadClassNames[mActTemplate.HeadClass]]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      Settings1.Delete(mActTemplate.SetID);
      mTemplateChanged := False;
      RefreshTemplateList('');
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_(cDeleteFailed + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acImportExecute(Sender: TObject);
begin
  inherited;
    // Import Template
end;
//------------------------------------------------------------------------------
procedure TTemplate.acNextExecute(Sender: TObject);
begin
  inherited;
    // Next Template
  Nav(True);
end;
//------------------------------------------------------------------------------
procedure TTemplate.acPrevExecute(Sender: TObject);
begin
  inherited;
    // Prev Template
  Nav(False);
end;
//------------------------------------------------------------------------------
procedure TTemplate.FormShow(Sender: TObject);
begin
  inherited;
end;
//------------------------------------------------------------------------------
procedure TTemplate.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  Handled := True;

  NavChanged;
  CheckActionState;
{
    acNew.Enabled    := not mReadOnly and MMSecurityControl1.CanEnabled(acNew);
    acCopy.Enabled   := not mReadOnly and MMSecurityControl1.CanEnabled(acCopy);
    acDelete.Enabled := not mReadOnly and MMSecurityControl1.CanEnabled(acDelete);
    acSave.Enabled   := not mReadOnly and (cobTemplates.Items.Count > 0) and
                        MMSecurityControl1.CanEnabled(acSave) and mTemplateChanged;
    acCopy.Enabled   := not mReadOnly and (cobTemplates.Items.Count > 0) and
                        MMSecurityControl1.CanEnabled(acCopy);

    bColor.Enabled   := not mReadOnly and MMSecurityControl1.CanEnabled(bColor);

    acNext.Enabled    := not mReadOnly and MMSecurityControl1.CanEnabled(acNext);
    acPrev.Enabled    := not mReadOnly and MMSecurityControl1.CanEnabled(acPrev);
{}
end;
//------------------------------------------------------------------------------
procedure TTemplate.CheckActionState;
begin
  cobTemplates.Enabled := not mReadOnly and not mDisablecobChooser;
//  bColor.Visible := not mReadOnly;
//  pBackColor.Visible := mReadOnly;
//  pBackColor.Left := cobTemplates.Left+cobTemplates.Width; //Richtige Positionierung von pBackColor
//  pColor.Visible := True;

  acNew.Enabled := not mReadOnly and not mDisablecobChooser and MMSecurityControl1.CanEnabled(acNew);
  acDelete.Enabled := not mReadOnly and not mDisablecobChooser and MMSecurityControl1.CanEnabled(acDelete) and not mIsDefaultSet;
  acSave.Enabled := not mReadOnly and (cobTemplates.Items.Count > 0) and
    MMSecurityControl1.CanEnabled(acSave) and mTemplateChanged;

  acEditSettings.Enabled := not mReadOnly and (cobTemplates.Items.Count > 0) and
    MMSecurityControl1.CanEnabled(acEditSettings);
  acRename.Enabled := not mReadOnly and (cobTemplates.Items.Count > 0) and
    MMSecurityControl1.CanEnabled(acRename);
//  Settings1.ReadOnly := not mReadOnly;

//  bColor.Enabled := not mReadOnly and MMSecurityControl1.CanEnabled(bColor);

  acNext.Enabled := not mReadOnly and not mDisablecobChooser and not mLast and MMSecurityControl1.CanEnabled(acNext);
  acPrev.Enabled := not mReadOnly and not mDisablecobChooser and not mFirst and MMSecurityControl1.CanEnabled(acPrev);
end;
//------------------------------------------------------------------------------
procedure TTemplate.acTempChangedExecute(Sender: TObject);
begin
  inherited;
  acEditSettings.Enabled := False;  //Nue: 12.08.08 Eingef�gt damit beim Wechsel von einer Zenit zu einer Spectra-Einstellung die Spleissklassen nicht mehr gesetzt werden k�nnen!!??
  if mTemplateChanged  then
    acSaveExecute(Sender) //Aufruf des Savedialogs 13.1.04 Nue
  else
    // The Template changed
    ToScreen;

end;
//------------------------------------------------------------------------------
procedure TTemplate.acDataChangedExecute(Sender: TObject);
begin
  inherited;
//  if (cobTemplates.items.count > 0) then
//    mTemplateChanged := True;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acSecurityExecute(Sender: TObject);
begin
  inherited;
  MMSecurityControl1.Configure;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acHelpExecute(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, self.HelpContext);
end;
//------------------------------------------------------------------------------
procedure TTemplate.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if mTemplateChanged  then
    acSaveExecute(Sender); //Aufruf des Savedialogs 13.1.04 Nue
  Action := caFree;
end;
//------------------------------------------------------------------------------
procedure TTemplate.acPrintExecute(Sender: TObject);
var
  //xYMSettingsByteArr: TYMSettingsByteArr;
  xLength: Word;
  xOldCursor: TCursor;
begin
{Alt
    inherited;
    Printer.Orientation := poLandscape;
    Print;
//end Alt {}
//Neu
  with TfrmPrintSettings.Create(self) do try
//Alt bis 5.7.06    cbYarnCount.Visible := False;
    cbYarnCount.Visible := True;        //Nue:5.7.06



    if ShowModal = mrOK then begin
      with TfrmPrint.Create(Self) do try
        mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);

//NUE1        Settings1.GetScreenSettings(xYMSettingsByteArr, xLength);
{wss Settings �ber Model
        qrmChannel.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
{}
         VCLXMLSettingsModel_Print.xmlAsString := Settings1.Model.xmlAsString;

        //Nue: Added 12.03.03
        with qrmChannel do begin
          ChannelVisible := ShowClearerSettingsSet.ShowChannel;
          SpliceVisible := ShowClearerSettingsSet.ShowSplice;
          ClusterVisible := ShowClearerSettingsSet.ShowFFCluster;
          BlackWhite := ShowClearerSettingsSet.PrintBlackWhite;
        end;

        qrmSiro.BlackWhite := ShowClearerSettingsSet.PrintBlackWhite;


        //Print Settings anzeigen
        QRXMLSettings_Channel.Enabled := ShowClearerSettingsSet.ShowChannel;
        QRXMLSettings_Cluster.Enabled := ShowClearerSettingsSet.ShowCluster;
        QRXMLSettings_FCluster.Enabled := ShowClearerSettingsSet.ShowFFCluster;
        QRXMLSettings_SFI.Enabled := ShowClearerSettingsSet.ShowSFI;
        QRXMLSettings_Splice.Enabled := ShowClearerSettingsSet.ShowSplice;
        QRXMLSettings_YarnCount.Enabled := ShowClearerSettingsSet.ShowYarnCount;
        QRXMLSettings_P.Enabled         := ShowClearerSettingsSet.ShowP; //Nue:5.7.06


//        QRXMLSettings_YarnCount.YarnCount :=  Single(10);
//        QRXMLSettings_YarnCount.YarnUnit  := Settings1.Model.



{wss Settings �ber Model
        qrmSiro.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrsSplice.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrsChannel1.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrsChannel2.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
{}

{ SDO
        qrsChannel1.YarnCount := 0;

        with qrsChannel1 do begin
          ChannelVisible      := cbChannel.Checked;
          //YarnCountVisible    := cbYarnCount.Checked;
          YarnCountVisible    := FALSE;
          FaultClusterVisible := cbCluster.Checked;
          SFIVisible          := FALSE;
          FFClusterVisible    := FALSE;
          SpliceVisible       := FALSE;
        end;
        with qrsChannel2 do begin
     //     SFIVisible := cbSFI.Checked;
     //     FFClusterVisible := cbFFCluster.Checked;

          ChannelVisible      := FALSE;
          YarnCountVisible    := FALSE;
          FaultClusterVisible := FALSE;
          SFIVisible          := cbSFI.Checked;
          FFClusterVisible    := cbFFCluster.Checked;
          SpliceVisible       := FALSE;


        end;
        with qrsSplice do begin
         // SpliceVisible := cbSplice.Checked;

          ChannelVisible      := FALSE;
          YarnCountVisible    := FALSE;
          FaultClusterVisible := FALSE;
          SFIVisible          := FALSE;
          FFClusterVisible    := FALSE;
          SpliceVisible       := cbSplice.Checked;

        end;
}

        qcbTemplate.Enabled := False;
        qcbLot.Enabled := False;
        qcbStyle.Enabled := False;
        qcbOrderPos.Enabled := False;
        qcbMachine.Enabled := False;

        qcbTemplate.Enabled := True;
        qlTemplate1.Caption := cobTemplates.Text;

        xOldCursor := Screen.Cursor;
        with mQuickReport do
        try
          Screen.Cursor := crHourGlass;
          Prepare;
          Print;
        finally
          QRPrinter.Free;
          QRPrinter := nil;
          Screen.Cursor := xOldCursor;
        end;
      finally
        Free;
      end;
    end;
//      mCAConfig := CAConfig;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
//Nue:14.8.01 Begin
procedure TTemplate.cobTemplatesDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);

var xTempItem: TTemplateItem;
  xIsSelected: boolean;
  xPos: integer;
begin
  inherited;
  with (Control as TComboBox).Canvas do begin
    xIsSelected := odSelected in state;
    FillRect(Rect);
    if xIsSelected then
      Font.Color := clHighlightText
    else
      Font.Color := clWindowText;

    xPos := Rect.Left + 2;
    xTempItem := (cobTemplates.Items.Objects[Index] as TTemplateItem);
    TextOut(xPos, Rect.Top, xTempItem.Name);
    inc(xPos, 120);
    TextOut(xPos, Rect.Top, cGUISensingHeadClassNames[xTempItem.HeadClass]);
  end; //with
end;

//------------------------------------------------------------------------------


procedure TTemplate.VCLXMLSettingsModel1Changed(Sender: TObject);
begin
  if (cobTemplates.items.count > 0) then
    mTemplateChanged := True;
end;

end.

