(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmSelectProfileDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.99 | 0.01 | PW  | File created
| 05.10.02 |        LOK | Umbau ADO
|=========================================================================================*)

unit mmSelectProfileDlg;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, Tabs, DB, DBCtrls, ComCtrls,
  IvDictio, IvMulti, IvEMulti, mmImageList, ActnList, mmToolBar, mmActionList,
  mmCommonLib, MemoINI, NwCombo, mmPageControl, mmPanel, ImgList, ToolWin,
  IvMlDlgs, mmButton, BASEFORM, mmTranslator, mmListView;

type
  TmmSelectProfileEditor = class(TmmForm)
    mmTranslator1: TmmTranslator;

    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;

    mmPanel1: TmmPanel;
    PageControl1: TmmPageControl;
    tsUser: TTabSheet;
    lbProfiles: TNWListBox;
    tsAdmin: TTabSheet;
    lbAdmin: TNWListBox;

    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure acCancelExecute(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbProfilesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure PageControl1Change(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    FMemoINI: TMemoINI;
    mProfiles: tSelectProfileRecordArrayT;
    procedure EnableActions;
    procedure Init;
    function GetProfileRecord(aID: integer): tSelectProfileRecordT;
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
    function GetSelectedProfile: tSelectProfileRecordT;
  end;                                  //TmmSelectProfileEditor

var
  mmSelectProfileEditor: TmmSelectProfileEditor;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmLib;

{$R *.DFM}

procedure TmmSelectProfileEditor.acCancelExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;                                    //procedure TmmSelectProfileEditor.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.acOKExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end;                                    //procedure TmmSelectProfileEditor.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.FormShow(Sender: TObject);
begin
  Init;
  if tsUser.TabVisible then PageControl1.ActivePage := tsUser;
end;                                    //procedure TmmSelectProfileEditor.FormShow
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.lbProfilesDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
  cOffset = 5;
  cProfilePos = 20;

var
//  xIsAdminPage,
    xIsSelected: boolean;
  xID: integer;
  xItemStr: string;
  xProfile: tSelectProfileRecordT;

begin
  with (Control as TNWListBox).Canvas do begin
//    xIsAdminPage := (Control as TNWListBox) = lbAdmin;
    xIsSelected := odSelected in state;

    FillRect(Rect);

    if xIsSelected then
      Font.Color := clHighlightText
    else
      Font.Color := clWindowText;

    xID := (Control as TNWListBox).GetItemIDAtPos(index);
    xProfile := GetProfileRecord(xID);

    if xProfile.Default then
      Font.Style := [fsBold]
    else
      Font.Style := [];

    xItemStr := format('%s  (%s)', [xProfile.Profilename, xProfile.Username]);

    if xProfile.Default then
      TextOut(Rect.Left + cOffset, Rect.Top, '*');
    TextOut(Rect.Left + cProfilePos, Rect.Top, xItemStr);
  end;                                  //with (Control as TNWListBox).Canvas do
{
  with (Control as TNWListBox).Canvas do begin
    xIsAdminPage := (Control as TNWListBox) = lbAdmin;

    xIsSelected := odSelected in state;
    FillRect(Rect);
    if xIsSelected then Font.Color := clHighlightText
    else Font.Color := clWindowText;

    xID := (Control as TNWListBox).GetItemIDAtPos(index);
    xProfile := GetProfileRecord(xID);

    if xProfile.Default then Font.Style := [fsBold]
    else Font.Style := [];

    if xIsAdminPage then xItemStr := format('%s  (%s)', [xProfile.Profilename, xProfile.Username])
    else xItemStr := xProfile.Profilename;

    if xProfile.Default then TextOut(Rect.Left + cOffset, Rect.Top, '*');
    TextOut(Rect.Left + cProfilePos, Rect.Top, xItemStr);
  end;                                  //with (Control as TNWListBox).Canvas do
{}
end;                                    //procedure TmmSelectProfileEditor.lbUserDrawItem
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.PageControl1Change(Sender: TObject);
begin
  EnableActions;
end;                                    //procedure TmmSelectProfileEditor.PageControl1Change
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.EnableActions;
begin
  tsUSer.TabVisible := lbProfiles.Items.Count > 0;
  if tsUSer.TabVisible then lbProfiles.ItemIndex := 0;

  tsAdmin.TabVisible := False;
//  tsAdmin.TabVisible := lbAdmin.Items.Count > 0;
//  if tsAdmin.TabVisible then lbAdmin.ItemIndex := 0;

  acOK.Enabled := tsUSer.TabVisible{ or tsAdmin.TabVisible};
end;                                    //procedure TmmSelectProfileEditor.EnableActions
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.Init;
var
  i: integer;
  xList: TStringList;

begin
  tsUser.Caption := FMemoINI.Username;

  xList := TStringList.Create;
  xList.AddStrings(FMemoINI.GetFormProfiles);

  try
    if xList.Count > 0 then begin
      SetLength(mProfiles, xList.Count);
      for i := 0 to xList.Count - 1 do begin
        mProfiles[i].ID := i;
        mProfiles[i].Profilename := GetSubStr(xList[i], cListDelimiterChar, 1);
        mProfiles[i].Username := GetSubStr(xList[i], cListDelimiterChar, 2);
        mProfiles[i].Default := GetSubStr(xList[i], cListDelimiterChar, 3) = '1';
        mProfiles[i].IsAdmin := GetSubStr(xList[i], cListDelimiterChar, 4) = '1';

        if (FMemoINI.Username = mProfiles[i].Username) or mProfiles[i].IsAdmin then
        lbProfiles.AddObj(mProfiles[i].Profilename, mProfiles[i].ID);
//        lvProfiles.AddObj(mProfiles[i].Profilename, mProfiles[i].ID);

{
        if FMemoINI.Username = mProfiles[i].Username then
          lbProfiles.AddObj(mProfiles[i].Profilename, mProfiles[i].ID)
        else if mProfiles[i].IsAdmin then
          lbAdmin.AddObj(mProfiles[i].Profilename, mProfiles[i].ID);
{}
      end;                              //for i := 0 to FProfiles.Count -1 do
    end;                                //if xList.Count > 0 then
  finally
    xList.Free;
  end;                                  //xList

  EnableActions;
end;                                    //procedure TSelectProfileDlg.Init
//-----------------------------------------------------------------------------
function TmmSelectProfileEditor.GetProfileRecord(aID: integer): tSelectProfileRecordT;
var
  i: integer;

begin
  fillchar(result, sizeof(result), 0);
  for i := 0 to high(mProfiles) do
    if mProfiles[i].ID = aID then begin
      result.ID := mProfiles[i].ID;
      result.Default := mProfiles[i].Default;
      result.IsAdmin := mProfiles[i].IsAdmin;
      result.Username := mProfiles[i].Username;
      result.Profilename := mProfiles[i].Profilename;
      break;
    end;                                //if mProfiles[i].ID = aID then
end;                                    //function TmmSelectProfileEditor.GetProfileRecord
//-----------------------------------------------------------------------------
constructor TmmSelectProfileEditor.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
  mProfiles := nil;
//  tsAdmin.TabVisible := False;
end;                                    //constructor TmmSelectProfileEditor.Create
//-----------------------------------------------------------------------------
destructor TmmSelectProfileEditor.Destroy;
begin
  inherited Destroy;
  mProfiles := nil;
end;                                    //destructor TmmSelectProfileEditor.Destroy
//-----------------------------------------------------------------------------
function TmmSelectProfileEditor.GetSelectedProfile: tSelectProfileRecordT;
begin
  Result := GetProfileRecord(lbProfiles.GetItemID)

{
  if PageControl1.ActivePage = tsUser then
    Result := GetProfileRecord(lbProfiles.GetItemID)
  else
    Result := GetProfileRecord(lbAdmin.GetItemID);
{}
end;                                    //function TmmSelectProfileEditor.GetSelectedProfile
//-----------------------------------------------------------------------------
procedure TmmSelectProfileEditor.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;                                    //procedure TmmSelectProfileEditor.acHelpExecute

end. //mmSelectProfileDlg.pas

