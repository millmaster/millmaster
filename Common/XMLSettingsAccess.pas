{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: XMLSettingsHandler
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Schreibt und liest Settings in/aus der Datenbank.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.05.2004  1.00  Lok | File created
| 27.09.2004  1.01  Nue | Anpassungen c_setting ->c_xml_setting; YarnCnt nicht mehr mit Faktor
| 10.11.2004  1.02  Nue | ConvertToXMLMode added. (F�r Konversionstool alte- -> neue (XML)Settings n�tig)
| 21.12.2004        Nue | DeleteSetting, UpdateSetting usw. added.
| 09.03.2005        Nue | New Create with Query as parameter added; all consequences coded.
| 25.04.2005        Lok | Hashcode darf nur �ber das DocumentElement generiert werden (Unterschiedliche Comment Elemente)
| 14.06.2005        Nue | Handling of -X name of settings problem and c_modify_date in t_xml_ym_settings added.
| 15.07.2005        Wss | In SaveProdGroupValues wurde nochmals der YarnCount konvertiert, was jedoch nicht n�tig ist, da
                          der YarnCount gleich beim Eingang vom NetHandler nach yuNm konvertiert wird.
| 06.09.2005        Nue | Handling of -X name of settings: No More '%'-Checks and Setname-Length on DB fronm 50 to 52.
| 25.11.2005        Lok | Hints und Warnings entfernt
| 05.12.2005        Nue | cSelectUniqueSettings% Queries richtig modifiziert.
| 12.12.2005        Nue | cSelectUniqueSettingsAutoname Query added.
| 22.04.2009  2.01  Nue | Query cSelectUniqueSettingsName angepasst (distinct durch TOP 1 ersetzt), damit in Subquery nur 1 Resultset erscheint.
|==========================================================================================}
//ACHTUNG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//Diese Unit nur �bers ModelMaker Projekt �ndern!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
unit XMLSettingsAccess;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, BaseGlobal, AdoDBAccess, XMLDef, VCLXMLSettingsModel;
  
const
  cUpdateYMSetName =
    'update t_xml_ym_settings set c_YM_set_name = :c_YM_set_name ' +
    'where c_ym_set_id = :c_ym_set_id';

type
  // Identifiziert die Art einer ID (zb. Settings oder Produktionsgruppen)
  TIDType = (itNone,
             itXMLSetID,
             itProdID);// TIDType

  TXMLSettingsAccess = class(TObject)
  private
    FConvertToXMLMode: Boolean;
    mADODBAccess: TADODBAccess;
    mQuery: TNativeAdoQuery;
    procedure BeginTransaction(aQuery: TNativeAdoQuery);
    function CheckAndInsertSettings(const aName, aXMLData: String; const aHeadClass: TSensingHeadClass; const aColor:
      Integer; const aTemplate: Boolean; const aYMSetID: Integer = 0): Integer;
    function CheckAvailabilityOfSetID(aSetID: Integer; aQuery: TNativeAdoQuery): Boolean;
    procedure CommitTransaction(aQuery: TNativeAdoQuery);
    //1 Generiert den Hashcode und gibt den Normalisierten XMLStream wieder zur�ck 
    function GenerateHashcode(const aXMLData: String; out aHash1: integer; out aHash2: integer): String;
    function InsertNewSetting(aModel: TVCLXMLSettingsModel; aName: String; aTemplate: Boolean): Integer;
    procedure RollbackTransaction(aQuery: TNativeAdoQuery);
    procedure SaveProdGroupValues(aSetting: PXMLSettingsRec; aQuery: TNativeAdoQuery);
    function SaveSettingInternal(aSetting: PXMLSettingsRec; aTemplate, aWithProdGrpValues: Boolean): Integer;
  protected
    function ReadSettingInternal(aSetting: PXMLSettingsRec; var aXMLData: String): Boolean;
  public
    constructor Create(aQuery: TNativeAdoQuery=NIL);
    destructor Destroy; override;
    //1 Liest aus DB-Tabelle t_xml_ym_settings den Eintrag mit ID=1 und gibt c_xml_setting als Result-Wert zur�ck. 
    function DefaultXMLYMSetting: String;
    function DeleteSetting(aID: Integer): Boolean;
    //1 Holt den Teil der YMPara, welche in DB-Tabelle t_prodgroup abgelegt sind 
    function FillProdGrpValues(aSetting: PXMLSettingsRec; aProdGrpID: Longint): Boolean;
    function NewSetting(aModel: TVCLXMLSettingsModel): Integer; overload;
    function NewSetting(aModel: TVCLXMLSettingsModel; aName: String): Integer; overload;
    function NewTemplateSetting(aModel: TVCLXMLSettingsModel; aName: String): Integer;
    function ReadSetting(aID: integer; aIDType: TIDType; var aXMLData, aName: String): Boolean; overload;
    function ReadSetting(aID: integer; aIDType: TIDType; var aXMLData, aName: String; var aHeadClass:
      TSensingHeadClass): Boolean; overload;
    function SaveSetting(aSetting: PXMLSettingsRec; aTemplate: Boolean = false): Integer; overload;
    function SaveSettingAndProdGrpValues(aSetting: PXMLSettingsRec): Integer;
    function UpdateSetting(const aSetID: integer; const aXMLData: String;  const aHeadClass: TSensingHeadClass):
      Integer;
    property ConvertToXMLMode: Boolean read FConvertToXMLMode write FConvertToXMLMode default False;
  end;
  

implementation
uses
  mmcs, XMLGlobal, MMUGlobal, LoepfeGlobal, MMXMLConverter, YMParaUtils;

const
  //..............................................................................
  cCheckAvailabilityOfSetID =
    'select c_YM_set_id from t_xml_ym_settings where c_YM_set_id = :c_YM_set_id';
  //..............................................................................
  cSaveProdGrpYMPara =
    'update t_prodgroup set c_machineGroup=:Group, c_spindle_first=:SpindleFirst, ' +
    'c_spindle_last=:SpindleLast, c_pilotSpindles=:PilotSpindles, c_speedRamp=:SpeedRamp, ' +
    'c_speed=:Speed, c_act_yarncnt=:ActYarnCnt, c_yarncnt_unit=:YarnUnit, ' +
    'c_nr_of_threads=:NrOfThreads, c_lengthWindow=:LengthWindow, c_lengthMode=:LengthMode ' +
    'where c_prod_id = :ProdID';

  //..............................................................................
  cGetProdGrpYMPara =
    'select * from t_prodgroup where c_prod_id = :c_prod_id';

  //..............................................................................
 cSelectSettingFromYMSetID = 'SELECT c_xml_setting, c_YM_set_name, c_head_class FROM t_xml_ym_settings ' +
                              'WHERE c_YM_set_id = :c_YM_set_id';

  cSelectSettingFromProdID  = 'SELECT c_xml_setting, c_YM_set_name, c_head_class FROM t_xml_ym_settings, t_prodgroup ' +
                              'WHERE t_prodgroup.c_YM_set_id = t_xml_ym_settings.c_YM_set_id ' +
                              'AND t_prodgroup.c_prod_id = :c_prod_id';
  //..............................................................................
  cDeleteSetting =
    'delete from t_xml_ym_settings ' +
    'where c_ym_set_id = :c_ym_set_id';
  //..............................................................................
  cUpdateSettings =
    'update t_xml_ym_settings set c_head_class = :c_head_class,  c_hashcode1 =:c_hashcode1, '+
    'c_hashcode2 =:c_hashcode2, c_modify_date =:c_modify_date, c_xml_setting =:c_xml_setting ' +
    'where c_ym_set_id = :c_ym_set_id';

//  cInsertSettings =
//    'insert t_xml_ym_settings ( ' +
//    'c_YM_set_id, c_YM_set_name, c_head_class, c_template_set, c_color, ' +
//    'c_hashcode1, c_hashcode2, c_xml_setting) '+
//    'values ( ' +
//    ':c_YM_set_id, :c_YM_set_name, :c_head_class, :c_template_set, :c_color, ' +
//    ':c_hashcode1, :c_hashcode2, :c_xml_setting)';
  cInsertSettings =
    'insert t_xml_ym_settings ' +
    '(c_YM_set_id, c_YM_set_name, c_head_class, c_template_set, c_color, ' +
    'c_hashcode1, c_hashcode2, c_modify_date, c_xml_setting) '+
    'values (:c_YM_set_id, :c_YM_set_name, :c_head_class, :c_template_set, :c_color, ' +
    ':c_hashcode1, :c_hashcode2, :c_modify_date, :c_xml_setting)';

  cSelectUniqueSettings =
    'select c_YM_set_id from t_xml_ym_settings ' +
    'where c_template_set = 0 ' +
//    'and c_YM_set_name like :SetName ' +
    'and ((c_YM_set_name = :SetName) or (c_YM_set_name = :SetNameX)) '+ //Nue:5.12.05 Bei z.B. 'Test1', 'Test1-X' Ausschluss von 'Test11'
    'and c_head_class = :c_head_class ' +
    'and c_hashcode1 = :Hash1 and c_hashcode2 = :Hash2';

  cSelectUniqueSettingsAutoname =  //Nue:12.12.05
    'select c_YM_set_id from t_xml_ym_settings ' +
    'where c_template_set = 0 ' +
    'and c_YM_set_name like :SetName ' +
    'and c_head_class = :c_head_class ' +
    'and c_hashcode1 = :Hash1 and c_hashcode2 = :Hash2';

//   cSelectUniqueSettingsName =
//     'select c_YM_set_id from t_xml_ym_settings ' +
//     'where c_template_set = 0 ' +
//     'and c_YM_set_name like :SetName ';

//  cSelectUniqueSettingsName =
//    ' select c_YM_set_id from t_xml_ym_settings' +
//    ' where c_template_set = 0' +
//    ' and c_YM_set_name like :SetName' +
//    ' and c_head_class = :c_head_class' +
//    ' and c_modify_date>=(select distinct c_modify_date from t_xml_ym_settings' +
//    ' where c_template_set = 1' +
//    ' and c_YM_set_name like :SetName' +
//    ' and c_head_class = :c_head_class)';
  cSelectUniqueSettingsName =
    ' select c_YM_set_id from t_xml_ym_settings' +
    ' where c_template_set = 0' +
    ' and c_YM_set_name = :SetName' +
    ' and c_head_class = :c_head_class' +
// bis 2.01    ' and c_modify_date>=(select distinct c_modify_date from t_xml_ym_settings' +
    ' and c_modify_date>=(select top 1 c_modify_date from t_xml_ym_settings' +    // ab 2.01
    ' where c_template_set = 1' +
    ' and c_YM_set_name = :SetName' +
    ' and c_head_class = :c_head_class)';
//..............................................................................

//:-------------------------------------------------------------------
constructor TXMLSettingsAccess.Create(aQuery: TNativeAdoQuery=NIL);
begin
  inherited Create;
  
  mQuery := NIL;
  mADODBAccess := NIL;
  
  if Assigned(aQuery) then begin
    mQuery := aQuery;
  end
  else begin
    //Eigene DBConnection etablieren und daraus Query dem mQuery zuweisen
    mADODBAccess := TADODBAccess.Create(1);
    mADODBAccess.Init;
    mQuery := mADODBAccess.Query[0];
  end; //if Assigned(aQuery)
end;// TXMLSettingsAccess.Create cat:No category

//:-------------------------------------------------------------------
destructor TXMLSettingsAccess.Destroy;
begin
  if Assigned(mADODBAccess) then
    mADODBAccess.Free;
  
  inherited Destroy;
end;// TXMLSettingsAccess.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TXMLSettingsAccess.BeginTransaction(aQuery: TNativeAdoQuery);
begin
  try
    aQuery.StartTransaction;
  except
    on e: Exception do
      raise Exception.Create(e.message + ' TXMLSettingsAccess.BeginTransaction failed: ');
   end;
end;// TXMLSettingsAccess.BeginTransaction cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.CheckAndInsertSettings(const aName, aXMLData: String; const aHeadClass: TSensingHeadClass;
  const aColor: Integer; const aTemplate: Boolean; const aYMSetID: Integer = 0): Integer;
var
  i: Integer;
  xHasXExtension: Boolean;
  xSetName: String;
  xHash1, xHash2: Integer;
  xXMLData: String;
begin
  Result := -1;
  with mQuery do
  try
    xSetName := Trim(aName);
  
    // Hashcode genereieren und Steuerzeichen l�schen
    xXMLData := GenerateHashcode(aXMLData, xHash1, xHash2);
  
    if not ConvertToXMLMode then begin
      if (xSetName <> '') and (AnsiPos(cAutoNameConst, aName)=0) then begin
        // erst mal ein eventuell vorhandenes "-X" entfernen
        i := AnsiPos(cX1, xSetName);
        xHasXExtension := (i > 0) and (i = (Length(xSetName) - Length(cX1) + 1));
        if xHasXExtension then
          Delete(xSetName, i, Length(cX1));
        // Sodele, jetzt mal gucken ob ein solches Settings mit gleichem Namen
        // und gleichem Hashcode vorhanden ist
        SQL.Text := cSelectUniqueSettings;
        //Nue:5.12.05 Bei z.B. 'Test1', 'Test1-X' Ausschluss von 'Test11'
        ParamByName('SetName').AsString   := xSetName;
        ParamByName('SetNameX').AsString   := xSetName + cX1;
        ParamByName('c_head_class').AsInteger   := ORD(aHeadClass);
        ParamByName('Hash1').AsInteger    := xHash1;
        ParamByName('Hash2').AsInteger    := xHash2;
      end else begin
        xSetName := cAutoNameConst;
        // Sodele, jetzt mal gucken ob ein solches Settings mit Autoname-%
        // und gleichem Hashcode vorhanden ist
        SQL.Text := cSelectUniqueSettingsAutoname;
        ParamByName('SetName').AsString   := xSetName+'%';
        ParamByName('c_head_class').AsInteger   := ORD(aHeadClass);
        ParamByName('Hash1').AsInteger    := xHash1;
        ParamByName('Hash2').AsInteger    := xHash2;
      end;
  
      Open;
      if FindFirst then
        Result := FieldByName('c_YM_set_id').AsInteger;
  
      // Nichts vorhanden ==> es gibt ein neues Settings in der DB.
      // Nun aber noch pr�fen ob �berhaupt schon ein Setting (ohne Templates) mit dem Roh-Namen (ohne -X) vorhanden ist,
      //  welches aber bereits auf das allenfalls modifizierte Template referenziert
      if ((Result < 0) and (not aTemplate)) then begin
        SQL.Text := cSelectUniqueSettingsName;
        ParamByName('SetName').AsString   := xSetName;
        ParamByName('c_head_class').AsInteger   := ORD(aHeadClass);
        Open;
        if FindFirst then
  //        xSetName := Copy(xSetName, 1, cYMSetNameSize - Length(cX1)) + cX1;
          //Der SetName ist auf der DB auf 52 Zeichen limitiert. Die Eingabe eines TemplateNamen MUSS auf 50 Zeichen
          //  limitiert werden damit, hier jederzeit der Zusatz cX1 (-X) noch im Namen Platz hat.
          xSetName := xSetName + cX1;
      end;
    end;
  
    if Result < 0 then begin
      SQL.Text := cInsertSettings;
      ParamByName('c_YM_set_name').AsString   := xSetName; //aName;
      ParamByName('c_head_class').AsInteger   := ORD(aHeadClass);
      ParamByName('c_template_set').AsBoolean := aTemplate;
      ParamByName('c_color').AsInteger        := aColor;
      ParamByName('c_hashcode1').AsInteger    := xHash1;
      ParamByName('c_hashcode2').AsInteger    := xHash2;
      ParamByName('c_modify_date').AsDateTime := Now;  //Nue:14.06.05
      ParamByName('c_xml_setting').AsString   := xXMLData;
      if CodeSite.Enabled then
        CodeSite.Sendmsg(FormatXML(xXMLData));
  
      if ConvertToXMLMode then begin
        //Beibehalten der �bergebenen YMSetId
        ParamByName('c_ym_set_id').AsInteger  := Smallint(aYMSetID);
        ExecSQL;
        Result := aYMSetID;
      end
      else
        Result := InsertSQL('t_xml_ym_settings', 'c_YM_set_id', High(Smallint), 1);
  
        // Autoname kann erst hier mit ID erstellt werden, wenn schon ein Insert erfolgte
      if ((aName = '') or (AnsiPos(cAutoNameConst, aName)>0)) then begin
        SQL.Text := cUpdateYMSetName;
        ParamByName('c_YM_set_name').AsString := Format(cNoYMNamePrefix, [Result]);
        ParamByName('c_YM_set_id').AsInteger  := Result;
        ExecSQL;
      end;// if aSetting^.YMSetName = '' then begin
    end;
  except
    on e:Exception do begin
      codesite.SendError('TXMLSettingsAccess.CheckAndInsertSettings: ' + e.Message);
      raise Exception.Create('CheckAndInsertSettings failed: ' + e.message);
    end;
  end;// with Query[0] do, try
end;// TXMLSettingsAccess.CheckAndInsertSettings cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.CheckAvailabilityOfSetID(aSetID: Integer; aQuery: TNativeAdoQuery): Boolean;
begin
  with aQuery do
  try
    SQL.Text := cCheckAvailabilityOfSetID;
    ParamByName('c_YM_set_id').AsInteger := aSetID;
    Open;
    Result := FindFirst;
    Close;
  except
    on e: Exception do
      raise Exception.create('CheckAvailabilityOfSetID failed: ' + e.Message);
  end;
end;// TXMLSettingsAccess.CheckAvailabilityOfSetID cat:No category

//:-------------------------------------------------------------------
procedure TXMLSettingsAccess.CommitTransaction(aQuery: TNativeAdoQuery);
begin
  try
    aQuery.Commit;
  except
    on e: Exception do
      raise Exception.Create(e.message + ' TXMLSettingsAccess.CommitTransaction failed: ');
  end;
end;// TXMLSettingsAccess.CommitTransaction cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.DefaultXMLYMSetting: String;
var
  xXMLData: String;
  xName: String;
begin
  Result := '';
  
  ReadSetting(cXMLDefaultSetingsID,itXMLSetID, xXMLData, xName);  //-13 ist die ID der Defaultsettings von MemoryC
  Result := xXMLData;
end;// TXMLSettingsAccess.DefaultXMLYMSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.DeleteSetting(aID: Integer): Boolean;
begin
  try
    with mQuery do begin
      SQL.Text := cDeleteSetting;
      Params.ParamByName('c_ym_set_id').AsInteger := aID;
      ExecSQL;
      Close;
      Result := True;
    end; // with Query[0]
  except
    on e: Exception do begin
      raise Exception.create(e.message + ' Delete failed.');
    end;
  end;
end;// TXMLSettingsAccess.DeleteSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.FillProdGrpValues(aSetting: PXMLSettingsRec; aProdGrpID: Longint): Boolean;
begin
  Result   := False;
  try
    with mQuery do begin
      SQL.Text := cGetProdGrpYMPara;
      ParamByName('c_prod_id').AsInteger := aProdGrpID;
      Open;
      if FindFirst then
      begin
        with aSetting^ do begin
          Group := FieldByName('c_machineGroup').AsInteger; // Local Group (0..5: AC338 Informator, 0..11 ZE)
          ProdGrpID := aProdGrpID;      // MillMaster's group ID, 0=not defined
          SpindleFirst := FieldByName('c_spindle_first').AsInteger; //Spindle first
          SpindleLast := FieldByName('c_spindle_last').AsInteger; //Spindle first
          PilotSpindles := FieldByName('c_pilotSpindles').AsInteger;
          SpeedRamp := FieldByName('c_speedRamp').AsInteger; // only machines with Speed Simulation
          Speed := FieldByName('c_speed').AsInteger; // winding speed, (20..1600), [20..1600 m/Min]
  //Old til 28.2.02 Nue          yarnCnt := FieldByName('c_act_yarncnt').AsInteger; // Yarn count [cMinYarnCount..cMinYarnCount]
          YarnCntUnit := TYarnUnit(FieldByName('c_yarncnt_unit').AsInteger); // Yarn unit in PutProdGrpYMPara
          //New 28.2.02 Nue
          YarnCnt := Round(YarnCountConvert(yuNm, TYarnUnit(yarnCntUnit), FieldByName('c_act_yarncnt').AsFloat)); //27.9.04 nue: No more * cYarnCntFactor
  //          yarnCnt := Round(YarnCountConvert(yuNm, TYarnUnit(yarnCntUnit), FieldByName('c_act_yarncnt').AsFloat*cYarnCntFactor));
          NrOfThreads := FieldByName('c_nr_of_threads').AsInteger; // Nr of threads in PutProdGrpYMPara
          LengthWindow := FieldByName('c_lengthWindow').AsInteger;
          LengthMode := FieldByName('c_lengthMode').AsInteger; // yMType ....
          Result   := True;
        end;
      end;
    end; // with mQuery
  except
    on e: Exception do begin
      raise Exception.create(e.message + ' FillProdGrpValues failed.');
    end;
  end;
end;// TXMLSettingsAccess.FillProdGrpValues cat:No category

//:-------------------------------------------------------------------
(*: Member:           GenerateHashcode
 *  Klasse:           TXMLSettingsAccess
 *  Kategorie:        No category 
 *  Argumente:        (aXMLData, aHash1, aHash2)
 *
 *  Kurzbeschreibung: Generiert den Hashcode und gibt den Normalisierten XMLStream wieder zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLSettingsAccess.GenerateHashcode(const aXMLData: String; out aHash1: integer; out aHash2: integer): String;
var
  xDOM: DOMDocumentMM;
begin
  result := StringReplace(Trim(aXMLData), #13#10, '', [rfReplaceAll]);
  result := StringReplace(result, #10#13, '', [rfReplaceAll]);
  result := StringReplace(result, #09, '', [rfReplaceAll]);
  
  (* F�r die Berechnung des Hashcodes darf nur das DocumentElement verwendet werden,
     da der Kommentar und die ProcessingInformations nicht zum Setting geh�ren. *)
  xDOM := XMLStreamToDOM(result);
  if (assigned(xDOM)) and (assigned(xDOM.DocumentElement)) then
    HashByMM(xDOM.DocumentElement, aHash1, aHash2)
  else
    HashByMM(result, aHash1, aHash2);
  // DOM wird nicht mehr ben�tigt
  xDOM := nil;
end;// TXMLSettingsAccess.GenerateHashcode cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.InsertNewSetting(aModel: TVCLXMLSettingsModel; aName: String; aTemplate: Boolean): Integer;
var
  xHeadClass: TSensingHeadClass;
begin
  EnterMethod('TXMLSettingsAccess.InsertNewSetting');
  
  xHeadClass := GetSensingHeadClass(aModel);
  //TMMXMLConverter.GenerateHashCode(aModel.xmlAsDOM, xHash1, xHash2);
  
  Result := CheckAndInsertSettings(aName, aModel.xmlAsString, xHeadClass, 0, aTemplate);
  
  // with mQuery do
  // try
  //   SQL.Text := cInsertSettings;
  //   ParamByName('c_YM_set_name').AsString   := aName;
  //   ParamByName('c_head_class').AsInteger   := Ord(xHeadClass);
  //   ParamByName('c_template_set').AsBoolean := aTemplate;
  //   ParamByName('c_color').AsInteger        := 0;
  //   ParamByName('c_hashcode1').AsInteger    := xHash1;
  //   ParamByName('c_hashcode2').AsInteger    := xHash2;
  //   ParamByName('c_xml_setting').AsString   := aModel.xmlAsString;
  //   if CodeSite.Enabled then
  //     CodeSite.Sendmsg(FormatXML(aModel.xmlAsString));
  //
  //   Result := InsertSQL('t_xml_ym_settings', 'c_YM_set_id', high(Smallint), 1); //SDo
  //   Close;
  //
  //   //Nue:17.9.01
  //   if aName = '' then begin
  //     SQL.Text := cUpdateYMSetName;
  //     ParamByName('c_YM_set_name').AsString := Format(cNoYMNamePrefix,[Result]);
  //     ParamByName('c_YM_set_id').AsInteger := Result;
  //     ExecSQL;
  //     Close;
  //   end;
  // except
  //   on e:Exception do begin
  //     codesite.SendMsg('EXEPCTION:TXMLSettingsAccess.InsertNewSetting: ' + e.Message);
  //     raise Exception.Create(e.message + ' InsertNewSetting failed.');
  //   end;
  // end;
end;// TXMLSettingsAccess.InsertNewSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.NewSetting(aModel: TVCLXMLSettingsModel): Integer;
begin
  try
    Result := InsertNewSetting(aModel, '', False {kein TemplateSet});
  except
    on e:Exception do
      raise Exception.create(e.message + ' NewSetting failed.');
  end;
end;// TXMLSettingsAccess.NewSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.NewSetting(aModel: TVCLXMLSettingsModel; aName: String): Integer;
begin
  try
    Result := InsertNewSetting(aModel, aName, False {kein TemplateSet});
  except
    on e:Exception do
      raise Exception.create(e.message + ' NewSetting failed.');
  end;
end;// TXMLSettingsAccess.NewSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.NewTemplateSetting(aModel: TVCLXMLSettingsModel; aName: String): Integer;
begin
  try
    Result := InsertNewSetting(aModel, aName, True {TemplateSet});
  except
    on e: Exception do
    begin
      raise Exception.create(e.message + ' NewTemplate failed.');
    end;
  end;
end;// TXMLSettingsAccess.NewTemplateSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.ReadSetting(aID: integer; aIDType: TIDType; var aXMLData, aName: String): Boolean;
var
  xHeadClass: TSensingHeadClass;
begin
  Result := ReadSetting(aID, aIDType, aXMLData, aName, xHeadClass);
  // Result   := False;
  // aXMLData := '';
  // aName    := '';
  //
  // try
  //   with mQuery do begin
  //     case aIDType of
  //       itXMLSetID: begin
  //           SQL.Text := cSelectSettingFromYMSetID;
  //           ParamByName('c_YM_set_id').AsInteger := aID;
  //         end;
  //       itProdID: begin
  //           SQL.Text := cSelectSettingFromProdID;
  //           ParamByName('c_prod_id').AsInteger := aID;
  //         end;
  //     else
  //       Abort;
  //     end;
  //     // Query �ffnen und XMLDaten auslesen
  //     Open;
  //     if FindFirst then begin
  //       aName    := FieldByName('c_YM_set_name').AsString;
  //       aXMLData := FieldByName('c_xml_setting').AsString;
  //       if Codesite.Enabled then begin
  //         CodeSite.SendString('TXMLSettingsAccess.ReadSetting: aXMLData: ', FormatXML(aXMLData));
  //       end;
  //       Result   := True;
  //     end;
  //   end; // with mQuery
  // except
  //   on e: Exception do begin
  //     raise Exception.create(e.message + ' ReadSetting failed.');
  //   end;
  // end;
end;// TXMLSettingsAccess.ReadSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.ReadSetting(aID: integer; aIDType: TIDType; var aXMLData, aName: String; var aHeadClass:
  TSensingHeadClass): Boolean;
begin
  Result     := False;
  aXMLData   := '';
  aName      := '';
  aHeadClass := shc8x;
  with mQuery do
  try
    case aIDType of
      itXMLSetID: begin
          SQL.Text := cSelectSettingFromYMSetID;
          ParamByName('c_YM_set_id').AsInteger := aID;
        end;
      itProdID: begin
          SQL.Text := cSelectSettingFromProdID;
          ParamByName('c_prod_id').AsInteger := aID;
        end;
    else
      Abort;
    end;
    // Query �ffnen und XMLDaten auslesen
    Open;
    if FindFirst then begin
      aHeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger);
      aName      := FieldByName('c_YM_set_name').AsString;
      aXMLData   := FieldByName('c_xml_setting').AsString;
      if Codesite.Enabled then
        CodeSite.SendString('TXMLSettingsAccess.ReadSetting: aXMLData: ', FormatXML(aXMLData));
      Result     := True;
    end;
  except
    on e: Exception do
      raise Exception.create(e.message + ' ReadSetting failed.');
  end; // with Query[0]
end;// TXMLSettingsAccess.ReadSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.ReadSettingInternal(aSetting: PXMLSettingsRec; var aXMLData: String): Boolean;
begin
  Result := False;
end;// TXMLSettingsAccess.ReadSettingInternal cat:No category

//:-------------------------------------------------------------------
procedure TXMLSettingsAccess.RollbackTransaction(aQuery: TNativeAdoQuery);
begin
  try
    aQuery.Rollback;
  except
    on e: Exception do
    begin
      raise Exception.Create(e.message + ' TXMLSettingsAccess.RollbackTransaction failed. ');
    end;
  end;
end;// TXMLSettingsAccess.RollbackTransaction cat:No category

//:-------------------------------------------------------------------
procedure TXMLSettingsAccess.SaveProdGroupValues(aSetting: PXMLSettingsRec; aQuery: TNativeAdoQuery);
begin
  with aQuery do
  try
    SQL.Text := cSaveProdGrpYMPara;
    with aSetting^ do begin
      ParamByName('Group').AsInteger         := Group;
      ParamByName('SpindleFirst').AsInteger  := SpindleFirst;
      ParamByName('SpindleLast').AsInteger   := SpindleLast;
      ParamByName('PilotSpindles').AsInteger := PilotSpindles;
      ParamByName('SpeedRamp').AsInteger     := SpeedRamp;
      ParamByName('Speed').AsInteger         := Speed;
      ParamByName('ActYarnCnt').AsFloat      := YarnCnt; // 15.7.2005 wss: nicht nochmal konvertieren...
  //    ParamByName('ActYarnCnt').AsFloat      := YarnCountConvert(TYarnUnit(YarnCntUnit), yuNm, YarnCnt); //27.9.04 nue: No more Div cYarnCntFactor
      ParamByName('YarnUnit').AsInteger      := Ord(YarnCntUnit);
      ParamByName('NrOfThreads').AsInteger   := NrOfThreads;
      ParamByName('LengthWindow').AsInteger  := LengthWindow;
      ParamByName('LengthMode').AsInteger    := LengthMode;
      ParamByName('ProdID').AsInteger        := ProdGrpID;
    end; //with
    ExecSQL;
    Close;
  except
    on e: Exception do
    begin
      raise Exception.create(e.Message + ' TSettingsDBAccess.SaveProdGrpYMPara failed.');
    end;
  end;
end;// TXMLSettingsAccess.SaveProdGroupValues cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.SaveSetting(aSetting: PXMLSettingsRec; aTemplate: Boolean = false): Integer;
begin
  // Setting abspeichern (Settings Header und XML Stream)
  Result := SaveSettingInternal(aSetting, aTemplate, False);
  if Codesite.Enabled then
    CodeSite.SendFmtMsg('Setting: %s', [StrPas(aSetting^.XMLData)]);  //FormatXML funktioniert nicht, weil XMLData und XML-Pattern ein Sonderzeichen ist!
end;// TXMLSettingsAccess.SaveSetting cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.SaveSettingAndProdGrpValues(aSetting: PXMLSettingsRec): Integer;
begin
  // Setting abspeichern (Settings Header und XML Stream)
  Result := SaveSettingInternal(aSetting, False, True);
  if Codesite.Enabled then
    CodeSite.SendString('Setting', FormatXML(StrPas(aSetting^.XMLData)));
end;// TXMLSettingsAccess.SaveSettingAndProdGrpValues cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.SaveSettingInternal(aSetting: PXMLSettingsRec; aTemplate, aWithProdGrpValues: Boolean):
  Integer;
var
  xSetName: String;
  
  const
  //   cInsertSetting    = 'INSERT INTO t_xml_ym_settings ' +
  //                       ' (c_YM_set_id, c_YM_set_name, c_head_class, c_template_set, c_color, ' +
  //                       ' c_hashcode1, c_hashcode2, c_xml_setting) ' +
  //                       ' VALUES (:c_YM_set_id, :c_YM_set_name, :c_head_class, :c_template_set, :c_color, ' +
  //                       ' :c_hashcode1, :c_hashcode2, :c_xml_setting) ';
  
  
  //   cUpdateYMSetName = 'UPDATE t_xml_ym_settings '+
  //                      ' SET c_YM_set_name = :c_YM_set_name ' +
  //                      ' WHERE c_ym_set_id = :c_ym_set_id';
  
    cUpdateProdGrp   = 'UPDATE t_prodgroup ' +
                       ' SET c_YM_set_id = :c_YM_set_id ' +
                       ' WHERE c_prod_id = :c_prod_id';
  
    cValidProdID     = 'SELECT Count(c_prod_id) AS ProdIDCount from t_prodgroup WHERE c_prod_ID = :c_prod_id';
  
begin
  Result := 0;
  xSetName := Trim(aSetting^.YMSetName);
  if aWithProdGrpValues then
    SaveProdGroupValues(aSetting, mQuery);
  
  { TODO -onue -cDatenbank : ACHTUNG! Transaction sollte hier wieder aktiviert werden! Ist aber so nicht m�glich, weil bereits in InsertSQL eine Transaktion gestartet wird! }
  //nue    StartTransaction;
  
  { TODO wss: zur Zeit wird das Setting immer gespeichert. Im alten Code wurde VOR dem Speichern
            nach einem Duplikat gesucht, welches diesem Setting gleicht. Wenn eines vorhanden ist
            wurde kein neuer Settings Record auf der DB eingef�gt. Dies sollte nun hier ebenfalls geschehen.}
  BeginTransaction(mQuery);
  try
    with aSetting^ do
      Result := CheckAndInsertSettings(xSetName, StrPas(XMLData), HeadClass, Color, aTemplate, YMSetID);
  
  //   SQL.Text := cInsertSettings;
  //   ParamByName('c_YM_set_name').AsString   := xSetName;
  //   ParamByName('c_head_class').AsInteger   := ORD(aSetting^.HeadClass);
  //   ParamByName('c_template_set').AsBoolean := aTemplate;
  //   ParamByName('c_color').AsInteger        := aSetting^.Color;
  //   ParamByName('c_hashcode1').AsInteger    := aSetting^.Hash1;
  //   ParamByName('c_hashcode2').AsInteger    := aSetting^.Hash2;
  //   ParamByName('c_xml_setting').AsString   := StrPas(aSetting^.XMLData);
  //   if CodeSite.Enabled then
  //     CodeSite.Sendmsg(FormatXML(StrPas(aSetting^.XMLData)));
  //
  //   if ConvertToXMLMode then begin
  //     //Beibehalten der �bergebenen YMSetId
  //     try
  //       ParamByName('c_ym_set_id').AsInteger  := Smallint(aSetting^.YMSetID);
  //       ExecSQL;
  //       Result := aSetting^.YMSetID;
  //     except
  //       raise Exception.CreateFmt('TXMLSettingsAccess.SaveSetting: Error inserting c_YM_set_id=%d in t_xml_ym_settings', [aSetting^.YMSetID]);
  //     end;
  //   end
  //   else begin
  //     Result := InsertSQL('t_xml_ym_settings', 'c_YM_set_id', High(Smallint), 1);
  //   end; //if ConvertToXMLMode
  //
  //     // Autoname kann erst hier mit ID erstellt werden, wenn schon ein Insert erfolgte
  //   if xSetName = '' then begin
  //     SQL.Text := cUpdateYMSetName;
  //     ParamByName('c_YM_set_name').AsString := Format(cNoYMNamePrefix, [Result]);
  //     ParamByName('c_YM_set_id').AsInteger  := Result;
  //     ExecSQL;
  //   end;// if aSetting^.YMSetName = '' then begin
  
      // wenn eine g�ltige ProdID mitgeliefert wurde, dann den Link noch aktualisieren
    if aSetting^.ProdGrpID <> 0 then begin
      with mQuery do begin
        Close;
        SQL.Text := cValidProdID;
        ParamByName('c_prod_id').AsInteger := aSetting^.ProdGrpID;
        Open;
        if FieldByName('ProdIDCount').AsInteger = 1 then begin
          Close;
          SQL.Text := cUpdateProdGrp;
          ParamByName('c_YM_set_id').AsInteger := Result;
          ParamByName('c_prod_id').AsInteger   := aSetting^.ProdGrpID;
          ExecSQL;
        end;
      end;
    end;  // if aSetting^.ProdGrpID <> 0 then begin
    CommitTransaction(mQuery);
  except
    RollbackTransaction(mQuery);
  end;// with Query[0] do, try
  
  
  // Result := 0;
  // xSetName := Trim(aSetting^.YMSetName);
  // if aWithProdGrpValues then
  //   SaveProdGroupValues(aSetting, mQuery);
  //
  // { TODO -onue -cDatenbank : ACHTUNG! Transaction sollte hier wieder aktiviert werden! Ist aber so nicht m�glich, weil bereits in InsertSQL eine Transaktion gestartet wird! }
  // //nue    StartTransaction;
  //
  // { TODO wss: zur Zeit wird das Setting immer gespeichert. Im alten Code wurde VOR dem Speichern
  //           nach einem Duplikat gesucht, welches diesem Setting gleicht. Wenn eines vorhanden ist
  //           wurde kein neuer Settings Record auf der DB eingef�gt. Dies sollte nun hier ebenfalls geschehen.}
  // with mQuery do
  // try
  //   BeginTransaction(mQuery);
  //   SQL.Text := cInsertSettings;
  //   ParamByName('c_YM_set_name').AsString   := xSetName;
  //   ParamByName('c_head_class').AsInteger   := ORD(aSetting^.HeadClass);
  //   ParamByName('c_template_set').AsBoolean := aTemplate;
  //   ParamByName('c_color').AsInteger        := aSetting^.Color;
  //   ParamByName('c_hashcode1').AsInteger    := aSetting^.Hash1;
  //   ParamByName('c_hashcode2').AsInteger    := aSetting^.Hash2;
  //   ParamByName('c_xml_setting').AsString   := StrPas(aSetting^.XMLData);
  //   if CodeSite.Enabled then
  //     CodeSite.Sendmsg(FormatXML(StrPas(aSetting^.XMLData)));
  //
  //   if ConvertToXMLMode then begin
  //     //Beibehalten der �bergebenen YMSetId
  //     try
  //       ParamByName('c_ym_set_id').AsInteger  := Smallint(aSetting^.YMSetID);
  //       ExecSQL;
  //       Result := aSetting^.YMSetID;
  //     except
  //       raise Exception.CreateFmt('TXMLSettingsAccess.SaveSetting: Error inserting c_YM_set_id=%d in t_xml_ym_settings', [aSetting^.YMSetID]);
  //     end;
  //   end
  //   else begin
  //     Result := InsertSQL('t_xml_ym_settings', 'c_YM_set_id', High(Smallint), 1);
  //   end; //if ConvertToXMLMode
  //
  //     // Autoname kann erst hier mit ID erstellt werden, wenn schon ein Insert erfolgte
  //   if xSetName = '' then begin
  //     SQL.Text := cUpdateYMSetName;
  //     ParamByName('c_YM_set_name').AsString := Format(cNoYMNamePrefix, [Result]);
  //     ParamByName('c_YM_set_id').AsInteger  := Result;
  //     ExecSQL;
  //   end;// if aSetting^.YMSetName = '' then begin
  //
  //     // wenn eine g�ltige ProdID mitgeliefert wurde, dann den Link noch aktualisieren
  //   if aSetting^.ProdGrpID <> 0 then begin
  //     Close;
  //     SQL.Text := cValidProdID;
  //     ParamByName('c_prod_id').AsInteger := aSetting^.ProdGrpID;
  //     Open;
  //     if FieldByName('ProdIDCount').AsInteger = 1 then begin
  //       Close;
  //       SQL.Text := cUpdateProdGrp;
  //       ParamByName('c_YM_set_id').AsInteger := Result;
  //       ParamByName('c_prod_id').AsInteger   := aSetting^.ProdGrpID;
  //       ExecSQL;
  //     end;
  //   end;// if aSetting^.ProdGrpID <> 0 then begin
  //   CommitTransaction(mQuery);
  // except
  //   RollbackTransaction(mQuery);
  // end;// with Query[0] do, try
end;// TXMLSettingsAccess.SaveSettingInternal cat:No category

//:-------------------------------------------------------------------
function TXMLSettingsAccess.UpdateSetting(const aSetID: integer; const aXMLData: String;  const aHeadClass:
  TSensingHeadClass): Integer;
var
  xXMLData: String;
  xHash1, xHash2: Integer;
begin
  EnterMethod('TXMLSettingsAccess.UpdateSetting');
  Result := aSetID;
  
  // Hashcode genereieren und Steuerzeichen l�schen
  xXMLData := GenerateHashcode(aXMLData, xHash1, xHash2);
  
  with mQuery do
  try
    BeginTransaction(mQuery);
    if not CheckAvailabilityOfSetID(aSetID, mQuery) then
      raise Exception.Create(Format('YM_SetID %s not available on Database.', [aSetID]));
  
    SQL.Text := cUpdateSettings;
    ParamByName('c_ym_set_id').AsInteger  := aSetID;
    ParamByName('c_head_class').AsInteger := Ord(aHeadClass);
    ParamByName('c_hashcode1').AsInteger  := xHash1;
    ParamByName('c_hashcode2').AsInteger  := xHash2;
    ParamByName('c_modify_date').AsDateTime := Now;  //Nue:14.06.05
    ParamByName('c_xml_setting').AsString := xXMLData;
    ExecSQL;
    Close;
    CommitTransaction(mQuery);
  except
    on e: Exception do begin
      RollbackTransaction(mQuery);
      codesite.SendMsg('EXEPCTION:TXMLSettingsAccess.UpdateSetting: ' + e.Message);
      raise Exception.Create(e.message + ' UpdateSetting failed.');
    end;
  end;
  
  // with mQuery do
  // try
  //   BeginTransaction(mQuery);
  //   if not CheckAvailabilityOfSetID(aSetID, mQuery) then
  //     raise Exception.Create(Format('YM_SetID %s not available on Database.', [aSetID]));
  //
  //   SQL.Text := cUpdateSettings;
  //   ParamByName('c_head_class').AsInteger := Ord(aHeadClass);
  //   ParamByName('c_hashcode1').AsInteger := aHashcode1;
  //   ParamByName('c_hashcode2').AsInteger := aHashcode2;
  // { DONE 1 -oNue -cXMLSettings : Stimmt hier AsString (f�r eine Text-Spalte)? }
  //   ParamByName('c_xml_setting').AsString := aXMLData;
  //   ParamByName('c_ym_set_id').AsInteger := aSetID;
  //   ExecSql;
  //   Close;
  //   CommitTransaction(mQuery);
  // except
  //   on e: Exception do begin
  //     RollbackTransaction(mQuery);
  //     codesite.SendMsg('EXEPCTION:TXMLSettingsAccess.UpdateSetting: ' + e.Message);
  //     raise Exception.Create(e.message + ' UpdateSetting failed.');
  //   end;
  // end;
end;// TXMLSettingsAccess.UpdateSetting cat:No category



end.

