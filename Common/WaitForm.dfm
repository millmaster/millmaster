inherited Wait: TWait
  Left = 525
  Top = 261
  BorderIcons = []
  Caption = '. . .'
  ClientHeight = 22
  ClientWidth = 242
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmProgressBar1: TmmProgressBar
    Left = 0
    Top = 0
    Width = 242
    Height = 22
    Align = alClient
    Min = 0
    Max = 100
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTimer1: TmmTimer
    Interval = 100
    OnTimer = mmTimer1Timer
    Left = 208
  end
end
