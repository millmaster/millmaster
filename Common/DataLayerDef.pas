(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: DataLayerDef.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...: Definition file with const and types for DataLayer modul
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 14.06.2001 1.00 Wss | File created
| 19.07.2001 1.00 Wss | - SetID added to TYMSettingsInfoRec
                        - WhereTimeRange corrected for v_production interval
| 14.06.2001 1.00 Wss | File created
| 10.06.2002 1.01 Nue | Addings because of longterm �mplementation.
| 16.01.2003 1.00 Wss | Zeitfelder in den Views werden nun generell umgesetzt mittels c_time
                        und c_time_filter f�r v_production_week
|=============================================================================*)
unit DataLayerDef;

interface

uses
  MMUGlobal, YMParaDef, XMLDef;

type
//  TClassDataRec = record
//    Len: Single;
//    ClassCut:    Array[1..cClassCutFields] of Single;
//    ClassUncut:  Array[1..cClassUncutFields] of Single;
//    SpliceCut:   Array[1..cSpCutFields] of Single;
//    SpliceUncut: Array[1..cSpUncutFields] of Single;
//    SiroCut:     Array[1..cSiroCutFields] of Single;
//    SiroUncut:   Array[1..cSiroUncutFields] of Single;
//  end;

//  PYMSettingsInfoRec = ^TYMSettingsInfoRec;
//  TYMSettingsInfoRec = record
//    SetID: Integer;
//    SetName: String;
//    YarnCount: Single;
//    YarnUnit: TYarnUnit;
//    SingleYarnCount: Boolean;
//    YMSettingsExt: TYMExtensionRec;
//  end;

  PXMLSettingsInfoRec = ^TXMLSettingsInfoRec;
  TXMLSettingsInfoRec = record
    SetID: Integer;
    SetName: String;
    YarnCount: Single;
    YarnUnit: TYarnUnit;
    SingleYarnCount: Boolean;
    HeadClass: TSensingHeadClass;
    XMLData: String;
  end;
  
const
  cMachNameIndex  = 0;
  cProdNameIndex  = 1;
  cStyleNameIndex = 2;
  
const
  cQueryGetIDs: Array[TTimeMode] of String = (
    'select distinct v.%s, v.%s from v_production_interval v where 1=1 ',
    'select distinct v.%s, v.%s from v_production_shift v where 1=1 ',
    'select distinct v.%s, v.%s from v_production_week v where 1=1 '  //1.01 Nue
  );

  cWhereTimeRange: Array[TTimeMode] of String = (
    'and v.c_time >= :TimeFrom and v.c_interval_end <= :TimeTo ',
    'and v.c_time >= :TimeFrom and v.c_time < :TimeTo ',
    'and c_time_filter between :TimeFrom and :TimeTo ' //1.01 Nue
  );

//  cWhereTimeRange: Array[TTimeMode] of String = (
//    'and v.c_interval_start >= :TimeFrom and v.c_interval_end <= :TimeTo ',
//    'and v.c_shift_start >= :TimeFrom and v.c_shift_start < :TimeTo ',
//    'and c_time_stamp between :TimeFrom and :TimeTo ' //1.01 Nue
//  );

  cWhereStyleID    = 'and v.c_style_id in (%s) ';
  cWhereProdID     = 'and v.c_prod_id in (%s) ';
  cWhereMachID     = 'and v.c_machine_id in (%s) ';
  cWhereSpindleID  = 'and v.c_spindle_id in (%s) ';
  cWhereYMSetID    = 'and v.c_ym_set_id in (%s) ';
  cWhereShiftCalID: Array[TTimeMode] of String = (  //NUE
    'and v.c_shiftcal_id = %d ',
    'and v.c_shiftcal_id = %d ',
    'and 1 = %d '   //NUE@@@@ Nicht fuer ewig!
  );
  cWhereMultTimeRange = 'and (%s) ';

  cWhereStyleName = 'and v.c_style_name in (%s) ';
  cWhereProdName  = 'and v.c_prod_name in (%s) ';
  cWhereMachName  = 'and v.c_machine_name in (%s) ';
  cWhereName      = 'and v.%s in (%s) ';


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
initialization
end.
