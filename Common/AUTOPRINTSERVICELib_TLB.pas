unit AUTOPRINTSERVICELib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 08.03.2001 10:13:07 from Type Library described below.

// *************************************************************************//
// NOTE:                                                                      
// Items guarded by $IFDEF_LIVE_SERVER_AT_DESIGN_TIME are used by properties  
// which return objects that may need to be explicitly created via a function 
// call prior to any access via the property. These items have been disabled  
// in order to prevent accidental use from within the object inspector. You   
// may enable them by defining LIVE_SERVER_AT_DESIGN_TIME or by selectively   
// removing them from the $IFDEF blocks. However, such items must still be    
// programmatically created via a method of the appropriate CoClass before    
// they can be used.                                                          
// ************************************************************************ //
// Type Lib: D:\MillMaster\Floor\autoprintservice.exe (1)
// IID\LCID: {DFB4F180-D43E-11D1-A212-006097C4C8E3}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\System32\StdOle2.tlb)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// Errors:
//   Error creating palette bitmap of (TAutoOutputNotify) : Server D:\MILLMA~1\Floor\AUTOPR~1.EXE contains no icons
//   Error creating palette bitmap of (TAutoOutputNotifyMachine) : Server D:\MILLMA~1\Floor\AUTOPR~1.EXE contains no icons
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  AUTOPRINTSERVICELibMajorVersion = 2;
  AUTOPRINTSERVICELibMinorVersion = 0;

  LIBID_AUTOPRINTSERVICELib: TGUID = '{DFB4F180-D43E-11D1-A212-006097C4C8E3}';

  IID_IAutoOutputNotify: TGUID = '{0466C462-FCEA-11D0-99F4-00A024C30CB6}';
  CLASS_AutoOutputNotify: TGUID = '{0466C463-FCEA-11D0-99F4-00A024C30CB6}';
  IID_IAutoOutputNotifyMachine: TGUID = '{DFB4F181-D43E-11D1-A212-006097C4C8E3}';
  CLASS_AutoOutputNotifyMachine: TGUID = '{DFB4F182-D43E-11D1-A212-006097C4C8E3}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IAutoOutputNotify = interface;
  IAutoOutputNotifyDisp = dispinterface;
  IAutoOutputNotifyMachine = interface;
  IAutoOutputNotifyMachineDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  AutoOutputNotify = IAutoOutputNotify;
  AutoOutputNotifyMachine = IAutoOutputNotifyMachine;


// *********************************************************************//
// Interface: IAutoOutputNotify
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0466C462-FCEA-11D0-99F4-00A024C30CB6}
// *********************************************************************//
  IAutoOutputNotify = interface(IDispatch)
    ['{0466C462-FCEA-11D0-99F4-00A024C30CB6}']
    procedure TriggerEvent(nEvent: Integer; nSubEvent: Integer); safecall;
    procedure SetupChange; safecall;
    procedure KillServer; safecall;
  end;

// *********************************************************************//
// DispIntf:  IAutoOutputNotifyDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {0466C462-FCEA-11D0-99F4-00A024C30CB6}
// *********************************************************************//
  IAutoOutputNotifyDisp = dispinterface
    ['{0466C462-FCEA-11D0-99F4-00A024C30CB6}']
    procedure TriggerEvent(nEvent: Integer; nSubEvent: Integer); dispid 1;
    procedure SetupChange; dispid 2;
    procedure KillServer; dispid 3;
  end;

// *********************************************************************//
// Interface: IAutoOutputNotifyMachine
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DFB4F181-D43E-11D1-A212-006097C4C8E3}
// *********************************************************************//
  IAutoOutputNotifyMachine = interface(IAutoOutputNotify)
    ['{DFB4F181-D43E-11D1-A212-006097C4C8E3}']
    procedure TriggerMachineEvent(nEvent: Integer; nSubEvent: Integer; nMachine: Smallint); safecall;
  end;

// *********************************************************************//
// DispIntf:  IAutoOutputNotifyMachineDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {DFB4F181-D43E-11D1-A212-006097C4C8E3}
// *********************************************************************//
  IAutoOutputNotifyMachineDisp = dispinterface
    ['{DFB4F181-D43E-11D1-A212-006097C4C8E3}']
    procedure TriggerMachineEvent(nEvent: Integer; nSubEvent: Integer; nMachine: Smallint); dispid 4;
    procedure TriggerEvent(nEvent: Integer; nSubEvent: Integer); dispid 1;
    procedure SetupChange; dispid 2;
    procedure KillServer; dispid 3;
  end;

// *********************************************************************//
// The Class CoAutoOutputNotify provides a Create and CreateRemote method to          
// create instances of the default interface IAutoOutputNotify exposed by              
// the CoClass AutoOutputNotify. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoOutputNotify = class
    class function Create: IAutoOutputNotify;
    class function CreateRemote(const MachineName: string): IAutoOutputNotify;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TAutoOutputNotify
// Help String      : BARCO Automatic Output
// Default Interface: IAutoOutputNotify
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TAutoOutputNotifyProperties= class;
{$ENDIF}
  TAutoOutputNotify = class(TOleServer)
  private
    FIntf:        IAutoOutputNotify;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TAutoOutputNotifyProperties;
    function      GetServerProperties: TAutoOutputNotifyProperties;
{$ENDIF}
    function      GetDefaultInterface: IAutoOutputNotify;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IAutoOutputNotify);
    procedure Disconnect; override;
    procedure TriggerEvent(nEvent: Integer; nSubEvent: Integer);
    procedure SetupChange;
    procedure KillServer;
    property  DefaultInterface: IAutoOutputNotify read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TAutoOutputNotifyProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TAutoOutputNotify
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TAutoOutputNotifyProperties = class(TPersistent)
  private
    FServer:    TAutoOutputNotify;
    function    GetDefaultInterface: IAutoOutputNotify;
    constructor Create(AServer: TAutoOutputNotify);
  protected
  public
    property DefaultInterface: IAutoOutputNotify read GetDefaultInterface;
  published
  end;
{$ENDIF}


// *********************************************************************//
// The Class CoAutoOutputNotifyMachine provides a Create and CreateRemote method to          
// create instances of the default interface IAutoOutputNotifyMachine exposed by              
// the CoClass AutoOutputNotifyMachine. The functions are intended to be used by             
// clients wishing to automate the CoClass objects exposed by the         
// server of this typelibrary.                                            
// *********************************************************************//
  CoAutoOutputNotifyMachine = class
    class function Create: IAutoOutputNotifyMachine;
    class function CreateRemote(const MachineName: string): IAutoOutputNotifyMachine;
  end;


// *********************************************************************//
// OLE Server Proxy class declaration
// Server Object    : TAutoOutputNotifyMachine
// Help String      : BARCO Automatic Output for a single machine
// Default Interface: IAutoOutputNotifyMachine
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  TAutoOutputNotifyMachineProperties= class;
{$ENDIF}
  TAutoOutputNotifyMachine = class(TOleServer)
  private
    FIntf:        IAutoOutputNotifyMachine;
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    FProps:       TAutoOutputNotifyMachineProperties;
    function      GetServerProperties: TAutoOutputNotifyMachineProperties;
{$ENDIF}
    function      GetDefaultInterface: IAutoOutputNotifyMachine;
  protected
    procedure InitServerData; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor  Destroy; override;
    procedure Connect; override;
    procedure ConnectTo(svrIntf: IAutoOutputNotifyMachine);
    procedure Disconnect; override;
    procedure TriggerEvent(nEvent: Integer; nSubEvent: Integer);
    procedure SetupChange;
    procedure KillServer;
    procedure TriggerMachineEvent(nEvent: Integer; nSubEvent: Integer; nMachine: Smallint);
    property  DefaultInterface: IAutoOutputNotifyMachine read GetDefaultInterface;
  published
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
    property Server: TAutoOutputNotifyMachineProperties read GetServerProperties;
{$ENDIF}
  end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
// *********************************************************************//
// OLE Server Properties Proxy Class
// Server Object    : TAutoOutputNotifyMachine
// (This object is used by the IDE's Property Inspector to allow editing
//  of the properties of this server)
// *********************************************************************//
 TAutoOutputNotifyMachineProperties = class(TPersistent)
  private
    FServer:    TAutoOutputNotifyMachine;
    function    GetDefaultInterface: IAutoOutputNotifyMachine;
    constructor Create(AServer: TAutoOutputNotifyMachine);
  protected
  public
    property DefaultInterface: IAutoOutputNotifyMachine read GetDefaultInterface;
  published
  end;
{$ENDIF}


procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

class function CoAutoOutputNotify.Create: IAutoOutputNotify;
begin
  Result := CreateComObject(CLASS_AutoOutputNotify) as IAutoOutputNotify;
end;

class function CoAutoOutputNotify.CreateRemote(const MachineName: string): IAutoOutputNotify;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoOutputNotify) as IAutoOutputNotify;
end;

procedure TAutoOutputNotify.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{0466C463-FCEA-11D0-99F4-00A024C30CB6}';
    IntfIID:   '{0466C462-FCEA-11D0-99F4-00A024C30CB6}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TAutoOutputNotify.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IAutoOutputNotify;
  end;
end;

procedure TAutoOutputNotify.ConnectTo(svrIntf: IAutoOutputNotify);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TAutoOutputNotify.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TAutoOutputNotify.GetDefaultInterface: IAutoOutputNotify;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TAutoOutputNotify.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TAutoOutputNotifyProperties.Create(Self);
{$ENDIF}
end;

destructor TAutoOutputNotify.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TAutoOutputNotify.GetServerProperties: TAutoOutputNotifyProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TAutoOutputNotify.TriggerEvent(nEvent: Integer; nSubEvent: Integer);
begin
  DefaultInterface.TriggerEvent(nEvent, nSubEvent);
end;

procedure TAutoOutputNotify.SetupChange;
begin
  DefaultInterface.SetupChange;
end;

procedure TAutoOutputNotify.KillServer;
begin
  DefaultInterface.KillServer;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TAutoOutputNotifyProperties.Create(AServer: TAutoOutputNotify);
begin
  inherited Create;
  FServer := AServer;
end;

function TAutoOutputNotifyProperties.GetDefaultInterface: IAutoOutputNotify;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

class function CoAutoOutputNotifyMachine.Create: IAutoOutputNotifyMachine;
begin
  Result := CreateComObject(CLASS_AutoOutputNotifyMachine) as IAutoOutputNotifyMachine;
end;

class function CoAutoOutputNotifyMachine.CreateRemote(const MachineName: string): IAutoOutputNotifyMachine;
begin
  Result := CreateRemoteComObject(MachineName, CLASS_AutoOutputNotifyMachine) as IAutoOutputNotifyMachine;
end;

procedure TAutoOutputNotifyMachine.InitServerData;
const
  CServerData: TServerData = (
    ClassID:   '{DFB4F182-D43E-11D1-A212-006097C4C8E3}';
    IntfIID:   '{DFB4F181-D43E-11D1-A212-006097C4C8E3}';
    EventIID:  '';
    LicenseKey: nil;
    Version: 500);
begin
  ServerData := @CServerData;
end;

procedure TAutoOutputNotifyMachine.Connect;
var
  punk: IUnknown;
begin
  if FIntf = nil then
  begin
    punk := GetServer;
    Fintf:= punk as IAutoOutputNotifyMachine;
  end;
end;

procedure TAutoOutputNotifyMachine.ConnectTo(svrIntf: IAutoOutputNotifyMachine);
begin
  Disconnect;
  FIntf := svrIntf;
end;

procedure TAutoOutputNotifyMachine.DisConnect;
begin
  if Fintf <> nil then
  begin
    FIntf := nil;
  end;
end;

function TAutoOutputNotifyMachine.GetDefaultInterface: IAutoOutputNotifyMachine;
begin
  if FIntf = nil then
    Connect;
  Assert(FIntf <> nil, 'DefaultInterface is NULL. Component is not connected to Server. You must call ''Connect'' or ''ConnectTo'' before this operation');
  Result := FIntf;
end;

constructor TAutoOutputNotifyMachine.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps := TAutoOutputNotifyMachineProperties.Create(Self);
{$ENDIF}
end;

destructor TAutoOutputNotifyMachine.Destroy;
begin
{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
  FProps.Free;
{$ENDIF}
  inherited Destroy;
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
function TAutoOutputNotifyMachine.GetServerProperties: TAutoOutputNotifyMachineProperties;
begin
  Result := FProps;
end;
{$ENDIF}

procedure TAutoOutputNotifyMachine.TriggerEvent(nEvent: Integer; nSubEvent: Integer);
begin
  DefaultInterface.TriggerEvent(nEvent, nSubEvent);
end;

procedure TAutoOutputNotifyMachine.SetupChange;
begin
  DefaultInterface.SetupChange;
end;

procedure TAutoOutputNotifyMachine.KillServer;
begin
  DefaultInterface.KillServer;
end;

procedure TAutoOutputNotifyMachine.TriggerMachineEvent(nEvent: Integer; nSubEvent: Integer; 
                                                       nMachine: Smallint);
begin
  DefaultInterface.TriggerMachineEvent(nEvent, nSubEvent, nMachine);
end;

{$IFDEF LIVE_SERVER_AT_DESIGN_TIME}
constructor TAutoOutputNotifyMachineProperties.Create(AServer: TAutoOutputNotifyMachine);
begin
  inherited Create;
  FServer := AServer;
end;

function TAutoOutputNotifyMachineProperties.GetDefaultInterface: IAutoOutputNotifyMachine;
begin
  Result := FServer.DefaultInterface;
end;

{$ENDIF}

procedure Register;
begin
  RegisterComponents('ActiveX',[TAutoOutputNotify, TAutoOutputNotifyMachine]);
end;

end.
