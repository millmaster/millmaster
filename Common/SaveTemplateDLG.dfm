inherited SaveTemplateDLGForm: TSaveTemplateDLGForm
  Left = 343
  Top = 340
  Caption = '(*)Reinigereinstellungen speichern unter ...'
  ClientHeight = 425
  ClientWidth = 825
  KeyPreview = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel [0]
    Left = 0
    Top = 384
    Width = 825
    Height = 41
    Align = alBottom
    TabOrder = 4
    object bHelp: TmmButton
      Left = 725
      Top = 10
      Width = 90
      Height = 25
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe'
      TabOrder = 0
      Visible = True
      OnClick = bHelpClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  inherited bOK: TmmButton
    Left = 525
    Top = 395
    Width = 90
    ModalResult = 0
    OnClick = bOKClick
  end
  inherited bCancel: TmmButton
    Left = 625
    Top = 395
    Width = 90
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 300
    Height = 384
    Align = alLeft
    TabOrder = 2
    object sbOverrideTemplate: TmmSpeedButton
      Tag = 1
      Left = 15
      Top = 140
      Width = 24
      Height = 22
      GroupIndex = 1
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888880000000000000880330000008803088033000000880308803300000088
        0308803300000000030880333333333333088033000000003308803088888888
        0308803088888888030880308888888803088030888888880308803088888888
        0008803088888888080880000000000000088888888888888888}
      Margin = 1
      Visible = True
      OnClick = sbOverrideTemplateClick
      AutoLabel.Control = laOverrideTemplate
      AutoLabel.Distance = 5
      AutoLabel.LabelPosition = lpRight
    end
    object sbNewTemplate: TmmSpeedButton
      Tag = 1
      Left = 15
      Top = 90
      Width = 24
      Height = 22
      GroupIndex = 1
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DD0000009999
        900DD22222999999999DD2FFF799999F999DD2FFF999999FF999D2F4499FFFFF
        FF99D2FFF99FFFFFFFF9D2F4499FFFFFFF99D2FFF999999FF999D2F44F99999F
        999DD2FFFF999999999DD2FFBB839999920DD2FFF373B0FFF0DDD2FF1838F0FF
        0DDDD2FFFFFFF0F0DDDDD2222222200DDDDDDDDDDDDDDDDDDDDD}
      Margin = 1
      Visible = True
      OnClick = sbOverrideTemplateClick
      AutoLabel.Control = laNewTemplate
      AutoLabel.Distance = 5
      AutoLabel.LabelPosition = lpRight
    end
    object sbTemplateOnly: TmmSpeedButton
      Tag = 1
      Left = 15
      Top = 40
      Width = 24
      Height = 22
      GroupIndex = 1
      Down = True
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        888888888888888888888800000000000888880FFFFFFFFF0888880FFFFFFFFF
        0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF
        0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFF0000888880FFFFFF0F0
        8888880FFFFFF008888888000000008888888888888888888888}
      Margin = 1
      Visible = True
      OnClick = sbOverrideTemplateClick
      AutoLabel.Control = laTemplateOnly
      AutoLabel.Distance = 5
      AutoLabel.LabelPosition = lpRight
    end
    object laOverrideTemplate: TmmLabel
      Left = 44
      Top = 136
      Width = 250
      Height = 30
      AutoSize = False
      Caption = '0'
      FocusControl = sbOverrideTemplate
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laNewTemplate: TmmLabel
      Left = 44
      Top = 86
      Width = 250
      Height = 30
      AutoSize = False
      Caption = '0'
      FocusControl = sbNewTemplate
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laTemplateOnly: TmmLabel
      Left = 44
      Top = 36
      Width = 250
      Height = 30
      AutoSize = False
      Caption = '0'
      FocusControl = sbTemplateOnly
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object sbRenameTemplate: TmmSpeedButton
      Tag = 1
      Left = 15
      Top = 190
      Width = 24
      Height = 22
      GroupIndex = 1
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888870000000000CCCCC7FFFFFFFFFFCCCCC70000F0F000F
        FCF070FF0FFF0FF0FCF07F000FFF0FF0FCF07FFF0FFF0FF0FCF070000FFF000F
        FCF07FFFFFFF0FFFFCF07FFFFFFF0FFFFCF07FFFFFFFFFFFFCF07FFFFFFFFFFC
        CCCC77777777777CCCCC88888888888888888888888888888888}
      Margin = 1
      Visible = True
      OnClick = sbOverrideTemplateClick
      AutoLabel.Control = laRenameTemplate
      AutoLabel.Distance = 5
      AutoLabel.LabelPosition = lpRight
    end
    object laRenameTemplate: TmmLabel
      Left = 44
      Top = 186
      Width = 250
      Height = 30
      AutoSize = False
      Caption = '0'
      FocusControl = sbRenameTemplate
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object Panel1: TPanel
    Left = 300
    Top = 0
    Width = 525
    Height = 384
    Align = alClient
    TabOrder = 3
    object lbHinweis: TmmLabel
      Left = 10
      Top = 15
      Width = 47
      Height = 13
      Caption = '(*)Hinweis'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pRemark: TPanel
      Left = 10
      Top = 30
      Width = 495
      Height = 100
      BevelOuter = bvLowered
      Color = clInfoBk
      TabOrder = 0
      object lbRemark: TmmLabel
        Left = 30
        Top = 5
        Width = 455
        Height = 90
        AutoSize = False
        Caption = '0'
        Visible = True
        WordWrap = True
        AutoLabel.LabelPosition = lpLeft
      end
      object Image1: TImage
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        AutoSize = True
        Picture.Data = {
          07544269746D617036030000424D360300000000000036000000280000001000
          0000100000000100180000000000000300000000000000000000000000000000
          0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
          E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
          FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
          FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
          84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
          638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
          FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
          DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
          CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
          FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
          FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
          CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
          716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
          FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
          DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
          695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
          FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
          FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
          7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
          FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
          FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
          FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
          FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
          DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
          FFFF}
      end
    end
    object pSavePanel: TmmPanel
      Left = 5
      Top = 140
      Width = 510
      Height = 235
      BevelOuter = bvNone
      TabOrder = 1
    end
    object pChangeTemplate: TmmPanel
      Left = 96
      Top = 16
      Width = 505
      Height = 230
      TabOrder = 2
      Visible = False
      OnResize = pChangeTemplateResize
      object laStyle: TmmLabel
        Left = 5
        Top = 54
        Width = 109
        Height = 13
        Caption = '(*)Ausgewaehlte Artikel'
        Visible = True
        AutoLabel.Distance = 3
        AutoLabel.LabelPosition = lpLeft
      end
      object laTemplateName_1: TmmLabel
        Left = 8
        Top = 5
        Width = 133
        Height = 13
        Caption = '(*)Name der Reinigervorlage'
        FocusControl = edTemplateName_1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laColor_OverrideTemplate: TmmLabel
        Left = 275
        Top = 5
        Width = 135
        Height = 13
        Caption = '(*)Farbe der  Reinigervorlage'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbStyles: TmmScrollBox
        Left = 8
        Top = 73
        Width = 220
        Height = 130
        Color = 13160660
        ParentColor = False
        TabOrder = 0
      end
      object edTemplateName_1: TmmEdit
        Left = 8
        Top = 20
        Width = 220
        Height = 21
        Color = 13160660
        MaxLength = 50
        ReadOnly = True
        TabOrder = 1
        Text = '0'
        Visible = True
        AutoLabel.Control = laTemplateName_1
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clSilver
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object mmPanel2: TmmPanel
        Left = 275
        Top = 20
        Width = 33
        Height = 21
        BevelInner = bvRaised
        BevelOuter = bvNone
        BevelWidth = 2
        BorderWidth = 1
        TabOrder = 2
        object pColor: TmmPanel
          Left = 5
          Top = 5
          Width = 15
          Height = 11
          BevelOuter = bvNone
          TabOrder = 0
        end
      end
    end
    object pNewTemplate: TmmPanel
      Left = 16
      Top = 136
      Width = 505
      Height = 221
      TabOrder = 3
      OnResize = pNewTemplateResize
      object laTemplateName: TmmLabel
        Left = 5
        Top = 5
        Width = 133
        Height = 13
        Caption = '(*)Name der Reinigervorlage'
        FocusControl = edTemplateName
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laColor: TmmLabel
        Left = 275
        Top = 5
        Width = 135
        Height = 13
        Caption = '(*)Farbe der  Reinigervorlage'
        FocusControl = bColor
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bColor: TmmColorButton
        Left = 275
        Top = 20
        Width = 33
        Height = 21
        AutoLabel.Control = laColor
        AutoLabel.LabelPosition = lpTop
        Caption = '&Weitere...'
        Color = clBlack
        OnChangeColor = bColorChangeColor
        TabOrder = 0
        TabStop = True
        Visible = True
      end
      object edTemplateName: TmmEdit
        Left = 5
        Top = 20
        Width = 220
        Height = 21
        Color = clWindow
        MaxLength = 50
        TabOrder = 1
        Text = '0'
        Visible = True
        OnChange = edTemplateNameChange
        OnKeyDown = edTemplateNameKeyDown
        AutoLabel.Control = laTemplateName
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object pStyles: TmmPanel
        Left = 1
        Top = 48
        Width = 503
        Height = 172
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object laAllStyles: TmmLabel
          Left = 5
          Top = 4
          Width = 57
          Height = 13
          Caption = '(*)Artikelliste'
          FocusControl = lbSource
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object sbAssignedStyles: TmmSpeedButton
          Left = 5
          Top = 155
          Width = 24
          Height = 22
          Hint = '(*)Zeigt zugewiesene Artikel von Vorlage'
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00DDDDDDDDDDDD
            DDDDDDDDDDDDDDDDDDDD0000DDDDDD0000D07CC0DDDDDD7CC0D77C80DDDDDD7C
            80D77CC0D0000D7CC0D77C80D7990D7C80D77CC0D7990D7CC0D77C80D7990D7C
            80D77CC0D7990D7CC0D77770D7990D7770D7DDDDD7990DDDDDDDDDDDD7990DDD
            DDDDDDDDD7770DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD}
          ParentShowHint = False
          ShowHint = True
          Visible = False
          OnClick = sbAssignedStylesClick
          AutoLabel.LabelPosition = lpLeft
        end
        object laFilter: TLabel
          Left = 232
          Top = 148
          Width = 160
          Height = 13
          Caption = '(*)Zugewiesene Artikel selektieren'
          Visible = False
        end
        object bMoveRight: TmmSpeedButton
          Left = 237
          Top = 55
          Width = 25
          Height = 25
          Anchors = [akTop]
          Glyph.Data = {
            92000000424D9200000000000000760000002800000007000000070000000100
            0400000000001C00000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00880888808800
            88808800088088000080880008808800888088088880}
          Visible = True
          OnClick = bMoveRightClick
          AutoLabel.LabelPosition = lpLeft
        end
        object bMoveLeft: TmmSpeedButton
          Left = 237
          Top = 80
          Width = 25
          Height = 25
          Anchors = [akTop]
          Glyph.Data = {
            92000000424D9200000000000000760000002800000007000000070000000100
            0400000000001C00000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888808808880
            08808800088080000880880008808880088088880880}
          Visible = True
          OnClick = bMoveLeftClick
          AutoLabel.LabelPosition = lpLeft
        end
        object laSelectedStyles: TmmLabel
          Left = 274
          Top = 4
          Width = 109
          Height = 13
          Caption = '(*)Ausgewaehlte Artikel'
          FocusControl = lbDest
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object lbSource: TmmListBox
          Left = 5
          Top = 23
          Width = 220
          Height = 130
          Enabled = True
          ItemHeight = 13
          MultiSelect = True
          Sorted = True
          TabOrder = 0
          Visible = True
          OnDblClick = lbSourceDblClick
          OnKeyUp = lbSourceKeyUp
          AutoLabel.Control = laAllStyles
          AutoLabel.Distance = 6
          AutoLabel.LabelPosition = lpTop
        end
        object lbDest: TmmListBox
          Left = 274
          Top = 23
          Width = 220
          Height = 130
          Enabled = True
          ItemHeight = 13
          MultiSelect = True
          Sorted = True
          TabOrder = 1
          Visible = True
          OnDblClick = lbDestDblClick
          OnKeyUp = lbDestKeyUp
          AutoLabel.Control = laSelectedStyles
          AutoLabel.Distance = 6
          AutoLabel.LabelPosition = lpTop
        end
        object cbActiveStyles: TCheckBox
          Left = 100
          Top = 3
          Width = 120
          Height = 17
          Caption = '(*)Aktive Artikel'
          Checked = True
          State = cbChecked
          TabOrder = 2
          OnClick = cbActiveStylesClick
        end
      end
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 112
    Top = 440
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 32
    Top = 256
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
