//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: MsgGlobal.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 20.03.2001  0.00  khp | Datei erstellt
//=========================================================================================*)

{ unit MMAlert -----------------------------------------------------------------------------}
//   In der Unit MsgGlobal sind sprachabhaenige Texte abgelegt welche von Services oder Consolen Appplikationen
//   verwendet werden.


unit MsgGlobal;


interface

const
  cMsgQOfflimit='(*)MILLMASTER QUALITAET-OFFLIMIT ALARM'; // ivlm

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
 
