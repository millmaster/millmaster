inherited mmGraphOptionsDlg: TmmGraphOptionsDlg
  Left = 509
  Top = 166
  ActiveControl = cbYLeftAutoScaling
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = '(60)Grafik-Optionen'
  ClientHeight = 273
  ClientWidth = 245
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TmmPanel
    Left = 0
    Top = 0
    Width = 245
    Height = 243
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object mmPageControl1: TmmPageControl
      Left = 5
      Top = 5
      Width = 235
      Height = 233
      ActivePage = tsYAxis
      Align = alClient
      HotTrack = True
      TabOrder = 0
      object tsOptions: TTabSheet
        Caption = '(20)Optionen'
        object rgBarType: TmmRadioGroup
          Left = 5
          Top = 5
          Width = 217
          Height = 195
          Caption = '(40)Grafiktyp'
          Items.Strings = (
            '(30)Balken nebeneinander'
            '(30)Balken gestapelt'
            '(30)Balken gestapelt 100%')
          TabOrder = 0
        end
      end
      object tsYAxis: TTabSheet
        Caption = '(20)Y-Achsen'
        ImageIndex = 1
        object GroupBox1: TmmGroupBox
          Left = 5
          Top = 5
          Width = 217
          Height = 95
          Caption = '(35)Y-Achsen Skalierung (Linien)'
          TabOrder = 0
          object laLeftMin: TmmLabel
            Left = 5
            Top = 43
            Width = 137
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Vorgabe Y(min)'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laLeftMax: TmmLabel
            Left = 5
            Top = 68
            Width = 137
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Vorgabe Y(max)'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbYLeftAutoScaling: TmmCheckBox
            Left = 10
            Top = 17
            Width = 200
            Height = 17
            Caption = '(30)Automatisch'
            TabOrder = 0
            Visible = True
            OnClick = cbYLeftAutoScalingClick
            AutoLabel.LabelPosition = lpLeft
          end
          object edLeftMin: TNumEdit
            Left = 147
            Top = 40
            Width = 60
            Height = 21
            Decimals = 2
            Digits = 12
            Masks.PositiveMask = '#,##0'
            NumericType = ntGeneral
            TabOrder = 1
            UseRounding = True
            Validate = False
            ValidateString = 
              '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
              's %s'
            OnExit = edLeftMinExit
          end
          object edLeftMax: TNumEdit
            Left = 147
            Top = 65
            Width = 60
            Height = 21
            Decimals = 2
            Digits = 12
            Masks.PositiveMask = '#,##0'
            NumericType = ntGeneral
            TabOrder = 2
            UseRounding = True
            Validate = False
            ValidateString = 
              '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
              's %s'
            OnExit = edLeftMinExit
          end
        end
        object GroupBox2: TmmGroupBox
          Left = 5
          Top = 105
          Width = 217
          Height = 95
          Caption = '(35)Y-Achsen Skalierung (Balken)'
          TabOrder = 1
          object laRightMin: TmmLabel
            Left = 5
            Top = 43
            Width = 137
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Vorgabe Y(min)'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object laRightMax: TmmLabel
            Left = 5
            Top = 68
            Width = 137
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = '(30)Vorgabe Y(max)'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbYRightAutoScaling: TmmCheckBox
            Left = 10
            Top = 17
            Width = 200
            Height = 17
            Caption = '(30)Automatisch'
            TabOrder = 0
            Visible = True
            OnClick = cbYRightAutoScalingClick
            AutoLabel.LabelPosition = lpLeft
          end
          object edRightMin: TNumEdit
            Left = 147
            Top = 40
            Width = 60
            Height = 21
            Decimals = 2
            Digits = 12
            Masks.PositiveMask = '#,##0'
            NumericType = ntGeneral
            TabOrder = 1
            UseRounding = True
            Validate = False
            ValidateString = 
              '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
              's %s'
            OnExit = edLeftMinExit
          end
          object edRightMax: TNumEdit
            Left = 147
            Top = 65
            Width = 60
            Height = 21
            Decimals = 2
            Digits = 12
            Masks.PositiveMask = '#,##0'
            NumericType = ntGeneral
            TabOrder = 2
            UseRounding = True
            Validate = False
            ValidateString = 
              '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
              's %s'
            OnExit = edLeftMinExit
          end
        end
      end
      object tsXAxis: TTabSheet
        Caption = '(20)X-Achsen'
        ImageIndex = 2
        TabVisible = False
        object GroupBox3: TmmGroupBox
          Left = 5
          Top = 5
          Width = 240
          Height = 195
          Caption = 'GroupBox3'
          TabOrder = 0
        end
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 243
    Width = 245
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 5
      Top = 1
      Width = 75
      Height = 25
      Action = acOK
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 85
      Top = 1
      Width = 75
      Height = 25
      Action = acCancel
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 165
      Top = 1
      Width = 75
      Height = 25
      Action = acHelp
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 128
    Top = 34
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Dialog schliessen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 1
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfe aufrufen...'
      ImageIndex = 2
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 158
    Top = 34
    TargetsData = (
      1
      6
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0)
      (
        ''
        'Items'
        0)
      (
        ''
        'Text'
        0)
      (
        ''
        'Tabs'
        0)
      (
        ''
        'Lines'
        0))
  end
end
