(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: NCButton
| Projectpart...: Allgemein
| Subpart.......: -
| Process(es)...: -
| Description...: Button f�r die Non Client Area eines Controls bsierend auf 
|                 TCaptionButton von Aleksey Kuznetsov (http://www.utilmind.com)
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01 / ModelMaker 6.20 1402
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 23.04.2003 1.00 Lok | Datei erstellt
|=============================================================================*)
(*-----------------------------------------------------------
  ANWENDUNG:
  
    mCollapseButton := TNCButton.Create(mTree);
    with mCollapseButton do begin
      mNCImages.GetBitmap(cCollapseImage, Glyph); // ImageList
      Top := cButtonTop;                          //  2
      Width := cButtonWidthHeight;                // 14
      Height := cButtonWidthHeight;               // 14
      Left := mTree.Width - Width - cButtonFrame; //  3
      OnClick := FullCollapse;                    // TNotifyEvent
    end;// with mCollapseButton do begin
    
-------------------------------------------------------------*)

unit NCButton;

interface

uses
  Windows,
  Classes, Controls, Forms, Messages, Graphics, sysUtils;

type
  (*: Klasse:        TNCButton
      Vorg�nger:     TComponent
      Kategorie:     No category
      Kurzbeschrieb: Button der im Non Client bereich eines Steuerelements dargestellt wird 
      Beschreibung:  
                     - *)
  //1 Button der im Non Client bereich eines Steuerelements dargestellt wird 
  TNCButton = class (TComponent)
  private
    FButtonDown: Boolean;
    FDown: Boolean;
    FGlyph: TBitmap;
    FHeight: Integer;
    FLeft: Integer;
    FNonFormYFrame: Integer;
    FOnClick: TNotifyEvent;
    FRightImageMargin: Integer;
    FTop: Integer;
    FVisible: Boolean;
    FWidth: Integer;
    mButtonRect: TRect;
    mCanvas: TCanvas;
    mCtrlMsg: Word;
    mParentControl: TWinControl;
    mPrevParentWndProc: Pointer;
    mSeekAndDestroy: Boolean;
    //1 Ersetzt die WndProc des Elternfensters um an den NC Bereich heranzukommen 
    procedure NewParentWndProc(var Msg: TMessage);
    //1 Zeichet das Bitmap f�r den Button 
    procedure PaintCaption(aDown: Boolean);
    //1 Zugriffsmethode f�r Glyph 
    procedure SetGlyph(Value: TBitmap);
    //1 Setzt den Abstand zum rechten Rand des Controls 
    procedure SetLeft(Value: Integer);
    //1 Setzt die Sichtbarkeit des Buttons 
    procedure SetVisible(Value: Boolean);
  public
    //1 Konstruktor 
    constructor Create(AOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 H�he des Buttons 
    property Height: Integer read FHeight write FHeight;
    //1 Vertikaler Abstand von oben, wenn das Control kein Form ist 
    property NonFormYFrame: Integer read FNonFormYFrame write FNonFormYFrame;
    //1 Rechter Rand f�r das Bitmap auf dem Button 
    property RightImageMargin: Integer read FRightImageMargin write FRightImageMargin;
    //1 Oberer Rand des Buttons 
    property Top: Integer read FTop write FTop;
    //1 Breite des Buttons 
    property Width: Integer read FWidth write FWidth;
  published
    //1 Bitmap das im Button dargestellt wird 
    property Glyph: TBitmap read FGlyph write SetGlyph;
    //1 Abstand vom Rechten Rand des Controls 
    property Left: Integer read FLeft write SetLeft;
    //1 Klick Event des Buttons 
    property OnClick: TNotifyEvent read FOnClick write FOnClick;
    //1 True, wenn der Button sichtbar ist 
    property Visible: Boolean read FVisible write SetVisible;
  end;
  
procedure Register;

implementation

const
  cNotUsedCtrlMsg: Word = 666;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (AOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TNCButton.Create(AOwner: TComponent);
var
  xNewWndProc: Pointer;
begin
  inherited Create(AOwner);

//: ----------------------------------------------
  // Im NC Bereich dieses Controls wird gemalt
  mParentControl := TWinControl(aOwner);
  
  FGlyph := TBitmap.Create;
  mCanvas := TCanvas.Create;
  FVisible := True;
  
  mCtrlMsg := cNotUsedCtrlMsg;
  inc(cNotUsedCtrlMsg);
  
  // Breite und H�he initialisieren
  FWidth := GetSystemMetrics(sm_cxSize);
  FHeight := GetSystemMetrics(sm_cySize);

//: ----------------------------------------------
  // Alte WndProc sichern
  mPrevParentWndProc := Pointer(GetWindowLong(mParentControl.Handle, GWL_WNDPROC));
  
  // Message Hook f�r das PArent Control setzten
  xNewWndProc := MakeObjectInstance(NewParentWndProc);
  SetWindowLong(mParentControl.Handle, GWL_WNDPROC, LongInt(xNewWndProc));
end;// TNCButton.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TNCButton.Destroy;
begin
  if not mSeekAndDestroy then begin
    Visible := False;
    SetWindowLong(mParentControl.Handle, GWL_WNDPROC, LongInt(mPrevParentWndProc));
  end;// if not mSeekAndDestroy then begin
  
  FreeAndNil(mCanvas);
  FreeAndNil(FGlyph);

//: ----------------------------------------------
  inherited Destroy;
end;// TNCButton.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           NewParentWndProc
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (Msg)
 *
 *  Kurzbeschreibung: Ersetzt die WndProc des Elternfensters um an den NC Bereich heranzukommen
 *  Beschreibung:     
                      Die originale Prozedur wird aufgerufen.
 --------------------------------------------------------------------*)
procedure TNCButton.NewParentWndProc(var Msg: TMessage);
var
  Pnt: TPoint;
begin
  // Original Code (leider nicht sonderlich klar strukturiert)
  with Msg do begin
    if FVisible then
      // Originale Prozedur aufrufen
      Result := CallWindowProc(mPrevParentWndProc, mParentControl.Handle, Msg, WParam, LParam);
      if (Msg = wm_NCPaint) or (Msg = wm_NCActivate) then
        PaintCaption(False)
      else
        if Msg = wm_NCHitTest then
          if Result = HTBORDER then begin
            Pnt.x := LoWord(lParam);
            ScreenToClient(mParentControl.Handle, Pnt);
            if (Pnt.x > mButtonRect.Left) and (Pnt.x < mButtonRect.Right) then begin
              if not FDown and FButtonDown then
                PaintCaption(True);
              Result := mCtrlMsg
            end
            else
              if FDown then
                PaintCaption(False);
          end
          else
            if FDown then
              PaintCaption(False)
            else
          else
            if (Msg = wm_NCLButtonDown) or (Msg = wm_NCLButtonDblClk) then
              if wParam = mCtrlMsg then begin
                if not FDown then
                  PaintCaption(True);
                if not FButtonDown then begin
                  FButtonDown := True;
                  SetCapture(mParentControl.Handle);
                end;
              end
              else begin
                if FDown then
                  PaintCaption(False);
                if FButtonDown then begin
                  FButtonDown := False;
                  ReleaseCapture;
                end;
              end
            else
              if (Msg = wm_NCLButtonUp) or (Msg = wm_LButtonUp) then begin
                if FButtonDown then begin
                  FButtonDown := False;
                  ReleaseCapture;
                  if FDown and Assigned(FOnClick) then
                    FOnClick(Self);
                end;
                if FDown then
                  PaintCaption(False);
              end
              else
                if (Msg = wm_Close) or (Msg = wm_Destroy) then
                  mSeekAndDestroy := True;
  end;// with Msg do begin
end;// TNCButton.NewParentWndProc cat:No category

//:-------------------------------------------------------------------
(*: Member:           PaintCaption
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (aDown)
 *
 *  Kurzbeschreibung: Zeichet das Bitmap f�r den Button
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TNCButton.PaintCaption(aDown: Boolean);
var
  xDC: hDC;
  xRect: TRect;
  xImage: TBitmap;
  xLeftX: Integer;
  xShift: Byte;
  
  procedure DrawUpFrame;
  begin
    with mCanvas do
     begin
      Pen.Color := clBtnHighlight;
      MoveTo(xLeftX, FTop + FHeight + 1);
      LineTo(xLeftX, FTop);
      LineTo(xLeftX + FWidth + 3, FTop);
      Pen.Color := clBlack;
      MoveTo(xLeftX, FTop + FHeight + 2);
      LineTo(xLeftX + FWidth + 2, FTop + FHeight + 2);
      LineTo(xLeftX + FWidth + 2, FTop - 1);
      Pen.Color := clGray;
      MoveTo(xLeftX + FWidth + 1, FTop + 1);
      LineTo(xLeftX + FWidth + 1, FTop + FHeight + 1);
      LineTo(xLeftX, FTop + FHeight + 1);
      xShift := 1;
     end;
  end;
  
  procedure DrawDownFrame;
  begin
    with mCanvas do
     begin
      Pen.Color := clBlack;
      MoveTo(xLeftX, FTop + FHeight + 1);
      LineTo(xLeftX, FTop);
      LineTo(xLeftX + FWidth + 3, FTop);
      Pen.Color := clBtnHighlight;
      MoveTo(xLeftX, FTop + FHeight + 2);
      LineTo(xLeftX + FWidth + 2, FTop + FHeight + 2);
      LineTo(xLeftX + FWidth + 2, FTop - 1);
      Pen.Color := clGray;
      MoveTo(xLeftX + FWidth, FTop + 1);
      LineTo(xLeftX + 1, FTop + 1);
      LineTo(xLeftX + 1, FTop + FHeight + 1);
      Pen.Color := clSilver;
      MoveTo(xLeftX + FWidth + 1, FTop + 1);
      LineTo(xLeftX + FWidth + 1, FTop + FHeight + 1);
      LineTo(xLeftX, FTop + FHeight + 1);
      xShift := 2;
     end;
  end;
  
begin
  FDown := aDown;
  if FVisible then begin
    xDC := GetWindowDC(mParentControl.Handle);
    try
      mCanvas.Handle := xDC;
      xImage := TBitmap.Create;
      GetWindowRect(mParentControl.Handle, xRect);
      xRect.Right := xRect.Right - xRect.Left;
  
      xLeftX := FLeft - FTop;
  
      with mButtonRect do begin
        Left := xLeftX - FTop;
        Top := FTop;
        Right := Left + FWidth + 3;
        Bottom := FHeight + 2;
      end;// with mButtonRect do begin
  
      if aDown then
        DrawDownFrame
      else
        DrawUpFrame;
  
      StretchBlt(xDC, xLeftX + xShift,
                 FTop + xShift,
                 FWidth - RightImageMargin, FHeight,
                 FGlyph.Canvas.Handle, 0, 0,
                 FGlyph.Width, FGlyph.Height,
                 srcCopy);
  
      xImage.Free;
    finally
      ReleaseDC(mParentControl.Handle, xDC);
    end;
  end;
end;// TNCButton.PaintCaption cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetGlyph
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Glyph
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TNCButton.SetGlyph(Value: TBitmap);
begin
  if FGlyph <> Value then begin
    FGlyph.Assign(Value);
    SendMessage(mParentControl.Handle, wm_NCActivate, 0, 0);
  end;// if FGlyph <> Value then begin
end;// TNCButton.SetGlyph cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetLeft
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt den Abstand zum rechten Rand des Controls
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TNCButton.SetLeft(Value: Integer);
begin
  if FLeft <> Value then begin
    FLeft := Value;
    SendMessage(mParentControl.Handle, wm_NCActivate, 0, 0);
  end;// if FLeft <> Value then begin
end;// TNCButton.SetLeft cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetVisible
 *  Klasse:           TNCButton
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die Sichtbarkeit des Buttons
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TNCButton.SetVisible(Value: Boolean);
begin
  if FVisible <> Value then begin
    FVisible := Value;
    SendMessage(mParentControl.Handle, wm_NCActivate, 0, 0);
  end;// if FVisible <> Value then begin
end;// TNCButton.SetVisible cat:No category

procedure Register;
begin
end;

end.
