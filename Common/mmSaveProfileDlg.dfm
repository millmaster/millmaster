inherited mmSaveProfileEditor: TmmSaveProfileEditor
  Left = 565
  Top = 527
  ActiveControl = comProfile
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = '(50)Profil speichern unter...'
  ClientHeight = 90
  ClientWidth = 245
  ParentFont = False
  Font.Color = clBlack
  KeyPreview = True
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object MainPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 245
    Height = 60
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object gbProfilename: TmmGroupBox
      Left = 5
      Top = 5
      Width = 235
      Height = 50
      Caption = '(50)Profilname'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      CaptionSpace = True
      object comProfile: TmmComboBox
        Left = 10
        Top = 20
        Width = 215
        Height = 21
        Color = clWindow
        DropDownCount = 15
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ItemHeight = 13
        ParentFont = False
        Sorted = True
        TabOrder = 0
        Visible = True
        OnChange = comProfileChange
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 60
    Width = 245
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 5
      Top = 1
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 85
      Top = 1
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 165
      Top = 1
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 114
    Top = 2
    object acOK: TAction
      Caption = '(9)Speichern'
      Hint = '(*)Auswahl uebernehmen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 1
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ImageIndex = 3
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 144
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Lines'
        0))
  end
end
