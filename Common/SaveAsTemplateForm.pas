(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SaveAsTemplateForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 23.12.1999  1.00  Mg  | Datei erstellt
| 05.12.2000  1.01  SDo | Property TemplateName extended (read, write)
|                       | Check TemplateName zero string ( zero string is not allow )
|                       | New Property OverrideAllow (used in
| 15.08.2001  1.02  Nue | No CheckTemplateNameAvailability
| 12.12.2001  1.03  Nue | CheckTemplateNameAvailability active again; SensingHeadClass added.
| 13.12.2001  1.04  Wss | - Property SingleName added to check for duplicate names only
                            in CheckTemplateNameAvailablility (used from ClearerAssistant)
                          - Const msg changed to resourcestring
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 17.01.2005  2.00  Nue | Umbau auf XML V.5.0.
| 06.09.2005  2.01  Nue | Anpassen SetName-Handling (-X). Beschränken TemplateName auf 50 Zeichen.
|                       | MaxValue-Property von edTemplateName von 20 auf 50 gesetzt.
|=============================================================================*)
unit SaveAsTemplateForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmLabel, Mask,
  mmMaskEdit, mmColorButton, IvDictio, IvMulti, IvEMulti,
  mmTranslator, MMUGlobal,
  XMLDef, YMParaDef, mmButton, mmAdoDataset;

resourcestring
  rsTemplateNameAlreadyAvailableMsg = '(*)Der Name existiert bereits, bitte einen anderen Namen angeben.'; //ivlm
  rsTemplateNameIsEmptyMsg = '(*)Bitte geben Sie einen Vorlagennamen ein.'; //ivlm
  rsOverrideTemplate0 = '(*)Die Vorlage %s existiert bereits.'; //ivlm
  rsOverrideTemplate1 = '(*)Wollen Sie diese Vorlage ueberschreiben?'; //ivlm

type
  TSaveAsTemplate = class(TDialogBottom)
    edTemplateName: TmmMaskEdit;
    mmLabel1: TmmLabel;
    bColor: TmmColorButton;
    mmTranslator1: TmmTranslator;
    procedure bOKClick(Sender: TObject);
  private
    mQuery: TmmAdoDataSet;
    fTemplateName: string;
//    fColor: TColor;
    fOverrideAllow: Boolean;
    fSensingHeadClass: TSensingHeadClass;
    fSingleNameOnly: Boolean; //Nue 12.12.01
    procedure SetColor(aColor: TColor);
    function GetColor: TColor;
    procedure SetTemplateName(const Value: string);
    procedure SetOverrideAllow(const Value: Boolean);
  public
    constructor Create(aOwner: TComponent; aQuery: TmmAdoDataset); reintroduce; virtual;
    function CheckTemplateNameAvailability(aName: string): boolean; // if aName is already in us the result will be true
    property Color: TColor read GetColor write SetColor;
    property SingleNameOnly: Boolean read fSingleNameOnly write fSingleNameOnly default False;
    property SensingHeadClass: TSensingHeadClass read fSensingHeadClass write fSensingHeadClass; //Nue 13.12.01
    property TemplateName: string read fTemplateName write SetTemplateName;
  published
    property OverrideAllow: Boolean read fOverrideAllow write SetOverrideAllow default False;
  end;
implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  LoepfeGlobal, BaseGlobal;

const

  cCheckTemplateNameAndTKAvailability =
    'select c_YM_set_name from t_xml_ym_settings ' +
    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name ' +
    'and c_head_class = :c_head_class';

  cCheckSingleTemplateNameAvailability =
    'select c_YM_set_name from t_xml_ym_settings ' +
    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name ';
//------------------------------------------------------------------------------
constructor TSaveAsTemplate.Create(aOwner: TComponent; aQuery: TmmAdoDataset);
begin
  inherited Create(aOwner);
  bColor.ColorArray := cMMProdGroupColors;

  mQuery := aQuery;
  fOverrideAllow := False;
  fSingleNameOnly := False;
  fTemplateName := '';
end;
//------------------------------------------------------------------------------
function TSaveAsTemplate.CheckTemplateNameAvailability(aName: string): boolean;
begin
//  Result := False;
{Nue:15.8.01}
  try
//    Result := true;
    with mQuery do begin
      Close;
      if fSingleNameOnly then
        CommandText := cCheckSingleTemplateNameAvailability
      else begin
        CommandText := cCheckTemplateNameAndTKAvailability;
        Parameters.ParamByName('c_head_class').value := Ord(fSensingHeadClass);
      end;

      Parameters.ParamByName('c_YM_set_name').value := edTemplateName.Text;
      Open;
      Result := not EOF;  // ADO Conform
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TSaveAsTemplate.CheckTemplateNameAvailability failed. ' + e.Message);
    end;
  end;
{
  try
    Result := true;
    with mQuery do begin
      Close;
      SQL.Text := cCheckTemplateNameAndTKAvailability;
      ParamByName('c_YM_set_name').AsString := edTemplateName.Text;
      ParamByName('c_head_class').AsInteger := Ord(fSensingHeadClass);
      Open;
      if EOF then begin
        Result := false;
      end;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TSaveAsTemplate.CheckTemplateNameAvailability failed. ' + e.Message);
    end;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TSaveAsTemplate.bOKClick(Sender: TObject);
var
  xExists: Boolean;
begin
  try
    fTemplateName := Trim(edTemplateName.Text);
    if (fTemplateName <> '') then begin
      xExists := CheckTemplateNameAvailability(fTemplateName);
         // Ueberschreiben erlaubt
      if fOverrideAllow then begin
        if not xExists then begin
          Self.Close;
          ModalResult := mrOk;
        end else
          if MessageDlg(Format(rsOverrideTemplate0 + cCRLF + rsOverrideTemplate1, [fTemplateName]), mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
            Self.Close;
            ModalResult := mrOk;
          end;                          // else ModalResult := mrCancel;
      end else
        // Ueberschreiben nicht erlaubt
        if not xExists then begin
          Self.Close;
          ModalResult := mrOk;
        end else begin
          WarningMsg(rsTemplateNameAlreadyAvailableMsg);
        end;
    end else
      // Kein Text
      WarningMsg(rsTemplateNameIsEmptyMsg);
  except
    on e: Exception do begin
      SystemErrorMsg_('TSaveAsTemplate.bOKClick failed. ' + e.Message);
      Close;
      ModalResult := mrCancel;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSaveAsTemplate.SetColor(aColor: TColor);
begin
  bColor.Color := aColor;
end;
//------------------------------------------------------------------------------
function TSaveAsTemplate.GetColor: TColor;
begin
  Result := bColor.Color;
end;
//------------------------------------------------------------------------------
procedure TSaveAsTemplate.SetTemplateName(const Value: string);
begin
  fTemplateName := Value;
  edTemplateName.Text := fTemplateName;
end;

procedure TSaveAsTemplate.SetOverrideAllow(const Value: Boolean);
begin
  fOverrideAllow := Value;
end;

end.

