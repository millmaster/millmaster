inherited mmDeleteProfileEditor: TmmDeleteProfileEditor
  Left = 474
  Width = 247
  Height = 268
  ActiveControl = lbUser
  BorderIcons = [biSystemMenu]
  Caption = '(40)Einstellungs-Profil loeschen'
  Constraints.MaxWidth = 247
  Constraints.MinWidth = 247
  ParentFont = False
  Font.Color = clBlack
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 239
    Height = 211
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 2
    TabOrder = 0
    object lbUser: TNWListBox
      Left = 2
      Top = 2
      Width = 235
      Height = 207
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ItemHeight = 15
      ParentFont = False
      Sorted = True
      Style = lbOwnerDrawFixed
      TabOrder = 0
      OnDblClick = acDeleteExecute
      OnDrawItem = lbUserDrawItem
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 211
    Width = 239
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bClose: TmmButton
      Left = 2
      Top = 3
      Width = 75
      Height = 25
      Action = acClose
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bDelete: TmmButton
      Left = 82
      Top = 3
      Width = 75
      Height = 25
      Action = acDelete
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 162
      Top = 3
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'ShiftCalendarDic'
    Left = 47
    Top = 36
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Lines'
        0))
  end
  object ActionList1: TmmActionList
    Left = 15
    Top = 35
    object acClose: TAction
      Caption = '(10)&Schliessen'
      Hint = '(*)Fenster schliessen'
      ShortCut = 27
      OnExecute = acCloseExecute
    end
    object acDelete: TAction
      Caption = '(9)&Loeschen'
      Hint = '(*)Profil loeschen'
      OnExecute = acDeleteExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
end
