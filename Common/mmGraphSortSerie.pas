(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphSortSerie.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: dialog interface for graph sort editor
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.00 | PW  | Datei erstellt
|=========================================================================================*)

unit mmGraphSortSerie;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 mmGraphSortSerieDlg,mmGraphGlobal;

type
 TmmGraphSortSeries = class(TComponent)
  private
   FSortOrder: eSortOrderT;
   FSortRow: byte;
  protected
  public
   Seriesnames: TStringList;
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function Execute: boolean;
   procedure AddName(aName: string);
  published
   property SortOrder: eSortOrderT read FSortOrder write FSortOrder;
   property SortRow: byte read FSortRow write FSortRow;
 end; //TmmGraphSortSeries

//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

constructor TmmGraphSortSeries.Create(AOwner: TComponent); 
begin
 inherited Create(AOwner);
 SeriesNames := TStringList.Create;
end; //constructor TmmGraphSortSeries.Create
//-----------------------------------------------------------------------------
destructor TmmGraphSortSeries.Destroy;
begin
 SeriesNames.Free;
 inherited Destroy;
end; //destructor TmmGraphSortSeries.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeries.AddName(aName: string);
begin
 Seriesnames.Add(aName);
end; //procedure TmmGraphSortSeries.AddName
//-----------------------------------------------------------------------------
function TmmGraphSortSeries.Execute: boolean;
begin
  if Owner is TWinControl then
    mmGraphSortSeriesDlg := TmmGraphSortSeriesDlg.Create(Owner)
  else
    mmGraphSortSeriesDlg := TmmGraphSortSeriesDlg.Create(Application);
  try
    mmGraphSortSeriesDlg.cobSortBy.Items.AddStrings(Seriesnames);
    mmGraphSortSeriesDlg.SortOrder := SortOrder;
    mmGraphSortSeriesDlg.SortRow := SortRow;

    result := mmGraphSortSeriesDlg.ShowModal = IDOk;
    if result then begin
      SortOrder := mmGraphSortSeriesDlg.SortOrder;
      SortRow := mmGraphSortSeriesDlg.SortRow;
    end; //if result then
  finally
    mmGraphSortSeriesDlg.Free;
  end; //try..finally
end; //function TmmGraphSortSeries.Execute
//-----------------------------------------------------------------------------
{
procedure Register;
begin
 RegisterComponents('Worbis',[TmmGraphSortSeries]);
end; //procedure Register
}

end. //mmGraphSortSerie.pas
