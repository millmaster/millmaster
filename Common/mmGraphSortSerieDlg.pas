(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphSortSerieDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: graph sort editor
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.00 | PW  | Datei erstellt
|=========================================================================================*)

unit mmGraphSortSerieDlg;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,CheckLst,ComCtrls,ToolWin,ImgList,ActnList,mmGraphGlobal,mmActionList,
 mmImageList,mmRadioGroup,mmListBox,mmGroupBox,mmPanel,mmToolBar,IvDictio,IvMulti,
 IvEMulti,mmTranslator,mmButton,BASEFORM;

type
 TmmGraphSortSeriesDlg = class(TmmForm)
  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  mmTranslator1: TmmTranslator;

  Panel1: TmmPanel;
   gbStatistik: TmmGroupBox;
   cobSortBy: TmmListBox;
   rgOrder: TmmRadioGroup;

  paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure acHelpExecute(Sender: TObject);
  procedure FormCreate(Sender: TObject);
  procedure cobSortByClick(Sender: TObject);
  procedure FormShow(Sender: TObject);
  private
   FSortOrder: eSortOrderT;
   FSortRow: byte;
   procedure SetSortOrder(Value: eSortOrderT);
   procedure SetSortRow(Value: byte);
   function GetSortOrder: eSortOrderT;
   function GetSortRow: byte;
  public
   property SortOrder: eSortOrderT read GetSortOrder write SetSortOrder;
   property SortRow: byte read GetSortRow write SetSortRow;
 end; //TmmGraphSortSeriesDlg

var
 mmGraphSortSeriesDlg: TmmGraphSortSeriesDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TmmGraphSortSeriesDlg.SetSortOrder(Value: eSortOrderT);
begin
 FSortOrder := Value;
 if Value = soAscending then rgOrder.ItemIndex := 0
                        else rgOrder.ItemIndex := 1;
end; //procedure TmmGraphSortSeriesDlg.SetSortOrder
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.SetSortRow(Value: byte);
begin
 FSortRow := Value;
 cobSortBy.ItemIndex := Value;
end; //procedure TmmGraphSortSeriesDlg.SetSortRow
//-----------------------------------------------------------------------------
function TmmGraphSortSeriesDlg.GetSortOrder: eSortOrderT;
begin
 if rgOrder.ItemIndex = 0 then result := soAscending
                          else result := soDescending;
end; //function TmmGraphSortSeriesDlg.GetSortOrder
//-----------------------------------------------------------------------------
function TmmGraphSortSeriesDlg.GetSortRow: byte;
begin
 result := cobSortBy.ItemIndex;
end; //function TmmGraphSortSeriesDlg.GetSortRow
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.acOKExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrOK;
end; //procedure TmmGraphSortSeriesDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.acCancelExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrCancel;
end; //procedure TmmGraphSortSeriesDlg.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.acHelpExecute(Sender: TObject);
begin
 //
end; //procedure TmmGraphSortSeriesDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.FormCreate(Sender: TObject);
begin
 cobSortBy.Items.Clear;
end; //procedure TmmGraphSortSeriesDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.cobSortByClick(Sender: TObject);
begin
 if cobSortBy.ItemIndex = 0 then
 begin
  SortOrder := soAscending;
  rgOrder.Enabled := false;
 end //if cobSortBy.ItemIndex = 0 then
 else rgOrder.Enabled := true;
end; //procedure TmmGraphSortSeriesDlg.cobSortByClick
//-----------------------------------------------------------------------------
procedure TmmGraphSortSeriesDlg.FormShow(Sender: TObject);
begin
 cobSortByClick(Sender);
end; //procedure TmmGraphSortSeriesDlg.FormShow

end. //mmGraphEditSerieDlg.pas
