(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseStoreXClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt die Basisklassen fuer die StoreX-Threads und den QueueManager.
|                 Da der QueueManager die StoreX's verwalten muss, sind beide
|                 Basisklassen in dieser Unit implementiert. TBaseStoreX ruft eine
|                 Methode von TBaseQueueManager auf, um sich aus der Liste zu
|                 entfernen.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | File created
| 15.09.1998  0.00  Wss | Standardumgebung fuer Entwicklung erstellt
| 09.10.1998  1.00  Mg  | TCleanUpStoreX eingefuegt
| 17.11.1998  1.01  Wss | Auf TmmXXX Klassen umgestellt
| 13.01.1999  1.02  Wss | Base of SubSystem thread changed to TBaseSubThread
|                         Base of TBaseStoreX changed to TBaseThread. Some implementation
|                          of TBaseStoreX removed because they are implemented in TBaseThread.
| 23.03.1999  1.03  Nue | Const. Types and classes from JobList implemented
| 30.03.1999  1.04  Nue | Changes at TBaseJobController and TBaseJobManager
| 01.04.1999  1.05  Mg  | TBaseThreadLauncher.ProcessJob : No error msg. if JobID not found in DataPool
| 07.04.1999  1.06  Nue | TDispContState inserted.
| 15.04.1999  1.07  Nue | TCleanUpEvJob inserted; Several changes in TBaseJobController
|                       | and TBaseJobManager
| 21.04.1999  1.08  Mg  | WriteJobFailedToJobController in TBaseThreadLauncher inserted
| 27.04.1999  1.09  Nue | jtDelJobID inserted in TBaseJobController.ProcessJob/ProcessInitJob
| 24.06.1999  1.10  Mg  | All StoreX uses the IPC-Chanel of ThreadLauncher for sending job to JobController
| 06.07.1999  1.11  Mg  | mWorkPriority in TBaseStoreX inserted
| 22.07.1999  1.20  Mg  | JobQueue inserted
| 30.08.1999  1.30  Mg  | TBaseDataPoolLineEditor inserted
| 13.01.2000  1.31  Nue | fDatabase in TBaseJob inserted (and dependent coding)
| 25.02.2000  1.32  Mg  | TBaseWorker.ProcessJob : Debug Messages and Log Messages inserted
| 28.02.2000  1.33  Mg  | TBaseWorker.ProcessJob : Debug Mode of JobEditors initialized
| 25.05.2000  1.34  Nue | if xBaseJob.fFirstReceiver=ttMsgController inserted in SendDelAndSuccMsg
| 06.06.2000  1.35  Nue | GetMaAssign renamed to MaEqualize. Old GetMaAssign only for getting Ma assigns used.
| 20.06.2000  1.36  Nue | try except coded for all calls within StartNext and in DelJobID.
| 20.06.2000  1.37  Nue | FreeMem for last job in joblist added.(Because of memory leack.)
| 09.01.2001  1.38  khp | TBaseJobEditor.Init: DebugMsg Transaction CleanUp and Query CleanUp
| 28.05.2001  1.39  Nue | System.Move in TBaseJob.Create replaced.
                    Wss   - jtCleanUpEv and jtDelJobId redirected to DataPoolWriter to be sequential
                          - TDelJobIDEditor and TCleanUpEditor removed, functionality moved to
                            TDataPoolWriterClass to have serialisation
| 30.05.2001  1.07  Nue | TCleanUpEvJob.DispatchContinuousJob added.
| 13.10.2002        LOK | Umbau ADO
| 05.12.2002  1.08  Nue | WriteLogAddMachID added.
| 14.03.2003  1.08  Wss | In WriteJobToNet Anpassungen, dass die verf�gbaren NetHandlers nun
                          �ber TSettingsReader (Params) abgefragt werden und nicht mehr selber
                          �ber mConnectedHandles
| 09.09.2003  1.08  Wss | WriteJobToNet: wenn z.b. GetNodelistJob an einen falschen NetHanlder
                          geschickt wird, welcher jedoch nicht installiert ist, wird ein
                          MsgNotComplete mit Fehlercode ERROR_PATH_NOT_FOUND an Jobcontroller
                          geschickt.
| 20.11.2003  1.09  Nue | Delete job in TBaseJob.ErrorHandling when repeating, if machine is Undefined or offline
| 22.04.2004  1.10  Nue | WriteLogDebug eingef�gt.
| 25.05.2004  1.10  Wss | AssignJob in TBaseJob eingef�gt.
| 12.05.2005  1.20  Nue | Verschiedene Anpassungen f�r Version V5.00.
| 17.05.2005  1.21  Nue | TJobList.IsDataAquisationJobActive added.
| 26.07.2005  1.02  Wss | ntLX Unterst�tzung
| 25.11.2005        Lok | Hints und Warnings entfernt
|=========================================================================================*)

unit MMBaseClasses;

interface

uses
  mmList, mmThread, mmRegistry, Classes, SysUtils, Windows,
  LoepfeGlobal, BaseGlobal, BaseThread, DataPoolClass, MMEventLog, MMAlert,
  AdoDBAccess, IPCClass, Mailslot, MsgListClass, JobQueueClass, syncobjs;

const
  cJobListItemHeaderSize = 16; // Change if TJobListItem changes
//  cNrOfTrials = 2; //Number of trials if a job failed
  cNrOfTrials = 1; //Number of trials if a job failed ab 26.4.05
  cNoMachineID = -1; //Kein maschinen abh�ngiger Job 26.4.05

type
  TDispContState = (dcFromBaseJob, dcTrue, dcFalse);

  //...........................................................................
  TBaseDataPoolWriter = class(TBaseSubThread)
  private
  protected
    mDataPool: TDataPool;
    procedure ProcessJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aDataPool: TDataPool); reintroduce;
    function Init: Boolean; override;
  end;

  //...........................................................................
  //.....JobList...............................................................

  TBaseJob = class; //Forward declaration
  TDependencyLevel = (dlNextStep, dlSame, dlNone);
                           //!!!!!!On changes change also cJobStateCount
  TJobState = (jsValid, //Job is only in JobList and waits for to run
    jsRunning, //Job is sarted and on the move in the system
    jsInvalid, //Job is only in JobList and waits to become valid by another Job
    jsAll,     //Used if all Jobs should be counted
    jsMaOffline); //Job is irrelevant because Machine is OFFLINE Nue:28.4.05
    
  PJobListItem = ^TJobListItem;
  TJobListItem = record
    JobID: DWord;
    TimeStamp: DWord; // TickCount, when a Job is inserted in JobList
    JobNext: DWord;
    JobInUse: Boolean; // 1 Byte
    JobState: TJobState; // 1 Byte
    //Bei MachineID soll nur ein Wert<>-1 drin wenn der Job davon abh�ngig ist, dass die Maschine ONLINE ist
    MachineID: Integer; // -1=No machine dependent Job  Nue:25.4.05
    Job: TBaseJob; //Pointer to JobClass
  end;

  PJobItemList = ^TJobItemList;
  TJobItemList = array of TJobListItem;

  TJobList = class(TProtectedList)
  private
    mMaxJobID: DWord;
    mMinJobID: DWord;
    fActJobID: DWord;
    mTestStr: string;
    function GetNextJobID: DWord; //@
    function GetDependentJob(aJobID: DWord): Boolean;
    function GetJobItem(aIndex: Integer): PJobListItem;
  //....................................................
    procedure RemoveJobQueue(aJob: TBaseJob);
    procedure SetJobItem(aIndex: Integer; const Value: PJobListItem);

  protected
    property ActJobID: DWord read fActJobID; // ActualJobID = last used JobID
    property JobItem[aIndex: Integer]: PJobListItem read GetJobItem write SetJobItem;
  public
    function AddJob(var aJobListItem: TJobListItem): Boolean; //@
    function CheckIn(aJobID: DWord): Boolean; //@
    function CheckOut(aJobID: DWord): TBaseJob; //@
    function DelJobID(aJobID: DWord): Boolean; //@
    destructor Destroy; override; //@
    function GetFullJobItem(var aJob: PJobListItem; aJobID: DWord): Boolean; //@
    function GetJob(var aJob: TBaseJob; aJobID: DWord; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aToDelete: Boolean = False): Boolean; //@
    function GetNumJobs(aJobState: TJobState; var aNumJobs: Word): boolean; //@
    function GetSameMaEqualizeJob(var aJob: TBaseJob; aMachineID: Word; var aFoundJobs: Word): Boolean;
    function Init: boolean;
    function SetJobState(aJobID: DWord; aJobState: TJobState): Boolean; //@
    function UpdateJobListItem(aJobID: DWord; var aJobListItem: TJobListItem): Boolean;

    function StartNextJob(var aJobListItem: TJobListItem): Boolean;
    function StartJobAgain(aJobID: DWord): Boolean;
    procedure DelAllDependentJobs(aDepJobID: DWord); //Rekursiv: Delets all jobs in all interlaced levels
    function SetDependence(aSrcJobID, aDepJobID: DWord): Boolean; //Set JobNext in Item aSrcJobID to aDepJobID
    function SetJobGroupValid(aJobGroupID: DWord): Boolean; //@
    function ExpiredJob(var aJobID: DWord; var aJobTyp: TJobTyp): Boolean;
    function IsDataAquisationJobActive: Boolean;
    function JobExists(var aJob: TBaseJob; aJobID: DWord): Boolean;
//    procedure HandleMachineOfflineJobs;
    function GetMachineOfflineJob(var aJob: TBaseJob): Boolean;
    function HasSetJobStateMaOffline(aMachineID: Integer): Boolean;

  published
  end;

  //...........................................................................
  TBaseJob = class(TObject)
  private
    fFirstReceiver: TThreadTyp;
    fJobInUse: Boolean;
    fJobID: DWord;
    fJobNext: DWord; // Next Job to access in JobList
    fJobState: TJobState;
    fJobGroup: DWord;
    fTimeout: DWord; //Timeout time in milliseconds, for one step
    FTimeStamp: DWord;
    fTrial: Byte; //Number of trials in actual step
    fMaxTrials: Byte; //Max. number of trials in actual step (0 means: go on even it fails)
    fJobRec: PJobRec;
    fNextJobInQueue: TBaseJob; // Next Job to access in the same JobQueue
    fQuery: TNativeAdoQuery;
    fDatabase: TAdoDBAccessDB;
    FMachineID: Integer;
    FQueueType: TJobTyp;
    function WriteJobBufferTo(aThreadTyp: TThreadTyp; aJobRec: PJobRec): Boolean; overload;
  protected
    fDebugMode: Boolean; // Checked by CodeSite.SendMsg for output or no
    fError: TErrorRec;
    fComputerName: TString30; // Workstation name; has to to set to '' if from ZE
    fPort: TString30; // Pipe name; has to to set to '' if from ZE
    fRestartMMOnEmergency: Boolean;
    fConfirmedByMsgController: Boolean;
    mJobList: TJobList;
    mJobSize: DWord;
    function SetError(aError: DWord; aErrorTyp: TErrorTyp = etNTError; aErrorMsg: string = ''): TErrorRec;
    procedure WriteLog(aEvent: TEventType; aText: string; aBuf: PByte = nil; aCount: DWord = 0);
    procedure WriteLogDebug(aMsg: String; aDefaultText: String = '');
    procedure WriteLogAddMachID(aEvent: TEventType; aText: string); //Nue:05.12.02
    property Query: TNativeAdoQuery read fQuery write fQuery;
    property Database: TAdoDBAccessDB read fDatabase write fDatabase;
  public
    mClassname: String;
    mTestStr: string;
    constructor Create(aJobList: TJobList; aJobTyp: TJobTyp; aJobRec: PJobRec;
        aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
        aFirstReceiver: TThreadTyp = ttMsgController; aParentJob: TBaseJob = nil;
        aMachineID: Integer = -1; aRestartMMOnEmergency: Boolean = False;
        aComputerName: TString30 = ''; aPort: TString30 = ''); reintroduce;
    destructor Destroy; override;

    procedure AssignJob(aJobRec: PJobRec);
    property FirstReceiver: TThreadTyp read fFirstReceiver write fFirstReceiver;
    property MachineID: Integer read FMachineID write FMachineID;
    property JobRec: PJobRec read fJobRec{ write fJobRec};
    property JobInUse: Boolean read fJobInUse write fJobInUse;
    property JobNext: DWord read fJobNext write fJobNext;
    property JobState: TJobState read fJobState write fJobState;
    property NextJobInQueue: TBaseJob read fNextJobInQueue write fNextJobInQueue;
    property JobID: DWord read fJobID write fJobID;
    property JobGroup: DWord read fJobGroup write fJobGroup;
    property QueueType: TJobTyp read FQueueType write FQueueType;
//    property Timeout: Word read fTimeout write fTimeout;
    property Timeout: DWord read fTimeout write fTimeout;
//  property TimeStamp: TDateTime read fTimeStamp write fTimeStamp;
    property TimeStamp: DWord read FTimeStamp write FTimeStamp;
    property Trial: Byte read fTrial write fTrial;
    function AddSelfToJobList(aJobState: TJobState = jsValid; aJobNext: DWord = 0): DWord; virtual;
    function AnalyseJobData: Boolean; virtual; //wss abstract; um abstract warning z.B. bei TSendMsgToApplJob zu vermeiden
    function DelDependentJobInQueue: Boolean; //Rekursiv
    function DelAllDependentJobsInList: Boolean;
    function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; virtual;
    function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; virtual;
    function ErrorHandling(aJobError: TJobTyp): Boolean; virtual;
    function ResetProdGrpState(aProdID: Longint): Boolean;
    function WriteBufferTo(aThreadTyp: TThreadTyp; aJobTyp: TJobTyp): Boolean; //Use for temp. messages(only JobTyp and JobID filled)
    function WriteJobBufferTo(aThreadTyp: TThreadTyp): Boolean; overload; //Use for ordinary JobBuffer
  end;

  //...........................................................................
//Nue 30.05.01  TCleanUpEvJob = class(TBaseJob);
  TCleanUpEvJob = class(TBaseJob)
  public
    function AnalyseJobData: Boolean; override;
    function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
  end;

  //...........................................................................
  TBaseJobController = class(TBaseSubThread)
  private
    function DeleteAndStartNew(var aJob: TBaseJob; aJobListItem: TJobListItem): Boolean;
    function DeleteUnknownJob(var aBaseJob: TBaseJob; aJobID: DWord): Boolean;
  protected
    mDB: TAdoDBAccess; // handles to the database
    mTestStr: string;
    mJobListItem: TJobListItem;
    mJobList: TJobList;
    procedure ProcessInitJob; override;
    procedure ProcessJob; override;
    function StartNext(aNew: Boolean): Boolean;
  public
    constructor Create(aThreadDef: TThreadDef; aJobList: TJobList); reintroduce; virtual;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;

  //...........................................................................
  TBaseJobManager = class(TBaseSubThread)
  private
    mTestStr: string;
    fFirstTick: Boolean;
  protected
    mDB: TAdoDBAccess; // handles to the database
    mJobListItem: TJobListItem;
    mJobList: TJobList;
    property FirstTick: Boolean read fFirstTick write fFirstTick;
    function Emergency(aJobTyp: TJobTyp; aJobID: DWord): Boolean;
    procedure JobToJobList(aBaseJob: TBaseJob);
    procedure ProcessInitJob; override;
    procedure ProcessJob; override;
    function Startup: Boolean; virtual; abstract;
  public
    constructor Create(aThreadDef: TThreadDef; aJobList: TJobList); reintroduce; virtual;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;

  //...........................................................................
  //...........................................................................
  TBaseMsgController = class(TBaseSubThread)
  private
    function GetNetTypOf(aThread: TThreadTyp): TNetTyp;
    function GetThreadTypOf(aNet: TNetTyp): TThreadTyp;
  protected
    mMsgList: TMsgList;
    function AddJobToMsgList(aFrom: DWord = 0; aTo: DWord = 0): Boolean;
    function MsgReceived: Boolean;
    procedure ProcessJob; override;
//    function RepeatCompleteMessage(aIndex: Integer): Boolean; virtual;
    function RepeatMessage(aIndex: Integer; aSingleMessage: Boolean = True): Boolean; virtual;
    function SendConfirmMsg(aComplete: Boolean; aJobID: Word; aError: TErrorRec): Boolean;
    procedure WriteJobToNet;
  public
    constructor Create(aThreadDef: TThreadDef; aMsgList: TMsgList); reintroduce;
    destructor Destroy; override;
    function DoConnect: Boolean; override;
    function Init: Boolean; override;
  end;

  //...........................................................................
  TBaseMsgDispatcher = class(TBaseSubThread)
  private
    mMaxJobID: DWord;
    mNextJobID: DWord;
  protected
    mMsgList: TMsgList;
    procedure ConfirmMsgReceipt(aValue: DWord = 0); virtual;
    function GetNextJobID: DWord;
    procedure ProcessJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aMsgList: TMsgList); reintroduce;
    function Init: Boolean; override;
  end;

  //...........................................................................
  TBaseQueueManager = class(TBaseSubThread)
  private
  protected
    mJobQueue: TJobQueue;
    procedure ProcessJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aJobQueue: TJobQueue); reintroduce; virtual;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;
  //...........................................................................
  TJobCtrlPort = class(TObject)
  private
    fError: DWord;
    mPort: TIPCClient;
    mCritSec: TCriticalSection;
  public
    constructor Create;
    destructor Destroy; override;
    function Connect: boolean;
    function WriteToJobController(aJob: PJobRec): boolean;
    property Error: Dword read fError;
  end;
  //...........................................................................
  TBaseJobEditor = class(TObject)
  private
    mEventLogDeb: TEventLogWriter;
    fDebugMode: boolean;
  protected
    mEventLog: TEventLogWriter;
    mJob: PJobRec;
    mJobSize: DWord;
    mDB: TAdoDBAccess;
    fError: TErrorRec;
    fPriority: TThreadPriority;
  //  function  BroadcastToMMClient ( aApplMsg : TApplMsg ) : boolean;
    procedure WriteLog(aEvent: TEventType; aText: string; aBuf: PByte = nil; aCount: DWord = 0);
    procedure WriteLogDebug(aText: string; aBuf: PByte = nil; aCount: DWord = 0);
    function IsError(aError: TErrorRec): boolean; // Check if aError.ErrorTyp is etNOError
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); virtual;
    destructor Destroy; override;
    function Init: boolean; virtual;
    function ProcessJob: TErrorRec; virtual;
    property Priority: TThreadPriority read fPriority;
    property DebugMode: boolean read fDebugMode write fDebugMode;
    property Error: TErrorRec read fError; // should be the same like Result of ProcessJob
  end;
  //...........................................................................
  TBaseWorker = class(TBaseSubThread)
  private
    mJobQueue: TJobQueue;
    mJobCtrlPort: TJobCtrlPort;
    function WriteConfirm: Boolean;
    function GetPriority: TThreadPriority;
    procedure SetPriority(aPriority: TThreadPriority);
    property Priority: TThreadPriority read GetPriority write SetPriority;
  protected
    mDataPool: TDataPool;
    mDB: TAdoDBAccess; // handles to the database
    mEditor: TBaseJobEditor;
    mJobError: TErrorRec; // Is used for message to JobController
                                 // it is absolutly necessary fill it in ProcessJob by error
    function getNextJob: Boolean; // returns if a new Job could be successfuly read from the JobQueue
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
    function getJobEditor: TBaseJobEditor; virtual; abstract;
  public
    constructor Create(aThreadDef: TThreadDef; aJobQueue: TJobQueue;
      aDataPool: TDataPool; aJobCtrlPort: TJobCtrlPort); reintroduce;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;
  //............................................................................
  TBaseDataPoolEditor = class(TBaseJobEditor)
  private
    mDataPool: TDataPool; // pointer to a TDataPool object
  protected
    function GetAllJobIDs(var aJobIDList: TJobIDList; var aNumJobs: Word): boolean;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); reintroduce;
    function ProcessJob: TErrorRec; override;
  end;
  //............................................................................
  TBaseDataPoolLineEditor = class(TBaseDataPoolEditor)
  private
    fDataList: TmmList; // pointer to a TmmList object
  protected
  public
    property DataList: TmmList read fDataList;
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); virtual;
    function ProcessJob: TErrorRec; override;
  end;
  //............................................................................
{
  TCleanUpEditor = class ( TBaseDataPoolEditor )
  private
    function ProcessJob : TErrorRec; override;
  public
    constructor Create ( aJob : TJobRec; aDB : TDBAccess; aEventLog : TEventLogWriter; aDataPool : TDataPool ); virtual;
  end;
  //............................................................................
  TDelJobIDEditor = class ( TBaseDataPoolEditor )
  private
    function ProcessJob : TErrorRec; override;
  public
    function Init():Boolean; override;
    constructor Create ( aJob : TJobRec; aDB : TDBAccess; aEventLog : TEventLogWriter; aDataPool : TDataPool ); reintroduce;
  end;
{}
  //............................................................................
  TTimeoutTicker = class(TBaseSubThread)
  private
    mSleepTime: DWord;
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
  public
    constructor Create(aThreadDef: TThreadDef); override;
    property SleepTime: DWord read mSleepTime write mSleepTime;
  end;

  //............................................................................

//var
//  ggFirstSettings: Boolean;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS,
  Registry,
  IPCUnit;

//******************************************************************************
//*                            TBaseDataPoolWriter                             *
//******************************************************************************
constructor TBaseDataPoolWriter.Create(aThreadDef: TThreadDef; aDataPool: TDataPool);
begin
  inherited Create(aThreadDef);

  mDataPool := aDataPool;
end;
//-----------------------------------------------------------------------------
function TBaseDataPoolWriter.Init: Boolean;
begin
  Result := inherited Init;
  if Result then begin
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseDataPoolWriter.ProcessJob;
begin
  // no common implementation for MM and OE yet
  inherited ProcessJob;
end;
//-----------------------------------------------------------------------------

//******************************************************************************
//*                            TJobList                                        *
//******************************************************************************
{ TJobList }

//-----------------------------------------------------------------------------

function TJobList.GetNextJobID: DWord;
begin
  CircInc(fActJobID, mMinJobID, mMaxJobID);
  Result := fActJobID;
end;
//-----------------------------------------------------------------------------

//Beim Aufruf gew�hren, dass die Liste gelockt ist!!
function TJobList.GetDependentJob(aJobID: DWord): Boolean;
var
  x: Word;
  xJob: PJobListItem;
begin
  Result := False;

  if Count > 0 then begin
    for x:=0 to Count-1 do begin
      xJob := Items[x];
{      if ((xJob^.JobNext = aJobID) and (xJob^.JobState = jsRunning)) or (xJob^.JobInUse) then begin
        Result := False;
//        break;
      end;
      if (xJob^.JobNext = aJobID) and (xJob^.JobState = jsValid) then begin
        aJobListItem := xJob^;
        Result := GetDependentJob(aJobListItem, xJob^.JobID);
        break;
      end;
}

      if (xJob^.JobNext = aJobID) then begin
        Result := True;
        break;
      end;
    end;
  end;
end;
//-----------------------------------------------------------------------------
function TJobList.GetJobItem(aIndex: Integer): PJobListItem;
begin
  Result := Items[aIndex];
end;
//-----------------------------------------------------------------------------
procedure TJobList.SetJobItem(aIndex: Integer; const Value: PJobListItem);
begin
  Items[aIndex] := Value;
end;

//-----------------------------------------------------------------------------

function TJobList.AddJob(var aJobListItem: TJobListItem): Boolean;
//Adds Job to JobList (Main).
//Allocation of memory for Job happens in the create constructor of the job
var
  xJob: PJobListItem;
begin
  Result := False;

  fListError := NO_ERROR;
  LockList;
  try
    xJob := Nil;
    try
      new(xJob);
      with xJob^ do begin
        JobID     := GetNextJobID;
        TimeStamp := GetTickCount;
        JobNext   := aJobListItem.JobNext;
        JobInUse  := False;
        JobState  := aJobListItem.JobState; //Valid or Invalid
        Job       := aJobListItem.Job;
      end;

      xJob^.Job.JobRec^.JobID := xJob^.JobID;
      aJobListItem            := xJob^;

      with xJob^ do
        mTestStr := Format('AddJob: %d JobTyp: %s JobNext: %d JobGroup: %d JobState: %d',
                           [JobID, Job.ClassName, JobNext, Job.JobGroup, ORD(JobState)]);
      OutputDebugString(PChar(mTestStr));
      // add new job to list
      Add(xJob);
      Result := True;
    except
      Dispose(xJob);
      fListError := ERROR_NOT_ENOUGH_MEMORY;
    end;
  finally
    UnlockList;
  end;
end;
//function TJobList.AddJob(var aJobListItem: TJobListItem): Boolean;
////Adds Job to JobList (Main).
////Allocation of memory for Job happens in the create constructor of the job
//var
//  xJob: PJobListItem;
//begin
//  Result := False;
//
//  fListError := NO_ERROR;
//  if GetMutex then begin
//    try
//      new(xJob);
//      try
//        with xJob^ do begin
//          JobID     := GetNextJobID;
//          TimeStamp := GetTickCount;
//          JobNext   := aJobListItem.JobNext;
//          JobInUse  := False;
//          JobState  := aJobListItem.JobState; //Valid or Invalid
//          Job       := aJobListItem.Job;
//        end;
//
//        xJob^.Job.JobRec^.JobID := xJob^.JobID;
//        aJobListItem            := xJob^;
//        Result                  := True;
//
//        with xJob^ do
//          mTestStr := Format('AddJob: %d JobTyp: %s JobNext: %d JobGroup: %d JobState: %d',
//                             [JobID, Job.ClassName, JobNext, Job.JobGroup, ORD(JobState)]);
//        OutputDebugString(PChar(mTestStr));
//      except
//        Dispose(xJob);
//        fListError := ERROR_NOT_ENOUGH_MEMORY;
//        Exit;
//      end;
//      // add new job to list
//      Add(xJob);
//    except
//      fListError := ERROR_NOT_ENOUGH_MEMORY;
//    end;
//    UnlockList;
//  end;
//end;
//-----------------------------------------------------------------------------

function TJobList.CheckIn(aJobID: DWord): Boolean;
//Used to reset the locking (JobInUse) for a job
var
  xJobListItem: PJobListItem;
begin
  Result := False;
  LockList;
  try
    GetFullJobItem(xJobListItem, aJobID);
    if xJobListItem^.JobInUse then begin
      xJobListItem^.JobInUse     := False;
      xJobListItem^.Job.JobInUse := False;
      Result := True;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------

function TJobList.CheckOut(aJobID: DWord): TBaseJob;
//Used to set the locking (JobInUse) for a job
var
  xJobListItem: PJobListItem;
begin
  Result := nil;
  LockList;
  try
    GetFullJobItem(xJobListItem, aJobID);
    if not xJobListItem^.JobInUse then begin
      xJobListItem^.JobInUse     := True;
      xJobListItem^.Job.JobInUse := True;
      Result := xJobListItem.Job;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------

function TJobList.DelJobID(aJobID: DWord): Boolean;
//Deletes a job from the JobList(Main)
var
  x: Integer;
  xJobListItem: PJobListItem;
begin
  Result := False;
  LockList;
  try
    if Count > 0 then
    try
      for x := 0 to Count - 1 do begin
        xJobListItem := JobItem[x];
        if not Assigned(xJobListItem) then
          WriteToEventLog('xJobListItem in DelJobID not Assigned!!', 'JobHandler: ', etError)
        else if not Assigned(xJobListItem^.Job) then
          WriteToEventLog('xJobListItem.Job in DelJobID not Assigned!!', 'JobHandler: ', etError)
        else if xJobListItem^.JobID = aJobID then begin
          // JobID found, anything alright
          Result := True;
          mTestStr := Format('DelJob: %d JobNext: %d JobState: %d Time: %s',
                             [xJobListItem^.JobID, xJobListItem^.JobNext, ORD(xJobListItem^.JobState), DateTimeToStr(Now)]);
          OutputDebugString(PChar(mTestStr));
        // remove associated memory of Job
//          FreeMem(xJobListItem.JobRec, xJobListItem.JobRec.JobLen);
          // remove entry from list
          // wss/lok: Hier m�sste doch auch Rekursiv die Unterjobs aufger�umt werden!?!?
//          if Assigned(xJobListItem^.Job) then
//            RemoveJobQueue(xJobListItem^.Job);

          Dispose(xJobListItem);
          Delete(x);
          Break;
        end;
      end; // for x
    except
      on E: EXCEPTION do begin
        WriteToEventLog('Error in DelJobID !! ' + e.Message, 'JobHandler: ', etError)
      end;
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
function TJobList.ExpiredJob(var aJobID: DWord; var aJobTyp: TJobTyp): Boolean;
var
  x: Integer;
  xJobListItem: PJobListItem;
begin
  Result := False;
  LockList;
  try 
    for x:=0 to Count-1 do begin
      xJobListItem := JobItem[x];
      if xJobListItem^.Job.Timeout > 0 then begin
        if (GetTickCount-xJobListItem^.Job.TimeStamp)>(xJobListItem^.Job.Timeout) then begin
          aJobID    := xJobListItem^.JobID;
          aJobTyp   := xJobListItem^.Job.JobRec^.JobTyp;
          Result := True;
          Break;
        end;
      end;
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------

destructor TJobList.Destroy;
var
  xJob: PJobListItem;
begin
  // clear all Jobs in JobList
  LockList;
  try
    while Count > 0 do begin
      xJob := JobItem[0];
      // remove associated memory of Job
      if Assigned(xJob^.Job) then
        RemoveJobQueue(xJob^.Job);
      Dispose(xJob);
      Delete(0);
    end;
  finally
    UnlockList;
    inherited Destroy;
  end;
end;
//-----------------------------------------------------------------------------

function TJobList.GetFullJobItem(var aJob: PJobListItem; aJobID: DWord): Boolean;
var
  xJobListItem: PJobListItem;
  x: Integer;
begin
  Result := False;
  aJob := nil;
  fListError := ERROR_INVALID_PARAMETER;
  // count through JobList to get the TList object of aJobID
  if Count > 0 then begin
    for x := 0 to Count - 1 do begin
      xJobListItem := JobItem[x];
      // if TList with aJobID found ...
      Result := (xJobListItem^.JobID = aJobID);
      if Result then begin
        aJob := xJobListItem;
        Break;
      end;
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TJobList.GetJob(var aJob: TBaseJob; aJobID: DWord; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aToDelete: Boolean): Boolean;
var
  xJobListItem: PJobListItem;
  x: Integer;
begin
  try
    Result := False;
    aJob := nil;
    fListError := ERROR_INVALID_PARAMETER;
  // count through JobList to get the TList object of aJobID
    if Count > 0 then begin
      for x := 0 to Count - 1 do begin
        xJobListItem := JobItem[x];
      // if TList with aJobID found ...
        Result := (xJobListItem^.JobID = aJobID);
        if Result then begin
//Nue 29.6.99
          if (xJobListItem^.JobInUse) and (not aToDelete) then begin
            CodeSite.SendFmtMsg('GetJob: IN USE  %d', [aJobID]);
            Result := False;
            Break;
          end;
//End Nue 29.6.99
          aJob := xJobListItem^.Job;
          aJob.Database := aDatabase;
          aJob.Query := aQuery;
          Break;
        end;
      end;
    end;
  except
    on e: Exception do begin
      aJob.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TJobList.GetJob ' + e.message);
      raise EMMException.Create('TJobList.GetJob ' + e.message);
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TJobList.GetNumJobs(aJobState: TJobState; var aNumJobs: Word): boolean;
var
  x: integer;
  xJobListItem: PJobListItem;
begin
  aNumJobs := 0;
  LockList;
  try
    for x := 0 to Count - 1 do begin
      xJobListItem := JobItem[x];
      if (xJobListItem^.JobState = aJobState) or (aJobState = jsAll) then
        INC(aNumJobs);
    end;
    Result := True;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------

function TJobList.GetSameMaEqualizeJob(var aJob: TBaseJob; aMachineID: Word; var aFoundJobs: Word): Boolean;
var
  x: integer;
  xJobListItem: PJobListItem;
  xJobID: DWord;
begin
  Result     := False;
  aFoundJobs := 0;
  fListError := ERROR_INVALID_PARAMETER;
  xJobID     := 0;
  LockList;
  try
    if Count > 0 then begin
      for x := 0 to Count - 1 do begin
        xJobListItem := JobItem[x];
        if (xJobListItem^.Job.JobRec.JobTyp = jtMaEqualize) and
           (xJobListItem^.Job.JobRec.MaEqualize.MachineID = aMachineID) then begin
          if xJobID = 0 then begin
            aJob   := xJobListItem^.Job;
            xJobID := xJobListItem^.Job.JobID;
          end
          else if aJob.JobState = jsValid then begin
            aJob   := xJobListItem^.Job;
            xJobID := xJobListItem^.Job.JobID;
          end;
          INC(aFoundJobs);
          Result := True; //Nue 2.5.2000
        end;
      end;
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
function TJobList.Init: boolean;
begin
  Result := True;
  mMinJobID := 1; // member the lowest valid automatic generated JobID
  mMaxJobID := (High(DWord) div 2); // member the highest valid automatic generated JobID
  fActJobID := mMinJobID; // 0 means no JobID -> cleared from NetHandler on auto data
end;

//-----------------------------------------------------------------------------
function TJobList.SetJobState(aJobID: DWord; aJobState: TJobState): Boolean; //@
var
  xJobListItem: PJobListItem;
  x: Integer;
begin
  Result := False;
  fListError := ERROR_INVALID_PARAMETER;
  // count through JobList to get the TList object of aJobID
  if Count > 0 then begin
    for x := 0 to Count - 1 do begin
      xJobListItem := JobItem[x];
      // if TList with aJobID found ...
      Result := (xJobListItem^.JobID = aJobID);
      if Result then begin
        xJobListItem^.JobState := aJobState;
        Break;
      end;
    end;
  end;
end;

//-----------------------------------------------------------------------------
function TJobList.UpdateJobListItem(aJobID: DWord; var aJobListItem: TJobListItem): Boolean;
var
  xJobListItem: PJobListItem;
  x: Integer;
begin
  Result     := False;
  fListError := ERROR_INVALID_PARAMETER;
  LockList;
  try
    if Count > 0 then
    try
      for x:=0 to Count-1 do begin
        xJobListItem := JobItem[x];
        if (xJobListItem^.JobID = aJobID) then begin
//            with xJobListItem do begin
//              JobNext := xBaseJob.JobNext;
//              JobInUse := False; // 1 Byte
//              JobState := jsRunning; // 1 Byte
//              Job := xBaseJob.NextJobInQueue; //Pointer to JobClass
//            end;
//            if (mJobList.UpdateJobListItem(xBaseJob.JobID, xJobListItem)) then begin
          with xJobListItem^ do begin
            JobID     := GetNextJobID;   //Neue JobID holen und dem neuen Job, der von der Queue in den obersten Level wechselt, zuweisen
            TimeStamp := GetTickCount;
            JobNext   := aJobListItem.JobNext;
            JobInUse  := False;

            JobState  := aJobListItem.JobState;
            Job       := aJobListItem.Job;
            //Fill property variables of job
            Job.JobID     := JobID;
            Job.TimeStamp := TimeStamp;
            Job.JobNext   := JobNext;
            Job.JobInUse  := JobInUse;
            Job.JobState  := JobState;
            //Fill JobRec
            Job.JobRec^.JobID := JobID;
          end; // with
          aJobListItem := xJobListItem^;
          Result       := True;
          break;
        end; // if
      end; // for x
    except
      on e: Exception do begin
        raise EMMException.Create('TJobList.UpdateJobListItem ' + e.message);
      end;
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

procedure TJobList.DelAllDependentJobs(aDepJobID: DWord);
begin

end;

//-----------------------------------------------------------------------------
function TJobList.IsDataAquisationJobActive: Boolean;
//Checkt ob noch irgendeine ProdGruppe in der DatenAquirierung steckt (Nicht Start oder Stopp!).
//Zur Aquirierung z�hlen die Jobs GenZeSpdData, DelZeSpdData und GenShiftData
//Wenn der GenShiftData Job mit jtJobSuccessful vom StorageHandler zur�ck kommt,
// wird der Status der ProdGroup auf der DB wieder von "Collecting" auf den alten Status gesetzt.
var
  x: Integer;
  xJobListItem: PJobListItem;
  xJobTyp: TJobTyp;
begin
  Result := False;
  LockList;
  try
    for x:=0 to Count-1 do begin
      xJobListItem := JobItem[x];
      xJobTyp := xJobListItem^.Job.JobRec^.JobTyp;
      with xJobListItem^.Job.JobRec^ do begin
        if ((xJobTyp=jtGetZESpdData) OR (xJobTyp=jtDelZESpdData) OR (xJobTyp=jtGenShiftData)) then begin
          if xJobTyp=jtDelZESpdData then begin
            if xJobListItem^.Job.QueueType=jtDataAquEv then begin
              xJobListItem^.Job.WriteLog(etWarning, Format('Dataaquisation for jobid:%d (Machine:%d Spindle:%d-%d) still running!',
                                       [JobID,DelZESpdData.MachineID,DelZESpdData.SpindleFirst,DelZESpdData.SpindleLast]));
              Result := True;
            end;
          end
          else if xJobTyp=jtGenShiftData then begin
            if xJobListItem^.Job.QueueType=jtDataAquEv then begin
              xJobListItem^.Job.WriteLog(etWarning, Format('Dataaquisation for jobid:%d (Machine:%d Spindle:%d-%d) still running!',
                                       [JobID,GenShiftData.MachineID,GenShiftData.SpindleFirst,GenShiftData.SpindleLast]));
              Result := True;
            end;
          end
          else begin
            xJobListItem^.Job.WriteLog(etWarning, Format('Dataaquisation for jobid:%d (Machine:%d Spindle:%d-%d) still running!',
                                     [JobID,GetZESpdData.MachineID,GetZESpdData.SpindleFirst,GetZESpdData.SpindleLast]));
            Result := True;
          end;
        end;

        if Result then 
          BREAK; //!!!!!!!!!!!!!!

      end; //with
    end; //for
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
//Nue:2.5.05
//Schaut ob die �bergebene JobID in der JobList existent ist
function TJobList.JobExists(var aJob: TBaseJob; aJobID: DWord): Boolean;
var
  xJobListItem: PJobListItem;
  x: Integer;
begin
  aJob := Nil;
  Result := False;
  LockList;
  try
    try
    // count through JobList to get the TList object of aJobID
      if Count > 0 then begin
        for x:=0 to Count-1 do begin
          xJobListItem := JobItem[x];
        // if TList with aJobID found ...
           if xJobListItem^.JobID = aJobID then begin
             aJob := xJobListItem^.Job;  //Gefundenen Job gerade dem Aufrufer zur�ckgeben.
             Result := True;
             BREAK; //!!!!!!!
           end; //if
        end;
      end;
    except
      on e: Exception do begin
        raise EMMException.Create('TJobList.JobExists ' + e.message);
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------

////L�schen nach erfolgreicher Installation Version 5.0
//procedure TJobList.HandleMachineOfflineJobs;
//var
//  x: Integer;
//  xJobListItem: PJobListItem;
//begin
//EnterMethod('TJobList.GetMachineOfflineJob');
//  LockList;
//  try
//    //Ganze JobListe durchgehen
//    for x := Count - 1 downto 0 do begin
//      xJobListItem := JobItem[x];
//      if (xJobListItem^.JobState=jsMaOffline) then begin
//        //Emergency aufrufen
//        // Falls sich noch ein SendMsgToAppl in Queue befindet, wird dieser in der Emergency Procedure neu aufgesetzt!
//        if Assigned(xJoblistItem^.Job) then begin
//          xJoblistItem^.Job.fTrial := xJoblistItem^.Job.fMaxTrials;  //Sofortiges Abruchkriterium f�r Job setzen
//          codesite.SendFmtMsg('Call Errorhandling for Job %d for JobTyp:%s', [xJoblistItem^.Job.JobID, GetJobName(xJoblistItem^.Job.fJobRec^.JobTyp)]);
//          //Errorhandling f�r Job starten
//          xJoblistItem^.Job.ErrorHandling(jtMsgNotComplete);
//        end; //if
//      end; //if
//    end; //for
//  finally
//    UnlockList;
//  end;
//end;

//-----------------------------------------------------------------------------

function TJobList.GetMachineOfflineJob(var aJob: TBaseJob): Boolean;
//Sucht JobList durch nach dem ersten jsMaOffline Job und gibt diesen Job zur�ck
var
  x: Integer;
  xJobListItem: PJobListItem;
begin
EnterMethod('TJobList.GetMachineOfflineJob');
  Result := False;
  LockList;
  try
    //Ganze JobListe durchgehen
    for x := Count - 1 downto 0 do begin
      xJobListItem := JobItem[x];
      if (xJobListItem^.JobState=jsMaOffline) then begin
        if Assigned(xJoblistItem^.Job) then begin
          aJob := xJoblistItem^.Job; //Job als R�ckgabeparameter setzen
          Result := True;
          BREAK; //!!!!!!!!
          codesite.SendFmtMsg('MaOffline found for Job %d for JobTyp:%s', [xJoblistItem^.Job.JobID, GetJobName(xJoblistItem^.Job.fJobRec^.JobTyp)]);
        end; //if
      end; //if
    end; //for
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
procedure TJobList.RemoveJobQueue(aJob: TBaseJob);
begin
  if Assigned(aJob.NextJobInQueue) then
    // Rekursiver Aufruf von RemoveJobQueue
    RemoveJobQueue(aJob.NextJobInQueue);
  aJob.Free;
end;

//-----------------------------------------------------------------------------
//26.4.05 Nue: Jobs, welche f�r diese Maschine noch in JobListe sind auf jsMaOffline setzen.
function TJobList.HasSetJobStateMaOffline(aMachineID: Integer): Boolean;
//Wird bei Maschine OFFLINE aus JobManager aufgerufen. Nue:26.4.05
var
  x: integer;
begin
EnterMethod('TJobList.SetJobStateMaOffline: MachID=' + IntToStr(aMachineID));
  Result := False;
  LockList;
  try
    //Ganze JobListe durchgehen
    for x:=0 to Count-1 do begin
      //Wenn Job �bergebene Maschine betrifft
      if JobItem[x]^.Job.MachineID = aMachineID then begin
        JobItem[x]^.JobState := jsMaOffline;
        Result := True;
        codesite.SendFmtMsg('!! MachineID:%d Job JobState set to jsMaOffline JobTyp:%s', [aMachineID,GetJobName(JobItem[x]^.Job.fJobRec.JobTyp)]);
      end;
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
function TJobList.SetDependence(aSrcJobID, aDepJobID: DWord): Boolean;
begin
  Result := True;
// ??Nue Codieren
end;

//-----------------------------------------------------------------------------
function TJobList.SetJobGroupValid(aJobGroupID: DWord): Boolean;
var
  x: integer;
  xJobListItem: PJobListItem;
begin
  Result := True;
  LockList;
  try
    for x := 0 to Count - 1 do begin
      xJobListItem := JobItem[x];
      if (xJobListItem^.Job.fJobGroup = aJobGroupID) then
        xJobListItem^.JobState := jsValid
    end;
  finally
    UnlockList;
  end;
end;

//-----------------------------------------------------------------------------
function TJobList.StartJobAgain(aJobID: DWord): Boolean;
begin
  Result := True;
// ??Nue Codieren
end;

//-----------------------------------------------------------------------------
function TJobList.StartNextJob(var aJobListItem: TJobListItem): Boolean;
var
  x: Word;
  xItem: PJobListItem;
begin
  Result     := False;
  fListError := ERROR_INVALID_PARAMETER;
  LockList;
  try
    try
      if Count > 0 then begin
        // count through JobList to get the used TList object
        for x := 0 to Count - 1 do begin
          xItem := JobItem[x];
          // Suche nach einem g�ltigen Job welcher noch nicht InUse ist
          if (xItem^.JobState = jsValid) and (not xItem^.JobInUse) then begin
            // Verweist ein anderer Job noch auf mich?
            if not GetDependentJob(xItem^.JobID) then begin
              // NEIN. Dieser Job darf bereits ausgef�hrt werden
              aJobListItem := xItem^;
              aJobListItem.JobState := jsRunning;
              Result       := True;
              xItem^.JobState := aJobListItem.JobState;
              xItem^.JobInUse := False;
              xItem^.Job.JobInUse := False;
              xItem^.Job.JobState := aJobListItem.JobState;
              break;
            end;
          end;
        end;
      end; // if Count

      if not Result then
        fListError := ERROR_NO_MORE_ITEMS;
    except
      on e: Exception do begin
        raise EMMException.Create('TJobList.StartNextJob ' + e.message);
      end;
    end;
  finally
    UnlockList;
  end;
end;

//function TJobList.StartNextJob(var aJobListItem: TJobListItem): Boolean;
//var
//  x: Word;
//  xJob: PJobListItem;
//begin
//  Result     := False;
//  fListError := ERROR_INVALID_PARAMETER;
//  LockList;
//  try
//    try
//      if Count > 0 then begin
//        // count through JobList to get the used TList object
//        for x := 0 to Count - 1 do begin
//          Result := False;
//          xJob := JobItem[x];
//          if (xJob^.JobState = jsValid) and (not xJob^.JobInUse) then begin
//            aJobListItem := xJob^;
//            Result       := True;
//            if not GetDependentJob(xJob^.JobID) then
//              break;
//          end;
//        end;
//      end; // if Count
//
//      if not Result then
//        fListError := ERROR_NO_MORE_ITEMS
//      else begin
//      // Set JobState to running
//        aJobListItem.JobState := jsRunning;
//        for x := 0 to Count - 1 do begin
//          xJob := JobItem[x];
//          if xJob^.JobID = aJobListItem.JobID then begin
//            xJob^.JobState := aJobListItem.JobState;
//            xJob^.JobInUse := False;
//            xJob^.Job.JobInUse := False;
//            xJob^.Job.JobState := aJobListItem.JobState;
//            break;
//          end;
//        end;
//      end; // if not Result
//    except
//      on e: Exception do begin
//        raise EMMException.Create('TJobList.StartNextJob ' + e.message);
//      end;
//    end;
//  finally
//    UnlockList;
//  end;
//end;
//
//******************************************************************************
//*                            TBaseJob                                        *
//******************************************************************************
{ TBaseJob }

function TBaseJob.AddSelfToJobList(aJobState: TJobState; aJobNext: DWord): DWord;
var
  xJobListItem: TJobListItem;
begin
  Result := 0;
  with xJobListItem do begin
    JobNext  := aJobNext;
    JobInUse := False;     // 1 Byte
    JobState := aJobState; // 1 Byte
    Job      := Self;      // Pointer to JobClass
  end;

  //Set class properties
  JobNext  := aJobNext;
  JobInUse := False; // 1 Byte
  JobState := aJobState; // 1 Byte

  if mJobList.AddJob(xJobListItem) then begin

    //Set dependent joblistitems to valid
    if aJobNext <> 0 then begin
      mJobList.SetJobState(aJobNext, jsValid);
      // DONE 1 -oNue -cBasis : Diese Linie ist wahrscheinlich ueberfluessig. -> Verursacht Absturz, wenn NIL (kein Nachfolgejob in Queue)
//     Self.fNextJobInQueue.JobState := jsValid
    end;
    fJobID := xJobListItem.JobID;
    Result := fJobID;
  end;
end;

//-----------------------------------------------------------------------------
{Alt: bis 17.9.2000 Nue
function TBaseJob.DelDependentJobInQueue: Boolean;  //Rekursiv
var
  xBaseJob: TBaseJob;
begin
  Result := True;
  // delete next job in Queue
  if Assigned(fNextJobInQueue) then begin
    fNextJobInQueue.DelDependentJobInQueue;
  end;
  Free;
end;
{}
//-----------------------------------------------------------------------------
function TBaseJob.AnalyseJobData: Boolean;
begin
  // leere Implementierung nur um compiler warning zu vermeiden (z.B. TSendMsgToApplJob)
  Result := False;
end;
//-----------------------------------------------------------------------------
function TBaseJob.DelDependentJobInQueue: Boolean; //Rekursiv
begin
  Result := True;
  // delete next job in Queue
  if Assigned(fNextJobInQueue) then begin
    fNextJobInQueue.DelDependentJobInQueue;
    FreeAndNil(fNextJobInQueue);
  end;
end;

//-----------------------------------------------------------------------------
function TBaseJob.DelAllDependentJobsInList: Boolean;
begin
  Result := True;
  // delete dependent jobs in JobList
//??Nue Has to be coded: wahrscheinlich als Methode im entsprechenden Job, da nicht bei jedem
// Job alle Nachfolger geloescht werden muessen!!
// Bis jetzt werden diese Jobs einfach durch den Ablauf des Timeouts aus der JobListe geoescht.
end;

//-----------------------------------------------------------------------------
function TBaseJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
begin
  Result := True;
  case aJobError of
    jtMsgNotComplete, jtJobFailed: begin
        // Last trial in JobController failed job and all dependent jobs deleted
        try
          Result := DelDependentJobInQueue; //Rekursiver Aufruf von FreeAndNil allfaelliger Folgeobjekte
          if aDelAllDependentJobsInList then
            Result := DelAllDependentJobsInList
        except
          on e: Exception do begin
            fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TBaseJob.EmergencyJM Delete job queue failed.' + e.message);
            raise EMMException.Create('TBaseJob.EmergencyJM Delete job queue failed.' + e.message);
          end;
        end;
      end;
  else
  end;
end;

//-----------------------------------------------------------------------------

function TBaseJob.ErrorHandling(aJobError: TJobTyp): Boolean;
const
  cxQrySelMachState =
    'SELECT c_machine_state FROM t_machine WHERE c_machine_id=:c_machine_id  ';

var
  xAlreadyDeleted: Boolean;

begin
  Result := False;
  case aJobError of
    jtMsgNotComplete: begin
// TODO wss: hier wird ein DelJobID an MC geschickt. Ist dann der zus�tzlich DelJobID bei MsgNotcomplete noch n�tig?
        //Cleanup with jtDelJobID in ttMsgController
        WriteBufferTo(ttMsgController, jtDelJobID);  // in TBaseJob, zum MsgController
        codesite.SendFmtMsg('!!!!After jtDelJobID: mJob.JobID: %d', [fJobID]);

        xAlreadyDeleted := False;
        if (fTrial < fMaxTrials) then begin
      //Start New part since 20.11.03 (Nue)
      // Delete job if machine is Undefined or offline
          if Self.fJobRec^.JobTyp in gMachIDJobTyp then
          try
            fQuery.Close;
            fQuery.SQL.Text := cxQrySelMachState;
            fQuery.Params.ParamByName('c_machine_id').AsInteger := Self.fJobRec^.MaEqualize.MachineID;
            fQuery.Open;
            if fQuery.FindFirst then begin
              if ((fQuery.FieldByName('c_machine_state').AsInteger = ORD(msUndefined)) or
//Nue 11.6.01                 (Query.FieldByName('c_machine_state').AsInteger=ORD(msNoDataCollection)) OR
                (Query.FieldByName('c_machine_state').AsInteger = ORD(msOffline))) then begin
                WriteBufferTo(ttJobManager, aJobError);
                xAlreadyDeleted := True;
                Result := True;
              end //if
            end //if
          except
            on e: Exception do begin
              fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBaseJob.ErrorHandling failed.' + e.message);
              raise EMMException.Create(fError.Msg);
            end;
          end;
      //End New part since 20.11.03 (Nue)

          if not xAlreadyDeleted then begin
            INC(fTrial);
            WriteJobBufferTo(ttMsgController);
            codesite.SendFmtMsg('!!!!After repeating Msg: mJob.JobID: %d', [fJobID]);
            WriteLogAddMachID(etWarning, Format(' MsgNotComplete: Additional trial for jobid: %d jobtyp: %s: ', [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
  //Start Test nue
            mTestStr := Format('MNC-Trial: %d JobTyp: %s JobNext: %d',
              [Self.JobID, mClassname, Self.JobNext]);
            OutputDebugString(PChar(mTestStr));
  //End Test Nue
          end //if NOT xAlreadyDeleted then begin
        end
        else if (fMaxTrials = 0) then begin
          WriteLogAddMachID(etWarning, Format(' MsgNotComplete for jobid: %d jobtyp: %s: ', [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
        end
        else begin
      //last trial failed job and all dependent jobs deleted -> inform JobManager
          Result := True;
          WriteBufferTo(ttJobManager, aJobError);
          WriteLogAddMachID(etError, Format(' MsgNotComplete: Last trial failed. jobid: %d jobtyp: %s: ',
            [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
        end;
      end;

    jtJobFailed: begin
//Start Test nue
        WriteLogAddMachID(etWarning, Format('JobFailed: Trial for jobid: %d jobtyp: %s Trials: %d : ', [fJobID, mClassname, fTrial]) + FormatMMErrorText(mJobList.ListError, etNTError));
//End Test Nue
        if (fTrial < fMaxTrials) then begin
          INC(fTrial);
          WriteJobBufferTo(ttQueueManager);
          WriteLogAddMachID(etWarning, Format('JobFailed: Additional trial for jobid: %d jobtyp: %s: ', [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
//Start Test nue
          mTestStr := Format('JF-Trial: %d JobTyp: %s JobNext: %d',
            [Self.JobID, mClassname, Self.JobNext]);
          OutputDebugString(PChar(mTestStr));
//End Test Nue
        end
        else if (fMaxTrials = 0) then begin
          WriteLogAddMachID(etWarning, Format('JobFailed for jobid: %d jobtyp: %s: ', [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
        end
        else begin
      //last trial failed job and all dependent jobs deleted -> inform JobManager
          Result := True;
          WriteBufferTo(ttJobManager, aJobError);
          WriteLogAddMachID(etError, Format('JobFailed: Last trial failed: jobid: %d jobtyp: %s: ',
            [fJobID, mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
        end;
      end;
  end;
end;

//-----------------------------------------------------------------------------
function TBaseJob.ResetProdGrpState(aProdID: Longint): Boolean;
const
  cxQrySetOnlyGrpStateBackP =
    'UPDATE t_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state ' +
    'WHERE c_prod_id= :c_prod_id';
begin
//  Result := False;
  try
    fQuery.Close;
    fQuery.SQL.Text := cxQrySetOnlyGrpStateBackP;
    fQuery.Params.ParamByName('c_prod_id').AsInteger := aProdID;
    fQuery.ExecSQL;
    Result := True;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBaseJob.ResetProdGrpState failed.' + e.message);
      raise EMMException.Create(fError.Msg);
    end;
  end;
end;

//-----------------------------------------------------------------------------

constructor TBaseJob.Create(aJobList: TJobList; aJobTyp: TJobTyp; aJobRec:
    PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase:
    TAdoDBAccessDB; aFirstReceiver: TThreadTyp = ttMsgController; aParentJob:
    TBaseJob = nil; aMachineID: Integer = -1; aRestartMMOnEmergency: Boolean =
    False; aComputerName: TString30 = ''; aPort: TString30 = '');
begin
  inherited Create;
  fDebugMode            := True; // Checked by CodeSite.SendMsg for output or no

  fQueueType := jtNone;  //Initialisierung mit None; fQueueType wird von den abgeleiteten Klassen, welche QueueType brauchen, �berschrieben

  fFirstReceiver        := aFirstReceiver;
  fComputerName         := aComputerName; // Workstation name; has to to set to '' if from ZE
  fPort                 := aPort; // Pipe name; has to to set to '' if from ZE
  fRestartMMOnEmergency := aRestartMMOnEmergency;
  FMachineID   := aMachineID; //Is a machine dependent job

  if aFirstReceiver <> ttMsgController then
    fConfirmedByMsgController := True
  else
    fConfirmedByMsgController := False;

  mJobList := aJobList;
  mJobSize := CloneJob(aJobRec, fJobRec);

  fJobInUse := False;
  fJobID    := 0;
  fJobNext  := 0;
  fJobState := jsValid;
  fJobGroup := 0;
  fTimeout  := 3600000; // Timeout time in milliseconds: This Timeout is only activ as
                        // long as the Job wasn't read by JobController. Then the Job
                        // associated Timeout will start (JobListTmo)!
  fTimeStamp := GetTickCount;
  fTrial     := 0;
  fMaxTrials := aMaxTrials;
  fNextJobInQueue := nil;

//TODO: XML: muss eventuell oben statt CloneJob manuell gr�sse ermittelt werden?
//  fJobRec^.JobLen := mJobSize; // mJobSize enth�lt gr�sse vom Buffer und nicht Jobl�nge
//  fJobRec^.JobID  := fJobID;
//  fJobRec^.NetTyp := aJobRec.NetTyp;

  fDatabase  := aDatabase;
  fQuery     := aQuery;
  mClassname := GetJobName(aJobTyp); // Self.ClassType;

  if Assigned(aParentJob) then
    aParentJob.fNextJobInQueue := Self;
end;
//-----------------------------------------------------------------------------

destructor TBaseJob.Destroy;
begin
//Alt Nue 15.5.2000: FreeMem(fJobRec, mJobSize);
//DONE: XML:  FreeMem(fJobRec, sizeof(TJobRec));
  FreeMem(fJobRec);
  inherited Destroy;
end;
//-----------------------------------------------------------------------------

function TBaseJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
//
begin
//  Result := dcFromBaseJob;
  case aJobTyp of
    jtMsgComplete: begin
        codesite.SendMsg('TBaseJob: MsgComplete (DispatchContinuousJob)');
        Trial := 0; //Set 0 for next part
//        Self.TimeStamp := Now; //Reset Timestamp for new Timeout
        Self.TimeStamp := GetTickCount; //Reset Timestamp for new Timeout
        Self.fConfirmedByMsgController := True;
        Result := dcTrue;
      end;
    jtJobFinished: begin //From jobs with an analyse job
        if Self.fConfirmedByMsgController then begin
          Result := dcFalse;
        end
        else begin
        //This steps are needed to synchronize MsgController. Without it happend, that the job
        // was deleted from JobController and MsgController didn't receive a confirm from
        // MsgDispatcher yet.
          Sleep(500);
          if not WriteBufferTo(ttJobController, jtJobFinished) then begin
          end;
          Result := dcTrue;
        end;
      end;
    jtJobSuccessful: begin
        Result := dcFalse;
      end;
  else
    Result := dcTrue;
  end;
end;
//-----------------------------------------------------------------------------

function TBaseJob.SetError(aError: DWord; aErrorTyp: TErrorTyp; aErrorMsg: string): TErrorRec;
begin
  fError.ErrorTyp := aErrorTyp;
  fError.Error := aError;
  fError.Msg := aErrorMsg;
  Result := fError;
end;
//-----------------------------------------------------------------------------

procedure TBaseJob.WriteLog(aEvent: TEventType; aText: string; aBuf: PByte; aCount: DWord);
begin
  fError.ErrorTyp := etNoError;
  fError.Error := NO_ERROR;

  with TEventLogWriter.Create(cEventLogClassForSubSystems, cEventLogServerNameForSubSystems,
    ssJobHandler, mClassname + ': ', True) do try
    if Assigned(aBuf) and (aCount > 0) then
      WriteBin(aEvent, aText, aBuf, aCount)
    else
      Write(aEvent, aText);
  finally
    Free;
  end;
end;
//-----------------------------------------------------------------------------

procedure TBaseJob.WriteLogDebug(aMsg: String; aDefaultText: String = '');
begin
  WriteToEventLogDebug(aMsg, aDefaultText, etInformation, ssJobHandler);
end;
//-----------------------------------------------------------------------------

procedure TBaseJob.WriteLogAddMachID(aEvent: TEventType; aText: string); //Nue:05.12.02
begin
  if Self.fJobRec^.JobTyp in gMachIDJobTyp then
    WriteLog(aEvent, Format(' MachineID: %d %s ', [Self.fJobRec^.MaEqualize.MachineID, aText]))
  else
    WriteLog(aEvent, aText);
end;
//-----------------------------------------------------------------------------

function TBaseJob.WriteBufferTo(aThreadTyp: TThreadTyp; aJobTyp: TJobTyp): Boolean;
//Used to write messages (with other JobTyp then the Job) within a job
var
  xJobRec: TJobRec;
begin
//ToDo: XML: was wird hier genau gemacht mit dem Jobbuffer?
  xJobRec.JobTyp := aJobTyp;
  xJobRec.JobID  := fJobID;
  xJobRec.NetTyp := fJobRec^.NetTyp;
  Result := WriteJobBufferTo(aThreadTyp, @xJobRec);
end;
//-----------------------------------------------------------------------------

function TBaseJob.WriteJobBufferTo(aThreadTyp: TThreadTyp): Boolean;
begin
  Result := WriteJobBufferTo(aThreadTyp, JobRec);
end;
//-----------------------------------------------------------------------------

function TBaseJob.WriteJobBufferTo(aThreadTyp: TThreadTyp; aJobRec: PJobRec): Boolean;
var
  xMsg: TMMGuardRec; // buffer of Msg information to MMGuard
begin
  with TIPCClient.Create('.', cChannelNames[aThreadTyp], cIPCClientWriteTimeout) do
  try
    aJobRec^.JobLen := GetJobDataSize(aJobRec);
    Result          := Write(PByte(aJobRec), aJobRec^.JobLen);
  finally
    Free;
  end;

  if not Result then begin
    WriteLog(etError, Format('Write to pipe %s failed twice by timeout(%d) in TBaseJob.WriteJobBufferTo ==>> RESTART MM. ',
                             [cChannelNames[aThreadTyp], cIPCClientWriteTimeout]));
    //temporaeres andocken an Main
    with TIPCClient.Create('.', cChannelNames[ttJobMain]) do
    try
      xMsg.MsgTyp         := gcRestartMM;
      xMsg.Error.ErrorTyp := etMMError;
      Result              := Write(@xMsg, sizeof(xMsg));
      if not Result then
        WriteLog(etError, Format('Write to Main %s failed: %s',
                                 [cChannelNames[ttJobMain], FormatMMErrorText(SetError(Error, xMsg.Error.ErrorTyp))]));
    finally
      Free;
    end;
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseJob.AssignJob(aJobRec: PJobRec);
begin
  if Assigned(aJobRec) then begin
    // Source Daten gr�sser als der eigene Buffer -> vergr�ssern
    if aJobRec^.JobLen > mJobSize then begin
      mJobSize := aJobRec^.JobLen;
      ReallocMem(fJobRec, mJobSize);
    end;
    CopyJob(aJobRec, fJobRec); // wss: CopyJob hier ok
  end;
end;

//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TCleanUpEvJob
//-----------------------------------------------------------------------------
function TCleanUpEvJob.AnalyseJobData: Boolean;
begin
  // Implementierung nur um Warning wegen abstract methods zu vermeiden
  Result := False;
end;
//-----------------------------------------------------------------------------
function TCleanUpEvJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  // Only jtMsgComplete possible
//  Result := inherited DispatchContinuousJob(aJobTyp);
  inherited DispatchContinuousJob(aJobTyp);
  Result := dcFalse;
end;

//******************************************************************************
//*                            TBaseJobController                              *
//******************************************************************************
constructor TBaseJobController.Create(aThreadDef: TThreadDef; aJobList: TJobList);
begin
  inherited Create(aThreadDef);
  mJobList := aJobList;
  mDB := TAdoDBAccess.Create(1, True); // First TQuery for use in TBaseJob
end;
//-----------------------------------------------------------------------------
destructor TBaseJobController.Destroy;
begin
  mDB.Free;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------

function TBaseJobController.DeleteAndStartNew(var aJob: TBaseJob; aJobListItem: TJobListItem): Boolean;
// Frees given aJob and returns new Job in aJob(on base of aJobListItem)
begin
  try
    Result := False;
    if Assigned(aJob) then begin
//Start Test nue
      mTestStr := Format('DelJobInQueue: %d JobTyp: %s JobNext: %d',
        [aJob.JobID, aJob.ClassName, aJob.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
    //old: aJob.Free;
      FreeAndNil(aJob);
    end;

    if mJobList.GetJob(aJob, aJobListItem.JobID, mDB.Query[cPrimaryQuery], mDB.DataBase) then begin
      if aJob.WriteJobBufferTo(aJob.FirstReceiver) then begin
//        aJob.TimeStamp := Now;
        aJob.TimeStamp := GetTickCount;
//        aJob.Timeout := cJobTimeout[aJob.JobRec^.JobTyp].JobListTmo div 1000;
        aJob.Timeout := cJobTimeout[aJob.JobRec^.JobTyp].JobListTmo;
//Start Test nue
        if aJob.FirstReceiver = ttMsgController then
          case aJob.JobRec^.JobTyp of
            jtMaEqualize:
              WriteLogDebug(Format('MsgToMC: %s: %d, %s, MachID:%d', [aJob.mClassname, aJob.JobID, GetJobName(aJob.JobRec^.JobTyp),
                aJob.JobRec^.MaEqualize.MachineID]));
            jtGetExpZESpdData, jtGetZESpdData:
              WriteLogDebug(Format('MsgToMC: %s: %d, %s, MachID:%d, First:%d, Last:%d', [aJob.mClassname, aJob.JobID,
                GetJobName(aJob.JobRec^.JobTyp), aJob.JobRec^.GetZESpdData.MachineID, aJob.JobRec^.GetZESpdData.SpindleFirst,
                  aJob.JobRec^.GetZESpdData.SpindleLast]));
          else
            WriteLogDebug(Format('MsgToMC: %s: %d, %s', [aJob.mClassname, aJob.JobID, GetJobName(aJob.JobRec^.JobTyp)]));
          end;
      end;
//End Test Nue
      Result := True;
//Start Test nue
      mTestStr := Format('NewJob: %d JobTyp: %s JobNext: %d ',
        [aJob.JobID, aJob.ClassName, aJob.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
    end
    else
      StartNext(True);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'DeleteAndStartNew ' + e.message);
      raise EMMException.Create('DeleteAndStartNew ' + e.message);
    end;
  end;
end;

  //...............................................
function TBaseJobController.DeleteUnknownJob(var aBaseJob: TBaseJob; aJobID: DWord): Boolean;
//Sendet dem MsgHandler ein jtDelJobId f�r einen unbekannten Job (DeleteUnknownJob)
begin
  if mJobList.JobExists(aBaseJob, mJob.JobID) then begin
    Result := False;
  end
  else begin //Neu ab 28.4.05
    Result := True;
    mJob.JobTyp := jtDelJobID;  // DeleteUnknownJob zum MsgController
    WriteJobBufferTo(ttMsgController);
  end;
end;

//-----------------------------------------------------------------------------

function TBaseJobController.StartNext(aNew: Boolean): Boolean;
var
  xBaseJob: TBaseJob;
  xJobListItem: TJobListItem;
  xDispContState: TDispContState;
  xFirstReceiver: TThreadTyp;

//Start local//
//...........................
  procedure SendDelAndSuccMsg(aFirstReceiver: TThreadTyp);
  begin
    try
      //Send jtDelJobID to ttMsgController
      if aFirstReceiver = ttMsgController then begin
        mJob.JobTyp := jtDelJobID;  // StartNext.SendDelAnsSuccMsg zum MsgController
        WriteJobBufferTo(ttMsgController);
      end;

      //F�r den Fall, dass der JobManager irgendwann doch noch mal etwas machen muss mit diesem Job
      // -> Achtung: dazu darf aber der Job noch nicht aus der Liste gel�scht werden!!
      mJob.JobTyp := jtJobSuccessful;
      WriteJobBufferTo(ttJobManager);
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'SendDelAndSuccMsg ' + e.message);
        raise EMMException.Create('SendDelAndSuccMsg ' + e.message);
      end;
    end;
  end;

  procedure DelJobSendDelAndSuccMsg(aFirstReceiver: TThreadTyp);
  begin
    try
      if (mJobList.DelJobID(mJob.JobID)) then begin
        SendDelAndSuccMsg(aFirstReceiver);
      end
      else begin
        WriteLog(etError, Format('TBaseJobController.StartNext: mJobList.DelJobID failed: jobid: %d jobtyp: %s: ',
          [mJob.JobID, xBaseJob.mClassname]) + FormatMMErrorText(mJobList.ListError, etNTError));
      end;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'DelJobSendDelAndSuccMsg ' + e.message);
        raise EMMException.Create('DelJobSendDelAndSuccMsg ' + e.message);
      end;
    end;
  end;
//...........................
//End local//

begin
  Result := True;
  try
    xBaseJob := nil;
    if aNew then begin
      // Sucht nach einem g�ltigen Job (jsValid) welcher noch nicht InUse ist und pr�ft auch nach abh�ngigen Jobs
      if mJobList.StartNextJob(xJobListItem) then begin
       //New Job from Main List
        DeleteAndStartNew(xBaseJob, xJobListItem);
      end;
    end
    else begin
      // bestehene JobQueue holen und weiter abarbeiten
      if mJobList.GetJob(xBaseJob, mJob.JobID, mDB.Query[cPrimaryQuery], mDB.DataBase) then begin

    //If not JobInUse then GetJob=True
        xBaseJob.Trial := 1;
        xFirstReceiver := xBaseJob.fFirstReceiver;
      //If no DispatchContinuousJob is implemented the job will be destroyed in TBaseJob.DispatchContinuousJob
        try
          xDispContState := xBaseJob.DispatchContinuousJob(mJob.JobTyp);
        except
          on e: Exception do begin
            fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'DispatchContinuousJob ' + e.message);
            raise EMMException.Create('DispatchContinuousJob ' + e.message);
          end;
        end;

        if (xDispContState = dcFalse) then begin
        //Job finished so far get next one
          if (xBaseJob.NextJobInQueue <> nil) {and (xBaseJob.JobInUse = False)Nue??} then begin
            with xJobListItem do begin                
              JobNext := xBaseJob.JobNext;
              JobInUse := False; // 1 Byte
              JobState := jsRunning; // 1 Byte
              Job := xBaseJob.NextJobInQueue; //Pointer to JobClass
            end;
            // wss: sendet DelJobID an MsgController und JobSuccess an JobManager
            SendDelAndSuccMsg(xFirstReceiver);
            if mJobList.UpdateJobListItem(xBaseJob.JobID, xJobListItem) then begin
              //Free memory from old job; get new job from JobQueue and send it to FirstReceiver
              DeleteAndStartNew(xBaseJob, xJobListItem);
            end
          end
          else begin
          //No further job in JobQueue get the next from JobList(Main)
            if (mJobList.StartNextJob(xJobListItem)) then begin
          //New JobListItem from Main List found
            //Free memory from old job; get new job from JobList and send it to FirstReceiver
              if DeleteAndStartNew(xBaseJob, xJobListItem) then begin
                DelJobSendDelAndSuccMsg(xFirstReceiver);
                StartNext(True); //Not really necessary?
              end;
            end
            else begin
          //No new JobListItem from Main List found
              if Assigned(xBaseJob) then //Nue: 7.8.00
              //Free memory from old job
                xBaseJob.Free; //Calls destroy   Nue: 7.8.00
              DelJobSendDelAndSuccMsg(xFirstReceiver);
              StartNext(True);
            end;
          end;
//        WriteLogDebug('JobController: jtMsgComplete arrived; No continuous job; Tried to get new job from joblist.');
        end
        else if xDispContState = dcFromBaseJob then begin
        //Return from TBaseJob.DispatchContinuousJob
          try
            xBaseJob.Free;
            DelJobSendDelAndSuccMsg(xFirstReceiver);
            StartNext(True);
          except
            on e: Exception do begin
              fError := SetError(ERROR_INVALID_FUNCTION, etDBError, '(xDispContState=dcFromBaseJob) ' + e.message);
              raise EMMException.Create(fError.Msg);
              EXIT;
            end;
          end;
        end
        else begin //dcTrue
        //Job is continuing
        end;
      end
      else begin //Repeat message because it's in use (GetJob)
        Sleep(100);
        WriteJobBufferTo(ttJobController); //Repeat GetJob, if it's in use!
      end;
    end;

  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError,
        Format('TBaseJobController.StartNext failed: JobTyp: %s' + e.message, [GetJobName(mJob.JobTyp)]));
      WriteLog(etError, fError.msg);
      Sleep(2000);
      StartNext(True);
    end;
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseJobController.ProcessInitJob;
var
  xBaseJob: TBaseJob;
begin
  // handle common job of both systems: OE and MM
  case mJob.JobTyp of
    jtJobFailed: begin
//Start test Nue: Gleicher Code wie jtMsgNotComplete
        if mJobList.GetJob(xBaseJob, mJob.JobID, mDB.Query[cPrimaryQuery], mDB.DataBase) then
          xBaseJob.ErrorHandling(mJob.JobTyp);
          //if last trial failed JobManager will be informed
      end;
    jtMsgNotComplete: begin
        if mJobList.GetJob(xBaseJob, mJob.JobID, mDB.Query[cPrimaryQuery], mDB.DataBase) then
          xBaseJob.ErrorHandling(mJob.JobTyp);
          //if last trial failed JobManager will be informed
      end;
    jtDelJobID, // TBaseJobController.ProcessInitJob // Used from EmergencyJM, to delete the failed Job
      jtJobFinished, // To delete Job from JobList, initiated by JobManager (AnalyseJobData)
      jtJobSuccessful,
      jtMsgComplete: begin
          codesite.SendFmtMsg('JC: %s (ProcessInitJob): JobID: %d', [GetJobName(mJob^.JobTyp), mJob^.JobID]);
          // Kommt eine JobID beim DelJobID wo nicht mehr in der Liste vorhanden ist, dann soll
          // nach neuen Jobs gesucht werden StartNext(True).
          // Ist die JobID doch noch vorhanden (z.B. wenn ein Job intern die L�schung beantragt hat), dann
          // wird mit dem Folgejob weitergemacht StartNext(False)
          // DeleteUnknownJob():
          // Wenn z.B. eine Best�tigung pos vom Storage/MsgHandler kommt, dieser Job jedoch durch ein MaOffline bereis
          // aus der Liste entfernt wurde, dann den DelJobID an MsgHandler ausl�sen
          StartNext(DeleteUnknownJob(xBaseJob, mJob.JobID));
      end;
    jtNewJobEv: begin
//      WriteLogDebug('JobController: jtNewJobEv arrived; Tried to get new jobs from joblist.');
        StartNext(True)
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessInitJob;
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseJobController.ProcessJob;
var
  xBaseJob: TBaseJob;
begin
  xBaseJob := Nil;
  // handle common job of both systems: OE and MM
  case mJob.JobTyp of
    jtJobFailed,
      jtMsgNotComplete: begin
        try
          if mJob.JobTyp = jtJobFailed then
            codesite.SendFmtMsg('!!!!jtJobFailed: mJob.JobID: %d', [mJob.JobID])
          else
            codesite.SendFmtMsg('!!!!jtMsgNotComplete: mJob.JobID: %d', [mJob.JobID]);

          if NOT DeleteUnknownJob(xBaseJob, mJob.JobID) then begin
            codesite.SendFmtMsg('!!!!Before ErrorHandling: mJob.JobID: %d', [mJob.JobID]);
            xBaseJob.ErrorHandling(mJob.JobTyp);
          //if last trial failed JobManager will be informed
            codesite.SendFmtMsg('!!!!After ErrorHandling: mJob.JobID: %d', [mJob.JobID]);
          end;
        except
          if Assigned(xBaseJob) then
            WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s Job %s', [GetJobName(mJob.JobTyp), xBaseJob.mClassname]))
          else
            WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
        end;
      end;
    jtDelJobID, // Used from EmergencyJM, to delete the failed Job
      jtJobFinished, // To delete Job from JobList, initiated by JobManager (AnalyseJobData)
      jtJobSuccessful,
      jtMsgComplete: begin
        try
          codesite.SendFmtMsg('JC: %s: JobID: %d', [GetJobName(mJob^.JobTyp), mJob^.JobID]);
          // Kommt eine JobID beim DelJobID wo nicht mehr in der Liste vorhanden ist, dann soll
          // nach neuen Jobs gesucht werden StartNext(True).
          // Ist die JobID doch noch vorhanden (z.B. wenn ein Job intern die L�schung beantragt hat), dann
          // wird mit dem Folgejob weitergemacht StartNext(False)
          // DeleteUnknownJob():
          // Wenn z.B. eine Best�tigung pos vom Storage/MsgHandler kommt, dieser Job jedoch durch ein MaOffline bereis
          // aus der Liste entfernt wurde, dann den DelJobID an MsgHandler ausl�sen
          StartNext(DeleteUnknownJob(xBaseJob, mJob.JobID));
        except
          if Assigned(xBaseJob) then
            WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s Job %s', [GetJobName(mJob.JobTyp), xBaseJob.mClassname]))
          else
            WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
        end;
      end;
    jtNewJobEv: begin
//      WriteLogDebug('JobController: jtNewJobEv arrived; Tried to get new jobs from joblist.');
        try
          StartNext(True);
        except
          WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
        end;
      end;
  else
    try // call inherited ProcessJob for unknown or common JobTyp
      inherited ProcessJob;
    except
      WriteLog(etError, Format('Crash in TBaseJobController.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
    end;
  end;
end;

//-----------------------------------------------------------------------------
function TBaseJobController.Init: Boolean;
begin
  Result := inherited Init;

  if Result then begin
    Result := mDB.Init;
  end;
end;

//-----------------------------------------------------------------------------
//******************************************************************************
//*                            TBaseJobManager                                 *
//******************************************************************************
constructor TBaseJobManager.Create(aThreadDef: TThreadDef; aJobList: TJobList);
begin
  inherited Create(aThreadDef);
  mJobList := aJobList;
  mDB := TAdoDBAccess.Create(3, True); // 2 Query-handles; Second TQuery only for use in TBaseJob
end;
//-----------------------------------------------------------------------------
destructor TBaseJobManager.Destroy;
begin
  mDB.Free;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------

function TBaseJobManager.Emergency(aJobTyp: TJobTyp; aJobID: DWord): Boolean;
var
  xBaseJob: TBaseJob;
  xJobTypStr: string;
  xTime: DWord;
begin
  Result := False;
  xTime := GetTickCount;
  sleep(1);
  if mJobList.GetJob(xBaseJob, aJobID, mDB.Query[cSecondaryQuery], mDB.DataBase, True) then begin
    xJobTypStr := GetJobName(xBaseJob.JobRec^.JobTyp);
    try
      if xBaseJob.EmergencyJM(aJobTyp, True, True) then begin
        FreeAndNil(xBaseJob); //Nach sauberem Aufraeumen wird der Speicher des Jobs freigegeben
//+++++++++++++++ Nach dieser Linie kein Zugriff mehr auf xBaseJob ++++++++++++++++++++++
        if mJobList.DelJobID(aJobID) then begin
          Result := True;
          WriteLog(etError, Format('Emergency: Last trial %s failed. Whole job queue deleted: jobid: %d jobtyp: %s %d',
            [GetJobName(aJobTyp), aJobID, xJobTypStr, xTime]) + FormatMMErrorText(mJobList.ListError, etNTError));
        end;
      end;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etMMError, Format('TBaseJobManager.Emergency %d' + e.message, [xTime]));
        WriteLog(etError, fError.msg);
        Result := False;
      end;
    end;
  end
  else begin
    WriteLog(etWarning, FormatMMErrorText(SetError(ERROR_NO_DATA, etMMError, Format('Emergency from %s: JobID %d not found in joblist ',
      [GetJobName(aJobTyp), aJobID]))));
  end;
  if not Result then begin
    fError := SetError(ERROR_CAN_NOT_COMPLETE, etMMError, Format('Emergency from %s: Could not delete job: jobid: %d %d',
      [GetJobName(aJobTyp), aJobID, xTime]));
// Alternative der Fehlerbehandlung mit raise
//    raise EMMException.Create(Format('EmergencyJM: Could not delete job: jobid: %d jobtyp: %s: ',
//           [mJob.JobID,xBaseClass.mClassname])+FormatMMErrorText(mJobList.ListError,etNTError));
  end;
end;
//-----------------------------------------------------------------------------

procedure TBaseJobManager.JobToJobList(aBaseJob: TBaseJob);
begin
  aBaseJob.AddSelfToJobList;
  mJob.JobID  := aBaseJob.JobID;
  mJob.JobTyp := jtNewJobEv;
  if not WriteJobBufferTo(ttJobController) then begin
  end;
end;
//-----------------------------------------------------------------------------

procedure TBaseJobManager.ProcessInitJob;
begin
  // handle common job of both systems: OE and MM
  case mJob.JobTyp of
    jtJobSuccessful,
      jtJobFailed,
      jtMsgNotComplete,
      jtTimerEvent: begin
        ProcessJob;
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessInitJob;
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseJobManager.ProcessJob;
var
  xJobID: DWord;
  xJobTyp: TJobTyp;
  xBaseJob: TBaseJob;
begin
  // handle common job of both systems: OE and MM
  case mJob.JobTyp of
    jtCleanUpEv: begin
        try
          xBaseJob := TCleanUpEvJob.Create(mJobList, jtCleanUpEv, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttMsgController);
          JobToJobList(xBaseJob);
        except
          WriteLog(etError, Format('Crash in TBaseJobManager.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
        end;
      end;

    jtJobSuccessful: begin
        try
//Start Test nue
          mTestStr := Format('JobSuccessful.JobManager: %d JobTyp: %d ', [mJob.JobID, ORD(mJob.JobTyp)]);
          OutputDebugString(PChar(mTestStr));
//End Test Nue
        except
          WriteLog(etError, Format('Crash in TBaseJobManager.ProcessJob: JobTyp: %s', [GetJobName(mJob.JobTyp)]));
        end;
      end;

    jtJobFailed,
    jtMsgNotComplete: begin
        try
//Start Test nue
          mTestStr := Format('jtMsgNotComplete/jtJobFailed.JobManager: %d JobTyp: %d ', [mJob.JobID, ORD(mJob.JobTyp)]);
          OutputDebugString(PChar(mTestStr));
//End Test Nue
          //last trial failed. Delete job and all dependent jobs and make other handlings
          if not Emergency(mJob.JobTyp, mJob.JobID) then
            WriteLog(etError, FormatMMErrorText(fError));

          mJob.JobID := 0;
          mJob.JobTyp := jtNewJobEv;
          WriteJobBufferTo(ttJobController);
        except
          WriteLog(etError, Format('Crash in TBaseJobManager.ProcessJob: JobTyp: %s JobID: %d -> Restart MM',
                                   [GetJobName(mJob.JobTyp), mJob.JobID]));
          //Send gcRestartMM to MM_Guard for restarting MM.
          if not WriteToMain(gcRestartMM, fError.Error) then begin
            WriteLog(etError, Format('Confirm RestartMM failed: %d', [IPCClientIndex[cMainChannelIndex]^.Error]));
            Exit;
          end;
        end;
      end;

    jtTimerEvent: begin //ok 21.7.99
    // check for expired jobs
        try
          if FirstTick then begin
            FirstTick := False;
            if not Startup then
          //Second trial
              if not Startup then begin
                WriteLog(etError, 'Last trial for Startup JobManager failed: -> Restart MM');
            //Send gcRestartMM to MM_Guard for restarting MM.
                if not WriteToMain(gcRestartMM, fError.Error) then begin
                  WriteLog(etError, Format('Confirm RestartMM failed: %d',
                    [IPCClientIndex[cMainChannelIndex]^.Error]));
                  Exit;
                end;
              end;
          end
          else
            while mJobList.ExpiredJob(xJobID, xJobTyp) do begin
          // Job is in Timeout: send Msg to Log file
              WriteLog(etError, Format('JobID: %d JobTyp: %s deleted in JobList because of timeout expired: Job/Msg/Rep = %d/%d/%d',
                                       [xJobID, GetJobName(xJobTyp), cJobTimeout[xJobTyp].JobListTmo, cJobTimeout[xJobTyp].MsgListTmo, cJobTimeout[xJobTyp].MsgListRep]));
              if not Emergency(xJobTyp, xJobID) then begin
                WriteLog(etError, FormatMMErrorText(fError));
              end;
            end;
        except
          WriteLog(etError, Format('Crash in TBaseJobManager.ProcessJob: JobTyp: %s JobID: %d -> Restart MM',
                                   [GetJobName(mJob.JobTyp), xJobID]));
  //Send gcRestartMM to MM_Guard for restarting MM.
          if not WriteToMain(gcRestartMM, fError.Error) then begin
            WriteLog(etError, Format('Confirm RestartMM failed: %d', [IPCClientIndex[cMainChannelIndex]^.Error]));
            Exit;
          end;
        end;
{}
      end;
  else
    try // call inherited ProcessJob for unknown or common JobTyp
      inherited ProcessJob;
    except
      WriteLog(etError, Format('Crash in TBaseJobManager.ProcessJob (else): JobTyp: %s', [GetJobName(mJob.JobTyp)]));
    end;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseJobManager.Init: Boolean;
begin
  Result := inherited Init;

  if Result then begin
    Result := mDB.Init;
  end;
// Disabled for tests
  mConfirmStartupImmediately := False;
  fFirstTick := True; // Flag (First tick from Timeout Ticker arrived) for start of startup
end;
//-----------------------------------------------------------------------------

//******************************************************************************
//*                            TBaseMsgController                              *
//******************************************************************************
constructor TBaseMsgController.Create(aThreadDef: TThreadDef; aMsgList: TMsgList);
begin
  inherited Create(aThreadDef);
  mMsgList := aMsgList;
end;
//-----------------------------------------------------------------------------
destructor TBaseMsgController.Destroy;
begin
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.AddJobToMsgList(aFrom, aTo: DWord): Boolean;
var
  i: Integer;
  xTime: DWord;
begin
  Result := False;
  xTime := GetTickCount;
  for i := aFrom to aTo do begin
    Result := mMsgList.AddMsg(mJob, i, cJobTimeout[mJob.JobTyp].MsgListRep, xTime, cJobTimeout[mJob.JobTyp].MsgListTmo);
    if not Result then
      break;
  end;
  if not Result then
    WriteLog(etError, 'Add Job to MsgList failed. ' + FormatMMErrorText(mMsgList.ListError));
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.DoConnect: Boolean;
var
  i: Integer;
  xNet: TNetTyp;
begin
  Result := False;
  for i := 1 to ThreadDef.ClientCount do begin
    xNet := GetNetTypOf(ConnectToIndex[i]);
    // if IPCClient connects to a NetHandler which is not installed (-> see registry)
    // free the associated object
    if (xNet in [ntTXN..ntLX]) and not Params.HasNetTyp(xNet) then begin
      IPCClientIndex[i]^.Free;
      IPCClientIndex[i]^ := nil;
    end
    else begin
      Result := IPCClientIndex[i]^.Connect;
      if not Result then
        Break;
    end;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.GetNetTypOf(aThread: TThreadTyp): TNetTyp;
begin
  case aThread of
    ttTXNWriter: Result := ntTXN;
    ttWSCWriter: Result := ntWSC;
    ttLXWriter:  Result := ntLX;
  else
    Result := ntNone;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.GetThreadTypOf(aNet: TNetTyp): TThreadTyp;
begin
  case aNet of
    ntTXN: Result := ttTXNWriter;
    ntWSC: Result := ttWSCWriter;
    ntLX:  Result := ttLXWriter;
  else
    Result := ttNone;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.Init: Boolean;
begin
  Result := inherited Init;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.MsgReceived: Boolean;
var
  xComplete: Boolean;
  xStr: string;
  xOrgJob: TJobTyp;
begin
  xOrgJob := mJob.MsgReceived.JobTyp;
  Result := mMsgList.MsgReceived(xComplete, mJob, mJobSize);
  if Result then begin
    if xComplete then begin
      if SendConfirmMsg(True, mJob.JobID, SetError(NO_ERROR, etNoError, '')) then begin

        CodeSite.SendFmtMsg('MC SendConfirmMsg: %d %s', [mJob.JobID, GetJobName(xOrgJob)]);
        if xOrgJob = jtDelZESpdData then begin
          xStr := Format('Msg %s received from MD and confirmed to JC: JobID=%d ', [GetJobName(xOrgJob), mJob.JobID]);
          WriteLogDebug(xStr);
        end;
        mMsgList.SetMsgConfirmed(mJob.JobID);
      end;
    end;
  end
  else
    WriteLog(etError, Format('MsgReceived error: NetTyp=%d, JobID=%d, Job=%s, Value=%d %s', [Integer(mJob.NetTyp), mJob.JobID, GetJobName(mJob.MsgReceived.JobTyp), mJob.MsgReceived.Value, FormatMMErrorText(mMsgList.ListError)]));
end;
//-----------------------------------------------------------------------------
procedure TBaseMsgController.ProcessJob;
var
  xCount: DWord;
  xIndex: Integer;
begin
  // handle common job of both systems: OE and MM
  case mJob.JobTyp of
    // removes all msg older than a specfied TimePeriod
    jtCleanUpEv: begin
        // to handle this job equals to other add to joblist
        if AddJobToMsgList then begin
          if mMsgList.CleanUp(mJob.CleanUpEv.TimePeriod, xCount) then begin
            if xCount > 0 then
              WriteLog(etInformation, Format('CleanUp has deleted %d items.', [xCount]));
            // get valid JobIDs from MsgList and send it to QueueManager to cleanup DataPool
            if mMsgList.GetJobIDList(mJob.CleanUpEv.NumOfJobs, mJob.CleanUpEv.JobIDList) then
              WriteJobBufferTo(ttDataPoolWriter)
            // if return where false and ListError is no NO_ERROR write to log
            else if mMsgList.ListError.ErrorTyp <> etNoError then
              WriteLog(etError, 'GetJobIDList failed: ' + FormatMMErrorText(mMsgList.ListError));
          end
          else
            WriteLog(etError, 'CleanUp failed: ' + FormatMMErrorText(mMsgList.ListError));
          // after all send confirmation to JobController
          if SendConfirmMsg(True, mJob.JobID, SetError(NO_ERROR, etNoError, '')) then
            mMsgList.SetMsgConfirmed(mJob.JobID);
        end;
      end;

    // delete items of current JobID in MsgList and send Job to QueueManager
    jtDelJobID: begin // TBaseMsgController
        if not mMsgList.DelMsg(mJob.JobID) then
          WriteLogDebug(Format('jtDelJobID: JobID %d not found in list.', [mJob.JobID]));

        if WriteJobBufferTo(ttDataPoolWriter) then
          CodeSite.SendMsg(Format('MC: jtDelJobID (%d) sent to DataPoolWriter', [mJob.JobID]));
      end;

    // JobResult
    jtJobResult: begin
        CodeSite.SendMsg(Format('JobResult received: %s', [FormatMMErrorText(mJob.JobResult.Error)]));
        if mJob.JobResult.Error.ErrorTyp <> etNoError then begin
          xIndex := mMsgList.MsgIndexFromJobID[mJob.JobID];
          case mJob.JobResult.Error.Error of
            // machine is offline
            ERROR_NOT_READY: begin
                if mMsgList.CheckRepeatCount(mJob.JobID, xIndex) then
                  // repeat the complete message
                  RepeatMessage(xIndex, False)
                else begin
                  mMsgList.SetMsgConfirmed(mJob.JobID);
                  SendConfirmMsg(False, mJob.JobID, mJob.JobResult.Error);
                end;
              end;
            ERROR_WRITE_FAULT, // NetHandler couldn't write message to machine (busy??)
              ERROR_DEV_NOT_EXIST: begin // machine is not in node table
                mMsgList.SetMsgConfirmed(mJob.JobID);
                SendConfirmMsg(False, mJob.JobID, mJob.JobResult.Error);
              end;
          else
            WriteLog(etWarning, 'JobResult doesn''t know error message');
            mMsgList.SetMsgConfirmed(mJob.JobID);
            SendConfirmMsg(False, mJob.JobID, mJob.JobResult.Error);
          end;
        end;
      end;

    jtMsgReceived: begin
        // call MsgReceived without to compare some values
        MsgReceived;
      end;
    // check for expired messages, negative confirm to JobController and set confirm flag
    jtTimerEvent: begin
        while mMsgList.GetExpiredMsg(xIndex) do begin
          // Msg is in Timeout: do I have to repeat the request?
          if mMsgList.DoRepeat(xIndex) then begin
            // repeat just the single message which is in timeout mode
            RepeatMessage(xIndex, True);
          end
          else begin
            // Msg is in Timeout: send MsgNotComplete to JobController
            mMsgList.SetMsgConfirmed(mMsgList.Items[xIndex]^.JobID);
            // override JobID from jtTimerEvent (=0) with JobID from job
            mJob.JobID := mMsgList.Items[xIndex]^.JobID;
            SendConfirmMsg(False, mJob.JobID, SetError(ERROR_TIMEOUT, etMMError, ''));
          end;
        end;
      end;
  else
    inherited ProcessJob;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.RepeatMessage(aIndex: Integer; aSingleMessage: Boolean): Boolean;
begin
  Result := True;
  // handles all messages which are only in the common way (one request -> one answer)
  // all other has to implemented in the derived class
  if mMsgList.GetOriginalJob(aIndex, mJob, mJobSize) then begin
    WriteLogDebug(Format('MC %d: Timeout: Repeat message: %s', [mJob.JobID, GetJobName(mJob.JobTyp)]));
//    CodeSite.SendMsg(Format('MC %d: Timeout: Repeat message: %s', [mJob.JobID, GetJobName(mJob.JobTyp)]));
    WriteJobToNet;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMsgController.SendConfirmMsg(aComplete: Boolean; aJobID: Word; aError: TErrorRec): Boolean;
var
  xStr: string;
begin
  if aComplete then begin
    mJob.JobTyp := jtMsgComplete;
    xStr := Format('Msg pos confirm: JobID=%d, NetTyp=%d', [aJobID, ord(mJob^.NetTyp)]);
  end
  else begin
    mJob.JobTyp := jtMsgNotComplete;
    mJob.MsgNotComplete.Error := aError;
    xStr := Format('Msg neg confirm: JobID=%d, NetTyp=%d', [aJobID, ord(mJob^.NetTyp)]);
  end;
  WriteLogDebug(xStr);

  Result := WriteJobBufferTo(ttJobController);
  if not Result then
    WriteLog(etError, 'FAILED: ' + xStr);
end;
//-----------------------------------------------------------------------------
procedure TBaseMsgController.WriteJobToNet;
var
  xNetTyp: TNetTyp;
begin
  xNetTyp := mJob.NetTyp;
  if xNetTyp in [ntTXN..ntLX] then begin
    if Params.HasNetTyp(xNetTyp) then
      WriteJobBufferTo(GetThreadTypOf(xNetTyp))
    else begin
      WriteLogDebug(Format('NetHandler ID %d not installed', [Integer(xNetTyp)]), PByte(mJob), 10);
      // Es wird dem JobController vorgegaukelt, dass diese Meldung korrekt verarbeitet wurde,
      // damit dieser nicht noch weitere Versuche unternimmt!!
      SendConfirmMsg(False, mJob.JobID, SetError(ERROR_PATH_NOT_FOUND, etMMError, 'NetTyp not available!'));
      // Da dieser Handler ja nicht vorhanden ist, muss der MsgController sich selber best�tigen
      mMsgList.SetMsgConfirmed(mJob.JobID);
    end;
  end
  else
    WriteLog(etWarning, Format('Unknown NetHandler ID: %d', [Integer(xNetTyp)]), PByte(mJob), 10);
end;
//-----------------------------------------------------------------------------

//******************************************************************************
//*                            TBaseMsgDispatcher                              *
//******************************************************************************
procedure TBaseMsgDispatcher.ConfirmMsgReceipt(aValue: DWord);
begin
  // keep actual JobTyp in subrecord for comparing in MsgController
  mJob.MsgReceived.JobTyp := mJob.JobTyp;
  // !!!! confirm dependent values has to be passed as parameter ELSE it is 0 !!!!
  mJob.MsgReceived.Value := aValue;
  // JobTyp is now MsgReceived
  mJob.JobTyp := jtMsgReceived;
  // send Job to MsgController
  WriteJobBufferTo(ttMsgController);
end;
//-----------------------------------------------------------------------------
constructor TBaseMsgDispatcher.Create(aThreadDef: TThreadDef; aMsgList: TMsgList);
begin
  inherited Create(aThreadDef);
  mMsgList := aMsgList;
end;
//-----------------------------------------------------------------------------
function TBaseMsgDispatcher.GetNextJobID: DWord;
begin
  Result := mNextJobID;
  CircInc(mNextJobID, 1, mMaxJobID);
end;
//-----------------------------------------------------------------------------
function TBaseMsgDispatcher.Init: Boolean;
begin
  Result := inherited Init;
  mMaxJobID := High(DWord) div 2; // member the highest valid automatic generated JobID
  mNextJobID := 1; // 0 means no JobID -> cleared from NetHandler on auto data
end;
//-----------------------------------------------------------------------------
procedure TBaseMsgDispatcher.ProcessJob;
begin
  // no common implementation for MM and OE yet
  inherited ProcessJob;
end;
//-----------------------------------------------------------------------------

//******************************************************************************
//*                            TBaseQueueManager                             *
//******************************************************************************
constructor TBaseQueueManager.Create(aThreadDef: TThreadDef; aJobQueue: TJobQueue);
begin
  inherited Create(aThreadDef);
  mJobQueue := aJobQueue;
end;
//-----------------------------------------------------------------------------
destructor TBaseQueueManager.Destroy;
begin
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseQueueManager.Init: Boolean;
begin
  Result := inherited Init;
end;
//-----------------------------------------------------------------------------
procedure TBaseQueueManager.ProcessJob;
begin
  inherited ProcessJob;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//*                            TBaseWorkerX                                    *
//******************************************************************************
constructor TBaseWorker.Create(aThreadDef: TThreadDef; aJobQueue: TJobQueue;
  aDataPool: TDataPool; aJobCtrlPort: TJobCtrlPort);
begin
  inherited Create(aThreadDef);
  mDataPool          := aDataPool;
  mEditor            := nil;
  mJobQueue          := aJobQueue;
  mJobCtrlPort       := aJobCtrlPort;
  mJobError.ErrorTyp := etNoError;
  mJobError.Error    := NO_ERROR;
  mJobError.Msg      := '';
  mDB                := TAdoDBAccess.Create(cMaxQueries);
end;
//-----------------------------------------------------------------------------
destructor TBaseWorker.Destroy;
begin
  // um zu verhindern, dass laufende Jobs weiterhin auf die Datenbank zugreifen,
  // muss dieser zuerst beendet werden. Ansonsten k�nnen AV entstehen.
  if Assigned(mEditor) then begin
    WriteLog(etWarning, 'TBaseWorker.Destroy: mEditor <> Nil');
    try
      FreeAndNil(mEditor);
    except
      on e: Exception do
        WriteLog(etError, 'TBaseWorker.Destroy failed: ' + e.Message);
    end;
  end;
  try
    mDB.Free;
  except
    on e: Exception do
      WriteLog(etError, 'TBaseWorker.Destroy mDB.Free failed: ' + e.Message);
  end;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseWorker.getNextJob: Boolean;
begin
  if ThreadDef.ThreadTyp = ttExpressWorker then
    Result := mJobQueue.getExpressJob(mJob)
  else
    Result := mJobQueue.getJob(mJob);
  if not Result then
    WriteLog(etError, 'TBaseWorker.getNextJob failed. ' + FormatMMErrorText(mJobQueue.QueueError));

{
  while ( not xResult )do begin
    if ThreadDef.ThreadTyp = ttExpressWorker then
      xResult := mJobQueue.getExpressJob ( @mJob )
    else
      xResult := mJobQueue.getJob ( @mJob );
    if not xResult then
      WriteLog ( etError, 'TBaseWorker.getNextJob failed. ' + FormatMMErrorText ( mJobQueue.Error ) );
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TBaseWorker.ProcessJob;
begin
  // get a Job from the JobQueue
  if getNextJob then begin // the Job will be written in mJob
    mJobError := SetError(NO_ERROR, etNoError, '');
    // Anhand vom Job wird ein dazu ben�tigtes Objekt erstellt
    mEditor := getJobEditor;
    case mJob.JobTyp of
      jtGenShiftData:
        WriteLogDebug(Format('START BaseWorker: Job=%s JobID=%d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenShiftData.ProdID]));
      jtGenExpshiftData:
        WriteLogDebug(Format('START BaseWorker: Job=%s JobID=%d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenExpShiftData.ProdID]));
    else
      WriteLogDebug(Format('START BaseWorker: Job=%s JobID=%d',
        [GetJobName(mJob.JobTyp), mJob.JobID]));
    end;
    if mEditor = nil then begin
      WriteLog(etWarning, 'No Editor available for Job : ' + GetJobName(mJob.JobTyp));
      Exit;
    end;

    mEditor.DebugMode := DebugMode;
    if mEditor.Init then begin
      Priority := mEditor.Priority;
      try
        mJobError := mEditor.ProcessJob;
        WriteLogDebug(Format('DONE BaseWorker: Job=%s JobID=%d',
          [GetJobName(mJob.JobTyp), mJob.JobID]));
      except
        on e: Exception do begin
          mJobError := SetError(ERROR_INVALID_FUNCTION, etMMerror, 'ProcessJob of ' + mEditor.ClassName + ' failed. ' + e.Message);
          WriteLog(etError, FormatMMErrorText(mJobError));
        end;
      end;
    end
    else begin
      WriteLog(etError, 'Init of Editor failed. Editor = ' + GetJobName(mJob.JobTyp) + ' Error : ' + FormatMMErrorText(mEditor.Error));
      mJobError := mEditor.Error;
    end;
    FreeAndNil(mEditor);

    // confirm Job to JobController
    if not WriteConfirm then begin
      WriteLog(etError, 'Write failed to JobController. ' + FormatErrorText(mJobCtrlPort.Error));
    end;
  end
  else
    raise Exception.Create('GetMutex, UnlockList or Exception in getNextJob failed');
end;
//-----------------------------------------------------------------------------
procedure TBaseWorker.ProcessInitJob;
begin
  ProcessJob;
end;
//-----------------------------------------------------------------------------
function TBaseWorker.Init: Boolean;
begin
  Result := inherited Init;
  if Result then
    Result := mDB.Init;
end;
//------------------------------------------------------------------------------
function TBaseWorker.WriteConfirm: Boolean;
begin
  case mJob.JobTyp of
    jtDelJobID, // TBaseWorker.WriteConfirm
      jtDiagnostic: begin
        // no message to JobController
        Result := True;
        Exit;
      end;
    jtGenDWData, jtGenExpDWData: ; //WriteLog ( etInformation, 'DW/ExpDW JobError : ' + FormatMMErrorText ( mJobError ) );
  end;
  if mJobError.ErrorTyp = etNoError then begin
    case mJob.JobTyp of
      jtGenShiftData:
        WriteLogDebug(Format('Confirm successful to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenShiftData.ProdID]), nil, 0, True);
      jtGenExpshiftData:
        WriteLogDebug(Format('Confirm successful to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenExpShiftData.ProdID]), nil, 0, True);
      jtGetDataStopZESpd:
        WriteLogDebug(Format('Confirm successful to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GetDataStopZESpd.ProdID]), nil, 0, True);
    else
    end;
    mJob.JobTyp := jtJobSuccessful;
  end
  else begin
    case mJob.JobTyp of
      jtGenShiftData:
        WriteLogDebug(Format('Confirm JobFailed to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenShiftData.ProdID]), nil, 0, True);
      jtGenExpshiftData:
        WriteLogDebug(Format('Confirm JobFailed to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GenExpShiftData.ProdID]), nil, 0, True);
      jtGetDataStopZESpd:
        WriteLogDebug(Format('Confirm JobFailed to JobContr: Job = %s JobID= %d ProdID= %d',
          [GetJobName(mJob.JobTyp), mJob.JobID, mJob.GetDataStopZESpd.ProdID]), nil, 0, True);
    else
    end;
    mJob.JobTyp := jtJobFailed;
    mJob.JobFailed.Error := mJobError;
  end;
  Result := mJobCtrlPort.WriteToJobController(mJob);
end;
//------------------------------------------------------------------------------
function TBaseWorker.GetPriority: TThreadPriority;
begin
  Result := inherited Priority;
end;
//------------------------------------------------------------------------------
procedure TBaseWorker.SetPriority(aPriority: TThreadPriority);
begin
  inherited Priority := aPriority;
  Sleep(1);
end;
//******************************************************************************
//*                            TBaseEditor                                     *
//******************************************************************************
constructor TBaseJobEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create;

  mJobSize     := CloneJob(aJob, mJob);
  mDB          := aDB;
  mEventLog    := aEventLog;
  mEventLogDeb := nil;
  fPriority    := tpLower;
end;
//------------------------------------------------------------------------------
destructor TBaseJobEditor.Destroy;
begin
  FreeMem(mJob);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseJobEditor.Init: boolean;
var
  x: integer;
  xQuerycleanUp: Integer;
begin
  Result := True;
  try
    if mDB.DataBase.InTransaction then begin
//      WriteLogDebug ('BaseJobEditor.Init Transaction CleanUp',Nil,0);
      mDB.DataBase.Commit;
    end;
    xQuerycleanUp := 0;
    for x := 0 to cMaxQueries - 1 do
      if mDB.Query[x].active then begin
        INC(xQuerycleanUp);
        mDB.Query[x].Close;
      end;
    if xQuerycleanUp <> 0 then
//      WriteLogDebug ('BaseJobEditor.Init Query CleanUp '+ inttostr(xQuerycleanUp),Nil,0);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBaseEditor.Init failed. ' + e.Message);
      Result := False;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseJobEditor.ProcessJob: TErrorRec;
begin
  Result.ErrorTyp := etNoError;
  Result.Error := NO_ERROR;
  Result.Msg := '';
end;
//------------------------------------------------------------------------------
{
function TBaseJobEditor.BroadcastToMMClient ( aApplMsg : TApplMsg ) : boolean;
var   xMailslot : TMailslotWriter;
      xMsg      : TMMClientRec;
      xCount    : DWord;
begin
  xMailslot := TMailslotWriter.Create ( '*', cChannelNames [ ttMMClientReader ] );
  Result := xMailslot.Init;
  if Result then begin
    FillChar ( xMsg, sizeof ( xMsg ), 0 );
    xMsg.MsgTyp := ccMMApplMsg;
// @@@@@@ gMMHost aus Setup Komponente
    xMsg.ServerName := ''; // gMMHost
    xMsg.Error.ErrorTyp := etNoError;
    xMsg.Error.Error := NO_ERROR;
    xMsg.ApplMsg := aApplMsg;
    Result := xMailslot.Write ( @xMsg, sizeof ( xMsg ), xCount );
  end;
  if not Result then begin
    fError := SetError ( xMailslot.Error, etNTError, 'Broadcast failed.' );
  end;
  xMailslot.Free;
end;
}
//------------------------------------------------------------------------------
procedure TBaseJobEditor.WriteLog(aEvent: TEventType; aText: string; aBuf: PByte; aCount: DWord);
begin
  if Assigned(aBuf) and (aCount > 0) then
    mEventLog.WriteBin(aEvent, ClassName + ' : ' + aText, aBuf, aCount)
  else
    mEventLog.Write(aEvent, ClassName + ' : ' + aText);
end;
//------------------------------------------------------------------------------
procedure TBaseJobEditor.WriteLogDebug(aText: string; aBuf: PByte = nil; aCount: DWord = 0);
begin
  if DebugMode then begin
    WriteToEventLog(aText, cEventLogDefaultText[ttWorker], etInformation, cEventLogClassForSubSystemsDebug, '.', ssStorageHandler);
{
    if not Assigned ( mEventLogDeb ) then
       mEventLogDeb := TEventLogWriter.Create ( cEventLogClassForSubSystemsDebug, '.', ssStorageHandler, ClassName + ' : ', True);

    if Assigned ( aBuf ) then
      mEventLogDeb.WriteBin ( etInformation , aText, aBuf, aCount )
    else
      mEventLogDeb.Write ( etInformation , aText );
{}
  end;
end;
//------------------------------------------------------------------------------
function TBaseJobEditor.IsError(aError: TErrorRec): boolean;
begin
  Result := not (aError.ErrorTyp = etNoError);
end;
//******************************************************************************
//*                            TJobCtrlPort                                    *
//******************************************************************************
constructor TJobCtrlPort.Create;
begin
  inherited Create;
  mPort := TIPCClient.Create('.', cChannelNames[ttJobController]);
  mCritSec := TCriticalSection.Create;
end;
//------------------------------------------------------------------------------
destructor TJobCtrlPort.Destroy;
begin
  mPort.Free;
  mCritSec.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TJobCtrlPort.Connect: boolean;
begin
  Result := mPort.Connect;
  if not Result then
    fError := mPort.Error;
end;
//------------------------------------------------------------------------------
function TJobCtrlPort.WriteToJobController(aJob: PJobRec): boolean;
begin
  mCritSec.Enter;
  try
    aJob^.JobLen := GetJobDataSize(aJob);
    Result       := mPort.Write(PByte(aJob), aJob^.JobLen);
    if not Result then
      fError := mPort.Error;
  finally
    mCritSec.Leave;
  end;
end;
//******************************************************************************
//*                            TBaseDataPoolEditor                             *
//******************************************************************************
constructor TBaseDataPoolEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
//DONE: XML:: inherited modifizieren
  inherited Create(aJob, aDB, aEventLog);

  mDataPool := aDataPool;
end;
//------------------------------------------------------------------------------
function TBaseDataPoolEditor.GetAllJobIDs(var aJobIDList: TJobIDList; var aNumJobs: Word): boolean;
begin
  Result := mDataPool.GetAllJobIDs(aJobIDList, aNumJobs);
  if not Result then begin
    fError := SetError(mDataPool.ListError, etNTError, 'Can not get the Job IDs');
  end;
end;
//------------------------------------------------------------------------------
function TBaseDataPoolEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
end;
//******************************************************************************
//*                          TDataPoolLineEditor                               *
//******************************************************************************
constructor TBaseDataPoolLineEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
//DONE: XML:: inherited modifizieren
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  fDataList := nil;
end;
//------------------------------------------------------------------------------
function TBaseDataPoolLineEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if not IsError(Result) then begin
    if not mDataPool.GetList(fDataList, mJob.JobID) then begin
      // Wait and try again
      Sleep(1000);
      if not mDataPool.GetList(fDataList, mJob.JobID) then begin
        Result := Error;
        fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'JobID not available in DataPool. JobID : ' + IntToStr(mJob.JobID));
        WriteLog(etError, FormatMMErrorText(Error));
      end;
    end;
  end;
end;
//-----------------------------------------------------------------------------

//******************************************************************************
//*                            TTimeoutTicker                                  *
//******************************************************************************
constructor TTimeoutTicker.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mSleepTime := cDefaultTimeoutTickerTime;
end;
//-----------------------------------------------------------------------------
procedure TTimeoutTicker.ProcessInitJob;
begin
  ProcessJob;
end;
//-----------------------------------------------------------------------------
procedure TTimeoutTicker.ProcessJob;
begin
  // DO NOT CALL INHERITED PROCESSJOB. IT HAS NO JOB TO BE HANDLED
  Sleep(mSleepTime);

  mJob.JobTyp := jtTimerEvent;
  mJob.NetTyp := ntNone;

// send TimerEvent always to the second IPCClient definition
// --> the first IPCClient connects to main program
  WriteJobBufferTo(ConnectToIndex[2]);
end;
//-----------------------------------------------------------------------------

begin
//  ggFirstSettings := True;
end.

