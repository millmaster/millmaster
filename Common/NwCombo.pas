(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: Nwcombo.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Parameter-Auswahl als Profil speichern
| Info..........: -
| Develop.system: Windows 2000 SP3
| Target.system.: Windows N2000
| Compiler/Tools: Delphi 5.01, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
|                       | Unbekannt
| 04.10.2002        LOK | Umbau ADO
|=========================================================================================*)
unit Nwcombo;
interface
uses
 SysUtils,WinTypes,WinProcs,Messages,Classes,Graphics,Controls,Forms,Dialogs,
 StdCtrls,mmAdoDataset, LoepfeGlobal;

type
 TNWComboBox = class(TComboBox)
 private
  FNumCols: integer;
  procedure SetNumCols(value: integer);
 protected
 public
  constructor Create(AOwner: TComponent); override;
  function GetItemID: integer;
  function GetItemText: string;
  procedure SetItemID(ID: integer);
  procedure SetItemText(s: string);
  function GetItemIDPos(ID: integer): integer;
  function GetItemIDAtPos(pos: integer): integer;
  procedure AddObj(s: string; id: integer);
  procedure ReadFromSQL(aDatabaseName,aTableName,aOrderByStr,aTextField,aIDField: string);
 published
  property NumCols: integer read FNumCols write SetNumCols default 1;
 end; //TNWComboBox

 TNWListBox = class(TListBox)
 private
 protected
 public
  constructor Create(AOwner: TComponent); override;
  function GetItemID: longint;
  function GetItemText: string;
  procedure SetItemID(ID: longint);
  procedure SetItemText(s: string);
  function GetItemIDPos(ID: longint): integer;
  function GetItemIDAtPos(pos: integer): longint;
  procedure AddObj(s: string; id: longint);
  procedure ReadFromSQL(aDatabaseName,aTableName,aOrderByStr,aTextField,aIDField: string);
 published
end; //TNWListBox

procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

const
 cNegIDAdd = 1273831111; //workaround for delphi 5 error if TObject < 0

function SetInternalID(aID: integer): integer;
begin
 if aID < 0 then result := cNegIDAdd +abs(aID)
            else result := aID;
end; //function SetInternalID
//-----------------------------------------------------------------------------
function GetID(aID: integer): integer;
begin
 if aID > cNegIDAdd then result := -(aID mod cNegIDAdd)
                    else result := aID;
end; //function GetID

//-----------------------------------------------------------------------------

constructor TNWComboBox.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 Style := csDropDownList;
 FNumCols := 1;
 DropDownCount := 12;
 Sorted := false;
end; //constructor TNWComboBox.Create
//-----------------------------------------------------------------------------
procedure TNWComboBox.SetNumCols(value: integer);
begin
 if value < 1 then Value := 1;
 if value > 3 then Value := 3;
 if FNumCols <> value then
 begin
  FNumCols := value;
  Refresh;
 end; //if FNumCols <> value then
end; //procedure TNWComboBox.SetNumCols
//-----------------------------------------------------------------------------
function TNWComboBox.GetItemID: integer;
begin
 result := integer(Items.Objects[ItemIndex]);
 result := GetID(result);
end; //function TNWComboBox.GetItemID
//-----------------------------------------------------------------------------
function TNWComboBox.GetItemText: string;
begin
 result := Items[ItemIndex];
end; //function TNWComboBox.GetItemText
//-----------------------------------------------------------------------------
procedure TNWComboBox.SetItemID(ID: integer);
begin
 id := SetInternalID(id);
 ItemIndex := Items.IndexOfObject(TObject(id));
end; //procedure TNWComboBox.SetItemPos
//-----------------------------------------------------------------------------
procedure TNWComboBox.SetItemText(s: string);
begin
 ItemIndex := Items.IndexOf(s);
end; //procedure TNWComboBox.SetItemText
//-----------------------------------------------------------------------------
function TNWComboBox.GetItemIDPos(ID: integer): integer;
begin
 id := SetInternalID(id);
 result := Items.IndexOfObject(TObject(id));
end; //procedure TNWComboBox.SetItemPos
//-----------------------------------------------------------------------------
function TNWComboBox.GetItemIDAtPos(pos: integer): integer;
begin
 result := integer(Items.Objects[pos]);
 result := GetID(result);
end; //function TNWComboBox.GetItemIDAtPos
//-----------------------------------------------------------------------------
procedure TNWComboBox.AddObj(s: string; id: integer);
begin
 id := SetInternalID(id);
 with Items do
  AddObject(s,TObject(id));
end; //procedure TNWComboBox.AddObj
//-----------------------------------------------------------------------------
procedure TNWComboBox.ReadFromSQL(aDatabaseName,aTableName,aOrderByStr,aTextField,aIDField: string);
var
 q : TmmADODataset;

begin
 q := TmmADODataset.Create(Application);
 try
  q.ConnectionString := GetDefaultConnectionString;

  q.CommandText := format('SELECT %s, %s FROM %s ',[aTextField,aIDField,aTableName]);
  q.CommandText := q.CommandText + format('ORDER BY %s',[aOrderByStr]);
  q.Open;

  Items.BeginUpdate;
  try
   while not q.Eof do
   begin
    AddObj(q.FieldByName(aTextField).AsString,q.FieldByName(aIDField).AsInteger);
    q.Next;
   end; //while not q.Eof do
  finally
   Items.EndUpdate;
  end; //try..finally
 finally
  q.Close;
  q.free;
 end; //try..finally
end; //procedure TNWComboBox.ReadFromSQL


//-----------------------------------------------------------------------------


constructor TNWListBox.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
 Sorted := false;
end; //constructor TNWListBox.Create
//-----------------------------------------------------------------------------
function TNWListBox.GetItemID: longint;
begin
 result := Longint(Items.Objects[ItemIndex]);
 result := GetID(result);
end; //function TNWListBox.GetItemID
//-----------------------------------------------------------------------------
function TNWListBox.GetItemText: string;
begin
 result := Items[ItemIndex];
end; //function TNWListBox.GetItemText
//-----------------------------------------------------------------------------
procedure TNWListBox.SetItemID(ID: longint);
begin
 id := SetInternalID(id);
 ItemIndex := Items.IndexOfObject(TObject(id));
end; //procedure TNWListBox.SetItemPos
//-----------------------------------------------------------------------------
procedure TNWListBox.SetItemText(s: string);
begin
 ItemIndex := Items.IndexOf(s);
end; //procedure TNWListBox.SetItemText
//-----------------------------------------------------------------------------
function TNWListBox.GetItemIDPos(ID: longint): integer;
begin
 id := SetInternalID(id);
 result := Items.IndexOfObject(TObject(id));
end; //procedure TNWListBox.SetItemPos
//-----------------------------------------------------------------------------
function TNWListBox.GetItemIDAtPos(pos: integer): longint;
begin
 result := integer(Items.Objects[pos]);
 result := GetID(result);
end; //function TNWListBox.GetItemIDAtPos
//-----------------------------------------------------------------------------
procedure TNWListBox.AddObj(s: string; id: longint);
begin
 id := SetInternalID(id);
 with Items do
  AddObject(s,TObject(id));
end; //procedure TNWListBox.AddObj
//-----------------------------------------------------------------------------
procedure TNWListBox.ReadFromSQL(aDatabaseName,aTableName,aOrderByStr,aTextField,aIDField: string);
var
 q : TmmADODataset;

begin
 q := TmmADODataset.Create(Application);
 try
  q.ConnectionString := GetDefaultConnectionString;

  q.CommandText := format('SELECT %s, %s FROM %s ',[aTextField,aIDField,aTableName]);
  q.CommandText := q.CommandText + format('ORDER BY %s',[aOrderByStr]);
  q.Open;

  Items.BeginUpdate;
  try
   while not q.Eof do
   begin
    AddObj(q.FieldByName(aTextField).AsString,q.FieldByName(aIDField).AsInteger);
    q.Next;
   end; //while not q.Eof do
  finally
   Items.EndUpdate;
  end; //try..finally
 finally
  q.Close;
  q.free;
 end; //try..finally
end; //procedure TNWListBox.ReadFromSQL

//-----------------------------------------------------------------------------

procedure Register;
begin
 RegisterComponents('Worbis', [TNWComboBox,TNWListBox]);
end; //procedure Register

end. //Nwcombo.pas
