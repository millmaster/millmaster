(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphEditSerie.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: dialog interface for graph series editor  
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.01 | PW  |
|=========================================================================================*)

unit mmGraphEditSerie;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 mmGrapheditSerieDlg,mmGraphGlobal;

type
 TmmGraphEditSeries = class(TComponent)
  private
   FNumSeries: integer;
   FYAutoCalc: array[eYAxisT] of boolean;
   FYDefaultMax: array[eYAxisT] of ValueType;
   FYDefaultMin: array[eYAxisT] of ValueType;
   FSeriesColor: array[1..cMaxSeries] of TColor;
   FSeriesYScalingType: array[1..cMaxSeries] of eYScalingTypeT;
   FSeriesName: array[1..cMaxSeries] of string;
   FSeriesVisible: array[1..cMaxSeries] of boolean;
   FSeriesYAxis: array[1..cMaxSeries] of eYAxisT;
   FStatistikData: array[1..cMaxSeries] of sStatistikT;
   FSeriesShowUpperLimit: array[1..cMaxSeries] of boolean;
   FSeriesShowLowerLimit: array[1..cMaxSeries] of boolean;
   FSeriesUpperLimit: array[1..cMaxSeries] of ValueType;
   FSeriesLowerLimit: array[1..cMaxSeries] of ValueType;

   procedure SetSeriesUpperLimit(index: integer; value: ValueType);
   function GetSeriesUpperLimit(index: integer): ValueType;
   procedure SetSeriesLowerLimit(index: integer; value: ValueType);
   function GetSeriesLowerLimit(index: integer): ValueType;
   procedure SetSeriesShowUpperLimit(index: integer; value: boolean);
   function GetSeriesShowUpperLimit(index: integer): boolean;
   procedure SetSeriesShowLowerLimit(index: integer; value: boolean);
   function GetSeriesShowLowerLimit(index: integer): boolean;
   procedure SetSeriesName(index: integer; value: string);
   function GetSeriesName(index: integer): string;
   procedure SetYAutoCalc(index: eYAxisT; value: boolean);
   function GetYAutoCalc(index: eYAxisT): boolean;
   procedure SetYDefaultMin(index: eYAxisT; value: ValueType);
   function GetYDefaultMin(index: eYAxisT): ValueType;
   procedure SetYDefaultMax(index: eYAxisT; value: ValueType);
   function GetYDefaultMax(index: eYAxisT): ValueType;
   procedure SetSeriesYScalingType(index: integer; value: eYScalingTypeT);
   function GetSeriesYScalingType(index: integer): eYScalingTypeT;
   procedure SetSeriesYAxis(index: integer; value: eYAxisT);
   function GetSeriesYAxis(index: integer): eYAxisT;
   procedure SetSeriesColor(index: integer; value: TColor);
   function GetSeriesColor(index: integer): TColor;
   procedure SetSeriesVisible(index: integer; value: boolean);
   function GetSeriesVisible(index: integer): boolean;
   procedure SetStatistikData(index: integer; value: sStatistikT);
   function GetStatistikData(index: integer): sStatistikT;
  protected
  public
   function Execute: boolean;
   property NumSeries: integer read FNumSeries write FNumSeries;
   property SeriesColor[index: integer]: TColor read GetSeriesColor write SetSeriesColor;
   property SeriesName[index: integer]: string read GetSeriesName write SetSeriesName;
   property SeriesVisible[index: integer]: boolean read GetSeriesVisible write SetSeriesVisible;
   property SeriesYAxis[index: integer]: eYAxisT read GetSeriesYAxis write SetSeriesYAxis;
   property StatistikData[index: integer]: sStatistikT read GetStatistikData write SetStatistikData;
   property YAutoCalc[index: eYAxisT]: boolean read GetYAutoCalc write SetYAutoCalc;
   property YDefaultMin[index: eYAxisT]: ValueType read GetYDefaultMin write SetYDefaultMin;
   property YDefaultMax[index: eYAxisT]: ValueType read GetYDefaultMax write SetYDefaultMax;
   property SeriesYScalingType[index: integer]: eYScalingTypeT read GetSeriesYScalingType write SetSeriesYScalingType;
   property SeriesUpperLimit[index: integer]: ValueType read GetSeriesUpperLimit write SetSeriesUpperLimit;
   property SeriesLowerLimit[index: integer]: ValueType read GetSeriesLowerLimit write SetSeriesLowerLimit;
   property SeriesShowUpperLimit[index: integer]: boolean read GetSeriesShowUpperLimit write SetSeriesShowUpperLimit;
   property SeriesShowLowerLimit[index: integer]: boolean read GetSeriesShowLowerLimit write SetSeriesShowLowerLimit;
  published
 end; //TmmGraphEditSeries

//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure TmmGraphEditSeries.SetSeriesColor(index: integer; value: TColor);
begin
 if value <> FSeriesColor[Index] then FSeriesColor[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesColor
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesColor(index: integer): TColor;
begin
 result := FSeriesColor[index];
end; //function TmmGraphEditSeries.GetSeriesColor
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesName(index: integer; value: string);
begin
 if value <> FSeriesName[Index] then FSeriesName[index] := value;
end; //procedure TmmGraphEditSeries.SetName
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesName(index: integer): string;
begin
 result := FSeriesName[index];
end; //function TmmGraphEditSeries.GetName
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesVisible(index: integer; value: boolean);
begin
 if value <> FSeriesVisible[Index] then FSeriesVisible[index] := value;
end; //procedure TmmGraphEditSeries.SetVisible
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesVisible(index: integer): boolean;
begin
 result := FSeriesVisible[index];
end; //function TmmGraphEditSeries.GetSeriesVisible
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesYAxis(index: integer; value: eYAxisT);
begin
 if value <> FSeriesYAxis[Index] then FSeriesYAxis[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesYAxis
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesYAxis(index: integer): eYAxisT;
begin
 result := FSeriesYAxis[index];
end; //function TmmGraphEditSeries.GetSeriesYAxis
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetStatistikData(index: integer; value: sStatistikT);
begin
 if value <> FStatistikData[Index] then FStatistikData[index] := value;
end; //procedure TmmGraphEditSeries.SetStatistikData
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetStatistikData(index: integer): sStatistikT;
begin
 result := FStatistikData[index];
end; //function TmmGraphEditSeries.GetStatistikData
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesUpperLimit(index: integer; value: ValueType);
begin
 if value <> FSeriesUpperLimit[Index] then FSeriesUpperLimit[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesUpperLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesUpperLimit(index: integer): ValueType;
begin
 result := FSeriesUpperLimit[index];
end; //function TmmGraphEditSeries.GetSeriesUpperLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesLowerLimit(index: integer; value: ValueType);
begin
 if value <> FSeriesLowerLimit[Index] then FSeriesLowerLimit[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesLowerLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesLowerLimit(index: integer): ValueType;
begin
 result := FSeriesLowerLimit[index];
end; //function TmmGraphEditSeries.GetSeriesLowerLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesShowUpperLimit(index: integer; value: boolean);
begin
 if value <> FSeriesShowUpperLimit[Index] then FSeriesShowUpperLimit[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesShowUpperLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesShowUpperLimit(index: integer): boolean;
begin
 result := FSeriesShowUpperLimit[index];
end; //function TmmGraphEditSeries.GetSeriesShowUpperLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesShowLowerLimit(index: integer; value: boolean);
begin
 if value <> FSeriesShowLowerLimit[Index] then FSeriesShowLowerLimit[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesShowLowerLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesShowLowerLimit(index: integer): boolean;
begin
 result := FSeriesShowLowerLimit[index];
end; //function TmmGraphEditSeries.GetSeriesShowLowerLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetYAutoCalc(index: eYAxisT; value: boolean);
begin
 if value <> FYAutoCalc[Index] then FYAutoCalc[index] := value;
end; //procedure TmmGraphEditSeries.SetYAutoCalc
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetYAutoCalc(index: eYAxisT): boolean;
begin
 result := FYAutoCalc[index];
end; //function TmmGraphEditSeries.GetYAutoCalc
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetYDefaultMin(index: eYAxisT; value: ValueType);
begin
 if value <> FYDefaultMin[Index] then FYDefaultMin[index] := value;
end; //procedure TmmGraphEditSeries.SetYDefaultMin
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetYDefaultMin(index: eYAxisT): ValueType;
begin
 result := FYDefaultMin[index];
end; //function TmmGraphEditSeries.GetYDefaultMin
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetYDefaultMax(index: eYAxisT; value: ValueType);
begin
 if value <> FYDefaultMax[Index] then FYDefaultMax[index] := value;
end; //procedure TmmGraphEditSeries.SetYDefaultMax
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetYDefaultMax(index: eYAxisT): ValueType;
begin
 result := FYDefaultMax[index];
end; //function TmmGraphEditSeries.GetYDefaultMax
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeries.SetSeriesYScalingType(index: integer; value: eYScalingTypeT);
begin
 if value <> FSeriesYScalingType[Index] then FSeriesYScalingType[index] := value;
end; //procedure TmmGraphEditSeries.SetSeriesYScalingType
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.GetSeriesYScalingType(index: integer): eYScalingTypeT;
begin
 result := FSeriesYScalingType[index];
end; //function TmmGraphEditSeries.GetSeriesYScalingType
//-----------------------------------------------------------------------------
function TmmGraphEditSeries.Execute: boolean;
var
 i : integer;

begin
 mmGraphEditSeriesDlg := TmmGraphEditSeriesDlg.Create(Application);
 try
  mmGraphEditSeriesDlg.NumSeries := NumSeries;

  for i := 1 to NumSeries do
  begin
   mmGraphEditSeriesDlg.SeriesYScalingType[i] := SeriesYScalingType[i];
   mmGraphEditSeriesDlg.SeriesShowUpperLimit[i] := SeriesShowUpperLimit[i];
   mmGraphEditSeriesDlg.SeriesShowLowerLimit[i] := SeriesShowLowerLimit[i];
   mmGraphEditSeriesDlg.SeriesUpperLimit[i] := SeriesUpperLimit[i];
   mmGraphEditSeriesDlg.SeriesLowerLimit[i] := SeriesLowerLimit[i];
   mmGraphEditSeriesDlg.SeriesColor[i] := SeriesColor[i];
   mmGraphEditSeriesDlg.SeriesName[i] := SeriesName[i];
   mmGraphEditSeriesDlg.SeriesVisible[i] := SeriesVisible[i];
   mmGraphEditSeriesDlg.SeriesYAxis[i] := SeriesYAxis[i];
   mmGraphEditSeriesDlg.StatistikData[i] := StatistikData[i];
  end; //for i := 1 to NumSeries do

  result := mmGraphEditSeriesDlg.ShowModal = IDOk;
  if result then
  begin
   for i := 1 to NumSeries do
   begin
    SeriesYScalingType[i] := mmGraphEditSeriesDlg.SeriesYScalingType[i];
    SeriesShowUpperLimit[i] := mmGraphEditSeriesDlg.SeriesShowUpperLimit[i];
    SeriesShowLowerLimit[i] := mmGraphEditSeriesDlg.SeriesShowLowerLimit[i];
    SeriesUpperLimit[i] := mmGraphEditSeriesDlg.SeriesUpperLimit[i];
    SeriesLowerLimit[i] := mmGraphEditSeriesDlg.SeriesLowerLimit[i];
    SeriesColor[i] := mmGraphEditSeriesDlg.SeriesColor[i];
    SeriesName[i] := mmGraphEditSeriesDlg.SeriesName[i];
    SeriesVisible[i] := mmGraphEditSeriesDlg.SeriesVisible[i];
    SeriesYAxis[i] := mmGraphEditSeriesDlg.SeriesYAxis[i];
    StatistikData[i] := mmGraphEditSeriesDlg.StatistikData[i];
   end; //for i := 1 to NumSeries do
  end; //if result then
 finally
  mmGraphEditSeriesDlg.Free;
 end; //try..finally
end; //function TmmGraphEditSeries.Execute

end. //mmGraphEditSeries.pas
