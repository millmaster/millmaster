inherited mmGraphSortSeriesDlg: TmmGraphSortSeriesDlg
  Left = 467
  Top = 289
  Width = 253
  Height = 288
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSizeToolWin
  Caption = '(60)Sortierung festlegen'
  Constraints.MaxHeight = 371
  Constraints.MaxWidth = 253
  Constraints.MinHeight = 227
  Constraints.MinWidth = 253
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TmmPanel
    Left = 0
    Top = 0
    Width = 245
    Height = 231
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 0
    object gbStatistik: TmmGroupBox
      Left = 5
      Top = 5
      Width = 235
      Height = 156
      Anchors = [akLeft, akTop, akBottom]
      Caption = '(60)Sortierkriterium auswaehlen'
      TabOrder = 0
      object cobSortBy: TmmListBox
        Left = 10
        Top = 20
        Width = 215
        Height = 126
        Anchors = [akLeft, akTop, akBottom]
        ItemHeight = 13
        Items.Strings = (
          'X'
          '1'
          '2'
          '3'
          '4'
          '5'
          '6'
          '7'
          '8'
          '9'
          '10'
          '11'
          '12'
          '13'
          '14'
          '15')
        TabOrder = 0
        Visible = True
        OnClick = cobSortByClick
        OnDblClick = acOKExecute
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object rgOrder: TmmRadioGroup
      Left = 5
      Top = 166
      Width = 235
      Height = 60
      Anchors = [akLeft, akBottom]
      Caption = '(60)Sortierfolge waehlen'
      Items.Strings = (
        '(35)Aufsteigend'
        '(35)Absteigend')
      TabOrder = 1
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 231
    Width = 245
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 5
      Top = 2
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 85
      Top = 2
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 165
      Top = 2
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 96
    Top = 34
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Dialog schliessen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 1
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfe aufrufen...'
      ImageIndex = 2
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 126
    Top = 34
    TargetsData = (
      1
      6
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0)
      (
        ''
        'Items'
        0)
      (
        ''
        'Text'
        0)
      (
        ''
        'Tabs'
        0)
      (
        ''
        'Lines'
        0))
  end
end
