(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMUGlobal.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt Definitionen von MIllMaster Spulerei
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date      Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.10.98  1.00  Mg | Datei erstellt
| 29.09.99  1.01  Nue| Types in conjunction with YMSettings added
| 16.12.99  1.02  Mg | YarnUnits added
| 28.12.99  1.03  Mg | cYarnCntMaskStr inseted
| 20.01.00  1.04  Nue| Filled in several constants in conjunction with DB Accesss
| 25.01.00  1.05  Wss| cMMProdGroupColors array added for Production Group Colors
| 26.01.00  1.06  Nue| temporary Stringconstant till MaschGrp is implemented cZERange
| 03.05.00  1.07  Nue| Cleaned up with constants in conjunction Default-,Dummy- and
|                    | System-(Style,Order,Color..)
| 21.06.00  1.08  Nue| cDefaultLengthMode value changed from 6=First to 7=Last
| 27.06.00  1.09  SDo| New const array added. cLenBaseTxt (TLenBaseRec)
| 27.06.00  1.10  Nue| New const cDBPhysicalName = 'MM_Winding' added.
| 27.07.00  1.11  Mg | TDBDataRec : Cluster events from integer to single
| 17.04.01  1.12  Wss| CalcValue copied from QOfflimit units for global access
| 16.05.01  1.13  Wss| cUndefinedSlip added
| 05.07.01  1.14  Wss| CalcValue, CalcValueUnit adapted to new aCalcMode (with SFI, Sp)
| 18.09.01  1.15  Wss| AdjustBase added in record TDBDataRec
| 25.09.01  1.16 WssNue| cNoYMNamePrefix constant added
| 26.09.01  1.17  Wss| GetDotColorValue added for quality matrix visibility
| 04.10.01  1.18  Wss| CalcSFI, CalcSFI_D functions added
| 31.10.01  1.19  Wss| YarnCountConvert function made changed to prototype def for  global use
| 26.11.01  1.20  Wss| in TDBDataRec data type for bob, cones changed to single
| 26.01.02  1.21  Nue| Release 2.03.01 erstellt. cX1 added.
| 08.02.02  1.22  Wss| cQMH2WFactor erstellt, um das Groessenverhaeltnis der QualityMatrix zu automatisieren
| 28.02.02  1.22  Wss| Funktion ConvertYarnCountFloatToInt hinzugefuegt
| 05.03.02  1.22  Wss| YarnCountConvert vervollstaendigt. Wenn Umrechung nicht direkt gemacht werden kann,
                       so wird der Umweg ueber Nm gemacht. Seit YarnCount ein Single Typ ist, gibt es
                       kein Verlust bei der Umrechung
| 06.06.02  1.23  Nue| In type TTimeMode tmLongtermWeek added.
| 11.03.03  1.23  Wss| Klasse TQueryParameter von LabMasterDef hierher verschoben
| 15.07.03  1.24  Nue| cYarnCountDelta added (moved from Style u_lib).
| 01.12.03        Lok| Funktion 'CalcAllValues()' eingef�gt, um die Normierten Werte f�r die
|                    |   DataItems ohne Fallunterscheidung berechnet werden k�nnen.
| 10.12.03  1.25  Wss| CalcValue: -1 = R�ckgabewert ohne Berechnung
| 27.09.04  1.26  Nue| Anpassungen an Version 5.0
|                    | YarnCount Berechnungen nicht mehr mit cYarnCntFactor verrechnen.
| 11.01.05        Nue| TSetOfSensingHeadClasses zugef�hrt.
| 03.02.05        Wss| - SetTimeMode: bei �nderung werden die TimeFrom/To/IDs zur�ck gesetzt
                       - UsedInFloor wird vorl�ufig nicht mehr ber�cksichtigt beim Query bilden
| 14.02.05        Nue| Konstante cXMLDefaultSetingsID zugef�hrt.
| 18.02.05        Wss| Funktion SaveMaConfigToDB() hinzugef�gt
| 25.02.05        Wss| Funktion XMLName2GUI hinzugef�gt
| 26.04.05        SDo| ID-Anpassung in cLengthMode-Array fuer XML; neu ID= 1..3 alt ID=6..8
| 26.05.05  1.27  Nue| Anpassen der Datenstrukturen an SpectraPlus und Zenit
| 08.06.05  1.28  khp| Anpassungen im TMMDataRec: COffCnt, COffCntPlus, COffCntMinus und CShortOffCnt,CShortOffCntPlus,CShortOffCntMinus
| 13.09.05        Wss| YarnCountConvert auf Double ge�ndert, aFact Parameter entfernt
| 21.10.05        Wss| CalcValue und CalcAllValue f�r Nutzeffektberechnung noch mit case -4 erg�nzt und im
                       CalcAllValue Parameter mit aSampleCount erweitert (wird aus Berechnungsmethoden aufgerufen)
                       -> Problematik war die AVG Berechnungsmethoden f�r den PEff im LabReport
| 11.04.06        Wss| ConvertYarnCountFloatToInt() entfernt
| 11.10.06  1.29  Nue| Anpassen von cMaxSpindeln von 80 auf 100
| 30.01.08  1.30  Nue| Anpassen f�r V5.04: cVCVFactor = 1.16 eingef�gt (F�r Berechnung CV/d)
| 06.02.08        Nue| Anpassen f�r V5.04: VCV-Daten in TMMDataRec eingef�gt.
|=========================================================================================*)

//**************************************************3
// Last change: 30.01.2008/Nue (V5.04.00)
//**************************************************3

unit MMUGlobal;
{$MINENUMSIZE 2}

interface

uses
  Classes, YMParaDef, mmColorButton {for TColorArray type}, Graphics, LoepfeGlobal,
  XMLDef, AdoDBAccess;

//:-----------------------------------------------------------------------------
//: Abschnitt f�r diverse Konstanten und Typen in Zusammenhang mit XML
//:-----------------------------------------------------------------------------
const
  cZEDefaultSpectraReinVer     = 36;
  cZEDefaultSpectraPlusReinVer = 37;

  cXMLDefaultSetingsID = -13; //14.2.05: Z.Zeit ist auf der DB ID=-13 das MemoryC

  // �bergeordnetes Element der Defaults im MaConfig XML
  cXMLDefaultsElementName = 'Defaults';

type
  // used for visualizing all sensingheadclasses available at customer (Nue:11.1.05)
  TSensingHeadClassSet = set of TSensingHeadClass;

//:-----------------------------------------------------------------------------


//:-----------------------------------------------------------------------------
//: Alle anderen Definitionen
//:-----------------------------------------------------------------------------
const

//  cMaxSpindeln          = 80;
  cMaxSpindeln          = 100; //ab 11.10.06 Nue
//  cMaxPartie            = 6;  not used??
  cMaxClassFields       = 128;
  cMaxSiroFields        = 64;
  cMinSpuledLen         = 2000; // minimal length of meters to say : the spindle has produced
  cMaxNumberOfIntervals = 200;
  cMaxSlip              = 1.5;
  cMinSlip              = 0.5;
  cUndefinedSlip        = 0;
  cDBPhysicalName       = 'MM_Winding';

// Constants for colon factors on databbase
  cSlipFactor           = 1000;
  cImpFactFactor        = 10;
//  cYarnCntFactor        = 10;
  cBobbinsFactor        = 100;
  cConesFactor          = 100;

  cCheckLenFactor       = 10;

// class fields
  cClassCutFields       = 128;
  cClassUncutFields     = cClassCutFields;
  cSpCutFields          = 128;
  cSpUncutFields        = cSpCutFields;
  cSIROCutFields        = 128; //64;
  cSIROUncutFields      = cSIROCutFields;

// Default IDs for WindingMaster in conjunction with DB
  cDefaultOrderPositionID = 1;
//  cSystemOrderPositionID  = 2;
//  cSystemOrderPositionName= 'SystemOrderPosition';
  cDefaultOrderPositionName = 'DefaultOrderPosition';
//  cDummyStyleID           = 1;
  cDefaultOrderID = 1;
//  cDummyStyleIDColor      = 8454143;  //RGB brightyellow
  cDefaultStyleID = 1;
  cDefaultStyleIDColor = 8454143; //RGB brightyellow
//  cSystemStyleID          = 2;
//  cSystemStyleIDColor     = clWhite; //RGB white
  cVirtualStyleColor = clWhite; //RGB white
//  cSystemStyleName        = 'DefaultStyle';
//  cSystemStartName        = 'DefaultStart';
  cDefaultStyleName = 'DefaultStyle';
  cDefaultStartName = 'DefaultStart';
  cVirtualStyleName = 'NotInProduction';
  cDummyYMSetID = 1; // Used to call mYMParaDBAccess.Get when new ProdGrp will be started from ZE (in SetSettings)
                                //  This value has to correspond with the c_YM_set_id in cQryInsDummyProdGrp!
  cDefaultM_to_prod      = 10000000.0;
  cDefaultYarn_cnt       = 600; //is 60
  c_DefaultNr_of_threads = 1;
  cDefaultSlip           = 1000; // is 1.000
  cDefaultSpeed          = 1200;
  cDefaultSpeedRamp      = 4; //4s
//NUE1  cDefaultLengthMode     = 7; //Last
  cDefaultLengthMode     = ord(lwmLast); //2; //Last
  cDefaultLengthWindow   = 100; //100km
  cAutoNameConst         = 'AutoName-'; // used in YMSettingsControl to handle filtering with autonames
  cNoYMNamePrefix        = cAutoNameConst + '%d'; //Nue:17.9.01
  cX1                    = '-X'; //Nue:31.1.02 Additional to changed YM_Set_Name
  cYMSetNameSize         = 50;

  cZERange               = 'ZERange'; //temporary Stringconstant till MaschGrp is implemented 26.01.00 Nue

  // Div. Constants for LZE and WSC (not supported for ZE) Nue
  cVCVFactor = 1.16;       //F�r Berechnung CV/d = cVCVFactor * SFI/D

  // Yarn Units
  cMaxArtPers = 20;
  // Garnnummer Umrechnungsfaktoren von .. nach..
  // z.B: Nm=1.69336*Ne
  cNmNe: Double = 1.69336;
  cNmTex = 1000.0;
//  cNmTex = 100000.0; // x10 because on database factor of 10    1000.0;
  //cTexNm = 1000.0;  cTexNe = 0.00169336;
  cNmNeL: Double = 0.604772;
  cNmNeK: Double = 1.12891;
  cNmNeW: Double = 0.516072;
  cNmTd: Double = 9000.0;
  cNmTs: Double = 29.0291;
//  cNmTd:  Double  = 900000.0; // x10 because on database factor of 10    9000.0;
//  cNmTs:  Double  = 2902.91; // x10 because on database factor of 10    29.0291;
  cNmNcC: Double = 1.7671;
  cNeNm: Double = 0.590541;
  cNeTex: Double = 590.541;
  cNeNeL: Double = 0.357143;
  cNeNeK: Double = 0.666667;
  cNeNeW: Double = 0.304762;
  cNeTd: Double = 5314.87;
  cNeTs: Double = 17.1429;
  cNeNcC: Double = 1.04347;

  //Korrekturwert f�r Filterfunktion der Garnfeinheit (Nue:15.07.03)
  cYarnCountDelta = 0.5;   //F�r Angabe Garnnummerbereich (x +/- 0.5)

  cKmToMile: Double = 0.62136995;
//  cMToYd: Double = 3937.0 / 3600.0; // 1.30 ca.1.094
  cMToYd = 3937.0 / 3600.0; // 1.30 ca.1.094
  cYdToM: Double = 3600.0 / 3937.0;
  cKgToLb: Double = 2.20462262185;

 {
  // Garnnummereinheiten
  cGarnNr1 = 'Nm ';   // Nummer Metrisch
  cGarnNr2 = 'NeC';   // English Cotton Count (Deutsch:NeB)
  cGarnNr3 = 'tex';   // Fineness Tex-System
  cGarnNr4 = 'Td ';   // Internat. denier count
  cGarnNr5 = 'NeL';   // English linen count
  cGarnNr6 = 'NeW';   // English worsted count (Deutsch:NeK)
  cGarnNr7 = 'Ny ';   // English woolen count yorkshire (Deutsch:NeW)
  cGarnNr8 = 'Ts ';   // Scottish count
  cGarnNr9 = 'NcC';   // Cotton Catalonian
  cGarnNrAnz = 9;
  }
  // Laengeneinheiten
  cLength1 = ' m ';
  cLength2 = 'km ';
  cLength3 = 'yd '; // 1.31 "->yd
  cLengthAnz = 3;

  // Gewichteinheiten
  cWeight1 = ' g ';
  cWeight2 = 'kg ';
  cWeight3 = ' t ';
  cWeight4 = 'lb ';
  cWeightAnz = 4;

  // Bestelleinheiten
  cOrder1 = 'kg      ';
  cOrder2 = 'Cones   ';
  cOrder3 = ' t      ';
  cOrder4 = 'lb      ';
  cOrderAnz = 4;

  // COM Server f�r den Matrix Editor
  cClassDesignerServer = 'LOEPFE.ClassDesignerServer';

type
  // used for quality matrix to get dot or color value depending on min, max value range -> GetDotColorValue()
  TDotColorValue          = (dcvDot, dcvColor);
  // used for DBSelectNavigator and DataLayers to define which data base are requested
  TTimeMode               = (tmInterval, tmShift, tmLongtermWeek);
// The following types are used to define a data watch level, len base ec.
// ATTENTION!! The following types are also used in the ZE environment DON'T CHANGE ANYTHING
//  without discussed with Kr (Nue/Kr: 2.5.2000)
  TDataLevel              = (dlSpindle, dlProdGrp, dlMachine, dlOrderPosition, dlStyle, dlAssortment);
  TClassDataTyp           = (cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut);
  TClassDataSet           = set of TClassDataTyp;
  TClassViewTyp           = (cvOnlyCut, cvOnlyUncut, cvCutAndUncut, cvCutAndUncutAdded);
  TClearerSettingsWarning = (cswNone, cswNoSettings, cswManySettings, cswManyYarnCount, cswFilterNotSupported);

  TLenBaseRec = record
    id: TLenBase;
    Text: string;
    LenUnit: string;
  end;

  TLengthModeRec = record
    id: integer;
    Text: string;
  end;

const

  cLenBaseTxt: array[0..integer(High(TLenBase))] of TLenBaseRec = (
    (id: lbAbsolute; Text: '(12)Absolut'; //ivlm
                     LenUnit: 'abs.'),
    (id: lb100KM;    Text: '100 km'; //ivlm
                     LenUnit: 'km'),
    (id: lb1000KM;   Text: '1000 km'; //ivlm   // Default 3
                     LenUnit: 'km'),
    (id: lb100000Y;  Text: '100.000 yds'; //ivlm
                     LenUnit: 'yds'),
    (id: lb1000000Y; Text: '1.000.000 yds'; //ivlm
                     LenUnit: 'yds')
    );

    {
  cLengthMode: array[0..2] of TLengthModeRec = (
    (id: 6; Text: '(8)Erste'), //ivlm
    (id: 7; Text: '(8)Letzte'), //ivlm
    (id: 8; Text: '(8)Kone') //ivlm
    );
}
  //for MMConfiguration; need XMLDef
  cLengthMode: array[0..2] of TLengthModeRec = (
    (id: Integer( lwmFirst ); Text: '(8)Erste'), //ivlm
    (id: Integer( lwmLast ); Text: '(8)Letzte'), //ivlm
    (id: Integer( lwmCone ); Text: '(8)Kone') //ivlm
    );



type
  TYarnUnitStr = string[4];
  TWeightUnits = (wuNone, wug, wukg, wut, wulb);
  TWeightUnitStr = string[4];
  TLengthUnits = (luNone, lum, lukm, luyd);
  TLengthUnitStr = string[4];

const
  // Yarn Unit
  cYarnUnitsByLength: set of TYarnUnit = [yutex, yuTd, yuTs];
  cYarnUnitsByWeight: set of TYarnUnit = [yuNm, yuNeB, yuNeL, yuNeK, yuNeW, yuNcC];
  cYarnUnitsStr: array[yuNone..yuNcC] of TYarnUnitStr = ('None', 'Nm', 'NeC', 'tex', 'Td', 'NeL', 'NeW', 'Ny', 'Ts', 'NcN'); // International
  cYarnCntMaskStr: array[yuNone..yuNcC] of ShortString = ('', '000;1; ', '000;1; ', '000;1; ', '000;1; ', '000;1; ', '000;1; ', '000;1; ', '000;1; ', '000;1; ');

  // Class Fields
  cNumOfClassFields: array[TClassDataTyp] of integer =
    (cClassUncutFields, cClassCutFields, cSpUncutFields, cSpCutFields, cSIROUncutFields, cSIROCutFields);

  cH2WFactorQMatrix = 1.3;
  cH2WFactorFMatrix = 1.8;
//..............................................................................

type
// The following records are used to define the DBData Record
// The types are the same or larger as the types on database -> type check by writing on DB
//..............................................................................
  // Imperfections
  PImpRec = ^TImpRec;
  TImpRec = record
    Neps:   LongWord;
    Thick:  LongWord;
    Thin:   LongWord;
    Small:  LongWord; // max. 34 events per meter ( 80 Spd. 12Std. )
    I2_4:   LongWord;
    I4_8:   LongWord;
    I8_20:  LongWord;
    I20_70: LongWord;
  end;
//..............................................................................
  // Classfields
  PMMSpCutField      = ^TMMSpCutField;
  TMMSpCutField      = array[1..128] of LongWord;
  PMMSpUncutField    = ^TMMSpUncutField;
  TMMSpUncutField    = array[1..128] of LongWord;
  PMMClassCutField   = ^TMMClassCutField;
  TMMClassCutField   = array[1..128] of LongWord;
  PMMClassUncutField = ^TMMClassUncutField;
  TMMClassUncutField = array[1..128] of LongWord;
  PMMSIROCutField    = ^TMMSIROCutField;
  TMMSIROCutField    = array[1..128] of LongWord;  //1-64 for dark-, 65-128 for bright cuts
  PMMSIROUncutField  = ^TMMSIROUncutField;
  TMMSIROUncutField  = array[1..128] of LongWord;  //1-64 for dark-, 65-128 for bright uncuts
//..............................................................................
// Classfield record
  PClassFieldRec = ^TClassFieldRec;
  TClassFieldRec = record
    ClassCutField: TMMClassCutField;
    ClassUncutField: TMMClassUncutField;
    SpCutField: TMMSpCutField;
    SpUncutField: TMMSpUncutField;
    SIROCutField: TMMSIROCutField;
    SIROUncutField: TMMSIROUncutField;
  end;
//..............................................................................
  PMMDataRec = ^TMMDataRec;
  TMMDataRec = record
    // von den NetHandlers ist in "Len" die Anzahl DrumPulse drin. Im StorageHandler werden diese Impulse
    // dann noch nach Meter umgerechnet und mit Maschine/Artikelschlupf korrigiert
    Len: LongWord;
    Wei: LongWord;
    Bob: Single;
    Cones: Single;
    Sp: LongInt;
    RSp: LongInt;
    YB: LongInt;
    CSys: LongInt;
    LckSys: LongInt;
    CUpY: LongInt;
    CYTot: LongInt;
    LSt: LongInt;
    tLStProd: LongInt;
    tLStBreak: LongInt;
    tLStMat: LongInt;
    tLStRev: LongInt;
    tLStClean: LongInt;
    tLStDef: LongInt;
    Red: LongInt;
    tRed: LongInt;
    tYell: LongInt;
    ManSt: LongInt;
    tManSt: LongInt;
    tRun: LongInt;
    tOp: LongInt;
    tWa: LongInt;
    totProdGrp: LongInt;
    twtProdGrp: LongInt;
    CS: LongInt;
    CL: LongInt;
    CT: LongInt;
    CN: LongInt;
    CSp: LongInt;
    CClS: LongInt;     //ab SpectraPlus; former ab Spectra CCl
    COffCnt: LongInt;  // Summe von +/-
    LckOffCnt: LongInt;   //Short offcnt alarms; former LckOffCnt
    CBu: LongInt;
    CDBu: LongInt;
    UClS: Single;
    UClL: Single;
    UClT: Single;
    CSIRO: LongInt;
    LckSIRO: LongInt;
    CSIROCl: LongInt;
    LckSIROCl: LongInt;
    LckClS: LongInt;    //ab SpectraPlus; former ab Spectra LckCl
    CSFI: LongInt;
    LckSFI: LongInt;
    SFICnt: LongInt;
    SFI: LongInt;
    AdjustBase: Single; // wss
    AdjustBaseCnt: SmallInt; // wss: needed to calculate average of AdjustBase from IntervalData to ShiftData

    CVCV: LongInt;           //Nue:6.02.08 new since MM5.04 (Zenit (not available on SPECTRA!)
    LckVCV: LongInt;         //Nue:6.02.08 new since MM5.04 (Zenit (not available on SPECTRA!)

    CP: LongInt;    //ab Zenit: synFFCuts, P cuts
    CClL: LongInt;  //ab SpectraPlus; Long Cluster cuts
    CClT: LongInt;  //ab SpectraPlus; Thin Cluster cuts
    LckP: LongInt;  //ab Zenit; synFFLocks, P locks
    LckClL: LongInt;  //ab SpectraPlus;  Long Cluster locks
    LckClT: LongInt;  //ab SpectraPlus; Thin Cluster locks
    LckShortOffCnt: LongInt;   //Short offcnt alarms; former LckOffCnt
    LckNSLT: LongInt;  //ab Zenit; former Yarn Fault locks 
    COffCntPlus: LongInt;   // ab Zenit Summe wird im Feld COffCnt abgelegt
    COffCntMinus: LongInt;  //ab Zenit; number cuts because of wrong yarncount (plus); former without plus (minus und plus werden z.Z.30.5.05 addiert in einem Feld angezeigt)
    CShortOffCnt: LongInt;  // Summe +/-
    CShortOffCntPlus: LongInt;    //Short offcntcuts alarms; former COffCnt
    CShortOffCntMinus: LongInt;  //ab Zenit; short offcntcuts minus (minus und plus werden z.Z.30.5.05 addiert in einem Feld angezeigt)
    CDrumWrap: LongInt;  //ab Zenit; Trommelwickel, z.Z. 30.5.05 nicht an Oberfl�che*/
    ClassFieldRec: TClassFieldRec;
    Imp: TImpRec;
  end;
// end of MMData Record

//..............................................................................
const
  cOneGrpDataHeaderSize = 4;
type
// record to send the spindle data to the application
  POneGrpDataRec = ^TOneGrpDataRec;
  TOneGrpDataRec = record
    SpindleFirst: Word;
    SpindleLast: Word;
    BadData: boolean;
    Data: array[0..0] of TMMDataRec;
  end;

const
  cMMProdGroupColors: TColorArray = (
    ($00FF0000, $00FF8080, $00F0CAA6, $00FFD3A8),
    ($00404080, $001500AA, $002734D8, $008000FF),
    ($00808000, $00C08000, $00ACB85A, $006ABB44),
    ($00008080, $003A8AC5, $0030CFCF, $00B7CAC1),
    ($00C0DCC0, $004080FF, $00FF80FF, $0080FF00));

//------------------------------------------------------------------------------
type
  TQueryParameters = class (TPersistent)
  private
    fLenBase: TLenBase;
    fMachIDs: string;
    fMachNames: string;
    fOnParamChange: TNotifyEvent;
    fProdIDs: string;
    fProdNames: string;
    fShiftCalID: Integer;
    fSpindleIDs: string;
    fStyleIDs: string;
    fStyleNames: string;
    fTimeFrom: TDateTime;
    fTimeIDs: string;
    fTimeMode: TTimeMode;
    fTimeTo: TDateTime;
    fUsedInFloor: Boolean;
    fYarnUnit: TYarnUnit;
    fYMSetIDs: string;
    fYMSetNames: string;
    procedure Clear;
    procedure ParamChanged;
    procedure SetLenBase(const Value: TLenBase);
    procedure SetMachIDs(const Value: string);
    procedure SetMachNames(const Value: string);
    procedure SetProdIDs(const Value: string);
    procedure SetProdNames(const Value: string);
    procedure SetShiftCalID(Value: Integer);
    procedure SetSpindleIDs(const Value: string);
    procedure SetStyleIDs(const Value: string);
    procedure SetStyleNames(const Value: string);
    procedure SetTimeFrom(const Value: TDateTime);
    procedure SetTimeIDs(const Value: string);
    procedure SetTimeMode(const Value: TTimeMode);
    procedure SetTimeTo(const Value: TDateTime);
    procedure SetUsedInFloor(const Value: Boolean);
    procedure SetYarnUnit(const Value: TYarnUnit);
    procedure SetYMSetIDs(const Value: string);
    procedure SetYMSetNames(const Value: string);
  public
    constructor Create;
    procedure Assign(Source: TPersistent); override;
    property LenBase: TLenBase read fLenBase write SetLenBase;
    property MachIDs: string read fMachIDs write SetMachIDs;
    property MachNames: string read fMachNames write SetMachNames;
    property OnParamChange: TNotifyEvent read fOnParamChange write fOnParamChange;
    property ProdIDs: string read fProdIDs write SetProdIDs;
    property ProdNames: string read fProdNames write SetProdNames;
    property ShiftCalID: Integer read fShiftCalID write SetShiftCalID;
    property SpindleIDs: string read fSpindleIDs write SetSpindleIDs;
    property StyleIDs: string read fStyleIDs write SetStyleIDs;
    property StyleNames: string read fStyleNames write SetStyleNames;
    property TimeFrom: TDateTime read fTimeFrom write SetTimeFrom;
    property TimeIDs: string read fTimeIDs write SetTimeIDs;
    property TimeMode: TTimeMode read fTimeMode write SetTimeMode;
    property TimeTo: TDateTime read fTimeTo write SetTimeTo;
    property UsedInFloor: Boolean read fUsedInFloor write SetUsedInFloor;
    property YarnUnit: TYarnUnit read fYarnUnit write SetYarnUnit;
    property YMSetIDs: string read fYMSetIDs write SetYMSetIDs;
    property YMSetNames: string read fYMSetNames write SetYMSetNames;
  end;
//------------------------------------------------------------------------------
// Functions for Yarn Units
//function CalcValue(aLenBase: TLenBase; aValue: Extended; aLen: Extended): Extended; overload;
function CalcAllValues(aLenBase: TLenBase; aValue, aLen: Extended; aCalcMode:
    Integer; aSFICnt, aAdjustBase, aAdjustBaseCnt: Single; aSampleCount:
    Integer): Single;
function CalcSFI_D(aSFI, aSFICnt: Single): Single;
function CalcSFI(aSFI, aSFICnt, aAdjustBase: Single): Single;
function CalcValue(aLenBase: TLenBase; aValue: Extended; aLen: Extended; aCalcMode: Integer = 0): Single;
function CalcValueUnit(aLenBase: TLenBase; aCalcMode: Integer): string;

// get number of dots or color value for quality matrix
function GetDotColorValue(aMode: TDotColorValue; aBase, aSteps: Integer; aLowValue, aHighValue, aValue: Single): Integer;
// aMode: defines to return a color value or number of dots
// aBase: in DotMode: max count of dots
//        in ColorMode: base color (0=red, 1=green, 2=blue)
// aSteps: number of steps
// aLowValue, aHighValue: range of value
// aValue: according to this value the result is calculated
function GetWarningMsg(aWarning: TClearerSettingsWarning): String;
function LengthToMeter(aLengthUnit: TLengthUnits; aLength: Single): Single;
function MeterToLength(aLengthUnit: TLengthUnits; aLength: Single): Single;
function MeterToWeightUnit(aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnit; aYarnCnt: Single; aLen: Single): Single;
function SaveMaConfigToDB(aMachID, aLastSpindle: Integer; aXMLData: String; aDB: TAdoDBAccess = Nil): Boolean;
function WeightUnitToMeter(aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnit; aYarnCnt: Single; aWeigth: Single): Single;
function YarnCountConvert(aInTyp, aOutTyp: TYarnUnit; aValIn: Double): Double;



implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  SysUtils, Windows, TypInfo, mmcs, XMLGlobal;
  
resourcestring
  cYMSettingsWarning1 = '(*)Es sind keine Reiniger Einstellungen vorhanden!';    // ivlm
  cYMSettingsWarning2 = '(*)Daten basieren auf mehreren Reinigereinstellungen!';  // ivlm
  cYMSettingsWarning3 = '(*)Daten basieren auf unterschiedlichen YarnCount!';  // ivlm
  cYMSettingsWarning4 = '(*)Aktuelle Filterinformationen werden nicht unterstuetzt!'; // ivlm

(*-----------------------------------------------------------
  Berechnet alle Arten von normierten Werten (Normal, SFI, SFI D)  
-------------------------------------------------------------*)
function CalcAllValues(aLenBase: TLenBase; aValue, aLen: Extended; aCalcMode:
    Integer; aSFICnt, aAdjustBase, aAdjustBaseCnt: Single; aSampleCount:
    Integer): Single;
begin
  case aCalcMode of
    -4: Result := aValue / aSampleCount;
    -3: if aAdjustBaseCnt <> 0 then
          result := CalcSFI(aValue, aSFICnt, (aAdjustBase / aAdjustBaseCnt))
        else
          result := 0;
    -2: result := CalcSFI_D(aValue, aSFICnt);
  else
    result := CalcValue(aLenBase, aValue, aLen, aCalcMode);
  end;// case aCalcMode of
end;// function CalcAllValues(aLenBase: TLenBase; aValue ...

//------------------------------------------------------------------------------
function CalcSFI_D(aSFI, aSFICnt: Single): Single;
begin
  if aSFICnt > 0.0 then
    Result := SQRT(aSFI / aSFICnt) / 1.45
  else
    Result := 0.0;
end;
//------------------------------------------------------------------------------
function CalcSFI(aSFI, aSFICnt, aAdjustBase: Single): Single;
begin
  if aSFICnt > 0.0 then begin
    Result := ((SQRT(aSFI / aSFICnt) / 1.45) * aAdjustBase) / 1000.0;
  end
  else
    Result := 0.0;
end;
//------------------------------------------------------------------------------
function CalcValue(aLenBase: TLenBase; aValue: Extended; aLen: Extended; aCalcMode: Integer): Single;
begin
  // etwas genauer definieren, welche L�nge mindestens n�tig ist, damit Berechnung gemacht wird
  if aLen > 0.01 then begin
    case aCalcMode of
      // normal len based value
      0: case aLenBase of
           lb100KM: Result := (aValue * 100000) / aLen;
           lb1000KM: Result := (aValue * 1000000) / aLen;
           lb100000Y: Result := (aValue * 100000) / (aLen * cMToYd);
           lb1000000Y: Result := (aValue * 1000000) / (aLen * cMToYd);
         else // lbAbsolute
           Result := aValue;
         end;
      -4,                      // Nutzeffekt
      -1: Result := aValue;    // R�ckgabewert OHNE Berechnung
    else
      //Imperfection or at fixed meter count
      Result := (aValue * aCalcMode) / aLen;
    end;
  end else
    Result := 0;
end;
//------------------------------------------------------------------------------
function CalcValueUnit(aLenBase: TLenBase; aCalcMode: Integer): string;
begin
  case aCalcMode of
    0: Result := cLenBaseTxt[Ord(aLenBase)].Text;
//    -2: Result := 'SFI/D';
    -3: Result := 'SFI';
  else
    //Imperfection: fixed length base
    Result := Format('%d m', [aCalcMode]);
  end;
end;
//------------------------------------------------------------------------------
// Unit Check Routinen ----------------------------------------------------
function YarnCountConvert(aInTyp, aOutTyp: TYarnUnit; aValIn: Double): Double;
const
  cExceptionStr = 'YarnUnit out of range. OutUnit = %s.';
begin
//  Result := aValIn;
  try
    aValIn := aValIn;
    case aInTyp of
      // Metric count
      yuNm:
        case aOutTyp of
          yuNm: Result := aValIn;
          yuNeB: Result := aValIn / cNmNe;
          yutex: Result := cNmTex / aValIn;
          yuTd: Result := cNmTd / aValIn;
          yuNeL: Result := aValIn / cNmNeL;
          yuNeK: Result := aValIn / cNmNeK;
          yuNeW: Result := aValIn / cNmNeW;
          yuTs: Result := cNmTs / aValIn;
          yuNcC: Result := aValIn / cNmNcC;
        else
          raise Exception.CreateFmt(cExceptionStr, [GetEnumName(TypeInfo(TYarnUnit), Ord(aOutTyp))])
        end;
      // English cotton count
      yuNeB:
        case aOutTyp of
          yuNeB: Result := aValIn;
          yuNm: Result := aValIn * cNmNe;
          yuTd: Result := cNeTd / aValIn;
          yutex: Result := cNeTex / aValIn;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, aValIn * cNmNe);
        end;
      // Tex
      yutex:
        case aOutTyp of
          yutex: Result := aValIn;
          yuNm: Result := cNmTex / aValIn;
          yuNeB: Result := cNeTex / aValIn;
          yuTd: Result := 9 * aValIn;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, cNmTex / aValIn);
        end;
      // Denier
      yuTd:
        case aOutTyp of
          yuTd: Result := aValIn;
          yuNm: Result := cNmTd / aValIn;
          yuNeB: Result := cNeTd / aValIn;
          yuTex: Result := aValIn / 9;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, cNmTd / aValIn);
        end;
      yuNeL:
        case aOutTyp of
          yuNeL: Result := aValIn;
          yuNm: Result := aValIn * cNmNeL;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, aValIn * cNmNeL);
        end;
      yuNeK:
        case aOutTyp of
          yuNeK: Result := aValIn;
          yuNm: Result := aValIn * cNmNeK;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, aValIn * cNmNeK);
        end;
      yuNeW:
        case aOutTyp of
          yuNeW: Result := aValIn;
          yuNm: Result := aValIn * cNmNeW;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, aValIn * cNmNeW);
        end;
      yuTs:
        case aOutTyp of
          yuTs: Result := aValIn;
          yuNm: Result := cNmTs / aValIn;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, cNmTs / aValIn);
        end;
      yuNcC:
        case aOutTyp of
          yuNcC: Result := aValIn;
          yuNm: Result := aValIn * cNmNcC;
        else
          // Zuerst nach Nm konvertieren, dann nach gewuenschter Einheit
          Result := YarnCountConvert(yuNm, aOutTyp, aValIn * cNmNcC);
        end;
    else
      raise Exception.CreateFmt('YarnUnit out of range. InUnit = %s.', [GetEnumName(TypeInfo(TYarnUnit), Ord(aInTyp))])
    end;
  except
    on e: Exception do begin
      raise Exception.Create('YarnCountConvert failed. ' + e.Message);
    end;
  end;
  Result := Result;
end;
//------------------------------------------------------------------------------
function GetDotColorValue(aMode: TDotColorValue; aBase, aSteps: Integer; aLowValue, aHighValue, aValue: Single): Integer;
//aMode: defines to return a color value or number of dots
//aBase: in DotMode: max count of dots
//       in ColorMode: base color (0=red, 1=green, 2=blue)
//aSteps: number of steps
//aLowValue, aHighValue: range of value
//aValue: according to this value the result is calculated

var
  xOffset,
    xDotOffset: Double;
  xStep,
    xDotStep: Double;
  //.......................................................................
  function GetStepFactor(aValue: Double): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    if aValue > 0.0 then begin
      i := 0;
      repeat
        inc(i);
      until exp(xOffset + i * xStep) >= aValue;
      if i > aSteps then
        Result := aSteps
      else
        Result := i;
    end;
  end;
  //.......................................................................
  function GetColorValue(aFactor: Integer): LongInt;
  var
    xMiddle: Single;
    xC1, xC2, xC3: Byte;
  begin
    xMiddle := (aSteps + 1) / 2;
    if aFactor = 0 then begin // White:               no value
      xC1 := 255;
      xC2 := 255;
      xC3 := 255;
    end
    else if aFactor <= xMiddle then begin // Light -> BaseColor:  low value
      xC1 := 240 - Round((240 / Int(xMiddle)) * (aFactor - 1));
      xC2 := xC1;
      xC3 := 254;
    end
    else begin // BaseColor -> Dark:    middle value
      xC1 := 0;
      xC2 := 0;
      xC3 := Round((254 / Int(xMiddle)) * (aSteps - aFactor));
    end;
                                              // else 0,0,0 for Black: high value
    case aBase of
      0: Result := RGB(xC3, xC1, xC2); // red
      1: Result := RGB(xC1, xC3, xC2); // green
      2: Result := RGB(xC1, xC2, xC3); // blue
    else
      Result := RGB(255, 255, 255); // else White
    end;
  end;
  //.......................................................................
  function GetDotCount(aFactor: Integer): Integer;
  begin
    if aFactor > 0 then
      Result := Round(exp(xDotOffset + aFactor * xDotStep))
    else
      Result := 0;
  end;
  //.......................................................................
begin
  Result := clBlack;
  if (aLowValue > 0) and (aHighValue > 0) then begin
    xOffset := ln(aLowValue);
    xStep := (ln(aHighValue) - xOffset) / aSteps;
    if aMode = dcvDot then begin
      xDotOffset := ln(aBase / 100);
      xDotStep := (ln(aBase) - xDotOffset) / aSteps;
      Result := GetDotCount(GetStepFactor(aValue));
    end
    else
      Result := GetColorValue(GetStepFactor(aValue));
  end;
end;
//-------------------------------------------------------------------------
function GetWarningMsg(aWarning: TClearerSettingsWarning): String;
begin
  case aWarning of
    cswNoSettings:         Result := cYMSettingsWarning1;
    cswManySettings:       Result := cYMSettingsWarning2;
    cswManyYarnCount:      Result := cYMSettingsWarning3;
    cswFilterNotSupported: Result := cYMSettingsWarning4;
  else
    Result := '';
  end;
end;
//-------------------------------------------------------------------------
function MeterToWeightUnit(aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnit; aYarnCnt: Single; aLen: Single): Single;
begin
  // base calculation in gramm
  Result := aLen / YarnCountConvert(aYarnUnit, yuNm, aYarnCnt);
  case aWeightUnit of
    wukg: Result := Result / 1000.0;
    wut: Result := Result / 1000000.0;
    wulb: Result := Result * cKgToLb / 1000.0;
  else
    // do nothing because -> wug
  end;
{
  case aWeightUnit of
    wug:
      Result := aLen/YarnCountConvert( aYarnUnit, yuNm, aYarnCnt);
    wukg:
      Result := aLen/YarnCountConvert( aYarnUnit, yuNm, aYarnCnt)/1000.0;
  else
    Result := aLen/YarnCountConvert( aYarnUnit, yuNm, aYarnCnt)*cKgToLb/1000.0;
  end;
{}
end;
//-------------------------------------------------------------------------
//------------------------------------------------------------------------------
function SaveMaConfigToDB(aMachID, aLastSpindle: Integer; aXMLData: String; aDB: TAdoDBAccess = Nil): Boolean;
const
  cSelectLastProdStart = ' select *' +
                         ' from t_prodgroup' +
                         ' where c_machine_id = :MachID' +
                         ' and c_prod_start >= :LastSavedMaConfigDate';
//  cSelectLastProdStart = ' select max(c_prod_start) LastProdStart' +
//                         ' from t_prodgroup' +
//                         ' where c_machine_id = :MachID';

  cSelectMaConfig = ' SELECT mc.c_date, mc.c_magroup_config_id, mc.c_hashcode1, mc.c_hashcode2' +
                    ' FROM t_machine m, t_magroup_config mc' +
                    ' WHERE m.c_machine_id = :MachID' +
                    ' AND m.c_magroup_config_id = mc.c_magroup_config_id';

  cInsertMaConfig = 'INSERT t_magroup_config ' +
                    'VALUES (:c_magroup_config_id, :MachID, :Date, :Hashcode1, :Hashcode2, :XMLData)';

  cUpdateMaConfig = 'UPDATE t_magroup_config ' +
                    'SET c_date = :Date, c_hashcode1 = :Hashcode1, c_hashcode2 = :Hashcode2, c_xml_data = :XMLData ' +
                    'WHERE c_magroup_config_id = :MaConfigID';

  cUpdateMachine  = ' UPDATE t_machine' +
                    ' SET c_nr_of_spindles = :LastSpindle, c_magroup_config_id = :ConfigID' +
                    ' WHERE c_machine_id = :MachID';
var
  xMaConfigID: Integer;
  xHash1, xHash2: LongInt;
  xDB: TAdoDBAccess;
  xLastSavedMaConfig: TDateTime;
  xEqualHashcode: Boolean;
  //.............................................................
  procedure InsertMaConfig;
  var
    xNewID: Integer;
  begin
    with xDB.Query[0] do
    try
      // das aktuelle MaConfig XML ist unterschiedlich zum neuen -> neu einf�gen
      Close;
      SQL.Text := cInsertMaConfig;
      ParamByName('MachID').AsInteger     := aMachID;
      ParamByName('Date').AsDateTime      := Now;
      ParamByName('Hashcode1').AsDateTime := xHash1;
      ParamByName('Hashcode2').AsDateTime := xHash2;
      ParamByName('XMLData').AsString     := aXMLData;
      xNewID:= InsertSQL('t_magroup_config', 'c_magroup_config_id', MAXINT, 1);

      // nun noch den Link von der Maschinentabelle auf den neuen Eintrag machen
      SQL.Text := cUpdateMachine;
      ParamByName('MachID').AsInteger      := aMachID;
      ParamByName('LastSpindle').AsInteger := aLastSpindle;
      ParamByName('ConfigID').AsInteger    := xNewID;
      ExecSQL;
    except
      on e:Exception do begin

      end;
    end;
  end;
  //.............................................................
  procedure UpdateMaConfig;
  begin
    with xDB.Query[0] do
    try
      Close;
      SQL.Text := cUpdateMaConfig;
      ParamByName('MaConfigID').AsInteger := xMaConfigID;
      ParamByName('Date').AsDateTime      := Now;
      ParamByName('Hashcode1').AsDateTime := xHash1;
      ParamByName('Hashcode2').AsDateTime := xHash2;
      ParamByName('XMLData').AsString     := aXMLData;
      ExecSQL;
    except
      on e:Exception do begin

      end;
    end;
  end;
  //.............................................................
begin
  Result            := False;

  HashByMM(aXMLData, xHash1, xHash2);

  if Assigned(aDB) then
    xDB := aDB
  else begin
    xDB := TAdoDBAccess.Create(1);
    xDB.Init;
  end;

  xMaConfigID  := -1;
  try
    with xDB do
    try
      with Query[0] do begin
        Close;
        SQL.Text := cSelectMaConfig;
        Params.ParamByName('MachID').AsInteger := aMachID;
        Open;
        if FindFirst then begin
          // OK, ein MaConfig existiert bereits, hole die Daten wo es gespeichert wurde
          xMaConfigID        := FieldByName('c_magroup_config_id').AsInteger;
          xLastSavedMaConfig := FieldByName('c_date').AsDateTime;
          xEqualHashcode     := (FieldByName('c_hashcode1').AsInteger = xHash1) and (FieldByName('c_hashcode2').AsInteger = xHash2);

          // Nun noch pr�fen, ob die zuletzt gestartete Partie ein aktuelleres Datum hat als der Bauzustand
          Close;
          SQL.Text := cSelectLastProdStart;
          Params.ParamByName('MachID').AsInteger                 := aMachID;
          Params.ParamByName('LastSavedMaConfigDate').AsDateTime := xLastSavedMaConfig;
          Open;
          // Hat es neuere Partien seit dem letzten Save vom Bauzustand aber ist der Hashcode noch gleich?
          if FindFirst then begin
            // Wenn der Hashcode unterschiedlich ist, dann neues MaConfig speichern
            if not xEqualHashcode then
              InsertMaConfig
          end else
            // MaConfig ist neuer als gestartete Partien -> aktualisieren
            UpdateMaConfig;
        end else
          // Es wurde noch kein MaConfig f�r diese Maschine gespeicht -> neues erzeugen
          InsertMaConfig;
      end; // with Query[0]
      Result := True;
    except
      on e: Exception do
        raise Exception.Create('SaveMaConfigToDB failed: ' + e.Message);
    end; // with aDB
  finally
    // Wenn das DB Objekt extra erstellt wurde, dann auch wieder aufr�umen
    if not Assigned(aDB) then
      xDB.Free;
  end;

//  Result := False;
//  HashByMM(aXMLData, xHash1, xHash2);
//
//  xOwnDB := not Assigned(aDB);
//  if xOwnDB then begin
//    aDB := TAdoDBAccess.Create(1);
//    aDB.Init;
//  end;
//
//  xLastSavedMaConfig   := cDateTime1970;
//  xMaConfigID := -1;
//  try
//    with aDB do
//    try
//      with Query[0] do begin
//        // Wenn vorhanden, aktuelles Speicherdatum vom Bauzustand holen
//        Close;
//        SQL.Text := cSelectMaConfig;
//        Params.ParamByName('MachID').AsInteger := aMachID;
//        Open;
//        if FindFirst then begin
//          xDoInsertNewMaConfig := False;
//          // OK, ein MaConfig existiert bereits, hole die Zeit wo es gespeichert wurde
//          xLastSavedMaConfig := FieldByName('c_date').AsDateTime;
//          xMaConfigID        := FieldByName('c_magroup_config_id').AsInteger;
//
//          // Nun noch pr�fen, ob die zuletzt gestartete Partie ein aktuelleres Datum hat als der Bauzustand
//          Close;
//          SQL.Text := cSelectLastProdStart;
//          Params.ParamByName('MachID').AsInteger := aMachID;
//          Open;
//          if FindFirst then
//            // Wenn der Bauzustand �lter ist als das Startdatum einer Partie dann muss der Bauzustand gespeichert werden
////            xDoInsertNewMaConfig := (xLastSavedMaConfig > FieldByName('LastProdStart').AsDateTime);
//            xDoInsertNewMaConfig := (FieldByName('LastProdStart').AsDateTime > xLastSavedMaConfig);
//        end else
//          xDoInsertNewMaConfig := True;
//
//
//        // Seit dem letzten Speichern des Bauzustandes wurde mind. 1 Partie gestartet -> neuer Bauzustand speichern und verlinken
//        if xDoInsertNewMaConfig then begin
//          // das aktuelle MaConfig XML ist unterschiedlich zum neuen -> neu einf�gen
//          Close;
//          SQL.Text := cInsertMaConfig;
//          ParamByName('MachID').AsInteger     := aMachID;
//          ParamByName('Date').AsDateTime      := Now;
//          ParamByName('Hashcode1').AsDateTime := xHash1;
//          ParamByName('Hashcode2').AsDateTime := xHash2;
//          ParamByName('XMLData').AsString     := aXMLData;
//          xMaConfigID := InsertSQL('t_magroup_config', 'c_magroup_config_id', MAXINT, 1);
//
//          // nun noch den Link von der Maschinentabelle auf den neuen Eintrag machen
//          SQL.Text := cUpdateMachine;
//          ParamByName('MachID').AsInteger      := aMachID;
//          ParamByName('LastSpindle').AsInteger := aLastSpindle;
//          ParamByName('ConfigID').AsInteger    := xMaConfigID;
//          ExecSQL;
//        end
//        else begin
//          Close;
//          SQL.Text := cUpdateMaConfig;
//          ParamByName('MaConfigID').AsInteger := xMaConfigID;
//          ParamByName('Date').AsDateTime      := Now;
//          ParamByName('Hashcode1').AsDateTime := xHash1;
//          ParamByName('Hashcode2').AsDateTime := xHash2;
//          ParamByName('XMLData').AsString     := aXMLData;
//          ExecSQL;
//        end; // if xDoInsertNewMaConfig
//      end; // with Query[0]
//      Result := True;
//    except
//      on e: Exception do
//        raise Exception.Create('SaveMaConfigToDB failed: ' + e.Message);
//    end; // with aDB
//  finally
//    // Wenn das DB Objekt extra erstellt wurde, dann auch wieder aufr�umen
//    if xOwnDB then
//      aDB.Free;
//  end;
end;
//-------------------------------------------------------------------------
function WeightUnitToMeter(aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnit; aYarnCnt: Single; aWeigth: Single): Single;
begin
  // base calculation in
  Result := aWeigth * YarnCountConvert(aYarnUnit, yuNm, aYarnCnt);
  case aWeightUnit of
    wukg: Result := Result * 1000.0;
    wut: Result := Result * 1000000.0;
    wulb: Result := Result / cKgToLb * 1000.0;
  else
    // do nothing because -> wug
  end;
{
  case aWeightUnit of
    wug:
      Result := aWeigth*YarnCountConvert(aYarnUnit, yuNm, aYarnCnt);
  else
    Result := aWeigth*YarnCountConvert(aYarnUnit, yuNm, aYarnCnt)/cKgToLb*1000.0;
  end;
{}
end;
//-------------------------------------------------------------------------
function LengthToMeter(aLengthUnit: TLengthUnits; aLength: Single): Single;
begin
  case aLengthUnit of
    lukm: Result := aLength * 1000.0;
    luyd: Result := aLength / cMToYd;
  else
    Result := aLength;
  end;
end;
//-------------------------------------------------------------------------
function MeterToLength(aLengthUnit: TLengthUnits; aLength: Single): Single;
begin
  case aLengthUnit of
    lukm: Result := aLength / 1000.0;
    luyd: Result := aLength * cMToYd;
  else
    Result := aLength;
  end;
end;
//:-----------------------------------------------------------------------------
function XMLName2GUI(aString: String): string;
//var
//  xStr: ShortString;
begin
  Result := aString;
  if Length(aString) > 0 then
    while IsCharLower(Result[1]) do
      Delete(Result, 1, 1);

//  xStr := aString;
//  while IsCharLower(xStr[1]) do
//    Delete(xStr, 1, 1);
//
//  Result := xStr;
end;

//-------------------------------------------------------------------------
//:---------------------------------------------------------------------------
//:--- Class: TQueryParameters
//:---------------------------------------------------------------------------
constructor TQueryParameters.Create;
begin
  inherited Create;
  fLenBase       := lbAbsolute;
  fShiftCalID    := 1;
  fYarnUnit      := yuNone;
  fOnParamChange := Nil;
  fUsedInFloor   := False;
  
  Clear;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.Assign(Source: TPersistent);
begin
  if Source is TQueryParameters then begin
    with Source as TQueryParameters do begin
      Self.fMachIDs    := MachIDs;
      Self.fMachNames  := MachNames;
      Self.fProdIDs    := ProdIDs;
      Self.fProdNames  := ProdNames;
      Self.fShiftCalID := ShiftCalID;
      Self.fStyleIDs   := StyleIDs;
      Self.fStyleNames := StyleNames;
      Self.fTimeMode   := TimeMode;
      Self.fTimeFrom   := TimeFrom;
      Self.fTimeTo     := TimeTo;
      Self.fUsedInFloor := UsedInFloor;
      Self.fYMSetIDs   := YMSetIDs;
      // TimeIDs overrules the From/To properties
      if TimeIDs <> '' then
        Self.fTimeIDs := TimeIDs;
    end;
    ParamChanged;
  end else
    inherited Assign(Source);
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.Clear;
begin
  fTimeIDs     := '';
  fTimeFrom    := 0;
  fTimeTo      := 0;
  fYMSetIDs    := '';
  fMachIDs     := '';
  fMachNames   := '';
  fProdIDs     := '';
  fProdNames   := '';
  fStyleIDs    := '';
  fStyleNames  := '';
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.ParamChanged;
begin
  if Assigned(fOnParamChange) then
    fOnParamChange(Self);
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetLenBase(const Value: TLenBase);
begin
  if fLenBase <> Value then begin
    fLenBase := Value;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetMachIDs(const Value: string);
begin
  if fMachIDs <> Value then
  begin
    fMachIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetMachNames(const Value: string);
begin
  if fMachNames <> Value then
  begin
    fMachNames := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetProdIDs(const Value: string);
begin
  if fProdIDs <> Value then
  begin
    fProdIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetProdNames(const Value: string);
begin
  if fProdNames <> Value then
  begin
    fProdNames := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetShiftCalID(Value: Integer);
begin
  if fShiftCalID <> Value then
  begin
    fShiftCalID := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetSpindleIDs(const Value: string);
begin
  if fSpindleIDs <> Value then
  begin
    fSpindleIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetStyleIDs(const Value: string);
begin
  if fStyleIDs <> Value then
  begin
    fStyleIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetStyleNames(const Value: string);
begin
  if fStyleNames <> Value then
  begin
    fStyleNames := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetTimeFrom(const Value: TDateTime);
begin
  if fTimeFrom <> Value then
  begin
    fTimeFrom := Value;
    fTimeIDs  := '';
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetTimeIDs(const Value: string);
begin
  if fTimeIDs <> Value then
  begin
    fTimeIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetTimeMode(const Value: TTimeMode);
begin
  if fTimeMode <> Value then
  begin
    fTimeMode := Value;
    fTimeFrom := 0;
    fTimeTo   := 0;
    fTimeIDs  := '';
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetTimeTo(const Value: TDateTime);
begin
  if fTimeTo <> Value then
  begin
    fTimeTo := Value;
    fTimeIDs := '';
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetUsedInFloor(const Value: Boolean);
begin
  if fUsedInFloor <> Value then
  begin
    fUsedInFloor := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetYarnUnit(const Value: TYarnUnit);
begin
  if fYarnUnit <> Value then
  begin
    fYarnUnit := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetYMSetIDs(const Value: string);
begin
  if fYMSetIDs <> Value then
  begin
    fYMSetIDs := Value;
    ParamChanged;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQueryParameters.SetYMSetNames(const Value: string);
var
  i: Integer;
  xIDList: TStringList;
  xStr: string;
  xValue: string;
  xPos: Integer;
begin
  if fYMSetNames <> Value then begin
    xValue := Value;
    xIDList := TStringList.Create;
    with TStringList.Create do
    try
      CommaText := xValue;
      i := 0;
      while i < Count do begin
        xStr := Strings[i];
          // if this string is automatic created from system (Hostlink) retrieve the ID part
        xPos := Pos(cAutoNameConst, xStr);
        if  xPos <> 0 then begin
          System.Delete(xStr, xPos, Length(cAutoNameConst));
          xStr := StringReplace(xStr, '''', '', [rfReplaceAll]);
          xIDList.Add(xStr);
            // this ID name isn't valid -> remove from list
          Delete(i);
        end else begin
          Strings[i] := Format('%s', [xStr]);
          inc(i);
        end;
  {
          if Pos(cAutoNameConst, xStr) = 1 then begin
            System.Delete(xStr, 1, Length(cAutoNameConst));
            xIDList.Add(xStr);
            // this ID name isn't valid -> remove from list
            Delete(i);
          end else begin
            Strings[i] := Format('%s', [xStr]);
            inc(i);
          end;
  {}
      end;
        // save changed YMName list (without AutoName-%d) back to xValue
      xValue := CommaText;
        // override previous YM IDs yet. Can be extendet in further implementations
      YMSetIDs := xIDList.CommaText;
    finally
      Free;
      xIDList.Free;
    end;
    fYMSetNames := xValue;
    ParamChanged;
  end;
end;
//------------------------------------------------------------------------------
end.

