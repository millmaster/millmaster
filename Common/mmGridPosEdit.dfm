ÿ
 TMMGRIDPOSEDITOR 0C0  TPF0ñTmmGridPosEditormmGridPosEditorLeftÓTopcWidthãHeightActiveControllbAvailableBorderIconsbiSystemMenu Caption$(60)Tabelleneinstellungen bearbeitenConstraints.MaxWidthãConstraints.MinHeightConstraints.MinWidthã
ParentFont
Font.ColorclBlackPositionpoScreenCenterOnCreate
FormCreateOnShowFormShowPixelsPerInch`
TextHeight TmmPanel	MainPanelLeft Top WidthÛHeightWAlignalClient
BevelOuterbvNoneBorderWidthTabOrder  TmmPageControlPageControl1LeftTopWidthÑHeightM
ActivePagetsColsAlignalClientHotTrack	TabOrder  	TTabSheettsColsCaption(30)Anzeige TmmSpeedButtonbMoveTo_lbShowLeftÈ TopcWidthHeightActionacMoveTo_lbShowFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
      BM       v   (                                                    ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ       
ParentFontParentShowHintShowHint	Visible	AutoLabel.LabelPositionlpLeft  TmmSpeedButtonbMoveTo_lbAvailableLeftÈ Top|WidthHeightActionacMoveTo_lbAvailableFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameMS Sans Serif
Font.Style 
Glyph.Data
      BM       v   (                                                    ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ    
ParentFontParentShowHintShowHint	Visible	AutoLabel.LabelPositionlpLeft  TmmSpeedButtonbMoveAllTo_lbShowLeftÈ Top WidthHeightActionacMoveAllTo_lbShow
Glyph.Data
²   ®   BM®       v   (   
            8                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                             ParentShowHintShowHint	Visible	AutoLabel.LabelPositionlpLeft  TmmSpeedButtonbMoveAllTo_lbAvailableLeftÈ Top¸ WidthHeightActionacMoveAllTo_lbAvailable
Glyph.Data
²   ®   BM®       v   (   
            8                                     ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ                             ParentShowHintShowHint	Visible	AutoLabel.LabelPositionlpLeft  TmmSpeedButtonbMoveUpLeft¬Top WidthHeightHint$(*)Position des Eintrags verschieben
Glyph.Data
      BM       v   (                                                    ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ        ParentShowHintShowHint	Visible	OnClickMoveBtnClickAutoLabel.LabelPositionlpLeft  TmmSpeedButton	bMoveDownLeft¬Top WidthHeightHint$(*)Position des Eintrags verschieben
Glyph.Data
      BM       v   (                                                    ÀÀÀ   ÿ  ÿ   ÿÿ ÿ   ÿ ÿ ÿÿ  ÿÿÿ        ParentShowHintShowHint	Visible	OnClickMoveBtnClickAutoLabel.LabelPositionlpLeft  TmmGroupBoxgbAvailableColsLeftTopWidth¹ Height'AnchorsakLeftakTopakBottom Caption(25)Vorhandene SpaltenwerteFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder CaptionSpace	 
TNWListBoxlbAvailableLeft
TopWidth¥ HeightAnchorsakLeftakTopakBottom DragModedmAutomaticFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ItemHeightMultiSelect	
ParentFontSorted	TabOrder 
OnDblClickacMoveTo_lbShowExecute
OnDragDropSH_ColsDragDrop
OnDragOverlbAvailableDragOver   TmmGroupBox
gbShowColsLefté TopWidth¹ Height'AnchorsakLeftakTopakBottom Caption(25)In BerichtFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderCaptionSpace	 
TNWListBoxlbShowLeft
TopWidth¥ HeightAnchorsakLeftakTopakBottom DragModedmAutomaticFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ItemHeightMultiSelect	
ParentFontStylelbOwnerDrawFixedTabOrder 
OnDblClickacMoveTo_lbAvailableExecute
OnDragDropSH_ColsDragDrop
OnDragOverlbShowDragOver
OnDrawItemlbShowDrawItem    	TTabSheet	tsOptionsCaption(30)Schrift und Farben TmmGroupBox	gbSchriftLeftTopWidth¿Height<Caption(40)SchriftFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder CaptionSpace	 TmmPanelpaTitleColorLeft
TopWidthÒ HeightHint$(*)Mit Mausklick Farbauswahl oeffnen
BevelInnerbvRaised
BevelOuter	bvLoweredFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrder OnClickColorPanelClick TmmLabellaTitleFontLeft
TopWidth&HeightHint'(*)Mit Mausklick Schriftauswahl oeffnenCaption	(30)TitelFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontVisible	OnClickSchriftLabelClickAutoLabel.LabelPositionlpLeft   TmmPanelpaColorLeftã TopWidthÒ HeightHint$(*)Mit Mausklick Farbauswahl oeffnen
BevelInnerbvRaised
BevelOuter	bvLoweredFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	TabOrderOnClickColorPanelClick TmmLabellaFontLeft
TopWidth/HeightHint'(*)Mit Mausklick Schriftauswahl oeffnenCaption	(30)DatenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontParentShowHintShowHint	Visible	OnClickSchriftLabelClickAutoLabel.LabelPositionlpLeft    TmmGroupBoxgbGridSettingsLeftTopKWidth¿Height¥ Caption(40)AnzeigeFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderCaptionSpace	 TmmCheckBox
cbRowLinesLeft
TopWidthHeightCaption(80)Zeilenlinien anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder Visible	AutoLabel.LabelPositionlpLeft  TmmCheckBox
cbColLinesLeft
Top(WidthHeightCaption(80)Spaltenlinien anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft  TmmCheckBoxcbIndicatorLeft
Top<WidthHeightCaption(80)Spaltenzeiger anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft  TmmCheckBoxcbRowSelectLeft
TopPWidthHeightCaption!(80)Ganze Zeile markiert anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft  TmmCheckBoxcbAlwaysShowSelectionLeft
TopdWidthHeightCaption!(80)Markierung permanent anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft  TmmCheckBoxcbTitleAlignmentLeft
TopxWidthHeightCaption#(80)Spaltentitel zentriert anzeigenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft  TmmCheckBoxcbTrailingEllipsisLeft
Top WidthHeightCaption(80)Fortsetzungspunkte (...)Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderVisible	AutoLabel.LabelPositionlpLeft   TmmGroupBoxgbFixedColsLeftTopú Width¿Height2AnchorsakLeftakTopakBottom Caption(40)Fixierte SpaltenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderCaptionSpace	 TmmEditedFixedColsLeft
TopWidth(HeightColorclWindowReadOnly	TabOrder Text0Visible	AutoLabel.LabelPositionlpLeftReadOnlyColorclInfoBkShowModesmNormal  	TmmUpDownudFixedColsLeft2TopWidthHeight	AssociateedFixedColsMin Max
Position TabOrderWrap    	TTabSheettsAttributesCaption(30)Feldformate TmmGroupBox
gbFieldDefLeftTopWidth¿Height'AnchorsakLeftakTopakBottom Caption(40)Feld-DefinitionFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrder CaptionSpace	 TmmRadioGrouprgDateLeftTopdWidth¢ Height¥ Caption	(25)DatumFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickrgDateClick  TmmRadioGroup
rgDateTimeLeftTopdWidth¢ Height¥ Caption(25)Datum/ZeitFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ParentFontTabOrderOnClickrgDateTimeClick  TmmRadioGrouprgAlignmentLeftTop
Width¢ HeightKCaption(25)AusrichtungFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style Items.Strings	(15)links(15)zentriert
(15)rechts 
ParentFontTabOrder OnClickrgAlignmentClickCaptionSpace	  
TNWListBox
lbAttributLeft
TopWidthÿ HeightAnchorsakLeftakTopakBottom Font.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style 
ItemHeight
ParentFontTabOrderOnClicklbAttributClick  TmmRadioGroup	rgIntegerLeftTopdWidth¢ Height7Caption
(25)ZahlenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style Items.Strings	1.234.4881234488 
ParentFontTabOrderOnClickrgIntegerClick  TmmRadioGrouprgFloatLeftTopdWidth¢ Height¥ Caption
(25)ZahlenFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style Items.Strings	1.234.4811.234.481,01.234.481,001.234.481,0001234481	1234481,0
1234481,001234481,000 
ParentFontTabOrderOnClickrgFloatClickCaptionSpace	  TmmRadioGrouprgTimeLeftTopdWidth¢ Height7Caption(25)ZeitFont.CharsetDEFAULT_CHARSET
Font.ColorclBlackFont.Heightõ	Font.NameMS Sans Serif
Font.Style Items.Strings17:3317:33:51 
ParentFontTabOrderOnClickrgTimeClickCaptionSpace	      TmmPanel	paButtonsLeft TopWWidthÛHeightAlignalBottom
BevelOuterbvNoneTabOrder 	TmmButtonbOKLeftë TopWidthKHeightActionacOKDefault	ParentShowHintShowHint	TabOrder Visible	AutoLabel.LabelPositionlpLeft  	TmmButtonbCancelLeft;TopWidthKHeightActionacCancelParentShowHintShowHint	TabOrderVisible	AutoLabel.LabelPositionlpLeft  	TmmButtonbHelpLeftTopWidthKHeightActionacHelpParentShowHintShowHint	TabOrderVisible	AutoLabel.LabelPositionlpLeft   TmmFontDialogFontDialog1Font.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Heightó	Font.NameSystem
Font.Style MinFontSize MaxFontSize Left Top\  TmmColorDialogColorDialog1Ctl3D	Leftà Top]  TmmActionListActionList1Left0TopZ TActionacOKCaption(9)OKHint(*)Dialog schliessen
ImageIndex 	OnExecuteacOKExecute  TActionacCancelCaption(9)AbbrechenHint(*)Dialog abbrechen
ImageIndexShortCut	OnExecuteacCancelExecute  TActionacHelpCaption	(9)&HilfeHint(*)Hilfe aufrufen...
ImageIndex	OnExecuteacHelpExecute  TActionacMoveTo_lbShowHint(*)Feld zur Anzeige hinzufuegen	OnExecuteacMoveTo_lbShowExecute  TActionacMoveTo_lbAvailableHint(*)Feld aus Anzeige entfernen	OnExecuteacMoveTo_lbAvailableExecute  TActionacMoveAllTo_lbShowHint&(*)Alle Felder zur Anzeige hinzufuegen	OnExecuteacMoveAllTo_lbShowExecute  TActionacMoveAllTo_lbAvailableHint$(*)Alle Felder aus Anzeige entfernen	OnExecuteacMoveAllTo_lbAvailableExecute   TmmTranslatormmTranslator1DictionaryNameDictionary1OnAfterTranslatemmTranslator1AfterTranslateLeftxTopXTargetsData*Hint  *Caption  *Text  *Items  *Lines      