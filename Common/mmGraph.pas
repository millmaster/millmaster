(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: mmGraph.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Grafik-Komponente
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 01.02.99 | 0.01 | PW  | Datei erstellt
| 02.03.99 | 0.02 | PW  | Aenderungen lt. Besprechung am 26.2.99
| 25.03.99 | 0.03 | PW  | Aenderungen lt. Besprechung am 17.3.99
| 21.04.99 | 0.04 | PW  | BeginUpdate, EndUpdate to prevent multiple "refreshes"
| 07.05.99 | 0.05 | PW  | YScaling-Factor for each series
| 23.07.99 | 0.06 | PW  | Correcting VCL TScrollBar.Pagesize-Error
| 25.09.99 | 0.07 | PW  | TMemoINI to read/write settings from/to DB
| 01.10.99 | 0.08 | PW  | Rename EditOptions to EditOptionsDlg
| 28.10.99 | 0.09 | Wss | CalcScrollBar had Range Check Error if value = 0
| 31.01.00 | 0.10 | Mg  | if in TmmGraphData.Stat_CV inserted
| 24.03.00 | 0.11 | SDo | New property OnGraphClick in TmmGraph
| 27.03.00 | 0.12 | SDo | New property Button and Shift in TmmSelection -> BarSelect
|                       | Anpassungen in FPaintBoxMouseUp
| 22.11.01 | 0.09 | Wss | MemoryLeak: FChartArea, FLines were not cleaned up in Destroy
| 10.06.04 | 0.13 | SDo | New properties PageHeader & PageFooter in TMMGraph, Func. PrintHeaderFooter()
|=============================================================================*)

{ $define UsePopup}
{ $define UseALT_LMB}
{ $define Debug}

unit mmGraph;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,Buttons,Menus,Math,Printers,mmGraphGlobal,mmGraphEditSerie,mmGraphSortSerie,
 mmGraphOption,MemoINI,IvDictio,IvMulti,IvEMulti,
 mmSpeedButton,mmScrollBar,mmPaintBox;

type
 TmmGraphData = class(TPersistent)
  private
   FControl: TControl;
   FNumSeries: integer;
   FSortOrder: eSortOrderT;
   FSortRow: integer;
   FXMax: double;
   FXMin: double;
   procedure SetNumSeries(value: integer);
  protected
   Data: tDataRecordT;
   property Control: TControl read FControl;
   function GetXDataPos(aValue: ValueType): longint;
   function Stat_SUM(const aData: array of ValueType): ValueType;
   function Stat_COUNT(const aData: array of ValueType): longint;
   function Stat_MEAN(const aData: array of ValueType): ValueType;
   function Stat_MIN(const aData: array of ValueType): ValueType;
   function Stat_MAX(const aData: array of ValueType): ValueType;
   function Stat_STDDEV(const aData: array of ValueType): ValueType;
   function Stat_SUMOFSQUARES(const aData: array of ValueType): ValueType;
   function Stat_TOTALVARIANCE(const aData: array of ValueType): ValueType;
   function Stat_VARIANCE(const aData: array of ValueType): ValueType;
   function Stat_CV(aMean,aStdDev: ValueType): ValueType;
   function Stat_TVALUE(aCount: longint; aTNiveau: byte): ValueType;
   function Stat_QVALUE(aCount: longint; aStdDev: ValueType; aTNiveau: byte): ValueType;
   procedure CalcXMinMax;
  public
   constructor Create(Control: TControl); virtual;
   destructor Destroy; override;
   procedure AddToSUM(aRow: integer);
   procedure AddXValue(aXValue: ValueType; aXLabel: XLabelType);
   procedure AddYValue(aRow: byte; aXValue,aYValue: ValueType; aXLabel: XLabelType);
   procedure CalcStatData;
   procedure ClearData;
   procedure ClearRow(aRow: byte);
   procedure ClearPoint(aRow: byte; aXValue: ValueType);
   function FindPoint(aXValue: ValueType; aFindType: eFindTypeT): longint;
   function GetLabelAtItemPos(aPoint: integer): XLabelType;
   function GetNumberOfData: longint;
   function GetXValue(aRow: byte; aPoint: integer): ValueType;
   function GetYValue(aRow: byte; aPoint: integer): tYPointT;
   procedure ResetSUM;
   procedure SetXMinMax;
   procedure SetXLabel(aXValue: ValueType; aXLabel: XLabelType);
   procedure SortByRow(aRow: byte; aSortOrder: eSortOrderT);
   property NumSeries: integer read FNumSeries write SetNumSeries default 0;
  published
   property SortOrder: eSortOrderT read FSortOrder;
   property SortRow: integer read FSortRow;
   property XMax: double read FXMax;
   property XMin: double read FXMin;
 end; //TmmGraphData


 //cMaxSeries -> mmGraphGlobal.pas 
 TmmGraphSeries = class(TPersistent)
  private
   FControl: TControl;
   FColor: array[1..cMaxSeries] of TColor;
   FFieldName: array[1..cMaxSeries] of string;
   FID: array[1..cMaxSeries] of integer;
   FName: array[1..cMaxSeries] of string;
   FOnChange: TNotifyEvent;
   FShowStat: array[1..cMaxSeries] of sStatistikT;
   FVisible: array[1..cMaxSeries] of boolean;
   FYAxis: array[1..cMaxSeries] of eYAxisT;
   FUpperLimit: array[1..cMaxSeries] of ValueType;
   FLowerLimit: array[1..cMaxSeries] of ValueType;
   FUpperLimitColor: array[1..cMaxSeries] of TColor;
   FLowerLimitColor: array[1..cMaxSeries] of TColor;
   FYScaling: array[1..cMaxSeries] of double;
   FYScalingType: array[1..cMaxSeries] of eYScalingTypeT;
   procedure SetColor(index: integer; value: TColor);
   function GetColor(index: integer): TColor;
   procedure SetFieldName(index: integer; value: string);
   function GetFieldName(index: integer): string;
   procedure SetID(index: integer; value: integer);
   function GetID(index: integer): integer;
   procedure SetName(index: integer; value: string);
   function GetName(index: integer): string;
   procedure SetShowStat(index: integer; value: sStatistikT);
   function GetShowStat(index: integer): sStatistikT;
   procedure SetVisible(index: integer; value: boolean);
   function GetVisible(index: integer): boolean;
   procedure SetYAxis(index: integer; value: eYAxisT);
   function GetYAxis(index: integer): eYAxisT;
   procedure SetUpperLimit(index: integer; value: ValueType);
   function GetUpperLimit(index: integer): ValueType;
   procedure SetLowerLimit(index: integer; value: ValueType);
   function GetLowerLimit(index: integer): ValueType;
   procedure SetUpperLimitColor(index: integer; value: TColor);
   function GetUpperLimitColor(index: integer): TColor;
   procedure SetLowerLimitColor(index: integer; value: TColor);
   function GetLowerLimitColor(index: integer): TColor;
   procedure SetYScaling(index: integer; value: double);
   function GetYScaling(index: integer): double;
   procedure SetYScalingType(index: integer; value: eYScalingTypeT);
   function GetYScalingType(index: integer): eYScalingTypeT;
  protected
   property Control: TControl read FControl;
   procedure Change; dynamic;
  public
   constructor Create(Control: TControl); virtual;
   property Color[index: integer]: TColor read GetColor write SetColor;
   property FieldName[index: integer]: string read GetFieldName write SetFieldName;
   property ID[index: integer]: integer read GetID write SetID;
   property LowerLimit[index: integer]: ValueType read GetLowerLimit write SetLowerLimit;
   property LowerLimitColor[index: integer]: TColor read GetLowerLimitColor write SetLowerLimitColor;
   property Name[index: integer]: string read GetName write SetName;
   property ShowStat[index: integer]: sStatistikT read GetShowStat write SetShowStat;
   property UpperLimit[index: integer]: ValueType read GetUpperLimit write SetUpperLimit;
   property UpperLimitColor[index: integer]: TColor read GetUpperLimitColor write SetUpperLimitColor;
   property Visible[index: integer]: boolean read GetVisible write SetVisible;
   property YAxis[index: integer]: eYAxisT read GetYAxis write SetYAxis;
   property YScaling[index: integer]: double read GetYScaling write SetYScaling;
   property YScalingType[index: integer]: eYScalingTypeT read GetYScalingType write SetYScalingType;
  published
   property OnChange: TNotifyEvent read FOnChange write FOnChange;
 end; //TmmGraphSeries


 TmmGraphColors = class(TPersistent)
  private
   FControl: TControl;
   FPanelBackground: TColor;
   procedure SetGraphColors(Index: Integer; Value: TColor);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property PanelBackground: TColor index 0 read FPanelBackground write SetGraphColors;
  end; //TmmGraphColors



 TmmGraphStatistikLines = class(TPersistent)
  private
   FControl: TControl;
   FStyle: array[stMin..stUpperLimit] of TPenStyle;
   FWidth: array[stMin..stUpperLimit] of  integer;
   procedure SetStyle(index: eStatistikT; value: TPenStyle);
   procedure SetWidth(index: eStatistikT; value: integer);
   function GetStyle(index: eStatistikT): TPenStyle;
   function GetWidth(index: eStatistikT): integer;
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
   property Style[index: eStatistikT]: TPenStyle read GetStyle write SetStyle;
   property Width[index: eStatistikT]: integer read GetWidth write SetWidth;
  published
 end; //TmmGraphStatistikLines



 TmmGraphMargin = class(TPersistent)
  private
   FControl: TControl;
   FBottom: integer;
   FGraph_Legend: integer;
   FLeft: integer;
   FRight: integer;
   FTitle_Graph: integer;
   FTop: integer;
   procedure SetGraphMargin(Index: Integer; Value: integer);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property Bottom: integer index 0 read FBottom write SetGraphMargin;
   property Graph_Legend: integer index 1 read FGraph_Legend write SetGraphMargin;
   property Left: integer index 2 read FLeft write SetGraphMargin;
   property Right: integer index 3 read FRight write SetGraphMargin;
   property Title_Graph: integer index 4 read FTitle_Graph write SetGraphMargin;
   property Top: integer index 5 read FTop write SetGraphMargin;
  end; //TmmGraphMargin



 TmmCustomGraphArea = class(TPersistent)
  private
   FControl: TControl;
   FAutoScaling: boolean;
   FBackgroundColor: TColor;
   FBorderStyle: TBorderStyle;
   FDecimals: integer;
   FDefaultMinY: double;
   FDefaultMaxY: double;
   FFont: TFont;
   FFormatStr: string;
   FHeight: integer;
   FPosition: eLegendPosT;
   FTitle: string;
   FTitleMargin: integer;
   FTitleOrientation: eTitleOrientationT;
   FVisible: boolean;
   FWidth: integer;
   FXValueFormat: string;
   FXAxisType: eXAxisTypeT;
   FXAxisTextAngle: integer;
   FLeft,FTop,FRight,FBottom: integer;
   FRefreshOnSizeChange: sSizeRefreshT;
   procedure SetAutoScaling(value: boolean);
   procedure SetBackgroundColor(value: TColor);
   procedure SetBorderStyle(value: TBorderStyle);
   procedure SetDecimals(value: integer);
   procedure SetDefaultMinY(value: double);
   procedure SetDefaultMaxY(value: double);
   procedure SetFont(value: TFont);
   procedure SetHeight(value: integer);
   procedure SetPosition(value: eLegendPosT);
   procedure SetTitle(value: string);
   procedure SetTitleMargin(value: integer);
   procedure SetTitleOrientation(value: eTitleOrientationT);
   procedure SetVisible(value: boolean);
   procedure SetWidth(value: integer);
   procedure SetXValueFormat(value: string);
   procedure SetXAxisType(value: eXAxisTypeT);
   procedure SetXAxisTextAngle(value: integer);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
   destructor Destroy; override;
   property AutoScaling: boolean read FAutoScaling write SetAutoScaling default true;
   property BackgroundColor: TColor read FBackgroundColor write SetBackgroundColor;
   property BorderStyle: TBorderStyle read FBorderStyle write SetBorderStyle;
   property Bottom: integer read FBottom write FBottom;
   property Decimals: integer read FDecimals write SetDecimals default 2;
   property DefaultMinY: double read FDefaultMinY write SetDefaultMinY;
   property DefaultMaxY: double read FDefaultMaxY write SetDefaultMaxY;
   property Font: TFont read FFont write SetFont;
   property FormatStr: string read FFormatStr write FFormatStr;
   property Height: integer read FHeight write SetHeight;
   property Left: integer read FLeft write FLeft;
   property Position: eLegendPosT read FPosition write SetPosition default lpRight;
   property RefreshOnSizeChange: sSizeRefreshT read FRefreshOnSizeChange write FRefreshOnSizeChange;
   property Right: integer read FRight write FRight;
   property Title: string read FTitle write SetTitle;
   property TitleMargin: integer read FTitleMargin write SetTitleMargin default 5;
   property TitleOrientation: eTitleOrientationT read FTitleOrientation write SetTitleOrientation default toUp;
   property Top: integer read FTop write FTop;
   property Visible: boolean read FVisible write SetVisible;
   property Width: integer read FWidth write SetWidth;
   property XAxisType: eXAxisTypeT read FXAxisType write SetXAxisType default xaFixed;
   property XAxisTextAngle: integer read FXAxisTextAngle write SetXAxisTextAngle default 0;
   property XValueFormat: string read FXValueFormat write SetXValueFormat;
  published
  end; //TmmCustomGraphArea



 TmmGraphLegend = class(TmmCustomGraphArea)
  private
  protected
  public
  published
   property BackgroundColor;
   property BorderStyle;
   property Font;
   property Position;
   property Visible;
  end; //TmmGraphLegend



 TmmGraphChartArea = class(TmmCustomGraphArea)
  private
  protected
  public
  published
   property BackgroundColor;
  end; //TmmGraphChartArea



 TmmGraphLeftYAxis = class(TmmCustomGraphArea)
  private
  protected
  public
  published
   property AutoScaling;
   property BackgroundColor;
   property BorderStyle;
   property Decimals;
   property DefaultMinY;
   property DefaultMaxY;
   property Font;
   property Title;
   property TitleMargin;
   property TitleOrientation;
   property Width;
   property Visible;
  end; //TmmGraphLeftYAxis



 TmmGraphRightYAxis = class(TmmCustomGraphArea)
  private
  protected
  public
  published
   property AutoScaling;
   property BackgroundColor;
   property BorderStyle;
   property Decimals;
   property DefaultMinY;
   property DefaultMaxY;
   property Font;
   property Title;
   property TitleMargin;
   property TitleOrientation;
   property Visible;
   property Width;
  end; //TmmGraphRightYAxis



 TmmGraphXAxis = class(TmmCustomGraphArea)
  private
  protected
  public
  published
   property BackgroundColor;
   property BorderStyle;
   property Font;
   property Height;
   property Title;
   property TitleMargin;
   property XAxisType;
   property XAxisTextAngle;
   property XValueFormat;
  end; //TmmGraphXAxis



 TmmGraphTitles = class(TmmCustomGraphArea)
  private
   FControl: TControl;
   FAlignment: TAlignment;
   FSubTitle: string;
   FSubTitleFont: TFont;
   procedure SetAlignment(value: TAlignment);
   procedure SetSubTitle(value: string);
   procedure SetSubTitleFont(value: TFont);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); override;
   destructor Destroy; override;
  published
   property Alignment: TAlignment read FAlignment write SetAlignment;
   property BackgroundColor;
   property BorderStyle;
   property Font;
   property SubTitle: string read FSubTitle write SetSubTitle;
   property SubTitleFont: TFont read FSubTitleFont write SetSubTitleFont;
   property Title;
   property Visible;
  end; //TmmGraphTitles



 TmmCustomGraphGrid = class(TPersistent)
  private
   FControl: TControl;
   FColor: TColor;
   FForeground: boolean;
   FGridType: eHorzGridT;
   FStyle: TPenStyle;
   FVisible: boolean;
   FWidth: integer;
   procedure SetColor(Value: TColor);
   procedure SetForeground(Value: boolean);
   procedure SetGridType(Value: eHorzGridT);
   procedure SetStyle(Value: TPenStyle);
   procedure SetVisible(value: boolean);
   procedure SetWidth(Value: integer);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
   property Color: TColor read FColor write SetColor default clBlack;
   property Foreground: boolean read FForeground write SetForeground default false;
   property GridType: eHorzGridT read FGridType write SetGridType default hgLeftAxis;
   property Style: TPenStyle read FStyle write SetStyle default psSolid;
   property Visible: boolean read FVisible write SetVisible default true;
   property Width: integer read FWidth write SetWidth default 1;
  published
  end; //TmmCustomGraphGrid


 TmmGraphHorzGrid = class(TmmCustomGraphGrid)
  private
  protected
  public
  published
   property Color;
   property Foreground;
   property GridType;
   property Style;
   property Width;
  end; //TmmGraphHorzGrid


 TmmGraphVertGrid = class(TmmCustomGraphGrid)
  private
  protected
  public
  published
   property Color;
   property Foreground;
   property Style;
   property Visible;
   property Width;
  end; //TmmGraphVertGrid



 TmmGraphScrollButton = class(TmmSpeedButton)
  private
   FRepeatTimer: TTimer;
   procedure TimerExpired(Sender: TObject);
  protected
   procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X,Y: Integer); override;
   procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X,Y: Integer); override;
  public
   constructor Create(AOwner: TComponent; bTyp: eScrollBtnT); overload;
   destructor Destroy; override;
 end; //TmmGraphScrollButton



 TmmGraphBars = class(TPersistent)
  private
   FControl: TControl;
   FDefaultBarWidth: integer;
   FGap: integer;
   FMinBarsPerPage: integer;
   FMinBarWidth: integer;
   FStyle: eBarStyleT;
   procedure SetDefaultBarWidth(Value: integer);
   procedure SetGap(Value: integer);
   procedure SetMinBarsPerPage(Value: integer);
   procedure SetMinBarWidth(Value: integer);
   procedure SetStyle(Value: eBarStyleT);
  protected
   BarWidth: integer;
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property DefaultBarWidth: integer read FDefaultBarWidth write SetDefaultBarWidth default 14;
   property Gap: integer read FGap write SetGap default 4;
   property MinBarsPerPage: integer read FMinBarsPerPage write SetMinBarsPerPage default 0;
   property MinBarWidth: integer read FMinBarWidth write SetMinBarWidth default 4;
   property Style: eBarStyleT read FStyle write SetStyle default bsSide;
  end; //TmmGraphBars


 TmmSelection = class(TPersistent)
  private
   FControl: TControl;
   FEnabled: boolean;
   FFixedColor: TColor;
   FStyle: eSelectionStyleT;
    fShift: TShiftState;
    fButton: TMouseButton;
   procedure SetEnabled(value: boolean);
   procedure SetFixedColor(value: TColor);
   procedure SetStyle(Value: eSelectionStyleT);
    procedure SetShift(const Value: TShiftState);
    procedure SetButton(const Value: TMouseButton);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property Enabled: boolean read FEnabled write SetEnabled default true;
   property FixedColor: TColor read FFixedColor write SetFixedColor default clBlue;
   property Style: eSelectionStyleT read FStyle write SetStyle default ssInverted;
   property Shift :TShiftState read fShift write SetShift;          // SDO
   property Button :TMouseButton read fButton write SetButton;      // SDO
  end; //TmmSelection



 TmmGraphLines = class(TPersistent)
  private
   FControl: TControl;
   FMinPointGap: integer;
   FSmoothLines: boolean;
   procedure SetMinPointGap(Value: integer);
   procedure SetSmoothLines(Value: boolean);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property MinPointGap: integer read FMinPointGap write SetMinPointGap default 8;
   property SmoothLines: boolean read FSmoothLines write SetSmoothLines default true;
  end; //TmmGraphLines



 //Header & Footer fuer Druck aus TmmGraph
 TPrintHeader = Class(TPersistent)
  private
    fTitle : String;
    FPrintHeader : Boolean;
    function GetTitle: String;
    procedure SetTitle(const Value: String);
    function GetPrintHeader: Boolean;
    procedure SetPrintHeader(const Value: Boolean);
  public
     constructor Create(Control: TControl); virtual;
     destructor Destroy; override;
  published
     property Title : String read GetTitle write SetTitle;
     property PrintHeader : Boolean read GetPrintHeader write SetPrintHeader;
 end;

 TPrintFooter = Class(TPersistent)
  private
    mImage : TImage;
    FPrintFooter : Boolean;
    function GetImage: TPicture;
    procedure SetImage(const Value: TPicture);
    function GetPrintFooter: Boolean;
    procedure SetPrintFooter(const Value: Boolean);
  protected
   //property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
   destructor Destroy; override;
  published
     property Logo : TPicture read GetImage write SetImage;
     property PrintFooter : Boolean read GetPrintFooter write SetPrintFooter;
 end;


 TmmGraphDblClick = procedure(Sender: TObject; AItem: eDblClickT; ARow,APoint: integer; AStatistik: eStatistikT) of object;
 TmmGraphClick = procedure(Sender: TObject; Button: TMouseButton; Shift: TShiftState; ARow,APoint: integer) of object;

 TmmGraph = class(TCustomPanel)
  private
   FCanRefresh: boolean;
   FColors: TmmGraphColors;
   FBars: TmmGraphBars;
   FChartArea: TmmGraphChartArea;
   FData: TmmGraphData;
   FDateTickType: eDateticksT;
   FhwInfo: THintWindow;
   FMargin: TmmGraphMargin;
   FLeftYAxis: TmmGraphLeftYAxis;
   FLegend: TmmGraphLegend;
   FLines: TmmGraphLines;
   FMemoINI: TMemoIni;
   FOnGraphDblClick: TmmGraphDblClick;
   FOnGraphClick  : TmmGraphClick;
   FPaintBox: TmmPaintBox;
   {$ifdef UsePopup}
   FPopUp: TPopUpMenu;
   {$endif}
   FRightYAxis: TmmGraphRightYAxis;
   FScrollBar: TmmScrollBar;
   FLeftBtnInc: TmmGraphScrollButton;
   FLeftBtnDec: TmmGraphScrollButton;
   FReadOnly: boolean;
   FRightBtnInc: TmmGraphScrollButton;
   FRightBtnDec: TmmGraphScrollButton;
   FSeries: TmmGraphSeries;
   FSelection: TmmSelection;
   FSelectedPoint: integer;
   FSelectedRow: integer;
   FStatistikLines: TmmGraphStatistikLines;
   FTitles: TmmGraphTitles;
   FXAxis: TmmGraphXAxis;
   FHorzGrid: TmmGraphHorzGrid;
   FVertGrid: TmmGraphVertGrid;
   FShowScrollButtons: boolean;
   FObj: array of tGraphItemT;

   FPrintHeader : TPrintHeader;
   FPrintFooter : TPrintFooter;

   procedure SetReadOnly(value: boolean);
   procedure SetShowScrollButtons(value: boolean);
   function GetNumOfBarSeries: integer;
   procedure SetMemoINI(value: TMemoINI);
   function GetMemoINI: TMemoINI;

  protected
   XScale: tXScaleRecordT;
   YScale: tYScaleRecordT;
   YStretch: array[eYAxisT] of double;
   BarWidth: integer;
   IsPrinting,
   IsSelecting: boolean;
   Origin,
   MovePt: TPoint;
   PInfo: tPrinterInfoT;
   procedure CalcBarWidth;
   procedure CalcGraphCoordinates(aCanvas: TCanvas);
   procedure CalcLegendWidth(aCanvas: TCanvas);
   procedure CalcPrintRect;
   procedure CalcScrollBtnPos;
   procedure CalcScrollBar;
   procedure CalcYAxis;
   procedure ClipGraph(aCanvas: TCanvas);
   {$ifdef UsePopup}
   procedure CreatePopUpMenu;
   {$endif}
   procedure DrawChartArea(aCanvas: TCanvas);
   procedure DrawLegend(aCanvas: TCanvas);
   procedure DrawMainChart(aCanvas: TCanvas);
   procedure DrawOnCanvas(aCanvas: TCanvas);
   procedure DrawPanelBackground(aCanvas: TCanvas);
   procedure DrawRubberBand(TopLeft,BottomRight: TPoint; aMode: TPenMode);
   procedure DrawStatistikLines(aCanvas: TCanvas);
   procedure DrawTitles(aCanvas: TCanvas);
   procedure DrawVertGrid(aCanvas: TCanvas);
   procedure DrawXAxis(aCanvas: TCanvas);
   procedure DrawYAxis(aCanvas: TCanvas; aYAxis: eYAxisT);

   function FindClickedItem(x,y: integer): longint;
   procedure FPaintBoxPaint(Sender: TObject);
   procedure FPaintBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
   procedure FPaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
   procedure FPaintBoxMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
   {$ifdef UsePopup}
   procedure FPopUpOnPopUp(Sender: TObject);
   {$endif}
   procedure FScrollBar1Change(Sender: TObject);
   procedure FScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
   procedure FScrollBtnClick(Sender: TObject);
   function fx(aPoint: integer; aXValue: ValueType): integer;
   function fy(v: double; aAxis: eYAxisT): integer;
   function GetDateFormat(aDateTicks: eDateTicksT; aValue: double): string;
   function GetDateStep(aDateTicks: eDateTicksT): double;
   function GetDblClickItem(x,y: integer): eDblClickT;
   function GetFirstDateTick(aMinDate: double): double;
   function GetHorzTextPos(x1,x2,TextWidth: integer; Alignment: TAlignment): integer;
   function GetLegendItemHeight(aCanvas: TCanvas): integer;
   function GetLegendItemWidth(aCanvas: TCanvas): integer;
   function GetNextDateTick(tick: double): double;
   function GetPrnFactX(aValue: double): integer;
   function GetPrnFactY(aValue: double): integer;
   function GetPrnOffsetX: integer;
   function GetPrnOffsetY: integer;
   procedure GetScrollBarMinMax(var aMin,aMax: integer);
   function GetStep(aMinSteps: Word; aMin,aMax: extended): double;
   function GetVertTextPos(y1,y2,TextHeight: integer; YAlignment: eYAlignmentT): integer;
   function GetXItemAtXPos(xPos: integer): integer;
   function GetXValueAtXPos(xPos: integer): double;
   procedure InitPrinterInfo;
   function InRect(aRect: trect; x,y: integer): boolean;
   procedure InsertObject(gi: eGraphItemT; r: trect; dr,dp: integer; st: eStatistikT);
   function LTrans(aText: string): string;
   procedure miEditSeriesClick(Sender: TObject);
   procedure miHideLegendClick(Sender: TObject);
   procedure miHideRightYAxisClick(Sender: TObject);
   procedure miShowLegendClick(Sender: TObject);
   procedure miShowRightYAxisClick(Sender: TObject);
   procedure miSortSeriesClick(Sender: TObject);
   procedure SetBorderStyle(aCanvas: TCanvas; BorderStyle: TBorderStyle; BackgroundColor: TColor);
   function SetFontHeight(aFont: TFont): integer;
   procedure SetMinMax(var aMin,aMax,aStep: extended; aAutoStep: boolean);
   procedure SetTextDirection(aCanvas: TCanvas; aAngle: single);
   procedure SetXScale(aMin,aMax: double);
   procedure unClipGraph(aCanvas: TCanvas);
   procedure WmSize(var Message: TMessage); message WM_Size;
   procedure PrintHeaderFooter(aCanvas: TCanvas);

  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   procedure BeginUpdate;
   procedure EditOptionsDlg;
   procedure EditSeriesDlg;
   procedure EndUpdate;
   property NumOfBarSeries: integer read GetNumOfBarSeries;
   procedure Print(aXPosMM,aYPosMM,aWidthMM,aHeightMM: integer);
   procedure ReadSettings;
   procedure SortSeriesDlg;
   procedure WriteSettings;
   procedure UnselectBar;     // SDo
  published
   property Align;
   property Anchors;
   property Bars: TmmGraphBars read FBars write FBars;
   property ChartArea: TmmGraphChartArea read FChartArea write FChartArea;
   property Colors: TmmGraphColors read FColors write FColors;
   property Constraints;
   property Data: TmmGraphData read FData write FData;
   property HorzGrid: TmmGraphHorzGrid read FHorzGrid write FHorzGrid;
   property Margin: TmmGraphMargin read FMargin write FMargin;
   property MemoINI: TMemoIni read GetMemoINI write SetMemoINI;
   property LeftYAxis: TmmGraphLeftYAxis read FLeftYAxis write FLeftYAxis;
   property Legend: TmmGraphLegend read FLegend write FLegend;
   property Lines: TmmGraphLines read FLines write FLines;
   property OnGraphDblClick: TmmGraphDblClick read FOnGraphDblClick write FOnGraphDblClick;
   property OnGraphClick: TmmGraphClick read FOnGraphClick write FOnGraphClick;  // SDo
   property PopupMenu;
   property ReadOnly: boolean read FReadOnly write SetReadOnly;
   property RightYAxis: TmmGraphRightYAxis read FRightYAxis write FRightYAxis;
   property Selection: TmmSelection read FSelection write FSelection;
   property Series: TmmGraphSeries read FSeries write FSeries;
   property ShowScrollButtons: boolean read FShowScrollButtons write SetShowScrollButtons default true;
   property StatistikLines: TmmGraphStatistikLines read FStatistikLines write FStatistikLines;
   property Titles: TmmGraphTitles read FTitles write FTitles;
   property VertGrid: TmmGraphVertGrid read FVertGrid write FVertGrid;
   property Visible;
   property XAxis: TmmGraphXAxis read FXAxis write FXAxis;
   property PageHeader : TPrintHeader read FPrintHeader write FPrintHeader;
   property PageFooter : TPrintFooter read FPrintFooter write FPrintFooter;


 end; //TmmGraph


//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

 mmCommonLib, SettingsReader;

{$R *.RES}

resourcestring
 cHintBtnUpStr       = '(40)Skalenbereich vergroessern';    //ivlm
 cHintBtnDownStr     = '(40)Skalenbereich verkleinern';    //ivlm
 cXAxisStr           = '(15)X-Achse';                      //ivlm
 cRowStr             = '(25)Datenreihe: %s';               //ivlm
 cPointStr           = '(25)Datenpunkt: %s';               //ivlm
 cParameterStr       = '(25)Parameter: %s = ';             //ivlm
 cValueStr           = '(25)Wert: %s';                     //ivlm
 cMinStr             = '(25)Minima';                       //ivlm
 cMaxStr             = '(25)Maxima';                       //ivlm
 cMeanStr            = '(25)Mittelwert';                   //ivlm
 cStdDevStr          = '(25)Standard-Abweichung';          //ivlm
 cQ90Str             = '(25)Vertrauensintervall Q90%';     //ivlm
 cQ95Str             = '(25)Vertrauensintervall Q95%';     //ivlm
 cQ99Str             = '(25)Vertrauensintervall Q99%';     //ivlm
 cUpperStr           = '(25)Oberer Grenzwert';             //ivlm
 cLowerStr           = '(25)Unterer Grenzwert';            //ivlm
 cPage               = '(*)Seite';                         //ivlm

const
 cScrollBarHeight    = 25;
 cInitRepeatPause    = 400;
 cRepeatPause        = 100;
 cPointDiff          = 5;
 cScrollBtnWidth     = 22;
 cScrollBtnHeight    = 12;
 cScrollBtnGap       = 0;
 cMaxLegendWidth     = 250;
 cMaxMinBarsPerPage  = 200;
 cMaxBarWidth        = 50;

 cDiff_LegendText    = 7;
 cDiff_LegendItems   = 3;
 cDiff_LegendBorder  = 5;
 cDiff_AxisTicksText = 8;


//TmmGraphSeries ***
constructor TmmGraphSeries.Create;
var
 i : integer;

begin
 inherited Create;
 FControl := Control;

 for i := 1 to cMaxSeries do
 begin
  ID[i] := 0;
  Color[i] := clBlack;
  FieldName[i] := '';
  Name[i] := '';
  ShowStat[i] := [];
  Visible[i] := true;
  YAxis[i] := yaxLeft;
  UpperLimit[i] := 0;
  LowerLimit[i] := 0;
  UpperLimitColor[i] := clRed;
  LowerLimitColor[i] := clRed;
  YScalingType[i] := ysNone;
 end; //for i := 1 to cMaxSeries do

 //demo data
 if csDesigning in FControl.ComponentState then
 begin
  Name[1] := '%MEff (%)';
  Name[2] := 'CSIRO (/100 Km)';
  Name[3] := 'CSp (/100 Km)';
  Name[4] := 'Sp (/100 Km)';
  Name[5] := 'RSp (/100 Km)';
  Name[6] := 'CBu (/100 Km)';
  Name[7] := 'YB (/100 Km)';
  Color[1] := $00804000;
  Color[2] := $0000E1E1;
  Color[3] := $00092EC1;
  Color[4] := clBlue;
  Color[5] := clGreen;
  Color[6] := clBlack;
  Color[7] := clOlive;

  ShowStat[1] := [];
  Visible[1] := true;
  YAxis[1] := yaxLeft;
  YScalingType[1] := ysNone;

  ShowStat[2] := [];
  Visible[2] := true;
  YAxis[2] := yaxRight;
  YScalingType[2] := ysNone;

  ShowStat[3] := [];
  Visible[3] := true;
  YAxis[3] := yaxRight;
  YScalingType[3] := ysNone;

  ShowStat[4] := [];
  Visible[4] := true;
  YAxis[4] := yaxLeft;
  YScalingType[4] := ysNone;

  ShowStat[5] := [];
  Visible[5] := true;
  YAxis[5] := yaxLeft;
  YScalingType[5] := ysNone;

  ShowStat[6] := [];
  Visible[6] := true;
  YAxis[6] := yaxLeft;
  YScalingType[6] := ysNone;

  ShowStat[7] := [];
  Visible[7] := true;
  YAxis[7] := yaxLeft;
  YScalingType[7] := ysNone;
 end; //if csDesigning in FControl.ComponentState then
end; //constructor TmmGraphSeries.Create
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetName(index: integer; value: string);
begin
 if value <> FName[Index] then
 begin
  FName[index] := value;

  if not(csDesigning in FControl.ComponentState) then
  begin
   if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
   Change;
  end; //if not(csDesigning in FControl.ComponentState) then
 end; //if value <> FName[Index] then
end; //procedure TmmGraphSeries.SetName
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetName(index: integer): string;
begin
 result := FName[index];
end; //function TmmGraphSeries.GetName
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetColor(index: integer; value: TColor);
begin
 if value <> FColor[Index] then
 begin
  FColor[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if aColor <> FColor[Index] then
end; //procedure TmmGraphSeries.SetColor
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetColor(index: integer): TColor;
begin
 result := FColor[index];
end; //function TmmGraphSeries.GetColor
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetFieldName(index: integer; value: string);
begin
 FFieldName[index] := value;
end; //procedure TmmGraphSeries.SetFieldName
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetFieldName(index: integer): string;
begin
 result := FFieldName[index];
end; //function TmmGraphSeries.GetFieldName
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetID(index: integer; value: integer);
begin
 FID[index] := value;
end; //procedure TmmGraphSeries.SetID
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetID(index: integer): integer;
begin
 result := FID[index];
end; //function TmmGraphSeries.GetID
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetShowStat(index: integer; value: sStatistikT);
begin
 if value <> FShowStat[Index] then
 begin
  FShowStat[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FShowStat[Index] then
end; //procedure TmmGraphSeries.SetShowStat
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetShowStat(index: integer): sStatistikT;
begin
 result := FShowStat[index];
end; //function TmmGraphSeries.GetShowStat
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetVisible(index: integer; value: boolean);
begin
 if value <> FVisible[Index] then
 begin
  FVisible[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FVisible[Index] then
end; //procedure TmmGraphSeries.SetVisible
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetVisible(index: integer): boolean;
begin
 result := FVisible[index];
end; //function TmmGraphSeries.GetVisible
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetYAxis(index: integer; value: eYAxisT);
begin
 if value <> FYAxis[Index] then
 begin
  FYAxis[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FYAxis[Index] then
end; //procedure TmmGraphSeries.SetYAxis
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetYAxis(index: integer): eYAxisT;
begin
 result := FYAxis[index];
end; //function TmmGraphSeries.GetYAxis
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetUpperLimit(index: integer; value: ValueType);
begin
 if value <> FUpperLimit[Index] then
 begin
  FUpperLimit[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FUpperLimit[Index] then
end; //procedure TmmGraphSeries.SetUpperLimit
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetUpperLimit(index: integer): ValueType;
begin
 result := FUpperLimit[index];
end; //function TmmGraphSeries.GetUpperLimit
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetLowerLimit(index: integer; value: ValueType);
begin
 if value <> FLowerLimit[Index] then
 begin
  FLowerLimit[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FLowerLimit[Index] then
end; //procedure TmmGraphSeries.SetLowerLimit
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetLowerLimit(index: integer): ValueType;
begin
 result := FLowerLimit[index];
end; //function TmmGraphSeries.GetLowerLimit
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetUpperLimitColor(index: integer; value: TColor);
begin
 if value <> FUpperLimitColor[Index] then
 begin
  FUpperLimitColor[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FUpperLimitColor[Index] then
end; //procedure TmmGraphSeries.SetUpperLimitColor
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetUpperLimitColor(index: integer): TColor;
begin
 result := FUpperLimitColor[index];
end; //function TmmGraphSeries.GetUpperLimitColor
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetLowerLimitColor(index: integer; value: TColor);
begin
 if value <> FLowerLimitColor[Index] then
 begin
  FLowerLimitColor[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FLowerLimitColor[Index] then
end; //procedure TmmGraphSeries.SetLowerLimitColor
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetLowerLimitColor(index: integer): TColor;
begin
 result := FLowerLimitColor[index];
end; //function TmmGraphSeries.GetLowerLimitColor
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetYScaling(index: integer; value: double);
begin
 if value <> FYScaling[Index] then FYScaling[index] := value;
end; //procedure TmmGraphSeries.SetYScaling
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetYScaling(index: integer): double;
begin
 result := FYScaling[index];
end; //function TmmGraphSeries.GetYScaling
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.SetYScalingType(index: integer; value: eYScalingTypeT);
const
 cYScaleArray : array[eYScalingTypeT] of double = (1,2,5,10,50,100,500,1000,0.5,0.1,0.05,0.01,0.005,0.001,0.0005);

begin

 FYScaling[index] := cYScaleArray[value];

 if value <> FYScalingType[Index] then
 begin
  FYScalingType[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
  Change;
 end; //if value <> FYScalingType[Index] then
end; //procedure TmmGraphSeries.SetYScalingType
//-----------------------------------------------------------------------------
function TmmGraphSeries.GetYScalingType(index: integer): eYScalingTypeT;
begin
 result := FYScalingType[index];
end; //function TmmGraphSeries.GetYScalingType
//-----------------------------------------------------------------------------
procedure TmmGraphSeries.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end; //procedure TmmGraphSeries.Change


//-----------------------------------------------------------------------------


//TmmGraphStatistikLines ***
constructor TmmGraphStatistikLines.Create;
var
 xStat : eStatistikT;

begin
 inherited Create;
 FControl := Control;

 FStyle[stMin] := psDot;
 FStyle[stMax] := psDot;
 FStyle[stMean] := psSolid;
 FStyle[stStdDev] := psDash;
 FStyle[stQ90] := psDashDot;
 FStyle[stQ95] := psDashDot;
 FStyle[stQ99] := psDashDot;
 FStyle[stLowerLimit] := psSolid;
 FStyle[stUpperLimit] := psSolid;

 for xStat := stMin to stUpperLimit do
  FWidth[xStat] := 1;
end; //constructor TmmGraphStatistikLines.Create
//-----------------------------------------------------------------------------
procedure TmmGraphStatistikLines.SetStyle(index: eStatistikT; value: TPenStyle);
begin
 if value <> FStyle[index] then
 begin
  FStyle[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if value <> FStyle[index] then
end; //procedure TmmGraphStatistikLines.SetYAxis
//-----------------------------------------------------------------------------
procedure TmmGraphStatistikLines.SetWidth(index: eStatistikT; value: integer);
begin
 if value <> FWidth[index] then
 begin
  FWidth[index] := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if value <> FWidth[index] then
end; //procedure TmmGraphStatistikLines.SetWidth
//-----------------------------------------------------------------------------
function TmmGraphStatistikLines.GetStyle(index: eStatistikT): TPenStyle;
begin
 result := FStyle[index];
end; //procedure TmmGraphStatistikLines.SetYAxis
//-----------------------------------------------------------------------------
function TmmGraphStatistikLines.GetWidth(index: eStatistikT): integer;
begin
 result := FWidth[index];
end; //procedure TmmGraphStatistikLines.SetWidth


//-----------------------------------------------------------------------------


//TmmGraphData ***
constructor TmmGraphData.Create;
const
 cMaxDemoPoints = 60;

var
 i : integer;

begin
 inherited Create;
 FControl := Control;
 FNumSeries := 0;
 FSortOrder := soNone;
 FSortRow := 0;
 FXMin := 1e99;
 FXMax := -1e99;

 ClearData;

 //demo data
 if csDesigning in FControl.ComponentState then
 begin
  FNumSeries := 7;
  for i := 1 to cMaxDemoPoints do
  begin
   AddYValue(1,i,50 +random(33),inttostr(i));
   AddYValue(2,i,60 +random(45),inttostr(i));
   AddYValue(3,i,20 +random(15),inttostr(i));
   AddYValue(4,i,40 +random(25),inttostr(i));
   AddYValue(5,i, 0 +random(50),inttostr(i));
   AddYValue(6,i,80 +random(8),inttostr(i));
   AddYValue(7,i,30 +random(10),inttostr(i));
  end; //for i  := 1 to cMaxDemoPoints do
 end; //if csDesigning in ComponentState then
end; //constructor TmmGraphData.Create
//-----------------------------------------------------------------------------
destructor TmmGraphData.Destroy;
begin
 inherited Destroy;
 Data.Values := nil;
end; //destructor TmmGraphData.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraphData.SetNumSeries(value: integer);
begin
 if value < 0 then value := 0;
 if value > cMaxSeries then value := cMaxSeries;      // ??? SDO
 if FNumSeries <> Value then FNumSeries := Value;
end; //procedure TmmGraphData.SetNumSeries
//-----------------------------------------------------------------------------
function TmmGraphData.GetNumberOfData: longint;
begin
 result := high(Data.Values) +1;
end; //function TmmGraphData.GetNumberOfData
//-----------------------------------------------------------------------------
function TmmGraphData.GetXDataPos(aValue: ValueType): longint;
var
 i : longint;

begin
 result := -1;
 for i := 0 to (GetNumberOfData -1) do
  if Data.Values[i].XValue.Value = aValue then
  begin
   result := i;
   break;
  end; //for i := 0 to (GetNumberOfData -1) do
end; //function TmmGraphData.GetXDataPos
//-----------------------------------------------------------------------------
procedure TmmGraphData.AddXValue(aXValue: ValueType; aXLabel: XLabelType);
var
 i,j : longint;

begin
 i := GetXDataPos(aXValue);
 if i = -1 then
 begin
  i := high(Data.Values) +2;
  SetLength(Data.Values,i);
  i := high(Data.Values);
  fillchar(Data.Values[i],sizeof(tXYValueT),0);
  for j := 1 to cMaxSeries do
  begin
   Data.Values[i].YValue[j].Value := 0;
   Data.Values[i].YValue[j].IsNull := true;
  end; //for j := 1 to cMaxSeries do

  Data.Values[i].XValue.Value := aXValue;
  FXMin := math.Min(aXValue,FXMin);
  FXMax := math.Max(aXValue,FXMax);
 end; //if i = -1 then

 if aXLabel <> '' then Data.Values[i].XValue.Name := aXLabel; 
end; //procedure TmmGraphData.AddXValue
//-----------------------------------------------------------------------------
procedure TmmGraphData.SetXLabel(aXValue: ValueType; aXLabel: XLabelType);
begin
 AddXValue(aXValue,aXLabel);
end; //procedure TmmGraphData.SetXLabel
//-----------------------------------------------------------------------------
procedure TmmGraphData.AddYValue(aRow: byte; aXValue,aYValue: ValueType; aXLabel: XLabelType);
var
 i : longint;

begin
 if aRow > cMaxSeries then exit;

 AddXValue(aXValue,aXLabel);
 i := GetXDataPos(aXValue);

 Data.Values[i].YValue[aRow].Value := aYValue;
 Data.Values[i].YValue[aRow].IsNull := false;
end; //procedure TmmGraphData.AddXValue
//-----------------------------------------------------------------------------
procedure TmmGraphData.ClearData;
begin
 Data.Values := nil;
 FSortOrder := soNone;
 FSortRow := 0;

 TmmGraph(FControl).XScale.actMin := 1;
 TmmGraph(FControl).XScale.actMax := 0;

 CalcXMinMax;
end; //procedure TmmGraphData.ClearData
//-----------------------------------------------------------------------------
procedure TmmGraphData.ClearRow(aRow: byte);
var
 i : longint;

begin
 if aRow > cMaxSeries then exit;
 for i := 0 to (GetNumberOfData -1) do
  Data.Values[i].YValue[aRow].IsNull := true;
end; //procedure TmmGraphData.ClearRow
//-----------------------------------------------------------------------------
procedure TmmGraphData.ClearPoint(aRow: byte; aXValue: ValueType);
var
 i : longint;

begin
 if aRow > cMaxSeries then exit;
 i := GetXDataPos(aXValue);
 if i > -1 then Data.Values[i].YValue[aRow].IsNull := true;
end; //procedure TmmGraphData.ClearPoint
//-----------------------------------------------------------------------------
function TmmGraphData.GetLabelAtItemPos(aPoint: integer): XLabelType;
begin
 result := '';
 if aPoint > (GetNumberOfData -1) then exit;
 result := Data.Values[aPoint].XValue.Name;
end; //function TmmGraphData.GetLabelAtItemPos
//-----------------------------------------------------------------------------
function TmmGraphData.GetXValue(aRow: byte; aPoint: integer): ValueType;
begin
 result := 0;
 if aRow > cMaxSeries then exit;
 if aPoint > (GetNumberOfData -1) then exit;
 result := Data.Values[aPoint].XValue.Value;
end; //function TmmGraphData.GetXValue
//-----------------------------------------------------------------------------
function TmmGraphData.GetYValue(aRow: byte; aPoint: integer): tYPointT;
begin
 result.IsNull := true;
 if aRow > cMaxSeries then exit;
 if aPoint > (GetNumberOfData -1) then exit;
 result := Data.Values[aPoint].YValue[aRow];
end; //function TmmGraphData.GetYValue
//-----------------------------------------------------------------------------
procedure TmmGraphData.SortByRow(aRow: byte; aSortOrder: eSortOrderT);

procedure QuickSort(var aData: array of tXYValueT; iLo,iHi: longint);

function GetValue(aPos: longint): ValueType;
begin
 case aRow of
  0: result := aData[aPos].XValue.Value;
  else result := aData[aPos].YValue[aRow].Value;
 end; //case SortBy of
end; //function GetValue

var
 Lo,Hi : longint;
 Mid   : ValueType;
 tmp   : tXYValueT;

begin //main
 Lo := iLo;
 Hi := iHi;
 Mid := GetValue((Lo +Hi) div 2);

 repeat
  case aSortOrder of
   soAscending: begin
    while GetValue(Lo) < Mid do inc(Lo);
    while GetValue(Hi) > Mid do dec(Hi);
   end; //soAscending

   soDescending: begin
    while GetValue(Lo) > Mid do inc(Lo);
    while GetValue(Hi) < Mid do dec(Hi);
   end; //soDescending
  end; //case aSortOrder of

  if Lo <= Hi then
  begin
   move(aData[lo],tmp,sizeof(tXYValueT));
   move(aData[hi],aData[lo],sizeof(tXYValueT));
   move(tmp,aData[hi],sizeof(tXYValueT));

   inc(Lo);
   dec(Hi);
  end; //if Lo <= Hi then
 until Lo > Hi;

 if Hi > iLo then QuickSort(aData,iLo,Hi);
 if Lo < iHi then QuickSort(aData,Lo,iHi);
end; //procedure QuickSort

begin
 if GetNumberOfData < 1 then exit;
 QuickSort(Data.Values,low(Data.Values),high(Data.Values));
 FSortOrder := aSortOrder;
 FSortRow := aRow;
end; //procedure TmmGraphData.SortByRow
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_SUM(const aData: array of ValueType): ValueType;
var
 i : longint;

begin
 result := 0;
 for i := low(aData) to high(aData) do
  result := result +aData[i];
end; //function TmmGraphData.Stat_SUM
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_COUNT(const aData: array of ValueType): longint;
var
 i : longint;

begin
 result := 0;
 for i := low(aData) to high(aData) do
  inc(result);
end; //function TmmGraphData.Stat_COUNT
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_MEAN(const aData: array of ValueType): ValueType;
var
 count : longint;

begin
 count := Stat_COUNT(aData);
 if count < 1 then result := 0
              else result := Stat_SUM(aData) /count;
end; //function TmmGraphData.Stat_Stat_MEAN
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_MIN(const aData: array of ValueType): ValueType;
var
 i : longint;

begin
 if Stat_COUNT(aData) < 1 then
 begin
  result := 0;
  exit;
 end //if Stat_COUNT(aData) < 1 then
 else result := 1e99;

 for i := low(aData) to high(aData) do
  result := math.Min(result,aData[i]);
end; //function TmmGraphData.Stat_MIN
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_MAX(const aData: array of ValueType): ValueType;
var
 i : longint;

begin
 if Stat_COUNT(aData) < 1 then
 begin
  result := 0;
  exit;
 end //if Stat_COUNT(aData) < 1 then
 else result := -1e99;

 for i := low(aData) to high(aData) do
  result := math.Max(result,aData[i]);
end; //TmmGraphData.Stat_MAX
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_STDDEV(const aData: array of ValueType): ValueType;
begin
 try
  result := sqrt(Stat_VARIANCE(aData));
 except
  result := 0;
 end; //try..except
end; //function TmmGraphData.Stat_STDDEV
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_SUMOFSQUARES(const aData: array of ValueType): ValueType;
var
 i : longint;

begin
 result := 0;
 for i := low(aData) to high(aData) do
  result := result +sqr(aData[i]);
end; //function TmmGraphData.Stat_SUMOFSQUARES
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_TOTALVARIANCE(const aData: array of ValueType): ValueType;
var
 count        : longint;
 Sum,
 SumOfSquares : ValueType;

begin
 count := Stat_COUNT(aData);
 Sum := Stat_SUM(aData);
 SumOfSquares := Stat_SUMOFSQUARES(aData);

 if count > 0 then result := SumOfSquares -sqr(sum) /count
              else result := 0;

 //wg. rechenungenauigkeit kleine werte auf null setzen, sonst fuehrt
 //sqrt(result) z.bsp. fuer -1.1e-15 zu error
 if result < cEpsilon then result := 0;
end; //function TmmGraphData.Stat_TOTALVARIANCE
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_VARIANCE(const aData: array of ValueType): ValueType;
var
 count : longint;

begin
 count := Stat_COUNT(aData);
 if count > 1 then result := Stat_TOTALVARIANCE(aData) /(count -1)
              else result := 0;
end; //function TmmGraphData.Stat_VARIANCE
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_CV(aMean,aStdDev: ValueType): ValueType;
begin
 {$ifdef DEBUG}
 if aMean > cEpsilon then result := aStdDev /aMean *100
                     else result := 0;
 {$else}
 try
  if aMean <> 0 then
{Mg}result := aStdDev /aMean *100
  else
    result := 0;  
 except
  result := 0;
 end; //try..except
 {$endif}
end; //function TmmGraphData.Stat_CV
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_TVALUE(aCount: longint; aTNiveau: byte): ValueType;
type
 abcRecord = record
  a,b,c,w2,w3: ValueType;
 end; //abcRecord

const
 abcAr : array[1..4] of abcRecord =
         ((a: 1.5310; b: 0.8621; c: 1.645; w2: 6.314; w3: 2.920),
          (a: 2.4210; b: 1.0260; c: 1.958; w2: 12.71; w3: 4.303),
          (a: 3.7613; b: 1.3799; c: 2.326; w2: 31.82; w3: 6.965),
          (a: 4.9494; b: 1.6007; c: 2.576; w2: 63.66; w3: 9.925));

var
 q : byte;

begin
 case aTNiveau of
  90: q := 1;
  95: q := 2;
  98: q := 3;
  99: q := 4;
  else q := 2; //nur wegen uebersichtlichkeit
 end; //case aTNiveau of

 case aCount of
  0,1: result :=  0;
    2: result := abcAr[q].w2;
    3: result := abcAr[q].w3;
    else result := abcAr[q].a /(aCount -1 -abcAr[q].b) +abcAr[q].c;
 end; //case aCount of
end; //function TmmGraphData.Stat_TVALUE
//-----------------------------------------------------------------------------
function TmmGraphData.Stat_QVALUE(aCount: longint; aStdDev: ValueType; aTNiveau: byte): ValueType;
begin
 result := 0;
 if aCount < 2 then exit;
 result := Stat_TVALUE(aCount,aTNiveau) *aStdDev /sqrt(aCount);
end; //function TmmGraphData.Stat_QVALUE
//-----------------------------------------------------------------------------
procedure TmmGraphData.CalcXMinMax;
var
 i : integer;

begin
 FXMin :=  1e99;
 FXMax := -1e99;

 for i := low(data.values) to high(data.values) do
 begin
  FXMin := math.Min(Data.Values[i].XValue.Value,FXMin);
  FXMax := math.Max(Data.Values[i].XValue.Value,FXMax);
 end; //for i := low(data.values) to high(data.values) do
end; //procedure TmmGraphData.CalcXMinMax
//-----------------------------------------------------------------------------
procedure TmmGraphData.ResetSUM;
var
 i : integer;

begin
 for i := low(Data.values) to high(Data.values) do
 begin
  Data.Values[i].YValue[0].IsNull := true;
  Data.Values[i].YValue[0].Value := 0;
 end; //for i := low(Data.values) to high(Data.values) do
end; //procedure TmmGraphData.ResetSUM
//-----------------------------------------------------------------------------
procedure TmmGraphData.SetXMinMax;
const
 cOverSize = 10; // = %

var
 dx : double;

begin
 CalcXMinMax;

 if FXMin > FXMax then
 begin
  FXMin := 0;
  FXMax := 1;
 end; //if FXMin > FXMax then

 dx := (FXMax -FXMin) *cOverSize /100 /2;
 if dx > cEpsilon then
 begin
  FXMin := FXMin -dx;
  FXMax := FXMax +dx;
 end //if dx > cEpsilon then
 else
 begin
  FXMin := FXMin -1;
  FXMax := FXMax +1;
 end; //else if dx > cEpsilon then
end; //procedure TmmGraphData.SetXMinMax
//-----------------------------------------------------------------------------
procedure TmmGraphData.AddToSUM(aRow: integer);
var
 i : integer;

begin
 for i := low(Data.values) to high(Data.values) do
  if not Data.Values[i].YValue[aRow].IsNull then
  begin
   Data.Values[i].YValue[0].IsNull := false;
   Data.Values[i].YValue[0].Value := Data.Values[i].YValue[0].Value +Data.Values[i].YValue[aRow].Value;
  end; //if not Data.Values[i].YValue[aRow].IsNull then
end; //procedure TmmGraphData.ResetSUMRow
//-----------------------------------------------------------------------------
procedure TmmGraphData.CalcStatData;
var
 xSeries : byte;
 count,
 i,j     : longint;
 xData   : array of ValueType;

begin
 fillchar(Data.Statistik,sizeof(Data.Statistik),0);
 try
  for xSeries := 0 to FNumSeries do
  begin
   xData := nil;

   for i := low(Data.values) to high(Data.values) do
    if not Data.Values[i].YValue[xSeries].IsNull then
    begin
     j := high(xData) +1;
     setlength(xData,j+1);
     xData[j] := Data.Values[i].YValue[xSeries].Value;
    end; //if not Data.Values[i].YValue[xDataRow].IsNull then

   count := round(Stat_COUNT(xData));
   Data.Statistik[xSeries,stCount]    := count;
   Data.Statistik[xSeries,stMin]      := Stat_MIN(xData);
   Data.Statistik[xSeries,stMax]      := Stat_MAX(xData);
   Data.Statistik[xSeries,stMean]     := Stat_MEAN(xData);
   Data.Statistik[xSeries,stStdDev]   := Stat_STDDEV(xData);
   Data.Statistik[xSeries,stVariance] := Stat_VARIANCE(xData);
   Data.Statistik[xSeries,stCV]       := Stat_CV(Data.Statistik[xSeries,stMean],Data.Statistik[xSeries,stStdDev]);
   Data.Statistik[xSeries,stQ90]      := Stat_QVALUE(Count,Data.Statistik[xSeries,stStdDev],90);
   Data.Statistik[xSeries,stQ95]      := Stat_QVALUE(Count,Data.Statistik[xSeries,stStdDev],95);
   Data.Statistik[xSeries,stQ99]      := Stat_QVALUE(Count,Data.Statistik[xSeries,stStdDev],99);
  end; //with Data.StatData[xDataRow] do
 finally
  xData := nil;
 end; //try..finally
end; //procedure TmmGraphData.CalcStatData
//-----------------------------------------------------------------------------
function TmmGraphData.FindPoint(aXValue: ValueType; aFindType: eFindTypeT): longint;
var
 i : longint;

begin
 //data must be sorted by xvalues,
 if (SortRow <> 0) or (SortOrder <> soAscending) then SortByRow(0,soAscending);

 result := -1;

 case aFindType of
  ftExact: begin
   for i := low(data.values) to high(data.values) do
    if abs(Data.Values[i].XValue.Value -aXValue) < cEpsilon then
    begin
     result := i;
     break;
    end; //if abs(Data.Values[i].XValue.Value -aXValue) < cEpsilon then
  end; //ftExact

  ftPrior: begin
   for i := high(data.values) downto low(data.values) do
    if Data.Values[i].XValue.Value < aXValue then
    begin
     result := i;
     break;
    end; //if Data.Values[i].XValue.Value < aXValue then
  end; //ftPrior

  ftNext: begin
   for i := low(data.values) to high(data.values) do
    if Data.Values[i].XValue.Value > aXValue then
    begin
     result := i;
     break;
    end; //if Data.Values[i].XValue.Value > aXValue then
  end; //ftNext
 end; //case aFindType of
end; //function TmmGraphData.FindPoint


//-----------------------------------------------------------------------------


//TmmGraphColors ***
constructor TmmGraphColors.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FPanelBackground := clLightGray1;
end; //constructor TmmGraphColors.Create
//-----------------------------------------------------------------------------
procedure TmmGraphColors.SetGraphColors(Index: Integer; Value: TColor);
begin
 case Index of
  0: if Value <> FPanelBackground then FPanelBackground := Value;
 end; //case Index of

 if Index in [0] then
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
end; //procedure TmmGraphColors.SetGraphColors


//-----------------------------------------------------------------------------


//TmmGraphMargin ***
constructor TmmGraphMargin.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FBottom := 5;
 FGraph_Legend := 10;
 FLeft := 5;
 FRight := 5;
 FTitle_Graph := 10;
 FTop := 5;
end; //constructor TmmGraphMargin.Create
//-----------------------------------------------------------------------------
procedure TmmGraphMargin.SetGraphMargin(Index: Integer; Value: integer);
begin
 case Index of
  0: if Value <> FBottom then FBottom := Value;
  1: if Value <> FGraph_Legend then FGraph_Legend := Value;
  2: if Value <> FLeft then FLeft := Value;
  3: if Value <> FRight then FRight := Value;
  4: if Value <> FTitle_Graph then FTitle_Graph:= Value;
  5: if Value <> FTop then FTop := Value; 
 end; //case Index of

 if Index in [0..5] then
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
end; //procedure TmmGraphMargin.SetGraphMargin


//-----------------------------------------------------------------------------


//TmmCustomGraphArea ***
constructor TmmCustomGraphArea.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;

 FAutoScaling := true;
 FBackgroundColor := clLightGray1;
 FBorderStyle := bsSingle;
 FDecimals := 1;
 FDefaultMinY := 0;
 FDefaultMaxY := 100;
 FFormatStr := ',0.0';
 FFont := TFont.Create;
 FFont.Style := [];
 FFont.Size := 8;
 FFont.Name := 'Arial';
 FFont.Color := clBlack;
 FHeight := 60;
 FPosition := lpRight;
 FTitleMargin := 5;
 FTitleOrientation := toUp;
 FVisible := true;
 FWidth := 100;
 FXValueFormat := '0';
 FXAxisType := xaFixed;
 FXAxisTextAngle := 0;
 FRefreshOnSizeChange := [];
end; //constructor TmmCustomGraphArea.Create
//-----------------------------------------------------------------------------
destructor TmmCustomGraphArea.Destroy;
begin
 FFont.Free;
 inherited Destroy;
end; //destructor TmmCustomGraphArea.Destroy
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetAutoScaling(value: boolean);
begin
 if Value <> FAutoScaling then
 begin
  FAutoScaling := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FAutoScaling then
end; //procedure TmmCustomGraphArea.SetAutoScaling
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetBackgroundColor(value: TColor);
begin
 if Value <> FBackgroundColor then
 begin
  FBackgroundColor := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FBackgroundColor then
end; //procedure TmmCustomGraphArea.SetBackgroundColor
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetBorderStyle(value: TBorderStyle);
begin
 if Value <> FBorderStyle then
 begin
  FBorderStyle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FBorderStyle then
end; //procedure TmmCustomGraphArea.SetBorderStyle
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetDecimals(value: integer);
var
 i : integer;

begin
 if Value <> FDecimals then
 begin
  FDecimals := value;
  if FDecimals < cMinDecimals then FDecimals := cMinDecimals;
  if FDecimals > cMaxDecimals then FDecimals := cMaxDecimals;
  if FDecimals > 0 then
  begin
   FFormatStr := ',0.';
   for i := 1 to FDecimals do
    FFormatStr := FFormatStr +'0';
  end //if FDecimals > 0 then
  else FFormatStr := ',0';
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FHeight then
end; //procedure TmmCustomGraphArea.SetDecimals
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetDefaultMinY(value: double);
begin
 if Value <> FDefaultMinY then
 begin
  FDefaultMinY := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FDefaultMinY then
end; //procedure TmmCustomGraphArea.SetDefaultMinY
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetDefaultMaxY(value: double);
begin
 if Value <> FDefaultMaxY then
 begin
  FDefaultMaxY := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FDefaultMaxY then
end; //procedure TmmCustomGraphArea.SetDefaultMaxY
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetFont(value: TFont);
begin
 if Value <> FFont then
 begin
  FFont.Assign(value);
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FFont then
end; //procedure TmmCustomGraphArea.SetFont
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetHeight(value: integer);
begin
 if Value <> FHeight then
 begin
  FHeight := value;
  if srHeight in FRefreshOnSizeChange then
   if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FHeight then
end; //procedure TmmCustomGraphArea.SetHeight
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetPosition(value: eLegendPosT);
begin
 if Value <> FPosition then
 begin
  FPosition := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FPosition then
end; //procedure TmmCustomGraphArea.SetPosition
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetTitle(value: string);
begin
 if Value <> FTitle then
 begin
  FTitle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FTitle then
end; //procedure TmmCustomGraphArea.SetTitle
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetTitleMargin(value: integer);
begin
 if Value <> FTitleMargin then
 begin
  FTitleMargin := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FTitleMargin then
end; //procedure TmmCustomGraphArea.SetTitleMargin
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetTitleOrientation(value: eTitleOrientationT);
begin
 if Value <> FTitleOrientation then
 begin
  FTitleOrientation := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FTitleOrientation then
end; //procedure TmmCustomGraphArea.SetTitleOrientation
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetVisible(value: boolean);
begin
 if Value <> FVisible then
 begin
  FVisible := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FVisible then
end; //procedure TmmCustomGraphArea.SetVisible
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetWidth(value: integer);
begin
 if Value <> FWidth then
 begin
  FWidth := value;
  if srWidth in FRefreshOnSizeChange then
   if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FWidth then
end; //procedure TmmCustomGraphArea.SetWidth
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetXAxisTextAngle(value: integer);
begin
 if Value > 90 then Value := 90;
 if Value < 0 then Value := 0;
 if Value <> FXAxisTextAngle then
 begin
  FXAxisTextAngle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FXAxisTextAngle then
end; //procedure TmmCustomGraphArea.SetXAxisTextAngle
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetXValueFormat(value: string);
begin
 if Value <> FXValueFormat then
 begin
  FXValueFormat := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FXValueFormat then
end; //procedure TmmCustomGraphArea.SetXValueFormat
//-----------------------------------------------------------------------------
procedure TmmCustomGraphArea.SetXAxisType(value: eXAxisTypeT);
begin
 if Value <> FXAxisType then
 begin
  FXAxisType := value;
  case Value of
   xaFixed: FXValueFormat := '';
   xaFloat: FXValueFormat := ',0.0';
   xaDateTime: FXValueFormat := '';
  end; //case Value of

  TmmGraph(FControl).SetXScale(TmmGraph(FControl).Data.XMin,TmmGraph(FControl).Data.XMax);
  TmmGraph(FControl).FData.SetXMinMax;

  if (FXAxisType = xaFixed) and (TmmGraph(FControl).GetNumOfBarSeries > 0)
   then TmmGraph(FControl).RightYAxis.Visible := true
   else TmmGraph(FControl).RightYAxis.Visible := false;

  //error in vcl, pagesize can't be altered...
  TmmGraph(FControl).FScrollBar.Position := 0;
  TmmGraph(FControl).FScrollBar.Max := MaxInt;
  TmmGraph(FControl).FScrollBar.PageSize := 1;

  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FXAxisType then
end; //procedure TmmCustomGraphArea.SetXAxisType


//-----------------------------------------------------------------------------


//TmmGraphTitles ***
constructor TmmGraphTitles.Create(Control: TControl);
begin
 inherited Create(Control);
 FControl := Control;
 FAlignment := taCenter;
 FHeight := 60;
 FSubTitle := '';
 FSubTitleFont := TFont.Create;
 FSubTitleFont.Style := [];
 FSubTitleFont.Size := 8;
 FSubTitleFont.Name := 'Arial';
 FSubTitleFont.Color := clBlack;
end; //constructor TmmGraphTitles.Create
//-----------------------------------------------------------------------------
destructor TmmGraphTitles.Destroy;
begin
 FSubTitleFont.Free;
 inherited Destroy;
end; //destructor TmmGraphTitles.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraphTitles.SetAlignment(value: TAlignment);
begin
 if Value <> FAlignment then
 begin
  FAlignment := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FAlignment then
end; //procedure TmmGraphTitles.SetAlignment
//-----------------------------------------------------------------------------
procedure TmmGraphTitles.SetSubTitle(value: string);
begin
 if Value <> FSubTitle then
 begin
  FSubTitle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FSubTitle then
end; //procedure TmmGraphTitles.SetSubTitle
//-----------------------------------------------------------------------------
procedure TmmGraphTitles.SetSubTitleFont(value: TFont);
begin
 if Value <> FSubTitleFont then
 begin
  FSubTitleFont.Assign(value);
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FSubTitleFont then
end; //procedure TmmGraphTitles.SetFont


//-----------------------------------------------------------------------------


//TmmCustomGraphGrid ***
constructor TmmCustomGraphGrid.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FColor := clSilver;
 FForeground := false;
 FGridType := hgLeftAxis;
 FStyle := psSolid;
 FVisible := true;
 FWidth := 1;
end; //constructor TmmCustomGraphGrid.Create
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetColor(Value: TColor);
begin
 if Value <> FColor then
 begin
  FColor := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FColor then
end; //procedure TmmCustomGraphGrid.SetColor
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetForeground(value: boolean);
begin
 if Value <> FForeground then
 begin
  FForeground := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FGridType then
end; //procedure TmmCustomGraphGrid.SetForeground
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetGridType(Value: eHorzGridT);
begin
 if Value <> FGridType then
 begin
  FGridType := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FGridType then
end; //procedure TmmCustomGraphGrid.SetGridType
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetStyle(Value: TPenStyle);
begin
 if Value <> FStyle then
 begin
  FStyle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FStyle then
end; //procedure TmmCustomGraphGrid.SetStyle
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetVisible(Value: boolean);
begin
 if Value <> FVisible then
 begin
  FVisible := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FVisible then
end; //procedure TmmCustomGraphGrid.SetVisible
//-----------------------------------------------------------------------------
procedure TmmCustomGraphGrid.SetWidth(Value: integer);
begin
 if Value <> FWidth then
 begin
  FWidth := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FWidth then
end; //procedure TmmCustomGraphGrid.SetWidth


//-----------------------------------------------------------------------------


//TmmGraphBars ***
constructor TmmGraphBars.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FMinBarsPerPage := 0;
 FDefaultBarWidth := 6;
 MinBarWidth := 4;
 FGap := 2;
 FStyle := bsSide;
end; //constructor TmmGraphBars.Create
//-----------------------------------------------------------------------------
procedure TmmGraphBars.SetDefaultBarWidth(Value: integer);
begin
 if Value < 1 then Value := 1;
 if Value < FMinBarWidth then Value := FMinBarWidth;
 if Value > cMaxBarWidth then Value := cMaxBarWidth;
 if Value <> FDefaultBarWidth then
 begin
  FDefaultBarWidth := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FDefaultBarWidth then
end; //procedure TmmGraphBars.SetDefaultBarWidth
//-----------------------------------------------------------------------------
procedure TmmGraphBars.SetGap(Value: integer);
begin
 if Value <> FGap then
 begin
  FGap := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FGap then
end; //procedure TmmGraphBars.SetGap
//-----------------------------------------------------------------------------
procedure TmmGraphBars.SetMinBarsPerPage(Value: integer);
begin
 if Value < 0 then Value := 0;
 if Value > cMaxMinBarsPerPage then Value := cMaxMinBarsPerPage;

 if Value <> FMinBarsPerPage then
 begin
  FMinBarsPerPage := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FMinBarsPerPage then
end; //procedure TmmGraphBars.SetMinBarsPerPage
//-----------------------------------------------------------------------------
procedure TmmGraphBars.SetMinBarWidth(Value: integer);
begin
 if Value < 1 then Value := 1;
 if Value > FDefaultBarWidth then Value := FDefaultBarWidth;

 if Value <> FMinBarWidth then
 begin
  FMinBarWidth := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FMinBarWidth then
end; //procedure TmmGraphBars.SetMinBarWidth
//-----------------------------------------------------------------------------
procedure TmmGraphBars.SetStyle(Value: eBarStyleT);
begin
 if Value <> FStyle then
 begin
  FStyle := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FStyle then
end; //procedure TmmGraphBars.SetStyle


//-----------------------------------------------------------------------------


//TmmGraphLines ***
constructor TmmGraphLines.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FMinPointGap := 8;
 FSmoothLines := true;
end; //constructor TmmGraphLines.Create
//-----------------------------------------------------------------------------
procedure TmmGraphLines.SetMinPointGap(Value: integer);
begin
 if Value < 1 then Value := 1;
 if Value > 50 then Value := 50;

 if Value <> FMinPointGap then
 begin
  FMinPointGap := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FMinPointGap then
end; //procedure TmmGraphLines.SetMinPointGap
//-----------------------------------------------------------------------------
procedure TmmGraphLines.SetSmoothLines(Value: boolean);
begin
 if Value <> FSmoothLines then
 begin
  FSmoothLines := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FDefaultBarWidth then
end; //procedure TmmGraphLines.SetSmoothLines


//-----------------------------------------------------------------------------


//TmmGraphBars ***
constructor TmmSelection.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;
 FEnabled := true;
 FFixedColor := clBlue;
 FStyle := ssInverted;
end; //constructor TmmSelection.Create
//-----------------------------------------------------------------------------
procedure TmmSelection.SetButton(const Value: TMouseButton);
begin
  fButton := Value;
end;
//-----------------------------------------------------------------------------
procedure TmmSelection.SetEnabled(value: boolean);
begin
 if Value <> FEnabled then
 begin
  FEnabled := value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FEnabled then
end; //procedure TmmSelection.SetEnabled
//-----------------------------------------------------------------------------
procedure TmmSelection.SetFixedColor(value: TColor);
begin
 if Value <> FFixedColor then
 begin
  FFixedColor:= value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FFixedColor then
end; //procedure TmmSelection.SetFixedColor
//-----------------------------------------------------------------------------
procedure TmmSelection.SetShift(const Value: TShiftState);
begin
  fShift := Value;
end;
//-----------------------------------------------------------------------------
procedure TmmSelection.SetStyle(Value: eSelectionStyleT);
begin
 if Value <> FStyle then
 begin
  FStyle:= value;
  if TmmGraph(FControl).FCanRefresh then FControl.Refresh;
 end; //if Value <> FEnabled then
end; //procedure TmmSelection.SetStyle


//-----------------------------------------------------------------------------


//TmmGraphScrollButton ***
constructor TmmGraphScrollButton.Create(AOwner: TComponent; bTyp: eScrollBtnT);
begin
 inherited Create(AOwner);
 Width := cScrollBtnWidth;
 Height := cScrollBtnHeight;
 Flat := true;
 ShowHint := true;
 if bTyp = sbInc then Glyph.LoadFromResourceName(HInstance,'SCROLL_UP')
                 else Glyph.LoadFromResourceName(HInstance,'SCROLL_DOWN');
 NumGlyphs := 1;
end; //constructor TmmGraphScrollButton.Create
//-----------------------------------------------------------------------------
destructor TmmGraphScrollButton.Destroy;
begin
 if FRepeatTimer <> nil then FRepeatTimer.Free;
 inherited Destroy;
end; //destructor TmmGraphScrollButton.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraphScrollButton.MouseDown(Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
 inherited MouseDown(Button,Shift,X,Y);
 if FRepeatTimer = nil then FRepeatTimer := TTimer.Create(Self);
 FRepeatTimer.OnTimer := TimerExpired;
 FRepeatTimer.Interval := cInitRepeatPause;
 FRepeatTimer.Enabled  := True;
end; //procedure TmmGraphScrollButton.MouseDown
//-----------------------------------------------------------------------------
procedure TmmGraphScrollButton.MouseUp(Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
begin
 inherited MouseUp(Button,Shift,X,Y);
 if FRepeatTimer <> nil then FRepeatTimer.Enabled  := False;
end; //procedure TmmGraphScrollButton.MouseUp
//-----------------------------------------------------------------------------
procedure TmmGraphScrollButton.TimerExpired(Sender: TObject);
begin
 FRepeatTimer.Interval := cRepeatPause;
 if (FState = bsDown) and MouseCapture then
 begin
  try
   Click;
  except
   FRepeatTimer.Enabled := False;
   raise;
  end; //try..except
 end; //if (FState = bsDown) and MouseCapture then
end; //procedure TmmGraphScrollButton.TimerExpired


//-----------------------------------------------------------------------------


//TmmGraph ***
constructor TmmGraph.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);

 FPrintFooter := TPrintFooter.Create(self);
 FPrintHeader := TPrintHeader.Create(self);

 BevelInner := bvNone;
 BevelOuter := bvNone;
 BevelWidth := 1;
 BorderStyle := bsNone;
 BorderWidth := 0;
 Caption := ' ';
 DoubleBuffered := true;
 Height := 300;
 Visible := true;
 Width := 500;

 FCanRefresh := true;

 YStretch[yaxLeft] := 1;
 YStretch[yaxRight] := 1;
 FShowScrollButtons := true;
 IsSelecting := false;
 IsPrinting := false;

 FSelectedPoint := -1;
 FSelectedRow := -1;
 FReadOnly := false;

 FChartArea := TmmGraphChartArea.Create(Self);
 FChartArea.BorderStyle := bsSingle;
 FChartArea.BackgroundColor := clLightGray2;

 FColors := TmmGraphColors.Create(self);
 FData := TmmGraphData.Create(self);
 FMargin := TmmGraphMargin.Create(self);
 FLeftYAxis := TmmGraphLeftYAxis.Create(Self);
 FLeftYAxis.Width := 60;
 FLeftYAxis.BorderStyle := bsNone;
 FLeftYAxis.RefreshOnSizeChange := [srWidth];

 FLegend := TmmGraphLegend.Create(Self);
 FLegend.Width := 150;

 FLines := TmmGraphLines.Create(Self);

 FRightYAxis := TmmGraphRightYAxis.Create(self);
 FRightYAxis.BorderStyle := bsNone;
 FRightYAxis.Width := 60;
 FRightYAxis.RefreshOnSizeChange := [srWidth];

 FXAxis := TmmGraphXAxis.Create(self);
 FXAxis.BorderStyle := bsNone;
 FXAxis.Height := 80;
 FXAxis.RefreshOnSizeChange := [srHeight];

 FTitles := TmmGraphTitles.Create(self);
 FTitles.RefreshOnSizeChange := [srHeight];

 FHorzGrid := TmmGraphHorzGrid.Create(Self);
 FVertGrid := TmmGraphVertGrid.Create(Self);

 FSeries := TmmGraphSeries.Create(Self);
 FSelection := TmmSelection.Create(Self);
 FStatistikLines := TmmGraphStatistikLines.Create(Self);

 {$ifdef UsePopup}
 FPopUp := TPopUpMenu.Create(self);
 FPopUp.OnPopUp := FPopUpOnPopUp;
 CreatePopUpMenu;
 {$endif}

 FBars := TmmGraphBars.Create(Self);

 FPaintBox := TmmPaintBox.Create(Self);
 FPaintBox.Parent := Self;
 FPaintBox.OnPaint := FPaintBoxPaint;
 FPaintBox.Align := alClient;
 {$ifdef UsePopup}
 FPaintBox.PopUpMenu := FPopUp;
 {$endif}

 FhwInfo := THintWindow.Create(FPaintBox);
 FhwInfo.Color := Application.HintColor; //clWhite;

 //gummiband
 FPaintBox.OnMouseDown := FPaintBoxMouseDown;
 FPaintBox.OnMouseUp := FPaintBoxMouseUp;
 FPaintBox.OnMouseMove := FPaintBoxMouseMove;

 FScrollBar := TmmScrollBar.Create(Self);
 FScrollBar.Parent := Self;
 FScrollBar.TabStop := false;
 FScrollBar.Visible := false; //***
 FScrollBar.OnChange := FScrollBar1Change;
 FScrollBar.OnScroll := FScrollBarScroll;

 FLeftBtnInc := TmmGraphScrollButton.Create(Self,sbInc);
 FLeftBtnInc.Parent := Self;
 FLeftBtnInc.OnClick := FScrollBtnClick;

 FLeftBtnDec := TmmGraphScrollButton.Create(Self,sbDec);
 FLeftBtnDec.Parent := Self;
 FLeftBtnDec.OnClick := FScrollBtnClick;

 FRightBtnInc := TmmGraphScrollButton.Create(Self,sbInc);
 FRightBtnInc.Parent := Self;
 FRightBtnInc.OnClick := FScrollBtnClick;

 FRightBtnDec := TmmGraphScrollButton.Create(Self,sbDec);
 FRightBtnDec.Parent := Self;
 FRightBtnDec.OnClick := FScrollBtnClick;

 XScale.actMin := 1; //min > max => indicator for initilization
 XScale.actMax := 0;

 FObj := nil;
 FMemoINI := TMemoINI.Create(aOwner);
end; //constructor TmmGraph.Create
//-----------------------------------------------------------------------------
destructor TmmGraph.Destroy;
begin
 FBars.Free;
 FChartArea.Free;
 FColors.Free;
 FData.Free;
 FMargin.Free;
 FLeftYAxis.Free;
 FLegend.Free;
 FLines.Free;
 FRightYAxis.Free;
 FTitles.Free;
 FXAxis.Free;
 FHorzGrid.Free;
 FhwInfo.Free;
 FSeries.Free;
 FSelection.Free;
 FStatistikLines.Free;
 FVertGrid.Free;

 FObj := nil;
 FMemoINI.Free;

 inherited Destroy;
end; //destructor TmmGraph.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraph.BeginUpdate;
begin
 FCanRefresh := false;                      
end; //procedure TmmGraph.BeginUpdate
//-----------------------------------------------------------------------------
procedure TmmGraph.EndUpdate;
begin
 FCanRefresh := true;
 Refresh;
end; //procedure TmmGraph.EndUpdate
//-----------------------------------------------------------------------------
function TmmGraph.GetLegendItemWidth(aCanvas: TCanvas): integer;
const
 cMaxScalingText = ' (1/2000x)';

var
 i,xLen : integer;

begin
 result := 0;
 aCanvas.Font := FLegend.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 for i := 1 to Data.NumSeries do
 begin
  xLen := aCanvas.TextHeight('H')
         +GetPrnFactX(cDiff_LegendBorder)
         +GetPrnFactX(2 *cDiff_LegendText)
         +aCanvas.TextWidth(Series.Name[i] +cMaxScalingText);
  result := math.max(xLen,result);
 end; //for i := 1 to Data.NumSeries do

 if result = 0 then result := GetPrnFactX(200);
end; //function TmmGraph.GetLegendItemWidth
//-----------------------------------------------------------------------------
function TmmGraph.GetLegendItemHeight(aCanvas: TCanvas): integer;
begin
 aCanvas.Font := FLegend.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 result := aCanvas.TextHeight('H') +GetPrnFactY(cDiff_LegendItems);
end; //function TmmGraph.GetLegendItemHeight
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcBarWidth;
var
 xMinBW : integer;

begin
 xMinBW := GetPrnFactX(math.max(NumOfBarSeries,FBars.MinBarWidth));
 BarWidth := GetPrnFactX(FBars.DefaultBarWidth);

 case FXAxis.XAxisType of
  xaFixed: begin
   if FBars.MinBarsPerPage > 0 then
   begin
    if FBars.Style <> bsSide then xMinBW := FBars.MinBarWidth;
    BarWidth := (FChartArea.Width div FBars.MinBarsPerPage) -GetPrnFactX(FBars.Gap);
    if BarWidth < xMinBW then BarWidth := xMinBW;
    if (NumOfBarSeries > 1) and (FBars.Style = bsSide) then BarWidth := BarWidth div NumOfBarSeries;
    if BarWidth > GetPrnFactX(FBars.DefaultBarWidth) then BarWidth := GetPrnFactX(FBars.DefaultBarWidth);
   end //if FBars.MinBarsPerPage > 0 then
   else BarWidth := GetPrnFactX(FBars.DefaultBarWidth);

   if (NumOfBarSeries > 0) and (FBars.Style = bsSide) then
   begin
    BarWidth := math.max(BarWidth,FBars.MinBarWidth);
    BarWidth := BarWidth *NumOfBarSeries;
   end; //if (NumOfBarSeries > 1) and (FBars.Style = bsSide) then
  end; //xaFixed
 end; //case FXAxis.XAxisType of
end; //procedure TmmGraph.CalcBarWidth
//-----------------------------------------------------------------------------
procedure TmmGraph.SortSeriesDlg;
var
 i        : integer;
 xSortDlg : TmmGraphSortSeries;

begin
  if FData.GetNumberOfData < 1 then exit;
//  xSortDlg := TmmGraphSortSeries.Create(Application);
  xSortDlg := TmmGraphSortSeries.Create(Owner);
  try
   xSortDlg.SortOrder := Data.SortOrder;
   xSortDlg.SortRow := Data.SortRow;
   xSortDlg.AddName(LTrans(cXAxisStr));

   for i := 1 to Data.NumSeries do
    xSortDlg.AddName(Series.Name[i]);

   if xSortDlg.Execute then
   begin
    Data.SortByRow(xSortDlg.SortRow,xSortDlg.SortOrder);
    Refresh;
   end; //if xSortDlg.Execute then
  finally
   xSortDlg.Free;
  end; //try..finally
end; //procedure TmmGraph.SortSeriesDlg
//-----------------------------------------------------------------------------
procedure TmmGraph.EditOptionsDlg;
var
 xOptionsDlg : TmmGraphOptions;

begin
 if FData.GetNumberOfData < 1 then exit;
 xOptionsDlg := TmmGraphOptions.Create(Application);
 try
  xOptionsDlg.BarStyle := FBars.Style;
  xOptionsDlg.AutoScaling[yaxLeft] := FLeftYAxis.AutoScaling;
  xOptionsDlg.DefaultMinY[yaxLeft] := FLeftYAxis.DefaultMinY;
  xOptionsDlg.DefaultMaxY[yaxLeft] := FLeftYAxis.DefaultMaxY;

  xOptionsDlg.AutoScaling[yaxRight] := FRightYAxis.AutoScaling;
  xOptionsDlg.DefaultMinY[yaxRight] := FRightYAxis.DefaultMinY;
  xOptionsDlg.DefaultMaxY[yaxRight] := FRightYAxis.DefaultMaxY;
  if xOptionsDlg.Execute then
  begin
   FBars.Style := xOptionsDlg.BarStyle;
   FLeftYAxis.AutoScaling := xOptionsDlg.AutoScaling[yaxLeft];
   FLeftYAxis.DefaultMinY := xOptionsDlg.DefaultMinY[yaxLeft];
   FLeftYAxis.DefaultMaxY := xOptionsDlg.DefaultMaxY[yaxLeft] ;

   FRightYAxis.AutoScaling := xOptionsDlg.AutoScaling[yaxRight];
   FRightYAxis.DefaultMinY := xOptionsDlg.DefaultMinY[yaxRight];
   FRightYAxis.DefaultMaxY := xOptionsDlg.DefaultMaxY[yaxRight];

   //Refresh;
  end; //if xOptionsDlg.Execute then
 finally
  xOptionsDlg.Free;
 end; //try..finally
end; //procedure TmmGraph.EditOptionsDlg
//-----------------------------------------------------------------------------
procedure TmmGraph.miEditSeriesClick(Sender: TObject);
begin
 EditSeriesDlg;
end; //procedure TmmGraph.miEditSeriesClick
//-----------------------------------------------------------------------------
procedure TmmGraph.miSortSeriesClick(Sender: TObject);
begin
 SortSeriesDlg;
end; //procedure TmmGraph.miSortSeriesClick
//-----------------------------------------------------------------------------
procedure TmmGraph.miHideLegendClick(Sender: TObject);
begin
 FLegend.Visible := false;
end; //procedure TmmGraph.miHideLegendClick
//-----------------------------------------------------------------------------
procedure TmmGraph.miShowLegendClick(Sender: TObject);
begin
 FLegend.Visible := true;
end; //procedure TmmGraph.miShowLegendClick
//-----------------------------------------------------------------------------
procedure TmmGraph.miHideRightYAxisClick(Sender: TObject);
begin
 FRightYAxis.Visible := false;
end; //procedure TmmGraph.miHideRightYAxisClick
//-----------------------------------------------------------------------------
procedure TmmGraph.miShowRightYAxisClick(Sender: TObject);
begin
 FRightYAxis.Visible := true;
end; //procedure TmmGraph.miShowRightYAxisClick
//-----------------------------------------------------------------------------
{$ifdef UsePopup}
procedure TmmGraph.CreatePopUpMenu;
var
 xMI : TMenuItem;

begin
 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Datenreihe bearbeiten...';
 xMI.Name := 'miEditSeries';
 xMI.OnClick := miEditSeriesClick;
 FPopUp.Items.Add(xMI);

 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Datenreihen sortieren...';
 xMI.Name := 'miSortSeries';
 xMI.OnClick := miSortSeriesClick;
 FPopUp.Items.Add(xMI);

 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Legende ausblenden';
 xMI.Name := 'miHideLegend';
 xMI.OnClick := miHideLegendClick;
 FPopUp.Items.Add(xMI);

 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Legende anzeigen';
 xMI.OnClick := miShowLegendClick;
 xMI.Name := 'miShowLegend';
 FPopUp.Items.Add(xMI);

 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Rechte Y-Achse ausblenden';
 xMI.Name := 'miHideRightYAxis';
 xMI.OnClick := miHideRightYAxisClick;
 FPopUp.Items.Add(xMI);

 xMI := TMenuItem.Create(Self);
 xMI.Caption := 'Rechte Y-Achse anzeigen';
 xMI.Name := 'miShowRightYAxis';
 xMI.OnClick := miShowRightYAxisClick;
 FPopUp.Items.Add(xMI);
end; //procedure TmmGraph.CreatePopUpMenu
{$endif}
//-----------------------------------------------------------------------------
function TmmGraph.InRect(aRect: trect; x,y: integer): boolean;
begin
 result := (x >= aRect.Left) and (x <= aRect.Right) and (y >= aRect.Top) and (y <= aRect.Bottom);
end; //function TmmGraph.InRect
//-----------------------------------------------------------------------------
{$ifdef UsePopup}
procedure TmmGraph.FPopUpOnPopUp(Sender: TObject);
var
 rPos,
 mPos : TPoint;

//-------------
procedure SetItemVisible(MenuItemName: string; IsVisible: boolean);
var
 xMI : TMenuItem;

begin
 xMI := TMenuItem(FindComponent(MenuItemName));
 xMI.Visible := IsVisible;
end; //function GetMenuItem
//-------------

begin //main
 SetItemVisible('miEditSeries',not FReadOnly);
 SetItemVisible('miSortSeries',not FReadOnly);
 SetItemVisible('miHideLegend',false);
 SetItemVisible('miShowLegend',not FLegend.Visible and not FReadOnly);
 SetItemVisible('miHideRightYAxis',false);
 SetItemVisible('miShowRightYAxis',not FRightYAxis.Visible and not FReadOnly);

 if FReadOnly then exit;

 GetCursorPos(mPos);
 rPos := ScreenToClient(mPos);

 if GetDblClickItem(rPos.x,rPos.y) = dcLegend then
 begin
  SetItemVisible('miHideLegend',true);
  exit;
 end; //if GetDblClickItem(rPos.x,rPos.y) = dcLegend then

 if GetDblClickItem(rPos.x,rPos.y) = dcRightAxis then
 begin
  SetItemVisible('miHideRightYAxis',true);
  exit;
 end; //if GetDblClickItem(rPos.x,rPos.y) = dcRightAxis then
end; //procedure TmmGraph.FPopUpOnPopUp
{$endif}
//-----------------------------------------------------------------------------
procedure TmmGraph.FScrollBtnClick(Sender: TObject);
const
 cDiffFactor = 2;

var
 LastLeft,
 LastRight : double;

begin
 LastLeft := YStretch[yaxLeft];
 LastRight := YStretch[yaxRight];

 if Sender = FLeftBtnInc then YStretch[yaxLeft] := YStretch[yaxLeft] *cDiffFactor;
 if Sender = FLeftBtnDec then YStretch[yaxLeft] := YStretch[yaxLeft] /cDiffFactor;

 if Sender = FRightBtnInc then YStretch[yaxRight] := YStretch[yaxRight] *cDiffFactor;
 if Sender = FRightBtnDec then YStretch[yaxRight] := YStretch[yaxRight] /cDiffFactor;

 YStretch[yaxLeft] := math.max(YStretch[yaxLeft],1/1024);
 YStretch[yaxLeft] := math.min(YStretch[yaxLeft],1024);

 YStretch[yaxRight] := math.max(YStretch[yaxRight],1/1024);
 YStretch[yaxRight] := math.min(YStretch[yaxRight],1024);

 if (LastLeft <> YStretch[yaxLeft]) or (LastRight <> YStretch[yaxRight]) then Refresh
                                                                         else MessageBeep(0);
end; //procedure TmmGraph.FScrollBtnClick
//-----------------------------------------------------------------------------
procedure TmmGraph.FScrollBar1Change(Sender: TObject);
begin
 if FScrollBar.Visible then Refresh;
end; //procedure TmmGraph.FScrollBar1Change
//-----------------------------------------------------------------------------
procedure TmmGraph.FScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
 case XAxis.XAxisType of
  xaFixed: begin
   
  end; //case XAxis.XAxisType of
  xaFloat,xaDatetime: begin
   XScale.actMin := XScale.absMin +XScale.absDX *ScrollPos /1000;
   XScale.actMax := XScale.actMin +XScale.actDX;
  end; //xaFloat,xaDatetime
 end; //case XAxis.XAxisType of
end; //procedure TmmGraph.FScrollBarScroll
//-----------------------------------------------------------------------------
procedure TmmGraph.SetReadOnly(value: boolean);
begin
 if Value <> FReadOnly then
 begin
  FReadOnly := value;
  FShowScrollButtons := not FReadOnly;
  Refresh;
 end; //if Value <> FShowScrollButtons then
end; //procedure TmmGraph.SetReadOnly
//-----------------------------------------------------------------------------
procedure TmmGraph.SetShowScrollButtons(value: boolean);
begin
 if Value <> FShowScrollButtons then
 begin
  FShowScrollButtons := value;
  Refresh;
 end; //if Value <> FShowScrollButtons then
end; //procedure TmmGraph.SetShowScrollButtons
//-----------------------------------------------------------------------------
function TmmGraph.GetNumOfBarSeries: integer;
var
 i : integer;

begin
 result := 0;
 for i := 1 to Data.NumSeries do
  if (Series.YAxis[i] = yaxRight) and Series.Visible[i] then inc(result);
end; //function TmmGraph.GetNumOfBarSeries
//-----------------------------------------------------------------------------
procedure TmmGraph.SetTextDirection(aCanvas: TCanvas; aAngle: single);
var
 LogRec: TLogFont;

begin
 GetObject(aCanvas.Font.Handle,sizeof(LogRec),@LogRec);
 LogRec.lfEscapement := round(aAngle *10);
 LogRec.lfOrientation := round(aAngle *10);
 aCanvas.Font.Handle := CreateFontIndirect(LogRec);
end; //procedure TmmGraph.SetTextDirection
//-----------------------------------------------------------------------------
function TmmGraph.GetHorzTextPos(x1,x2,TextWidth: integer; Alignment: TAlignment): integer;
begin
 case Alignment of
  taLeftJustify:  result := x1 +GetPrnFactX(5);
  taCenter:       result := x1 +(x2 -(x1 +TextWidth)) div 2;
  taRightJustify: result := x2 -TextWidth -GetPrnFactX(5);
 else
   result := x1 +GetPrnFactX(5);
 end; //case Alignment of
end; //function TmmGraph.GetHorzTextPos
//-----------------------------------------------------------------------------
function TmmGraph.GetVertTextPos(y1,y2,TextHeight: integer; YAlignment: eYAlignmentT): integer;
begin
 case YAlignment of
  tyaTop:    result := y1 +GetPrnFactY(5);
  tyaCenter: result := y1 +(y2 -(y1 +TextHeight)) div 2;
  tyaBottom: result := y1 -TextHeight -GetPrnFactY(5);
 else
   result := y1 +GetPrnFactY(5);
 end; //case YAlignment of
end; //function TmmGraph.GetVertTextPos
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcGraphCoordinates(aCanvas: TCanvas);
var
 xCols,
 xRows: integer;
// xPos,
// yPos  : integer;
 hText : integer;

begin
 FLeftYAxis.Left := GetPrnFactX(FMargin.Left) +GetPrnOffsetX;
 FLeftYAxis.Right := FLeftYAxis.Left +GetPrnFactX(FLeftYAxis.Width);

 FChartArea.Left := FLeftYAxis.Right;
 FChartArea.Right := GetPrnFactX(Width -FMargin.Right);

 if FRightYAxis.Visible then FChartArea.Right := FChartArea.Right -GetPrnFactX(FRightYAxis.Width);
 if FLegend.Visible and (FLegend.Position = lpRight) then FChartArea.Right := FChartArea.Right -GetPrnFactX(FLegend.Width +FMargin.Graph_Legend);

 if FRightYAxis.Visible then
 begin
  FRightYAxis.Left := FChartArea.Right;
  FRightYAxis.Right := FRightYAxis.Left +GetPrnFactX(FRightYAxis.Width);
 end; //if FRightYAxis.Visible then

 if FLegend.Visible and (FLegend.Position = lpRight) then
 begin
  if FRightYAxis.Visible then FLegend.Left := FRightYAxis.Right +GetPrnFactX(FMargin.Graph_Legend)
                         else FLegend.Left := FChartArea.Right +GetPrnFactX(FMargin.Graph_Legend); //??
  FLegend.Right := FLegend.Left +GetPrnFactX(FLegend.Width);
 end; //if FLegend.Visible and (FLegend.Position = lpRight) then

 FChartArea.Width := FChartArea.Right -FChartArea.Left;

 FTitles.Left := FChartArea.Left;
 FTitles.Right := FChartArea.Right;

 FXAxis.Left := FChartArea.Left;
 FXAxis.Right := FChartArea.Right;

 FTitles.Top := GetPrnFactY(FMargin.Top) +GetPrnOffsetY;
 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 hText := aCanvas.TextHeight('H');
 aCanvas.Font := FTitles.SubTitleFont;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 hText := hText +aCanvas.TextHeight('H');
 FTitles.Bottom := FTitles.Top +hText +GetPrnFactY(3*cDiff_LegendBorder);

 if not FTitles.Visible then FTitles.Bottom := FTitles.Top;

 if FLegend.Visible and (FLegend.Position = lpTop) then
 begin
  FLegend.Width := FChartArea.Right -FChartArea.Left;
  FLegend.Top := FTitles.Bottom +GetPrnFactY(FMargin.Title_Graph);
  FLegend.Left := FChartArea.Left;
  FLegend.Right := FChartArea.Right;

  xCols := FLegend.Width div GetLegendItemWidth(aCanvas);
  if xCols < 1 then xCols := 1;
  xRows := Data.NumSeries div xCols +1;
  if Data.NumSeries mod xCols = 0 then dec(xRows);

  FLegend.Height := xRows *GetLegendItemHeight(aCanvas) +GetPrnFactY(cDiff_LegendBorder);
  FLegend.Bottom := FLegend.Top +FLegend.Height;

  FChartArea.Top := FLegend.Bottom +GetPrnFactY(FMargin.Graph_Legend);
 end //if FLegend.Visible and (FLegend.Position = lpTop) then
 else FChartArea.Top := FTitles.Bottom +GetPrnFactY(FMargin.Title_Graph);

 FChartArea.Bottom := GetPrnFactY(Height) -GetPrnFactY(FMargin.Bottom +cScrollBarHeight +FXAxis.Height);
 FChartArea.Height := FChartArea.Bottom -FChartArea.Top;

 FLeftYAxis.Top := FChartArea.Top;
 FLeftYAxis.Bottom := FChartArea.Bottom;
 FLeftYAxis.Height := FLeftYAxis.Bottom -FLeftYAxis.Top;

 FRightYAxis.Top := FChartArea.Top;
 FRightYAxis.Bottom := FChartArea.Bottom;
 FRightYAxis.Height := FLeftYAxis.Height;

 if FLegend.Visible and (FLegend.Position = lpRight) then
 begin
  FLegend.Top := FChartArea.Top;
  FLegend.Bottom := FChartArea.Bottom;
  FLegend.Height := FRightYAxis.Height;
 end; //if FLegend.Visible and (FLegend.Position = lpRight) then

 FXAxis.Top := FChartArea.Bottom;
 FXAxis.Bottom := FXAxis.Top +GetPrnFactY(FXAxis.Height);
end; //procedure TmmGraph.CalcGraphCoordinates
//-----------------------------------------------------------------------------
procedure TmmGraph.EditSeriesDlg;
var
 i         : integer;
 xEditSerie: TmmGraphEditSeries;

begin
 if FData.GetNumberOfData < 1 then exit;
 xEditSerie := TmmGraphEditSeries.Create(Application);
 try
  xEditSerie.NumSeries := Data.NumSeries;

  for i := 1 to Data.NumSeries do
  begin
   xEditSerie.SeriesYScalingType[i] := Series.YScalingType[i];
   xEditSerie.SeriesUpperLimit[i] := Series.UpperLimit[i];
   xEditSerie.SeriesLowerLimit[i] := Series.LowerLimit[i];
   xEditSerie.SeriesColor[i] := Series.Color[i];
   xEditSerie.SeriesName[i] := Series.Name[i];
   xEditSerie.SeriesVisible[i] := Series.Visible[i];
   xEditSerie.SeriesYAxis[i] := Series.YAxis[i];
   xEditSerie.StatistikData[i] := Series.ShowStat[i];
  end; //for i := 1 to Data.NumSeries do

  if xEditSerie.Execute then
  begin
   for i := 1 to Data.NumSeries do
   begin
    Series.YScalingType[i] := xEditSerie.SeriesYScalingType[i];
    Series.UpperLimit[i] := xEditSerie.SeriesUpperLimit[i];
    Series.LowerLimit[i] := xEditSerie.SeriesLowerLimit[i];
    Series.Color[i] := xEditSerie.SeriesColor[i];
    Series.Name[i] := xEditSerie.SeriesName[i];
    Series.Visible[i] := xEditSerie.SeriesVisible[i];
    Series.YAxis[i] := xEditSerie.SeriesYAxis[i];
    Series.ShowStat[i] := xEditSerie.StatistikData[i];
   end; //for i := 1 to Data.NumSeries do
  end; //if xEditSerie.Execute then
 finally
  xEditSerie.Free;
 end; //try..finally
end; //procedure TmmGraph.EditSeriesDlg
//-----------------------------------------------------------------------------
procedure TmmGraph.SetBorderStyle(aCanvas: TCanvas; BorderStyle: TBorderStyle; BackgroundColor: TColor);
begin
 aCanvas.Pen.Width := 1;
 aCanvas.Pen.Style := psSolid;
 if IsPrinting then aCanvas.Brush.Color := clWhite
               else aCanvas.Brush.Color := BackgroundColor;
 if BorderStyle = bsSingle then aCanvas.Pen.Color := clBlack
                           else aCanvas.Pen.Color := aCanvas.Brush.Color;
 aCanvas.Brush.Style := bsSolid;
end; //procedure TmmGraph.SetBorderStyle
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawYAxis(aCanvas: TCanvas; aYAxis: eYAxisT);
const
 cMinDiff    = 6; //min-pixels between y-labels
 cTickLength = 4;

var
 xDrawHorzGrid : boolean;
 xTitleAngle,
 x1,x2,
 ySkip,
 dY,
 hText,
 wText,
 ticks,
 i,
 x,y           : integer;
 v             : double;
 xFrmStr,
 hstr          : string;
 xYLeg         : TmmCustomGraphArea;

begin
 if aYAxis = yaxLeft then xYLeg := FLeftYAxis
                     else xYLeg := FRightYAxis;

 if not xYLeg.Visible then exit;

 SetBorderStyle(aCanvas,xYLeg.BorderStyle,xYLeg.BackgroundColor);

 aCanvas.Font := xYLeg.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 wText := aCanvas.TextWidth(xYLeg.Title) div 2;
 hText := aCanvas.TextHeight('H');
 y := FChartArea.Top +(FChartArea.Height div 2);

 if xYLeg.TitleOrientation = toUp then xTitleAngle := 90
                                  else xTitleAngle := 270;

 SetTextDirection(aCanvas,xTitleAngle);

 if aYAxis = yaxLeft then
 begin
  if xYLeg.BorderStyle = bsNone then x := xYLeg.Right
                                else x := xYLeg.Right +1;
  aCanvas.Rectangle(xYLeg.Left,xYLeg.Top,x,xYLeg.Bottom);
  if xYLeg.TitleOrientation = toUp then x := xYLeg.Left +GetPrnFactX(xYLeg.TitleMargin)
                                   else x := xYLeg.Left +GetPrnFactX(xYLeg.TitleMargin) +hText;
  x1 := xYLeg.Right;
  x2 := xYLeg.Right -GetPrnFactX(cTickLength);
  xDrawHorzGrid := FHorzGrid.GridType in [hgLeftAxis,hgLeftRightAxis];
 end //if aYAxis = yaxLeft then
 else
 begin
  if xYLeg.BorderStyle = bsNone then x := xYLeg.Left
                                else x := xYLeg.Left -1;
  aCanvas.Rectangle(x,xYLeg.Top,xYLeg.Right,xYLeg.Bottom);
  if xYLeg.TitleOrientation = toUp then x := xYLeg.Right -GetPrnFactX(xYLeg.TitleMargin) -hText
                                   else x := xYLeg.Right -GetPrnFactX(xYLeg.TitleMargin);
  x1 := xYLeg.Left;
  x2 := xYLeg.Left +GetPrnFactX(cTickLength);
  xDrawHorzGrid := FHorzGrid.GridType = hgRightAxis;
 end; //else if aYAxis = yaxLeft then

 if xYLeg.TitleOrientation = toUp then y := y +wText
                                  else y := y -wText;

 aCanvas.TextOut(x,y,xYLeg.Title);
 SetTextDirection(aCanvas,0);

 hText := aCanvas.TextHeight('0') div 2;
 ticks := round((YScale.Max[aYAxis] -YScale.Min[aYAxis]) /YScale.Step[aYAxis]);
 ySkip := 1;

 dY := FChartArea.Height;
 y := ticks *(hText +GetPrnFactY(cMinDiff));
 if dY > 10 then
  while dY < (y /ySkip) do
   inc(ySkip);

 xFrmStr := xYLeg.FFormatStr;
 if YScale.Step[aYAxis] < 1 then
  if xYLeg.FDecimals < 1 then xFrmStr := ',0.0';
 if YScale.Step[aYAxis] < 0.1 then
  if xYLeg.FDecimals < 2 then xFrmStr := ',0.00';

 for i := 0 to ticks do
 begin
  v := YScale.Min[aYAxis] +(i*YScale.Step[aYAxis]);
  y := fy(v,aYAxis);

  aCanvas.Pen.Style := psSolid;
  aCanvas.Pen.Color := clBlack;
  aCanvas.Pen.Width := 1;

  if y >= fy(YScale.Max[aYAxis],aYAxis) then
  begin
   aCanvas.MoveTo(x1,y);
   aCanvas.LineTo(x2,y);

   if xDrawHorzGrid then
   begin
    aCanvas.Pen.Style := FHorzGrid.Style;
    aCanvas.Pen.Color := FHorzGrid.Color;
    aCanvas.Pen.Width := FHorzGrid.Width;

    //prevent drawing in/on first/last gridline
    if (y <> fy(YScale.Min[aYAxis],aYAxis)) and (y <> fy(YScale.Max[aYAxis],aYAxis)) then
    begin
     aCanvas.MoveTo(FChartArea.Left,y);
     aCanvas.LineTo(FChartArea.Right,y);
    end; //if (y <> fy(YScale.Min[aYAxis],aYAxis)) and (y <> fy(YScale.Max[aYAxis],aYAxis)) then
   end; //if xDrawHorzGrid then

   //hstr := formatfloat(xYLeg.FFormatStr,v);
   hstr := formatfloat(xFrmStr,v);
   if aYAxis = yaxLeft then x := FChartArea.Left -GetPrnFactX(cDiff_AxisTicksText) -aCanvas.TextWidth(hstr)
                       else x := FChartArea.Right +GetPrnFactX(cDiff_AxisTicksText);

   y := y -hText;

   if (i mod ySkip) = 0 then aCanvas.TextOut(x,y,hstr);
  end; //if y > fy(YScale.Min[yaxLeft],yaxLeft) then
 end; //for i := 0 to ticks do
end; //procedure TmmGraph.DrawYAxis
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawTitles(aCanvas: TCanvas);
var
 x,
 y1,y2 : integer;

begin
 if not FTitles.Visible then exit; 

 SetBorderStyle(aCanvas,FTitles.BorderStyle,FTitles.BackgroundColor);
 aCanvas.Rectangle(FTitles.Left,FTitles.Top,FTitles.Right,FTitles.Bottom +1);

 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 y1 := FTitles.Top +GetPrnFactY(cDiff_LegendBorder);
 y2 := FTitles.Top +GetPrnFactY(2*cDiff_LegendBorder) +aCanvas.TextHeight('H');

 aCanvas.Brush.Style := bsClear;

 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 x := GetHorzTextPos(FChartArea.Left,FChartArea.Right,aCanvas.TextWidth(FTitles.Title),FTitles.Alignment);
 aCanvas.TextOut(x,y1,FTitles.Title);

 aCanvas.Font := FTitles.SubTitleFont;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 x := GetHorzTextPos(FChartArea.Left,FChartArea.Right,aCanvas.TextWidth(FTitles.SubTitle),FTitles.Alignment);
 aCanvas.TextOut(x,y2,FTitles.SubTitle);
end; //procedure TmmGraph.DrawTitles
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawLegend(aCanvas: TCanvas);
var
 i,
 x,x1,y,
 hText,
 cCol,
 cRow,
 xCols : integer;

//-------------
procedure DrawLegendItem(aLegItem: integer; aSymbolRect,aTextRect: trect);
var
 xSymbolHeight : integer;
 xStr,
 xItemText     : string;

begin
 with aCanvas do
 begin
  xSymbolHeight := aSymbolRect.Bottom -aSymbolRect.Top;

  Brush.Style := bsSolid;
  Brush.Color := Series.Color[aLegItem];
  Pen.Color := clBlack;
  Pen.Width := 1;

  if Series.YAxis[aLegItem] = yaxLeft then
  begin
   Pen.Color := Series.Color[aLegItem];
   Pen.Width := GetPrnFactY(3);
   MoveTo(aSymbolRect.Left,aSymbolRect.Top +xSymbolHeight div 2);
   LineTo(aSymbolRect.Right,aSymbolRect.Top +xSymbolHeight div 2);
  end //if Series.YAxis[aLegItem] = yaxLeft then
  else Rectangle(aSymbolRect.Left,aSymbolRect.Top,aSymbolRect.Right,aSymbolRect.Bottom);

  if Series.Visible[i] then Font.Style := []
                       else Font.Style := [fsItalic];

  Brush.Style := bsClear;

  if Series.YScalingType[aLegItem] = ysNone
   then xStr := ''
   else xStr := format(' (%s)',[cYScalingStr[Series.YScalingType[aLegItem]]]);

  if (Series.YAxis[aLegItem] = yaxRight) and (FBars.Style <> bsSide)
   then xItemText := Series.Name[aLegItem]
   else xItemText := Series.Name[aLegItem] +xStr;

  TextRect(aTextRect,aTextRect.Left,aTextRect.Top,xItemText);
 end; //with aCanvas do
end; //procedure DrawLegendItem
//-------------

begin //main
 if not FLegend.Visible then exit;

 SetBorderStyle(aCanvas,FLegend.BorderStyle,FLegend.BackgroundColor);
 aCanvas.Rectangle(FLegend.Left,FLegend.Top,FLegend.Right,FLegend.Bottom);

 with aCanvas do
 begin
  Font := FLegend.Font;
  Font.Height := SetFontHeight(Font);
  hText := TextHeight('H');

  for i := 1 to Data.NumSeries do
  begin
   if FLegend.Position = lpRight then
   begin
    x := FLegend.Left +GetPrnFactX(cDiff_LegendBorder);
    y := FLegend.Top +GetPrnFactY(cDiff_LegendBorder) +(i-1) *GetLegendItemHeight(aCanvas);
    x1 := FLegend.Left +GetPrnFactX(cDiff_LegendBorder +cDiff_LegendText) +hText;

    if (y +hText) < FLegend.Bottom then
     DrawLegendItem(i,rect(x+1,y+1,x+hText-1,y+hText-1),rect(x1,y-1,FLegend.Right -2,y+hText));
   end //if FLegend.Position = lpRight then
   else
   begin
    xCols := FLegend.Width div GetLegendItemWidth(aCanvas);
    if xCols < 1 then xCols := 1;

    cRow := (i div xCols) +1;
    cCol := i mod xCols;
    if cCol = 0 then
    begin
     cCol := xCols;
     dec(cRow);
    end; //if cCol = 0 then

    x := FLegend.Left +GetPrnFactX(cDiff_LegendBorder) +(cCol -1) *GetLegendItemWidth(aCanvas);
    y := FLegend.Top +GetPrnFactY(cDiff_LegendBorder) +(cRow-1) *GetLegendItemHeight(aCanvas);
    x1 := x +GetPrnFactX(cDiff_LegendText) +hText;

    DrawLegendItem(i,rect(x+1,y+1,x+hText-1,y+hText-1),rect(x1,y-1,FLegend.Right -2,y+hText));
   end; //else if FLegend.Position = lpRight then
  end; //for i := 1 to Data.NumSeries do
 end; //with aCanvas do
end; //procedure TmmGraph.DrawLegend
//-----------------------------------------------------------------------------
procedure TmmGraph.GetScrollBarMinMax(var aMin,aMax: integer);
begin
 if FScrollBar.Visible then
 begin
  aMin := FScrollBar.Position;
  aMax := FScrollBar.Position +FScrollBar.PageSize -1;
  if aMax > (Data.GetNumberOfData -1) then aMax := Data.GetNumberOfData -1;
 end //if FScrollBar.Visible then
 else
 begin
  aMin := 0;
  aMax := Data.GetNumberOfData -1;
  FScrollBar.Position := 0;
 end; //else if FScrollBar.Visible then
end; //procedure TmmGraph.GetScrollBarMinMax
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawVertGrid(aCanvas: TCanvas);
var
 x,i,
 ticks  : integer;
 xMin,
 xMax,
 xStep,
 xValue : extended;

//-------------
procedure DrawGridLine(x: integer);
begin
 aCanvas.MoveTo(x,FChartArea.Top);
 aCanvas.LineTo(x,FChartArea.Bottom);
end; //procedure DrawGridLine

//-------------

begin //main
 if not FVertGrid.Visible then exit;
 if XAxis.XAxisType = xaFixed then exit;

 aCanvas.Pen.Color := FVertGrid.Color;
 aCanvas.Pen.Style := FVertGrid.Style;
 aCanvas.Pen.Width := FVertGrid.Width;

 case XAxis.XAxisType of
  xaFloat: begin
   xMin := XScale.actMin;
   xMax := XScale.actMax;
   xStep := 0;
   SetMinMax(xMin,xMax,xStep,true);

   ticks := round((xMax -xMin) /xStep) -1;

   for i := 0 to ticks do
   begin
    xValue := xMin +(i*xStep);

    if ((xValue +cEpsilon) > XScale.actMin) and (xValue < XScale.actMax) then
    begin
     x := fx(0,xValue);
     DrawGridLine(x);
    end; //if (xValue > XScale.actMin) and (xValue < XScale.actMax) then
   end; //for i := 1 to XScale.Step do
  end; //xaFloat

  xaDateTime: begin
   FDateTickType := dtMinute;

   xStep := GetDatestep(FDateTickType);
   //while ((XScale.actMax -XScale.actMin) /xStep) > 18 {cMaxSteps} do
   while ((XScale.actMax -XScale.actMin) /xStep) > cMaxSteps do
   begin
    if FDateTickType = dtYear100 then break;
    inc(FDateTickType);
    xStep := GetDatestep(FDateTickType);
   end; //while ((FMax -FMin) /xStep) > cMaxSteps do

   xStep := GetDatestep(FDateTickType);
   xValue := GetFirstDateTick(XScale.actMin);
   x := fx(0,xValue);
   DrawGridLine(x);

   while (xValue +xStep) < XScale.actMax do
   begin
    xValue := GetNextDateTick(xValue);
    x := fx(0,xValue);
    DrawGridLine(x);
   end; //while xTick +FStep < FMax do
  end; //xaFloat,xaDateTime
 end; //for i := iMin to iMax do
end; //procedure TmmGraph.DrawVertGrid
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawXAxis(aCanvas: TCanvas);
const
 cLenXTicks = 4;

var
 x,y,i,
 iMin,
 iMax,
 ticks,
 wText,
 hText,
 lastX      : integer;
 xMin,
 xMax,
 xStep,
 xValue     : extended;
 xLabel     : XLabelType;

//-------------
procedure DrawXLabel(x: integer; xLabel: string);
const
 cMinDiff = 4;

var
 dx,dy,
 dxx,
 y     : integer;

begin
 wText := aCanvas.TextWidth(xLabel);
 hText := aCanvas.TextHeight(xLabel);

 aCanvas.MoveTo(x,FXAxis.Top);
 aCanvas.LineTo(x,FXAxis.Top +GetPrnFactX(cLenXTicks));

 if FXAxis.XAxisTextAngle = 0 then
 begin
  x := x -(wText div 2);
  y := FXAxis.Top +GetPrnFactY(2*cLenXTicks);

  if (x > lastX) and ((x +wText +GetPrnFactX(cMinDiff)) < FChartArea.Right) then
  begin
   aCanvas.TextOut(x,y,xLabel);
   lastX := x +wText +GetPrnFactX(cMinDiff);
  end; //if (x > lastX) and ((x +wText +cMinDiff) < FChartArea.Right) then
 end  //if FXAxis.XAxisTextAngle = 0 then
 else
 begin
  dy := round(sin(DegToRad(FXAxis.XAxisTextAngle))*wtext);
  dx := round(cos(DegToRad(FXAxis.XAxisTextAngle))*wtext);
  dxx := round(cos(DegToRad(90 -FXAxis.XAxisTextAngle))*htext) div 2;

  x := x -(dx+dxx);
  y := FXAxis.Top +dy +GetPrnFactY(2*cLenXTicks);

  if (x > lastX) and ((x +GetPrnFactX(cMinDiff)) < FChartArea.Right) then
  begin
   aCanvas.TextOut(x,y,xLabel);
   lastX := x +(dx+dxx) +GetPrnFactX(cMinDiff);
  end; //if (x > lastX) and ((x +hText +cMinDiff) < FChartArea.Right) then
 end; //else if FXAxis.XAxisTextAngle = 0 then
end; //procedure DrawXLabel

//-------------

begin
 lastX := -999;

 SetBorderStyle(aCanvas,FXAxis.BorderStyle,FXAxis.BackgroundColor);
 if FXAxis.BorderStyle = bsNone then y := FXAxis.Top
                                else y := FXAxis.Top -1;
 aCanvas.Rectangle(FChartArea.Left,y,FChartArea.Right,FXAxis.Bottom);

 aCanvas.Pen.Color := clBlack;
 aCanvas.Font := FXAxis.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);

 if XAxis.XAxisType = xaFixed then GetScrollBarMinMax(iMin,iMax);

 wText := aCanvas.TextWidth(FXAxis.Title);
 hText := aCanvas.TextHeight('H');
 x := FChartArea.Left +(FChartArea.Width -wText) div 2;
 y := FXAxis.Bottom -hText -GetPrnFactY(FXAxis.TitleMargin);
 aCanvas.TextOut(x,y,FXAxis.Title);

 SetTextDirection(aCanvas,FXAxis.XAxisTextAngle);

 case XAxis.XAxisType of
  xaFixed: begin
   for i := iMin to iMax do
   begin
    x := fx(i,Data.GetXValue(1,i));
    xLabel := Data.Data.Values[i].XValue.Name;
    DrawXLabel(x,xLabel);
   end; //for i := iMin to iMax do
  end; //xaFixed

  xaFloat: begin
   xMin := XScale.actMin;
   xMax := XScale.actMax;
   xStep := 0;
   SetMinMax(xMin,xMax,xStep,true);

   ticks := round((xMax -xMin) /xStep) -1;

   for i := 0 to ticks do
   begin
    xValue := xMin +(i*xStep);

    if ((xValue +cEpsilon) > XScale.actMin) and (xValue < XScale.actMax) then
    begin
     x := fx(0,xValue);
     xLabel := formatfloat(XAxis.XValueFormat,xValue);
     DrawXLabel(x,xLabel);
    end; //if (xValue > XScale.actMin) and (xValue < XScale.actMax) then
   end; //for i := 1 to XScale.Step do
  end; //xaFloat,xaDateTime

  xaDateTime: begin
   FDateTickType := dtMinute;

   xStep := GetDatestep(FDateTickType);
   //while ((XScale.actMax -XScale.actMin) /xStep) > 18 {cMaxSteps} do
   while ((XScale.actMax -XScale.actMin) /xStep) > cMaxSteps do
   begin
    if FDateTickType = dtYear100 then break;
    inc(FDateTickType);
    xStep := GetDatestep(FDateTickType);
   end; //while ((FMax -FMin) /xStep) > cMaxSteps do

   xStep := GetDatestep(FDateTickType);

   xValue := GetFirstDateTick(XScale.actMin);
   x := fx(0,xValue);
   xLabel := GetDateFormat(FDateTickType,xValue);

   DrawXLabel(x,xLabel);

   while (xValue +xStep) < XScale.actMax do
   begin
    xValue := GetNextDateTick(xValue);
    x := fx(0,xValue);
    xLabel := GetDateFormat(FDateTickType,xValue);
    DrawXLabel(x,xLabel);
   end; //while xTick +FStep < FMax do
  end; //xaFloat,xaDateTime
 end; //for i := iMin to iMax do

 SetTextDirection(aCanvas,0);
end; //procedure TmmGraph.DrawXAxis
//-----------------------------------------------------------------------------
procedure TmmGraph.ClipGraph(aCanvas: TCanvas);
var
 ClipRgn: HRgn;

begin
 ClipRgn := CreateRectRgn(FChartArea.Left,FChartArea.Top+1,FChartArea.Right,FChartArea.Bottom);
 SelectClipRgn(aCanvas.Handle,ClipRgn);
 DeleteObject(ClipRgn);
end; //procedure TmmGraph.ClipGraph
//-----------------------------------------------------------------------------
procedure TmmGraph.UnclipGraph(aCanvas: TCanvas);
var
 ClipRgn: HRgn;

begin
 //??
 ClipRgn := CreateRectRgn(0,0,GetPrnFactX(FPaintBox.Width),GetPrnFactY(FPaintBox.Height));
 SelectClipRgn(aCanvas.Handle,ClipRgn);
 DeleteObject(ClipRgn);
end; //procedure TmmGraph.UnclipGraph
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawMainChart(aCanvas: TCanvas);
var
 iMin,iMax : integer;

//-----------

procedure DrawLimitArea(xRow: byte; p1,p2: tpoint);
var
 x,y       : integer;
 m         : double;
 yUpperLim,
 yLowerLim : integer;

begin
 try
  m := (p2.y -p1.y) /(p2.x -p1.x);
 except
  exit;
 end;   //try..except

 yUpperLim := fy(FSeries.UpperLimit[xRow] *FSeries.YScaling[xRow],yaxLeft);
 yLowerLim := fy(FSeries.LowerLimit[xRow] *FSeries.YScaling[xRow],yaxLeft);

 with aCanvas do
 begin
  for x := p1.x to p2.x do
  begin
   y := p1.y +round((x -p1.x) *m);

   if (stLowerLimit in Series.ShowStat[xRow]) and (y > yLowerLim) then
   begin
    Pen.Color := FSeries.LowerLimitColor[xRow];
    MoveTo(x,y-1);
    LineTo(x,yLowerLim);
   end; //if y > yLowerLim then

   if (stUpperLimit in Series.ShowStat[xRow]) and (y < yUpperLim) then
   begin
    Pen.Color := FSeries.UpperLimitColor[xRow];
    MoveTo(x,y+1);
    LineTo(x,yUpperLim);
   end; //if y > yLowerLim then
  end; //for x := p1.x to p2.x do
 end; //with aCanvas do
end; //procedure DrawLimitArea

//-----------

procedure DrawLineSeries;
const
 cdy0    = 2;
 cdy1    = 0;
 cMaxRes = 30000;

type
 tSmoothRecordT = record
  Count  : integer;
  XValue,
  YValue : double;
 end; //tSmoothRecordT

 smRecArray = array[1..cMaxRes] of tSmoothRecordT;

var
 xRow        : byte;
 x,
 xLast,
 yLast,
 y,i         : integer;
 LastPoint,
 yP          : tYPointT;
 smPointGap,
 smINT       : integer;
 smXValue,
 smDX        : double;
 smRec       : smRecArray;

begin
 for xRow := 1 to Data.NumSeries do
  if (Series.YAxis[xRow] = yaxLeft) and Series.Visible[xRow] then
  begin
   LastPoint.IsNull := true;

   aCanvas.Brush.Color := Series.Color[xRow];
   aCanvas.Pen.Color := Series.Color[xRow];
   aCanvas.Pen.Style := psSolid;
   aCanvas.Pen.Width := 1;
   //aCanvas.Pen.Width := 2;
   case XAxis.XAxisType of
    xaFixed: begin
     aCanvas.Brush.Style := bsClear;

     for i := iMin to iMax do
     begin
      x := fx(i,Data.GetXValue(xRow,i));
      yP := Data.GetYValue(xRow,i);

      if not yP.IsNull then
      begin
       y := fy(yP.Value *Series.YScaling[xRow],yaxLeft);

       aCanvas.Pen.Color := Series.Color[xRow];

       if i = FSelectedPoint then aCanvas.Rectangle(x-GetPrnFactX(cdy0),y-GetPrnFactY(cdy0),x+GetPrnFactX(cdy0),y+GetPrnFactY(cdy0))
                             else aCanvas.Rectangle(x-GetPrnFactX(cdy1),y-GetPrnFactY(cdy1),x+GetPrnFactX(cdy1),y+GetPrnFactY(cdy1));

       if not LastPoint.IsNull then
       begin
        yLast := fy(LastPoint.Value *Series.YScaling[xRow],yaxLeft);
        aCanvas.MoveTo(xLast,yLast);
        aCanvas.LineTo(x,y);
        DrawLimitArea(xRow,point(xLast,yLast),point(x,y));

        InsertObject(giLine,rect(xLast,yLast,x,y),xRow,i,stData);
       end; //if not LastPoint.IsNull then
      end; //if not yP.IsNull then

      LastPoint := yP;
      xLast := x;
     end; //for i := iMin to iMax do
    end; //xaFixed

    xaFloat,xaDateTime: begin
     iMin := Data.FindPoint(XScale.actMin,ftNext);
     if iMin < 0 then iMin := Data.GetNumberOfData;
     iMax := Data.FindPoint(XScale.actMax,ftPrior);

     fillchar(smRec,sizeof(smRecArray),0);

     //??
     if FLines.SmoothLines then smPointGap := GetPrnFactX(FLines.FMinPointGap)
                           else smPointGap := GetPrnFactX(1); //=> max resolution

     smDX := XScale.actDX /(FChartArea.Width /smPointGap);

     //insert data into intervalls
     for i := iMin to iMax do
     begin
      yP := Data.GetYValue(xRow,i);
      smXValue := Data.GetXValue(xRow,i);
      smINT := trunc((smXValue -XScale.actMin) /smDX +1);

      if (smINT < 1) or (smINT > cMaxRes) then smINT := cMaxRes;

      if not yP.IsNull then
      begin
       smRec[smINT].YValue := smRec[smINT].YValue +(yP.Value *Series.YScaling[xRow]);
       smRec[smINT].XValue := smRec[smINT].XValue +smXValue;
       inc(smRec[smINT].Count);
      end; //if not yP.IsNull then
     end; //for i := iMin to iMax do

     iMin := 1;
     iMax := round(FChartArea.Width /smPointGap +0.5 -cEpsilon);

     LastPoint.IsNull := true;

     for i := iMin to iMax do
     begin
      if smRec[i].Count > 0 then
      begin
       x := fx(0,(smRec[i].XValue /smRec[i].Count));
       y := fy((smRec[i].YValue /smRec[i].Count),yaxLeft);

       aCanvas.Pen.Color := Series.Color[xRow];
       aCanvas.Brush.Style := bsClear;
       aCanvas.Rectangle(x-GetPrnFactX(cdy1),y-GetPrnFactY(cdy1),x+GetPrnFactX(cdy1),y+GetPrnFactY(cdy1));

       if not LastPoint.IsNull then
       begin
        yLast := fy(LastPoint.Value,yaxLeft);
        aCanvas.MoveTo(xLast,yLast);
        aCanvas.LineTo(x,y);
        DrawLimitArea(xRow,point(xLast,yLast),point(x,y));
       end; //if not LastPoint.IsNull then

       LastPoint.IsNull := false;
       LastPoint.Value := smRec[i].YValue /smRec[i].Count;
       xLast := x;
      end; //if smRec[i].Count > 0 then
     end; //for i := iMin to iMax do
    end; //xaFloat,xaDateTime
   end; //case XAxis.XAxisType of
  end; //if (Series.YAxis[xRow] = yaxLeft) and Series.Visible[xRow] then
end; //procedure DrawLineSeries

//-----------

procedure DrawBarSeries;
var
 xRow,
 xRowCounter : byte;
 xBarWidth,
 lastY,
 x,
 x1,x2,
 y100,
 y0,y1,
 y2,dy,i     : integer;
 yP,ySUM     : tYPointT;
 bmpBrush    : TBitmap;
 xBarRect    : trect;

//-----------
procedure SetBarStyle(x1,y1,x2,y2: integer);
var
 xInvRgn: HRgn;

begin
 xInvRgn := CreateRectRgn(x1,y1,x2,y2);
 InvertRgn(aCanvas.Handle,xInvRgn);
 DeleteObject(xInvRgn);
end; //procedure SetBarStyle
//-----------

procedure DrawBar;
begin
 if (i = FSelectedPoint) and FSelection.Enabled then
 begin
  case FSelection.Style of
   ssInverted: aCanvas.Brush.Color := FSeries.Color[xRow] xor clWhite;
   ssGray:     aCanvas.Brush.Color := FSeries.Color[xRow] and clGray; //rgb(160,160,160);
   ssFixed:    aCanvas.Brush.Color := FSelection.FixedColor;
  end; //case FSelection.Style of

  if FSelection.Style = ssFixed then aCanvas.Pen.Color := clBlack
                                else aCanvas.Pen.Color := aCanvas.Brush.Color;
  aCanvas.Rectangle(xBarRect.Left,xBarRect.Top,xBarRect.Right,xBarRect.Bottom);
 end //if (i = FSelectedPoint) and FSelection.Enabled then
 else
 begin
  aCanvas.Brush.Color := Series.Color[xRow];
  aCanvas.Pen.Color := aCanvas.Brush.Color;
  aCanvas.Rectangle(xBarRect.Left,xBarRect.Top,xBarRect.Right,xBarRect.Bottom);
 end; //if (i = FSelectedPoint) and FSelection.Enabled then

 InsertObject(giRectangle,xBarRect,xRow,i,stData);
end; //procedure DrawBar

//-----------

begin //main DrawBarSeries
 if NumOfBarSeries = 0 then exit;
 xRowCounter := 0;
 xBarWidth := BarWidth div NumOfBarSeries;

 bmpBrush := TBitmap.Create;
 bmpBrush.Monochrome := false;
 bmpBrush.Width := 8;
 bmpBrush.Height := 8;
 try
  case FBars.Style of
   bsSide: begin
    for xRow := 1 to Data.NumSeries do
     if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
     begin
      aCanvas.Pen.Style := psSolid;
      aCanvas.Pen.Width := 1;

      for i := iMin to iMax do
      begin
       x := fx(i,Data.GetXValue(xRow,i));
       yP := Data.GetYValue(xRow,i);

       if not yP.IsNull then
       begin
        y1 := fy(yP.Value *Series.YScaling[xRow],yaxRight);

        x1 := x -(BarWidth div 2) +xBarWidth *xRowCounter;
        x2 := x1 +xBarWidth;

        xBarRect := rect(x1,y1,x2,FXAxis.Top -1);
        DrawBar;
       end; //if not yP.IsNull then
      end; //for i := iMin to iMax do

      inc(xRowCounter);
     end; //if (Series.YAxis[xRow] = aYAxis) and Series.Visible[xRow] then
   end; //bsSide

   bsStacked,bsStacked100: begin
    y0 := fy(0,yaxRight);
    y100 := fy(100,yaxRight);
    for i := iMin to iMax do
    begin
     lastY := y0;
     ySUM := Data.GetYValue(0,i);
     x := fx(i,Data.GetXValue(xRow,i));

     for xRow := 1 to Data.NumSeries do
      if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
      begin
       aCanvas.Pen.Color := Series.Color[xRow];
       aCanvas.Pen.Style := psSolid;
       aCanvas.Pen.Width := 1;

       yP := Data.GetYValue(xRow,i);

       if not yP.IsNull then
       begin
        if FBars.Style = bsStacked then
        begin
         dy := y0 -fy(yP.Value,yaxRight);
        end //if FBars.Style = bsStacked then
        else
        begin
         try
          dy := y0 -fy(yP.Value /ySUM.Value *100,yaxRight);
         except
          dy := 0;
         end; //try..except
        end; //else if FBars.Style = bsStacked then

        y1 := lastY;
        y2 := lastY -dy;

        //anpassung an rundungsfehler
        if FBars.Style = bsStacked100 then
        begin
         if y2 < y100 then y2 := y100;
         if abs(y2 -y100) < 3 then y2 := y100;
        end; //if FBars.Style = bsStacked100 then

        x1 := x -(BarWidth div 2);
        x2 := x1 +BarWidth;

        xBarRect := rect(x1,y2,x2,y1); //y2,y1 korrekt !!!
        DrawBar;

        lastY := y2;
       end; //if not yP.IsNull then
      end; //if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
    end; //for i := iMin to iMax do
   end; //bsStacked,bsStacked100
  end; //case FBars.Style of

  aCanvas.Pen.Style := psSolid;
  aCanvas.Pen.Color := clBlack;
  aCanvas.Pen.Width := 1;
 finally
  bmpBrush.free;
 end; //try..finally
end; //procedure DrawBarSeries

//-----------

begin //main
 GetScrollBarMinMax(iMin,iMax);

 ClipGraph(aCanvas);
 try
  if XAxis.XAxisType = xaFixed then DrawBarSeries;
  DrawLineSeries;
 finally
  UnClipGraph(aCanvas);

  if IsPrinting then
  begin
   aCanvas.MoveTo(FChartArea.Left,FChartArea.Bottom-1);
   aCanvas.LineTo(FChartArea.Right,FChartArea.Bottom-1);
  end; //if IsPrinting then
 end; //try..finally
end; //procedure TmmGraph.DrawMainChart
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawStatistikLines(aCanvas: TCanvas);
var
 xRow   : byte;
 y1,y2  : integer;
 xYAxis : eYAxisT;
 xStat  : eStatistikT;

//----------
procedure DrawLine(y: integer);
begin
 aCanvas.MoveTo(FChartArea.Left,y);
 aCanvas.LineTo(FChartArea.Right,y);
 InsertObject(giLine,rect(FChartArea.Left,y,FChartArea.Right,y),xRow,-1,xStat);
end; //procedure DrawLine
//----------

begin //main
 ClipGraph(aCanvas);
 try
  for xRow := 1 to Data.NumSeries do
  begin
   xYAxis := Series.YAxis[xRow];

   //no stat-lines if bars are stacked or xaxis = xaFloat
   if (xYAxis = yaxLeft) or
      ((xYAxis = yaxRight) and (FBars.Style = bsSide) and (FXAxis.XAxisType = xaFixed)) then
   begin
    aCanvas.Brush.Color := Series.Color[xRow];
    aCanvas.Pen.Color := Series.Color[xRow];

    for xStat := stMin to stUpperLimit do
     if xStat in Series.ShowStat[xRow] then
     begin
      aCanvas.Pen.Style := StatistikLines.Style[xStat];
      aCanvas.Pen.Width := StatistikLines.Width[xStat];

      case xStat of
       stMin,stMax,stMean: begin
        y1 := fy(Data.Data.Statistik[xRow,xStat] *Series.YScaling[xRow],xYAxis);
        DrawLine(y1);
       end; //stMin,stMax,stMean

       stStdDev,stQ90,stQ95,stQ99: begin
        y1 := fy((Data.Data.Statistik[xRow,stMean] +Data.Data.Statistik[xRow,xStat]) *Series.YScaling[xRow],xYAxis);
        y2 := fy((Data.Data.Statistik[xRow,stMean] -Data.Data.Statistik[xRow,xStat]) *Series.YScaling[xRow],xYAxis);
        DrawLine(y1);
        DrawLine(y2);
       end; //stStdDev,stQ90,stQ95,stQ99

       stUpperLimit: begin
        if stUpperLimit in FSeries.ShowStat[xRow] then
        begin
         y1 := fy(FSeries.UpperLimit[xRow] *Series.YScaling[xRow],xYAxis);
         DrawLine(y1);
        end; //if stUpperLimit in FSeries.ShowStat[xRow] then
       end; //stUpperLimit

       stLowerLimit: begin
        if stLowerLimit in FSeries.ShowStat[xRow] then
        begin
         y1 := fy(FSeries.LowerLimit[xRow] *Series.YScaling[xRow],xYAxis);
         DrawLine(y1);
        end; //if stLowerLimit in FSeries.ShowStat[xRow] then
       end; //stLowerLimit
      end; //case xStat of
     end; //if xStat in Series.ShowStat[xRow] then
   end; //if not((xYAxis = yaxRight) and (FBars.Style <> bsSide)) then
  end; //for xRow := 1 to Data.NumSeries do
 finally
  UnClipGraph(aCanvas);
 end; //try..finally
end; //procedure TmmGraph.DrawStatistikLines
//-----------------------------------------------------------------------------
procedure TmmGraph.SetXScale(aMin,aMax: double);
const
 maxXZoom = 100;

var
 min_actDX : double;

begin
 XScale.actMin := aMin;
 XScale.actMax := aMax;
 XScale.absMin := Data.XMin;
 XScale.absMax := Data.XMax;
 XScale.absDX := XScale.absMax -XScale.absMin;

 min_actDX := XScale.absDX /maxXZoom;
 if abs(XScale.actMax -XScale.actMin) < min_actDX then XScale.actMax := XScale.actMin +min_actDX;

 XScale.actDX := XScale.actMax -XScale.actMin;
 FScrollBar.Visible := XScale.actDX < (XScale.absDX -cEpsilon);
 FScrollBar.Max := 1000;

 try
  if FScrollBar.Visible then FScrollBar.PageSize := round(XScale.actDX /XScale.absDX *1000)
                        else FScrollBar.PageSize := FScrollBar.Max;
  FScrollBar.Position := round((XScale.actMin -XScale.absMin) /XScale.absDX *1000);
 except
  FScrollBar.Visible := false;
  FScrollBar.Position := 0;
 end; //try..except
end; //procedure TmmGraph.SetXScale
//-----------------------------------------------------------------------------

procedure TmmGraph.CalcScrollBar;
var
 PointsPerPage : integer;
 xInt          : Integer;

begin
 FScrollBar.Left := FChartArea.Left;
 FScrollBar.Width := FChartArea.Width;
 FScrollBar.Top := GetPrnFactY(FPaintBox.Height -FMargin.Bottom -cScrollBarHeight +7);

 //scrollbar position
 if XAxis.XAxisType = xaFixed then
 begin
  //??
  if IsPrinting then
  begin
   PointsPerPage := FChartArea.Width div (BarWidth +GetPrnFactX(FBars.Gap));
  end
  else PointsPerPage := FChartArea.Width div (BarWidth +GetPrnFactX(FBars.Gap));

  //vcl-error setting pagesize when FScrollBar.Visible = false not possible !!!
  FScrollBar.PageSize := 0;
  FScrollBar.Visible := Data.GetNumberOfData > PointsPerPage;
  FScrollBar.Max := Data.GetNumberOfData;
  if FScrollBar.Visible then FScrollBar.PageSize := PointsPerPage;
 end; //if XAxis.XAxisType = xaFixed then

 FScrollBar.Min := 0;
 //Wss
 xInt := FScrollBar.Max div 100;
 if xInt = 0 then xInt := 1;
 FScrollBar.SmallChange := xInt;
 xInt := FScrollBar.Max div 10;
 if xInt = 0 then xInt := 1;
 FScrollBar.LargeChange := xInt;

{
 Wss: Range error if SmallChange, LargeChane is 0. Catch 0 in variable
 FScrollBar.SmallChange := FScrollBar.Max div 100;
 if FScrollBar.SmallChange = 0 then FScrollBar.SmallChange := 1;
 FScrollBar.LargeChange := FScrollBar.Max div 10;
 if FScrollBar.LargeChange = 0 then FScrollBar.LargeChange := 1;
}
end; //procedure TmmGraph.CalcScrollBar
//-----------------------------------------------------------------------------
procedure TmmGraph.InsertObject(gi: eGraphItemT; r: trect; dr,dp: integer; st: eStatistikT);
var
 i : integer;

begin
 i := high(FObj) +1;
 SetLength(FObj,i +1);

 with FObj[i] do
 begin
  ItemType  := gi;
  ItemRect  := r;
  DataRow   := dr;
  DataPoint := dp;
  StatType  := st;
 end; //with FObj[i] do
end; //procedure TmmGraph.InsertObject
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawPanelBackground(aCanvas: TCanvas);
begin
 if IsPrinting then SetBorderStyle(aCanvas,bsNone,clWhite)
               else SetBorderStyle(aCanvas,bsNone,FColors.PanelBackground);
 if not IsPrinting then
  aCanvas.Rectangle(0,0,GetPrnFactX(FPaintBox.Width),GetPrnFactY(FPaintBox.Height));
end; //procedure TmmGraph.DrawPanelBackground
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawChartArea(aCanvas: TCanvas);
begin
 SetBorderStyle(aCanvas,FChartArea.BorderStyle,FChartArea.BackgroundColor);
 aCanvas.Rectangle(FChartArea.Left,FChartArea.Top,FChartArea.Right,FChartArea.Bottom);
end; //procedure TmmGraph.DrawChartArea
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcLegendWidth(aCanvas: TCanvas);
begin
 case FLegend.Position of
  lpRight: begin
   FLegend.Width := GetLegendItemWidth(aCanvas) +GetPrnFactX(20);
   //??
   FLegend.Width := math.min(FLegend.Width,GetPrnFactX(cMaxLegendWidth));
   //if FLegend.Width > GetPrnFactX(cMaxLegendWidth) then FLegend.Width := GetPrnFactX(cMaxLegendWidth);
  end; //case FLegend.Position of
 end; //case FLegend.Position of
end; //procedure TmmGraph.CalcLegendWidth
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcScrollBtnPos;
const
 cDiffScrollBtns = 0;

var
 y : integer;

begin
 if IsPrinting then exit;

 //xxx
 FLeftBtnInc.Hint := LTrans(cHintBtnUpStr);
 FLeftBtnDec.Hint := LTrans(cHintBtnDownStr);
 FRightBtnInc.Hint := LTrans(cHintBtnUpStr);
 FRightBtnDec.Hint := LTrans(cHintBtnDownStr);

 FLeftBtnInc.Visible := FShowScrollButtons;
 FLeftBtnDec.Visible := FShowScrollButtons;
 FRightBtnInc.Visible := FShowScrollButtons and FRightYAxis.Visible and (FHorzGrid.GridType <> hgLeftRightAxis);
 FRightBtnDec.Visible := FRightBtnInc.Visible;

 FLeftBtnInc.Left := FLeftYAxis.Right -(cDiff_AxisTicksText +FLeftBtnInc.Width);
 FLeftBtnDec.Left := FLeftBtnInc.Left;

 FRightBtnInc.Left := FRightYAxis.Left +cDiff_AxisTicksText;
 FRightBtnDec.Left := FRightBtnInc.Left;

 if (FLegend.Position = lpTop) and FLegend.Visible then y := FLegend.Bottom
                                                   else y := FTitles.Bottom;

 FLeftBtnInc.Top := y -(cScrollBtnHeight *2 +cDiffScrollBtns);
 FLeftBtnDec.Top := FLeftBtnInc.Top +cScrollBtnHeight +cDiffScrollBtns;

 FRightBtnInc.Top := FLeftBtnInc.Top;
 FRightBtnDec.Top := FLeftBtnDec.Top;
end; //procedure TmmGraph.CalcScrollBtnPos
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawOnCanvas(aCanvas: TCanvas);
begin
 FObj := nil;

 if (Data.SortOrder = soNone) then Data.SortByRow(0,soAscending);
 if XAxis.XAxisType in [xaFloat,xaDateTime] then
  if (Data.SortRow <> 0) or (Data.SortOrder <> soAscending) then Data.SortByRow(0,soAscending);

 CalcLegendWidth(aCanvas);

 CalcGraphCoordinates(aCanvas);
 CalcBarWidth;
 CalcYAxis;

 DrawPanelBackground(aCanvas);

 CalcScrollBar;
 if XAxis.XAxisType in [xaFloat,xaDateTime] then
  if XScale.actMin >= XScale.actMax then
  begin
   Data.SetXMinMax;
   SetXScale(Data.XMin,Data.XMax);
  end; //if XScale.actMin >= XScale.actMax then

 CalcScrollBtnPos;
 DrawChartArea(aCanvas);
 DrawTitles(aCanvas);
 DrawLegend(aCanvas);
 DrawXAxis(aCanvas);
 if not FVertGrid.Foreground then DrawVertGrid(aCanvas);
 if FHorzGrid.Foreground then DrawMainChart(aCanvas);
 DrawYAxis(aCanvas,yaxLeft);
 DrawYAxis(aCanvas,yaxRight);
 if not FHorzGrid.Foreground then DrawMainChart(aCanvas);
 if FVertGrid.Foreground then DrawVertGrid(aCanvas);
 DrawStatistikLines(aCanvas);
end; //procedure TmmGraph.DrawOnCanvas
//-----------------------------------------------------------------------------
procedure TmmGraph.FPaintBoxPaint(Sender: TObject);
begin
 DrawOnCanvas(FPaintBox.Canvas);
end; //procedure TmmGraph.FPaintBoxPaint
//-----------------------------------------------------------------------------
procedure TmmGraph.InitPrinterInfo;
begin
 with PInfo do
 begin
  xMM := GetDeviceCaps(Printer.Handle,HORZSIZE);
  yMM := GetDeviceCaps(Printer.Handle,VERTSIZE);
  xPix := GetDeviceCaps(Printer.Handle,HORZRES);
  yPix := GetDeviceCaps(Printer.Handle,VERTRES);
  xPixInch := GetDeviceCaps(Printer.Handle,LOGPIXELSX);
  yPixInch := GetDeviceCaps(Printer.Handle,LOGPIXELSY);
  xPhysPix := GetDeviceCaps(Printer.Handle,PHYSICALWIDTH);
  yPhysPix := GetDeviceCaps(Printer.Handle,PHYSICALHEIGHT);
  xOffset := GetDeviceCaps(Printer.Handle,PHYSICALOFFSETX);
  yOffset := GetDeviceCaps(Printer.Handle,PHYSICALOFFSETY);
  xPixMM := xPix /xMM;
  yPixMM := yPix /yMM;
 end; //with PInfo do
end; //procedure TmmGraph.InitPrinterInfo
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcPrintRect;
begin
 with PInfo do
 begin
  if prnXPos[puMM] < 0 then prnXPos[puMM] := 0;
  if prnYPos[puMM] < 0 then prnYPos[puMM] := 0;
  if prnWidth[puMM] < 0 then prnWidth[puMM] := 0;
  if prnHeight[puMM] < 0 then prnHeight[puMM] := 0;

  prnXPos[puPix] := round(prnXPos[puMM] *xPixMM);
  prnYPos[puPix] := round(prnYPos[puMM] *yPixMM);

  if prnXPos[puPix] < xOffset then prnXPos[puPix] := xOffset;
  if prnYPos[puPix] < yOffset then prnYPos[puPix] := yOffset;

  dec(prnXPos[puPix],xOffset);
  dec(prnYPos[puPix],yOffset);

  if prnWidth[puMM] = 0 then prnWidth[puMM] := xMM -prnXPos[puMM];
  if prnHeight[puMM] = 0 then prnHeight[puMM] := yMM -prnYPos[puMM];

  prnWidth[puPix] := round(prnWidth[puMM] *xPixMM) +prnXPos[puPix];
  prnHeight[puPix] := round(prnHeight[puMM] *yPixMM) +prnYPos[puPix];
 end; //with PInfo do
end; //procedure TmmGraph.CalcPrintRect
//-----------------------------------------------------------------------------
function TmmGraph.GetPrnFactX(aValue: double): integer;
var
 xFactor : double;

begin
 if IsPrinting then
 begin
  xFactor := PInfo.prnWidth[puPix] /Width;
  result := round(aValue *xFactor);
 end //if IsPrinting then
 else result := round(aValue);
end; //function TmmGraph.GetPrnFactX
//-----------------------------------------------------------------------------
function TmmGraph.GetPrnFactY(aValue: double): integer;
var
 yFactor : double;

begin
 if IsPrinting then
 begin
  yFactor := PInfo.prnHeight[puPix] /Height;
  result := round(aValue *yFactor);
 end //if IsPrinting then
 else result := round(aValue);
end; //function TmmGraph.GetPrnFactY
//-----------------------------------------------------------------------------
function TmmGraph.GetPrnOffsetX: integer;
begin
 if IsPrinting then result := PInfo.prnXPos[puPix]
               else result := 0;
end; //function TmmGraph.GetPrnOffsetX
//-----------------------------------------------------------------------------
function TmmGraph.GetPrnOffsetY: integer;
begin
 if IsPrinting then result := PInfo.prnYPos[puPix]
               else result := 0;
end; //function TmmGraph.GetPrnOffsetY
//-----------------------------------------------------------------------------
function TmmGraph.SetFontHeight(aFont: TFont): integer;
var
 fHeight : double;

begin
 fHeight := -aFont.Size *aFont.PixelsPerInch /72;
 if IsPrinting then result := round(PInfo.prnHeight[puPix] /PInfo.yPix *fHeight)
               else result := round(fHeight);
end; //function TmmGraph.SetFontHeight
//-----------------------------------------------------------------------------
function TmmGraph.LTrans(aText: string): string;
begin
 if (csDesigning in ComponentState) then result := aText
                                    else result := IvDictio.Translate(aText);
//                                    else result := FTranslator.Dictionary.Translate(aText);
end; //function TmmGraph.LTrans
//-----------------------------------------------------------------------------
procedure TmmGraph.Print(aXPosMM,aYPosMM,aWidthMM,aHeightMM: integer);
var
 xExternalPrinting : boolean;
begin

 xExternalPrinting := Printer.Printing;
 IsPrinting := true;
 PInfo.prnXPos[puMM] := aXPosMM;
 PInfo.prnYPos[puMM] := aYPosMM;
 PInfo.prnWidth[puMM] := aWidthMM;
 PInfo.prnHeight[puMM] := aHeightMM;

 try
  InitPrinterInfo;
  CalcPrintRect;

  if not xExternalPrinting then
     Printer.BeginDoc;

  DrawOnCanvas(Printer.Canvas);
  PrintHeaderFooter(Printer.Canvas);

  if not xExternalPrinting then
     Printer.EndDoc;

 finally
  IsPrinting := false;
  Refresh;
 end; //try..finally
end; //procedure TmmGraph.Print
//-----------------------------------------------------------------------------
function TmmGraph.GetDateFormat(aDateTicks: eDateTicksT; aValue: double): string;
var
 xFormatStr : string;
 y,m,d,
 h,mm,s,hs  : word;

begin
 if XAxis.XValueFormat = '' then
 begin
  DecodeDate(aValue,y,m,d);
  DecodeTime(aValue,h,mm,s,hs);

  case aDateTicks of
   dtMinute..dtMinute30: begin
    if (h = 0) and (mm = 0) then xFormatStr := 'dd/mm/yy'
                            else xFormatStr := 'hh:nn';
   end; //dtMinute..dtMinute30
   dtHour..dtHour12: begin
    if (h = 0) and (mm = 0) then xFormatStr := 'dd/mm/yy'
                            else xFormatStr := 'hh:nn';
   end; //dtHour..dtHour12
   dtDay,dtWeek: xFormatStr := 'dd/mm/yyyy';
   dtMonth..dtYear: xFormatStr := 'mmm yyyy';
   dtYear10,dtYear100: xFormatStr := 'yyyy';
  end; //case aDateTicks of
 end //if XAxis.XValueFormat = '' then
 else xFormatStr := XAxis.XValueFormat;

 result := FormatDateTime(xFormatStr,aValue);

 {
 try
  result := FormatDateTime(xFormatStr,aValue);
 except
  xFormatStr := 'dd/mm/yyyy';
  result := FormatDateTime(xFormatStr,aValue);
 end; //try..except
 }
end; //function TmmGraph.GetDateFormat
//-----------------------------------------------------------------------------
function TmmGraph.GetDateStep(aDateTicks: eDateTicksT): double;
begin
 case aDateTicks of
  dtMinute:   result := cMinute1;
  dtMinute5:  result := cMinute5;
  dtMinute30: result := cMinute30;
  dtHour:     result := cHour;
  dtHour3:    result := cHour *3;
  dtHour6:    result := cHour *6;
  dtHour12:   result := cHour *12;
  dtDay:      result := 1;
  dtWeek:     result := 7;
  dtMonth:    result := 30;
  dtMonth2:   result := 61;
  dtMonth3:   result := 91;
  dtYear:     result := 365;
  dtYear10:   result := 3654;
  dtYear100:  result := 36540;
 else
   result := cMinute1;
 end; //case aDateTicks of
end; //function TmmGraph.GetDateStep
//-----------------------------------------------------------------------------
function TmmGraph.GetFirstDateTick(aMinDate: double): double;
var
 Year,Month,Day,
 Hour,mMin,Sec,MSec,
 xAddHour,xAddYear   : word;

begin
 DecodeDate(aMinDate,Year,Month,Day);
 DecodeTime(aMinDate,Hour,mMin,Sec,MSec);

 case FDateTickType of
  dtMinute: if Sec <> 0 then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0) +encodetime(0,1,0,0)
                        else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0);
  dtMinute5: if (mMin mod 5 <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,5 *(mMin div 5),0,0) +encodetime(0,5,0,0)
                                                else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0);
  dtMinute30: if (mMin = 0) and (Sec = 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,0,0,0)
                                          else if (mMin >= 30) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,0,0,0) +encodetime(1,0,0,0)
                                                               else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,30,0,0);

  dtHour..dtHour12: begin
   if FDateTickType = dtHour then xAddHour := 1;
   if FDateTickType = dtHour3 then xAddHour := 3;
   if FDateTickType = dtHour6 then xAddHour := 6;
   if FDateTickType = dtHour12 then xAddHour := 12;
   if (Hour mod xAddHour <> 0) or (mMin <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(xAddHour *(Hour div xAddHour),0,0,0) +encodetime(xAddHour,0,0,0)
                                                            else result := EncodeDate(Year,Month,Day) +encodetime(xAddHour,0,0,0);
  end; //dtHour..dthour12

  dtDay: if (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +1
                                                    else result := EncodeDate(Year,Month,Day);
  dtWeek: result := EncodeDate(Year,Month,Day) +8 -DayOfWeek(aMinDate);

  dtMonth: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   result := EnCodeDate(Year,Month,1);
  end; //dtMonth

  dtMonth2: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   if Month mod 2 <> 1 then AddMonth(Year,Month,1);
   result := EnCodeDate(Year,Month,1);
  end; //dtMonth2

  dtMonth3: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   while Month mod 3 <> 1 do AddMonth(Year,Month,1);
   result := EnCodeDate(Year,Month,1);
  end; //dtMonth3

  dtYear..dtYear100: begin
   if FDateTickType = dtYear then xAddYear := 1;
   if FDateTickType = dtYear10 then xAddYear := 10;
   if FDateTickType = dtYear100 then xAddYear := 100;

   if (Month > 1) or(Day > 1) or (Hour <> 0) or (mMin <>0) or (Sec <> 0) then inc(Year);
   result := EncodeDate(xAddYear *(Year div xAddYear),1,1);
  end; //dtYear..dtYear100
 else
   Result := 0;
 end; //case FDateTickType of
end; //function TmmGraph.GetFirstDateTick
//-----------------------------------------------------------------------------
function TmmGraph.GetNextDateTick(tick: double): double;
var
 year,month,day: word;

begin
 case FDateTickType of
  dtMinute: result := tick +cminute1;
  dtMinute5: result := tick +cminute5;
  dtMinute30: result := tick +cminute30;
  dtHour: result := tick +chour;
  dtHour3: result := tick +3*chour;
  dtHour6: result := tick +6*chour;
  dtHour12: result := tick +12*chour;
  dtDay: result := tick +1;
  dtWeek: result := tick +7;

  dtMonth: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,1);
   result := encodedate(year,month,1);
  end; //dtMonth

  dtMonth2: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,2);
   result := encodedate(year,month,1);
  end; //dtMonth2

  dtMonth3: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,3);
   result := encodedate(year,month,1);
  end; //dtMonth3

  dtYear: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +1,1,1);
  end; //dtYear

  dtYear10: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +10,1,1);
  end; //dtYear10

  dtYear100: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +100,1,1);
  end; //dtYear100
 else
   Result := 0;
 end; //case FDateTickType of
end; //function TmmGraph.GetNextDateTick
//-----------------------------------------------------------------------------
function TmmGraph.GetStep(aMinSteps: Word; aMin,aMax: extended): double;
var
 w,t,b,
 xLogE : extended;

begin
 xLogE := 100 /ln(1e100);

 w := aMax -aMin;

 if w <= 0 then raise Exception.Create('Error: Min >= Max in GetStep');
 t := ln(w) *xLogE;
 if t < 0 then t := t -1;
 b := exp(trunc(t *1.001) /xLogE);

 if w/B >= aMinSteps then result := b
  else if 2*w/B >= aMinSteps then result := b /2
   else if 5*w/B >= aMinSteps then result := b /5
    else if 10*w/B >= aMinSteps then result := b /10
     else if 20*w/B >= aMinSteps then result := b /20
      else if 50*w/B >= aMinSteps then result := b /50
                                  else result := b/ 100;
end; //function TmmGraph.GetStep
//-----------------------------------------------------------------------------
procedure TmmGraph.SetMinMax(var aMin,aMax,aStep: extended; aAutoStep: boolean);
var
 xMin,xMax : extended;

begin
 xMin := aMin +cEpsilon;
 xMax := aMax -cEpsilon;

 if xMin > xMax then xMin := xMax;

 if xMin = xMax then
 begin
  if xMin <> 0 then
  begin
   if xMin < 0 then
   begin
    aMin := xMin * 1.2;
    aMax := 0;
   end //if xMin < 0 then
   else
   begin
    aMin := 0;
    aMax := xMax * 1.2;
   end; //else if xMin < 0 then

   aStep := GetStep(cMinSteps,aMin,aMax);
   aMax := trunc((aMax +1/1e4) /aStep) *aStep;

   xMin := aMax;
   if xMin < 0 then messagebeep(0);
  end //if xMin <> 0 then
  else
  begin
   aMin := 0;
   aMax := 100;
   aStep := GetStep(cMinSteps,aMin,aMax);
  end; //else if xMin <> 0 then

  exit;
 end //if xMin = xMax then
 else
 begin
  aMin := xMin;
  aMax := xMax;
 end; //else if xMin = xMax then

 if aAutoStep or (aStep <= 0) or (aStep /(aMax -aMin) < 1 /cMaxSteps)
  then aStep := GetStep(cMinSteps,aMin,aMax);

 aMin := trunc(aMin /aStep) *aStep;
 if (xMin < 0) then aMin := aMin -aStep;
 if (aMin > xMin -0.01 *aStep) then aMin := aMin -aStep;

 aMax := trunc(aMax /aStep) *aStep +aStep;
 if (xMax < 0) then aMax := aMax -aStep;
 if (aMax < xMax +0.01 *aStep) then aMax := aMax +aStep;

 if (xMin > 0) and (xMin /(aMax -aMin) < 0.25)
  then aMin := 0
  else if (xMax < 0) and (-xMax /(aMax -aMin) < 0.25) then aMax := 0;

 if aMin >= aMax then raise Exception.Create('SetMinMax Error');
end; //procedure TmmGraph.SetMinMax
//-----------------------------------------------------------------------------
procedure TmmGraph.CalcYAxis;
var
 i,
 LTicks  : integer;
 xYAxis  : eYAxisT;
 xLeg    : TmmCustomGraphArea;
 RMax,
 RFactor,
 dY      : extended;

begin
 //reset YStretch
 if FHorzGrid.GridType = hgLeftRightAxis then YStretch[yaxRight] := 1;

 //calc sum for BarStyle = bsStacked, bsStacked100
 Data.ResetSUM;
 for i := 1 to Data.NumSeries do
  if Series.Visible[i] and (Series.YAxis[i] = yaxRight) then
   Data.AddToSUM(i);

 Data.CalcStatData;
 fillchar(YScale,sizeof(tYScaleRecordT),0);

 YScale.Min[yaxLeft] := 9e99;
 YScale.Max[yaxLeft] := -9e99;
 YScale.Min[yaxRight] := 9e99;
 YScale.Max[yaxRight] := -9e99;

 for i := 1 to Data.NumSeries do
 begin
  if Series.Visible[i] then
  begin
   xYAxis := Series.YAxis[i];

   if xYAxis = yaxLeft then xLeg := FLeftYAxis
                       else xLeg := FRightYAxis;

   if xLeg.AutoScaling then
   begin
    YScale.Min[xYAxis] := math.Min(YScale.Min[xYAxis],Data.Data.Statistik[i,stMin]);
    YScale.Max[xYAxis] := math.Max(YScale.Max[xYAxis],Data.Data.Statistik[i,stMax]);
   end //if xLeg.DefaultYMinMax then
   else
   begin
    YScale.Min[xYAxis] := math.Min(YScale.Min[xYAxis],xLeg.DefaultMinY);
    YScale.Max[xYAxis] := math.Max(YScale.Max[xYAxis],xLeg.DefaultMaxY);
   end; //else if xLeg.DefaultYMinMax then
  end; //if Series.Visible[i] then
 end; //for i := 1 to Data.NumSeries do

 if NumOfBarSeries > 0 then
 begin
  case FBars.Style of
   bsStacked: begin
    YScale.Min[yaxRight] := 0; //math.Min(YScale.Min[yaxRight],Data.Data.Statistik[0,stMin]);
    YScale.Max[yaxRight] := math.Max(YScale.Max[yaxRight],Data.Data.Statistik[0,stMax]);
   end; //bsStacked

   bsStacked100: begin
    YScale.Min[yaxRight] := 0;
    YScale.Max[yaxRight] := 100;
   end; //bsStacked100
  end; //case FBars.Style of
 end; //if NumOfBarSeries > 0 then

 for xYAxis := yaxLeft to yaxRight do
 begin
  if (YScale.Min[xYAxis] > YScale.Max[xYAxis]) or
    ((YScale.Min[xYAxis] = YScale.Max[xYAxis]) and (YScale.Min[xYAxis] = 0)) then
  begin
   YScale.Min[xYAxis] := 0;
   YScale.Max[xYAxis] := 100;
  end; //if YScale.Min[xYAxis] > YScale.Max[xYAxis] then

  dY := (YScale.Max[xYAxis] -YScale.Min[xYAxis]) *YStretch[xYAxis];
  YScale.Max[xYAxis] := YScale.Min[xYAxis] +dY;
 end; //for xYAxis := yaxLeft to yaxRight do

 if FLeftYAxis.AutoScaling
  then SetMinMax(YScale.Min[yaxLeft],YScale.Max[yaxLeft],YScale.Step[yaxLeft],true)
  else YScale.Step[yaxLeft] := GetStep(cMinSteps,YScale.Min[yaxLeft],YScale.Max[yaxLeft]);

 if FRightYAxis.AutoScaling
  then SetMinMax(YScale.Min[yaxRight],YScale.Max[yaxRight],YScale.Step[yaxRight],true)
  else YScale.Step[yaxRight] := GetStep(cMinSteps,YScale.Min[yaxRight],YScale.Max[yaxRight]);

 //link grid for left and right yaxis
 if FHorzGrid.GridType = hgLeftRightAxis then
  if FLeftYAxis.Visible and FRightYAxis.Visible then
   if FRightYAxis.AutoScaling then
    if FLeftYAxis.Visible and FRightYAxis.Visible then
    begin
     LTicks := round((YScale.Max[yaxLeft] -YScale.Min[yaxLeft]) /YScale.Step[yaxLeft]);
     RMax := YScale.Step[yaxRight] *LTicks;

     if RMax < YScale.Max[yaxRight] then
     begin
      RFactor := YScale.Max[yaxRight] /RMax;
      RFactor := round((RFactor /0.5) +0.5);

      YScale.Max[yaxRight] := RMax *RFactor;
      YScale.Step[yaxRight] := YScale.Step[yaxRight] *RFactor;
     end //if RMax < YScale.Max[yaxRight] then
     else YScale.Max[yaxRight] := RMax;
    end; //if FLeftYAxis.Visible and FRightYAxis.Visible then
end; //procedure TmmGraph.CalcYAxis
//-----------------------------------------------------------------------------
function TmmGraph.fy(v: double; aAxis: eYAxisT): integer;
var
 w,fScale : double;

begin
 fScale := (FChartArea.Height -1) /(YScale.Max[aAxis] -YScale.Min[aAxis]);
 w := (YScale.Max[aAxis] -v) *fScale;
 result := FChartArea.Top +round(w);
end; //function TmmGraph.fy
//-----------------------------------------------------------------------------
function TmmGraph.fx(aPoint: integer; aXValue: ValueType): integer;
var
 w : double;

begin
 case XAxis.XAxisType of
  xaFixed: result := FChartArea.Left +round(((aPoint -FScrollBar.Position) +1/2) *(BarWidth +GetPrnFactX(FBars.Gap)));
  xaFloat,xaDateTime: begin
   w := (aXValue -XScale.actMin) /XScale.actDX *FChartArea.Width;
   result := FChartArea.Left +round(w);
  end; //xaFloat,xaDateTime
 else
   Result := 0;
 end; //case XAxis.XAxisType of
end; //function TmmGraph.fx
//-----------------------------------------------------------------------------
function TmmGraph.GetXItemAtXPos(xPos: integer): integer;
begin
 //=> only for XAxis.XAxisType = xaFixed
 result := round((xPos -FChartArea.Left) /(BarWidth +GetPrnFactX(FBars.Gap)) -1/2) +FScrollBar.Position;
 if result > (Data.GetNumberOfData -1) then result := -1;
end; //function TmmGraph.GetXValueAtXPos
//-----------------------------------------------------------------------------
function TmmGraph.GetXValueAtXPos(xPos: integer): double;
begin
 //=> only for XAxis.XAxisType in [xaFloat,xaDateTime]
 result := XScale.actMin +XScale.actDX /FChartArea.Width *(xPos -FChartArea.Left);
end; //function TmmGraph.GetXValueAtXPos
//-----------------------------------------------------------------------------
procedure TmmGraph.WmSize(var Message: TMessage);
begin
 inherited;
end; //procedure TmmGraph.WmSize
//-----------------------------------------------------------------------------
procedure TmmGraph.DrawRubberBand(TopLeft,BottomRight: TPoint; aMode: TPenMode);
begin
 with FPaintBox.Canvas do
 begin
  Pen.Mode := aMode;
  Rectangle(TopLeft.X,TopLeft.Y,BottomRight.X,BottomRight.Y);
 end; //with FPaintBox.Canvas do
end; //procedure TmmGraph.DrawRubberBand
//-----------------------------------------------------------------------------
function TmmGraph.GetDblClickItem(x,y: integer): eDblClickT;
begin
 result := dcNone;
 if InRect(rect(FChartArea.Left,FChartArea.Top,FChartArea.Right,FChartArea.Bottom),x,y) then result := dcChart;
 if InRect(rect(FLeftYAxis.Left,FLeftYAxis.Top,FLeftYAxis.Right,FLeftYAxis.Bottom),x,y) then result := dcLeftAxis;
 if InRect(rect(FRightYAxis.Left,FRightYAxis.Top,FRightYAxis.Right,FRightYAxis.Bottom),x,y) and FRightYAxis.Visible then result := dcRightAxis;
 if InRect(rect(FLegend.Left,FLegend.Top,FLegend.Right,FLegend.Bottom),x,y) then result := dcLegend;
 if InRect(rect(FTitles.Left,FTitles.Top,FTitles.Right,FTitles.Bottom),x,y) then result := dcTitle;
 if InRect(rect(FXAxis.Left,FXAxis.Top,FXAxis.Right,FXAxis.Bottom),x,y) then result := dcXAxis;
end; //function TmmGraph.GetDblClickItem
//-----------------------------------------------------------------------------
procedure TmmGraph.FPaintBoxMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
var
 hstr,
 hstr1,
 hstr2    : string;
 xyScreen : TPoint;
 r        : TRect;
 dcItem   : eDblClickT;
 resID    : integer;
 xPoint,
 xRow     : integer;
 xStat    : eStatistikT;
 wText    : integer;

begin
 if FReadOnly then exit;

 dcItem := GetDblClickItem(x,y);

 if ssDouble in Shift then
 begin
  resID := FindClickedItem(x,y);
  if Assigned(FOnGraphDblClick) then
   if resID > -1 then FOnGraphDblClick(self,dcItem,FObj[resID].DataRow,FObj[resID].DataPoint,FObj[resID].StatType)
                 else FOnGraphDblClick(self,dcItem,-1,-1,stData);

  //IsSelecting := false;  //rem
  Exit;
 end; //if ssDouble in Shift then

 if (Button = mbLeft) and not (ssShift in Shift) then
 begin
  if FXAxis.XAxisType in [xaFloat,xaDateTime] then
  begin
   IsSelecting := true;
   FPaintBox.Canvas.Pen.Style := psDot;
   FPaintBox.Canvas.Pen.Color := clRed;
   FPaintBox.Canvas.Pen.Width := 1;
   FPaintBox.Canvas.Brush.Style := bsClear;
   FPaintBox.Canvas.MoveTo(X,Y);
   Origin := Point(X,Y);
   MovePt := Origin;
  end; //if FXAxis.XAxisType in [xaFloat,xaDateTime] then

  {$ifdef UseALT_LMB}
  if ssAlt in Shift then
  {$endif}
   if dcItem = dcChart then
   begin
    resID := FindClickedItem(x,y);

    if resID > -1 then
    begin
     xRow := FObj[resID].DataRow;
     xPoint := FObj[resID].DataPoint;
     xStat := FObj[resID].StatType;

     hStr := format(LTrans(cRowStr),[FSeries.Name[xRow]]);
     hstr2 := '';
     case xStat of
      stMin: hstr1 := format(LTrans(cParameterStr),[LTrans(cMinStr)]);
      stMax: hstr1 := format(LTrans(cParameterStr),[LTrans(cMaxStr)]);
      stMean: hstr1 := format(LTrans(cParameterStr),[LTrans(cMeanStr)]);
      stStdDev: hstr1 := format(LTrans(cParameterStr),[LTrans(cStdDevStr)]);
      stQ90: hstr1 := format(LTrans(cParameterStr),[LTrans(cQ90Str)]);
      stQ95: hstr1 := format(LTrans(cParameterStr),[LTrans(cQ95Str)]);
      stQ99: hstr1 := format(LTrans(cParameterStr),[LTrans(cQ99Str)]);
      stUpperLimit: hstr1 := format(LTrans(cParameterStr),[LTrans(cUpperStr)]);
      stLowerLimit: hstr1 := format(LTrans(cParameterStr),[LTrans(cLowerStr)]);
      else
      begin
       hstr1 := format(LTrans(cPointStr),[Data.GetLabelAtItemPos(xPoint)]);
       hstr2 := format(LTrans(cValueStr),[formatfloat(',0.0',Data.GetYValue(xRow,xPoint).Value)]);
      end; //else
     end; //case FObj[resID].StatType of

     if xStat in [stMin..stQ99] then hstr1 := hstr1 +formatfloat(',0.0',Data.Data.Statistik[xRow,xStat]);
     if xStat = stLowerLimit then hstr1 := hstr1 +formatfloat(',0.0',FSeries.LowerLimit[xRow]);
     if xStat = stUpperLimit then hstr1 := hstr1 +formatfloat(',0.0',FSeries.UpperLimit[xRow]);

     xyScreen.X := X +10;
     xyScreen.Y := Y +10;
     xyScreen := FPaintBox.ClientToScreen(xyScreen);

     wText := math.Max(FhwInfo.Canvas.TextWidth(hStr),FhwInfo.Canvas.TextWidth(hstr1));
     wText := math.Max(wText,FhwInfo.Canvas.TextWidth(hstr2));

     r.Left := xyScreen.X;
     r.Top := xyScreen.Y;
     r.Right := r.Left +wText +10;
     if hStr2 <> '' then
     begin
      r.Bottom := r.Top +FhwInfo.Canvas.TextHeight(hStr) *3 +3;
      FhwInfo.ActivateHint(r,hstr +#10#13 +hstr1 +#10#13 +hstr2);
     end //if hStr2 <> '' then
     else
     begin
      r.Bottom := r.Top +FhwInfo.Canvas.TextHeight(hStr) *2 +3;
      FhwInfo.ActivateHint(r,hstr +#10#13 +hstr1);
     end; //else if hStr2 <> '' then
    end; //if resID > -1 then
   end; //if dcItem = dcChart then
 end; //if Button = mbLeft then
end; //procedure TmmGraph.FPaintBoxMouseDown
//-----------------------------------------------------------------------------
function TmmGraph.FindClickedItem(x,y: integer): longint;
const
 cLineSensitivity = 2;

var
 i,y1 : integer;
 m : double;

begin
 result := -1;
 for i := low(FObj) to high(FObj) do
  with FObj[i] do
  begin
   case ItemType of
    giRectangle: if (ItemRect.Left <= x) and (ItemRect.Right >= x) and
                    (ItemRect.Top <= y) and (ItemRect.Bottom >= y) then
                 begin
                  result := i;
                  break;
                 end; //if (ItemRect.Left <= x) and (ItemRect.Right >= x) and

    giLine: begin
     try
      m := (ItemRect.Bottom -ItemRect.Top) /(ItemRect.Right -ItemRect.Left);
     except
      m := 0;
     end;   //try..except

     y1 := ItemRect.Top +round((x -ItemRect.Left) *m);

     if (abs(y1 -y) <= cLineSensitivity) and (x >= ItemRect.Left) and (x <= ItemRect.Right) then
     begin
      result := i;
      break;
     end; //if abs(y1 -y) <= cLineSensitivity then
    end; //giLine
   end; //case ItemType of
  end; //with FObj[i] do
end; //function TmmGraph.FindClickedItem
//-----------------------------------------------------------------------------
procedure TmmGraph.FPaintBoxMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X,Y: Integer);
var
 x1,x2   : integer;
 xMin    : double;
 resID   : integer;
begin
 if FReadOnly then exit;

 //if not IsSelecting then  FhwInfo.ReleaseHandle;

 if IsSelecting then //nur bei FXAxis.XAxisType in [xaFloat,xaDateTime]
 begin
  FPaintBox.Canvas.Pen.Mode := pmCopy;
  IsSelecting := false;

  //from left to right, => zoom
  if X > Origin.x then
  begin
   x1 := math.Max(Origin.x,FChartArea.Left);
   x2 := math.Min(x,FChartArea.Right);
   xMin := GetXValueAtXPos(x1); //=> keep XScale.actMin
   XScale.actMax := GetXValueAtXPos(x2);
   XScale.actMin := xMin;

   SetXScale(XScale.actMin,XScale.actMax);
  end; //if X > Origin.x then

  //from right to left, "re-zoom" to full size
  if X < Origin.x then SetXScale(Data.XMin,Data.XMax);

  Refresh;
 end //if IsSelecting then
 else
 begin

  if Button = Selection.Button then
  begin
   if Shift = Selection.Shift then
   begin
    if (GetDblClickItem(x,y) = dcChart) and (XAxis.XAxisType = xaFixed) then
    begin
      resID := FindClickedItem(x,y);

      if resID > -1 then
      begin
         FSelectedPoint := FObj[resID].DataPoint; //GetXItemAtXPos(X);
         FSelectedRow := FObj[resID].DataRow;
      end //if resID > -1 then
      else FSelectedPoint := -1;

     Refresh;
    end; //if (GetDblClickItem(x,y) = dcChart) and (XAxis.XAxisType = xaFixed) then
   end; //if Shift = [] then
  end; //if Button = mbLeft then
 end; //if IsSelecting then

 if (Button <> Selection.Button) or (Shift <> Selection.Shift) then FhwInfo.ReleaseHandle;

{
  //if Button = mbLeft then
  begin
   if Shift = [] then
   begin
    if (GetDblClickItem(x,y) = dcChart) and (XAxis.XAxisType = xaFixed) then
    begin
     lastSel := FSelectedPoint;
     resID := FindClickedItem(x,y);
     if resID > -1 then
     begin
      FSelectedPoint := FObj[resID].DataPoint; //GetXItemAtXPos(X);
      FSelectedRow := FObj[resID].DataRow;
     end //if resID > -1 then
     else FSelectedPoint := -1;

     Refresh;

    end; //if (GetDblClickItem(x,y) = dcChart) and (XAxis.XAxisType = xaFixed) then
   end; //if Shift = [] then

   if ssAlt in Shift then
   begin
   end; //if ssAlt in Shift then


   FhwInfo.ReleaseHandle;
 }
//  end; //if Button = mbLeft then
// end; //if IsSelecting then

 // OnClick Event
 if Assigned(FOnGraphClick) then begin
     resID := FindClickedItem(x,y);
     if resID > -1 then
        FOnGraphClick(self, Button, Shift, FObj[resID].DataRow,FObj[resID].DataPoint)
     else FOnGraphClick(self,Button, Shift,-1,-1);
 end;
end; //procedure TmmGraph.FPaintBoxMouseUp
//-----------------------------------------------------------------------------
procedure TmmGraph.FPaintBoxMouseMove(Sender: TObject; Shift: TShiftState; X,Y: Integer);
begin
 if IsSelecting then
 begin
  DrawRubberBand(Origin,MovePt,pmNotXor);
  MovePt := Point(X,Y);
  DrawRubberBand(Origin,MovePt,pmNotXor);
 end; //if IsSelecting then
end; //procedure TmmGraph.FPaintBoxMouseMove
//-----------------------------------------------------------------------------
function TmmGraph.GetMemoINI: TMemoINI;
begin
 result := FMemoINI;
end; //function TmmGraph.GetMemoINI
//-----------------------------------------------------------------------------
procedure TmmGraph.SetMemoINI(value: TMemoINI);
begin
 FMemoINI := value;
end; //procedure TmmGraph.SetMemoINI
//-----------------------------------------------------------------------------
procedure TmmGraph.ReadSettings;
var
 i,j,k     : integer;
 xParamStr,
 xStr      : string;
 xStat     : eStatistikT;

begin
 FMemoINI.ReadSettings;
 FBars.Style := eBarStyleT(FMemoINI.ReadInteger(Name +'.Chart','BarStyle',0));

 FLeftYAxis.DefaultMinY := FMemoIni.ReadFloat(Name+'.Chart','LeftYAxis.DefaultMinY',0);
 FLeftYAxis.DefaultMaxY := FMemoIni.ReadFloat(Name+'.Chart','LeftYAxis.DefaultMaxY',100);
 FLeftYAxis.AutoScaling := FMemoIni.ReadBool(Name+'.Chart','LeftYAxis.AutoScaling',true);
 FRightYAxis.DefaultMinY := FMemoIni.ReadFloat(Name+'.Chart','RightYAxis.DefaultMinY',0);
 FRightYAxis.DefaultMaxY := FMemoIni.ReadFloat(Name+'.Chart','RightYAxis.DefaultMaxY',100);
 FRightYAxis.AutoScaling := FMemoIni.ReadBool(Name+'.Chart','RightYAxis.AutoScaling',true);

 j := FMemoINI.ReadInteger(Name +'.Chart','ParamCount',-1);

 if j > 0 then
 begin
  Data.NumSeries := j;

  for i := 1 to j do
  begin
   xParamStr := FMemoINI.ReadString(Name +'.Params',inttostr(i),'');

   Series.ID[i]           := strtoint(GetSubStr(xParamStr,cListDelimiterChar,1));
   Series.YScalingType[i] := eYScalingTypeT(strtoint(GetSubStr(xParamStr,cListDelimiterChar,2)));
   Series.YAxis[i]        := eYAxisT(strtoint(GetSubStr(xParamStr,cListDelimiterChar,3)));
   Series.Color[i]        := strtoint(GetSubStr(xParamStr,cListDelimiterChar,4));

   xStr := GetSubStr(xParamStr,cListDelimiterChar,5);
   Series.ShowStat[i] := [];
   for xStat := stMin to stUpperLimit do
   begin
    k := ord(xStat) +1;
    if xStr[k] = '1' then Series.ShowStat[i] := Series.ShowStat[i] +[xStat];
   end; //for xStat := stMin to stUpperLimit do

   Series.LowerLimit[i] := strtoint(GetSubStr(xParamStr,cListDelimiterChar,6)) /100;
   Series.UpperLimit[i] := strtoint(GetSubStr(xParamStr,cListDelimiterChar,7)) /100;
   Series.Visible[i]    := boolean(strtoint(GetSubStr(xParamStr,cListDelimiterChar,8)));
   Series.FieldName[i]  := GetSubStr(xParamStr,cListDelimiterChar,9);
  end; //for i := 0 to j do
 end; //if j > -1 then
end; //procedure TmmGraph.ReadSettings
//-----------------------------------------------------------------------------
procedure TmmGraph.WriteSettings;
var
 i         : integer;
 xStat     : eStatistikT;
 xParamStr : string;

begin
 FMemoIni.ReadSettings;
 FMemoIni.EraseSection(Name+'.Chart');
 FMemoIni.EraseSection(Name+'.Params');

 FMemoIni.WriteInteger(Name+'.Chart','ParamCount',Data.NumSeries);
 FMemoIni.WriteInteger(Name+'.Chart','BarStyle',ord(FBars.Style));

 FMemoIni.WriteFloat(Name+'.Chart','LeftYAxis.DefaultMinY',FLeftYAxis.DefaultMinY);
 FMemoIni.WriteFloat(Name+'.Chart','LeftYAxis.DefaultMaxY',FLeftYAxis.DefaultMaxY);
 FMemoIni.WriteBool(Name+'.Chart','LeftYAxis.AutoScaling',FLeftYAxis.AutoScaling);
 FMemoIni.WriteFloat(Name+'.Chart','RightYAxis.DefaultMinY',FRightYAxis.DefaultMinY);
 FMemoIni.WriteFloat(Name+'.Chart','RightYAxis.DefaultMaxY',FRightYAxis.DefaultMaxY);
 FMemoIni.WriteBool(Name+'.Chart','RightYAxis.AutoScaling',FRightYAxis.AutoScaling);

 for i := 1 to Data.NumSeries do
 begin
  xParamStr := inttostr(Series.ID[i]) +cListDelimiterChar
              +inttostr(ord(Series.YScalingType[i])) +cListDelimiterChar
              +inttostr(ord(Series.YAxis[i])) +cListDelimiterChar
              +inttostr(Series.Color[i]) +cListDelimiterChar;

  for xStat := stMin to stUpperLimit do
   if xStat in Series.ShowStat[i] then xParamStr := xParamStr +'1'
                                  else xParamStr := xParamStr +'0';
  xParamStr := xParamStr +cListDelimiterChar
              +formatfloat('0',Series.LowerLimit[i] *100) +cListDelimiterChar
              +formatfloat('0',Series.UpperLimit[i] *100) +cListDelimiterChar
              +inttostr(ord(Series.Visible[i])) +cListDelimiterChar
              +Series.FieldName[i] +cListDelimiterChar;

  FMemoIni.WriteString(Name+'.Params',inttostr(i),xParamStr);
 end; //for i := 1 to Data.NumSeries do

 FMemoIni.WriteSettings;

end; //procedure TmmGraph.WriteSettings
//-----------------------------------------------------------------------------
{
procedure Register;
begin
 RegisterComponents('Worbis',[TmmGraph]);
end; //procedure Register
{}
//-----------------------------------------------------------------------------
procedure TmmGraph.UnselectBar;
begin
 FhwInfo.ReleaseHandle;
 FSelection.Enabled:= False;
end;
//-----------------------------------------------------------------------------
procedure TmmGraph.PrintHeaderFooter(aCanvas: TCanvas);
var xBMP : TBitmap;
    xSrcRect: TRect;
    xDestRect: TRect;

    xDPIPrinter, xDPIScreen, xFactor: integer;
    xPos, yPos, xNewWidth, xNewHeight : Integer;
    xPrntPixelHeight, xPrntPixelWidth : Integer;
    xText :String;
const
  cLogoMaxWidth = 282;
  cLogoMaxHight = 25;
begin

  xDPIPrinter := GetDeviceCaps(Printer.handle, LOGPIXELSX );  //DPI Drucker z.B. 600 dpi
  xDPIScreen  := GetDeviceCaps(GetDC(0), LOGPIXELSX );        //DPI Scree z.B. 72 / 96 dpi
  xFactor     := xDPIPrinter DIV xDPIScreen;

  xPrntPixelHeight := GetDeviceCaps(Printer.handle, VERTRES) - (20 * xFactor);
  xPrntPixelWidth  := GetDeviceCaps(Printer.handle, HORZRES) - (20 * xFactor);

  aCanvas.Font.Color := clBlack;
  aCanvas.Brush.Color:= clWhite;
  try
    if PageHeader.PrintHeader then begin
       //Position Titel
       aCanvas.Font.Size := 14;
       aCanvas.Font.Style := [fsBold];

       xText := Translate(PageHeader.Title);
       xPos := 0;
       yPos := 10 * xFactor;
       aCanvas.TextOut( xPos , yPos, xText );

       try
         xText := TMMSettingsReader.Instance.Value[cCompanyName] + '  ';
       except
         xText := '';
       end;

       aCanvas.Font.Size := 10;
       aCanvas.Font.Style := [];
       xPos := xPrntPixelWidth - aCanvas.TextWidth(xText);
       yPos := 13 * xFactor;
       aCanvas.TextOut( xPos , yPos, xText );
    end;
  finally
  end;

  try
    if PageFooter.PrintFooter then begin
      try

        if PageFooter.Logo.Graphic <> NIL then begin
          xBMP := TBitmap.Create;
          xBMP.Transparent := TRUE;
          xBMP.TransparentMode := tmAuto;
          xBMP.Assign(PageFooter.Logo.Bitmap);
          //xBMP.LoadFromFile('W:\Images\MillMaster\MillMaster_Report.bmp');
          xBMP.TransParentColor := xBMP.canvas.pixels[1,1];


          xBMP.Height := PageFooter.Logo.Height;
          xBMP.Width  := PageFooter.Logo.Width;

          //Logo size
          xNewWidth   := (xFactor * cLogoMaxWidth);
          xNewHeight  := (xFactor * cLogoMaxHight);

          xPos := 0;
          yPos := xPrntPixelHeight - xNewHeight ;

          xSrcRect  := Rect(0, 0, xBMP.Width, xBMP.Height);
          xDestRect := Rect(xPos, yPos, xNewWidth + xPos, xNewHeight + yPos);

          aCanvas.CopyRect(xDestRect, xBMP.Canvas, xSrcRect);
          xBMP.Free;
        end;
      finally
      end;

      //Position Linie & zeichen
      yPos := xPrntPixelHeight - xFactor * cLogoMaxHight - (2 * xFactor);
      aCanvas.Pen.Width:= 1 * xFactor + 2;
      aCanvas.Pen.Style:= psSolid;
      aCanvas.Pen.Color:= clBlack;
      aCanvas.MoveTo(0, yPos);
      aCanvas.LineTo(xPrntPixelWidth, yPos);


      //Position Datum
      aCanvas.Font.Size := 10;
      aCanvas.Font.Style := [];
      aCanvas.Font.Color := clBlack;
      xText := DateTimeToStr(Now) + '  ';

      yPos := xPrntPixelHeight - ((xFactor * cLogoMaxHight) Div 2) - (aCanvas.TextHeight(xText) DIV 2);
      xPos := xPrntPixelWidth - aCanvas.TextWidth(xText);
      aCanvas.TextOut( xPos , yPos, xText );

      //Position Seiten Nr.
      xText := cPage + '1' ;
      xPos := (xPrntPixelWidth Div 2)  - (aCanvas.TextWidth(xText) DIV 2);
      aCanvas.TextOut( xPos , yPos, xText );
    end;
  finally
  end;

end;
//-----------------------------------------------------------------------------


{ TPrintHeader }

constructor TPrintHeader.Create(Control: TControl);
begin
  inherited Create;
  FPrintHeader := TRUE;
end;
//------------------------------------------------------------------------------
destructor TPrintHeader.Destroy;
begin

  inherited;
end;
//------------------------------------------------------------------------------
function TPrintHeader.GetPrintHeader: Boolean;
begin
  Result := FPrintHeader;
end;
//------------------------------------------------------------------------------
function TPrintHeader.GetTitle: String;
begin
  Result := fTitle;
end;
//------------------------------------------------------------------------------
procedure TPrintHeader.SetPrintHeader(const Value: Boolean);
begin
  FPrintHeader := Value;
end;
//------------------------------------------------------------------------------
procedure TPrintHeader.SetTitle(const Value: String);
begin
  fTitle := Value;
end;
//------------------------------------------------------------------------------

{ TPrintFooter }

constructor TPrintFooter.Create(Control: TControl);
begin
  inherited Create;
//  FControl := Control;
  FPrintFooter := TRUE;
  mImage := TImage.Create(Control);
  Logo := NIL;
end;
//------------------------------------------------------------------------------
destructor TPrintFooter.Destroy;
begin
  mImage.Free;
  inherited;
end;
//------------------------------------------------------------------------------
function TPrintFooter.GetImage: TPicture;
begin
  Result := mImage.Picture;
end;
//------------------------------------------------------------------------------
function TPrintFooter.GetPrintFooter: Boolean;
begin
  Result := FPrintFooter;
end;
//------------------------------------------------------------------------------
procedure TPrintFooter.SetImage(const Value: TPicture);
begin
  mImage.Picture := Value;
end;

//------------------------------------------------------------------------------
procedure TPrintFooter.SetPrintFooter(const Value: Boolean);
begin
  FPrintFooter := Value;
end;
//------------------------------------------------------------------------------





end. //mmGraph.pas

