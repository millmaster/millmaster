inherited frmFixBorderFrame: TfrmFixBorderFrame
  Height = 37
  object mmLabel1: TmmLabel [0]
    Left = 66
    Top = 12
    Width = 58
    Height = 13
    Caption = '(*)Grenzwert'
    FocusControl = edLimitValue
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laLimitValueUnit: TmmLabel [1]
    Left = 192
    Top = 11
    Width = 6
    Height = 13
    Caption = '0'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edLimitValue: TmmEdit [2]
    Left = 128
    Top = 8
    Width = 57
    Height = 21
    Color = clWindow
    TabOrder = 0
    Text = '10'
    Visible = True
    OnChange = GUIChanging
    OnExit = GUIChanged
    Alignment = taRightJustify
    AutoLabel.Control = mmLabel1
    AutoLabel.Distance = 4
    AutoLabel.LabelPosition = lpLeft
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
  end
  inherited mmTranslator1: TmmTranslator
    TargetsData = (
      1
      1
      (
        '*'
        'Caption'
        0))
  end
end
