{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseGlobal.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains common data types and definitions of both
|                 system: MillMaster Loepfe and MillMaster OE. Do not implement any
|                 independence types or constants which are not for both system.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.06.1998  1.00  Wss | File created
| 19.10.1998  1.01  Wss | cJobRecSize ist infolge des Alignments 10
| 23.10.1998  1.02  Wss | cDefaultTimeoutTickerTime implemented
|                       | TJobTimeoutRec and constant array implemented
| 28.10.1998  1.03  Wss | divided into second file: BaseInclude.inc
| 29.10.1998  1.04  Wss | System dependend code part splitted into a separated INC file: BaseGlobalCode.inc
| 04.11.1998  1.05  Nue | Umfeld fuer JobTimer eingefuegt.
| 17.11.1998  1.06  Wss | Auf TmmXXX Klassen umgestellt
| 13.01.1999  1.06WssNue| FormatMMErrorText added (overloaded)
| 26.01.1999  1.07  Wss | Function GetRegString() implemented
| 15.02.1999  1.08  Wss | Function GetRegInteger() implemented
| 25.02.1999  1.09  Nue | ThreadCount for JobHandler set from 2 to 3
| 26.02.1999  1.10  Mg  | New error type: etTXNDrvError
| 05.03.1999  1.11  Mg  | Process type WSCRPCHandler inserted
| 08.03.1999  1.12  Mg  | TMMGuardCommand enlarged with commands for initialization state
|                       | State of thread to TMMProcessState
| 23.03.1999  1.13  Wss | Splitted to LoepfeGlobal.pas
| 14.04.1999  1.14  SDO | Function TimeStampToMin() implemented
| 16.04.1999  1.15  Mg  | TJobIDList and cMaxJobs from BaseInclude to BaseGlobal
| 06.09.1999  1.16  Mg  | TMMGuardCommand : gcSQLRebooted inserted
| 15.09.1999  1.17  Nue | New Jobs added: jtSetProdID,jtStartGrpEvFromMa,jtStopGrpEvFromMa.
| 10.01.2000  1.18  Nue | New Job added: jtProdGrpUpdate added.
| 24.02.2000  1.19  Wss | BaseInclude and BaseCodeInc to BaseGlobal included.
| 01.03.2000  1.20  Nue | Slip in jtProdGrpUpdate added.
| 13.03.2000  1.21  Mg  | TMMClientRec : ServerName from array [0..30] of char -> TString30
|                   Nue | jtGetSettingsAllGroups inserted.
|                   Nue | TSendEventToClients expanded with MachineID.
| 17.03.2000  1.22  Mg  | TSettingsArrin ResponseRec inserted
| 21.03.2000  1.23  Nue | TStartGrpEv and TSetMaConfig and TGetSettings field MachineGrp inserted.
| 18.04.2000  1.24  Nue | TStartGrpEv and TSetMaConfig and TGetSettings field MachineGrp inserted.
| 04.05.2000  1.25  Mg  | cMachTypNames inserted
| 05.05.2000  1.26  Mg  | TProdGrpUpdate : ThreadCnt inserted
| 22.05.2000  1.27  Mg  | MachineID in TMMClientRec inserted
| 05.06.2000  1.28  Nue | grGetSettingsAllGroups inserted.
|                       | GetMaAssign renamed to MaEqualize. Old GetMaAssign only for getting Ma assigns used.
| 13.06.2000  1.28  Wss | RepCount for GetZESpdData changed to 0
| 20.06.2000  1.28  Wss | Tmo and Rep changed for jtMaEqualize
| 11.07.2000  1.29  Nue | New Job added: jtDBDump added.
| 14.09.2000  1.30  Nue | Job TSetMaConfig changed.  (No fullcompilation necessary!)
| 19.09.2000  1.23  Wss | TIPCClientDef record extened with IPCClient write timeout parameter
| 03.10.2000  1.24 NueKr| Constants moved from BaseGlobal to YMParaDef: cZESpdGroupLimit
|                       | and cWSCSpdGroupLimit.
| 30.10.2000  1.25 NueKr| Typ TMachTyp and constant cMachTypNames moved from BaseGlobal to YMParaDef
| 15.02.2001  1.26 Nue  | TProdGrpNameT added.
| 15.03.2001  1.27 Wss  | new enum in TApplMsg added: amQOfflimit and record extended
| 26.03.2001  1.28 Nue  | Fields ShiftcalID and ShiftInDay in
|                       | TDataAquEv and TGenDWData added.
| 28.05.2001  1.29 Wss  | MsgListRep = 1 for GetZESpdData, GetExpZESpdData
| 06.06.2001  1.30 Nue  | MMUserName in type TStartGrpEv added
|                       | jtBroadcastToClients, amSystemInfo and
|                       | ccMMSystemInfo :(NextInterval, NextShift: TDateTime; DataAquisitionBusy : Boolean); added.
| 11.06.2001  1.31  Nue | Modifications because of DataCollection-Handling. (msNoDataCollection replaced to msFiller)
| 26.06.2001  1.32  Nue | In TProdGrpInfo field c_style_id from word to smallint
| 12.09.2001  1.33  khp | // Fuer Informator V5.3x Ttext100= array[1..100]of Char
| 13.09.2001  1.34  Nue | YM_Set_Name and YM_Set_Changed added.
| 18.09.2001  1.35 NueKr| TText50 to YMParaDef and TText100 from BaseGlobal to AC338INFClass
| 07.11.2001  1.36  Nue | New Job added; jtGetZEReady , 1a: Checks after MaOnline, if ZE is ready.
| 22.11.2001  1.37  Nue | grZEStopDuringAquisation added.
| 12.12.2001  1.38  Wss | cProdGrpStartModeArr string array added for use in Hostlink
| 23.01.2002  1.39  Nue | Release 2.03.01 erstellt. c_YM_set_name: TText50; in TProdGrpInfo added.
| 07.03.2002  1.40  Nue | cGetZEReadyTimeout added and inserted in job GetZEReady (before timeout was 60s).
| 14.03.2002  1.41  Nue | TProdGrpInfo.c_prod_name from TText50 to TText100
| 15.11.2002  1.41  Wss | Enumeration amMaAssChanged -> amMachineStateChange
| 05.12.2002  1.42  Nue | Global Variable gMachIDJobTyp added
| 10.12.2002  1.42  Nue | aMsgBox eingef�gt bei:procedure SystemErrorMsg_(aMsg: String; aOwner: TWinControl = Nil; aMsgBox = True);
| 17.03.2004  2.00  Wss | CloneJob, CopyJob hinzugef�gt
| 14.05.2004        Lok |  hinzugef�gt
| 19.05.2004        Wss | cBinBufferSize hinzugef�gt
|  Juni 2004        Wss | - LX Typen/Records mal hinzugef�gt
                          - Jeder TXNReader bekommt eine Verbindung zum TXNWriter
| 17.02.2005        Nue | cGUISensingHeadClassNames added.
| 22.02.2005        Wss | TTxnNodeState zu TNodeState ge�ndert und TTxnNodeState auf diese Referenziert
| 03.03.2005        Wss | Arrays f�r Gruppen und Settings sind nun definitiv 0-Basiert
| 23.03.2005        Wss | Member MaConfigXML in ResponseRec f�r MaConfig Antwort hinzugef�gt
| 26.07.2005        Wss | MsgController: ConnectTo = ttLXWriter hinzugef�gt
| 18.07.2005        Nue | cNetTypeEthernet hinzugef�gt
| 13.10.2005        Nue | cNetTypNames hinzugef�gt
| 08.10.2006  2.01  Nue | TGroupState gsMemory zugef�gt wegen Memories in LX-Handler.
| 17.10.2006        Nue | IPICalcOnMachine in TXMLSettingsRec und TWscNetNodeRec zugef�gt.
| 08.02.2007        Nue | 'InfPPCAC5' zugef�gt.
| 18.12.2007  2.02  Nue | TK ZENITC.. zugef�gt.
| 21.02.2008        Nue | Neue Maschinen in TMachineType zugef�gt. (mtAC5, mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar)
| 05.03.2008        Nue | Umbennen der TKZenitCxx: TKZenitCF in TKZenitFC; TKZenitCFP in TKZenitFPC
| 18.03.2010        Nue | Erweiterungen wegen neuen P2-TK's TK_ZenitFP2 und TK_ZenitFP2C.
|==========================================================================================}

//**************************************************
// Last change: 21.02.2008/Nue (V5.04.00)
//**************************************************

unit BaseGlobal;
{$MINENUMSIZE 2}
interface

uses
  LoepfeGlobal, mmThread, Classes, Controls, Forms, SysUtils, Windows,
  IPCClass, IPCUnit, YMParaDef, MMUGlobal, XMLDef;

//------------------------------------------------------------------------------
// Compiler constants (switches) for conditional compiling
//------------------------------------------------------------------------------
const
  cADODBAccess = True; // Switch for using ADO access to SQL-Server instead of BDE

//------------------------------------------------------------------------------
// miscellaneos constants without any group
//------------------------------------------------------------------------------
type
  // identifies the job to which NetType it belongs
  // The ordernumber has to fit with c_net_id of DB table t_net
  TNetTyp = (ntNone, ntTXN, ntWSC, ntLX, ntCI);
  TNetTypSet = set of TNetTyp;

const
  cNetTypeEthernet          : TNetTypSet=[ntWSC, ntLX];
  cMainChannelIndex         = 1; // index of IPCClient to connect to BaseMain channel
  cChannelMainTimeout       = 3000; // ms
  cChannelWaitForever       = -1; // = wait forever
  cDefJobTimeout            = 600000; // ms
  cIPCClientWriteTimeout    = 60000; //60s timeout for nonblocking write
  cDefMsgRep                = 0; // no repetition to request job again
  cDefOffset                = 60000; // Timeout of 1min
  cDefMsgTimeout            = 180000; // ms -> 3 minutes
  cDefaultTimeoutTickerTime = 20000; // ms
  cGetZEReadyTimeout        = 210000; // ms  Max. time until ZE has to be ready, otherwise is Machine offline
  cMaxWaitTimeToKill        = 1000; // ms
  cThreadStartupTimeout     = 300000; // ms worst case of MillMaster Startup: 5 minutes
  cUnknownJobID             = 0; // Jobs generated by machine or from NetHandler
//  cBinBufferSize            = 400;
  // (ZE Tmo - Msg Tmo) div 2 = (240 - 180) div 2 = 30; 30s �brig um Daten auf DB zu speichern bevor ZE Timeout abl�uft
  cMsgRepeatTimeout         = 30000;  // ms -> 30s
  cMaxMsgRepeatTrigger      = 5;

const
  cWorkerSessionName = 'SessionWorker_';
  // wenn mehrere XMLSettings in einer StringList gespeichert sind, dann wird
  // per .Values['Settings1'] darauf zugegriffen -> auch in Assignment verwenden
  cXMLSettingIniName = 'Settings%d';

//------------------------------------------------------------------------------
// Konstanten, welche ans GUI ausgegeben werden
//------------------------------------------------------------------------------
const
  cNetTypNames: array[TNetTyp] of string = ('None', // = 0
                                            'TXN',  // = 1
                                            'WSC',  // = 2
                                            'LX',   // = 3
                                            'CI');  //   cNetTypNames


  cGUISensingHeadNames: array[TSensingHead] of string = ('Unknown', // = 0
                                                      'TK830', // = 1
                                                      'TK840', // = 2
                                                      'TK850', // = 3
                                                      'TK870', // = 4
                                                      'TK880', // = 5
                                                      'TK930F', // = 6
                                                      'TK940BD', // = 7
                                                      'TK930H', // = 8
                                                      'TK940F', // = 9
                                                      'TK930S', // = 10
                                                      'TKZenit', // = 11
                                                      'TKZenitF', // = 12
                                                      'TKZenitFP', // = 13
                                                      'TKZenitC', // = 14
                                                      'TKZenitFC', // = 15
                                                      'TKZenitFPC', // = 16
                                                      'TKZenitFP2', // = 17
                                                      'TKZenitFP2C');//   cGUISensingHeadNames

  cGUISensingHeadClassNames: array[TSensingHeadClass] of string = ('Unknown', // = 0
                                                                'TK8x', // = 1
                                                                'TK9xFS', // = 2
                                                                'TK9xH', // = 3
                                                                'TK9xBD', // = 4
                                                                'TKZenit', // = 5
                                                                'TKZenitF', // = 6
                                                                'TKZenitFP', // = 7
                                                                'TKZenitC', // = 8
                                                                'TKZenitFC', // = 9
                                                                'TKZenitFPC',// = 10
                                                                'TKZenitFP2', // = 11
                                                                'TKZenitFP2C');//   cGUISensingHeadNames

  cGUISWOptionNames: array[TSWOption] of string = ('None', // = 0
                                                'Base', // = 1
                                                'Quality', // = 2
                                                'Imperfection', // = 3
                                                'LabPack');//   cSWOptionNames

  cGUIFrontTypeNames: array[TFrontType] of string = ('None', // = 0
                                                  'ZE80', // = 1
                                                  'ZE800', // = 2
                                                  'ZE900', // = 3
                                                  'ZE80i', // = 4
                                                  'ZE800i', // = 5
                                                  'ZE900i', // = 6
                                                  'Inf68K', // = 7
                                                  'InfPPC', // = 8
                                                  'InfPPCAC5', // = 9
                                                  'ZEUnknown');//   cGUIFrontTypeNames

//------------------------------------------------------------------------------
// Definition der Maschinentypen, ben�tigt f�r MaConfig und Assignment
//------------------------------------------------------------------------------
type
  //ACHTUNG: Neue Elemente bei dem Typ TMachineType IMMER hinten anh�ngen, sonst gibts mit bestehenden MM-Anlagen ein Chaos,
  //         da die Ordinalzahl des entsprechenden Elementes unter t_machine.c_machine_type auf der DB abgelegt ist!! (Nue)
  TMachineType = (mtGeneric, mtAC138, mtAC238_146, mtAC238_147, mtAC338,
                  mtSavioEspero, mtSavioOrion, mtMurata7_2, mtMurata7_5, mtMurata21C,
                  mtAC5, mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar);       //Nue:21.02.08
  TMachineTypeRec = record
    Name: String;
    Manufacturer: String;
    NetSet: TNetTypSet;
  end;
  
const
  cMachineTypeArr: Array[TMachineType] of TMachineTypeRec = (
    (Name: 'Generic';       Manufacturer: 'Generic';     NetSet: [ntTXN, ntWSC, ntLX]),
    (Name: 'AC138';         Manufacturer: 'Schlafhorst'; NetSet: [ntTXN, ntLX]),
    (Name: 'AC238/146';     Manufacturer: 'Schlafhorst'; NetSet: [ntTXN, ntLX]),
    (Name: 'AC238/147';     Manufacturer: 'Schlafhorst'; NetSet: [ntTXN, ntLX]),
    (Name: 'AC338';         Manufacturer: 'Schlafhorst'; NetSet: [ntWSC]),
    (Name: 'Savio Espero';  Manufacturer: 'Savio';       NetSet: [ntTXN, ntLX]),
    (Name: 'Savio Orion';   Manufacturer: 'Savio';       NetSet: [ntTXN, ntLX]),
    (Name: 'Murata 7-2';    Manufacturer: 'Murata';      NetSet: [ntTXN, ntLX]),
    (Name: 'Murata 7-5';    Manufacturer: 'Murata';      NetSet: [ntTXN, ntLX]),
    (Name: 'Murata 21C';    Manufacturer: 'Murata';      NetSet: [ntTXN, ntLX]),
    (Name: 'AC5';           Manufacturer: 'Schlafhorst'; NetSet: [ntWSC]),             //Nue:02.21.08
    (Name: 'Savio Espero Nuovo'; Manufacturer: 'Savio';  NetSet: [ntTXN, ntLX]),       //Nue:02.21.08
    (Name: 'Savio Excello'; Manufacturer: 'Savio';       NetSet: [ntTXN, ntLX]),       //Nue:02.21.08
    (Name: 'Savio Polar';   Manufacturer: 'Savio';       NetSet: [ntTXN, ntLX])        //Nue:02.21.08
  );
//------------------------------------------------------------------------------
// constants and types for CleanUp event and diagnostics
//------------------------------------------------------------------------------
const
  cMaxJobs = 1000;
type
  // List of job id's used to clean up the data pool and for diagnostics
  TJobIDList = array[0..cMaxJobs-1] of record
    JobID: Word;
    JobTyp: Word
  end;
  // JobTyp is Word because the JobType of Loepfe and Barco are different

//------------------------------------------------------------------------------
// type information's for every SubSystem and thread
//------------------------------------------------------------------------------
type
  // Identifications for each thread and his interface
  TThreadTyp = (ttNone,
    ttJobList, // virtual thread, not realy used
    ttMsgList, // virtual thread, not realy used
    ttMMGuardMain, ttMMGuardWatchDog, ttMMGuardTimer,
    ttMMClientMain, ttMMClientReader,
    ttJobMain, ttJobManager, ttJobController, ttJobTimer,
    ttMsgMain, ttMsgController, ttMsgDispatcher, ttMsgTimer,
    ttStorageMain, ttDataPoolWriter, ttQueueManager, ttWorker, ttExpressWorker,
    ttTimeMain, ttJobExecute, ttTimeTicker,
    ttTXNMain, ttTXNWriter, ttTXNControl, ttTXNRead1, ttTXNRead2, ttTXNRead3,
    ttWSCMain, ttWSCWriter,
    ttLXMain, ttLXWriter,
    ttCIMain, ttCIWriter);

//------------------------------------------------------------------------------
// Error Codes
//------------------------------------------------------------------------------
type
  // To raise a MillMaster Exception
  EMMException = class(Exception);

  // Error codes for MillMaster SubSystems and all Applications
  TErrorTyp = (etNoError, // No error occured
    etDBError, // Database error
    etNTError, // Windows NT error
    etMMError, // self defined error from NT error list
    etTXNDrvError); // self defined error from Texnet device driver

  // Error structure
  TErrorRec = record
    ErrorTyp: TErrorTyp;
    Error: DWord; // Contains error from NT error list or DB error list
    Msg: shortstring; // Contains the error msg. from developer or from the database
  end;

  //****************************************************************************
  // HAS TO BE COMPLETED BY BARCO AND LOEFPE
  // Type to send the events to MMClient
  TApplMsg = (amNoMsg,
    amMachineStateChange,
    amMulMaDataReady,
    amOneMaDataReady,
    amOfflimitsReady,
    amStartAquisation, // Dataaquisation of Machinedata begins
    amEndAquisation, // Dataaquisation of Machinedata has finished
    amQOfflimit, // message sent from StorageHandler
    amSystemInfo); // message sent from JobHandler
  //****************************************************************************
  // Type to send the events from MMClient and applications to JobHandler
  TBroadToClientMsg = (bcNoMsg, bcClientStarted); //New client started
  //****************************************************************************

//------------------------------------------------------------------------------
// miscellaneos constants without any group
//------------------------------------------------------------------------------
const
  cAddOnChannelName = 'MM'; // 'MM' this constant has to be set different for OE
  cAddOnServiceName = 'Loepfe'; // used to see the diference between Barco Service and Leopfe Service
{ !!! EXPORTED TO LoepfeGlobal.pas
  cSecurityLevelCount       = 10;       // Level 0 to 10: 0 = lowest access, 10 = highest access
}

//------------------------------------------------------------------------------
// constants to write to Windows NT EventLog
//------------------------------------------------------------------------------
const
  cEventLogServerNameForSubSystems = '.';
  cEventLogClassForSubSystems      = 'MillMaster';
  cEventLogClassForSubSystemsDebug = 'MillMasterDebug';
  cEventLogClassForMMGuard         = 'MMGuard';

//------------------------------------------------------------------------------
// any constants to access the registry
//------------------------------------------------------------------------------
{ !!! EXPORTED TO LoepfeGlobal.pas
const
  cRegBarcoPath         = '\SOFTWARE\BARCO';
  cRegBarcoAddons       = cRegBarcoPath + '\Addons';      // RegPath for Floor to register Plugins
  cRegBarcoLocalePath   = cRegBarcoPath + '\Locale';      // RegPath of language information
  cRegBarcoLanguage    = 'Language';           // String

{}

//------------------------------------------------------------------------------
// any constants to access the registry of LOEPFE
//------------------------------------------------------------------------------
const
  // HKEY_LOCAL_MACHINE: constants of registry path and names
  //========================================================
  // MMGuard
  cRegMMAdminGroupName = 'AdminGroup'; // String
  cRegMMAlertTime      = 'AlertTime'; // Number
  cRegMMErrResetTime   = 'ErrResetTime'; // Number
  cRegMMNumOfProcesses = 'NumOfProcesses'; // Number
  cRegMMProcess        = 'Process%d'; // String
  cRegMMRetrayTime     = 'RetrayTime'; // Number
  cRegRunImageFile     = 'RunImageFile'; // String
  cRegStartMMIm        = 'StartMMIm'; // Boolean
  cRegWkstAler         = 'Wkst_Alert'; // String

  // MMGuard\Debug
  cRegNetTyp           = 'NetTyp'; // Number
  cRegSimulateMMGuard  = 'SimulateMMGuard'; // Boolean
  cRegDebugMsg         = 'DebugMsg'; // Boolean

  // Security
  cRegMMUserGroupName  = 'UserGroup'; // String

  // MM-Login & MM-Client
  cRegMMServer         = 'MMServer';

const
  // General constants
  cMaxProdID            = -1;
  cMinProdID            = low(integer);

  cMaxSpdDataArraySize  = 2000;
  cMaxGrpDataArraySize  = 2000;

// Texnet Const
  cMinTxnNodeID         = 128; // gueltige Texnet-Adressen: 128..239
  cMaxTxnNodeID         = 239;
  cMaxNodeperTxnAdapter = 31; // Max. Anzahl nodes pro Adapter
  cMaxTxnAdapter        = 3; // Max. Anzahl Texnet-Adapter im Host
  xcBlockTimeOut        = 255;  // Ist in TXN_glbDef definiert

// WSC AC338 Const
  cFirstMachPos         = 0;
  cMaxWscMachine        = 100;
  cMaxLXMachine         = 100;

type
//wss  TSpdID      = 1..1000;
//wss  TMachID     = 1..32767;
  TIntID      = 1..255;
  TShiftID    = 1..2147483647;
  TShiftCalID = 1..255;
  TProdGrpID  = Smallint;
  BITSET8     = set of 0..7;
  BITSET16    = set of 0..15;
  TString30   = string[30];
  TString255  = string[255];

const
  cDataCollectionOn = 1; //Has to be synchronized with DB. 1=DCOn, 2=DCOff
//.......................................
type
  //TIPOption = ( ioNotDefined, ioIPNO, ioIPOld, ioIPNew );
  TProdGrpState = (psUndefined, psOrdinaryAquis, psNotStarted, psInconsistent,
    psStopped, psCollectingData, psStopping, psForcedDBStop, psForcedZEStop);
const
  cProdGrpStateArr: array[TProdGrpState] of string = (
    'Undefined', 'Started by MM', 'Not started', 'Started at machine',
    'Stopped', 'Collecting data', 'Stopping', 'Forced stop by DB', 'Forced stop by machine');
//.......................................
type
  TProdGrpStartMode = (smUndefined, smMM, smZE,
    smRange, //, smRange (Forced start cause of rangechange on MM)
    smMMStop, //, smMMStop (Forced start cause of stop on MM)
    smZEStop); //, smZEStop (Forced start cause of stop on ZE(Only for AC338 available))
const
  cProdGrpStartModeArr: array[TProdGrpStartMode] of string = (
    'Undefined', 'MM', 'ZE', 'Range', 'MMStop', 'ZEStop');
//.......................................
type
  TMachState = (msUndefined, msRunning, msFiller (*former msNoDataCollection*), msOffline, msInSynchronisation);
const
  cMachStateArr: array[TMachState] of string = ('Undefined', 'Running', 'No data collection', 'Offline', 'In synchronisation');
//.......................................
type
  //@@@@@@@ Gleicher Typ wie in AC338INFClass ==>> Synchronisieren!!!!! Nue:14.3.02
  TText100     = array[1..100] of Char; // Fuer Informator V5.3x
  TArrayOfChar = Array[0..0] of Char;

  TProdGrpInfo = record
    c_prod_id: Longint; //not null
    c_order_position_id: Byte; //not null
    c_order_id: Word; //not null
    c_machine_id: Word; //not null
    c_style_id: smallint; //not null from word to smallint Nue 26.06.01
//wss    c_clear_type: Byte; //not null
    c_YM_set_id: Word;
    c_YM_set_name: TText50; // 31.01.02 Nue
    c_prod_name: TText100; // not null,  /* ProdGrp name 20Signs: optional, usefull in WindingMaster*/
    c_m_to_prod: Real;
//   c_act_yarncnt: Word;            //used for WindingMaster
//   c_yarncnt_unit: Byte;   //not null,    /*1=metr. 2=engl. 3=tex. (redundant in DispoMaster but used for WindingMaster)*/
//   c_nr_of_threads: Byte;  //nr of threads
    c_slip: Word; // Range (0,500..1.500)*1000
    c_order_position_prod_id: DWord; //not null,/* Code aus OrderPos. und ProdGrp.innerhalb OrderPos. */
    c_style_name: TText50; // not null,  /* Style name 20Signs: For fast access from MM-Applic.*/
    c_order_position_name: TText50; //not null,/* order_position name: For fast access from MM-Applic.*/
  end;

  // Array of spindeldata. For every kind of machine and net
//26.5.05 wss: wird nicht mehr ben�tigt
//  PSpdDataArr = ^TSpdDataArr;
//  TSpdDataArr = array[0..cMaxSpdDataArraySize] of byte;

  // Array of prodgroupdata. For every kind of machine and net
  PGrpDataArr = ^TGrpDataArr;
  TGrpDataArr = array[0..cMaxGrpDataArraySize] of byte;

  TJobTimeoutRec = record
    JobListTmo: DWord;
    MsgListTmo: DWord;
    MsgListRep: Word;
  end;

  TNodeState = (nsOffline, nsOnline, nsNotValid);
  // Nodelist for Texnet
  TTxnNodeState = TNodeState;
  TNodeID = cMinTxnNodeID..cMaxTxnNodeID; // NodeID 128..239
  TTxnNetNodeRec = record
    AdapterNo: Byte; // TexnetadapterNo 1..3
    MachID: Integer; // Maschinen ID: if 0 then no machine defined on the node
    MachState: TTxnNodeState; // 0,1,2:(nsOffline, nsOnline, nsNotValid)
    MapName: TString30;
  end;

  // Nodelist for WSC 338
  TWscNodeState = TNodeState;
  TWscNetNodeRec = record
    IpAdresse: array[0..15] of Char; // IpAdress der Maschine
    MachID: Integer; // Maschinen ID: if 0 then no machine defined on the node
    MachState: TWscNodeState; // 0,1,2:(nsOffline, nsOnline, nsNotValid)
    MapName: TString30;         // overruled MapID f�r die Settings
    Overruled: Boolean;       // True wenn die MapID per DB vorbestimmt wird
    IsZenit: Boolean;         // True wenn die ZENIT-Funktionalit�t vorhanden ist
    IPICalcOnMachine: Boolean; //True, wenn IPI-Werte bereits auf dem Frontend verrechnet werden
    CheckNewerMapfile: Boolean; //True, wenn bei Overrule Mapfile das neuere zwischen Overrule und das auf der Maschine genommen wird.
    MaxAnzGroups: Integer;     //Anzahl m�glicher Gruppen auf WSC (Default 6)
  end;

  // Nodelist for LX
  TLXNodeState = TNodeState;
  TLXNetNodeRec = record
    IpAdresse: array[0..15] of Char; // IpAdress der Maschine
    MachID: Integer; // Maschinen ID: if 0 then no machine defined on the node
    MachState: TLXNodeState; // 0,1,2:(nsOffline, nsOnline, nsNotValid)
    MapName: TString30;
    Overruled: Boolean;       // True wenn die MapID per DB vorbestimmt wird
    IsZenit: Boolean;         // True wenn die ZENIT-Funktionalit�t vorhanden ist
    IPICalcOnMachine: Boolean; //True, wenn IPI-Werte bereits auf dem Frontend verrechnet werden
    CheckNewerMapfile: Boolean; //True, wenn bei Overrule Mapfile das neuere zwischen Overrule und das auf der Maschine genommen wird.
  end;

  TTxnNetNodeList = array[cMinTxnNodeID..cMaxTxnNodeID] of TTxnNetNodeRec;
  TWscNetNodeList = array[cFirstMachPos..cMaxWscMachine] of TWscNetNodeRec;
  TLXNetNodeList  = array[cFirstMachPos..cMaxLXMachine] of TLXNetNodeRec;

  // Record to send the node items to NetHandlers
  TNodeItems = record
    case TNetTyp of
      ntTXN: (TxnNetNodeList: TTxnNetNodeList);
      ntWSC: (WscNetNodeList: TWscNetNodeList);
      ntLX:  (LXNetNodeList: TLXNetNodeList);  //nue
      ntCI: ();
  end;

  // Declaration to send changes from Handlers to Basesystem
const
  // Wird f�r das Modify Feld im Assign-Record verwendet
  cSpindle_range = 0; // Bit 0: Spindle_range
  cParameter     = 1; // Bit 1: Parameter
  cAdjusted      = 2; // Bit 2: Adjusted
  cStart         = 3; // Bit 3: Start    /338
  cStop          = 4; // Bit 4: Stop     /338 Wird gesetzt bei Wechsel in den State Definiert
  // ??
  cKeyLocked     = 0; // Bit 0: State of Key entry: 0 = unlocked, 1 = locked
//  cInitialReset  = 1; // Bit 1: 1 = InitialReset
type

  TDecEvent = (deNone, deInitialReset, deReset, deEntryLocked, deEntryUnlocked, deRange, deSettings, deAdjust,
    (*Additional declarations*) deAssignComplete, deStart, deStop);
  TGroupState = (gsfree, gsDefined, gsInProd, gsLotChange, gsMemory{Nue:8.10.06});

  TProdGrpNameT = (pnNone, pnAutoNameAndID, pnAutoNameAndDate, pnAutoName, pnIndividual); //Used for the prodgroupnaming when started from GUI

  TProdGrpData = record
    ProdGrpInfo: TProdGrpInfo;
//NUETmp    ProdGrpYMPara: TProdGrpYMParaRec;
  end;

  TMaAssign = record
    Event: TDecEvent;
    MachState: BITSET16; //New 27.03.00 Nue
//    Groups: array[0..cZESpdGroupLimit-1] of record
    Groups: array[0..cMaxGroupMemoryLimit-1] of record
      Modified: BITSET16;
      GroupState: TGroupState; //Used for AC338 New 27.03.00 Nue
      ProdId: Longint;
      SpindleFirst, SpindleLast: Word;
      Color: Integer; //Color for representation on floor (Nue:17.5.2000)
      ProdGrpParam: TProdGrpData; // Used to get ProdGrpData from Machine
    end;
  end;

  TMaEqualizeReason = (grGetNodeList, grZEOnline, grZEAssigComplete, grZEColdStart, grZEStopDuringAquisation);

//Description of the different job classes: (Description starts when JobC has read the job from joblist)
// Jobs containing a number have been coded completely
// Legend: MsgC=MsgController, MsgD=MsgDispatcher
//         JobC=JobController, DC=DispatchContinousJobM=JobManager
//         StoH=StorageHandler, NetH=NetHandler
// 1: MsgC-NetH-MsgD-MsgC-JobC(DC(MsgComplete))-StoH-JobC(DC(JobSuccessful))->Delete
// 1a: MsgC-NetH-MsgD-MsgC-JobC(DC(MsgComplete))->Delete
// 2: StoH-JobC(DC(JobSuccessful))->Delete
// 2a: JobM-JobC-StoH-JobC(DC(JobSuccessful))->Delete
// 3: MsgC-NetH-MsgD-MsgC-JobC(DC(MsgComplete))------------------------------------------
//                   JobM(AnalyseJob)-(Loop JobC(DC(JobFinished) til MsgComplete arrived)-->Delete
// 4: JobM(AnalyseJob)-JobC(DC(JobFinished)-->Delete
// 5: MsgC-(Datapool)-JobC->Delete
// 6: JobC->Delete (No entry in JobList)
// 7: From JobM-JobC->Delete (No entry in JobList)
// 8: Only to JobM (Then changed to another job)
// 9: From NetH-MsgD-JobM (Then changed to another job eventually)
// x: Special

  TJobTyp = (// common job definition of both systems
    jtNone,
    jtCleanUpEv, // 5: Cleanprocedure for datapool
    jtDelJobID, // 6: JobID to delete in datapool
    jtJobFailed, // x: The job failed (Error)
    jtJobSuccessful, // x: Delete entries in Job- and MsgList for the job
    jtMsgComplete, // x: Job in MsgHandler done
    jtMsgNotComplete, // x: Job in MsgHandler not finished correctly
    jtMsgReceived, // Data from machine received
    jtNewJobEv, // 7: New job into JobList from JobManager
    jtTimerEvent, // 8: Timer has expired, make an event
             // individual job definition for MillMaster Loepfe
{10}jtTest,
    jtTestConfirm,
    jtTestStore,
    jtAdjust, // Adjust on machine
    jtAssign, // 8: Assign on machine (range or settings)
    jtAssignComplete, // 8: Assign on machine (range or settings) completed
    jtClearZEGrpData, // Clears the datapots for groups on ZE
    jtClearZESpdData, // 1a:Clears the datapots for spindles on ZE
    jtDataAquEv, // 8: Dataaquisation event from TimeHandler
    jtDelInt, // 1a:Delete oldest intervalldata on DB
{20}jtDelShiftByTime, // 1a:All shift related data older time x will be deleted
    jtDelZEGrpData, // Delete group data on ZE
    jtDelZESpdData, // 1a:Delete spindle data on ZE
    jtEntryLocked, // 9: Key on ZE became locked
    jtEntryUnlocked, // 9: Key on ZE became unlocked
    jtGenDWData, // 2: Generating data for DW-tables. Succeeding jtGenShiftData
    jtGenExpDWData, // 2: Gen.data for DW-tables (also changes in already ex.sets). Suc.jtGenExpShiftData
    jtGenShiftData, // 2: Start generating shift data
    jtGenExpShiftData, // 2: Generate shift data of an already existing shift
    jtGenSpdOfflimits, // 2: Start generating offlimits on the base of spindle data
{30}jtGetDataStopZEGrp, // regular group data aquisition at ProdGrp stop
    jtGetDataStopZESpd, // 1: regular spindle data aquisition at ProdGrp stop (before jtGetDataStop)
    jtGetExpDataStopZEGrp, // irregular group data aquisition at ProdGrp stop
    jtGetExpDataStopZESpd, // 1: irregular spindle data aquisition at ProdGrp stop (before jtGetDataStop)
    jtGetExpZEGrpData, // irregular group data aquisition at shift end
    jtGetExpZESpdData, // 1: irregular spindle data aquisition at shift end (before jtGetExpData)
    jtGetZEGrpData, // regular group data aquisition at intervall end
    jtGetZESpdData, // 1: regular spindle data aquisition at intervall end (before jtGetIntData)
    jtGetMaAssign, // 3: Get machine configuration
    jtSaveMaConfigToDB, // Get Machine configuration from Maschine, will be initiated by MaConfig application
{40}jtGetSettings, // ?? Get settings from prodgroup
    jtInitialReset, // 9: Coldstart on machine (ZE)
    jtJobResult, // ?? JobResult from NetHandler to MsgDispatcher
    jtSendEventToClients, // 2: Event to all client (e.g. One of the floor relevant data changed)
    jtMaNotFound, // ?? Could not find machine in node list
    jtMaOffline, // 9: Machine state changed to offline
    jtMaOnline, // 9: Machine state changed to online
    jtMulGrpDataEv, // ?? Start multiple group aquisition
    jtOEEventJobID, // Job to send new JobID to OEHandler
    jtOEEventData, // Each received event from CIMServer
{50}jtReset, // ??9: Warmstart on ZE (after PowerOff)
    jtSendMsgToAppl, // 2: Inform application about something
    jtSetNodeList, // 1a:Load NodeList to the NetHandler
    jtSetProdID, // save ProdID only after settings change on machine
    jtSetSettings, // 3: Load settings of a prodgroup to ZE
    jtStartGrpEv, // 8: Start aquisition of prodgroup data (from MM)
    jtStopGrpEv, // 8: Stop aquisition of prodgroup data (from MM)
    jtSynchronize, // 4: Embed job jtMaEqualize
    jtUplAutoMaData, // ?? Prodgroup setting and spindledata arrived (automatic after change on ZE)
    jtViewZEGrpData, // regular group data aquisition without deletion (before jtGetMulGrpData)
{60}jtViewZESpdData, // ?? regular spindle data aquisition without deletion (before jtGetOneGrpData)
    jtDiagnostic, // 8: diagnistic Job
    jtInitialized, // 4: To signalize that a handler is initialized
    jtGetNodeList, // 3: Load NodeList from NetHandler to JobHandler
    jtPrintJob, // ?? PrintJob
    jtJobFinished, // 7: Delete entries in JobList. Msg from JobManager
    jtUnDoGetZESpdData, // 2: Delete the stored Interval/ProdGrp Spindle Data from Database
    jtStartGrpEvFromMa, // 9: Start aquisition of prodgroup data (from Machine (AC338))
    jtStopGrpEvFromMa, // 9: Stop aquisition of prodgroup data (from Machine (AC338))
    jtProdGrpUpdate, // Update from prodgroup DB-tables t_prodgroup or t_prodgroup_state
{70}jtGetSettingsAllGroups, // 3: Get settings from all groupmemories
    jtMaEqualize, // 3: Equalize machine and Millmaster
    jtDBDump, // 2a: Starts a dump of a database
    jtGenQOfflimit, // Generation of Quality Offlimit
    jtBroadcastToClients, // 8: Event happend in MMClient or MMApplication (changed to other job(s) and send last to all MMClients
    jtGetZEReady         // 1a: Checks after MaOnline, if ZE is ready.
  );     // Antwort vom JobHandler an die Maschine

//Pendenzen: Aendern von 'jtDelZESpdData' zu 'jtAckGetZESpdData' (20.4.99 Nue/Khp)

  TMachIDJobTyp = set of TJobTyp;
//------------------------------------------------------------------------------
// Definition of all records for each Job
//------------------------------------------------------------------------------

  // The following records are used to define where the parameters
  // in the TJobHeader has to be initialized and to define how many
  // bytes has to be written

  //.........................................................................
  // <<<<<< COMMON SUBRECORDS FOR JOB DEFINITION >>>>>>
  //.........................................................................
  TCleanUpEv = record // Cleanprocedure for datapool
    case TThreadTyp of
      ttJobManager,
        ttJobList,
        ttMsgController: (TimePeriod: DWord); // in ms
      ttQueueManager: (NumOfJobs: Word;
        JobIDList: TJobIDList);
  end;
  //.........................................................................
  // This Job never appears in the JobList
  TDelJobID = record // JobID to delete in datapool
    case TThreadTyp of
      ttMsgController,
        ttQueueManager: ();
  end;
  //.........................................................................
  TJobFailed = record // The job failed (Error)
    case TThreadTyp of
      ttJobController,
        ttJobManager: (Error: TErrorRec);
  end;
  //.........................................................................
  TJobSuccessful = record // Delete entries in Job- and MsgList for the job
    case TThreadTyp of
      ttJobController: ();
  end;
  //.........................................................................
  TJobFinished = TJobSuccessful;
  //.........................................................................
  // This Job never appears in the JobList
  TMsgComplete = record // Job in MsgHandler done
    case TThreadTyp of
      ttJobController: ();
  end;
  //.........................................................................
  // This Job never appears in the JobList
  TMsgNotComplete = record // Job in MsgHandler not finished correctly
    case TThreadTyp of
      ttJobController,
        ttJobManager: (Error: TErrorRec);
  end;
  //.........................................................................
  // This Job never appears in the JobList
  TMsgReceived = record // Data from machine received
    case TThreadTyp of
      ttMsgController: (JobTyp: TJobTyp; Value: DWord; Error: TErrorRec);
  end;
  //.........................................................................
  TNewJobEv = record // New job into JobList from JobManager
    case TThreadTyp of // Job doesn't appear in joblist
      ttJobController: ();
  end;
  //.........................................................................
  TTimerEvent = record
    case TThreadTyp of
      ttJobExecute: (TimeStamp: TTimeStamp;
        FirstAsynchEvent: Boolean; // immediately after startup of MillMaster
        TimeStampOld: TTimeStamp;
        TimeChange: Boolean);
      ttMsgController,
        ttJobManager: ();
  end;

  //.........................................................................
  // <<<<<< INDIVIDUAL SUBRECORDS FOR JOB DEFINITION >>>>>>
  //.........................................................................
  TNone = record
  end;
  //.........................................................................
  TTest = record
    Buf: array[0..cMaxSpdDataArraySize - 1] of byte;
  end;
  //.........................................................................
  TTestConfirm = record
  end;
  //.........................................................................
  TTestStore = record
  end;
  //.........................................................................
  TAdjust = record // Adjust on machine
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TAssign = record // Assign on machine (range or settings)
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word; Assigns: TMaAssign);
  end;
  //.........................................................................
  TBroadcastToClients = record // Event to all client initialized by a an MMClient or an MMapplication
    case TThreadTyp of // For broadcasting any events to the clients
      ttJobManager: (Event: TBroadToClientMsg);
  end;
  TAssignComplete = TAssign; // Assign on machine (range or settings)
  //.........................................................................
  TClearZEGrpData = record // Clears the datapots for groups on ZE
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter: (MachineID: Word; SpindleFirst, SpindleLast: Word; );
  end;
  //.........................................................................
  TClearZESpdData = TClearZEGrpData; // Clears the datapots for spindles on ZE
  //.........................................................................
  TDataAquEv = record // Intervallevent from TimeHandler
    case TThreadTyp of
      ttJobManager: (IntID, OldIntID: Byte; FragID, OldFragID: Longint;
                     RegularInt: Boolean;
                     ShiftEv: Boolean;
                     ShiftcalID: Byte; //Added on 25.03.01 Nue
                     ShiftInDay: Byte; //Added on 25.03.01 Nue
                     MulMaData: Boolean);
  end;
  //.........................................................................
  TDBDump = record // Starts a dump of a database
    case TThreadTyp of
      ttJobManager,
        ttJobList,
        ttQueueManager: (DBName: TString30);
  end;
  //.........................................................................
  TDelInt = record // Delete oldest intervalldata on DB
    case TThreadTyp of
      ttJobList,
        ttQueueManager: (IntID: Byte);
  end;
  //.........................................................................
  TDelShiftByTime = record // All shift related data older time x will be deleted
    case TThreadTyp of
      ttJobManager,
        ttJobList,
        ttQueueManager: (DateTime: TDateTime);
  end;
  //.........................................................................
  TDelZESpdData = TClearZESpdData; // Delete spindle data on ZE
  //.........................................................................
  TDelZEGrpData = TClearZEGrpData; // Delete group data on ZE
  //.........................................................................
  TEntryLocked = record // Key on ZE became locked
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TEntryUnlocked = TEntryLocked; // Key on ZE became unlocked
  //.........................................................................
  TGenDWData = record // Generating data for DW-tables. Succeeding jtGenShiftData
    MachineID: Word;  //Notwendig f�r JobList um maschinenabh�ngige Jobs l�schen zu k�nnen.
    case TThreadTyp of
      ttJobList,
        ttQueueManager: (IntID: Byte; FragID, ProdID: Longint;
                         ShiftEv: Boolean;
                         ShiftcalID: Byte; //Added on 25.03.01 Nue
                         ShiftInDay: Byte); //Added on 25.03.01 Nue
  end;
  //.........................................................................
  TGenExpDWData = TGenDWData; // Gen.data for DW-tables (also changes in already ex.sets). Suc.jtGenExpShiftData
  //.........................................................................
  TGenShiftData = record // Start generating shift data
    case TThreadTyp of // after all data are aquired on maschines
      ttJobList, // Succeeding job to GetZESpdData
        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word; //Needed for SendMsg Info
                         IntID: Byte; //Actual intervalID shiftdata has to be generated for
                         FragID, //Actual fragshiftID shiftdata has to be generated for
                         ProdID: Longint);
  end;
  //.........................................................................
  TGenExpShiftData = record // Generate shift data of an already existing shift
    case TThreadTyp of // after all data are aquired on maschines
      ttJobList, // Succeeding job to GetZESpdData
        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word; //Needed for SendMsg Info
                         StartIntID, //Last intervalID shiftdata were saved successful on DB
                         EndIntID: Byte; //Actual intervalID shiftdata has to be generated for
                         StartFragID, //Last fragshiftID shiftdata were saved successful on DB
                         EndFragID, //Actual fragshiftID shiftdata has to be generated for
                         ProdID: Longint);
  end;
  //.........................................................................
  TGenSpdOfflimits = record // Start generating offlimits on the base of spindle data
    MachineID: Word;  //Notwendig f�r JobList um maschinenabh�ngige Jobs l�schen zu k�nnen.
    case TThreadTyp of // Succeeding job to jtGetZESpdData
      ttJobList,
        ttQueueManager: (IntID: Byte; ProdID: Longint);
  end;
  //............................................................................
  TGenQOfflimit = record
    case TThreadTyp of
      ttJobList,
        ttQueueManager: (FragshiftID: integer);
  end;
  //.........................................................................
  TGetZEReady = record // Checks after MaOnline, if ZE is ready.
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter: (MachineID: Word);
  end;
  //.........................................................................
  TGetZESpdData = record // regular spindle data aquisition at intervall end
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word;
                         IntID: Byte; //Actual intervalID spindledata has to be saved on DB
//                         MachineTyp: TWinderType; //alt TAWEMachType;
                         ProdID,
                         FragID: Longint; //Actual fragshiftID spindledata has to be saved on DB
                         BadData: Boolean); // if data are inkonsistent  ????
      ttMsgDispatcher,
        ttDataPoolWriter: (SpindID: Word; SpdData: TMMDataRec);
//                           SpdDataArr: TSpdDataArr);
  end;
  //.........................................................................
  TGetDataStopZESpd = TGetZESpdData; // regular spindle data aquisition at ProdGrp stop
  //.........................................................................
  //wss: sollte eigentlich nicht mehr verwendet werden. Beibt aber aus Kompilationsgr�nden noch bestehen
  TGetExpZESpdData = record // irregular spindle data aquisition at intervall end
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word;
                         StartIntID, //Last intervalID spindledata were saved successful on DB and deleted on ZE
                         EndIntID: Byte; //Actual intervalID spindledata has to be saved on DB
                         MachineTyp: TWinderType; //alt TAWEMachType;;
                         ProdID,
                         StartFragID, //Last fragshiftID spindledata were saved successful on DB and deleted on ZE
                         EndFragID: Longint; //Actual fragshiftID spindledata has to be saved on DB
                         BadData: Boolean); // if data are inkonsistent  ????
      ttMsgDispatcher,
        ttDataPoolWriter: (SpindID: Word; SpdData: TMMDataRec);
//        ttDataPoolWriter: (SpindID: Word; SpdDataArr: TSpdDataArr);
  end;
  //.........................................................................
  TGetExpDataStopZESpd = TGetExpZESpdData; // irregular group data aquisition at ProdGrp stop
  //.........................................................................
  TGetNodeList = record // Load NodeList from NetHandler to JobHandler
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttJobController,
        ttQueueManager: ();
      ttMsgDispatcher,
        ttJobManager: (NodeItems: TNodeItems);
  end;
  //.........................................................................
  TGetZEGrpData = record // regular group data aquisition at intervall end
    case TThreadTyp of
      ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word; IntID: Byte;
                         MachineTyp: TWinderType; //alt TAWEMachType;
                         ProdID, FragID: Longint;
                         BadData: Boolean); // if data are inconsistent
      ttMsgDispatcher,
        ttDataPoolWriter: (GrpDataArr: TGrpDataArr);
  end;
  //.........................................................................
  TGetDataStopZEGrp = TGetZEGrpData; // regular group data aquisition at ProdGrp stop
  //.........................................................................
  TGetExpDataStopZEGrp = TGetZEGrpData; // irregular group data aquisition at ProdGrp stop
  //.........................................................................
  TGetExpZEGrpData = TGetZEGrpData; // irregular group data aquisition at shift end
  //.........................................................................
  TGetMaAssign = record // Get machine configuration
    case TThreadTyp of
      ttJobList,
        ttJobController,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttMsgDispatcher,
        ttDataPoolWriter,
        ttJobManager: (MachineID: Word; Assigns: TMaAssign);
  end;
  //.........................................................................
{ DONE -owss :
XML: (siehe unten TXMLSettingsRec) Hier kommt statt DataArray dann der XML-Bereich z.B. als Data: Array of Char hinzu
welcher beim Ermitteln der Jobgr�sse mit StrLen(Data) plus Header ermittelt werden muss }
//  TSettingsRec = record
//    SpindleFirst, SpindleLast, MachGrp, LengthData: Word;
//    GroupState: TGroupState; //Used for AC338 New 25.04.00 Nue
//    Color: Integer; //Color for representation on floor (Nue:25.4.2000)
//    YM_Set_Name: TText50; //YM_Set_name  (Nue:12.9.01)
//    YM_Set_Changed: Boolean; //True, if Set loaded from GUI was changed on AC338 (Nue:12.9.01)
//    ProdGrpInfo: TProdGrpInfo; //includes machine_id, prod_id,....
//    DataArray: TYMSettingsByteArr; //Only used for upload
//    XMLData: TArrayOfChar; // tempor�r drin um zu kompilieren
//  end;

  PXMLSettingsRec = ^TXMLSettingsRec;
  TXMLSettingsRec = record
    // Parameter welche von ausserhalb in den Sack gef�llt werden m�ssen
    AssignMode: Byte;         // cAssigGroup, cAssigProdGrpIDOnly
    ProdGrpID: Integer;       // MillMaster's group ID, 0=not defined
    SpindleFirst: Word;
    SpindleLast: Word;
    Group: Byte;              // 0-Basiiert: Local Group (0..5: AC338 Informator, 0..11 ZE)
    PilotSpindles: Byte;
    SpeedRamp: Byte;          // only machines with Speed Simulation
    Speed: Word;              // winding speed, (20..1600), [20..1600 m/Min]
    YarnCnt: Single;          // Yarn count [cMinYarnCount/10..cMinYarnCount/10]
    YarnCntUnit: TYarnUnit;  // Yarn count unit only valid on PutProdGrpYMPara
    NrOfThreads: Byte;        // threadCnt
    // to define from Nue
    LengthWindow: Word;
    LengthMode: Byte;
    // Weitere Felder welche von dem ProdGrpInfo Feld stammen
    OrderPositionID: Integer;
    OrderID: Integer;
    // Parameter welche vom Sack interpretiert, und ausserhalb eingef�llt werden m�ssen
    HeadClass: TSensingHeadClass;  //Ben�tigt f�r Kompatibilit�tscheck unter Reinigersettings
    IPICalcOnMachine: Boolean;  //FALSE = die Imperfektiondaten werden auf dem MillMaster aus den Klassierdaten berechnet
    // Zus�tzliche Informationen, welche f�r den AC338 verwendet werden.
    Color: Integer;           // Color for representation on floor (Nue:25.4.2000)
    GroupState: TGroupState;  // Used for AC338 New 25.04.00 Nue
    ProdName: TText100;
    Slip: Integer;            // Faktor 1000
    StyleID: DWord;
    YMSetID: Word;
    YMSetName: TText50;       // YM_Set_name  (Nue:12.9.01)
    YMSetChanged: Boolean;    // True, if Set loaded from GUI was changed on AC338 (Nue:12.9.01)
    ComputerName: TString30;  // Workstation name; has to to set to '' if from ZE
    MMUserName: TString30;    // MMUser name; has to to set to '' if from ZE
    Port: TString30;          // Pipe name; has to to set to '' if from ZE
//    Hash1: Integer; 
//    Hash2: Integer;
    case Boolean of
      False: (DataArrayOld: TYMSettingsByteArr);  // tempor�r noch drin um zu kompilieren
      True:  (XMLData: TArrayOfChar);
  end;

  //.........................................................................
  TGetSettings = record // Get settings from prodgroup
    case TThreadTyp of
      ttJobController,
        ttJobList,
        ttMsgController,
        ttMsgList,
        ttTXNWriter,
        ttWSCWriter,
        ttQueueManager,
        ttMsgDispatcher,
        ttJobManager: (MachineID: Word;
                       AllXMLValues: Boolean;  // wird f�r Assignment ben�tigt
                       SettingsRec: TXMLSettingsRec);
  end;

  PSettingsArr = ^TSettingsArr;
//  TSettingsArr = array[0..cZESpdGroupLimit-1] of TXMLSettingsRec;
  TSettingsArr = array[0..cMaxGroupMemoryLimit-1] of TXMLSettingsRec;
//  TSettingsArr = array[0..(cZESpdGroupLimit - 1)] of TSettingsRec;

  PXMLSettingsCollection = ^TXMLSettingsCollection;
  TXMLSettingsCollection = record
//    SettingsArr:   array[0..(cZESpdGroupLimit-1)] of TXMLSettingsRec;
    SettingsArr: TSettingsArr;
    XMLData: TArrayOfChar;  // zusammengefasste XMLSettings in einer Settings0=Wert,Settings1=Wert Struktur
  end;

//  TSpindleRangeArr = array[0..cZESpdGroupLimit-1] of record
  TSpindleRangeArr = array[0..cMaxGroupMemoryLimit-1] of record
    SpindleFirst, SpindleLast: Word;
  end;

  TGetSettingsAllGroups = record // Get settings from prodgroup
    MachineID: Word;        // used in all cases
    AllXMLValues: Boolean;  // wird f�r Assignment ben�tigt
    case TThreadTyp of
      ttJobController,
        ttJobList,
        ttMsgController,
        ttMsgList: (SpindleRanges: TSpindleRangeArr);

      ttJobManager,
        ttQueueManager: (ComputerName: TString30; // Workstation name
                         Port: TString30); // Pipe name
      ttTXNWriter,
        ttWSCWriter,
        ttMsgDispatcher,
        ttDataPoolWriter: (SettingsRec: TXMLSettingsRec);
//        ttDataPoolWriter: (SettingsRec: TSettingsRec);
  end;
  //.........................................................................
  TSaveMaConfigToDB =  record       // NetHandler saves machine configuration into database
    MachineID: Word; // used in all cases
    LastSpindle: Integer; // Enth�lt die letzte Spindel nach dem Zusammenfassen des Bauzustandes f�r die t_machine Tabelle
    UseXMLData: Boolean;  // bestimmt ob bei der Ermittlung der Jobgr�sse die XMLDaten verwendet wird
    case TThreadTyp of
      ttJobController,
      ttJobList,
      ttMsgController,
      ttMsgList,
      ttTXNWriter,
      ttWSCWriter:();

      ttJobManager,
      ttQueueManager:   (ComputerName: TString30;     // Workstation name
                         Port: TString30);             // Pipe name
      ttMsgDispatcher,
      ttDataPoolWriter: (XMLData: TArrayOfChar);

  end;
  //.........................................................................
  TInitialized = record // To signalize that a handler is initialized
    case TThreadTyp of
      ttJobManager: ();
  end;
  //.........................................................................
  TInitialReset = record // Coldstart on machine (ZE)
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TJobResult = record // JobResult from NetHandler to MsgDispatcher
    case TThreadTyp of
      ttTXNWriter,
      ttLXWriter,
        ttMsgDispatcher: (JobTyp: TJobTyp; Error: TErrorRec);
  end;
  //.........................................................................
  TProdGrpUpdate = record // To signalize that a handler is initialized
    case TThreadTyp of
      ttJobManager: (ProdGrpID: Longint;
                    YarnCnt: Double; YarnCntValid: Boolean;
                    ThreadCnt: integer; ThreadCntValid: boolean;
                    Color: Integer; ColorValid: Boolean;
                    ProdName: TString30; ProdNameValid: Boolean;
                    Slip: Integer; SlipValid: Boolean);
  end;
  //.........................................................................
  TSendEventToClients = record // Event to all client (e.g. One of the floor relevant data changed)
    case TThreadTyp of // For broadcasting any events to the clients
      ttJobList,
      ttQueueManager: (MachineID: Word;
                       Event: TApplMsg);
  end;
  //.........................................................................
  TMaEqualize = TGetMaAssign; //GetAssignment from Machine
  //.........................................................................
  TMaNotFound = record // Could not find machine in node list
    case TThreadTyp of
      ttMsgDispatcher,
        ttMsgController,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TMaOffline = record // Machine state changed to offline
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TMaOnline = record // Machine state changed to online
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TMulGrpDataEv = record // Start multiple machine aquisition
    case TThreadTyp of
      ttJobManager: ();
  end;
  //.........................................................................
//  TOEEventJobID = record
//    case TThreadTyp of
//      ttJobManager,
//        ttMsgController,
//        ttMsgDispatcher,
//        ttQueueManager: ();
//  end;
//  //.........................................................................
//  TOEEventData = record
//    case TThreadTyp of
//      ttMsgDispatcher,
//        ttDataPoolWriter: (Data: array[0..100] of Char);
//  end;

  //.........................................................................
  TPrintJob = record
    PrintJobID: Integer; // Id aus Tab. t_PrintJobs
    Reportname: array[0..100] of char;
    Printername: array[0..100] of char;
    PrintToFile: array[0..100] of char; // Path and Filename for printing in file
      //data        : record Parameter
  end;
  //.........................................................................
  TReset = record // Warmstart on ZE (after PowerOff)
    case TThreadTyp of
      ttMsgDispatcher,
        ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  // Type for messages to the application (not to the MMClient)
  // The message (String) can not be sent direct because the translation
  // to the languages
  TResponseMsgTyp = (rmNone,
    rmBadSpdRange,        // Wrong spindle range
    rmMachineOffline,     // Machine offline
    rmProdGrpStoppedOk,   // ProdGroup is stopped successful
    rmProdGrpStoppedNOk,  // ProdGroup couldn't stop successful. (e.g.DB-Error)
    rmProdGrpStartedOk,   // ProdGroup is started successful
    rmProdGrpStartedNOk,  // ProdGroup couldn't start successful. (e.g.DB-Error)
    rmProdGrpStateBad,    // ProdGroup wrong for the required operation -> try later
    rmNodeListOnDBOk,     // Actual nodelist from adapters loaded successfully to DB
    rmNodeListOnDBNotOk,  // Error while loading actual nodelist from adapters.
    rmGetSettingsOK,      // Settings of Machine available
    rmGetSettingsNotOk,   // Error while loading settings from machine
    rmBadMachineGrp,      // Error tried to start or stop ProdGrp on wrong MachineGrp
    rmSaveMaConfigOk,     // MachineConfig saved to DB
    rmSaveMaConfigNotOk); // Error while saving MachineConfig to DB

// DONE: XML: kontrollieren
  // wss: TResponseRec wird im StorageHandler direkt selber verwendet (TGetSettingsAllGroupsEditor)
  // um die einzeln empfangenen Settings von der Maschine zum Assignment zu schicken.
  // Ansonsten noch im Job jtSendMsgToAppl, wo jedoch der Sizeof f�r die Jobgr�sse
  // verwendet wird.
  PResponseRec = ^TResponseRec;
  TResponseRec = record
    case MsgTyp: TResponseMsgTyp of
      rmNone,
      rmNodeListOnDBOk,
      rmNodeListOnDBNotOk: ();

      rmGetSettingsOK,
      rmGetSettingsNotOk,
      rmSaveMaConfigOk,
      rmSaveMaConfigNotOk,
      rmBadSpdRange,
      rmMachineOffline,
      rmProdGrpStoppedOk,
      rmProdGrpStoppedNOk,
      rmProdGrpStartedOk,
      rmProdGrpStartedNOk,
      rmProdGrpStateBad: (MachineID: Word;
                          case TResponseMsgTyp of
                            rmGetSettingsOK:     (XMLSettingsCollection: TXMLSettingsCollection);
                            rmGetSettingsNotOk:  ();
                            rmSaveMaConfigOk:    (XMLMaConfig: TArrayOfChar);
                            rmSaveMaConfigNotOk: ();
                            rmBadSpdRange,
                            rmMachineOffline,
                            rmProdGrpStoppedOk,
                            rmProdGrpStoppedNOk,
                            rmProdGrpStartedOk,
                            rmProdGrpStartedNOk,
                            rmProdGrpStateBad:   (SpindleFirst, SpindleLast: Word;
                                                  ProdGrpID: Longint);
                          );
//    case MsgTyp: TResponseMsgTyp of
//      rmNone,
//        rmNodeListOnDBOk,
//        rmNodeListOnDBNotOk: ();
//{ DONE -owss :
//XML: Diese Settings werden auch als XML zum Assignment verschickt
//-> demzufolge beim ermitteln der Jobgr�sse Anhand von MsgTyp dies auch noch abfangen }
//      rmGetSettingsOK: (MachineID1: Word; XMLSettingsCollection: TXMLSettingsCollection);
////      rmGetSettingsOK: (MaID: Word;
////                        Settings: TSettingsArr;
////                        XMLData: TArrayOfChar);  // tempor�r mal drin um zu kompilieren
//      rmGetSettingsNotOk: (MachineID2: Word);
//      rmBadSpdRange,
//        rmMachineOffline,
//        rmProdGrpStoppedOk,
//        rmProdGrpStoppedNOk,
//        rmProdGrpStartedOk,
//        rmProdGrpStartedNOk,
//        rmProdGrpStateBad: (MachineID3, SpindleFirst, SpindleLast: Word;
//                            ProdGrpID: Longint);
  end;
  //.........................................................................
  TSendMsgToAppl = record // Inform application about something
    case TThreadTyp of
      ttJobList,
        ttQueueManager: (ComputerName: TString30;
                         Port: TString30;
                         ResponseMsg: TResponseRec);
  end;
  //.........................................................................
  TSetNodeList = record // Load NodeList to the NetHandler
    case TThreadTyp of
      ttJobController,
        ttJobManager: ();
      ttJobList,
        ttMsgController,
        ttMsgDispatcher,
        ttTXNWriter,
        ttWSCWriter: (NodeItems: TNodeItems);
  end;
  //.........................................................................
  TSetProdID = record // SetProdID on machine (Send ProdID from MM to machine)
    MachineID: Word;
    ProdGrpID: Longint;
    case TThreadTyp of
//      ttMsgDispatcher,
//      ttJobController: ();
//      ttJobList,
//      ttMsgController,
      ttTXNWriter: (SpindleFirst, SpindleLast: Word);
      ttWSCWriter,
      ttLXWriter:  (Group: Byte); // 0-Basiert
    end;
  //.........................................................................
  TSetSettings = record // Set Machine configuration from MM to ZE
    case TThreadTyp of
      ttJobManager,
        ttJobList,
        ttJobController,
        ttMsgController,
        ttMsgDispatcher,
        ttMsgList,
        ttTXNWriter,
        ttLXWriter,
        ttWSCWriter: (MachineID: Word; SettingsRec: TXMLSettingsRec);
//        ttWSCWriter: (MachineID, SpindleFirst, SpindleLast, MachineGrp, LengthData: Word;
//                      Color: Integer; //Color for representation on floor (Nue:25.4.2000)
//                      YM_Set_Name: TText50; //YM_Set_name  (Nue:12.9.01)
//                      YM_Set_Changed: Boolean; //True, if Set loaded from GUI was changed on AC338 (Nue:12.9.01)
//                      ProdGrpInfo: TProdGrpInfo; //includes machine_id, prod_id,....
//                      DataArray: TYMSettingsByteArr;
//                      XMLData: TArrayOfChar);  // tempor�r mal drin um zu kompilieren
  end;

  //.........................................................................
  TStartGrpEv = record // Start aquisition of prodgroup data (from MM)
    case TThreadTyp of // On GUI: GetFromScreen(TYMSettingsByteArr)
      ttJobManager, //         GetFromProdGrpScreen(TProdGrpParam)
        ttJobList, // In JobManager: New ProdGrp, Add missing data to TYMSettingsByteArr
        ttMsgController,
        ttWSCWriter,
        ttLXWriter,
        ttTXNWriter: (MachineID: Word; SettingsRec: TXMLSettingsRec);
(**
        ttCIWriter: (MachineID, SpindleFirst, SpindleLast, MachineGrp: Word;
//wss                     LengthData: Word; //Contains the effectiv, filled datasize of DataArray (Nue,Kr 26.10.99)
                                       // This field is filled by Kr on ZE or by methodes called in GUI
                     ProdGrpID: Longint;
                     Color: Integer; //Color for representation on floor (Nue:10.1.2000)
                     YM_Set_Name: TText50; //YM_Set_name  (Nue:12.9.01)
                     YM_Set_Changed: Boolean; //True, if Set loaded from GUI was changed on AC338 (Nue:12.9.01)
                     ProdGrpParam: TProdGrpData; // Used to get ProdGrpData from GUI
                     ComputerName: TString30; // Workstation name; has to to set to '' if from ZE
                     MMUserName: TString30; // MMUser name; has to to set to '' if from ZE
                     Port: TString30; // Pipe name; has to to set to '' if from ZE
                     XMLData: TArrayOfChar);
(**)
  end;
  //.........................................................................
  TStartGrpEvFromMa = TStartGrpEv; // Start aquisition of prodgroup data (from Machine (AC338))
  //.........................................................................
  TStopGrpEv        = TStartGrpEv; // Stop aquisition of prodgroup data (from MM)
  //.........................................................................
  TStopGrpEvFromMa  = TStartGrpEv; // Stop aquisition of prodgroup data (from Machine (AC338))
  //.........................................................................
  TSynchronize = record // Equalize machine and Millmaster
    case TThreadTyp of
      ttJobManager: (MachineID: Word);
  end;
  //.........................................................................
  TUplAutoMaData = record // Prodgroup setting and spindledata arrived (automatic after change on ZE)

  end;
  //.........................................................................
//wss  TViewZEGrpData = record // Get group base data without delete (before TGetMulGrpData)
//    case TThreadTyp of
//      ttJobList, //1 Succeeding MulGrpDataEv
//        ttMsgController, //2
//        ttMsgList, //3
//        ttTXNWriter, //4
//        ttWSCWriter, //4
//        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word); //7
//      ttMsgDispatcher, //5
//        ttDataPoolWriter: (DataArray: array[0..cMaxGrpDataArraySize - 1] of byte; ); //6
//  end;
  //.........................................................................
//  TViewZESpdData = record // Get all group data without delete
//    case TThreadTyp of
//      ttJobList,
//        ttMsgController,
//        ttMsgList,
//        ttTXNWriter,
//        ttWSCWriter,
//        ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word;
//        MachineTyp: TAWEMachType;
//        ProdID: Longint;
//        ComputerName: TString30;
//        Port: TString30);
//      ttMsgDispatcher,
//        ttDataPoolWriter: (SpindID: Word;
//        SpdDataArr: TSpdDataArr);
//  end;
  //.........................................................................
  TDiagnosticTyp = (dtShowList);
  TDiagnostic = record
    case TThreadTyp of
      ttJobController,
        ttQueueManager,
        ttTXNWriter,
        ttMsgController: (DiagnosticTyp: TDiagnosticTyp);
  end;
  //.........................................................................
  TUnDoGetZESpdData = record
    case TThreadTyp of
      ttQueueManager: (MachineID, SpindleFirst, SpindleLast: Word; //Needed for SendMsg Info
        ProdID: longint;
        IntID: Byte; //Last intervalID spindledata had been saved successful on DB and deleted on ZE
        FragID: longint); //Last fragshiftID spindledata had been saved successful on DB and deleted on ZE
  end;
  //.........................................................................

//------------------------------------------------------------------------------
// Main Job Record definition to communicate between the MillMaster Processes
//------------------------------------------------------------------------------
const
  cJobHeaderSize = 12; // it is 12, not 10 -> member JobTyp has an alignment of 4

type
  PJobRec = ^TJobRec;
  TJobRec = packed record
    JobID: DWord;              // 4 byte: ID of job
    NetTyp: TNetTyp;           // 2 byte: Type of the net handler (None, Texnet, WSC, OE .......)
//DONE -owss: XML: JobLen auf DWord �ndern. cJobHeaderSize wird dann 12 Byte (�berpr�ft mit Testappl)
    JobLen: DWord;              // 2 byte: Count of data bytes will be filled automaticlly in thread
//    JobLen: Word;              // 2 byte: Count of data bytes will be filled automaticlly in thread
    case JobTyp: TJobTyp of    // 2 byte
    // common job definition of both systems
      jtCleanUpEv: (CleanUpEv: TCleanUpEv); // Cleanprocedure for datapool
      jtDelJobID: (DeleteJobID: TDelJobID); // JobID to delete in datapool
      jtJobFailed: (JobFailed: TJobFailed); // The job failed (Error)
      jtJobSuccessful: (JobSuccessful: TJobSuccessful); // Delete entries in Job- and MsgList for the job
      jtMsgComplete: (MsgComplete: TMsgComplete); // Job in MsgHandler done
      jtMsgNotComplete: (MsgNotComplete: TMsgNotComplete); // Job in MsgHandler not finished correctly
      jtMsgReceived: (MsgReceived: TMsgReceived); // Data from machine received
      jtNewJobEv: (NewJob: TNewJobEv); // New job into JobList from JobManager
      jtTimerEvent: (TimerEvnet: TTimerEvent); // A timer event has occured
    // individual job definition for MillMaster Loepfe
      jtNone: (buf: array[1..1000] of byte);
      jtTest: (Test: TTest);
      jtTestConfirm: ();
      jtTestStore: ();
      jtAdjust: (Adjust: TAdjust); // Adjust on machine
      jtAssign: (Assign: TAssign); // Assign on machine (range or settings)
      jtAssignComplete: (AssignComplete: TAssignComplete); // Assign on machine (range or settings)
      jtClearZEGrpData: (ClearZEGrpData: TClearZEGrpData); // Clears the datapots for groups on ZE
      jtClearZESpdData: (ClearZESpdData: TClearZESpdData); // Clears the datapots for spindles on ZE
      jtDataAquEv: (DataAquEv: TDataAquEv); // Dataaquisation event from TimeHandler
      jtDelInt: (DelInt: TDelInt); // Delete oldest intervalldata on DB
      jtDelShiftByTime: (DelShiftByTime: TDelshiftByTime); // All shift related data older time x will be deleted
      jtDelZEGrpData: (DelZEGrpData: TDelZEGrpData); // Delete group data on ZE
      jtDelZESpdData: (DelZESpdData: TDelZESpdData); // Delete spindle data on ZE
      jtEntryLocked: (EntryLocked: TentryLocked); // Key on ZE became locked
      jtEntryUnlocked: (EntryUnlocked: TEntryUnlocked); // Key on ZE became unlocked
      jtGenDWData: (GenDWData: TGenDWData); // Generating data for DW-tables. Succeeding jtGenShiftData
      jtGenExpDWData: (GenExpDWData: TGenExpDWData); // Gen.data for DW-tables (also changes in already ex.sets). Suc.jtGenExpShiftData
      jtGenShiftData: (GenShiftData: TGenShiftData); // Start generating shift data
      jtGenExpShiftData: (GenExpShiftData: TGenExpShiftData); // Generating shift data of an already existing shift
      jtGenSpdOfflimits: (GenSpdOfflimits: TGenspdOfflimits); // Start generating offlimits on the base of spindle data
      jtGetDataStopZEGrp: (GetDataStopZEGrp: TGetDataStopZEGrp); // regular group data aquisition at ProdGrp stop
      jtGetDataStopZESpd: (GetDataStopZESpd: TGetDataStopZESpd); // regular spindle data aquisition at ProdGrp stop
      jtGetExpDataStopZEGrp: (GetExpDataStopZEGrp: TGetExpDataStopZEGrp); // irregular group data aquisition at ProdGrp stop
      jtGetExpDataStopZESpd: (GetExpDataStopZESpd: TGetExpDataStopZESpd); // irregular spindle data aquisition at ProdGrp stop
      jtGetExpZEGrpData: (GetExpZEGrpData: TGetExpZEGrpData); // irregular group data aquisition at shift end
      jtGetExpZESpdData: (GetExpZESpdData: TGetExpZESpdData); // irregular spindle data aquisition at shift end
      jtGetZEGrpData: (GetZEGrpData: TGetZEGrpData); // regular group data aquisition at intervall end
      jtGetZESpdData: (GetZESpdData: TGetZESpdData); // regular spindle data aquisition at intervall end (before jtGetIntData)
      jtGetMaAssign: (GetMaAssign: TGetMaAssign); // Get machine configuration
      jtSaveMaConfigToDB: (SaveMaConfig: TSaveMaConfigToDB); // Get Machine configuration from Maschine, will be initiated by MaConfig application
      jtGetSettings: (GetSettings: TGetSettings); // Get settings from prodgroup (ZE)
      jtInitialReset: (InitialReset: TInitialReset); // Coldstart on machine (ZE)
      jtJobResult: (JobResult: TJobResult); // JobResult from NetHandler to MsgDispatcher
      jtSendEventToClients: (SendEventToClients: TSendEventToClients); // Event to all client (e.g. One of the floor relevant data changed)
      jtMaNotFound: (MaNotFound: TMaNotFound); // Could not find machine in node list
      jtMaOffline: (MaOffline: TMaOffline); // Machine state changed to offline
      jtMaOnline: (MaOnline: TMaOnline); // Machine state changed to online
      jtMulGrpDataEv: (MulGrpDataEv: TMulGrpDataEv); // Start multiple group aquisition
//      jtOEEventJobID: (OEEventJobID: TOEEventJobID);
//      jtOEEventData: (OEEventData: TOEEventData);
      jtReset: (Reset: TReset); // Warmstart on ZE (after PowerOff)
      jtSendMsgToAppl: (SendMsgToAppl: TSendMsgToAppl); // Inform application about something
      jtSetNodeList: (SetNodeList: TSetNodeList); // Load NodeList to the NetHandler
      jtSetSettings: (SetSettings: TSetSettings); // Load settings of a prodgroup to ZE
      jtStartGrpEv: (StartGrpEv: TStartGrpEv); // Start aquisition of prodgroup data (from MM)
      jtStopGrpEv: (StopGrpEv: TStopGrpEv); // Stop aquisition of prodgroup data (from MM)
      jtSynchronize: (Synchronize: TSynchronize); // Equalize machine and Millmaster
      jtUplAutoMaData: (UplAutoMaData: TUplAutoMaData); // Prodgroup setting and spindledata arrived (automatic after change on ZE)
      jtDiagnostic: (Diagnostic: TDiagnostic); // for diagnostic jobs
      jtInitialized: (Initialized: TInitialized); // To signalize that a handler is initialized
      jtGetNodeList: (GetNodeList: TGetNodeList); // Load NodeList from NetHandler to JobHandler
      jtPrintJob: (PrintJob: TPrintJob); // Execution of printjobs
      jtJobFinished: (JobFinished: TJobFinished); // Delete entries in JobList. Msg from JobManager
      jtUnDoGetZESpdData: (UnDoGetZESpdData: TUnDoGetZESpdData); // Undo of spindle data store
      jtSetProdID: (SetProdIDXX: TSetProdID); // SetProdID on machine (Send ProdID from MM to machine)
      jtStartGrpEvFromMa: (StartGrpEvFromMa: TStartGrpEvFromMa); // Start aquisition of prodgroup data (from Machine (AC338))
      jtStopGrpEvFromMa: (StopGrpEvFromMa: TStopGrpEvFromMa); // Stop aquisition of prodgroup data (from Machine (AC338))
      jtProdGrpUpdate: (ProdGrpUpdate: TProdGrpUpdate); // Update from prodgroup DB-tables t_prodgroup or t_prodgroup_state
      jtGetSettingsAllGroups: (GetSettingsAllGroups: TGetSettingsAllGroups); // Get settings from all groupmemories
      jtMaEqualize: (MaEqualize: TMaEqualize); // Equalize machine and Millmaster
      jtDBDump: (DBDump: TDBDump); // Starts a dump of a database
      jtGenQOfflimit: (GenQOfflimit: TGenQOfflimit); // Generation of Quality Offlimits
      jtBroadcastToClients: (BroadcastToClients: TBroadcastToClients); // Event happend in MMClient or MMApplication (changed to other job(s) and send last to all MMClients
      jtGetZEReady: (GetZEReady: TGetZEReady); // 1a: Checks after MaOnline, if ZE is ready.
  end;
//******************************************************************************

//------------------------------------------------------------------------------
// Type information's for MMGuard, BaseMain and MMClient
//------------------------------------------------------------------------------
type
  // Process state used in TBaseMain
  TMMProcessState = (psNotAvailable, // the process is in the NT process table not available
    psDead, // the process is in the NT process table available but terminated
    psCreated, // the process is created
    psAvailable, // the process is created ans has also created the IPCs for MMGuard
    psConnected, // the process is connected to the other processes
    psInit, // the process is in the init state or has finished the initialization
    psRun); // the process is running

  // old TMMProcessState = (psWaiting, psConnectThreads, psStartThreads, psRunning, psStopping, psError);

  // Commands between SubSystems, MMClient and MMGuard
  TMMGuardCommand = (gcStartMM, // from MMClient to MMGuard
    gcRestartMM, // MMGuard internal use
    gcStopMM, // from MMClient to MMGuard
    gcRunning, // SubSystem internal use
    gcAvailable, // to MMGuard at startup of process
    gcDoConnect, // from MMGuard to processes
    gcConnected, // from processes to MMGuard
    gcDoInit, // from MMGuard to processes
    gcInitialized, // from process to MMGuard and from threads to BaseMain
    gcDoRun, // from MMGuard to processes
    gcRun, // from processes to MMGuard
    gcKillSelf, // from MMGuard to handler
    gcKillInProgress, // to MMGuard
    gcDebug, // from MMGuard to switch ON/OFF debug mode
    gcStopService, // from service handler to MMGuard
    gcProcessStoped, // from MMGuard ( WatchDog ) to MMGuard
    gcAllProcessStoped, // fron Terminatoor to MMGuard if all processes are stopped
    gcAlert, // from MMGuard ( Timer ) to MMGuard for Alert ( Beep )
    gcTimeout, // from MMGuard ( Timer ) to MMGuard for timeout event
    gcResetTimeout, // from MMGuard ( Timer ) to MMGuard for reset of error
    gcAlertRestart, // from MMGuard ( Timer ) to MMGuard for restart if state is alert
    gcGetState, // from any application to MMGuard for asking the actual state of MillMaster
    gcResetHandler, // from MMGuard to Handler to reset instead of new startup
    gcRebootServer, // use this command carefuly
    gcSQLRebooted); // internel use of MMGuard

  // Data record to communcate between SubSystems/MMClient and MMGuard
  TMMGuardRec = record
    MsgTyp: TMMGuardCommand;
    Transmitter: TSubSystemTyp;
    Error: TErrorRec;
    case TMMGuardCommand of
      gcAvailable: (ThreadTyp: TThreadTyp; // used in BaseMain
        ProcessID: DWord); // only used for auto louncher
      gcDebug: (DebugMode: Byte); // 0 = OFF; <>0 = ON
      gcTimeout: (Count: integer); // to count the number of timeouts
      gcStartMM,
        gcStopMM: (ComputerAndUser: ShortString); // to know who started or stopped MM
  end;

  // describes the state of the MillMaster in MMGuard and MMClient
  TMMState = (mmsSleep, mmsStart, mmsRun, mmsStop, mmsAlert, mmsStopService);

  // State information will be sent within this record to MMClient
  TMMStateHeader = record
    MMState: TMMState; // Actual state of the MillMaster
    StartTime: TDateTime; // Last start of the MillMaster
    UserStart: Byte; // 0 = auto start else user start
    pad1: Byte;
  end;

  //............................................................................
  // Command between Server and MMClient
  TMMClientCommand = (ccNoCommand,
    ccMMStarted, // MillMaster started
    ccMMStarting, // MillMaster is starting
    ccMMStopped, // MM is stopped
    ccMMStopping, // MM is stopping
    ccMMAlert, // MM is in alert status
    ccMMServiceStopped, // MMGuard Service is stopped
    ccMMApplMsg, // Message to MM Applikations
    ccJobSent // Internal used from MMClient
           // khp          ccMMSystemInfo          // Next Interval or Shift and data aquisition busy
    );
  TApplMsgRec = record
    ApplMsgEvent: TApplMsg;
    MachineID: Integer;
    case TApplMsg of
      amNoMsg,
        amMachineStateChange,
        amMulMaDataReady,
        amOneMaDataReady,
        amOfflimitsReady,
        amStartAquisation,
        amEndAquisation,
        amQOfflimit: (); // alle Leer nur Event
      amSystemInfo: (NextInterval, NextShift: TDateTime);
  end;
  
  // Data record to communicate between SubSystems/MMGuard and MMClient
  TMMClientRec = record
    MsgTyp: TMMClientCommand;
    ServerName: TString30;
//Wss, 21.3.2001, remarked because broadcast msg are limited about to 400 bytes
//    Error      : TErrorRec;
    case TMMClientCommand of
      ccMMStarted,
        ccMMStopped,
        ccMMStarting,
        ccMMStopping,
        ccMMServiceStopped,
        ccMMAlert: (DateTime: TDateTime; UserStart: Boolean);
      ccMMApplMsg: (ApplMsg: TApplMsgRec);
    //khp  ccMMSystemInfo :    (NextInterval, NextShift: TDateTime);
  end;

//------------------------------------------------------------------------------
// Some constant string array
//------------------------------------------------------------------------------
{//  they shouldn't be in use anymore  -> see MMClient definitions
const
  cMMStateBroad       = 'Glb_MMStateBroad';  // Broadcast Mailboxname for MM state
  cMMStateBroadClient = 'Glb_MMStateClient'; // Broadcast Mailboxname for MMGuardClient
{}

const
  // cChannelNames are completed with an additional label in the implementation part of this unit
  cChannelNames: array[TThreadTyp] of string = (
    'NONE',
    'JobList', // virtual, not realy used
    'MsgList', // virtual, not realy used
    // MMGuard
    'Glb_MMGuard' + cAddOnChannelName,
    'NONE',
    'NONE',
    // MMClient
    'NONE',
    'Glb_MMClientReader' + cAddOnChannelName,
    // JobHandler
    'Glb_JobMain' + cAddOnChannelName,
    'Glb_JobManager' + cAddOnChannelName,
    'Glb_JobController' + cAddOnChannelName,
    'NONE',
    // MsgHandler
    'Glb_MsgMain' + cAddOnChannelName,
    'Glb_MsgController' + cAddOnChannelName,
    'Glb_MsgDispatcher' + cAddOnChannelName,
    'NONE', // MsgTimer
    // StorageHandler
    'Glb_StorageMain' + cAddOnChannelName,
    'Glb_DataPool' + cAddOnChannelName,
    'Glb_ThreadLauncher' + cAddOnChannelName,
    'NONE', // ExpressWorker
    'NONE', // Worker
    // TimeHandler
    'Glb_TimeMain' + cAddOnChannelName,
    'Glb_JobExecute' + cAddOnChannelName,
    'NONE', // ttTimeTicker
    // TxnHandler
    'Glb_TXNMain' + cAddOnChannelName,
    'Glb_TXNWriter' + cAddOnChannelName,
    'NONE_TXNControl' + cAddOnChannelName,
    'NONE_TXNRead1' + cAddOnChannelName,
    'NONE_TXNRead2' + cAddOnChannelName,
    'NONE_TXNRead3' + cAddOnChannelName,
    // WSCHandler
    'Glb_WSCMain' + cAddOnChannelName,
    'Glb_WSCWriter' + cAddOnChannelName,
    // LXHandler
    'Glb_LXMain' + cAddOnChannelName,
    'Glb_LXWriter' + cAddOnChannelName,
    // CIHandler
    'Glb_CIMain' + cAddOnChannelName,
    'Glb_CIWriter' + cAddOnChannelName
    );

  cEventLogDefaultText: array[TThreadTyp] of string = (
    'NONE: ',
    'JobList', // virtual, not realy used
    'MsgList', // virtual, not realy used
    'MMGuardMain: ', 'MMGuardWatchDog: ', 'MMGuardTimer: ',
    'MMClientMain: ', 'MMClientReader: ',
    'JobMain: ', 'JobManager: ', 'JobController: ', 'JobTimer: ',
    'MsgMain: ', 'MsgController: ', 'MsgDispatcher: ', 'MsgTimer: ',
    'StorageMain: ', 'DataPool: ', 'QueueManager: ', 'Worker: ', 'ExpressWorker; ',
    'TimeMain: ', 'JobExecute: ', 'TimeTicker: ',
    'TXNMain: ', 'TXNWriter: ', 'TXNControl: ',
    'TXNRead1: ', 'TXNRead2: ', 'TXNRead3: ',
    'WSCMain: ', 'WSCWriter: ',
    'LXMain: ', 'LXWriter: ',
    'CIMain: ', 'CIWriter: '
    );

{
  cApplicationNames: Array[TSubSystemTyp] of String = (
    'MMGUARD.EXE',
    'MMCLIENT.EXE',
    'JOBHANDLER.EXE',
    'MSGHANDLER.EXE',
    'STORAGEHANDLER.EXE',
    'TIMEHANDLER.EXE',
    'TXNHANDLER.EXE',
    'WSCHANDLER.EXE',
    'OEHANDLER.EXE',
    'CIHANDLER.EXE',
    'APPLICATION.EXE',
    'WSCRPCHANDLER.EXE'
  );
{}

  // only used in MsgController to request to which NetHandler it has to connect
  cNetHandlerNames: array[ntTXN..ntCI] of TSubSystemTyp = (
    ssTXNHandler,
    ssWSCHandler,
    ssLXHandler,
    ssCIHandler
    );

//------------------------------------------------------------------------------
// Definition of all SubSystems and its resources
//------------------------------------------------------------------------------
const
  cMaxThreadDef = 14; // count of threads in SubSystem
  cMaxIPCClientDef = 7; // count of connections from SubSystem to other channels

type
  PIPCClientDef = ^TIPCClientDef;
  TIPCClientDef = record
    ConnectTo: TThreadTyp;
    Timeout: DWord;
    IPCWrite: TIPCClient;
  end;

  // Each SubSystem owns several Channels and threads. This record contains
  // concentrated information's
  PThreadDef = ^TThreadDef;
  TThreadDef = record
    Thread: TmmThread; // object of thread which will read from Channel
    SubSystemTyp: TSubSystemTyp; // needed for EventLog
    ThreadTyp: TThreadTyp;
    MaxChannels: ShortInt;
    IPCRead: TIPCServer;
    State: TMMProcessState;
    ClientCount: Byte;
    IPCClientDef: array[1..cMaxIPCClientDef] of TIPCClientDef;
  end;

  TSubSystemDef = record
    ThreadTyp: TThreadTyp;
    ThreadCount: SmallInt;
    Threads: array[1..cMaxThreadDef] of TThreadDef;
  end;

const
  cSubSystemDefinitions: array[ssJobHandler..ssCIHandler] of TSubSystemDef = (
    {
     * JobMain
     }
    (ThreadTyp: ttJobMain;
    ThreadCount: 3;
     // JobMain -> JobManager
    Threads: ((Thread: nil;
    SubSystemTyp: ssJobHandler;
    ThreadTyp: ttJobManager;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 3;
    IPCClientDef: ((ConnectTo: ttJobMain),
    (ConnectTo: ttJobController; Timeout: cIPCClientWriteTimeout),
    (ConnectTo: ttJobManager; Timeout: cIPCClientWriteTimeout),
    (), (), (), ())),
     // JobMain -> JobController
    (Thread: nil;
    SubSystemTyp: ssJobHandler;
    ThreadTyp: ttJobController;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 5;
    IPCClientDef: ((ConnectTo: ttJobMain),
    (ConnectTo: ttJobManager; Timeout: cIPCClientWriteTimeout),
    (ConnectTo: ttMsgController; Timeout: cIPCClientWriteTimeout),
    (ConnectTo: ttQueueManager; Timeout: cIPCClientWriteTimeout),
    (ConnectTo: ttJobController; Timeout: cIPCClientWriteTimeout),
    (), ())),
     // JobMain -> JobTimer
    (Thread: nil;
    SubSystemTyp: ssJobHandler;
    ThreadTyp: ttJobTimer;
    MaxChannels: - 1; // no IPCServer to create
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttJobMain),
    (ConnectTo: ttJobManager; Timeout: cIPCClientWriteTimeout),
    (), (), (), (), ())),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone))),
    {
     * MsgMain
     }
    (ThreadTyp: ttMsgMain;
    ThreadCount: 3;
     // MsgMain -> MsgController
    Threads: ((Thread: nil;
    SubSystemTyp: ssMsgHandler;
    ThreadTyp: ttMsgController;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 6;
    IPCClientDef: ((ConnectTo: ttMsgMain),
                   (ConnectTo: ttJobController),
                   (ConnectTo: ttDataPoolWriter),
                   (ConnectTo: ttTXNWriter),
                   (ConnectTo: ttWSCWriter),
                   (ConnectTo: ttLXWriter), ())),
     // MsgMain -> MsgDispatcher
    (Thread: nil;
    SubSystemTyp: ssMsgHandler;
    ThreadTyp: ttMsgDispatcher;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 4;
    IPCClientDef: ((ConnectTo: ttMsgMain),
                   (ConnectTo: ttMsgController),
                   (ConnectTo: ttDataPoolWriter),
                   (ConnectTo: ttJobManager),
                   (), (), ())),
     // MsgMain -> MsgTimer
    (Thread: nil;
    SubSystemTyp: ssMsgHandler;
    ThreadTyp: ttMsgTimer;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttMsgMain),
                   (ConnectTo: ttMsgController),
                   (), (), (), (), ())),
               //({ThreadTyp: ttNone}),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone))),
    {
     * StorageMain
     }
    (ThreadTyp: ttStorageMain;
    ThreadCount: 8;
     // StorageMain -> DataPoolWriter
    Threads: ((Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttDataPoolWriter;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (ConnectTo: ttQueueManager),
    (), (), (), (), ())),
     // StorageMain -> QueueManager
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttQueueManager;
    MaxChannels: 0;
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (ConnectTo: ttJobController),
    (), (), (), (), ())),
     // StorageMain -> ExpressWorker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttExpressWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> ExpressWorker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttExpressWorker;
//                ThreadTyp:    ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
    // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
  // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
   // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ())),
     // StorageMain -> Worker
    (Thread: nil;
    SubSystemTyp: ssStorageHandler;
    ThreadTyp: ttWorker;
    MaxChannels: - 1;
    State: psAvailable;
    ClientCount: 1;
    IPCClientDef: ((ConnectTo: ttStorageMain),
    (), (), (), (), (), ()))

    )),
    {
     * TimeMain  TimeHandler
     }
    (ThreadTyp: ttTimeMain;
    ThreadCount: 2;
    Threads: ((Thread: nil;
    SubSystemTyp: ssTimeHandler;
    ThreadTyp: ttJobExecute;
    MaxChannels: 0; // no IPCServer to create
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttTimeMain),
    (ConnectTo: ttJobManager),
    (), (), (), (), ())),

    (Thread: nil;
    SubSystemTyp: ssTimeHandler;
    ThreadTyp: ttTimeTicker;
    MaxChannels: - 1; // no IPCServer to create
    State: psAvailable;
    ClientCount: 2;
    IPCClientDef: ((ConnectTo: ttTimeMain),
    (ConnectTo: ttJobExecute),
    (), (), (), (), ())),

    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone)
    )),

    {
     * TXNMain
     }
    (ThreadTyp: ttTXNMain;
     ThreadCount: 1;
              // TXNMain -> TXNWriter
     Threads: ((Thread: nil;
               SubSystemTyp: ssTXNHandler;
               ThreadTyp: ttTXNWriter;
               MaxChannels: 0;
               State: psAvailable;
               ClientCount: 2;
               IPCClientDef: ((ConnectTo: ttTXNMain),
                              (ConnectTo: ttMsgDispatcher),
                              (), (), (), (), ())),
              // TXNMain -> TXNControl
              (Thread: nil;
               SubSystemTyp: ssTXNHandler;
               ThreadTyp: ttTXNControl;
               MaxChannels: - 1; // no IPCServer to create
               State: psAvailable;
               ClientCount: 2;
               IPCClientDef: ((ConnectTo: ttTXNMain),
                              (ConnectTo: ttMsgDispatcher),
                              (), (), (), (), ())),
               // TXNMain -> TXNRead1
              (Thread: nil;
               SubSystemTyp: ssTXNHandler;
               ThreadTyp: ttTXNRead1;
               MaxChannels: - 1; // no IPCServer to create
               State: psAvailable;
               ClientCount: 3;
               IPCClientDef: ((ConnectTo: ttTXNMain),
                              (ConnectTo: ttMsgDispatcher),
                              (ConnectTo: ttTXNWriter),
                              (), (), (), ())),
               // TXNMain -> TXNRead2
              (Thread: nil;
               SubSystemTyp: ssTXNHandler;
               ThreadTyp: ttTXNRead2;
               MaxChannels: - 1; // no IPCServer to create
               State: psAvailable;
               ClientCount: 3;
               IPCClientDef: ((ConnectTo: ttTXNMain),
                              (ConnectTo: ttMsgDispatcher),
                              (ConnectTo: ttTXNWriter), (), (), (), ())),
               // TXNMain -> TXNRead3
              (Thread: nil;
               SubSystemTyp: ssTXNHandler;
               ThreadTyp: ttTXNRead3;
               MaxChannels: - 1; // no IPCServer to create
               State: psAvailable;
               ClientCount: 3;
               IPCClientDef: ((ConnectTo: ttTXNMain),
                              (ConnectTo: ttMsgDispatcher),
                              (ConnectTo: ttTXNWriter),
                              (), (), (), ())),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone))
    ), // Threads

    {
     * WSCMain
     }
    (ThreadTyp: ttWSCMain;
     ThreadCount: 1;
     Threads: ((Thread: nil;
                SubSystemTyp: ssWSCHandler;
                ThreadTyp: ttWSCWriter;
                MaxChannels: 0;
                State: psAvailable;
                ClientCount: 2;
                IPCClientDef: ((ConnectTo: ttWSCMain),
                               (ConnectTo: ttMsgDispatcher),
                               (), (), (), (), ())),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone),
               (ThreadTyp: ttNone))
     ),
    {
     * LXMain
     }
    (ThreadTyp: ttLXMain;
    ThreadCount: 1;
     // LXMain -> LXWriter
    Threads: ((Thread: nil;
               SubSystemTyp: ssLXHandler;
               ThreadTyp: ttLXWriter;
               MaxChannels: 0;
               State: psAvailable;
               ClientCount: 2;
               IPCClientDef: ((ConnectTo: ttLXMain),
                              (ConnectTo: ttMsgDispatcher),
                              (), (), (), (), ())),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone),
              (ThreadTyp: ttNone))),
    {
     * CIMain
     }
    (ThreadTyp: ttCIMain;
    ThreadCount: 0;
    Threads: ((ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone),
    (ThreadTyp: ttNone)))
    );

//------------------------------------------------------------------------------
// Job Timeout definition
//------------------------------------------------------------------------------
const
  cJobTimeout: array[TJobTyp] of TJobTimeoutRec = (
  // common job definition of both systems
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtNone
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: 0), //* jtCleanUpEv,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtDelJobID,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtJobFailed,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtJobSuccessful,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtMsgComplete,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtMsgNotComplete,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtMsgReceived,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtNewJobEv,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtTimerEvent,
  // individual job definition for MillMaster Loepfe
{10}(JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtTest
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtTestConfirm
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtTestStore,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtAdjust,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtAssign,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtAssignComplete,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtClearZEGrpData
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtClearZESpdData
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtDataAquEv
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtDelInt,
{20}(JobListTmo: 2 * cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtDelShiftByTime,
    (JobListTmo: cDefMsgTimeout * (cDefMsgRep + 1) + cDefOffset; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtDelZEGrpData,
    (JobListTmo: cDefMsgTimeout * (cDefMsgRep + 1) + cDefOffset; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), //* jtDelZESpdData,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtEntryLocked,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), //* jtEntryUnlocked,
    (JobListTmo: 2 * cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenDWData,
    (JobListTmo: 2 * cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenExpDWData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenShiftData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenExpShiftData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenSpdOfflimits,
{30}(JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetDataStopZEGrp,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetDataStopZESpd,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetExpDataStopZEGrp,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetExpDataStopZESpd,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetExpZEGrpData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: 1), // jtGetExpZESpdData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetZEGrpData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: 1), // jtGetZESpdData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetMaAssign,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtSaveMaConfigToDB,
{40}(JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetSettings,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtInitialReset,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtJobResult,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtMaAssChanged,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtMaNotFound,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtMaOffline,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtMaOnline,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtMulGrpDataEv,
// OE Test
    (JobListTmo: 120000; MsgListTmo: 120000; MsgListRep: cDefMsgRep), // jtOEEventJobID,
    (JobListTmo: 120000; MsgListTmo: 120000; MsgListRep: cDefMsgRep), // jtOEEventData,
// OE Test
{50}(JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtReset,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtSendMsgToAppl,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtSetNodeList,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtSetProdID (old: jtSetMaConfig)
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtSetSettings,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtStartGrpEv,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtStopGrpEv,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtSynchronize,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtUplAutoMaData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtViewZEGrpData,
{60}(JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: 1), // jtViewZESpdData,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtDiagnostic,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtInitialized,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetNodeList,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtPrintJob,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtJobFinished,
    (JobListTmo: 0; MsgListTmo: 0; MsgListRep: 0), // jtUnDoGetZESpdData
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtStartGrpEvFromMa,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtStopGrpEvFromMa,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtProdGrpUpdate,
{70}(JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtGetSettingsAllGroups,
    // 20.6.2000 Tmo and Rep changed
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: 1), // jtMaEqualize,
    (JobListTmo: cDefJobTimeout; MsgListTmo: cDefMsgTimeout; MsgListRep: cDefMsgRep), // jtDBDump,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: cDefMsgRep), //* jtGenQOfflimit,
    (JobListTmo: cDefJobTimeout; MsgListTmo: 0; MsgListRep: 0), //* jtBroadcastToClients,
{75}(JobListTmo: cGetZEReadyTimeout + 20000; MsgListTmo: cGetZEReadyTimeout; MsgListRep: 0) //* jtGetZEReady,
    );

//******************************************************************************
// GLOBAL VARIABLES
//******************************************************************************
var
  gMachIDJobTyp: set of TJobTyp; //'tMaEqualize','jtGenShiftData';

//******************************************************************************
// PROTOTYPES
//******************************************************************************
// ---------------- Prototypes from include file -------------------------------
// Gets the size of the job record back
//function CloneJob(aSrc: PJobRec): PJobRec;
function CloneJob(aSrc: PJobRec; var aDst: PJobRec): DWord;
function CopyJob(aSrc, aDest: PJobRec): Boolean;
function GetJobHeaderSize(aJobTyp: TJobTyp): DWord;
function GetJobDataSize(aJob: PJobRec): DWord;

// ---------------- Prototypes from local file ---------------------------------
function FormatMMErrorText(aError: DWord; aErrorTyp: TErrorTyp): string; overload;
function FormatMMErrorText(aErrorRec: TErrorRec): string; overload;
function GetJobName(aJob: TJobTyp): string;
function SetError(aError: DWord; aErrorTyp: TErrorTyp = etNTError; aErrorMsg: string = ''): TErrorRec;
procedure SystemErrorMsg_(aMsg: string; aOwner: TWinControl = nil; aMsgBox: Boolean = True);
function TimeStampToMin(aDateTime: TDateTime): DWord;

function SendGuardCommand(aCommand: TMMGuardCommand; aMsg: String): Boolean;

function GetJobValue(aLiteral: string): TJobTyp;
function BitsetAsString(ABitset: BITSET16): String;
function TText100AsString(AText100: TText100): String;

//******************************************************************************
// IMPLEMENTATION
//******************************************************************************
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  TypInfo, IvDictio, MMEventLog, Dialogs, mmDialogs, mmcs, registry;

//-----------------------------------------------------------------------------
function CloneJob(aSrc: PJobRec; var aDst: PJobRec): DWord;
//var
//  xJobLen: DWord;
begin
//  xJobLen := 0;
  if Assigned(aSrc) then begin
//    Result := GetJobHeaderSize(aSrc^.JobTyp);
    Result := GetJobDataSize(aSrc);
    // d�rfte nur im XML Fall auftreten
//    if xJobLen > Result then
//      Result := xJobLen;
  end else
    Result  := SizeOf(TJobRec);

  aDst := AllocMem(Result);
  if Assigned(aSrc) then begin
    Move(aSrc^, aDst^, Result);
    aDst^.JobLen := Result;
  end;

//  Result  := SizeOf(TJobRec);
//  if Assigned(aSrc) then begin
//    xJobLen := GetJobDataSize(aSrc);
//    // d�rfte nur im XML Fall auftreten
//    if xJobLen > Result then
//      Result := xJobLen;
//  end;
//
//  aDst := AllocMem(Result);
//  if Assigned(aSrc) then begin
//    Move(aSrc^, aDst^, xJobLen);
//    aDst^.JobLen := xJobLen;
//  end;
end;
//------------------------------------------------------------------------------
function CopyJob(aSrc, aDest: PJobRec): Boolean;
begin
  Result := False;
  if Assigned(aSrc) and Assigned(aDest) then begin
    Move(aSrc^, aDest^, aSrc^.JobLen);
    Result := True;
  end;
end;
//-----------------------------------------------------------------------------
function GetJobHeaderSize(aJobTyp: TJobTyp): DWord;
begin
  case aJobTyp of
  // common job definition of both systems
    jtCleanUpEv: Result := sizeof(TCleanUpEv);
    jtDelJobID: Result := sizeof(TDelJobID);
    jtMsgComplete: Result := sizeof(TMsgComplete);
    jtMsgNotComplete: Result := sizeof(TMsgNotComplete);
    jtMsgReceived: Result := sizeof(TMsgReceived);
    jtTimerEvent: Result := sizeof(TTimerEvent);
  // individual job definition for MillMaster Loepfe
    jtNone: Result := sizeof(TNone);
    jtTest: Result := sizeof(TTest);
    jtTestConfirm: Result := sizeof(TTestConfirm);
    jtTestStore: Result := sizeof(TTestStore);
    jtAdjust: Result := sizeof(TAdjust);
    jtAssign: Result := sizeof(TAssign);
    jtAssignComplete: Result := sizeof(TAssignComplete);
    jtClearZEGrpData: Result := sizeof(TClearZEGrpData);
    jtClearZESpdData: Result := sizeof(TClearZESpdData);
    jtDataAquEv: Result := sizeof(TDataAquEv);
    jtDelInt: Result := sizeof(TDelInt);
    jtDelShiftByTime: Result := sizeof(TDelShiftByTime);
    jtDelZEGrpData: Result := sizeof(TDelZEGrpData);
    jtDelZESpdData: Result := sizeof(TDelZESpdData);
    jtEntryLocked: Result := sizeof(TEntryLocked);
    jtEntryUnlocked: Result := sizeof(TEntryUnlocked);
    jtGenDWData: Result := sizeof(TGenDWData);
    jtGenExpDWData: Result := sizeof(TGenExpDWData);
    jtGenShiftData: Result := sizeof(TGenShiftData);
    jtGenExpShiftData: Result := sizeof(TGenExpShiftData);
    jtGenSpdOfflimits: Result := sizeof(TGenSpdOfflimits);
    jtGenQOfflimit: Result := sizeof(TGenQOfflimit);
    jtGetDataStopZEGrp: Result := sizeof(TGetDataStopZEGrp);
    jtGetDataStopZESpd: Result := sizeof(TGetDataStopZESpd);
    jtGetExpDataStopZEGrp: Result := sizeof(TGetExpDataStopZEGrp);
    jtGetExpDataStopZESpd: Result := sizeof(TGetExpDataStopZESpd);
    jtGetExpZEGrpData: Result := sizeof(TGetExpZEGrpData);
    jtGetExpZESpdData: Result := sizeof(TGetExpZESpdData);
    jtGetZEGrpData: Result := sizeof(TGetZEGrpData);
    jtGetZESpdData: Result := sizeof(TGetZESpdData);
    jtGetMaAssign: Result := sizeof(TGetMaAssign);
{ TODO -owss :
XML: Der angeh�ngte XML-String bei der Anwort vergr�ssert die
Jobbuffergr�sse! Bei der Anfrage ist der String im Anhang leer. }
    jtGetSettings: Result := sizeof(TGetSettings);
    jtInitialReset: Result := sizeof(TInitialReset);
    jtJobFailed: Result := sizeof(TJobFailed);
    jtJobResult: Result := sizeof(TJobResult);
    jtJobSuccessful: Result := sizeof(TJobSuccessful);
    jtSendEventToClients: Result := sizeof(TSendEventToClients);
    jtMaNotFound: Result := sizeof(TMaNotFound);
    jtMaOffline: Result := sizeof(TMaOffline);
    jtMaOnline: Result := sizeof(TMaOnline);
    jtMulGrpDataEv: Result := sizeof(TMulGrpDataEv);
    jtNewJobEv: Result := sizeof(TNewJobEv);
//    jtOEEventJobID: Result := sizeof(TOEEventJobID);
//    jtOEEventData: Result := sizeof(TOEEventData);
    jtReset: Result := sizeof(TReset);
{ DONE -owss :
XML: Kontrollieren! Je nach Antwort eventuell unterschiedliche Jobgr�sse,
-> GetSettingsAllGroups verwendet bei der Antwort den Subrecord direkt. }
    jtSendMsgToAppl: Result := sizeof(TSendMsgToAppl);
    jtSetNodeList: Result := sizeof(TSetNodeList);
{ DONE -owss :
XML: Beim Download sind im Anhang die Settings per XML String vorhanden
-> definieren schlussendlich die Gr�sse des Jobbuffers }
    jtSetSettings: Result := sizeof(TSetSettings);
    jtStartGrpEv: Result := sizeof(TStartGrpEv);
    jtStopGrpEv: Result := sizeof(TStopGrpEv);
    jtSynchronize: Result := sizeof(TSynchronize);
    jtUplAutoMaData: Result := sizeof(TUplAutoMaData);
//wss    jtViewZEGrpData: Result := sizeof(TViewZEGrpData);
//wss    jtViewZESpdData: Result := sizeof(TViewZESpdData);
    jtDiagnostic: Result := sizeof(TDiagnostic);
    jtInitialized: Result := sizeof(TInitialized);
    jtGetNodeList: Result := sizeof(TGetNodeList);
    jtPrintJob: Result := sizeof(TPrintJob);
    jtJobFinished: Result := sizeof(TJobFinished);
    jtUnDoGetZESpdData: Result := sizeof(TUnDoGetZESpdData);
    jtSetProdID: Result := sizeof(TSetProdID);
    jtStartGrpEvFromMa: Result := sizeof(TStartGrpEvFromMa);
    jtStopGrpEvFromMa: Result := sizeof(TStopGrpEvFromMa);
    jtProdGrpUpdate: Result := sizeof(TProdGrpUpdate);
    jtGetSettingsAllGroups: Result := sizeof(TGetSettingsAllGroups);
    jtMaEqualize: Result := sizeof(TMaEqualize);
    jtDBDump: Result := sizeof(TDBDump);
    jtBroadcastToClients: Result := sizeof(TBroadcastToClients);
    jtGetZEReady: Result := sizeof(TGetZEReady);
    jtSaveMaConfigToDB: Result := sizeof(TSaveMaConfigToDB);
  else
    Result := 0;
  end;
  Result := Result + cJobHeaderSize;
end;
//-----------------------------------------------------------------------------
function GetJobDataSize(aJob: PJobRec): DWord;
begin
  Result := GetJobHeaderSize(aJob^.JobTyp);
  case aJob^.JobTyp of
    jtGetSettings:      Result := Result + StrLen(aJob^.GetSettings.SettingsRec.XMLData); // abschliessendes 0-Char ist bereits im Record integriert
    jtSetSettings:      Result := Result + StrLen(aJob^.SetSettings.SettingsRec.XMLData); // abschliessendes 0-Char ist bereits im Record integriert
//    jtSetMapfile:    Result := Result + StrLen(aJob^.SetMapfile.Mapfile); // abschliessendes 0-Char ist bereits im Record integriert
    jtSendMsgToAppl: begin
        if aJob^.SendMsgToAppl.ResponseMsg.MsgTyp = rmSaveMaConfigOk then
          Result := Result + StrLen(aJob^.SendMsgToAppl.ResponseMsg.XMLSettingsCollection.XMLData); // abschliessendes 0-Char ist bereits im Record integriert
      end;
    jtStartGrpEv:       Result := Result + StrLen(aJob^.StartGrpEv.SettingsRec.XMLData);
    jtStartGrpEvFromMa: Result := Result + StrLen(aJob^.StartGrpEvFromMa.SettingsRec.XMLData);
    jtStopGrpEvFromMa:  Result := Result + StrLen(aJob^.StopGrpEvFromMa.SettingsRec.XMLData);
    jtStopGrpEv:        Result := Result + StrLen(aJob^.StopGrpEv.SettingsRec.XMLData);
    jtSaveMaConfigToDB: begin
        // DONE wss: Gr�sse ermitteln muss noch �berpr�ft werden, wegen varianten Record!!!
        if aJob^.SaveMaConfig.UseXMLData then
          Result := Result + StrLen(aJob^.SaveMaConfig.XMLData);
      end;
  else
  end;
end;
//------------------------------------------------------------------------------
function GetJobName(aJob: TJobTyp): string;
begin
  Result := GetEnumName(TypeInfo(TJobTyp), Ord(aJob));
end;
//-----------------------------------------------------------------------------
function GetJobValue(aLiteral: string): TJobTyp;
var
  xEnumValue: Integer;
begin
  xEnumValue := GetEnumValue(TypeInfo(TJobTyp), aLiteral);
  // RangeCheck (Bei einem Fehler wird der kleinste Wert zur�ckgegeben)
  if (xEnumValue < 0) or (xEnumValue > ord(High(TJobTyp))) then
    Result := Low(TJobTyp)
  else
    Result := TJobTyp(xEnumValue);
end;// function GetSwitchValue(aLiteral: string): TSwitch;
//-----------------------------------------------------------------------------
function FormatMMErrorText(aError: DWord; aErrorTyp: TErrorTyp): string; overload;
var
  xError: TErrorRec;
begin
  xError.ErrorTyp := aErrorTyp;
  xError.Error := aError;
  FormatMMErrorText(xError);
end;
//-----------------------------------------------------------------------------
function FormatMMErrorText(aErrorRec: TErrorRec): string; overload;
begin
  case aErrorRec.ErrorTyp of
    etNoError:
      Result := aErrorRec.Msg + ' No Error occured';
    etDBError:
      Result := 'DB Error: ' + IntToStr(aErrorRec.Error) + '  ' + aErrorRec.Msg;
    etNTError:
      Result := 'NT Error: ' + FormatErrorText(aErrorRec.Error) + '  ' + aErrorRec.Msg;
    etMMError:
      Result := 'MM Error: ' + FormatErrorText(aErrorRec.Error) + '  ' + aErrorRec.Msg;
    etTXNDrvError:
      Result := 'Texnet driver Error: ' + aErrorRec.Msg;
  else
    Result := 'Unexpected Error Type: error = ' + IntToStr(aErrorRec.Error);
  end;
end;
//------------------------------------------------------------------------------
function SendGuardCommand(aCommand: TMMGuardCommand; aMsg: String): Boolean;
var
  xMsg: TMMGuardRec;
begin
  FillChar(xMsg, sizeof(xMsg), 0);
  xMsg.MsgTyp      := aCommand;
  xMsg.Transmitter := ssApplication;
  xMsg.ComputerAndUser := aMsg + Format(': %s/%s', [MMGetComputerName, GetCurrentMMUser]);

  with TIPCClient.Create(gMMHost, cChannelNames[ttMMGuardMain]) do
  try
    Result := Write(@xMsg, sizeof(xMsg));
  finally
    Free;
  end;
end; 
//------------------------------------------------------------------------------
procedure SystemErrorMsg_(aMsg: string; aOwner: TWinControl; aMsgBox: Boolean);
//var
//  xEventLog: TEventLogWriter;
begin
  if aMsgBox then begin
    SysUtils.Beep;
    MMMessageDlg(cErrorMsg + aMsg, mtError, [mbOk], 0, aOwner);
  end;

  WriteToEventLog(aMsg, 'Assignment', etError, cEventLogClassForSubSystems, gMMHost, ssApplication);
//  try
//    xEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
//                 gMMHost,   // Temp.
//                 ssApplication,
//                 'Assignment',
//                 True);
//    xEventLog.Write ( etError, aMsg );
//    xEventLog.Free;
//  except
//  end;
end;
//------------------------------------------------------------------------------
function SetError(aError: DWord; aErrorTyp: TErrorTyp; aErrorMsg: string): TErrorRec;
begin
  Result.ErrorTyp := aErrorTyp;
  Result.Error := aError;
  Result.Msg := aErrorMsg;
end;

function BitsetAsString(ABitSet: BITSET16): String;
var
  x: Integer;
begin
  Result := '';
  for x := 0 to 15 do
  begin
    if x in ABitSet then
    begin
      if Length(Result) > 1 then Result := Result + ', ';
      Result := Result + IntToStr(x);
    end;
  end;
end;

function TText100AsString(AText100: TText100): String;
begin
  Result := StrPas(PChar(@AText100[1]));
end;

//******************************************************************************
// Caluclate the minutes for a date and time since December 30. 1899, 00:00 Hour
//******************************************************************************
function TimeStampToMin(aDateTime: TDateTime): DWord;
var
  xT0: TTimeStamp;
  xMin0, xMin1: DWord;
begin
  xT0 := DateTimeToTimeStamp(aDateTime);
  xMin0 := ((xT0.Time div 1000) div 60);
  xMin1 := (xT0.Date * 60 * 24);
  Result := xMin1 + xMin0;
end;
//------------------------------------------------------------------------------

var
  xJobTyp: TJobTyp;
  i: Integer;
  xNameList, xParaList: TStringList;
initialization
  gMachIDJobTyp := [//all jobtypes enabled containing MachineID
           //jtCleanUpEv,             // 5: Cleanprocedure for datapool
           //jtDelJobID,              // 6: JobID to delete in datapool
           //jtJobFailed,             // x: The job failed (Error)
           //jtJobSuccessful,         // x: Delete entries in Job- and MsgList for the job
           //jtMsgComplete,           // x: Job in MsgHandler done
           //jtMsgNotComplete,        // x: Job in MsgHandler not finished correctly
           //jtMsgReceived,           // Data from machine received
           //jtNewJobEv,              // 7: New job into JobList from JobManager
           //jtTimerEvent,            // 8: Timer has expired, make an event
             // individual job definition for MillMaster Loepfe
           //jtNone,
{10}//jtTest,
           //jtTestConfirm,
           //jtTestStore,
  jtAdjust, // Adjust on machine
    jtAssign, // 8: Assign on machine (range or settings)
    jtAssignComplete, // 8: Assign on machine (range or settings) completed
    jtClearZEGrpData, // Clears the datapots for groups on ZE
    jtClearZESpdData, // 1a:Clears the datapots for spindles on ZE
           //jtDataAquEv,             // 8: Dataaquisation event from TimeHandler
           //jtDelInt,                // 1a:Delete oldest intervalldata on DB
{20}//jtDelShiftByTime,        // 1a:All shift related data older time x will be deleted
  jtDelZEGrpData, // Delete group data on ZE
    jtDelZESpdData, // 1a:Delete spindle data on ZE
    jtEntryLocked, // 9: Key on ZE became locked
    jtEntryUnlocked, // 9: Key on ZE became unlocked
           //jtGenDWData,             // 2: Generating data for DW-tables. Succeeding jtGenShiftData
           //jtGenExpDWData,          // 2: Gen.data for DW-tables (also changes in already ex.sets). Suc.jtGenExpShiftData
  jtGenShiftData, // 2: Start generating shift data
    jtGenExpShiftData, // 2: Generate shift data of an already existing shift
           //jtGenSpdOfflimits,       // 2: Start generating offlimits on the base of spindle data
{30} jtGetDataStopZEGrp, // regular group data aquisition at ProdGrp stop
    jtGetDataStopZESpd, // 1: regular spindle data aquisition at ProdGrp stop (before jtGetDataStop)
    jtGetExpDataStopZEGrp, // irregular group data aquisition at ProdGrp stop
    jtGetExpDataStopZESpd, // 1: irregular spindle data aquisition at ProdGrp stop (before jtGetDataStop)
    jtGetExpZEGrpData, // irregular group data aquisition at shift end
    jtGetExpZESpdData, // 1: irregular spindle data aquisition at shift end (before jtGetExpData)
    jtGetZEGrpData, // regular group data aquisition at intervall end
    jtGetZESpdData, // 1: regular spindle data aquisition at intervall end (before jtGetIntData)
    jtGetMaAssign, // 3: Get machine configuration
    jtSaveMaConfigToDB, // ?? Get Machine configuration from ZE (only at initial startup at the moment)
{40} jtGetSettings, // ?? Get settings from prodgroup
    jtInitialReset, // 9: Coldstart on machine (ZE)
           //jtJobResult,             // ?? JobResult from NetHandler to MsgDispatcher
           //jtSendEventToClients,    // 2: Event to all client (e.g. One of the floor relevant data changed)
  jtMaNotFound, // ?? Could not find machine in node list
    jtMaOffline, // 9: Machine state changed to offline
    jtMaOnline, // 9: Machine state changed to online
           //jtMulGrpDataEv,          // ?? Start multiple group aquisition
           //jtOEEventJobID,          // Job to send new JobID to OEHandler
           //jtOEEventData,           // Each received event from CIMServer
{50}jtReset, // ??9: Warmstart on ZE (after PowerOff)
           //jtSendMsgToAppl,         // 2: Inform application about something
           //jtSetNodeList,           // 1a:Load NodeList to the NetHandler
    jtSetProdID, // Set new ProdID to machine after settings change on machine
    jtSetSettings, // 3: Load settings of a prodgroup to ZE
    jtStartGrpEv, // 8: Start aquisition of prodgroup data (from MM)
    jtStopGrpEv, // 8: Stop aquisition of prodgroup data (from MM)
    jtSynchronize, // 4: Embed job jtMaEqualize
           //jtUplAutoMaData,         // ?? Prodgroup setting and spindledata arrived (automatic after change on ZE)
  jtViewZEGrpData, // regular group data aquisition without deletion (before jtGetMulGrpData)
{60} jtViewZESpdData, // ?? regular spindle data aquisition without deletion (before jtGetOneGrpData)
           //jtDiagnostic ,           // 8: diagnistic Job
           //jtInitialized,           // 4: To signalize that a handler is initialized
           //jtGetNodeList,           // 3: Load NodeList from NetHandler to JobHandler
           //jtPrintJob,              // ?? PrintJob
           //jtJobFinished,           // 7: Delete entries in JobList. Msg from JobManager
  jtUnDoGetZESpdData, // 2: Delete the stored Interval/ProdGrp Spindle Data from Database
    jtSetProdID, // ??Ueberfluessig da mit SetSettings abgehandelt? (Nue:11.11.99) on machine (Send ProdID from MM to machine)
    jtStartGrpEvFromMa, // 9: Start aquisition of prodgroup data (from Machine (AC338))
    jtStopGrpEvFromMa, // 9: Stop aquisition of prodgroup data (from Machine (AC338))
{70}//jtProdGrpUpdate,         // Update from prodgroup DB-tables t_prodgroup or t_prodgroup_state
  jtGetSettingsAllGroups, // 3: Get settings from all groupmemories
    jtMaEqualize, // 3: Equalize machine and Millmaster
           //jtDBDump,                // 2a: Starts a dump of a database
           //jtGenQOfflimit,          // Generation of Quality Offlimit
           //jtBroadcastToClients,    // 8: Event happend in MMClient or MMApplication (changed to other job(s) and send last to all MMClients
  jtGetZEReady // 1a: Checks after MaOnline, if ZE is ready.
    ];

  //-----------------------------------------------------------------
  // Timeouts f�r Job/Msg Handler k�nnen �ber die Registry �bersteuert werden
  // Eintrag sieht wie folgt aus und kann per DebugConfig erstellt werden:
  // "jtName" = "JobTimeout,MsgTimeout,MsgRepetition" wobei Timeouts in ms entsprechen
  // Beispiel: "jtSetSettings" = "600000,180000,1"
  //-----------------------------------------------------------------
  xNameList := TStringList.Create;
  xParaList := TStringList.Create;
  with TRegistry.Create do
  try
    RootKey := cRegLM;
    if OpenKeyReadOnly(cRegMMTimeouts) then begin
      GetValueNames(xNameList);
      for i:=0 to xNameList.Count-1 do begin
        if GetDataType(xNameList[i]) = rdString then begin
          xParaList.CommaText := ReadString(xNameList[i]);
          if xParaList.Count = 3 then begin
            xJobTyp := GetJobValue(xNameList[i]);
            with cJobTimeout[xJobTyp] do begin
              JobListTmo := StrToIntDef(xParaList[0], JobListTmo);
              MsgListTmo := StrToIntDef(xParaList[1], MsgListTmo);
              MsgListRep := StrToIntDef(xParaList[2], MsgListRep);
            end;
          end;
        end;
      end;
    end;
  finally
    Free;
    xNameList.Free;
    xParaList.Free;
  end;
  //-----------------------------------------------------------------
end.

