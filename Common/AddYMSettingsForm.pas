(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AddYMSettingsForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 28.12.1999  1.00  Mg  | Projekt started
| 20.04.2000  1.01  Mg  | Spindelranges inserted
| 08.05.2000  1.02  Mg  | Help inserted
| 15.05.2000  1.03  Mg  | Procedure UpdateComponent inserted
| 27.06.2000  1.04  Mg  | Page Control and LengthMode inserted
| 22.03.2006  2.00  Mue | XML / LZE-MM-Anbindung; LengthModeToItemIndex eingefügt; Inhalt von bCancelClick
|=============================================================================*)
unit AddYMSettingsForm;               
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmLabel, Mask, mmMaskEdit,
  AssignComp, IvDictio, IvMulti, IvEMulti, mmTranslator, ExtCtrls,
  mmRadioGroup, ComCtrls, mmPageControl, MMUGlobal, LoepfeGlobal, YMParaDef,
  mmButton, mmEdit, xmlMaConfigClasses;

type
  TAddYMSettings = class(TDialogBottom)
    bMachineDefaults: TmmButton;
    mmTranslator1: TmmTranslator;
    bHelp: TmmButton;
    pcAddSettings: TmmPageControl;
    tsMachine: TTabSheet;
    tsYarnMaster: TTabSheet;
    mmLabel1: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel5: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel6: TmmLabel;
    rgLenMode: TmmRadioGroup;
    edLenWin: TmmEdit;
    edSpeed: TmmEdit;
    edSpeedRamp: TmmEdit;
    procedure bOKClick(Sender: TObject);
    procedure bMachineDefaultsClick(Sender: TObject);
    procedure edKeyPress(Sender: TObject; var Key: Char);
    procedure edExit(Sender: TObject);
    procedure bHelpClick(Sender: TObject);
    procedure Change(Sender: TObject);
    procedure rgLenModeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
  private
    mSpdRange: TBaseSpindleRange;
    fSpeed: integer;
    fSpeedRamp: integer;
    FLengthWindow: integer;
    FLengthMode: integer;
    FAddYMSettingsChanged: boolean;
    fOrgLengthMode: Integer;
    fOrgLengthWindow: Integer;
    procedure LengthModeToItemIndex;
//    mMaconfig: TMaconfigReader;
    procedure SetLengthMode(const Value: integer);
    procedure SetLengthWindow(const Value: integer);
//NUE1    procedure Check;
  public
    constructor Create(aOwner: TComponent); override;
    function ShowModal: integer; override;
    property Speed: integer read fSpeed;
    property SpeedRamp: integer read fSpeedRamp;
    property LengthWindow: integer read FLengthWindow write SetLengthWindow;
    property LengthMode: integer read FLengthMode write SetLengthMode;
    procedure UpdateComponent(aSpdRange: TBaseSpindleRange);
    property AddYMSettingsChanged: boolean read FAddYMSettingsChanged;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp,
  BaseGlobal, XMLDef, SettingsReader;
{$R *.DFM}
//------------------------------------------------------------------------------
constructor TAddYMSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Zusaetzliche_Einstellungen.htm');
end;

//------------------------------------------------------------------------------
//NUE1
//procedure TAddYMSettings.Check;
//begin
//  try
//      // Do Check if Editfield is Disabled because the Machine does support the Field
//    if edSpeed.Enabled then begin
//      if StrToInt(edSpeed.Text) > 1600 then
//        edSpeed.Text := '1600';
//      if StrToInt(edSpeed.Text) < 500 then
//        edSpeed.Text := '500';
//    end;
//  except edSpeed.Text := '1000';
//  end;
//
//  try
//    if edSpeedRamp.Enabled then begin
//      if StrToInt(edSpeedRamp.Text) > 30 then
//        edSpeedRamp.Text := '30';
//      if StrToInt(edSpeedRamp.Text) < 1 then
//        edSpeedRamp.Text := '1';
//    end;
//  except edSpeedRamp.Text := '10';
//  end;
//
//  try
//    if edLenWin.Enabled then begin
//      if StrToInt(edLenWin.Text) > 1000 then
//        edLenWin.Text := '1000';
//      if StrToInt(edLenWin.Text) < 10 then
//        edLenWin.Text := '10';
//    end;
//  except edLenWin.Text := '1000';
//  end;
//end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.UpdateComponent(aSpdRange: TBaseSpindleRange);
begin
  if not Assigned(aSpdRange) then Exit;

  mSpdRange := aSpdRange;

  edSpeed.Enabled := True;
  edSpeedRamp.Enabled := True;
  edLenWin.Enabled := True;

  fLengthWindow := mSpdRange.LengthWindow;
  fLengthMode := mSpdRange.LengthMode;

  LengthModeToItemIndex;   //Nue:22.3.06

  edLenWin.Text := IntToStr(fLengthWindow);

//  if aSpdRange.MachineType = mtAC338 then begin
  if not (mSpdRange.MaConfigReader.FPHandler.ValueDef[cFP_SpeedSimulationName, False]) then begin
    edSpeed.Enabled     := False;
    edSpeedRamp.Enabled := False;
    fSpeed              := mSpdRange.Speed;
    fSpeedRamp          := mSpdRange.SpeedRamp;

    if mSpdRange is TProdGrp then begin
      edSpeed.Text     := IntToStr(fSpeed);
      edSpeedRamp.Text := IntToStr(fSpeedRamp);
    end
    else begin
      edSpeed.Text     := '';
      edSpeedRamp.Text := '';
    end;
  end
  else begin
    fSpeed           := mSpdRange.Speed;
    fSpeedRamp       := mSpdRange.SpeedRamp;
    edSpeed.Text     := IntToStr(fSpeed);
    edSpeedRamp.Text := IntToStr(fSpeedRamp);
  end;
//  case mSpdRange.FrontType of
//    ftInf68K, ftInfPPC: begin
//        edSpeed.Enabled := False;
//        edSpeedRamp.Enabled := False;
//        fSpeed := mSpdRange.Speed;
//        fSpeedRamp := mSpdRange.SpeedRamp;
//        if mSpdRange is TProdGrp then begin
//          edSpeed.Text := IntToStr(fSpeed);
//          edSpeedRamp.Text := IntToStr(fSpeedRamp);
//        end
//        else begin
//          edSpeed.Text := '';
//          edSpeedRamp.Text := '';
//        end;
//      end;
//  else
//    fSpeed := mSpdRange.Speed;
//    fSpeedRamp := mSpdRange.SpeedRamp;
//    edSpeed.Text := IntToStr(fSpeed);
//    edSpeedRamp.Text := IntToStr(fSpeedRamp);
//  end;
end;
//------------------------------------------------------------------------------
function TAddYMSettings.ShowModal: integer;
begin
  FAddYMSettingsChanged := False;
  Result := inherited ShowModal;
end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.bOKClick(Sender: TObject);
begin
  inherited;
  try
//NUE1    Check;
    if edSpeed.Enabled then
      fSpeed := StrToInt(edSpeed.Text);
    if edSpeedRamp.Enabled then
      fSpeedRamp := StrToInt(edSpeedRamp.Text);
    if edLenWin.Enabled then
      fLengthWindow := StrToInt(edLenWin.Text);

  except
    on e: Exception do begin
      SystemErrorMsg_('TAddYMSettings.bOKClick failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.bMachineDefaultsClick(Sender: TObject);
begin
  inherited;
  case pcAddSettings.ActivePageIndex of
    0: begin
        if mSpdRange is TProdGrp then begin
          fSpeed := (mSpdRange as TProdGrp).MachSpeed;
          fSpeedRamp := (mSpdRange as TProdGrp).MachSpeedRamp;
        end
        else begin
          fSpeed := mSpdRange.Speed;
          fSpeedRamp := mSpdRange.SpeedRamp;
        end;

//        if mSpdRange.MachineType = mtAC338 then begin
      if not (mSpdRange.MaConfigReader.FPHandler.ValueDef[cFP_SpeedSimulationName, False]) then begin
        end
        else begin
          edSpeed.Text     := IntToStr(fSpeed);
          edSpeedRamp.Text := IntToStr(fSpeedRamp);
        end;
//        case mSpdRange.FrontType of
//          ftInf68K, ftInfPPC: begin
//
//            end;
//        else
//          edSpeed.Text := IntToStr(fSpeed);
//          edSpeedRamp.Text := IntToStr(fSpeedRamp);
//        end;
      end;
  else
    fLengthMode  := TMMSettingsReader.Instance.Value[cMMLenghtMode];
    fLengthWindow  := TMMSettingsReader.Instance.Value[cYMLenWindow];


//    if mSpdRange is TProdGrp then begin
//      fLengthWindow := (mSpdRange as TProdGrp).MachLengthWindow;
//      fLengthMode := (mSpdRange as TProdGrp).MachLengthMode;
//    end
//    else begin
//      fLengthWindow := mSpdRange.LengthWindow;
//      fLengthMode := mSpdRange.LengthMode;
//    end;
//
    edLenWin.Text := IntToStr(fLengthWindow);
//
//
    LengthModeToItemIndex;   //Nue:22.3.06
  end;
end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.edKeyPress(Sender: TObject; var Key: Char);
begin
  inherited;
  CheckIntChar(Key);
end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.edExit(Sender: TObject);
begin
  inherited;
//NUE1  Check;
end;
//------------------------------------------------------------------------------
procedure TAddYMSettings.bHelpClick(Sender: TObject);
begin
  inherited;
  Application.HelpCommand(0, self.HelpContext);
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.Change(Sender: TObject);
begin
  inherited;
  FAddYMSettingsChanged := True;
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.LengthModeToItemIndex;
begin
  case fLengthMode of
    ORD(lwmFirst): rgLenMode.ItemIndex := 0;
    ORD(lwmLast): rgLenMode.ItemIndex := 1;
    ORD(lwmCone): rgLenMode.ItemIndex := 2;
  else
    rgLenMode.ItemIndex := 0;
  end;
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.rgLenModeClick(Sender: TObject);
begin
  inherited;
  case rgLenMode.ItemIndex of
    0: fLengthMode := ORD(lwmFirst);
    1: fLengthMode := ORD(lwmLast);
    2: fLengthMode := ORD(lwmCone);
  else
    fLengthMode := cDefaultLengthMode;
  end;
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.SetLengthMode(const Value: integer);
begin
  FLengthMode := Value;
  LengthModeToItemIndex;   //Nue:22.3.06
//  rgLenMode.ItemIndex := Value;
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.SetLengthWindow(const Value: integer);
begin
  edLenWin.Text := IntToStr(Value);
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.FormShow(Sender: TObject);
begin
  inherited;

  fOrgLengthWindow := mSpdRange.LengthWindow;
  fOrgLengthMode := mSpdRange.LengthMode;

  if (mSpdRange.MaConfigReader.NetTyp = ntLX) then begin
    //Weil die Elemente LengthMode und LengthWindow nicht im Datenrecord der LZE enthalten sind! (Werden nicht auf Maschine geschrieben!)
    tsYarnMaster.TabVisible := False;
  end else begin
    tsYarnMaster.TabVisible := True;
  end; //else
end;

//------------------------------------------------------------------------------
procedure TAddYMSettings.bCancelClick(Sender: TObject);
begin
  inherited;
  fLengthWindow := fOrgLengthWindow;
  edLenWin.Text := IntToStr(fLengthWindow);
  fLengthMode := fOrgLengthMode;
  LengthModeToItemIndex;
end;
//------------------------------------------------------------------------------

end.

