(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AVGBorderFrame
| Projectpart...: MillMaster Spulerei
| Subpart.......: Berechnungsmethoden
| Process(es)...: -
| Description...: GUI f�r die Berechnungsmethode 'AVGBorder'
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows NT/2000/XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 16.12.2003  1.00  Lok | Initial release
|=============================================================================*)
unit AVGBorderFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  CalcMethodBaseFrame, StdCtrls, mmLineLabel, mmEdit, mmLabel, CalcMethods,
  IvDictio, IvMulti, IvEMulti, mmTranslator, ClassDef;

type
  (*: Klasse:        TfrmAvgBorderFrame
      Vorg�nger:     TfrmCalcMethodBaseFrame
      Kategorie:     No category
      Kurzbeschrieb: Konstruktor 
      Beschreibung:  
                     - *)
  //1 Konstruktor 
  TfrmAvgBorderFrame = class(TfrmCalcMethodBaseFrame)
    edDeviation: TmmEdit;
    edShiftCompression: TmmEdit;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
  protected
    //1 F�gt die Methoden Settings zur String Liste hinzu 
    procedure GetInternalMethodSettings; override;
    //1 Zeigt die Methodensettings im GUI an 
    procedure SetInternalMethodSettings; override;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent; aDataItem: TBaseDataItem); overload; override;
  end;
  

implementation

{$R *.DFM}
uses
  LoepfeGlobal;
  
(*:
  Klasse:        TfrmAvgBorderFrame
 *  Vorg�nger:     TfrmCalcMethodBaseFrame
 *  Kategorie:     No category
 *  Kurzbeschrieb: Konstruktor 
 *  Beschreibung:  - 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmAvgBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner, aDataItem)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmAvgBorderFrame.Create(aOwner: TComponent; aDataItem: TBaseDataItem);
begin
  mCalcMethodType := cmAVG;

//: ----------------------------------------------
  inherited Create(aOwner, aDataItem);
end;// TfrmAvgBorderFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInternalMethodSettings
 *  Klasse:           TfrmAvgBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�gt die Methoden Settings zur String Liste hinzu
 *  Beschreibung:     
                      Die StringListe wird in der Basisklasse verwaltet.
 --------------------------------------------------------------------*)
procedure TfrmAvgBorderFrame.GetInternalMethodSettings;
begin
  inherited GetInternalMethodSettings;
  mMethodParam.Values[cAVGShiftCompression] := edShiftCompression.Text;
  mMethodParam.Values[cAVGDeviation] := MMFloatToStr(edDeviation.AsFloat);
end;// TfrmAvgBorderFrame.GetInternalMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetInternalMethodSettings
 *  Klasse:           TfrmAvgBorderFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zeigt die Methodensettings im GUI an
 *  Beschreibung:     
                      Die StringListe wird in der Basisklasse verwaltet.
 --------------------------------------------------------------------*)
procedure TfrmAvgBorderFrame.SetInternalMethodSettings;
begin
  inherited SetInternalMethodSettings;
  edShiftCompression.Text := mMethodParam.ValueDef(cAVGShiftCompression, 5);
  (* String  in einen Float verwandeln um den
     L�nderspezifischen Dezimaltrenner in den String einzuf�gen *)
  edDeviation.AsFloat := MMStrToFloat(mMethodParam.ValueDef(cAVGDeviation, 0));
end;// TfrmAvgBorderFrame.SetInternalMethodSettings cat:No category


initialization
  RegisterClass(TfrmAvgBorderFrame);
  
end.

