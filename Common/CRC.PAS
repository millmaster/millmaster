unit CRC;

interface

  function GetCRC16(s:string):integer;

implementation

{ ***************************************************************************
  * CRC16_1.PAS
  * Turbo Pascal Routinen zur Berechnung des CRC16
  * OHNE Verwendung von irgendwelchen Tabellen
  *
  * Wickenh�user Elektrotechnik, Karlsruhe (http://www.wickenhaeuser.de)
  *
  * Stand: 16.11.1998
  ***************************************************************************}


const Polynom = $1021;


{ ***************************************************************************
  * UpdateCRC16_2
  * Berechnung des CRC16 - reines Pascal - zum besseren Nachvollziehen
  *
  * Data      = Byte-Wert �ber den der CRC berechnet werden soll
  * OldCRC    = alter CRC (bei Beginn = 0)
  ***************************************************************************}
function UpdateCRC16(Data: byte; OldCRC: word): word;
var NewCRC: word;
   BitCount: byte;
begin
  NewCRC:=(OldCRC xor (Data SHL 8));
  for BitCount:=1 to 8 do
    if ((NewCRC AND $8000) <> 0) then
       NewCRC:=((NewCRC SHL 1) XOR Polynom)
    else
       NewCRC:=(NewCRC SHL 1);
  result:=NewCRC;
end;// function UpdateCRC16_2(Data: byte; OldCRC: word): word;

function GetCRC16(s:string):integer;
var
  i:integer;
begin
  result:=0;
  for i:=1 to Length(s) do begin
    result:=UpdateCRC16(ord(s[i]),result);
  end;//
end;//

end.
