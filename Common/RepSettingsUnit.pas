(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_lib.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Allgemeine Funktionen und Prozeduren
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.02, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.09.00 | 1.00 | Mg  | File created
| 30.08.01 | 1.01 | Wss | Open: fill order of combobox changed -> check: should be date ordering DESC
| 26.09.01   1.02   Wss | YM_set_name added in output of prodgroup info in combobox
| 01.11.01   1.02   Wss | correct YarnCount filled in CSettings by using of global settings of YarnUnit
| 26.02.02   1.02   Wss | QMatix mit TSizingPanel in richtigen Proportionen darstellen
| 28.02.02   1.02   Wss | Aufteilung von Tab RepSettings angepasst
| 04.03.02   1.02   Wss | TSizingPanel nicht mehr noetig, da RepQMatrix Komponente Verhaeltnis
                          nun selber abhandelt
| 18.09.02   1.03   Nue | Umbau ADO
| 03.12.02   1.02   Wss | Bug in cobSettingsChange Methode behoben: es wurde per ItemIndex auf die
                          SettingsFinder Liste zugegriffen, wodurch dann die falschen YMSettings zur
                          Korrekten Überschrift gezeigt wurden.
| 25.01.05   1.03   SDo | Umbau auf XML (MM-Vers. 5.0)
| 15.06.05   1.02   Wss | Optische Korrekturen und "Wiederholung" EditBox eingefügt
| 18.02.08   1.10   Nue | Einbau VCV und neue Komponente mSpliceQMatrix
|=========================================================================================*)


unit RepSettingsUnit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmPanel, StdCtrls, Nwcombo, ComCtrls,
  mmPageControl, mmComboBox, IvDictio, IvMulti,
  IvEMulti, mmTranslator, mmLabel, mmGroupBox, EditBox,
  YMParaEditBox, MMUGlobal, XMLDef, QualityMatrixBase, QualityMatrix,
  XMLSettingsModel, VCLXMLSettingsModel, SizingPanel, DataLayerDef,
  ClassDataReader, mmCheckBox;

const cListSep = '|';


type

   TProdGroupX = class(TObject)
   private
      fClearSetID : integer;
      fMachName : String;
      fLotName : String;
      fTimeRange : String;
      fYMSetName : String;
      fYarnCount : Single;
      fYarnUnit : TYarnUnit;
   public
      property ClearSetID: integer read fClearSetID write fClearSetID;
      property MachName: string read fMachName write fMachName;
      property LotName: string read fLotName write fLotName;
      property YMSetName: string read fYMSetName write fYMSetName;
      property TimeRange: string read fTimeRange write fTimeRange;
      property YarnCount: Single read fYarnCount write fYarnCount;
      property YarnUnit: TYarnUnit read fYarnUnit write fYarnUnit;
   end;


  TRepSettings = class(TFrame)
    pnBaseTop: TmmPanel;
    cobSettings: TmmComboBox;
    mmPanel2: TmmPanel;
    laTimeRange: TmmLabel;
    laLot: TmmLabel;
    laTemplate: TmmLabel;
    laMachine: TmmLabel;
    pnSettings: TmmPanel;
    mmPanel1: TmmPanel;
    mmPanel3: TmmPanel;
    pnBottom: TmmPanel;
    mFaultCluster: TFaultClusterEditBox;
    spQMatrix: TSizingPanel;
    mQMatrix: TQualityMatrix;
    mmPanel6: TmmPanel;
    mmPanel4: TmmPanel;
    mChannel: TChannelEditBox;
    mSplice: TSpliceEditBox;
    mYarnCount: TYarnCountEditBox;
    mSFI: TSFIEditBox;
    SizingPanel1: TSizingPanel;
    mFMatrix: TQualityMatrix;
    mmPanel5: TmmPanel;
    mFCluster: TFFClusterEditBox;
    mPSettings: TPEditBox;
    RepetitionEditBox1: TRepetitionEditBox;
    mVCV: TVCVEditBox;
    mSpliceQMatrix: TQualityMatrix;
    mCurveSelection: TmmPanel;
    mcbSpliceCurve: TmmCheckBox;
    mcbChannelCurve: TmmCheckBox;
    mcbClusterCurve: TmmCheckBox;
    procedure cobSettingsChange(Sender: TObject);
    procedure cobSettingsDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure mcbChannelCurveClick(Sender: TObject);
    procedure mcbClusterCurveClick(Sender: TObject);
    procedure mcbSpliceCurveClick(Sender: TObject);

  private
    //mYarnUnit: TYarnUnit;
    fModel: TVCLXMLSettingsModel;
    fClassDataReader: TClassDataReader;
    mSettings: TXMLSettingsInfoRec;
    fMachineName : String;
    mOwner: TComponent;
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    function GetSelectedSettings: TProdGroupInfo;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Open();
    property SelectedSettings: TProdGroupInfo read GetSelectedSettings;
    property ClassDataReader: TClassDataReader read fClassDataReader write fClassDataReader;
    procedure ClearSettingsList;
    property MachineName : String read fMachineName write fMachineName;

  published
    //property SettingsFinder : TSettingsFinder read fSettingsFinder write SetSettingsFinder;
    property Model: TVCLXMLSettingsModel read fModel write SetModel;

  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS,

  MMSetupModul; //, SettingsReader;
//------------------------------------------------------------------------------
(*
procedure TRepSettings.SetSettingsFinder ( aSettingsFinder : TSettingsFinder );
begin
{25.01.2005 @SDo
  fSettingsFinder := aSettingsFinder;
  RepQMatrix1.SettingsFinder := aSettingsFinder;
  RepQMatrix2.SettingsFinder := aSettingsFinder;
  RepCSettings1.SettingsFinder := aSettingsFinder;
  RepCSettings2.SettingsFinder := aSettingsFinder;
  RepCSettings3.SettingsFinder := aSettingsFinder;
  RepCSettings4.SettingsFinder := aSettingsFinder;
  RepCSettings5.SettingsFinder := aSettingsFinder;
}
end;
//------------------------------------------------------------------------------
*)
procedure TRepSettings.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification ( AComponent, Operation );
{25.01.2005 @SDo
  if Assigned ( fSettingsFinder ) then
    if ( Operation = opRemove ) and ( AComponent = fSettingsFinder ) then begin
      fSettingsFinder := nil;
      RepQMatrix1.SettingsFinder := nil;
      RepQMatrix2.SettingsFinder := nil;
      RepCSettings1.SettingsFinder := nil;
      RepCSettings2.SettingsFinder := nil;
      RepCSettings3.SettingsFinder := nil;
      RepCSettings4.SettingsFinder := nil;
      RepCSettings5.SettingsFinder := nil;
    end;
}

  //if Assigned ( fModel ) then
    if ( Operation = opRemove ) and ( AComponent = fModel ) then begin
      fModel := nil;
      mQMatrix.Model := nil;
      mSpliceQMatrix.Model := nil;    //Nue:18.02.08
      mFMatrix.Model := nil;
      mChannel.Model := nil;
      mSplice.Model := nil;
      mYarnCount.Model := nil;
      mFaultCluster.Model := nil;
      mSFI.Model := nil;
      mFCluster.Model := nil;
      mVCV.Model := nil;    //Nue:18.02.08
    end;

end;
//------------------------------------------------------------------------------
procedure TRepSettings.Open();
var
  i: Integer;
  //xText : String;
  //xMachName : String;

  xProdGroup : TProdGroupInfo;
begin
  if Assigned(fModel) and Assigned(fClassDataReader) then begin
     ClearSettingsList;

     //Daten aus fClassDataReader in CB schreiben
     for i:=0 to fClassDataReader.ProdGroupList.Count-1 do begin
       xProdGroup := fClassDataReader.ProdGroupList.Items[i];
       //YarnCount und Yarnunit separat ermitteln
//       fClassDataReader.GetXMLSettingsFromID(fClassDataReader.XMLSettings.Items[i].SetID, mSettings);
       cobSettings.Items.AddObject(xProdGroup.YMSetName, xProdGroup);
       Application.ProcessMessages;
     end;
  end;

//  if Assigned(fModel) and Assigned(fClassDataReader) then begin
//     ClearSettingsList;
//
//     //Daten aus fClassDataReader in CB schreiben
//     for i:=0 to fClassDataReader.XMLSettings.Count-1 do begin
//       xProdGroup := TProdGroup.Create;
//
//       xProdGroup.ClearSetID := fClassDataReader.XMLSettings.Items[i].SetID;
//       xProdGroup.YMSetName  := fClassDataReader.XMLSettings.Items[i].SetName;
//       xProdGroup.YarnUnit   := fClassDataReader.Params.YarnUnit;
//       xProdGroup.MachName   := MachineName;
//
//       //YarnCount und Yarnunit separat ermitteln
//       fClassDataReader.GetXMLSettingsFromID(fClassDataReader.XMLSettings.Items[i].SetID, mSettings);
//       xProdGroup.YarnCount := mSettings.YarnCount;
//       xProdGroup.YarnUnit  := mSettings.YarnUnit;
//
//       cobSettings.Items.AddObject(xProdGroup.YMSetName, xProdGroup);
//
//       Application.ProcessMessages;
//     end;
//  end;

  with cobSettings do begin
    ItemIndex := 0;
    Enabled := (Items.Count > 1);
  end;
  mCurveSelection.Left := (mQMatrix.Left+mQMatrix.Width)- mCurveSelection.Width;  //Nue:11.03.08
  mCurveSelection.Visible := True;  //Nue:11.03.08
  pnSettings.Visible := (cobSettings.Items.Count > 0);
  cobSettingsChange(self);
end;
//------------------------------------------------------------------------------
procedure TRepSettings.cobSettingsChange(Sender: TObject);
var
  xProdGroup : TProdGroupInfo;
  xSetting: TXMLSettingsInfoRec;
begin
EnterMethod('cobSettingsChange');
  with cobSettings do begin
    if ItemIndex >= 0 then begin
      //Settings
      xProdGroup           := Items.Objects[ItemIndex] as TProdGroupInfo;
      mYarnCount.YarnCount := xProdGroup.YarnCount;
      mYarnCount.YarnUnit  := fClassDataReader.Params.YarnUnit;

      fClassDataReader.GetXMLSettingsFromID(xProdGroup.YMSetID, xSetting);
      fModel.xmlAsString   := xSetting.XMLData;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TRepSettings.cobSettingsDrawItem(Control: TWinControl;
  Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  xIsSelected : boolean;
  //xProdGrp    : TProdGrp;
  xPos: Integer;
  xProdGrpTimeRange: String;
  xProdGroup : TProdGroupInfo;
begin
  with (Control as TmmComboBox) do begin
    xProdGroup := TProdGroupInfo(Items.Objects[Index]);

    if xProdGroup.ProdStart <> ( xProdGroup.ProdEnd )  then
      xProdGrpTimeRange := DateTimeToStr(xProdGroup.ProdStart) + ' - ' + DateTimeToStr(xProdGroup.ProdEnd)
    else
      xProdGrpTimeRange := DateTimeToStr(xProdGroup.ProdStart) + ' -  ...';

    // check for selected item
    xIsSelected := odSelected in state;
    // now draw the information manually with formatted text
    with Canvas do begin
      // clear background
      FillRect(Rect);
      if xIsSelected then Font.Color := clHighlightText
                     else Font.Color := clWindowText;


      xPos := 0;
      if not xIsSelected then
        Font.Color := clBlack;
      TextOut(Rect.Left + xPos,Rect.Top, xProdGroup.ProdName);
      inc(xPos, laLot.Width);

      TextOut(Rect.Left + xPos,Rect.Top, xProdGroup.YMSetName);
      inc(xPos, laTemplate.Width);

      TextOut(Rect.Left + xPos,Rect.Top,xProdGroup.MachName);
      inc(xPos, laMachine.Width);

      TextOut(Rect.Left + xPos,Rect.Top, xProdGrpTimeRange);
    end; // with Canvas



{25.01.2005 @SDo
    // get prodgroup object for detailed information
    xProdGrp := TProdGrp(Items.Objects[Index]);
    if xProdGrp.ProdStart <> ( xProdGrp.ProdEnd )  then
      xProdGrpTimeRange := DateTimeToStr(xProdGrp.ProdStart) + ' - ' + DateTimeToStr(xProdGrp.ProdEnd)
    else
      xProdGrpTimeRange := DateTimeToStr(xProdGrp.ProdStart) + ' -  ...';

    // check for selected item
    xIsSelected := odSelected in state;
    // now draw the information manually with formatted text
    with Canvas do begin
      // clear background
      FillRect(Rect);
      if xIsSelected then Font.Color := clHighlightText
                     else Font.Color := clWindowText;


      xPos := 0;
      if not xIsSelected then
        Font.Color := clBlack;
      TextOut(Rect.Left + xPos,Rect.Top, xProdGrp.ProdGrpName);
      inc(xPos, laLot.Width);

      TextOut(Rect.Left + xPos,Rect.Top, xProdGrp.YMSetName);
      inc(xPos, laTemplate.Width);

      TextOut(Rect.Left + xPos,Rect.Top, xProdGrp.MachName);
      inc(xPos, laMachine.Width);

      TextOut(Rect.Left + xPos,Rect.Top, xProdGrpTimeRange);
    end; // with Canvas
}
  end; // with (Control as ...
end;
//------------------------------------------------------------------------------
constructor TRepSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
{25.01.2005 @SDo
  // get the global YarnUnit
  with TMMSettingsReader.Instance do
    mYarnUnit   := Value[cYarnCntUnit];
  // Globale YarnUnit noch setzen
  RepCSettings1.YarnUnit := mYarnUnit;
}
  mSpliceQMatrix.Visible := False; //Nue:11.03.08
  mCurveSelection.Visible := False;  //Nue:11.03.08
  cobSettings.Enabled := FALSE;
  mOwner := aOwner;
end;
//------------------------------------------------------------------------------
procedure TRepSettings.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    fModel := aValue;
    mQMatrix.Model      := aValue;
    mSpliceQMatrix.Model      := aValue;   //Nue:18.02.08
    mFMatrix.Model      := aValue;
    mChannel.Model      := aValue;
    mSplice.Model       := aValue;
    mYarnCount.Model    := aValue;
    mFaultCluster.Model := aValue;
    mSFI.Model          := aValue;
    mFCluster.Model     := aValue;
    mPSettings.Model    := aValue;
    mVCV.Model          := aValue;   //Nue:18.02.08
  end;
end;
//------------------------------------------------------------------------------
function TRepSettings.GetSelectedSettings: TProdGroupInfo;
begin
  Result:= cobSettings.Items.Objects[cobSettings.ItemIndex] as TProdGroupInfo;
end;
//------------------------------------------------------------------------------
destructor TRepSettings.Destroy;
var x : integer;
begin
  ClearSettingsList;
  inherited;
end;
//------------------------------------------------------------------------------
procedure TRepSettings.ClearSettingsList;
begin
  with cobSettings.Items do begin
    while Count > 0 do begin
//      (Objects[0] as TProdGroup).Free;
      cobSettings.Items.Delete(0);
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TRepSettings.mcbChannelCurveClick(Sender: TObject);
begin
  mQMatrix.Visible := mcbChannelCurve.Checked; //Nue:2.7.07
  mSpliceQMatrix.Visible := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbSpliceCurve.Checked := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked; //Nue:2.7.07
end;

//------------------------------------------------------------------------------
procedure TRepSettings.mcbClusterCurveClick(Sender: TObject);
begin
  mQMatrix.ClusterVisible := mcbClusterCurve.Checked;
end;

//------------------------------------------------------------------------------
procedure TRepSettings.mcbSpliceCurveClick(Sender: TObject);
begin
  mSpliceQMatrix.Visible :=  mcbSpliceCurve.Checked;
  mQMatrix.Visible := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbChannelCurve.Checked := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := not(mcbSpliceCurve.Checked);
end;

//------------------------------------------------------------------------------

end.



