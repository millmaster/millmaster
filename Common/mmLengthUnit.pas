(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLengthUnit.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.04.99 | 0.00 | PW  | Datei erstellt
| 20.07.00 | 1.00 | Mg  | TLen Base anstelle eLengthUnitT
| 14.08.00 | 1.00 | Wss | TLenBase changed into LoepfeGlobal to avoid include MMUGlobal
| 25.02.03 | 1.00 | Wss | Property HelpContext hinzugefügt
|=========================================================================================*)

unit mmLengthUnit;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, LoepfeGlobal;

type
 // Mg eLengthUnitT = (lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,lu1Kg,lu1Lbs,luAbsolute,luCone,luBobine);
 // sLengthUnitT = set of eLengthUnitT;
  sLengthUnitT = set of TLenBase;
const
 //cEuropeanLengthUnits = [lu1000Km,lu100Km,lu1Kg,luAbsolute,luCone,luBobine];
  cEuropeanLengthUnits = [lbAbsolute, lb100Km, lb1000Km];
 //cAmericanLenthUnits  = [lu1000Kyrd,lu100Kyrd,lu1Lbs,luAbsolute,luCone,luBobine];
  cAmericanLenthUnits = [lbAbsolute, lb100000y, lb1000000y];
 //cAllLengthUnits      = [lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,lu1Kg,lu1Lbs,luAbsolute,luCone,luBobine];
  cAllLengthUnits = [lbAbsolute, lb100Km, lb1000Km, lb100000y, lb1000000y];
 //cClassLengthUnits    = [lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,luAbsolute];
  cClassLengthUnits = [lbAbsolute, lb100Km, lb1000Km, lb100000y, lb1000000y];

type
  TmmLengthUnits = class(TComponent)
  private
   //FLengthUnit: eLengthUnitT;
    FLengthUnit: TLenBase;
    FLengthUnitSet: sLengthUnitT;
    fHelpContext: Integer;
  protected
  public
    property HelpContext: Integer read fHelpContext write fHelpContext;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Execute: boolean;
  published
    property LengthUnit: TLenBase read FLengthUnit write FLengthUnit;
    property LengthUnitSet: sLengthUnitT read FLengthUnitSet write FLengthUnitSet;
  end; //TmmLengthUnits

//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmLengthUnitDlg;

constructor TmmLengthUnits.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fHelpContext := 0;
end; //constructor TmmLengthUnits.Create
//-----------------------------------------------------------------------------
destructor TmmLengthUnits.Destroy;
begin
  inherited Destroy;
end; //destructor TmmLengthUnits.Destroy
//-----------------------------------------------------------------------------
function TmmLengthUnits.Execute: boolean;
begin
  if Owner is TWinControl then
    mmLengthUnitsDlg := TmmLengthUnitsDlg.Create(Owner)
  else
    mmLengthUnitsDlg := TmmLengthUnitsDlg.Create(Application);
  try
    mmLengthUnitsDlg.HelpContext   := fHelpContext;
    mmLengthUnitsDlg.LengthUnitSet := LengthUnitSet;
    mmLengthUnitsDlg.LengthUnit    := LengthUnit;

    Result := mmLengthUnitsDlg.ShowModal = IDOk;
    if Result then
      LengthUnit := mmLengthUnitsDlg.LengthUnit;
  finally
    mmLengthUnitsDlg.Free;
  end; //try..finally
end; //function TmmLengthUnits.Execute
//-----------------------------------------------------------------------------
{
procedure Register;
begin
 RegisterComponents('Worbis',[TmmLengthUnits]);
end; //procedure Register
{}
end. //mmLengthUnit.pas

