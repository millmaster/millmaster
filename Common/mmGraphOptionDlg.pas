(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphOptionDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: graph options editor
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.01 | PW  |
|=========================================================================================*)

unit mmGraphOptionDlg;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,CheckLst,ComCtrls,ToolWin,ImgList,ActnList,mmGraphGlobal,mmActionList,
 mmImageList,mmRadioGroup,mmGroupBox,mmPanel,mmToolBar,IvDictio,IvMulti,IvEMulti,
 NumCtrl,mmCheckBox,mmLabel,mmPageControl,mmTranslator,mmButton,BASEFORM;

type
 TmmGraphOptionsDlg = class(TmmForm)
  mmTranslator1: TmmTranslator;

  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  Panel1: TmmPanel;
   mmPageControl1: TmmPageControl;
   tsOptions: TTabSheet;
   tsYAxis: TTabSheet;
   rgBarType: TmmRadioGroup;
   GroupBox1: TmmGroupBox;
   laLeftMin: TmmLabel;
   laLeftMax: TmmLabel;
   cbYLeftAutoScaling: TmmCheckBox;
   edLeftMin: TNumEdit;
   edLeftMax: TNumEdit;
   GroupBox2: TmmGroupBox;
   laRightMin: TmmLabel;
   laRightMax: TmmLabel;
   cbYRightAutoScaling: TmmCheckBox;
   edRightMin: TNumEdit;
   edRightMax: TNumEdit;
   tsXAxis: TTabSheet;
   GroupBox3: TmmGroupBox;

  paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure acHelpExecute(Sender: TObject);
  procedure edLeftMinExit(Sender: TObject);
  procedure FormCreate(Sender: TObject);
  procedure cbYLeftAutoScalingClick(Sender: TObject);
  procedure cbYRightAutoScalingClick(Sender: TObject);
  private
   FAutoScaling: array[eYAxisT] of boolean;
   FBarStyle: eBarStyleT;
   FDefaultMinY: array[eYAxisT] of ValueType;
   FDefaultMaxY: array[eYAxisT] of ValueType;
   procedure SetAutoScaling(index: eYAxisT; value: boolean);
   function GetAutoScaling(index: eYAxisT): boolean;
   procedure SetBarStyle(value: eBarStyleT);
   function GetBarStyle: eBarStyleT;
   procedure SetDefaultMinY(index: eYAxisT; value: ValueType);
   function GetDefaultMinY(index: eYAxisT): ValueType;
   procedure SetDefaultMaxY(index: eYAxisT; value: ValueType);
   function GetDefaultMaxY(index: eYAxisT): ValueType;
  public
   property AutoScaling[index: eYAxisT]: boolean read GetAutoScaling write SetAutoScaling;
   property BarStyle: eBarStyleT read GetBarStyle write SetBarStyle;
   property DefaultMinY[index: eYAxisT]: ValueType read GetDefaultMinY write SetDefaultMinY;
   property DefaultMaxY[index: eYAxisT]: ValueType read GetDefaultMaxY write SetDefaultMaxY;
 end; //TmmGraphOptionsDlg

var
 mmGraphOptionsDlg: TmmGraphOptionsDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TmmGraphOptionsDlg.SetAutoScaling(index: eYAxisT; value: boolean);
begin
 FAutoScaling[index] := value;
 cbYLeftAutoScaling.Checked := FAutoScaling[yaxLeft];
 cbYRightAutoScaling.Checked := FAutoScaling[yaxRight];
end; //procedure TmmGraphOptionsDlg.SetAutoScaling
//-----------------------------------------------------------------------------
function TmmGraphOptionsDlg.GetAutoScaling(index: eYAxisT): boolean;
begin
 result := FAutoScaling[index];
end; //function TmmGraphOptionsDlg.GetAutoScaling
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.SetBarStyle(value: eBarStyleT);
begin
 FBarStyle := value;
 rgBarType.ItemIndex := ord(value);
end; //procedure TmmGraphOptionsDlg.SetSortOrder
//-----------------------------------------------------------------------------
function TmmGraphOptionsDlg.GetBarStyle: eBarStyleT;
begin
 result := eBarStyleT(rgBarType.ItemIndex);
end; //function TmmGraphOptionsDlg.GetSortOrder
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.SetDefaultMinY(index: eYAxisT; value: ValueType);
begin
 FDefaultMinY[index] := value;
 edLeftMin.Value := DefaultMinY[yaxLeft];
 edRightMin.Value := DefaultMinY[yaxRight];
end; //procedure TmmGraphOptionsDlg.SetDefaultMinY
//-----------------------------------------------------------------------------
function TmmGraphOptionsDlg.GetDefaultMinY(index: eYAxisT): ValueType;
begin
 result := FDefaultMinY[index];
end; //function TmmGraphOptionsDlg.GetDefaultMinY
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.SetDefaultMaxY(index: eYAxisT; value: ValueType);
begin
 if value <> FDefaultMaxY[Index] then FDefaultMaxY[index] := value;
 edLeftMax.Value := DefaultMaxY[yaxLeft];
 edRightMax.Value := DefaultMaxY[yaxRight];
end; //procedure TmmGraphOptionsDlg.SetDefaultMaxY
//-----------------------------------------------------------------------------
function TmmGraphOptionsDlg.GetDefaultMaxY(index: eYAxisT): ValueType;
begin
 result := FDefaultMaxY[index];
end; //function TmmGraphOptionsDlg.GetDefaultMaxY
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.acOKExecute(Sender: TObject);
begin
 bOK.SetFocus; //forcing exit-event for numctrl's

 Close;
 ModalResult := mrOK;
end; //procedure TmmGraphOptionsDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.acCancelExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrCancel;
end; //procedure TmmGraphOptionsDlg.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.acHelpExecute(Sender: TObject);
begin
 //
end; //procedure TmmGraphOptionsDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.edLeftMinExit(Sender: TObject);
begin
 DefaultMinY[yaxLeft] := edLeftMin.Value;
 DefaultMaxY[yaxLeft] := edLeftMax.Value;
 DefaultMinY[yaxRight] := edRightMin.Value;
 DefaultMaxY[yaxRight] := edRightMax.Value;
end; //procedure TmmGraphOptionsDlg.edLeftMinExit
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.FormCreate(Sender: TObject);
begin
 mmPageControl1.ActivePage := tsOptions;
end; //procedure TmmGraphOptionsDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.cbYLeftAutoScalingClick(Sender: TObject);
begin
 FAutoScaling[yaxLeft] := cbYLeftAutoScaling.Checked;
end; //procedure TmmGraphOptionsDlg.cbYLeftAutoScalingClick
//-----------------------------------------------------------------------------
procedure TmmGraphOptionsDlg.cbYRightAutoScalingClick(Sender: TObject);
begin
 FAutoScaling[yaxRight] := cbYRightAutoScaling.Checked;
end; //procedure TmmGraphOptionsDlg.cbYRightAutoScalingClick

end. //mmGraphOptionsDlg.pas
