(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: LabMasterDef.pas
| Projectpart...: Definition file for LabMaster
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 10.11.2000 1.00 Wss | File created
| 27.08.2001 1.00 Wss | Coarse/Fine QMatrixMode implemented
| 30.08.2001 1.00 Wss | Enable/disable visibility for filter values at printout
| 26.02.2002 1.00 Wss | Feld AdjustBaseCnt hinzugefuegt fuer korrekte SFI berechnung wie in Hostlink
| 13.06.2002 1.01 Nue | Zusaetze wegen Einbau Longterm.
| 10.07.2002 1.02 Nue | Conditional compiling added for OrgLabReport an MMUSB
| 18.07.2002 1.03 Nue | Name TParameters changed to TQueryParameters
| 04.10.2002 1.03 Wss | Option for Hierarchy table and DescendingSort member for Key sorting added
| 14.10.2002 1.03 Wss | COffCnt, CUpY Element hinzugef�gt
| 13.02.2003 1.03 Wss | Property UsedInFloor hinzugef�gt
| 26.06.2003 1.03 Lok | Member QO bei TDataItemRec hinzugef�gt
                  Wss | Text f�r Gewicht und L�nge korrigiert (vorher Weight, Len)
| 02.07.2003 1.04 SDO | TDataItemRec und cDataItemArray fuer MatrixEditor erweitert
|                     | Flag: CD fuer Classdesigner
| 10.12.2003 1.03 Wss | Neue Datenelemente hinzugef�gt
| 19.02.2004 1.03 Wss | Datenelement SpindleRange hinzugef�gt -> DB-Version 4.03 n�tig!
| Juni 2005  1.03 Wss | Neue Datenelemente f�r SpectraPlus und Zenit
| 09.06.2005 2.03 Wss | Garn/Kurznummerbezeichung: gem�ss Jo nun definitiv CYCnt... und CSCnt...
| 21.10.2005 2.03 Wss | Handling von PEff korrigiert damit Berechnungsmethoden funktionieren
                        -> UseMode f�r PEff auf umNormal ge�ndert
| 05.07.2006 2.03 Wss | Datenelement Schichtteam hinzugef�gt -> DB-Version 5.03 n�tig!
| 19.11.2007 2.04 Nue | Adding additional PrintSettings-Komponents to saved settings on DB (t_settings)
| 10.01.2008 2.05 Nue | Einbau von VCV.
|=============================================================================*)
unit LabMasterDef;

interface

uses
  Classes, DB, Graphics, Messages, IniFiles, Windows,
  Chart, QualityMatrixDef, MMUGlobal, ClassDef;

resourcestring
  rsAll              = '(*)Alle'; // ivlm
  rsHidenFilterText  = '(*)Anzeige ausgeschalten'; // ivlm
  rsPrintSettings    = '(20)Eigenschaften'; // ivlm
  rsNavigate         = '(14)Navigieren'; //ivlm
  rsAnalyse          = '(14)Auswerten'; //ivlm
  rsSaveSelection    = '(*)Selektion mitspeichern'; // ivlm
  rsMsgRemoveProfile = '(*)Konnte Profil nicht laden. Soll es von der Liste entfernt werden?'; // ivlm

type
  TSourceSelection     = (ssStyle, ssLot, ssMach, ssTime, ssYMSet);
  TSourceSelectionSet  = set of TSourceSelection;

  TBarStyle    = (bsNormal, bsSide, bsStacked, bsStacked100);
  TChartType   = (ctNone, ctLine, ctBar, ctArea, ctMixed);
  TDataMode    = (dmChart, dmGrid, dmMatrix);
  TAxisType    = (atLeft, atLeftTop, atRight, atRightTop);
  TAxisScaling = (asAutomatic, asManual, asDetermine);

  TDataModeSet = set of TDataMode;
  TSet10       = set of 1..10;
  TSet255      = set of 1..255;

const
  cPrefixCol  = 'c_';
  cPrefixCalc = 'calc_';
  cXLabelAngle: Array[Boolean] of Integer = (0, 300);

  cDummySeriesCnt = 3;
  cDummyValuesCnt = 10;
  cDummyValues: Array[0..cDummySeriesCnt-1, 1..cDummyvaluesCnt] of Integer = (
    (1, 2, 4, 3, 6, 8, 3, 5, 9, 7),
    (3, 7, 1, 5, 9, 3, 2, 4, 6, 8),
    (4, 3, 2, 7, 5, 9, 5, 8, 2, 1)
  );

  cMaxCommaCount = 4;
  cNumberFormat: Array[0..cMaxCommaCount-1] of String = (
    '#0',
    '#0.0',
    '#0.0#',
    '#0.00#'
  );

  cChartAlign: Array[0..3] of TLegendAlignment = (laLeft, laRight, laTop, laBottom);
  cDisplayModeIndex: Array[0..1] of TDisplayMode = (dmCalculateSCValues, dmValues);

  // Diese Konstante definiert die Zeit f�r die TmmLineSeries, ab wieviel Zeitdifferenz
  // zwischen den St�tzdaten KEINE Verbindunslinie gezeichnet werden soll, damit
  // zeitliche Pausen von Artikeln auch optisch (=grafisch) angezeigt werden kann.
  // TmmLineSeries.DateTimeDelta := cSeriesDateTimeDeltas[]
  // In  der Draw-Methode wird momentan noch mit dem Faktor 2 gerechnet, um eine
  // gewisse Toleranz zu erhalten
  cSeriesDateTimeDeltas: Array[TTimeMode] of Single = (
    1/24,  // f�r Interval 1 Stunde wird als default genommen
    8/24,  // f�r Schicht 8 Stunden werden als default genommen
    7);    // f�r Langzeit 7 Tage werden als Default genommen

  cTabJumpWidth = 40; // wenn #9 f�r Textformatierungen verwendet wird dann entspricht dies einem TextWidth = 40

type
  // used for drag mode in StringGrid for col moving
  TScrollDirection = (sdNone, sdLeft, sdRight);

  PAxisInfoRec = ^TAxisInfoRec;
  TAxisInfoRec = record
    Scaling: TAxisScaling;
    Min: Integer;
    Max: Integer;
  end;

  PReportOptions = ^TReportOptions;
  TReportOptions = record
    CompanyName: String;
    ReportTitle: String;
    ShowExtend: Boolean;
    UseYMSettings: Boolean;
    ShowStyle: Boolean;
    ShowLot: Boolean;
    ShowMachine: Boolean;
    ShowYMSettings: Boolean;
    PrintChart: Boolean;
    PrintQMatrix: Boolean;
    PrintCluster: Boolean;
    PrintSpliceMatrix: Boolean;
    PrintFFMatrix: Boolean;
    PrintBlackWhite: Boolean;
    PrintCSChannel: Boolean;
    PrintCSSplice: Boolean;
    PrintCSCluster: Boolean;
    PrintCSFCluster: Boolean;
    PrintCSSFI: Boolean;
    PrintCSVCV: Boolean;
    PrintCSRepetition: Boolean;
    PrintCSYarnCount: Boolean;
    PrintTable: Boolean;
    StretchTable: Boolean;
    NewPageMatrix: Boolean;
    NewPageYMSettings: Boolean;
    NewPageTable: Boolean;
    RectChart: TRect;
    RectQMatrix: TRect;
    RectSpliceMatrix: TRect;
    RectFFMatrix: TRect;
    RectCSChannel: TRect;    //Nue:19.11.07
    RectCSRepetition: TRect; //Nue:19.11.07
    RectCSSplice: TRect;     //Nue:19.11.07
    RectCSFCluster: TRect;   //Nue:19.11.07
    RectCSPSettings: TRect;  //Nue:19.11.07
    RectCSCluster: TRect;    //Nue:19.11.07
    RectCSSFI: TRect;        //Nue:19.11.07
    RectCSVCV: TRect;        //Nue:14.01.08
    RectCSYarnCount: TRect;  //Nue:19.11.07
  end;
  
  PFontRec = ^TFontRec;
  TFontRec = record
    Name: TFontName;
    Color: TColor;
    BkColor: TColor;
    Size: Integer;
    Style: TFontStyles;
  end;
  
  PGridOptions = ^TGridOptions;
  TGridOptions = record
    Options: TSet10;
    FixedCols: Integer;
    TitleFont: TFontRec;
    DataFont: TFontRec;
    Hierarchy: Boolean;
  end;
  
  PChartOptions = ^TChartOptions;
  TChartOptions = record
    BarStyle: TBarStyle;
    LegendAlign: TLegendAlignment;
    View3D: Boolean;
    AllowRotate: Boolean;
    Width3D: Integer;
    RotateX: Integer;
    RotateY: Integer;
    EndColor: TColor;
    StartColor: TColor;
    GradientDir: Integer;
    ImageInside: Boolean;
    ImageFile: String;
    Zoom: Integer;
    HPos: Integer;
    VPos: Integer;
    XLabelRotated: Boolean;
    AxisInfo: Array[TAxisType] of TAxisInfoRec;
    CalcMethodsConfig: String;
    UseCalcMethods: Boolean;
    SplitXAxis: Boolean;
  end;
  
  PMatrixOptions = ^TMatrixOptions;
  TMatrixOptions = record
    ChannelVisible: Boolean;
    ClusterVisible: Boolean;
    SpliceVisible: Boolean;
    DefectValues: Boolean;
    CutValues: Boolean;
    ActiveVisible: Boolean;
    ActiveColor: TColor;
    InactiveColor: TColor;
    MatrixMode: Integer;
  end;
  
  TUseMode   = (umNormal, umNever, umAlways);
  TMassField = (mfNone, mfLen, mfWT);

//  PDataItemRec = ^TDataItemRec;
  TDataItemRec = record
    DisplayName: String[30];
    Field: String[30];
    KeyField: Boolean;
    KeyFieldName: String;
    MassField: TMassField;
    DataType: TFieldType;
    CalcMode: Integer;
    UseMode: TUseMode;
    Commas: Integer;
    ShowSeconds: Boolean;
    Thousands: Boolean;
    Alignment: TAlignment;
    DisplayWidth: Integer;
    ShowInTable: Boolean;
    TableIndex: Integer;
    DescendingSort: Boolean;
    ShowInChart: Boolean;
    ChartIndex: Integer;
    ChartType: TChartType;
    Color: TColor;
    LowerLimit: Single;
    UpperLimit: Single;
    RightAxis: Boolean;
    AverageStep: Integer;
    ClassTypeGroup: TClassTypeGroup;
    QO: Boolean;
    CD: Boolean;
    LR: Boolean;
  end;

//------------------------------------------------------------------------------
const
  cTimeModeTables: Array[TTimeMode] of String = (
    'v_production_interval v', 'v_production_shift v', 'v_production_week v'
  );

  cDetailKeyItemFields: Array[ssStyle..ssYMSet] of String = (
    'style_name', 'prod_name', 'machine_name', 'time', 'YM_set_name'
  );

  cMassFields: Array[TMassField] of String = ('', 'len', 'wtSpd');

  cBasicQuery: Array[TTimeMode] of String = (
                // Interval
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where v.c_shiftcal_id = %d ' +
                '%s ',             // zus�tzliche Where Anweisungen
                // shift
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where v.c_shiftcal_id = %d ' +
                '%s ',             // zus�tzliche Where Anweisungen
                // Longterm
                'select %s %s ' +  // first key fields then data fiels (cQryDataCols)
                'from %s ' +
                'where 1 = %d ' +    //NUE nicht fuer ewig
                '%s '              // zus�tzliche Where Anweisungen
  );

  cWhereStyleId_lr  = 'and v.c_style_id in (%s)';
  cWhereLotId_lr    = 'and v.c_prod_id in (%s)';
  cWhereMachId_lr   = 'and v.c_machine_id in (%s)';
  cWhereTimeID      = 'and v.c_time_id in (%s)';
  cWhereShiftRange  = 'and v.c_time between :TimeFrom and :TimeTo';
  cWhereWeekRange   = 'and v.c_time_filter between :TimeFrom and :TimeTo';
  cWhereYMSetId_lr  = 'and v.c_ym_set_id in (%s)';

  cSelectCountField = '('+ '''#''' + '+cast(count(distinct %s) as char)) AS %s';

const
  // CalcMode:    (Nue:wird in MMUGlobal verwendet!)
  // 0  = Laengenbasiert (LenBase)
  // >0 = Laengenbasiert (Zahl entspricht Meter)
  // -1 = Keine Berechnung: Mode fuer 1:1 Durchreichung ab DB c_ -> calc_
  // -2 = SFI/D
  // -3 = SFI
  // -4 = ProdNutzeffekt
  // -5 = Temporaerer Mode fuer 1:1 Durchreichung ab DB c_ -> calc_ (in spaeterer Phase KeyField mit Visibility mit enum's erweitern! NUE
  // -6 = CVd (SFI/D*cVCV`Factor) Nue:12.02.08  ??????

  cDataItemCount = 70;    //Nue:11.02.08 wegen VCV von 67 auf 70 erh�ht
  cDataItemArray: Array[0..cDataItemCount-1] of TDataItemRec = (
    // this are key fields
    (DisplayName:'(15)Artikel'; // ivlm
     Field:'style_name';    KeyField:True; KeyFieldName: 'c_style_name';   MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
    (DisplayName:'(15)Partie';   // ivlm
     Field:'prod_name';     KeyField:True; KeyFieldName: 'c_prod_name';    MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
    (DisplayName:'(15)Maschine'; // ivlm
     Field:'machine_name';  KeyField:True; KeyFieldName: 'c_machine_name'; MassField:mfNone; DataType: ftString;   CalcMode: -1; ShowInTable: True; TableIndex: 1; Color: clInfoBk; LR: True),
    (DisplayName:'(25)Zeit';  // ivlm
     Field:'time';          KeyField:True; KeyFieldName: 'c_time';         MassField:mfNone; DataType: ftDateTime; CalcMode: -1; ShowInTable: True; TableIndex: 0; Color: clInfoBk; LR: True),
    (DisplayName:'(20)Reinigereinstellungen';  // ivlm
     Field:'YM_set_name';   KeyField:True; KeyFieldName: 'c_YM_set_name';  MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
    (DisplayName:'(*)Spindelbereich';  // ivlm
     Field:'spindle_range'; KeyField:True; KeyFieldName: 'c_spindle_range';MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
    (DisplayName:'(*)Schichtteam';  // ivlm
     Field:'team_id'; KeyField:True; KeyFieldName: 'c_team_id';MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
//    (DisplayName:'(*)Woche';  
//     Field:'week'; KeyField:True; KeyFieldName: 'c_week';MassField:mfNone; DataType: ftString;   CalcMode: -1; Color: clInfoBk; LR: True),
    // this are data fields
    (DisplayName:'%EffP';        Field:'PEff';    KeyField:False; MassField:mfWT; DataType: ftFloat; CalcMode: -4; UseMode: umNormal; Color: 65280; QO: false; LR: True), //
    (DisplayName:'(15)Gewicht kg'; // ivlm
     Field:'kg';    KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 128; QO: false; LR: True),
    (DisplayName:'(15)Gewicht lb'; // ivlm
     Field:'lb';    KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 128; QO: false; LR: True),
    (DisplayName:'(15)Laenge km'; // ivlm
     Field:'km';      KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 255; QO: False; LR: True),
    (DisplayName:'(15)Laenge yards'; // ivlm
     Field:'yards';   KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 255; QO: False; LR: True),
    (DisplayName:'SFI';          Field:'SFI';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -3; UseMode: umAlways; Color: 16777041; ClassTypeGroup: ctLabData; QO: True; LR: True), //
    (DisplayName:'SFI/D';        Field:'SFI_D';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -2; UseMode: umNever; Color: 16570115; ClassTypeGroup: ctLabData; QO: True; LR: True), //
    (DisplayName:'CSFI';         Field:'CSFI';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 1451435; CD: TRUE; LR: True), //

    (DisplayName:'CYTot';        Field:'CYTot';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    ShowInTable: True; TableIndex: 2; ShowInChart: True; ChartIndex: 2; Color: 16744576; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CN';           Field:'CN';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 14802077; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CS';           Field:'CS';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 32896; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CL';           Field:'CL';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 4210688; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CT';           Field:'CT';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 12615808; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CF';           Field:'CSIRO';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1341291; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CSp';          Field:'CSp';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1341291; QO: True; CD: TRUE; LR: True), //

    (DisplayName:'CClS';         Field:'CClS';       KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 1424106; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CFCl';         Field:'CSIROCl';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16711680; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'YB';           Field:'YB';         KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16744703; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'Sp';           Field:'Sp';         KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 16711808; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CYCnt';        Field:'COffCnt';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 13251949; QO: True; LR: True), //
    (DisplayName:'CUpY';         Field:'CUpY';       KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;    Color: 65535; QO: True; CD: TRUE; LR: True), //

    (DisplayName:'INeps/km';     Field:'INeps';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 14079702; ClassTypeGroup: ctLabData; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'ISmall/m';     Field:'ISmall';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1;    Color: 204; ClassTypeGroup: ctLabData;      QO: True; CD: TRUE; LR: True), //
    (DisplayName:'IThick/km';    Field:'IThick';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 183; ClassTypeGroup: ctLabData;      QO: True; CD: TRUE; LR: True), //
    (DisplayName:'IThin/km';     Field:'IThin';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 7783934; ClassTypeGroup: ctLabData;  QO: True; CD: TRUE; LR: True), //
    (DisplayName:'I2-4/km';      Field:'I2_4';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 12711922; ClassTypeGroup: ctLabData; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'I4-8/km';      Field:'I4_8';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 293070; ClassTypeGroup: ctLabData;   QO: True; CD: TRUE; LR: True), //
    (DisplayName:'I8-20/km';     Field:'I8_20';   KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 15573757; ClassTypeGroup: ctLabData; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'I20-70/km';    Field:'I20_70';  KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 1000; Color: 14411488; ClassTypeGroup: ctLabData; QO: True; CD: TRUE; LR: True), //

    // Neu 10.12.2003
    (DisplayName:'CBu';          Field:'CBu';        KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;   Color: 13372404; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'CDBu';         Field:'CDBu';       KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;   Color: 13251949; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'RSp';          Field:'RSp';        KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;   Color: 16766333; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'CSys';         Field:'CSys';       KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;   Color: 15456472; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LSt';          Field:'LSt';        KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 5530027;  QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckClS';       Field:'LckClS';     KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 10132324; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckF';         Field:'LckSIRO';    KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 16711680; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckSFI';       Field:'LckSFI';     KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 6619057;  QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckFCl';       Field:'LckSIROCl';  KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 16512;    QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckSys';       Field:'LckSys';     KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 10976513; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckYCnt';      Field:'LckOffCnt';  KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8684799;  QO: False; CD: TRUE; LR: False), //
    // Neu 02.06.2005
    (DisplayName:'CP';            Field:'CP';               KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8014799; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CClL';          Field:'CClL';             KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8024799; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CClT';          Field:'CClT';             KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8034799; QO: True; CD: TRUE; LR: True), //
     // Der Allgemeine COffCnt wird weiter oben so belassen und stellt die Summe von Plus/Minus dar f�r ZE
    (DisplayName:'CYCnt+';        Field:'COffCntPlus';      KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 13251949; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CYCnt-';        Field:'COffCntMinus';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8104799;  QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CSCnt';         Field:'CShortOffCnt';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 13251949; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CSCnt+';        Field:'CShortOffCntPlus'; KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8044799;  QO: True; CD: TRUE; LR: True), //
    (DisplayName:'CSCnt-';        Field:'CShortOffCntMinus';KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8114799;  QO: True; CD: TRUE; LR: True), //
//8.6.05 kast        (DisplayName:'CDW';           Field:'CDrumWrap';        KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 8124799; QO: True; CD: TRUE; LR: True), //
    (DisplayName:'LckP';          Field:'LckP';             KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8054799; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckClL';        Field:'LckClL';           KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8064799; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckClT';        Field:'LckClT';           KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8074799; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckSCnt';       Field:'LckShortOffCnt';   KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8084799; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'LckNSLT';       Field:'LckNSLT';          KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8094799; QO: False; CD: TRUE; LR: False), //

//Nue:11.02.08  ???
    (DisplayName:'LckVCV';       Field:'LckVCV';  KeyField:False; MassField:mfNone; DataType: ftFloat; CalcMode: -1; Color: 8104799; QO: False; CD: TRUE; LR: False), //
    (DisplayName:'CVd';          Field:'CVd';     KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: -2; UseMode: umNever; Color: 16580115; ClassTypeGroup: ctLabData; QO: True; LR: True), //
    (DisplayName:'CVCV';         Field:'CVCV';    KeyField:False; MassField:mfLen; DataType: ftFloat; CalcMode: 0;  Color: 1451435; CD: TRUE; LR: True), //

    // this field are always required
    (DisplayName:'';   Field:'Len';           KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'SFICnt';        KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'AdjustBase';    KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'rtSpd';         KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'wtSpd';         KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'AdjustBaseCnt'; KeyField:False; DataType: ftFloat; CalcMode: -1; UseMode: umAlways; QO: True; CD: False; LR: True),
    (DisplayName:'';   Field:'prod_id';       KeyField:True;  DataType: ftInteger; CalcMode: -1; UseMode: umNormal; LR: True)
  );

//  cYScalingStr : array[TYScalingType] of string[10] = ('1x','2x','5x','10x','50x','100x','500x','1000x','1/2x','1/10x','1/20x','1/100x','1/200x','1/1000x','1/2000x');
//------------------------------------------------------------------------------
(**
  // Default Feldfarben
  cDefaultColor : Array[0..9] of TDefaultColorRec = (
    // imperfection

    (Fieldname : 'calc_LckCl';              Color: 16570115  ),
     // Offlimit
    (Fieldname : 'calc_OfflineRatio';       Color: 16744576),
    (Fieldname : 'c_offline_offlimit_time'; Color:  8421376),
    (Fieldname : 'c_YB_offline_time';       Color:  12615680),
    (Fieldname : 'c_Eff_offline_time';      Color:  8421631),
    (Fieldname : 'c_CSp_offline_time';      Color:  16711680  ),
    (Fieldname : 'c_RSp_offline_time';      Color:  32896  ),
    (Fieldname : 'c_CBu_offline_time';      Color:  12615935  ),
    (Fieldname : 'c_CUpY_offline_time';     Color:  65535  ),
    (Fieldname : 'c_tLSt_offline_time';     Color:  16744576  ),
    (Fieldname : 'c_LSt_offline_time';      Color:  8453888  ),
    (Fieldname : 'c_CSIRO_offline_time';    Color:  128  ),
    (Fieldname : 'c_CYTot_offline_time';    Color:  16776960  ),

   // Machine
    (Fieldname : 'calc_npProdGrp';          Color: 16711680  ),
    (Fieldname : 'calc_MEff';               Color:  65535  ),
    (Fieldname : 'c_Bob';                   Color:  33023  ),
    (Fieldname : 'c_Cones';                 Color:  8421376  ),



    (Fieldname : 'calc_UCLS';               Color: 9623533  ),
    (Fieldname : 'calc_UCLL';               Color: 2023734  ),
    (Fieldname : 'calc_UCLT';               Color: 2872763  ),
    (Fieldname : 'calc_CDBU';               Color: 15513591  ),
    (Fieldname : 'c_tRed';                  Color: 16745665  ),
    (Fieldname : 'c_tYell';                 Color: 13676240  ),
    (Fieldname : 'c_tManSt';                Color: 261291  ),
    (Fieldname : 'calc_LckCl';              Color: 16570115  ),
{}
 );
(**)
//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units

end.

