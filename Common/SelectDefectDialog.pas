(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SelectDefectDialog.pas
| Projectpart...: Dialog zur Auswahl ob Schnitte, Ungeschnittene oder Beide gewünscht sind
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| Juni 2003  1.00 Wss | File created
| 18.07.2003 1.02 Wss | Property DefectType hinzugefügt
| 16.01.2004      Lok | Property 'DisabledClassDefects' hinzugefügt
|                     |   In der Methode 'DoShow' werden die gewünschten Classdefects disabled
| 20.09.2005      Wss | Als Default den letzten 'Enabled' Radiobutton selektieren (Beide -> Uncut -> Cut)
|=============================================================================*)
unit SelectDefectDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, IvDictio, IvMulti, IvEMulti, mmTranslator, StdCtrls,
  ExtCtrls, mmRadioGroup, mmButton, ClassDef;

type
  TfrmSelectDefects = class(TDialogBottom)
    rgDefectType: TmmRadioGroup;
    mTranslator: TmmTranslator;
  private
    fTitleInfo: String;
    FDisabledClassDefects: TClassDefectsSet;
    procedure SetTitleInfo(const Value: String);
    function GetDefectType: TClassDefects;
  protected
    procedure DoShow; override;
  public
    constructor Create(aOwner: TComponent); override;
    property DefectType: TClassDefects read GetDefectType;
    property TitleInfo: String read fTitleInfo write SetTitleInfo;
    property DisabledClassDefects: TClassDefectsSet read FDisabledClassDefects write FDisabledClassDefects;
  end;

var
  frmSelectDefects: TfrmSelectDefects;

implementation
{$R *.DFM}

const
  cDefectTypeCut   = 0;
  cDefectTypeUncut = 1;
  cDefectTypeBoth  = 2;

resourcestring
  rsFormCaption     = '(*)Klassierdaten'; // ivlm

//------------------------------------------------------------------------------
// TfrmSelectDefects
//------------------------------------------------------------------------------
constructor TfrmSelectDefects.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fTitleInfo := '';
end;
//------------------------------------------------------------------------------
procedure TfrmSelectDefects.DoShow;
var
  xClassDefect: TClassDefects;
  xFound: boolean;
begin
  inherited DoShow;

  // Die gewünschten RadioButtons auf disabled stellen
  for xClassDefect := Low(TClassDefects) to High(TClassDefects) do begin
    if rgDefectType.Components[ord(xClassDefect)] is TWinControl then
      TWinControl(rgDefectType.Components[ord(xClassDefect)]).Enabled := not((xClassDefect in DisabledClassDefects));
  end;// for xClassDefect := Low(TClassDefects) to High(TClassDefects) do begin

  // Als Default den letzten 'enabled' Radiobutton selektieren (Beide -> Uncut -> Cut)
  xClassDefect := High(TClassDefects);
  xFound := false;
  while not(xFound) and (xClassDefect >= Low(TClassDefects)) do begin
    if rgDefectType.Components[ord(xClassDefect)] is TWinControl then begin
      if TWinControl(rgDefectType.Components[ord(xClassDefect)]).Enabled then begin
        xFound := true;
        rgDefectType.ItemIndex := ord(xClassDefect);
      end;// if TWinControl(rgDefectType.Components[ord(xClassDefect)]).Enabled then begin
    end;// if rgDefectType.Components[ord(xClassDefect)] is TWinControl then begin
    dec(xClassDefect);
  end;// while not(xFound) and (xClassDefect <= High(TClassDefects)) do begin
end;

//------------------------------------------------------------------------------
function TfrmSelectDefects.GetDefectType: TClassDefects;
begin
  Result := TClassDefects(rgDefectType.ItemIndex);
end;

procedure TfrmSelectDefects.SetTitleInfo(const Value: String);
begin
  fTitleInfo := Value;
  Self.Caption := Format('%s: %s', [rsFormCaption, Value]);
end;

end.
