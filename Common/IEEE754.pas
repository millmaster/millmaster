//  IEEE-754 Support for NANs and INFs.
//  Copyright (C) 1997-1998, Earl F. Glynn, Overland Park, KS  USA.
//  All Rights Reserved.
//
//  Information about IEEE Arithmetic was taken from
//  "Numerical Computation Guide," Sun Microsystems, 1990.

unit IEEE754;

interface

{$J-} // Do not allow constants to be changed

{$IFDEF VER120} // Delphi 4
const
  NANSignalingBits: Int64 = $7FF0000000000001;
var
    // With $J- directive above and Absolute reference below, this
    // value cannot be changed
  NANSignaling: DOUBLE absolute NANSignalingBits;
{$ELSE} // Delphi 1-3
const
  NANSignalingBits: array[1..2] of LongInt = ($00000001, $7FF00000);
var
  NANSignaling: DOUBLE absolute NANSignalingBits;
{$ENDIF}

function NAN: DOUBLE; // The "Quiet" Nan

function PositiveInfinity: DOUBLE; //  INF
function NegativeInfinity: DOUBLE; // -INF

  // "Is" functions
function IsNAN(const d: DOUBLE): BOOLEAN;
function IsInfinity(const d: DOUBLE): BOOLEAN;

  // Hex String Conversions
function DoubleToHex(const d: DOUBLE): string;
function HexToDouble(const hex: string): DOUBLE;

implementation

uses
  Windows, // DWORD
  SysUtils; // IntToHex

type
  EIEEEMath = class(Exception);

{$IFDEF VER120} // Delphi 4
  // With Int64s, the logical order of the floating point values isn't
  // obfuscated by the "little endian" physical order
const
  NANQuietBits: Int64 = $7FFFFFFFFFFFFFFF;
  PositiveInfinityBits: Int64 = $7FF0000000000000;
  NegativeInfinityBits: Int64 = $FFF0000000000000;

var
  dNANQuiet: DOUBLE absolute NANQuietBits;
  dPositiveInfinity: DOUBLE absolute PositiveInfinityBits;
  dNegativeInfinity: DOUBLE absolute NegativeInfinityBits;

  // Since a NAN is not a single, unique value, a special function is needed
  // for this test
function IsNAN(const d: DOUBLE): BOOLEAN;
var
  Overlay: Int64 absolute d;
begin
  Result := ((Overlay and $7FF0000000000000) = $7FF0000000000000) and
    ((Overlay and $000FFFFFFFFFFFFF) <> $0000000000000000)
end {IsNAN};

function IsInfinity(const d: DOUBLE): BOOLEAN;
var
  Overlay: Int64 absolute d;
begin
  Result := (Overlay and $7FF0000000000000) = $7FF0000000000000
end {IsInfinity};

//  The following is a tempting "improvement" in Delphi 4, BUT
//  there no longer is zero padding on the left.  So stick with old
//  way for now.
//  FUNCTION DoubleToHex(CONST d:  DOUBLE):  STRING;
//    VAR
//      Overlay:  Int64 ABSOLUTE d;
//  BEGIN
//    RESULT := IntToHex(Overlay, 16)
//  END {DoubleToHex};

//  The following is a tempting "improvement" in Delphi 4, BUT
//  the constant for -0, $8000000000000000 generates an error.
//
//  FUNCTION HexToDouble(CONST hex:  STRING):  DOUBLE;
//    VAR
//      i:  Int64;
//      d:  Double absolute i;
//  BEGIN
//    i := StrToInt('$' + hex);
//    RESULT := d;
//  END {HexToDouble};

{$ELSE} // Delphi 1-3

  // The order of integers is "reversed" because of "Little Endian" order.
  // With "Little Endian," the 2nd array element in physical order is the
  // 1st array element in "logical" order.
const // Use DWORD here for D3-D5 compatibility
  NANQuietBits: array[1..2] of DWORD = ($FFFFFFFF, $7FFFFFFF);
  PositiveInfinityBits: array[1..2] of DWORD = ($00000000, $7FF00000);
  NegativeInfinityBits: array[1..2] of DWORD = ($00000000, $FFF00000);

var
    // NEVER MODIFY THESE VARIABLES
    // "Absolute" cannot be used in CONST defintions.

  dNANQuiet: DOUBLE absolute NANQuietBits;
  dPositiveInfinity: DOUBLE absolute PositiveInfinityBits;
  dNegativeInfinity: DOUBLE absolute NegativeInfinityBits;

  // Since a NAN is not a single, unique value, a special function is needed
  // for this test
function IsNAN(const d: DOUBLE): BOOLEAN;
var
  Overlay: array[1..4] of WORD absolute d;
begin
  Result := ((Overlay[4] and $7FF0) = $7FF0) and
    ((Overlay[1] <> 0) or (Overlay[2] <> 0) or (Overlay[3] <> 0))
end {IsNAN};

function IsInfinity(const d: DOUBLE): BOOLEAN;
var
  Overlay: array[1..2] of LongInt absolute d;
begin
  Result := ((Overlay[2] and $7FF00000) = $7FF00000) and
    (Overlay[1] = 0)
end {IsInfinity};

{$ENDIF}

function DoubleToHex(const d: DOUBLE): string;
var
  Overlay: array[1..2] of LongInt absolute d;
begin
    // Look at element 2 before element 1 because of "Little Endian" order.
  Result := IntToHex(Overlay[2], 8) + IntToHex(Overlay[1], 8);
end {DoubleToHex};

function HexToDouble(const hex: string): DOUBLE;
var
  d: DOUBLE;
  Overlay: array[1..2] of LongInt absolute d;
begin
  if LENGTH(hex) <> 16 then raise EIEEEMath.Create('Invalid hex string for HexToDouble');

  Overlay[1] := StrToInt('$' + COPY(hex, 9, 8));
  Overlay[2] := StrToInt('$' + Copy(hex, 1, 8));

  Result := d
end {HexToDouble};

  // Use functions to make sure values can never be changed.
function NAN: DOUBLE;
begin
  Result := dNANQuiet
end {NAN};

function PositiveInfinity: DOUBLE;
begin
  Result := dPositiveInfinity
end {NAN};

function NegativeInfinity: DOUBLE;
begin
  Result := dNegativeInfinity
end {NAN};

end.

