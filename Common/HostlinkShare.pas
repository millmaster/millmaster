{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMLinkSharer.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: DLL for Floor
| Process(es)...: -
| Description...: Provides interface for sharing machine information with FloorPlugin
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  0.00  Wss | File created
| 01.03.2000  1.01  Wss | Error message extended with additional text (unknown error at Streiff)
| 19.06.2000  1.01  Wss | Curr/Last month added, registry update completed
| 29.06.2000  1.01  Wss | constructor Connect added for connect only -> Create changed to create memory only
| 05.07.2000  1.01  Wss | Save format of CustShiftFrom/To changed to save as float instead of string (regional settings)
| 23.08.2000  1.00  Wss | ShowEmptyProdGroup implemented
| 04.02.2001  2.01  Wss | StyleColor, ShowStyleColor implemented
| 26.04.2001  2.01  Wss | GrpData.gdStyleName implemented
| 16.10.2002        Wss | Umbau ADO
| 24.10.2001  2.01  Wss | Benutzeroptionen nach CurrentUser in Registry verschoben
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 20.01.2004  1.00  Wss | QOfflimit Parameter entfernt
| 29.09.2005  1.02  Wss | Compiler Hint/Warnings bearbeitet
|=========================================================================================*}
unit HostlinkShare;

interface
uses
  Classes, Graphics, SysUtils, Windows, LoepfeGlobal, BaseGlobal, HostlinkDef;

{$IFDEF MMLinkDummy}
const
//  cProdGroupPalette: Array[1..cMaxLotCount] of TColor = (  // use for MMLinkDummy
//    clRed,
//    clBlue,
//    clGreen,
//    clYellow,
//    clPurple,
//    clLime,
//    clRed,
//    clBlue,
//    clGreen,
//    clYellow,
//    clPurple,
//    clLime
//  );
  cProdGroupPalette: Array[1..cMaxLotCount] of TColor = (  // use for MMLinkDummy
    clRed,
    clBlue,
    clGreen,
    clYellow,
    clPurple,
    clLime,
    clRed,
    clBlue,
    clGreen,
    clYellow,
    clPurple,
    clLime,
    clRed,             //Einf�gen von 13-30 Nue:11.10.06
    clBlue,
    clGreen,
    clYellow,
    clPurple,
    clLime,
    clRed,
    clBlue,
    clGreen,
    clYellow,
    clPurple,
    clLime,
    clRed,
    clBlue,
    clGreen,
    clYellow,
    clPurple,
    clLime
  );
{$ENDIF}

//------------------------------------------------------------------------------
type
{$MINENUMSIZE 2}
  TProdGroupState = (pgsUndefined, pgsNormal, pgsNotStarted, pgsInconsistent,
                     pgsStopped, pgsCollecting, pgsStopping);
{$MINENUMSIZE 1}
type
  PProdGrpRec = ^TProdGrpRec;
  TProdGrpRec = record
    ProdID:       LongInt;
    ProdStart:    TDateTime;
    GrpColor:     TColor;
    StyleColor:   TColor;
    SpdFirst:     Word;
    SpdLast:      Word;
    StartMode:    Word;
    GrpName:      String[70];
    StyleName:    String[50];
  end;

  PMachineRec = ^TMachineRec;
  TMachineRec = record
    MachID:       Word;
    Typ:          TMachineType;
    HeadLeft:     Boolean;
    State:        TMachState;
    NrOfSpindles: Word;
    NrOfProdGrp:  Integer;
    Name:         String[10];
    ProdGrp:      Array[1..cMaxLotCount] of TProdGrpRec
  end;

  PShiftSelectionRec = ^TShiftSelectionRec;
  TShiftSelectionRec = record
    TimeFrom: TDateTime;
    TimeTo:   TDateTime;
  end;

  TChangeMode = (cmCustShift, cmQOfflimit, cmMaOfflimit);
  TChangeModeSet = set of TChangeMode;

type
  PHostlinkConfigRec = ^THostlinkConfigRec;
  THostlinkConfigRec = record
    DefaultShiftCalID: Integer;
    CustShiftCalID:    Integer;
    DefaultTrendSelection: TDataSelection;
    OfflimitTime: Integer;
    ShiftSelection: Array[dsCurShift..dsIntervalData] of TShiftSelectionRec;
    ChangeMode: TChangeModeSet;
    CustShiftDetail: Boolean;
    IntervalTimeRange: Integer;
    ShowEmptyProdGroup: Boolean;
    ShowStyleColor:     Boolean;
    ShowSystemQO:       Boolean;
    IsStandard: Boolean;
    IsPro: Boolean;
    NumberOfMachines: Word;                // 2 bytes
    Machine: Array[0..0] of TMachineRec;
  end;

//------------------------------------------------------------------------------
type
  THostlinkShare = class(TObject)
  private
    fMemory: PHostlinkConfigRec;
    function GetBooleanValue(aIndex: Integer): Boolean;
    function GetChangeMode: TChangeModeSet;
    function GetDefaultTrendSelecton: TDataSelection;
    function GetIntegerValue(aIndex: Integer): Integer;
    function GetMachRec(aIndex: Word): PMachineRec;
    function GetNrOfMachines: Word;
    function GetShiftSelection(aIndex: TDataSelection): PShiftSelectionRec;
    procedure SetBooleanValue(aIndex: Integer; const Value: Boolean);
    procedure SetChangeMode(const Value: TChangeModeSet);
    procedure SetDefaultTrendSelection(const Value: TDataSelection);
    procedure SetIntegerValue(aIndex: Integer; aValue: Integer);
  protected
    mSHHandle: THandle;
  public
    property ChangeMode: TChangeModeSet read GetChangeMode write SetChangeMode;
    property CustShiftDetail: Boolean    index 1 read GetBooleanValue write SetBooleanValue;
    property ShowEmptyProdGroup: Boolean index 2 read GetBooleanValue write SetBooleanValue;
    property IsStandard: Boolean         index 3 read GetBooleanValue;
    property IsPro: Boolean              index 4 read GetBooleanValue;
    property ShowStyleColor: Boolean     index 5 read GetBooleanValue write SetBooleanValue;

    // common Get/SetIntegerValue
    property IntervalTimeRange: Integer index 0 read GetIntegerValue write SetIntegerValue;
    property OfflimitTime: Integer      index 1 read GetIntegerValue write SetIntegerValue;
    property CustShiftCalID: Integer    index 2 read GetIntegerValue write SetIntegerValue;
    property DefaultShiftCalID: Integer index 3 read GetIntegerValue write SetIntegerValue;
    // all other properties
    property DefaultTrendSelection: TDataSelection read GetDefaultTrendSelecton write SetDefaultTrendSelection;
    property ShiftSelection[aIndex: TDataSelection]: PShiftSelectionRec read GetShiftSelection;
    property MachRec[aIndex: Word]: PMachineRec read GetMachRec;
    property NrOfMachines: Word read GetNrOfmachines;

    procedure ClearChangeMode(aMode: TChangeMode);
    function CheckChangeMode(aMode: TChangeMode): Boolean;
    constructor Create(aNrOfMachines: Word); virtual;
    constructor Connect;
    destructor Destroy; override;
    procedure UpdateRegistry;
  published
  end;
  //----------------------------------------------------------------------------

var
  gMachConfig: THostlinkShare;

implementation    // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  SettingsReader, mmEventLog, NTApis, mmRegistry;
//------------------------------------------------------------------------------
const
  cSHMMLinkName = 'MMLink-Plugin-Share';

//******************************************************************************
// TMMLinkShareServer
//******************************************************************************
procedure THostlinkShare.ClearChangeMode(aMode: TChangeMode);
begin
  fMemory^.ChangeMode := fMemory^.ChangeMode - [aMode];
end;
//------------------------------------------------------------------------------
function THostlinkShare.CheckChangeMode(aMode: TChangeMode): Boolean;
begin
  Result := aMode in fMemory^.ChangeMode;
end;
//------------------------------------------------------------------------------
constructor THostlinkShare.Connect;
var
  xStr: String;
begin
  inherited Create;
  fMemory := Nil;
  mSHHandle := INVALID_HANDLE_VALUE;
  try
    // Client side: only connect to shared memory
    xStr := 'Connecting: Error connecting shared memory: %s';
    CodeSite.SendString('OpenFileMapping', cSHMMLinkName);
    mSHHandle := OpenFileMapping(FILE_MAP_WRITE, False, PChar(cSHMMLinkName));
    if mSHHandle = 0 then begin
      xStr := 'Handle is NULL -> Abort with: ' + xStr;
      Abort;
    end;

    // We need to map a pointer to the handle of the shared memory region
    CodeSite.SendFmtMsg('MapViewOfFile: Handle = %d', [mSHHandle]);
    fMemory := MapViewOfFile(mSHHandle, FILE_MAP_WRITE, 0, 0, 0);
    CodeSite.SendFmtMsg('Connect fMemory = %.8x', [Integer(fMemory)]);
    if fMemory = nil then begin
      xStr := 'MapViewOfFile failed -> Abort with: ' + xStr;
      Abort;
    end;

  except
    on e:Exception do begin
      xStr := Format(xStr, [e.Message]);
      WriteToEventLog(xStr, 'THostlinkShare: ', etError);
    end;
//    raise Exception.Create(xStr);
  end;
end;
//------------------------------------------------------------------------------
constructor THostlinkShare.Create(aNrOfMachines: Word);
var
  xSize: DWord;
  xStr: String;
  xUserGroup: String;
  xSettings: TMMSettingsReader;
  xDateTime: TDateTime;
  xSel: TDataSelection;
  xExist: Boolean;
begin
  inherited Create;
  fMemory := Nil;
  mSHHandle := INVALID_HANDLE_VALUE;
  try
    // if AutoPrintService is running SharedMemory already exists -> check for it
    xStr := 'Creating: Error connecting shared memory: %s';
    mSHHandle := OpenFileMapping(FILE_MAP_WRITE, False, PChar(cSHMMLinkName));
    xExist := (mSHHandle <> 0);
    if not xExist then begin
      CodeSite.SendString('not xExist', GetLastErrorText);
      // SharedMemory doesn't exist -> create
      CodeSite.SendInteger('Number of machines', aNrOfMachines);
      xStr := 'Creating: Error creating shared memory for machines: %s';
      xSize := sizeof(THostlinkConfigRec) + (aNrOfMachines * sizeof(TMachineRec));
      mSHHandle := CreateFileMapping($FFFFFFFF, nil, PAGE_READWRITE, 0, xSize, PChar(cSHMMLinkName));
    end;
    if mSHHandle = 0 then begin
      xStr := 'Handle is NULL -> Abort with: ' + xStr;
      Abort;
    end;

    // We need to map a pointer to the handle of the shared memory region
    CodeSite.SendFmtMsg('MapViewOfFile: Handle = %d', [mSHHandle]);
    fMemory := MapViewOfFile(mSHHandle, FILE_MAP_WRITE, 0, 0, 0);
    CodeSite.SendFmtMsg('Create fMemory = %.8x', [Integer(fMemory)]);
    if fMemory = nil then begin
      xStr := 'MapViewOfFile failed -> Abort with: ' + xStr;
      Abort;
    end;

    // if this is the initialize call do some initializations
    if not xExist then begin
{$IFDEF MMLinkDummy}
      xUserGroup := ''; //'Benutzer';
      xStr := 'Initialize with TMMSettingsReader failed -> Exception with: ' + xStr;
      try
        with fMemory^ do begin
          ChangeMode := [cmCustShift, cmQOfflimit];
          DefaultShiftCalID     := 1;
          CustShiftCalID        := 1;
          DefaultTrendSelection := dsIntervalData;
          OfflimitTime          := 60;

          xDateTime := Now;
          for xSel:=dsCurShift to dsIntervalData do begin
            ShiftSelection[xSel].TimeTo   := xDateTime;
            ShiftSelection[xSel].TimeFrom := xDateTime - 1;
          end;

          CustShiftDetail       := True;
          IntervalTimeRange     := 8;
          ShowEmptyProdGroup    := True;
          NumberOfMachines      := aNrOfMachines;
        end;
      finally
      end;
{$ELSE}
      xSettings := TMMSettingsReader.Instance;
      try
        if xSettings.Init then begin
          xUserGroup := xSettings.Value[cUserGroup];
          // enable access to this shared memory for all users in this group
          CodeSite.SendString('MMLinkShare: xUserGroup', xUserGroup);
          if xUserGroup <> '' then
            if not SetKernelObjectSecurity(mSHHandle, PChar(xUserGroup), True) then begin
              xStr := 'SetKernelObjectSecurity failed for group ' + xUserGroup + '-> Abort with: %s';
              Abort;
            end;

          with fMemory^ do begin
            ChangeMode := [cmCustShift, cmQOfflimit, cmMaOfflimit];
            NumberOfMachines   := aNrOfMachines;

            DefaultShiftCalID     := xSettings.Value[cDefaultShiftCalID];
            IsStandard            := xSettings.IsPackageStandard;
            IsPro                 := xSettings.IsPackagePro;
            OfflimitTime          := xSettings.Value[cOfflimitTime];
            DefaultTrendSelection := TDataSelection({Integer}(xSettings.Value[cFloorTrendSelection]) + 1);

            xDateTime := Now;
            // Array der Zeitbereiche erst mal mit Standardwerten abf�llen
            for xSel:=dsCurShift to dsIntervalData do begin
              ShiftSelection[xSel].TimeTo   := xDateTime;
              ShiftSelection[xSel].TimeFrom := xDateTime - 1;
            end;
            IntervalTimeRange     := xSettings.Value[cIntervalTimeRange];
            with TmmRegistry.Create do
            try
              RootKey := cRegCU;
              if OpenKey(cRegMMFloor, True) then begin
                CustShiftCalID     := StrToIntDef(ReadString(cCustCalID), DefaultShiftCalID);
                // Kundenzeitbereich von Registry �berschreiben
                with ShiftSelection[dsCustShiftSel] do begin
                  if ValueExists(cCustShiftFrom) then TimeFrom := ReadFloat(cCustShiftFrom)
                                                 else TimeFrom := xDateTime-1;
                  if ValueExists(cCustShiftTo)   then TimeTo   := ReadFloat(cCustShiftTo)
                                                 else TimeTo   := xDateTime;
                  if (TimeFrom < cDateTime1970) or (TimeTo < cDateTime1970) then begin
                    TimeFrom := xDateTime-1;
                    TimeTo   := xDateTime;
                  end;
                end;
                CustShiftDetail       := Boolean(StrToIntDef(ReadString(cCustShiftDetail), 0));
                ShowEmptyProdGroup    := Boolean(StrToIntDef(ReadString(cShowEmptyProdGroup), 1));
                ShowStyleColor        := Boolean(StrToIntDef(ReadString(cShowStyleColor), 0));
                // Werden �ber SettingsReader gelesen
                //OfflimitTime          := StrToIntDef(ReadString(cOfflimitTime), OfflimitTime);
                //DefaultTrendSelection := StrToIntDef(ReadString(cFloorTrendSelection), DefaultTrendSelection);
              end;
            finally
              Free;
            end; // TmmRegistry try
          end; // with fMemory
        end; // if Init
      finally
        xStr := Format('Getting system defaults for [%s]: ', [xSettings.LastRequestedValue]) + xStr;
      end; // try
{$ENDIF}
    end; // if not xExist
  except
    on e:Exception do begin
      xStr := Format(xStr, [e.Message]);
      WriteToEventLog(xStr, 'THostlinkShare: ', etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
destructor THostlinkShare.Destroy;
begin
  // unmap pointer to shared memory
  if Assigned(fMemory) then
    UnmapViewOfFile(fMemory);

  // close handle of shared memory
  if mSHHandle <> 0 then
    CloseHandle(mSHHandle);

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetBooleanValue(aIndex: Integer): Boolean;
begin
  case aIndex of
    1: Result := fMemory^.CustShiftDetail;
    2: Result := fMemory^.ShowEmptyProdGroup;

    3: Result := fMemory^.IsStandard;
    4: Result := fMemory^.IsPro;

    5: Result := fMemory^.ShowStyleColor;
  else
    Result := False;
  end;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetChangeMode: TChangeModeSet;
begin
  Result := fMemory^.ChangeMode;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetDefaultTrendSelecton: TDataSelection;
begin
  Result := fMemory^.DefaultTrendSelection;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetIntegerValue(aIndex: Integer): Integer;
begin
  case aIndex of
    0: Result := fMemory^.IntervalTimeRange;
    1: Result := fMemory^.OfflimitTime;
    2: Result := fMemory^.CustShiftCalID;
    3: Result := fMemory^.DefaultShiftCalID;
  else
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetMachRec(aIndex: Word): PMachineRec;
begin
  with fMemory^ do
    if aIndex < NumberOfMachines then
      Result := @Machine[aIndex]  // starting index: Floor = 1, Memory = 0
    else
      raise Exception.CreateFmt('Machine index %d out of range (%d)', [aIndex, NumberOfMachines]);
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetNrOfMachines: Word;
begin
  if Assigned(fMemory) then
    Result := fMemory^.NumberOfMachines
  else
    Result := $FFFF;
end;
//------------------------------------------------------------------------------
function THostlinkShare.GetShiftSelection(aIndex: TDataSelection): PShiftSelectionRec;
begin
  if not(aIndex in [dsCurShift..dsIntervalData]) then
    aIndex := dsCurShift;

  Result := @fMemory^.ShiftSelection[aIndex];
end;
//------------------------------------------------------------------------------
procedure THostlinkShare.SetBooleanValue(aIndex: Integer; const Value: Boolean);
begin
  case aIndex of
    1: fMemory^.CustShiftDetail    := Value;
    2: fMemory^.ShowEmptyProdGroup := Value;

    5: fMemory^.ShowStyleColor     := Value;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure THostlinkShare.SetChangeMode(const Value: TChangeModeSet);
begin
  fMemory^.ChangeMode := Value;
end;
//------------------------------------------------------------------------------
procedure THostlinkShare.SetDefaultTrendSelection(const Value: TDataSelection);
begin
  fMemory^.DefaultTrendSelection := Value;
end;
//------------------------------------------------------------------------------
procedure THostlinkShare.SetIntegerValue(aIndex: Integer; aValue: Integer);
begin
  case aIndex of
    0: fMemory^.IntervalTimeRange := aValue;
    1: fMemory^.OfflimitTime      := aValue;
    2: fMemory^.CustShiftCalID    := aValue;
    3: fMemory^.DefaultShiftCalID := aValue;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure THostlinkShare.UpdateRegistry;
begin
  with TmmRegistry.Create do
  try
    RootKey := cRegCU;
    if OpenKey(cRegMMFloor, True) then begin
      WriteString(cCustCalID,           IntToStr(fMemory^.CustShiftCalID));
      WriteFloat(cCustShiftFrom,        fMemory^.ShiftSelection[dsCustShiftSel].TimeFrom);
      WriteFloat(cCustShiftTo,          fMemory^.ShiftSelection[dsCustShiftSel].TimeTo);
      WriteString(cCustShiftDetail,     IntToStr(Integer(fMemory^.CustShiftDetail)));
      WriteString(cShowEmptyProdGroup,  IntToStr(Integer(fMemory^.ShowEmptyProdGroup)));
      WriteString(cShowStyleColor,      IntToStr(Integer(fMemory^.ShowStyleColor)));
      // Dieser Werte werden �ber System Configuration gesetzt
//      WriteString(cFloorTrendSelection, IntToStr(Ord(fMemory^.DefaultTrendSelection)-1));
//      WriteString(cOfflimitTime,        IntToStr(fMemory^.OfflimitTime));
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------

initialization
  gMachConfig := Nil;
finalization
  gMachConfig.Free;
end.

