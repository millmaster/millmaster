(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SaveTemplateDLG.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: - Der Dialog wird im MMClearerAssistant, MMStyle, Assignments,
|                   verwendet
|                 - Dieser Dialog stammt aus dem Dialog TSaveAsTemplate
| Develop.system: W2K
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.11.2003  1.00  SDo  | Datei erstellt
| 15.01.2003  1.01 SDoNue| Init call nicht mehr n�tig (-> Prepare), Prepare Aufruf neu in OnShow
| 15.01.2004  1.01  Wss  | Hilfekontextaufruf implementiert
| 19.01.2004  1.01  Nue  | Tag unterscheiden: Modus Button sollten 1 haben; alle anderen SpeedButtons sollten 0 haben
| 22.01.2004  1.01  SDO  | EditBox Enter-Beep unterdr�ckt
| 03.02.2004  1.02  SDO  | Error behoben, wenn keine YMSetId gesetzt ist.
|                        | -> Ist keine YMSetId gesetzt, dann neues Template ohne Artikelzuweisung
| 18.01.2005  2.00  Nue  | Umbau auf XML V.5.0.
| 15.04.2005  2.00  SDo  | F�r CLA: Standard Buttons gewechselt in Abhaengikeit von mLayoutAsStyleAdministrator (gedrueckt) -> Func. Prepare()
| 06.09.2005  2.01  Nue  | Anpassen SetName-Handling (-X). Beschr�nken TemplateName auf 50 Zeichen.
|                        | MaxValue-Property von edTemplateName und edTemplateName_1 auf 50 gesetzt.
| 22.04.2009  2.02  Nue  | Check existing YM-Name und TK beim Update.
|=============================================================================*)
unit SaveTemplateDLG;     

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, mmLabel, Mask,
  mmMaskEdit, mmColorButton, IvDictio, IvMulti, IvEMulti,
  mmTranslator, MMUGlobal,
  YMParaDef, mmButton, mmAdoDataset, mmListBox, mmRadioButton, ExtCtrls,
  mmPanel, mmComboBox, mmSpeedButton, mmEdit, mmScrollBox,
  AdoDBAccess, {YMParaDBAccess,} XMLDef;

resourcestring


  //Msg Texte
  rsStyleIsEmptyMsg = '(*)Bitte waehlen Sie einen oder mehrere Artikel aus.'; //ivlm
  rsTemplateNameIsEmptyMsg = '(*)Bitte geben Sie einen Reinigervorlagennamen ein.'; //ivlm
  rsTemplateNameAlreadyAvailableMsg = '(*)Der Name existiert bereits, bitte einen anderen Namen angeben.'; //ivlm
  rsExistsTemplateMsg = '(*)Die Reinigervorlage %s existiert bereits.'; //ivlm
  rsOverrideTemplateMsg = '(*)Wollen Sie diese Reinigervorlage ueberschreiben?'; //ivlm
  rsSaveQuestion = '(*)Wollen Sie die Reinigervorlage ''%s'' aendern?';//ivlm
  rsSaveTemplateQuestion = '(*)Wollen Sie die Reinigervorlage ''%s'' speichern?';//ivlm

  rsSaveQuestionNewTemplate = '(*)Wollen Sie die neue Reinigervorlage ''%s'' speichern?';//ivlm
  rsRenameTemplateMsg = '(*)Bitte geben Sie einen anderen Reinigervorlagennamen ein.'; //ivlm

  rsSaveNotAllowedMsg = '(*)Die Vorlage %s kann nicht gespeichert werden! Es besteht bereits eine Vorlage mit gleichem Namen und gleicher Tastkopf-Klasse.'; //ivlm



  //Hinweis Texte
  rsRemarkOverrideTemplate = '(*)Allen aufgelisteten Artikeln wird die geaenderte Reinigervorlage zugewiesen. Diese Reinigervorlage ist ab dem Speicherdatum gueltig. Die gezeigten Artikel werden bei der naechsten Zuordnung mit der neuen Einstellung produziert.';//ivlm
  rsRemarkOverrideTemplate_TemplateView = '(*)Reinigervorlage speichern.';//ivlm

  rsRemarkNewTemplate0 = '(*)Waehlen Sie die Artikel aus, welche Sie der geaenderten Reinigervorlage zuweisen wollen und geben Sie dieser Vorlage einen neuen Namen.';//ivlm
  rsRemarkNewTemplate1 = '(*)Die Artikel werden bei der naechsten Zuordnung mit der neuen Einstellung produziert.';//ivlm

  rsRemarkTemplateOnly = '(*)Reinigervorlage speichern.';//ivlm

  rsRemarkRenameTemplate = '(*)Die Reinigervorlage wird umbenannt. Die Artikel werden bei der naechsten Zuordnung mit dem neuen Reinigervorlagennamen produziert.';//ivlm
  rsRemarkRenameTemplate_TemplateView = '(*)Die Reinigervorlage wird umbenannt.';  //ivlm



  //Button Texte
  rsSBText_TemplateOnly_TemplateView = '(*)Neue Reinigervorlage';  //ivlm
  rsSBText_TemplateOnly = '(*)Neue Reinigervorlage ohne Artikelzuweisung'; //ivlm

  rsSBText_NewTemplate  ='(*)Neue Reinigervorlage mit Artikelzuweisung'; //ivlm

  rsSBText_OverrideTemplate = '(*)Bestehende Reinigervorlage mit Artikelzuweisung'; //ivlm
  rsSBText_OverrideTemplate_TemplateView = '(*)Bestehende Reinigervorlage'; //ivlm

  rsSBText_RenameTemplate = '(*)Reinigervorlage %s umbenennen'; //ivlm

type

  TTemplateType = (ttNewTemplateWithAssignToStyles,       //Neue Reinigervorlage mit Artikelzuweisung
                   ttModifySettingsToAllAssignedStyles,   //Bestehende Reinigervorlage mit Artikel zuweisen -> Zugewiesene Artikel werden von der Parnet-Vorlage uebernommen
                   ttModifyTemplate,                      //Bestehende Reinigervorlage ohne Artikel. Im Package ist die Vorlagenverwaltung gesetzt ( -> Not(.IsComponentStyle  = True & .IsOnlyTemplateSetsAvailable = True)
                   ttNewTemplateOnly,                     //Neue Reinigervorlage ohne Artikelzuweisung
                   ttRenameTemplateOnly,                  //Template umbenennen
                   ttActualSetIsNoTemplate,               //Mitgegebenes Reinigerset ist KEINE Reinigervorlage (Call von Assigment (ProdGrp)) ; Einziges Property, welches von aussen gesetzt wird
                   ttNone);

  TCallType = (tcClearerAssitant, tcTemplate, tcStyle, tcAssignment);



  TSaveTemplateDLGForm = class (TDialogBottom)
    bColor: TmmColorButton;
    bHelp: TmmButton;
    bMoveLeft: TmmSpeedButton;
    bMoveRight: TmmSpeedButton;
    cbActiveStyles: TCheckBox;
    edTemplateName: TmmEdit;
    edTemplateName_1: TmmEdit;
    Image1: TImage;
    laAllStyles: TmmLabel;
    laColor: TmmLabel;
    laColor_OverrideTemplate: TmmLabel;
    laFilter: TLabel;
    laNewTemplate: TmmLabel;
    laOverrideTemplate: TmmLabel;
    laRenameTemplate: TmmLabel;
    laSelectedStyles: TmmLabel;
    laStyle: TmmLabel;
    laTemplateName: TmmLabel;
    laTemplateName_1: TmmLabel;
    laTemplateOnly: TmmLabel;
    lbDest: TmmListBox;
    lbHinweis: TmmLabel;
    lbRemark: TmmLabel;
    lbSource: TmmListBox;
    lbStyles: TmmScrollBox;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmTranslator: TmmTranslator;
    mmTranslator1: TmmTranslator;
    Panel1: TPanel;
    Panel3: TPanel;
    pChangeTemplate: TmmPanel;
    pColor: TmmPanel;
    pNewTemplate: TmmPanel;
    pRemark: TPanel;
    pSavePanel: TmmPanel;
    pStyles: TmmPanel;
    sbAssignedStyles: TmmSpeedButton;
    sbNewTemplate: TmmSpeedButton;
    sbOverrideTemplate: TmmSpeedButton;
    sbRenameTemplate: TmmSpeedButton;
    sbTemplateOnly: TmmSpeedButton;
    procedure bColorChangeColor(Sender: TObject);
    procedure bHelpClick(Sender: TObject);
    procedure bMoveLeftClick(Sender: TObject);
    procedure bMoveRightClick(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure cbActiveStylesClick(Sender: TObject);
    procedure edTemplateNameChange(Sender: TObject);
    procedure edTemplateNameKeyDown(Sender: TObject; var Key: Word; Shift: 
            TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure lbDestDblClick(Sender: TObject);
    procedure lbDestKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure lbSourceDblClick(Sender: TObject);
    procedure lbSourceKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure pChangeTemplateResize(Sender: TObject);
    procedure pNewTemplateResize(Sender: TObject);
    procedure sbAssignedStylesClick(Sender: TObject);
    procedure sbOverrideTemplateClick(Sender: TObject);
  private
    FIsTemplateSet: Boolean;
    FOverrideAllow: Boolean;
    FSensingHeadClass: TSensingHeadClass;
    FSingleNameOnly: Boolean;
    FStyleIDs: string;
    FTemplateName: string;
    FTemplateType: TTemplateType;
    fYMSettingID: string;
    mActualStyles: TStringList;
    mAdoDBAccess: TAdoDBAccess;
    mCallType: TCallType;
    mColor: TColor;
    mLayoutAsStyleAdministrator: Boolean;
    mRenameTemplate: string;
    mStyles: TStringList;
    mTemplateName_From_Outside: string;
    function CheckTemplateNameAvailability(aName: string): Boolean;
    procedure ChooseSaveManner(aSender: TmmSpeedButton);
    procedure CreateInfoPanel(aValue : String);
    procedure Delete_YM_SetID_StyleID(aYMID, aStyleID: Integer);
    procedure DestroyInfoPanel;
    function GetColor: TColor;
    function GetSelected(aListBox: TCustomListBox; var aIndex: Integer): 
            Boolean;
    procedure New_YM_SetID_StyleID(aYMID, aStyleID: Integer);
    procedure Prepare;
    procedure SetColor(aColor: TColor);
    procedure SetIsTemplateSet(const Value: Boolean);
    procedure SetOverrideAllow(const Value: Boolean);
    procedure SetSettingID(const Value: string);
    procedure SetStyleIDs(const Value: string);
    procedure SetTemplateName(const Value: string);
    procedure SetTemplateType(const Value: TTemplateType);
    procedure ShowConfirmDLG;
//    procedure ShowModifyTemplateWithStyleList(aWithStyleList :Boolean);
    procedure ShowStyleList(aSQL:String);
    procedure UpdateTemplateName(aSetID: integer; aName: string);
    procedure UpdateYMSetColor(aSetID : Integer);
  public
    constructor Create(aOwner: TComponent; aCallType : TCallType; aVisualStyle
            : TTemplateType = ttNone); reintroduce; virtual;
    procedure RebuildDBConsistensy(aYMSetID : Integer);
    property Color: TColor read GetColor write SetColor;
    property IsTemplateSet: Boolean write SetIsTemplateSet;
    property SensingHeadClass: TSensingHeadClass read FSensingHeadClass write
            FSensingHeadClass;
    property SingleNameOnly: Boolean write FSingleNameOnly default False;
    property StyleIDs: string read FStyleIDs write SetStyleIDs;
    property TemplateName: string read FTemplateName write SetTemplateName;
    property TemplateType: TTemplateType read FTemplateType write 
            SetTemplateType;
    property YMSettingID: string write SetSettingID;
  published
    property OverrideAllow: Boolean write SetOverrideAllow default False;
  end;
  
  TDialogBottom = class (TObject)
  end;
  



implementation // 15.07.2002 added mmMBCS to imported units

{$R *.DFM}

uses
  mmMBCS,
  LoepfeGlobal, BaseGlobal, memcheck, SettingsReader, MMHtmlHelp, XMLSettingsAccess;

const

  cCheckTemplateNameAndTKAvailability =
    'select c_YM_set_name from t_xml_ym_settings ' +
    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name ' +
    'and c_head_class = :c_head_class';


  cCheckTemplateNameAndTKClassAvailability =
    'select c_YM_set_name from t_xml_ym_settings ' +
    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name  ' +
    'and c_head_class =:c_head_class  ' +
    'and c_YM_set_id <> :c_YM_set_id ';


  cCheckSingleTemplateNameAvailability =
    'select c_YM_set_name from t_xml_ym_settings ' +
    'where c_template_set = 1 and c_YM_set_name = :c_YM_set_name ';


  cGetStyleName = 'select c_style_id, c_style_name ' +
                  'from t_style ' +
                  'where c_style_id = %s ';

  cGetAllStyles = 'select c_style_name, c_style_id '+
                  'from t_style ' +
                  'order by c_style_name ';

  cGetAllActiveStyles = 'select c_style_name, c_style_id '+
                        'from t_style ' +
                        'where c_style_state = 1 ' +
                        'order by c_style_name ';



  cGetYM_Settings_Color = 'select * ' +
                          'from t_xml_ym_settings ' +
                          'where c_ym_set_id = %s' ;


  cDelete_YM_SetID_StyleID = 'delete t_Style_Settings where c_ym_set_id = %d and c_style_id = %d';

  cInsert_YM_SetID_StyleID = 'insert INTO t_Style_Settings (c_ym_set_id,c_style_id) Values (%d, %d)';


  cUpdateColorQuery =
    'update t_xml_ym_settings ' +
    'set c_color = :c_color ' +
    'where c_ym_set_id = :c_ym_set_id';
//------------------------------------------------------------------------------


//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
{
***************************** TSaveTemplateDLGForm *****************************
}
//:-----------------------------------------------------------------------------
constructor TSaveTemplateDLGForm.Create(aOwner: TComponent; aCallType : 
        TCallType; aVisualStyle : TTemplateType = ttNone);
begin
  inherited Create(aOwner);
  
  pNewTemplate.Visible:= FALSE;
  pChangeTemplate.Visible := FALSE;

  pNewTemplate.BevelInner := bvNone;
  pNewTemplate.BevelOuter := bvNone;

  pChangeTemplate.BevelInner := bvNone;
  pChangeTemplate.BevelOuter := bvNone;

  lbRemark.Caption  := '';


  HelpContext := GetHelpContext('CLA\CLA_Reinigereinstellung_speichern.htm');
  mCallType := aCallType;

  bColor.ColorArray := cMMProdGroupColors;

  mAdoDBAccess := TAdoDBAccess.Create(1);
  mAdoDBAccess.Init;

  fTemplateType := aVisualStyle;

  FOverrideAllow := False;
  FSingleNameOnly := False;
  FTemplateName := '';
  FIsTemplateSet := TRUE;
  
  mActualStyles := TStringList.Create;
  mStyles       := TStringList.Create;
  
  mActualStyles.Sorted := TRUE;
  mStyles.Sorted := TRUE;
  
  edTemplateName.Text := '';
  
  
  
  //mLayoutAsStyleAdministrator := TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable;
  
  {
   if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable  then  //Artikelverwaltung
     else  //Vorlagenverwaltung
  }
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.bColorChangeColor(Sender: TObject);
begin
  inherited;
  mColor := bColor.Color;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.bHelpClick(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.bMoveLeftClick(Sender: TObject);
var
  xIndex: Integer;
begin
  inherited;
  xIndex := 0;
  while GetSelected(lbDest, xIndex) do begin
    // move selected items to left list box
    lbSource.Items.AddObject(lbDest.Items.Strings[xIndex], lbDest.Items.Objects[xIndex]);
    lbDest.Items.Delete(xIndex);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.bMoveRightClick(Sender: TObject);
var
  xIndex: Integer;
begin
  inherited;
  xIndex := 0;
  while GetSelected(lbSource, xIndex) do begin
    // move selected items to right list box
    lbDest.Items.AddObject(lbSource.Items.Strings[xIndex], lbSource.Items.Objects[xIndex]);
    lbSource.Items.Delete(xIndex);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.bOKClick(Sender: TObject);
var
  xExists: Boolean;
  xText, xValue: string;
  x, xStyleID: Integer;
  xList: TStringList;
begin

  try

     FTemplateName := Trim(edTemplateName.Text);
     xExists := CheckTemplateNameAvailability(FTemplateName);

    case FTemplateType of
      ttNewTemplateWithAssignToStyles, ttNewTemplateOnly :
          //**********************************************************************
          //Neues Template
        begin
  //           FTemplateName := Trim(edTemplateName.Text);
             // Keine Reiniger Vorlage
           if (FTemplateName = '') then begin
               WarningMsg(rsTemplateNameIsEmptyMsg);
               edTemplateName.SetFocus;
           end else
               if (lbDest.Items.Count = 0) and (FTemplateType = ttNewTemplateWithAssignToStyles ) then begin
                    // Kein Artikel
                  WarningMsg(rsStyleIsEmptyMsg);
                  lbSource.SetFocus;
  
                  if lbSource.Items.Count >0 then
                     if (lbSource.ItemIndex < 0) then
                         lbSource.ItemIndex := 0;
               end else
                  if (FTemplateName <> '') then begin
                       //xExists := CheckTemplateNameAvailability(FTemplateName);
  
  
                     if FTemplateType = ttNewTemplateWithAssignToStyles then begin
                           //Ausgewaehlte Artikel ID's ermitteln und in property StyleIDs schreiben
                         xList := TStringList.Create;
                         xList.Sorted := TRUE;
                         for x:= 0 to lbDest.Items.Count-1 do begin
  
                             try
                               xStyleID := Integer( lbDest.Items.Objects[x] );
                             except
                               xStyleID:= -1;
                             end;
  
                             xValue := IntToStr(xStyleID);
                             xList.Add(xValue);
                         end;
                         FStyleIDs :=  xList.CommaText;
                         xList.Free;
                      end else begin
                         FStyleIDs := '';
                         Color := pColor.Color;
                      end;
  
  
                       // Ueberschreiben erlaubt
                     if FOverrideAllow then begin
  
                        if not xExists then begin
                         ShowConfirmDLG;
                        end else
                          if MessageDlg(Format(rsExistsTemplateMsg + cCRLF +
                                               rsOverrideTemplateMsg,
                                               [FTemplateName]),
                                               mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
                            Self.Close;
                            ModalResult := mrOk;
                          end;
  
                     end else
                          // Ueberschreiben nicht erlaubt
                        if not xExists then begin
                          ShowConfirmDLG;
                        end else begin
                            //Vorlagenname existiert
                            //WarningMsg(rsTemplateNameAlreadyAvailableMsg);
                          xText := Format( rsSaveNotAllowedMsg ,[FTemplateName]);
                          WarningMsg(xText);
                          edTemplateName.SetFocus;
                        end;
                  end else begin
                        // Kein Text
                      WarningMsg(rsTemplateNameIsEmptyMsg);
                      edTemplateName.SetFocus;
                  end;
        end;
          //**********************************************************************
  
      ttModifySettingsToAllAssignedStyles,
      ttModifyTemplate :
          //**********************************************************************
        begin
          //Geaendertes Template "uebreschreiben" (neue ID & in CorssTab. updaten)
          //property StyleIDs ist schon abgefuellt
          //Start 2.02
          // Neuer Check "if Exists" um Settings mit gleichem YM-Namen und TK zu unterbinden. (z.B. Klein/Gross-schreibung zweier YM-Sets mit gleichem TK geht nicht mehr!!) Nue:22.04.09
          if not xExists then begin
            ShowConfirmDLG;
          end else begin
              //Vorlagenname existiert
              //WarningMsg(rsTemplateNameAlreadyAvailableMsg);
            xText := Format( rsSaveNotAllowedMsg ,[FTemplateName]);
            WarningMsg(xText);
            edTemplateName.SetFocus;
          end;
          //End 2.02
////          ShowConfirmDLG;
        end;
          //**********************************************************************
      ttRenameTemplateOnly : begin
                                  //FTemplateName := Trim(edTemplateName.Text);
                                if (FTemplateName <> '') then begin
                                      //xExists := CheckTemplateNameAvailability(FTemplateName);
                                    if xExists then begin
                                         //Vorlagenname existiert
                                       WarningMsg(rsTemplateNameAlreadyAvailableMsg);
                                       edTemplateName.SetFocus;
                                       exit;
                                    end;
  
                                    ShowConfirmDLG;
                                      {
                                      if FTemplateName = mTemplateName_From_Outside then begin
                                         WarningMsg(rsRenameTemplateMsg);
                                         edTemplateName.SetFocus;
                                      end else begin
  
                                      end;
                                      }
                                end else begin
                                      // Kein Text
                                    WarningMsg(rsTemplateNameIsEmptyMsg);
                                    edTemplateName.SetFocus;
                                end;
                             end;
     end; //End case
  
  except
    on e: Exception do begin
            SystemErrorMsg_('TSaveTemplate.bOKClick failed. ' + e.Message);
            Close;
            ModalResult := mrCancel;
          end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.cbActiveStylesClick(Sender: TObject);
begin
  inherited;
  
  if Assigned( mAdoDBAccess ) then
     with mAdoDBAccess.Query[cPrimaryQuery] do begin
       Close;
       if cbActiveStyles.Checked then
          ShowStyleList(cGetAllActiveStyles)
       else
          ShowStyleList(cGetAllStyles);
     end;
end;

//:-----------------------------------------------------------------------------
function TSaveTemplateDLGForm.CheckTemplateNameAvailability(aName: string): 
        Boolean;
var
  xYmID: Integer;

begin
  
  try
    Result := FALSE;
    xYmID := StrToInt(fYMSettingID);   //War bis 22.04.09 geklammert
    with mAdoDBAccess.Query[cPrimaryQuery] do begin
      Close;
      if FSingleNameOnly then
        SQL.Text := cCheckSingleTemplateNameAvailability
      else begin
  //Anpassungen 2.02
//Neu ab 22.04.09 geklammert        SQL.Text := cCheckTemplateNameAndTKAvailability;
        SQL.Text := cCheckTemplateNameAndTKClassAvailability;  //War bis 22.04.09 geklammert
        ParamByName('c_head_class').AsInteger := Ord(FSensingHeadClass);
        ParamByName('c_YM_set_id').AsInteger := xYmID;  //War bis 22.04.09 geklammert
      end;
  
  //      ParamByName('c_YM_set_name').AsString := edTemplateName.Text;
      ParamByName('c_YM_set_name').AsString := aName;
      Open;
      Result := not EOF;  // ADO Conform
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TSaveTemplateDLGForm.CheckTemplateNameAvailability failed. ' + e.Message);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.ChooseSaveManner(aSender: TmmSpeedButton);
var
  xText: string;
begin
  
  edTemplateName.Text := TemplateName;
  xText := TemplateName;
  if sbOverrideTemplate.Down then begin
     lbRemark.Caption := rsRemarkOverrideTemplate; // + rsRemarkChangeTempalte1;
  
     pNewTemplate.Visible := FALSE;
  
     pStyles.Visible         := TRUE;
     pChangeTemplate.Parent  := pSavePanel;
     pChangeTemplate.Align   := alClient;
     pChangeTemplate.Visible := TRUE;
  
     edTemplateName.Text := mTemplateName_From_Outside;
     TemplateName :=  xText;
  
     if not mLayoutAsStyleAdministrator then begin
        FTemplateType := ttModifyTemplate  ;
        lbRemark.Caption := rsRemarkOverrideTemplate_TemplateView;
     end else begin
        FTemplateType := ttModifySettingsToAllAssignedStyles;
        lbRemark.Caption := rsRemarkOverrideTemplate; // + rsRemarkChangeTempalte1;
     end;
  end;
  
  
  if sbNewTemplate.Down then begin
     lbRemark.Caption := rsRemarkNewTemplate0 + rsRemarkNewTemplate1;
     pChangeTemplate.Visible := FALSE;
  
     pStyles.Visible      := TRUE;
     pStyles.Enabled      := TRUE;
     pNewTemplate.Parent  := pSavePanel;
     pNewTemplate.Align   := alClient;
     pNewTemplate.Visible := TRUE;
  
     bMoveRight.Enabled      := TRUE;
     bMoveLeft.Enabled      := TRUE;
  
     edTemplateName.Text := '';
     FTemplateType := ttNewTemplateWithAssignToStyles;
     edTemplateName.SetFocus;
  end;
  
  
  if sbTemplateOnly.Down then begin
     if not mLayoutAsStyleAdministrator then
       lbRemark.Caption := rsRemarkTemplateOnly
     else
       lbRemark.Caption := rsRemarkTemplateOnly;
  
     pChangeTemplate.Visible := FALSE;
  
     pStyles.Visible      := FALSE;
     pNewTemplate.Parent  := pSavePanel;
     pNewTemplate.Align   := alClient;
     pNewTemplate.Visible := TRUE;
  
     FTemplateType := ttNewTemplateOnly;
     edTemplateName.Text := '';
     edTemplateName.SetFocus;
  end;
  
  if sbRenameTemplate.Down then begin
     pChangeTemplate.Visible := FALSE;
  
     if not mLayoutAsStyleAdministrator then
       lbRemark.Caption := rsRemarkRenameTemplate_TemplateView
     else
       lbRemark.caption:= rsRemarkRenameTemplate;
  
     pStyles.Visible      := FALSE;
     pNewTemplate.Parent  := pSavePanel;
     pNewTemplate.Align   := alClient;
     pNewTemplate.Visible := TRUE;
  
     FTemplateType := ttRenameTemplateOnly;
     edTemplateName.Text := mRenameTemplate;
     edTemplateName.SetFocus;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.CreateInfoPanel(aValue : String);
var
  xLabel: TLabel;
  xPanel: TPanel;
begin
  xPanel := TPanel.Create(lbStyles);
  xLabel := TLabel.Create(xPanel);
  
  xPanel.Height := xLabel.Height;
  xPanel.Align:= alTop;
  xPanel.BevelOuter := bvNone;
  xPanel.Visible := TRUE;
  xPanel.Color := lbStyles.Color;
  
  xLabel.Top := 0;
  xLabel.Left := 5;
  xLabel.Visible := TRUE;
  xLabel.Parent := xPanel;
  
  xPanel.Parent := lbStyles;
  xLabel.Caption :=  aValue;
  
  lbStyles.Update;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.Delete_YM_SetID_StyleID(aYMID, aStyleID: 
        Integer);
begin
  with mAdoDBAccess.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := Format(cDelete_YM_SetID_StyleID, [aYMID, aStyleID]);
    ExecSQL;
  finally
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.DestroyInfoPanel;
var
  xPanel: TPanel;
begin
  
  while lbStyles.ComponentCount > 0 do
    if lbStyles.Components[0] is TPanel then begin
       xPanel :=  (lbStyles.Components[0] as TPanel);
       xPanel.Free;
    end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.edTemplateNameChange(Sender: TObject);
begin
  inherited;
  TemplateName := edTemplateName.Text;
  
  if sbRenameTemplate.Down then
     mRenameTemplate := edTemplateName.Text;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.edTemplateNameKeyDown(Sender: TObject; var Key:
        Word; Shift: TShiftState);
var
  xMgs: TMsg;
begin
  inherited;
  if (Key = VK_RETURN) or (Key = VK_ESCAPE) then begin
     Perform(WM_NextDlgCtl, 0, 0);
     PeekMessage(xMgs, 0, WM_CHAR, WM_CHAR, PM_REMOVE);  // Beep-Ton ausschalten  -> KeyUp-Event loeschen

     if Key = VK_ESCAPE then
        bCancel.Click
     else
        bOK.Click;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.FormCreate(Sender: TObject);
begin

  inherited;
{
  pNewTemplate.Visible:= FALSE;
  pChangeTemplate.Visible := FALSE;

  pNewTemplate.BevelInner := bvNone;
  pNewTemplate.BevelOuter := bvNone;

  pChangeTemplate.BevelInner := bvNone;
  pChangeTemplate.BevelOuter := bvNone;

  lbRemark.Caption  := '';
}
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.FormDestroy(Sender: TObject);
begin
  inherited;
  mActualStyles.Free;
  mStyles.Free;
  mAdoDBAccess.Free;
  DestroyInfoPanel;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.FormKeyDown(Sender: TObject; var Key: Word; 
        Shift: TShiftState);
var
  xMgs: TMsg;
begin
  inherited;
  if (Key = VK_ESCAPE) then begin
     Perform(WM_NextDlgCtl, 0, 0);
     PeekMessage(xMgs, 0, WM_CHAR, WM_CHAR, PM_REMOVE);  // Beep-Ton ausschalten  -> KeyUp-Event loeschen
     bCancel.Click;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.FormShow(Sender: TObject);
begin
  inherited;
  Prepare;
end;

//:-----------------------------------------------------------------------------
function TSaveTemplateDLGForm.GetColor: TColor;
begin
  //  Result := bColor.Color;
  Result := mColor;
end;

//:-----------------------------------------------------------------------------
function TSaveTemplateDLGForm.GetSelected(aListBox: TCustomListBox; var aIndex: 
        Integer): Boolean;
begin
  if (aIndex < 0) then
    aIndex := 0;
  
  Result := False;
  with aListBox do
    while (aIndex < Items.Count) do begin
      Result := Selected[aIndex];
      if Result then
        Break;
      inc(aIndex);
    end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.lbDestDblClick(Sender: TObject);
begin
  inherited;
  lbSource.Items.AddObject(lbDest.Items.Strings[lbDest.ItemIndex], lbDest.Items.Objects[lbDest.ItemIndex]);
  lbDest.Items.Delete(lbDest.ItemIndex);
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.lbDestKeyUp(Sender: TObject; var Key: Word; 
        Shift: TShiftState);
var
  x: Integer;
begin
  inherited;
  if (( Key = Ord('A') ) or ( Key = Ord('a')) ) and  (ssCtrl in Shift) then
     for x:= 0 to lbDest.Items.Count-1 do
         lbDest.Selected[x] := TRUE;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.lbSourceDblClick(Sender: TObject);
begin
  inherited;
  lbDest.Items.AddObject(lbSource.Items.Strings[lbSource.ItemIndex], lbSource.Items.Objects[lbSource.ItemIndex]);
  lbSource.Items.Delete(lbSource.ItemIndex);
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.lbSourceKeyUp(Sender: TObject; var Key: Word; 
        Shift: TShiftState);
var
  x: Integer;
begin
  inherited;
  if (( Key = Ord('A') ) or ( Key = Ord('a')) ) and  (ssCtrl in Shift) then
     for x:= 0 to lbSource.Items.Count-1 do
         lbSource.Selected[x] := TRUE;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.New_YM_SetID_StyleID(aYMID, aStyleID: Integer);
begin
  with mAdoDBAccess.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := Format(cInsert_YM_SetID_StyleID, [aYMID, aStyleID]);
    ExecSQL
  finally
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.pChangeTemplateResize(Sender: TObject);
begin
  inherited;
  //lbStyles.Width  :=  pRemark.Width;
  lbStyles.Height :=  pChangeTemplate.Height - lbStyles.Top - 5;
  
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.pNewTemplateResize(Sender: TObject);
begin
  inherited;
  pStyles.Height := pNewTemplate.Height - edTemplateName.Top - edTemplateName.Height - 7;
  lbSource.Height :=  pStyles.Height - lbSource.Top - 5;
  lbDest.Height := lbSource.Height;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.Prepare;
var
  xList: TStringList;
  xSQL: string;
  x: Integer;
  xSpeedButton: TmmSpeedButton;
begin
   try

     if Assigned( mAdoDBAccess ) then
        with mAdoDBAccess.Query[cPrimaryQuery] do begin
           Close;
  
           xList:=  TStringList.Create;
  
             //Artikel-Namen ermitteln anhand der Artikel-ID
           xList.CommaText := FStyleIDs;
  
           if xList.Count >0 then begin
              for x:=0 to xList.Count-1 do begin
                  if x = 0 then
                     xSQL := Format(cGetStyleName,[xList.Strings[x]])
                  else
                     xSQL := Format('%s or c_style_id = %s',[xSQL, xList.Strings[x]] )
              end;
              xSQL := xSQL+' order by c_style_name ';

              SQL.Text :=  xSQL;
              Open;
              First;

              mActualStyles.Clear;
              while not EOF do begin
                  mActualStyles.AddObject( FieldByName('c_style_name').AsString,
                                           TObject(FieldByName('c_style_id').AsInteger) );

                  CreateInfoPanel( FieldByName('c_style_name').AsString );

                  Next;
              end;
           end;

           if fYMSettingID <> '' then begin
              Close;
              xSQL := Format(cGetYM_Settings_Color,[fYMSettingID] );
              SQL.Text :=  xSQL;
              Open;

              //Farbe vom Template ermitteln und setzen
              try
                 mColor := TColor( FieldByName('c_color').AsInteger );
              except
              end;
           end else begin
              mColor:= cDefaultStyleIDColor;
              fYMSettingID := '-1';
           end;

           Color := mColor;
           pColor.Color := mColor;


           //Alle aktiven Artikel ermitteln
           ShowStyleList(cGetAllActiveStyles);


           xList.Free;
        end;
   except
     on e: Exception do begin
       raise Exception.Create('TSaveCLATemplate.CheckTemplateNameAvailability failed. ' + e.Message);
     end;
   end;
  

   mTemplateName_From_Outside := TemplateName;
   edTemplateName_1.Text :=  mTemplateName_From_Outside;
   edTemplateName.Text := '';
  
  
     //Layout als...
     // mLayoutAsStyleAdministrator = TRUE -> Artikelverwaltung
     // mLayoutAsStyleAdministrator = FALSE -> Vorlagenverwaltung
  {
     mLayoutAsStyleAdministrator := TMMSettingsReader.Instance.IsComponentStyle and
                              not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable and
                              not FIsTemplateSet;
  }
     //Neu einlesen
   with TMMSettings.Create do begin
        Init;
  
        mLayoutAsStyleAdministrator :=  IsComponentStyle and
                                        not IsOnlyTemplateSetsAvailable and
                                        not FIsTemplateSet;
        Free;
   end;

  
  
   if Not mLayoutAsStyleAdministrator then begin
      //Vorlagenverwaltung
      lbStyles.Visible := FALSE;
      laStyle.Visible := FALSE;
      sbNewTemplate.Visible := FALSE;
      laNewTemplate.Visible := FALSE;
  
      sbOverrideTemplate.Top :=  sbNewTemplate.Top;
  
      sbRenameTemplate.Top := sbOverrideTemplate.Top + 50;
      sbRenameTemplate.Visible := TRUE;
  
      edTemplateName.Text := mTemplateName_From_Outside;
  
      //Button Texte
      laTemplateOnly.Caption     := rsSBText_TemplateOnly_TemplateView;
      laOverrideTemplate.Caption := rsSBText_OverrideTemplate_TemplateView;

   end else begin
        //Artikelverawltung
        //Button Texte
      laTemplateOnly.Caption     := rsSBText_TemplateOnly;
      laNewTemplate.Caption      := rsSBText_NewTemplate;
      laOverrideTemplate.Caption := rsSBText_OverrideTemplate;
   end;
   //Button Text Rename
   laRenameTemplate.Caption := Format(rsSBText_RenameTemplate,[mTemplateName_From_Outside]);
  


   case fTemplateType of
      ttNewTemplateOnly                    : sbTemplateOnly.Down := TRUE;
      ttNewTemplateWithAssignToStyles      : sbNewTemplate.Down := TRUE;
      ttRenameTemplateOnly                 : sbRenameTemplate.Down := TRUE;
      ttModifySettingsToAllAssignedStyles  : sbOverrideTemplate.Down := TRUE;
      ttActualSetIsNoTemplate              : sbNewTemplate.Down := TRUE;
   end;
  
     //Nur eine Funktion aktivieren -> Direkt- oder Single-Modus
   if fTemplateType <> ttNone then begin
       if fTemplateType = ttActualSetIsNoTemplate then begin
          //Einiges Property, welches von aussen gesetzt wird; Assigments
  
          if not FIsTemplateSet then begin
             sbNewTemplate.Down := TRUE;
             sbNewTemplate.Click;
          end else begin
             sbNewTemplate.Enabled := FALSE;
             sbTemplateOnly.Down := TRUE;
             sbTemplateOnly.Click;
          end;
  
          sbOverrideTemplate.Enabled := FALSE;
          sbRenameTemplate.Enabled := FALSE;
  
       end else
           for x:= 0 to  ComponentCount - 1 do
               if (Components[x] is TMMSpeedButton) then
                 if ((Components[x] as TMMSpeedButton).Tag=1) then  begin  //Nue: Tag unterscheiden: Modus Button sollten 1 haben; alle anderen SpeedButtons sollten 0 haben
                    xSpeedButton := Components[x] As TmmSpeedButton;
                    if xSpeedButton.Down then
                       xSpeedButton.Click
                    else
                       xSpeedButton.Enabled := FALSE;
                 end;
   end else begin
          //Alle Funktionen sind aktiv

         //Standard Button gedrueckt
         if mCallType = tcClearerAssitant then begin
            //Nur fuer ClearerAssitant
            if Not mLayoutAsStyleAdministrator then begin
               sbTemplateOnly.Down := TRUE;
               sbTemplateOnly.Click;
            end else begin
               //Vorlagen nur ab Vorlagenverwaltung m�glich -> MMConfig.
               sbNewTemplate.Down := TRUE;
               sbNewTemplate.Click;
            end;
         end else begin
             sbOverrideTemplate.Down := TRUE;
             sbOverrideTemplate.Click;
        end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.RebuildDBConsistensy(aYMSetID : Integer);
var
  xOldSetID, xStyleID, x: Integer;
  xList: TStringList;
  

begin
  try
    xOldSetID := StrToInt(fYMSettingID);
  except
     xOldSetID := -1;
  end;


  if aYMSetID < 0 then aYMSetID := xOldSetID;  // Nur fuer Rename-Template

  mAdoDBAccess.StartTransaction;
  
  UpdateYMSetColor(aYMSetID);
  
  
  if (fTemplateType = ttNewTemplateOnly) or (fTemplateType = ttModifyTemplate) or
     (fTemplateType = ttRenameTemplateOnly) then begin
  
     if fTemplateType = ttRenameTemplateOnly then
        UpdateTemplateName(aYMSetID, TemplateName);
  
     mAdoDBAccess.Commit;
     exit;
  end;
  
  if not FIsTemplateSet or (not (FIsTemplateSet and  (TemplateType = ttModifySettingsToAllAssignedStyles) ))  then begin
     //Reiniger Assistent: immer als neues Template speichern -> (mach aus Vorlage ein Template)
     //Assigment:
  
     xList := TStringList.Create;
     xList.CommaText :=  StyleIDs;
  
     {
     if fTemplateType = ttRenameTemplateOnly then begin
        UpdateTemplateName(aYMSetID, TemplateName);
     end;
     }
  
     for x:= 0 to xList.Count-1 do begin
       try
         xStyleID := StrToInt( xList.Strings[x] );
  
         if TemplateType = ttModifySettingsToAllAssignedStyles then
            //Alte YMSetting-ID und Art.-ID in t_style_settings loeschen
            Delete_YM_SetID_StyleID(xOldSetID, xStyleID);
  
         //In KreuzTab. t_style_settings YMSetting-ID und Art.-ID speichern
         New_YM_SetID_StyleID(aYMSetID, xStyleID);
  
       except
       end;
  
     end;//END for
     xList.Free;
  end;
  
  mAdoDBAccess.Commit;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.sbAssignedStylesClick(Sender: TObject);
var
  x, xIndex, n: Integer;
begin
  inherited;
  n:= -1;
  for x:= 0 to lbSource.Items.Count-1 do
      lbSource.Selected[x] := FALSE;
  
  for x:= 0 to mActualStyles.Count-1 do begin
      xIndex := lbSource.Items.IndexOf(  mActualStyles.Strings[x] );
      if xIndex >= 0 then begin
         lbSource.Selected[xIndex] := TRUE;
         if n < 0 then n:= xIndex;
      end;
  end;
  
  lbSource.ItemIndex := n;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.sbOverrideTemplateClick(Sender: TObject);
begin
  inherited;
  //  if Self.Visible then
    if Sender is TmmSpeedButton then ChooseSaveManner(Sender as TmmSpeedButton);
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetColor(aColor: TColor);
begin
  bColor.Color := aColor;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetIsTemplateSet(const Value: Boolean);
begin
  FIsTemplateSet := Value;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetOverrideAllow(const Value: Boolean);
begin
  FOverrideAllow := Value;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetSettingID(const Value: string);
begin
  fYMSettingID := Value;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetStyleIDs(const Value: string);
begin
  FStyleIDs := Value;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetTemplateName(const Value: string);
begin
  FTemplateName := Value;
  //  edTemplateName_1.Text := fTemplateName;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.SetTemplateType(const Value: TTemplateType);
begin
  FTemplateType := Value;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.ShowConfirmDLG;
//var
//  xText: string;
begin
   {
   if (FTemplateType = ttNewTemplateWithAssignToStyles) or (FTemplateType = ttNewTemplateOnly) then
      xText := Format(rsSaveQuestionNewTemplate, [FTemplateName] )
   else
      if (FTemplateType = ttRenameTemplateOnly) then
         xText := Format(rsSaveTemplateQuestion, [FTemplateName] )
      else
        xText := Format(rsSaveQuestion, [FTemplateName] );
  
   if MessageDlg(xText, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      Close;
      ModalResult := mrOk;
   end else begin
      Close;
      ModalResult := mrCancel;
   end;
  }
  Close;
  ModalResult := mrOk;
end;

//:-----------------------------------------------------------------------------
//procedure TSaveTemplateDLGForm.ShowModifyTemplateWithStyleList(aWithStyleList 
//        :Boolean);
//begin
//  
//end;
//
//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.ShowStyleList(aSQL:String);
var
  x, xIndex: Integer;
begin
  
  if aSQL = '' then exit;
  if Assigned( mAdoDBAccess ) then
     with mAdoDBAccess.Query[cPrimaryQuery] do begin
       Close;
       SQL.Text := aSQL;
       Open;
       First;
  
       mStyles.Clear;
  
       while not EOF do begin
           mStyles.AddObject( FieldByName('c_style_name').AsString,
                              TObject(FieldByName('c_style_id').AsInteger) );
           Next;
       end;
  
       //Aktuelle Artikel aus Gesamtliste loeschen
       for x:=0 to mActualStyles.Count-1 do
           if mStyles.Find(mActualStyles.Strings[x], xIndex) then
              mStyles.Delete(xIndex);
     end; //END with
  
  lbSource.Clear;
  lbSource.Items.AddStrings(mStyles);
  lbSource.Items.AddStrings(mActualStyles);
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.UpdateTemplateName(aSetID: integer; aName: 
        string);
begin
  try
    if Assigned( mAdoDBAccess ) then
       with mAdoDBAccess.Query[cPrimaryQuery] do begin
         Close;
         SQL.Text := cUpdateYMSetName;
         ParamByName('c_ym_set_id').AsInteger := aSetID;
         ParamByName('c_YM_set_name').AsString := aName;
         ExecSQL;
       end
     else raise Exception.Create('AdoDBAccess not assigned.');
  
  except
    on e: Exception do begin
       raise Exception.Create('TSaveTemplateDLGForm.UpdateTemplateName failed. ' + e.Message);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TSaveTemplateDLGForm.UpdateYMSetColor(aSetID : Integer);
begin
  try
    if Assigned( mAdoDBAccess ) then
       with mAdoDBAccess.Query[cPrimaryQuery] do begin
         Close;
         SQL.Text := cUpdateColorQuery;
         ParamByName('c_ym_set_id').AsInteger := aSetID;
         ParamByName('c_color').AsInteger := Color;
         ExecSQL;
       end
     else raise Exception.Create('AdoDBAccess not assigned.');
  
  except
    on e: Exception do begin
       raise Exception.Create('TSaveTemplateDLGForm.UpdateYMSetColor failed. ' + e.Message);
    end;
  end;
end;
//:-----------------------------------------------------------------------------


//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------
//:-----------------------------------------------------------------------------

//:-----------------------------------------------------------------------------
end.








