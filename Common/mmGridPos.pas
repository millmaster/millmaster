(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGridPos.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.04.99 | 0.01 | PW  | File created
| 27.09.99 | 0.02 | PW  | Read/Write settings from/to db, using TMemoINI
|=========================================================================================*)

unit mmGridPos;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Wwdbgrid, Wwdbigrd, DB, Registry, MemoINI, mmCommonLib;

const
  cFieldHideID = -1;
  cRegistryRoot = 'Software\Loepfe\Gridinfo';

type
  tFontRecordT = record
    Name: TFontName;
    Color: TColor;
    Size: integer;
    Style: TFontStyles;
  end;                                  //FontRecord

  tColumnRecordT = record
    ID: integer;
    FieldName,
      DisplayLabel: string;
    DataType: TFieldType;
    Index: integer;
    DisplayWidth: byte;
    Alignment: TAlignment;
    DisplayFormat: string;
    Tag: longint;
    Visible: boolean;
  end;                                  //tColumnRecordT

  tGridInfoRecordT = record
    Color,
      TitleColor: TColor;
    Font,
      TitleFont: tFontRecordT;
    Options: TwwDBGridOptions;
    FixedCols: byte;
    TitleAlignment: TAlignment;
    Columns: array of tColumnRecordT;
  end;                                  //tGridInfoRecordT

  TmmGridPos = class(TComponent)
  private
    FGrid: TwwDBGrid;
    FMemoINI: TMemoIni;
    FLookupID: string;
    FAlterColors: boolean;
    FAlterColumnFormat: boolean;
    FAlterColumnPosition: boolean;
    FAlterFixedCols: boolean;
    FAlterFonts: boolean;
    FAlterGridOptions: boolean;
    FAlterVisibleCols: boolean;
    procedure SetGrid(value: TwwDBGrid);
    function GetGrid: TwwDBGrid;
    procedure SetLookupID(value: string);

    procedure SetMemoINI(value: TMemoINI);
    function GetMemoINI: TMemoINI;
  protected
   //GridInfo: tGridInfoRecordT;
    function GetDisplayFormatStr(aField: TField): string;
    function GetFontStyles(aStyles: string): TFontStyles;
    function SetFontStyles(aStyles: TFontStyles): string;
    function GetGridOptions(aOptions: string): TwwDBGridOptions;
    function SetGridOptions(aOptions: TwwDBGridOptions): string;
    function GetSectionName(aSection: string): string;
   //function GetSubStr(aStr: string; aPos: byte): string;
   //procedure ReadFromDataset;
   //procedure WriteToDataset;
    procedure ReadFromRegistry;
    procedure WriteToRegistry;
  public
    GridInfo: tGridInfoRecordT;         //SDO
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure ReadSettings;
    procedure WriteSettings;

    procedure EditSettings;
    procedure EditSettingsStatistic;    //SDO


    procedure ReadFromDataset;          // SDO
    procedure WriteToDataset;           //SDO
  published
    property MemoINI: TMemoIni read GetMemoINI write SetMemoINI;

    property AlterColors: boolean read FAlterColors write FAlterColors;
    property AlterColumnFormat: boolean read FAlterColumnFormat write FAlterColumnFormat;
    property AlterColumnPosition: boolean read FAlterColumnPosition write FAlterColumnPosition;
    property AlterGridOptions: boolean read FAlterGridOptions write FAlterGridOptions;
    property AlterFixedCols: boolean read FAlterFixedCols write FAlterFixedCols;
    property AlterFonts: boolean read FAlterFonts write FAlterFonts;
    property AlterVisibleCols: boolean read FAlterVisibleCols write FAlterVisibleCols;
    property Grid: TwwDBGrid read GetGrid write SetGrid;
    property LookupID: string read FLookupID write SetLookupID;
  end;                                  //TmmGridPos

//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmGridPosEditDlg;


function GetDisplayLabel(aDisplayLabel: string): string;
const
  cWordWrapChar = '~';                  //#126 do not modify

var
  i: integer;

begin
  i := pos(cWordWrapChar, aDisplayLabel);
  while i > 0 do begin
    aDisplayLabel[i] := #32;
    i := pos(cWordWrapChar, aDisplayLabel);
  end;                                  //while i > 0 do

  i := pos('- ', aDisplayLabel);
  while i > 0 do begin
    if i > 0 then system.delete(aDisplayLabel, i, 2);
    i := pos('- ', aDisplayLabel);
  end;                                  //while i > 0 do

  result := aDisplayLabel;
end;                                    //function GetDisplayLabel


//TmmGraph ***
constructor TmmGridPos.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  GridInfo.Columns := nil;

  FAlterColors := true;
  FAlterColumnFormat := true;
  FAlterColumnPosition := true;
  FAlterFixedCols := true;
  FAlterFonts := true;
  FAlterGridOptions := true;
  FAlterVisibleCols := true;
  FGrid := nil;
  FLookupID := '';

  FMemoINI := TMemoINI.Create(aOwner);
end;                                    //constructor TmmGridPos.Create
//-----------------------------------------------------------------------------
destructor TmmGridPos.Destroy;
begin
  GridInfo.Columns := nil;
  FMemoINI.Free;
  inherited Destroy;
end;                                    //destructor TmmGridPos.Destroy
//-----------------------------------------------------------------------------
procedure TmmGridPos.SetGrid(value: TwwDBGrid);
begin
  FGrid := value;
end;                                    //procedure TmmGridPos.SetGrid
//-----------------------------------------------------------------------------
function TmmGridPos.GetGrid: TwwDBGrid;
begin
  result := FGrid;
end;                                    //function TmmGridPos.GetGrid
//-----------------------------------------------------------------------------
procedure TmmGridPos.SetLookupID(value: string);
begin
  FLookupID := uppercase(trim(value));
end;                                    //procedure TmmGridPos.SetLookupID
//-----------------------------------------------------------------------------
function TmmGridPos.GetDisplayFormatStr(aField: TField): string;
begin
  case aField.DataType of
    ftSmallint, ftInteger, ftFloat, ftWord, ftAutoInc, ftLargeInt: result := TNumericField(aField).DisplayFormat;
    ftDate, ftTime, ftDateTime: result := TDateTimeField(aField).DisplayFormat;
  else result := '';
  end;                                  //case aField.DataType of
end;                                    //function TmmGridPos.GetDisplayFormatStr
//-----------------------------------------------------------------------------
function TmmGridPos.GetFontStyles(aStyles: string): TFontStyles;
begin
  result := [];
  if pos('fsBold', aStyles) > 0 then result := result + [fsBold];
  if pos('fsItalic', aStyles) > 0 then result := result + [fsItalic];
  if pos('fsUnderline', aStyles) > 0 then result := result + [fsUnderline];
  if pos('fsStrikeout', aStyles) > 0 then result := result + [fsStrikeout];
end;                                    //function TmmGridPos.GetFontStyles
//-----------------------------------------------------------------------------
function TmmGridPos.SetFontStyles(aStyles: TFontStyles): string;
begin
  result := '';
  if fsBold in aStyles then result := result + 'fsBold,';
  if fsItalic in aStyles then result := result + 'fsItalic,';
  if fsUnderline in aStyles then result := result + 'fsUnderline,';
  if fsStrikeout in aStyles then result := result + 'fsStrikeout';
end;                                    //function TmmGridPos.SetFontStyles
//-----------------------------------------------------------------------------
function TmmGridPos.GetGridOptions(aOptions: string): TwwDBGridOptions;
begin
  result := [];
  if pos('dgEditing', aOptions) > 0 then result := result + [dgEditing];
  if pos('dgTitles', aOptions) > 0 then result := result + [dgTitles];
  if pos('dgColumnResize', aOptions) > 0 then result := result + [dgColumnResize];
  if pos('dgTabs', aOptions) > 0 then result := result + [dgTabs];
  if pos('dgConfirmDelete', aOptions) > 0 then result := result + [dgConfirmDelete];
  if pos('dgCancelOnExit', aOptions) > 0 then result := result + [dgCancelOnExit];
  if pos('dgWordWrap', aOptions) > 0 then result := result + [dgWordWrap];
  if pos('dgPerfectRowFit', aOptions) > 0 then result := result + [dgPerfectRowFit];
  if pos('dgMultiSelect', aOptions) > 0 then result := result + [dgMultiSelect];
  if pos('dgShowFooter', aOptions) > 0 then result := result + [dgShowFooter];
  if pos('dgFooter3DCells', aOptions) > 0 then result := result + [dgFooter3DCells];
  if pos('dgNoLimitColSize', aOptions) > 0 then result := result + [dgNoLimitColSize];
  if pos('dgIndicator', aOptions) > 0 then result := result + [dgIndicator];
  if pos('dgColLines', aOptions) > 0 then result := result + [dgColLines];
  if pos('dgRowLines', aOptions) > 0 then result := result + [dgRowLines];
  if pos('dgRowSelect', aOptions) > 0 then result := result + [dgRowSelect];
  if pos('dgAlwaysShowSelection', aOptions) > 0 then result := result + [dgAlwaysShowSelection];
  if pos('dgTrailingEllipsis', aOptions) > 0 then result := result + [dgTrailingEllipsis];
end;                                    //function TmmGridPos.GetGridOptions
//-----------------------------------------------------------------------------
function TmmGridPos.SetGridOptions(aOptions: TwwDBGridOptions): string;
begin
  result := '';
  if dgEditing in aOptions then result := result + 'dgEditing,';
  if dgTitles in aOptions then result := result + 'dgTitles,';
  if dgColumnResize in aOptions then result := result + 'dgColumnResize,';
  if dgTabs in aOptions then result := result + 'dgTabs,';
  if dgConfirmDelete in aOptions then result := result + 'dgConfirmDelete,';
  if dgCancelOnExit in aOptions then result := result + 'dgCancelOnExit,';
  if dgWordWrap in aOptions then result := result + 'dgWordWrap,';
  if dgPerfectRowFit in aOptions then result := result + 'dgPerfectRowFit,';
  if dgMultiSelect in aOptions then result := result + 'dgMultiSelect,';
  if dgShowFooter in aOptions then result := result + 'dgShowFooter,';
  if dgFooter3DCells in aOptions then result := result + 'dgFooter3DCells,';
  if dgNoLimitColSize in aOptions then result := result + 'dgNoLimitColSize,';
  if dgIndicator in aOptions then result := result + 'dgIndicator,';
  if dgColLines in aOptions then result := result + 'dgColLines,';
  if dgRowLines in aOptions then result := result + 'dgRowLines,';
  if dgRowSelect in aOptions then result := result + 'dgRowSelect,';
  if dgAlwaysShowSelection in aOptions then result := result + 'dgAlwaysShowSelection,';
  if dgTrailingEllipsis in aOptions then result := result + 'dgTrailingEllipsis,';
end;                                    //function TmmGridPos.SetGridOptions
//-----------------------------------------------------------------------------
function TmmGridPos.GetSectionName(aSection: string): string;
begin
  result := FGrid.Name + '.' + aSection;
end;                                    //function TmmGridPos.GetSectionName
//-----------------------------------------------------------------------------
procedure TmmGridPos.ReadFromDataset;
var
  xDS: TDataSet;
  i: integer;

begin
  if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;
  xDS := FGrid.DataSource.DataSet;

  GridInfo.Columns := nil;
  fillchar(GridInfo, sizeof(GridInfo), 0);

  xDS.DisableControls;
  try
  //1. read values from dbgrid
    GridInfo.Color := FGrid.Color;
    GridInfo.TitleColor := FGrid.TitleColor;
    GridInfo.FixedCols := FGrid.FixedCols;
    GridInfo.Options := FGrid.Options;
    GridInfo.TitleAlignment := FGrid.TitleAlignment;

    GridInfo.Font.Name := FGrid.Font.Name;
    GridInfo.Font.Color := FGrid.Font.Color;
    GridInfo.Font.Size := FGrid.Font.Size;
    GridInfo.Font.Style := FGrid.Font.Style;

    GridInfo.TitleFont.Name := FGrid.TitleFont.Name;
    GridInfo.TitleFont.Color := FGrid.TitleFont.Color;
    GridInfo.TitleFont.Size := FGrid.TitleFont.Size;
    GridInfo.TitleFont.Style := FGrid.TitleFont.Style;

    SetLength(GridInfo.Columns, xDS.FieldCount);

    for i := 0 to xDS.FieldCount - 1 do begin
      GridInfo.Columns[i].ID := i;
      GridInfo.Columns[i].FieldName := xDS.Fields[i].FieldName;
      GridInfo.Columns[i].DisplayLabel := GetDisplayLabel(xDS.Fields[i].DisplayLabel);
      GridInfo.Columns[i].DataType := xDS.Fields[i].DataType;
      GridInfo.Columns[i].Index := xDS.Fields[i].Index;
      GridInfo.Columns[i].DisplayWidth := xDS.Fields[i].DisplayWidth;
      GridInfo.Columns[i].Alignment := xDS.Fields[i].Alignment;
      GridInfo.Columns[i].DisplayFormat := GetDisplayFormatStr(xDS.Fields[i]);
      GridInfo.Columns[i].Tag := xDS.Fields[i].Tag;
      GridInfo.Columns[i].Visible := xDS.Fields[i].Visible and (xDS.Fields[i].Tag <> cFieldHideID);
    end;                                //for i := 0 to xDS.FieldCount -1 do
  finally
    xDS.EnableControls;
  end;                                  //try..finally
end;                                    //procedure TmmGridPos.ReadFromDataset
//-----------------------------------------------------------------------------
procedure TmmGridPos.WriteToDataset;
var
  xDS: TDataSet;
  i, j: integer;
  xDispStr,
    xEditStr: string;
  xField: TField;

begin
  if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;
  xDS := FGrid.DataSource.DataSet;

  xDS.DisableControls;
  try
    FGrid.Color := GridInfo.Color;
    FGrid.TitleColor := GridInfo.TitleColor;
    FGrid.FixedCols := GridInfo.FixedCols;
    FGrid.Options := GridInfo.Options;
    FGrid.TitleAlignment := GridInfo.TitleAlignment;

    FGrid.Font.Name := GridInfo.Font.Name;
    FGrid.Font.Color := GridInfo.Font.Color;
    FGrid.Font.Size := GridInfo.Font.Size;
    FGrid.Font.Style := GridInfo.Font.Style;

    FGrid.TitleFont.Name := GridInfo.TitleFont.Name;
    FGrid.TitleFont.Color := GridInfo.TitleFont.Color;
    FGrid.TitleFont.Size := GridInfo.TitleFont.Size;
    FGrid.TitleFont.Style := GridInfo.TitleFont.Style;

    for i := 0 to high(GridInfo.Columns) do begin
      xField := xDS.FieldByName(GridInfo.Columns[i].FieldName);

      xField.Index := GridInfo.Columns[i].Index;
      xField.DisplayWidth := GridInfo.Columns[i].DisplayWidth;
      xField.Alignment := GridInfo.Columns[i].Alignment;

      xDispStr := GridInfo.Columns[i].DisplayFormat;
      xEditStr := deletechar(GridInfo.Columns[i].DisplayFormat, #44);

      case xField.DataType of
        ftSmallint, ftInteger, ftFloat, ftWord, ftAutoInc, ftLargeInt: begin
            TNumericField(xField).DisplayFormat := xDispStr;

            xEditStr := '';
            for j := 1 to length(xDispStr) do
              if xDispStr[j] in ['.', '0'] then xEditStr := xEditStr + xDispStr[j];
     //if xEditStr = '' then

            TNumericField(xField).EditFormat := xEditStr;
          end;                          //ftSmallint,ftInteger,ftFloat,ftWord,ftAutoInc,ftLargeInt
        ftDateTime, ftDate, ftTime: TDateTimeField(xField).DisplayFormat := xDispStr;
      end;                              //case xField.DataType of

      xField.Tag := GridInfo.Columns[i].Tag;
      xField.Visible := GridInfo.Columns[i].Visible;
    end;                                //for i := 0 to xDS.FieldCount -1 do
  finally
    xDS.EnableControls;
  end;                                  //try..finally
end;                                    //procedure TmmGridPos.WriteToDataset
//-----------------------------------------------------------------------------
procedure TmmGridPos.ReadFromRegistry;
var
  xDS: TDataSet;
  xInt,
    i: integer;
  xStr,
    xSubStr: string;

begin
  if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;
  xDS := FGrid.DataSource.DataSet;

  GridInfo.Columns := nil;
  fillchar(GridInfo, sizeof(GridInfo), 0);

  xDS.DisableControls;
  try
  //1. init record with default values from dbgrid
    ReadFromDataset;

  //2. read values from registry => GridInfo
    FMemoIni.ReadSettings;
    xInt := FMemoIni.ReadInteger(GetSectionName('Grid'), 'Color', -1);
    if xInt > -1 then GridInfo.Color := xInt;
    xInt := FMemoIni.ReadInteger(GetSectionName('Grid'), 'TitleColor', -1);
    if xInt > -1 then GridInfo.TitleColor := xInt;
    xInt := FMemoIni.ReadInteger(GetSectionName('Grid'), 'FixedCols', -1);
    if xInt > -1 then GridInfo.FixedCols := xInt;
    xStr := FMemoIni.ReadString(GetSectionName('Grid'), 'Options', '');
    if xStr <> '' then GridInfo.Options := GetGridOptions(xStr);
    xInt := FMemoIni.ReadInteger(GetSectionName('Grid'), 'TitleAlignment', -1);
    if xInt > -1 then GridInfo.TitleAlignment := TAlignment(xInt);

    xStr := FMemoIni.ReadString(GetSectionName('Font'), 'Name', '');
    if xStr <> '' then GridInfo.Font.Name := xStr;
    xInt := FMemoIni.ReadInteger(GetSectionName('Font'), 'Color', -1);
    if xInt > -1 then GridInfo.Font.Color := xInt;
    xInt := FMemoIni.ReadInteger(GetSectionName('Font'), 'Size', -1);
    if xInt > -1 then GridInfo.Font.Size := xInt;
    xStr := FMemoIni.ReadString(GetSectionName('Font'), 'Style', '');
    if xStr <> '' then GridInfo.Font.Style := GetFontStyles(xStr);

    xStr := FMemoIni.ReadString(GetSectionName('TitleFont'), 'Name', '');
    if xStr <> '' then GridInfo.TitleFont.Name := xStr;
    xInt := FMemoIni.ReadInteger(GetSectionName('TitleFont'), 'Color', -1);
    if xInt > -1 then GridInfo.TitleFont.Color := xInt;
    xInt := FMemoIni.ReadInteger(GetSectionName('TitleFont'), 'Size', -1);
    if xInt > -1 then GridInfo.TitleFont.Size := xInt;
    xStr := FMemoIni.ReadString(GetSectionName('TitleFont'), 'Style', '');
    if xStr <> '' then GridInfo.TitleFont.Style := GetFontStyles(xStr);

    for i := 0 to high(GridInfo.Columns) do begin
      xStr := FMemoIni.ReadString(GetSectionName('Columns'), GridInfo.Columns[i].FieldName, '');
      if xStr <> '' then begin
        xSubStr := GetSubStr(xStr, cListDelimiterChar, 1);
        GridInfo.Columns[i].Index := strtoint(xSubStr);

        xSubStr := GetSubStr(xStr, cListDelimiterChar, 2);
        GridInfo.Columns[i].DisplayWidth := strtoint(xSubStr);

        xSubStr := GetSubStr(xStr, cListDelimiterChar, 3);
        GridInfo.Columns[i].Alignment := TAlignment(strtoint(xSubStr));

        xSubStr := GetSubStr(xStr, cListDelimiterChar, 4);
        GridInfo.Columns[i].DisplayFormat := xSubStr;

        xSubStr := GetSubStr(xStr, cListDelimiterChar, 5);
        GridInfo.Columns[i].Tag := strtoint(xSubStr);

        xSubStr := GetSubStr(xStr, cListDelimiterChar, 6);
        GridInfo.Columns[i].Visible := strtoint(xSubStr) = 1;
      end;                              //if xStr <> '' then
    end;                                //for i := 0 to high(GridInfo.Columns) do

  //3. GridInfo => dataset
    WriteToDataset;
  finally
    xDS.EnableControls;
  end;                                  //try..finally

end;                                    //procedure TmmGridPos.ReadFromRegistry
//-----------------------------------------------------------------------------
procedure TmmGridPos.WriteToRegistry;
var
  xDS: TDataSet;
  xInt,
    i: integer;
  xStr,
    xSubStr: string;

begin
  if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;
  xDS := FGrid.DataSource.DataSet;
  FMemoIni.ReadSettings;

  GridInfo.Columns := nil;
  fillchar(GridInfo, sizeof(GridInfo), 0);

  xDS.DisableControls;
  try
    FMemoIni.EraseSection(GetSectionName('Grid'));
    FMemoIni.EraseSection(GetSectionName('Font'));
    FMemoIni.EraseSection(GetSectionName('TitleFont'));
    FMemoIni.EraseSection(GetSectionName('Columns'));
  except
  end;                                  //try..except

  try
    FMemoIni.WriteInteger(GetSectionName('Grid'), 'Color', FGrid.Color);
    FMemoIni.WriteInteger(GetSectionName('Grid'), 'TitleColor', FGrid.TitleColor);
    FMemoIni.WriteInteger(GetSectionName('Grid'), 'FixedCols', FGrid.FixedCols);
    FMemoIni.WriteString(GetSectionName('Grid'), 'Options', SetGridOptions(FGrid.Options));
    FMemoIni.WriteInteger(GetSectionName('Grid'), 'TitleAlignment', ord(FGrid.TitleAlignment));

    FMemoIni.WriteString(GetSectionName('Font'), 'Name', FGrid.Font.Name);
    FMemoIni.WriteInteger(GetSectionName('Font'), 'Color', FGrid.Font.Color);
    FMemoIni.WriteInteger(GetSectionName('Font'), 'Size', FGrid.Font.Size);
    FMemoIni.WriteString(GetSectionName('Font'), 'Style', SetFontStyles(FGrid.Font.Style));

    FMemoIni.WriteString(GetSectionName('TitleFont'), 'Name', FGrid.TitleFont.Name);
    FMemoIni.WriteInteger(GetSectionName('TitleFont'), 'Color', FGrid.TitleFont.Color);
    FMemoIni.WriteInteger(GetSectionName('TitleFont'), 'Size', FGrid.TitleFont.Size);
    FMemoIni.WriteString(GetSectionName('TitleFont'), 'Style', SetFontStyles(FGrid.TitleFont.Style));

    for i := 0 to xDS.FieldCount - 1 do begin
      xStr := inttostr(xDS.Fields[i].Index) + cListDelimiterChar
        + inttostr(xDS.Fields[i].DisplayWidth) + cListDelimiterChar
        + inttostr(ord(xDS.Fields[i].Alignment)) + cListDelimiterChar
        + GetDisplayFormatStr(xDS.Fields[i]) + cListDelimiterChar
        + inttostr(xDS.Fields[i].Tag) + cListDelimiterChar
        + inttostr(ord(xDS.Fields[i].Visible)) + cListDelimiterChar;
      FMemoIni.WriteString(GetSectionName('Columns'), xDS.Fields[i].FieldName, xStr);
    end;                                //for i := 0 to xDS.FieldCount -1 do

    FMemoIni.WriteSettings;
  finally
    xDS.EnableControls;
  end;                                  //try..finally
end;                                    //procedure TmmGridPos.WriteToRegistry
//-----------------------------------------------------------------------------
procedure TmmGridPos.ReadSettings;
begin
  ReadFromRegistry;
end;                                    //procedure TmmGridPos.ReadSettings
//-----------------------------------------------------------------------------
procedure TmmGridPos.WriteSettings;
begin
  WriteToRegistry;
end;                                    //procedure TmmGridPos.WriteSettings
//-----------------------------------------------------------------------------
procedure TmmGridPos.EditSettings;
var
  xEditOptions: TmmGridPosEditorDlg;

begin
  if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;

  ReadFromDataset;

  xEditOptions := TmmGridPosEditorDlg.Create(Application);

  try
    xEditOptions.AlterColors := FAlterColors;
    xEditOptions.AlterColumnFormat := FAlterColumnFormat;
    xEditOptions.AlterColumnPosition := FAlterColumnPosition;
    xEditOptions.AlterFixedCols := FAlterFixedCols;
    xEditOptions.AlterFonts := FAlterFonts;
    xEditOptions.AlterGridOptions := FAlterGridOptions;
    xEditOptions.AlterVisibleCols := FAlterVisibleCols;
    xEditOptions.GridInfo := GridInfo;

    if xEditOptions.Execute then begin
      GridInfo := xEditOptions.GridInfo;
      WriteToDataset;
    end;                                //if xEditOptions.Execute then
  finally
    xEditOptions.Free;
  end;                                  //try..finally
end;                                    //procedure TmmGridPos.EditSettings
//-----------------------------------------------------------------------------
procedure TmmGridPos.SetMemoINI(value: TMemoINI);
begin
  FMemoINI := value;
end;                                    //procedure TmmGridPos.SetMemoINI
//-----------------------------------------------------------------------------
function TmmGridPos.GetMemoINI: TMemoINI;
begin
  result := FMemoINI;
end;                                    //function TmmGridPos.GetMemoINI
//-----------------------------------------------------------------------------

{
procedure Register;
begin
 RegisterComponents('Worbis', [TmmGridPos]);
end; //procedure Register
{}

procedure TmmGridPos.EditSettingsStatistic;
begin
// if FLookupID = '' then exit;
  if FGrid = nil then exit;
  if FGrid.DataSource.DataSet = nil then exit;

  ReadFromDataset;

  WriteToDataset;
end;

end. //mmGridPos.pas

