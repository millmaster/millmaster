(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGridPosEdit.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.04.99 | 0.01 | PW  | Datei erstellt
| 00.00.99 | 0.00 | PW  |
|=========================================================================================*)

unit mmGridPosEdit;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, Tabs, DB, DBCtrls, Wwdbigrd, Wwdbgrid, ComCtrls,
  IvDictio, IvMulti, IvEMulti, mmImageList, ActnList, mmToolBar, mmActionList,
  mmGridPos, NwCombo, mmColorDialog, mmFontDialog, mmRadioGroup, mmUpDown, mmEdit,
  mmCheckBox, mmLabel, mmGroupBox, mmPageControl, mmPanel, ImgList, ToolWin,
  IvMlDlgs, BASEFORM, mmTranslator, mmSpeedButton, mmButton, mmIvMlDlgs;

type
  TmmGridPosEditor = class(TmmForm)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;
    acMoveTo_lbShow: TAction;
    acMoveTo_lbAvailable: TAction;
    acMoveAllTo_lbShow: TAction;
    acMoveAllTo_lbAvailable: TAction;

    FontDialog1: TmmFontDialog;
    ColorDialog1: TmmColorDialog;
    mmTranslator1: TmmTranslator;

    MainPanel: TmmPanel;
    PageControl1: TmmPageControl;
    tsOptions: TTabSheet;
    gbSchrift: TmmGroupBox;
    paTitleColor: TmmPanel;
    laTitleFont: TmmLabel;
    paColor: TmmPanel;
    laFont: TmmLabel;
    gbGridSettings: TmmGroupBox;
    cbRowLines: TmmCheckBox;
    cbColLines: TmmCheckBox;
    cbIndicator: TmmCheckBox;
    cbRowSelect: TmmCheckBox;
    cbAlwaysShowSelection: TmmCheckBox;
    cbTitleAlignment: TmmCheckBox;
    cbTrailingEllipsis: TmmCheckBox;
    gbFixedCols: TmmGroupBox;
    edFixedCols: TmmEdit;
    udFixedCols: TmmUpDown;
    tsCols: TTabSheet;
    gbAvailableCols: TmmGroupBox;
    lbAvailable: TNWListBox;
    gbShowCols: TmmGroupBox;
    lbShow: TNWListBox;
    tsAttributes: TTabSheet;
    gbFieldDef: TmmGroupBox;
    lbAttribut: TNWListBox;
    rgFloat: TmmRadioGroup;
    rgDate: TmmRadioGroup;
    rgInteger: TmmRadioGroup;
    rgTime: TmmRadioGroup;
    rgAlignment: TmmRadioGroup;
    rgDateTime: TmmRadioGroup;
    bMoveTo_lbShow: TmmSpeedButton;
    bMoveTo_lbAvailable: TmmSpeedButton;
    bMoveAllTo_lbShow: TmmSpeedButton;
    bMoveAllTo_lbAvailable: TmmSpeedButton;
    bMoveUp: TmmSpeedButton;
    bMoveDown: TmmSpeedButton;

    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SchriftLabelClick(Sender: TObject);
    procedure ColorPanelClick(Sender: TObject);
    procedure SH_ColsDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure SH_ColLBDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure lbShowDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure lbAttributClick(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure rgAlignmentClick(Sender: TObject);
    procedure rgIntegerClick(Sender: TObject);
    procedure rgFloatClick(Sender: TObject);
    procedure rgTimeClick(Sender: TObject);
    procedure rgDateClick(Sender: TObject);
    procedure rgDateTimeClick(Sender: TObject);
    procedure mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
    procedure acMoveTo_lbShowExecute(Sender: TObject);
    procedure acMoveTo_lbAvailableExecute(Sender: TObject);
    procedure acMoveAllTo_lbShowExecute(Sender: TObject);
    procedure acMoveAllTo_lbAvailableExecute(Sender: TObject);
    procedure lbAvailableDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure lbShowDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure MoveBtnClick(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    FWork,
      FGridInfo: tGridInfoRecordT;
    FAlterColors: boolean;
    FAlterColumnFormat: boolean;
    FAlterColumnPosition: boolean;
    FAlterFixedCols: boolean;
    FAlterFonts: boolean;
    FAlterGridOptions: boolean;
    FAlterVisibleCols: boolean;
    procedure SetLabelPos(l: TLabel);
    function GetRecordPos(aID: integer): integer;
    procedure MoveSelectedItem(aOldPos, aDropPos: integer);
    procedure Update_lbAttribut;
    procedure Update_VisibleColumns;
    procedure InitFormatRadioGroups;
    procedure SetEditControls;
    procedure GetEditControls;
    procedure MoveAllItems(aSource, aTarget: TNWListBox);
    procedure EnableActions;
  public
    property AlterColors: boolean read FAlterColors write FAlterColors;
    property AlterColumnFormat: boolean read FAlterColumnFormat write FAlterColumnFormat;
    property AlterColumnPosition: boolean read FAlterColumnPosition write FAlterColumnPosition;
    property AlterGridOptions: boolean read FAlterGridOptions write FAlterGridOptions;
    property AlterFixedCols: boolean read FAlterFixedCols write FAlterFixedCols;
    property AlterFonts: boolean read FAlterFonts write FAlterFonts;
    property AlterVisibleCols: boolean read FAlterVisibleCols write FAlterVisibleCols;
    property GridInfo: tGridInfoRecordT read FGridInfo write FGridInfo;
  end;                                  //TGridDefDlg

var
  mmGridPosEditor: TmmGridPosEditor;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, MMHtmlHelp;

{$R *.DFM}

procedure TmmGridPosEditor.FormCreate(Sender: TObject);
begin
  HelpContext := GetHelpContext('globale-fenster-und-funktionen\DLG_Tabelleneigenschaften.htm');
  PageControl1.ActivePage := tsCols;
  InitFormatRadioGroups;
end;                                    //procedure TGridDefDlg.FormCreate
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.SetLabelPos(l: TLabel);
begin
  l.Left := (l.Parent.Width - l.Width) div 2;
  l.Top := (l.Parent.Height - l.Height) div 2;
end;                                    //procedure TmmGridPosEditor.SetLabelPos
//-----------------------------------------------------------------------------
function TmmGridPosEditor.GetRecordPos(aID: integer): integer;
var
  i: integer;
begin
  result := -1;
  for i := 0 to high(FWork.Columns) do
    if FWork.Columns[i].ID = aID then begin
      result := i;
      break;
    end;                                //if FWork.Columns[i].ID = aID then
end;                                    //function TmmGridPosEditor.GetRecordPos
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.Update_lbAttribut;
begin
  lbAttribut.Items.Clear;
  lbAttribut.Items.AddStrings(lbShow.Items);
  if lbAttribut.Items.Count > 0 then begin
    lbAttribut.ItemIndex := 0;
    lbAttributClick(nil);
  end;                                  //if lbAttribut.Items.Count > 0 then
end;                                    //procedure TmmGridPosEditor.Update_lbAttribut
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.Update_VisibleColumns;
var
  i, j,
  xID: integer;
begin
  for i := 0 to high(FWork.Columns) do
    FWork.Columns[i].Visible := false;

  for i := 0 to lbShow.Items.Count - 1 do begin
    xID := lbShow.GetItemIDAtPos(i);
    j := GetRecordPos(xID);
    FWork.Columns[j].Visible := true;
  end;                                  //for i := 0 to lbShow.Items.Count -1 do

  Update_lbAttribut;
end;                                    //procedure TmmGridPosEditor.Update_VisibleColumns
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.InitFormatRadioGroups;
begin
  rgTime.Items.Clear;
  rgTime.Items.Add(formatdatetime('hh:nn', time));
  rgTime.Items.Add(formatdatetime('hh:nn:ss', time));

  rgDate.Items.Clear;
  rgDate.Items.Add(formatdatetime('d/m/yy', date));
  rgDate.Items.Add(formatdatetime('dd/mm/yy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mm/yy', date));
  rgDate.Items.Add(formatdatetime('d/m/yyyy', date));
  rgDate.Items.Add(formatdatetime('dd/mm/yyyy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mm/yyyy', date));
  rgDate.Items.Add(formatdatetime('dd/mmm/yyyy', date));
  rgDate.Items.Add(formatdatetime('ddd dd/mmm/yyyy', date));

  rgDateTime.Items.Clear;
  rgDateTime.Items.Add(formatdatetime('d/m/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mm/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mm/yy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('d/m/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('dd/mmm/yyyy hh:nn', now));
  rgDateTime.Items.Add(formatdatetime('ddd dd/mmm/yyyy hh:nn', now));
end;                                    //procedure TmmGridPosEditor.InitFormatRadioGroups
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.SetEditControls;
var
  i: integer;
begin
  FWork := GridInfo;

  laTitleFont.Font.Name := FWork.TitleFont.Name;
  laTitleFont.Font.Size := FWork.TitleFont.Size;
  laTitleFont.Font.Color := FWork.TitleFont.Color;
  laTitleFont.Font.Style := FWork.TitleFont.Style;

  laFont.Font.Name := FWork.Font.Name;
  laFont.Font.Size := FWork.Font.Size;
  laFont.Font.Color := FWork.Font.Color;
  laFont.Font.Style := FWork.Font.Style;

  paTitleColor.Color := FWork.TitleColor;
  paColor.Color := FWork.Color;

  SetLabelPos(laTitleFont);
  SetLabelPos(laFont);

  cbRowLines.Checked := dgRowLines in FWork.Options;
  cbColLines.Checked := dgColLines in FWork.Options;
  cbIndicator.Checked := dgIndicator in FWork.Options;
  cbRowSelect.Checked := dgRowSelect in FWork.Options;
  cbAlwaysShowSelection.Checked := dgAlwaysShowSelection in FWork.Options;
  cbTrailingEllipsis.Checked := dgTrailingEllipsis in FWork.Options;
  cbTitleAlignment.Checked := FWork.TitleAlignment = taCenter;

  lbShow.Items.Clear;
  lbAvailable.Items.Clear;

  for i := 0 to high(FWork.Columns) do
    if FWork.Columns[i].Tag <> cFieldHideID then begin
      if FWork.Columns[i].Visible then lbShow.AddObj(FWork.Columns[i].DisplayLabel, FWork.Columns[i].ID)
      else lbAvailable.AddObj(FWork.Columns[i].DisplayLabel, FWork.Columns[i].ID);
    end;                                //if FWork.Columns[i].Tag <> cFieldHideID then

  gbGridSettings.Enabled := AlterGridOptions;

  udFixedCols.Position := FWork.FixedCols;

  udFixedCols.Enabled := AlterFixedCols;
  lbShow.Enabled := AlterVisibleCols or AlterColumnPosition;
  lbAvailable.Enabled := AlterVisibleCols;

  rgAlignment.Enabled := AlterColumnFormat;
  rgInteger.Enabled := AlterColumnFormat;
  rgFloat.Enabled := AlterColumnFormat;
  rgTime.Enabled := AlterColumnFormat;
  rgDate.Enabled := AlterColumnFormat;
  rgDateTime.Enabled := AlterColumnFormat;

  if not AlterColors then paTitleColor.Hint := '';
  if not AlterColors then paColor.Hint := '';

  if not AlterFonts then laTitleFont.Hint := '';
  if not AlterFonts then laFont.Hint := '';

  EnableActions;
  Update_lbAttribut;
end;                                    //procedure TmmGridPosEditor.SetEditControls
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.GetEditControls;
var
  xIndex,
  xID,
  i, j: integer;
begin
  FWork.TitleFont.Name := laTitleFont.Font.Name;
  FWork.TitleFont.Size := laTitleFont.Font.Size;
  FWork.TitleFont.Color := laTitleFont.Font.Color;
  FWork.TitleFont.Style := laTitleFont.Font.Style;

  FWork.Font.Name := laFont.Font.Name;
  FWork.Font.Size := laFont.Font.Size;
  FWork.Font.Color := laFont.Font.Color;
  FWork.Font.Style := laFont.Font.Style;

  FWork.TitleColor := paTitleColor.Color;
  FWork.Color := paColor.Color;

  FWork.Options := FWork.Options - [dgRowLines, dgColLines, dgIndicator, dgRowSelect, dgAlwaysShowSelection, dgTrailingEllipsis];
  if cbRowLines.Checked then FWork.Options := FWork.Options + [dgRowLines];
  if cbColLines.Checked then FWork.Options := FWork.Options + [dgColLines];
  if cbIndicator.Checked then FWork.Options := FWork.Options + [dgIndicator];
  if cbRowSelect.Checked then FWork.Options := FWork.Options + [dgRowSelect];
  if cbAlwaysShowSelection.Checked then FWork.Options := FWork.Options + [dgAlwaysShowSelection];
  if cbTrailingEllipsis.Checked then FWork.Options := FWork.Options + [dgTrailingEllipsis];

  if cbTitleAlignment.Checked then FWork.TitleAlignment := taCenter
  else FWork.TitleAlignment := taLeftJustify;

  FWork.FixedCols := udFixedCols.Position;

 //1st set index for visible cols
  for i := 0 to lbShow.Items.Count - 1 do begin
    xID := lbShow.GetItemIDAtPos(i);
    j := GetRecordPos(xID);
    FWork.Columns[j].Index := i;
  end;                                  //for i := 0 to lbColPos.Items.Count -1 do

 //2nd set index for non-visible cols
  xIndex := i + 1;
  for i := 0 to lbAvailable.Items.Count - 1 do begin
    xID := lbAvailable.GetItemIDAtPos(i);
    j := GetRecordPos(xID);
    FWork.Columns[j].Index := i + xIndex;
  end;                                  //for i := 0 to lbAvailable.Items.Count -1 do

  GridInfo := FWork;
  FWork.Columns := nil;
end;                                    //procedure TmmGridPosEditor.GetEditControls
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.FormShow(Sender: TObject);
begin
  SetEditControls;
end;                                    //procedure TGridDefDlg.FormShow
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.SchriftLabelClick(Sender: TObject);
begin
  if not AlterFonts then exit;
  FontDialog1.Font := TLabel(Sender).Font;
  if FontDialog1.Execute then
    TLabel(Sender).Font := FontDialog1.Font;
  SetLabelPos(TLabel(Sender));
end;                                    //procedure TGridDefDlg.SchriftLabelClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.ColorPanelClick(Sender: TObject);
begin
  if not AlterColors then exit;
  ColorDialog1.Color := TPanel(Sender).Color;
  if ColorDialog1.Execute then
    TPanel(Sender).Color := ColorDialog1.Color;
end;                                    //procedure TGridDefDlg.ColorPanelClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.lbAttributClick(Sender: TObject);
var
  xID, i,
  rgIndex: integer;
begin
  xID := lbAttribut.GetItemID;
  i := GetRecordPos(xID);

  rgInteger.Visible := FWork.Columns[i].DataType in [ftSmallint, ftInteger, ftWord, ftAutoInc, ftLargeInt];
  rgFloat.Visible := FWork.Columns[i].DataType = ftFloat;
  rgTime.Visible := FWork.Columns[i].DataType = ftTime;
  rgDate.Visible := FWork.Columns[i].DataType = ftDate;
  rgDateTime.Visible := FWork.Columns[i].DataType = ftDateTime;

  case FWork.Columns[i].ALignment of
    taLeftJustify: rgAlignment.ItemIndex := 0;
    taCenter: rgAlignment.ItemIndex := 1;
    taRightJustify: rgAlignment.ItemIndex := 2;
  end;                                  //case FWork.Columns[i].ALignment of

  if rgInteger.Visible then begin
    rgIndex := 0;
    if FWork.Columns[i].DisplayFormat = '0' then rgIndex := 1;
    rgInteger.ItemIndex := rgIndex;
  end;                                  //if rgInteger.Visible then

  if rgFloat.Visible then begin
    rgIndex := 0;
    if FWork.Columns[i].DisplayFormat = ',0.0' then rgIndex := 1;
    if FWork.Columns[i].DisplayFormat = ',0.00' then rgIndex := 2;
    if FWork.Columns[i].DisplayFormat = ',0.000' then rgIndex := 3;
    if FWork.Columns[i].DisplayFormat = '0' then rgIndex := 4;
    if FWork.Columns[i].DisplayFormat = '0.0' then rgIndex := 5;
    if FWork.Columns[i].DisplayFormat = '0.00' then rgIndex := 6;
    if FWork.Columns[i].DisplayFormat = '0.000' then rgIndex := 7;
    rgFloat.ItemIndex := rgIndex;
  end;                                  //if rgFloat.Visible then

  if rgTime.Visible then begin
    rgIndex := 0;
    if FWork.Columns[i].DisplayFormat = 'hh:nn:ss' then rgIndex := 1;
    rgTime.ItemIndex := rgIndex;
  end;                                  //if rgTime.Visible then

  if rgDate.Visible then begin
    rgIndex := 0;
    if FWork.Columns[i].DisplayFormat = 'dd/mm/yy' then rgIndex := 1;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mm/yy' then rgIndex := 2;
    if FWork.Columns[i].DisplayFormat = 'd/m/yyyy' then rgIndex := 3;
    if FWork.Columns[i].DisplayFormat = 'dd/mm/yyyy' then rgIndex := 4;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mm/yyyy' then rgIndex := 5;
    if FWork.Columns[i].DisplayFormat = 'dd/mmm/yyyy' then rgIndex := 6;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mmm/yyyy' then rgIndex := 7;
    rgDate.ItemIndex := rgIndex;
  end;                                  //if rgDate.Visible then

  if rgDateTime.Visible then begin
    rgIndex := 0;
    if FWork.Columns[i].DisplayFormat = 'dd/mm/yy hh:nn' then rgIndex := 1;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mm/yy hh:nn' then rgIndex := 2;
    if FWork.Columns[i].DisplayFormat = 'd/m/yyyy hh:nn' then rgIndex := 3;
    if FWork.Columns[i].DisplayFormat = 'dd/mm/yyyy hh:nn' then rgIndex := 4;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mm/yyyy hh:nn' then rgIndex := 5;
    if FWork.Columns[i].DisplayFormat = 'dd/mmm/yyyy hh:nn' then rgIndex := 6;
    if FWork.Columns[i].DisplayFormat = 'ddd dd/mmm/yyyy hh:nn' then rgIndex := 7;
    rgDateTime.ItemIndex := rgIndex;
  end;                                  //if rgDateTime.Visible then
end;                                    //procedure TmmGridPosEditor.lbAttributClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.MoveSelectedItem(aOldPos, aDropPos: integer);
var
  i: integer;
begin
  lbShow.Items.Move(aOldPos, aDropPos);

  for i := 0 to lbShow.Items.Count - 1 do
    lbShow.Selected[i] := false;
  lbShow.Selected[aDropPos] := true;

  Update_lbAttribut;
end;                                    //procedure TmmGridPosEditor.AfterMoveSelectedItem
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.SH_ColsDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  xDropPos: integer;
  i, k: integer;
  b: boolean;
  s: string;
begin
 //end of dragging
  if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then begin
    for i := 0 to TNWListBox(Source).Items.Count - 1 do begin
      b := TNWListBox(Source).Selected[i];
      s := TNWListBox(Source).Items[i];
      k := TNWListBox(Source).GetItemIDAtPos(i);

      if b then TNWListBox(Sender).AddObj(s, k);
    end;                                //for i := 0 to TNWListBox(Source).Items.Count -1 do

    while TNWListBox(Source).SelCount > 0 do
      for i := TNWListBox(Source).Items.Count - 1 downto 0 do begin
        b := TNWListBox(Source).Selected[i];
        if b then TNWListBox(Source).Items.Delete(i);
        if TNWListBox(Source).SelCount = 0 then break;
      end;                              //for i := TNWListBox(Source).Items.Count -1 downto 0 do

    EnableActions;
    Update_lbAttribut;
  end                                   //if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then
  else begin
    if (Sender = Source) and ((Sender as TNWListBox) = lbShow) then begin
   //only move 1 selected item (the first one)
   //there is min. one selected item
      xDropPos := lbShow.ItemAtPos(Point(X, Y), false);

      for i := 0 to lbShow.Items.Count - 1 do
        if lbShow.Selected[i] then Break;

      MoveSelectedItem(i, xDropPos);
    end;                                //if (Sender = Source) and ((Sender as TNWListBox) = lbShow) then
  end;                                  //else if (Sender is TNWListBox) and (Source is TNWListBox) and (Sender <> Source) then

  Update_VisibleColumns;
end;                                    //procedure TmmGridPosEditor.SH_ColsDragDrop
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.SH_ColLBDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source <> Sender;
end;                                    //procedure TGridDefDlg.SH_ColLBDragOver
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.lbShowDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  Offset: integer;
  IsSelected: boolean;

begin
  with (Control as TNWListBox).Canvas do begin
    IsSelected := odSelected in state;
    FillRect(Rect);
    Offset := 2;
    if index <= (udFixedCols.Position - 1) then Font.Color := clBlue
    else Font.Color := clWindowText;

    if not AlterColumnPosition then Font.Color := clGray;

    if IsSelected then Font.Color := clHighlightText;
    TextOut(Rect.Left + Offset, Rect.Top, (Control as TNWListBox).Items[Index]);
  end;                                  //with (Control as TNWListBox).Canvas do
end;                                    //procedure TmmGridPosEditor.lbShowDrawItem
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acOKExecute(Sender: TObject);
begin
  GetEditControls;

  Close;
  ModalResult := mrOK;
end;                                    //procedure TmmGridPosEditor.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acCancelExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;                                    //procedure TmmGridPosEditor.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgAlignmentClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgAlignment.ItemIndex of
    0: FWork.Columns[i].ALignment := taLeftJustify;
    1: FWork.Columns[i].ALignment := taCenter;
    2: FWork.Columns[i].ALignment := taRightJustify;
  end;                                  //case rgAlignment.ItemIndex of
end;                                    //procedure TmmGridPosEditor.rgAlignmentClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgIntegerClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgInteger.ItemIndex of
    0: FWork.Columns[i].DisplayFormat := ',0';
    1: FWork.Columns[i].DisplayFormat := '0';
  end;                                  //case rgInteger.ItemIndex 0f
end;                                    //procedure TmmGridPosEditor.rgIntegerClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgFloatClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgFloat.ItemIndex of
    0: FWork.Columns[i].DisplayFormat := ',0';
    1: FWork.Columns[i].DisplayFormat := ',0.0';
    2: FWork.Columns[i].DisplayFormat := ',0.00';
    3: FWork.Columns[i].DisplayFormat := ',0.000';
    4: FWork.Columns[i].DisplayFormat := '0';
    5: FWork.Columns[i].DisplayFormat := '0.0';
    6: FWork.Columns[i].DisplayFormat := '0.00';
    7: FWork.Columns[i].DisplayFormat := '0.000';
  end;                                  //case rgFloat.ItemIndex 0f
end;                                    //procedure TmmGridPosEditor.rgFloatClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgTimeClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgTime.ItemIndex of
    0: FWork.Columns[i].DisplayFormat := 'hh:nn';
    1: FWork.Columns[i].DisplayFormat := 'hh:nn:ss';
  end;                                  //case rgTime.ItemIndex 0f
end;                                    //procedure TmmGridPosEditor.rgTimeClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgDateClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgDate.ItemIndex of
    0: FWork.Columns[i].DisplayFormat := 'd/m/yy';
    1: FWork.Columns[i].DisplayFormat := 'dd/mm/yy';
    2: FWork.Columns[i].DisplayFormat := 'ddd dd/mm/yy';
    3: FWork.Columns[i].DisplayFormat := 'd/m/yyyy';
    4: FWork.Columns[i].DisplayFormat := 'dd/mm/yyyy';
    5: FWork.Columns[i].DisplayFormat := 'ddd dd/mm/yyyy';
    6: FWork.Columns[i].DisplayFormat := 'dd/mmm/yyyy';
    7: FWork.Columns[i].DisplayFormat := 'ddd dd/mmm/yyyy';
  end;                                  //case rgDate.ItemIndex 0f
end;                                    //procedure TmmGridPosEditor.rgDateClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.rgDateTimeClick(Sender: TObject);
var
  i: integer;

begin
  i := GetRecordPos(lbAttribut.GetItemID);
  case rgDateTime.ItemIndex of
    0: FWork.Columns[i].DisplayFormat := 'd/m/yy hh:nn';
    1: FWork.Columns[i].DisplayFormat := 'dd/mm/yy hh:nn';
    2: FWork.Columns[i].DisplayFormat := 'ddd dd/mm/yy hh:nn';
    3: FWork.Columns[i].DisplayFormat := 'd/m/yyyy hh:nn';
    4: FWork.Columns[i].DisplayFormat := 'dd/mm/yyyy hh:nn';
    5: FWork.Columns[i].DisplayFormat := 'ddd dd/mm/yyyy hh:nn';
    6: FWork.Columns[i].DisplayFormat := 'dd/mmm/yyyy hh:nn';
    7: FWork.Columns[i].DisplayFormat := 'ddd dd/mmm/yyyy hh:nn';
  end;                                  //case rgDateTime.ItemIndex 0f
end;                                    //procedure TmmGridPosEditor.rgDateTimeClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.mmTranslator1AfterTranslate(translator: TIvCustomTranslator);
begin
  SetLabelPos(laTitleFont);
  SetLabelPos(laFont);
end;                                    //procedure TmmGridPosEditor.IvExtendedTranslator1AfterTranslate
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acMoveTo_lbShowExecute(Sender: TObject);
begin
  SH_ColsDragDrop(lbShow, lbAvailable, 0, 0);
end;                                    //procedure TmmGridPosEditor.acMoveTo_lbShowExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acMoveTo_lbAvailableExecute(Sender: TObject);
begin
  SH_ColsDragDrop(lbAvailable, lbShow, 0, 0);
end;                                    //procedure TmmGridPosEditor.acMoveTo_lbAvailableExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acMoveAllTo_lbShowExecute(Sender: TObject);
begin
  MoveAllItems(lbAvailable, lbShow);
end;                                    //procedure TmmGridPosEditor.acMoveAllTo_lbShowExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acMoveAllTo_lbAvailableExecute(Sender: TObject);
begin
  MoveAllItems(lbShow, lbAvailable);
end;                                    //procedure TmmGridPosEditor.acMoveAllTo_lbAvailableExecute
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.lbAvailableDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := Source <> Sender;
end;                                    //procedure TmmGridPosEditor.lbAvailableDragOver
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.lbShowDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  xDropPos: integer;

begin
  if Source = lbShow then begin
    xDropPos := lbShow.ItemAtPos(Point(X, Y), false);
    Accept := (xDropPos > -1) and
      (xDropPos <> lbShow.ItemIndex) and
      (xDropPos < lbShow.Items.Count) and
      (not lbShow.Selected[xDropPos]) and
      AlterColumnPosition;
  end                                   //if Source = lbShow then
  else Accept := Source = lbAvailable;
end;                                    //procedure TmmGridPosEditor.lbShowDragOver
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.MoveBtnClick(Sender: TObject);
var
  xFirst,
    xDropPos,
    i: integer;

begin
 //only move 1 selected item (the first one)
  xFirst := -1;
  for i := 0 to lbShow.Items.Count - 1 do
    if lbShow.Selected[i] then begin
      xFirst := i;
      Break;
    end;                                //if lbShow.Selected[i] then

  if xFirst = -1 then exit;

  xDropPos := xFirst;
  if Sender = bMoveUp then begin
    if xFirst > 0 then xDropPos := xFirst - 1;
  end                                   //if Sender = bMoveUp then
  else begin
    if xFirst < (lbShow.Items.Count - 1) then xDropPos := xFirst + 1;
  end;                                  //else if Sender = bMoveUp then

  MoveSelectedItem(xFirst, xDropPos);
end;                                    //procedure TmmGridPosEditor.MoveBtnClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.MoveAllItems(aSource, aTarget: TNWListBox);
var
  xStart,
    i: integer;

begin
 //prevent moving all cols to lbAvailable, there should allways be one visible col in the grid
  aSource.Selected[0] := false;
  if aSource = lbShow then xStart := 1
  else xStart := 0;

  aSource.Items.BeginUpdate;
  for i := xStart to aSource.Items.Count - 1 do
    aSource.Selected[i] := true;
  aSource.Items.EndUpdate;

  SH_ColsDragDrop(aTarget, aSource, 0, 0);
end;                                    //procedure TmmGridPosEditor.bMoveAllTo_lbShowClick
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.EnableActions;

//-----------
  procedure SetSelectedItem(aListBox: TNWListBox);
  var
    i: integer;

  begin
    if aListBox.Items.Count > 0 then begin
      i := aListBox.ItemIndex;
      if i > -1 then aListBox.Selected[i] := true
      else aListBox.Selected[0] := true;
    end;                                //if aListBox.Items.Count > 0 then
  end;                                  //procedure SetSelectedItem
//-----------

begin                                   //main
  acOK.Enabled := lbShow.Items.Count > 0;

  acMoveTo_lbAvailable.Enabled := (lbShow.Items.Count > 1) and AlterVisibleCols;
  acMoveAllTo_lbAvailable.Enabled := acMoveTo_lbAvailable.Enabled;
  acMoveTo_lbShow.Enabled := (lbAvailable.Items.Count > 0) and AlterVisibleCols;
  acMoveAllTo_lbShow.Enabled := acMoveTo_lbShow.Enabled;

  bMoveUp.Enabled := (lbShow.Items.Count > 1) and AlterColumnPosition;
  bMoveDown.Enabled := bMoveUp.Enabled;
  tsAttributes.TabVisible := acOK.Enabled;

  SetSelectedItem(lbAvailable);
  SetSelectedItem(lbShow);
end;                                    //procedure TmmGridPosEditor.EnableActions
//-----------------------------------------------------------------------------
procedure TmmGridPosEditor.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;                                    //procedure TmmGridPosEditor.acHelpExecute
//-----------------------------------------------------------------------------

end. //mmGridPosEdit.pas

