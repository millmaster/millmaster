(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AssignAnimationForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 11.05.2000  1.01  Mg  | Simulation inserted
| 22.01.2001  1.02  Nue | ProgressBar added.
| 22.11.2002        LOK | Progressbar entfernt
|=============================================================================*)
unit AssignAnimationForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, ComCtrls, mmAnimate, IvDictio, IvMulti, IvEMulti,
  mmTranslator, StdCtrls, mmLabel, ExtCtrls, mmPanel, Buttons, mmBitBtn,
  AssignComp, mmTimer;
const
  cAssignAvi = 'Assign.avi';

  cAssignTitle =
  '(*)Reiniger Einstellungen werden zugeordnet.'; //ivlm

  cCloseTitle =
  '(*)Datenerfassung der Partie wird gestoppt.'; //ivlm

  cAssignSuccessfulMsg =
  '(*)Die Zuordnung der Reiniger Einstellungen ist erfolgreich abgeschlossen.'; // ivlm

  cCloseSuccessfulMsg =
  '(*)Datenerfassung der Partie wurde erfolgreich gestoppt.'; //ivlm

type
  TAnimationTyp = ( atAssign, atClose );
  TAssignAnimation = class(TmmDialog)
    mmTranslator1: TmmTranslator;
    mmPanel1: TmmPanel;
    lMsg: TmmLabel;
    bOK: TmmBitBtn;
    mmTimer1: TmmTimer;
    mmPanel2: TmmPanel;
    mmAnimate1: TmmAnimate;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bOKClick(Sender: TObject);
    procedure mmTimer1Timer(Sender: TObject);
  private
    fSimulation   : boolean;
    fAnimationTyp : TAnimationTyp;
    procedure SetAnimationTyp ( aTyp : TAnimationTyp );
  public
    procedure WndProc(var msg : TMessage);override;
    constructor Create(aOwner: Tcomponent); override;
    procedure  StoppAnimation;
    property   AnimationTyp : TAnimationTyp read fAnimationTyp write SetAnimationTyp;
    property   Simulation : boolean read fSimulation write fSimulation;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
const
  cFormCloseTimeout = 1 * 60 * 1000;
//TestNue  cFormCloseTimeout = 30 * 1000;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.StoppAnimation;
  begin
    mmAnimate1.Active := false;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.FormShow(Sender: TObject);
  begin
    inherited;
    if Simulation then
      mmTimer1.Interval := 20;
    mmAnimate1.Active := true;
    bOK.Enabled       := false;
    lMsg.Caption      := '';
    mmTimer1.Enabled  := false;
    screen.Cursor := crHourGlass;
  end;
//------------------------------------------------------------------------------
  constructor TAssignAnimation.Create(aOwner: Tcomponent);
  begin
    inherited Create ( aOwner );
    fAnimationTyp     := atAssign;
    Caption           := cAssignTitle;
    mmTimer1.Interval := cFormCloseTimeout;
    mmTimer1.Enabled  := false;
    Height            := 377;
    Width             := 500;
    fSimulation       := false;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.SetAnimationTyp ( aTyp : TAnimationTyp );
  begin
    fAnimationTyp := aTyp;

    if not(csDesigning in ComponentState) then
    try
      case fAnimationTyp of
      atAssign : begin
          Caption := Translate ( cAssignTitle );
          mmAnimate1.FileName := ExtractFilePath ( Application.ExeName ) + cAssignAvi;
        end;
      atClose  : begin
          Caption := Translate ( cCloseTitle );
          mmAnimate1.FileName := ExtractFilePath ( Application.ExeName ) + cAssignAvi;
        end;
      else
        Caption := Translate ( cAssignTitle );
        mmAnimate1.FileName := ExtractFilePath ( Application.ExeName ) + cAssignAvi;
      end;
    except
      on e:Exception do begin
        ShowMessage(e.Message);
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.WndProc(var msg : TMessage);
  begin
    if ( msg.msg = RegisterWindowMessage ( ASSIGN_REG_STR ) ) then begin
      case msg.wParam of

        WM_ASSIGN_DONE : begin
            lMsg.Caption := Translate ( cAssignSuccessfulMsg );
            bOK.Enabled  := true;
            bOK.SetFocus;
            StoppAnimation;
            mmTimer1.Enabled  := true;
            screen.Cursor := crDefault;
          end;
        WM_CLOSE_DONE  : begin
            lMsg.Caption := Translate ( cCloseSuccessfulMsg );
            bOK.Enabled  := true;
            bOK.SetFocus;
            StoppAnimation;
            mmTimer1.Enabled  := true;
            screen.Cursor := crDefault;
          end;
      else
      end;
    end else begin // Handle all messages with the default handler
      inherited WndProc ( msg );
    end;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.FormClose(Sender: TObject; var Action: TCloseAction);
  begin
  inherited;
   Action := caHide;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.bOKClick(Sender: TObject);
  begin
    inherited;
    Close;
  end;
//------------------------------------------------------------------------------
  procedure TAssignAnimation.mmTimer1Timer(Sender: TObject);
  begin
    inherited;
//NUE1    mmTimer1.Enabled  := false;
//NUE1    Close;
  end;

//------------------------------------------------------------------------------
end.
