inherited mmSelectProfileEditor: TmmSelectProfileEditor
  Left = 492
  Top = 283
  Width = 386
  Height = 315
  BorderIcons = [biSystemMenu]
  Caption = '(40)Einstellungs-Profil laden'
  Constraints.MinWidth = 253
  ParentFont = False
  Font.Color = clBlack
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 378
    Height = 258
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object PageControl1: TmmPageControl
      Left = 5
      Top = 5
      Width = 368
      Height = 248
      ActivePage = tsUser
      Align = alClient
      HotTrack = True
      TabOrder = 0
      OnChange = PageControl1Change
      object tsUser: TTabSheet
        Caption = ' '
        object lbProfiles: TNWListBox
          Left = 0
          Top = 0
          Width = 360
          Height = 220
          Align = alClient
          Columns = 2
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 15
          ParentFont = False
          Sorted = True
          Style = lbOwnerDrawFixed
          TabOrder = 0
          OnDblClick = acOKExecute
          OnDrawItem = lbProfilesDrawItem
        end
      end
      object tsAdmin: TTabSheet
        Caption = '(30)Vorlagen'
        ImageIndex = 1
        object lbAdmin: TNWListBox
          Left = 0
          Top = 0
          Width = 360
          Height = 220
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ItemHeight = 15
          ParentFont = False
          Sorted = True
          Style = lbOwnerDrawFixed
          TabOrder = 0
          OnDblClick = acOKExecute
          OnDrawItem = lbProfilesDrawItem
        end
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 258
    Width = 378
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 138
      Top = 1
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 218
      Top = 1
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 298
      Top = 1
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 128
    Top = 44
    TargetsData = (
      1
      5
      (
        ''
        'Hint'
        0)
      (
        ''
        'Caption'
        0)
      (
        ''
        'Text'
        0)
      (
        ''
        'Items'
        0)
      (
        ''
        'Lines'
        0))
  end
  object ActionList1: TmmActionList
    Left = 95
    Top = 43
    object acOK: TAction
      Caption = '(9)OK'
      Hint = '(*)Auswahl uebernehmen'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(9)&Hilfe'
      Hint = '(*)Hilfetext anzeigen...'
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
end
