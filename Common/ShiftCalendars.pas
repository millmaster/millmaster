(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: ShiftCalendars.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Komponenten zur Bearbeitung von
|                 - Schichtkalendern
|                 - Std.Schichten
|                 - Schichtmustern
|                 - Betriebskalendern
| Info..........: -
| Develop.system: Windows NT 4.0 SP5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.98 | 0.00 | PW   | Datei erstellt
| 17.01.99 | 0.01 | PW   | New Event "OnChange" for TShiftPatternGrid
| 22.01.99 | 0.02 | PW   | Added "Multilizer" functionality
| 15.09.99 | 0.03 | PW   | FTranslator removed from components
|          |      |      | TmmCustomCalendar, TmmCustomShiftcalendar => Base Class for ShiftCalendar Components
|          |      |      | Component TAppendDefaultShifts to append default shifts
| 24.09.99 | 0.04 | PW   | rename c_shift_dauer => c_shift_length
| 16.02.00 | 0.05 |Mg/SDO| New by all create of quereies: -> xQuery.SessionName := FDatabase.Session.SessionName
|                        | New parameter type in func. GetShiftData(aDB: TmmDataBase; old aDBName: String
|                        | New TmmQuery.create with NIL; old with application
| 02.01.00 | 0.04 | SDo  | TAppendDefaultShifts.Execute uebrarbeitet
|                        | ( -> gab DB Errors beim Erstellen neuer Eintraege ende Monat)
| 15.03.00 | 1.00 | Wss  | ReadShiftData: if no team set default to ' ' instead of ''
| 22.10.02 |      | LOK  | Umbau ADO
|=========================================================================================*)

unit ShiftCalendars;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,Grids,DB,
 IvDictio,IvMulti,IvEMulti,mmCommonLib,mmADOConnection, mmADODataSet, mmADOCommand,
 RzcsIntf, LoepfeGlobal;

const
 cCRLF                  = #13#10;
 cEpsilon               = 1e-9;
 cMinShiftsPerDay       = 1;
 cMaxShiftsPerDay       = 5;
 cDaysPerWeek           = 7; //bible
 cMonthsPerYear         = 12;
 cMaxDaysPerMonth       = 31;
 cMaxShiftPatternPeriod = 60;
 cMinPerHour            = 60;
 cHoursPerDay           = 24;
 cMinPerDay             = cMinPerHour *cHoursPerDay;
 cTimeSeparator         = ':';  //always use the same sp.char indpendent from the language settings

 cMinShiftDuration      = 30; //minutes
 cMaxShiftDuration      = cMinPerDay;

 cErrorFirstShiftEmpty  = 1;
 cErrorShiftOverlapping = 2;
 cErrorShiftGap         = 3;
 cErrorMinShiftDuration = 4;
 cErrorMaxShiftDuration = 5;
 cErrorTeamNotFound     = 6;

 clMarkWeekEnd          = $00E1FFFF;
 clLightGray1           = $00E0E0E0;
 clLightGray2           = $00E5E5E5;
 clLightGray3           = $00D4D4D4;
 clLightYellow          = $00E8FCFF;

type
 s1                     = string[1];
 s5                     = string[5];
 eWeekendDayT           = (wdSunday,wdMonday,wdTuesday,wdWednesday,wdThursday,wdFriday,wdSaturday);
 eMarkFieldT            = (mfTimeDifferent,mfTeamDifferent);
 eScrollDirectionT      = (sdPrev,sdNone,sdNext);

 sWeekendDayT = set of eWeekendDayT;
 sMarkFieldT  = set of eMarkFieldT;

 rShiftT = record
  StartTime: array[1..cMaxDaysPerMonth,1..cMaxShiftsPerDay] of s5;
  Team:      array[1..cMaxDaysPerMonth,1..cMaxShiftsPerDay] of s1;
  Duration:  array[1..cMaxDaysPerMonth,1..cMaxShiftsPerDay] of integer;
 end; //rShiftT

 rCellInfoT = record
  cString : string;
  bColor,
  fColor  : TColor;
  fStyle  : TFontStyles;
 end; //rCellInfoT

 aOffDayT = array[1..cMaxDaysPerMonth] of boolean;

 rDefaultShiftT = record
  StartTime: array[1..cDaysPerWeek,1..cMaxShiftsPerDay] of s5;
 end; //rDefaultShiftT

 rShiftPatternT = record
  Period: integer;
  Team: array[1..cMaxShiftPatternPeriod,1..cMaxShiftsPerDay] of s1;
 end; //rShiftPatternT

 rShiftCheckT = record
  Year,
  Month,
  Day,
  Shift     : word;
  StartTime : double;
  Duration  : integer;
  ShiftID   : longint;
 end; //rShiftCheckT

 TCalendarMessages = class(TPersistent)
  private
   FControl: TControl;
   FDateStr: string;
   FShift1Str: string;
   FShift2Str: string;
   FShift3Str: string;
   FShift4Str: string;
   FShift5Str: string;
   FChangesStr: string;
   FPatternErrMsg: string;
   FDaysInPastErrorMsg: string;
   FDateShiftStr: string;
   FFirstShiftEmptyErrorMsg: string;
   FShiftOverlappingErrorMsg: string;
   FShiftGapErrorMsg: string;
   FMinShiftDurationErrorMsg: string;
   FMaxShiftDurationErrorMsg: string;
   FTeamNotFoundErrorMsg: string;
   procedure SetCalendarMessages(Index: Integer; Value: string);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
   destructor Destroy; override;
   procedure ShowErrorMessage(aErrorMsgID: byte; aErrorStr: string);
  published
   property DateStr: string index 0 read FDateStr write SetCalendarMessages;
   property Shift1Str: string index 1 read FShift1Str write SetCalendarMessages;
   property Shift2Str: string index 2 read FShift2Str write SetCalendarMessages;
   property Shift3Str: string index 3 read FShift3Str write SetCalendarMessages;
   property Shift4Str: string index 4 read FShift4Str write SetCalendarMessages;
   property Shift5Str: string index 5 read FShift5Str write SetCalendarMessages;
   property ChangesStr: string index 6 read FChangesStr write SetCalendarMessages;
   property PatternErrMsg: string index 7 read FPatternErrMsg write SetCalendarMessages;
   property DaysInPastErrorMsg: string index 8 read FDaysInPastErrorMsg write SetCalendarMessages;
   property DateShiftStr: string index 9 read FDateShiftStr write SetCalendarMessages;
   property FirstShiftEmptyErrorMsg: string index 10 read FFirstShiftEmptyErrorMsg write SetCalendarMessages;
   property ShiftOverlappingErrorMsg: string index 11 read FShiftOverlappingErrorMsg write SetCalendarMessages;
   property ShiftGapErrorMsg: string index 12 read FShiftGapErrorMsg write SetCalendarMessages;
   property MinShiftDurationErrorMsg: string index 13 read FMinShiftDurationErrorMsg write SetCalendarMessages;
   property MaxShiftDurationErrorMsg: string index 14 read FMaxShiftDurationErrorMsg write SetCalendarMessages;
   property TeamNotFoundErrorMsg: string index 15 read FTeamNotFoundErrorMsg write SetCalendarMessages;
  end; //TCalendarMessages


 TCalendarColors = class(TPersistent)
  private
   FControl: TControl;
   FDayColor: TColor;
   FDayTextColor: TColor;
   FMarkColor: TColor;
   FMarkTextColor: TColor;
   FOffDayColor: TColor;
   FOffDayTextColor: TColor;
   FTimeColor: TColor;
   FTimeTextColor: TColor;
   FTitleColor: TColor;
   FTitleTextColor: TColor;
   FWeekEndColor: TColor;
   FWeekEndTextColor: TColor;
   procedure SetCalendarColors(Index: Integer; Value: TColor);
  protected
   property Control: TControl read FControl;
  public
   constructor Create(Control: TControl); virtual;
  published
   property DayColor: TColor index 0 read FDayColor write SetCalendarColors;
   property DayTextColor: TColor index 1 read FDayTextColor write SetCalendarColors;
   property MarkColor: TColor index 2 read FMarkColor write SetCalendarColors;
   property MarkTextColor: TColor index 3 read FMarkTextColor write SetCalendarColors;
   property OffDayColor: TColor index 4 read FOffDayColor write SetCalendarColors;
   property OffDayTextColor: TColor index 5 read FOffDayTextColor write SetCalendarColors;
   property TimeColor: TColor index 6 read FTimeColor write SetCalendarColors;
   property TimeTextColor: TColor index 7 read FTimeTextColor write SetCalendarColors;
   property TitleColor: TColor index 8 read FTitleColor write SetCalendarColors;
   property TitleTextColor: TColor index 9 read FTitleTextColor write SetCalendarColors;
   property WeekEndColor: TColor index 10 read FWeekEndColor write SetCalendarColors;
   property WeekEndTextColor: TColor index 11 read FWeekEndTextColor write SetCalendarColors;
  end; //TCalendarColors


 TmmCustomCalendar = class(TCustomGrid)
  private
   FCalendarColors: TCalendarColors;
   FCalendarID: longint;
   FCalendarMessages: TCalendarMessages;
   FConnection: TmmADOConnection;
   FReadOnly: boolean;
   FShowWeekEndDays: boolean;
   FWeekEndDays : sWeekendDayT;
   procedure SetCalendarID(Value: longint);
   procedure SetReadOnly(Value: boolean);
   procedure SetShowWeekEndDays(Value: boolean);
   procedure SetWeekEndDays(Value: sWeekendDayT);
  protected
   function LTrans(aTextStr: string): string;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   property Options;
   property ShowWeekEndDays: boolean read FShowWeekEndDays write SetShowWeekEndDays default true;
   property WeekEndDays: sWeekendDayT read FWeekEndDays write SetWeekEndDays default [wdSaturday,wdSunday];
  published
   property Anchors;
   property BorderStyle default bsSingle;
   property CalendarColors: TCalendarColors read FCalendarColors write FCalendarColors;
   property CalendarID: longint read FCalendarID write SetCalendarID default 0;
   property CalendarMessages: TCalendarMessages read FCalendarMessages write FCalendarMessages;
   property Color;
   property Constraints;
   property Ctl3D;
   property DatabaseConnection: TmmADOConnection read FConnection write FConnection;
   property Enabled;
   property Font;
   property ParentColor;
   property ParentFont;
   property ParentShowHint;
   property PopupMenu;
   property ReadOnly: boolean read FReadOnly write SetReadOnly default False;
   property ShowHint;
   property TabOrder;
   property TabStop;
   property Visible;
 end; //TmmCustomCalendar


 TmmCustomShiftCalendar = class(TmmCustomCalendar)
  private
   FShowTeamColors: boolean;
   procedure SetShowTeamColors(Value: boolean);
  protected
   procedure DrawCell(aCol,aRow: Longint; ARect: TRect; AState: TGridDrawState); override;
   procedure SetRowHeights;
  public
   TeamColor: TStrings;
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function GetTeamColor(Team: string): TColor;
   function TeamIsPresent(Team: string): boolean;
   procedure AddTeamColor(Team: string; Color: TColor);
   procedure ReadTeamColors;
   function GetCellInfoAt(aCol,aRow: Longint): rCellInfoT; virtual;
  published
   property Align;
   property ShowTeamColors: boolean read FShowTeamColors write SetShowTeamColors default false;
   property OnClick;
   property OnDblClick;
   property OnEnter;
   property OnExit;
 end; //TmmCustomShiftCalendar


 TShiftCalendarGrid = class(TmmCustomShiftCalendar)
  private
   FFirstEditCell: longint; //year *1e5 +month *1e3 +day *10 +shift
   FMarkChar: Char;
   FMarkFields: sMarkFieldT;
   FMaxShiftsPerDay: integer;
   FMonth: integer;
   FOffDay: aOffDayT;
   FShifts: array of rShiftCheckT;
   FYear: integer;
   FDefault,
   FSave,
   FWork: rShiftT;
   function GetFirstEditCell(aYear,aMonth,aDay,aShift: word): longint;
   procedure SetMarkChar(Value: Char);
   procedure SetMarkFields(Value: sMarkFieldT);
   procedure SetMaxShiftsPerDay(Value: integer);
   procedure SetMonth(Value: integer);
   procedure SetYear(Value: integer);
  protected
   function GetEditMask(aCol,aRow: Longint): string; override; //***
   function GetEditText(aCol,aRow: Longint): string; override;
   procedure SetEditText(aCol,aRow: Longint; const Value: string); override;
   function CheckShiftDuration(var aErrorMsg: string): integer;
   function CheckShiftTimes(var aErrorMsg: string): integer;
   function CheckTeams(var aErrorMsg: string): integer;
   function GetCol(aDay,aShift: integer): longint;
   function GetDay(aCol,aRow: longint): integer;
   function GetEditTimeStr(aCol,aRow: longint): string;
   function GetRow(aDay: integer): longint;
   function GetShift(aCol: longint): integer;
   function GetShiftBegin(aDate: tdatetime; aShift: byte): s5;
   function SelectCell(aCol,aRow: longint): boolean; override;
   procedure CheckEditOptions;
   procedure SetColWidths;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function GetCellInfoAt(aCol,aRow: longint): rCellInfoT; override;
   function GetShiftRecordData(aDate: TDateTime; aShift: byte; aDirection: eScrollDirectionT): rShiftCheckT;
   function IsDifferent: boolean;
   function WriteShiftData: boolean;
   procedure CalcFirstEditCell;
   procedure ReadDefaultShifts;
   procedure ReadOffDays;
   procedure ReadShiftData;
  published
   property MarkChar: Char read FMarkChar write SetMarkChar default '*';
   property MarkFields: sMarkFieldT read FMarkFields write SetMarkFields default [mfTimeDifferent];
   property MaxShiftsPerDay: integer read FMaxShiftsPerDay write SetMaxShiftsPerDay default cMaxShiftsPerDay;
   property Month: integer read FMonth write SetMonth;
   property ShowTeamColors;
   property ShowWeekEndDays;
   property WeekEndDays;
   property Year: integer read FYear write SetYear;
   property OnKeyDown;
   property OnKeyPress;
   property OnKeyUp;
  end; //TShiftCalendarGrid


 TDefaultShiftGrid = class(TmmCustomShiftCalendar)
  private
   FMaxShiftsPerDay: integer;
   FSave,
   FWork: rDefaultShiftT;
   procedure SetMaxShiftsPerDay(Value: integer);
  protected
   function GetEditMask(aCol,aRow: Longint): string; override;
   function GetEditText(aCol,aRow: Longint): string; override;
   procedure SetEditText(aCol,aRow: Longint; const Value: string); override;
   function CheckShiftDuration(var aErrorMsg: string): integer;
   function CheckShiftTimes(var aErrorMsg: string): integer;
   function GetEditTimeStr(aCol,aRow: longint): string;
   function SelectCell(aCol,aRow: longint): boolean; override;
   procedure SetColWidths;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function GetCalendarName: string;
   function GetCellInfoAt(aCol,aRow: Longint): rCellInfoT; override;
   function IsDifferent: boolean;
   function WriteShiftData: boolean;
   procedure ReadShiftData;
  published
   property MaxShiftsPerDay: integer read FMaxShiftsPerDay write SetMaxShiftsPerDay default cMaxShiftsPerDay;
   property ShowWeekEndDays;
   property WeekEndDays;
   property OnKeyDown;
   property OnKeyPress;
   property OnKeyUp;
  end; //TDefaultShiftGrid


 TShiftPatternGrid = class(TmmCustomShiftCalendar)
  private
   FMaxShiftsPerDay: integer;
   FOnChange: TNotifyEvent;
   FShiftPatternID: longint;
   FShiftPatternPeriod: integer;
   FSave,
   FWork: rShiftPatternT;
   procedure SetMaxShiftsPerDay(Value: integer);
   procedure SetShiftPatternID(Value: longint);
   procedure SetShiftPatternPeriod(Value: integer);
  protected
   function GetEditMask(aCol,aRow: Longint): string; override;
   function GetEditText(aCol,aRow: Longint): string; override;
   procedure SetEditText(aCol,aRow: Longint; const Value: string); override;
   function SelectCell(aCol,aRow: longint): boolean; override;
   procedure Change; dynamic;
   procedure KeyPress(var Key: Char); override;
   procedure KeyUp(var Key: Word; Shift: TShiftState); override;
   procedure SetColWidths;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function IsDifferent: boolean;
   function GetCellInfoAt(aCol,aRow: Longint): rCellInfoT; override;
   procedure DeleteShiftPattern(aPatternID: longint);
   procedure ReadShiftPatternData;
   procedure SetCursorTo(aDay,aShift: integer);
   function Validate: boolean;
   procedure WriteShiftPatternData;
  published
   property MaxShiftsPerDay: integer read FMaxShiftsPerDay write SetMaxShiftsPerDay default cMaxShiftsPerDay;
   property ShiftPatternID: longint read FShiftPatternID write SetShiftPatternID default 0;
   property ShiftPatternPeriod: integer read FShiftPatternPeriod write SetShiftPatternPeriod;
   property ShowTeamColors;
   property OnChange: TNotifyEvent read FOnChange write FOnChange;
   property OnKeyDown;
  end; //TShiftPatternGrid


 TAppendDefaultShifts = class(TComponent)
  private
   FCalendarID: longint;
   FConnection: TmmADOConnection;
   FHideExceptions: boolean;
   procedure SetCalendarID(Value: longint);
  protected
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function Execute: shortint;
  published
   property CalendarID: longint read FCalendarID write SetCalendarID default 0;
   property Connection: TmmADOConnection read FConnection write FConnection;
   property HideExceptions: boolean read FHideExceptions write FHideExceptions;
 end; //TAppendDefaultShifts


 TFactoryCalendar = class(TmmCustomCalendar)
  private
   FColWidth: integer;
   FMonth: integer;
   FMonthOffset: integer;
   FOnChange: TNotifyEvent;
   FRowHeight: integer;
   FShowWeekEndTitle : boolean;
   FStartOfWeek: eWeekendDayT;
   FYear: integer;
   FWork,
   FSave: array[1..cMaxDaysPerMonth] of boolean;
   procedure SetColWidth(Value: integer);
   procedure SetMonth(Value: integer);
   procedure SetRowHeight(Value: integer);
   procedure SetShowWeekEndTitle(Value: boolean);
   procedure SetStartOfWeek(Value: eWeekendDayT);
   procedure SetYear(Value: integer);
  protected
   function DaysThisMonth: integer; virtual;
   function GetDay(aCol,aRow: longint): integer;
   procedure Change; dynamic;
   procedure DblClick; override;
   procedure DrawCell(aCol,aRow: Longint; ARect: TRect; AState: TGridDrawState); override;
   procedure KeyPress(var Key: Char); override;
   procedure GetMonthOffset;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
   function IsDifferent: boolean;
   procedure ReadExceptDays;
   procedure WriteExceptDays;
   property Height;
   property Width;
  published
   property ColWidth: integer read FColWidth write SetColWidth default 25;
   property RowHeight: integer read FRowHeight write SetRowHeight default 18;
   property Month: integer read FMonth write SetMonth;
   property ShowWeekEndDays;
   property ShowWeekEndTitle: boolean read FShowWeekEndTitle write SetShowWeekEndTitle default true;
   property StartOfWeek: eWeekendDayT read FStartOfWeek write SetStartOfWeek;
   property WeekEndDays;
   property Year: integer read FYear write SetYear;
   property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end; //TFactoryCalendar


 function GetShiftMonthNo(aYear,aMonth: word): longint;
 function GetShiftID(aShiftDate: tdatetime; aShiftInDay: byte): longint;
 function GetDS_ShiftDuration(aDSRec: rDefaultShiftT; aDay,aShift: byte): integer;


// procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

resourcestring
 cDateStr                  = '(8)Datum'; //ivlm
 cShift1Str                = '(5)S1'; //ivlm
 cShift2Str                = '(5)S2'; //ivlm
 cShift3Str                = '(5)S3'; //ivlm
 cShift4Str                = '(5)S4'; //ivlm
 cShift5Str                = '(5)S5'; //ivlm
 cChangesStr               = '(2)X'; //ivlm
 cPatternErrMsg            = '(255)Schichtgruppe nicht vorhanden !'; //ivlm
 cDaysInPastErrorMsg       = '(255)Der Status zurueckliegender Tage kann nachtraeglich nicht mehr geaendert werden !'; //ivlm
 cDateShiftStr             = '(50)Datum / Schicht: %s'; //ivlm
 cFirstShiftEmptyErrorMsg  = '(255)1. Schicht darf nicht leer sein!'; //ivlm
 cShiftOverlappingErrorMsg = '(255)Aktuelle Schichtzeit <= Schichtzeit der vorhergehenden Schicht!'; //ivlm
 cShiftGapErrorMsg         = '(255)Luecken in Schichtfolge sind nicht erlaubt!'; //ivlm
 cMinShiftDurationErrorMsg = '(255)Schichtdauer zu gering! Die Mindest-Schichtdauer betraegt %d min.'; //ivlm
 cMaxShiftDurationErrorMsg = '(255)Schichtdauer zu gross! Die Maximal-Schichtdauer betraegt %d min.'; //ivlm
 cTeamNotFoundErrorMsg     = '(255)Schichtgruppe / Team nicht vorhanden!'; //ivlm


//common functions, procedures
function GetMinute(s: string): byte;
begin
 s := GetLastStr(s,cTimeSeparator);
 try
  result := strtoint(s);
 except
  result := 0;
 end; //try..except
end; //function GetMinute
//-----------------------------------------------------------------------------
function GetHour(s: string): byte;
begin
 s := GetFirstStr(s,cTimeSeparator);
 try
  result := strtoint(s);
 except
  result := 0;
 end; //try..except
end; //function GetHour
//-----------------------------------------------------------------------------
function ValTimeStr(aTimeStr: s5): s5;
var
 xHour,
 xMin  : byte;

begin
 result := trim(aTimeStr);
 if (result <> '') and (result <> cTimeSeparator) then
 begin
  xMin := GetMinute(result);
  xHour := GetHour(result);
  result := lz(xHour,2) +cTimeSeparator +lz(xMin,2);
 end //if (result <> '') and (result <> cTimeSeparator) then
 else result := ''; //null
end; //function ValTimeStr
//-----------------------------------------------------------------------------
function FormatTimeToStr(aTime: TDateTime): s5;
var
 xHour,
 xMin  : word;
begin
 xHour := round(frac(aTime) *cMinPerDay) div 60;
 xMin := round(frac(aTime) *cMinPerDay) mod 60;
 result := lz(xHour,2) +cTimeSeparator +lz(xMin,2);
end; //function FormatTimeToStr
//-----------------------------------------------------------------------------
function GetTimeFromStr(aTimeStr: s5): double;
begin
 aTimeStr := trim(aTimeStr);
 if (aTimeStr <> '') and (aTimeStr <> cTimeSeparator) then
 begin
  result := (GetHour(aTimeStr) *cMinPerHour +GetMinute(aTimeStr)) /cMinPerDay;
 end //if (aTimeStr <> '') and (aTimeStr <> cTimeSeparator) then
 else result := 0;
end; //function GetTimeFromStr
//-----------------------------------------------------------------------------
function GetCurrentMonth: word;
var
 xYear,xDay: word;

begin
 DecodeDate(sysutils.date,xYear,result,xDay);
end; //function GetCurrentMonth
//-----------------------------------------------------------------------------
function GetCurrentYear: word;
var
 xMonth,xDay: word;

begin
 DecodeDate(sysutils.date,result,xMonth,xDay);
end; //function GetCurrentYear
//-----------------------------------------------------------------------------
function GetMonth(aDate: TDateTime): word;
var
 xYear,xDay: word;

begin
 DecodeDate(aDate,xYear,result,xDay);
end; //function GetMonth
//-----------------------------------------------------------------------------
function GetYear(aDate: TDateTime): word;
var
 xMonth,xDay: word;

begin
 DecodeDate(aDate,result,xMonth,xDay);
end; //function GetYear
//-----------------------------------------------------------------------------

//******************************************************************************
// Faktor aus anzahl Monate seit Sept. 1998 erstellen  -> Faktor = anz. Monte * 1000
//******************************************************************************
function GetShiftMonthNo(aYear,aMonth: word): longint;
const
 cStartYear  = 1998;
 cStartMonth = 9;
begin
 result := ((aYear - cStartYear) * cMonthsPerYear + (aMonth - cStartMonth) + 1) * 1000;
end; //function TShiftCalendarGrid.GetShiftMonthNo
//-----------------------------------------------------------------------------

//******************************************************************************
// ShiftID ermitteln; ist zusammen gesetzt aus Datum
//******************************************************************************
function GetShiftID(aShiftDate: tdatetime; aShiftInDay: byte): longint;
var
 xYear,xMonth,xDay : word;
begin
 DecodeDate(aShiftDate,xYear,xMonth,xDay);
 result := aShiftInDay +xDay * 10 + GetShiftMonthNo(xYear,xMonth);
end; //function GetShiftID
//-----------------------------------------------------------------------------

//************************************************************************************
// Ermittelt alle Daten eines Schichtkalenders, welche aelter (sdPrev) sind als
// ein bestimmtes Datum ( ShiftID ) und fuellt diese in einen Record
// (rShiftCheckT) ab.   SDO
//************************************************************************************
function GetShiftData(aDB: TmmADOConnection; aCalID: longint; aShiftID: longint; aDirection: eScrollDirectionT): rShiftCheckT;
var
 xQuery : TmmADODataset;

begin
 fillchar(result,sizeof(result),0);

 xQuery := TmmADODataset.Create(NIL);
 xQuery.Connection := aDB;
// xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := format('SELECT * FROM t_shift WHERE c_shiftcal_id = %d',[aCalID]);
   case aDirection of
    sdPrev: begin
     CommandText := CommandText + format(' AND c_shift_id < %d',[aShiftID]);
     CommandText := CommandText + ' ORDER BY c_shift_id DESC';
    end; //sdPrev
    sdNone: CommandText := CommandText + format('AND c_shift_id = %d',[aShiftID]);
    sdNext: begin
     CommandText := CommandText + format(' AND c_shift_id > %d',[aShiftID]);
     CommandText := CommandText + ' ORDER BY c_shift_id';
    end; //sdNext
   end; //case aDirection of

   Open;

   if not Eof then
   begin
    DecodeDate(FieldByName('c_shift_start').AsDateTime,result.Year,result.Month,result.Day);
    result.Shift     := FieldByName('c_shift_in_day').AsInteger;
    result.StartTime := FieldByName('c_shift_start').AsDateTime;
    result.Duration  := FieldByName('c_shift_length').AsInteger;
    result.ShiftID   := FieldByName('c_shift_id').AsInteger;
   end; //if not Eof then
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //function GetShiftData
//-----------------------------------------------------------------------------
function GetDS_ShiftDuration(aDSRec: rDefaultShiftT; aDay,aShift: byte): integer;
var
 xTimeBegin,
 xTimeEnd   : double;
 xDayNext,
 xShiftNext : integer;

begin
 xDayNext := aDay;
 xShiftNext := aShift +1;

 xTimeBegin := GetTimeFromStr(aDSRec.StartTime[aDay,aShift]);
 if (xShiftNext > cMaxShiftsPerDay) or (aDSRec.StartTime[aDay,xShiftNext] = '') then
 begin
  xShiftNext := 1;
  inc(xDayNext);
  if xDayNext > cDaysPerWeek then xDayNext := 1;
 end; //if (xShiftNext > cMaxShiftsPerDay) or (xDSRec.StartTime[xDay,xShiftNext] = '') then

 xTimeEnd := GetTimeFromStr(aDSRec.StartTime[xDayNext,xShiftNext]);

 if xTimeEnd < (xTimeBegin +cEpsilon) then xTimeEnd := xTimeEnd +1;
 result := round((xTimeEnd -xTimeBegin) *cMinPerDay);
end; //function GetDS_ShiftDuration


//TCalendarMessages ***
constructor TCalendarMessages.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;

 DateStr := cDateStr;
 Shift1Str := cShift1Str;
 Shift2Str := cShift2Str;
 Shift3Str := cShift3Str;
 Shift4Str := cShift4Str;
 Shift5Str := cShift5Str;
 ChangesStr := cChangesStr;
 PatternErrMsg := cPatternErrMsg;
 DaysInPastErrorMsg := cDaysInPastErrorMsg;
 DateShiftStr := cDateShiftStr;
 FirstShiftEmptyErrorMsg := cFirstShiftEmptyErrorMsg;
 ShiftOverlappingErrorMsg := cShiftOverlappingErrorMsg;
 ShiftGapErrorMsg := cShiftGapErrorMsg;
 MinShiftDurationErrorMsg := cMinShiftDurationErrorMsg;
 MaxShiftDurationErrorMsg := cMaxShiftDurationErrorMsg;
 TeamNotFoundErrorMsg := cTeamNotFoundErrorMsg;
end; //constructor TCalendarMessages.Create
//-----------------------------------------------------------------------------
destructor TCalendarMessages.Destroy;
begin
 inherited Destroy;
end; //destructor TCalendarMessages.Destroy
//-----------------------------------------------------------------------------
procedure TCalendarMessages.SetCalendarMessages(Index: Integer; Value: string);
begin
 case Index of
  0:  if Value <> FDateStr then begin FDateStr := Value; FControl.Refresh; end;
  1:  if Value <> FShift1Str then begin FShift1Str := Value; FControl.Refresh; end;
  2:  if Value <> FShift2Str then begin FShift2Str := Value; FControl.Refresh; end;
  3:  if Value <> FShift3Str then begin FShift3Str := Value; FControl.Refresh; end;
  4:  if Value <> FShift4Str then begin FShift4Str := Value; FControl.Refresh; end;
  5:  if Value <> FShift5Str then begin FShift5Str := Value; FControl.Refresh; end;
  6:  if Value <> FChangesStr then begin FChangesStr := Value; FControl.Refresh; end;
  7:  if Value <> FPatternErrMsg then begin FPatternErrMsg := Value; FControl.Refresh; end;
  8:  if Value <> FDaysInPastErrorMsg then begin FDaysInPastErrorMsg := Value; FControl.Refresh; end;
  9:  if Value <> FDateShiftStr then begin FDateShiftStr := Value; FControl.Refresh; end;
  10: if Value <> FFirstShiftEmptyErrorMsg then begin FFirstShiftEmptyErrorMsg := Value; FControl.Refresh; end;
  11: if Value <> FShiftOverlappingErrorMsg then begin FShiftOverlappingErrorMsg := Value; FControl.Refresh; end;
  12: if Value <> FShiftGapErrorMsg then begin FShiftGapErrorMsg := Value; FControl.Refresh; end;
  13: if Value <> FMinShiftDurationErrorMsg then begin FMinShiftDurationErrorMsg := Value; FControl.Refresh; end;
  14: if Value <> FMaxShiftDurationErrorMsg then begin FMaxShiftDurationErrorMsg := Value; FControl.Refresh; end;
  15: if Value <> FTeamNotFoundErrorMsg then begin FTeamNotFoundErrorMsg := Value; FControl.Refresh; end;
 end; //case Index of
end; //procedure TCalendarMessages.SetCalendarMessages
//-----------------------------------------------------------------------------
procedure TCalendarMessages.ShowErrorMessage(aErrorMsgID: byte; aErrorStr: string);
var
 xMsgStr : string;

begin
 case aErrorMsgID of
  cErrorFirstShiftEmpty  : xMsgStr := IvDictio.Translate(FirstShiftEmptyErrorMsg);
  cErrorShiftOverlapping : xMsgStr := IvDictio.Translate(ShiftOverlappingErrorMsg);
  cErrorShiftGap         : xMsgStr := IvDictio.Translate(ShiftGapErrorMsg);
  cErrorMinShiftDuration : xMsgStr := format(IvDictio.Translate(MinShiftDurationErrorMsg),[cMinShiftDuration]);
  cErrorMaxShiftDuration : xMsgStr := format(IvDictio.Translate(MaxShiftDurationErrorMsg),[cMaxShiftDuration]);
  cErrorTeamNotFound     : xMsgStr := IvDictio.Translate(TeamNotFoundErrorMsg);
 end; //case aErrorMsgID of

 xMsgStr := xMsgStr +cCRLF +format(IvDictio.Translate(DateShiftStr),[aErrorStr]);

 MessageBeep(0);
 MessageDlg(xMsgStr,mtError,[mbOK],0);
end; //procedure TCalendarMessages.ShowErrorMessage


//TCalendarColors ***
constructor TCalendarColors.Create(Control: TControl);
begin
 inherited Create;
 FControl := Control;

 FDayColor := clLightGray1;
 FDayTextColor := clBlack;
 FMarkColor := clLightGray1;
 FMarkTextColor := clBlue;
 FOffDayColor := clLightGray1;
 FOffDayTextColor := clRed;
 FTimeColor := clWindow;
 FTimeTextColor := clBlack;
 FTitleColor := clLightGray2;
 FTitleTextColor := clBlack;
 FWeekEndColor := clMarkWeekEnd;
 FWeekEndTextColor := clBlack;
end; //constructor TCalendarMessages.Create
//-----------------------------------------------------------------------------
procedure TCalendarColors.SetCalendarColors(Index: Integer; Value: TColor);
begin
 case Index of
  0: if Value <> FDayColor then begin FDayColor := Value; FControl.Refresh; end;
  1: if Value <> FDayTextColor then begin FDayTextColor := Value; FControl.Refresh; end;
  2: if Value <> FMarkColor then begin FMarkColor := Value; FControl.Refresh; end;
  3: if Value <> FMarkTextColor then begin FMarkTextColor := Value; FControl.Refresh; end;
  4: if Value <> FOffDayColor then begin FOffDayColor := Value; FControl.Refresh; end;
  5: if Value <> FOffDayTextColor then begin FOffDayTextColor := Value; FControl.Refresh; end;
  6: if Value <> FTimeColor then begin FTimeColor := Value; FControl.Refresh; end;
  7: if Value <> FTimeTextColor then begin FTimeTextColor := Value; FControl.Refresh; end;
  8: if Value <> FTitleColor then begin FTitleColor := Value; FControl.Refresh; end;
  9: if Value <> FTitleTextColor then begin FTitleTextColor := Value; FControl.Refresh; end;
  10: if Value <> FWeekEndColor then begin FWeekEndColor := Value; FControl.Refresh; end;
  11: if Value <> FWeekEndTextColor then begin FWeekEndTextColor := Value; FControl.Refresh; end;
 end; //case Index of
end; //procedure TCalendarColors.SetCalendarColors


//TmmCustomCalendar ***
constructor TmmCustomCalendar.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);

 BorderStyle := bsSingle;
 Ctl3D := false;
 FCalendarID := 0;
 FConnection := nil;
 FixedCols := 1;
 FixedRows := 1;
 FReadOnly := false;
 FShowWeekEndDays := true;
 FWeekEndDays := [wdSaturday,wdSunday];
 Options := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine,goDrawFocusSelected,goEditing];
 ScrollBars := ssNone;

 FCalendarColors := TCalendarColors.Create(self);
 FCalendarMessages := TCalendarMessages.Create(self);
end; //constructor TmmCustomCalendar.Create
//-----------------------------------------------------------------------------
destructor TmmCustomCalendar.Destroy;
begin
 FCalendarMessages.Free;
 FCalendarColors.Free;
 inherited Destroy;
end; //destructor TmmCustomCalendar.Destroy
//-----------------------------------------------------------------------------
function TmmCustomCalendar.LTrans(aTextStr: string): string;
begin
 if (csDesigning in ComponentState) then result := aTextStr
                                    else result := IvDictio.Translate(aTextStr);
end; //function TmmCustomCalendar.LTrans
//-----------------------------------------------------------------------------
procedure TmmCustomCalendar.SetCalendarID(Value: longint);
begin
 if FCalendarID <> Value then
 begin
  FCalendarID := Value;
  if csDesigning in ComponentState then Refresh;
 end; //if FCalendarID <> Value then
end; //procedure TmmCustomCalendar.SetCalendarID
//-----------------------------------------------------------------------------
procedure TmmCustomCalendar.SetReadOnly(Value: boolean);
begin
 if FReadOnly <> Value then
 begin
  FReadOnly := Value;
  if FReadOnly then Options := Options -[goEditing]
               else Options := Options +[goEditing];
 end; //if FReadOnly <> Value then
end; //procedure TmmCustomCalendar.SetReadOnly
//-----------------------------------------------------------------------------
procedure TmmCustomCalendar.SetShowWeekEndDays(Value: boolean);
begin
 if FShowWeekEndDays <> Value then
 begin
  FShowWeekEndDays := Value;
  Refresh;
 end; //if FShowWeekEndDays <> Value then
end; //procedure TmmCustomCalendar.SetShowWeekEndDays
//-----------------------------------------------------------------------------
procedure TmmCustomCalendar.SetWeekEndDays(Value: sWeekendDayT);
begin
 if FWeekEndDays <> Value then
 begin
  FWeekEndDays := Value;
  Refresh;
 end; //if FWeekEndDays <> Value then
end; //procedure TmmCustomCalendar.SetWeekEndDays


//TmmCustomShiftCalendar ***
constructor TmmCustomShiftCalendar.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);
 FShowTeamColors := false;
 TeamColor := TStringList.Create;
end; //constructor TmmCustomShiftCalendar.Create
//-----------------------------------------------------------------------------
destructor TmmCustomShiftCalendar.Destroy;
begin
 TeamColor.Free;
 inherited Destroy;
end; //destructor TmmCustomShiftCalendar.Destroy
//-----------------------------------------------------------------------------
function TmmCustomShiftCalendar.GetTeamColor(Team: string): TColor;
var
 i : longint;

begin
 result := FCalendarColors.FTimeColor;
 i := TeamColor.IndexOf(Team);
 if i = -1 then exit;
 result := longint(TeamColor.Objects[i]);
end; //function TmmCustomShiftCalendar.GetTeamColor
//-----------------------------------------------------------------------------
function TmmCustomShiftCalendar.TeamIsPresent(Team: string): boolean;
begin
 result := TeamColor.IndexOf(Team) > -1;
end; //function TmmCustomShiftCalendar.TeamIsPresent
//-----------------------------------------------------------------------------
procedure TmmCustomShiftCalendar.AddTeamColor(Team: string; color: TColor);
begin
 if TeamColor.IndexOf(Team) > -1 then exit;
 TeamColor.AddObject(Team,TObject(Color));
end; //procedure TmmCustomShiftCalendar.AddTeamColor
//-----------------------------------------------------------------------------
procedure TmmCustomShiftCalendar.ReadTeamColors;
var
 xQuery : TmmADODataset;

begin
 TeamColor.Clear;
 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := 'SELECT c_team_id, c_team_color FROM t_team ORDER BY c_team_id';
   Open;

   while not Eof do
   begin
    AddTeamColor(FieldByName('c_team_id').AsString,FieldByName('c_team_color').AsInteger);
    Next;
   end; //while not Eof do
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TmmCustomShiftCalendar.ReadTeamColors
//-----------------------------------------------------------------------------
procedure TmmCustomShiftCalendar.SetShowTeamColors(Value: boolean);
begin
 if FShowTeamColors <> Value then
 begin
  FShowTeamColors := Value;
  Refresh;
 end; //if FShowTeamColors <> Value then
end; //procedure TmmCustomShiftCalendar.SetShowTeamColors
//-----------------------------------------------------------------------------
function TmmCustomShiftCalendar.GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
begin
 result.cString := '';
 result.fStyle := [];
 result.fColor := clBlack;
 result.bColor := clWindow;
end; //function TmmCustomShiftCalendar.GetCellInfoAt
//-----------------------------------------------------------------------------
procedure TmmCustomShiftCalendar.DrawCell(aCol,aRow: Longint; ARect: TRect; AState: TGridDrawState);
var
 xCellInfo : rCellInfoT;

begin
 xCellInfo := GetCellInfoAt(aCol,aRow);

 if (gdSelected in AState) and SelectCell(aCol,aRow) then
 begin
  xCellInfo.fColor := clHighlightText;
  xCellInfo.bColor := clHighlight;
 end; //if gdSelected in AState then

 Canvas.Font.Style := xCellInfo.fStyle;
 Canvas.Font.Color := xCellInfo.fColor;
 Canvas.Brush.Color := xCellInfo.bColor;

 with ARect, Canvas do
 begin
  FillRect(ARect);
  TextRect(ARect,Left +(Right -Left -TextWidth(xCellInfo.cString)) div 2,
           Top +(Bottom -Top -TextHeight(xCellInfo.cString)) div 2,xCellInfo.cString);
  if (gdFocused in AState) and (not SelectCell(aCol,aRow)) then DrawFocusRect(ARect);
 end; //with ARect, Canvas do
end; //procedure TmmCustomShiftCalendar.DrawCell
//-----------------------------------------------------------------------------
procedure TmmCustomShiftCalendar.SetRowHeights;
const
 cDefHeight = 24;

var
 i : integer;

begin
 RowHeights[0] := cDefHeight;
 for i := 1 to RowCount -1 do
  RowHeights[i] := cDefHeight -2;

 Height := RowHeights[0] +(RowHeights[1] *RowCount) +GridLineWidth *(RowCount +1);
end; //procedure TmmCustomShiftCalendar.SetRowHeights

//------------------------------------------------------------------------------
// TShiftCalendarGrid 
//------------------------------------------------------------------------------
constructor TShiftCalendarGrid.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);
 FMaxShiftsPerDay := cMaxShiftsPerDay;

 ColCount := (2 +FMaxShiftsPerDay) *2 +1;
 RowCount := 17;
 FixedCols := 0;
 FMarkChar := '*';
 FMarkFields := [mfTimeDifferent];
 FMonth := GetCurrentMonth;
 FYear := GetCurrentYear;

 FFirstEditCell := GetFirstEditCell(GetCurrentYear,GetCurrentMonth,cMaxDaysPerMonth,FMaxShiftsPerDay);
 SetColWidths;
 SetRowHeights;
end; //constructor TShiftCalendarGrid.Create
//-----------------------------------------------------------------------------
destructor TShiftCalendarGrid.Destroy;
begin
 inherited Destroy;
end; //destructor TShiftCalendarGrid.Destroy
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetColWidths;
var
 i : integer;

begin
 for i := 0 to ColCount -1 do
 begin
  if (i = 0) or (i = (3+FMaxShiftsPerDay)) then ColWidths[i] := 60;
  if (i > 0) and (i <= FMaxShiftsPerDay) then ColWidths[i] := 55;
  if (i >= (4+FMaxShiftsPerDay)) and (i <= (3+2*FMaxShiftsPerDay)) then ColWidths[i] := 55;
  if (i = (1+FMaxShiftsPerDay)) or (i = (4+2*FMaxShiftsPerDay)) then ColWidths[i] := 15;
  if i = (2+FMaxShiftsPerDay) then ColWidths[i] := 40;
 end; //for i := 1 to ColCount -1 do
end; //procedure TShiftCalendarGrid.SetColWidths
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.ReadOffDays;
var
 xQuery : TmmADODataset;

begin
 fillchar(FOffDay,sizeof(FOffDay),0);
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := 'SELECT DATEPART(day,e.c_except_date) AS day FROM t_except_days e';
   CommandText := CommandText + ' JOIN t_shiftcal t ON (e.c_factory_calendar_id = t.c_factory_calendar_id)';
   CommandText := CommandText + format(' WHERE t.c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + format(' AND DATEPART(month,e.c_except_date) = %d',[FMonth]);
   CommandText := CommandText + format(' AND DATEPART(year,e.c_except_date) = %d',[FYear]);
   CommandText := CommandText + ' ORDER BY e.c_except_date';
   Open;

   while not Eof do
   begin
    FOffDay[FieldByName('day').asinteger] := true;
    Next;
   end; //while not Eof do
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TShiftCalendarGrid.ReadOffDays
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetDay(aCol,aRow: longint): integer;
begin
 result := 0;
 if aRow = 0 then exit;
 if aCol < (2+FMaxShiftsPerDay) then result := aRow
                                else result := aRow +16;
 if result > DaysPerMonth(FYear,FMonth) then result := 0;
end; //function TShiftCalendarGrid.GetDay
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetShift(aCol: longint): integer;
begin
 result := 0;
 if (aCol = 1) or (aCol = (4+FMaxShiftsPerDay)) then result := 1;
 if FMaxShiftsPerDay > 1 then
  if (aCol = 2) or (aCol = (5+FMaxShiftsPerDay)) then result := 2;
 if FMaxShiftsPerDay > 2 then
  if (aCol = 3) or (aCol = (6+FMaxShiftsPerDay)) then result := 3;
 if FMaxShiftsPerDay > 3 then
  if (aCol = 4) or (aCol = (7+FMaxShiftsPerDay)) then result := 4;
 if FMaxShiftsPerDay > 4 then
  if (aCol = 5) or (aCol = (8+FMaxShiftsPerDay)) then result := 5;
end; //function TShiftCalendarGrid.GetShift
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetCol(aDay,aShift: integer): longint;
begin
 if aDay < 17 then result := 0 +aShift
              else result := 3 +FMaxShiftsPerDay;
 result := result +aShift;
end; //function TShiftCalendarGrid.GetCol
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetRow(aDay: integer): longint;
begin
 if aDay < 17 then result := aDay
              else result := aDay mod 16;
end; //function TShiftCalendarGrid.GetRow
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetMaxShiftsPerDay(Value: integer);
begin
 if Value <> FMaxShiftsPerDay then
 begin
  if Value > cMaxShiftsPerDay then Value := cMaxShiftsPerDay;
  if Value < cMinShiftsPerDay then Value := cMinShiftsPerDay;
  FMaxShiftsPerDay := Value;
  ColCount := (2 +FMaxShiftsPerDay) *2 +1;
  SetColWidths;
  Refresh;
 end; //if Value <> FMaxShiftsPerDay then
end; //procedure TShiftCalendarGrid.SetMaxShiftsPerDay
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.CheckEditOptions;
begin
 if (FYear *100 +FMonth) < (GetCurrentYear *100 +GetCurrentMonth) then Options := Options -[goEditing]
                                                                  else Options := Options +[goEditing];
 //SetToNextValidEditCell;
end; //procedure TShiftCalendarGrid.CheckEditOptions
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetMonth(Value: integer);
begin
 if Value <> FMonth then
 begin
  FMonth := Value;
  if not FReadOnly then CheckEditOptions;
  if csDesigning in ComponentState then Refresh;
 end; //if Value <> FMonth then
end; //procedure TShiftCalendarGrid.SetMonth
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetYear(Value: integer);
begin
 if Value <> FYear then
 begin
  FYear := Value;
  if not FReadOnly then CheckEditOptions;
  if csDesigning in ComponentState then Refresh;
 end; //if Value <> FYear then
end; //procedure TShiftCalendarGrid.SetYear
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
var
 xIsDifferent,
 xIsWeekEndDay : boolean;
 i,
 xDOW,
 xShift,xDay   : integer;
 xDate         : tdatetime;

begin
 result.cString := '';
 xDOW := 0;

 if aRow = 0 then
 begin
  if (aCol = 0) or (aCol = (3+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FdateStr);
  if (aCol = 1) or (aCol = (4+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FShift1Str);
  if FMaxShiftsPerDay > 1 then
   if (aCol = 2) or (aCol = (5+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FShift2Str);
  if FMaxShiftsPerDay > 2 then
   if (aCol = 3) or (aCol = (6+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FShift3Str);
  if FMaxShiftsPerDay > 3 then
   if (aCol = 4) or (aCol = (7+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FShift4Str);
  if FMaxShiftsPerDay > 4 then
   if (aCol = 5) or (aCol = (8+FMaxShiftsPerDay)) then result.cString := LTrans(FCalendarMessages.FShift5Str);
  if aCol in [1+FMaxShiftsPerDay,4+2*FMaxShiftsPerDay] then result.cString := LTrans(FCalendarMessages.FChangesStr);

  result.fStyle := [fsBold];
  result.fColor := FCalendarColors.FTitleTextColor;
  result.bColor := FCalendarColors.FTitleColor;
 end //if aRow = 0 then
 else
 begin
  result.fStyle := [];
  xDay := GetDay(aCol,aRow);
  xShift := GetShift(aCol);

  if xDay > 0 then
  begin
   xDOW := DayOfWeek(EncodeDate(FYear,FMonth,xDay)) -1;
   xIsWeekEndDay := eWeekendDayT(xDOW) in WeekEndDays;
   xDate := EncodeDate(FYear,FMonth,xDay);
   result.cString := LTrans(GetShortDayNames(bDOW(xDOW +1))) +formatdatetime(' dd/mm',xDate);
   result.fColor := FCalendarColors.FDayTextColor;
   result.bColor := FCalendarColors.FDayColor;

   if FOffDay[xDay] then
    result.bColor := FCalendarColors.FOffDayColor;

   if xIsWeekEndDay and FShowWeekEndDays then
   begin
    result.fColor := FCalendarColors.FWeekEndTextColor;
    result.bColor := FCalendarColors.FWeekEndColor;
   end; //if xIsWeekEndDay then

   if FOffDay[xDay] then result.fColor := FCalendarColors.FOffDayTextColor;
  end //if xDay > 0 then
  else result.bColor := FCalendarColors.FDayColor;

  if aCol in [1+FMaxShiftsPerDay,4+2*FMaxShiftsPerDay] then
  begin
   result.cString := '';
   result.fStyle := [fsBold];
   result.bColor := clWindow;

   if xDay > 0 then
   begin
    for i := 1 to cMaxShiftsPerDay do
    begin
     xIsDifferent := false;
     if mfTimeDifferent in FMarkFields then xIsDifferent := FWork.StartTime[xDay,i] <> FDefault.StartTime[xDay,i];
     if mfTeamDifferent in FMarkFields then xIsDifferent := xIsDifferent or (FWork.Team[xDay,i] <> FDefault.Team[xDay,i]);
     if xIsDifferent then break;
    end; //for i := 1 to cMaxShiftsPerDay do

    if xIsDifferent then
    begin
     result.cString := FMarkChar;
     result.bColor := FCalendarColors.FMarkColor;
     result.fColor := FCalendarColors.FMarkTextColor;
    end; //if xIsDifferent then
   end; //if xDay > 0 then
  end; //if aCol in [1+FMaxShiftsPerDay,4+2*FMaxShiftsPerDay] then

  if xShift > 0 then
  begin
   result.fColor := FCalendarColors.FTimeTextColor;
   result.bColor := FCalendarColors.FTimeColor;
   result.fStyle := [];

   if xDay > 0 then
   begin
    xIsWeekEndDay := (eWeekendDayT(xDOW) in WeekEndDays) and FShowWeekEndDays;
    if xIsWeekEndDay then
    begin
     result.bColor := FCalendarColors.FWeekEndColor;
     result.fColor := FCalendarColors.FWeekEndTextColor;
    end; //if xIsWeekEndDay then
    if FShowTeamColors then result.bColor := GetTeamColor(FWork.Team[xDay,xShift]);
    result.cString := FWork.StartTime[xDay,xShift] +' (' +FWork.Team[xDay,xShift] +')';
   end; //if xDay > 0 then
  end; //if xShift > 0 then

  if aCol = (2+FMaxShiftsPerDay) then
  begin
   result.cString := '';
   result.bColor := clWindow;
  end; //if aCol = (2+FMaxShiftsPerDay) then
 end; //else if aRow = 0 then
end; //function TShiftCalendarGrid.GetCellInfoAt
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetMarkChar(Value: Char);
begin
 if Value <> FMarkChar then
 begin
  FMarkChar := Value;
  Refresh;
 end; //if Value <> FMarkChar then
end; //procedure TShiftCalendarGrid.SetMarkChar
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetMarkFields(Value: sMarkFieldT);
begin
 if FMarkFields <> Value then
 begin
  FMarkFields := Value;
  Refresh;
 end; //if FMarkFields <> Value then
end; //procedure TShiftCalendarGrid.SetMarkFields
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetFirstEditCell(aYear,aMonth,aDay,aShift: word): longint;
begin
 result := aYear *100000 +aMonth *1000 +aDay *10 +aShift;
end; //function TShiftCalendarGrid.GetFirstEditCell
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.CalcFirstEditCell;
var
 xStartDate  : tdatetime;
 xStartShift : integer;
 xYear,
 xMonth,
 xDay        : word;
 xQuery      : TmmADODataset;

begin
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   //get some records including the current date
   CommandText := 'SELECT * FROM t_shift';
   CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + format(' AND c_shift_id BETWEEN %d AND %d',[GetShiftID(Date-1,1),GetShiftID(Date+2,FMaxShiftsPerDay)]);
   CommandText := CommandText + ' ORDER BY c_shift_id';
   Open;

   xStartDate := Date +1;
   xStartShift := 1;

   //find matching record
   if not Eof then
   begin
    while not Eof do
    begin
     if Now < FieldByName('c_shift_start').Value then break;
     Next;
    end; //while not Eof do

    //get next possible shift
    if not Eof then
    begin
     xStartDate := FieldByName('c_shift_start').Value;
     xStartShift := FieldByName('c_shift_in_day').Value;

     if xStartShift < FMaxShiftsPerDay then
     begin
      inc(xStartShift);
     end //if xStartShift < FMaxShiftsPerDay then
     else
     begin
      xStartDate := xStartDate +1;
      xStartShift := 1;
     end; //else if xStartShift < FMaxShiftsPerDay then
    end; //if not Eof then
   end; //if not Eof then

   DecodeDate(xStartDate,xYear,xMonth,xDay);
   FFirstEditCell := GetFirstEditCell(xYear,xMonth,xDay,xStartShift);
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TShiftCalendarGrid.CalcFirstEditCell
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.SelectCell(aCol,aRow: Longint): boolean;
var
 xDay,
 xShift : integer;

begin
 xDay   := GetDay(aCol,aRow);
 xShift := GetShift(aCol);
 result := ((xDay *xShift) > 0) and (goEditing in Options);

 if result then
 begin
  result := GetFirstEditCell(FYear,FMonth,xDay,xShift) >= FFirstEditCell;
  if result then result := inherited SelectCell(aCol,aRow);
 end; //if aRow > 0 then
end; //function TShiftCalendarGrid.SelectCell
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetEditMask(aCol,aRow: Longint): string;
begin
 if SelectCell(aCol,aRow) then result := '99:99 (>A<);1; '
                          else result := '';
end; //function TShiftCalendarGrid.GetEditMask
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.SetEditText(aCol,aRow: Longint; const Value: string);
var
 xMod,
 xDay,
 xShift,
 xHour,
 xMin     : integer;
 xHourStr,
 xMinStr,
 xTimeStr,
 xTeamStr : string;

begin
 xDay := GetDay(aCol,aRow);
 xShift := GetShift(aCol);
 if (xDay *xShift) = 0 then exit;

 xHourStr := copy(Value,1,2);
 xMinStr := copy(Value,4,2);
 xTimeStr := xHourStr +cTimeSeparator +xMinStr;
 xTeamStr := copy(Value,8,1);

 if trim(xTimeStr) = cTimeSeparator then
 begin
  xTimeStr := '';
  xTeamStr := '';
 end; //if trim(xTimeStr) = cTimeSeparator then

 FWork.StartTime[xDay,xShift] := xTimeStr;
 FWork.Team[xDay,xShift] := xTeamStr;

 if xDay < 17 then InvalidateCell(1+FMaxShiftsPerDay,aRow)
              else InvalidateCell(4+2*FMaxShiftsPerDay,aRow);


 if xTimeStr = '' then exit;

 try
  xHour := strtoint(xHourStr);
 except
  xHour := 0;
 end; //try..except

 try
  xMin := strtoint(xMinStr);
 except
  xMin := 0;
 end; //try..except

 if xMin >= cMinPerHour then
 begin
  xMin := 0;
  inc(xHour);
 end; //if xMin >= cMinPerHour then

 //round min to 0 or 5
 xMod := xMin mod 5;
 if xMod > 0 then
 begin
  xMin := (xMin div 5) *5;
  if xMod > 2 then inc(xMin,5);
  if xMin = cMinPerHour then
  begin
   xMin := 0;
   inc(xHour);
  end; //if xMin = cMinPerHour then
 end; //if xMod > 0 then

 if xHour >= cHoursPerDay then xHour := 0;

 FWork.StartTime[xDay,xShift] := lz(xHour,2) +cTimeSeparator +lz(xMin,2);
end; //procedure TShiftCalendarGrid.SetEditText
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetEditText(aCol,aRow: Longint): string;
begin
 result := GetEditTimeStr(aCol,aRow);
end; //function TShiftCalendarGrid.GetEditText
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetEditTimeStr(aCol,aRow: longint): string;
var
 xShift,
 xDay   : integer;

begin
 result := '';
 xDay := GetDay(aCol,aRow);
 xShift := GetShift(aCol);
 if (xDay *xShift) = 0 then exit;
 if trim(FWork.StartTime[xDay,xShift]) = '' then result := '  :   ( )'
                                            else result := FWork.StartTime[xDay,xShift] +' (' +FWork.Team[xDay,xShift] +')';
end; //function TShiftCalendarGrid.GetEditTimeStr
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.IsDifferent: boolean;
var
 xDay,
 xShift : integer;

begin
 for xDay := 1 to cMaxDaysPerMonth do
  for xShift := 1 to cMaxShiftsPerDay do
  begin
   result := (FWork.StartTime[xDay,xShift] <> FSave.StartTime[xDay,xShift]) or
             (FWork.Team[xDay,xShift] <> FSave.Team[xDay,xShift]);
   if result then Exit;
  end; //for shift := 1 to cMaxShiftsPerDay do
end; //function TShiftCalendarGrid.IsDifferent
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.ReadDefaultShifts;
var
 xDay,
 xShift : integer;
 i      : integer;
 xTime  : s5;
 xQuery : TmmADODataset;

begin
 fillchar(FDefault,sizeof(FDefault),0);
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := 'SELECT * FROM t_default_shift';
   CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + ' ORDER BY c_default_shift_id';
   Open;

   while not Eof do
   begin
    xDay := FieldByName('c_default_shift_id').AsInteger div 10;
    xShift := FieldByName('c_default_shift_id').AsInteger mod 10;
    xTime := FieldByName('c_start_time').AsString;

    for i := 1 to DaysPerMonth(FYear,FMonth) do
     if bDOW(DayOfWeek(EncodeDate(FYear,FMonth,i))) = xDay then
      FDefault.StartTime[i,xShift] := xTime;

    Next;
   end; //while not Eof do
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TShiftCalendarGrid.ReadDefaultShifts
//-----------------------------------------------------------------------------
procedure TShiftCalendarGrid.ReadShiftData;
var
 xDay,
 xShift : integer;
 xQuery : TmmADODataset;

begin
 fillchar(FWork,sizeof(rShiftT),0);
 if FCalendarID = 0 then exit;

 ReadDefaultShifts;
 ReadOffDays;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := 'SELECT * FROM t_shift';
   CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + format(' AND c_shift_id BETWEEN %d AND %d',[GetShiftMonthNo(FYear,FMonth),GetShiftMonthNo(FYear,FMonth) +999]);
   Open;

   while not Eof do
   begin
    xDay := (FieldByName('c_shift_id').Value mod 1000) div 10;
    xShift := FieldByName('c_shift_id').Value mod 10;
    //FWork.StartTime[xDay,xShift] := ValTimeStr(formatdatetime('hh:nn',FieldByName('c_shift_start').Value));
    FWork.StartTime[xDay,xShift] := ValTimeStr(FormatTimeToStr(FieldByName('c_shift_start').Value));
    if FieldByName('c_team_id').IsNull then FWork.Team[xDay,xShift] := ' '
                                       else FWork.Team[xDay,xShift] := trim(FieldByName('c_team_id').Value);
    FWork.Duration[xDay,xShift] := FieldByName('c_shift_length').Value;
    Next;
   end; //while not Eof do

   FSave := FWork;
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally

 CalcFirstEditCell;
 Refresh;
end; //procedure TShiftCalendarGrid.ReadShiftData
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.CheckShiftTimes(var aErrorMsg: string): integer;
var
 i,
 xStartDay,
 xDay,
 xShift     : integer;
 xStartTime : array[1..cMaxShiftsPerDay] of double;
 xLastTime  : double;

begin
 result := 0;
 aErrorMsg := '';
 xShift := 0;
 xDay := 0;

 if GetShiftMonthNo(FYear,FMonth) > GetShiftMonthNo(GetCurrentYear,GetCurrentMonth)
  then xStartDay := 1
  else xStartDay := ((FFirstEditCell) mod 1000) div 10;

 try
  for xDay := xStartDay to DaysPerMonth(FYear,FMonth) do
  begin
   for i := 1 to cMaxShiftsPerDay do xStartTime[i] := -2;

   for xShift := 1 to FMaxShiftsPerDay do
    if trim(FWork.StartTime[xDay,xShift]) <> '' then
     xStartTime[xShift] := GetTimeFromStr(FWork.StartTime[xDay,xShift]);

   // 1st shift must have a correct time
   xShift := 1;
   if xStartTime[xShift] < 0 then
   begin
    result := cErrorFirstShiftEmpty;
    exit;
   end; //if xStart[1] < 0 then

   xLastTime := xStartTime[1] +cEpsilon;
   for xShift := 2 to FMaxShiftsPerDay do
   begin
    // current shift time < prev. shift time ??
    if (xStartTime[xShift] < xLastTime) and (xStartTime[xShift] > -1) then
    begin
     result := cErrorShiftOverlapping;
     Exit;
    end; //if (xStartTime[j] < xLastTime) and (xStartTime[j] > -1) then

    //empty shift between valid shifts ??
    if (xLastTime < 0) and (xStartTime[xShift] > -1) then
    begin
     result := cErrorShiftGap;
     Exit;
    end; //if (xLastTime < 0) and (xStartTime[xShift] > -1) then

    xLastTime := xStartTime[xShift] +cEpsilon;
   end; //for xShift := 2 to FMaxShiftsPerDay do
  end; //for xDay := 1 to DaysPerMonth(FYear,FMonth) do
 finally
  if result > 0 then aErrorMsg := formatdatetime('dd/mm/yyyy',encodedate(FYear,FMonth,xDay)) +' - ' +inttostr(xShift);
 end; //try..finally
end; //function TShiftCalendarGrid.CheckShiftTimes
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetShiftBegin(aDate: tdatetime; aShift: byte): s5;
var
 xQuery : TmmADODataset;

begin
 result := ''; //empty if shift doesn't exist

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 with xQuery do
 begin
  try
   CommandText := 'SELECT * FROM t_shift';
   CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + format(' AND c_shift_id = %d',[GetShiftID(aDate,aShift)]);
   Open;

   //if not FieldByName('c_shift_start').IsNull then result := ValTimeStr(formatdatetime('hh:nn',FieldByName('c_shift_start').Value));
   if not FieldByName('c_shift_start').IsNull then result := ValTimeStr(FormatTimeToStr(FieldByName('c_shift_start').Value));
  finally
   Free;
  end; //try..finally
 end; //with xQuery do
end; //function TShiftCalendarGrid.GetShiftBegin
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.CheckShiftDuration(var aErrorMsg: string): integer;
var
 i,
 xShift,
 xDay,
 xAnzahl   : integer;
 xLSPM,                 //last shift prev month
 xFSNM     : tdatetime; //first shift next month
 xSBegTime : s5;

begin
 FShifts := nil;
 result := 0;
 xAnzahl := 0;

 //read shiftbegin of last shift in prev. month >> if shift is present
 xLSPM := Encodedate(FYear,FMonth,1) -1;
 for xShift := FMaxShiftsPerDay downto 1 do
 begin
  xSBegTime := GetShiftBegin(xLSPM,xShift);
  if xSBegTime <> '' then break;
 end; //for xShift := FMaxShiftsPerDay downto 1 do

 if xSBegTime <> '' then //record exists ?
 begin
  inc(xAnzahl);
  SetLength(FShifts,xAnzahl);

  with FShifts[high(FShifts)] do
  begin
   Year := GetYear(xLSPM);
   Month := GetMonth(xLSPM);
   Day := DaysPerMonth(Year,Month);
   Shift := xShift;
   StartTime := GetTimeFromStr(xSBegTime);
  end; //with FShifts[high(FShifts)] do
 end; //if xSBegTime <> '' then

 //read shift data from selected month/year
 for xDay := 1 to DaysPerMonth(FYear,FMonth) do
  for xShift := 1 to FMaxShiftsPerDay do
  begin
   if trim(FWork.StartTime[xDay,xShift]) <> '' then
   begin
    inc(xAnzahl);
    SetLength(FShifts,xAnzahl);

    with FShifts[high(FShifts)] do
    begin
     Year := FYear;
     Month := FMonth;
     Day := xDay;
     Shift := xShift;
     StartTime := GetTimeFromStr(FWork.StartTime[xDay,xShift]);
    end; //with FShifts[high(FShifts)] do
   end; //if trim(FWork.StartTime[xDay,xShift]) <> '' then
  end; //for xShift := 1 to FMaxShiftsPerDay do

 //read shiftbegin of 1st shift in next month
 inc(xAnzahl);
 SetLength(FShifts,xAnzahl);
 xFSNM := Encodedate(FYear,FMonth,DaysPerMonth(FYear,FMonth)) +1;
 with FShifts[high(FShifts)] do
 begin
  Year := GetYear(xFSNM);
  Month := GetMonth(xFSNM);
  Day := 1;
  Shift := 1;
  StartTime := GetTimeFromStr(GetShiftBegin(xFSNM,1)); // startime is set to 00:00 if shift doesn't exist
 end; //with FShifts[high(FShifts)] do

 //calc shift duration
 for i := 0 to high(FShifts) -1 do
 begin
  if FShifts[i+1].Day = FShifts[i].Day
   then FShifts[i].Duration := round((FShifts[i+1].StartTime -FShifts[i].StartTime) *cMinPerDay)
   else FShifts[i].Duration := round((FShifts[i+1].StartTime +1 -FShifts[i].StartTime) *cMinPerDay);
 end; //for i := 0 to high(FShifts) -1 do

 //validate shift duration
 for i := 0 to (high(FShifts) -1) do
  with FShifts[i] do
  begin
   if Month = FMonth then FWork.Duration[Day,Shift] := Duration;
   if Duration < cMinShiftDuration then result := cErrorMinShiftDuration;
   if Duration > cMaxShiftDuration then result := cErrorMaxShiftDuration;
   if result <> 0 then
   begin
    aErrorMsg := formatdatetime('dd/mm/yyyy',Encodedate(Year,Month,Day)) +' - ' +inttostr(Shift);
    exit;
   end; //if Duration < cMinShiftDuration then
  end; //with FShifts[i] do
end; //function TShiftCalendarGrid.CheckShiftDuration
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.CheckTeams(var aErrorMsg: string): integer;
var
 xShift,
 xDay   : integer;

begin
 result := 0;

 for xDay := 1 to DaysPerMonth(FYear,FMonth) do
  for xShift := 1 to FMaxShiftsPerDay do
   if trim(FWork.Team[xDay,xShift]) <> '' then
    if not TeamIsPresent(FWork.Team[xDay,xShift]) then
    begin
     result := cErrorTeamNotFound;
     aErrorMsg := formatdatetime('dd/mm/yyyy',Encodedate(FYear,FMonth,xDay)) +' - ' +inttostr(xShift);
     Exit;
    end; //if not TeamIsPresent(FWork.Team[xDay,xShift]) then
end; //function TShiftCalendarGrid.CheckTeams
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.WriteShiftData: boolean;
var
 xDay,
 xShift    : integer;
 xShiftID  : integer;
 xDate     : tdatetime;
 xQuery    : TmmADOCommand;
 xError    : integer;
 xErrorStr : string;

//-----------
function RecordIsDifferent: boolean;
begin
 result := (FWork.StartTime[xDay,xShift] <> FSave.StartTime[xDay,xShift]) or
           (FWork.Team[xDay,xShift] <> FSave.Team[xDay,xShift]) or
           (FWork.Duration[xDay,xShift] <> FSave.Duration[xDay,xShift]);
end; //function RecordIsDifferent
//-----------

begin //main
 result := true;
 if FCalendarID = 0 then exit;
 if not IsDifferent then exit;

 try
  xError := CheckTeams(xErrorStr);
  if xError = 0 then xError := CheckShiftTimes(xErrorStr);
  if xError = 0 then xError := CheckShiftDuration(xErrorStr);

  if xError > 0 then
  begin
   result := false;
   CalendarMessages.ShowErrorMessage(xError,xErrorStr);
   Exit;
  end; //if xError > 0 then

  xQuery := TmmADOCommand.Create(NIL);
  xQuery.ConnectionString := GetDefaultConnectionString;
  try
   //if 1st shift of month was edited, update shift duration of last shift in prev. month
   if (FWork.Duration[1,1] <> FSave.Duration[1,1]) then
    with FShifts[0] do                      // <== last shift of prev. month
     if (Month <> FMonth) then              //if true then record in prev. month exists
     begin
      xShiftID := GetShiftID(EncodeDate(Year,Month,Day),Shift);
      xQuery.CommandText := 'UPDATE t_shift';
      xQuery.CommandText := xQuery.CommandText + format(' SET c_shift_length = %d',[Duration]);
      xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d AND c_shift_id = %d',[FCalendarID,xShiftID]);
      xQuery.Execute;
     end; //with FShifts[0] do

   //update selected month
   for xDay := 1 to DaysPerMonth(FYear,FMonth) do
   begin
    xDate := EncodeDate(FYear,FMonth,xDay);
    for xShift := 1 to FMaxShiftsPerDay do
    begin
     if RecordIsDifferent then
     begin
      xShiftID := GetShiftID(xDate,xShift);

     //delete existing record
      xQuery.CommandText := 'DELETE FROM t_shift';
      xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d AND c_shift_id = %d',[FCalendarID,xShiftID]);
      xQuery.Execute;

      if FWork.StartTime[xDay,xShift] <> '' then
      begin
       //insert, update records
       xQuery.CommandText := '';
       if trim(FWork.Team[xDay,xShift]) = '' then
       begin
        xQuery.CommandText := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_shift_in_day,c_shift_start,c_shift_length)';
        xQuery.CommandText := xQuery.CommandText + ' VALUES(:shift_id,:shiftcal_id,:shift_in_day,:shift_start,:shift_length)';
       end //if trim(FWork.Team[xDay,xShift]) = '' then
       else
       begin
        xQuery.CommandText := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_team_id,c_shift_in_day,c_shift_start,c_shift_length)';
        xQuery.CommandText := xQuery.CommandText + ' VALUES(:shift_id,:shiftcal_id,:team_id,:shift_in_day,:shift_start,:shift_length)';
       end; //else if trim(FWork.Team[xDay,xShift]) = '' then

       xQuery.Parameters.ParamByname('SHIFT_ID').value := xShiftID;
       xQuery.Parameters.ParamByname('SHIFTCAL_ID').value := FCalendarID;
       if trim(FWork.Team[xDay,xShift]) <> '' then xQuery.Parameters.ParamByname('TEAM_ID').value := FWork.Team[xDay,xShift];
       xQuery.Parameters.ParamByname('SHIFT_IN_DAY').value := xShift;
       xQuery.Parameters.ParamByname('SHIFT_START').DataType := ftDateTime;
       xQuery.Parameters.ParamByname('SHIFT_START').value := xDate +GetTimeFromStr(FWork.StartTime[xDay,xShift]);
       xQuery.Parameters.ParamByname('SHIFT_LENGTH').value := FWork.Duration[xDay,xShift];
       xQuery.Execute;
      end; //if FWork.StartTime[xDay,xShift] <> '' then
     end; //if RecordIsDifferent then
    end; //for xShift := 1 to FMaxShiftsPerDay do
   end; //for xDay := 1 to DaysPerMonth(FYear,FMonth) do

   FSave := FWork;
  finally
   xQuery.Free;
  end; //try..finally
 finally
  FShifts := nil;
 end; //try..finally
end; //function TShiftCalendarGrid.WriteShiftData
//-----------------------------------------------------------------------------
function TShiftCalendarGrid.GetShiftRecordData(aDate: TDateTime; aShift: byte; aDirection: eScrollDirectionT): rShiftCheckT;
begin
 result := GetShiftData(FConnection,FCalendarID,GetShiftID(aDate,aShift),aDirection);
end; //function TShiftCalendarGrid.GetShiftRecordData


//TDefaultShiftGrid ***
constructor TDefaultShiftGrid.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);
 FMaxShiftsPerDay := cMaxShiftsPerDay;

 ColCount := FMaxShiftsPerDay +1;
 RowCount := cDaysPerWeek +1;
 Width := 310;

 SetColWidths;
 SetRowHeights;
end; //TDefaultShiftGrid.Create
//-----------------------------------------------------------------------------
destructor TDefaultShiftGrid.Destroy;
begin
 inherited Destroy;
end; //destructor TShiftCalendarGrid.Destroy
//-----------------------------------------------------------------------------
procedure TDefaultShiftGrid.SetColWidths;
var
 i : integer;

begin
 ColWidths[0] := 80;
 for i := 1 to ColCount -1 do
  ColWidths[i] := 45;
end; //procedure TDefaultShiftGrid.SetColWidths
//-----------------------------------------------------------------------------
procedure TDefaultShiftGrid.SetMaxShiftsPerDay(Value: integer);
begin
 if Value <> FMaxShiftsPerDay then
 begin
  if Value > cMaxShiftsPerDay then Value := cMaxShiftsPerDay;
  if Value < cMinShiftsPerDay then Value := cMinShiftsPerDay;
  FMaxShiftsPerDay := Value;
  ColCount := FMaxShiftsPerDay +1;
  SetColWidths;
  Refresh;
 end; //if Value <> FMaxShiftsPerDay then
end; //procedure TDefaultShiftGrid.SetMaxShiftsPerDay
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
var
 xIsWeekEndDay : boolean;
 xDow          : integer;

begin
 result.cString := '';

 if aRow = 0 then
 begin
  if aCol = 0 then result.cString := LTrans(FCalendarMessages.FDateStr);
  if aCol = 1 then result.cString := LTrans(FCalendarMessages.FShift1Str);
  if aCol = 2 then result.cString := LTrans(FCalendarMessages.FShift2Str);
  if aCol = 3 then result.cString := LTrans(FCalendarMessages.FShift3Str);
  if aCol = 4 then result.cString := LTrans(FCalendarMessages.FShift4Str);
  if aCol = 5 then result.cString := LTrans(FCalendarMessages.FShift5Str);

  result.fStyle := [fsBold];
  result.fColor := FCalendarColors.FTitleTextColor;
  result.bColor := FCalendarColors.FTitleColor;
 end //if aRow = 0 then
 else
 begin
  result.fStyle := [];
  xDOW := usDOW(aRow);
  xIsWeekEndDay := eWeekendDayT(xDOW-1) in WeekEndDays;
  if aCol = 0 then
  begin
   result.cString := LTrans(GetLongDayNames(bDOW(xDOW)));
   result.fColor := FCalendarColors.FDayTextColor;
   result.bColor := FCalendarColors.FDayColor;

   if xIsWeekEndDay then
   begin
    result.fColor := FCalendarColors.FWeekEndTextColor;
    result.bColor := FCalendarColors.FWeekEndColor;
   end; //if xIsWeekEndDay then
  end //if aCol = 0 then
  else
  begin
   result.cString := FWork.StartTime[aRow,aCol];
   result.fColor := FCalendarColors.FTimeTextColor;
   result.bColor := FCalendarColors.FTimeColor;

   xIsWeekEndDay := xIsWeekEndDay and FShowWeekEndDays;
   if xIsWeekEndDay then
   begin
    result.fColor := FCalendarColors.FWeekEndTextColor;
    result.bColor := FCalendarColors.FWeekEndColor;
   end; //if xIsWeekEndDay then
  end; //else if aCol = 0 then
 end; //if aRow > 0 then
end; //function TDefaultShiftGrid.GetCellInfoAt
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.SelectCell(aCol,aRow: Longint): boolean;
begin
 result := ((aCol *aRow) > 0) and (goEditing in Options);
 if result then result := inherited SelectCell(aCol,aRow);
end; //function TDefaultShiftGrid.SelectCell
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.GetEditMask(aCol,aRow: Longint): string;
begin
 if SelectCell(aCol,aRow) then result := '99:99;1; '
                          else result := '';
end; //function TDefaultShiftGrid.GetEditMask
//-----------------------------------------------------------------------------
procedure TDefaultShiftGrid.SetEditText(aCol,aRow: Longint; const Value: string);
var
 xMod,
 xHour,
 xMin      : integer;
 xHourStr,
 xMinStr,
 xTimeStr  : string;

begin
 xHourStr := copy(Value,1,2);
 xMinStr := copy(Value,4,2);
 xTimeStr := xHourStr +cTimeSeparator +xMinStr;

 if trim(xTimeStr) = cTimeSeparator then xTimeStr := '';
 FWork.StartTime[aRow,aCol] := xTimeStr;
 if xTimeStr = '' then exit;

 try
  xHour := strtoint(xHourStr);
 except
  xHour := 0;
 end; //try..except

 try
  xMin := strtoint(xMinStr);
 except
  xMin := 0;
 end; //try..except

 if xMin >= cMinPerHour then
 begin
  xMin := 0;
  inc(xHour);
 end; //if xMin >= cMinPerHour then

 xMod := xMin mod 5;
 if xMod > 0 then
 begin
  xMin := (xMin div 5) *5;
  if xMod > 2 then inc(xMin,5);
  if xMin = cMinPerHour then
  begin
   xMin := 0;
   inc(xHour);
  end; //if xMin = 60 then
 end; //if xMod > 0 then

 if xHour >= cHoursPerDay then xHour := 0;

 FWork.StartTime[aRow,aCol] := lz(xHour,2) +cTimeSeparator +lz(xMin,2);
end; //procedure TDefaultShiftGrid.SetEditText
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.GetEditText(aCol,aRow: Longint): string;
begin
 result := GetEditTimeStr(aCol,aRow);
end; //function TDefaultShiftGrid.GetEditText
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.GetEditTimeStr(aCol,aRow: longint): string;
begin
 result := '';
 if trim(FWork.StartTime[aRow,aCol]) = '' then result := '  :  '
                                          else result := FWork.StartTime[aRow,aCol];
end; //function TDefaultShiftGrid.GetEditTimeStr
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.IsDifferent: boolean;
var
 xDay,
 xShift : integer;

begin
 for xDay := 1 to cDaysPerWeek do
  for xShift := 1 to cMaxShiftsPerDay do
  begin
   result := FWork.StartTime[xDay,xShift] <> FSave.StartTime[xDay,xShift];
   if result then Exit;
  end; //for shift := 1 to cMaxShiftsPerDay do
end; //function TDefaultShiftGrid.IsDifferent
//-----------------------------------------------------------------------------
procedure TDefaultShiftGrid.ReadShiftData;
var
 xDay,
 xShift : integer;
 xQuery : TmmADODataset;

begin
 fillchar(FWork,sizeof(rDefaultShiftT),0);
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   xQuery.CommandText := 'SELECT * FROM t_default_shift';
   xQuery.CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   CommandText := CommandText + ' ORDER BY c_default_shift_id';
   Open;

   while not Eof do
   begin
    xDay := FieldByName('c_default_shift_id').AsInteger div 10;
    xShift := FieldByName('c_default_shift_id').AsInteger mod 10;
    FWork.StartTime[xDay,xShift] := FieldByName('c_start_time').AsString;
    Next;
   end; //while not Eof do

   FSave := FWork;
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
 Refresh;
end; //procedure TDefaultShiftGrid.ReadShiftData
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.CheckShiftTimes(var aErrorMsg: string): integer;
var
 i,
 xDay,
 xShift     : integer;
 xStartTime : array[1..cMaxShiftsPerDay] of double;
 xLastTime  : double;

begin
 result := 0;
 aErrorMsg := '';
 xShift := 0;
 xDay := 0;

 try
  for xDay := 1 to cDaysPerWeek do
  begin
   for i := 1 to cMaxShiftsPerDay do xStartTime[i] := -2;

   for xShift := 1 to FMaxShiftsPerDay do
    if trim(FWork.StartTime[xDay,xShift]) <> '' then
     xStartTime[xShift] := GetTimeFromStr(FWork.StartTime[xDay,xShift]);

   // 1st shift must have a correct time
   xShift := 1;
   if xStartTime[xShift] < 0 then
   begin
    result := cErrorFirstShiftEmpty;
    exit;
   end; //if xStart[1] < 0 then

   xLastTime := xStartTime[1] +cEpsilon;
   for xShift := 2 to FMaxShiftsPerDay do
   begin
    // current shift time < prev. shift time ??
    if (xStartTime[xShift] < xLastTime) and (xStartTime[xShift] > -1) then
    begin
     result := cErrorShiftOverlapping;
     Exit;
    end; //if (xStartTime[xShift] < xLastTime) and (xStartTime[xShift] > -1) then

    // empty shift between valid shifts ??
    if (xLastTime < 0) and (xStartTime[xShift] > -1) then
    begin
     result := cErrorShiftGap;
     Exit;
    end; //if (xLastTime < 0) and (xStartTime[xShift] > -1) then

    xLastTime := xStartTime[xShift] +cEpsilon;
   end; //for xShift := 2 to FMaxShiftsPerDay do
  end; //for xDay := 1 to cDaysPerWeek do
 finally
  if result > 0 then aErrorMsg := IvDictio.Translate(GetLongDayNames(xDay)) +' - ' +inttostr(xShift);
 end; //try..finally
end; //function TDefaultShiftGrid.CheckShiftTimes
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.CheckShiftDuration(var aErrorMsg: string): integer;
var
 i,
 xShift,
 xAnzahl : integer;
 aShifts : array of rShiftCheckT;

begin
 aShifts := nil;
 result := 0;
 xAnzahl := 0;

 try
  //init shift array
  for i := 1 to cDaysPerWeek do
   for xShift := 1 to FMaxShiftsPerDay do
   begin
    if trim(FWork.StartTime[i,xShift]) <> '' then
    begin
     inc(xAnzahl);
     SetLength(aShifts,xAnzahl);

     with aShifts[high(aShifts)] do
     begin
      Day := i;
      Shift := xShift;
      StartTime := GetTimeFromStr(FWork.StartTime[i,xShift]);
     end; //with aShifts[high(aShifts)] do
    end; //if trim(FWork.StartTime[i,xShift]) <> '' then
   end; //for xShift := 1 to FMaxShiftsPerDay do

  //calc shift duration
  for i := 0 to high(aShifts) -1 do
  begin
   if aShifts[i+1].Day = aShifts[i].Day
    then aShifts[i].Duration := round((aShifts[i+1].StartTime -aShifts[i].StartTime) *cMinPerDay)
    else aShifts[i].Duration := round((aShifts[i+1].StartTime +1 -aShifts[i].StartTime) *cMinPerDay);
  end; //for i := 0 to high(aShifts) -1 do

  i := high(aShifts);
  aShifts[i].Duration := round((aShifts[0].StartTime +1 -aShifts[i].StartTime) *cMinPerDay);

  //check shift duration is valid
  for i := 0 to high(aShifts) do
   with aShifts[i] do
   begin
    if Duration < cMinShiftDuration then result := cErrorMinShiftDuration;
    if Duration > cMaxShiftDuration then result := cErrorMaxShiftDuration;
    if result <> 0 then
    begin
     aErrorMsg := IvDictio.Translate(GetLongDayNames(Day)) +' - ' +inttostr(Shift);
     exit;
    end; //if Duration < cMinShiftDuration then
   end; //with aShifts[i] do
 finally
  aShifts := nil;
 end; //try..finally
end; //function TDefaultShiftGrid.CheckShiftDuration
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.WriteShiftData: boolean;
var
 xDay,
 xShift,
 xShiftID,
 xError    : integer;
 xQuery    : TmmADOCommand;
 xErrorStr : string;

function RecordIsDifferent: boolean;
begin
 result := FWork.StartTime[xDay,xShift] <> FSave.StartTime[xDay,xShift];
end; //function RecordIsDifferent

begin //main
 result := true;
 if FCalendarID = 0 then exit;
 if not IsDifferent then exit;

 xError := CheckShiftTimes(xErrorStr);
 if xError = 0 then xError := CheckShiftDuration(xErrorStr);

 if xError > 0 then
 begin
  result := false;
  CalendarMessages.ShowErrorMessage(xError,xErrorStr);
  Exit;
 end; //if xError > 0 then

 xQuery := TmmADOCommand.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  for xDay := 1 to cDaysPerWeek do
  begin
   for xShift := 1 to cMaxShiftsPerDay do
   begin
    if RecordIsDifferent then
    begin
     //delete existing record
     xShiftID := xDay *10 +xShift;

     xQuery.CommandText := 'DELETE FROM t_default_shift';
     xQuery.CommandText := xQuery.CommandText + format(' WHERE c_default_shift_id = %d',[xShiftID]);
     xQuery.CommandText := xQuery.CommandText + format(' AND c_shiftcal_id = %d',[FCalendarID]);
     xQuery.Execute;

     if FWork.StartTime[xDay,xShift] <> '' then
     begin
      //insert, update records
      xQuery.CommandText := 'INSERT INTO t_default_shift(c_default_shift_id,c_shiftcal_id,c_start_time)';
      xQuery.CommandText := xQuery.CommandText + format(' VALUES(%d,%d,%s)',[xShiftID,FCalendarID,#39 +FWork.StartTime[xDay,xShift] +#39]);
      xQuery.Execute;
     end; //if FWork.StartTime[xDay,xShift] <> '' then
    end; //if RecordIsDifferent then
   end; //for xShift := 1 to FMaxShiftsPerDay do
  end; //for xDay := 1 to DaysPerMonth(FYear,FMonth) do

  FSave := FWork;
 finally
  xQuery.Free;
 end; //try..finally
end; //function TDefaultShiftGrid.WriteShiftData
//-----------------------------------------------------------------------------
function TDefaultShiftGrid.GetCalendarName: string;
var
 xQuery : TmmADODataset;

begin
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := format('SELECT c_shiftcal_name FROM t_shiftcal WHERE c_shiftcal_id = %d',[FCalendarID]);
   Open;
   result := FieldByName('c_shiftcal_name').AsString;
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TDefaultShiftGrid.ReadShiftData


//TShiftPatternGrid ***
constructor TShiftPatternGrid.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);
 FMaxShiftsPerDay := cMaxShiftsPerDay;

 fillchar(FWork,sizeof(rShiftPatternT),0);
 fillchar(FSave,sizeof(rShiftPatternT),0);

 FShiftPatternID := 0;
 FShiftPatternPeriod := 1;
 ColCount := FShiftPatternPeriod +1;
 RowCount := FMaxShiftsPerDay +1;
 Options := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine,goDrawFocusSelected,goRangeSelect,goEditing];
 ScrollBars := ssHorizontal;

 SetColWidths;
 SetRowHeights;
end; //TShiftPatternGrid.Create
//-----------------------------------------------------------------------------
destructor TShiftPatternGrid.Destroy;
begin
 inherited Destroy;
end; //destructor TShiftPatternGrid.Destroy
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetColWidths;
var
 i : integer;

begin
 ColWidths[0] := 20;
 for i := 1 to ColCount -1 do
  ColWidths[i] := 17;
end; //procedure TShiftPatternGrid.SetColWidths
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetMaxShiftsPerDay(Value: integer);
begin
 if Value <> FMaxShiftsPerDay then
 begin
  if Value > cMaxShiftsPerDay then Value := cMaxShiftsPerDay;
  if Value < cMinShiftsPerDay then Value := cMinShiftsPerDay;
  FMaxShiftsPerDay := Value;
  RowCount := FMaxShiftsPerDay +1;
  SetRowHeights;
  Refresh;
 end; //if Value <> FMaxShiftsPerDay then
end; //procedure TShiftPatternGrid.SetMaxShiftsPerDay
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetShiftPatternPeriod(Value: integer);
begin
 if Value <> FShiftPatternPeriod then
 begin
  if Value > cMaxShiftPatternPeriod then Value := cMaxShiftPatternPeriod;
  if Value < 1 then Value := 1;
  FShiftPatternPeriod := Value;
  FWork.Period := Value;
  ColCount := ShiftPatternPeriod +1;
  SetColWidths;
  Refresh;
 end; //if Value <> FShiftPatternPeriod then
end; //procedure TShiftPatternGrid.SetShiftPatternPeriod
//-----------------------------------------------------------------------------
function TShiftPatternGrid.GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
var
 xShift,
 xDay   : integer;

begin
 result.cString := '';
 xDay := aCol;
 xShift := aRow;

 if xShift = 0 then
 begin
  result.fStyle := [fsBold];
  if xDay > 0 then result.cString := inttostr(xDay);
  result.fColor := FCalendarColors.FTitleTextColor;
  result.bColor := FCalendarColors.FTitleColor;
 end //if xShift = 0 then
 else
 begin
  result.fStyle := [];
  if xDay = 0 then
  begin
   if aRow = 1 then result.cString := LTrans(FCalendarMessages.FShift1Str);
   if aRow = 2 then result.cString := LTrans(FCalendarMessages.FShift2Str);
   if aRow = 3 then result.cString := LTrans(FCalendarMessages.FShift3Str);
   if aRow = 4 then result.cString := LTrans(FCalendarMessages.FShift4Str);
   if aRow = 5 then result.cString := LTrans(FCalendarMessages.FShift5Str);

   result.fColor := FCalendarColors.FDayTextColor;
   result.bColor := FCalendarColors.FDayColor;
  end //if xDay = 0 then
  else
  begin
   result.cString := FWork.Team[xDay,xShift];
   result.fColor := FCalendarColors.FTimeTextColor;
   if ShowTeamColors then result.bColor := GetTeamColor(FWork.Team[xDay,xShift])
                     else result.bColor := FCalendarColors.FTimeColor;
  end; //if xDay = 0 then
 end; //else if xShift = 0 then
end; //function TShiftPatternGrid.GetCellInfoAt
//-----------------------------------------------------------------------------
function TShiftPatternGrid.SelectCell(aCol,aRow: Longint): boolean;
begin
 result := ((aCol *aRow) > 0) and (goEditing in Options);
 if result then result := inherited SelectCell(aCol,aRow);
end; //function TShiftPatternGrid.SelectCell
//-----------------------------------------------------------------------------
function TShiftPatternGrid.GetEditMask(aCol,aRow: Longint): string;
begin
 if SelectCell(aCol,aRow) then result := '>c;0; '
                          else result := '';
end; //function TShiftPatternGrid.GetEditMask
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetEditText(aCol,aRow: Longint; const Value: string);
begin
 FWork.Team[aCol,aRow] := trim(Value);
end; //procedure TShiftPatternGrid.SetEditText
//-----------------------------------------------------------------------------
function TShiftPatternGrid.GetEditText(aCol,aRow: Longint): string;
begin
 if trim(FWork.Team[aCol,aRow]) = '' then result := ' '
                                     else result := FWork.Team[aCol,aRow];
end; //function TShiftPatternGrid.GetEditText
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetShiftPatternID(Value: longint);
begin
 if FShiftPatternID <> Value then FShiftPatternID := Value;
end; //procedure TShiftPatternGrid.SetShiftPatternID
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.KeyPress(var Key: Char);
var
 xDay,
 xShift : integer;

begin
 key := UpCase(key);
 if Key in ['0'..'1','A'..'Z',' '] then
 begin
  for xDay := Selection.Left to Selection.Right do
   for xShift := Selection.Top to Selection.Bottom do
    FWork.Team[xDay,xShift] := key;
  Refresh;
 end; //if Key in ['0'..'1','A'..'Z','-',' '] then

 if Key = #8 then
 begin
  for xDay := Selection.Left to Selection.Right do
   for xShift := Selection.Top to Selection.Bottom do
    FWork.Team[xDay,xShift] := '';
  Refresh;
 end; //if Key in [#8] then
 Change;
end; //procedure TShiftPatternGrid.KeyPress
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.KeyUp(var Key: Word; Shift: TShiftState);
begin
 EditorMode := false;
end; //TShiftPatternGrid.KeyUp
//-----------------------------------------------------------------------------
function TShiftPatternGrid.IsDifferent: boolean;
var
 xDay,
 xShift : integer;

begin
 result := FWork.Period <> FSave.Period;
 if result then exit;

 for xDay := 1 to FWork.Period do
  for xShift := 1 to cMaxShiftsPerDay do
  begin
   result := trim(FWork.Team[xDay,xShift]) <> trim(FSave.Team[xDay,xShift]);
   if result then Exit;
  end; //for xShift := 1 to cMaxShiftsPerDay do
end; //function TShiftPatternGrid.IsDifferent
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.SetCursorTo(aDay,aShift: integer);
begin
 MoveColRow(aDay,aShift,true,true);
end; //procedure TShiftPatternGrid.SetCursorTo
//-----------------------------------------------------------------------------
function TShiftPatternGrid.Validate: boolean;
var
 xDay,
 xShift : integer;
 xTeam  : string;

begin
 result := true;
 for xDay := 1 to FWork.Period do
  for xShift := 1 to cMaxShiftsPerDay do
  begin
   xTeam := trim(FWork.Team[xDay,xShift]);
   if xTeam <> '' then result := TeamIsPresent(xTeam);
   if not result then
   begin
    SetCursorTo(xDay,xShift);
    MessageDlg(IvDictio.Translate(FCalendarMessages.PatternErrMsg),mtError,[mbOK],0);
    Exit;
   end; //if not result then
  end; //for xShift := 1 to cMaxShiftsPerDay do
end; //function TShiftPatternGrid.Validate
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.ReadShiftPatternData;
var
 i,
 xDay,
 xShift : integer;
 xQuery : TmmADODataset;

begin
 fillchar(FWork,sizeof(rShiftPatternT),0);
 if FShiftPatternID = 0 then exit;

 i := 1;
 xQuery := TmmADODataset.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  with xQuery do
  begin
   CommandText := 'SELECT * FROM t_shift_pattern';
   CommandText := CommandText + format(' WHERE c_shift_pattern_id = %d',[FShiftPatternID]);
   CommandText := CommandText + ' ORDER BY c_shift_in_id';
   Open;

   while not Eof do
   begin
    xDay := FieldByName('c_shift_in_id').AsInteger div 10;
    xShift := FieldByName('c_shift_in_id').AsInteger mod 10;
    if FieldByName('c_team_id').IsNull then FWork.Team[xDay,xShift] := ''
                                       else FWork.Team[xDay,xShift] := FieldByName('c_team_id').AsString;
    if xDay > i then i := xDay;
    Next;
   end; //while not Eof do

   SetShiftPatternPeriod(i);
   FWork.Period := i;

   FSave := FWork;
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
 Refresh;
end; //procedure TShiftPatternGrid.ReadShiftPatternData
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.WriteShiftPatternData;
var
 xDay,
 xShift : integer;
 xQuery : TmmADOCommand;

//----------
function RecordIsDifferent: boolean;
begin
 result := (FWork.Team[xDay,xShift] <> FSave.Team[xDay,xShift]) or (xDay > FSave.Period);
end; //function RecordIsDifferent
//----------

begin //main
 if FShiftPatternID = 0 then exit;
 if not IsDifferent then exit;
 if not Validate then exit;

 xQuery := TmmADOCommand.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  //delete all records > pattern period
  xQuery.CommandText := 'DELETE FROM t_shift_pattern';
  xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shift_pattern_id = %d',[FShiftPatternID]);
  xQuery.CommandText := xQuery.CommandText + format(' AND c_shift_in_id >= %d',[(FWork.Period +1)*10]);
  xQuery.Execute;

  for xDay := 1 to FWork.Period do
   for xShift := 1 to cMaxShiftsPerDay do
    if RecordIsDifferent then
    begin
     //delete existing record
     xQuery.CommandText := 'DELETE FROM t_shift_pattern';
     xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shift_pattern_id = %d',[FShiftPatternID]);
     xQuery.CommandText := xQuery.CommandText + format(' AND c_shift_in_id = %d',[xDay *10 +xShift]);
     xQuery.Execute;

     //insert new record
     if FWork.Team[xDay,xShift] <> '' then
     begin
      xQuery.CommandText := 'INSERT INTO t_shift_pattern(c_shift_pattern_id,c_shift_in_id,c_team_id)';
      xQuery.CommandText := xQuery.CommandText + format(' VALUES(%d,%d,%s)',[FShiftPatternID,xDay *10 +xShift,#39 +FWork.Team[xDay,xShift] +#39]);
      xQuery.Execute;
     end; //if FWork.Team[xDay,xShift] <> '' then
    end; //if RecordIsDifferent then

  FSave := FWork;
 finally
  xQuery.Free;
 end; //try..finally
end; //function TShiftPatternGrid.WriteShiftPatternData
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.DeleteShiftPattern(aPatternID: longint);
var
 xQuery : TmmADOCommand;

begin
 xQuery := TmmADOCommand.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  xQuery.CommandText := 'DELETE FROM t_shift_pattern';
  xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shift_pattern_id = %d',[aPatternID]);
  xQuery.Execute;
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TShiftPatternGrid.DeleteShiftPattern
//-----------------------------------------------------------------------------
procedure TShiftPatternGrid.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end; //procedure TShiftPatternGrid.Change

//------------------------------------------------------------------------------
//TAppendDefaultShifts ***
constructor TAppendDefaultShifts.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);
 FCalendarID := 0;
 FConnection := nil;
 FHideExceptions := false;;
end; //constructor TAppendDefaultShifts.Create
//-----------------------------------------------------------------------------
destructor TAppendDefaultShifts.Destroy;
begin
 inherited Destroy;
end; //destructor TAppendDefaultShifts.Destroy
//-----------------------------------------------------------------------------
procedure TAppendDefaultShifts.SetCalendarID(Value: longint);
begin
 if FCalendarID <> Value then FCalendarID := Value;
end; //procedure TAppendDefaultShifts.SetCalendarID
//-----------------------------------------------------------------------------

//******************************************************************************
// Setzt neue Schichten in den Schichtkaleneder ein
// Es werden pro Schichtkalender, Standardschichten fuer 7 Tage in der
// Zukunft auf die DB geschrieben
//******************************************************************************
function TAppendDefaultShifts.Execute: shortint;
var
 xDay,
 xShift                  : integer;
 xQuery                  : TmmADODataset;
 xStartDate, xEndDate,
 xShiftStart, xDateTime  : TDateTime;
 xDSRec                  : rDefaultShiftT;
 xCheckBegin             : rShiftCheckT;
 xSQLText                : String;
 Year, Month, Day        : Word;
 xText                   : String;
const cAddDays = 8;

begin
 result := 0;

 if not Assigned(FConnection) then
 begin
  result := -1;
  if FHideExceptions then Abort
                     else raise Exception.Create('Connection not assigned!');
 end; // if not Assigned(FConnection) then

 xText := 'not in InTransaction ';
 if FConnection.InTransaction then xText := 'in InTransaction ';
 FConnection.BeginTrans;

 xQuery := TmmADODataset.Create(NIL);
 try
  xQuery.ConnectionString := GetDefaultConnectionString;
  try

   codesite.SendMsg('TAppendDefaultShifts.Execute:  ' +  xText);

   // Get Start and End Date from a shiftcal

   xSQLText:= Format('select distinct max ( c_shift_start ) c_shift_start ' +
                     'from t_shift where c_shiftcal_id = %d ', [FCalendarID]);
   xQuery.Close;
   xQuery.CommandText := xSQLText;
   xQuery.Open;

   // 0. Init from xStartDate and xEndDate

   // Set begin of shiftdate
   xDateTime:= xQuery.FieldByName('c_shift_start').AsDateTime;
   if xDateTime <= 0 then
      xDateTime:= Sysutils.Now -1;
   DecodeDate(xDateTime, Year, Month, Day);
   xStartDate:=  EncodeDate(Year, Month, Day) + EncodeTime(0, 0, 0, 0) + 1; // + 1 Tg

   // Set end of shiftdate
   xEndDate := Sysutils.Date + cAddDays + EncodeTime(0, 1, 0, 0);


   //1. read default shift data
   fillchar(xDSRec,sizeof(rDefaultShiftT),0);
   xQuery.Close; //SDO
   xQuery.CommandText := 'SELECT * FROM t_default_shift';
   xQuery.CommandText := xQuery.CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
   xQuery.CommandText := xQuery.CommandText + ' ORDER BY c_default_shift_id';
   xQuery.Open;

   if xQuery.Eof then
   begin
    result := -2;
    if FHideExceptions then Abort
                       else raise Exception.Create('No default shifts found!');
   end; //if xQuery.Eof then

   while not xQuery.Eof do
   begin
    xDay := xQuery.FieldByName('c_default_shift_id').AsInteger div 10;
    xShift := xQuery.FieldByName('c_default_shift_id').AsInteger mod 10;
    xDSRec.StartTime[xDay,xShift] := trim(xQuery.FieldByName('c_start_time').AsString);
    xQuery.Next;
   end; //while not xQuery.Eof do
   xQuery.Active := false;


   //3. check shift duration of last record before "begin of copy period"
   // Die juengsten Shiftdaten eines shiftcals ermitteln
   xCheckBegin := GetShiftData(FConnection,FCalendarID,GetShiftID(xStartDate,1),sdPrev);
   if xCheckBegin.ShiftID > 0 then
   begin
    // Wochentag ermitteln -> Mo= 1,  So = 7
    xDay := bDOW(DayOfWeek(xStartDate));
    // Typen Umwandlung von Schichtkalenderdatum (Zelle) in TDateTime
    // -> Schichtbegin = Datum + Zeit aus Schichtkalender
    xShiftStart := xStartDate + GetTimeFromStr(xDSRec.StartTime[xDay,1]);
    xCheckBegin.Duration := round( (xShiftStart - xCheckBegin.StartTime) * cMinPerDay);
    //Schichtdauer muss zwischen 30 und 1440
    if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then
    begin
     result := -4;
     if FHideExceptions then Abort
                        else raise Exception.CreateFmt('Shift duration of %d/%d/%d - %d',[xCheckBegin.Day,xCheckBegin.Month,xCheckBegin.Year,xCheckBegin.Shift]);
    end; //if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then
   end; //if xCheckBegin.ShiftID > 0 then

    with TmmAdoCommand.Create(nil) do try
     //4. write default shift data into t_shift
     while xStartDate <= xEndDate do
     begin
      xDay := bDOW(DayOfWeek(xStartDate));

      ConnectionString := GetDefaultConnectionString;
      for xShift := 1 to cMaxShiftsPerDay do
       if xDSRec.StartTime[xDay,xShift] <> '' then
       begin
        CommandText := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_shift_in_day,c_shift_start,c_shift_length)';
        CommandText := CommandText + ' VALUES(:shift_id,:shiftcal_id,:shift_in_day,:shift_start,:shift_length)';
        Parameters.ParamByname('shift_id').value := GetShiftID(xStartDate,xShift);
        Parameters.ParamByname('shiftcal_id').value := FCalendarID;
        Parameters.ParamByname('shift_in_day').value := xShift;
        Parameters.ParamByname('shift_start').DataType := ftDateTime;
        Parameters.ParamByname('shift_start').value := xStartDate +GetTimeFromStr(xDSRec.StartTime[xDay,xShift]);
        Parameters.ParamByname('shift_length').value := GetDS_ShiftDuration(xDSRec,xDay,xShift);
        Execute;
       end; //for xShift := 1 to cMaxShiftsPerDay do

      xStartDate := xStartDate + 1;
     end; //while xStartDate <= xEndDate do

     //5. update shift duration of last record before "begin of copy period"
     if xCheckBegin.ShiftID > 0 then
     begin
      CommandText := 'UPDATE t_shift';
      CommandText := CommandText + format(' SET c_shift_length = %d',[xCheckBegin.Duration]);
      CommandText := CommandText + format(' WHERE c_shiftcal_id = %d',[FCalendarID]);
      CommandText := CommandText + format(' AND c_shift_id = %d',[xCheckBegin.ShiftID]);
      Execute;
     end; //if xCheckBegin.ShiftID > 0 then
    finally
      free;
    end;// with TmmAdoCommand.Create(nil) do try

   FConnection.CommitTrans;
  except
   FConnection.RollbackTrans;
   result := -5;
   if result < 0 then raise
                 else raise Exception.Create('Error inserting new shifts!');
  end; //try..except
 finally
  xQuery.Free;
  if FConnection.InTransaction then FConnection.RollbackTrans;
 end; //try..finally
end; //function TAppendDefaultShifts.Execute
//------------------------------------------------------------------------------




//TFactoryCalendar ***
constructor TFactoryCalendar.Create(aOwner: TComponent);
begin
 inherited Create(aOwner);

 ColCount := 7;
 ColWidth := 18;
 FixedCols := 0;
 FMonth := GetCurrentMonth;
 FStartOfWeek := wdMonday;
 FShowWeekEndTitle := false;
 FYear := GetCurrentYear;
 Options := [goFixedVertLine,goFixedHorzLine,goVertLine,goHorzLine];
 RowCount := 7;
 RowHeight := 13;

 GetMonthOffset;

 DefaultColWidth := ColWidth;
 DefaultRowHeight := RowHeight;
end; //constructor TFactoryCalendar.Create
//-----------------------------------------------------------------------------
destructor TFactoryCalendar.Destroy;
begin
 inherited Destroy;
end; //destructor TFactoryCalendar.Destroy
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.Change;
begin
 if Assigned(FOnChange) then FOnChange(Self);
end; //procedure TFactoryCalendar.Change
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.DblClick;
var
 xDay : integer;

begin
 if FReadOnly then exit;
 xDay := GetDay(Col,Row);
 if xDay > 0 then
  if Date <= EncodeDate(FYear,FMonth,xDay) then
  begin
   FWork[xDay] := not FWork[xDay];
   Refresh;
   Change;
  end //if Date <= EncodeDate(FYear,FMonth,aDay) then
  else
  begin
   MessageBeep(0);
   MessageDlg(IvDictio.Translate(FCalendarMessages.DaysInPastErrorMsg),mtError,[mbOk],0);
  end; //else if Date <= EncodeDate(FYear,FMonth,aDay)
end; //procedure TFactoryCalendar.DblClick
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.KeyPress(var Key: Char);
begin
 if key = #32 then DblClick;
end; //procedure TFactoryCalendar.KeyPress
//-----------------------------------------------------------------------------
function TFactoryCalendar.DaysThisMonth: integer;
begin
 result := DaysPerMonth(FYear,FMonth);
end; //function TFactoryCalendar.DaysThisMonth
//-----------------------------------------------------------------------------
function TFactoryCalendar.GetCellInfoAt(aCol,aRow: Longint): rCellInfoT;
var
 xDay          : word;
 xIsWeekEndDay : boolean;
 xColWeekDay   : eWeekendDayT;

begin
 xColWeekDay := eWeekendDayT((ord(FStartOfWeek) +aCol) mod 7);
 xIsWeekEndDay := (xColWeekDay in FWeekEndDays) and ShowWeekEndDays;

 result.fStyle := [];
 result.cString := '';
 if aRow = 0 then
 begin
  if (csDesigning in ComponentState) then result.cString := GetShortDayNames(bDOW((ord(FStartOfWeek) +aCol) mod 7 +1))
                                     else result.cString := IvDictio.Translate(GetShortDayNames(bDOW((ord(FStartOfWeek) +aCol) mod 7 +1)));
  result.fColor := FCalendarColors.FTitleTextColor;
  result.bColor := FCalendarColors.FTitleColor;

  if ShowWeekEndTitle and (xColWeekDay in FWeekEndDays) then
  begin
   result.fColor := FCalendarColors.FWeekEndTextColor;
   result.bColor := FCalendarColors.FWeekEndColor
  end; //if ShowWeekEndTitle and (xColWeekDay in FWeekEndDays) then
 end //if aRow = 0 then
 else
 begin
  if xIsWeekEndDay then
  begin
   result.fColor := FCalendarColors.FWeekEndTextColor;
   result.bColor := FCalendarColors.FWeekEndColor
  end //if xIsWeekEndDay then
  else
  begin
   result.fColor := FCalendarColors.FDayTextColor;
   result.bColor := FCalendarColors.FDayColor;
  end; //else if xIsWeekEndDay then

  xDay := GetDay(aCol,aRow);
  if xDay > 0 then
  begin
   result.cString := inttostr(xDay);
   if FWork[xDay] then
   begin
    result.fColor := FCalendarColors.FOffDayTextColor;
    if xIsWeekEndDay then result.bColor := FCalendarColors.FWeekEndColor
                     else result.bColor := FCalendarColors.FOffDayColor;
    result.fStyle := [fsBold];
   end; //if FWork[aDay] then
  end; //if xDay > 0 then
 end; //else if aRow = 0 then
end; //function TFactoryCalendar.GetCellInfoAt
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.DrawCell(aCol,aRow: Longint; ARect: TRect; AState: TGridDrawState);
var
 xCellInfo : rCellInfoT;

begin
 xCellInfo := GetCellInfoAt(aCol,aRow);

 Canvas.Font.Style := xCellInfo.fStyle;
 Canvas.Font.Color := xCellInfo.fColor;
 Canvas.Brush.Color := xCellInfo.bColor;

 with ARect, Canvas do
 begin
  FillRect(ARect);
  TextRect(ARect,Left +(Right -Left -TextWidth(xCellInfo.cString)) div 2,
           Top +(Bottom -Top -TextHeight(xCellInfo.cString)) div 2,xCellInfo.cString);
 end; //with ARect, Canvas do
end; //procedure TFactoryCalendar.DrawCell
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetStartOfWeek(Value: eWeekendDayT);
begin
 if Value <> FStartOfWeek then
 begin
  FStartOfWeek := Value;
  GetMonthOffset;
  Refresh;
 end; //if Value <> FStartOfWeek then
end; //procedure TFactoryCalendar.SetStartOfWeek
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetColWidth(Value: integer);
begin
 if Value <> FColWidth then
 begin
  FColWidth := Value;
  DefaultColWidth := Value;
  if BorderStyle = bsSingle then Width := ColCount *DefaultColWidth +(ColCount +1)
                            else Width := ColCount *DefaultColWidth +(ColCount -1);
  Refresh;
 end; //if Value <> FColWidth then
end; //procedure TFactoryCalendar.SetColWidth
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetRowHeight(Value: integer);
begin
 if Value <> FRowHeight then
 begin
  FRowHeight := Value;
  DefaultRowHeight := Value;
  if BorderStyle = bsSingle then Height := RowCount *DefaultRowHeight +(RowCount +1)
                            else Height := RowCount *DefaultRowHeight +(RowCount -1);
  Refresh;
 end; //if Value <> FRowHeight then
end; //procedure TFactoryCalendar.SetRowHeight
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetShowWeekEndTitle(Value: boolean);
begin
 if FShowWeekEndTitle <> Value then
 begin
  FShowWeekEndTitle := Value;
  Refresh;
 end; //if FShowWeekEndTitle <> Value then
end; //procedure TFactoryCalendar.SetShowWeekEndTitle
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.ReadExceptDays;
var
 xQuery : TmmADODataset;

begin
 fillchar(FWork,sizeof(FWork),0);
 if FCalendarID = 0 then exit;

 xQuery := TmmADODataset.Create(NIL);
 try
 xQuery.ConnectionString := GetDefaultConnectionString;
  with xQuery do
  begin
   CommandText := 'SELECT DATEPART(day,c_except_date) AS day FROM t_except_days';
   CommandText := CommandText + format(' WHERE c_factory_calendar_id = %d',[FCalendarID]);
   CommandText := CommandText + format(' AND DATEPART(month,c_except_date) = %d',[FMonth]);
   CommandText := CommandText + format(' AND DATEPART(year,c_except_date) = %d',[FYear]);
   Open;

   while not Eof do
   begin
    FWork[FieldByName('day').asinteger] := true;
    Next;
   end; //while not Eof do

   FSave := FWork;
  end; //with xQuery do
 finally
  xQuery.Free;
 end; //try..finally
 Refresh;
end; //procedure TFactoryCale1ndar.ReadExceptDays
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.WriteExceptDays;
var
 xDay   : word;
 xQuery : TmmADOCommand;

begin
 if FCalendarID = 0 then exit;
 if not IsDifferent then exit;

 xQuery := TmmADOCommand.Create(NIL);
 xQuery.ConnectionString := GetDefaultConnectionString;
 try
  //1. alte daten loeschen...
  xQuery.CommandText := 'DELETE FROM t_except_days';
  xQuery.CommandText := xQuery.CommandText + format(' WHERE c_factory_calendar_id = %d',[FCalendarID]);
  xQuery.CommandText := xQuery.CommandText + format(' AND DATEPART(month,c_except_date) = %d',[FMonth]);
  xQuery.CommandText := xQuery.CommandText + format(' AND DATEPART(year,c_except_date) = %d',[FYear]);
  xQuery.Execute;

  xQuery.CommandText := 'INSERT INTO t_except_days(c_factory_calendar_id,c_except_date) VALUES(:CalendarID,:ExceptDate)';

  for xDay := 1 to DaysThisMonth do
   if FWork[xDay] then
   begin
    xQuery.Parameters.ParamByName('CalendarID').value := FCalendarID;
    xQuery.Parameters.ParamByName('ExceptDate').DataType := ftDateTime;
    xQuery.Parameters.ParamByName('ExceptDate').value := EncodeDate(FYear,FMonth,xDay);
    xQuery.Execute;
   end; //if FWork[xDay] then

  FSave := FWork;
 finally
  xQuery.Free;
 end; //try..finally
end; //procedure TFactoryCalendar.WriteExceptDays
//-----------------------------------------------------------------------------
function TFactoryCalendar.IsDifferent: boolean;
var
 i : integer;

begin
 result := false;
 for i := 1 to DaysThisMonth do
 begin
  result := FWork[i] <> FSave[i];
  if result then break;
 end; //for i := 1 to DaysThisMonth do
end; //function TFactoryCalendar.IsDifferent
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetMonth(Value: integer);
begin
 if Value <> FMonth then
 begin
  FMonth := Value;
  GetMonthOffset;
  if csDesigning in ComponentState then Refresh;
 end; //if Value <> FMonth then
end; //procedure TFactoryCalendar.SetMonth
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.SetYear(Value: integer);
begin
 if Value <> FYear then
 begin
  FYear := Value;
  GetMonthOffset;
  if csDesigning in ComponentState then Refresh;
 end; //if Value <> FYear then
end; //procedure TFactoryCalendar.SetYear
//-----------------------------------------------------------------------------
procedure TFactoryCalendar.GetMonthOffset;
begin
 FMonthOffset := 2 - ((DayOfWeek(encodedate(FYear,FMonth,1)) -ord(FStartOfWeek) + cDaysPerWeek) mod cDaysPerWeek);
 if FMonthOffset = 2 then FMonthOffset := -5;
end; //procedure TFactoryCalendar.GetMonthOffset
//-----------------------------------------------------------------------------
function TFactoryCalendar.GetDay(aCol,aRow: longint): integer;
begin
 result := FMonthOffset +aCol +(aRow -1) *cDaysPerWeek;
 if (result < 1) or (result > DaysThisMonth) then result := 0;
end; //function TFactoryCalendar.GetDay
//-----------------------------------------------------------------------------
end. 




