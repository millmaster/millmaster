//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: MMAlert.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 20.03.2001  0.00  khp | Datei erstellt
// 05.10.2002        LOK | Umbau ADO
// 19.01.2004        Wss | reQOfflimit Element entfernt
// 25.10.2004        Nue | TriggerEventToAutoprint CoInitialize aufruf korrigiert
//========================================================================================*)

{ unit MMAlert -----------------------------------------------------------------------------}
//   In der Unit MMAlert sind Funktionen implementiert mit welchen aus dem Basis-System
//   heraus Meldungen und Events abgesetzt werden koennen. Unterstuetzt werden zur Zeit Automatische
//   Protokolle mit dem Automatic Print Service der Floor

unit MMAlert;

interface
uses
  Windows, SysUtils, Classes,
  Baseglobal;

type
  {Reihefolge muss identisch mit EvtList.TXT sein,
   welche fuer den Automatic Printout verwendet wird}
  TReportEvent = (reNone, re1Shift, re2shift, re3shift, re4hift, re5shift);


procedure SMS ();
Procedure Pager();
function  TriggerEventToAutoprint (aTriggerEvent: TReportEvent): TErrorRec;


{ Test Routine fuer Handlecount des Prozessses
 function QueryProcessInformation(aPID: DWord; aInfoEnum: Integer; var aBuffer; aSize: Integer): Boolean;

 function NtQueryInformationProcess(hProcess: THandle; ProcessInformationClass: integer;
                                     var ProcessInformation; ProcessInformationLength: integer;
                                     var ReturnLength: integer): integer; StdCall; external 'NTDLL.DLL';
}

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  LoepfeGlobal,Mailslot, SettingsReader, MMBaseClasses,ActiveX,
  AUTOPRINTSERVICELib_TLB, MMICOMLib_TLB;



//-----------------------------------------------------------------------------------------
{ Test Routine fuer Handlecount des Prozessses
function QueryProcessInformation(aPID: DWord; aInfoEnum: Integer; var aBuffer; aSize: Integer): Boolean;
var
  xHnd: THandle;
  xRetLen: Integer;
  xRetValue: Integer;
begin
  Result := False;
  xHnd := OpenProcess(PROCESS_QUERY_INFORMATION, False, aPID);
  if xHnd = Null then
    Exit;

  // Zero the input buffer that NtQueryInformationProcess sees
  FillChar(aBuffer, aSize, 0);
  xRetValue := NtQueryInformationProcess(xHnd, aInfoEnum, aBuffer, aSize, xRetLen);
  CloseHandle(xHnd);
  if xRetValue < 0  then // NtQueryInformationProcess returns a negative
    Exit;

  Result := True;
end;
}






{SMS -------------------------------------------------------------------------------------}
//  Meldung mit max 160 Zeichen wird dem Handy uebermittelt }
procedure SMS ();

  var
    mAlert: TMMICtl;
  begin
    mAlert:= TMMICtl.Create(nil);
    mAlert.SyncInit(100);
    mAlert.AboutBox;
    mAlert.SyncExit(100);
  end;{Procedure SMS----------------------------------------------------------------------}



{Pager -----------------------------------------------------------------------------------}
//   Meldungsnummer wird dem Pager uebermittelt }
Procedure Pager();

  var
    mAlert: TMMICtl;
    x:integer;
  begin
    mAlert:= TMMICtl.Create(nil);
    mAlert.SyncInit(100);
    mAlert.LibraryConfigDlg(0,x);
    mAlert.SyncExit(100);
  end;{Procedure pager --------------------------------------------------------------------}


  
{TriggerEventToAutoprint-------------------------------------------------------------------}
//   Aktiviert den Automatic Printservice der Floor miitels eines TriggerEvents. Falls der Autoprint
//   Service nicht lauft wird eine Fehlermeldung zurueckgegeben. Ungueltige Events werden nicht beachtet.
//   Folgende Events werden unterstuetzt:
//   RepEventtype = reNone, reQOfflimit, re1Shift, re2shift, re3shift, re4hift, re5shift


function TriggerEventToAutoprint (aTriggerEvent: TReportEvent): TErrorRec;
var
  xIntf: IAutoOutputNotify;
  xhresult: Longint;
begin
  result:= SetError(No_error,etNoError,'');
  try
    xhresult := CoInitialize(Nil); //initializes the Component Object Model(COM) library
    if (xhresult <> S_OK) and (xhresult <> S_FALSE) then
       raise Exception.Create('CoInitialize failed ');
    try
      xIntf := CoAutoOutputNotify.Create;
      xIntf.TriggerEvent(Integer(aTriggerEvent), 1);
    finally
      xIntf := Nil;
      CoUninitialize;
    end;
  except
    on e:Exception do
      Result := SetError ( ERROR_SERVICE_DOES_NOT_EXIST, etNTError, 'TriggerEventToAutoprint failed. ' + e.Message );
  end;
end; {TriggerEventToAutoprint------------------------------------------------------------}

//function TriggerEventToAutoprint (aTriggerEvent: TReportEvent): TErrorRec;
//
//  var
//    xIntf: IAutoOutputNotify;
//    xhresult: Longint;
//  begin
//    result:= SetError(No_error,etNoError,'');
//    try
//      try
//        xhresult:= CoInitialize(Nil); //initializes the Component Object Model(COM) library
//        if (xhresult <> S_OK) and (xhresult <> S_FALSE) then
//           raise Exception.Create ( 'CoInitializ failed ');
//        xIntf:= CoAutoOutputNotify.Create;
//        xIntf.TriggerEvent(Integer(aTriggerEvent), 1);
//      except
//        on e:Exception do
//            Result := SetError ( ERROR_SERVICE_DOES_NOT_EXIST, etNTError, 'TriggerEventToAutoprint failed. ' + e.Message );
//      end;
//    finally
//      xIntf := Nil;
//      CoUninitialize;
//    end;
//  end; {TriggerEventToAutoprint------------------------------------------------------------}
end.
