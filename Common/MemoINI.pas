(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MemoINI.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.09.99 | 0.01 | PW  | Datei erstellt
| 25.09.99 | 0.02 | PW  |
| 16.10.99 | 0.03 | PW  | db-column "c_Admin" added to identify administrator-profiles
| 27.02.02 | 0.03 | Wss | Query in ReadSettings angepasst. Bei Profil laden wurde das selektierte nicht
                          geholt, da in Query Filter c_default = 1 drin war
| 04.10.2002        LOK | Umbau ADO
| 08.10.2002        Wss | c_admin wird beim Profil speichern nicht mehr von Benutzerrechten abh�ngig
                          gemacht, sondern default = 0. Es f�hrte zu verwirrung, wenn ein User
                          Adminrechte hatte und die Sortierung schliesslich �ber den Benutzernamen erfolgte.
                          Es wurde z.B. immer das Profile von MMSupp geladen, da MMSystemUser per sortierung
                          erst nachher kommt, obwohl dieser Benutzer ebenfalls Adminrechte hat.
|=========================================================================================*)

unit MemoIni;
interface
uses
  Classes, Controls, DB, Forms , mmADOQuery, LoepfeGlobal;

const
  cParamTable = 't_settings';
  cMemoField = 'c_text';
  cUsername = 'c_username';
  cApplicationname = 'c_applicationname';
  cFormname = 'c_formname';
  cDefault = 'c_default';
  cProfilename = 'c_profilename';
  cAdmin = 'c_admin';


type
  tSelectProfileRecordT = record
    ID: integer;
    Formname,
      Username,
      Profilename: string;
    Default,
      IsAdmin: boolean;
  end;                                  //tSelectProfileRecordT

  tSelectProfileRecordArrayT = array of tSelectProfileRecordT;


  TMemoIni = class(TPersistent)
  private
//    fxDatabase: TmmDatabase;
    FListBuffer: TStringList;
    FApplicationname: string;
    FFormname: string;
    FProfilename: string;
    FUsername: string;
    FAdoConnectionString: string;
    function GetName(const Line: string): string;
    function GetValue(const Line, Name: string): string;
    function IsSection(const Line: string): Boolean;
    function GetSectionIndex(const Section: string): integer;
    function GetApplicationname: string;
    procedure SetApplicationname(Value: string);
    procedure SetFormname(Value: string);
    procedure SetProfilename(Value: string);
    procedure SetUsername(Value: string);
  protected
    mOwner: TComponent;
  public
    constructor Create; overload;
    constructor Create(aOwner: TComponent); overload;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;

    procedure DeleteKey(const Section, Name: string);
    procedure EraseSection(const Section: string);
    procedure ReadSection(const Section: string; Strings: TStrings);
    procedure ReadSections(Strings: TStrings);
    procedure ReadSectionValues(const Section: string; strings: TStrings);
    function ReadBool(const Section, Name: string; aDefault: boolean): boolean;
    function ReadDate(const Section, Name: string; aDefault: TDateTime): TDateTime;
    function ReadFloat(const Section, Name: string; aDefault: double): double;
    function ReadInteger(const Section, Name: string; aDefault: longint): Longint;
    function ReadString(const Section, Name: string; aDefault: string): string;
    procedure WriteBool(const Section, Name: string; value: Boolean);
    procedure WriteDate(const Section, Name: string; Value: TDateTime);
    procedure WriteFloat(const Section, Name: string; Value: double);
    procedure WriteInteger(const Section, Name: string; value: Longint);
    procedure WriteString(const Section, Name: string; Value: string);

    function GetDefaultProfile: string;
    function GetFormProfiles: TStringList;
    function SelectProfile: tSelectProfileRecordT;
    function SaveProfile: tSelectProfileRecordT;
    procedure DeleteProfile;
    procedure DeleteProfileDlg;
    procedure ReadSettings;
    procedure SetDefaultProfile;
    procedure WriteSettings;
  published
//    property Database: TmmDatabase read FxDatabase write FxDatabase;
    property Applicationname: string read FApplicationname write SetApplicationname;
    property Formname: string read FFormname write SetFormname;
    property Profilename: string read FProfilename write SetProfilename;
    property Username: string read FUsername write SetUsername;
    property AdoConnectionString: string read FAdoConnectionString write FAdoConnectionString;
  end;                                  //TMemoIni


  TBaseMemoINIObject = class(TObject)
  private
    FMemoINI: TMemoIni;
    procedure SetMemoINI(value: TMemoINI);
    function GetMemoINI: TMemoINI;
  protected
  public
    constructor Create; overload;
    constructor Create(aOwner: TComponent); overload;
    destructor Destroy; override;
  published
    property MemoINI: TMemoIni read GetMemoINI write SetMemoINI;
  end;                                  //TProgOptions

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf,
  SysUtils,
  mmCommonLib, mmSelectProfile, mmDeleteProfile, mmSaveProfile;

resourcestring
  cStringsUnassigned = 'Parameter strings not assigned';
  cDatabaseUnassigned = 'Parameter Database not assigned';

const
  cBrackets: array[0..1] of Char = ('[', ']');
  cSeparator: Char = '=';


//TMemoIni
constructor TMemoIni.Create;
begin
  FListBuffer := TStringList.Create;
  mOwner := nil;
end;                                    //constructor TMemoIni.Create
//-----------------------------------------------------------------------------
constructor TMemoIni.Create(aOwner: TComponent);
begin
  Create;
  mOwner := aOwner;
end;                                    //constructor TMemoIni.Create
//-----------------------------------------------------------------------------
destructor TMemoIni.Destroy;
begin
  FListBuffer.Free;
end;                                    //destructor TMemoIni.Destroy
//-----------------------------------------------------------------------------
function TMemoIni.GetName(const line: string): string;
var
  i: integer;

begin
  i := AnsiPos(cSeparator, line);
  if i <> 0 then result := trim(system.copy(Line, 1, i - 1))
  else result := EmptyStr;
end;                                    //function TMemoIni.GetName
//-----------------------------------------------------------------------------
function TMemoIni.GetValue(const line, name: string): string;
var
  i, j: integer;

begin
  result := EmptyStr;
  if (line <> EmptyStr) and (name <> EmptyStr) then begin
    i := AnsiPos(name, line);
    j := AnsiPos(cSeparator, line);
    if (i <> 0) and (j <> 0) and (j > i) then result := trim(system.copy(line, j + 1, MaxInt));
  end;                                  //if (line <> EmptyStr) and (name <> EmptyStr) then
end;                                    //function TMemoIni.GetValue
//-----------------------------------------------------------------------------
function TMemoIni.GetApplicationname: string;
begin
  result := trim(FApplicationname);
  if result = '' then result := uppercase(Application.Title);
end;                                    //function TMemoIni.GetApplicationname
//-----------------------------------------------------------------------------
procedure TMemoIni.SetApplicationname(Value: string);
begin
  FApplicationname := uppercase(Value);
end;                                    //procedure TMemoIni.SetApplicationname
//-----------------------------------------------------------------------------
procedure TMemoIni.SetFormname(Value: string);
begin
  FFormname := uppercase(Value);
end;                                    //procedure TMemoIni.SetFormname
//-----------------------------------------------------------------------------
procedure TMemoIni.SetProfilename(Value: string);
begin
  FProfilename := uppercase(Value);
end;                                    //procedure TMemoIni.SetProfilename
//-----------------------------------------------------------------------------
procedure TMemoIni.SetUsername(Value: string);
begin
  FUsername := trim(uppercase(Value));
end;                                    //procedure TMemoIni.SetUsername
//-----------------------------------------------------------------------------
function TMemoIni.IsSection(const line: string): Boolean;
var
  s: string;

begin
  result := false;
  if line <> EmptyStr then begin
    s := trim(Line);
    if (s[1] = cBrackets[0]) and (s[system.length(s)] = cBrackets[1]) then result := true;
  end;                                  //if line <> EmptyStr then
end;                                    //function TMemoIni.IsSection
//-----------------------------------------------------------------------------
function TMemoIni.GetSectionIndex(const section: string): integer;
begin
  Result := FListBuffer.IndexOf(cBrackets[0] + Section + cBrackets[1]);
end;                                    //function TMemoIni.GetSectionIndex
//-----------------------------------------------------------------------------
procedure TMemoIni.ReadSection(const section: string; strings: TStrings);
var
  i: integer;
  n: string;

begin
  if not Assigned(Strings) then raise Exception.Create(cStringsUnassigned);
  strings.BeginUpdate;
  try
    strings.Clear;
    if FListBuffer.Count > 0 then begin
      i := GetSectionIndex(Section);
      if i <> -1 then begin
        Inc(i);
        while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do begin
          n := GetName(FListBuffer[i]);
          if n <> EmptyStr then strings.Add(n);
          inc(i);
        end;                            //while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do
      end;                              //if i <> -1 then
    end;                                //if FListBuffer.Count > 0 then
  finally
    strings.EndUpdate;
  end;                                  //try..finally
end;                                    //procedure TMemoIni.ReadSection
//-----------------------------------------------------------------------------
procedure TMemoIni.ReadSections(strings: TStrings);
var
  i: integer;
  Section: string;

begin
  if not Assigned(Strings) then raise Exception.Create(cStringsUnassigned);
  Strings.BeginUpdate;
  try
    Strings.Clear;
    if FListBuffer.Count > 0 then begin
      i := 0;
      while (i < FListBuffer.Count) do begin
        if IsSection(FListBuffer[i]) then begin
          section := trim(FListBuffer[i]);
          system.delete(section, 1, 1);
          system.delete(section, system.length(section), 1);
          strings.add(trim(section));
        end;                            //if IsSection(FListBuffer[i]) then
        Inc(i);
      end;                              //while (i < FListBuffer.Count) do
    end;                                //if FListBuffer.Count > 0 then
  finally
    Strings.EndUpdate;
  end;                                  //try..finally
end;                                    //procedure TMemoIni.ReadSections
//-----------------------------------------------------------------------------
function TMemoIni.ReadString(const Section, Name: string; aDefault: string): string;
var
  i: integer;
  v: string;

begin
  result := aDefault;
  if FListBuffer.Count > 0 then begin
    i := GetSectionIndex(Section);
    if i <> -1 then begin
      inc(i);
      while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do begin
        if GetName(FListBuffer[i]) = Name then begin
          v := GetValue(FListBuffer[i], Name);
          if v <> EmptyStr then Result := v;
          Exit;
        end;                            //if GetName(FListBuffer[i]) = Name then
        inc(i);
      end;                              //while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do
    end;                                //if i <> -1 then
  end;                                  //if FListBuffer.Count > 0 then
end;                                    //function TMemoIni.ReadString
//-----------------------------------------------------------------------------
function TMemoIni.ReadInteger(const Section, Name: string; aDefault: Longint): Longint;
var
  IntStr: string;

begin
  IntStr := ReadString(Section, Name, '');
  if (Length(IntStr) > 2) and (IntStr[1] = '0') and ((IntStr[2] = 'X') or (IntStr[2] = 'x')) then
    IntStr := '$' + System.Copy(IntStr, 3, Maxint);
  Result := StrToIntDef(IntStr, aDefault);
end;                                    //function TMemoIni.ReadInteger
//-----------------------------------------------------------------------------
function TMemoIni.ReadDate(const Section, Name: string; aDefault: TDateTime): TDateTime;
var
  xDateStr: string;

begin
  xDateStr := ReadString(Section, Name, '');
  if xDateStr <> '' then begin
    try
      result := EncodeDate(strtoint(copy(xDateStr, 1, 4)),
        strtoint(copy(xDateStr, 5, 2)),
        strtoint(copy(xdateStr, 7, 2)));
    except
      result := aDefault;
    end;                                //try..except
  end                                   //if xDateStr <> '' then
  else result := aDefault;
end;                                    //function TMemoIni.ReadDate
//-----------------------------------------------------------------------------
function TMemoIni.ReadFloat(const Section, Name: string; aDefault: double): double;
var
  FloatStr: string;

begin
  FloatStr := ReadString(Section, Name, '');
  Result := aDefault;
  if FloatStr <> '' then try
    Result := StrToFloat(FloatStr);
  except
    on EConvertError do
  else raise;
  end;                                  //try..except
end;                                    //function TMemoIni.ReadFloat
//-----------------------------------------------------------------------------
function TMemoIni.ReadBool(const Section, Name: string; aDefault: Boolean): Boolean;
begin
  Result := ReadInteger(Section, Name, Ord(aDefault)) <> 0;
end;                                    //function TMemoIni.ReadBool
//-----------------------------------------------------------------------------
procedure TMemoIni.ReadSectionValues(const Section: string; Strings: TStrings);
var
  N, V: string;
  i: integer;

begin
  if not Assigned(Strings) then raise Exception.Create(cStringsUnassigned);
  Strings.BeginUpdate;
  try
    Strings.Clear;
    if FListBuffer.Count > 0 then begin
      i := GetSectionIndex(Section);
      if i <> -1 then begin
        Inc(i);
        while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do begin
          N := GetName(FListBuffer[i]);
          if N <> EmptyStr then begin
            V := GetValue(FListBuffer[i], N);
            Strings.Add(N + cSeparator + V);
          end;                          //if N <> EmptyStr then
          Inc(i);
        end;                            //while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do
      end;                              //if i <> -1 then
    end;                                //if FListBuffer.Count > 0 then
  finally
    Strings.EndUpdate;
  end;                                  //try.-.finally
end;                                    //procedure TMemoIni.ReadSectionValues
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteString(const Section, Name: string; Value: string);
var
  i: integer;

begin
  i := GetSectionIndex(Section);
  if i <> -1 then begin
    Inc(i);
    while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) and (GetName(FListBuffer[i]) <> Name)
      do Inc(i);

    if (i >= FListBuffer.Count) or IsSection(FListBuffer[i]) then begin
      if Name <> EmptyStr then FListBuffer.Insert(i, Name + cSeparator + Value);
    end                                 //if (i >= FListBuffer.Count) or IsSection(FListBuffer[i]) then
    else if Name <> EmptyStr then FListBuffer[i] := Name + cSeparator + Value;
  end                                   //if i <> -1 then
  else begin
    FListBuffer.Add(EmptyStr);
    FListBuffer.Add(cBrackets[0] + Section + cBrackets[1]);
    if Name <> EmptyStr then FListBuffer.Add(Name + cSeparator + Value);
  end;                                  //else if i <> -1 then
end;                                    //procedure TMemoIni.WriteString
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteInteger(const Section, Name: string; Value: Longint);
begin
  WriteString(Section, Name, IntToStr(Value));
end;                                    //procedure TMemoIni.WriteInteger
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteDate(const Section, Name: string; Value: TDateTime);
var
  xYear,
    xMonth,
    xDay: word;
  DateStr: string;

begin
  DecodeDate(value, xYear, xMonth, xDay);
  DateStr := lz(xYear, 4) + lz(xMonth, 2) + lz(xDay, 2);
  WriteString(Section, Name, DateStr);
end;                                    //procedure TMemoIni.WriteDate
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteFloat(const Section, Name: string; Value: Double);
begin
  WriteString(Section, Name, FloatToStr(Value));
end;                                    //procedure TMemoIni.WriteFloat
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteBool(const Section, Name: string; Value: Boolean);
const
  values: array[boolean] of string = ('0', '1');

begin
  WriteString(Section, Name, Values[Value]);
end;                                    //procedure TMemoIni.WriteBool
//-----------------------------------------------------------------------------
procedure TMemoIni.DeleteKey(const Section, Name: string);
var
  i: integer;

begin
  i := GetSectionIndex(Section);
  if i <> -1 then begin
    Inc(i);
    while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) and (GetName(FListBuffer[i]) <> Name)
      do Inc(i);
    if not (i >= FListBuffer.Count) and not IsSection(FListBuffer[i]) then
      FListBuffer.Delete(i);
  end;                                  //if i <> -1 then
end;                                    //procedure TMemoIni.DeleteKey
//-----------------------------------------------------------------------------
procedure TMemoIni.EraseSection(const Section: string);
var
  i: integer;

begin
  i := GetSectionIndex(Section);
  if i <> -1 then begin
    FListBuffer.Delete(i);
    while (i < FListBuffer.Count) and not IsSection(FListBuffer[i]) do
      FListBuffer.Delete(i);
    if i > 0 then FListBuffer.Insert(i, EmptyStr);
  end;                                  //if i <> -1 then
end;                                    //procedure TMemoIni.EraseSection
//-----------------------------------------------------------------------------
procedure TMemoIni.ReadSettings;
var
  xList: TStringList;

begin
  xList := TStringList.Create;
  with TmmADOQuery.Create(nil) do
  try
    ConnectionString := GetDefaultConnectionString;
    SQL.Text := 'SELECT c_text FROM t_settings';
    SQL.Add(format('WHERE (c_username = ''%s'' OR c_admin = 1)', [FUsername]));
    SQL.Add(format('AND c_applicationname = ''%s''', [GetApplicationname]));
    SQL.Add(format('AND c_formname = ''%s''', [FFormname]));
    SQL.Add(format('AND c_profilename = ''%s''', [FProfilename]));
    SQL.Add('ORDER BY c_admin ASC'); // user default should come first -> c_admin = 0
    CodeSite.SendStringList('ReadSettings', SQL);
    Open;
    if not EOF then
      xList.Assign(TMemoField(FieldByName(cMemoField)));
  finally
    FListBuffer.Assign(xList);
    xList.Free;
    Free;
  end;                                  //try..finally
end;                                    //procedure TMemoIni.ReadSettings
//-----------------------------------------------------------------------------
procedure TMemoIni.WriteSettings;
var
  i,
    xAnzahl: integer;
begin
  if trim(FProfilename) = '' then FProfilename := 'DEFAULT';

  i := 0;
  while i < FListBuffer.Count do begin
    if trim(FListBuffer[i]) = '' then FListBuffer.Delete(i);
    inc(i);
  end;                                  //while i < FListBuffer.Count do

  with TmmADOQuery.Create(nil) do begin
    try
      ConnectionString := GetDefaultConnectionString;
      SQL.Clear;
      SQL.Add('SELECT COUNT(*) AS anzahl FROM t_settings');
      SQL.Add(format('WHERE c_username = ''%s''', [FUsername]));
      SQL.Add(format('AND c_applicationname = ''%s''', [GetApplicationname]));
      SQL.Add(format('AND c_formname = ''%s''', [FFormname]));
      SQL.Add(format('AND c_profilename = ''%s''', [FProfilename]));
//      Parameters.ParamByName('user').value := FUsername;
//      Parameters.ParamByName('appl').value := GetApplicationname;
//      Parameters.ParamByName('form').value := FFormname;
//      Parameters.ParamByName('profile').value := FProfilename;
      CodeSite.SendStringList('WriteSettings 1', SQL);
      Open;
      xAnzahl := FieldByName('anzahl').asinteger;

      Active := false;
      SQL.Clear;
      if xAnzahl = 0 then begin
        SQL.Add('INSERT INTO t_settings (c_username, c_applicationname, c_formname, c_profilename, c_default, c_admin)');
        SQL.Add('VALUES(:user,:appl,:form,:profile,:default,:admin)');
        Parameters.ParamByName('user').value := FUsername;
        Parameters.ParamByName('appl').value := GetApplicationname;
        Parameters.ParamByName('form').value := FFormname;
        Parameters.ParamByName('profile').value := FProfilename;
        Parameters.ParamByName('default').value := ord(false);
        // Admin Flag wird als default nicht mehr gesetzt, da sonst beim Laden bei 2 Admin feldern
        // die Sortierung der Namen zu verwirrung f�hrt
        Parameters.ParamByName('admin').value := 0; //ord(IsAdmin(FUsername));
        CodeSite.SendStringList('WriteSettings 2', SQL);
        ExecSQL;
        SQL.Clear;
      end;                              //if xAnzahl = 0 then

      SQL.Add(format('UPDATE %s', [cParamTable]));
      SQL.Add(format('SET %s = :text', [cMemoField]));
      SQL.Add(format('WHERE %s = :user', [cUsername]));
      SQL.Add(format('AND %s = :appl', [cApplicationname]));
      SQL.Add(format('AND %s = :form', [cFormname]));
      SQL.Add(format('AND %s = :profile', [cProfilename]));
      Parameters.ParamByName('user').value := FUsername;
      Parameters.ParamByName('appl').value := GetApplicationname;
      Parameters.ParamByName('form').value := FFormname;
      Parameters.ParamByName('profile').value := FProfilename;
      Parameters.ParamByName('text').assign(FListBuffer);
      CodeSite.SendStringList('WriteSettings 3', SQL);
      ExecSQL;
    finally
      Free;
    end;                                //try..finally
  end;                                  //with TmmQuery.Create(nil) do
end;                                    //procedure WriteParameterToList
//-----------------------------------------------------------------------------
function TMemoIni.GetDefaultProfile: string;
begin
  with TmmADOQuery.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    SQL.Text := format('SELECT %s FROM %s', [cProfilename, cParamTable]);
    SQL.Add(format('WHERE ((%s = ''%s'' AND c_default = 1) OR c_admin = 1)', [cUsername, FUsername]));
    SQL.Add(format('AND %s = ''%s''', [cApplicationname, GetApplicationname]));
    SQL.Add(format('AND %s = ''%s''', [cFormname, FFormname]));
    // user default should come first -> c_admin = 0
    SQL.Add('ORDER BY c_admin ASC');
    CodeSite.SendStringList('GetDefaultProfile', SQL);
    Open;
    result := fieldbyname(cProfilename).asstring;
  finally
    Free;
  end;                                  //with TmmQuery.Create(nil) do
end;                                    //function TMemoIni.GetDefaultProfile
//-----------------------------------------------------------------------------
function TMemoIni.GetFormProfiles: TStringList;
begin
  result := TStringList.Create;

  with TmmADOQuery.Create(nil) do try
    ConnectionString := GetDefaultConnectionString;
    SQL.Text := format('SELECT %s, %s, CONVERT(int,%s) AS int_def, CONVERT(int,%s) AS int_admin FROM %s', [cProfilename, cUsername, cDefault, cAdmin, cParamTable]);
    SQL.Add(format('WHERE %s = ''%s''', [cApplicationname, GetApplicationname]));
    SQL.Add(format('AND %s = ''%s''', [cFormname, FFormname]));
    CodeSite.SendStringList('GetFormProfiles', SQL);
    Open;

    while not Eof do begin
      Result.Add(fieldbyname(cProfilename).asstring + cListDelimiterChar
        + fieldbyname(cUsername).asstring + cListDelimiterChar
        + fieldbyname('int_def').asstring + cListDelimiterChar
        + fieldbyname('int_admin').asstring + cListDelimiterChar);
      Next;
    end;                                //while not Eof do
  finally
    Free;
  end;                                  //with TmmQuery.Create(nil) do
{
  with TmmQuery.Create(nil) do begin
    DatabaseName := FDatabase.DataBaseName;
    SQL.Clear;
    SQL.Add(format('SELECT %s, %s, CONVERT(int,%s) AS int_def, CONVERT(int,%s) AS int_admin FROM %s', [cProfilename, cUsername, cDefault, cAdmin, cParamTable]));
    SQL.Add(format('WHERE %s = :appl', [cApplicationname]));
    SQL.Add(format('AND %s = :form', [cFormname]));

    ParamByName('appl').asstring := GetApplicationname;
    ParamByName('form').asstring := FFormname;
    Open;

    while not Eof do begin
      Result.Add(fieldbyname(cProfilename).asstring + cListDelimiterChar
        + fieldbyname(cUsername).asstring + cListDelimiterChar
        + fieldbyname('int_def').asstring + cListDelimiterChar
        + fieldbyname('int_admin').asstring + cListDelimiterChar);
      Next;
    end;                                //while not Eof do
    Free;
  end;                                  //with TmmQuery.Create(nil) do
{}
end;                                    //function TMemoIni.GetFormProfiles
//-----------------------------------------------------------------------------
procedure TMemoIni.Assign(Source: TPersistent);
begin
  if Source is TMemoIni then begin
    FApplicationname := TMemoIni(Source).FApplicationname;
    FFormname        := TMemoIni(Source).FFormname;
    FProfilename     := TMemoIni(Source).FProfilename;
    FUsername        := TMemoIni(Source).FUsername;
    Exit;
  end;                                  //if Source is TMemoIni then
  inherited Assign(Source);             // <= otherwise error
end;                                    //procedure TMemoIni.Assign
//-----------------------------------------------------------------------------
function TMemoIni.SelectProfile: tSelectProfileRecordT;
var
  xProfile: TmmSelectProfile;
begin
  if Assigned(mOwner) and (mOwner is TWinControl) then
    xProfile := TmmSelectProfile.Create(mOwner, Self)
  else
    xProfile := TmmSelectProfile.Create(Application, Self);

 // with TmmSelectProfile.Create(Application,Self) do
  with xProfile do try
    if Execute then
      result := Selection
    else
      fillchar(result, sizeof(tSelectProfileRecordT), 0);
  finally
    Free;
  end;                                  //with TmmSelectProfile.Create(Application,Self) do
end;                                    //function TMemoIni.SelectProfile
//-----------------------------------------------------------------------------
function TMemoIni.SaveProfile: tSelectProfileRecordT;
var
  xProfile: TmmSaveProfile;
begin
  if Assigned(mOwner) and (mOwner is TWinControl) then
    xProfile := TmmSaveProfile.Create(mOwner, Self)
  else
    xProfile := TmmSaveProfile.Create(Application, Self);

  with xProfile do try
    if Execute then result := Selection
               else fillchar(result, sizeof(tSelectProfileRecordT), 0);
  finally
    Free;
  end;                                  //with TmmSaveProfile.Create(Application,Self) do
end;                                    //function TMemoIni.SaveProfile
//-----------------------------------------------------------------------------
procedure TMemoIni.DeleteProfileDlg;
var
  xProfile: TmmDeleteProfile;
begin
  if Assigned(mOwner) and (mOwner is TWinControl) then
    xProfile := TmmDeleteProfile.Create(mOwner, Self)
  else
    xProfile := TmmDeleteProfile.Create(Application, Self);

// with TmmDeleteProfile.Create(Application,Self) do
  with xProfile do
  try
    Execute;
  finally
    Free;
  end;                                  //with TmmDeleteProfile.Create(Application,Self) do
end;                                    //procedure TMemoIni.DeleteProfile
//-----------------------------------------------------------------------------
procedure TMemoINI.DeleteProfile;
begin
  with TmmADOQuery.Create(nil) do begin
    ConnectionString := GetDefaultConnectionString;
    try
      SQL.Clear;
      SQL.Add(format('DELETE FROM %s', [cParamTable]));
      SQL.Add(format('WHERE %s = :user', [cUsername]));
      SQL.Add(format('AND %s = :appl', [cApplicationname]));
      SQL.Add(format('AND %s = :form', [cFormname]));
      SQL.Add(format('AND %s = :profile', [cProfilename]));
      Parameters.ParamByName('user').value := FUsername;
      Parameters.ParamByName('appl').value := GetApplicationname;
      Parameters.ParamByName('form').value := FFormname;
      Parameters.ParamByName('profile').value := FProfilename;
      CodeSite.SendStringList('DeleteProfiles', SQL);
      ExecSQL;
    finally
      Free;
    end;                                //try..finally
  end;                                  //with TmmQuery.Create(nil) do
end;                                    //procedure TMemoINI.DeleteProfile
//-----------------------------------------------------------------------------
procedure TMemoINI.SetDefaultProfile;
begin
  with TmmADOQuery.Create(nil) do begin
    ConnectionString := GetDefaultConnectionString;
    try
   //set all profiles to 0 = false
      SQL.Clear;
      SQL.Add(format('UPDATE t_settings', [cParamTable]));
      SQL.Add(format('SET %s = 0', [cDefault]));
      SQL.Add(format('WHERE %s = :user', [cUsername]));
      SQL.Add(format('AND %s = :appl', [cApplicationname]));
      SQL.Add(format('AND %s = :form', [cFormname]));
      Parameters.ParamByName('user').value := FUsername;
      Parameters.ParamByName('appl').value := GetApplicationname;
      Parameters.ParamByName('form').value := FFormname;
      ExecSQL;

   //set selected profiles to 1 = true
      SQL.Clear;
      SQL.Add(format('UPDATE %s', [cParamTable]));
      SQL.Add(format('SET %s = 1', [cDefault]));
      SQL.Add(format('WHERE %s = :user', [cUsername]));
      SQL.Add(format('AND %s = :appl', [cApplicationname]));
      SQL.Add(format('AND %s = :form', [cFormname]));
      SQL.Add(format('AND %s = :profile', [cProfilename]));
      Parameters.ParamByName('user').value := FUsername;
      Parameters.ParamByName('appl').value := GetApplicationname;
      Parameters.ParamByName('form').value := FFormname;
      Parameters.ParamByName('profile').value := FProfilename;
      ExecSQL;
    finally
      Free;
    end;                                //try..finally
  end;                                  //with TmmQuery.Create(nil) do
end;                                    //procedure TMemoINI.SetDefaultProfile

//TBaseMemoINIObject
constructor TBaseMemoINIObject.Create;
begin
  FMemoINI := TMemoINI.Create;
end;                                    //constructor TBaseMemoINIObject.Create
//-----------------------------------------------------------------------------
constructor TBaseMemoINIObject.Create(aOwner: TComponent);
begin
  FMemoINI := TMemoINI.Create(aOwner);
end;                                    //constructor TBaseMemoINIObject.Create
//-----------------------------------------------------------------------------
destructor TBaseMemoINIObject.Destroy;
begin
  FMemoINI.Free;
end;                                    //destructor TBaseMemoINIObject.Destroy
//-----------------------------------------------------------------------------
function TBaseMemoINIObject.GetMemoINI: TMemoINI;
begin
  result := FMemoINI;
end;                                    //function TBaseMemoINIObject.GetMemoINI
//-----------------------------------------------------------------------------
procedure TBaseMemoINIObject.SetMemoINI(value: TMemoINI);
begin
  FMemoINI := value;
end;                                    //procedure TBaseMemoINIObject.SetMemoINI

end.                                    //unit MemoINI.pas

