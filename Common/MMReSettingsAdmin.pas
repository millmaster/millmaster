unit MMReSettingsAdmin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MMUGlobal,
  YMParaDBAccess,
  YMParaDef,
  YMParaUtils,
  YMBasicProdParaBox,
  QualityMatrix,
  QualityMatrixDef,
  YMParaEditBox;

type

  TOnChange = procedure(aOwner: TComponent) of object;


  TReSettingsAdmin = class(TComponent)
  private
    { Private declarations }
    fOnChange: TOnChange;

    mYMSettings: TYMSettings;

    mQualityMatrix,
    mFFMatrix            : TQualityMatrix;
    mChannelEditBox      : TChannelEditBox;
    mSpliceEditBox       : TSpliceEditBox;
    mYarnCountEditBox    : TYarnCountEditBox;
    mFaultClusterEditBox : TFaultClusterEditBox;

    procedure DoConfirm(aTag: Integer);
    procedure DoParameterChange(aSource: TParameterSource);

  protected
    { Protected declarations }
    procedure Loaded; override;

  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }

  end;

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('MillMaster', [TReSettingsAdmin]);
end;

{ TReSettingsAdmin }

constructor TReSettingsAdmin.Create(aOwner: TComponent);
begin
  inherited;
  mQualityMatrix       := NIL;
  mChannelEditBox      := NIL;
  mSpliceEditBox       := NIL;
  mYarnCountEditBox    := NIL;
  mFaultClusterEditBox := NIL;
  mFFMatrix            := NIL;
  mYMSettings := TYMSettings.Create;
end;

destructor TReSettingsAdmin.Destroy;
begin
  mQualityMatrix       := NIL;
  mChannelEditBox      := NIL;
  mSpliceEditBox       := NIL;
  mYarnCountEditBox    := NIL;
  mFaultClusterEditBox := NIL;
  mFFMatrix            := NIL;
  mYMSettings.Free;
  inherited;
end;

procedure TReSettingsAdmin.DoConfirm(aTag: Integer);
var
  xSettings: TYMSettingsRec;
begin
  if mYMSettings.Changed then
  begin
    mYMSettings.GetFromScreen(xSettings);
    mYMSettings.PutToScreen(xSettings);
  end;
end;

procedure TReSettingsAdmin.DoParameterChange(aSource: TParameterSource);
var
  xPara: TChannelSettingsRec;
  xCluster: TFaultClusterSettingsRec;
  xSettings: TYMSettingsRec;
begin

  case aSource of

    psChannel:
      begin
        if mQualityMatrix = NIL then exit;
        xPara := mQualityMatrix.GetChannelPara;
        mChannelEditBox.PutPara(xPara);
        mYMSettings.GetFromScreen(xSettings);
        mQualityMatrix.PutYMSettings(xSettings);
      end;

    psCluster:
      begin
        if mFaultClusterEditBox = NIL then exit;
        mFaultClusterEditBox.GetPara(xCluster);
        xCluster.clusterDia := mQualityMatrix.GetClusterDiameter;
        mFaultClusterEditBox.PutPara(xCluster);
        mYMSettings.GetFromScreen(xSettings);
        mQualityMatrix.PutYMSettings(xSettings);
      end;

    psSplice:
      begin
        if mSpliceEditBox = NIL then exit;
        xPara := mQualityMatrix.GetSplicePara;
        mSpliceEditBox.PutPara(xPara);
        mYMSettings.GetFromScreen(xSettings);
        mQualityMatrix.PutYMSettings(xSettings);
      end;
  end;
  mQualityMatrix.Invalidate;
end;


procedure TReSettingsAdmin.Loaded;
var x     : integer;
    xComp : TComponent;
    xQM   : TQualityMatrix;
begin
  inherited;
  with mYMSettings do begin
    for x:=0 to Owner.ComponentCount -1 do begin
      xComp := Owner.Components[x];

      if (xComp is TQualityMatrix)  then begin
          xQM := xComp as TQualityMatrix;
          if (mQualityMatrix = NIL) and (xQM.MatrixType = mtShortLongThin ) then begin
             mQualityMatrix :=  xComp as TQualityMatrix;
             mQualityMatrix.OnParameterChange := DoParameterChange;
             ClassMatrixEditBox := mQualityMatrix;

             //mQualityMatrix.ClusterVisible:= TRUE;
          end;

          if (mFFMatrix = NIL) and (xQM.MatrixType = mtSiro ) then begin
              mFFMatrix :=  xComp as TQualityMatrix;
              FFMatrixEditBox := mFFMatrix;
              mFFMatrix.OnParameterChange := DoParameterChange;
          end;
      end;


      if (xComp is TChannelEditBox) and (mChannelEditBox = NIL) then begin
         mChannelEditBox :=  xComp as TChannelEditBox;
         ChannelEditBox := mChannelEditBox;
         mChannelEditBox.OnConfirm := DoConfirm;
      end;

      if (xComp is TSpliceEditBox) and (mSpliceEditBox = NIL) then begin
         mSpliceEditBox :=  xComp as TSpliceEditBox;
         SpliceEditBox := mSpliceEditBox;
         mSpliceEditBox.OnConfirm := DoConfirm;
      end;

      if (xComp is TYarnCountEditBox) and (mYarnCountEditBox = NIL) then begin
         mYarnCountEditBox :=  xComp as TYarnCountEditBox;
         YarnCountEditBox := mYarnCountEditBox;
         mYarnCountEditBox.OnConfirm := DoConfirm;
      end;

      if (xComp is TFaultClusterEditBox) and (mFaultClusterEditBox = NIL) then begin
         mFaultClusterEditBox :=  xComp as TFaultClusterEditBox;
         FaultClusterEditBox := mFaultClusterEditBox;
         mFaultClusterEditBox.OnConfirm := DoConfirm;
      end;
    end; // for

    PutDefaultToScreen;
  end; // with
end;






end.
