inherited AssignAnimation: TAssignAnimation
  Left = 457
  Top = 180
  BorderIcons = []
  Caption = ''
  ClientHeight = 350
  ClientWidth = 493
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 248
    Width = 493
    Height = 102
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 0
    object lMsg: TmmLabel
      Left = 8
      Top = 16
      Width = 481
      Height = 21
      Alignment = taCenter
      AutoSize = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bOK: TmmBitBtn
      Left = 208
      Top = 56
      Width = 75
      Height = 25
      Caption = 'OK'
      Enabled = False
      TabOrder = 0
      Visible = True
      OnClick = bOKClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 0
    Width = 493
    Height = 248
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvNone
    TabOrder = 1
    object mmAnimate1: TmmAnimate
      Left = 1
      Top = 1
      Width = 491
      Height = 246
      Align = alClient
      Active = False
      StopFrame = 34
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 448
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
  object mmTimer1: TmmTimer
    Enabled = False
    OnTimer = mmTimer1Timer
    Left = 416
  end
end
