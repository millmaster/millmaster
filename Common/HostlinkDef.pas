{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMDef.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: DLL for LinkManager
| Process(es)...: -
| Description...: Contains Types and Constants
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  0.00  Wss | File created
| 03.04.2000  1.02  Wss | - Changed to support as Hostlink directly
                          - extra function for Trend selection implemented
                          - Database connection is kept alive: no create/free anymore.
| 09.10.2000  1.02  Wss | WindowLength added to data item list to calculate in Floor
| 09.05.2001  1.02  Wss | most fdtLong data items changed to fdtSingle because of overflow error in DB
| 31.05.2001  1.02  Wss | cMaxGroupData = 20, Size of GroupData set to 130 -> hint bug is fixed in Floor
| 24.09.2001  1.02  Wss | DataItem Template added
| 12.12.2001  2.03  Wss | DataItem for day, week, month, year, SpdCount, Assortment and StartMode added
| 13.12.2001  2.03  Wss | DataItem YarnCount as text added
| 05.02.2002  2.03  Wss | DataItem SFI added
| 13.02.2002  2.03  Wss | - DataSize von LotData auf 200 erhoeht, da keine Probleme mehr mit Hint in Floor
                          - Konstante cReReadGroupDataTimeOut eingefuegt
| 16.07.2002  2.03  Wss | Dataitem LckCl hinzugefuegt
| 24.07.2002  2.03  Wss | Dataitem TemplateName auf 50 vergr�ssert
| 16.09.2002 2.50 Wss | - Auf ADO umgebaut
                        - TDataTyp umbenannt auf TFloorDataTyp da Namenskonflikt mit MMSetup Datentyp
| 22.11.2002 2.03 Wss | - DataItem diOffOutOfProd hinzugef�gt
| 02.12.2002 2.03 Wss | - DataItem diShiftInDay hinzugef�gt
| 12.01.2004 2.03 Wss | - CFF und xxxFF DataItems an neuen Namen CF und xxxF angepasst
                        - Unterst�tzung f�r QOfflimit Items entfernt
                        - Benutzerfelder von Artikel hinzugef�gt: 3 Num, 7 Text
| 02.06.2005 2.03 Wss | SpectraPlus und Zenit Datenelemente hinzugef�gt
| 09.06.2005 2.03 Wss | Garn/Kurznummerbezeichung: gem�ss Jo nun definitiv CYCnt... und CSCnt...
| 29.09.2005 1.02 Wss | Trend Items erweitert
| 21.09.2006 3.00 Nue | Uebernahme Nue: Neue SubDataItems f�r LotData added; expansion to 30 Groups (cWSCSpdGroupLimit).
| 12.02.2008 3.01 Nue | Anpassen f�r V5.04: VCV-Zus�tze eingef�gt.
| 19.06.2008      Nue |  VCV-Zus�tze an den Schluss der Typen verschoben.
|=========================================================================================*}
unit HostlinkDef;

interface
uses
  Consts, Windows, mmList, mmEventLog, YMParaDef;

//------------------------------------------------------------------------------
const
  cLinkSynchronous = 1;
  cHostlinkVersion = 3;
  cMaxGroupData    = 20;
//  cMaxLotCount     = cZESpdGroupLimit;
  cMaxLotCount     = cWSCSpdGroupLimit;
  cReReadGroupDataTimeOut = 5000;

  // data item mask
  cDIDKeyFlag      = $8000;
  cAllValues       = $FFFF;  // see DataModul at set of machine filter
  cInvalidMachID   = $FFFF;

  // data type mask
  cFormatTextItems    = $01;
  cFormatNumericItems = $02;
  cFormatTimeItems    = $04;
  cFormatKeyItems     = $08;


//------------------------------------------------------------------------------
type
  // self defined date and time data types of Floor
  TIDateTime = Array[0..5] of Byte;
  TIDate = Array[0..2] of Byte;
  TITime = Array[0..2] of Byte;

  // descriptor of expecting data in return baskets
  TFloorDataTyp = (fdtPad, fdtChar, fdtByte, fdtShort, fdtWord, fdtLong, fdtDWord, fdtSingle,
                   fdtDouble, fdtString, fdtDate, fdtTime, fdtDateTime, fdtOLEDateTime);


{$MINENUMSIZE 4}
  TItemText = (itLongText, itShortText);

{$MINENUMSIZE 2}
  TDidItems = (diUnknown,  // pad: is for diDIDMC begins with 1
    // fixed DIDs
//ACHTUNG: Neue ITEMS immer am Schluss anh�ngen, sonst sind sie teilweise in der Floor nicht sichtbar???? Nue:19.06.08
    diMachine_Name,
    diDIDStopGroup,
    diDIDMCStatus,
    diDIDStatusTexts,
    diDIDSpdTrend,
    diShiftStart,      // necessary for repitmob.dll
    diShiftEnd,        // necessary for repitmob.dll
    diTime,
    // t_prodgroup
    diProdGrpCount,
    diLotData,
    // production data from t_dw_prodgroup_shift and t_spindle_interval_data
    diProd_Name, diSpindle_Range, diProd_Start, diProd_End,
    diStyle_Name,
    diLen, diWei, diBob, diCones, diRed, ditRed, ditYell, diManSt, ditManSt,
    ditRt, ditOt, ditWt, diSp, diRSp, diYB,
    // long stop
    diLSt, diLStProd,
    // cuts
    diCS, diCL, diCT, diCN, diCSp, diCClS, diUClS, diUClL, diUClT, diCOffCnt,
    diLckOffCnt, diCBu, diCDBu, dicSys, diLckSys, dicUpY, diCYTot,
    // SIRO
    diCSIRO, diLckSIRO, diCSIROCl, diLckSIROCl,
    // imperfections
    diINeps, diIThick, diIThin, diISmall, diI2_4, diI4_8, diI8_20, diI20_70,
    // SFI:
    diSFI_D, diCSFI, diLckSFI,
    // Offlimits
    diOffSpindle_id,
    diOffOnlineTime,
    diOffYB,
    diOffEff,
    diOffCSp,
    diOffRSp,
    diOffBu,
    diOffCUpY,
    diOfftLSt,
    diOffLSt,
    diOffCSiro,
    diOffCYTot,

    diDisplayUnit,
    // added 14.1.2004
    // Benutzerdefinierte Felder vom Artikel
    diNum1,
    diNum2,
    diNum3,
    diString1,
    diString2,
    diString3,
    diString4,
    diString5,
    diString6,
    diString7,
    // added 12.12.2001
    diDay,
    diWeek,
    diMonth,
    diYear,
    diStart_Mode,
    diSpindleCount,
    diAssortment_Name,
    diAct_yarncnt,
    // added 05.02.2002
    diSFI,
    // added 16.07.2002
    diLckClS,
    // added 22.11.2002
    diOffOutOfProd,
    // added 02.12.2002
    diShiftInDay,
    diYM_set_name, // diTemplateName
    // addes 02.06.2005
    diCP,
    diCClL,
    diCClT,
    diCOffCntPlus,
    diCOffCntMinus,
    diCShortOffCnt,
    diCShortOffCntPlus,
    diCShortOffCntMinus,
//8.6.05 kast    diCDrumWrap
    diLckP,
    diLckClL,
    diLckClT,
    diLckShortOffCnt,
    diLckNSLT,
    // VCV:   Nue:11.02.08
    diCVd, diCVCV, diLckVCV

{$IFDEF NewItems}
{$ENDIF}
  );

  TSubDIDList = (
    slLot,
    slShiftSelection,
    slTrendSelection,
    slOfflimitSelection,
    slLotData,
    // ^^ insert here above ^^
    slMachineState,
    slNone);
{$MINENUMSIZE 1}

  // data type
  TDataSelection = (dsNone,
    dsCurShift, dsLastShift,
    dsLast3Shift, dsLastDay,
    dsLast7Day, dsLastWeek,
    dsCurMonth, dsLastMonth,
    dsCustShiftSel, dsIntervalData
  );

  TTrendItems = (
    tiSFI_D, tiSFI,
    tiEffM, tiEffP,
    tiINeps, tiISmall, tiIThick, tiIThin,
    tiI2_4, tiI4_8, tiI8_20, tiI20_70,
    tiCYTot, tiCN, tiCS, tiCL, tiCT, tiCSp, tiCClS, tiCFF, // 29.9.205: tiCCl -> tiCClS
    tiYB, tiWeight, tiCones,
    tiSP, tiRSp, tiEffRSp, tiEffCSp,
    // neu 29.9.2005
    tiCClL, tiCClT, tiCP, tiCSFI, tiCUpY,
    tiCYCnt, tiCYCntPlus, tiCYCntMinus, tiCSCnt, tiCSCntPlus, tiCSCntMinus,
    //Nue:11.02.08
    tiCVCV, tiCVd
  );

//  TTrendItems = (
//    tiSFI_D, tiSFI,
//    tiEffM, tiEffP,
//    tiINeps, tiISmall, tiIThick, tiIThin,
//    tiI2_4, tiI4_8, tiI8_20, tiI20_70,
//    tiCYTot, tiCN, tiCS, tiCL, tiCT, tiCSp, tiCCl, tiCFF,
//    tiYB, tiWeight, tiCones,
//    tiSP, tiRSp, tiEffRSp, tiEffCSp
//  );
//

type
  TLotData = (ldLot, ldSpdRange, ldEff, ldStyle, ldStartMode,
     ldCYTot, ldCF, ldSP, ldCSP, ldProdKg, ldProdStart, ldCL, ldCN, ldCS, ldCT,
     ldINEPSkm, ldISMALm, ldITHICKkm, ldITHINkm
  );

//------------------------------------------------------------------------------
// records used for reading and building basket structure
//------------------------------------------------------------------------------
type
  // data item definition of baskets
  PDid = ^TDid;
  TDid = record
    Did:    TDidItems;
    SubDid: Word;
  end;

  // structur of requesting data
  PDidReqDescr = ^TDidReqDescr;
  TDidReqDescr = record
    Machine:   Word;
    DidCount:  Word;
    DLPos:     Word;
    SectionNo: Word;
  end;

  // structure of returning data
  PDidRetDescr = ^TDidRetDescr;
  TDidRetDescr = record
    Data:    TDid;
    DataPos: DWord;
  end;

//------------------------------------------------------------------------------
// records used for DLL interface GetLinkStatus
//------------------------------------------------------------------------------
type
  TLinkStats = record
    DidTableSize,
    BasketsRequested,
    SubBasketsRequested,
    DataItemsRequested,
    DataItemsReturned,
    MaxBasketSize,
    RequestsWaiting,
    BasketRetries: DWord;
  end;

  PLinkTotStats = ^TLinkTotStats;
  TLinkTotStats = record
    LinkStats: TLinkStats;
  end;

  PLinkStatus = ^TLinkStatus;
  TLinkStatus = record
    LinkTyp: Word;
    HostName: PChar;
    HostNameSz: Word;
    LinkStats: PLinkTotStats;
  end;

//------------------------------------------------------------------------------
// records used for DLL interface IsIndexedDID
//------------------------------------------------------------------------------
  PFloorSelInfo = ^TFloorSelInfo;
  TFloorSelInfo = record
    Description: WideString;
    ExtraSelFn:  WideString;
  end;

  PExtraDidSelection = ^TExtraDidSelection;
  TExtraDidSelection = record
    MaxSelection: Word;
    Title: WideString;
    SelInfo: Array of TFloorSelInfo;
  end;

//------------------------------------------------------------------------------
// records used for definitions of SubDid and DID array
//------------------------------------------------------------------------------
  PSubDidDef = ^TSubDidDef;
  TSubDidDef = record
    SubDidCount: Word;
    Title:       String;
    SelInfo:     Array of String;
  end;

  TDataItemDef = packed record
    LongText: String;
    ShortText: String;
    DataTyp: TFloorDataTyp;
    SubDid: TSubDIDList;
    DataSize: Word;         // has to be set only on strings
    Key: Boolean;
    LabM: Boolean;
    DispoM: Boolean;
  end;

  TTrendDef = record
    ColName: String;
    Factor:  Integer; //Single;
  end;


//------------------------------------------------------------------------------
// TABLE OF META DATA DEFINITION
//------------------------------------------------------------------------------
const
//ACHTUNG: Neue ITEMS immer am Schluss anh�ngen, sonst sind sie teilweise in der Floor nicht sichtbar???? Nue:19.06.08
  cMetaData: Array[TDidItems] of TDataItemDef = (
    // only dummy fill to perform the first fixed DID starting at 1
    (LongText:''; ShortText:''; DataTyp:fdtLong;     SubDid:slNone),
    // fixed DIDs
    (LongText:'Ma';             DataTyp:fdtString;   SubDid:slNone; DataSize: 11),
    (LongText:'Ma Stop table';  DataTyp:fdtByte;     SubDid:slNone),
    (LongText:'MaStatus';       DataTyp:fdtWord;     SubDid:slNone),
    (LongText:'Stop Text';      DataTyp:fdtString;   SubDid:slNone; DataSize: 30),
    (LongText:'Trend Data';     DataTyp:fdtSingle;    SubDid:slTrendSelection),
    // shift start/end time necessary for repitmob.dll: DO NOT CHANGE ShortText DESCRIPTION
    (LongText:'Shift Start'; ShortText:'SH_Start';  DataTyp:fdtOLEDateTime; SubDid:slShiftSelection),
    (LongText:'Shift End';   ShortText:'SH_End';    DataTyp:fdtOLEDateTime; SubDid:slShiftSelection),
    (LongText:'Time';                               DataTyp:fdtOLEDateTime; SubDid:slShiftSelection),
    // nProdGrp is necessary for Floor to get the number of sections with colors
    (LongText:'LotCount';    DataTyp:fdtLong;        SubDid:slLot),
    (LongText:'LotData';     DataTyp:fdtString;      SubDid:slLotData;      DataSize: cMaxLotCount*cMaxGroupData),
//    (LongText:'LotData';     DataTyp:fdtString;      SubDid:slLotData;      DataSize: 200),
    (LongText:'Lot';         DataTyp:fdtString;      SubDid:slShiftSelection; DataSize: 41; Key: True),
    (LongText:'SpdRange';    DataTyp:fdtString;      SubDid:slShiftSelection; DataSize: 10),
    (LongText:'ProdStart';   DataTyp:fdtOLEDateTime; SubDid:slShiftSelection),
    (LongText:'ProdEnd';     DataTyp:fdtOLEDateTime; SubDid:slShiftSelection),
    (LongText:'Style';       DataTyp:fdtString;      SubDid:slShiftSelection; DataSize: 21; Key: True),
    (LongText:'Length';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'Weight';      DataTyp:fdtDouble;      SubDid:slShiftSelection),
    (LongText:'Bob';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'Cones';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'nRed';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tRed';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tYellow';     DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'ManSt';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tManSt';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tRu';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tOp';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tWa';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'Sp';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'RSp';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'YB';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // diLSt, diLStProd, diLStBreak, diLStMat, diLStRev, diLStClean, diLStDef,
    (LongText:'nLSt';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'tLStProd';    DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // cuts
    //diCS, diCL, diCT, diCN, diCSp, diCCl, diUClS, diUClL, diUCiT, diCOffCnt,
    (LongText:'CS';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CL';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CT';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CN';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CSp';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CClS';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'rSCl';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'rLCl';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'rTCl';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CYCnt';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    //diLckOffCnt, diCBu, diCDBu, diCSys, diLckSys, diCUpY, diCYTot,
    (LongText:'LckYCnt';     DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CBu';         DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CDBu';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CSys';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'LckSys';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CUpY';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CYTot';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // diCSIRO, diLckSIRO, diCSIROCl, diLckSIROCl,
    (LongText:'CF';          DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'LckF';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'CFCl';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'LckFCl';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // imperfections
    //diINeps, diIThick, diIThin, diISmall, diI2_4, diI4_8, diI8_20, diI20_70
    (LongText:'INeps';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'IThick';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'IThin';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'ISmall';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'I2-4';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'I4-8';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'I8-20';       DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'I20-70';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // SFI
    // diSFI, diSFI_d, diCSFI, diLckSFI
    (LongText:'SFI/D';       DataTyp:fdtDouble;      SubDid:slShiftSelection),
    (LongText:'CSFI';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'LckSFI';      DataTyp:fdtSingle;      SubDid:slShiftSelection),
    // Offlimits: LongText werden zu c_XXX Felder, ShortText sind die OnlineOfflimit Zeiten
    (LongText:'OffSpd';                               DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OfftOnline';                           DataTyp:fdtSingle; SubDid:slOfflimitSelection),
    (LongText:'OffYB';         ShortText:'tOnlYB';    DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffEff';        ShortText:'tOnlEff';   DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffCSp';        ShortText:'tOnlCSp';   DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffRSp';        ShortText:'tOnlRSp';   DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffCBu';        ShortText:'tOnlBu';    DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffCUpY';       ShortText:'tOnlCUpY';  DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OfftLSt';       ShortText:'tOnltLSt';  DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffLSt';        ShortText:'tOnlLSt';   DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffCF';         ShortText:'tOnlCSiro'; DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    (LongText:'OffCYTot';      ShortText:'tOnlCYTot'; DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),

    (LongText:'DisplayUnit';   DataTyp:fdtSingle;       SubDid:slShiftSelection),
    // Benutzerdefinierte Felder vom Artikel
    (LongText:'StyleNum1';  DataTyp:fdtSingle;   SubDid:slShiftSelection; LabM: True),
    (LongText:'StyleNum2';  DataTyp:fdtSingle;   SubDid:slShiftSelection; LabM: True),
    (LongText:'StyleNum3';  DataTyp:fdtSingle;   SubDid:slShiftSelection; LabM: True),
    (LongText:'StyleText1'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText2'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText3'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText4'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText5'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText6'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    (LongText:'StyleText7'; DataTyp:fdtString;   SubDid:slShiftSelection; DataSize: 255; LabM: True),
    // added 12.12.2001
    (LongText:'Day';           DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    (LongText:'Week';          DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    (LongText:'Month';         DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    (LongText:'Year';          DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    (LongText:'StartMode';     DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    (LongText:'SpdCount';      DataTyp:fdtLong;     SubDid:slShiftSelection),
    (LongText:'Assortment';    DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:50; Key: True),
    (LongText:'YarnCount';     DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:10),
    // added 05.02.2002
    (LongText:'SFI';           DataTyp:fdtDouble;   SubDid:slShiftSelection),
    // added 16.07.2002
    (LongText:'LckClS';         DataTyp:fdtDouble;   SubDid:slShiftSelection),
    // added 22.11.2002
    (LongText:'OffOutOfProd';  ShortText:'tOnlOutOfProd'; DataTyp:fdtString; SubDid:slOfflimitSelection; DataSize: 10),
    // added 02.12.2002
    (LongText:'ShiftInDay';    ShortText:'c_shift_in_day'; DataTyp:fdtString; SubDid:slShiftSelection; DataSize: 5),
    (LongText:'Template';      DataTyp:fdtString;   SubDid:slShiftSelection; DataSize:50; Key: True),
    // added 02.06.2005
    (LongText:'CP';            DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CClL';          DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CClT';          DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CYCnt+';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CYCnt-';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CSCnt';         DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CSCnt+';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'CSCnt-';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
//    (LongText:'CDW';         ShortText:'c_CDrumWrap'; DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'LckP';          DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'LckClL';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'LckClT';        DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'LckSCnt';       DataTyp:fdtSingle;   SubDid:slShiftSelection),
    (LongText:'LckNSLT';       DataTyp:fdtSingle;   SubDid:slShiftSelection),
    // VCV
    // diCVd, diCVCV, diLckVCV
    (LongText:'CVd';         DataTyp:fdtDouble;      SubDid:slShiftSelection),
    (LongText:'CVCV';        DataTyp:fdtSingle;      SubDid:slShiftSelection),
    (LongText:'LckVCV';      DataTyp:fdtSingle;      SubDid:slShiftSelection)

{$IFDEF NewItems}
{$ENDIF}
  );

const
  cDataItemSize: Array[TFloorDataTyp] of Word = (
    0,           // dtPad
    1,1,         // fdtChar, fdtByte
    2,2,         // fdtShort, fdtWord
    4,4,4,       // fdtLong, fdtWord, fdtSingle
    8,           // fdtDouble
    0,           // fdtString will be calcualted from string
    3,3,         // fdtDate, fdtTime
    6,8          // fdtDateTime, fdtOLEDateTime
  );

const
  cDataSelectionText: Array[dsCurShift..dsIntervalData] of String = (
    '(*)Aktuelle Schicht',     // ivlm
    '(*)Letzte Schicht',       // ivlm
    '(*)Letzten 3 Schichten',  // ivlm
    '(*)Letzter Tag',          // ivlm
    '(*)Letzte 7 Tagen',       // ivlm
    '(*)Letzte Woche',         // ivlm
    '(*)Aktueller Monat',      // ivlm
    '(*)Letzter Monat',        // ivlm
    '(*)Kundenschichtbereich', // ivlm
    '(*)Intervalldaten'        // ivlm
  );

//..............................................................................
// TABLE OF SUBDID DEFINITION
//..............................................................................
const
  // **** SUBDID Status Text
  cMachineStateTextCount = 10;
  cMachineStateText: Array[1..cMachineStateTextCount] of String = (
    '(*)Undefiniert',        // ivlm
    'Undef2',
    '(*)Laeuft',              // ivlm
    'Run',
    '(*)Keine Akquirierung', // ivlm
    'NoDataColl2',
    '(*)Offline',            // ivlm
    'Offl2',
    '(*)In Synchronisation', // ivlm
    'InSynchro2'
  );

const
  // **** SUBDID Trend
  cTrendSelection: Array[TTrendItems] of String = (
    // SFI
    'SFI/D', 'SFI',
    // Eff tiEffMachine, tiEffProduction,
    '%MEff', '%PEff',
    // imperfections: tiINeps, tiISmall, tiIThick, tiIThin, tiI2_4, tiI4_8, tiI8_20, tiI20_70,
    'INeps', 'ISmall','IThick','IThin',
    'I2-4', 'I4-8','I8-20','I20-70',
    // cuts: tiCYTot, tiCN, tiCS, tiCL, tiCT, tiCSp, tiCCl, tiCSIRO,
    'CYTot', 'CN', 'CS', 'CL', 'CT', 'CSp', 'CClS', 'CF', // 29.9.205: tiCCl -> tiCClS
    // other: tiYB, tiRSp
    'YB', 'Weight', 'Cones',
    // Splice values
    'Sp', 'RSp', '%RSp', '%CSp',
    // neu 29.9.2005
    'CClL', 'CClT', 'CP', 'CSFI', 'CUpY',
    'CYCnt', 'CYCnt+', 'CYCnt-', 'CSCnt', 'CSCnt+', 'CSCnt-',
    //Nue:11.02.08
    'CVd','CVCV'
  );

  // Factor = 0: Schnittzahlen basierend auf LengthBase, Factor > 0: Schnittzahlen basieren auf Factor [m]
  cTrendColNames: Array[TTrendItems] of TTrendDef = (
    // SFI
    (ColName: 'c_SFI_D';   Factor: 0),
    (ColName: 'c_SFI';     Factor: 0),
    // Eff
    (ColName: 'c_EffMach'; Factor: 0),
    (ColName: 'c_EffProd'; Factor: 0),
    // imperfecitons
    (ColName: 'c_INeps';   Factor: 1000),
    (ColName: 'c_ISmall';  Factor: 1),
    (ColName: 'c_IThick';  Factor: 1000),
    (ColName: 'c_IThin';   Factor: 1000),
    (ColName: 'c_I2_4';    Factor: 1000),
    (ColName: 'c_I4_8';    Factor: 1000),
    (ColName: 'c_I8_20';   Factor: 1000),
    (ColName: 'c_I20_70';  Factor: 1000),
    // cuts
    (ColName: 'c_CYTot';   Factor: 0),
    (ColName: 'c_CN';      Factor: 0),
    (ColName: 'c_CS';      Factor: 0),
    (ColName: 'c_CL';      Factor: 0),
    (ColName: 'c_CT';      Factor: 0),
    (ColName: 'c_CSp';     Factor: 0),
    (ColName: 'c_CClS';    Factor: 0),
    (ColName: 'c_CSIRO';   Factor: 0),
    // others
    (ColName: 'c_YB';      Factor: 0),
    (ColName: 'c_Wei';     Factor: 0),
    (ColName: 'c_Cones';   Factor: 0),
    (ColName: 'c_Sp';      Factor: 0),
    (ColName: 'c_RSp';     Factor: 0),
    (ColName: 'c_EffRSp';  Factor: 0),
    (ColName: 'c_EffCSp';  Factor: 0),
    // neu 29.9.2005
    (ColName: 'c_CClL';              Factor: 0),
    (ColName: 'c_CClT';              Factor: 0),
    (ColName: 'c_CP';                Factor: 0),
    (ColName: 'c_CSFI';              Factor: 0),
    (ColName: 'c_CUpY';              Factor: 0),
    (ColName: 'c_COffCnt';           Factor: 0),
    (ColName: 'c_COffCntPlus';       Factor: 0),
    (ColName: 'c_COffCntMinus';      Factor: 0),
    (ColName: 'c_CShortOffCnt';      Factor: 0),
    (ColName: 'c_CShortOffCntPlus';  Factor: 0),
    (ColName: 'c_CShortOffCntMinus'; Factor: 0),
    //Nue:11.02.08
    (ColName: 'c_CVd';              Factor: 0),
    (ColName: 'c_CVCV';             Factor: 0)

  );

const
  // **** special SubDid table to define the production groups in Floor
  // **** SubDid for data items which are linked with production groups
  // HKEY_LOCAL_MACHINE\SOFTWARE\BARCO\Clients\MILLMASTER\floor default\Settings\Sections
  // a DWord value "Number Of Sections" which defines the count of supported
  // production groups. The DID which has this count of SubDid appears in the
  // <Options><Appearence> Section tab page in Floor.
  cLotNumberCount = cMaxLotCount + 1;  // registry key value = cMaxLotCount
  cLotNumberStr: Array[1..cLotNumberCount] of String = (
    'Lot Count',
    'Lot 1',
    'Lot 2',
    'Lot 3',
    'Lot 4',
    'Lot 5',
    'Lot 6',
    'Lot 7',
    'Lot 8',
    'Lot 9',
    'Lot 10',
    'Lot 11',
    'Lot 12',
    'Lot 13',      //Einf�gen von 13-30; ACHTUNG, auch HostlinkShare.pas-cProdGroupPalette entsprechend erweitern Nue:11.10.06
    'Lot 14',
    'Lot 15',
    'Lot 16',
    'Lot 17',
    'Lot 18',
    'Lot 19',
    'Lot 20',
    'Lot 21',
    'Lot 22',
    'Lot 23',
    'Lot 24',
    'Lot 25',
    'Lot 26',
    'Lot 27',
    'Lot 28',
    'Lot 29',
    'Lot 30'
  );

const
  cOfflimitTextCount = 1;  // registry key value = cMaxLotCount
  cOfflimitText: Array[1..cOfflimitTextCount] of String = (
    '(*)Zeige Detail'  // ivlm
  );

const
  cLotData: Array[TLotData] of String = (
    '(*)Partiename',          // ivlm
    '(*)Spindelbereich',      // ivlm
    '(*)Gruppen Nutzeffekt',  // ivlm
    '(*)Artikelname',         // ivlm
    '(*)Startmode',           // ivlm
    'CYTot/DU',
    'CF/DU',
    'SP/DU',
    'CSP/DU',
    'ProdKg',
    'ProdStart',
    'CL/DU',
    'CN/DU',
    'CS/DU',
    'CT/DU',
    'INEPS/Km',
    'ISMAL/m',
    'ITHICK/Km',
    'ITHIN/Km'
  );

//..............................................................................
// TABLE OF SUB DID DEFINITIONS
//..............................................................................
const
  cSubDIDList: Array[slLot..slMachineState] of TSubDidDef = (
    (SubDidCount: cLotNumberCount;
     Title:       '(*)Partieanzahl';  // ivlm
     SelInfo:     @cLotNumberStr),

    (SubDidCount: Integer(High(TDataSelection));
     Title:       '(*)Maschinendaten';  // ivlm
     SelInfo:     @cDataSelectionText), // array from MMLinkShare

    (SubDidCount: Integer(High(TTrendItems))+1;
     Title:       '(*)Trendauswahl';
     SelInfo:     @cTrendSelection),

    (SubDidCount: cOfflimitTextCount;
     Title:       '(*)Offlimitdetail';  // ivlm
     SelInfo:     @cOfflimitText),

    (SubDidCount: Integer(High(TLotData))+1;
     Title:       '(*)Partieinfo';  // ivlm
     SelInfo:     @cLotData),

    (SubDidCount: cMachineStateTextCount;
     Title:       '(*)Maschinenstatus';  // ivlm
     SelInfo:     @cMachineStateText)
  );

//******************************************************************************
// Global variable
//******************************************************************************
var
  gConnectionStats: TLinkTotStats;
  gEventLog: TEventLogWriter;

//******************************************************************************
// published procedure and functions
//******************************************************************************
function ConvertToIDateTime(aDateTime: TDateTime): TIDateTime;
function ConvertToIDate(aDateTime: TDateTime): TIDate;
function ConvertToITime(aDateTime: TDateTime): TITime;
function GetDIDColName(aDid: TDidItems): String;
function GetDIDFormat(aDataTyp: TFloorDataTyp): DWord;
function GetDIDItemFromName(aLongText: String; var aDataItem: TDidItems): Boolean;
function GetDIDName(aDid: TDidItems): String;
function GetDIDSize(aDid: TDidItems): Word;
function GetDidType(aDid: TDidItems): TFloorDataTyp;
function GetKeyColNames(aDid: TDidItems): String;
function GetTrendColName(aTrend: TTrendItems): String;
function GetTrendFactor(aTrend: TTrendItems): Integer;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

//  RzCSIntf,
  SysUtils, TypInfo, LoepfeGlobal, MMMessages;

//------------------------------------------------------------------------------
function ConvertToIDateTime(aDateTime: TDateTime): TIDateTime;
begin
  Result[0] := StrToInt(FormatDateTime('yy', aDateTime));
  Result[1] := StrToInt(FormatDateTime('mm', aDateTime));
  Result[2] := StrToInt(FormatDateTime('dd', aDateTime));
  Result[3] := StrToInt(FormatDateTime('hh', aDateTime));
  Result[4] := StrToInt(FormatDateTime('nn', aDateTime));
  Result[5] := StrToInt(FormatDateTime('ss', aDateTime));
end;
//------------------------------------------------------------------------------
function ConvertToIDate(aDateTime: TDateTime): TIDate;
begin
  Result[0] := StrToInt(FormatDateTime('yy', aDateTime));
  Result[1] := StrToInt(FormatDateTime('mm', aDateTime));
  Result[2] := StrToInt(FormatDateTime('dd', aDateTime));
end;
//------------------------------------------------------------------------------
function ConvertToITime(aDateTime: TDateTime): TITime;
begin
  Result[0] := StrToInt(FormatDateTime('hh', aDateTime));
  Result[1] := StrToInt(FormatDateTime('nn', aDateTime));
  Result[2] := StrToInt(FormatDateTime('ss', aDateTime));
end;
//------------------------------------------------------------------------------
function GetDIDColName(aDid: TDidItems): String;
begin
  Result := '';
  if (aDid > diUnknown) and (aDid <= High(TDidItems)) then begin
    case aDID of
      diShiftInDay: Result := cMetaData[aDid].ShortText;
    else
      Result := ReplaceSubStr(LowerCase(GetDIDName(aDid)), 'di', 'c_');
    end;
  end;
end;
//------------------------------------------------------------------------------
function GetDIDFormat(aDataTyp: TFloorDataTyp): DWord;
begin
  case aDataTyp of
    fdtChar, fdtByte, fdtShort, fdtWord, fdtLong, fdtDWord, fdtSingle, fdtDouble:
      Result := cFormatNumericItems;
    fdtString:
      Result := cFormatTextItems;
    fdtDate, fdtTime, fdtDateTime, fdtOLEDateTime:
      Result := cFormatTimeItems;
  else
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------
function GetDIDItemFromName(aLongText: String; var aDataItem: TDidItems): Boolean;
var
  i: TDidItems;
begin
  aDataItem := diUnknown;
  for i:=diMachine_Name to High(TDidItems) do begin
    if CompareText(aLongText, cMetaData[i].LongText) = 0 then begin
      aDataItem := i;
      Break;
    end;
  end;
  Result := (aDataItem <> diUnknown);
end;
//------------------------------------------------------------------------------
function GetDIDName(aDid: TDidItems): String;
begin
  Result := 'Unknown DID';
  if (aDid > diUnknown) and (aDid <= High(TDidItems)) then
    Result := GetEnumName(TypeInfo(TDidItems), Ord(aDid));
end;
//------------------------------------------------------------------------------
function GetDIDSize(aDid: TDidItems): Word;
begin
  Result := 0;
  if (aDid > diUnknown) and (aDid <= High(TDidItems)) then
    Result := cMetaData[aDid].DataSize;
end;
//------------------------------------------------------------------------------
function GetDidType(aDid: TDidItems): TFloorDataTyp;
begin
  Result := fdtByte;
  if (aDid > diUnknown) and (aDid <= High(TDidItems)) then
    Result := cMetaData[aDid].DataTyp;
end;
//------------------------------------------------------------------------------
function GetKeyColNames(aDid: TDidItems): String;
begin
  Result := '';
  if (aDid > diUnknown) and (aDid <= High(TDidItems)) then begin
    case aDID of
      diYM_set_name: Result := 'yms.c_ym_set_id, yms.c_ym_set_name';
    else
      Result := ReplaceSubStr(LowerCase(GetDIDName(aDid)), 'di', 'c_');
    end;
  end;
end;
//------------------------------------------------------------------------------
function GetTrendColName(aTrend: TTrendItems): String;
begin
  Result := '';
  if (aTrend >= Low(TTrendItems)) and (aTrend <= High(TTrendItems)) then begin
    Result := cTrendColNames[aTrend].ColName;
  end;
end;
//------------------------------------------------------------------------------
function GetTrendFactor(aTrend: TTrendItems): Integer;
begin
  Result := 0;
  if (aTrend >= Low(TTrendItems)) and (aTrend <= High(TTrendItems)) then begin
    Result := cTrendColNames[aTrend].Factor;
  end;
end;
//------------------------------------------------------------------------------

initialization
  FillChar(gConnectionStats, Sizeof(gConnectionStats), 0);
finalization
end.


