(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseStoreXClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt die Klasse fuer den DB-Zugriff der StoreX Threads.
|
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 16.09.1998  1.00  Mg  | Datei erstellt
| 25.03.1999  1.01  Mg  | TDBAccess.Init : Session name with time stamp
| 11.02.2000  1.02  Mg  | TDBAccess.Init : Exception in try of Open inserted
| 16.02.2000  1.03  Mg  | Property DataBase new TmmDataBase
|=========================================================================================*)

unit DBAccessClass;

interface
uses
  Windows, SysUtils, Classes,Db, DBTables,MMQuery,mmDatabase;

const
  cMaxQueries = 6;
  cPrimaryQuery = 0;
  cSecondaryQuery = 1;
  cThirdQuery = 2;
type

//..............................................................................
  TQueryArr = array [0..cMaxQueries - 1] of TmmQuery;
//..............................................................................
//..............................................................................
  TDBAccess = class(TObject)
  private
    { Private declarations }
    mDBAliasName: String;
    mSession  : TSession;
    fDatabase : TmmDatabase;
    mQueries  : TQueryArr;
    mNumQueries : integer;
    fDBError    : DWord;
    fDBErrorTxt : String;
  public
    { Public declarations }
    constructor Create( aNumOfQueries : integer; aDBODBC: Boolean );
    destructor Destroy; override;
    function Init : Boolean;
    property Query : TQueryArr read mQueries write mQueries;
    property DataBase : TmmDataBase read fDataBase write fDataBase;
    property DBError : DWord read fDBError;
    property DBErrorTxt : String read fDBErrorTxt;
  end;
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,
  LoepfeGlobal, BaseGlobal;

const
  cDatabaseName = 'MMDatabase_';

//---implementation of TDBAccess------------------------------------------------
 constructor TDBAccess.Create ( aNumOfQueries : integer; aDBODBC: Boolean );
 var  x : integer;
 begin
   inherited Create;
   fDBError := NO_ERROR;
   if ( aNumOfQueries > 0 ) and ( aNumOfQueries <= cMaxQueries )  then begin
     mNumQueries := aNumOfQueries;

     if aDBODBC then
       mDBAliasName := gDBAliasNameODBC
     else
       mDBAliasName := gDBAliasNameNative;

     mSession := TSession.Create ( Nil );
     mSession.AutoSessionName := False;
     try
       fDatabase := TmmDataBase.Create ( Nil );
     except
       sysutils.beep;
     end;


     // Create the Queries
     for x := 0 to aNumOfQueries - 1 do
       mQueries[x] := TmmQuery.Create ( Nil );
   end else begin
     mSession := Nil;
     fDatabase := Nil;
   end;
 end;

//------------------------------------------------------------------------------
 destructor TDBAccess.Destroy;
 var   x : integer;
 begin
   if ( mNumQueries <= cMaxQueries ) and ( mNumQueries > 0 ) then begin
     for x := 0 to mNumQueries - 1 do begin
       mQueries[x].Close;
       FreeAndNil(mQueries[x]);
     end;
     fDataBase.Close;
     fDatabase.Free;
     mSession.Close;
     mSession.Free;
   end;
   inherited Destroy;
 end;

//------------------------------------------------------------------------------
  function TDBAccess.Init : boolean;
  var   x      : integer;
        xTime  : TTimeStamp;
  begin
    fDBErrorTxt := '';

    Result := true;
    if ( mNumQueries > 0 ) then begin
      if ( mNumQueries <= cMaxQueries ) then begin
        xTime := DateTimeToTimeStamp ( Time );
        mSession.SessionName := cWorkerSessionName + IntToStr ( Windows.GetCurrentThreadId ) + '_' + IntToStr ( xTime.Time );
//        CodeSite.SendString('TDBAccess.Init.mSession.SessionName', mSession.SessionName);
        with fDatabase do begin
          SessionName := mSession.SessionName;
          DatabaseName := cDatabaseName + IntToStr ( xTime.Time );;
          AliasName := mDBAliasName;
          Params.Clear;
          Params.Add('USER NAME=' + gDBUsername);
          Params.Add('PASSWORD=' + gDBPassword);
          LoginPrompt := false;
          KeepConnection := true;
        end; // with

        for x := 0 to mNumQueries - 1 do begin
          mQueries[x].DatabaseName := fDatabase.DatabaseName;
          mQueries[x].SessionName := mSession.SessionName;
          mQueries[x].Name := 'Query_' + mSession.SessionName;
        end;

        try
          fDatabase.Open;
        except
          on E:EdbEngineError do begin
            fDBError := DWord ( e.Errors [ e.ErrorCount - 1 ].ErrorCode );
            fDBErrorTxt := E.Message;
            Result := false;
          end;
          on E:Exception do begin
            fDBError := ERROR_INVALID_FUNCTION;
            fDBErrorTxt := E.Message;
            Result := false;
          end;
        end;
      end else begin
        Result := false;
      end; // if
    end; // if
  end;
end.

