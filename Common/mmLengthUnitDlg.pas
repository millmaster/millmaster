(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmLengthUnitDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Component to select a unit (100km, 1000km etc...)
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 22.04.99 | 0.01 | PW  | file created
| 07.05.99 | 0.02 | PW  |
| 20.07.00 | 1.00 | Mg  | Umbau auf TLenBase
| 14.08.00 | 1.00 | Wss | TLenBase changed into LoepfeGlobal to avoid include MMUGlobal
|=========================================================================================*)

unit mmLengthUnitDlg;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,CheckLst,ComCtrls,ToolWin,ImgList,ActnList,mmRadioGroup,mmToolBar,mmActionList,
 mmImageList,mmPanel,IvDictio,IvMulti,IvEMulti,BASEFORM,mmTranslator,mmButton,
 mmLengthUnit, LoepfeGlobal;

(*
type
 eLengthUnitT = (lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,lu1Kg,lu1Lbs,luAbsolute,luCone,luBobine);
 sLengthUnitT = set of eLengthUnitT;

const
 cEuropeanLengthUnits = [lu1000Km,lu100Km,lu1Kg,luAbsolute,luCone,luBobine];
 cAmericanLenthUnits  = [lu1000Kyrd,lu100Kyrd,lu1Lbs,luAbsolute,luCone,luBobine];
 cAllLengthUnits      = [lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,lu1Kg,lu1Lbs,luAbsolute,luCone,luBobine];
 cClassLengthUnits    = [lu1000Km,lu100Km,lu1000Kyrd,lu100Kyrd,luAbsolute];
 *)

type
 TmmLengthUnitsDlg = class(TmmForm)

  Panel1: TmmPanel;
   rgUnits: TmmRadioGroup;

  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  mmTranslator1: TmmTranslator;

  paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure acHelpExecute(Sender: TObject);
  procedure FormCreate(Sender: TObject);
  private
   FLengthUnit: TLenBase;
   FLengthUnitSet: sLengthUnitT;
   procedure SetLengthUnit(value: TLenBase);
   function GetLengthUnit: TLenBase;
   procedure SetLengthUnitSet(value: sLengthUnitT);
  public
   property LengthUnit: TLenBase read GetLengthUnit write SetLengthUnit;
   property LengthUnitSet: sLengthUnitT write SetLengthUnitSet;
 end; //TmmLengthUnitsDlg

var
 mmLengthUnitsDlg: TmmLengthUnitsDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TmmLengthUnitsDlg.SetLengthUnit(value: TLenBase);
var
 xUnit : TLenBase;

begin
 if not(Value in FLengthUnitSet) then
  for xUnit := lbAbsolute to lb1000000Y do
   if xUnit in FLengthUnitSet then Value := xUnit;

 FLengthUnit := value;
 rgUnits.ItemIndex := rgUnits.Items.IndexOfObject(TObject(ord(value)));
end; //procedure TmmLengthUnitsDlg.SetLengthUnit
//-----------------------------------------------------------------------------
function TmmLengthUnitsDlg.GetLengthUnit: TLenBase;
begin
 with rgUnits do
  result := TLenBase(Longint(Items.Objects[ItemIndex]));
end; //function TmmLengthUnitsDlg.GetLengthUnit
//-----------------------------------------------------------------------------
procedure TmmLengthUnitsDlg.SetLengthUnitSet(value: sLengthUnitT);
const
 cUnitArray : array[TLenBase] of string[20] = (    '(30)Absolut',  //ivlm
                                                   '100 Km',
                                                   '1.000 Km',
                                                   '100.000 yrd',
                                                   '1.000.000 yrd');

                                                  //Mg  '1 Kg',
                                                  // '1 lbs',
                                                  // '(30)Kone',     //ivlm
                                                  // '(30)Kops');    //ivlm

var
 xCounter : integer;
 xUnit    : TLenBase;

begin
 FLengthUnitSet := value;
 xCounter := 0;

 rgUnits.Items.Clear;

 for xUnit := lbAbsolute to lb1000000Y  do
  if xUnit in FLengthUnitSet then
  begin
   inc(xCounter);
   rgUnits.Items.AddObject(mmTranslator1.Dictionary.Translate(cUnitArray[xUnit]),TObject(ord(xUnit)));
  end; //if xUnit in FLengthUnitSet then

 rgUnits.Height := 15 +20 *xCounter;
 ClientHeight := rgUnits.Height +10 +paButtons.Height;
end; //procedure TmmLengthUnitsDlg.SetLengthUnit
//-----------------------------------------------------------------------------
procedure TmmLengthUnitsDlg.acOKExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrOK;
end; //procedure TmmLengthUnitsDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmLengthUnitsDlg.acCancelExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrCancel;
end; //procedure TmmLengthUnitsDlg.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmLengthUnitsDlg.acHelpExecute(Sender: TObject);
begin
//
end; //procedure TmmLengthUnitsDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TmmLengthUnitsDlg.FormCreate(Sender: TObject);
begin
 FLengthUnitSet := cEuropeanLengthUnits;
 FLengthUnit := lb100Km;
end; //procedure TmmLengthUnitsDlg.FormCreate

end. //mmLengthUnit.pas
