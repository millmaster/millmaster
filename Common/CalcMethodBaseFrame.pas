(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: CalcMethodBaseFrame
| Projectpart...: MillMaster Spulerei
| Subpart.......: Berechnungsmethoden
| Process(es)...: -
| Description...: GUI f�r die Berechnungsmethoden
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows NT/2000/XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 16.12.2003  1.00  Lok | Initial release
|=============================================================================*)
unit CalcMethodBaseFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLineLabel, CalcMethods, mmLabel, mmStringList, mmEdit,
  IvDictio, IvMulti, IvEMulti, mmTranslator, LoepfeGlobal, ClassDef;

const
  cCalcMode   = 'CalcMode';  // Dieser CalcMode wird nur f�r die Darstellung der Einheit von Grenzwerten ben�tigt 
  cLenBase    = 'LenBase';   // Diese LenBase wird nur f�r die Darstellung der Einheit von Grenzwerten ben�tigt
  
type
  TfrmCalcMethodBaseFrame = class;
  TBaseCalcMethodFrameClass = class of TfrmCalcMethodBaseFrame;
  
  //1 Event f�r die Benachrichtigung �ber �nderungen in den Eingabefeldern 
  TGUIMethodSettingsChange = procedure (aSender: TfrmCalcMethodBaseFrame) of object;
  (*: Klasse:        TfrmCalcMethodBaseFrame
      Vorg�nger:     TFrame
      Kategorie:     No category
      Kurzbeschrieb: Basisklasse f�r das GUI der Berechnungsmethoden 
      Beschreibung:  
                     Jede Berechnungsmethode muss einen Frame von dieser Klasse ableiten und die 
                     Methodensettings �ber GUI Elemente zug�nglich machen. Weiter muss in der 
                     Factorymethode der neue Frame erzeugt werden. *)
  //1 Basisklasse f�r das GUI der Berechnungsmethoden 
  TfrmCalcMethodBaseFrame = class(TFrame)
    mmTranslator1: TmmTranslator;
    //1 Diese Methode kann in einem Nachfolger mit 'OnExit' verdrahtet werden 
    procedure GUIChanged(Sender: TObject);
    //1 Diese Methode kann in einem Nachfolger mit 'OnChange' verdrahtet werden 
    procedure GUIChanging(Sender: TObject);
  private
    FGUIMethodSettingsChanged: TGUIMethodSettingsChange;
    FGUIMethodSettingsChanging: TGUIMethodSettingsChange;
    FReadOnly: Boolean;
    mNoNotification: Boolean;
    //1 Liefert die Beschreibung f�r die Berechnungsmethode 
    function GetDescription: String;
    //1 Liefert die Settings als 'Name=Wert'-Liste 
    function GetMethodSettings: String;
    //1 Setzt die GUI Elemente (Settings als 'Name=Wert'-Liste) 
    procedure SetMethodSettings(const Value: String);
    //1 Wenn true, dann wird der Frame in den ReadOnly Modus gesetzt 
    procedure SetReadOnly(Value: Boolean);
  protected
    mCalcMethodType: TCalculationMethod;
    mDataItem: TBaseDataItem;
    mMethodParam: TmmStringList;
    //1 Wird f�r jedes verdrahtete GUI Element beim OnExit aufgerufen 
    procedure DoGUIMethodSettingsChanged(aSender: TfrmCalcMethodBaseFrame); virtual;
    //1 Wird f�r jedes verdrahtete GUI Element beim OnChange aufgerufen 
    procedure DoGUIMethodSettingsChanging(aSender: TfrmCalcMethodBaseFrame); virtual;
    //1 Im Nachfolger zu �berschreibende Methode um die methodenabh�ngigen Settings hinzuzuf�gen 
    procedure GetInternalMethodSettings; virtual;
    //1 Im Nachfolger zu �berschreibende Methode um die methodenabh�ngigen Settings ins GUIO zu �bernehmen 
    procedure SetInternalMethodSettings; virtual;
    //1 Setzt oder entfernt den ReadOnly Modus 
    procedure SetResetReadOnly(aReadOnly: boolean); virtual;
  public
    //1 Konstruktor 
    constructor Create(aOwner: TComponent; aDataItem: TBaseDataItem); reintroduce; overload; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Beschreibung f�r die Berechnungsmethode 
    property Description: String read GetDescription;
    //1 Settings als 'Name=Wert'-Liste 
    property MethodSettings: String read GetMethodSettings write SetMethodSettings;
    //1 Wird f�r jedes verdrahtete GUI Element beim OnExit aufgerufen 
    property OnGUIMethodSettingsChanged: TGUIMethodSettingsChange read FGUIMethodSettingsChanged write
      FGUIMethodSettingsChanged;
    //1 Wird f�r jedes verdrahtete GUI Element beim OnChange aufgerufen 
    property OnGUIMethodSettingsChanging: TGUIMethodSettingsChange read FGUIMethodSettingsChanging write
      FGUIMethodSettingsChanging;
    //1 Wenn true, dann wird der Frame in den ReadOnly Modus gesetzt 
    property ReadOnly: Boolean read FReadOnly write SetReadOnly;
  end;
  

resourcestring
  srAbsolute     = '(30)Absolut';       //ivlm

function CalcMethodFrameFactory(aMethod: TCalculationMethod; aDataItem: TBaseDataItem): TfrmCalcMethodBaseFrame;

function GetLenBaseString(aLenBase: TLenBase): String; overload;

function GetLenBaseString(aCalcMode: integer): String; overload;

implementation

{$R *.DFM}
(*-----------------------------------------------------------
  Erzeugt den gew�nschten Frame. Der Aufrufer ist verantwortlich, die
  R�ckgabe auf nil zu pr�fen.
-------------------------------------------------------------*)
function CalcMethodFrameFactory(aMethod: TCalculationMethod; aDataItem: TBaseDataItem): TfrmCalcMethodBaseFrame;
var
  xFrameClass: TPersistentClass;
begin
  result := nil;
  
  // Zuerst wird eine Klassenreferenz auf die GUI Klasse der Methode geholt
  xFrameClass := FindClass(cCalculationMethods[aMethod].GUIClassName);
  
  (* Ist eine Klassenreferenz gefunden, dann wird mit dieser der Frame erzeugt.
     Die Klasse muss mit RegisterClass() registriert worden sein.
     --> siehe initialzation in der entsprechenden Unit.*)
  if xFrameClass.InheritsFrom(TfrmCalcMethodBaseFrame) then
    result := TBaseCalcMethodFrameClass(xFrameClass).Create(nil, aDataItem);
end;// function CalcMethodFrameFactory(aMethod: TCalculationMethod): TfrmCalcMethodBaseFrame;

(* Definitionen aus LabMasterDef:

   CalcMode:
   0  = Laengenbasiert (LenBase)
   >0 = Laengenbasiert (Zahl entspricht Meter)
   -1 = Keine Berechnung: Mode fuer 1:1 Durchreichung ab DB c_ -> calc_
   -2 = SFI/D
   -3 = SFI
   -4 = ProdNutzeffekt
   -5 = Temporaerer Mode fuer 1:1 Durchreichung ab DB c_ -> calc_ (in spaeterer Phase KeyField mit Visibility mit enum's erweitern! NUE
*)

(*-----------------------------------------------------------
  Liefert die L�ngenbasis anhand des CalcMode f�r unregelm�ssige DataItems
-------------------------------------------------------------*)
function GetLenBaseString(aCalcMode: integer): String;
begin
  result := '';
  if (aCalcMode > 0) then begin
    if (aCalcMode mod 1000 = 0) then
      result := Format('/%dkm', [aCalcMode div 1000])
    else
      result := Format('/%dm', [aCalcMode]);
  end;// if (aCalcMode > 0) then begin
end;// function GetLenBaseString(aCalcMode: integer): String;

(*-----------------------------------------------------------
  Liefert die gew�nschte L�ngenbasis als String
-------------------------------------------------------------*)
function GetLenBaseString(aLenBase: TLenBase): String;
begin
  Result := '';
  case aLenBase of
    lbAbsolute : Result := srAbsolute;
    lb100KM    : Result := '/100km';
    lb1000KM   : Result := '/1000km';
    lb100000Y  : Result := '/100000 yards';
    lb1000000Y : Result := '/1000000 yards';
  end;// case GetLenBase of
end;// function GetLenBaseString: TLenBase;







(*:
  Klasse:        TfrmCalcMethodBaseFrame
 *  Vorg�nger:     TFrame
 *  Kategorie:     No category
 *  Kurzbeschrieb: Basisklasse f�r das GUI der Berechnungsmethoden 
 *  Beschreibung:  Jede Berechnungsmethode muss einen Frame von dieser Klasse ableiten und die 
                   Methodensettings �ber GUI Elemente zug�nglich machen. Weiter muss in der 
                   Factorymethode der neue Frame erzeugt werden. 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aOwner, aDataItem)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmCalcMethodBaseFrame.Create(aOwner: TComponent; aDataItem: TBaseDataItem);
begin
  inherited Create(aOwner);
  mMethodParam := TmmStringList.Create;
  mNoNotification := false;
  mDataItem := aDataItem;
end;// TfrmCalcMethodBaseFrame.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmCalcMethodBaseFrame.Destroy;
begin
  FreeAndNil(mMethodParam);
  inherited;
end;// TfrmCalcMethodBaseFrame.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGUIMethodSettingsChanged
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender)
 *
 *  Kurzbeschreibung: Wird f�r jedes verdrahtete GUI Element beim OnExit aufgerufen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.DoGUIMethodSettingsChanged(aSender: TfrmCalcMethodBaseFrame);
begin
  if assigned(FGUIMethodSettingsChanged) then
    FGUIMethodSettingsChanged(self);
end;// TfrmCalcMethodBaseFrame.DoGUIMethodSettingsChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoGUIMethodSettingsChanging
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aSender)
 *
 *  Kurzbeschreibung: Wird f�r jedes verdrahtete GUI Element beim OnChange aufgerufen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.DoGUIMethodSettingsChanging(aSender: TfrmCalcMethodBaseFrame);
begin
  if assigned(FGUIMethodSettingsChanging) then
    FGUIMethodSettingsChanging(self);
end;// TfrmCalcMethodBaseFrame.DoGUIMethodSettingsChanging cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDescription
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Beschreibung f�r die Berechnungsmethode
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmCalcMethodBaseFrame.GetDescription: String;
begin
  result := StringReplace(Translate(cCalculationMethods[mCalcMethodType].Description), cNewLine, ccrlf, [rfReplaceAll]);
end;// TfrmCalcMethodBaseFrame.GetDescription cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetInternalMethodSettings
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Im Nachfolger zu �berschreibende Methode um die methodenabh�ngigen Settings hinzuzuf�gen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.GetInternalMethodSettings;
begin
  mMethodParam.Values[cMethodType] := IntToStr(ord(mCalcMethodType));
end;// TfrmCalcMethodBaseFrame.GetInternalMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMethodSettings
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Settings als 'Name=Wert'-Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TfrmCalcMethodBaseFrame.GetMethodSettings: String;
begin
  GetInternalMethodSettings;
  result := mMethodParam.CommaText;
end;// TfrmCalcMethodBaseFrame.GetMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           GUIChanged
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Diese Methode kann in einem Nachfolger mit 'OnExit' verdrahtet werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.GUIChanged(Sender: TObject);
begin
  if not(mNoNotification) then
    DoGUIMethodSettingsChanged(self);
end;// TfrmCalcMethodBaseFrame.GUIChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           GUIChanging
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Diese Methode kann in einem Nachfolger mit 'OnChange' verdrahtet werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.GUIChanging(Sender: TObject);
begin
  if not(mNoNotification) then
    DoGUIMethodSettingsChanging(self);
end;// TfrmCalcMethodBaseFrame.GUIChanging cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetInternalMethodSettings
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Im Nachfolger zu �berschreibende Methode um die methodenabh�ngigen Settings ins GUIO zu �bernehmen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.SetInternalMethodSettings;
begin
end;// TfrmCalcMethodBaseFrame.SetInternalMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMethodSettings
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Setzt die GUI Elemente (Settings als 'Name=Wert'-Liste)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.SetMethodSettings(const Value: String);
begin
  try
    mNoNotification := true;
  
    mMethodParam.CommaText := Value;
    SetInternalMethodSettings;
  finally
    mNoNotification := false;
  end;// try finally
end;// TfrmCalcMethodBaseFrame.SetMethodSettings cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetReadOnly
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Wenn true, dann wird der Frame in den ReadOnly Modus gesetzt
 *  Beschreibung:     
                      Soll das Standardverhalten ge�ndert werden muss die Funktion 'SetResetReadOnly' 
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.SetReadOnly(Value: Boolean);
begin
  if FReadOnly <> Value then
  begin

//: ----------------------------------------------

//: ----------------------------------------------
    FReadOnly := Value;

//: ----------------------------------------------
    SetResetReadOnly(Value);

//: ----------------------------------------------
  end;
end;// TfrmCalcMethodBaseFrame.SetReadOnly cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetResetReadOnly
 *  Klasse:           TfrmCalcMethodBaseFrame
 *  Kategorie:        No category 
 *  Argumente:        (aReadOnly)
 *
 *  Kurzbeschreibung: Setzt oder entfernt den ReadOnly Modus
 *  Beschreibung:     
                      Diese Methode kann in einem Nachfolger �berschrieben werden, sofern das
                      Standardverhalten nicht gen�gt.
 --------------------------------------------------------------------*)
procedure TfrmCalcMethodBaseFrame.SetResetReadOnly(aReadOnly: boolean);
var
  i: Integer;
begin
  for i := 0 to ComponentCount - 1 do begin
    if Components[i] is TmmEdit then
      TmmEdit(Components[i]).ReadOnly := aReadOnly;
  end;// for i := 0 to ComponentCount - 1 do begin
end;// TfrmCalcMethodBaseFrame.SetResetReadOnly cat:No category

end.

