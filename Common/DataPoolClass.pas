(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataPoolClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 29.07.1998  0.00  Wss | Datei erstellt, start Implementierung Klasse TDataPool
| 30.07.1998  1.00  Wss | Klasse TDataPool fertig gestellt
| 01.10.1998  1.01  Wss | TDataPool an JobHeader angepasst.
| 14.10.1998  1.02  Mg  | Function GetAllJobIDs eingefuegt.
| 28.10.1998  1.03  Wss | Ableitung auf TProtectedList geaendert
| 17.11.1998  1.04  Wss | Auf TmmXXX Klassen umgestellt
| 16.04.1999  1.05  Mg  | GetAllJobIDs with new parameter
| 25.05.2000  1.06  Mg  | TDataPool.GetList : Second trial inserted
| 06.06.2000  1.07  Mg  | TDataPool.GetList : Wait 2 seconds
| 07.06.2000  1.08  Mg  | TDataPool.GetList : Second trial and wait deleted
|=========================================================================================*)
unit DataPoolClass;

interface

uses
  mmList, Classes, Windows, SysUtils, 
  IPCClass, BaseGlobal;

type
  PJobListRec = ^TJobListRec;
  TJobListRec = packed record
    JobID: Word;
    List:  TmmList;
  end;


  TDataPool = class(TProtectedList)
  private
    function DelAllItemsInLine(aList: TmmList): Boolean;
    function DelJobLine(aJobListRec: PJobListRec): Boolean;
    function GetJobListRec(var aJobListRec: PJobListRec; aJobID: Word; aDelete: Boolean): Boolean;
  public
    destructor Destroy; override;
    function Add(aJob: PJobRec): Boolean;
    function GetList(var aList: TmmList; aJobID: Word): Boolean;
    function DelJobID(aJobID: Word): Boolean;
    function GetAllJobIDs ( var aJobIDList : TJobIDList; var aNumJobs : Word ): boolean;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//-----------------------------------------------------------------------------
destructor TDataPool.Destroy;
begin
  // clear all JobLines in DataPool List
  while Count > 0 do begin
    DelJobLine(Items[0]);
    Delete(0);
  end;

  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TDataPool.Add(aJob: PJobRec): Boolean;
var
  xTmpList: TmmList;
  xJob: PJobRec;
  xJobLine: PJobListRec;
  xSize: DWord;
begin
  Result := False;
  // FIRST look if we have already an horizontal line of this JobID
  if not GetList(xTmpList, aJob^.JobID) then begin
    // this message is a new one. No entry found in list -> create new JobListRec object
    try
      new(xJobLine);
      xJobLine^.List  := TmmList.Create;
    except
      fListError := ERROR_NOT_ENOUGH_MEMORY;
      Exit;
    end;
    xJobLine^.JobID := aJob^.JobID;
    // call Add methode of original TmmList class
    inherited Add(xJobLine);
    // use now the new created message list
    xTmpList := xJobLine^.List;
  end;

  // NOW work with the horizontal line of JobListRec
  xSize := aJob^.JobLen;
  // allocate memory to store the message
  try
    GetMem(xJob, xSize);
    // use the Move procedure not the Move methode of derived TmmList class
    System.Move(aJob^, xJob^, xSize);
    // add new JobItem to horizontal list
    xTmpList.Add(xJob);
    Result := True;
  except
    fListError := ERROR_NOT_ENOUGH_MEMORY;
  end;
end;
//-----------------------------------------------------------------------------
function TDataPool.DelAllItemsInLine(aList: TmmList): Boolean;
var
  xJob: PJobRec;
begin
  Result := True;
  // loop until message entries are empty
  while aList.Count > 0 do begin
    xJob := aList.Items[0];
    // let the memory gone
    FreeMem(xJob, xJob^.JobLen );
    // remove pointer from list
    aList.Delete(0);
  end;
end;
//-----------------------------------------------------------------------------
function TDataPool.DelJobID(aJobID: Word): Boolean;
var
  xJobListRec: PJobListRec;
begin
  LockList;
  try
    Result := GetJobListRec(xJobListRec, aJobID, True);
    if Result then
      DelJobLine(xJobListRec);
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TDataPool.DelJobLine(aJobListRec: PJobListRec): Boolean;
begin
  // first delete all messages in this thread
  DelAllItemsInLine(aJobListRec^.List);
  // destroy horizontal TmmList object
  aJobListRec^.List.Free;
  // let the memory gone
  Dispose(aJobListRec);

  Result := True;
end;
//-----------------------------------------------------------------------------
function TDataPool.GetList(var aList: TmmList; aJobID: Word): Boolean;
var
 xJobListRec: PJobListRec;
begin
  LockList;
  try
    Result := GetJobListRec(xJobListRec, aJobID, False);
    if Result then
      aList := xJobListRec^.List;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TDataPool.GetJobListRec(var aJobListRec: PJobListRec; aJobID: Word; aDelete: Boolean): Boolean;
var
  i: Integer;
begin
  // Diese Funktion wird nur jeweils innerhalb von Lock/UnlockList verwendet. Daher ist es nicht n�dit
  // hier nochmals zu sperren.
  Result := False;
  fListError := ERROR_INVALID_PARAMETER;
  // count through pool list to get the TmmList object of aJobID
  for i:=0 to Count-1 do begin
    aJobListRec := Items[i];
    // if TmmList with aJobID found ...
    Result := (aJobListRec^.JobID = aJobID);
    if Result then begin
      // ... remove entry from front list if whished
      if aDelete then
        Delete(i);   // this delete removes only the pointer to the horizontal job line
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataPool.GetAllJobIDs(var aJobIDList: TJobIDList; var aNumJobs : Word): boolean;
var
  x: Integer;
  xJobListRec: PJobListRec;
  xJobRec: PJobRec;
begin
  FillChar(aJobIDList, sizeof(TJobIDList), 0);

  aNumJobs := Count;
  LockList;
  try
    for x:=0 to Count-1 do begin
      xJobListRec          := Items[x];
      aJobIDList[x].JobID  := xJobListRec^.JobID;
      xJobRec              := xJobListRec^.List.First;
      aJobIDList[x].JobTyp := ORD(xJobRec.JobTyp);
      // there are only limited space available -> max 1000 yet
      if x >= cMaxJobs-1 then
        Break;
    end;
    Result := True;
  finally
    UnlockList;
  end;
end;
//------------------------------------------------------------------------------
end.

