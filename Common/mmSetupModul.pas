{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MillMasterSource.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.07.1999  1.00  SDo | File created
| 28.09.1999            | Beachte Security levels in Proc. MakeEntries
|                       | DB-Access noch nicht implementiert
| 11.11.1999        SDo | TMMSettingsReader-Klasse implementiert
| 12.12.2000        SDo | Neue Properties, welche die MMPackages und
|                       | MMComponenten ermitteln
| 21.02.2001        SDo | Neues TMMSetupRec-Item Printable; fuer Ausdruck
| 27.02.2001        Wss | Init und GetValue nun nicht mehr Abstract, da automatisch
                          initialisiert wird, wenn 1. mal mit GetValue Wert geholt wird.
| 27.03.2001        Wss | TMMPackageSet type erstellt
| 14.11.2001        Wss | - Aufruf und Dekodierung von Packages korrigiert
                          - wenn nur Packages gelesen werden, waren die Properties nicht initialisiert
                          - Object fuer TMMSettings hier Modulglobal gemacht, damit nicht jeder
                            SettingsReader sein eigenes Objekt erstellt und demzufolge die Daten
                            immer wieder aufs neue initialisiert.
                            Erstellt und freigegeben wird es im Initialization Teil dieser Unit.
                            Initialisiert wird es aber erst, wenn der erste Aufruf fuer Werte erfolgt.
| 28.11.2001        Wss | Erstellen und verwenden von TmmSettings Objekt wieder in
                          Klasse TMMSettingsReader genommen, da es Probleme mit dem Aufraeumen im
                          Finalization Teil gab (AV wenn mit Hostlink im Delphi verwendet)
| 30.11.2001        Wss | RefreshDatapool aufgeteilt in ReadRegistryValues und ReadDBValues um
                          besseres Fehlerhandling zu bekommen.
| 13.12.2001        Wss | TMMSettingsReader.GetValue verbessert: Daten sind ja schon gelesen und
                          muessen demzufolge nicht nochmal geholt werden
| 07.03.2002        Wss | MMPackages, MMComponents sind nicht mehr verschluesselt auf der DB gespeichert
| 19.06.2002        Wss | Init lieferte immer True zurueck, auch wenn DB-Fehler war
| 12.09.2002        Sdo | Umbau auf ADO (Achtung: es gibt kein RecordCount)
===============================================================================}
unit mmSetupModul;

interface

uses
  Windows, SysUtils, Classes, BaseSetup, AdoDbAccess,
  IPCClass, IPCUnit, BaseGlobal, MMEventLog, SettingsReader;
type

  TMMSetupRec = record
    UniqueValueName: string; // unique Name
    Location: TLocation; // Ort der Daten -> Registry, DB, Ini-File
    LocationName: string; // RegistryPath, TabellenName, FileName
    LocationKey: DWord; // RootKey, DBIndex,
    ValueName: string; // KeyNames, DB-Col.Name,
    UserValue: string; // User-, Registry-, DB-Tupel-Wert
    NativeValue: string; // Gleich wie UserValue, bleibt in der orginal Einheit wie in Reg. oder DB
    DefaultValue: string; // Default Wert
    MinValue: string; // Max. Wert
    MaxValue: string; // Min. Wert
    Factor: Real; // Umrechungsfaktor z.B. ms -> Min.; m -> ft
    Mask: string; // Eingabemaske -> Syntax siehe EditMask
    ValueType: TDataType; // Anzeige-Typ (als Float, Datum etc.)
    ValueChange: Boolean; // Wert hat geaendert
    ComponentType: TComponentType; // ctNone, ctEdit, ctSpinButton, ctLabel, ctComboBox, ctMemo
    Group: string; // Gruppename des Eintrages (Kategorie)
    Restart: Boolean; // Benoetigt einen Neustart des MM
    ClientServer: TClientServerType; // Wert wird in der Rubrik C, S oder CS angezeigt
    ValueText: string; // Masseinheit oder kurzer Text fuer einen Wert
    HintText: string; // Hinweistext
    Description: string; // Beschreibung eines Wertes
    Data: string; // Daten
    ReadOnlyData: Boolean; // Falg fuer die Datenveraenderbarkeit
    Printable: Boolean // Darf gedruckt werden, wenn TRUE
  end;
  pSetupRec = ^TMMSetupRec;

  // MM Installation
//  TMMPackage = (mmpEasy, mmpStandard, mmpPro, mmpClearerAssistant);
//  TMMComponent = (mmcQOfflimit, mmcStyle, mmcDatawarehouse, mmcLongTerm, mmcClearerAssistant, mmcLabReport);
//
//  TMMPackageSet = set of TMMPackage;
//  TMMComponentSet = set of TMMComponent;
//
//  TMMInstallsRec = record
//    Package: TMMPackage;
//    Components: TMMComponentSet;
//  end;

  TPartieIndex = (piNoneInput, piArtNameId, piArtNameDate, piArtName, piUserInput);

  //TMMSettingsReader = class;
  //TMMSettingsReader = class;

  TMMSetupModul = class(TBaseSetupModul)
  private
    mApplName: string;
    mEventLogWriter: TEventLogWriter;
    FUseApplName: Boolean;
    function GetUseApplName: Boolean;
    procedure SetUseApplName(const Value: Boolean);
  protected
    procedure Loaded; override;
  public
    mDB: TAdoDBAccess;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure MakeEntries(aPCType: TClientServerType); //override;
    procedure SaveAllRegValues(aChangedValue: Boolean; aPCType: TClientServerType); //override;
    procedure WriteRegToUserValue;
    //function Init: Boolean; override;

   // function GetDBValue(aId: Word): Variant; override;
   // procedure SetDBValue(aId: Word; const Value: Variant); override;

  published
    property UseApplName: Boolean read GetUseApplName write SetUseApplName default False;
  end;

{}
  TMMSettings = class(TBaseSettings)
  private
    FIsComponentClearerAssistant: Boolean;
    FIsComponentLongTerm: Boolean;
    FIsComponentLabReport: Boolean;
    FIsComponentQOfflimit: Boolean;
    FIsComponentStyle: Boolean;
    FIsPackageEasy: Boolean;
    FIsPackageStandard: Boolean;
    FIsPackagePro: Boolean;
    FComponentsAsText: string;
    FPackagesAsText: string;
//    FIsPackageClearerAssistant: Boolean;
//    FIsComponentDataWareHouse: Boolean;
    function GetOnlyTemplateSetsAvailable: Boolean;
  protected
    function GetClientServer(aId: Word): Variant; override;
    function GetComponentType(aId: Word): Variant; override;
    function GetData(aId: Word): Variant; override;
    function GetDefaultValue(aId: Word): Variant; override;
    function GetDescription(aId: Word): Variant; override;
    function GetReadOnlyData(aId: Word): Variant; override;
    function GetFactor(aId: Word): Variant; override;
    function GetGroup(aId: Word): Variant; override;
    function GetHintText(aId: Word): Variant; override;
    function GetLocation(aId: Word): Variant; override;
    function GetLocationKey(aId: Word): Variant; override;
    function GetLocationName(aId: Word): Variant; override;
    function GetMask(aId: Word): Variant; override;
    function GetMaxValue(aId: Word): Variant; override;
    function GetMinValue(aId: Word): Variant; override;
    function GetNativeValue(aId: Word): Variant; override;
    function GetNumberOfValues: Word; override;
    function GetRestart(aId: Word): Variant; override;
    function GetUniqueValueName(aId: Word): Variant; override;
    function GetUserValue(aId: Word): Variant; override;
    function GetValueChange(aId: Word): Variant; override;
    function GetValueName(aId: Word): Variant; override;
    function GetValueText(aId: Word): Variant; override;
    function GetValueType(aId: Word): Variant; override;
    function GetUserValueByName(aUniqueName: string): Variant; override;
    procedure SetData(aId: Word; const Value: Variant); override;
    procedure SetHintText(aId: Word; const Value: Variant); override;
    procedure SetNativeValue(aId: Word; const Value: Variant); override;
    procedure SetUserValue(aId: Word; const Value: Variant); override;
    procedure SetValueChange(aId: Word; const Value: Variant); override;
    procedure SetValueText(aId: Word; const Value: Variant); override;

    function GetDBValue(aId: Word): Variant; override;
    procedure SetDBValue(aId: Word; const Value: Variant); override;

    function GetPrintable(aId: Word): Boolean; override;
    procedure SetPrintable(aId: Word; const Value: Boolean); override;

    procedure SetGroup(aId: Word; const Value: Variant); override;

    procedure FetchInstalledComponents; //Ermittelt die Components & decodiert
    procedure FetchInstalledPackages; //Ermittelt die Packages & decodiert
  public
    mDataBase: TAdoDBAccess;
    mSettings: TMMSettingsReader;

    constructor Create; override;
    destructor Destroy; override;
    function Init: Boolean; override;

    function GetID(aRegUniqueName: string): Integer; override;
    procedure ReadDBValues; override;
    procedure ReadRegistryValues; override;
//    procedure RefreshDatapool; override;

    // MMPackages and MMComponents
    property IsComponentQOfflimit: Boolean read FIsComponentQOfflimit default False;
    property IsComponentStyle: Boolean read FIsComponentStyle default False;
//    property IsComponentDataWareHouse: Boolean read FIsComponentDataWareHouse default False;
    property IsComponentLongTerm: Boolean read FIsComponentLongTerm default False;
    property IsComponentClearerAssistant: Boolean read FIsComponentClearerAssistant default False;
    property IsComponentLabReport: Boolean read FIsComponentLabReport default False;
    property ComponentsAsText: string read FComponentsAsText;

    property IsPackageEasy: Boolean read FIsPackageEasy default False;
    property IsPackageStandard: Boolean read FIsPackageStandard default False;
    property IsPackagePro: Boolean read FIsPackagePro default False;
//    property IsPackageClearerAssistant: Boolean read FIsPackageClearerAssistant default False;
    property PackagesAsText: string read FPackagesAsText;

    property IsOnlyTemplateSetsAvailable: Boolean read GetOnlyTemplateSetsAvailable;

  published
  end;

{
  TMMSettingsReader = class(TBaseSettingsReader)
  private
    fError: TErrorRec;
    mSettings: TmmSettings;
    //    fIsComponentQOfflimit : Boolean;

    //    function ReadArray: Boolean;
    function GetIsComponentQOfflimit: Boolean;
    function GetComponentStyle: Boolean;
    function GetComponentClearerAssistant: Boolean;
//    function GetComponentDataWareHouse: Boolean;
    function GetComponentLongTerm: Boolean;
    function GetComponentLabReport: Boolean;
    function GetComponentsAsText: string;
//    function GetPackageClearerAssistant: Boolean;
    function GetPackagePro: Boolean;
    function GetPackageStandard: Boolean;
    function GetPackagesAsText: string;
    function GetPackageEasy: Boolean;
    function GetOnlyTemplateSetsAvailable: Boolean;
  protected
    function GetValue(aUniqueName: string): Variant; override;

  public
    constructor Create; override;
    destructor Destroy; override;
    function Init: Boolean; override;

    // MMPackages and MMComponents
    property IsComponentQOfflimit: Boolean read GetIsComponentQOfflimit;
    property IsComponentStyle: Boolean read GetComponentStyle;
//    property IsComponentDataWareHouse: Boolean read GetComponentDataWareHouse;
    property IsComponentLongTerm: Boolean read GetComponentLongTerm;
    property IsComponentClearerAssistant: Boolean read GetComponentClearerAssistant;
    property IsComponentLabReport: Boolean read GetComponentLabReport;
    property ComponentsAsText: string read GetComponentsAsText;

    property IsPackageEasy: Boolean read GetPackageEasy;
    property IsPackageStandard: Boolean read GetPackageStandard;
    property IsPackagePro: Boolean read GetPackagePro;
//    property IsPackageClearerAssistant: Boolean read GetPackageClearerAssistant;
    property PackagesAsText: string read GetPackagesAsText;

    property IsOnlyTemplateSetsAvailable: Boolean read GetOnlyTemplateSetsAvailable;

  published
  end;
{}

function WriteMMPackageToDB(aPackageSet: TMMPackageSet): Boolean; overload;
function WriteMMPackageToDB(aPackage: TMMPackage): Boolean; overload;
function DeleteMMPackage(aDelPackage: TMMPackageSet): Boolean;
function DeleteAllPackages: Boolean;

//function Encode(const aText: string): string;
//function Decode(const aText: string): string;

procedure Register;

const
//  cQryUpdate = 'Update t_MMUParm set Data = ''%s'' ' +
//    'where AppName = ''MMConfiguration'' ' +
//    'and Appkey = ''%s'' ';
//  cQryInsert = 'Insert into t_MMUParm (AppName, Appkey, Data) ' +
//    'Values( ''MMConfiguration'', ''%s'', ''%s'' )';

  // Key fuer Kodierung
//  cKey = $D5; //5284565;
//    cKey = $50A2D5;

    //MMPackages als Text
//  cMMPackages: array[TMMPackage] of string = (
//    'Easy',
//    'Standard',
//    'Pro',
//    'ClearerAssistant'
//    );
//
//  //MMComponents als Text
//  cMMComponents: array[TMMComponent] of string = (
//    'QualityOfflimit',
//    'Style',
//    'Datawarehouse',
//    'Longterm',
//    'ClearerAssistant',
//    'LabReport'
//    );

  //MMPackage Zusammenstellungh
//  cMMinstalls: array[TMMPackage] of TMMComponentSet = (
//    // mmpEasy
//    [],
//    // mmpStandard
//    [mmcQOfflimit, mmcStyle, mmcLabReport],
//    // mmpPro
//    [mmcStyle, mmcDatawarehouse, mmcLongTerm],
//    // mmpClearerAssistant
//    [mmcClearerAssistant]
//    );

  // PartieIndex Texte
  cNoInput = '(*)Keine Eingabe'; //ivlm
  cArtName_Id = '(*)Artikelname und ID'; //ivlm
  cArtName_DateTime = '(*)Artikelname und Datum'; //ivlm
  cArtName = '(*)Artikelname'; //ivlm
  cUserInput = '(*)Individueller Name'; //ivlm

  cUserPartieKeyType: array[TPartieIndex] of string = (
    cNoInput,
    cArtName_Id,
    cArtName_DateTime,
    cArtName,
    cUserInput
    );

  //cSuperKey = 12345;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  mmRegistry, LoepfeGlobal, Dialogs, Forms, IvDictio, ActiveX;

const
  cApp = 'MMConfiguration';
  cProgTerminate = 'Program will terminate.'; //
  //  cProgTerminate = 'The User has no database right. Program will terminate.'; //

procedure Register;
begin
  RegisterComponents('MillMaster', [TMMSetupModul]);
end;

  //******************************************************************************
  // Schreibt Packages mit den dazugehoerigen Components in die DB
  //******************************************************************************
//function WriteMMPackageToDB(aPackageSet: TMMPackageSet): Boolean;
function WriteMMPackageToDB(aPackageSet: TMMPackageSet): Boolean;
var
  x: TMMPackage;
  n: TMMComponent;
  xPList, xCList: TStringList;
  xPac, xComp: Boolean;
  xDB: TAdoDBAccess;
begin
  xPList := TStringList.Create;
  xPList.Duplicates := dupIgnore;
  xPList.Sorted := True;

  xCList := TStringList.Create;
  xCList.Duplicates := dupIgnore;
  xCList.Sorted := True;

  try
    xDB := TAdoDBAccess.Create(1);
  except
    ActiveX.CoInitialize(nil);
    xDB := TAdoDBAccess.Create(1);
  end;

  with xDB do
  try
    HostName := GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost, '');
    if Init then begin
      with Query[0] do
      try
                // Hole MMPackages
        Close;
        SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMPackages'' and AppName = ''MMConfiguration''';
        Open;
                //Recordset.RecordCount
        xPac := (Recordset.RecordCount = 1);
        if xPac then
                  // Vorhandene Packages in Liste schreiben
          xPList.CommaText := FieldByName('Data').asString;

                // Hole MMComponents
        Close;
        SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMComponents'' and AppName = ''MMConfiguration''';
        Open;
        xComp := (Recordset.RecordCount = 1);
        if xComp then
                  // Vorhandene Componenten in Liste schreiben
          xCList.CommaText := FieldByName('Data').asString;

                // entsprechende neue Angaben in beiden StringListen hinzufuegen
        for x := low(TMMPackage) to high(TMMPackage) do begin
          if x in aPackageSet then begin
                    // neues Package zufuegen
            xPList.Add(cMMPackages[x]);
            for n := low(TMMComponent) to high(TMMComponent) do begin
              if n in cMMInstalls[x] then
                        // neue Komponente zufuegen
                xCList.Add(cMMComponents[n]);
            end;
          end; // if x in
        end; // for x

        Close;
        if xPac then
          SQL.Text := Format(cQryUpdate, [xPList.CommaText, 'MMPackages'])
        else
          SQL.Text := Format(cQryInsert, ['MMPackages', xPList.CommaText]);
                //Prepare;
        ExecSQL;

        Close;
        if xComp then
          SQL.Text := Format(cQryUpdate, [xCList.CommaText, 'MMComponents'])
        else
          SQL.Text := Format(cQryInsert, ['MMComponents', xCList.CommaText]);
        ExecSQL;
        Result := True;
      except
        on E: Exception do begin
          CodeSite.SendError('Error in WriteMMPackageToDB(). AdoDBAccess Hostname = ' + HostName + '; Error msg : ' + e.Message);
          Result := False;
        end;
      end;
    end else begin
      CodeSite.SendError('Error in WriteMMPackageToDB(). AdoDBAccess Hostname = ' + HostName + ';TAdoDBAccess.Init Error ');
      Result := False;
    end;
  finally
    Free;
    xPList.Free;
    xCList.Free;
    ActiveX.CoUninitialize;
  end;
end;
//------------------------------------------------------------------------------
function WriteMMPackageToDB(aPackage: TMMPackage): Boolean;
var
  xPack: TMMPackage;
  xComp: TMMComponent;
  xCompList: TStringList;
  xDB: TAdoDBAccess;
begin
  Result := False;

  xCompList := TStringList.Create;
  for xComp := Low(TMMComponent) to High(TMMComponent) do begin
    if xComp in cMMInstalls[aPackage] then
      xCompList.Add(cMMComponents[xComp]);
  end;


  try
    xDB := TAdoDBAccess.Create(1);
  except
    ActiveX.CoInitialize(nil);
    try
      xDB := TAdoDBAccess.Create(1);
    except
      Exit;
    end;
  end;

  with xDB do
  try
    if Init then begin
      with Query[0] do
      try
        // Hole MMPackages
        Close;
        SQL.Text := 'Delete t_MMUParm where AppName = ''MMConfiguration'' and (Appkey = ''MMPackages'' or Appkey = ''MMComponents'')';
        ExecSQL;

        SQL.Text := Format(cQryInsert, ['MMPackages', cMMPackages[aPackage]]);
        ExecSQL;

        SQL.Text := Format(cQryInsert, ['MMComponents', xCompList.CommaText]);
        ExecSQL;

        Result := True;
      except
        on E: Exception do begin
          CodeSite.SendError('Error in WriteMMPackageToDB(): ' + e.Message);
        end;
      end;
    end else begin
      CodeSite.SendError('DBAccess Init failed in in WriteMMPackageToDB: ' + DBErrorTxt);
    end;
  finally
    Free;
    xCompList.Free;
    ActiveX.CoUninitialize;
  end;
{}
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loescht Packages mit den dazugehoerigen Components
//******************************************************************************
function DeleteMMPackage(aDelPackage: TMMPackageSet): Boolean;
var
  xPList: TStringList;
  xInstalledPackages: TMMPackageSet;
  xPackage: TMMPackage;
begin
  Result := True;
  xInstalledPackages := [];

  xPList := TStringList.Create;
  xPList.Duplicates := dupIgnore;
  xPList.Sorted := True;
  ActiveX.CoInitialize(nil);
  with TAdoDBAccess.Create(1, True) do try
    Init;

    Query[0].Close;
    Query[0].SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMPackages'' and AppName = ''MMConfiguration'' ';
    Query[0].Open;
    if Query[0].Recordset.RecordCount = 1 then begin
      //Installierte Packgages
      xPList.CommaText := Query[0].FieldByName('Data').asString;

      //Installierte Packgages in Set umwandeln
      for xPackage := low(TMMPackage) to high(TMMPackage) do
        if xPList.IndexOf(cMMPackages[xPackage]) >= 0 then
          include(xInstalledPackages, xPackage);

      //Uebrig gebliebene Packages
      xInstalledPackages := xInstalledPackages - aDelPackage;
      //Alle Packages loeschen
      if DeleteAllPackages then
        // Packgages installieren
        Result := WriteMMPackageToDB(xInstalledPackages);
    end;
  finally
    Free;
    xPList.Free;
    ActiveX.CoUninitialize;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loescht alle Packages und Components
//******************************************************************************
function DeleteAllPackages: Boolean;
var
  xSQLText: string;
begin
  Result := True;
  ActiveX.CoInitialize(nil);
  with TAdoDBAccess.Create(1, True) do try
    Init;

    xSQLText := 'Delete from t_MMUParm ' +
      'where AppName = ''MMConfiguration'' ' +
      'and (Appkey = ''MMComponents'' or  Appkey = ''MMPackages'')';

    Query[0].Close;
    Query[0].SQL.text := xSQLText;
    try
      Query[0].ExecSQL;
    except
      Result := False;
    end;
  finally
    Free;
    ActiveX.CoUninitialize;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Verschluesselt einen Text
//******************************************************************************
{
function Encode(const aText: string): string;
var
  i: byte;
  xOut: string;
begin
  if aText = '' then begin
    Result := '';
    exit;
  end;

  setlength(xOut, length(aText));
  for i := 1 to length(aText) do begin
    xOut[i] := Char(byte(aText[i]) xor cKey);
  end;
  setlength(xOut, length(aText));
  Result := xOut;
end;
{}
//------------------------------------------------------------------------------

//******************************************************************************
// Entschluesselt einen Text, welcher mit Encode() kodiert wurde
//******************************************************************************
{
function Decode(const aText: string): string;
var
  i: byte;
  xOut: string;
begin

  if aText = '' then begin
    Result := '';
    exit;
  end;

  setlength(xOut, length(aText));
  for i := 1 to length(aText) do begin
    try
      xOut[i] := char(byte(aText[i]) xor cKey);
    except
    end
  end;
  setlength(xOut, length(aText));
  Result := xOut;
end;
{}

//------------------------------------------------------------------------------
// TMMSetupModul
//------------------------------------------------------------------------------
constructor TMMSetupModul.Create(AOwner: TComponent);
var //xApplTxt: string;
  //xAppl   : TApplication;
  xErrorTxt: string;
begin
  inherited Create(AOwner);
  mSettings := nil;

  //  if not RunTime then exit;   // ????

  mApplName := copy(ExtractFileName(Application.ExeName), 0,
    Pos('.', ExtractFileName(Application.ExeName)) - 1);

  mEventLogWriter := TEventLogWriter.Create(cEventLogClassForSubSystems,
    cEventLogServerNameForSubSystems,
    ssApplication, mApplName + ': ', True);

  try
    mSettings := TMMSettings.Create;

    if FUseApplName then
      mSettings.ApplicationName := mApplName
    else
      mSettings.ApplicationName := cApp;

    if not mSettings.Init then begin
      with mEventLogWriter do begin
        xErrorTxt := mSettings.ErrorTxt + #13#10 + cProgTerminate;
        Write(etError, xErrorTxt);
        MessageDLG(xErrorTxt, mtError, [mbOk], 0);
      end;
      MessageBeep(MB_ICONERROR);
      ExitProcess(0);
    end;

    ActiveX.CoInitialize(nil);

    mDB := TAdoDBAccess.Create(2, True);
    mDB.Init;

//    mSettings.RefreshDatapool;
    mSettings.ReadRegistryValues;
    mSettings.ReadDBValues;
  except
    on E: Exception do begin
      xErrorTxt := E.Message + #13#10 + cProgTerminate;
      mEventLogWriter.Write(etError, xErrorTxt);
      MessageDLG(xErrorTxt, mtError, [mbOk], 0);
      Application.Terminate;
    end;

  end;

  {
   mApplName := copy( ExtractFileName(Application.ExeName), 0,
                             Pos('.', ExtractFileName(Application.ExeName))-1  );

   mSettings.ApplicationName:= mApplName;

   if not mSettings.Init then begin
      xAppl :=  TApplication(AOwner) ;
      xApplTxt:= xAppl.name + ': ' ;
      with TEventLogWriter.Create(cEventLogClassForSubSystems,
                                       cEventLogServerNameForSubSystems,
                                       ssApplication, xApplTxt, True) do begin

         Write(etError, cProgTerminate);
         MessageDLG(cProgTerminate, mtError, [mbOk], 0);

      end;
      MessageBeep(MB_ICONERROR);
      ExitProcess(0);
   end;
   mDB:= TAdoDBAccess.Create(2, TRUE);
   mDB.Init;
   }
end;
//------------------------------------------------------------------------------
destructor TMMSetupModul.Destroy;
begin
  mDB.Free;
  mSettings.Free;
  mEventLogWriter.Free;

  inherited Destroy;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Speichert alle Werte aus dem Konst. Array in die Reg.
// aChangedValue = TRUE -> alle Werte die geaendert haben speichern
// aChangedValue = FALSE -> alle Werte die nicht geaendert haben speichern
//******************************************************************************
procedure TMMSetupModul.SaveAllRegValues(aChangedValue: Boolean;
  aPCType: TClientServerType);
var
  x: integer;
  xPath, xRegValueName, xUserValue, {xTable,} xData: string;
  xRootKey: HKEY;
begin

  for x := 0 to mSettings.NumberOfValues - 1 do begin

    // Nur Registry
    if (mSettings.ValueChange[x] = aChangedValue) and
      (mSettings.Location[x] = lReg) and
      (aPCType = mSettings.ClientServer[x]) then begin

      xRootKey := mSettings.LocationKey[x];
      xPath := mSettings.LocationName[x];
      xRegValueName := mSettings.ValueName[x];
      xUserValue := mSettings.UserValue[x];
      xData := mSettings.Data[x];

      // Daten in Reg. schreiben
      if xData <> '' then
        SetRegString(xRootKey, xPath, xRegValueName + 'Data', xData);

      SetRegString(xRootKey, xPath, xRegValueName, xUserValue);

    end;

    // Nur Datenbank
    if (mSettings.ValueChange[x] = aChangedValue) and
      (mSettings.Location[x] = lDB) then begin

      //beep;

    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Setzt die Defaultwerte aus dem Konst.Array in die Registry und Datenbank,
// falls diese noch nicht existieren
//******************************************************************************
procedure TMMSetupModul.MakeEntries(aPCType: TClientServerType);
var
  x: integer;
  xPath, xRegValueName,
    xDefaultValue, xTable,
    xColumnValue, xData: string;
  xRootKey: HKEY;
  xPCType: TClientServerType;
  xErrorTxt, xSQLTxt: string;
  xDBValueFound: Boolean;
begin
  try
    for x := 0 to mSettings.NumberOfValues do begin
      if mSettings.UniqueValueName[x] = 'END' then exit;
      xPCType := mSettings.ClientServer[x];

      // Nur Registry
//      if (mSettings.Location[x] = lReg) and ( mSettings.ClientServer[x] = aPCType) then begin
      if (mSettings.Location[x] = lReg) and
        (((xPCType = csServer) and (NTProduct > 1)) or // GetNTProduct
        ((xPCType = csClient) and (NTProduct = 1)) or // GetNTProduct
        (xPCType = csClientServer)) then begin

        xRootKey := mSettings.LocationKey[x];
        xPath := mSettings.LocationName[x];
        xRegValueName := mSettings.ValueName[x];
        xDefaultValue := mSettings.DefaultValue[x];
        xData := mSettings.Data[x];
        {
                 if ((xPCType = csServer) and  (GetNTProduct > 1)) or
                    ((xPCType = csClient) and  (GetNTProduct = 1)) or
                    ( xPCType = csClientServer) then begin
         }
        if not CheckRegExists(xRootKey, xPath, xRegValueName) then begin
          // xData nur fuer ComboBox
          if xData <> '' then begin
            SetRegString(xRootKey, xPath, xRegValueName + 'Data', xData);
            xDefaultValue := xDefaultValue + cDefaultText;
          end;
          SetRegString(xRootKey, xPath, xRegValueName, xDefaultValue);
        end;

        if mSettings.ComponentType[x] = ctNone then
          mSettings.UserValue[x] :=
            GetRegString(xRootKey, xPath, xRegValueName, xData);
      end;

      // Nur Datenbank
      if mSettings.Location[x] = lDB then begin
        xTable := mSettings.LocationName[x];
        xColumnValue := mSettings.ValueName[x];
        xDefaultValue := mSettings.DefaultValue[x];
        xDBValueFound := False;

        try
          with mDB.Query[1] do begin
            // Auf vorhanden sein ueberpruefen
            Close;
            Params.Clear;
            SQL.Clear;

            //SQL.Add(' Select * from t_MMUParm');

            xSQLTxt := Format(' Select * from t_MMUParm where Appname = ''%s'' ', [mApplName]);
            SQL.Add(xSQLTxt);
            Open;

            //FetchAll;
            // mDB.Query[1].FieldByName('xx').AsString;
            First;
            while not mDB.Query[1].Eof do begin
              if mDB.Query[1].FieldByName('Appkey').AsString = xColumnValue then begin
                xDBValueFound := True;
                break; //Naechster Datensatz von mSettings.UniqueValueName[x]
              end;
              next;
            end;

            if not xDBValueFound then begin
              Close;
              Params.Clear;
              SQL.Clear;
              SQL.Add('INSERT INTO t_MMUParm ');
              SQL.Add(' (Appname, Appkey, Data) ');
              SQL.Add(' VALUES(:Appname, :Appkey, :Data ) ');

              Params.ParamByName('Appname').asString := mApplName;
              Params.ParamByName('Appkey').asString := xColumnValue;
              Params.ParamByName('Data').asString := xDefaultValue;
              ExecSQL;
            end;
          end; // end with
        except

        end; // end try inner
      end;
    end;

  except
    on E: Exception do begin
      xErrorTxt := 'Error in MakeEntries. ' + E.Message;
      raise exception.Create(xErrorTxt);
    end;
  end; // end try outer
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Kopiert die Reg.Eintraege ins UserValue des Konst.Arrays
//******************************************************************************
procedure TMMSetupModul.WriteRegToUserValue;
var
  x: integer;
  xPath, xRegValueName, {xTable,} xData: string;
  xRootKey: HKEY;
  xLocation: TLocation;
  xErrorTxt: string;
begin
  try
    x := 0;
    repeat
      xLocation := SettingsReader.cMMSetupDefaults[x].Location;
      // Nur Registry-Eintrag
      if xLocation = lReg then begin
        xRootKey := cMMSetupDefaults[x].LocationKey;
        xPath := cMMSetupDefaults[x].LocationName;
        xRegValueName := cMMSetupDefaults[x].ValueName;
        cMMSetupDefaults[x].UserValue :=
          GetRegString(xRootKey, xPath, xRegValueName, xData);

      end;
      // Nur Datenbank-Eintrag ->
      if xLocation = lDB then begin
        cMMSetupDefaults[x].UserValue := mSettings.DBValue[x];
      end;

      inc(x);
    until x = GetNumberOfValues;

  except
    on E: Exception do begin
      xErrorTxt := 'Error in WriteRegToUserValue. ' + E.Message;
      raise exception.Create(xErrorTxt);
    end;
  end;
end;
//------------------------------------------------------------------------------
{
function TMMSetupModul.GetDBValue(aId: Word): Variant;
begin
  //mSettings.
  beep;
end;
}

{
procedure TMMSetupModul.SetDBValue(aId: Word; const Value: Variant);
begin
  inherited;
 beep;
end;
}
{
function TMMSetupModul.Init: Boolean;
begin
 result := mDB.Init;
 if result then result:= mSettings.Init;
 fDBConnected := result;
end;
}
//------------------------------------------------------------------------------
procedure TMMSetupModul.Loaded;
//var xErrorTxt: string;
begin
  inherited;

  {
   try
     mSettings := TMMSettings.Create;
     mSettings.ApplicationName:= mApplName;

     if not mSettings.Init then begin
        with mEventLogWriter do begin
           xErrorTxt := mSettings.ErrorTxt + #13#10 + cProgTerminate;
           Write(etError, xErrorTxt);
           MessageDLG(xErrorTxt, mtError, [mbOk], 0);
        end;
        MessageBeep(MB_ICONERROR);
        ExitProcess(0);
     end;

     mDB:= TAdoDBAccess.Create(2, TRUE);
     mDB.Init;

     mSettings.RefreshDatapool;
   except
     on E: Exception do begin
         xErrorTxt := E.Message + #13#10 + cProgTerminate;
         mEventLogWriter.Write(etError, xErrorTxt);
         MessageDLG(xErrorTxt, mtError, [mbOk], 0);
         Application.Terminate;
     end;

   end;
   }
end;
//------------------------------------------------------------------------------
function TMMSetupModul.GetUseApplName: Boolean;
begin
  Result := FUseApplName;
end;
//------------------------------------------------------------------------------
procedure TMMSetupModul.SetUseApplName(const Value: Boolean);
begin
  FUseApplName := Value;
  if not FUseApplName then
    mSettings.ApplicationName := cApp
  else
    mSettings.ApplicationName := mApplName;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TMMSettings
//------------------------------------------------------------------------------
constructor TMMSettings.Create;
var
  xErrorTxt: string;
begin
  inherited Create;
  try
    ActiveX.CoInitialize(nil);
    mDataBase := TAdoDBAccess.Create(1, True);
  except
    on E: Exception do begin
      xErrorTxt := 'Error in TAdoDBAccess. ' + E.Message + '  ' + IntToStr(mDataBase.DBError);
      mDataBase.Free;
      raise exception.Create(xErrorTxt);
    end
  end;
end;
//------------------------------------------------------------------------------
destructor TMMSettings.Destroy;
begin
  if mDataBase <> nil then mDataBase.free;
   ActiveX.CoUninitialize;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt alle MMComponenten
//******************************************************************************
procedure TMMSettings.FetchInstalledComponents;
var
  xCList: TStringList;
  xComp: TMMComponent;
begin

  FIsComponentQOfflimit := False;
  FIsComponentStyle := False;
//  FIsComponentDataWareHouse := False;
  FIsComponentLongTerm := False;
  FIsComponentClearerAssistant := False;
  FIsComponentLabReport := False;

  FComponentsAsText := '';
  // vorhandene Components auf DB ermitteln
  //if not active then

  xCList := TStringList.Create;
    //  xCList:=  TStringList.Create;
  with mDataBase do try
    Query[0].Close;
    Query[0].SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMComponents'' and AppName = ''MMConfiguration''';
    Query[0].Open;

 //   if Query[0].Recordset.RecordCount = 1 then begin
    if Query[0].FindFirst then begin
      xCList.CommaText := Query[0].FieldByName('Data').asString;
      FComponentsAsText := xCList.CommaText;

      for xComp := low(TMMComponent) to high(TMMComponent) do begin
        if xCList.IndexOf(cMMComponents[xComp]) >= 0 then
          case xComp of
            mmcQOfflimit: FIsComponentQOfflimit := True;
            mmcStyle: FIsComponentStyle := True;
//            mmcDatawarehouse: FIsComponentDataWareHouse := True;
            mmcLongTerm: FIsComponentLongTerm := True;
            mmcClearerAssistant: FIsComponentClearerAssistant := True;
            mmcLabReport: FIsComponentLabReport := True;
          end;
      end;
    end;
  finally
    xCList.Free;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt alle MMPackages
//******************************************************************************
procedure TMMSettings.FetchInstalledPackages;
var
  xPList: TStringList;
  xPack: TMMPackage;
begin
  FIsPackageEasy := False;
  FIsPackagePro := False;
//  FIsPackageClearerAssistant := False;
  FIsPackageStandard := False;
  FPackagesAsText := '';

  // vorhandene Packages auf DB ermitteln
  xPList := TStringList.Create;
  with mDataBase do try
    Query[0].Close;
    Query[0].SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMPackages'' and AppName = ''MMConfiguration''';
    Query[0].Open;
    //if Query[0].Recordset.RecordCount = 1 then begin
    if Query[0].FindFirst then begin
      xPList.CommaText := Query[0].FieldByName('Data').asString;
      FPackagesAsText := xPList.CommaText;

      for xPack := low(TMMPackage) to high(TMMPackage) do begin
        if xPList.IndexOf(cMMPackages[xPack]) >= 0 then
          case xPack of
            mmpEasy: FIsPackageEasy := True;
            mmpStandard: FIsPackageStandard := True;
            mmpPro: FIsPackagePro := True;
//            mmpClearerAssistant: FIsPackageClearerAssistant := True;
          end;
      end;
    end;
  finally
    xPList.Free;
  end;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetClientServer(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ClientServer;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetComponentType(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ComponentType;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetData(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Data;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetDBValue(aId: Word): Variant;
var
  xTable, xColumn: string;
begin
  xTable := cMMSetupDefaults[aId].LocationName;
  xColumn := cMMSetupDefaults[aId].ValueName;

  // set DefaultValue to return parameter in case value on database is not available
  Result := cMMSetupDefaults[aId].DefaultValue;
  with mDataBase.Query[0] do try
    Close;
    if ApplicationName <> '' then
      SQl.Text := Format('Select Data from %s where Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xColumn, ApplicationName])
    else
      SQl.Text := Format('Select Data from %s where Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xColumn, cApp]);
    Open;
    if not EOF then
      Result := FieldByName('Data').AsString
  except
  end;

end;
//------------------------------------------------------------------------------
function TMMSettings.GetDefaultValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].DefaultValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetDescription(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Description;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetFactor(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Factor;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetGroup(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Group;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetHintText(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].HintText;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetID(aRegUniqueName: string): Integer;
var
  x: Integer;
begin
  Result := -1;
  for x := 0 to GetNumberOfValues - 1 do
    if cMMSetupDefaults[x].UniqueValueName = aRegUniqueName then begin
      Result := x;
      break;
    end;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetLocation(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Location;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetLocationKey(aId: Word): Variant;
begin
  Result := Variant(cMMSetupDefaults[aId].LocationKey);
end;
//------------------------------------------------------------------------------
function TMMSettings.GetLocationName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].LocationName;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetMask(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Mask;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetMaxValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].MaxValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetMinValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].MinValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetNativeValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].NativeValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetOnlyTemplateSetsAvailable: Boolean;
begin
  Result := GetUserValue(GetID(cOnlyTemplateSetsAvailable)); // Pos. aus SetupMMDefaults
end;
//------------------------------------------------------------------------------
function TMMSettings.GetNumberOfValues: Word;
begin
  Result := Word(High(cMMSetupDefaults)) - Word(Low(cMMSetupDefaults)) + 1;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetPrintable(aId: Word): Boolean;
begin
  Result := cMMSetupDefaults[aId].Printable;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetReadOnlyData(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ReadOnlyData;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetRestart(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Restart;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetUniqueValueName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].UniqueValueName;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetUserValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].UserValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetUserValueByName(aUniqueName: string): Variant;
begin
  Result := cMMSetupDefaults[GetID(aUniqueName)].UserValue;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetValueChange(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueChange;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetValueName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueName;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetValueText(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueText;
end;
//------------------------------------------------------------------------------
function TMMSettings.GetValueType(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueType;
end;
//------------------------------------------------------------------------------
function TMMSettings.Init: Boolean;
begin
  Result := False;
  fErrorTxt := '';
  if not Initialized then
  try
    ReadRegistryValues;
    if Assigned(mDataBase) then
    try
      if mDataBase.Init then begin
        try
          ReadDBValues;
        except
          on e: Exception do begin
            fErrorTxt := 'ReadDBValues failed';
            Exit;
          end;
        end;
        inherited Init;
      end else
        fErrorTxt := mDataBase.DBErrorTxt;
    except
      fErrorTxt := mDataBase.DBErrorTxt;
    end;
  except
    on e: Exception do
      fErrorTxt := 'TMMSettings.Init: ' + e.Message;
  end;
  Result := Initialized;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.ReadRegistryValues;
var
  i: Integer;
  xCount: Integer;

begin
  xCount := GetNumberOfValues;
  with TmmRegistry.Create do try
    for i := 0 to xCount - 1 do try
      with SettingsReader.cMMSetupDefaults[i] do begin
        if Location = lReg then begin
          // set to default value if UserValue not exists in registry
          UserValue := DefaultValue;
          RootKey := LocationKey;
          if OpenKeyReadOnly(LocationName) then
            if ValueExists(ValueName) then
              // if value exists in registry override UserValue
              UserValue := ReadString(ValueName);
        end;
      end;
    except
      on e: Exception do
        raise exception.Create('Error in ReadRegistryValues: ' + e.Message);
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.ReadDBValues;
var
  xCount: Integer;
  i: integer;
begin

  xCount := GetNumberOfValues;

  for i := 0 to xCount - 1 do try

    with SettingsReader.cMMSetupDefaults[i] do begin
      if Location = lDB then begin
        UserValue := Trim(GetDBValue(i));
      end;
    end;
  except
    on e: Exception do
      raise exception.Create('Error in ReadDBValues: ' + e.Message);
  end;
  FetchInstalledComponents;
  FetchInstalledPackages;

end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetData(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].Data := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetDBValue(aId: Word; const Value: Variant);
var
  xTable, xColumn, xSQLtext, xUserValue: string;
begin
  try
    xTable := cMMSetupDefaults[aId].LocationName;
    xColumn := cMMSetupDefaults[aId].ValueName;
    xUserValue := Value;

    if ApplicationName <> '' then
      xSQLtext := Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xUserValue, xColumn, ApplicationName])
    else
      xSQLtext := Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xUserValue, xColumn, cApp]);

    //   xSQLtext:= Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and AppName = ''%s''', [xTable, xUserValue, xColumn]);

    mDataBase.Query[0].Close;
    mDataBase.Query[0].SQL.Clear;
    mDataBase.Query[0].Params.Clear;
    mDataBase.Query[0].SQL.Add(xSQLtext);
    mDataBase.Query[0].ExecSQL;
  except
    on E: Exception do begin
      //Result:= xDefault;
      // Cerate New DB-Data

      //MessageDlg(e.Message + '[TMMSettings.GetDBValue]' , mtWarning, [mbOk], 0);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetGroup(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].Group := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetHintText(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].HintText := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetNativeValue(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].NativeValue := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetPrintable(aId: Word; const Value: Boolean);
begin
  cMMSetupDefaults[aId].Printable := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetUserValue(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].UserValue := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetValueChange(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].ValueChange := Value;
end;
//------------------------------------------------------------------------------
procedure TMMSettings.SetValueText(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].ValueText := Value;
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TMMSettingsReader
//------------------------------------------------------------------------------
{
constructor TMMSettingsReader.Create;
begin
  inherited Create;
  mSettings := TmmSettings.Create;
end;
//------------------------------------------------------------------------------
destructor TMMSettingsReader.Destroy;
begin
  FreeAndNil(mSettings);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetComponentStyle: Boolean;
begin
  Init;
  Result := mSettings.IsComponentStyle;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetComponentClearerAssistant: Boolean;
begin
  Init;
  Result := mSettings.IsComponentClearerAssistant;
end;
//------------------------------------------------------------------------------
//function TMMSettingsReader.GetComponentDataWareHouse: Boolean;
//begin
//  Init;
//  Result := mSettings.IsComponentDataWareHouse;
//end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetComponentLabReport: Boolean;
begin
  Init;
  Result := mSettings.IsComponentLabReport;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetComponentLongTerm: Boolean;
begin
  Init;
  Result := mSettings.IsComponentLongTerm;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetComponentsAsText: string;
begin
  Init;
  Result := mSettings.ComponentsAsText;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetIsComponentQOfflimit: Boolean;
begin
  Init;
  Result := mSettings.IsComponentQOfflimit;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetOnlyTemplateSetsAvailable: Boolean;
begin
  Init;
  Result := mSettings.IsOnlyTemplateSetsAvailable;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetPackagePro: Boolean;
begin
  Init;
  Result := mSettings.IsPackagePro;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetPackageStandard: Boolean;
begin
  Init;
  Result := mSettings.IsPackageStandard;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetPackagesAsText: string;
begin
  Init;
  Result := mSettings.PackagesAsText;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetPackageEasy: Boolean;
begin
  Init;
  Result := mSettings.IsPackageEasy;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.GetValue(aUniqueName: string): Variant;
var
  xNr: Integer;
begin
  // it just checks if the object is initialized. If not it calls the Init method
  inherited GetValue(aUniqueName);
  try
    xNr := mSettings.GetID(aUniqueName);

    //Default Value
    if xNr >= 0 then
      Result := mSettings.UserValue[xNr]
    else begin
//      Result := '0'; // do not return an empty string '', it will raise an exception if converted to number
      // Handelt es sich um Processe ?
      if Pos(cProcess, aUniqueName) > 0 then begin
        // Processe aus Reg. lesen
        xNr := mSettings.GetID(cNumOfProcess);
        // Reg.Pfad ermitteln
        with mSettings do begin
          Result := GetRegString(LocationKey[xNr], LocationName[xNr], aUniqueName);
          if Result = '' then begin
            Result := '0'; // do not return an empty string '', it will raise an exception if converted to number
            fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'No registry entry exist.');
            raise exception.Create(fError.Msg);
          end;
        end; // with mSettings
      end; // if Pos(
    end; // if xNr
  except
    on E: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'Error in GetUserValue. ' + E.Message);
      Result := '';
      raise exception.Create(fError.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMMSettingsReader.Init: Boolean;
begin
  with mSettings do begin
    Result := Init;
    if ErrorTxt <> '' then begin
      WriteToEventLog(ErrorTxt, 'TMMSettingsReader.Init: ', etError);
    end;
  end;
end;
{}
//------------------------------------------------------------------------------

initialization

//ActiveX.CoInitialize(nil); //SDO

{wss: removed because Hostlink has problems if the TmmSettings object is created module global
  lSettings := TmmSettings.Create;
{}
finalization
{wss: removed because Hostlink has problems if the TmmSettings object is created module global
  FreeAndNil(lSettings);
{}
end.

