{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintSettingsForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Druckerauswahl-Dialog und PreView & Print
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.01.2002  1.00  Nue | File created
| 17.07.2003  1.00  Wss | TBitBtn mit normalen TButton ersetzt
| 04.06.2004  1.00  SDo | Anpassung: Aenderung am TFramePrintSetup
| 22.02.2005  1.01  SDo | Neue P-Chekbox eingebaut
|                       | -> momentan nur im Assigments sichtbar
|                       | Sichtbar/Unsichtbar mittels Property ShowPCheckbox schalten
| 15.06.2005  1.00  Wss | "P-Reinigung" nach "P Einstellungen" umtaufen
| 10.01.2008  1.02  Nue | VCV eingefügt.
===============================================================================}
unit PrintSettingsTemplateForm;

interface

uses
  Windows, SysUtils, Messages, Classes, Graphics, Controls,
  StdCtrls, ExtCtrls, BASEFORM, Forms, mmLabel, mmComboBox, Buttons, mmBitBtn,
  mmGroupBox, NumCtrl, mmRadioGroup, mmRadioButton, mmEdit, IvDictio,
  IvMulti, IvEMulti, mmTranslator, Mask, mmExtStringGrid,
//  ClearerAssistantDef,
  QRPrntr, mmButton, PrintSetupFrame, CheckLst,
  mmCheckListBox, mmCheckBox {, ClearerAssistantDef} ;

type
////Nue 11.12.01 for PrintReport added
//  TReportInfoRec = record
//    Style: String;
//    Lot:   String;
//    Machine: String;
//    Template: String;
//    YarnCount: Integer;
////    YarnUnit: TYarnUnits;
//  end;


  TClearerSettingsRec = record
    ShowChannel: Boolean;
    ShowSplice: Boolean;
    ShowYarnCount: Boolean;
    ShowCluster: Boolean;
    ShowSFI: Boolean;
    ShowVCV: Boolean;
    ShowFFCluster: Boolean;
    ShowP : Boolean;
    PrintBlackWhite: Boolean;
  end;


  {:----------------------------------------------------------------------------
   Print settings dialog for configure the printout.
   ----------------------------------------------------------------------------}
  TfrmPrintSettings = class (TmmForm)
    cbChannel: TmmCheckBox;
    cbCluster: TmmCheckBox;
    cbFFCluster: TmmCheckBox;
    cbSFI: TmmCheckBox;
    cbSplice: TmmCheckBox;
    cbYarnCount: TmmCheckBox;
    gbClearerSettings: TmmGroupBox;
    mmTranslator: TmmTranslator;
    mPrintSetup: TFramePrintSetup;
    bCancel: TmmButton;
    bPrint: TmmButton;
    cbP: TmmCheckBox;
    cbVCV: TmmCheckBox;
    procedure bPrintClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);

  private
    mClearerSettingsRec : TClearerSettingsRec;
    fXMLData: String;
    fShowPCheckbox: Boolean;
    procedure SetShowClearerSettings(const Value: Boolean);
    function GetClearerSettingsRec: TClearerSettingsRec;
    procedure SetClearerSettingsRec(const Value: TClearerSettingsRec);
    procedure SetShowPCheckbox(const Value: Boolean);
  public
    property ShowClearerSettings : Boolean Write SetShowClearerSettings;
    property PrinterSet : TFramePrintSetup read mPrintSetup Write mPrintSetup;
    property ShowClearerSettingsSet : TClearerSettingsRec read GetClearerSettingsRec Write SetClearerSettingsRec;
    property ShowPCheckbox : Boolean read  fShowPCheckbox Write SetShowPCheckbox;
    //property XMLData : String : write  fXMLData read fXMLData;

//    TFramePrintSetup
  end;


var
  frmPrintSettings: TfrmPrintSettings;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  Printers, WinSpool, PrintTemplateForm;


{:------------------------------------------------------------------------------
 TfrmPrintSettings}
{:-----------------------------------------------------------------------------}

{:-----------------------------------------------------------------------------}
//function TfrmPrintSettings.GetSelectedSeries: string;
//var
//  i: Integer;
//begin
//  Result := '';
//  with TStringList.Create do
//  try
//    with clbSeries do
//      for i:=0 to Items.Count-1 do begin
//        if Checked[i] then
//          Add(IntToStr(i+1));
//      end;
//
//    Result := CommaText;
//  finally
//    Free;
//  end;
//end;
//
{:-----------------------------------------------------------------------------}
{:-----------------------------------------------------------------------------}

{:-----------------------------------------------------------------------------}

{:-----------------------------------------------------------------------------}


{:-----------------------------------------------------------------------------}
function TfrmPrintSettings.GetClearerSettingsRec: TClearerSettingsRec;
begin
  Result := mClearerSettingsRec;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetClearerSettingsRec(const Value: TClearerSettingsRec);
begin
  mClearerSettingsRec := Value;

  with mClearerSettingsRec do begin
    cbChannel.Checked   := ShowChannel;
    cbSplice.Checked    := ShowSplice;
    cbYarnCount.Checked := ShowYarnCount;
    cbCluster.Checked   := ShowCluster;
    cbSFI.Checked       := ShowSFI;
    cbVCV.Checked       := ShowSFI;
    cbFFCluster.Checked := ShowFFCluster;
    cbP.Checked         := ShowP;
    mPrintSetup.PrintBlackWhite := PrintBlackWhite;
  end;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetShowClearerSettings(const Value: Boolean);
begin
  gbClearerSettings.Visible := Value;
  if not Value then
     Height  := 200
  else
//     Height := 285;
     Height := 305;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.bPrintClick(Sender: TObject);
begin
  inherited;

  with mClearerSettingsRec do begin
    ShowChannel   := cbChannel.Checked;
    ShowSplice    := cbSplice.Checked;
    ShowYarnCount := cbYarnCount.Checked and cbYarnCount.Visible;
    ShowCluster   := cbCluster.Checked;
    ShowSFI       := cbSFI.Checked;
    ShowVCV       := cbVCV.Checked;
    ShowFFCluster := cbFFCluster.Checked;
    ShowP         := cbP.Checked;
    PrintBlackWhite := mPrintSetup.PrintBlackWhite;
  end;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.SetShowPCheckbox(const Value: Boolean);
begin
  fShowPCheckbox := Value;
  cbP.Visible := fShowPCheckbox;

  if not fShowPCheckbox then begin
     //ohne P-Checkbox
     gbClearerSettings.Height := 85;
     Height :=  285;
  end else begin
     //mit P-Checkbox
     gbClearerSettings.Height :=  105;
     Height :=  305;
  end;
end;
{:-----------------------------------------------------------------------------}
procedure TfrmPrintSettings.FormCreate(Sender: TObject);
begin
  inherited;
 //Nue:11.03.08
 //  ShowPCheckbox  := FALSE;
 mPrintSetup.rbLandscape.Visible := False;   //Nue: 02.07.08 Solange die anordnung der Printkomponenten nicht stimmt
                                             // überarbeitung der Routine TfrmPrint.mQuickReportBeforePrint im Modul PrintTemplateForm
end;
{:-----------------------------------------------------------------------------}
end.

