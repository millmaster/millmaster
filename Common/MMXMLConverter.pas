{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMXMLConverter.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains classes for converting YM-Settings and Maschineconfigurations
|                 from a binary stream to a MMXML stream and vis versa.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.24
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.06.2004  1.00  Nue | File created
| 30.09.2004  1.01  Nue | Modifikations: Facets always describe XML side, Formula always describe XML->BIN
|                       | Enumeration (Switch and Value handling opn the same memory position.
| 23.11.2004  1.02  Nue | Hashcode methodes programmed.
| 15.12.2004  1.03  Lok | ConfigCode attribute ins MMXML kopieren (HandleElementAttributeNodesBinToXML())
| 25.01.2005  1.04  Nue | CommentNodes modified; Hashcode handling modified and properties Hashcode1, Hashcode2, MapSection added.
| 03.02.2005        Nue | Bug fixes for Internal events. (Overwriting memory ranges)
| 10.02.2005        Nue | Erweitern EventRecord mit WriteElementToBin; Handle Overwrite Flag in CheckBitfield
| 29.03.2005  1.05  Lok | Warnings sammeln und am Ende der Konvertierung gesammelt ausgeben
|                       |   Tracking f�r einzelne Elemente �ber die Registry ein- und ausschalten:
|                       |      - XMLConverterElementTracking  : Ein- und ausschalten des Trackings
|                       |      - XMLConverterTrackingElements : Kommaseparierte Liste mit den zu beobachtenden Elementen (XPath)
|                       |   Property IgnoreDeleteFlag eingef�hrt. Es werden nach der Konvertierung KEINE Nodes aus dem XML gel�scht
|
| 14.04.2005  1.05  Wss | Warnings werden nach dem ausgeben gel�scht
| 14.04.2005  1.06  Lok | Alle Spuren vom Hash Code entfernt
|                       |   Zus�tzliche Processing Informations eingef�gt (Version und verwendetes Mapfile)
| 02.06.2005  1.07  Lok | Fehler bei den Enumeratinen (XML --> Bin). Es wurde als Defaultwert immer 0 eingetragen. Auch
|                       |   wenn ein Defaultwert als Enumeration definiert war. Neu wird der Defaultwert anhand der
|                       |   definierten Enumeration aufgel�st und der entsprechende WErt eingetragen.
| 21.07.2005  1.08  Lok | Enumeration im Defaults XML als Attribut einf�gen.
| 12.08.2005        Lok | In GetValueFromBin() den Datentyp UCChar ge�ndert (ergab immer 'C')
| 16.08.2005        Lok | ProcessEvent() ge�ndert.
|                       |   Wenn ein Element nicht im MMXML vorhanden ist, dann muss ein definierter XML Event
|                       |   trotzdem ausgel�st werden. Da aber das XML Element nicht vorhanden ist, k�nnen die evt.
|                       |   definierten "Event-Elemente" nicht per XPath relativ zum Element selektiert werden (Element = nil).
|                       |   Deshalb wird in diesem speziellen Fall der XPath auf das Dokument angewendet.
| 27.09.2005  1.09  Lok | Fehler in HandleElementAttributeNodesBinToXML(). Wenn Datentyp dtIgnore wurde das Element behandelt als ob das
|                       |   BinFile = nil w�re (Defaults). Das hatte zur Folge, dass die Attribute in das Element kopiert wurde
| 06.10.2005  1.10  Lok | F�r die Konvertierung Datentyp UTF16 eingef�hrt (dtUTF16).
| 12.10.2005  1.11  Lok | Bei der LZE sind die einzelnen ConfigCodes zB. Bytes und smallints. Deshalb wurden im Konverter
|                       |   Die Werte 'True' und 'False' nicht in die entsprechenden Zahlen �bersetzt. Dies wird nun nachgeholt.
|                       |   Alternativ kann im Mapfile das Element mit der XMLBoolean Enummeration definiert werden.
| 02.11.2005  1.12  Nue | Neue lokale Prozedur SetEnumXMLBin in CheckEnum f�r Rekursion bei fehlerhaften enum-Werten
|                       | und Errormeldung bei falschem Defaultwert.
|                   Lok | Thread eingef�hrt, der die tempor�ren Konverterdateien l�scht, wenn sie mehrere Tage alt sind
|                       |   alt sind (Default = 30. �bersteuerbar mit 'DayCountToDeleteBinaryFiles' in der Registry)
| 11.09.2006  1.13  Nue | In ProcessEventNodeList Defaultwert erneut holen und dem Event mitgeben, (7.9.06 Nue)
|                       | wenn kein MMXML-Node vorhanden ist.
| 02.04.2007  1.14  Nue | SaveBinFile added in XmlToBin wenn der bin�r Inputbuffer nicht leer ist.
| 12.04.2007  1.15  Nue | Aenderung in WriteValueToBin und CheckBitfield wegen ConfigCode-Verschmelzung.
| 25.05.2008  1.16  Nue | Bugfix f�r Aenderung 1.15: Swap am richtigen Ort!!!!! in WriteValueToBin.
|                       |
| !!!!!!!!!!! ACHTUNG !!!!!!!!!!! Nicht vergessen cVersion mit der aktuellen Version zu aktualisieren !!!!!!!!!!!!!!!!!!
|==========================================================================================}
//ACHTUNG!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//Diese Unit nur �bers ModelMaker Projekt �ndern!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
(*:********************************************************************
 *  Modulname:        MMXMLConverter
 *  Kurzbeschreibung: 
 *
 *  Beschreibung:
    Mit dem setzen des Registrykey "GetRegBoolean(cRegLM, cRegMMDebug, 'MMXMLConverter', False);" kann veranlasst
    werden, dass ALLE Debugmeldungen des Konverters ausgegeben werden in CodeSite.
 ************************************************************************)
unit MMXMLConverter;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, MSXML2_TLB, LoepfeGlobal, XMLMappingDef, mmStringList,
  XMLGlobal, Math, MMEventLog, mmThread;

const
  cVersion = '1.16';
  cNrOfEvents = 2;
  cTrueFalse  : Array [Boolean] of string = ('false','true');

  cRegDebugTrackingEnabled = 'XMLConverterElementTracking';
  cRegDebugTrackingElements = 'XMLConverterTrackingElements';
  cDayCountToDeleteBinaryFiles = 'DayCountToDeleteBinaryFiles';

type
  EMMXMLConverter = class(Exception)
  end;

  TElementEventRec = record
    ArgumentList: TmmStringList;
    ArgumentsString: string;
    CalcElementName: string;
    Value: Variant;
    WriteElementToBin: Boolean;
  end;

  (*: Klasse:        TDeleteOldTempFiles
      Vorg�nger:     TMMThread
      Kategorie:     No category
      Kurzbeschrieb: L�scht alle *.tmp Files in einem Verzeichnis die �lter als eine bestimmte Anzahl Tage sind 
      Beschreibung:  
                     L�scht die alten Files die vor und nach dem Konvertieren geschrieben werden (Debug Einstellung)
                     Die Files werden asynchron gel�scht. Der Thread gibt sich selber frei und muss nicht freigegeben
                     werden.
                     Es werden nur die Dateien mit der Endung 'tmp' gel�scht. *)
  TDeleteOldTempFiles = class(TMMThread)
  private
    fDayCount: Integer;
    fDirName: string;
  public
    //1 Konstruktor 
    constructor Create(aDayCount: integer; aDirName: string);
    //1 L�scht alle tmp Dateien im Konverter Temp Verzeichnis 
    procedure Execute; override;
  end;

  (*: Klasse:        TMyMapMMXMLList
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Verwaltung der Element mit Events 
      Beschreibung:  
                     Die Klasse gew�hrleistet, dass zwei Listen (die erste mit den MAP-File Nodes 
                     und die andere mit den MMXML Nodes) synchronisiert und verwaltet werden. *)
  TMyMapMMXMLList = class(TObject)
  private
    mMapList: TInterfaceList;
    mMMXMLList: TInterfaceList;
    function GetCount: Integer;
    function GetMapItems(Index: Integer): IXMLDOMNode;
    function GetMMXMLItems(Index: Integer): IXMLDOMNode;
  public
    constructor Create;
    destructor Destroy; override;
    procedure Add(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
    property Count: Integer read GetCount;
    property MapItems[Index: Integer]: IXMLDOMNode read GetMapItems;
    property MMXMLItems[Index: Integer]: IXMLDOMNode read GetMMXMLItems;
  end;

  TCalcElement = procedure (Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean) of object;
  TBeforeAfterEvent = procedure (Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40) of
    object;
  TMMXMLConverter = class(TObject)
  private
    FIgnoreDeleteFlag: Boolean;
    FMapSection: TMapSection;
    FOnAfterDeleteElements: TBeforeAfterEvent;
    FOnBeforeConvert: TBeforeAfterEvent;
    FOnBeforeDeleteElements: TBeforeAfterEvent;
    FOnCalcExternal: TCalcElement;
    FOnCalcInternal: TCalcElement;
    FVersion: string;
    mActElementType: TDataType;
    mBinDoc: Pointer;
    mBinLen: Integer;
    mChangeEndian: Boolean;
    mCreateBufferForXMLToBin: Boolean;
    mDebugOutEnabled: Boolean;
    mDebugTracking: Boolean;
    mDebugTrackingElements: TStringList;
    mDelEventList: TMyMapMMXMLList;
    mEventList: TMyMapMMXMLList;
    mInternalEventLogHandle: Boolean;
    mIntEventLog: TEventLogWriter;
    mMapSectionElement: IXMLDOMElement;
    mMMXML: DOMDocument40;
    mMsgList: TmmStringList;
    mTrackCurrentElement: Boolean;
    mUp: Boolean;
    mValidate: Boolean;
    mWarnings: TStringList;
    mXMLMap: DOMDocument40;
    mXMLSchemaStream: string;
    //1 Behandelt die Attribute 'bitOffset' und 'bitCount' in einem Element Knoten 
    function CheckBitfield(aMapNode: IXMLDOMNode; var aValue: Variant; var aOverwrite: Boolean; out aBitCount,
      aBitOffset: Integer): Boolean;
    //1 Behandelt die Convert-Attribute in einem Element Knoten 
    procedure CheckConvert(aMapNode: IXMLDOMNode; var aValue: Variant);
    //1 Behandelt die enum-Attribute in einem Element Knoten 
    procedure CheckEnum(aMapNode, aMMXMLNode: IXMLDOMNode; var aValue: Variant);
    //1 Behandelt die Facets eines XML Elements 
    procedure CheckFacets(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; var aValue: Variant);
    procedure CheckMMXMLPlausability(aValue: Variant; aDataType: TDataType; out aLast: Boolean);
    //1 Die verschiedenen Attribute eines XML-Map Elements werden gecheckt. 
    procedure CheckSequenceBinToXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; var aValue: Variant);
    //1 Setzt einen Boolean Zahlenwert (0/1) in einen String (false/true) um. 
    procedure ConvertBoolToString(var aValue: Variant);
    //1 Die Eventliste wird durchgegangen und Nodes, welche mit 'Delete' markiert sind, werden gel�scht 
    procedure DelEventListNodes;
    //1 F�hrt ein Element, welches Event(s) ausf�hren muss, einer Liste zu. 
    function EventsExists(aMapNode, aMMXMLNode: IXMLDOMNode): Boolean;
    //1 Holt den Defaultwert des �bergebenen XML-Elements 
    function GetDefaultFromMap(aMapNode: IXMLDOMNode; out aValue: Variant): Boolean;
    //1 REKURSIV!! Gibt den vollst�ndigen X-Path des �bergebenen Elements ab Rootelement zur�ck. 
    function GetElementFullXPath(aNode: IXMLDOMNode; aWithMapSection: Boolean=False): string;
    function GetNewTempFileName(aPrefix: string): string;
    //1 Wandelt den Typ aus einem XML-String ins entsprechende Element im TBasicType um 
    function GetTypeFromString(aNode: IXMLDOMElement): TDataType;
    //1 Liest einen Wert(Variant) aus dem Bin�rfile 
    function GetValueFromBin(aMapNode: IXMLDOMNode; out aLast: Boolean): Variant;
    //1 Liest einen Wert(Variant) aus dem MMXML 
    function GetValueFromMMXML(aMapNode, aMMXMLParentNode: IXMLDOMNode; out aMMXMLNode: IXMLDOMNode; out aValue:
      Variant; out aLast: Boolean): Boolean;
    function GetXMLDeclaration: string;
    //1 Behandelt die Attribute in einem Element Knoten (Bin->XML) 
    procedure HandleElementAttributeNodesBinToXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
    //1 F�hrt ein neues Element mit Wert im MMXML ein. (XML->Bin) 
    procedure HandleElementAttributeNodesXMLToBin(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
    //1 Initialisiert ein XML-Element vom Typ dtUCBit 
    procedure InitucBitNode(aMapNode: IXMLDOMNode; out aCutFlag, aUnCutFlag: Char; out aBitCount, aRestBitCount:
      Integer; out aByteCount: Cardinal);
    //1 L�dt das Map-File in das XMLMap Interface ein 
    function LoadXMLMap(const aXMLMap: string): Boolean;
    //1 Geht durch alle Nodes des XML-Map-Files und f�llt die Werte aus dem Bin-File ins Output-MMXML 
    function MapBinToXML: Boolean;
    //1 REKURSIV! Mappt pro Aufruf einen Level des MapFiles 
    procedure MapLevelXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; aRootCall:Boolean=False);
    //1 Geht durch alle Nodes des XML-Map-Files und f�llt die Werte aus dem Input-MMXML ins Output Bin-File 
    function MapXMLToBin(const aMapSection: TMapSection; out aBinDoc: PByte; out aBinDocLen: Integer): Boolean;
    //1 Abhandlen allf�lliger interner Events Bin->XML 
    procedure NewInternalBinToXMLEvent(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    //1 Abhandlen allf�lliger interner Events XML->Bin 
    procedure NewInternalXMLToBinEvent(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    //1 Behandlet die Eventliste f�r einen Node 
    function ProcessEvent(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; aValue: Variant; aEvent: Integer; var
      aWriteElementToBin: Boolean): Variant;
    //1 Abarbeiten der EventNodeList  
    procedure ProcessEventNodeList(aMaxIndex : integer=cEventExt);
    //1 REKURSIV!! L�scht den Node, falls der Parent keine Children mehr besitzt, soll dieser auch gel�scht werden 
    procedure RemoveNode(aMMXMLNode: IXMLDOMNode);
    //1 Bereitet CodeSite-Meldungen auf. 
    procedure SendMessage(aMsg: string);
    //1 Schaltet das Tracking eines Elements je nach Debugeinstellungen ein oder aus 
    procedure SetElementTracking(aMapElement: IXMLDOMNode);
    procedure SetMapSection(aMapSection: TMapSection);
    //1 Setzt den gew�nschten Parser 
    procedure SetParser(aXMLDoc: DOMDocument40);
    //1 Gibt die Informationen �ber den aufgetretenen Fehler an Codesite aus 
    procedure ShowError(aError: IXMLDOMParseError);
    function SwapDWordIt(aValue: DWord): DWord;
    function SwapIt(aValue: Word): Word;
    //1 Checkt 'type'- und 'offset'-Attribut. 
    function TypeAndOffsetAtrExists(aMapNode: IXMLDOMNode; out aIgnore: Boolean): Boolean;
    //1 Validiert das MMXML-File gegen�ber einem XML-Schema 
    function Validate: Boolean;
    //1 Schreibt ins Millmaster EventLogFile 
    procedure WriteLogFile(aEvent: TEventType; aText: string);
    //1 Schreibt einen Wert, abh�ngig vom Datentyp in das Bin�rfile 
    procedure WriteValueToBin(aMapNode: IXMLDOMNode; aValue: Variant; aOverwrite: Boolean; const aBitCount: Integer = 0;
      const aBitOffset: Integer = 0);
  protected
    procedure DoAfterDeleteElements(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure DoBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40); virtual;
    procedure DoBeforeDeleteElements(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
      virtual;
    procedure DoCalcExternal(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean); virtual;
    procedure DoCalcInternal(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean); virtual;
  public
    //1 Constructor f�r den MSXML-Konverter 
    constructor Create(aEventLog :TEventLogWriter=NIL);
    destructor Destroy; override;
    //1 Hauptmethode, f�r externen Aufruf von Bin->XML Konversion. 
    function BinToXML(aBinData: PByte; aBinLen: integer; aXMLMap: string; aMapSection: TMapSection; aChangeEndian:
      Boolean): string; overload;
    //1 Hauptmethode, f�r externen Aufruf von Bin->XML Konversion. 
    function BinToXML(aBinData: PByte; aBinLen: integer; aXMLMap: DOMDocument40; aMapSection: TMapSection;
      aChangeEndian: Boolean): string; overload;
    //1 Erstellt Binfile zu Debugzwecken 
    procedure SaveBinFile(aBinData: PByte; aLen: integer; aPrefix: string);
    //1 Erstellt XMLfile zu Debugzwecken  
    procedure SaveXMLFile(aXML: string; aPrefix: string);
    //1 Schreibt ein Element ins Bin�rfile 
    procedure WriteElementToBinWithoutCheck(aSelPath: string; aValue: Variant);
    //1 Hauptmethode, f�r externen Aufruf von XML->Bin Konversion. 
    function XmlToBin(aBinData: PByte; aBinLen: Integer; aXMLMap: DOMDocument40; aMapSection: TMapSection;
      aChangeEndian: Boolean; aMMXML: string): Integer; overload;
    //1 Hauptmethode, f�r externen Aufruf von XML->Bin Konversion. 
    function XmlToBin(out aBinData:PByte; aXMLMap: string; aMapSection: TMapSection; aChangeEndian: Boolean; aMMXML:
      string): Integer; overload;
    //1 Hauptmethode, f�r externen Aufruf von XML->Bin Konversion. 
    function XmlToBin(out aBinData:PByte; aXMLMap: DOMDocument40; aMapSection: TMapSection; aChangeEndian: Boolean;
      aMMXML: string): Integer; overload;
    //1 Wenn True, dann werden die Delte Events nicht angewendet 
    property IgnoreDeleteFlag: Boolean read FIgnoreDeleteFlag write FIgnoreDeleteFlag;
    property MapSection: TMapSection read FMapSection;
    property Version: string read FVersion;
    //1 Gibt das MMXML Dokument als String zur�ck. 
    property XMLDeclaration: string read GetXMLDeclaration;
    property OnAfterDeleteElements: TBeforeAfterEvent read FOnAfterDeleteElements write FOnAfterDeleteElements;
    //1 Event wird ausgel�st nachdem MMXML-Dom Baum erstellt ist.  
    property OnBeforeConvert: TBeforeAfterEvent read FOnBeforeConvert write FOnBeforeConvert;
    //1 Event wird ausgel�st nachdem MMXML-Dom Baum vollst�ndig gef�llt ist.  
    property OnBeforeDeleteElements: TBeforeAfterEvent read FOnBeforeDeleteElements write FOnBeforeDeleteElements;
    //1 Event, um ausserhalb des Konverters ein XML-Element zu modifizieren  
    property OnCalcExternal: TCalcElement read FOnCalcExternal write FOnCalcExternal;
    //1 Event, um innerhalb des Konverters ein XML-Element zu modifizieren 
    property OnCalcInternal: TCalcElement read FOnCalcInternal write FOnCalcInternal;
  end;



implementation

uses
  mmCS, comobj, filectrl, typinfo, xmlDef;

const
  cWriteInOutDebug = 'WriteConverterInOutDebug';


(*:
 *  Klasse:        TDeleteOldTempFiles
 *  Vorg�nger:     TMMThread
 *  Kategorie:     No category
 *  Kurzbeschrieb: L�scht alle *.tmp Files in einem Verzeichnis die �lter als eine bestimmte Anzahl Tage sind
 *  Beschreibung:  L�scht die alten Files die vor und nach dem Konvertieren geschrieben werden (Debug Einstellung)
                   Die Files werden asynchron gel�scht. Der Thread gibt sich selber frei und muss nicht freigegeben
                   werden.
                   Es werden nur die Dateien mit der Endung 'tmp' gel�scht.
 *)

//:-------------------------------------------------------------------
(*:
 *  Klasse:        TDeleteOldTempFiles
 *  Vorg�nger:     TMMThread
 *  Kategorie:     No category
 *  Kurzbeschrieb: L�scht alle *.tmp Files in einem Verzeichnis die �lter als eine bestimmte Anzahl Tage sind 
 *  Beschreibung:  L�scht die alten Files die vor und nach dem Konvertieren geschrieben werden (Debug Einstellung)
                   Die Files werden asynchron gel�scht. Der Thread gibt sich selber frei und muss nicht freigegeben
                   werden.
                   Es werden nur die Dateien mit der Endung 'tmp' gel�scht. 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TDeleteOldTempFiles
 *  Kategorie:        No category 
 *  Argumente:        (aDayCount, aDirName)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Initialisiert die Variablen auf und startet dann den Thread dann.
 --------------------------------------------------------------------*)
constructor TDeleteOldTempFiles.Create(aDayCount: integer; aDirName: string);
begin
  inherited Create(true);
  fDayCount := aDayCount;
  fDirName := aDirName;
  FreeOnTerminate := true;
  resume;
end;// TDeleteOldTempFiles.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Execute
 *  Klasse:           TDeleteOldTempFiles
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle tmp Dateien im Konverter Temp Verzeichnis
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TDeleteOldTempFiles.Execute;
var
  xSearchRec: TSearchRec;
  xFileAttrs: Integer;
  xThresholdDate: TDateTime;
  xDeleteCount: Integer;

  (*-----------------------------------------------------------
    L�scht ein File, wenn es �lter ist als das Threshold Datum
  -------------------------------------------------------------*)
  procedure LocalDeleteFile(const aFileName: string; aCreateDate: integer);
  var
    xCreateDate: TDateTime;
  begin
    // Das Filedatum bestimmen
    xCreateDate := FileDateToDateTime(aCreateDate);
    if xCreateDate < xThresholdDate then try
      // File l�schen
      SysUtils.DeleteFile(fDirName + aFileName);
      inc(xDeleteCount);
    except
      // Fehler nur im Codesite ausgeben
      CodeSite.SendStringEx(csmError, 'Cannot delete File', fDirName + aFileName);
    end;// if xCreateDate < xThresholdDate then try
  end;// procedure LocalDeleteFile(const aFileName: string; aCreateDate: integer);

begin
  // Datum bestimmen vor dem die Dateien gel�scht werden sollen
  xThresholdDate := now - fDayCount;
  xDeleteCount := 0;

  // Alle files finden
  xFileAttrs := faAnyFile;

  // Suche starten
  if FindFirst(fDirName + '*.tmp', xFileAttrs, xSearchRec) = 0 then try
    // erstes File l�schen (Wenn Datum stimmt) und dann weitersuchen
    LocalDeleteFile(xSearchRec.Name, xSearchRec.Time);
    while FindNext(xSearchRec) = 0 do
      // Alle weiteren Files behandeln
      LocalDeleteFile(xSearchRec.Name, xSearchRec.Time);
  finally
    // Suche abschliessen
    SysUtils.FindClose(xSearchRec);
    // Wenn Dateien gel�scht wurden, dann ausgeben wieviele es waren
    if xDeleteCount > 0 then
      CodeSite.SendInteger('TempFiles Deleted - Count', xDeleteCount);
  end;// if FindFirst(fDirName + '*.tmp', xFileAttrs, xSearchRec) = 0 then try
end;// TDeleteOldTempFiles.Execute cat:No category

//:-------------------------------------------------------------------
(*:
 *  Klasse:        TMyMapMMXMLList
 *  Vorg�nger:     TObject
 *  Kategorie:     No category
 *  Kurzbeschrieb: Verwaltung der Element mit Events
 *  Beschreibung:  Die Klasse gew�hrleistet, dass zwei Listen (die erste mit den MAP-File Nodes
                   und die andere mit den MMXML Nodes) synchronisiert und verwaltet werden.
 *)

//:-------------------------------------------------------------------
(*:
 *  Klasse:        TMyMapMMXMLList
 *  Vorg�nger:     TObject
 *  Kategorie:     No category
 *  Kurzbeschrieb: Verwaltung der Element mit Events 
 *  Beschreibung:  Die Klasse gew�hrleistet, dass zwei Listen (die erste mit den MAP-File Nodes 
                   und die andere mit den MMXML Nodes) synchronisiert und verwaltet werden. 
 *)
 
//:-------------------------------------------------------------------
constructor TMyMapMMXMLList.Create;
begin
  try
    //Erstellen der LIste f�r die Events
    mMapList:= TInterfaceList.Create;
    mMMXMLList:= TInterfaceList.Create;
    if (not Assigned(mMapList)) or (not Assigned(mMapList)) then
      raise EMMXMLConverter.Create('');
  except
    on e:EMMXMLConverter do begin
      raise EMMXMLConverter.Create(e.message+Format(' Error creating %s ',[Self.Classname]));
    end;
  end;// try except
end;// TMyMapMMXMLList.Create cat:No category

//:-------------------------------------------------------------------
destructor TMyMapMMXMLList.Destroy;
begin
  mMapList.Clear;
  mMMXMLList.Clear;
  freeAndNil(mMapList);
  freeAndNil(mMMXMLList);

  //: ----------------------------------------------

//: ----------------------------------------------
  inherited Destroy;
end;// TMyMapMMXMLList.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TMyMapMMXMLList.Add(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
begin
  try
    mMapList.Add(aMapNode);
    mMMXMLList.Add(aMMXMLNode);
  except
    on e:EMMXMLConverter do begin
      raise EMMXMLConverter.Create(e.message+Format(' Error Add items in %s ',[Self.Classname]));
    end;
  end;// try except
end;// TMyMapMMXMLList.Add cat:No category

//:-------------------------------------------------------------------
function TMyMapMMXMLList.GetCount: Integer;
begin
  Result := 0;

  if (mMapList.Count = mMMXMLList.Count) then
    Result := mMapList.Count;
end;// TMyMapMMXMLList.GetCount cat:No category

//:-------------------------------------------------------------------
function TMyMapMMXMLList.GetMapItems(Index: Integer): IXMLDOMNode;
begin
  Result := Nil;

  if (mMapList.Count = mMMXMLList.Count) and (Index<mMMXMLList.Count) then
    Result := IXMLDOMNode(mMapList[Index]);
end;// TMyMapMMXMLList.GetMapItems cat:No category

//:-------------------------------------------------------------------
function TMyMapMMXMLList.GetMMXMLItems(Index: Integer): IXMLDOMNode;
begin
  Result := Nil;

  if (mMapList.Count = mMMXMLList.Count) and (Index<mMMXMLList.Count) then
    Result := IXMLDOMNode(mMMXMLList[Index]);
end;// TMyMapMMXMLList.GetMMXMLItems cat:No category

//:-------------------------------------------------------------------
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aEventLog)
 *
 *  Kurzbeschreibung: Constructor f�r den MSXML-Konverter
 *  Beschreibung:     
                      aEventLog: Wird der Handle mitgegeben, erscheinen EventLog-Meldungen
                      unter dem Namen des Aufrufers.
                      
                      In der Registry werden 2 Eintr�ge gecheckt. Auf Grund der Existenz und G�ltigkeit
                      wird festgelegt, ob im sp�teren Verlauf das MMXML gegen�ber dem vorhandenen 
                      XML-Schema-File validiert werden soll.
 --------------------------------------------------------------------*)
constructor TMMXMLConverter.Create(aEventLog :TEventLogWriter=NIL);
begin
  inherited Create;

//: ----------------------------------------------
  //: ----------------------------------------------
  FVersion := cVersion;

  mIntEventLog := aEventLog; //Zuweisen EventLogHandle
  if (mIntEventLog=Nil) then
    mInternalEventLogHandle := True
  else
    mInternalEventLogHandle := False;

  mEventList := Nil;
  mDelEventList := Nil;

    //Check 2 registry entries for valid XML Validation to do
  mXMLSchemaStream := GetRegString(cRegLM, cRegMMServicePath, 'XMLSchemaFile');
  if GetRegBoolean(cRegLM, cRegMMServicePath, 'XMLValidation', False) and
     FileExists(mXMLSchemaStream) then
    mValidate := True
  else
    mValidate := False;

    // Sammelt alle Warnings um diese am Ende gesammelt auszugeben
  mWarnings := nil;

    (* Als Default, wird der Konverter bei einer Abw�rtskonvertierung den ben�tigten
       Buffer selber erstellen *)
  mCreateBufferForXMLToBin := true;

//: ----------------------------------------------
  //: ----------------------------------------------
    // Debug Settings

    // Zus�tzliche CodeSite Outputs
  mDebugOutEnabled := GetClassDebug(ClassName);

    // Tracking einzelner Elemente
  mDebugTrackingElements := nil;
    // Kennzeichnet ein Element w�hrend der Konvertierung
  mTrackCurrentElement := false;

    // Liest ob das Tracking eingeschaltet werden soll
  mDebugTracking := CodeSite.Enabled and GetRegBoolean(cRegLM, cRegMMDebug, cRegDebugTrackingEnabled, false);
  if mDebugTracking then begin
      // Wenn das Tracking eingeschaltet werden soll, dann die Liste der Elemente einlesen
    mDebugTrackingElements := TStringList.Create;
    mDebugTrackingElements.Commatext := GetRegString(cRegLM, cRegMMDebug, cRegDebugTrackingElements, '');
  end;// if mDebugTracking then begin
end;// TMMXMLConverter.Create cat:No category

//:-------------------------------------------------------------------
destructor TMMXMLConverter.Destroy;
begin
  if (Assigned(mIntEventLog)) and (mInternalEventLogHandle) then
    freeAndNil(mIntEventLog);

  FreeAndNil(mWarnings);

    // Liste mit den Elementen die detailiert beobachtet wurden
  FreeAndNil(mDebugTrackingElements);

  FreeAndNil(mDelEventList);
  FreeAndNil(mEventList);

  //: ----------------------------------------------

//: ----------------------------------------------
  inherited Destroy;
end;// TMMXMLConverter.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           BinToXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aBinLen, aXMLMap, aMapSection, aChangeEndian)
 *
 *  Kurzbeschreibung: Hauptmethode, f�r externen Aufruf von Bin->XML Konversion.
 *  Beschreibung:     
                      aBinData:      Pointer aufs Bin�rfile
                      aBinLen:       L�nge des Bin�rfiles in Bytes
                      aXMLMap:       MapFile (Inhalt) als Stream
                      aChangeEndian: True setzen, falls Bytereihenfolge nicht INTEL Format entspricht.
                      Result:        R�ckgabe des MMXML-Files als Stream
 --------------------------------------------------------------------*)
function TMMXMLConverter.BinToXML(aBinData: PByte; aBinLen: integer; aXMLMap: string; aMapSection: TMapSection;
  aChangeEndian: Boolean): string;
begin
  if mDebugOutEnabled then
    EnterMethod('BinToXML(1)');

  if (LoadXMLMap(aXMLMap)) then begin
    //Call von Overloaded BinToXML
    Result := BinToXML(aBinData, aBinLen, mXMLMap, aMapSection, aChangeEndian);
  end else begin
   // Gibt eine Detailierte Fehlermeldung in Codesite aus
     ShowError(mXMLMap.parseError);
     raise EMMXMLConverter.Create(' XML Descriptor (MAP) parsing failed! ');
  end;// if (LoadXMLMap(aXMLMap))
end;// TMMXMLConverter.BinToXML cat:No category

//:-------------------------------------------------------------------
(*: Member:           BinToXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aBinLen, aXMLMap, aMapSection, aChangeEndian)
 *
 *  Kurzbeschreibung: Hauptmethode, f�r externen Aufruf von Bin->XML Konversion.
 *  Beschreibung:     
                      aBinData:      Pointer aufs Bin�rfile
                      aBinLen:       L�nge des Bin�rfiles in Bytes
                      aXMLMap:       MapFile (Inhalt) als Stream
                      aChangeEndian: True setzen, falls Bytereihenfolge nicht INTEL Format entspricht.
                      Result:        R�ckgabe des MMXML-Files als Stream
 --------------------------------------------------------------------*)
function TMMXMLConverter.BinToXML(aBinData: PByte; aBinLen: integer; aXMLMap: DOMDocument40; aMapSection: TMapSection;
  aChangeEndian: Boolean): string;

  procedure InitOutputDocument(aDoc: DOMDocument40);
  var
    xXMLElement: IXMLDOMElement;
    xCommentElement: IXMLDOMComment;
  begin
    try
      if assigned(aDoc) then begin
        // Der neue Parser in MSXML 4.0 soll verwendet werden
        SetParser(aDoc);

        aDoc.AppendChild(aDoc.CreateProcessingInstruction(cXMLLanguage, cLoepfeProlog));

        xCommentElement := aDoc.CreateComment(Format('%s %s', [cLoepfeAdditionalInfo, FormatDateTime('c',Now)]));
        aDoc.AppendChild(xCommentElement);


        xXMLElement := aDoc.CreateElement(cEleLoepfeBody);
        aDoc.AppendChild(xXMLElement);

        SetMapSection(aMapSection); //Sets the decided MapSection as Rootelement

        //Versionsinfo lesen
  //      xCommentElement := aDoc.CreateComment(Format('%s: %s', [cLoepfeSoftwareAttr, AttributeToStringDef(cAtrVersion, mMapSectionElement, '?no version found?')]));
        aDoc.insertBefore(aDoc.CreateProcessingInstruction(cConverterProlog, Format(cPIConverter, [AttributeToStringDef(cAtrVersion, mMapSectionElement, '?no version found?'), cVersion])), xCommentElement);
  //      aDoc.insertBefore(xCommentElement, xXMLElement);
      end;// if assigned(aDoc) then begin
    except
        raise;
    end;
  end;

begin
  if mDebugOutEnabled then
    EnterMethod('BinToXML');

    // zu Debug Zwecken
  if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then
    SaveBinFile(aBinData, aBinLen, 'In');

    //Initialisieren
  Result := '';
  mUp := True;
  mBinDoc := aBinData;
  mChangeEndian := aChangeEndian;
  mBinLen := aBinLen; //Effektive L�nge des bin�r files
  FMapSection := aMapSection;

//: ----------------------------------------------
  //: ----------------------------------------------
  try
      // Pr�fen ob ein Mapfile angegeben ist (z.b. Konnte nicht aus DB gelesen werden)
    if not(assigned(aXMLMap)) then
      raise EMMXMLConverter.Create('No Mapfile assigned.');
      // Pr�fen ob das Mapfile einen Inhalt hat (mindestens einen Knoten)
    if aXMLMap.childNodes.length = 0 then
      raise EMMXMLConverter.Create('No valid Mapfile assigned.');

      //Zuweisen des �bergebenen MAP-DOMBaums an die Membervariable
    mXMLMap := aXMLMap;
      // XML Dokument (MSXML) f�r den Output erzeugen
    mMMXML := DOMDocumentMMCreate ;
    if assigned(mMMXML) then begin
        // Erzeugt die Grundstruktur des Outputfiles
      InitOutputDocument(mMMXML);

        //Before (start filling MMXML) event ausl�sen
      DoBeforeConvert(Self, mXMLMap, mMMXML);

      if NOT(MapBinToXML()) then begin
        raise EMMXMLConverter.Create('MapBinToXML failed!');
      end
      else begin  //Konvertierung erfolgreich
        if mValidate then begin
            //Output XML gegen�ber dem Schema validieren
          if not Validate then begin
            WriteLogFile(etWarning, 'XML Output (MMXML) not valid against XML schema! ');
          end;
        end; //if not Validate

          //After (filled MMXML) and before DelEventListNodes event ausl�sen
        DoBeforeDeleteElements(Self, mXMLMap, mMMXML);

          //L�schen allf�lliger nicht ben�tigter Elemente in MMXML
        DelEventListNodes;

          //After (filled MMXML) and after DelEventListNodes event ausl�sen
        DoAfterDeleteElements(Self, mXMLMap, mMMXML);

          // XML Stream als String zur�ckgeben
        Result := mMMXML.xml;
      end; //if NOT(MapBinToXML(aBinData))
    end; //if

      // zu Debug Zwecken
    if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then
      SaveXMLFile(result, 'Ot');

    if mDebugOutEnabled and Assigned(mMMXML) then    //Nue:31.10.06
      CodeSite.SendStringEx(csmInfo, 'XML After Delete', FormatXML(mMMXML));

    if mMsgList<>NIL then
      codeSite.SendString('BinToXML:Summary of MMXMLConverter-Messages:',mMsgList.Text);

    if assigned(mWarnings) then begin
      if mWarnings.Count > 0 then begin
        WriteToEventLogDebug(mWarnings.Text, '', etWarning);
        mWarnings.Clear;
      end;// if mWarnings.Count > 0 then
    end;// if assigned(mWarnings) then begin

  except
    on e: Exception do begin
          //Im Fehlerfall wird mEventList sonst nicht mehr regul�r freigegeben
      if mMsgList<>NIL then begin
        codeSite. SendString('BinToXML:Summary of MMXMLConverter-Messages:',mMsgList.Text);
        mMsgList.Clear;
        freeAndNil(mMsgList);
      end;
      if Assigned(mEventList) then
          //Im Fehlerfall wird mEventList sonst nicht mehr regul�r freigegeben
        freeAndNil(mEventList);
      if Assigned(mDelEventList) then
          //Im Fehlerfall wird mDelEventList sonst nicht mehr regul�r freigegeben
        freeAndNil(mDelEventList);
      raise EMMXMLConverter.Create(e.Message+' BinToXML failed. ');
    end;
  end;
end;// TMMXMLConverter.BinToXML cat:No category

//:-------------------------------------------------------------------
(*: Member:           CheckBitfield
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aValue, aOverwrite, aBitCount, aBitOffset)
 *
 *  Kurzbeschreibung: Behandelt die Attribute 'bitOffset' und 'bitCount' in einem Element Knoten
 *  Beschreibung:     
                      Jedes Element enth�lt ein OFFSET und ein TYPE Attribut.
                      Damit Funktion durchlaufen wird, m�ssen  'bitOffset' und 'bitCount' 
                      vorhanden sein und die Bereiche aufgrund des Datentyps nicht gesprengt
                      werden.
                      Als Datentypen sind alle Integertypen erlaubt.
 --------------------------------------------------------------------*)
function TMMXMLConverter.CheckBitfield(aMapNode: IXMLDOMNode; var aValue: Variant; var aOverwrite: Boolean; out
  aBitCount, aBitOffset: Integer): Boolean;
var
  xTmpNodeList: IXMLDOMNodeList;
  xBitOffset: Integer;
  xBitCount: Integer;
  xMaskValue: Variant;
  i: Integer;

  procedure ConvertTrueFalse(var aValue: Variant);
  begin
    // aValue kann auch bereits eine Zahl sein. Deshalb nicht in jedem Fall wandelbn
    if AnsiSameText(aValue,cFalseXML) then begin
      aValue := 0;
    end
    else if AnsiSameText(aValue,cTrueXML) then begin
      aValue := 1;
    end; //if AnsiSameText(aValue,'false')
  end;

begin
  // result := aVariant; //Result den richtigen Type zuordnen

  try
    Result := False;
    aOverwrite := True;
    //Check ob der Node bitfelder enth�lt
    //'bitOffset'-, 'bitCount'-Element m�ssen immer vorhanden sein
    xTmpNodeList := aMapNode.selectNodes(Format('../%s[@%s and @%s]', [aMapNode.nodeName,cAtrBitOffset,cAtrBitCount]));
    if xTmpNodeList.length<>1 then begin
      // Wenn der Datentyp ein Zahlwert ist, dann kontrollieren ob Value True oder false ist und entsprechend wandeln
      if mActElementType in [dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle] then
        ConvertTrueFalse(aValue);

      //Geforderte Attribute sind nicht beide vorhanden
      EXIT;   //Kein Bitfield => zur�ck zum Aufrufer
    end; //if not(VarIsNull(xTmpNodeList))

    //Offsetwert lesen (Attribute ist vorhanden wurde vorg�ngig gecheckt)
    xBitOffset := AttributeToIntDef(cAtrBitOffset, aMapNode, 0);
    xBitCount  := AttributeToIntDef(cAtrBitCount, aMapNode, 0);
    if xBitCount=0 then begin
      raise EMMXMLConverter.Create(Format('Error Bitcount=0 in XMLMap: Element:%s',[aMapNode.xml]));
      EXIT;
    end; //if xBitCount=0

    //Check ob xBitCount nicht gr�sser als der Datentyp
    if ((xBitCount+xBitOffset) > (cBasicTypeDimensions[mActElementType]*8{8 Bit pro Byte})) then begin
      raise EMMXMLConverter.Create(Format('Error (Bitcount+Bitoffset) bigger then datatype in XMLMap: Element:%s',[aMapNode.xml]));
      EXIT;
    end;

    if mUp then begin
      //Bitfelder aus �bergebenem Wert extrahieren und als String zur�ckgeben
      //Bsp: xBitOffset=3, xBitCount=4, aValue= 01010101  Datentyp ist Byte
      //     aMaskValue=00001111 => SHL 3 =>  aMaskValue = 01111000
      //     Veranden: aMaskValue 01111000
      //               aValue     01010101
      //               ===================
      //               aValue     01010000  => SHR 3 => aValue = 00001010
      //

      xMaskValue := aValue; //xMaskValue den richtigen Type zuordnen
      xMaskValue := 0;
      for i := 0 to xBitCount-1 do
        xMaskValue := xMaskValue + (1 shl i) ;  //Maske generieren 00001111

      xMaskValue := xMaskValue SHL xBitOffset;  //Schieben der Maske auf die richtige Position 01111000
      aValue := aValue AND xMaskValue;          //Den Wert mit dem Muster ver'anden' 01010000
      aValue := aValue SHR xBitOffset;          //Schieben des Wertes auf die richtige Position 00001010

      if xBitCount=1 then begin
        ConvertBoolToString(aValue);
      end; //if xBitCount=1

      Result := True;
    end

    else begin // not mUp (XML to Bin)
      //false/true umsetzen, bei XMLToBin auf ein Bit
      ConvertTrueFalse(aValue);
      //Wert an die geforderte Position verschieben
      aValue := aValue SHL xBitOffset;
      //!!Pro Call dieser Fuktion CheckBitField wird hier ein XML-Element auf einen Speicherbereich im Bin�rFile abgebildet.
      //  Mehrere XML-Elemente, welche eine Bit- oder BitGruppen -Repr�sentation sind k�nnen im selben Speicherbereich liegen.
      //  Daher wird beim Schreiben ins Bin�rfile (WriteValueToBin), jeweils der bestehende Wert des Speicherbereichs mit dem
      //  neuen aValue "verodert".   aOverwrite := False;
      aOverwrite := False;

      aBitOffset := xBitOffset; //Nue:1.15
      aBitCount := xBitCount;   //Nue:1.15
    end; ////if mUp

    // Tracking (siehe SetElementTracking())
    if mTrackCurrentElement and (not(VarIsNull(aValue))) then
      CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue after Bitfield', aValue);

  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' CheckBitfield failed. ');
    end;
  end; //try
end;// TMMXMLConverter.CheckBitfield cat:No category

//:-------------------------------------------------------------------
(*: Member:           CheckConvert
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aValue)
 *
 *  Kurzbeschreibung: Behandelt die Convert-Attribute in einem Element Knoten
 *  Beschreibung:     
                      Jedes Element enth�lt ein OFFSET und ein TYPE Attribut.
                      ivision.
                      Es werden Formeln und 'bitwise negation' behandelt.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.CheckConvert(aMapNode: IXMLDOMNode; var aValue: Variant);
var
  i: Integer;
  xConvert: Integer;
  xBoolValue: Boolean;

  procedure ProcessFormulaParam(xActAttribute: IXMLDOMNode; var aValue: Variant);
    var
      xDivisor: extended;
      xFloatString: string;
      xStringList: TmmStringlist;
      i,j: integer;
      xOperator: integer;
  begin
    try
      xStringList := TmmStringList.Create;	{ construct the list object }
      try
        xStringList.CommaText := xActAttribute.nodeValue;
        xOperator := cOperatorNone;
        if mUp then begin
          //Direction Bin->XML
          //Umgekehrte Reihenfolge der Abarbeitung
          for i := xStringList.Count-1 downto 0 do begin
            for j := cOperatorNone to cOperatorMul do begin
              if cOperatorNames[j]=xStringList.Names[i] then begin
                xOperator := j;
                BREAK; //!!!!!!!!!!!!!
              end; //if
            end; //for

            xFloatString := xStringList.Values[xStringList.Names[i]];
            case xOperator of
              cOperatorNone:; //Keine Aktion
              cOperatorAdd: aValue := aValue - MMStrToFloatDef(xFloatString, 0);  //Addition => Wird zur Subtraktion
              cOperatorMul: begin   //Multiplikation => Wird zur Division
                xDivisor := MMStrToFloatDef(xFloatString, 1);
                if xDivisor <> 0 then
                  aValue := aValue / xDivisor;
              end;
            else
            end;  // case
          end; //for
        end
        else begin
          //Direction XML->Bin
          for i := 0 to xStringList.Count-1 do begin
            for j := cOperatorNone to cOperatorMul do begin
              if cOperatorNames[j]=xStringList.Names[i] then begin
                xOperator := j;
                BREAK; //!!!!!!!!!!!!!
              end; //if
            end; //for

            xFloatString := xStringList.Values[xStringList.Names[i]];
            case xOperator of
              cOperatorNone:; //Keine Aktion
              cOperatorAdd: aValue := aValue + MMStrToFloatDef(xFloatString, 0);  //Addition
              cOperatorMul: aValue := aValue * MMStrToFloatDef(xFloatString , 1); //Multiplikation
            else
            end;  // case
          end; //for
        end; //if mUp
      finally
        xStringList.Free;	{ destroy the list object }
      end;
    except
      raise EMMXMLConverter.Create('Error in ProcessFormulaParam ');
    end;
  end;

begin
  try
    //Check 'conv' in Mapfile
    if AttributExists(cAtrConvert, aMapNode.attributes) then begin
      //convert gefunden -> Verarbeiten
      xConvert := cConvertNone;
      for i := cConvertNone to cConvertNot do begin
        if AnsiSameText(aMapNode.attributes.getNamedItem(cAtrConvert).nodeValue,cFunctionNames[i]) then begin
          xConvert := i;
        end; //if
      end; //for
      case xConvert of
        cConvertFormula: begin
          if AttributExists(cAtrParameter, aMapNode.attributes) then begin
            //Verarbeiten der Formelparameter
            ProcessFormulaParam(aMapNode.attributes.getNamedItem(cAtrParameter), aValue);
          end; //if AttributExists(cAtrParameter, aMapNode.attributes)
        end;
        cConvertNot: begin
          // Die Text Repr�sentation von true und false k�nnen nicht direkt konvertiert werden
          xBoolValue := aValue;
          aValue := not xBoolValue; //bitwise negation of aValue
          // Wieder in einen String verwandeln
          ConvertBoolToString(aValue);
        end;
      else ;
        SendMessage(Format('Invalid value for XML-Attribute %s in XML-Element %s', [cAtrConvert, GetElementFullXPath(aMapNode)]));
        raise EMMXMLConverter.Create(Format('Invalid value for XML-Attribute %s in XML-Element %s', [cAtrConvert, GetElementFullXPath(aMapNode)]));
      end;
    end; //if AttributExists(cAtrConvert, aMapNode.attributes)

    // Tracking (siehe SetElementTracking())
    if mTrackCurrentElement and (not(VarIsNull(aValue))) then
      CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue after Convert', aValue);

  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' CheckConvert failed. ');
    end;
  end;
end;// TMMXMLConverter.CheckConvert cat:No category

//:-------------------------------------------------------------------
(*: Member:           CheckEnum
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode, aValue)
 *
 *  Kurzbeschreibung: Behandelt die enum-Attribute in einem Element Knoten
 *  Beschreibung:     
                      Jedes Element enth�lt ein OFFSET und ein TYPE Attribut.
                      ivision.
                      Wandelt Enumerations-Werte in entsprechende Zahlenwerte (XML->Bin)
                      oder umgekehrt um. (Bin->XML)
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.CheckEnum(aMapNode, aMMXMLNode: IXMLDOMNode; var aValue: Variant);
var
  xStringList: TmmStringList;
  i: Integer;
  xOrgValue: Variant;
  xFound: Boolean;

  //--Local Procedure ------------------------------------------------------------------------
  procedure SetEnumXMLBin(aMapNode: IXMLDOMNode; var aValue: Variant; aFirstCall: Boolean);
  var
    xDefValue: Variant;
  begin
    if (xStringList.Values[aValue] = cEnumOthers) then begin
      aValue := 0;  //Nichts schreiben
    end
    else begin
      aValue := xStringList.Values[aValue];
      if aFirstCall then begin
        if (aValue = '')  then begin
          // Wenn der Enumerationswert unbekannt ist, dann den Defaultwert eintragen
          if GetDefaultFromMap(aMapNode, aValue) then begin
            //Wert des Defaultwertes speichern f�r Ausgabe im Fehlerfall
            xDefValue := aValue;
  //Nue:21.3.06          // Defaultwert konvertieren
  //Nue:21.3.06          aValue := xStringList.Values[aValue];

            SetEnumXMLBin(aMapNode, aValue, False);  //REKURSIVER Aufruf !!!!!!!!!!!!
            // Wenn der Defaultwert auch nicht konvertiert werden kann, dann auf 0 setzen
            if aValue = '' then begin
              aValue := 0;
              WriteLogFile(etError, Format('Invalid defaultvalue (%s) in mapfile for XML element <%s>! Set to value (%s).',
              [xDefValue, GetElementFullXPath(aMapNode), aValue]));
            end; //if aValue = '' then begin
          end else begin
            // Kein Defaultwert gefunden
            aValue := 0;
          end;// if GetDefaultFromMap(aXMLMapNode, aValue) then begin
        end;// if (aValue = '')  then begin
      end; // aFirstCall then
    end; //if (aValue = cEnumOthers);
  end;
  //--End Local Procedure ------------------------------------------------------------------------

begin
  try
    //Check 'enum' in Mapfile
    if AttributExists(cAtrEnum, aMapNode.attributes) then begin

      // Originalwert sichern (wird noch gebraucht)
      xOrgValue := aValue;

      //enum gefunden -> Verarbeiten
      xStringList := TmmStringList.Create;	{ construct the list object }
      try
        xStringList.CommaText := aMapNode.attributes.getNamedItem(cAtrEnum).nodeValue;
        if mUp then begin
  //l�schen        // Originalwert sichern (wird noch gebraucht)
  //l�schen        xOrgValue := aValue;
          //Direction Bin->XML
          aValue := AttributeToStringDef(cAtrDefault, aMapNode, 'No default!');  //Attribute Default muss immer existieren, ansonsten wird 'No default!' zur�ckgegeben.
  //alt        aValue := xStringList.Names[0]; //Erster Wert der Enumeration als Defaultwert, falls der Wert im Bin�rfile out of Range w�re

          xFound := False;
          i := 0;
          //Check xOrgValue als Wert in Enumeratoren vorhanden
          while (i <= xStringList.Count-1) and (not xFound) do begin
  ////          if AnsiSameText(VarAsType(xStringList.Names[i], varString), VarAsType(xOrgValue, varString)) then begin//Checken des Wertes
            if AnsiSameText(VarAsType(xStringList.Values[xStringList.Names[i]], varString), VarAsType(xOrgValue, varString)) then begin//Checken des Wertes
              aValue := xStringList.Names[i]; //Zuweisen des Namens
  ////            aValue := xStringList.Values[xStringList.Names[i]];
              xFound := True;
            end;
            INC(i);
          end; //while

          //xOrgValue als Wert in Enumeratoren NICHT vorhanden
          if not xFound then begin
            i := 0;
            //Check cEnumOthers als Wert in Enumeratoren vorhanden
            while (i <= xStringList.Count-1) and (not xFound) do begin
              if AnsiSameText(VarAsType(xStringList.Values[xStringList.Names[i]], varString), VarAsType(cEnumOthers, varString)) then begin//Checken des Wertes
                aValue := xStringList.Names[i]; //Zuweisen des Namens
                xFound := True;
              end;
              INC(i);
            end; //while
          end; //if not xFound then begin
        end //mUp
        else begin
          //Direction XML->Bin
          SetEnumXMLBin(aMapNode, aValue, True);
        end; //if mUp
      finally
        xStringList.Free;	{ destroy the list object }
      end;
    end; //if AttributExists(cAtrEnum, aMapNode.attributes)

    // Tracking (siehe SetElementTracking())
    if mTrackCurrentElement and (not(VarIsNull(aValue))) then
      CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue after Enum', aValue);

  except
    on e: Exception do begin
        SendMessage(Format('Invalid value for XML-Attribute %s (value:%s) in XML-Element %s',
          [aMapNode.attributes.getNamedItem(cAtrEnum).nodeName, aMapNode.attributes.getNamedItem(cAtrEnum).nodeValue, GetElementFullXPath(aMapNode)]));
        raise EMMXMLConverter.Create(e.Message+' CheckEnum failed. ');
    end;
  end;
end;// TMMXMLConverter.CheckEnum cat:No category

//:-------------------------------------------------------------------
(*: Member:           CheckFacets
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode, aValue)
 *
 *  Kurzbeschreibung: Behandelt die Facets eines XML Elements
 *  Beschreibung:     
                      Alle Facets werden durchgegangen und gecheckt.
                      Die Facet-Werte beziehen sich immer auf die XML Seite (wie der Defaultwert).
                      Liegt ein Wert ausserhalb der Facets, dann wird der Defaultwert verwendet.
                      Liegt ein Defaultwert (wegen fahrl�ssiger Eingabe oder wegen einer allf�llig nachfolgenden
                      Konvertierung) ausserhalb der Facets wird der Wert wie folgt angepasst (Beispiele):
                      < minIncl="1" 	: bei Integer -> 1		bei Float -> 1.0
                      > maxIncl="100" 	: bei Integer -> 100		bei Float -> 100.0
                      < minExcl="1"	: bei Integer -> 2		bei Float -> 1.0+Math.MinSingle
                      > maxExcl="100"	: bei Integer -> 99		bei Float -> 100.0-Math.MinSingle
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.CheckFacets(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; var aValue: Variant);
var
  xFacetList: Array[cFacetNone..cFacetMaxExcl] of Extended;
  xFacetFound: Array[cFacetNone..cFacetMaxExcl] of Boolean;
  xValueInRange: Boolean;
  i: Integer;
  xValue: Variant;
begin
  try
    //Check Facets in Mapfile
    xFacetFound[cFacetNone] := True; //Initialisieren: 1=Keine Facets
    //Facets suchen und in Liste xFacetsList ablegen
    for i:= cFacetNone+1 to cFacetMaxExcl do begin
      if AttributExists(cFacetNames[i], aMapNode.attributes) then begin
        //Verarbeiten Facet item
        //ACHTUNG: Die Verarbeitung mit der xFacetList funktioniert fehlerhaft, wenn die Elemente anstelle von 'extended' als
        //  'variant' deklariert werden. Der Fehler konnte nicht genau eruiert werden!!!!!!!!!!!!!!! (20.10.04 Nue/Lok)
        xFacetList[i] := MMStrToFloat(aMapNode.attributes.getNamedItem(cFacetNames[i]).nodeValue);
        xFacetFound[i] := True; //Initialisieren: Facets gefunden
        xFacetFound[cFacetNone] := False; //Initialisieren: 1=Keine Facets

        if mUp and
              //Sonderfall: Keine Bin�rfile �bergeben => Defaultwert aus MAP-File holen
              (mBinDoc=NIL) then begin //Check ob BinToXML
          //Facet attribut im MMXMLNode anh�ngen
          (aMMXMLNode as IXMLDOMElement).setAttribute(cFacetNames[i],xFacetList[i]);
        end; //if mUp
      end; //if AttributExists(cFacetNames[i], aMapNode.attributes)
    end; //for i :=

    if not(xFacetFound[cFacetNone]) then begin
      xValueInRange := True;

  //Facets vorhanden -> Verarbeiten der Facets
   // Liegt der Wert ausserhalb der Facets -> Defaultwert �bernehmen
      if (((xFacetFound[cFacetMinIncl]) and (not (aValue >= xFacetList[cFacetMinIncl]))) or //Check MinIncl
         ((xFacetFound[cFacetMinExcl]) and (not (aValue > xFacetList[cFacetMinExcl]))) or //Check MinExcl
         ((xFacetFound[cFacetMaxIncl]) and (not (aValue <= xFacetList[cFacetMaxIncl]))) or //Check MaxIncl
         ((xFacetFound[cFacetMaxExcl]) and (not (aValue < xFacetList[cFacetMaxExcl])))) then begin //Check MaxExcl

        // Bisherigen Wert f�r die Fehlermeldung sichern
        xValue := aValue;
  {      if (VarType(aValue)=varSingle) or (VarType(aValue)=varDouble) then //float Datentyp
          aValue := AttributeToVariantDef(cAtrDefault, aMapNode, 0.0)  //Attribute Default muss immer existieren, ansonsten wird 0 zur�ckgegeben.
        else}
          //Ganzzahlige Datentypen
          aValue := AttributeToFloatDef(cAtrDefault, aMapNode, 0);  //Attribute Default muss immer existieren, ansonsten wird 0 zur�ckgegeben.

        // Warnung ins Eventlog (wird gesammelt und am Ende der Konvertierung ausgegeben)
        WriteLogFile(etWarning, Format('Facet violation: value (%s) for XML element <%s> out of bounds! Set to Default (%s)',
            [xValue, GetElementFullXPath(aMapNode), aValue]));
      end; //if (not (aValue >= xFacetList[cFacetMinInc

   //Setzen des Wertes auf die entsprechende Facetgrenze, falls der Default ausserhalb liegt.
    //MinIncl
      if xValueInRange and(xFacetFound[cFacetMinIncl]=True) then begin
        if not (aValue >= xFacetList[cFacetMinIncl]) then begin //Check MinIncl
          WriteLogFile(etWarning, Format('Facet violation: value (%s) for XML element <%s> out of bounds! Set to MinIncl (%f)',
              [aValue, GetElementFullXPath(aMapNode), xFacetList[cFacetMinIncl]]));
          aValue := xFacetList[cFacetMinIncl];
          xValueInRange := False;
        end;
      end;
    //MinExcl
      if xValueInRange and(xFacetFound[cFacetMinExcl]=True) then begin
        if not (aValue > xFacetList[cFacetMinExcl]) then begin //Check MinExcl
          xValue := aValue;
          if (VarType(aValue)=varSingle) or (VarType(aValue)=varDouble) then begin //float Datentyp
            aValue := xFacetList[cFacetMinExcl]+MinSingle;
          end
          else begin
            //Ganzzahlige Datentypen
            aValue := xFacetList[cFacetMinExcl]+1;
          end;
          WriteLogFile(etWarning, Format('Facet violation: value (%s) for XML element <%s> out of bounds! Set to MinExcl (%f)',
              [xValue, GetElementFullXPath(aMapNode), aValue]));
          xValueInRange := False;
        end;
      end;
    //MaxIncl
      if xValueInRange and(xFacetFound[cFacetMaxIncl]=True) then begin
        if not (aValue <= xFacetList[cFacetMaxIncl]) then begin //Check MaxIncl
          WriteLogFile(etWarning, Format('Facet violation: value (%s) for XML element <%s> out of bounds! Set to MaxIncl (%f)',
              [aValue, GetElementFullXPath(aMapNode), xFacetList[cFacetMaxIncl]]));
          aValue := xFacetList[cFacetMaxIncl];
          xValueInRange := False;
        end;
      end;
    //MaxExcl
      if xValueInRange and(xFacetFound[cFacetMaxExcl]=True) then begin
        if not (aValue < xFacetList[cFacetMaxExcl]) then begin //Check MaxExcl
          xValue := aValue;
          if (VarType(aValue)=varSingle) or (VarType(aValue)=varDouble) then begin //float Datentyp
            aValue := xFacetList[cFacetMaxExcl]-MinSingle;
          end
          else begin
            //Ganzzahlige Datentypen
            aValue := xFacetList[cFacetMaxExcl]-1;
          end;
          WriteLogFile(etWarning, Format('Facet violation: value (%s) for XML element <%s> out of bounds! Set to MaxExcl (%f)',
              [xValue, GetElementFullXPath(aMapNode), aValue]));
  //        xValueInRange := False;
        end;
      end;

  //   //Check ob der Wert g�ltig. Wenn nicht Defaultwert �bernehmen
  //     if not(aValueInRange) then
  //       xActAttribute := xAttributeList.getNamedItem(cAtrDefault);
  //       if not VarIsNull(xActAttribute) then begin
  //         //Defaultwert gefunden und zuweisen
  //         aValue := xActAttribute.value;
  //       else begin  //Kein Defaultwert gefunden ERROR
  //         CodeSite.SendFmtMsg('Invalid value %s for XML-Element %s and no defaultvalue available!',[aValue, aMapNode.nodeName]);
  //         raise Error(Format('Invalid value %s for XML-Element %s and no defaultvalue available!',[aValue, aMapNode.nodeName]));
  //       end;
    end; //if not(xFacetFound[cFacetNone])

    // Tracking (siehe SetElementTracking())
    if mTrackCurrentElement and (not(VarIsNull(aValue))) then
      CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue after Facets', aValue);

  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' CheckFacets failed. ');
    end;
  end;
end;// TMMXMLConverter.CheckFacets cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.CheckMMXMLPlausability(aValue: Variant; aDataType: TDataType; out aLast: Boolean);
begin
  aLast := False;

  case aDataType of
    dtChar: begin
      aLast := True;
    end;
    dtUCChar: begin
      aLast := True;
    end;
    dtUCBit: begin
      aLast := True;
    end;
    dtWord: begin
      ;
    end;
    dtDWord: begin
      ;
    end;
  else
     ;
  end; //case
end;// TMMXMLConverter.CheckMMXMLPlausability cat:No category

//:-------------------------------------------------------------------
(*: Member:           CheckSequenceBinToXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode, aValue)
 *
 *  Kurzbeschreibung: Die verschiedenen Attribute eines XML-Map Elements werden gecheckt.
 *  Beschreibung:     
                      Die verschiedenen Attribute eines XML-Map Elements (aMapNode) werden aufgrund 
                      des Wertes im Bin�rfile (aValue) in der richtigen Reihenfolge gecheckt.
                      aValue wird gegebenenfalls angepasst.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.CheckSequenceBinToXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; var aValue: Variant);
var
  xOverwrite: Boolean;
  xBitCount: Integer;
  xBitOffset: Integer;
begin
  try
    //Checken allf�lliger Biffields
    CheckBitfield(aMapNode,aValue,xOverwrite, xBitCount{obsolet}, xBitOffset{obsolet});

    //Checken allf�lliger Converts
    CheckConvert(aMapNode,aValue);

    //Checken allf�lliger Enums
    CheckEnum(aMapNode,aMMXMLNode,aValue);

    //Checken allf�lliger Facets
    CheckFacets(aMapNode,aMMXMLNode,aValue);

  except
    raise;
  end;
end;// TMMXMLConverter.CheckSequenceBinToXML cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.ConvertBoolToString(var aValue: Variant);
begin
  //false/true setzen, bei BinToXML wenn nur ein Bit gesetzt ist
  aValue := GetEnumName(TypeInfo(boolean), ord(boolean(aValue <> 0)))
end;// TMMXMLConverter.ConvertBoolToString cat:No category

//:-------------------------------------------------------------------
(*: Member:           DelEventListNodes
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Die Eventliste wird durchgegangen und Nodes, welche mit 'Delete' markiert sind, werden gel�scht
 *  Beschreibung:     
                      Wird nur f�r Bin -> XML ben�tigt und ganz am Schluss der Konvertierung 
                      (nach OnAfterConvert) aufgerufen.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.DelEventListNodes;
var
  i: Integer;
begin
  // Keine Nodes l�schen, wenn von aussen nicht gew�nscht
  if not(IgnoreDeleteFlag) then begin
    try
      if Assigned(mDelEventList) then begin
        try
          for i:= 0 to mDelEventList.Count-1 do begin
            // Grunds�tzlich ist das Tracking ausgeschaltet
            mTrackCurrentElement := false;
            if (Assigned(mDelEventList.MapItems[i])) then begin

              // Tracking f�r jedes zu l�schende Element �berpr�fen
              SetElementTracking(mDelEventList.MapItems[i]);
              if mTrackCurrentElement then
                CodeSite.SendMsgEx(csmLevel2, 'Tracking: Delete XML Node');

              // ==> MMXML-Node l�schen
              RemoveNode(mDelEventList.MMXMLItems[i]);
            end
            else begin
              WriteLogFile(etWarning, Format('DelEventListNodes: Couldn''t process event %d from mDelEventList, because it was not assigned! ',[i]));
            end; //if (Assigned(mEventList.MapItems[i]))
          end; //for
        finally
          freeAndNil(mDelEventList);
        end;// try finally
      end; //if Assigned(mDelEventList)
    except
      on e: Exception do begin
          raise EMMXMLConverter.Create(e.Message+' DelEventListNodes failed. ');
      end;
    end;
  end;// if not(IgnoreDeleteFlag) then begin
end;// TMMXMLConverter.DelEventListNodes cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.DoAfterDeleteElements(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
  DOMDocument40);
begin
  if Assigned(FOnAfterDeleteElements) then FOnAfterDeleteElements(Sender, aMapDoc, aMMXMLDoc);
end;// TMMXMLConverter.DoAfterDeleteElements cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.DoBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
  DOMDocument40);
begin
  if Assigned(FOnBeforeConvert) then FOnBeforeConvert(Sender, aMapDoc, aMMXMLDoc);
end;// TMMXMLConverter.DoBeforeConvert cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.DoBeforeDeleteElements(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
  DOMDocument40);
begin
  if Assigned(FOnBeforeDeleteElements) then FOnBeforeDeleteElements(Sender, aMapDoc, aMMXMLDoc);
end;// TMMXMLConverter.DoBeforeDeleteElements cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.DoCalcExternal(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
begin
  if Assigned(FOnCalcExternal) then FOnCalcExternal(Sender, aEventRec, aDeleteFlag);

//: ----------------------------------------------
  //: ----------------------------------------------
  if mDebugOutEnabled then
    CodeSite.SendFmtMsg('Jetzt ausf�hren: Abhandlen des EventReturnwertes: %s', [aEventRec.ArgumentsString]);

    //Returnwert
end;// TMMXMLConverter.DoCalcExternal cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.DoCalcInternal(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
begin
  if Assigned(FOnCalcInternal) then FOnCalcInternal(Sender, aEventRec, aDeleteFlag);
end;// TMMXMLConverter.DoCalcInternal cat:No category

//:-------------------------------------------------------------------
(*: Member:           EventsExists
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode)
 *
 *  Kurzbeschreibung: F�hrt ein Element, welches Event(s) ausf�hren muss, einer Liste zu.
 *  Beschreibung:     
                      Return=True, wenn ein Event f�r diesen MAP-Node existiert.
                      
                      False Bin->XML: F�hrt ein Element, welches Event(s) ausf�hren muss, einer Liste zu. 
                      Die Liste wird, falls noch nicht vorhanden erstellt.
                      Die Liste wird am Ende der Outputfile-Erstellung abgearbeitet.
 --------------------------------------------------------------------*)
function TMMXMLConverter.EventsExists(aMapNode, aMMXMLNode: IXMLDOMNode): Boolean;
var
  xTmpNodeList: IXMLDOMNodeList;
begin
  Result := False;

  try
    //Check ob Element Events enth�lt
    xTmpNodeList := aMapNode.selectNodes(cEleEvent);

    if not (xTmpNodeList.Length=0) then begin
      //Das Element enth�lt Childelemente, welche cEleEvent heissen

  //Alt    if mUp then begin //BinToXML
        if not Assigned(mEventList) then begin
          //Erstellen der LIste f�r die Events
          mEventList:= TMyMapMMXMLList.Create;
          //FreeAndNil dieser LIste passiert in ProcessEventNodeList
        end;

        //Add Element to EventList
        mEventList.Add(aMapNode, aMMXMLNode);
  //Alt    end; //if mUp
      Result := True;
    end;
  except
    on e: Exception do begin
      raise EMMXMLConverter.Create(e.message+' Error in EventsExists ');
    end;
  end;// try except
end;// TMMXMLConverter.EventsExists cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetDefaultFromMap
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aValue)
 *
 *  Kurzbeschreibung: Holt den Defaultwert des �bergebenen XML-Elements
 *  Beschreibung:     
                      Es wird der Wert des Attributes 'def' geholt.
 --------------------------------------------------------------------*)
function TMMXMLConverter.GetDefaultFromMap(aMapNode: IXMLDOMNode; out aValue: Variant): Boolean;
var
  xCount: Integer;
  xStr: Variant;
  xString: string;
  xAttribute: IXMLDOMNode;
begin
  aValue := 0;
  Result := False;
  //Element aus Map-File wurde in MM-XML-File nicht gefunden => Defaultwert suchen
  if AttributExists(cAtrDefault, aMapNode.attributes) then
  try
    xAttribute := aMapNode.attributes.getNamedItem(cAtrDefault);
    if assigned(xAttribute) then begin
      aValue := xAttribute.nodeValue;
      Result := True;
      case mActElementType of
        dtUCChar, dtUCBit: begin
          //Spezielle Behandlung, da im Mapfile als Default nur ein Character z.B. 'U' angegeben wird,
          //  muss dieser an dieser Stelle mit dem Wert vom cAtrCount vervielf�ltigt werden.
          if AttributExists(cAtrCount, aMapNode.attributes) then begin
            xCount := VarAsType(aMapNode.attributes.getNamedItem(cAtrCount).nodeValue, varInteger);
            xStr := (VarAsType(aValue, varString));
            xString := xStr;
            aValue := StringOfChar(xString[1], xCount); //String erstellen mit dem Default-Buchstaben
          end
          else begin
            WriteLogFile(etWarning, Format('Required XML attribute %s in Mapfile (Node:%s) is missing! ',[cAtrCount, GetElementFullXPath(aMapNode)]));
            Result := False;
          end; //if AttributExists(cAtrCount, aMapNode.attributes)
        end;

        // Konvertierung vornehmen, wenn der Dezimal Separator nicht '.' ist
        dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle: begin
          if not(AttributExists(cAtrEnum, aMapNode.attributes) or AttributExists(cAtrBitCount, aMapNode.attributes)) then
            aValue := MMStrToFloat(aValue);
        end;// dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle: begin

      else ;
      end;
    end; //if assigned(xAttribute)
  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' GetDefaultFromMap failed. ');
    end;
  end;
end;// TMMXMLConverter.GetDefaultFromMap cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetElementFullXPath
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aNode, aWithMapSection)
 *
 *  Kurzbeschreibung: REKURSIV!! Gibt den vollst�ndigen X-Path des �bergebenen Elements ab Rootelement zur�ck.
 *  Beschreibung:     
                      REKURSIV!! Gibt den vollst�ndigen X-Path des �bergebenen Elements ab Rootelement zur�ck.
 --------------------------------------------------------------------*)
function TMMXMLConverter.GetElementFullXPath(aNode: IXMLDOMNode; aWithMapSection: Boolean=False): string;
begin
  //  if not(AnsiSameText(aNode.NodeName, mMapSectionElement.NodeName)) then
  // Sections Element darf nicht im Pfad erscheinen
  if not(AttributExists(cAtrStructSize, aNode.attributes)) or aWithMapSection then
    result := '/'+aNode.nodeName;

  if (not(IsInterface(aNode.ParentNode, IXMLDOMDocument))) then begin
    //Noch nicht am Document Element angelangt
    if (assigned(aNode.ParentNode)) then
      result := GetElementFullXPath(aNode.ParentNode, aWithMapSection) + result
    else
      WriteLogFile(etWarning, Format('Problem detected while building X-path! Incomplete X-path is: %s',[result]));
  end;// if (not(IsInterface(aNode.ParentNode, IXMLDOMDocument))) then begin
end;// TMMXMLConverter.GetElementFullXPath cat:No category

//:-------------------------------------------------------------------
function TMMXMLConverter.GetNewTempFileName(aPrefix: string): string;
var
  xLength: Integer;
  xTempPath: string;
begin
  result := '';
  SetLength(xTempPath, MAX_PATH);
  // Gibt erstmal den Pfad zum Tempor�r Verzeichnis
  xLength := GetTempPath(MAX_PATH, PChar(xTempPath));
  if xLength > 0 then
    SetLength(xTempPath, xLength);
  // Ein Unterordner wird f�r das Millmaster erstellt
  xTempPath := xTempPath + 'MMConverter\';
  if not(DirectoryExists(xTempPath)) then
    ForceDirectories(xTempPath);

  SetLength(result, MAX_PATH);
  // Erzeugt einen neuen Filenamen
  GetTempFileName(PChar(xTempPath), PChar(aPrefix), 0, PChar(result));
end;// TMMXMLConverter.GetNewTempFileName cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetTypeFromString
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aNode)
 *
 *  Kurzbeschreibung: Wandelt den Typ aus einem XML-String ins entsprechende Element im TBasicType um
 *  Beschreibung:     
                      Wandelt den Typ aus einem XML-String ins entsprechende Element im TBasicType um.
                      
                      Ist zur Zeit PUBLIC, weil auch von Testumgebung gebraucht!!
 --------------------------------------------------------------------*)
function TMMXMLConverter.GetTypeFromString(aNode: IXMLDOMElement): TDataType;
var
  xAttributeValue: Variant;
  xBasicType: string;
  i: Integer;
begin
  // Basistyp des Datenelements lesen
  xAttributeValue := aNode.getAttribute(cAtrType);
  // Initialisieren
  result := dtNone;
  // Nur weiter, wenn das Attribut existiert
  if not(VarIsNull(xAttributeValue)) then begin
    // In einen String umwandeln
    xBasicType := xAttributeValue;
    i := ord(Low(TDataType));
    // Array mit den Namen der Datentypen solange durchsuchen, bis der Typ gefunden wurde
    while (i <= ord(High(TDataType))) and (result = dtNone) do begin
      // Wenn der Datentyp bekannt ist, dann den Typ zuweisen
      if AnsiSameText(cBasicTypeNames[TDataType(i)], xBasicType) then begin
        // Datentyp sichern
        result := TDataType(i);
      end;// if AnsiSameText(cBasicTypeNames[TBasicType(i)], xBasicType) then begin
      // N�chsten Datentyp untersuchen
      inc(i);
    end;// while (i <= ord(High(TDataType))) and (BasicType = btNone) do begin
  end; //if not(VarIsNull(xAttributeValue))
end;// TMMXMLConverter.GetTypeFromString cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetValueFromBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aLast)
 *
 *  Kurzbeschreibung: Liest einen Wert(Variant) aus dem Bin�rfile
 *  Beschreibung:     
                      Liest einen Wert(Variant) aus dem Bin�rfile. Auf Grund des Types werden
                      die entsprechende Anzahl Bytes ausgelesen und in aValue geschrieben.
 --------------------------------------------------------------------*)
function TMMXMLConverter.GetValueFromBin(aMapNode: IXMLDOMNode; out aLast: Boolean): Variant;
var
  xCurrentOffset: Cardinal;
  xByteCount: Cardinal;
  i: Cardinal;
  xString: string;
  xChar: Char;
  j: Integer;
  xCutFlag: Char;
  xUncutFlag: Char;
  xRestBitCount: Integer;
  xBitCount: Integer;
  xWideStr: WideString;

  function IsSet(aBitNr: Integer; aValue: Byte): Boolean;
  begin
    if aBitNr > 7 then raise EMMXMLConverter.Create('Nr. of Bit out of range.');
    Result := boolean((1 shl aBitNr) and aValue);
  end;

begin
    //Initialisierung
  aLast := False;

//: ----------------------------------------------
  //: ----------------------------------------------
  if Assigned(mBinDoc) then begin
      //Offsetwert lesen (Attribute ist vorhanden wurde vorg�ngig gecheckt)
    try
      xCurrentOffset := StrToInt((aMapNode as IXMLDOMElement).getAttribute(cAtrOffset));
    except
      raise EMMXMLConverter.Create(Format('Convert error (StrToInt) XMLMap: Element:%s Attribute:%s Value:%s',[GetElementFullXPath(aMapNode),cAtrOffset,(aMapNode as IXMLDOMElement).getAttribute(cAtrOffset)]));
    end; //try

    try
      mActElementType := GetTypeFromString(aMapNode as IXMLDOMElement);
        // Je nach Basistyp die Entsprechende Pointer Akrobatik vornehmen
      case mActElementType of
        dtNone: result := '';

          // Das Datenelement(e) ist/sind ein Alphanummerisches Zeichen (#0..#256)
        dtChar: begin
            //Anzahl zu lesende Bytes holen (wenn kein 'count' dann 1)
          xByteCount := AttributeToIntDef(cAtrCount, aMapNode, 1);
          xString := '';

          for i:= 1 to xByteCount do begin
            xChar := chr(PByte(cardinal(mBinDoc) + xCurrentOffset+i-1)^);
            if ORD(xChar)=0 then
                //Abbruchkriterium
              BREAK; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            if xChar >= ' ' then begin
                // Zur Ausgabe hinzuf�gen, wenn das Zeichen kein Steuerzeichen ist
              xString := xString+xChar;
            end
            else begin
                // Steuerzeichen k�nnen im XML-Output nicht dargestellt werden und m�ssen maskiert werden
              xString := xString+'#';
              xString := xString+IntToStr(ord(xChar));
            end; //if xChar >= ' '
          end; //for i:= 1 to xByteCount

          aLast := True; //Dieser Datentyp muss nicht mehr weiter bearbeitet werden
          result := xString;
        end;// btChar

        dtUTF16: begin
         xByteCount := AttributeToIntDef(cAtrCount, aMapNode, -1) * 2;
         SetLength(xWideStr, xByteCount div 2);
         System.Move(Pointer(cardinal(mBinDoc) + xCurrentOffset)^, xWideStr[1], xByteCount);
         Result := xWideStr;
        end;// dtWideChar: begin

        dtUCChar: begin
            //Anzahl zu lesende Bytes holen (wenn kein 'count' dann 1)
          xByteCount := AttributeToIntDef(cAtrCount, aMapNode, 1);
          xString := '';

          for i:= 1 to xByteCount do begin
            xChar := chr(PByte(cardinal(mBinDoc) + xCurrentOffset+i-1)^);
            if (xChar = 'c') or (xChar = 'C') then begin
                // Zur Ausgabe hinzuf�gen, wenn das Zeichen kein Steuerzeichen ist
              xString :=  xString+cCutFlag;
            end
            else begin
                // Steuerzeichen k�nnen im XML-Output nicht dargestellt werden und m�ssen maskiert werden
              xString := xString+cUncutFlag;
            end; //if xChar >= 'c'
          end; //for i:= 1 to xByteCount
          aLast := True; //Dieser Datentyp muss nnicht mehr weiter bearbeitet werden
          result := xString;
        end;// btUCChar

          // Das Datenelement ist ein Byte (0..256)
        dtByte: begin
          result := PByte(cardinal(mBinDoc) + xCurrentOffset)^;
        end;// btByte

          // Das Datenelement ist ein Shortint (-128..127)
        dtShortInt: begin
          result := PShortInt(cardinal(mBinDoc) + xCurrentOffset)^;
         end;// btShortInt

          // Das Datenelement ist ein Word (0..65535)
        dtWord: begin
          result := PWord(cardinal(mBinDoc) + xCurrentOffset)^;
        end;// btWord

          // Das Datenelement ist ein Smallint (-32768..32767	)
        dtSmallInt: begin
          result := PSmallInt(cardinal(mBinDoc) + xCurrentOffset)^;
        end;// btSmallInt

          // Das Datenelement ist ein Longword (0..4294967295)
        dtDWord: begin
            {$IFDEF VER130}
            result := Integer(PDWord(cardinal(mBinDoc) + xCurrentOffset)^);
            {$ELSE}
            Bei Delphi5 kann der Variant Datentyp maximal einen Integer aufnehhmen. Bei der Zuweisung
            eines DWords an einen Variant wird dieser von Delphi bereits auf einen Integer gecastet.
            Bei h�hren Delphi Versionen k�nnte diese Schranke fallen. Deshalb wird die Compilerversion
            abgefragt. (Lok 25.11.2005)
            {$ENDIF}
        end;// btLongInt

          // Das Datenelement ist ein Longint (-2147483648..2147483647)
        dtLongInt: begin
          result := PLongint(cardinal(mBinDoc) + xCurrentOffset)^;
        end;// btLongInt

        //       // Das Datenelement ist ein Int64 (-9223372036854775808..9223372036854775808)
        //       dtInt64: begin
        //         result := result + xSpace + IntToStr(PInt64(cardinal(aBase) + xCurrentOffset)^);
        //         inc(xCurrentOffset, SizeOf(Int64));
        //       end;// btInt64

          // Das Datenelement ist ein Singele (1.5 x 1045 .. 3.4 x 1038)
        dtSingle: begin
            (* HexDarstellung C8 00 02 01 --> 1 10010000 00000000000001000000001
               Vorzeichen = 1   (1 = positiv, 0 = negativ)
               Exponent   = 10010000 2^(e - 127) = 2^(144-127) = 2^17)

               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
               0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0  0  0  1

               Mantisse   = 00000000000000111110100 (1,m = 1+ 2^-14 + 2^-23
                                                         = 1.00006115436553955078125)

               Fliesskommazahl = (-1) * 2^17 * 1.00006115436553955078125 = -131080.015625
            *)
            result := MMFloatToStr(VarAsType(PSingle(cardinal(mBinDoc) + xCurrentOffset)^, varSingle));
        end;// btSingle

          // Das Datenelement ist ein Double (5.0 x 10324 .. 1.7 x 10308)
        dtDouble: begin
            (* HexDarstellung C8 00 02 01 F4 00 00 00 --> 1 10010000000 0000000000100000000111110100000000000000000000000000
               Vorzeichen = 1   (1 = positiv, 0 = negativ)
               Exponent   = 10010000000 (2^(e - 1023) = 2^(1152-1023) = 2^129)

               1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
               0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0  0  0  1  1  1  1  1  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0

               Mantisse   = 00000000000000111110100 (1,m = 1 + 2^-11 + 2^-20 + 2^-21 + 2^-22 + 2^-23 + 2^-24 + 2^-26
                                                         = 1.00049014389514923095703125)

               Fliesskommazahl = (-1) * 2^129 * 1.00049014389514923095703125 = -6.8089830849142338412437666333181e+38
            *)
            result := MMFloatToStr(VarAsType(PDouble(cardinal(mBinDoc) + xCurrentOffset)^, varDouble));
        end;// btDouble

          // Das Datenelement ist ein ucBit (True/False Array of bits)
        dtUCBit: begin
          xString := '';

            //Logik festlegen und BitCout holen
          InitucBitNode(aMapNode, xCutFlag, xUnCutFlag, xBitCount, xRestBitCount, xByteCount);

          for i:= 1 to xByteCount do begin
            for j := 0 to 7 do begin
              if ((i=xByteCount) and (xRestBitCount>0) and (j>=xRestBitCount)) then  //Abbruchkriterium, wenn Restbits vorhanden
                BREAK;  //Abbruch  der for-Schlaufe
              if IsSet(j, (PByte(cardinal(mBinDoc) + xCurrentOffset+i-1)^)) then
                xString := xString+xCutFlag
              else
                xString := xString+xUncutFlag;
            end; //for j := 0 to 7
          end; //for i := 0 to xByteCount

          aLast := True; //Dieser Datentyp muss nnicht mehr weiter bearbeitet werden
          result := xString;
         end;// dtUCBit

      end;// case aAttributeRec of
    except
      on e: Exception do begin
          raise EMMXMLConverter.Create(e.Message+' GetValueFromBin failed. ');
      end;
    end;
  end; //if Assigned(mBinDoc)
end;// TMMXMLConverter.GetValueFromBin cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetValueFromMMXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLParentNode, aMMXMLNode, aValue, aLast)
 *
 *  Kurzbeschreibung: Liest einen Wert(Variant) aus dem MMXML
 *  Beschreibung:     
                      N�chstes Element aus dem Map-File suchen und entsprechenden Wert aus dem MMXML holen.
 --------------------------------------------------------------------*)
function TMMXMLConverter.GetValueFromMMXML(aMapNode, aMMXMLParentNode: IXMLDOMNode; out aMMXMLNode: IXMLDOMNode; out
  aValue: Variant; out aLast: Boolean): Boolean;
var
  xNodeList: IXMLDOMNodeList;
  i: Integer;
  xElementFound: Boolean;
begin
    //Initialisierung
  result := False;

  aLast := False;
  xElementFound := False;
  aValue := 0; //Wert mit 0 initialisieren
  aMMXMLNode := Nil;

//: ----------------------------------------------
  //: ----------------------------------------------
  try
    if not(aMMXMLParentNode=NIL) then begin
        //Schauen ob das entsprechende Map-Element im MM-XML vorhanden ist
      xNodeList := aMMXMLParentNode.childNodes;
      i := 0;
      while ((i<xNodeList.length) and (Not xElementFound)) do begin
        if xNodeList.item[i].nodeName = aMapNode.nodeName then begin
            //Element aus Map-File in MM-XML-File gefunden

          aMMXMLNode := xNodeList.item[i]; //Den gefundenen childNode zur�ckgeben
          aValue :=GetElementValueDef(xNodeList.item[i],0);

          xElementFound := True;
        end; //if xNodeList.item[i].nodeName = aMapNode.nodeName
        INC(i);
      end; //while
    end; //if not(aMMXMLParentNode=NIL)

    mActElementType := GetTypeFromString(aMapNode as IXMLDOMElement);

    if not xElementFound then begin
        // Tracking eines bestimmten Elements (siehe SetElementTracking())
      if mTrackCurrentElement then
        CodeSite.SendMsgEx(csmLevel2, 'Tracking: Need default value');

        //Element aus Map-File wurde in MM-XML-File nicht gefunden => Defaultwert suchen
      if not GetDefaultFromMap(aMapNode, aValue) then
          //Kein Wert gefunden, es wird kein Wert ins Bin�rfile eingetragen => ABBRUCH f�r dieses Element
        EXIT;

        // Tracking eines bestimmten Elements (siehe SetElementTracking())
      if mTrackCurrentElement then
        CodeSite.SendStringEx(csmLevel2, 'Tracking: Default Value', aValue);
    end; //if not xElementFound

    CheckMMXMLPlausability(aValue,mActElementType,aLast);

      // Konvertierung vornehmen, wenn der Dezimal Separator nicht '.' ist
    if mActElementType in [dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle] then begin
      if not(AttributExists(cAtrEnum, aMapNode.attributes) or AttributExists(cAtrBitCount, aMapNode.attributes)) then begin
        if not((AnsiSameText(aValue, 'true')) or (AnsiSameText(aValue, 'false'))) then
          aValue := MMStrToFloat(aValue);
      end;// if not(AttributExists(cAtrEnum, aMapNode.attributes) or AttributExists(cAtrBitCount, aMapNode.attributes)) then begin
    end;// if mActElementType in [dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle] then begin

    result := true;
  except
    on e: Exception do begin
        SendMessage(Format('GetValueFromMMXML: XML-Element %s, Value: %s ',[GetElementFullXPath(aMapNode), aValue]));
        raise EMMXMLConverter.Create(e.Message+' GetValueFromMMXML failed. ');
    end;
  end;
end;// TMMXMLConverter.GetValueFromMMXML cat:No category

//:-------------------------------------------------------------------
function TMMXMLConverter.GetXMLDeclaration: string;
begin
  Result := mMMXML.xml;
end;// TMMXMLConverter.GetXMLDeclaration cat:No category

//:-------------------------------------------------------------------
(*: Member:           HandleElementAttributeNodesBinToXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode)
 *
 *  Kurzbeschreibung: Behandelt die Attribute in einem Element Knoten (Bin->XML)
 *  Beschreibung:     
                      Check ob Element ein OFFSET und ein TYPE Attribut enth�lt.
                      Wert aus Bin�rfile holen und die m�glichen Attributetypen behandeln.
                      Der eigentliche Wert wird als Textnode im MMXML eingef�gt.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.HandleElementAttributeNodesBinToXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
var
  xLast: Boolean;
  xActElementValue: Variant;
  xValueFound: Boolean;
  xIgnore: Boolean;
  xRemoveNode: Boolean;
  xAtrValue: string;
  xNodeList: IXMLDOMNodeList;
  i: Integer;
  xMMXMLElement: IXMLDOMElement;
begin
    //Initialisierung
  xLast := False;
  xValueFound := False;
  xIgnore := False;
  xRemoveNode := False;

  if mDebugOutEnabled then
    EnterMethod('HandleElementAttributeNodesBinToXML');

//: ----------------------------------------------
  //: ----------------------------------------------
  try
      //Check ob 'type'- und 'offset'-Attribut existieren
    if not TypeAndOffsetAtrExists(aMapNode, xIgnore) then
      EXIT;

      //Falls ein Element nur f�r den Dowmload XML->Bin markiert ist, wird es zum L�schen angezeigt
    if GetAttributeAsString(cAtrDirection, aMapNode, xAtrValue) then  //'dir' Attribut ist vorhanden
      if (xAtrValue=cDirNames[cDirDown]) then
        xRemoveNode := True;

    if not xRemoveNode then begin
        //Wert aus Bin�rfile holen
      if (mBinDoc<>NIL) then begin
        if (not xIgnore) then begin
            //Normalfall: Wert aus dem Bin�rfile holen
          xActElementValue := GetValueFromBin(aMapNode, xLast);
          if not xLast then begin //Diese Datentypen m�ssen weiter bearbeitet werden
              //Check aller verschiedenen M�glichkeiten
            CheckSequenceBinToXML(aMapNode,aMMXMLNode,xActElementValue);
          end; //if xLast
          xValueFound := True;
        end;// if (not xIgnore) then begin
      end
      else begin
          //Sonderfall: Keine Bin�rfile �bergeben => Defaultwert aus MAP-File holen
        mActElementType := GetTypeFromString(aMapNode as IXMLDOMElement); //Actuellen Elementtype der Membervariablen zuweisen
        if GetDefaultFromMap(aMapNode, xActElementValue)then begin
            //Checken allf�lliger Facets (Keine Formeln anwenden, da ja bereits XML Werte)
          CheckFacets(aMapNode,aMMXMLNode,xActElementValue);

            // Mehrere Attribute ins MMXML kopieren
          xNodeList := aMapNode.SelectNodes(Format('@%s | @%s | @%s |@%s', [cAtrBitNr, cAtrConfigCode, cAtrConfigCodeInverted, cAtrEnum]));

            // Die folgende Operation kann nur auf ein Element angewendet werden
          Supports(aMMXMLNode, IXMLDOMElement, xMMXMLElement);
          if assigned(xMMXMLElement) then begin
              // Jedes einzelne Attribut aus der Liste kopieren und ins MMXML einf�gen
            for i := 0 to xNodeList.Length - 1 do
              xMMXMLElement.setAttributeNode((xNodeList[i].CloneNode(true)) as IXMLDOMAttribute);
          end;// if assigned(xMMXMLElement) then begin

          xValueFound := True;
        end
        else begin
            //L�schen des Nodes, da kein g�ltiger Wert gefunden werden konnte
          xRemoveNode := True;
        end;  //if GetDefaultFromMap(aMapNode, xActElementValue)
      end; //if (mBinDoc<>NIL)

      if xValueFound then begin
        if mTrackCurrentElement and (not(VarIsNull(xActElementValue))) then
          CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue', xActElementValue);

          //Der eigentliche Wert muss als Textnode zugef�hrt werden -> TextNode kreieren
        aMMXMLNode.appendChild(mMMXML.createNode(NODE_TEXT,'',''));
          //Wert dem MMXML-Element im DOM-Baum zuweisen
        if (vartype(xActElementValue) in [varSmallint, varInteger, varSingle, varDouble, varCurrency, varByte]) then
           aMMXMLNode.childNodes.item[0].nodeValue := FloatVariantToString(xActElementValue)
        else
           aMMXMLNode.childNodes.item[0].nodeValue := xActElementValue;

          //Check Events
        EventsExists(aMapNode, aMMXMLNode);
      end; //if xValueFound
    end; //if not xRemove

      //L�schen des zuvor erstellten Elements in MMXML, sofern gekennzeichnet
    if xRemoveNode then begin
      aMMXMLNode.parentNode.removeChild(aMMXMLNode);
    end; //if xRemove
  except
    on e: Exception do begin
      SendMessage(Format('%s.HandleElementAttributeNodesBinToXML: Error handling XMLElement:%s ',[Self.Classname, GetElementFullXPath(aMapNode)]));
      raise EMMXMLConverter.Create(e.message+Format(' Error handling Map-XMLElement:<%s> ',[GetElementFullXPath(aMapNode)]));
    end;
  end;
end;// TMMXMLConverter.HandleElementAttributeNodesBinToXML cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.HandleElementAttributeNodesXMLToBin(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode);
var
  xActElementValue: Variant;
  xLast: Boolean;
  xMMXMLChildNode: IXMLDOMNode;
  xValueFound: Boolean;
  xIgnore: Boolean;
  xAtrValue: string;
  xWriteElementToBin: Boolean;
  xOverwrite: Boolean;
  xBitCount: Integer;
  xBitOffset: Integer;
begin
    //Initialisierung
  xValueFound := False;
  xIgnore := False;
  xOverwrite := True;
  xWriteElementToBin := True; //Nue:22.3.05

//: ----------------------------------------------
  //: ----------------------------------------------
  if mDebugOutEnabled then
    EnterMethod('HandleElementAttributeNodesXMLToBin');

  try  //G�ltiges Element im Map-File
      //Check ob 'type'- und 'offset'-Attribut existieren
    if not TypeAndOffsetAtrExists(aMapNode, xIgnore) then
      EXIT;

      //Falls ein Element nur f�r den Upload Bin->XML markiert ist, wird es nicht behandelt (nicht ins Binfile eingef�gt)
    if GetAttributeAsString(cAtrDirection, aMapNode, xAtrValue) then  //'dir' Attribut ist vorhanden
      if (xAtrValue=cDirNames[cDirUp]) then
        EXIT;

      //Behandeln aller verschiedener Elementtypen
    if not xIgnore then
        //Wert aus MMXML-File holen, wird der Wert im MMXML nicht gefunden und besteht im MAPFile kein Defaultwert,
        //  wird der Wert nicht behandelt und �bersprungen (nur allf�llige Events werden noch ausgel�st).
      xValueFound := GetValueFromMMXML(aMapNode, aMMXMLNode, xMMXMLChildNode, xActElementValue, xLast);

      //Check ob Element Events enth�lt
    if EventsExists(aMapNode,xMMXMLChildNode) then begin
        //Abhandlen des Events
      if (aMMXMLNode<>Nil) and (mDebugOutEnabled) then
        codesite.SendFmtMsg('%s.HandleElementAttributeNodesXMLToBin: Element %s starts Eventhandling.',[Self.Classname,GetElementFullXPath(aMMXMLNode)]);
        //Zusammenbauen der EventsStruktur, Events ausl�sen und Abhandeln

      xActElementValue := ProcessEvent(aMapNode,xMMXMLChildNode, xActElementValue, cEventExt, xWriteElementToBin);
    end; //if EventsExists(aMapNode,aMMXMLNode)

    if xValueFound and (xWriteElementToBin) then begin
      if mTrackCurrentElement and (not(VarIsNull(xActElementValue))) then
        CodeSite.SendStringEx(csmLevel2, 'Tracking: ElementValue before checking', xActElementValue);

      if not xLast then begin //Diese Datentypen m�ssen weiter bearbeitet werden
         //Checken allf�lliger Facets
        CheckFacets(aMapNode,xMMXMLChildNode,xActElementValue);

           //Checken allf�lliger Enums
        CheckEnum(aMapNode,xMMXMLChildNode,xActElementValue);

          //Checken allf�lliger Converts
        CheckConvert(aMapNode,xActElementValue);

          //Checken allf�lliger Biffields
        CheckBitfield(aMapNode,xActElementValue, xOverwrite, xBitCount, xBitOffset);
      end; //if xLast

        //Wert ins Bin�r-File schreiben
        // Ist xOverwrite=True �berschreibt xActElementValue den Wert an der entsprechenden Speicherstelle,
        // andernfalls wird der bestehende Wert mit xActElementValue bitweise "verodert"
      WriteValueToBin(aMapNode, xActElementValue, xOverwrite, xBitCount, xBitOffset);
    end; //if xValueFound

  except
    on e: Exception do begin
      SendMessage(Format('%s.HandleElementAttributeNodesXMLToBin: Error handling XMLElement:%s %s',[Self.Classname, GetElementFullXPath(aMapNode),aMapNode.xml]));
      raise EMMXMLConverter.Create(e.Message+Format(' Error handling Map-XMLElement:<%s> %s',[GetElementFullXPath(aMapNode),aMapNode.xml]));
    end;
  end;
end;// TMMXMLConverter.HandleElementAttributeNodesXMLToBin cat:No category

//:-------------------------------------------------------------------
(*: Member:           InitucBitNode
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aCutFlag, aUnCutFlag, aBitCount, aRestBitCount, aByteCount)
 *
 *  Kurzbeschreibung: Initialisiert ein XML-Element vom Typ dtUCBit
 *  Beschreibung:     
                      Falls conv='not' im MAP vorhanden ist, wird die Logik gekehrt, andernfalls nicht.
                      Anzahl Bits, Bytes und Bits in angefressenem Byte werden bestimmt und zur�ckgebgeben.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.InitucBitNode(aMapNode: IXMLDOMNode; out aCutFlag, aUnCutFlag: Char; out aBitCount,
  aRestBitCount: Integer; out aByteCount: Cardinal);
begin
  //Ist eine Negation conv='not' als Attribut vorhanden? Ist nur f�r UCBit relevant
  //  darum wird das hier abgehandelt!
  if (AttributeToStringDef(cAtrConvert, aMapNode, '')=cFunctionNames[cConvertNot]) then begin
    //Logik umkehren
    aUncutFlag := cCutFlag;
    aCutFlag   := cUncutFlag;
  end
  else begin
    //Logik beibehalten
    aUncutFlag := cUncutFlag;
    aCutFlag   := cCutFlag;
  end; //if (AttributeToIntDef(cAtrConvert, aMapNode, '')=cFunctionNames[cConvertNot

  //Anzahl zu lesende Bits holen (wenn kein 'count' dann 1)
  aBitCount := AttributeToIntDef(cAtrCount, aMapNode, 1);

  //Anzahl zu lesende Bytes bestimmen
  aByteCount := (aBitCount div 8);
  aRestBitCount := (aBitCount mod 8);

  if aRestBitCount>0 then //Auch angefressenes Byte lesen
    INC(aByteCount);
end;// TMMXMLConverter.InitucBitNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadXMLMap
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aXMLMap)
 *
 *  Kurzbeschreibung: L�dt das Map-File in das XMLMap Interface ein
 *  Beschreibung:     
                      Beim laden wird das MapFile geparst und als Objektstruktur im
                      Speicher abgelegt (DOM).
 --------------------------------------------------------------------*)
function TMMXMLConverter.LoadXMLMap(const aXMLMap: string): Boolean;
begin
    // XMLMap freigeben (Interface)
  mXMLMap := nil;

//: ----------------------------------------------
  //: ----------------------------------------------
    // XML Dokument (MSXML) f�r die Beschreibung der Datenstruktur erzeugen
  mXMLMap := DOMDocumentMMCreate ;
  SetParser(mXMLMap);
    (*mXMLMap.setProperty('NewParser', True);
    mXMLMap.preserveWhiteSpace := true;*)

  if assigned(mXMLMap) then
      // Stream in das Dokument laden und parsen
    result := mXMLMap.loadXML(aXMLMap)
  else
    raise EMMXMLConverter.Create('XML Map-File could not be generated! ');
end;// TMMXMLConverter.LoadXMLMap cat:No category

//:-------------------------------------------------------------------
(*: Member:           MapBinToXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Geht durch alle Nodes des XML-Map-Files und f�llt die Werte aus dem Bin-File ins Output-MMXML
 *  Beschreibung:     
                      Geht durch alle Nodes des XML-Map-Files und f�llt die Werte aus dem Bin-File ins Output-MMXML
 --------------------------------------------------------------------*)
function TMMXMLConverter.MapBinToXML: Boolean;
var
  xRootNode: IXMLDOMNode;
  xAttributeValue: Variant;

  //             // Traversiert den Knoten mit der Beschreibung der Daten
  //             with TMSXMLStructWalker.Create do begin
  //               // Erzeugt jeweils ein neues Attribut und "h�ngt" es an den Knoten 'mObjectStructNode'
  //               OnNewDataElement := NewXMLDataElement;
  //               // Startet den Durchlauf
  //               Process(xStructNode);
  //             end;// with TMSXMLStructWalker.Create do begin

begin
  try
    //L�nge des erwarteten Bin�rfiles lesen
    xAttributeValue := mMapSectionElement.getAttribute(cAtrStructSize);
    if not(VarIsNull(xAttributeValue)) then begin
      //L�nge des erwarteten Bin�rfiles darf nicht l�nger als effiktives Bin�rfile sein
      if (VarAsType(xAttributeValue,varInteger)>mBinLen) and (mBinDoc<>NIL) then
        raise EMMXMLConverter.Create(Format('Binfile size (%d Bytes) is smaller than estimated size by Mapfile (%s Bytes)! ',[mBinLen,VarAsType(xAttributeValue,varInteger)]));

      // Die Outputdaten werden im Knoten 'LoepfeBody' gespeichert
      xRootNode := mMMXML.documentElement; //Erstellen RootNode im Outputdokument
      if mDebugOutEnabled then
        codesite.SendFmtMsg('%s.MapXMLToBin: mMMXML.xml: %s',[Self.Classname, mMMXML.xml]);

      //Parsen der Nodes unter dem Rootelement
      MapLevelXML(mMapSectionElement, xRootNode);
      //Abarbeiten der EventNodeList
      ProcessEventNodeList;
      Result := True;
    end
    else begin
      raise EMMXMLConverter.Create('Required attribut <structSize> in Mapfile not found! ');
    end; //if not(VarIsNull(xAttributeValue))
  except
    on e:EMMXMLConverter do begin
      raise EMMXMLConverter.Create(e.Message+' MapBinToXML failed. ');
    end;
  end;// try except
end;// TMMXMLConverter.MapBinToXML cat:No category

//:-------------------------------------------------------------------
(*: Member:           MapLevelXML
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode, aRootCall)
 *
 *  Kurzbeschreibung: REKURSIV! Mappt pro Aufruf einen Level des MapFiles
 *  Beschreibung:     
                      Beim der weiteren Verarbeitung (SubCalls) wird jeweils das entsprechende Map-Element mitgegeben. 
                      Bei XML->Bin wird vom MMXML File der Parent des entsprechenden Elements mitgegeben.
                      Dadurch wird die Reihenfolge der Elemente zwischen dem Map und dem MMXML 
                      irrelevant.
                      Bei Bin->XML wird das dem MAP-Element entsprechende Element im MMXML erstellt 
                      und mitgegeben
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.MapLevelXML(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; aRootCall:Boolean=False);
var
  xMapLen: Integer;
  xChildNodes: IXMLDOMNodeList;
  i: Integer;
  xNamedNodeMap: IXMLDOMNamedNodeMap;
  xNewChildNode: IXMLDOMNode;
  xRootCall: Boolean;
  xParentNode: IXMLDOMNode;
begin
  if mDebugOutEnabled then
    EnterMethod('MapLevelXML');
  xRootCall := aRootCall;
  if codesite.Enabled and mDebugOutEnabled then begin
    codesite.SendMsg(FormatXML((aMapNode as IXMLDOMElement), 'MapXML-Node: '));
    codesite.SendMsg(FormatXML((aMMXMLNode as IXMLDOMElement), 'MMXML-Node: '));
  end; //if codesite.Enabled
  xMapLen := aMapNode.childNodes.Length;  //L�nge f�r die FOR-Schleife holen
    //xMapLen := aMapNode.selectNodes('child::*').Length;  //L�nge f�r die FOR-Schleife holen

//: ----------------------------------------------
  //: ----------------------------------------------
    // Wenn das vorherige Element getrackt wurde
  if mTrackCurrentElement then
    CodeSite.SendMsgEx(csmLevel1, 'Tracking: Stop');

    // Setzt das "Tracking"-Flag wenn in der Registry so eingestellt
  SetElementTracking(aMapNode);

//: ----------------------------------------------
  //: ----------------------------------------------
  if (xMapLen > 0) and ((aMapNode.selectNodes(cEleEvent)).Length=0) then begin //Kinder sind vorhanden und keine Subelemente 'event'
    //Durch alle Siblings gehen
    for i:= 0 to (xMapLen-1) do begin
      xNamedNodeMap := aMapNode.attributes;
      if (xNamedNodeMap.length=0) or (aMapNode<>mXMLMap.documentElement) then begin
          //Keine Attribute gefunden, somit kein Blatt
          //Weitere Kinder: Rekursion eine Ebene absinken
          //Aufruf von Childelement
        if i=0 then begin //FirstChild
            //Child liste erstellen
          xChildNodes := aMapNode.childNodes;
        end;
        if mUp {Bin to XML}then begin
            // Das erste ChildElement des Map-File im Outputdokument erstellen
          xNewChildNode := aMMXMLNode.appendChild(mMMXML.createElement(xChildNodes.item[i].nodeName));
          MapLevelXML(xChildNodes.item[i], xNewChildNode);  //REKURSIVER Aufruf !!!!!!!!!!!!
        end
        else begin {XML to Bin}
          if xRootCall then begin //Sonderbehandlung bei erstem Aufruf aus Root
            xRootCall := False;
            MapLevelXML(xChildNodes.item[i], aMMXMLNode);  //REKURSIVER Aufruf !!!!!!!!!!!!
          end
          else begin
            if aRootCall then begin
              xParentNode := aMMXMLNode;
            end
            else begin
              if aMMXMLNode=Nil then
                xParentNode := Nil
              else
                xParentNode := aMMXMLNode.selectSingleNode(aMapNode.nodeName);
            end; //if aRootCall
            MapLevelXML(xChildNodes.item[i], xParentNode);  //REKURSIVER Aufruf !!!!!!!!!!!!
      //        MapLevelXML(xChildNodes.item[i], aMMXMLNode.firstChild);  //REKURSIVER Aufruf !!!!!!!!!!!!
          end; //if mRootCall
        end; //if aUp then
      end
      else begin
          //Attribute gefunden, somit Element behandeln
        if mUp then
          HandleElementAttributeNodesBinToXML(aMapNode, aMMXMLNode)
        else
          HandleElementAttributeNodesXMLToBin(aMapNode, aMMXMLNode);
      end; //if (xNamedNodeMap.length=0) then begin
    end; //for
  end
  else begin
      //Keine Kinder gefunden, somit Element behandeln
    if mUp then
      HandleElementAttributeNodesBinToXML(aMapNode, aMMXMLNode)
    else
      HandleElementAttributeNodesXMLToBin(aMapNode, aMMXMLNode);
  end; //If xMapLen > 0
end;// TMMXMLConverter.MapLevelXML cat:No category

//:-------------------------------------------------------------------
(*: Member:           MapXMLToBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapSection, aBinDoc, aBinDocLen)
 *
 *  Kurzbeschreibung: Geht durch alle Nodes des XML-Map-Files und f�llt die Werte aus dem Input-MMXML ins Output Bin-
 File
 *  Beschreibung:     
                      Geht durch alle Nodes des XML-Map-Files und f�llt die Werte dem Input-MMXML ins Output Bin-File,
                      sofern er dort existiert. Andernfalls wird ein Defaultwert oder (Standardwert) eigef�llt.
 --------------------------------------------------------------------*)
function TMMXMLConverter.MapXMLToBin(const aMapSection: TMapSection; out aBinDoc: PByte; out aBinDocLen: Integer):
  Boolean;
var
  xAttributeValue: Variant;
  xTempBinDoc: PByte;
begin
  Result := False;
  xTempBinDoc := aBinDoc;
  aBinDoc := NIL;

  try
    SetMapSection(aMapSection); //Sets the decided MapSection as Rootelement

    //L�nge des Bin�rfiles lesen und Outputdokument erzeugen
    xAttributeValue := mMapSectionElement.getAttribute(cAtrStructSize);
    if not(VarIsNull(xAttributeValue)) then begin
      // L�nge zuweisen
      aBinDocLen := xAttributeValue;
      // reserviert den Speicher
      // Allozieren und initialisieren
      if mCreateBufferForXMLToBin then begin
        mBinLen := aBinDocLen; //Wird f�r Checks (z.B. ob Offsetwert innerhalb der Struktur liegt in TypeAndOffsetAtrExists) ben�tigt.
        aBinDoc := AllocMem(aBinDocLen);
        mBinDoc := aBinDoc; //MemberVariable beschreiben
      end else begin
        if mBinLen < aBinDocLen then
          raise EMMXMLConverter.CreateFMT('Buffer %d to small (%d)', [mBinLen, aBinDocLen]);
        mBinDoc := xTempBinDoc;
      end;// if mCreateBufferForXMLToBin then begin

      //Parsen der Nodes unter dem Rootelement
      MapLevelXML(mMapSectionElement, mMMXML.documentElement, True);

      //Abarbeiten der EventNodeList (nur die internen Events)
      //  Externe Events wurden jeweils beim Element verarbeitet
      ProcessEventNodeList(cEventInt);

      Result := True;

    end; //if xBinFileLen <> 0

  except
    on e:EMMXMLConverter do begin
      raise EMMXMLConverter.Create(e.Message+' MapXMLToBin failed. ');
    end;
  end;// try except
end;// TMMXMLConverter.MapXMLToBin cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.NewInternalBinToXMLEvent(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag:
  Boolean);
begin
  ;
end;// TMMXMLConverter.NewInternalBinToXMLEvent cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.NewInternalXMLToBinEvent(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag:
  Boolean);
var
  xMapNode: IXMLDOMNode;
  xEnumIndex: Integer;
  xStr: string;
begin
  //Value wurde zugewiesen
  if NOT (VarIsNull(aEventRec.Value)) then begin
    // Zuerst versuchen das Element zu selektieren
  //alt  xMapNode := mXMLMap.SelectSingleNode(aEventRec.CalcElementName);
    //MapSection zuf�gen um x-Pfad f�r MapElement zu erstellen
    xStr := aEventRec.CalcElementName;
    Delete(xStr,1,Length(cEleLoepfeBody)+1);
    xMapNode := mXMLMap.SelectSingleNode(cMapSectionNames[FMapSection]+xStr);
    if (assigned(xMapNode)) then begin
      if AttributExists(cAtrEnum, xMapNode.attributes) then begin
      //Ein Attribute 'enum' existiert
        with TStringList.Create do try
          commaText := AttributeToStringDef(cAtrEnum, xMapNode, '');
          xEnumIndex := IndexOfName(aEventRec.Value);
          //Suchen des XML-Namens aEventRec.Value
          if xEnumIndex>=0 then begin
            //XML-Name gefunden; schauen ob der Wert davon NICHT 'others'ist
            if not AnsiSameText(Values[Names[xEnumIndex]], cEnumOthers) then begin
              mActElementType := GetTypeFromString(xMapNode as IXMLDOMElement); //Setzen des Elementtyp
              WriteValueToBin(xMapNode, Values[Names[xEnumIndex]], True);  //Ueberschreiben der Zelle mit neuem Wert
            end; //if not AnsiSameText(Values[Names[i]], cEnumOthers)
          end; //if
        finally
          free;
        end;// with TStringList.Create do try
      end; //if AttributeExists(cAtrEnum, xNode.attributes)
    end; //if (assigned(xNode)) then begin
  end; //if NOT (VarIsNull(aEventRec.Value)) then
end;// TMMXMLConverter.NewInternalXMLToBinEvent cat:No category

//:-------------------------------------------------------------------
(*: Member:           ProcessEvent
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aMMXMLNode, aValue, aEvent, aWriteElementToBin)
 *
 *  Kurzbeschreibung: Behandlet die Eventliste f�r einen Node
 *  Beschreibung:     
                      Es k�nnen max. 2 verschiedene Events pro Node ausgel�st werden.
                      Sie werden in xElementEventRecList aufgebaut:
                      xElementEventRecList[0]: Interner Event (mit oder ohne Parameter)
                      xElementEventRecList[1]: Externer Event (mit oder ohne Parameter) 
                      
                      Der interne Event wird vor dem externen - abgearbeitet!
 --------------------------------------------------------------------*)
function TMMXMLConverter.ProcessEvent(aMapNode: IXMLDOMNode; aMMXMLNode: IXMLDOMNode; aValue: Variant; aEvent: Integer;
  var aWriteElementToBin: Boolean): Variant;
var
  xEventElements: IXMLDOMNodeList;
  xEventArgument: string;
  i: Integer;
  xEventElement: IXMLDOMNode;
  xDirectionStr: string;
  xHandling: Boolean;
  xElementEventRecList: array [cEventInt..cEventExt] of TElementEventRec;
  xModeStr: string;
  xDeleteFlag: Boolean;
  xPrepareAndSend: Boolean;

  (**********************************************************************
   *  Kurzbeschreibung: Der Liste mDelEventList wird ein Eintrag zugef�hrt.
   *                    Die MMXML-Nodes dieser Liste werden in DelEventListNodes
   *                     sind, werden gel�scht
   *  Beschreibung:     Wird nur f�r Bin -> XML ben�tigt und ganz am Schluss der Konvertierung
                        (nach OnAfterConvert) aufgerufen.
   ************************************************************************)
  procedure AddDelEventList;
  var
    k:      Integer;
    xFound: Boolean;
  begin
    try
      if not Assigned(mDelEventList) then begin
        //Erstellen der LIste f�r die Nodes, welche im Ausgangs MMXML am Schluss noch gel�scht werden m�ssen
        mDelEventList:= TMyMapMMXMLList.Create;
        //FreeAndNil dieser LIste passiert in DelEventListNodes
      end;

      k := 0;
      xFound := False;
      //Check ob Node nicht schon in Liste ist
      while not xFound and (k<mDelEventList.Count) do begin
        if aMMXMLNode=mDelEventList.MMXMLItems[k] then
          xFound := True;
          INC(k);
      end;//while not xFound and (k<=mDelEventList.Count)

      if not xFound then begin
        //Add Element to EventList
        mDelEventList.Add(aMapNode, aMMXMLNode);
      end; //if xFound
    except
      on e: Exception do begin
        raise EMMXMLConverter.Create(e.message+' Error in AddDelEventList ');
      end;
    end;

  end;

begin
    //Initialisieren
  xDeleteFlag := False;
  xPrepareAndSend := False;

    // Uebergebenen Wert (Wert der schon vorhanden war oder Defaultwert aus XMLMap) zuweisen.
  xElementEventRecList[aEvent].Value := aValue;
  Result := aValue;

  try
      // Alle SubNodes des aMapNode mit Namen 'event'
    xEventElements := aMapNode.selectNodes(cEleEvent);
    xEventArgument := '';

//: ----------------------------------------------
  //: ----------------------------------------------
      //Loop �ber die Liste aller 'event' Nodes
    for i := 0 to xEventElements.length - 1 do begin
      xHandling := True;
        //'event nur behandlen, wenn direction stimmt
      if GetAttributeAsString(cAtrDirection, xEventElements.item[i], xDirectionStr) then begin
        if (mUp and (xDirectionStr=cDirNames[cDirDown])) or (not mUp and (xDirectionStr=cDirNames[cDirUp])) then
          xHandling := False;
      end; //if GetAttributeAsString(cAtrDirection, xEventElements.item[i], xDirectionStr)

      if xHandling then begin
          //Aufl�sen des 'event' Attributes (meistens eine x-path Anweisung)
          // Es funktioniert eine relative wie eine absolute x-path Anweisung
        xEventElement := Nil;
        if (AttributeToStringDef(cAtrElement, xEventElements.item[i], '')<>'') then //Attribute <element> wurde gefunden
          if aMMXMLNode<>Nil then //Kann NIL sein, wenn z.B. der Node im MMXML nicht vorhanden ist
            xEventElement := aMMXMLNode.SelectSingleNode(AttributeToStringDef(cAtrElement, xEventElements.item[i], ''))
          else
              // Wenn das Element nicht im XML vorhanden ist, dann den XPath auf das Dokument ausf�hren
            xEventElement := mMMXML.SelectSingleNode(AttributeToStringDef(cAtrElement, xEventElements.item[i], ''));

        GetAttributeAsString(cAtrMode, xEventElements.item[i], xModeStr); //mode holen

        if (xModeStr=cEventNames[aEvent]) then begin  //Der gefundenen Event entspricht dem Eventtyp (int/ext) von aEvent
            //'del' Attribute suchen und wenn gefunden xDeleteFlag setzen
            // Wenn mindestens ein EventElement das 'del' Attribut auf TRUE hat wird das zugeh�rige Parentelement zum L�schen markiert.
          if AttributeToBoolDef(cAtrDeleteFlag, xEventElements.item[i], xDeleteFlag) then begin
            xDeleteFlag := True;
          end; //if GetAttributeAsString(cAtrDirection, xEventElements.item[i], xDirectionStr)
    //alt        xElementEventRecList[aEvent].CalcElementName := GetElementFullXPath(aMapNode);
          xElementEventRecList[aEvent].CalcElementName := GetElementFullXPath(aMapNode);
          xPrepareAndSend := True;
          if (Assigned(xEventElement)) then begin
              //Event mit Attributen
            xElementEventRecList[aEvent].ArgumentsString := xElementEventRecList[aEvent].ArgumentsString + ',' + GetElementFullXPath(xEventElement) + '=' + GetElementValueDef(xEventElement,'');
          end; //if (Assigned(xEventElement))
        end; //if xModeStr=cEventNames[aEvent])

      end; //if xHandling then begin
    end; //for i := 0 to xEventElements.length - 1

//: ----------------------------------------------
  //: ----------------------------------------------
    if xPrepareAndSend then begin
        //Aufbereiten und versenden der Events
      if xElementEventRecList[aEvent].ArgumentsString > '' then
        system.delete(xElementEventRecList[aEvent].ArgumentsString, 1, 1);  //Ueberfl�ssigen cCRLF l�schen
      if mDebugOutEnabled then
        CodeSite.SendFmtMsg('EventList[%d].ArgumentString: %s',[aEvent, xElementEventRecList[aEvent].ArgumentsString]);
        //ArgumentsString in StringList umkopieren
      xElementEventRecList[aEvent].ArgumentList           := TmmStringList.Create;
      try
        xElementEventRecList[aEvent].ArgumentList.CommaText :=xElementEventRecList[aEvent].ArgumentsString;

        if xElementEventRecList[aEvent].CalcElementName <> '' then begin
          //Event ausl�sen mit der Beschreibung des neuen Datenelements
          xElementEventRecList[aEvent].WriteElementToBin := True; //Initialisieren auf True
          if aEvent>=cEventExt then begin
              // Externer Event
            DoCalcExternal(self, xElementEventRecList[aEvent], xDeleteFlag);
            if xDeleteFlag and mUp{BinToXML} then  //Falls True Einf�gen des Nodes in die DeleteListe
              AddDelEventList;
            if not mUp{BinToXML} then
              aWriteElementToBin := xElementEventRecList[aEvent].WriteElementToBin; //Ob das behandelte XML-Element wirklich ins Bin�rfile geschrieben werden soll
          end
          else begin
              // Interner Event
            if not Assigned(FOnCalcInternal) then
              if mUp then
                FOnCalcInternal := NewInternalBinToXMLEvent  //Zuweisen der BinToXML-Procedure an den Event, welcher nachfolgend ausgel�st wird
              else
                FOnCalcInternal := NewInternalXMLToBinEvent; //Zuweisen der XMLToBin-Procedure an den Event, welcher nachfolgend ausgel�st wird
            DoCalcInternal(self, xElementEventRecList[aEvent], xDeleteFlag);
            if xDeleteFlag and mUp{BinToXML} then //Falls True Einf�gen des Nodes in die DeleteListe
              AddDelEventList;
            FOnCalcInternal := Nil;  //Interner Event wieder auf Nil
          end; //if aEvent>=cEventExt

            //Sofern ein Wert verrechnet wurde Wert dem Result zuweisen
          if not VarIsEmpty(xElementEventRecList[aEvent].Value) then begin
            Result :=  xElementEventRecList[aEvent].Value;
            if (vartype(aValue) in [varSmallint, varInteger, varSingle, varDouble, varCurrency, varByte]) and (vartype(result) <> vartype(aValue)) then
              result := MMStrToFloat(result);
          end; //if not VarIsEmpty(xElementEventRecList[aEvent].Value)
        end; //if xElementEventRecList[aEvent].CalcElementName <> ''
      finally
        xElementEventRecList[aEvent].ArgumentList.Free;
      end;
    end; //if xPrepareAndSend

  except
    on e:EMMXMLConverter do begin
      raise EMMXMLConverter.Create (e.message+Format(' %s:TError while preparing internal or external events!',[Self.Classname]));
    end;
  end;
end;// TMMXMLConverter.ProcessEvent cat:No category

//:-------------------------------------------------------------------
(*: Member:           ProcessEventNodeList
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMaxIndex)
 *
 *  Kurzbeschreibung: Abarbeiten der EventNodeList 
 *  Beschreibung:     
                      Die vorg�ngig w�hrend dem Mapping (in HandleElementAttributeNodesBinToXML) erstellte
                      EventNodeList wird hier abgearbeitet.
                      Der Event wird auf XML-Ebene abgehandelt. Da der XML-Wert ausserhalb des Konverters
                      ver�ndert werden kann, m�ssen die durch den Event ver�nderten Werte nochmals gecheckt werden.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.ProcessEventNodeList(aMaxIndex : integer=cEventExt);
var
  i: Integer;
  xDataType: TDataType;
  xLast: Boolean;
  xValue: Variant;
  j: Integer;
  xMapNode: IXMLDOMNode;
  xMMNode: IXMLDOMNode;
  xObsolet: Boolean;

  function ConvertNumVariant(aMapNode: IXMLDOMNode; aValue: variant): variant;
  begin
    result := aValue;
    // Konvertierung vornehmen, wenn der Dezimal Separator nicht '.' ist
    if xDataType in [dtByte, dtShortint, dtWord, dtSmallint, dtDWord, dtLongint, dtSingle] then begin
      if not(AttributExists(cAtrEnum, aMapNode.attributes) or AttributExists(cAtrBitCount, aMapNode.attributes)) then
        result := MMStrToFloat(aValue);
    end;
  end;

begin
  try
    if Assigned(mEventList) then begin
      try
        for j:= cEventInt to aMaxIndex do begin
          for i:= 0 to mEventList.Count-1 do begin
            if (Assigned(mEventList.MapItems[i])) then begin
              if mDebugOutEnabled then
                codesite.SendFmtMsg('%s.ProcessEventNodeList: Element %s starts Eventhandling.',[Self.Classname,(mEventList.MapItems[i] as IXMLDOMElement).NodeName]);
              xDataType := GetTypeFromString(mEventList.MapItems[i] as IXMLDOMElement);

              // Da die Events in der Up Richtung asynchron bearbeitet werden, muss das Tracking neu bestimmt werden
              SetElementTracking(mEventList.MapItems[i]);
              if mTrackCurrentElement then
                CodeSite.SendMsgEx(csmLevel2, 'Tracking: EventHandling');

              if (mEventList.MMXMLItems[i]=NIL) then begin
                //Das ist der Fall wenn kein MMXML-Node vorhanden ist und der Defaultwert eingef�llt wurde
                //Defaultwert erneut holen und dem Event mitgeben (7.9.06 Nue)
                mActElementType := GetTypeFromString(mEventList.MapItems[i] as IXMLDOMElement); //Setzen des Elementtyp
                GetDefaultFromMap(mEventList.MapItems[i],xValue);  //(7.9.06 Nue)

                //alt bis 7.9.06 xValue := mEventList.MapItems[i].childNodes.item[0].nodeValue;
                xValue := ProcessEvent(mEventList.MapItems[i],NIL, xValue, j, xObsolet)
              end else begin
                //Normalfall
                xValue := mEventList.MMXMLItems[i].childNodes.item[0].nodeValue;
                xValue := ConvertNumVariant(mEventList.MapItems[i], xValue);
                xValue := ProcessEvent(mEventList.MapItems[i],mEventList.MMXMLItems[i], xValue, j, xObsolet);
              end;

              //Zus�tzlicher Check nur bei externen Events und wenn das Element kein 'enum'-Attribut enth�lt
              if (j=cEventExt) and (not AttributExists(cAtrEnum, (mEventList.MapItems[i]).attributes)) then begin
                //Nochmaliger Check der ver�nderten Werte durch Events, da grunds�tzlich irgendwelche Werte h�tten eingegeben werden k�nnen!
                CheckMMXMLPlausability(xValue, xDataType, xLast);
                if not xLast then begin
                  (* Nicht alle �berpr�fungen machen (insbesondere die Konvertierung sollte
                     nicht noch einmal gemacht werden) *)
                  xMapNode := mEventList.MapItems[i];
                  xMMNode  := mEventList.MMXMLItems[i];

                  //Checken allf�lliger Enums
                  CheckEnum(xMapNode,xMMNode,xValue);
                  //Checken allf�lliger Facets
                  CheckFacets(xMapNode,xMMNode,xValue);

  //                CheckSequenceBinToXML(mEventList.MapItems[i],mEventList.MMXMLItems[i],xValue);
                  mEventList.MMXMLItems[i].childNodes.item[0].nodeValue := xValue;
                end; //if not xLast
              end; //if j=cEventExt
            end
            else begin
              WriteLogFile(etWarning, Format('ProcessEventNodeList: Couldn''t process event %d from mEventList, because it was not assigned! ',[i]));
            end; //if (Assigned(mEventList.MapItems[i]))
          end; //for
        end; //for j:=
      finally
        freeAndNil(mEventList);
      end;// try finally

  //   end
  //   else begin
  //     WriteLogFile(etWarning, 'ProcessEventNodeList: Couldn''t process mEventList, because it was not assigned! ');
    end; //if Assigned(mEventList)
  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' ProcessEventNodeList failed. ');
    end;
  end;
end;// TMMXMLConverter.ProcessEventNodeList cat:No category

//:-------------------------------------------------------------------
(*: Member:           RemoveNode
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMMXMLNode)
 *
 *  Kurzbeschreibung: REKURSIV!! L�scht den Node, falls der Parent keine Children mehr besitzt, soll dieser auch
 gel�scht werden
 *  Beschreibung:     
                      REKURSIV!!!!
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.RemoveNode(aMMXMLNode: IXMLDOMNode);
var
  xNode: IXMLDOMNode;
  xParentNode: IXMLDOMNode;
begin
  //Sichern des ParentNodes vor dem L�schen
  xParentNode := aMMXMLNode.parentNode;
  //L�schen des �bergebebenen Nodes
  xNode := aMMXMLNode.parentNode.removeChild(aMMXMLNode);

  //Falls der Node gel�scht werden konnte, wird der gel�schte Node zur�ckgegeben
  if (xNode<>NIL) then begin
    if mDebugOutEnabled then
      codesite.SendFmtMsg('%s.DelEventListNodes: Element %s deleted in MMXML.',[Self.Classname,xNode.nodeName]);
    if xParentNode<>NIL then begin
      if not(xParentNode.hasChildnodes) and not(xParentNode.nodeName=mMMXML.documentElement.nodeName) then begin
        //Falls der Parent des gel�schten Nodes keine Children mehr besitzt, wird dieser auch gel�scht
        RemoveNode(xParentNode); //REKURSIVER Aufruf !!!!!!!!!!
      end; //if ((xNode.parentNode.Childnodes.length)>0)
    end; //if xParentNode<>NIL
  end
  else begin
    WriteLogFile(etWarning, Format('Removing XML node <%s> from MMXML failed!', [GetElementFullXPath(aMMXMLNode)]));
  end; //if ((mEventList.MMXMLItems[i].parentNode.removeChild(mEventList.MMXMLItems[i]))=NIL)
end;// TMMXMLConverter.RemoveNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveBinFile
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aLen, aPrefix)
 *
 *  Kurzbeschreibung: Erstellt Binfile zu Debugzwecken
 *  Beschreibung:     
                      Zu Debugzwecken kann eine Datei im Temp-Verzeichnis angelegt werden. 
                      Diese Einstellung wird mit einem Eintrag in der Registry festgelegt (cWriteInOutDebug=true). 
                      Die Einstellung gilt f�r In- und Output
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.SaveBinFile(aBinData: PByte; aLen: integer; aPrefix: string);
var
  xFileName: string;
  xStream: TFileStream;
  xDayCount: Integer;
begin
  aPrefix := copy(aPrefix, 1, 2);
    // liefert den kompleten Dateinamen mit Pfad zum Temp Verzeichnis
  xFileName := GetNewTempFileName(aPrefix + 'B');

    // File schreiben
  xStream := nil;
  try
    xStream := TFileStream.Create(xFileName, fmCreate	or fmShareDenyWrite);
      // Schreibt den gesammten Buffer in das File
    xStream.Write(aBinData^, aLen);
  finally
    xStream.Free;
  end;// try finally

//: ----------------------------------------------
  //: ----------------------------------------------
    // Alte Files nach einer bestimmten Anzahl Tagen l�schen (Registry DayCountToDeleteBinaryFiles)
  xDayCount := StrToIntDef(GetRegString(HKEY_LOCAL_MACHINE, cRegMMDebug, cDayCountToDeleteBinaryFiles, '30'), 30);
    // Wenn mindestens ein Tag eingestellt ist, dann die Files asynchron l�schen.
  if xDayCount > 0 then
      // Das Objekt muss nicht freigegeben werden, da der Thread mit FreeOnTerminate erstellt wird
    TDeleteOldTempFiles.Create(xDayCount, ExtractFilePath(xFileName));
end;// TMMXMLConverter.SaveBinFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           SaveXMLFile
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aXML, aPrefix)
 *
 *  Kurzbeschreibung: Erstellt XMLfile zu Debugzwecken 
 *  Beschreibung:     
                      Zu Debugzwecken kann eine Datei im Temp-Verzeichnis angelegt werden. 
                      Diese Einstellung wird mit einem Eintrag in der Registry festgelegt (cWriteInOutDebug=true). 
                      Die Einstellung gilt f�r In- und Output
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.SaveXMLFile(aXML: string; aPrefix: string);
var
  xFileName: string;
begin
  aPrefix := copy(aPrefix, 1, 2);
  xFileName := GetNewTempFileName(aPrefix + 'X');

  // File schreiben
  with TFileStream.Create(xFileName, fmCreate	or fmShareExclusive) do try
    // Schreibt den gesammten XML String in das File
    Write(PChar(aXML)^, Length(aXML));
  finally
    free;
  end;// with TFileStream.Create(ChangeFileExt(FileName, '.xml'), fmOpenWrite or fmShareExclusive) do try
end;// TMMXMLConverter.SaveXMLFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendMessage
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMsg)
 *
 *  Kurzbeschreibung: Bereitet CodeSite-Meldungen auf.
 *  Beschreibung:     
                      Wenn DebugMode eingeschaltet ist, werden CodeSiteMeldungen normal ausgegeben.
                      Andernfalls werden sie in einer StringList zusammengefasst und bei Ende des 
                      KonverterAufrufes (BinToXML oder XMLToBin) als eine Meldung ausgegeben.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.SendMessage(aMsg: string);
begin
  if mDebugOutEnabled then begin
    CodeSite.SendMsg(aMsg);
  end
  else begin
    if mMsgList=NIL then
      mMsgList := TmmStringList.Create;

    if mMsgList<>NIL then begin
      mMsgList.Add(aMsg);
    end;
  end;
end;// TMMXMLConverter.SendMessage cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetElementTracking
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapElement)
 *
 *  Kurzbeschreibung: Schaltet das Tracking eines Elements je nach Debugeinstellungen ein oder aus
 *  Beschreibung:     
                      Elemente die "getrackt" werden erzeugen diverse Ausgaben im CodeSite.
                      Die Elemente werden �ber einen Registry Key definiert (Sowohl das Ein- Ausschalten, wie
                      auch die einzelnen Elemente).
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.SetElementTracking(aMapElement: IXMLDOMNode);
var
  xElementName: string;
begin
  mTrackCurrentElement := false;
  if mDebugTracking then begin
    xElementName := '';
    if assigned(aMapElement) then
      xElementName := GetElementFullXPath(aMapElement);
    if assigned(mDebugTrackingElements) then
      mTrackCurrentElement := (mDebugTrackingElements.IndexOf(xElementName) >= 0);
  end;// if mDebugTracking then begin

//: ----------------------------------------------
  //: ----------------------------------------------
    // Gleich mal den Start des Trackings bekantgeben
  if mTrackCurrentElement then begin
      // Elementname ausgeben
    if assigned(aMapElement) then
      CodeSite.SendStringEx(csmLevel1, 'Tracking: Start', xElementName)
    else
      CodeSite.SendStringEx(csmLevel1, 'Tracking: Stop', 'not assigned');
  end;// if mTrackCurrentElement then begin
end;// TMMXMLConverter.SetElementTracking cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.SetMapSection(aMapSection: TMapSection);
var
  xNode: IXMLDOMNode;
begin
  //Root Element lesen und Memory f�r Bin�rfile bereitstellen
  if (mXMLMap.documentElement.nodeName = cEleLoepfeBody) then begin
  //  xTmpNodeList := mXMLMap.documentElement.selectNodes(Format('%s', [cMapNames[aMapSection]]));
    xNode := mXMLMap.SelectSingleNode(Format('%s', [cMapSectionNames[aMapSection]]));
    if NOT(assigned(xNode)) then begin
      //Gefordertes Element ist nicht oder mehrfach vorhanden  vorhanden
      SendMessage(Format('%s.SetMapSection: Mapfile xPath :%s, no results found!',[Self.Classname, cMapSectionNames[aMapSection]]));
      raise EMMXMLConverter.Create(Format('%s.SetMapSection: Mapfile xPath :%s, No results found! ',[Self.Classname, cMapSectionNames[aMapSection]]));
    end
    else begin
      mMapSectionElement := (xNode as IXMLDOMElement); //Gefundenen Node als neues Rootelement festhalten
    end; //if NOT(assigned(xNode))
  end
  else begin
    //Rootelement ist falsch
    SendMessage(Format('%s.SetMapSection: Could not find xml root element in MapFile',[Self.Classname]));
    raise EMMXMLConverter.Create(Format('Could not find xml root element %s in MapFile',[cEleLoepfeBody]));
  end; //if NOT(mXMLMap.documentElement.nodeName = cEleLoepfeBody)
end;// TMMXMLConverter.SetMapSection cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetParser
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aXMLDoc)
 *
 *  Kurzbeschreibung: Setzt den gew�nschten Parser
 *  Beschreibung:     
                      MSXML hat mit der Version 4.0 einen neuen Parser eingef�hrt der �ber ein 
                      'second Level' Property ausgew�hlt werden kann.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.SetParser(aXMLDoc: DOMDocument40);
begin
  aXMLDoc.setProperty('NewParser', true);
end;// TMMXMLConverter.SetParser cat:No category

//:-------------------------------------------------------------------
(*: Member:           ShowError
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aError)
 *
 *  Kurzbeschreibung: Gibt die Informationen �ber den aufgetretenen Fehler an Codesite aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.ShowError(aError: IXMLDOMParseError);
var
  xErrorString: string;
begin
  with aError do begin
    xErrorString := 'Pfad: ' + url + ccrlf;
    xErrorString := xErrorString + Format('Error Code: %d %sBegr�ndung: %s',[ErrorCode, ccrlf, reason, ccrlf]);
    xErrorString := xErrorString + Format('Line Text: %s%sZeile: %d%s', [srcText, ccrlf, line, ccrlf]);
    xErrorString := xErrorString + Format('Zeilenposition: %d%sDateiposition: %d%s', [linepos, ccrlf, filepos, ccrlf]);
  end;// with aError do begin
  if mDebugOutEnabled then
    codesite.SendError(xErrorString);
end;// TMMXMLConverter.ShowError cat:No category

//:-------------------------------------------------------------------
function TMMXMLConverter.SwapDWordIt(aValue: DWord): DWord;
begin
  if mChangeEndian then
    Result := SwapDWord(aValue)
  else
    Result := aValue;
end;// TMMXMLConverter.SwapDWordIt cat:No category

//:-------------------------------------------------------------------
function TMMXMLConverter.SwapIt(aValue: Word): Word;
begin
  if mChangeEndian then
    Result := Swap(aValue)
  else
    Result := aValue;
end;// TMMXMLConverter.SwapIt cat:No category

//:-------------------------------------------------------------------
(*: Member:           TypeAndOffsetAtrExists
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aIgnore)
 *
 *  Kurzbeschreibung: Checkt 'type'- und 'offset'-Attribut.
 *  Beschreibung:     
                      Checkt ob die Attributte 'type' und 'offset' im XML-Element vorhanden sind.
 --------------------------------------------------------------------*)
function TMMXMLConverter.TypeAndOffsetAtrExists(aMapNode: IXMLDOMNode; out aIgnore: Boolean): Boolean;
var
  xAtrValue: string;
begin
  try
    aIgnore := False;
    //Attribute behandlen 'type'
    if GetAttributeAsString(cAtrType, aMapNode, xAtrValue) then begin  //'type' Attribut ist vorhanden
      if (xAtrValue=cBasicTypeNames[dtIgnore]) then begin
        aIgnore := True;
        Result := True;
        EXIT;
      end; //if not(xAtrValue=cBasicTypeNames[dtIgnore])
    end
    else begin
      //Gefordertes Attribut nicht vorhanden
      SendMessage(Format('%s.TypeAndOffsetAtrExists: Could not find required attribute %s in node:%s in MapFile',[Self.Classname,cAtrType,GetElementFullXPath(aMapNode)]));
      raise EMMXMLConverter.Create(Format(' Could not find required attribute %s! ',[cAtrType]));
      //EXIT;
    end; //if GetAttributeAsString(cAtrType, aMapNode, xAtrValue)

    //Attribute behandlen 'offset'
    if GetAttributeAsString(cAtrOffset, aMapNode, xAtrValue) then begin  //'offset' Attribut ist vorhanden
      if (StrToInt(xAtrValue)>mBinLen) and (mBinDoc<>NIL) then begin
        //Gefordertes Attribut nicht vorhanden
        SendMessage(Format('%s.TypeAndOffsetAtrExists: Attribute %s in node:%s in MapFile. Offsetvalue %s is higher then Binfile length %d!!',[Self.Classname,cAtrOffset,GetElementFullXPath(aMapNode),xAtrValue,mBinLen]));
        raise EMMXMLConverter.Create(Format(' Offsetvalue %s of attribute %s in MapFile is higher then Binfile length (%d)!!',[xAtrValue,cAtrOffset,mBinLen]));
        //EXIT;
      end; //if xAtrValue>mBinLen then begin
      Result := True;
    end
    else begin
      //Gefordertes Attribut nicht vorhanden
      SendMessage(Format('%s.TypeAndOffsetAtrExists: Could not find required attribute %s in node:%s in MapFile',[Self.Classname,cAtrOffset,GetElementFullXPath(aMapNode)]));
      raise EMMXMLConverter.Create(Format(' Could not find required attribute %s! ',[cAtrOffset]));
      //EXIT;
    end; //if GetAttributeAsString(cAtrType, aMapNode, xAtrValue)
  except
    on e: Exception do
      raise EMMXMLConverter.Create(e.Message+' TypeAndOffsetAtrExists failed. ');
  end;
end;// TMMXMLConverter.TypeAndOffsetAtrExists cat:No category

//:-------------------------------------------------------------------
(*: Member:           Validate
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Validiert das MMXML-File gegen�ber einem XML-Schema
 *  Beschreibung:     
                      Das verwendete XML-Schema wird aus der lokalen Registry gelesen.
                      (PS: Das MAP-File ist NICHT gegen�ber diesem XML-Schema validierbar!!)
 --------------------------------------------------------------------*)
function TMMXMLConverter.Validate: Boolean;
var
  xXMLSchemaColl: IXMLDOMSchemaCollection2;
  xString: string;
begin
  Result := False;
  try
    mMMXML.async := False; //?
    mMMXML.validateOnParse := False; //?
    mMMXML.resolveExternals := False; //?
    xXMLSchemaColl := CoXMLSchemaCache40.Create;

    xString := mXMLSchemaStream;
    mXMLSchemaStream := StringReplace(xString,'\','\\',[rfReplaceAll]);

    xXMLSchemaColl.add(cSchemaNameSpace + ' ' + cLoepfeNameSpace, mXMLSchemaStream);
    mMMXML.schemas := xXMLSchemaColl;
    if mMMXML.load(mMMXML.xml) then begin
      Result := True;
    end; //if mMMXML.load(mMMXML.xml)

  except
    on e: Exception do begin
        raise EMMXMLConverter.Create(e.Message+' Error while XML schema validation! ');
    end;
  end;// try except

//: ----------------------------------------------
  //: ----------------------------------------------
    // // XML Dokument (MSXML) f�r die Beschreibung der Datenstruktur erzeugen
    // xXMLSchema := DOMDocumentMMCreate ;
    // SetParser(xXMLSchema);
    // (*mXMLMap.setProperty('NewParser', True);
    // mXMLMap.preserveWhiteSpace := true;*)
    //
    // if assigned(xXMLSchema)and assigned(mMMXML) then
    //   //MMXML validieren gegen�ber XMLSchema
    //   mMMXML.
    //
    //
    //
    // var xmldoc = new ActiveXObject("Msxml2.FreeThreadedDOMDocument.4.0");
    // var SchemaCache = new ActiveXObject("Msxml2.XMLSchemaCache.4.0");
    //
    // xmldoc.async = false;
    // xmldoc.validateOnParse = false;
    // SchemaCache.add("x-schema:books", "c:\\books.xsd");
    // xmldoc.schemas = SchemaCache;
    // // The document will load only if a valid schema is attached to the xml
    // // file.
    // xmldoc.load("c:\\books.xml");
    // if (xmlDoc.parseError.errorCode <> 0) {
    //    var myErr = xmlDoc.parseError;
    //    alert("You have error " + myErr.reason);
    // } else {
    //    alert(xmldoc.xml) ;
    //
    //
    //   // Stream in das Dokument laden und parsen
    //   result := xXMLMap.loadXML(aXMLMap)
    // else
    //   raise Exception.Create('XML Map-File konnte nicht erzeugt werden');
end;// TMMXMLConverter.Validate cat:No category

//:-------------------------------------------------------------------
(*: Member:           WriteElementToBinWithoutCheck
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath, aValue)
 *
 *  Kurzbeschreibung: Schreibt ein Element ins Bin�rfile
 *  Beschreibung:     
                      Die Funktion sucht das Element(aSelPath) im Mapfile und schreibt den �bergebenen Wert ohne 
                      weiteren Checks nach der entsprechenden Vorschrift (Offset und Datentyp) in das Bin�rfile.
                      Ob das Value geswapt werden muss, ist noch von der laufenden Konvertierung bekannt. Soll
                      Diese Funktion auch ausserhalb der Konvertierung ben�tigt werden, muss sie neu Implementiert
                      werden,
                      da sonst einige Voraussetzungen nicht erf�llt sein k�nnten (Map Sektion, Bin�rfile, Mapping
                      Element ...).
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.WriteElementToBinWithoutCheck(aSelPath: string; aValue: Variant);
var
  xElement: IXMLDOMNode;
  xSelPath: string;
begin
  // Stellt den Selektionspfad relativ zum aktuellen Mapping Element ein (laufende Konvertierung)
  xSelPath := RemoveXPRoot('/' + cXMLLoepfeBody + '/', aSelPath);

  if assigned(mMapSectionElement) then begin
    // Selektiert das Mapping Element im Mapfile
    xElement := mMapSectionElement.SelectSingleNode(xSelPath);
    if assigned(xElement) then begin
      // Jetzt noch den aktuellen Datentyp feststellen
      mActElementType := GetTypeFromString(xElement as IXMLDOMElement);
      // Wert in das Bin�rfile schreiben
      WriteValueToBin(xElement, aValue, true);
    end;// if assigned(xElement) then begin
  end;// if assigned(mMapSectionElement) then begin
end;// TMMXMLConverter.WriteElementToBinWithoutCheck cat:No category

//:-------------------------------------------------------------------
procedure TMMXMLConverter.WriteLogFile(aEvent: TEventType; aText: string);
begin
  if not(assigned(mIntEventLog)) then begin
    //Ist der Fall, wenn der EventlogHAndle nicht von aussen mitgegeben wird
    mIntEventLog := TEventLogWriter.Create('Millmaster', '', ssApplication, '', True);
  end; //if not(assigned(mEventLog))

  if aEvent = etWarning then begin
    if not(assigned(mWarnings)) then
      mWarnings := TStringList.Create;
    mWarnings.Add(aText);
    mWarnings.Add('---------------');
  end else begin
    mIntEventLog.Write(aEvent, aText);
  end;// if aEvent = etWarning then begin
end;// TMMXMLConverter.WriteLogFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           WriteValueToBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aMapNode, aValue, aOverwrite, aBitCount, aBitOffset)
 *
 *  Kurzbeschreibung: Schreibt einen Wert, abh�ngig vom Datentyp in das Bin�rfile
 *  Beschreibung:     
                      Das Bin�rfile wird mit Null initialisiert.
                      Der �bergebene Wert wird immer zum bestehenden Wert an dieser Speicherstelle addiert.
                      Das ist notwendig, weil es XML-Elemente gibt, welche Bit-Werte repr�sentieren und sich daher
                      Bytem�ssig auf der selben Adresse befinden.
 --------------------------------------------------------------------*)
procedure TMMXMLConverter.WriteValueToBin(aMapNode: IXMLDOMNode; aValue: Variant; aOverwrite: Boolean; const aBitCount:
  Integer = 0; const aBitOffset: Integer = 0);
var
  xOffset: Cardinal;
  xRestBitCount: Cardinal;
  xByteCount: Cardinal;
  i: Cardinal;
  j: Cardinal;
  xUCByte: Byte;
  xString: string;
  xBitCount: Integer;
  xUnCutFlag: Char;
  xCutFlag: Char;
  xValueInt: Integer;
  xValueOk: Integer;
  k: Integer;
  xString1: string;
  xFinish: Boolean;
  xWideStr: WideString;
  xValueWord: Word;
  xMaskValue: Word;

  procedure SetBit(aBitNr: Integer; var aValue: byte);
  begin
    if aBitNr > 7 then raise EMMXMLConverter.Create('Nr. of Bit out of range.');
    aValue := aValue or (1 shl aBitNr);
  end;

begin
  xOffset := AttributeToIntDef(cAtrOffset, aMapNode, -1);

  //{Nue}  CodeSite.SendIntegerEx(csmLevel2, Format('Tracking: (Word) Binary at Offset:%d Node:%s',[PWord(cardinal(mBinDoc) + xOffset)^, GetElementFullXPath(aMapNode)]), xOffset);
  // Tracking eines bestimmten Elements (siehe SetElementTracking())
  if mTrackCurrentElement then begin
    CodeSite.SendIntegerEx(csmLevel2, 'Tracking: Binary Offset', xOffset);

    if VarIsNull(aValue) then
      CodeSite.SendMsgEx(csmError, 'Tracking: WriteToBin Value is NULL')
    else
      CodeSite.SendStringEx(csmLevel2, 'Tracking: WriteToBin', aValue);
  end;// if mTrackCurrentElement then begin

  // Je nach Basistyp die Entsprechende VariantTyp zuweisen
  case mActElementType of
    // Das Datenelement(e) ist/sind ein Alphanummerisches Zeichen (#0..#256)
    dtChar, dtUCChar: begin
      //Anzahl zu schreibende Bytes holen (wenn kein 'count' dann 1)
      xString := VarAsType(aValue, varString);
      xByteCount := AttributeToIntDef(cAtrCount, aMapNode, -1);
      i := 0; //Laufvariable Source (MMXML)
      j := 0; //Laufvariable Target (BinFile)
      while ((i<xByteCount) and (integer(i)<Length(xString)) and (j<xByteCount)) do begin
        if xString[i+1]='#' then begin
          (* Behandlung von Sonderzeichen
             Die Sonderzeichen werden mit Entit�ten dargestellt. Eine Entit�t beginnt immer mit einem
             Ampersand (&). Danach folgt ein Hash (#) und die Nummer des Zeichens. Ist die Nummer im Hex
             Zahlensystem folgt zuerst ein f�hrendes x.
             Beispiel:
               &#x20 ist ein Leerzeichen.
               &#32  ist auch ein Leerzeichen.
           *)
          { TODO -oLok : Behandlung von Sonderzeichen berichtigen, wenn �berhaupt notwendig }
          INC(i);
          k := i;
          xValueOk := 0;
          xString1 := '';
          xFinish := False;
          while ((integer(i)<Length(xString)) and (NOT xFinish)) do begin
            xString1 := xString1+xString[i+1];
            VAL(xString1,xValueInt, k);
            if k=0 then begin //Ziffer gelesen
              xValueOk := xValueInt;
              INC(i);
            end
            else begin
              PByte(cardinal(mBinDoc) + xOffset+j)^ := Byte(xValueOk);
              INC(j);
              xFinish := True; //Zahl gefunden -> Schleifenabbruch
            end; //if k=0
          end; //while
        end
        else begin
          PChar(cardinal(mBinDoc) + xOffset+j)^ := xString[i+1];
          INC(j);
          INC(i);
        end; //if xString[i+1]='#'
      end; //while
    end;// btChar, dtUCChar

    dtUTF16: begin
      xWideStr := aValue;
      xByteCount := AttributeToIntDef(cAtrCount, aMapNode, -1) * 2;
      if (Length(xWideStr) * 2) < integer(xByteCount) then
        xByteCount := Length(xWideStr) * 2;
      System.Move(xWideStr[1], Pointer(cardinal(mBinDoc) + xOffset)^, integer(xByteCount));
    end;// dtWideChar: begin

    // Das Datenelement ist ein Byte (0..256)
    dtByte: begin
      if aOverwrite then
        PByte(cardinal(mBinDoc) + xOffset)^ := aValue
      else
        PByte(cardinal(mBinDoc) + xOffset)^ := PByte(cardinal(mBinDoc) + xOffset)^ OR aValue;
    end;// btByte

    // Das Datenelement ist ein Shortint (-128..127)
    dtShortInt: begin
      if aOverwrite then
        PShortInt(cardinal(mBinDoc) + xOffset)^ := ShortInt(aValue)
      else
        PShortInt(cardinal(mBinDoc) + xOffset)^ := PShortInt(cardinal(mBinDoc) + xOffset)^ OR ShortInt(aValue);
     end;// btShortInt

    // Das Datenelement ist ein Word (0..65535)
    dtWord: begin
            { TODO -oLOK :
  Offenbar kann ein DWORD oder Cardinal nicht in einem Variant gehalten werden. Als integer wird aber das
  h�chstwertige Bit als Vorzeivchen angesehen und bei der Wandlung in einen String falsch dargestellt. (Gilt auch f�r das Lesen)}
  //    PByte(cardinal(mBinDoc) + xOffset)^ := VarAsType(aValue, varSmallint);
  //      PWORD(cardinal(mBinDoc) + xOffset)^ := SwapIt(PWORD(cardinal(mBinDoc) + xOffset)^+VarAsType(aValue, varSmallint));
      if aOverwrite then
        PWORD(cardinal(mBinDoc) + xOffset)^ := WORD(SwapIt(aValue))
      else begin
  //alt          PWORD(cardinal(mBinDoc) + xOffset)^ := PWORD(cardinal(mBinDoc) + xOffset)^ OR WORD(SwapIt(aValue));

  { TODO 2 -oNUE -cKonvertierung : Die folgende Sequenz (der ganze for-loop), sollte eigentlich bei jedem
Datentyp implementiert weden. Z.Z. ist dies nur beim Datentyp dtWord (nur ConfigCode) der Fall. }
        //Einbau ganzer For-Loop bei V1.15
        for i:= 0 to (aBitCount-1) do begin
          //Maske vorbereiten
          xMaskValue := 1;
          xMaskValue := (xMaskValue shl aBitOffset);  //Bit auf Position schieben; Maske generieren z.B. 00001000

//Falsche Variante von 1.15
//          xValueWord := (WORD(SwapIt(aValue)) shr (WORD(aBitOffset)+i));  //Gew�nschtes Bit auf BitPos 0 schieben
//     CodeSite.SendStringEx(csmViolet, Format('Offset=%d Value=%d xValueWord=%d MaskValue=%d vor Change',[xOffset, WORD(aValue), xValueWord, xMaskValue]), 'MWriteValueToBin');
//          if ODD(xValueWord) then  //Check ob Bit gesetzt ist
//            //Maske mit bestehenden Wert mit neuem Wert "verORen"
//            PWORD(cardinal(mBinDoc) + xOffset)^ := PWORD(cardinal(mBinDoc) + xOffset)^ OR xMaskValue
//          else
//            //Maske negieren (z.B. 00001000 => 111101111) und mit bestehenden Wert "verANDen"
//            PWORD(cardinal(mBinDoc) + xOffset)^ := PWORD(cardinal(mBinDoc) + xOffset)^ AND NOT(xMaskValue);


          xValueWord := (aValue shr (WORD(aBitOffset)+i));  //Gew�nschtes Bit auf BitPos 0 schieben
     CodeSite.SendStringEx(csmViolet, Format('Offset=%d Value=%d xValueWord=%d MaskValue=%d vor Change',[xOffset, WORD(aValue), xValueWord, xMaskValue]), 'MWriteValueToBin');
          if ODD(xValueWord) then  //Check ob Bit gesetzt ist
            //Maske mit bestehenden Wert mit neuem Wert "verORen"
            PWORD(cardinal(mBinDoc) + xOffset)^ := PWORD(cardinal(mBinDoc) + xOffset)^ OR WORD(SwapIt(xMaskValue))
          else
            //Maske negieren (z.B. 00001000 => 111101111) und mit bestehenden Wert "verANDen"
            PWORD(cardinal(mBinDoc) + xOffset)^ := PWORD(cardinal(mBinDoc) + xOffset)^ AND NOT(WORD(SwapIt(xMaskValue)));



     CodeSite.SendStringEx(csmViolet, Format('Offset=%d Value=%d xValueWord=%d MaskValue=%d nach Change',[xOffset, WORD(aValue), xValueWord, xMaskValue]), 'MWriteValueToBin');
        end; // for
      end; //else begin
  //      PWORD(cardinal(mBinDoc) + xOffset)^ := SwapIt(PWORD(cardinal(mBinDoc) + xOffset)^+aValue);
    end;// btWord

    // Das Datenelement ist ein Smallint (-32768..32767	)
    dtSmallInt: begin
      if aOverwrite then
        PSmallInt(cardinal(mBinDoc) + xOffset)^ := Smallint(SwapIt(aValue))
      else
        PSmallInt(cardinal(mBinDoc) + xOffset)^ := PSmallInt(cardinal(mBinDoc) + xOffset)^ OR Smallint(SwapIt(aValue));
    end;// btSmallInt

    // Das Datenelement ist ein Longint (-2147483648..2147483647)
    dtDWord: begin
      if aOverwrite then
        PDWORD(cardinal(mBinDoc) + xOffset)^ := DWORD(SwapDWordIt(aValue))
      else
        PDWORD(cardinal(mBinDoc) + xOffset)^ := PDWORD(cardinal(mBinDoc) + xOffset)^ OR DWORD(SwapDWordIt(aValue));
    end;// btLongInt

    // Das Datenelement ist ein Longint (-2147483648..2147483647)
    dtLongInt: begin
      if aOverwrite then
        PLongInt(cardinal(mBinDoc) + xOffset)^ := LongInt(SwapDWordIt(aValue))
      else
        PLongInt(cardinal(mBinDoc) + xOffset)^ := PLongInt(cardinal(mBinDoc) + xOffset)^ OR LongInt(SwapDWordIt(aValue));
    end;// btLongInt

  //       // Das Datenelement ist ein Int64 (-9223372036854775808..9223372036854775808)
  //       dtInt64: begin
  //         result := result + xSpace + IntToStr(PInt64(cardinal(aBase) + xCurrentOffset)^);
  //         inc(xCurrentOffset, SizeOf(Int64));
  //       end;// btInt64

    // Das Datenelement ist ein Singele (1.5 x 1045 .. 3.4 x 1038)
    dtSingle: begin
      (* HexDarstellung C8 00 02 01 --> 1 10010000 00000000000001000000001
         Vorzeichen = 1   (1 = positiv, 0 = negativ)
         Exponent   = 10010000 2^(e - 127) = 2^(144-127) = 2^17)

         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23
         0  0  0  0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0  0  0  1

         Mantisse   = 00000000000000111110100 (1,m = 1+ 2^-14 + 2^-23
                                                   = 1.00006115436553955078125)

         Fliesskommazahl = (-1) * 2^17 * 1.00006115436553955078125 = -131080.015625
      *)
      PSingle(cardinal(mBinDoc) + xOffset)^ := MMStrToFloat(aValue);
    end;// btSingle

    // Das Datenelement ist ein Double (5.0 x 10324 .. 1.7 x 10308)
    dtDouble: begin
      (* HexDarstellung C8 00 02 01 F4 00 00 00 --> 1 10010000000 0000000000100000000111110100000000000000000000000000
         Vorzeichen = 1   (1 = positiv, 0 = negativ)
         Exponent   = 10010000000 (2^(e - 1023) = 2^(1152-1023) = 2^129)

         1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50 51 52
         0  0  0  0  0  0  0  0  0  0  1  0  0  0  0  0  0  0  0  1  1  1  1  1  0  1  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0  0

         Mantisse   = 00000000000000111110100 (1,m = 1 + 2^-11 + 2^-20 + 2^-21 + 2^-22 + 2^-23 + 2^-24 + 2^-26
                                                   = 1.00049014389514923095703125)

         Fliesskommazahl = (-1) * 2^129 * 1.00049014389514923095703125 = -6.8089830849142338412437666333181e+38
      *)
      PDouble(cardinal(mBinDoc) + xOffset)^ := MMStrToFloat(aValue);
    end;// btDouble

    // Das Datenelement ist ein ucBit (True/False Array of bits)
    dtUCBit: begin
      //Logik festlegen und xByteCount, xRestBitCount holen
      InitucBitNode(aMapNode, xCutFlag, xUnCutFlag, xBitCount, integer(xRestBitCount), xByteCount);

      xString := aValue;
      //Komprimieren des Wertes(String) in Bytes (8 zu 1)
      for i := 1 to xByteCount do begin
        xUCByte := 0;  //Tempor�res Hilfsbyte zum Schreiben in Binfile
        for j := 0 to 7 do begin
          if ((i=xByteCount) and (xRestBitCount>0) and (j>=xRestBitCount)) then  //Abbruchkriterium, wenn Restbits vorhanden
            BREAK; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!
          if AnsiSameText(xString[((i-1)*8)+(j+1)],xCutFlag) then
            SetBit(j, xUCByte);
        end; //for j := 0 to 7
        //Schreiben eines Byte in Binfile
        PByte(cardinal(mBinDoc) + xOffset+i-1)^ := VarAsType(xUCByte, varByte);
      end; //for i := 1 to xByteCount
    end;// dtUCBit

  end;// case aAttributeRec of
end;// TMMXMLConverter.WriteValueToBin cat:No category

//:-------------------------------------------------------------------
(*: Member:           XmlToBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aBinLen, aXMLMap, aMapSection, aChangeEndian, aMMXML)
 *
 *  Kurzbeschreibung: Hauptmethode, f�r externen Aufruf von XML->Bin Konversion.
 *  Beschreibung:     
                      out aBinData:  R�ckgabepointer auf das Bin�rfile
                      aXMLMap:       MapFile (Inhalt) als Stream
                      aChangeEndian: True setzen, falls Bytereihenfolge nicht INTEL Format entspricht.
                      aMMXML:        MMXML-File (Inhalt) als Stream
                      Result:        L�nge des Bin�rfles
                      
                      
                      
 --------------------------------------------------------------------*)
function TMMXMLConverter.XmlToBin(aBinData: PByte; aBinLen: Integer; aXMLMap: DOMDocument40; aMapSection: TMapSection;
  aChangeEndian: Boolean; aMMXML: string): Integer;
var
  xOldCreateBufferForXMLToBin: Boolean;
begin
  xOldCreateBufferForXMLToBin := mCreateBufferForXMLToBin;
  try
    // Der Buffer ist bereits reserviert
    mCreateBufferForXMLToBin := false;
    // L�nge des Buffers setzen
    mBinLen := aBinLen;

      // zu Debug Zwecken Nue:2.4.07
    if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then
      if aBinLen>0 then
        SaveBinFile(aBinData, aBinLen, 'Im');    //Im = In Machinefile

    // Konvertierung ausl�sen
    result := XMLToBin(aBinData, aXMLMap, aMapSection, aChangeEndian, aMMXML);
  finally
    mCreateBufferForXMLToBin := xOldCreateBufferForXMLToBin;
  end;// try finally
end;// TMMXMLConverter.XmlToBin cat:No category

//:-------------------------------------------------------------------
(*: Member:           XmlToBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aXMLMap, aMapSection, aChangeEndian, aMMXML)
 *
 *  Kurzbeschreibung: Hauptmethode, f�r externen Aufruf von XML->Bin Konversion.
 *  Beschreibung:     
                      out aBinData:  R�ckgabepointer auf das Bin�rfile
                      aXMLMap:       MapFile (Inhalt) als Stream
                      aChangeEndian: True setzen, falls Bytereihenfolge nicht INTEL Format entspricht.
                      aMMXML:        MMXML-File (Inhalt) als Stream
                      Result:        L�nge des Bin�rfles
                      
                      
                      
 --------------------------------------------------------------------*)
function TMMXMLConverter.XmlToBin(out aBinData:PByte; aXMLMap: string; aMapSection: TMapSection; aChangeEndian: Boolean;
  aMMXML: string): Integer;
begin
  if mDebugOutEnabled then
    EnterMethod('XMLToBin(1)');

  try
    if (LoadXMLMap(aXMLMap)) then begin
      //Call von Overloaded XMLToBin
      Result := XmlToBin(aBinData, mXMLMap, aMapSection, aChangeEndian, aMMXML)
    end else begin
     // Gibt eine Detailierte Fehlermeldung in Codesite aus
       ShowError(mXMLMap.parseError);
       raise EMMXMLConverter.Create('XML Descriptor (MAP) parsing failed! ');
    end;// if (LoadXMLMap(aXMLMap))
  except
    raise;
  end;
end;// TMMXMLConverter.XmlToBin cat:No category

//:-------------------------------------------------------------------
(*: Member:           XmlToBin
 *  Klasse:           TMMXMLConverter
 *  Kategorie:        No category 
 *  Argumente:        (aBinData, aXMLMap, aMapSection, aChangeEndian, aMMXML)
 *
 *  Kurzbeschreibung: Hauptmethode, f�r externen Aufruf von XML->Bin Konversion.
 *  Beschreibung:     
                      out aBinData:  R�ckgabepointer auf das Bin�rfile
                      aXMLMap:       MapFile (Inhalt) als Stream
                      aChangeEndian: True setzen, falls Bytereihenfolge nicht INTEL Format entspricht.
                      aMMXML:        MMXML-File (Inhalt) als Stream
                      Result:        L�nge des Bin�rfles
                      
                      
                      
 --------------------------------------------------------------------*)
function TMMXMLConverter.XmlToBin(out aBinData:PByte; aXMLMap: DOMDocument40; aMapSection: TMapSection; aChangeEndian:
  Boolean; aMMXML: string): Integer;
var
  xOk: Boolean;
begin
  if mDebugOutEnabled then
    EnterMethod('XMLToBin');

    // zu Debug Zwecken
  if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then
    SaveXMLFile(aMMXML, 'In');

    // Initialisieren
  Result        := 0;
  mXMLMap       := nil;
  mUp           := False;
  mChangeEndian := aChangeEndian;
  FMapSection := aMapSection;

//: ----------------------------------------------
  //: ----------------------------------------------
  try
      //Zuweisen des �bergebenen MAP-DOMBaums an die Membervariable
    mXMLMap := aXMLMap;
      // XML Dokument (MSXML) f�r den Output erzeugen
    mMMXML := DOMDocumentMMCreate ;
    SetParser(mMMXML);
    if assigned(mMMXML) then begin
      xOk := True;

      if mValidate then begin
          //Laden MMXML und gegen�ber dem Schema validieren
        if not Validate then begin
          xOk := False;
          WriteLogFile(etWarning, 'XML Input (MMXML) not valid against XML schema! ');
        end;  //if not mValidate
      end
      else begin
          //Laden MMXML
        if not(mMMXML.loadXML(aMMXML)) then begin
          xOk := False;
        end;
      end; //if mValidate

      if xOk then begin
          //Before (start filling BinDoc) event ausl�sen
        DoBeforeConvert(Self, mXMLMap, mMMXML);

          //Map MMXML (Input) to Bin (Output)
        if NOT(MapXMLToBin(aMapSection, aBinData, Result)) then begin
          Result := 0;
          raise EMMXMLConverter.Create('MapXMLToBin failed!');
        end
        else begin  //Konvertierung successfull
            //After (filled BinDoc) event ausl�sen
          if mDebugOutEnabled then
            codesite.SendFmtMsg('MMXML vor ''AfterConvert'': %s.',[mMMXML.xml]);
          DoBeforeDeleteElements(Self, mXMLMap, mMMXML);
        end; //IF
      end
      else begin
        if mMMXML.parseError.errorCode <> 0 then begin
          WriteLogFile(etWarning, 'MMXML parsing failed!' + 'parse error: ' + inttostr(mMMXML.parseError.errorCode) +
            ' reason: ' + mMMXML.parseError.reason + ' srcText: ' + mMMXML.parseError.srcText +
            ' Line: ' +inttostr(mMMXML.parseError.line) +  ' Linepos: ' +inttostr(mMMXML.parseError.linepos));
        end;
        raise EMMXMLConverter.Create('Load XML input for MMXML failed, probably not well formed (or not valid against XML schema)! ');
      end;  //if xOk

        // zu Debug Zwecken
      if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then
        SaveBinFile(mBinDoc, result, 'Ot');

    end; //if assigned(mMMXML)

    if mMsgList<>NIL then
      codeSite. SendString('XMLToBin:Summary of MMXMLConverter-Messages:',mMsgList.Text);

    if assigned(mWarnings) then begin
      if mWarnings.Count > 0 then begin
        WriteToEventLogDebug(mWarnings.Text, '', etWarning);
        mWarnings.Clear;
      end;// if mWarnings.Count > 0 then
    end;// if assigned(mWarnings) then begin

  except
    on e: Exception do begin
          //Im Fehlerfall wird mEventList sonst nicht mehr regul�r freigegeben
      if mMsgList<>NIL then begin
        codeSite. SendString('XMLToBin:Summary of MMXMLConverter-Messages:',mMsgList.Text);
        mMsgList.Clear;
        freeAndNil(mMsgList);
      end;
      if Assigned(mEventList) then
          //Im Fehlerfall wird mEventList sonst nicht mehr regul�r freigegeben
        freeAndNil(mEventList);

      raise EMMXMLConverter.Create(e.message+' XMLToBin conversion failed! ');
    end;
  end;
end;// TMMXMLConverter.XmlToBin cat:No category

//:-------------------------------------------------------------------

end.
