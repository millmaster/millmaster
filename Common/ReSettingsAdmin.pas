unit ReSettingsAdmin;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TReSettingsAdmin = class(TComponent)
  private
    { Private declarations }
  protected
    { Protected declarations }
  public
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('MillMaster', [TReSettingsAdmin]);
end;

end.
 
