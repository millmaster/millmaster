(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AskCloseForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
=====================================================================================*)
unit AskCloseForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, MMSecurity, ActnList,
  mmActionList, IvDictio, IvMulti, IvEMulti, mmTranslator, mmLabel,
  mmButton, ExtCtrls, mmImage,
  AssignComp;   //Added on 12.11.01 Nue

type
  TAskClose = class(TDialogBottom)
    mmActionList1: TmmActionList;
    MMSecurityControl1: TMMSecurityControl;
    mmTranslator1: TmmTranslator;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    lMachine: TmmLabel;
    lSpindleRange: TmmLabel;
    lProdGrpName: TmmLabel;
    mmButton1: TmmButton;
    acOK: TAction;
    acCancel: TAction;
    acSecurity: TAction;
    mmImage2: TmmImage;
    mmImage1: TmmImage;
    lStyleName: TmmLabel;
    lStyle: TmmLabel;
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
  private
    procedure SetMachineName ( aMachineName : String );
    procedure SetProdGrpName ( aProdGrpName : String );
    procedure SetSpindleRange ( aSpindleRange : String );
    procedure SetStyleName(const aStyleName: String);
    function  GetMachineName : String;
    function  GetProdGrpName : String;
    function  GetSpindleRange : String;
    function GetStyleName: String;
  public
    property MachineName : String read GetMachineName write SetMachineName;
    property ProdGrpName : String read GetProdGrpName write SetProdGrpName;
    property SpindleRange : String read GetSpindleRange write SetSpindleRange;
    property StyleName : String read GetStyleName write SetStyleName;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, SettingsReader;

{$R *.DFM}
//------------------------------------------------------------------------------
  procedure TAskClose.SetMachineName ( aMachineName : String );
  begin
    lMachine.Caption := aMachineName;
  end;
//------------------------------------------------------------------------------
  procedure TAskClose.SetProdGrpName ( aProdGrpName : String );
  begin
    lProdGrpName.Caption := aProdGrpName;
  end;
//------------------------------------------------------------------------------
  procedure TAskClose.SetSpindleRange ( aSpindleRange : String );
  begin
    lSpindleRange.Caption := aSpindleRange;
  end;
//------------------------------------------------------------------------------
  function  TAskClose.GetMachineName : String;
  begin
    Result := lMachine.Caption;
  end;
//------------------------------------------------------------------------------
  function  TAskClose.GetProdGrpName : String;
  begin
    Result := lProdGrpName.Caption;
  end;
//------------------------------------------------------------------------------
  function  TAskClose.GetSpindleRange : String;
  begin
    Result := lSpindleRange.Caption;
  end;
//------------------------------------------------------------------------------
  procedure TAskClose.acOKExecute(Sender: TObject);
  begin
    inherited;
    //
  end;
//------------------------------------------------------------------------------
  procedure TAskClose.acCancelExecute(Sender: TObject);
  begin
    inherited;
    //
  end;
//------------------------------------------------------------------------------
  procedure TAskClose.acSecurityExecute(Sender: TObject);
  begin
    MMSecurityControl1.Configure;
  end;
//------------------------------------------------------------------------------
function TAskClose.GetStyleName: String;
begin
    Result := lStyleName.Caption;
end;
//------------------------------------------------------------------------------

procedure TAskClose.SetStyleName(const aStyleName: String);
begin
    lStyleName.Caption := aStyleName;
//    if NOT((TMMSettingsReader.Instance.IsPackageLabMaster) or (TMMSettingsReader.Instance.IsPackageDispoMaster)) then begin
    if NOT TMMSettingsReader.Instance.IsComponentStyle then begin
      lStyle.Hide;
      lStyleName.Hide;
    end;
end;

end.
