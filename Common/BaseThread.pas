(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseThread.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Kapselt allgemeine Funktionalitaet der Threads in eine BasisKlasse
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.06.1998  0.00  Wss | File created
| 15.06.1998  1.00  Wss | Class TBaseThread created
| 20.08.1998  1.01  Mg  | mAccessGroup for access rights for NamedPipe inserted
| 10.09.1998  1.02  Wss | Execute Methode simplified: call always ProcessJob methode
| 13.10.1998  1.03  Wss | On error WriteJobBufferTo() and WriteToMain() record error to EventLog
| 22.10.1998  1.04  Wss | On error in WriteJobBufferTo() job header is written to EventLog too
| 17.11.1998  1.05  Wss | Deriving changed to TmmThread
| 12.01.1999  1.06  Wss | mConfirmStartupImmediately implemented
| 13.01.1999  1.07  Wss | TBaseThread splitted into two classes: TBaseThread and TBaseSubThread
                          - TBaseThread contains common implementation for every thread in
                            the SubSystem AND StoreX
                          - TBaseSubThread contains common implementation only for SubSystem
                            threads. On pipe read the receiving job will be written into EventLog
                            if DebugMode is enabled.
                          - TBaseStoreX has to derived from TBaseThread. Some implementation aren't
                            needed anymore and are removed from TBaseStoreX implementation
| 02.03.1999  1.08 NueMg| TBaseSubThread : ProcessInitJob with case inserted
| 22.06.1999  1.09  Wss | bottom of ProcessJob writes Wrong Job received with JobTyp as clear text
| 20.07.1999  1.10 NueMg| try/except in TBaseSubThread.Execute implemented.
| 15.11.1999  1.11  Mg  | Params in TBaseThread inserted
| 29.11.1999  1.12  Mg  | TBaseSubThread.Destroy : TerminateThread zuoberst eingefuegt
| 20.03.2000  1.12 Wss  | constructor Create neu: reintroduce; virtual;
| 04.05.2000  1.12 Wss  | WriteLogDebug redirected to write with CodeSite
                          Use Handlername as DWord value in HKLM\....\MillMaster\Debug or use
                          DebugConfig tool to Add/Change key values.
                          0 = Disable (default), 1 = Enabled
| 28.06.2000  1.23  Mg  | BaseSubThread : Restart in exception inserted
| 19.09.2000  1.23  Wss | TBaseSubThread.Init: extened with IPCClient write timeout parameter
                          BaseSubThread.WriteJobBufferTo: Repetition of write and restart on error
| 05.04.2001  1.23  Wss | WriteLogDebug: aUseEventLog parameter added to distinguish to write with
                          CodeSite (False) or write to EventLog (True)
| 11.04.2001  1.24  Nue | In TBaseThread neu Params : TMMSettingsReader (statt TBaseSettingsReader)
| 21.06.2001  1.25  Nue | WriteLogDebug: aUseEventLog parameter: aUseEventLog: Boolean = True (former was FALSE)
| 06.10.2002        LOK | Umbau ADO
| 11.12.2002  1.26  Nue | In TBaseSubThread.ProcessInitJob and TBaseSubThread.ProcessInitJob Msg new as etWarning (former was etError)
| 18.03.2004  1.23  Wss | JobBuffer Handling auf Dynamisch abge�ndert
|=========================================================================================*)
unit BaseThread;              

interface

uses
  Classes, SysUtils, Windows,
  mmThread, IPCClass, BaseGlobal, MMEventLog, SettingsReader;

type
  TBaseThread = class(TmmThread)
  private
    fMMParams: TMMSettingsReader;    // Handle to the Parameter Object
  protected
    fError: TErrorRec;
    mEventLog:    TEventLogWriter;     // object references to MillMaster entries in EventLog
    mJob: PJobRec;                     // buffer of Job information
    mJobSize: DWord;                 // allocated memory size for mJob
    property Error: TErrorRec read fError;
//    procedure ProcessJob; virtual; abstract;
    procedure WriteLog(aEvent: TEventType; aText: String; aBuf: PByte = Nil; aCount: DWord = 0);
  public
    property Params : TMMSettingsReader read fMMParams write fMMParams;
    constructor Create; virtual;
    destructor Destroy; override;
    function Init: Boolean; virtual;
  end;

  TBaseSubThread = class(TBaseThread)
  private
    mMsg: TMMGuardRec;              // buffer of Msg information to MMGuard
    mHasChannel: Boolean;
    fDebugMode: Boolean;               // True, if DebugMode is enabled from MMGuard
    fThreadDef: TThreadDef;            // definition of themselves
    function GetConnectToIndex(aIndex: Byte): TThreadTyp;
    function GetIPCClientIndex(aIndex: Byte): PIPCClient;
    function GetIPCClientTyp(aThreadTyp: TThreadTyp): PIPCClient;
    procedure SetDebugMode(aValue: Boolean);
  protected
    mAccessGroup: String;              // aditional access control for pipe
//    mEventLogDeb: TEventLogWriter;     // object references to MillMasterDeb entries in EventLog
    mConfirmStartupImmediately: Boolean; // confirm startup of thread immediately: default = True
    procedure Execute; override;
    procedure ProcessError; virtual;
    procedure ProcessInitJob; virtual;
    procedure ProcessJob; virtual;
    function WriteJobBufferTo(aThreadTyp: TThreadTyp; aAlternativeJob: PJobRec = Nil): Boolean;
    procedure WriteLogDebug(aText: String; aBuf: PByte = Nil; aCount: DWord = 0; aUseEventLog: Boolean = True);
    function WriteToMain(aMsg: TMMGuardCommand; aError: DWord): Boolean;

    property ConnectToIndex[aIndex: Byte]: TThreadTyp read GetConnectToIndex;
    property IPCClientIndex[aIndex: Byte]: PIPCClient read GetIPCClientIndex;
    property IPCClientTyp[aThreadTyp: TThreadTyp]: PIPCClient read GetIPCClientTyp;
  public
    constructor Create(aThreadDef: TThreadDef); reintroduce; virtual;
    destructor Destroy; override;
    function DoConnect: Boolean; virtual;
    function Init: Boolean; override;
    property DebugMode: Boolean read fDebugMode write SetDebugMode default False;
    property ThreadDef : TThreadDef read fThreadDef write fThreadDef;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS, LoepfeGlobal, activex;

//-----------------------------------------------------------------------------
// TBaseThread
//-----------------------------------------------------------------------------
constructor TBaseThread.Create;
begin
  // creates thread in suspended mode
  inherited Create(True);

  FreeOnTerminate := False;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
  mJobSize        := sizeof(TJobRec);
  mJob            := AllocMem(mJobSize);
  mEventLog       := Nil;
  fMMParams       := Nil;
end;
//-----------------------------------------------------------------------------
destructor TBaseThread.Destroy;
begin
  // Wenn unter dem Kontext vom eigenen Thread das Objekt gel�scht wird,
  // dann muss TerminateThread NICHT aufgerufen werden, da dieser ja die
  // Endlosschlaufe bereits verlassen hat!
  if ThreadID = GetCurrentThreadID then begin
    // clean up any instances
    mEventLog.Free;
  end else begin    // the caller is a foreign thread
    // An diesem Punkt wurde Free von einem Prozess/Thread ausserhalb aufgerufen,
    // und dieser Code l�uft unter dessen Kontext. Da die Thread z.T. in
    // blocking read h�ngen, muss leider per Hammermethode TerminateThread()
    // diesen gestoppt werden.
    // Suspend bewirkt, dass in TThread.Destroy die Funktionen Terminate und
    // WaitFor NICHT aufgerufen werden.
    Suspend;
    // clean up any instances
    mEventLog.Free;
    // terminate my own thread before destroy my self
    TerminateThread(Handle, 0);
  end;

  FreeMem(mJob, mJobSize);
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseThread.Init: Boolean;
begin
  Result := True;
end;
//-----------------------------------------------------------------------------
procedure TBaseThread.WriteLog(aEvent: TEventType; aText: String; aBuf: PByte; aCount: DWord);
begin
  if Assigned(mEventLog) then
    if Assigned(aBuf) and (aCount > 0) then
      mEventLog.WriteBin(aEvent, aText, aBuf, aCount)
    else
      mEventLog.Write(aEvent, aText);
end;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TBaseSubThread
//-----------------------------------------------------------------------------
constructor TBaseSubThread.Create(aThreadDef: TThreadDef);
begin
  inherited Create;

  fDebugMode     := False;

  mAccessGroup   := '';
  mConfirmStartupImmediately := True;
  fThreadDef     := aThreadDef;
  fThreadDef.Thread := self;
  mHasChannel    := (fThreadDef.MaxChannels > -1);

  // make connection to Windows NT EventLog
  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cEventLogServerNameForSubSystems,
                                      fThreadDef.SubSystemTyp, cEventLogDefaultText[fThreadDef.ThreadTyp], True);
//  mEventLogDeb   := Nil;
end;
//-----------------------------------------------------------------------------
destructor TBaseSubThread.Destroy;
var
  i: Integer;
begin
  // FIRST thread will be terminated so we can close the read handle in IPCServer
  TerminateThread(Handle, 0); //wss, nicht n�tig, da dies weiter in ableitung behandelt wird!
  // clean up any instances
  with fThreadDef do begin
    // release pipe server object
    IPCRead.Free;
    //  // terminate my own thread before destroy my self
//    mEventLogDeb.Free;
    // release any pipe client objects
    for i:=1 to fThreadDef.ClientCount do
      IPCClientIndex[i]^.Free;
  end;

  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.DoConnect: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=1 to fThreadDef.ClientCount do begin
    Result := IPCClientIndex[i]^.Connect;
    if not Result then begin
      WriteLog(etError, Format('Connect failed: %d', [IPCClientIndex[i]^.Error]));
      Break;
    end;
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.Execute;
var
  xCount: DWord;
  xCoInitializeResult: HResult;
begin
  xCoInitializeResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xCoInitializeResult <> S_OK) and (xCoInitializeResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    mMsg.ThreadTyp := fThreadDef.ThreadTyp;

    try
      // confirm resume command to parent process
      if mConfirmStartupImmediately then begin
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d',
                                   [IPCClientIndex[cMainChannelIndex]^.Error]));
          Exit;
        end;
      end;

      // if not confirm or confirm succeeded loop
      while not Terminated do begin
        // read from channel if it does exist
        if mHasChannel then begin
          if fThreadDef.IPCRead.ReadDynamic(PByte(mJob), mJobSize, xCount) then begin
            if xCount <> mJob^.JobLen then
              WriteLog(etError, Format('JobSize and xCount are different: (ID:%d/ Typ:%s) %d/%d',
                       [mJob^.JobID,GetJobName(mJob^.JobTyp), mJob^.JobLen, xCount]), Nil, 0);
          end else
            ProcessError;
        end; // mHasChannel

        case fThreadDef.State of
          psConnected, psInit :
            // Jobs prcessed during system initialization
            ProcessInitJob;
          psRun :
            // it doesn't matter if thread has a channel to read from it. Call
            // ProcessJob anyway.
            ProcessJob;
        else
        end;
        // allows task switching
        Sleep(1);
      end; // while
    except
      on e:Exception do begin
        fError := SetError (ERROR_INVALID_FUNCTION, etDBError, Format('Thread stopped: %s %s, JobTyp: %d, RESTART MM' , [Self.Classname, e.message, ord(mJob^.JobTyp)] ) );
        WriteLog(etError, FormatMMErrorText(fError));
        WriteToMain(gcRestartMM, fError.Error);
      end;
    end;
  finally
    CoUninitialize;
  end;
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.GetConnectToIndex(aIndex: Byte): TThreadTyp;
begin
  Result := ttNone;
  if (fThreadDef.ClientCount > 0) and (aIndex > 0) and (aIndex <= fThreadDef.ClientCount) then
    Result := fThreadDef.IPCClientDef[aIndex].ConnectTo;
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.GetIPCClientIndex(aIndex: Byte): PIPCClient;
begin
  Result := Nil;
  if (fThreadDef.ClientCount > 0) and (aIndex > 0) and (aIndex <= fThreadDef.ClientCount) then
    Result := @fThreadDef.IPCClientDef[aIndex].IPCWrite;
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.GetIPCClientTyp(aThreadTyp: TThreadTyp): PIPCClient;
var
  i: Integer;
begin
  Result := Nil;
  for i:=1 to fThreadDef.ClientCount do
    if ConnectToIndex[i] = aThreadTyp then begin
      Result := @fThreadDef.IPCClientDef[i].IPCWrite;
      Break;
    end;
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.Init: Boolean;
var
  i: Integer;
begin
  Result := Inherited Init;

  // create client write connections
  for i:=1 to fThreadDef.ClientCount do
    IPCClientIndex[i]^ := TIPCClient.Create('.', cChannelNames[ConnectToIndex[i]], ThreadDef.IPCClientDef[i].Timeout);

  // create my own reading channel
  if mHasChannel then
    with fThreadDef do begin
      IPCRead := TIPCServer.Create(cChannelNames[ThreadTyp], mAccessGroup, sizeof(TJobRec), cChannelMainTimeOut, MaxChannels);
      Result  := IPCRead.Init;
      IPCRead.PipeWait := False;
    end;
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.ProcessError;
begin
  // an error is happened
  case fThreadDef.IPCRead.Error of
    ERROR_BROKEN_PIPE: begin
    end;
  else
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.ProcessInitJob;
begin
  WriteLog(etWarning, Format('Wrong JobTyp received (ProcessInitJob): %s', [GetJobName(mJob^.JobTyp)]), @mJob, cJobHeaderSize);
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.ProcessJob;
begin
  WriteLog(etWarning, Format('Wrong JobTyp received (ProcessJob): %s', [GetJobName(mJob^.JobTyp)]), @mJob, cJobHeaderSize);
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.SetDebugMode(aValue: Boolean);
const
  cBoolean: Array[Boolean] of String = ('False', 'True');
begin
  if aValue <> fDebugMode then begin
    fDebugMode := aValue;
    WriteLogDebug('Debug mode changed to: ' + cBoolean[fDebugMode], Nil, 0, True);
  end;
{
  if aValue <> fDebugMode then begin
    if aValue then begin
      fDebugMode := True;
      mEventLogDeb := TEventLogWriter.Create(cEventLogClassForSubSystemsDebug,
                                             cEventLogServerNameForSubSystems,
                                             fThreadDef.SubSystemTyp,
                                             cEventLogDefaultText[fThreadDef.ThreadTyp], True);
      WriteLogDebug('DebugMode enabled', Nil, 0);
    end else begin
      WriteLogDebug('DebugMode disabled', Nil, 0);
      Sleep(100);
      mEventLogDeb.Free;
      mEventLogDeb := Nil;
      fDebugMode := False;
    end;
  end;
{}
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.WriteJobBufferTo(aThreadTyp: TThreadTyp; aAlternativeJob: PJobRec): Boolean;
var
  xIPCClient: PIPCClient;
  xStr: String;
  xBuf: PByte;
  xCount: DWord;
begin
  Result := False;
  xBuf   := Nil;
  xCount := 0;

  if not Assigned(aAlternativeJob) then
    aAlternativeJob := mJob;

  CodeSite.SendFmtMsg('TBaseSubThread.WriteJobBufferTo: ThreadType=%s, JobID=%d, JobType=%d', [PChar(cEventLogDefaultText[aThreadTyp]), aAlternativeJob^.JobID, Ord(aAlternativeJob^.JobTyp)]);

  aAlternativeJob^.JobLen := GetJobDataSize(aAlternativeJob);
  xIPCClient := IPCClientTyp[aThreadTyp];
  // write only if IPCClient object exist
  if Assigned(xIPCClient) then begin
    Result := xIPCClient^.Write(PByte(aAlternativeJob), aAlternativeJob^.JobLen );
    if not Result then begin
      xStr := 'Write to %s failed: ' + FormatMMErrorText(xIPCClient^.Error, etNTError);
      xBuf := PByte(aAlternativeJob);
      xCount := cJobHeaderSize;
      WriteLog(etError, Format('Write to pipe %s failed twice by timeout(%d) in TBaseSubThread.WriteJobBufferTo ==>> RESTART MM. ',
               [cChannelNames[aThreadTyp],cIPCClientWriteTimeout]), xBuf, xCount);
      //Nonblocking write returns FALSE after 2 trials with timeout
      if not WriteToMain(gcRestartMM, fError.Error) then begin
        WriteLog(etError, Format('Confirm RestartMM in TBaseSubThread.WriteJobBufferTo failed: %d',
                             [IPCClientIndex[cMainChannelIndex]^.Error]));
        Exit;
      end;
    end;
  end else
    xStr := 'IPCClient connection to %s does not exist.' +
            FormatMMErrorText(SetError(ERROR_INVALID_HANDLE, etMMError));

  if not Result then
    WriteLog(etError, Format(xStr, [cChannelNames[aThreadTyp]]), xBuf, xCount);
{
  mJob.JobLen := GetJobDataSize(mJob.JobTyp);
  xIPCClient := IPCClientTyp[aThreadTyp];
  // write only if IPCClient object exist
  if Assigned(xIPCClient) then begin
    Result := xIPCClient^.Write(@mJob, mJob.JobLen );
    if not Result then begin
      xStr := 'Write to %s failed: ' + FormatMMErrorText(xIPCClient^.Error, etNTError);
      xBuf := @mJob;
      xCount := cJobHeaderSize;
    end;
  end else
    xStr := 'IPCClient connection to %s does not exist.' +
            FormatMMErrorText(SetError(ERROR_INVALID_HANDLE, etMMError));

  if not Result then
    WriteLog(etError, Format(xStr, [cChannelNames[aThreadTyp]]), xBuf, xCount);
{}
end;
//-----------------------------------------------------------------------------
procedure TBaseSubThread.WriteLogDebug(aText: String; aBuf: PByte; aCount: DWord; aUseEventLog: Boolean);
var
  i: Integer;
  xStr: String;
  xPChar: PChar;
begin
  // CodeSite will be enabled with set debug mode from MMClient or DebugTool
  if CodeSite.Enabled then begin
//  if fDebugMode then begin
    if aUseEventLog then begin
      WriteToEventLog(aText, cEventLogDefaultText[fThreadDef.ThreadTyp], etInformation, cEventLogClassForSubSystemsDebug, '.', ThreadDef.SubSystemTyp);
    end else begin
      if Assigned(aBuf) and (aCount > 0) then begin
        xPChar := PChar(aBuf);
        xStr   := '';
        for i:=1 to aCount do begin
          xStr := Format('%s %.2x', [xStr, Byte(xPChar^)]);
          inc(xPChar);
        end;
        CodeSite.SendString(aText, xStr);
      end else
        CodeSite.SendMsg(aText);
    end;
  end;
{
  if DebugMode then
    if Assigned(aBuf) and (aCount > 0) then
      mEventLogDeb.WriteBin(etInformation, aText, aBuf, aCount)
    else
      mEventLogDeb.Write(etInformation, aText);
{}
end;
//-----------------------------------------------------------------------------
function TBaseSubThread.WriteToMain(aMsg: TMMGuardCommand; aError: DWord): Boolean;
var
  xIPCClient: PIPCClient;
begin
  mMsg.MsgTyp      := aMsg;
  mMsg.Error.Error := aError;
  if aError = NO_ERROR then mMsg.Error.ErrorTyp := etNoError
                       else mMsg.Error.ErrorTyp := etMMError;

  xIPCClient  := IPCClientIndex[cMainChannelIndex];
  Result := xIPCClient^.Write(@mMsg, sizeof(mMsg));
  if not Result then
    WriteLog(etError, Format('Write to Main %s failed: %s',
                      [cChannelNames[ConnectToIndex[cMainChannelIndex]],
                      FormatMMErrorText(SetError(xIPCClient^.Error))]));
end;
//-----------------------------------------------------------------------------

end.

