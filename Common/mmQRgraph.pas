(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmQRGraph.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Quickreport - Grafik-Komponente
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.02.99 | 0.01 | PW  | Datei erstellt
| 02.03.99 | 0.02 | PW  | Aenderungen lt. Besprechung am 26.2.99
| 25.03.99 | 0.03 | PW  | Aenderungen lt. Besprechung am 17.3.99
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
|=========================================================================================*)

unit mmQRGraph;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,Buttons,Menus,Math,QuickRpt,
 mmGraphGlobal,mmGraph;

type
 TmmQRGraph = class(TQRPrintable)
  private
   FColors: TmmGraphColors;
   FBars: TmmGraphBars;
   FChartArea: TmmGraphChartArea;
   FData: TmmGraphData;
   FMargin: TmmGraphMargin;
   FLeftYAxis: TmmGraphLeftYAxis;
   FLegend: TmmGraphLegend;
   FLines: TmmGraphLines;
   FPaintBox: TPaintBox;
   FPopUp: TPopUpMenu;
   FRightYAxis: TmmGraphRightYAxis;
   FScrollBar: TScrollBar;
   FSeries: TmmGraphSeries;
   FSelection: TmmSelection;
   FStatistikLines: TmmGraphStatistikLines;
   FTitles: TmmGraphTitles;
   FXAxis: TmmGraphXAxis;
   FHorzGrid: TmmGraphHorzGrid;
   FVertGrid: TmmGraphVertGrid;

   FDateTickType: eDateticksT;
   FSelectedPoint: integer;
   FSelectedRow: integer;

   FReadOnly: boolean;
   procedure SetReadOnly(value: boolean);
   function GetNumOfBarSeries: integer;
  protected
   XScale: tXScaleRecordT;
   YScale: tYScaleRecordT;
   YStretch: array[eYAxisT] of integer;
   BarWidth: integer;
   IsPrinting,
   IsSelecting: boolean;

   qrXOffset,
   qrYOffset: integer;

   procedure DrawOnCanvas(aCanvas: TCanvas);
   procedure FPaintBoxPaint(Sender: TObject);

   function GetHorzTextPos(x1,x2,TextWidth,aGap: integer; Alignment: TAlignment): integer;
   function GetVertTextPos(y1,y2,TextHeight,aGap: integer; YAlignment: eYAlignmentT): integer;
   procedure SetTextDirection(aCanvas: TCanvas; aAngle: single);
   procedure CalcGraphCoordinates(aCanvas: TCanvas);

   procedure FScrollBar1Change(Sender: TObject);
   procedure FScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);

   procedure SetXScale(aMin,aMax: double);

   procedure CalcBarWidth;

   procedure SetBorderStyle(aCanvas: TCanvas; BorderStyle: TBorderStyle; BackgroundColor: TColor);
   procedure DrawPanelBackground(aCanvas: TCanvas);
   procedure DrawChartArea(aCanvas: TCanvas);
   procedure CalcLegendWidth(aCanvas: TCanvas);
   procedure DrawYAxis(aCanvas: TCanvas; aYAxis: eYAxisT);
   procedure DrawTitles(aCanvas: TCanvas);
   procedure DrawLegend(aCanvas: TCanvas);
   procedure GetScrollBarMinMax(var aMin,aMax: integer);
   procedure DrawVertGrid(aCanvas: TCanvas);
   procedure DrawXAxis(aCanvas: TCanvas);
   procedure DrawMainChart(aCanvas: TCanvas);
   procedure DrawStatistikLines(aCanvas: TCanvas);
   procedure CalcScrollBar;
   procedure ClipGraph(aCanvas: TCanvas);
   procedure unClipGraph(aCanvas: TCanvas);

   function GetLegendItemWidth(aCanvas: TCanvas): integer;
   function GetLegendItemHeight(aCanvas: TCanvas): integer;

   function GetDateStep(aDateTicks: eDateTicksT): double;
   function GetDateFormat(aDateTicks: eDateTicksT; aValue: double): string;
   function GetFirstDateTick(aMinDate: double): double;
   function GetNextDateTick(tick: double): double;

   function GetStep(aMinSteps: Word; aMin,aMax: double): double;
   procedure SetMinMax(var aMin,aMax,aStep: double; aAutoStep: boolean);
   function fy(v: double; aAxis: eYAxisT): integer;
   function fx(aPoint: integer; aXValue: ValueType): integer;

   function GetPrnFactX(aValue: double): integer;
   function GetPrnFactY(aValue: double): integer;
   function GetPrnOffsetX(aValue: double): integer;
   function GetPrnOffsetY(aValue: double): integer;

   function SetFontHeight(aFont: TFont): integer;
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;

   procedure CalcYAxis; //=> move to protected section

   property NumOfBarSeries: integer read GetNumOfBarSeries;
   procedure Print(aXOffset,aYOffset: integer); override;

  published
   //property Align;
   property Anchors;
   property Bars: TmmGraphBars read FBars write FBars;
   property ChartArea: TmmGraphChartArea read FChartArea write FChartArea;
   property Colors: TmmGraphColors read FColors write FColors;
   property Constraints;
   property Margin: TmmGraphMargin read FMargin write FMargin;
   property LeftYAxis: TmmGraphLeftYAxis read FLeftYAxis write FLeftYAxis;
   property Legend: TmmGraphLegend read FLegend write FLegend;
   property Lines: TmmGraphLines read FLines write FLines;
   property ReadOnly: boolean read FReadOnly write SetReadOnly;
   property RightYAxis: TmmGraphRightYAxis read FRightYAxis write FRightYAxis;
   property Titles: TmmGraphTitles read FTitles write FTitles;
   property Visible;
   property XAxis: TmmGraphXAxis read FXAxis write FXAxis;
   property HorzGrid: TmmGraphHorzGrid read FHorzGrid write FHorzGrid;
   property Data: TmmGraphData read FData write FData;
   property Series: TmmGraphSeries read FSeries write FSeries;
   property Selection: TmmSelection read FSelection write FSelection;
   property StatistikLines: TmmGraphStatistikLines read FStatistikLines write FStatistikLines;
   property VertGrid: TmmGraphVertGrid read FVertGrid write FVertGrid;
 end; //TmmQRGraph


procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.RES}

const
 cScrollBarHeight    = 25;
 cInitRepeatPause    = 400;
 cRepeatPause        = 100;
 cPointDiff          = 5;
 cScrollBtnWidth     = 22;
 cScrollBtnHeight    = 12;
 cScrollBtnGap       = 0;
 cMaxLegendWidth     = 250;
 cMaxMinBarsPerPage  = 200;
 cMaxBarWidth        = 50;

 cDiff_LegendText    = 7;
 cDiff_LegendItems   = 3;
 cDiff_LegendBorder  = 5;
 cDiff_AxisTicksText = 8;


procedure AddMonth(var aYear,aMonth: word; aMonthsToAdd: word);
begin
 if (aMonth +aMonthsToAdd) > 12 then
 begin
  inc(aYear);
  aMonth := aMonth +aMonthsToAdd -12;
 end //if (aMonth +aMonthsToAdd) > 12 then
 else inc(aMonth,aMonthsToAdd);
end; //procedure AddMonth


//-----------------------------------------------------------------------------


//TmmQRGraph ***
constructor TmmQRGraph.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);

 Align := alNone;
 BevelInner := bvNone;
 BevelOuter := bvNone;
 BevelWidth := 1;
 BorderWidth := 0;
 Caption := ' ';
 DoubleBuffered := true;
 Height := 300;
 Visible := true;
 Width := 500;

 YStretch[yaxLeft] := 0;
 YStretch[yaxRight] := 0;
 IsSelecting := false;
 IsPrinting := false;

 FSelectedPoint := -1;
 FSelectedRow := -1;
 FReadOnly := false;

 FChartArea := TmmGraphChartArea.Create(Self);
 FChartArea.BorderStyle := bsSingle;
 FChartArea.BackgroundColor := clLightGray2;

 FColors := TmmGraphColors.Create(self);
 FData := TmmGraphData.Create(self);
 FMargin := TmmGraphMargin.Create(self);
 FLeftYAxis := TmmGraphLeftYAxis.Create(Self);
 FLeftYAxis.Width := 60;
 FLeftYAxis.BorderStyle := bsNone;
 FLeftYAxis.RefreshOnSizeChange := [srWidth];

 FLegend := TmmGraphLegend.Create(Self);
 FLegend.Width := 150;

 FLines := TmmGraphLines.Create(Self);

 FRightYAxis := TmmGraphRightYAxis.Create(self);
 FRightYAxis.BorderStyle := bsNone;
 FRightYAxis.Width := 60;
 FRightYAxis.RefreshOnSizeChange := [srWidth];

 FXAxis := TmmGraphXAxis.Create(self);
 FXAxis.BorderStyle := bsNone;
 FXAxis.Height := 80;
 FXAxis.RefreshOnSizeChange := [srHeight];

 FTitles := TmmGraphTitles.Create(self);
 FTitles.RefreshOnSizeChange := [srHeight];

 FHorzGrid := TmmGraphHorzGrid.Create(Self);
 FVertGrid := TmmGraphVertGrid.Create(Self);

 FSeries := TmmGraphSeries.Create(Self);
 FSelection := TmmSelection.Create(Self);
 FStatistikLines := TmmGraphStatistikLines.Create(Self);

 FBars := TmmGraphBars.Create(Self);

 FPaintBox := TPaintBox.Create(Self);
 FPaintBox.Parent := Self;
 FPaintBox.OnPaint := FPaintBoxPaint;
 FPaintBox.Align := alClient;
 FPaintBox.PopUpMenu := FPopUp;

 FScrollBar := TScrollBar.Create(Self);
 FScrollBar.Parent := Self;
 FScrollBar.TabStop := false;
 FScrollBar.Visible := false; //***
 FScrollBar.OnChange := FScrollBar1Change;
 FScrollBar.OnScroll := FScrollBarScroll;

 XScale.actMin := 1; //min > max => indicator for initilization
 XScale.actMax := 0;
end; //constructor TmmQRGraph.Create
//-----------------------------------------------------------------------------
destructor TmmQRGraph.Destroy;
begin
 FBars.Free;
 FColors.Free;
 FData.Free;
 FMargin.Free;
 FLeftYAxis.Free;
 FLegend.Free;
 FRightYAxis.Free;
 FTitles.Free;
 FXAxis.Free;
 FHorzGrid.Free;
 FSeries.Free;
 FSelection.Free;
 FStatistikLines.Free;
 FVertGrid.Free;

 inherited Destroy;
end; //destructor TmmQRGraph.Destroy
//-----------------------------------------------------------------------------
function TmmQRGraph.GetLegendItemWidth(aCanvas: TCanvas): integer;
var
 i,xLen : integer;

begin
 result := 0;
 aCanvas.Font := FLegend.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 for i := 1 to Data.NumSeries do
 begin
  xLen := aCanvas.TextHeight('H')
         +GetPrnFactX(cDiff_LegendBorder)
         +GetPrnFactX(2 *cDiff_LegendText)
         +aCanvas.TextWidth(Series.Name[i]);
  result := math.max(xLen,result);
 end; //for i := 1 to Data.NumSeries do

 if result = 0 then result := GetPrnFactX(200);
end; //function TmmQRGraph.GetLegendItemWidth
//-----------------------------------------------------------------------------
function TmmQRGraph.GetLegendItemHeight(aCanvas: TCanvas): integer;
begin
 aCanvas.Font := FLegend.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 result := aCanvas.TextHeight('H') +GetPrnFactY(cDiff_LegendItems);
end; //function TmmQRGraph.GetLegendItemHeight
//-----------------------------------------------------------------------------
procedure TmmQRGraph.CalcBarWidth;
var
 xMinBW : integer;

begin
 xMinBW := GetPrnFactX(math.max(NumOfBarSeries,1));
 BarWidth := GetPrnFactX(FBars.DefaultBarWidth);

 case FXAxis.XAxisType of
  xaFixed: begin
   if FBars.MinBarsPerPage > 0 then
   begin
    if FBars.Style <> bsSide then xMinBW := 1;
    BarWidth := (FChartArea.Width div FBars.MinBarsPerPage) -GetPrnFactX(FBars.Gap);
    if BarWidth < xMinBW then BarWidth := xMinBW;
    if (NumOfBarSeries > 1) and (FBars.Style = bsSide) then BarWidth := BarWidth div NumOfBarSeries;
    if BarWidth > GetPrnFactX(FBars.DefaultBarWidth) then BarWidth := GetPrnFactX(FBars.DefaultBarWidth);
   end //if FBars.MinBarsPerPage > 0 then
   else BarWidth := GetPrnFactX(FBars.DefaultBarWidth);

   if (NumOfBarSeries > 1) and (FBars.Style = bsSide) then BarWidth := BarWidth *NumOfBarSeries;
  end; //xaFixed
 end; //case FXAxis.XAxisType of
end; //procedure TmmQRGraph.CalcBarWidth
//-----------------------------------------------------------------------------
procedure TmmQRGraph.FScrollBar1Change(Sender: TObject);
begin
 if FScrollBar.Visible then Refresh;
end; //procedure TmmQRGraph.FScrollBar1Change
//-----------------------------------------------------------------------------
procedure TmmQRGraph.FScrollBarScroll(Sender: TObject; ScrollCode: TScrollCode; var ScrollPos: Integer);
begin
 case XAxis.XAxisType of
  xaFixed: begin
  end; //
  xaFloat,xaDatetime: begin
   XScale.actMin := XScale.absMin +XScale.absDX *ScrollPos /1000;
   XScale.actMax := XScale.actMin +XScale.actDX;
  end; //xaFloat,xaDatetime
 end; //case XAxis.XAxisType of
end; //procedure TmmQRGraph.FScrollBarScroll
//-----------------------------------------------------------------------------
procedure TmmQRGraph.SetReadOnly(value: boolean);
begin
 if Value <> FReadOnly then
 begin
  FReadOnly := value;
  Refresh;
 end; //if Value <> FShowScrollButtons then
end; //procedure TmmQRGraph.SetReadOnly
//-----------------------------------------------------------------------------
function TmmQRGraph.GetNumOfBarSeries: integer;
var
 i : integer;

begin
 result := 0;
 for i := 1 to Data.NumSeries do
  if (Series.YAxis[i] = yaxRight) and Series.Visible[i] then inc(result);
end; //function TmmQRGraph.GetNumOfBarSeries
//-----------------------------------------------------------------------------
procedure TmmQRGraph.SetTextDirection(aCanvas: TCanvas; aAngle: single);
var
 LogRec: TLogFont;

begin
 GetObject(aCanvas.Font.Handle,sizeof(LogRec),@LogRec);
 LogRec.lfEscapement := round(aAngle *10);
 LogRec.lfOrientation := round(aAngle *10);
 aCanvas.Font.Handle := CreateFontIndirect(LogRec);
end; //procedure TmmQRGraph.SetTextDirection
//-----------------------------------------------------------------------------
function TmmQRGraph.GetHorzTextPos(x1,x2,TextWidth,aGap: integer; Alignment: TAlignment): integer;
begin
 //quickreport's textalignment form right to left
 if IsPrinting then
 begin
  case Alignment of
   taLeftJustify:  result := x1 +TextWidth +GetPrnFactX(aGap);
   taCenter:       result := x1 +(x2 -x1 +TextWidth) div 2;
   taRightJustify: result := x2 -GetPrnFactX(aGap);
  end; //case Alignment of
 end //if IsPrinting then
 else
 begin
  case Alignment of
   taLeftJustify:  result := x1 +GetPrnFactX(aGap);
   taCenter:       result := x1 +(x2 -(x1 +TextWidth)) div 2;
   taRightJustify: result := x2 -TextWidth -GetPrnFactX(aGap);
  end; //case Alignment of
 end; //else if IsPrinting then
end; //function TmmQRGraph.GetHorzTextPos
//-----------------------------------------------------------------------------
function TmmQRGraph.GetVertTextPos(y1,y2,TextHeight,aGap: integer; YAlignment: eYAlignmentT): integer;
begin
 case YAlignment of
  tyaTop:    result := y1 +GetPrnFactY(aGap);
  tyaCenter: result := y1 +(y2 -(y1 +TextHeight)) div 2;
  tyaBottom: result := y1 -TextHeight -GetPrnFactY(aGap);
 end; //case YAlignment of
end; //function TmmQRGraph.GetVertTextPos
//-----------------------------------------------------------------------------
procedure TmmQRGraph.CalcGraphCoordinates(aCanvas: TCanvas);
var
 xCols,
 xRows,
 xPos,
 yPos  : integer;
 hText : integer;

begin
 FLeftYAxis.Left := GetPrnFactX(FMargin.Left) +GetPrnOffsetX(0);
 FLeftYAxis.Right := FLeftYAxis.Left +GetPrnFactX(FLeftYAxis.Width);

 FChartArea.Left := FLeftYAxis.Right;
 FChartArea.Right := GetPrnFactX(Width -FMargin.Right);

 if FRightYAxis.Visible then FChartArea.Right := FChartArea.Right -GetPrnFactX(FRightYAxis.Width);
 if FLegend.Visible and (FLegend.Position = lpRight) then FChartArea.Right := FChartArea.Right -GetPrnFactX(FLegend.Width +FMargin.Graph_Legend);

 if FRightYAxis.Visible then
 begin
  FRightYAxis.Left := FChartArea.Right;
  FRightYAxis.Right := FRightYAxis.Left +GetPrnFactX(FRightYAxis.Width);
 end; //if FRightYAxis.Visible then

 if FLegend.Visible and (FLegend.Position = lpRight) then
 begin
  if FRightYAxis.Visible then FLegend.Left := FRightYAxis.Right +GetPrnFactX(FMargin.Graph_Legend)
                         else FLegend.Left := FChartArea.Right +GetPrnFactX(FMargin.Graph_Legend); //??
  FLegend.Right := FLegend.Left +GetPrnFactX(FLegend.Width);
 end; //if FLegend.Visible and (FLegend.Position = lpRight) then

 FChartArea.Width := FChartArea.Right -FChartArea.Left;

 FTitles.Left := FChartArea.Left;
 FTitles.Right := FChartArea.Right;

 FXAxis.Left := FChartArea.Left;
 FXAxis.Right := FChartArea.Right;

 FTitles.Top := GetPrnFactY(FMargin.Top) +GetPrnOffsetY(0);
 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 hText := aCanvas.TextHeight('H');
 aCanvas.Font := FTitles.SubTitleFont;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 hText := hText +aCanvas.TextHeight('H');
 FTitles.Bottom := FTitles.Top +hText +GetPrnFactY(3*cDiff_LegendBorder);

 if FLegend.Visible and (FLegend.Position = lpTop) then
 begin
  FLegend.Width := FChartArea.Right -FChartArea.Left;
  FLegend.Top := FTitles.Bottom +GetPrnFactY(FMargin.Title_Graph);
  FLegend.Left := FChartArea.Left;
  FLegend.Right := FChartArea.Right;

  xCols := FLegend.Width div GetLegendItemWidth(aCanvas);
  if xCols < 1 then xCols := 1;
  xRows := Data.NumSeries div xCols +1;
  if Data.NumSeries mod xCols = 0 then dec(xRows);

  FLegend.Height := xRows *GetLegendItemHeight(aCanvas) +GetPrnFactY(cDiff_LegendBorder);
  FLegend.Bottom := FLegend.Top +FLegend.Height;

  FChartArea.Top := FLegend.Bottom +GetPrnFactY(FMargin.Graph_Legend);
 end //if FLegend.Visible and (FLegend.Position = lpTop) then
 else FChartArea.Top := FTitles.Bottom +GetPrnFactY(FMargin.Title_Graph);

 FChartArea.Bottom := GetPrnFactY(Height) -GetPrnFactY(FMargin.Bottom +cScrollBarHeight +FXAxis.Height);
 FChartArea.Height := FChartArea.Bottom -FChartArea.Top;

 FLeftYAxis.Top := FChartArea.Top;
 FLeftYAxis.Bottom := FChartArea.Bottom;
 FLeftYAxis.Height := FLeftYAxis.Bottom -FLeftYAxis.Top;

 FRightYAxis.Top := FChartArea.Top;
 FRightYAxis.Bottom := FChartArea.Bottom;
 FRightYAxis.Height := FLeftYAxis.Height;

 if FLegend.Visible and (FLegend.Position = lpRight) then
 begin
  FLegend.Top := FChartArea.Top;
  FLegend.Bottom := FChartArea.Bottom;
  FLegend.Height := FRightYAxis.Height;
 end; //if FLegend.Visible and (FLegend.Position = lpRight) then

 FXAxis.Top := FChartArea.Bottom;
 FXAxis.Bottom := FXAxis.Top +GetPrnFactY(FXAxis.Height);
end; //procedure TmmQRGraph.CalcGraphCoordinates
//-----------------------------------------------------------------------------
procedure TmmQRGraph.SetBorderStyle(aCanvas: TCanvas; BorderStyle: TBorderStyle; BackgroundColor: TColor);
begin
 aCanvas.Pen.Width := 1;
 aCanvas.Pen.Style := psSolid;
 if IsPrinting then aCanvas.Brush.Color := clWhite
               else aCanvas.Brush.Color := BackgroundColor;
 if BorderStyle = bsSingle then aCanvas.Pen.Color := clBlack
                           else aCanvas.Pen.Color := aCanvas.Brush.Color;
 aCanvas.Brush.Style := bsSolid;
end; //procedure TmmQRGraph.SetBorderStyle
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawYAxis(aCanvas: TCanvas; aYAxis: eYAxisT);
const
 cMinDiff    = 6; //min-pixels between y-labels
 cTickLength = 4;

var
 xDrawHorzGrid : boolean;
 xTitleAngle,
 x1,x2,
 ySkip,
 dY,
 hText,
 wText,
 ticks,
 i,
 x,y           : integer;
 v             : double;
 hstr          : string;
 xYLeg         : TmmCustomGraphArea;

begin
 if aYAxis = yaxLeft then xYLeg := FLeftYAxis
                     else xYLeg := FRightYAxis;

 if not xYLeg.Visible then exit;

 SetBorderStyle(aCanvas,xYLeg.BorderStyle,xYLeg.BackgroundColor);

 aCanvas.Font := xYLeg.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 wText := aCanvas.TextWidth(xYLeg.Title) div 2;
 hText := aCanvas.TextHeight('H');
 y := FChartArea.Top +(FChartArea.Height div 2);

 if xYLeg.TitleOrientation = toUp then xTitleAngle := 90
                                  else xTitleAngle := 270;

 SetTextDirection(aCanvas,xTitleAngle);

 if aYAxis = yaxLeft then
 begin
  if xYLeg.BorderStyle = bsNone then x := xYLeg.Right
                                else x := xYLeg.Right +1;
  aCanvas.Rectangle(xYLeg.Left,xYLeg.Top,x,xYLeg.Bottom);
  if xYLeg.TitleOrientation = toUp then x := xYLeg.Left +GetPrnFactX(xYLeg.TitleMargin)
                                   else x := xYLeg.Left +GetPrnFactX(xYLeg.TitleMargin) +hText;
  x1 := xYLeg.Right;
  x2 := xYLeg.Right -GetPrnFactX(cTickLength);
  xDrawHorzGrid := FHorzGrid.GridType in [hgLeftAxis,hgLeftRightAxis];
 end //if aYAxis = yaxLeft then
 else
 begin
  if xYLeg.BorderStyle = bsNone then x := xYLeg.Left
                                else x := xYLeg.Left -1;
  aCanvas.Rectangle(x,xYLeg.Top,xYLeg.Right,xYLeg.Bottom);
  if xYLeg.TitleOrientation = toUp then x := xYLeg.Right -GetPrnFactX(xYLeg.TitleMargin) -hText
                                   else x := xYLeg.Right -GetPrnFactX(xYLeg.TitleMargin);
  x1 := xYLeg.Left;
  x2 := xYLeg.Left +GetPrnFactX(cTickLength);
  xDrawHorzGrid := FHorzGrid.GridType = hgRightAxis;
 end; //else if aYAxis = yaxLeft then

 if IsPrinting then
 begin
  if xYLeg.TitleOrientation = toUp then y := y -wText
                                   else y := y +wText;
 end //if IsPrinting then
 else
 begin
  if xYLeg.TitleOrientation = toUp then y := y +wText
                                   else y := y -wText;
 end; //else if IsPrinting then

 //aCanvas.Brush.Style := bsClear;
 aCanvas.TextOut(x,y,xYLeg.Title);
 SetTextDirection(aCanvas,0);

 hText := aCanvas.TextHeight('0') div 2;
 ticks := round((YScale.Max[aYAxis] -YScale.Min[aYAxis]) /YScale.Step[aYAxis]);
 ySkip := 1;

 dY := FChartArea.Height;
 y := ticks *(hText +GetPrnFactY(cMinDiff));
 if dY > 10 then
  while dY < (y /ySkip) do
   inc(ySkip);

 for i := 0 to ticks do
 begin
  v := YScale.Min[aYAxis] +(i*YScale.Step[aYAxis]);
  y := fy(v,aYAxis);

  aCanvas.Pen.Style := psSolid;
  aCanvas.Pen.Color := clBlack;
  aCanvas.Pen.Width := 1;

  if y >= fy(YScale.Max[aYAxis],aYAxis) then
  begin
   aCanvas.MoveTo(x1,y);
   aCanvas.LineTo(x2,y);

   if xDrawHorzGrid then
   begin
    aCanvas.Pen.Style := FHorzGrid.Style;
    aCanvas.Pen.Color := FHorzGrid.Color;
    aCanvas.Pen.Width := FHorzGrid.Width;

    //prevent drawing in/on first/last gridline
    if (y <> fy(YScale.Min[aYAxis],aYAxis)) and (y <> fy(YScale.Max[aYAxis],aYAxis)) then
    begin
     aCanvas.MoveTo(FChartArea.Left,y);
     aCanvas.LineTo(FChartArea.Right,y);
    end; //if (y <> fy(YScale.Min[aYAxis],aYAxis)) and (y <> fy(YScale.Max[aYAxis],aYAxis)) then
   end; //if xDrawHorzGrid then

   hstr := formatfloat(xYLeg.FormatStr,v);
   if aYAxis = yaxLeft then x := GetHorzTextPos(0,FChartArea.Left -GetPrnFactX(cDiff_AxisTicksText),aCanvas.TextWidth(hstr),0,taRightJustify)
                       else x := GetHorzTextPos(FChartArea.Right +GetPrnFactX(cDiff_AxisTicksText),0,aCanvas.TextWidth(hstr),0,taLeftJustify);
   y := y -hText;
   if (i mod ySkip) = 0 then aCanvas.TextOut(x,y,hstr);
  end; //if y > fy(YScale.Min[yaxLeft],yaxLeft) then
 end; //for i := 0 to ticks do
end; //procedure TmmQRGraph.DrawYAxis
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawTitles(aCanvas: TCanvas);
var
 x,
 y1,y2 : integer;

begin
 SetBorderStyle(aCanvas,FTitles.BorderStyle,FTitles.BackgroundColor);
 aCanvas.Rectangle(FTitles.Left,FTitles.Top,FTitles.Right,FTitles.Bottom +1);

 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 y1 := FTitles.Top +GetPrnFactY(cDiff_LegendBorder);
 y2 := FTitles.Top +GetPrnFactY(2*cDiff_LegendBorder) +aCanvas.TextHeight('H');

 aCanvas.Brush.Style := bsClear;

 aCanvas.Font := FTitles.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 x := GetHorzTextPos(FChartArea.Left,FChartArea.Right,aCanvas.TextWidth(FTitles.Title),5,FTitles.Alignment);
 aCanvas.TextOut(x,y1,FTitles.Title);

 aCanvas.Font := FTitles.SubTitleFont;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);
 x := GetHorzTextPos(FChartArea.Left,FChartArea.Right,aCanvas.TextWidth(FTitles.SubTitle),5,FTitles.Alignment);
 aCanvas.TextOut(x,y2,FTitles.SubTitle);
end; //procedure TmmQRGraph.DrawTitles
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawLegend(aCanvas: TCanvas);
var
 i,
 x,x1,y,
 hText,
 cCol,
 cRow,
 xCols : integer;

//-------------
procedure DrawLegendItem(aLegItem: integer; aSymbolRect,aTextRect: trect);
var
 xSymbolHeight: integer;
 wText        : integer;

begin
 with aCanvas do
 begin
  xSymbolHeight := aSymbolRect.Bottom -aSymbolRect.Top;

  Brush.Style := bsSolid;
  Brush.Color := Series.Color[aLegItem];
  Pen.Color := clBlack;
  Pen.Width := 1;

  if Series.YAxis[aLegItem] = yaxLeft then
  begin
   Pen.Color := Series.Color[aLegItem];
   Pen.Width := GetPrnFactY(3);
   MoveTo(aSymbolRect.Left,aSymbolRect.Top +xSymbolHeight div 2);
   LineTo(aSymbolRect.Right,aSymbolRect.Top +xSymbolHeight div 2);
  end //if Series.YAxis[aLegItem] = yaxLeft then
  else Rectangle(aSymbolRect.Left,aSymbolRect.Top,aSymbolRect.Right,aSymbolRect.Bottom);

  if Series.Visible[i] then Font.Style := []
                       else Font.Style := [fsItalic];

  Brush.Style := bsClear;
  wText := TextWidth(Series.Name[aLegItem]);
  if IsPrinting then TextRect(aTextRect,aTextRect.Left +wText,aTextRect.Top,Series.Name[aLegItem])
                else TextRect(aTextRect,aTextRect.Left,aTextRect.Top,Series.Name[aLegItem]);
 end; //with aCanvas do
end; //procedure DrawLegendItem
//-------------

begin //main
 if not FLegend.Visible then exit;

 SetBorderStyle(aCanvas,FLegend.BorderStyle,FLegend.BackgroundColor);
 aCanvas.Rectangle(FLegend.Left,FLegend.Top,FLegend.Right,FLegend.Bottom);

 with aCanvas do
 begin
  Font := FLegend.Font;
  Font.Height := SetFontHeight(Font);
  hText := TextHeight('H');

  for i := 1 to Data.NumSeries do
  begin
   if FLegend.Position = lpRight then
   begin
    x := FLegend.Left +GetPrnFactX(cDiff_LegendBorder);
    y := FLegend.Top +GetPrnFactY(cDiff_LegendBorder) +(i-1) *GetLegendItemHeight(aCanvas);
    x1 := FLegend.Left +GetPrnFactX(cDiff_LegendBorder +cDiff_LegendText) +hText;

    if (y +hText) < FLegend.Bottom then
     DrawLegendItem(i,rect(x+1,y+1,x+hText-1,y+hText-1),rect(x1,y-1,FLegend.Right -2,y+hText));
   end //if FLegend.Position = lpRight then
   else
   begin
    xCols := FLegend.Width div GetLegendItemWidth(aCanvas);
    if xCols < 1 then xCols := 1;

    cRow := (i div xCols) +1;
    cCol := i mod xCols;
    if cCol = 0 then
    begin
     cCol := xCols;
     dec(cRow);
    end; //if cCol = 0 then

    x := FLegend.Left +GetPrnFactX(cDiff_LegendBorder) +(cCol -1) *GetLegendItemWidth(aCanvas);
    y := FLegend.Top +GetPrnFactY(cDiff_LegendBorder) +(cRow-1) *GetLegendItemHeight(aCanvas);
    x1 := x +GetPrnFactX(cDiff_LegendText) +hText;

    DrawLegendItem(i,rect(x+1,y+1,x+hText-1,y+hText-1),rect(x1,y-1,FLegend.Right -2,y+hText));
   end; //else if FLegend.Position = lpRight then
  end; //for i := 1 to Data.NumSeries do
 end; //with aCanvas do
end; //procedure TmmQRGraph.DrawLegend
//-----------------------------------------------------------------------------
procedure TmmQRGraph.GetScrollBarMinMax(var aMin,aMax: integer);
begin
 if FScrollBar.Visible then
 begin
  aMin := FScrollBar.Position;
  aMax := FScrollBar.Position +FScrollBar.PageSize -1;
  if aMax > (Data.GetNumberOfData -1) then aMax := Data.GetNumberOfData -1;
 end //if FScrollBar.Visible then
 else
 begin
  aMin := 0;
  aMax := Data.GetNumberOfData -1;
  FScrollBar.Position := 0;
 end; //else if FScrollBar.Visible then
end; //procedure TmmQRGraph.GetScrollBarMinMax
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawVertGrid(aCanvas: TCanvas);
var
 x,i,
 ticks      : integer;
 xMin,
 xMax,
 xStep,
 xValue     : double;

//-------------
procedure DrawGridLine(x: integer);
begin
 aCanvas.MoveTo(x,FChartArea.Top);
 aCanvas.LineTo(x,FChartArea.Bottom);
end; //procedure DrawGridLine

//-------------

begin //main
 if not FVertGrid.Visible then exit;
 if XAxis.XAxisType = xaFixed then exit;

 aCanvas.Pen.Color := FVertGrid.Color;
 aCanvas.Pen.Style := FVertGrid.Style;
 aCanvas.Pen.Width := FVertGrid.Width;

 case XAxis.XAxisType of
  xaFloat: begin
   xMin := XScale.actMin;
   xMax := XScale.actMax;
   xStep := 0;
   SetMinMax(xMin,xMax,xStep,true);

   ticks := round((xMax -xMin) /xStep) -1;

   for i := 0 to ticks do
   begin
    xValue := xMin +(i*xStep);

    if ((xValue +cEpsilon) > XScale.actMin) and (xValue < XScale.actMax) then
    begin
     x := fx(0,xValue);
     DrawGridLine(x);
    end; //if (xValue > XScale.actMin) and (xValue < XScale.actMax) then
   end; //for i := 1 to XScale.Step do
  end; //xaFloat

  xaDateTime: begin
   FDateTickType := dtMinute;

   xStep := GetDatestep(FDateTickType);
   //while ((XScale.actMax -XScale.actMin) /xStep) > 18 {cMaxSteps} do
   while ((XScale.actMax -XScale.actMin) /xStep) > cMaxSteps do
   begin
    if FDateTickType = dtYear100 then break;
    inc(FDateTickType);
    xStep := GetDatestep(FDateTickType);
   end; //while ((FMax -FMin) /xStep) > cMaxSteps do

   xStep := GetDatestep(FDateTickType);
   xValue := GetFirstDateTick(XScale.actMin);
   x := fx(0,xValue);
   DrawGridLine(x);

   while (xValue +xStep) < XScale.actMax do
   begin
    xValue := GetNextDateTick(xValue);
    x := fx(0,xValue);
    DrawGridLine(x);
   end; //while xTick +FStep < FMax do
  end; //xaFloat,xaDateTime
 end; //for i := iMin to iMax do
end; //procedure TmmQRGraph.DrawVertGrid
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawXAxis(aCanvas: TCanvas);
const
 cLenXTicks = 4;

var
 x,y,i,
 iMin,
 iMax,
 ticks,
 wText,
 hText,
 lastX      : integer;
 xMin,
 xMax,
 xStep,
 xValue     : double;
 xLabel     : XLabelType;

//-------------
procedure DrawXLabel(x: integer; xLabel: string);
const
 cMinDiff = 4;

var
 CanPrnLabel : boolean;
 dx,dy,
 dxx,
 y           : integer;

begin
 wText := aCanvas.TextWidth(xLabel);
 hText := aCanvas.TextHeight(xLabel);

 aCanvas.MoveTo(x,FXAxis.Top);
 aCanvas.LineTo(x,FXAxis.Top +GetPrnFactX(cLenXTicks));

 if FXAxis.XAxisTextAngle = 0 then
 begin
  y := FXAxis.Top +GetPrnFactY(2*cLenXTicks);

  //quickreport => right to left !!!!
  if IsPrinting then
  begin
   x := x +(wText div 2);
   CanPrnLabel := (x +GetPrnFactX(cMinDiff)) < FChartArea.Right;
  end //if IsPrinting then
  else
  begin
   x := x -(wText div 2);
   CanPrnLabel := (x +wText +GetPrnFactX(cMinDiff)) < FChartArea.Right;
  end; //else if IsPrinting then

  if (x > lastX) and CanPrnLabel then
  begin
   aCanvas.TextOut(x,y,xLabel);
   lastX := x +wText +GetPrnFactX(cMinDiff);
  end; //if (x > lastX) and CanPrnLabel then
 end  //if FXAxis.XAxisTextAngle = 0 then
 else
 begin
  dy := round(sin(DegToRad(FXAxis.XAxisTextAngle))*wtext);
  dx := round(cos(DegToRad(FXAxis.XAxisTextAngle))*wtext);
  dxx := round(cos(DegToRad(90 -FXAxis.XAxisTextAngle))*htext) div 2;
  y := FXAxis.Top +dy +GetPrnFactY(2*cLenXTicks);

  //quickreport => right to left !!!!
  if IsPrinting then x := x //dummy
                else x := x -(dx+dxx);

  CanPrnLabel := (x +GetPrnFactX(cMinDiff)) < FChartArea.Right;

  if (x > lastX) and CanPrnLabel then
  begin
   aCanvas.TextOut(x,y,xLabel);
   lastX := x +(dx+dxx) +GetPrnFactX(cMinDiff);
  end; //if (x > lastX) and ((x +hText +cMinDiff) < FChartArea.Right) then
 end; //else if FXAxis.XAxisTextAngle = 0 then
end; //procedure DrawXLabel

//-------------

begin
 lastX := -999;

 SetBorderStyle(aCanvas,FXAxis.BorderStyle,FXAxis.BackgroundColor);
 if FXAxis.BorderStyle = bsNone then y := FXAxis.Top
                                else y := FXAxis.Top -1;
 aCanvas.Rectangle(FChartArea.Left,y,FChartArea.Right,FXAxis.Bottom);

 aCanvas.Pen.Color := clBlack;
 aCanvas.Font := FXAxis.Font;
 aCanvas.Font.Height := SetFontHeight(aCanvas.Font);

 if XAxis.XAxisType = xaFixed then GetScrollBarMinMax(iMin,iMax);

 wText := aCanvas.TextWidth(FXAxis.Title);
 hText := aCanvas.TextHeight('H');
 x := GetHorzTextPos(FChartArea.Left,FChartArea.Right,wText,0,taCenter);
 y := FXAxis.Bottom -hText -GetPrnFactY(FXAxis.TitleMargin);
 aCanvas.TextOut(x,y,FXAxis.Title);

 SetTextDirection(aCanvas,FXAxis.XAxisTextAngle);

 case XAxis.XAxisType of
  xaFixed: begin
   for i := iMin to iMax do
   begin
    x := fx(i,Data.GetXValue(1,i));
    //xLabel := Data.Data.Values[i].XValue.Name;
    DrawXLabel(x,xLabel);
   end; //for i := iMin to iMax do
  end; //xaFixed

  xaFloat: begin
   xMin := XScale.actMin;
   xMax := XScale.actMax;
   xStep := 0;
   SetMinMax(xMin,xMax,xStep,true);

   ticks := round((xMax -xMin) /xStep) -1;

   for i := 0 to ticks do
   begin
    xValue := xMin +(i*xStep);

    if ((xValue +cEpsilon) > XScale.actMin) and (xValue < XScale.actMax) then
    begin
     x := fx(0,xValue);
     xLabel := formatfloat(XAxis.XValueFormat,xValue);
     DrawXLabel(x,xLabel);
    end; //if (xValue > XScale.actMin) and (xValue < XScale.actMax) then
   end; //for i := 1 to XScale.Step do
  end; //xaFloat,xaDateTime

  xaDateTime: begin
   FDateTickType := dtMinute;

   xStep := GetDatestep(FDateTickType);
   while ((XScale.actMax -XScale.actMin) /xStep) > cMaxSteps do
   begin
    if FDateTickType = dtYear100 then break;
    inc(FDateTickType);
    xStep := GetDatestep(FDateTickType);
   end; //while ((FMax -FMin) /xStep) > cMaxSteps do

   xStep := GetDatestep(FDateTickType);

   xValue := GetFirstDateTick(XScale.actMin);
   x := fx(0,xValue);
   xLabel := GetDateFormat(FDateTickType,xValue);

   DrawXLabel(x,xLabel);

   while (xValue +xStep) < XScale.actMax do
   begin
    xValue := GetNextDateTick(xValue);
    x := fx(0,xValue);
    xLabel := GetDateFormat(FDateTickType,xValue);
    DrawXLabel(x,xLabel);
   end; //while xTick +FStep < FMax do
  end; //xaFloat,xaDateTime
 end; //for i := iMin to iMax do

 SetTextDirection(aCanvas,0);
end; //procedure TmmQRGraph.DrawXAxis
//-----------------------------------------------------------------------------
procedure TmmQRGraph.ClipGraph(aCanvas: TCanvas);
var
 ClipRgn: HRgn;

begin
 ClipRgn := CreateRectRgn(FChartArea.Left,FChartArea.Top+1,FChartArea.Right,FChartArea.Bottom);
 SelectClipRgn(aCanvas.Handle,ClipRgn);
 DeleteObject(ClipRgn);
end; //procedure TmmQRGraph.ClipGraph
//-----------------------------------------------------------------------------
procedure TmmQRGraph.UnclipGraph(aCanvas: TCanvas);
var
 ClipRgn: HRgn;

begin
 //??
 ClipRgn := CreateRectRgn(0,0,GetPrnFactX(FPaintBox.Width),GetPrnFactY(FPaintBox.Height));
 SelectClipRgn(aCanvas.Handle,ClipRgn);
 DeleteObject(ClipRgn);
end; //procedure TmmQRGraph.UnclipGraph
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawMainChart(aCanvas: TCanvas);
var
 iMin,iMax : integer;

//-----------

procedure DrawLimitArea(xRow: byte; p1,p2: tpoint);
var
 x,y       : integer;
 m         : double;
 yUpperLim,
 yLowerLim : integer;

begin
 try
  m := (p2.y -p1.y) /(p2.x -p1.x);
 except
  exit;
 end;   //try..except

 yUpperLim := fy(FSeries.UpperLimit[xRow],yaxLeft);
 yLowerLim := fy(FSeries.LowerLimit[xRow],yaxLeft);

 with aCanvas do
 begin
  for x := p1.x to p2.x do
  begin
   y := p1.y +round((x -p1.x) *m);

   if (stLowerLimit in Series.ShowStat[xRow]) and (y > yLowerLim) then
   begin
    Pen.Color := FSeries.LowerLimitColor[xRow];
    MoveTo(x,y-1);
    LineTo(x,yLowerLim);
   end; //if y > yLowerLim then

   if (stUpperLimit in Series.ShowStat[xRow]) and (y < yUpperLim) then
   begin
    Pen.Color := FSeries.UpperLimitColor[xRow];
    MoveTo(x,y+1);
    LineTo(x,yUpperLim);
   end; //if y > yLowerLim then
  end; //for x := p1.x to p2.x do
 end; //with aCanvas do
end; //procedure DrawLimitArea

//-----------

procedure DrawLineSeries;
const
 cdy0    = 2;
 cdy1    = 0;
 cMaxRes = 30000;

type
 tSmoothRecordT = record
  Count  : integer;
  XValue,
  YValue : double;
 end; //tSmoothRecordT

 smRecArray = array[1..cMaxRes] of tSmoothRecordT;

var
 xRow        : byte;
 x,
 xLast,
 yLast,
 y,i         : integer;
 LastPoint,
 yP          : tYPointT;
 smPointGap,
 smINT       : integer;
 smXValue,
 smDX        : double;
 smRec       : smRecArray;

begin
 for xRow := 1 to Data.NumSeries do
  if (Series.YAxis[xRow] = yaxLeft) and Series.Visible[xRow] then
  begin
   LastPoint.IsNull := true;

   aCanvas.Brush.Color := Series.Color[xRow];
   aCanvas.Pen.Color := Series.Color[xRow];
   aCanvas.Pen.Style := psSolid;
   aCanvas.Pen.Width := 1;
   //aCanvas.Pen.Width := 2;

   case XAxis.XAxisType of
    xaFixed: begin
     aCanvas.Brush.Style := bsClear;

     for i := iMin to iMax do
     begin
      x := fx(i,Data.GetXValue(xRow,i));
      yP := Data.GetYValue(xRow,i);

      if not yP.IsNull then
      begin
       y := fy(yP.Value,yaxLeft);

       aCanvas.Pen.Color := Series.Color[xRow];

       if i = FSelectedPoint then aCanvas.Rectangle(x-GetPrnFactX(cdy0),y-GetPrnFactY(cdy0),x+GetPrnFactX(cdy0),y+GetPrnFactY(cdy0))
                             else aCanvas.Rectangle(x-GetPrnFactX(cdy1),y-GetPrnFactY(cdy1),x+GetPrnFactX(cdy1),y+GetPrnFactY(cdy1));

       if not LastPoint.IsNull then
       begin
        yLast := fy(LastPoint.Value,yaxLeft);
        aCanvas.MoveTo(xLast,yLast);
        aCanvas.LineTo(x,y);
        DrawLimitArea(xRow,point(xLast,yLast),point(x,y));
       end; //if not LastPoint.IsNull then
      end; //if not yP.IsNull then

      LastPoint := yP;
      xLast := x;
     end; //for i := iMin to iMax do
    end; //xaFixed

    xaFloat,xaDateTime: begin
     iMin := Data.FindPoint(XScale.actMin,ftNext);
     if iMin < 0 then iMin := Data.GetNumberOfData;
     iMax := Data.FindPoint(XScale.actMax,ftPrior);

     fillchar(smRec,sizeof(smRecArray),0);

     //??
     if FLines.SmoothLines then smPointGap := GetPrnFactX(FLines.MinPointGap)
                           else smPointGap := GetPrnFactX(1); //=> max resolution

     smDX := XScale.actDX /(FChartArea.Width /smPointGap);

     //insert data into intervalls
     for i := iMin to iMax do
     begin
      yP := Data.GetYValue(xRow,i);
      smXValue := Data.GetXValue(xRow,i);
      smINT := trunc((smXValue -XScale.actMin) /smDX +1);

      if (smINT < 1) or (smINT > cMaxRes) then smINT := cMaxRes;

      if not yP.IsNull then
      begin
       smRec[smINT].YValue := smRec[smINT].YValue +yP.Value;
       smRec[smINT].XValue := smRec[smINT].XValue +smXValue;
       inc(smRec[smINT].Count);
      end; //if not yP.IsNull then
     end; //for i := iMin to iMax do

     iMin := 1;
     iMax := round(FChartArea.Width /smPointGap +0.5 -cEpsilon);

     LastPoint.IsNull := true;

     for i := iMin to iMax do
     begin
      if smRec[i].Count > 0 then
      begin
       x := fx(0,(smRec[i].XValue /smRec[i].Count));
       y := fy((smRec[i].YValue /smRec[i].Count),yaxLeft);

       aCanvas.Pen.Color := Series.Color[xRow];
       aCanvas.Brush.Style := bsClear;
       aCanvas.Rectangle(x-GetPrnFactX(cdy1),y-GetPrnFactY(cdy1),x+GetPrnFactX(cdy1),y+GetPrnFactY(cdy1));

       if not LastPoint.IsNull then
       begin
        yLast := fy(LastPoint.Value,yaxLeft);
        aCanvas.MoveTo(xLast,yLast);
        aCanvas.LineTo(x,y);
        DrawLimitArea(xRow,point(xLast,yLast),point(x,y));
       end; //if not LastPoint.IsNull then

       LastPoint.IsNull := false;
       LastPoint.Value := smRec[i].YValue /smRec[i].Count;
       xLast := x;
      end; //if smRec[i].Count > 0 then
     end; //for i := iMin to iMax do
    end; //xaFloat,xaDateTime
   end; //case XAxis.XAxisType of
  end; //if (Series.YAxis[xRow] = yaxLeft) and Series.Visible[xRow] then
end; //procedure DrawLineSeries

//-----------

procedure DrawBarSeries;
var
 xRow,
 xRowCounter : byte;
 xBarWidth,
 lastY,
 x,
 x1,x2,
 y100,
 y0,y1,
 y2,dy,i     : integer;
 yP,ySUM     : tYPointT;
 bmpBrush    : TBitmap;
 xBarRect    : trect;

//-----------
procedure SetBarStyle(x1,y1,x2,y2: integer);
var
 xInvRgn: HRgn;

begin
 xInvRgn := CreateRectRgn(x1,y1,x2,y2);
 InvertRgn(aCanvas.Handle,xInvRgn);
 DeleteObject(xInvRgn);
end; //procedure SetBarStyle
//-----------

procedure DrawBar;
begin
 if (i = FSelectedPoint) and FSelection.Enabled then
 begin
  case FSelection.Style of
   ssInverted: aCanvas.Brush.Color := FSeries.Color[xRow] xor clWhite;
   ssGray:     aCanvas.Brush.Color := FSeries.Color[xRow] and clGray; //rgb(160,160,160);
   ssFixed:    aCanvas.Brush.Color := FSelection.FixedColor;
  end; //case FSelection.Style of

  if FSelection.Style = ssFixed then aCanvas.Pen.Color := clBlack
                                else aCanvas.Pen.Color := aCanvas.Brush.Color;
  aCanvas.Rectangle(xBarRect.Left,xBarRect.Top,xBarRect.Right,xBarRect.Bottom);
 end //if (i = FSelectedPoint) and FSelection.Enabled then
 else
 begin
  aCanvas.Brush.Color := Series.Color[xRow];
  aCanvas.Pen.Color := aCanvas.Brush.Color;
  aCanvas.Rectangle(xBarRect.Left,xBarRect.Top,xBarRect.Right,xBarRect.Bottom);
 end; //if (i = FSelectedPoint) and FSelection.Enabled then
end; //procedure DrawBar

//-----------

begin //main DrawBarSeries
 if NumOfBarSeries = 0 then exit;
 xRowCounter := 0;
 xBarWidth := BarWidth div NumOfBarSeries;

 bmpBrush := TBitmap.Create;
 bmpBrush.Monochrome := false;
 bmpBrush.Width := 8;
 bmpBrush.Height := 8;
 try
  case FBars.Style of
   bsSide: begin
    for xRow := 1 to Data.NumSeries do
     if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
     begin
      aCanvas.Pen.Style := psSolid;
      aCanvas.Pen.Width := 1;

      for i := iMin to iMax do
      begin
       x := fx(i,Data.GetXValue(xRow,i));
       yP := Data.GetYValue(xRow,i);

       if not yP.IsNull then
       begin
        y1 := fy(yP.Value,yaxRight);

        x1 := x -(BarWidth div 2) +xBarWidth *xRowCounter;
        x2 := x1 +xBarWidth;

        xBarRect := rect(x1,y1,x2,FXAxis.Top -1);
        DrawBar;
       end; //if not yP.IsNull then
      end; //for i := iMin to iMax do

      inc(xRowCounter);
     end; //if (Series.YAxis[xRow] = aYAxis) and Series.Visible[xRow] then
   end; //bsSide

   bsStacked,bsStacked100: begin
    y0 := fy(0,yaxRight);
    y100 := fy(100,yaxRight);
    for i := iMin to iMax do
    begin
     lastY := y0;
     ySUM := Data.GetYValue(0,i);
     x := fx(i,Data.GetXValue(xRow,i));

     for xRow := 1 to Data.NumSeries do
      if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
      begin
       aCanvas.Pen.Color := Series.Color[xRow];
       aCanvas.Pen.Style := psSolid;
       aCanvas.Pen.Width := 1;

       yP := Data.GetYValue(xRow,i);

       if not yP.IsNull then
       begin
        if FBars.Style = bsStacked then
        begin
         dy := y0 -fy(yP.Value,yaxRight);
        end //if FBars.Style = bsStacked then
        else
        begin
         try
          dy := y0 -fy(yP.Value /ySUM.Value *100,yaxRight);
         except
          dy := lastY;
         end; //try..except
        end; //else if FBars.Style = bsStacked then

        y1 := lastY;
        y2 := lastY -dy;

        //anpassung an rundungsfehler
        if FBars.Style = bsStacked100 then
         if abs(y2 -y100) < 2 then y2 := y100;

        x1 := x -(BarWidth div 2);
        x2 := x1 +BarWidth;

        xBarRect := rect(x1,y2,x2,y1); //y2,y1 korrekt !!!
        DrawBar;

        lastY := y2;
       end; //if not yP.IsNull then
      end; //if (Series.YAxis[xRow] = yaxRight) and Series.Visible[xRow] then
    end; //for i := iMin to iMax do
   end; //bsStacked,bsStacked100
  end; //case FBars.Style of

  aCanvas.Pen.Style := psSolid;
  aCanvas.Pen.Color := clBlack;
  aCanvas.Pen.Width := 1;
 finally
  bmpBrush.free;
 end; //try..finally
end; //procedure DrawBarSeries

//-----------

begin //main
 GetScrollBarMinMax(iMin,iMax);

 ClipGraph(aCanvas);
 try
  if XAxis.XAxisType = xaFixed then DrawBarSeries;
  DrawLineSeries;
 finally
  UnClipGraph(aCanvas);

  if IsPrinting then
  begin
   aCanvas.MoveTo(FChartArea.Left,FChartArea.Bottom-1);
   aCanvas.LineTo(FChartArea.Right,FChartArea.Bottom-1);
  end; //if IsPrinting then
 end; //try..finally
end; //procedure TmmQRGraph.DrawMainChart
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawStatistikLines(aCanvas: TCanvas);
var
 xRow   : byte;
 y1,y2  : integer;
 xYAxis : eYAxisT;
 xStat  : eStatistikT;

//----------
procedure DrawLine(y: integer);
begin
 aCanvas.MoveTo(FChartArea.Left,y);
 aCanvas.LineTo(FChartArea.Right,y);
end; //procedure DrawLine
//----------

begin //main
 ClipGraph(aCanvas);
 try
  for xRow := 1 to Data.NumSeries do
  begin
   xYAxis := Series.YAxis[xRow];

   //no stat-lines if bars are stacked or xaxis = xaFloat
   if (xYAxis = yaxLeft) or
      ((xYAxis = yaxRight) and (FBars.Style = bsSide) and (FXAxis.XAxisType = xaFixed)) then
   begin
    aCanvas.Brush.Color := Series.Color[xRow];
    aCanvas.Pen.Color := Series.Color[xRow];

    for xStat := stMin to stUpperLimit do
     if xStat in Series.ShowStat[xRow] then
     begin
      aCanvas.Pen.Style := StatistikLines.Style[xStat];
      aCanvas.Pen.Width := StatistikLines.Width[xStat];

      case xStat of
       stMin,stMax,stMean: begin
        //y1 := fy(Data.Data.Statistik[xRow,xStat],xYAxis);
        DrawLine(y1);
       end; //stMin,stMax,stMean

       stStdDev,stQ90,stQ95,stQ99: begin
        //y1 := fy(Data.Data.Statistik[xRow,stMean] +Data.Data.Statistik[xRow,xStat],xYAxis);
        //y2 := fy(Data.Data.Statistik[xRow,stMean] -Data.Data.Statistik[xRow,xStat],xYAxis);
        DrawLine(y1);
        DrawLine(y2);
       end; //stStdDev,stQ90,stQ95,stQ99

       stUpperLimit: begin
        if stUpperLimit in FSeries.ShowStat[xRow] then
        begin
         y1 := fy(FSeries.UpperLimit[xRow],xYAxis);
         DrawLine(y1);
        end; //if stUpperLimit in FSeries.ShowStat[xRow] then
       end; //stUpperLimit

       stLowerLimit: begin
        if stLowerLimit in FSeries.ShowStat[xRow] then
        begin
         y1 := fy(FSeries.LowerLimit[xRow],xYAxis);
         DrawLine(y1);
        end; //if stLowerLimit in FSeries.ShowStat[xRow] then
       end; //stLowerLimit
      end; //case xStat of
     end; //if xStat in Series.ShowStat[xRow] then
   end; //if not((xYAxis = yaxRight) and (FBars.Style <> bsSide)) then
  end; //for xRow := 1 to Data.NumSeries do
 finally
  UnClipGraph(aCanvas);
 end; //try..finally
end; //procedure TmmQRGraph.DrawStatistikLines
//-----------------------------------------------------------------------------
procedure TmmQRGraph.SetXScale(aMin,aMax: double);
const
 maxXZoom = 100;

var
 min_actDX : double;

begin
 XScale.actMin := aMin;
 XScale.actMax := aMax;
 XScale.absMin := Data.XMin;
 XScale.absMax := Data.XMax;
 XScale.absDX := XScale.absMax -XScale.absMin;

 min_actDX := XScale.absDX /maxXZoom;
 if abs(XScale.actMax -XScale.actMin) < min_actDX then XScale.actMax := XScale.actMin +min_actDX;

 XScale.actDX := XScale.actMax -XScale.actMin;
 FScrollBar.Visible := XScale.actDX < (XScale.absDX -cEpsilon);
 FScrollBar.Max := 1000;

 try
  if FScrollBar.Visible then FScrollBar.PageSize := round(XScale.actDX /XScale.absDX *1000)
                        else FScrollBar.PageSize := FScrollBar.Max;
  FScrollBar.Position := round((XScale.actMin -XScale.absMin) /XScale.absDX *1000);
 except
  FScrollBar.Visible := false;
  FScrollBar.Position := 0;
 end; //try..except
end; //procedure TmmQRGraph.SetXScale
//-----------------------------------------------------------------------------
procedure TmmQRGraph.CalcScrollBar;
var
 PointsPerPage : integer;

begin
 FScrollBar.Left := FChartArea.Left;
 FScrollBar.Width := FChartArea.Width;
 FScrollBar.Top := GetPrnFactY(FPaintBox.Height -FMargin.Bottom -cScrollBarHeight +7);

 //scrollbar position
 if XAxis.XAxisType = xaFixed then
 begin
  //??
  if IsPrinting then
  begin
   PointsPerPage := FChartArea.Width div (BarWidth +GetPrnFactX(FBars.Gap));
  end
  else PointsPerPage := FChartArea.Width div (BarWidth +GetPrnFactX(FBars.Gap));

  FScrollBar.Visible := Data.GetNumberOfData > PointsPerPage;
  FScrollBar.Max := Data.GetNumberOfData;
  if FScrollBar.Visible then FScrollBar.PageSize := PointsPerPage
                        else FScrollBar.PageSize := FScrollBar.Max;
 end; //if XAxis.XAxisType = xaFixed then

 FScrollBar.Min := 0;
 FScrollBar.SmallChange := FScrollBar.Max div 100;
 FScrollBar.LargeChange := FScrollBar.Max div 10;
end; //procedure TmmQRGraph.CalcScrollBar
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawPanelBackground(aCanvas: TCanvas);
begin
 if IsPrinting then SetBorderStyle(aCanvas,bsNone,clWhite)
               else SetBorderStyle(aCanvas,bsNone,FColors.PanelBackground);
 if not IsPrinting then
  aCanvas.Rectangle(0,0,GetPrnFactX(FPaintBox.Width),GetPrnFactY(FPaintBox.Height));
end; //procedure TmmQRGraph.DrawPanelBackground
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawChartArea(aCanvas: TCanvas);
begin
 SetBorderStyle(aCanvas,FChartArea.BorderStyle,FChartArea.BackgroundColor);
 aCanvas.Rectangle(FChartArea.Left,FChartArea.Top,FChartArea.Right,FChartArea.Bottom);
end; //procedure TmmQRGraph.DrawChartArea
//-----------------------------------------------------------------------------
procedure TmmQRGraph.CalcLegendWidth(aCanvas: TCanvas);
begin
 case FLegend.Position of
  lpRight: begin
   FLegend.Width := GetLegendItemWidth(aCanvas) +GetPrnFactX(20);
   FLegend.Width := math.min(FLegend.Width,GetPrnFactX(cMaxLegendWidth));
  end; //case FLegend.Position of
 end; //case FLegend.Position of
end; //procedure TmmQRGraph.CalcLegendWidth
//-----------------------------------------------------------------------------
procedure TmmQRGraph.DrawOnCanvas(aCanvas: TCanvas);
begin
 if (Data.SortOrder = soNone) then Data.SortByRow(0,soAscending);
 if XAxis.XAxisType in [xaFloat,xaDateTime] then
  if (Data.SortRow <> 0) or (Data.SortOrder <> soAscending) then Data.SortByRow(0,soAscending);

 CalcLegendWidth(aCanvas);
 CalcGraphCoordinates(aCanvas);
 CalcBarWidth;
 CalcYAxis;

 DrawPanelBackground(aCanvas);

 CalcScrollBar;
 if XAxis.XAxisType in [xaFloat,xaDateTime] then
  if XScale.actMin >= XScale.actMax then
  begin
   Data.SetXMinMax;
   SetXScale(Data.XMin,Data.XMax);
  end; //if XScale.actMin >= XScale.actMax then

 DrawChartArea(aCanvas);
 DrawTitles(aCanvas);
 DrawLegend(aCanvas);
 DrawXAxis(aCanvas);
 if not FVertGrid.Foreground then DrawVertGrid(aCanvas);
 if FHorzGrid.Foreground then DrawMainChart(aCanvas);
 DrawYAxis(aCanvas,yaxLeft);
 DrawYAxis(aCanvas,yaxRight);
 if not FHorzGrid.Foreground then DrawMainChart(aCanvas);
 if FVertGrid.Foreground then DrawVertGrid(aCanvas);
 DrawStatistikLines(aCanvas);
end; //procedure TmmQRGraph.DrawOnCanvas
//-----------------------------------------------------------------------------
procedure TmmQRGraph.FPaintBoxPaint(Sender: TObject);
begin
 DrawOnCanvas(FPaintBox.Canvas);
end; //procedure TmmQRGraph.FPaintBoxPaint
//-----------------------------------------------------------------------------
function TmmQRGraph.GetPrnFactX(aValue: double): integer;
var
 Factor : double;

begin
 if IsPrinting then
 begin
  Factor := Size.Width /Width;
  result := ParentReport.QRPrinter.XSize(aValue *Factor);
 end //if IsPrinting then
 else result := round(aValue);
end; //function TmmQRGraph.GetPrnFactX
//-----------------------------------------------------------------------------
function TmmQRGraph.GetPrnFactY(aValue: double): integer;
var
 Factor : double;

begin                              
 if IsPrinting then
 begin
  Factor := Size.Height /Height;
  result := ParentReport.QRPrinter.YSize(aValue *Factor);
 end //if IsPrinting then
 else result := round(aValue);
end; //function TmmQRGraph.GetPrnFactY
//-----------------------------------------------------------------------------
function TmmQRGraph.GetPrnOffsetX(aValue: double): integer;
begin
 if IsPrinting then result := ParentReport.QRPrinter.XPos(Size.Left +qrXOffset +aValue)
               else result := 0;
end; //function TmmQRGraph.GetPrnOffsetX
//-----------------------------------------------------------------------------
function TmmQRGraph.GetPrnOffsetY(aValue: double): integer;
begin
 if IsPrinting then result := ParentReport.QRPrinter.YPos(Size.Top +qrYOffset +aValue)
               else result := 0;
end; //function TmmQRGraph.GetPrnOffsetY
//-----------------------------------------------------------------------------
function TmmQRGraph.SetFontHeight(aFont: TFont): integer;
var
 fHeight : double;

begin
 fHeight := -aFont.Size *aFont.PixelsPerInch /72;
 result := round(fHeight);
 //if IsPrinting then result := round(PInfo.prnHeight[puPix] /PInfo.yPix *fHeight)
 //              else result := round(fHeight);
end; //function TmmQRGraph.SetFontHeight
//-----------------------------------------------------------------------------
procedure TmmQRGraph.Print(aXOffset,aYOffset: integer);
begin
 qrXOffset := aXOffset;
 qrYOffset := aYOffset;

 IsPrinting := true;
 try
  DrawOnCanvas(ParentReport.QRPrinter.Canvas);
 finally
  IsPrinting := false;
 end; //try..finally
end; //procedure TmmQRGraph.Print
//-----------------------------------------------------------------------------
function TmmQRGraph.GetDateFormat(aDateTicks: eDateTicksT; aValue: double): string;
var
 xFormatStr : string;
 y,m,d,
 h,mm,s,hs  : word;

begin
 if XAxis.XValueFormat = '' then
 begin
  DecodeDate(aValue,y,m,d);
  DecodeTime(aValue,h,mm,s,hs);

  case aDateTicks of
   dtMinute..dtMinute30: begin
    if (h = 0) and (mm = 0) then xFormatStr := 'dd/mm/yy'
                            else xFormatStr := 'hh:nn';
   end; //dtMinute..dtMinute30
   dtHour..dtHour12: begin
    if (h = 0) and (mm = 0) then xFormatStr := 'dd/mm/yy'
                            else xFormatStr := 'hh:nn';
   end; //dtHour..dtHour12
   dtDay,dtWeek: xFormatStr := 'dd/mm/yyyy';
   dtMonth..dtYear: xFormatStr := 'mmm yyyy';
   dtYear10,dtYear100: xFormatStr := 'yyyy';
  end; //case aDateTicks of
 end //if XAxis.XValueFormat = '' then
 else xFormatStr := XAxis.XValueFormat;
 result := FormatDateTime(xFormatStr,aValue);
end; //function TmmQRGraph.GetDateFormat
//-----------------------------------------------------------------------------
function TmmQRGraph.GetDateStep(aDateTicks: eDateTicksT): double;
begin
 case aDateTicks of
  dtMinute:   result := cMinute1;
  dtMinute5:  result := cMinute5;
  dtMinute30: result := cMinute30;
  dtHour:     result := cHour;
  dtHour3:    result := cHour *3;
  dtHour6:    result := cHour *6;
  dtHour12:   result := cHour *12;
  dtDay:      result := 1;
  dtWeek:     result := 7;
  dtMonth:    result := 30;
  dtMonth2:   result := 61;
  dtMonth3:   result := 91;
  dtYear:     result := 365;
  dtYear10:   result := 3654;
  dtYear100:  result := 36540;
 end; //case aDateTicks of
end; //function TmmQRGraph.GetDateStep
//-----------------------------------------------------------------------------
function TmmQRGraph.GetFirstDateTick(aMinDate: double): double;
var
 Year,Month,Day,
 Hour,mMin,Sec,MSec,
 xAddHour,xAddYear   : word;

begin
 DecodeDate(aMinDate,Year,Month,Day);
 DecodeTime(aMinDate,Hour,mMin,Sec,MSec);

 case FDateTickType of
  dtMinute: if Sec <> 0 then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0) +encodetime(0,1,0,0)
                        else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0);
  dtMinute5: if (mMin mod 5 <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,5 *(mMin div 5),0,0) +encodetime(0,5,0,0)
                                                else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,mMin,0,0);
  dtMinute30: if (mMin = 0) and (Sec = 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,0,0,0)
                                          else if (mMin >= 30) then result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,0,0,0) +encodetime(1,0,0,0)
                                                               else result := EncodeDate(Year,Month,Day) +EncodeTime(Hour,30,0,0);

  dtHour..dtHour12: begin
   if FDateTickType = dtHour then xAddHour := 1;
   if FDateTickType = dtHour3 then xAddHour := 3;
   if FDateTickType = dtHour6 then xAddHour := 6;
   if FDateTickType = dtHour12 then xAddHour := 12;
   if (Hour mod xAddHour <> 0) or (mMin <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +EncodeTime(xAddHour *(Hour div xAddHour),0,0,0) +encodetime(xAddHour,0,0,0)
                                                            else result := EncodeDate(Year,Month,Day) +encodetime(xAddHour,0,0,0);
  end; //dtHour..dthour12

  dtDay: if (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then result := EncodeDate(Year,Month,Day) +1
                                                    else result := EncodeDate(Year,Month,Day);
  dtWeek: result := EncodeDate(Year,Month,Day) +8 -DayOfWeek(aMinDate);

  dtMonth: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   result := EnCodeDate(Year,Month,1);
  end; //dtMonth

  dtMonth2: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   if Month mod 2 <> 1 then inc(Month);
   result := EnCodeDate(Year,Month, 1);
  end; //dtMonth2

  dtMonth3: begin
   if (Day > 1) or (Hour <> 0) or (mMin <> 0) or (Sec <> 0) then AddMonth(Year,Month,1);
   while Month mod 3 <> 1 do AddMonth(Year,Month,1);
   result := EnCodeDate(Year,Month, 1);
  end; //dtMonth3

  dtYear..dtYear100: begin
   if FDateTickType = dtYear then xAddYear := 1;
   if FDateTickType = dtYear10 then xAddYear := 10;
   if FDateTickType = dtYear100 then xAddYear := 100;

   if (Month > 1) or(Day > 1) or (Hour <> 0) or (mMin <>0) or (Sec <> 0) then inc(Year);
   result := EncodeDate(xAddYear *(Year div xAddYear),1,1);
  end; //dtYear..dtYear100
 end; //case FDateTickType of
end; //function TmmQRGraph.GetFirstDateTick
//-----------------------------------------------------------------------------
function TmmQRGraph.GetNextDateTick(tick: double): double;
var
 year,month,day: word;

begin
 case FDateTickType of
  dtMinute: result := tick +cminute1;
  dtMinute5: result := tick +cminute5;
  dtMinute30: result := tick +cminute30;
  dtHour: result := tick +chour;
  dtHour3: result := tick +3*chour;
  dtHour6: result := tick +6*chour;
  dtHour12: result := tick +12*chour;
  dtDay: result := tick +1;
  dtWeek: result := tick +7;

  dtMonth: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,1);
   result := encodedate(year,month,1);
  end; //dtMonth

  dtMonth2: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,2);
   result := encodedate(year,month,1);
  end; //dtMonth2

  dtMonth3: begin
   decodedate(tick,year,month,day);
   AddMonth(year,month,3);
   result := encodedate(year,month,1);
  end; //dtMonth3

  dtYear: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +1,1,1);
  end; //dtYear

  dtYear10: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +10,1,1);
  end; //dtYear10

  dtYear100: begin
   decodedate(tick,year,month,day);
   result := encodedate(year +100,1,1);
  end; //dtYear100
 end; //case FDateTickType of
end; //function TmmQRGraph.GetNextDateTick
//-----------------------------------------------------------------------------
function TmmQRGraph.GetStep(aMinSteps: Word; aMin,aMax: double): double;
const
 LogE = 0.4342944818; //100 /ln(1e100)

var
 w,t,b: double;

begin
 w := aMax -aMin;

 if w <= 0 then raise Exception.Create('Error: Min >= Max in GetStep');
 t := ln(w) *LogE;
 if t < 0 then t := t -1;
 b := exp(trunc(t *1.001) /LogE);

 if w/B >= aMinSteps then result := b
  else if 2*w/B >= aMinSteps then result := b /2
   else if 5*w/B >= aMinSteps then result := b /5
    else if 10*w/B >= aMinSteps then result := b /10
     else if 20*w/B >= aMinSteps then result := b /20
      else if 50*w/B >= aMinSteps then result := b /50
                                  else result := b/ 100;
end; //function TmmQRGraph.GetStep
//-----------------------------------------------------------------------------
procedure TmmQRGraph.SetMinMax(var aMin,aMax,aStep: double; aAutoStep: boolean);
var
 xMin,xMax : double;

begin
 xMin := aMin +cEpsilon;
 xMax := aMax -cEpsilon;

 if xMin > xMax then xMin := xMax;

 if xMin = xMax then
 begin
  if xMin <> 0 then
  begin
   if xMin < 0 then
   begin
    aMin := xMin * 1.2;
    aMax := 0;
   end //if xMin < 0 then
   else
   begin
    aMin := 0;
    aMax := xMax * 1.2;
   end; //else if xMin < 0 then

   aStep := GetStep(cMinSteps,aMin,aMax);
   aMax := trunc(aMax /aStep) *aStep;
  end //if xMin <> 0 then
  else
  begin
   aMin := 0;
   aMax := 100;
   aStep := GetStep(cMinSteps,aMin,aMax);
  end; //else if xMin <> 0 then

  exit;
 end //if xMin = xMax then
 else
 begin
  aMin := xMin;
  aMax := xMax;
 end; //else if xMin = xMax then

 if aAutoStep or (aStep <= 0) or (aStep /(aMax -aMin) < 1 /cMaxSteps)
  then aStep := GetStep(cMinSteps,aMin,aMax);

 aMin := trunc(aMin /aStep) *aStep;
 if (xMin < 0) then aMin := aMin -aStep;
 if (aMin > xMin -0.01 *aStep) then aMin := aMin -aStep;

 aMax := trunc(aMax /aStep) *aStep +aStep;
 if (xMax < 0) then aMax := aMax -aStep;
 if (aMax < xMax +0.01 *aStep) then aMax := aMax +aStep;

 if (xMin > 0) and (xMin /(aMax -aMin) < 0.25)
  then aMin := 0
  else if (xMax < 0) and (-xMax /(aMax -aMin) < 0.25) then aMax := 0;

 if aMin >= aMax then raise Exception.Create('SetMinMax Error');
end; //procedure TmmQRGraph.SetMinMax
//-----------------------------------------------------------------------------
procedure TmmQRGraph.CalcYAxis;
var
 i       : integer;
 xYAxis  : eYAxisT;
 dY      : double;
 xLeg    : TmmCustomGraphArea;
 LTicks  : integer;
 RMax    : double;
 RFactor : double;

begin
 //reset YStretch
 if FHorzGrid.GridType = hgLeftRightAxis then YStretch[yaxRight] := 0;

 //calc sum for BarStyle = bsStacked, bsStacked100
 Data.ResetSUM;
 for i := 1 to Data.NumSeries do
  if Series.Visible[i] and (Series.YAxis[i] = yaxRight) then
   Data.AddToSUM(i);

 Data.CalcStatData;
 fillchar(YScale,sizeof(tYScaleRecordT),0);

 YScale.Min[yaxLeft] := 9e99;
 YScale.Max[yaxLeft] := -9e99;
 YScale.Min[yaxRight] := 9e99;
 YScale.Max[yaxRight] := -9e99;

 {//***2.10.99
 for i := 1 to Data.NumSeries do
 begin
  if Series.Visible[i] then
  begin
   xYAxis := Series.YAxis[i];

   if xYAxis = yaxLeft then xLeg := FLeftYAxis
                       else xLeg := FRightYAxis;

   if xLeg.DefaultYMinMax then
   begin
    YScale.Min[xYAxis] := math.Min(YScale.Min[xYAxis],Series.DefaultMinY[i]);
    YScale.Max[xYAxis] := math.Max(YScale.Max[xYAxis],Series.DefaultMaxY[i]);
   end //if xLeg.DefaultYMinMax then
   else
   begin
    YScale.Min[xYAxis] := math.Min(YScale.Min[xYAxis],Data.Data.Statistik[i,stMin]);
    YScale.Max[xYAxis] := math.Max(YScale.Max[xYAxis],Data.Data.Statistik[i,stMax]);
   end; //else if xLeg.DefaultYMinMax then
  end; //if Series.Visible[i] then
 end; //for i := 1 to Data.NumSeries do
 }
 if NumOfBarSeries > 0 then
 begin
  case FBars.Style of
   bsStacked: begin
    YScale.Min[yaxRight] := 0; //math.Min(YScale.Min[yaxRight],Data.Data.Statistik[0,stMin]);
    //YScale.Max[yaxRight] := math.Max(YScale.Max[yaxRight],Data.Data.Statistik[0,stMax]);
   end; //bsStacked

   bsStacked100: begin
    YScale.Min[yaxRight] := 0;
    YScale.Max[yaxRight] := 100;
   end; //bsStacked100
  end; //case FBars.Style of
 end; //if NumOfBarSeries > 0 then

 for xYAxis := yaxLeft to yaxRight do
 begin
  //all series for this axis are disabled (not visible)
  if (YScale.Min[xYAxis] > YScale.Max[xYAxis]) or
    ((YScale.Min[xYAxis] = YScale.Max[xYAxis]) and (YScale.Min[xYAxis] = 0)) then
  begin
   YScale.Min[xYAxis] := 0;
   YScale.Max[xYAxis] := 100;
  end; //if YScale.Min[xYAxis] > YScale.Max[xYAxis] then

  dY := (YScale.Max[xYAxis] -YScale.Min[xYAxis]) *YStretch[xYAxis]/100;
  YScale.Max[xYAxis] := YScale.Max[xYAxis] +dY;
 end; //for xYAxis := yaxLeft to yaxRight do

 {//***2.10.99
 if FLeftYAxis.DefaultYMinMax
  then YScale.Step[yaxLeft] := GetStep(cMinSteps,YScale.Min[yaxLeft],YScale.Max[yaxLeft])
  else SetMinMax(YScale.Min[yaxLeft],YScale.Max[yaxLeft],YScale.Step[yaxLeft],true);

 if FRightYAxis.DefaultYMinMax
  then YScale.Step[yaxRight] := GetStep(cMinSteps,YScale.Min[yaxRight],YScale.Max[yaxRight])
  else SetMinMax(YScale.Min[yaxRight],YScale.Max[yaxRight],YScale.Step[yaxRight],true);
 }

 {//***2.10.99
 //link grid for left and right yaxis
 if FHorzGrid.GridType = hgLeftRightAxis then
  if FLeftYAxis.Visible and FRightYAxis.Visible then
   if not FRightYAxis.DefaultYMinMax then
    if FLeftYAxis.Visible and FRightYAxis.Visible then
    begin
     LTicks := round((YScale.Max[yaxLeft] -YScale.Min[yaxLeft]) /YScale.Step[yaxLeft]);
     RMax := YScale.Step[yaxRight] *LTicks;

     if RMax < YScale.Max[yaxRight] then
     begin
      RFactor := YScale.Max[yaxRight] /RMax;
      RFactor := round((RFactor /0.5) +0.5);

      YScale.Max[yaxRight] := RMax *RFactor;
      YScale.Step[yaxRight] := YScale.Step[yaxRight] *RFactor;
     end //if RMax < YScale.Max[yaxRight] then
     else YScale.Max[yaxRight] := RMax;
    end; //if FLeftYAxis.Visible and FRightYAxis.Visible then
}
end; //procedure TmmQRGraph.CalcYAxis
//-----------------------------------------------------------------------------
function TmmQRGraph.fy(v: double; aAxis: eYAxisT): integer;
var
 w,fScale : double;

begin
 fScale := (FChartArea.Height -1) /(YScale.Max[aAxis] -YScale.Min[aAxis]);
 w := (YScale.Max[aAxis] -v) *fScale;
 result := FChartArea.Top +round(w);
end; //function TmmQRGraph.fy
//-----------------------------------------------------------------------------
function TmmQRGraph.fx(aPoint: integer; aXValue: ValueType): integer;
var
 w : double;

begin
 case XAxis.XAxisType of
  xaFixed: result := FChartArea.Left +round(((aPoint -FScrollBar.Position) +1/2) *(BarWidth +GetPrnFactX(FBars.Gap)));
  xaFloat,xaDateTime: begin
   w := (aXValue -XScale.actMin) /XScale.actDX *FChartArea.Width;
   result := FChartArea.Left +round(w);
  end; //xaFloat,xaDateTime
 end; //case XAxis.XAxisType of
end; //function TmmQRGraph.fx
//-----------------------------------------------------------------------------
procedure Register;
begin
 RegisterComponents('Worbis',[TmmQRGraph]);
end; //procedure Register

end. //mmQRGraph.pas
