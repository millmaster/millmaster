{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Druckvorbereitung fuer PreView & Print
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.01.2002  1.00  Nue | File created
| 04.06.2006  1.01  SDo | Layout Anpassung
| 18.01.2005  2.00  SDo | Umbau & Anpassungen an XML Vers. 5
| 15.02.2005  2.01  SDo | P-Settingsbox eingebaut
| 26.04.2005  2.02  SDo | Groesse der beiden Matrixen festlegen -> FormCreate()
|                       | Anpassung Ausdruck an Papiergroesse mQuickReportBeforePrint()
| 12.05.2005  2.02  SDo | Anpassung Querformat Ausdruck: neue Positionierung der Setting-Boxen
| 15.06.2005  2.03  SDo | Settings Box 'P-Konfiguration' (sgPExt) wird nicht angezeigt; 15.06.2005 Mj
|                       | Printout Boxen neu positioniert
| 10.01.2008  2.04  Nue | VCV eingefügt.
===============================================================================}
unit PrintTemplateForm;
                                     
interface

uses

  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, IvDictio, IvMulti, IvEMulti, mmTranslator, QuickRpt,
  Qrctrls, mmQRSysData, mmQRShape, mmQRLabel, BaseForm,
  mmQuickRep, QualityMatrixDef,
  PrintSettingsTemplateForm,
  mmExtStringGrid, mmQRImage, mmQRRichText,
  mmQRChildBand, XMLSettingsModel, VCLXMLSettingsModel, YMQRUnit;

const
  cOrgSiroHeight = 255;   //Nue:24.06.08

type
  {:----------------------------------------------------------------------------
   Print selected settings on seperate pages by using QuickReport components.
   ----------------------------------------------------------------------------}
  TfrmPrint = class(TmmForm)
    mmQRSysData1: TmmQRSysData;
    mmTranslator1: TmmTranslator;
    mQuickReport: TmmQuickRep;
    PageHeaderBand1: TQRBand;
    qbDetail: TQRBand;
    qbFooter: TQRBand;
    qlPageValue: TmmQRSysData;
    qlTitle: TmmQRLabel;
    qrmChannel: TQRQMatrix;
    qrmSiro: TQRQMatrix;
    qcbTemplate: TQRChildBand;
    qcbLot: TQRChildBand;
    qcbStyle: TQRChildBand;
    qcbOrderPos: TmmQRChildBand;
    qcbMachine: TmmQRChildBand;
    mmQRLabel18: TmmQRLabel;
    qlStyle3: TmmQRLabel;
    mmQRLabel19: TmmQRLabel;
    qlTemplate3: TmmQRLabel;
    mmQRLabel7: TmmQRLabel;
    qlStyle2: TmmQRLabel;
    mmQRLabel8: TmmQRLabel;
    qlLot2: TmmQRLabel;
    mmQRLabel9: TmmQRLabel;
    qlMachine2: TmmQRLabel;
    mmQRLabel11: TmmQRLabel;
    qlTemplate2: TmmQRLabel;
    mmQRLabel41: TmmQRLabel;
    qlStyle4: TmmQRLabel;
    mmQRLabel42: TmmQRLabel;
    qlLot4: TmmQRLabel;
    mmQRLabel43: TmmQRLabel;
    qlMachine4: TmmQRLabel;
    mmQRLabel44: TmmQRLabel;
    qlTemplate4: TmmQRLabel;
    qlMachine5: TmmQRLabel;
    mmQRLabel50: TmmQRLabel;
    mmQRLabel2: TmmQRLabel;
    qlSpindleRange5: TmmQRLabel;
    mmQRLabel49: TmmQRLabel;
    qlLot5: TmmQRLabel;
    mmQRLabel51: TmmQRLabel;
    qlTemplate5: TmmQRLabel;
    mmQRLabel6: TmmQRLabel;
    qlTemplate1: TmmQRLabel;
    mmQRLabel1: TmmQRLabel;
    qlSpindleRange2: TmmQRLabel;
    mmQRLabel5: TmmQRLabel;
    qlSlip2: TmmQRLabel;
    mmQRLabel10: TmmQRLabel;
    qlSlip3: TmmQRLabel;
    qlCompanyName: TmmQRLabel;
    mmQRImage1: TmmQRImage;
    qcbClearerSettings: TmmQRChildBand;
    qlMachine6: TmmQRLabel;
    mmQRLabel4: TmmQRLabel;
    mmQRLabel14: TmmQRLabel;
    qlLot6: TmmQRLabel;
    mmQRLabel16: TmmQRLabel;
    qlTemplate6: TmmQRLabel;
    mmQRLabel3: TmmQRLabel;
    qlTimeRange6: TmmQRLabel;
    VCLXMLSettingsModel_Print: TVCLXMLSettingsModel;
    QRXMLSettings_Channel: TQRXMLSettings;
    QRXMLSettings_Cluster: TQRXMLSettings;
    QRXMLSettings_FCluster: TQRXMLSettings;
    QRXMLSettings_SFI: TQRXMLSettings;
    QRXMLSettings_Splice: TQRXMLSettings;
    QRXMLSettings_YarnCount: TQRXMLSettings;
    QRXMLSettings_P: TQRXMLSettings;
    XMLSettingsRepetition: TQRXMLSettings;
    QRXMLSettings_VCV: TQRXMLSettings;
    qrmSplice: TQRQMatrix;
    procedure mQuickReportBeforePrint(Sender: TCustomQuickRep;
      var PrintReport: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  frmPrint: TfrmPrint;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, SettingsReader, PRINTERS;
{$R *.DFM}
//uses
//  MainForm;

{TfrmPrint}
//------------------------------------------------------------------------------
procedure TfrmPrint.mQuickReportBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
var
  xDetailBandHeight: integer;
  y, y1, y2, y3, yP, yRep,
  x, x1, x2, x3, x11, x21, x31, xP, xRep, xTemp: Integer;
  xHeight, xRight : integer;
  xHeightSiro, xWidthSiro : integer;

  xChannelVisible, xFaultClusterVisible, xFFClusterVisible,
  xSFIVisible, xVCVVisible, xSpliceVisible, xYarnCountVisible, xSet: Boolean;
begin
  inherited;

  try
    qlCompanyName.Caption := TMMSettingsReader.Instance.Value[cCompanyName];
  except
    qlCompanyName.Caption := '';
  end;

  //Hoehe Detail-Band
  xDetailBandHeight := mQuickReport.Height - PageHeaderBand1.Height -
    qcbTemplate.Height - qbFooter.Height - 100;

  qbDetail.Height := xDetailBandHeight;

  //Channel Matrix
  qrmChannel.Top := 10;
  qrmChannel.Left := 0;

  if (qrmSiro.FModeBD) and (qrmSiro.Height = cOrgSiroHeight) then begin
    qrmSiro.Height := qrmSiro.Height div 2;
    qrmSiro.Width := qrmSiro.Width div 2;
  end;

  //Portrait
  if mQuickReport.Page.Orientation = (poPortrait) then begin
     //Siro Matrix

    qrmSiro.Top := qbDetail.Height - qrmSiro.Height;
    qrmSiro.Left := 0;


     //Abstand zwischen den beiden Matrixen
    y := (qrmSiro.Top - qrmChannel.Top - qrmChannel.Height);

    //Anpassung, wenn Papiergroesse kleiner als A4 ist. z.B. Letter (US)
    if y < QRXMLSettings_Cluster.Height then begin
       //-> qrmSiro wird proportional verkleinert
       xHeight :=  QRXMLSettings_Cluster.Height - y + 10;
       qrmSiro.Height :=  qrmSiro.Height - xHeight;
       qrmSiro.Width := qrmSiro.Width - xHeight;
       qrmSiro.Top := qrmSiro.Top + xHeight;
       y := (qrmSiro.Top - qrmChannel.Top - qrmChannel.Height);
    end;


     //Settings Top
    y1 := qrmChannel.Top -2; //gleiche hoehe wie qrmChannel-Matrix
    y2 := qrmSiro.Top -2 ; //gleiche hoehe wie qrmSiro-Matrix

    y := y2-(y2 - (y1 + qrmChannel.Height)) div 2; //zwischen qrmChannel- & qrmSiro-Matrix eingemittet
//NueAlt    y3 := y2 - (y - (QRXMLSettings_Cluster.Height div 2)) - QRXMLSettings_Cluster.Height;
//    y3 := y2 - (y - (qrmChannel.Height div 2)) - qrmChannel.Height;  //Oberer Rand der Slicematrix
    y3 := y - (qrmSplice.Height div 2);

     //Settings Left 1. Komp.
    x1 := qrmChannel.Left + qrmChannel.Width + 10; // links von qrmChannel-Matrix -> QRXMLSettings_Channel
//NueAlt    x1 := qrmChannel.Left + qrmChannel.Width - QRXMLSettings_Channel.Width - 35; // links von qrmChannel-Matrix -> QRXMLSettings_Channel
//NueAlt1    x1 := qrmChannel.Left + qrmChannel.Width - QRXMLSettings_Channel.Width - 20; // links von qrmChannel-Matrix -> QRXMLSettings_Channel  NueNeu


    x2 := qrmSiro.Left + qrmSiro.Width + 10; //links von qrmSiro-Matrix -> QRXMLSettings_FCluster
    x3 := 0; // -> QRXMLSettings_Cluster

     //Settings Left 2. Komp.
//    x11 := QRXMLSettings_Channel.Left + QRXMLSettings_Channel.Width + 10; // -> QRXMLSettings_Splice
//NueOrg    x11 := x1 + QRXMLSettings_Channel.Width + 10; // -> QRXMLSettings_Splice
    x21 := qbDetail.Width - QRXMLSettings_YarnCount.Width - 10; // -> QRXMLSettings_YarnCount
//NueOrg    x31 := QRXMLSettings_Cluster.Left + QRXMLSettings_Cluster.Width + 10; // -> QRXMLSettings_SFI3
    x31 := qbDetail.Width - QRXMLSettings_SFI.Width - 10; // -> QRXMLSettings_SFI3  //NueNeu
    xP  := qbDetail.Width - QRXMLSettings_P.Width - 10; //x P-Settingsbox

    if not QRXMLSettings_Channel.Enabled then
      x11 := x1;

//NueOrg
//    if not QRXMLSettings_Cluster.Enabled then
//      x31 := x3;

    yP := y2; //y von P-Settingsbox

    //Settings Box Repetition
//    yRep := y3 - XMLSettingsRepetition.Height  + 5;
    yRep := y1;
    xRep := qbDetail.Width - XMLSettingsRepetition.Width - 10; //x P-Settingsbox

  end
  else begin
  //Landscape

     //Abstand zu den Matrixen

    //Vertikale Ausrichtung: Setting Boxen
//    y := qrmChannel.Top + qrmChannel.Height + 10;
    y := 0;
    y1 := y;
    y2 := qrmSiro.Top;
    y3 := y;
    yP := qrmSiro.Top + QRXMLSettings_FCluster.Height + 10; //P-Reinigung
    //yP := qrmSiro.Top;  //P-Reinigung


    //Horizontale Ausrichtung: Settings-Boxen

    //Kanal
//NueAlt1    x1 := 0; //QRXMLSettings_Channel
    x1 := qrmChannel.Left;

    //Spleiss
    x := QRXMLSettings_Channel.Width + 5;
    if not QRXMLSettings_Channel.Enabled then x := 0;
    x11 := x1 + x; // QRXMLSettings_Splice
    xTemp := x;

    qrmSiro.Top := qrmChannel.Top;
    qrmSiro.Left := qrmChannel.Left + qrmChannel.Width + x;
//NueAlt    qrmSiro.Left := QRXMLSettings_Channel.Left + QRXMLSettings_Channel.Width + 10;

    //Fremdfaser
    x := QRXMLSettings_Splice.Width + 5;
    //if not QRXMLSettings_Splice.Enabled then x := 0;
    x2 := x11 + x; //QRXMLSettings_FCluster
    x2 := qrmSiro.Left + qrmSiro.Width + 10;



    //Fehlerschwarm
//    x := QRXMLSettings_FCluster.Width + 5;
    if xTemp > 0 then
       x := QRXMLSettings_Splice.Width + 5;
    if not QRXMLSettings_FCluster.Enabled then x := 0;
//    x3 := xTemp + x; // QRXMLSettings_Cluster
    x3 := x11 + x;

    //SFI
    if x > 0 then
       x := QRXMLSettings_Cluster.Width + 5;
    if not QRXMLSettings_Cluster.Enabled then x := 0;
    x31 := x3 + x; //QRXMLSettings_SFI

    //VCV    Gleiches x wie SFI x31
//    if x > 0 then
//       x := QRXMLSettings_Cluster.Width + 5;
//    if not QRXMLSettings_Cluster.Enabled then x := 0;
//    x32 := x31 + x; //QRXMLSettings_VCV

    //Garnnummer
    if x > 0 then
       x := QRXMLSettings_SFI.Width + 5;
    if not QRXMLSettings_SFI.Enabled then x := 0;
    x21 := x31 + x; //QRXMLSettings_YarnCount

    //P-Reinigung
    xP := qrmSiro.Left + qrmSiro.Width + 10;

    //Settings Box Repetition
    yRep := y1 +  QRXMLSettings_Splice.Height -  XMLSettingsRepetition.Height;
    xRep := x31 + XMLSettingsRepetition.Width + 10;

    yRep := yP + QRXMLSettings_P.Height + 10;
    xRep := xP;
x1 := qrmChannel.Left + qrmChannel.Width + 10; // links von qrmChannel-Matrix -> QRXMLSettings_Channel
  end;



  //Settings Boxen

  //1. Gruppe

  //Kanal
  QRXMLSettings_Channel.Top := y1;
  QRXMLSettings_Channel.Left := x1;

//NueAlt
//  //Spleiss
//  QRXMLSettings_Splice.Top := y1;
//  QRXMLSettings_Splice.Left := x11;
//
  //neu 15.06.2005
  XMLSettingsRepetition.Top  := yRep;
  XMLSettingsRepetition.Left := xRep;
  xRight := xRep + XMLSettingsRepetition.Width;
  //Garnnummer   NueNeu
  if mQuickReport.Page.Orientation = (poPortrait) then
//NueAlt1     QRXMLSettings_YarnCount.Top := yRep + XMLSettingsRepetition.Height +5
     QRXMLSettings_YarnCount.Top := y1 + QRXMLSettings_Channel.Height + 10
  else
     QRXMLSettings_YarnCount.Top := y3;   //??
//NueAlt1  QRXMLSettings_YarnCount.Left := x21;
  QRXMLSettings_YarnCount.Left := x1;

//  //Spleiss1 Nue
//  QRXMLSettings_Splice1.Top := y1;
//  QRXMLSettings_Splice1.Left := x11;

  //2. Gruppe
  //Spleissmatrix  NueNeu
  qrmSplice.Top := y3;
  qrmSplice.Left := 0;

  //Spleiss  NueNeu
  QRXMLSettings_Splice.Top := y3 + 2;
  QRXMLSettings_Splice.Left := qrmSplice.Left + qrmSplice.Width + 10;

  //Fremdfaser
  QRXMLSettings_FCluster.Top := y2;
  QRXMLSettings_FCluster.Left := x2;

//NueAlt
    //Garnnummer
//  if mQuickReport.Page.Orientation = (poPortrait) then
//     QRXMLSettings_YarnCount.Top := y3 -  QRXMLSettings_YarnCount.Height + QRXMLSettings_Cluster.Height
//  else
//     QRXMLSettings_YarnCount.Top := y3;
//  QRXMLSettings_YarnCount.Left := x21;


  //3. Gruppe

//NueAlt
//  //Fehlerschwarm
//  QRXMLSettings_Cluster.Top := y3;
//  QRXMLSettings_Cluster.Left := x3;

//  //SFI
////  QRXMLSettings_SFI.Top := y3;
//  QRXMLSettings_SFI.Top := QRXMLSettings_YarnCount.Top;
//  QRXMLSettings_SFI.Left := x31;
  //SFI NueNeu
  QRXMLSettings_SFI.Top := y - QRXMLSettings_SFI.Height -20;
  QRXMLSettings_SFI.Left := xRight - QRXMLSettings_SFI.Width;

//NueAlt
//  //VCV
//  QRXMLSettings_VCV.Top := QRXMLSettings_SFI.Top + QRXMLSettings_SFI.Height + 5;
//  QRXMLSettings_VCV.Left := x31;
  //VCV  NueNeu
  QRXMLSettings_VCV.Top := QRXMLSettings_SFI.Top + QRXMLSettings_SFI.Height + 5;
  QRXMLSettings_VCV.Left := xRight - QRXMLSettings_VCV.Width;

  //Fehlerschwarm NueNeu
  QRXMLSettings_Cluster.Top := QRXMLSettings_VCV.Top + QRXMLSettings_VCV.Height + 5;
  QRXMLSettings_Cluster.Left := xRight - QRXMLSettings_Cluster.Width;

  //neu 15.02.2005
  //P-Reinigung
  QRXMLSettings_P.Top  := yP;
  QRXMLSettings_P.Left := xP;


end;
//------------------------------------------------------------------------------
procedure TfrmPrint.FormCreate(Sender: TObject);
begin
  inherited;

{
  //Matrix groesse
  qrmChannel.Height := 385;
  qrmChannel.Width := 514;

  qrmSiro.Height := 270;
  qrmSiro.Width := 341;
}

  //Matrix groesse
//  qrmChannel.Height := 370;
//  qrmChannel.Width := 500;
//  qrmChannel.Height := 314;    //Nue:11.03.08
//  qrmChannel.Width := 420;     //Nue:11.03.08
//
//  qrmSplice.Height := 185;     //Nue:11.03.08
//  qrmSplice.Width := 250;      //Nue:11.03.08

//  qrmChannel.Height := 241;    //Nue:11.03.08
//  qrmChannel.Width := 323;     //Nue:11.03.08

  qrmChannel.Height := 253;    //Nue:11.03.08
  qrmChannel.Width := 341;     //Nue:11.03.08

  qrmSplice.Height := 253;     //Nue:11.03.08
  qrmSplice.Width := 341;      //Nue:11.03.08


  qrmSiro.Height := cOrgSiroHeight;
  qrmSiro.Width := 325;



  qlPageValue.Text := qlPageValue.Text + ' ';
end;

//------------------------------------------------------------------------------
end.

