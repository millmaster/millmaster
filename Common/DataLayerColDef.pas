(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClassDataLayerDef.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
contains following constant definitions for field access in db
  cSUM_Production =
  cSUM_classCut =
  cSUM_classUncut =
  cSUM_spliceCut =
  cSUM_spliceUncut =
  cSUM_siroCut =
  cSUM_siroUncut =
|-------------------------------------------------------------------------------
| 11.06 2001 1.00 Wss | File created
| 02.06 2002 1.01 Nue | Longterm extensions added
| 22.12.2004 1.01 Wss | Multiplikation * 1.0 bei Klassdatenfeldern entfernt
| 02.06.2005 1.01 Wss | Datenelemente f�r SpectraPlus und Zenit hinzugef�gt   
| 06.02.2008 1.10 Nue | VCV-Fields added.
|=============================================================================*)
unit DataLayerColDef;

interface
uses
  MMUGlobal;

const
  cSUM_Production =
    'sum(c_Len * 1.0) c_Len ' +
    'sum(c_Wei * 1.0) c_Wei ' +
    'sum(c_Bob * 1.0) c_Bob ' +
    'sum(c_Cones * 1.0) c_Cones ' +
    'sum(c_Red * 1.0) c_Red ' +
    'sum(c_tRed * 1.0) c_tRed ' +
    'sum(c_tYell * 1.0) c_tYell ' +
    'sum(c_ManSt * 1.0) c_ManSt ' +
    'sum(c_tManSt * 1.0) c_tManSt ' +
    'sum(c_rtSpd * 1.0) c_rtSpd ' +
    'sum(c_otSpd * 1.0) c_otSpd ' +
    'sum(c_wtSpd * 1.0) c_wtSpd ' +
    'sum(c_Sp * 1.0) c_Sp ' +
    'sum(c_RSp * 1.0) c_RSp ' +
    'sum(c_YB * 1.0) c_YB ' +
    'sum(c_otProdGrp * 1.0) c_otProdGrp ' +
    'sum(c_wtProdGrp * 1.0) c_wtProdGrp ' +
    'sum(c_LSt * 1.0) c_LSt ' +
    'sum(c_LStProd * 1.0) c_LStProd ' +
    'sum(c_LStBreak * 1.0) c_LStBreak ' +
    'sum(c_LStMat * 1.0) c_LStMat ' +
    'sum(c_LStRev * 1.0) c_LStRev ' +
    'sum(c_LStClean * 1.0) c_LStClean ' +
    'sum(c_LStDef * 1.0) c_LStDef ' +
    'sum(c_CS * 1.0) c_CS ' +
    'sum(c_CL * 1.0) c_CL ' +
    'sum(c_CT * 1.0) c_CT ' +
    'sum(c_CN * 1.0) c_CN ' +
    'sum(c_CSp * 1.0) c_CSp ' +
    'sum(c_CClS * 1.0) c_CClS ' +
    'sum(c_LckClS * 1.0) c_LckClS ' +
    'sum(c_UClS * 1.0) c_UClS ' +
    'sum(c_UClL * 1.0) c_UClL ' +
    'sum(c_UClT * 1.0) c_UClT ' +
    'sum(c_COffCnt * 1.0) c_COffCnt ' +
    'sum(c_LckOffCnt * 1.0) c_LckOffCnt ' +
    'sum(c_CBu * 1.0) c_CBu ' +
    'sum(c_CDBu * 1.0) c_CDBu ' +
    'sum(c_CSys * 1.0) c_CSys ' +
    'sum(c_LckSys * 1.0) c_LckSys ' +
    'sum(c_CUpY * 1.0) c_CUpY ' +
    'sum(c_CYTot * 1.0) c_CYTot ' +
    'sum(c_CSIRO * 1.0) c_CSIRO ' +
    'sum(c_LckSIRO * 1.0) c_LckSIRO ' +
    'sum(c_CSIROCl * 1.0) c_CSIROCl ' +
    'sum(c_LckSIROCl * 1.0) c_LckSIROCl ' +
    'sum(c_INeps * 1.0) c_INeps ' +
    'sum(c_IThick * 1.0) c_IThick ' +
    'sum(c_IThin * 1.0) c_IThin ' +
    'sum(c_ISmall * 1.0) c_ISmall ' +
    'sum(c_I2_4 * 1.0) c_I2_4 ' +
    'sum(c_I4_8 * 1.0) c_I4_8 ' +
    'sum(c_I8_20 * 1.0) c_I8_20 ' +
    'sum(c_I20_70 * 1.0) c_I20_70 ' +
    'sum(c_SFI * 1.0) c_SFI ' +
    'sum(c_SFICnt * 1.0) c_SFICnt ' +
    'sum(c_CSFI * 1.0) c_CSFI ' +
    'sum(c_LckSFI * 1.0) c_LckSFI ' +
    // Juni 2005
    'sum(c_CP * 1.0) c_CP ' +
    'sum(c_CClL * 1.0) c_CClL ' +
    'sum(c_CClT * 1.0) c_CClT ' +
    // Bisheriges Feld CYCnt wird wieder zu COffCnt und entspricht der Summe von Plus/Minus 
    'sum(c_COffCntPlus * 1.0) c_COffCntPlus ' +
    'sum(c_COffCntMinus * 1.0) c_COffCntMinus ' +
    'sum(c_CShortOffCnt * 1.0) c_CShortOffCnt ' +
    'sum(c_CShortOffCntMinus * 1.0) c_CShortOffCntMinus ' +
    'sum(c_CShortOffCntPlus * 1.0) c_CShortOffCntPlus ' +
//8.6.05 kast        'sum(c_CDrumWrap * 1.0) c_CDrumWrap '+
    'sum(c_LckP * 1.0) c_LckP ' +
    'sum(c_LckClL * 1.0) c_LckClL ' +
    'sum(c_LckClT * 1.0) c_LckClT ' +
    'sum(c_LckShortOffCnt * 1.0) c_LckShortOffCnt ' +
    'sum(c_LckNSLT * 1.0) c_LckNSLT ' +
//Nue:06.02.08 VCV added.
    'sum(c_CVCV * 1.0) c_CVCV ' +
    'sum(c_LckVCV * 1.0) c_LckVCV ';

  cSUM_classUncut =
    'sum(c_cU1) c_cU1, ' +
    'sum(c_cU2) c_cU2, ' +
    'sum(c_cU3) c_cU3, ' +
    'sum(c_cU4) c_cU4, ' +
    'sum(c_cU5) c_cU5, ' +
    'sum(c_cU6) c_cU6, ' +
    'sum(c_cU7) c_cU7, ' +
    'sum(c_cU8) c_cU8, ' +
    'sum(c_cU9) c_cU9, ' +
    'sum(c_cU10) c_cU10, ' +
    'sum(c_cU11) c_cU11, ' +
    'sum(c_cU12) c_cU12, ' +
    'sum(c_cU13) c_cU13, ' +
    'sum(c_cU14) c_cU14, ' +
    'sum(c_cU15) c_cU15, ' +
    'sum(c_cU16) c_cU16, ' +
    'sum(c_cU17) c_cU17, ' +
    'sum(c_cU18) c_cU18, ' +
    'sum(c_cU19) c_cU19, ' +
    'sum(c_cU20) c_cU20, ' +
    'sum(c_cU21) c_cU21, ' +
    'sum(c_cU22) c_cU22, ' +
    'sum(c_cU23) c_cU23, ' +
    'sum(c_cU24) c_cU24, ' +
    'sum(c_cU25) c_cU25, ' +
    'sum(c_cU26) c_cU26, ' +
    'sum(c_cU27) c_cU27, ' +
    'sum(c_cU28) c_cU28, ' +
    'sum(c_cU29) c_cU29, ' +
    'sum(c_cU30) c_cU30, ' +
    'sum(c_cU31) c_cU31, ' +
    'sum(c_cU32) c_cU32, ' +
    'sum(c_cU33) c_cU33, ' +
    'sum(c_cU34) c_cU34, ' +
    'sum(c_cU35) c_cU35, ' +
    'sum(c_cU36) c_cU36, ' +
    'sum(c_cU37) c_cU37, ' +
    'sum(c_cU38) c_cU38, ' +
    'sum(c_cU39) c_cU39, ' +
    'sum(c_cU40) c_cU40, ' +
    'sum(c_cU41) c_cU41, ' +
    'sum(c_cU42) c_cU42, ' +
    'sum(c_cU43) c_cU43, ' +
    'sum(c_cU44) c_cU44, ' +
    'sum(c_cU45) c_cU45, ' +
    'sum(c_cU46) c_cU46, ' +
    'sum(c_cU47) c_cU47, ' +
    'sum(c_cU48) c_cU48, ' +
    'sum(c_cU49) c_cU49, ' +
    'sum(c_cU50) c_cU50, ' +
    'sum(c_cU51) c_cU51, ' +
    'sum(c_cU52) c_cU52, ' +
    'sum(c_cU53) c_cU53, ' +
    'sum(c_cU54) c_cU54, ' +
    'sum(c_cU55) c_cU55, ' +
    'sum(c_cU56) c_cU56, ' +
    'sum(c_cU57) c_cU57, ' +
    'sum(c_cU58) c_cU58, ' +
    'sum(c_cU59) c_cU59, ' +
    'sum(c_cU60) c_cU60, ' +
    'sum(c_cU61) c_cU61, ' +
    'sum(c_cU62) c_cU62, ' +
    'sum(c_cU63) c_cU63, ' +
    'sum(c_cU64) c_cU64, ' +
    'sum(c_cU65) c_cU65, ' +
    'sum(c_cU66) c_cU66, ' +
    'sum(c_cU67) c_cU67, ' +
    'sum(c_cU68) c_cU68, ' +
    'sum(c_cU69) c_cU69, ' +
    'sum(c_cU70) c_cU70, ' +
    'sum(c_cU71) c_cU71, ' +
    'sum(c_cU72) c_cU72, ' +
    'sum(c_cU73) c_cU73, ' +
    'sum(c_cU74) c_cU74, ' +
    'sum(c_cU75) c_cU75, ' +
    'sum(c_cU76) c_cU76, ' +
    'sum(c_cU77) c_cU77, ' +
    'sum(c_cU78) c_cU78, ' +
    'sum(c_cU79) c_cU79, ' +
    'sum(c_cU80) c_cU80, ' +
    'sum(c_cU81) c_cU81, ' +
    'sum(c_cU82) c_cU82, ' +
    'sum(c_cU83) c_cU83, ' +
    'sum(c_cU84) c_cU84, ' +
    'sum(c_cU85) c_cU85, ' +
    'sum(c_cU86) c_cU86, ' +
    'sum(c_cU87) c_cU87, ' +
    'sum(c_cU88) c_cU88, ' +
    'sum(c_cU89) c_cU89, ' +
    'sum(c_cU90) c_cU90, ' +
    'sum(c_cU91) c_cU91, ' +
    'sum(c_cU92) c_cU92, ' +
    'sum(c_cU93) c_cU93, ' +
    'sum(c_cU94) c_cU94, ' +
    'sum(c_cU95) c_cU95, ' +
    'sum(c_cU96) c_cU96, ' +
    'sum(c_cU97) c_cU97, ' +
    'sum(c_cU98) c_cU98, ' +
    'sum(c_cU99) c_cU99, ' +
    'sum(c_cU100) c_cU100, ' +
    'sum(c_cU101) c_cU101, ' +
    'sum(c_cU102) c_cU102, ' +
    'sum(c_cU103) c_cU103, ' +
    'sum(c_cU104) c_cU104, ' +
    'sum(c_cU105) c_cU105, ' +
    'sum(c_cU106) c_cU106, ' +
    'sum(c_cU107) c_cU107, ' +
    'sum(c_cU108) c_cU108, ' +
    'sum(c_cU109) c_cU109, ' +
    'sum(c_cU110) c_cU110, ' +
    'sum(c_cU111) c_cU111, ' +
    'sum(c_cU112) c_cU112, ' +
    'sum(c_cU113) c_cU113, ' +
    'sum(c_cU114) c_cU114, ' +
    'sum(c_cU115) c_cU115, ' +
    'sum(c_cU116) c_cU116, ' +
    'sum(c_cU117) c_cU117, ' +
    'sum(c_cU118) c_cU118, ' +
    'sum(c_cU119) c_cU119, ' +
    'sum(c_cU120) c_cU120, ' +
    'sum(c_cU121) c_cU121, ' +
    'sum(c_cU122) c_cU122, ' +
    'sum(c_cU123) c_cU123, ' +
    'sum(c_cU124) c_cU124, ' +
    'sum(c_cU125) c_cU125, ' +
    'sum(c_cU126) c_cU126, ' +
    'sum(c_cU127) c_cU127, ' +
    'sum(c_cU128) c_cU128';

  cSUM_classCut =
    'sum(c_cC1) c_cC1, ' +
    'sum(c_cC2) c_cC2, ' +
    'sum(c_cC3) c_cC3, ' +
    'sum(c_cC4) c_cC4, ' +
    'sum(c_cC5) c_cC5, ' +
    'sum(c_cC6) c_cC6, ' +
    'sum(c_cC7) c_cC7, ' +
    'sum(c_cC8) c_cC8, ' +
    'sum(c_cC9) c_cC9, ' +
    'sum(c_cC10) c_cC10, ' +
    'sum(c_cC11) c_cC11, ' +
    'sum(c_cC12) c_cC12, ' +
    'sum(c_cC13) c_cC13, ' +
    'sum(c_cC14) c_cC14, ' +
    'sum(c_cC15) c_cC15, ' +
    'sum(c_cC16) c_cC16, ' +
    'sum(c_cC17) c_cC17, ' +
    'sum(c_cC18) c_cC18, ' +
    'sum(c_cC19) c_cC19, ' +
    'sum(c_cC20) c_cC20, ' +
    'sum(c_cC21) c_cC21, ' +
    'sum(c_cC22) c_cC22, ' +
    'sum(c_cC23) c_cC23, ' +
    'sum(c_cC24) c_cC24, ' +
    'sum(c_cC25) c_cC25, ' +
    'sum(c_cC26) c_cC26, ' +
    'sum(c_cC27) c_cC27, ' +
    'sum(c_cC28) c_cC28, ' +
    'sum(c_cC29) c_cC29, ' +
    'sum(c_cC30) c_cC30, ' +
    'sum(c_cC31) c_cC31, ' +
    'sum(c_cC32) c_cC32, ' +
    'sum(c_cC33) c_cC33, ' +
    'sum(c_cC34) c_cC34, ' +
    'sum(c_cC35) c_cC35, ' +
    'sum(c_cC36) c_cC36, ' +
    'sum(c_cC37) c_cC37, ' +
    'sum(c_cC38) c_cC38, ' +
    'sum(c_cC39) c_cC39, ' +
    'sum(c_cC40) c_cC40, ' +
    'sum(c_cC41) c_cC41, ' +
    'sum(c_cC42) c_cC42, ' +
    'sum(c_cC43) c_cC43, ' +
    'sum(c_cC44) c_cC44, ' +
    'sum(c_cC45) c_cC45, ' +
    'sum(c_cC46) c_cC46, ' +
    'sum(c_cC47) c_cC47, ' +
    'sum(c_cC48) c_cC48, ' +
    'sum(c_cC49) c_cC49, ' +
    'sum(c_cC50) c_cC50, ' +
    'sum(c_cC51) c_cC51, ' +
    'sum(c_cC52) c_cC52, ' +
    'sum(c_cC53) c_cC53, ' +
    'sum(c_cC54) c_cC54, ' +
    'sum(c_cC55) c_cC55, ' +
    'sum(c_cC56) c_cC56, ' +
    'sum(c_cC57) c_cC57, ' +
    'sum(c_cC58) c_cC58, ' +
    'sum(c_cC59) c_cC59, ' +
    'sum(c_cC60) c_cC60, ' +
    'sum(c_cC61) c_cC61, ' +
    'sum(c_cC62) c_cC62, ' +
    'sum(c_cC63) c_cC63, ' +
    'sum(c_cC64) c_cC64, ' +
    'sum(c_cC65) c_cC65, ' +
    'sum(c_cC66) c_cC66, ' +
    'sum(c_cC67) c_cC67, ' +
    'sum(c_cC68) c_cC68, ' +
    'sum(c_cC69) c_cC69, ' +
    'sum(c_cC70) c_cC70, ' +
    'sum(c_cC71) c_cC71, ' +
    'sum(c_cC72) c_cC72, ' +
    'sum(c_cC73) c_cC73, ' +
    'sum(c_cC74) c_cC74, ' +
    'sum(c_cC75) c_cC75, ' +
    'sum(c_cC76) c_cC76, ' +
    'sum(c_cC77) c_cC77, ' +
    'sum(c_cC78) c_cC78, ' +
    'sum(c_cC79) c_cC79, ' +
    'sum(c_cC80) c_cC80, ' +
    'sum(c_cC81) c_cC81, ' +
    'sum(c_cC82) c_cC82, ' +
    'sum(c_cC83) c_cC83, ' +
    'sum(c_cC84) c_cC84, ' +
    'sum(c_cC85) c_cC85, ' +
    'sum(c_cC86) c_cC86, ' +
    'sum(c_cC87) c_cC87, ' +
    'sum(c_cC88) c_cC88, ' +
    'sum(c_cC89) c_cC89, ' +
    'sum(c_cC90) c_cC90, ' +
    'sum(c_cC91) c_cC91, ' +
    'sum(c_cC92) c_cC92, ' +
    'sum(c_cC93) c_cC93, ' +
    'sum(c_cC94) c_cC94, ' +
    'sum(c_cC95) c_cC95, ' +
    'sum(c_cC96) c_cC96, ' +
    'sum(c_cC97) c_cC97, ' +
    'sum(c_cC98) c_cC98, ' +
    'sum(c_cC99) c_cC99, ' +
    'sum(c_cC100) c_cC100, ' +
    'sum(c_cC101) c_cC101, ' +
    'sum(c_cC102) c_cC102, ' +
    'sum(c_cC103) c_cC103, ' +
    'sum(c_cC104) c_cC104, ' +
    'sum(c_cC105) c_cC105, ' +
    'sum(c_cC106) c_cC106, ' +
    'sum(c_cC107) c_cC107, ' +
    'sum(c_cC108) c_cC108, ' +
    'sum(c_cC109) c_cC109, ' +
    'sum(c_cC110) c_cC110, ' +
    'sum(c_cC111) c_cC111, ' +
    'sum(c_cC112) c_cC112, ' +
    'sum(c_cC113) c_cC113, ' +
    'sum(c_cC114) c_cC114, ' +
    'sum(c_cC115) c_cC115, ' +
    'sum(c_cC116) c_cC116, ' +
    'sum(c_cC117) c_cC117, ' +
    'sum(c_cC118) c_cC118, ' +
    'sum(c_cC119) c_cC119, ' +
    'sum(c_cC120) c_cC120, ' +
    'sum(c_cC121) c_cC121, ' +
    'sum(c_cC122) c_cC122, ' +
    'sum(c_cC123) c_cC123, ' +
    'sum(c_cC124) c_cC124, ' +
    'sum(c_cC125) c_cC125, ' +
    'sum(c_cC126) c_cC126, ' +
    'sum(c_cC127) c_cC127, ' +
    'sum(c_cC128) c_cC128';

  cSUM_spliceUncut =
    'sum(c_spU1) c_spU1, ' +
    'sum(c_spU2) c_spU2, ' +
    'sum(c_spU3) c_spU3, ' +
    'sum(c_spU4) c_spU4, ' +
    'sum(c_spU5) c_spU5, ' +
    'sum(c_spU6) c_spU6, ' +
    'sum(c_spU7) c_spU7, ' +
    'sum(c_spU8) c_spU8, ' +
    'sum(c_spU9) c_spU9, ' +
    'sum(c_spU10) c_spU10, ' +
    'sum(c_spU11) c_spU11, ' +
    'sum(c_spU12) c_spU12, ' +
    'sum(c_spU13) c_spU13, ' +
    'sum(c_spU14) c_spU14, ' +
    'sum(c_spU15) c_spU15, ' +
    'sum(c_spU16) c_spU16, ' +
    'sum(c_spU17) c_spU17, ' +
    'sum(c_spU18) c_spU18, ' +
    'sum(c_spU19) c_spU19, ' +
    'sum(c_spU20) c_spU20, ' +
    'sum(c_spU21) c_spU21, ' +
    'sum(c_spU22) c_spU22, ' +
    'sum(c_spU23) c_spU23, ' +
    'sum(c_spU24) c_spU24, ' +
    'sum(c_spU25) c_spU25, ' +
    'sum(c_spU26) c_spU26, ' +
    'sum(c_spU27) c_spU27, ' +
    'sum(c_spU28) c_spU28, ' +
    'sum(c_spU29) c_spU29, ' +
    'sum(c_spU30) c_spU30, ' +
    'sum(c_spU31) c_spU31, ' +
    'sum(c_spU32) c_spU32, ' +
    'sum(c_spU33) c_spU33, ' +
    'sum(c_spU34) c_spU34, ' +
    'sum(c_spU35) c_spU35, ' +
    'sum(c_spU36) c_spU36, ' +
    'sum(c_spU37) c_spU37, ' +
    'sum(c_spU38) c_spU38, ' +
    'sum(c_spU39) c_spU39, ' +
    'sum(c_spU40) c_spU40, ' +
    'sum(c_spU41) c_spU41, ' +
    'sum(c_spU42) c_spU42, ' +
    'sum(c_spU43) c_spU43, ' +
    'sum(c_spU44) c_spU44, ' +
    'sum(c_spU45) c_spU45, ' +
    'sum(c_spU46) c_spU46, ' +
    'sum(c_spU47) c_spU47, ' +
    'sum(c_spU48) c_spU48, ' +
    'sum(c_spU49) c_spU49, ' +
    'sum(c_spU50) c_spU50, ' +
    'sum(c_spU51) c_spU51, ' +
    'sum(c_spU52) c_spU52, ' +
    'sum(c_spU53) c_spU53, ' +
    'sum(c_spU54) c_spU54, ' +
    'sum(c_spU55) c_spU55, ' +
    'sum(c_spU56) c_spU56, ' +
    'sum(c_spU57) c_spU57, ' +
    'sum(c_spU58) c_spU58, ' +
    'sum(c_spU59) c_spU59, ' +
    'sum(c_spU60) c_spU60, ' +
    'sum(c_spU61) c_spU61, ' +
    'sum(c_spU62) c_spU62, ' +
    'sum(c_spU63) c_spU63, ' +
    'sum(c_spU64) c_spU64, ' +
    'sum(c_spU65) c_spU65, ' +
    'sum(c_spU66) c_spU66, ' +
    'sum(c_spU67) c_spU67, ' +
    'sum(c_spU68) c_spU68, ' +
    'sum(c_spU69) c_spU69, ' +
    'sum(c_spU70) c_spU70, ' +
    'sum(c_spU71) c_spU71, ' +
    'sum(c_spU72) c_spU72, ' +
    'sum(c_spU73) c_spU73, ' +
    'sum(c_spU74) c_spU74, ' +
    'sum(c_spU75) c_spU75, ' +
    'sum(c_spU76) c_spU76, ' +
    'sum(c_spU77) c_spU77, ' +
    'sum(c_spU78) c_spU78, ' +
    'sum(c_spU79) c_spU79, ' +
    'sum(c_spU80) c_spU80, ' +
    'sum(c_spU81) c_spU81, ' +
    'sum(c_spU82) c_spU82, ' +
    'sum(c_spU83) c_spU83, ' +
    'sum(c_spU84) c_spU84, ' +
    'sum(c_spU85) c_spU85, ' +
    'sum(c_spU86) c_spU86, ' +
    'sum(c_spU87) c_spU87, ' +
    'sum(c_spU88) c_spU88, ' +
    'sum(c_spU89) c_spU89, ' +
    'sum(c_spU90) c_spU90, ' +
    'sum(c_spU91) c_spU91, ' +
    'sum(c_spU92) c_spU92, ' +
    'sum(c_spU93) c_spU93, ' +
    'sum(c_spU94) c_spU94, ' +
    'sum(c_spU95) c_spU95, ' +
    'sum(c_spU96) c_spU96, ' +
    'sum(c_spU97) c_spU97, ' +
    'sum(c_spU98) c_spU98, ' +
    'sum(c_spU99) c_spU99, ' +
    'sum(c_spU100) c_spU100, ' +
    'sum(c_spU101) c_spU101, ' +
    'sum(c_spU102) c_spU102, ' +
    'sum(c_spU103) c_spU103, ' +
    'sum(c_spU104) c_spU104, ' +
    'sum(c_spU105) c_spU105, ' +
    'sum(c_spU106) c_spU106, ' +
    'sum(c_spU107) c_spU107, ' +
    'sum(c_spU108) c_spU108, ' +
    'sum(c_spU109) c_spU109, ' +
    'sum(c_spU110) c_spU110, ' +
    'sum(c_spU111) c_spU111, ' +
    'sum(c_spU112) c_spU112, ' +
    'sum(c_spU113) c_spU113, ' +
    'sum(c_spU114) c_spU114, ' +
    'sum(c_spU115) c_spU115, ' +
    'sum(c_spU116) c_spU116, ' +
    'sum(c_spU117) c_spU117, ' +
    'sum(c_spU118) c_spU118, ' +
    'sum(c_spU119) c_spU119, ' +
    'sum(c_spU120) c_spU120, ' +
    'sum(c_spU121) c_spU121, ' +
    'sum(c_spU122) c_spU122, ' +
    'sum(c_spU123) c_spU123, ' +
    'sum(c_spU124) c_spU124, ' +
    'sum(c_spU125) c_spU125, ' +
    'sum(c_spU126) c_spU126, ' +
    'sum(c_spU127) c_spU127, ' +
    'sum(c_spU128) c_spU128';

  cSUM_spliceCut =
    'sum(c_spC1) c_spC1, ' +
    'sum(c_spC2) c_spC2, ' +
    'sum(c_spC3) c_spC3, ' +
    'sum(c_spC4) c_spC4, ' +
    'sum(c_spC5) c_spC5, ' +
    'sum(c_spC6) c_spC6, ' +
    'sum(c_spC7) c_spC7, ' +
    'sum(c_spC8) c_spC8, ' +
    'sum(c_spC9) c_spC9, ' +
    'sum(c_spC10) c_spC10, ' +
    'sum(c_spC11) c_spC11, ' +
    'sum(c_spC12) c_spC12, ' +
    'sum(c_spC13) c_spC13, ' +
    'sum(c_spC14) c_spC14, ' +
    'sum(c_spC15) c_spC15, ' +
    'sum(c_spC16) c_spC16, ' +
    'sum(c_spC17) c_spC17, ' +
    'sum(c_spC18) c_spC18, ' +
    'sum(c_spC19) c_spC19, ' +
    'sum(c_spC20) c_spC20, ' +
    'sum(c_spC21) c_spC21, ' +
    'sum(c_spC22) c_spC22, ' +
    'sum(c_spC23) c_spC23, ' +
    'sum(c_spC24) c_spC24, ' +
    'sum(c_spC25) c_spC25, ' +
    'sum(c_spC26) c_spC26, ' +
    'sum(c_spC27) c_spC27, ' +
    'sum(c_spC28) c_spC28, ' +
    'sum(c_spC29) c_spC29, ' +
    'sum(c_spC30) c_spC30, ' +
    'sum(c_spC31) c_spC31, ' +
    'sum(c_spC32) c_spC32, ' +
    'sum(c_spC33) c_spC33, ' +
    'sum(c_spC34) c_spC34, ' +
    'sum(c_spC35) c_spC35, ' +
    'sum(c_spC36) c_spC36, ' +
    'sum(c_spC37) c_spC37, ' +
    'sum(c_spC38) c_spC38, ' +
    'sum(c_spC39) c_spC39, ' +
    'sum(c_spC40) c_spC40, ' +
    'sum(c_spC41) c_spC41, ' +
    'sum(c_spC42) c_spC42, ' +
    'sum(c_spC43) c_spC43, ' +
    'sum(c_spC44) c_spC44, ' +
    'sum(c_spC45) c_spC45, ' +
    'sum(c_spC46) c_spC46, ' +
    'sum(c_spC47) c_spC47, ' +
    'sum(c_spC48) c_spC48, ' +
    'sum(c_spC49) c_spC49, ' +
    'sum(c_spC50) c_spC50, ' +
    'sum(c_spC51) c_spC51, ' +
    'sum(c_spC52) c_spC52, ' +
    'sum(c_spC53) c_spC53, ' +
    'sum(c_spC54) c_spC54, ' +
    'sum(c_spC55) c_spC55, ' +
    'sum(c_spC56) c_spC56, ' +
    'sum(c_spC57) c_spC57, ' +
    'sum(c_spC58) c_spC58, ' +
    'sum(c_spC59) c_spC59, ' +
    'sum(c_spC60) c_spC60, ' +
    'sum(c_spC61) c_spC61, ' +
    'sum(c_spC62) c_spC62, ' +
    'sum(c_spC63) c_spC63, ' +
    'sum(c_spC64) c_spC64, ' +
    'sum(c_spC65) c_spC65, ' +
    'sum(c_spC66) c_spC66, ' +
    'sum(c_spC67) c_spC67, ' +
    'sum(c_spC68) c_spC68, ' +
    'sum(c_spC69) c_spC69, ' +
    'sum(c_spC70) c_spC70, ' +
    'sum(c_spC71) c_spC71, ' +
    'sum(c_spC72) c_spC72, ' +
    'sum(c_spC73) c_spC73, ' +
    'sum(c_spC74) c_spC74, ' +
    'sum(c_spC75) c_spC75, ' +
    'sum(c_spC76) c_spC76, ' +
    'sum(c_spC77) c_spC77, ' +
    'sum(c_spC78) c_spC78, ' +
    'sum(c_spC79) c_spC79, ' +
    'sum(c_spC80) c_spC80, ' +
    'sum(c_spC81) c_spC81, ' +
    'sum(c_spC82) c_spC82, ' +
    'sum(c_spC83) c_spC83, ' +
    'sum(c_spC84) c_spC84, ' +
    'sum(c_spC85) c_spC85, ' +
    'sum(c_spC86) c_spC86, ' +
    'sum(c_spC87) c_spC87, ' +
    'sum(c_spC88) c_spC88, ' +
    'sum(c_spC89) c_spC89, ' +
    'sum(c_spC90) c_spC90, ' +
    'sum(c_spC91) c_spC91, ' +
    'sum(c_spC92) c_spC92, ' +
    'sum(c_spC93) c_spC93, ' +
    'sum(c_spC94) c_spC94, ' +
    'sum(c_spC95) c_spC95, ' +
    'sum(c_spC96) c_spC96, ' +
    'sum(c_spC97) c_spC97, ' +
    'sum(c_spC98) c_spC98, ' +
    'sum(c_spC99) c_spC99, ' +
    'sum(c_spC100) c_spC100, ' +
    'sum(c_spC101) c_spC101, ' +
    'sum(c_spC102) c_spC102, ' +
    'sum(c_spC103) c_spC103, ' +
    'sum(c_spC104) c_spC104, ' +
    'sum(c_spC105) c_spC105, ' +
    'sum(c_spC106) c_spC106, ' +
    'sum(c_spC107) c_spC107, ' +
    'sum(c_spC108) c_spC108, ' +
    'sum(c_spC109) c_spC109, ' +
    'sum(c_spC110) c_spC110, ' +
    'sum(c_spC111) c_spC111, ' +
    'sum(c_spC112) c_spC112, ' +
    'sum(c_spC113) c_spC113, ' +
    'sum(c_spC114) c_spC114, ' +
    'sum(c_spC115) c_spC115, ' +
    'sum(c_spC116) c_spC116, ' +
    'sum(c_spC117) c_spC117, ' +
    'sum(c_spC118) c_spC118, ' +
    'sum(c_spC119) c_spC119, ' +
    'sum(c_spC120) c_spC120, ' +
    'sum(c_spC121) c_spC121, ' +
    'sum(c_spC122) c_spC122, ' +
    'sum(c_spC123) c_spC123, ' +
    'sum(c_spC124) c_spC124, ' +
    'sum(c_spC125) c_spC125, ' +
    'sum(c_spC126) c_spC126, ' +
    'sum(c_spC127) c_spC127, ' +
    'sum(c_spC128) c_spC128';

  cSUM_siroUncut =
    'sum(c_siU1) c_siU1, ' +
    'sum(c_siU2) c_siU2, ' +
    'sum(c_siU3) c_siU3, ' +
    'sum(c_siU4) c_siU4, ' +
    'sum(c_siU5) c_siU5, ' +
    'sum(c_siU6) c_siU6, ' +
    'sum(c_siU7) c_siU7, ' +
    'sum(c_siU8) c_siU8, ' +
    'sum(c_siU9) c_siU9, ' +
    'sum(c_siU10) c_siU10, ' +
    'sum(c_siU11) c_siU11, ' +
    'sum(c_siU12) c_siU12, ' +
    'sum(c_siU13) c_siU13, ' +
    'sum(c_siU14) c_siU14, ' +
    'sum(c_siU15) c_siU15, ' +
    'sum(c_siU16) c_siU16, ' +
    'sum(c_siU17) c_siU17, ' +
    'sum(c_siU18) c_siU18, ' +
    'sum(c_siU19) c_siU19, ' +
    'sum(c_siU20) c_siU20, ' +
    'sum(c_siU21) c_siU21, ' +
    'sum(c_siU22) c_siU22, ' +
    'sum(c_siU23) c_siU23, ' +
    'sum(c_siU24) c_siU24, ' +
    'sum(c_siU25) c_siU25, ' +
    'sum(c_siU26) c_siU26, ' +
    'sum(c_siU27) c_siU27, ' +
    'sum(c_siU28) c_siU28, ' +
    'sum(c_siU29) c_siU29, ' +
    'sum(c_siU30) c_siU30, ' +
    'sum(c_siU31) c_siU31, ' +
    'sum(c_siU32) c_siU32, ' +
    'sum(c_siU33) c_siU33, ' +
    'sum(c_siU34) c_siU34, ' +
    'sum(c_siU35) c_siU35, ' +
    'sum(c_siU36) c_siU36, ' +
    'sum(c_siU37) c_siU37, ' +
    'sum(c_siU38) c_siU38, ' +
    'sum(c_siU39) c_siU39, ' +
    'sum(c_siU40) c_siU40, ' +
    'sum(c_siU41) c_siU41, ' +
    'sum(c_siU42) c_siU42, ' +
    'sum(c_siU43) c_siU43, ' +
    'sum(c_siU44) c_siU44, ' +
    'sum(c_siU45) c_siU45, ' +
    'sum(c_siU46) c_siU46, ' +
    'sum(c_siU47) c_siU47, ' +
    'sum(c_siU48) c_siU48, ' +
    'sum(c_siU49) c_siU49, ' +
    'sum(c_siU50) c_siU50, ' +
    'sum(c_siU51) c_siU51, ' +
    'sum(c_siU52) c_siU52, ' +
    'sum(c_siU53) c_siU53, ' +
    'sum(c_siU54) c_siU54, ' +
    'sum(c_siU55) c_siU55, ' +
    'sum(c_siU56) c_siU56, ' +
    'sum(c_siU57) c_siU57, ' +
    'sum(c_siU58) c_siU58, ' +
    'sum(c_siU59) c_siU59, ' +
    'sum(c_siU60) c_siU60, ' +
    'sum(c_siU61) c_siU61, ' +
    'sum(c_siU62) c_siU62, ' +
    'sum(c_siU63) c_siU63, ' +
    'sum(c_siU64) c_siU64, ' +
    'sum(c_siU65) c_siU65, ' +
    'sum(c_siU66) c_siU66, ' +
    'sum(c_siU67) c_siU67, ' +
    'sum(c_siU68) c_siU68, ' +
    'sum(c_siU69) c_siU69, ' +
    'sum(c_siU70) c_siU70, ' +
    'sum(c_siU71) c_siU71, ' +
    'sum(c_siU72) c_siU72, ' +
    'sum(c_siU73) c_siU73, ' +
    'sum(c_siU74) c_siU74, ' +
    'sum(c_siU75) c_siU75, ' +
    'sum(c_siU76) c_siU76, ' +
    'sum(c_siU77) c_siU77, ' +
    'sum(c_siU78) c_siU78, ' +
    'sum(c_siU79) c_siU79, ' +
    'sum(c_siU80) c_siU80, ' +
    'sum(c_siU81) c_siU81, ' +
    'sum(c_siU82) c_siU82, ' +
    'sum(c_siU83) c_siU83, ' +
    'sum(c_siU84) c_siU84, ' +
    'sum(c_siU85) c_siU85, ' +
    'sum(c_siU86) c_siU86, ' +
    'sum(c_siU87) c_siU87, ' +
    'sum(c_siU88) c_siU88, ' +
    'sum(c_siU89) c_siU89, ' +
    'sum(c_siU90) c_siU90, ' +
    'sum(c_siU91) c_siU91, ' +
    'sum(c_siU92) c_siU92, ' +
    'sum(c_siU93) c_siU93, ' +
    'sum(c_siU94) c_siU94, ' +
    'sum(c_siU95) c_siU95, ' +
    'sum(c_siU96) c_siU96, ' +
    'sum(c_siU97) c_siU97, ' +
    'sum(c_siU98) c_siU98, ' +
    'sum(c_siU99) c_siU99, ' +
    'sum(c_siU100) c_siU100, ' +
    'sum(c_siU101) c_siU101, ' +
    'sum(c_siU102) c_siU102, ' +
    'sum(c_siU103) c_siU103, ' +
    'sum(c_siU104) c_siU104, ' +
    'sum(c_siU105) c_siU105, ' +
    'sum(c_siU106) c_siU106, ' +
    'sum(c_siU107) c_siU107, ' +
    'sum(c_siU108) c_siU108, ' +
    'sum(c_siU109) c_siU109, ' +
    'sum(c_siU110) c_siU110, ' +
    'sum(c_siU111) c_siU111, ' +
    'sum(c_siU112) c_siU112, ' +
    'sum(c_siU113) c_siU113, ' +
    'sum(c_siU114) c_siU114, ' +
    'sum(c_siU115) c_siU115, ' +
    'sum(c_siU116) c_siU116, ' +
    'sum(c_siU117) c_siU117, ' +
    'sum(c_siU118) c_siU118, ' +
    'sum(c_siU119) c_siU119, ' +
    'sum(c_siU120) c_siU120, ' +
    'sum(c_siU121) c_siU121, ' +
    'sum(c_siU122) c_siU122, ' +
    'sum(c_siU123) c_siU123, ' +
    'sum(c_siU124) c_siU124, ' +
    'sum(c_siU125) c_siU125, ' +
    'sum(c_siU126) c_siU126, ' +
    'sum(c_siU127) c_siU127, ' +
    'sum(c_siU128) c_siU128';

  cSUM_siroCut =
    'sum(c_siC1) c_siC1, ' +
    'sum(c_siC2) c_siC2, ' +
    'sum(c_siC3) c_siC3, ' +
    'sum(c_siC4) c_siC4, ' +
    'sum(c_siC5) c_siC5, ' +
    'sum(c_siC6) c_siC6, ' +
    'sum(c_siC7) c_siC7, ' +
    'sum(c_siC8) c_siC8, ' +
    'sum(c_siC9) c_siC9, ' +
    'sum(c_siC10) c_siC10, ' +
    'sum(c_siC11) c_siC11, ' +
    'sum(c_siC12) c_siC12, ' +
    'sum(c_siC13) c_siC13, ' +
    'sum(c_siC14) c_siC14, ' +
    'sum(c_siC15) c_siC15, ' +
    'sum(c_siC16) c_siC16, ' +
    'sum(c_siC17) c_siC17, ' +
    'sum(c_siC18) c_siC18, ' +
    'sum(c_siC19) c_siC19, ' +
    'sum(c_siC20) c_siC20, ' +
    'sum(c_siC21) c_siC21, ' +
    'sum(c_siC22) c_siC22, ' +
    'sum(c_siC23) c_siC23, ' +
    'sum(c_siC24) c_siC24, ' +
    'sum(c_siC25) c_siC25, ' +
    'sum(c_siC26) c_siC26, ' +
    'sum(c_siC27) c_siC27, ' +
    'sum(c_siC28) c_siC28, ' +
    'sum(c_siC29) c_siC29, ' +
    'sum(c_siC30) c_siC30, ' +
    'sum(c_siC31) c_siC31, ' +
    'sum(c_siC32) c_siC32, ' +
    'sum(c_siC33) c_siC33, ' +
    'sum(c_siC34) c_siC34, ' +
    'sum(c_siC35) c_siC35, ' +
    'sum(c_siC36) c_siC36, ' +
    'sum(c_siC37) c_siC37, ' +
    'sum(c_siC38) c_siC38, ' +
    'sum(c_siC39) c_siC39, ' +
    'sum(c_siC40) c_siC40, ' +
    'sum(c_siC41) c_siC41, ' +
    'sum(c_siC42) c_siC42, ' +
    'sum(c_siC43) c_siC43, ' +
    'sum(c_siC44) c_siC44, ' +
    'sum(c_siC45) c_siC45, ' +
    'sum(c_siC46) c_siC46, ' +
    'sum(c_siC47) c_siC47, ' +
    'sum(c_siC48) c_siC48, ' +
    'sum(c_siC49) c_siC49, ' +
    'sum(c_siC50) c_siC50, ' +
    'sum(c_siC51) c_siC51, ' +
    'sum(c_siC52) c_siC52, ' +
    'sum(c_siC53) c_siC53, ' +
    'sum(c_siC54) c_siC54, ' +
    'sum(c_siC55) c_siC55, ' +
    'sum(c_siC56) c_siC56, ' +
    'sum(c_siC57) c_siC57, ' +
    'sum(c_siC58) c_siC58, ' +
    'sum(c_siC59) c_siC59, ' +
    'sum(c_siC60) c_siC60, ' +
    'sum(c_siC61) c_siC61, ' +
    'sum(c_siC62) c_siC62, ' +
    'sum(c_siC63) c_siC63, ' +
    'sum(c_siC64) c_siC64, ' +
    'sum(c_siC65) c_siC65, ' +
    'sum(c_siC66) c_siC66, ' +
    'sum(c_siC67) c_siC67, ' +
    'sum(c_siC68) c_siC68, ' +
    'sum(c_siC69) c_siC69, ' +
    'sum(c_siC70) c_siC70, ' +
    'sum(c_siC71) c_siC71, ' +
    'sum(c_siC72) c_siC72, ' +
    'sum(c_siC73) c_siC73, ' +
    'sum(c_siC74) c_siC74, ' +
    'sum(c_siC75) c_siC75, ' +
    'sum(c_siC76) c_siC76, ' +
    'sum(c_siC77) c_siC77, ' +
    'sum(c_siC78) c_siC78, ' +
    'sum(c_siC79) c_siC79, ' +
    'sum(c_siC80) c_siC80, ' +
    'sum(c_siC81) c_siC81, ' +
    'sum(c_siC82) c_siC82, ' +
    'sum(c_siC83) c_siC83, ' +
    'sum(c_siC84) c_siC84, ' +
    'sum(c_siC85) c_siC85, ' +
    'sum(c_siC86) c_siC86, ' +
    'sum(c_siC87) c_siC87, ' +
    'sum(c_siC88) c_siC88, ' +
    'sum(c_siC89) c_siC89, ' +
    'sum(c_siC90) c_siC90, ' +
    'sum(c_siC91) c_siC91, ' +
    'sum(c_siC92) c_siC92, ' +
    'sum(c_siC93) c_siC93, ' +
    'sum(c_siC94) c_siC94, ' +
    'sum(c_siC95) c_siC95, ' +
    'sum(c_siC96) c_siC96, ' +
    'sum(c_siC97) c_siC97, ' +
    'sum(c_siC98) c_siC98, ' +
    'sum(c_siC99) c_siC99, ' +
    'sum(c_siC100) c_siC100, ' +
    'sum(c_siC101) c_siC101, ' +
    'sum(c_siC102) c_siC102, ' +
    'sum(c_siC103) c_siC103, ' +
    'sum(c_siC104) c_siC104, ' +
    'sum(c_siC105) c_siC105, ' +
    'sum(c_siC106) c_siC106, ' +
    'sum(c_siC107) c_siC107, ' +
    'sum(c_siC108) c_siC108, ' +
    'sum(c_siC109) c_siC109, ' +
    'sum(c_siC110) c_siC110, ' +
    'sum(c_siC111) c_siC111, ' +
    'sum(c_siC112) c_siC112, ' +
    'sum(c_siC113) c_siC113, ' +
    'sum(c_siC114) c_siC114, ' +
    'sum(c_siC115) c_siC115, ' +
    'sum(c_siC116) c_siC116, ' +
    'sum(c_siC117) c_siC117, ' +
    'sum(c_siC118) c_siC118, ' +
    'sum(c_siC119) c_siC119, ' +
    'sum(c_siC120) c_siC120, ' +
    'sum(c_siC121) c_siC121, ' +
    'sum(c_siC122) c_siC122, ' +
    'sum(c_siC123) c_siC123, ' +
    'sum(c_siC124) c_siC124, ' +
    'sum(c_siC125) c_siC125, ' +
    'sum(c_siC126) c_siC126, ' +
    'sum(c_siC127) c_siC127, ' +
    'sum(c_siC128) c_siC128';

//------------------------------------------------------------------------------
const
  // class data definitions for longterm_week
  cTableForLongClassCols: Array[TClassDataTyp] of String = (
    't_longterm_classUncut d',     //NUE
    't_longterm_classCut d',       //NUE
    't_longterm_spliceUncut d',    //NUE
    't_longterm_spliceCut d',      //NUE
    't_longterm_siroUncut d',      //NUE
    't_longterm_siroCut d'         //NUE
//    't_longterm_siroCutUncut d',   //NUE
//    't_longterm_siroCutUncut d'    //NUE
  );
  // class data definitions for shift
  cTableForClassCols: Array[TClassDataTyp] of String = (
    't_dw_classUncut d',
    't_dw_classCut d',
    't_dw_spliceUncut d',
    't_dw_spliceCut d',
    't_dw_siroUncut d',
    't_dw_siroCut d'
//    't_dw_siroCutUncut d',
//    't_dw_siroCutUncut d'
  );

  cWhereLinkForClassTable: Array[TClassDataTyp] of String = (
    'and v.c_classUncut_id   = d.c_classUncut_id',
    'and v.c_classCut_id     = d.c_classCut_id',
    'and v.c_spliceUncut_id  = d.c_spliceUncut_id',
    'and v.c_spliceCut_id    = d.c_spliceCut_id',
    'and v.c_siroUncut_id    = d.c_siroUncut_id',
    'and v.c_siroCut_id      = d.c_siroCut_id'
//    'and v.c_siroCutUncut_id = d.c_siroCutUncut_id',
//    'and v.c_siroCutUncut_id = d.c_siroCutUncut_id'
  );

  cSUM_ClassCols: Array[TClassDataTyp] of String = (
    cSUM_classUncut,
    cSUM_classCut,
    cSUM_spliceUncut,
    cSUM_spliceCut,
    cSUM_siroUncut,
    cSUM_siroCut
  );

  cClassFieldMask: Array[TClassDataTyp] of String = (
    'c_cU%d',
    'c_cC%d',
    'c_spU%d',
    'c_spC%d',
    'c_siU%d',
    'c_siC%d'
  );

  // class data definitions for interval
  cFLD_ClassImage = 'c_class_image';

  // YMSettings definitions
  cQry_ProdGroupInfo =
    'distinct c_prod_id, c_prod_name, c_ym_set_id, c_ym_set_name, c_machine_name, ' +
    'c_spindle_first, c_spindle_last, c_prod_start, c_prod_end, c_act_yarncnt';

  cQRY_YMSettings = 'distinct yms.c_ym_set_id, yms.c_ym_set_name, yms.c_head_class';
  cTAB_YMSettings = 't_xml_ym_settings yms';
  cWhereLinkForYMSettings = 'and v.c_ym_set_id = yms.c_ym_set_id';
  cORD_YMSettings = 'yms.c_ym_set_id';
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
