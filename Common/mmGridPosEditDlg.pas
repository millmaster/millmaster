(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGridPosEditDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.04.99 | 0.01 | PW  | Datei erstellt
| 00.00.99 | 0.00 | PW  |
|=========================================================================================*)

unit mmGridPosEditDlg;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmGridPosEdit, mmGridPos;

type
  TmmGridPosEditorDlg = class(TComponent)
  private
    FAlterColors: boolean;
    FAlterColumnFormat: boolean;
    FAlterColumnPosition: boolean;
    FAlterFixedCols: boolean;
    FAlterFonts: boolean;
    FAlterGridOptions: boolean;
    FAlterVisibleCols: boolean;
    FGridInfo: tGridInfoRecordT;
  protected
  public
    function Execute: boolean;
    property AlterColors: boolean read FAlterColors write FAlterColors;
    property AlterColumnFormat: boolean read FAlterColumnFormat write FAlterColumnFormat;
    property AlterColumnPosition: boolean read FAlterColumnPosition write FAlterColumnPosition;
    property AlterGridOptions: boolean read FAlterGridOptions write FAlterGridOptions;
    property AlterFixedCols: boolean read FAlterFixedCols write FAlterFixedCols;
    property AlterFonts: boolean read FAlterFonts write FAlterFonts;
    property AlterVisibleCols: boolean read FAlterVisibleCols write FAlterVisibleCols;
    property GridInfo: tGridInfoRecordT read FGridInfo write FGridInfo;
  published
  end;                                  //TmmGridPosEditorDlg

//procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

function TmmGridPosEditorDlg.Execute: boolean;
begin
  mmGridPosEditor := TmmGridPosEditor.Create(Application);
  try
    mmGridPosEditor.AlterColors := FAlterColors;
    mmGridPosEditor.AlterColumnFormat := FAlterColumnFormat;
    mmGridPosEditor.AlterColumnPosition := FAlterColumnPosition;
    mmGridPosEditor.AlterFixedCols := FAlterFixedCols;
    mmGridPosEditor.AlterFonts := FAlterFonts;
    mmGridPosEditor.AlterGridOptions := FAlterGridOptions;
    mmGridPosEditor.AlterVisibleCols := FAlterVisibleCols;
    mmGridPosEditor.GridInfo := GridInfo;
    result := mmGridPosEditor.ShowModal = IDOk;
    if result then begin
      GridInfo := mmGridPosEditor.GridInfo;
    end;                                //if result then
  finally
    mmGridPosEditor.Free;
  end;                                  //try..finally
end;                                    //function TmmGridPosEditorDlg.Execute

end. //mmGridPosEditDlg.pas
as
