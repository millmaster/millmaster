(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMSeries.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: New series for TChart component
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 28.11.2000  1.00  Wss | Initial release
| 10.02.2003  1.00  Wss | Pen.Color wurde nicht gesetzt -> fixed
| 21.02.2003  1.00  Wss | Neue LineSeries mit Min/Max Einf�rbung
| April 2003  1.00  Wss | TmmLineSeries erweitert
                          - Grenzwertbereiche einf�rben
                          - Gewichtete Mittelwertberechnung -> function AddXYMass()
                          ACHTUNG Typecasten!! TmmLineSeries(mChart.Series[i]).AddXYMass()
| 06.06.2003  1.00  Wss | TmmLineSeries: Mittelwert wird nur gezeichnet, wenn er innerhalb
                          des ChartRect liegt -> wegen Zoom wurde es z.T. ausserhalb gezeichnet
| 08.10.2003  1.00  Wss | - TmmLineSeries Erweiterung mit Einf�rben anhand Grenzwertserien
                          - Funktion GetGradientColor hinzugef�gt
|=============================================================================*)
unit MMSeries;

interface

uses
  Classes, Graphics, Windows, Series, SysUtils, TeEngine;

type
  PBorderValueRec = ^TBorderValueRec;
  TBorderValueRec = record
    X: Double;
    Y: Double;
  end;
  
  TBorderValueList = class (TList)
  private
    mTempList: TBorderValueList;
    function GetItems(Index: Integer): PBorderValueRec;
  protected
    function AddNew(aNV: TBorderValueRec): Integer;
    procedure Notify(Ptr: Pointer; Action: TListNotification); override;
  public
    constructor Create(aWidthTemp: Boolean = True); virtual;
    destructor Destroy; override;
    function AppendNew(aBorderRec: TBorderValueRec): Integer;
    procedure Assign(aSource: TBorderValueList);
    procedure CheckAndAddFirst(aNV: TBorderValueRec);
    procedure CheckAndAddNext(aLV, aRV: TBorderValueRec);
    function GetLeftIndex(aValue: TBorderValueRec): Integer;
    function GetRightIndex(aValue: TBorderValueRec): Integer;
    procedure InitFromSerie(aLineSerie: TLineSeries);
    procedure Update;
    property Items[Index: Integer]: PBorderValueRec read GetItems; default;
  end;
  
type
  TBorderMode = (bmNone, bmFix, bmPercentage, bmBorderSeries);
  //--------------------------------------------------------------------------
  IExtendSeries = interface (IUnknown)
    ['{07F86040-7F39-4B60-8591-996C8A76DDA6}']
    function AddXYMass(const AXValue, AYValue, aMassValue: Double; const ALabel: String; AColor: 
            TColor): LongInt;
    procedure DrawClickPoint;
    function GetData: Pointer; stdcall;
    function GetMassValue: Double; stdcall;
    function GetMeanValue: Double; stdcall;
    function GetShowAverage: Boolean; stdcall;
    function GetShowClickPoint: Boolean;
    function GetTag: Integer; stdcall;
    function GetValue: Variant; stdcall;
    procedure MarkClickPoint(const aValue: Boolean);
    procedure SetData(aValue: Pointer); stdcall;
    procedure SetMassValue(aValue: Double); stdcall;
    procedure SetMeanValue(aValue: Double); stdcall;
    procedure SetShowAverage(const Value: Boolean);
    procedure SetShowClickPoint(aValue: Boolean);
    procedure SetTag(aValue: Integer); stdcall;
    procedure SetValue(aValue: Variant); stdcall;
    property Data: Pointer read GetData write SetData;
    property MassValue: Double read GetMassValue write SetMassValue;
    property MeanValue: Double read GetMeanValue write SetMeanValue;
    property ShowAverage: Boolean read GetShowAverage write SetShowAverage;
    property ShowClickPoint: Boolean read GetShowClickPoint write SetShowClickPoint;
    property Tag: Integer read GetTag write SetTag;
    property Value: Variant read GetValue write SetValue;
  end;
  
  //--------------------------------------------------------------------------
  //--------------------------------------------------------------------------
  TExtendSeries = class (TPersistent, IExtendSeries)
    function GetShowAverage: Boolean; stdcall;
    procedure SetShowAverage(const Value: Boolean);
  private
    fData: Pointer;
    fMassValue: Double;
    fMeanValue: Double;
    fShowAverage: Boolean;
    fShowClickPoint: Boolean;
    fTag: Integer;
    fValue: Variant;
    mCoordX: Double;
    mCoordY: Double;
    mDrawClickPoint: Boolean;
    mSerie: TChartSeries;
  public
    constructor Create(aSerie: TChartSeries); virtual;
    function AddXYMass(const AXValue, AYValue, aMassValue: Double; const ALabel: String; AColor: 
            TColor): LongInt;
    procedure Assign(aSource: TPersistent); override;
    procedure DrawClickPoint;
    function GetData: Pointer; stdcall;
    function GetMassValue: Double; stdcall;
    function GetMeanValue: Double; stdcall;
    function GetShowClickPoint: Boolean;
    function GetTag: Integer; stdcall;
    function GetValue: Variant; stdcall;
    procedure MarkClickPoint(const aValue: Boolean);
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    procedure SetData(aValue: Pointer); stdcall;
    procedure SetMassValue(aValue: Double); stdcall;
    procedure SetMeanValue(aValue: Double); stdcall;
    procedure SetShowClickPoint(aValue: Boolean);
    procedure SetTag(aValue: Integer); stdcall;
    procedure SetValue(aValue: Variant); stdcall;
    function _AddRef: Integer; stdcall;
    function _Release: Integer; stdcall;
    property Data: Pointer read GetData write SetData;
    property MassValue: Double read GetMassValue write SetMassValue;
    property MeanValue: Double read GetMeanValue write SetMeanValue;
    property ShowAverage: Boolean read GetShowAverage write SetShowAverage;
    property ShowClickPoint: Boolean read GetShowClickPoint write SetShowClickPoint;
    property Tag: Integer read GetTag write SetTag;
    property Value: Variant read GetValue write SetValue;
  end;
  
  //--------------------------------------------------------------------------
  T2DAreaSeries = class (TAreaSeries, IExtendSeries)
  private
    fExtended: TExtendSeries;
  protected
    procedure DrawValue(ValueIndex:Integer); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoAfterDrawValues; override;
    property Extended: TExtendSeries read fExtended implements IExtendSeries;
  end;
  
  TmmBarSeries = class (TBarSeries, IExtendSeries)
  private
    fExtended: TExtendSeries;
  protected
    procedure DrawValue(ValueIndex:Integer); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure DoAfterDrawValues; override;
    property Extended: TExtendSeries read fExtended implements IExtendSeries;
  end;
  
  TmmLineSeries = class (TLineSeries, IExtendSeries)
  private
    fBorderCondition: Integer;
    fBorderSeries: TList;
    fDateTimeDelta: Single;
    fExtended: TExtendSeries;
    fHighBorder: Double;
    fHighBorderColor: TColor;
    fHighBorderMode: TBorderMode;
    fLowBorder: Double;
    fLowBorderColor: TColor;
    fLowBorderMode: TBorderMode;
    procedure SetBorderCondition(const Value: Integer);
    procedure SetDateTimeDelta(Value: Single);
    procedure SetHighBorder(const Value: Double);
    procedure SetHighBorderColor(const Value: TColor);
    procedure SetHighBorderMode(const Value: TBorderMode);
    procedure SetLowBorder(const Value: Double);
    procedure SetLowBorderColor(const Value: TColor);
    procedure SetLowBorderMode(const Value: TBorderMode);
  protected
    mBorderValues: TBorderValueList;
    procedure DrawValue(ValueIndex:Integer); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function AddNullXY(const AXValue, AYValue: Double; const ALabel: String): LongInt; override;
    function AddXY(const AXValue, AYValue: Double; const ALabel: String; AColor: TColor): LongInt; 
            override;
    procedure Assign(Source: TPersistent); override;
    procedure DoAfterDrawValues; override;
    procedure RefreshSeries; virtual;
    property BorderCondition: Integer read fBorderCondition write SetBorderCondition;
    property BorderSeries: TList read fBorderSeries write fBorderSeries;
    property DateTimeDelta: Single read fDateTimeDelta write SetDateTimeDelta;
    property Extended: TExtendSeries read fExtended implements IExtendSeries;
    property HighBorder: Double read fHighBorder write SetHighBorder;
    property HighBorderColor: TColor read fHighBorderColor write SetHighBorderColor;
    property HighBorderMode: TBorderMode read fHighBorderMode write SetHighBorderMode;
    property LowBorder: Double read fLowBorder write SetLowBorder;
    property LowBorderColor: TColor read fLowBorderColor write SetLowBorderColor;
    property LowBorderMode: TBorderMode read fLowBorderMode write SetLowBorderMode;
  end;
  

//------------------------------------------------------------------------------
function CalcYAverage(aSeries: TChartSeries): Double;
procedure DetermineMinMaxForAxis(aSeries: TChartSeries; aRange: Single = 0.3);
function GetGradientColor(aColor: TColor; aPercentage: Integer): TColor;
//------------------------------------------------------------------------------

implementation
uses
  mmMBCS, mmCS, ColorLibrary;

//------------------------------------------------------------------------------
function CalcYAverage(aSeries: TChartSeries): Double;
var
  i: Integer;
begin
  Result := 0;
  with aSeries do begin
    if YValues.Count > 0 then begin
      for i:=0 to YValues.Count-1 do
        Result := Result + YValue[i];
      Result := Result / YValues.Count;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure DetermineMinMaxForAxis(aSeries: TChartSeries; aRange: Single);
var
  xAvg: Double;
  xAxis: TChartAxis;
  xMin, xMax: Double;
begin
  xAvg := CalcYAverage(aSeries);
  xMin := xAvg - (xAvg * aRange);
  xMax := xAvg + (xAvg * aRange);

  if Assigned(aSeries.CustomVertAxis) then
    xAxis := aSeries.CustomVertAxis
  else if Assigned(aSeries.ParentChart) then begin
    if aSeries.VertAxis = aLeftAxis then
      xAxis := aSeries.ParentChart.LeftAxis
    else
      xAxis := aSeries.ParentChart.RightAxis;
  end else
    Exit;

  if xAxis.Automatic then begin
    xAxis.Automatic := False;
    xAxis.SetMinMax(xMin, xMax);
  end else begin
    if xMin < xAxis.Minimum then
      xAxis.Minimum := xMin;
    if xMax > xAxis.Maximum then
      xAxis.Maximum := xMax;
  end;
end;
//------------------------------------------------------------------------------
function GetGradientColor(aColor: TColor; aPercentage: Integer): TColor;
var
  xH, xL, xS: Integer;
  xRGB: TRGBTriple;
begin
  Result := aColor;
  with xRGB do begin
    rgbtRed   := GetRValue(aColor);
    rgbtGreen := GetGValue(aColor);
    rgbtBlue  := GetBValue(aColor);
  end;

  RGBTripleToHLS(xRGB, xH, xL, xS);
  xL := Round(xL * (1 + (aPercentage / 100)));
  if xL < 0 then xL := 0
  else if xL > 246 then xL := 246;
//  else if xL > 255 then xL := 255;

  try
    xRGB := HLSToRGBTriple(xH, xL, xS);
    with xRGB do
      Result := RGB(rgbtRed, rgbtGreen, rgbtBlue);
  except
  end;

end;
//------------------------------------------------------------------------------
function GetYatX(aLV, aRV: TBorderValueRec; aX: Double): Double;
var
  a, b: Double;
begin
  a := (aRV.Y - aLV.Y) / (aRV.X - aLV.X);
  b := aLV.Y - (a * aLV.X);
  // Y-Wert direkt als Result berechnen
  Result := (a * aX) + b;
end;
//------------------------------------------------------------------------------
function GetCrossValuePoint(aL1, aR1, aL2, aR2: TBorderValueRec; var aCrossPoint: TBorderValueRec): Boolean;
var
  a1,a2, b1, b2: Double;
begin
  Result := False;
  try
    // Parameter von Funktion 1
    a1 := (aR1.Y - aL1.Y) / (aR1.X - aL1.X);
    b1 := aL1.Y - (a1 * aL1.X);
    // Parameter von Funktion 2
    a2 := (aR2.Y - aL2.Y) / (aR2.X - aL2.X);
    b2 := aL2.Y - (a2 * aL2.X);

    if (a2 - a1) <> 0.0 then begin
      with aCrossPoint do begin
        // Schnittpunkt auf X-Achse
        X := (b1 - b2) / (a2 - a1);
        // Schnittpunktwert f�r Y-Achse mit einer Funktion berechnen
        Y := (a1 * X) + b1;
        // Schnittpunkt ist dann gegeben, wenn diese Werte innerhalb der Parameter einer Linie sind
        Result := (X > aL1.X) and (X > aL2.X) and (X < aR1.X) and (X < aR2.X);
      end;
    end;
  except
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TExtendSeries
//:---------------------------------------------------------------------------
constructor TExtendSeries.Create(aSerie: TChartSeries);
begin
  inherited Create;
  //:...................................................................
  fData           := Nil;
  fMassValue      := 0.0;
  fMeanValue      := 0.0;
  fShowAverage    := False;
  fShowClickPoint := False;
  fValue          := 0;
  
  mCoordX         := 0;
  mCoordY         := 0;
  mDrawClickPoint := False;
  mSerie          := aSerie
end; // TExtendSeries.Create

//:---------------------------------------------------------------------------
function TExtendSeries.AddXYMass(const AXValue, AYValue, aMassValue: Double; const ALabel: String; 
        AColor: TColor): LongInt;
begin
  // es wird die gewichtete Differenz zum bestehenden Durchschnittswert hinzugef�gt. Dabei
  // wird die vergr�sserung/verkleinerung des Mittelwert automatisch richtig verrechnet -> LabMaster.doc
  if fMassValue > 0.0 then
    fMeanValue := fMeanValue + ((AYValue - fMeanValue) * aMassValue) / (fMassValue + aMassValue)
  else
    fMeanValue := AYValue;
  fMassValue := fMassValue + aMassValue;
  
  // nicht inherited AddXY aufrufen, da in der abgeleiteten Methode AddXY zus�tzliche eine
  // Nullwertunterdr�ckung implementiert ist.
  Result := mSerie.AddXY(AXValue, AYValue, ALabel, AColor);
end; // TExtendSeries.AddXYMass

//:---------------------------------------------------------------------------
procedure TExtendSeries.Assign(aSource: TPersistent);
var
  xSource: TExtendSeries;
begin
  if aSource is TExtendSeries then begin
    xSource := TExtendSeries(aSource);
    fData      := xSource.Data;
    fMassValue := xSource.MassValue;
    fMeanValue := xSource.MeanValue;
    fValue     := xSource.Value;
  end else
    inherited Assign(aSource);
end; // TExtendSeries.Assign

//:---------------------------------------------------------------------------
procedure TExtendSeries.DrawClickPoint;
var
  xLeft: Integer;
  xTop: Integer;
begin
  // Clickpunkt zeichnen
  if fShowClickPoint and mDrawClickPoint then begin
    with mSerie, ParentChart do begin
      xLeft := CalcXPosValue(mCoordX);
      xTop  := CalcYPosValue(mCoordY);
  //     CodeSite.SendRect('DrawClickPoint: ChartRect', ChartRect);
  //     CodeSite.SendInteger('xLeft', xLeft);
  //     CodeSite.SendInteger('xTop', xTop);
  
      if (xLeft > ChartRect.Left) and (xLeft < ChartRect.Right) and
         (xTop > ChartRect.Top)  and (xTop < ChartRect.Bottom) then begin
        with Canvas do begin
          Pen.Color := clRed;
          Pen.Width := 2;
          Brush.Style := bsClear;
          Ellipse(xLeft-5, xTop-5, xLeft+5, xTop+5);
        end;
      end;
    end; // with
  end; // if fShowClickPoint
end; // TExtendSeries.DrawClickPoint

//:---------------------------------------------------------------------------
function TExtendSeries.GetData: Pointer;
begin
  Result := fData;
end; // TExtendSeries.GetData

//:---------------------------------------------------------------------------
function TExtendSeries.GetMassValue: Double;
begin
  Result := fMassValue;
end; // TExtendSeries.GetMassValue

//:---------------------------------------------------------------------------
function TExtendSeries.GetMeanValue: Double;
begin
  Result := fMeanValue;
end; // TExtendSeries.GetMeanValue

//:---------------------------------------------------------------------------
function TExtendSeries.GetShowAverage: Boolean;
begin
  Result := fShowAverage;
end; // TExtendSeries.GetShowAverage

//:---------------------------------------------------------------------------
function TExtendSeries.GetShowClickPoint: Boolean;
begin
  Result := fShowClickPoint;
end; // TExtendSeries.GetShowClickPoint

//:---------------------------------------------------------------------------
function TExtendSeries.GetTag: Integer;
begin
  Result := fTag;
end; // TExtendSeries.GetTag

//:---------------------------------------------------------------------------
function TExtendSeries.GetValue: Variant;
begin
  Result := fValue;
end; // TExtendSeries.GetValue

//:---------------------------------------------------------------------------
procedure TExtendSeries.MarkClickPoint(const aValue: Boolean);
begin
  mDrawClickPoint := aValue;
  if fShowClickPoint and mDrawClickPoint then begin
    mSerie.GetCursorValues(mCoordX, mCoordY);
  end;
  if Assigned(mSerie.ParentChart) then
    mSerie.ParentChart.Invalidate;
end; // TExtendSeries.MarkClickPoint

//:---------------------------------------------------------------------------
function TExtendSeries.QueryInterface(const IID: TGUID; out Obj): HResult;
begin
  if GetInterface(IID, Obj) then Result := S_OK else Result := E_NOINTERFACE;
end; // TExtendSeries.QueryInterface

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetData(aValue: Pointer);
begin
  fData := aValue;
end; // TExtendSeries.SetData

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetMassValue(aValue: Double);
begin
  fMassValue := aValue;
end; // TExtendSeries.SetMassValue

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetMeanValue(aValue: Double);
begin
  fMeanValue := aValue;
end; // TExtendSeries.SetMeanValue

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetShowAverage(const Value: Boolean);
begin
  if fShowAverage <> Value then
  begin
  //:...................................................................
    fShowAverage := Value;
  //:...................................................................
    if Assigned(mSerie.ParentChart) then
      mSerie.ParentChart.Invalidate;
  //:...................................................................
  end;
end; // TExtendSeries.SetShowAverage

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetShowClickPoint(aValue: Boolean);
begin
  if fShowClickPoint <> aValue then
  begin
  //:...................................................................
    fShowClickPoint := aValue;
  //:...................................................................
    if mSerie is TLineSeries then begin
      TLineSeries(mSerie).Pointer.Visible := fShowClickPoint;
    end else
    if mSerie is TBarSeries then begin
      if fShowClickPoint then TBarSeries(mSerie).BarBrush.Style := bsBDiagonal
                         else TBarSeries(mSerie).BarBrush.Style := bsSolid;
    end else
    if mSerie is TAreaSeries then begin
      if fShowClickPoint then TAreaSeries(mSerie).AreaBrush := bsBDiagonal
                         else TAreaSeries(mSerie).AreaBrush := bsSolid;
    end;
  
    mDrawClickPoint := False;
    if Assigned(mSerie.ParentChart) then
      mSerie.ParentChart.Invalidate;
  //:...................................................................
  end;
end; // TExtendSeries.SetShowClickPoint

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetTag(aValue: Integer);
begin
  fTag := aValue;
end; // TExtendSeries.SetTag

//:---------------------------------------------------------------------------
procedure TExtendSeries.SetValue(aValue: Variant);
begin
    fValue := aValue;
end; // TExtendSeries.SetValue

//:---------------------------------------------------------------------------
function TExtendSeries._AddRef: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end; // TExtendSeries._AddRef

//:---------------------------------------------------------------------------
function TExtendSeries._Release: Integer;
begin
  Result := -1;
  // non-refcounted memorymanagement
end; // TExtendSeries._Release

//:---------------------------------------------------------------------------
//:--- Class: T2DAreaSeries
//:---------------------------------------------------------------------------
constructor T2DAreaSeries.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fExtended        := TExtendSeries.Create(Self);
end; // T2DAreaSeries.Create

//:---------------------------------------------------------------------------
destructor T2DAreaSeries.Destroy;
begin
  FreeAndNil(fExtended);
  //:...................................................................
  inherited Destroy;
end; // T2DAreaSeries.Destroy

//:---------------------------------------------------------------------------
procedure T2DAreaSeries.DoAfterDrawValues;
var
  y: Integer;
begin
  inherited DoAfterDrawValues;
  // Durchschnittslinie zeichnen
  if fExtended.ShowAverage and (XValues.Count > 0) then begin
    y := CalcYPosValue(fExtended.MeanValue * YValues.Multiplier);
    with ParentChart do begin
      if (y > ChartRect.Top) and (y < ChartRect.Bottom) then
        with Canvas do begin
          Pen.Color   := Self.SeriesColor;
          Pen.Style   := psDashDot;
          Brush.Style := bsClear;
          LineWithZ(ChartRect.Left, y, ChartRect.Right, y, StartZ);
        end;
    end;
  end;
  // Clickpunkt zeichnen
  fExtended.DrawClickPoint;
end; // T2DAreaSeries.DoAfterDrawValues

//:---------------------------------------------------------------------------
procedure T2DAreaSeries.DrawValue(ValueIndex:Integer);
var
  x1, x2: Integer;
  y1, y2: Integer;
  b1, b2: Integer;
begin
  if ValueIndex > 0 then begin
    x1 := CalcXPosValue(XValue[ValueIndex-1]);
    x2 := CalcXPosValue(XValue[ValueIndex]);
  
    y1 := CalcYPosValue(YValue[ValueIndex-1]);
    y2 := CalcYPosValue(YValue[ValueIndex]);
  
    b1 := GetOriginPos(ValueIndex-1);
    b2 := GetOriginPos(ValueIndex);
  
    with ParentChart.Canvas do begin
      Brush.Color := SeriesColor;
      Pen.Color   := clSilver; //clGray;
      PlaneWithZ(Point(x1, b1), Point(x1, y1), Point(x2, y2), Point(x2, b2), MiddleZ);
    end;
  end;
end; // T2DAreaSeries.DrawValue

//:---------------------------------------------------------------------------
//:--- Class: TmmBarSeries
//:---------------------------------------------------------------------------
constructor TmmBarSeries.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fExtended        := TExtendSeries.Create(Self);
end; // TmmBarSeries.Create

//:---------------------------------------------------------------------------
destructor TmmBarSeries.Destroy;
begin
  FreeAndNil(fExtended);
  //:...................................................................
  inherited Destroy;
end; // TmmBarSeries.Destroy

//:---------------------------------------------------------------------------
procedure TmmBarSeries.DoAfterDrawValues;
var
  y: Integer;
begin
  inherited DoAfterDrawValues;
  // Durchschnittslinie zeichnen
  if fExtended.ShowAverage and (XValues.Count > 0) then begin
    y := CalcYPosValue(fExtended.MeanValue * YValues.Multiplier);
    with ParentChart do begin
      if (y > ChartRect.Top) and (y < ChartRect.Bottom) then
        with Canvas do begin
          Pen.Color   := Self.SeriesColor;
          Pen.Style   := psDashDot;
          Brush.Style := bsClear;
          LineWithZ(ChartRect.Left, y, ChartRect.Right, y, StartZ);
        end;
    end;
  end;
  // Clickpunkt zeichnen
  fExtended.DrawClickPoint;
end; // TmmBarSeries.DoAfterDrawValues

//:---------------------------------------------------------------------------
procedure TmmBarSeries.DrawValue(ValueIndex:Integer);
var
  xAxis: TChartAxis;
begin
  // Wenn die Achsen per Prozentwerte in der H�he verstellt werden, dann werden
  // Werte �ber die "virtuelle" Achsenbeschr�nkung von Min/Max Werte trotzdem gezeichnet
  // Um dies zu unterbinden wird per Funktion ClipRectangle() ein Rechteck definiert,
  // welches die g�ltige Zeichenfl�che anhand der Achseninformationen ermittelt.
  // Kann nicht in OnBeforeDrawValues() gesetzt werden. Warum weiss ich auch nicht!!
  with ParentChart do begin
    with Canvas do begin
      if Assigned(Self.CustomVertAxis) OR
         ((VertAxis = aLeftAxis) and (LeftAxis.StartPosition > 0)) OR
         ((VertAxis = aRightAxis) and (RightAxis.StartPosition > 0)) then begin
        if Assigned(CustomVertAxis) then
          xAxis := CustomVertAxis
        else if VertAxis = aLeftAxis then
          xAxis := LeftAxis
        else
          xAxis := RightAxis;
        ClipRectangle(Rect(ChartRect.Left, xAxis.IStartPos, ChartRect.Right, xAxis.IEndPos));
      end;
    end;
  end;
  inherited DrawValue(ValueIndex);
end; // TmmBarSeries.DrawValue

//:---------------------------------------------------------------------------
//:--- Class: TmmLineSeries
//:---------------------------------------------------------------------------
constructor TmmLineSeries.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fBorderCondition := 1;
  fBorderSeries    := TList.Create;
  fDateTimeDelta   := 0.0;
  fExtended        := TExtendSeries.Create(Self);
  fHighBorder      := 0;
  fHighBorderColor := clTeeColor;
  fHighBorderMode  := bmNone;
  fLowBorder       := 0;
  fLowBorderColor  := clTeeColor;
  fLowBorderMode   := bmNone;
  
  mBorderValues    := Nil; //TBorderValueList.Create;
end; // TmmLineSeries.Create

//:---------------------------------------------------------------------------
destructor TmmLineSeries.Destroy;
begin
  FreeAndNil(fBorderSeries);
  FreeAndNil(fExtended);
  FreeAndNil(mBorderValues);
  inherited Destroy;
end; // TmmLineSeries.Destroy

//:---------------------------------------------------------------------------
function TmmLineSeries.AddNullXY(const AXValue, AYValue: Double; const ALabel: String): LongInt;
begin
  // es muss direkt der inherited aufgerufen werden, da bei einem normalen AddXY auf unsere
  // �berschriebene Methode verweist wird, welche dann wiederum diese AddNullXY aufrufen w�rde.
  // w�re dann Rekursiv!
  Result := inherited AddXY(AXValue, AYValue, ALabel, clNone);
end; // TmmLineSeries.AddNullXY

//:---------------------------------------------------------------------------
function TmmLineSeries.AddXY(const AXValue, AYValue: Double; const ALabel: String; AColor: TColor): 
        LongInt;
begin
  // wenn Zeitdifferenz vom letzten X-Wert > Zeitschrittwert ist, dann ein Null-Wert einf�gen
  // damit keine Verbindunslinie zwischen diesen beiden Punkten gezeichnet wird
  // -> ValueColor <> clNone
  if ParentChart.BottomAxis.IsDateTime and (XValues.Count > 0) and
     ((fDateTimeDelta > 0.01) and ((AXValue - XValue[XValues.Count-1]) >= (2 * fDateTimeDelta))) then
    AddNullXY(AXValue, AYValue, ALabel);
  
  Result := inherited AddXY(AXValue, AYValue, ALabel, AColor);
end; // TmmLineSeries.AddXY

//:---------------------------------------------------------------------------
procedure TmmLineSeries.Assign(Source: TPersistent);
begin
  if Source is TmmLineSeries then begin
    with TmmLineSeries(Source) do begin
      Self.fDateTimeDelta   := DateTimeDelta;
      Self.fLowBorder       := LowBorder;
      Self.fLowBorderColor  := LowBorderColor;
      Self.fLowBorderMode   := LowBorderMode;
      Self.fHighBorder      := HighBorder;
      Self.fHighBorderColor := HighBorderColor;
      Self.fHighBorderMode  := HighBorderMode;
      Self.fExtended.Assign(Extended);
    end;
  end;
  inherited Assign(Source);
end; // TmmLineSeries.Assign

//:---------------------------------------------------------------------------
{* Nach dem Zeichnen wird der gewichtete Mittelwert noch gezeichnet.
*}
procedure TmmLineSeries.DoAfterDrawValues;
var
  y: Integer;
begin
  inherited DoAfterDrawValues;
  //:...................................................................
  // Durchschnittslinie zeichnen
  if fExtended.ShowAverage and (XValues.Count > 0) then begin
    y := CalcYPosValue(fExtended.MeanValue * YValues.Multiplier);
    with ParentChart do begin
      if (y > ChartRect.Top) and (y < ChartRect.Bottom) then
        with Canvas do begin
          Pen.Color   := Self.SeriesColor;
          Pen.Style   := psDashDot;
          Brush.Style := bsClear;
          LineWithZ(ChartRect.Left, y, ChartRect.Right, y, StartZ);
        end;
    end;
  end;
  // Clickpunkt zeichnen
  fExtended.DrawClickPoint;
end; // TmmLineSeries.DoAfterDrawValues

//:---------------------------------------------------------------------------
procedure TmmLineSeries.DrawValue(ValueIndex:Integer);
var
  a1, a2, b1, b2: Integer;
  xLeft, xRight: Integer;
  x1, x2: Integer;
  y1, y2: Integer;
  yb1, yb2, yb3: Integer;
  xHighBorderValue: Double;
  xLowBorderValue: Double;
  xAxis: TChartAxis;
  xPointArray: Array of TPoint;
  
  //............................................................
  procedure AddPoint(aValue: TBorderValueRec);
  var
    xCount: Integer;
  begin
    xCount := Length(xPointArray);
    SetLength(xPointArray, xCount+1);
    with ParentChart do begin
      xPointArray[xCount].X := CalcXPosValue(aValue.X);
      xPointArray[xCount].Y := CalcYPosValue(aValue.Y);
    end;
  end;
  //............................................................
  procedure DrawAreaPoints;
  var
    i: Integer;
  begin
    // noch den Abschluss mit dem ersten Poligonpunkt herstellen
    i := Length(xPointArray);
    SetLength(xPointArray, i+1);
    xPointArray[i] := xPointArray[0];
  
    with ParentChart.Canvas do begin
      Pen.Color   := GetGradientColor(SeriesColor, 60);
      Pen.Width   := 1;
      Brush.Color := Pen.Color;
      Brush.Style := bsSolid;
      PolygonWithZ(xPointArray, StartZ);
    end;
  end;
  
  //............................................................
  function ValueIndexOverBorder(aValueIndex: Integer): Boolean;
  var
    xBIndex: Integer;
    xLB, xRB: TBorderValueRec;
    xV: TBorderValueRec;
    xY: Double;
  begin
    Result := False;
    xV.X := XValue[aValueIndex];
    xV.Y := YValue[aValueIndex];
    // Erst die Indexes f�r den Bereich aus der mBorderValues Liste ermitteln
    xBIndex := mBorderValues.Count-1;
    while (xBIndex >= 0) do begin
      if mBorderValues.Items[xBIndex]^.X <= xV.X then begin
        xLB := mBorderValues.Items[xBIndex]^;
        Break;
      end;
      dec(xBIndex);
    end;
    if xBIndex = -1 then
      Exit;
  
    xBIndex := 0;
    while (xBIndex < mBorderValues.Count) do begin
      if mBorderValues.Items[xBIndex]^.X > xV.X then begin
        xRB := mBorderValues.Items[xBIndex]^;
        Break;
      end;
      inc(xBIndex);
    end;
    if xBIndex = mBorderValues.Count then
      Exit;
  
  
    // Y-Wert von Datenwert auf der Grenzgerade
    xY := GetYatX(xLB, xRB, xV.X);
    Result := (xV.Y > xY);
  end;
  //............................................................
  procedure DrawBorderArea(aLeftIndex, aRightIndex: Integer);
  var
    i: Integer;
    xLIndex, xRIndex: Integer;
    xRec: TBorderValueRec;
    xLB, xRB: TBorderValueRec;
    xLV, xRV: TBorderValueRec;
    xY: Double;
  begin
    xLV.X := XValue[aLeftIndex];
    xLV.Y := YValue[aLeftIndex];
    xRV.X := XValue[aRightIndex];
    xRV.Y := YValue[aRightIndex];
  
    // Linker Index aus der BorderValues Liste suchen
    xLIndex := mBorderValues.GetLeftIndex(xLV);
    if xLIndex = -1 then
      Exit;
  
    // Rechter Index aus der BorderValues Liste suchen
    xRIndex := mBorderValues.GetRightIndex(xRV);
    if xRIndex = -1 then
      Exit;
  
    // Nun die Datenwerte innerhalb dieses Bereiches LIndex - RIndex verarbeiten
    for i:=xLIndex to xRIndex-1 do begin
      xLB := mBorderValues.Items[i]^;
      xRB := mBorderValues.Items[i+1]^;
  
      // Linker Datenpunkt verarbeiten
      if xLB.X <= xLV.X then begin
        // Y-Wert von linkem Datenwert auf der ersten Grenzgerade
        xY := GetYatX(xLB, xRB, xLV.X);
        // Y-Grenzwert bei diesem Datenpunkt ist tiefer -> aufnehmen
        if xY < xLV.Y then begin
          // Startpunkt f�r Poligon ist der linke Datenwert
          AddPoint(xLV);
          // und der zweite Punkt f�rs Poligon ist gleich noch der Grenz/Schnittwert
          xRec.X := xLV.X;
          xRec.Y := xY;
          AddPoint(xRec);
        end;
  
        // wenn Datenpunkt unter der Grenzlinie ist, dann wird dieser Schnittpunkt
        // der Startpunkt f�r das Poligon
        // schneiden sich die die Linien rechts vom linken Datenpunkt?
        if GetCrossValuePoint(xLV, xRV, xLB, xRB, xRec) then
          AddPoint(xRec);
      end
      // Grenzzwischenpunkt liegt zwischen den beiden Wertpunkte
      else if (xLB.X > xLV.X) and (xLB.X < xRV.X) then begin
        xY := GetYatX(xLV, xRV, xLB.X);
        if xY >= xLB.Y then
          AddPoint(xLB);
  
        // schneiden sich die die Linien rechts vom linken Datenpunkt?
        if GetCrossValuePoint(xLV, xRV, xLB, xRB, xRec) then
          AddPoint(xRec);
        // beim zweit letzten Grenzpunkt muss noch dieser separat behandelt werden
        // da er sonst �bersprungen wird weil der Indexz�hler i fertig ist
        if i = xRIndex-1 then begin
          xY := GetYatX(xLV, xRV, xRB.X);
          if xY > xRB.Y then
            AddPoint(xRB);
        end;
      end;
  
      // Rechter Datenpunkt unabh�ngig verarbeiten
      if xRB.X >= xRV.X then begin
        // Y-Wert von rechtem Datenwert auf der letzten Grenzgerade
        xY := GetYatX(xLB, xRB, xRV.X);
        // Grenzwert bei diesem Datenpunkt ist tiefer -> aufnehmen
        if xY < xRV.Y then begin
          // dieser Grenzpunkt ins Poligon �bernehmen
          xRec.X := xRV.X;
          xRec.Y := xY;
          AddPoint(xRec);
          // Rechter Datenpunkt ist der zweitletzte Punkt f�rs Poligon
          AddPoint(xRV);
        end;
  
        // wenn letzte Grenzlinie die Datenlinie schneidet, dann ist dieser Schnittpunkt
        // der zweitletzte Punkt f�rs Poligon
        if GetCrossValuePoint(xLV, xRV, xLB, xRB, xRec) then
          AddPoint(xRec);
      end;
    end; // for i
  
    if Length(xPointArray) > 0 then begin
      DrawAreaPoints;
      SetLength(xPointArray, 0);
    end;
  end;
  //............................................................
  procedure PaintPlane(aColor: TColor);
  begin
    with ParentChart.Canvas do begin
      Pen.Color   := aColor;
      Brush.Color := aColor;
      Brush.Style := Self.AreaBrush;
      PlaneWithZ(Point(x1, yb1), Point(x1, yb2), Point(x2, yb3), Point(x2, yb1), StartZ);
    end;
  end;
  //............................................................
  procedure CalcRightIntersection(aBorderValue: Double; aInverted: Boolean);
  var
    xIntersection: Boolean;
  begin
    if aInverted then xIntersection := aBorderValue < YValue[ValueIndex]
                 else xIntersection := aBorderValue > YValue[ValueIndex];
      // Grenzlinie schneidet Kurve -> Schnittpunkt ermitteln
    yb2 := CalcYPosValue(YValue[ValueIndex-1]);
    if xIntersection then begin
      yb3 := yb1;
      b2  := yb1 - y2;
      try
        a2  := Round((a1 * b2) / b1);
      except
        a2 := 0;
      end;
      x2  := x2 + a2;
    end else
      yb3 := y2;
  end;
  //............................................................
  procedure CalcLeftIntersection(aBorderValue: Double; aInverted: Boolean);
  var
    xIntersection: Boolean;
  begin
    if aInverted then xIntersection := aBorderValue < YValue[ValueIndex-1]
                 else xIntersection := aBorderValue > YValue[ValueIndex-1];
      // Grenzlinie schneidet Kurve -> Schnittpunkt ermitteln
    yb3 := CalcYPosValue(YValue[ValueIndex]);
    if xIntersection then begin
      yb2 := yb1;
      b2  := yb1 - y1;
      try
        a2  := Round((a1 * b2) / b1);
      except
        a2 := 0;
      end;
      x1  := x1 + a2;
    end else
      yb2 := y1;
  end;
    //............................................................
  function GetHighBorderValue: Double;
  begin
    case fHighBorderMode of
      bmFix:          Result := fHighBorder;
      bmPercentage:   Result := fExtended.MeanValue + (fExtended.MeanValue * HighBorder);
    else
      Result := 0;
    end;
    Result := Result * YValues.Multiplier;
  end;
  //............................................................
  function GetLowBorderValue: Double;
  begin
    case fLowBorderMode of
      bmFix:          Result := fLowBorder;
      bmPercentage:   Result := fExtended.MeanValue - (fExtended.MeanValue * LowBorder);
    else
      Result := 0;
    end;
    Result := Result * YValues.Multiplier;
  end;
  //............................................................
  function BorderConditionOK(aBorderMode: TBorderMode; aBorderValue: Double; aInverted: Boolean): Boolean;
  var
    i: Integer;
    xMinIndex, xMaxIndex: Integer;
    xIndexCount: Integer;
  begin
    Result := False;
    if (aBorderMode = bmBorderSeries) and not Assigned(mBorderValues) then
      Exit;
  
    xMinIndex := ValueIndex - fBorderCondition;
    xMaxIndex := ValueIndex + fBorderCondition - 1;
    if xMinIndex < 0 then
      xMinIndex := 0;
  
    if xMaxIndex > XValues.Count-1 then
      xMaxIndex := XValues.Count-1;
  
    xIndexCount := 0;
    for i:=xMinIndex to xMaxIndex do begin
      if aBorderMode = bmBorderSeries then begin
        if ValueIndexOverBorder(i) then
          inc(xIndexCount)
        else
          xIndexCount := 0;
      end else begin
        if ((not aInverted) and (YValue[i] > aBorderValue)) OR
           ( aInverted and (YValue[i] < aBorderValue)) then
          inc(xIndexCount)
        else
          xIndexCount := 0;
      end;
  
      Result := (xIndexCount >= fBorderCondition);
      if Result then
        Break;
    end; // for
  end;
  //............................................................
  
begin
  if (ValueIndex > 0) and not ParentChart.View3D then begin
    // Wenn die Achsen per Prozentwerte in der H�he verstellt werden, dann werden
    // Werte �ber die "virtuelle" Achsenbeschr�nkung von Min/Max trotzdem gezeichnet
    // Um dies zu unterbinden wird per Funktion ClipRectangle() ein Rechteck definiert,
    // welches die g�ltige Zeichenfl�che anhand der Achseninformationen ermittelt.
    // Kann nicht in OnBeforeDrawValues() gesetzt werden. Warum weiss ich auch nicht!!
    with ParentChart do begin
      with Canvas do begin
        if Assigned(Self.CustomVertAxis) OR
           ((VertAxis = aLeftAxis) and (LeftAxis.StartPosition > 0)) OR
           ((VertAxis = aRightAxis) and (RightAxis.StartPosition > 0)) then begin
          if Assigned(CustomVertAxis) then
            xAxis := CustomVertAxis
          else if VertAxis = aLeftAxis then
            xAxis := LeftAxis
          else
            xAxis := RightAxis;
          ClipRectangle(Rect(ChartRect.Left, xAxis.IStartPos, ChartRect.Right, xAxis.IEndPos));
        end;
      end;
    end;
  
    xLeft  := CalcXPosValue(XValue[ValueIndex-1]);
    xRight := CalcXPosValue(XValue[ValueIndex]);
    y1     := CalcYPosValue(YValue[ValueIndex-1]);
    y2     := CalcYPosValue(YValue[ValueIndex]);
    a1     := xRight - xLeft;
    b1     := y2 - y1;
  
    if (ValueColor[ValueIndex] <> clNone) and (ValueColor[ValueIndex-1] <> clNone) then begin
      if fHighBorderMode <> bmNone then begin
        xHighBorderValue := GetHighBorderValue;
        // Wenn BorderCondition OK ist...
        if BorderConditionOK(fHighBorderMode, xHighBorderValue, False) then begin
          // ...dann der Modus BorderSeries anderst behandeln...
          if fHighBorderMode = bmBorderSeries then
            DrawBorderArea(ValueIndex-1, ValueIndex)
          // ...als die Fix- und Percentage Modus
          else begin
            x1  := xLeft;
            x2  := xRight;
            yb1 := CalcYPosValue(xHighBorderValue);
            if YValue[ValueIndex-1] < YValue[ValueIndex] then
              // steigende Flanke
              CalcLeftIntersection(xHighBorderValue, False)
            else
              // fallende Flanke
              CalcRightIntersection(xHighBorderValue, False);
  
            PaintPlane(HighBorderColor);
          end;
        end;
      end;
  
      if fLowBorderMode in [bmFix, bmPercentage] then begin
        xLowBorderValue := GetLowBorderValue;
        // xLowBorderValue nur dann behandeln, wenn auch St�tzwerte drunter liegen
        if BorderConditionOK(fLowBorderMode, xLowBorderValue, True) then begin
          x1  := xLeft;
          x2  := xRight;
          yb1 := CalcYPosValue(xLowBorderValue);
          if YValue[ValueIndex-1] > YValue[ValueIndex] then
            // fallende Flanke
            CalcLeftIntersection(xLowBorderValue, True)
          else
            // steigende Flanke
            CalcRightIntersection(xLowBorderValue, True);
  
          PaintPlane(LowBorderColor);
        end; // if BorderConditionOK
      end; // if fLowBorderMode in []
    end; // if ValueColor <> clNone
  end;
  inherited DrawValue(ValueIndex);
end; // TmmLineSeries.DrawValue

//:---------------------------------------------------------------------------
{* Am Schluss, wenn Grenzserien hinzugef�gt wurden, muss RefreshSeries aufgerufen werden, damit die 
Grenzpunkte berechnet werden k�nnen, um sp�ter danach einzuf�rben. 
*}
procedure TmmLineSeries.RefreshSeries;
var
  i, j: Integer;
  xBS: TmmLineSeries;
  xRV: TBorderValueRec;
  xLV: TBorderValueRec;
  
  // CrossSerieLeft, CrossSerieRight: Indexes von fBorderSeries
    //..........................................................
  function AsBorderSerie(aIndex: Integer): TmmLineSeries;
  begin
    Result := TmmLineSeries(fBorderSeries.Items[aIndex]);
  end;
    //..........................................................
  
begin
  inherited RefreshSeries;
  if fBorderSeries.Count > 0 then begin
    // Kopie von den Datenwerten von erster BorderSerie erstellen
    if not Assigned(mBorderValues) then
      mBorderValues := TBorderValueList.Create
    else
      mBorderValues.Clear;
    mBorderValues.InitFromSerie(AsBorderSerie(0));
    // nun die weiteren BorderSerien durchgehen und berechnen lassen
    for j:=1 to fBorderSeries.Count-1 do begin
      xBS := AsBorderSerie(j);
      for i:=0 to xBS.XValues.Count-1 do begin
        xRV.X := xBS.XValues[i];
        xRV.Y := xBS.YValues[i];
        if i = 0 then mBorderValues.CheckAndAddFirst(xRV)
                 else mBorderValues.CheckAndAddNext(xLV, xRV);
        xLV := xRV;
      end; // for i
      mBorderValues.Update;
    end; // for j
  end; // if Count
end; // TmmLineSeries.RefreshSeries

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetBorderCondition(const Value: Integer);
begin
  if Value <> fBorderCondition then begin
    fBorderCondition := Value;
    if fBorderCondition < 1 then
      fBorderCondition := 1;
  
    if Assigned(ParentChart) then
      ParentChart.Invalidate;
  end;
end; // TmmLineSeries.SetBorderCondition

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetDateTimeDelta(Value: Single);
begin
  if fDateTimeDelta <> Value then
  begin
  //:...................................................................
    fDateTimeDelta := Value;
  //:...................................................................
  end;
end; // TmmLineSeries.SetDateTimeDelta

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetHighBorder(const Value: Double);
begin
  fHighBorder := Value;
  if Assigned(ParentChart) then
    ParentChart.Invalidate;
end; // TmmLineSeries.SetHighBorder

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetHighBorderColor(const Value: TColor);
begin
  fHighBorderColor := Value;
  if Assigned(ParentChart) then
    ParentChart.Invalidate;
end; // TmmLineSeries.SetHighBorderColor

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetHighBorderMode(const Value: TBorderMode);
begin
  fHighBorderMode := Value;
  if Assigned(ParentChart) then
    ParentChart.Invalidate;
end; // TmmLineSeries.SetHighBorderMode

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetLowBorder(const Value: Double);
begin
  fLowBorder := Value;
  if Assigned(ParentChart) then
    ParentChart.Invalidate;
end; // TmmLineSeries.SetLowBorder

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetLowBorderColor(const Value: TColor);
begin
  fLowBorderColor := Value;
  if Assigned(ParentChart) then
    ParentChart.Invalidate;
end; // TmmLineSeries.SetLowBorderColor

//:---------------------------------------------------------------------------
procedure TmmLineSeries.SetLowBorderMode(const Value: TBorderMode);
begin
  if Value <> bmBorderSeries then begin
    fLowBorderMode := Value;
    if Assigned(ParentChart) then
      ParentChart.Invalidate;
  end;
end; // TmmLineSeries.SetLowBorderMode

//:---------------------------------------------------------------------------
//:--- Class: TBorderValueList
//:---------------------------------------------------------------------------
constructor TBorderValueList.Create(aWidthTemp: Boolean = True);
begin
  inherited Create;
  if aWidthTemp then mTempList := TBorderValueList.Create(False)
                else mTempList := Nil;
end; // TBorderValueList.Create

//:---------------------------------------------------------------------------
destructor TBorderValueList.Destroy;
begin
  FreeAndNil(mTempList);
  inherited Destroy;
end; // TBorderValueList.Destroy

//:---------------------------------------------------------------------------
function TBorderValueList.AddNew(aNV: TBorderValueRec): Integer;
var
  xRec: PBorderValueRec;
  xItem: PBorderValueRec;
  xFound: Boolean;
begin
  xFound := False;
  for Result:=0 to Count-1 do begin
    xItem  := Items[Result];
    // Bei gleichen X-Werten aber der Y-Wert ist h�her dann diesen �bernehmen
    if aNV.X = xItem^.X then begin
      if (aNV.Y > xItem^.Y) then
        xItem^ := aNV;
      xFound := True;
      Break;
    end
    // suche die Position in der Liste wo der neue Punkt eingef�gt werden soll
    else if aNV.X < xItem^.X then begin
      new(xRec);
      xRec^ := aNV;
      Insert(Result, xRec);
      xFound := True;
      Break;
    end
  end;
  // dieser X-Wert ist h�her als die in der Liste -> einfach hinten anstellen
  if not xFound then begin
    new(xRec);
    xRec^ := aNV;
    Result := Add(xRec);
  end;
end; // TBorderValueList.AddNew

//:---------------------------------------------------------------------------
function TBorderValueList.AppendNew(aBorderRec: TBorderValueRec): Integer;
var
  xRec: PBorderValueRec;
begin
  new(xRec);
  xRec^  := aBorderRec;
  Result := Add(xRec);
end; // TBorderValueList.AppendNew

//:---------------------------------------------------------------------------
procedure TBorderValueList.Assign(aSource: TBorderValueList);
var
  i: Integer;
begin
  Clear;
  for i:=0 to aSource.Count-1 do
    AppendNew(aSource.Items[i]^);
end; // TBorderValueList.Assign

//:---------------------------------------------------------------------------
procedure TBorderValueList.CheckAndAddFirst(aNV: TBorderValueRec);
var
  xRec: PBorderValueRec;
  i: Integer;
  xY: Double;
begin
  for i:=0 to Count-1 do begin
    new(xRec);
    xRec^ := Items[i]^;
    // alles was weiter Links ist hinzuf�gen
    if xRec^.X < aNV.X then begin
      mTempList.Add(xRec);
    end
    // Gleicher X-Wert aber Y-Wert gr�sser dann den h�heren Y-Wert nehmen anschliessend abbrechen
    else if xRec^.X = aNV.X then begin
      if aNV.Y > xRec^.Y then
        xRec^.Y := aNV.Y;
      mTempList.Add(xRec);
      Break;
    end
    // neuer Punkt liegt zwischen vorhandenen Punkten
    else if xRec^.X > aNV.X then begin
      if i > 0 then begin
        xY := GetYatX(Items[i-1]^, xRec^, aNV.X);
        if aNV.Y > xY then
          xRec^ := aNV;
      end else
        xRec^ := aNV;
      mTempList.Add(xRec);
      Break;
    end;
  end;
end; // TBorderValueList.CheckAndAddFirst

//:---------------------------------------------------------------------------
procedure TBorderValueList.CheckAndAddNext(aLV, aRV: TBorderValueRec);
var
  xL: TBorderValueRec;
  xR: TBorderValueRec;
  xLIndex: Integer;
  xRIndex: Integer;
  xCrossPoint: TBorderValueRec;
  i: Integer;
  xY: Double;
begin
  // Linker Index von BV holen
  xLIndex := GetLeftIndex(aLV);
  if xLIndex = -1 then
    xLIndex := AddNew(aLV);
  
  // Rechter Index von BV holen
  xRIndex := GetRightIndex(aRV);
  if xRIndex = -1 then
    xRIndex := AddNew(aRV);
  
  // nun diese Werte durchz�hlen und pr�fen
  for i:=xLIndex to xRIndex-1 do begin
    xL := Items[i]^;
    xR := Items[i+1]^;
  
    if GetCrossValuePoint(aLV, aRV, xL, xR, xCrossPoint) then begin
      mTempList.AddNew(xCrossPoint);
    end
    else if (aRV.X > xL.X) and (aRV.X <= xR.X) then begin
      // was f�r einen Y-Wert hat der rechte Datenpunkt aRV auf einer vorhandenen Grenzlinie xL-xR?
      xY := GetYatX(xL, xR, aRV.X);
      // wenn der neue rechte Punkt h�her ist als der entspr. Punkt auf der Grenzlinie,
      // dann diesen �bernehmen und Schlaufe verlassen. Wir sind hier sowieso bereits an
      // der letzten Grenzlinie f�r diesen Datenabschnitt.
      if aRV.Y > xY then begin
        mTempList.AddNew(aRV);
        break;
      end;
    end;
  
    // Wenn der Y-Wert von der Parameterlinie LV-RV bei diesem X-Wert gem�ss der Funktion
    // y = ax + b h�her ist, dann diesen Wert �bernehmen und in neue Liste abf�llen
    xY := GetYatX(aLV, aRV, xR.X);
    if xY > xR.Y then
      xR.Y := xY;
    mTempList.AddNew(xR);
  end; // for i
end; // TBorderValueList.CheckAndAddNext

//:---------------------------------------------------------------------------
function TBorderValueList.GetItems(Index: Integer): PBorderValueRec;
begin
  Result := inherited Items[Index];
end; // TBorderValueList.GetItems

//:---------------------------------------------------------------------------
function TBorderValueList.GetLeftIndex(aValue: TBorderValueRec): Integer;
begin
  // Linker Index aus der BorderValues Liste suchen
  Result := Count-1;
  while Result >= 0 do begin
    if Items[Result]^.X <= aValue.X then begin
      Break;
    end;
    dec(Result);
  end;
end; // TBorderValueList.GetLeftIndex

//:---------------------------------------------------------------------------
function TBorderValueList.GetRightIndex(aValue: TBorderValueRec): Integer;
begin
  Result := 0;
  while Result < Count do begin
    if Items[Result]^.X >= aValue.X then begin
      Break;
    end;
    inc(Result);
  end;
  if Result = Count then
    Result := -1;
end; // TBorderValueList.GetRightIndex

//:---------------------------------------------------------------------------
procedure TBorderValueList.InitFromSerie(aLineSerie: TLineSeries);
var
  xRec: PBorderValueRec;
  i: Integer;
begin
  Clear;
  mTempList.Clear;
  for i:=0 to aLineSerie.XValues.Count-1 do begin
    new(xRec);
    xRec^.X := aLineSerie.XValues[i];
    xRec^.Y := aLineSerie.YValues[i];
    Add(xRec);
  end;
end; // TBorderValueList.InitFromSerie

//:---------------------------------------------------------------------------
procedure TBorderValueList.Notify(Ptr: Pointer; Action: TListNotification);
var
  xRec: PBorderValueRec;
begin
  if Action = lnDeleted then begin
    xRec := Ptr;
    Dispose(xRec);
  end;
end; // TBorderValueList.Notify

//:---------------------------------------------------------------------------
procedure TBorderValueList.Update;
var
  xRec: TBorderValueRec;
  i: Integer;
begin
  // Falls die aktuelle Liste in der X-Achse noch weitere Datenpunkte hat
  // m�ssen diese noch in die TempList umkopiert werden
  if mTempList.Count > 0 then begin
    xRec := mTempList.Items[mTempList.Count-1]^;
    for i:=0 to Count-1 do begin
      if Items[i]^.X > xRec.X then
        mTempList.AddNew(Items[i]^);
    end;
  end;
  Assign(mTempList);
  mTempList.Clear;
end; // TBorderValueList.Update

end.

