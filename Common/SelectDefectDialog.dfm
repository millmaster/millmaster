inherited frmSelectDefects: TfrmSelectDefects
  Left = 380
  Top = 279
  Caption = '(*)Klassierdaten'
  ClientHeight = 151
  ClientWidth = 200
  Position = poOwnerFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 8
    Top = 121
  end
  inherited bCancel: TmmButton
    Left = 108
    Top = 121
  end
  object rgDefectType: TmmRadioGroup
    Left = 8
    Top = 8
    Width = 185
    Height = 105
    Caption = '(*)Fehlerart'
    Items.Strings = (
      '(30)Schnitte'
      '(30)Ungeschnittene'
      '(*)Beide')
    TabOrder = 2
    CaptionSpace = True
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 152
    Top = 16
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
