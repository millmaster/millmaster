(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphOption.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: dialog interface for graph options editor
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.01 | PW  |
|=========================================================================================*)

unit mmGraphOption;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,
 mmGraphOptionDlg,mmGraphGlobal;

type
 TmmGraphOptions = class(TComponent)
  private
   FAutoScaling: array[eYAxisT] of boolean;
   FBarStyle: eBarStyleT;
   FDefaultMinY: array[eYAxisT] of ValueType;
   FDefaultMaxY: array[eYAxisT] of ValueType;
   procedure SetAutoScaling(index: eYAxisT; value: boolean);
   function GetAutoScaling(index: eYAxisT): boolean;
   procedure SetDefaultMinY(index: eYAxisT; value: ValueType);
   function GetDefaultMinY(index: eYAxisT): ValueType;
   procedure SetDefaultMaxY(index: eYAxisT; value: ValueType);
   function GetDefaultMaxY(index: eYAxisT): ValueType;
  protected
  public
   constructor Create(AOwner: TComponent); override;
   destructor Destroy; override;
   function Execute: boolean;
   property AutoScaling[index: eYAxisT]: boolean read GetAutoScaling write SetAutoScaling;
   property BarStyle: eBarStyleT read FBarStyle write FBarStyle;
   property DefaultMinY[index: eYAxisT]: ValueType read GetDefaultMinY write SetDefaultMinY;
   property DefaultMaxY[index: eYAxisT]: ValueType read GetDefaultMaxY write SetDefaultMaxY;
  published
 end; //TmmGraphOptions

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

constructor TmmGraphOptions.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);
end; //constructor TmmGraphOptions.Create
//-----------------------------------------------------------------------------
destructor TmmGraphOptions.Destroy;
begin
 inherited Destroy;
end; //destructor TmmGraphOptions.Destroy
//-----------------------------------------------------------------------------
procedure TmmGraphOptions.SetAutoScaling(index: eYAxisT; value: boolean);
begin
 if value <> FAutoScaling[Index] then FAutoScaling[index] := value;
end; //procedure TmmGraphOptions.SetAutoScaling
//-----------------------------------------------------------------------------
function TmmGraphOptions.GetAutoScaling(index: eYAxisT): boolean;
begin
 result := FAutoScaling[index];
end; //function TmmGraphOptions.GetAutoScaling
//-----------------------------------------------------------------------------
procedure TmmGraphOptions.SetDefaultMinY(index: eYAxisT; value: ValueType);
begin
 if value <> FDefaultMinY[Index] then FDefaultMinY[index] := value;
end; //procedure TmmGraphOptions.SetDefaultMinY
//-----------------------------------------------------------------------------
function TmmGraphOptions.GetDefaultMinY(index: eYAxisT): ValueType;
begin
 result := FDefaultMinY[index];
end; //function TmmGraphOptions.GetDefaultMinY
//-----------------------------------------------------------------------------
procedure TmmGraphOptions.SetDefaultMaxY(index: eYAxisT; value: ValueType);
begin
 if value <> FDefaultMaxY[Index] then FDefaultMaxY[index] := value;
end; //procedure TmmGraphOptions.SetDefaultMaxY
//-----------------------------------------------------------------------------
function TmmGraphOptions.GetDefaultMaxY(index: eYAxisT): ValueType;
begin
 result := FDefaultMaxY[index];
end; //function TmmGraphOptions.GetDefaultMaxY
//-----------------------------------------------------------------------------
function TmmGraphOptions.Execute: boolean;
var
 xYAxis : eYAxisT;

begin
 mmGraphOptionsDlg := TmmGraphOptionsDlg.Create(Application);
 try
  mmGraphOptionsDlg.BarStyle := BarStyle;

  for xYAxis := yaxLeft to yaxRight do
  begin
   mmGraphOptionsDlg.AutoScaling[xYAxis] := AutoScaling[xYAxis];
   mmGraphOptionsDlg.DefaultMaxY[xYAxis] := DefaultMaxY[xYAxis];
   mmGraphOptionsDlg.DefaultMinY[xYAxis] := DefaultMinY[xYAxis];
  end; //for xYAxis := yaxLeft to yaxRight do

  result := mmGraphOptionsDlg.ShowModal = IDOk;
  if result then
  begin
   BarStyle := mmGraphOptionsDlg.BarStyle;

   for xYAxis := yaxLeft to yaxRight do
   begin
    AutoScaling[xYAxis] := mmGraphOptionsDlg.AutoScaling[xYAxis];
    DefaultMaxY[xYAxis] := mmGraphOptionsDlg.DefaultMaxY[xYAxis];
    DefaultMinY[xYAxis] := mmGraphOptionsDlg.DefaultMinY[xYAxis];
   end; //for xYAxis := yaxLeft to yaxRight do
  end; //if result then
 finally
  mmGraphOptionsDlg.Free;
 end; //try..finally
end; //function TmmGraphOptions.Execute

end. //mmGraphOption.pas
