(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphEditSerieDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: graph series editor  
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.04.99 | 0.01 | PW  |
|=========================================================================================*)

unit mmGraphEditSerieDlg;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,CheckLst,ComCtrls,ToolWin,ImgList,ActnList,
 mmGraphGlobal,mmColorDialog,mmActionList,mmImageList,mmCheckBox,
 mmRadioGroup,mmCheckListBox,mmEdit,mmGroupBox,mmTabControl,mmPanel,
 mmToolBar,IvDictio,IvMulti,IvEMulti,IvMlDlgs,NwCombo,NumCtrl,mmLabel,
 mmTranslator,mmButton,BASEFORM;

type
 TmmGraphEditSeriesDlg = class(TmmForm)
  ActionList1: TmmActionList;
   acOK: TAction;
   acCancel: TAction;
   acHelp: TAction;

  paButtons: TmmPanel;
   bOK: TmmButton;
   bCancel: TmmButton;
   bHelp: TmmButton;

  ColorDialog1: TmmColorDialog;
  mmTranslator1: TmmTranslator;

  Panel1: TmmPanel;
   GroupBox3: TmmGroupBox;
   gbStatistik: TmmGroupBox;
   clbStatData: TmmCheckListBox;
   rgYScaling: TmmRadioGroup;
   mmGroupBox3: TmmGroupBox;
   mmLabel4: TmmLabel;
   cbLowerLimit: TmmCheckBox;
   edLowerLimit: TNumEdit;
   rgChartType: TmmRadioGroup;
   gbColor: TmmGroupBox;
   paColor: TmmPanel;
   mmGroupBox2: TmmGroupBox;
   mmLabel3: TmmLabel;
   cbUpperLimit: TmmCheckBox;
   edUpperLimit: TNumEdit;
   gbVisible: TmmGroupBox;
   cbVisible: TmmCheckBox;
   lbSeries: TNWListBox;

  procedure acOKExecute(Sender: TObject);
  procedure acCancelExecute(Sender: TObject);
  procedure acHelpExecute(Sender: TObject);
  procedure paColorClick(Sender: TObject);
  procedure FormShow(Sender: TObject);
  procedure FormKeyPress(Sender: TObject; var Key: Char);
  procedure lbSeriesClick(Sender: TObject);
  procedure rgYScalingClick(Sender: TObject);
  procedure rgChartTypeClick(Sender: TObject);
  procedure cbVisibleClick(Sender: TObject);
  procedure clbStatDataClick(Sender: TObject);
  procedure cbLowerLimitClick(Sender: TObject);
  procedure cbUpperLimitClick(Sender: TObject);
  procedure edLowerLimitExit(Sender: TObject);
  private
   FNumSeries: integer;
   FSeriesColor: array[1..cMaxSeries] of TColor;
   FSeriesYScalingType: array[1..cMaxSeries] of eYScalingTypeT;
   FSeriesName: array[1..cMaxSeries] of string;
   FSeriesVisible: array[1..cMaxSeries] of boolean;
   FSeriesYAxis: array[1..cMaxSeries] of eYAxisT;
   FStatistikData: array[1..cMaxSeries] of sStatistikT;
   FSeriesShowUpperLimit: array[1..cMaxSeries] of boolean;
   FSeriesShowLowerLimit: array[1..cMaxSeries] of boolean;
   FSeriesUpperLimit: array[1..cMaxSeries] of ValueType;
   FSeriesLowerLimit: array[1..cMaxSeries] of ValueType;
   procedure SetSeriesUpperLimit(index: integer; value: ValueType);
   function GetSeriesUpperLimit(index: integer): ValueType;
   procedure SetSeriesLowerLimit(index: integer; value: ValueType);
   function GetSeriesLowerLimit(index: integer): ValueType;
   procedure SetSeriesShowUpperLimit(index: integer; value: boolean);
   function GetSeriesShowUpperLimit(index: integer): boolean;
   procedure SetSeriesShowLowerLimit(index: integer; value: boolean);
   function GetSeriesShowLowerLimit(index: integer): boolean;
   procedure SetSeriesName(index: integer; value: string);
   function GetSeriesName(index: integer): string;
   procedure SetSeriesYScalingType(index: integer; value: eYScalingTypeT);
   function GetSeriesYScalingType(index: integer): eYScalingTypeT;
   procedure SetSeriesYAxis(index: integer; value: eYAxisT);
   function GetSeriesYAxis(index: integer): eYAxisT;
   procedure SetSeriesColor(index: integer; value: TColor);
   function GetSeriesColor(index: integer): TColor;
   procedure SetSeriesVisible(index: integer; value: boolean);
   function GetSeriesVisible(index: integer): boolean;
   procedure SetStatistikData(index: integer; value: sStatistikT);
   function GetStatistikData(index: integer): sStatistikT;
   function GetCurrentSeries: integer;
  public
   property NumSeries: integer read FNumSeries write FNumSeries;
   property SeriesColor[index: integer]: TColor read GetSeriesColor write SetSeriesColor;
   property SeriesName[index: integer]: string read GetSeriesName write SetSeriesName;
   property SeriesVisible[index: integer]: boolean read GetSeriesVisible write SetSeriesVisible;
   property SeriesYAxis[index: integer]: eYAxisT read GetSeriesYAxis write SetSeriesYAxis;
   property StatistikData[index: integer]: sStatistikT read GetStatistikData write SetStatistikData;
   property SeriesYScalingType[index: integer]: eYScalingTypeT read GetSeriesYScalingType write SetSeriesYScalingType;
   property SeriesUpperLimit[index: integer]: ValueType read GetSeriesUpperLimit write SetSeriesUpperLimit;
   property SeriesLowerLimit[index: integer]: ValueType read GetSeriesLowerLimit write SetSeriesLowerLimit;
   property SeriesShowUpperLimit[index: integer]: boolean read GetSeriesShowUpperLimit write SetSeriesShowUpperLimit;
   property SeriesShowLowerLimit[index: integer]: boolean read GetSeriesShowLowerLimit write SetSeriesShowLowerLimit;
 end; //TmmGraphEditSeriesDlg

var
 mmGraphEditSeriesDlg: TmmGraphEditSeriesDlg;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

procedure TmmGraphEditSeriesDlg.SetSeriesColor(index: integer; value: TColor);
begin
 if value <> FSeriesColor[Index] then FSeriesColor[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesColor
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesColor(index: integer): TColor;
begin
 result := FSeriesColor[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesColor
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesName(index: integer; value: string);
begin
 if value <> FSeriesName[Index] then FSeriesName[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetName
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesName(index: integer): string;
begin
 result := FSeriesName[index];
end; //function TmmGraphEditSeriesDlg.GetName
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesVisible(index: integer; value: boolean);
begin
 if value <> FSeriesVisible[Index] then FSeriesVisible[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetVisible
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesVisible(index: integer): boolean;
begin
 result := FSeriesVisible[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesVisible
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesYAxis(index: integer; value: eYAxisT);
begin
 if value <> FSeriesYAxis[Index] then FSeriesYAxis[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesYAxis
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesYAxis(index: integer): eYAxisT;
begin
 result := FSeriesYAxis[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesYAxis
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetStatistikData(index: integer; value: sStatistikT);
begin
 if value <> FStatistikData[Index] then FStatistikData[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetStatistikData
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetStatistikData(index: integer): sStatistikT;
begin
 result := FStatistikData[index];
end; //function TmmGraphEditSeriesDlg.GetStatistikData
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesUpperLimit(index: integer; value: ValueType);
begin
 if value <> FSeriesUpperLimit[Index] then FSeriesUpperLimit[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesUpperLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesUpperLimit(index: integer): ValueType;
begin
 result := FSeriesUpperLimit[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesUpperLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesLowerLimit(index: integer; value: ValueType);
begin
 if value <> FSeriesLowerLimit[Index] then FSeriesLowerLimit[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesLowerLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesLowerLimit(index: integer): ValueType;
begin
 result := FSeriesLowerLimit[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesLowerLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesShowUpperLimit(index: integer; value: boolean);
begin
 if value <> FSeriesShowUpperLimit[Index] then FSeriesShowUpperLimit[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesShowUpperLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesShowUpperLimit(index: integer): boolean;
begin
 result := FSeriesShowUpperLimit[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesShowUpperLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesShowLowerLimit(index: integer; value: boolean);
begin
 if value <> FSeriesShowLowerLimit[Index] then FSeriesShowLowerLimit[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesShowLowerLimit
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesShowLowerLimit(index: integer): boolean;
begin
 result := FSeriesShowLowerLimit[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesShowLowerLimit
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.SetSeriesYScalingType(index: integer; value: eYScalingTypeT);
begin
 if value <> FSeriesYScalingType[Index] then FSeriesYScalingType[index] := value;
end; //procedure TmmGraphEditSeriesDlg.SetSeriesYScalingType
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetSeriesYScalingType(index: integer): eYScalingTypeT;
begin
 result := FSeriesYScalingType[index];
end; //function TmmGraphEditSeriesDlg.GetSeriesYScalingType
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.acOKExecute(Sender: TObject);
begin
 bOK.SetFocus; //forcing exit-event for numctrl's

 Close;
 ModalResult := mrOK;
end; //procedure TmmGraphEditSeriesDlg.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.acCancelExecute(Sender: TObject);
begin
 Close;
 ModalResult := mrCancel;
end; //procedure TmmGraphEditSeriesDlg.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.acHelpExecute(Sender: TObject);
begin
 //
end; //procedure TmmGraphEditSeriesDlg.acHelpExecute
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.paColorClick(Sender: TObject);
begin
 ColorDialog1.Color := paColor.Color;
 if ColorDialog1.Execute then
 begin
  paColor.Color := ColorDialog1.Color;
  SeriesColor[GetCurrentSeries] := paColor.Color;
 end; //if ColorDialog1.Execute then
end; //procedure TmmGraphEditSeriesDlg.paColorClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.FormShow(Sender: TObject);
var
 i : integer;

begin
 lbSeries.Clear;

 for i := 1 to NumSeries do
  lbSeries.AddObj(SeriesName[i],i);

 lbSeries.ItemIndex := 0;
 lbSeriesClick(Sender);
end; //procedure TmmGraphEditSeriesDlg.FormShow
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.FormKeyPress(Sender: TObject; var Key: Char);
begin
 if Key = #13 then
 begin
  Key := #0;
  PostMessage(Handle,WM_NEXTDLGCTL,0,0);
 end; //if Key = #13 then
end; //procedure TmmGraphEditSeriesDlg.FormKeyPress
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.lbSeriesClick(Sender: TObject);
var
 xSeries   : integer;
 eStatData : eStatistikT;

begin
 //set edit controls
 xSeries := GetCurrentSeries;
 rgYScaling.ItemIndex := ord(SeriesYScalingType[xSeries]);
 rgChartType.ItemIndex := ord(SeriesYAxis[xSeries]);
 paColor.Color := SeriesColor[xSeries];
 cbVisible.Checked := SeriesVisible[xSeries];
 for eStatData := stMin to stQ99 do
  clbStatData.Checked[ord(eStatData)] := eStatData in StatistikData[xSeries];
 cbLowerLimit.Checked := stLowerLimit in StatistikData[xSeries];
 cbUpperLimit.Checked := stUpperLimit in StatistikData[xSeries];
 edLowerLimit.Value := SeriesLowerLimit[xSeries];
 edUpperLimit.Value := SeriesUpperLimit[xSeries];
end; //procedure TmmGraphEditSeriesDlg.lbSeriesClick
//-----------------------------------------------------------------------------
function TmmGraphEditSeriesDlg.GetCurrentSeries: integer;
begin
 result := lbSeries.ItemIndex +1;
end; //function TmmGraphEditSeriesDlg.GetCurrentSeries
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.rgYScalingClick(Sender: TObject);
begin
 SeriesYScalingType[GetCurrentSeries] := eYScalingTypeT(rgYScaling.ItemIndex);
end; //procedure TmmGraphEditSeriesDlg.rgYScalingClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.rgChartTypeClick(Sender: TObject);
begin
 SeriesYAxis[GetCurrentSeries] := eYAxisT(rgChartType.ItemIndex);
end; //procedure TmmGraphEditSeriesDlg.rgChartTypeClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.cbVisibleClick(Sender: TObject);
begin
 SeriesVisible[GetCurrentSeries] := cbVisible.Checked;
end; //procedure TmmGraphEditSeriesDlg.cbVisibleClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.clbStatDataClick(Sender: TObject);
var
 eStatData : eStatistikT;
 sStatData : sStatistikT;

begin
 sStatData := StatistikData[GetCurrentSeries] -[stMin,stMax,stMean,stStdDev,stQ90,stQ95,stQ99];
 for eStatData := stMin to stQ99 do
  if clbStatData.Checked[ord(eStatData)] then include(sStatData,eStatData);
 StatistikData[GetCurrentSeries] := sStatData;
end; //procedure TmmGraphEditSeriesDlg.clbStatDataClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.cbLowerLimitClick(Sender: TObject);
begin
 exclude(FStatistikData[GetCurrentSeries],stLowerLimit);
 if cbLowerLimit.Checked then include(FStatistikData[GetCurrentSeries],stLowerLimit);
end; //procedure TmmGraphEditSeriesDlg.cbLowerLimitClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.cbUpperLimitClick(Sender: TObject);
begin
 exclude(FStatistikData[GetCurrentSeries],stUpperLimit);
 if cbUpperLimit.Checked then include(FStatistikData[GetCurrentSeries],stUpperLimit);
end; //procedure TmmGraphEditSeriesDlg.cbUpperLimitClick
//-----------------------------------------------------------------------------
procedure TmmGraphEditSeriesDlg.edLowerLimitExit(Sender: TObject);
begin
 SeriesLowerLimit[GetCurrentSeries] := edLowerLimit.Value;
 SeriesUpperLimit[GetCurrentSeries] := edUpperLimit.Value;
end; //procedure TmmGraphEditSeriesDlg.edLowerLimitExit

end. //mmGraphEditSerieDlg.pas
