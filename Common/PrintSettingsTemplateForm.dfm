inherited frmPrintSettings: TfrmPrintSettings
  Left = 465
  Top = 265
  BorderStyle = bsDialog
  BorderWidth = 3
  Caption = '(*)Drucker'
  ClientHeight = 272
  ClientWidth = 318
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mPrintSetup: TFramePrintSetup
    Left = 0
    Top = 0
    Width = 318
    Height = 130
    Align = alTop
    TabOrder = 1
    Caption = '(*)Drucker'
    PrintSetup.Copies = 0
    PrintSetup.FromPage = 0
    PrintSetup.MaxPage = 0
    PrintSetup.MinPage = 0
    PrintSetup.Options = []
    PrintSetup.PrintRange = prAllPages
    PrintSetup.ToPage = 0
  end
  object gbClearerSettings: TmmGroupBox
    Left = 0
    Top = 133
    Width = 318
    Height = 105
    Caption = '(20)Reinigereinstellungen'
    TabOrder = 2
    CaptionSpace = True
    object cbChannel: TmmCheckBox
      Left = 16
      Top = 20
      Width = 129
      Height = 17
      Caption = '(14)Kanal'
      Checked = True
      State = cbChecked
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbSplice: TmmCheckBox
      Tag = 1
      Left = 16
      Top = 40
      Width = 129
      Height = 17
      Caption = '(14)Spleiss'
      Checked = True
      State = cbChecked
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbYarnCount: TmmCheckBox
      Tag = 2
      Left = 16
      Top = 60
      Width = 129
      Height = 17
      Caption = '(11)Garnnummer'
      Checked = True
      State = cbChecked
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbCluster: TmmCheckBox
      Tag = 3
      Left = 168
      Top = 20
      Width = 129
      Height = 17
      Caption = '(15)Fehlerschwarm'
      Checked = True
      State = cbChecked
      TabOrder = 4
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbSFI: TmmCheckBox
      Tag = 4
      Left = 168
      Top = 40
      Width = 129
      Height = 17
      Caption = '(11)SFI'
      Checked = True
      State = cbChecked
      TabOrder = 5
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbFFCluster: TmmCheckBox
      Tag = 5
      Left = 168
      Top = 80
      Width = 129
      Height = 17
      Caption = '(11)FF-Cluster'
      Checked = True
      State = cbChecked
      TabOrder = 7
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbP: TmmCheckBox
      Left = 16
      Top = 80
      Width = 130
      Height = 17
      Caption = '(11)P Einstellungen'
      Checked = True
      State = cbChecked
      TabOrder = 3
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbVCV: TmmCheckBox
      Tag = 4
      Left = 168
      Top = 60
      Width = 129
      Height = 17
      Caption = '(11)VCV'
      Checked = True
      State = cbChecked
      TabOrder = 6
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object bCancel: TmmButton
    Left = 210
    Top = 245
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Cancel = True
    Caption = '(9)Abbrechen'
    ModalResult = 2
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bPrint: TmmButton
    Tag = 1
    Left = 105
    Top = 245
    Width = 97
    Height = 25
    Anchors = [akLeft, akBottom]
    Caption = '(10)Drucken'
    Default = True
    ModalResult = 1
    TabOrder = 3
    Visible = True
    OnClick = bPrintClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 120
    Top = 80
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
