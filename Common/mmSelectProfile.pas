(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmSelectProfile.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.99 | 0.01 | PW  | File created
|
|=========================================================================================*)

unit mmSelectProfile;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MemoINI;

type
  TmmSelectProfile = class(TComponent)
  private
    FMemoINI: TMemoINI;
    FSelection: tSelectProfileRecordT;
  protected
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
    function Execute: boolean;
  published
    property Selection: tSelectProfileRecordT read FSelection write FSelection;
  end;                                  //TmmSelectProfile

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmSelectProfileDlg;

constructor TmmSelectProfile.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
end;                                    //constructor TmmSelectProfile.Create
//-----------------------------------------------------------------------------
destructor TmmSelectProfile.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TmmSelectProfile.Destroy
//-----------------------------------------------------------------------------
function TmmSelectProfile.Execute: boolean;
begin
  if Owner is TWinControl then
    mmSelectProfileEditor := TmmSelectProfileEditor.Create(Owner, FMemoINI)
  else
    mmSelectProfileEditor := TmmSelectProfileEditor.Create(Application, FMemoINI);
  try
    result := mmSelectProfileEditor.ShowModal = IDOk;
    if result then
      Selection := mmSelectProfileEditor.GetSelectedProfile;
  finally
    mmSelectProfileEditor.Free;
  end;                                  //try..finally
end;                                    //function TmmSelectProfile.Execute

end. //mmSelectProfile.pas

