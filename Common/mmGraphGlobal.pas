(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmGraphGlobal.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Globale Variablem,Konstanten,Typen fuer Grafik-Komponente
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.02.99 | 0.01 | PW  | Datei erstellt
| 02.03.99 | 0.02 | PW  | Aenderungen lt. Besprechung am 26.2.99
| 07.05.99 | 0.03 | PW  | YScaling-Factor for each series
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
| 00.00.99 | 0.00 | PW  |
| 03.06.00 |      | sdo | Farbpalette (Defaultfarben) fuer Berichts-Grafik -> Felder
                          cMaxSeries auf 70 gesetzt
|=========================================================================================*)

unit mmGraphGlobal;
interface
uses
 Windows,Messages,SysUtils,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
 ExtCtrls,Buttons,Menus,Math;

const
 cEpsilon           = 1e-12;
 //cMaxSeries         = 15;    // ?? SDO
 cMaxSeries         = 70;      // SDO
 cMinDecimals       = 0;
 cMaxDecimals       = 9;
 cMinSteps          = 5;  //6;
 cMaxSteps          = 12; //15;

 cSecsPerMin        = 60;
 cMinsPerHour       = 60;
 cHoursPerDay       = 24;
 cMinsPerDay        = cMinsPerHour *cHoursPerDay;

 cMinute1           = 1 /cMinsPerDay;
 cMinute5           = 5 /cMinsPerDay;
 cMinute30          = 30 /cMinsPerDay;
 cHour              = 1/cHoursPerDay;

 clLightGray1       = $00E0E0E0;
 clLightGray2       = $00E5E5E5;
 clLightGray3       = $00D4D4D4;

type
 s20                = string[20];
 s30                = string[30];
 s50                = string[50];

 XLabelType         = string[20];
 ValueType          = single;          //performance: extended=100% double=113% single=361%

 eYScalingTypeT     = (ysNone,ys2,ys5,ys10,ys50,ys100,ys500,ys1000,ys1_2,ys1_10,ys1_20,ys1_100,ys1_200,ys1_1000,ys1_2000);


 eBarStyleT         = (bsSide,bsStacked,bsStacked100);
 eFindTypeT         = (ftExact,ftPrior,ftNext);
 eXAxisTypeT        = (xaFixed,xaFloat,xaDateTime);
 eSortOrderT        = (soNone,soAscending,soDescending);
 eYAlignmentT       = (tyaTop,tyaCenter,tyaBottom);
 eYAxisT            = (yaxLeft,yaxRight);  //left = lines, right = bars
 eHorzGridT         = (hgNone,hgLeftAxis,hgRightAxis,hgLeftRightAxis);
 eScrollBtnT        = (sbInc,sbDec);
 eLegendPosT        = (lpTop,lpRight);
 eDateticksT        = (dtMinute,dtMinute5,dtMinute30,
                       dtHour,dtHour3,dtHour6,dtHour12,
                       dtDay,
                       dtWeek,
                       dtMonth,dtMonth2,dtMonth3,
                       dtYear,dtYear10,dtYear100);
 eDblClickT         = (dcChart,dcLegend,dcTitle,dcLeftAxis,dcRightAxis,dcXAxis,dcNone);
 eGraphItemT        = (giRectangle,giLine);
 eTitleOrientationT = (toUp,toDown);
 eSelectionStyleT   = (ssInverted,ssGray,ssFixed);

 eSizeRefreshT      = (srHeight,srWidth);
 sSizeRefreshT      = set of eSizeRefreshT;

 eStatistikT        = (stMin,stMax,stMean,stStdDev,stQ90,stQ95,stQ99,stLowerLimit,stUpperLimit,stCount,stCV,stVariance,stData);
 sStatistikT        = set of stMin..stUpperLimit;
 ePrnUnitsT         = (puMM,puPix);
 eChartTypeT        = (ctLine,ctBar);


 tYPointT = record
  Value  : ValueType;
  IsNull : boolean;
 end; //tYPoint

 tXPointT = record
  Value : ValueType;
  Name  : XLabelType;
 end; //tXPoint

 tXYValueT = record
  YValue : array[0..cMaxSeries] of tYPointT;
  XValue : tXPointT;
 end; //tXValueT

 tDataRecordT = record
  Values: array of tXYValueT;
  Statistik: array[0..cMaxSeries,eStatistikT] of ValueType;
 end; //tDataArray

 tYScaleRecordT = record
  Min,
  Max,
  Step  : array[eYAxisT] of extended;
  Ticks : array[eYAxisT] of integer;
 end; //tYScaleRecordT

 tXScaleRecordT = record
  absMin,absMax,absDX,
  actMin,actMax,actDX,
  Step                : extended;
 end; //tXScaleRecordT

 tGraphItemT = record
  ItemType  : eGraphItemT;
  Itemrect  : TRect;
  DataRow,
  DataPoint : integer;
  StatType  : eStatistikT;
 end; //tGraphItemT

 tPrinterInfoT = record
  xMM,
  yMM,
  xPix,
  yPix,
  xPixInch,
  yPixInch,
  xPhysPix,
  yPhysPix,
  xOffset,
  yOffset    : longint;
  xPixMM,
  yPixMM     : double;
  prnXPos,
  prnYPos,
  prnWidth,
  prnHeight  : array[ePrnUnitsT] of integer;
 end; //tPrinterInfoT


 TDefaultColorRec = record
   Color   : TColor;
   Fieldname : String;
 end;

 const
 cYScalingStr : array[eYScalingTypeT] of string[10] = ('1x','2x','5x','10x','50x','100x','500x','1000x','1/2x','1/10x','1/20x','1/100x','1/200x','1/1000x','1/2000x');

//------------------------------------------------------------------------------
 // Default Feldfarben
 cDefaultColor : Array[0..58] of TDefaultColorRec = (
     // Offlimit
   ( Color     : 16744576;
      Fieldname : 'calc_OfflineRatio';   ),
 {1} ( Color     :  8421376;
      Fieldname : 'c_offline_offlimit_time';   ),
    ( Color     :  12615680;
      Fieldname : 'c_YB_offline_time';   ),
    ( Color     :  8421631;
      Fieldname : 'c_Eff_offline_time';   ),
    ( Color     :  16711680;
     Fieldname : 'c_CSp_offline_time';   ),
    ( Color     :  32896;
      Fieldname : 'c_RSp_offline_time';   ),
    ( Color     :  12615935;
     Fieldname : 'c_CBu_offline_time';   ),
    ( Color     :  65535;
     Fieldname : 'c_CUpY_offline_time';   ),
    ( Color     :  16744576;
     Fieldname : 'c_tLSt_offline_time';   ),
    ( Color     :  8453888;
     Fieldname : 'c_LSt_offline_time';   ),
 {10} ( Color     :  128;
     Fieldname : 'c_CSIRO_offline_time';   ),
   ( Color     :  16776960;
     Fieldname : 'c_CYTot_offline_time';   ),

   // Machine
   ( Color     : 16711680;
     Fieldname : 'calc_npProdGrp';   ),
   ( Color     :  65535;
     Fieldname : 'calc_MEff';   ),
   ( Color     :  65280;
     Fieldname : 'calc_PEff';   ),
   ( Color     :  255;
     Fieldname : 'c_Len';   ),
   ( Color     :  128;
     Fieldname : 'c_Wei';   ),
   ( Color     :  33023;
     Fieldname : 'c_Bob';   ),
   ( Color     :  8421376;
     Fieldname : 'c_Cones';   ),
   ( Color     :  16744576;
     Fieldname : 'calc_CYTOT';   ),
 {20}  ( Color     :  16711808;
     Fieldname : 'calc_Sp';   ),
   ( Color     :  16744703;
     Fieldname : 'calc_YB';   ),
   ( Color     :  14802077;
     Fieldname : 'calc_CN';   ),
   ( Color     : 32896 ;
     Fieldname : 'calc_CS';   ),
   ( Color     : 4210688;
     Fieldname : 'calc_CL';   ),
   ( Color     :  12615808;
     Fieldname : 'calc_CT';   ),
   ( Color     :  13412915;
     Fieldname : 'calc_CSIRO';   ),
   ( Color     :  13251949;
     Fieldname : 'calc_COFFCNT';   ),
   ( Color     :  1424106;
     Fieldname : 'calc_CCL';   ),
   ( Color     :  5530027;
     Fieldname : 'c_LSt';   ),
 {30}  ( Color     :  10132324;
     Fieldname : 'c_LStProd';   ),
   ( Color     : 16777143;
     Fieldname : 'calc_CUPY';   ),
   ( Color     :  13372404;
     Fieldname : 'calc_CBU';   ),
   ( Color     : 5532130 ;
     Fieldname : 'calc_CSP';   ),
   ( Color     : 15456472 ;
     Fieldname : 'calc_CSYS';   ),
   ( Color     :  16766333;
     Fieldname : 'calc_RSP';   ),
   ( Color     : 6619057;
     Fieldname : 'calc_PCSp';   ),
   ( Color     : 16512 ;
     Fieldname : 'calc_PRSp';   ),
   ( Color     : 10976513 ;
     Fieldname : 'c_LckSys';   ),
   ( Color     : 16711680 ;
     Fieldname : 'c_LckSIRO';   ),
 {40} ( Color     : 8684799;
     Fieldname : 'calc_ManSt';   ),
   ( Color     : 9623533 ;
     Fieldname : 'calc_UCLS';   ),
   ( Color     : 2023734 ;
     Fieldname : 'calc_UCLL';   ),
   ( Color     : 2872763 ;
     Fieldname : 'calc_UCLT';   ),
   ( Color     : 15513591 ;
     Fieldname : 'calc_CDBU';   ),
   ( Color     : 16745665 ;
     Fieldname : 'c_tRed';   ),
   ( Color     : 13676240 ;
     Fieldname : 'c_tYell';   ),
   ( Color     : 261291 ;
     Fieldname : 'c_tManSt';   ),
   ( Color     : 14079702;
     Fieldname : 'calc_INeps';   ),
   ( Color     : 16777041 ;
     Fieldname : 'calc_SFI';   ),
  {50} ( Color     : 1451435 ;
     Fieldname : 'calc_CSFI';   ),
   ( Color     : 16570115 ;
     Fieldname : 'calc_LckCl';   ),
   ( Color     : 183;
     Fieldname : 'calc_IThick';   ),
   ( Color     : 7783934 ;
     Fieldname : 'calc_IThin';   ),
   ( Color     : 204 ;
     Fieldname : 'calc_ISmall';   ),
   ( Color     : 12711922;
     Fieldname : 'calc_I2_4';   ),
   ( Color     : 293070 ;
     Fieldname : 'calc_I4_8';   ),
   ( Color     : 15573757 ;
     Fieldname : 'calc_I8_20';   ),
   ( Color     : 14411488 ;
     Fieldname : 'calc_I20_70';   )
 );
//------------------------------------------------------------------------------






//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

begin

end. //mmGraphGlobal.pas
