(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmSaveProfileDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Parameter-Auswahl als Profil speichern
| Info..........: -
| Develop.system: Windows NT 4.0 SP 4
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.04.99 | 0.00 | PW  | Datei erstellt
| 04.10.2002        LOK | Umbau ADO
|=========================================================================================*)

unit mmSaveProfileDlg;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs, StdCtrls,
  Buttons, ExtCtrls, DB, ComCtrls, ToolWin, BASEFORM,
  mmToolBar, IvDictio, IvMulti, IvEMulti, ActnList, mmActionList, MemoINI,
  ImgList, mmImageList, mmButton, mmPanel, mmTranslator, mmComboBox, mmGroupBox;

type
  TmmSaveProfileEditor = class(TmmForm)
    ActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    acHelp: TAction;

    mmTranslator1: TmmTranslator;

    MainPanel: TmmPanel;
    gbProfilename: TmmGroupBox;
    comProfile: TmmComboBox;

    paButtons: TmmPanel;
    bOK: TmmButton;
    bCancel: TmmButton;
    bHelp: TmmButton;

    procedure FormShow(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure comProfileChange(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
  private
    FMemoINI: TMemoINI;
    procedure Init;
    procedure EnableActions;
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
    function GetSelectedProfile: tSelectProfileRecordT;
  end;                                  //TmmSaveProfileEditor

var
  mmSaveProfileEditor: TmmSaveProfileEditor;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmCommonLib, mmLib;

{$R *.DFM}

constructor TmmSaveProfileEditor.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
end;                                    //constructor TmmSaveProfileEditor.Create
//-----------------------------------------------------------------------------
destructor TmmSaveProfileEditor.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TmmSaveProfileEditor.Destroy
//-----------------------------------------------------------------------------
function TmmSaveProfileEditor.GetSelectedProfile: tSelectProfileRecordT;
begin
  result.Profilename := trim(uppercase(comProfile.Text));
end;                                    //function TmmSaveProfileEditor.GetSelectedProfile
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.FormShow(Sender: TObject);
begin
  Init;
end;                                    //procedure TmmSaveProfileEditor.FormShow
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.acOKExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end;                                    //procedure TmmSaveProfileEditor.acOKExecute
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.acCancelExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrCancel;
end;                                    //procedure TmmSaveProfileEditor.acCancelExecute
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.Init;
var
  i: integer;
  xProfile,
    xUser: string;
  xList: TStringList;

begin
  xList := TStringList.Create;
  xList.AddStrings(FMemoINI.GetFormProfiles);
  try
    for i := 0 to xList.Count - 1 do begin
      xProfile := GetSubStr(xList[i], cListDelimiterChar, 1);
      xUser := GetSubStr(xList[i], cListDelimiterChar, 2);
      if FMemoINI.Username = xUser then
        comProfile.Items.Add(xProfile);
    end;                                //for i := 0 to FProfiles.Count -1 do
  finally
    xList.Free;
  end;                                  //xList

  comProfile.Text := FMemoINI.Profilename;
  EnableActions;
end;                                    //procedure TmmSaveProfileEditor.Init
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.EnableActions;
begin
  acOK.Enabled := trim(comProfile.Text) <> '';
end;                                    //procedure TmmSaveProfileEditor.EnableActions
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.comProfileChange(Sender: TObject);
begin
  inherited;
  EnableActions;
end;                                    //procedure TmmSaveProfileEditor.comProfileChange
//-----------------------------------------------------------------------------
procedure TmmSaveProfileEditor.acHelpExecute(Sender: TObject);
begin
  inherited;
 //
end;                                    //procedure TmmSaveProfileEditor.acHelpExecute

end. //mmSaveProfileDlg.pas

