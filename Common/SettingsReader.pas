{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MillMasterSource.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 05.09.2002  2.00  LOK | File recreated (war in mmSetupModule.pas)
| 13.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 04.11.2002        LOK | Settingsreader neu als Singelton
| 05.11.2002        Wss | mDatabase wird nach dem Lesen der DB-Werte gel�scht, um
                          keine Unn�tigen Resourcen zu verbrauchen
| 29.01.2003        Wss | Anpassungen an die neuen Packages Easy,Standard,Pro
| 19.02.2003        Wss | Property DBVersion und Funktion DBVersionOK hinzugef�gt
                          �ber parameter aShowMsg kann definiert werden, ob eine
                          Meldung ausgegeben wird.
| 14.03.2003        Wss | Funktion HasNetTyp hinzugef�gt
| 22.05.2003        Wss | Beim Auslesen der Handlerprozesse wird nun erst auf Value gepr�ft
                          ob NumOfProcesses existiert.
| 11.06.2003        Lok | TMMStettingsreader.Init() der Init wird mehrfach probiert
| 03.03.2004        Nue | NetHandler nicht mehr ab Registry sondern ab DB lesen.
| 28.05.2004        Wss | cCompanyName hinzugef�gt
| 31.08.2004        SDo | Neue Konstante: cMaxHandlers -> Max. Anzahl Handlers (MMConfiguration.exe) ; Reg. \MMGuard\Process1..ProcessX
| 25.02.2005        Wss | In InstalledHandlers: der Ausgelesende Wert in UpperCase umwandeln
| 27.04.2005        SDo | Anpassung an XML (TLengthWindowMode): DefaultValue Wert = '2' im cMMLenghtMode; Alt = '7'
| 28.08.2006        SDo | DefaultSpindleReportTimeRange ( cDefaultSpindleReportTimeRange ) auch auf dem Client einstellbar
|                       | ->  ClientServer      : csClientServer; // Server & Client
| 13.11.2006        SDo | Neuer Text in Const.Array cMMSetupDefaults : siehe cDomainNames -> In Reg. DomainNames sind jetzt alle WKS MM-PC oder der MM-Host eingetragen
|                       | siehe cDomainNames -> In Reg. DomainController ist der Computer mit den MM-Benutzer eingetragen
| 13.12.2006        Nue | Korrektur der neu eingef�gten Texte!!!!!!!
|                       |



===============================================================================

    *********************************************
    ! ! ! ! ! !     A C H T U N G     ! ! ! ! ! !

    Die Datei 'SettingsReader' nur mit Modelmaker
    bearbeiten. Das Modell befindet sich im
    Unterverzeichnis '\ModelMaker'.

    *********************************************}
unit SettingsReader;

interface

uses
  Windows, SysUtils, Classes, AdoDBAccess, LoepfeGlobal,
  IPCClass, IPCUnit, BaseGlobal, MMEventLog, syncobjs;

type
  TComponentType = (ctNone, ctEdit, ctSpinEdit, ctLabel, ctComboBox,
    ctCheckBox, ctMemo, ctRadioGroup);

  TClientServerType = (csServer, csClient, csClientServer, csNone, csServerEdit);
  //csServerEdit = nur auf Server editierbar, auf Client sichtbar

  TLocation = (lReg, lDB, lIni);

  // aus MMDef.pas
  TDataType = (dtNone, dtArray, dtChar, dtByte, dtShort, dtWord, dtLong, dtDWord,
    dtFloat, dtDouble, dtString, dtDate, dtTime, dtDateTime,
    dtOLEDateTime, dtKey, dtInteger, dtBoolean, dtSaveComboItemID);
  // dtSaveComboItemID gehoert nicht gerade in TDataType,
  // aber es muss leider sein.


  TMMSetupRec = record
    UniqueValueName: String;
    Location: TLocation;
    LocationName: String;
    LocationKey: DWord;
    ValueName: String;
    UserValue: String;
    NativeValue: String;
    DefaultValue: String;
    MinValue: String;
    MaxValue: String;
    Factor: Real;
    Mask: String;
    ValueType: TDataType;
    ValueChange: Boolean;
    ComponentType: TComponentType;
    Group: String;
    Restart: Boolean;
    ClientServer: TClientServerType;
    ValueText: String;
    HintText: String;
    Description: String;
    Data: String;
    ReadOnlyData: Boolean;
    Printable: Boolean // Darf gedruckt werden, wenn TRUE;;
  end;
  
  pSetupRec = ^TMMSetupRec;


const
  cMinDBVersionMsg = 'Application can not start because of wrong database update state (%s)';
  // PartieIndex Texte
  cNoInput          = '(*)Keine Eingabe'; //ivlm
  cArtName_Id       = '(*)Artikelname und ID'; //ivlm
  cArtName_DateTime = '(*)Artikelname und Datum'; //ivlm
  cArtName          = '(*)Artikelname'; //ivlm
  cUserInput        = '(*)Individueller Name'; //ivlm

  cProcess          = 'Process';
  cMaxHandlers      = '7'; //Max. MM-Handlers

  // registry path
  cRegMMSecurityPath    = cRegMillMasterPath + '\Security';
  cRegMMGuardPath       = cRegMillMasterPath + '\MMGuard';
  cRegMMCommonPath      = cRegMillMasterPath + '\Common';

  cCUpTimePeriod     = 'RegCUpTimePeriod';
  cIntervalLen       = 'RegIntervalLen';
  cMaxInterval       = 'RegMaxInterval';
  cDaysForDelShift   = 'RegDaysForDelShift';
  cErrResetTime      = 'RegErrResetTime';
  cRetrayTime        = 'RegRetrayTime';
  cStartMMIm         = 'RegStartMMIm';
  cUserGroup         = 'RegUserGroup';
  cNumOfProcess      = 'RegNumOfProcess';
  cAlertTime         = 'RegAlertTime';
  cAlertSound        = 'RegAlertSound';
  cRebootSQLServer   = 'RegRebootSQLServer';
  cHelpFilePath      = 'HelpFilePath';

  cDomainNames       = 'RegDomainNames';
  cMMHost            = 'RegMMHost';
  cDomainController  = 'RegDomainController';

  cYarnCntUnit         = 'SQLYarnCntUnit';
  cPilotSpindles       = 'SQLPilotSpindles';
  cYMLenWindow         = 'SQLYMLenWindow';
  cDefaultPartieIndex  = 'SQLDefaultPartieIndexSet';
  cDefaultPartieName   = 'SQLDefaultPartieNameSet';
  cCorrectionFactorImp = 'SQLCorrectionFactorImp';
  cMinStatisticLength  = 'SQLStatisticLenght';   // QOfflimit -> Minimale Laenge zur Mittelwertberechnung
  cGraphicShiftdepth   = 'SQLGraphicShiftdepth'; // QOfflimit -> Graphic
  cStatisticLengthUnit = 'SQLStatisticLengthUnit';
  cQMatrixZeroLimit    = 'SQLQMatrixZeroLimit';  // Unterdrueckungswert fuer QMatrix
  cOnlyTemplateSetsAvailable = 'SQLTemplateSetsAvailable';  // VorlagenVerwaltung -> nicht mit Style
  cStartDayOfWeek      = 'StartDayOfWeek';            // definiert den Wochenbegin
  cDefaultSpindleReportTimeRange = 'DefaultSpindleReportTimeRange'; // Stunden, welche als Default beim aufstarten im Spindlereport selektiert werden
  cCompanyName         = 'SQLCompanyName';

  // MillMasterPlugin
  cIntervalTimeRange   = 'IntervalTimeRange';
  cFloorTrendSelection = 'FloorTrendSelection';
  cOfflimitTime        = 'OfflimitTime';      // wird nicht vom MMConfig verwaltet
  cCustShiftFrom       = 'CustShiftFrom';     // wird nicht vom MMConfig verwaltet
  cCustShiftTo         = 'CustShiftTo';       // wird nicht vom MMConfig verwaltet
  cCustCalID           = 'CustCalID';         // wird nicht vom MMConfig verwaltet
  cCustShiftDetail     = 'CustShiftDetail';   // wird nicht vom MMConfig verwaltet
  cShowSystemQO        = 'ShowSystemQO';      // wird nicht vom MMConfig verwaltet
  cShowStyleColor      = 'ShowStyleColor';    // wird nicht vom MMConfig verwaltet
  cShowEmptyProdGroup  = 'ShowEmptyProdGroup';
  cDefaultShiftCalID   = 'DefaultShiftCalID'; // standard Schichtkalender fuer Berichte und MMAlerts

  cMMUnit            = 'SQLMMUnits';      // absolute, 100km, 1000km, 100'000yrd, 1'000'000yrd
  cMMLenghtMode      = 'SQLMMLenghtMode'; // Erste, Letzte, ...

  cSQLComputers               =  '(*)Name des Computers, auf welchem die MillMaster SQL Datenbank installiert ist.'; //ivlm
//  cMMDomainNamesDescription_a =  '(*)MillMaster Host: Namen aller Computer (Host und / oder Clients), welche auf die MillMaster SQL Datenbank zugreifen. Namen sind mit Komma zu trennen.'; //ivlm
//  cMMDomainNamesDescription_b =  '(*)Beispiel: MMServer1, MMClient1, MMClient2'; //ivlm
//  cMMDomainNamesDescription_c =  '(*)MillMaster Client: '; //ivlm
//  cMMDomainNamesDescription_d =  '(*)Beispiel: MMServer1'; //ivlm

//'(*)MM-Host: Namen aller Computer, welche auf die MillMaster SQL Datenbank zugreifen. (Kommagetrennt).'+cCRLF+'MM-Client: Name des Computers, auf welchem die MillMaster SQL Datenbank installiert ist.; //ivlm
//  cMMDomainNamesDescription_b =  '(*)Beispiel: MMServer1, MMClient1, MMClient2'; //ivlm
//  cMMDomainNamesDescription_c =  '(*)MillMaster Client: '; //ivlm
//  cMMDomainNamesDescription_d =  '(*)Beispiel: MMServer1'; //ivlm
  cEND               = 'END';


//------------------------------------------------------------------------------
 cMMSetupDefaults : Array[0..37] of TMMSetupRec = (
// 0) CleanUpEV Admin
   (UniqueValueName  : cCUpTimePeriod ;
    Location         : lReg;     // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath; // RegistryPath
    LocationKey      : cRegLM;   // RootKey
    ValueName        : 'cUpTimePeriod'; // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1800000'; // ms   (30 Min.)
    MinValue         : '1800000'; // ms  (30 Min.)
    MaxValue         : '86400000';// ms  (24 h)
    Factor           : 60000;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'Jobs';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Min.';  //ivlm
    HintText         : '(*)CleanUp Event'; //ivlm
    Description      : '(*)Loescht Daten aus dem Datenpool, die aelter sind als die eingestellte Zeit.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE;
    Printable        : FALSE),

// 1) Interval lenght
   (UniqueValueName  : cIntervalLen ;
    Location         : lReg;     // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath; // RegistryPath
    LocationKey      : cRegLM; // RootKey
    ValueName        : 'IntervalLen'; // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '60';
    MinValue         : '10';
    MaxValue         : '70';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer     : csServer;  // Server only
    ValueText        : '(*)Min.';  //ivlm
    HintText         : '(*)DelInt Event'; //ivlm
    Description      : '(*)Laenge der Intervalle' ; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE;
    Printable        : FALSE),

// 2) DelIntAdmin
   (UniqueValueName  : cMaxInterval ;
    Location         : lReg;     // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath; // RegistryPath
    LocationKey      : cRegLM; // RootKey
    ValueName        : 'MaxInterval'; // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '30' ;
    MinValue         : '1';
    MaxValue         : '50';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Anzahl Intervalle';  //ivlm
    HintText         : '(*)Anzahl Intervalle'; //ivlm
    Description      : '(*)Loescht die aeltesten Intervalle und deren abhaengige Daten aus den Tabellen.' ; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 3) DelShiftByTime Admin
//  (Beachte: Wird auch von DB ausgefuehrt -> 60 Tg., wenn kein LabMaster)
   (UniqueValueName  : cDaysForDelShift ;
    Location         : lReg;     // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath; // RegistryPath
    LocationKey      : cRegLM; // RootKey
    ValueName        : 'DaysForDelShift'; // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '400' ; // max. Tage
    MinValue         : '62';
    MaxValue         : '400';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'Jobs';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Anzahl Tage';  //ivlm
    HintText         : '(*)DelShiftByTime Event'; //ivlm
    Description      : '(*)Loescht Daten aus Tabellen, die aelter sind als die eingestellten Tage.' ; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 4) ErrResetTime
   (UniqueValueName  : cErrResetTime ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'ErrResetTime';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '3900'; // sec.   -> 65Min
    MinValue         : '600';  // sec. -> 10Min
    MaxValue         : '36000';// sec.  -> 600Min.
    Factor           : 60;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer     : csServer;  // Server only
    ValueText        : '(*)Min.';  //ivlm
    HintText         : '(*)Fehlerreset Zeit';  //ivlm
    Description      : '(*)Nach dieser Zeit vergisst das System die vorangegangenen Restarts.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 5) RetrayTime
   (UniqueValueName  : cRetrayTime;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'RetrayTime';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '120' ; // Min
    MinValue         : '10';   // Min
    MaxValue         : '1440';  // Min
    Factor           : 1;
    Mask             : '';
    ValueType        : dtWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Min.'; //ivlm
    HintText         : '(*)Erneuter Startversuch bei Alarm'; //ivlm
    Description      : '(*)MillMaster wird nach der eingegebenen Zeit automatisch aus dem Alarmzustand neu aufgestartet.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 6) StartMMIm
   (UniqueValueName  : 'RegStartMMIm' ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'StartMMIm';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtBoolean;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)MillMaster Autostart (ja/nein)'; //ivlm
    HintText         : '(*)Automatischer Start des MillMasters beim Aufstarten des Servers'; //ivlm
    Description      : '(*)Automatischer Start des MillMasters beim Aufstarten des Servers.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 7) UserGroup
    (UniqueValueName  : cUserGroup ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'UserGroup';  // KeyNames
    UserValue        : 'MMUserGroup' ;
    NativeValue      : '' ;
    DefaultValue     : 'MMUserGroup' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Basis Benutzergruppe'; //ivlm
    HintText         : '(*)Benutzergruppe mit niedrigstem Zugriffsschutz, der alle MM Benutzer angehoeren'; //ivlm
    Description      : '(*)Benutzergruppe der alle MillMasterbenutzer angehoeren.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 8) NumOfProcess
    (UniqueValueName  : cNumOfProcess ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'NumOfProcesses';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '5' ;
    MinValue         : '5';
    MaxValue         : cMaxHandlers;
    Factor           : 1;
    Mask             : '';
    ValueType        : dtWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Anzahl der Prozesse'; //ivlm
    HintText         : '(*)Anzahl der Prozesse'; //ivlm
    Description      : '(*)Anzahl der Prozesse.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 9) AlertTime
    (UniqueValueName  : cAlertTime ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'AlertTime';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '60' ;
    MinValue         : '10';
    MaxValue         : '600';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Sek.'; //ivlm
    HintText         : '(*)Intervall des Alarmes falls das MillMaster nicht gestartet werden kann.'; //ivlm
    Description      : '(*)Intervall des Alarmes falls das MillMaster nicht gestartet werden kann.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 10) AlertSound
    (UniqueValueName  : cAlertSound ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'AlertSound';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1' ;
    MinValue         : '0';
    MaxValue         : '1';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtBoolean;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Alarmklang (ja/nein)'; //ivlm
    HintText         : '(*)Soll ein Akustisches Signal ausgegeben werden, wenn das MillMaster nicht gestartet werden kann?'; //ivlm
    Description      : '(*)Soll ein Akustisches Signal ausgegeben werden, wenn das MillMaster nicht gestartet werden kann?'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 11) RebootSQLServer
    (UniqueValueName : cRebootSQLServer ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMGuardPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'RebootSQLServer';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '0' ;
    MinValue         : '0';
    MaxValue         : '1';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtBoolean;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox;
    Group            : 'MMGuard';
    Restart          : TRUE;
    ClientServer      : csServer; // Server only
    ValueText        : '(*)Neustart von SQL-Server'; //ivlm
    HintText         : '(*)Soll der SQL-Server beim Neustart des MillMasters ebenfalls neu gestartet werden?'; //ivlm
    Description      : '(*)Soll der SQL-Server beim Neustart des MillMasters ebenfalls neu gestartet werden?'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 12) DomainNames
    (UniqueValueName : cDomainNames;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'DomainNames';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '.';
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer     : csClientServer; // Client & Server only
    ValueText        : '(*)Namen aller MillMaster Computer'; //ivlm
    HintText         : '(*)Namen aller MillMaster Computer'; //ivlm

    Description      :  '(*)MM-Host: Namen aller Computer, welche auf die MillMaster SQL Datenbank zugreifen. (Kommagetrennt).      MM-Client: Name des Computers, auf welchem die MillMaster SQL Datenbank installiert ist.'; //ivlm
// Geht hier leider nicht mit zusammengef�gten Strings!!!! Nue: 13.12.06
//                        cSQLComputers + cCRLF +
//                        Translate('(*)MillMaster Host: Namen aller Computer (Host und / oder Clients), welche auf die MillMaster SQL Datenbank zugreifen. Namen sind mit Komma zu trennen.') //ivlm
//                       +'(*)Beispiel: MMServer1, MMClient1, MMClient2'; //ivlm
//                       + cCRLF + cCRLF +
//                       '(*)MillMaster Client: ' //ivlm
//                       + cSQLComputers + cCRLF +
//                       '(*)Beispiel: MMServer1'; //ivlm
//    Description      : cMMDomainNamesDescription_a + cCRLF + cMMDomainNamesDescription_b + cCRLF + cCRLF
//                       + cMMDomainNamesDescription_c + cSQLComputers + cCRLF + cMMDomainNamesDescription_d; // Neuer Text 13.11.2006
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 13) // MMHost   org. > ServerName
    (UniqueValueName : cMMHost ;
    Location         : lReg;            // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath ; // RegistryPath    cRegMillMasterPath
    LocationKey      : cRegLM;          // RootKey
    ValueName        : 'MMHost';  // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '.' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer      : csClientServer; // Server & Client
    ValueText        : '(*)MillMaster Host'; //ivlm
    HintText         : '(*)MillMaster Host'; //ivlm
    Description      : cSQLComputers;
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 14) YarnCntUnit
    (UniqueValueName : cYarnCntUnit;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'YarnCntUnit';   // Wert fuer ColName  Appkey
    UserValue        : '1' ;
    NativeValue      : '' ;
    DefaultValue     : '1' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCombobox; // ctNone;
    Group            : 'Winding Settings';
    Restart          : FALSE;
    ClientServer     : csServerEdit;  // Server only
    ValueText        : '(*)Einheit Garnnummer'; //ivlm
    HintText         : '(*)Einheit Garnnummer'; //ivlm
    Description      : '(*)Einheit Garnnummer'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 15) PilotSpindles
    (UniqueValueName : cPilotSpindles;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'PilotSpindles'; // Wert fuer ColName Appkey
    UserValue        : '4' ;
    NativeValue      : '' ;
    DefaultValue     : '4' ;
    MinValue         : '1';
    MaxValue         : '80';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit; // ctNone;
    Group            : 'Winding Settings';
    Restart          : TRUE;
    ClientServer     : csServerEdit;  // Server only
    ValueText        : '(*)Anzahl Pilot Spulstellen'; //ivlm
    HintText         : '(*)Anzahl Pilot Spulstellen'; //ivlm
    Description      : '(*)Anzahl Pilot Spulstellen'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 16) YMLenWindow
    (UniqueValueName : cYMLenWindow;
    Location         : lDB;           // Ort der Daten -> DB
    LocationName     : 't_MMUParm';   // Table
    LocationKey      : 0;             // RootKey
    ValueName        : 'YMLenWindow'; // Wert fuer ColName  Appkey
    UserValue        : '1000' ;
    NativeValue      : '' ;
    DefaultValue     : '1000' ;
    MinValue         : '100';
    MaxValue         : '1000';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCombobox; // ctNone;
    Group            : 'Winding Settings';
    Restart          : FALSE;
    ClientServer     : csServerEdit;  // Server only
    ValueText        : '(*)Laengenfenster fuer YarnMaster'; //ivlm
    HintText         : '(*)Laengenfenster fuer YarnMaster'; //ivlm
    Description      : '(*)Laengenfenster fuer YarnMaster'; //ivlm
    Data             : '"10",'  +     // ivlm
                       '"100",' +     // ivlm
                       '"1000"'  ;    // ivlm
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 17) Interval Time Range in Floor's actual view
    (UniqueValueName : cIntervalTimeRange;
    Location         : lReg;          // from Registry
    LocationName     : cRegMMFloor;   // Table
    LocationKey      : cRegLM;        // RootKey
    ValueName        : cIntervalTimeRange;        // ColName
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '8';
    MinValue         : '1';
    MaxValue         : '20';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtByte;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit; // ctNone;
    Group            : 'MillMasterPlugin';
    Restart          : False;
    ClientServer     : csClientServer;
    ValueText        : '(*)Zeitbereich fuer aktuelle Ansicht'; //ivlm
    HintText         : '(*)Zeitbereich fuer aktuelle Ansicht'; //ivlm
    Description      : '(*)Zeitbereich fuer aktuelle Ansicht'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 18) Floor Trend Count
    (UniqueValueName : cFloorTrendSelection;
    Location         : lReg;          // from Registry
    LocationName     : cRegMMFloor;   // Table
    LocationKey      : cRegLM;        // RootKey
    ValueName        : cFloorTrendSelection;  // ColName
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '9';  // Intervall
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtSaveComboItemID;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctComboBox; // ctNone;
    Group            : 'MillMasterPlugin';
    Restart          : False;
    ClientServer     : csClientServer;  // Server only
    ValueText        : '(*)Default Zeitselektion fuer Trenddaten'; //ivlm
    HintText         : '(*)Default Zeitselektion fuer Trenddaten'; //ivlm
    Description      : '(*)Default Zeitselektion fuer Trenddaten'; //ivlm
    Data             : '"(*)Aktuelle Schicht",' +     // ivlm
                       '"(*)Letzte Schicht",' +       // ivlm
                       '"(*)Letzten 3 Schichten",' +  // ivlm
                       '"(*)Letzter Tag",' +          // ivlm
                       '"(*)Letzte 7 Tagen",'+        // ivlm
                       '"(*)Letzte Woche",' +         // ivlm
                       '"(*)Aktueller Monat",' +      // ivlm
                       '"(*)Letzter Monat",' +        // ivlm
                       '"(*)Kundenschichtbereich",' + // ivlm
                       '"(*)Intervalldaten"';         // ivlm
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 19) MillMaster Pfad fuer Helpdatei
    (UniqueValueName : cHelpFilePath;
    Location         : lReg;          // from Registry
    LocationName     : cRegMMCommonPath;
    LocationKey      : cRegLM;        // RootKey
    ValueName        : cHelpFilePath;  // ColName
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '';
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'Winding Settings';
    Restart          : False;
    ClientServer     : csClientServer;  // Server only
    ValueText        : '(*)MillMaster Pfad fuer Helpdatei'; //ivlm
    HintText         : '(*)MillMaster Pfad fuer Helpdatei'; //ivlm
    Description      : '(*)MillMaster Pfad fuer Helpdatei'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 20) Offlimit time filter
    (UniqueValueName : cOfflimitTime;
    Location         : lReg;          // from Registry
    LocationName     : cRegMMFloor;   // Table
    LocationKey      : cRegLM;        // RootKey
    ValueName        : cOfflimitTime; // ColName
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '180';
    MinValue         : '0';
    MaxValue         : '99999';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtLong;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit; // ctNone;
    Group            : 'MillMasterPlugin';
    Restart          : False;
    ClientServer     : csClientServer;
    ValueText        : '(*)Anzeigeunterdrueckung fuer Offlimit'; //ivlm
    HintText         : '(*)Anzeigeunterdrueckung fuer Offlimit'; //ivlm
    Description      : '(*)Anzeigeunterdrueckung fuer Offlimit'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 21) MMUnit
    (UniqueValueName : cMMUnit;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'MMUnits';   // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '2' ; // pro 1000 km
    MinValue         : '0';
    MaxValue         : '4';
{
    MinValue         : '0';
    MaxValue         : '4';
{}
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCombobox; // ctNone;
    Group            : 'Allgemein';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Laengeneinheit'; //ivlm
    HintText         : '(*)Laengeneinheit fuer den Bericht'; //ivlm
    Description      : '(*)Laengeneinheit fuer den Bericht'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 22) MMLenghtMode
    (UniqueValueName : cMMLenghtMode;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'MMLenghtMode';   // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '2'; // Last -> siehe XMLDef TLengthWindowMode
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCombobox; // ctNone;
    Group            : 'Allgemein';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Laengenmodus'; //ivlm
    HintText         : '(*)Laengenmodus'; //ivlm
    Description      : '(*)Laengenmodus'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 23) ShowEmptyProductionGroups in Floor
    (UniqueValueName : cShowEmptyProdGroup;
    Location         : lReg;             // Ort der Daten -> DB
    LocationName     : cRegMMFloor;     // Table
    LocationKey      : cRegLM;               // RootKey
    ValueName        : cShowEmptyProdGroup;   // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1' ; // Last
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtBoolean;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox; // ctNone;
    Group            : 'Allgemein';
    Restart          : TRUE;
    ClientServer     : csClientServer;
    ValueText        : '(40)Leere Produktionsgruppen anzeigen'; //ivlm
    HintText         : '(40)Leere Produktionsgruppen anzeigen'; //ivlm
    Description      : '(40)Leere Produktionsgruppen anzeigen'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 24) // Domain Controller
    (UniqueValueName : cDomainController;
    Location         : lReg;                 // Ort der Daten -> Registry
    LocationName     : cRegMMCommonPath ;    // RegistryPath
    LocationKey      : cRegLM;               // RootKey
    ValueName        : cRegDomainController; // KeyNames
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '.' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer     : csClientServer; // Server & Client
    ValueText        : '(*)MillMaster User Host'; //ivlm  //'(*)MM Primary Domain Controller'; //ivlm
    HintText         : '(*)Name des Computers, auf welchem die MillMaster-Benutzer und -Gruppen installiert sind.'; //ivlm  //'(*)MM Primary Domain Controller'; //ivlm
    Description      : '(*)Name des Computers, auf welchem die MillMaster-Benutzer und -Gruppen installiert sind.'; //ivlm  //'(*)MM Primary Domain Controller'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 25) ArtikelPartieKey
   (UniqueValueName  : cDefaultPartieIndex ;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'DefaultSetPartieIndex';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '2' ; // ItemIndex
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctRadioGroup; // ctNone;
    Group            : 'Style';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Definition des Standard-Partienamens'; //ivlm
    HintText         : '(*)Definition des Standard-Partienamens'; //ivlm
    Description      : '(*)Definition des Standard-Partienamens, welcher beim Start einer neuen Partie angezeigt werden soll.'; //ivlm
    Data             : '"' + cNoInput + '","' +
                             cArtName_Id+ '","' +
                             cArtName_DateTime + '","' +
                             cArtName + '","' +
                             cUserInput + '"';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 26) ArtikelPartieKey
   (UniqueValueName  : cDefaultPartieName ;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'DefaultSetPartieName';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit; // ctNone;
    Group            : 'Style';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Definition des Standard-Partienamens'; //ivlm
    HintText         : '(*)Definition des Standard-Partienamens'; //ivlm
    Description      : '(*)Definition des Standard-Partienamens'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 27 ArtikelPartieKey
   (UniqueValueName  : cCorrectionFactorImp;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'CorrectionFactorImp';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '0' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox; // ctNone;
    Group            : 'Style';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Korrekturfaktor fuer die Imperfektion zulassen.'; //ivlm
    HintText         : '(*)Korrekturfaktor fuer die Imperfektion zulassen.'; //ivlm
    Description      : '(*)Korrekturfaktor fuer die Imperfektion zulassen.'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 28) MinStatisticLength  // Offlimit
   (UniqueValueName  : cMinStatisticLength ;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'QOfflimitMinProdLength';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '100000' ; //m
    MinValue         : '1';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit; // ctNone;
    Group            : 'QOfflimit';
    Restart          : FALSE;
    ClientServer     : csClientServer; // Server & Client
    ValueText        : '(*)Minimale produzierte Laenge pro Schicht, ab welcher die Daten in den Mittelwert einbezogen werden.'; //ivlm
    HintText         : '(*)Minimale produzierte Laenge pro Schicht, ab welcher die Daten in den Mittelwert einbezogen werden.'; //ivlm
    Description      : '(*)Minimale produzierte Laenge pro Schicht, ab welcher die Daten in den Mittelwert einbezogen werden.'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 29) Graphic Shiftdepth for QOfflimit
   (UniqueValueName : cGraphicShiftDepth;
    Location         : lDB;          // from Registry
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;        // RootKey
    ValueName        : 'QOfflimitShiftDepth'; // ColName
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '5';
    MinValue         : '5';
    MaxValue         : '20';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit; // ctNone;
    Group            : 'QOfflimit';
    Restart          : False;
    ClientServer     : csClientServer;
    ValueText        : '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraeftige QOfflimitgrafik benoetigt werden.'; //ivlm
    HintText         : '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraeftige QOfflimitgrafik benoetigt werden.'; //ivlm
    Description      : '(*)Anzahl vergangener Schichten, welche fuer eine aussagekraeftige QOfflimitgrafik benoetigt werden.'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),


// 30) StatisticLengthUnit
   (UniqueValueName  : cStatisticLengthUnit;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'StatisticLengthUnit';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '0' ; // ItemIndex
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctRadioGroup; // ctNone;
    Group            : 'QOfflimit';
    Restart          : FALSE;
    ClientServer     : csClientServer;
    ValueText        : '(*)Laengeneinheit'; //ivlm
    HintText         : '(*)Laengeneinheit fuer die QOfflimit Mittelwertbildung.'; //ivlm
    Description      : '(*)Laengeneinheit fuer die QOfflimit Mittelwertbildung.'; //ivlm
    Data             : '"m","yds"';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 31) DefaultShiftCalID
   (UniqueValueName  : cDefaultShiftCalID;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : cDefaultShiftCalID;  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1' ; // ItemIndex
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctComboBox; // ctNone;
    Group            : 'System';
    Restart          : False;
    ClientServer     : csClientServer;
    ValueText        : '(*)Standard Schichtkalender'; //ivlm
    HintText         : '(*)Standard Schichtkalender fuer Berichte und MMAlarm.'; //ivlm
    Description      : '(*)Standard Schichtkalender fuer Berichte und MMAlarm.'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

// 31) QMatrixZeroLimit
   (UniqueValueName  : cQMatrixZeroLimit;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : cQMatrixZeroLimit;  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '0.01' ; // ItemIndex
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtFloat;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit; // ctNone;
    Group            : 'System';
    Restart          : False;
    ClientServer     : csClientServer;
    ValueText        : '(*)Unterdrueckungswert fuer Klassierung'; //ivlm
    HintText         : '(*)Unterdrueckungswert fuer Klassierung'; //ivlm
    Description      : '(*)Unterdrueckungswert fuer Klassierung'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),
 //32)  SQLTemplateSetsAvailable
   (UniqueValueName  : cOnlyTemplateSetsAvailable ;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : 'OnlyTemplateSetsAvailable';  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '0' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtBoolean;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctCheckBox;
    Group            : 'Style';
    Restart          : TRUE;
    ClientServer      : csClientServer;  // Server only
    ValueText        : '(*)Vorlagen nur ab Vorlagenverwaltung moeglich'; //ivlm
    HintText         : '(*)Vorlagen nur ab Vorlagenverwaltung moeglich'; //ivlm
    Description      : '(*)Vorlagen nur ab Vorlagenverwaltung moeglich'; //ivlm
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),
 //33)  Start day of week
   (UniqueValueName  : cStartDayOfWeek;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : cStartDayOfWeek;  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '1';  // default start is Monday (ISO: Monday = 2 -> inc one before use)
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtSaveComboItemID;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctComboBox;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Die Woche startet mit diesem Tag.'; //ivlm
    HintText         : '(*)Die Woche startet mit diesem Tag.'; //ivlm
    Description      : '(*)Die Woche startet mit diesem Tag.'; //ivlm
    Data             : '(15)Sonntag' + ',' + // ivlm
                       '(15)Montag' + ',' + // ivlm
                       '(15)Dienstag' + ',' + // ivlm
                       '(15)Mittwoch' + ',' + // ivlm
                       '(15)Donnerstag' + ',' + // ivlm
                       '(15)Freitag' + ',' + // ivlm
                       '(15)Samstag'; // ivlm
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),
 //34)  Default time range for spindle report
   (UniqueValueName  : cDefaultSpindleReportTimeRange;
    Location         : lDB;             // Ort der Daten -> DB
    LocationName     : 't_MMUParm';     // Table
    LocationKey      : 0;               // RootKey
    ValueName        : cDefaultSpindleReportTimeRange;  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '8';  // default start is Monday (ISO: Monday = 2 -> inc one before use)
    MinValue         : '1';
    MaxValue         : '30';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtWord;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctSpinEdit;
    Group            : 'System';
    Restart          : TRUE;
    ClientServer     : csClientServer; // Server & Client     csServer;  // Server only
    ValueText        : '(*)Zeitvorgabe fuer Spindelbericht.'; //ivlm
    HintText         : '(*)Zeitvorgabe fuer Spindelbericht.'; //ivlm
    Description      : '(*)Zeitvorgabe fuer Spindelbericht.'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

//35) Company name used in QuickReports
   (UniqueValueName  : cCompanyName;
    Location         : lDB;           // Ort der Daten -> DB
    LocationName     : 't_MMUParm';   // Table
    LocationKey      : 0;             // RootKey
    ValueName        : cCompanyName;  // Wert fuer ColName  Appkey
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '';  // default start is Monday (ISO: Monday = 2 -> inc one before use)
    MinValue         : '';
    MaxValue         : '';
    Factor           : 1;
    Mask             : '';
    ValueType        : dtString;
    ValueChange      : FALSE;  // Eingabewert hat geaendert. (fuer Speicherung)
    ComponentType    : ctEdit;
    Group            : 'System';
    Restart          : False;
    ClientServer      : csServer;  // Server only
    ValueText        : '(*)Firmenname'; //ivlm
    HintText         : '(*)Firmenname'; //ivlm
    Description      : '(*)Firmenname'; //ivlm
    Data             : '';
    ReadOnlyData     : FALSE ;
    Printable        : FALSE),

    //--------------------------------------------------------------------------
    // +++ Last item +++
    //--------------------------------------------------------------------------
   (UniqueValueName  : cEND ;
    Location         : lReg;
    LocationName     : '';
    LocationKey      : 0;
    ValueName        : 'END';
    UserValue        : '' ;
    NativeValue      : '' ;
    DefaultValue     : '' ;
    MinValue         : '';
    MaxValue         : '';
    Factor           : 0;
    Mask             : '';
    ValueType        : dtDWord;
    ValueChange      : FALSE;
    ComponentType    : ctNone;
    Group            : 'END';
    Restart          : FALSE;
    ClientServer      : csNone;
    ValueText        : '';
    HintText         : '';
    Description      : '';
    Data             : '' ;
    ReadOnlyData     : FALSE ;
    Printable        : FALSE)
   );


type
  // MM Installation
//  TMMPackage = (mmpWindingMaster, mmpLabMaster, mmpDispoMaster, mmpClearerAssistant);
//  TMMComponent = (mmcQOfflimit, mmcArticle, mmcDatawarehouse, mmcLongTerm, mmcClearerAssistant, mmcLabReport);
  TMMPackage = (mmpEasy, mmpStandard, mmpPro);
  TMMComponent = (mmcQOfflimit, mmcStyle, mmcLongTerm, mmcClearerAssistant, mmcLabReport);

  TMMPackageSet = set of TMMPackage;
  TMMComponentSet = set of TMMComponent;

  TMMInstallsRec = record
    Package: TMMPackage;
    Components: TMMComponentSet;
  end;
  
  TPartieIndex = (piNoneInput, piArtNameId, piArtNameDate, piArtName, piUserInput);

  TMMSettings = class(TObject)
  private
    FApplName: String;
    FComponentsAsText: String;
    FDBVersion: String;
    FInitialized: Boolean;
    FIsComponentClearerAssistant: Boolean;
    FIsComponentLabReport: Boolean;
    FIsComponentLongterm: Boolean;
    FIsComponentQOfflimit: Boolean;
    FIsComponentStyle: Boolean;
    FIsPackageEasy: Boolean;
    FIsPackagePro: Boolean;
    FIsPackageStandard: Boolean;
    FPackagesAsText: String;
    mInstalledHandlers: set of TNetTyp;
    function GetOnlyTemplateSetsAvailable: Boolean;
  protected
    FErrorTxt: String;
    procedure FetchDBVersion;
    procedure FetchInstalledComponents;
    procedure FetchInstalledHandlers;
    procedure FetchInstalledPackages;
    function GetClientServer(aId: Word): Variant;
    function GetComponentType(aId: Word): Variant;
    function GetData(aId: Word): Variant;
    function GetDBValue(aId: Word): Variant;
    function GetDefaultValue(aId: Word): Variant;
    function GetDescription(aId: Word): Variant;
    function GetFactor(aId: Word): Variant;
    function GetGroup(aId: Word): Variant;
    function GetHintText(aId: Word): Variant;
    function GetLocation(aId: Word): Variant;
    function GetLocationKey(aId: Word): Variant;
    function GetLocationName(aId: Word): Variant;
    function GetMask(aId: Word): Variant;
    function GetMaxValue(aId: Word): Variant;
    function GetMinValue(aId: Word): Variant;
    function GetNativeValue(aId: Word): Variant;
    function GetNumberOfValues: Word;
    function GetPrintable(aId: Word): Boolean;
    function GetReadOnlyData(aId: Word): Variant;
    function GetRestart(aId: Word): Variant;
    function GetUniqueValueName(aId: Word): Variant;
    function GetUserValue(aId: Word): Variant;
    function GetUserValueByName(aUniqueName: string): Variant;
    function GetValueChange(aId: Word): Variant;
    function GetValueName(aId: Word): Variant;
    function GetValueText(aId: Word): Variant;
    function GetValueType(aId: Word): Variant;
    procedure SetData(aId: Word; const Value: Variant);
    procedure SetDBValue(aId: Word; const Value: Variant);
    procedure SetGroup(aId: Word; const Value: Variant);
    procedure SetHintText(aId: Word; const Value: Variant);
    procedure SetNativeValue(aId: Word; const Value: Variant);
    procedure SetPrintable(aId: Word; const Value: Boolean);
    procedure SetUserValue(aId: Word; const Value: Variant);
    procedure SetValueChange(aId: Word; const Value: Variant);
    procedure SetValueText(aId: Word; const Value: Variant);
  public
    mDataBase: TAdoDBAccess;
    constructor Create; virtual;
    destructor Destroy; override;
    function GetID(aRegUniqueName: string): Integer;
    function HasNetTyp(aNet: TNetTyp): Boolean;
    function Init(aFreeDB: Boolean = False): Boolean;
    procedure ReadDBValues;
    procedure ReadRegistryValues;
    property ApplicationName: String read FApplName write FApplName;
    property ClientServer[aId: Word]: Variant read GetClientServer;
    property ComponentsAsText: String read FComponentsAsText;
    property ComponentType[aId: Word]: Variant read GetComponentType;
    property Data[aId: Word]: Variant read GetData write SetData;
    property DBValue[aId: Word]: Variant read GetDBValue write SetDBValue;
    property DBVersion: String read FDBVersion;
    property DefaultValue[aId: Word]: Variant read GetDefaultValue;
    property Description[aId: Word]: Variant read GetDescription;
    property ErrorTxt: String read FErrorTxt;
    property Factor[aId: Word]: Variant read GetFactor;
    property Group[aId: Word]: Variant read GetGroup write SetGroup;
    property HintText[aId: Word]: Variant read GetHintText write SetHintText;
    property Initialized: Boolean read FInitialized;
    property IsComponentClearerAssistant: Boolean read FIsComponentClearerAssistant default False;
    property IsComponentLabReport: Boolean read FIsComponentLabReport default False;
    property IsComponentLongterm: Boolean read FIsComponentLongterm default False;
    property IsComponentQOfflimit: Boolean read FIsComponentQOfflimit default False;
    property IsComponentStyle: Boolean read FIsComponentStyle default False;
    property IsOnlyTemplateSetsAvailable: Boolean read GetOnlyTemplateSetsAvailable;
    property IsPackageEasy: Boolean read FIsPackageEasy default False;
    property IsPackagePro: Boolean read FIsPackagePro default False;
    property IsPackageStandard: Boolean read FIsPackageStandard default False;
    property Location[aId: Word]: Variant read GetLocation;
    property LocationKey[aId: Word]: Variant read GetLocationKey;
    property LocationName[aId: Word]: Variant read GetLocationName;
    property Mask[aId: Word]: Variant read GetMask;
    property MaxValue[aId: Word]: Variant read GetMaxValue;
    property MinValue[aId: Word]: Variant read GetMinValue;
    property NativeValue[aId: Word]: Variant read GetNativeValue write SetNativeValue;
    property NumberOfValues: Word read GetNumberOfValues;
    property PackagesAsText: String read FPackagesAsText;
    property Printable[aId: Word]: Boolean read GetPrintable write SetPrintable;
    property ReadOnlyData[aId: Word]: Variant read GetReadOnlyData;
    property Restart[aId: Word]: Variant read GetRestart;
    property UniqueValueName[aId: Word]: Variant read GetUniqueValueName;
    property UserValue[aId: Word]: Variant read GetUserValue write SetUserValue;
    property UserValueByName[aUniqueName: string]: Variant read GetUserValueByName;
    property ValueChange[aId: Word]: Variant read GetValueChange write SetValueChange;
    property ValueName[aId: Word]: Variant read GetValueName;
    property ValueText[aId: Word]: Variant read GetValueText write SetValueText;
    property ValueType[aId: Word]: Variant read GetValueType;
  end;
  
  TMMSettingsReader = class(TObject)
  private
    FError: TErrorRec;
    FLastRequestedValue: String;
    mCriticalSection: TCriticalSection;
    mSettings: TMMSettings;
    function GetComponentClearerAssistant: Boolean;
    function GetComponentLabReport: Boolean;
    function GetComponentLongTerm: Boolean;
    function GetComponentsAsText: String;
    function GetDBVersion: String;
    function GetIsComponentQOfflimit: Boolean;
    function GetIsComponentStyle: Boolean;
    function GetIsPackageEasy: Boolean;
    function GetIsPackagePro: Boolean;
    function GetIsPackageStandard: Boolean;
    function GetOnlyTemplateSetsAvailable: Boolean;
    function GetPackagesAsText: String;
    function GetUserValue(aUniqueName: string): Variant;
  protected
    constructor CreateInstance;
    class function AccessInstance(Request: Integer): TMMSettingsReader;
    function GetValue(aUniqueName: string): Variant;
  public
    constructor Create;
    destructor Destroy; override;
    function DBVersionOK(aMinDBVersion: String; aShowMsg: Boolean): Boolean;
    function HasNetTyp(aNet: TNetTyp): Boolean;
    function Init: Boolean;
    class function Instance: TMMSettingsReader;
    class procedure ReleaseInstance;
    procedure _Create; virtual;
    procedure _Destroy; virtual;
    property ComponentsAsText: String read GetComponentsAsText;
    property DBVersion: String read GetDBVersion;
    property Error: TErrorRec read FError;
    property IsComponentClearerAssistant: Boolean read GetComponentClearerAssistant;
    property IsComponentLabReport: Boolean read GetComponentLabReport;
    property IsComponentLongterm: Boolean read GetComponentLongTerm;
    property IsComponentQOfflimit: Boolean read GetIsComponentQOfflimit;
    property IsComponentStyle: Boolean read GetIsComponentStyle default False;
    property IsOnlyTemplateSetsAvailable: Boolean read GetOnlyTemplateSetsAvailable;
    property IsPackageEasy: Boolean read GetIsPackageEasy default False;
    property IsPackagePro: Boolean read GetIsPackagePro default False;
    property IsPackageStandard: Boolean read GetIsPackageStandard default False;
    property LastRequestedValue: String read FLastRequestedValue;
    property PackagesAsText: String read GetPackagesAsText;
    property Value[aUniqueName: string]: Variant read GetUserValue;
  end;
  
const
  cQryUpdate = 'Update t_MMUParm set Data = ''%s'' ' +
    'where AppName = ''MMConfiguration'' ' +
    'and Appkey = ''%s'' ';
  cQryInsert = 'Insert into t_MMUParm (AppName, Appkey, Data) ' +
    'Values( ''MMConfiguration'', ''%s'', ''%s'' )';

  // Key fuer Kodierung
  cKey = $D5; //5284565;
//    cKey = $50A2D5;

    //MMPackages als Text
  cMMPackEasy     = 'Easy';
  cMMPackStandard = 'Standard';
  cMMPackPro      = 'Pro';

  cMMCompClearerAssistant = 'ClearerAssistant';
  cMMCompLabReport        = 'LabReport';
  cMMCompLongterm         = 'Longterm';
  cMMCompQOfflimit        = 'QOfflimit';
  cMMCompStyle            = 'Style';

 cMMPackages: array[TMMPackage] of string = (
    cMMPackEasy,
    cMMPackStandard,
    cMMPackPro
   );

  //MMComponents als Text
  cMMComponents: array[TMMComponent] of string = (
    cMMCompQOfflimit,
    cMMCompStyle,
    cMMCompLongterm,
    cMMCompClearerAssistant,
    cMMCompLabReport
    );

  //MMPackage Zusammenstellungh
//  TMMComponent = (mmcQOfflimit, mmcStyle, mmcLongTerm, mmcClearerAssistant, mmcLabReport);
  cMMInstalls: array[TMMPackage] of TMMComponentSet = (
    // mmpEasy
    [],
    // mmpStandard
    [mmcStyle, mmcClearerAssistant],
    // mmpPro
    [mmcQOfflimit, mmcStyle, mmcLongTerm, mmcClearerAssistant, mmcLabReport]
    );

  cUserPartieKeyType: array[TPartieIndex] of string = (
    cNoInput,
    cArtName_Id,
    cArtName_DateTime,
    cArtName,
    cUserInput
    );

  //cSuperKey = 12345;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,

  mmRegistry,
  Dialogs, Forms, IvDictio;

const
  cApp = 'MMConfiguration';
  cProgTerminate = 'Program will terminate.'; //
  //  cProgTerminate = 'The User has no database right. Program will terminate.'; //

//------------------------------------------------------------------------------
//:-------------------------------------------------------------------
constructor TMMSettings.Create;
var
  xErrorTxt: String;
begin
  fDBVersion   := '0.00.00';
  fInitialized := False;
  mInstalledHandlers := [];
  try
    mDataBase := TAdoDBAccess.Create(1, True);
  except
    on E: Exception do begin
      xErrorTxt := 'Error in TAdoDBAccess. ' + E.Message + '  ' + IntToStr(mDataBase.DBError);
      mDataBase.Free;
      raise exception.Create(xErrorTxt);
    end
  end;
end;// TMMSettings.Create cat:No category

//:-------------------------------------------------------------------
destructor TMMSettings.Destroy;
begin
  if mDataBase <> nil then mDataBase.free;
  inherited Destroy;
end;// TMMSettings.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.FetchDBVersion;
begin
  with mDataBase.Query[0] do
  try
    Close;
    SQL.text := 'Select Data from t_MMUParm where Appkey = ''ActualVersionUpdate'' and AppName = ''MM_WindingDB''';
    Open;
    if FindFirst then begin
      fDBVersion := FieldByName('Data').asString;
      CodeSite.SendString('FetchDBVersion', fDBVersion);
    end;
  except
    on e:Exception do
      CodeSite.SendError('FetchDBVersion failed: ' + e.Message);
  end;
end;// TMMSettings.FetchDBVersion cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.FetchInstalledComponents;
var
  xDBList: TStringList;
begin
  FComponentsAsText            := '';
  FIsComponentClearerAssistant := False;
  FIsComponentLabReport        := False;
  FIsComponentLongTerm         := False;
  FIsComponentQOfflimit        := False;
  FIsComponentStyle            := False;
  
  //  FIsComponentArticle := False;
  //  FIsComponentDataWareHouse := False;
  
    // vorhandene Components auf DB ermitteln
  xDBList := TStringList.Create;
  with mDataBase.Query[0] do
  try
    Close;
    SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMComponents'' and AppName = ''MMConfiguration''';
    Open;
    //  if not EOF then begin  // ADO Conform
    if FindFirst then begin  // ADO Conform
      FComponentsAsText := FieldByName('Data').asString;
      xDBList.CommaText  := FComponentsAsText;
  
      FIsComponentClearerAssistant := (xDBList.IndexOf(cMMCompClearerAssistant) <> -1);
      FIsComponentLabReport        := (xDBList.IndexOf(cMMCompLabReport) <> -1);
      FIsComponentLongTerm         := (xDBList.IndexOf(cMMCompLongterm) <> -1);
      FIsComponentQOfflimit        := (xDBList.IndexOf(cMMCompQOfflimit) <> -1);
      FIsComponentStyle            := (xDBList.IndexOf(cMMCompStyle) <> -1);
    end;
  finally
    xDBList.Free;
  end;
end;// TMMSettings.FetchInstalledComponents cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.FetchInstalledHandlers;
var
  xStr: String;
  xNetTyp: TNetTyp;
begin
  mInstalledHandlers := [];
  // vorhandene Components auf DB ermitteln
  with mDataBase.Query[0] do try
    Close;
    SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMInstalledHandlers'' and AppName = ''MMConfiguration''';
    Open;
    if FindFirst then begin  // ADO Conform
      xStr := UpperCase(FieldByName('Data').asString);
      for xNetTyp := ntTXN to ntCI do
        // if yes ...
        if Pos(cApplicationNames[cNetHandlerNames[xNetTyp]], xStr) > 0 then begin
          // ... include the ID to the set
          include(mInstalledHandlers, xNetTyp);
        end;
    end; //if FindFirst
  except
    on e: Exception do
      CodeSite.SendError('FetchInstalledHandlers Query error: ' + e.Message);
  end;
end;// TMMSettings.FetchInstalledHandlers cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.FetchInstalledPackages;
var
  xDBList: TStringList;
begin
  FIsPackageEasy     := False;
  FIsPackageStandard := False;
  FIsPackagePro      := False;
  FPackagesAsText    := '';
  
  // vorhandene Packages auf DB ermitteln
  xDBList := TStringList.Create;
  with mDataBase.Query[0] do
  try
    Close;
    SQL.text := 'Select Data from t_MMUParm where Appkey = ''MMPackages'' and AppName = ''MMConfiguration''';
    Open;
    //  if not EOF then begin  // ADO Conform
    if FindFirst then begin  // ADO Conform
      FPackagesAsText   := FieldByName('Data').asString;
      xDBList.CommaText := FPackagesAsText;
  
      FIsPackageEasy     := (xDBList.IndexOf(cMMPackEasy) <> -1);
      FIsPackageStandard := (xDBList.IndexOf(cMMPackStandard) <> -1);
      FIsPackagePro      := (xDBList.IndexOf(cMMPackPro) <> -1);
    end;
  finally
    xDBList.Free;
  end;
end;// TMMSettings.FetchInstalledPackages cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetClientServer(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ClientServer;
end;// TMMSettings.GetClientServer cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetComponentType(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ComponentType;
end;// TMMSettings.GetComponentType cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetData(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Data;
end;// TMMSettings.GetData cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetDBValue(aId: Word): Variant;
var
  xTable, xColumn: String;
begin
  xTable := cMMSetupDefaults[aId].LocationName;
  xColumn := cMMSetupDefaults[aId].ValueName;
  
    // set DefaultValue to return parameter in case value on database is not available
  Result := cMMSetupDefaults[aId].DefaultValue;
  with mDataBase.Query[0] do try
    Close;
    if ApplicationName <> '' then
      SQl.Text := Format('Select Data from %s where Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xColumn, ApplicationName])
    else
      SQl.Text := Format('Select Data from %s where Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xColumn, cApp]);
    Open;
    if not EOF then   // ADO Conform
      Result := FieldByName('Data').AsString
  except
    on e: Exception do
      CodeSite.SendError('Query error: ' + e.Message);
  end;
end;// TMMSettings.GetDBValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetDefaultValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].DefaultValue;
end;// TMMSettings.GetDefaultValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetDescription(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Description;
end;// TMMSettings.GetDescription cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetFactor(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Factor;
end;// TMMSettings.GetFactor cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetGroup(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Group;
end;// TMMSettings.GetGroup cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetHintText(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].HintText;
end;// TMMSettings.GetHintText cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetID(aRegUniqueName: string): Integer;
var
  x: Integer;
begin
  Result := -1;
  for x := 0 to GetNumberOfValues - 1 do
    if cMMSetupDefaults[x].UniqueValueName = aRegUniqueName then begin
      Result := x;
      break;
    end;
end;// TMMSettings.GetID cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetLocation(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Location;
end;// TMMSettings.GetLocation cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetLocationKey(aId: Word): Variant;
begin
  Result := Variant(cMMSetupDefaults[aId].LocationKey);
end;// TMMSettings.GetLocationKey cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetLocationName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].LocationName;
end;// TMMSettings.GetLocationName cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetMask(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Mask;
end;// TMMSettings.GetMask cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetMaxValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].MaxValue;
end;// TMMSettings.GetMaxValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetMinValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].MinValue;
end;// TMMSettings.GetMinValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetNativeValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].NativeValue;
end;// TMMSettings.GetNativeValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetNumberOfValues: Word;
begin
  Result := Word(High(cMMSetupDefaults)) - Word(Low(cMMSetupDefaults)) + 1;
end;// TMMSettings.GetNumberOfValues cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetOnlyTemplateSetsAvailable: Boolean;
begin
  Result := GetUserValue(GetID(cOnlyTemplateSetsAvailable)); // Pos. aus SetupMMDefaults
end;// TMMSettings.GetOnlyTemplateSetsAvailable cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetPrintable(aId: Word): Boolean;
begin
  Result := cMMSetupDefaults[aId].Printable;
end;// TMMSettings.GetPrintable cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetReadOnlyData(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ReadOnlyData;
end;// TMMSettings.GetReadOnlyData cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetRestart(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].Restart;
end;// TMMSettings.GetRestart cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetUniqueValueName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].UniqueValueName;
end;// TMMSettings.GetUniqueValueName cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetUserValue(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].UserValue;
end;// TMMSettings.GetUserValue cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetUserValueByName(aUniqueName: string): Variant;
begin
  Result := cMMSetupDefaults[GetID(aUniqueName)].UserValue;
end;// TMMSettings.GetUserValueByName cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetValueChange(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueChange;
end;// TMMSettings.GetValueChange cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetValueName(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueName;
end;// TMMSettings.GetValueName cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetValueText(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueText;
end;// TMMSettings.GetValueText cat:No category

//:-------------------------------------------------------------------
function TMMSettings.GetValueType(aId: Word): Variant;
begin
  Result := cMMSetupDefaults[aId].ValueType;
end;// TMMSettings.GetValueType cat:No category

//:-------------------------------------------------------------------
function TMMSettings.HasNetTyp(aNet: TNetTyp): Boolean;
begin
  Result := (aNet in mInstalledHandlers);
end;// TMMSettings.HasNetTyp cat:No category

//:-------------------------------------------------------------------
function TMMSettings.Init(aFreeDB: Boolean = False): Boolean;
begin
  Result := False;
  fErrorTxt := '';
  if not Initialized then
  try
    ReadRegistryValues;
    if Assigned(mDataBase) then
    try
      if mDataBase.Init then begin
        try
          ReadDBValues;
        except
          on e: Exception do begin
            fErrorTxt := 'ReadDBValues failed ' + e.Message;
            Exit;
          end;
        end;
        fInitialized := True;
        if aFreeDB then
          FreeAndNil(mDataBase);
  //      Result := True;
      end else
        fErrorTxt := mDataBase.DBErrorTxt;
    except
      on e: Exception do
        fErrorTxt := mDataBase.DBErrorTxt + ': ' + e.Message;
    end;
  except
    on e: Exception do
      fErrorTxt := 'TMMSettings.Init: ' + e.Message;
  end;
  Result := Initialized;
end;// TMMSettings.Init cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.ReadDBValues;
var
  xCount: Integer;
  i: Integer;
begin
  xCount := GetNumberOfValues;
  for i := 0 to xCount - 1 do try
    with cMMSetupDefaults[i] do begin
      if Location = lDB then begin
        UserValue := Trim(GetDBValue(i));
      end;
    end;
  except
    on e: Exception do
      raise exception.Create('Error in ReadDBValues: ' + e.Message);
  end;
  FetchInstalledComponents;
  FetchInstalledPackages;
  FetchInstalledHandlers;
  FetchDBVersion;
end;// TMMSettings.ReadDBValues cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.ReadRegistryValues;
var
  i: Integer;
  xCount: Integer;
begin
  with TmmRegistry.Create do
  try
    RootKey := HKEY_LOCAL_MACHINE;
    // alle Werte aus der Registry auslesen
    xCount := GetNumberOfValues;
    for i:=0 to xCount-1 do
    try
      with cMMSetupDefaults[i] do begin
        if Location = lReg then begin
          // set to default value if UserValue not exists in registry
          UserValue := DefaultValue;
          RootKey   := LocationKey;
          if OpenKeyReadOnly(LocationName) then
            if ValueExists(ValueName) then
              // if value exists in registry override UserValue
              UserValue := ReadString(ValueName);
        end; // if
      end; // with
    except
      on e: Exception do
        raise exception.Create('Error in ReadRegistryValues: ' + e.Message);
    end; // for i
  finally
    Free;
  end; // with
end;// TMMSettings.ReadRegistryValues cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetData(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].Data := Value;
end;// TMMSettings.SetData cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetDBValue(aId: Word; const Value: Variant);
var
  xTable, xColumn, xSQLtext, xUserValue: String;
begin
  try
    xTable := cMMSetupDefaults[aId].LocationName;
    xColumn := cMMSetupDefaults[aId].ValueName;
    xUserValue := Value;
  
    if ApplicationName <> '' then
      xSQLtext := Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xUserValue, xColumn, ApplicationName])
    else
      xSQLtext := Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and  AppName = ''%s'' ', [xTable, xUserValue, xColumn, cApp]);
  
    //   xSQLtext:= Format('Update %s SET Data = ''%s'' WHERE Appkey = ''%s'' and AppName = ''%s''', [xTable, xUserValue, xColumn]);
  
    mDataBase.Query[0].Close;
    mDataBase.Query[0].SQL.Clear;
    mDataBase.Query[0].Params.Clear;
    mDataBase.Query[0].SQL.Add(xSQLtext);
    mDataBase.Query[0].ExecSQL;
  except
    on E: Exception do begin
      //Result:= xDefault;
      // Cerate New DB-Data
  
      //MessageDlg(e.Message + '[TMMSettings.GetDBValue]' , mtWarning, [mbOk], 0);
    end;
  end;
end;// TMMSettings.SetDBValue cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetGroup(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].Group := Value;
end;// TMMSettings.SetGroup cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetHintText(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].HintText := Value;
end;// TMMSettings.SetHintText cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetNativeValue(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].NativeValue := Value;
end;// TMMSettings.SetNativeValue cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetPrintable(aId: Word; const Value: Boolean);
begin
  cMMSetupDefaults[aId].Printable := Value;
end;// TMMSettings.SetPrintable cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetUserValue(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].UserValue := Value;
end;// TMMSettings.SetUserValue cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetValueChange(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].ValueChange := Value;
end;// TMMSettings.SetValueChange cat:No category

//:-------------------------------------------------------------------
procedure TMMSettings.SetValueText(aId: Word; const Value: Variant);
begin
  cMMSetupDefaults[aId].ValueText := Value;
end;// TMMSettings.SetValueText cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//:-------------------------------------------------------------------
constructor TMMSettingsReader.Create;
begin
  inherited Create;
  
  {----------------------------------------------}
  
  {----------------------------------------------}
  
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TMMSettingsReader.Create cat:No category

//:-------------------------------------------------------------------
constructor TMMSettingsReader.CreateInstance;
begin
  inherited Create;
  
  {----------------------------------------------}
  mCriticalSection := TCriticalSection.Create;
  fLastRequestedValue := '';
  mSettings := TMMSettings.Create;
end;// TMMSettingsReader.CreateInstance cat:No category

//:-------------------------------------------------------------------
destructor TMMSettingsReader.Destroy;
begin
  FreeAndNil(mSettings);
  mCriticalSection.Free;
  
  
  {----------------------------------------------}
  
  if AccessInstance(0) = Self then AccessInstance(2);
  
  {----------------------------------------------}
  
  {----------------------------------------------}
  
  inherited Destroy;
end;// TMMSettingsReader.Destroy cat:No category

//:-------------------------------------------------------------------
class function TMMSettingsReader.AccessInstance(Request: Integer): TMMSettingsReader;
  
  const FInstance: TMMSettingsReader = nil;
  
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TMMSettingsReader.AccessInstance cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.DBVersionOK(aMinDBVersion: String; aShowMsg: Boolean): Boolean;
begin
  Result := (DBVersion >= aMinDBVersion) or GetRegBoolean(cRegLM, cRegMMDebug, 'IgnoreDBVersion', False);
  if not Result and aShowMsg then
    ShowMessage(Format(cMinDBVersionMsg, [DBVersion]));
end;// TMMSettingsReader.DBVersionOK cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetComponentClearerAssistant: Boolean;
begin
  Init;
  Result := mSettings.IsComponentClearerAssistant;
end;// TMMSettingsReader.GetComponentClearerAssistant cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetComponentLabReport: Boolean;
begin
  Init;
  Result := mSettings.IsComponentLabReport;
end;// TMMSettingsReader.GetComponentLabReport cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetComponentLongTerm: Boolean;
begin
  Init;
  Result := mSettings.IsComponentLongTerm;
end;// TMMSettingsReader.GetComponentLongTerm cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetComponentsAsText: String;
begin
  Init;
  Result := mSettings.ComponentsAsText;
end;// TMMSettingsReader.GetComponentsAsText cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetDBVersion: String;
begin
  Init;
  Result := mSettings.DBVersion;
end;// TMMSettingsReader.GetDBVersion cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetIsComponentQOfflimit: Boolean;
begin
  Init;
  Result := mSettings.IsComponentQOfflimit;
end;// TMMSettingsReader.GetIsComponentQOfflimit cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetIsComponentStyle: Boolean;
begin
  Init;
  Result := mSettings.IsComponentStyle;
end;// TMMSettingsReader.GetIsComponentStyle cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetIsPackageEasy: Boolean;
begin
  Init;
  Result := mSettings.IsPackageEasy;
end;// TMMSettingsReader.GetIsPackageEasy cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetIsPackagePro: Boolean;
begin
  Init;
  Result := mSettings.IsPackagePro;
end;// TMMSettingsReader.GetIsPackagePro cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetIsPackageStandard: Boolean;
begin
  Init;
  Result := mSettings.IsPackageStandard;
end;// TMMSettingsReader.GetIsPackageStandard cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetOnlyTemplateSetsAvailable: Boolean;
begin
  Init;
  Result := mSettings.IsOnlyTemplateSetsAvailable;
end;// TMMSettingsReader.GetOnlyTemplateSetsAvailable cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetPackagesAsText: String;
begin
  Init;
  Result := mSettings.PackagesAsText;
end;// TMMSettingsReader.GetPackagesAsText cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetUserValue(aUniqueName: string): Variant;
begin
  Result := '';
  mCriticalSection.Enter;
  try
    Result := GetValue(aUniqueName);
  finally
    mCriticalSection.Leave;
  end;
end;// TMMSettingsReader.GetUserValue cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.GetValue(aUniqueName: string): Variant;
var
  xNr: Integer;
begin
    // it just checks if the object is initialized. If not it calls the Init method
  Result := 0; // dummy return value to prevent compiler hint
  fLastRequestedValue := aUniqueName;
  Init;
  try
    xNr := mSettings.GetID(aUniqueName);
  
      //Default Value
    if xNr >= 0 then
      Result := mSettings.UserValue[xNr]
    else begin
  //      Result := '0'; // do not return an empty string '', it will raise an exception if converted to number
        // Handelt es sich um Processe ?
      if Pos(cProcess, aUniqueName) > 0 then begin
          // Processe aus Reg. lesen
        xNr := mSettings.GetID(cNumOfProcess);
          // Reg.Pfad ermitteln
        with mSettings do begin
          Result := GetRegString(LocationKey[xNr], LocationName[xNr], aUniqueName);
          if Result = '' then begin
            Result := '0'; // do not return an empty string '', it will raise an exception if converted to number
            fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'No registry entry exist.');
            raise exception.Create(fError.Msg);
          end;
        end; // with mSettings
      end; // if Pos(
    end; // if xNr
  except
    on E: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'Error in GetUserValue. ' + E.Message);
      Result := '';
      raise exception.Create(fError.Msg);
    end;
  end;
end;// TMMSettingsReader.GetValue cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.HasNetTyp(aNet: TNetTyp): Boolean;
begin
  Result := mSettings.HasNetTyp(aNet);
end;// TMMSettingsReader.HasNetTyp cat:No category

//:-------------------------------------------------------------------
function TMMSettingsReader.Init: Boolean;
var
  i: Integer;
begin
  with mSettings do begin
    result := false;
    i := 0;
    // Mindestens 3 aml probieren wenn nicht erfolgreich
    while (i < 3) and (not(result)) do begin
      Result := Init(True);
      inc(i);
      if ErrorTxt <> '' then
        WriteToEventLog(ErrorTxt, 'TMMSettingsReader.Init: ', etError);
    end;// while (i < 3) and (not(result)) do begin
  
  end;
end;// TMMSettingsReader.Init cat:No category

//:-------------------------------------------------------------------
class function TMMSettingsReader.Instance: TMMSettingsReader;
begin
  Result := AccessInstance(1);
end;// TMMSettingsReader.Instance cat:No category

//:-------------------------------------------------------------------
class procedure TMMSettingsReader.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TMMSettingsReader.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
procedure TMMSettingsReader._Create;
begin
  mCriticalSection := TCriticalSection.Create;
  fLastRequestedValue := '';
  mSettings := TMMSettings.Create;
end;// TMMSettingsReader._Create cat:No category

//:-------------------------------------------------------------------
procedure TMMSettingsReader._Destroy;
begin
  FreeAndNil(mSettings);
  mCriticalSection.Free;
  
  inherited Destroy;
end;// TMMSettingsReader._Destroy cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//function TMMSettingsReader.GetComponentArticle: Boolean;
//begin
//  Init;
//  Result := mSettings.IsComponentArticle;
//end;// TMMSettingsReader.GetComponentArticle cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//function TMMSettingsReader.GetComponentDataWareHouse: Boolean;
//begin
//  Init;
//  Result := mSettings.IsComponentDataWareHouse;
//end;// TMMSettingsReader.GetComponentDataWareHouse cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//function TMMSettingsReader.GetPackageClearerAssistant: Boolean;
//begin
//  Init;
//  Result := mSettings.IsPackageClearerAssistant;
//end;// TMMSettingsReader.GetPackageClearerAssistant cat:No category

//------------------------------------------------------------------------------
//function TMMSettingsReader.GetPackageDispoMaster: Boolean;
//begin
//  Init;
//  Result := mSettings.IsPackageDispoMaster;
//end;// TMMSettingsReader.GetPackageDispoMaster cat:No category

//------------------------------------------------------------------------------
//function TMMSettingsReader.GetPackageLabMaster: Boolean;
//begin
//  Init;
//  Result := mSettings.IsPackageLabMaster;
//end;// TMMSettingsReader.GetPackageLabMaster cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//function TMMSettingsReader.GetPackageWindingMaster: Boolean;
//begin
//  Init;
//  Result := mSettings.IsPackageWindingMaster;
//end;// TMMSettingsReader.GetPackageWindingMaster cat:No category

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

initialization

finalization
  TMMSettingsreader.ReleaseInstance;
end.
