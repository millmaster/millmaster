inherited frmAvgBorderFrame: TfrmAvgBorderFrame
  Width = 209
  Height = 55
  object mmLabel1: TmmLabel [0]
    Left = 19
    Top = 8
    Width = 105
    Height = 13
    Caption = '(*)Schichtkompression'
    FocusControl = edShiftCompression
    Visible = True
    WordWrap = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel [1]
    Left = 13
    Top = 36
    Width = 111
    Height = 13
    Caption = '(*)Erlaubte Abweichung'
    FocusControl = edDeviation
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel [2]
    Left = 192
    Top = 35
    Width = 8
    Height = 13
    Caption = '%'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edShiftCompression: TmmEdit [3]
    Left = 128
    Top = 4
    Width = 57
    Height = 21
    Color = clWindow
    TabOrder = 0
    Text = '3'
    Visible = True
    OnChange = GUIChanging
    OnExit = GUIChanged
    Alignment = taRightJustify
    AutoLabel.Control = mmLabel1
    AutoLabel.Distance = 4
    AutoLabel.LabelPosition = lpLeft
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
  end
  object edDeviation: TmmEdit [4]
    Left = 128
    Top = 32
    Width = 57
    Height = 21
    Color = clWindow
    TabOrder = 1
    Text = '0'
    Visible = True
    OnChange = GUIChanging
    OnExit = GUIChanged
    Alignment = taRightJustify
    AutoLabel.Control = mmLabel2
    AutoLabel.Distance = 4
    AutoLabel.LabelPosition = lpLeft
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smExtended
  end
  inherited mmTranslator1: TmmTranslator
    TargetsData = (
      1
      1
      (
        '*'
        'Caption'
        0))
  end
end
