(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MsgListClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 22.10.1998  1.00  Wss | Aenderungen an Parameter und Rueckgabeparameter
| 22.06.1999  1.01  Wss | CheckMsg() for MsgDispatcher implemented
| 28.05.2001  1.02  Wss | In SetMsgConfirmed set Received flag to true even it isn't received
                          CheckMsg: check if msg was not received previous -> Received flag
                          DoRepeat checks now for the max count of open messages: > 10 = no repetition
| 10.10.2002        LOK | Umbau ADO
| 10.12.2002  1.02  Wss | GetJobTypFromJobID added
| 17.05.2005  1.02  Wss | In DoRepeat wurde noch falsch gez�hlt um die fehlenden Msg's zu holen
| 26.07.2005  1.02  Wss | In MsgComplete war LockList nicht mit try...finally gekappselt
| 21.12.2005  1.02  Wss | DoRepeat modifiziert: wenn Anzahl < Trigger ist, jedoch ganze Msg fehlt
                          -> MsgNotComplete an JobHandler
|=========================================================================================*)
unit MsgListClass;

interface

uses
  mmList, Classes, SysUtils, Windows,
  IPCClass, BaseGlobal;

type
  PMsgRec = ^TMsgRec;
  TMsgRec = record
    JobID:        DWord;
    TimeStamp:    DWord;
    Timeout:      DWord;
    Value:        DWord;
    RepeatCount:  Byte;
    Received:     Boolean;
    Confirmed:    Boolean;
    Job:          PJobRec;
  end;

  TMsgListError = (mlNone, mlJobID, mlJobTyp, mlValue);

  TMsgList = class(TProtectedList)
  private
    fListError: TErrorRec;
    fMsgIndex: Integer;
    function GetItems(aIndex: Integer): PMsgRec;
    function MsgComplete(aJobID: DWord): Boolean;
    function GetMsgIndexFromJobID(aJobID: DWord): Integer;
//    function DecRepeatCount(aIndex: Integer): Boolean;
  protected
  public
    property Items[aIndex: Integer]: PMsgRec read GetItems;
    property MsgIndexFromJobID[aJobID: DWord]: Integer read GetMsgIndexFromJobID;
    property MsgIndex: Integer read fMsgIndex;
    constructor Create; override;
    destructor Destroy; override;
    function AddMsg(aJob: PJobRec; aValue, aRepeat, aTime, aTimeout: DWord): Boolean;
    function MsgPending(aJobTyp: TJobTyp; aJobID, aValue: DWord): Boolean;
    function CheckRepeatCount(aJobID: DWord; var aIndex: Integer): Boolean;
    function CleanUp(aTimePeriod: DWord; var aCount: DWord): Boolean;
    function DelMsg(aJobID: DWord): Boolean;
    function DoRepeat(aIndex: Integer): Boolean;
    function GetExpiredMsg(var aIndex: Integer): Boolean;
    function GetJobIDList(var aCount: Word; var aJobList: TJobIDList): Boolean;
    function GetJobTypFromJobID(aJobID: DWord): TJobTyp;
    function GetOriginalJob(aIndex: Integer; var aDestJob: PJobRec; var aBufferSize: DWord): Boolean;
    function GetOriginalJobLink(aIndex: Integer; var aDestJob: PJobRec): Boolean;
    function MsgReceived(var aIsComplete: Boolean; var aJob: PJobRec; var aBufferSize: DWord): Boolean;
    function SetMsgConfirmed(aJobID: DWord): Boolean;
    property ListError: TErrorRec read fListError;
  end;

{$IFDEF MsgDebug}
var
  gMachID1: Integer;
  gMachID6: Integer;
{$ENDIF}
  
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  LoepfeGlobal, mmCS;

//-----------------------------------------------------------------------------
// TMsgList
//-----------------------------------------------------------------------------
constructor TMsgList.Create;
begin
  inherited Create;

  fListError.ErrorTyp := etNoError;
  fMsgIndex           := -1;
end;
//-----------------------------------------------------------------------------
destructor TMsgList.Destroy;
var
  xMsg: PMsgRec;
begin
  while Count > 0 do begin
    xMsg := Items[0];
    // remove associated memory of Job
    FreeMem(xMsg.Job, xMsg.Job.JobLen);
    Dispose(xMsg);
    Delete(0);
  end;

  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TMsgList.AddMsg(aJob: PJobRec; aValue, aRepeat, aTime, aTimeout: DWord): Boolean;
var
  xMsg: PMsgRec;
  xSize: DWord;
begin
  Result := False;

  fListError := SetError(ERROR_NOT_ENOUGH_MEMORY, etNTError, '');
  LockList;
  try
    new(xMsg);
    with xMsg^ do begin
      JobID       := aJob.JobID;
      TimeStamp   := aTime;
      Timeout     := aTimeout;
      RepeatCount := aRepeat;
      Value       := aValue;
      Received    := False;
      Confirmed   := False;
      // allocate memory to hold the original job
      xSize := aJob.JobLen;
      try
        Job := AllocMem(xSize);
        //vvvv don't use the Move methode of TmmList class
        System.Move(aJob^, Job^, xSize);
        Result := True;
      except
        Dispose(xMsg);
        Exit;
      end;
    end;
    // add new message to list
    Add(xMsg);
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.MsgPending(aJobTyp: TJobTyp; aJobID, aValue: DWord): Boolean;
var
  i: Integer;
  xMsg: PMsgRec;
begin
  Result := False;
  LockList;
  try
    for i:=0 to Count-1 do begin
      xMsg := Items[i];
      Result := (aJobID = xMsg^.JobID)
                and (aJobTyp = xMsg^.Job.JobTyp)
                and (aValue = xMsg^.Value)
                and not xMsg^.Received;
      if Result then begin
        fMsgIndex := i;
        Break;
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.CheckRepeatCount(aJobID: DWord; var aIndex: Integer): Boolean;
var
  i: Integer;
  xTime: DWord;
begin
  Result := False;
  LockList;
  try
    xTime := GetTickCount;
    for i:=0 to Count-1 do begin
      if aJobID = Items[i]^.JobID then begin
        Result := (Items[i]^.RepeatCount > 0);
        if Result then begin
          Dec(Items[i]^.RepeatCount);
          Items[i]^.TimeStamp := xTime;
        end else
          Break;
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.CleanUp(aTimePeriod: DWord; var aCount: DWord): Boolean;
var
  i: Integer;
  xTime: DWord;
begin
  i      := 0;
  aCount := 0;
  Result := True;
  fListError.ErrorTyp := etNoError;
  LockList;
  try
    // get current time stamp
    xTime := GetTickCount;
    while i < Count do
    try
      // is it an old message ?
      if (xTime - Items[i].TimeStamp) > aTimePeriod then begin
        // YES, remove the complete message
        DelMsg(Items[i].JobID);
        inc(aCount);
      end else
        inc(i);     // select next MsgItem
    except
      fListError := SetError(ERROR_CANNOT_MAKE, etMMError, 'CleanUp');
      Result := False;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.DelMsg(aJobID: DWord): Boolean;
var
  i: Integer;
  xMsg: PMsgRec;
begin
  i := 0;
  Result := False;
  LockList;
  try
    while i < Count do begin
      xMsg := Items[i];
      if xMsg.JobID = aJobID then begin
        // JobID found, anything alright
        Result := True;
        // remove associated memory of Job
        FreeMem(xMsg.Job, xMsg.Job.JobLen);
        Dispose(xMsg);
        // remove entry from list
        Delete(i);
      end else
        inc(i);
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.DoRepeat(aIndex: Integer): Boolean;
var
  xMaxRepeatTrigger: Integer;
  xMsgCount: Integer;
  xMsgLeftCount: Integer;
  //...........................................................
  function GetMsgLeft(aJobID: DWord): Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i:=0 to Count-1 do begin
      if (Items[i]^.JobID = aJobID) and (not Items[i]^.Received) then
        inc(Result);
    end;
  end;
  //...........................................................
begin
  Result := False;
  LockList;
  try
    if Items[aIndex]^.RepeatCount > 0 then begin
      xMsgCount         := 1;
      xMaxRepeatTrigger := 0;
      if Items[aIndex]^.Job^.JobTyp in [jtGetZESpdData, jtGetExpDataStopZESpd, jtGetExpZESpdData] then begin
        with Items[aIndex]^.Job^ do begin
          case JobTyp of
            jtGetZESpdData:        xMsgCount := (GetZESpdData.SpindleLast - GetZESpdData.SpindleFirst + 1);
            jtGetExpDataStopZESpd: xMsgCount := (GetExpDataStopZEGrp.SpindleLast - GetExpDataStopZEGrp.SpindleFirst + 1);
            jtGetExpZESpdData:     xMsgCount := (GetExpZESpdData.SpindleLast - GetExpZESpdData.SpindleFirst + 1);
          end;
        end;
        // Bis 1/3 des Spindelbereiches werden wiederholt. Wenn es mehr sind dann wird der Job verworfen...
        xMaxRepeatTrigger := xMsgCount div 3;
      end;

      if xMaxRepeatTrigger < cMaxMsgRepeatTrigger then
        xMaxRepeatTrigger := cMaxMsgRepeatTrigger;

      // Wiederholung gibt es nur, wenn die Anzahl nicht zuviele sind. Ansonsten wird der Job dem JobHandler
      // negativ best�tigt und der gesamte Job wird nochmals wiederholt.
      xMsgLeftCount := GetMsgLeft(Items[aIndex]^.JobID);
      Result := (xMsgLeftCount < xMsgCount) and  (xMsgLeftCount <= xMaxRepeatTrigger);
      if Result then begin
        Dec(Items[aIndex]^.RepeatCount);
        Items[aIndex]^.TimeStamp := GetTickCount;
      end;
    end;
  finally
    UnlockList;
  end;

//  Result := False;
//  LockList;
//  try
//    if Items[aIndex]^.RepeatCount > 0 then begin
//      Result := (GetMsgLeft(Items[aIndex]^.JobID) <= cMaxMsgRepeatTrigger);
//      if Result then begin
//        Dec(Items[aIndex]^.RepeatCount);
//        Items[aIndex]^.TimeStamp := GetTickCount;
//      end;
//    end;
//  finally
//    UnlockList;
//  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetExpiredMsg(var aIndex: Integer): Boolean;
var
  i: Integer;
  xTime: DWord;
begin
  Result := False;
  aIndex := -1;
  LockList;
  try
    // get current time stamp
    xTime := GetTickCount;
    for i:=0 to Count-1 do begin
      // check only message items which aren't received or confirmed yet
//      if not Items[i]^.Received or Items[i]^.Confirmed then begin
      if (not Items[i]^.Received) and (not Items[i]^.Confirmed) then begin
        Result := ((xTime - Items[i]^.TimeStamp) > Items[i]^.Timeout);
        // Time on this message is expired -> Timeout
        if Result then begin
          with Items[i]^ do
            CodeSite.SendMsgEx(csmCheckPoint, Format('JobID: %d, Timeout:%d, xTime:%d, TimeStamp:%d', [JobID, Timeout, xTime, TimeStamp]));
          aIndex := i;
          break;
        end;
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetItems(aIndex: Integer): PMsgRec;
begin
  Result := inherited Items[aIndex];
end;
//-----------------------------------------------------------------------------
function TMsgList.GetJobIDList(var aCount: Word; var aJobList: TJobIDList): Boolean;
var
  i,j: Integer;
  xMsg: PMsgRec;
  xFound: Boolean;
begin
  Result := True;

  fListError.ErrorTyp := etNoError;
  aCount    := 0;
  LockList;
  try
    // count through all items
    for i:=0 to Count-1 do begin
      // select a MsgItem
      xMsg := Items[i];
      xFound := False;
      // looking in aJobList if it does exist
      for j:=0 to aCount-1 do begin
        xFound := (xMsg.JobID = aJobList[j].JobID);
        // JobID already in aJobList -> stop search further
        if xFound then
          break;
      end;
      // JobID does not exist in aJobList
      if not xFound then begin
        // add it
        aJobList[aCount].JobID := xMsg.JobID;
        inc(aCount);
      end;
      // if there are more valid JobID than aJobList can hold  -> break
      if aCount >= cMaxJobs then begin
        fListError := SetError(ERROR_TOO_MANY_OPEN_FILES, etNTError, 'GetJobIDList');
        aCount := 0;
        Result := False;
        break;
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetJobTypFromJobID(aJobID: DWord): TJobTyp;
var
  i: Integer;
begin
  Result := jtNone;
  LockList;
  try
    for i:=0 to Count-1 do
      if aJobID = Items[i]^.JobID then begin
        Result := Items[i]^.Job^.JobTyp;
        Break;
      end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetMsgIndexFromJobID(aJobID: DWord): Integer;
var
  i: Integer;
begin
  Result := -1;
  LockList;
  try
    for i:=0 to Count-1 do
      if aJobID = Items[i]^.JobID then begin
        Result := i;
        Break;
      end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetOriginalJob(aIndex: Integer; var aDestJob: PJobRec; var aBufferSize: DWord): Boolean;
var
  xPJob: PJobRec;
begin
  Result := False;
  LockList;
  try
    xPJob := Nil;
    if aIndex < Count then
    try
      // Job holen..
      xPJob := Items[aIndex]^.Job;
      if xPJob.JobLen > aBufferSize then begin
        aBufferSize := xPJob.JobLen;
        ReallocMem(aDestJob, aBufferSize);
      end;
      //...und anschliessen r�berkopieren. Voraussetzung: Buffergr�sse ist genug gross, muss
      // es ja aber sein, da der Buffer f�r mJob nicht verkleinert wird, nur vergr�ssert
      System.Move(xPJob^, aDestJob^, xPJob^.JobLen);
      Result := True;
    except
      CodeSite.SendError(Format('TMsgList.GetOriginalJob failed(%d): Index = %d', [xPJob^.JobID, aIndex]));
    end;
  finally
    UnlockList;
  end;

//  LockList;
//  try
//    try
//      // Job holen..
//      xPJob := Items[aIndex]^.Job;
//      //...Speichergr�sse von Buffer anpassen...
//      ReallocMem(aDestJob, xPJob^.JobLen);
//      //...und anschliessen r�berkopieren.
//      System.Move(xPJob^, aDestJob^, xPJob^.JobLen);
//      Result := True;
//    except
//      CodeSite.SendError(Format('TMsgList.GetOriginalJob failed(%d): Index = %d', [xPJob^.JobID, aIndex]));
//    end;
//  finally
//    UnlockList;
//  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.GetOriginalJobLink(aIndex: Integer; var aDestJob: PJobRec): Boolean;
var
  xPJob: PJobRec;
begin
  Result := False;
  LockList;
  try
    xPJob := Nil;
    if aIndex < Count then
    try
      // Job holen..
      aDestJob := Items[aIndex]^.Job;
      Result := True;
    except
      CodeSite.SendError(Format('TMsgList.GetOriginalJob failed(%d): Index = %d', [xPJob^.JobID, aIndex]));
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.MsgComplete(aJobID: DWord): Boolean;
var
  i: Integer;
  xMsg: PMsgRec;
begin
  // start default
  Result := True;
  LockList;
  try
    for i:=0 to Count-1 do begin
      xMsg := Items[i];
      if xMsg.JobID = aJobID then
        Result := Result and xMsg.Received;
        if not Result then
          Break;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.MsgReceived(var aIsComplete: Boolean; var aJob: PJobRec; var aBufferSize: DWord): Boolean;
var
  i: Integer;
  xJobIndex: Integer;
  xMsg: PMsgRec;
  xConfirmed: Boolean;
begin
  Result     := False;
  xJobIndex  := -1;
  xConfirmed := False;
  // if JobID isn't in MsgList set default error value
  fListError := SetError(ERROR_NO_SUCH_PACKAGE, etMMError, 'MsgReceived');

  LockList;
  try
    xMsg := Items[0];
    CodeSite.SendFmtMsg('-->TMsgList.MsgReceived: Count=%d, JodID=%d, JobType=%d', [Count, xMsg^.JobID, Ord(xMsg^.Job^.JobTyp)]);

    for i:=0 to Count-1 do begin
      xMsg := Items[i];
      Result := (aJob^.JobID = xMsg^.JobID) and
                (aJob^.MsgReceived.JobTyp = xMsg^.Job^.JobTyp) and
                (aJob^.MsgReceived.Value = xMsg^.Value);

      if Result then begin
        xMsg^.Received := True;
        xConfirmed     := xMsg^.Confirmed;
        xJobIndex := i;
        Break;
      end;
    end;  // for
  finally
    UnlockList;
  end;
  // check if the current Job with JobID is complete
  if Result then begin
    // prevent of confirming message more than once -> and not xConfirmed
    aIsComplete := MsgComplete(aJob.JobID) and not xConfirmed;
    if aIsComplete then begin
      // Msg is complete -> return the original Job with all settings to positive confirm
      GetOriginalJob(xJobIndex, aJob, aBufferSize);
    end;
  end;
end;
//-----------------------------------------------------------------------------
function TMsgList.SetMsgConfirmed(aJobID: DWord): Boolean;
var
  i: Integer;
  xMsg: PMsgRec;
begin
  Result := True;
  // count through all items and set all msg with aJobID as confirmed
  LockList;
  try
    for i:=0 to Count-1 do begin
      xMsg := Items[i];
      if xMsg.JobID = aJobID then begin
        xMsg.Confirmed := True;
        xMsg.Received  := True; // in case lost messages are received later -> no not care
      end;
    end;
  finally
    UnlockList;
  end;
end;
//-----------------------------------------------------------------------------
initialization
{$IFDEF MsgDebug}
  gMachID1 := 0; // Job wird immer beblockt gegen�ber JobHandler
  gMachID6 := 0; // Job wird 1x beblockt gegen�ber JobHandler
{$ENDIF}
end.

