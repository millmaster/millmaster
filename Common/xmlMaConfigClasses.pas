{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: xmlMaConfigClasses.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: Helper Klassen f�r die Bearbeitung und dem Zugriff von/auf Bauzust�nde(n)
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows 2000
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.10.2004  1.00  Lok | File created
| 14.02.2005        Nue | MachName implemented.
| 05.07.2005        Lok | 2 Warnings "entsorgt"
| 05.10.2005        Nue | = eingef�gt bei (aSpindle <= cMaxSpindeln) in TXMLMaConfigHelper.FirstGroupSpindleFromSpindle
| 10.03.2006        Lok | ReadOnly Property NetTyp in MaConfigReader eingef�gt. Wird beim ersten Zugriff abgef�llt.
| 18.01.2007    Nue/Lok | Try except eingef�gt in TXMLMaConfigHelper.GetMachNode
|==========================================================================================}
(*---------------------------------------------------------
  DEBUG Registry Keys:
    TXMLMaConfigHelper --> Ausgabe von Codesite Meldungen bei der Zusammenfassung
                           von Gruppen mit identischen Settings.
----------------------------------------------------------*)
unit xmlMaConfigClasses;
                                                   
interface
uses
  Classes, Sysutils, MSXML2_TLB, BaseGlobal, XMLGlobal,
  MMUGlobal, FPatternClasses, XMLMappingDef;

const
  cDefaultIndex = 0;
  cMaxExclIndex = 1;
  cMaxInclIndex = 2;
  cMinExclIndex = 3;
  cMinInclIndex = 4;
  cEnumIndex    = 5;
  
type
  TConstraintType = (ctNone,
                     ctDefault,
                     ctMinIncl,
                     ctMinExcl,
                     ctMaxIncl,
                     ctMaxExcl,
                     ctEnum);// TConstraintType
  
  // Klassenreferenz auf einen MaConfigHelper (Wird f�r die Fabrik ben�tigt)
  TMaConfigReaderClass = class of TMaConfigReader;
  
  TXMLMaConfigHelper = class;
  TMaConfigReader = class;
  TMaConfigFPHandler = class;
  (*: Klasse:        TXMLConstraints
      Vorg�nger:     TObject
      Kategorie:     No category
      Kurzbeschrieb: Hilfsklasse f�r die Grenzwerte und die Defaults 
      Beschreibung:  
                     Die Klasse extrahiert aus dem DOM Node die entsprechenden Constraints (zB: MinIncl) heraus
                     und merkt sich das Element auf das zuletzt zugegriffen wurde. *)
  //1 Hilfsklasse f�r die Grenzwerte und die Defaults 
  TXMLConstraints = class(TObject)
  private
    //1 aktuelles Element, dessen Grenzwerte abgefragt werden sollen 
    fDOMElement: IXMLDOMElement;
    //1 Liefert den Wert des gew�nschten Constraints (Index Eigenschaft) 
    function GetConstraintValue(aIndex: integer): Variant;
  public
    //1 Liefert den "richtigen" Selektionspfad zum gwe�nschten Element (MMXML) 
    class function GetMMSelPathFromElement(aElement: IXMLDOMNode): string;
    //1 Abfrage ob ein gew�nschter Constraint vorhanden ist 
    function HasConstraint(aType: TConstraintType): Boolean;
    //1 Default Wert aus dem XML 
    property Default: Variant index cDefaultIndex read GetConstraintValue;
    //1 aktuelles Element, dessen Grenzwerte abgefragt werden sollen 
    property DOMElement: IXMLDOMElement read fDOMElement write fDOMElement;
    //1 Grenzwert aus dem XML 
    property Enum: Variant index cEnumIndex read GetConstraintValue;
    //1 Grenzwert aus dem XML 
    property MaxExcl: Variant index cMaxExclIndex read GetConstraintValue;
    //1 Grenzwert aus dem XML 
    property MaxIncl: Variant index cMaxInclIndex read GetConstraintValue;
    //1 Grenzwert aus dem XML 
    property MinExcl: Variant index cMinExclIndex read GetConstraintValue;
    //1 Grenzwert aus dem XML 
    property MinIncl: Variant index cMinInclIndex read GetConstraintValue;
  end;

  (*: Klasse:        TFabMaConfigHelper
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Repr�sentiert eine gesamte Fabrik 
      Beschreibung:  
                     In dieser Klasse werden die FPattern aus der gesamten Fabrik gesammelt.
                     Einzige Funktion dieser Klasse ist es die FPattern zu verschmelzen und der Applikation
                     ein verschmolzenes FPattern zur Verf�gung zu stellen.
                     
                     Anwendungsbeispiel:
                     
                       procedure TFPatternMergeTest.TestMergeFab;
                       var
                         xFab: TFabMaConfigHelper;
                       begin
                         xFab := TFabMaConfigHelper.Create(TMaConfigReader);
                         try
                           { �ber xFab.FPValue[XPath] kann auf die einzelnen FPattern zugegriffen werden.
                             Beim ersten Zugriff wird das FPattern generiert. }
                           CheckTrue(xFab.FPValue[cXPFP_ClassClearingItem], 'cXPFP_ClassClearingItem');
                           CheckTrue(xFab.FPValue[cXPFP_ShortCountItem], 'cXPFP_ShortCountItem');
                           CheckTrue(xFab.FPValue[cXPFP_FSpectraNormalItem], 'cXPFP_FSpectraNormalItem');
                           CheckFalse(xFab.FPValue[cXPFP_FSpectraBDItem], 'cXPFP_FSpectraBDItem');
                         finally
                           xFab.Free;
                         end;// try finally
                       end;
                      *)
  //1 Repr�sentiert eine gesamte Fabrik 
  TFabMaConfigHelper = class(TPersistent)
  private
    //1 True, wenn seit dem Laden der Maschinen von der DB das FPattern berechnet wurde 
    fFPCalculated: Boolean;
    fFPElement: IXMLDOMElement;
    //1 Typ f�r das Verschmelzen von FPattern (Min oder Max) 
    fFPType: TFPType;
    //1 True, wenn alle verf�gbaren Bauzust�nde aus der DB gelesen wurden 
    fMachinesLoaded: Boolean;
    //1 Zeigt an ob die MaConfig Helper wieder freigegeben werden m�ssen 
    fOwnedObjects: Boolean;
    //1 Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine 
    function GetAvailableSensingHeadClasses: TSensingHeadClassSet;
    //1 Liefert beim ersten Zugriff auf das FPElement das selbige zur�ck 
    function GetFPElement: IXMLDOMElement;
  protected
    //1 Wird aufgerufen, sobald ein Eleemnt aus der Liste gel�scht wird 
    fOnDeleteItem: TNotifyEvent;
    //1 Referenz auf die Klasse die beim laden von der DB erzeugt werden soll 
    mClassToHold: TMaConfigReaderClass;
    //1 Liste mit den einzelen Bauzust�nden 
    mItems: TList;
    //1 Wenn True, dann Ausgabe in Codesite 
    mOutputDebugMsg: Boolean;
    //1 Liefert die Anzahl der Eintr�ge in der Liste 
    function GetCount: Integer;
    //1 Liest den Wert eines FPattern Elements 
    function GetFPValue(aSelPath: string): Boolean; virtual;
    //1 Liefert ein MaConfig f�r eine einzelne Maschine 
    function GetItems(aIndex: Integer): TMaConfigReader;
  public
    //1 Konstruktor 
    constructor Create; overload; virtual;
    //1 Alternativer Konstruktor 
    constructor Create(aClassToHold: TMaConfigReaderClass); overload; virtual;
    //1 Einzelne Bauzust�nde wieder freigeben 
    destructor Destroy; override;
    //1 F�gt einen Bauzustand f�r eine Maschine zur Liste hinzu 
    function Add(aItem: TMaConfigReader): Integer; virtual;
    //1 Berechnet das Verschmolzene FPattern �ber alle Bauzust�nde in der Liste 
    procedure CalculateFPattern(aFPType: TFPType);
    //1 L�scht alle Bauzust�nde aus der Lsite 
    procedure Clear;
    //1 L�scht einen einzelen Eintrag aus der Liste 
    procedure Delete(aIndex: integer); virtual;
    //1 L�dt alle verf�gbaren Bauzust�nde aus der Datenbank 
    procedure LoadMachines;
    //1 Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine 
    property AvailableSensingHeadClasses: TSensingHeadClassSet read GetAvailableSensingHeadClasses;
    //1 Liefert die Anzahl Eintr�ge in der Liste 
    property Count: Integer read GetCount;
    //1 True, wenn seit dem Laden der Maschinen von der DB das FPattern berechnet wurde 
    property FPCalculated: Boolean read fFPCalculated write fFPCalculated;
    //1 XML Element f�r die Speicherung des generierten FPatterns 
    property FPElement: IXMLDOMElement read GetFPElement;
    //1 Typ f�r das Verschmelzen von FPattern (Min oder Max) 
    property FPType: TFPType read fFPType write fFPType;
    //1 Wert eines FPattern Elements 
    property FPValue[aSelPath: string]: Boolean read GetFPValue;
    //1 Zugriff auf ein einzelnes MaConfig 
    property Items[aIndex: Integer]: TMaConfigReader read GetItems; default;
    //1 True, wenn alle verf�gbaren Bauzust�nde aus der DB gelesen wurden 
    property MachinesLoaded: Boolean read fMachinesLoaded write fMachinesLoaded;
    //1 Zeigt an ob die MaConfig Helper wieder freigegeben werden m�ssen 
    property OwnedObjects: Boolean read fOwnedObjects write fOwnedObjects;
    //1 Wird aufgerufen, sobald ein Eleemnt aus der Liste gel�scht wird 
    property OnDeleteItem: TNotifyEvent read fOnDeleteItem write fOnDeleteItem;
  end;

  (*: Klasse:        TXMLMaConfigHelper
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Kapselung f�r den Bauzustand und die Defaults einer einzelnen Maschine 
      Beschreibung:  
                     Diese Klasse dient zur generellen Bearbeitung und Kapselung der verschiedenen
                     Aktionen die mit dem Bauzustand zusammenh�ngen. Die Klasse wird von den NetHandlern
                     wie auch vom GUI (Assignment und MaConfig) verwendet. *)
  //1 Kapselung f�r den Bauzustand und die Defaults einer einzelnen Maschine 
  TXMLMaConfigHelper = class(TPersistent)
  private
    //1 Normiertes Bauzustands XML 
    fDOM: DOMDocumentMM;
    //1 Wenn True, dann ausgabe in Codesite 
    mOutputDebugMsg: Boolean;
    //1 Gibt den absoluten Pfad zur Gruppe zur�ck 
    function GetGroupXP: string;
    //1 Gibt den absoluten Pfad zur Gruppe zur�ck 
    function GetMachXP: string;
    //1 Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsgruppen Pfad um 
    function MMXPToAbsoluteXP(aMMPath: string): string;
    //1 Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsgruppen Pfad um 
    function MMXPToGroupXP(aMMPath: string): string;
    //1 Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsmaschinen Pfad um 
    function MMXPToMachXP(aMMPath: string): string;
    //1 Bei einer �nderung soll ein Event ausgel�st werden 
    procedure SetDOM(const Value: DOMDocumentMM);
  protected
    //1 Wird aufgerufen, wenn ein neues DOM zugewiesen oder geladen wird (string) 
    procedure DOMChanged; virtual;
    //1 Pr�ft ob ein g�ltiges DOM f�r einen Bauzustand verf�gbar ist. 
    function GetAvailable: Boolean; virtual;
    //1 Zugriffsmethode f�r das Group[] Property. 
    function GetGroup(aIndex: Integer): IXMLDOMElement; virtual;
    //1  Gibt die Anzahl der verf�gbaren Gruppen zur�ck 
    function GetGroupCount: Integer; virtual;
    //1 Liefert ein Element das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetGroupNode(aGroupIndex: Integer; aSelPath: string): IXMLDOMElement; virtual;
    //1 Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetGroupValue(aGroupIndex: Integer; aSelPath: string): Variant; virtual;
    //1 Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetGroupValueDef(aGroupIndex: Integer; aSelPath: string; aDefault: variant): Variant; virtual;
    //1 Liefert die letzte erfasse Spindel der Maschine 
    function GetLastSpindle: Integer; virtual;
    //1 Zugriffsmethode f�r das Group[] Property. 
    function GetMachine: IXMLDOMElement; virtual;
    //1 Liefert ein Element das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetMachNode(aSelPath: string): IXMLDOMElement; virtual;
    //1 Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetMachValue(aSelPath: string): Variant; virtual;
    //1 Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    function GetMachValueDef(aSelPath: string; aDefault: variant): Variant; virtual;
    //1 �berpr�ft zwei Gruppen auf identische Einstellungen 
    function SameGroup(aGroup1, aGroup2: IXMLDOMNode): Boolean; virtual;
    //1 Setzt den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    procedure SetGroupValue(aGroupIndex: Integer; aSelPath: string; const Value: Variant); virtual;
    //1 Setzt den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird. 
    procedure SetMachValue(aSelPath: string; const Value: Variant); virtual;
    procedure SortGroups;
  public
    //1 Debugausgabe initialisieren 
    constructor Create; virtual;
    //1 Aufr�umen 
    destructor Destroy; override;
    //1 F�gt eine Zus�tzliche Gruppe zum DOM hinzu 
    function AddGroup(aGroup: IXMLDOMNode): Integer;
    //1 Erm�glicht das Kopieren von Bauzust�nden 
    procedure Assign(Source: TPersistent); override;
    //1 Stellt das FPattern (TXN, WSC) aus verschiedenen Informationen aus dem DOM zusammen 
    procedure BuildFPatternFromConfig(aNetTyp: TNetTyp);
    //1 Wandelt den String der vom Handler zusammengesetzt wird in die endg�ltige Version 
    function BuildMaConfigFromHandler(const aMaConfXML, aDefaultsXML: string; aNetTyp: TNetTyp; aMapSection:
      TMapSection): string;
    //1 Fasst die einzelnen Gruppen nach M�glichkeit zusammen 
    procedure ConcentrateGroups; virtual;
    //1 L�scht eine Gruppe aus dem DOM 
    procedure DeleteGroup(aIndex: integer);
    //1 Gibt die erste Spindel aus der Gruppe in der sich die gew�nschte Spindel befindet 
    function FirstGroupSpindleFromSpindle(aSpindle: integer; aAutoIncrement: boolean): Integer;
    //1 Liefert das Gruppenelement f�r einen Spindelrange 
    function GetGroupNodeFromSpindle(aSpindle: integer): IXMLDOMNode;
    //1 Gibt die erste und die letzte Spindel der Maschine zur�ck 
    procedure GetMachSpindleRange(out aSpdlFrom: integer; out aSpdlTo: integer);
    //1 Liefert den Index der Gruppe in welcher der gew�nschte Spulstellen bereich liegt 
    function GroupIndexFromRange(aSpindleFrom, aSpindleTo: integer; out aGroupIndex: integer): Boolean;
    //1 True, wenn beide Spindeln in der selben Sektion liegen 
    function IsSameSection(aFirstSpindle, aSecondSpindle: integer): Boolean;
    //1 Ermittelt die letzte Spindel der Gruppe in der die gesuchte Spindel enthalten ist. 
    function LastGroupSpindleFromSpindle(aSpindle: integer; aAutoDecrement: boolean): Integer;
    //1 Parst den String und l�dt ihn in das interne DOM Bei einem Fehler wird false zur�ckgegeben 
    function LoadDOM(aDOM: string): Boolean;
    //1 Pr�ft ob ein g�ltiges DOM f�r einen Bauzustand verf�gbar ist. 
    property Available: Boolean read GetAvailable;
    //1 Normiertes Bauzustands XML 
    property DOM: DOMDocumentMM read fDOM write SetDOM;
    //1 Liefert die entsprechende Gruppe im XML (Muss nicht in der richtigen Reihenfolge sein) 
    property Group[aIndex: Integer]: IXMLDOMElement read GetGroup;
    //1 Anzahl der gelisteten Gruppen (Anzahl der Bauzust�nde) 
    property GroupCount: Integer read GetGroupCount;
    //1 Liefert ein IXMLDOMElement das einem einzelnen Wert entspricht 
    property GroupNode[aGroupIndex: Integer; aSelPath: string]: IXMLDOMElement read GetGroupNode;
    //1 Liefert den Wert des entsprechenden Elements 
    property GroupValue[aGroupIndex: Integer; aSelPath: string]: Variant read GetGroupValue write SetGroupValue;
    //1 Liefert den Wert des entsprechenden Elements (Default wenn das Element nicht vorhanden ist) 
    property GroupValueDef[aGroupIndex: Integer; aSelPath: string; aDefault: variant]: Variant read GetGroupValueDef;
    //1 Liefert die letzte erfasse Spindel der Maschine 
    property LastSpindle: Integer read GetLastSpindle;
    //1 Liefert den Maschinen Knoten 
    property Machine: IXMLDOMElement read GetMachine;
    //1 Liefert das IXMLDOMElement das einen Wert im Maschinen Element repr�sentiert 
    property MachNode[aSelPath: string]: IXMLDOMElement read GetMachNode;
    //1 Liefert den Wert des entsprechenden Elements 
    property MachValue[aSelPath: string]: Variant read GetMachValue write SetMachValue;
    //1 Liefert den Wert des entsprechenden Elements (Default wenn das Element nicht vorhanden ist) 
    property MachValueDef[aSelPath: string; aDefault: variant]: Variant read GetMachValueDef;
  end;

  
  (*: Klasse:        TMaConfigReader
      Vorg�nger:     TXMLMaConfigHelper
      Kategorie:     No category
      Kurzbeschrieb: Hat eine Verbindung zur Datenbank und liest die Maschinenkonfiguration von der DB 
      Beschreibung:  
                     Die Maschinenkonfiguration ist in einem XML Dokument gespeichert das wie folgt aufgebaut ist.
                     1) F�r jede Bauzustandsgruppe ein Group Element
                     2) Ein Maschinen Element f�r Maschinen abh�ngigen Settings
                     3) Ein Defaults XML Settings mit den Facets als Attribute und den Defaults als Text *)
  //1 Hat eine Verbindung zur Datenbank und liest die Maschinenkonfiguration von der DB 
  TMaConfigReader = class(TXMLMaConfigHelper)
  private
    //1 Set mit allen verwendeten Tastk�pfen auf der Maschine 
    fAvailableSensingHeadClasses: TSensingHeadClassSet;
    //1 Erm�glicht den Zugriff auf die Attribute im Defaults-XML 
    fConstraints: TXMLConstraints;
    //1 Hashcode von der DB 
    fDBHashcode1: LongInt;
    //1 Hashcode von der DB 
    fDBHashcode2: LongInt;
    //1 FPattern Reader 
    fFPHandler: TBaseFPHandler;
    //1 DB ID der Maschine f�r die die Werte gew�nscht sind 
    fMachID: Integer;
    fNetTyp: TNetTyp;
    //1 Wird ausgel�st, wenn eine neue MachID zugewiesen wird 
    fOnMachineChanged: TNotifyEvent;
    //1 Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine 
    function GetAvailableSensingHeadClasses: TSensingHeadClassSet;
    //1 Liefert die Grenz- und Default Wert(e) des entsprechenden Elements 
    function GetConstraints(aSelPath: string): TXMLConstraints;
    //1 Erzeugt einen FPReader. 
    function GetFPHandler: TBaseFPHandler;
    function GetNetTyp: TNetTyp;
    //1 Zugriffsmethode f�r die MachID 
    procedure SetMachID(const Value: Integer);
  protected
    //1 Name der Maschine auf der DB (nicht im DOM) 
    fMachName: string;
    //1 Leitet den Event aus dem FPReader weiter 
    procedure ConfigChanged(Sender: TObject);
    //1 Dispatch Routine f�r den Event 'OnMachineChanged' 
    procedure DoMachineChanged;
    //1 Wird aufgerufen, wenn ein neues DOM zugewiesen oder geladen wird 
    procedure DOMChanged; override;
    //1 Gibt zur�ck ob ein Bauzustand vorhanden ist 
    function GetAvailable: Boolean; override;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 FPReader wieder freigeben 
    destructor Destroy; override;
    //1 Erm�glicht das Kopieren von Bauzust�nden 
    procedure Assign(Source: TPersistent); override;
    //1 L�dt das Bauzustands XML f�r eine Maschine von der DB 
    function LoadMachConfigFromDB(aMachID: integer): Boolean;
    //1 Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine 
    property AvailableSensingHeadClasses: TSensingHeadClassSet read GetAvailableSensingHeadClasses;
    //1 Erm�glicht den Zugriff auf die Attribute im Defaults-XML 
    property Constraints[aSelPath: string]: TXMLConstraints read GetConstraints; default;
    //1 Hashcode von der DB 
    property DBHashcode1: LongInt read fDBHashcode1;
    //1 Hashcode von der DB 
    property DBHashcode2: LongInt read fDBHashcode2;
    //1 FPattern Reader 
    property FPHandler: TBaseFPHandler read GetFPHandler;
    //1 DB ID der Maschine f�r die die Werte gew�nscht sind 
    property MachID: Integer read fMachID write SetMachID;
    //1 Name der Maschine auf der DB (nicht im DOM) 
    property MachName: string read fMachName;
    property NetTyp: TNetTyp read GetNetTyp;
    //1 Wird ausgel�st, wenn eine neue MachID zugewiesen wird 
    property OnMachineChanged: TNotifyEvent read fOnMachineChanged write fOnMachineChanged;
  end;

  (*: Klasse:        TMaConfigFPHandler
      Vorg�nger:     TBaseFPHandler
      Kategorie:     No category
      Kurzbeschrieb: Bildet das minimale- oder maximale FPattern �ber mehrere Gruppen 
      Beschreibung:  
                     Je nach Zugriff wird das minimale (Spindelrange -1 bis -1 z.B. mit der Methode 'resetSpindleRange')
                     oder
                     das maximale (beliebiger Spindelrange > 0) FPattern gebildet. *)
  //1 Bildet das minimale- oder maximale FPattern �ber mehrere Gruppen 
  TMaConfigFPHandler = class(TBaseFPHandler)
  private
    //1 Backpointer auf den MaConfigHelper um auf das MaConfigXML zuzugreifen 
    fMaConfigHelper: TXMLMaConfigHelper;
    //1 Speichert, ob das maximale oder das minimale FPattern gebildet werden muss 
    mFPType: TFPType;
    //1 Zwischenspeicher f�r die Feststellung was aktuell berechnet ist --> mFPElement 
    mOldFPType: TFPType;
    //1 Zwischenspeicher f�r die Feststellung was aktuell berechnet ist --> mFPElement 
    mOldSpdlFrom: Integer;
    //1 Zwischenspeicher f�r die Feststellung was aktuell berechnet ist --> mFPElement 
    mOldSpdlTo: Integer;
    //1 Aktuelle erste Spindel 
    mSpdlFrom: Integer;
    //1 Aktuelle letzte Spindel 
    mSpdlTo: Integer;
  protected
    //1 Erm�glicht den Zugriff auf ein einzelnes Item im FPattern 
    function GetValue(aSelPath: string): Variant; override;
    //1 Stellt fest ob vor dem Bilden des FPatterns der Spindelrange der Maschine ermittelt werden muss 
    function IsMachineRange: Boolean;
    //1 Wenn seit der letzten Berechnung der Spindelrange ge�ndert hat, muss neu berechnet werden 
    function NeedToRecalc: Boolean;
    //1 Setzt den Spindelbereich neu der f�r die Berechnung des Min- oder Max FPattern benutzt wird 
    function PrepareNodeListForCalc: IXMLDOMNodeList;
    //1 Setzt die Parameter um zu Kennzeichnen, dass keine Neuberechnung notwendig ist 
    procedure SetCalculatedSpindleRange;
  public
    //1 Konstruktor 
    constructor Create; override;
    //1 Erstellt das FPattern das die maximale Funktionalit�t eines Spindelbereichs beschreibt 
    procedure CalcFPattern(aFPType: TFPType);
    //1 Setzt die Berechnung des FPattern zur�ck, so wird beim n�chsten Zugriff das FPattern neu berechnet 
    procedure ResetCalculation;
    //1 Setzt den Spindelrange zur�ck 
    procedure ResetSpindleRange; override;
    //1 aktualisiert den Spindelrange 
    procedure SetSpindleRange(aSpdlFrom, aSpdlTo: integer); reintroduce; overload; virtual;
    //1 aktualisiert den Spindelrange 
    procedure SetSpindleRange(aSpdlFrom, aSpdlTo: integer; aFPType: TFPType); overload; override;
    //1 Backpointer auf den MaConfigHelper um auf das MaConfigXML zuzugreifen 
    property MaConfigHelper: TXMLMaConfigHelper read fMaConfigHelper write fMaConfigHelper;
  end;


const
  cUninitializedMachine = -1;
  
implementation
uses
  LoepfeGlobal, mmcs, typinfo, XMLDef, MMEventLog, AdoDbAccess, YMParaUtils;

(*:
 *  Klasse:        TXMLConstraints
 *  Vorg�nger:     TObject
 *  Kategorie:     No category
 *  Kurzbeschrieb: Hilfsklasse f�r die Grenzwerte und die Defaults 
 *  Beschreibung:  Die Klasse extrahiert aus dem DOM Node die entsprechenden Constraints (zB: MinIncl) heraus
                   und merkt sich das Element auf das zuletzt zugegriffen wurde. 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           GetConstraintValue
 *  Klasse:           TXMLConstraints
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Liefert den Wert des gew�nschten Constraints (Index Eigenschaft)
 *  Beschreibung:     
                      Zugriffsmethode f�r alle Constraints Eigenschaften (Indiziert).
 --------------------------------------------------------------------*)
function TXMLConstraints.GetConstraintValue(aIndex: integer): Variant;
begin
  Assert(assigned(fDOMElement), 'Ohne XML-Element k�nnen keine Constraints ermittelt werden');

//: ----------------------------------------------
  result := unassigned;
  if assigned(fDOMElement) then begin
    case aIndex of
      cDefaultIndex: begin
        result := GetElementValueDef(fDOMElement, NULL);
        try
          result := NumVariantToFloat(result, 0);
        except
        end;
      end;

      cMaxExclIndex: result := NumVariantToFloat(fDOMElement.GetAttribute(cAtrMaxExcl), 0);
      cMaxInclIndex: result := NumVariantToFloat(fDOMElement.GetAttribute(cAtrMaxIncl), 0);
      cMinExclIndex: result := NumVariantToFloat(fDOMElement.GetAttribute(cAtrMinExcl), 0);
      cMinInclIndex: result := NumVariantToFloat(fDOMElement.GetAttribute(cAtrMinIncl), 0);
      cEnumIndex   : result := fDOMElement.GetAttribute(cAtrEnum)
    end;// case aIndex of
  end;
end;// TXMLConstraints.GetConstraintValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMMSelPathFromElement
 *  Klasse:           TXMLConstraints
 *  Kategorie:        No category 
 *  Argumente:        (aElement)
 *
 *  Kurzbeschreibung: Liefert den "richtigen" Selektionspfad zum gwe�nschten Element (MMXML)
 *  Beschreibung:     
                      Die Methode wird von der Klasse nicht ben�tigt, ist aber wegen der engen Zusammengeh�rigkeit
                      mit den Defaults in dieser Klasse als "Class" Methode definiert.
                      Die Methode liefert f�r ein Element das im Defaults XML liegt den XPath der ben�tigt wird
                      um das entsprechende Element in einem MMXML zu selektieren. Dies ist nicht einfach der rekursive
                      Pfad,
                      sondern muss zusammengesetzt werden (Element "defaults").
 --------------------------------------------------------------------*)
class function TXMLConstraints.GetMMSelPathFromElement(aElement: IXMLDOMNode): string;
var
  xPos: Integer;
begin
  result := GetSelectionPath(aElement);

//: ----------------------------------------------
  xPos := AnsiPos(cXMLDefaultsElementName, result);
  // entweder auf Position 2 (absolute Pfade) oder auf Position 1 (relative Pfade)
  if (xPos > (Length(cXMLLoepfeBody) + 1)) and (xPos <= (Length(cXMLLoepfeBody) + 3)) then
    result := StringReplace(result, cXMLDefaultsElementName + '/', '', []);
end;// TXMLConstraints.GetMMSelPathFromElement cat:No category

//:-------------------------------------------------------------------
(*: Member:           HasConstraint
 *  Klasse:           TXMLConstraints
 *  Kategorie:        No category 
 *  Argumente:        (aType)
 *
 *  Kurzbeschreibung: Abfrage ob ein gew�nschter Constraint vorhanden ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLConstraints.HasConstraint(aType: TConstraintType): Boolean;
begin
  result := false;

  if assigned(fDOMElement) then begin
    case aType of
      // Ein leeres Element bringt einen Leerstring
      ctDefault: result := fDOMElement.Text <> '';
      ctMinIncl: result := AttributExists(cAtrMinIncl, fDOMElement.attributes);
      ctMinExcl: result := AttributExists(cAtrMinExcl, fDOMElement.attributes);
      ctMaxIncl: result := AttributExists(cAtrMaxIncl, fDOMElement.attributes);
      ctMaxExcl: result := AttributExists(cAtrMaxExcl, fDOMElement.attributes);
      ctEnum   : result := AttributExists(cAtrEnum, fDOMElement.attributes);
    end;// case aType of
  end;// if assigned(fDOMElement) then begin
end;// TXMLConstraints.HasConstraint cat:No category

(*:
 *  Klasse:        TFabMaConfigHelper
 *  Vorg�nger:     TPersistent
 *  Kategorie:     No category
 *  Kurzbeschrieb: Repr�sentiert eine gesamte Fabrik 
 *  Beschreibung:  In dieser Klasse werden die FPattern aus der gesamten Fabrik gesammelt.
                   Einzige Funktion dieser Klasse ist es die FPattern zu verschmelzen und der Applikation
                   ein verschmolzenes FPattern zur Verf�gung zu stellen.
                   
                   Anwendungsbeispiel:
                   
                     procedure TFPatternMergeTest.TestMergeFab;
                     var
                       xFab: TFabMaConfigHelper;
                     begin
                       xFab := TFabMaConfigHelper.Create(TMaConfigReader);
                       try
                         { �ber xFab.FPValue[XPath] kann auf die einzelnen FPattern zugegriffen werden.
                           Beim ersten Zugriff wird das FPattern generiert. }
                         CheckTrue(xFab.FPValue[cXPFP_ClassClearingItem], 'cXPFP_ClassClearingItem');
                         CheckTrue(xFab.FPValue[cXPFP_ShortCountItem], 'cXPFP_ShortCountItem');
                         CheckTrue(xFab.FPValue[cXPFP_FSpectraNormalItem], 'cXPFP_FSpectraNormalItem');
                         CheckFalse(xFab.FPValue[cXPFP_FSpectraBDItem], 'cXPFP_FSpectraBDItem');
                       finally
                         xFab.Free;
                       end;// try finally
                     end;
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      Default wird das maximale FPattern berechnet.
 --------------------------------------------------------------------*)
constructor TFabMaConfigHelper.Create;
begin
  inherited Create;

//: ----------------------------------------------
  mItems := TList.Create;
  FFPElement := CreateDefaultFPattern;
  FFPType := fpMax;

  // Speichert die Referenz auf die zu erzeugende Klasse
  mClassToHold := TMaConfigReader;

//: ----------------------------------------------
  // Debug Settings aus der Registry lesen
  mOutputDebugMsg := GetClassDebug(ClassName);
end;// TFabMaConfigHelper.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aClassToHold)
 *
 *  Kurzbeschreibung: Alternativer Konstruktor
 *  Beschreibung:     
                      Wenn beim Laden der Bauzust�nde von der DB eine spezielle Klasse erzeugt werden soll,
                      kann dies mit diesem Konstruktor mitgeteilt werden. Jede einzelne Klasse wird unter 
                      Verwendung dieser Referenz erzeugt. Soll allerdings auf die einzelne Klasse zugegriffen 
                      werden, dann muss gecastet werden  (oder ein Nachfahre implementiert werden in dem dann die 
                      richtige Klasse zur�ckgegeben wird).
 --------------------------------------------------------------------*)
constructor TFabMaConfigHelper.Create(aClassToHold: TMaConfigReaderClass);
begin
  // Ruft den eigenen offiziellen Konstruktor auf
  Create;
  // Speichert die Referenz auf die zu erzeugende Klasse
  mClassToHold := aClassToHold;
end;// TFabMaConfigHelper.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Einzelne Bauzust�nde wieder freigeben
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TFabMaConfigHelper.Destroy;
begin
  Clear;
  FreeAndNil(mItems);
  inherited;
end;// TFabMaConfigHelper.Destroy cat:List

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        (aItem)
 *
 *  Kurzbeschreibung: F�gt einen Bauzustand f�r eine Maschine zur Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.Add(aItem: TMaConfigReader): Integer;
begin
  // FPattern muss vor dem n�chsten Zugriff neu berechnet werden
  FPCalculated := false;

//: ----------------------------------------------
  result := -1;
  if assigned(mItems) then
    result := mItems.Add(aItem);
end;// TFabMaConfigHelper.Add cat:List

//:-------------------------------------------------------------------
(*: Member:           CalculateFPattern
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aFPType)
 *
 *  Kurzbeschreibung: Berechnet das Verschmolzene FPattern �ber alle Bauzust�nde in der Liste
 *  Beschreibung:     
                      Das FPattern wird mehrstufig verschmolzen. Zuerst wird jeweils die gesamte 
                      Maschine eigenst�ndig berechnet. Danach wird die entsprechende Maschine mit 
                      dem aktuellen FPattern verschmolzen.
 --------------------------------------------------------------------*)
procedure TFabMaConfigHelper.CalculateFPattern(aFPType: TFPType);
var
  i: Integer;
  xMergedFP: TBaseFPHandler;
  xSecondFP: TBaseFPHandler;

  function CreateCalculatedFPattern(aItemIndex: integer): TBaseFPHandler;
  begin
    // Selektiert die gesamte Maschine mit dem gew�nschten FPattern typ
    Items[aItemIndex].FPHandler.SetSpindleRange(-1, -1, aFPType);
    // Berechnet das FPattern f�r die Maschine
    if (Items[aItemIndex].FPHandler is TMaConfigFPHandler) then
      TMaConfigFPHandler(Items[aItemIndex].FPHandler).CalcFPattern(aFPType);
    // Erzeugt einen FPattern Handler zur Weiterverarbeitung
    result := TBaseFPHandler.Create(Items[aItemIndex].FPHandler.FPElement.CloneNode(true));
  end;// function CreateCalculatedFPattern(aItemIndex: integer): TBaseFPHandler;

begin
  (* Immer ausgeben wenn Codesite eingeschaltet, um feststellen zu k�nnen
     ob beim ersten Zugriff auf 'FPValue' neu Berechnet wird. *)
  EnterMethod('TFabMaConfigHelper.CalculateFPattern');

  // Initialisieren
  xMergedFP := nil;

//: ----------------------------------------------
  if Count > 0 then begin
    try
      // Das erste FPattern als Basis f�r die Verschmelzung verwenden
      xMergedFP := CreateCalculatedFPattern(0);

      for i := 1 to Count - 1 do begin
        xSecondFP := CreateCalculatedFPattern(i);
        try
          (* Verschmelzen der beiden FPattern. Als Ziel ist wieder das �rspr�nglich erste
             FPattern angegeben. Am Ende ist in diesem FPattern das verschmolzene Element *)
          xMergedFP.MergeFPattern(xSecondFP, aFPType, xMergedFP);
        finally
          xSecondFP.Free;
        end;// try finally
      end;// for i := 1 to Count - 1 do begin
      // erzeugtes FPattern sichern
      FFPElement := xMergedFP.FPElement;

      if mOutputDebugMsg then
        CodeSite.SendString('MergedFP', FormatXML(FFPElement.xml));
    finally
      xMergedFP.Free;
    end;// try finally
  end;// if Count > 0 then begin
end;// TFabMaConfigHelper.CalculateFPattern cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Bauzust�nde aus der Lsite
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TFabMaConfigHelper.Clear;
begin
  while mItems.Count > 0 do
    delete(0);
end;// TFabMaConfigHelper.Clear cat:List

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht einen einzelen Eintrag aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TFabMaConfigHelper.Delete(aIndex: integer);
begin
  // FPattern muss vor dem n�chsten Zugriff neu berechnet werden
  FPCalculated := false;

//: ----------------------------------------------
  if assigned(mItems) then begin
    if aIndex < mItems.Count then begin
      if assigned(fOnDeleteItem) then
        fOnDeleteItem(Items[aIndex]);
      if fOwnedObjects then
        Items[aIndex].Free;
      mItems.delete(aIndex);
    end;// if aIndex < mItems.Count then begin
  end;// if assigned(mItems) then begin
end;// TFabMaConfigHelper.Delete cat:List

//:-------------------------------------------------------------------
(*: Member:           GetAvailableSensingHeadClasses
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.GetAvailableSensingHeadClasses: TSensingHeadClassSet;
var
  i: Integer;
begin
  // Maschinen laden, sollte dies noch nicht geschehen sein
  if not(fMachinesLoaded) then
    LoadMachines;

  result := [];
  for i := 0 to Count - 1 do
    result := result + Items[i].AvailableSensingHeadClasses;
end;// TFabMaConfigHelper.GetAvailableSensingHeadClasses cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetCount
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Anzahl der Eintr�ge in der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.GetCount: Integer;
begin
  result := 0;

  if assigned(mItems) then
    result := mItems.Count;
end;// TFabMaConfigHelper.GetCount cat:List

//:-------------------------------------------------------------------
(*: Member:           GetFPElement
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert beim ersten Zugriff auf das FPElement das selbige zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.GetFPElement: IXMLDOMElement;
begin
  // Initialisieren
  if not(fMachinesLoaded) then
    LoadMachines;

  // FPattern f�r die Fabrik bilden
  if not(FPCalculated) then
    CalculateFPattern(FPType);

//: ----------------------------------------------
  result := FFPElement;
end;// TFabMaConfigHelper.GetFPElement cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetFPValue
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath)
 *
 *  Kurzbeschreibung: Liest den Wert eines FPattern Elements
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.GetFPValue(aSelPath: string): Boolean;
var
  xElement: IXMLDOMElement;
  xNode: IXMLDOMNode;
begin
  // Zugriff auf ein Element des FPattern
  result := false;

  if assigned(FPElement) then begin
    xNode := FPElement.SelectSingleNode(RemoveXPRoot(cXPFPatternNode, aSelPath));
    Supports(xNode, IXMLDOMElement, xElement);
    if assigned(xElement) then
      result := GetElementValueDef(xElement, result);
  end;// if assigned(FPElement) then begin
end;// TFabMaConfigHelper.GetFPValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetItems
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        List 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Liefert ein MaConfig f�r eine einzelne Maschine
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TFabMaConfigHelper.GetItems(aIndex: Integer): TMaConfigReader;
begin
  result := nil;
  if assigned(mItems) then
    if mItems.Count > aIndex then
      result := TMaConfigReader(mItems.Items[aIndex]);
end;// TFabMaConfigHelper.GetItems cat:List

//:-------------------------------------------------------------------
(*: Member:           LoadMachines
 *  Klasse:           TFabMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�dt alle verf�gbaren Bauzust�nde aus der Datenbank
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TFabMaConfigHelper.LoadMachines;
var
  xMaConfigReader: TMaConfigReader;

  const
    cLoadMaConfigFromDB = 'SELECT c_machine_id' +
                          ' FROM t_machine' +
                          ' WHERE not(c_magroup_config_id IS NULL)';

begin
  Clear;
  fOwnedObjects := true;
  fMachinesLoaded := false;

//: ----------------------------------------------
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cLoadMaConfigFromDB;
      open;
      while not(EOF) do begin
        xMaConfigReader := mClassToHold.Create;
        try
          // Wenn das laden des MaConfig XML schief geht, die Maschine nicht zur Liste hinzuf�gen
          if xMaConfigReader.LoadMachConfigFromDB(FieldByName('c_machine_id').AsInteger)then begin
            Add(xMaConfigReader);
            if mOutputDebugMsg then
              CodeSite.SendString(Format('DOM (MaID = %d)', [FieldByName('c_machine_id').AsInteger]), FormatXML(xMaConfigReader.DOM.SelectNodes(cXPGroupNode)));
          end else begin
            CodeSite.SendError('TFabMaConfigHelper.LoadMachines: LoadMaConfig failed for MachineID: ' + FieldByName('c_machine_id').AsString);
            FreeAndNil(xMaConfigReader);
          end;// if xMaConfigReader.LoadMachConfigFromDB(FieldByName('c_machine_id').AsInteger)then begin
          // Mindestens eine Maschine ist geladen worden
          fMachinesLoaded := true;
        except
          on e: Exception do begin
            CodeSite.SendError(Format('TFabMaConfigHelper.LoadMachines: Exception for MachineID: %s (%s)',
                                      [FieldByName('c_machine_id').AsString, e.Message]));
            FreeAndNil(xMaConfigReader);
          end;// on e: Exception do begin
        end;
        Next;
      end; //while
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// TFabMaConfigHelper.LoadMachines cat:No category

(*:
 *  Klasse:        TXMLMaConfigHelper
 *  Vorg�nger:     TPersistent
 *  Kategorie:     No category
 *  Kurzbeschrieb: Kapselung f�r den Bauzustand und die Defaults einer einzelnen Maschine 
 *  Beschreibung:  Diese Klasse dient zur generellen Bearbeitung und Kapselung der verschiedenen
                   Aktionen die mit dem Bauzustand zusammenh�ngen. Die Klasse wird von den NetHandlern
                   wie auch vom GUI (Assignment und MaConfig) verwendet. 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Debugausgabe initialisieren
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TXMLMaConfigHelper.Create;
begin
  inherited;

  // Debug Settings aus der Registry lesen
  mOutputDebugMsg := GetClassDebug(ClassName);
end;// TXMLMaConfigHelper.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Aufr�umen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TXMLMaConfigHelper.Destroy;
begin
  FDOM := nil;

  inherited;
end;// TXMLMaConfigHelper.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddGroup
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroup)
 *
 *  Kurzbeschreibung: F�gt eine Zus�tzliche Gruppe zum DOM hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.AddGroup(aGroup: IXMLDOMNode): Integer;
begin
  Assert(IsInterface(aGroup, IXMLDOMElement), 'Group is not Element');
  result := -1;

  if assigned(aGroup) then begin
    FDOM.documentElement.firstChild.insertBefore(aGroup, Group[GroupCount -1].nextSibling);
    result := GroupCount -1;
  end;// if assigned(aGroup) then begin
end;// TXMLMaConfigHelper.AddGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Erm�glicht das Kopieren von Bauzust�nden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.Assign(Source: TPersistent);
var
  xSource: TXMLMaConfigHelper;
begin
  if Source is TXMLMaConfigHelper then begin
    xSource := TXMLMaConfigHelper(Source);
    DOM := xSource.DOM;
  end else begin
    inherited Assign(Source);
  end;
end;// TXMLMaConfigHelper.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildFPatternFromConfig
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        FPattern 
 *  Argumente:        (aNetTyp)
 *
 *  Kurzbeschreibung: Stellt das FPattern (TXN, WSC) aus verschiedenen Informationen aus dem DOM zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.BuildFPatternFromConfig(aNetTyp: TNetTyp);
var
  i: Integer;
  xFPattern: TMaConfigFPBuilder;
begin
  xFPattern := TMaConfigFPBuilder.Create;
  try
    xFPattern.MachineElement := Machine;
    for i := 0 to GroupCount - 1 do begin
      xFPattern.GroupElement := Group[i];
      xFPattern.BuildFPatternFromConfig(aNetTyp, msMaConfig);
    end;// for i := 0 to GroupCount - 1 do begin
  finally
    xFPattern.Free;
  end;// try finally
end;// TXMLMaConfigHelper.BuildFPatternFromConfig cat:FPattern

//:-------------------------------------------------------------------
(*: Member:           BuildMaConfigFromHandler
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aMaConfXML, aDefaultsXML, aNetTyp, aMapSection)
 *
 *  Kurzbeschreibung: Wandelt den String der vom Handler zusammengesetzt wird in die endg�ltige Version
 *  Beschreibung:     
                        Tritt w�hrend der Transformation ein Fehler auf, wird dieser ins EWventlog geschrieben,
                        und es wird ein leerer String zur�ckgegeben
                      
                        Ein (gek�rztes) Endresultat k�nnte wie folgt aussehen:
                        <LoepfeBody>
                          <Group>
                            <AWEType/>
                            <ReinVer>50</ReinVer>
                            <SensingHead>shTKZenitF</SensingHead>
                            <FPattern/>
                            <ProdGrpID>0</ProdGrpID>
                            <PilotSpindles>1</PilotSpindles>
                            <Group>1</Group>
                            <SpindleFrom>1</SpindleFrom>
                            <SpindleTo>1</SpindleTo>
                            <FDispMode>0</FDispMode>
                          </Group>
                          <Group>
                            <AWEType/>
                                .
                                .
                                .
                          </Group>
                          <Machine>
                            <YMSwVersion>V 6.12</YMSwVersion>
                            <SWOption>3</SWOption>
                              .
                              .
                              .
                          </Machine>
                        </LoepfeBody>
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.BuildMaConfigFromHandler(const aMaConfXML, aDefaultsXML: string; aNetTyp: TNetTyp;
  aMapSection: TMapSection): string;
var
  xMachNode: IXMLDOMNode;
  i: Integer;
  xNodeList: IXMLDOMNodeList;
  xConfigNode: IXMLDOMNode;
  xNewConfigNode: IXMLDOMNode;
  xMsg: string;
  xDefaultsDOM: DOMDocumentMM;
  xNewDefaultElement: IXMLDOMElement;
begin
  // Im Fehlerfall wird ein leerer String zur�ckgegeben
  Result := '';
  xMsg := '';
  try
    if mOutputDebugMsg then begin
      EnterMethod('TXMLMaConfigHelper.BuildMaConfigFromHandler');
      CodeSite.SendString('BZ von Handler', FormatXML(aMaConfXML));
      CodeSite.SendString('Defaults von Handler', FormatXML(aDefaultsXML));
    end;// if mOutputDebugMsg then

    // Zuerst einmal den String in ein DOM laden
    LoadDOM(aMaConfXML);
    (* Der Config Node wird sp�ter entfernt. Vorher werden die entsprechenden
       Elemente (Group und Machine) rausgeholt und vor dem Config Node plaziert *)
    xConfigNode := FDOM.SelectSingleNode(cXPConfigNode);
    if assigned(xConfigNode) then begin
      xNewConfigNode := FDOM.CreateElement(cConfigName);
      // Zuerst werden alle 'Group' Elemente vor den ConfigNode verschoben
      xNodeList := FDOM.SelectNodes(cXPGroupNode);
      for i := 0 to xNodeList.Length - 1 do
        xNewConfigNode.AppendChild(xNodeList.Item[i]);
      if mOutputDebugMsg then
        CodeSite.SendString('Step 1 (NewConfigElement)', FormatXML(xNewConfigNode as IXMLDOMElement));

      // Ersten Maschinen Knoten selektieren und vor den Config Knoten verschieben
      xMachNode := FDOM.selectSingleNode(cXPMachineNode);
      if assigned(xMachNode) then
        xNewConfigNode.AppendChild(xMachNode);

      if mOutputDebugMsg then
        CodeSite.SendString('Step 2 (NewConfigElement)', FormatXML(xNewConfigNode as IXMLDOMElement));
      if mOutputDebugMsg then
        CodeSite.SendString('Step 2 (DOM)', FormatXML(FDOM));

      // Alle Config Knoten (sollten jetzt leer sein) slektieren und dann l�schen
      xNodeList := FDOM.SelectNodes(cXPConfigNode);
      for i := 0 to xNodeList.Length - 1 do
        xNodeList.Item[i].parentNode.removeChild(xNodeList.Item[i]);

      if mOutputDebugMsg then
        CodeSite.SendString('Step 3', FormatXML(FDOM));

      // Neuen Config Node zum Dokument hinzuf�gen (Funktioniert auch, wenn es kein FirstChild gibt)
      FDOM.DocumentElement.InsertBefore(xNewConfigNode, FDOM.DocumentElement.firstChild);

      if mOutputDebugMsg then
        CodeSite.SendString('Step 4', FormatXML(FDOM));

      // Gruppen zusammenfassen, wenn die Konfiguration identisch ist
      ConcentrateGroups;

      (* Defaults in das MaConfig XML einf�gen.
         Dazu muss das Defaults XML zuerst ver�ndert werden. *)
      xDefaultsDOM := XMLStreamToDOM(aDefaultsXML);
      if assigned(xDefaultsDOM) then begin
        if assigned(xDefaultsDOM.DocumentElement) then begin
          xNewDefaultElement := FDOM.CreateElement(cXMLDefaultsElementName);
          while xDefaultsDOM.DocumentElement.childNodes.Length > 0 do
            xNewDefaultElement.appendChild(xDefaultsDOM.DocumentElement.childNodes[0]);
          FDOM.DocumentElement.appendChild(xNewDefaultElement);
        end;// if assigned(xDefaultsDOM.DocumentElement) then begin
      end;// if assigned(xDefaultsDOM) then begin

      // Ergebnis der Transformation
      result := FDOM.xml;

      if mOutputDebugMsg then
        CodeSite.SendString('MaConfig XML', FormatXML(FDOM));

    end else begin
      xMsg := 'Invalid Config Node';
    end;// if assigned(xConfigNode) then begin
  except
    on e: Exception do begin
      result := '';
      xMsg := e. Message;
    end;// on e: Exception do begin
  end;// try except

  // Wenn es irgendwo eine Fehlermeldung gab, dann diese als Warnung ins Event Log schreiben
  if xMsg > '' then
    WriteToEventLog(' (TXMLMaConfigHelper.BuildMaConfigFromHandler)', xMsg, etWarning);
end;// TXMLMaConfigHelper.BuildMaConfigFromHandler cat:No category

//:-------------------------------------------------------------------
(*: Member:           ConcentrateGroups
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Fasst die einzelnen Gruppen nach M�glichkeit zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.ConcentrateGroups;
var
  xCurrentGroupNode: IXMLDOMNode;
  xCurrentGroupIndex: Integer;

  (*---------------------------------------------------------
    Fasst zwei Gruppen zusammen, die mit dem Spindelbereich anschliessend sind
  ----------------------------------------------------------*)
  function ConcentrateTwoGroups(aOriginNode: IXMLDOMNode): boolean;
  var
    xCurrentGroupTo: Integer;
    xFollowingGroupNode: IXMLDOMNode;
  begin
    result := false;
    if assigned(aOriginNode) then begin
      xCurrentGroupTo := GroupValueDef[xCurrentGroupIndex, cXPSpindleToItem, -1];
      // Sucht eine Gruppe mit anschliessendem Spindelbereich
      xFollowingGroupNode := aOriginNode.ParentNode.SelectSingleNode(Format('Group[SpindleFrom=%d]', [xCurrentGroupTo + 1]));

      if SameGroup(aOriginNode, xFollowingGroupNode) then begin
        // Wenn die beiden Gruppen gleich sind neuen Endbereich eintragen
        GroupValue[xCurrentGroupIndex, cXPSpindleToItem] := GetElementValueDef(xFollowingGroupNode.selectSingleNode(cSpindleToName), xCurrentGroupTo);
        // ... und das urspr�ngliche Element l�schen
        xFollowingGroupNode.parentNode.removeChild(xFollowingGroupNode);
        if mOutputDebugMsg then
          CodeSite.SendString(classname + ': Zusammengefasst', FormatXML(Group[xCurrentGroupIndex]));
        // Die Gruppen wurden zusammengefasst (result := true)
        result := true;
      end;// if SameGroup(aOriginNode, xFollowingGroupNode) then begin
    end;// if assigned(aOriginNode) then begin
  end;// function ConcentrateTwoGroups(aOriginNode: IXMLDOMNode): boolean;

begin
  if mOutputDebugMsg then begin
    EnterMethod('TXMLMaConfigHelper.ConcentrateGroups');
    CodeSite.SendString(classname + ': DOM Begin', FormatXML(FDOM));
  end;// if mOutputDebugMsg then begin

//: ----------------------------------------------
  // Gruppen zusammenfassen
  xCurrentGroupIndex := 0;

  (* Solange durch alle Gruppen bis die letzte Gruppe untersucht ist.
     Die Anzahl der Gruppen kann sich �ndern, wenn zwei Gruppen mit dem
     Spindelbereich angrenzend sind und das selbe FPattern haben *)
  while xCurrentGroupIndex < GroupCount do begin
    xCurrentGroupNode := Group[xCurrentGroupIndex];
    if ConcentrateTwoGroups(xCurrentGroupNode) then
      xCurrentGroupIndex := 0
    else
      inc(xCurrentGroupIndex);
  end;// while xCurrentGroup < GroupCount do begin

//: ----------------------------------------------
  // Aufsteigend nach Spindelrange sortieren
  SortGroups;

//: ----------------------------------------------
  if mOutputDebugMsg then
    CodeSite.SendString(classname + ': DOM End', FormatXML(FDOM));
end;// TXMLMaConfigHelper.ConcentrateGroups cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteGroup
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht eine Gruppe aus dem DOM
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.DeleteGroup(aIndex: integer);
var
  xGroupNode: IXMLDOMNode;
begin
  xGroupNode := Group[aIndex];
  if assigned(xGroupNode) then
    xGroupNode.parentNode.removeChild(xGroupNode);
end;// TXMLMaConfigHelper.DeleteGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           DOMChanged
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein neues DOM zugewiesen oder geladen wird (string)
 *  Beschreibung:     
                      Diese Methode kann in einem Nachkommen �berschrieben werden, um z.B. das Modell
                      zu benachrichtigen, dass sich der zugrunde liegende Bauzustand ge�ndert hat.
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.DOMChanged;
begin
  // Default Implementierung
end;// TXMLMaConfigHelper.DOMChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           FirstGroupSpindleFromSpindle
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSpindle, aAutoIncrement)
 *
 *  Kurzbeschreibung: Gibt die erste Spindel aus der Gruppe in der sich die gew�nschte Spindel befindet
 *  Beschreibung:     
                      Wenn die Spindel 5 gesucht wird und die zweite Gruppe als Spindel 3 - 7 definiert ist,
                      dann wird als R�ckgabewert die Zahl 3 eingetragen. 
                      Die Spindel 3 ist in diesem Fall die erste Spindel der Gruppe.
                      
                      Das Argument 'aAutoIncrement' legt fest, ob die Spindelposition automatisch erh�ht
                      wird, wenn die gesuchte Spindel in keiner Sektion ist.
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.FirstGroupSpindleFromSpindle(aSpindle: integer; aAutoIncrement: boolean): Integer;
var
  xSpindleNode: IXMLDOMNode;
begin
  result := 0;
  xSpindleNode := nil;

  if (assigned(FDOM) and (aSpindle <= cMaxSpindeln)) then begin
    (* In einer Schleife abfragen, da eine Spindel in einem nicht zugewiesenen Bereich
       liegen k�nnte (L�cher). *)
    repeat
      // Selektiert die enthaltende Gruppe
      xSpindleNode := GetGroupNodeFromSpindle(aSpindle);
      if assigned(xSpindleNode) then
        xSpindleNode := xSpindleNode.SelectSingleNode(cSpindleFromName);

      if not(assigned(xSpindleNode)) then
        inc(aSpindle);
    // Weitermachen bis eine Spindel gefunden wird, oder bei cMaxSpindeln angelangt
    until (assigned(xSpindleNode)) or (aSpindle >= cMaxSpindeln) or (not(aAutoIncrement));

    result := GetElementValueDef(xSpindleNode, 0);
  end;// if (assigned(FDOM) and (aSpindle <= cMaxSpindeln)) then begin
end;// TXMLMaConfigHelper.FirstGroupSpindleFromSpindle cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAvailable
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Pr�ft ob ein g�ltiges DOM f�r einen Bauzustand verf�gbar ist.
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetAvailable: Boolean;
begin
  result := assigned(fDOM);
end;// TXMLMaConfigHelper.GetAvailable cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroup
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r das Group[] Property.
 *  Beschreibung:     
                      Es wird das XMLElement mit der entsprechenden Gruppe zur�ckgegeben.
                      Ist aIndex gr�sser als die Anzahl der verf�gbaren Gruppen, dann wird nil zur�ckgegeben
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroup(aIndex: Integer): IXMLDOMElement;
var
  xGroupNode: IXMLDOMNode;
begin
  Assert(assigned(DOM), 'Der MaConfigHelper muss ein g�ltiges DOM aufweisen');

//: ----------------------------------------------
  Result := nil;
  if aIndex < GroupCount then begin
    xGroupNode := FDOM.SelectSingleNode(Format('%s[%d]', [GetGroupXP, aIndex + 1]));
    Supports(xGroupNode, IXMLDOMElement, result);
  end;// if aIndex < GroupCount then begin
end;// TXMLMaConfigHelper.GetGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupCount
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung:  Gibt die Anzahl der verf�gbaren Gruppen zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupCount: Integer;
begin
  Result := 0;
  if assigned(FDOM) then
    result := FDOM.SelectNodes(cXPGroupNode).Length;
end;// TXMLMaConfigHelper.GetGroupCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupNode
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroupIndex, aSelPath)
 *
 *  Kurzbeschreibung: Liefert ein Element das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupNode(aGroupIndex: Integer; aSelPath: string): IXMLDOMElement;
var
  xResultNode: IXMLDOMNode;
  xXPath: string;
  xGroupNode: IXMLDOMNode;
begin
  Result := nil;

  xGroupNode := Group[aGroupIndex];
  if assigned(xGroupNode) then begin
    xXPath := MMXPToGroupXP(aSelPath);
    xResultNode := xGroupNode.selectSingleNode(xXPath);
    Supports(xResultNode, IXMLDOMElement, result);
  end;// if assigned(xGroupNode) then begin
end;// TXMLMaConfigHelper.GetGroupNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupNodeFromSpindle
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSpindle)
 *
 *  Kurzbeschreibung: Liefert das Gruppenelement f�r einen Spindelrange
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupNodeFromSpindle(aSpindle: integer): IXMLDOMNode;
var
  xXPGetGroupFromSpindle: string;
begin
  (* Generiert den XPath der ben�tigt wird um die Gruppe zu selektieren
     in der die gew�nschte Spindel liegt. *)
  xXPGetGroupFromSpindle := cXPGroupNode + Format('[(.//%s <= %d) and (.//%s >= %1:d)]', [cSpindleFromName, aSpindle, cSpindleToName]);
  result := FDOM.SelectSingleNode(xXPGetGroupFromSpindle);
end;// TXMLMaConfigHelper.GetGroupNodeFromSpindle cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupValue
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroupIndex, aSelPath)
 *
 *  Kurzbeschreibung: Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupValue(aGroupIndex: Integer; aSelPath: string): Variant;
var
  xElement: IXMLDOMElement;
begin
  xElement := GroupNode[aGroupIndex, aSelPath];
  result := NULL;
  if assigned(xElement) then
    result := GetElementValueDef(xElement, NULL);
end;// TXMLMaConfigHelper.GetGroupValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupValueDef
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroupIndex, aSelPath, aDefault)
 *
 *  Kurzbeschreibung: Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupValueDef(aGroupIndex: Integer; aSelPath: string; aDefault: variant): Variant;
var
  xElement: IXMLDOMElement;
begin
  result := aDefault;
  xElement := GroupNode[aGroupIndex, aSelPath];
  if assigned(xElement) then
    result := GetElementValueDef(xElement, aDefault);

  // Pr�ft, ob der Default ein nummerischer Wert ist und liefert den Variant vom richtigen Typ
  result := NumVariantToFloat(result, aDefault);
end;// TXMLMaConfigHelper.GetGroupValueDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetGroupXP
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den absoluten Pfad zur Gruppe zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetGroupXP: string;
begin
  //result := '/' + cXMLLoepfeBody + XPFromRight(cXPGroupNode, 1);
  result := cXPGroupNode;
end;// TXMLMaConfigHelper.GetGroupXP cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetLastSpindle
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die letzte erfasse Spindel der Maschine
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetLastSpindle: Integer;
var
  xFirstSpindle: Integer;
begin
  // Holt den gesamten Range. Es wird nur die letzte Spindle ben�tigt
  GetMachSpindleRange(xFirstSpindle, result);
end;// TXMLMaConfigHelper.GetLastSpindle cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachine
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r das Group[] Property.
 *  Beschreibung:     
                      Es wird das XMLElement mit der entsprechenden Gruppe zur�ckgegeben.
                      Ist aIndex gr�sser als die Anzahl der verf�gbaren Gruppen, dann wird nil zur�ckgegeben
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetMachine: IXMLDOMElement;
var
  xMachNode: IXMLDOMNode;
begin
  Assert(assigned(DOM), 'Der MaConfigHelper muss ein g�ltiges DOM aufweisen');

//: ----------------------------------------------
  Result := nil;
  xMachNode := FDOM.SelectSingleNode(GetMachXP);
  Supports(xMachNode, IXMLDOMElement, result);
end;// TXMLMaConfigHelper.GetMachine cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachNode
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath)
 *
 *  Kurzbeschreibung: Liefert ein Element das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetMachNode(aSelPath: string): IXMLDOMElement;
var
  xMachNode: IXMLDOMNode;
begin
  Result := nil;

  try   //Nue: 18.1.07
    xMachNode := Machine;
  except
    xMachNode := nil;
  end;

  if assigned(xMachNode) then begin
    xMachNode := xMachNode.selectSingleNode(MMXPToMachXP(aSelPath));
    Supports(xMachNode, IXMLDOMElement, result);
  end;// if assigned(xMachNode) then begin
end;// TXMLMaConfigHelper.GetMachNode cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachSpindleRange
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSpdlFrom, aSpdlTo)
 *
 *  Kurzbeschreibung: Gibt die erste und die letzte Spindel der Maschine zur�ck
 *  Beschreibung:     
                      Die Funktion selektiert zuerst alle Gruppen und gibt dann die jeweils erste oder letzte Spindel
                      in einer IXMLDOMNodeList zur�ck. Diese Liste muss dann noch durchsucht werden.
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.GetMachSpindleRange(out aSpdlFrom: integer; out aSpdlTo: integer);
var
  xNodeList: IXMLDOMNodeList;
  i: Integer;
  xValue: Integer;
begin
  // initialisiern
  aSpdlFrom := 0;
  aSpdlTo := 0;

  if assigned(FDom) then begin

//: ----------------------------------------------
    // erste Spindel
    aSpdlFrom := MAXINT;
    // Alle SpindelFrom Elemente selektieren
    xNodeList := FDOM.SelectNodes(MMXPToAbsoluteXP(cXPSpindleFromItem));
    // Kleinste Spindelnummer zur�ckgeben
    for i := 0 to xNodeList.Length - 1 do begin
      xValue := GetElementValueDef(xNodeList.item[i], aSpdlFrom);
      if  xValue < aSpdlFrom then
        aSpdlFrom := xValue;
    end;// for i := 0 to xNodeList.Length - 1 do begin

    // Sollten keine Gruppen Elemente vorhanden sein, dann 0 zur�ckgeben
    if aSpdlFrom = MAXINT then
      aSpdlFrom := 0;

//: ----------------------------------------------
    // letzte Spindel
    aSpdlTo := 0;
    // Alle SpindelFrom Elemente selektieren
    xNodeList := FDOM.SelectNodes(MMXPToAbsoluteXP(cXPSpindleToItem));
    // gr�sste Spindelnummer zur�ckgeben
    for i := 0 to xNodeList.Length - 1 do begin
      xValue := GetElementValueDef(xNodeList.item[i], aSpdlTo);
      if  xValue > aSpdlTo then
        aSpdlTo := xValue;
    end;// for i := 0 to xNodeList.Length - 1 do begin

//: ----------------------------------------------
  end;// if assigned(FDom) then begin
end;// TXMLMaConfigHelper.GetMachSpindleRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachValue
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath)
 *
 *  Kurzbeschreibung: Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetMachValue(aSelPath: string): Variant;
var
  xElement: IXMLDOMElement;
begin
  xElement := MachNode[aSelPath];
  result := NULL;
  if assigned(xElement) then
    result := GetElementValueDef(xElement, NULL);
end;// TXMLMaConfigHelper.GetMachValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachValueDef
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath, aDefault)
 *
 *  Kurzbeschreibung: Liefert den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetMachValueDef(aSelPath: string; aDefault: variant): Variant;
var
  xElement: IXMLDOMElement;
begin
  result := aDefault;
  xElement := MachNode[aSelPath];
  if assigned(xElement) then
    result := GetElementValueDef(xElement, aDefault);

  // Pr�ft, ob der Default ein nummerischer Wert ist und liefert den Variant vom richtigen Typ
  result := NumVariantToFloat(result, aDefault);
end;// TXMLMaConfigHelper.GetMachValueDef cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMachXP
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt den absoluten Pfad zur Gruppe zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GetMachXP: string;
begin
  //result := '/' + cXMLLoepfeBody + XPFromRight(cXPMachineNode, 1);
  result := cXPMachineNode;
end;// TXMLMaConfigHelper.GetMachXP cat:No category

//:-------------------------------------------------------------------
(*: Member:           GroupIndexFromRange
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSpindleFrom, aSpindleTo, aGroupIndex)
 *
 *  Kurzbeschreibung: Liefert den Index der Gruppe in welcher der gew�nschte Spulstellen bereich liegt
 *  Beschreibung:     
                      Ist der Bereich Gruppen �bergreifend, dann ist der Aufruf nicht
                      erfolgreich (result = false)
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.GroupIndexFromRange(aSpindleFrom, aSpindleTo: integer; out aGroupIndex: integer): Boolean;
var
  xFirstGroupSpindle: Integer;
  i: Integer;
  xFound: Boolean;
begin
  aGroupIndex := -1;

//: ----------------------------------------------
  // Zuerst feststellen ob in der selben Sektion
  result := IsSameSection(aSpindleFrom, aSpindleTo);

  // Wenn in der selben Sektion, dann den Index herausfinden
  if result then begin
    xFirstGroupSpindle := FirstGroupSpindleFromSpindle(aSpindleFrom, false);
    i := 0;
    xFound := false;
    // Alle Gruppen durchsuchen, bis die richtige Gruppe gefunden wird
    while (i < GroupCount) and(not(xFound)) do begin
      xFound := (GroupValue[i, cXPSpindleFromItem] = xFirstGroupSpindle);
      if xFound then
        aGroupIndex := i;
      inc(i);
    end;// while (i < GroupCount) and(not(xFound)) do begin
  end;// if result then begin
end;// TXMLMaConfigHelper.GroupIndexFromRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           IsSameSection
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aFirstSpindle, aSecondSpindle)
 *
 *  Kurzbeschreibung: True, wenn beide Spindeln in der selben Sektion liegen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.IsSameSection(aFirstSpindle, aSecondSpindle: integer): Boolean;
var
  xFirstGroup: Integer;
  xSecondGroup: Integer;
begin
  // Zu jeder Sindel das entsprechende Gruppen Element selektieren
  xFirstGroup := FirstGroupSpindleFromSpindle(aFirstSpindle, false);
  xSecondGroup := FirstGroupSpindleFromSpindle(aSecondSpindle, false);

  result := false;
  // Die Spindeln sind in der selben Gruppe, wenn beide Gruppenelemente gleich sind
  if (xFirstGroup > 0) and (xSecondGroup > 0) then
    result := (xFirstGroup = xSecondGroup);
end;// TXMLMaConfigHelper.IsSameSection cat:No category

//:-------------------------------------------------------------------
(*: Member:           LastGroupSpindleFromSpindle
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSpindle, aAutoDecrement)
 *
 *  Kurzbeschreibung: Ermittelt die letzte Spindel der Gruppe in der die gesuchte Spindel enthalten ist.
 *  Beschreibung:     
                      Unter Umst�nden ist eine Spindel nicht in einer Gruppe enthalten (z.B.: weil sie defekt ist).
                      In diesem Fall wird versucht die letzte Spindel der vorhergehenden Gruppe zu finden.
                      
                      Das Argument 'aAutoDecrement' legt fest, ob die Spindelposition automatisch verringert
                      wird, wenn die gesuchte Spindel in keiner Sektion ist.
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.LastGroupSpindleFromSpindle(aSpindle: integer; aAutoDecrement: boolean): Integer;
var
  xSpindleNode: IXMLDOMNode;
begin
  result := 0;
  xSpindleNode := nil;

  if (assigned(FDOM) and (aSpindle > 0)) then begin
    (* In einer Schleife abfragen, da eine Spindel in einem nicht zugewiesenen Bereich
       liegen k�nnte (L�cher). *)
    repeat
      // Selektiert die enthaltende Gruppe
      xSpindleNode := GetGroupNodeFromSpindle(aSpindle);
      if assigned(xSpindleNode) then
        xSpindleNode := xSpindleNode.SelectSingleNode(cSpindleToName);

      if not(assigned(xSpindleNode)) then
        dec(aSpindle);
    // Weitermachen bis eine Spindel gefunden wird, oder bei der 0 angelangt
    until (assigned(xSpindleNode)) or (aSpindle <= 0) or (not(aAutoDecrement));

    result := GetElementValueDef(xSpindleNode, 0);
  end;// if (assigned(FDOM) and (aSpindle > 0)) then begin
end;// TXMLMaConfigHelper.LastGroupSpindleFromSpindle cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadDOM
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aDOM)
 *
 *  Kurzbeschreibung: Parst den String und l�dt ihn in das interne DOM Bei einem Fehler wird false zur�ckgegeben
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.LoadDOM(aDOM: string): Boolean;
begin
  FDOM := nil;
  try
    FDOM := XMLStreamToDOM(aDOM);
    result := true;
    DOMChanged;
  except
    result := false;
  end;// try except
end;// TXMLMaConfigHelper.LoadDOM cat:No category

//:-------------------------------------------------------------------
(*: Member:           MMXPToAbsoluteXP
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aMMPath)
 *
 *  Kurzbeschreibung: Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsgruppen Pfad um
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.MMXPToAbsoluteXP(aMMPath: string): string;

  function GetPath(aCopyIndex: integer): string;
  begin
    result := '';
    if Length(aMMPath) > aCopyIndex then
      result := copy(aMMPath, aCopyIndex, MAXINT);
  end;// function GetPath(aCopyIndex: integer): string;

begin
  result := aMMPath;
end;// TXMLMaConfigHelper.MMXPToAbsoluteXP cat:No category

//:-------------------------------------------------------------------
(*: Member:           MMXPToGroupXP
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aMMPath)
 *
 *  Kurzbeschreibung: Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsgruppen Pfad um
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.MMXPToGroupXP(aMMPath: string): string;
begin
  result := RemoveXPRoot(cXPGroupNode, aMMPath);
  //result := RemoveXPRoot(cXPGroupParamsNode, result);
  // Es wird immer ein relativer Pfad zur�ckgegeben (Bsp: './SpindleFrom')
  result := './' + result;
end;// TXMLMaConfigHelper.MMXPToGroupXP cat:No category

//:-------------------------------------------------------------------
(*: Member:           MMXPToMachXP
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aMMPath)
 *
 *  Kurzbeschreibung: Stellt einen Selektionspfad der f�r MMXML ben�tigt wurde auf einen Bauzustandsmaschinen Pfad um
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.MMXPToMachXP(aMMPath: string): string;
begin
  result := RemoveXPRoot(cXPMachineNode, aMMPath);
  //result := RemoveXPRoot(cXPMachineParamsNode, result);

  // Es wird immer ein relativer Pfad zur�ckgegeben (Bsp: './SpindleFrom')
  result := './' + result;
end;// TXMLMaConfigHelper.MMXPToMachXP cat:No category

//:-------------------------------------------------------------------
(*: Member:           SameGroup
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroup1, aGroup2)
 *
 *  Kurzbeschreibung: �berpr�ft zwei Gruppen auf identische Einstellungen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TXMLMaConfigHelper.SameGroup(aGroup1, aGroup2: IXMLDOMNode): Boolean;
var
  xFP2: IXMLDOMNode;
  xFP1: IXMLDOMNode;
  xSH1: string;
  xSH2: string;
begin
  Result := false;
  if assigned(aGroup1) and assigned(aGroup2) then begin
    if mOutputDebugMsg then begin
      CodeSite.SendString(classname + ': Gruppe 1', FormatXML(aGroup1 as IXMLDOMElement));
      CodeSite.SendString(classname + ': Gruppe 2', FormatXML(aGroup2 as IXMLDOMElement));
    end;// if mOutputDebugMsg then begin

      // Zuerst den Tastkopf vergleichen
    xSH1 := GetElementValueDef(aGroup1.SelectSingleNode(RemoveXPRoot(cXPGroupNode, cXPSensingHeadItem)), 'GP1');
    xSH2 := GetElementValueDef(aGroup2.SelectSingleNode(RemoveXPRoot(cXPGroupNode, cXPSensingHeadItem)), 'GP2');
    result := (xSH1 = xSH2);

    // Danach das FPattern
    if result then begin
      xFP1 := aGroup1.selectSingleNode(cFPatternName);
      xFP2 := aGroup2.selectSingleNode(cFPatternName);

      if assigned(xFP1) and assigned(xFP2) then
        // Wenn beide Tastk�pfe identisch sind, dann das FPattern vergleichen
        result := SameNodeText(xFP1, xFP2);
    end;// if result then begin
  end;// if assigned(aGroup1) and assigned(aGroup2) then begin
end;// TXMLMaConfigHelper.SameGroup cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetDOM
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Bei einer �nderung soll ein Event ausgel�st werden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.SetDOM(const Value: DOMDocumentMM);
begin
  if fDOM <> Value then
  begin

//: ----------------------------------------------
    fDOM := Value;

//: ----------------------------------------------
    DOMChanged;
    if mOutputDebugMsg then
      CodeSite.SendString('MaConfigHelper DOM Changed (New DOM)', FormatXML(fDOM));

//: ----------------------------------------------
  end;
end;// TXMLMaConfigHelper.SetDOM cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetGroupValue
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aGroupIndex, aSelPath, Value)
 *
 *  Kurzbeschreibung: Setzt den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.SetGroupValue(aGroupIndex: Integer; aSelPath: string; const Value: Variant);
var
  xElement: IXMLDOMElement;
begin
  xElement := GroupNode[aGroupIndex, aSelPath];

  if not(assigned(xElement)) then begin
    if assigned(Group[aGroupIndex]) then
      xElement := GetCreateElement(RemoveXPRoot(cXPGroupNode, aSelPath), DOM, Group[aGroupIndex]);
  end;// if not(assigned(xElement)) then begin

  if assigned(xElement) then begin
    if VarType(Value) = varBoolean then
      xElement.Text := GetEnumName(TypeInfo(boolean), ord(boolean(Value)))
    else
      xElement.Text := FloatVariantToString(Value);
  end;// if assigned(xElement) then begin
end;// TXMLMaConfigHelper.SetGroupValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMachValue
 *  Klasse:           TXMLMaConfigHelper
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath, Value)
 *
 *  Kurzbeschreibung: Setzt den Wert eines Elements das mit einem Selektions Pfad f�r MMXML angegeben wird.
 *  Beschreibung:     
                      Der Selektionspfad wird intern zerlegt und auf das Bauzustands XML angepasst
 --------------------------------------------------------------------*)
procedure TXMLMaConfigHelper.SetMachValue(aSelPath: string; const Value: Variant);
var
  xElement: IXMLDOMElement;
begin
  xElement := MachNode[aSelPath];

  if not(assigned(xElement)) then begin
    if assigned(Machine) then
      xElement := GetCreateElement(RemoveXPRoot(cXPMachineNode, aSelPath), DOM, Machine);
  end;// if not(assigned(xElement)) then begin

  if assigned(xElement) then begin
    if VarType(Value) = varBoolean then
      xElement.Text := GetEnumName(TypeInfo(boolean), ord(boolean(Value)))
    else
      xElement.Text := FloatVariantToString(Value);
  end;// if assigned(xElement) then begin
end;// TXMLMaConfigHelper.SetMachValue cat:No category

//:-------------------------------------------------------------------
procedure TXMLMaConfigHelper.SortGroups;
var
  xSorted: Array of integer;
  i: Integer;
  xExchanged: Boolean;
  xTempSpindle: Integer;
begin
  // Gruppen nach Spindelrange sortieren (Kein aufw�ndiger Sortieralgoritmus, da nur wenige Gruppen

  // Zuerst alle Startspindeln in ein Array das dann sortiert werden kann
  SetLength(xSorted, GroupCount);
  for i := 0 to GroupCount - 1 do
    xSorted[i] := GroupValue[i, cXPSpindleFromItem];

  (* Austauschverfahren:
     Es werden jewils zwei benachbarte Elemente getauscht bis alles sortiert ist. *)
  repeat
    xExchanged := false;
    for i := 1 to Length(xSorted) - 1 do begin
      if xSorted[i-1] > xSorted[i] then begin
        // Elemente m�ssen ausgetauscht werden
        xTempSpindle := xSorted[i-1];
        xSorted[i - 1] := xSorted[i];
        xSorted[i] := xTempSpindle;
        // Es ist getauscht worden, also muss die �ussere Schleife noch mindestens einmal durchlaufen werden
        xExchanged := true;
      end;// if xSorted[i-1] > xSorted[i] then begin
    end;// for i := 1 to Length(xSorted) - 1 do begin
  until not(xExchanged); // Solange bis nicht mehr getauscht werden musste

  (* Braucht zwingend einen Maschinen Knoten. Sollte dies sp�ter nicht mehr so sein,
     dann muss das Verfahren angepasst werden *)
  if not(assigned(Machine)) then
    raise Exception.Create('TXMLMaConfigHelper.SortGroups: Need machinenode to sort groups');

  // Hier ist das Array sortiert. Jetzt m�ssen die Gruppenelemente in der selben Reihenfolge gruppiert werden
  for i := 0 to Length(xSorted) - 1 do
    Machine.parentNode.InsertBefore(GetGroupNodeFromSpindle(xSorted[i]), Machine);

  SetLength(xSorted, 0);
end;// TXMLMaConfigHelper.SortGroups cat:No category

(*:
 *  Klasse:        TMaConfigReader
 *  Vorg�nger:     TXMLMaConfigHelper
 *  Kategorie:     No category
 *  Kurzbeschrieb: Hat eine Verbindung zur Datenbank und liest die Maschinenkonfiguration von der DB 
 *  Beschreibung:  Die Maschinenkonfiguration ist in einem XML Dokument gespeichert das wie folgt aufgebaut ist.
                   1) F�r jede Bauzustandsgruppe ein Group Element
                   2) Ein Maschinen Element f�r Maschinen abh�ngigen Settings
                   3) Ein Defaults XML Settings mit den Facets als Attribute und den Defaults als Text 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TMaConfigReader.Create;
begin
  inherited Create;

//: ----------------------------------------------
  // Mach ID initialisieren
  fMachID := cUninitializedMachine;
  //MachName mit Leerstring initialisieren
  fMachName := '';

  fDBHashcode1 := 0;
  fDBHashcode2 := 0;
  fNetTyp := ntNone;
end;// TMaConfigReader.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: FPReader wieder freigeben
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TMaConfigReader.Destroy;
begin
  FreeAndNil(fFPHandler);

//: ----------------------------------------------
  inherited Destroy;
end;// TMaConfigReader.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Erm�glicht das Kopieren von Bauzust�nden
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigReader.Assign(Source: TPersistent);
var
  xSource: TMaConfigReader;
begin
  if Source is TMaConfigReader then begin
    xSource := TMaConfigReader(Source);
    fMachID := xSource.MachID;
    fMachName := xSource.MachName;
    fDBHashcode1 := xSource.DBHashcode1;
    fDBHashcode2 := xSource.DBHashcode2;
    fAvailableSensingHeadClasses := [];
  end;// if Source is TMaConfigReader then begin

  inherited Assign(Source);
end;// TMaConfigReader.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           ConfigChanged
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Leitet den Event aus dem FPReader weiter
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigReader.ConfigChanged(Sender: TObject);
begin
  DoMachineChanged;
end;// TMaConfigReader.ConfigChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           DoMachineChanged
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Dispatch Routine f�r den Event 'OnMachineChanged'
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigReader.DoMachineChanged;
begin
  if Assigned(fOnMachineChanged) then fOnMachineChanged(Self);
end;// TMaConfigReader.DoMachineChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           DOMChanged
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wird aufgerufen, wenn ein neues DOM zugewiesen oder geladen wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigReader.DOMChanged;
begin
  inherited DOMChanged;

//: ----------------------------------------------
  // Benachrichtigt z.B. das Modell von der �nderung
  DoMachineChanged;
end;// TMaConfigReader.DOMChanged cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAvailable
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt zur�ck ob ein Bauzustand vorhanden ist
 *  Beschreibung:     
                      Ein Bauzustand ist nur dann verf�gbar, wenn eine Maschinen ID zugewiesen wurde.
 --------------------------------------------------------------------*)
function TMaConfigReader.GetAvailable: Boolean;
begin
  Result := inherited GetAvailable;

//: ----------------------------------------------
  result := (result and (fMachID <> cUninitializedMachine));
end;// TMaConfigReader.GetAvailable cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetAvailableSensingHeadClasses
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert ein Set mit allen verwendeten Tastk�pfen auf der Maschine
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TMaConfigReader.GetAvailableSensingHeadClasses: TSensingHeadClassSet;
var
  i: Integer;
begin
  if FAvailableSensingHeadClasses = [] then begin
    for i := 0 to GroupCount - 1 do
      FAvailableSensingHeadClasses := FAvailableSensingHeadClasses + [GetSensingHeadClass(Self,i)];
  end;// if FAvailableSensingHeadClasses = [] then begin

//: ----------------------------------------------
  result := FAvailableSensingHeadClasses;
end;// TMaConfigReader.GetAvailableSensingHeadClasses cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetConstraints
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath)
 *
 *  Kurzbeschreibung: Liefert die Grenz- und Default Wert(e) des entsprechenden Elements
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TMaConfigReader.GetConstraints(aSelPath: string): TXMLConstraints;
var
  xNode: IXMLDOMNode;
  xElement: IXMLDOMElement;
  xLoepfeBodyPos: Integer;
begin
  if not(assigned(fConstraints)) then
    fConstraints := TXMLConstraints.Create;

//: ----------------------------------------------
  fConstraints.DOMElement := nil;

  // Selktion des gew�nschten Elements
  if assigned(DOM) then begin
    xLoepfeBodyPos := AnsiPos(cXMLLoepfeBody, aSelPath);
    if xLoepfeBodyPos <= 2 then
      system.Insert('/' + cXMLDefaultsElementName, aSelPath, xLoepfeBodyPos + Length(cXMLLoepfeBody));

    xNode := DOM.SelectSingleNode(aSelPath);
    Supports(xNode, IXMLDOMElement, xElement);
    // Element zuweisen
    fConstraints.DOMElement := xElement;
  end;// if assigned(DOM) then begin

//: ----------------------------------------------
  result := fConstraints;
end;// TMaConfigReader.GetConstraints cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetFPHandler
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Erzeugt einen FPReader.
 *  Beschreibung:     
                      Der FPReader dient dazu, �ber einen Spindelbereich auf ein gemeinsames FPattern zuzugreifen.
                      Dabei kann angegeben werden ob die maximale oder die minimale Konfiguration abgefragt
                      werden soll.
 --------------------------------------------------------------------*)
function TMaConfigReader.GetFPHandler: TBaseFPHandler;
begin
  if not(assigned(fFPHandler)) then
    fFPHandler := TMaConfigFPHandler.Create;

  // Den Bauzustand mitgeben
  TMaConfigFPHandler(fFPHandler).MaConfigHelper := self;
  // Weiterleitung f�r Den Event aus dem FPReader (geht dann �ber OnMachineChanged)
  fFPHandler.OnConfigChanged := ConfigChanged;

  result := fFPHandler;
end;// TMaConfigReader.GetFPHandler cat:No category

//:-------------------------------------------------------------------
function TMaConfigReader.GetNetTyp: TNetTyp;
var
  xNetTyp: Integer;

  const
    cLoadMaTyp   = ' SELECT c_net_id ' +
                   ' FROM t_machine ' +
                   ' WHERE c_machine_id = :c_machine_id ';

begin
  if fNetTyp = ntNone then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cLoadMaTyp;
        ParamByName('c_machine_id').AsInteger := MachID;
        open;
        if not(EOF) then begin
          xNetTyp := FieldByName('c_net_id').AsInteger;
          if (xNetTyp >= 0) and (xNetTyp <= ord(High(TNetTyp))) then
            fNetTyp := TNetTyp(FieldByName('c_net_id').AsInteger);
        end; //if not(EOF)
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if fNetTyp = ntNone then begin

//: ----------------------------------------------
  Result := fNetTyp;
end;// TMaConfigReader.GetNetTyp cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadMachConfigFromDB
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        (aMachID)
 *
 *  Kurzbeschreibung: L�dt das Bauzustands XML f�r eine Maschine von der DB
 *  Beschreibung:     
                      Im XML sind die einzelnen Gruppen, die Machine und die Defaults f�r die Settings definiert.
 --------------------------------------------------------------------*)
function TMaConfigReader.LoadMachConfigFromDB(aMachID: integer): Boolean;

  const
    cLoadMaConfig = ' SELECT c_xml_data, c_hashcode1, c_hashcode2' +
                    ' FROM t_magroup_config mc, t_machine m ' +
                    ' WHERE m.c_machine_id = :c_machine_id ' +
                    '   and m.c_magroup_config_id = mc.c_magroup_config_id';

    cLoadMaName   = ' SELECT c_machine_name ' +
                    ' FROM t_machine ' +
                    ' WHERE c_machine_id = :c_machine_id ';

begin
  fDBHashcode1 := 0;
  fDBHashcode2 := 0;

//: ----------------------------------------------
  result := false;

  fAvailableSensingHeadClasses := []; //TK-Klassen nullen

  if aMachID > cUninitializedMachine then begin
    // Alte Defaults freigeben
    fDOM := nil;

    // Da diese Methode auch von aussen aufgerufen werden kann, wird die MachID zugewiesen
    if aMachID <> fMachID then
      fMachID := aMachID;

    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := cLoadMaConfig;
        ParamByName('c_machine_id').AsInteger := aMachID;
        open;
        if not(EOF) then begin
          result := LoadDOM(FieldByName('c_xml_data').AsString);
          fDBHashcode1 := FieldByName('c_hashcode1').AsInteger;
          fDBHashcode2 := FieldByName('c_hashcode2').AsInteger;
        end; //if not(EOF)

        SQL.Text := cLoadMaName;
        ParamByName('c_machine_id').AsInteger := aMachID;
        open;
        if not(EOF) then begin
          fMachName := FieldByName('c_machine_name').AsString;
        end; //if not(EOF)
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
    // Das DOM hat sich ge�ndert
    DOMChanged;
  end;// if aMachID > cUninitializedMachine then begin

//: ----------------------------------------------
  DoMachineChanged;
end;// TMaConfigReader.LoadMachConfigFromDB cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetMachID
 *  Klasse:           TMaConfigReader
 *  Kategorie:        No category 
 *  Argumente:        (Value)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r die MachID
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigReader.SetMachID(const Value: Integer);
begin
  if fMachID <> Value then
  begin

//: ----------------------------------------------
    fMachID := Value;

//: ----------------------------------------------
    LoadMachConfigFromDB(fMachID);

//: ----------------------------------------------
  end;
end;// TMaConfigReader.SetMachID cat:No category

(*:
 *  Klasse:        TMaConfigFPHandler
 *  Vorg�nger:     TBaseFPHandler
 *  Kategorie:     No category
 *  Kurzbeschrieb: Bildet das minimale- oder maximale FPattern �ber mehrere Gruppen 
 *  Beschreibung:  Je nach Zugriff wird das minimale (Spindelrange -1 bis -1 z.B. mit der Methode 'resetSpindleRange')
 oder
                   das maximale (beliebiger Spindelrange > 0) FPattern gebildet. 
 *)
 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      �ber den MaConfigHelper wird auf den Bauzustand der aktuellen Maschine zugegriffen.
                      Bevor auf das FPattern zugegriffen werden kann, muss die Anwendung einen Spindel Bereich
                      ausw�hlen und dabei auch angeben ob minimale oder maximale Funktionalit�t
                      gew�nscht wird (SetSpindleRange()).
 --------------------------------------------------------------------*)
constructor TMaConfigFPHandler.Create;
begin
  inherited Create;

  // Old und aktuell m�ssen unterschiedlich initialisiert werden (--> NeedToRecalc)
  mOldSpdlFrom := 0;
  mOldSpdlTo := 0;
  mOldFPType := fpNone;

  mSpdlFrom := -1;
  mSpdlTo := -1;
end;// TMaConfigFPHandler.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           CalcFPattern
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        (aFPType)
 *
 *  Kurzbeschreibung: Erstellt das FPattern das die maximale Funktionalit�t eines Spindelbereichs beschreibt
 *  Beschreibung:     
                      Zuerst muss von der Anwendung der Spindel Range gesetzt werden (--> SetSpindelRange()). Bei diesem
                      Aufruf wird auch festgelegt ob das Minimale oder das Maximale FPattern gew�nscht wird. Danach
                      kann
                      �ber die Array Eigenschaft 'Values[]' auf die einzelnen Felder des FPattern zugreifen.
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.CalcFPattern(aFPType: TFPType);
var
  i: Integer;
  xNodeList: IXMLDOMNodeList;
  xNewFP: TBaseFPHandler;
  xFPToMerge: TBaseFPHandler;
begin
  // Stellt alle Gruppen Nodes zur Verf�gung
  xNodeList := PrepareNodeListForCalc;

  if assigned(xNodeList) and (xNodeList.Length > 0) then begin
    if mOutputDebugMsg then
      CodeSite.SendString('Nodelist f�r alle selektierten Gruppen', FormatXML(xNodeList));

    // Setzt die Parameter um zu Kennzeichnen, dass keine weitere Neuberechnung notwendig ist
    SetCalculatedSpindleRange;

    // Erstes FPattern zuweisen
    xNewFP := TBaseFPHandler.Create(xNodeList[0].cloneNode(true));
    try
      for i := 1 to xNodeList.Length - 1 do begin
        // Zweites FPattern zuweisen
        xFPToMerge := TBaseFPHandler.Create(xNodeList[i].cloneNode(true));
        try
          (* Verschmelzen der beiden FPattern. Als Ziel ist wieder das �rspr�nglich erste
             FPattern angegeben. Am Ende ist in diesem FPattern das verschmolzene Element *)
          xNewFP.MergeFPattern(xFPToMerge, aFPType, xNewFP);
        finally
          xFPToMerge.Free;
        end;// try finally
      end;// for i := 1 to xNodeList.Length - 1 do begin

      // Wenn ein Parentnode vorhanden ist, dann das Element ersetzen
      if assigned(FPElement.ParentNode) then
        FPElement.ParentNode.replaceChild(xNewFP.FPElement, FFPElement)
      else
        // ... sonst einfach zuweisen
        FPElement := xNewFP.FPElement;
    finally
      xNewFP.Free;
    end;// try finally
  end;// if assigned(xNodeList) and (xNodeList.Length > 0) then begin
end;// TMaConfigFPHandler.CalcFPattern cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetValue
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        (aSelPath)
 *
 *  Kurzbeschreibung: Erm�glicht den Zugriff auf ein einzelnes Item im FPattern
 *  Beschreibung:     
                      Der Selektionspfad kann mit den Konstanten f�r das MMXML angegeben werden. Die
                      Konstanten werden auf die aktuelle Struktur angepasst (FPattern ist eine Untermenge
                      der Settings).
 --------------------------------------------------------------------*)
function TMaConfigFPHandler.GetValue(aSelPath: string): Variant;
begin
  // Default ist das maximale FPattern
  if mFPType = fpNone then
    mFPType := fpMax;

  // FPattern f�r den gew�nschten Spindelbereich ermitteln
  CalcFPattern(mFPType);

//: ----------------------------------------------
  // Fragt das entsprechende Element ab
  result := inherited GetValue(aSelPath);
end;// TMaConfigFPHandler.GetValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           IsMachineRange
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Stellt fest ob vor dem Bilden des FPatterns der Spindelrange der Maschine ermittelt werden muss
 *  Beschreibung:     
                      Wenn mindestens eine der Spindeln des aktuellen Spindelbereichs auf 0 oder kleiner steht,
                      wird vor der Generierung des FPatterns der Spindelbereich �ber den Bauzustand der aktuellen
                      Maschine(MaConfigHelper) ermittelt.
                      Der Spindelbereich kann �ber die Funktion 'ResetSpdlRange()' zur�ckgesetzt werden. Danach gilt
                      wieder die gesamte Maschine als selektiert.
 --------------------------------------------------------------------*)
function TMaConfigFPHandler.IsMachineRange: Boolean;
begin
  result := (mSpdlFrom <= 0) or (mSpdlTo <= 0)
end;// TMaConfigFPHandler.IsMachineRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           NeedToRecalc
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Wenn seit der letzten Berechnung der Spindelrange ge�ndert hat, muss neu berechnet werden
 *  Beschreibung:     
                      Zus�tzlich muss der Typ f�r die Berechnung festgelegt werden. Im Normalfall
                      gilt bei der Berechnung �ber die gesamte Maschine (ResetSpdlRange()), dass das 
                      maximale FPattern berechnet werden soll. Bei einem Range wird normalerweise 
                      das Minimum berechnet. 
                      Ein Sonderfall ist eine Maschine mit inkompatibler Mischbest�ckung (z.B.: Zenit und BD), dann muss
                      f�r die einzelnen Sektionen das Maximum berechnet werden.
 --------------------------------------------------------------------*)
function TMaConfigFPHandler.NeedToRecalc: Boolean;
begin
  result := (mOldSpdlFrom <> mSpdlFrom) or (mOldSpdlTo <> mSpdlTo) or (mOldFPType <> mFPType);
end;// TMaConfigFPHandler.NeedToRecalc cat:No category

//:-------------------------------------------------------------------
(*: Member:           PrepareNodeListForCalc
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt den Spindelbereich neu der f�r die Berechnung des Min- oder Max FPattern benutzt wird
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TMaConfigFPHandler.PrepareNodeListForCalc: IXMLDOMNodeList;
var
  xFrom: Integer;
  xTo: Integer;
  xXPGetGroupsFromSpindle: string;
begin
  result := nil;

  if (NeedToRecalc) and (assigned(fMaConfigHelper)) then begin
    xFrom := mSpdlFrom;
    xTo   := mSpdlTo;

    // Maschinen Range f�r die Abfrage bereitstellen
    if IsMachineRange then
      fMaConfigHelper.GetMachSpindleRange(xFrom, xTo);


    (* Zuerst muss festgestellt werden welches die erste Spindel in der Gruppe ist,
       in der die jeweils gew�nschte Spindel enthalten ist. *)
    xFrom := fMaConfigHelper.FirstGroupSpindleFromSpindle(xFrom, true);
    xTo   := fMaConfigHelper.LastGroupSpindleFromSpindle(xTo, true);

    // Wenn eine der Spindeln ung�ltig ist, dann wurde sie auf 0 gesetzt
    if (xFrom > 0) and (xTo > 0) then begin
      // z.B.: '/LoepfeBody/Group/FPattern[(../SpindleFrom >= 1) and (../SpindleTo <= 30)]'
      xXPGetGroupsFromSpindle := cXPFPatternNode + Format('[(../%s >= %d) and (../%s <= %d)]', [cSpindleFromName, xFrom, cSpindleToName, xTo]);
      result := fMaConfigHelper.Dom.SelectNodes(xXPGetGroupsFromSpindle);
    end;// if (xFrom > 0) and (xTo > 0) then begin
  end;// if not NeedToRecalc then begin
end;// TMaConfigFPHandler.PrepareNodeListForCalc cat:No category

//:-------------------------------------------------------------------
(*: Member:           ResetCalculation
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt die Berechnung des FPattern zur�ck, so wird beim n�chsten Zugriff das FPattern neu berechnet
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.ResetCalculation;
begin
  // Erzwingt beim Zugriff auf das FPElement eine neuberechnug
  mOldSpdlFrom := MAXINT;
end;// TMaConfigFPHandler.ResetCalculation cat:No category

//:-------------------------------------------------------------------
(*: Member:           ResetSpindleRange
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt den Spindelrange zur�ck
 *  Beschreibung:     
                      Spindelrange gilt f�r die gesammte Maschine.
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.ResetSpindleRange;
begin
  // F�r gesasmten Maschinenbereich das Maximale FPattern bilden
  SetSpindleRange(-1, -1, fpMax);
end;// TMaConfigFPHandler.ResetSpindleRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetCalculatedSpindleRange
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Setzt die Parameter um zu Kennzeichnen, dass keine Neuberechnung notwendig ist
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.SetCalculatedSpindleRange;
begin
  mOldSpdlTo := mSpdlTo;
  mOldSpdlFrom := mSpdlFrom;
  mOldFPType := mFPType;
end;// TMaConfigFPHandler.SetCalculatedSpindleRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetSpindleRange
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        (aSpdlFrom, aSpdlTo)
 *
 *  Kurzbeschreibung: aktualisiert den Spindelrange
 *  Beschreibung:     
                      Markiert ob eine Neuberechnung durchgef�hrt werden muss (Neuer SpindelRange).
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.SetSpindleRange(aSpdlFrom, aSpdlTo: integer);
begin
  SetSpindleRange(aSpdlFrom, aSpdlTo, mFPType);
end;// TMaConfigFPHandler.SetSpindleRange cat:No category

//:-------------------------------------------------------------------
(*: Member:           SetSpindleRange
 *  Klasse:           TMaConfigFPHandler
 *  Kategorie:        No category 
 *  Argumente:        (aSpdlFrom, aSpdlTo, aFPType)
 *
 *  Kurzbeschreibung: aktualisiert den Spindelrange
 *  Beschreibung:     
                      Markiert ob eine Neuberechnung durchgef�hrt werden muss (Neuer SpindelRange).
 --------------------------------------------------------------------*)
procedure TMaConfigFPHandler.SetSpindleRange(aSpdlFrom, aSpdlTo: integer; aFPType: TFPType);
var
  xChanged: Boolean;
begin
  // Pr�fen ob etwas ge�ndert hat
  xChanged := (aSpdlFrom <> mSpdlFrom) or (aSpdlTo <> mSpdlTo) or (aFPType <> mFPType);

//: ----------------------------------------------
  // Werte �bernehmen
  mSpdlFrom := aSpdlFrom;
  mSpdlTo := aSpdlTo;
  mFPType := aFPType;

//: ----------------------------------------------
  // Bei einer �nderung einen Event ausl�sen
  if xChanged then
   DoConfigChanged;
end;// TMaConfigFPHandler.SetSpindleRange cat:No category


end.
