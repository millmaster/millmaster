{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMXMLWare.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: - Wird auch im Setup benoetigt
| Develop.system: Windows 2000
| Target.system.: W2k / XP / W2k3
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 22.12.2004  1.00  SDo | File created
===============================================================================}
unit MMXMLWare;

interface

uses SysUtils, Classes, Windows, Dialogs, Forms, Messages;


function WriteXMLMapFilesToDB: Boolean;
function WriteXMLMapFileToDB(aMapFilePath: String; var aLogMsg: String): Boolean;

function ExistsTableXMLMapfile: Boolean;
function GetXMLMapFileVersion(aMapFile:String):String;
procedure GetAllFiles(aPath :string; var aFileList : TStringList);


implementation

uses LoepfeGlobal, AdoDbAccess, XMLGlobal, XMLDef, XMLMappingDef, MSXML2_TLB,
     RzCSIntf;

//------------------------------------------------------------------------------




//******************************************************************************
//Alle Dateien aus angegebenen Verzeichnis & Unterverzeichnis ermitteln
//******************************************************************************
procedure GetAllFiles(aPath :string; var aFileList : TStringList);
var
  xsr: TSearchRec;
  xFileAttrs: Integer;
  xDir, xFind: string;
begin
  xDir  := ExtractFilePath(aPath);
  xFind := ExtractFileName(aPath);

  if xDir [length(xDir)] <> '\' then xDir:= xDir + '\';

  xFileAttrs:=  faHidden or faArchive  or faReadOnly;

  //Alle Dateien suchen
  if FindFirst(xDir + xfind, xFileAttrs	, xsr) = 0 then repeat
     aFileList.Add(xDir + xsr.Name );
  until FindNext(xsr) <> 0;
  Sysutils.FindClose(xsr);

  //Unterverzeichnisse durchsuchen
  if FindFirst(xDir + '*.*', faDirectory, xsr) = 0 then repeat
     if ((xSr.Attr and faDirectory) = faDirectory) and  (xSr.name[1] <> '.' ) then
         GetAllFiles( xDir + xSr.name + '\' + xfind, aFileList);
  until FindNext(xsr) <> 0;
  Sysutils.FindClose(xsr);
end;
//------------------------------------------------------------------------------


//******************************************************************************
//Ermittelt die XML-Mapfile Version  (-> Vers. von YMSetting & MaConfig )
//******************************************************************************
function GetXMLMapFileVersion(aMapFile:String):String;
var xMSDOM          : DOMDocumentMM;
    xIXMLDOMNode, xIXMLNode    : IXMLDOMNode;
    xVers, xMapFile, xFilename, xVMaConfig, xNodeName, xLabel, xVersText : String;
    x, xCount : integer;
    xIXMLDOMNodeList  : IXMLDOMNodeList;


begin
  xVers := '';
  Result :=  '';
  try

    if aMapFile = '' then exit;
    if ExtractFilePath(aMapFile) <> '' then begin
       if not FileExists(aMapFile) then exit;
       xMapFile := LoadXMLFile(aMapFile)
    end else
       with TAdoDBAccess.Create(1, True) do begin
         try
          Init;
          with Query[0] do begin
             xFilename := aMapFile;
             SQL.Text := 'SELECT * FROM t_xml_mapfile WHERE c_mapfile_name = ''' + xFilename + '''';
             Open;
             First;
             if not EOF then
                xMapFile := FieldByName('c_mapfile').AsString;
          end;
         finally
          Free;
         end;
       end;

    if xMapFile = '' then exit;

    xMSDOM := DOMDocumentMMCreate;
    if assigned(xMSDOM) then begin
       xVersText := '';

       xMSDOM.loadXML(xMapFile);
       xIXMLDOMNode:= xMSDOM.selectSingleNode(cEleLoepfeBody); //Root

       xIXMLDOMNodeList :=  xIXMLDOMNode.Get_childNodes; //Alle Nodes auslesen (1. Unter Nodes)
       xCount:= xIXMLDOMNodeList.length;
       for x:=0 to xCount -1 do begin

          //Node Name ermitteln
          xNodeName := xIXMLDOMNodeList.item[x].Get_nodeName;

          xIXMLDOMNode:= xMSDOM.selectSingleNode( Format('/%s/%s',[cEleLoepfeBody, xNodeName]) );

          xVers := AttributeToVariantDef( 'version' , xIXMLDOMNode, '');

          if xVers <> '' then begin
             if x < xCount - 1 then
                xLabel := Format('%s: %s;   ',[xNodeName, xVers])
             else
                xLabel := Format('%s: %s',[xNodeName, xVers]); // Letztes Attribut

             xVersText :=  xVersText + xLabel;
          end;
       end;    

    end;
  finally
  end;
  Result := xVersText;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Ermmitelt die Table XML Mappfile auf der DB MM_Winding
//******************************************************************************
function ExistsTableXMLMapfile: Boolean;
var xRet : Boolean;
begin
  xRet := FALSE;
  //Tabelle t_xml_mapfile ueberpruefen
  with TAdoDBAccess.Create(1, True) do begin
     try
       Init;
       with Query[0] do begin
         Close;
         SQL.text := 'if exists (select * from dbo.sysobjects where id = object_id(N''[dbo].[t_xml_mapfile]'')) ' +
                       'select c_mapfile_name name, c_mapfile_version version from t_xml_mapfile';
         Open;
         xRet:= Active; // Tablle existiert: Active = TRUE
       end;
     finally
       Free;
     end;
  end;
  Result := xRet;
end;
//------------------------------------------------------------------------------


//******************************************************************************
//Schreibt ein XML-Mapfile in die DB
//******************************************************************************
function WriteXMLMapFileToDB(aMapFilePath: String; var aLogMsg: String):Boolean;
var xRet : Boolean;
    xFilename, xVersion, xMapFileData : String;
begin

 if aMapFilePath = '' then begin
    aLogMsg := 'WriteXMLMapFileToDB(): Map file path is empty';
    xRet := FALSE;
    exit;
 end;

 if not FileExists(aMapFilePath) then begin
    aLogMsg := Format('WriteXMLMapFileToDB(): Map file %s not exists',[aMapFilePath]);
    xRet := FALSE;
    exit;
 end;


 //XML-Files in DB schreiben
 with TAdoDBAccess.Create(1, True) do begin
   try
     Init;
     xRet := TRUE;

     //Filename ohne Ext.
     xFilename := ExtractFileName(aMapFilePath);
     xFilename := StringReplace( xFilename, ExtractFileExt( xFilename ), '', [rfReplaceAll]);

     //Mapfile Version
     xVersion := GetXMLMapFileVersion(aMapFilePath);

     //Mapfile Data einlesen
     xMapFileData := LoadXMLFile(aMapFilePath);

     with Query[0] do begin
         //Check: existiert XML Mapfile auf der DB
         SQL.Text := 'SELECT c_mapfile_id FROM t_xml_mapfile WHERE c_mapfile_name = ''' + xFilename + '''';
         Open;

         if not(EOF) then begin
            //Update
            SQL.Text := Format('UPDATE t_xml_mapfile SET c_mapfile = ''%s'', c_mapfile_version =  ''%s'' WHERE c_mapfile_id = %d', [xMapFileData, xVersion, FieldByName('c_mapfile_id').AsInteger]);

            if ExecSQL > 0 then
               aLogMsg := Format('Map file %s version %s updated',[xFilename, xVersion])
            else begin
               aLogMsg := Format('Map file %s version %s not updated',[xFilename, xVersion]);
               xRet := FALSE;
            end;
         end else begin
            //Insert
            SQL.Text := 'INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile_version, c_mapfile) VALUES(:c_mapfile_id, :c_mapfile_name, :c_mapfile_version, :c_mapfile)';
            ParamByName('c_mapfile_name').AsString := xFilename;
            ParamByName('c_mapfile').AsString := xMapFileData;
            ParamByName('c_mapfile_version').AsString := xVersion;

            if InsertSQL('t_xml_mapfile', 'c_mapfile_id', MAXINT, 1) < 0 then
               aLogMsg := Format('Map file %s version %s inserted',[xFilename, xVersion])
            else begin
               aLogMsg := Format('Map file %s version %s not inserted',[xFilename, xVersion]);
               xRet := FALSE;
            end;
         end;

     end; //END with
     Free;
     xRet := TRUE;
   except
      on e: exception do begin
            aLogMsg := Format('WriteXMLMapFileToDB(): Map file %s error msg: %s',[xFilename, e.Message]);
            xRet := FALSE;
            Free;
      end;
   end;
 end;

 if xRet then
    CodeSite.SendMsg(aLogMsg)
 else
    CodeSite.SendError(aLogMsg);

 result := xRet;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Schreibt die XML-Mapfiles in die DB
//******************************************************************************
function WriteXMLMapFilesToDB:Boolean;
var xMapFilesList : TStringList;
    xMapFile, xLogMsg, xRoot, xFile, xPath : String;
    x : integer;
    xRet : Boolean;
begin
  try
    xRet := FALSE;

    if not ExistsTableXMLMapfile then exit;

    xMapFilesList :=  TStringList.Create;
    xMapFilesList.Sorted := TRUE;
    xMapFilesList.Duplicates := dupIgnore;

    //Ort der Map files
    xRoot := GetRegString(cRegLM, cRegMillMasterPath, 'MILLMASTERROOTPATH', '');
    //xRoot := 'D:\Delphi\MM-XML\MapFile';
    //xRoot := 'D:\Delphi\MM-XML';

    if xRoot = '' then exit;

    xRoot := xRoot + '\Queries\XMLMapFiles';

    //Alle Map files ermitteln
    xFile := '*.map';
    xPath:= Format('%s\%s',[xRoot, xFile]);
    GetAllFiles(xPath, xMapFilesList );

    xRet := TRUE;

    if xMapFilesList.Count > 0 then
       for x:= 0 to  xMapFilesList.Count - 1 do begin
           //Map File in DB schreiben
           xMapFile := xMapFilesList.Strings[x];
           if WriteXMLMapFileToDB(xMapFile, xLogMsg) = FALSE then
              xRet := FALSE;
       end;
  finally
    xMapFilesList.Free;
  end;
  Result := xRet;
end;
//------------------------------------------------------------------------------

end.
