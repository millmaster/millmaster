(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Kapselt die Funktionalitaet des Hauptprozesses in eine BasisKlasse
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.06.1998  0.00  Wss | File created
| 10.06.1998  1.00  Wss | Class TBaseMain created
| 01.12.1998  1.01  Wss | Create of mEventLog object moved into Create methode
| 11.01.1999  1.02  Mg  | If Statment in WriteMsgToMMGuard implemented
| 13.01.1999  1.03  Nue | MMGuard command gcRestartMM implemented
| 18.01.1999  1.04  Wss | write property ThreadCount implemented for TXNHandler
| 08.03.1999  1.05  Mg  | New state init in main thread implemented
|                       | GetThreadInfo : result is a pointer to the info stored in the thread
| 07.04.1999  1.06  Wss | In PipeNoWait mode (psInit) error handling were wrong
| 04.05.2000  1.12 Wss  | On Debug command CodeSite will be enabled/diabled from registry
                          Use Handlername as DWord value in HKLM\....\MillMaster\Debug or use
                          DebugConfig tool to Add/Change key values.
                          0 = Disable (default), 1 = Enabled
| 11.04.2001  1.13  Nue | In TBaseMain neu MMSettings : TMMSettingsReader (statt TBaseSettingsReader)
| 26.03.2002  1.13  Wss | - UpdateDebugMode wird nun am Schluss von Initialize aufgerufen und nur noch beim gcDebug Kommando
                            Darf nicht frueher aufgerufen werden, da Objekt fuer WriteLogDebug noch nicht initialisiert ist
                          - Beep bei gcKillSelf entfernt
| 06.10.2002        LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
|=========================================================================================*)
unit BaseMain;

interface

uses
  mmThread, Windows, SysUtils, Classes, extctrls,
  LoepfeGlobal, BaseGlobal, IPCClass, BaseThread, MMEventLog, SettingsReader; //BaseSetup;

type
  // Base class of Mailslot
  TBaseMain = class(TObject)
  private
    mCurrentState:  TMMProcessState;
    mCommandTime:   DWord;
    mMMGuardRead:   TIPCServer;      // interface for MMGuard
    mMMGuardWrite:  TIPCClient;      // interface to MMGuard
    fMMSettings  :  TMMSettingsReader;
    mMsg:           TMMGuardRec;
    mTerminated:    Boolean;
    function GetThreadInfo(aIndex: Integer): PThreadDef;
    procedure CleanUp;
    procedure SetThreadCount(const aValue: SmallInt);
  protected
    mEventLog:      TEventLogWriter;
    mSubSystemTyp:  TSubSystemTyp;
    mSubSystemDef:  TSubSystemDef;
    function CheckThreadState( aCommand: TMMProcessState ): Boolean;
    function ConnectThreads: Boolean;virtual;
    // this methode is only called from overriden implenetation
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; virtual; abstract;
    function CreateThreads: Boolean; virtual;
    function ProcessReadError: DWord;
    procedure ResumeThreads;
    procedure SetSystemRun;
    procedure UpdateDebugMode;
    procedure SuspendThreads;
    procedure WriteLog(aEvent: TEventType; aText: String; aBuf: PByte = Nil; aCount: DWord = 0);
    function WriteMsgToMMGuard(aCommand: TMMGuardCommand; aError: DWord): Boolean;
    function getParamHandle:TMMSettingsReader;virtual;abstract;   //alt vor 11.4.01 Nue:TBaseSettingsReader;virtual;abstract;
    property ThreadInfo[aIndex: Integer]: PThreadDef read GetThreadInfo;
    property ThreadCount: SmallInt read mSubSystemDef.ThreadCount write SetThreadCount;
    property MMSettings : TMMSettingsReader read fMMSettings;
  public
    constructor Create(aSubSystemTyp: TSubSystemTyp); virtual;
    destructor Destroy; override;
    function Initialize: Boolean; virtual;
    procedure Run;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS;

//*****************************************************************************
//*****************************************************************************
// Initialize member variables and perform the connection to MMGuard
constructor TBaseMain.Create(aSubSystemTyp: TSubSystemTyp);
begin
  inherited Create;

  // enable/diable CodeSite

  fMMSettings := Nil;
  // defines the current SubSystem
  mCurrentState  := psCreated;
  mCommandTime   := 0;
  mSubSystemTyp  := aSubSystemTyp;

  // with informations to additional interfaces
  mSubSystemDef := cSubSystemDefinitions[mSubSystemTyp];

  // not terminated yet
  mTerminated := False;

  // make connection to Windows NT EventLog
  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cEventLogServerNameForSubSystems,
                                      mSubSystemTyp, cEventLogDefaultText[mSubSystemDef.ThreadTyp], True);
end;
//-----------------------------------------------------------------------------
// Clean up any instances and memory
destructor TBaseMain.Destroy;
begin
  CleanUp;
  // call inherited Destroy
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TBaseMain.CheckThreadState( aCommand: TMMProcessState ): Boolean;
var
  i: Integer;
begin
  // check if all thread have confirmed aCommand parameter
  Result := True;
  for i:=1 to ThreadCount do
    with ThreadInfo[i]^ do begin
      if mMsg.ThreadTyp = ThreadTyp then
        // set state for this thread to the command parameter
        State := aCommand;
      Result := Result and (State = aCommand);
    end;
end;
//-----------------------------------------------------------------------------
procedure TBaseMain.CleanUp;
var
  i: Integer;
begin
  // Free all threads
  for i:=1 to ThreadCount do
    with mSubSystemDef.Threads[i] do begin
      Thread.Terminate; // wss, 11.12.2002 wegen TimeHandler hinzugefügt
      Thread.Free;
    end;

  // clean up any instances
  mMMGuardRead.Free;
  mMMGuardWrite.Free;
  mEventLog.Free;
  fMMSettings.ReleaseInstance;
//  fMMSettings.Free;
end;
//-----------------------------------------------------------------------------
function TBaseMain.ConnectThreads: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i:=1 to ThreadCount do begin
    // call threads DoConnect methode
    Result := (ThreadInfo[i]^.Thread as TBaseSubThread).DoConnect;
    if Result then begin
       ThreadInfo[i]^.State := psConnected;
      // just to allow task switching to perform the connection to NamedPipe of TIPCServer class
      Sleep(100);
    end else
      Break;            // break if somethings where wrong
  end;
end;
//-----------------------------------------------------------------------------
function TBaseMain.CreateThreads: Boolean;
var
  i: Integer;
begin
  Result := True;
  for i:=1 to ThreadCount do
    with mSubSystemDef.Threads[i] do begin
      Result := CreateThread(Thread, ThreadTyp);
      State  := psAvailable;
      if Result then begin
        ( Thread as TBaseThread ).Params := MMSettings;
        Result := (Thread as TBaseThread).Init;
        if not Result then
          Break;
      end;
      // just to allow for task switching
      Sleep(1);
    end;
end;
//-----------------------------------------------------------------------------
// Just for easy access to the thread info variable
function TBaseMain.GetThreadInfo(aIndex: Integer): PThreadDef;
begin
  if (mSubSystemDef.ThreadCount > 0) and (aIndex <= mSubSystemDef.ThreadCount) then
    // old ... Result := @mSubSystemDef.Threads[aIndex]
    Result := @( mSubSystemDef.Threads[aIndex].Thread as TBaseSubThread ).ThreadDef
  else
    Result := Nil;
end;
//-----------------------------------------------------------------------------
function TBaseMain.Initialize: Boolean;
var
  xError: DWord;
begin
  try
    // init variables if somethings goes wrong
    Result := False;
    // Set the priority of the Process
    //  SetPriorityClass ( OpenProcess ( PROCESS_ALL_ACCESS, false, GetCurrentProcessId ),  NORMAL_PRIORITY_CLASS );
    mCurrentState := psCreated;
    // creates the own channel to receive commands from MMGuard
    mMMGuardRead  := TIPCServer.Create(cChannelNames[mSubSystemDef.ThreadTyp],  '', sizeof(TMMGuardRec), cChannelMainTimeOut, 0);
    // creates the connection to MMGuard
    mMMGuardWrite := TIPCClient.Create('.', cChannelNames[ttMMGuardMain]);

    if mMMGuardRead.Init then begin
      // try to connect to MMGuard
      if mMMGuardWrite.Connect then begin
        // now I can respond to MMGuard states
        Result := True;
        try
          fMMSettings := getParamHandle;
        except
          WriteLog ( etError, 'getParamHandle failed.' );
          fMMSettings := Nil;
        end;
        if Assigned ( fMMSettings ) then begin
          if fMMSettings.Init then begin
            if CreateThreads then begin
              // the threads where successful created
              xError := NO_ERROR;
              mCurrentState := psAvailable;
            end else begin
              WriteLog ( etError, 'Create of Threads failed.' );
              xError := ERROR_CAN_NOT_COMPLETE;
            end;
          end else begin
            WriteLog ( etError, 'Init of MMSettings failed. ' + fMMSettings.Error.Msg );
            xError := ERROR_CAN_NOT_COMPLETE;
          end;
        end else begin
          WriteLog ( etError, 'No Parameter Handle vailable.');
          xError := ERROR_CAN_NOT_COMPLETE;
        end;
        // notify MMGuard about state
        mMsg.ProcessID := GetCurrentProcessId;
        WriteMsgToMMGuard(gcAvailable, xError);
      end else
        xError := mMMGuardWrite.Error;
    end else
      xError := mMMGuardRead.Error;

    if xError <> NO_ERROR then begin
      Result := False;
      WriteLog(etError, 'Initialize failed in BaseMain. Error : ' + FormatErrorText(xError) );
    end;
    UpdateDebugMode;
  except
    on e:Exception do begin
      Result := False;
      WriteLog ( etError, 'Exception in Initialize of BaseMain. Error : ' + e.Message );
    end;
  end;
end;
//-----------------------------------------------------------------------------
// Main loop of the process. Runs until the Kill comamnd is received
procedure TBaseMain.Run;
var
  xCount: DWord;
  xError: DWord;
begin
  // initialize local variables
  xError := NO_ERROR;

  // loop until stop command received
  while not mTerminated do begin
    // read from Channel to receive commands from MMGuard or confirms from threads
    if mMMGuardRead.Read(@mMsg, SizeOf(mMsg), xCount) then begin

      case mMsg.MsgTyp of
        // from MMGuard to connect threads
        gcDoConnect: begin        // > ConnectDone > Start
            if mCurrentState = psAvailable then begin
              // set to next process state
              mCurrentState := psConnected;
              // initialize all threads
              if not ConnectThreads then
                xError := ERROR_CAN_NOT_COMPLETE;
            end else
              xError := ERROR_INVALID_FUNCTION;
            WriteMsgToMMGuard(gcConnected, xError);
          end;

        // from MMGuard to resume threads to do the init jobs
        gcDoInit: begin          // > ThreadStarted
            if mCurrentState = psConnected then begin
              // set to next process state
              mCurrentState := psInit;
              // let the thread running
              ResumeThreads;
              // remember command time to check start timeout
              mCommandTime  := GetTickCount;
              // after all, set PipeState from MMGuard to non blocking mode to check threads startup
              mMMGuardRead.PipeWait := False;
            end else
              WriteMsgToMMGuard(gcInitialized, ERROR_INVALID_FUNCTION);
          end;

        // from each thread after resume or initialize jobs
        gcInitialized: begin
            if mCurrentState = psInit then begin
              if CheckThreadState( psInit ) then begin
//                UpdateDebugMode;
                // if all threads started up, reset ChannelState to WAIT_FOREVER
                mMMGuardRead.PipeWait := True;
                // notify MMGuard for successfully start of all threads
                WriteMsgToMMGuard(gcInitialized, NO_ERROR);
              end;
            end else
              WriteMsgToMMGuard(gcInitialized, ERROR_INVALID_FUNCTION);
          end;

        // from MMGuard
        gcDoRun: begin
            if mCurrentState = psInit then begin
              // set the SystemReady property
              SetSystemRun;
              WriteMsgToMMGuard( gcRun, NO_ERROR );
              mCurrentState := psRun;
            end else
              WriteMsgToMMGuard(gcInitialized, ERROR_INVALID_FUNCTION);
          end;
        // from MMGuard to stop all threads
        gcKillSelf: begin
//            SuspendThreads;
//            Beep;
            mTerminated := True;
          end;
        gcDebug: begin
            UpdateDebugMode;
          end;
        gcResetHandler: begin
            CleanUp;
            Initialize;
          end;

        // general messages from threads (mains)
        gcRestartMM: begin
            // notify MMGuard to restart Millmaster
            WriteMsgToMMGuard(gcRestartMM, mMsg.Error.Error);
          end;
      else
      end;
    end else begin
      xError := ProcessReadError;
      Sleep(10);
    end;
  end;  // while

  // notify MMGuard about to stop myself (not necessary)
  WriteMsgToMMGuard(gcKillInProgress, xError);
  Sleep(100);
end;
//-----------------------------------------------------------------------------
// ReadChannel failed after timeout
function TBaseMain.ProcessReadError: DWord;
begin
  Result := NO_ERROR;
  case mMMGuardRead.Error of
    // incomming pipe from MMGuard has state PIPE_NOWAIT to detect startup timeout from threads
    ERROR_NO_DATA: begin
        // it is not really an error
        if mCurrentState = psInit then begin
          // not all threads have confirmed its resume
          if (GetTickCount - mCommandTime) > cThreadStartupTimeout then begin
            // set to next process state
            mCurrentState := psCreated;
            // notify MMGuard about error
            WriteMsgToMMGuard(gcAvailable, ERROR_NOT_READY);
            // set pipe state to PIPE_WAIT mode
            mMMGuardRead.PipeWait := True;
          end;
        end else
          // we are in the wrong movie ;-)
          mMMGuardRead.PipeWait := True;
      end;
    ERROR_BROKEN_PIPE: begin
        if not WriteMsgToMMGuard(gcRestartMM, ERROR_BROKEN_PIPE) then begin
          mCurrentState := psCreated;
        end;
      end;
  else
  end;
end;
//-----------------------------------------------------------------------------
procedure TBaseMain.ResumeThreads;
var
  i: Integer;
begin
  for i:=1 to ThreadCount do begin
    with ThreadInfo[i]^ do begin
      State := psConnected;
      Thread.Resume;
    end;
    Sleep(200);
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseMain.SetSystemRun;
var
  i: Integer;
begin
  for i:=1 to ThreadCount do
    with ThreadInfo[i]^ do
       State := psRun;
end;
//------------------------------------------------------------------------------
procedure TBaseMain.UpdateDebugMode;
var
  i: Integer;
  xStr: String;
begin
  // enable/diable CodeSite dependencies from registry
  xStr := ChangeFileExt(ExtractFileName(cApplicationNames[mSubSystemTyp]), '');
  CodeSite.Enabled := CodeSiteEnabled(xStr);
  CodeSite.SendString('CodeSite.Enabled', cApplicationNames[mSubSystemTyp]);
{
  CodeSite.DestinationDetails := CodeSiteConnectionString;
  xStr := ChangeFileExt(ExtractFileName(cApplicationNames[mSubSystemTyp]), '');
  CodeSite.Enabled := CodeSiteEnabled(xStr);
  CodeSite.SendString('DestinationDetails', CodeSite.DestinationDetails);
{}
//  CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMMDebug, xStr, False);

  // forward debug information to all SubThreads
  for i:=1 to ThreadCount do
    with ThreadInfo[i]^ do
      (Thread as TBaseSubThread).DebugMode := CodeSite.Enabled;
end;
//-----------------------------------------------------------------------------
procedure TBaseMain.SetThreadCount(const aValue: SmallInt);
begin
  if (aValue >=0) and (aValue <= cMaxThreadDef) then
    mSubSystemDef.ThreadCount := aValue;
end;
//-----------------------------------------------------------------------------
procedure TBaseMain.SuspendThreads;
var
  i: Integer;
begin
  for i:=1 to ThreadCount do
    with ThreadInfo[i]^ do
      (Thread as TBaseSubThread).Suspend;
end;
//-----------------------------------------------------------------------------
procedure TBaseMain.WriteLog(aEvent: TEventType; aText: String; aBuf: PByte; aCount: DWord);
begin
  if Assigned(mEventLog) then
    if Assigned(aBuf) and (aCount > 0) then
      mEventLog.WriteBin(aEvent, aText, aBuf, aCount)
    else
      mEventLog.Write(aEvent, aText);
end;
//-----------------------------------------------------------------------------
// writes the local msg buffer to MMGuard. MsgTyp MUST be set befor
function TBaseMain.WriteMsgToMMGuard(aCommand: TMMGuardCommand; aError: DWord): Boolean;
begin
  mMsg.MsgTyp      := aCommand;
  mMsg.Error.Error := aError;
  if aError = NO_ERROR then mMsg.Error.ErrorTyp := etNoError
                       else mMsg.Error.ErrorTyp := etMMError;
  // idetify the msg to MMGuard
  mMsg.Transmitter := mSubSystemTyp;

  Result := mMMGuardWrite.Write(@mMsg, SizeOf(mMsg));
  if not Result then
    WriteLog(etError, Format('Write to MMGuard failed: %s', [FormatErrorText(mMMGuardWrite.Error)]));
end;
//-----------------------------------------------------------------------------



//*****************************************************************************
//*****************************************************************************

end.

