(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobQueueClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.07.1999  1.00  Mg  | Datei erstellt
| 22.03.2004  1.00  Wss | Anpassungen an neue IPC mit dynamischem JobBuffer
|=========================================================================================*)
unit JobQueueClass;

interface
uses
  Classes, Windows, SysUtils,
  IPCClass, BaseGlobal;
type
//..............................................................................
  TJobQueuePriority = (qpLow, // the Job has a low priotity on JobQueue
                       qpNormal, // dito Normal
                       qpHigh, // dito High
                       qpExpress); // if there is an Thread defined waiting with
                                   // getExpressJob this Job will be processed of
                                   // this thread immediately
//..............................................................................
  PJobQueueRec = ^TJobQueueRec;
  TJobQueueRec = record
    Priority: TJobQueuePriority; // the priority of the Job in Queue
//    Job      : PJobRec;           // contains the Job received from JobController/MsgController
    Job: TJobRec; // contains the Job received from JobController/MsgController
  end;
//..............................................................................
  TJobQueue = class(TProtectedList)
  private
    mEvent: TEvent;
    mExpressEvent: TEvent;
    fError: TErrorRec;
    function getNextJob(var aJob: PJobRec; var aJobFound: boolean; aExpressJob: boolean): boolean;
    function insertJob(aJob: PJobRec; aPriority: TJobQueuePriority): boolean;
  public
    constructor Create; override;
    destructor Destroy; override;
    function Init: boolean;
    function getJob(var aJob: PJobRec): boolean;
    (* The function returns when a Job has been read from the Queue.
       It always searches the Job with highest priority.
       getJob returns also Jobs with Express priority.
       If an error occured the result will be false.
    *)
    function getExpressJob(var aJob: PJobRec): boolean;
    (* The function returns when a Job with Express priority has been read from the Queue.
       The function does not return if a non Express Job is in the Queue.
       If an error occured the result will be false.
    *)
    function putJob(aJob: PJobRec; aPriority: TJobQueuePriority): boolean;
    (* The function inserts a Job with the Priority into the Queue.
       If an error occured the result will be false.
    *)
    function viewAllJobs(var aJobIDList: TJobIDList; var aNumJobs: Word): boolean;
    (* The function returns all actual Jobs in Queue.
       If an error occured the result will be false.
    *)
    property QueueError: TErrorRec read fError;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//------------------------------------------------------------------------------
constructor TJobQueue.Create;
begin
  inherited Create;
  mEvent := TEvent.Create('', False);
  mExpressEvent := TEvent.Create('', False);
  fError.ErrorTyp := etNoError;
  fError.Error := NO_ERROR;
  fError.Msg := '';
end;
//------------------------------------------------------------------------------
destructor TJobQueue.Destroy;
begin
  mEvent.Free;
  mExpressEvent.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TJobQueue.Init: boolean;
begin
  Result := mEvent.Init(False);
  if Result then
    Result := mExpressEvent.Init(False);
  if not Result then
    fError := SetError(mEvent.Error, etNTError, 'Init of TJobQueue failed.');
end;
//------------------------------------------------------------------------------
function TJobQueue.insertJob(aJob: PJobRec; aPriority: TJobQueuePriority): boolean;
var
  xJobQueueRec: PJobQueueRec;
begin
  Result := False;
  try
    // prepare the Queue record
    GetMem(xJobQueueRec, aJob^.JobLen + sizeof(aPriority));
    xJobQueueRec^.Priority := aPriority;
// TODO: XML: kontrollieren
    System.Move(aJob^, xJobQueueRec^.Job, aJob.JobLen);
//      System.Move ( aJob^, xJobQueueRec^.Job, aJob.JobLen );
    // put the prepared record into the Queue
    LockList;
    try
      Add(xJobQueueRec);
      Result := True;
    finally
      UnlockList;
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TJobQueue : putJob failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TJobQueue.getNextJob(var aJob: PJobRec; var aJobFound: boolean; aExpressJob: boolean): boolean;
var
  xPriority: TJobQueuePriority;
  i: integer;
  xJobQueueRec: PJobQueueRec;
  xLen: Integer;
  xJobDataSize: integer;
begin
  Result    := True;
  aJobFound := False;
  LockList;
  try
    try
      // search the Job with highest priority in Queue. Begin at the end of Queue.
      for xPriority := qpExpress downto qpLow do begin
        for i := 0 to Count - 1 do begin
          xJobQueueRec := Items[i];


          (*---------------------------------------------------------
            evt. Fehler beim Zugriff auf QueueList
            Protokoll:
              Worker: TBaseWorker.getNextJob failed. MM Error: 1: Incorrect function.   TJobQueue : getJob failed.
                      Access violation at address 00401CCA in module 'STORAGEHANDLER.EXE'. Write of address 61747369
            12.05.2005 Lok/Wss/Khp/Nue
          ----------------------------------------------------------*)
          xLen := -1;
          xJobDataSize := -1;
          try
            if xJobQueueRec^.Priority = xPriority then begin
              // the highest priority job is found
              aJobFound := True;
              // wss: ich weiss ja nicht wieviel Platz der aJob Speicher zur Verf�gung stellt -> immer neu allozieren
              //nue: Verwendet jaaaaaa niiiiieee ReallocMemory -> funtioniert NICHT!!!!!!!!!!!!!!!!!!!!!!!! 28.10.04
              try
                xLen := xJobQueueRec^.Job.JobLen;
                xJobDataSize := GetJobDataSize(@xJobQueueRec^.Job);
                ReallocMem(aJob, xJobQueueRec^.Job.JobLen);
              except
                on e: Exception do
                  raise Exception.CreateFMT('ReallocMem (Len: %d, JobDataSize: %d): ' + e.Message, [xLen, xJobDataSize]);
              end;
              try
                System.Move(xJobQueueRec^.Job, aJob^, xLen);
              except
                on e: Exception do
                  raise Exception.CreateFMT('Move (Len: %d, JobDataSize: %d): ' + e.Message, [xLen, xJobDataSize]);
              end;
              try
                // Delete the Job from JobQueue
                FreeMem(xJobQueueRec, aJob^.JobLen + sizeof(xPriority));
              except
                on e: Exception do
                  raise Exception.CreateFMT('FreeMem (Len: %d, JobDataSize: %d): ' + e.Message, [xLen, xJobDataSize]);
              end;
              Delete(i);
              Break;
            end; // if xJobQueueRec^
          except
            on e: Exception do
              raise Exception.CreateFMT('xJobQueueRec^.Priority (%d, JobDataSize: %d): ' + e.Message, [xLen, xJobDataSize]);
          end;




        end; // for i
        if aJobFound or aExpressJob then
          Break;
      end; // for xPriority := qpExpress
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TJobQueue : getJob failed. ' + e.Message);
        Result := False;
      end;
    end;
  finally
    UnlockList;
  end;
end;
//------------------------------------------------------------------------------
function TJobQueue.getExpressJob(var aJob: PJobRec): boolean;
var
  xJobFound: boolean;
begin
  Result := True;
  xJobFound := False;
  while (Result and not xJobFound) do begin
    Result := getNextJob(aJob, xJobFound, True);
    if Result and not xJobFound then begin
      if mExpressEvent.WaitEvent then begin
        Result := getNextJob(aJob, xJobFound, True);
      end
      else begin
        fError := SetError(mEvent.Error, etNTError, 'TJobQueue : WaitEvent in getExpressJob failed.');
        Result := False;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TJobQueue.getJob(var aJob: PJobRec): boolean;
var
  xJobFound: boolean;
begin
  Result := True;
  xJobFound := False;
  while (Result and not xJobFound) do begin
    Result := getNextJob(aJob, xJobFound, False);
    if Result and not xJobFound then begin
      if mEvent.WaitEvent then begin
        Result := getNextJob(aJob, xJobFound, True);
      end
      else begin
        fError := SetError(mEvent.Error, etNTError, 'TJobQueue : WaitEvent in getJob failed.');
        Result := False;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TJobQueue.putJob(aJob: PJobRec; aPriority: TJobQueuePriority): boolean;
begin
  Result := insertJob(aJob, aPriority);
  if Result then begin
      // signal to one of waiting threads
    if not mEvent.SetEvent then begin
      fError := SetError(mEvent.Error, etNTError, 'TJobQueue : SetEvent in putJob failed.');
      Result := False;
    end;
    if aPriority = qpExpress then begin
      // if all threads waiting with getJob are working : signal to thread waiting with getExpressJob
      if not mExpressEvent.SetEvent then begin
        fError := SetError(mEvent.Error, etNTError, 'TJobQueue : SetEvent in putJob failed.');
        Result := False;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TJobQueue.viewAllJobs(var aJobIDList: TJobIDList; var aNumJobs: Word): boolean;
var
  xJobItem: integer;
  xJobQueueRec: PJobQueueRec;
begin
  Result := True;
  try
    aNumJobs := 0;
    FillChar(aJobIDList, sizeof(aJobIDList), 0);
    LockList;
    try
      for xJobItem := 0 to Count - 1 do begin
        if xJobItem < cMaxJobs then begin
          Inc(aNumJobs);
          xJobQueueRec := Items[xJobItem];
          aJobIDList[xJobItem].JobID  := xJobQueueRec^.Job.JobID;
          aJobIDList[xJobItem].JobTyp := ORD(xJobQueueRec^.Job.JobTyp);
        end;
      end;
    finally
      UnlockList;
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TJobQueue : viewAllJobs failed. ' + e.Message);
      Result := False;
    end;
  end;
end;

end.

