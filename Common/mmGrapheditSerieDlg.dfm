inherited mmGraphEditSeriesDlg: TmmGraphEditSeriesDlg
  Left = 341
  Top = 122
  ActiveControl = lbSeries
  BorderStyle = bsToolWindow
  Caption = '(80)Einstellungen fuer Datenreihen bearbeiten'
  ClientHeight = 360
  ClientWidth = 680
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TmmPanel
    Left = 0
    Top = 0
    Width = 680
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 0
    object GroupBox3: TmmGroupBox
      Left = 5
      Top = 5
      Width = 670
      Height = 320
      Caption = '(40)Parameter'
      TabOrder = 0
      object gbStatistik: TmmGroupBox
        Left = 450
        Top = 10
        Width = 210
        Height = 140
        Caption = '(40)Anzeige von Statistik-Daten'
        TabOrder = 5
        object clbStatData: TmmCheckListBox
          Left = 10
          Top = 17
          Width = 165
          Height = 115
          BorderStyle = bsNone
          Color = clSilver
          Ctl3D = False
          ItemHeight = 16
          Items.Strings = (
            '(20)Minima'
            '(20)Maxima'
            '(20)Mittelwert'
            '(20)Standard-Abweichung'
            '(20)Q90'
            '(20)Q95'
            '(20)Q99')
          ParentCtl3D = False
          Style = lbOwnerDrawFixed
          TabOrder = 0
          Visible = True
          OnClick = clbStatDataClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object rgYScaling: TmmRadioGroup
        Left = 230
        Top = 10
        Width = 210
        Height = 140
        Caption = '(40)Anzeige-Faktor'
        Columns = 3
        Items.Strings = (
          '1x'
          '2x'
          '5x'
          '10x'
          '50x'
          '100x'
          '500x'
          '1000x'
          '1/2x'
          '1/10x'
          '1/20x'
          '1/100x'
          '1/200x'
          '1/1000x'
          '1/2000x')
        TabOrder = 1
        OnClick = rgYScalingClick
      end
      object mmGroupBox3: TmmGroupBox
        Left = 450
        Top = 235
        Width = 210
        Height = 75
        Caption = '(20)Untere Grenze'
        TabOrder = 7
        object mmLabel4: TmmLabel
          Left = 10
          Top = 48
          Width = 125
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Grenzwert'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbLowerLimit: TmmCheckBox
          Left = 10
          Top = 20
          Width = 190
          Height = 17
          Caption = '(30)Grenze anzeigen'
          TabOrder = 0
          Visible = True
          OnClick = cbLowerLimitClick
          AutoLabel.LabelPosition = lpLeft
        end
        object edLowerLimit: TNumEdit
          Left = 140
          Top = 45
          Width = 60
          Height = 21
          Decimals = 2
          Digits = 12
          Masks.PositiveMask = '#,##0'
          NumericType = ntGeneral
          TabOrder = 1
          UseRounding = True
          Validate = False
          ValidateString = 
            '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
            's %s'
          OnExit = edLowerLimitExit
        end
      end
      object rgChartType: TmmRadioGroup
        Left = 230
        Top = 155
        Width = 210
        Height = 60
        Caption = '(40)Grafiktyp'
        Items.Strings = (
          '(35)Liniendiagramm'
          '(35)Balkendiagramm')
        TabOrder = 2
        OnClick = rgChartTypeClick
      end
      object gbColor: TmmGroupBox
        Left = 230
        Top = 220
        Width = 210
        Height = 45
        Caption = '(40)Farbauswahl'
        TabOrder = 3
        object paColor: TmmPanel
          Left = 10
          Top = 17
          Width = 190
          Height = 20
          Hint = '(*)Mit Mausklick Farbauswahl oeffnen'
          BevelInner = bvRaised
          BevelOuter = bvLowered
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = paColorClick
        end
      end
      object mmGroupBox2: TmmGroupBox
        Left = 450
        Top = 155
        Width = 210
        Height = 75
        Caption = '(20)Obere Grenze'
        TabOrder = 6
        object mmLabel3: TmmLabel
          Left = 10
          Top = 48
          Width = 125
          Height = 17
          Alignment = taRightJustify
          AutoSize = False
          Caption = '(30)Grenzwert'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbUpperLimit: TmmCheckBox
          Left = 10
          Top = 20
          Width = 190
          Height = 17
          Caption = '(30)Grenze anzeigen'
          TabOrder = 0
          Visible = True
          OnClick = cbUpperLimitClick
          AutoLabel.LabelPosition = lpLeft
        end
        object edUpperLimit: TNumEdit
          Left = 140
          Top = 45
          Width = 60
          Height = 21
          Decimals = 2
          Digits = 12
          Masks.PositiveMask = '#,##0'
          NumericType = ntGeneral
          TabOrder = 1
          UseRounding = True
          Validate = False
          ValidateString = 
            '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
            's %s'
          OnExit = edLowerLimitExit
        end
      end
      object gbVisible: TmmGroupBox
        Left = 230
        Top = 270
        Width = 210
        Height = 40
        TabOrder = 4
        object cbVisible: TmmCheckBox
          Left = 10
          Top = 15
          Width = 190
          Height = 17
          Caption = '(30)Datenreihe anzeigen'
          TabOrder = 0
          Visible = True
          OnClick = cbVisibleClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object lbSeries: TNWListBox
        Left = 10
        Top = 15
        Width = 210
        Height = 295
        ItemHeight = 13
        TabOrder = 0
        OnClick = lbSeriesClick
      end
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 330
    Width = 680
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 440
      Top = 2
      Width = 75
      Height = 25
      Action = acOK
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 520
      Top = 2
      Width = 75
      Height = 25
      Action = acCancel
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 600
      Top = 2
      Width = 75
      Height = 25
      Action = acHelp
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 112
    Top = 34
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Dialog schliessen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 1
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfe aufrufen...'
      ImageIndex = 2
      ShortCut = 112
      OnExecute = acHelpExecute
    end
  end
  object ColorDialog1: TmmColorDialog
    Ctl3D = True
    Left = 144
    Top = 34
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 176
    Top = 34
    TargetsData = (
      1
      6
      (
        ''
        'Hint'
        0)
      (
        ''
        'Text'
        0)
      (
        ''
        'Caption'
        0)
      (
        ''
        'Tabs'
        0)
      (
        ''
        'Items'
        0)
      (
        ''
        'Lines'
        0))
  end
end
