inherited AskClose: TAskClose
  Left = 774
  Top = 106
  Caption = '(50)Stopp von Datenerfassung bestaetigen'
  ClientHeight = 254
  PixelsPerInch = 96
  TextHeight = 13
  object mmImage1: TmmImage [0]
    Left = 0
    Top = 0
    Width = 418
    Height = 254
    Align = alClient
    Stretch = True
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmImage2: TmmImage [1]
    Left = 0
    Top = 0
    Width = 418
    Height = 254
    Align = alClient
    Stretch = True
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lStyleName: TmmLabel [2]
    Left = 195
    Top = 179
    Width = 9
    Height = 16
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel1: TmmLabel [3]
    Left = 11
    Top = 40
    Width = 401
    Height = 25
    AutoSize = False
    Caption = '(40)Soll die Datenerfassung gestoppt werden ?'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel [4]
    Left = 11
    Top = 80
    Width = 176
    Height = 17
    AutoSize = False
    Caption = '(20)Maschine:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel [5]
    Left = 11
    Top = 112
    Width = 176
    Height = 16
    AutoSize = False
    Caption = '(20)Spulstellenbereich:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel4: TmmLabel [6]
    Left = 11
    Top = 144
    Width = 176
    Height = 16
    AutoSize = False
    Caption = '(20)Partie:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lMachine: TmmLabel [7]
    Left = 195
    Top = 80
    Width = 217
    Height = 17
    AutoSize = False
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lSpindleRange: TmmLabel [8]
    Left = 195
    Top = 112
    Width = 217
    Height = 17
    AutoSize = False
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lProdGrpName: TmmLabel [9]
    Left = 195
    Top = 144
    Width = 217
    Height = 17
    AutoSize = False
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object lStyle: TmmLabel [10]
    Left = 11
    Top = 179
    Width = 176
    Height = 16
    Caption = '(20)Artikel:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Transparent = True
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  inherited bOK: TmmButton
    Top = 219
    Action = acOK
  end
  inherited bCancel: TmmButton
    Top = 219
    Action = acCancel
  end
  object mmButton1: TmmButton
    Left = 13
    Top = 219
    Width = 121
    Height = 25
    Action = acSecurity
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmActionList1: TmmActionList
    Left = 384
    Top = 8
    object acOK: TAction
      Caption = 'OK'
      Hint = 'OK'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(9)Abbrechen'
      Hint = '(9)Abbrechen'
      OnExecute = acCancelExecute
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      Hint = '(*)Zugriffkontrolle'
      OnExecute = acSecurityExecute
    end
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(50)Stopp von Datenerfassung bestaetigen'
    Active = True
    Left = 352
    Top = 8
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 320
    Top = 8
    TargetsData = (
      1
      2
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0))
  end
end
