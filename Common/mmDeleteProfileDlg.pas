(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDeleteProfileDlg.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.99 | 0.01 | PW  | File created
| 05.10.02 |        LOK | Umbau ADO
|=========================================================================================*)

unit mmDeleteProfileDlg;
interface
uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Mask, Tabs, DB, DBCtrls, ComCtrls,
  IvDictio, IvMulti, IvEMulti, mmImageList, ActnList, mmToolBar, mmActionList,
  mmCommonLib, MemoINI,
  NwCombo, mmPanel, ImgList, ToolWin, IvMlDlgs, mmButton, BaseForm, mmTranslator;

type
  TmmDeleteProfileEditor = class(TmmForm)
    mmTranslator1: TmmTranslator;

    ActionList1: TmmActionList;
    acClose: TAction;
    acDelete: TAction;
    acHelp: TAction;

    mmPanel1: TmmPanel;
    lbUser: TNWListBox;

    paButtons: TmmPanel;
    bClose: TmmButton;
    bDelete: TmmButton;
    bHelp: TmmButton;

    procedure FormShow(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure lbUserDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
    procedure acHelpExecute(Sender: TObject);
  private
    FMemoINI: TMemoINI;
    procedure Init;
    procedure EnableActions;
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
  end;                                  //TmmDeleteProfileEditor

var
  mmDeleteProfileEditor: TmmDeleteProfileEditor;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmDialogs, mmLib;

{$R *.DFM}

resourcestring
  cMsg1 = '(*)Profil %s wird jetzt geloescht. Weitermachen ?'; //ivlm

constructor TmmDeleteProfileEditor.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
end;                                    //constructor TmmDeleteProfileEditor.Create
//-----------------------------------------------------------------------------
destructor TmmDeleteProfileEditor.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TmmDeleteProfileEditor.Destroy
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.FormShow(Sender: TObject);
begin
  Init;
end;                                    //procedure TmmDeleteProfileEditor.FormShow
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.acCloseExecute(Sender: TObject);
begin
  Close;
  ModalResult := mrOK;
end;                                    //procedure TmmDeleteProfileEditor.acCloseExecute
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.acDeleteExecute(Sender: TObject);
begin
  if MMMessageDlg(format(cMsg1, [lbUser.GetItemText]), mtConfirmation, [mbYes, mbNo], 0, TWinControl(Owner)) <> mrYes then
    exit;
// if MMMessageDlg(format(cMsg1,[lbUser.GetItemText]),mtConfirmation,[mbYes,mbNo],0, Self) <> mrYes then exit;

  FMemoINI.Profilename := lbUser.GetItemText;
  FMemoINI.DeleteProfile;
  Init;
end;                                    //procedure TmmDeleteProfileEditor.acDeleteExecute
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.lbUserDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
const
  cOffset = 5;
  cProfilePos = 20;

var
  xIsSelected: boolean;
  xProfile: string;
  xDefault: boolean;

begin
  with (Control as TNWListBox).Canvas do begin
    xIsSelected := odSelected in state;
    FillRect(Rect);
    if xIsSelected then Font.Color := clHighlightText
    else Font.Color := clWindowText;

    xProfile := (Control as TNWListBox).Items[Index];
    xDefault := (Control as TNWListBox).GetItemIDAtPos(index) = 1;

    if xDefault then Font.Style := [fsBold]
    else Font.Style := [];

    if xDefault then TextOut(Rect.Left + cOffset, Rect.Top, '*');
    TextOut(Rect.Left + cProfilePos, Rect.Top, xProfile);
  end;                                  //with (Control as TNWListBox).Canvas do
end;                                    //procedure TmmDeleteProfileEditor.lbUserDrawItem
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.Init;
var
  i: integer;
  xProfile,
    xUser: string;
  xDefault: boolean;
  xList: TStringList;

begin
  lbUser.Items.BeginUpdate;
  lbUser.Items.Clear;
  xList := TStringList.Create;
  xList.AddStrings(FMemoINI.GetFormProfiles);
  try
    for i := 0 to xList.Count - 1 do begin
      xProfile := GetSubStr(xList[i], cListDelimiterChar, 1);
      xUser := GetSubStr(xList[i], cListDelimiterChar, 2);
      xDefault := GetSubStr(xList[i], cListDelimiterChar, 3) = '1';
      if FMemoINI.Username = xUser then lbUser.AddObj(xProfile, ord(xDefault));
    end;                                //for i := 0 to FProfiles.Count -1 do
  finally
    xList.Free;
    lbUser.Items.EndUpdate;
  end;                                  //xList

  EnableActions;
  if acDelete.Enabled then lbUser.ItemIndex := 0;
end;                                    //procedure TSelectProfileDlg.Init
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.EnableActions;
begin
  acDelete.Enabled := lbUser.Items.Count > 0;
end;                                    //procedure TmmDeleteProfileEditor.EnableActions
//-----------------------------------------------------------------------------
procedure TmmDeleteProfileEditor.acHelpExecute(Sender: TObject);
begin
  inherited;
 //
end;                                    //procedure TmmDeleteProfileEditor.acHelpExecute

end. //mmDeleteProfileDlg.pas

