unit MMICOMLib_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 16.01.2001 14:17:49 from Type Library described below.

// ************************************************************************ //
// Type Lib: C:\Program Files\mmdev\MMICOM.tlb (1)
// IID\LCID: {D3A6DF40-4484-11D1-BFAD-32E1A3000000}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINNT\SYSTEM32\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINNT\System32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  MMICOMLibMajorVersion = 1;
  MMICOMLibMinorVersion = 0;

  LIBID_MMICOMLib: TGUID = '{D3A6DF40-4484-11D1-BFAD-32E1A3000000}';

  DIID__MMIEvents: TGUID = '{F53D2F91-43BD-11D1-BFAB-3641DE000000}';
  IID_IMMICtl: TGUID = '{D3A6DF50-4484-11D1-BFAD-32E1A3000000}';
  CLASS_MMICtl: TGUID = '{F53D2F90-43BD-11D1-BFAB-3641DE000000}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  _MMIEvents = dispinterface;
  IMMICtl = interface;
  IMMICtlDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  MMICtl = IMMICtl;


// *********************************************************************//
// Declaration of structures, unions and aliases.                         
// *********************************************************************//
  PInteger1 = ^Integer; {*}


// *********************************************************************//
// DispIntf:  _MMIEvents
// Flags:     (4096) Dispatchable
// GUID:      {F53D2F91-43BD-11D1-BFAB-3641DE000000}
// *********************************************************************//
  _MMIEvents = dispinterface
    ['{F53D2F91-43BD-11D1-BFAB-3641DE000000}']
    procedure MessageSent(lIndex: Integer; lMessageType: Integer; 
                          const bstrWorkstation: WideString; lStatus: Integer; 
                          lProtocolResult: Integer; lSplittingParts: Integer; 
                          lResendAttempts: Integer; lDuration: Integer; 
                          dateTransmissionTime: TDateTime; const bstrInfo: WideString); dispid 1;
    procedure MessageInfo(lCurrentStatus: Integer; const bstrInfoString: WideString; 
                          const bstrWorkstation: WideString); dispid 2;
    procedure MessageIncoming(lMessageType: Integer; const bstrOriginator: WideString; 
                              const bstrDestinator: WideString; dateTimeStamp: TDateTime; 
                              const bstrMessageString: WideString; const bstrInfo: WideString); dispid 3;
  end;

// *********************************************************************//
// Interface: IMMICtl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D3A6DF50-4484-11D1-BFAD-32E1A3000000}
// *********************************************************************//
  IMMICtl = interface(IDispatch)
    ['{D3A6DF50-4484-11D1-BFAD-32E1A3000000}']
    function  MessageSend(var plIndex: Integer; const bstrService: WideString; 
                          const bstrDestinator: WideString; const bstrOriginator: WideString; 
                          dateTimeToSend: TDateTime; lOptions: Integer; 
                          const bstrMessage: WideString): Integer; safecall;
    function  LibraryConfigDlg(lParentWnd: Integer; out plChanges: Integer): Integer; safecall;
    function  MessageDelete(lIndex: Integer; out plCount: Integer): Integer; safecall;
    function  SpoolerStart(var plSpoolerType: Integer): Integer; safecall;
    function  SpoolerStop(var plSpoolerType: Integer): Integer; safecall;
    function  SpoolerTransmitNow(var plSpoolerType: Integer): Integer; safecall;
    function  SpoolerGetStatus(lSpoolerType: Integer; out plStatus: Integer; 
                               out plMessageType: Integer; out plMessages: Integer): Integer; safecall;
    function  ServiceGetNames(out pbstrServices: WideString; out plCount: Integer): Integer; safecall;
    function  ServiceGetProps(const bstrServiceName: WideString; out plMessageType: Integer; 
                              out pbstrProperties: WideString): Integer; safecall;
    function  LibraryGetName(out pbstrLibName: WideString): Integer; safecall;
    function  LibraryGetAPIVersion(out plAPIVersion: Integer): Integer; safecall;
    function  LibrarySetLicenseCode(const bstrCode: WideString; out plLicenses: Integer): Integer; safecall;
    function  SyncInit(lTimeOut: Integer): Integer; safecall;
    function  SyncExit(lTimeOut: Integer): Integer; safecall;
    function  LibraryConfigDlgEx(lParentWnd: Integer; lType: Integer; 
                                 const bstrPrameter: WideString; out plChanges: Integer): Integer; safecall;
    procedure AboutBox; safecall;
  end;

// *********************************************************************//
// DispIntf:  IMMICtlDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D3A6DF50-4484-11D1-BFAD-32E1A3000000}
// *********************************************************************//
  IMMICtlDisp = dispinterface
    ['{D3A6DF50-4484-11D1-BFAD-32E1A3000000}']
    function  MessageSend(var plIndex: Integer; const bstrService: WideString; 
                          const bstrDestinator: WideString; const bstrOriginator: WideString; 
                          dateTimeToSend: TDateTime; lOptions: Integer; 
                          const bstrMessage: WideString): Integer; dispid 1;
    function  LibraryConfigDlg(lParentWnd: Integer; out plChanges: Integer): Integer; dispid 3;
    function  MessageDelete(lIndex: Integer; out plCount: Integer): Integer; dispid 4;
    function  SpoolerStart(var plSpoolerType: Integer): Integer; dispid 5;
    function  SpoolerStop(var plSpoolerType: Integer): Integer; dispid 6;
    function  SpoolerTransmitNow(var plSpoolerType: Integer): Integer; dispid 7;
    function  SpoolerGetStatus(lSpoolerType: Integer; out plStatus: Integer; 
                               out plMessageType: Integer; out plMessages: Integer): Integer; dispid 8;
    function  ServiceGetNames(out pbstrServices: WideString; out plCount: Integer): Integer; dispid 9;
    function  ServiceGetProps(const bstrServiceName: WideString; out plMessageType: Integer; 
                              out pbstrProperties: WideString): Integer; dispid 10;
    function  LibraryGetName(out pbstrLibName: WideString): Integer; dispid 11;
    function  LibraryGetAPIVersion(out plAPIVersion: Integer): Integer; dispid 12;
    function  LibrarySetLicenseCode(const bstrCode: WideString; out plLicenses: Integer): Integer; dispid 13;
    function  SyncInit(lTimeOut: Integer): Integer; dispid 14;
    function  SyncExit(lTimeOut: Integer): Integer; dispid 15;
    function  LibraryConfigDlgEx(lParentWnd: Integer; lType: Integer; 
                                 const bstrPrameter: WideString; out plChanges: Integer): Integer; dispid 16;
    procedure AboutBox; dispid -552;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TMMICtl
// Help String      : MMICtl Class
// Default Interface: IMMICtl
// Def. Intf. DISP? : No
// Event   Interface: _MMIEvents
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TMMICtlMessageSent = procedure(Sender: TObject; lIndex: Integer; lMessageType: Integer; 
                                                  const bstrWorkstation: WideString; 
                                                  lStatus: Integer; lProtocolResult: Integer; 
                                                  lSplittingParts: Integer; 
                                                  lResendAttempts: Integer; lDuration: Integer; 
                                                  dateTransmissionTime: TDateTime; 
                                                  const bstrInfo: WideString) of object;
  TMMICtlMessageInfo = procedure(Sender: TObject; lCurrentStatus: Integer; 
                                                  const bstrInfoString: WideString; 
                                                  const bstrWorkstation: WideString) of object;
  TMMICtlMessageIncoming = procedure(Sender: TObject; lMessageType: Integer; 
                                                      const bstrOriginator: WideString; 
                                                      const bstrDestinator: WideString; 
                                                      dateTimeStamp: TDateTime; 
                                                      const bstrMessageString: WideString; 
                                                      const bstrInfo: WideString) of object;

  TMMICtl = class(TOleControl)
  private
    FOnMessageSent: TMMICtlMessageSent;
    FOnMessageInfo: TMMICtlMessageInfo;
    FOnMessageIncoming: TMMICtlMessageIncoming;
    FIntf: IMMICtl;
    function  GetControlInterface: IMMICtl;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    function  MessageSend(var plIndex: Integer; const bstrService: WideString; 
                          const bstrDestinator: WideString; const bstrOriginator: WideString; 
                          dateTimeToSend: TDateTime; lOptions: Integer; 
                          const bstrMessage: WideString): Integer;
    function  LibraryConfigDlg(lParentWnd: Integer; out plChanges: Integer): Integer;
    function  MessageDelete(lIndex: Integer; out plCount: Integer): Integer;
    function  SpoolerStart(var plSpoolerType: Integer): Integer;
    function  SpoolerStop(var plSpoolerType: Integer): Integer;
    function  SpoolerTransmitNow(var plSpoolerType: Integer): Integer;
    function  SpoolerGetStatus(lSpoolerType: Integer; out plStatus: Integer; 
                               out plMessageType: Integer; out plMessages: Integer): Integer;
    function  ServiceGetNames(out pbstrServices: WideString; out plCount: Integer): Integer;
    function  ServiceGetProps(const bstrServiceName: WideString; out plMessageType: Integer; 
                              out pbstrProperties: WideString): Integer;
    function  LibraryGetName(out pbstrLibName: WideString): Integer;
    function  LibraryGetAPIVersion(out plAPIVersion: Integer): Integer;
    function  LibrarySetLicenseCode(const bstrCode: WideString; out plLicenses: Integer): Integer;
    function  SyncInit(lTimeOut: Integer): Integer;
    function  SyncExit(lTimeOut: Integer): Integer;
    function  LibraryConfigDlgEx(lParentWnd: Integer; lType: Integer; 
                                 const bstrPrameter: WideString; out plChanges: Integer): Integer;
    procedure AboutBox;
    property  ControlInterface: IMMICtl read GetControlInterface;
    property  DefaultInterface: IMMICtl read GetControlInterface;
  published
    property OnMessageSent: TMMICtlMessageSent read FOnMessageSent write FOnMessageSent;
    property OnMessageInfo: TMMICtlMessageInfo read FOnMessageInfo write FOnMessageInfo;
    property OnMessageIncoming: TMMICtlMessageIncoming read FOnMessageIncoming write FOnMessageIncoming;
  end;

procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 ComObj;

procedure TMMICtl.InitControlData;
const
  CEventDispIDs: array [0..2] of DWORD = (
    $00000001, $00000002, $00000003);
  CControlData: TControlData2 = (
    ClassID: '{F53D2F90-43BD-11D1-BFAB-3641DE000000}';
    EventIID: '{F53D2F91-43BD-11D1-BFAB-3641DE000000}';
    EventCount: 3;
    EventDispIDs: @CEventDispIDs;
    LicenseKey: nil (*HR:$80040154*);
    Flags: $00000000;
    Version: 401);
begin
  ControlData := @CControlData;
  TControlData2(CControlData).FirstEventOfs := Cardinal(@@FOnMessageSent) - Cardinal(Self);
end;

procedure TMMICtl.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IMMICtl;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TMMICtl.GetControlInterface: IMMICtl;
begin
  CreateControl;
  Result := FIntf;
end;

function  TMMICtl.MessageSend(var plIndex: Integer; const bstrService: WideString; 
                              const bstrDestinator: WideString; const bstrOriginator: WideString; 
                              dateTimeToSend: TDateTime; lOptions: Integer; 
                              const bstrMessage: WideString): Integer;
begin
  Result := DefaultInterface.MessageSend(plIndex, bstrService, bstrDestinator, bstrOriginator, 
                                         dateTimeToSend, lOptions, bstrMessage);
end;

function  TMMICtl.LibraryConfigDlg(lParentWnd: Integer; out plChanges: Integer): Integer;
begin
  Result := DefaultInterface.LibraryConfigDlg(lParentWnd, plChanges);
end;

function  TMMICtl.MessageDelete(lIndex: Integer; out plCount: Integer): Integer;
begin
  Result := DefaultInterface.MessageDelete(lIndex, plCount);
end;

function  TMMICtl.SpoolerStart(var plSpoolerType: Integer): Integer;
begin
  Result := DefaultInterface.SpoolerStart(plSpoolerType);
end;

function  TMMICtl.SpoolerStop(var plSpoolerType: Integer): Integer;
begin
  Result := DefaultInterface.SpoolerStop(plSpoolerType);
end;

function  TMMICtl.SpoolerTransmitNow(var plSpoolerType: Integer): Integer;
begin
  Result := DefaultInterface.SpoolerTransmitNow(plSpoolerType);
end;

function  TMMICtl.SpoolerGetStatus(lSpoolerType: Integer; out plStatus: Integer; 
                                   out plMessageType: Integer; out plMessages: Integer): Integer;
begin
  Result := DefaultInterface.SpoolerGetStatus(lSpoolerType, plStatus, plMessageType, plMessages);
end;

function  TMMICtl.ServiceGetNames(out pbstrServices: WideString; out plCount: Integer): Integer;
begin
  Result := DefaultInterface.ServiceGetNames(pbstrServices, plCount);
end;

function  TMMICtl.ServiceGetProps(const bstrServiceName: WideString; out plMessageType: Integer; 
                                  out pbstrProperties: WideString): Integer;
begin
  Result := DefaultInterface.ServiceGetProps(bstrServiceName, plMessageType, pbstrProperties);
end;

function  TMMICtl.LibraryGetName(out pbstrLibName: WideString): Integer;
begin
  Result := DefaultInterface.LibraryGetName(pbstrLibName);
end;

function  TMMICtl.LibraryGetAPIVersion(out plAPIVersion: Integer): Integer;
begin
  Result := DefaultInterface.LibraryGetAPIVersion(plAPIVersion);
end;

function  TMMICtl.LibrarySetLicenseCode(const bstrCode: WideString; out plLicenses: Integer): Integer;
begin
  Result := DefaultInterface.LibrarySetLicenseCode(bstrCode, plLicenses);
end;

function  TMMICtl.SyncInit(lTimeOut: Integer): Integer;
begin
  Result := DefaultInterface.SyncInit(lTimeOut);
end;

function  TMMICtl.SyncExit(lTimeOut: Integer): Integer;
begin
  Result := DefaultInterface.SyncExit(lTimeOut);
end;

function  TMMICtl.LibraryConfigDlgEx(lParentWnd: Integer; lType: Integer; 
                                     const bstrPrameter: WideString; out plChanges: Integer): Integer;
begin
  Result := DefaultInterface.LibraryConfigDlgEx(lParentWnd, lType, bstrPrameter, plChanges);
end;

procedure TMMICtl.AboutBox;
begin
  DefaultInterface.AboutBox;
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TMMICtl]);
end;

end.
