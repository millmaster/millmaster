inherited mmLengthUnitsDlg: TmmLengthUnitsDlg
  Left = 549
  Top = 199
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = '(60)Datenformat waehlen'
  ClientHeight = 175
  ClientWidth = 245
  ParentFont = False
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TmmPanel
    Left = 0
    Top = 0
    Width = 245
    Height = 145
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 0
    object rgUnits: TmmRadioGroup
      Left = 5
      Top = 5
      Width = 235
      Height = 135
      Caption = '(40)Einheit'
      Items.Strings = (
        '1'
        '2'
        '3'
        '4'
        '5'
        '6')
      TabOrder = 0
      CaptionSpace = True
    end
  end
  object paButtons: TmmPanel
    Left = 0
    Top = 145
    Width = 245
    Height = 30
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object bOK: TmmButton
      Left = 5
      Top = 1
      Width = 75
      Height = 25
      Action = acOK
      Anchors = [akTop, akRight]
      Caption = '(9)OK'
      Default = True
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bCancel: TmmButton
      Left = 85
      Top = 1
      Width = 75
      Height = 25
      Action = acCancel
      Anchors = [akTop, akRight]
      Caption = '(9)Abbrechen'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bHelp: TmmButton
      Left = 165
      Top = 1
      Width = 75
      Height = 25
      Action = acHelp
      Anchors = [akTop, akRight]
      Caption = '(9)&Hilfe...'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object ActionList1: TmmActionList
    Left = 104
    Top = 18
    object acOK: TAction
      Caption = '(12)&OK'
      Hint = '(*)Dialog schliessen'
      ImageIndex = 0
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Caption = '(12)&Abbrechen'
      Hint = '(*)Dialog abbrechen'
      ImageIndex = 1
      ShortCut = 27
      OnExecute = acCancelExecute
    end
    object acHelp: TAction
      Caption = '(12)&Hilfe...'
      Hint = '(*)Hilfe aufrufen...'
      ImageIndex = 2
      OnExecute = acHelpExecute
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 136
    Top = 19
    TargetsData = (
      1
      5
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Text'
        0)
      (
        '*'
        'Lines'
        0))
  end
end
