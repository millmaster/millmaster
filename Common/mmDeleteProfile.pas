(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDeleteProfile.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 5
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0, Multilizer
|-------------------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.99 | 0.01 | PW  | File created
|
| 04.10.2002        LOK | Umbau ADO
|=========================================================================================*)

unit mmDeleteProfile;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  MemoINI;

type
  TmmDeleteProfile = class(TComponent)
  private
    FMemoINI: TMemoINI;
  protected
  public
    constructor Create(aOwner: TComponent; aMemoINI: TMemoINI); reintroduce;
    destructor Destroy; override;
    function Execute: boolean;
  published
  end;                                  //TmmDeleteProfile

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmDeleteProfileDlg;

constructor TmmDeleteProfile.Create(aOwner: TComponent; aMemoINI: TMemoINI);
begin
  inherited Create(aOwner);
  FMemoINI := aMemoINI;
end;                                    //constructor TmmDeleteProfile.Create
//-----------------------------------------------------------------------------
destructor TmmDeleteProfile.Destroy;
begin
  inherited Destroy;
end;                                    //destructor TmmDeleteProfile.Destroy
//-----------------------------------------------------------------------------
function TmmDeleteProfile.Execute: boolean;
begin
  if Owner is TWinControl then
    mmDeleteProfileEditor := TmmDeleteProfileEditor.Create(Owner, FMemoINI)
  else
    mmDeleteProfileEditor := TmmDeleteProfileEditor.Create(Application, FMemoINI);
//  mmDeleteProfileEditor := TmmDeleteProfileEditor.Create(Application,FMemoINI);
  try
    result := mmDeleteProfileEditor.ShowModal = IDOk;
  finally
    mmDeleteProfileEditor.Free;
  end;                                  //try..finally
end;                                    //function TmmDeleteProfile.Execute

end. //mmDeleteProfile.pas

