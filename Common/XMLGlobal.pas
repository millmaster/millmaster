(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: XMLGlobal.pas
| Projectpart...: MillMaster Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows NT/2000/XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.04.2004  1.00  Lok | Initial release
| 05.05.2005        Lok | Funktion 'FormatXML' eingef�hrt
| 26.05.2005        Nue | Funktion 'GetElementValueDef' eingef�hrt
| 22.07.2004        Lok | TMMStringBuilder eingef�hrt (Kommentar siehe Klassendeklaration).
| 19.05.2005        Lok | AttributeToFloat eingef�hrt
| 09.09.2005        Lok | Compiler Hints entfernt
| 29.03.2007        Nue | cXMLDefaults eingef�gt.
|=============================================================================*)
unit XMLGlobal;

interface
uses
  Classes, Sysutils, MSXML2_TLB, consts, BaseGlobal, windows;

type
  // Alias f�r XML Dokumente
  DomDocumentMM = DOMDocument40;

  (*---------------------------------------------------------
    Erkl�rung:
      Die Klasse dient zum Zusammenstellen eines Strings der
      mit Trennzeichen getrennt ist. Das Trennzeichen ist #3 = EndOfText.
      Diese zusammenstellung wird  beispielsweise in den Nethandlern gebraucht um
      mit dem Settingsrecord mehrere XML Fragmente zu verschicken (Settings, Bauzustand);

    Anwendung:
      Einzelstrings einf�llen:
        with TMMStringBuilder.Create do try
          Add(mTest);
          Add(cTest);
          xString := Text;
        finally
          free;
        end;

      Einzelstrings auslesen:
        with TMMStringBuilder.Create do try
          Text := xString;
          memoXMLSetting.Text := Strings[0] + cCrlf + Strings[1];
        finally
          free;
        end;
  ----------------------------------------------------------*)
  TMMStringBuilder = class(TObject)
  private
    FCount: Integer;
    FStrings: Array of string;
  protected
    function GetStrings(aIndex: Integer): string; virtual;
    function GetText: string; virtual;
    procedure SetText(const Value: string); virtual;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Add(aText: string): Integer; virtual;
    property Count: Integer read FCount;
    property Strings[aIndex: Integer]: string read GetStrings;
    property Text: string read GetText write SetText;
  end;

const
  cLoepfeNS              = 'http://www.loepfe.com/xmlns/mmschema';
  cLNSName               = 'loepfe';
  cLNS                   = cLNSName + ':';
  cLoepfeNameSpace       = 'xmlns:' + cLNSName + '="' + cLoepfeNS  + '"';

  cSchemaNS              = 'http://www.w3.org/2001/XMLSchema';
  cSchemaNSName          = 'xs';
  cSNS                   = cSchemaNSName  + ':';
  cSchemaNameSpace       = 'xmlns:' + cSchemaNSName + '="' + cSchemaNS + '"';

  cMMStringBuilderDelimiter = #3; // ETX = End of Text
  cXMLLoepfeBody = 'LoepfeBody';
  cXMLDefaults = 'Defaults';
  cXMLHeader = '<?xml version="1.0" encoding="UTF-8"?>' +
               '<' + cXMLLoepfeBody + '>';
  cXMLFooter = '</' + cXMLLoepfeBody + '>';

// Pr�ft ob ein Element ein bestimmtes Attribut besitzt
function AttributExists(aAttrName: string; aAttrMap: IXMLDOMNamedNodeMap): boolean;

(* Gibt das benannte Attribut als String zur�ck.
   Der R�ckgabewert sagt ob ein Attribut vorhanden ist oder nicht *)
function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;


// Gibt das benannte Attribut als Integer zur�ck.
function AttributeToIntDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: integer): integer;

// Gibt das benannte Attribut als Boolean zur�ck.
function AttributeToBoolDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: boolean): boolean;

// Gibt das benannte Attribut als string zur�ck.
function AttributeToStringDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: string): string;

// Gibt das benannte Attribut als Float zur�ck.
function AttributeToFloatDef(aAttrName: string; aElement: IXMLDOMNode;
    aDefault: extended): extended;

// Gibt das benannte Attribut als string zur�ck.
function AttributeToVariantDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: Variant): Variant;

// Gibt den Value (erster childNode) eines Elements zur�ck
function GetElementValueDef(aNode: IXMLDOMNode; aDefault:Variant):Variant;


// Formatiert ein XML Element f�r die Ausgabe
function FormatXML(aNode: IXMLDOMElement; aIndentString: string = '  '): string; overload;
function FormatXML(aDoc: DOMDocumentMM; aIndentString: string = '  '): string; overload;
function FormatXML(aXMLStream: string; aIndentString: string = '  '): string; overload;
function FormatXML(aNodeList: IXMLDOMNodeList; aIndentString: string = '  '): string; overload;

// L�dt ein Text File in einen String
function LoadXMLFile(aFileName: string): String;

//Erstellt aus einem XMLStream einen DOM-Baum und gibt diesen als Returnparameter zur�ck
function XMLStreamToDOM(aXMLStream: string): DOMDocumentMM;

// Erzeugt die Processing Informationen und das DocumentElement
procedure InitXMLDocument(var aDoc: DOMDocumentMM; aDocumentElement: string);

// Erzeugt anhand eines Selektions Pfades die gesammte Struktur bis zum Zielelement
function GetCreateElement(aElementPath: string; aDoc: DOMDocumentMM; aRootElement: IXMLDOMNode = nil): IXMLDOMElement;

// Verschiebt alle Kind Elemente von Source Element zum Targhet Element
procedure MoveChildsToNewElement(aSource: IXMLDOMNodeList; aTarget: IXMLDOMNode); overload;
procedure MoveChildsToNewElement(aSource: IXMLDOMNode; aTarget: IXMLDOMNode); overload;

// L�scht alle Knoten der Liste aus Ihrem Element
procedure removeNodes(aNodeList: IXMLDOMNodeList);

(* Gibt den XPath f�r aIndex Elemente von rechts.
   Bsp: '/LoepfeBody/YMSetting/Basic/Channel/Neps'
      Der Aufruf XPFromRight(2) liefert den String 'Basic/Channel/Neps' *)
function XPFromLeft(aName: string; aCount: integer): string;

// Erzeugt eine Instanz eines XML Dokuments
function DOMDocumentMMCreate: DOMDocumentMM;

(*  Vergleicht die untergeordneten Knoten auf Gleichheit im Namen und im Wert.
    Attribute werden nicht verglichen. *)
function SameNodeText(aNode1, aNode2: IXMLDOMNode): Boolean;

(* Gibt den XPath f�r aIndex Elemente von rechts.
   Bsp: '/LoepfeBody/YMSetting/Basic/Channel/Neps'
      Der Aufruf XPFromRight(2) liefert den String 'Basic/Channel/Neps' *)
function XPFromRight(aName: string; aCount: integer): string;

//   Entfernt einen Teil von einem globalen XPath.
function RemoveXPRoot(aXPToRemove: string; aSource: string): String;

// Ermittelt von einem Element den absoluten Selektionspfad.
function GetSelectionPath(aElement: IXMLDOMNode): string;
// Konvertiert einen Parse Error zu einem String
function FormatError(aParseError: IXMLDOMParseError): string;

//  F�gt einem bestehenden Element ein neues hinzu und gibt eine Referenz auf das neue Element zur�ck.
function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string): IXMLDOMElement; overload;
function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string; aElementValue: Variant): IXMLDOMElement; overload;

//  Generiert einen Hashcode �ber einen gegebenen Text nach dem Algorithmus von SDBM
function HashBySDBM(aText: string): Longint;
// Generiert zwei Hashcodes �ber einen gegebenen Text
procedure HashByMM(aText: string; out aHash1: Longint; out aHash2: Longint); overload;
procedure HashByMM(aElement: IXMLDOMElement; out aHash1: Longint; out aHash2: Longint); overload;

//  Generiert einen Hashcode �ber einen gegebenen Text nach dem Algorithmus von OCaml.
function HashByOCaml(aText: string): Longint;

// Z�hlt die Level eines Selektionspfades. DAbei werden lediglich die Slashes ('/') gez�hlt.
function XPLevelCount(aSelPath: string): integer;

// Gibt den Inhalt des Elements als Integer zur�ck.
function ElementToBoolDef(aElement: IXMLDOMNode; aDef: boolean): Boolean;

// Setzt den Wert in das entsprechende Element (kann auch ein Attribut sein)
function SetElementValue(aNode: IXMLDOMNode; aValue: Variant): Boolean;

// Gibt den Inhalt des Elements als Integer zur�ck.
function ElementToIntDef(aElement: IXMLDOMNode; aDef: Integer): Integer;

implementation

uses
  LoepfeGlobal, filectrl, XMLMappingDef, MMEventLog, mmcs, typinfo, XMLDef;

(*---------------------------------------------------------
  Generiert zwei Hashcodes �ber einen gegebenen Text
----------------------------------------------------------*)
procedure HashByMM(aText: string; out aHash1: Longint; out aHash2: Longint);
begin
  aHash1 := HashBySDBM(aText);
  aHash2 := HashByOCaml(aText);
end;// procedure HashByMM(aText: string; out aHash1: DWORD; out aHash2: DWORD);

(*---------------------------------------------------------
  Generiert zwei Hashcodes �ber einen gegebenen Text
----------------------------------------------------------*)
procedure HashByMM(aElement: IXMLDOMElement; out aHash1: Longint; out aHash2: Longint);
begin
  aHash1 := 0;
  aHash2 := 0;
  if assigned(aElement) then
    HashByMM(aElement.xml, aHash1, aHash2);
end;// procedure HashByMM(aElement: IXMLDOMElement; out aHash1: DWORD; out aHash2: DWORD);

(*

function HashByOCaml_D(aText: string): Longint;
var
  xByte: PByte;
begin
  Result := 0;
  xByte := @(aText[1]);
  while xByte^ <> 0 do begin
    Result := xByte^ + (Result * 19);
    inc(xByte);
  end;
end;// HashByOCaml
*)
function HashByOCaml(aText: string): Longint;
var
  xByte: PByte;
begin
  Result := 0;
  xByte := @(aText[1]);
  while xByte^ <> 0 do begin
    (* Der folgende Code ist gleichbedeutend mit:

            Result := xByte^ + (Result * 19);

       Die Codierung in Assembler ist notwendig, da sonst ein �berlauf produziert
       wird (Overflow kann nur f�r die ganze Unit ausgeschaltet werden)
      *)
    asm
      // Pointer auf das Byte in EAX
      mov eax, [xByte]
      // Byte ins eax laden (Typecast, da nur ein Byte geladen werden soll)
      movzx eax, BYTE PTR[eax]
      // result mit 19 Multiplizieren und in edx ablegen
      imul edx, DWORD PTR [result], $13
      // Zwischenresultat addieren
      add eax,edx
      // Ergebnis ins Result
      mov result, eax
    end;
    inc(xByte);
  end;
end;// HashByOCaml

(*---------------------------------------------------------
  Generiert einen Hashcode �ber einen gegebenen Text nach dem
  Algorithmus von SDBM (Sleepycats Berkley DataBase) (zB: www.fantsy-coders.de/projects/gh/html/x-435.html)
----------------------------------------------------------*)
function HashBySDBM(aText: string): Longint;
var
  xByte: PByte;
begin
  Result := 0;
  xByte := @(aText[1]);
  while xByte^ <> 0 do begin
    (* Der folgende Code ist gleichbedeutend mit:

            Result := xByte^ + (Result shl 6) + (Result shl 16) - Result;

       Die Codierung in Assembler ist notwendig, da sonst ein �berlauf produziert
       wird (Overflow kann nur f�r die ganze Unit ausgeschaltet werden)
      *)
    asm
      // Pointer auf das Byte in EAX
      mov eax, [xByte]
      // Byte ins eax laden (Typecast, da nur ein Byte geladen werden soll)
      movzx eax, BYTE PTR[eax]
      // alten Hash ins edx Register
      mov edx,result
      // ... und 6 Bit nach links
      shl edx,$06
      // Zwischenresultat addieren
      add eax,edx
      // alten Hash ins edx Register
      mov edx,result
      // ... und 16 Bit nach links schieben
      shl edx,$10
      // Zweischenresultat addieren
      add eax,edx
      // alten Hasch subtrahieren
      sub eax,result
      // Ergebnis ins Result
      mov result, eax
    end;
    inc(xByte);
  end;
end;// HashBySDBM
(*
function HashBySDBM_D(aText: string): Longint;
var
  xByte: PByte;
begin
  Result := 0;
  xByte := @(aText[1]);
  try
    while xByte^ <> 0 do begin
      Result := xByte^ + (Result shl 6) + (Result shl 16) - Result;
      inc(xByte);
    end;
  except
    on e:Exception do
      codeSite.SendError('HasBySDBM error: ' + e.message);
  end;
end;// HashBySDBM
*)


(*-----------------------------------------------------------
  Pr�ft ob ein Element ein bestimmtes Attribut besitzt
-------------------------------------------------------------*)
function AttributExists(aAttrName: string; aAttrMap: IXMLDOMNamedNodeMap): boolean;
begin
  result := false;
  if assigned(aAttrMap) then
    result := (aAttrMap.getNamedItem(aAttrname) <> nil);
end;// function AttributExists(): boolean;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als String zur�ck.
  Der R�ckgabewert sagt ob ein Attribut vorhanden ist oder nicht
-------------------------------------------------------------*)
function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;
begin
  aValue := '';

  // Zuerst abfragen ob das Attribut existiert
  result := AttributExists(aAttrName, aElement.attributes);
  if result then
    aValue := aElement.attributes.getNamedItem(aAttrName).nodeValue;
end;// function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als Integer zur�ck.

  Ist das Attribut nicht vorhanden oder kein Integer, dann wird der
  Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function AttributeToIntDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: integer): integer;
begin
  result := aDefault;

  if assigned(aElement) then
    // Zuerst abfragen ob das Attribut existiert
    if AttributExists(aAttrName, aElement.attributes) then
      result := StrToIntDef(aElement.attributes.getNamedItem(aAttrName).nodeValue, result);
end;// function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als Boolean zur�ck.

  Ist das Attribut nicht vorhanden oder kein Integer, dann wird der
  Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function AttributeToBoolDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: boolean): boolean;
begin
  result := aDefault;

  if assigned(aElement) then
    // Zuerst abfragen ob das Attribut existiert
    if AttributExists(aAttrName, aElement.attributes) then
      result := aElement.attributes.getNamedItem(aAttrName).nodeValue;
end;// function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als string zur�ck.

  Ist das Attribut nicht vorhanden oder kein Float, dann wird der
  Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function AttributeToFloatDef(aAttrName: string; aElement: IXMLDOMNode;
    aDefault: extended): extended;
var
  xStrValue: string;
begin
  xStrValue := AttributeToStringDef(aAttrName, aElement, MMFloatToStr(aDefault));
  try
    result := MMStrToFloat(xStrValue);
  except
    on e: EConvertError do 
      result := aDefault;
  end;
end;// function AttributeToFloatDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: extended): string;

(*-----------------------------------------------------------
  Gibt den Value (erster childNode) eines Elements zur�ck

  Sind meherere childNodes vorhanden, wird der Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function GetElementValueDef(aNode: IXMLDOMNode; aDefault:Variant):Variant;
begin
  result := aDefault;
  //Folgende Abtrage muss gemacht werden, weil es f�r ein Element keinen nodeValue gibt. Der Wert in unserem MMXML
  //  entspricht einem Textnode. Falls unser Element leer w�re, oder ein verschachteltes Element enthielte, g�be die Abfrage NULL zur�ck.

  if assigned(aNode) then begin
    if (aNode.childNodes.Length=1) then  //Es darf nur einen Wert pro Element im MMXML haben
      if (aNode.childNodes.item[0].nodeValue <> NULL) then
        result := aNode.childNodes.item[0].nodeValue;
  end;// if assigned(aNode) then begin
end; //function GetElementValueDef(aNode: IXMLDOMNode; aDefault:Variant):Variant;

(*---------------------------------------------------------
  Formatiert ein XML String zur Ausgabe.
  mit den Parameter aIndentString kann bestimmt werden welcher String
  f�r die Einr�ckung verwendet wird.
----------------------------------------------------------*)
function FormatXML(aNodeList: IXMLDOMNodeList; aIndentString: string = '  '): string;
var
  i: integer;
begin
  result := '';
  
  if assigned(aNodeList) then begin
    for i := 0 to aNodeList.Length - 1 do begin
      if IsInterface(aNodeList.Item[i], IXMLDOMElement) then
        result := result + #13#10 + FormatXML(aNodeList.item[i] as IXMLDOMElement, aIndentString);
    end;// for i := 0 to aNodeList.Length - 1 do begin
  end;// if assigned(aNodeList) then begin

  // erstes crlf l�schen
  if Length(result) > 2 then
    system.delete(result, 1, 2);
end;// function FormatXML(aXMLStream: string; aIndentString: string = '') string;

(*---------------------------------------------------------
  Formatiert ein XML Document zur Ausgabe.
  mit den Parameter aIndentString kann bestimmt werden welcher String
  f�r die Einr�ckung verwendet wird.
----------------------------------------------------------*)
function FormatXML(aDoc: DOMDocumentMM; aIndentString: string = '  '): string;
var
  i: integer;
  xPointerChild: pointer;
  xPointerDocumentElement: pointer;
begin
  result := '';
  if assigned(aDoc) then begin
    xPointerDocumentElement := pointer(aDoc.DocumentElement as IUnknown);
    for i := 0 to aDoc.childNodes.length - 1 do begin
      xPointerChild := pointer(aDoc.childNodes.Item[i] as IUnknown);

      if xPointerChild = xPointerDocumentElement then
        result := result + FormatXML((aDoc.childNodes.Item[i] as IXMLDOMElement), aIndentString)
      else
        result := result + aDoc.childNodes.Item[i].xml + #13#10;
    end;
  end;// if assigned(aDoc) then begin
end;

(*---------------------------------------------------------
  Formatiert ein XML Element zur Ausgabe.
  mit den Parameter aIndentString kann bestimmt werden welcher String
  f�r die Einr�ckung verwendet wird.

  Aufruf:
    procedure TfrmMainWindow.mmBitBtn3Click(Sender: TObject);
    var
      xDOM: DOMDocumentMM;
    const
      cTestXML = '<Body><Level2 attr1="1">Text<Level3/></Level2><Level2_1/></Body>';
    begin
      xDOM := CoDOMDocumentMM.Create;
      xDOM.loadXML(cTestXML);
      memoDOM.Text := FormatXML(xDOM.DocumentElement);
oder
      memoDOM.Text := FormatXML(xDOM.DocumentElement, #9);
    end;
----------------------------------------------------------*)
function FormatXML(aNode: IXMLDOMElement; aIndentString: string = '  '): string;
  function ProcessElement(aElement: IXMLDOMNode; aIndent: integer):string;
  var
    i: integer;

    // Einr�cken
    function SetIndent(aIndentCount: integer): string;
    var
      i: integer;
    begin
      result := '';
      for i := 1 to aIndentCount do
        result := result + aIndentString;
    end;// function SetIndent(aSpaceCount: integer): string;
  begin
    result := '';
    if assigned(aElement) then begin
      // Element- oder Attributnamen
      case aElement.NodeType of
        NODE_TEXT: begin
          if not(VarIsNull(aElement.nodeValue)) then
            result := aElement.nodeValue;
        end;// NODE_TEXT: begin
        NODE_COMMENT: begin
          if not(VarIsNull(aElement.Text)) then
            result := cCrlf + SetIndent(aIndent) + '<!--' + aElement.Text + '-->';
        end;// NODE_COMMENT: begin
      else
        result := cCrlf + SetIndent(aIndent) + '<' + aElement.nodename;
        // Attribute eintragen
        if assigned(aElement.attributes) then begin
          for i := 0 to aElement.attributes.length - 1 do begin
            with aElement.attributes do
              result := Format('%s %s="%s"', [result, item[i].nodeName, item[i].Text]);
          end;// for i := 0 to aElement.attributes.length - 1 do begin
        end;// f not(VarIsNull(aElement.attributes)) then begin

        // Rekursive Verarbeitung der Elemente
        if aElement.hasChildNodes then begin
          // Element abschliessen
          result := result + '>';
          // Alle Unterelemente rekursiv verarbeiten
          for i := 0 to aElement.childNodes.length - 1 do
            result := result + ProcessElement(aElement.childNodes.item[i], aIndent + 1);
            // Element abschliessen
          if result > '' then begin
            if (copy(result, Length(result), 1) = '>') then
              result := result + cCrlf + SetIndent(aIndent) + '</' + aElement.NodeName + '>'
            else
              result := result + '</' + aElement.NodeName + '>';
          end;// if result > '' then begin
        end else begin
          // Wenn kein Textknoten, dann Element abschliessen
          result := result + '/>';
        end;// if aElement.hasChildNodes then begin
      end;//
    end;// if assigned(aElement) then begin
  end;// function ProcessElement(aElement: IXMLDOMElement):string
begin
  // Rekursiv verarbeiten
  result := ProcessElement(aNode, 0);

  // f�hrendes crlf entfernen
  if Length(result) > 2 then begin
    if copy(result, 1, 2) = cCrlf then
      result := copy(result, 3, MAXINT);
  end;// if Length(result) > 2 then begin
end;// function FormatXML(aNode: IXMLDOMElement; aIndentString: string = '  '): string;

(*---------------------------------------------------------
  L�dt das angegebene File in einen String
----------------------------------------------------------*)
function LoadXMLFile(aFileName: string): String;
begin
  result := '';
  if FileExists(aFileName) then begin
    with TStringList.Create do try
      LoadFromFile(aFileName);
      result := Text;
    finally
      free;
    end;
  end;// if FileExists(aFileName) then begin
end;// TForm1.LoadXMLFile cat:No category

(*---------------------------------------------------------
 Erstellt aus einem XMLStream einen DOM-Baum und gibt diesen als Returnparameter zur�ck.
 Falls etwas schief geht, wird eine Exception ausgel�st.
----------------------------------------------------------*)
function XMLStreamToDOM(aXMLStream: string): DOMDocumentMM;
begin
  // XML Dokument (DOM Baum) f�r die Beschreibung der Datenstruktur erzeugen
  Result := DOMDocumentMMCreate;
  if assigned(Result) then begin
    Result.setProperty('NewParser', true);
    // Stream in das Dokument laden und parsen
    if not Result.loadXML(aXMLStream) then begin
      raise Exception.Create('XML DOM tree could not been parsed correctly:' + cCrlf+ cCrlf
                            + FormatError(result.ParseError));
    end; //if not Result.loadXML(aXMLStream)
  end
  else begin
    raise Exception.Create('XML DOM tree could not be generated! ');
  end; //if assigned(Result)
end; //function XMLStreamToDOM

(*---------------------------------------------------------
  Konvertiert einen Parse Error zu einem String
----------------------------------------------------------*)
function FormatError(aParseError: IXMLDOMParseError): string;
begin
  if assigned(aParseError) then begin
    with aParseError do begin
      result := reason + cCrlf +
                'Line: ' + IntToStr(line) + cCrlf +
                'Col: ' + intToStr(linePos) + cCrlf +
                'Source: ' +  cCrlf + srcText;
    end;// with aParseError do begin
  end;// if assigned(aParseError) then begin
end;// function FormatError(aParseError: IXMLDOMParseError): string;

(*---------------------------------------------------------
  Erzeugt die Processing Informationen und das DocumentElement
----------------------------------------------------------*)
procedure InitXMLDocument(var aDoc: DOMDocumentMM; aDocumentElement: string);
begin
  if not(assigned(aDoc)) then
    aDOC := DOMDocumentMMCreate;

  // Der neue Parser in MSXML 4.0 soll verwendet werden
  aDoc.setProperty('NewParser', true);
  aDoc.AppendChild(aDoc.CreateProcessingInstruction(cXMLLanguage, cLoepfeProlog));
  // Document Element erzeugen
  aDoc.AppendChild(aDoc.CreateElement(aDocumentElement));
end;// procedure InitXMLDocument(aDoc: DOMDocumentMM; aDocumentElement: string);

(*---------------------------------------------------------
  Formatiert ein XML String zur Ausgabe.
  mit den Parameter aIndentString kann bestimmt werden welcher String
  f�r die Einr�ckung verwendet wird.
----------------------------------------------------------*)
function FormatXML(aXMLStream: string; aIndentString: string = '  '): string;
var
  xDoc: DOMDocumentMM;
begin
  result := '';
  xDoc := DOMDocumentMMCreate;
  xDoc.preserveWhiteSpace := false;
  xDoc.loadXML(aXMLStream);
  if not(xDoc.parseError.errorCode <> 0) then
    result := FormatXML(xDoc, aIndentString)
  else
    result := xDoc.ParseError.reason + cCrlf + cCrlf + xDoc.ParseError.srcText;
  xDoc := nil;
end;// function FormatXML(aXMLStream: string; aIndentString: string = '') string;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als Variant zur�ck.

  Ist das Attribut nicht vorhanden oder kein String, dann wird der
  Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function AttributeToVariantDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: Variant): Variant;
begin
  result := aDefault;

  if assigned(aElement) then
    // Zuerst abfragen ob das Attribut existiert
    if AttributExists(aAttrName, aElement.attributes) then
      result := aElement.attributes.getNamedItem(aAttrName).nodeValue;
end;// function AttributeToVariantDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: Variant): string;

(*---------------------------------------------------------
  Erzeugt alle Elemente entlang eines Selektionspfades (sofern nicht bereits vorhanden)
  Der Selektionspfad kann mit einem '/' beginnen muss aber nicht. Im Pfad d�rfen keine Bedingungen
  angegeben sein (Beispiel f�r einen ung�ltigen Pfad: /LoepfeBody/Test[@atr = 'hallo'])

  Beispiel:
  Der Selektionspfad '/LoepfeBody/Test' erzeugt das Element 'LoepfeBody' wenn es nicht vorhanden ist,
  danach wird das Element 'Test' erzeugt, wenn es nciht vorhanden ist.
  Der R�ckgabewert ist dann das Element 'Test'.
----------------------------------------------------------*)
function GetCreateElement(aElementPath: string; aDoc: DOMDocumentMM; aRootElement: IXMLDOMNode = nil): IXMLDOMElement;
var
  xNode: IXMLDOMNode;
  xTempNode: IXMLDOMNode;
  i: integer;
begin
  result := nil;

  // evt. vorhandnen f�hrenden Slash entfernen
  if Trim(aElementPath) > '' then
    if Trim(aElementPath)[1] = '/' then
      system.Delete(aElementPath, 1, 1);

  if assigned(aDoc) then begin
    if IsInterface(aRootElement, IXMLDOMElement) then
      xNode := aRootElement.SelectSingleNode(aElementPath)
    else
      // Zuerst versuchen das Element zu selektieren
      xNode := aDoc.SelectSingleNode(aElementPath);

    if not(assigned(xNode)) then begin
      (* Ist die Selektion fehlgeschlagen, dann die Einzelteile des Pfades in eine Stringlist aufnehmen.
         Ein neues Element ist durch '/' gekennzeichnet. Der Pfad darf keine Bedingungen beinhalten. Jeder '/' wird
         durch ein Komma ersetzt, damit der gesamte Pfad an Commtext() der StringList �bergeben werden kann. *)
      with TStringList.Create do try
        commaText := StringReplace(aElementPath, '/', ',', [rfReplaceAll]);
        if IsInterface(aRootElement, IXMLDOMElement) then
          xNode := aRootElement
        else
          xNode := aDoc;
        // Jedes Element einzeln behandeln
        for i := 0 to Count - 1 do begin
          xTempNode := xNode;
          // versucht das n�chste Element zu selektieren
          xNode := xNode.SelectSingleNode(Strings[i]);
          if not(assigned(xNode)) then begin
            // Wenn das Element nicht vorhanden ist, dann wird es erzeugt
            xNode := aDoc.createElement(Strings[i]);
            // ... und am vorhergeneden Element angeh�ngt
            xTempNode.appendChild(xNode);
          end;// if not(assigned(xNode)) then begin
        end;// for i := 0 to Count - 1 do begin
      finally
        free;
      end;// with TStringList.Create do try
    end;// if not(assigned(xNode)) then begin

    // R�ckgabewert abf�llen und als IXMLDOMElement zur�ckgeben
    if IsInterface(xNode, IXMLDOMElement) then
      result := (xNode as IXMLDOMElement);
  end;// if assigned(aDoc) then begin
end;// function GetCreateElement(aElementPath: string; aDoc: DOMDocumentMM): IXMLDOMElement;

(*---------------------------------------------------------
  Verschiebt alle Kind Elemente von Source Element zum Target Element
----------------------------------------------------------*)
procedure MoveChildsToNewElement(aSource: IXMLDOMNode; aTarget: IXMLDOMNode);
begin
  // Sicherstellen, dass Elemente �bergeben wurden (Keine Auswirkung in Build mit Buildtool)
  Assert(IsInterface(aSource, IXMLDOMElement), 'Source is not Element');
  Assert(IsInterface(aTarget, IXMLDOMElement), 'Source is not Element');

  if assigned(aSource) and assigned(aTarget) then begin
    // von Source nach Target kopieren
    while aSource.childNodes.length > 0 do
      aTarget.appendChild(aSource.childNodes.item[0]);
  end;// if assigned(aSource) and assigned(aTarget) then begin
end;// procedure MoveChildsToNewElement(aSource: IXMLDOMNode; aTarget: IXMLDOMNode);

(*---------------------------------------------------------
  Verschiebt alle Kind Elemente von der Source List zum Target Element
----------------------------------------------------------*)
procedure MoveChildsToNewElement(aSource: IXMLDOMNodeList; aTarget: IXMLDOMNode);
var
  i: Integer;
begin
  // Sicherstellen, dass Elemente �bergeben wurden (Keine Auswirkung in Build mit Buildtool)
  Assert(IsInterface(aSource, IXMLDomNodeList), 'Source is not NodeList');
  Assert(IsInterface(aTarget, IXMLDOMElement), 'Source is not Element');

  if assigned(aSource) and assigned(aTarget) then begin
    // von Source nach Target kopieren
    for i := 0 to aSource.length - 1 do
      aTarget.appendChild(aSource.item[i]);
  end;// if assigned(aSource) and assigned(aTarget) then begin
end;// procedure MoveChildsToNewElement(aSource: IXMLDOMNode; aTarget: IXMLDOMNode);

(*---------------------------------------------------------
  L�scht alle Knoten der Liste aus Ihrem Element
----------------------------------------------------------*)
procedure removeNodes(aNodeList: IXMLDOMNodeList);
var
  i: Integer;
begin
  Assert(IsInterface(aNodeList, IXMLDOMNodeList), 'Source is not NodeList');

  if assigned(aNodeList) then begin
    for i := 0 to aNodeList.Length - 1 do begin
      if assigned(aNodeList.Item[i].parentNode) then
        aNodeList.Item[i].parentNode.removeChild(aNodeList.Item[i]);
    end;// for i := 0 to xNodeList.Length - 1 do begin
  end;// if assigned(xNodeList) then begin
end;// procedure removeNodes(aNodeList: IXMLNodeList);

(*-----------------------------------------------------------
  Gibt den XPath f�r aIndex Elemente.
  Bsp: '/LoepfeBody/YMSetting/Basic/Channel/Neps'
      Der Aufruf XPFromLeft(2) liefert den String '/LoepfeBody/YMSetting'
-------------------------------------------------------------*)
function XPFromLeft(aName: string; aCount: integer): string;
var
  xDelimiterIndex: integer;
  xTempStr: string;
  i: integer;
begin
  result := '';
  i := 0;
  xTempStr := aName;
  repeat
    xDelimiterIndex := AnsiPos('/', xTempStr);

    if xDelimiterIndex > 0 then
      result := copyLeft(xTempStr, '/', true);

    inc(i);
  until (i >= aCount);
end;// function XPFromLeft(aIndex: integer): string;

(*---------------------------------------------------------
  Erzeugt eine Instanz eines XML Dokuments
----------------------------------------------------------*)
function DOMDocumentMMCreate: DOMDocumentMM;
begin
  Result := CoDOMDocument40.Create;
end;// function DOMDocumentMMCreate: DOMDocumentMM;

(*---------------------------------------------------------
  Vergleicht die untergeordneten Knoten auf Gleichheit im Namen und im Wert.
  Attribute werden nicht verglichen.
  Die Funktion wird rekursiv f�r alle Childs aufgerufen
----------------------------------------------------------*)
function SameNodeText(aNode1, aNode2: IXMLDOMNode): Boolean;
var
  xTempResult: Boolean;
  i: Integer;
begin
  result := false;

  if assigned(aNode1) and assigned(aNode2) then begin
    if AnsiSameText(aNode1.nodeName, aNode2.NodeName) then begin
      if aNode1.childnodes.length = aNode2.childnodes.length then begin
        // Hier sind die Knoten potenziell gleich
        xTempResult := (aNode1.Text = aNode2.Text);
        i := 0;
        // Durch alle Knoten iterieren und aussteigen sobald einer nicht gleich ist
        while xTempResult and (i < aNode1.childnodes.length) do begin
          xTempResult := xTempResult and SameNodeText(aNode1.childnodes.item[i], aNode2.childnodes.item[i]);
          inc(i);
        end;// while xTempResult and (i < aNode1.childnodes.length) do begin
        // Resultat des Vergleichs �bernehmen
        result := xTempResult;
      end;// if aNode1.childnodes.length = aNode2.childnodes.length then begin
    end;// if AnsiSameText(aNode1.nodeName, aNode2.NodeName) then begin
  end;// if assigned(aNode1) and assigned(aNode2) then begin
end;// procedure SameChildNodes(aNode1, aNode2: IXMLDOMNode);

(*-----------------------------------------------------------
  Gibt den XPath f�r aIndex Elemente von rechts.
  Bsp: '/LoepfeBody/YMSetting/Basic/Channel/Neps'
      Der Aufruf XPFromRight(2) liefert den String 'Basic/Channel/Neps'
-------------------------------------------------------------*)
function XPFromRight(aName: string; aCount: integer): string;
var
  xDelimiterIndex: integer;
  xTempStr: string;
  i: integer;
begin
  result := '';
  i := 0;
  xTempStr := aName;
  repeat
    xDelimiterIndex := LastDelimiter('/', xTempStr);

    if xDelimiterIndex > 0 then begin
      result := copy(xTempStr, xDelimiterIndex, MAXINT);
      xTempStr := copy(xTempStr, 1, xDelimiterIndex - 1);
    end;// if xDelimiterIndex > 0 then begin

    inc(i);
  until (i >= aCount);
end;// function XPFromRight(aIndex: integer): string;

(*---------------------------------------------------------
  Z�hlt die Level eines Selektionspfades. DAbei werden lediglich die
  Slashes ('/') gez�hlt.
  Bsp: '/LoepfeBody/YMSetting/Basic/Channel/Neps' leifert 5
----------------------------------------------------------*)
function XPLevelCount(aSelPath: string): integer;
var
  i: Integer;
const
  cXPDelimiter = '/';
begin
  result := 0;
  if Length(aSelPath) > 0 then begin
    for i := 1 to Length(aSelPath) do begin
      if aSelPath[i] = cXPDelimiter then
        inc(result);
    end;// for i := 1 to Length(aSelPath) do begin
    // Wenn das erste Zeichen kein Slash ist, dann das Level noch incrementieren
    if aSelPath[1] <> cXPDelimiter then
      inc(result);
  end;// if Length(aSelPath) > 0 then begin
end;// function XPLevelCount(aSelPath: string): integer;

(*---------------------------------------------------------
  Entfernt einen Teil von einem globalen XPath.

  Beispiel:
    Eine Klasse ben�tigt ein Element aus dem XML. In der Unit XMLDef.pas
    sind die Selektionspfade definiert. Um aber relativ zu einem Knoten auf ein Element
    zugreifen zu k�nnen muss ein Teil des Selektionspfades abgeschnitten werden.

    cXPFPatternNode         = '/LoepfeBody/Config/Group/Info/FPattern';
    cXPSpeedSimulationItem  = '/LoepfeBody/Config/Group/Info/FPattern/SpeedSimulation';

    RemoveXPRoot(cXPFPatternNode, cXPSpeedSimulationItem) ==> 'SpeedSimulation'
----------------------------------------------------------*)
function RemoveXPRoot(aXPToRemove: string; aSource: string): String;
  function GetPath(aCopyIndex: integer): string;
  begin
    result := '';
    if Length(aSource) > aCopyIndex then begin
      result := copy(aSource, aCopyIndex, MAXINT);

      // evt. f�hrenden Slash entfernen (ergiebt einen relativen XPath)
      if (result > '') and (result[1] = '/') then
        system.delete(result, 1, 1);
    end;// if Length(aSource) > aCopyIndex then begin
  end;// function GetPath(aCopyIndex: integer): string;
begin
  Result := aSource;
  // Wenn die Source mit dem zu entfernenen String begint
  if AnsiPos(aXPToRemove, aSource) = 1 then
  // Diesen ersten String entfernen
  result := GetPath(Length(aXPToRemove) + 1);
end;// TXMLMaConfigHelper.RemoveFPRoot cat:No category

(*---------------------------------------------------------
  Ermittelt von einem Element den absoluten Selektionspfad.
  Dieser Pfad selektiert, auf das DOM angewendet, das Element 'aElement'
----------------------------------------------------------*)
function GetSelectionPath(aElement: IXMLDOMNode): string;
var
  xElement: IXMLDOMElement;
  xNode: IXMLDOMNode;
begin
  Result := '';
  xNode := aElement;
  while assigned(xNode) do begin
    if Supports(xNode, IXMLDOMElement, xElement) then
      result := '/' + xElement.NodeName + result;
    xNode := xNode.parentNode;
  end;// while assigned(xNode) do begin
end;// function GetSelectionPath(aElement: IXMLDOMNode): string;

(*---------------------------------------------------------
  F�gt einem bestehenden Element ein neues hinzu und gibt eine Referenz
  auf das neue Element zur�ck.
----------------------------------------------------------*)
function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string; aElementValue: Variant): IXMLDOMElement;
begin
  result := AppendNewXMLElement(aParentNode, aElementName);
  if assigned(result) then
    result.text := aElementValue;
end;// function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string; aElementValue: Variant): IXMLDOMElement

(*---------------------------------------------------------
  F�gt einem bestehenden Element ein neues hinzu und gibt eine Referenz
  auf das neue Element zur�ck.
----------------------------------------------------------*)
function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string): IXMLDOMElement;
var
  xDom: DOMDocumentMM;
begin
  result := nil;

  if assigned(aParentNode) then begin
    // Verwendet das DOM des Parent oder erzeugt ein neues
    if IsInterface(aParentNode.OwnerDocument, DOMDocumentMM) then
      xDom := aParentNode.OwnerDocument as DOMDocumentMM
    else
      xDOM := DOMDocumentMMCreate;

    // neues Element erzeugen
    result := xDOM.createElement(aElementName);
    // ... und an das Parent Element anf�gen
    aParentNode.appendChild(result);
  end;// if assigned(aParentNode) then begin
end;// function AppendNewXMLElement(aParentNode: IXMLDOMElement; aElementName: string): IXMLDOMElement;

(*-----------------------------------------------------------
  Gibt das benannte Attribut als string zur�ck.

  Ist das Attribut nicht vorhanden oder kein String, dann wird der
  Defaultwert zur�ckgegeben.
-------------------------------------------------------------*)
function AttributeToStringDef(aAttrName: string; aElement: IXMLDOMNode; aDefault: string): string;
begin
  result := aDefault;

  if assigned(aElement) then
    // Zuerst abfragen ob das Attribut existiert
    if AttributExists(aAttrName, aElement.attributes) then
      result := aElement.attributes.getNamedItem(aAttrName).nodeValue;
end;// function GetAttributeAsString(aAttrName: string; aElement: IXMLDOMNode; out aValue: string): boolean;

(*---------------------------------------------------------
  Gibt den Inhalt des Elements als Integer zur�ck.

  Ist das Element nicht vorhanden (nil)  oder der Inhalt kein Integer, dann
  wird der Defaultwert zur�ckgegeben.
----------------------------------------------------------*)
function ElementToIntDef(aElement: IXMLDOMNode; aDef: Integer): Integer;
begin
  if Assigned(aElement) then begin
    try
      result := GetElementValueDef(aElement, aDef);
    except
      on e: EVariantError do
        Result := aDef;
    end;// try finally
  end else begin
    Result := aDef;
  end;// if Assigned(aElement) then begin
end;// function ElementToIntDef(aElement: IXMLDOMNode; aDef: Integer): Integer;

(*---------------------------------------------------------
  Setzt den Wert in das entsprechende Element (kann auch ein Attribut sein)
----------------------------------------------------------*)
function SetElementValue(aNode: IXMLDOMNode; aValue: Variant): Boolean;
begin
  Result := Assigned(aNode);
  if result then
    aNode.text := aValue;
end;// function SetElementValue(): Boolean;

(*---------------------------------------------------------
  Gibt den Inhalt des Elements als Boolean zur�ck.

  Ist das Element nicht vorhanden (nil)  oder der Inhalt kein Integer, dann
  wird der Defaultwert zur�ckgegeben.
----------------------------------------------------------*)
function ElementToBoolDef(aElement: IXMLDOMNode; aDef: boolean): Boolean;
begin
  if Assigned(aElement) then begin
    try
      result := GetElementValueDef(aElement, aDef);
    except
      on e: EVariantError do
        Result := aDef;
    end;// try finally
  end else begin
    result := aDef;
  end;// if Assigned(aElement) then begin
end;// function ElementToIntDef(aElement: IXMLDOMNode; aDef: Integer): Integer;

(*---------------------------------------------------------
  TMMStringBuilder Konstruktor

  TMMStringBuilder erm�glicht es mehrere Strings in einem einzelen
  String unterzubringen. Als Trennzeichen wird ein Steuerzeichen verwendet das in einem
  XML Stream nicht vorkommen darf. Die einzelnen Strings werden mit 'Add()' hinzugef�gt und
  mit der Eigenschaft 'Strings[]' wieder gelesen.
  Mt der Eigenschaft 'Text' kann eine komplete Stringsammlung gelesen oder zugewiesen werden.
----------------------------------------------------------*)
constructor TMMStringBuilder.Create;
begin
  inherited;
  FCount := 0;
  SetLength(FStrings, 0);
end;// constructor TMMStringBuilder.Create;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TMMStringBuilder.Destroy;
begin
  SetLength(FStrings, 0);
  inherited;
end;// destructor TMMStringBuilder.Destroy;

(*---------------------------------------------------------
  F�gt einen String als Teilstring an das Ende an
----------------------------------------------------------*)
function TMMStringBuilder.Add(aText: string): Integer;
begin
  inc(FCount);
  SetLength(FStrings, FCount);
  FStrings[FCount - 1] := aText;
  result := FCount - 1;
end;// function TMMStringBuilder.Add(aText: string): Integer;

(*---------------------------------------------------------
  Liefert einen einzelnen String aus dem Array zur�ck
----------------------------------------------------------*)
function TMMStringBuilder.GetStrings(aIndex: Integer): string;
begin
  // L�st bei einem Zugriff auf einen nicht vorhandenen Index eine Exception aus
  if aIndex < FCount then
    Result := FStrings[aIndex]
  else
    // SListIndexError in Unit Consts
    raise EStringListError.CreateFmt(LoadResString(@SListIndexError), [aIndex]);
end;// function TMMStringBuilder.GetStrings(aIndex: Integer): string;

(*---------------------------------------------------------
  Liefert den Text aller Strings mit einem Trennzeichen getrennt
----------------------------------------------------------*)
function TMMStringBuilder.GetText: string;
var
  i: integer;
begin
  result := '';
  for i := 0 to Count - 1 do
    result := result + cMMStringBuilderDelimiter + Strings[i];
  // �berz�hliges Trennzeichen am Beginn des Strings l�schen
  if result > '' then
    system.Delete(result, 1, 1);
end;// function TMMStringBuilder.GetText: string;

(*---------------------------------------------------------
  L�st die Teilstrings die mit dem Trennzeichen getrennt sind auf und
  legt sie im Array ab
----------------------------------------------------------*)
procedure TMMStringBuilder.SetText(const Value: string);
var
  xText: string;
begin
  xText := Value;
  while xText > '' do
    Add(copyLeft(xText, cMMStringBuilderDelimiter, true));
end;// procedure TMMStringBuilder.SetText(const Value: string);



end.
