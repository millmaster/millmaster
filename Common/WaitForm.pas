(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AssignComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 09.10.2000  1.00  Mg  | Datei erstellt
| 13.03.2002  1.00  Wss | mmPanel1 entfernt
--------------------------------------------------------------------------------
*)
unit WaitForm;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOG, ComCtrls, mmProgressBar, ExtCtrls, mmTimer, mmPanel;

type
  TWait = class(TmmDialog)
    mmProgressBar1: TmmProgressBar;
    mmTimer1: TmmTimer;
    procedure mmTimer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Wait: TWait;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}
//------------------------------------------------------------------------------
  procedure TWait.mmTimer1Timer(Sender: TObject);
  begin
    inherited;
    mmProgressBar1.StepIt;
  end;
//------------------------------------------------------------------------------
  procedure TWait.FormShow(Sender: TObject);
  begin
    inherited;
    mmProgressBar1.Position := 0;
  end;
//------------------------------------------------------------------------------
end.
