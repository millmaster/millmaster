{===========================================================================================
| Copyright:        Innolab GmbH
|-------------------------------------------------------------------------------------------
| Dateiname:        lkExpandingPanel
| Projekt:          LK Komponenten
| Beschreibung:     Panel das ein- und ausgeklappt werden kann. Kann als Container verwendet werden
| Info:             -
| Org. Entw.System: Windows XP SP2
| Compiler/Tools:   Delphi
|-------------------------------------------------------------------------------------------
| History:
| Datum       Vers. Vis.| Grund
|-------------------------------------------------------------------------------------------
| 06.03.2005  1.00  Lok | Datei erzeugt
|==========================================================================================}
unit lkExpandingPanel;

interface

uses
  SysUtils, Classes, Controls, ExtCtrls, StdCtrls, Graphics, mmPanel;

type
  TlkExpandingPanel = class;

  TExpandCollapseMethod = procedure (Sender: TlkExpandingPanel) of object;
  TExpandingCollapsingMethod = procedure (Sender: TlkExpandingPanel; var Allowed: boolean) of object;

  (* -----------------------------------------------------------
    Panel das den Titel des Containers anzeigt.
    Ist das Alignemnt alLeft oder alRight, wid der Titel um 90� gedreht
    angezeigt.
  -------------------------------------------------------------- *)
  TTitlePanel = class(TmmPanel)
  private
    FTitle: string;
    procedure SetTitle(const Value: string);
  public
    procedure Paint; override;
    property Title: string read FTitle write SetTitle;
  end;// TTitlePanel = class(TPanel)
  
  TlkExpandingPanel = class(TCustomPanel)
  private
    mTitleNamePanel: TTitlePanel;
    bExpand: TImage;
    FExpanded: Boolean;
    FOnCollapsed: TExpandCollapseMethod;
    FOnCollapsing: TExpandingCollapsingMethod;
    FOnExpanded: TExpandCollapseMethod;
    FOnExpanding: TexpandingCollapsingMethod;
    FExpandedHeight: Integer;
    FExpandedWidth: Integer;
    function GetTitleFont: TFont;
    function GetPanelHeight: Integer;
    function GetPanelWidth: Integer;
    function GetTitle: string;
    procedure SetExpanded(const Value: Boolean);
    procedure SetTitleFont(const Value: TFont);
    procedure SetPanelHeight(const Value: Integer);
    procedure SetPanelWidth(const Value: Integer);
    procedure SetTitle(const Value: string);
  protected
    procedure DoCollapsed; virtual;
    function DoCollapsing: Boolean; virtual;
    function DoExpanding: Boolean; virtual;
    procedure DoExpanded; virtual;
    procedure SetName(const Value: TComponentName); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure bExpandClick(Sender: TObject);
    //1 Klappt den Frame zusammen
    procedure Collapse;
    //1 macht den Frame auf und zeigt die Kontrollelemente fr die Settings 
    procedure Expand;
    procedure Resize; override;
    procedure SyncTitlePanelAlignment;
    property PanelHeight: Integer read GetPanelHeight write SetPanelHeight;
    property PanelWidth: Integer read GetPanelWidth write SetPanelWidth;
  published
    property Expanded: Boolean read FExpanded write SetExpanded;
    property ExpandedHeight: Integer read FExpandedHeight write FExpandedHeight;
    property ExpandedWidth: Integer read FExpandedWidth write FExpandedWidth;
    property OnCollapsed: TExpandCollapseMethod read FOnCollapsed write
        FOnCollapsed;
    property OnCollapsing: TExpandingCollapsingMethod read FOnCollapsing write
        FOnCollapsing;
    property OnExpanded: TExpandCollapseMethod read FOnExpanded write FOnExpanded;
    property OnExpanding: TexpandingCollapsingMethod read FOnExpanding write
        FOnExpanding;
    property Title: string read GetTitle write SetTitle;
    property TitleFont: TFont read GetTitleFont write SetTitleFont;
    property Align;
    property BevelEdges;
    property BevelInner;
    property BevelKind;
    property BevelOuter;
    property BevelWidth;
    property Color;
    property Constraints;
    property Enabled;
    property PopupMenu;
    property ShowHint;
    property Visible;
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property OnStartDrag;
  end;

procedure Register;

implementation
uses
  Windows;
{$R lkExpandingPanel.res}

procedure Register;
begin
  RegisterComponents('LK', [TlkExpandingPanel]);
end;

constructor TlkExpandingPanel.Create(AOwner: TComponent);
begin
  inherited;
  Caption := '';
  mTitleNamePanel := TTitlePanel.Create(Self);
  bExpand         := TImage.Create(Self);
{  BevelOuter := bvNone;
  BevelInner := bvNone;}
  with mTitleNamePanel do begin
    Parent := self;
//    SetBounds(0, 0, 451, 18);
    Height := 18;
    Align := alTop;
    BevelInner := bvLowered;
    BevelOuter := bvNone;
    Color := clInfoBk;
  end;
  with bExpand do begin
    Parent := mTitleNamePanel;
    SetBounds(3, 3, 12, 12);
    Stretch := True;
    OnClick := bExpandClick;
  end;
  ExpandedHeight := Height;
  ExpandedWidth := Width;
  Align := alTop;
  Collapse;
end;

destructor TlkExpandingPanel.Destroy;
begin
  FreeAndNil(bExpand);
  FreeAndNil(mTitleNamePanel);
  inherited;
end;

procedure TlkExpandingPanel.bExpandClick(Sender: TObject);
begin
  if Expanded then
    Collapse
  else
    Expand;
end;

//:-------------------------------------------------------------------
(*: Member:           Collapse
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category
 *  Argumente:
 *
 *  Kurzbeschreibung: Klappt den Frame zusammen
 *  Beschreibung:
                      -
 --------------------------------------------------------------------*)
procedure TlkExpandingPanel.Collapse;
begin
  SyncTitlePanelAlignment;
  if DoCollapsing then begin
    bExpand.Picture.Bitmap.LoadFromResourceName(Hinstance, 'ID_lkPlus');
    FExpanded := false;
    case Align of
      alTop, alBottom: begin
        FExpandedHeight := Height;
        Height := mTitleNamePanel.Height;
      end;// alTop, alBottom: begin
      alLeft, alRight: begin
        FExpandedWidth := Width;
        Width := mTitleNamePanel.Width;
      end;// alLeft, alRight: begin
    end;// case Align of
    DoCollapsed;
  end;// if DoCollapsing then begin
end;// TfrmMethodBaseFrame.Collapse cat:No category

procedure TlkExpandingPanel.DoCollapsed;
begin
  if Assigned(FOnCollapsed) then 
    FOnCollapsed(Self);
end;

function TlkExpandingPanel.DoCollapsing: Boolean;
begin
  result := true;
  if Assigned(FOnCollapsing) then 
    FOnCollapsing(Self, result);
end;

function TlkExpandingPanel.DoExpanding: Boolean;
begin
  result := true;
  if Assigned(FOnExpanding) then
    FOnExpanding(Self, result);
end;

//:-------------------------------------------------------------------
(*: Member:           Expand
 *  Klasse:           TfrmMethodBaseFrame
 *  Kategorie:        No category
 *  Argumente:
 *
 *  Kurzbeschreibung: macht den Frame auf und zeigt die Kontrollelemente fr die Settings
 *  Beschreibung:
                      Jede Berechnungs Methode hat unterschiedliche Settings und daher auch
                      verschiedne GUI Elemente. Die Hhe des Frames wird im Design Mode bestimmt
                      und beim zusammenklappen gesichert.
 --------------------------------------------------------------------*)
procedure TlkExpandingPanel.Expand;
begin
  SyncTitlePanelAlignment;
  if DoExpanding then begin
    bExpand.Picture.Bitmap.LoadFromResourceName(Hinstance, 'ID_lkMinus');
    FExpanded := true;
    case Align of
      alTop, alBottom: Height := FExpandedHeight;
      alLeft, alRight: Width := FExpandedWidth;
    end;// case Align of
    // Benachrichtigung
    DoExpanded;
  end;
end;// TfrmMethodBaseFrame.Expand cat:No category

procedure TlkExpandingPanel.DoExpanded;
begin
  if Assigned(FOnExpanded) then 
    FOnExpanded(Self);
end;

function TlkExpandingPanel.GetTitleFont: TFont;
begin
  Result := mTitleNamePanel.Font;
end;

procedure TlkExpandingPanel.SetExpanded(const Value: Boolean);
begin
  if FExpanded <> Value then
  begin
    FExpanded := Value;
    if Expanded then
      Expand
    else
      Collapse;
  end;
end;

(* -----------------------------------------------------------
  Gibt die H�he des Panels zur�ck, das vom User benutzt werden kann
-------------------------------------------------------------- *)
function TlkExpandingPanel.GetPanelHeight: Integer;
begin
  Result := Height;
  case Align of
    alTop, alBottom: result := width - mTitleNamePanel.Height;
    alLeft, alRight: result := Height;
  end;// case Align of
end;// function TlkExpandingPanel.GetPanelHeight: Integer;

function TlkExpandingPanel.GetTitle: string;
begin
  Result := mTitleNamePanel.Title;
end;

procedure TlkExpandingPanel.SetTitleFont(const Value: TFont);
begin
  mTitleNamePanel.Font := Value;
end;

(* -----------------------------------------------------------
  Setzt die H�he des Benutzerbereichs
-------------------------------------------------------------- *)
procedure TlkExpandingPanel.SetPanelHeight(const Value: Integer);
begin
  case Align of
    alTop, alBottom: Width := Value + mTitleNamePanel.Height;
    alLeft, alRight: width := Height;
  end;// case Align of
end;

procedure TlkExpandingPanel.SetTitle(const Value: string);
begin
  mTitleNamePanel.Title := Value;
end;

procedure TlkExpandingPanel.Resize;
begin
  inherited;
  SyncTitlePanelAlignment;
end;
{ TTitlePanel }

procedure TTitlePanel.Paint;
var
  xSize: TSize;
  xOldFont: TFont;
  xLogFont:TLogFont;
begin
  inherited;
  case Align of
    alTop, alBottom: Canvas.TextOut(20, 2, FTitle);
    alLeft, alRight: begin
      // MS SansSerif kann NICHT gedreht werden ==> anderen Font verwenden z.B. Tahoma
      xOldFont := TFont.Create;
      try
        xOldFont.Assign(Canvas.Font);
        try
          with xLogFont do begin
            lfHeight := xOldFont.Height;
            lfWidth := 0;
            lfEscapement := 900;
            lfOrientation := 0;
            if fsBold in xOldFont.Style then
              lfWeight := FW_Bold
            else
              lfWeight := FW_NORMAL;
            lfItalic := Byte(ByteBool(fsItalic in xOldFont.Style));
            lfUnderline := Byte(ByteBool(fsUnderline in xOldFont.Style));
            lfStrikeOut := Byte(ByteBool(fsStrikeOut in xOldFont.Style));
            lfCharSet :=  xOldFont.Charset;
            StrPCopy(lfFaceName, xOldFont.Name);
            lfQuality := PROOF_QUALITY;
            lfOutPrecision := OUT_TT_PRECIS;
            lfClipPrecision := CLIP_CHARACTER_PRECIS;
            lfPitchAndFamily := TMPF_TRUETYPE;
          end;

          Canvas.Font.Handle := CreateFontIndirect(xLogFont);
          GetTextExtentPoint32(Canvas.Handle, PChar(FTitle), Length(FTitle), xSize);

          Canvas.TextOut(0, 20 + xSize.cx, FTitle);
          DeleteObject(Canvas.Font.Handle);
        finally
          Canvas.Font.Assign(xOldFont);
        end;//
      finally
        FreeAndNil(xOldFont);
      end;
    end;// alLeft, alRight: begin
  end;// case Align of
end;

procedure TTitlePanel.SetTitle(const Value: string);
begin
  if FTitle <> Value then begin
    FTitle := Value;
    Invalidate;
  end;
end;

(* -----------------------------------------------------------
  Die Komponente soll die Caption des Panels unterdr�cken.
  Schlechter OO Stil, da Commitments verletzt werden. Ist aber
  so am einfachsten. Die Caption wird im allgemeinen sowiso nicht ben�tigt.
-------------------------------------------------------------- *)
procedure TlkExpandingPanel.SetName(const Value: TComponentName);
begin
  inherited;
  Caption := '';
end;// procedure TlkExpandingPanel.SetName(const Value: TComponentName);

(* -----------------------------------------------------------
  Gibt die Breite des Panels zur�ck, das vom User benutzt werden kann
-------------------------------------------------------------- *)
function TlkExpandingPanel.GetPanelWidth: Integer;
begin
  Result := Width;
  case Align of
    alTop, alBottom: result := Width;
    alLeft, alRight: result := width - mTitleNamePanel.Width;
  end;// case Align of
end;// function TlkExpandingPanel.GetPanelWidth: Integer;

(* -----------------------------------------------------------
  Setzt die Breite des Benutzerbereichs
-------------------------------------------------------------- *)
procedure TlkExpandingPanel.SetPanelWidth(const Value: Integer);
var
  xPanelWidth: Integer;
begin
  case Align of
    alTop, alBottom: Width := Value;
    alLeft, alRight: begin
      xPanelWidth := Value + mTitleNamePanel.Width;
      if Expanded then
        width := xPanelWidth
      else
        FExpandedWidth := xPanelWidth;
    end;// alLeft, alRight: begin
  end;// case Align of
end;// procedure TlkExpandingPanel.SetPanelWidth(const Value: Integer);

(* -----------------------------------------------------------
  Wird immer vor Collapse oder Expand aufgerufen, um die Settings zu Synchronisieren.
  Dies ist vor allem das erste mal notwendig, da das Panel am Anfang immer alTop ist.
-------------------------------------------------------------- *)
procedure TlkExpandingPanel.SyncTitlePanelAlignment;
begin
  case Align of
    alTop, alBottom: mTitleNamePanel.Align := alTop;
    alLeft, alRight: mTitleNamePanel.Align := alLeft;
  end;// case Align of
end;// procedure TlkExpandingPanel.SyncTitlePanelAlignment;

end.