unit RecordInput;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  VirtualTrees, comctrls, stdctrls, MMEdit, MSXML2_TLB, XMLGlobal,
  extctrls, typinfo, Buttons;

type
  TPropDataType = (dtNone,
                   dtNumber,
                   dtString,
                   dtEnum
                   ); // TPropDataType

(*  TBasicTypeNames = Array[TPropDataType] of string;
const
  cDataTypeNames: TBasicTypeNames = ('', 'num', 'string', 'enum');*)

const
  // Attribute
  cEleItem  = 'Item';
    cAtrItemName  = 'Name';
    cAtrItemTitle = 'Title';
    cAtrItemValue = 'Value';
    cAtrItemDataType = 'DataType';

  cEleEnum  = 'Enum';
    cAtrEnumName = 'Name';
    cAtrEnumValue = 'Value';

type
  (*---------------------------------------------------------
    Klasse um eine editierbare gruppierbare Eigenschaftsliste anzuzeigen

    Anwendung:
      with TfrmRecordInput.Create(nil) do try
        with xNodeListRec do begin
          AddItem('Allgemein');
          AddItem('MapID', rgMapFile.Items[rgMapFile.ItemIndex]);
          case xNodeListRec.netTyp of
            ntTxn: begin
              AddItem('Texnet');
              AddItem('AdapterNo', AdapterNo);
            end;
            ntWSC: begin
              AddItem('WSC');
              AddItem('IpAdresse', Copy(IpAdresse, 1, 16));
            end;
          else
            raise Exception.Create('TfrmMainWindow.SetNodeList: Netztyp nicht unterst�tzt')
          end;// case xNodeListRec.netTyp of
        end;// with xNodeListRec do begin

        ShowRecord;

        with DOM do begin
          xNodeListRec.MapID     := AttributeToVariantDef('Value', DocumentElement.SelectSingleNode('Item[@Name=''MapID'']'), xNodeListRec.MapID);
          xNodeListRec.AdapterNo := AttributeToVariantDef('Value', DocumentElement.SelectSingleNode('Item[@Name=''AdapterNo'']'), xNodeListRec.AdapterNo);
          xStr                   := copy(AttributeToVariantDef('Value', DocumentElement.SelectSingleNode('Item[@Name=''IpAdresse'']'), Copy(xNodeListRec.IpAdresse, 1, 16)), 1, 16);
          StrCopy(@xNodeListRec.IpAdresse, PChar(xStr));
        end;// with CODomDocument40.Create do begin
      finally
        free;
      end;// with TfrmRecordInput.Create(nil) do try
  ----------------------------------------------------------*)
  TfrmRecordInput = class(TForm)
    vtRecordInput: TVirtualStringTree;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    procedure vtRecordInputChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure vtRecordInputCreateEditor(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure vtRecordInputEditing(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
    procedure vtRecordInputGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vtRecordInputPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
  private
    FDom: DOMDocument40;
    function AddItem(aName: string; aValue: Variant): IXMLDOMElement; overload;
    function GetValueFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;
    function GetNameFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;
    function GetDataTypeFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): TPropDataType;
    function GetNameColWidth: Integer;
    function GetValueColWidth: Integer;
    function GetXMLElement(Sender: TBaseVirtualTree; Node: PVirtualNode): IXMLDOMElement;
    function IsTitle(Sender: TBaseVirtualTree; Node: PVirtualNode): Boolean;
    procedure SetNameColWidth(const Value: Integer);
    procedure SetValueColWidth(const Value: Integer);
    function _AddItem(aName: string; aTitle: boolean): IXMLDOMElement;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function AddItem(aName: string): IXMLDOMElement; overload;
    function AddItem(aName: string; aValue: Variant; aDataType: TPropDataType): IXMLDOMElement; overload;
    function AddEnum(aName: string; aValue: Variant;  aEnumValues: string): IXMLDOMElement; overload;
    function AddEnum(aName: string; aValue: Variant; aEnum: PTypeInfo): IXMLDOMElement; overload;
    procedure BuildTree;
    function ValueByName(aName: string; aDefault: variant): Variant;
    procedure InitDom;
    function Execute: Boolean;
    property Dom: DOMDocument40 read FDom write FDom;
    property NameColWidth: Integer read GetNameColWidth write SetNameColWidth;
    property ValueColWidth: Integer read GetValueColWidth write SetValueColWidth;
  end;

  // Our own edit link to implement several different node editors.
  TPropertyEditLink = class(TInterfacedObject, IVTEditLink)
  private
    mItemElement: IXMLDOMElement;
    mColumn: Integer;
    mLink: TWinControl;
    mNode: PVirtualNode;
    mParent: TfrmRecordInput;
    mTree: TVirtualStringTree;
  public
    destructor Destroy; override;
    function BeginEdit: Boolean; stdcall;
    function CancelEdit: Boolean; stdcall;
    procedure ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    constructor Create(aParent: TfrmRecordInput; aItemElement: IXMLDOMElement);
        reintroduce; virtual;
    function EndEdit: Boolean; stdcall;
    function GetBounds: TRect; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean; stdcall;
    procedure ProcessMessage(var Message: TMessage); stdcall;
    procedure SetBounds(R: TRect); stdcall;
  end;

var
  frmRecordInput: TfrmRecordInput;

implementation

uses
  LoepfeGlobal;

{$R *.DFM}

const
  cDocumentElement = 'Fields';

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TfrmRecordInput.Create(aOwner: TComponent);
begin
  inherited;
  // Initialisiert das DOM
  InitDom;
end;// constructor TfrmRecordInput.Create(aOwner: TComponent);

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TfrmRecordInput.Destroy;
begin
  FDom := nil;
  inherited;
end;// destructor TfrmRecordInput.Destroy;

(*---------------------------------------------------------
  F�gt ein neues Item Element ein mit dem angegebenen Name
----------------------------------------------------------*)
function TfrmRecordInput.AddItem(aName: string): IXMLDOMElement;
begin
  result := _AddItem(aName, true);
end;// function TfrmRecordInput.AddItem(aName: string): IXMLDOMNode;

(*---------------------------------------------------------
  Erzeugt ein neues Item Element mit dem Namen und dem angegeben Wert
----------------------------------------------------------*)
function TfrmRecordInput.AddItem(aName: string; aValue: Variant): IXMLDOMElement;
begin
  result := _AddItem(aName, false);
  result.setAttribute(cAtrItemValue, aValue);
end;// function TfrmRecordInput.AddItem(aName: string; aValue: Variant): IXMLDOMElement;

(*---------------------------------------------------------
  Erzeugt ein neues Item Element mit dem Namen und dem angegeben Wert
----------------------------------------------------------*)
function TfrmRecordInput.AddItem(aName: string; aValue: Variant; aDataType: TPropDataType): IXMLDOMElement;
begin
  result := AddItem(aName, aValue);
  result.setAttribute(cAtrItemDataType, GetEnumName(TypeInfo(TPropDataType), ord(aDataType)));
end;// function TfrmRecordInput.AddItem(aName: string; aValue: Variant): IXMLDOMElement;

(*---------------------------------------------------------
  Erzeugt ein neues Item Element mit dem Namen und dem angegeben Wert
----------------------------------------------------------*)
function TfrmRecordInput.AddEnum(aName: string; aValue: Variant;  aEnumValues: string): IXMLDOMElement;
var
  i: integer;
  xTempEnum: IXMLDOMElement;
begin
  result := AddItem(aName, aValue, dtEnum);
  with TStringList.Create do try
    CommaText := aEnumValues;
    for i := 0 to Count - 1 do begin
      xTempEnum := FDom.CreateElement(cEleEnum);
      result.appendChild(xTempEnum);
      xTempEnum.setAttribute(cAtrEnumName, Strings[i]);
      xTempEnum.setAttribute(cAtrEnumValue, i);
    end;// for i := 0 to Count - 1 do begin
  finally
    free;
  end;// with TStringList.Create do try
end;// function TfrmRecordInput.AddEnum(aName: string; aValue: Variant;  aEnumValues: string): IXMLDOMElement;

(*---------------------------------------------------------
  Erzeugt ein neues Item Element mit dem Namen und dem angegeben Wert
----------------------------------------------------------*)
function TfrmRecordInput.AddEnum(aName: string; aValue: Variant; aEnum: PTypeInfo): IXMLDOMElement;
var
  i: integer;
  xTempEnum: IXMLDOMElement;
  xTempBool: boolean;
begin
  if aEnum = System.TypeInfo(Boolean) then begin
    // Boolean und Variant zeigen leider ein etwas seltsames Verhalten (Variant -1 und 0 --> Delphi 1 und 0)
    xTempBool := aValue;
    result := AddItem(aName, integer(xTempBool), dtEnum);
  end else begin
    // F�r die anderen Enum Typen kann der Wert ermittelt werden
    result := AddItem(aName, aValue, dtEnum);
  end;

  for i := GetTypeData(aEnum)^.MinValue to GetTypeData(aEnum)^.MaxValue do begin
    xTempEnum := FDom.CreateElement(cEleEnum);
    result.appendChild(xTempEnum);
    xTempEnum.setAttribute(cAtrEnumName, GetEnumName(aEnum, i));
    xTempEnum.setAttribute(cAtrEnumValue, i);
  end;// for i := GetTypeData(aEnum)^.MinValue to GetTypeData(aEnum)^.MaxValue do begin
end;// function TfrmRecordInput.AddEnum(aName: string; aValue: Variant; aEnum: PTypeInfo): IXMLDOMElement;

(*---------------------------------------------------------
  Baut das Tree auf (Mit Kategorien)
----------------------------------------------------------*)
procedure TfrmRecordInput.BuildTree;
var
  i: Integer;
  xParentNode: PVirtualNode;
  xXMLNode: IXMLDOMNode;
begin
  xParentNode := nil;
  for i := 0 to FDom.documentElement.childNodes.length - 1 do begin
    xXMLNode := FDom.documentElement.childNodes.Item[i];
    if AttributeToBoolDef(cAtrItemTitle, xXMLNode, false) then
      xParentNode := vtRecordInput.AddChild(nil)
    else
      vtRecordInput.AddChild(xParentNode);
  end;// for i := 0 to FDom.documentElement.childNodes.length - 1 do begin

  // Das Tree aufmachen
  vtRecordInput.FullExpand;
end;// procedure TfrmRecordInput.BuildTree(aText:string);

(*---------------------------------------------------------
  Fragt den Wert der Einstellung ab
----------------------------------------------------------*)
function TfrmRecordInput.GetValueFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;
var
  xEnumElement: IXMLDOMNode;
begin
  case GetDataTypeFromNode(Sender, Node) of
    dtNone,
    dtNumber,
    dtString: result := AttributeToStringDef(cAtrItemValue, GetXMLElement(Sender, Node), '');

    dtEnum: begin
      xEnumElement := GetXMLElement(Sender, Node).selectSingleNode(Format('./Enum[@Value=../@Value]', [cAtrItemValue, cAtrEnumValue]));
      result := AttributeToStringDef(cAtrEnumName, xEnumElement, '');
    end;
    else
      raise Exception.Create('TfrmRecordInput.GetValueFromNode: Datentyp niht definiert.');
  end;// case GetDataTypeFromNode of
end;// function TfrmRecordInput.GetValueFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;

(*---------------------------------------------------------
  Fragt den Wert der Einstellung ab
----------------------------------------------------------*)
function TfrmRecordInput.GetNameFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;
begin
  result := AttributeToStringDef(cAtrItemName, GetXMLElement(Sender, Node), '');
end;// function TfrmRecordInput.GetNameFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;

(*---------------------------------------------------------
  Fragt den Wert der Einstellung ab
----------------------------------------------------------*)
function TfrmRecordInput.GetDataTypeFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): TPropDataType;
begin
  result := TPropDataType(GetEnumValue(TypeInfo(TPropDataType), AttributeToStringDef(cAtrItemDataType, GetXMLElement(Sender, Node), 'dtNone')));
end;// function TfrmRecordInput.GetValueFromNode(Sender: TBaseVirtualTree; Node: PVirtualNode): string;

(* -----------------------------------------------------------
  Holt einen Wert aus dem DOM
-------------------------------------------------------------- *)  
function TfrmRecordInput.ValueByName(aName: string; aDefault: variant): Variant;
begin
  result := AttributeToVariantDef(cAtrItemValue, FDom.DocumentElement.SelectSingleNode(Format('Item[@Name=''%s'']', [aName])), aDefault);
end;// procedure TfrmRecordInput.GetValue(aName: string; aDefault: variant);

(*---------------------------------------------------------
  Gibt das Element aus dem DOM zur�ck das dem entsprechenden Knoten entspricht
----------------------------------------------------------*)
function TfrmRecordInput.GetXMLElement(Sender: TBaseVirtualTree; Node: PVirtualNode): IXMLDOMElement;
begin
  Result := (FDom.DocumentElement.childNodes.Item[Sender.AbsoluteIndex(Node)] as IXMLDOMElement);
end;// function TfrmRecordInput.GetXMLNode(Sender: TBaseVirtualTree; Node: PVirtualNode): IXMLDOMNode;

(*---------------------------------------------------------
  Erzeugt und initialisiert das DOM. das bisherige DOM wird entsorgt
----------------------------------------------------------*)
procedure TfrmRecordInput.InitDom;
begin
  FDom := nil;
  FDOM := CoDOMDocument40.Create;
  InitXMLDocument(FDom, cDocumentElement);
end;// procedure TfrmRecordInput.InitDom;

(*---------------------------------------------------------
  Bestimmt ob ein Knoten ein Titel ist
----------------------------------------------------------*)
function TfrmRecordInput.IsTitle(Sender: TBaseVirtualTree; Node: PVirtualNode): Boolean;
begin
  result := AttributeToBoolDef(cAtrItemTitle, GetXMLElement(Sender, Node), false);
end;// function TfrmRecordInput.IsTitle(Sender: TBaseVirtualTree; Node: PVirtualNode): Boolean;

(*---------------------------------------------------------
  Zeigt den Record der als DOM aufgebaut wird.
  Die einzelnen Felder k�nnen Editiert werden.
----------------------------------------------------------*)
function TfrmRecordInput.Execute: Boolean;
begin
  BuildTree;
  ShowModal;
  result := (ModalResult = mrOK );
  ODS(FormatXML(fDOM.xml, #9));
end;// function TfrmRecordInput.ShowRecord(aText: string): string;

(*---------------------------------------------------------
  Initiert die Anzeige des Editors (TEdit) f�r den Wert des Knoten
----------------------------------------------------------*)
procedure TfrmRecordInput.vtRecordInputChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  if assigned(Node) then
    Sender.EditNode(Node, 1);
end;// procedure TfrmRecordInput.vtRecordInputChange(Sender: TBaseVirtualTree;...

(*---------------------------------------------------------
  Erzeugt den Editor (TEdit) um den Wert ver�ndern zu k�nnen.
  Da der PropertyEditor referenzgez�hlt wird, muss er nicht freigegeben werden
----------------------------------------------------------*)
procedure TfrmRecordInput.vtRecordInputCreateEditor(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  out EditLink: IVTEditLink);
begin
  EditLink := TPropertyEditLink.Create(self, GetXMLElement(Sender, Node));
end;// procedure TfrmRecordInput.vtRecordInputCreateEditor(...

(*---------------------------------------------------------
  Entscheidet ob editiert werden darf
----------------------------------------------------------*)
procedure TfrmRecordInput.vtRecordInputEditing(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := not(isTitle(Sender, Node)) and (Column = 1);
end;// procedure TfrmRecordInput.vtRecordInputEditing(Sender: TBaseVirtualTree;...

(*---------------------------------------------------------
  Zeigt den entsprecheneden Text an
----------------------------------------------------------*)
procedure TfrmRecordInput.vtRecordInputGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
begin
  CellText := '';
  case Column of
    // Name des Wertes
    0: CellText := GetNameFromNode(Sender,Node);
    // Wer anzeigen (wenn kein Titel)
    1: begin
      if not(IsTitle(Sender, Node)) then 
        CellText := GetValueFromNode(Sender, Node);
    end;// 1: begin
  else;
  end;// case Column of
end;// procedure TfrmRecordInput.vtRecordInputGetText(...

(*---------------------------------------------------------
  �ndert den Font f�r die Titel
----------------------------------------------------------*)
procedure TfrmRecordInput.vtRecordInputPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin
  // Im Normalfall nichts besonderes
  TargetCanvas.Font.Style := [];
  case Column of
    0: begin
      if IsTitle(Sender, Node) then
        // Titel werden Fett und Unterstrichen angezeigt
        TargetCanvas.Font.Style := [fsBold, fsUnderline];
    end;// 0: begin
  else;
  end;// case Column of
end;// procedure TfrmRecordInput.vtRecordInputPaintText(Sender: TBaseVirtualTree; ...

(*---------------------------------------------------------
  F�gt Intern ein Element hinzu
----------------------------------------------------------*)
function TfrmRecordInput._AddItem(aName: string; aTitle: boolean): IXMLDOMElement;
begin
  result := FDom.createElement(cEleItem);
  FDom.documentElement.appendChild(result);
  result.setAttribute(cAtrItemName, aName);
  if aTitle then
    result.setAttribute(cAtrItemTitle, aTitle);
end;// function TfrmRecordInput._AddItem(aName: string; aTitle: boolean): IXMLDOMElement;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TPropertyEditLink.Destroy;
begin
  mLink.Free;
  inherited;
end;// destructor TPropertyEditLink.Destroy;

(*---------------------------------------------------------
  Das Feld soll in der entsprechenden Spalte angezeigt werden
----------------------------------------------------------*)
function TPropertyEditLink.BeginEdit: Boolean;
begin
  mParent.butOk.Default := False;
  Result := True;
  mLink.Show;
  mLink.SetFocus;
end;// function TPropertyEditLink.BeginEdit: Boolean;

(*---------------------------------------------------------
  Der Editor soll mit der Escape Taste verlassen werden. Der Wert wird nicht �bernommen
----------------------------------------------------------*)
function TPropertyEditLink.CancelEdit: Boolean;
begin
  Result := True;
  mLink.Hide;
  mParent.butOk.Default := true;
end;// function TPropertyEditLink.CancelEdit: Boolean;

(*---------------------------------------------------------
  OnKeyDown Handler f�r das Control
----------------------------------------------------------*)
procedure TPropertyEditLink.ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    // ESCAPE verl�ssst das Control, ohne den Wert zu �bernehmen
    VK_ESCAPE: begin
      mTree.CancelEditNode;
      mTree.SetFocus;
    end;// VK_ESCAPE: begin

    // �bernimmt den Wert und selektiert den n�chsten Knoten
    VK_DOWN : begin
      mTree.EndEditNode;
      mTree.SetFocus;
      mTree.FocusedNode := mTree.GetNext(mTree.FocusedNode);
    end;// VK_DOWN : begin

    // �bernimmt den Wert und selektiert den vorderen Knoten
    VK_UP: begin
      mTree.EndEditNode;
      mTree.SetFocus;
      mTree.FocusedNode := mTree.GetPrevious(mTree.FocusedNode);
    end;// VK_UP: begin

    // Ubernimmt den Wert
    VK_RETURN: begin
      mParent.butOk.Default := true;
      mTree.EndEditNode;
      mTree.SetFocus;
    end;// VK_ENTER, VK_RETURN: begin
  end;// case Key of

  if Key in [VK_DOWN, VK_UP] then begin
    mTree.ClearSelection;
    mTree.Selected[mTree.FocusedNode] := true;
    Key := 0;
  end;// if Key in [VK_DOWN, VK_UP] then begin
end;// procedure TPropertyEditLink.ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TPropertyEditLink.Create(aParent: TfrmRecordInput; aItemElement:
    IXMLDOMElement);
begin
  mParent := aParent;
  mItemElement := aItemElement;
end;// procedure TPropertyEditLink.Create(aInputForm: TfrmRecordInput);

(*---------------------------------------------------------
  Wird aufgerufen, wenn ein Knoten angeklickt wurde
----------------------------------------------------------*)
function TPropertyEditLink.EndEdit: Boolean;
var
  P: TPoint;
  Dummy: Integer;
begin
  // �berpr�fen, ob auf den editierten Knoten geklickt wurde
  GetCursorPos(P);
  P := mTree.ScreenToClient(P);
  Result := mTree.GetNodeAt(P.X, P.Y, True, Dummy) <> mNode;

  if Result then begin
    // Wert ins DOM eintragen
    if mLink is TCustomEdit then
      mItemElement.setAttribute(cAtrItemValue, TCustomEdit(mLink).Text)
    else if mLink is TComboBox then
      mItemElement.setAttribute(cAtrItemValue, TComboBox(mLink).ItemIndex)
    else
      raise Exception.Create('TPropertyEditLink.EndEdit: Unbekanntes Control');
    mLink.Hide;
  end;// if Result then begin
end;// function TPropertyEditLink.EndEdit: Boolean;

(*---------------------------------------------------------
  Wird aufgerufen um den Umfang des Controls zu erfragen
----------------------------------------------------------*)
function TPropertyEditLink.GetBounds: TRect;
begin
  Result := mLink.BoundsRect;
end;// function TPropertyEditLink.GetBounds: TRect;

(*---------------------------------------------------------
  Wird aufgerufen, bevor ein Control angezeigt werden soll
----------------------------------------------------------*)
function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;
var
  i: Integer;
  xDataType: TPropDataType;
  xNodeList: IXMLDOMNodeList;
begin
  // Die internen Paramerter sichern
  mTree := nil;
  if Tree is TVirtualStringTree then
    mTree := TVirtualStringTree(Tree);
  mNode := Node;
  mColumn := Column;

  // ein evt. vorheriges Editfeld freigeben
  FreeAndNil(mLink);

  xDataType := TPropDataType(GetEnumValue(TypeInfo(TPropDataType), AttributeToStringDef(cAtrItemDataType, mItemElement, 'dtNone')));
  case xDataType of
    dtNumber, dtString: begin
      // Neues Editfeld erzeugen
      mLink := TMMEdit.Create(nil);
      with mLink as TMMEdit do begin
        Visible := False;
        Parent := Tree;
        Text := AttributeToStringDef(cAtrItemValue, mItemElement, '');
        NumMode := (xDataType = dtNumber);
        OnKeyDown := ControlKeyDown;
      end;// with mEdit as TMMEdit do begin
    end;// dtNumber, dtString: begin

    dtEnum: begin
      mLink := TCombobox.Create(mParent);
      with mLink as TCombobox do begin
        Visible := False;
        Parent := Tree;
        Style := csDropDownList;
        xNodeList := mItemElement.SelectNodes('./' + cEleEnum);
        for i := 0 to xNodeList.length - 1 do
          Items.Add(AttributeToStringDef(cAtrEnumName, xNodeList.Item[i], ''));
        ItemIndex := AttributeToIntDef(cAtrItemValue, mItemElement, -1);
      end;// with mLink as TmmCombobox do begin
    end;
  end;// case xDataType of
  // Nur weiter, wenn das Tree g�ltig ist
  Result := assigned(mTree);
end;// function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;

(*---------------------------------------------------------
  Wird vom Interface ben�tigt
----------------------------------------------------------*)
procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
  mLink.WindowProc(Message);
end;// procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);

(*---------------------------------------------------------
  Wird aufgerufen um die Gr�sse des Controls an die Spalte anzupassen
----------------------------------------------------------*)
procedure TPropertyEditLink.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  // Since we don't want to activate grid extensions in the tree (this would influence how the selection is drawn)
  // we have to set the edit's width explicitly to the width of the column.
  mTree.Header.Columns.GetColumnBounds(mColumn, Dummy, R.Right);
  mLink.BoundsRect := R;
end;// procedure TPropertyEditLink.SetBounds(R: TRect);

(*---------------------------------------------------------
  �bernimmt die letzte �nderung
----------------------------------------------------------*)
procedure TfrmRecordInput.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  vtRecordInput.EndEditNode;
end;// procedure TfrmRecordInput.FormClose(Sender: TObject;

procedure TfrmRecordInput.FormShow(Sender: TObject);
var
  xFound: Boolean;
  xNode: PVirtualNode;
begin
  xFound := false;
  xNode := vtRecordInput.GetFirst;
  while assigned(xNode) and (not(xFound)) do begin
    if not(IsTitle(vtRecordInput, xNode)) then begin
      vtRecordInput.EditNode(xNode, 1);
      xFound := true;
    end;// if not(IsTitle(vtRecordInput, xNode) then begin
    xNode := vtRecordInput.GetNext(xNode);
  end;//  while assigned(xNode) do begin
end;

(*---------------------------------------------------------
  Liefert die Breite der Namensspalte
----------------------------------------------------------*)
function TfrmRecordInput.GetNameColWidth: Integer;
begin
  Result := vtRecordInput.Header.Columns[0].Width;
end;// function TfrmRecordInput.GetNameColWidth: Integer;

(*---------------------------------------------------------
  Liefert die Breite der Wertspalte
----------------------------------------------------------*)
function TfrmRecordInput.GetValueColWidth: Integer;
begin
  Result := vtRecordInput.Header.Columns[1].Width;
end;// function TfrmRecordInput.GetValueColWidth: Integer;

(*---------------------------------------------------------
  Setzt die Breite der Namensspalte
----------------------------------------------------------*)
procedure TfrmRecordInput.SetNameColWidth(const Value: Integer);
begin
  vtRecordInput.Header.Columns[0].Width := Value;
end;// procedure TfrmRecordInput.SetNameColWidth(const Value: Integer);

(*---------------------------------------------------------
  Setzt die Breite der Wertspalte
----------------------------------------------------------*)
procedure TfrmRecordInput.SetValueColWidth(const Value: Integer);
begin
  vtRecordInput.Header.Columns[1].Width := Value;
end;// procedure TfrmRecordInput.SetValueColWidth(const Value: Integer);

end.
