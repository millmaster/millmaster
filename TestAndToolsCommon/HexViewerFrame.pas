unit HexViewerFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  VirtualTrees, mmVirtualStringTree, ExtCtrls, mmPanel,
  ComCtrls, mmStatusBar, Grids, mmStringGrid, StdCtrls, mmCheckBox,
  lkExpandingPanel, ImgList, mmImageList, ActnList, mmActionList, ToolWin,
  mmToolBar;

type
  PReal48 = ^Real48;

  TDisplayState = (dsNone,
                   dsHex,
                   dsASCII,
                   dsDez);// TDisplayState

  TDataType = (dtByte,
               dtshortInt,
               dtWord,
               dtSmallInt,
               dtDWord,
               dtInteger,
               dtInt64,
               dtSingle,
               dtReal48,
               dtDouble,
               dtExtended,
               dtString,
               dtBit);// TDataType

  TIntArray = array of Integer;

const
  cDataType: Array[TDataType] of string = ('Byte',
                                           'Shortint',
                                           'Word',
                                           'Smallint',
                                           'DWord',
                                           'Integer',
                                           'Int64',
                                           'Single',
                                           'Real48',
                                           'Double',
                                           'Extended',
                                           'String',
                                           'Bit');

type
  TBinFile = class;
  
  TfrmHexViewerFrame = class(TFrame)
    paClient: TmmPanel;
    paRight: TmmPanel;
    vstBinGrid: TmmVirtualStringTree;
    sb: TmmStatusBar;
    mValueGrid: TmmStringGrid;
    paTop: TmmPanel;
    cbSwap: TmmCheckBox;
    mmActionList1: TmmActionList;
    acDez: TAction;
    acHex: TAction;
    acASCII: TAction;
    mmImageList1: TmmImageList;
    paLeft: TmmPanel;
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    b1: TToolButton;
    acSave: TAction;
    mSaveDialog: TSaveDialog;
    procedure mBinGridChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure mBinGridGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
    procedure mBinGridAfterCellPaint(Sender: TBaseVirtualTree; TargetCanvas:
        TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellRect: TRect);
    procedure cbSwapClick(Sender: TObject);
    procedure vstBinGridColumnClick(Sender: TBaseVirtualTree;
      Column: TColumnIndex; Shift: TShiftState);
    procedure vstBinGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure acDezExecute(Sender: TObject);
    procedure acHexExecute(Sender: TObject);
    procedure acASCIIExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure GridBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas;
        Node: PVirtualNode; Column: TColumnIndex; CellRect: TRect);
  private
    mColorIndex: TIntArray;
    FBinFile: TBinFile;
    FDisplayState: TDisplayState;
    mExpandingPanel: TLKExpandingPanel;
    procedure ConfigureCols;
    procedure CreateCols;
    function GetDataTypeExpand: Boolean;
    function GetSelectedIndex: cardinal;
    procedure UpdateStatusbar;
    procedure UpdateValue;
    procedure SetBinFile(const aValue: TBinFile);
    procedure SetDataTypeExpand(const aValue: Boolean);
    procedure SetDisplayState(const aValue: TDisplayState);
    { Private declarations }
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure setColorIndex(aIntArray: TIntArray);
    property BinFile: TBinFile read FBinFile write SetBinFile;
    property DataTypeExpand: Boolean read GetDataTypeExpand write SetDataTypeExpand;
    property DisplayState: TDisplayState read FDisplayState write SetDisplayState;
  end;

  TBinFile = class(TObject)
  private
    FBasePointer: PByte;
    FFileName: string;
    FLen: cardinal;
    FColWidth: cardinal;
    FSaveWordWidth: Integer;
    function GetChar(aIndex: cardinal): Char;
  public
    constructor Create;virtual;
    destructor Destroy;override;

    procedure UnloadFile;
    function LoadFile(aFileName: string): boolean;
    function GetByte(aIndex: cardinal): Byte;
    function GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;
    procedure CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal; out
        aLengthDifference: cardinal);

    function GetInteger(aIndex: cardinal; aLength: cardinal; aSigned: boolean;
        aSwap: boolean): int64;
    function GetFloat(aIndex: cardinal; aLength: cardinal): extended;
    function GetString(aIndex: cardinal; aLength: cardinal): string;
    function GetBit(aIndex: cardinal; aLength: cardinal): string;
    function SaveFile(aFileName: string): boolean;
    procedure NewMemFile(aBasePointer: PByte; aLength: cardinal);

    property BasePointer: PByte read FBasePointer;
    property Length: cardinal read FLen;
    property FileName: string read FFileName write FFileName;
    property ColWidth: cardinal read FColWidth write FColWidth;
    property Byte[aIndex: cardinal]:Byte read GetByte;
    property ASCII[aIndex: cardinal]:Char read GetChar;
    property SaveWordWidth: Integer read FSaveWordWidth write FSaveWordWidth;
  end;// TBinFile = class(TObject)

implementation

uses
  LoepfeGlobal;

{$R *.DFM}

const
  // 1 Splate f�r die Zeilenzahl, 16 Spalten f�r Hex und 1 Spalte f�r den Text
  cMaxTreeCol = 18;
  cDafaultColWidth = 35;
  cLineNumberColWidth = 40;
  cDezCols = 10;
  cHexCols = 16;

  // Statusbar
  cDisplayStatePanel = 0;
  cCoordinatePanel = 1;
  cValuePanel = 2;
  cFileNamePanel = 3;

constructor TBinFile.Create;
begin
  inherited;
  FBasePointer := nil;
  // Anzahl Bytes die beim Speichern ein Wert ergeben
  SaveWordWidth := 2;
end;

destructor TBinFile.Destroy;
begin
  UnloadFile;
  inherited;
end;

(*---------------------------------------------------------
  TBinFile
----------------------------------------------------------*)
procedure TBinFile.CompareWith(aBinFile: TBinFile; out aDifferenceCount:
    cardinal; out aLengthDifference: cardinal);
var
  xLength: cardinal;
  i: integer;
begin
  aDifferenceCount := 0;

  if assigned(aBinFile) then begin
    xLength := self.Length;
    if aBinFile.Length < xLength then
      xLength := aBinFile.Length;

    // Midestens der Unterschied in der L�nge der Files
    aLengthDifference := abs(self.Length - aBinFile.Length);

    for i := 0 to xLength - 1 do begin
      if Byte[i] <> aBinFile.Byte[i] then
        inc(aDifferenceCount)
    end;// for i := 0 to xLength - 1 do begin
  end;// if assigned(aBinFile) then begin
end;// function TBinFile.CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal): boolean;

function TBinFile.GetBit(aIndex: cardinal; aLength: cardinal): string;
var
  i, j: cardinal;
  xByte: system.Byte;
  xTempByte: string;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xTempByte := '';
    xByte := Byte[aIndex + i];
    for j := 0 to 7 do begin
      if j = 4 then
        xTempByte := '-' + xTempByte;
      if (xByte and (1 shl j)) = (1 shl j) then
        xTempByte := '1' + xTempByte
      else
        xTempByte := '0' + xTempByte;
    end;// for j := 0 to 7 do begin
    result := result + ' ' + xTempByte;
  end;// for i := 0 to aLength - 1 do begin

  if system.Length(result) > 0  then
    system.delete(result, 1, 1);
end;

function TBinFile.GetByte(aIndex: cardinal): Byte;
begin
  result := 0;
  if aIndex < FLen then 
    result := PByte(cardinal(FBasePointer) + aIndex)^;
end;

function TBinFile.GetChar(aIndex: cardinal): Char;
var
  xByte: System.Byte;
begin
  xByte := Byte[aIndex];
  if xByte >= ord(' ') then
    result := chr(xByte)
  else
    result := '.';
end;

function TBinFile.GetFloat(aIndex: cardinal; aLength: cardinal): extended;
begin
  result := 0;
  if (aIndex + aLength) < self.Length then begin
    case aLength of
      4: result := PSingle(cardinal(FBasePointer) + aIndex)^;
      6: result := PReal48(cardinal(FBasePointer) + aIndex)^;
      8: result := PDouble(cardinal(FBasePointer) + aIndex)^;
      10: result := PExtended(cardinal(FBasePointer) + aIndex)^;
    end;// case aLength of
  end;// if (aIndex + aLength) < self.Length then begin
end;

function TBinFile.GetInteger(aIndex: cardinal; aLength: cardinal; aSigned:
    boolean; aSwap: boolean): int64;
begin
  result := 0;
  if ((aIndex + aLength) <= self.Length) then begin
    if aSigned then begin
      case aLength of
        1: result := PShortInt(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
            result := Swap(PSmallInt(cardinal(FBasePointer) + aIndex)^)
           else
            result := PSmallInt(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PInteger(cardinal(FBasePointer) + aIndex)^)
           else
             result := PInteger(cardinal(FBasePointer) + aIndex)^;
        8: result := PInt64(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end else begin
      case aLength of
        1: result := PByte(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
             result := swap(PWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PWord(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PDWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PDWord(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end;// if aSigned then begin
  end;// if ((aIndex + aLength) <= self.Length) then begin
end;

function TBinFile.GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;
begin
  result := '';
  if aIndex < FLen then begin
    case aDisplayState of
      dsHex:   result := IntToHex(Byte[aIndex], 2);
      dsDez:   result := IntToStr(Byte[aIndex]);
      dsASCII: result := ASCII[aIndex];
    end;// case mDisplayState of
  end;// if aIndex < FLen then
end;// function TBinFile.GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;

function TBinFile.GetString(aIndex: cardinal; aLength: cardinal): string;
var
  i: cardinal;
  xByte: system.Byte;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xByte := Byte[aIndex + i];
    if xByte >= ord(' ') then
      result := result + chr(xByte)
    else
      result := result + '.';
  end;// for i := 0 to aLength - 1 do begin
end;

function TBinFile.LoadFile(aFileName: string): boolean;
var
  xStream: TFileStream;
begin
  result := false;
  UnloadFile;
  
  if FileExists(aFileName) then begin
    xStream := nil;
    try
      xStream := TFileStream.Create(aFileName, fmOpenRead	or fmShareDenyNone);
      xStream.Position := 0;
      try
        GetMem(FBasePointer, xStream.Size);
        FLen := xStream.Read(FBasePointer^, xStream.Size);
        result := true;
        FFileName := aFileName;
      except
        on e:EOutOfMemory do
          FBasePointer := nil;
      end;// try except
    finally
      if assigned(xStream) then
        xStream.Free;
    end;// try finally
  end;// if FileExists(aFileName) then begin
end;// TForm1.LoadBinFile cat:No category

procedure TBinFile.UnloadFile;
begin
  if assigned(FBasePointer) then
    FreeMem(FBasePointer);
  FBasePointer := nil;
  FLen := 0;
end;

constructor TfrmHexViewerFrame.Create(aOwner: TComponent);
var
  i: TDataType;
begin
  inherited;
  SetLength(mColorIndex , 2);
  SetLength(mColorIndex , 0);

  CreateCols;
  FDisplayState := dsDez;

  // StringGrid beschriften
  mValueGrid.Cells[0,0] := 'Datentyp';
  mValueGrid.Cells[1,0] := 'Wert';
  for i := Low(TDataType) to High(TDataType) do
    mValueGrid.Cells[0, ord(i) + 1] := cDataType[i];

  mValueGrid.ColWidths[0] := 55;
  mValueGrid.ColWidths[1] := 145;


  mExpandingPanel := TLKExpandingPanel.Create(self);
  with mExpandingPanel do begin
    Font.Name := 'Tahoma';
    Align := alRight;
    ExpandedWidth := 227;
    Title := 'Datentypen';
    Parent := self;
    Expanded := true;
    TitleFont.Style := [fsBold, fsItalic];
    paRight.Parent := mExpandingPanel;
    paRight.Align := alClient;
    Visible := true;
  end;// with mExpandingPanel do begin
end;

procedure TfrmHexViewerFrame.ConfigureCols;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin
  // Zuerst alle Spalten ausblenden
  for i := 0 to vstBinGrid.Header.Columns.Count - 1 do begin
    xTreeCol := vstBinGrid.Header.Columns[i];
    xTreeCol.Options := xTreeCol.Options - [coVisible];
  end;// for i := 0 to aTree.Header.Columns.Count - 1 do begin

  // Zuerst die Spalte f�r dem Zeichenindex des ersten Zeichens
  xTreeCol := vstBinGrid.Header.Columns[0];
  xTreeCol.Options := xTreeCol.Options + [coVisible];
  xTreeCol.Options := xTreeCol.Options - [coAllowClick, coEnabled];
  xTreeCol.Text := '#';

  // Dann die Spalten f�r die ersten 10 Bytes
  for i := 0 to 9 do begin
    xTreeCol := vstBinGrid.Header.Columns[i + 1];
    xTreeCol.Options := xTreeCol.Options + [coVisible];
    xTreeCol.Text := intToStr(i);
  end;// for i := 0 to 9 do begin

  if assigned(FBinFile) then begin
    // F�r die Hex Anzeige
    if FDisplayState = dsHex then begin
      // Hexadezimale Darstellung
      //  ... Die restlichen Spalten anzeigen
      for i := 10 to 15 do begin
        xTreeCol := vstBinGrid.Header.Columns[i + 1];
        xTreeCol.Options := xTreeCol.Options + [coVisible];
        xTreeCol.Text := intToHex(i,1);
      end;// for i := 10 to 15 do begin

      // Anzahl Zeilen definieren
      vstBinGrid.RootNodeCount := (FBinFile.Length div cHexCols);
      if vstBinGrid.RootNodeCount * cHexCols < FBinFile.Length then
        vstBinGrid.RootNodeCount := vstBinGrid.RootNodeCount + 1;
    end else begin // if mDisplayState = dsHex then begin
      // Dezimale Darstellung
      // Anzahl Zeilen definieren
      vstBinGrid.RootNodeCount := (FBinFile.Length div cDezCols);
      if vstBinGrid.RootNodeCount * cDezCols < FBinFile.Length then
        vstBinGrid.RootNodeCount := vstBinGrid.RootNodeCount + 1;
    end;// if mDisplayState = dsHex then begin
  end else begin
    vstBinGrid.RootNodeCount := 0;
  end;// if assigned(FBinFile) then begin
end;

procedure TfrmHexViewerFrame.CreateCols;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin
  // Spalten f�r das Tree erzeugen (eine Spalte pro Methode)
  for i := 0 to cMaxTreeCol - 1 do begin
    xTreeCol := vstBinGrid.Header.Columns.Add;
    xTreeCol.Options := xTreeCol.Options - [coVisible];
    xTreeCol.Width := cDafaultColWidth;
  end;// for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin

  vstBinGrid.Header.Columns[0].Width := cLineNumberColWidth;
end;

function TfrmHexViewerFrame.GetSelectedIndex: cardinal;
var
  xColCount: cardinal;
begin
  xColCount := cHexCols;
  case FDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez: xColCount := cDezCols;
    dsASCII:  xColCount := cDezCols;
  end;// case mDisplayState of
  result := vstBinGrid.AbsoluteIndex(vstBinGrid.FocusedNode) * xColCount + cardinal(vstBinGrid.FocusedColumn) - 1;
end;

procedure TfrmHexViewerFrame.mBinGridChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
  UpdateValue;
end;

procedure TfrmHexViewerFrame.mBinGridGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType; var CellText: WideString);
var
  xColCount: cardinal;
begin
  if not(assigned(FBinFile)) then
    exit;

  if Column >= 1 then begin
    xColCount := cHexCols;
    case FDisplayState of
      dsHex: xColCount := cHexCols;
      dsDez, dsASCII: xColCount := cDezCols;
    end;// case mDisplayState of

    CellText := FBinFile.GetNumberString(Sender.AbsoluteIndex(Node) * xColCount + (cardinal(Column) - 1), FDisplayState);
  end;// if Column >= 1 then begin
end;// procedure TfrmMainWindow.mBinGrid1GetText(...

procedure TfrmHexViewerFrame.mBinGridAfterCellPaint(Sender: TBaseVirtualTree;
    TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellRect:
    TRect);
var
  xTree: TVirtualStringTree;
  xLineIndex: string;
begin
  if Column = 0 then begin
    if Sender is TVirtualStringTree then begin
      xTree := TVirtualStringTree(Sender);
      with TargetCanvas do begin
        // Simuliert die Anzeige einer FixedRow wie in TStringGrid
        if toShowVertGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Right);
        if toShowHorzGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Bottom);
        // Zeichnet eine 3D Fl�che
        DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);

        xLineIndex := '';
        case FDisplayState of
          dsHex : xLineIndex :=  IntToHex(Sender.AbsoluteIndex(Node) * cHexCols, 4);
          dsDez, dsASCII : xLineIndex :=  IntToStr(Sender.AbsoluteIndex(Node) * cDezCols);
        end;// case mDisplayState of

        TargetCanvas.TextOut(CellRect.Left, CellRect.Top, xLineIndex);
      end;// with TargetCanvas do begin
    end;// if Sender is TVirtualStringTree then begin
  end;// if Column = 0 then begin
end;

procedure TfrmHexViewerFrame.UpdateStatusbar;
begin
  // DisplayModus
  case FDisplayState of
    dsHex:  sb.Panels[cDisplayStatePanel].Text := 'HEX';
    dsDez: sb.Panels[cDisplayStatePanel].Text := 'DEZ';
    dsASCII: sb.Panels[cDisplayStatePanel].Text := 'ASCII';
  end;// case mDisplayState of

  // Selektiertes Element
  if assigned(vstBinGrid.FocusedNode) then begin
    // Index des selektierten Elements anzeigen
    sb.Panels[cCoordinatePanel].Text := Format('Index: $%s (%d)', [IntToHex(GetSelectedIndex, 4), GetSelectedIndex]);

    if assigned(FBinFile) then begin
      // Wert des Elements
      with FBinFile do
        sb.Panels[cValuePanel].Text := Format('Wert: $%s (%s)', [GetNumberString(GetSelectedIndex, dsHex), GetNumberString(GetSelectedIndex, dsDez)]);
    end;// if assigned(FBinFile) then begin
  end;// if assigned(aGrid.FocusedNode) then begin

  if assigned(FBinFile) then
    // Filename
    sb.Panels[cFileNamePanel].Text := FBinFile.FileName;
end;// procedure TfrmMainWindow.UpdateStatusbar(aGrid: TVirtualStringTree);

procedure TfrmHexViewerFrame.UpdateValue;
var
  xTemp: string;
  xIndex: integer;
  xSwap: boolean;
  i: Integer;
begin
  UpdateStatusbar();

  if assigned(FBinFile) then begin

    xIndex := GetSelectedIndex;

    xSwap := cbSwap.Checked;

    with mValueGrid do begin
      Cells[1, ord(dtByte) + 1] := intToStr(FBinFile.GetInteger(xIndex, 1, false, xSwap));

      Cells[1, ord(dtshortInt) + 1] := intToStr(FBinFile.GetInteger(xIndex, 1, true, xSwap));

      Cells[1, ord(dtWord) + 1] := intToStr(FBinFile.GetInteger(xIndex, 2, false, xSwap));

      Cells[1, ord(dtSmallInt) + 1] := intToStr(FBinFile.GetInteger(xIndex, 2, true, xSwap));

      Cells[1, ord(dtDWord) + 1] := intToStr(FBinFile.GetInteger(xIndex, 4, false, xSwap));

      Cells[1, ord(dtInteger) + 1] := intToStr(FBinFile.GetInteger(xIndex, 4, true, xSwap));

      Cells[1, ord(dtInt64) + 1] := intToStr(FBinFile.GetInteger(xIndex, 8, true, xSwap));

      Cells[1, ord(dtSingle) + 1] := FloatToStr(FBinFile.GetFloat(xIndex, 4));

      Cells[1, ord(dtReal48) + 1] := FloatToStr(FBinFile.GetFloat(xIndex, 6));

      Cells[1, ord(dtDouble) + 1] := FloatToStr(FBinFile.GetFloat(xIndex, 8));

      Cells[1, ord(dtExtended) + 1] := '-';
{      try
        Cells[1, ord(dtExtended) + 1] := FloatToStr(FBinFile.GetFloat(xIndex, 10));
      except
        Cells[1, ord(dtExtended) + 1] := '-'
      end;}

      Cells[1, ord(dtString) + 1] := FBinFile.GetString(xIndex, 30);

      if xSwap then begin
        xTemp := FBinFile.GetBit(xIndex, 2);
        if Length(xTemp) >= 16 then
          Cells[1, ord(dtBit) + 1] := copy(xTemp, 11, 9) + ' ' + copy(xTemp, 1,9)
        else
          Cells[1, ord(dtBit) + 1] := 'Fehler';
      end else
        Cells[1, ord(dtBit) + 1] := FBinFile.GetBit(xIndex, 2);
    end;// with mValueGrid do begin
  end else begin
    // Wenn kein Binfile zugewiesen ist, dann das Grid leeren
    for i := 1 to mValueGrid.RowCount - 1 do
      mValueGrid.Cells[1, i] := '';
  end;// if assigned(FBinFile) then begin
end;


procedure TfrmHexViewerFrame.SetBinFile(const aValue: TBinFile);
begin
  FBinFile:= aValue;
  ConfigureCols;
  UpdateValue;
  vstBinGrid.Invalidate;
end;

function TBinFile.SaveFile(aFileName: string): boolean;
var
  xStream: TFileStream;
  xCurrentByte: Pointer;
  xText: string;
begin
  result := false;

  xStream := TFileStream.Create(aFileName, fmOpenWrite or fmCreate);
  try
    if Assigned(FBasePointer) then begin
      xCurrentByte := FBasePointer;
      while (Cardinal(xCurrentByte) < (Cardinal(FBasePointer) + FLen)) do begin
        case FSaveWordWidth of
          1: xText := IntToStr(PByte(xCurrentByte)^) + cCRlf;
          2: xText := IntToStr(PWord(xCurrentByte)^) + cCRlf;
          4: xText := IntToStr(PDWord(xCurrentByte)^) + cCRlf;
          else
            raise Exception.Create('TBinFile.SaveFile: Falsche Wortgr�sse angegeben');
        end;// case FSaveWordWidth of
        xStream.Write(PChar(xText)^, System.Length(xText));
        xCurrentByte := Pointer(Cardinal(xCurrentByte) + Cardinal(FSaveWordWidth));
      end;// while (xCurrentByte < (FBasePointer + FLen)) do begin
    end;// if Assigned(FBasePointer) then begin
  finally
    FreeAndNil(xStream);
  end;
end;// TForm1.LoadBinFile cat:No category

procedure TBinFile.NewMemFile(aBasePointer: PByte; aLength: cardinal);
begin
  UnloadFile;
  FBasePointer := aBasePointer;
  FLen := aLength;
end;

destructor TfrmHexViewerFrame.Destroy;
begin
  SetLength(mColorIndex , 0);
  FreeAndNil(mExpandingPanel);
  inherited;
end;

procedure TfrmHexViewerFrame.cbSwapClick(Sender: TObject);
begin
  UpdateValue;
end;

function TfrmHexViewerFrame.GetDataTypeExpand: Boolean;
begin
  Result := mExpandingPanel.Expanded;
end;

procedure TfrmHexViewerFrame.SetDataTypeExpand(const aValue: Boolean);
begin
  mExpandingPanel.Expanded := aValue;
end;

procedure TfrmHexViewerFrame.vstBinGridColumnClick(
  Sender: TBaseVirtualTree; Column: TColumnIndex; Shift: TShiftState);
begin
  UpdateValue;
end;

procedure TfrmHexViewerFrame.vstBinGridKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key in [VK_HOME, VK_END, VK_PRIOR, VK_NEXT, VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_BACK, VK_TAB] then 
    UpdateValue;
end;

procedure TfrmHexViewerFrame.acDezExecute(Sender: TObject);
begin
  FDisplayState := dsDez;
  ConfigureCols;
end;

procedure TfrmHexViewerFrame.acHexExecute(Sender: TObject);
begin
  FDisplayState := dsHex;
  ConfigureCols;
end;

procedure TfrmHexViewerFrame.acASCIIExecute(Sender: TObject);
begin
  FDisplayState := dsASCII;
  ConfigureCols;
end;

procedure TfrmHexViewerFrame.SetDisplayState(const aValue: TDisplayState);
begin
  case aValue of
    dsDez: begin
      acDez.Execute;
      acDez.Checked := true;
      acHex.Checked := false;
      acASCII.Checked := false;
    end;// dsDez: begin
    dsHex: begin
      acHex.Execute;
      acDez.Checked := false;
      acHex.Checked := true;
      acASCII.Checked := false;
    end;// dsHex: begin
    dsAscii: begin
      acAscii.Execute;
      acDez.Checked := false;
      acHex.Checked := false;
      acASCII.Checked := true;
    end;// dsAscii: begin
  end;
end;

procedure TfrmHexViewerFrame.acSaveExecute(Sender: TObject);
begin
  if mSaveDialog.Execute then
    FBinFile.SaveFile(mSaveDialog.FileName);
end;

procedure TfrmHexViewerFrame.GridBeforeCellPaint(Sender: TBaseVirtualTree;
    TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellRect:
    TRect);
var
  xColCount: cardinal;
  xIndex: integer;
  i: Integer;
begin
  xColCount := cHexCols;
  case fDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez, dsASCII: xColCount := cDezCols;
  end;// case fDisplayState of

  xIndex := Sender.AbsoluteIndex(Node) * xColCount + Cardinal((Column - 1));
  for i := 0 to High(mColorIndex) do begin
    if mColorIndex[i] = xIndex then begin
      TargetCanvas.Brush.Color := $00EEC19D;
      TargetCanvas.FillRect(CellRect);
      Break;
    end;// if mColorIndex[i] = xIndex then begin
  end;// for i := 0 to High(mColorIndex) do begin
end;

procedure TfrmHexViewerFrame.setColorIndex(aIntArray: TIntArray);
var
  i: Integer;
begin
  SetLength(mColorIndex, High(aIntArray) + 1);
  for i := 0 to High(aIntArray) do
    mColorIndex[i] := aIntArray[i];
  vstBinGrid.Invalidate;
end;

end.
