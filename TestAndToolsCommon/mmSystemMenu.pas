unit mmSystemMenu;

interface

uses
  StandardSystemMenu, Classes;

type
  TMMStandardSystemMenu = class(TStandardSystemMenu)
  public

    constructor Create(aOwner: TComponent; aExecute: boolean); reintroduce; virtual;
    procedure Execute;
  end;

implementation

constructor TMMStandardSystemMenu.Create(aOwner: TComponent; aExecute: boolean);
begin
  inherited Create(aOwner);
  if aExecute then
    Execute;
end;

procedure TMMStandardSystemMenu.Execute;
begin
  Loaded;
end;

end.
