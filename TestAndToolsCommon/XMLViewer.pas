unit XMLViewer;

(* Anwendung:

        unit Unit1;

        interface

        uses
          Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, XMLViewer,
          StdCtrls, Buttons, mmBitBtn, mmMemo;

        type
          TForm1 = class(TForm)
            mCurrentText: TmmMemo;
            bAdd: TmmBitBtn;
            bGet: TmmBitBtn;
            procedure FormCreate(Sender: TObject);
            procedure bAddClick(Sender: TObject);
            procedure bGetClick(Sender: TObject);
          private
            mXMLViewer: TfrmXMLViewer;
            { Private declarations }
          public
            { Public declarations }
          end;

        var
          Form1: TForm1;

        implementation

        {$R *.DFM}

        procedure TForm1.FormCreate(Sender: TObject);
        begin
          mXMLViewer := TfrmXMLViewer.Create(Application);
          mXMLViewer.Show;
        end;

        procedure TForm1.bAddClick(Sender: TObject);
        begin
          mXMLViewer.AddText(mCurrentText.Text, Format('NewText(%d)', [mXMLViewer.BufferCount]));
        end;

        procedure TForm1.bGetClick(Sender: TObject);
        begin
          mCurrentText.Text := mXMLViewer.Text;
        end;

        end.

*)
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, SynEdit,
  ActnList, ComCtrls, ImgList, mmImageList, mmActionList, ToolWin,
  mmToolBar, IvMlDlgs, mmOpenDialog, StdCtrls, mmStaticText, ExtCtrls,
  mmImage, mmSplitter, mmMemo, mmLabel, mmPanel, mmButton, mmCheckBox, XMLGlobal,
  Menus, mmPopupMenu, VirtualTrees, mmVirtualStringTree, MSXML2_TLB,
  ConfigurationPersistence, mmTabControl, SynEditPrint, Buttons,
  mmSpeedButton, mmSaveDialog, mmPrintDialog;

type
  TBuffer = class;
  TBuffers = class;
  TfrmXMLViewer = class(TForm)
    mmToolBar1: TmmToolBar;
    mmActionList1: TmmActionList;
    mmImageList1: TmmImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    acLoadXML: TAction;
    acLoadXMLFromDB: TAction;
    mXMLOpenDialog: TmmOpenDialog;
    paXP: TmmPanel;
    paXPHeader: TmmPanel;
    paXPRoot: TmmPanel;
    paXPResult: TmmPanel;
    paXPInput: TmmPanel;
    mMethodNamePanel: TmmPanel;
    mMinusImage: TmmImage;
    bExpand: TmmImage;
    mPlusImage: TmmImage;
    laMethodName: TmmStaticText;
    paXMLRoot: TmmPanel;
    mXPResultSplitter: TmmSplitter;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmLabel1: TmmLabel;
    memoXP: TmmMemo;
    cbExecuteOnChange: TmmCheckBox;
    mmButton1: TmmButton;
    acExecuteXP: TAction;
    mDescriptionPanel: TmmPanel;
    mmPanel8: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmMemo;
    mmLabel2: TmmLabel;
    pmOpenFileMRU: TmmPopupMenu;
    paXMLResultLeft: TmmPanel;
    paXMLResultRight: TmmPanel;
    acNewTab: TAction;
    ToolButton3: TToolButton;
    pmEditor: TmmPopupMenu;
    acCloseFile: TAction;
    Schliessen1: TMenuItem;
    acPrint: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    acFormatXML: TAction;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    acRenameTab: TAction;
    Umbenennen1: TMenuItem;
    acReload: TAction;
    ToolButton9: TToolButton;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmPanel5: TmmPanel;
    mBufferTab: TmmTabControl;
    mmSpeedButton1: TmmSpeedButton;
    acSaveFile: TAction;
    ToolButton10: TToolButton;
    mmSplitter1: TmmSplitter;
    mSaveDialog: TmmSaveDialog;
    pmSaveMRU: TmmPopupMenu;
    ToolButton11: TToolButton;
    acSaveDB: TAction;
    mPrintDialog: TmmPrintDialog;
    acSearch: TAction;
    b1: TToolButton;
    acSearchNext: TAction;
    N1: TMenuItem;
    Finden1: TMenuItem;
    FindNext1: TMenuItem;
    N2: TMenuItem;
    Drucken1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure acLoadXMLExecute(Sender: TObject);
    procedure acLoadXMLFromDBExecute(Sender: TObject);
    procedure bExpandClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure mXPResultSplitterMoved(Sender: TObject);
    procedure acExecuteXPExecute(Sender: TObject);
    procedure memoXPChange(Sender: TObject);
    procedure pmOpenFileMRUPopup(Sender: TObject);
    procedure mBufferTabChange(Sender: TObject);
    procedure acNewTabExecute(Sender: TObject);
    procedure acCloseFileExecute(Sender: TObject);
    procedure memoXMLChange(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure acPrintExecute(Sender: TObject);
    procedure acFormatXMLExecute(Sender: TObject);
    procedure acRenameTabExecute(Sender: TObject);
    procedure acReloadExecute(Sender: TObject);
    procedure acSaveFileExecute(Sender: TObject);
    procedure acSaveDBExecute(Sender: TObject);
    procedure acSearchExecute(Sender: TObject);
    procedure acSearchNextExecute(Sender: TObject);
  private
    mMSDOM: DOMDocumentMM;
    FSettings: TBaseXMLSettings;
    FBuffers: TBuffers;
    memoCurrentBuffer: TSynEdit;
    memoXPResult: TSynEdit;
    mLastXPHeight: Integer;
    mTextToFind: string;
    // Aufruf nur �ber AccessInstance()
    procedure EnableDisableSave;
    function GetBufferCount: Integer;
    function GetBufferIndex: Integer;
    function GetText: string;
    procedure SetXMLAsString(const Value: string);
    procedure LoadDocumentHandler(Sender: TObject);
    procedure SaveDocumentHandler(Sender: TObject);
    procedure SetText(const Value: string);
    procedure SaveXMLFile(aFileName: string);
    procedure SetBufferIndex(const aValue: Integer);
    { Private declarations }
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function AddText(aText:string; aName: string; aFilename: string = ''): Integer;
    procedure BufferChanged(Sender: TObject);
    procedure CangeMemoMapfile(Sender: TObject);
    procedure Clear;
    procedure CollapseXP;
    procedure DropFiles(Sender: TObject; X, Y: integer; AFiles: TStrings);
    procedure ExpandXP;
    procedure LoadXMLFile(aFileName: string);
    procedure RefreshXPath(aIfAutoModeOnly: boolean);
    procedure ShowNewMapfile(aXMLAsString: string; aTitle: string);
    property BufferCount: Integer read GetBufferCount;
    property BufferIndex: Integer read GetBufferIndex write SetBufferIndex;
    property Buffers: TBuffers read FBuffers write FBuffers;
    property XMLAsString: string write SetXMLAsString;
    property Settings: TBaseXMLSettings read FSettings write FSettings;
    property Text: string read GetText write SetText;
    { Public declarations }
  end;

  TBuffer = class(TPersistent)
  private
    FFilename: string;
    FName: string;
    FLines: TStrings;
    FXCursorPos: Integer;
    FYCursorPos: Integer;
    function GetText: string;
    procedure SetText(const Value: string);
  public
    constructor Create; virtual;
    destructor Destroy; override;
    property Filename: string read FFilename write FFilename;
    property Name: string read FName write FName;
    property Lines: TStrings read FLines write FLines;
    property Text: string read GetText write SetText;
    property XCursorPos: Integer read FXCursorPos write FXCursorPos;
    property YCursorPos: Integer read FYCursorPos write FYCursorPos;
  end;

  TBuffers = class(TPersistent)
  private
    FCurrentIndex: Integer;
    FOnBufferChanged: TNotifyEvent;
    function GetBufferNames: string;
    function GetCurrentName: string;
    function GetCurrentText: string;
    procedure SetCurrentIndex(const Value: Integer);
    procedure SetCurrentName(const Value: string);
    procedure SetCurrentText(const Value: string);
  protected
    FOnDeleteItem: TNotifyEvent;
    mBuffers: TList;
    procedure DoBufferChanged;
    function GetCount: Integer;
    function GetItems(aIndex: Integer): TBuffer;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Add(aItem: TBuffer): Integer; virtual;
    procedure AddNewText(aText: string; aName: string = 'Neu');
    procedure Assign(Source: TPersistent); override;
    function ChangeIndex(aOldCursorPos: TPoint; aNewIndex: integer): TPoint;
    procedure Clear;
    procedure Delete(aIndex: integer); virtual;
    function IndexOf(aItem: TBuffer): Integer; virtual;
    property BufferNames: string read GetBufferNames;
    property Count: Integer read GetCount;
    property CurrentIndex: Integer read FCurrentIndex write SetCurrentIndex;
    property CurrentName: string read GetCurrentName write SetCurrentName;
    property CurrentText: string read GetCurrentText write SetCurrentText;
    property Items[aIndex: Integer]: TBuffer read GetItems; default;
    property OnDeleteItem: TNotifyEvent read FOnDeleteItem write FOnDeleteItem;
  published
    property OnBufferChanged: TNotifyEvent read FOnBufferChanged write FOnBufferChanged;
  end;

const
  cAtrXMLViewerVisible = 'XMLViewerVisible';
  cAtrHEXViewerVisible = 'HEXViewerVisible';

implementation

uses
  SynHighlighterXML, mmcs, LoepfeGlobal, LoadFromDB, comobj, winsock;

{$R *.DFM}
const
  cRecentFilesXMLRootElement = 'RecentFilesMRU_XML';
    cXPRecentFilesXML        = cXPDocumentBody + '/' + cRecentFilesXMLRootElement;

(*---------------------------------------------------------
  F�hrt den XPath neu aus.
    Wenn aIfAutoModeOnly = true, dann nur wenn die Checkbox "cbExecuteOnChange"
    Checked ist.
----------------------------------------------------------*)
procedure TfrmXMLViewer.RefreshXPath(aIfAutoModeOnly: boolean);
begin
  if memoXP.Text > '' then begin
    if (aIfAutoModeOnly and cbExecuteOnChange.Checked) or not(aIfAutoModeOnly) then
      acExecuteXP.Execute;
  end;// if memoXP.Text > '' then begin
end;// procedure TfrmXMLViewer.RefreshXPath(aIfAutoModeOnly: boolean);

constructor TfrmXMLViewer.Create(aOwner: TComponent);
begin
  inherited;
  fBuffers := TBuffers.Create;
  fBuffers.OnBufferChanged := BufferChanged;
end;

destructor TfrmXMLViewer.Destroy;
begin
//  FreeAndNil(mSystemMenu);
  FreeAndNil(fBuffers);
  inherited;
end;

procedure TfrmXMLViewer.FormCreate(Sender: TObject);
var
  xHighlighter: TSynXMLSyn;
begin
  // Erm�glicht es dem Benutzer 'MMSupp' XML Dateien als File, oder in die DB zu speichern.
  EnableDisableSave;

  memoCurrentBuffer                        := TSynEdit.Create(self);
  memoCurrentBuffer.MaxLeftChar            := MAX_SCROLL;
  memoCurrentBuffer.Parent                 := paXMLRoot;
  memoCurrentBuffer.Align                  := alClient;
  memoCurrentBuffer.TabWidth               := 2;
  memoCurrentBuffer.OnChange               := CangeMemoMapfile;
  memoCurrentBuffer.OnDropFiles            := DropFiles;
  memoCurrentBuffer.Options                := memoCurrentBuffer.Options + [eoDropFiles];
  memoCurrentBuffer.Gutter.ShowLineNumbers := true;
  memoCurrentBuffer.Gutter.Font.Name       := 'Courier New';
  memoCurrentBuffer.PopupMenu              := pmEditor;

  memoXPResult          := TSynEdit.Create(self);
  memoXPResult.Parent   := paXMLResultLeft;
  memoXPResult.Align    := alClient;
  memoXPResult.TabWidth := 2;

  xHighlighter                           := TSynXMLSyn.Create(memoCurrentBuffer);
  xHighlighter.AttributeAttri.Foreground := clRed;
  xHighlighter.ElementAttri.Style        := [fsItalic];

  memoCurrentBuffer.Highlighter          := xHighlighter;
  memoXPResult.Highlighter               := xHighlighter;

  CollapseXP;

  if assigned(fBuffers) then
    fBuffers.AddNewText('', 'Neu');
end;

procedure TfrmXMLViewer.SetXMLAsString(const Value: string);
begin
  Text := FormatXML(Value);
end;

procedure TfrmXMLViewer.ShowNewMapfile(aXMLAsString: string; aTitle: string);
begin
  XMLAsString := aXMLAsString;
  Caption:= aTitle;
end;

procedure TfrmXMLViewer.acLoadXMLExecute(Sender: TObject);
begin
  with mXMLOpenDialog do begin
    InitialDir := ExtractFilePath(Application.ExeName) + '\Mapfiles';
    if Execute then
      LoadXMLFile(FileName);
  end;// with mXMLOpenDialog do begin
end;

procedure TfrmXMLViewer.LoadXMLFile(aFileName: string);
begin
  if FileExists(aFileName) then begin
    with TStringList.Create do try
      LoadFromFile(aFileName);
      if assigned(Settings) then
        Settings.SaveRecentFiles(aFileName, false, Settings.GetElement(cXPRecentFilesXML));
      AddText(Text, ExtractFileName(aFileName), aFilename);
    finally
      free;
    end;// with TStringList.Create do try
  end;// if FileExists(aFileName) then begin
end;

procedure TfrmXMLViewer.acSaveFileExecute(Sender: TObject);
begin
  with mSaveDialog do begin
    InitialDir := ExtractFilePath(Application.ExeName) + '\Mapfiles';
    Filename := '';
    if assigned(fBuffers) and (fBuffers.Count > 0) and (fBuffers.Items[fBuffers.CurrentIndex].Filename > '')then begin
      InitialDir := ExtractFileDir(fBuffers.Items[fBuffers.CurrentIndex].Filename);
      Filename := fBuffers.Items[fBuffers.CurrentIndex].Filename;
    end;
    if Execute then
      SaveXMLFile(FileName);
  end;// with mXMLOpenDialog do begin
end;

procedure TfrmXMLViewer.SaveXMLFile(aFileName: string);
begin
  with TStringList.Create do try
    Text := fBuffers.CurrentText;
    SaveToFile(aFilename);
    fBuffers.CurrentName := ExtractFileName(ChangeFileExt(aFilename, ''));
    fBuffers.Items[fBuffers.CurrentIndex].Filename := aFilename;
    if assigned(Settings) then
      Settings.SaveRecentFiles(aFileName, false, Settings.GetElement(cXPRecentFilesXML));
  finally
    free;
  end;// with TStringList.Create do try
end;

procedure TfrmXMLViewer.acLoadXMLFromDBExecute(Sender: TObject);
begin
  with TfrmLoadFromDB.Create(nil) do try
    Settings := self.Settings;
    DialogMode := dmLoad;
    ShowModal;
    if XMLStream > '' then
      AddText(XMLStream, XMLName);
  finally
    free;
  end;// with TfrmLoadFromDB.Create(nil) do try
end;

procedure TfrmXMLViewer.bExpandClick(Sender: TObject);
begin
  if paXPRoot.visible then
    CollapseXP
  else
    ExpandXP;
end;

procedure TfrmXMLViewer.CollapseXP;
begin
  mLastXPHeight := paXP.Height;
  paXP.Height := paXPHeader.Height;
  bExpand.Picture := mPlusImage.Picture;
  paXPRoot.Visible := false;
  mXPResultSplitter.Visible := false;
end;

procedure TfrmXMLViewer.ExpandXP;
begin
  paXP.Height := mLastXPHeight;
  bExpand.Picture := mMinusImage.Picture;
  paXPRoot.Visible := true;
  mXPResultSplitter.Visible := true;
end;

const
  cLastXPHeight = 'LastXPHeight';
  cXPExpanded   = 'XPExpanded';
  cExecuteXPOnChange = 'ExecuteXPOnChange';
  
procedure TfrmXMLViewer.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  xFormElement: IXMLDOMElement;
begin
  if assigned(Settings) then begin
    with Settings do begin
      xFormElement := SaveForm(self, self.Name);
      if assigned(xFormElement) then begin
        xFormElement.setAttribute(cLastXPHeight, mLastXPHeight);
        xFormElement.setAttribute(cXPExpanded, paXPRoot.visible);
        xFormElement.setAttribute(cExecuteXPOnChange, cbExecuteOnChange.Checked);
      end;
    end;//
  end;// if assigned(Settings) then begin
end;

procedure TfrmXMLViewer.FormShow(Sender: TObject);
var
  xFormElement: IXMLDOMElement;
begin
  if assigned(Settings) then begin
    with Settings do begin
      xFormElement := LoadForm(self, self.Name);
      if assigned(xFormElement) then begin
        mLastXPHeight := AttributeToIntDef(cLastXPHeight, xFormElement, mLastXPHeight);
        paXP.Height := mLastXPHeight;
        cbExecuteOnChange.Checked := AttributeToBoolDef(cExecuteXPOnChange, xFormElement, false);

        if AttributeToBoolDef(cXPExpanded, xFormElement, false) then
          ExpandXP
        else
          CollapseXP;
      end;
    end;//
  end;// if assigned(Settings) then begin
end;

procedure TfrmXMLViewer.mXPResultSplitterMoved(Sender: TObject);
begin
  mLastXPHeight := paXP.Height;
end;

procedure TfrmXMLViewer.acExecuteXPExecute(Sender: TObject);
begin
  if not(assigned(mMSDOM)) then begin
    mMSDOM := DOMDocumentMMCreate;
    mMSDOM.setProperty('NewParser', true);
    mMSDOM.ValidateOnParse := false;
    mMSDOM.loadxml(memoCurrentBuffer.Text);
    mMSDOM.setProperty('SelectionNamespaces', cSchemaNameSpace + ' ' + cLoepfeNameSpace);
  end;// if not(assigned(mMSDOM)) then begin

  try
    if memoXP.SelLength > 0 then
      memoXPResult.Text := FormatXML(mMSDOM.SelectNodes(StringReplace(memoXP.SelText, cCrlf, ' ', [rfReplaceAll])))
    else
      memoXPResult.Text := FormatXML(mMSDOM.SelectNodes(StringReplace(memoXP.Text, cCrlf, ' ', [rfReplaceAll])));
  except
    on e: Exception do begin
      memoXPResult.Text := e.message;
    end;// on e:EOleException do begin
  end;// try except

end;

function TfrmXMLViewer.AddText(aText:string; aName: string; aFilename: string = ''): Integer;
begin
  if aText > '' then
    fBuffers.AddNewText(FormatXML(aText))
  else
    fBuffers.AddNewText('');

  fBuffers.CurrentName := aName;
  fBuffers[fBuffers.CurrentIndex].Filename := aFileName;
  Result := fBuffers.CurrentIndex;
end;

procedure TfrmXMLViewer.BufferChanged(Sender: TObject);
begin
  mMSDOM := nil;
  if assigned(fBuffers) then begin
    mBufferTab.Tabs.Commatext := fBuffers.BufferNames;
    mBufferTab.TabIndex := fBuffers.CurrentIndex;
    memoCurrentBuffer.Text := fBuffers.CurrentText;
    Caption := fBuffers.CurrentName;
  end;// if assigned(mBuffers) then begin
end;

procedure TfrmXMLViewer.CangeMemoMapfile(Sender: TObject);
begin
  mMSDOM := nil;
  if assigned(fBuffers) then
    fBuffers[fBuffers.CurrentIndex].Text := memoCurrentBuffer.Text;
end;

procedure TfrmXMLViewer.Clear;
begin
  memoCurrentBuffer.Text := '';
end;

function TfrmXMLViewer.GetText: string;
begin
  Result := fBuffers.CurrentText;
end;

procedure TfrmXMLViewer.memoXPChange(Sender: TObject);
begin
  if cbExecuteOnChange.Checked then
    acExecuteXP.Execute;
end;

(* -----------------------------------------------------------
  Handler f�r das Popupmenu mit den letzten geladenen Files
-------------------------------------------------------------- *)
procedure TfrmXMLViewer.LoadDocumentHandler(Sender: TObject);
begin
  LoadXMLFile(copyRight((Sender as TMenuItem).Caption, ' '));
end;// procedure TfrmMainWindow.LoadDocumentHandler(Sender: TObject);

(* -----------------------------------------------------------
  Handler f�r das Popupmenu mit den letzten geladenen Files
-------------------------------------------------------------- *)
procedure TfrmXMLViewer.SaveDocumentHandler(Sender: TObject);
begin
  SaveXMLFile(copyRight((Sender as TMenuItem).Caption, ' '));
end;// procedure TfrmMainWindow.LoadDocumentHandler(Sender: TObject);

(*---------------------------------------------------------
  Recent Files
----------------------------------------------------------*)
procedure TfrmXMLViewer.pmOpenFileMRUPopup(Sender: TObject);
var
  i: Integer;
  xFileList: IXMLDOMNodeList;
  xNewMenuItem: TMenuItem;
begin
  if Sender is TPopupMenu then begin
    TPopupMenu(Sender).Items.Clear;
    if assigned(Settings) then begin
      xFileList := Settings.GetElement(cXPRecentFilesXML).selectNodes(cRecentFilesElement);
      for i := 0 to xFileList.length - 1 do begin
        xNewMenuItem := TMenuItem.Create(TPopupMenu(Sender));
        xNewMenuItem.Caption := AttributeToStringDef(cAtrRecentFilesName, xFileList.Item[i], '');
        if Sender = pmSaveMRU then
          xNewMenuItem.OnClick := SaveDocumentHandler
        else
          xNewMenuItem.OnClick := LoadDocumentHandler;

        if Caption <> '' then begin
          xNewMenuItem.Caption := '&' + IntToStr(i + 1) + ' ' + xNewMenuItem.Caption;
          TPopupMenu(Sender).Items.Add(xNewMenuItem)
        end else
          FreeAndNil(xNewMenuItem);
      end;// for i := 0 to xFileList.length - 1 do begin
    end;// if assigned(Settings) then begin
  end;// if Sender is TPopupMenu then begin
end;// procedure TfrmXMLViewer.pmOpenFileMRUPopup(Sender: TObject);

procedure TfrmXMLViewer.SetText(const Value: string);
begin
  fBuffers.CurrentText := Value;
end;

constructor TBuffers.Create;
begin
  inherited;
  mBuffers := TList.Create;
end;

destructor TBuffers.Destroy;
begin
  Clear;
  FreeAndNil(mBuffers);
  inherited;
end;

function TBuffers.Add(aItem: TBuffer): Integer;
begin
  result := -1;
  if assigned(mBuffers) then begin
    result := mBuffers.Add(aItem);
    FCurrentIndex := result;
    DoBufferChanged;
  end;
end;

procedure TBuffers.AddNewText(aText: string; aName: string = 'Neu');
var
  xBuffer: TBuffer;
begin
  xBuffer := TBuffer.Create;
  with xBuffer do begin
    Text := aText;
    Name := aName;
  end;// xBuffer := TBuffer.Create;
  Add(xBuffer);
end;

procedure TBuffers.Assign(Source: TPersistent);
var
  xSource: TBuffers;
  xItem: TBuffer;
  i: Integer;
begin
  if Source is TBuffers then begin
    xSource := TBuffers(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.Count - 1 do begin
      xItem := TBuffer.Create;
      xItem.Assign(xSource.Items[i]);
      Add(xItem);
    end;// for i := 0 to xSource.Count - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TBufferList then begin
end;

function TBuffers.ChangeIndex(aOldCursorPos: TPoint; aNewIndex: integer): TPoint;
begin
  Items[CurrentIndex].XCursorPos := aOldCursorPos.x;
  Items[CurrentIndex].YCursorPos := aOldCursorPos.y;
  CurrentIndex := aNewIndex;
  result.x := Items[CurrentIndex].XCursorPos;
  result.y := Items[CurrentIndex].YCursorPos;
end;

procedure TBuffers.Clear;
begin
  while mBuffers.Count > 0 do
    delete(0);
end;

procedure TBuffers.Delete(aIndex: integer);
begin
  if assigned(mBuffers) then begin
    if aIndex < mBuffers.Count then begin
      if assigned(FOnDeleteItem) then
        FOnDeleteItem(Items[aIndex]);
      if aIndex <= FCurrentIndex then begin
        CurrentIndex := CurrentIndex - 1;
        DoBufferChanged;
      end;
      Items[aIndex].Free;
      mBuffers.delete(aIndex);
    end;// if aIndex < mBuffers.Count then begin
  end;// if assigned(mBuffers) then begin
end;

procedure TBuffers.DoBufferChanged;
begin
  if Assigned(FOnBufferChanged) then FOnBufferChanged(Self);
end;

function TBuffers.GetBufferNames: string;
var
  i: Integer;
begin
  Result := '';
  with TStringList.Create do try
    for i := 0 to self.Count - 1 do
      Add(Items[i].Name);
    result := Commatext;
  finally
    free;
  end;// with TStringList.Create do try
end;

function TBuffers.GetCount: Integer;
begin
  result := 0;

  if assigned(mBuffers) then
    result := mBuffers.Count;
end;

function TBuffers.GetCurrentName: string;
begin
  Result := '';
  if (FCurrentIndex >= 0) and (FCurrentIndex < Count) then
    result := Items[FCurrentIndex].Name;
end;

function TBuffers.GetCurrentText: string;
begin
  Result := '';
  if (FCurrentIndex >= 0) and (FCurrentIndex < Count) then
    result := Items[FCurrentIndex].Text;
end;

function TBuffers.GetItems(aIndex: Integer): TBuffer;
begin
  result := nil;
  if assigned(mBuffers) then
    if mBuffers.Count > aIndex then
      result := TBuffer(mBuffers.Items[aIndex]);
end;

function TBuffers.IndexOf(aItem: TBuffer): Integer;
var
  i: integer;
begin
  result := -1;

  i := 0;
  while (i < mBuffers.Count) and (result = -1) do begin
    if mBuffers[i] = aItem then
      result := i;
    inc(i);
  end;// while (i < mBuffers.Count) and (result = -1) do begin
end;

procedure TBuffers.SetCurrentIndex(const Value: Integer);
var
  xOldIndex: integer;
begin
  xOldIndex := FCurrentIndex;
  if (Value > 0 ) and (Value < Count) then begin
    FCurrentIndex := Value;
  end else begin
    if Value > 0 then
      FCurrentIndex := Count - 1
    else
      FCurrentIndex := 0;
  end;
  if xOldIndex <> FCurrentIndex then
    DoBufferChanged;
end;

procedure TBuffers.SetCurrentName(const Value: string);
begin
  if (FCurrentIndex >= 0) and (FCurrentIndex < Count) then begin
   Items[FCurrentIndex].Name := Value;
   DoBufferChanged;
  end;
end;

procedure TBuffers.SetCurrentText(const Value: string);
begin
  if (FCurrentIndex >= 0) and (FCurrentIndex < Count) then begin
    Items[FCurrentIndex].Text := Value;
    DoBufferChanged;
  end else begin
    AddNewText(Value);
  end;
end;

constructor TBuffer.Create;
begin
  inherited;
  FLines := TStringList.Create;
end;

destructor TBuffer.Destroy;
begin
  FreeAndNil(FLines);
  inherited;
end;

function TBuffer.GetText: string;
begin
  Result := Lines.Text;
end;

procedure TBuffer.SetText(const Value: string);
begin
  Lines.Text := Value;
end;

(* -----------------------------------------------------------
  Aufruf nur �ber AccessInstance()
-------------------------------------------------------------- *)  
procedure TfrmXMLViewer.EnableDisableSave;
var
  i: Integer;
  xSaveEnabled: Boolean;

  (*---------------------------------------------------------
    Die Funktion "GetLocalIPs" ermittelt alle aktuellen IP-Adressen im System.
    Die einzelnen Adressen sind durch Zeilenwechsel getrennt
  ----------------------------------------------------------*)
  function GetLocalIPs: String;
  type
    PPInAddr= ^PInAddr;
  var
    wsaData  : TWSAData;
    HostInfo : PHostEnt;
    HostName : Array[0..255] of Char;
    Addr     : PPInAddr;
  begin
    Result:='';
    if WSAStartup(MakeWord(1, 1), wsaData) <> 0 then
      Exit;
    try
      if GetHostName(HostName, SizeOf(HostName)) <> 0 then
        Exit;
      HostInfo:= GetHostByName(HostName);
      if HostInfo=nil then
        Exit;
      Addr:=Pointer(HostInfo^.h_addr_list);
      if (Addr=nil) or (Addr^=nil) then
        Exit;
      Result:=StrPas(inet_ntoa(Addr^^));
      inc(Addr);
      while Addr^ <> nil do begin
        Result:=Result+^M^J+StrPas(inet_ntoa(Addr^^));
        inc(Addr);
      end;
    finally
      WSACleanup;
    end;
  end;
begin
  xSaveEnabled := (AnsiSameText(GetCurrentMMUser, 'MMSupp'));
  if not(xSaveEnabled) then begin
    with TStringList.Create do try
      Text := GetLocalIPs;
      i := 0;
      while (i < Count) and (not(xSaveEnabled)) do begin
        xSaveEnabled := copy(Strings[i], 1, 3) = '150';
        inc(i);
      end;
    finally
      free;
    end;// with TStringList.Create do try
  end;// if not(xSaveEnabled) then begin

  acSaveDB.Enabled := xSaveEnabled;
  acSaveFile.Enabled := xSaveEnabled;
end;// constructor TXMLViewerSettings.CreateInstance;

function TfrmXMLViewer.GetBufferCount: Integer;
begin
  Result := fBuffers.Count;
end;

procedure TfrmXMLViewer.mBufferTabChange(Sender: TObject);
begin
  memoCurrentBuffer.CaretXY := fBuffers.ChangeIndex(memoCurrentBuffer.CaretXY, mBufferTab.TabIndex);
end;

procedure TfrmXMLViewer.acNewTabExecute(Sender: TObject);
var
  aNewName: string;
begin
  aNewName := 'Neu';
  if assigned(fBuffers) then begin
    InputQuery('Tab Name', 'Name der neuen Lasche eingeben', aNewName);
    fBuffers.AddNewText('', aNewName);
  end;
end;

procedure TfrmXMLViewer.acCloseFileExecute(Sender: TObject);
begin
  if assigned(fBuffers) then
    fBuffers.Delete(fBuffers.CurrentIndex);
  BufferChanged(Sender);
  if fBuffers.Count = 0 then
    fBuffers.AddNewText('', 'Neu');
end;

procedure TfrmXMLViewer.memoXMLChange(Sender: TObject);
begin
  if assigned(fBuffers) then
    fBuffers[fBuffers.CurrentIndex].Text := memoCurrentBuffer.Text;
end;

procedure TfrmXMLViewer.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  mBufferTab.Visible := (assigned(fBuffers)) and (fBuffers.Count > 0);
  if fBuffers.Count > 0 then
    acReload.Enabled := (fBuffers[fBuffers.CurrentIndex].Filename > '') and (FileExists(fBuffers[fBuffers.CurrentIndex].Filename));
end;

procedure TfrmXMLViewer.acPrintExecute(Sender: TObject);
begin
  with TSynEditPrint.Create(nil) do try
    SynEdit     := memoCurrentBuffer;
    Colors      := true;
    DocTitle    := fBuffers.CurrentName;
    Highlight   := true;
    Highlighter := memoCurrentBuffer.Highlighter;
    Wrap        := false;
    if mPrintDialog.Execute then
      Print;
 finally
    Free;
  end;// with TSynEditPrint.Create(nil) do try
end;

procedure TfrmXMLViewer.acFormatXMLExecute(Sender: TObject);
var
  xDOM: DOMDocumentMM;
begin
  try
    xDOM := XMLStreamToDOM(memoCurrentBuffer.Text);
    // Wenn ohne Fehler geparst werden konnte, dann den Text Formatieren
    memoCurrentBuffer.Text := FormatXML(memoCurrentBuffer.Text);
  except
    on e: Exception do begin
      ShowMessage('Fehler: ' + cCrlf + cCrlf + e.Message);
      if assigned(xDOM) and assigned(xDOM.ParseError) then
        memoCurrentBuffer.CaretXY := Point(xDOM.parseError.linepos, xDOM.parseError.line);
    end;
  end;
  xDOM := nil;
end;

procedure TfrmXMLViewer.acRenameTabExecute(Sender: TObject);
var
  xName: string;
begin
  xName := fBuffers[fBuffers.CurrentIndex].Name;
  if InputQuery('Buffer Name', 'Neuen Namen eingeben', xName) then begin
    fBuffers[fBuffers.CurrentIndex].Name := xName;
    mBufferTab.Tabs[mBufferTab.Tabindex] := xName;
  end;
end;

procedure TfrmXMLViewer.acReloadExecute(Sender: TObject);
var
  xBuffer: TBuffer;
begin
  xBuffer := fBuffers[fBuffers.CurrentIndex];
  if (FileExists(xBuffer.Filename)) then begin
    memoCurrentBuffer.Lines.LoadFromFile(xBuffer.Filename);
    acFormatXML.Execute;
  end;
end;

procedure TfrmXMLViewer.DropFiles(Sender: TObject; X, Y: integer; AFiles: TStrings);
var
  i: Integer;
begin
  for i := 0 to aFiles.Count - 1 do 
    LoadXMLFile(aFiles[i]);
end;

procedure TfrmXMLViewer.acSaveDBExecute(Sender: TObject);
begin
  if fBuffers.CurrentText > '' then begin
    with TfrmLoadFromDB.Create(nil) do try
      Settings := self.Settings;
      XMLStream := fBuffers.CurrentText;
      DialogMode := dmSave;
      ShowModal;
    finally
      free;
    end;// with TfrmLoadFromDB.Create(nil) do try
  end else begin
    MessageBox(0, 'Kein Text angegeben.'+#13+#10+'Der Dialog wird nicht angezeigt', 'In DB speichern', MB_ICONINFORMATION or MB_OK);
  end;
end;

procedure TfrmXMLViewer.acSearchExecute(Sender: TObject);
begin
  with memoCurrentBuffer do begin
    if WordatCursor > '' then
      mTextToFind := WordAtCursor;
    mTextToFind := InputBox('XML Viewer', 'Suchtext', mTextToFind);
    SearchReplace(mTextToFind, '', []);
  end;// with memoCurrentBuffer do begin
end;

procedure TfrmXMLViewer.acSearchNextExecute(Sender: TObject);
begin
  memoCurrentBuffer.SearchReplace(mTextToFind, '', []);
end;

function TfrmXMLViewer.GetBufferIndex: Integer;
begin
  Result := 0;
  if Assigned(fBuffers) then
    Result := fBuffers.CurrentIndex;
end;

procedure TfrmXMLViewer.SetBufferIndex(const aValue: Integer);
begin
  if Assigned(fBuffers) then begin
    if aValue <= fBuffers.Count then
      mBufferTab.TabIndex := aValue;
      mBufferTabChange(mBufferTab);
  end;//// if Assigned(mBuffers) then begin
end;

end.
