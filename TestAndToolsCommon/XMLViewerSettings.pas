unit XMLViewerSettings;

interface

uses
  ConfigurationPersistence, MSXML2_TLB;

type
  TXMLViewerSettings = class(TBaseXMLSettings)
  private
    FSaveInDBEnabled: Boolean;
  protected
  public
    destructor Destroy; override;
    function GetSettingsFilePath: string; override;
    class function Instance: TXMLViewerSettings;
    property SaveInDBEnabled: Boolean read FSaveInDBEnabled;
  end;

implementation

uses
  XMLGlobal, sysutils, LoepfeGlobal, mmcs, winsock, windows, classes;

(*---------------------------------------------------------
  Destructor
----------------------------------------------------------*)
destructor TXMLViewerSettings.Destroy;
begin
  SaveSettings;
  inherited Destroy;
end;// destructor TXMLViewerSettings.Destroy;

(*---------------------------------------------------------
   Gibt dei Instanz des Singelton zur�ck
----------------------------------------------------------*)
class function TXMLViewerSettings.Instance: TXMLViewerSettings;
begin
  Result := TXMLViewerSettings(AccessInstance(1));
end;// class function TXMLViewerSettings.Instance: TXMLViewerSettings;

(*---------------------------------------------------------
  Gibt den Name der Settings Datei zur�ck
----------------------------------------------------------*)
function TXMLViewerSettings.GetSettingsFilePath: string;
begin
  result := GetUserSettingsDirectory + 'XMLViewer.xml';
end;// function TXMLViewerSettings.GetSettingsFilePath: string;

initialization
finalization
  // Singleton freigeben und Settings speichern
  TXMLViewerSettings.ReleaseInstance;

end.
