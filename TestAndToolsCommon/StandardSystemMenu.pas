{-----------------------------------------------------------------------------
 Unit Name: StandardSystemMenu
 Author:    Collin Willson (http://www.wilsonc.demon.co.uk/delphi_5.htm)
 Date:      14-Mrz-2005
 Purpose:
 History:
-----------------------------------------------------------------------------}

unit StandardSystemMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Menus;

const
  scxRESTORE  = $fff0;
  scxMINIMIZE = $fff2;
  scxMAXIMIZE = $fff3;
  scxSIZE     = $fff4;
  scxMOVE     = $fff5;

type
  TStandardSystemMenu = class(TComponent)
  private
    fMenuHandle : HMenu;
    fWindowMenuHandle : HMenu;
    fObjectInstance : pointer;
    fOldOwnerWindowProc : TFNWndProc;

    fSysObjectInstance : pointer;
    fOldSysWindowProc : TFNWndProc;

    fIconic : boolean;
    fMaximized : boolean;

    procedure CloneSystemMenu;
    procedure OwnerWindowProc(var msg: TMessage);
    procedure SysOwnerWindowProc(var msg: TMessage);

    procedure OnMinimized;
    procedure OnMaximized;
    procedure OnRestored (resetmax : boolean);

  protected
    { Protected declarations }
    procedure Loaded; override;
  public
    procedure SetItemState (itemID, state : Integer);
    destructor Destroy; override;
    { Public declarations }
  published
    { Published declarations }
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Misc Units', [TStandardSystemMenu]);
end;

{ TStandardSystemMenu }

procedure TStandardSystemMenu.CloneSystemMenu;
var
  count : Integer;
  item : TMenuItemInfo;
  buffer : array [0..256] of char;
  i : Integer;
begin
  count := GetMenuItemCount (fMenuHandle);
  while count > 0 do
  begin
    DeleteMenu (fMenuHandle, 0, MF_BYPOSITION);
    Dec (count)
  end;

  count := GetMenuItemCount (fWindowMenuHandle);

  for i := 0 to count - 1 do
  begin
    FillChar (item, sizeof (item), 0);

    item.cbSize := 44; /// No No!  Don't use 'SizeOf' Bug in API.  Gets me every time!!!

    item.fMask := MIIM_TYPE or MIIM_ID or MIIM_STATE;
    item.cch := sizeof (buffer);
    item.dwTypeData := buffer;
    if GetMenuItemInfo (fWindowMenuHandle, i, True, item) then
    begin
      case item.wID of
        SC_RESTORE  : item.wID := scxRestore;
        SC_MINIMIZE : item.wID := scxMinimize;
        SC_MAXIMIZE : item.wID := scxMaximize;
        SC_MOVE     : item.wID := scxMove;
        SC_SIZE     : item.wID := scxSize;
      end;
      InsertMenuItem (fMenuHandle, i, True, item)
    end
  end
end;

destructor TStandardSystemMenu.Destroy;
begin
  FreeObjectInstance (fObjectInstance);
  FreeObjectInstance (fSysObjectInstance);

  inherited;
end;

procedure TStandardSystemMenu.Loaded;
begin
  inherited;
  fMenuHandle := GetSystemMenu (Application.Handle, False);
  fWindowMenuHandle := GetSystemMenu ((Owner as TForm).Handle, False);
  CloneSystemMenu;

  fObjectInstance := MakeObjectInstance (OwnerWindowProc);
  fOldOwnerWindowProc := TfnWndProc (SetWindowLong (TForm (Owner).Handle, GWL_WNDPROC, Integer (fObjectInstance)));

  fSysObjectInstance := MakeObjectInstance (SysOwnerWindowProc);
  fOldSysWindowProc := TfnWndProc (SetWindowLong (Application.Handle, GWL_WNDPROC, Integer (fSysObjectInstance)));
end;

procedure TStandardSystemMenu.OnMaximized;
begin
  fIconic := False;
  fMaximized := True;
  SetItemState (scxMinimize, MFS_ENABLED);
  SetItemState (scxMaximize, MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxMove,     MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxSize,     MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxRestore,  MFS_ENABLED);
end;

procedure TStandardSystemMenu.OnMinimized;
begin
  fIconic := True;
  SetItemState (scxMinimize, MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxMaximize, MFS_ENABLED);
  SetItemState (scxMove,     MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxSize,     MFS_DISABLED or MFS_GRAYED);
  SetItemState (scxRestore,  MFS_ENABLED);
end;

procedure TStandardSystemMenu.OnRestored (resetmax : boolean);
begin
  fIconic := False;
  if resetmax then fMaximized := False;
  if fMaximized then
    OnMaximized
  else
  begin
    SetItemState (scxMinimize, MFS_ENABLED);
    SetItemState (scxMaximize, MFS_ENABLED);
    SetItemState (scxMove,     MFS_ENABLED);
    SetItemState (scxSize,     MFS_ENABLED);
    SetItemState (scxRestore,  MFS_DISABLED or MFS_GRAYED)
  end
end;

procedure TStandardSystemMenu.OwnerWindowProc (var msg : TMessage);
begin
  with msg do
  begin
    if msg = WM_SIZE then
    begin
      case wParam of
        SIZE_MAXIMIZED : OnMaximized;
        SIZE_MINIMIZED : OnMinimized;
        SIZE_RESTORED  : OnRestored (true)
      end
    end
    else
      if msg = WM_DESTROY then
      begin
        SetWindowLong (TForm (Owner).Handle, GWL_WNDPROC, Integer (fOldOwnerWindowProc));
        SetWindowLong (Application.Handle, GWL_WNDPROC, Integer (fOldSysWindowProc))
      end;

    result := CallWindowProc (fOldOwnerWindowProc, TForm (Owner).Handle, msg, wParam, lParam)
  end
end;

procedure TStandardSystemMenu.SetItemState(itemID, state: Integer);
var
  item : TMenuItemInfo;
begin
  FillChar (item, SizeOf (item), 0);
  item.cbSize := 44;
  item.fMask := MIIM_STATE;
  if GetMenuItemInfo (fMenuHandle, itemID, False, item) then
  begin
    item.fState := state;
    SetMenuItemInfo (fMenuHandle, itemID, False, item)
  end
end;

procedure TStandardSystemMenu.SysOwnerWindowProc(var msg: TMessage);
var
  m : Integer;
begin
  with msg do
  begin
    if msg = WM_SYSCOMMAND then
    begin
      m := -1;
      case wParam of
        scxRestore  : m := SC_RESTORE;
        scxMinimize : m := SC_MINIMIZE;
        scxMaximize : if fMaximized then  // It's also minimized, but it *was* maximized so restore!
                        SendMessage (Application.Handle, WM_SYSCOMMAND, SC_RESTORE, lParam)
                      else
                      begin
                        if fIconic then
                          SendMessage (Application.Handle, WM_SYSCOMMAND, SC_RESTORE, lParam);
                        SendMessage (TForm (owner).Handle, WM_SYSCOMMAND, SC_MAXIMIZE, lParam);
                      end;

        scxMove     : m := SC_MOVE;
        scxSize     : m := SC_SIZE;
      end;

      if m <> -1 then
        if fIconic then
          SendMessage (Application.Handle, WM_SYSCOMMAND, m, lParam)
        else
          SendMessage (TForm (owner).Handle, WM_SYSCOMMAND, m, lParam);
    end
    else
      if msg = WM_SIZE then
        case wParam of
          SIZE_MAXIMIZED : OnMaximized;
          SIZE_MINIMIZED : OnMinimized;
          SIZE_RESTORED  : OnRestored (false)
        end;

    result := CallWindowProc (fOldSysWindowProc, Application.Handle, msg, wParam, lParam);
  end
end;

end.
