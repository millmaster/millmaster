unit SendKeys;

interface

uses
  Windows, SysUtils;
  
// Schickt den Text an das bezeichnete Fenster  
procedure SendTextToHandle(WindowHandle: hWnd; Text: String);
// Schickt Steuertasten (VK_...) an das bezeichnete Fenster
procedure SendKeysToHandle(WindowHandle: hWnd; aKeys: Array of Byte);

implementation

uses
  forms;
  
const
  cIncrement = 4;

(* -----------------------------------------------------------
  Da die Tasten an das aktive Control geschickt werden muss das entsprechende 
  Fenster aktiv sein.
  Original von:
    Copyright (c) 1999 by Borut Batagelj (Slovenia) 
                       Aleksey Kuznetsov (Ukraine)  
            Home Page: www.utilmind.com             
            E-Mail: info@utilmind.com               
-------------------------------------------------------------- *)  
procedure MakeWindowActive(wHandle: hWnd);
begin
  if IsIconic(wHandle) then
    ShowWindow(wHandle, SW_RESTORE)
  else
    BringWindowToTop(wHandle);
end;// procedure MakeWindowActive(wHandle: hWnd);

(* -----------------------------------------------------------
  Schickt Steuertasten (VK_...) an das aktive Fenster
-------------------------------------------------------------- *)  
procedure SendExtendedKey(aKeys: Array of Byte);
var
  i: Integer;
  xArrayIndex: integer;
  xInputs: array of TInput;
  
  (* -----------------------------------------------------------
    F�llt einen "Tastendruck" in den Record
  -------------------------------------------------------------- *)  
  procedure FillRecord(aKey: Byte; aFlag: DWORD);
  begin
    // Das Array wenn notwendig dynamisch erweitern
    if (xArrayIndex + 1) > Length(xInputs) then
      SetLength(xInputs, xArrayIndex + cIncrement);
      
    with xInputs[xArrayIndex] do begin
      Itype := INPUT_KEYBOARD;
      ki.wVk := aKey;
      // ScanCode ermitteln
      ki.wScan := MapVirtualKey(aKey, 0);
      // Sondertaste
      ki.dwFlags := aFlag or KEYEVENTF_EXTENDEDKEY;
      ki.time := 0;
      ki.dwExtraInfo := 0;
      // Anzahl virtueller Tastendr�cke
      inc(xArrayIndex);
    end;// with xInputs[(i -1) * 2] do begin
  end;// procedure FillRecord(aChar: Char; aNumber: integer);
begin
  xArrayIndex := 0;
  // Doppelte L�nge des Arrays da KeyDown und KeyUp gesendet werden
  SetLength(xInputs, Length(aKeys) * 2);

  // Immer jeweils ein Tastendruck und dann die Taste wieder loslassen
  for i := 0 to Length(aKeys) - 1 do begin
    FillRecord(aKeys[i], 0);
    FillRecord(aKeys[i], KEYEVENTF_KEYUP);
  end;// for i := 0 to Length(aKeys) do begin
  
  // Tastendr�cke versenden
  SendInput(xArrayIndex, xInputs[0], SizeOf(TInput));
  // Verarbeiten
  Application.ProcessMessages;
  
  SetLength(xInputs, 0);
end;// procedure SendExtendedKey(aKeys: Array of Byte);

(* -----------------------------------------------------------
  Schickt einen String an das aktive Fenster
-------------------------------------------------------------- *)  
{ TODO -olok : Umlaute werden nicht versendet }
procedure SendText(aText: string);
var
  i: Integer;
  xArrayIndex: integer;
  xInputs: array of TInput;
  xText: string;    
  xShift: boolean;                               
  
  procedure FillRecord(aChar: Char; aFlag: DWORD);
  begin
    // Das Array wenn notwendig dynamisch erweitern
    if (xArrayIndex + 1) > Length(xInputs) then
      SetLength(xInputs, xArrayIndex + cIncrement);
      
    with xInputs[xArrayIndex] do begin
      Itype := INPUT_KEYBOARD;
      ki.wVk := Ord(aChar);
      // ScanCode ermitteln
      ki.wScan := VkKeyScan(aChar);
      ki.dwFlags := aFlag;
      ki.time := 0;
      ki.dwExtraInfo := GetMessageExtraInfo;
      // Anzahl virtueller Tastendr�cke
      inc(xArrayIndex);
    end;// with xInputs[(i -1) * 2] do begin
  end;// procedure FillRecord(aChar: Char; aNumber: integer);

  procedure FillShiftRecord(aFlag: DWORD);
  begin
    if (xArrayIndex + 1) > Length(xInputs) then
      SetLength(xInputs, xArrayIndex + cIncrement);
      
    with xInputs[xArrayIndex] do begin
      Itype := INPUT_KEYBOARD;
      ki.wVk := VK_SHIFT;
      // ScanCode ermitteln
      ki.wScan := MapVirtualKey(VK_SHIFT, 0);
      // Sondertaste
      ki.dwFlags := aFlag or KEYEVENTF_EXTENDEDKEY;
      ki.time := 0;
      ki.dwExtraInfo := GetMessageExtraInfo;
      // Anzahl virtueller Tastendr�cke
      inc(xArrayIndex);
    end;// with xInputs[(i -1) * 2] do begin
  end;// procedure FillShiftRecord(aChar: Char; aNumber: integer);
begin
  xArrayIndex := 0;
  // Mindestens die doppelte L�nge des Textes, da KeyDown und KeyUp gesendet werden
  SetLength(xInputs, Length(aText) * 2);

  (* Die Buchstaben m�ssen als Grossbuchstaben gesendet werden
     Ob Gross oder Klein entscheidet die Shift Taste *)
  xText := AnsiUpperCase(aText);
  
  for i := 1 to Length(xText) do begin
    // K�nnte ein Grossbuchstabe sein
    xShift := (aText[i] = xText[i]);
    // Wenn sich Uppercase und normal unterscheiden, dann ist es ein Grossbuchstabe
    xShift := xShift and (AnsiLowerCase(xText[i]) <> AnsiUpperCase(xText[i]));
  
    if (xShift) then
      // Shift Taste "dr�cken"            
      FillShiftRecord(0);

    // Taste "dr�cken" und "loslassen"
    FillRecord(xText[i], 0);
    FillRecord(xText[i], KEYEVENTF_KEYUP);
    
    if (xShift) then               
      // Shift Taste "loslassen"            
      FillShiftRecord(KEYEVENTF_KEYUP);
  end;// for i := 1 to Length(xText) do begin
  
  // Tastendr�cke versenden
  SendInput(xArrayIndex, xInputs[0], SizeOf(TInput));
  // Verarbeiten
  Application.ProcessMessages;
  
  SetLength(xInputs, 0);
end;// procedure SendText(aText: string);

(* -----------------------------------------------------------
  Schickt Steuertasten (VK_...) an das bezeichnete Fenster
-------------------------------------------------------------- *)  
procedure SendKeysToHandle(WindowHandle: hWnd; aKeys: Array of Byte);
begin
  MakeWindowActive(WindowHandle);
  SendExtendedKey(aKeys);
end;// procedure SendKeysToHandle(WindowHandle: hWnd; aKeys: Array of Byte);

(* -----------------------------------------------------------
  Schickt den Text an das bezeichnete Fenster
-------------------------------------------------------------- *)  
procedure SendTextToHandle(WindowHandle: hWnd; Text: String);
begin
  MakeWindowActive(WindowHandle);
  SendText(Text);
end;// procedure SendTextToHandle(WindowHandle: hWnd; Text: String);

end.

