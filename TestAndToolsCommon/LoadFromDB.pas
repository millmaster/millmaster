unit LoadFromDB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, DBCtrls, mmDBListBox, Db, ADODB, mmADODataSet,
  mmADOConnection, mmDataSource, Grids, DBGrids, mmDBGrid, Buttons,
  mmBitBtn, mmListBox, ExtCtrls, mmPanel, mmLabel, mmEdit, VirtualTrees, MSXML2_TLB,
  mmMemo, mmImage, ConfigurationPersistence, mmVirtualStringTree, Menus,
  ActnList, ImgList, mmImageList, mmActionList, mmPopupMenu, ComCtrls,
  ToolWin, mmToolBar;

type
  TDBDialogMode = (dmNone, dmLoad, dmSave);

  TfrmLoadFromDB = class(TForm)
    gridXMLTable: TmmDBGrid;
    mADOConnection: TmmADOConnection;
    mXMLDataSet: TmmADODataSet;
    mDSXMLData: TmmDataSource;
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    mmPopupMenu1: TmmPopupMenu;
    mmActionList1: TmmActionList;
    mmImageList1: TmmImageList;
    acDeleteSelected: TAction;
    EintragLschen1: TMenuItem;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel5: TmmLabel;
    edTable: TmmEdit;
    edColumns: TmmEdit;
    edXMLCol: TmmEdit;
    edAdditional: TmmEdit;
    mDescriptionPanel: TmmPanel;
    mmPanel8: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmMemo;
    bShowData: TmmBitBtn;
    bLoadDefaults: TmmBitBtn;
    mmPanel1: TmmPanel;
    mmLabel4: TmmLabel;
    vstTable: TmmVirtualStringTree;
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    mmPanel4: TmmPanel;
    mmLabel6: TmmLabel;
    laDBServername: TmmLabel;
    procedure bShowDataClick(Sender: TObject);
    procedure vstTableGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure FormShow(Sender: TObject);
    procedure vstTableChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure butOKClick(Sender: TObject);
    procedure butCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure edTableChange(Sender: TObject);
    procedure bLoadDefaultsClick(Sender: TObject);
    procedure vstTablePaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure acDeleteSelectedExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
  private
    FDialogMode: TDBDialogMode;
    FSettings: TBaseXMLSettings;
    FUpdateCnt: Integer;
    FXMLName: string;
    FXMLStream: string;
    procedure ExecuteQuery(aName, aCols, aXMLCol, aAdditional: string);
    function GetDBTablesMRU: IXMLDOMElement;
    function GetNodeFromVST(Sender: TBaseVirtualTree; Node: PVirtualNode): IXMLDOMElement;
    procedure RefreshTree;
    { Private declarations }
  protected
    function IsUpdating: Boolean;
    procedure SetUpdating(Updating: Boolean);
  public
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure SaveRecentDBTables(aTable, aCols, aXMLCol, aAdditional: string);
    property DBTablesMRU: IXMLDOMElement read GetDBTablesMRU;
    property DialogMode: TDBDialogMode read FDialogMode write FDialogMode;
    property Settings: TBaseXMLSettings read FSettings write FSettings;
    property XMLName: string read FXMLName;
    property XMLStream: string read FXMLStream write FXMLStream;
    { Public declarations }
  end;

var
  frmLoadFromDB: TfrmLoadFromDB;

implementation

uses
  AdoDBAccess, XMLGlobal, LoepfeGlobal;

{$R *.DFM}

const
  cDBTablesRootElement = 'DBTablesMRU';
    cDBMRUElementName  = 'DBMRU';
    cXPDBTablesRoot    =  cXPDocumentBody + '/' + cDBTablesRootElement;
    cAtrTableName      = 'Name';
    cAtrColNames       = 'Col';
    cAtrXMLCol         = 'XMLCol';
    cAtrAdditional     = 'Additional';
    cAtrLastUsed       = 'Last';

  cTabCol  = 0;
  cColsCol = 1;
  cXMLCol  = 2;
  cAddCol  = 3;

procedure TfrmLoadFromDB.BeginUpdate;
begin
  Inc(FUpdateCnt);
  if FUpdateCnt = 1 then SetUpdating(True);
end;

procedure TfrmLoadFromDB.ExecuteQuery(aName, aCols, aXMLCol, aAdditional: string);
begin
  try
    mXMLDataSet.Active := false;
    Screen.Cursor := crHourglass;
    mXMLDataSet.CommandText := Format('SELECT %s FROM %s %s', [aCols, aName, aAdditional]);
    SaveRecentDBTables(aName, aCols, aXMLCol, aAdditional);
    mXMLDataSet.Active := true;
    RefreshTree;
    bShowData.Enabled := false;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TfrmLoadFromDB.bShowDataClick(Sender: TObject);
begin
  ExecuteQuery(edTable.Text, edColumns.Text, edXMLCol.Text, edAdditional.Text);
end;

procedure TfrmLoadFromDB.vstTableGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xNodeElement: IXMLDOMElement;
begin
  xNodeElement := nil;
  CellText := '';

  // Selektiertes Element
  xNodeElement := GetNodeFromVST(Sender, Node);
  if assigned(xNodeElement) then begin
    case Column of
      cTabCol  : CellText := AttributeToStringDef(cAtrTableName, xNodeElement, '');
      cColsCol : CellText := AttributeToStringDef(cAtrColNames, xNodeElement, '');
      cXMLCol  : CellText := AttributeToStringDef(cAtrXMLCol, xNodeElement, '');
      cAddCol  : CellText := AttributeToStringDef(cAtrAdditional, xNodeElement, '');
    end;// case Column of
  end;// if assigned(xNodeElement) then begin
end;

procedure TfrmLoadFromDB.FormShow(Sender: TObject);
var
  xLastUsed: Cardinal;
  xCurrentNode: PVirtualNode;
begin
  RefreshTree;
  if assigned(FSettings) then
    Settings.LoadForm(self, self.name, false);

  xLastUsed := Cardinal(AttributeToIntDef(cAtrLastUsed, DBTablesMRU, 0));
  xCurrentNode := vstTable.GetFirst;
  while (assigned(xCurrentNode)) and (vstTable.AbsoluteIndex(xCurrentNode) < xLastUsed) do
    xCurrentNode := xCurrentNode.NextSibling;

  vstTable.FocusedNode := xCurrentNode;
  vstTable.ClearSelection;
  vstTable.Selected[vstTable.FocusedNode] := true;
end;

function TfrmLoadFromDB.GetNodeFromVST(Sender: TBaseVirtualTree; Node: PVirtualNode): IXMLDOMElement;
var
  xNode: IXMLDOMNode;
begin
  xNode := nil;
  if assigned(FSettings) then
    xNode := DBTablesMRU.SelectSingleNode(Format('%s[%d]', [cDBMRUElementName, Sender.AbsoluteIndex(Node) + 1]));
  Supports(xNode, IXMLDOMElement, result);
end;

procedure TfrmLoadFromDB.RefreshTree;
begin
  if assigned(FSettings) then
    vstTable.RootNodeCount := DBTablesMRU.childNodes.Length;

  vstTable.ReinitNode(vstTable.RootNode, true);
  // Tree neu zeichen um die neue Sortierung anzuzeigen
  vstTable.Invalidate;
end;

procedure TfrmLoadFromDB.vstTableChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
var
  xNodeElement: IXMLDOMElement;
  function GetAtr(aAtrName: string): string;
  begin
    result := AttributeToStringDef(aAtrName, xNodeElement, '');
  end;// function GetAtr(aAtrName: string): string;
begin
  if not(IsUpdating) then begin
    BeginUpdate;
    try
      xNodeElement := GetNodeFromVST(Sender, Node);

      if assigned(xNodeElement) then begin
        edTable.Text      := GetAtr(cAtrTableName);
        edColumns.Text    := GetAtr(cAtrColNames);
        edXMLCol.Text     := GetAtr(cAtrXMLCol);
        edAdditional.Text := GetAtr(cAtrAdditional);

        ExecuteQuery(GetAtr(cAtrTableName), GetAtr(cAtrColNames), GetAtr(cAtrXMLCol), GetAtr(cAtrAdditional));
        DBTablesMRU.SetAttribute(cAtrLastUsed, integer(Sender.AbsoluteIndex(Node)));
      end;// if assigned(xNodeElement) then begin
    finally
      EndUpdate
    end;// try finally
  end;// if not(IsUpdating) then begin
end;

procedure TfrmLoadFromDB.butOKClick(Sender: TObject);
var
  i: Integer;
  xFound: Boolean;
begin
  case DialogMode of
    dmLoad: begin
      if mXMLDataSet.RecordCount > 0 then begin
        FXMLStream := mXMLDataSet.FieldByName(edXMLCol.Text).AsString;

        xFound := false;
        i := 0;
        while (not(xFound)) and (i < mXMLDataSet.FieldDefs.Count) do begin
          if AnsiPos('name', LowerCase(mXMLDataSet.FieldDefs[i].DisplayName)) > 0 then begin
            FXMLName := mXMLDataSet.FieldByName(mXMLDataSet.FieldDefs[i].Name).AsString;
            xFound := true;
          end;// if AnsiPos(LowerCase(mXMLDataSet.FieldDefs[i].DisplayName), 'name') > 0 then begin
          inc(i);
        end;// if AnsiPos('name', LowerCase(mXMLDataSet.FieldDefs[i].DisplayName)) > 0 then begin

        if FXMLName = '' then
          FXMLName := edXMLCol.Text;
      end else begin
        FXMLStream := '';
        FXMLName := '';
      end;// if mXMLDataSet.RecordCount > o then begin
    end;// dmLoad: begin

    dmSave: begin
      if mXMLDataSet.RecordCount > 0 then begin
(*          Table2.Edit;
    { hier eine andere Methode zum Erzeugen eines BLOB-Streams }
    Stream2 := Table2.CreateBlobStream(Table2.FieldByName('Remarks'),
                                       bmReadWrite);
    try
      Stream2.CopyFrom(Stream1, Stream1.Size);
      Table2.Post;
*)
        mXMLDataSet.Edit;
        mXMLDataSet.FieldByName(edXMLCol.Text).AsString := FXMLStream;
        mXMLDataSet.Post;
      end else begin
        MessageBox(0, 'Speichern nicht m�glich (Kein Datensatz)', 'Speichern', MB_ICONERROR or MB_OK);
      end;//

    end;// dmSave: begin
  end;// case DialogMode of
  Close;
end;

procedure TfrmLoadFromDB.butCancelClick(Sender: TObject);
begin
  close;
end;

procedure TfrmLoadFromDB.EndUpdate;
begin
  Dec(FUpdateCnt);
  if FUpdateCnt = 0 then SetUpdating(False);
end;

procedure TfrmLoadFromDB.FormCreate(Sender: TObject);
begin
  FXMLStream := '';
  FXMLName := '';
  laDBServerName.Caption := gSQLServerName;
end;

procedure TfrmLoadFromDB.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if assigned(FSettings) then
    Settings.SaveForm(self, self.name);
end;

function TfrmLoadFromDB.IsUpdating: Boolean;
begin
  Result := FUpdateCnt > 0;
end;

procedure TfrmLoadFromDB.SetUpdating(Updating: Boolean);
begin
end;

procedure TfrmLoadFromDB.edTableChange(Sender: TObject);
begin
  bShowData.Enabled := true;
end;

(* -----------------------------------------------------------
  Gibt das Root Element f�r die Liste der gesendeten Befehle zur�ck
-------------------------------------------------------------- *)  
function TfrmLoadFromDB.GetDBTablesMRU: IXMLDOMElement;
begin
  Result := nil;
  if assigned(Settings) then
    result := Settings.GetElement(cXPDBTablesRoot, true);
end;// function TLKTermSettings.GetReccentFilesRoot: IXMLDOMElement;

(* -----------------------------------------------------------
  Sichert das aktuelle File als das letzte verwendete File
-------------------------------------------------------------- *)
procedure TfrmLoadFromDB.SaveRecentDBTables(aTable, aCols, aXMLCol, aAdditional: string);
var
  xSelectNode: string;
  xRecentElement: IXMLDOMElement;
  xRecentNode: IXMLDOMNode;
  xMRUElement: IXMLDOMElement;
begin
  xMRUElement := DBTablesMRU;
  if assigned(xMRUElement) then begin
    with xMRUElement do begin
      xSelectNode := Format(cDBMRUElementName + '[@%s=''%s'' and @%s=''%s'' and @%s=''%s'' and @%s=''%s'']', [cAtrTableName, aTable, cAtrColNames, aCols, cAtrXMLCol, aXMLCol, cAtrAdditional, aAdditional]);
      xRecentNode := SelectSingleNode(xSelectNode);
      if not(assigned(xRecentNode)) then begin
        xRecentElement := Settings.DOM.createElement(cDBMRUElementName);
        xRecentElement.setAttribute(cAtrTableName, aTable);
        xRecentElement.setAttribute(cAtrColNames, aCols);
        xRecentElement.setAttribute(cAtrXMLCol, aXMLCol);
        xRecentElement.setAttribute(cAtrAdditional, aAdditional);
        insertBefore(xRecentElement, firstChild);
      end else begin
        Supports(xRecentNode, IXMLDOMElement, xRecentElement);
      end;// if not(assigned(xRecentFileNode)) then begin
    end;// with xMRUFileElement do begin
  end;// if assigned(xMRUElement) then begin
end;// procedure TLKTermSettings.SaveRecentFiles(aFileName: string);

procedure TfrmLoadFromDB.bLoadDefaultsClick(Sender: TObject);
begin
  SaveRecentDBTables('t_magroup_config', 'c_machine_id,c_xml_data', 'c_xml_data', '');
  SaveRecentDBTables('t_xml_mapfile', 'c_mapfile_name, c_mapfile', 'c_mapfile', '');
  SaveRecentDBTables('t_xml_ym_settings', 'c_YM_set_id, c_YM_set_name, c_xml_setting', 'c_xml_setting', '');
  RefreshTree;
end;

procedure TfrmLoadFromDB.vstTablePaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
begin
  case Column of
    cTabCol : TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold];
    cAddCol : TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsItalic];
  end;// case Column of
end;

procedure TfrmLoadFromDB.acDeleteSelectedExecute(Sender: TObject);
var
  xNodeElement: IXMLDOMElement;
begin
  xNodeElement := GetNodeFromVST(vstTable, vstTable.FocusedNode);

  if (assigned(xNodeElement)) and (assigned(xNodeElement.ParentNode)) then
    xNodeElement.ParentNode.removeChild(xNodeElement);
  RefreshTree;
end;

procedure TfrmLoadFromDB.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  acDeleteSelected.Enabled := (vstTable.FocusedNode <> nil);
end;

end.
