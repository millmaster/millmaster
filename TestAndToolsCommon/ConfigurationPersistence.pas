unit ConfigurationPersistence;

interface

uses Forms, MSXML2_TLB, XMLGlobal, sysutils;

type
  (* -----------------------------------------------------------
    Basisklasse f�r die settings. Die Klasse stellt einen Singleton und ein DOM Document zur Verf�gung.
    Das Document Element heisst <Body/>. Verwendet wird der MSXML 4.0

    Anwendung: 
      F�r eine Applkikation wird eine Klasse f�r die Settings abgeleitet. Einige Funktionen m�ssen
      �berschrieben werden (siehe unten). Der Singleton wird automatisch vererbt. Die freigabe muss allerdings in 
      der Applikation  erfolgen.

        destructor TLKTermSettings.Destroy;
        begin
          SaveSettings;
          inherited Destroy;
        end;// destructor TLKTermSettings.Destroy;

        class function TLKTermSettings.Instance: TLKTermSettings;
        begin
          Result := TLKTermSettings(AccessInstance(1));
        end;// class function TLKTermSettings.Instance: TLKTermSettings;

        function TLKTermSettings.GetSettingsFilePath: string;
        begin
          result := ExtractFileDir(Application.ExeName) + '\LKTerm Settings.xml';
        end;// function GetSettingsFilePath: string;
  -------------------------------------------------------------- *)
  TBaseXMLSettings = class(TObject)
  private
    function GetGeneralRoot: IXMLDOMElement;
    function GetMRUCount(aMRURoot: IXMLDOMNode): Integer;
    function GetRecentFilesMRU: IXMLDOMElement;
    function TestMSXMLInstalled: Boolean;
  protected
    FDOM: DOMDocumentMM;
    FXMLEnabled: Boolean;
    // Aufruf nur �ber AccessInstance()
    constructor CreateInstance; virtual;
    // Zugriffsmethode f�r den Singleton. 
    class function AccessInstance(Request: Integer): TBaseXMLSettings;
    function CanWorkWithoutXML: Boolean; virtual;
    // Gibt das globale Settings DOM zur�ck. Beim ersten Mal wird das DOM neu erzeugt.
    function GetDOM: DOMDocumentMM; virtual;
    function GetFormElement(aName: string): IXMLDOMElement;
    function GetUserSettingsDirectory: string;
  public
    // Konstruktor (Darf nicht aufgerufen werden. Zugriff nur �ber AccessInstance())
    constructor Create;
    destructor Destroy; override;
    // Gibt den Pfad zur Konfigurationsdatei zur�ck
    function GetSettingsFilePath: string; virtual; abstract;
    // Zugriff auf den Singleton
    class function BaseInstance: TBaseXMLSettings;
    function GetElement(aSelPath: string; aCreate: boolean = true): IXMLDOMElement;
    function LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement; virtual;
    function LoadMRU(aMRURootElement: IXMLDOMElement; aElementIndex: integer):
        string;
    // Freigeben des Singleton
    class procedure ReleaseInstance;
    function SaveForm(aForm: TForm; aName: string): IXMLDOMElement; virtual;
    function SaveMRU(aMRUEntry: string; aMRURootElement: IXMLDOMElement; aMRULength: integer): IXMLDOMElement;
    procedure SaveRecentFiles(aFileName: string; aReadOnly:boolean; aMRURootElement: IXMLDOMElement = nil);
    // Speichert die Settings im XML File
    procedure SaveSettings; virtual;

    // DOM f�r den Zugriff auf die Settings
    property DOM: DOMDocumentMM read GetDOM;
    property GeneralRoot: IXMLDOMElement read GetGeneralRoot;
    property MRUCount[aMRURoot: IXMLDOMNode]: Integer read GetMRUCount;
    property RecentFilesMRU: IXMLDOMElement read GetRecentFilesMRU;
    property XMLEnabled: Boolean read FXMLEnabled write FXMLEnabled;
  end;// TBaseXMLSettings = class(TObject)

const
  cXPDocumentBody = '/' + cXMLLoepfeBody;

  // Formular Position
  cFormElement      = 'Form';
    cAtrFormName    = 'Name';
    cAtrFormLeft    = 'Left';
    cAtrFormTop     = 'Top';
    cAtrFormRight   = 'Right';
    cAtrFormBottom  = 'Bottom';
    cAtrState       = 'State';
    cAtrFlags       = 'Flags';

  cFormPosElement   = 'Position';
    cAtrPosName     = 'Name';
    cAtrPosLeft     = 'Left';
    cAtrPosTop      = 'Top';
  cMinPosName       = 'Min';
  cMaxPosName       = 'Max';
  cXPFormElement    = cXPDocumentBody + '/' + cFormElement;

  cGeneralElement         = 'General';
    cXPGeneral            = cXPDocumentBody + '/' + cGeneralElement;

  cRecentFilesRootElement = 'RecentFilesMRU';
  cRecentFilesRecElement  = 'RecentFilesReceive';
    cXPRecentFiles        = cXPDocumentBody + '/' + cRecentFilesRootElement;
    cAtrRecentFilesName   = 'Name';
    cRecentFilesElement   = 'RecentFile';
    cAtrReadOnly          = 'ReadOnly';

  cMRUListElement   = 'MRUList';
  cMRUElement       = 'MRU';
    cAtrMRUName     = 'Name';
  cXPMRUElementFromList = cMRUListElement + '/' + cMRUElement;


implementation

uses
  windows, LoepfeGlobal, shlobj, comobj, filectrl, dialogs, messages, registry;

(* -----------------------------------------------------------
  Konstruktor (Darf nicht aufgerufen werden. Zugriff nur �ber AccessInstance())
-------------------------------------------------------------- *)
constructor TBaseXMLSettings.Create;
begin
  inherited Create;
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// constructor TBaseXMLSettings.Create;

(* -----------------------------------------------------------
  Aufruf nur �ber AccessInstance()
-------------------------------------------------------------- *)  
constructor TBaseXMLSettings.CreateInstance;
begin
  inherited Create;

  if not(TestMSXMLInstalled) then begin
    XMLEnabled := false;
    if not(CanWorkWithoutXML) then begin
      ShowMessage('Dieses Programm ben�tigt MSXML 4.0. Download bei "http://download.microsoft.com"');
      SendMessage(Application.Handle, WM_CLOSE, 0, 0);
    end;// if not(CanWorkWithoutXML) then
  end else begin
    XMLEnabled := true;
  end;// if not(TestMSXMLInstalled) then begin
end;// constructor TBaseXMLSettings.CreateInstance;

(* -----------------------------------------------------------
  Destruktor
-------------------------------------------------------------- *)  
destructor TBaseXMLSettings.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);
       
  FDOM := nil;
  inherited Destroy;
end;// destructor TBaseXMLSettings.Destroy;

(* -----------------------------------------------------------
  Zugriffsmethode f�r den Singleton. 
-------------------------------------------------------------- *)  
class function TBaseXMLSettings.AccessInstance(Request: Integer): TBaseXMLSettings;
{$J+}
  const FInstance: TBaseXMLSettings = nil;
{$J-}
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// class function TBaseXMLSettings.AccessInstance(Request: Integer): TLKTermSettings;         

(* -----------------------------------------------------------
  Zugriff auf den Singleton
-------------------------------------------------------------- *)  
class function TBaseXMLSettings.BaseInstance: TBaseXMLSettings;
begin
  Result := AccessInstance(1);
end;// class function TBaseXMLSettings.BaseInstance: TLKTermSettings;

(* -----------------------------------------------------------
  Ein Programm kann in seinem SettingsObjekt diese Methode �berschreiben und 
  die M�glichkeit mitteilen, ohne XML zu arbeiten
-------------------------------------------------------------- *)  
function TBaseXMLSettings.CanWorkWithoutXML: Boolean;
begin
  Result := false;
end;// function TBaseXMLSettings.CanWorkWithoutXML: Boolean;

(* -----------------------------------------------------------
  Gibt das globale Settings DOM zur�ck. Beim ersten Mal wird das DOM neu erzeugt.
-------------------------------------------------------------- *)  
function TBaseXMLSettings.GetDOM: DOMDocumentMM;
begin
  // DOM Document erstellen, wenn noch nicht zugewiesen
  if not(assigned(FDOM)) then begin
    // Settings DOM erzeugen
    FDOM := DOMDocumentMMCreate;
    if FileExists(GetSettingsFilePath) then 
      // Wenn das File existiert, dann das DOM aus dem File laden
      FDOM.load(GetSettingsFilePath)
    else 
      // Sonst das DOM initialisieren
      InitXMLDocument(FDOM, cXMLLoepfeBody);
  end;// if not(assigned(FDOM)) then begin

  // Mit diesem DOM werden die Settings abgebildet
  result := FDOM;
end;// function TBaseXMLSettings.GetDOM: DOMDocumentMM;

(*---------------------------------------------------------
  Gibt das entsprechende Element zur�ck
  Mit aCreate kann angegeben werden, dass das Element erzeugt werden soll,
  wenn es noch nicht existiert.
----------------------------------------------------------*)
function TBaseXMLSettings.GetElement(aSelPath: string; aCreate: boolean = true): IXMLDOMElement;
var
  xResultNode: IXMLDOMNode;
begin
  Result := nil;

  if assigned(DOM) then begin
    xResultNode := nil;
    if aCreate then
      xResultNode := GetCreateElement(aSelPath, DOM)
    else
      xResultNode := DOM.SelectSingleNode(aSelPath);
    Supports(xResultNode, IXMLDOMElement, result);
  end;// if assigned(DOM) then begin
end;// function TBaseXMLSettings.GetElement(aSelPath: string; aCreate: boolean = true): IXMLDOMElement;

(* -----------------------------------------------------------
  Sucht das Element f�r das Formular mit dem angegebenen Namen
-------------------------------------------------------------- *)  
function TBaseXMLSettings.GetFormElement(aName: string): IXMLDOMElement;
var
  xFormNode: IXMLDOMNode;
begin
  xFormNode := nil;
  if assigned(DOM) then
    xFormNode := DOM.SelectSingleNode(Format(cXPFormElement + '[@%s=''%s'']', [cAtrFormName, aName]));
  if assigned(xFormNode) then
    result := (xFormNode as IXMLDOMElement);
end;// function TBaseXMLSettings.GetFormElement(aName: string): IXMLDOMElement;

(* -----------------------------------------------------------
  Gibt das Root Element f�r die Liste der gesendeten Befehle zur�ck
-------------------------------------------------------------- *)  
function TBaseXMLSettings.GetGeneralRoot: IXMLDOMElement;
begin
  Result := nil;
  if assigned(DOM) then 
    result := GetCreateElement(cXPGeneral, DOM);
end;// function TLKTermSettings.GetReccentFilesRoot: IXMLDOMElement;

(* -----------------------------------------------------------
  Gibt die Anzahl vorhandener MRU Elemente zur�ck
-------------------------------------------------------------- *)  
function TBaseXMLSettings.GetMRUCount(aMRURoot: IXMLDOMNode): Integer;
begin
  Result := 0;
  if assigned(aMRURoot) then 
    result := aMRURoot.selectNodes(cXPMRUElementFromList).length;  
end;// function TBaseXMLSettings.GetMRUCount(aMRURoot: IXMLDOMNode): Integer;

(* -----------------------------------------------------------
  Gibt das Root Element f�r die Liste der gesendeten Befehle zur�ck
-------------------------------------------------------------- *)  
function TBaseXMLSettings.GetRecentFilesMRU: IXMLDOMElement;
begin
  Result := nil;
  if assigned(DOM) then 
    result := GetCreateElement(cXPRecentFiles, DOM);
end;// function TLKTermSettings.GetReccentFilesRoot: IXMLDOMElement;

(*---------------------------------------------------------
  Liefert den Pfad zum Applikationsverzeichnis des angemeldeten Benutzers
----------------------------------------------------------*)
function TBaseXMLSettings.GetUserSettingsDirectory: string;
var
  item: PItemIDList;
  buffer: array[0..MAX_PATH] of Char;
begin
  // PIDL holen
  OleCheck(SHGetSpecialFolderLocation(0, CSIDL_APPDATA, item));
  // Pfad erfragen
  SHGetPathFromIDList(item, buffer);
  // In Pascalstring umkopieren
  SetString(result, buffer, StrLen(buffer));

  // Jetzt fehlt nur noch das L�pfe Verzeichnis
  result := IncludeTrailingBackslash(result)+ 'Loepfe\';
end;// function TBaseXMLSettings.GetUserSettingsDirectory: string;

(* -----------------------------------------------------------
  L�dt die Position und den Status eines Formulars aus dem DOM
-------------------------------------------------------------- *)
function TBaseXMLSettings.LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement;
var
  xFormElement: IXMLDOMElement;
  xPosElement: IXMLDOMElement;
  xPosNode: IXMLDOMNode;
  xWindowPlacement: TWindowPlacement;
var
  xFormDim: TPoint;
begin
  result := nil;
  if assigned(aForm) and assigned(DOM) then begin
    if aDialog then begin
      // Originalgr�sse bei Dialogen
      xFormDim.x := aForm.Width;
      xFormDim.y := aForm.Height;
    end;// if aDialog then begin
    
    xFormElement := GetFormElement(aName);
    result := xFormElement;
    if assigned(xFormElement) then begin
      // Initialisieren
      xWindowPlacement.length := sizeOf(TWindowPlacement);  
      // Fragt die aktuelle Position und Gr�sse ab
      GetWindowPlacement(aForm.Handle, @xWindowPlacement);

      // Position und Gr�sse des Formulars aus dem DOM holen
      xWindowPlacement.rcNormalPosition.Left := AttributeToIntDef(cAtrFormLeft, xFormElement, xWindowPlacement.rcNormalPosition.Left);
      xWindowPlacement.rcNormalPosition.Top  := AttributeToIntDef(cAtrFormTop, xFormElement, xWindowPlacement.rcNormalPosition.Top);
      xWindowPlacement.rcNormalPosition.Right  := AttributeToIntDef(cAtrFormRight, xFormElement, xWindowPlacement.rcNormalPosition.Right);
      xWindowPlacement.rcNormalPosition.Bottom  := AttributeToIntDef(cAtrFormBottom, xFormElement, xWindowPlacement.rcNormalPosition.Bottom);
      xWindowPlacement.showCmd := AttributeToIntDef(cAtrState, xFormElement, xWindowPlacement.showCmd);
      xWindowPlacement.flags := AttributeToIntDef(cAtrFlags, xFormElement, xWindowPlacement.flags);

      // Position des Formulars, wenn es minimiert ist
      xPosNode := xFormElement.selectSingleNode(cFormPosElement + Format('[@%s=''%s'']', [cAtrPosName, cMinPosName]));
      Supports(xPosNode, IXMLDOMElement, xPosElement);
      if assigned(xPosElement) then begin
        xWindowPlacement.ptMinPosition.x := AttributeToIntDef(cAtrPosLeft, xPosElement, xWindowPlacement.ptMinPosition.x);
        xWindowPlacement.ptMinPosition.y := AttributeToIntDef(cAtrPosTop, xPosElement, xWindowPlacement.ptMinPosition.y);
      end;// if assigned(xPosElement) then begin
      
      // Position des Formulars, wenn es maximiert ist
      xPosNode := xFormElement.selectSingleNode(cFormPosElement + Format('[@%s=''%s'']', [cAtrPosName, cMaxPosName]));
      Supports(xPosNode, IXMLDOMElement, xPosElement);
      if assigned(xPosElement) then begin
        xWindowPlacement.ptMinPosition.x := AttributeToIntDef(cAtrPosLeft, xPosElement, xWindowPlacement.ptMinPosition.x);
        xWindowPlacement.ptMinPosition.y := AttributeToIntDef(cAtrPosTop, xPosElement, xWindowPlacement.ptMinPosition.y);
      end;// if assigned(xPosElement) then begin
      SetWindowPlacement(aForm.Handle, @xWindowPlacement);

      if aForm.Left < Screen.DesktopLeft then
        aForm.Left := Screen.DesktopLeft;
      if aForm.Top < Screen.DesktopTop then
        aForm.Top := Screen.DesktopTop;
      if (aForm.Left + aForm.Width) > (Screen.DesktopLeft + Screen.DesktopWidth) then
        aForm.Left := (Screen.DesktopLeft + Screen.DesktopWidth) - aForm.Width;
      if (aForm.Top + aForm.Height) > (Screen.DesktopTop + Screen.DesktopHeight) then
        aForm.Top := (Screen.DesktopTop + Screen.DesktopHeight) - aForm.Height;

    end;// if assigned(xFormElement) then begin
    if aDialog then begin
      // Originalgr�sse bei Dialogen
      aForm.Width := xFormDim.x;
      aForm.Height := xFormDim.y;
    end;// if aDialog then begin
  end;// if assigned(aForm) and assigned(DOM) then begin
end;// procedure TBaseXMLSettings.LoadForm(aForm: TForm);

(*---------------------------------------------------------
  Gibt das selektierte Element zur�ck
----------------------------------------------------------*)
function TBaseXMLSettings.LoadMRU(aMRURootElement: IXMLDOMElement;
    aElementIndex: integer): string;
var
  xMRUNode: IXMLDOMNode;
begin
 Result := '';
  if assigned(aMRURootElement) then begin
    xMRUNode := aMRURootElement.selectSingleNode(Format('%s[%d]', [cXPMRUElementFromList, aElementIndex + 1]));
    result := AttributeToStringDef(cAtrMRUName, xMRUNode, '');      
  end;// if assigned(aMRURootElement) then begin
end;// function TBaseXMLSettings.LoadMRU(aMRURootElement: IXMLDOMElement; aElementIndex: integer): string;

(* -----------------------------------------------------------
  Freigeben des Singleton
-------------------------------------------------------------- *)  
class procedure TBaseXMLSettings.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// class procedure TBaseXMLSettings.ReleaseInstance;

(* -----------------------------------------------------------
  Speichert die Position und den Status eines Formulars im DOM
  Wenn kein Name angegeben ist, dann wird die Form unter dem eigenen Namen gespeichert.
  Der R�ckgabewert ist das DOMElement in das die Parameter gespeichert wurden.
-------------------------------------------------------------- *)  
function TBaseXMLSettings.SaveForm(aForm: TForm; aName: string): IXMLDOMElement;
var
  xFormElement: IXMLDOMElement;
  xPosElement: IXMLDOMElement;
  xPosNode: IXMLDOMNode;
  xWindowPlacement: TWindowPlacement;
  
  (* -----------------------------------------------------------
    Erzeugt ein Form Element und die untergeordneten Min und Max Elemente
  -------------------------------------------------------------- *)  
  function CreateFormElement: IXMLDOMElement;
  begin
    result := DOM.createElement(cFormElement);

    // Erzeugt das Element f�r die Min Position
    xPosElement := DOM.createElement(cFormPosElement);
    xPosElement.setAttribute(cAtrPosName, cMinPosName);
    result.appendChild(xPosElement);
      
    // Erzeugt das Element f�r die Max Position
    xPosElement := DOM.createElement(cFormPosElement);
    xPosElement.setAttribute(cAtrPosName, cMaxPosName);
    result.appendChild(xPosElement);

    // F�gt das gesammte Form Element zum DOM hinzu  
    DOM.documentElement.appendChild(result);
  end;// procedure CreateFormElement(aName: string);

begin
  result := nil;
  if assigned(aForm) and assigned(DOM) then begin
    xFormElement := GetFormElement(aName);

    // Wenn das Element nicht vorhanden ist, dann erzeugen
    if not(assigned(xFormElement)) then 
      xFormElement := CreateFormElement;

    if assigned(xFormElement) then begin
      result := xFormElement;
      // Initialisieren
      xWindowPlacement.length := sizeOf(TWindowPlacement);  
      // Fragt die aktuelle Position und Gr�sse ab
      GetWindowPlacement(aForm.Handle, @xWindowPlacement);

      // Position und Gr�sse des Formulars ins DOM schreiben
      xFormElement.SetAttribute(cAtrFormName, aName);
      xFormElement.SetAttribute(cAtrFormLeft, xWindowPlacement.rcNormalPosition.Left);
      xFormElement.SetAttribute(cAtrFormTop, xWindowPlacement.rcNormalPosition.Top);
      xFormElement.SetAttribute(cAtrFormRight, xWindowPlacement.rcNormalPosition.Right);
      xFormElement.SetAttribute(cAtrFormBottom, xWindowPlacement.rcNormalPosition.Bottom);
      
      xFormElement.SetAttribute(cAtrState, integer(xWindowPlacement.showCmd));
      xFormElement.SetAttribute(cAtrFlags, integer(xWindowPlacement.flags));

      // Position des Formulars, wenn es minimiert ist
      xPosNode := xFormElement.selectSingleNode(cFormPosElement + Format('[@%s=''%s'']', [cAtrPosName, cMinPosName]));
      Supports(xPosNode, IXMLDOMElement, xPosElement);
      if assigned(xPosElement) then begin
        xPosElement.setAttribute(cAtrPosLeft, xWindowPlacement.ptMinPosition.x);
        xPosElement.setAttribute(cAtrPosTop, xWindowPlacement.ptMinPosition.y);
      end;// if assigned(xPosElement) then begin
      
      // Position des Formulars, wenn es maximiert ist
      xPosNode := xFormElement.selectSingleNode(cFormPosElement + Format('[@%s=''%s'']', [cAtrPosName, cMaxPosName]));
      Supports(xPosNode, IXMLDOMElement, xPosElement);
      if assigned(xPosElement) then begin
        xPosElement.setAttribute(cAtrPosLeft, xWindowPlacement.ptMaxPosition.x);
        xPosElement.setAttribute(cAtrPosTop, xWindowPlacement.ptMaxPosition.y);
      end;// if assigned(xPosElement) then begin
    end;// if assigned(xFormElement) then begin
  end;// if assigned(aForm) and assigned(DOM) then begin
end;// procedure TBaseXMLSettings.SaveForm(aForm: TForm);

(* -----------------------------------------------------------
  Sichert das aktuelle Element als das letzte verwendete File
-------------------------------------------------------------- *)
function TBaseXMLSettings.SaveMRU(aMRUEntry: string; aMRURootElement: IXMLDOMElement; aMRULength: integer): IXMLDOMElement;
var
  xMRUElement: IXMLDOMElement;
  xMRUNode: IXMLDOMNode;
  xMRUList: IXMLDOMElement;
begin
  result := nil;
  if assigned(aMRURootElement) then begin
    with aMRURootElement do begin
      xMRUNode := SelectSingleNode(Format(cXPMRUElementFromList + '[@%s=''%s'']', [cAtrMRUName, aMRUEntry]));
      if not(assigned(xMRUNode)) then 
        xMRUElement := FDOM.createElement(cMRUElement)
      else 
        Supports(xMRUNode, IXMLDOMElement, xMRUElement);

      // Element an erster Stelle einf�gen
      if assigned(xMRUElement) then begin
        xMRUElement.setAttribute(cAtrMRUName, aMRUEntry);
        xMRUList := GetCreateElement(cMRUListElement, DOM, aMRURootElement);
        if assigned(xMRUList) then 
          xMRUList.insertBefore(xMRUElement, xMRUList.firstChild);
      end;// if assigned(xMRUElement) then
      
      if selectNodes(cMRUElement).length > aMRULength then
        removeChild(SelectSingleNode(cMRUElement + '[last()]'));
      result := xMRUElement;
    end;// with aMRURootElement do begin
  end;// if assigned(aMRURootElement) then begin
end;// function TBaseXMLSettings.SaveMRU(aMRUEntry: string; aMRURootElement: IXMLDOMElement; aMRULength: integer = 0): IXMLDOMElement;

(* -----------------------------------------------------------
  Sichert das aktuelle File als das letzte verwendete File
-------------------------------------------------------------- *)
procedure TBaseXMLSettings.SaveRecentFiles(aFileName: string; aReadOnly:boolean; aMRURootElement: IXMLDOMElement = nil);
var
  xRecentFileElement: IXMLDOMElement;
  xRecentFileNode: IXMLDOMNode;
  xMRUFileElement: IXMLDOMElement;
begin
  xMRUFileElement := RecentFilesMRU;
  if assigned(aMRURootElement) then
    xMRUFileElement := aMRURootElement;

  with xMRUFileElement do begin
    xRecentFileNode := SelectSingleNode(Format(cRecentFilesElement + '[@%s=''%s'']', [cAtrRecentFilesName, aFileName]));
    if not(assigned(xRecentFileNode)) then begin
      xRecentFileElement := FDOM.createElement(cRecentFilesElement);
      xRecentFileElement.setAttribute(cAtrRecentFilesName, aFileName);
    end else begin
      if IsInterface(xRecentFileNode, IXMLDOMElement) then
        xRecentFileElement := xRecentFileNode as IXMLDOMElement;
    end;// if not(assigned(xRecentFileNode)) then begin

    // ReadOnly-Flag setzen
    xRecentFileElement.setAttribute(cAtrReadOnly, aReadOnly);

    // Element an erster Stelle einf�gen
    if assigned(xRecentFileElement) then
      insertBefore(xRecentFileElement, firstChild);
    // Maximal 9 Files
    if selectNodes(cRecentFilesElement).length > 9 then 
      removeChild(SelectSingleNode(cRecentFilesElement + '[last()]'));
  end;// with xMRUFileElement do begin
end;// procedure TLKTermSettings.SaveRecentFiles(aFileName: string);

(* -----------------------------------------------------------
  Speichert die Settings im XML File
-------------------------------------------------------------- *)  
procedure TBaseXMLSettings.SaveSettings;
var
  xFilename: string;
begin
  if assigned(DOM) then begin
    xFilename := GetSettingsFilePath;
    if not(DirectoryExists(ExtractFileDir(xFilename))) then
      ForceDirectories(ExtractFileDir(xFilename));
    DOM.save(xFilename);
  end;// if assigned(DOM) then begin
end;// procedure TBaseXMLSettings.SaveSettings;

(* -----------------------------------------------------------
  Pr�ft ob MSXML 4.0 installiert ist
-------------------------------------------------------------- *)  
function TBaseXMLSettings.TestMSXMLInstalled: Boolean;

  function ExpandEnvStr(const szInput: string): string;
  const
    MAXSIZE = 32768;
  begin
    SetLength(Result,MAXSIZE);
    SetLength(Result,ExpandEnvironmentStrings(pchar(szInput),
      @Result[1],length(Result)));
  end;

var
  reg : TRegistry;
  s   : string;
begin
  Result := false;

  reg    := TRegistry.Create(KEY_READ);
  if(reg <> nil) then begin
    with reg do try
      RootKey := HKEY_CLASSES_ROOT;

      if(OpenKey('CLSID\' + GuidToString(CLASS_DOMDocument40) + '\InProcServer32',false)) then
      try
        s      := ReadString('');
        Result := fileexists(ExpandEnvStr(s));
      finally
        CloseKey;
      end;
    finally
      Free;
    end;
  end;
end;// function TBaseXMLSettings.TestMSXMLInstalled: Boolean;


end.

