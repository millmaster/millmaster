object Form1: TForm1
  Left = 342
  Top = 202
  BorderStyle = bsDialog
  Caption = 'Set MillMaster Packages (ADO)'
  ClientHeight = 293
  ClientWidth = 507
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 16
    Top = 8
    Width = 61
    Height = 16
    Caption = 'MMHost:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laMMHost: TmmLabel
    Left = 80
    Top = 8
    Width = 7
    Height = 16
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel
    Left = 184
    Top = 8
    Width = 73
    Height = 16
    Caption = 'Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDatabase: TmmLabel
    Left = 264
    Top = 8
    Width = 7
    Height = 16
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object GroupBox2: TGroupBox
    Left = 208
    Top = 32
    Width = 273
    Height = 255
    Caption = 'Info'
    TabOrder = 0
    object Label4: TLabel
      Left = 15
      Top = 16
      Width = 90
      Height = 13
      Caption = 'Installed Packages'
    end
    object Label5: TLabel
      Left = 15
      Top = 120
      Width = 100
      Height = 13
      Caption = 'Installed components'
    end
    object ListBox1: TListBox
      Left = 15
      Top = 32
      Width = 240
      Height = 73
      ItemHeight = 13
      TabOrder = 0
    end
    object ListBox2: TListBox
      Left = 15
      Top = 135
      Width = 240
      Height = 106
      ItemHeight = 13
      TabOrder = 1
    end
  end
  object rgPackage: TmmRadioGroup
    Left = 8
    Top = 32
    Width = 193
    Height = 137
    Caption = 'MillMaster Package'
    Items.Strings = (
      'Easy'
      'Standard'
      'Pro')
    TabOrder = 1
  end
  object bSave: TButton
    Left = 7
    Top = 175
    Width = 150
    Height = 25
    Caption = 'Save selected Packages'
    TabOrder = 2
    OnClick = bSaveClick
  end
end
