unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  SettingsReader, StdCtrls, mmCheckBox, ExtCtrls, mmRadioGroup, mmLabel;

type
  TForm1 = class(TForm)
    GroupBox2: TGroupBox;
    ListBox1: TListBox;
    Label4: TLabel;
    ListBox2: TListBox;
    Label5: TLabel;
    rgPackage: TmmRadioGroup;
    bSave: TButton;
    mmLabel1: TmmLabel;
    laMMHost: TmmLabel;
    mmLabel3: TmmLabel;
    laDatabase: TmmLabel;
    procedure bSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    mSettings : TMMSettings;
    procedure ShowSettings;
  public
  end;

var
  Form1: TForm1;


implementation
{$R *.DFM}
uses
  LoepfeGlobal, mmSetupModul;

//------------------------------------------------------------------------------
procedure TForm1.bSaveClick(Sender: TObject);
var
  xp : TMMPackage;
begin
  case rgPackage.ItemIndex of
    1: xp := mmpStandard;
    2: xp := mmpPro;
  else
    xp := mmpEasy;
  end;
  WriteMMPackageToDB(xp);
  mSettings.ReadDBValues;
  ShowSettings;
end;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  laMMHost.Caption := gMMHost;
  laDatabase.Caption := gSQLServerName + '/' + gDBName;
  
  mSettings := SettingsReader.TMMSettings.Create;
  mSettings.Init;
  ShowSettings;
end;
//------------------------------------------------------------------------------
procedure TForm1.FormDestroy(Sender: TObject);
begin
  mSettings.Free;
end;
//------------------------------------------------------------------------------
procedure TForm1.ShowSettings;
begin
  if mSettings.IsPackageEasy then rgPackage.ItemIndex := 0
  else if mSettings.IsPackageStandard then rgPackage.ItemIndex := 1
  else if mSettings.IsPackagePro then rgPackage.ItemIndex := 2;

  ListBox1.Items.CommaText:= mSettings.PackagesAsText;
  ListBox2.Items.CommaText:= mSettings.ComponentsAsText;
end;
//------------------------------------------------------------------------------
end.

