object Form1: TForm1
  Left = 459
  Top = 128
  BorderStyle = bsDialog
  Caption = 'Set MillMaster Packages'
  ClientHeight = 588
  ClientWidth = 323
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 40
    Width = 140
    Height = 25
    Caption = 'Save Packages'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 32
    Top = 136
    Width = 140
    Height = 25
    Caption = 'Delete all Packages'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 32
    Top = 80
    Width = 140
    Height = 25
    Caption = 'Delete Package'
    TabOrder = 2
    OnClick = Button3Click
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 184
    Width = 289
    Height = 385
    Caption = 'TMMSettings'
    TabOrder = 3
    object Label1: TLabel
      Left = 100
      Top = 64
      Width = 46
      Height = 13
      Caption = 'Unknown'
    end
    object Label2: TLabel
      Left = 100
      Top = 96
      Width = 46
      Height = 13
      Caption = 'Unknown'
    end
    object Label3: TLabel
      Left = 100
      Top = 136
      Width = 46
      Height = 13
      Caption = 'Unknown'
    end
    object Button4: TButton
      Left = 10
      Top = 24
      Width = 75
      Height = 25
      Caption = 'Refresh Data'
      TabOrder = 0
      OnClick = Button4Click
    end
    object Button5: TButton
      Left = 10
      Top = 56
      Width = 75
      Height = 25
      Caption = 'IsQOfflimit'
      TabOrder = 1
      OnClick = Button5Click
    end
    object Button6: TButton
      Left = 10
      Top = 88
      Width = 75
      Height = 25
      Caption = 'IsArticle'
      TabOrder = 2
      OnClick = Button6Click
    end
    object Button7: TButton
      Left = 10
      Top = 128
      Width = 75
      Height = 25
      Caption = 'IsLabMaster'
      TabOrder = 3
      OnClick = Button7Click
    end
    object Button8: TButton
      Left = 10
      Top = 184
      Width = 113
      Height = 25
      Caption = 'GetPackages as Text'
      TabOrder = 4
      OnClick = Button8Click
    end
    object Button9: TButton
      Left = 10
      Top = 280
      Width = 137
      Height = 25
      Caption = 'GetComponents as Text'
      TabOrder = 5
      OnClick = Button9Click
    end
    object ListBox1: TListBox
      Left = 152
      Top = 176
      Width = 121
      Height = 81
      ItemHeight = 13
      TabOrder = 6
    end
    object ListBox2: TListBox
      Left = 152
      Top = 280
      Width = 121
      Height = 97
      ItemHeight = 13
      TabOrder = 7
    end
  end
  object cbWM: TmmCheckBox
    Left = 200
    Top = 40
    Width = 97
    Height = 17
    Caption = 'WindingMaster'
    TabOrder = 4
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbLM: TmmCheckBox
    Left = 200
    Top = 80
    Width = 97
    Height = 17
    Caption = 'LabMaster'
    TabOrder = 5
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbDB: TmmCheckBox
    Left = 200
    Top = 112
    Width = 97
    Height = 17
    Caption = 'DispoMaster'
    TabOrder = 6
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbRA: TmmCheckBox
    Left = 200
    Top = 144
    Width = 97
    Height = 17
    Caption = 'ReinigerAssistent'
    TabOrder = 7
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
end
