unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmSetupModul, StdCtrls, mmCheckBox;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    GroupBox1: TGroupBox;
    Button4: TButton;
    Label1: TLabel;
    Button5: TButton;
    Button6: TButton;
    Label2: TLabel;
    Button7: TButton;
    Label3: TLabel;
    cbWM: TmmCheckBox;
    cbLM: TmmCheckBox;
    cbDB: TmmCheckBox;
    cbRA: TmmCheckBox;
    Button8: TButton;
    Button9: TButton;
    ListBox1: TListBox;
    ListBox2: TListBox;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure Button5Click(Sender: TObject);
    procedure Button6Click(Sender: TObject);
    procedure Button7Click(Sender: TObject);
    procedure Button8Click(Sender: TObject);
    procedure Button9Click(Sender: TObject);
  private
    { Private declarations }
    mSettings : TMMSettings;
    procedure ShowSettings;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;


implementation

{$R *.DFM}


procedure TForm1.Button1Click(Sender: TObject);
var xp : TMMPackageSet;
begin
  xp:= [];
  if cbWM.Checked then include(xp, mmpWindingMaster);
  if cbLM.Checked then include(xp, mmpLabMaster);
  if cbDB.Checked then include(xp, mmpDispoMaster);
  if cbRA.Checked then include(xp, mmpClearerAssistant);

  WriteMMPackageToDB(xp);
  mSettings.ReadDBValues;
  ShowSettings;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  DeleteAllPackages;
  mSettings.ReadDBValues;
  ShowSettings;
end;

procedure TForm1.Button3Click(Sender: TObject);
var xp : TMMPackageSet;
begin
  xp:= [];

  if cbWM.Checked then include(xp, mmpWindingMaster);
  if cbLM.Checked then include(xp, mmpLabMaster);
  if cbDB.Checked then include(xp, mmpDispoMaster);
  if cbRA.Checked then include(xp, mmpClearerAssistant);

  DeleteMMPackage( xp );
  mSettings.ReadDBValues;
  ShowSettings;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mSettings := TMMSettings.Create;
  mSettings.Init;
  ShowSettings;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  mSettings.Free;
end;

procedure TForm1.Button4Click(Sender: TObject);
begin
  mSettings.ReadDBValues;
  ShowSettings;
end;

procedure TForm1.Button5Click(Sender: TObject);
var x : String;
begin
  case mSettings.IsComponentQOfflimit of
       TRUE  : x:= 'Yes';
       FALSE : x:= 'No';
  end;
  Label1.Caption:= x;
end;

procedure TForm1.Button6Click(Sender: TObject);
var x : String;
begin
  case mSettings.IsComponentArticle of
       TRUE  : x:= 'Yes';
       FALSE : x:= 'No';
  end;
  Label2.Caption:= x;
end;

procedure TForm1.Button7Click(Sender: TObject);
var x : String;
begin
  case mSettings.IsPackageLabMaster of
 //case mSettings.IsComponentLabReport of
       TRUE  : x:= 'Yes';
       FALSE : x:= 'No';
  end;
  Label3.Caption:= x;
end;

procedure TForm1.showsettings;
begin
  cbWM.Checked := mSettings.IsPackageWindingMaster;
  cbLM.Checked := mSettings.IsPackageLabMaster;
  cbDB.Checked := mSettings.IsPackageDispoMaster;
  cbRA.Checked := mSettings.IsComponentClearerAssistant;

  ListBox1.Items.CommaText:= mSettings.PackagesAsText;
  ListBox2.Items.CommaText:= mSettings.ComponentsAsText;
end;

procedure TForm1.Button8Click(Sender: TObject);
begin
  ListBox1.Items.CommaText:= mSettings.PackagesAsText;
end;

procedure TForm1.Button9Click(Sender: TObject);
begin
  ListBox2.Items.CommaText:= mSettings.ComponentsAsText;
end;

end.
