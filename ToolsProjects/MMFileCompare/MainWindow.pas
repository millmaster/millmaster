unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, ImgList, mmImageList, ActnList, mmActionList, ExtCtrls,
  mmSplitter, ToolWin, mmToolBar, VirtualTrees, mmVirtualStringTree,
  mmPanel, mmStatusBar, StdCtrls, mmStaticText, mmLabel, Buttons,
  mmSpeedButton, Spin, mmSpinEdit, Grids, mmStringGrid, mmCheckBox, mmEdit;

type
  PReal48 = ^Real48;

  TDisplayState = (dsNone,
                   dsHex,
                   dsASCII,
                   dsDez);// TDisplayState

  TDataType = (dtByte,
               dtshortInt,
               dtWord,
               dtSmallInt,
               dtDWord,
               dtInteger,
               dtInt64,
               dtSingle,
               dtReal48,
               dtDouble,
               dtExtended,
               dtString,
               dtBit);// TDataType
const
  cDataType: Array[TDataType] of string = ('Byte',
                                           'Shortint',
                                           'Word',
                                           'Smallint',
                                           'DWord',
                                           'Integer',
                                           'Int64',
                                           'Single',
                                           'Real48',
                                           'Double',
                                           'Extended',
                                           'String',
                                           'Bit');
type
  TBinFile = class(TObject)
  private
    FBasePointer: PByte;
    FFileName: string;
    FLen: cardinal;
    FColWidth: cardinal;
    function GetChar(aIndex: cardinal): Char;
  public
    constructor Create;virtual;
    destructor Destroy;override;

    procedure UnloadFile;
    function LoadFile(aFileName: string): boolean;
    function GetByte(aIndex: cardinal): Byte;
    function GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;
    function CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal; out aLengthDifference: cardinal): boolean;

    function GetInteger(aIndex: integer; aLength: integer; aSigned: boolean; aSwap: boolean): int64;
    function GetFloat(aIndex: integer; aLength: integer): extended;
    function GetString(aIndex: integer; aLength: integer): string;
    function GetBit(aIndex: integer; aLength: integer): string;

    property BasePointer: PByte read FBasePointer;
    property Length: cardinal read FLen;
    property FileName: string read FFileName;
    property ColWidth: cardinal read FColWidth write FColWidth;
    property Byte[aIndex: cardinal]:Byte read GetByte;
    property ASCII[aIndex: cardinal]:Char read GetChar;
  end;// TBinFile = class(TObject)

  TfrmMainWindow = class(TForm)
    mmToolBar1: TmmToolBar;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    mmPanel3: TmmPanel;
    mmPanel4: TmmPanel;
    mmPanel1: TmmPanel;
    mBinGrid1: TmmVirtualStringTree;
    mBinFile1Status: TmmStatusBar;
    mmPanel2: TmmPanel;
    mBinGrid2: TmmVirtualStringTree;
    mBinFile2Status: TmmStatusBar;
    mmActionList1: TmmActionList;
    acLoadFile1: TAction;
    acLoadFile2: TAction;
    acToggleHexDec: TAction;
    mButtonImages: TmmImageList;
    OpenDialog: TOpenDialog;
    mmSplitter1: TmmSplitter;
    mmSplitter2: TmmSplitter;
    mmLabel1: TmmLabel;
    laDifferenceCount: TmmStaticText;
    mmLabel2: TmmLabel;
    laLengthDifference: TmmStaticText;
    ToolButton4: TToolButton;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    mmSpeedButton3: TmmSpeedButton;
    mValueGrid: TmmStringGrid;
    mmLabel3: TmmLabel;
    cbSwap1: TmmCheckBox;
    cbSwap2: TmmCheckBox;
    mmPanel5: TmmPanel;
    edTitle: TmmEdit;
    procedure acLoadFile1Execute(Sender: TObject);
    procedure acLoadFile2Execute(Sender: TObject);
    procedure acToggleHexDecExecute(Sender: TObject);
    procedure mBinGrid1GetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure mBinGrid1Change(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure mBinGridAfterCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellRect: TRect);
    procedure mBinGridColumnClick(Sender: TBaseVirtualTree;
      Column: TColumnIndex; Shift: TShiftState);
    procedure mBinGridKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mBinGridBeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellRect: TRect);
    procedure cbSwapClick(Sender: TObject);
    procedure edTitleChange(Sender: TObject);
  private
    mBinFile1: TBinFile;
    mBinFile2: TBinFile;
    mDisplayState: TDisplayState;
    function LoadBinFileDialog(var aBinFile: TBinFile): boolean;

    function CreateCols(aTree: TmmVirtualStringTree): boolean;
    function ConfigureCols(aTree: TmmVirtualStringTree; aBinFile: TBinFile): boolean;
    procedure UpdateStatusbar(aBinFile: TBinFile; aStatusBar: TStatusBar; aGrid: TVirtualStringTree);
    procedure DetermineDifference;
    function IsDifferent(aIndex: cardinal): boolean;
    procedure Syncronize(aSender: TBaseVirtualTree);
    procedure UpdateValue;
    function GetSelectedIndex(aGrid: TVirtualStringTree): integer;

    { Private declarations }
  public
    constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    { Public declarations }
  end;

var
  frmMainWindow: TfrmMainWindow;

implementation

uses
  math, LoepfeGlobal;

{$R *.DFM}

const
  // 1 Splate f�r die Zeilenzahl, 16 Spalten f�r Hex und 1 Spalte f�r den Text
  cMaxTreeCol = 18;
  cDafaultColWidth = 35;
  cLineNumberColWidth = 40;
  cDezCols = 10;
  cHexCols = 16;

  // Statusbar
  cDisplayStatePanel = 0;
  cCoordinatePanel = 1;
  cValuePanel = 2;
  cFileNamePanel = 3;

  // Farben
  cDifferentColor = $008EBDFD;
  cMissingColor   = $00EEC19D;

(*---------------------------------------------------------
  TBinFile
----------------------------------------------------------*)
function TBinFile.CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal; out aLengthDifference: cardinal): boolean;
var
  xLength: cardinal;
  i: integer;
begin
  aDifferenceCount := 0;

  if assigned(aBinFile) then begin
    xLength := self.Length;
    if aBinFile.Length < xLength then
      xLength := aBinFile.Length;

    // Midestens der Unterschied in der L�nge der Files
    aLengthDifference := abs(self.Length - aBinFile.Length);

    for i := 0 to xLength - 1 do begin
      if Byte[i] <> aBinFile.Byte[i] then
        inc(aDifferenceCount)
    end;// for i := 0 to xLength - 1 do begin
  end;// if assigned(aBinFile) then begin
end;// function TBinFile.CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal): boolean;

constructor TBinFile.Create;
begin
  inherited;
  FBasePointer := nil;
end;

destructor TBinFile.Destroy;
begin
  UnloadFile;
  inherited;
end;

function TBinFile.GetBit(aIndex: integer; aLength: integer): string;
var
  i, j: integer;
  xByte: system.Byte;
  xTempByte: string;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xTempByte := '';
    xByte := Byte[aIndex + i];
    for j := 0 to 7 do begin
      if j = 4 then
        xTempByte := '-' + xTempByte;
      if (xByte and (1 shl j)) = (1 shl j) then
        xTempByte := '1' + xTempByte
      else
        xTempByte := '0' + xTempByte;
    end;// for j := 0 to 7 do begin
    result := result + ' ' + xTempByte;
  end;// for i := 0 to aLength - 1 do begin

  if system.Length(result) > 0  then
    system.delete(result, 1, 1);
end;

function TBinFile.GetByte(aIndex: cardinal): Byte;
begin
  result := 0;
  if aIndex < FLen then 
    result := PByte(cardinal(FBasePointer) + aIndex)^;
end;

function TBinFile.GetChar(aIndex: cardinal): Char;
var
  xByte: System.Byte;
begin
  result := '.';
  xByte := Byte[aIndex];
  if xByte >= ord(' ') then
    result := chr(xByte)
  else
    result := '.';
end;

function TBinFile.GetFloat(aIndex, aLength: integer): extended;
begin
  result := 0;
  if (aIndex + aLength) < self.Length then begin
    case aLength of
      4: result := PSingle(cardinal(FBasePointer) + aIndex)^;
      6: result := PReal48(cardinal(FBasePointer) + aIndex)^;
      8: result := PDouble(cardinal(FBasePointer) + aIndex)^;
      10: result := PExtended(cardinal(FBasePointer) + aIndex)^;
    end;// case aLength of
  end;// if (aIndex + aLength) < self.Length then begin
end;

function TBinFile.GetInteger(aIndex: integer; aLength: integer; aSigned: boolean; aSwap: boolean): int64;
begin
  result := 0;
  if (aIndex >= 0) and ((aIndex + aLength) < self.Length) then begin
    if aSigned then begin
      case aLength of
        1: result := PShortInt(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
            result := Swap(PSmallInt(cardinal(FBasePointer) + aIndex)^)
           else
            result := PSmallInt(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PInteger(cardinal(FBasePointer) + aIndex)^)
           else
             result := PInteger(cardinal(FBasePointer) + aIndex)^;
        8: result := PInt64(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end else begin
      case aLength of
        1: result := PByte(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
             result := swap(PWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PWord(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PDWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PDWord(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end;// if aSigned then begin
  end;// if (aIndex >= 0) and ((aIndex + aLength) < self.Length) then begin
end;

function TBinFile.GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;
begin
  result := '';
  if aIndex < FLen then begin
    case aDisplayState of
      dsHex:   result := IntToHex(Byte[aIndex], 2);
      dsDez:   result := IntToStr(Byte[aIndex]);
      dsASCII: result := ASCII[aIndex];
    end;// case mDisplayState of
  end;// if aIndex < FLen then
end;// function TBinFile.GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState): string;

function TfrmMainWindow.GetSelectedIndex(aGrid: TVirtualStringTree): integer;
var
  xColCount: Integer;
begin
  xColCount := cHexCols;
  case mDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez: xColCount := cDezCols;
    dsASCII:  xColCount := cDezCols;
  end;// case mDisplayState of
  result := aGrid.AbsoluteIndex(aGrid.FocusedNode) * xColCount + aGrid.FocusedColumn - 1;
end;

function TBinFile.GetString(aIndex, aLength: integer): string;
var
  i, j: integer;
  xByte: system.Byte;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xByte := Byte[aIndex + i];
    if xByte >= ord(' ') then
      result := result + chr(xByte)
    else
      result := result + '.';
  end;// for i := 0 to aLength - 1 do begin
end;

function TBinFile.LoadFile(aFileName: string): boolean;
var
  xStream: TFileStream;
  xPointer: Pointer;
begin
  result := false;
  UnloadFile;
  
  if FileExists(aFileName) then begin
    xStream := nil;
    try
      xStream := TFileStream.Create(aFileName, fmOpenRead	or fmShareDenyNone);
      xStream.Position := 0;
      try
        GetMem(FBasePointer, xStream.Size);
        FLen := xStream.Read(FBasePointer^, xStream.Size);
        result := true;
        FFileName := aFileName;
      except
        on e:EOutOfMemory do
          FBasePointer := nil;
      end;// try except
    finally
      if assigned(xStream) then
        xStream.Free;
    end;// try finally
  end;// if FileExists(aFileName) then begin
end;// TForm1.LoadBinFile cat:No category

procedure TBinFile.UnloadFile;
begin
  if assigned(FBasePointer) then
    FreeMem(FBasePointer);
  FBasePointer := nil;
end;

(*---------------------------------------------------------
  Main Window
----------------------------------------------------------*)
constructor TfrmMainWindow.Create(AOwner: TComponent);
var
  i: TDataType;
  x : integer;
begin
  inherited Create(aOwner);

  mDisplayState := dsHex;
  
  mBinFile1 := TBinFile.Create;
  mBinFile2 := TBinFile.Create;
  CreateCols(mBinGrid1);
  CreateCols(mBinGrid2);

  mBinFile1.LoadFile(ParamStr(1));
  mBinFile2.LoadFile(ParamStr(2));

  ConfigureCols(mBinGrid1, mBinFile1);
  ConfigureCols(mBinGrid2, mBinFile2);
  
  {
  if ParamCount > 0 then
    // Dateien laden
    if ParamCount = 1 then begin


    end else begin
       mBinFile2.LoadFile(ParamStr(2));

    end;
  }

  UpdateValue;

  DetermineDifference;

  // StringGrid beschriften
  mValueGrid.Cells[1,0] := 'Datei 1';
  mValueGrid.Cells[2,0] := 'Datei 2';
  for i := Low(TDataType) to High(TDataType) do
    mValueGrid.Cells[0, ord(i) + 1] := cDataType[i];

  mValueGrid.ColWidths[0] := 55;
  mValueGrid.ColWidths[1] := 140;
  mValueGrid.ColWidths[2] := 140;
end;// constructor TfrmMainWindow.Create(AOwner: TComponent);

destructor TfrmMainWindow.Destroy;
begin
  FreeAndNil(mBinFile1);
  FreeAndNil(mBinFile2);
  inherited;
end;// destructor TfrmMainWindow.Destroy;

procedure TfrmMainWindow.DetermineDifference;
var
  xDifferenceCount: cardinal;
  xLengthDifference: cardinal;
begin
  mBinFile1.CompareWith(mBinfile2, xDifferenceCount, xLengthDifference);
  laDifferenceCount.Caption := IntToStr(xDifferenceCount);
  laLengthDifference.Caption := IntToStr(xLengthDifference);
end;// procedure TfrmMainWindow.DetermineDifference;

function TfrmMainWindow.CreateCols(aTree: TmmVirtualStringTree): boolean;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin
  // Spalten f�r das Tree erzeugen (eine Spalte pro Methode)
  for i := 0 to cMaxTreeCol - 1 do begin
    xTreeCol := aTree.Header.Columns.Add;
    xTreeCol.Options := xTreeCol.Options - [coVisible];
    xTreeCol.Width := cDafaultColWidth;
  end;// for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin

  aTree.Header.Columns[0].Width := cLineNumberColWidth;
end;

function TfrmMainWindow.ConfigureCols(aTree: TmmVirtualStringTree; aBinFile: TBinFile): boolean;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin
  // Zuerst alle Spalten ausblenden
  for i := 0 to aTree.Header.Columns.Count - 1 do begin
    xTreeCol := aTree.Header.Columns[i];
    xTreeCol.Options := xTreeCol.Options - [coVisible];
  end;// for i := 0 to aTree.Header.Columns.Count - 1 do begin

  // Zuerst die Spalte f�r dem Zeichenindex des ersten Zeichens
  xTreeCol := aTree.Header.Columns[0];
  xTreeCol.Options := xTreeCol.Options + [coVisible];
  xTreeCol.Options := xTreeCol.Options - [coAllowClick, coEnabled];
  xTreeCol.Text := '#';

  // Dann die Spalten f�r die ersten 10 Bytes
  for i := 0 to 9 do begin
    xTreeCol := aTree.Header.Columns[i + 1];
    xTreeCol.Options := xTreeCol.Options + [coVisible];
    xTreeCol.Text := intToStr(i);
  end;// for i := 0 to 9 do begin

  // F�r die Hex Anzeige
  if mDisplayState = dsHex then begin
    // Hexadezimale Darstellung
    //  ... Die restlichen Spalten anzeigen
    for i := 10 to 15 do begin
      xTreeCol := aTree.Header.Columns[i + 1];
      xTreeCol.Options := xTreeCol.Options + [coVisible];
      xTreeCol.Text := intToHex(i,1);
    end;// for i := 10 to 15 do begin

    // Anzahl Zeilen definieren
    aTree.RootNodeCount := (aBinFile.Length div cHexCols);
  end else begin // if mDisplayState = dsHex then begin
    // Dezimale Darstellung
    // Anzahl Zeilen definieren
    aTree.RootNodeCount := (aBinFile.Length div cDezCols);
  end;// if mDisplayState = dsHex then begin
end;


function TfrmMainWindow.LoadBinFileDialog(var aBinFile: TBinFile):boolean;
begin
  result := false;
  with OpenDialog do begin
    if Execute then
      result := aBinFile.LoadFile(FileName);
  end;// with OpenDialog do begin

  DetermineDifference;
end;// function TfrmMainWindow.LoadBinFileDialog(var aBinFile: TBinFile):boolean;

procedure TfrmMainWindow.acLoadFile1Execute(Sender: TObject);
begin
  if not(LoadBinFileDialog(mBinFile1)) then
    ShowMessage('Datei wurde nicht geladen')
  else
    UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
end;// procedure TfrmMainWindow.acLoadFile1Execute(Sender: TObject);

procedure TfrmMainWindow.acLoadFile2Execute(Sender: TObject);
begin
  if not(LoadBinFileDialog(mBinFile2)) then
    ShowMessage('Datei wurde nicht geladen')
  else
    UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
end;// procedure TfrmMainWindow.acLoadFile2Execute(Sender: TObject);

procedure TfrmMainWindow.acToggleHexDecExecute(Sender: TObject);
begin
  if Sender is TSpeedButton then
    mDisplayState := TDisplayState(TSpeedButton(Sender).Tag);

  ConfigureCols(mBinGrid1, mBinFile1);
  ConfigureCols(mBinGrid2, mBinFile2);
end;// procedure TfrmMainWindow.acToggleHexDecExecute(Sender: TObject);

procedure TfrmMainWindow.mBinGrid1GetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  xBinfile: TBinFile;
  xColCount: cardinal;
begin
  if Column >= 1 then begin
    xBinFile := nil;

    // Zu verwendendes Bin�rfile
    If Sender = mBinGrid1 then
      xBinFile := mBinFile1;
    If Sender = mBinGrid2 then
      xBinFile := mBinFile2;

    xColCount := cHexCols;
    case mDisplayState of
      dsHex: xColCount := cHexCols;
      dsDez, dsASCII: xColCount := cDezCols;
    end;// case mDisplayState of

    if assigned(xBinFile) then
      CellText := xBinFile.GetNumberString(Sender.AbsoluteIndex(Node) * xColCount + (Column - 1), mDisplayState);
  end;// if Column >= 1 then begin
end;// procedure TfrmMainWindow.mBinGrid1GetText(...

procedure TfrmMainWindow.UpdateStatusbar(aBinFile: TBinFile; aStatusBar: TStatusBar; aGrid: TVirtualStringTree);
begin
  // DisplayModus
  case mDisplayState of
    dsHex:  aStatusBar.Panels[cDisplayStatePanel].Text := 'HEX';
    dsDez: aStatusBar.Panels[cDisplayStatePanel].Text := 'DEZ';
    dsASCII: aStatusBar.Panels[cDisplayStatePanel].Text := 'ASCII';
  end;// case mDisplayState of

  // Selektiertes Element
  if assigned(aGrid.FocusedNode) then begin
    // Index des selektierten Elements anzeigen
    aStatusBar.Panels[cCoordinatePanel].Text := Format('Index: $%s (%d)', [IntToHex(GetSelectedIndex(aGrid), 4), GetSelectedIndex(aGrid)]);

    // Wert des Elements
    with aBinFile do
      aStatusBar.Panels[cValuePanel].Text := Format('Wert: $%s (%s)', [GetNumberString(GetSelectedIndex(aGrid), dsHex), GetNumberString(GetSelectedIndex(aGrid), dsDez)]);

  end;// if assigned(aGrid.FocusedNode) then begin

  // Filename
  aStatusBar.Panels[cFileNamePanel].Text := aBinFile.FileName;
end;// procedure TfrmMainWindow.UpdateStatusbar(aGrid: TVirtualStringTree);

procedure TfrmMainWindow.mBinGrid1Change(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  UpdateValue;
end;


procedure TfrmMainWindow.UpdateValue;
var
  xTemp: string;
  xIndex1: integer;
  xIndex2: integer;
  xSwap1: boolean;
  xSwap2: boolean;
begin
  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);

  xIndex1 := GetSelectedIndex(mBinGrid1);
  xIndex2 := GetSelectedIndex(mBinGrid2);

  xSwap1 := cbSwap1.Checked;
  xSwap2 := cbSwap2.Checked;

  with mValueGrid do begin
    Cells[1, ord(dtByte) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 1, false, xSwap1));
    Cells[2, ord(dtByte) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 1, false, xSwap2));

    Cells[1, ord(dtshortInt) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 1, true, xSwap1));
    Cells[2, ord(dtshortInt) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 1, true, xSwap2));

    Cells[1, ord(dtWord) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 2, false, xSwap1));
    Cells[2, ord(dtWord) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 2, false, xSwap2));

    Cells[1, ord(dtSmallInt) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 2, true, xSwap1));
    Cells[2, ord(dtSmallInt) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 2, true, xSwap2));

    Cells[1, ord(dtDWord) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 4, false, xSwap1));
    Cells[2, ord(dtDWord) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 4, false, xSwap2));

    Cells[1, ord(dtInteger) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 4, true, xSwap1));
    Cells[2, ord(dtInteger) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 4, true, xSwap2));

    Cells[1, ord(dtInt64) + 1] := intToStr(mBinFile1.GetInteger(xIndex1, 8, true, xSwap1));
    Cells[2, ord(dtInt64) + 1] := intToStr(mBinFile2.GetInteger(xIndex2, 8, true, xSwap2));

    Cells[1, ord(dtSingle) + 1] := FloatToStr(mBinFile1.GetFloat(xIndex1, 4));
    Cells[2, ord(dtSingle) + 1] := FloatToStr(mBinFile2.GetFloat(xIndex2, 4));

    Cells[1, ord(dtReal48) + 1] := FloatToStr(mBinFile1.GetFloat(xIndex1, 6));
    Cells[2, ord(dtReal48) + 1] := FloatToStr(mBinFile2.GetFloat(xIndex2, 6));

    Cells[1, ord(dtDouble) + 1] := FloatToStr(mBinFile1.GetFloat(xIndex1, 8));
    Cells[2, ord(dtDouble) + 1] := FloatToStr(mBinFile2.GetFloat(xIndex2, 8));

    try
      Cells[1, ord(dtExtended) + 1] := FloatToStr(mBinFile1.GetFloat(xIndex1, 10));
    except
      Cells[1, ord(dtExtended) + 1] := '-'
    end;
    try
      Cells[2, ord(dtExtended) + 1] := FloatToStr(mBinFile2.GetFloat(xIndex2, 10));
    except
      Cells[2, ord(dtExtended) + 1] := '-';
    end;

    Cells[1, ord(dtString) + 1] := mBinFile1.GetString(xIndex1, 30);
    Cells[2, ord(dtString) + 1] := mBinFile2.GetString(xIndex2, 30);

    if xSwap1 then begin
      xTemp := mBinFile1.GetBit(xIndex1, 2);
      if Length(xTemp) >= 16 then
        Cells[1, ord(dtBit) + 1] := copy(xTemp, 11, 9) + ' ' + copy(xTemp, 1,9)
      else
        Cells[1, ord(dtBit) + 1] := 'Fehler';
    end else
      Cells[1, ord(dtBit) + 1] := mBinFile1.GetBit(xIndex1, 2);

    if xSwap2 then begin
      xTemp := mBinFile2.GetBit(xIndex2, 2);
      if Length(xTemp) >= 16 then
        Cells[2, ord(dtBit) + 1] := copy(xTemp, 11, 9) + ' ' + copy(xTemp, 1,9)
      else
        Cells[2, ord(dtBit) + 1] := 'Fehler';
    end else
      Cells[2, ord(dtBit) + 1] := mBinFile2.GetBit(xIndex2, 2);
  end;// with mValueGrid do begin
end;

procedure TfrmMainWindow.mBinGridAfterCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellRect: TRect);
var
  xTree: TVirtualStringTree;
  xLineIndex: string;
begin

  if Column = 0 then begin
    if Sender is TVirtualStringTree then begin
      xTree := TVirtualStringTree(Sender);
      with TargetCanvas do begin
        // Simuliert die Anzeige einer FixedRow wie in TStringGrid
        if toShowVertGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Right);
        if toShowHorzGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Bottom);
        // Zeichnet eine 3D Fl�che
        DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);

        xLineIndex := '';
        case mDisplayState of
          dsHex : xLineIndex :=  IntToHex(Sender.AbsoluteIndex(Node) * cHexCols, 4);
          dsDez, dsASCII : xLineIndex :=  IntToStr(Sender.AbsoluteIndex(Node) * cDezCols);
        end;// case mDisplayState of

        TargetCanvas.TextOut(CellRect.Left, CellRect.Top, xLineIndex);
      end;// with TargetCanvas do begin
    end;// if Sender is TVirtualStringTree then begin
  end;// if Column = 0 then begin
end;

procedure TfrmMainWindow.mBinGridColumnClick(Sender: TBaseVirtualTree;
  Column: TColumnIndex; Shift: TShiftState);
begin
  UpdateValue;
{
  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);}
  Syncronize(Sender);
end;

procedure TfrmMainWindow.mBinGridKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key in [VK_HOME, VK_END, VK_PRIOR, VK_NEXT, VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_BACK, VK_TAB] then begin
    UpdateValue;
{    UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
    UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);}
    if Sender is TBaseVirtualTree then
      Syncronize(TBaseVirtualTree(Sender));
  end;// if Key in [VK_HOME, VK_END, VK_PRIOR, VK_NEXT, VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, VK_BACK, VK_TAB] then begin
end;

procedure TfrmMainWindow.Syncronize(aSender: TBaseVirtualTree);
var
  xSyncGrid: TBaseVirtualTree;
  xNodeIndex: cardinal;
  xColIndex: cardinal;
  xNode: PVirtualNode;
begin
  if aSender = mBinGrid1 then
    xSyncGrid := mBinGrid2
  else
    xSyncGrid := mBinGrid1;

  xNodeIndex := aSender.AbsoluteIndex(aSender.FocusedNode);
  xColIndex  := aSender.FocusedColumn;

  // AlteSelektion l�schen
  xSyncGrid.ClearSelection;

  // Zeile markieren
  xNode := xSyncGrid.GetFirst;
  while assigned(xNode) do begin
    if xSyncGrid.AbsoluteIndex(xNode) = xNodeIndex then begin
      xSyncGrid.Selected[xNode] := true;
      xSyncGrid.FocusedNode := xNode;
      xNode := nil;
    end;// if xSyncGrid.AbsoluteIndex(xNode) = xNodeIndex then begin
    xNode := xSyncGrid.GetnextSibling(xNode);
  end;// while assigned(xNode) do begin

  xSyncGrid.FocusedColumn := xColIndex;

  UpdateValue;
{  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);}
end;// procedure TfrmMainWindow.Syncronize;

procedure TfrmMainWindow.mBinGridBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellRect: TRect);
var
  xColCount: cardinal;
  xIndex: integer;
begin
  xColCount := cHexCols;
  case mDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez, dsASCII: xColCount := cDezCols;
  end;// case mDisplayState of

  xIndex := Sender.AbsoluteIndex(Node) * xColCount + (Column - 1);
  if IsDifferent(xIndex) then begin
    // Bytes sind unterschiedlich
    TargetCanvas.Brush.Color := cDifferentColor;
    TargetCanvas.FillRect(CellRect);
  end else begin
    // Entweder identisch, oder in einem File nicht vorhanden
    if (xIndex >= mBinFile1.Length) or (xIndex >= mBinFile2.Length) then begin
      // In einem File nicht vorhanden
      TargetCanvas.Brush.Color := cMissingColor;
      TargetCanvas.FillRect(CellRect);
    end;// if (xIndex >= mBinFile1.Length) or (xIndex >= mBinFile2.Length) then begin
  end;// if IsDifferent(Sender.AbsoluteIndex(Node) * xColCount + (Column - 1)) then begin
end;

function TfrmMainWindow.IsDifferent(aIndex: cardinal): boolean;
begin
  result := false;
  if (aIndex < mBinFile1.Length) and (aIndex < mBinFile2.Length) then
    result := mBinFile1.Byte[aIndex] <> mBinFile2.Byte[aIndex]
end;

procedure TfrmMainWindow.cbSwapClick(Sender: TObject);
begin
  UpdateValue;
end;

procedure TfrmMainWindow.edTitleChange(Sender: TObject);
begin
  Caption := edTitle.Text;
  Application.Title := edTitle.Text;
end;

end.
