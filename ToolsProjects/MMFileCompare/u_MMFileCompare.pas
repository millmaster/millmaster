{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMFileCompare.pas
| Projectpart...: MillMaster 
| Subpart.......: -
| Process(es)...: -
| Description...: Main Programm MMFileComapre
|                 Func. : Vergleichen eines oder mehere Dateinen mit einem
|                         Masterfile. Unterschiede werden farbig dargestellt.
| Info..........: Uebername aus BinDiff
| Develop.system: W2k
| Target.system.: W2k, XP, W2k3
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2005  1.00  SDO | File created
| 11.08.2005  1.01  SDO | Dateien mit Commandozeile einlesen; Bsp. MMFileCompare.exe "Memory A.xml" "Memory Q.xml"
|==============================================================================}
unit u_MMFileCompare;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Menus, mmPopupMenu, ImgList, mmImageList, ActnList,
  mmActionList, mmEdit, ExtCtrls, mmPanel, Buttons, mmSpeedButton, ToolWin,
  mmToolBar, IvMlDlgs, mmOpenDialog, mmStatusBar, VirtualTrees,
  mmVirtualStringTree, mmSplitter, mmCheckBox, Grids, mmStringGrid,
  mmStaticText, mmLabel, mmTabControl, AdvGrid, mmExtStringGrid,
  u_MMFileCompareGlobal, ShellAPI, ActiveX, mmLineLabel;

type
  TMain_MMFileComapre = class(TForm)
    mmToolBar1: TmmToolBar;
    tbExit: TToolButton;
    ToolButton4: TToolButton;
    spHex: TmmSpeedButton;
    spDez: TmmSpeedButton;
    spAscii: TmmSpeedButton;
    mmPanel5: TmmPanel;
    edTitle: TEdit;
    mmActionList1: TmmActionList;
    acToggleHexDec: TAction;
    mButtonImages: TmmImageList;
    tbOpen: TToolButton;
    acOpen: TAction;
    acOpenMasterFile: TAction;
    acOpenCompareFile: TAction;
    acExit: TAction;
    pmOpenFiles: TmmPopupMenu;
    OpenMasterFile1: TMenuItem;
    OpenCompareFile1: TMenuItem;
    OpenDialog: TmmOpenDialog;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    mmSplitter2: TmmSplitter;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    pMaster: TmmPanel;
    mmPanel4: TmmPanel;
    mBinFile2Status: TmmStatusBar;
    mBinFile1Status: TmmStatusBar;
    mValueGrid: TmmExtStringGrid;
    sgFileInfo: TmmExtStringGrid;
    mmSplitter1: TmmSplitter;
    Panel1: TPanel;
    Panel2: TPanel;
    tbcCompareFiles: TmmTabControl;
    mBinGrid2: TmmVirtualStringTree;
    sbClearMaster: TmmSpeedButton;
    mmTabControl1: TmmTabControl;
    mBinGrid1: TmmVirtualStringTree;
    sbMoveToComp: TmmSpeedButton;
    sbClearComp: TmmSpeedButton;
    sbClearAllComp: TmmSpeedButton;
    sbMoveToMaster: TmmSpeedButton;
    cbSwap1: TmmCheckBox;
    cbSwap2: TmmCheckBox;
    mmLineLabel1: TmmLineLabel;
    mmLineLabel2: TmmLineLabel;
    mmActionList2: TmmActionList;
    acClearMaster: TAction;
    acMasterToCompareFiles: TAction;
    ilButtonsForFile: TmmImageList;
    acMoveToMaster: TAction;
    acClearAllComp: TAction;
    acClearActiveComp: TAction;
    lbDifferentColor: TmmLabel;
    pDifferentColor: TmmPanel;
    lbMissingColor: TmmLabel;
    pMissingColor: TmmPanel;
    pIdentical: TmmPanel;
    lbIdentical: TmmLabel;

    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);

    procedure acExitExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acOpenMasterFileExecute(Sender: TObject);
    procedure acOpenCompareFileExecute(Sender: TObject);
    procedure edTitleChange(Sender: TObject);
    procedure tbOpenContextPopup(Sender: TObject; MousePos: TPoint;
      var Handled: Boolean);

    procedure acToggleHexDecExecute(Sender: TObject);
    procedure tbcCompareFilesChange(Sender: TObject);
    procedure mBinGrid2BeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellRect: TRect);
    procedure mBinGrid2AfterCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellRect: TRect);
    procedure mBinGrid2Change(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure mBinGrid2GetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);  
    procedure sgFileInfoGridHint(Sender: TObject; Arow, Acol: Integer;
      var hintstr: String);
    procedure mBinGrid1ColumnClick(Sender: TBaseVirtualTree;
      Column: TColumnIndex; Shift: TShiftState);
    procedure mBinGrid2ColumnClick(Sender: TBaseVirtualTree;
      Column: TColumnIndex; Shift: TShiftState);
    procedure cbSwap1Click(Sender: TObject);
    procedure acClearMasterExecute(Sender: TObject);
    procedure acMasterToCompareFilesExecute(Sender: TObject);
    procedure acMoveToMasterExecute(Sender: TObject);
    procedure acClearAllCompExecute(Sender: TObject);
    procedure acClearActiveCompExecute(Sender: TObject);
    procedure sgFileInfoClickCell(Sender: TObject; Arow, Acol: Integer);
    procedure sgFileInfoKeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure mBinGrid1Scroll(Sender: TBaseVirtualTree; DeltaX,
      DeltaY: Integer);
    procedure mBinGrid2KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    mBinFile1 : TBinFile;
    mBinFile2 : TBinFile;
    mFileList : TFileList;

    mDisplayState: TDisplayState;

    //mClaerCol1, mClaerCol2 : Boolean;

    //Read Drop Files (von Explorer)
    procedure WMDropFiles(var aMsg: TMessage);
    procedure BinGrid1WindowProc(var Message: TMessage);
    procedure BinGrid2WindowProc(var Message: TMessage);


    procedure CheckMasterFile(aFile : String);
    procedure MsgFileExists(aFile : String);

    //Grid Func. -> Datenverarbeitung
    function CreateCols(aTree: TmmVirtualStringTree): boolean;
    procedure UpdateValue;
    function GetSelectedIndex(aGrid: TVirtualStringTree): integer;
    function ConfigureCols(aTree: TmmVirtualStringTree; aBinFile: TBinFile): boolean;
    procedure Syncronize(aSender: TBaseVirtualTree);
    function IsDifferent(aIndex: cardinal): boolean;
    procedure UpdateStatusbar(aBinFile: TBinFile; aStatusBar: TStatusBar; aGrid: TVirtualStringTree);

    procedure MoveFileToMaster;
    procedure MasterToCompareFiles;
    procedure DeleteFile(aTree: TmmVirtualStringTree; aBinFile: TBinFile);
    procedure DetermineDifference;

    procedure NavigateGrid(aRow : Integer);

  protected
  public
    { Public declarations }
  end;


var
  Main_MMFileComapre: TMain_MMFileComapre;


const cFileNotExists = '%s "%s" does not exists !';

implementation

{$R *.DFM}
uses  JCLFileUtils;


var
  mOldBinGrid1WindowProc: TWndMethod;
  mOldBinGrid2WindowProc: TWndMethod;


//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.FormCreate(Sender: TObject);
var i: TDataType;
    x, xIndex, xParamID, xErrorCount : Integer;
    xFileName, xParamFile, xMSG, xMsg1 : string;
begin
  edTitle.OnChange(Self);

  mFileList  := TFileList.Create;


  mBinFile1:= NIL;
  mBinFile2:= NIL;

  mBinFile1 := TBinFile.Create;
  mBinFile2 := TBinFile.Create;


  mOldBinGrid1WindowProc := mBinGrid1.WindowProc;
  mBinGrid1.WindowProc   := BinGrid1WindowProc;
  DragAcceptFiles(mBinGrid1.Handle, True);


  mOldBinGrid2WindowProc := mBinGrid2.WindowProc;
  mBinGrid2.WindowProc   := BinGrid2WindowProc;
  DragAcceptFiles(mBinGrid2.Handle, True);


  tbcCompareFiles.Tabs.Clear;

  mDisplayState := dsHex;

  CreateCols(mBinGrid1);
  CreateCols(mBinGrid2);


  //ConfigureCols(mBinGrid1, mBinFile1);
  //ConfigureCols(mBinGrid2, mBinFile2);


  // StringGrid beschriften
  mValueGrid.Cells[0,0] :=  #10#13 + 'Werte';
  mValueGrid.Cells[1,0] := cFile1;
  mValueGrid.Cells[2,0] := cFile2;
  for i := Low(TDataType) to High(TDataType) do
    mValueGrid.Cells[0, ord(i) + 1] := cDataType[i];

  mValueGrid.ColWidths[0] := 55;
  mValueGrid.ColWidths[1] := 140;
  mValueGrid.ColWidths[2] := 140;

  sgFileInfo.ColCount:= 5;
  sgFileInfo.HideColumns(3,5);
  sgFileInfo.RowCount := 2;


  xIndex :=0;
  xErrorCount := 0;
  //Dateien aus Command Zeile auslesen
  for xParamID := 1 to ParamCount do begin
      xParamFile := ParamStr(xParamID);
      if xParamID = 1 then
         xMsg := 'Master file'
      else
         xMsg := 'File';

      //File exists check
      if FileExists(xParamFile) then begin
          if xParamID = 1 then begin
              //Command Master File einlesen
             if xParamFile <> '' then begin
                CheckMasterFile(xParamFile);
                ConfigureCols(mBinGrid1, mBinFile1);
                UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);

                mFileList.MasterFile := xParamFile;
             end else
                ConfigureCols(mBinGrid1, mBinFile1);
          end else begin
             //Command Comaper File(s) einlesen
             xFileName := xParamFile;
             mFileList.AddFile(xFileName);
             mFileList.BinFileByName(xFileName).TabIndex := xIndex;


             //Filename zu TabSheet
             xFileName := ExtractFileName(xFileName);
             tbcCompareFiles.Tabs.Add(xFileName);

             {
             if (xParamID = ParamCount) then begin
                //1. Compare File auf 1. Tabsheet setzen
                if mFileList.MasterFile = '' then
                   mBinFile2.AssignFile( mFileList.BinFile[0] )
                else
                   mBinFile2.AssignFile( mFileList.BinFile[1] );

                ConfigureCols(mBinGrid2, mBinFile2);
                UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
                tbcCompareFiles.TabIndex := 0;
             end;
             }
             Inc(xIndex);
          end;

      end else begin
          xMsg1 := Format(cFileNotExists,[xMsg, xParamFile]);
          MessageDlg(xMsg1, mtWarning	,  [mbOk], 0);
      end;
  end; //END for

  //1.Comapre File im Grind anzeigen (File dem Grid zuweisen)
  if ParamCount > 1 then
     if (xParamID-1 = ParamCount) and (mFileList.Count > 1)then begin
         mBinFile2.AssignFile( mFileList.BinFile[1] );
         ConfigureCols(mBinGrid2, mBinFile2);
         UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
         tbcCompareFiles.TabIndex := 0;
      end;


  UpdateValue;
  DetermineDifference;


  pDifferentColor.Color := cDifferentColor;
  pMissingColor.Color   := cMissingColor;
  pIdentical.Color      := clWhite;


end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.FormDestroy(Sender: TObject);
begin

 mBinGrid1.WindowProc := mOldBinGrid1WindowProc;
 DragAcceptFiles(mBinGrid1.Handle, False);


 mBinGrid2.WindowProc := mOldBinGrid2WindowProc;
 DragAcceptFiles(mBinGrid2.Handle, False);

 try
   mBinFile1.Free;
   mBinFile2.Free;
 finally
 end;
  mFileList.Free;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acExitExecute(Sender: TObject);
begin
  close;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acOpenExecute(Sender: TObject);
begin
 pmOpenFiles.Popup(self.left + tbOpen.Left + 3,  self.Top + tbOpen.Top + tbOpen.Height + 27);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acOpenMasterFileExecute(Sender: TObject);
begin
  with OpenDialog do begin
    Title := 'Master Datei �ffnen';

    Options := Options - [ ofAllowMultiSelect ];
    if Execute then begin
       CheckMasterFile(FileName);
       ConfigureCols(mBinGrid1, mBinFile1);
       UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
       UpdateValue;
       DetermineDifference;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acOpenCompareFileExecute(Sender: TObject);
var xIndex, x : Integer;
    xFileName : String;

begin
  with OpenDialog do begin
    Title := 'Compare Dateien �ffnen';
    Options := Options + [ ofAllowMultiSelect ];
    if Execute then begin

//       ShowMessage( Files.CommaText );
       for x:= 0 to Files.Count -1 do begin
          mFileList.AddFile(Files.Strings[x]);
       end;


//       mFileList.AddFile(FileName);

       xIndex := 0;
       tbcCompareFiles.Tabs.Clear;
       for x:= 0 to  mFileList.Count - 1 do begin
           xFileName := mFileList.BinFile[x].FileName;

           if CompareText( xFileName, mFileList.MasterFile ) <> 0 then begin
              //Comapre Files zum Register (Tabs) hinzufuegen
              xFileName := ExtractFileName(xFileName);
              tbcCompareFiles.Tabs.Add(xFileName);
              mFileList.BinFile[x].TabIndex := xIndex;
              inc(xIndex);
           end;
       end;
       tbcCompareFiles.OnChange(Self);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.edTitleChange(Sender: TObject);
begin
  Caption := Format('%s  [ %s ]',[cMainCaption, edTitle.Text]);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.tbOpenContextPopup(Sender: TObject;
  MousePos: TPoint; var Handled: Boolean);
begin
end;
//------------------------------------------------------------------------------





//******************************************************************************
//Drop Files einlesen
//******************************************************************************
procedure TMain_MMFileComapre.WMDropFiles(var aMsg: TMessage);
var
  xFileName: PChar;
  i, xSize, xFileCount: integer;
  xFile, xTxt, xDropfile : String;
  xFileExists : Boolean;
begin

  xFileName := '';
  xFileCount := DragQueryFile(aMsg.wParam, $FFFFFFFF, xFileName, 255);
  if (aMsg.LParamLo = 1) and (xFileCount > 1) then begin
     ShowMessage('More than 1 File. not allowed!');
     exit;
  end;

  for i := 0 to xFileCount - 1 do  begin
      xSize := DragQueryFile(aMsg.wParam, i, nil, 0) + 1;
      xFileName := StrAlloc(xSize);
      DragQueryFile(aMsg.wParam, i, xFileName, xSize);
      xDropfile := xFileName;


      if FileExists(xDropfile) then begin
         xFileExists :=  mFileList.ExistsFileInList(xDropfile);

         //Null Byte Files duerfen nicht eingelesen werden
         if FileGetSize(xDropfile) = 0 then begin
            xTxt := Format('Zero byte file not allowed!' + #10#13 + '%s',[xDropfile] );
            MessageDlg(xTxt, mtError, [mbOK], 0);
            DragFinish(aMsg.wParam);
            break;
         end;

         //Add ; Master-File zur FileListe zufuegen
         if aMsg.LParamLo = 1 then begin

            CheckMasterFile(xDropfile);
{
            if mFileList.MasterFile <> '' then begin
               xFile := xDropfile;
               if CompareText(xDropfile, mFileList.MasterFile) = 0 then begin
                  xTxt := Format('Would you reload ''%s'' ?',[xFile]);
                  if MessageDlg(xTxt, mtConfirmation	, [mbYes, mbNo], 0) = mrYes then begin
                     mFileList.BinFileByName( mFileList.MasterFile ).LoadFile(xDropfile);
                  end else
                     xDropfile := mFileList.MasterFile;
               end else begin
                  if xFileExists then begin
                     MsgFileExists(xDropfile);
                     xDropfile := mFileList.MasterFile;
                  end else begin
                     xTxt := Format('Would you replace ''%s'' ' + #13#10 + 'with ''%s'' ?',[xFile, mFileList.MasterFile]);
                     if MessageDlg(xTxt, mtConfirmation	, [mbYes, mbNo], 0) = mrYes then begin
                        mFileList.BinFileByName( mFileList.MasterFile ).LoadFile(xDropfile);
                     end else
                        xDropfile := mFileList.MasterFile;
                  end;
               end;
            end else
               if not xFileExists then
                  mFileList.AddFile(xDropfile)
               else begin
                  MsgFileExists(xDropfile);
               end;

            if not xFileExists then begin
               mBinFile1.AssignFile(mFileList.BinFileByName(xDropfile));
               mBinFile1.TabIndex := -1;
               mFileList.MasterFile:= xDropfile;

               mmTabControl1.Tabs.Clear;
               mmTabControl1.Tabs.Add( Format(cMasterTabFile,[ ExtractFileName(xDropfile)  ]) );
            end;
 }
         end else begin
            //ADD Compare-Files zur FileListe zufuegen
            mFileList.AddFile(xDropfile);
         end;
      end;//END FileExists()

      StrDispose(xFileName);
  end;
  DragFinish(aMsg.wParam);
  //DetermineDifference;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Drop File auf BinGrid1 akzeptieren (nur ein File erlaubt) -> Master-File
//******************************************************************************
procedure TMain_MMFileComapre.BinGrid1WindowProc(var Message: TMessage);
begin
  if Message.Msg = WM_DROPFILES then begin
    Message.LParamLo := 1;
    mBinGrid1.Clear;

    WMDROPFILES(Message);

    ConfigureCols(mBinGrid1, mBinFile1);
    UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
    UpdateValue;
    DetermineDifference;
  end;


  mOldBinGrid1WindowProc(Message);
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Drop Files auf BinGrid2 akzeptieren  -> Comapre-Files
//******************************************************************************
procedure TMain_MMFileComapre.BinGrid2WindowProc(var Message: TMessage);
var x, xIndex: Integer;
    xFileName : String;
begin
  if Message.Msg = WM_DROPFILES then begin
    Message.LParamLo := 2;
    mBinGrid2.Clear;
    WMDROPFILES(Message);

    xIndex := 0;
    tbcCompareFiles.Tabs.Clear;
    for x:= 0 to  mFileList.Count - 1 do begin
        xFileName := mFileList.BinFile[x].FileName;

        if CompareText( xFileName, mFileList.MasterFile ) <> 0 then begin
           //Comapre Files zum Register (Tabs) hinzufuegen
           xFileName := ExtractFileName(xFileName);
           tbcCompareFiles.Tabs.Add(xFileName);
           mFileList.BinFile[x].TabIndex := xIndex;
           inc(xIndex);
        end;
    end;
    tbcCompareFiles.OnChange(Self);
  end;

  mOldBinGrid2WindowProc(Message);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.tbcCompareFilesChange(Sender: TObject);
var x : integer;
begin
  for x:= 0 to mFileList.Count - 1 do
      if mFileList.BinFile[x].TabIndex = tbcCompareFiles.TabIndex then begin
         mBinGrid2.Clear;
         mBinFile2.AssignFile(mFileList.BinFile[x]);
         break;
      end;

  ConfigureCols(mBinGrid2, mBinFile2);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
  UpdateValue;
  DetermineDifference;
end;
//------------------------------------------------------------------------------
function TMain_MMFileComapre.CreateCols(
  aTree: TmmVirtualStringTree): boolean;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin
  // Spalten f�r das Tree erzeugen (eine Spalte pro Methode)
  for i := 0 to cMaxTreeCol - 1 do begin
    xTreeCol := aTree.Header.Columns.Add;
    xTreeCol.Options := xTreeCol.Options - [coVisible];
    xTreeCol.Width := cDafaultColWidth;
  end;// for i := Low(TCalculationMethod) to High(TCalculationMethod) do begin

  aTree.Header.Columns[0].Width := cLineNumberColWidth;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.UpdateValue;
var
  xTemp: string;
  xIndex1: integer;
  xIndex2: integer;
  xSwap1: boolean;
  xSwap2: boolean;
  xExtended : Extended;

  xSelCell1, xSelCell2 : integer;

  xMaxCells1,   xMaxCells2 : Integer;
  xSelCellOverflow1, xSelCellOverflow2 : Boolean;

  procedure ShowValueGrid(aCol: Integer; aBinFile : TBinFile ; aIndex: integer; aSwap : boolean);
  begin
    try
      with mValueGrid do begin
          if Assigned(aBinFile) and (aIndex >= 0) then begin

              Cells[aCol, ord(dtByte) + 1]     := intToStr(aBinFile.GetInteger(aIndex, 1, FALSE, aSwap));
              Cells[aCol, ord(dtshortInt) + 1] := intToStr(aBinFile.GetInteger(aIndex, 1, TRUE, aSwap));
              Cells[aCol, ord(dtWord) + 1]     := intToStr(aBinFile.GetInteger(aIndex, 2, FALSE, aSwap));
              Cells[aCol, ord(dtSmallInt) + 1] := intToStr(aBinFile.GetInteger(aIndex, 2, TRUE, aSwap));
              Cells[aCol, ord(dtDWord) + 1]    := intToStr(aBinFile.GetInteger(aIndex, 4, FALSE, aSwap));
              Cells[aCol, ord(dtInteger) + 1]  := intToStr(aBinFile.GetInteger(aIndex, 4, TRUE, aSwap));
              Cells[aCol, ord(dtInt64) + 1]    := intToStr(aBinFile.GetInteger(aIndex, 8, TRUE, aSwap));
              Cells[aCol, ord(dtSingle) + 1]   := FloatToStr(aBinFile.GetFloat(aIndex, 4));
              Cells[aCol, ord(dtReal48) + 1]   := FloatToStr(aBinFile.GetFloat(aIndex, 6));
              Cells[aCol, ord(dtDouble) + 1]   := FloatToStr(aBinFile.GetFloat(aIndex, 8));    

              {
              try
                Cells[aCol, ord(dtExtended) + 1] := FloatToStr( aBinFile.GetFloat(aIndex, 10));
              except
                Cells[aCol, ord(dtExtended) + 1] := '-'
              end;

              Cells[aCol, ord(dtString) + 1] := aBinFile.GetString(aIndex, 30);
              }

              if aSwap then begin
                xTemp := aBinFile.GetBit(aIndex, 2);
                if Length(xTemp) >= 16 then
                  Cells[aCol, ord(dtBit) + 1] := copy(xTemp, 11, 9) + ' ' + copy(xTemp, 1,9)
                else
                  Cells[aCol, ord(dtBit) + 1] := 'Fehler';
              end else
                Cells[aCol, ord(dtBit) + 1] := aBinFile.GetBit(aIndex, 2);

          end;
      end;//END With
    except
    end;
  end;
begin

  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);

  xIndex1 := GetSelectedIndex(mBinGrid1);
  xIndex2 := GetSelectedIndex(mBinGrid2);

  xSwap1 := cbSwap1.Checked;
  xSwap2 := cbSwap2.Checked;

  ShowValueGrid(1, mBinFile1, xIndex1, xSwap1);
  ShowValueGrid(2, mBinFile2, xIndex2, xSwap2);

  xSelCellOverflow1 := FALSE;
  xSelCellOverflow2 := FALSE;

  //Check: Ist eine selektierte BinGrid-Zelle ausserhalb des Zellbereiches
  if Assigned(mBinGrid1.FocusedNode) and  Assigned(mBinGrid2.FocusedNode) then begin

     xSelCell1 := GetSelectedIndex(mBinGrid1);
     xSelCell2 := GetSelectedIndex(mBinGrid2);

//     xMaxCells1 := (mBinGrid1.TotalCount) *  (mBinGrid1.Header.Columns.Count-2);
//     xMaxCells2 := (mBinGrid2.TotalCount) *  (mBinGrid2.Header.Columns.Count-2);

     xMaxCells1 := mBinFile1.Length-1;
     xMaxCells2 := mBinFile2.Length-1;

     if (xMaxCells1 < xMaxCells2) or (xSelCell1 >  xMaxCells1) then begin
        //Master-Zelle ist ausserhalb
        if (xSelCell1 < xSelCell2) or (xSelCell1 >  xMaxCells1) then
            xSelCellOverflow1 := TRUE;

     end else if (xMaxCells1 > xMaxCells2)  or (xSelCell2 > xMaxCells2) then begin
         //Compare-Zelle ist ausserhalb
         if (xSelCell1 > xSelCell2) or (xSelCell2 > xMaxCells2) then
            xSelCellOverflow2 := TRUE;
     end;
  end;

  //Col Master-Werte loeschen
  if xSelCellOverflow1 then
     mValueGrid.ClearRect(1,1, 1, mValueGrid.RowCount - mValueGrid.FixedRows);

  //Col Compare-Werte loeschen
  if xSelCellOverflow2 then
     mValueGrid.ClearRect(2,1, 2,mValueGrid.RowCount - mValueGrid.FixedRows);

end;
//------------------------------------------------------------------------------
function TMain_MMFileComapre.GetSelectedIndex(
  aGrid: TVirtualStringTree): integer;
var
  xColCount: Integer;
begin
  xColCount := cHexCols;
  case mDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez: xColCount := cDezCols;
    dsASCII:  xColCount := cDezCols;
  end;// case mDisplayState of
  try
    result := aGrid.AbsoluteIndex(aGrid.FocusedNode) * xColCount + aGrid.FocusedColumn - 1;
  except
  end;
end;
//------------------------------------------------------------------------------
function TMain_MMFileComapre.ConfigureCols(aTree: TmmVirtualStringTree;
  aBinFile: TBinFile): boolean;
var
  xTreeCol: TVirtualTreeColumn;
  i: integer;
begin

  // Zuerst alle Spalten ausblenden
  for i := 0 to aTree.Header.Columns.Count - 1 do begin
    xTreeCol := aTree.Header.Columns[i];
    xTreeCol.Options := xTreeCol.Options - [coVisible];
  end;// for i := 0 to aTree.Header.Columns.Count - 1 do begin

  // Zuerst die Spalte f�r dem Zeichenindex des ersten Zeichens
  xTreeCol := aTree.Header.Columns[0];
  xTreeCol.Options := xTreeCol.Options + [coVisible];
  xTreeCol.Options := xTreeCol.Options - [coAllowClick, coEnabled];
  xTreeCol.Text := '#';

  // Dann die Spalten f�r die ersten 10 Bytes
  for i := 0 to 9 do begin
    xTreeCol := aTree.Header.Columns[i + 1];
    xTreeCol.Options := xTreeCol.Options + [coVisible];
    xTreeCol.Text := intToStr(i);
  end;// for i := 0 to 9 do begin


  // F�r die Hex Anzeige
  if mDisplayState = dsHex then begin
    // Hexadezimale Darstellung
    //  ... Die restlichen Spalten anzeigen
    for i := 10 to 15 do begin
      xTreeCol := aTree.Header.Columns[i + 1];
      xTreeCol.Options := xTreeCol.Options + [coVisible];
      xTreeCol.Text := intToHex(i,1);
    end;// for i := 10 to 15 do begin

    // Anzahl Zeilen definieren
    if aBinFile <> NIL then
       if (aBinFile.Length Mod cHexCols) <> 0 then
          aTree.RootNodeCount := (aBinFile.Length div cHexCols) +1
       else
          aTree.RootNodeCount := (aBinFile.Length div cHexCols);
//       aTree.RootNodeCount := (aBinFile.Length div cHexCols);

  end else begin // if mDisplayState = dsHex then begin
    // Dezimale Darstellung
    // Anzahl Zeilen definieren
    if aBinFile <> NIL then
//      aTree.RootNodeCount := (aBinFile.Length div cDezCols);
       if (aBinFile.Length Mod cHexCols) <> 0 then
          aTree.RootNodeCount := (aBinFile.Length div cDezCols) +1
       else
          aTree.RootNodeCount := (aBinFile.Length div cDezCols);


  end;// if mDisplayState = dsHex then begin
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acToggleHexDecExecute(Sender: TObject);
begin
  if Sender is TSpeedButton then
    mDisplayState := TDisplayState(TSpeedButton(Sender).Tag);

  ConfigureCols(mBinGrid1, mBinFile1);
  ConfigureCols(mBinGrid2, mBinFile2);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.Syncronize(aSender: TBaseVirtualTree);
var
  xSyncGrid: TBaseVirtualTree;
  xNodeIndex: cardinal;
  xColIndex: cardinal;
  xNode: PVirtualNode;
  xCol : integer;
begin

  if aSender = mBinGrid1 then  begin
    xSyncGrid := mBinGrid2;
    xCol := 2;
  end else begin
    xSyncGrid := mBinGrid1;
    xCol := 1;
  end;

  //Zeile
  xNodeIndex := aSender.AbsoluteIndex(aSender.FocusedNode);
  //Kolonne
  xColIndex  := aSender.FocusedColumn;

  // AlteSelektion l�schen
  xSyncGrid.ClearSelection;

  // Zeile markieren
  xNode := xSyncGrid.GetFirst;
  while assigned(xNode) do begin
    if xSyncGrid.AbsoluteIndex(xNode) = xNodeIndex then begin
      xSyncGrid.Selected[xNode] := TRUE;
      xSyncGrid.FocusedNode := xNode;
      xNode := nil;
      //xColIndex := 0;
      //xSyncGrid.FocusedNode.Index := 0;
//      xSyncGrid.FocusedColumn := xColIndex;
    end;   // if xSyncGrid.AbsoluteIndex(xNode) = xNodeIndex then begin

    xNode := xSyncGrid.GetnextSibling(xNode);
  end;// while assigned(xNode) do begin

  xSyncGrid.FocusedColumn := xColIndex;
  UpdateValue;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2AfterCellPaint(
  Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode;
  Column: TColumnIndex; CellRect: TRect);
var
  xTree: TVirtualStringTree;
  xLineIndex: string;
begin
 try
  if Column = 0 then begin
    if Sender is TVirtualStringTree then begin
      xTree := TVirtualStringTree(Sender);
      with TargetCanvas do begin
        // Simuliert die Anzeige einer FixedRow wie in TStringGrid
        if toShowVertGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Right);
        if toShowHorzGridLines in xTree.TreeOptions.PaintOptions then
          Inc(CellRect.Bottom);
        // Zeichnet eine 3D Fl�che
        DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);

        xLineIndex := '';
        case mDisplayState of
          dsHex : xLineIndex :=  IntToHex(Sender.AbsoluteIndex(Node) * cHexCols, 4);
          dsDez, dsASCII : xLineIndex :=  IntToStr(Sender.AbsoluteIndex(Node) * cDezCols);
        end;// case mDisplayState of

        TargetCanvas.TextOut(CellRect.Left, CellRect.Top, xLineIndex);
      end;// with TargetCanvas do begin
    end;// if Sender is TVirtualStringTree then begin
  end;// if Column = 0 then begin
 except
 end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2BeforeCellPaint(
  Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode;
  Column: TColumnIndex; CellRect: TRect);
var
  xColCount: cardinal;
  xIndex: integer;
begin

  xColCount := cHexCols;
  case mDisplayState of
    dsHex: xColCount := cHexCols;
    dsDez, dsASCII: xColCount := cDezCols;
  end;// case mDisplayState of

  try
    xIndex := Sender.AbsoluteIndex(Node) * xColCount + (Column - 1);
    if IsDifferent(xIndex) then begin
      // Bytes sind unterschiedlich
      TargetCanvas.Brush.Color := cDifferentColor;
      TargetCanvas.FillRect(CellRect);
    end else begin
      // Entweder identisch, oder in einem File nicht vorhanden
      if  Assigned(mBinFile1) and  Assigned(mBinFile2) then
          if (xIndex >= mBinFile1.Length) or (xIndex >= mBinFile2.Length) then begin
             // In einem File nicht vorhanden
             TargetCanvas.Brush.Color := cMissingColor;
             TargetCanvas.FillRect(CellRect);
          end;// if (xIndex >= mBinFile1.Length) or (xIndex >= mBinFile2.Length) then begin
    end;// if IsDifferent(Sender.AbsoluteIndex(Node) * xColCount + (Column - 1)) then begin
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2Change(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
 try
   UpdateValue;
 except
 end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2GetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;var CellText: WideString);
var
  xBinfile: TBinFile;
  xColCount: cardinal;
begin
  if Column >= 1 then begin
    try
      xBinFile := nil;

      // Zu verwendendes Bin�rfile
      If Sender = mBinGrid1 then
        xBinFile := mBinFile1;
      If Sender = mBinGrid2 then
        xBinFile := mBinFile2;

      xColCount := cHexCols;
      case mDisplayState of
        dsHex: xColCount := cHexCols;
        dsDez, dsASCII: xColCount := cDezCols;
      end;// case mDisplayState of

      if assigned(xBinFile) then
        CellText := xBinFile.GetNumberString(Sender.AbsoluteIndex(Node) * xColCount + (Column - 1), mDisplayState);

      Application.ProcessMessages;
    except
    end;
  end;// if Column >= 1 then begin
end;
//------------------------------------------------------------------------------
function TMain_MMFileComapre.IsDifferent(aIndex: cardinal): boolean;
begin
  result := false;
  if  Assigned(mBinFile1) and  Assigned(mBinFile2) then
     if (mBinFile1.FileName <> '') and (mBinFile2.FileName <> '') then
        if (aIndex < mBinFile1.Length) and (aIndex < mBinFile2.Length) then
            result := mBinFile1.Byte[aIndex] <> mBinFile2.Byte[aIndex]
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.UpdateStatusbar(aBinFile: TBinFile;
  aStatusBar: TStatusBar; aGrid: TVirtualStringTree);
begin
  // DisplayModus
  case mDisplayState of
    dsHex:  aStatusBar.Panels[cDisplayStatePanel].Text := 'HEX';
    dsDez: aStatusBar.Panels[cDisplayStatePanel].Text := 'DEZ';
    dsASCII: aStatusBar.Panels[cDisplayStatePanel].Text := 'ASCII';
  end;// case mDisplayState of

  try
      // Selektiertes Element
      if assigned(aGrid.FocusedNode) then begin
         // Index des selektierten Elements anzeigen
         aStatusBar.Panels[cCoordinatePanel].Text := Format('Index: $%s (%d)', [IntToHex(GetSelectedIndex(aGrid), 4), GetSelectedIndex(aGrid)]);

         // Wert des Elements
         with aBinFile do
              aStatusBar.Panels[cValuePanel].Text := Format('Wert: $%s (%s)', [GetNumberString(GetSelectedIndex(aGrid), dsHex), GetNumberString(GetSelectedIndex(aGrid), dsDez)]);
      end;// if assigned(aGrid.FocusedNode) then begin

      // Filename
      if assigned(aBinFile) and assigned(aBinFile.BasePointer) then
         aStatusBar.Panels[cFileNamePanel].Text := aBinFile.FileName
      else
         aStatusBar.Panels[cFileNamePanel].Text := '';
  except
  end;
end;
//------------------------------------------------------------------------------









procedure TMain_MMFileComapre.MoveFileToMaster;
var x, xActTabID: integer;
    xBinFile: TBinFile;
    xFileNameComp, xFileNameMaster : String;

    xActMaster, xNewMaster : String;
    xActMasterTabId, xNewMasterTabId : Integer;
begin

  if mBinGrid1.RootNodeCount <=0 then exit;
  if mBinGrid2.RootNodeCount <=0 then exit;

  //Aktueller Master
  xActMaster := mFileList.MasterFile;


  for x:= 0 to mFileList.Count - 1 do begin
    {
     //Aktueller Master
     if  mFileList.BinFile[x].TabIndex = -1 then begin
         xActMaster := mFileList.BinFile[x].FileName;
         xActMasterTabId := -1;
     end;
     }

     //Neuer Master ermitteln
     if mFileList.BinFile[x].TabIndex = tbcCompareFiles.TabIndex then begin
        xNewMaster := mFileList.BinFile[x].FileName;
        xNewMasterTabId := tbcCompareFiles.TabIndex;
        mFileList.MasterFile := xNewMaster;
        mFileList.BinFile[x].TabIndex := -1;
     end;
  end;

  //Master
  mBinGrid1.Clear;
  mBinFile1.AssignFile(mFileList.BinFileByName(xNewMaster));
  //mBinFile1.TabIndex := -1;

  mmTabControl1.Tabs.Clear;
  mmTabControl1.Tabs.Add( Format(cMasterTabFile,[ ExtractFileName(xNewMaster)  ]) );

  ConfigureCols(mBinGrid1, mBinFile1);
  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);


  //Compare Files
  mFileList.BinFileByName(xActMaster).TabIndex := xNewMasterTabId;
  mBinGrid2.Clear;
  mBinFile2.AssignFile( mFileList.BinFileByName(xActMaster) );
//  mBinFile2.TabIndex := xNewMasterTabId;

  ConfigureCols(mBinGrid2, mBinFile2);
  UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);

  tbcCompareFiles.Tabs.Strings[xNewMasterTabId] :=  ExtractFileName(xActMaster);
  tbcCompareFiles.TabIndex := 0;
  tbcCompareFiles.OnChange(Self);


  //Update File-Info Grid
  DetermineDifference;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.DeleteFile(aTree: TmmVirtualStringTree;
  aBinFile: TBinFile);
var xFileName : String;
    x: integer;
begin

  xFileName := aBinFile.FileName;

  mFileList.DeleteBinFile(xFileName);

  aTree.Clear;
  aBinFile.UnloadFile;

  if aTree = mBinGrid1 then begin
     mmTabControl1.Tabs.Clear;
     mmTabControl1.Tabs.Add( Format(cMasterTabFile,[ ' ' ]) );
     if assigned(mBinFile1) then
        UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  end else begin
     if tbcCompareFiles.Tabs.Count > 0 then begin
        tbcCompareFiles.Tabs.Delete(tbcCompareFiles.TabIndex );
        tbcCompareFiles.TabIndex := 0;
        tbcCompareFiles.OnChange(Self);
     end;
     UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
  end;

  DetermineDifference;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.DetermineDifference;
var
  xDifferenceCount: cardinal;
  xLengthDifference: cardinal;
  xBinfile : TBinfile;
  x, xRow, xMasterRow : integer;
begin
  xRow := 2;
  xMasterRow := 1;
  sgFileInfo.FixedRows:= 1;
  sgFileInfo.ClearNormalCells;
  sgFileInfo.RowCount:= xRow;


  //Differenzen der Comparefiles berechnen

  if Assigned(mBinFile1) then
   if mBinFile1.FileName <> '' then begin

      //Alle Dateien ins Grid schreiben
      for x:= 0 to mFileList.Count-1 do begin
         xRow := x + 2;
         sgFileInfo.RowCount:= xRow;

         if Assigned(mBinFile1) then begin
            sgFileInfo.RowCount:= xRow;

            sgFileInfo.Cells[0, xRow-1]:= ExtractFileName(mFileList.BinFile[x].FileName);
            sgFileInfo.Cells[3, xRow-1]:= mFileList.BinFile[x].FileName;
            sgFileInfo.Cells[4, xRow-1]  := Format('%.3d', [mFileList.BinFile[x].TabIndex]   );  //Sort Index
        end;
      end;

      sgFileInfo.QSort;

      for x:= 1 to sgFileInfo.RowCount do begin
         //Differenzen der Comparefiles berechnen und in Grid schreiben

         xDifferenceCount := 0;
         xLengthDifference := 0;

         mBinFile1.CompareWith( mFileList.BinFileByName( sgFileInfo.Cells[3, x] ), xDifferenceCount, xLengthDifference);
         sgFileInfo.Cells[1, x]:=IntToStr(xDifferenceCount);
         sgFileInfo.Cells[2, x]:=IntToStr(xLengthDifference);
      end;

      if (mBinGrid1.RootNodeCount > 0) and (mBinGrid2.RootNodeCount > 0) then begin
         // mBinGrid2.OnClick(Self);
         mBinGrid2.Repaint;
         mBinGrid1.Repaint;
      end;
   end;

   //cbSwap1.Caption :=  Format('Swap %s',[ExtractFileName(mBinFile1.FileName)]);
   //cbSwap2.Caption :=  Format('Swap %s',[ExtractFileName(mBinFile2.FileName)]);


   mValueGrid.Cells[1,0] := cFile1 + #10#13 + ExtractFileName(mBinFile1.FileName);
   mValueGrid.Cells[2,0] := cFile2 + #10#13 + ExtractFileName(mBinFile2.FileName);

end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.sgFileInfoGridHint(Sender: TObject; Arow,
  Acol: Integer; var hintstr: String);
begin
  if (Acol = 0) and (Arow > 0) then
     hintstr := sgFileInfo.Cells[3, Arow];
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid1ColumnClick(
  Sender: TBaseVirtualTree; Column: TColumnIndex; Shift: TShiftState);
begin

end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2ColumnClick(
  Sender: TBaseVirtualTree; Column: TColumnIndex; Shift: TShiftState);
begin
 try
   Syncronize(Sender);
 except
 end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.cbSwap1Click(Sender: TObject);
begin
  UpdateValue;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.MasterToCompareFiles;
var x: integer;
    xActMaster : String;
begin
  if not assigned(mBinFile1) then exit;
  if mBinFile1.FileName = '' then exit;

  xActMaster :=  mBinFile1.FileName;

  //Master Tab. leeren
  mmTabControl1.Tabs.Clear;
  mmTabControl1.Tabs.Add( Format(cMasterTabFile, [' ']) );

  mFileList.MasterFile := '';
  tbcCompareFiles.Tabs.Add( ExtractFileName(xActMaster) );
  mFileList.BinFileByName(xActMaster).TabIndex := tbcCompareFiles.Tabs.Count-1;
  tbcCompareFiles.TabIndex := 0;
  tbcCompareFiles.OnChange(self);

  mBinGrid1.Clear;
  mBinFile1.UnloadFile;
  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  DetermineDifference;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acClearMasterExecute(Sender: TObject);
begin

 // if mBinGrid1.RootNodeCount = 0 then exit;
  mFileList.ClearMasterFile;

  mBinGrid1.Clear;
  mBinFile1.UnloadFile;

  //Master Tab. leeren
  mmTabControl1.Tabs.Clear;
  mmTabControl1.Tabs.Add( Format(cMasterTabFile, [' ']) );

  UpdateStatusbar(mBinFile1, mBinFile1Status, mBinGrid1);
  DetermineDifference;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acMasterToCompareFilesExecute(
  Sender: TObject);
begin
 if mBinGrid1.RootNodeCount = 0 then exit;
 MasterToCompareFiles;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acMoveToMasterExecute(Sender: TObject);
begin
 if mBinGrid2.RootNodeCount >0 then
    MoveFileToMaster;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acClearAllCompExecute(Sender: TObject);
var xMaster : String;
    xCount : integer;
    xBinFile : TBinFile;
begin

//if mBinGrid2.RootNodeCount = 0 then exit;

 xMaster := mFileList.MasterFile;

 xCount := mFileList.Count-1;

 while xCount >= 0 do begin
    if TBinFile(mFileList.Items[xCount]).FileName <> xMaster then
       mFileList.DeleteBinFile(xCount);
    dec(xCount);
 end;

 tbcCompareFiles.Tabs.Clear;
 mBinGrid2.Clear;
 mBinFile2.UnloadFile;
 UpdateStatusbar(mBinFile2, mBinFile2Status, mBinGrid2);
 DetermineDifference;

end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.acClearActiveCompExecute(Sender: TObject);
begin
 // if mBinGrid2.RootNodeCount >0 then
     DeleteFile(mBinGrid2, mBinFile2);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.sgFileInfoClickCell(Sender: TObject; Arow,
  Acol: Integer);
begin
  NavigateGrid(Arow);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.sgFileInfoKeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var xRow: Integer;
begin
  xRow := sgFileInfo.GetRealRow;
  sgFileInfo.OnClickCell(self, xRow,  0);
  sgFileInfo.SelectRows(xRow, 1);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.NavigateGrid(aRow : Integer);
var xTabIndex : Integer;
begin
  if aRow > 0 then begin
     xTabIndex := mFileList.BinFileByName( sgFileInfo.Cells[3, aRow] ).TabIndex;

    if xTabIndex >= 0 then begin
      tbcCompareFiles.TabIndex := xTabIndex;
      tbcCompareFiles.OnChange(Self);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid1Scroll(Sender: TBaseVirtualTree;
  DeltaX, DeltaY: Integer);
begin
  try
     Sleep(200);
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.CheckMasterFile(aFile: String);
var xFile, xDropfile, xTxt : String;
    xFileExists : Boolean;
begin
  xDropfile:= aFile;
  xFileExists :=  mFileList.ExistsFileInList(xDropfile);

  if mFileList.MasterFile <> '' then begin
     xFile := xDropfile;
     if CompareText(xDropfile, mFileList.MasterFile) = 0 then begin
        xTxt := Format('Would you reload ''%s'' ?',[xFile]);
        if MessageDlg(xTxt, mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
           mFileList.BinFileByName( mFileList.MasterFile ).LoadFile(xDropfile);
        end else
           xDropfile := mFileList.MasterFile;

     end else begin
        if xFileExists then begin
           MsgFileExists(xDropfile);
           xDropfile := mFileList.MasterFile;
         end else begin
           xTxt := Format('Would you replace ''%s'' ' + #13#10 + 'with ''%s'' ?',[xFile, mFileList.MasterFile]);
           if MessageDlg(xTxt, mtConfirmation	, [mbYes, mbNo], 0) = mrYes then begin
              mFileList.BinFileByName( mFileList.MasterFile ).LoadFile(xDropfile);
           end else
              xDropfile := mFileList.MasterFile;
         end;
     end;
  end else
     if not xFileExists then
        mFileList.AddFile(xDropfile)
     else begin
        MsgFileExists(xDropfile);
     end;

  if not xFileExists then begin
     mBinFile1.AssignFile(mFileList.BinFileByName(xDropfile));
     mBinFile1.TabIndex := -1;
     mFileList.MasterFile:= xDropfile;

     mmTabControl1.Tabs.Clear;
     mmTabControl1.Tabs.Add( Format(cMasterTabFile,[ ExtractFileName(xDropfile)  ]) );
  end;
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.MsgFileExists(aFile: String);
var xTxt : String;
begin
  xTxt := Format('File ''%s'' already exists!',[aFile]);
  MessageDlg(xTxt, mtWarning, [mbOK], 0);
end;
//------------------------------------------------------------------------------
procedure TMain_MMFileComapre.mBinGrid2KeyUp(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
   Syncronize(Sender as TmmVirtualStringTree);
end;

end.
