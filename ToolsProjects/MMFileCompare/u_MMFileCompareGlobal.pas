{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMFileCompareGlobal.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: Funktionen & Klassen zum 'MillMaster File Compare' Tool
| Info..........: -
| Develop.system: W2k
| Target.system.: W2k, XP, W2k3
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2005  1.00  SDO | File created
|==============================================================================}
unit u_MMFileCompareGlobal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  LoepfeGlobal, MMlist;



type
  PReal48 = ^Real48;

  TDisplayState = (dsNone,
                   dsHex,
                   dsASCII,
                   dsDez);// TDisplayState

  TDataType = (dtByte,
               dtshortInt,
               dtWord,
               dtSmallInt,
               dtDWord,
               dtInteger,
               dtInt64,
               dtSingle,
               dtReal48,
               dtDouble,
               dtExtended,
               dtString,
               dtBit);// TDataType
const
  cDataType: Array[TDataType] of string = ('Byte',
                                           'Shortint',
                                           'Word',
                                           'Smallint',
                                           'DWord',
                                           'Integer',
                                           'Int64',
                                           'Single',
                                           'Real48',
                                           'Double',
                                           'Extended',
                                           'String',
                                           'Bit');


  cMainCaption      = 'MM File Compare';
  cMasterTabFile    = 'Master: [%s]';
  cFile1            = 'Datei 1';
  cFile2            = 'Datei 2';

  // 1 Splate f�r die Zeilenzahl, 16 Spalten f�r Hex und 1 Spalte f�r den Text
  cMaxTreeCol = 18;
  cDafaultColWidth = 35;
  cLineNumberColWidth = 40;
  cDezCols = 10;
  cHexCols = 16;

  // Statusbar
  cDisplayStatePanel = 0;
  cCoordinatePanel = 1;
  cValuePanel = 2;
  cFileNamePanel = 3;

  // Farben
  cDifferentColor = $008EBDFD;
  cMissingColor   = $00EEC19D;




type
  pBinFile = ^TBinFile;
  TBinFile = class(TObject)
  private
    FBasePointer: PByte;
    FColWidth: Cardinal;
    FFileName: string;
    FLen: Cardinal;
    FTabIndex: Integer;
    function GetChar(aIndex: cardinal): Char;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function CompareWith(aBinFile: TBinFile; out aDifferenceCount: cardinal;
            out aLengthDifference: cardinal): Boolean;
    function GetBit(aIndex: integer; aLength: integer): string;
    function GetByte(aIndex: cardinal): Byte;
    function GetFloat(aIndex: integer; aLength: integer): Extended;
    function GetInteger(aIndex: integer; aLength: integer; aSigned: boolean;
            aSwap: boolean): Int64;
    function GetNumberString(aIndex: cardinal; aDisplayState: TDisplayState):
            string;
    function GetString(aIndex: integer; aLength: integer): string;
    function LoadFile(aFileName: string): Boolean;
    function AssignFile(aBinFile : TBinFile): Boolean;
    procedure UnloadFile;
    property ASCII[aIndex: cardinal]: Char read GetChar;
    property BasePointer: PByte read FBasePointer;
    property Byte[aIndex: cardinal]: Byte read GetByte;
    property ColWidth: Cardinal read FColWidth write FColWidth;
    property FileName: string read FFileName;
    property Length: Cardinal read FLen;
    property TabIndex: Integer read FTabIndex write FTabIndex;
  end;


  TFileList = class(TMMList)
  private
    FMasterFile: string;
    function GetBinFile(Index: Integer): TBinFile;
    procedure SetBinFile(Index: Integer; const Value: TBinFile);
  public
    constructor Create;
    destructor Destroy; override;
    function ExistsFileInList(aFile : String): Boolean;
    procedure AddFile(aPathFile : String);
    function BinFileByName(aFileName : String): TBinFile;
    procedure DeleteBinFile(Index : Integer); overload; virtual;
    procedure DeleteBinFile(aFileName : String); overload; virtual;
    procedure ClearMasterFile;
    function GetItemIndex(aFileName : String):Integer;
    property BinFile[Index: Integer]: TBinFile read GetBinFile write SetBinFile;
    property MasterFile: string read FMasterFile write FMasterFile;

  end;


implementation


(*---------------------------------------------------------
  TBinFile
----------------------------------------------------------*)
constructor TBinFile.Create;
begin
  inherited;
  FBasePointer := nil;
  fTabIndex := -1;
end;

//:-------------------------------------------------------------------
destructor TBinFile.Destroy;
begin
  UnloadFile;
  inherited;
end;

//:-------------------------------------------------------------------
function TBinFile.CompareWith(aBinFile: TBinFile; out aDifferenceCount:
        cardinal; out aLengthDifference: cardinal): Boolean;
var
  xLength: Cardinal;
  i: Integer;
begin
  aDifferenceCount := 0;

  if assigned(aBinFile) then begin
    xLength := self.Length;
    if aBinFile.Length < xLength then
      xLength := aBinFile.Length;

    // Midestens der Unterschied in der L�nge der Files
    aLengthDifference := abs(self.Length - aBinFile.Length);

    for i := 0 to xLength - 1 do begin
      if Byte[i] <> aBinFile.Byte[i] then
        inc(aDifferenceCount)
    end;// for i := 0 to xLength - 1 do begin
  end;// if assigned(aBinFile) then begin
end;

//:-------------------------------------------------------------------
function TBinFile.GetBit(aIndex: integer; aLength: integer): string;
var
  i, j: Integer;
  xByte: system.Byte;
  xTempByte: string;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xTempByte := '';
    xByte := Byte[aIndex + i];
    for j := 0 to 7 do begin
      if j = 4 then
        xTempByte := '-' + xTempByte;
      if (xByte and (1 shl j)) = (1 shl j) then
        xTempByte := '1' + xTempByte
      else
        xTempByte := '0' + xTempByte;
    end;// for j := 0 to 7 do begin
    result := result + ' ' + xTempByte;
  end;// for i := 0 to aLength - 1 do begin
  
  if system.Length(result) > 0  then
    system.delete(result, 1, 1);
end;

//:-------------------------------------------------------------------
function TBinFile.GetByte(aIndex: cardinal): Byte;
begin
  result := 0;
  if (aIndex < FLen) and assigned(FBasePointer) then
    result := PByte(cardinal(FBasePointer) + aIndex)^;
end;

//:-------------------------------------------------------------------
function TBinFile.GetChar(aIndex: cardinal): Char;
var
  xByte: System.Byte;
begin
  result := '.';
  xByte := Byte[aIndex];
  if xByte >= ord(' ') then
    result := chr(xByte)
  else
    result := '.';
end;

//:-------------------------------------------------------------------
function TBinFile.GetFloat(aIndex: integer; aLength: integer): Extended;
begin
  result := 0;
  if (aIndex + aLength) < self.Length then begin
    case aLength of
      4: result := PSingle(cardinal(FBasePointer) + aIndex)^;
      6: result := PReal48(cardinal(FBasePointer) + aIndex)^;
      8: result := PDouble(cardinal(FBasePointer) + aIndex)^;
      10: result := PExtended(cardinal(FBasePointer) + aIndex)^;
    end;// case aLength of
  end;// if (aIndex + aLength) < self.Length then begin
end;

//:-------------------------------------------------------------------
function TBinFile.GetInteger(aIndex: integer; aLength: integer; aSigned:
        boolean; aSwap: boolean): Int64;
begin
  result := 0;
  if (aIndex >= 0) and ((aIndex + aLength) <= self.Length) then begin
    if aSigned then begin
      case aLength of
        1: result := PShortInt(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
            result := Swap(PSmallInt(cardinal(FBasePointer) + aIndex)^)
           else
            result := PSmallInt(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PInteger(cardinal(FBasePointer) + aIndex)^)
           else
             result := PInteger(cardinal(FBasePointer) + aIndex)^;
        8: result := PInt64(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end else begin
      case aLength of
        1: result := PByte(cardinal(FBasePointer) + aIndex)^;
        2: if aSwap then
             result := swap(PWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PWord(cardinal(FBasePointer) + aIndex)^;
        4: if aSwap then
             result := SwapDWord(PDWord(cardinal(FBasePointer) + aIndex)^)
           else
             result := PDWord(cardinal(FBasePointer) + aIndex)^;
      end;// case aLength of
    end;// if aSigned then begin
  end;// if (aIndex >= 0) and ((aIndex + aLength) < self.Length) then begin
end;

//:-------------------------------------------------------------------
function TBinFile.GetNumberString(aIndex: cardinal; aDisplayState:
        TDisplayState): string;
begin
  result := '';
  if aIndex < FLen then begin
    case aDisplayState of
      dsHex:   result := IntToHex(Byte[aIndex], 2);
      dsDez:   result := IntToStr(Byte[aIndex]);
      dsASCII: result := ASCII[aIndex];
    end;// case mDisplayState of
  end;// if aIndex < FLen then
end;

//:-------------------------------------------------------------------
function TBinFile.GetString(aIndex: integer; aLength: integer): string;
var
  i, j: Integer;
  xByte: system.Byte;
begin
  result := '';
  for i := 0 to aLength - 1 do begin
    xByte := Byte[aIndex + i];
    if xByte >= ord(' ') then
      result := result + chr(xByte)
    else
      result := result + '.';
  end;// for i := 0 to aLength - 1 do begin
end;

//:-------------------------------------------------------------------
function TBinFile.AssignFile(aBinFile : TBinFile): Boolean;
begin
  result := FALSE;

  if assigned(aBinFile) then begin
     try
       if not Assigned( FBasePointer ) then
          New(FBasePointer);

       FLen := aBinFile.FLen;
       GetMem(FBasePointer,  FLen);
       System.Move(aBinFile.BasePointer^, FBasePointer^, FLen);
//       FBasePointer := aBinFile.BasePointer;
          
       FFileName := aBinFile.FileName;;
       result := TRUE;
     except
       on e:EOutOfMemory do
          FBasePointer := nil;
     end;// try except
  end;// if assigned(aFileName) then begin

end;
//:-------------------------------------------------------------------
function TBinFile.LoadFile(aFileName: string): Boolean;
var
  xStream: TFileStream;
  xPointer: Pointer;
begin
  result := false;
  UnloadFile;

  if FileExists(aFileName) then begin
    xStream := nil;
    try
      xStream := TFileStream.Create(aFileName, fmOpenRead	or fmShareDenyNone);
      xStream.Position := 0;
      try
        GetMem(FBasePointer, xStream.Size);
        FLen := xStream.Read(FBasePointer^, xStream.Size);
        result := true;
        FFileName := aFileName;
      except
        on e:EOutOfMemory do
          FBasePointer := nil;
      end;// try except
    finally
      if assigned(xStream) then
        xStream.Free;
    end;// try finally
  end;// if FileExists(aFileName) then begin

end;

//:-------------------------------------------------------------------
procedure TBinFile.UnloadFile;
begin
  if assigned(FBasePointer) then
    FreeMem(FBasePointer);
  FBasePointer := nil;
  FLen :=0;
  FFileName := '';
//  fTabIndex := -1;
end;
//:-------------------------------------------------------------------


//******************************************************************************
//TFileList
//******************************************************************************
//:-------------------------------------------------------------------
constructor TFileList.Create;
begin
  inherited Create;
end;

//:-------------------------------------------------------------------
destructor TFileList.Destroy;
begin
  while Count > 0 do begin
        TBinFile( Items[0] ).Free;
        Delete(0);
  end;
  inherited Destroy;
end;

//:-------------------------------------------------------------------
procedure TFileList.AddFile(aPathFile : String);
var
  xBinFile: TBinFile;
  //xpBinFile : pBinFile;
begin
  if ExistsFileInList(aPathFile) then begin
{
   mBinFile1.LoadFile('W:\MillMaster\ToolsProjects\MMFileCompare\Memory Q.xml');
  //mBinFile2.LoadFile(ParamStr(2));
  ConfigureCols(mBinGrid1, mBinFile1);
  UpdateValue;

  DetermineDifference;
}


     if Assigned(BinFileByName(aPathFile)) then
        BinFileByName(aPathFile).LoadFile(aPathFile)
     else begin
        xBinFile := TBinFile.Create;
        xBinFile.LoadFile(aPathFile);

        Add( pBinFile (xBinFile) );
     end;

  end else begin
     xBinFile := TBinFile.Create;
     xBinFile.LoadFile(aPathFile);
     Add( pBinFile (xBinFile) );
  end;
end;
//:-------------------------------------------------------------------
function TFileList.BinFileByName(aFileName : String): TBinFile;
var
  x: Integer;
begin
  Result := Nil;
  for x:= 0 to Count -1 do
      if CompareText( aFileName, TBinFile(Items[x]).FileName ) = 0 then begin
         Result := TBinFile(Items[x]);
         break;
      end;
end;

//:-------------------------------------------------------------------
procedure TFileList.DeleteBinFile(Index : Integer);
var xBinFile : TBinFile;
    xTabIndex, x, xTabId : Integer;
    xIsMaster : Boolean;
begin
  xBinFile  := TBinFile(Items[Index]);
  xTabIndex := xBinFile.TabIndex;
  xIsMaster := FALSE;


  if CompareText( FMasterFile, xBinFile.FileName ) = 0 then begin
     FMasterFile := '';
     xIsMaster := TRUE;
  end;

  xBinFile.Free;
 //xBinFile := NIL;
  Delete(Index);


  //Register Index anpassen
  if not xIsMaster then
     for x:= 0 to Count -1 do
         if TBinFile(Items[x]).TabIndex > xTabIndex then begin
            xTabId := TBinFile(Items[x]).TabIndex;
            dec( xTabId );
            TBinFile(Items[x]).TabIndex := xTabId;
         end;
end;

//:-------------------------------------------------------------------
procedure TFileList.DeleteBinFile(aFileName : String);
var x : integer;
    xBinFile : TBinFile;
begin
  for x:= 0 to Count -1 do begin
      xBinFile := TBinFile(Items[x]);
      if CompareText( aFileName, xBinFile.FileName ) = 0 then begin
         DeleteBinFile(x);
         break;
      end;
  end;
end;

//:-------------------------------------------------------------------
function TFileList.ExistsFileInList(aFile : String): Boolean;
var
  x: Integer;
begin
  Result := FALSE;
  for x:= 0 to Count -1 do
      if CompareText( aFile, TBinFile(Items[x]).FileName ) = 0 then begin
         Result := TRUE;
         break;
      end else
         Result := FALSE;
end;

//:-------------------------------------------------------------------
function TFileList.GetBinFile(Index: Integer): TBinFile;
begin
  Result := TBinFile(Items[Index]);
end;
//:-------------------------------------------------------------------
procedure TFileList.ClearMasterFile;
var xCount, x: Integer;
    xMaster : String;
begin
   xMaster := MasterFile;

   xCount := Count - 1;

   while xCount >= 0 do begin
     if CompareText( xMaster, TBinFile(Items[xCount]).FileName ) = 0 then begin
        TBinFile(Items[xCount]).Free;
        Delete(xCount);
        MasterFile := '';
        break;
     end;
     dec(xCount);
   end;
end;
//:-------------------------------------------------------------------
procedure TFileList.SetBinFile(Index: Integer; const Value: TBinFile);
var xBinFile : TBinFile;
begin

    xBinFile := TBinFile.Create;
    xBinFile := Value;
    Items[Index] := ( pBinFile (xBinFile) );

{
  if Items[Index] <> NIL then
     Items[Index] := pBinFile (Value)
  else begin
    xBinFile := TBinFile.Create;
    xBinFile := Value;
    Add( pBinFile (xBinFile) );
  end;
}

 //   Result := TBinFile(Items[Index]);
end;
//:-------------------------------------------------------------------
function TFileList.GetItemIndex(aFileName: String): Integer;
var
  x: Integer;
begin
  Result := -1;;
  for x:= 0 to Count -1 do
      if CompareText( aFileName, TBinFile(Items[x]).FileName ) = 0 then begin
         Result := x;
         break;
      end;
end;
//:-------------------------------------------------------------------
end.
