{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: BDE_ConfigurationInit.pas
| Projectpart...: Fuer das IS-Setup
| Subpart.......: -
| Process(es)...: -
| Description...: Konfiguriert die BDE Part : Configuration/System/init
|                 'SHAREDMEMLOCATION', '0x5BDE'
|                 'SHAREDMEMSIZE', '4096'
|                 'LANGDRIVER', '''ascii'' ANSI'
|                 
| Info..........:
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 19.04.2002  1.00  SDo | File created
===============================================================================}

program BDE_ConfigurationInit;
{$APPTYPE CONSOLE}

uses
  Windows,
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  mmSession,
  DBTables,
  BDE,
  MMEventLog,
  LoepfeGlobal;

// Insert user code here
var gMSG: String;

//******************************************************************************
// Setzt einen SYSTEM BDE-Config Wert
// (Configuration page in BDE Administrator oder in Reg.
// \Database Engine\Settings\SYSTEM\INIT
//******************************************************************************        
function SetBDEConfigSettings(aParam, aValue : String):Boolean;
var
 xCur : hDBICur;
 xConfig : CFGDesc;
 xContinueIt: boolean;
 xPath : PChar;
 xMsg, xKey, xRegValue, xParam, xValue :String;
 xRet :Boolean;

 xRegPath : HKEY;
 x: integer;
begin
  xRet := FALSE;

  xParam := aParam;
  xValue := aValue;
  xMsg   := 'Parameter does not exists.';
  try

   if DbiInit(nil) = DBIERR_NONE then begin
      xCur := nil;
      xPath:= '\SYSTEM\Init';
    //  ShowMessage('DbiInit(nil) ok');
      if DbiOpenCfgInfoList(nil,
                            dbiREADWRITE,
                            cfgPersistent,
                            xPath,
                            xCur) = DBIERR_NONE then
         begin
           if DbiSetToBegin(xCur) = DBIERR_NONE then begin
              xContinueIt := true;

              while xContinueIt do begin
                    if(DbiGetNextRecord(xCur, dbiWRITELOCK, @xConfig, nil)<> DBIERR_NONE) then
                      xContinueIt := false
                    else if StrIComp(xConfig.szNodeName, PChar(xParam)) = 0 then begin
                        if AnsiStrIComp(  xConfig.szValue , PChar(xValue) ) <> 0 then
                           StrCopy(xConfig.szValue, PChar(xValue)) //Set Value
                        else begin
                            xMsg   := Format('Param value already exists. (%s = %s)',[xParam, xValue]);
                            xContinueIt := false;
                            break;
                        end;

                        if DbiModifyRecord(xCur, @xConfig, true) = DBIERR_NONE then begin
                           xMsg := Format('BDE config settings modified : Parameter : %s, Value  : %s',[xParam, xValue] );
                           xMsg := xMsg + '     successful';
                           xRet:= TRUE;
                           break;
                        end else begin
                           xMSg := Format('BDE config settings not modified: Parameter : %s, Value  : %s',[xParam, xValue] );
                           xMsg := xMsg + '     failed';
                           xRet:= FALSE;
                           break;
                        end;
                        xContinueIt := FALSE;
                        //if not xRet then break;
                    end;
              end; //End while
           end else begin // END if DbiSetToBegin(xCur)
             xMSg := Format('BDE config settings not modified: Parameter : %s, Value  : %s ; Error : DbiSetToBegin() The specified cursor handle is invalid or NULL.' ,[xParam, xValue] );
           end;
         end;
      DbiExit();
   end else begin
       xMSg := Format('BDE config settings not modified: Parameter : %s, Value  : %s. Can not BDE initialize ',[xParam, xValue] );
       xMsg := xMsg + '     failed';
       xRet:= FALSE;
   end;

  except
       xMSg := Format('Error in BDE config settings.  Parameter : %s, Value  : %s.',[xParam, xValue] );
       xMsg := xMsg + '     failed';
       xRet:= FALSE;
  end;


  //Werte aus Sicherheitsgruenden in die Reg. schreiben
  xRegPath := cRegLM;
  xKey     := 'SOFTWARE\Borland\Database Engine\Settings\SYSTEM\INIT';

  if AnsiStrIComp('SHAREDMEMLOCATION', PChar(xParam)) = 0 then begin
     xRegValue := '0x5BDE';
     SetRegString(xRegPath, xKey, xParam, xRegValue);
  end;

  if AnsiStrIComp('SHAREDMEMSIZE', PChar(xParam)) = 0 then begin
     xRegValue := '4096';
     SetRegString(xRegPath, xKey, xParam, xRegValue);
  end;

  if AnsiStrIComp('LANGDRIVER', PChar(xParam)) = 0 then begin
     xRegValue := '''ascii'' ANSI';
     SetRegString(xRegPath, xKey, PChar(xParam), xRegValue);
  end;

  gMSG := xParam + ' : ' + xMsg;
  Result := xRet;

end;
//------------------------------------------------------------------------------

var xLogFile: TStrings;
    xFileName : String;
    xSize : Integer;
    xFileNameTemp : array[0..500] of char;
    xDLLName :String;
begin

  //Dateiname ermitteln (Pfad & Dateiname)
  xSize := GetModuleFileName(HInstance,
                             xFileNameTemp,
                             SizeOf(xFileNameTemp)
                             );

  if xSize > 0 then
     xFileName := xFileNameTemp
  else
     xFileName := 'c:\Temp\BDE_ConfigurationInit.log';

  //Logfile
  xLogFile := TStringList.Create;

  //Parameter setzen
  SetBDEConfigSettings('SHAREDMEMLOCATION', '0x5BDE');
  xLogFile.Add(gMsg);

  SetBDEConfigSettings('SHAREDMEMSIZE', '4096');
  xLogFile.Add(gMsg);

  SetBDEConfigSettings('LANGDRIVER', '''ascii'' ANSI');
  xLogFile.Add(gMsg);

  xFileName:=  StringReplace(xFileName,'.exe', '.log', [rfReplaceAll] );

  xLogFile.SaveToFile(xFileName);
  xLogFile.Free;
end.


