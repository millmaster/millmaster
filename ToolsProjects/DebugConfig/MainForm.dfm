object frmMain: TfrmMain
  Left = 554
  Top = 233
  Width = 386
  Height = 455
  Caption = 'DebugConfig'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object sbError: TmmStatusBar
    Left = 0
    Top = 398
    Width = 378
    Height = 23
    Panels = <>
    SimplePanel = True
  end
  object tsADOConnection: TmmPageControl
    Left = 0
    Top = 0
    Width = 378
    Height = 398
    ActivePage = tsCodeSite
    Align = alClient
    TabOrder = 1
    object tsCodeSite: TTabSheet
      Caption = 'CodeSite'
      object mmLabel1: TmmLabel
        Left = 0
        Top = 0
        Width = 370
        Height = 15
        Align = alTop
        AutoSize = False
        Caption = 'Debug keys:'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object clbCodeSite: TmmCheckListBox
        Left = 0
        Top = 15
        Width = 370
        Height = 290
        OnClickCheck = clbCodeSiteClickCheck
        Align = alClient
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpTop
        MultiSelect = True
      end
      object mmPanel2: TmmPanel
        Left = 0
        Top = 305
        Width = 370
        Height = 65
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object bUpdateDebug: TmmButton
          Left = 4
          Top = 4
          Width = 363
          Height = 25
          Anchors = [akLeft, akBottom]
          TabOrder = 0
          Visible = True
          OnClick = bUpdateDebugClick
          AutoLabel.LabelPosition = lpLeft
        end
        object bUpdateCodeSite: TmmButton
          Left = 13
          Top = 36
          Width = 60
          Height = 25
          Action = acUpdateCodeSite
          Anchors = [akBottom]
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bNew: TmmButton
          Left = 106
          Top = 36
          Width = 60
          Height = 25
          Action = acAddKey
          Anchors = [akBottom]
          TabOrder = 2
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bDefaults: TmmButton
          Left = 201
          Top = 36
          Width = 60
          Height = 25
          Action = acDefaults
          Anchors = [akBottom]
          TabOrder = 3
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bDelete: TmmButton
          Left = 294
          Top = 36
          Width = 60
          Height = 25
          Action = acDeleteKey
          Anchors = [akBottom]
          TabOrder = 4
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
    object tsPlugins: TTabSheet
      Caption = 'Plugins'
      ImageIndex = 1
      object clbPlugins: TmmCheckListBox
        Left = 5
        Top = 10
        Width = 352
        Height = 331
        OnClickCheck = clbPluginsClickCheck
        Anchors = [akLeft, akTop, akRight, akBottom]
        ItemHeight = 13
        Sorted = True
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpTop
        MultiSelect = True
      end
      object bUpdatePlugins: TmmButton
        Left = 146
        Top = 347
        Width = 75
        Height = 25
        Action = acUpdatePlugins
        Anchors = [akBottom]
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object tsMMLinkShare: TTabSheet
      Caption = 'MMLinkShare'
      ImageIndex = 2
      object meMMLinkShare: TmmMemo
        Left = 0
        Top = 37
        Width = 364
        Height = 340
        Align = alBottom
        Anchors = [akLeft, akTop, akRight, akBottom]
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        ParentFont = False
        ScrollBars = ssVertical
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bRead: TmmButton
        Left = 8
        Top = 8
        Width = 75
        Height = 25
        Caption = 'Read'
        TabOrder = 1
        Visible = True
        OnClick = bReadClick
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object tsTimeouts: TTabSheet
      Caption = 'Timeouts'
      ImageIndex = 4
      object lvTimeouts: TmmListView
        Left = 0
        Top = 8
        Width = 369
        Height = 273
        Anchors = [akLeft, akTop, akRight, akBottom]
        Columns = <
          item
            Caption = 'Job'
            Width = 200
          end
          item
            Caption = 'Parameters'
            Width = 150
          end>
        HideSelection = False
        ReadOnly = True
        RowSelect = True
        SortType = stText
        TabOrder = 0
        ViewStyle = vsReport
        Visible = True
        OnSelectItem = lvTimeoutsSelectItem
        AutoLabel.LabelPosition = lpLeft
      end
      object mmPanel1: TmmPanel
        Left = 0
        Top = 280
        Width = 370
        Height = 90
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object laTmoJob: TmmLabel
          Left = 8
          Top = 2
          Width = 41
          Height = 13
          Caption = 'Job Tmo'
          FocusControl = edJobTmo
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laTmoMsg: TmmLabel
          Left = 88
          Top = 2
          Width = 44
          Height = 13
          Caption = 'Msg Tmo'
          FocusControl = edMsgTmo
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laTmoRep: TmmLabel
          Left = 168
          Top = 2
          Width = 43
          Height = 13
          Caption = 'Msp Rep'
          FocusControl = edMsgRep
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bTmoAdd: TmmSpeedButton
          Left = 272
          Top = 55
          Width = 24
          Height = 21
          Action = acTmoAdd
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            8888888888888888888888888800008888888888880440888888888888044088
            8888888888044088888888000004400000888804444444444088880444444444
            4088880000044000008888888804408888888888880440888888888888044088
            8888888888000088888888888888888888888888888888888888}
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bTmoDelete: TmmSpeedButton
          Left = 96
          Top = 55
          Width = 24
          Height = 21
          Action = acTmoDelete
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            8888888888888888888888888888888888888888888888888888888888888888
            8888888888888888888888000000000000888804444444444088880444444444
            4088880000000000008888888888888888888888888888888888888888888888
            8888888888888888888888888888888888888888888888888888}
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bTmoUpdate: TmmButton
          Left = 8
          Top = 53
          Width = 75
          Height = 25
          Action = acTmoUpdate
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edJobTmo: TmmEdit
          Left = 8
          Top = 17
          Width = 65
          Height = 21
          Color = clWindow
          TabOrder = 1
          Visible = True
          OnChange = edTimeoutChange
          AutoLabel.Control = laTmoJob
          AutoLabel.LabelPosition = lpTop
          Decimals = 0
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object edMsgTmo: TmmEdit
          Left = 88
          Top = 17
          Width = 65
          Height = 21
          Color = clWindow
          TabOrder = 2
          Visible = True
          OnChange = edTimeoutChange
          AutoLabel.Control = laTmoMsg
          AutoLabel.LabelPosition = lpTop
          Decimals = 0
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object edMsgRep: TmmEdit
          Left = 168
          Top = 17
          Width = 65
          Height = 21
          Color = clWindow
          TabOrder = 3
          Visible = True
          OnChange = edTimeoutChange
          AutoLabel.Control = laTmoRep
          AutoLabel.LabelPosition = lpTop
          Decimals = 0
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object cobTimeouts: TmmComboBox
          Left = 128
          Top = 55
          Width = 145
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 0
          Sorted = True
          TabOrder = 4
          Visible = True
          AutoLabel.LabelPosition = lpLeft
          Edit = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'ADO'
      ImageIndex = 4
      object mmLabel3: TmmLabel
        Left = 24
        Top = 16
        Width = 52
        Height = 13
        Caption = 'SQLServer'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel4: TmmLabel
        Left = 24
        Top = 72
        Width = 49
        Height = 13
        Caption = 'Database:'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object cobSQLServer: TmmComboBox
        Left = 24
        Top = 32
        Width = 250
        Height = 21
        Color = clWindow
        ItemHeight = 0
        Sorted = True
        TabOrder = 0
        Visible = True
        OnChange = cobSQLServerChange
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object cobDatabase: TmmComboBox
        Left = 24
        Top = 88
        Width = 250
        Height = 21
        Color = clWindow
        ItemHeight = 0
        Sorted = True
        TabOrder = 1
        Visible = True
        OnChange = cobSQLServerChange
        AutoLabel.LabelPosition = lpLeft
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object bUpdateADO: TmmButton
        Left = 146
        Top = 347
        Width = 75
        Height = 25
        Anchors = [akBottom]
        Caption = 'Update'
        Enabled = False
        TabOrder = 2
        Visible = True
        OnClick = bUpdateADOClick
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object alMainForm: TmmActionList
    OnUpdate = alMainFormUpdate
    Left = 271
    Top = 45
    object acAddKey: TAction
      Caption = 'Add'
      OnExecute = acAddKeyExecute
    end
    object acDeleteKey: TAction
      Caption = 'Delete'
      OnExecute = acDeleteKeyExecute
    end
    object acUpdateCodeSite: TAction
      Caption = 'Update'
      OnExecute = acUpdateCodeSiteExecute
    end
    object acUpdatePlugins: TAction
      Caption = 'Update'
      OnExecute = acUpdatePluginsExecute
    end
    object acDefaults: TAction
      Caption = 'Defaults'
      OnExecute = acDefaultsExecute
    end
    object acTmoUpdate: TAction
      Caption = 'Update'
      OnExecute = acTmoUpdateExecute
    end
    object acTmoAdd: TAction
      OnExecute = acTmoAddExecute
    end
    object acTmoDelete: TAction
      OnExecute = acTmoDeleteExecute
    end
  end
end
