unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ActnList, mmActionList, StdCtrls, mmButton, CheckLst, mmCheckListBox,
  ExtCtrls, mmRadioGroup, ComCtrls, mmStatusBar, mmPanel, mmLabel,
  mmPageControl, HostlinkShare, HostlinkDef, mmMemo, mmComboBox,
  mmHeaderControl, mmListView, mmEdit, Grids, mmStringGrid, Buttons,
  mmSpeedButton;

type
  TfrmMain = class(TForm)
    alMainForm: TmmActionList;
    acUpdateCodeSite: TAction;
    sbError: TmmStatusBar;
    acAddKey: TAction;
    acDeleteKey: TAction;
    tsADOConnection: TmmPageControl;
    tsCodeSite: TTabSheet;
    tsPlugins: TTabSheet;
    mmLabel1: TmmLabel;
    clbCodeSite: TmmCheckListBox;
    clbPlugins: TmmCheckListBox;
    bUpdatePlugins: TmmButton;
    acUpdatePlugins: TAction;
    acDefaults: TAction;
    tsMMLinkShare: TTabSheet;
    meMMLinkShare: TmmMemo;
    bRead: TmmButton;
    TabSheet1: TTabSheet;
    cobSQLServer: TmmComboBox;
    mmLabel3: TmmLabel;
    cobDatabase: TmmComboBox;
    mmLabel4: TmmLabel;
    bUpdateADO: TmmButton;
    tsTimeouts: TTabSheet;
    lvTimeouts: TmmListView;
    acTmoUpdate: TAction;
    acTmoAdd: TAction;
    acTmoDelete: TAction;
    mmPanel1: TmmPanel;
    bTmoUpdate: TmmButton;
    edJobTmo: TmmEdit;
    laTmoJob: TmmLabel;
    laTmoMsg: TmmLabel;
    edMsgTmo: TmmEdit;
    laTmoRep: TmmLabel;
    edMsgRep: TmmEdit;
    cobTimeouts: TmmComboBox;
    bTmoAdd: TmmSpeedButton;
    bTmoDelete: TmmSpeedButton;
    mmPanel2: TmmPanel;
    bUpdateDebug: TmmButton;
    bUpdateCodeSite: TmmButton;
    bNew: TmmButton;
    bDefaults: TmmButton;
    bDelete: TmmButton;
    procedure acAddKeyExecute(Sender: TObject);
    procedure acDeleteKeyExecute(Sender: TObject);
    procedure acUpdateCodeSiteExecute(Sender: TObject);
    procedure acUpdatePluginsExecute(Sender: TObject);
    procedure alMainFormUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure clbCodeSiteClickCheck(Sender: TObject);
    procedure clbPluginsClickCheck(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rgComputerClick(Sender: TObject);
    procedure acDefaultsExecute(Sender: TObject);
    procedure bReadClick(Sender: TObject);
    procedure bUpdateDebugClick(Sender: TObject);
    procedure bUpdateADOClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure cobSQLServerChange(Sender: TObject);
    procedure bTmoUpdateClick(Sender: TObject);
    procedure lvTimeoutsSelectItem(Sender: TObject; Item: TListItem;
      Selected: Boolean);
    procedure edTimeoutChange(Sender: TObject);
    procedure acTmoUpdateExecute(Sender: TObject);
    procedure acTmoAddExecute(Sender: TObject);
    procedure acTmoDeleteExecute(Sender: TObject);
  private
    mCodeSiteChanged: Boolean;
    mPluginsChanged: Boolean;
    mTempStrList: TStringList;
    mTimeoutsChanged: Boolean;
    procedure ReadCodeSite;
    procedure ReadPlugins;
    procedure ReadMMLinkshare;
    procedure ReadTimeouts;
  public
  end;

var
  frmMain: TfrmMain;

implementation
uses
  mmRegistry, mmStringList, Registry, IPCClass, LoepfeGlobal, BaseGlobal, TypInfo;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TfrmMain.acAddKeyExecute(Sender: TObject);
var
  xStr: String;
  xIndex: Integer;
begin
  mCodeSiteChanged := True;
  if InputQuery('Enter new value', 'Value name:', xStr) then begin
    xIndex := clbCodeSite.Items.Add(xStr);
    clbCodeSite.Checked[xIndex] := True;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acDeleteKeyExecute(Sender: TObject);
var
  i: Integer;
begin
  if MessageDlg('Delete selected value(s)?', mtWarning, mbOKCancel, 0) = mrOK then begin
    with TmmRegistry.Create do
    try
      RootKey := cRegLM;
      if OpenKey(cRegMMDebug, True) then begin
        for i:=0 to clbCodeSite.Items.Count-1 do begin
          if clbCodeSite.Selected[i] then
            DeleteValue(clbCodeSite.Items[i]);
        end;
      end;
    finally
      Free;
    end;
    ReadCodeSite;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acUpdateCodeSiteExecute(Sender: TObject);
var
  i: Integer;
  xMsg : TMMGuardRec;
begin
  sbError.SimpleText := '';
  with TmmRegistry.Create do
  try
    RootKey := cRegLM;
    if OpenKey(cRegMMDebug, True) then begin
      for i:=0 to clbCodeSite.Items.Count-1 do
        WriteBool(clbCodeSite.Items[i], clbCodeSite.Checked[i]);
    end else
      sbError.SimpleText := GetLastErrorText;
  finally
    Free;
  end;

  mCodeSiteChanged := False;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acUpdatePluginsExecute(Sender: TObject);
var
  i: Integer;
  xStr: String;
begin
  sbError.SimpleText := '';

  for i:=0 to clbPlugins.Items.Count-1 do begin
     xStr := Format('%s\%s', [cRegLoepfeAddons, clbPlugins.Items[i]]);
     SetRegBoolean(cRegLM,  xStr, 'Disabled', not clbPlugins.Checked[i]);
  end;

  mPluginsChanged := False;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.alMainFormUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
  Handled := True;
  acUpdateCodeSite.Enabled := mCodeSiteChanged;
  acUpdatePlugins.Enabled  := mPluginsChanged;
  acTmoUpdate.Enabled      := mTimeoutsChanged;
  acTmoDelete.Enabled      := Assigned(lvTimeouts.Selected);
  acTmoAdd.Enabled         := (cobTimeouts.Text <> '');
  edJobTmo.Enabled         := acTmoDelete.Enabled;
  edMsgTmo.Enabled         := acTmoDelete.Enabled;
  edMsgRep.Enabled         := acTmoDelete.Enabled;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.clbCodeSiteClickCheck(Sender: TObject);
begin
  mCodeSiteChanged := True;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.clbPluginsClickCheck(Sender: TObject);
begin
  mPluginsChanged := True;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  xIndex: Integer;
begin
  mCodeSiteChanged  := False;
  mTempStrList      := TStringList.Create;

  cobSQLServer.Items.CommaText := GetRegString(cRegLM, cRegMMDebug, 'SQLServerNames', '');
  cobSQLServer.ItemIndex := cobSQLServer.Items.IndexOf(gSQLServerName);

  cobDatabase.Items.CommaText := GetRegString(cRegLM, cRegMMDebug, 'DatabaseNames', '');
  cobDatabase.ItemIndex := cobDatabase.Items.IndexOf(gDBName);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  mTempStrList.Free;

  SetRegString(cRegLM, cRegMMDebug, 'SQLServerNames', cobSQLServer.Items.CommaText);
  SetRegString(cRegLM, cRegMMDebug, 'DatabaseNames', cobDatabase.Items.CommaText);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormShow(Sender: TObject);
begin
  bUpdateDebug.Caption := Format('Send debug notification to MMGuard on [%s]', [gMMHost]);
  ReadCodeSite;
  ReadPlugins;
  ReadTimeouts;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ReadCodeSite;
var
  i: Integer;
  xIndex: Integer;
  xList: TStringList;
begin
  mCodeSiteChanged := False;
  clbCodeSite.Clear;

  xList := TStringList.Create;
  with TmmRegistry.Create do
  try
    RootKey := cRegLM;
    if OpenKeyReadOnly(cRegMMDebug) then begin
      GetValueNames(xList);
      if xList.Count > 0 then
        for i:=0 to xList.Count-1 do begin
          if GetDataType(xList.Strings[i]) = rdInteger then begin
            xIndex := clbCodeSite.Items.Add(xList.Strings[i]);
            clbCodeSite.Checked[xIndex] := ReadBool(xList.Strings[i]);
          end;
        end;
    end;
  finally
    xList.Free;
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ReadMMLinkshare;
var
  xMachCfg: THostlinkShare;
  xSL: TDataSelection;
begin
  meMMLinkShare.Clear;
  try
    xMachCfg := THostlinkShare.Connect;
    with xMachCfg, meMMLinkShare.Lines do
    try
      Add(Format('CustShiftDetail    (bool): %d', [Integer(CustShiftDetail)]));
      Add(Format('ShowEmptyProdGorup (bool): %d', [Integer(ShowEmptyProdGroup)]));

      Add(Format('IntervalTimeRange  (int) : %d', [IntervalTimeRange]));
      Add(Format('OfflimitTime       (int) : %d', [OfflimitTime]));
      Add(Format('CustShiftCalID     (int) : %d', [CustShiftCalID]));
      Add(Format('DefaultShiftCalID  (int) : %d', [DefaultShiftCalID]));
      Add(Format('DefaultTrendSelection (int): %d', [Integer(DefaultTrendSelection)]));
      Add(Format('NrOfMachines       (int): %d', [NrOfMachines]));
      Add(Format('ShowEmptyProdGorup (bool): %d', [Integer(ShowEmptyProdGroup)]));


      for xSL:=dsCurShift to dsIntervalData do begin
        Add(Format('%s', [GetEnumName(TypeInfo(TDataSelection), Ord(xSL))]));
        Add(Format('  From: %s', [DateTimeToStr(ShiftSelection[xSL].TimeFrom)]));
        Add(Format('  To:   %s', [DateTimeToStr(ShiftSelection[xSL].TimeTo)]));
      end;

    finally
      xMachCfg.Free;
    end;
  except
    meMMLinkShare.Lines.Add(GetLastErrorText);
    exit;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ReadPlugins;
var
  i: Integer;
  xIndex: Integer;
  xStr: String;
begin
  mPluginsChanged := False;
  clbPlugins.Clear;

  with TmmRegistry.Create do
  try
    RootKey := cRegLM;

    if OpenKeyReadOnly(cRegLoepfeAddons) then begin
      GetKeyNames(clbPlugins.Items);
      for i:=0 to clbPlugins.Items.Count-1 do begin
        xStr := Format('%s\%s', [cRegLoepfeAddons, clbPlugins.Items[i]]);
        clbPlugins.Checked[i] := not GetRegBoolean(cRegLM, xStr, 'Disabled', False);
      end;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.rgComputerClick(Sender: TObject);
begin
  ReadCodeSite;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acDefaultsExecute(Sender: TObject);
const
  cCodeSiteCount = 18;
  cCodeSite: Array[1..cCodeSiteCount] of String = (
    'Assignments',
    'MillMasterPlugin',
    'Hostlink',
    'YMSettingsControl',
    'MMClearerAssistant',
    'MMClient',
    'MMConfiguration',
    'MMEventViewer',
    'MMLabReport',
    'MMMaConfig',
    'MMQOfflimit',
    'SpindleReport',
    'TimeHandler',
    'JobHandler',
    'StorageHandler',
    'MsgHandler',
    'TXNHandler',
    'WSCHandler'
  );
var
  i: Integer;
begin
  mCodeSiteChanged := True;
  for i:=1 to cCodeSiteCount do begin
    if clbCodeSite.Items.IndexOf(cCodeSite[i]) = -1 then
      clbCodeSite.Items.Add(cCodeSite[i]);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bReadClick(Sender: TObject);
begin
  ReadMMLinkShare;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bUpdateDebugClick(Sender: TObject);
var
  xMsg: TMMGuardRec;
begin
  FillChar ( xMsg, sizeof ( xMsg ), 0 );
  xMsg.MsgTyp := gcDebug;
  xMsg.DebugMode := 0;
  with TIPCClient.Create (gMMHost, cChannelNames[ttMMGuardMain]) do
  try
    if Write(@xMsg, sizeof(xMsg)) then
      ShowMessage('Debug notification sent.')
    else
      ShowMessage('Debug notification failed: ' + FormatErrorText(Error)); //GetLastErrorText)
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bUpdateADOClick(Sender: TObject);
begin
  with cobSQLServer do
    if Text <> '' then begin
      SetRegString(cRegLM, cRegMMCommonPath, cRegSQLServerName, Text);
      if Items.IndexOf(Text) = -1 then
        Items.Add(Text);
    end;

  with cobDatabase do
    if Text <> '' then begin
      SetRegString(cRegLM, cRegMMCommonPath, cRegDBName, Text);
      if Items.IndexOf(Text) = -1 then
        Items.Add(Text);
    end;

  bUpdateADO.Enabled := False;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.cobSQLServerChange(Sender: TObject);
begin
  bUpdateADO.Enabled := True;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.bTmoUpdateClick(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ReadTimeouts;
var
  i: Integer;
  xIndex: Integer;
  xStr: String;
  xJobTyp: TJobTyp;
  xList: TmmStringList;
begin
  mTimeoutsChanged := False;

  if cobTimeouts.Items.Count = 0 then begin
    for xJobTyp:=Low(TJobTyp) to High(TJobTyp) do
      cobTimeouts.Items.Add(GetJobName(xJobTyp));
  end;

  mTempStrList.Clear;
  lvTimeouts.Items.Clear;
  xList := TmmStringList.Create;
  with TmmRegistry.Create do
  try
    RootKey := cRegLM;
    if OpenKeyReadOnly(cRegMMTimeouts) then begin
      GetValueNames(mTempStrList);
      for i:=0 to mTempStrList.Count-1 do begin
        if GetDataType(mTempStrList[i]) = rdString then begin
          xList.CommaText := ReadString(mTempStrList[i]);
          if xList.Count = 3 then begin
            with lvTimeouts.Items.Add do begin
              Caption := mTempStrList[i];
              SubItems.Add(xList.CommaText);
            end;
          end;
        end;
      end;
    end;
  finally
    Free;
    xList.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.lvTimeoutsSelectItem(Sender: TObject; Item: TListItem;
  Selected: Boolean);
var
  xJobTyp: TJobTyp;
begin
  if Selected then begin
    xJobTyp := GetJobValue(Item.Caption);
    mTempStrList.CommaText := Item.SubItems.Strings[0];
    with mTempStrList do begin
      if Count = 3 then begin
        edJobTmo.Text := Strings[0];
        edMsgTmo.Text := Strings[1];
        edMsgRep.Text := Strings[2];
      end
      else begin
        edJobTmo.AsInteger := cJobTimeout[xJobTyp].JobListTmo;
        edMsgTmo.AsInteger := cJobTimeout[xJobTyp].MsgListTmo;
        edMsgRep.AsInteger := cJobTimeout[xJobTyp].MsgListRep;
      end;
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.edTimeoutChange(Sender: TObject);
var
  xIndex: Integer;
  xListItem: TListItem;
begin
  xListItem := lvTimeouts.Selected;
  if Assigned(xListItem) then 
    xListItem.SubItems.Text := Format('%d,%d,%d', [edJobTmo.AsInteger, edMsgTmo.AsInteger, edMsgRep.AsInteger]);

  mTimeoutsChanged := True;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.acTmoUpdateExecute(Sender: TObject);
var
  i: Integer;
begin
  sbError.SimpleText := '';
  with TmmRegistry.Create do
  try
    RootKey := cRegLM;
    if OpenKey(cRegMMTimeouts, True) then begin
      for i:=0 to lvTimeouts.Items.Count-1 do
        WriteString(lvTimeouts.Items[i].Caption, Trim(lvTimeouts.Items[i].SubItems.Text));
    end else
      sbError.SimpleText := GetLastErrorText;
  finally
    Free;
  end;

  mTimeoutsChanged := False;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.acTmoAddExecute(Sender: TObject);
var
  i: Integer;
  xIndex: Integer;
  xJobTyp: TJobTyp;
  xListItem: TListItem;
begin
  with lvTimeouts.Items do begin
    for i:=0 to Count-1 do begin
      if AnsiSameText(Item[i].Caption, cobTimeouts.Text) then begin
        ShowMessage('This JobType is already in list.');
        Exit;
      end;
    end; // for i

    // neuer Job hinzufügen
    xJobTyp   := GetJobValue(cobTimeouts.Text);
    xListItem := Add;
    with xListItem do begin
      Caption := cobTimeouts.Text;
      with cJobTimeout[xJobTyp] do
        SubItems.Add(Format('%d,%d,%d', [JobListTmo, MsgListTmo, MsgListRep]));
    end;
    lvTimeouts.Selected := xListItem;
    mTimeoutsChanged    := True;
  end; // with lvTimeouts
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.acTmoDeleteExecute(Sender: TObject);
begin
  if MessageDlg('Delete selected value?', mtWarning, mbOKCancel, 0) = mrOK then begin
    with TmmRegistry.Create do
    try
      RootKey := cRegLM;
      if OpenKey(cRegMMTimeouts, True) then
        DeleteValue(lvTimeouts.Selected.Caption);
    finally
      Free;
    end;
    ReadTimeouts;
  end;
end;
//:-----------------------------------------------------------------------------

end.

