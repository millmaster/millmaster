unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton;

type
  TfrmMain = class(TForm)
    bRefreshMsg: TmmButton;
    procedure bRefreshMsgClick(Sender: TObject);
  private
  public
  end;

var
  frmMain: TfrmMain;

implementation
{$R *.DFM}
uses
  MMMessages;

procedure TfrmMain.bRefreshMsgClick(Sender: TObject);
var
  xHnd: THandle;
begin
  xHnd := RegisterWindowMessage(cMsgAppl);
  MMBroadCastMessage(xHnd, cMMRefresh);
end;


end.
