object frmMain: TfrmMain
  Left = 243
  Top = 123
  BorderStyle = bsToolWindow
  Caption = 'Floor Refresh'
  ClientHeight = 41
  ClientWidth = 221
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object bRefreshMsg: TmmButton
    Left = 8
    Top = 8
    Width = 201
    Height = 25
    Caption = 'Refresh Floor (Msg)'
    TabOrder = 0
    Visible = True
    OnClick = bRefreshMsgClick
    AutoLabel.LabelPosition = lpLeft
  end
end
