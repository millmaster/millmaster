unit u_LongTerm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, u_Query, u_CodeSite;


type
  TForm1 = class(TForm)
    Button1: TButton;
    Memo1: TMemo;
    QryPath: TEdit;
    bbQryPath: TBitBtn;
    OpenDialog1: TOpenDialog;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure bbQryPathClick(Sender: TObject);
  private
    { Private declarations }
    mQueryPath : String;
    mLogPath   : String;
  public
    { Public declarations }
  end;

 // function CheckQueryLogDir: String;
 // function ExequteQuery(aQuery, aLogPath, aUser, aPWD : String):Boolean;

var
  Form1: TForm1;


const cLongtermQry = 'CreateLongtermWeek.qry';



implementation

{$R *.DFM}


Uses LoepfeGlobal ;
(*
//------------------------------------------------------------------------------
function CheckQueryLogDir: String;
 var xKey, xValue, xPath : String;
begin
  xKey   := 'SOFTWARE\Loepfe\MillMaster';
  xValue := 'MillMasterRootPath';
  xPath := GetRegString(cRegLM,  xKey, xValue, cDefaultMMRootSRV ) + '\log';
  if not DirectoryExists( xPath ) then CreateDir(xPath);
  result := xPath;
end;
//------------------------------------------------------------------------------
function ExequteQuery(aQuery, aLogPath, {aMMLogFile,} aUser, aPWD : String):Boolean;
var xShowAppl, xApplRet, xLogRet : Boolean;
    xProg, xQuery, xParam, xLogFile  :String;
    xValue, xMSG, xText : String;
    xLogMMHistory, xLogReadMe, xUser, xPWD : String;
begin

  if not DirectoryExists(cLogFileDir) then
     ForceDirectories(cLogFileDir);

  xLogMMHistory   :=  Format('%s\%s' , [cLogFileDir, cLOGFILE_HISTORY]);
  xLogReadMe      :=  Format('%s\%s' , [cLogFileDir, cReadMeFILE]);

  xProg   := cSQLExe;
  xQuery  := aQuery;

  xUser  := aUser;
  xPWD   := aPWD;

  if not FileExists( xQuery ) then begin
     xMSG := Format('Query %s does not exists.',[xQuery]);
     CodeSiteMSGError( PChar(xMSG) );
     xMSG := Format('failed    Query %s does not exists.',[xQuery]);
     WriteMsgToLog(PChar(xLogReadMe), PChar(xMSG), TRUE );
     WriteMsgToLog(PChar(xLogMMHistory), PChar(xMSG), FALSE);
     Result := FALSE;
     exit;
  end;

  //LogFile von Query
  xLogFile    := ExtractFileName(xQuery) ;
  Delete(xLogFile, Pos('.', xLogFile) , 4 );
  xLogFile:= aLogPath + '\'+ xLogFile + '.log';

  //xParam := Format(' -U "MMSystemSQL" -P "netpmek32" -i "%s" -o "%s" ',
  //                 [xQuery, xLogFile ]);

  //xParam := Format(' -U "sa" -P "nt50beta" -i "%s" -o "%s" ',
  //                 [xQuery, xLogFile ]);


  xParam := Format(' -U "%s" -P "%s" -i "%s" -o "%s" ',
                   [xUser, xPWD, xQuery, xLogFile ]);

  xShowAppl:= FALSE;

  try
      xLogRet  := TRUE;
      xApplRet := TRUE;

      //Query ausfuehren
      Application.ProcessMessages;
      xApplRet := Boolean( CallAppAndWait(PChar(xProg), PChar(xParam), xShowAppl) );
      Application.ProcessMessages;
      //Logfile uebrpruefen
      xLogRet := Boolean( IsLogFileOk( PChar(xLogFile) ) );

      if xApplRet and xLogRet then begin

         xMSG := Format('successful    Query %s and logfile %s are ok',
                        [ExtractFileName(xQuery), ExtractFileName(xLogFile)]);
         WriteMsgToLog(PChar(xLogReadMe), PChar(xMSG), TRUE);
         WriteMsgToLog(PChar(xLogMMHistory), PChar(xMSG), FALSE);

         xMSG := Format('Query %s and logfile %s are ok',
                        [ExtractFileName(xQuery), ExtractFileName(xLogFile)]);
         CodeSiteSendMsg(PChar(xMSG) );
      end else begin
         xMSG:= Format('failed    Check query %s',[xQuery]);

         if not xLogRet and not xApplRet  then
            xMSG := Format('failed    Unknown error in Query %s. Check logfile %s.',[xQuery, xLogFile])
         else
           if not xLogRet then
              xMSG := Format('failed    Query error in %s. Check logfile %s.',[xQuery, xLogFile])
           else
             if not xApplRet then
                xMSG := Format('failed    Error with executing %s',[xQuery])
             else
                xMSG:= Format('failed    Check query %s',[xQuery]);

         CodeSiteMSGError( PChar(xMSG) );
         //xMSG := Format('Query %s not successful',[xQuery]);
         WriteMsgToLog(PChar(xLogReadMe), PChar(xMSG), TRUE );
         WriteMsgToLog(PChar(xLogMMHistory), PChar(xMSG), FALSE);
         //CodeSiteSendMsgError(PChar(xMSG) );
         ShowMessage(xMSG);
      end;

  except
     on E: Exception do begin
        xMSG := 'Error in ExequteQuery(): ';
        xText := Format('%s. Last Query %s',[E.Message, xQuery]);
        if not xApplRet then begin
            xMSG := 'Error in ExequteQuery : ';
            xText := Format('%s. Last Query %s',[E.Message, xQuery]);
        end;

        if not xLogRet then begin
            xMSG := 'Error in IsLogFileOk : ';
            xText := Format('%s. Last logfile %s',[E.Message, xLogFile]);
        end;

        CodeSiteMSGTextError(PChar(xMSG), PChar(xText) );
        ShowMessage( xMSG + xText);

        xMSG := 'failed    ' + xMSG + xText;
        WriteMsgToLog(PChar(xLogReadMe), PChar(xMSG), TRUE );
        WriteMsgToLog(PChar(xLogMMHistory), PChar(xMSG), FALSE);
        xLogRet := FALSE;
     end;
  end;
   Result := xApplRet and xLogRet;
end;
//------------------------------------------------------------------------------
*)


procedure TForm1.FormCreate(Sender: TObject);
var xISQLPath : String;
    xSQLReg   : String;
begin

  mQueryPath:= ExtractFilePath(Application.ExeName);
//  mLogPath:= GetRegString(cRegLM, cRegMillMasterPath, cRegMillMasterRootPath, '') + '\Log';

  mLogPath:= CheckQueryLogDir ;

  QryPath.Text :=  Format('%s%s',[mQueryPath, cLongtermQry]);

  // Check ISQL.exe
  xSQLReg := 'SOFTWARE\Microsoft\Microsoft SQL Server\80\Tools\ClientSetup';
  xISQLPath:= GetRegString(cRegLM, xSQLReg, 'SQLPath', '');
  xISQLPath:=   xISQLPath + '\Binn';
  if not FileExists(xISQLPath + '\' + cSQLExe) then begin
     MessageDlg('ISQl.exe not found', mtWarning,[mbOk],0);
     bbQryPath.Enabled := FALSE;
  end;


  Memo1.Clear;

  Label1.Caption := '';

  Button1.Enabled := Boolean(ExistsMMWinding);
  if not Button1.Enabled then
     Label1.Caption := 'MMWinding does not exists on this machine';
end;

procedure TForm1.Button1Click(Sender: TObject);
var xRet : Boolean;
//    xFile : TFileStream;
    xFile : TStrings;
    xQryFile, xExt : String;
begin
  Screen.Cursor := crHourGlass;
  Memo1.Clear;

  xQryFile:= ExtractFileName( QryPath.text );
  xExt := ExtractFileExt( xQryFile );
  xQryFile:= StringReplace(xQryFile, xExt, '.log', [rfReplaceAll]);

  xRet := ExequteQuery(QryPath.text, mLogPath, cSQLMMSystemSQLUser, cSQLMMSystemSQLUserPWD);

  Memo1.Lines.LoadFromFile(mLogPath + '\' + xQryFile);
  Memo1.Lines.Insert(0, 'Logfile :'+ mLogPath + '\' + cLongtermQry );
  Memo1.Lines.Insert(1, '' );
  Memo1.Lines.Insert(2, '' );

  Screen.Cursor := crDefault;
end;

procedure TForm1.bbQryPathClick(Sender: TObject);
begin

  OpenDialog1.InitialDir := mQueryPath;
  OpenDialog1.Execute;

  if ExtractFileExt(OpenDialog1.FileName) = '.qry' then
     QryPath.Text := OpenDialog1.FileName;

end;

end.
