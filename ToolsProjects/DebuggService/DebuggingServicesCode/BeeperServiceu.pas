unit BeeperServiceu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, 
  Controls, SvcMgr, Dialogs;

type
  TService1 = class(TService)
    procedure Service1Execute(Sender: TService);
  private
    { Private declarations }
    procedure Ontimerproc(Sender: TObject);
  public
    function GetServiceController: TServiceController; override;
    { Public declarations }
  end;

var
  Service1: TService1;

implementation

uses
  Extctrls;

{$R *.DFM}

procedure ServiceController(CtrlCode: DWord); stdcall;
begin
  Service1.Controller(CtrlCode);
end;

function TService1.GetServiceController: TServiceController;
begin
  Result := ServiceController;
end;

procedure TService1.Ontimerproc(Sender: TObject);
begin
  Beep; // set a Break point here to enable debugging
end;

procedure TService1.Service1Execute(Sender: TService);
var
  fTimer: TTimer;
begin

  if FileExists(ExtractFilePath(paramstr(0))+'Debugfile.txt') then
  begin
    fTimer := TTimer.create(nil);
    ftimer.interval := 2000;
    ftimer.ontimer := Ontimerproc;
    ftimer.enabled := true;

    while not Terminated and ftimer.enabled do
      ServiceThread.ProcessRequests(True);

    ftimer.enabled := false;
    FreeAndNil(ftimer);
  end;

  while not Terminated do
  begin

    // Do your normal processing here

    ServiceThread.ProcessRequests(True);
  end;

end;

end.
