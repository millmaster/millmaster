program Ecex_AT_1MinFromNow;
{$APPTYPE CONSOLE}
uses
  windows,SysUtils;

var
  fcmdstr:string;
begin
  if FindCmdLineSwitch('StartDelphi',['/',' ','-'],true) then begin
    fcmdstr:='AT '+ FormatDateTime('h:n',Now+0.000694) // 1Minute
                  + ' /interactive delphi32.EXE';
    WinExec(PChar(fcmdstr),SW_SHOWMINNOACTIVE);
  end else begin
    fcmdstr:='AT '+ FormatDateTime('h:n',Now+0.000694) // 1Minute
                  + ' /interactive CMD.EXE';
    WinExec(PChar(fcmdstr),SW_SHOWMINNOACTIVE);
  end;// if FindCmdLineSwitch('StartDelphi',['/',' ','-'],true) then begin
end.
