object fPing: TfPing
  Left = 528
  Top = 251
  Width = 328
  Height = 239
  Caption = 'MM Ping'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 10
    Top = 15
    Width = 105
    Height = 13
    Caption = 'Check connection to: '
    FocusControl = edHost
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edHost: TmmEdit
    Left = 10
    Top = 30
    Width = 300
    Height = 21
    Color = clWindow
    TabOrder = 0
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mPingResult: TmmMemo
    Left = 10
    Top = 104
    Width = 300
    Height = 97
    ScrollBars = ssVertical
    TabOrder = 1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bCheck: TmmButton
    Left = 10
    Top = 65
    Width = 300
    Height = 25
    Caption = 'Check connection'
    TabOrder = 2
    Visible = True
    OnClick = bCheckClick
    AutoLabel.LabelPosition = lpLeft
  end
  object IdIcmpClient: TIdIcmpClient
    Port = 80
    Left = 256
  end
end
