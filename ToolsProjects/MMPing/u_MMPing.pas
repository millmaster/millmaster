{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMPing.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: - Prueft eine Netzverbindung mittels PING zu einem PC
| Develop.system: Windows 2K
| Target.system.: Windows 2K, XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 11.09.2003  1.00  SDo | File created
===============================================================================}
unit u_MMPing;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmMemo, mmLabel, mmEdit, IdBaseComponent,
  IdComponent, IdRawBase, IdRawClient, IdIcmpClient;

type
  TfPing = class(TForm)
    IdIcmpClient: TIdIcmpClient;
    edHost: TmmEdit;
    mmLabel1: TmmLabel;
    mPingResult: TmmMemo;
    bCheck: TmmButton;
    procedure bCheckClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fPing: TfPing;

implementation

{$R *.DFM}
Uses LoepfeGlobal;


procedure TfPing.bCheckClick(Sender: TObject);
var xPCName :String;
begin
  screen.Cursor := crHourGlass;
  mPingResult.Clear;

  xPCName := LoepfeGlobal.MMGetComputerName;

  with TIdIcmpClient.Create(NIL) do begin

       if edHost.text = '' then
         Host := xPCName
       else
         Host := edHost.text;
         
       Port := 80;

       try


           Ping;
           Ping;

           case ReplyStatus.ReplyStatusType of
               rsEcho:  begin

                           mPingResult.Lines.Add (format('Response from host ''%s'' (%s) in %d millisec.',
                                                   [Host,
                                                    ReplyStatus.FromIpAddress,
                                                    ReplyStatus.MsRoundTripTime]));

                        end;
                else
                      mPingResult.Lines.Add ( Format( 'No connection to ''%s'' possible.',[Host]) );
           end;

      except
         on e: exception do begin
            mPingResult.Lines.Add( Format( 'Ping error with ''Host'' %s. Error msg : %s',[Host, e.Message]) );
         end;
       end;

       Free;
  end;

  screen.Cursor := crDefault;
end;

end.
