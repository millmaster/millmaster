unit MMHtmlHelp;

interface

uses
  Classes, Controls, Forms, Windows, LoepfeGlobal;

//------------------------------------------------------------------------------
const
  cMMHelpMask    = 'MMHelp-%.3d.chm';
  cFloorHelpMask = 'Floor-%.3d.chm';
  cDefaultLoepfeIndex = 1;

//------------------------------------------------------------------------------
type
  TMMHtmlHelp = class(TComponent)
  private
    fAutoConfig: Boolean;
    fHelpFile: String;
    fLoepfeIndex: Integer;
    fCallerHWnd: HWnd;
    mHelpFilePath: String;
    FHelpMask: String;
    procedure DefineHelpFile;
    procedure SetAutoConfig(const Value: Boolean);
    procedure SetHelpFile(const Value: String);
    procedure SetLoepfeIndex(const Value: Integer);
    procedure SetHelpMask(const Value: String);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    function ShowHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
    procedure ShowHelpContext(aHelpContext: Integer);
    property CallerHWND: HWnd read fCallerHWnd write fCallerHWnd;
  published
    property AutoConfig: Boolean read fAutoConfig write SetAutoConfig default False;
    property HelpFile: String read fHelpFile write SetHelpFile;
    property HelpMask: String read FHelpMask write SetHelpMask;
    property LoepfeIndex: Integer read fLoepfeIndex write SetLoepfeIndex default 1;
  end;


//------------------------------------------------------------------------------
// PROTOTYPE
//------------------------------------------------------------------------------
function HtmlHelp(aCaller: HWnd; aFile: PChar; aCommand: DWord; aData: DWord): HWND; stdcall;

procedure Register;
//------------------------------------------------------------------------------
implementation

uses
  SysUtils;

//------------------------------------------------------------------------------
const
  cHtmlHelpLib     = 'HHCtrl.ocx';
  cRegHelpFilePath = 'HelpFilePath';       // String for TMMHtmlHelp component

function HtmlHelp; external cHtmlHelpLib name 'HtmlHelpA';

procedure Register;
begin
  RegisterComponents('LOEPFE', [TMMHtmlHelp]);
end;

//------------------------------------------------------------------------------
// RoboHELP HTML Edition Report
//------------------------------------------------------------------------------
// Used Files Report for: WindingM
// Created by kast on 02.02.2000 10:20:00
// Project Location: D:\Bedi\WindingM\Ger\WindingM.MPJ
//
// !!! IMPORTANT: new index strings only add at the end of the array otherwise we
// !!! have to renumber all index in each application!!
//
const
  cHelpContextCount = 135;
  cHelpContext: Array[1..cHelpContextCount] of String = (
      // FFolder: D:\Bedi\WindingM\Ger\
{001} 'Ger\FLOOR_Onlineueberwachung-aktivieren.htm',
      // FFolder: D:\Bedi\WindingM\Ger\WindingMaster\Betriebskalender\
      'WindingMaster\Betriebskalender\BETRKAL_Fenster_Betriebskalender.htm',
      'WindingMaster\Betriebskalender\BETRKAL-definition.htm',
      // FFolder: D:\Bedi\WindingM\Ger\WindingMaster\Spulstellenbericht\
      'WindingMaster\Spulstellenbericht\SPST_Spulstellenbericht.htm',
{005} 'WindingMaster\Spulstellenbericht\SPST_DLG_Maschinenbericht.htm',
      'WindingMaster\Spulstellenbericht\SPST_DLG_Spulstellenbericht.htm',
      'WindingMaster\Spulstellenbericht\SPST_DLG_Trend-Klassierung.htm',
      'WindingMaster\Spulstellenbericht\SPST_Neue-Klassierungsgruppe-definieren.htm',
      // FFolder: D:\Bedi\WindingM\Ger\WindingMaster\USR\
      'WindingMaster\USR\USR_USR.htm',
{010} 'WindingMaster\USR\USR_Bericht-bearbeiten.htm',
      'WindingMaster\USR\USR_Neuen-Bericht.htm',
      // FFolder: D:\Bedi\WindingM\Ger\WindingMaster\Schichtkalender\
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalenderbereich_bearbeiten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalendernamen_bearbeiten.htm',
{015} 'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen-Schichtkalender.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtgruppen-erstellen-bearbeiten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-bearbeiten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-drucken.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-bearbeiten.htm',
{020} 'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-uebernehmen.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-bearbeiten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standart-Schichten-kopieren.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Neue-Schichten-eintragen.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Neues-Schichtmuster.htm',
{025} 'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definition.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-drucken.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-Einleitung.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster.htm',
{030} 'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster-bearbeiten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Standardschichten.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Trend-einer-Gruppe-ansehen.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Gruppenfarbe-veraendern.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Schichtgruppen-definieren.htm',
{035} 'WindingMaster\Schichtkalender\SHIFTCAL_Spezial-Schichten-eintragen.htm',
      'WindingMaster\Schichtkalender\SHIFTCAL_Standard-Schichten-definieren.htm',
      // FFolder: D:\Bedi\WindingM\Ger\WindingMaster\Offlimit\
      'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Einstellungen.htm',
      'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Ursachen.htm',
      'WindingMaster\Offlimit\OFFL_DLG_Offline-Offlimit-Filter.htm',
{040} 'WindingMaster\Offlimit\OFFL_DLG_Online-Offlimit-Filter-bearbeiten.htm',
      'WindingMaster\Offlimit\OFFL_Einstellungen_anpassen.htm',
      'WindingMaster\Offlimit\OFFL_DLG_Fenster_Offline_Offlimit.htm',
      'WindingMaster\Offlimit\OFFL_DLG_Fenster_Online_Offlimit.htm',
      'WindingMaster\Offlimit\OFFL_Grenzwerte.htm',
{045} 'WindingMaster\Offlimit\OFFL_Mittelwert.htm',
      'WindingMaster\Offlimit\OFFL_Offlimit_Zeiten.htm',
      'WindingMaster\Offlimit\OFFL_Offlimitbericht.htm',
      'WindingMaster\Offlimit\OFFL_Offline_Offlimit.htm',
      'WindingMaster\Offlimit\OFFL_Online_Offlimit.htm',
{050} 'WindingMaster\Offlimit\OFFL_Offlimit-Analyse.htm',
      'WindingMaster\Offlimit\OFFL_Offlimitursache-definieren.htm',
      // FFolder: D:\Bedi\WindingM\Ger\Floor\
      'Floor\FLOOR_Bestehende-Saaluebersicht-laden.htm',
      'Floor\FLOOR_Entwurfsmodus-aktivieren.htm',
      'File_not_Found.htm',                      // replaced at 7.2.2000 -> KAST
{055} 'Floor\FLOOR_Maschinen-Anzeige-anpassen.htm',
      'Floor\FLOOR_Maschinenform-anpassen.htm',
      'Floor\FLOOR_Neues-Saallayout.htm',
      'Floor\FLOOR_Maschine-einfuegen.htm',
      'Floor\FLOOR_Saalelementkopieren-einfuegen.htm',
{060} 'Floor\FLOOR_Sprache-waehlen.htm',
      'Floor\FLOOR_Toolbar-anpassen.htm',
      // Folder: D:\Bedi\WindingM\Ger\Allgemein\
      'Allgemein\MM_Datenbank_Zugriff.htm',
      'Allgemein\MM_Imperfektionen.htm',
      'Allgemein\MM_Kontaktadresse.htm',
{065} 'Allgemein\MM_Allgemein.htm',
      'Allgemein\MM_DispoMaster.htm',
      'Allgemein\MM_LabMaster.htm',
      'Allgemein\MM_Langzeitdaten-Haltung.htm',
      'Allgemein\MM_Module.htm',
{070} 'Allgemein\MM_Optionen-WindingMaster.htm',
      'Allgemein\MM_Produktions-Zeiten.htm',
      'File_not_Found.htm',                      // replaced at 7.2.2000 -> KAST
      'Allgemein\MM_Qualittsdaten.htm',
      'Allgemein\MM_Reiniger-Assistent.htm',
{075} 'Allgemein\MM_Ringmaster.htm',
      'Allgemein\MM_WindingMaster.htm',
      // FFolder: D:\Bedi\WindingM\Ger\Benutzerverwaltung\
      'Benutzerverwaltung\BV_Administration.htm',
      'Benutzerverwaltung\BV_Grp-aendern.htm',
      'Benutzerverwaltung\BV_Rechte-anpassen.htm',
{080} 'Benutzerverwaltung\BV_Neuer-Benutzer.htm',
      'Benutzerverwaltung\BV_Passwort-aendern.htm',
      'Benutzerverwaltung\BV_Anmelden.htm',
      'Benutzerverwaltung\BV_Benutzerrechte.htm',
      'Benutzerverwaltung\BV_Security.htm',
      // FFolder: D:\Bedi\WindingM\Ger\System\
{085} 'System\SYS_Eventviewer.htm',
      'System\SYS_DLG_Fenster_Eventviewer.htm',
      'System\SYS_DLG_Fenster_MMClient.htm',
      'System\SYS_DLG_Setup_Jobs_Einstellungen.htm',
      'System\SYS_Meldungsarten.htm',
{090} 'System\SYS_DLG_Setup_Allgemein.htm',
      'System\SYS_DLG_Setup_MMClient_Einstellungen.htm',
      'System\SYS_Randbedingungen-definitionen.htm',
      'System\SYS_Start-MillMaster.htm',
      'System\SYS_Status.htm',
{095} 'System\SYS_Stop-MillMaster.htm',
      'System\SYS_DLG_Setup_Einstellungen.htm',
      'System\SYS_DLG_Setup_Versionsverzeichnis.htm',
      'System\SYS_Automatisch-Ausdrucken.htm',
      'System\SYS_Hintergrundprozesse.htm',
{100} 'System\SYS_MMClient.htm',
      'System\SYS_Systemfunktionen.htm',
      'System\SYS_Systemzusammenhaenge.htm',
      'System\SYS_Zeitintervall-Aktualisierung.htm',
      // FFolder: D:\Bedi\WindingM\Ger\Reiniger\
      'Reiniger\RE_Einstellungen.htm',
      // FFolder: D:\Bedi\WindingM\Ger\LabMaster\
{105} 'LabMaster\LM_SFI.htm',
      // FFolder: D:\Bedi\WindingM\Ger\Globale-Fenster-und-Funktionen\
      'Globale-Fenster-und-Funktionen\GV_Beobachtungszeit-waehlen.htm',
      'Globale-Fenster-und-Funktionen\DLG_Drucken.htm',
      'Globale-Fenster-und-Funktionen\DLG_Anzeigeformat.htm',
      'Globale-Fenster-und-Funktionen\DLG_Datenreihen-auswaehlen-und-bearbeiten.htm',
{110} 'Globale-Fenster-und-Funktionen\DLG_Datenreihensortieren.htm',
      'Globale-Fenster-und-Funktionen\DLG_Grafikoptionen.htm',
      'Globale-Fenster-und-Funktionen\DLG_Parameter-fuer-Graphik.htm',
      'Globale-Fenster-und-Funktionen\DLG_Profile-bearbeiten.htm',
      'Globale-Fenster-und-Funktionen\DLG_Tabelleneigenschaften.htm',
{115} 'Globale-Fenster-und-Funktionen\DLG_Zeitraum-auswaehlen.htm',
      'Globale-Fenster-und-Funktionen\GV_Parameter_ein-_ausblenden.htm',
      'Globale-Fenster-und-Funktionen\GV_GrafikParameterEinstellungenaendern.htm',
      'Globale-Fenster-und-Funktionen\GV_Neues-Profil-erstellen.htm',
      'Globale-Fenster-und-Funktionen\GV_Profil-als-Standard-speichern.htm',
{120} 'Globale-Fenster-und-Funktionen\GV_Profil-laden.htm',
      'Globale-Fenster-und-Funktionen\GV_Profil-loeschen.htm',
      'Globale-Fenster-und-Funktionen\GV_Profil-speichern.htm',
      'Globale-Fenster-und-Funktionen\GV_Sortierung_in_der_Tabelle_aendern.htm',
      'Globale-Fenster-und-Funktionen\GV_Spalten_ein-_ausblenden_in_der_Tabelle.htm',
      // FFolder: D:\Bedi\WindingM\Ger\Zuordnung\
{125} 'Zuordnung\ZUO_Definition_Abgleich_Adjust.htm',
      'Zuordnung\ZUO_Definition_Maschinenbereich.htm',
      'Zuordnung\ZUO_Definition_Pilotspulstelle.htm',
      'Zuordnung\ZUO_DLG_Fenster_Zuordnung.htm',
      'Zuordnung\ZUO_DLG_Zusaetzliche_Einstellungen.htm',
{130} 'Zuordnung\ZUO_Neue_Reinigervorlage_erstellen.htm',
      'Zuordnung\ZUO_Reinigereinstellung_zuordnen.htm',
      'Zuordnung\ZUO_Reinigereinstellungen_ueberpruefen.htm',
      'Zuordnung\ZUO_Zuordnung_stoppen.htm',
      'Zuordnung\ZUO_DLG_Reinigereinstellungen_bestaetigen.htm',
      'FLOOR_Definition_Maschinenlayout.htm'
  );
//------------------------------------------------------------------------------
const
// Commands to pass to HtmlHelp()
  HH_DISPLAY_TOPIC        = $0000;
  HH_HELP_FINDER          = $0000;  // WinHelp equivalent
  HH_DISPLAY_TOC          = $0001;  // not currently implemented
  HH_DISPLAY_INDEX        = $0002;  // not currently implemented
  HH_DISPLAY_SEARCH       = $0003;  // not currently implemented
  HH_SET_WIN_TYPE         = $0004;
  HH_GET_WIN_TYPE         = $0005;
  HH_GET_WIN_HANDLE       = $0006;
  HH_ENUM_INFO_TYPE       = $0007;  // Get Info type name, call repeatedly to enumerate, -1 at end
  HH_SET_INFO_TYPE        = $0008;  // Add Info type to filter.
  HH_SYNC                 = $0009;
  HH_RESERVED1            = $000A;
  HH_RESERVED2            = $000B;
  HH_RESERVED3            = $000C;
  HH_KEYWORD_LOOKUP       = $000D;
  HH_DISPLAY_TEXT_POPUP   = $000E;  // display string resource id or text in a popup window
  HH_HELP_CONTEXT         = $000F;  // display mapped numeric value in dwData
  HH_TP_HELP_CONTEXTMENU  = $0010;  // text popup help, same as WinHelp HELP_CONTEXTMENU
  HH_TP_HELP_WM_HELP      = $0011;  // text popup help, same as WinHelp HELP_WM_HELP
  HH_CLOSE_ALL            = $0012;  // close all windows opened directly or indirectly by the caller
  HH_ALINK_LOOKUP         = $0013;  // ALink version of HH_KEYWORD_LOOKUP
  HH_GET_LAST_ERROR       = $0014;  // not currently implemented // See HHERROR.h
  HH_ENUM_CATEGORY        = $0015;  // Get category name, call repeatedly to enumerate, -1 at end
  HH_ENUM_CATEGORY_IT     = $0016;  // Get category info type members, call repeatedly to enumerate, -1 at end
  HH_RESET_IT_FILTER      = $0017;  // Clear the info type filter of all info types.
  HH_SET_INCLUSIVE_FILTER = $0018;  // set inclusive filtering method for untyped topics to be included in display
  HH_SET_EXCLUSIVE_FILTER = $0019;  // set exclusive filtering method for untyped topics to be excluded from display
  HH_INITIALIZE           = $001C;  // Initializes the help system.
  HH_UNINITIALIZE         = $001D;  // Uninitializes the help system.
  HH_PRETRANSLATEMESSAGE  = $00FD;  // Pumps messages. (NULL, NULL, MSG*).
  HH_SET_GLOBAL_PROPERTY  = $00FC;  // Set a global property. (NULL, NULL, HH_GPROP)

//------------------------------------------------------------------------------
// TMMHtmlHelp
//------------------------------------------------------------------------------
constructor TMMHtmlHelp.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fAutoConfig    := False;
  fCallerHWnd    := 0;
  fHelpFile      := '';
  fHelpMask      := cMMHelpMask;
  fLoepfeIndex   := 1;
  mHelpFilePath  := '';
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.DefineHelpFile;
var
  xStr: String;
begin
  if (([csLoading, csDesigning] * ComponentState) = []) and fAutoConfig then begin
    try
      xStr := mHelpFilePath + Format(fHelpMask, [fLoepfeIndex]);
      // if HelpFile not available try of default HelpFile (German)
      if not FileExists(xStr) then
        xStr := mHelpFilePath + Format(fHelpMask, [cDefaultLoepfeIndex]);
      if not FileExists(xStr) then
        xStr := '';
    except
      xStr := '';
    end;

    fHelpFile := xStr;
  end;
end;
//------------------------------------------------------------------------------
destructor TMMHtmlHelp.Destroy;
begin
  Application.OnHelp := Nil;

  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.Loaded;
begin
  inherited Loaded;

  fCallerHWnd        := Application.Handle;
  Application.OnHelp := ShowHelp;

  DefineHelpFile;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetAutoConfig(const Value: Boolean);
begin
  if Value <> fAutoConfig then begin
    fAutoConfig := Value;
    mHelpFilePath := GetRegString(cRegLM, cRegMMCommonPath, cRegHelpFilePath, '');
    if mHelpFilePath <> '' then begin
      mHelpFilePath := IncludeTrailingBackslash(mHelpFilePath);
      DefineHelpFile;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetHelpFile(const Value: String);
begin
  if Value <> fHelpFile then begin
    fHelpFile := Value;
    fAutoConfig := False;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetHelpMask(const Value: String);
begin
  if Value <> fHelpMask then begin
    fHelpMask := Value;
    DefineHelpFile;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetLoepfeIndex(const Value: Integer);
begin
  if (Value <> fLoepfeIndex) and (Value > 0) then begin
    fLoepfeIndex := Value;
    DefineHelpFile;
  end;
end;
//------------------------------------------------------------------------------
function TMMHtmlHelp.ShowHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
  Result := True;
  CallHelp := False;
  if fHelpFile <> '' then begin
    if (Data > 0) and (Data <= cHelpContextCount) then
      HtmlHelp(fCallerHWnd, PChar(fHelpFile), HH_DISPLAY_TOPIC, DWord(cHelpContext[Data]))
    else
      HtmlHelp(fCallerHWnd, PChar(fHelpFile), Command, 0);
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.ShowHelpContext(aHelpContext: Integer);
var
  xCallHelp: Boolean;
begin
  if aHelpContext = 0 then
    ShowHelp(HH_DISPLAY_TOC, 0, xCallHelp)
  else
    ShowHelp(HH_DISPLAY_TOPIC, aHelpContext, xCallHelp);
end;
//------------------------------------------------------------------------------

end.

