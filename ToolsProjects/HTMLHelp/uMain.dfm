object Form1: TForm1
  Left = 241
  Top = 123
  Width = 498
  Height = 532
  HelpContext = 135
  Caption = 'Help Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbHelpPath: TmmListBox
    Left = 0
    Top = 49
    Width = 490
    Height = 407
    Align = alClient
    Enabled = True
    ItemHeight = 13
    Items.Strings = (
      #39'globale-fenster-und-funktionen\DLG_Anzeigeformat.htm'#39','
      
        #39'globale-fenster-und-funktionen\DLG_Berichtseinstellung_festlege' +
        'n.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Datenreihensortieren.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Drucken.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Grafikoptionen.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Parameter-fuer-Graphik.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Profile-bearbeiten.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Tabelleneigenschaften.htm'#39','
      #39'globale-fenster-und-funktionen\DLG_Zeitraum-auswaehlen.htm'#39','
      
        #39'globale-fenster-und-funktionen\GV_Beobachtungszeit-waehlen.htm'#39 +
        ','
      
        #39'globale-fenster-und-funktionen\GV_Grafik_Sortierung_anpassen.ht' +
        'm'#39','
      
        #39'globale-fenster-und-funktionen\GV_GrafikParameterEinstellungena' +
        'endern.htm'#39','
      #39'globale-fenster-und-funktionen\GV_Neues-Profil-erstellen.htm'#39','
      
        #39'globale-fenster-und-funktionen\GV_Parameter_ein-_ausblenden.htm' +
        #39','
      
        #39'globale-fenster-und-funktionen\GV_Profil-als-Standard-speichern' +
        '.htm'#39','
      #39'globale-fenster-und-funktionen\GV_Profil-laden.htm'#39','
      #39'globale-fenster-und-funktionen\GV_Profil-loeschen.htm'#39','
      #39'globale-fenster-und-funktionen\GV_Profil-speichern.htm'#39','
      
        #39'globale-fenster-und-funktionen\GV_Sortierung_in_der_Tabelle_aen' +
        'dern.htm'#39','
      
        #39'globale-fenster-und-funktionen\GV_Spalten_ein-_ausblenden_in_de' +
        'r_Tabelle.htm'#39','
      #39'Benutzerverwaltung\BV_Abmelden_vom_MM.htm'#39','
      #39'Benutzerverwaltung\BV_Anmelden.htm'#39','
      #39'Benutzerverwaltung\BV_Anmeldung_am_MM.htm'#39','
      #39'Benutzerverwaltung\BV_Benutzerrechte.htm'#39','
      #39'Benutzerverwaltung\BV_Benutzerverwaltung.htm'#39','
      #39'Benutzerverwaltung\BV_Default_Benutzer.htm'#39','
      #39'Benutzerverwaltung\BV_Grp-aendern.htm'#39','
      #39'Benutzerverwaltung\BV_Neuer-Benutzer.htm'#39','
      #39'Benutzerverwaltung\BV_Passwort-aendern.htm'#39','
      #39'Benutzerverwaltung\BV_Rechte-anpassen.htm'#39','
      #39'Benutzerverwaltung\BV_Standard_Benutzerkonfiguration.htm'#39','
      #39'Benutzerverwaltung\BV_Zugriffskontrolle_anpassen.htm'#39','
      
        #39'WindingMaster\Schichtkalender\ShifCal_Standard_Schichten_�berne' +
        'hmen.htm'#39','
      #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen-Schich' +
        'tkalender.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalenderbereich_bear' +
        'beiten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalendernamen_bearbe' +
        'iten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtgruppen-erste' +
        'llen-bearbeiten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-bear' +
        'beiten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-druc' +
        'ken.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-bearbe' +
        'iten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-uebern' +
        'ehmen.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-b' +
        'earbeiten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-k' +
        'opieren.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Gruppenfarbe-veraendern.' +
        'htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Neues-Schichtmuster.htm'#39 +
        ','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtgruppe_loeschen.h' +
        'tm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtgruppen-definiere' +
        'n.htm'#39','
      #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender_neu_defi' +
        'nieren.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definiti' +
        'on.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-drucken.' +
        'htm'#39','
      #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster-bearbeiten' +
        '.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Spezial-Schichten-eintra' +
        'gen.htm'#39','
      #39'WindingMaster\Schichtkalender\SHIFTCAL_Standardschichten.htm'#39','
      
        #39'WindingMaster\Schichtkalender\SHIFTCAL_Standard-Schichten-defin' +
        'ieren.htm'#39','
      #39'WindingMaster\Offlimit\Filter_einstellen.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_DLG_Fenster_Offline_Offlimit.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_DLG_Fenster_Online_Offlimit.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Einstellungen.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Ursachen.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_DLG_Offline-Offlimit-Filter.htm'#39','
      
        #39'WindingMaster\Offlimit\OFFL_DLG_Online-Offlimit-Filter-bearbeit' +
        'en.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Einstellungen_anpassen.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Grenzwerte.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Mittelwert.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Offlimit_Zeiten.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Offlimit-Analyse.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Offlimitursache-definieren.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Offline_Offlimit.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Online_Offlimit.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Spulstellendetail_aufrufen.htm'#39','
      #39'WindingMaster\Offlimit\OFFL_Ursachenanalyse_durchfuehren.htm'#39','
      #39'WindingMaster\Offlimit\Offlimit_einstellen.htm'#39','
      #39'WindingMaster\Offlimit\Offlimit_Ursachen_bearbeiten.htm'#39','
      #39'WindingMaster\Offlimit\Offlimit_Ursachen_zuweisen.htm'#39','
      #39'WindingMaster\Offlimit\Ursachenerkennung_definieren.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_AC338_Partieerfassung_starten.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_AC338_Partieerfassung_stoppen.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Definition_Abgleich_Adjust.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Definition_Maschinenbereich.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Definition_Pilotspulstelle.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Definition_Schlupf.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Dialog_Partie_Eigenschaften.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_DLG_Fenster_Zuordnung.htm'#39','
      
        #39'WindingMaster\Zuordnung\ZUO_DLG_Reinigereinstellungen_bestaetig' +
        'en.htm'#39','
      
        #39'WindingMaster\Zuordnung\ZUO_DLG_Zusaetzliche_Einstellungen.htm'#39 +
        ','
      #39'WindingMaster\Zuordnung\ZUO_Fenster_Vorlagenverwaltung.htm'#39','
      
        #39'WindingMaster\Zuordnung\ZUO_Neue_Reinigervorlage_erstellen.htm'#39 +
        ','
      
        #39'WindingMaster\Zuordnung\ZUO_Reinigereinstellung_als_Vorlage_spe' +
        'ichern.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Reinigereinstellung_zuordnen.htm'#39','
      
        #39'WindingMaster\Zuordnung\ZUO_Reinigereinstellungen_anpassen.htm'#39 +
        ','
      
        #39'WindingMaster\Zuordnung\ZUO_Reinigereinstellungen_ueberpruefen.' +
        'htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Reinigervorlage_loeschen.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Reinigervorlagen.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Vorlage_kopieren.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_Zuordnung_stoppen.htm'#39','
      #39'WindingMaster\Zuordnung\ZUO_ZuordnungAllgemein.htm'#39','
      
        #39'WindingMaster\Spulstellenbericht\SPST_DLG_Maschinenbericht.htm'#39 +
        ','
      
        #39'WindingMaster\Spulstellenbericht\SPST_DLG_Spulstellenbericht.ht' +
        'm'#39','
      #39'WindingMaster\Spulstellenbericht\SPST_Spulstellenbericht.htm'#39','
      
        #39'WindingMaster\Betriebskalender\BETRCAL_Betriebskalender_loesche' +
        'n.htm'#39','
      
        #39'WindingMaster\Betriebskalender\BETRKAL_Arbeitsfreie_Tage_eintra' +
        'gen.htm'#39','
      
        #39'WindingMaster\Betriebskalender\BETRKAL_Betriebskalender_Drucken' +
        '.htm'#39','
      
        #39'WindingMaster\Betriebskalender\BETRKAL_Fenster_Betriebskalender' +
        '.htm'#39','
      
        #39'WindingMaster\Betriebskalender\BETRKAL_Neuer_Betriebskalender.h' +
        'tm'#39','
      #39'WindingMaster\Betriebskalender\BETRKAL-definition.htm'#39','
      #39'MMClient\SYS_DLG_Fenster_MMClient.htm'#39','
      #39'MMClient\SYS_MMClient.htm'#39','
      #39'MMClient\SYS_Start-MillMaster.htm'#39','
      #39'MMClient\SYS_Stop-MillMaster.htm'#39','
      #39'System\SYS_DLG_Setup_Allgemein.htm'#39','
      #39'System\SYS_DLG_Setup_Einstellungen.htm'#39','
      #39'System\SYS_DLG_Setup_MMClient_Einstellungen.htm'#39','
      #39'System\SYS_DLG_Setup_Versionsverzeichnis.htm'#39','
      #39'System\SYS_MillMaster_Service.htm'#39','
      #39'System\SYS_Randbedingungen-definitionen.htm'#39','
      #39'System\SYS_Setup_BenutzerAdministration.htm'#39','
      #39'System\SYS_Status.htm'#39','
      #39'System\SYS_Zeitintervall-Aktualisierung.htm'#39','
      #39'Reiniger\RE_Einstellungen.htm'#39','
      #39'Reiniger\RE_Grundeinstellungen.htm'#39','
      #39'Reiniger\RE_Reinigereinstellungen-Allgemein.htm'#39','
      #39'Eventviewer\SYS_Eventviewer.htm'#39','
      #39'Eventviewer\SYS_Meldungsarten.htm'#39','
      #39'Eventviewer\SYS_MillMaster_Protokoll.htm'#39','
      #39'Eventviewer\SYS_Protokoll_speichern.htm'#39','
      #39'LabMaster\LM_SFI.htm'#39','
      #39'LabMaster\Style\STYLE_Artikel_bearbeiten.htm'#39','
      #39'LabMaster\Style\STYLE_Artikeldaten_export.htm'#39','
      #39'LabMaster\Style\STYLE_Artikeldaten_exportieren.htm'#39','
      #39'LabMaster\Style\STYLE_Artikeldaten_import.htm'#39','
      #39'LabMaster\Style\STYLE_Artikeldaten_importieren.htm'#39','
      #39'LabMaster\Style\STYLE_Artikelstamm_bearbeiten.htm'#39','
      #39'LabMaster\Style\STYLE_Artikelstamm_betrachten.htm'#39','
      #39'LabMaster\Style\STYLE_Benutzerfelder_definieren.htm'#39','
      #39'LabMaster\Style\STYLE_Filter_setzen.htm'#39','
      #39'LabMaster\Style\STYLE_Sortimentstamm.htm'#39','
      #39'LabMaster\Style\STYLE_Sortimentstamm_bearbeiten.htm'#39','
      #39'LabMaster\QOfflimit\QOFF_Administrator.htm'#39','
      #39'LabMaster\QOfflimit\QOFF_Aufgabe_erstellen.htm'#39','
      #39'LabMaster\QOfflimit\QOFF_Auswertung.htm'#39','
      #39'LabMaster\QOfflimit\QOFF_generieren.htm'#39','
      #39'LabMaster\QOfflimit\QOFF_Qualitaetsofflimit.htm'#39','
      #39'LabMaster\QOfflimit\Q-Offlimit_Auswertung.htm'#39','
      #39'LabMaster\LabBericht\LAB_Bericht_configurieren.htm'#39','
      #39'LabMaster\LabBericht\LAB_Bericht_erstellen.htm'#39','
      #39'LabMaster\LabBericht\LAB_Druckoptionen.htm'#39','
      #39'LabMaster\LabBericht\LAB_Erweiterte_Einstellungen.htm'#39','
      #39'LabMaster\LabBericht\LAB_Grafik_bearbeiten.htm'#39','
      #39'LabMaster\LabBericht\LAB_Klasiermatrix_konfigurieren.htm'#39','
      #39'LabMaster\LabBericht\LAB_Labor-Bericht.htm'#39','
      #39'LabMaster\LabBericht\LAB_Tabelle_konfigurieren.htm'#39','
      #39'Remote_Client\RMS_Erste_Hilfe.htm'#39','
      #39'Remote_Client\RMS_Remote_Client.htm'#39','
      #39'Allgemein\GV_Fremdfasern-Def.htm'#39','
      #39'Allgemein\MM_Datenbank_Zugriff.htm'#39','
      #39'Allgemein\MM_DispoMaster.htm'#39','
      #39'Allgemein\MM_Easy.htm'#39','
      #39'Allgemein\MM_Imperfektionen.htm'#39','
      #39'Allgemein\MM_Kontaktadresse.htm'#39','
      #39'Allgemein\MM_LabMaster.htm'#39','
      #39'Allgemein\MM_Langzeitdaten-Haltung.htm'#39','
      #39'Allgemein\MM_Module.htm'#39','
      #39'Allgemein\MM_Optionen-WindingMaster.htm'#39','
      #39'Allgemein\MM_Pro.htm'#39','
      #39'Allgemein\MM_Produktions-Zeiten.htm'#39','
      #39'Allgemein\MM_Qualittsdaten.htm'#39','
      #39'Allgemein\MM_Reiniger-Assistent.htm'#39','
      #39'Machinenkonfiguration\MACONF_Maschine_aktivieren.htm'#39','
      #39'Machinenkonfiguration\MACONF_Maschinen_Konfiguration.htm'#39','
      #39'Machinenkonfiguration\MACONF_Maschinendaten_loeschen.htm'#39','
      #39'Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm'#39','
      #39'CLA\Ausgangsdaten_laden.htm'#39','
      #39'CLA\Auswertung_drucken.htm'#39','
      #39'CLA\CLA_Cluster_Einstellung.htm'#39','
      #39'CLA\CLA_Reinigereinstellungs_Empfehlung.htm'#39','
      #39'CLA\CLA_SFI_Einstellung.htm'#39','
      #39'CLA\CLA_Spleiss_empfindliche_Einstellung.htm'#39','
      #39'CLA\CLA_Spleiss_sehr_empfindliche_Einstellung.htm'#39','
      #39'CLA\Einstellungen_anpassen.htm'#39','
      #39'CLA\Fenster_Reiniger_Assistent.htm'#39','
      #39'CLA\Neue_Serie_erstellen.htm'#39','
      #39'CLA\Reinigervorlage_speichern.htm'#39','
      #39'CLA\Reinigung_mit_Reinigervorlage_simulieren.htm'#39','
      #39'CLA\Reinigung_simulieren.htm'#39)
    TabOrder = 0
    Visible = True
    OnClick = lbHelpPathClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 490
    Height = 49
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object mmLabel1: TmmLabel
      Left = 5
      Top = 5
      Width = 38
      Height = 13
      Caption = 'Helpfile:'
      FocusControl = edHelpfile
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 149
      Top = 5
      Width = 36
      Height = 13
      Caption = 'Context'
      FocusControl = edContextID
      Visible = False
      AutoLabel.LabelPosition = lpLeft
    end
    object bUp: TmmSpeedButton
      Left = 224
      Top = 24
      Width = 24
      Height = 22
      Caption = '^'
      Visible = True
      OnClick = bUpClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bDown: TmmSpeedButton
      Left = 256
      Top = 24
      Width = 24
      Height = 22
      Caption = 'v'
      Visible = True
      OnClick = bDownClick
      AutoLabel.LabelPosition = lpLeft
    end
    object edHelpfile: TmmEdit
      Left = 5
      Top = 20
      Width = 121
      Height = 21
      Color = clWindow
      TabOrder = 0
      Text = 'MM-Pro-001.chm'
      Visible = True
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpTop
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object edContextID: TmmEdit
      Left = 149
      Top = 20
      Width = 46
      Height = 21
      Color = clWindow
      TabOrder = 1
      Text = '1'
      Visible = False
      OnKeyPress = edContextIDKeyPress
      AutoLabel.Control = mmLabel2
      AutoLabel.LabelPosition = lpTop
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object udContextID: TmmUpDown
      Left = 195
      Top = 20
      Width = 15
      Height = 21
      Associate = edContextID
      Min = 1
      Max = 1000
      Position = 1
      TabOrder = 2
      Visible = False
      Wrap = False
      OnChanging = udContextIDChanging
      OnClick = udContextIDClick
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 456
    Width = 490
    Height = 49
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 2
    object laFunctionCall: TmmLabel
      Left = 0
      Top = 32
      Width = 489
      Height = 13
      Cursor = crHandPoint
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsUnderline]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      OnClick = laFunctionCallClick
      AutoLabel.LabelPosition = lpLeft
    end
    object edContext: TmmEdit
      Left = 96
      Top = 6
      Width = 391
      Height = 21
      Anchors = [akLeft, akTop, akRight]
      Color = clWindow
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object bShowContext: TmmButton
      Left = 0
      Top = 4
      Width = 90
      Height = 25
      Caption = 'Show from Text'
      TabOrder = 1
      Visible = True
      OnClick = bShowContextClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object MMHtmlHelp: TMMHtmlHelp
    HelpMask = 'MMHelp-%.3d.chm'
    Left = 380
    Top = 290
  end
  object mmDictionary1: TmmDictionary
    DictionaryName = 'Dictionary1'
    AutoConfig = True
    Left = 415
    Top = 290
  end
end
