unit uMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, Menus, mmMainMenu, MMHtmlHelp, Spin, mmSpinEdit,
  mmLabel, mmEdit, ComCtrls, mmUpDown, IvDictio, IvAMulti, IvBinDic,
  mmDictionary, mmListBox, ExtCtrls, mmPanel, Buttons, mmSpeedButton;

type
  TForm1 = class(TForm)
    MMHtmlHelp: TMMHtmlHelp;
    mmDictionary1: TmmDictionary;
    lbHelpPath: TmmListBox;
    mmPanel1: TmmPanel;
    edHelpfile: TmmEdit;
    mmLabel1: TmmLabel;
    mmPanel2: TmmPanel;
    edContext: TmmEdit;
    bShowContext: TmmButton;
    mmLabel2: TmmLabel;
    edContextID: TmmEdit;
    udContextID: TmmUpDown;
    laFunctionCall: TmmLabel;
    bUp: TmmSpeedButton;
    bDown: TmmSpeedButton;
    procedure ShowHelpIndex;
    procedure bShowContextClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure udContextIDChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure udContextIDClick(Sender: TObject; Button: TUDBtnType);
    procedure edContextIDKeyPress(Sender: TObject; var Key: Char);
    procedure lbHelpPathClick(Sender: TObject);
    procedure bUpClick(Sender: TObject);
    procedure bDownClick(Sender: TObject);
    procedure laFunctionCallClick(Sender: TObject);
  private
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}
uses
  Clipbrd;
//------------------------------------------------------------------------------
procedure TForm1.ShowHelpIndex;
begin
//  edContext.Text := cHelpContext[udContextID.Position];
  MMHtmlHelp.HelpFile := edHelpfile.Text;
  try
    MMHtmlHelp.ShowHelpContext(GetHelpContext(edContext.Text));
    laFunctionCall.Caption := Format('HelpContext := GetHelpContext(''%s'');', [edContext.Text]);
    laFunctionCall.Hint    := laFunctionCall.Caption;
  except
  end;
//  edContext.Text := cHelpContext[udContextID.Position];
//  MMHtmlHelp.HelpFile := edHelpfile.Text;
//  try
//    MMHtmlHelp.ShowHelpContext(udContextID.Position);
//  except
//  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bShowContextClick(Sender: TObject);
begin
  HtmlHelp(0, PChar(edHelpfile.Text), 0, DWord(edContext.Text));
end;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  udContextID.Max := cHelpContextCount;
end;
//------------------------------------------------------------------------------
procedure TForm1.udContextIDChanging(Sender: TObject; var AllowChange: Boolean);
begin
end;
//------------------------------------------------------------------------------
procedure TForm1.udContextIDClick(Sender: TObject; Button: TUDBtnType);
begin
  ShowHelpIndex;
end;
//------------------------------------------------------------------------------
procedure TForm1.edContextIDKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then begin
    Key := #0;
    ShowHelpIndex;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.lbHelpPathClick(Sender: TObject);
var
  xStr: String;
begin
  xStr := StringReplace(lbHelpPath.Items.Strings[lbHelpPath.ItemIndex], '''', '', [rfReplaceAll]);
  xStr := StringReplace(xStr, ',', '', [rfReplaceAll]);
  edContext.Text := xStr; //lbHelpPath.Items.Strings[lbHelpPath.ItemIndex];
  ShowHelpIndex;
end;

procedure TForm1.bUpClick(Sender: TObject);
begin
  with lbHelpPath do begin
    if ItemIndex > 0 then begin
      ItemIndex := ItemIndex - 1;
      lbHelpPathClick(Nil);
    end;
  end;
end;

procedure TForm1.bDownClick(Sender: TObject);
begin
  with lbHelpPath do begin
    if ItemIndex < Items.Count-1 then begin
      ItemIndex := ItemIndex + 1;
      lbHelpPathClick(Nil);
    end;
  end;
end;

procedure TForm1.laFunctionCallClick(Sender: TObject);
begin
  Clipboard.AsText := laFunctionCall.Caption;
end;

end.
