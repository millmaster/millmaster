object frmMain: TfrmMain
  Left = 228
  Top = 138
  Width = 300
  Height = 524
  Caption = 'Khp'#39's Data Export'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 8
    Top = 57
    Width = 55
    Height = 13
    Caption = 'Main Label:'
    FocusControl = edMainLabel
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 120
    Top = 57
    Width = 55
    Height = 13
    Caption = 'Data Label:'
    FocusControl = edDataLabel
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bOpenFile: TmmSpeedButton
    Left = 264
    Top = 24
    Width = 24
    Height = 22
    Caption = '...'
    Visible = True
    OnClick = bOpenFileClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel
    Left = 8
    Top = 9
    Width = 47
    Height = 13
    Caption = 'Textdatei:'
    FocusControl = edInputFile
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object edMainLabel: TmmEdit
    Left = 8
    Top = 72
    Width = 105
    Height = 21
    Color = clWindow
    TabOrder = 0
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edDataLabel: TmmEdit
    Left = 120
    Top = 72
    Width = 97
    Height = 21
    Color = clWindow
    TabOrder = 1
    Visible = True
    AutoLabel.Control = mmLabel2
    AutoLabel.LabelPosition = lpTop
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object edInputFile: TmmEdit
    Left = 8
    Top = 24
    Width = 257
    Height = 21
    Color = clWindow
    TabOrder = 2
    Visible = True
    AutoLabel.Control = mmLabel3
    AutoLabel.LabelPosition = lpTop
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mMemo: TmmMemo
    Left = 8
    Top = 104
    Width = 249
    Height = 353
    ScrollBars = ssVertical
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bGo: TmmButton
    Left = 224
    Top = 72
    Width = 33
    Height = 25
    Caption = 'Go'
    TabOrder = 4
    Visible = True
    OnClick = bGoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object bClipboard: TmmButton
    Left = 8
    Top = 464
    Width = 249
    Height = 25
    Caption = 'Kopiere ins Clipboard'
    TabOrder = 5
    Visible = True
    OnClick = bClipboardClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mOpenDialog: TmmOpenDialog
    DefaultExt = 'txt'
    Filter = 'Textdatei (*.txt)|*.txt'
    Options = [ofHideReadOnly]
    Left = 168
    Top = 8
  end
  object mSaveDialog: TmmSaveDialog
    Options = [ofHideReadOnly]
    Left = 216
    Top = 8
  end
end
