unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmMemo, Buttons, mmSpeedButton, IvMlDlgs,
  mmSaveDialog, mmOpenDialog, mmLabel, mmEdit;

type
  TfrmMain = class(TForm)
    edMainLabel: TmmEdit;
    mmLabel1: TmmLabel;
    edDataLabel: TmmEdit;
    mmLabel2: TmmLabel;
    mOpenDialog: TmmOpenDialog;
    mSaveDialog: TmmSaveDialog;
    edInputFile: TmmEdit;
    bOpenFile: TmmSpeedButton;
    mMemo: TmmMemo;
    bGo: TmmButton;
    mmLabel3: TmmLabel;
    bClipboard: TmmButton;
    procedure bOpenFileClick(Sender: TObject);
    procedure bGoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bClipboardClick(Sender: TObject);
  private
    mInputFile: TStringList;
  public
  end;

var
  frmMain: TfrmMain;

implementation

uses
  clipbrd;

{$R *.DFM}
//:-----------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  mInputFile := TStringList.Create;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.bOpenFileClick(Sender: TObject);
begin
  with mOpenDialog do begin
    if Execute then begin
      edInputFile.Text := FileName;
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.bGoClick(Sender: TObject);
var
  xIndex: Integer;
  xStr: string;
  //..........................................................
  procedure ScanDataLabel;
  var
    xMainLabel: Boolean;
    xResultStr: string;
  begin
    xResultStr := '';
    try
      repeat
        xStr := mInputFile[xIndex];
        Inc(xIndex);

        xMainLabel := (xStr[1] <> ' ');
        xStr := Trim(xStr);
        if Pos(edDataLabel.Text, xStr) = 1 then begin
          xStr := StringReplace(xStr, edDataLabel.Text, '', [rfReplaceAll]);
          if xResultStr <> '' then
            xResultStr := xResultStr + #9;  // Tabulator als Trennzeichen
          xResultStr := xResultStr + Trim(xStr);
        end;
      until (xIndex >= mInputFile.Count) or xMainLabel;
      if xResultStr <> '' then
        mMemo.Lines.Add(xResultStr);
    except
      on e:Exception do
        mMemo.Lines.Add('Importfehler: ' + e.Message);
    end;
  end;
  //..........................................................
begin
  mMemo.Clear;

  mInputFile.LoadFromFile(edInputFile.Text);
  xIndex := 0;
  while xIndex < mInputFile.Count do
  try
    // Grundsätzlich nach Hauptlabel scannen
    xStr := mInputFile[xIndex];
    Inc(xIndex);
    if Pos(edMainLabel.Text, xStr) = 1 then begin
      // Wenn gefunden, dann die Data Labels absuchen
      ScanDataLabel;
    end;
  except
    on e:Exception do
      mMemo.Lines.Add('Importfehler: ' + e.Message);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.bClipboardClick(Sender: TObject);
begin
  Clipboard.AsText := mMemo.Text;
end;

end.
