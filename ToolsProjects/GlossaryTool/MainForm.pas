(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MainForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 18.11.1998  1.00  Wss | Initial
| 21.07.2009  1.01  Nue | Change DB-Servername from "WETSRVBDE2" to "WETFPS03\SQLEXPRESS"
|=========================================================================================*)
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLabel, mmEdit, Db, mmDataSource, DBTables, mmTable,
  mmDatabase, Grids, DBGrids, mmDBGrid, mmQuery, mmCheckBox, ExtCtrls,
  mmTimer, mmRadioGroup, BaseForm, ADODB, mmPanel, Buttons, mmSpeedButton,
  mmComboBox, ComCtrls, mmPageControl, ActnList, mmActionList, DBCtrls,
  mmDBNavigator, mmStatusBar, Spin, mmSpinEdit, mmRadioButton, mmGroupBox,
  CheckLst, mmCheckListBox, mmStringGrid, ImgList, mmImageList, mmButton,
  IvMlDlgs, mmOpenDialog, mmListBox, mmProgressBar, mmADOTable, mmADOQuery;

type
  TLanguageRec = record
    Code: string;
    Name: string;
  end;

const
  cMaxLanguages = 10;
  cLanguages: Array[0..cMaxLanguages-1] of TLanguageRec = (
    (Code: '';      Name: 'Native'),
    (Code: 'de';    Name: 'German'),
    (Code: 'en';    Name: 'English'),
    (Code: 'es';    Name: 'Spanish'),
    (Code: 'pt';    Name: 'Portuguese'),
    (Code: 'it';    Name: 'Italian'),
    (Code: 'fr';    Name: 'French'),
    (Code: 'zh';    Name: 'Chinese Simpl.'),
    (Code: 'zhTW';  Name: 'Chinese Tradi.'),
    (Code: 'tr';    Name: 'Turkish')
  );
//:-----------------------------------------------------------------------------
type
  TMLDHeader = packed record
    Tag: Array[1..3] of Char;
    Version: Byte;
    ByteOrder: Byte;
    CharacterSet: Byte;
    Context: Byte;
    Rerserved1: Array[1..5] of Byte;
    LanguageCount: Word;
    LanguageOffset: DWord;
    TranslationCount: Word;
    TranslationOffset: DWord;
    LocaleCount: Word;
    LocaleOffset: DWord;
    InfoSize: Word;
    InfoOffset: DWord;
    Recerved2: Array[1..6] of Byte;
  end;


  TfrmGlossaryTool = class(TmmForm)
    timUpdate: TmmTimer;
    ADOConnection: TADOConnection;
    mmPanel1: TmmPanel;
    pcMain: TmmPageControl;
    tsViewEdit: TTabSheet;
    edFilter: TmmEdit;
    cbCaseSensitive: TmmCheckBox;
    bEdit: TmmSpeedButton;
    bDeleteTranslation: TmmSpeedButton;
    mmActionList: TmmActionList;
    acAllowEdit: TAction;
    acDeleteTranslation: TAction;
    mmGroupBox1: TmmGroupBox;
    rbOverLengthColor: TmmRadioButton;
    rbNoColor: TmmRadioButton;
    dbNativeLengthColor: TmmRadioButton;
    seLengthLimit: TmmSpinEdit;
    mmPanel2: TmmPanel;
    laTextWidth: TmmLabel;
    laText: TmmLabel;
    pnHex: TmmPanel;
    mmImageList1: TmmImageList;
    mOpenDialog: TmmOpenDialog;
    qryNative: TmmADOQuery;
    dsNative: TmmDataSource;
    qryTranslation: TmmADOQuery;
    mmLabel3: TmmLabel;
    qryWork: TmmADOQuery;
    sgGlossary: TmmStringGrid;
    mmLabel1: TmmLabel;
    laNativeCount: TmmLabel;
    procedure timUpdateTimer(Sender: TObject);
    procedure OnFilterChange(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure acAllowEditExecute(Sender: TObject);
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure OnLengthColorClick(Sender: TObject);
    procedure rgLanguageClick(Sender: TObject);
    procedure acDeleteTranslationExecute(Sender: TObject);
    procedure sgGlossaryClick(Sender: TObject);
    procedure sgGlossaryDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure sgGlossarySelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure sgGlossaryKeyPress(Sender: TObject; var Key: Char);
    procedure sgGlossaryMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    mAppPath: String;
    mMLDHeader: TMLDHeader;
    mCancelTranslation: Boolean;
    mCol: Integer;
    mStrList: TStringList;
    mLengthColor: Integer;
    mInFile: TMemoryStream;
    mHeader: TMemoryStream;
    mTranslation: TMemoryStream;
    mInfo: TMemoryStream;
    mMLDFileList: TStringList;
    mOrgText: string;
    mRow: Integer;
    procedure ReadLanguageHeader;
    function ReadString(aStream: TStream): String;
    procedure UpdateStatusBar(aCol, aRow: Integer);
    procedure SaveMLD(aFileName: String);
  protected
    procedure InitGrid;
    procedure OpenGlossary;
    procedure SaveEditedTranslation(aCol, aRow: Integer);
  public
  end;

var
  frmGlossaryTool: TfrmGlossaryTool;

implementation
{$R *.DFM}
uses
  mmCS,
  LoepfeGlobal, MemCheck, IniFiles, Unicode;

type
  TFieldDef = record
    Width: Integer;
    Visible: Boolean;
  end;

const
  cCaption         = 'Suchen in / Sortieren nach [%s]';
  cMsgUpdateWarning = 'Achtung: entsprechende Datenfelder werden uebernommen und koennen nicht mehr rueckgaengig gemacht werden! Fortfahren?';

//  cQryShowEditLanguage = 'select * from %s';
  cQrySelectNative = 'select Id, NativeValue from MlNative ' +
                     'WHERE NativeValue LIKE ''%s'' ' +
                     'order by NativeValue';

  cQrySelectTranslation = 'select t.Id, t.TranslatedValue, (t.TargetLanguage + t.TargetCountry) as TargetLanguage' +
                          ' from MlNative n, MlTranslation t' +
                          ' where n.Id = t.NativeID' +
                          ' and n.NativeValue LIKE ''%s''';

  cQryUpdateTranslation = 'update MlTranslation set TranslatedValue = ''%s'' where Id = %d';

  // Format parameters: FieldName, FieldName, TableName
  cQryImportPreview = 'SELECT DISTINCT tt.Native, tt.[:Field] Destination, it.[:Field] Source ' +
                      'FROM TranslationTable tt, :ImportTable it ' +
                      'WHERE tt.Native = it.Native %s ' +
                      'AND it.[:Field] is not NULL ' +
                      'ORDER BY tt.Native';

  cQryImportUpdate = 'UPDATE TranslationTable ' +
                     'SET [:Field] = it.[:Field] ' +
                     'FROM TranslationTable tt, :ImportTable it ' +
                     'WHERE tt.Native = it.Native %s ' +
//                     'WHERE tt.Native = it.Native AND ((tt.:Field <> it.:Field) OR (tt.:Field is NULL)) ' +
                     'AND it.[:Field] is not NULL';

  cADOConnectionString = 'Provider=SQLOLEDB.1;Password=netpmek32;Persist Security Info=True;User ID=MMSystemSQL;Initial Catalog=LoepfeGlossary;Data Source=%s';

  cQryFillWithEnglish = 'UPDATE TranslationTable SET ' +
                        '[:Field] = English + ''_''' +
                        'WHERE English is not NULL ' +
                        'AND [:Field] is NULL ';

  cQryRemoveEnglish   = 'UPDATE TranslationTable SET ' +
                        '[:Field] = NULL ' +
                        'WHERE [:Field] = English + ''_''' +
                        'AND German <> English';

//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.timUpdateTimer(Sender: TObject);
begin
  timUpdate.Enabled := False;
  OpenGlossary;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.OnFilterChange(Sender: TObject);
begin
  timUpdate.Restart;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.FormCreate(Sender: TObject);
var
  i: Integer;
  xCount: Integer;
  xStr: String;
  xP: Pointer;
  xServer: String;
begin
  InitGrid;

  mAppPath := ExtractFilePath(Application.ExeName);
  RestoreFormLayout;

  mCancelTranslation := False;
  mMLDFileList := TStringList.Create;
  mHeader      := TMemoryStream.Create;
  mInFile      := TMemoryStream.Create;
  mInfo        := TMemoryStream.Create;
  mTranslation := TMemoryStream.Create;
  mStrList     := TStringList.Create;
  mCol         := -1;
  mRow         := -1;
  mOrgText     := '';

  xServer := 'WETFPS03\SQLEXPRESS';

  with ADOConnection do begin
    Connected := False;
    if ParamCount = 1 then begin
      if CompareText(ParamStr(1), '/LoginPrompt') = 0 then
        LoginPrompt := True
      else
        xServer := ParamStr(1);
    end;
    ConnectionString := Format(cADOConnectionString, [xServer]);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.FormDestroy(Sender: TObject);
var
  i: Integer;
  xStr: String;
begin
  with TIniFile.Create(mAppPath + 'GlossaryTool.ini') do
  try
    with mMLDFileList do begin
      WriteInteger('Translate', 'Count', Count);
      for i:=0 to Count-1 do begin
        WriteString('Translate', 'File' + IntToStr(i), Strings[i]);
      end;
    end;
    UpdateFile;
  finally
    Free;
  end;

  mInFile.Free;
  mMLDFileList.Free;
  mHeader.Free;
  mTranslation.Free;
  mInfo.Free;

  mStrList.Free;

  SaveFormLayout;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.acAllowEditExecute(Sender: TObject);
begin
// Diese Zeile nicht l�schen, da sonst die Action nicht aktiv ist.
  if bEdit.Down then sgGlossary.Options := sgGlossary.Options + [goEditing]
                else sgGlossary.Options := sgGlossary.Options - [goEditing]; 
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;
  acDeleteTranslation.Enabled := bEdit.Down;

  seLengthLimit.Enabled := (mLengthColor = 1);
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.UpdateStatusBar(aCol, aRow: Integer);
var
  xIndex: Integer;
  xBlob: TBlobStream;
  xWString: WideString;
  xChar: PChar;
  i: Integer;
  xStr: String;
  xCellStr: string;
begin
  laText.Caption    := '';
  laTextWidth.Value := '';

  with sgGlossary do begin
    if (aRow > 0) and (aCol >= 0) then begin
      xCellStr := Cells[aCol, aRow];
      xIndex   := Length(xCellStr);

      xChar := Pointer(xCellStr);
      xStr  := '';
      for i:=0 to xIndex-1 do begin
        xStr := xStr + IntToHex(Byte(xChar^), 2);
        inc(xChar);
      end;
      pnHex.Caption := xStr;

      laText.Caption    := xCellStr;
      laTextWidth.Value := Length(StringReplace(xCellStr, '&', '', [rfReplaceAll]));
    end; // if
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.OnLengthColorClick(Sender: TObject);
begin
  mLengthColor := (Sender as TControl).Tag;
  sgGlossary.Invalidate;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.rgLanguageClick(Sender: TObject);
begin
  timUpdateTimer(Nil);
end;
//------------------------------------------------------------------------------
function TfrmGlossaryTool.ReadString(aStream: TStream): String;
var
  xLen: Word;
  xCharArr: Array[1..255] of Char;
begin
  Result := '';
  with aStream do begin
    Read(xLen, 2);
    FillChar(xCharArr, 255, 0);
    Read(xCharArr, xLen);
    Result := StrPas(@xCharArr);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.ReadLanguageHeader;
var
  xLen: Word;
  xCharArr: Array[1..255] of Char;
  xCol: Integer;
begin
  xCol := 0;
  // Hier wird die MLD Datei eingelesen. Das Format ist in einem Dokument separat beschrieben.
{
  sgTranslation.RowCount := 2;
  with mInFile do begin
    Position := mMLDHeader.LanguageOffset;
    while Position < mMLDHeader.TranslationOffset do
    try
      // read size
      Read(xLen, 2);
      // read Primary LangID
      Read(xLen, 2);
      // ISO language
      Read(xCharArr, 2);
      // Default sub
      Read(xLen, 2);
      // ISO default country
      Read(xCharArr, 2);
      // All subs
      ReadString(mInFile);
      // ISO all countries
      ReadString(mInFile);
      // Character set
      Read(xLen, 2);
      // CodePage
      Read(xLen, 2);
      // Options
      Read(xLen, 1);
      // English Name
      sgTranslation.Rows[0].Add(ReadString(mInFile));
      // Native Name
      ReadString(mInFile);
      // Font Name
      ReadString(mInFile);
      // Font Size
      Read(xLen, 1);

      inc(xCol)
    except
    end;
  end; // with mInFile
{}
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.SaveMLD(aFileName: String);
var
  xWord: Word;
  xCol, xRow: Integer;
  xMem: TMemoryStream;
  xOldInfoOffset: DWord;
  //.............................................................
  procedure WriteString(aString: String);
  var
    i: Word;
    xPChar: PChar;
  begin
    i := Length(aString);
    xMem.Write(i, 2);
    xPChar := PChar(aString);
    xMem.Write(xPChar^, i);
  end;
  //.............................................................
begin
// Hier wird eine MLD Datei erzeugt!!
{
  xMem := TMemoryStream.Create;
  with sgTranslation do begin
    for xRow:=1 to RowCount-1 do begin
      if Cells[0, xRow] <> '' then
        for xCol:=0 to ColCount-1 do
          WriteString(Cells[xCol, xRow]);
    end;
  end;

  with TFileStream.Create(aFileName, fmCreate) do
  try
    // Offsetposition an neun Groesse anpassen und Header schreiben
    with mMLDHeader do begin
      xOldInfoOffset := InfoOffset;
      InfoOffset :=  TranslationOffset + xMem.Size;
    end;
    Write(mMLDHeader, sizeof(TMLDHeader));

    // Language Sektion schreiben
    with mMLDHeader do begin
      mInFile.Position := LanguageOffset;
      CopyFrom(mInFile, TranslationOffset - LanguageOffset);
    end;

    // nun Uebersetzungen schreiben
    xMem.Position := 0;
    CopyFrom(xMem, xMem.Size);

    // und am Schluss noch Info Sektion uebernehmen
    with mMLDHeader do begin
      mInFile.Position := xOldInfoOffset;
      CopyFrom(mInFile, InfoSize);
    end;

  finally
    Free;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmGlossaryTool.acDeleteTranslationExecute(Sender: TObject);
var
  xNativeID: Integer;
begin
  if MessageDlg('Aktuelle Zeile wirklich loeschen?', mtConfirmation, [mbOK,mbCancel], 0) = mrOK then
  try
    xNativeID := Integer(sgGlossary.Rows[sgGlossary.Row].Objects[0]);
    with qryWork do begin
      SQL.Text := Format('delete MlTranslation where NativeId = %d', [xNativeID]);
      ExecSQL;

      SQL.Text := Format('delete MlNative where Id = %d', [xNativeID]);
      ExecSQL;
    end;
  except
    on e: Exception do
      ShowMessage('Fehler beim l�schen: ' + e.Message);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.InitGrid;
var
  i: Integer;
begin
  sgGlossary.ColCount := cMaxLanguages;
  sgGlossary.Rows[0].Clear;
  for i:=0 to cMaxLanguages-1 do begin
    sgGlossary.Rows[0].AddObject(cLanguages[i].Name, Pointer(@cLanguages[i]));
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.sgGlossaryClick(Sender: TObject);
begin
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.sgGlossaryDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  xSize: Integer;
  xBool: Boolean;
  xInt1, xInt2: Integer;
  xNative: String;
  xChinese: Boolean;
  xText: String;
  xColor: TColor;
  xVertOffset, xHorzOffset: Integer;
  xRect: TRect;
begin
  xBool := False;

{}
  xText    := sgGlossary.Cells[ACol, ARow];

//  xColor := Color;
  with Sender as TmmStringGrid do begin
    // Erstmal den Hintergrund zeichnen
    if gdFixed in State then begin
      if ARow = 0 then xColor := FixedColor
                  else xColor := $00E3E8EA;
    end else
      xColor := Color;
      
    Canvas.Brush.Color := xColor;
    Canvas.FillRect(Rect);

    xVertOffset := Rect.Top  + (((Rect.Bottom - Rect.Top) - Canvas.TextHeight(xText)) div 2);
    xHorzOffset := Rect.Left + Canvas.TextWidth('Mi') div 4;

    if ACol in [7, 8] then
      Canvas.Font.Charset := CHINESEBIG5_CHARSET
    else if ACol = 9 then
      Canvas.Font.Charset := TURKISH_CHARSET
    else
      Canvas.Font.Charset := DEFAULT_CHARSET;

    if Canvas.Font.Charset <> CHINESEBIG5_CHARSET then
      // Sonderzeichen noch umwandeln in darstellbare Zeichen
      xText := UTF8ToWideString(xText);

    Canvas.TextRect(Rect, xHorzOffset, xVertOffset, xText);

    // Den 3D-Effekt noch zeichnen
    if (gdFixed in State) and (ARow = 0) then
      Frame3D(Canvas, Rect, clWindow, clBtnShadow, 1);
  end;

{
  if not(gdSelected in State) and (DataCol > 0) then begin
    xSize := Length(StringReplace(xText, '&', '', [rfReplaceAll]));
    case mLengthColor of
      1: xBool := (xSize > seLengthLimit.Value);
      2: begin
          xNative := qryTranslation.Fields[0].AsString;
          xInt1 := Pos('(', xNative);
          xInt2 := Pos(')', xNative);
          if (xInt1 = 1) and (xInt2 <= 5) then begin
            xNative := Copy(xNative, xInt1+1, xInt2 - xInt1 - 1);
            xInt1 := StrToIntDef(xNative, 0);
            xBool := (xSize > xInt1) and (xInt1 > 0);
          end;
        end;
    else
    end;

    if xBool  then begin
      with mDBGrid.Canvas do begin
        Brush.Color := $00DDDDFF;
        FillRect(Rect);
      end;
    end;
  end;
  // nun noch den Text ausgeben
  with mDBGrid.Canvas do begin
    if xChinese then begin
      Font.Charset := CHINESEBIG5_CHARSET
    end else
      Font.Charset := DEFAULT_CHARSET;
    TextRect(Rect, Rect.Left + 2, Rect.Top + 2, xText);
  end;
{}
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.sgGlossarySelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.FormResize(Sender: TObject);
var
  i: Integer;
  xWidth: Integer;
begin
  xWidth := sgGlossary.ClientWidth div sgGlossary.ColCount;
  for i:=0 to sgGlossary.ColCount-1 do begin
    sgGlossary.ColWidths[i] := xWidth;
  end;
end;
//:-----------------------------------------------------------------------------

procedure TfrmGlossaryTool.FormShow(Sender: TObject);
begin
  sgGlossary.Align := alClient;
  FormResize(Nil);
  OpenGlossary;
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.OpenGlossary;
var
  i: Integer;
  xRow: Integer;
  xRowList: TStrings;
  xNativeID: Integer;
  xTargetLanguage: string;
  xIndex: Integer;
  xFilter: string;
  xStr: string;
  xOutChar: Array[1..300] of WideChar;
begin


  sgGlossary.RowCount := 2;
  sgGlossary.Rows[1].Clear;

  xRow := 1;
  xFilter := edFilter.Text;
  if Trim(xFilter) <> '' then begin
    qryTranslation.Close;
    with qryNative do begin
      Close;
      if not cbCaseSensitive.Checked then
        xFilter := '%' + xFilter + '%';
      Parameters.ParamByName('Filter').Value := xFilter;
      Open;

      if FindFirst then begin
        qryTranslation.Open;
        while not Eof do begin
          // Inkremental StringGrid erh�hen
          with sgGlossary do begin
            if xRow = (RowCount - FixedRows) then
              RowCount := RowCount + 100;
          end;
          // Erst mal den Native Text einf�gen
          xRowList  := sgGlossary.Rows[xRow];
          xNativeID := FieldByName('NativeId').AsInteger;
          xRowList.Strings[0] := FieldByName('NativeValue').AsString;
          xRowList.Objects[0] := Pointer(xNativeID);

          // Dann die �bersetzungen
          with qryTranslation do begin
            if FindFirst then begin
              while not Eof do begin
                xTargetLanguage := FieldByName('TargetLanguage').AsString;
                for i:=1 to cMaxLanguages-1 do begin
                  if AnsiSameText(cLanguages[i].Code, xTargetLanguage) then begin
                    xStr := FieldByName('TranslatedValue').AsString;
                    StringToWideChar(xStr, @xOutChar, 300);
//                    xIndex := MultiByteToWideChar(CP_ACP, MB_COMPOSITE, PChar(xStr), Length(xStr), @xOutChar, 300);
                    xRowList.Strings[i] := FieldByName('TranslatedValue').AsString;
                    xRowList.Objects[i] := Pointer(FieldByName('Id').AsInteger);
                    Break;
                  end;
                end;

                Next;
              end; // while qryTranslation
            end; // if FindFirst
          end;

          Inc(xRow);
          Next;
        end; // while qryNative

        with sgGlossary do begin
          Row := 1;
          RowCount := xRow;
          laNativeCount.Value := RowCount - 1;
        end;
      end;
    end;
  end // if xFilter
  else begin
    with qryWork do begin
      SQL.Text := 'select count(*) as Anzahl from MlNative';
      Open;
      if FindFirst then
        laNativeCount.Caption := Format('Total %d', [FieldByName('Anzahl').AsInteger]);
      Close;
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.SaveEditedTranslation(aCol, aRow: Integer);
var
  xTranslationID: Integer;
  xNewTranslation: string;
begin
  // hole die ID der �bersetzung
  xTranslationID := Integer(sgGlossary.Objects[mCol, mRow]);
  if xTranslationID > 0 then begin
    xNewTranslation := sgGlossary.Cells[aCol, aRow];
    with qryWork do begin
      SQL.Text := Format(cQryUpdateTranslation, [xNewTranslation, xTranslationID]);
      ExecSQL;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.sgGlossaryKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = cCR then
    SaveEditedTranslation(sgGlossary.Col, sgGlossary.Row);
end;
//:-----------------------------------------------------------------------------
procedure TfrmGlossaryTool.sgGlossaryMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xRow, xCol: Integer;
begin
  sgGlossary.MouseToCell(X, Y, xCol, xRow);

  if bEdit.Down then begin
    if (mRow > 0) and (mCol > 0) and ((mRow <> xRow) or (mCol <> xCol)) then begin
      SaveEditedTranslation(mCol, mRow);
    end;
  end;

  mCol := xCol;
  mRow := xRow;
  UpdateStatusBar(mCol, mRow);
end;
//:-----------------------------------------------------------------------------
end.

