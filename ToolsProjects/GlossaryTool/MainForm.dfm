object frmGlossaryTool: TfrmGlossaryTool
  Left = 345
  Top = 129
  Width = 871
  Height = 630
  ActiveControl = edFilter
  Caption = 'Glossary Tool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  ShowHint = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 863
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object bEdit: TmmSpeedButton
      Left = 8
      Top = 8
      Width = 113
      Height = 25
      Action = acAllowEdit
      AllowAllUp = True
      GroupIndex = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object pcMain: TmmPageControl
    Left = 0
    Top = 41
    Width = 863
    Height = 128
    ActivePage = tsViewEdit
    Align = alTop
    TabOrder = 1
    object tsViewEdit: TTabSheet
      Caption = 'Anzeigen / Editieren'
      object bDeleteTranslation: TmmSpeedButton
        Left = 160
        Top = 16
        Width = 121
        Height = 22
        Action = acDeleteTranslation
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel3: TmmLabel
        Left = 0
        Top = -1
        Width = 79
        Height = 13
        Caption = 'Suche in Native:'
        FocusControl = edFilter
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel1: TmmLabel
        Left = 0
        Top = 64
        Width = 104
        Height = 13
        Caption = 'Anzahl Native Strings:'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laNativeCount: TmmLabel
        Left = 112
        Top = 64
        Width = 12
        Height = 13
        Caption = '00'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edFilter: TmmEdit
        Left = 0
        Top = 16
        Width = 121
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        OnChange = OnFilterChange
        AutoLabel.Control = mmLabel3
        AutoLabel.Distance = 4
        AutoLabel.LabelPosition = lpTop
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object cbCaseSensitive: TmmCheckBox
        Left = 0
        Top = 42
        Width = 129
        Height = 17
        Caption = 'Exakte schreibweise'
        Enabled = False
        TabOrder = 1
        Visible = True
        OnClick = OnFilterChange
        AutoLabel.LabelPosition = lpLeft
      end
      object mmGroupBox1: TmmGroupBox
        Left = 368
        Top = 0
        Width = 145
        Height = 97
        Anchors = [akLeft, akTop, akBottom]
        Caption = 'Einfaerben'
        Enabled = False
        TabOrder = 2
        Visible = False
        CaptionSpace = True
        object rbOverLengthColor: TmmRadioButton
          Tag = 1
          Left = 8
          Top = 32
          Width = 113
          Height = 17
          Caption = 'Ueberlaenge'
          TabOrder = 0
          OnClick = OnLengthColorClick
        end
        object rbNoColor: TmmRadioButton
          Left = 8
          Top = 16
          Width = 113
          Height = 17
          Caption = 'keine'
          Checked = True
          TabOrder = 1
          TabStop = True
          OnClick = OnLengthColorClick
        end
        object dbNativeLengthColor: TmmRadioButton
          Tag = 2
          Left = 8
          Top = 72
          Width = 113
          Height = 17
          Caption = 'Kodierung in Native'
          TabOrder = 2
          OnClick = OnLengthColorClick
        end
        object seLengthLimit: TmmSpinEdit
          Left = 26
          Top = 48
          Width = 73
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 3
          Value = 120
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 584
    Width = 863
    Height = 19
    Align = alBottom
    BevelOuter = bvLowered
    BorderWidth = 1
    Caption = 'mmPanel2'
    TabOrder = 4
    object laTextWidth: TmmLabel
      Left = 2
      Top = 2
      Width = 40
      Height = 15
      Align = alLeft
      Alignment = taCenter
      AutoSize = False
      Caption = '0'
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laText: TmmLabel
      Left = 47
      Top = 2
      Width = 814
      Height = 15
      Align = alRight
      Anchors = [akLeft, akTop, akRight, akBottom]
      AutoSize = False
      Caption = '0'
      Layout = tlCenter
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object pnHex: TmmPanel
    Left = 0
    Top = 560
    Width = 863
    Height = 24
    Align = alBottom
    Alignment = taLeftJustify
    BevelOuter = bvLowered
    BorderWidth = 2
    Caption = 'pnHex'
    TabOrder = 3
  end
  object sgGlossary: TmmStringGrid
    Left = 0
    Top = 208
    Width = 233
    Height = 328
    DefaultRowHeight = 17
    DefaultDrawing = False
    RowCount = 2
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
    ParentFont = False
    TabOrder = 2
    Visible = True
    OnClick = sgGlossaryClick
    OnDrawCell = sgGlossaryDrawCell
    OnKeyPress = sgGlossaryKeyPress
    OnMouseDown = sgGlossaryMouseDown
    OnSelectCell = sgGlossarySelectCell
    AutoLabel.LabelPosition = lpLeft
    RowHeights = (
      17
      17)
  end
  object timUpdate: TmmTimer
    Interval = 300
    OnTimer = timUpdateTimer
    Left = 16
    Top = 256
  end
  object ADOConnection: TADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security In' +
      'fo=False;User ID=MMSystemSQL;Initial Catalog=LoepfeGlossary;Data' +
      ' Source=WETSRVBDE2;Use Procedure for Prepare=1;Auto Translate=Tr' +
      'ue;Packet Size=4096;Workstation ID=WETWSS1;Use Encryption for Da' +
      'ta=False;Tag with column collation when possible=False'
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    Left = 16
    Top = 304
  end
  object mmActionList: TmmActionList
    OnUpdate = mmActionListUpdate
    Left = 160
    Top = 8
    object acAllowEdit: TAction
      Caption = '!! Enable Edit !!'
      OnExecute = acAllowEditExecute
    end
    object acDeleteTranslation: TAction
      Caption = 'Zeile loeschen'
      OnExecute = acDeleteTranslationExecute
    end
  end
  object mmImageList1: TmmImageList
    Left = 200
    Top = 8
  end
  object mOpenDialog: TmmOpenDialog
    DefaultExt = 'mld'
    Filter = 'Multilizer Binary File (*.mld)|*.mld'
    Options = [ofHideReadOnly, ofAllowMultiSelect, ofFileMustExist]
    Title = 'MLD Datei hinzufuegen'
    Left = 36
    Top = 97
  end
  object qryNative: TmmADOQuery
    Connection = ADOConnection
    CursorType = ctOpenForwardOnly
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'Filter'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 255
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      'select Id as NativeID, NativeValue from MlNative'
      'WHERE NativeValue LIKE :Filter'
      'order by NativeValue')
    Left = 272
    Top = 264
  end
  object dsNative: TmmDataSource
    DataSet = qryNative
    Left = 304
    Top = 264
  end
  object qryTranslation: TmmADOQuery
    Connection = ADOConnection
    CursorType = ctOpenForwardOnly
    LockType = ltReadOnly
    DataSource = dsNative
    Parameters = <
      item
        Name = 'NativeID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end>
    Prepared = True
    SQL.Strings = (
      
        'select Id, TranslatedValue, (TargetLanguage + TargetCountry) as ' +
        'TargetLanguage'
      'from MlTranslation'
      'where NativeID = :NativeID')
    Left = 272
    Top = 296
  end
  object qryWork: TmmADOQuery
    Connection = ADOConnection
    CursorType = ctOpenForwardOnly
    LockType = ltReadOnly
    Parameters = <>
    Prepared = True
    Left = 272
    Top = 328
  end
end
