program MBCSTool;

uses
  Forms,
  uMBCS in 'uMBCS.PAS',
  uMBCS_Lists in 'uMBCS_Lists.PAS',
  uFileHandling in 'uFileHandling.PAS',
  uMBCSGlobal in 'uMBCSGlobal.PAS',
  uMainWindow in 'uMainWindow.pas' {MainWindow};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TMainWindow, MainWindow);
  MainWindow.Init;
  Application.Run;
end.
