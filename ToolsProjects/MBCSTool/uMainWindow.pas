(*
*)
{1 Hauptformular }
unit uMainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, mmBitBtn, ComCtrls, mmStatusBar, StdCtrls, mmCheckBox, mmButton,
  ExtCtrls, mmPanel, CheckLst, mmCheckListBox, mmMemo, mmPageControl,
  mmSplitter, mmRichEdit,uMBCSGlobal,ObjectCheckListBox, mmGroupBox, DirectoryDialog, uAppSettings,
  ActnList,uFileHandling, mmStaticText, mmEdit;

type
  (* Hauptformular *)
  {1 Hauptformular }
  TMainWindow = class (TForm)
    ActionList: TActionList;
    aStart: TAction;
    aStop: TAction;
    bDirChangedFiles: TmmBitBtn;
    bLogFile: TmmBitBtn;
    bShowCahngedFiles: TmmBitBtn;
    bShowLogFile: TmmBitBtn;
    bStart: TmmBitBtn;
    bStop: TmmBitBtn;
    cbCreateNewLogFiles: TmmCheckBox;
    cbIncludeSubDir: TmmCheckBox;
    cbScanOnly: TmmCheckBox;
    eChangedFiles: TmmEdit;
    eLogFile: TmmEdit;
    gbDirectoryExcludeList: TmmGroupBox;
    gbDirectoryList: TmmGroupBox;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmPanel3: TmmPanel;
    mmSplitter: TmmSplitter;
    mmStaticText1: TmmStaticText;
    mmStaticText2: TmmStaticText;
    mmStatusBar: TmmStatusBar;
    mmStatusMemo: TmmRichEdit;
    pcOptions: TmmPageControl;
    pOptionPanel: TmmPanel;
    tabConfig: TTabSheet;
    tabSettings: TTabSheet;
    TabSheet1: TTabSheet;
    {1 Versetzt das GUI in den Zustand Process }
    procedure aStartExecute(Sender: TObject);
    {1 Versetzt das GUI in den Zustand Ready }
    procedure aStopExecute(Sender: TObject);
    {1 Pfad f�r das Logfile f�r die ge�nderten Dateien festlegen }
    procedure bDirChangedFilesClick(Sender: TObject);
    {1 Pfad f�r das Logfile f�r alle Eintr�ge festlegen }
    procedure bLogFileClick(Sender: TObject);
    {1 Zeigt ein Logfile im Editor an }
    procedure bShowLogFileClick(Sender: TObject);
    {1 Startet den Thread }
    procedure bStartClick(Sender: TObject);
    {1 Unterbricht den Thread }
    procedure bStopClick(Sender: TObject);
    {1 Wenn True, dann werden jedes mal neue Files erzeugt. Sonst werden die neuen Daten angeh�ngt }
    procedure cbCreateNewLogFilesClick(Sender: TObject);
    {1 Legt fest ob die Dateien nur durchsucht oder auch ge�ndert werden }
    procedure cbScanOnlyClick(Sender: TObject);
    {1 Anpassend er Steuerelemente bei Gr�ssenanderung des Fensters }
    procedure pcOptionsResize(Sender: TObject);
    {1 Einstellungen �bernehmen }
    procedure tabSettingsShow(Sender: TObject);
  private
    FChangedFiles: TLogFile;
    FLogFile: TLogFile;
    mPathExcludeList: TPathList;
    mPathExcludeListBox: TObjectCheckListBox;
    mPathList: TPathList;
    mPathListBox: TObjectCheckListBox;
    {1 F�gt ein Verzeichnis zu Include Liste hinzu }
    procedure AddDir(AInclude: boolean);
    {1 F�gt eine Datei zur Include Liste hinzu }
    procedure AddFile(AInclude: boolean);
    {1 Fragt den gew�nschten Dateinamen des Log Files ab }
    function GetLogFileDest(aInitPath:string): String;
    {1 Aktualisiert die Anzeige der Eigenschaften des selektierten Pfades }
    procedure ItemChange(PathList: TPathList; CheckBox: TmmCheckBox; NewIndex: Integer; OldIndex: Integer);
    {1 Ereignisprozedur wenn die Selektion in der Exclude Liste �ndert }
    procedure ItemExcludeChange(NewIndex: Integer; OldIndex: Integer);
    {1 Ereignisprozedur wenn die Selektion in der Include Liste �ndert }
    procedure ItemIncludeChange(NewIndex: Integer; OldIndex: Integer);
    {1 Wird Aufgerufen bevor der Thread freigegeben wird }
    procedure OnThreadTerminate(Sender: TObject);
    {1 Erm�glicht das �ndern eines Pfades (ObjectListBox) }
    procedure PathPropertiesClick(APath:TPath);
  public
    {1 F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox) }
    procedure AddClick;
    {1 F�gt ein Directory oder ein File zur Liste hinzu (ObjectListox) }
    procedure AddExcludeClick;
    {1 F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox) }
    procedure AddFileClick(var Allow:Boolean);
    {1 F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox) }
    procedure AddFileExcludeClick(var Allow:Boolean);
    {1 Eigenschaften eines Pfades der Exclude Liste (ObjectListBox) }
    procedure ExcludeProperties(ASelected:Integer);
    {1 Eigenschaften eines Pfades der Exclude Liste (ObjectListBox) }
    procedure IncludeProperties(ASelected:Integer);
    {1 Initialisiert die Listen mit den Verzeichnissen oder Files }
    procedure Init;
  published
    {1 Am Programmende alles wieder freigeben }
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  end;
  
var
  MainWindow: TMainWindow;

implementation // 01.03.2002 added mmMBCS to imported units
uses
  mmMBCS,
  shellapi,
  uMBCS;

const
  // Steuercodes
  cr   = #10;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;


{$R *.DFM}

(* Am Programmende alles wieder freigeben *)
{1 Am Programmende alles wieder freigeben }
(* Am Programmende alles wieder freigeben *)
{1 Am Programmende alles wieder freigeben }
{
***************************************************** TMainWindow ******************************************************
}
(********************************************************************************
  TMainWindow.AddClick
  Sichtbarkeit: public
  Argumente   : 
  Zweck:
    F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox)
********************************************************************************)
procedure TMainWindow.AddClick;
begin
  AddDir(true);
end;// TMainWindow.AddClick()

(********************************************************************************
  TMainWindow.AddDir
  Sichtbarkeit: private
  Argumente   : (AInclude)
  Zweck:
    F�gt ein Verzeichnis zu Include Liste hinzu
********************************************************************************)
procedure TMainWindow.AddDir(AInclude: boolean);
var
  p: TPath;
  DirectoryDialog: TDirectoryDialog;
begin
  // TDirectoryDialog kapselt den Auruf von SHBrowseForFolders
  DirectoryDialog:=TDirectoryDialog.create(self);
  with DirectoryDialog do begin
    if AInclude then
      DialogTitle:='MBCS Tool - Directories to include'
    else
      DialogTitle:='MBCS Tool - Directories to exclude';
    if Execute then begin
      // Neuen Eintrag f�r die Liste erzeugen
      p:=TPath.Create(self);
      p.PathType:=ptDir;
      p.Path:=DirectoryDialog.Path;
      // Neuen Pfad zur Include- oder zur Exclude Liste hinzuf�gen
      if AInclude then begin
        mPathList.Add(p);
        mPathListBox.SetList;
        // Selektion auf den neuen Eintrag
        mPathListBox.ListBox.ItemIndex:=mPathListBox.ListBox.Items.Count-1;
      end else begin
        mPathExcludeList.Add(p);
        mPathExcludeListBox.SetList;
        // Selektion auf den neuen Eintrag
        mPathExcludeListBox.ListBox.ItemIndex:=mPathExcludeListBox.ListBox.Items.Count-1;
      end;// if AInclude then begin
    end;// if Execute then begin
  end;// with DirectoryDialog do begin
end;// TMainWindow.AddDir()

(********************************************************************************
  TMainWindow.AddExcludeClick
  Sichtbarkeit: public
  Argumente   : 
  Zweck:
    F�gt ein Directory oder ein File zur Liste hinzu (ObjectListox)
********************************************************************************)
procedure TMainWindow.AddExcludeClick;
begin
  AddDir(false);
end;// TMainWindow.AddExcludeClick()

(********************************************************************************
  TMainWindow.AddFile
  Sichtbarkeit: private
  Argumente   : (AInclude)
  Zweck:
    F�gt eine Datei zur Include Liste hinzu
********************************************************************************)
procedure TMainWindow.AddFile(AInclude: boolean);
var
  p: TPath;
  OpenDialog: TOpenDialog;
begin
  OpenDialog:=TOpenDialog.create(self);
  with OpenDialog do begin
    if AInclude then
      Title:='MBCS Tool - File to include'
    else
      Title:='MBCS Tool - File to exclude';
    if Execute then begin
      // Neuen Eintrag f�r die Liste erzeugen
      p:=TPath.Create(self);
      p.PathType:=ptFile;
      p.Path:=OpenDialog.FileName;
      // Neuen Pfad zur Include- oder zur Exclude Liste hinzuf�gen
      if AInclude then begin
        mPathList.Add(p);
        mPathListBox.SetList;
        // Selektion auf den neuen Eintrag
        mPathListBox.ListBox.ItemIndex:=mPathListBox.ListBox.Items.Count-1;
      end else begin
        mPathExcludeList.Add(p);
        mPathExcludeListBox.SetList;
        // Selektion auf den neuen Eintrag
        mPathExcludeListBox.ListBox.ItemIndex:=mPathExcludeListBox.ListBox.Items.Count-1;
      end;// if AInclude then begin
    end;// if Execute then begin
  end;// with OpenDialog do begin
end;// TMainWindow.AddFile()

(********************************************************************************
  TMainWindow.AddFileClick
  Sichtbarkeit: public
  Argumente   : (Allow)
  Zweck:
    F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox)
********************************************************************************)
procedure TMainWindow.AddFileClick(var Allow:Boolean);
begin
  Allow:=false;
  AddFile(true);
end;// TMainWindow.AddFileClick()

(********************************************************************************
  TMainWindow.AddFileExcludeClick
  Sichtbarkeit: public
  Argumente   : (Allow)
  Zweck:
    F�gt ein Directory oder ein File zur Liste hinzu (ObjectListBox)
********************************************************************************)
procedure TMainWindow.AddFileExcludeClick(var Allow:Boolean);
begin
  Allow:=false;
  AddFile(false);
end;// TMainWindow.AddFileExcludeClick()

(********************************************************************************
  TMainWindow.aStartExecute
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Versetzt das GUI in den Zustand Process
********************************************************************************)
procedure TMainWindow.aStartExecute(Sender: TObject);
begin
  bStart.Enabled:=false;
  bStop.Enabled:=true;
end;// TMainWindow.aStartExecute()

(********************************************************************************
  TMainWindow.aStopExecute
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Versetzt das GUI in den Zustand Ready
********************************************************************************)
procedure TMainWindow.aStopExecute(Sender: TObject);
begin
  bStart.Enabled:=true;
  bStop.Enabled:=false;
end;// TMainWindow.aStopExecute()

(********************************************************************************
  TMainWindow.bDirChangedFilesClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Pfad f�r das Logfile f�r die ge�nderten Dateien festlegen
********************************************************************************)
procedure TMainWindow.bDirChangedFilesClick(Sender: TObject);
begin
  if (Sender is TmmBitBtn) then
    eChangedFiles.Text:=GetLogFileDest(eChangedFiles.Text);
  eChangedFiles.Text:=ChangeFileExt(eChangedFiles.Text,'.log');
  
  // Pfad dem Logfile zuweisen
  MBCSSettings.ChangedFiles:= eChangedFiles.Text;
end;// TMainWindow.bDirChangedFilesClick()

(********************************************************************************
  TMainWindow.bLogFileClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Pfad f�r das Logfile f�r alle Eintr�ge festlegen
********************************************************************************)
procedure TMainWindow.bLogFileClick(Sender: TObject);
begin
  if (Sender is TmmBitBtn) then
    eLogFile.Text:=GetLogFileDest(eLogFile.Text);
  eLogFile.Text:=ChangeFileExt(eLogFile.Text,'.log');
  
  // Pfad dem Logfile zuweisen
  MBCSSettings.LogFile:= eLogFile.Text;
end;// TMainWindow.bLogFileClick()

(********************************************************************************
  TMainWindow.bShowLogFileClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Zeigt ein Logfile im Editor an
********************************************************************************)
procedure TMainWindow.bShowLogFileClick(Sender: TObject);
var
  xFileName: String;
begin
  if (sender as TmmBitBtn).name='bShowLogFile' then
    xFileName:=eLogFile.Text
  else
    xFileName:=eChangedFiles.Text;
  
  // �ffnet den Editor mit der angegebenen Datei
  if FileExists(xFileName) then
    ShellExecute(Application.Handle,'open','Notepad.exe',PChar(xFileName),'',SW_SHOWNORMAL)
  else
    showMessage('File ' + xFileName + ' does not exists');
end;// TMainWindow.bShowLogFileClick()

(********************************************************************************
  TMainWindow.bStartClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Startet den Thread
********************************************************************************)
procedure TMainWindow.bStartClick(Sender: TObject);
var
  i: Integer;
  xDir: TDirectory;
  xFile: TFile;
begin
  // GUI sperren
  AStart.Execute;
  
  // nil, wenn noch kein Prozess gestartet wurde oder der letzte bereits fertig ist
  if MBCSParser=nil then begin
    mmStatusMemo.Lines.Clear;
  
    // evt. Ge�nderte Daten noch �bernehmen
    ItemChange(mPathList,cbIncludeSubDir, mPathListBox.ListBox.ItemIndex, mPathListBox.ListBox.ItemIndex);
    ItemChange(mPathExcludeList, nil,mPathExcludeListBox.ListBox.ItemIndex, mPathExcludeListBox.ListBox.ItemIndex);
  
    // Thread erzeugen (suspended)
    MBCSParser:=TMBCSParser.Create(true);
    with MBCSParser do begin
      // Benachrichtigung wenn der Thread fertig ist
      OnTerminate:=OnThreadTerminate;
      // Parameter setzen
      Filter:=gDefaultFilter;
      FreeOnTerminate:=true;
      StatusBar:=mmStatusBar;
      StatusMemo:=mmStatusMemo;
      LogFile:=FLogFile;
      ChangedFiles:=FChangedFiles;
      ScanOnly:=MBCSSettings.ScanOnly;
    end;// with MBCSParser do begin
  
    // Alle Pfade in den Thread �bernehmen
  
    // Include
    for i:=0  to mPathList.Count-1 do begin
      case mPathList[i].PathType of
        ptDir: begin
          if mPathList[i].Checked then begin
            // Verzeichnis erzeugen und Parameter �bertragen
            xDir:=TDirectory.Create;
            mPathList[i].AssignDir(xDir);
            // Zur Liste hinzuf�gen
            MBCSParser.AddNewDirectory(xDir);
          end;// if mPathList[i].Checked then
        end;// ptDir: begin
        ptFile: begin
          if mPathList[i].Checked then begin
            // Dateiobjekt erzeugen und die Parameter eintragen
            xFile:=TFile.CreateParam(mPathList[i].Path, FLogFile, FChangedFiles);
            // In die Liste eintragen
            MBCSParser.AddNewFile(xFile);
          end;// if mPathList[i].Checked then
        end;// ptFile: begin
      end;// case mPathList[i].PathType of
    end;// for i:=0  to mPathList.Count-1 do begin
  
    // Exclude
    for i:=0  to mPathExcludeList.Count-1 do begin
      case mPathExcludeList[i].PathType of
        ptDir: begin
          if mPathExcludeList[i].Checked then begin
            // Verzeichnis erzeugen und Parameter �bertragen
            xDir:=TDirectory.Create;
            xDir.FileName:=mPathExcludeList[i].Path;
            // Zur Liste hinzuf�gen
            MBCSParser.AddNewExcludeDirectory(xDir);
          end;// if mPathExcludeList[i].Checked then
        end;// ptDir: begin
        ptFile: begin
          if mPathExcludeList[i].Checked then begin
            // Dateiobjekt erzeugen und die Parameter eintragen
            xFile:=TFile.CreateParam(mPathExcludeList[i].Path, FLogFile, FChangedFiles);
            // In die Liste eintragen
            MBCSParser.AddNewExcludeFile(xFile);
          end;// if mPathExcludeList[i].Checked then
        end;// ptFile: begin
      end;// case mPathList[i].PathType of
    end;// for i:=0  to mPathList.Count-1 do begin
  
    // Tread starten
    MBCSParser.Resume;
  end;// if MBCSParser=nil then begin
end;// TMainWindow.bStartClick()

(********************************************************************************
  TMainWindow.bStopClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Unterbricht den Thread
********************************************************************************)
procedure TMainWindow.bStopClick(Sender: TObject);
begin
  if (MBCSParser<>nil) then
    MBCSParser.Terminate;
end;// TMainWindow.bStopClick()

(********************************************************************************
  TMainWindow.cbCreateNewLogFilesClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Wenn True, dann werden jedes mal neue Files erzeugt. Sonst werden die neuen Daten angeh�ngt
********************************************************************************)
procedure TMainWindow.cbCreateNewLogFilesClick(Sender: TObject);
begin
  MBCSSettings.CreateNewLogFiles := cbCreateNewLogFiles.Checked;
  FLogFile.CreateNewFile:=cbCreateNewLogFiles.Checked;
  FChangedFiles.CreateNewFile:=cbCreateNewLogFiles.Checked;
end;// TMainWindow.cbCreateNewLogFilesClick()

(********************************************************************************
  TMainWindow.cbScanOnlyClick
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Legt fest ob die Dateien nur durchsucht oder auch ge�ndert werden
********************************************************************************)
procedure TMainWindow.cbScanOnlyClick(Sender: TObject);
begin
  MBCSSettings.ScanOnly:=cbScanOnly.Checked;
end;// TMainWindow.cbScanOnlyClick()

(********************************************************************************
  TMainWindow.ExcludeProperties
  Sichtbarkeit: public
  Argumente   : (ASelected)
  Zweck:
    Eigenschaften eines Pfades der Exclude Liste (ObjectListBox)
********************************************************************************)
procedure TMainWindow.ExcludeProperties(ASelected:Integer);
begin
  PathPropertiesClick(mPathExcludeList[ASelected]);
  mPathExcludeListBox.SetList;
end;// TMainWindow.ExcludeProperties()

(********************************************************************************
  TMainWindow.FormClose
  Sichtbarkeit: published
  Argumente   : (Sender, Action)
  Zweck:
    Am Programmende alles wieder freigeben
********************************************************************************)
procedure TMainWindow.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  MBCSSettings.SplitterPos := pOptionPanel.Width;
  
  FChangedFiles.Free;
  FLogFile.Free;
  
  mPathListBox.SaveObjects;
  mPathListBox.Free;
  mPathExcludeListBox.SaveObjects;
  mPathExcludeListBox.Free;
  MBCSSettings.Free;
end;// TMainWindow.FormClose()

(********************************************************************************
  TMainWindow.GetLogFileDest
  Sichtbarkeit: private
  Argumente   : (aInitPath)
  Zweck:
    Fragt den gew�nschten Dateinamen des Log Files ab
********************************************************************************)
function TMainWindow.GetLogFileDest(aInitPath:string): String;
var
  OpenDialog: TOpenDialog;
begin
  result:=aInitPath;
  OpenDialog:=TOpenDialog.create(self);
  with OpenDialog do begin
    Title:='MBCS Tool - Log File';
    if Execute then begin
      result:=OpenDialog.FileName;
    end;// if Execute then begin
  end;// with OpenDialog do begin
end;// TMainWindow.GetLogFileDest()

(********************************************************************************
  TMainWindow.IncludeProperties
  Sichtbarkeit: public
  Argumente   : (ASelected)
  Zweck:
    Eigenschaften eines Pfades der Exclude Liste (ObjectListBox)
********************************************************************************)
procedure TMainWindow.IncludeProperties(ASelected:Integer);
begin
  PathPropertiesClick(mPathList[ASelected]);
  mPathListBox.SetList;
end;// TMainWindow.IncludeProperties()

(********************************************************************************
  TMainWindow.Init
  Sichtbarkeit: public
  Argumente   : 
  Zweck:
    Initialisiert die Listen mit den Verzeichnissen oder Files
    Erzeugt die ObjektListen f�r die Verwaltung der 
    Dateien und Verzeichnissen.
********************************************************************************)
procedure TMainWindow.Init;
begin
  RegisterClass(TPath);
  
  // Pfadliste f�r die Include Pfade
  mPathList:=TPathList.Create;
  mPathListBox:=TObjectCheckListBox.Create(self);
  with mPathListBox do begin
    parent:=gbDirectoryList;
    List:=@mPathList;
    ButtonPos:=bpBottom;
    butCopy.visible:=true;
    butAdd.Caption:='Add Dir...';
    butCopy.Caption:='Add File...';
    SetBounds(5,15,parent.Width-10,parent.Height-20);
  
    // Class Funktionen zuweisen
    ObjectNameFunc:=TPath.ObjectName;
    CheckedFunc:=TPath.CheckedFunc;
    EnableFunc:=TPath.EnableFunc;
  
    // Ereigniusproceduren
    OnAddClick:=AddClick;
    OnPropClick:=IncludeProperties;
    OnBeforeCopy:=AddFileClick;
    OnItemChange:=ItemIncludeChange;
  
    // Speicherpfad f�r die Konfigurationsdatei
    Path:=ChangeFileExt(Application.ExeName,'.include');
    // Konfiguration laden
    LoadObjects;
  end;// with mPathListBox do begin
  
  // Pfadliste f�r die Exclude Pfade
  mPathExcludeList:=TPathList.Create;
  mPathExcludeListBox:=TObjectCheckListBox.Create(self);
  with mPathExcludeListBox do begin
    parent:=gbDirectoryExcludeList;
    List:=@mPathExcludeList;
    ButtonPos:=bpBottom;
    butCopy.visible:=true;
    butAdd.Caption:='Add Dir...';
    butCopy.Caption:='Add File...';
    SetBounds(5,15,parent.Width-10,parent.Height-20);
  
    // Class Funktionen zuweisen
    ObjectNameFunc:=TPath.ObjectName;
    CheckedFunc:=TPath.CheckedFunc;
    EnableFunc:=TPath.EnableFunc;
  
    // Ereigniusproceduren
    OnAddClick:=AddExcludeClick;
    OnPropClick:=ExcludeProperties;
    OnBeforeCopy:=AddFileExcludeClick;
    OnItemChange:=ItemExcludeChange;
  
    // Speicherpfad f�r die Konfigurationsdatei
    Path:=ChangeFileExt(Application.ExeName,'.exclude');
    // Konfiguration laden
    LoadObjects;
  end;// with mPathExcludeListBox do begin
  
  // Applikations Einstellungen (inkl. Fensterposition)
  MBCSSettings:=TMBCSSettings.Create(MainWindow,ChangeFileExt(Application.ExeName,'.set'),false);
  
  // Defaultdateiname setzen
  MBCSSettings.ChangedFiles:=ExtractFilePath(Application.ExeName)+'\MBCS_ChangedLog.log';
  MBCSSettings.LogFile:=ExtractFilePath(Application.ExeName)+'\MBCS_Log.log';
  
  Application.ProcessMessages;
  // Koordinaten des Hauptformulars Persistent
  MBCSSettings.Forms.Add(MainWindow);
  
  // Einstellungen laden
  MBCSSettings.Load;
  // ... und die gespeicherten Koordinaten wiederherstellen
  MBCSSettings.RestoreForm(MainWindow);
  
  if MBCSSettings.SplitterPos > 10 then
    pOptionPanel.Width:=MBCSSettings.SplitterPos;
  
  // Logfile f�r alle Eintr�ge
  FChangedFiles:=TLogFile.Create;
  FChangedFiles.FileName:=MBCSSettings.ChangedFiles;
  FChangedFiles.CreateNewFile:=MBCSSettings.CreateNewLogFiles;
  
  // Logfile f�r die ge�nderten Dateien
  FLogFile:=TLogFile.Create;
  FLogFile.FileName:=MBCSSettings.LogFile;
  FLogFile.CreateNewFile:=MBCSSettings.CreateNewLogFiles;
end;// TMainWindow.Init()

(********************************************************************************
  TMainWindow.ItemChange
  Sichtbarkeit: private
  Argumente   : (PathList, CheckBox, NewIndex, OldIndex)
  Zweck:
    Aktualisiert die Anzeige der Eigenschaften des selektierten Pfades
********************************************************************************)
procedure TMainWindow.ItemChange(PathList: TPathList; CheckBox: TmmCheckBox; NewIndex: Integer; OldIndex: Integer);
begin
  if CheckBox<>nil then begin
    if ((OldIndex<PathList.Count) and (OldIndex>=0) and (PathList[OldIndex].PathType = ptDir)) then
      PathList[OldIndex].SubDir:=CheckBox.Checked;
    // Wenn der Pfad eine Datei ist, die CheckBox f�r die Unterverzeichnisse nicht verf�gbar
    if ((NewIndex<PathList.Count) and(NewIndex>=0) and (PathList[NewIndex].PathType = ptDir)) then begin
      CheckBox.Checked:=PathList[NewIndex].SubDir;
      CheckBox.Enabled:=true;
    end else begin
      CheckBox.Checked:=false;
      CheckBox.Enabled:=false;
    end;// if ((NewIndex<PathList.Count) and(NewIndex>=0) and (PathList[NewIndex].PathType = ptDir)) then begin
  end;// if CheckBox=nil then begin
end;// TMainWindow.ItemChange()

(********************************************************************************
  TMainWindow.ItemExcludeChange
  Sichtbarkeit: private
  Argumente   : (NewIndex, OldIndex)
  Zweck:
    Ereignisprozedur wenn die Selektion in der Exclude Liste �ndert
********************************************************************************)
procedure TMainWindow.ItemExcludeChange(NewIndex: Integer; OldIndex: Integer);
begin
  ItemChange(mPathExcludeList, nil, NewIndex, OldIndex);
end;// TMainWindow.ItemExcludeChange()

(********************************************************************************
  TMainWindow.ItemIncludeChange
  Sichtbarkeit: private
  Argumente   : (NewIndex, OldIndex)
  Zweck:
    Ereignisprozedur wenn die Selektion in der Include Liste �ndert
********************************************************************************)
procedure TMainWindow.ItemIncludeChange(NewIndex: Integer; OldIndex: Integer);
begin
  ItemChange(mPathList,cbIncludeSubDir, NewIndex, OldIndex);
end;// TMainWindow.ItemIncludeChange()

(********************************************************************************
  TMainWindow.OnThreadTerminate
  Sichtbarkeit: private
  Argumente   : (Sender)
  Zweck:
    Wird Aufgerufen bevor der Thread freigegeben wird
********************************************************************************)
procedure TMainWindow.OnThreadTerminate(Sender: TObject);
begin
  // GUI Elemente wieder verf�gbar
  aStop.Execute;
  // Initialisieren
  MBCSParser:=nil;
end;// TMainWindow.OnThreadTerminate()

(********************************************************************************
  TMainWindow.PathPropertiesClick
  Sichtbarkeit: private
  Argumente   : (APath)
  Zweck:
    Erm�glicht das �ndern eines Pfades (ObjectListBox)
********************************************************************************)
procedure TMainWindow.PathPropertiesClick(APath:TPath);
var
  OpenDialog: TOpenDialog;
  DirectoryDialog: TDirectoryDialog;
begin
  // Je nachdem ob Verzeichnis oder File ausgew�hlt ist, den entsprechenden Dialog anzeigen
  if APath.PathType = ptDir then begin
    DirectoryDialog:=TDirectoryDialog.create(self);
    with DirectoryDialog do begin
      InitDir:=APath.Path;
      DialogTitle:='MBCS Tool - Change Path';
      if Execute then
        // Verzeichnis aktualisieren
        APath.Path:=DirectoryDialog.Path;
    end;// with DirectoryDialog do begin
  end else begin
    OpenDialog:=TOpenDialog.create(self);
    with OpenDialog do begin
      InitialDir:=APath.Path;
      Title:='MBCS Tool - Change File';
      if Execute then
        // Dateipfad aktualisieren
        APath.Path:=FileName;
    end;// with DirectoryDialog do begin
  end;// if Path.Representation = rDirectory then begin
end;// TMainWindow.PathPropertiesClick()

(********************************************************************************
  TMainWindow.pcOptionsResize
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Anpassend er Steuerelemente bei Gr�ssenanderung des Fensters
    Aktualisiertdie die Dimensionen der Objekt Listboxen und deren Buttons
********************************************************************************)
procedure TMainWindow.pcOptionsResize(Sender: TObject);
begin
  with mPathListBox do
    mPathListBox.SetBounds(5,15,parent.Width-10,parent.Height-20);
  
  with mPathExcludeListBox do
    mPathExcludeListBox.SetBounds(5,15,parent.Width-10,parent.Height-20);
end;// TMainWindow.pcOptionsResize()

(********************************************************************************
  TMainWindow.tabSettingsShow
  Sichtbarkeit: default
  Argumente   : (Sender)
  Zweck:
    Einstellungen �bernehmen
********************************************************************************)
procedure TMainWindow.tabSettingsShow(Sender: TObject);
begin
  cbScanOnly.Checked := MBCSSettings.ScanOnly;
  eLogFile.Text := MBCSSettings.LogFile;
  eChangedFiles.Text := MBCSSettings.ChangedFiles;
  cbCreateNewLogFiles.Checked := MBCSSettings.CreateNewLogFiles;
end;// TMainWindow.tabSettingsShow()


initialization
end.












