object MainWindow: TMainWindow
  Left = 539
  Top = 232
  Width = 666
  Height = 430
  Caption = 'MBCS Tool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object mmSplitter: TmmSplitter
    Left = 300
    Top = 0
    Width = 3
    Height = 381
    Cursor = crHSplit
  end
  object mmStatusMemo: TmmRichEdit
    Left = 303
    Top = 0
    Width = 355
    Height = 381
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
    WordWrap = False
  end
  object mmStatusBar: TmmStatusBar
    Left = 0
    Top = 381
    Width = 658
    Height = 22
    Panels = <>
    SimplePanel = True
    SimpleText = 'W:\MillMaster\Components\VCL\AssignHandlerComp.pas'
  end
  object pOptionPanel: TmmPanel
    Left = 0
    Top = 0
    Width = 300
    Height = 381
    Align = alLeft
    Caption = 'pOptionPanel'
    TabOrder = 2
    object mmPanel1: TmmPanel
      Left = 1
      Top = 328
      Width = 298
      Height = 52
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object bStart: TmmBitBtn
        Left = 0
        Top = 0
        Width = 149
        Height = 52
        Anchors = [akLeft, akTop, akBottom]
        Caption = 'Start'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        Visible = True
        OnClick = bStartClick
        Kind = bkOK
        AutoLabel.LabelPosition = lpLeft
      end
      object bStop: TmmBitBtn
        Left = 148
        Top = 0
        Width = 150
        Height = 52
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'Stop'
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -24
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
        Visible = True
        OnClick = bStopClick
        Kind = bkAbort
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object pcOptions: TmmPageControl
      Left = 1
      Top = 1
      Width = 298
      Height = 327
      ActivePage = tabSettings
      Align = alClient
      TabOrder = 1
      OnResize = pcOptionsResize
      object tabConfig: TTabSheet
        Caption = 'Konfigurieren'
        object mmPanel3: TmmPanel
          Left = 0
          Top = 0
          Width = 290
          Height = 299
          Align = alLeft
          Anchors = [akLeft, akTop, akRight, akBottom]
          BevelOuter = bvNone
          Caption = 'mmPanel3'
          TabOrder = 0
          object mmPanel2: TmmPanel
            Left = 0
            Top = 266
            Width = 290
            Height = 33
            Align = alBottom
            Anchors = []
            BevelOuter = bvNone
            TabOrder = 0
            object cbIncludeSubDir: TmmCheckBox
              Left = 8
              Top = 8
              Width = 257
              Height = 17
              Caption = 'Include Sub Dirs'
              Checked = True
              Enabled = False
              State = cbChecked
              TabOrder = 0
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
          object gbDirectoryList: TmmGroupBox
            Left = 0
            Top = 0
            Width = 290
            Height = 266
            Align = alClient
            Caption = 'Directories / Files to include'
            TabOrder = 1
          end
        end
      end
      object TabSheet1: TTabSheet
        Caption = 'Exclude'
        ImageIndex = 2
        object gbDirectoryExcludeList: TmmGroupBox
          Left = 0
          Top = 0
          Width = 290
          Height = 299
          Align = alClient
          Caption = 'Directories / Files to exclude'
          TabOrder = 0
        end
      end
      object tabSettings: TTabSheet
        Caption = 'Settings'
        ImageIndex = 1
        OnShow = tabSettingsShow
        object mmStaticText2: TmmStaticText
          Left = 8
          Top = 136
          Width = 71
          Height = 17
          Caption = 'Changed Files'
          TabOrder = 6
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmStaticText1: TmmStaticText
          Left = 8
          Top = 64
          Width = 41
          Height = 17
          Caption = 'Log File'
          TabOrder = 5
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object eLogFile: TmmEdit
          Left = 8
          Top = 80
          Width = 273
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          Visible = True
          OnChange = bLogFileClick
          AutoLabel.LabelPosition = lpLeft
          Decimals = 0
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object bLogFile: TmmBitBtn
          Left = 253
          Top = 82
          Width = 27
          Height = 21
          Anchors = [akTop, akRight]
          TabOrder = 2
          Visible = True
          OnClick = bLogFileClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
            333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
            300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
            333337F373F773333333303330033333333337F3377333333333303333333333
            333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
            333337777F337F33333330330BB00333333337F373F773333333303330033333
            333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
            333377777F77377733330BBB0333333333337F337F33333333330BB003333333
            333373F773333333333330033333333333333773333333333333}
          NumGlyphs = 2
          AutoLabel.LabelPosition = lpLeft
        end
        object eChangedFiles: TmmEdit
          Left = 8
          Top = 152
          Width = 273
          Height = 24
          Anchors = [akLeft, akTop, akRight]
          AutoSize = False
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          Visible = True
          OnChange = bDirChangedFilesClick
          AutoLabel.LabelPosition = lpLeft
          Decimals = 0
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
        object bDirChangedFiles: TmmBitBtn
          Left = 253
          Top = 154
          Width = 27
          Height = 21
          Anchors = [akTop, akRight]
          TabOrder = 4
          Visible = True
          OnClick = bDirChangedFilesClick
          Glyph.Data = {
            76010000424D7601000000000000760000002800000020000000100000000100
            04000000000000010000120B0000120B00001000000000000000000000000000
            800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00303333333333
            333337F3333333333333303333333333333337F33FFFFF3FF3FF303300000300
            300337FF77777F77377330000BBB0333333337777F337F33333330330BB00333
            333337F373F773333333303330033333333337F3377333333333303333333333
            333337F33FFFFF3FF3FF303300000300300337FF77777F77377330000BBB0333
            333337777F337F33333330330BB00333333337F373F773333333303330033333
            333337F3377333333333303333333333333337FFFF3FF3FFF333000003003000
            333377777F77377733330BBB0333333333337F337F33333333330BB003333333
            333373F773333333333330033333333333333773333333333333}
          NumGlyphs = 2
          AutoLabel.LabelPosition = lpLeft
        end
        object bShowCahngedFiles: TmmBitBtn
          Left = 8
          Top = 176
          Width = 273
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          Caption = 'show logfile'
          TabOrder = 7
          Visible = True
          OnClick = bShowLogFileClick
          AutoLabel.LabelPosition = lpLeft
        end
        object bShowLogFile: TmmBitBtn
          Left = 8
          Top = 104
          Width = 273
          Height = 17
          Anchors = [akLeft, akTop, akRight]
          Caption = 'show logfile'
          TabOrder = 8
          Visible = True
          OnClick = bShowLogFileClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbScanOnly: TmmCheckBox
          Left = 8
          Top = 24
          Width = 97
          Height = 17
          Caption = 'Scan Only'
          TabOrder = 0
          Visible = True
          OnClick = cbScanOnlyClick
          AutoLabel.LabelPosition = lpLeft
        end
        object cbCreateNewLogFiles: TmmCheckBox
          Left = 8
          Top = 208
          Width = 153
          Height = 17
          Caption = 'Create new logfiles'
          TabOrder = 9
          Visible = True
          OnClick = cbCreateNewLogFilesClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  object ActionList: TActionList
    Left = 253
    Top = 1
    object aStart: TAction
      Caption = 'aStart'
      OnExecute = aStartExecute
    end
    object aStop: TAction
      Caption = 'aStop'
      OnExecute = aStopExecute
    end
  end
end
