object frmSelectDrive: TfrmSelectDrive
  Left = 463
  Top = 287
  Width = 234
  Height = 130
  BorderIcons = []
  Caption = 'Select Drive'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 9
    Top = 8
    Width = 176
    Height = 13
    Caption = 'Select a drive where the projects are:'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cbDrive: TDriveComboBox
    Left = 8
    Top = 24
    Width = 209
    Height = 19
    TabOrder = 0
  end
  object bOK: TmmBitBtn
    Left = 75
    Top = 64
    Width = 75
    Height = 25
    TabOrder = 1
    Visible = True
    Kind = bkOK
    AutoLabel.LabelPosition = lpLeft
  end
end
