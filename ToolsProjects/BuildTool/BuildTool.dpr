program BuildTool;



uses
 // madExcept,
 // madLinkDisAsm,
  Forms,
  MainForm in 'MainForm.pas' {frmMain},
  SelectDriveForm in 'SelectDriveForm.pas' {frmSelectDrive};

{$R *.RES}
{$R Version.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmSelectDrive, frmSelectDrive);
  Application.Run;
end.
