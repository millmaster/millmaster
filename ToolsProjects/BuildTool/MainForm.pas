(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MainFrom.pas
| Projectpart...: BuildTool
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 17.05.2005 1.00 Wss | ProductVersion wird nun auch von FileVersion gesetzt
|=============================================================================*)
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IvMlDlgs, mmOpenDialog, StdCtrls, mmButton, ComCtrls, mmListView,
  mmPageControl, ActnList, mmActionList, Buttons, mmSpeedButton, mmLabel,
  mmEdit, mmMemo, mmBitBtn, Spin, mmSpinEdit, mmCheckBox, FolderBrowser,
  ExtCtrls, mmPanel, mmStatusBar, BaseForm, mmLineLabel, mmSplitter, syncobjs,
  mmThread;

type
  TfrmMain = class(TmmForm)
    mmPageControl1: TmmPageControl;
    tsCompile: TTabSheet;
    tsOptions: TTabSheet;
    lvProjects: TmmListView;
    mmOpenDialog: TmmOpenDialog;
    mmActionList: TmmActionList;
    acAdd: TAction;
    acDelete: TAction;
    acBuildAll: TAction;
    acBuildSelected: TAction;
    edDrcc: TmmEdit;
    laDrcc: TmmLabel;
    laOutputPath: TmmLabel;
    edOutputPath: TmmEdit;
    laBrcc32: TmmLabel;
    edBrcc32: TmmEdit;
    bDcc32Exe: TmmSpeedButton;
    bBrcc32Exe: TmmSpeedButton;
    bOutputPath: TmmSpeedButton;
    meConfig: TmmMemo;
    laConfig: TmmLabel;
    meSearchPath: TmmMemo;
    laSearchPath: TmmLabel;
    acBuildRessource: TAction;
    acOpenPas: TAction;
    tsBuildLog: TTabSheet;
    meBuildLog: TmmMemo;
    pnSpinEdit: TmmPanel;
    laVProduct: TmmLabel;
    laVDatabase: TmmLabel;
    laVApplication: TmmLabel;
    seVProduct: TmmSpinEdit;
    seVDatabase: TmmSpinEdit;
    seVApplication: TmmSpinEdit;
    bBuildRessource: TmmButton;
    bSetVersionInfo: TmmButton;
    acSetVersionInfo: TAction;
    pnBuildLog: TmmPanel;
    bFindError: TmmButton;
    meComments: TmmMemo;
    laComments: TmmLabel;
    sbMain: TmmStatusBar;
    mmPanel1: TmmPanel;
    bAdd: TmmButton;
    bDelete: TmmButton;
    bBuildAll: TmmButton;
    bBuildSelected: TmmButton;
    laYMSettings: TmmLabel;
    bYMSettings: TmmSpeedButton;
    laBaseGlobal: TmmLabel;
    bBaseGlobal: TmmSpeedButton;
    laMMUGlobal: TmmLabel;
    bMMUGlobal: TmmSpeedButton;
    laYMSettingsDate: TmmLabel;
    laBaseGlobalDate: TmmLabel;
    laMMUGlobalDate: TmmLabel;
    edYMSettings: TmmEdit;
    edBaseGlobal: TmmEdit;
    edMMUGlobal: TmmEdit;
    mmLineLabel1: TmmLineLabel;
    mmLineLabel2: TmmLineLabel;
    meHostory: TmmMemo;
    mmLineLabel3: TmmLineLabel;
    mmSplitter1: TmmSplitter;
    acSelectAll: TAction;
    FolderBrowser: TFolderBrowser;
    cbThreaded: TmmCheckBox;
    edContStartDelay: TmmEdit;
    mmLabel2: TmmLabel;
    mmButton1: TmmButton;
    procedure acAddExecute(Sender: TObject);
    procedure acBuildAllExecute(Sender: TObject);
    procedure acBuildCancelExecute(Sender: TObject);
    procedure acBuildRessourceExecute(Sender: TObject);
    procedure acBuildSelectedExecute(Sender: TObject);
    procedure acDeleteExecute(Sender: TObject);
    procedure acSetVersionInfoExecute(Sender: TObject);
    procedure bFindErrorClick(Sender: TObject);
    procedure bOutputPathClick(Sender: TObject);
    procedure bPascalFileSelectClick(Sender: TObject);
    procedure bSelectExeClick(Sender: TObject);
    procedure edPasFileChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lvProjectsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure acSelectAllExecute(Sender: TObject);
    procedure mmPageControl1DrawTab(Control: TCustomTabControl;
      TabIndex: Integer; const Rect: TRect; Active: Boolean);
    procedure mmButton1Click(Sender: TObject);
  private
    mCSLogFile: TCriticalSection;
    mDrive: String[1];
    mHomeDir: String;
    mLogFile: String;
    mLogLine: Integer;
    mResChanged: Boolean;
    procedure AddProject(aProject: String);
    procedure CopyCfgFile;
    procedure BuildProject(aIndex: Integer);
    procedure BuildProjectThreaded(aIndex: Integer);
    procedure BuildRessource(aIndex: Integer);
    procedure GetVersionInfo(aVersionInfo: String; var aV1, aV2, aV3: Integer);
    procedure OpenLogFile;
    function SetVersionInfo(aV1, aV2, aV3: Integer): String;
    function GetHistoryLines(aName: String): String;
  public
    procedure ClearDirectory(aFilePath: String);
    procedure Loaded; override;
  end;

type
  TBuildThread = class(TmmThread)
  private
    mCS: TCriticalSection;
    mTempPath: string;
    mMainLogFile: string;
    mProjectName: string;
    mProjectPath: string;
  protected
    procedure Execute; override;
  public
    constructor Create; reintroduce; virtual;
    procedure Compile(aTempPath, aProjectPath, aProjectName, aMainLogfile: String; aCS: TCriticalSection);
  end;

var
  frmMain: TfrmMain;

implementation
{$R *.DFM}
uses
  mmCS,
  IniFiles, ShellApi, SelectDriveForm, LoepfeGlobal;

const
  cCommentsMask       = 'VALUE "Comments", "%s\0"';
  cDriveMask          = '[Drive]';
  cFileVersionMask    = 'VALUE "FileVersion", "%s\0"';
  cProductVersionMask = 'VALUE "ProductVersion", "%s\0"';
  cLastChangeMask     = '// Last change:';  // search for this text for additional version info in "SystemInfo"
  cSystemInfoMask     = 'VALUE "SystemInfo", "YM: %s; BG: %s; MMU: %s\0"';
  cHistoryFile        = 'BuildToolHistory.ini';

  cMsgCurProject      = 'No project compiling';
  cErrorString        = 'Fatal';

//  cCRLF               = #13#10;
  cTextlineSeparator  = '@@';

  cVersion         = 0;
  cChanged         = 1;
  cPath            = 2;
  cComments        = 3;

//------------------------------------------------------------------------------
var
  lDrcc: String;
  lOutputPath: String;
//------------------------------------------------------------------------------
procedure TfrmMain.AddProject(aProject: String);
var
  xItem: TListItem;
  xVInfo: TStringList;
  xPath, xVersion, xComments: String;
  //...................................................
  function ExtractInfoString(aInfoList: TStringList; aName: String): String;
  var
    i: Integer;
  begin
    Result := '';
    with aInfoList do
      for i:=0 to Count-1 do begin
        if Pos(aName, Strings[i]) > 0 then begin
          Result := StringReplace(Strings[i], '"','', [rfReplaceAll]);
          System.Delete(Result, 1, Pos(',', Result));
          System.Delete(Result, Pos('\', Result), 2);
          Result := Trim(Result);
          Break;
        end;
      end;
  end;
  //...................................................
begin
  lvProjects.Items.BeginUpdate;
  xItem := lvProjects.Items.Add;                                   // set Project
  xItem.Caption := ExtractFileName(aProject);
  xPath := ExtractFilePath(aProject);
  if FileExists(xPath + 'Version.rc') then begin
    xVInfo := TStringList.Create;
    try
      xVInfo.LoadFromFile(xPath + 'Version.rc');
      xVersion  := ExtractInfoString(xVInfo, 'FileVersion');  // Version
      xComments := ExtractInfoString(xVInfo, 'Comments');     // Comments
    finally
      xVInfo.Free;
    end;
  end else begin
    xVersion  := '0.00.00';
    xComments := 'No comments';
  end;

  xItem.SubItems.Add(xVersion);  // Version
  xItem.SubItems.Add(' ');       // Changed
  xItem.SubItems.Add(xPath);     // Path
  xItem.SubItems.Add(xComments); // Comments

  lvProjects.Items.EndUpdate;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acAddExecute(Sender: TObject);
begin
  with mmOpenDialog do begin
    FileName := '*.dpr';
    if Execute then begin
      AddProject(FileName);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acBuildCancelExecute(Sender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acBuildAllExecute(Sender: TObject);
var
  i: Integer;
  xFirst: Boolean;
begin
  Screen.Cursor := crHourGlass;
  xFirst := True;

  CopyCfgFile;
  for i:=0 to lvProjects.Items.Count-1 do begin
    if cbThreaded.Checked and (not xFirst) then begin
      BuildProjectThreaded(i);
      Sleep(edContStartDelay.AsInteger * 1000);
    end else begin
      xFirst := False;
      BuildProject(i);
    end;
  end;

  OpenLogFile;
  Screen.Cursor := crDefault;

//  Screen.Cursor := crHourGlass;
//  xDelay := edFirstStartDelay.AsInteger * 1000;
//  xFirst := True;
//
//  CopyCfgFile;
//  for i:=0 to lvProjects.Items.Count-1 do begin
//    if cbThreaded.Checked then begin
//      BuildProjectThreaded(i);
//      Sleep(xDelay);
//      xDelay := edContStartDelay.AsInteger * 1000;
//     end else
//      BuildProject(i);
//  end;
//
//  OpenLogFile;
//  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acBuildSelectedExecute(Sender: TObject);
var
  i: Integer;
  xFirst: Boolean;
begin
  Screen.Cursor := crHourGlass;
  xFirst        := True;

  CopyCfgFile;
  for i:=0 to lvProjects.Items.Count-1 do begin
    if lvProjects.Items[i].Selected then
      if cbThreaded.Checked and (not xFirst) then begin
        BuildProjectThreaded(i);
        Sleep(edContStartDelay.AsInteger * 1000);
      end else begin
        xFirst := False;
        BuildProject(i);
      end;
  end;

  OpenLogFile;
  Screen.Cursor := crDefault;

//  Screen.Cursor := crHourGlass;
//
//  CopyCfgFile;
//  if lvProjects.SelCount > 1 then xDelay := edFirstStartDelay.AsInteger * 1000
//                             else xDelay := 1;
//  for i:=0 to lvProjects.Items.Count-1 do begin
//    if lvProjects.Items[i].Selected then
//      if cbThreaded.Checked then begin
//        BuildProjectThreaded(i);
//        Sleep(xDelay);
//        xDelay := edContStartDelay.AsInteger * 1000;
//      end else
//        BuildProject(i);
//  end;
//
//  OpenLogFile;
//
//  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acBuildRessourceExecute(Sender: TObject);
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;

  for i:=0 to lvProjects.Items.Count-1 do
    if lvProjects.Items[i].SubItems[cChanged] = 'X' then begin
      lvProjects.Items[i].SubItems[cChanged] := ' ';
      BuildRessource(i);
    end;
  mResChanged := False;

  ChDir(mHomeDir);
  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acDeleteExecute(Sender: TObject);
begin
  while lvProjects.SelCount > 0 do
  try
    lvProjects.Items.Delete(lvProjects.Selected.Index);
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acSelectAllExecute(Sender: TObject);
var
  i: Integer;
begin
  with lvProjects do begin
    for i:=0 to Items.Count-1 do
      Items[i].Selected := True;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acSetVersionInfoExecute(Sender: TObject);
var
  i: Integer;
  xV1, xV2, xV3: Integer;
  xStrList: TStringList;
  //.............................................
{
 procedure DeleteLine(aStr: String);
  var
    i: Integer;
  begin
    for i:=0 to xStrList.Count-1 do
      if Pos(aStr, xStrList.Strings[i]) > 0 then begin
        xStrList.Delete(i);
        Break;
      end;
  end;
  //.............................................
  procedure UpdateRCFile(aVersion: String);
  var
    xStr: String;
  begin
    xStr := 'Version.rc';
    if FileExists(xStr) then begin
      xStrList.LoadFromFile(xStr);
      DeleteLine('FILEVERSION');  // Info in header part of rc file for W2000
      DeleteLine('FileVersion');
      DeleteLine('SystemInfo');
      DeleteLine('Comments');

      xStrList.Insert(xStrList.Count-3, Format(cFileVersionMask, [aVersion]));
      xStrList.Insert(xStrList.Count-3, Format(cSystemInfoMask,  [laYMSettingsDate.Caption, laBaseGlobalDate.Caption, laMMUGlobalDate.Caption]));
      xStrList.Insert(xStrList.Count-3, Format(cCommentsMask,    [meComments.Text]));

      xStrList.SaveToFile(xStr);
    end;
  end;
  //.............................................
{}
begin
  if (lvProjects.SelCount > 0) then begin
    mResChanged := True;
    xStrList := TStringList.Create;
    for i:=0 to lvProjects.Items.Count-1 do
      with lvProjects.Items[i] do begin
        if Selected then begin
          GetVersionInfo(SubItems[cVersion], xV1, xV2, xV3);
          xV1 := seVProduct.Value;
          xV2 := seVDatabase.Value;
          xV3 := seVApplication.Value;

          SubItems[cVersion]  := SetVersionInfo(xV1, xV2, xV3);
          SubItems[cChanged]  := 'X';
          SubItems[cComments] := meComments.Text;
          // now save changes to RC file
        end;
      end;
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bFindErrorClick(Sender: TObject);
var
  i, j, xSkipChars: Integer;
  xPos: Integer;
begin
  xPos := 0;
  for i:=mLogLine to meBuildLog.Lines.Count-1 do begin
    xPos := Pos(cErrorString, meBuildLog.Lines.Strings[i]);
    if xPos > 0 then begin
      xSkipChars := 0;
      for j:=0 to i-1 do
        xSkipchars := xSkipchars + Length(meBuildLog.Lines[j]);
      xSkipChars := xSkipChars + (i*2);
      xSkipChars := xSkipChars + xPos - 1;

      meBuildLog.SetFocus;
      meBuildLog.SelStart := xSkipChars;
      meBuildLog.SelLength := Length(cErrorString);

      mLogLine := i+1;
      Break;
    end;
  end;
  if (mLogLine >= meBuildLog.Lines.Count) or (xPos = 0) then
    mLogLine := 0;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bOutputPathClick(Sender: TObject);
begin
  with FolderBrowser do begin
    ParentForm := Self;
    Folder := edOutputPath.Text;
    if Execute then begin
      edOutputPath.Text := Folder;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bPascalFileSelectClick(Sender: TObject);
begin
  with mmOpenDialog do begin
    FileName := '*.pas';
    if Execute then begin
      case (Sender as TControl).Tag of
        0: edYMSettings.Text := FileName;
        1: edBaseGlobal.Text := FileName;
        2: edMMUGlobal.Text  := FileName;
      else
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bSelectExeClick(Sender: TObject);
begin
  with mmOpenDialog do begin
    FileName := '*.exe; *.bat';
    if Execute then begin
      case (Sender as TControl).Tag of
        0: edDrcc.Text := FileName;
        1: edBrcc32.Text := FileName;
        2: edOutputPath.Text := FileName;
      else
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.BuildProject(aIndex: Integer);
var
  xStr: String;
  xStartupInfo: TStartupInfo;
  xProcessInfo: TProcessInformation;
  xFile: TextFile;
  xPath: Array[0..100] of Char;
begin
  if aIndex >= lvProjects.Items.Count then Exit;

  with lvProjects.Items[aIndex] do begin
    sbMain.SimpleText := 'Compiling project: ' + SubItems[cPath] + Caption;

    AssignFile(xFile, mLogFile);
    if FileExists(mLogFile) then begin
      Append(xFile);
      WriteLn(xFile);
    end else begin
      Rewrite(xFile);
      WriteLn(xFile, 'Compiled at ' + DateTimeToStr(Now));
    end;
    WriteLn(xFile, '*******************************************************************');
    WriteLn(xFile, '*** ' + Caption);
    WriteLn(xFile, '*******************************************************************');
    CloseFile(xFile);

    ChDir(SubItems[cPath]);
    xStr := Format('CMD /C "%s" -E%s %s >>%s', [edDrcc.Text, edOutputPath.Text, Caption, mLogFile]);
  end;

  GetStartupInfo(xStartupInfo);
  xStartupInfo.dwFlags := xStartupInfo.dwFlags or STARTF_USESHOWWINDOW;
  xStartupInfo.wShowWindow := SW_HIDE; //SW_MINIMIZE;
  if CreateProcess(Nil, PChar(xStr), nil, nil, True, NORMAL_PRIORITY_CLASS, nil, nil, xStartupInfo, xProcessInfo) then begin
    WaitForSingleObject(xProcessInfo.hProcess, INFINITE);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.BuildRessource(aIndex: Integer);
var
  xStr: String;
  xStrList: TStringList;
  xStartupInfo: TStartupInfo;
  xProcessInfo: TProcessInformation;
  xIni: TIniFile;
  //.......................................................
  procedure DeleteLine(aStr: String);
  var
    i: Integer;
  begin
    for i:=0 to xStrList.Count-1 do
      if Pos(aStr, xStrList.Strings[i]) > 0 then begin
        xStrList.Delete(i);
        Break;
      end;
  end;
  //.......................................................
begin
  if aIndex >= lvProjects.Items.Count then Exit;

  with lvProjects.Items[aIndex] do begin
    ChDir(SubItems[cPath]);
    xStr := 'Version.rc';
    if FileExists(xStr) then begin
      xIni := TIniFile.Create(mHomeDir + cHistoryFile);
      xIni.WriteString(Caption, SubItems[cVersion], SubItems[cComments]);
      xIni.Free;

      xStrList := TStringList.Create;
      xStrList.LoadFromFile(xStr);
      DeleteLine('FILEVERSION');  // file info in header part for Win2000
      DeleteLine('FileVersion');
      DeleteLine('ProductVersion');
      DeleteLine('SystemInfo');
      DeleteLine('Comments');

      xStrList.Insert(1, Format('FILEVERSION %s,0', [StringReplace(SubItems[cVersion], '.', ',', [rfReplaceAll])]));
      xStrList.Insert(xStrList.Count-3, Format(cFileVersionMask,    [SubItems[cVersion]]));
      xStrList.Insert(xStrList.Count-3, Format(cProductVersionMask, [SubItems[cVersion]]));
      xStrList.Insert(xStrList.Count-3, Format(cSystemInfoMask,     [laYMSettingsDate.Caption, laBaseGlobalDate.Caption, laMMUGlobalDate.Caption]));
      xStrList.Insert(xStrList.Count-3, Format(cCommentsMask,       [SubItems[cComments]]));

      xStrList.SaveToFile(xStr);

      GetStartupInfo(xStartupInfo);
      xStartupInfo.dwFlags := xStartupInfo.dwFlags or STARTF_USESHOWWINDOW;
      xStartupInfo.wShowWindow := SW_HIDE;
      xStr := Format('CMD /C "%s" %s', [edBrcc32.Text, 'Version.rc']);
      if CreateProcess(Nil, PChar(xStr), nil, nil, False, NORMAL_PRIORITY_CLASS, nil, nil, xStartupInfo, xProcessInfo) then begin
        WaitForSingleObject(xProcessInfo.hProcess, INFINITE);
      end;
    end else
      ShowMessage('Ressource file not found: ' + SubItems[cPath] + xStr);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.CopyCfgFile;
begin
  mLogLine := -1;
  ClearDirectory('C:\Temp\*.dcu');
  if FileExists(mLogFile) then
    DeleteFile(mLogFile);

  with TStringList.Create do
  try
    // adds the search paths in to config file
    Add('-U' + StringReplace(meSearchPath.Lines.Text, cCRLF, ';', [rfReplaceAll]));
    // adds the compiler options in to config file
    Add(meConfig.Lines.Text);
    // save config file into borlands bin directory
    SaveToFile(ExtractFilePath(edDrcc.Text) + 'Dcc32.cfg');
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.edPasFileChange(Sender: TObject);
var
  xStrList: TStringList;
  xStr: String;
  i: Integer;
  xIndex: Integer;
begin
  xStr := '00.00.0000';
  with Sender as TmmEdit do begin
    if FileExists(Text) then begin
      xStrList := TStringList.Create;

      xStrList.LoadFromFile(Text);
      for i:=0 to xStrList.Count-1 do begin
        if Pos(cLastChangeMask, xStrList.Strings[i]) > 0 then begin
           xStr := xStrList.Strings[i];
           Delete(xStr, 1, Length(cLastChangeMask));
           xIndex := Pos('/', xStr);
           if xIndex > 0 then
             xStr := Copy(xStr, 1, xIndex-1);
           break;
        end;
      end;
      xStrList.Free;
    end;

    case Tag of
      0: laYMSettingsDate.Caption := Trim(xStr);
      1: laBaseGlobalDate.Caption := Trim(xStr);
      2: laMMUGlobalDate.Caption  := Trim(xStr);
    else
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  i: Integer;
  xCnt: Integer;
  xStr: String;
begin
  mCSLogFile := TCriticalSection.Create;
  mDrive := 'C';
  mHomeDir := ExtractFilePath(Application.ExeName);
  mLogFile := mHomeDir + 'BuildTool.log';
  mLogLine := 0;
  mResChanged := False;

{
  with TIniFile.Create(mHomeDir + 'BuildTool.ini') do
  try
    edDrcc.Text                  := ReadString('Settings', 'DelphiEXE', '');
    edBrcc32.Text                := ReadString('Settings', 'Brcc32EXE', '');
    edOutputPath.Text            := ReadString('Settings', 'OutputPath', '');

    xStr := ReadString('Settings', 'CompilerConfig', '');
    meConfig.Lines.Text     := StringReplace(xStr, cTextlineSeparator, cCRLF, [rfReplaceAll]);

    xStr := ReadString('Settings', 'SearchPath', '');
    meSearchPath.Lines.Text := StringReplace(xStr, cTextlineSeparator, cCRLF, [rfReplaceAll]);

    xCnt := ReadInteger('Projects', 'Count', 0);
    for i:=1 to xCnt do begin
      xStr := ReadString('Projects', IntToStr(i-1), 'C:\NotFound');
      AddProject(xStr);
    end;

    edYMSettings.Text := ReadString('CheckModule', 'YMSettings', '');
    edBaseGlobal.Text := ReadString('CheckModule', 'BaseGlobal', '');
    edMMUGlobal.Text  := ReadString('CheckModule', 'MMUGlobal', '');
  finally
    Free;
  end;
{}

  OpenLogFile;

  RestoreFormLayout;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
var
  i: Integer;
  xStr: String;
begin
  mCSLogFile.Free;

  with TIniFile.Create(mHomeDir + 'BuildTool.ini') do
  try
    WriteString('Settings', 'DelphiEXE',  edDrcc.Text);
    WriteString('Settings', 'Brcc32EXE',  edBrcc32.Text);
    WriteString('Settings', 'OutputPath', edOutputPath.Text);

    xStr := StringReplace(meConfig.Lines.Text, cCRLF, cTextlineSeparator, [rfReplaceAll]);
    WriteString('Settings', 'CompilerConfig', xStr);

    xStr := StringReplace(meSearchPath.Lines.Text, cCRLF, cTextlineSeparator, [rfReplaceAll]);
    WriteString('Settings', 'SearchPath', xStr);
    WriteString('Settings', 'ProjectDrive', mDrive);

    WriteInteger('Projects', 'Count', lvProjects.Items.Count);
    for i:=0 to lvProjects.Items.Count-1 do begin
      with lvProjects.Items[i] do
        xStr := SubItems.Strings[cPath] + Caption;
      System.Delete(xStr, 1, 1);
      xStr := cDriveMask + xStr;
      WriteString('Projects', IntToStr(i), xStr);
    end;

    WriteString('CheckModule', 'YMSettings', edYMSettings.Text);
    WriteString('CheckModule', 'BaseGlobal', edBaseGlobal.Text);
    WriteString('CheckModule', 'MMUGlobal',  edMMUGlobal.Text);
  finally
    Free;
  end;

  SaveFormLayout;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormShow(Sender: TObject);
var
  i: Integer;
  xCnt: Integer;
  xStr: String;
begin
  with TIniFile.Create(mHomeDir + 'BuildTool.ini') do
  try
    edDrcc.Text       := ReadString('Settings', 'DelphiEXE', '');
    edBrcc32.Text     := ReadString('Settings', 'Brcc32EXE', '');
    edOutputPath.Text := ReadString('Settings', 'OutputPath', '');

    xStr := ReadString('Settings', 'CompilerConfig', '');
    meConfig.Lines.Text := StringReplace(xStr, cTextlineSeparator, cCRLF, [rfReplaceAll]);

    xStr := ReadString('Settings', 'SearchPath', '');
    meSearchPath.Lines.Text := StringReplace(xStr, cTextlineSeparator, cCRLF, [rfReplaceAll]);

    mDrive := ReadString('Settings', 'ProjectDrive', 'C');
    // now ask first for the drive where the projects are
    with TfrmSelectDrive.Create(Self) do
    try
      cbDrive.Drive := mDrive[1];
      ShowModal;
      mDrive := cbDrive.Drive;
    finally
      Free;
    end;

    xCnt := ReadInteger('Projects', 'Count', 0);
    for i:=1 to xCnt do begin
      xStr := ReadString('Projects', IntToStr(i-1), 'C:\NotFound');
      xStr := StringReplace(xStr, cDriveMask, mDrive, []);
      AddProject(xStr);
    end;

    edYMSettings.Text := ReadString('CheckModule', 'YMSettings', '');
    edBaseGlobal.Text := ReadString('CheckModule', 'BaseGlobal', '');
    edMMUGlobal.Text  := ReadString('CheckModule', 'MMUGlobal', '');
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.GetVersionInfo(aVersionInfo: String; var aV1, aV2, aV3: Integer);
var
  xStrList: TStringList;
begin
  xStrList := TStringList.Create;
  xStrList.CommaText := StringReplace(aVersionInfo, '.', ',', [rfReplaceAll]);
  try
    aV1 := StrToInt(xStrList.Strings[0]);
    aV2 := StrToInt(xStrList.Strings[1]);
    aV3 := StrToInt(xStrList.Strings[2]);
  except
    aV1 := 0;
    aV2 := 0;
    aV3 := 0;
  end;
  xStrList.Free;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.Loaded;
begin
  inherited;
  // set Comments col to 0
  with lvProjects.Columns do
    Items[4].Width := 0;
end;

//------------------------------------------------------------------------------
function SortList(aList: TStringList; aIndex1, aIndex2: Integer): Integer;
begin
  Result := AnsiCompareStr(aList.Strings[aIndex1], aList.Strings[aIndex2]) * -1;
end;

//------------------------------------------------------------------------------
procedure TfrmMain.BuildProjectThreaded(aIndex: Integer);
var
  xStr: String;
  xStartupInfo: TStartupInfo;
  xProcessInfo: TProcessInformation;
  xFile: TextFile;
  xPath: Array[0..100] of Char;
begin
  if aIndex >= lvProjects.Items.Count then Exit;

  lDrcc       := edDrcc.Text;
  lOutputPath := edOutputPath.Text;

  with lvProjects.Items[aIndex] do begin
    with TBuildThread.Create do 
      Compile(lOutputPath, SubItems[cPath], Caption, mLogFile, mCSLogFile);
  end;
  Sleep(2000);
{
    sbMain.SimpleText := 'Compiling project: ' + SubItems[cPath] + Caption;

    AssignFile(xFile, mLogFile);
    if FileExists(mLogFile) then begin
      Append(xFile);
      WriteLn(xFile);
    end else begin
      Rewrite(xFile);
      WriteLn(xFile, 'Compiled at ' + DateTimeToStr(Now));
    end;
    WriteLn(xFile, '*******************************************************************');
    WriteLn(xFile, '*** ' + Caption);
    WriteLn(xFile, '*******************************************************************');
    CloseFile(xFile);

    ChDir(SubItems[cPath]);
    xStr := Format('CMD /C "%s" -E%s %s >>%s', [edDrcc.Text, edOutputPath.Text, Caption, mLogFile]);
  end;

  GetStartupInfo(xStartupInfo);
  xStartupInfo.dwFlags := xStartupInfo.dwFlags or STARTF_USESHOWWINDOW;
  xStartupInfo.wShowWindow := SW_HIDE; //SW_MINIMIZE;
  if CreateProcess(Nil, PChar(xStr), nil, nil, True, NORMAL_PRIORITY_CLASS, nil, nil, xStartupInfo, xProcessInfo) then begin
    WaitForSingleObject(xProcessInfo.hProcess, INFINITE);
  end;
{}
end;

//------------------------------------------------------------------------------
function TfrmMain.GetHistoryLines(aName: String): String;
var
  xIni: TIniFile;
  xStrList: TStringList;
begin
  xIni := TIniFile.Create(mHomeDir + cHistoryFile);
  xStrList := TStringList.Create;
  xIni.ReadSectionValues(aName, xStrList);

  xStrList.CustomSort(SortList);
  Result := xStrList.Text;

  xIni.Free;
  xStrList.Free;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvProjectsChange(Sender: TObject; Item: TListItem; Change: TItemChange);
var
  xV1, xV2, xV3: Integer;
begin
  if (Item.SubItems.Count = 4) and (lvProjects.SelCount = 1) then begin
    GetVersionInfo(Item.SubItems[cVersion], xV1, xV2, xV3);
    seVProduct.Value     := xV1;
    seVDatabase.Value    := xV2;
    seVApplication.Value := xV3;

    meComments.Text := Item.SubItems[cComments];
    meHostory.Lines.Text := GetHistoryLines(Item.Caption);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
//  Handled := True;

  acDelete.Enabled        := (lvProjects.SelCount > 0);
  acBuildSelected.Enabled := (lvProjects.SelCount > 0);

  acBuildRessource.Enabled := mResChanged;
  acSetVersionInfo.Enabled := (lvProjects.SelCount > 0);

  seVProduct.Enabled      := (lvProjects.SelCount > 0);
  seVDatabase.Enabled     := (lvProjects.SelCount > 0);
  seVApplication.Enabled  := (lvProjects.SelCount > 0);
  meComments.Enabled      := (lvProjects.SelCount > 0);
  edContStartDelay.Enabled  := cbThreaded.Checked;

{
  seVApplication.Enabled  := (lvProjects.SelCount = 1);
  meComments.Enabled      := (lvProjects.SelCount = 1);
{}

  bFindError.Enabled := (Pos(cErrorString, meBuildLog.Text) > 0);

  with lvProjects do
    if Screen.Cursor = crDefault then
      if SelCount = 1 then
        sbMain.SimpleText := Selected.SubItems[cPath] + Selected.Caption
      else
        sbMain.SimpleText := Format('%d projects selected', [SelCount]);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.OpenLogFile;
begin
  if FileExists(mLogFile) then
    meBuildLog.Lines.LoadFromFile(mLogFile);
end;
//------------------------------------------------------------------------------
function TfrmMain.SetVersionInfo(aV1, aV2, aV3: Integer): String;
begin
  try
    Result := Format('%d.%0.2d.%0.2d', [aV1, aV2, aV3]);
  except
    Result := '0.00.00';
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mmPageControl1DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
var
  xLeft: Integer;
  xTop: Integer;
begin
  with Control.Canvas do begin
    Canvas.Brush.Color := clBtnFace;
    Canvas.FillRect(Rect);

    if (TabIndex = 2) and bFindError.Enabled then
      Font.Color := clRed;

    with Rect do begin
      xLeft := Left + ((Right - Left - TextWidth(mmPageControl1.Pages[TabIndex].Caption) +1) div 2);
      xTop  := Top + ((Bottom - Top - TextHeight(mmPageControl1.Pages[TabIndex].Caption) +1) div 2);
      xTop  := Top + 3;
    end;
    TextOut(xLeft, xTop, mmPageControl1.Pages[TabIndex].Caption);
  end;
end;

//:-----------------------------------------------------------------------------
//: TBuildThread
//:-----------------------------------------------------------------------------
constructor TBuildThread.Create;
begin
  // TODO -cMM: TBuildThread.Create default body inserted
  inherited Create(True);

end;
//:-----------------------------------------------------------------------------
procedure TBuildThread.Compile(aTempPath, aProjectPath, aProjectName, aMainLogfile: String; aCS: TCriticalSection);
begin
  mCS          := aCS;
  mProjectName := aProjectName;
  mProjectPath := aProjectPath;
  mTempPath    := aTempPath;

  mMainLogFile := aMainLogfile;

  Resume;
end;
//:-----------------------------------------------------------------------------
procedure TBuildThread.Execute;
var
  xLogFileName: string;
  xStr: String;
  xStartupInfo: TStartupInfo;
  xProcessInfo: TProcessInformation;
  xFile: TextFile;
  xPath: Array[0..100] of Char;
  xLogFile: TStringList;
begin
  xLogFileName := IncludeTrailingBackslash(mTempPath) + ChangeFileExt(mProjectName, '.log');

  AssignFile(xFile, xLogFileName);
  Rewrite(xFile);
  WriteLn(xFile, 'Compiled at ' + DateTimeToStr(Now));
  WriteLn(xFile, '*******************************************************************');
  WriteLn(xFile, '*** ' + mProjectName);
  WriteLn(xFile, '*******************************************************************');
  CloseFile(xFile);

  ChDir(mProjectPath);
  xStr := Format('CMD /C "%s" -E%s %s >>%s', [lDrcc, lOutputPath, mProjectName, xLogFileName]);

  GetStartupInfo(xStartupInfo);
  xStartupInfo.dwFlags := xStartupInfo.dwFlags or STARTF_USESHOWWINDOW;
  xStartupInfo.wShowWindow := SW_HIDE; //SW_MINIMIZE;
  if CreateProcess(Nil, PChar(xStr), nil, nil, True, NORMAL_PRIORITY_CLASS, nil, nil, xStartupInfo, xProcessInfo) then begin
    WaitForSingleObject(xProcessInfo.hProcess, INFINITE);

    // Projektlogfile �ffnen
    xLogFile := TStringList.Create;
    xLogFile.LoadFromFile(xLogFileName);
    try
      mCS.Enter;
      // Hauptlogfile �ffnen...
      with TStringList.Create do
      try
        if FileExists(mMainLogFile) then
          LoadFromFile(mMainLogFile);
        //...und das Projektlog hineinkopieren
        AddStrings(xLogFile);
        SaveToFile(mMainLogFile);
      finally
        Free;
        mCS.Leave;
      end;
    finally
      xLogFile.Free;
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.ClearDirectory(aFilePath: String);
var
  xF: TSearchRec;
  xAttr: Integer;
  xPath: String;
begin
  xAttr := faAnyFile and (not faDirectory);
  if FindFirst(aFilePath, xAttr, xF) = 0 then begin
    xPath := ExtractFilePath(aFilePath);
    repeat
      DeleteFile(xPath + xF.Name);
    until FindNext(xF) <> 0;
    FindClose(xF);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfrmMain.mmButton1Click(Sender: TObject);
begin
  OpenLogFile;
end;
//:-----------------------------------------------------------------------------

end.

