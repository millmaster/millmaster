(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMUGlobal.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt Definitionen von MIllMaster Spulerei
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date      Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 20.10.98  1.00  Mg | Datei erstellt
| 29.09.99  1.01  Nue| Types in conjunction with YMSettings added
| 16.12.99  1.02  Mg | YarnUnits added
| 28.12.99  1.03  Mg | cYarnCntMaskStr inseted
| 20.01.00  1.04  Nue| Filled in several constants in conjunction with DB Accesss
| 25.01.00  1.05  Wss| cMMProdGroupColors array added for Production Group Colors
| 26.01.00  1.06  Nue| temporary Stringconstant till MaschGrp is implemented cZERange
| 03.05.00  1.07  Nue| Cleaned up with constants in conjunction Default-,Dummy- and
|                    | System-(Style,Order,Color..)
| 21.06.00  1.08  Nue| cDefaultLengthMode value changed from 6=First to 7=Last
| 27.06.00  1.09  SDo| New const array added. cLenBaseTxt (TLenBaseRec)
| 27.06.00  1.10  Nue| New const cDBPhysicalName = 'MM_Winding' added.
| 27.07.00  1.11  Mg | TDBDataRec : Cluster events from integer to single
|=========================================================================================*)

//**************************************************3
// Last change: 27.07.2000/Mg
//**************************************************3

unit MMUGlobal;
{$MINENUMSIZE 2}

interface

uses YMParaDef, mmColorButton {for TColorArray type}, Graphics, LoepfeGlobal;

const

  cMaxSpindeln = 80;
  cMaxPartie = 6;
  cMaxClassFields = 128;
  cMaxSiroFields  = 64;
  cMinSpuledLen = 2000; // minimal length of meters to say : the spindle has produced
  cMaxNumberOfIntervals = 200;
  cMaxSlip = 1.5;
  cMinSlip = 0.5;
  cDBPhysicalName = 'MM_Winding';

// Constants for colon factors on databbase
  cSlipFactor    = 1000;
  cImpFactFactor = 10;
  cYarnCntFactor = 10;
  cBobbinsFactor = 100;
  cConesFactor   = 100;

  cCheckLenFactor = 10;

// class fields
  cClassCutFields = 128;
  cClassUncutFields = cClassCutFields;
  cSpCutFields = 128;
  cSpUncutFields = cSpCutFields;
  cSIROCutFields = 64;
  cSIROUncutFields = cSIROCutFields;

// Default IDs for WindingMaster in conjunction with DB
  cDefaultOrderPositionID = 1;
//  cSystemOrderPositionID  = 2;
//  cSystemOrderPositionName= 'SystemOrderPosition';
  cDefaultOrderPositionName= 'DefaultOrderPosition';
//  cDummyStyleID           = 1;
  cDefaultOrderID           = 1;
//  cDummyStyleIDColor      = 8454143;  //RGB brightyellow
  cDefaultStyleID           = 1;
  cDefaultStyleIDColor      = 8454143;  //RGB brightyellow
//  cSystemStyleID          = 2;
//  cSystemStyleIDColor     = clWhite; //RGB white
  cVirtualStyleColor        = clWhite; //RGB white
//  cSystemStyleName        = 'DefaultStyle';
//  cSystemStartName        = 'DefaultStart';
  cDefaultStyleName        = 'DefaultStyle';
  cDefaultStartName        = 'DefaultStart';
  cVirtualStyleName       = 'NotInProduction';
  cDummyYMSetID           = 1;  // Used to call mYMParaDBAccess.Get when new ProdGrp will be started from ZE (in SetSettings)
                                //  This value has to correspond with the c_YM_set_id in cQryInsDummyProdGrp!
  cDefaultM_to_prod       = 10000000.0;
  cDefaultYarn_cnt        = 600;  //is 60
  c_DefaultNr_of_threads  = 1;
  cDefaultSlip            = 1000; // is 1.000
  cDefaultSpeed           = 1200;
  cDefaultSpeedRamp       = 4; //4s
  cDefaultLengthMode      = 7; //Last
  cDefaultLengthWindow    = 100; //100km

  cZERange                = 'ZERange';   //temporary Stringconstant till MaschGrp is implemented 26.01.00 Nue

  // Yarn Units
   cMaxArtPers = 20;
  // Garnnummer Umrechnungsfaktoren von .. nach..
  // z.B: Nm=1.69336*Ne
  cNmNe:  Double  = 1.69336;
  cNmTex = 1000.0;
  //cTexNm = 1000.0;  cTexNe = 0.00169336;
  cNmNeL: Double  = 0.604772;
  cNmNeK: Double  = 1.12891;
  cNmNeW: Double  = 0.516072;
  cNmTd:  Double  = 9000.0;
  cNmTs:  Double  = 29.0291;
  cNmNcC: Double  = 1.7671;
  cNeNm:  Double  = 0.590541;
  cNeTex: Double  = 590.541;
  cNeNeL: Double  = 0.357143;
  cNeNeK: Double  = 0.666667;
  cNeNeW: Double  = 0.304762;
  cNeTd:  Double  = 5314.87;
  cNeTs:  Double  = 17.1429;
  cNeNcC: Double  = 1.04347;

  cKmToMile: Double = 0.62136995;
  cMToYd:    Double = 3937.0/3600.0; // 1.30 ca.1.094
  cYdToM:    Double = 3600.0/3937.0;
  cKgToLb:   Double = 2.20462262185;

 {
  // Garnnummereinheiten
  cGarnNr1 = 'Nm ';   // Nummer Metrisch
  cGarnNr2 = 'NeC';   // English Cotton Count (Deutsch:NeB)
  cGarnNr3 = 'tex';   // Fineness Tex-System
  cGarnNr4 = 'Td ';   // Internat. denier count
  cGarnNr5 = 'NeL';   // English linen count
  cGarnNr6 = 'NeW';   // English worsted count (Deutsch:NeK)
  cGarnNr7 = 'Ny ';   // English woolen count yorkshire (Deutsch:NeW)
  cGarnNr8 = 'Ts ';   // Scottish count
  cGarnNr9 = 'NcC';   // Cotton Catalonian
  cGarnNrAnz = 9;
  }
  // Laengeneinheiten
  cLength1 = ' m ';
  cLength2 = 'km ';
  cLength3 = 'yd ';   // 1.31 "->yd
  cLengthAnz = 3;

  // Gewichteinheiten
  cWeight1 = ' g ';
  cWeight2 = 'kg ';
  cWeight3 = ' t ';
  cWeight4 = 'lb ';
  cWeightAnz = 4;

  // Bestelleinheiten
  cOrder1 = 'kg      ';
  cOrder2 = 'Cones   ';
  cOrder3 = ' t      ';
  cOrder4 = 'lb      ';
  cOrderAnz = 4;

type
// The following types are used to define a data watch level, len base ec.
// ATTENTION!! The following types are also used in the ZE environment DON'T CHANGE ANYTHING
//  without discussed with Kr (Nue/Kr: 2.5.2000)
  TDataLevel    = ( dlSpindle, dlProdGrp, dlMachine, dlOrderPosition, dlStyle, dlAssortment );
  TClassDataTyp = ( cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut );
  TClassDataSet = set of TClassDataTyp;
  TClassViewTyp = ( cvOnlyCut, cvOnlyUncut, cvCutAndUncut, cvCutAndUncutAdded );
//  TLenBase      = ( lbAbsolute, lb100KM, lb1000KM, lb100000Y, lb1000000Y );

  TLenBaseRec = record
     id    : TLenBase;
     Text  : String;
  end;

  TLengthModeRec = record
     id    : integer;
     Text  : String;
  end;


const

  cLenBaseTxt : Array[0..integer(High(TLenBase)) ] of TLenBaseRec = (
    ( id   : lbAbsolute ;
      Text : '(12)Absolut'),  //ivlm
    ( id   : lb100KM ;
      Text : '100 km'),       //ivlm
    ( id   : lb1000KM ;
      Text : '1000 km'),      //ivlm   // Default 3
    ( id   : lb100000Y ;
      Text : '100.000 yrd'),  //ivlm
    ( id   : lb1000000Y ;
      Text : '1.000.000 yrd') //ivlm
  );

  cLengthMode : Array[0..2 ] of TLengthModeRec = (
    ( id   : 6 ;
      Text : '(8)Erste'),  //ivlm
    ( id   : 7 ;
      Text : '(8)Letzte'), //ivlm
    ( id   : 8 ;
      Text : '(8)Kone')    //ivlm
  );



type
// Yartn Unts
  TYarnUnits     = (yuNone, yuNm, yuNeB, yutex, yuTd, yuNeL, yuNeK, yuNeW, yuTs, yuNcC); // German
  TYarnUnitSet   = SET OF TYarnUnits;
  TYarnUnitStr   = String[4];
  TWeightUnits   = (wuNone, wug, wukg, wut, wulb);
  TWeightUnitStr = String[4];
  TLengthUnits   = (luNone, lum, lukm, luyd);
  TLengthUnitStr = String[4];

const
  // Yarn Unit
  cYarnUnitsByLength: set of TYarnUnits = [yutex, yuTd, yuTs];
  cYarnUnitsByWeight: set of TYarnUnits = [yuNm, yuNeB, yuNeL, yuNeK, yuNeW, yuNcC];
  cYarnUnitsStr : array [yuNone..yuNcC] of TYarnUnitStr =  ('None','Nm'     ,'NeC'    ,'tex'    ,'Td'     ,'NeL'    ,'NeW'    ,'Ny'     ,'Ts'     ,'NcN' ); // International
  cYarnCntMaskStr : array [yuNone..yuNcC] of ShortString = (''    ,'000;1; ','000;1; ','000;1; ','000;1; ','000;1; ','000;1; ','000;1; ','000;1; ','000;1; ' );

  // Class Fields
  cNumOfClassFields : array [TClassDataTyp] of integer =
  ( cClassUncutFields, cClassCutFields, cSpUncutFields, cSpCutFields, cSIROUncutFields, cSIROCutFields );
//..............................................................................

type
// The following records are used to define the DBData Record
// The types are the same or larger as the types on database -> type check by writing on DB
//..............................................................................
// Imperfections
  PImpRec = ^TImpRec;
  TImpRec = record
    Neps   : longword;
    Thick  : longword;
    Thin   : longword;
    Small  : longword; // max. 34 events per meter ( 80 Spd. 12Std. )
    I2_4   : longword;
    I4_8   : longword;
    I8_20  : longword;
    I20_70 : longword;
  end;
//..............................................................................
// Classfields
  PDBSpCutField = ^TDBSpCutField;
  TDBSpCutField   = ARRAY [1..128] OF longword;
  PDBSpUncutField = ^TDBSpUncutField;
  TDBSpUncutField = ARRAY [1..128] OF longword;
  PDBClassCutField = ^TDBClassCutField;
  TDBClassCutField    = ARRAY [1..128] OF longword;
  PDBClassUncutField = ^TDBClassUncutField;
  TDBClassUncutField  = ARRAY [1..128] OF longword;
  PDBSIROCutField = ^TDBSIROCutField;
  TDBSIROCutField     = ARRAY [1..64] OF longword;
  PDBSIROUncutField = ^TDBSIROUncutField;
  TDBSIROUncutField   = ARRAY [1..64] OF longword;
//..............................................................................
// Classfield record
  PClassFieldRec = ^TClassFieldRec;
  TClassFieldRec = record
    ClassCutField   : TDBClassCutField;
    ClassUncutField : TDBClassUncutField;
    SpCutField      : TDBSpCutField;
    SpUncutField    : TDBSpUncutField;
    SIROCutField    : TDBSIROCutField;
    SIROUncutField  : TDBSIROUncutField;
  end;
//..............................................................................
  PDBDataRec = ^TDBDataRec;
  TDBDataRec = record
     Len        : longword;
     Wei        : longword;
     Bob        : longint;
     Cones      : longint;
     Sp         : longint;
     RSp        : longint;
     YB         : longint;
     CSys       : longint;
     LckSys     : longint;
     CUpY       : longint;
     CYTot      : longint;
     LSt        : longint;
     tLStProd   : longint;
     tLStBreak  : longint;
     tLStMat    : longint;
     tLStRev    : longint;
     tLStClean  : longint;
     tLStDef    : longint;
     Red        : longint;
     tRed       : longint;
     tYell      : longint;
     ManSt      : longint;
     tManSt     : longint;
     tRun       : longint;
     tOp        : longint;
     tWa        : longint;
     totProdGrp : longint;
     twtProdGrp : longint;
     CS         : longint;
     CL         : longint;
     CT         : longint;
     CN         : longint;
     CSp        : longint;
     CCl        : longint;
     COffCnt    : longint;
     LckOffCnt  : longint;
     CBu        : longint;
     CDBu       : longint;
     UClS       : Single;
     UClL       : Single;
     UClT       : Single;
     //UClS       : integer;
     //UClL       : integer;
     //UClT       : integer;
     CSIRO      : longint;
     LckSIRO    : longint;
     CSIROCl    : longint;
     LckSIROCl  : longint;
     LckCl      : longint;
     CSFI       : longint;
     LckSFI     : longint;
     SFICnt     : longint;
     SFI        : longint;
     ClassFieldRec : TClassFieldRec;
     Imp : TImpRec;
  end;
// end of DBData Record

//..............................................................................
const
  cOneGrpDataHeaderSize = 4;
type
// record to send the spindle data to the application
  POneGrpDataRec = ^TOneGrpDataRec;
  TOneGrpDataRec = record
    SpindleFirst : Word;
    SpindleLast  : Word;
    BadData      : boolean;
    Data         : array [0..0] of TDBDataRec;
  end;


const
  cMMProdGroupColors: TColorArray = (($00FF0000, $00FF8080, $00F0CAA6, $00FFD3A8),
                                     ($008000FF, $002734D8, $001500AA, $00404080),
                                     ($006ABB44, $00ACB85A, $00C08000, $00808000),
                                     ($00008080, $0030CFCF, $00B7CAC1, $00C0DCC0),
                                     ($003A8AC5, $00A4A0A0, $00A4A0A0, $00A4A0A0));


// Functions for Yarn Units
function MeterToWeightUnit( aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnits; aYarnCnt: Single; aLen: Single ): Single;
function WeightUnitToMeter( aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnits; aYarnCnt: Single; aWeigth: Single ): Single;
function LengthToMeter(aLengthUnit: TLengthUnits; aLength: Single): Single;
function MeterToLength(aLengthUnit: TLengthUnits; aLength: Single): Single;

implementation
uses sysutils,typinfo;
// Unit Check Routinen ----------------------------------------------------
function  GarnNrConvert(aInTyp, aOutTyp : TYarnUnits; aValIn : Single): Single;
const
  cExceptionStr = 'YarnUnit out of range. OutUnit = %s.';
begin
  try
    case aInTyp of
      yuNm  : case aOutTyp of
              yuNeB : Result := aValIn/cNmNe;
              yutex : Result := cNmTex/aValIn;
              yuTd  : Result := cNmTd/aValIn;
              yuNeL : Result := aValIn/cNmNeL;
              yuNeK : Result := aValIn/cNmNeK;
              yuNeW : Result := aValIn/cNmNeW;
              yuTs  : Result := cNmTs/aValIn;
              yuNcC : Result := aValIn/cNmNcC;
              yuNm  : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuNeB : case aOutTyp of
              yuNm : Result := aValIn*cNmNe;
              yuNeB : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yutex : case aOutTyp of
              yuNm : Result := cNmTex/aValIn;
              yutex  : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuTd :  case aOutTyp of
              yuNm : Result := cNmTd/aValIn;
              yuTd : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuNeL : case aOutTyp of
              yuNm : Result := aValIn*cNmNeL;
              yuNeL  : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuNeK : case aOutTyp of
              yuNm : Result := aValIn*cNmNeK;
              yuNeK : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuNeW : case aOutTyp of
              yuNm : Result := aValIn*cNmNeW;
              yuNeW : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuTs :  case aOutTyp of
              yuNm : Result := cNmTs/aValIn;
              yuTs : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
      yuNcC : case aOutTyp of
              yuNm : Result := aValIn*cNmNcC;
              yuNcC : Result := aValIn;
            else
              raise Exception.CreateFmt ( cExceptionStr, [GetEnumName(TypeInfo(TYarnUnits), Ord(aOutTyp))])
            end;
    else
      raise Exception.CreateFmt ( 'YarnUnit out of range. InUnit = %s.', [GetEnumName(TypeInfo(TYarnUnits), Ord(aInTyp))])
    end;
  except
    on e:Exception do begin
      raise Exception.Create ( 'GarnNrConvert failed. ' + e.Message );
    end;
  end;
end;
//-------------------------------------------------------------------------
function MeterToWeightUnit( aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnits; aYarnCnt: Single; aLen: Single ): Single;
begin
  case aWeightUnit of
    wug:
      Result := aLen/GarnNrConvert( aYarnUnit, yuNm, aYarnCnt);
    wukg:
      Result := aLen/GarnNrConvert( aYarnUnit, yuNm, aYarnCnt)/1000.0;
  else
    Result := aLen/GarnNrConvert( aYarnUnit, yuNm, aYarnCnt)*cKgToLb/1000.0;
  end;
end;
//-------------------------------------------------------------------------
function WeightUnitToMeter( aWeightUnit: TWeightUnits; aYarnUnit: TYarnUnits; aYarnCnt: Single; aWeigth: Single ): Single;
begin
  case aWeightUnit of
    wug:
      Result := aWeigth*GarnNrConvert(aYarnUnit, yuNm, aYarnCnt);
  else
    Result := aWeigth*GarnNrConvert(aYarnUnit, yuNm, aYarnCnt)/cKgToLb*1000.0;
  end;
end;
//-------------------------------------------------------------------------
function LengthToMeter(aLengthUnit: TLengthUnits; aLength: Single): Single;
begin
  case aLengthUnit of
    lukm : Result := aLength*1000.0;
    luyd : Result := aLength/cMToYd;
  else
    Result := aLength;
  end;
end;
//-------------------------------------------------------------------------
function MeterToLength(aLengthUnit: TLengthUnits; aLength: Single): Single;
begin
  case aLengthUnit of
    lukm : Result := aLength/1000.0;
    luyd : Result := aLength*cMToYd;
  else
    Result := aLength;
  end;
end;
//-------------------------------------------------------------------------
end.
