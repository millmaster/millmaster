object frmMain: TfrmMain
  Left = 361
  Top = 104
  Width = 853
  Height = 653
  Caption = 'BuildTool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmPageControl1: TmmPageControl
    Left = 0
    Top = 0
    Width = 845
    Height = 604
    ActivePage = tsCompile
    Align = alClient
    OwnerDraw = True
    TabOrder = 0
    OnDrawTab = mmPageControl1DrawTab
    object tsCompile: TTabSheet
      Caption = 'Generieren'
      object mmSplitter1: TmmSplitter
        Left = 0
        Top = 398
        Width = 837
        Height = 3
        Cursor = crVSplit
        Align = alBottom
        Beveled = True
        ResizeStyle = rsUpdate
      end
      object lvProjects: TmmListView
        Left = 0
        Top = 0
        Width = 628
        Height = 350
        Align = alClient
        Columns = <
          item
            Caption = 'Project'
            Width = 200
          end
          item
            Caption = 'Version'
            Width = 60
          end
          item
            Caption = 'X'
            Width = 20
          end
          item
            AutoSize = True
            Caption = 'Path'
          end
          item
            Caption = 'Comment'
          end>
        HideSelection = False
        MultiSelect = True
        ReadOnly = True
        RowSelect = True
        ParentShowHint = False
        ShowHint = True
        SortType = stText
        TabOrder = 0
        ViewStyle = vsReport
        Visible = True
        OnChange = lvProjectsChange
        AutoLabel.LabelPosition = lpLeft
      end
      object pnSpinEdit: TmmPanel
        Left = 628
        Top = 0
        Width = 209
        Height = 350
        Align = alRight
        BevelOuter = bvLowered
        TabOrder = 1
        object laVProduct: TmmLabel
          Left = 8
          Top = 9
          Width = 37
          Height = 13
          Caption = 'Product'
          FocusControl = seVProduct
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laVDatabase: TmmLabel
          Left = 72
          Top = 9
          Width = 46
          Height = 13
          Caption = 'Database'
          FocusControl = seVDatabase
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laVApplication: TmmLabel
          Left = 136
          Top = 9
          Width = 52
          Height = 13
          Caption = 'Application'
          FocusControl = seVApplication
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laComments: TmmLabel
          Left = 8
          Top = 57
          Width = 52
          Height = 13
          Caption = 'Comments:'
          FocusControl = meComments
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object seVProduct: TmmSpinEdit
          Left = 8
          Top = 24
          Width = 49
          Height = 22
          MaxValue = 99
          MinValue = 1
          TabOrder = 0
          Value = 1
          Visible = True
          AutoLabel.Control = laVProduct
          AutoLabel.LabelPosition = lpTop
        end
        object seVDatabase: TmmSpinEdit
          Tag = 1
          Left = 72
          Top = 24
          Width = 49
          Height = 22
          MaxValue = 99
          MinValue = 0
          TabOrder = 1
          Value = 0
          Visible = True
          AutoLabel.Control = laVDatabase
          AutoLabel.LabelPosition = lpTop
        end
        object seVApplication: TmmSpinEdit
          Tag = 2
          Left = 136
          Top = 24
          Width = 49
          Height = 22
          MaxValue = 999
          MinValue = 0
          TabOrder = 2
          Value = 0
          Visible = True
          AutoLabel.Control = laVApplication
          AutoLabel.LabelPosition = lpTop
        end
        object bBuildRessource: TmmButton
          Left = 112
          Top = 208
          Width = 89
          Height = 25
          Action = acBuildRessource
          TabOrder = 5
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bSetVersionInfo: TmmButton
          Left = 8
          Top = 208
          Width = 97
          Height = 25
          Action = acSetVersionInfo
          Caption = 'Save Version Info'
          TabOrder = 4
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object meComments: TmmMemo
          Left = 8
          Top = 72
          Width = 193
          Height = 129
          ScrollBars = ssVertical
          TabOrder = 3
          Visible = True
          AutoLabel.Control = laComments
          AutoLabel.LabelPosition = lpTop
        end
      end
      object mmPanel1: TmmPanel
        Left = 0
        Top = 350
        Width = 837
        Height = 48
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 2
        object mmLabel2: TmmLabel
          Left = 440
          Top = 7
          Width = 76
          Height = 13
          Caption = 'Cont. start delay'
          FocusControl = edContStartDelay
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bAdd: TmmButton
          Left = 8
          Top = 12
          Width = 50
          Height = 25
          Action = acAdd
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bDelete: TmmButton
          Left = 72
          Top = 12
          Width = 50
          Height = 25
          Action = acDelete
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bBuildAll: TmmButton
          Left = 168
          Top = 12
          Width = 81
          Height = 25
          Action = acBuildAll
          TabOrder = 2
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bBuildSelected: TmmButton
          Left = 256
          Top = 12
          Width = 81
          Height = 25
          Action = acBuildSelected
          TabOrder = 3
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbThreaded: TmmCheckBox
          Left = 360
          Top = 16
          Width = 81
          Height = 17
          Caption = 'Threaded'
          Checked = True
          State = cbChecked
          TabOrder = 4
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edContStartDelay: TmmEdit
          Left = 440
          Top = 22
          Width = 49
          Height = 21
          Color = clWindow
          TabOrder = 5
          Text = '5'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = mmLabel2
          AutoLabel.LabelPosition = lpTop
          Decimals = 0
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
      end
      object meHostory: TmmMemo
        Left = 0
        Top = 401
        Width = 837
        Height = 175
        Align = alBottom
        ScrollBars = ssVertical
        TabOrder = 3
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object tsOptions: TTabSheet
      Caption = 'Optionen'
      ImageIndex = 1
      object laDrcc: TmmLabel
        Left = 8
        Top = 33
        Width = 116
        Height = 13
        Caption = 'Delphi compiler (Dcc32):'
        FocusControl = edDrcc
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laOutputPath: TmmLabel
        Left = 8
        Top = 113
        Width = 59
        Height = 13
        Caption = 'Output path:'
        FocusControl = edOutputPath
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laBrcc32: TmmLabel
        Left = 8
        Top = 73
        Width = 134
        Height = 13
        Caption = 'Resource compiler (Brcc32):'
        FocusControl = edBrcc32
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bDcc32Exe: TmmSpeedButton
        Left = 416
        Top = 48
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bSelectExeClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bBrcc32Exe: TmmSpeedButton
        Tag = 1
        Left = 416
        Top = 88
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bSelectExeClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bOutputPath: TmmSpeedButton
        Tag = 2
        Left = 416
        Top = 128
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bOutputPathClick
        AutoLabel.LabelPosition = lpLeft
      end
      object laConfig: TmmLabel
        Left = 8
        Top = 183
        Width = 139
        Height = 13
        Caption = 'Compiler options (Dcc32.cfg):'
        FocusControl = meConfig
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laSearchPath: TmmLabel
        Left = 360
        Top = 183
        Width = 125
        Height = 13
        Caption = 'Search paths (Dcc32.cfg):'
        FocusControl = meSearchPath
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laYMSettings: TmmLabel
        Left = 8
        Top = 433
        Width = 57
        Height = 13
        Caption = 'YMSettings:'
        FocusControl = edYMSettings
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bYMSettings: TmmSpeedButton
        Left = 376
        Top = 448
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bPascalFileSelectClick
        AutoLabel.Control = laYMSettingsDate
        AutoLabel.Distance = 10
        AutoLabel.LabelPosition = lpRight
      end
      object laBaseGlobal: TmmLabel
        Left = 8
        Top = 469
        Width = 57
        Height = 13
        Caption = 'BaseGlobal:'
        FocusControl = edBaseGlobal
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bBaseGlobal: TmmSpeedButton
        Tag = 1
        Left = 376
        Top = 484
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bPascalFileSelectClick
        AutoLabel.Control = laBaseGlobalDate
        AutoLabel.Distance = 10
        AutoLabel.LabelPosition = lpRight
      end
      object laMMUGlobal: TmmLabel
        Left = 8
        Top = 509
        Width = 59
        Height = 13
        Caption = 'MMUGloabl:'
        FocusControl = edMMUGlobal
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bMMUGlobal: TmmSpeedButton
        Tag = 2
        Left = 376
        Top = 524
        Width = 21
        Height = 21
        Caption = '...'
        Visible = True
        OnClick = bPascalFileSelectClick
        AutoLabel.Control = laMMUGlobalDate
        AutoLabel.Distance = 10
        AutoLabel.LabelPosition = lpRight
      end
      object laYMSettingsDate: TmmLabel
        Left = 407
        Top = 452
        Width = 54
        Height = 13
        Caption = '00.00.0000'
        FocusControl = bYMSettings
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laBaseGlobalDate: TmmLabel
        Left = 407
        Top = 488
        Width = 54
        Height = 13
        Caption = '00.00.0000'
        FocusControl = bBaseGlobal
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laMMUGlobalDate: TmmLabel
        Left = 407
        Top = 528
        Width = 54
        Height = 13
        Caption = '00.00.0000'
        FocusControl = bMMUGlobal
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLineLabel1: TmmLineLabel
        Left = 8
        Top = 160
        Width = 819
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Delphi Compiler Options'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Distance = 2
      end
      object mmLineLabel2: TmmLineLabel
        Left = 8
        Top = 408
        Width = 819
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Additional File Informations'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Distance = 2
      end
      object mmLineLabel3: TmmLineLabel
        Left = 8
        Top = 8
        Width = 819
        Height = 17
        Anchors = [akLeft, akTop, akRight]
        AutoSize = False
        Caption = 'Delphi Working Tools'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        Distance = 2
      end
      object edDrcc: TmmEdit
        Left = 8
        Top = 48
        Width = 409
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        AutoLabel.Control = laDrcc
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object edOutputPath: TmmEdit
        Left = 8
        Top = 128
        Width = 409
        Height = 21
        Color = clWindow
        TabOrder = 2
        Visible = True
        AutoLabel.Control = laOutputPath
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object edBrcc32: TmmEdit
        Left = 8
        Top = 88
        Width = 409
        Height = 21
        Color = clWindow
        TabOrder = 1
        Visible = True
        AutoLabel.Control = laBrcc32
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object meConfig: TmmMemo
        Left = 8
        Top = 198
        Width = 337
        Height = 193
        ScrollBars = ssBoth
        TabOrder = 3
        Visible = True
        WordWrap = False
        AutoLabel.Control = laConfig
        AutoLabel.LabelPosition = lpTop
      end
      object meSearchPath: TmmMemo
        Left = 360
        Top = 198
        Width = 305
        Height = 193
        TabOrder = 4
        Visible = True
        AutoLabel.Control = laSearchPath
        AutoLabel.LabelPosition = lpTop
      end
      object edYMSettings: TmmEdit
        Left = 8
        Top = 448
        Width = 369
        Height = 21
        Color = clWindow
        TabOrder = 5
        Visible = True
        OnChange = edPasFileChange
        AutoLabel.Control = laYMSettings
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object edBaseGlobal: TmmEdit
        Tag = 1
        Left = 8
        Top = 484
        Width = 369
        Height = 21
        Color = clWindow
        TabOrder = 6
        Visible = True
        OnChange = edPasFileChange
        AutoLabel.Control = laBaseGlobal
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
      object edMMUGlobal: TmmEdit
        Tag = 2
        Left = 8
        Top = 524
        Width = 369
        Height = 21
        Color = clWindow
        TabOrder = 7
        Visible = True
        OnChange = edPasFileChange
        AutoLabel.Control = laMMUGlobal
        AutoLabel.LabelPosition = lpTop
        Decimals = -1
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        ValidateMode = [vmInput]
      end
    end
    object tsBuildLog: TTabSheet
      Caption = 'BuildLog'
      ImageIndex = 2
      object meBuildLog: TmmMemo
        Left = 0
        Top = 0
        Width = 837
        Height = 535
        Align = alClient
        ScrollBars = ssBoth
        TabOrder = 0
        Visible = True
        WordWrap = False
        AutoLabel.LabelPosition = lpLeft
      end
      object pnBuildLog: TmmPanel
        Left = 0
        Top = 535
        Width = 837
        Height = 41
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object bFindError: TmmButton
          Left = 16
          Top = 8
          Width = 75
          Height = 25
          Caption = 'Find Error'
          Enabled = False
          TabOrder = 0
          Visible = True
          OnClick = bFindErrorClick
          AutoLabel.LabelPosition = lpLeft
        end
        object mmButton1: TmmButton
          Left = 120
          Top = 8
          Width = 97
          Height = 25
          Caption = 'Reopen log file'
          TabOrder = 1
          Visible = True
          OnClick = mmButton1Click
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  object sbMain: TmmStatusBar
    Left = 0
    Top = 604
    Width = 845
    Height = 22
    Panels = <>
    SimplePanel = True
  end
  object mmOpenDialog: TmmOpenDialog
    DefaultExt = '*.dpr'
    Filter = 'All Files (*.*)|*.*'
    Options = [ofReadOnly, ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Left = 268
    Top = 8
  end
  object mmActionList: TmmActionList
    OnUpdate = mmActionListUpdate
    Left = 220
    Top = 8
    object acAdd: TAction
      Caption = 'Add'
      OnExecute = acAddExecute
    end
    object acBuildAll: TAction
      Caption = 'Build All'
      OnExecute = acBuildAllExecute
    end
    object acBuildSelected: TAction
      Caption = 'Build Selected'
      OnExecute = acBuildSelectedExecute
    end
    object acDelete: TAction
      Caption = 'Delete'
      OnExecute = acDeleteExecute
    end
    object acBuildRessource: TAction
      Caption = 'Build Ressource'
      OnExecute = acBuildRessourceExecute
    end
    object acOpenPas: TAction
      Caption = '...'
    end
    object acSetVersionInfo: TAction
      Caption = 'Set Version Info'
      OnExecute = acSetVersionInfoExecute
    end
    object acSelectAll: TAction
      ShortCut = 16449
      OnExecute = acSelectAllExecute
    end
  end
  object FolderBrowser: TFolderBrowser
    BrowseFlags = [bfDirsOnly]
    Title = 'Select folder'
    Left = 320
    Top = 8
  end
end
