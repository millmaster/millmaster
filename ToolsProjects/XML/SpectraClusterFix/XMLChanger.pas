{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: XMLChanger.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 27.06.2006  1.00  Wss | File created
|==========================================================================================}
unit XMLChanger;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Dialogs, ADODBAccess, MMEventLog,
  XMLGlobal, BaseGlobal, MSXML2_TLB, LoepfeGlobal;

type
  TXMLChanger = class(TObject)
  private
    mDB: TADODBAccess;
    mEventLog: TEventLogWriter;
    mYMSetIDs: string;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function Process: Boolean;
  published
  end;

implementation

uses
  mmCS, FileCtrl, JclStrings, MMUGlobal, XMLDef;

//------------------------------------------------------------------------------
constructor TXMLChanger.Create;
begin
  mYMSetIDs          := '';
  mEventLog          := TEventLogWriter.Create('MillMaster', '', ssApplication, 'SpectraClusterFix: ', True);
  mDB                := TAdoDBAccess.Create(3, True); // 2 Query-handles
end; // TYMSettingsTransformer.Create cat:No category

//------------------------------------------------------------------------------
destructor TXMLChanger.Destroy;
begin
  mDB.Free;
  mEventLog.Free;

  inherited Destroy;
end; // TYMSettingsTransformer.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GenerateHashcode
 *  Klasse:           TXMLSettingsAccess
 *  Kategorie:        No category
 *  Argumente:        (aXMLData, aHash1, aHash2)
 *
 *  Kurzbeschreibung: Generiert den Hashcode und gibt den Normalisierten XMLStream wieder zur�ck
 *  Beschreibung:
                      -
 --------------------------------------------------------------------*)

//------------------------------------------------------------------------------
function TXMLChanger.Process: Boolean;
const
  cSelectSettings = 'select c_ym_set_id, c_xml_setting from t_xml_ym_settings ';
  cUpdateSettings =
    'update t_xml_ym_settings set c_hashcode1 =:c_hashcode1, c_hashcode2 =:c_hashcode2, c_xml_setting =:c_xml_setting ' +
    'where c_ym_set_id = :c_ym_set_id';
var
  xXMLDom: DOMDocumentMM;
  xXMLStr: string;
  xID: Integer;
  xStr: string;
  xXMLNode: IXMLDOMNode;
  xClusterValue: Double;
  xCount: Integer;
  //............................................................
  function GenerateHashcode(aXMLDom: DOMDocumentMM; out aXMLData: String; out aHash1: integer; out aHash2: integer): Boolean;
  begin
    Result   := False;
    aXMLData := '';
    aHash1   := 0;
    aHash2   := 0;
    if assigned(aXMLDom) then begin
      aXMLData := aXMLDom.xml;
      aXMLData := StringReplace(Trim(aXMLData), #13#10, '', [rfReplaceAll]);
      aXMLData := StringReplace(aXMLData, #10#13, '', [rfReplaceAll]);
      aXMLData := StringReplace(aXMLData, #09, '', [rfReplaceAll]);

      aXMLDom.setProperty('NewParser', true);
      // Stream in das Dokument laden und parsen
      if aXMLDom.loadXML(aXMLData) and Assigned(aXMLDom.DocumentElement) then begin
        // F�r die Berechnung des Hashcodes darf nur das DocumentElement verwendet werden,
        // da der Kommentar und die ProcessingInformations nicht zum Setting geh�ren.
        HashByMM(aXMLDom.DocumentElement, aHash1, aHash2);
        Result := True;
      end;
    end;
  end;
  //............................................................
  procedure UpdateSetting(aID: Integer; aXMLDom: DOMDocumentMM);
  var
    xHash1, xHash2: Integer;
    xXMLData: String;
  begin
    if GenerateHashcode(aXMLDom, xXMLData, xHash1, xHash2) then begin
      with mDB.Query[1] do
      try
        SQL.Text := cUpdateSettings;
        ParamByName('c_hashcode1').AsInteger  := xHash1;
        ParamByName('c_hashcode2').AsInteger  := xHash2;
        ParamByName('c_xml_setting').AsString := xXMLData;
        ParamByName('c_ym_set_id').AsInteger  := aID;
        ExecSQL;
      except
        on e: Exception do
          mEventLog.Write(etError, Format('UpdateSetting %d failed: %s', [xID, e.Message]));
      end;
    end else
      mEventLog.Write(etError, Format('UpdateSetting %d failed by GenerateHashcode', [aID]));
  end;
  //............................................................
begin
  EnterMethod('TXMLChanger.Process');
  Result     := False;

  if mDB.Init then begin
    xCount := 0;
    with mDB.Query[0] do
    try
      SQL.Text := cSelectSettings;
//      mYMSetIds := '1360,1364,1374'; //'-26,-25,-24,-23';
      if Trim(mYMSetIDs) = '' then
        mYMSetIds := GetRegString(cRegLM, cRegMMDebug, 'ConvertYMSetIDs');

      if Trim(mYMSetIDs) <> '' then
        SQL.Add(Format('where c_ym_set_id in (%s)', [mYMSetIDs]));

      SQL.Add('order by c_ym_set_id');
      Open;
      while not EOF do begin
        xStr    := '';
        xID     := FieldByName('c_ym_set_id').AsInteger;
        xXMLStr := FieldByName('c_xml_setting').AsString;
        xXMLDom := XMLStreamToDOM(xXMLStr);

        if Assigned(xXMLDom) then begin
          // Hole erst mal den Cluster Wert
          xXMLNode      := xXMLDom.selectSingleNode(cXPClusterShortLengthItem);
          xStr          := GetElementValueDef(xXMLNode, '1.0');
          xClusterValue := MMStrToFloat(xStr);
          if xClusterValue < 1.01 then begin
            // Lese den Wert aus Channel Short Length...
            xStr := GetElementValueDef(xXMLDom.selectSingleNode(cXPChannelShortLengthItem), '1.0');

            CodeSite.SendFmtMsg('%d: OldValue = %f', [xID, xClusterValue]);
            CodeSite.SendString('    NewValue', xStr);
            //...und setze diesen in den Short Cluster ein
            SetElementValue(xXMLNode, xStr);

            UpdateSetting(xID, xXMLDom);
            Inc(xCount);
          end;
        end;
        Next;
      end; // while not EOF
      CodeSite.SendInteger('Number of settings fixed', xCount);
    except
      on e: Exception do
        raise EMMException.Create(e.message + 'in SpectraClusterFix;');
    end;
  end else
    mEventLog.Write(etError, 'mDB.Init failed: ' + mDB.DBErrorTxt);
end;

end.

