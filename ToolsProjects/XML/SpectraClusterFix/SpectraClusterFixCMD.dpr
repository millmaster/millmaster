{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SpectraClusterFixCMD.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 27.06.2006  1.00  Wss | File created
|==========================================================================================}
program SpectraClusterFixCMD;
{$APPTYPE CONSOLE}
uses
  MemCheck,
  Dialogs,
  SysUtils,
  ActiveX,
  Windows,
  XMLChanger in 'XMLChanger.pas';

var
  xhresult: Longint;

{$R 'Version.res'}
begin
{$IFDEF MemCheck}
  MemChk('XMLSettingsChangerCMD');
{$ENDIF}
  try
    xhresult:= CoInitialize(Nil); //initializes the Component Object Model(COM) library
    if (xhresult <> S_OK) and (xhresult <> S_FALSE) then
      raise Exception.Create ( 'CoInitialize failed ');

    with TXMLChanger.Create do
    try
      Process;
    finally
      Free;
      CoUninitialize;
    end;
  except
    on e:Exception do
      ShowMessage('XMLChanger failed: ' + e.Message);
  end;
end.
