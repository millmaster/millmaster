unit FrameCollision;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FrameBaseFunction, StdCtrls, mmListBox, mmStringList, XMLMapping;

type
  (*
  Listet Elemente auf, welche Kollisionen in der Speicherbelegung aufweisen.
  *)
  TfraCollision = class(TfraBaseFunction)
    lbResult: TmmListBox;
    procedure lbResultDblClick(Sender: TObject);
  private
    mMemPool: TmmStringList;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; override;
    procedure Init;
  end;
  
var
  fraCollision: TfraCollision;

implementation
{$R *.DFM}
uses
  mmCS, comctrls, XMLMappingDef, math;

const
  cMemFree = 'MemFree';

//:---------------------------------------------------------------------------
//:--- Class: TfrCollision
//:---------------------------------------------------------------------------
constructor TfraCollision.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  //:...................................................................
  mMemPool := TmmStringList.Create;
end; // TfrCollision.Create

//:---------------------------------------------------------------------------
destructor TfraCollision.Destroy;
begin
  FreeAndNil(mMemPool);
  //:...................................................................
  inherited Destroy;
end; // TfrCollision.Destroy

//:---------------------------------------------------------------------------
procedure TfraCollision.Execute;
  
  //...............................................................
  function GetBitMask(aElement: TMMElement): LongWord;
  begin
    // Alle BitCount-Bits auf 1 initialisieren...
    Result := StrToIntDef(aElement.BitCount, 0);
    Result := Trunc(Power(2, Result)-1);
    //...und an richtige Position innerhalb Platzieren
    Result := Result shl StrToIntDef(aElement.BitOffset, 0);
  end;
  //...............................................................
  procedure ScanTree(aNode: TTreeNode);
  var
    i,j: Integer;
    xStr: String;
    xName: String;
    xStartByte, xEndByte: Integer;
    xElement: TMMElement;
    xStrList: TmmStringList;
    xMaskTot, xMask: Integer;
  begin
    xStrList := TmmStringList.Create;
    try
      while Assigned(aNode) do begin
        if not TBaseElement(aNode.Data).Root then begin
          xElement := TMMElement(aNode.Data);
          if xElement.Active then begin
            xStartByte := xElement.Offset;
            // ist es ein Byte/Char Array?
            if (xElement.Datatype in [dtChar, dtUCChar]) or // bei Char und UCChar immer Count verwenden
               ((xElement.Datatype = dtByte) and (xElement.Count > 0)) then begin
              xEndByte := xStartByte + xElement.Count - 1;
            end
            else if xElement.Datatype = dtUCBit then begin
              xEndByte := xStartByte + (xElement.Count div 8) - 1;
              // Angefressenes Byte geh�rt nat�rlich auch noch dazu
              if (xElement.Count mod 8) > 0 then
                inc(xEndByte);
            end
            else begin
              // normaler Datentyp mit eventuell einzelnen Bitdefinitionen
              xEndByte := xStartByte + cBasicTypeDimensions[xElement.DataType] - 1;
            end;
            // Anzahl Bytes vom Datentyp durchz�hlen
            for i:=xStartByte to xEndByte do begin
              // Hole die Belegung aus dem MemPool
              xStr := mMemPool.Values[IntToStr(i)];
              if AnsiSameText(xStr, cMemFree) then begin
                // aha, dieser Speicherplatz war nicht nicht belegt -> schreibe eigenen Namen hinein
                xStr := xElement.ElementName;
                // wenn es ein Bitfeld ist, die Bit-Infos in Text mitcodieren
                if (xElement.BitOffset <> '') and (xElement.BitCount <> '') then begin
                  xMaskTot := GetBitMask(xElement);
                  xStr     := Format('MASK=%d,%s=%d', [xMaskTot, xStr, xMaskTot]);
                end;
                mMemPool.Values[IntToStr(i)] := xStr;
              end
              else if xStr = '' then
                // Offset ist ausserhalb MemPool: in Fehlerliste hinzuf�gen ElementName is OutOfRange
                lbResult.Items.AddObject(xElement.ElementName + ' ausserhalb StructSize', aNode)
              else begin
                xStrList.CommaText := xStr;
                // Speicher ist bereits belegt.
                // Bei Bitfelder noch die Bits pr�fen, erst dann ist eine Aussage m�glich
                // Ist es ein Bitfeld-Element?
                if (xElement.BitOffset <> '') and (xElement.BitCount <> '') then begin
                  // Wenn das Kollisions-Element auch ein Bitfeld ist, dann pr�fen
                  // wenn nicht, dann einfach Kollision in ResultList hinzuf�gen
                  if xStrList.Count > 1 then begin
                    // xStr l�schen, damit keine Kollision protokolliert wird
                    xStr := '';
                    // ist aktuelles Element ein Bitfeld?
                    if (xElement.BitOffset <> '') and (xElement.BitCount <> '') then begin
                      // hole die gesamte BitMask aus dem Memorypool
                      // Index 0 ist immer mit dem ersten Element + TotMask
                      xName    := xStrList.Names[0];
                      xMaskTot := xStrList.Values[xName];
                      xMask    := GetBitMask(xElement);
                      // Maskieren und wenn > 0 dann ist diese Bit bereits belegt
                      if (xMask and xMaskTot) > 0 then begin
                        // das Kollisionselement suchen, Index 0 ist immer der TotalMask-Wert -> ignorieren
                        for j:=1 to xStrList.Count-1 do begin
                          xName    := xStrList.Names[j];
                          xMaskTot := xStrList.Values[xName];
                          if (xMask and xMaskTot) > 0 then begin
                            xStr := xName;
                            Break;
                          end;
                        end;
                      end else begin
                        // Bit ist noch frei -> mit eigenem Bit-Element belegen und...
                        xMaskTot := xMaskTot or xMask;
                        xStrList.Values[xName] := xMaskTot;
                        xStrList.Values[xElement.ElementName] := xMask;
                        //...in Memorypool erg�nzen
                        mMemPool.Values[IntToStr(i)] := xStrList.CommaText;
                      end; // if xMask
                    end;
                  end; // if xStrList.Count
                end
                // aktuelles Feld ist ein normales Feld aber das Kollisionsfeld ist ein Bitfeld
                // -> verwende einfach den 1. g�ltigen Namen aus der StringList. Index 0
                // ist die Totalmaske
                else if xStrList.Count > 1 then begin
                  xStr := xStrList.Names[1];
                end; // if BitOffset

                if xStr <> '' then begin
                  // Fehlerliste hinzuf�gen: ElementName used by xStr
                  lbResult.Items.AddObject(Format('%s doppelbelegt mit %s', [xElement.ElementName, xStr]), aNode);
                  Break;
                end;
              end; // if AnsiSameText
            end; // for i
          end; // if Active
        end; // if not Root

        if aNode.HasChildren then
          ScanTree(aNode.GetFirstChild);
  
        aNode := aNode.GetNextSibling;
      end; // while Assigned
    finally
      xStrList.Free;
    end;
  end;
  //...............................................................
  
begin
  if Assigned(TreeView) then begin
    if TreeView.Items.Count > 0 then begin
      Init;
      ScanTree(TreeView.Items.GetFirstNode);
    end;
  end;
end; // TfrCollision.Execute

//:---------------------------------------------------------------------------
procedure TfraCollision.Init;
var
  i: Integer;
begin
  mMemPool.Clear;
  for i:=0 to LoepfeBody.StructSize-1 do begin
    mMemPool.Values[IntToStr(i)] := cMemFree;
  end;
end; // TfrCollision.Init

//:---------------------------------------------------------------------------
procedure TfraCollision.lbResultDblClick(Sender: TObject);
var
  xNode: TTreeNode;
begin
  with lbResult do begin
    xNode := TTreeNode(Items.Objects[ItemIndex]);
    if Assigned(xNode) then begin
      xNode.MakeVisible;
      xNode.Selected := True;
    end;
  end;
end; // TfrCollision.lbResultDblClick


end.



