object frmMain: TfrmMain
  Left = 1371
  Top = 109
  Width = 1044
  Height = 774
  Caption = 'LOEPFE Schematool'
  Color = clBtnFace
  Constraints.MinHeight = 744
  Constraints.MinWidth = 1042
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = mMainMenu
  OldCreateOrder = False
  Position = poDefault
  ShowHint = True
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmSplitter2: TmmSplitter
    Left = 0
    Top = 201
    Width = 1036
    Height = 3
    Cursor = crVSplit
    Align = alTop
  end
  object pcMain: TmmPageControl
    Left = 0
    Top = 29
    Width = 1036
    Height = 172
    ActivePage = tsMapping
    Align = alTop
    Style = tsFlatButtons
    TabOrder = 0
    object tsMapping: TTabSheet
      Caption = 'Mapping'
      object mmLabel2: TmmLabel
        Left = 472
        Top = 1
        Width = 68
        Height = 13
        Caption = 'Strukturgr�sse'
        FocusControl = edStructSize
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmPanel5: TmmPanel
        Left = 0
        Top = 0
        Width = 553
        Height = 141
        Align = alLeft
        BevelOuter = bvNone
        Caption = 'mmPanel5'
        TabOrder = 0
        object mmSpeedButton11: TmmSpeedButton
          Left = 8
          Top = 40
          Width = 24
          Height = 22
          Action = acMapCopy
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            8888888888888888888888888844444444488888884FFFFFFF488888884F0000
            0F480000004FFFFFFF480FFFFF4F00000F480F00004FFFFFFF480FFFFF4F00F4
            44480F00004FFFF4F4880FFFFF4FFFF448880F00F044444488880FFFF0F08888
            88880FFFF0088888888800000088888888888888888888888888}
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmSpeedButton12: TmmSpeedButton
          Left = 8
          Top = 64
          Width = 24
          Height = 22
          Action = acMapDelete
          Glyph.Data = {
            42020000424D4202000000000000420000002800000010000000100000000100
            1000030000000002000000000000000000000000000000000000007C0000E003
            00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C00001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
            00001F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C00000000
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
            1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
            1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C00000000
            1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
            00001F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
            1F7C1F7C1F7C}
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmSpeedButton14: TmmSpeedButton
          Left = 8
          Top = 16
          Width = 24
          Height = 22
          Action = acMapNew
          Glyph.Data = {
            F6000000424DF600000000000000760000002800000010000000100000000100
            0400000000008000000000000000000000001000000000000000000000000000
            8000008000000080800080000000800080008080000080808000C0C0C0000000
            FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
            888888888888888888888800000000000888880FFFFFFFFF0888880FFFFFFFFF
            0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFFFFF
            0888880FFFFFFFFF0888880FFFFFFFFF0888880FFFFFF0000888880FFFFFF0F0
            8888880FFFFFF008888888000000008888888888888888888888}
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel21: TmmLabel
          Left = 288
          Top = 46
          Width = 189
          Height = 13
          Caption = 'Beschreibung (keine ":" in dieses Feld!!)'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edStructSize: TmmEdit
          Left = 472
          Top = 16
          Width = 65
          Height = 21
          Color = clWindow
          TabOrder = 0
          Text = '0'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = mmLabel2
          AutoLabel.LabelPosition = lpTop
          Decimals = -1
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
        object mmPanel6: TmmPanel
          Left = 40
          Top = 0
          Width = 241
          Height = 137
          Anchors = [akLeft, akTop, akBottom]
          BevelOuter = bvNone
          TabOrder = 1
          object mmSplitter1: TmmSplitter
            Left = 113
            Top = 0
            Width = 6
            Height = 137
            Cursor = crHSplit
          end
          object mmPanel7: TmmPanel
            Left = 0
            Top = 0
            Width = 113
            Height = 137
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object mmLabel12: TmmLabel
              Left = 0
              Top = 0
              Width = 113
              Height = 13
              Align = alTop
              Caption = 'Kategorien:'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object lbMapCategories: TmmListBox
              Left = 0
              Top = 13
              Width = 113
              Height = 124
              Align = alClient
              Anchors = [akLeft, akTop, akBottom]
              Enabled = True
              ItemHeight = 13
              TabOrder = 0
              Visible = True
              OnClick = lbMapCategoriesClick
              AutoLabel.LabelPosition = lpTop
            end
          end
          object mmPanel17: TmmPanel
            Left = 119
            Top = 0
            Width = 122
            Height = 137
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object mmLabel22: TmmLabel
              Left = 0
              Top = 0
              Width = 122
              Height = 13
              Align = alTop
              Caption = 'MapID:'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
            object lbMapID: TmmListBox
              Left = 0
              Top = 13
              Width = 122
              Height = 124
              Align = alClient
              Anchors = [akLeft, akTop, akBottom]
              DragMode = dmAutomatic
              Enabled = True
              ItemHeight = 13
              Sorted = True
              TabOrder = 0
              Visible = True
              OnClick = lbMapIDClick
              AutoLabel.LabelPosition = lpTop
            end
          end
        end
        object meDescription: TmmRichEdit
          Left = 288
          Top = 61
          Width = 257
          Height = 76
          Anchors = [akLeft, akTop, akBottom]
          Lines.Strings = (
            '')
          TabOrder = 2
        end
        object rbMapIDFilter: TmmRadioGroup
          Left = 288
          Top = 0
          Width = 169
          Height = 41
          Caption = 'Filter'
          Columns = 3
          ItemIndex = 0
          Items.Strings = (
            'alle'
            'S'
            'C')
          TabOrder = 3
          OnClick = lbMapCategoriesClick
          CaptionSpace = True
        end
      end
      object mmPanel18: TmmPanel
        Left = 553
        Top = 0
        Width = 475
        Height = 141
        Align = alClient
        BevelOuter = bvNone
        Caption = 'mmPanel18'
        TabOrder = 1
        object mmLabel3: TmmLabel
          Left = 0
          Top = 0
          Width = 475
          Height = 13
          Align = alTop
          Caption = 'History'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object meHistory: TmmRichEdit
          Left = 0
          Top = 13
          Width = 475
          Height = 128
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          WordWrap = False
        end
      end
    end
    object tsEnumeration: TTabSheet
      Caption = 'Enumeration'
      ImageIndex = 1
      object mmSpeedButton15: TmmSpeedButton
        Left = 8
        Top = 48
        Width = 24
        Height = 22
        Action = acEnumPoolNew
        Glyph.Data = {
          42020000424D4202000000000000420000002800000010000000100000000100
          1000030000000002000000000000000000000000000000000000007C0000E003
          00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C00000000000000000000000000000000000000000000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7FFF7F0000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F0000000000000000
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F0000FF7F00001F7C
          1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F000000001F7C1F7C
          1F7C1F7C1F7C1F7C1F7C000000000000000000000000000000001F7C1F7C1F7C
          1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
          1F7C1F7C1F7C}
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmSpeedButton16: TmmSpeedButton
        Left = 40
        Top = 48
        Width = 24
        Height = 22
        Action = acEnumPoolDelete
        Glyph.Data = {
          F6000000424DF600000000000000760000002800000010000000100000000100
          0400000000008000000000000000000000001000000000000000000000000000
          8000008000000080800080000000800080008080000080808000C0C0C0000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
          8888888188888888889888991888888888888899918888888988888999888888
          9888888899188889188888888991889188888888889919188888888888899188
          8888888888991918888888888991889188888889991888891888889991888888
          9188887978888888889888888888888888888888888888888888}
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel24: TmmLabel
        Left = 8
        Top = 9
        Width = 59
        Height = 13
        Caption = 'Enumeration'
        FocusControl = cobEnumPool
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object cobEnumPool: TmmComboBox
        Left = 8
        Top = 24
        Width = 145
        Height = 21
        Style = csDropDownList
        Color = clWindow
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        OnChange = cobEnumPoolChange
        AutoLabel.Control = mmLabel24
        AutoLabel.LabelPosition = lpTop
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object meEnumPoolValues: TmmMemo
        Left = 184
        Top = 8
        Width = 217
        Height = 113
        ScrollBars = ssVertical
        TabOrder = 1
        Visible = True
        WordWrap = False
        OnExit = meEnumPoolValuesExit
        OnKeyPress = meEnumPoolValuesKeyPress
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Funktionen'
      ImageIndex = 2
      object mmLabel23: TmmLabel
        Left = 8
        Top = 1
        Width = 41
        Height = 13
        Caption = 'Funktion'
        FocusControl = lbFunction
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbFunction: TmmListBox
        Left = 8
        Top = 16
        Width = 121
        Height = 105
        Enabled = True
        ItemHeight = 13
        Items.Strings = (
          'Kollision'
          'Speicherbelegung')
        TabOrder = 0
        Visible = True
        AutoLabel.Control = mmLabel23
        AutoLabel.LabelPosition = lpTop
      end
      object mmButton1: TmmButton
        Left = 136
        Top = 16
        Width = 75
        Height = 25
        Action = acFunctionShow
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object pnFunction: TmmPanel
        Left = 268
        Top = 0
        Width = 760
        Height = 141
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 2
      end
    end
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 1036
    Height = 29
    Caption = 'mmToolBar1'
    Flat = True
    Images = mImageList
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = acExit
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Action = acOpen
      DropdownMenu = pmOpenFileMRU
      Style = tbsDropDown
    end
    object ToolButton18: TToolButton
      Left = 59
      Top = 0
      Action = acReloadCurrentSchema
    end
    object ToolButton3: TToolButton
      Left = 82
      Top = 0
      Action = acSave
    end
    object ToolButton4: TToolButton
      Left = 105
      Top = 0
      Width = 16
      ImageIndex = 3
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 121
      Top = 0
      Action = acExport
    end
    object btn1: TToolButton
      Left = 144
      Top = 0
      Action = acImport
    end
    object ToolButton19: TToolButton
      Left = 167
      Top = 0
      Action = acExportYM
    end
    object ToolButton13: TToolButton
      Left = 190
      Top = 0
      Width = 8
      Caption = 'ToolButton13'
      ImageIndex = 24
      Style = tbsSeparator
    end
    object bShowLastExportetMapfile: TToolButton
      Left = 198
      Top = 0
      Action = acShowLastExportetMapfile
      Grouped = True
    end
    object ToolButton17: TToolButton
      Left = 221
      Top = 0
      Action = acShowOtherMap
    end
    object ToolButton22: TToolButton
      Left = 244
      Top = 0
      Action = acCompareMapfiles
    end
    object ToolButton12: TToolButton
      Left = 267
      Top = 0
      Width = 8
      Caption = 'ToolButton12'
      ImageIndex = 23
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 275
      Top = 0
      Action = acExportMMConst
    end
  end
  object sbBottom: TmmStatusBar
    Left = 0
    Top = 706
    Width = 1036
    Height = 22
    Panels = <
      item
        Width = 80
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object mmPanel16: TmmPanel
    Left = 0
    Top = 204
    Width = 1036
    Height = 502
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object mmPanel2: TmmPanel
      Left = 0
      Top = 0
      Width = 260
      Height = 502
      Align = alClient
      BevelOuter = bvNone
      BorderWidth = 1
      TabOrder = 0
      object tvSchema: TmmTreeView
        Left = 1
        Top = 25
        Width = 258
        Height = 262
        Align = alClient
        HideSelection = False
        Images = mImageList
        Indent = 19
        PopupMenu = pmTreeView
        ReadOnly = True
        StateImages = mImageList
        TabOrder = 0
        Visible = True
        OnChange = tvSchemaChange
        OnCustomDrawItem = tvSchemaCustomDrawItem
        OnGetImageIndex = tvSchemaGetImageIndex
        AutoLabel.LabelPosition = lpTop
      end
      object mFilterPanel: TmmPanel
        Left = 1
        Top = 328
        Width = 258
        Height = 173
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object mAdvancedFilterPanel: TmmPanel
          Left = 0
          Top = 22
          Width = 258
          Height = 151
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object mPatternWithoutList: TmmCheckListBox
            Left = 113
            Top = 0
            Width = 120
            Height = 151
            Align = alLeft
            BorderStyle = bsNone
            Color = clBtnFace
            ItemHeight = 13
            Items.Strings = (
              'Ohne Default'
              'Ohne Enum'
              'Ohne Facet'
              'Ohne Formel'
              'Ohne "Not"'
              'Kein Bitfeld'
              'Ohne Event'
              'Kein ConfigCode'
              'Ohne Flag')
            TabOrder = 0
            Visible = True
            OnClick = mPatternListClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mPatternWithList: TmmCheckListBox
            Left = 0
            Top = 0
            Width = 113
            Height = 151
            Align = alLeft
            BorderStyle = bsNone
            Color = clBtnFace
            ItemHeight = 13
            Items.Strings = (
              'Mit Default'
              'Mit Enum'
              'Mit Facet'
              'Mit Formel'
              'Mit "Not"'
              'Bitfeld'
              'Mit Event'
              'ConfigCode'
              'Flag')
            TabOrder = 1
            Visible = True
            OnClick = mPatternListClick
            AutoLabel.LabelPosition = lpLeft
          end
          object mmToolBar5: TmmToolBar
            Left = 234
            Top = 0
            Width = 24
            Height = 151
            Align = alRight
            Caption = 'mmToolBar5'
            EdgeBorders = []
            Flat = True
            Images = mImageList
            TabOrder = 2
            object bOr: TToolButton
              Left = 0
              Top = 0
              Hint = 'AND'
              Caption = 'bOr'
              ImageIndex = 24
              Style = tbsCheck
              OnClick = mPatternListClick
            end
          end
        end
        object mBasicFilterPanel: TmmPanel
          Left = 0
          Top = 0
          Width = 258
          Height = 22
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object edTreeFilter: TmmEdit
            Left = 0
            Top = 0
            Width = 212
            Height = 21
            Color = clWindow
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clGrayText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            Visible = True
            OnKeyDown = edTreeFilterKeyDown
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            InfoText = 'Filter'
            NumMode = False
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object mmToolBar3: TmmToolBar
            Left = 157
            Top = 0
            Width = 101
            Height = 22
            Align = alRight
            Caption = 'mmToolBar3'
            EdgeBorders = []
            Flat = True
            Images = mImageList
            TabOrder = 1
            object ToolButton15: TToolButton
              Left = 0
              Top = 0
              Action = acDeleteFilter
            end
            object ToolButton16: TToolButton
              Left = 23
              Top = 0
              Width = 8
              Caption = 'ToolButton16'
              ImageIndex = 24
              Style = tbsSeparator
            end
            object bShowAssignedElements: TToolButton
              Left = 31
              Top = 0
              Action = acShowActiveElements
              Style = tbsCheck
            end
            object bShowUnassignedElements: TToolButton
              Left = 54
              Top = 0
              Action = acShowInactiveElements
              Style = tbsCheck
            end
            object ToolButton14: TToolButton
              Left = 77
              Top = 0
              Action = acAdvancedFilter
              Style = tbsCheck
            end
          end
        end
      end
      object mmPanel4: TmmPanel
        Left = 1
        Top = 1
        Width = 258
        Height = 24
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object mmLabel4: TmmLabel
          Left = 0
          Top = 7
          Width = 85
          Height = 13
          Anchors = [akLeft, akBottom]
          Caption = 'Schemaelemente:'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmToolBar2: TmmToolBar
          Left = 209
          Top = 0
          Width = 49
          Height = 24
          Align = alRight
          Caption = 'mmToolBar2'
          EdgeBorders = []
          Flat = True
          Images = mImageList
          TabOrder = 0
          object bShowPrevElement: TToolButton
            Left = 0
            Top = 0
            Action = acPreviousItem
          end
          object bNextElement: TToolButton
            Left = 23
            Top = 0
            Action = acNextItem
          end
        end
      end
      object paFlags: TmmPanel
        Left = 1
        Top = 287
        Width = 258
        Height = 41
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 3
        object la1: TmmLineLabel
          Left = 1
          Top = 1
          Width = 256
          Height = 17
          Align = alTop
          AutoSize = False
          Caption = 'Flags:'
          Distance = 2
        end
        object cbProofed: TmmCheckBox
          Left = 16
          Top = 16
          Width = 73
          Height = 17
          Caption = 'Kontrolliert'
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbObserve: TmmCheckBox
          Left = 96
          Top = 16
          Width = 81
          Height = 17
          Caption = 'Beobachten'
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
    object mmPanel1: TmmPanel
      Left = 260
      Top = 0
      Width = 776
      Height = 502
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object mmPanel13: TmmPanel
        Left = 0
        Top = 433
        Width = 776
        Height = 69
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object meDocu: TmmRichEdit
          Left = 6
          Top = 0
          Width = 770
          Height = 69
          Hint = 'Beschreibung von diesem Element'
          Align = alClient
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            'meDocu')
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
          OnChange = ControlEntryChange
        end
        object mmPanel15: TmmPanel
          Left = 0
          Top = 0
          Width = 6
          Height = 69
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
        end
      end
      object mmPanel3: TmmPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 433
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object mmLabel1: TmmLabel
          Left = 520
          Top = 104
          Width = 142
          Height = 13
          Caption = 'Elementname f�r Yarn Master:'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmGroupBox3: TmmGroupBox
          Left = 296
          Top = 272
          Width = 249
          Height = 155
          Caption = 'Konvertierungen'
          TabOrder = 1
          CaptionSpace = True
          object mmLabel10: TmmLabel
            Left = 96
            Top = 17
            Width = 41
            Height = 13
            Caption = 'Operator'
            FocusControl = cobOperator
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel11: TmmLabel
            Left = 160
            Top = 17
            Width = 23
            Height = 13
            Caption = 'Wert'
            FocusControl = edOperatorValue
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel19: TmmLabel
            Left = 8
            Top = 17
            Width = 66
            Height = 13
            Caption = 'Konvertierung'
            FocusControl = cobConvertMode
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton3: TmmSpeedButton
            Left = 216
            Top = 32
            Width = 24
            Height = 22
            Action = acConvUp
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000F
              FFFFFFFF0000000FFFFFFFFFF00000FFFFFFFFFFF00000FFFFFFFFFFFF000FFF
              FFFFFFFFFF000FFFFFFFFFFFFFF0FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton4: TmmSpeedButton
            Left = 216
            Top = 80
            Width = 24
            Height = 22
            Action = acConvDown
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
              FFFFFFFFFFF0FFFFFFFFFFFFFF000FFFFFFFFFFFFF000FFFFFFFFFFFF00000FF
              FFFFFFFFF00000FFFFFFFFFF0000000FFFFFFFFF0000000FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton5: TmmSpeedButton
            Left = 8
            Top = 64
            Width = 97
            Height = 22
            Action = acConvAdd
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton6: TmmSpeedButton
            Left = 216
            Top = 56
            Width = 24
            Height = 22
            Action = acConvDelete
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              00001F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              00001F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cobOperator: TmmComboBox
            Left = 96
            Top = 32
            Width = 57
            Height = 21
            Hint = 'Verf�gbare Operatoren'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            TabOrder = 1
            Visible = True
            AutoLabel.Control = mmLabel10
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object edOperatorValue: TmmEdit
            Left = 160
            Top = 32
            Width = 49
            Height = 21
            Hint = 
              'Wert, welcher als Parameter zu dieser Konvertierung mitgespeiche' +
              'rt wird.'
            Color = clWindow
            TabOrder = 2
            Text = '10'
            Visible = True
            OnEnter = edOffsetEnter
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel11
            AutoLabel.LabelPosition = lpTop
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object lbConvertPara: TmmListBox
            Left = 112
            Top = 64
            Width = 97
            Height = 41
            Hint = 
              'Diese Parameter f�r die Konvertierung sind definiert. Mit den Pf' +
              'eilen auf/ab kann die Reihenfolge definieren werden.'
            Enabled = True
            ExtendedSelect = False
            ItemHeight = 13
            TabOrder = 3
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cobConvertMode: TmmComboBox
            Tag = 14
            Left = 8
            Top = 32
            Width = 81
            Height = 21
            Hint = 'Diese Konvertierungen sind m�glich.'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            TabOrder = 0
            Visible = True
            OnChange = cobConvertModeChange
            AutoLabel.Control = mmLabel19
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object mmPanel11: TmmPanel
            Left = 8
            Top = 112
            Width = 233
            Height = 32
            BevelOuter = bvLowered
            TabOrder = 4
            object mmPanel12: TmmPanel
              Left = 1
              Top = 1
              Width = 21
              Height = 30
              Align = alLeft
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 0
              object mmImage2: TmmImage
                Left = 2
                Top = 0
                Width = 16
                Height = 16
                Picture.Data = {
                  07544269746D617036030000424D360300000000000036000000280000001000
                  0000100000000100180000000000000300000000000000000000000000000000
                  0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                  E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                  FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                  84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                  638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                  DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                  CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                  FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                  FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                  CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                  716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                  FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                  DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                  695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                  FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                  FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                  7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                  FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                  FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                  FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                  FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                  DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFF}
                Transparent = True
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object mmMemo3: TmmMemo
              Left = 22
              Top = 1
              Width = 210
              Height = 30
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Lines.Strings = (
                'Es wird die Konvertierung von XML '
                'nach Bin�r beschrieben.')
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
        object mmGroupBox4: TmmGroupBox
          Left = 40
          Top = 16
          Width = 249
          Height = 161
          Caption = '&Grunddaten'
          TabOrder = 2
          CaptionSpace = True
          object mmLabel5: TmmLabel
            Left = 88
            Top = 17
            Width = 100
            Height = 13
            Caption = 'Byte Offset (Abs/Rel)'
            FocusControl = edOffset
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel9: TmmLabel
            Left = 8
            Top = 17
            Width = 43
            Height = 13
            Caption = 'Datentyp'
            FocusControl = cobDataType
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel8: TmmLabel
            Left = 8
            Top = 57
            Width = 32
            Height = 13
            Caption = 'Anzahl'
            FocusControl = edCount
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel17: TmmLabel
            Left = 88
            Top = 57
            Width = 34
            Height = 13
            Caption = 'Default'
            FocusControl = edDefault
            Visible = True
            AutoLabel.LabelPosition = lpTop
          end
          object mmSpeedButton24: TmmSpeedButton
            Left = 160
            Top = 33
            Width = 24
            Height = 22
            Action = acChangeOffRec
            Flat = True
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000E07F00001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000000000E07F00001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000E07FE07FE07FE07FE07F00001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07F0000000000000000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000E07FE07FE07F00001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00000000000000000000E07FFF7FE07F00001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07FE07FE07FFF7FE07FE07FE07F0000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07FFF7FE07F0000000000000000
              00001F7C1F7C1F7C1F7C1F7C1F7C0000E07FFF7FE07FFF7FE07F00001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000E07FFF7FE07FFF7FE07F00001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FFF7FFF7FFF7FFF7FFF7F0000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000FF7FE07FFF7FFF7FE07FFF7F
              00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000000000000000000000
              000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmBevel2: TmmBevel
            Left = 8
            Top = 101
            Width = 145
            Height = 2
          end
          object edOffset: TmmEdit
            Left = 88
            Top = 32
            Width = 65
            Height = 21
            Hint = 'Absoluter Offset des Speicherplatzes'
            Color = clWindow
            TabOrder = 1
            Text = '10'
            Visible = True
            OnChange = ControlEntryChange
            OnEnter = edOffsetEnter
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel5
            AutoLabel.LabelPosition = lpTop
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object cobDataType: TmmComboBox
            Left = 8
            Top = 32
            Width = 65
            Height = 21
            Hint = 'Datentyp definieren'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            TabOrder = 0
            Visible = True
            OnChange = cobDataTypeChange
            AutoLabel.Control = mmLabel9
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object edCount: TmmEdit
            Left = 8
            Top = 72
            Width = 65
            Height = 21
            Hint = 'Nur f�r Byte/Char Arrays oder UCBit/UCChar Datentypen'
            Color = clWindow
            TabOrder = 3
            Text = '1'
            Visible = True
            OnChange = ControlEntryChange
            OnEnter = edOffsetEnter
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel8
            AutoLabel.LabelPosition = lpTop
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object edDefault: TmmEdit
            Left = 88
            Top = 72
            Width = 65
            Height = 21
            Hint = 
              'Defaultwert, welcher verwendet wird, wenn in den XML-Settings di' +
              'eses Element nicht vorhanden ist. Kann auch ein Aufz�hlungswert ' +
              'sein.'
            Color = clWindow
            TabOrder = 4
            Text = '65'
            Visible = True
            OnChange = ControlEntryChange
            OnEnter = edOffsetEnter
            AutoLabel.Control = mmLabel17
            AutoLabel.LabelPosition = lpTop
            Decimals = -1
            NumMode = False
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object cbConvertBoth: TmmRadioButton
            Left = 8
            Top = 116
            Width = 113
            Height = 17
            Caption = 'Beide Richtungen'
            Checked = True
            TabOrder = 5
            TabStop = True
          end
          object cbConvertUp: TmmRadioButton
            Left = 8
            Top = 133
            Width = 81
            Height = 17
            Caption = 'Bin --> XML'
            TabOrder = 6
          end
          object cbConvertDown: TmmRadioButton
            Left = 104
            Top = 134
            Width = 81
            Height = 17
            Caption = 'XML --> Bin'
            TabOrder = 7
          end
          object mmPanel10: TmmPanel
            Left = 160
            Top = 70
            Width = 82
            Height = 43
            BevelOuter = bvLowered
            BorderWidth = 1
            TabOrder = 2
            object mmMemo2: TmmMemo
              Left = 2
              Top = 2
              Width = 78
              Height = 39
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Lines.Strings = (
                'Default Wert '
                'bezieht sich '
                'auf XML.')
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 0
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
        object mFacetGroupBox: TmmGroupBox
          Left = 299
          Top = 16
          Width = 206
          Height = 105
          Caption = '&Facet'
          TabOrder = 3
          CaptionSpace = True
          object mmLabel6: TmmLabel
            Left = 12
            Top = 24
            Width = 34
            Height = 13
            Caption = 'MinInkl'
            FocusControl = edMinIncl
            Visible = True
            AutoLabel.LabelPosition = lpTop
          end
          object mmLabel7: TmmLabel
            Left = 9
            Top = 58
            Width = 37
            Height = 13
            Caption = 'MaxInkl'
            FocusControl = edMaxIncl
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel15: TmmLabel
            Left = 105
            Top = 24
            Width = 37
            Height = 13
            Caption = 'MinExkl'
            FocusControl = edMinExcl
            Visible = True
            AutoLabel.LabelPosition = lpTop
          end
          object mmLabel16: TmmLabel
            Left = 102
            Top = 58
            Width = 40
            Height = 13
            Caption = 'MaxExkl'
            FocusControl = edMaxExcl
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object edMinIncl: TmmEdit
            Left = 48
            Top = 20
            Width = 50
            Height = 21
            Hint = 'Wert muss gr�sser/gleich sein'
            Color = clWindow
            TabOrder = 0
            Text = '30'
            Visible = True
            OnChange = edMinInclChange
            OnEnter = edOffsetEnter
            OnExit = edMinInclExit
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel6
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object edMaxIncl: TmmEdit
            Left = 48
            Top = 54
            Width = 50
            Height = 21
            Hint = 'Wert muss kleiner/gleich sein'
            Color = clWindow
            TabOrder = 2
            Text = '300'
            Visible = True
            OnChange = edMaxInclChange
            OnEnter = edOffsetEnter
            OnExit = edMinInclExit
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel7
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object edMinExcl: TmmEdit
            Left = 144
            Top = 20
            Width = 50
            Height = 21
            Hint = 'Wert muss gr�sser sein'
            Color = clWindow
            TabOrder = 1
            Visible = True
            OnChange = edMinExclChange
            OnEnter = edOffsetEnter
            OnExit = edMinInclExit
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel15
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object edMaxExcl: TmmEdit
            Left = 144
            Top = 54
            Width = 50
            Height = 21
            Hint = 'Wert muss kleiner sein'
            Color = clWindow
            TabOrder = 3
            Visible = True
            OnChange = edMaxExclChange
            OnEnter = edOffsetEnter
            OnExit = edMinInclExit
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel16
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object mmPanel8: TmmPanel
            Left = 8
            Top = 83
            Width = 193
            Height = 16
            BevelOuter = bvLowered
            TabOrder = 4
            object mmPanel9: TmmPanel
              Left = 1
              Top = 1
              Width = 21
              Height = 14
              Align = alLeft
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 0
              object mmImage1: TmmImage
                Left = 2
                Top = 0
                Width = 16
                Height = 16
                Picture.Data = {
                  07544269746D617036030000424D360300000000000036000000280000001000
                  0000100000000100180000000000000300000000000000000000000000000000
                  0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                  E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                  FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                  84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                  638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                  DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                  CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                  FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                  FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                  CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                  716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                  FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                  DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                  695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                  FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                  FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                  7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                  FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                  FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                  FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                  FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                  DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFF}
                Transparent = True
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object mmMemo1: TmmMemo
              Left = 22
              Top = 1
              Width = 170
              Height = 14
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Lines.Strings = (
                'Bezieht sich auf XML Werte.')
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
        object mmGroupBox6: TmmGroupBox
          Left = 8
          Top = 186
          Width = 281
          Height = 241
          Caption = 'Aufz�hlungen'
          TabOrder = 4
          CaptionSpace = True
          object mmSpeedButton1: TmmSpeedButton
            Left = 120
            Top = 104
            Width = 24
            Height = 22
            Action = acEnumUp
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000F
              FFFFFFFF0000000FFFFFFFFFF00000FFFFFFFFFFF00000FFFFFFFFFFFF000FFF
              FFFFFFFFFF000FFFFFFFFFFFFFF0FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton2: TmmSpeedButton
            Left = 120
            Top = 152
            Width = 24
            Height = 22
            Action = acEnumDown
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
              FFFFFFFFFFF0FFFFFFFFFFFFFF000FFFFFFFFFFFFF000FFFFFFFFFFFF00000FF
              FFFFFFFFF00000FFFFFFFFFF0000000FFFFFFFFF0000000FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel20: TmmLabel
            Left = 8
            Top = 17
            Width = 18
            Height = 13
            Caption = 'Typ'
            FocusControl = cobEnumeration
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton7: TmmSpeedButton
            Left = 120
            Top = 128
            Width = 24
            Height = 22
            Action = acEnumDelete
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              00001F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              00001F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton13: TmmSpeedButton
            Left = 144
            Top = 212
            Width = 129
            Height = 22
            Action = acEnumSetDefault
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton17: TmmSpeedButton
            Left = 120
            Top = 184
            Width = 24
            Height = 22
            Action = acEnumIterate
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton18: TmmSpeedButton
            Left = 112
            Top = 56
            Width = 24
            Height = 22
            Action = acEnumAdd
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000001F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel25: TmmLabel
            Left = 120
            Top = 36
            Width = 46
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'Wert:'
            FocusControl = seEnumValue
            Visible = True
            AutoLabel.Distance = 4
            AutoLabel.LabelPosition = lpLeft
          end
          object cobEnumeration: TmmComboBox
            Left = 8
            Top = 32
            Width = 97
            Height = 21
            Hint = 'Liste der verf�gbaren Aufz�hlungen'
            Style = csDropDownList
            Color = clWindow
            ItemHeight = 13
            TabOrder = 1
            Visible = True
            OnChange = cobEnumerationChange
            AutoLabel.Control = mmLabel20
            AutoLabel.LabelPosition = lpTop
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object lbEnumerationSelected: TmmListBox
            Left = 144
            Top = 56
            Width = 129
            Height = 153
            Hint = 
              'Diese Aufz�hlungen sind selektiert. Mit den Pfeilen auf/ab kann ' +
              'die Reihenfolge definieren werden.'
            Enabled = True
            ItemHeight = 13
            TabOrder = 4
            Visible = True
            OnClick = lbEnumerationSelectedClick
            AutoLabel.LabelPosition = lpLeft
          end
          object lbEnumerationAvailable: TmmListBox
            Left = 8
            Top = 56
            Width = 97
            Height = 177
            Hint = 
              'Doppelklicken sie einzelne Werte oder markieren sie mehrere und ' +
              'dr�cken sie den Pfeil rechts.'
            Enabled = True
            ItemHeight = 13
            MultiSelect = True
            TabOrder = 3
            Visible = True
            OnDblClick = lbEnumerationAvailableDblClick
            AutoLabel.LabelPosition = lpLeft
          end
          object seEnumValue: TmmSpinEdit
            Left = 168
            Top = 32
            Width = 49
            Height = 22
            Hint = 'Aufz�hlungswert'
            MaxValue = 0
            MinValue = 0
            TabOrder = 2
            Value = 0
            Visible = True
            OnChange = seEnumValueChange
            AutoLabel.Control = mmLabel25
            AutoLabel.LabelPosition = lpLeft
          end
          object cbEnumOthers: TmmCheckBox
            Left = 144
            Top = 15
            Width = 129
            Height = 17
            Caption = 'Element als '#39'others'#39
            TabOrder = 0
            Visible = True
            OnClick = cbEnumOthersClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmGroupBox1: TmmGroupBox
          Left = 552
          Top = 186
          Width = 217
          Height = 241
          Caption = 'Ereignisse'
          TabOrder = 5
          CaptionSpace = True
          object mmLabel26: TmmLabel
            Left = 8
            Top = 17
            Width = 38
            Height = 13
            Caption = 'Element'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton19: TmmSpeedButton
            Left = 184
            Top = 32
            Width = 24
            Height = 22
            Action = acEventNew
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C00000000000000000000100010000000000000000000
              00001F7C1F7C1F7C1F7C00001000100010001000100010001000100010001000
              00001F7C1F7C1F7C1F7C00001000100010001000100010001000100010001000
              00001F7C1F7C1F7C1F7C00000000000000000000100010000000000000000000
              00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00001000100000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton21: TmmSpeedButton
            Left = 184
            Top = 96
            Width = 24
            Height = 22
            Action = acEventUp
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0000000F
              FFFFFFFF0000000FFFFFFFFFF00000FFFFFFFFFFF00000FFFFFFFFFFFF000FFF
              FFFFFFFFFF000FFFFFFFFFFFFFF0FFFFFFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton22: TmmSpeedButton
            Left = 184
            Top = 120
            Width = 24
            Height = 22
            Action = acEventDelete
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C
              00001F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C000000000000000000001F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C0000000000001F7C1F7C000000001F7C
              1F7C1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C00000000
              1F7C1F7C1F7C1F7C1F7C00000000000000001F7C1F7C1F7C1F7C1F7C1F7C0000
              00001F7C1F7C1F7C1F7C0000000000001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C00001F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton23: TmmSpeedButton
            Left = 184
            Top = 144
            Width = 24
            Height = 22
            Action = acEventDown
            Glyph.Data = {
              F6000000424DF600000000000000760000002800000010000000100000000100
              0400000000008000000000000000000000001000000000000000000000000000
              8000008000000080800080000000800080008080000080808000C0C0C0000000
              FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF0FFFF
              FFFFFFFFFFF0FFFFFFFFFFFFFF000FFFFFFFFFFFFF000FFFFFFFFFFFF00000FF
              FFFFFFFFF00000FFFFFFFFFF0000000FFFFFFFFF0000000FFFFFFFFFFFFFFFFF
              FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object rbEventInt: TmmRadioButton
            Left = 8
            Top = 56
            Width = 49
            Height = 17
            Hint = 'Interner oder externer Event'
            Caption = 'Int'
            Checked = True
            TabOrder = 0
            TabStop = True
          end
          object rbEventExt: TmmRadioButton
            Tag = 1
            Left = 64
            Top = 56
            Width = 49
            Height = 17
            Hint = 'Interner oder externer Event'
            Caption = 'Ext'
            TabOrder = 1
          end
          object lbEvents: TmmListBox
            Left = 8
            Top = 96
            Width = 169
            Height = 71
            Hint = 
              'Diese Ereignisse sind definiert. Mit den Pfeilen auf/ab kann die' +
              ' Reihenfolge definieren werden.'
            Anchors = [akLeft, akTop, akRight]
            Enabled = True
            ExtendedSelect = False
            ItemHeight = 13
            TabOrder = 2
            Visible = True
            OnClick = lbEventsClick
            AutoLabel.LabelPosition = lpLeft
          end
          object edEventPath: TmmEdit
            Left = 8
            Top = 32
            Width = 169
            Height = 21
            Hint = 'Zu selektierendes Element mittels XPath eingeben'
            Color = clWindow
            TabOrder = 3
            Visible = True
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = False
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object cbEventDelflag: TmmCheckBox
            Left = 8
            Top = 74
            Width = 201
            Height = 17
            Hint = 
              'Das Element wird nach dem Konvertieren gel�scht. Dies ist z.B. n' +
              'otwendig, wenn ein Element im Millmaster nebenl�ufig gef�hrt wir' +
              'd und deshalb nicht mehr im MMXML erscheinen soll (z.B. YarnCnt)'
            Caption = 'Element nach Konvertierung l�schen'
            TabOrder = 4
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object memoCurrentEvent: TmmRichEdit
            Left = 8
            Top = 169
            Width = 201
            Height = 64
            BorderStyle = bsNone
            Color = clInfoBk
            ReadOnly = True
            ScrollBars = ssBoth
            TabOrder = 5
          end
        end
        object mDescriptionPanel: TmmPanel
          Left = 520
          Top = 20
          Width = 239
          Height = 77
          BevelOuter = bvLowered
          TabOrder = 7
          object mmPanel14: TmmPanel
            Left = 1
            Top = 1
            Width = 21
            Height = 75
            Align = alLeft
            BevelOuter = bvNone
            Color = clInfoBk
            TabOrder = 0
            object mInfoImage: TmmImage
              Left = 2
              Top = 0
              Width = 16
              Height = 16
              Picture.Data = {
                07544269746D617036030000424D360300000000000036000000280000001000
                0000100000000100180000000000000300000000000000000000000000000000
                0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFF}
              Transparent = True
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
          object mDescriptionMemo: TmmMemo
            Left = 22
            Top = 1
            Width = 216
            Height = 75
            Align = alClient
            BorderStyle = bsNone
            Color = clInfoBk
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmToolBar4: TmmToolBar
          Left = 8
          Top = 8
          Width = 25
          Height = 133
          Align = alNone
          ButtonHeight = 23
          Caption = 'mmToolBar4'
          EdgeBorders = []
          Flat = True
          Images = mImageList
          TabOrder = 8
          object ToolButton8: TToolButton
            Left = 0
            Top = 0
            Action = acElementCopy
            Wrap = True
          end
          object ToolButton9: TToolButton
            Left = 0
            Top = 23
            Action = acElementPaste
            Wrap = True
          end
          object ToolButton10: TToolButton
            Left = 0
            Top = 46
            Action = acElementClear
            Wrap = True
          end
          object ToolButton11: TToolButton
            Left = 0
            Top = 69
            Action = acElementUndo
          end
          object ToolButton20: TToolButton
            Left = 0
            Top = 69
            Width = 8
            Caption = 'ToolButton20'
            ImageIndex = 14
            Wrap = True
            Style = tbsSeparator
          end
          object ToolButton21: TToolButton
            Left = 0
            Top = 100
            Action = acPasteActiveOnly
          end
        end
        object mmGroupBox2: TmmGroupBox
          Left = 296
          Top = 186
          Width = 249
          Height = 81
          Caption = 'Config Code'
          TabOrder = 9
          CaptionSpace = True
          object mmLabel18: TmmLabel
            Left = 126
            Top = 20
            Width = 12
            Height = 13
            Caption = 'Bit'
            Enabled = False
            FocusControl = cbCCBit
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton8: TmmSpeedButton
            Left = 96
            Top = 16
            Width = 24
            Height = 22
            Action = acFindCC
            Flat = True
            Glyph.Data = {
              42020000424D4202000000000000420000002800000010000000100000000100
              1000030000000002000000000000000000000000000000000000007C0000E003
              00001F0000001F7C893D574A1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C6D622376E951594A1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7CC97ECA7E2476E951584A1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7CCA7ECA7E0372E951594A1F7C1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7CCA7EC97E2372E951574A1F7C1F7C1F7C1F7C1F7C1F7C
              1F7C1F7C1F7C1F7C1F7C1F7CEA7EC97E046EAB391F7C163E784A784A77461F7C
              1F7C1F7C1F7C1F7C1F7C1F7C1F7CEA7E5677774A77467C63FF73FF73FF6F1B5B
              36421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C994ABF5FFF6FFF6FFF6FFF73FF7F
              7D6F153E1F7C1F7C1F7C1F7C1F7C1F7C1F7C984A7F57DF6BFF6FFF73FF7FFF7F
              FF7FB84E1F7C1F7C1F7C1F7C1F7C1F7C984A9F571E4BDF67FF6FFF73FF7BFF7B
              FF779D6757461F7C1F7C1F7C1F7C1F7C984A9F57DD427E57FF6FFF6FFF73FF73
              FF73BE6B57461F7C1F7C1F7C1F7C1F7C57469F5B1E4B3E4B9F5FFF6FFF6FFF6F
              FF735C5F36421F7C1F7C1F7C1F7C1F7C1F7C1B57FF737E5F3E4B5E53BF5FDF67
              FF6B56461F7C1F7C1F7C1F7C1F7C1F7C1F7C36427D6FFF7B7F531E475F4FBF5F
              DA4E57421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C153EB94E9D5B7D5B3C575742
              57421F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C1F7C5746774656461F7C
              1F7C1F7C1F7C}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbCCBit: TmmComboBox
            Left = 142
            Top = 16
            Width = 73
            Height = 21
            Style = csDropDownList
            Color = clWindow
            Enabled = False
            ItemHeight = 13
            TabOrder = 0
            Visible = True
            OnClick = ControlEntryChange
            Items.Strings = (
              '0'
              '1'
              '2'
              '3'
              '4'
              '5'
              '6'
              '7'
              '8'
              '9'
              '10'
              '11'
              '12'
              '13'
              '14'
              '15'
              '16')
            AutoLabel.Control = mmLabel18
            AutoLabel.Distance = 4
            AutoLabel.LabelPosition = lpLeft
            Edit = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
          end
          object rbCCNone: TmmRadioButton
            Left = 16
            Top = 20
            Width = 49
            Height = 17
            Caption = '-'
            Checked = True
            TabOrder = 1
            TabStop = True
            OnClick = ControlEntryChange
          end
          object rbCCA: TmmRadioButton
            Left = 16
            Top = 40
            Width = 90
            Height = 17
            Caption = 'Configcode A'
            Enabled = False
            TabOrder = 2
            OnClick = ControlEntryChange
          end
          object rbCCB: TmmRadioButton
            Left = 16
            Top = 60
            Width = 90
            Height = 17
            Caption = 'Configcode B'
            Enabled = False
            TabOrder = 3
            OnClick = ControlEntryChange
          end
          object rbCCC: TmmRadioButton
            Left = 125
            Top = 40
            Width = 90
            Height = 17
            Caption = 'Configcode C'
            Enabled = False
            TabOrder = 4
            OnClick = ControlEntryChange
          end
          object rbCCD: TmmRadioButton
            Left = 125
            Top = 60
            Width = 90
            Height = 17
            Caption = 'Configcode D'
            Enabled = False
            TabOrder = 5
            OnClick = ControlEntryChange
          end
        end
        object mBitFieldGroupBox: TmmGroupBox
          Left = 299
          Top = 128
          Width = 206
          Height = 49
          Caption = 'Bitfeld'
          TabOrder = 0
          CaptionSpace = True
          object mmLabel13: TmmLabel
            Left = 14
            Top = 22
            Width = 40
            Height = 13
            Caption = 'BitOffset'
            FocusControl = edBitOffset
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel14: TmmLabel
            Left = 110
            Top = 22
            Width = 40
            Height = 13
            Caption = 'BitCount'
            FocusControl = edBitCount
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object edBitOffset: TmmEdit
            Left = 56
            Top = 18
            Width = 41
            Height = 21
            Hint = 
              'Offset des Startbits bei Bitwerten. Der Datentyp muss byte, smal' +
              'lint, word, shortint, dword oder longint sein'
            Color = clWindow
            TabOrder = 0
            Text = '2'
            Visible = True
            OnChange = ControlEntryChange
            OnEnter = edOffsetEnter
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel13
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
          object edBitCount: TmmEdit
            Left = 152
            Top = 18
            Width = 41
            Height = 21
            Hint = 'Anzahl der verwendeten Bits'
            Color = clWindow
            TabOrder = 1
            Text = '1'
            Visible = True
            OnChange = ControlEntryChange
            OnEnter = edOffsetEnter
            Alignment = taRightJustify
            AutoLabel.Control = mmLabel14
            AutoLabel.LabelPosition = lpLeft
            Decimals = -1
            NumMode = True
            ReadOnlyColor = clInfoBk
            ShowMode = smNormal
            ValidateMode = [vmInput]
          end
        end
        object edYMElementName: TRichEdit
          Left = 520
          Top = 120
          Width = 241
          Height = 57
          Hint = 
            'Legt die Konstante fest die in das File '#39'MMXMLItemsDef'#39' eingetra' +
            'gen wird. Die Konstante bezeichnet den XPath zum aktuellen Eleme' +
            'nt.'
          TabOrder = 6
          WantTabs = True
          WantReturns = False
          OnEnter = edOffsetEnter
        end
      end
    end
  end
  object timTreeFilter: TmmTimer
    Enabled = False
    Interval = 300
    OnTimer = timTreeFilterTimer
    Left = 168
    Top = 365
  end
  object mImageList: TmmImageList
    Left = 800
    Top = 40
    Bitmap = {
      494C010122002700040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000040000000A0000000010020000000000000A0
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000024707D00175A6400175A
      6400175A6400175A640000000000000000000000000044434600444346000000
      00000000000000000000000000000000000000000000282D7A001B2062001B20
      62001B2062001B20620000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E4144001B5C
      6600000000000000000000000000000000001668780063C3D50066E1F60048D9
      F2003CC4DA002EA9BF00175A6400175A64000000000044434600403F45001B20
      6200000000000000000000000000000000001A1F7500676CD0006C74F0005059
      EC00434CD400343CBA001B2062001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003C3E41003BA2
      B30051DFF7003BB5C9002B9CAF00168193000B768A0089EDFF0099F1FF007CEC
      FF005EE8FF0047E4FF0029A4B900175A64000000000044434600444346004047
      AF005760F1004048C3003139AA001B228E00101685008A91FF0099A0FF007C84
      FF005F6AFF004B55FB002F37B3001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E41440040AD
      C00089EDFF0080ECFF0058E7FF0015B0C90000788E0080ECFF0096F0FF0072EA
      FF0055E6FF0041DEFA0029A4B900175A640000000000444346003D3D4000464D
      BC008A91FF008188FF005963FF001C27C200060D8800868CF600969CFF00727A
      FF00555FFF004852F3002F37B3001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E41440040AD
      C00089EDFF007CECFF004BE4FF0016A9C30001778D0080ECFF0096F0FF0072EA
      FF0055E6FF0041DEFA0029A4B9001B5C66000000000044434600403F4500464D
      BC008188FF007C84FF004F59FB001D26BC00060D8800868CF600969CFF00727A
      FF00555FFF004852F3003038B3001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E41440040AD
      C00080ECFF007CECFF004BE4FF0016A9C30001778D0080ECFF0096F0FF0072EA
      FF0055E6FF0047E4FF0029A4B9001657610000000000444346003D3D4000464D
      BC008A91FF007C84FF004B55FB001D26BC00060D8800868CF600969CFF00727A
      FF00555FFF004852F3003038B3001A1F5E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E41440040AD
      C00089EDFF0072EAFF004BE4FF0016A9C30001778D0080ECFF0096F0FF0072EA
      FF0055E6FF0047E4FF0029A4B900175A640000000000444346003D3D4000464D
      BC008188FF007C84FF004F59FB001D26BC00060D8800868CF600969CFF00727A
      FF00555FFF004852F300343CBA001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E41440041B3
      C60089EDFF0080ECFF0055E6FF0016A9C30001778D0080ECFF0096F0FF0072EA
      FF0055E6FF0040E1FD002FB7CD00175A640000000000444346003D3D40004048
      C300969CFF007C84FF00555FFF001D26BC00060D8800888EFA00969CFF00727A
      FF00555FFF004852F300363FC7001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000444546003E4144003593
      A2004ED1E70048D9F20043D7F10016A9C30001778D003D919F002F8595002EA1
      B50031A8BB002EA1B5002795A8001B5C66000000000044434600444346003A41
      9F00545DE1005059EC004A53EB001D26BC00060D88003A419F00333891003139
      AA003038B3003038B3002C33A3001B2062000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004445460044454600345A
      640015616E002D8D9F002EA1B500166878000E677600165D6800000000000000
      0000000000000000000015525C00175A64000000000044434600444346003637
      6200191E6B0031369A003038B3001B217900111772001B206200000000000000
      000000000000000000001A1F5E001A1F5E000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044454600444546000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044434600444346000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000063737B00315A84000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004A637B00BD9494000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B3110004A8CC6001084EF004A5A
      7B008C4221007B3110007B3110007B3110007B3110007B3110007B3110007B31
      10007B3110007B3110007B3110007B3110006B9CC600188CEF004A7BA500CE94
      9400000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006B291000B5DEF70042ADFF00187B
      E70063739400BD521000FFDEB500FFDEAD00FFD6A500FFD6A500FFCE9C00FFCE
      9400FFCE9400FFCE9400FFCE9400732910004AB5FF0052B5FF00218CEF004A7B
      A500C69494000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006B291000FFFFFF00B5D6F70042AD
      FF00187BE700736B6B00C6A58C00B5948400B5948400C6A58C00E7BD9400FFCE
      9C00FFCE9400FFCE9400FFCE9400732910000000000052B5FF0052B5FF001884
      E7004A7BA500CE94940000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006B291000FFFFFF00FFFFFF00ADD6
      F7006BADDE009C8C8400AD8C7B00D6A57300DEC6A500CEAD9400B58C7300DE84
      1800FF840000FF840000FFCE940073291000000000000000000052B5FF004AB5
      FF00188CE7004A7BA500BD949400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006B291000FFFFFF0031A5310031A5
      3100BDADAD00C6947300F7D6A500FFFFD600FFFFE700FFFFEF00DEC6BD00BD9C
      8C00FFCE9C00FFCE9C00FFCE94007329100000000000000000000000000052BD
      FF004AB5FF002184DE005A6B730000000000B5847B00C69C9400C69C9400BD9C
      8C00000000000000000000000000000000000000000044464500444645000000
      00000000000000000000000000000000000000000000346E3800255728002557
      2800255728002557280000000000000000000000000046454400464544000000
      000000000000000000000000000000000000000000006E343500562425005624
      2500562425005624250000000000000000006B291000FFFFFF0031A5310031A5
      3100B59C8C00D69C6B00FFE79C00FFF7C600FFFFDE00FFFFF700F7F7EF00B594
      8400FF840000FF840000FFCE9C00732910000000000000000000000000000000
      000052BDFF00B5D6EF00BD9C9400BD9C8C00E7DEC600FFFFE700FFFFE700FFFF
      DE00DEC6B500B58C840000000000000000000000000044464500444645002959
      2C000000000000000000000000000000000028692D0077C17D0080DD870067D5
      6F0059BF600048A64F0025572800255728000000000046454400464544005929
      2A000000000000000000000000000000000069282900C1777700DD808100D567
      6800BF595A00A648490056242500562425006B291000FFFFFF00FFFFFF00FFFF
      FF00B5948C00DEB58C00FFC67B00F7DE9C00FFFFE700FFFFEF00F7F7D600B59C
      8C00FFD6AD00FFD6A500FFCE9C00732910000000000000000000000000000000
      00000000000000000000CEA59400FFEFBD00FFFFDE00FFFFDE00FFFFDE00FFFF
      E700FFFFFF00EFDEDE00AD847B000000000000000000444645003E403E00519F
      56006EDA760054B05A00439849002C7D3200227429009CEEA3009AFEA0007DFE
      870070EE780065E16E0043A04A00255728000000000046454400403E3E009F51
      5200DA6E6F00B0545500984344007D2C2D0074222200EE9C9C00FE9A9C00FE7D
      7E00EE707200E1656600A0434400562425006B291000FFFFFF00FFFFFF00FFFF
      FF00C6ADA500CEAD8C00FFDE9400F7C68400F7E7B500FFF7CE00DED6B500CEAD
      8C00FF840000FF840000FFD6A500732910000000000000000000000000000000
      00000000000000000000C6A59400FFDEAD00FFF7D600FFFFDE00FFFFE700FFFF
      FF00FFFFFF00FFFFFF00C6AD9C000000000000000000444645003E403E0058AB
      5E0088FF910080FF89006DEB760035AA3D001975210097E69D0097FE9F007DFE
      87006CE6750062DA6B0043A04A00255728000000000046454400403E3E00AB58
      5900FF888900FF808100EB6D6E00AA35370075191900E6979700FE979800FE7D
      7E00E66C6D00DA626300A0434400562425006B291000FFFFFF00527BFF00527B
      FF00E7D6CE00B58C7300DECEB500F7E7BD00EFBD8400DEBD9400C6A58C00CEB5
      9400FFDEB500FFDEB500FFD6AD00732910000000000000000000000000000000
      000000000000C6A59400FFE7AD00F7C69400FFF7CE00FFFFDE00FFFFE700FFFF
      F700FFFFF700FFFFEF00EFE7CE00BD948C0000000000444645003E403E0057AA
      5D0088FF91007DFE87006CE6750035A53D001975210097E69D0097FE9F0079F6
      82006CE6750062DA6B0043A04A00255728000000000046454400403E3E00AA57
      5800FF888900FE7D7E00E66C6D00A535360075191900E6979700FE979800F679
      7A00E66C6D00DA626300A0434400562425006B291000FFFFFF00527BFF00527B
      FF00FFFFFF00B55A2100B59C8C00B5948400B5948400C6A58C00CE9C6B00E77B
      1000FF840000FF840000FFDEAD00732910000000000000000000000000000000
      000000000000C6A59400FFE7AD00EFB58400F7DEAD00FFFFDE00FFFFDE00FFFF
      E700FFFFE700FFFFE700F7EFD600BD948C0000000000444645003E403E0057AA
      5D0088FF91007DFE87006EDA760035A53D001975210097E69D0097FE9F007DFE
      87006CE6750062DA6B0043A04A00245527000000000046454400403E3E00AA57
      5800FF888900FE7D7E00DA6E6F00A535360075191900E6979700FE979800FE7D
      7E00E66C6D00DA626300A0434400562425009C522100C68C4200C68C4200C68C
      4200C68C4200C68C4200C68C4200C68C4200C68C4200C68C4200CE9C5200C68C
      4200CE9C5200C68C4200BD945A00A55221000000000000000000000000000000
      000000000000BD948C00FFE7B500F7C69400F7CE9400FFE7BD00FFFFDE00FFFF
      DE00FFFFDE00FFFFE700E7D6BD00B58C840000000000444645003E403E0057AA
      5D0088FF91007DFE870065E16E0035A53D001975210097E69D0097FE9F0079F6
      82006CE6750062DA6B0043A04A00255928000000000046454400403E3E00AA57
      5800FF888900FE7D7E00E1656600A535360075191900E6979700FE979800F679
      7A00E66C6D00DA626300A0434400592526009C421000D6631000D6631000D663
      1000D6631000D6631000D6631000D6631000D6631000D6631000F7AD6300D663
      1000F7AD63009C6339003152C600A54208000000000000000000000000000000
      00000000000000000000DEC6AD00FFFFE700F7DEBD00F7CE9400F7D6A500FFEF
      BD00FFF7CE00FFFFD600B5948C000000000000000000444645003E403E0058AB
      5E0088FF910080FF89006CE6750035A53D00197521009CEEA30097FE9F0079F6
      82006CE6750062DA6B004CB25300255928000000000046454400403E3E00AB58
      5900FF888900FF808100E66C6D00A535360075191900EE9C9C00FE979800F679
      7A00E66C6D00DA626300B24C4D00592526000000000094421800944218009442
      1800944218009442180094421800944218009442180094421800944218009442
      1800944218009442180094421800000000000000000000000000000000000000
      00000000000000000000B58C8400EFDEDE00FFFFF700FFDEA500F7C68C00FFD6
      9C00FFEFBD00D6B59C00BD9484000000000000000000444645003E403E004990
      4E0069CC70006EDA760067D56F0035A53D0016741D004F8F5300418346004A9E
      50004AA350004398490043984900255728000000000046454400403E3E009049
      4A00CC696A00DA6E6F00D3626300A5353600741617008F4F5000834141009E4A
      4B00A64849009F474800923E3F00592526000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AD847B00CEAD9C00EFE7B500EFDEB500E7CE
      AD00BD948400BD94840000000000000000000000000044464500444645003D5C
      410025592800418B47004398490028692D002064250025572800000000000000
      0000000000000000000024552700245527000000000046454400464544005C3E
      3D00592526008B4141009E4A4B00692829006420210059252600000000000000
      0000000000000000000056242500562425000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BD948C00BD9C8C00B594
      8C00000000000000000000000000000000000000000044464500444645000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046454400464544000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000029292900292929002929290029292900292929000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00005252520029292900DEDEE700BDBDBD009C949C008C8484007B7373002929
      29000000000000000000000000000000000000000000A5636B008C5A5A008C5A
      5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A5A008C5A
      5A008C5A5A0084524A0000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000029292900FFFFFF009C9C9C00393131003931310052525200736B6B002929
      29000000000000000000000000000000000000000000A5636B00FFEFCE00F7DE
      BD00F7D6B500F7D6A500EFCE9C00EFC69400EFC68C00EFBD8400EFBD7B00EFBD
      7B00EFC6840084524A0000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000042424200948C940039313100847373008C7B7B0039313100393131002929
      29000000000000000000000000000000000000000000A5635A00FFEFDE00F7E7
      C600F7DEBD00F7D6B500C6C68C000073000000730000BDB57300EFBD8400EFBD
      7B00EFC6840084524A0000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002929
      29009CA5B50094949C007B737B005A525A004239390042313100292929000000
      00000000000000000000000000000000000000000000A5635A00FFF7E700F7E7
      CE0000730000C6CE940000730000C6C68400C6BD840000730000BDB56B00EFBD
      8400EFC6840084524A0000000000000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000FFFF0000FFFF000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000292929008C84
      8400FFBD4A00FFC66300F7C68400E7C6A500CEC6C600A59C9C006B6B73004242
      4A002931310000000000000000000000000000000000A5736B00FFFFF700FFEF
      DE000073000000730000C6CE9C00F7D6B500F7D6AD00BDBD840000730000EFC6
      8C00EFC6840084524A0000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000031313100948C
      8C00FF8C0000FF940800FF9C1000FF9C1800FFA52100F7B54A00E7B56B00BD9C
      7B007B736B0031313100000000000000000000000000A5736B00FFFFFF00FFF7
      E700007300000073000000730000F7DEBD00F7D6B500F7D6A500EFCE9C00EFC6
      9400F7CE8C0084524A0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF000000000000FFFF00000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042424200948C
      8C00F7941000FFA51800FFA52900FFAD2900FFAD2900FFA52100FF9C1000FF9C
      1800E7942900524A3900000000000000000000000000BD846B00FFFFFF00FFF7
      F700FFEFE700FFEFDE00F7E7CE00F7E7C600007300000073000000730000EFCE
      9C00F7CE940084524A0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000042424200948C
      8C00FFA52100FFB53100FFBD4200FFBD4A00FFBD4200FFBD4200FFB53900FFAD
      2100EF9418004A392900000000000000000000000000BD846B00FFFFFF00FFFF
      FF0000730000CEDEBD00F7EFDE00F7E7CE00C6CE9C000073000000730000F7D6
      A500F7D6A50084524A0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      00000000000000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A009494
      9400FFAD3100FFBD4A00FFCE5200FFCE6300FFCE6B00FFCE5200FFC64A00FFB5
      3900E79C290042393100000000000000000000000000D6946B00FFFFFF00FFFF
      FF00CEE7CE0000730000CEDEBD00CED6B50000730000C6CE9C0000730000F7DE
      B500EFCEA50084524A0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A009494
      9400FFBD4200FFCE5200FFDE7300FFDE7B00FFDE7B00FFDE7300FFCE6300FFC6
      4A00DE9C390039393900000000000000000000000000D6946B00FFFFFF00FFFF
      FF00FFFFFF00CEE7CE000073000000730000CED6B500FFE7D600FFEFCE00DECE
      B500B5AD940084524A0000000000000000000000000000000000000000000000
      000000FFFF0000FFFF0000FFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A009494
      9400FFC65200FFD66B00FFE78400FFE79400FFEF9400FFE78400FFD66B00FFCE
      5200DEA5420039393900000000000000000000000000DE9C7300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFF7EF00FFF7E700EFDECE00A5635A00A563
      5A00A5635A00A5635A00000000000000000000000000000000000000000000FF
      FF0000FFFF000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A00948C
      8C00EFB55200FFD66B00FFEF8C00FFFFA500FFFFAD00FFEF9400FFDE7300FFCE
      5200E7AD4A0039393900000000000000000000000000DE9C7300FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700DEC6BD00A5635A00E79C
      5200E78C3100B56B4A000000000000000000000000000000000000FFFF0000FF
      FF0000000000000000000000000000FFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004A4A4A00948C
      8C008C846B00948C73009C947300ADA58400BDB58C00CEBD7B00DEBD6300FFC6
      4A00E7AD4A0042424200000000000000000000000000E7AD7B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00DEC6C600A5635A00FFB5
      5A00BD7B5A000000000000000000000000000000000000FFFF0000FFFF000000
      00000000000000000000000000000000000000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002929
      290029292900292929005A5A5A007B7B84007B7B8400847B8400948C8C007B73
      7B008C7B730042393900000000000000000000000000E7AD7B00FFF7F700FFF7
      EF00FFF7EF00FFF7EF00FFF7EF00F7F7EF00F7F7EF00DEC6C600A5635A00C684
      6B00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000042424A0042424200424242004A4A4A005252
      52004242420000000000000000000000000000000000E7AD7B00CE8C6B00CE8C
      6B00CE8C6B00CE8C6B00CE8C6B00CE8C6B00CE8C6B00CE8C6B00A5635A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5848400B584
      8400B5848400B5848400B5848400B5848400B5848400B5848400B5848400B584
      8400B5848400B5848400B5848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000006300000063000000000000000000000000
      00000000000000000000000000000000000000000000B5848400A57B7300A57B
      7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B7300A57B
      7300A57B73009C6B630000000000000000000000000000000000C6A59C00FFEF
      D600F7E7C600F7DEBD00F7DEB500F7D6AD00F7D6A500F7CE9C00F7CE9400F7CE
      9C00F7CE9C00F7D69C00B584840000000000189CCE0029A5D600189CD600088C
      C600000000000000000000000000000000000000000000000000000000000000
      0000BDA5A5004A5A730000000000000000000000000000000000000000000000
      00000000000000000000007B080063FF9C00009C000000630000000000000000
      00000000000000000000000000000000000000000000B5848400FFEFDE00F7E7
      CE00F7DEC600F7DEBD00F7D6B500F7D6AD00F7D6A500EFCE9C00EFCE9400EFCE
      9400F7D69C009C6B630000000000000000000000000000000000C6A59C00FFEF
      DE00F7E7CE00F7DEC600F7DEBD00F7D6B500F7D6AD00F7D6A500EFCE9C00EFCE
      9400EFCE9400F7D69C00B584840000000000219CCE00A5EFF7006BEFFF005ADE
      F7004AC6E70039BDE70021A5D6000894CE000894CE000000000000000000BDA5
      A500526384001073E7003994DE00000000000000000000000000000000000000
      00000000000000000000007B080063FF9C00009C000000630000000000000000
      00000000000000000000000000000000000000000000B5847300FFF7E7009C31
      00009C3100009C3100009C3100009C3100009C3100009C3100009C310000EFCE
      9400F7D69C009C6B630000000000000000000000000000000000C6ADA500FFF7
      E700FFE7D600F7E7CE00F7DEC600F7DEBD00F7D6B500F7D6AD00F7D6A500EFCE
      9C00EFCE9400F7D69C00B584840000000000088CC6009CDEEF008CFFFF0084FF
      FF0084FFFF007BFFFF007BF7FF0063E7F7004ACEEF0039BDE7007394A5005A63
      84001073DE0031A5FF00299CFF00000000000000000000000000000000000000
      00000000000000000000007B080063FF9C00009C000000630000000000000000
      00000000000000000000000000000000000000000000B5847300FFF7EF009C31
      0000FFFFFF00FFFFFF00FFFFFF008CA5FF00BDC6FF00FFFFFF009C310000EFCE
      9C00F7D69C009C6B630000000000000000000000000000000000CEADA500FFF7
      EF00FFEFDE00F7E7D600F7E7CE00F7DEC600F7DEBD00F7D6B500F7D6AD00F7D6
      A500EFCE9C00F7D69C00B5848400000000001094CE005ABDDE009CFFFF0073F7
      FF007BF7FF0073F7FF008CDEE70084D6DE007BE7EF0094BDC600526B8C001073
      D60031A5FF00319CFF0000000000000000000000000000000000000000000000
      00000000000000000000007B080063FF9C00009C000000630000000000000000
      00000000000000000000000000000000000000000000BD8C8400FFFFF7009C31
      0000FFFFFF00FFFFFF007B9CFF000031FF005A7BFF00FFFFFF009C310000F7D6
      A500F7D69C009C6B630000000000000000000000000000000000CEB5AD00FFFF
      F700FFEFE700FFEFDE00F7E7D600F7E7CE00F7DEC600F7DEBD00F7D6B500F7D6
      AD00F7D6A500F7D69C00B58484000000000021A5D6004AC6E7009CEFFF007BF7
      FF0084DEDE00BDCEBD00DEBDA500D6B59C00AD9C8C00847B7300427BAD0031A5
      FF0039ADFF00189CD60000000000000000000000000000000000000000000000
      00000000000000000000007B080039CE6B0021BD420000630000000000000000
      00000000000000000000000000000000000000000000BD8C8400FFFFFF009C31
      0000D6DEFF00426BFF000031FF004263FF000031FF00DEE7FF009C310000F7D6
      AD00F7D6A5009C6B630000000000000000000000000000000000D6B5AD00FFFF
      FF00FFF7EF00FFEFE700FFEFDE00F7E7D600F7E7CE00F7DEC600F7DEBD00F7D6
      B500F7D6AD00F7D6A500B58484000000000021ADDE008C94A5008CCEE7008CD6
      D600CEB5AD00F7F7DE00FFFFDE00FFFFDE00FFFFC600CEB58C00C6ADA5004AC6
      FF0052D6FF0042C6E70000000000000000000000000000000000000000000000
      000000000000007B080042DE7B0029CE5A0018B5390008A51800006300000000
      00000000000000000000000000000000000000000000CE9C8400FFFFFF009C31
      00005273FF001042FF00BDCEFF00EFF7FF001842FF004A73FF0094310000F7DE
      B500F7DEAD009C6B630000000000000000000000000000000000D6BDB500FFFF
      FF00FFFFF700FFF7EF00FFEFE700FFEFDE00FFE7D600F7E7CE00F7DEC600F7DE
      BD00F7DEB500F7DEAD00B58484000000000021ADE700AD736B005ABDDE00B5A5
      9C00F7F7EF00FFFFFF00FFFFE700FFFFD600FFF7C600FFE7A500DEA584006BD6
      EF005AD6FF006BE7FF000894CE00000000000000000000000000000000000000
      0000007B080052EF8C0039D6730029C6520018B5310008A50800009C00000063
      00000000000000000000000000000000000000000000CE9C8400FFFFFF009C31
      0000E7EFFF00DEE7FF00FFFFFF00FFFFFF009CADFF000031FF0063315A00F7DE
      BD00FFDEB5009C6B630000000000000000000000000000000000DEBDB500FFFF
      FF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEFDE00F7E7D600F7E7CE00F7DE
      C600F7DEBD00FFDEB500B58484000000000021ADE700B5736B00A5CEDE00CEB5
      9C00FFFFEF00FFFFF700FFFFEF00FFFFD600F7DEAD00F7CE8400E7BD940094C6
      C60052D6FF007BF7FF0029ADDE0000000000000000000000000000000000007B
      08006BFF9C0052EF8C0039D6730029C6520010B5290000A50800009C0000009C
      00000063000000000000000000000000000000000000DEAD8400FFFFFF009C31
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF005273FF000031FF00F7E7
      C600F7DEB5009C6B630000000000000000000000000000000000DEC6B500FFFF
      FF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEFDE00FFE7D600F7E7
      CE00F7E7C600F7DEB500B58484000000000029B5E700BD7B6B00FFF7EF00D6BD
      A500FFFFDE00FFFFDE00FFFFE700FFFFCE00EFBD8400F7BD7300E7C69C0094C6
      C6004AD6FF007BF7FF0052D6EF00000000000000000000000000007B080052E7
      840042D6730029B5520018A5390010942100088C08000084000000840000008C
      0000008C000000630000000000000000000000000000DEAD8400FFFFFF009C31
      00009C3100009C3100009C3100009C3100009C3100008C3110002131CE000031
      FF00C6BDAD009C6B630000000000000000000000000000000000E7C6B500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00FFEFE700FFEFDE00FFEF
      DE00E7DEC600C6BDAD00B58484000000000029ADE700C68C7300FFFFFF00CEAD
      9C00FFFFCE00FFFFCE00FFE7AD00F7C68C00F7CE9400FFEFB500D6AD94009CCE
      D6007BE7FF009CFFFF0094FFFF0021A5D60000000000007B0800088408002184
      000021840000008C100042AD520052DED60000D6DE00089C940031AD4A0031BD
      520029AD420008941800006300000000000000000000E7B58C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00F7E7D600B58473000031
      FF000031FF000031FF0000000000000000000000000000000000E7C6B500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFF700FFF7EF00F7E7D600C6A5
      9400B5948C00B58C8400B58484000000000029B5E700CE947300FFFFF700DEC6
      BD00DEC69C00FFE7A500FFC68400FFD68C00FFF7E700E7DECE008CA5AD006BCE
      E7005ABDE70063BDDE006BC6E70029A5D600007B0800007B00008CA50800F7B5
      3900EFAD210094A5100010BD390073DE9C0052F7FF0000E7FF0018ADB5005AE7
      8C0042E76B0094D6B500219C31000063000000000000E7B58C00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7CECE00B5847300EFB5
      7300EFA54A000031FF0000000000000000000000000000000000EFCEBD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7CECE00BD8C
      7300EFB57300EFA54A00C6846B000000000029B5E700D69C7300FFFFF700FFFF
      FF00E7D6CE00EFCEA500E7C69C00E7C69C00CEB59C00B58C8C0073E7FF006BEF
      FF00109CD60000000000000000000000000000000000007B0800E7BD7B00F7EF
      B500EFDE7B00DEA51000006B0800006B000063A56B0052F7FF00007B6B00005A
      00005A8C5A00FF7BF700522152000000000000000000EFBD9400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6D600B5847300FFC6
      7300CE9473000000000000000000000000000000000000000000EFCEBD00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00E7D6D600CE9C
      7B00FFC67300CE947300000000000000000018A5DE00DEA57300FFFFF700FFFF
      FF00FFFFFF00FFFFFF00FFF7EF00B5847B00BD7B4A00B56B520052C6E70052C6
      E700189CD6000000000000000000000000000000000000000000E7C6A500FFFF
      F700F7E7A500DEAD210000000000000000000000000000000000000000000000
      0000FFADFF00FF08FF00BD00BD009400940000000000EFBD9400FFF7F700FFF7
      F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700E7D6CE00B5847300CE9C
      8400000000000000000000000000000000000000000000000000EFCEB500FFF7
      F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700FFF7F700E7D6CE00C694
      7B00CE9C840000000000000000000000000000000000E7A57B00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00B5847B00E79C520000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D6B5
      8C00D6AD5A000000000000000000000000000000000000000000000000000000
      0000FFADFF00FF4AFF00A500A5000000000000000000EFBD9400DEA58400DEA5
      8400DEA58400DEA58400DEA58400DEA58400DEA58400DEA58400B58473000000
      0000000000000000000000000000000000000000000000000000EFC6B500EFCE
      BD00EFCEBD00EFCEBD00EFCEBD00EFCEBD00EFCEBD00EFCEBD00DEBDB500BD84
      7B000000000000000000000000000000000000000000E7AD7B00D6946B00D694
      6B00D6946B00D6946B00D6946B00B57B6B000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFADFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000063A5000084C600846363000000
      0000000000000000000000000000000000000000000042424200424242004242
      4200424242000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000008CBD00008CBD00008C
      BD00008CBD00008CBD00008CBD00008CBD00008CBD00008CBD00008CBD00008C
      BD00008CBD00008CBD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A5A5A500424242008484E700A5A5A500846363000000
      00000000000000000000000000000000000000000000C6630000C68442008463
      2100844221004242420042424200424242000000000000000000000000000000
      000000000000000000000000000000000000008CBD0063CEFF00008CBD00A5E7
      FF0063CEFF0063CEFF0063CEFF0063CEFF0063CEFF0063CEFF0063CEFF0063CE
      FF0039ADDE00ADE7F700008CBD00000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000848484000000
      00000000000000000000000000004242420000FFFF00A5A5A500000000000000
      00000000000000000000000000000000000000000000C6630000C6A58400F7CE
      A500C68442008463210084422100424242004242420042424200000000000000
      000000000000000000000000000000000000008CBD006BD6FF00008CBD00ADE7
      FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6FF006BD6
      FF0039B5DE00B5EFF700008CBD00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000000
      0000000000000000000000000000000000000000000084848400424242008463
      63008463630000000000000000004242420000FFFF00A5A5A500000000000000
      00000000000000000000000000000000000000000000C6630000F7CEA500FFFF
      FF00F7CEA500F7CEA500F7CEA500C68442008463210042422100424242000000
      000000000000000000000000000000000000008CBD0073D6FF00008CBD00ADEF
      FF007BDEFF007BDEFF007BDEFF007BDEFF007BDEFF007BDEFF007BDEFF007BDE
      FF0042B5DE00B5EFF700008CBD00000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000008484
      84004242420084636300000000004242420000FFFF00A5A5A500000000000000
      00000000000000000000000000000000000000000000C6630000F7CEA500F7FF
      FF00F7CEA500F7CEA500F7CEA500F7CEA500C6A5630084422100424242000000
      000000000000000000000000000000000000008CBD007BDEFF00008CBD00B5EF
      FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7FF0084E7
      FF004ABDDE00BDF7F700008CBD00000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000084636300846363000000
      00000000000000000000000000004242420000FFFF00A5A5A500848484000000
      00000000000000000000000000000000000000000000C6630000F7CEA500FFFF
      FF00C6DEC60000A5C600F7CEA500C6C66300C6C6840084422100424242000000
      000000000000000000000000000000000000008CBD0084E7FF00008CBD00BDF7
      FF008CEFFF008CEFFF008CEFFF008CEFFF008CEFFF008CEFFF008CEFFF008CEF
      FF004ABDDE00BDF7F700008CBD00000000000000000000000000000000000000
      000000000000000000000000000000FFFF0000FFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000A5A5A5008463
      63000000000084636300000000004242420000FFFF00C6C6C600848484004242
      42008484840084848400000000000000000000000000C6630000F7CEA500C6DE
      C60000A5C60000A5C600C6C6A50084A58400C6A5630084422100424242004242
      420000000000000000000000000000000000008CBD008CEFFF00008CBD00FFFF
      FF00CEF7FF00CEF7FF00CEF7FF00CEF7FF00CEF7FF00CEF7FF00CEF7FF00CEF7
      FF009CD6E700DEFFFF00008CBD00000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000000000000000000000000
      00008463630000000000000000004242420000FFFF00A5A5A500000000000000
      00000000000042424200848484000000000000000000C6630000F7FFFF0000A5
      C600C6DEC600C6DEC60000A5C600A5A5A500F7CEA500F7CEA500C68442008463
      420042424200424242000000000000000000008CBD0094F7FF00008CBD00008C
      BD00008CBD00008CBD00008CBD00008CBD00008CBD00008CBD00008CBD00008C
      BD00008CBD00008CBD00008CBD00000000000000000000000000000000000000
      0000FFFFFF0000FFFF0000FFFF0000FFFF00FFFFFF0000FFFF0000FFFF0000FF
      FF00000000000000000000000000000000000000000084636300426363008463
      63000000000000000000000000004242420000FFFF00A5A5A500A5A5A500A5A5
      A5000000000000000000000000000000000000000000C6630000F7FFFF00F7FF
      FF00FFFFFF00C6DEC60000A5C600F7CEA500C6A58400C6844200F7CEA500F7CE
      A500C6A56300426384004242420000000000008CBD009CF7FF009CF7FF009CF7
      FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7FF009CF7
      FF00088CBD000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF00FFFFFF0000FFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008463
      63008463630000000000000000004242420000FFFF00A5A5A500A5A5A5008463
      6300A5A5A50084636300000000000000000000000000C6630000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000A5C600F7FFFF00F7CEA5008442210042424200C6A5
      8400A5A5A5004263E7004263C60000000000008CBD00FFFFFF00A5FFFF00A5FF
      FF00A5FFFF00A5FFFF00A5FFFF00A5FFFF00A5FFFF00A5FFFF00A5FFFF00A5FF
      FF00088CBD000000000000000000000000000000000000000000000000000000
      00000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000084848400848484008484
      8400000000004221210042006300004284004284E7000084C600846363000000
      000000000000A5A5A500848484000000000000000000C6630000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00C6DEC600C6DEC600F7CEA50042422100424242000000
      00000000000000000000000000000000000000000000008CBD00FFFFFF00A5FF
      FF00A5FFFF00A5FFFF00008CBD00008CBD00008CBD00008CBD00008CBD00008C
      BD00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF000000
      0000000000000000000000000000000000000000000084848400846363008463
      6300424242000000000042212100A5A5A500A5A5A500A5A5A500422121008484
      84000000000000000000000000000000000000000000C6630000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00F7FFFF00C6DEC600F7CEA50042422100424242000000
      0000000000000000000000000000000000000000000000000000008CBD00008C
      BD00008CBD00008CBD0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000846363004263630042212100846363004221210000000000000000000000
      00008484840042424200000000000000000000000000C6630000C6844200C6A5
      6300C6C6A500F7CEA500F7FFFF00FFFFFF00F7CEA50042422100424242000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000FFFFFF0000FFFF00FFFFFF00FFFFFF0000FF
      FF00FFFFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000846363000000000000000000A5A5A5008484
      84000000000000000000000000000000000000000000C6842100C6630000C663
      0000C6630000C6630000C6632100C6844200C684420042424200424242000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A5A5A500846363004242
      4200A5A5A500A5A5A50000000000000000000000000000000000000000008463
      630084636300C6844200C6844200C6842100C663000084634200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000848484008463630084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000104A0000106300001873000018730000106300001052000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000104A0000106300001873000018730000106300001052000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000010
      63000010630000219C000021BD000021BD000021BD000021BD0000219C000010
      6B0000106B000000000000000000000000000000000000000000000000000010
      63000010630000219C000021BD000021BD000021BD000021BD0000219C000010
      6B0000106B000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000001873000018
      8C000029C6000029C6000021BD000021BD000021BD000021BD000029C6000029
      C60000188C000010520000000000000000000000000000000000001873000018
      8C000029C6000029C6000021BD000021BD000021BD000021BD000029C6000029
      C60000188C000010520000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      0000000000000000000000000000000000000000000000187300001894000029
      D6000029CE000021BD000021BD000021BD000021BD000021BD000021BD000021
      BD000029C60000188C0000106B00000000000000000000187300001894000029
      D6000029CE000021BD000021BD000021BD000021BD000021BD000021BD000021
      BD000029C60000188C0000106B00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008484
      8400840000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      00000000000000000000000000000000000000000000001873000029DE000029
      DE000029CE000021BD005A73D600EFF7FF00DEE7F7002142C6000021BD000021
      BD000021BD000029C60000106B000000000000000000001873000029DE000029
      DE000029CE000021BD002142C600DEE7F700EFF7FF005A73D6000021BD000021
      BD000021BD000029C60000106B00000000000000000000000000840000008400
      0000840000008400000084000000000000000000000000000000000000000000
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      00000000000000000000000000000000000000187B000021B5000031F7000029
      DE000029C600526BD600F7FFFF00FFFFFF0094A5E7000829BD000021BD000021
      BD000021BD000029C60000219C000010630000187B000021B5000031F7000029
      DE000029C6000021BD000829BD0094A5E700FFFFFF00F7FFFF00526BD6000021
      BD000021BD000029C60000219C00001063000000000000000000840000008400
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      00000000000000000000000000000000000000187B000029E7000031F7000029
      DE00526BD600F7F7FF00FFFFFF008C9CE7000021BD000021BD000021BD000021
      BD000021BD000021BD000021B5000010630000187B000029E7000031F7000029
      DE000029C6000021BD000021BD000021BD008C9CDE00FFFFFF00F7F7FF00526B
      D6000021BD000021BD000021B500001063000000000000000000840000008400
      0000840000000000000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      000084000000000000000000000000000000001894000031FF000031FF006B8C
      FF00F7F7FF00FFFFFF00FFFFFF00BDCEFF00BDC6FF00BDC6EF00BDC6EF00BDCE
      EF00C6CEEF000021BD000021BD0000106B00001894000031FF000031FF00C6CE
      FF00BDCEFF00BDCEF700BDCEFF00BDC6FF00BDCEFF00FFFFFF00FFFFFF00F7F7
      FF006B84D6000021BD000021BD0000106B000000000000000000840000008400
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000008400
      0000840000008400000084000000840000008400000084000000840000008400
      0000840000000000000000000000000000000021A5002152FF000839FF00BDCE
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF000021BD000021BD00001873000021A5002152FF000839FF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00BDC6EF000021BD000021BD00001873000000000000000000840000000000
      0000000000000000000084000000840000000000000000000000000000000000
      0000840000008484840000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      0000000000000000000000000000000000000021A500426BFF00315AFF001842
      F700A5B5FF00FFFFFF00FFFFFF00849CFF003963EF004A6BE7004A6BDE004A6B
      DE004A6BDE000029CE000021BD00001063000021A500426BFF00315AFF004A6B
      F7004A73FF004A6BFF004A6BFF003963FF008494F700FFFFFF00FFFFFF00A5B5
      EF001839D6000029CE000021BD00001063000000000000000000000000000000
      0000000000000000000000000000000000008400000084000000840000008400
      0000848484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      0000000000000000000000000000000000000021A500315AFF00849CFF000031
      FF000031F70094ADFF00FFFFFF00E7EFFF004A6BF7000029DE000029DE000029
      DE000029D6000029CE000021AD00001063000021A500315AFF00849CFF000031
      FF000031F7000031FF000031FF004A6BFF00E7EFFF00FFFFFF0094A5EF000029
      DE000029D6000029CE000021AD00001063000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      000000000000000000000000000000000000000000000031EF00ADBDFF005A7B
      FF000031EF000031F7008CA5FF00FFFFFF00F7F7FF00214AEF000029DE000029
      D6000029CE000029D60000188C0000000000000000000031EF00ADBDFF005A7B
      FF000031EF000031F700214AFF00F7F7FF00FFFFFF008CA5F7000029DE000029
      D6000029CE000029D60000188C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000840000008400000000000000000000000000
      000000000000000000000000000000000000000000000031EF00426BFF00C6D6
      FF005A7BFF000031FF000031EF0094ADFF00ADBDFF000839EF000029E7000029
      DE000029E7000029C60000188C0000000000000000000031EF00426BFF00C6D6
      FF005A7BFF000031FF000839EF00ADBDFF0094ADFF000031EF000029E7000029
      DE000029E7000029C60000188C00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000029D6005A7B
      FF00D6DEFF0094ADFF003963FF00184AFF000839FF001042FF001042FF000039
      FF000029D60000188C00000000000000000000000000000000000029D6005A7B
      FF00D6DEFF0094ADFF003963FF00184AFF000839FF001042FF001042FF000039
      FF000029D60000188C0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000315A
      FF00315AFF0094ADFF00BDCEFF00A5BDFF008CA5FF007394FF003963FF000029
      E7000029E700000000000000000000000000000000000000000000000000315A
      FF00315AFF0094ADFF00BDCEFF00A5BDFF008CA5FF007394FF003963FF000029
      E7000029E7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000031EF001042FF00295AFF002152FF000031FF000029C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000031EF001042FF00295AFF002152FF000031FF000029C6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C6C6C60000FFFF0000000000848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000008484840000848400C6C6
      C60000FFFF00FFFFFF0000000000848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF000000000000000000FFFFFF0000000000848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000848400008484000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084840000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084000000000000000000000000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000FFFF00000000000000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000000000FFFF0000FFFF00000000000084840000848400008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0000000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000848484000084840000FF
      FF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00008484000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00FFFFFF0084840000FFFF0000FFFF00008400000084840000848400008484
      0000848400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF0084840000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      8400008484000084840000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000084000000840000008400000084000000840000008400
      0000840000008400000084000000840000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000000000000000000000
      0000000000000000000084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00840000000000000000000000000000000000
      0000C6C6C60000FFFF0000000000848484008484840084848400848484008484
      8400848484008484840084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FF000000FF000000FF00
      0000FF000000FF000000FFFFFF00000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF008400000084000000840000008400
      00008400000084000000FFFFFF0084000000000000008484840000848400C6C6
      C60000FFFF00FFFFFF0000000000848484008484840084848400848484008484
      840084848400FFFF000084848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF008400000000000000848484000084840000FF
      FF00FFFFFF0000FFFF000000000000000000FFFFFF0000000000848484008484
      8400FFFF0000FFFF0000FFFF0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FF000000FF000000FF00
      0000FF000000FF000000FFFFFF00000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00840000008400000084000000FFFF
      FF008400000084000000840000008400000000000000848484000084840000FF
      FF0000FFFF00FFFFFF000000000000000000000000000000000000000000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FF00
      0000FF000000FF000000FF00000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00000000000000000000848400848484000084
      8400848484000084840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0084000000FFFFFF00840000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0000000000FFFFFF00FFFFFF00FFFFFF0084840000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FF000000FF000000FFFF
      FF00000000000000000000000000000000000000000084848400008484008484
      8400008484008484840084000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF008400000084000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF0000000000000000000000000000000000FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FF00
      0000FF000000FF000000FF00000000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF0000000000FFFFFF0000000000000000000000000000848400848484000084
      8400848484000084840084000000840000008400000084000000840000008400
      00008400000000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF00FFFFFF00FFFFFF0000FFFF0000000000000000008484
      0000FFFF0000FFFF000084840000848400000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000084848400008484008484
      8400008484008484840000848400848484000084840084848400008484008484
      84000084840000000000000000000000000000000000848484000084840000FF
      FF0000FFFF00FFFFFF00FFFFFF0000FFFF00FFFFFF0000000000000000008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FF00
      0000FF000000FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      84008484840000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084840000848400008484000084840000848400008484
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400848484000000
      0000000000000000000000000000000000000000000000000000000000008484
      84000084840000000000000000000000000000000000848484000084840000FF
      FF00FFFFFF0000FFFF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF0000FFFF000084840000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000848400848484000084
      84000000000000FFFF00000000000000000000FFFF0000000000848484000084
      840084848400000000000000000000000000000000008484840000848400FFFF
      FF0000FFFF00FFFFFF0084840000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFF00008484000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000084848400FFFFFF0000FF
      FF00FFFFFF00FFFFFF00FFFFFF00848400008400000084840000848400008484
      0000848400000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000084848400FFFF
      FF0000FFFF00FFFFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      8400008484000084840000848400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      0000000000000084840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      8400008484000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000084
      8400008484000084840000848400008484000084840000848400008484000084
      8400000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF000000
      0000008484000084840000848400008484000084840000848400008484000084
      8400008484000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000084840000848400008484000084840000848400008484000084
      8400008484000084840000000000000000000000000000000000008484000084
      8400008484000084840000848400008484000084840000848400008484000084
      840000848400008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000000000
      00008400000084000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000084
      8400000000000000000000000000000000000000000000000000000000000000
      000000848400008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000008400
      0000840000008400000084000000840000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000840000008400
      00008400000084000000840000008400000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      84000084840000000000FFFFFF00FFFFFF000000000000000000000000008400
      0000840000008400000084000000840000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000008484000084
      840000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000840000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000008484000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008400000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000FFFFFF000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008484000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000A00000000100010000000000000500000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000009FFF9FFF000000009FFF9FFF00000000
      9FFF9FFF000000009FFF9FFF000000009FFF9FFF000000009F839F8300000000
      8F008F0000000000800080000000000080008000000000008000800000000000
      8000800000000000800080000000000080008000000000008000800000000000
      803C803C000000009FFF9FFF000000009FFF9FFF9FFF9FFF00000FFF9FFF9FFF
      000007FF9FFF9FFF000083FF9FFF9FFF0000C1FF9FFF9FFF0000E10F9F839F83
      0000F0038F008F000000FC01800080000000FC01800080000000F80080008000
      0000F800800080000000F800800080000000FC01800080008001FC0180008000
      FFFFFE03803C803CFFFFFF8F9FFF9FFFFFFFFC1FFFFFF0F0FFFFF00F8003F0F0
      FFFFF00F8003F0F0E1CFF00F8003F090C0CFE01F8003F0009C4FC0078003E000
      9E67C0038003E0009E67C0038003E0608E63C0038003E0F0C0E0C0038003E0F8
      E1E4C0038003E0FFFFFFC0038003C07FFFFFC0038003843FFFFFC00380070E1F
      FFFFE003800F0E1FFFFFFE07801FFFFFFFFFC001FFFFFE7F8003C0010FF3FC3F
      8003C0010061FC3F8003C0010001FC3F8003C0010003FC3F8003C0010003FC3F
      8003C0010003F81F8003C0010001F00F8003C0010001E0078003C0010001C003
      8003C001000080018003C001000000008003C001000780018007C0030007C3F0
      800FC007807FE7F1801FC00F80FFFFFBFCFFFC1F87FF8003FE7FFC1F80FF0001
      FE3FDE3F803F0001F81F863F801F0001F80FE23F801F0001FC079E1F801F0001
      FC1FC203800F0001E00FF63980030001E0078E0F80010007F003E60380010007
      F01F8819801F800FF80F840F801FC3FFF807F073801FFFFFFC03FE0F801FFFFF
      FC01FF83E03FFFFFFFFFFFF1FFFFFFFFFFFFFFFFF81FF81FFFFFFFFFE007E007
      FFFFFC3FC003C003FFFFFC3F80018001FFE7FC3F80018001C1F3FC3F00000000
      C3FBC00300000000C7FBC00300000000CBFBC00300000000DCF3C00300000000
      FF07FC3F00000000FFFFFC3F80018001FFFFFC3F80018001FFFFFC3FC003C003
      FFFFFFFFE007E007FFFFFFFFF81FF81FFFFFFFFFFFFFFFFFF000FFFFFFFFFFFF
      C000FFFFFFFFFFFF8000FFFFFFFFFFFF8000FFFFF01FFEFF8000FDFFF01FFEFF
      8000FCFFF83FFC7F8000FC7FF83FFC7F8061FC3FFC7FF83F8061FC7FFC7FF83F
      8001FCFFFEFFF01F8001FDFFFEFFF01F8003FFFFFFFFFFFF8007FFFFFFFFFFFF
      C07FFFFFFFFFFFFFE07FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00FC00F000
      EFFDFE008000C000C7FFFE0000008000C3FB800000008000E3F7800000008000
      F1E7800000018000F8CF800000038000FC1F800100038020FE3F800300038021
      FC1F800700038001F8CF807F0FC38001E1E780FF00038003C3F381FF80078007
      C7FDFFFFF87FC07FFFFFFFFFFFFFE07FFBFFFFFFFFFFFFFFF3FFFFFFC001FFFF
      E3FF001F8031C007C3FF000F8031C007000700078031C007807F00038001C007
      807B00018001C007807300008001C0078060001F8FF1C0078040001F8FF1C007
      8060001F8FF1C00780738FF18FF1C007807BFFF98FF1C00F807FFF758FF5C01F
      807FFF8F8001C03FFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object mMainMenu: TmmMainMenu
    Images = mImageList
    Left = 800
    Top = 77
    object Datei1: TMenuItem
      Caption = 'Datei'
      object miOpen: TMenuItem
        Action = acOpen
      end
      object miSave: TMenuItem
        Action = acSave
      end
      object miExit: TMenuItem
        Action = acExit
      end
    end
    object Bearbeiten1: TMenuItem
      Caption = 'Bearbeiten'
      object miExport: TMenuItem
        Action = acExport
      end
      object miImport: TMenuItem
        Action = acImport
      end
    end
    object Millmaster1: TMenuItem
      Caption = 'Millmaster'
      object Millmastermapfreischalten1: TMenuItem
        Action = acEnableMillmasterMap
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object ffneExplorerMapfiles1: TMenuItem
        Action = acOpenMapExplorer
      end
    end
  end
  object mActionList: TmmActionList
    Images = mImageList
    OnUpdate = mActionListUpdate
    Left = 800
    Top = 112
    object acPreviousItem: TAction
      Category = 'Navigation'
      Caption = 'Vorheriges Element'
      Hint = 'Springt zum vorhergehenden Element'
      ImageIndex = 14
      ShortCut = 16422
      OnExecute = acPreviousItemExecute
    end
    object acEnumPoolNew: TAction
      Category = 'Enumeration'
      Hint = 'Neue Aufz�hlung erstellen'
      ImageIndex = 3
      OnExecute = acEnumPoolNewExecute
    end
    object acOpen: TAction
      Caption = '�ffnen'
      Hint = 'Schemadatei �ffnen'
      ImageIndex = 1
      OnExecute = acOpenExecute
    end
    object acEnumPoolDelete: TAction
      Category = 'Enumeration'
      Hint = 'Aktuelle Aufz�hlung l�schen'
      ImageIndex = 4
      OnExecute = acEnumPoolDeleteExecute
    end
    object acSave: TAction
      Caption = 'Speichern'
      Hint = 'Schemadatei speichern'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = acSaveExecute
    end
    object acExit: TAction
      Caption = 'Beenden'
      Hint = 'Schematool beenden'
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acExport: TAction
      Caption = 'Exportieren'
      Hint = 'Exportieren in eine Map-Datei'
      ImageIndex = 8
      ShortCut = 16453
      OnExecute = acExportExecute
    end
    object acImport: TAction
      Caption = 'Importieren'
      Hint = 'Map-Datei importieren'
      ImageIndex = 7
      OnExecute = acImportExecute
    end
    object acMapNew: TAction
      Hint = 'Eine neue Map erstellen'
      ImageIndex = 3
      OnExecute = acMapNewExecute
    end
    object acPasteActiveOnly: TAction
      Caption = 'Nur aktive Elemente'
      Hint = 
        'Kopiert f�r alle aktivenb Elemente die entsprechenden Elemente a' +
        'us dem angegebenen Map'
      ImageIndex = 6
      OnExecute = acElementPasteExecute
    end
    object acMapCopy: TAction
      Hint = 'Aktuelle Map in eine Neue kopieren'
      ImageIndex = 5
      OnExecute = acMapCopyExecute
    end
    object acMapDelete: TAction
      Hint = 'Aktuelle Map l�schen'
      ImageIndex = 4
      OnExecute = acMapDeleteExecute
    end
    object acElementCopy: TAction
      Hint = 'Elementdaten kopieren'
      ImageIndex = 5
      ShortCut = 49219
      OnExecute = acElementCopyExecute
    end
    object acElementPaste: TAction
      Hint = 'Elementdaten einf�gen'
      ImageIndex = 6
      ShortCut = 49238
      OnExecute = acElementPasteExecute
    end
    object acElementClear: TAction
      Hint = 'Elementdaten l�schen'
      ImageIndex = 4
      OnExecute = acElementClearExecute
    end
    object acEnumDelete: TAction
      Category = 'Enumeration'
      Hint = 'Aufz�hlungswert l�schen'
      ImageIndex = 4
      OnExecute = acEnumDeleteClick
    end
    object acEnumIterate: TAction
      Category = 'Enumeration'
      Caption = '123'
      Hint = 'Autmatisch Durchnummerieren'
      OnExecute = acEnumIterateExecute
    end
    object acEnumAdd: TAction
      Category = 'Enumeration'
      Hint = 'Aufz�hlungswert hinzuf�gen'
      ImageIndex = 9
      OnExecute = acEnumAddExecute
    end
    object acEnumUp: TAction
      Tag = -1
      Category = 'Enumeration'
      Hint = 'Aufz�hlungwert nach oben schieben'
      ImageIndex = 10
      OnExecute = acEnumUpDownExecute
    end
    object acEnumDown: TAction
      Tag = 1
      Category = 'Enumeration'
      Hint = 'Aufz�hlungwert nach unten schieben'
      ImageIndex = 11
      OnExecute = acEnumUpDownExecute
    end
    object acConvDelete: TAction
      Category = 'Enumeration'
      Hint = 'Konvertierung l�schen'
      ImageIndex = 4
      OnExecute = acConvDeleteExecute
    end
    object acConvAdd: TAction
      Category = 'Enumeration'
      Hint = 'Konvertierung hinzuf�gen'
      ImageIndex = 9
      OnExecute = acConvAddExecute
    end
    object acConvUp: TAction
      Tag = -1
      Category = 'Enumeration'
      Hint = 'Konvertierung nach oben schieben'
      ImageIndex = 10
      OnExecute = acConvUpDownExecute
    end
    object acConvDown: TAction
      Tag = 1
      Category = 'Enumeration'
      Hint = 'Konvertierung nach unten schieben'
      ImageIndex = 11
      OnExecute = acConvUpDownExecute
    end
    object acEnumSetDefault: TAction
      Category = 'Enumeration'
      Caption = 'Als Defaultwert'
      Hint = 'Aufz�hlungswert als Default setzen'
      OnExecute = acEnumSetDefaultExecute
    end
    object acEventNew: TAction
      Hint = 'Ereignis hinzuf�gen'
      ImageIndex = 13
      OnExecute = acEventNewExecute
    end
    object acEventDelete: TAction
      Hint = 'Ereignis l�schen'
      ImageIndex = 4
      OnExecute = acEventDeleteExecute
    end
    object acEventUp: TAction
      Tag = -1
      Hint = 'Ereignis nach oben schieben'
      ImageIndex = 10
      OnExecute = acEventUpDownExecute
    end
    object acEventDown: TAction
      Tag = 1
      Hint = 'Ereignis nach unten schieben'
      ImageIndex = 11
      OnExecute = acEventUpDownExecute
    end
    object acElementUndo: TAction
      Hint = 'Originalwerte wieder zur�cklesen'
      ImageIndex = 12
      OnExecute = acElementUndoExecute
    end
    object acFunctionShow: TAction
      Caption = 'Anzeigen'
      OnExecute = acFunctionShowExecute
    end
    object acNextItem: TAction
      Category = 'Navigation'
      Caption = 'N�chstes Element'
      Hint = 'Springt zum nachfolgenden Element'
      ImageIndex = 15
      ShortCut = 16424
      OnExecute = acNextItemExecute
    end
    object acChangeOffRec: TAction
      Category = 'Navigation'
      Hint = 
        '�ndert den Offset der nachfolgenden Elemente um den selben Betra' +
        'g'
      ImageIndex = 16
      OnExecute = acChangeOffRecExecute
    end
    object acEnableMillmasterMap: TAction
      Caption = 'Millmaster Map freischalten'
      OnExecute = acEnableMillmasterMapExecute
    end
    object acExportMMConst: TAction
      Caption = 'ExportMMConst'
      Hint = 'Exportiert die Konstantendatei f�r Delphi'
      ImageIndex = 17
      Visible = False
      OnExecute = acExportMMConstExecute
    end
    object acShowActiveElements: TAction
      Category = 'Navigation'
      Caption = 'Aktive Elemente einblenden'
      Checked = True
      Hint = 'Ein-/Ausblenden aller aktiven Elemente'
      ImageIndex = 20
      OnExecute = bShowAssignedElementsxClick
    end
    object acShowInactiveElements: TAction
      Category = 'Navigation'
      Caption = 'Inaktive Elemente einblenden'
      Checked = True
      Hint = 'Ein-/Ausblenden aller inaktiven Elemente'
      ImageIndex = 21
      OnExecute = bShowAssignedElementsxClick
    end
    object acCopyXPath: TAction
      Category = 'XPath'
      Caption = 'XPath kopieren'
      Hint = 
        'Kopiert den XPath in den Speicher der das aktuelle Element selek' +
        'tiert'
      OnExecute = acCopyXPathExecute
    end
    object acShowLastExportetMapfile: TAction
      Caption = 'acShowLastExportetMapfile'
      Hint = 'Zeigt das zuletzt exportierte Mapfile an'
      ImageIndex = 22
      OnExecute = acShowLastExportetMapfileExecute
    end
    object acAdvancedFilter: TAction
      Category = 'Navigation'
      Caption = 'Erweiterter Filter'
      Checked = True
      Hint = 'Erweiterter Filter'
      ImageIndex = 23
      OnExecute = acAdvancedFilterExecute
    end
    object acDeleteFilter: TAction
      Category = 'Navigation'
      Caption = 'Filter l�schen'
      ImageIndex = 4
      ShortCut = 49240
      OnExecute = acDeleteFilterExecute
    end
    object acShowOtherMap: TAction
      Category = 'Navigation'
      Caption = 'Map Informationen'
      Hint = 'Zeigt Informationen aus einem anderen Map an'
      ImageIndex = 25
      OnExecute = acShowOtherMapExecute
    end
    object acReloadCurrentSchema: TAction
      Category = 'Navigation'
      Caption = 'Schema neu laden'
      Hint = 'L�dt das Schema neu und selektiert wieder das aktuelle Element'
      ImageIndex = 26
      OnExecute = acReloadCurrentSchemaExecute
    end
    object acExportYM: TAction
      Caption = 'Export YarnMaster Map'
      Hint = 'Exportiert das Yarn Master Mapfile'
      ImageIndex = 27
      OnExecute = acExportYMExecute
    end
    object acCompareMapfiles: TAction
      Category = 'Navigation'
      Caption = 'Vergleichen'
      Hint = 'Erm�glicht den Vergleich von mehreren Mapfiles'
      ImageIndex = 28
      OnExecute = acCompareMapfilesExecute
    end
    object acFindCC: TAction
      Category = 'Navigation'
      Hint = 'Elemente nach Configcode suchen'
      ImageIndex = 29
      OnExecute = acFindCCExecute
    end
    object acFocusToFilter: TAction
      Category = 'Navigation'
      Caption = 'acFocusToFilter'
      ShortCut = 49222
      OnExecute = acFocusToFilterExecute
    end
    object acOpenMapExplorer: TAction
      Caption = '�ffne Explorer - Mapfiles'
      OnExecute = acOpenMapExplorerExecute
    end
  end
  object mOpenDialog: TmmOpenDialog
    Options = [ofFileMustExist, ofEnableSizing]
    Left = 800
    Top = 144
  end
  object pmTreeView: TmmPopupMenu
    Images = mImageList
    Left = 128
    Top = 325
    object XPathkopieren1: TMenuItem
      Action = acCopyXPath
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object AktiveElementeeinblenden1: TMenuItem
      Action = acShowActiveElements
    end
    object InaktiveElementeeinblenden1: TMenuItem
      Action = acShowInactiveElements
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object NchstesElement1: TMenuItem
      Action = acPreviousItem
    end
    object VorherigesElement1: TMenuItem
      Action = acNextItem
    end
  end
  object pmOpenFileMRU: TmmPopupMenu
    OnPopup = pmOpenFileMRUPopup
    Left = 272
    Top = 24
  end
  object mNodesToCollapseNames: TmmVCLStringList
    Strings.Strings = (
      'Extra'
      'Debug'
      'NotMM')
    Left = 488
    Top = 32
  end
end
