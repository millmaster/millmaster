unit FrameMemUsage;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  FrameBaseFunction, Grids, mmStringGrid, StdCtrls, mmLabel, ExtCtrls, XMLMapping,
  mmPanel, mmEdit, Buttons, mmSpeedButton, mmButton;

type
  TfraMemUsage = class(TfraBaseFunction)
    sgMemory: TmmStringGrid;
    mmPanel1: TmmPanel;
    mmLabel1: TmmLabel;
    laName: TmmLabel;
    mmLabel3: TmmLabel;
    laDatatype: TmmLabel;
    mmLabel5: TmmLabel;
    laCount: TmmLabel;
    mmLabel7: TmmLabel;
    laOffset: TmmLabel;
    edOffset: TmmEdit;
    mmLabel2: TmmLabel;
    bUndockFrame: TmmSpeedButton;
    procedure sgMemoryDblClick(Sender: TObject);
    procedure sgMemoryDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
    procedure sgMemoryClick(Sender: TObject);
    procedure edOffsetChange(Sender: TObject);
    procedure bUndockFrameClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
  private
    function SelectObject(AObject: Pointer): Boolean;
    procedure ReCalcRowHeightWith;
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  protected
    procedure SetActiveElement(const aElement: TBaseElement); override;
  public
    procedure Execute; override;
    procedure Init;
  published
  end;

var
  fraMemUsage: TfraMemUsage;

implementation
{$R *.DFM}
uses
  ComCtrls, XMLMappingDef, SchematoolGlobal;

const
  cMemWidth  = 32;
  cRowHeight = 16;

  cColor1    = clBlue;
  cColor2    = $00FF8000;
  cColor3    = $00C08000;
  cColor4    = clTeal;

  cStartByteCode = 0;
  cEndByteCode   = 1;
  cOneByteCode   = 2;
  cMultiByteCode = 3;
  cBitFieldCode  = 4;
  cClassByteCode = 5;

//:---------------------------------------------------------------------------
//:--- Class: TfraMemUsage
//:---------------------------------------------------------------------------
procedure TfraMemUsage.Execute;
var
  xLoepfeBody: TLoepfeBody;
  i: integer;
  //............................................................
  procedure ScanElement(aElement: TMMElement);
  var
    i: Integer;
    xStartByte, xEndByte: Integer;
    xRow, xCol: Integer;
    xIndex: Integer;
    xColor: Integer;
  begin
    if assigned(aElement) then begin
      if aElement.Active then begin
        xIndex := -1;
        xStartByte := aElement.Offset;
        // ist es ein Byte/Char Array?
        if (aElement.Datatype in [dtChar, dtUCChar]) or // bei Char und UCChar immer Count verwenden
           ((aElement.Datatype = dtByte) and (aElement.Count > 0)) then begin
          xEndByte := xStartByte + aElement.Count - 1;
        end
        else if aElement.Datatype = dtUCBit then begin
          xEndByte := xStartByte + (aElement.Count div 8) - 1;
          // Angefressenes Byte geh�rt nat�rlich auch noch dazu
          if (aElement.Count mod 8) > 0 then
            inc(xEndByte);
        end
        else begin
          // normaler Datentyp mit eventuell einzelnen Bitdefinitionen
          if (aElement.BitOffset <> '') and (aElement.BitCount <> '') then
            xIndex   := xStartByte + (StrToIntDef(aElement.BitOffset, 0) div 8);
          xEndByte := xStartByte + cBasicTypeDimensions[aElement.DataType] - 1;
        end;
        // Anzahl Bytes vom Datentyp durchz�hlen
        for i:=xStartByte to xEndByte do begin
          // Bitfeld
          if (aElement.BitOffset <> '') and (aElement.BitCount <> '') then
            xColor := cBitFieldCode
          else if aElement.Datatype in [dtUCBit, dtUCChar] then // Klassierfeld
            xColor := cClassByteCode
          else if (xEndByte - xStartByte) = 0 then // nur 1 Byte
            xColor := cOneByteCode
          else if i = xStartByte then              // StartByte
            xColor := cStartByteCode
          else if i = xEndByte then
            xColor := cEndByteCode
          else
            xColor := cMultiByteCode;

          xRow := (i div cMemWidth) + sgMemory.FixedRows;
          xCol := (i mod cMemWidth) + sgMemory.FixedCols;
          sgMemory.Cells[xCol, xRow]   := IntToStr(xColor);
          if (not Assigned(sgMemory.Objects[xCol, xRow])) AND ((xIndex = -1) or (xIndex = i)) then
            sgMemory.Objects[xCol, xRow] := aElement;
        end; // for i
      end; // if Active

      for i := 0 to aElement.MMElements.Count - 1 do
        ScanElement(aElement.MMElements[i]);
    end;// if assigned(aElement) then begin
  end;// procedure ScanElement(aElement: TMMElement);
  //............................................................
begin
  if Assigned(TreeView) then begin
    xLoepfeBody := LoepfeBody;
    if assigned(xLoepfeBody) then begin
      if xLoepfeBody.MMElements.Count > 0 then begin
        Init;
        for i := 0 to xLoepfeBody.MMElements.Count - 1 do
          ScanElement(xLoepfeBody.MMElements[i]);
        Invalidate;
      end; // if assigned(xLoepfeBody) then begin
    end;// if assigned(xLoefeBody) then begin
  end; // if Assigned
end; // TfraMemUsage.Execute

//:---------------------------------------------------------------------------
procedure TfraMemUsage.Init;
var
  i: Integer;
  xDataRowCount: Integer;
begin
  with sgMemory do begin
    // erste Spalte etwas breiter
    ColWidths[0]  := 32;
    // soviele Datenzeilen braucht es
    xDataRowCount := (LoepfeBody.StructSize div cMemWidth) + 1;
    RowCount      := xDataRowCount + 1;

    // Headerinfos setzen
    Rows[0].CommaText := ',0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31';
  end;

  for i:=0 to sgMemory.RowCount-sgMemory.FixedRows-1 do
    sgMemory.Cells[0, i+1] := IntToStr(i * cMemWidth);

    ReCalcRowHeightWith;
end; // TfraMemUsage.Init

procedure TfraMemUsage.SetActiveElement(const aElement: TBaseElement);
begin
  inherited SetActiveElement(aElement);
  sgMemory.Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TfraMemUsage.sgMemoryDblClick(Sender: TObject);
begin
  with sgMemory do
    SelectObject(Objects[Col, Row]);
end; // TfraMemUsage.sgMemoryDblClick

function TfraMemUsage.SelectObject(AObject: Pointer): Boolean;
var
{ TODO -oLok -cKonvertierung : Gruppe Kontrollieren (nach Doku 1 basiert) }{ TODO -oLok -cKonvertierung : Gruppe Kontrollieren (nach Doku 1 basiert) }  xActNode: TTreeNode;
begin
  result := false;
  if assigned(AObject) then begin
    // Vom ersten Knoten nach unten
    xActNode := TreeView.Items.GetFirstNode;
    while (assigned(xActNode)) and (not(result)) do begin
      // aktuellen Knoten selektiren, wenn auf das selbe MMElement wie vorher verwiesen wird
      if xActNode.Data = AObject then begin
        TreeView.Selected := xActNode;
        TreeView.Selected.MakeVisible;
        TreeView.TopItem  := xActNode;
        result := true;
      end;//
      // W�hlt den n�chsten Knoten (unabh�ng von der aktuellen Ebene)
      xActNode := xActNode.GetNext;
    end;// while (assigned(xActNode)) and (not(xFound)) do begin
  end;// if assigned(xSelectedObject) then begin;
end;

//:---------------------------------------------------------------------------
procedure TfraMemUsage.sgMemoryDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  xBottom: Integer;
  xRight: Integer;
  xLeft: Integer;
  xTop: Integer;
  xRect: TRect;
  //..............................................................
  procedure PaintCell(aRect: TRect; aColor: TColor);
  begin
    with sgMemory.Canvas do begin
      Brush.Color := aColor;
      FillRect(aRect);
    end;
  end;
  //..............................................................
begin
  with sgMemory, Canvas do begin
    if (ARow < FixedRows)then
      inherited
    else begin
      if ACol >= FixedCols then begin
        case StrToIntDef(Cells[ACol, ARow], Color) of
          cStartByteCode: begin
              // zuerst die linke H�lfte zeichnen
              xRect       := Rect;
              xRect.Right := xRect.Left + (xRect.Right - xRect.Left) div 2;
              PaintCell(xRect, cColor1);
              // dann die rechte H�lfte
              xRect.Left := xRect.Right;
              xRect.Right := Rect.Right + 1;
              PaintCell(xRect, cColor2);
            end;
          cEndByteCode: begin
              // zuerst die linke H�lfte zeichnen
              xRect       := Rect;
              xRect.Right := xRect.Left + (xRect.Right - xRect.Left) div 2;
              dec(xRect.Left);
              PaintCell(xRect, cColor2);
              // dann die rechte H�lfte
              xRect.Left := xRect.Right;
              xRect.Right := Rect.Right;
              PaintCell(xRect, cColor1);
            end;
          cOneByteCode: begin
              PaintCell(Rect, cColor1);
            end;
          cMultiByteCode: begin
              inc(Rect.Right);
              PaintCell(Rect, cColor2);
            end;
          cBitFieldCode: begin
              PaintCell(Rect, cColor3);
            end;
          cClassByteCode: begin
              PaintCell(Rect, cColor4);
            end;
        else
          PaintCell(Rect, Color);
        end;
        if (Objects[aCol, aRow] is TBaseElement) then begin
          if TBaseElement(Objects[aCol, aRow]) = FActiveElement then begin
            Brush.Color := clRed;
            with Rect do begin
              xTop    := Top + ((Bottom - Top) div 3);
              xLeft   := Left + ((Right - Left) div 3);
              xRight  := Right - ((Right - Left) div 3);
              xBottom := Bottom - ((Bottom - Top) div 3);

              Ellipse(xLeft, xTop, xRight, xBottom);
            end;
          end;// if TBaseElement(Objects[Row, Col]) = FActiveElement then begin
        end;// if (Objects[Row, Col] is TBaseElement) then begin
      end
      else begin
        FillRect(Rect);
        TextRect(Rect, Rect.Left, Rect.Top, Cells[ACol, ARow]);
      end;
    end;
  end;
end; // TfraMemUsage.sgMemoryDrawCell

procedure TfraMemUsage.sgMemoryClick(Sender: TObject);
var
  xElement: TMMElement;
begin
  with sgMemory do begin
    xElement := TMMElement(Objects[Col, Row]);
  end;

  if Assigned(xElement) then begin
    laName.Caption     := xElement.ElementName;
    laDatatype.Caption := cBasicTypeNames[xElement.DataType];
    laOffset.Value     := xElement.Offset;
    laCount.Value      := xElement.Count;
  end else begin
    laName.Caption     := '';
    laDatatype.Caption := '';
    laOffset.Value     := '';
    laCount.Value      := '';
  end;
end;

procedure TfraMemUsage.edOffsetChange(Sender: TObject);
var
  xCol: Integer;
  xRow: Integer;
begin
  with sgMemory do begin
    xCol := FixedCols + (edOffset.AsInteger mod (ColCount - FixedCols));
    xRow := FixedRows + edOffset.AsInteger div (ColCount - FixedCols);
    SelectObject(Objects[xCol, xRow]);
  end;// with sgMemory do
end;

procedure TfraMemUsage.bUndockFrameClick(Sender: TObject);
var
  xForm: TForm;
begin
  xForm := TForm.Create(Application);
  Parent := xForm;
  Align := alClient;
  xForm.OnCloseQuery := FormCloseQuery;
  xForm.OnResize := FrameResize;
  xForm.Show;
  TSchematoolSettings.Instance.LoadForm(xForm, self.Name);
end;

procedure TfraMemUsage.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if (Sender is TForm) then
    TSchematoolSettings.Instance.SaveForm(TForm(Sender), self.Name);
end;

procedure TfraMemUsage.ReCalcRowHeightWith;
var
  xRowHeight: integer;
  i: Integer;
  xColWidth: integer;
begin
  // H�he der Datenzeilen so anpassen dass kein Scrollbar n�tig ist
  xColWidth    := (sgMemory.ClientWidth - sgMemory.DefaultColWidth) div (sgMemory.ColCount - 1);
  for i := sgMemory.FixedCols to sgMemory.ColCount-1 do
    sgMemory.ColWidths[i] := xColWidth - 1;

  // H�he der Datenzeilen so anpassen dass kein Scrollbar n�tig ist
  xRowHeight    := (sgMemory.ClientHeight - sgMemory.DefaultRowHeight) div (sgMemory.RowCount - 1);
  for i := sgMemory.FixedRows to sgMemory.RowCount-1 do begin
    if xRowHeight > 5 then
      sgMemory.RowHeights[i] := xRowHeight - 1
    else
      sgMemory.RowHeights[i] := xRowHeight;
  end;
end;

procedure TfraMemUsage.FrameResize(Sender: TObject);
begin
  ReCalcRowHeightWith;
end;

end.


