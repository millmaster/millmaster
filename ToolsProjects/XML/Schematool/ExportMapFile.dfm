object frmExportMapFile: TfrmExportMapFile
  Left = 591
  Top = 77
  Width = 351
  Height = 439
  ActiveControl = butOk
  Caption = 'frmExportMapFile'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 370
    Width = 343
    Height = 42
    Align = alBottom
    BevelOuter = bvNone
    Caption = ' '
    TabOrder = 0
    object Panel2: TPanel
      Left = 120
      Top = 0
      Width = 223
      Height = 42
      Align = alRight
      BevelOuter = bvNone
      Caption = ' '
      TabOrder = 0
      object butCancel: TBitBtn
        Tag = 74
        Left = 128
        Top = 8
        Width = 91
        Height = 25
        Cancel = True
        Caption = '&Abbruch'
        ModalResult = 2
        TabOrder = 0
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000130B0000130B00001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          3333333333333333333F3933333333333993338FF33333333388339933333333
          393333388F33333333833399933333339933333888F333333883333999333339
          93333333888F33338833333399933399933333333888F3388833333399933999
          333333333888FF88833333333999999333333333338888883333333333999933
          33333333333888833333333333999933333333333338888FF333333339999993
          3333333333888888FF33333399993999933333333888838888F3333999933399
          9933333388883338888F33999333333399933338883333333888399933333333
          3393338883333333333899333333333333333883333333333333}
        NumGlyphs = 2
      end
      object butOk: TBitBtn
        Tag = 100
        Left = 32
        Top = 8
        Width = 91
        Height = 25
        Caption = '&OK'
        Default = True
        ModalResult = 1
        TabOrder = 1
        Glyph.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          0400000000000001000000000000000000001000000000000000000000000000
          80000080000000808000800000008000800080800000C0C0C000808080000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00555555555555
          555555555555555555555555555555555555555555FF55555555555552055555
          55555555588FF5555555555522205555555555558888F5555555555522205555
          555555558888FF5555555552222205555555555888888F555555552222220555
          5555558888888FF5555558220522205555555888858888F55555820555522055
          55558885555888FF5555555555522205555555555558888F5555555555552205
          555555555555888FF5555555555552205555555555555888FF55555555555582
          05555555555555888FF5555555555558205555555555555888FF555555555555
          5220555555555555588855555555555555555555555555555555}
        NumGlyphs = 2
      end
    end
  end
  object vtExistingMaps: TVirtualStringTree
    Left = 0
    Top = 177
    Width = 343
    Height = 193
    Align = alClient
    Enabled = False
    Header.AutoSizeIndex = 0
    Header.Font.Charset = DEFAULT_CHARSET
    Header.Font.Color = clWindowText
    Header.Font.Height = -11
    Header.Font.Name = 'MS Sans Serif'
    Header.Font.Style = []
    Header.Options = [hoColumnResize, hoDrag, hoVisible]
    RootNodeCount = 3
    TabOrder = 1
    TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
    TreeOptions.PaintOptions = [toHideFocusRect, toShowHorzGridLines, toShowRoot, toShowVertGridLines, toThemeAware, toUseBlendedImages]
    TreeOptions.SelectionOptions = [toFullRowSelect, toRightClickSelect]
    OnChange = vtExistingMapsChange
    OnGetText = vtExistingMapsGetText
    Columns = <
      item
        Position = 0
        Width = 40
        WideText = 'ID'
      end
      item
        Position = 1
        Width = 157
        WideText = 'Name'
      end>
  end
  object pa1: TmmPanel
    Left = 0
    Top = 0
    Width = 343
    Height = 177
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object mmLabel3: TmmLabel
      Left = 13
      Top = 132
      Width = 108
      Height = 13
      Caption = 'Name (Max 8 Zeichen)'
      FocusControl = edMapName
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object rbNamePrefix: TmmRadioGroup
      Left = 14
      Top = 8
      Width = 81
      Height = 113
      Caption = 'Ziel'
      ItemIndex = 0
      Items.Strings = (
        'ZE'
        'WSC'
        'LZE'
        'Test')
      TabOrder = 0
      OnClick = rbNamePrefixClick
      CaptionSpace = True
    end
    object edMapName: TmmEdit
      Left = 13
      Top = 147
      Width = 108
      Height = 21
      Color = clWindow
      TabOrder = 1
      Visible = True
      OnChange = edMapNameChange
      AutoLabel.Control = mmLabel3
      AutoLabel.LabelPosition = lpTop
      Decimals = -1
      NumMode = False
      ReadOnlyColor = clBtnFace
      ShowMode = smExtended
      ValidateMode = [vmInput]
    end
    object cbSaveToDB: TmmCheckBox
      Left = 136
      Top = 136
      Width = 161
      Height = 17
      Caption = 'In DB Speichern (CTRL + D)'
      Enabled = False
      TabOrder = 2
      Visible = True
      OnClick = cbSaveToDBClick
      AutoLabel.LabelPosition = lpLeft
    end
    object cbFileCompressed: TmmCheckBox
      Left = 136
      Top = 152
      Width = 161
      Height = 17
      Caption = 'Datei komprimieren (*.MMZ)'
      TabOrder = 3
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object vstExportMap: TmmVirtualStringTree
      Left = 104
      Top = 13
      Width = 225
      Height = 108
      Header.AutoSizeIndex = 0
      Header.Font.Charset = DEFAULT_CHARSET
      Header.Font.Color = clWindowText
      Header.Font.Height = -11
      Header.Font.Name = 'MS Sans Serif'
      Header.Font.Style = []
      Header.Options = [hoAutoResize, hoDrag, hoVisible]
      Indent = 0
      TabOrder = 4
      TreeOptions.AutoOptions = [toAutoDropExpand, toAutoExpand, toAutoScrollOnExpand, toAutoSpanColumns, toAutoTristateTracking, toAutoDeleteMovedNodes]
      TreeOptions.PaintOptions = [toHideFocusRect, toShowButtons, toShowRoot, toThemeAware, toUseBlendedImages]
      TreeOptions.SelectionOptions = [toFullRowSelect]
      OnChange = vstExportMapChange
      OnClick = vstExportMapClick
      OnCreateEditor = vstExportMapCreateEditor
      OnEdited = vstExportMapEdited
      OnEditing = vstExportMapEditing
      OnGetText = vstExportMapGetText
      Columns = <
        item
          Position = 0
          Width = 121
          WideText = 'Typ'
        end
        item
          Position = 1
          Width = 100
          WideText = 'Mapfile'
        end>
    end
  end
  object mmActionList1: TmmActionList
    Left = 272
    Top = 200
    object acToggleSaveInDB: TAction
      Caption = 'Toggle "In DB speichern"'
      ShortCut = 16452
      OnExecute = acToggleSaveInDBExecute
    end
  end
end
