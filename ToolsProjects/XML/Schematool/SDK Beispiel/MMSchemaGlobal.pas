unit MMSchemaGlobal;

interface

uses
  MSXML2_TLB, sysutils, windows, classes;

  Function printTab(numTabs: integer): string;
  Function printParticles(oParticle: ISchemaElement): string;
  Function printRemark(r: string): string;
  Function printRestrictions(oType: ISchemaType; numTabs: integer): string;
  Function processChoiceOrSequence(poGroup: ISchemaModelGroup; numTabs: integer): string;
  Function processGroup(poGroup: ISchemaModelGroup; numTabs: integer): string;
  Function printAttr(oAttr: ISchemaAttribute; numTabs: integer): string;
  Function processComplexType(oComplex: ISchemaItem; numTabs: integer): string;
  Function processSimpleType(oSimple: ISchemaType; numTabs: integer): string;
  function printElement(oElement: ISchemaElement; numTabs: integer):string;
  function processType(aType: ISchemaItem; aNumTabs: integer): string;

  function IsInterface(aInterface: IUnknown; aIID: TGUID): boolean;

const
  cr   = #13;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;
  vbNewLine = crlf;

  SOMITEM_SCHEMA = 4 * 1024;
  SOMITEM_ATTRIBUTE = SOMITEM_SCHEMA + 1;
  SOMITEM_ATTRIBUTEGROUP = SOMITEM_SCHEMA + 2;
  SOMITEM_NOTATION = SOMITEM_SCHEMA + 3;

  SOMITEM_ANYTYPE = 8 * 1024;
  SOMITEM_DATATYPE = SOMITEM_ANYTYPE + 256;
  SOMITEM_SIMPLETYPE = SOMITEM_DATATYPE + 256;
  SOMITEM_COMPLEXTYPE = 9 * 1024;

  SOMITEM_PARTICLE = 16 * 1024;
  SOMITEM_ANY = SOMITEM_PARTICLE + 1;
  SOMITEM_ANYATTRIBUTE = SOMITEM_PARTICLE + 2 ;
  SOMITEM_ELEMENT = SOMITEM_PARTICLE + 3;
  SOMITEM_GROUP = SOMITEM_PARTICLE + 256;

  SOMITEM_ALL = SOMITEM_GROUP + 1;
  SOMITEM_CHOICE = SOMITEM_GROUP + 2;
  SOMITEM_SEQUENCE = SOMITEM_GROUP + 3;
  SOMITEM_EMPTYPARTICLE = SOMITEM_GROUP + 4;

  SCHEMAUSE_OPTIONAL = 0;
  SCHEMAUSE_PROHIBITED = 1;
  SCHEMAUSE_REQUIRED = 2;

  SCHEMACONTENTTYPE_EMPTY = 0;
  SCHEMACONTENTTYPE_TEXTONLY = 1;
  SCHEMACONTENTTYPE_ELEMENTONLY = 2;
  SCHEMACONTENTTYPE_MIXED = 3;

implementation

uses
  dialogs;
  Function printTab(numTabs: integer): string;
  var
    x: Integer;
  begin
    result := '';
    For x := 0 To numTabs do
      result := result + '   ';
  end;

  Function printParticles(oParticle: ISchemaElement): string;
  begin
    If oParticle.minOccurs <> 1 Then
        result := result + ' minOccurs=''' + oParticle.minOccurs + '''';

    If oParticle.maxOccurs <> 1 Then  begin
        If oParticle.maxOccurs = -1 Then
            result := result + ' maxOccurs=''unbounded'''
        Else
            result := result + ' maxOccurs=''' + oParticle.maxOccurs + '''';
    End;
  End;

  Function printRemark(r: string): string;
  begin
    result := '<!-- ' + r + ' -->' + crlf;
  End;

  Function printName(item: ISchemaItem): string;
  begin
    result := '';
    If (item.itemType And SOMITEM_DATATYPE) = SOMITEM_DATATYPE Then
        result := 'xsd:';

    If item.itemType = SOMITEM_ANYTYPE Then
        result := 'xsd:';

    result := result + item.Name;
  End;


  Function printRestrictions(oType: ISchemaType; numTabs: integer): string;
  var
    oItem: ISchemaItem;
    strPattern: Variant;
    strItem: String;
    i: integer;
    str1: string;
  begin
    result := '';
    If oType.minExclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:minExclusive value=''' + oType.minExclusive + '''/>' + vbNewLine;

    If oType.minInclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:minInclusive value=''' + oType.minInclusive + '''/>' + vbNewLine;

    If oType.maxExclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:maxExclusive value=''' + oType.maxExclusive + '''/>' + vbNewLine;

    If oType.maxInclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:maxInclusive value=''' + oType.maxInclusive + '''/>' + vbNewLine;

    If oType.totalDigits > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:totalDigits value=''' + oType.totalDigits + '''/>' + vbNewLine;

    If oType.fractionDigits > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:fractionDigits value=''' + oType.fractionDigits + '''/>' + vbNewLine;

    If oType.length > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:length value=''' + oType.length + '''/>' + vbNewLine;

    If oType.minLength > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:minLength value=''' + oType.minLength + '''/>' + vbNewLine;

    If oType.MaxLength > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:maxLength value=''' + oType.MaxLength + '''/>' + vbNewLine;

    If oType.enumeration.length > 0 Then begin
      for i := 0 to oType.enumeration.length - 1 do
        result := result + printTab(numTabs + 1) + '<xsd:enumeration value=''' + oType.enumeration.Item[i] + '''/>' + vbNewLine;
    End ;

    If oType.whitespace > 0 Then
        result := result + printTab(numTabs + 1) + '<xsd:whitespace value=''' + IntToStr(oType.whitespace) + '''/>' + vbNewLine;

    If oType.patterns.length <> 0 Then begin
      for i := 0 to oType.patterns.length - 1 do
         result := result + printTab(numTabs + 1) + '<xsd:pattern value=''' + oType.patterns.Item[i] + '''/>' + vbNewLine;
    End;

    str1 := '';
    If (result <> '') And (oType.baseTypes.length > 0) Then begin
      Str1 := printTab(numTabs) + '<xsd:restriction base=''' + printName(oType.baseTypes[0]) + '''>' + vbNewLine;
      Str1 := Str1 + result;
      result := Str1 + printTab(numTabs) + '</xsd:restriction>' + vbNewLine;
    End;
  End;

  Function processChoiceOrSequence(poGroup: ISchemaModelGroup; numTabs: integer): string;
  var
    item: ISchemaParticle;
    i: integer;
  begin
    result := '';
    for i := 0 to poGroup.particles.length - 1 do begin
      If poGroup.particles.item[i].itemType = SOMITEM_ELEMENT Then
        result := result + printElement(poGroup.particles.item[i] as ISchemaElement, numTabs + 1);

      If (poGroup.particles.item[i].itemType And SOMITEM_GROUP) = SOMITEM_GROUP Then
        result := result + processGroup(poGroup.particles.item[i] as ISchemaModelGroup, numTabs + 1) + vbNewLine;

      If poGroup.particles.item[i].itemType = SOMITEM_ANY Then
        result := result + 'any: ' + poGroup.particles.item[i].Name + vbNewLine;
    end;// for i := 0 to poGroup.particles.length - 1 do begin
  End;


  Function processGroup(poGroup: ISchemaModelGroup; numTabs: integer): string;
  begin
    result := '';
    // List elements in the sequence.

    If poGroup.itemType = SOMITEM_ALL Then begin
      result := result + printTab(numTabs + 1) + '<xsd:all>' + vbNewLine ;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:all>' + vbNewLine;
    End;

    If poGroup.itemType = SOMITEM_CHOICE Then begin
      result := result + printTab(numTabs + 1) + '<xsd:choice>' + vbNewLine;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:choice>' + vbNewLine;
    end;

    If poGroup.itemType = SOMITEM_SEQUENCE Then begin
      result := result + printTab(numTabs + 1) + '<xsd:sequence>' + vbNewLine;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:sequence>' + vbNewLine ;
    End;
  End;


  Function printAttr(oAttr: ISchemaAttribute; numTabs: integer): string;
  var
    strRem: String;
  begin
    If oAttr.isReference Then
        result := result + printTab(numTabs) + '<xsd:attribute ref='' + oAttr.Name + '''
    Else
        result := result + printTab(numTabs) + '<xsd:attribute name='' + oAttr.Name + ''';

    If oAttr.Type_.Name <> '' Then
        result := result + ' type='' + printName(oAttr.Type) + ''';

    If oAttr.defaultValue <> '' Then
        result := result + ' default='' + oAttr.defaultValue + ''';

    If oAttr.fixedValue <> '' Then
        result := result + ' fixed='' + oAttr.fixedValue + ''';

    If oAttr.use = SCHEMAUSE_OPTIONAL Then
      result := result + ' use=''optional''';
    If oAttr.use = SCHEMAUSE_PROHIBITED Then
      result := result + ' use=''prohibited''';
    If oAttr.use = SCHEMAUSE_REQUIRED Then
      result := result + ' use=''required''';

    result := result + '/>' + vbNewLine;
    If assigned(oAttr.scope) Then begin
        strRem := 'scope:' + printName(oAttr.scope);
        result := result + printTab(numTabs) + printRemark(strRem);
    End;
    //strRem = 'scope:' + printName(oElement.scope)
  End;

  Function processComplexType(oComplex: ISchemaItem; numTabs: integer): string;
  var
    strAny: String;
    oAttr: ISchemaAttribute;
    oComplexCast: ISchemaComplexType;
    i: integer;
  begin
    result := '';
    if IsInterface(oComplex, ISchemaComplexType) then begin
      oComplexCast := oComplex as ISchemaComplexType;
      result := printTab(numTabs) + '<xsd:complexType';
      If oComplexCast.Name <> '' Then
          result := result + ' name=''' + oComplexCast.Name + '''';

      result := result + '>' + vbNewLine;

      If oComplexCast.contentType = SCHEMACONTENTTYPE_EMPTY Then
          result := result + printTab(numTabs) + printRemark('emtpy');

      If oComplexCast.contentType = SCHEMACONTENTTYPE_TEXTONLY Then
          result := result + printTab(numTabs) + printRemark('textonly');

      If oComplexCast.contentType = SCHEMACONTENTTYPE_ELEMENTONLY Then begin
          result := result + printTab(numTabs) + printRemark('elementonly');
          result := result + processGroup(oComplexCast.contentModel, numTabs + 1);
      End;

      If oComplexCast.contentType = SCHEMACONTENTTYPE_MIXED Then begin
          result := result + printTab(numTabs) + printRemark('mixed');
          result := result + processGroup(oComplexCast.contentModel, numTabs + 1);
      End;
      result := result + vbNewLine;
      result := result + printRestrictions(oComplexCast, numTabs + 1);

      try
        strAny := oComplexCast.anyAttribute.Name;
        result := result + oComplexCast.anyAttribute.Name;
      except
      End;

      for i := 0 to oComplexCast.Attributes.length - 1 do
        result := result + printAttr(oComplexCast.Attributes.item[i] as ISchemaAttribute, numTabs + 1);

      result := result + printTab(numTabs) + '</xsd:complexType>' + vbNewLine;
    end;// if IsInterface(oComplex, ISchemaComplexType) then begin
  End;



  Function processSimpleType(oSimple: ISchemaType; numTabs: integer): string;
  var
    oType: ISchemaType;
    i: integer;
  begin
    result := printTab(numTabs) + '<xsd:simpleType';
    If oSimple.Name <> '' Then
      result := result + ' name=''' + oSimple.Name + '''';

    result := result + '>' + vbNewLine;

    If oSimple.baseTypes.length = 1 Then begin
        result := result + printRestrictions(oSimple, numTabs + 1);
    end Else begin
      for i := 0 to oSimple.baseTypes.length - 1 do
        result := result + '<baseType name=''' + printName(oSimple.baseTypes.item[i]) + '''>' + vbNewLine;
    End;
    result := result + printTab(numTabs) + '</xsd:simpleType>' + vbNewLine;
  End;


  function printElement(oElement: ISchemaElement; numTabs: integer):string;
  var
    res: String;
    strRem: String;
    oType: ISchemaType;
  begin
    result := printTab(numTabs) + '<xsd:element ';

    If oElement.isReference Then begin
        result := result + 'ref=''' + oElement.Name + '''' + printParticles(oElement) + '>';
        result := result + '<!-- ';
        result := result + ' abstract=''' + IntToStr(Word(oElement.isAbstract)) + '''';
        result := result + ' -->' + vbNewLine;
    end Else begin
        oType := oElement.Type_;
        result := result + 'name=''' + oElement.Name + '''' + printParticles(oElement);
        result := result + ' abstract=''' + IntToStr(Word(oElement.isAbstract)) + '''';
        result := result + ' id=''' + oElement.id + '''';
        If oType.Name = '' Then begin
            result := result + ' >' + vbNewLine;
            If oType.itemType = SOMITEM_COMPLEXTYPE Then
                result := result + result + processComplexType(oType, numTabs + 1)
            Else
                result := result + processSimpleType(oType, numTabs);
            result := result + printTab(numTabs) + '</xsd:element>' + vbNewLine;
        end Else begin
            If printName(oType) <> 'xsd:anyType' Then
                result := result + ' type=''' + printName(oType) + '''';

            If oType.itemType <> SOMITEM_COMPLEXTYPE Then begin
                If printRestrictions(oType, 0) = '' Then begin
                    result := result + '/>' + vbNewLine;
                end Else begin
                    result := result + '>' + vbNewLine + processSimpleType(oType, numTabs);
                    result := result + printTab(numTabs) + '</xsd:element>'
                end;
            end Else begin
                result := result + '/>' + vbNewLine;
            End;
        End;
    End;
    If assigned(oElement.scope)Then begin
        strRem := 'scope:' + printName(oElement.scope);
        result := result + printTab(numTabs) + printRemark(strRem);
    End;
  end;//

  function processType(aType: ISchemaItem; aNumTabs: integer): string;
  begin
{    If oType.itemType = SOMITEM_ANYTYPE Then
      result := result + printTab(numTabs + 1) + '<!-- ' + oType.Name + ' -->' + vbNewLine;
}
    If aType.itemType = SOMITEM_COMPLEXTYPE Then
      result := result + processComplexType(aType, aNumTabs + 1);
{
    If oType.itemType = SOMITEM_SIMPLETYPE Then
      result := result + processSimpleType(oType as ISchemaType, numTabs + 1);}

    result := result + crlf;
  End;

  function IsInterface(aInterface: IUnknown; aIID: TGUID): boolean;
  var
    xPointer: Pointer;
  begin
    result := aInterface.QueryInterface(aIID, xPointer) = S_OK
  end;
end.
