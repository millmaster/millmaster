unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmMemo, ExtCtrls, mmPanel, mmEdit, mmLabel,
  mmSplitter;

type
  TForm1 = class(TForm)
    memoOutput: TmmMemo;
    mmPanel1: TmmPanel;
    mmSplitter1: TmmSplitter;
    memoDetail: TmmMemo;
    mmPanel2: TmmPanel;
    mmButton1: TmmButton;
    bReadSchemaVB: TmmButton;
    edReadSchema: TmmEdit;
    mmLabel1: TmmLabel;
    edNamespace: TmmEdit;
    laDetailLevel: TmmLabel;
    bWeiter: TmmButton;
    bAppinfo: TmmButton;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    edTyp: TmmEdit;
    Item: TmmLabel;
    edItem: TmmEdit;
    bReadSchemaLok: TmmButton;
    mmLabel4: TmmLabel;
    procedure mmButton1Click(Sender: TObject);
    procedure bReadSchemaVBClick(Sender: TObject);
    procedure bWeiterClick(Sender: TObject);
    procedure bAppinfoClick(Sender: TObject);
    procedure bReadSchemaLokClick(Sender: TObject);
  private
    procedure writeDetail(aDetailName, aText: string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

uses
  MSXML2_TLB, MMSchemaGlobal;

{$R *.DFM}

const
  cr   = #13;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;

var
  gWeiter: boolean;

procedure TForm1.bAppinfoClick(Sender: TObject);
var
  xSchemaCache: IXMLDOMSchemaCollection2;
  xAnnotationDoc: DOMDocument40;
  xSchema: ISchema;
  xNamespace: WideString;
  xType: ISchemaItem;
  xCompType: ISchemaComplexType;
begin
  // Namespace festlegen
  xNamespace := edNamespace.Text;
//  xNamespace := 'http://www.loepfe.com/schema';

  // Schemacache erzeugen in den dann das Schema f�r den entsprechenden Namespace geladen wird
  xSchemaCache := CoXMLSchemaCache40.Create;

  // Pr�ft das Schema auf Fehler.
  xSchemaCache.validateOnLoad := true;

  // Das Schema f�r den entsprechenden Namespace laden
  xSchemaCache.add(xNamespace, edReadSchema.Text);
//  xSchemaCache.add(xNamespace, 'D:\Data\Delphi\Test\XML\Schema\SDK_Beispiele\po3.xsd');

  // Referenz auf das Schema holen
  xSchema := xSchemaCache.getSchema(xNamespace);
  if assigned(xSchema) then begin
    // Ausgabe Dokument erzeugen
    xAnnotationDoc  := CoDOMDocument40.Create;

    // Zuerst den Typ suchen
    xCompType := xSchema.types.itemByName(edTyp.Text) as ISchemaComplexType;
//    xAnnotationDoc.documentElement := xAnnotationDoc.createElement('root');
//    xCompType.writeAnnotation(xAnnotationDoc.documentElement);

    // Dann den WErt im Typ
    xType := xCompType.contentModel.particles.itemByName(edItem.Text);
    // Erzeugt das Ausgabeformat und das DocumentElement
    xAnnotationDoc.documentElement := xAnnotationDoc.createElement('root');
    // Schreibt die Appinfo zum Element
    xType.writeAnnotation(xAnnotationDoc.documentElement);

    // Zeigt den entstandenen XML Stream an
    ShowMessage(xAnnotationDoc.documentElement.xml);
  end;// if assigned(xSchema) then begin
end;

procedure TForm1.mmButton1Click(Sender: TObject);
var
  xSchemaCache: IXMLDOMSchemaCollection2;
  xSchema: ISchema;
  xSchemaItem: ISchemaItem;

  xNamespace: WideString;
  i: integer;
  xElementCount: integer;
  xMsg: string;
  xTypeName: string;

  function PrintElement(aName: string; aSchemaElement: ISchemaElement): string;
  var
    xSchemaType: ISchemaType;
    i: integer;
    xSchemaItem: ISchemaItem;
  begin
    result := crlf + aName + crlf;

    xSchemaType := aSchemaElement.scope;
    if assigned(xSchemaType) then begin
//      result := Format(result + '%s : = %s%s', ['defaultValue', aSchemaType.defaultValue, crlf]);
      result := Format(result + '%s : = %s%s', ['minExclusive', xSchemaType.minExclusive, crlf]);
      result := Format(result + '%s : = %s%s', ['minInclusive', xSchemaType.minInclusive, crlf]);
      result := Format(result + '%s : = %s%s', ['maxExclusive', xSchemaType.maxExclusive, crlf]);
      result := Format(result + '%s : = %s%s', ['maxInclusive', xSchemaType.maxInclusive, crlf]);
      result := Format(result + '%s : = %s%s', ['totalDigits', xSchemaType.totalDigits, crlf]);
      result := Format(result + '%s : = %s%s', ['fractionDigits', xSchemaType.fractionDigits, crlf]);
      result := Format(result + '%s : = %s%s', ['length', xSchemaType.length, crlf]);
      result := Format(result + '%s : = %s%s', ['minLength', xSchemaType.minLength, crlf]);
      result := Format(result + '%s : = %s%',  ['maxLength', xSchemaType.maxLength]);

    end;// if assigned(xSchemaType) then begin
    for i := 0 to aSchemaElement.identityConstraints.length - 1 do begin
      xSchemaItem := aSchemaElement.identityConstraints.item[i];
      PrintElement('', xSchemaItem as ISchemaElement);
    end;//
  end;
begin
  // Namespace festlegen
  xNamespace := edNamespace.Text;
//  xNamespace := 'http://www.loepfe.com/schema';

  // Schemacache erzeugen in den dann das Schema f�r den entsprechenden Namespace geladen wird
  xSchemaCache := CoXMLSchemaCache40.Create;
  // Das Schema f�r den entsprechenden Namespace laden
  xSchemaCache.add(xNamespace, edReadSchema.Text);

  xSchema := xSchemaCache.getSchema(xNamespace);
  if assigned(xSchema) then begin
    // Anzahl der definierten Elemente holen
    xElementCount := xSchema.elements.length;

    // �ber alle Elemente iterieren
    for i := 0 to xElementCount - 1 do begin
      xMsg := '';

      xSchemaItem := xSchema.elements.item[i];
      try
        try
          xTypeName := (xSchemaItem as ISchemaElement).name;
        except
          xTypeName := '';
        end;

        xMsg := PrintElement(xTypeName, (xSchemaItem as ISchemaElement));
        ShowMessage(xMsg);
      except
        on e:Exception do
          ShowMessage(Format('Fehler beim Wandeln von ISchemaItem nach ISchemaElement (%s)', [e.Message]));
      end;
    end;// for i := 0 to xElementCount - 1 do begin
  end;// if assigned(xSchema) then begin
end;

procedure TForm1.writeDetail(aDetailName, aText: string);
begin
  laDetailLevel.Caption := aDetailName;
  memoDetail.Text := aText;

//  gWeiter := false;
//  while not(gWeiter) do begin
//    Application.ProcessMessages;
//    sleep(1);
//  end;
end;

procedure TForm1.bReadSchemaVBClick(Sender: TObject);
var
  nsTarget: String;
  oSchema: ISchema;
  oSchemaCache: XMLSchemaCache40;
  oAnnotationDoc: DOMDocument40;
  oE: ISchemaElement;
  oA: ISchemaAttribute;
  oT: ISchemaType;

  xMsg: string;
  // Zwischenresultat jeder Stufe (Elemente, Atribute, Typen)
  xDetailResult1: string;
  // Zwischenreszultat eines einzelnen Scrittes (pro Element, pro Attribut, ...);
  xDetailResult2: string;

  i: integer;

  function printElement(oElement: ISchemaElement; numTabs: integer):string; forward;
  Function processGroup(poGroup: ISchemaModelGroup; numTabs: integer): string; forward;

  Function printTab(numTabs: integer): string;
  var
    x: Integer;
  begin
    result := '';
    For x := 0 To numTabs do
      result := result + '   ';
  end;

  Function printParticles(oParticle: ISchemaElement): string;
  begin
    If oParticle.minOccurs <> 1 Then
        result := result + ' minOccurs=''' + oParticle.minOccurs + '''';

    If oParticle.maxOccurs <> 1 Then  begin
        If oParticle.maxOccurs = -1 Then
            result := result + ' maxOccurs=''unbounded'''
        Else
            result := result + ' maxOccurs=''' + oParticle.maxOccurs + '''';
    End;
  End;

  Function printRemark(r: string): string;
  begin
    result := '<!-- ' + r + ' -->' + crlf;
  End;

  Function printName(item: ISchemaItem): string;
  begin
    result := '';
    If (item.itemType And SOMITEM_DATATYPE) = SOMITEM_DATATYPE Then
        result := 'xsd:';

    If item.itemType = SOMITEM_ANYTYPE Then
        result := 'xsd:';

    result := result + item.Name;
  End;


  Function printRestrictions(oType: ISchemaType; numTabs: integer): string;
  var
    oItem: ISchemaItem;
    strPattern: Variant;
    strItem: String;
    i: integer;
    str1: string;
  begin
    result := '';
    If oType.minExclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:minExclusive value=''' + oType.minExclusive + '''/>' + vbNewLine;

    If oType.minInclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:minInclusive value=''' + oType.minInclusive + '''/>' + vbNewLine;

    If oType.maxExclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:maxExclusive value=''' + oType.maxExclusive + '''/>' + vbNewLine;

    If oType.maxInclusive <> '' Then
        result := result + printTab(numTabs + 1) + '<xsd:maxInclusive value=''' + oType.maxInclusive + '''/>' + vbNewLine;

    If oType.totalDigits > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:totalDigits value=''' + oType.totalDigits + '''/>' + vbNewLine;

    If oType.fractionDigits > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:fractionDigits value=''' + oType.fractionDigits + '''/>' + vbNewLine;

    If oType.length > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:length value=''' + oType.length + '''/>' + vbNewLine;

    If oType.minLength > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:minLength value=''' + oType.minLength + '''/>' + vbNewLine;

    If oType.MaxLength > -1 Then
        result := result + printTab(numTabs + 1) + '<xsd:maxLength value=''' + oType.MaxLength + '''/>' + vbNewLine;

    If oType.enumeration.length > 0 Then begin
      for i := 0 to oType.enumeration.length - 1 do
        result := result + printTab(numTabs + 1) + '<xsd:enumeration value=''' + oType.enumeration.Item[i] + '''/>' + vbNewLine;
    End ;

    If oType.whitespace > 0 Then
        result := result + printTab(numTabs + 1) + '<xsd:whitespace value=''' + IntToStr(oType.whitespace) + '''/>' + vbNewLine;

    If oType.patterns.length <> 0 Then begin
      for i := 0 to oType.patterns.length - 1 do
         result := result + printTab(numTabs + 1) + '<xsd:pattern value=''' + oType.patterns.Item[i] + '''/>' + vbNewLine;
    End;

    str1 := '';
    If (result <> '') And (oType.baseTypes.length > 0) Then begin
      Str1 := printTab(numTabs) + '<xsd:restriction base=''' + printName(oType.baseTypes[0]) + '''>' + vbNewLine;
      Str1 := Str1 + result;
      result := Str1 + printTab(numTabs) + '</xsd:restriction>' + vbNewLine;
    End;
  End;

  Function processChoiceOrSequence(poGroup: ISchemaModelGroup; numTabs: integer): string;
  var
    item: ISchemaParticle;
    i: integer;
  begin
    result := '';
    for i := 0 to poGroup.particles.length - 1 do begin
      If poGroup.particles.item[i].itemType = SOMITEM_ELEMENT Then
        result := result + printElement(poGroup.particles.item[i] as ISchemaElement, numTabs + 1);

      If (poGroup.particles.item[i].itemType And SOMITEM_GROUP) = SOMITEM_GROUP Then
        result := result + processGroup(poGroup.particles.item[i] as ISchemaModelGroup, numTabs + 1) + vbNewLine;

      If poGroup.particles.item[i].itemType = SOMITEM_ANY Then
        result := result + 'any: ' + poGroup.particles.item[i].Name + vbNewLine;
    end;// for i := 0 to poGroup.particles.length - 1 do begin
  End;


  Function processGroup(poGroup: ISchemaModelGroup; numTabs: integer): string;
  begin
    result := '';
    // List elements in the sequence.

    If poGroup.itemType = SOMITEM_ALL Then begin
      result := result + printTab(numTabs + 1) + '<xsd:all>' + vbNewLine ;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:all>' + vbNewLine;
    End;

    If poGroup.itemType = SOMITEM_CHOICE Then begin
      result := result + printTab(numTabs + 1) + '<xsd:choice>' + vbNewLine;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:choice>' + vbNewLine;
    end;

    If poGroup.itemType = SOMITEM_SEQUENCE Then begin
      result := result + printTab(numTabs + 1) + '<xsd:sequence>' + vbNewLine;
      result := result + processChoiceOrSequence(poGroup, numTabs + 1);
      result := result + printTab(numTabs + 1) + '</xsd:sequence>' + vbNewLine ;
    End;
  End;


  Function printAttr(oAttr: ISchemaAttribute; numTabs: integer): string;
  var
    strRem: String;
  begin
    If oAttr.isReference Then
        result := result + printTab(numTabs) + '<xsd:attribute ref='' + oAttr.Name + '''
    Else
        result := result + printTab(numTabs) + '<xsd:attribute name='' + oAttr.Name + ''';

    If oAttr.Type_.Name <> '' Then
        result := result + ' type='' + printName(oAttr.Type) + ''';

    If oAttr.defaultValue <> '' Then
        result := result + ' default='' + oAttr.defaultValue + ''';

    If oAttr.fixedValue <> '' Then
        result := result + ' fixed='' + oAttr.fixedValue + ''';

    If oAttr.use = SCHEMAUSE_OPTIONAL Then
      result := result + ' use=''optional''';
    If oAttr.use = SCHEMAUSE_PROHIBITED Then
      result := result + ' use=''prohibited''';
    If oAttr.use = SCHEMAUSE_REQUIRED Then
      result := result + ' use=''required''';

    result := result + '/>' + vbNewLine;
    If assigned(oAttr.scope) Then begin
        strRem := 'scope:' + printName(oAttr.scope);
        result := result + printTab(numTabs) + printRemark(strRem);
    End;
    //strRem = 'scope:' + printName(oElement.scope)
  End;

  Function processComplexType(oComplex: ISchemaItem; numTabs: integer): string;
  var
    strAny: String;
    oAttr: ISchemaAttribute;
    oComplexCast: ISchemaComplexType;
    i: integer;
  begin
    oComplexCast := oComplex as ISchemaComplexType;
    result := printTab(numTabs) + '<xsd:complexType';
    If oComplexCast.Name <> '' Then
        result := result + ' name=''' + oComplexCast.Name + '''';

    result := result + '>' + vbNewLine;

    If oComplexCast.contentType = SCHEMACONTENTTYPE_EMPTY Then
        result := result + printTab(numTabs) + printRemark('emtpy');

    If oComplexCast.contentType = SCHEMACONTENTTYPE_TEXTONLY Then
        result := result + printTab(numTabs) + printRemark('textonly');

    If oComplexCast.contentType = SCHEMACONTENTTYPE_ELEMENTONLY Then begin
        result := result + printTab(numTabs) + printRemark('elementonly');
        result := result + processGroup(oComplexCast.contentModel, numTabs + 1);
    End;

    If oComplexCast.contentType = SCHEMACONTENTTYPE_MIXED Then begin
        result := result + printTab(numTabs) + printRemark('mixed');
        result := result + processGroup(oComplexCast.contentModel, numTabs + 1);
    End;
    result := result + vbNewLine;
    result := result + printRestrictions(oComplexCast, numTabs + 1);

    try
      strAny := oComplexCast.anyAttribute.Name;
      result := result + oComplexCast.anyAttribute.Name;
    except
    End;

    for i := 0 to oComplexCast.Attributes.length - 1 do
      result := result + printAttr(oComplexCast.Attributes.item[i] as ISchemaAttribute, numTabs + 1);

    result := result + printTab(numTabs) + '</xsd:complexType>' + vbNewLine;
  End;



  Function processSimpleType(oSimple: ISchemaType; numTabs: integer): string;
  var
    oType: ISchemaType;
    i: integer;
  begin
    result := printTab(numTabs) + '<xsd:simpleType';
    If oSimple.Name <> '' Then
      result := result + ' name=''' + oSimple.Name + '''';

    result := result + '>' + vbNewLine;

    If oSimple.baseTypes.length = 1 Then begin
        result := result + printRestrictions(oSimple, numTabs + 1);
    end Else begin
      for i := 0 to oSimple.baseTypes.length - 1 do
        result := result + '<baseType name=''' + printName(oSimple.baseTypes.item[i]) + '''>' + vbNewLine;
    End;
    result := result + printTab(numTabs) + '</xsd:simpleType>' + vbNewLine;
  End;


  function printElement(oElement: ISchemaElement; numTabs: integer):string;
  var
    res: String;
    strRem: String;
    oType: ISchemaType;
  begin
    result := printTab(numTabs) + '<xsd:element ';

    If oElement.isReference Then begin
        result := result + 'ref=''' + oElement.Name + '''' + printParticles(oElement) + '>';
        result := result + '<!-- ';
        result := result + ' abstract=''' + IntToStr(Word(oElement.isAbstract)) + '''';
        result := result + ' -->' + vbNewLine;
    end Else begin
        oType := oElement.Type_;
        result := result + 'name=''' + oElement.Name + '''' + printParticles(oElement);
        result := result + ' abstract=''' + IntToStr(Word(oElement.isAbstract)) + '''';
        result := result + ' id=''' + oElement.id + '''';
        If oType.Name = '' Then begin
            result := result + ' >' + vbNewLine;
            If oType.itemType = SOMITEM_COMPLEXTYPE Then
                result := result + result + processComplexType(oType, numTabs + 1)
            Else
                result := result + processSimpleType(oType, numTabs);
            result := result + printTab(numTabs) + '</xsd:element>' + vbNewLine;
        end Else begin
            If printName(oType) <> 'xsd:anyType' Then
                result := result + ' type=''' + printName(oType) + '''';

            If oType.itemType <> SOMITEM_COMPLEXTYPE Then begin
                If printRestrictions(oType, 0) = '' Then begin
                    result := result + '/>' + vbNewLine;
                end Else begin
                    result := result + '>' + vbNewLine + processSimpleType(oType, numTabs);
                    result := result + printTab(numTabs) + '</xsd:element>'
                end;
            end Else begin
                result := result + '/>' + vbNewLine;
            End;
        End;
    End;
    If assigned(oElement.scope)Then begin
        strRem := 'scope:' + printName(oElement.scope);
        result := result + printTab(numTabs) + printRemark(strRem);
    End;
  end;//

  Function processType(oType: ISchemaItem; numTabs: integer): string;
  begin
//    res = printTab(numTabs) + printRemark(oType.name)+ vbNewLine
    If oType.itemType = SOMITEM_ANYTYPE Then
      result := result + printTab(numTabs + 1) + '<!-- ' + oType.Name + ' -->' + vbNewLine;

    If oType.itemType = SOMITEM_COMPLEXTYPE Then
      result := result + processComplexType(oType, numTabs + 1);

    If oType.itemType = SOMITEM_SIMPLETYPE Then
      result := result + processSimpleType(oType as ISchemaType, numTabs + 1);

    result := result + vbNewLine;
  End;
begin
  memoOutput.Clear;

  oSchemaCache := CoXMLSchemaCache40.Create;
  oAnnotationDoc := CoDOMDocument40.Create;

  // Load the schema.
  nsTarget := edNamespace.Text;

  oSchemaCache.Add (nsTarget, edReadSchema.Text);
  oSchema := oSchemaCache.getSchema(nsTarget);

  xMsg := '<xsd:schema xmlns:xsd=''http://www.w3.org/2001/XMLSchema''>' + vbNewLine;

  // elemente
  xDetailResult1 := '';
  for i := 0 to oSchema.elements.Length - 1 do begin
    xDetailResult2 := '';
    xDetailResult2 := printElement(oSchema.elements.item[i] as ISchemaElement, 0);
    xDetailResult1 := xDetailResult1 + xDetailResult2;
    writeDetail('Element [' + IntToStr(i) +']', xDetailResult2);
  end;
  writeDetail('Elements', xDetailResult1);
  xMsg := xMsg + xDetailResult1;

  // attribute
  xDetailResult1 := '';
  for i := 0 to oSchema.Attributes.length - 1 do begin
    xDetailResult2 := '';
    xDetailResult2 := printAttr(oSchema.Attributes.item[i] as ISchemaAttribute, 0);
    xDetailResult1 := xDetailResult1 + xDetailResult2;
    writeDetail('Attribute [' + IntToStr(i) +']', xDetailResult2);
  end;
  writeDetail('Attributes', xDetailResult1);
  xMsg := xMsg + xDetailResult1;

  xMsg := xMsg + vbNewLine;

  // Typen
  xDetailResult1 := '';
  for i := 0 to oSchema.types.length - 1 do begin
    xDetailResult2 := '';
    xDetailResult2 := processType(oSchema.types.item[i], 0);
    xDetailResult1 := xDetailResult1 + xDetailResult2;
    writeDetail('type [' + IntToStr(i) +']', xDetailResult2);
  end;
  writeDetail('types', xDetailResult1);
  xMsg := xMsg + xDetailResult1;

  xMsg := xMsg + '</xsd:schema>';

  memoOutput.Text := xMsg;
end;

procedure TForm1.bWeiterClick(Sender: TObject);
begin
  gWeiter := true;
end;

procedure TForm1.bReadSchemaLokClick(Sender: TObject);
var
  xNamespace: String;
  oSchema: ISchema;
  oSchemaCache: XMLSchemaCache40;

  xMsg: string;
  // Zwischenresultat jeder Stufe (Elemente, Atribute, Typen)
  xDetailResult1: string;
  // Zwischenreszultat eines einzelnen Scrittes (pro Element, pro Attribut, ...);
  xDetailResult2: string;

  i: integer;
begin

  memoOutput.Clear;

  oSchemaCache := CoXMLSchemaCache40.Create;

  // Load the schema.
  xNamespace := edNamespace.Text;

  oSchemaCache.Add (xNamespace, edReadSchema.Text);
  oSchema := oSchemaCache.getSchema(xNamespace);

  xMsg := '<xsd:schema xmlns:xsd=''http://www.w3.org/2001/XMLSchema''>' + vbNewLine;

  // Typen
  xDetailResult1 := '';
  for i := 0 to oSchema.types.length - 1 do begin
    xDetailResult2 := '';
    xDetailResult2 := processType(oSchema.types.item[i], 0);
    xDetailResult1 := xDetailResult1 + xDetailResult2;
//    writeDetail('type [' + IntToStr(i) +']', xDetailResult2);
  end;
//  writeDetail('types', xDetailResult1);
  xMsg := xMsg + xDetailResult1;

  xMsg := xMsg + '</xsd:schema>';

  memoOutput.Text := xMsg;
end;

end.
