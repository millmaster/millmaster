object Form1: TForm1
  Left = 606
  Top = 134
  Width = 668
  Height = 560
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmSplitter1: TmmSplitter
    Left = 217
    Top = 0
    Width = 9
    Height = 533
    Cursor = crHSplit
  end
  object memoOutput: TmmMemo
    Left = 226
    Top = 0
    Width = 434
    Height = 533
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 0
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 217
    Height = 533
    Align = alLeft
    BevelOuter = bvNone
    TabOrder = 1
    object memoDetail: TmmMemo
      Left = 0
      Top = 313
      Width = 217
      Height = 220
      Align = alClient
      ScrollBars = ssBoth
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmPanel2: TmmPanel
      Left = 0
      Top = 0
      Width = 217
      Height = 313
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object mmLabel1: TmmLabel
        Left = 8
        Top = 160
        Width = 57
        Height = 13
        Caption = 'Namespace'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laDetailLevel: TmmLabel
        Left = 8
        Top = 296
        Width = 61
        Height = 13
        Anchors = [akLeft, akBottom]
        Caption = 'laDetailLevel'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 8
        Top = 120
        Width = 39
        Height = 13
        Caption = 'Schema'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel3: TmmLabel
        Left = 8
        Top = 208
        Width = 18
        Height = 13
        Caption = 'Typ'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object Item: TmmLabel
        Left = 8
        Top = 248
        Width = 20
        Height = 13
        Caption = 'Item'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel4: TmmLabel
        Left = 16
        Top = 40
        Width = 76
        Height = 13
        Caption = 'Typen aufl�sen:'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmButton1: TmmButton
        Left = 12
        Top = 8
        Width = 173
        Height = 25
        Caption = 'Document Element auslesen'
        TabOrder = 0
        Visible = True
        OnClick = mmButton1Click
        AutoLabel.LabelPosition = lpLeft
      end
      object bReadSchemaVB: TmmButton
        Left = 12
        Top = 56
        Width = 85
        Height = 25
        Caption = 'VB'
        TabOrder = 1
        Visible = True
        OnClick = bReadSchemaVBClick
        AutoLabel.LabelPosition = lpLeft
      end
      object edReadSchema: TmmEdit
        Left = 8
        Top = 136
        Width = 169
        Height = 21
        Color = clWindow
        TabOrder = 2
        Text = 'LOKTest1.xsd'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edNamespace: TmmEdit
        Left = 8
        Top = 176
        Width = 169
        Height = 21
        Color = clWindow
        TabOrder = 3
        Text = 'http://www.loepfe.com/schema'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object bWeiter: TmmButton
        Left = 168
        Top = 296
        Width = 43
        Height = 17
        Anchors = [akRight, akBottom]
        Caption = 'Weiter'
        TabOrder = 4
        Visible = True
        OnClick = bWeiterClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bAppinfo: TmmButton
        Left = 12
        Top = 88
        Width = 173
        Height = 25
        Caption = 'Appinfo lesen'
        TabOrder = 5
        Visible = True
        OnClick = bAppinfoClick
        AutoLabel.LabelPosition = lpLeft
      end
      object edTyp: TmmEdit
        Left = 8
        Top = 224
        Width = 121
        Height = 21
        Color = clWindow
        TabOrder = 6
        Text = 'LOKConcrete'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object edItem: TmmEdit
        Left = 8
        Top = 264
        Width = 121
        Height = 21
        Color = clWindow
        TabOrder = 7
        Text = 'Wert1'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object bReadSchemaLok: TmmButton
        Left = 101
        Top = 56
        Width = 84
        Height = 25
        Caption = 'Delphi'
        TabOrder = 8
        Visible = True
        OnClick = bReadSchemaLokClick
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
end
