unit FrameBaseFunction;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, XMLMapping,
  comctrls;

type
  (*
  Basisklasse f�r die statistischen Funktionen.
  *)
  TfraBaseFunction = class(TFrame)
  private
    fTreeView: TTreeView;
    function GetLoepfeBody: TLoepfeBody;
    procedure SetTreeView(Value: TTreeView);
  protected
    FActiveElement: TBaseElement;
    procedure SetActiveElement(const aElement: TBaseElement); virtual;
    property LoepfeBody: TLoepfeBody read GetLoepfeBody;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Execute; virtual; abstract;
    property ActiveElement: TBaseElement read FActiveElement write SetActiveElement;
    property TreeView: TTreeView read fTreeView write SetTreeView;
  end;
  
implementation

//:---------------------------------------------------------------------------
//:--- Class: TfrBaseFunction
//:---------------------------------------------------------------------------
constructor TfraBaseFunction.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fTreeView := Nil;
end; // TfrBaseFunction.Create

//:---------------------------------------------------------------------------
destructor TfraBaseFunction.Destroy;
begin
  inherited Destroy;
end; // TfrBaseFunction.Destroy

//:---------------------------------------------------------------------------
function TfraBaseFunction.GetLoepfeBody: TLoepfeBody;
begin
  Result := Nil;
  if Assigned(fTreeView) then begin
    Result := TLoepfeBody(fTreeView.Items.GetFirstNode.Data);
  end;
  
  if Result = Nil then
    Abort;
end; // TfrBaseFunction.GetLoepfeBody

//:---------------------------------------------------------------------------
procedure TfraBaseFunction.SetTreeView(Value: TTreeView);
begin
  //:...................................................................
  fTreeView := Value;
end; // TfrBaseFunction.SetTreeView

{$R *.DFM}

procedure TfraBaseFunction.SetActiveElement(const aElement: TBaseElement);
begin
  if FActiveElement <> aElement then
  begin
    FActiveElement := aElement;
  end;
end;


end.


