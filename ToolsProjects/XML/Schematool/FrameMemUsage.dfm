inherited fraMemUsage: TfraMemUsage
  Width = 769
  Height = 239
  object sgMemory: TmmStringGrid
    Left = 0
    Top = 0
    Width = 580
    Height = 239
    Align = alClient
    ColCount = 33
    Ctl3D = False
    DefaultColWidth = 16
    DefaultRowHeight = 16
    RowCount = 2
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -8
    Font.Name = 'Verdana'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goRangeSelect]
    ParentCtl3D = False
    ParentFont = False
    TabOrder = 0
    Visible = True
    OnClick = sgMemoryClick
    OnDblClick = sgMemoryDblClick
    OnDrawCell = sgMemoryDrawCell
    AutoLabel.LabelPosition = lpLeft
    ColWidths = (
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16
      16)
  end
  object mmPanel1: TmmPanel
    Left = 580
    Top = 0
    Width = 189
    Height = 239
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object mmLabel1: TmmLabel
      Left = 8
      Top = 32
      Width = 28
      Height = 13
      Caption = 'Name'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laName: TmmLabel
      Left = 64
      Top = 32
      Width = 18
      Height = 13
      Caption = '      '
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 8
      Top = 48
      Width = 43
      Height = 13
      Caption = 'Datentyp'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laDatatype: TmmLabel
      Left = 64
      Top = 48
      Width = 18
      Height = 13
      Caption = '      '
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel5: TmmLabel
      Left = 8
      Top = 80
      Width = 32
      Height = 13
      Caption = 'Anzahl'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laCount: TmmLabel
      Left = 64
      Top = 80
      Width = 18
      Height = 13
      Caption = '      '
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel7: TmmLabel
      Left = 8
      Top = 64
      Width = 28
      Height = 13
      Caption = 'Offset'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laOffset: TmmLabel
      Left = 64
      Top = 64
      Width = 18
      Height = 13
      Caption = '      '
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 8
      Top = 105
      Width = 78
      Height = 13
      Caption = 'gesuchter Offset'
      FocusControl = edOffset
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bUndockFrame: TmmSpeedButton
      Left = 8
      Top = 5
      Width = 24
      Height = 22
      Glyph.Data = {
        36050000424D3605000000000000360400002800000010000000100000000100
        0800000000000001000000000000000000000001000000010000FF00FF007416
        1700751919005022230056242500592526005F25260059292A00403E3E005C3E
        3D006420210069282900742222007D2C2D006E3435004442410046454400923E
        3F00A5353600AA353700834141008B414100984344009D4546009F4748009049
        4A009E4A4B008F4F50009F515200A0434400A0444500A4464700A34A4B00A648
        4900B24C4D00AA575800AB585900AF595A00B0545500BF595A00CC696A00D362
        6300DA626300DC626300DD636400D5676800D86A6B00DA6E6F00C1777700E165
        6600E2696A00E56A6B00E66C6D00EB6D6E00EE707200F6797A00F87A7B00FA7D
        7E00FE7D7E00DD808100FF808100FF888900FF8E8F00E6979700E9999900EE9C
        9C00FB969700FE979800FE9A9C00000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000000000000000
        0000000000000000000000000000000000000000000000000000001010000000
        0000000000000000000000101000000000000000000000000000001010000000
        0000000000000000000000101000000000000000000000000000001010000000
        000000000000000000000010100000000000000E040404040000001010070000
        00000B303B2D272104040010081C2F26160D0C41443A36311D04001008243D3C
        3513023F433A342A1D04001008233D3A3412023F4337342A1D04001008233D3A
        2F12023F433A342A1D04001008233D3A3112023F4337342A1D05001008243D3C
        341202414337342A220500100819282F2912011B141A21181105001010090515
        1A0B0A0500000000040400101000000000000000000000000000}
      Visible = True
      OnClick = bUndockFrameClick
      AutoLabel.LabelPosition = lpLeft
    end
    object edOffset: TmmEdit
      Left = 8
      Top = 120
      Width = 57
      Height = 21
      Color = clWindow
      TabOrder = 0
      Visible = True
      OnChange = edOffsetChange
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel2
      AutoLabel.LabelPosition = lpTop
      Decimals = -1
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      ValidateMode = [vmInput]
    end
  end
end
