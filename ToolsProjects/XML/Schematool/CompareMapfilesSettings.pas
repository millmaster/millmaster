unit CompareMapfilesSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, mmLabel, CheckLst, mmCheckListBox, SchematoolGlobal, XMLMapping,
  mmButton;

type
  TfrmCompareMapfilesSettings = class(TForm)
    lbAvailableItems: TmmCheckListBox;
    lbAvailableMapfiles: TmmCheckListBox;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    bAllIItems: TmmButton;
    bNoItems: TmmButton;
    bAllMapfiles: TmmButton;
    bNoMapfiles: TmmButton;
    procedure FormShow(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure bNoItemsClick(Sender: TObject);
    procedure bAllIItemsClick(Sender: TObject);
    procedure bAllMapfilesClick(Sender: TObject);
    procedure bNoMapfilesClick(Sender: TObject);
  private
    FSettings: TSchematoolSettings;
    { Private declarations }
  public
    procedure ShowForm(aSelectedItems: TStringList; aSelectedMapfiles: TStringList; aLoepfeBody: TLoepfeBody);
    property Settings: TSchematoolSettings read FSettings write FSettings;
    { Public declarations }
  end;

var
  frmCompareMapfilesSettings: TfrmCompareMapfilesSettings;

implementation

{$R *.DFM}

(*---------------------------------------------------------
  Stellt die Einstellungen (Position) wieder her
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.FormShow(Sender: TObject);
begin
  if assigned(Settings) then
    Settings.LoadForm(self, self.name, true);
end;// procedure TfrmCompareMapfilesSettings.FormShow(Sender: TObject);

(*---------------------------------------------------------
  Sichert die Settings (Position)
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.FormHide(Sender: TObject);
begin
  if assigned(Settings) then
    Settings.SaveForm(self, self.name);
end;// procedure TfrmCompareMapfilesSettings.FormHide(Sender: TObject);

(*---------------------------------------------------------
  Zeigt den Dialog an
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.ShowForm(aSelectedItems: TStringList; aSelectedMapfiles: TStringList;
    aLoepfeBody: TLoepfeBody);
var
  xIndex: Integer;
  i: Integer;
begin
  if assigned(aSelectedItems) and assigned(aSelectedMapfiles) and (assigned(aLoepfeBody)) then begin
    // Sollten sowiso leer sein, da der Dialog f�r jeden Aufruf neu erzeugt wird
    lbAvailableMapfiles.Items.Clear;
    lbAvailableItems.Items.Clear;

    // Mapfiles und Items anzeigen
    for i := 0 to aLoepfeBody.MapIDs.Count - 1 do begin
      xIndex := lbAvailableMapfiles.Items.Add(aLoepfeBody.MapIDs.Names[i]);
      lbAvailableMapfiles.Checked[xIndex] := (aSelectedMapfiles.IndexOf(lbAvailableMapfiles.Items[xIndex]) >= 0);
    end;// for i := 0 to FLoepfeBody.MapIDs.Count - 1 do begin

    // Alle verf�gbaren Items
    for i := Low(cAvailableItems) to High(cAvailableItems) do begin
      xIndex := lbAvailableItems.Items.Add(cAvailableItems[i].DisplayName);
      lbAvailableItems.Checked[xIndex] := (aSelectedItems.IndexOf(lbAvailableItems.Items[xIndex]) >= 0);
    end;// for i := Low(cAvailableItems) to High(cAvailableItems) do begin

    // Dialog Anzeigen
    if showModal = mrOK then begin
      // Mapfiles und Items �bernehmen
      aSelectedMapfiles.Clear;
      for i := 0 to lbAvailableMapfiles.Items.Count - 1 do
        if lbAvailableMapfiles.Checked[i] then
          aSelectedMapfiles.Add(lbAvailableMapfiles.Items[i]);

      aSelectedItems.Clear;
      for i := 0 to lbAvailableItems.Items.Count - 1 do
        if lbAvailableItems.Checked[i] then
          aSelectedItems.AddObject(lbAvailableItems.Items[i], TObject(i));
    end;// if showModal = mrOK then begin
  end else begin
    MessageDlg('SelectedItems, SelectedMapfiles oder LoepfeBody = nil', mtError, [mbOK], 0);
  end;// if assigned(aSelectedItems) and assigned(aSelectedMapfiles) then begin
end;// procedure TfrmCompareMapfilesSettings.ShowForm(aSelectedItems: TStringList; aSelectedMapfiles: TStringList);

(*---------------------------------------------------------
  Deselktiert alle Items
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.bNoItemsClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbAvailableItems.Items.Count - 1 do
    lbAvailableItems.Checked[i] := false;
end;// procedure TfrmCompareMapfilesSettings.bNoItemsClick(Sender: TObject);

(*---------------------------------------------------------
  Selektiert alle Items
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.bAllIItemsClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbAvailableItems.Items.Count - 1 do
    lbAvailableItems.Checked[i] := true;
end;// procedure TfrmCompareMapfilesSettings.bAllIItemsClick(Sender: TObject);

(*---------------------------------------------------------
  Selektiert alle Mapfiles
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.bAllMapfilesClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbAvailableMapfiles.Items.Count - 1 do
    lbAvailableMapfiles.Checked[i] := true;
end;// procedure TfrmCompareMapfilesSettings.bAllMapfilesClick(Sender: TObject);

(*---------------------------------------------------------
  Deselektiert alle Mapfiels
----------------------------------------------------------*)
procedure TfrmCompareMapfilesSettings.bNoMapfilesClick(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to lbAvailableMapfiles.Items.Count - 1 do
    lbAvailableMapfiles.Checked[i] := false;
end;// procedure TfrmCompareMapfilesSettings.bNoMapfilesClick(Sender: TObject);

end.
