unit mwComboControl;

interface

uses
  Classes, Controls;

type
  TMwComboControl = class(TComponent)
  private
    FLink: TControl;
    FPopup: TControl;
    FHeight: Integer;
    FWidth: Integer;
    FBeforeShow: TNotifyEvent;
    FAfterHide: TNotifyEvent;
    procedure SetLink(Value: TControl);
    procedure SetPopup(Value: TControl);
  protected
    procedure DoBeforeShow; virtual;
    procedure DoAfterHide; virtual;
  public
    procedure ShowPopup;
    procedure HidePopup;
  published
    property Link: TControl read FLink write SetLink;
    property Popup: TControl read FPopup write SetPopup;
    property Height: Integer read FHeight write FHeight;
    property Width: Integer read FWidth write FWidth;
    property BeforeShow: TNotifyEvent read FBeforeShow write FBeforeShow;
    property AfterHide: TNotifyEvent read FAfterHide write FAfterHide;
  end;

implementation

uses
  Windows, Messages, Forms;

type
  TPopupForm = class(TCustomForm)
  private
    FOwner: TMwComboControl;
    FHookedForm: TCustomForm;
    FOrgFormProc: Pointer;
    FOrgPopupParent: TWinControl;
    FOrgPopupWidth: Integer;
    FOrgPopupHeight: Integer;
    FOrgPopupAlign: TAlign;
    procedure HookPopup;
    procedure UnhookPopup;
    procedure HookForm;
    procedure UnhookForm;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(AOwner: TMwComboControl); reintroduce;
    destructor Destroy; override;
  end;

var
  FPopupForm: TPopupForm;

{ Helper functions }

procedure HidePopupForm;
begin
  if not Assigned(FPopupForm) then
    exit;
  with FPopupForm do begin
    UnhookPopup;
    UnhookForm;
    FOwner.DoAfterHide;
    Release;
    FPopupForm := nil;
  end;
end;

function HookFormProc(Wnd: HWND; Msg, wParam, lParam: LongInt): LongInt; stdcall;
begin
  if Assigned(FPopupForm) then begin
    case Msg of
      WM_NCACTIVATE: Result := 1;
      WM_ACTIVATEAPP,
      WM_NCLBUTTONDOWN,
      WM_NCRBUTTONDOWN,
      WM_LBUTTONDOWN,
      WM_RBUTTONDOWN,
      WM_PARENTNOTIFY: begin
        HidePopupForm;
        Result := DefWindowProc(Wnd, Msg, wParam, lParam);
      end;
      else Result := CallWindowProc(FPopupForm.FOrgFormProc, Wnd, Msg, wParam, lParam);
    end;
  end else begin
    Result := DefWindowProc(Wnd, Msg, wParam, lParam);
  end;
end;

{ TPopupForm }

constructor TPopupForm.Create(AOwner: TMwComboControl);
begin
  inherited CreateNew(Application);
  FOwner := AOwner;
end;

procedure TPopupForm.CreateParams(var Params: TCreateParams);
begin
  inherited;
  Params.Style := WS_POPUP or WS_CLIPSIBLINGS or WS_VISIBLE or WS_BORDER;
  Params.ExStyle := WS_EX_TOPMOST or WS_EX_TOOLWINDOW;
  Params.WndParent := Application.Handle;
end;

destructor TPopupForm.Destroy;
begin
  UnhookPopup;
  UnhookForm;
  inherited;
end;

procedure TPopupForm.HookForm;
var
  O: LongInt;
begin
  FHookedForm := GetParentForm(FOwner.FLink);
  if not Assigned(FHookedForm) then
    exit;
  O := GetWindowLong(FHookedForm.Handle, GWL_WNDPROC);
  if O = LongInt(@HookFormProc) then
    exit;
  FOrgFormProc := Pointer(O);
  SetWindowLong(FHookedForm.Handle, GWL_WNDPROC, LongInt(@HookFormProc));
end;

procedure TPopupForm.HookPopup;
begin
  with FOwner.FPopup do begin
    FOrgPopupParent := Parent;
    FOrgPopupWidth := Width;
    FOrgPopupHeight := Height;
    FOrgPopupAlign := Align;
    Parent := Self;
    Align := alClient;
    Show;
  end;
end;

procedure TPopupForm.UnhookForm;
begin
  if not Assigned(FHookedForm) then
    exit;
  if not Assigned(FOrgFormProc) then
    exit;
  SetWindowLong(FHookedForm.Handle, GWL_WNDPROC, LongInt(FOrgFormProc));
  FOrgFormProc := nil;
  FHookedForm := nil;
end;

procedure TPopupForm.UnhookPopup;
begin
  if not Assigned(FOrgPopupParent) then
    exit;
  with FOwner.FPopup do begin
    Hide;
    Align := FOrgPopupAlign;
    Width := FOrgPopupWidth;
    Height := FOrgPopupHeight;
    Parent := FOrgPopupParent;
    FOrgPopupParent := nil;
  end;
end;

{ TMwComboControl }

procedure TMwComboControl.DoAfterHide;
begin
  if Assigned(FAfterHide) then
    FAfterHide(Self);
end;

procedure TMwComboControl.DoBeforeShow;
begin
  if Assigned(FBeforeShow) then
    FBeforeShow(Self);
end;

procedure TMwComboControl.HidePopup;
begin
  HidePopupForm;
end;

procedure TMwComboControl.SetLink(Value: TControl);
begin
  FLink := Value;
  if Assigned(Value) then
    Value.FreeNotification(Self);
end;

procedure TMwComboControl.SetPopup(Value: TControl);
begin
  FPopup := Value;
  if Assigned(Value) then
    Value.FreeNotification(Self);
end;

procedure TMwComboControl.ShowPopup;
var
  R: TRect;
  SW, SH: Integer;
  W, H, H1: Integer;
begin
  HidePopupForm;
  if not Assigned(FLink) then
    exit;
  if not Assigned(FPopup) then
    exit;
  FPopupForm := TPopupForm.Create(Self);
  DoBeforeShow;
  SW := Screen.Width;
  SH := Screen.Height;
  R := FLink.BoundsRect;
  MapWindowPoints(FLink.Parent.Handle, 0, R, 2);
  if FWidth > 0 then
    W := FWidth
  else
    W := R.Right - R.Left + 2;
  if W > SW then
    W := SW;
  if FHeight > 0 then
    H := FHeight
  else
    H := FPopup.Height + 2;
  if H > SH then
    H := SH;
  if R.Left + W > SW then
    R.Left := SW - W;
  if R.Left < 0 then
    R.Left := 0;
  R.Right := R.Left + W;
  H1 := SH - R.Bottom;
  if (H < H1) or (R.Top < H1) then begin
    if H > H1 then
      H := H1;
    R.Top := R.Bottom;
    inc(R.Bottom, H);
  end else begin
    if H > R.Top then
      H := R.Top;
    R.Bottom := R.Top;
    dec(R.Top, H);
  end;
  FPopupForm.BoundsRect := R;
  FPopupForm.HookPopup;
  FPopupForm.HookForm;
  FPopupForm.Show;
end;

end.
