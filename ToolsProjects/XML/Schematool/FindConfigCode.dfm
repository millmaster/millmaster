object frmFindConfigCode: TfrmFindConfigCode
  Left = 1823
  Top = 205
  BorderStyle = bsDialog
  Caption = 'frmFindConfigCode'
  ClientHeight = 140
  ClientWidth = 263
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 14
    Top = 57
    Width = 103
    Height = 13
    Caption = 'Gefundene Elemente:'
    FocusControl = lbCCElements
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object rgCCName: TmmRadioGroup
    Left = 14
    Top = 13
    Width = 185
    Height = 33
    Caption = 'ConfigCode'
    Columns = 4
    ItemIndex = 0
    Items.Strings = (
      '&A'
      '&B'
      '&C'
      '&D')
    TabOrder = 0
    OnClick = rgCCNameClick
    CaptionSpace = True
  end
  object edCC: TmmEdit
    Left = 214
    Top = 21
    Width = 33
    Height = 21
    Color = clWindow
    TabOrder = 1
    Text = '0'
    Visible = True
    OnChange = edCCChange
    OnKeyDown = edCCKeyDown
    Alignment = taRightJustify
    AutoLabel.LabelPosition = lpLeft
    Decimals = 0
    MaxValue = 15
    MinValue = -0.1
    NumMode = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
    ValidateMode = [vmInput]
  end
  object lbCCElements: TmmListBox
    Left = 14
    Top = 72
    Width = 233
    Height = 57
    Enabled = True
    ItemHeight = 13
    TabOrder = 2
    Visible = True
    OnClick = lbCCElementsClick
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
  end
end
