unit ChooseMapfile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmLabel, mmComboBox, Buttons, ExtCtrls, XMLMapping, mmMemo,
  mmImage, mmPanel;

type
  TfrmChooseMapfile = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    cobMapfile: TmmComboBox;
    laPrompt: TmmLabel;
    mDescriptionPanel: TmmPanel;
    mmPanel8: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmMemo;
  private
    { Private declarations }
  public
    class function AskForMapfile(aPrompt: string; var aMapfile: string; aLoepfeBody: TLoepfeBody; aInfo: string): Boolean;

    { Public declarations }
  end;

var
  frmChooseMapfile: TfrmChooseMapfile;

implementation

{$R *.DFM}

class function TfrmChooseMapfile.AskForMapfile(aPrompt: string; var aMapfile: string; aLoepfeBody: TLoepfeBody; aInfo:
    string): Boolean;
var
  i: integer;
begin
  Result := false;
  with TfrmChooseMapfile.Create(nil) do try
    laPrompt.Caption := aPrompt;

    // Combo Box abf�llen
    if assigned(aLoepfeBody) then begin
      for i := 0 to aLoepfeBody.MapIDs.Count - 1 do
        cobMapfile.Items.Add(aLoepfeBody.MapIDs.Names[i]);
    end;
    if aInfo > '' then begin
      mDescriptionMemo.Text := aInfo;
      mDescriptionPanel.visible := true;
    end;

    if showModal = mrOK then begin
      if cobMapfile.ItemIndex >= 0 then
        aMapfile := cobMapfile.Items[cobMapfile.ItemIndex];
      result := (aMapfile > '');
    end;
  finally
    free;
  end;
end;

end.
