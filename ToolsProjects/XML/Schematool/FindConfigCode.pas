unit FindConfigCode;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, XMLMapping,
  StdCtrls, mmLabel, mmListBox, mmEdit, ExtCtrls, mmRadioGroup, comctrls, SchematoolGlobal;

type
  TfrmFindConfigCode = class(TForm)
    rgCCName: TmmRadioGroup;
    edCC: TmmEdit;
    lbCCElements: TmmListBox;
    mmLabel1: TmmLabel;
    procedure rgCCNameClick(Sender: TObject);
    procedure edCCChange(Sender: TObject);
    procedure edCCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lbCCElementsClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FSettings: TSchematoolSettings;
    mLoepfeBody: TLoepfeBody;
    mTree: TTreeView;
    function SelectObject(AObject: Pointer): Boolean;
    { Private declarations }
  public
    procedure refreshSearch;
    function ShowFindCC(aLoepfeBody: TLoepfeBody; aTree: TTreeView): Boolean;
    property Settings: TSchematoolSettings read FSettings write FSettings;
    { Public declarations }
  end;

var
  frmFindConfigCode: TfrmFindConfigCode;

implementation

{$R *.DFM}

(*---------------------------------------------------------
  Rekursive Suche durch den Baum um alle Elemente mit dem gesuchten Config Code zu erhalten.
  Zum Teil sind die ConfigCodes doppelt belegt (Spectra und Zenit)
----------------------------------------------------------*)
procedure TfrmFindConfigCode.refreshSearch;
var
  i: Integer;
  xSelectedCC: string;
  xSelectedBit: integer;

  (*---------------------------------------------------------
    Rekursive Suche
  ----------------------------------------------------------*)
  procedure FindCC(aElement: TMMElement);
  var
    i: Integer;
  begin
    if assigned(aElement) then begin
      if (aElement.Active) and (aElement.ConfigCode = xSelectedCC) and (aElement.CCBitNumber = xSelectedBit) then
        lbCCElements.Items.AddObject(aElement.ElementName, aElement);

      // Rekurrsion starten
      for i := 0 to aElement.MMElements.Count - 1 do
        FindCC(aElement.MMElements[i]);
    end;// if assigned(aElement) then begin
  end;// procedure FindCC(aElement: TMMElement);
begin
  // Bestimmen was gesucht wird
  xSelectedCC := chr(rgCCName.ItemIndex + ord('A'));
  xSelectedBit := edCC.AsInteger;

  // evt. Vorheriges Suchergebnis l�schen
  lbCCElements.Clear;

  // Die Liste aus dem Loepfebody rekursiv verarbeiten
  for i := 0 to mLoepfeBody.MMElements.Count - 1 do
    FindCC(mLoepfeBody.MMElements[i]);
end;// procedure TfrmFindConfigCode.refreshSearch;

(*---------------------------------------------------------
  Zeigt den Dialog an. Gleich zu Begin wird eine Suche nach dem ConfigCode A0 ausgel�st
----------------------------------------------------------*)
function TfrmFindConfigCode.ShowFindCC(aLoepfeBody: TLoepfeBody; aTree: TTreeView): Boolean;
begin
  result := false;
  mLoepfeBody := aLoepfeBody;
  mTree := aTree;
  if assigned(mLoepfeBody) then begin
    result := true;
    // Zeigt das aktuelle Map an. Diese Information wird aktualisiert, wenn das Map �ndert
    Caption := mLoepfeBody.MapID;
    // Zeigt den Dialog an
    show;
    // ... Und l�st gleich die erste Suche aus
    refreshSearch;
  end else begin
    Free;
  end;// if assigned(mLoepfeBody) then begin
end;// procedure TfrmFindConfigCode.ShowFindCC(aLoepfeBody: TLoepfeBody);

(*---------------------------------------------------------
  Suche aktualisieren
----------------------------------------------------------*)
procedure TfrmFindConfigCode.rgCCNameClick(Sender: TObject);
begin
  refreshSearch;
end;// procedure TfrmFindConfigCode.rgCCNameClick(Sender: TObject);

(*---------------------------------------------------------
  Suche aktualisieren
----------------------------------------------------------*)
procedure TfrmFindConfigCode.edCCChange(Sender: TObject);
begin
  refreshSearch;
end;// procedure TfrmFindConfigCode.edCCChange(Sender: TObject);

(*---------------------------------------------------------
  Wenn der Cursor im Editfeld steht, dann mit den Cursortasten das Bit �ndern
----------------------------------------------------------*)
procedure TfrmFindConfigCode.edCCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_DOWN, VK_LEFT: begin
      if edCC.AsInteger > 0 then
        edCC.AsInteger := edCC.AsInteger - 1;
      Key := 0;
    end;// VK_DOWN, VK_LEFT: begin
    VK_UP, VK_RIGHT: begin
      edCC.AsInteger := edCC.AsInteger + 1;
      Key := 0;
    end;// VK_UP, VK_RIGHT: begin
  end;// case Key of
end;// procedure TfrmFindConfigCode.edCCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

(*---------------------------------------------------------
  Selektiert das Element im Tree
----------------------------------------------------------*)
procedure TfrmFindConfigCode.lbCCElementsClick(Sender: TObject);
begin
  if lbCCElements.ItemIndex >= 0 then
    SelectObject(lbCCElements.Items.Objects[lbCCElements.ItemIndex]);
end;// procedure TfrmFindConfigCode.lbCCElementsDblClick(Sender: TObject);

(*---------------------------------------------------------
  Selectiert im Treeview das MainWindow das aktuelle Element
----------------------------------------------------------*)
function TfrmFindConfigCode.SelectObject(AObject: Pointer): Boolean;
var
  xActNode: TTreeNode;
begin
  result := false;
  if assigned(AObject) and assigned(mTree) then begin
    // Vom ersten Knoten nach unten
    xActNode := mTree.Items.GetFirstNode;
    result := false;
    while (assigned(xActNode)) and (not(result)) do begin
      // aktuellen Knoten selektiren, wenn auf das selbe MMElement wie vorher verwiesen wird
      if xActNode.Data = AObject then begin
        mTree.Selected := xActNode;
        mTree.Selected.MakeVisible;
        mTree.TopItem  := xActNode;
        result := true;
      end;// if xActNode.Data = AObject then begin
      // W�hlt den n�chsten Knoten (unabh�ng von der aktuellen Ebene)
      xActNode := xActNode.GetNext;
    end;// while (assigned(xActNode)) and (not(xFound)) do begin
  end;// if assigned(aObject) and assigned(mTree) then begin;
end;// function TfrmFindConfigCode.SelectObject(AObject: Pointer): Boolean;

(*---------------------------------------------------------
  Position des Formulars wieder herstellen
----------------------------------------------------------*)
procedure TfrmFindConfigCode.FormShow(Sender: TObject);
begin
  if assigned(FSettings) then
    FSettings.LoadForm(self, self.name, true);
end;// procedure TfrmFindConfigCode.FormShow(Sender: TObject);

(*---------------------------------------------------------
  Position des Formulars speichern
----------------------------------------------------------*)
procedure TfrmFindConfigCode.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if assigned(FSettings) then
    FSettings.SaveForm(self, self.name);
end;// procedure TfrmFindConfigCode.FormCloseQuery(Sender: TObject; var CanClose: Boolean);

end.
