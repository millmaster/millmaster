program Schematool;

uses
  Forms,
  MainForm in 'MainForm.pas' {frmMain},
  MSXML2_TLB in '..\..\..\Common\MSXML2_TLB.pas',
  XMLMapping in 'XMLMapping.PAS',
  XMLMappingDef in '..\..\..\Common\XMLMappingDef.pas',
  FrameBaseFunction in 'FrameBaseFunction.pas' {fraBaseFunction: TFrame},
  FrameCollision in 'FrameCollision.pas' {fraCollision: TFrame},
  FrameMemUsage in 'FrameMemUsage.pas' {fraMemUsage: TFrame},
  NewMapInputForm in 'NewMapInputForm.pas' {frmNewMapInputForm},
  ChooseMapfile in 'ChooseMapfile.pas' {frmChooseMapfile},
  ExportMapFile in 'ExportMapFile.pas' {frmExportMapFile},
  SchematoolGlobal in 'SchematoolGlobal.pas',
  MapElementInfo in 'MapElementInfo.pas' {frmMapElementInfo},
  CompareMapfile in 'CompareMapfile.pas' {frmCompareMapfile},
  CompareMapfilesSettings in 'CompareMapfilesSettings.pas' {frmCompareMapfilesSettings},
  FindConfigCode in 'FindConfigCode.pas' {frmFindConfigCode},
  Importer in 'Importer.pas' {frmImporter};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
