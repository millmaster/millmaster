object frmNewMapInputForm: TfrmNewMapInputForm
  Left = 486
  Top = 157
  BorderStyle = bsDialog
  Caption = 'Neue Map erstellen'
  ClientHeight = 353
  ClientWidth = 346
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 13
    Top = 132
    Width = 129
    Height = 13
    Caption = 'Name des neuen Map Files'
    FocusControl = edMapName
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel21: TmmLabel
    Left = 14
    Top = 217
    Width = 65
    Height = 13
    Caption = 'Beschreibung'
    FocusControl = memoDescription
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel
    Left = 14
    Top = 177
    Width = 45
    Height = 13
    Caption = 'Kategorie'
    FocusControl = cobCategory
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel5: TmmLabel
    Left = 247
    Top = 129
    Width = 87
    Height = 13
    Caption = 'Laufende Nummer'
    FocusControl = edNummber
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 167
    Top = 129
    Width = 66
    Height = 13
    Caption = 'Hauptnummer'
    FocusControl = edMainNumber
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object memoDescription: TmmMemo
    Left = 14
    Top = 232
    Width = 321
    Height = 73
    TabOrder = 1
    Visible = True
    AutoLabel.Control = mmLabel21
    AutoLabel.LabelPosition = lpTop
  end
  object mmButton1: TmmButton
    Left = 89
    Top = 320
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton2: TmmButton
    Left = 185
    Top = 320
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Abbrechen'
    ModalResult = 2
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object cobCategory: TmmComboBox
    Left = 14
    Top = 192
    Width = 131
    Height = 21
    Color = clWindow
    ItemHeight = 13
    TabOrder = 0
    Visible = True
    AutoLabel.Control = mmLabel3
    AutoLabel.LabelPosition = lpTop
    Edit = True
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object rbNamePrefix: TmmRadioGroup
    Left = 14
    Top = 8
    Width = 107
    Height = 113
    Caption = 'Ziel'
    ItemIndex = 0
    Items.Strings = (
      'ZE'
      'WSC'
      'LZE'
      'Test')
    TabOrder = 4
    OnClick = rbNamePrefixClick
    CaptionSpace = True
  end
  object rbNamePostfix: TmmRadioGroup
    Left = 150
    Top = 8
    Width = 171
    Height = 113
    Caption = 'Typ'
    ItemIndex = 0
    Items.Strings = (
      'Settings'
      'Bauzustand')
    TabOrder = 5
    OnClick = rbNamePostfixClick
    CaptionSpace = True
  end
  object edMapName: TmmEdit
    Left = 13
    Top = 147
    Width = 132
    Height = 21
    Color = clBtnFace
    ReadOnly = True
    TabOrder = 6
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
    Decimals = -1
    NumMode = False
    ReadOnlyColor = clBtnFace
    ShowMode = smExtended
    ValidateMode = [vmInput]
  end
  object edNummber: TmmSpinEdit
    Left = 247
    Top = 144
    Width = 58
    Height = 22
    MaxValue = 99
    MinValue = 0
    TabOrder = 7
    Value = 0
    Visible = True
    OnChange = edNummberChange
    AutoLabel.Control = mmLabel5
    AutoLabel.LabelPosition = lpTop
  end
  object edMainNumber: TmmSpinEdit
    Left = 167
    Top = 144
    Width = 66
    Height = 22
    MaxValue = 99
    MinValue = 1
    TabOrder = 8
    Value = 1
    Visible = True
    OnChange = edNummberChange
    AutoLabel.Control = mmLabel2
    AutoLabel.LabelPosition = lpTop
  end
end
