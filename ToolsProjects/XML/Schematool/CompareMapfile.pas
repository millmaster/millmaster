unit CompareMapfile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, XMLGlobal,
  SchematoolGlobal, ConfigurationPersistence, ImgList, mmImageList,
  ActnList, mmActionList, ToolWin, ComCtrls, mmToolBar, ExtCtrls, mmPanel,
  VirtualTrees, mmVirtualStringTree, activex, MapElementObserver, XMLMapping,
  typinfo, StdCtrls, mmMemo, mmImage, mmRichEdit, mmSplitter, Menus,
  VTHeaderPopup;

type
  TCompareMapfileColumn = class;
  TMapTree = class;
  TfrmCompareMapfile = class(TForm)
    mmPanel1: TmmPanel;
    mmToolBar1: TmmToolBar;
    mmActionList1: TmmActionList;
    mImageList: TmmImageList;
    acClose: TAction;
    ToolButton1: TToolButton;
    acSettings: TAction;
    ToolButton2: TToolButton;
    acSetColWidth: TAction;
    ToolButton3: TToolButton;
    mDescriptionPanel: TmmPanel;
    mmPanel8: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmRichEdit;
    mmSplitter1: TmmSplitter;
    mmVirtualStringTree1: TmmVirtualStringTree;
    vstHeaderMenu: TVTHeaderPopupMenu;
    test1: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure acCloseExecute(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure acSettingsExecute(Sender: TObject);
    procedure acSetColWidthExecute(Sender: TObject);
    procedure vstHeaderDraggedOut(Sender: TVTHeader;
      Column: TColumnIndex; DropPosition: TPoint);
    procedure vstHeaderMenuPopup(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FLoepfeBody: TLoepfeBody;
    mMapElementChangeObserver: TMapElementChangeObserver;
    FSettings: TSchematoolSettings;
    mCurrentElement: TMMElement;
    mDragNode: PVirtualNode;
    mItemList: TStringList;
    vst: TMapTree;
    procedure AddNewElement(aElementName: string);
    function GetPropertyString(aIndex: integer; aMMElement: TMMElement): string;
    procedure RenewVTNodes;
    procedure SetRootCount;
  protected
    procedure vstDragAllowed(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
    procedure vstDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject; Formats: TFormatArray; Shift:
        TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
    procedure vstDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState; State: TDragState; Pt: TPoint;
        Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
    procedure vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType; var
        CellText: WideString);
    procedure vstAfterCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellRect: TRect);
    procedure vstBeforeCellPaint(Sender: TBaseVirtualTree; TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
        CellRect: TRect);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function AddMapfile(aMapname: string): TVirtualTreeColumn;
    procedure SetOptimalColWidths;
    procedure ChangeElement(aTo: TBaseElement);
    function CreateMapfileStringList: TStringList;
    function GetMapCol(aIndex: integer): TCompareMapfileColumn;
    procedure NewMapfileMenuClick(Sender: TObject);
    procedure RemoveElement(aIndex: integer);
    procedure ShowCompareMapfiles(aObserver: TMapElementChange);
    property LoepfeBody: TLoepfeBody read FLoepfeBody write FLoepfeBody;
    property Settings: TSchematoolSettings read FSettings write FSettings;
  end;

  (*---------------------------------------------------------
    Eigene Klasse um in den Spalten ein MapElement zur Verf�gung zu haben
  ----------------------------------------------------------*)
  TCompareMapfileColumn = class(TVirtualTreeColumn)
  private
    FMMElement: TMMElement;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property MMElement: TMMElement read FMMElement;
  end;

  TMapTree = class(TmmVirtualStringTree)
  protected
    function GetColumnClass: TVirtualTreeColumnClass; override;
  end;

const
  cAtrCompareMapfileVisible = 'CompareMapfileVisible';
  cAtrMapIDFilter           = 'MapIDFilter';
  cAtrMapIDCategory         = 'MapCategory';

implementation

uses
  mmcs, CompareMapfilesSettings, MSXML2_TLB;

{$R *.DFM}

const
  cAtrItems      = 'Items';
  cAtrDocHeight  = 'DocumentationHeight';
  cColElement    = 'CompareMapfileColumn';
    cAtrColPos   = 'Position';
    cAtrColWidth = 'Width';
    cAtrColMap   = 'Mapfile';
  cXPColElement  = cXPFormElement + '/' + cColElement;

(*---------------------------------------------------------
  Map Element erzeugen
----------------------------------------------------------*)
constructor TCompareMapfileColumn.Create(Collection: TCollection);
begin
  inherited;
  fMMElement := TMMElement.Create;
end;// constructor TCompareMapfileColumn.Create(Collection: TCollection);

(*---------------------------------------------------------
  Element wieder freigeben
----------------------------------------------------------*)
destructor TCompareMapfileColumn.Destroy;
begin
  FreeAndNil(fMMElement);
  inherited;
end;// destructor TCompareMapfileColumn.Destroy;
//: -------------------------------------------------------------------------------------



(*---------------------------------------------------------
  Gibt die zu verwendende Spaltenklasse zur�ck
----------------------------------------------------------*)
function TMapTree.GetColumnClass: TVirtualTreeColumnClass;
begin
  Result := TCompareMapfileColumn;
end;
//: -------------------------------------------------------------------------------------



(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TfrmCompareMapfile.Create(aOwner: TComponent);
begin
  inherited;

  (* Das Tree erst zur Laufzeit erzeugen, damit die Splatenklasse f�r den Header
     selber bestimmt werden kann. *)
  vst := TMapTree.Create(Self);
  with vst do begin
    Parent := mmPanel1;
    Align := alClient;
    DragType := dtVCL;
    Header.Options := [hoColumnResize, hoDrag, hoDblClickResize, hoVisible];
    Indent := 0;
    TreeOptions.MiscOptions := [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning];
    TreeOptions.AutoOptions := [toAutoDropExpand, toAutoScrollOnExpand, toAutoTristateTracking];
    TreeOptions.PaintOptions := [toHideFocusRect, toShowDropmark, toShowHorzGridLines, toShowRoot, toShowVertGridLines, toThemeAware, toUseBlendedImages];
    TreeOptions.SelectionOptions := [toFullRowSelect, toRightClickSelect];
    // Verschieben von Zeilen
    OnDragAllowed := vstDragAllowed;
    OnDragOver := vstDragOver;
    OnDragDrop := vstDragDrop;
    // Anzeige
    OnGetText := vstGetText;
    // "FixedRow"
    OnAfterCellPaint := vstAfterCellPaint;
    // Hntergrund der Zelle
    OnBeforeCellPaint := vstBeforeCellPaint;
    // L�schen einer Spalte
    OnHeaderDraggedOut := vstHeaderDraggedOut;
    Header.PopupMenu := vstHeaderMenu;
    with Header.Columns.Add do begin
      Position := 0;
      Width := 100;
      Text := 'Items';
    end;// with Header.Columns.Add do begin
  end;// with vst do begin

  // Liste mit den Anzuzeigenden Eigenschaften des jeweiligen Elements
  mItemList    := TStringList.Create;

  // Observer der benachrichtigt wird, wenn im Hauptfenster das Element gewechselt wird
  mMapElementChangeObserver := TMapElementChangeObserver.Create;
  mMapElementChangeObserver.OnChangeMapElement := ChangeElement;
  mMapElementChangeObserver.Enabled := true;

  mCurrentElement := TMMElement.Create;
end;// constructor TfrmCompareMapfile.Create(aOwner: TComponent);

(*---------------------------------------------------------
  Aufr�umen
----------------------------------------------------------*)
destructor TfrmCompareMapfile.Destroy;
begin
  FreeAndNil(mCurrentElement);
  FreeAndNil(mItemList);
  FreeAndNil(mMapElementChangeObserver);
  inherited;
end;// destructor TfrmCompareMapfile.Destroy;

(*---------------------------------------------------------
  Settings laden
----------------------------------------------------------*)
procedure TfrmCompareMapfile.FormShow(Sender: TObject);
var
  xCol: TVirtualTreeColumn;
  xTitle: string;
  xColNode: IXMLDOMNode;
  i: Integer;
  xFormElement: IXMLDOMElement;
begin
  if assigned(Settings) then
    xFormElement := Settings.LoadForm(self, self.name);

  if assigned(xFormElement) then begin
    // L�dt die Konfigurierten Mapfiles und Splaten
    i := 0;
    repeat
      // Selektiert das Element f�r die Spalte an der entsprechenden Position
      xColNode := xFormElement.SelectSingleNode(Format('%s[@%s=%d]', [cColElement, cAtrColPos, i]));
      if assigned(xColNode) then begin
        // Holt das Mapfile das mit der Splate verbundne ist
        xTitle := AttributeToStringDef(cAtrColMap, xColNode, '');

        // Ohne Title ist es die "Items" Spalte
        if xTitle > '' then
          xCol := AddMapfile(xTitle)
        else
          xCol := GetMapCol(0);

        // Die Eigenschaften der Spalte setzen
        if assigned(xCol) then begin
          // Die Position ergibt sich aus der Iteration
          xCol.Position := i;
          xCol.Width := AttributeToIntDef(cAtrColWidth, xColNode, xCol.Width);
        end;// if assigned(xCol) then begin
      end;// if assigned(xColElement) then begin
      // N�chste Spalte
      inc(i);
    until not(assigned(xColNode));
  end;// if assigned(xFormElement) then begin

  // Angezeigte Items Laden
  with TStringList.Create do try
    Commatext := AttributeToStringDef(cAtrItems, xFormElement, '');

    // Wenn keine Elemente selektiert, dann alle hinzuf�gen
    if Count = 0 then begin
      for i := 0 to High(cAvailableItems) do
        AddNewElement(cAvailableItems[i].DisplayName);
    end else begin
      for i := 0 to Count - 1 do
        AddNewElement(Strings[i]);
    end;// if Count = 0 then begin
  finally
    free;
  end;// with TStringList.Create do try

  // H�he des Memos f�r die Beschreibung des Elements
  mDescriptionPanel.Height := AttributeToIntDef(cAtrDocHeight, xFormElement, mDescriptionPanel.Height);
end;// procedure TfrmCompareMapfile.FormShow(Sender: TObject);

(*---------------------------------------------------------
  Einstellungen speichern
----------------------------------------------------------*)
procedure TfrmCompareMapfile.FormHide(Sender: TObject);
var
  xColElement: IXMLDOMElement;
  i: Integer;
  xFormElement: IXMLDOMElement;
begin
  if assigned(Settings) then begin
    xFormElement := Settings.SaveForm(self, self.name);

    // L�scht alle Childs (sofern vorhanden)
    with xFormElement.SelectNodes(cColElement) do begin
      for i := 0 to length - 1 do
        xFormElement.removeChild(item[i]);
    end;// with xFormElement.SelectNodes(cColElement) do begin

    for i := 0 to vst.Header.Columns.Count - 1 do begin
      xColElement := xFormElement.ownerDocument.createElement(cColElement);
      xColElement.setAttribute(cAtrColPos, integer(GetMapCol(i).Position));
      xColElement.setAttribute(cAtrColWidth, GetMapCol(i).Width);
      // F�r die Items Spalte keinen Title speichern
      if i > 0 then
        xColElement.setAttribute(cAtrColMap, GetMapCol(i).Text);
      xFormElement.appendChild(xColElement);
    end;// for i := 0 to vst.Header.Columns.Count - 1 do begin
  end;// if assigned(Settings) then begin

  // Angezeigte Items
  xFormElement.setAttribute(cAtrItems, mItemList.Commatext);

  // H�he des Memos f�r die Beschreibung des Elements
  xFormElement.setAttribute(cAtrDocHeight, mDescriptionPanel.Height);
end;// procedure TfrmCompareMapfile.FormHide(Sender: TObject);

(*---------------------------------------------------------
  Dialog schliessen
----------------------------------------------------------*)
procedure TfrmCompareMapfile.acCloseExecute(Sender: TObject);
begin
  Close;
end;// procedure TfrmCompareMapfile.acCloseExecute(Sender: TObject);

(*---------------------------------------------------------
  F�gt ein neues Mapfile hinzu
----------------------------------------------------------*)
function TfrmCompareMapfile.AddMapfile(aMapname: string): TVirtualTreeColumn;
begin
  result := nil;

  // Pr�ft, ob bereits eine Spalte mit dem entsprechenden Mapfile existiert (DragDrop)
  with CreateMapfileStringList do try
    if (IndexOf(aMapname) <= 0) then begin
      result := VST.Header.Columns.Add;
      with result do begin
        Text  := aMapname;
        Width := 150;
      end;// with result do begin

      ChangeElement(mCurrentElement);
      RenewVTNodes;
    end;// if (IndexOf(aMapname) <= 0) then begin
  finally
    free;
  end;// with CreateMapfileStringList do try
end;// procedure TfrmCompareMapfile.AddMapfile(aMapname: string);

(*---------------------------------------------------------
  F�gt ein neues Element zur Liste hinzu
----------------------------------------------------------*)
procedure TfrmCompareMapfile.AddNewElement(aElementName: string);
var
  i: Integer;
  xIndex: Integer;
begin
  // Nur wenn das Element noch nicht in der Liste ist
  if mItemList.IndexOf(aElementName) < 0 then begin
    xIndex := -1;

    // sucht den Index im Array
    i := 0;
    while (i < Length(cAvailableItems)) and(xIndex < 0) do begin
      if AnsiSameText(cAvailableItems[i].DisplayName, aElementName) then
        xIndex := i;
      inc(i);
    end;// while (i < Length(cAvailableItems)) and(xIndex < 0) do begin

    mItemList.AddObject(aElementName, TObject(xIndex));
    RenewVTNodes;
  end;// if mItemList.IndexOf(aElementName) < 0 then begin
end;// procedure TfrmCompareMapfile.AddNewElement(aElementName: string);

(*---------------------------------------------------------
  Observer Methode.
  Diese Methode wird aufgerufen, wenn das selektierte Element �ndert
----------------------------------------------------------*)
procedure TfrmCompareMapfile.ChangeElement(aTo: TBaseElement);
var
  i: Integer;

  (*---------------------------------------------------------
    Setzt die neue MapID
  ----------------------------------------------------------*)
  procedure SetMapID(aCurrentElement: TMMElement; aMapID: string);
  begin
    if assigned(aCurrentElement) then begin
      aCurrentElement.MapID := '';
      aCurrentElement.Clear;
      aCurrentElement.DOMElement := aTo.DOMElement;
      aCurrentElement.MapID := aMapID;
    end;// if assigned(aElement) then begin
  end;// procedure SetMapID(aCurrentElement: TMMElement; aMapID: string);

begin
  if (aTo is TMMElement) then begin
    // In der ersten Spalte stehen die Itemnamen, darum erst ab zweiter Spalte
    for i := 1 to vst.Header.Columns.Count - 1 do
      SetMapID(GetMapCol(i).MMElement, GetMapCol(i).Text);
    SetMapID(mCurrentElement, aTo.MapID);
    Caption := aTo.ElementName;
    mDescriptionMemo.Text := aTo.Documentation;
  end;// if (aTo is TMMElement) then begin
  vst.Invalidate;
end;// procedure TfrmCompareMapfile.ChangeElement(aTo: TBaseElement);

(*---------------------------------------------------------
  Entfernt ein Element aus der Liste
----------------------------------------------------------*)
procedure TfrmCompareMapfile.RemoveElement(aIndex: integer);
begin
  if aIndex < mItemList.Count then
    mItemList.Delete(aIndex);
  RenewVTNodes;
end;// procedure TfrmCompareMapfile.RemoveElement(aIndex: integer);

(* -----------------------------------------------------------
  Baut das Tree neu auf
-------------------------------------------------------------- *)
procedure TfrmCompareMapfile.RenewVTNodes;
begin
  SetRootCount;
  vst.ReinitNode(vst.RootNode, true);
  // Tree neu zeichen um die neue Sortierung anzuzeigen
  vst.Invalidate;
end;// procedure TfrmMainWindow.RenewVTNodes;

(* -----------------------------------------------------------
  Aktualisiert den RootCount des Trees
-------------------------------------------------------------- *)  
procedure TfrmCompareMapfile.SetRootCount;
begin
  vst.RootNodeCount := mItemList.Count;
end;// procedure TfrmMainWindow.SetRootCount;

(*---------------------------------------------------------
  Textanzeige der einzelenne Zellen
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xCurrentElement: TMMElement;
  xLineIndex: Cardinal;
begin
  CellText :='';
  // Zeile abfragen
  xLineIndex := Sender.AbsoluteIndex(Node);
  if (xLineIndex < cardinal(mItemList.Count)) then begin
    case Column of
      0: CellText := mItemList[xLineIndex];
    else
      // Alle Spalten die oben nicht behandelt wurden, stellen Mapinformationen dar
      xCurrentElement := TCompareMapfileColumn(vst.Header.Columns[Column]).MMElement;
      if xCurrentElement.Active then
        CellText := GetPropertyString(integer(mItemList.Objects[xLineIndex]), xCurrentElement);
    end;// case Column of
  end;// if (xLineIndex <= mItemList.Count) then begin
end;// procedure TfrmCompareMapfile.vstGetText( ...

(*---------------------------------------------------------
  Gibt die gew�nschte Spalte zur�ck
----------------------------------------------------------*)
function TfrmCompareMapfile.GetMapCol(aIndex: integer): TCompareMapfileColumn;
begin
  Result := TCompareMapfileColumn(VST.Header.Columns[aIndex]);
end;// function TfrmCompareMapfile.GetMapCol(aIndex: integer): TVirtualTreeColumn;

(*---------------------------------------------------------
  Zeigt das Form an. Gleichzeitig kann mit dem Observer auf das jeweils selektierte Element reagiert werden
----------------------------------------------------------*)
procedure TfrmCompareMapfile.ShowCompareMapfiles(aObserver: TMapElementChange);
begin
  if assigned(aObserver) then
    aObserver.RegisterObserver(mMapElementChangeObserver);
  show;
end;// procedure TfrmCompareMapfile.ShowCompareMapfiles(aObserver: TMapElementChange);

(*---------------------------------------------------------
  Die einzelenen Elemente d�rfen verschoben werden. Nicht jedoch der Titel
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstDragAllowed(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := true;
  mDragNode := Node;
end;// procedure TfrmCompareMapfile.vstDragAllowed(Sender: TBaseVirtualTree...

(*---------------------------------------------------------
  Testet ob das Ziel zul�ssig ist. Als Ziel sind nur Elementzeilen erlaubt.
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstDragOver(Sender: TBaseVirtualTree; Source: TObject; Shift: TShiftState;
  State: TDragState; Pt: TPoint; Mode: TDropMode; var Effect: Integer; var Accept: Boolean);
begin
  Accept := false;
  // Kein Ole DragDrop
  if Sender = Source then
    Accept := true;

  if Source is TListBox then begin
    if TListBox(Source).name = 'lbMapID' then
      Accept := true;
  end;// if Source is TListBox then begin
end;// procedure TfrmCompareMapfile.vstDragOver(Sender: TBaseVirtualTree...

(*---------------------------------------------------------
  Loslassen der Maustaste --> Ablegen des Nodes
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject;
  Formats: TFormatArray; Shift: TShiftState; Pt: TPoint; var Effect: Integer; Mode: TDropMode);
var
  xTargetNode: PVirtualNode;
  xSourceIndex: cardinal;
begin
  // Nur innerhalb des Trees erlaubt
  if Sender = Source then begin
    // Index des "gedraggten" Nodes
    xSourceIndex := VST.AbsoluteIndex(mDragNode);
    // Holt den Zielknoten
    xTargetNode := Sender.GetNodeAt(PT.x, Pt.y);
    // Je nach Zielmodus, den Eintrag an den entsprechenden Ort verschieben
    case Mode of
      dmNowhere: mItemList.Move(xSourceIndex, mItemList.Count - 1);
      dmAbove,
      dmOnNode:  mItemList.Move(xSourceIndex, Sender.AbsoluteIndex(xTargetNode));
      dmBelow:   mItemList.Move(xSourceIndex, Sender.AbsoluteIndex(xTargetNode) + 1);
    else
      MessageDlg('Verschieben Nicht erlaubt (Modus unbekannt)', mtError, [mbOK], 0);
    end;// case Mode of
    Sender.ClearSelection;
  end;// if xSourceIndex > 0 then begin
  // Tree neu zeichnen
  RenewVTNodes;

  if Source is TListBox then begin
    if TListBox(Source).name = 'lbMapID' then
      AddMapfile(TListBox(Source).Items[TListBox(Source).Itemindex]);
  end;// if Source is TListBox then begin
end;// procedure TfrmCompareMapfile.vstDragDrop(Sender: TBaseVirtualTree; Source: TObject; DataObject: IDataObject

(*---------------------------------------------------------
  Einstellungen anzeigen
----------------------------------------------------------*)
procedure TfrmCompareMapfile.acSettingsExecute(Sender: TObject);
var
  i: Integer;
  xMapfileList: TStringList;
  xOriginalMapfiles: TStringList;
begin
  xOriginalMapfiles := nil;
  xMapfileList := nil;
  with TfrmCompareMapfilesSettings.Create(nil) do try
    Settings := FSettings;

    // Mapfileliste erzeugen (Stringliste mit den Namen der aktuellen Mapfiles)
    xMapfileList := CreateMapfileStringList;

    xOriginalMapfiles := TStringList.Create;
    xOriginalMapfiles.assign(xMapfileList);

    ShowForm(mItemList, xMapfileList, FLoepfeBody);

    (* Jetzt die entsprechenden Mapfiles anzeigen oder ausblenden
       Danach sind in der Original Liste die zu l�schenden Mapfiles un in
       der Mapfilelist die neuen. *)
    for i := xMapfileList.Count - 1 downto 0 do begin
      // Wenn ein Eintrag in beiden Listen vorkommt, dann den Eintrag in beiden Listen l�schen
      if xOriginalMapfiles.IndexOf(xMapfileList[i]) >= 0 then begin
        xOriginalMapfiles.Delete(xOriginalMapfiles.IndexOf(xMapfileList[i]));
        xMapfileList.Delete(i);
      end;// if xOriginalMapfiles.IndexOf(xMapfileList[i]) >= 0 then begin
    end;// for i := xMapfileList.Count - 1 downto 0 do begin

    // L�schen der "unerw�nschten Maps"
    for i := vst.Header.Columns.Count - 1 downto 1 do begin
      if xOriginalMapfiles.IndexOf(GetMapCol(i).Text) >= 0 then
        vst.Header.Columns.Delete(i);
    end;// for i := 1 to vst.Header.Columns.Count - 1 do begin

    // Anzeigen der "neuen" Maps
    for i := 0 to xMapfileList.Count - 1 do
      AddMapfile(xMapfileList[i]);
  finally
    Free;
    FreeAndNil(xMapfileList);
    FreeAndNil(xOriginalMapfiles);
    RenewVTNodes;
  end;// with TfrmCompareMapfilesSettings.Create(nil) do try
end;// procedure TfrmCompareMapfile.acSettingsExecute(Sender: TObject);

(*---------------------------------------------------------
  Justiert die Breite der Spalten
----------------------------------------------------------*)
procedure TfrmCompareMapfile.SetOptimalColWidths;
begin
  vst.Header.AutoFitColumns;
end;// procedure TfrmCompareMapfile.SetOptimalColWidths;

(*---------------------------------------------------------
  Gibt das Property mit dem entsprechenden Index als String zur�ck
----------------------------------------------------------*)
function TfrmCompareMapfile.GetPropertyString(aIndex: integer; aMMElement: TMMElement): string;
var
  xClass: TObject;
begin
  result := '';
  if (aIndex >= Low(cAvailableItems)) and (aIndex <= High(cAvailableItems)) then begin
    result := 'nicht unterst�tzt';
    case cAvailableItems[aIndex].PropType of
      tkInteger,
      tkString,
      tkEnumeration: result := GetPropValue(aMMElement, cAvailableItems[aIndex].PropName);
      tkClass: begin
        result := 'Klasse unbekannt';
        xClass := GetObjectProp(aMMElement, cAvailableItems[aIndex].PropName);
        if assigned(xClass) then begin
          if xClass is TStrings then
            result := TStrings(xClass).Text;
        end;// if assigend(xClass) then begin
      end;// tkClass: begin
    end;// case cAvailableItems[aItemIndex].PropType of
  end;// if (aIndex >= Low(cAvailableItems)) and (aIndex <= High(cAvailableItems)) then begin
  result := Trim(Result);
end;// function TfrmCompareMapfile.GetPropertyString(aIndex: integer; aMMElement: TMMElement): string;

(*---------------------------------------------------------
  Zeichent die erste Splate wie FixedRow im Stringgrid
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstAfterCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;  CellRect: TRect);
var
  xLineIndex: Cardinal;
begin
  if Column = 0 then begin
    if Sender is TVirtualStringTree then begin
      with TargetCanvas do begin
        // Simuliert die Anzeige einer FixedRow wie in TStringGrid
        if toShowVertGridLines in TVirtualStringTree(Sender).TreeOptions.PaintOptions then
          Inc(CellRect.Right);
        if toShowHorzGridLines in TVirtualStringTree(Sender).TreeOptions.PaintOptions then
          Inc(CellRect.Bottom);
        // Zeichnet eine 3D Fl�che
        DrawEdge(Handle, CellRect, BDR_RAISEDINNER, BF_RECT or BF_MIDDLE);
        // Text wieder ausgeben
        xLineIndex := Sender.AbsoluteIndex(Node);
        if (xLineIndex < cardinal(mItemList.Count)) then
          TargetCanvas.TextOut(CellRect.Left, CellRect.Top, mItemList[xLineIndex]);
      end;// with TargetCanvas do begin
    end;// if Sender is TVirtualStringTree then begin
  end;// if Column = 0 then begin
end;// procedure TfrmCompareMapfile.vstAfterCellPaint(Sender: TBaseVirtualTree;

(*---------------------------------------------------------
  Zeichnet den Hintergrund einer Zelle
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex; CellRect: TRect);
var
  xCurrentProp: string;
  xCurrentElement: TMMElement;
  xLineIndex: integer;

  procedure PaintCellBkg(aColor: TColor);
  begin
    TargetCanvas.Brush.Color := aColor;
    TargetCanvas.FillRect(CellRect);
  end;

begin
  if Column > 0 then begin
    xLineIndex := Sender.AbsoluteIndex(Node);
    xCurrentElement := GetMapCol(Column).MMElement;
    if not(assigned(xCurrentElement)) or (not(xCurrentElement.Active)) then begin
      PaintCellBkg(clSilver);
    end else begin
      if (xCurrentElement.Active) then begin
        xCurrentProp := GetPropertyString(integer(mItemList.Objects[xLineIndex]), xCurrentElement);
        if not(AnsiSameText(xCurrentProp, GetPropertyString(integer(mItemList.Objects[xLineIndex]), mCurrentElement))) then
          PaintCellBkg($008AB9E3);
      end;// if (xCurrentElement.Active) then begin
    end;// if not(assigned(xCurrentElement)) or (not(xCurrentElement.Active)) then begin
  end;// if Column > 0 then begin
end;// procedure TfrmCompareMapfile.mBinGridBeforeCellPaint(Sender: TBaseVirtualTree;

(*---------------------------------------------------------
  Setzt die optimale Spaltenbreite f�r jede einzelne Spalte
----------------------------------------------------------*)
procedure TfrmCompareMapfile.acSetColWidthExecute(Sender: TObject);
begin
  SetOptimalColWidths;
end;// procedure TfrmCompareMapfile.acSetColWidthExecute(Sender: TObject);

(*---------------------------------------------------------
  Gibt eine Stringlist mit allen verwendeten Mapfiles zur�ck
  Der Aufrufer muss die Stringlist freigeben.
----------------------------------------------------------*)
function TfrmCompareMapfile.CreateMapfileStringList: TStringList;
var
  i: Integer;
begin
  result := TStringList.Create;
  for i := 1 to vst.Header.Columns.Count - 1 do
    result.Add(GetMapCol(i).Text);
end;

(*---------------------------------------------------------
  F�gt ein neues Mapfile zur Liste hinzu
----------------------------------------------------------*)
procedure TfrmCompareMapfile.NewMapfileMenuClick(Sender: TObject);
begin
  // Shortcut der von Windows hinzugef�gt wird entfernen
  if Sender is TMenuItem then
    AddMapfile(StringReplace(TMenuItem(Sender).Caption, '&', '', [rfReplaceAll]));
end;// procedure TfrmCompareMapfile.NewMapfileMenuClick(Sender: TObject);

(*---------------------------------------------------------
  L�scht die gezogene Spalte
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstHeaderDraggedOut(
  Sender: TVTHeader; Column: TColumnIndex; DropPosition: TPoint);
var
  xRect: TRect;
begin
  xRect := vst.ClientRect;
  // Das Client Rechteck muss noch in Bildschirm Koordinaten umgerechnet werden
  OffsetRect(xRect, vst.ClientOrigin.x, ClientOrigin.y);
  // Wenn der DropPoint ausserhalb des Trees leigt, dann Spalte entfernen
  if not(ptInRect(xRect, DropPosition)) then
    vst.Header.Columns.Delete(Column);
end;// procedure TfrmCompareMapfile.vstHeaderDraggedOut(...

(*---------------------------------------------------------
  F�gt dem Popupmenu alle nicht verwendeten Mapfiles hinzu
----------------------------------------------------------*)
procedure TfrmCompareMapfile.vstHeaderMenuPopup(Sender: TObject);
var
  i: Integer;
  xNewMenu: TMenuItem;
  xFirstMenuAdded: boolean;
  xMapIDs: TStringList;
begin
  xMapIDs := nil;
  xFirstMenuAdded := false;
  if assigned(FLoepfeBody) then begin
    with CreateMapfileStringList do try
      xMapIDs := TStringList.Create;
      xMapIDs.Assign(FLoepfeBody.MapIds);
      xMapIDs.Sort;
      for i := 0 to xMapIDs.Count - 1 do begin
        if IndexOf(xMapIDs.Names[i]) < 0 then begin
          if not(xFirstMenuAdded) then begin
            xNewMenu := TMenuItem.Create(vstHeaderMenu);
            xNewMenu.Caption := '-';
            vstHeaderMenu.Items.Add(xNewMenu);
            xFirstMenuAdded := true;
          end;// if not(xFirstMenuAdded) then begin
          // Hier ist klar das das entsprecehnde Mapfile nicht verwendet wird
          xNewMenu := TMenuItem.Create(vstHeaderMenu);
          xNewMenu.Caption := xMapIDs.Names[i];
          xNewMenu.OnClick := NewMapfileMenuClick;
          vstHeaderMenu.Items.Add(xNewMenu);
        end;// if IndexOf(xMapIDs.Names[i]) < 0 then begin
      end;// for i := 0 to xMapIDs.Count do begin
    finally
      FreeAndNil(xMapIDs);
      free;
    end;// with CreateMapfileStringList do try
  end;// if assigned(FLoepfeBody) then begin
end;// procedure TfrmCompareMapfile.vstHeaderMenuPopup(Sender: TObject);

procedure TfrmCompareMapfile.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

end.
