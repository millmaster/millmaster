unit MapElementInfo;

interface



uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, MapElementObserver, XMLMapping,
  StdCtrls, mmLabel, mmComboBox, Buttons, mmBitBtn, ImgList, mmImageList,
  ActnList, mmActionList, ComCtrls, mmRichEdit, ExtCtrls, mmShape, ToolWin,
  mmToolBar, mmBevel, mmCheckBox;

type
  TfrmMapElementInfo = class(TForm)
    mmLabel1: TmmLabel;
    cbMapfile: TmmComboBox;
    mmActionList1: TmmActionList;
    acLoadMapfiles: TAction;
    mmImageList1: TmmImageList;
    mmBitBtn1: TmmBitBtn;
    mmLabel2: TmmLabel;
    laDataType: TmmLabel;
    mmLabel4: TmmLabel;
    laOffset: TmmLabel;
    mmLabel6: TmmLabel;
    laCount: TmmLabel;
    mmLabel8: TmmLabel;
    laDefault: TmmLabel;
    mmLabel10: TmmLabel;
    laMinIncl: TmmLabel;
    mmLabel12: TmmLabel;
    laMaxIncl: TmmLabel;
    mmLabel14: TmmLabel;
    laMinExcl: TmmLabel;
    mmLabel16: TmmLabel;
    laMaxExcl: TmmLabel;
    mmLabel18: TmmLabel;
    mmLabel19: TmmLabel;
    laBitOffset: TmmLabel;
    laBitCount: TmmLabel;
    mmLabel22: TmmLabel;
    mmLabel23: TmmLabel;
    memoEnum: TmmRichEdit;
    memoFormula: TmmRichEdit;
    acSetBasicData: TAction;
    acSetFacets: TAction;
    acSetBitValue: TAction;
    acSetEnum: TAction;
    acSetFormula: TAction;
    mmBitBtn2: TmmBitBtn;
    mmBitBtn3: TmmBitBtn;
    mmBitBtn4: TmmBitBtn;
    mmBitBtn5: TmmBitBtn;
    mmBitBtn6: TmmBitBtn;
    shFormula: TmmShape;
    shEnum: TmmShape;
    shMaxExcl: TmmShape;
    shMinExcl: TmmShape;
    shMaxIncl: TmmShape;
    shDefault: TmmShape;
    shCount: TmmShape;
    shByteOffset: TmmShape;
    shDataType: TmmShape;
    shMinIncl: TmmShape;
    shBitCount: TmmShape;
    shBitOffset: TmmShape;
    memoDescription: TmmRichEdit;
    shEvent: TmmShape;
    mmLabel3: TmmLabel;
    mmBitBtn7: TmmBitBtn;
    memoEvent: TmmRichEdit;
    acSetEvents: TAction;
    mmToolBar1: TmmToolBar;
    bCopyTo: TToolButton;
    bCopyFrom: TToolButton;
    cbWithType: TmmCheckBox;
    mmBevel1: TmmBevel;
    procedure cbMapfileChange(Sender: TObject);
    procedure acLoadMapfilesExecute(Sender: TObject);
    procedure acSetBasicDataExecute(Sender: TObject);
    procedure acSetFacetsExecute(Sender: TObject);
    procedure acSetBitValueExecute(Sender: TObject);
    procedure acSetEnumExecute(Sender: TObject);
    procedure acSetFormulaExecute(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure acSetEventsExecute(Sender: TObject);
    procedure bCopyFromClick(Sender: TObject);
  private
    FLoepfeBody: TLoepfeBody;
    mCurrentElement: TMMElement;
    mMapElementChangeObserver: TMapElementChangeObserver;
    function GetMapVersion: string;
    procedure SetMapVersion(const Value: string);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure ChangeElement(aTo: TBaseElement);
    procedure ShowMapInfo(aObserver: TMapElementChange);
    property LoepfeBody: TLoepfeBody read FLoepfeBody write FLoepfeBody;
    property MapVersion: string read GetMapVersion write SetMapVersion;
  published
  end;

implementation

uses
  LoepfeGlobal, MainForm, typinfo, XMLMappingDef;

{$R *.DFM}

function MainForm: TfrmMain;
begin
  result := nil;
  if Application.MainForm is TfrmMain then
    result := TfrmMain(Application.MainForm);
end;

constructor TfrmMapElementInfo.Create(aOwner: TComponent);
begin
  inherited;
  mMapElementChangeObserver := TMapElementChangeObserver.Create;
  mMapElementChangeObserver.OnChangeMapElement := ChangeElement;
  mMapElementChangeObserver.Enabled := true;
  mCurrentElement := TMMElement.Create;
end;

destructor TfrmMapElementInfo.Destroy;
begin
  // Unregistriert sich automatisch beim Observer
  FreeAndNil(mMapElementChangeObserver);
  FreeAndNil(mCurrentElement);
  inherited;
end;

procedure TfrmMapElementInfo.ChangeElement(aTo: TBaseElement);
var
  xNewMapID: string;
begin
  if (aTo is TMMElement) then begin

    xNewMapID := '';
    if cbMapfile.ItemIndex >= 0 then
      xNewMapID := cbMapfile.Items[cbMapfile.ItemIndex];

    mCurrentElement.MapID := '';
    mCurrentElement.Clear;
    mCurrentElement.DOMElement := aTo.DOMElement;
    mCurrentElement.MapID := xNewMapID;

    with mCurrentElement do begin
      Caption := ElementName;

      laDataType.Caption   := GetEnumName(TypeInfo(TDataType), ord(Datatype));
      laOffset.Caption     := intToStr(Offset);
      laCount.Caption      := intToStr(Count);
      laDefault.Caption    := Default;
      laMinIncl.Caption    := MinIncl;
      laMaxIncl.Caption    := MaxIncl;
      laMinExcl.Caption    := MinExcl;
      laMaxExcl.Caption    := MaxExcl;
      laBitOffset.Caption  := BitOffset;
      laBitCount.Caption   := BitCount;
      memoDescription.Text := Documentation;

      memoEnum.Lines.CommaText    := Enumeration;
      if ConvertPara > '' then
        memoFormula.Lines.CommaText := Format('%s:%s%s', [ConvertMode, cCRLF, ConvertPara])
      else
        memoFormula.Lines.CommaText := ConvertMode;

      memoEvent.Lines.Assign(Events);

      shDataType.visible   := (Datatype <> TMMElement(aTo).Datatype);
      shByteOffset.visible := (Offset <> TMMElement(aTo).Offset);
      shCount.visible      := (Count <> TMMElement(aTo).Count);
      shDefault.visible    := (Default <> TMMElement(aTo).Default);
      shMinIncl.visible    := (MinIncl <> TMMElement(aTo).MinIncl);
      shMaxIncl.visible    := (MaxIncl <> TMMElement(aTo).MaxIncl);
      shMinExcl.visible    := (MinExcl <> TMMElement(aTo).MinExcl);
      shMaxExcl.visible    := (MaxExcl <> TMMElement(aTo).MaxExcl);
      shBitOffset.visible  := (BitOffset <> TMMElement(aTo).BitOffset);
      shBitCount.visible   := (BitCount <> TMMElement(aTo).BitCount);
      shEnum.visible       := (Enumeration <> TMMElement(aTo).Enumeration);
      shFormula.visible    := (ConvertPara <> TMMElement(aTo).ConvertPara) or (ConvertMode <> TMMElement(aTo).ConvertMode);
      shEvent.visible      := (Events.CommaText <> TMMElement(aTo).Events.CommaText);
    end;// with do begin
  end else begin
    shEvent.visible      := false;
    shFormula.visible    := false;
    shEnum.visible       := false;
    shMaxExcl.visible    := false;
    shMinExcl.visible    := false;
    shMaxIncl.visible    := false;
    shDefault.visible    := false;
    shCount.visible      := false;
    shByteOffset.visible := false;
    shDataType.visible   := false;
    shMinIncl.visible    := false;
    shBitCount.visible   := false;
    shBitOffset.visible  := false;

    laDataType.Caption   := '';
    laOffset.Caption     := '';
    laCount.Caption      := '';
    laDefault.Caption    := '';
    laMinIncl.Caption    := '';
    laMaxIncl.Caption    := '';
    laMinExcl.Caption    := '';
    laMaxExcl.Caption    := '';
    laBitOffset.Caption  := '';
    laBitCount.Caption   := '';
    memoEnum.Text        := '';
    memoFormula.Text     := '';
    memoEvent.Text       := '';
    memoDescription.Text := '';
  end;// if (aTo is TMMElement) then begin
end;

procedure TfrmMapElementInfo.ShowMapInfo(aObserver: TMapElementChange);
begin
  if assigned(aObserver) then
    aObserver.RegisterObserver(mMapElementChangeObserver);
  acLoadMapfiles.Execute;
  Show;
end;

procedure TfrmMapElementInfo.cbMapfileChange(Sender: TObject);
begin
  ChangeElement(mCurrentElement);
end;

procedure TfrmMapElementInfo.acLoadMapfilesExecute(Sender: TObject);
var
  i: Integer;
begin
  if assigned(FLoepfeBody) then begin
    cbMapfile.Clear;
    for i := 0 to FLoepfeBody.MapIDs.Count - 1 do
      cbMapfile.Items.Add(CopyLeft(FLoepfeBody.MapIDs[i], '='));
  end;// if assigned(FLoepfeBody) then begin
end;

procedure TfrmMapElementInfo.acSetBasicDataExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      if cbWithType.checked then begin
        cobDataType.ItemIndex := cobDataType.Items.IndexOf(laDataType.Caption);
        edOffset.Text := laOffset.Caption;
      end;// if cbWithType.checked then begin
      edCount.Text   := laCount.Caption;
      edDefault.Text := laDefault.Caption;
    end else begin
      if mCurrentElement.Active then begin
        if cbWithType.checked then begin
          mCurrentElement.Datatype := TDataType(cobDataType.Items.Objects[cobDataType.ItemIndex]);
          mCurrentElement.Offset := edOffset.AsInteger;
        end;// if cbWithType.checked then begin
        mCurrentElement.Count   := edCount.Value;
        mCurrentElement.Default := edDefault.Text;
      end;// if mCurrentElement.Active then begin
      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    
    SetFocus;
  end;//
end;

procedure TfrmMapElementInfo.acSetFacetsExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      edMinIncl.Text := laMinIncl.Caption;
      edMaxIncl.Text := laMaxIncl.Caption;
      edMinExcl.Text := laMinExcl.Caption;
      edMaxExcl.Text := laMaxExcl.Caption;
    end else begin
      mCurrentElement.MinIncl := edMinIncl.Text;
      mCurrentElement.MaxIncl := edMaxIncl.Text;
      mCurrentElement.MinExcl := edMinExcl.Text;
      mCurrentElement.MaxExcl := edMaxExcl.Text;

      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    
    SetFocus;
  end;// with MainForm do begin
end;

procedure TfrmMapElementInfo.acSetBitValueExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      edBitOffset.Text := laBitOffset.Caption;
      edBitCount.Text  := laBitCount.Caption;
    end else begin
      mCurrentElement.BitOffset := edBitOffset.Text;
      mCurrentElement.BitCount  :=  edBitCount.Text;

      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    
    SetFocus;
  end;// with MainForm do begin
end;

procedure TfrmMapElementInfo.acSetEnumExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      lbEnumerationSelected.Items.Assign(memoEnum.Lines);
    end else begin
      mCurrentElement.Enumeration := memoEnum.Text;
      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    
    SetFocus;
  end;// with MainForm do begin
end;

procedure TfrmMapElementInfo.acSetFormulaExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      cobConvertMode.ItemIndex := cobConvertMode.Items.IndexOf(mCurrentElement.ConvertMode);
      lbConvertPara.Items.Text := mCurrentElement.ConvertPara;
    end else begin
      mCurrentElement.ConvertMode := cobConvertMode.Items[cobConvertMode.ItemIndex];
      mCurrentElement.ConvertPara := lbConvertPara.Items.Text;

      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    
    SetFocus;
  end;// with MainForm do begin
end;

procedure TfrmMapElementInfo.FormHide(Sender: TObject);
begin
  Release;
end;

function TfrmMapElementInfo.GetMapVersion: string;
begin
  Result := '';
  if cbMapfile.ItemIndex >= 0 then
    result := cbMapfile.Items[cbMapfile.ItemIndex];
end;

procedure TfrmMapElementInfo.SetMapVersion(const Value: string);
begin
  cbMapfile.ItemIndex := cbMapfile.Items.IndexOf(Value);
end;

procedure TfrmMapElementInfo.acSetEventsExecute(Sender: TObject);
begin
  with MainForm do begin
    if bCopyTo.Down then begin
      lbEvents.Items.Assign(memoEvent.Lines);
    end else begin
      mCurrentElement.Events.Assign(lbEvents.Items);
      mCurrentElement.Update;
      ChangeElement(mCurrentElement);
    end;// if bCopyTo.Down then begin
    SetFocus;
  end;// with MainForm do begin
end;

procedure TfrmMapElementInfo.bCopyFromClick(Sender: TObject);
begin
  if bCopyFrom.Down then
    MessageBox(0, '!!! ACHTUNG !!!'+#13+#10+''+#13+#10+'  Die �nderungen am Mapfile k�nnen nicht '+#13+#10+'  mehr r�ckg�ngig gemacht werden.', 'Gef�hrliche Funktion!!!', MB_ICONWARNING or MB_OK);
end;

end.



