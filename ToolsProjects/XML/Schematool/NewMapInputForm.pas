unit NewMapInputForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmComboBox, mmButton, mmMemo, mmLabel, mmEdit, mmStringList,
  ExtCtrls, mmRadioGroup, Spin, mmSpinEdit, XMLMapping;

type
  TfrmNewMapInputForm = class(TForm)
    mmLabel1: TmmLabel;
    memoDescription: TmmMemo;
    mmLabel21: TmmLabel;
    mmButton1: TmmButton;
    mmButton2: TmmButton;
    mmLabel3: TmmLabel;
    cobCategory: TmmComboBox;
    rbNamePrefix: TmmRadioGroup;
    rbNamePostfix: TmmRadioGroup;
    edMapName: TmmEdit;
    mmLabel5: TmmLabel;
    edNummber: TmmSpinEdit;
    mmLabel2: TmmLabel;
    edMainNumber: TmmSpinEdit;
    procedure rbNamePrefixClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure rbNamePostfixClick(Sender: TObject);
    procedure edNummberChange(Sender: TObject);
  private
    FLoepfeBody: TLoepfeBody;
    mPrefix: string;
    mPostfix: string;
    FSelectedCategory: string;
    function GetCategory: string;
    function GetDescription: string;
    function GetMapName: string;
    procedure GetNameFragments;
    procedure SetCategories(const aCategoryList: TMMStringList);
    procedure SetSelectedCategory(const Value: string);
    procedure UpdateMapName;
    { Private declarations }
  public
    procedure FillMapfileTypes;
    property Categories: TMMStringList write SetCategories;
    property Category: string read GetCategory;
    property Description: string read GetDescription;
    property LoepfeBody: TLoepfeBody write FLoepfeBody;
    property MapName: string read GetMapName;
    property SelectedCategory: string write SetSelectedCategory;
    { Public declarations }

  end;

var
  frmNewMapInputForm: TfrmNewMapInputForm;

implementation

uses
  SchematoolGlobal, XMLMappingDef;

{$R *.DFM}
//: ---------------------------------------------------------
function TfrmNewMapInputForm.GetCategory: string;
begin
  Result := cobCategory.Text;
end;

//: ---------------------------------------------------------
function TfrmNewMapInputForm.GetDescription: string;
begin
  Result := memoDescription.Text;
end;

//: ---------------------------------------------------------
function TfrmNewMapInputForm.GetMapName: string;
begin
  result := edMapName.Text;
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.SetCategories(const aCategoryList: TMMStringList);
var
  i: integer;
  xCategory: string;
begin
  cobCategory.Items.Clear;
  for i := 0 to aCategoryList.Count - 1 do begin
    xCategory := aCategoryList.Values[aCategoryList.Names[i]];
    if (xCategory > '') and (cobCategory.Items.IndexOf(xCategory) = -1) then
      cobCategory.Items.Add(xCategory);
  end;// for i := 0 to aCategoryList.Count - 1 do begin
  cobCategory.Text  := FSelectedCategory
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.SetSelectedCategory(const Value: string);
begin
  FSelectedCategory := Value;
  cobCategory.Text  := FSelectedCategory;
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.rbNamePrefixClick(Sender: TObject);
begin
  GetNameFragments;
  UpdateMapName;
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.UpdateMapName;
begin
  edMapName.Text := SchematoolGlobal.GetMapname(mPrefix, edMainNumber.Value, edNummber.Value, mPostfix);
//  edMapName.Text := Format('%s%.2d%.2d%s', [mPrefix, edMainNumber.Value, edNummber.Value, mPostfix]);
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.FormShow(Sender: TObject);
begin
  FillMapfileTypes;
  GetNameFragments;
  UpdateMapName;
end;

procedure TfrmNewMapInputForm.GetNameFragments;
begin
  edNummber.MinValue := 1;
  edNummber.Value    := 1;
  mPostfix := cMapfileSections[TMapSection(rbNamePostfix.ItemIndex)].Abbreviation;
  mPrefix := cPrefix[rbNamePrefix.ItemIndex];
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.rbNamePostfixClick(Sender: TObject);
begin
  GetNameFragments;
  UpdateMapName;
end;

//: ---------------------------------------------------------
procedure TfrmNewMapInputForm.edNummberChange(Sender: TObject);
begin
  UpdateMapName;
end;

procedure TfrmNewMapInputForm.FillMapfileTypes;
var
  i: TMapSection;
begin
  rbNamePostfix.Items.Clear;
  for i := low(TMapSection) to High(TMapSection) do
    rbNamePostfix.Items.Add(cMapfileSections[i].DisplayName);
  rbNamePostfix.ItemIndex := 0;
end;

end.
