unit MapElementObserver;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs, XMLMapping;

type
  TMapElementChange = class;
  TOnChangeMapElement = procedure (aTo: TBaseElement) of object;
  TMapElementChangeObserver = class(TObject)
  private
    fEnabled: Boolean;
    fOnChangeMapElement: TOnChangeMapElement;
    fParentObserver: TMapElementChange;
  public
    destructor Destroy; override;
    procedure DoChangeMapElement(aTo: TBaseElement);
    property OnChangeMapElement: TOnChangeMapElement read fOnChangeMapElement write fOnChangeMapElement;
    property ParentObserver: TMapElementChange read fParentObserver write fParentObserver;
  published
    property Enabled: Boolean read fEnabled write fEnabled;
  end;
  
  TMapElementChange = class(TObject)
  private
    FObservers: TList;
  public
    constructor Create;
    destructor Destroy; override;
    procedure ChangeElement(aTo: TBaseElement);
    procedure RegisterObserver(Observer: TMapElementChangeObserver);
    procedure UnregisterObserver(Observer: TMapElementChangeObserver);
  end;
  

implementation

//:-------------------------------------------------------------------
destructor TMapElementChangeObserver.Destroy;
begin
  if assigned(ParentObserver) then
    ParentObserver.UnregisterObserver(self);
end;// TMapElementChangeObserver.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TMapElementChangeObserver.DoChangeMapElement(aTo: TBaseElement);
begin
  if Assigned(fOnChangeMapElement) then fOnChangeMapElement(aTo);
end;// TMapElementChangeObserver.DoChangeMapElement cat:No category

//:-------------------------------------------------------------------
constructor TMapElementChange.Create;
begin
  inherited;
  FObservers := TList.Create;
end;// TMapElementChange.Create cat:No category

//:-------------------------------------------------------------------
destructor TMapElementChange.Destroy;
begin
  FreeAndNil(FObservers);
  inherited;
end;// TMapElementChange.Destroy cat:No category

//:-------------------------------------------------------------------
procedure TMapElementChange.ChangeElement(aTo: TBaseElement);
var
  xObs: TMapElementChangeObserver;
  i: Integer;
begin
  for i := 0 to FObservers.Count - 1 do begin
    xObs := FObservers[i];
    if xObs.Enabled then
      xObs.DoChangeMapElement(aTo);
  end;
end;// TMapElementChange.ChangeElement cat:No category

//:-------------------------------------------------------------------
procedure TMapElementChange.RegisterObserver(Observer: TMapElementChangeObserver);
begin
  if assigned(FObservers) and assigned(Observer) then begin
    Observer.ParentObserver := self;

//: ----------------------------------------------
    if FObservers.IndexOf(Observer) = -1 then
      FObservers.Add(Observer);

//: ----------------------------------------------
  end;// if assigned(FObservers) and assigned(Observer) then begin
end;// TMapElementChange.RegisterObserver cat:No category

//:-------------------------------------------------------------------
procedure TMapElementChange.UnregisterObserver(Observer: TMapElementChangeObserver);
begin
  FObservers.Remove(Observer);
end;// TMapElementChange.UnregisterObserver cat:No category




end.
