unit Importer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  XMLMapping, ActnList, ImgList, mmImageList, mmActionList, Buttons,
  mmSpeedButton, StdCtrls, mmLabel, mmComboBox, mmLineLabel, mmBitBtn,
  ExtCtrls, mmPanel, IvMlDlgs, mmOpenDialog, VirtualTrees,
  mmVirtualStringTree, inifiles, comctrls, StdActns, ToolWin, mmToolBar,
  mmMemo, mmImage;

type
  TfrmImporter = class(TForm)
    acnlst: TmmActionList;
    ilActions: TmmImageList;
    acOpenFile: TAction;
    acImportLefXMLOffsets: TAction;
    pa1: TmmPanel;
    cobFilename: TmmComboBox;
    la1: TmmLabel;
    bOpenFile: TmmSpeedButton;
    bImportLefOffset: TmmBitBtn;
    la2: TmmLineLabel;
    mOpenDialog: TmmOpenDialog;
    acUpdateParameterFromLEF: TAction;
    b1: TmmBitBtn;
    vstUpdatedElements: TmmVirtualStringTree;
    laTreeHeader: TmmLabel;
    acClose: TWindowClose;
    tb1: TmmToolBar;
    btn1: TToolButton;
    b2: TToolButton;
    acPrintReport: TAction;
    pa2: TmmPanel;
    acDiffXML: TAction;
    b3: TmmBitBtn;
    la3: TmmLabel;
    cobXMLDiffFile1: TmmComboBox;
    b4: TmmSpeedButton;
    la4: TmmLabel;
    cobXMLDiffFile2: TmmComboBox;
    b5: TmmSpeedButton;
    acOpenDiffFile1: TAction;
    acOpenDiffFile2: TAction;
    paDescriptionPanel: TmmPanel;
    pa3: TmmPanel;
    imInfoImage: TmmImage;
    memoDescriptionMemo: TmmMemo;
    paDescriptionPanel1: TmmPanel;
    pa4: TmmPanel;
    imInfoImage1: TmmImage;
    memoDescriptionMemo1: TmmMemo;
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure acImportLefXMLOffsetsExecute(Sender: TObject);
    procedure acOpenFileExecute(Sender: TObject);
    procedure acUpdateParameterFromLEFExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure vstUpdatedElementsGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vstUpdatedElementsChange(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure acCloseExecute(Sender: TObject);
    procedure acPrintReportExecute(Sender: TObject);
    procedure acOpenDiffFile1Execute(Sender: TObject);
    procedure acOpenDiffFile2Execute(Sender: TObject);
    procedure acDiffXMLExecute(Sender: TObject);
  private
    FLoepfeBody: TLoepfeBody;
    FTree: TTreeView;
    mSections: TStringList;
    mUpdatedElements: TMemIniFile;
    mValues: TStringList;
    procedure SelectElement(aElement: Pointer);
    procedure UpdateElementsList;
    { Private declarations }
  public
    procedure ImportFile;
    property LoepfeBody: TLoepfeBody read FLoepfeBody write FLoepfeBody;
    property Tree: TTreeView read FTree write FTree;
    { Public declarations }
  end;

implementation

uses
  SchematoolGlobal, MapExchange, printers, XMLGlobal, MSXML2_TLB, LoepfeGlobal,
  clipbrd;

{$R *.DFM}

(*---------------------------------------------------------
  Zeigt den Inputdialog an
----------------------------------------------------------*)
procedure TfrmImporter.ImportFile;
begin
//  ShowModal;
  Show;
end;// procedure TfrmImporter.ImportFile;

(*---------------------------------------------------------
  Settings laden
----------------------------------------------------------*)
procedure TfrmImporter.FormShow(Sender: TObject);
var
  i: Integer;
begin
  Settings.LoadForm(Self, Self.Name, false);
  for i := 0 to Settings.MRUCount[Settings.GetImporterElement] - 1 do
    cobFilename.Items.Add(Settings.LoadMRU(Settings.GetImporterElement, i));
  if cobFilename.Items.Count > 0 then
    cobFilename.ItemIndex := 0;

  for i := 0 to Settings.MRUCount[Settings.GetXMLDiffElement1] - 1 do
    cobXMLDiffFile1.Items.Add(Settings.LoadMRU(Settings.GetXMLDiffElement1, i));
  if cobXMLDiffFile1.Items.Count > 0 then
    cobXMLDiffFile1.ItemIndex := 0;

  for i := 0 to Settings.MRUCount[Settings.GetXMLDiffElement2] - 1 do
    cobXMLDiffFile2.Items.Add(Settings.LoadMRU(Settings.GetXMLDiffElement2, i));
  if cobXMLDiffFile2.Items.Count > 0 then
    cobXMLDiffFile2.ItemIndex := 0;
end;// procedure TfrmImporter.FormShow(Sender: TObject);

(*---------------------------------------------------------
  Settings speichern
----------------------------------------------------------*)
procedure TfrmImporter.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  Settings.SaveForm(Self, Self.Name);
end;// procedure TfrmImporter.FormCloseQuery(Sender: TObject; var CanClose: Boolean);

(*---------------------------------------------------------
  Gleicht das Modell mit den YM Namen ab
----------------------------------------------------------*)
procedure TfrmImporter.acImportLefXMLOffsetsExecute(Sender: TObject);
begin
  try
    Screen.Cursor := crHourglass;
    with TLefXMLReader.Create do try
      Filename := cobFilename.Text;
      UpdatedElements := mUpdatedElements;
      // Kein Knoten selektiert, da sonst das angezeigte Element nicht geupdatet werden kann
      if Assigned(fTree) then
        FTree.Selected := nil;
      Execute(LoepfeBody);
      Settings.SaveMRU(Filename, Settings.GetImporterElement, 20);
      // Ge�nderte Elemente anzeigen
      UpdateElementsList;
    finally
      Free;
    end;// with TLefXMLReader.Create do try
  finally
    Screen.Cursor := crDefault;
  end;
end;// procedure TfrmImporter.acImportLefXMLOffsetsExecute(Sender: TObject);

(*---------------------------------------------------------
  Datei ausw�hlen die importiert werden soll
----------------------------------------------------------*)
procedure TfrmImporter.acOpenFileExecute(Sender: TObject);
begin
  with mOpenDialog do begin
    if Execute then
      cobFilename.Text := FileName;
  end;// with mOpenDialog do begin
end;// procedure TfrmImporter.acOpenFileExecute(Sender: TObject);

(*---------------------------------------------------------
  Korrigiert die Parameter anhand des LEF XML
----------------------------------------------------------*)
procedure TfrmImporter.acUpdateParameterFromLEFExecute(Sender: TObject);
begin
  try
    Screen.Cursor := crHourglass;
    mUpdatedElements.Clear;
    vstUpdatedElements.RootNodeCount := 0;
    with TLefXMLUpdater.Create do try
      Filename := cobFilename.Text;
      UpdatedElements := mUpdatedElements;
      // Kein Knoten selektiert, da sonst das angezeigte Element nicht geupdatet werden kann
      if Assigned(fTree) then
        FTree.Selected := nil;
      Execute(LoepfeBody);
      Settings.SaveMRU(Filename, Settings.GetImporterElement, 20);
      // Ge�nderte Elemente anzeigen
      UpdateElementsList;
    finally
      Free;
    end;// with TLefXMLReader.Create do try
  finally
    Screen.Cursor := crDefault;
  end;
end;// procedure TfrmImporter.acUpdateParameterFromLEFExecute(Sender: TObject);

procedure TfrmImporter.FormCreate(Sender: TObject);
begin
  mUpdatedElements := TMemIniFile.Create('');
  mSections := TStringList.Create;
  mValues := TStringList.Create;
end;

procedure TfrmImporter.FormDestroy(Sender: TObject);
begin
  freeAndNil(mUpdatedElements);
  FreeAndNil(mSections);
  FreeAndNil(mValues);
end;

procedure TfrmImporter.SelectElement(aElement: Pointer);
var
  xFound: Boolean;
  xActNode: TTreeNode;
begin
  if Assigned(fTree) and Assigned(aElement) then begin
    // Vom ersten Knoten nach unten
    xActNode := fTree.Items.GetFirstNode;
    xFound := false;
    while (assigned(xActNode)) and (not(xFound)) do begin
      // aktuellen Knoten selektieren, wenn das gesuchte MMElement
      if xActNode.Data = aElement then begin
        fTree.Selected := xActNode;
        fTree.Selected.MakeVisible;
        fTree.TopItem  := xActNode;
        xFound := true;
      end;// if xActNode.Data = aElement then begin
      // W�hlt den n�chsten Knoten (unabh�ng von der aktuellen Ebene)
      xActNode := xActNode.GetNext;
    end;// while (assigned(xActNode)) and (not(xFound)) do begin
  end;// if Assigned(fTree) and Assigned(aElement) then begin
end;

procedure TfrmImporter.UpdateElementsList;
var
  xTreeCol: TVirtualTreeColumn;
  i: Integer;
begin
  // Alle Spalten l�schen
  vstUpdatedElements.Header.Columns.Clear;

  mUpdatedElements.ReadSections(mSections);
  if mSections.Count > 0 then begin
    // Hauptspalte erzeugen
    xTreeCol := vstUpdatedElements.Header.Columns.Add;
    xTreeCol.Text := 'YMName';
    mUpdatedElements.ReadSectionValues(mSections[0], mValues);
    // Alle Spalten bis auf die letzte erzeugen (Link zum Element)
    for i := 0 to mValues.Count - 2 do begin
      xTreeCol := vstUpdatedElements.Header.Columns.Add;
      xTreeCol.Text := mValues.Names[i];
    end;// for i := 0 to mValues.Count - 1 do begin
  end;// if mSections.Count > 0 then begin

  vstUpdatedElements.RootNodeCount := mSections.Count;
  vstUpdatedElements.Header.AutoFitColumns;
  laTreeHeader.Caption := Format('ver�nderte Elemente [%d]:', [mSections.Count]);
end;

procedure TfrmImporter.vstUpdatedElementsGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  xNodeIndex: integer;
begin
  CellText := '';
  xNodeIndex := Sender.AbsoluteIndex(Node);
  if (Column = 0) then
    CellText := mSections[xNodeIndex];
  if (Column > 0) and (Column <= mValues.Count) then
    CellText := mUpdatedElements.ReadString(mSections[xNodeIndex], mValues.Names[Column - 1], '');
end;

procedure TfrmImporter.vstUpdatedElementsChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  xNodeIndex: Integer;
begin
  if Assigned(Node) then begin
    xNodeIndex := vstUpdatedElements.AbsoluteIndex(Node);
    SelectElement(Pointer(mUpdatedElements.ReadInteger(mSections[xNodeIndex], mValues.Names[mValues.count - 1], 0)));
  end;// if Assigned(xNode) then begin
end;

procedure TfrmImporter.acCloseExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmImporter.acPrintReportExecute(Sender: TObject);
begin
  with TPrintDialog.Create(nil) do try
    if Execute then
      vstUpdatedElements.Print(Printer, True);
  finally
    Free;
  end;// with TPrintDialog.Create(nil) do try
end;

procedure TfrmImporter.acOpenDiffFile1Execute(Sender: TObject);
begin
  with mOpenDialog do begin
    if Execute then
      cobXMLDiffFile1.Text := FileName;
  end;// with mOpenDialog do begin
end;

procedure TfrmImporter.acOpenDiffFile2Execute(Sender: TObject);
begin
  with mOpenDialog do begin
    if Execute then
      cobXMLDiffFile2.Text := FileName;
  end;// with mOpenDialog do begin
end;

procedure TfrmImporter.acDiffXMLExecute(Sender: TObject);
var
  xDOM1: DOMDocumentMM;
  xDOM2: DOMDocumentMM;
  xDifferences: string;

  (*---------------------------------------------------------
    Rekursiv wird f�r jedes Element in File 1 das entsprechende Element im
    File 2 gesucht. Ist das Element in beiden Files vorhanden, wird es gel�scht.
    Was am Ende �brig bleibt, ist der Unterschied.
  ----------------------------------------------------------*)
  procedure ProcessXML(aNode1: IXMLDOMNode);
  var
    xSelectionPath: string;
    xNode2: IXMLDOMNode;
    i: Integer;
  begin
    if Assigned(aNode1) then  begin
      xSelectionPath := GetSelectionPath(aNode1);

      // Zuerst in die Tiefe
      if aNode1.hasChildNodes then begin
        for i := aNode1.childNodes.length - 1 downto 0 do
          ProcessXML(aNode1.childNodes[i]);
      end;// if aNode1.hasChildNodes then begin

      xNode2 := xDOM2.SelectSingleNode(xSelectionPath);
      if Assigned(xNode2) then begin
        if not(aNode1.hasChildNodes) then
          aNode1.parentNode.removeChild(aNode1);
        if not(xNode2.hasChildNodes) then
          xNode2.parentNode.removeChild(xNode2);
      end;// if Assigned(xNode2) then begin
    end;// if Assigned(aNode1) then  begin
 end;// procedure ProcessXML(const aElement: IXMLDOMNode);

begin
  // File 2 und File 1 in die MRU Liste eintragen
  Settings.SaveMRU(cobXMLDiffFile2.Text, Settings.GetXMLDiffElement1, 20);
  Settings.SaveMRU(cobXMLDiffFile1.Text, Settings.GetXMLDiffElement1, 20);

  // File 1 und File 2 in die MRU Liste eintragen
  Settings.SaveMRU(cobXMLDiffFile1.Text, Settings.GetXMLDiffElement2, 20);
  Settings.SaveMRU(cobXMLDiffFile2.Text, Settings.GetXMLDiffElement2, 20);

  if FileExists(cobXMLDiffFile1.Text) then begin
    if FileExists(cobXMLDiffFile2.Text) then begin
      xDOM1 := DOMDocumentMMCreate;
      xDOM2 := DOMDocumentMMCreate;

      // Beide Files laden
      if not(xDOM1.load(cobXMLDiffFile1.Text)) then
        raise Exception.Create('XML DOM Fehler (File 1):' + cCrlf+ cCrlf + FormatError(xDom1.ParseError));
      if not(xDOM2.load(cobXMLDiffFile2.Text)) then
        raise Exception.Create('XML DOM Fehler (File 2):' + cCrlf+ cCrlf + FormatError(xDom2.ParseError));

      ProcessXML(xDOM1.documentElement);

      xDifferences := 'Nicht mehr vorhandene Elemente:' + cCrlf;
      xDifferences := xDifferences + FormatXML(xDOM1.xml) + cCrlf;
      xDifferences := xDifferences + 'Neue Elemente:' + cCrlf;
      xDifferences := xDifferences + FormatXML(xDOM2.xml);
      Clipboard.AsText := xDifferences;
      ShowMessage('die Unterschiede sind ins Clippboard eingef�gt worden');
    end else begin
      ShowMessage(Format('File 2 existiert nicht (%s)', [cobXMLDiffFile2.Text]));
    end;// if FileExists(cobXMLDiffFile2.Text) then begin
  end else begin
    ShowMessage(Format('File 1 existiert nicht (%s)', [cobXMLDiffFile1.Text]));
  end;// if FileExists(cobXMLDiffFile1.Text) then begin
end;

end.
