unit SchematoolGlobal;

interface

uses Forms, MSXML2_TLB, XMLGlobal, sysutils, ConfigurationPersistence, typinfo,
  XMLMappingDef;


const
  cPrefix: array [0..3] of string = ('ZE', 'WSC', 'LZE', 'Tst');
//  // Nur Bindestrich und ein weiterer Bezeichner erlaubt ==> siehe 'GetNameFragments'
//  cPostfix: array [0..1] of string = ('-S', '-C');

  // Name der Sektion im Inifile wo die Nummern hochgez�hlt werden
  cMapfileNumbers = 'Numbers';

type
  TSchematoolSettings = class(TBaseXMLSettings)
  private
  public
    destructor Destroy; override;
    procedure DeleteAllMapForms;
    function GetImporterElement: IXMLDOMElement;
    function GetXMLDiffElement1: IXMLDOMElement;
    // Pfad zur Konfigurationsdatei
    function GetSettingsFilePath: string; override;
    function GetXMLDiffElement2: IXMLDOMElement;
    class function Instance: TSchematoolSettings;
    function LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement; override;
    function SaveForm(aForm: TForm; aName: string): IXMLDOMElement; override;
  end;

const
  cAtrMapName          = 'MapName';
  cRecentMapfileName   = 'RecentMapfile';
  cSelectedElementName = 'SelectedElement';
  cMapElementInfo      = 'Map Element Info';

  cImporterElement     = 'Importer';
  cXPImporterElement   = cXPDocumentBody + '/' + cImporterElement;
  cXMLDiff1Element     = 'XMLDiff1';
  cXMLDiff2Element     = 'XMLDiff2';
  cXPXMLDiff1Element   = cXPDocumentBody + '/' + cXMLDiff1Element;
  cXPXMLDiff2Element   = cXPDocumentBody + '/' + cXMLDiff2Element;

(*---------------------------------------------------------
  Wird f�r das vergleichen der mapfiles gebraucht
----------------------------------------------------------*)
type
  TItemRec = record
    DisplayName: string;
    PropName: string;
    PropType: TTypeKind;
  end;// TItemRec = record

  TMapfileSectionRec = record
    DisplayName: string;
    Abbreviation: string;
    Name: string;
  end;

function GetMapname(aPrefix:string; aMainNumber, aReleaseNumber: integer;
    aPostfix: string): string;

function Settings: TSchematoolSettings;

(*  TTypeKind = (tkUnknown, tkInteger, tkChar, tkEnumeration, tkFloat,
    tkString, tkSet, tkClass, tkMethod, tkWChar, tkLString, tkWString,
    tkVariant, tkArray, tkRecord, tkInterface, tkInt64, tkDynArray);*)
const
  cAvailableItems: Array [0..19] of TItemRec = (
      (DisplayName: 'Datentyp'; PropName: 'Datatype'; PropType: tkEnumeration),
      (DisplayName: 'Byte Offset'; PropName: 'Offset'; PropType: tkInteger),
      (DisplayName: 'Anzahl'; PropName: 'Count'; PropType: tkInteger),
      (DisplayName: 'Default'; PropName: 'Default'; PropType: tkString),
      (DisplayName: 'Richtung'; PropName: 'Direction'; PropType: tkInteger),
      (DisplayName: 'Min Incl.'; PropName: 'MinIncl'; PropType: tkString),
      (DisplayName: 'Min Excl.'; PropName: 'MinExcl'; PropType: tkString),
      (DisplayName: 'Max Incl.'; PropName: 'MaxIncl'; PropType: tkString),
      (DisplayName: 'Max Excl.'; PropName: 'MaxExcl'; PropType: tkString),
      (DisplayName: 'Bit Offset'; PropName: 'BitOffset'; PropType: tkString),
      (DisplayName: 'Bit Count'; PropName: 'BitCount'; PropType: tkString),
      (DisplayName: 'ConfigCode?'; PropName: 'IsConfigCode'; PropType: tkEnumeration),
      (DisplayName: 'ConfigCode'; PropName: 'ConfigCode'; PropType: tkString),
      (DisplayName: 'ConfigCode Bit'; PropName: 'CCBitNumber'; PropType: tkString),
      (DisplayName: 'Enumeration'; PropName: 'Enumeration'; PropType: tkString),
      (DisplayName: 'Konvertierung'; PropName: 'ConvertMode'; PropType: tkString),
      (DisplayName: 'Konvertierung Parameter'; PropName: 'ConvertPara'; PropType: tkString),
      (DisplayName: 'Events'; PropName: 'Events'; PropType: tkClass),
      (DisplayName: 'Version'; PropName: 'Version'; PropType: tkString),
      (DisplayName: 'Yarn Master Name'; PropName: 'YMElementName'; PropType: tkString)
  );// cAvailableItems = Array [0..1] of TItemRec = (

  // M�gliche Sektionen (Mapfiles) innerhalb eines Mapfiles
  cMapfileSections: Array [TMapSection] of TMapfileSectionRec = (
    (DisplayName: 'Settings'           ; Abbreviation: 'S'; Name: cEleYMSetting),
    (DisplayName: 'Gruppe Bauzustand'  ; Abbreviation: 'C'; Name: cEleMaConfig),
    (DisplayName: 'Maschine Bauzustand'; Abbreviation: 'M'; Name: cEleMapMachine)
  );// cMapfileSections: Array [TMapSection] of TMapfileSectionRec = (

implementation

uses
  windows, LoepfeGlobal, shlobj, comobj, filectrl, MapElementInfo;

function GetMapname(aPrefix:string; aMainNumber, aReleaseNumber: integer;
    aPostfix: string): string;
begin
  result := Format('%s-%.2d%.2d-%s', [aPrefix, aMainNumber, aReleaseNumber, aPostfix]);
end;

(*---------------------------------------------------------
  Liefert DIE Instanz der Settings
----------------------------------------------------------*)
function Settings: TSchematoolSettings;
begin
  Result := TSchematoolSettings.Instance;
end;// function Settings: TSchematoolSettings;

(*---------------------------------------------------------
  Speichert die Settings am Ende des Programms ab
----------------------------------------------------------*)
destructor TSchematoolSettings.Destroy;
begin
  SaveSettings;
  inherited;
end;// destructor TSchematoolSettings.Destroy;

(*---------------------------------------------------------
  L�scht alle Form Eleemnte
----------------------------------------------------------*)
procedure TSchematoolSettings.DeleteAllMapForms;
var
  i: Integer;
  xNodeList: IXMLDOMNodeList;
begin
  if assigned(DOM) then begin
    xNodeList := DOM.selectNodes(cXPFormElement + Format('[starts-with(@Name,''%s'')]', [cMapElementInfo]));
    for i := 0 to xNodeList.length - 1 do
      xNodeList.item[i].parentNode.removeChild(xNodeList.item[i]);
  end;// if assigned(DOM) then begin
end;// procedure TSchematoolSettings.DeleteAllForms;

(*---------------------------------------------------------
  Liefert das Element f�r den Importer
----------------------------------------------------------*)
function TSchematoolSettings.GetImporterElement: IXMLDOMElement;
begin
  // TODO -cMM: TSchematoolSettings.GetImporterElement default body inserted
  Result := GetElement(cXPImporterElement, True);
end;// function TSchematoolSettings.GetImporterElement: IXMLDOMElement;

(*---------------------------------------------------------
  Liefert das Element f�r den Importer
----------------------------------------------------------*)
function TSchematoolSettings.GetXMLDiffElement1: IXMLDOMElement;
begin
  // TODO -cMM: TSchematoolSettings.GetImporterElement default body inserted
  Result := GetElement(cXPXMLDiff1Element, True);
end;// function TSchematoolSettings.GetXMLDiffElement1: IXMLDOMElement;

(* -----------------------------------------------------------
  Gibt den Dateinamen f�r die Settings zur�ck
-------------------------------------------------------------- *)
function TSchematoolSettings.GetSettingsFilePath: string;
var
  item: PItemIDList;
  buffer: array[0..MAX_PATH] of Char;
begin
  OleCheck(SHGetSpecialFolderLocation(0, CSIDL_APPDATA, item));
  SHGetPathFromIDList(item, buffer);

  SetString(result, buffer, StrLen(buffer));

  result := IncludeTrailingBackslash(result)+ 'Loepfe\Schematool Settings.xml';
end;// function TSchematoolSettings.GetSettingsFilePath: string;

(*---------------------------------------------------------
  Liefert das Element f�r den Importer
----------------------------------------------------------*)
function TSchematoolSettings.GetXMLDiffElement2: IXMLDOMElement;
begin
  // TODO -cMM: TSchematoolSettings.GetImporterElement default body inserted
  Result := GetElement(cXPXMLDiff2Element, True);
end;// function TSchematoolSettings.GetXMLDiffElement2: IXMLDOMElement;

(* -----------------------------------------------------------
  Zugriff auf den Singleton
-------------------------------------------------------------- *)  
class function TSchematoolSettings.Instance: TSchematoolSettings;
begin
  Result := TSchematoolSettings(AccessInstance(1));
end;// class function TSchematoolSettings.Instance: TLKTermSettings;

(* -----------------------------------------------------------
  L�dt die Position und den Status eines Formulars aus dem DOM
-------------------------------------------------------------- *)
function TSchematoolSettings.LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement;
begin
  result := inherited LoadForm(aForm, aName);
  if (aForm is TfrmMapElementInfo) and (assigned(result)) then
    TfrmMapElementInfo(aForm).MapVersion := AttributeToStringDef(cAtrMapName, result, '');
end;// procedure TSchematoolSettings.LoadForm(aForm: TForm);

(* -----------------------------------------------------------
  Speichert die Position und den Status eines Formulars im DOM
  Wenn kein Name angegeben ist, dann wird die Form unter dem eigenen Namen gespeichert.
  Der R�ckgabewert ist das DOMElement in das die Parameter gespeichert wurden.
-------------------------------------------------------------- *)
function TSchematoolSettings.SaveForm(aForm: TForm; aName: string): IXMLDOMElement;
begin
  result := inherited SaveForm(aForm, aName);
  if (aForm is TfrmMapElementInfo) and (assigned(result)) then
    result.setAttribute(cAtrMapName, TfrmMapElementInfo(aForm).MapVersion);
end;// procedure TSchematoolSettings.SaveForm(aForm: TForm);

initialization
finalization
  // Singleton freigeben und Settings speichern
  TSchematoolSettings.ReleaseInstance;

end.
