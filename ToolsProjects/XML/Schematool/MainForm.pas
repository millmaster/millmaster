{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MainForm.pas
| Projectpart...: MillMaster Visual 
| Description...: 
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.07.2005  1.00  Wss | File created
|==========================================================================================}
unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, mmTreeView, StdCtrls, mmEdit, mmListBox, mmMemo, mmLabel,
  mmComboBox, mmGroupBox, ImgList, mmImageList, ToolWin, mmToolBar,
  Buttons, mmSpeedButton, CheckLst, mmCheckListBox, Menus, mmMainMenu,
  mmButton, MSXML2_TLB, ActnList, mmActionList, IvMlDlgs, mmOpenDialog,
  XMLMapping, VirtualTrees, mmPageControl, Spin, mmSpinEdit, mmCheckBox,
  mmStatusBar, mmRadioButton, mmBitBtn, ExtCtrls, mmRadioGroup, mmListView,
  mmPanel, mmBevel, FrameBaseFunction, mmTimer, mmSplitter, NewMapInputForm,
  mmRichEdit, mmImage, mmPopupMenu, XMLViewer, MapElementObserver,
  mmVCLStringList, CompareMapfile, FindConfigCode, Importer, mmLineLabel;

type
  TfrmMain = class(TForm)
    acChangeOffRec: TAction;
    acConvAdd: TAction;
    acConvDelete: TAction;
    acConvDown: TAction;
    acConvUp: TAction;
    acCopyXPath: TAction;
    acElementClear: TAction;
    acElementCopy: TAction;
    acElementPaste: TAction;
    acElementUndo: TAction;
    acEnableMillmasterMap: TAction;
    acEnumAdd: TAction;
    acEnumDelete: TAction;
    acEnumDown: TAction;
    acEnumIterate: TAction;
    acEnumPoolDelete: TAction;
    acEnumPoolNew: TAction;
    acEnumSetDefault: TAction;
    acEnumUp: TAction;
    acEventDelete: TAction;
    acEventDown: TAction;
    acEventNew: TAction;
    acEventUp: TAction;
    acExit: TAction;
    acExport: TAction;
    acExportMMConst: TAction;
    acFunctionShow: TAction;
    acImport: TAction;
    acMapCopy: TAction;
    acMapDelete: TAction;
    acMapNew: TAction;
    acNextItem: TAction;
    acOpen: TAction;
    acPreviousItem: TAction;
    acSave: TAction;
    acShowActiveElements: TAction;
    acShowInactiveElements: TAction;
    AktiveElementeeinblenden1: TMenuItem;
    Bearbeiten1: TMenuItem;
    cobEnumPool: TmmComboBox;
    Datei1: TMenuItem;
    InaktiveElementeeinblenden1: TMenuItem;
    lbFunction: TmmListBox;
    mActionList: TmmActionList;
    meEnumPoolValues: TmmMemo;
    miExit: TMenuItem;
    miExport: TMenuItem;
    miImport: TMenuItem;
    Millmaster1: TMenuItem;
    Millmastermapfreischalten1: TMenuItem;
    mImageList: TmmImageList;
    miOpen: TMenuItem;
    miSave: TMenuItem;
    mMainMenu: TmmMainMenu;
    mmButton1: TmmButton;
    mmLabel2: TmmLabel;
    mmLabel23: TmmLabel;
    mmLabel24: TmmLabel;
    mmSpeedButton15: TmmSpeedButton;
    mmSpeedButton16: TmmSpeedButton;
    mmToolBar1: TmmToolBar;
    mOpenDialog: TmmOpenDialog;
    N1: TMenuItem;
    N2: TMenuItem;
    NchstesElement1: TMenuItem;
    pcMain: TmmPageControl;
    pmTreeView: TmmPopupMenu;
    pnFunction: TmmPanel;
    sbBottom: TmmStatusBar;
    TabSheet3: TTabSheet;
    timTreeFilter: TmmTimer;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton7: TToolButton;
    tsEnumeration: TTabSheet;
    tsMapping: TTabSheet;
    VorherigesElement1: TMenuItem;
    XPathkopieren1: TMenuItem;
    acShowLastExportetMapfile: TAction;
    bShowLastExportetMapfile: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    acAdvancedFilter: TAction;
    acDeleteFilter: TAction;
    acShowOtherMap: TAction;
    ToolButton17: TToolButton;
    pmOpenFileMRU: TmmPopupMenu;
    acReloadCurrentSchema: TAction;
    ToolButton18: TToolButton;
    ToolButton19: TToolButton;
    acExportYM: TAction;
    acPasteActiveOnly: TAction;
    mNodesToCollapseNames: TmmVCLStringList;
    acCompareMapfiles: TAction;
    ToolButton22: TToolButton;
    acFindCC: TAction;
    mmPanel16: TmmPanel;
    mmPanel2: TmmPanel;
    tvSchema: TmmTreeView;
    mFilterPanel: TmmPanel;
    mAdvancedFilterPanel: TmmPanel;
    mPatternWithoutList: TmmCheckListBox;
    mPatternWithList: TmmCheckListBox;
    mmToolBar5: TmmToolBar;
    bOr: TToolButton;
    mBasicFilterPanel: TmmPanel;
    edTreeFilter: TmmEdit;
    mmToolBar3: TmmToolBar;
    ToolButton15: TToolButton;
    ToolButton16: TToolButton;
    bShowAssignedElements: TToolButton;
    bShowUnassignedElements: TToolButton;
    ToolButton14: TToolButton;
    mmPanel4: TmmPanel;
    mmLabel4: TmmLabel;
    mmToolBar2: TmmToolBar;
    bShowPrevElement: TToolButton;
    bNextElement: TToolButton;
    mmPanel1: TmmPanel;
    mmPanel13: TmmPanel;
    meDocu: TmmRichEdit;
    mmPanel15: TmmPanel;
    mmPanel3: TmmPanel;
    mmLabel1: TmmLabel;
    mBitFieldGroupBox: TmmGroupBox;
    mmLabel13: TmmLabel;
    mmLabel14: TmmLabel;
    edBitOffset: TmmEdit;
    edBitCount: TmmEdit;
    mmGroupBox3: TmmGroupBox;
    mmLabel10: TmmLabel;
    mmLabel11: TmmLabel;
    mmLabel19: TmmLabel;
    mmSpeedButton3: TmmSpeedButton;
    mmSpeedButton4: TmmSpeedButton;
    mmSpeedButton5: TmmSpeedButton;
    mmSpeedButton6: TmmSpeedButton;
    cobOperator: TmmComboBox;
    edOperatorValue: TmmEdit;
    lbConvertPara: TmmListBox;
    cobConvertMode: TmmComboBox;
    mmPanel11: TmmPanel;
    mmPanel12: TmmPanel;
    mmImage2: TmmImage;
    mmMemo3: TmmMemo;
    mmGroupBox4: TmmGroupBox;
    mmLabel5: TmmLabel;
    mmLabel9: TmmLabel;
    mmLabel8: TmmLabel;
    mmLabel17: TmmLabel;
    mmSpeedButton24: TmmSpeedButton;
    mmBevel2: TmmBevel;
    edOffset: TmmEdit;
    cobDataType: TmmComboBox;
    edCount: TmmEdit;
    edDefault: TmmEdit;
    cbConvertBoth: TmmRadioButton;
    cbConvertUp: TmmRadioButton;
    cbConvertDown: TmmRadioButton;
    mmPanel10: TmmPanel;
    mmMemo2: TmmMemo;
    mFacetGroupBox: TmmGroupBox;
    mmLabel6: TmmLabel;
    mmLabel7: TmmLabel;
    mmLabel15: TmmLabel;
    mmLabel16: TmmLabel;
    edMinIncl: TmmEdit;
    edMaxIncl: TmmEdit;
    edMinExcl: TmmEdit;
    edMaxExcl: TmmEdit;
    mmPanel8: TmmPanel;
    mmPanel9: TmmPanel;
    mmImage1: TmmImage;
    mmMemo1: TmmMemo;
    mmGroupBox6: TmmGroupBox;
    mmSpeedButton1: TmmSpeedButton;
    mmSpeedButton2: TmmSpeedButton;
    mmLabel20: TmmLabel;
    mmSpeedButton7: TmmSpeedButton;
    mmSpeedButton13: TmmSpeedButton;
    mmSpeedButton17: TmmSpeedButton;
    mmSpeedButton18: TmmSpeedButton;
    mmLabel25: TmmLabel;
    cobEnumeration: TmmComboBox;
    lbEnumerationSelected: TmmListBox;
    lbEnumerationAvailable: TmmListBox;
    seEnumValue: TmmSpinEdit;
    cbEnumOthers: TmmCheckBox;
    mmGroupBox1: TmmGroupBox;
    mmLabel26: TmmLabel;
    mmSpeedButton19: TmmSpeedButton;
    mmSpeedButton21: TmmSpeedButton;
    mmSpeedButton22: TmmSpeedButton;
    mmSpeedButton23: TmmSpeedButton;
    rbEventInt: TmmRadioButton;
    rbEventExt: TmmRadioButton;
    lbEvents: TmmListBox;
    edEventPath: TmmEdit;
    cbEventDelflag: TmmCheckBox;
    memoCurrentEvent: TmmRichEdit;
    mDescriptionPanel: TmmPanel;
    mmPanel14: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmMemo;
    mmToolBar4: TmmToolBar;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    ToolButton20: TToolButton;
    ToolButton21: TToolButton;
    mmGroupBox2: TmmGroupBox;
    mmLabel18: TmmLabel;
    mmSpeedButton8: TmmSpeedButton;
    cbCCBit: TmmComboBox;
    rbCCNone: TmmRadioButton;
    rbCCA: TmmRadioButton;
    rbCCB: TmmRadioButton;
    rbCCC: TmmRadioButton;
    rbCCD: TmmRadioButton;
    mmSplitter2: TmmSplitter;
    acFocusToFilter: TAction;
    btn1: TToolButton;
    edYMElementName: TRichEdit;
    paFlags: TmmPanel;
    la1: TmmLineLabel;
    cbProofed: TmmCheckBox;
    cbObserve: TmmCheckBox;
    acOpenMapExplorer: TAction;
    ffneExplorerMapfiles1: TMenuItem;
    N3: TMenuItem;
    mmPanel5: TmmPanel;
    mmSpeedButton11: TmmSpeedButton;
    mmSpeedButton12: TmmSpeedButton;
    mmSpeedButton14: TmmSpeedButton;
    mmLabel21: TmmLabel;
    edStructSize: TmmEdit;
    mmPanel6: TmmPanel;
    mmSplitter1: TmmSplitter;
    mmPanel7: TmmPanel;
    mmLabel12: TmmLabel;
    lbMapCategories: TmmListBox;
    mmPanel17: TmmPanel;
    mmLabel22: TmmLabel;
    lbMapID: TmmListBox;
    meDescription: TmmRichEdit;
    rbMapIDFilter: TmmRadioGroup;
    mmPanel18: TmmPanel;
    mmLabel3: TmmLabel;
    meHistory: TmmRichEdit;
    procedure acChangeOffRecExecute(Sender: TObject);
    procedure acConvAddExecute(Sender: TObject);
    procedure acConvDeleteExecute(Sender: TObject);
    procedure acConvUpDownExecute(Sender: TObject);
    procedure acCopyXPathExecute(Sender: TObject);
    procedure acElementClearExecute(Sender: TObject);
    procedure acElementCopyExecute(Sender: TObject);
    procedure acElementPasteExecute(Sender: TObject);
    procedure acElementUndoExecute(Sender: TObject);
    procedure acEnableMillmasterMapExecute(Sender: TObject);
    procedure acEnumAddExecute(Sender: TObject);
    procedure acEnumDeleteClick(Sender: TObject);
    procedure acEnumIterateExecute(Sender: TObject);
    procedure acEnumPoolDeleteExecute(Sender: TObject);
    procedure acEnumPoolNewExecute(Sender: TObject);
    procedure acEnumSetDefaultExecute(Sender: TObject);
    procedure acEnumUpDownExecute(Sender: TObject);
    procedure acEventDeleteExecute(Sender: TObject);
    procedure acEventNewExecute(Sender: TObject);
    procedure acEventUpDownExecute(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acExportExecute(Sender: TObject);
    procedure acExportMMConstExecute(Sender: TObject);
    procedure acFunctionShowExecute(Sender: TObject);
    procedure acImportExecute(Sender: TObject);
    procedure acMapCopyExecute(Sender: TObject);
    procedure acMapDeleteExecute(Sender: TObject);
    procedure acMapNewExecute(Sender: TObject);
    procedure acNextItemExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acPreviousItemExecute(Sender: TObject);
    procedure acSaveExecute(Sender: TObject);
    procedure bShowAssignedElementsxClick(Sender: TObject);
    procedure cobConvertModeChange(Sender: TObject);
    procedure cobDataTypeChange(Sender: TObject);
    procedure cobEnumerationChange(Sender: TObject);
    procedure cobEnumPoolChange(Sender: TObject);
    procedure ControlEntryChange(Sender: TObject);
    procedure edMaxExclChange(Sender: TObject);
    procedure edMaxInclChange(Sender: TObject);
    procedure edMinExclChange(Sender: TObject);
    procedure edMinInclChange(Sender: TObject);
    procedure edTreeFilterKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure lbEnumerationAvailableDblClick(Sender: TObject);
    procedure lbEnumerationSelectedClick(Sender: TObject);
    procedure lbEventsClick(Sender: TObject);
    procedure lbMapCategoriesClick(Sender: TObject);
    procedure lbMapIDClick(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure meEnumPoolValuesKeyPress(Sender: TObject; var Key: Char);
    procedure seEnumValueChange(Sender: TObject);
    procedure timTreeFilterTimer(Sender: TObject);
    procedure tvSchemaChange(Sender: TObject; Node: TTreeNode);
    procedure tvSchemaCustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState; var
      DefaultDraw: Boolean);
    procedure tvSchemaGetImageIndex(Sender: TObject; Node: TTreeNode);
    procedure acShowLastExportetMapfileExecute(Sender: TObject);
    procedure acAdvancedFilterExecute(Sender: TObject);
    procedure mPatternListClick(Sender: TObject);
    procedure acDeleteFilterExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cbEnumOthersClick(Sender: TObject);
    procedure edMinInclExit(Sender: TObject);
    procedure acShowOtherMapExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure pmOpenFileMRUPopup(Sender: TObject);
    procedure acReloadCurrentSchemaExecute(Sender: TObject);
    procedure acExportYMExecute(Sender: TObject);
    procedure acCompareMapfilesExecute(Sender: TObject);
    procedure acFindCCExecute(Sender: TObject);
    procedure meEnumPoolValuesExit(Sender: TObject);
    procedure edOffsetEnter(Sender: TObject);
    procedure acFocusToFilterExecute(Sender: TObject);
    procedure acOpenMapExplorerExecute(Sender: TObject);
  private
    mMapFileViewer: TfrmXMLViewer;
    mCanSave: Boolean;
    mCanUndo: Boolean;
    mCompareMapfile: TfrmCompareMapfile;
    mCopyElement: TMMElement;
    mCurrentControl: TControl;
    mCurrentElement: TMMElement;
    mDoc: DOMDocument40;
    mFindCC: TfrmFindConfigCode;
    mFunctionFrame: TfraBaseFunction;
    mImporterForm: TfrmImporter;
    mLastExportedMapfile: string;
    mLastExportedMapfileName: string;
    mLoepfeBody: TLoepfeBody;
    mMapElementChange: TMapElementChange;
    mSchemaFileName: String;
    mSchemaModified: Boolean;
    procedure AssignEnums;
    procedure BuildTree(aFullExpand: boolean);
    function CheckControlState: Boolean;
    function CreateNewMap(aMsg: String; var aCategory: string; var aNewDescription: string): Boolean;
    procedure CreateXMLViewer;
    function GetMapDataDir: string;
    procedure LoadDocument(aFileName: string; aReadOnly: boolean);
    function LockFile: Boolean;
    procedure OpenSchema(aReadOnly: boolean);
    procedure ReadValuesFromLoepfeBody;
    procedure ReadValuesFromMMElement;
    procedure UnlockFile;
    procedure UpdateMapList;
    procedure WriteValuesToElement;
    procedure WriteValuesToLoepfeBody;
    function GetNodePath(aNode: TTreeNode): String;
    procedure SelectElement(aElementPath: string);
  public
    procedure CheckDefault;
    function IsMillmasterMap: Boolean;
    procedure LoadDocumentHandler(Sender: TObject);
    procedure LoadSettings;
    procedure SaveSettings;
  end;

var
  frmMain: TfrmMain;
  x: TShortCut;

implementation
{$R *.DFM}
uses
  mmCS, mmStringList, LoepfeGlobal, XMLMappingDef, FrameCollision, MapExchange, FrameMemUsage,
  filectrl, clipbrd, ChooseMapfile, ExportMapFile,
  AdoDBAccess, BZIP2, math, MapElementInfo, SchematoolGlobal, XMLGlobal, ConfigurationPersistence,
  BaseGlobal, shellapi;

const
  cNormalFileExtension     = '.map';
  cCompressedFileExtension = '.MMZ';
  cFileFilterSchema        = 'XML Schem (*.xsd)|*.xsd';
  cCaptionText             = 'LOEPFE Schematool';

  cAllowedEnumChars        = [#0..#31, '0'..'9', '_', '-', 'a'..'z','A'..'Z'];

  cStatusElementCountPanel = 0;
  cStatusXPathPanel        = 1;

type
  TBaseElementClass = class of TBaseElement;

(*---------------------------------------------------------
  Incrementiert den Offset aller Elemente deren Offset h�her ist als
  der des aktuellen Elementes um einen festen Betrag der eingegeben werden kann.
  Es sind auch negative Zahlen erlaubt. In diesem Fall wird der Offset f�r alle
  nachfolgenden Elemente dekrementiert.
----------------------------------------------------------*)
procedure TfrmMain.acChangeOffRecExecute(Sender: TObject);
var
  i: integer;
  xIncrementStr: string;
  xMsg: string;
  xIncrement: integer;

  (*---------------------------------------------------------
    Incrementiert den Offset aller Elemente (auch untergeordnete Elemente)
  ----------------------------------------------------------*)
  procedure DeepIncrementOffset(aElement: TMMElement; aIncrement: integer; aAboveOffset: integer);
  var
    i: integer;
  begin
    if assigned(aElement) then begin
      if (aElement.Active) and (aElement.Offset > aAboveOffset) then
        aElement.Offset := aElement.Offset + aIncrement;
      for i := 0 to aElement.MMElements.Count - 1 do
        DeepIncrementOffset(aElement.MMElements[i], aIncrement, aAboveOffset);
    end;//
  end;// procedure DeepIncrementOffset(aElement: TMMElement; aIncrement: integer; aAboveOffset: integer);

begin
  if assigned(mCurrentElement) then begin
    xMsg := 'Alle nachfolgenden Elemente verschieben um:';
    if InputQuery('Offset �nderung', xMsg, xIncrementStr) then begin
      xIncrement := StrToIntDef(xIncrementStr, 0);
      if xIncrement <> 0 then begin
        xMsg := Format('Soll der Offset aller nachfolgenden Elemente um %d ge�ndert werden?'+#13+#10+#13+#10+
                       'ACHTUNG'+#13+#10+'�nderungen k�nnen nicht r�ckg�ngig gemacht werden.', [xIncrement]);
        if (MessageBox(0, PChar(xMsg), '�nderung des Offsets', MB_ICONWARNING or MB_OKCANCEL) = mrOK) then begin
          for i := 0 to mLoepfeBody.MMElements.Count - 1 do
            DeepIncrementOffset(mLoepfeBody.MMElements[i], xIncrement, mCurrentElement.Offset);
        end;// if (MessageBox(...
      end;// if xIncrement <> 0 then begin
    end;// if InputQuery('Offset �nderung', 'Um wieviel soll der Offset aller nachfolgenden Elemente ge�ndert werden?', xIncrementStr) then begin
  end;// if assigned(mCurrentElement) then begin
end;

//:---------------------------------------------------------------------------
//:--- Class: TfrmMain
//:---------------------------------------------------------------------------
procedure TfrmMain.acConvAddExecute(Sender: TObject);
begin
  mCanUndo := True;
  with lbConvertPara do begin
    case cobConvertMode.ItemIndex of
      cConvertFormula: Items.Add(Format('%s=%s', [cobOperator.Text, edOperatorValue.Text]));
    else
      // cConvertNone, cConvertNot nichts machen
    end;
  end;
end; // TfrmMain.acConvAddExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acConvDeleteExecute(Sender: TObject);
var
  xOldIndex: integer;
begin
  mCanUndo := True;
  with lbConvertPara do begin
    if ItemIndex <> -1 then begin
      xOldIndex := ItemIndex;
      Items.Delete(ItemIndex);
      // Eintrag f�r die Formel l�schen, wenn keine Formel mehr definiert ist
      if Items.Count = 0 then
        cobConvertMode.ItemIndex := cConvertNone;
      if xOldIndex < Items.Count then
        ItemIndex := xOldIndex
      else
        ItemIndex := Items.Count - 1;
    end;
  end;
end; // TfrmMain.acConvDeleteExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acConvUpDownExecute(Sender: TObject);
var
  xNewIndex: Integer;
begin
  mCanUndo := True;
  with lbConvertPara do begin
    xNewIndex := ItemIndex + TControl(Sender).Tag;
    Items.Move(ItemIndex, ItemIndex + TControl(Sender).Tag);
    ItemIndex := xNewIndex;
  end;
end; // TfrmMain.acConvUpDownExecute

procedure TfrmMain.acCopyXPathExecute(Sender: TObject);
begin
  Clipboard.AsText := sbBottom.Panels[cStatusXPathPanel].Text;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acElementClearExecute(Sender: TObject);
var
  xDeepClearPrompt: string;
begin
  if MessageDlg('Map Informationen wirklich l�schen?', mtConfirmation, mbOKCancel, 0) = mrOK then begin
    if mCurrentElement.IsLeafElement then begin
      mCurrentElement.Clear;
    end else begin
      xDeepClearPrompt := 'ACHTUNG!'+#13+#10+'Die Map Informationen aller untergeordneten Elemente werden gel�scht!'+#13+#10+'Die Mapinformationen k�nnen nachher nicht wieder hergestellt werden.';
      // F�r alle Elemente sicherheitshalber nocheinmal nachfragen
      if (MessageDlg(xDeepClearPrompt, mtWarning, [mbOK, mbCancel], 0) = mrOk) then
        mCurrentElement.DeepClear;
    end;
    ReadValuesFromMMElement;
  end;
  // Zeichnet das Tree neu und aktualisiert die Icons der Elemente
  tvSchema.Invalidate;
end; // TfrmMain.acElementClearExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acElementCopyExecute(Sender: TObject);
begin
  if not Assigned(mCopyElement) then
    mCopyElement := TMMElement.Create;

  mCopyElement.Assign(mCurrentElement);
end; // TfrmMain.acElementCopyExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acElementPasteExecute(Sender: TObject);
var
  xCurrentOffset: Integer;
  xFromMap: string;
  xInfo: string;
begin
  if (assigned(mCurrentElement)) and (mCurrentElement.IsLeafElement) then begin
    if  assigned(mCopyElement) then begin
      xCurrentOffset := mCurrentElement.Offset;
      mCurrentElement.Assign(mCopyElement);
      mCurrentElement.Offset := xCurrentOffset;
      ReadValuesFromMMElement;
      edOffset.SetFocus;
      edOffset.SelectAll;
    end;// if  assigned(mCopyElement) then begin
  end else begin
    (* Wenn vorher kein Element kopiert wurde oder das aktuelle Element kein Blatt ist
       steht die M�glichkeit zur Verf�gung einen ganzen Ast aus einer andern Map zu kopieren *)
    xFromMap := '';
    xInfo := '! ! ! A C H T U N G ! ! !'#13#10'Alle untergeordneten Elemente werden �berschrieben';
    if TfrmChooseMapfile.AskForMapfile('Mapfile von dem kopiert werden soll', xFromMap, mLoepfeBody, xInfo) then begin
      if xFromMap > '' then
        mCurrentElement.CopyFromMap(xFromMap, true, Sender = acPasteActiveOnly);
    end;
  end;// if (mCurrentElement.IsLeafElement) and (assigned(mCurrentElement)) then begin
  // Zeichnet das Tree neu und aktualisiert die Icons der Elemente
  tvSchema.Invalidate;
end; // TfrmMain.acElementPasteExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acElementUndoExecute(Sender: TObject);
begin
  // Werte auslesen und in den Komponenten darstellen
  ReadValuesFromMMElement;
end; // TfrmMain.acElementUndoExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnableMillmasterMapExecute(Sender: TObject);
begin
  acEnableMillmasterMap.Checked := not(acEnableMillmasterMap.Checked);
  UpdateMapList;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumAddExecute(Sender: TObject);
var
  i: Integer;
begin
  with lbEnumerationAvailable do begin
    for i:=0 to Items.Count-1 do begin
      if Selected[i] and (lbEnumerationSelected.Items.Values[Items.Strings[i]] = '') then begin
        mCanUndo := True;
        lbEnumerationSelected.Items.Values[Items[i]] := IntToStr(lbEnumerationSelected.Items.Count);
      end;
    end;
  end;
end; // TfrmMain.acEnumAddExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumDeleteClick(Sender: TObject);
var
  xOldIndex: integer;
begin
  mCanUndo := True;
  with lbEnumerationSelected do begin
    if ItemIndex <> -1 then begin
      xOldIndex := ItemIndex;
      Items.Delete(ItemIndex);
      if xOldIndex < Items.Count then
        ItemIndex := xOldIndex
      else
        ItemIndex := Items.Count - 1;
    end;
  end;
end; // TfrmMain.acEnumDeleteClick

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumIterateExecute(Sender: TObject);
var
  i: Integer;
begin
  mCanUndo := True;
  with lbEnumerationSelected do begin
    for i:=0 to Items.Count-1 do
      Items.Values[Items.Names[i]] := IntToStr(i);
  end;
end; // TfrmMain.acEnumIterateExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumPoolDeleteExecute(Sender: TObject);
var
  xIndex: Integer;
begin
  if MessageDlg('Diesen Aufz�hlungstyp wirklich l�schen?', mtConfirmation, mbOKCancel, 0) = mrOK then begin
    // suchen nach diesem Eintrag in LoepfeBody und dann l�schen
    xIndex := mLoepfeBody.Enumerations.IndexOf(cobEnumPool.Text);
    if xIndex <> -1 then
      mLoepfeBody.Enumerations.Delete(xIndex);
    // aus ComboBox immer l�schen
    cobEnumPool.Items.Delete(cobEnumPool.ItemIndex);
    meEnumPoolValues.Lines.CommaText := '';
    AssignEnums;
  end;
end; // TfrmMain.acEnumPoolDeleteExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumPoolNewExecute(Sender: TObject);
var
  xStr: String;
  xIndex: Integer;
begin
  // Erst einmal die aktuelle Enumeration sichern
  if cobEnumPool.ItemIndex >= 0 then
    mLoepfeBody.Enumerations.Values[cobEnumPool.Text] := meEnumPoolValues.Lines.CommaText;

  if InputQuery('Aufz�hlung', 'Neue Aufz�hlung:', xStr) then begin
    xIndex := cobEnumPool.Items.IndexOf(xStr);
    if xIndex = -1 then begin
      cobEnumPool.ItemIndex := cobEnumPool.Items.Add(xStr);
      AssignEnums;
    end else
      cobEnumPool.ItemIndex := xIndex;
    cobEnumPoolChange(Nil);
  end;
end; // TfrmMain.acEnumPoolNewExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumSetDefaultExecute(Sender: TObject);
begin
  with lbEnumerationSelected do begin
    if ItemIndex <> -1 then
      edDefault.Text := Items.Names[ItemIndex];
  end;
end; // TfrmMain.acEnumSetDefaultExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEnumUpDownExecute(Sender: TObject);
var
  xNewIndex: Integer;
begin
  mCanUndo := True;
  with lbEnumerationSelected do begin
    xNewIndex := ItemIndex + TControl(Sender).Tag;
    Items.Move(ItemIndex, xNewIndex);
    ItemIndex := xNewIndex;
  end;
end; // TfrmMain.acEnumUpDownExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEventDeleteExecute(Sender: TObject);
var
  xOldIndex: integer;
begin
  mCanUndo := True;
  with lbEvents do begin
    if ItemIndex <> -1 then begin
      xOldIndex := ItemIndex;
      Items.Delete(ItemIndex);
      if xOldIndex < Items.Count then
        ItemIndex := xOldIndex
      else
        ItemIndex := Items.Count - 1;
    end;
  end;
end; // TfrmMain.acEventDeleteExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEventNewExecute(Sender: TObject);
var
  xEntryString: string;
begin
  mCanUndo := True;

  xEntryString := '';
  // XPath eintragen
  if edEventPath.Text > '' then
    xEntryString := Format('%s=%s', [cAtrEventElement, edEventPath.Text]);

  // Interner oder Externen Event
  if rbEventInt.Checked then
    xEntryString := Format('%s,%s=%s', [xEntryString, cAtrEventMode, cEventNames[cEventInt]])
  else
    xEntryString := Format('%s,%s=%s', [xEntryString, cAtrEventMode, cEventNames[cEventExt]]);

  // Deleteflag
  if cbEventDelflag.Checked then
    xEntryString := Format('%s,%s=%s', [xEntryString, cAtrEventDeleteFlag,cTrueXML]);

  // F�hrendes Komma l�schen (Ist der Fall, wenn kein XPath eingegeben wurde)
  if (xEntryString > '') and (xEntryString[1] = ',') then
    system.delete(xEntryString, 1, 1);

  // Event zur Liste hinzuf�gen
  if xEntryString > '' then
    lbEvents.Items.Add(xEntryString);
end; // TfrmMain.acEventNewExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acEventUpDownExecute(Sender: TObject);
var
  xNewIndex: Integer;
begin
  mCanUndo := True;
  with lbEvents do begin
    xNewIndex := ItemIndex + TControl(Sender).Tag;
    Items.Move(ItemIndex, ItemIndex + TControl(Sender).Tag);
    ItemIndex := xNewIndex;
  end;
end; // TfrmMain.acEventUpDownExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acExitExecute(Sender: TObject);
begin
  Close;
end; // TfrmMain.acExitExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acExportExecute(Sender: TObject);
var
  xNetTyp: TNetTyp;
  xMapName: string;
  xSaveToDB: boolean;
  xFileCompressed: Boolean;
  xFileName: String;
  xStrStream: TStringStream;
  xCompressionStream: TStream;
  xFileStream: TStream;
  xMapfileText: string;
begin
  xFileName := '';
  xMapName  := '';
  xSaveToDB := false;
  xMapfileText := '';

  // Header Informationen sichern
  WriteValuesToLoepfeBody;
  // Aktuelles Element sichern
  WriteValuesToElement;
  with TMapFileExporter.Create do
  try
    LoepfeBody := mLoepfeBody;
    MapFileList := TfrmExportMapFile.GetMapfileList(mLoepfeBody, xMapName, xSaveToDB, xFileCompressed, emMM);

    if xSaveToDB then
      FileName  := ''
    else if xFileCompressed then begin
      FileName  := '';
      xFileName := GetMapDataDir + xMapName + cCompressedFileExtension
    end else
      FileName  := GetMapDataDir + xMapName + cNormalFileExtension;


    if MapfileList > '' then begin
      // 8.11.04 wss
//      if WriteYMMap then begin
      if WriteMMMap then begin
        xMapfileText := DOM.xml;
        xMapfileText := StringReplace(xMapfileText, #13#10, '', []);
        
        // DOM und Name des Mapfiles merken zum sp�teren anzeigen
        mLastExportedMapfile := xMapfileText;
        mLastExportedMapfileName := xMapName;

        if xSaveToDB then begin
          with TAdoDBAccess.Create(1) do try
            Init;
            with Query[0] do begin
              SQL.Text := 'SELECT c_mapfile_id FROM t_xml_mapfile WHERE c_mapfile_name = ''' + xMapName + '''';
              open;
              if not(EOF) then begin
                if (MessageDlg('!!! A C H T U N G !!!'+#13+#10+'Das vorhandene Mapfile wird �berschrieben', mtWarning, [mbOK, mbCancel], 0) = mrOk) then begin
                  SQL.Text := Format('UPDATE t_xml_mapfile SET c_mapfile = ''%s'' WHERE c_mapfile_id = %d', [xMapfileText, FieldByName('c_mapfile_id'). AsInteger]);
                  if ExecSQL > 0 then
                    ShowMessage('Datensatz ge�ndert');
                end;
              end else begin
                // Netztyp bestimmen
                xNetTyp := ntNone;
                if AnsiSameText(copy(xMapName, 1, 2), 'ZE') then
                  xNetTyp := ntTxn
                else if AnsiSameText(copy(xMapName, 1, 3), 'WSC') then
                  xNetTyp := ntWsc
                else if AnsiSameText(copy(xMapName, 1, 3), 'LZE') then
                  xNetTyp := ntLx;

                SQL.Text := 'INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_net_id, c_mapfile) VALUES(:c_mapfile_id, :c_mapfile_name, :c_net_id, :c_mapfile)';
                ParamByName('c_mapfile_name').AsString := xMapName;
                ParamByName('c_net_id').AsInteger := ord(xNetTyp);
                ParamByName('c_mapfile').AsString := xMapfileText;
                if InsertSQL('t_xml_mapfile', 'c_mapfile_id', MAXINT, 1) > 0 then
                  ShowMessage('Mapfile eingef�gt');
              end;
            end;// with Query[0] do begin
          finally
            Free;
          end;// with TAdoDBAccess.Create(1) do try

        end
        else if xFileCompressed then begin
          xFileStream := TFileStream.Create(xFileName, fmCreate);
          try
            xCompressionStream  := TBZCompressionStream.Create(bs9, xFileStream);
            xStrStream          := TStringStream.Create(xMapfileText);
            xStrStream.Position := 0;
            try
              xCompressionStream.CopyFrom(xStrStream, 0);
            finally
              xCompressionStream.Free;
              xStrStream.Free;
            end;
          finally
            xFileStream.Free;
          end;
        end;
      end else begin
        ShowMessage('Misslungen');
      end;// if WriteMMMap then begin
    end;// if MapfileList > '' then begin

    if assigned(mMapFileViewer) and (mMapFileViewer.Showing) then 
      acShowLastExportetMapfile.Execute;

  finally
    Free;
  end;// with TMapFileExporter.Create do begin
end; // TfrmMain.acExportExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acExportMMConstExecute(Sender: TObject);
begin
  if IsMillmasterMap then begin
    with TMapFileExporter.Create do
    try
      FileName := GetMapDataDir + 'XMLDef.pas';
      LoepfeBody := mLoepfeBody;
      if not(Execute(TDelphiConstWriter, '')) then
        ShowMessage('Misslungen');
    finally
      Free;
    end;// with TMapFileExporter.Create do begin
  end else begin
    MessageDlg('Diese Funktion ist nur mit dem Millmaster Map zul�ssig', mtInformation, [mbOK], 0);
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acFunctionShowExecute(Sender: TObject);
begin
  // vor dem Speichern �nderungen �bernehmen
  WriteValuesToLoepfeBody;
  WriteValuesToElement;
  if Assigned(mFunctionFrame) then
    FreeAndNil(mFunctionFrame);

  case lbFunction.ItemIndex of
    0: mFunctionFrame := TfraCollision.Create(Self);
    1: mFunctionFrame := TfraMemUsage.Create(Self);
  else
    ShowMessage('Diese Funktion ist noch nicht implementiert.');
  end;

  if Assigned(mFunctionFrame) then begin
    mFunctionFrame.Parent     := pnFunction;
    mFunctionFrame.Align      := alLeft;
    mFunctionFrame.TreeView   := tvSchema;
    try
      Screen.Cursor := crHourglass;
      mFunctionFrame.Execute;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acImportExecute(Sender: TObject);
begin
  if not(Assigned(mImporterForm)) then
    mImporterForm := TfrmImporter.Create(Self);

  with mImporterForm do begin
    LoepfeBody := mLoepfeBody;
    Tree := tvSchema;
    ImportFile;
  end;// with mImporterForm do begin
end; // TfrmMain.acImportExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acMapCopyExecute(Sender: TObject);
var
  xMapCategory: string;
  xNewDescription: string;
  xMap: string;
begin
  // �nderungen �bernehmen
  WriteValuesToElement;
  if CreateNewMap('Aktuelle Map kopieren', xMapCategory, xNewDescription) then begin
    xMap := lbMapID.Items[lbMapID.ItemIndex];
    mLoepfeBody.CopyToMap(xMap, True, xMapCategory);
    UpdateMapList;

    if xMapCategory > '' then
      lbMapCategories.ItemIndex := lbMapCategories.Items.IndexOf(xMapCategory)
    else
      lbMapCategories.ItemIndex := 0;

    lbMapCategoriesClick(lbMapCategories);
    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xMap);
    // K�nstlich ausl�sen, damit die neue Map ausgelesen wird
    lbMapIDClick(Sender);
    meDescription.Text := xNewDescription;
  end;// if CreateNewMap('Aktuelle Map kopieren', xMapCategory, xNewDescription) then begin
end; // TfrmMain.acMapCopyExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acMapDeleteExecute(Sender: TObject);
begin
  if MessageDlg('Map wirklich l�schen?', mtConfirmation, mbOKCancel, 0) = mrOK then begin
    tvSchema.FullCollapse;
    mLoepfeBody.DeleteMap(True);
    lbMapID.Items.Delete(lbMapID.ItemIndex);
    ReadValuesFromLoepfeBody;
    UpdateMapList;
  end;
end; // TfrmMain.acMapDeleteExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acMapNewExecute(Sender: TObject);
var
  xMapCategory: string;
  xNewDescription: string;
  xMap: string;
begin
  if CreateNewMap('Neue Map erstellen', xMapCategory, xNewDescription) then begin
    xMap := lbMapID.Items[lbMapID.ItemIndex];
    mLoepfeBody.NewMap(xMap, xMapCategory);
    UpdateMapList;

    if xMapCategory > '' then
      lbMapCategories.ItemIndex := lbMapCategories.Items.IndexOf(xMapCategory)
    else
      lbMapCategories.ItemIndex := 0;

    lbMapCategoriesClick(lbMapCategories);
    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xMap);
    // K�nstlich ausl�sen, damit die neue Map ausgelesen wird
    lbMapIDClick(Sender);
    meDescription.Text := xNewDescription;
  end;// if CreateNewMap('Neue Map erstellen', xMapCategory, xNewDescription) then begin
end; // TfrmMain.acMapNewExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.acPreviousItemExecute(Sender: TObject);
var
  xTreeNode: TTreeNode;
begin
  if assigned(tvSchema.Selected) then begin
    // Vorheriges Blatt suchen
    xTreeNode := tvSchema.Selected;
    repeat
      xTreeNode := xTreeNode.GetPrev;
    until (not(assigned(xTreeNode))) OR (not(xTreeNode.HasChildren));

    // gefundenen Knoten selektieren (kann auch nil sein)
    tvSchema.Selected := xTreeNode;
    if ActiveControl is TEdit then
      TEdit(ActiveControl).SelectAll;
  end;// if assigned(tvSchema.Selected) then begin
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acNextItemExecute(Sender: TObject);
var
  xTreeNode: TTreeNode;
begin
  if not(assigned(tvSchema.Selected)) then
    tvSchema.Selected := tvSchema.Items.GetFirstNode;

  if assigned(tvSchema.Selected) then begin
    // N�chstes Blatt suchen
    xTreeNode := tvSchema.Selected;
    repeat
      xTreeNode := xTreeNode.GetNext;
    until (not(assigned(xTreeNode))) OR (not(xTreeNode.HasChildren));

    // gefundenen Knoten selektieren (kann auch nil sein)
    tvSchema.Selected := xTreeNode;
    if (ActiveControl = edTreeFilter) or (ActiveControl = tvSchema) then
      cobDataType.SetFocus
    else if ActiveControl is TEdit then
      TEdit(ActiveControl).SelectAll;
  end;// if assigned(tvSchema.Selected) then begin
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.acOpenExecute(Sender: TObject);
begin
  if mSchemaModified then begin
    case MessageDlg('Das Schema wurde ver�ndert.'+#13+#10+'Soll gespeichert werden?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
      // Speichern
      mrYes: acSave.Execute;
      // Keine weitere Aktion (es wird gespeichert)
      mrNo: ;
      // Nicht speichern
      mrCancel: exit;
    end;
  end;// if mSchemaModified then begin


  mOpenDialog.DefaultExt := 'xsd';
  mOpenDialog.Filter     := cFileFilterSchema;
  mOpenDialog.InitialDir := mSchemaFileName;
  mOpenDialog.FileName   := mSchemaFileName;
  if mOpenDialog.Execute then
    LoadDocument(mOpenDialog.FileName, ofReadOnly	in mOpenDialog.Options);
end; // TfrmMain.acOpenExecute


//:---------------------------------------------------------------------------
procedure TfrmMain.acSaveExecute(Sender: TObject);
var
  xBackupFile: String;
begin
  if Assigned(mDoc) then begin
    mSchemaModified  := False;
    if cobEnumPool.ItemIndex >= 0 then
      mLoepfeBody.Enumerations.Values[cobEnumPool.Text] := meEnumPoolValues.Lines.CommaText;

    // vor dem Speichern �nderungen �bernehmen
    WriteValuesToLoepfeBody;
    WriteValuesToElement;
    // eine Sicherheitskopie erstellen
    xBackupFile := ChangeFileExt(mSchemaFileName, '.bak');
    CopyFile(PChar(mSchemaFileName), PChar(xBackupFile), False);
    // und schlussendlich noch speichern
    mDoc.save(mSchemaFileName);
  end;
end; // TfrmMain.acSaveExecute

//:---------------------------------------------------------------------------
procedure TfrmMain.AssignEnums;
begin
  cobEnumeration.Items.Text := 'None';
  cobEnumeration.Items.AddStrings(cobEnumPool.Items);

  lbEnumerationAvailable.Clear;
end; // TfrmMain.AssignEnums

procedure TfrmMain.bShowAssignedElementsxClick(Sender: TObject);
begin
  if Sender is TAction then
    TAction(Sender).Checked := not(TAction(Sender).Checked);

  mCurrentElement := nil;
  BuildTree(true);
  CheckControlState;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.BuildTree(aFullExpand: boolean);
var
  i: Integer;
  xActNode: TTreeNode;
  xSelectedObject: Pointer;
  xVisibleItems: integer;
  xNodesToCollapse: Array of TTreeNode;
  //............................................................
  procedure LocBuildTree(aParentNode: TTreeNode; aElement: TBaseElement);
  var
    i: Integer;
    xNode: TTreeNode;
    xAddNode: boolean;
    xFilterPatternSet: TFilterPatternSet;

    function GetFilterSetFromListBox(aListbox: TCheckListBox; aFirstItem: TFilterPattern): TFilterPatternSet;
    var
      i: Integer;
    begin
      result := [];
      for i := 0 to aListBox.Items.Count -1 do begin
        if aListbox.Checked[i] then
          Include(result, TFilterPattern(ord(aFirstItem) + i));
      end;// for i := 0 to aListBox.Items.Count -1 do begin
    end;// function GetFilterSetFromListBox(aListbox: TCheckListBox; aFirstItem: TFilterPattern): TFilterPatternSet;

  begin
    xAddNode := true;

    //: -----------------------------------------------------------
    // Filter auswerten
    xFilterPatternSet := [];
    if acAdvancedFilter.Checked then begin
      // Aktuelles Filterset anhand der Checkboxen bestimmen
      xFilterPatternSet := GetFilterSetFromListBox(mPatternWithList, cFirstWithFilter);
      xFilterPatternSet := xFilterPatternSet + GetFilterSetFromListBox(mPatternWithoutList, cFirstWithoutFilter);
    end;// if acAdvancedFilter.Checked then begin

    // Bestimmen ob nur die aktiven, die inaktiven oder alle Elemente angezeigt werden sollen
    if bShowAssignedElements.Down then
      Include(xFilterPatternSet, fpAssigned);
    if bShowUnAssignedElements.Down then
      Include(xFilterPatternSet, fpUnAssigned);

    // Der Loepfe Body wird nicht gefiltert
    if aElement is TMMElement then
      // Knoten wird angezeigt wenn mindestens ein Blatt angezeigt werden muss
      xAddNode := xAddNode and (TMMElement(aElement).GetFilterElementCount(xFilterPatternSet, edTreeFilter.Text, not(bOr.Down)) > 0);

    if xAddNode then begin
      xNode      := tvSchema.Items.AddChildObject(aParentNode, aElement.ElementName, aElement);
      xNode.Data := aElement;
      // MMSelektionspfad hinterlegen
      aElement.MMPath := GetNodePath(xNode);

      // Gewisse Knoten sollen am Ende wieder eingeklappt werden
      if mNodesToCollapseNames.Strings.IndexOf(aElement.ElementName) >= 0 then begin
        SetLength(xNodesToCollapse, Length(xNodesToCollapse) + 1);
        xNodesToCollapse[Length(xNodesToCollapse) - 1] := xNode;
      end;// if mNodesToCollapseNames.IndexOf(aElement.ElementName) >= 0 then begin

      // Die Anzuahl nur erh�hen, wenn das Element ein Blatt ist
      if (aElement.MMElements.Count = 0) then
        inc(xVisibleItems);
      for i:=0 to aElement.MMElements.Count-1 do
        LocBuildTree(xNode, aElement.MMElements[i]);
    end;
  end;// procedure BuildTree(aParentNode: TTreeNode; aElement: TBaseElement);
  //............................................................
begin
  SetLength(xNodesToCollapse,0);
  tvSchema.Items.BeginUpdate;
  try
    // Selektiertes Element merken
    xSelectedObject := nil;
    if assigned(tvSchema.Selected) then
      xSelectedObject := tvSchema.Selected.Data;

    tvSchema.Items.Clear;

    xVisibleItems := 0;
    sbBottom.Panels[cStatusElementCountPanel].Text := '0';
    if assigned(mLoepfeBody) then
      LocBuildTree(Nil, mLoepfeBody);
    sbBottom.Panels[cStatusElementCountPanel].Text := 'Sichtbar: ' + IntToStr(xVisibleItems);

    if aFullExpand then
      tvSchema.FullExpand;

    for i := 0 to Length(xNodesToCollapse) - 1 do
      xNodesToCollapse[i].Collapse(false);

    // Vorher selektiertes Objekt wieder herstellen (wenn m�glich)
    if assigned(xSelectedObject) then begin
      // Vom ersten Knoten nach unten
      xActNode := tvSchema.Items.GetFirstNode;
      while (assigned(xActNode)) and (not(assigned(tvSchema.Selected))) do begin
        // aktuellen Knoten selektiren, wenn auf das selbe MMElement wie vorher verwiesen wird
        if xActNode.Data = xSelectedObject then begin
          tvSchema.Selected := xActNode;
          tvSchema.Selected.MakeVisible;
          tvSchema.TopItem  := xActNode;
        end;//
        // W�hlt den n�chsten Knoten (unabh�ng von der aktuellen Ebene)
        xActNode := xActNode.GetNext;
      end;// while assigned(xActNode) do begin
    end;// if assigned(xSelectedObject) then begin

    // Wenn kein Knoten selektiert ist, dann wenigstens den ersten Knoten selektieren
    if not(assigned(tvSchema.Selected)) then
      tvSchema.Selected := tvSchema.Items.GetFirstNode;
  finally
    tvSchema.Items.EndUpdate;
    SetLength(xNodesToCollapse,0);
  end;// try finally
end; // TfrmMain.BuildTree

//:---------------------------------------------------------------------------
function TfrmMain.CheckControlState: Boolean;
var
  xEventEnabled: Boolean;
  xDataType: TDataType;
begin
  xEventEnabled := assigned(mCurrentElement) and (mCurrentElement.MMElements.Count = 0);
  Result := (cobDataType.ItemIndex > 0);
  // die Controls entsprechend der Umgebung freischalten
  xDataType := TDataType(cobDataType.ItemIndex);
  // Grunddaten
  edOffset.Enabled      := Result and (xDataType <> dtIgnore) and (mCanSave);
  edCount.Enabled       := Result and (xDataType in [dtChar, dtByte, dtUCBit, dtUCChar, dtUTF16]) and (mCanSave);
  edDefault.Enabled     := Result and (mCanSave);
  cbConvertBoth.Enabled := Result and (mCanSave);
  cbConvertUp.Enabled   := Result and (mCanSave);
  cbConvertDown.Enabled := Result and (mCanSave);

  // Config Code
  rbCCNone.Enabled := result and (mCanSave);
  rbCCA.Enabled    := result and (mCanSave);
  rbCCB.Enabled    := result and (mCanSave);
  rbCCC.Enabled    := result and (mCanSave);
  rbCCD.Enabled    := result and (mCanSave);

  // Facet
  edMinIncl.Enabled    := Result and (not(xDataType in [dtChar, dtChar, dtUCBit, dtUCChar])) and (mCanSave);
  edMinExcl.Enabled    := edMinIncl.Enabled and (mCanSave);
  edMaxIncl.Enabled    := edMinIncl.Enabled and (mCanSave);
  edMaxExcl.Enabled    := edMinIncl.Enabled and (mCanSave);
  // Bitfeld
  edBitOffset.Enabled  := Result and (xDataType in [dtByte, dtSmallint, dtWord, dtShortint, dtDWord, dtLongint]) and (mCanSave);
  edBitCount.Enabled   := edBitOffset.Enabled and (mCanSave);
  // Aufz�hlungen
  cobEnumeration.Enabled         := Result and (xDataType in [dtByte, dtSmallint, dtWord, dtShortint, dtDWord, dtLongint]) and (mCanSave);
  lbEnumerationAvailable.Enabled := cobEnumeration.Enabled and (mCanSave);
  lbEnumerationSelected.Enabled  := Result and (mCanSave);
  // Konvertierungen
  cobConvertMode.Enabled         := Result and (xDataType <> dtIgnore) and (mCanSave);
  lbConvertPara.Enabled          := Result and (mCanSave);
  // Ereignisse
  edEventPath.Enabled            := Result or (xEventEnabled and IsMillmasterMap) and (mCanSave);
  rbEventInt.Enabled             := Result or (xEventEnabled and IsMillmasterMap) and (mCanSave);
  rbEventExt.Enabled             := Result or (xEventEnabled and IsMillmasterMap) and (mCanSave);
  lbEvents.Enabled               := Result or (xEventEnabled and IsMillmasterMap) and (mCanSave);
  cbEventDelflag.Enabled         := Result or (xEventEnabled and IsMillmasterMap) and (mCanSave);

  // wenn nicht Enabled dann Eintr�ge l�schen
  if not Result then begin
    // Grunddaten
    edOffset.Value        := 0;
    edCount.Value         := -1;
    edDefault.Text        := '';
    cbConvertBoth.Checked := true;
    // Facet
    edMinIncl.Text        := '';
    edMinExcl.Text        := '';
    edMaxIncl.Text        := '';
    edMaxExcl.Text        := '';
    // Bitfeld
    edBitOffset.Text      := '';
    edBitCount.Text       := '';
    // Aufz�hlungen
    lbEnumerationSelected.Clear;
    // Konvertierungen
    cobConvertMode.ItemIndex := -1;
    cobOperator.ItemIndex    := -1;
    edOperatorValue.Text     := '';
    lbConvertPara.Clear;
    memoCurrentEvent.Clear;
    // Ereignis
    if not(xEventEnabled and IsMillmasterMap) then
      lbEvents.Clear;
    cbEventDelflag.Checked := false;
  end;
end; // TfrmMain.CheckControlState

//:---------------------------------------------------------------------------
procedure TfrmMain.cobConvertModeChange(Sender: TObject);
begin
  case cobConvertMode.ItemIndex of
    cConvertNone,
    cConvertNot:  lbConvertPara.Clear;
  else
  end;
end; // TfrmMain.cobConvertModeChange

//:---------------------------------------------------------------------------
procedure TfrmMain.cobDataTypeChange(Sender: TObject);
begin
  mCanUndo := True;
  CheckControlState;
end; // TfrmMain.cobDataTypeChange

//:---------------------------------------------------------------------------
procedure TfrmMain.cobEnumerationChange(Sender: TObject);
begin
  lbEnumerationAvailable.Items.CommaText := mLoepfeBody.Enumerations.Values[cobEnumeration.Text];
end; // TfrmMain.cobEnumerationChange

//:---------------------------------------------------------------------------
procedure TfrmMain.cobEnumPoolChange(Sender: TObject);
begin
  meEnumPoolValues.Lines.CommaText := mLoepfeBody.Enumerations.Values[cobEnumPool.Text];
end; // TfrmMain.cobEnumPoolChange

//:---------------------------------------------------------------------------
procedure TfrmMain.ControlEntryChange(Sender: TObject);
begin
  mCanUndo := True;
end; // TfrmMain.ControlEntryChange

//:---------------------------------------------------------------------------
function TfrmMain.CreateNewMap(aMsg: String; var aCategory: string; var aNewDescription: string): Boolean;
begin
  result := false;
  with TfrmNewMapInputForm.Create(nil) do try
    Categories := mLoepfeBody.MapCategories;
    if lbMapCategories.ItemIndex > 1 then
      SelectedCategory := lbMapCategories.Items[lbMapCategories.ItemIndex];
    LoepfeBody := mLoepfeBody;
    ShowModal;
    if ModalResult = mrOK then begin
      if mLoepfeBody.MapIDs.IndexOfName(MapName) = -1 then begin
        lbMapID.ItemIndex := lbMapID.Items.Add(MapName);
        aNewDescription   := Description;
        aCategory         := Category;
        result := true;
      end else begin
        ShowMessage('Eine Map mit diesem Namen existiert bereits!');
      end;// if mLoepfeBody.MapIDs.IndexOfName(MapFile) = -1 then begin
    end;// if ModalResult = mrOK then begin
  finally
    Free;
  end;// with TfrmNewMapInputForm.Create do try
end; // TfrmMain.CreateNewMap

(*---------------------------------------------------------
  �berpr�ft ob der Defaultwert in den erlaubten Grenzen liegt
----------------------------------------------------------*)
procedure TfrmMain.CheckDefault;

  procedure SetNewDefault(aDefault: string);
  begin
    edDefault.Text := aDefault;
  end;// procedure SetNewDefault(aDefault: string);

begin
  // MinIncl
  if edMinIncl.Text > '' then begin
    try
      if StrToFloat(edDefault.Text) < edMinIncl.Value then
        SetNewDefault(edMinIncl.Text);
    except
      SetNewDefault(edMinIncl.Text);
    end;// try except
  end;// if edMinIncl.Text > '' then begin

  // MaxIncl
  if edMaxIncl.Text > '' then begin
    try
      if StrToFloat(edDefault.Text) > edMaxIncl.Value then
        SetNewDefault(edMaxIncl.Text);
    except
      SetNewDefault(edMaxIncl.Text);
    end;// try except
  end;// if edMaxIncl.Text > '' then begin

  // MinExcl
  if edMinExcl.Text > '' then begin
    try
      if StrToFloat(edDefault.Text) <= edMinExcl.Value then
        SetNewDefault(FloatToStr(edMinExcl.value + 0.1));
    except
      SetNewDefault(FloatToStr(edMinExcl.value + 0.1));
    end;// try except
  end;// if edMinExcl.Text > '' then begin

  // MaxExcl
  if edMaxExcl.Text > '' then begin
    try
      if StrToFloat(edDefault.Text) >= edMaxExcl.Value then
        SetNewDefault(FloatToStr(edMaxExcl.value - 0.1));
    except
      SetNewDefault(FloatToStr(edMaxExcl.value - 0.1));
    end;// try except
  end;// if edMaxExcl.Text > '' then begin
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.edMaxExclChange(Sender: TObject);
begin
  mCanUndo := True;
  if edMaxExcl.Text <> '' then
    edMaxIncl.Text := '';
end; // TfrmMain.edMaxExclChange

//:---------------------------------------------------------------------------
procedure TfrmMain.edMaxInclChange(Sender: TObject);
begin
  mCanUndo := True;
  if edMaxIncl.Text <> '' then
    edMaxExcl.Text := '';
end; // TfrmMain.edMaxInclChange

//:---------------------------------------------------------------------------
procedure TfrmMain.edMinExclChange(Sender: TObject);
begin
  mCanUndo := True;
  if edMinExcl.Text <> '' then
    edMinIncl.Text := '';
end; // TfrmMain.edMinExclChange

//:---------------------------------------------------------------------------
procedure TfrmMain.edMinInclChange(Sender: TObject);
begin
  mCanUndo := True;
  if edMinIncl.Text <> '' then
    edMinExcl.Text := '';
end; // TfrmMain.edMinInclChange

//:---------------------------------------------------------------------------
procedure TfrmMain.edTreeFilterKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Assigned(mLoepfeBody) then
    timTreeFilter.Restart;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  i: Integer;
  xDT: TDataType;
  xFilter: TFilterPattern;
begin
  tvSchema.Items.Clear;
  mMapElementChange := TMapElementChange.Create;

  mCanSave        := False;
  mCanUndo        := False;
  mCopyElement    := Nil;
  mCurrentControl := Nil;
  mDoc            := CoDOMDocument40.Create;
  mFunctionFrame  := Nil;
  mLoepfeBody     := Nil;
  mSchemaModified  := False;
  // Datentype
  for xDT:=Low(TDataType) to High(TDataType) do
    cobDataType.Items.AddObject(cBasicTypeNames[xDT], Pointer(xDT));
  cobDataType.DropDownCount := ord(High(TDataType)) + 1;
  // Funktionen
  for i:=Low(cFunctionNames) to High(cFunctionNames) do
    cobConvertMode.Items.Add(cFunctionNames[i]);
  // Operatoren
  for i:=Low(cOperatorNames) to High(cOperatorNames) do
    cobOperator.Items.Add(cOperatorNames[i]);

  // wenn Appl im Delphi l�uft gleich ein Schema zu begin laden
  if (DebugHook <> 0) then
    acEnableMillmasterMap.Checked := true;

  bShowAssignedElements.Caption   := '';
  bShowUnassignedElements.Caption := '';
  bShowPrevElement.Caption        := '';
  bNextElement.Caption            := '';
  acAdvancedFilter.Execute;

  mPatternWithList.Clear;
  for xFilter := cFirstWithFilter to pred(cFirstWithoutFilter) do
    mPatternWithList.Items.Add(cFilterPatternNames[xFilter]);

  mPatternWithoutList.Clear;
  for xFilter := cFirstWithoutFilter to pred(cFirstExtFilter) do
    mPatternWithoutList.Items.Add(cFilterPatternNames[xFilter]);
end; // TfrmMain.FormCreate

//:---------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(mMapElementChange);
  UnlockFile;
  mDoc := Nil;
  if Assigned(mCopyElement) then
    mCopyElement.Free;
end; // TfrmMain.FormDestroy

function TfrmMain.GetMapDataDir: string;
begin
  result := ExtractFilePath(mSchemaFileName) + 'Mapfiles\';
  if not(DirectoryExists(result)) then
    ForceDirectories(result);
end;

//:---------------------------------------------------------------------------
function TfrmMain.IsMillmasterMap: Boolean;
begin
  Result := (lbMapID.ItemIndex >= 0) and AnsiSameText(lbMapID.Items[lbMapID.ItemIndex], cMillmasterMap);
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.lbEnumerationAvailableDblClick(Sender: TObject);
begin
  acEnumAdd.Execute;
end; // TfrmMain.lbEnumerationAvailableDblClick

//:---------------------------------------------------------------------------
procedure TfrmMain.lbEnumerationSelectedClick(Sender: TObject);
var
  xName: String;
begin
  with lbEnumerationSelected do begin
    if ItemIndex <> -1 then begin
      xName := Items.Names[ItemIndex];
      if not(AnsiSameText(Items.Values[xName], cEnumOthers)) then begin
        seEnumValue.Value := StrToIntDef(Items.Values[xName], 0);
        cbEnumOthers.Checked := false;
      end else begin
        cbEnumOthers.Checked := true;
      end;// if not(AnsiSameText(Items.Values[xName], cEnumOthers)) then begin
    end;
  end;
end; // TfrmMain.lbEnumerationSelectedClick

procedure TfrmMain.lbEventsClick(Sender: TObject);
begin
  memoCurrentEvent.Lines.CommaText := lbEvents.Items[lbEvents.ItemIndex];
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.lbMapCategoriesClick(Sender: TObject);
var
  xSelectedMap: string;
  i: integer;
  xCategory: string;
  xAddMap: boolean;

  (*---------------------------------------------------------
    F�gt ein Map zur Liste mit den verf�gbaren Maps hinzu
  ----------------------------------------------------------*)
  procedure AddMapID(aMapID: string);
  var
    xAddMap: boolean;
  begin
    xAddMap := false;
    case rbMapIDFilter.ItemIndex of
      // Alle
      0: xAddMap := true;
      // Nur Settings Maps
      1: xAddMap := (aMapID[Length(aMapID)] = 'S');
      // Nur Bauzustands Maps
      2: xAddMap := (aMapID[Length(aMapID)] = 'C');
    end;// case rbMapIDFilter.ItemIndex of

    // MapID hinzuf�gen, wenn der Filter das zul�sst
    if xAddMap then
      lbMapID.Items.Add(aMapID);
  end;// procedure AddMapID(aMapID: string);
begin
  // Selektiertes Map sichern
  if lbMapID.ItemIndex >= 0 then
    xSelectedMap := lbMapID.Items[lbMapID.ItemIndex];

  lbMapID.Clear;
  for i := 0 to mLoepfeBody.MapCategories.Count - 1 do begin
    xAddMap := acEnableMillmasterMap.Checked and AnsiSameText(mLoepfeBody.MapIDs.Names[i], cMillmasterMap);
    xAddMap := xAddMap or not(AnsiSameText(mLoepfeBody.MapIDs.Names[i], cMillmasterMap));
    if xAddMap then begin
      case lbMapCategories.ItemIndex of
        // Alle Kategorien
        0: AddMapID(mLoepfeBody.MapCategories.Names[i]);
        // Keine kategorie
        1: begin
            if mLoepfeBody.MapCategories.Values[mLoepfeBody.MapCategories.Names[i]] = '' then
              AddMapID(mLoepfeBody.MapCategories.Names[i]);
          end;
      else
        // Spezifische Kategorie
        if lbMapCategories.ItemIndex >= 0 then begin
          xCategory := lbMapCategories.Items[lbMapCategories.ItemIndex];
          if ANSISameText(xCategory, mLoepfeBody.MapCategories.Values[mLoepfeBody.MapCategories.Names[i]]) then
            AddMapID(mLoepfeBody.MapCategories.Names[i]);
        end;// if lbMapCategories.ItemIndex >= 0 then begin
      end;// case lbMapCategories.ItemIndex of
    end;// if xAddMap then begin
  end;// for i := 0 to mLoepfeBody.MapCategories.Count - 1 do begin

  // Das selbe Map selektieren wie vorher (Wenn es in der aktuellen Liste vorhanden ist)
  if xSelectedMap > '' then
    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xSelectedMap);
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.lbMapIDClick(Sender: TObject);
begin
 with lbMapID do begin
    if ItemIndex <> -1 then begin
      // �nderungen �bernehmen
      WriteValuesToLoepfeBody;
      WriteValuesToElement;
      // die MapID dem DOM-Baum mitgeben
      mLoepfeBody.MapID  := lbMapID.Items[lbMapID.ItemIndex];
      // Werte auslesen und in den Komponenten darstellen
      ReadValuesFromLoepfeBody;
      ReadValuesFromMMElement;
      tvSchema.Invalidate;
      BuildTree(true);
    end;
  end;
  
  if assigned(mFindCC) and mFindCC.visible then
    acFindCC.Execute;
end; // TfrmMain.lbMapIDClick

//:---------------------------------------------------------------------------
function TfrmMain.LockFile: Boolean;
var
  xName: String;
begin
  Result := False;
  xName := ChangeFileExt(mSchemaFileName, '.lck');
  if FileExists(xName) then begin
    with TmmStringList.Create do
    try
      LoadFromFile(xName);
      ShowMessage(Format('Die Datei wird bereits von %s ge�ffnet. Speichern wird nicht m�glich sein.', [UpperCase(Text)]));
    finally
      Free;
    end;
  end else begin
    with TmmStringList.Create do
    try
      Text := GetWindowsUserName;
      SaveToFile(xName);
      Result := True;
    finally
      Free;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
var
  xEnumOthersEnabled: Boolean;
  i: Integer;
  xLoepfeBodyValid: Boolean;
  xEnableEvents: boolean;
begin
  Handled := True;
  if ActiveControl <> mCurrentControl then begin
    mCurrentControl := ActiveControl;
    if Assigned(mCurrentControl) then
      mDescriptionMemo.Text := mCurrentControl.Hint;
  end;
  xLoepfeBodyValid          := Assigned(mLoepfeBody);

  acSave.Enabled            := xLoepfeBodyValid and mCanSave;
  // Import/Export
  acExport.Enabled          := xLoepfeBodyValid and (lbMapID.ItemIndex <> -1);
//  acImport.Enabled        := xLoepfeBodyValid and (cobMapID.ItemIndex <> -1);

  // Statistische Funktionen
  acFunctionShow.Enabled    := (lbMapID.ItemIndex <> -1) and (lbFunction.ItemIndex <> -1) and xLoepfeBodyValid;

  // TabSheet Enumeration
  acEnumPoolNew.Enabled     := xLoepfeBodyValid;
  acEnumPoolDelete.Enabled  := (cobEnumPool.ItemIndex <> -1) and (mCanSave);

  cobEnumPool.Enabled       := xLoepfeBodyValid;
  meEnumPoolValues.Enabled  := (cobEnumPool.ItemIndex <> -1) and (mCanSave);

  // TabSheet Mapping
  acMapNew.Enabled          := xLoepfeBodyValid;
  acMapCopy.Enabled         := (lbMapID.ItemIndex <> -1);
  acMapDelete.Enabled       := (lbMapID.ItemIndex <> -1);
  tvSchema.Enabled          := (lbMapID.ItemIndex <> -1);

  lbMapID.Enabled           := xLoepfeBodyValid;
  lbMapCategories.Enabled   := xLoepfeBodyValid;
  meDescription.Enabled     := (lbMapID.ItemIndex <> -1) and (mCanSave);
  edStructSize.Enabled      := (lbMapID.ItemIndex <> -1) and (mCanSave);
  meHistory.Enabled         := (lbMapID.ItemIndex <> -1) and (mCanSave);

  // Element Handling
  acElementCopy.Enabled     := Assigned(mCurrentElement) and (mCanSave);
  acElementPaste.Enabled    := Assigned(mCurrentElement) and (mCanSave);
  acPasteActiveOnly.Enabled := Assigned(mCurrentElement) and (mCanSave);
  acElementClear.Enabled    := Assigned(mCurrentElement) and (mCanSave);
  acElementUndo.Enabled     := Assigned(mCurrentElement) and mCanUndo and (mCanSave);

  // Enumeration
  acEnumAdd.Enabled         := (lbEnumerationAvailable.SelCount > 0) and (mCanSave);
  acEnumDelete.Enabled      := (lbEnumerationSelected.ItemIndex <> -1) and (mCanSave);
  acEnumSetDefault.Enabled  := (lbEnumerationSelected.ItemIndex <> -1) and (mCanSave);
  acEnumUp.Enabled          := (lbEnumerationSelected.ItemIndex > 0) and (mCanSave);
  acEnumDown.Enabled        := (lbEnumerationSelected.ItemIndex <> -1) and (lbEnumerationSelected.ItemIndex < lbEnumerationSelected.Items.Count-1) and (mCanSave);
  acEnumIterate.Enabled     := (lbEnumerationSelected.Items.Count > 0) and (mCanSave);

  xEnumOthersEnabled := true;
  for i := 0 to lbEnumerationSelected.Items.Count-1 do begin
    if copyRight(lbEnumerationSelected.Items[i], '=') = cEnumOthers then begin
      // 'others' bereits vorhanden. Deshalb darf die CheckBox nicht verf�gbar sein
      if i <> lbEnumerationSelected.ItemIndex then
        xEnumOthersEnabled := false;
      BREAK;
    end;// if lbEnumerationSelected.Items.Values[xStringList.Names[i]] = cEnumOthers then begin
  end; // for i := 0 to lbEnumerationSelected.Items.Count-1 do begin
  cbEnumOthers.Enabled     := (lbEnumerationSelected.ItemIndex <> -1) and xEnumOthersEnabled and (mCanSave);

  // Config Code
  cbCCBit.Enabled          := (rbCCA.Checked) or (rbCCB.Checked) or (rbCCC.Checked) or (rbCCD.Checked);

  // Konvertierung
  cobOperator.Enabled      := (cobConvertMode.ItemIndex = 1) and (mCanSave);
  edOperatorValue.Enabled  := (cobConvertMode.ItemIndex = 1) and (mCanSave);
  acConvAdd.Enabled        := // formula
                              ((cobConvertMode.ItemIndex = 1) and (cobOperator.ItemIndex <> -1) and (edOperatorValue.Text <> '')) OR
                              // uc
                              ((cobConvertMode.ItemIndex = 2) and (edOperatorValue.Text <> '') and (lbConvertPara.Items.Count = 0))
                               and (mCanSave);
  acConvDelete.Enabled     := (lbConvertPara.ItemIndex <> -1) and (mCanSave);
  acConvUp.Enabled         := (lbConvertPara.ItemIndex > 0) and (mCanSave);
  acConvDown.Enabled       := (lbConvertPara.ItemIndex <> -1) and (lbConvertPara.ItemIndex < lbConvertPara.Items.Count-1) and (mCanSave);

  // Ereignisse
  // Millmaster Map
  xEnableEvents := IsMillmasterMap;
  // Nur Enabled, wenn ein "Blatt" selektiert ist
  xEnableEvents := xEnableEvents and assigned(mCurrentElement) and (mCurrentElement.MMElements.Count = 0);
  xEnableEvents := xEnableEvents or (cobDataType.ItemIndex > 0);

  acEventNew.Enabled       :=  xEnableEvents and (mCanSave);
  acEventDelete.Enabled    := (lbEvents.ItemIndex <> -1) and (mCanSave);
  acEventUp.Enabled        := (lbEvents.ItemIndex > 0) and (mCanSave);
  acEventDown.Enabled      := (lbEvents.ItemIndex <> -1) and (lbEvents.ItemIndex < lbEvents.Items.Count-1) and (mCanSave);

  // Navigation
  acPreviousItem.Enabled   := (assigned(tvSchema.Selected)) and (tvSchema.Selected.GetPrev <> nil);
  acNextItem.Enabled       := (assigned(tvSchema.Selected)) and (tvSchema.Selected.GetNext <> nil);
  acChangeOffRec.Enabled   := (edOffset.Enabled);
  acDeleteFilter.Enabled   := (edTreeFilter.Text > '');
  
  // Mapfile

  // Millmastermap
  cbEventDelflag.Enabled  := acEventNew.Enabled ;
  // Konstantendatei nur exportieren wenn Millmastermap freigeschaltet
  acExportMMConst.Visible := acEnableMillmasterMap.Checked;
  acExportMMConst.Enabled := IsMillmasterMap and acEnableMillmasterMap.Checked;
end; // TfrmMain.mActionListUpdate

//:---------------------------------------------------------------------------
procedure TfrmMain.meEnumPoolValuesKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in cAllowedEnumChars) and (cobEnumPool.ItemIndex <> -1) then
    // jede Ver�nderung wird sofort im Elementobjekt gespeichert
    mLoepfeBody.Enumerations.Values[cobEnumPool.Text] := meEnumPoolValues.Lines.CommaText
  else
    Key := #0;
end; // TfrmMain.meEnumPoolValuesKeyPress

//:---------------------------------------------------------------------------
procedure TfrmMain.OpenSchema(aReadOnly: boolean);
var
  i: Integer;
  //............................................................
  procedure ParseElements(aInstanceClass: TBaseElementClass; aXMLNode: IXMLDOMNode; aMMElement: TBaseElement);
  var
    xElement: TBaseElement;
  begin
    xElement := aMMElement;
    repeat
      if AnsiSameCaption(aXMLNode.nodeName, 'xs:element') then begin
        // neues Element erstellen mit der Instanz von dem Klassentyp
        xElement := aInstanceClass.Create;
        if IsInterface(aXMLNode, IXMLDOMElement) then
          xElement.DOMElement := IXMLDOMElement(aXMLNode);
  
        if aMMElement = Nil then
          // Root Element in mLoepfeBody behalten
          mLoepfeBody := TLoepfeBody(xElement)
        else
          // alle weiteren in der Hierarchie ablegen
          aMMElement.MMElements.Add(TMMElement(xElement));
      end;

      // zuerst den Baum weiter nach unten parsern...
      if aXMLNode.hasChildNodes then
        ParseElements(TMMElement, aXMLNode.firstChild, xElement);

      //...und erst jetzt weiter zum n�chsten DOM Element
      aXMLNode := aXMLNode.nextSibling;
    until not Assigned(aXMLNode);
  end;
  //............................................................
  procedure ClearLoepfeBody;
  begin
    tvSchema.Items.Clear;
    FreeAndNil(mLoepfeBody);
    mCurrentElement := Nil;
    // Map Controls l�schen
    lbMapID.Clear;
    lbMapCategories.Clear;
    edStructSize.Value := 0;
    meDescription.Clear;
    meHistory.Clear;
    // Aufz�hlung
    cobEnumPool.Clear;
    meEnumPoolValues.Clear;
  end;
  //............................................................

begin
  Caption := cCaptionText;
  // Allgemeines Initialisieren
  cobEnumPool.Clear;
  meEnumPoolValues.Clear;
  cobDataType.Enabled   := False;
  cobDataType.ItemIndex := -1;

  ClearLoepfeBody;
  mSchemaModified  := False;

  mDoc.load(mSchemaFileName);
  mDoc.setProperty('SelectionNamespaces', cSchemaNameSpace + ' ' + cLoepfeNameSpace);
  if mDoc.parseError.errorCode = 0 then begin
    if aReadOnly then
      mCanSave := false
    else
      mCanSave := LockFile;

    Caption  := Format('%s  [%s]', [Caption, ExtractFileName(mSchemaFileName)]);
    if not(mCanSave) then
      Caption := Caption + ' (Read Only)';

    // nach �ffnen zuerst parsern...
    ParseElements(TLoepfeBody, mDoc.documentElement.firstChild, Nil);

    // ...anschliessend die globalen Informationen auslesen...
    UpdateMapList;
    // Erste Kategorie selektieren (Eintrag 'Alle Kategorien')
    if lbMapCategories.Items.Count > 0 then
      lbMapCategories.ItemIndex := 0;
    lbMapCategoriesClick(lbMapCategories);

    for i:=0 to mLoepfeBody.Enumerations.Count-1 do
      cobEnumPool.Items.Add(mLoepfeBody.Enumerations.Names[i]);
    AssignEnums;

    // ...dann den Baum im TreeView anzeigen
    BuildTree(false);
    if tvSchema.Items.Count > 0 then
      tvSchema.Selected := tvSchema.Items[0];

    CheckControlState;
  end;
  TSchematoolSettings.Instance.SaveRecentFiles(mSchemaFileName, not(mCanSave));
end; // TfrmMain.OpenSchema

//:---------------------------------------------------------------------------
procedure TfrmMain.ReadValuesFromLoepfeBody;
begin
  if lbMapID.ItemIndex <> -1 then begin
    edStructSize.Value := mLoepfeBody.StructSize;
    meDescription.Text := mLoepfeBody.Description;
    meHistory.Text     := mLoepfeBody.History;
  end else begin
    edStructSize.Value := 0;
    meDescription.Clear;
    meHistory.Clear;
  end;
end; // TfrmMain.ReadValuesFromLoepfeBody

//:---------------------------------------------------------------------------
procedure TfrmMain.ReadValuesFromMMElement;
var
  xIndex: Integer;
begin
  // mal prophilaktisch kein Datentyp selektieren
  cobDataType.ItemIndex := 0;

  if Assigned(mCurrentElement) then begin
    // Beschreibung
    meDocu.Text := mCurrentElement.Documentation;
    // Datentyp kann nur gew�hlt werden, wenn es ein Endknoten ist
    cobDataType.Enabled := (mCurrentElement.MMElements.Count = 0);
    if cobDataType.Enabled then
      cobDataType.ItemIndex := cobDataType.Items.IndexOfObject(Pointer(mCurrentElement.Datatype));

    // hier noch generelle Initialisierungen vornehmen, welche bei jedem Elementwechsle gemacht werden m�ssen
    // Aufz�hlungen
    cobEnumeration.ItemIndex := -1;
    lbEnumerationAvailable.Clear;
    seEnumValue.Value        := 0;
    lbEnumerationSelected.Clear;
    // Konvertierungen
    cobConvertMode.ItemIndex := -1;
    cobOperator.ItemIndex    := -1;
    edOperatorValue.Text     := '';
    lbConvertPara.Clear;
    // Ereignisse
    edEventPath.Text         := '';
    cbEventDelflag.Checked   := false;
    rbCCNone.Checked         := true;
    cbCCBit.ItemIndex        := 0;
    edYMElementName.Text     := '';
    cbProofed.Checked := false;
    cbObserve.Checked := False;
  end // if Assigned
  else
    meDocu.Text := mLoepfeBody.Documentation;
  
  // Enabled oder Disabled die Controls anhand der Umgebung
  if CheckControlState or IsMillmasterMap then begin
    // wenn die Controls freigegeben sind, dann die Werte abf�llen
    if not(IsMillmasterMap) then begin
      // Grunddaten
      edOffset.Value        := mCurrentElement.Offset;
      edCount.Value         := mCurrentElement.Count;
      edDefault.Text        := mCurrentElement.Default;

      cbConvertBoth.Checked := (mCurrentElement.Direction = cDirBoth);
      cbConvertUp.Checked   := (mCurrentElement.Direction = cDirUp);
      cbConvertDown.Checked := (mCurrentElement.Direction = cDirDown);

      // Facet
      edMinIncl.Text        := mCurrentElement.MinIncl;
      edMinExcl.Text        := mCurrentElement.MinExcl;
      edMaxIncl.Text        := mCurrentElement.MaxIncl;
      edMaxExcl.Text        := mCurrentElement.MaxExcl;
      // Bitfeld
      edBitOffset.Text      := mCurrentElement.BitOffset;
      edBitCount.Text       := mCurrentElement.BitCount;
      // Aufz�hlungen
      lbEnumerationSelected.Items.CommaText := mCurrentElement.Enumeration;
      // Konvertierungen
      xIndex := cobConvertMode.Items.IndexOf(mCurrentElement.ConvertMode);
      cobConvertMode.ItemIndex     := xIndex;
      lbConvertPara.Items.CommaText := mCurrentElement.ConvertPara;

      if mCurrentElement.IsConfigCode then begin
        if mCurrentElement.ConfigCode > '' then begin
          case mCurrentElement.ConfigCode[1] of
            'A': rbCCA.Checked := true;
            'B': rbCCB.Checked := true;
            'C': rbCCC.Checked := true;
            'D': rbCCD.Checked := true;
          else
            ShowMessage('Unbekannter ConfigCode');
          end;// if mCurrentElement.IsConfigCode then begin
        end;// if mCurrentElement.ConfigCode > '' then begin
        cbCCBit.ItemIndex := mCurrentElement.CCBitNumber;
      end;// if mCurrentElement.IsConfigCode then begin
    end;// if not(IsMillmasterMap) then begin
    // Ereignisse
    if assigned(mCurrentElement) then begin
      lbEvents.Items.Assign(mCurrentElement.Events);
      edYMElementName.Text := mCurrentElement.YMElementNames.Commatext;
    end;// if assigned(mCurrentElement) then begin

    cbProofed.Checked := (efProofed in mCurrentElement.Flags);
    cbObserve.Checked := (efObserve in mCurrentElement.Flags);
  end; // if CheckControlState
  // wenn nun alles ausgelesen wurde -> Flag l�schen
  mCanUndo := False;
end; // TfrmMain.ReadValuesFromMMElement

//:---------------------------------------------------------------------------
procedure TfrmMain.seEnumValueChange(Sender: TObject);
var
  xName: String;
begin
  with lbEnumerationSelected do begin
    if ItemIndex <> -1 then begin
      xName := Items.Names[ItemIndex];
      Items.Values[xName] := IntToStr(seEnumValue.Value);
      mCanUndo := True;
    end;
  end;
end; // TfrmMain.seEnumValueChange

//:---------------------------------------------------------------------------
procedure TfrmMain.cbEnumOthersClick(Sender: TObject);
var
  xName: String;
begin
  with lbEnumerationSelected do begin
    if ItemIndex <> -1 then begin
      xName := Items.Names[ItemIndex];
      if cbEnumOthers.Checked then
        Items.Values[xName] := cEnumOthers
      else
        Items.Values[xName] := IntToStr(seEnumValue.Value);
      mCanUndo := True;
    end;
  end;
end;// procedure TfrmMain.mmCheckBox1Click(Sender: TObject);

//:---------------------------------------------------------------------------
procedure TfrmMain.timTreeFilterTimer(Sender: TObject);
begin
  timTreeFilter.Enabled := False;
  BuildTree(true);
end;

(*---------------------------------------------------------
  Liefert den Selektionspfad des Elements
----------------------------------------------------------*)
function TfrmMain.GetNodePath(aNode: TTreeNode): String;
begin
  if Assigned(aNode) then
    Result := GetNodePath(aNode.Parent) + '/' + aNode.Text;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.tvSchemaChange(Sender: TObject; Node: TTreeNode);
var
  xNode: TTreeNode;
begin
  // �nderungen �bernehmen
  WriteValuesToElement;
  // neues Element ermitteln
  xNode := tvSchema.Selected;
  sbBottom.Panels[cStatusXPathPanel].Text := GetNodePath(xNode);
  if Assigned(xNode) then begin
    if xNode.Data = mLoepfeBody then
      meDocu.Text := mLoepfeBody.Documentation
    else
      mCurrentElement := TMMElement(xNode.Data);
  end else
    mCurrentElement := Nil;
  // Werte auslesen und in den Komponenten darstellen
  ReadValuesFromMMElement;
  CheckControlState;

  // Observer benachrichtigen
  mMapElementChange.ChangeElement(mCurrentElement);

  if assigned(mFunctionFrame) then
    mFunctionFrame.ActiveElement := mCurrentElement;
end; // TfrmMain.tvSchemaChange

//:---------------------------------------------------------------------------
procedure TfrmMain.tvSchemaCustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode; State: TCustomDrawState; var
  DefaultDraw: Boolean);
var
  xElement: TMMElement;
begin
  if Node.Data <> mLoepfeBody then begin
    xElement := TMMElement(Node.Data);
    if xElement.MMElements.Count = 0 then begin
      if not(cdsSelected in State) then begin
        if not xElement.Active then
          tvSchema.Canvas.Font.Color := clGrayText;
      end; // if not
    end; // if xElement
  end; // if Node.Data
end; // TfrmMain.tvSchemaCustomDrawItem

procedure TfrmMain.tvSchemaGetImageIndex(Sender: TObject; Node: TTreeNode);
var
  xElement: TMMElement;
begin
  if (assigned(Node.Data)) then begin
    xElement := TMMElement(Node.Data);
    if (assigned(xElement)) and (xElement.MMElements.Count = 0) then begin
      if xElement.Active then begin
        if efObserve in xElement.Flags then
          Node.ImageIndex := 33
        else if efProofed in xElement.Flags then
          Node.ImageIndex := 31
        else
          Node.ImageIndex := 18
      end else begin
        Node.ImageIndex := -1;
      end;// if xElement.Active then begin
    end else begin
      Node.ImageIndex := 19;
    end;
  end; // if Node.Data
  Node.SelectedIndex := Node.ImageIndex;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.UnlockFile;
var
  xName: String;
begin
  if mCanSave then begin
    xName := ChangeFileExt(mSchemaFileName, '.lck');
    DeleteFile(xName);
    mCanSave := False;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmMain.UpdateMapList;
var
  i: integer;
  xCurrentCategory: string;
  xSelectedCat: string;
  xSelectedMap: string;
begin
  if lbMapCategories.ItemIndex >= 0 then
    xSelectedCat := lbMapCategories.Items[lbMapCategories.ItemIndex];
  if lbMapID.ItemIndex >= 0 then
    xSelectedMap := lbMapID.Items[lbMapID.ItemIndex];

  lbMapCategories.Clear;
  lbMapID.Clear;

  // Standard Kategorien hinzuf�gen
  lbMapCategories.Items.Add('[Alle Kategorien]');
  lbMapCategories.Items.Add('[Keine Kategorie]');
  for i := 0 to mLoepfeBody.MapCategories.Count - 1 do begin
    if mLoepfeBody.MapCategories.Value(i) > '' then begin
      xCurrentCategory := mLoepfeBody.MapCategories.Values[mLoepfeBody.MapCategories.Names[i]];

      if (xCurrentCategory > '') and (lbMapCategories.Items.IndexOf(xCurrentCategory) = -1) then
        lbMapCategories.Items.Add(xCurrentCategory);
    end;// if mLoepfeBody.MapCategories.Value(i) > '' then begin
  end;// for i := 0 to mLoepfeBody.MapCategories.Count - 1 do begin

  if xSelectedCat > '' then
    lbMapCategories.ItemIndex := lbMapCategories.Items.IndexOf(xSelectedCat);
  lbMapCategoriesClick(nil);

  if xSelectedMap > '' then
    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xSelectedMap);
end;// procedure TfrmMain.UpdateMapList;

//:---------------------------------------------------------------------------
procedure TfrmMain.WriteValuesToElement;
var
  xFlags: TElementFlagSet;
begin
  if Assigned(mCurrentElement) then begin
    with mCurrentElement do begin
      // Grunddaten
      Datatype := TDataType(cobDataType.Items.Objects[cobDataType.ItemIndex]);

      // F�r das Millmaster Map die Events immer �bernommen
      if Active or IsMillmasterMap then begin
        // Ereignisse
        Events.Assign(lbEvents.Items);
      end;// if Active or IsMillmasterMap then begin

      if Active then begin
        // F�r das Millmaster Map werden nur die Events �bernommen
        if not(IsMillmasterMap) then begin
          Offset := edOffset.Value;
          if edCount.Value > 1 then Count := edCount.Value
                               else Count := -1;

          // Konvertier Richtung
          if cbConvertBoth.Checked then
            Direction := cDirBoth;
          if cbConvertUp.Checked then
            Direction := cDirUp;
          if cbConvertDown.Checked then
            Direction := cDirDown;

          Default := edDefault.Text;

          // Facet
          MinIncl := edMinIncl.Text;
          MinExcl := edMinExcl.Text;
          MaxIncl := edMaxIncl.Text;
          MaxExcl := edMaxExcl.Text;
          // Bitfeld
          BitOffset := edBitOffset.Text;
          BitCount  := edBitCount.Text;
          // Aufz�hlungen
          Enumeration := lbEnumerationSelected.Items.CommaText;
          // Konvertierungen
          ConvertMode := cobConvertMode.Text;
          ConvertPara := lbConvertPara.Items.CommaText;

          if (rbCCA.Checked) or (rbCCB.Checked) or (rbCCC.Checked) or (rbCCD.Checked) then begin
            mCurrentElement.IsConfigCode := true;
            if rbCCA.Checked then
              mCurrentElement.ConfigCode := 'A';
            if rbCCB.Checked then
              mCurrentElement.ConfigCode := 'B';
            if rbCCC.Checked then
              mCurrentElement.ConfigCode := 'C';
            if rbCCD.Checked then
              mCurrentElement.ConfigCode := 'D';
            mCurrentElement.CCBitNumber := cbCCBit.ItemIndex;
          end else begin
            mCurrentElement.IsConfigCode := false;
          end;// if mCurrentElement.IsConfigCode then begin

          xFlags := mCurrentElement.Flags;
          if cbProofed.Checked then
            Include(xFlags, efProofed)
          else
            Exclude(xFlags, efProofed);
          if cbObserve.Checked then
            Include(xFlags, efObserve)
          else
            Exclude(xFlags, efObserve);
          mCurrentElement.Flags := xFlags;

        end;// if not(IsMillmasterMap then begin
      end else
        Clear;

      // Yarn Master Name auch f�r deaktivierte Elemente �bernehmen
      YMElementNames.CommaText := edYMElementName.Text;
      // Beschreibung immer �bernehmen
      Documentation := meDocu.Text;
      // Daten in den XML-Node schreiben lassen
      Update;
    end; // with

    // Modified des Schemas damit vor dem Beenden nach dem Speichern gefragt werden kann
    if mCanUndo then
      mSchemaModified := true;

    mCanUndo := False;
  end;
end; // TfrmMain.WriteValuesToElement

//:---------------------------------------------------------------------------
procedure TfrmMain.WriteValuesToLoepfeBody;
begin
  if Assigned(mLoepfeBody) then begin
    mLoepfeBody.StructSize  := edStructSize.Value;
    mLoepfeBody.Description := meDescription.Text;
    mLoepfeBody.History     := meHistory.Text;
    // nun noch die �nderungen im XML verewigen
    mLoepfeBody.Update;
  end;
end; // TfrmMain.WriteValuesToLoepfeBody

procedure TfrmMain.acShowLastExportetMapfileExecute(Sender: TObject);
begin
  CreateXMLViewer;
  mMapFileViewer.ShowNewMapfile(mLastExportedMapfile, mLastExportedMapfileName);

  mMapFileViewer.Show;
end;// procedure TfrmMain.acShowLastExportetMapfileExecute(Sender: TObject);

procedure TfrmMain.acAdvancedFilterExecute(Sender: TObject);
begin
  if Sender is TAction then
    TAction(Sender).Checked := not(TAction(Sender).Checked);

  mCurrentElement := nil;
  if TAction(Sender).Checked then begin
    mFilterPanel.Height := 173;
    mAdvancedFilterPanel.Visible := true;
  end else begin
    mFilterPanel.Height := 22;
    mAdvancedFilterPanel.Visible := false;
  end;
  BuildTree(true);
end;

procedure TfrmMain.mPatternListClick(Sender: TObject);
begin
  BuildTree(true);
end;

procedure TfrmMain.acDeleteFilterExecute(Sender: TObject);
begin
  edTreeFilter.Text := '';
  BuildTree(true);
end;

procedure TfrmMain.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
  if mSchemaModified then begin
    case MessageDlg('Das Schema wurde ver�ndert.'+#13+#10+'Soll gespeichert werden?', mtWarning, [mbYes, mbNo, mbCancel], 0) of
      // Speichern
      mrYes: acSave.Execute;
      // Keine weitere Aktion (es wird gespeichert)
      mrNo: ;
      // Nicht speichern
      mrCancel: CanClose := false;
    end;
  end;// if mSchemaModified then begin

  if CanClose then begin
    if Assigned(mImporterForm) then
      mImporterForm.Close;
    FreeAndNil(mImporterForm);
    SaveSettings;
  end;// if CanClose then begin
end;

procedure TfrmMain.edMinInclExit(Sender: TObject);
begin
  CheckDefault;
end;// procedure TfrmMain.edMinInclExit(Sender: TObject);

(*---------------------------------------------------------
  Zeigt ein zus�tzliches Fenster mit kompremierten Informationen �ber ein Mapfile an
----------------------------------------------------------*)
procedure TfrmMain.acShowOtherMapExecute(Sender: TObject);
begin
  with TfrmMapElementInfo.Create(Application) do try
    LoepfeBody := mLoepfeBody;
    ShowMapInfo(mMapElementChange);
    if assigned(mCurrentElement) then
      ChangeElement(mCurrentElement);
  except
    on e: Exception do
      ShowMessage('Das Formular kann nicht angezeigt werden: ' + e.Message);
  end;
end;

(*---------------------------------------------------------
  L�dt die Settings und stellt den vorherigen Zustand wieder her
----------------------------------------------------------*)
procedure TfrmMain.FormShow(Sender: TObject);
begin
  LoadSettings;
end;// procedure TfrmMain.FormShow(Sender: TObject);

procedure TfrmMain.LoadDocument(aFileName: string; aReadOnly: boolean);
begin
  if FileExists(aFileName) then begin
    // ein vorher schon ge�ffnete Datei erst "entsperren" = lck Datei l�schen
    UnlockFile;
    mSchemaFileName := Trim(aFileName);
    OpenSchema(aReadOnly);
    CheckControlState;
  end;// if FileExists(aFileName) then begin
end;

procedure TfrmMain.SelectElement(aElementPath: string);
var
  xFound: Boolean;
  xActNode: TTreeNode;
begin
  if aElementPath > '' then begin
    // Vom ersten Knoten nach unten
    xActNode := tvSchema.Items.GetFirstNode;
    xFound := false;
    while (assigned(xActNode)) and (not(xFound)) do begin
      // aktuellen Knoten selektiren, wenn auf das selbe MMElement wie vorher verwiesen wird
      if GetNodePath(xActNode) = aElementPath then begin
        tvSchema.Selected := xActNode;
        tvSchema.Selected.MakeVisible;
        tvSchema.TopItem  := xActNode;
        xFound := true;
      end;//
      // W�hlt den n�chsten Knoten (unabh�ng von der aktuellen Ebene)
      xActNode := xActNode.GetNext;
    end;// while (assigned(xActNode)) and (not(xFound)) do begin
  end;// if aElementPath > '' then begin
end;

(*---------------------------------------------------------
  Stellt die Settings wieder her
----------------------------------------------------------*)
procedure TfrmMain.LoadSettings;
var
  xMapCategoryIndex: integer;
  xReadOnly: Boolean;
  xMapID: string;

  procedure RestoreMapInfo;
  var
    i: Integer;
    xNodeList: IXMLDOMNodeList;
    xForm: TfrmMapElementInfo;
  begin
    xNodeList := TSchematoolSettings.Instance.DOM.documentElement.selectNodes(Format('Form[starts-with(@Name,''%s'')]', [cMapElementInfo]));
    for i := 0 to xNodeList.Length - 1 do begin
      xForm := TfrmMapElementInfo.Create(Application);
      with xForm do try
        LoepfeBody := mLoepfeBody;
        ShowMapInfo(mMapElementChange);
        TSchematoolSettings.Instance.LoadForm(xForm, AttributeToStringDef(cAtrFormName, xNodeList.Item[i], ''));
      except
        on e: Exception do
          ShowMessage('Das Formular kann nicht angezeigt werden: ' + e.Message);
      end;// with TfrmMapElementInfo.Create(Application) do try
    end;// for i := 0 to xNodeList.Length - 1 do begin
  end;// procedure LoadMapInfo;
begin
  with TSchematoolSettings.Instance do begin
    Instance.LoadForm(self, self.name);
    xReadOnly := AttributeToBoolDef(cAtrReadOnly, RecentFilesMRU.selectSingleNode(cRecentFilesElement), false);
    LoadDocument(AttributeToStringDef(cAtrRecentFilesName, RecentFilesMRU.selectSingleNode(cRecentFilesElement), ''), xReadOnly);

    xMapID := AttributeToStringDef(cRecentMapfileName, GeneralRoot, '');
    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xMapID);
    lbMapIDClick(nil);
    SelectElement(AttributeToStringDef(cSelectedElementName, GeneralRoot, ''));

    RestoreMapInfo;
    mMapElementChange.ChangeElement(mCurrentElement);

    // Filter und Kategorie f�r die Mapfiles
    xMapCategoryIndex := lbMapCategories.Items.IndexOf(AttributeToStringDef(cAtrMapIDCategory, GeneralRoot, ''));
    if xMapCategoryIndex >= 0 then
      lbMapCategories.ItemIndex := xMapCategoryIndex;
    rbMapIDFilter.ItemIndex := AttributeToIntDef(cAtrMapIDFilter, GeneralRoot, 0);

    // XML Viewer
    if AttributeToBoolDef(cAtrXMLViewerVisible, GeneralRoot, false) then begin
      CreateXMLViewer;
      mMapFileViewer.Show;
    end;

    // Vergleichsliste
    if AttributeToBoolDef(cAtrCompareMapfileVisible, GeneralRoot, false) then
      acCompareMapfiles.Execute;
  end;// with TSchematoolSettings.Instance do begin
end;// procedure TfrmMain.LoadSettings;

(*---------------------------------------------------------
  Speichert die aktuellen Settings
----------------------------------------------------------*)
procedure TfrmMain.SaveSettings;
var
  i: Integer;
begin
  with TSchematoolSettings.Instance do begin
    // XML Viewer
    if Assigned(mMapFileViewer) then begin
      GeneralRoot.setAttribute(cAtrXMLViewerVisible, mMapFileViewer.Visible);
      mMapFileViewer.Close;
    end;

    // Vergleichsliste
    if Assigned(mCompareMapfile) then
      GeneralRoot.setAttribute(cAtrCompareMapfileVisible, mCompareMapfile.Visible);

    // Filter und Kategorie f�r die Mapfiles
    GeneralRoot.setAttribute(cAtrMapIDFilter, rbMapIDFilter.ItemIndex);
    GeneralRoot.setAttribute(cAtrMapIDCategory, lbMapCategories.Items[lbMapCategories.ItemIndex]);

    // Alle Positionsdaten l�schen
    DeleteAllMapForms;
    // Alle MapInfo Formulare schreiben
    for i := 0 to Application.ComponentCount - 1 do begin
      if Application.Components[i] is TfrmMapElementInfo then
        SaveForm(TfrmMapElementInfo(Application.Components[i]), Format(cMapElementInfo + ' (%d)', [i]));
    end;// for i := 0 to Application.ComponentCount - 1 do begin
    // Speichert Das MainWindow
    SaveForm(self, self.name);
    if assigned(mLoepfeBody) then begin
      GeneralRoot.setAttribute(cRecentMapfileName, mLoepfeBody.MapID);
      GeneralRoot.setAttribute(cSelectedElementName, GetNodePath(tvSchema.Selected));
    end;// if assigned(mLoepfeBody) then begin
  end;// with TSchematoolSettings.Instance do begin
end;// procedure TfrmMain.SaveSettings;

(* -----------------------------------------------------------
  Handler f�r das Popupmenu mit den letzten geladenen Files
-------------------------------------------------------------- *)
procedure TfrmMain.LoadDocumentHandler(Sender: TObject);
begin
  LoadDocument(copyRight((Sender as TMenuItem).Caption, ' '), false);
end;// procedure TfrmMainWindow.LoadDocumentHandler(Sender: TObject);

(*---------------------------------------------------------
  Recent Files
----------------------------------------------------------*)
procedure TfrmMain.pmOpenFileMRUPopup(Sender: TObject);
var
  i: Integer;
  xFileList: IXMLDOMNodeList;
  xNewMenuItem: TMenuItem;
begin
  if Sender is TPopupMenu then begin
    TPopupMenu(Sender).Items.Clear;
    xFileList := TSchematoolSettings.Instance.RecentFilesMRU.selectNodes(cRecentFilesElement);
    for i := 0 to xFileList.length - 1 do begin
      xNewMenuItem := TMenuItem.Create(TPopupMenu(Sender));
      xNewMenuItem.Caption := AttributeToStringDef(cAtrRecentFilesName, xFileList.Item[i], '');
      xNewMenuItem.OnClick := LoadDocumentHandler;

      if Caption <> '' then begin
        xNewMenuItem.Caption := '&' + IntToStr(i + 1) + ' ' + xNewMenuItem.Caption;
        TPopupMenu(Sender).Items.Add(xNewMenuItem)
      end else
        FreeAndNil(xNewMenuItem);
    end;// for i := 0 to xFileList.length - 1 do begin
  end;// if Sender is TPopupMenu then begin
end;

(*---------------------------------------------------------
  aktuelles Schema neu laden (ZB nach einer �nderung in XMLSpy
----------------------------------------------------------*)
procedure TfrmMain.acReloadCurrentSchemaExecute(Sender: TObject);
var
  xCurrentElement: string;
  xCurrentMap: string;
begin
  if assigned(mLoepfeBody) then begin
    xCurrentMap := mLoepfeBody.MapID;
    xCurrentElement := GetNodePath(tvSchema.Selected);
    LoadDocument(mSchemaFileName, not(mCanSave));  

    lbMapID.ItemIndex := lbMapID.Items.IndexOf(xCurrentMap);
    lbMapIDClick(nil);
    SelectElement(xCurrentElement);

    mMapElementChange.ChangeElement(mCurrentElement);
  end;// if assigned(mLoepfeBody) then begin
end;

procedure TfrmMain.acExportYMExecute(Sender: TObject);
var
  xMapName: string;
  xFileName: String;
  xDummy: boolean;
begin
  xFileName := '';
  xMapName  := '';
  // Header Informationen sichern
  WriteValuesToLoepfeBody;
  // Aktuelles Element sichern
  WriteValuesToElement;
  with TMapFileExporter.Create do
  try
    LoepfeBody := mLoepfeBody;
    MapFileList := TfrmExportMapFile.GetMapfileList(mLoepfeBody, xMapName, xDummy, xDummy, emYM);

    FileName  := GetMapDataDir + xMapName + cNormalFileExtension;

    if MapfileList > '' then begin
      if WriteYMMap then begin
        // DOM und Name des Mapfiles merken zum sp�teren anzeigen
        mLastExportedMapfile := DOM.xml;
        mLastExportedMapfileName := xMapName;

      end else begin
        ShowMessage('Misslungen');
      end;// if WriteMMMap then begin
    end;// if MapfileList > '' then begin

    acShowLastExportetMapfile.Execute;
  finally
    Free;
  end;// with TMapFileExporter.Create do begin
end;

procedure TfrmMain.CreateXMLViewer;
begin
  if not(assigned(mMapFileViewer)) then begin
    mMapFileViewer := TfrmXMLViewer.Create(Application);
    mMapFileViewer.Settings := TSchematoolSettings.Instance;
  end;// if not(assigned(mMapFileViewer)) then begin
end;// procedure TfrmMain.CreateXMLViewer;

(*---------------------------------------------------------
  Zeigt den Dialog an in dem das vergleichen mehrerer Mapfiles erm�glicht wird
----------------------------------------------------------*)
procedure TfrmMain.acCompareMapfilesExecute(Sender: TObject);
begin
  with TfrmCompareMapfile.Create(Application) do begin
    Settings := TSchematoolSettings.Instance;
    LoepfeBody := mLoepfeBody;
    ShowCompareMapfiles(mMapElementChange);
    // Gleich einen Wechsel anzeigen
    if assigned(mCurrentElement) then
      ChangeElement(mCurrentElement);
  end;// with TfrmCompareMapfile.Create(Application) do begin
end;// procedure TfrmMain.acCompareMapfilesExecute(Sender: TObject);

(*---------------------------------------------------------
  Listet die Elemente auf die einem Config Code entsprechen
----------------------------------------------------------*)
procedure TfrmMain.acFindCCExecute(Sender: TObject);
begin
  if not(assigned(mFindCC)) then begin
    mFindCC := TfrmFindConfigCode.Create(self);
    mFindCC.Settings := TSchematoolSettings.Instance;
  end;// if not(assigned(mFindCC)) then begin
  mFindCC.ShowFindCC(mLoepfeBody, tvSchema);
end;// procedure TfrmMain.acFindCCExecute(Sender: TObject);

procedure TfrmMain.meEnumPoolValuesExit(Sender: TObject);
begin
  if cobEnumPool.ItemIndex >= 0 then
    mLoepfeBody.Enumerations.Values[cobEnumPool.Text] := meEnumPoolValues.Lines.CommaText;
end;

procedure TfrmMain.edOffsetEnter(Sender: TObject);
begin
  if Sender is TEdit then
    TEdit(Sender).SelectAll;
end;

procedure TfrmMain.acFocusToFilterExecute(Sender: TObject);
begin
  edTreeFilter.SetFocus;
end;

procedure TfrmMain.acOpenMapExplorerExecute(Sender: TObject);
begin
  try
    Screen.Cursor := crHourglass;
    shellexecute(Application.handle,'explore',PChar(GetMapDataDir), nil, nil, SW_SHOWNORMAL);
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.



