unit ExportMapFile;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, XMLMapping,
  mwComboControl, StdCtrls, mmEdit, Buttons, mmBitBtn, Grids, mmStringGrid,
  ExtCtrls, mmLabel, VirtualTrees, mmCheckBox, mmRadioGroup, AdoDBAccess,
  mmSpeedButton, ActnList, mmActionList, mmVirtualStringTree, mmPanel,
  mmComboBox;

type
  TExportMode = (emNone,
                 emMM,
                 emYM);

  TMapFileEditLink = class(TPropertyEditLink, IVTEditLink)
  private
    FSelectedMapfile: string;
    mMapfiles: TStringList;
    mSelected: string;
  public
    constructor Create(aControlType: TPropDataType; aLoepfeBody: TLoepfeBody;
        aSelectedText: string); reintroduce; virtual;
    destructor Destroy; override;
    function EndEdit: Boolean; override; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex): Boolean; override; stdcall;
    property SelectedMapfile: string read FSelectedMapfile write FSelectedMapfile;
  end;

  TfrmExportMapFile = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    butCancel: TBitBtn;
    butOk: TBitBtn;
    vtExistingMaps: TVirtualStringTree;
    mmActionList1: TmmActionList;
    acToggleSaveInDB: TAction;
    pa1: TmmPanel;
    mmLabel3: TmmLabel;
    rbNamePrefix: TmmRadioGroup;
    edMapName: TmmEdit;
    cbSaveToDB: TmmCheckBox;
    cbFileCompressed: TmmCheckBox;
    vstExportMap: TmmVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure vtExistingMapsGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure FormDestroy(Sender: TObject);
    procedure vtExistingMapsChange(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure rbNamePrefixClick(Sender: TObject);
    procedure cbSaveToDBClick(Sender: TObject);
    procedure edMapNameChange(Sender: TObject);
    procedure acToggleSaveInDBExecute(Sender: TObject);
    procedure vstExportMapGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure vstExportMapClick(Sender: TObject);
    procedure vstExportMapChange(Sender: TBaseVirtualTree;
      Node: PVirtualNode);
    procedure vstExportMapCreateEditor(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; out EditLink: IVTEditLink);
    procedure vstExportMapEditing(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
    procedure vstExportMapEdited(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex);
    procedure FormShow(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
  private
    FLoepfeBody: TLoepfeBody;
    mAdoDBAccess: TADODBAccess;
    mCurrentNumber: Integer;
    mCurrentPrefix: string;
    mEditLink: TMapFileEditLink;
    mSelectedMapfiles: TStringList;
    procedure SetLoepfeBody(const Value: TLoepfeBody);
    procedure SyncMapfileName(aMapfile: string);
  public
    class function GetMapfileList(aLoepfeBody: TLoepfeBody; var aMapName: string; var aSaveToDB, aCompressFile: boolean;
        aExportMode: TExportMode): string;
    property LoepfeBody: TLoepfeBody read FLoepfeBody write SetLoepfeBody;
  end;

const
  cTypeCol    = 0;
  cMapfileCol = 1;

var
  frmExportMapFile: TfrmExportMapFile;

implementation

uses
  XMLMappingDef, SchematoolGlobal, LoepfeGlobal, JclIniFiles;

{$R *.DFM}

class function TfrmExportMapFile.GetMapfileList(aLoepfeBody: TLoepfeBody; var aMapName: string; var aSaveToDB,
    aCompressFile: boolean; aExportMode: TExportMode): string;
var
  i: TMapSection;
begin
  result := '';
  with TfrmExportMapFile.Create(nil) do
  try
    LoepfeBody := aLoepfeBody;

    case aExportMode of
      emYM: begin
        cbSaveToDB.Enabled := false;
        cbFileCompressed.Enabled := false;
        rbNamePrefix.Enabled := false;
      end;// emYM: begin
    end;// case aExportMode of

    if showModal = mrOK then begin
      aMapName      := edMapName.Text;
      aSaveToDB     := cbSaveToDB.Checked;
      aCompressFile := cbFileCompressed.Checked;

      result := '';
      for i := Low(TMapSection) to High(TMapSection) do begin
        if mSelectedMapfiles[ord(i)] > '' then
          result := result + Format(',"%s=%s"', [cMapfileSections[i].Name, mSelectedMapfiles[ord(i)]]);
      end;// for i := Low(TMapSection) to High(TMapSection) do begin
    end;// if showModal = mrOK then begin

    //  am Anfang �berz�hliges Komma l�schen.
    if result > '' then
      system.delete(result , 1, 1);
  finally
    free;
  end;// with TfrmExportMapFile.Create(nil) do try
end;

procedure TfrmExportMapFile.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  vstExportMap.RootNodeCount := ord(High(TMapSection)) + 1;
  mSelectedMapfiles := TStringList.Create;
  for i := 0 to vstExportMap.RootNodeCount - 1 do
    mSelectedMapfiles.Add('');

  rbNamePrefixClick(nil);
  
  // Vorhandene Mapfiles auslesen
  mAdoDBAccess := TAdoDBAccess.Create(1);
  try
    with mAdoDBAccess do begin
      if Init then begin
        Caption := Format('%s (%s)', [HostName, DBName]);
        Query[0].SQL.Text := 'SELECT c_mapfile_id, c_mapfile_name FROM t_xml_mapfile ORDER BY c_mapfile_name';
        Query[0].Open;
        vtExistingMaps.RootNodeCount := Query[0].Recordset.RecordCount;
        cbSaveToDB.Enabled := true;
        vtExistingMaps.Enabled := true;
      end;
    end;// with mAdoDBAccess do begin
  except
  end;
end;

procedure TfrmExportMapFile.vtExistingMapsGetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xNodeIndex: cardinal;
begin
  xNodeIndex := vtExistingMaps.AbsoluteIndex(Node);
  if xNodeIndex < vtExistingMaps.RootNodeCount then begin
    mAdoDBAccess.Query[0].Recordset.AbsolutePosition := xNodeIndex + 1;
    case Column of
      0: CellText := mAdoDBAccess.Query[0].FieldByName('c_mapfile_id').AsString;
      1: CellText := mAdoDBAccess.Query[0].FieldByName('c_mapfile_name').AsString;
    else
      CellText := '';
    end;
  end;
end;

procedure TfrmExportMapFile.SetLoepfeBody(const Value: TLoepfeBody);
begin
  FLoepfeBody := Value;
end;

procedure TfrmExportMapFile.FormDestroy(Sender: TObject);
begin
  FreeAndNil(mSelectedMapfiles);
  FreeAndNil(mAdoDBAccess);
end;

procedure TfrmExportMapFile.vtExistingMapsChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
var
  xFound: Boolean;
  i: Integer;
  xNodeIndex: cardinal;
  xMapName: string;
begin
  xNodeIndex := vtExistingMaps.AbsoluteIndex(Node);
  if xNodeIndex < vtExistingMaps.RootNodeCount then begin
    mAdoDBAccess.Query[0].Recordset.AbsolutePosition := xNodeIndex + 1;
    xMapName := mAdoDBAccess.Query[0].FieldByName('c_mapfile_name').AsString;

    xFound := false;
    i := Low(cPrefix);
    while (i < High(cPrefix)) and (not(xFound)) do begin
      if AnsiSameText(cPrefix[i], copy(xMapName, 1, Length(cPrefix[i]))) then
        xFound := true
      else
        inc(i);
    end;// while (i < High(cPrefix)) and (not(xFound)) do begin
    if i < rbNamePrefix.Items.Count then
      rbNamePrefix.ItemIndex := i;
    rbNamePrefixClick(nil);
    edMapName.Text := xMapName;
  end;
end;

procedure TfrmExportMapFile.rbNamePrefixClick(Sender: TObject);
begin
(*  mCurrentNumber := 1 + IniReadInteger(ChangeFileExt(Application.Exename, '.ini'), cMapfileNumbers, rbNamePrefix.Items[rbNamePrefix.ItemIndex]);*)
  mCurrentPrefix := cPrefix[rbNamePrefix.ItemIndex];
  edMapName.Text := Format('%s-%.4d', [mCurrentPrefix, mCurrentNumber]);
end;

procedure TfrmExportMapFile.cbSaveToDBClick(Sender: TObject);
begin
  cbFileCompressed.Enabled := not cbSaveToDB.Checked;
end;

procedure TfrmExportMapFile.edMapNameChange(Sender: TObject);
var
  xCode: integer;
  xCurrentNumber: integer;
begin
  val(copyRight(edMapName.Text, '-'), xCurrentNumber, xCode);
  if xCode = 0 then
    mCurrentNumber := xCurrentNumber;
end;

procedure TfrmExportMapFile.SyncMapfileName(aMapfile: string);
var
  xCode: integer;
  xCurrentNumber: integer;
begin
  if AnsiSameText(copy(aMapfile, 1, 2), 'ZE') then
    rbNamePrefix.ItemIndex := 0
  else if AnsiSameText(copy(aMapfile, 1, 3), 'WSC') then
    rbNamePrefix.ItemIndex := 1
  else if AnsiSameText(copy(aMapfile, 1, 3), 'LZE') then
    rbNamePrefix.ItemIndex := 2
  else if AnsiSameText(copy(aMapfile, 1, 3), 'TST') then
    rbNamePrefix.ItemIndex := 3;

  val(copyLeft(copyRight(aMapfile, '-'), '-'), xCurrentNumber, xCode);
  if xCode = 0 then
    mCurrentNumber := xCurrentNumber;
  rbNamePrefixClick(rbNamePrefix);
end;

procedure TfrmExportMapFile.acToggleSaveInDBExecute(Sender: TObject);
begin
  cbSaveToDB.Checked := not(cbSaveToDB.Checked);
end;

procedure TfrmExportMapFile.vstExportMapGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
begin
  case Column of
    cTypeCol:    CellText := cMapfileSections[TMapSection(Sender.AbsoluteIndex(Node))].DisplayName;
    cMapfileCol: CellText := mSelectedMapfiles[Sender.AbsoluteIndex(Node)];
  end;// case Column of
end;

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TMapFileEditLink.Create(aControlType: TPropDataType; aLoepfeBody: TLoepfeBody; aSelectedText: string);
var
  i: Integer;
begin
  mSelected := aSelectedText;
  mMapfiles := TStringList.Create;
  mMapfiles.Sorted := true;
  for i := 0 to aLoepfeBody.MapIDs.Count - 1 do
    mMapfiles.Add(aLoepfeBody.MapIDs.Names[i]);

  mMapfiles.Add('');
  inherited Create(aControlType, mMapfiles.Text);
end;// constructor TMapFileEditLink.Create(aControlType: TPropDataType; aLoepfeBody: TLoepfeBody; aSelectedText: string);

destructor TMapFileEditLink.Destroy;
begin
  FreeAndNil(mMapfiles);
  inherited;
end;

(*---------------------------------------------------------
  Wird aufgerufen, wenn ein Knoten angeklickt wurde
----------------------------------------------------------*)
function TMapFileEditLink.EndEdit: Boolean;
begin
  result := inherited EndEdit;
  SelectedMapfile := '';
  if not(VarIsNull(UserSelection) or VarIsEmpty(UserSelection)) then begin
    if UserSelection >= 0 then
      SelectedMapfile := mMapfiles[UserSelection];
  end;// if not(VarIsNull(UserSelection) or VarIsEmpty(UserSelection)) then begin
end;// function TMapFileEditLink.EndEdit: Boolean;

(*---------------------------------------------------------
  Wird aufgerufen, bevor ein Control angezeigt werden soll
----------------------------------------------------------*)
function TMapFileEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node:
    PVirtualNode; Column: TColumnIndex): Boolean;
begin
  result := inherited PrepareEdit(Tree, Node, Column);

  if mLink is TCombobox then begin
    TCombobox(mLink).ItemIndex := mMapfiles.IndexOf(mSelected);
    if mMapfiles.Count < 20 then
      TCombobox(mLink).DropDownCount := mMapfiles.Count
    else
      TCombobox(mLink).DropDownCount := 20;
  end;
end;// function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;

procedure TfrmExportMapFile.vstExportMapClick(Sender: TObject);
var
  xHitInfo: THitInfo;
  xMousePoint: TPoint;
begin
  xMousePoint := vstExportMap.ScreenToClient(Mouse.CursorPos);
  vstExportMap.GetHitTestInfoAt(xMousePoint.x, xMousePoint.y, true, xHitInfo);
  if assigned(xHitInfo.HitNode) then
    vstExportMap.EditNode(xHitInfo.HitNode, cMapfileCol);
end;

procedure TfrmExportMapFile.vstExportMapChange(Sender: TBaseVirtualTree;
  Node: PVirtualNode);
begin
  if assigned(Node) then
    Sender.EditNode(Node, cMapfileCol);
end;

procedure TfrmExportMapFile.vstExportMapCreateEditor(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  out EditLink: IVTEditLink);
begin
   if Column = cMapfileCol then
    mEditLink := TMapFileEditLink.Create(dtEnum, LoepfeBody, mSelectedMapfiles[Sender.AbsoluteIndex(Node)]);
  EditLink := mEditLink;
end;

procedure TfrmExportMapFile.vstExportMapEditing(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; var Allowed: Boolean);
begin
  Allowed := (Column = cMapfileCol);
end;

procedure TfrmExportMapFile.vstExportMapEdited(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex);
begin
  mSelectedMapfiles[Sender.AbsoluteIndex(Node)] := mEditLink.SelectedMapfile; 
  mEditLink := nil;
end;

procedure TfrmExportMapFile.FormShow(Sender: TObject);
var
  xNumberpos: integer;
  xNumber: string;
  xPrefix: string;
  i: TMapSection;
  xMapName: string;
  xMainNr: integer;
  xReleaseNr: integer;
  xFound: Boolean;
begin
  vstExportMap.FocusedNode := vstExportMap.GetFirst;
  TSchematoolSettings.Instance.LoadForm(self, self.name);
  xNumberpos := AnsiPos('-', LoepfeBody.MapID);
  xNumber := copy(LoepfeBody.MapID, xNumberpos + 1, 4);
  xPrefix := copy(LoepfeBody.MapID, 1, xNumberPos - 1);

  if Length(xNumber) = 4 then begin

    for i := Low(TMapSection) to High(TMapSection) do begin
      xMainNr    := StrToIntDef(copy(xNumber, 1, 2), 0);
      xReleaseNr := StrToIntDef(copy(xNumber, 3, 2), 0);

      // Wenn der aktuelle Mapname nicht der ist, der �bergeben wurde, dann immer die h�chste Revision w�hlen
      xMapName := GetMapname(xPrefix, xMainNr, xReleaseNr, cMapfileSections[i].Abbreviation);
      if xMapname <> LoepfeBody.MapID then
        xReleaseNr := 99;

      xFound := false;

      repeat
        xMapName := GetMapname(xPrefix, xMainNr, xReleaseNr, cMapfileSections[i].Abbreviation);
        if LoepfeBody.MapIDs.IndexOfName(xMapName) >= 0 then begin
          mSelectedMapfiles[ord(i)] := xMapName;
          xFound := true;
        end else begin // if LoepfeBody.MapIDs.IndexOfName(xMapName) >= 0 then begin
          // Wenn das entsprechende Mapfile nicht gefunden wurde, dann erst mal den Release runterz�hlen
          if xReleaseNr > 0 then begin
            dec(xReleaseNr);
          end else begin
            // Wenn der Release bei 0 ist, dann die Main Version runterz�hlen
            dec(xMainNr);
            xReleaseNr := 99;
          end;// if xReleaseNr > 0 then begin
        end;// if LoepfeBody.MapIDs.IndexOfName(xMapName) >= 0 then begin
      until xFound or (xMainNr < 0);
    end;// for i := Low(TMapSection) to High(TMapSection) do begin

  end;// if Length(xNumber) = 4 then begin
  SyncMapfileName(LoepfeBody.MapID);
end;

procedure TfrmExportMapFile.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  TSchematoolSettings.Instance.SaveForm(self, self.name);
  // Den letzten editierten Wert �bernehmen
  if assigned(mEditLink) then
    vstExportMap.EndEditNode;
end;

end.
