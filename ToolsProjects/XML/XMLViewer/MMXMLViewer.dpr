program MMXMLViewer;

uses
  Forms,
  XMLViewer in '..\..\..\TestAndToolsCommon\XMLViewer.pas' {frmXMLViewer},
  XMLViewerSettings in '..\..\..\TestAndToolsCommon\XMLViewerSettings.pas',
  LoadFromDB in '..\..\..\TestAndToolsCommon\LoadFromDB.pas' {frmLoadFromDB};

{$R *.RES}

var
  frmXMLViewer: TfrmXMLViewer;

begin
  Application.Initialize;
  Application.CreateForm(TfrmXMLViewer, frmXMLViewer);
  frmXMLViewer.Settings := TXMLViewerSettings.Instance;
  Application.Run;
end.
