Funktion:
	Nimmt das LEF.xml auseinander und stellt die Strukturen als eigenständige XML Dokumente bereit. 
	Ausserdem werden die Offsets der einzelnen Elemente berechnet. 
	Diese XML Dateien können als Input im Schematool verwendet werden um Änderungen zu verfolgen.

Anwendung: 
	Aufruf mit folgenden Parametern
	- Dateiname der LEF XML Datei
	- Name der Struktur aus dem LEF XML
	Ausgabedatei ist die Datei mit dem Namen der LEF Datei + Name des Struct
