program LEFXMLParser;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  filectrl,
  MSXML2_TLB,
  XMLGlobal,
  dialogs,
  activex,
  windows,
  mmCS;

var
  gResultDOM: DomDocumentMM;

const
  cAttrPos = 'Pos';
  cAttrLefDataType = 'Type';

procedure IncCurrentPosFromDataType(var aCurrentPos: Integer; aLefNode: IXMLDOMNode; aBaseOffset: integer);
var
  xDataType: string;
  xDataTypeWidth: integer;
begin
  xDataTypeWidth := 0;

  xDataType := AttributeToStringDef(cAttrLefDataType, aLefNode, '');
  if AnsiSameText(xDataType, 'char') then
    xDataTypeWidth := 1
  else if AnsiSameText(xDataType, 'widechar') then
    xDataTypeWidth := 2
  else if AnsiSameText(xDataType, 'byte') then
    xDataTypeWidth := 1
  else if AnsiSameText(xDataType, 'shortint') then
    xDataTypeWidth := 1
  else if AnsiSameText(xDataType, 'word') then
    xDataTypeWidth := 2
  else if AnsiSameText(xDataType, 'smallint') then
    xDataTypeWidth := 2
  else if AnsiSameText(xDataType, 'dword') then
    xDataTypeWidth := 4
  else if AnsiSameText(xDataType, 'longint') then
    xDataTypeWidth := 4
  else if AnsiSameText(xDataType, 'single') then
    xDataTypeWidth := 4
  else if AnsiSameText(xDataType, 'double') then
    xDataTypeWidth := 8
  else if AnsiSameText(xDataType, 'longword') then
    xDataTypeWidth := 4
  else
    MessageDlg(Format('Datentyp %s nicht bekannt', [xDataType]), mtWarning, [mbOK], 0);

  aCurrentPos := aCurrentPos + (xDataTypeWidth * (AttributeToIntDef('High', aLefNode, 0) + 1));
end;// procedure IncCurrentPosFromDataType(var aCurrentPos: Integer; aLefNode: IXMLDOMNode; aBaseOffset: integer);

function GetPosOfCurrentElement(aLefElement: IXMLDOMNode; var aCurrentPos: integer; aBasePos: integer): integer;
var
  xOffset: Integer;
begin
  xOffset := aBasePos + AttributeToIntDef('Offset', aLefElement, 0);
  if xOffset > aCurrentPos then
    aCurrentPos := xOffset;
  result := aCurrentPos;
end;// function GetPosOfCurrentElement(aLefElement: IXMLDOMNode; var aCurrentPos: integer; aBasePos: integer): integer;

procedure ProcessStruct(aLefNode: IXMLDOMNode; aParentElement: IXMLDOMElement; var aCurrentPos: integer);
var
  xLEFElement: IXMLDOMElement;
  i: Integer;
  xNewMMElement: IXMLDOMElement;
  j: Integer;
begin
  if assigned(aLEFNode) then begin
    if Supports(aLEFNode, IXMLDOMElement, xLEFElement) then begin
      for i := 0 to xLEFElement.childNodes.length - 1 do begin
        xNewMMElement := gResultDOM.CreateElement(xLEFElement.childNodes[i].nodename);
        xNewMMElement.setAttribute(cAttrPos, GetPosOfCurrentElement(xLEFElement.childNodes[i], aCurrentPos, AttributeToIntDef(cAttrPos, aParentElement, 0)));
        for j := 0 to xLEFElement.childNodes[i].attributes.length - 1 do
          xNewMMElement.setAttribute(xLEFElement.childNodes[i].attributes[j].nodeName, xLEFElement.childNodes[i].attributes[j].nodeValue);
        if AttributExists(cAttrLefDataType, xLEFElement.childNodes[i].attributes) then begin
          IncCurrentPosFromDataType(aCurrentPos, xLEFElement.childNodes[i], AttributeToIntDef(cAttrPos, aParentElement, 0));
        end else begin
          if (xLEFElement.childNodes[i].childNodes.length = 0) then
            ProcessStruct(aLefNode.SelectSingleNode('/LoepfeBody/LoepfeStruct/' + xLEFElement.childNodes[i].nodename), xNewMMElement, aCurrentPos)
          else
            ProcessStruct(xLEFElement.childNodes[i], xNewMMElement, aCurrentPos);
        end;// if AttributExists(cAttrLefDataType, xLEFElement.childNodes[i].attributes) then begin
        aParentElement.appendChild(xNewMMElement);
      end;// if Supports(aLEFNode, IXMLDOMElement, xLEFElement) then begin
    end;// if Supports(IXMLDOMElement, aLEFNode, xLEFElement) then begin
  end;// if assigned(aLEFNode) then begin
end;// procedure ProcessStruct(aStructName: string; aParentElement: IXMLDOMElement; aCurrentPos: integer);

procedure Execute(aSection: string; aDOM: DomDocumentMM = nil);
var
  xDOM: DomDocumentMM;
  xStructNode: IXMLDOMNode;
  xCurrentStructName: string;
  xCurrentPos: integer;
begin
  gResultDOM := nil;

  if FileExists(ParamStr(1)) then begin
    if assigned(aDOM) then begin
      xDOM := aDOM;
    end else begin
      xDOM := DOMDocumentMMCreate;
      xDOM.load(ParamStr(1));
    end;// if assigned(aDOM) then begin

    xStructNode := xDOM.SelectSingleNode('/LoepfeBody/LoepfeObject/' + aSection);
    if assigned(xStructNode) then begin
      xCurrentStructName := AttributeToStringDef('DataStruct', xStructNode, '');
      if xCurrentStructName > '' then begin
        InitXMLDocument(gResultDOM, xCurrentStructName);
        gResultDOM.documentElement.setAttribute(cAttrPos, 0);
        xCurrentPos := 0;
        ProcessStruct(xDOM.selectSingleNode('/LoepfeBody/LoepfeStruct/' + xCurrentStructName), gResultDOM.DocumentElement, xCurrentPos);
        gResultDOM.save(ChangeFileExt(ParamStr(1), '_') + aSection + '.xml');
      end;// if xCurrentStructName > '' then begin
    end;// if assigned(xStructNode) then begin
  end;// if FileExists(ParamStr(1)) then begin
end;// procedure Execute;

procedure ExecuteAll;
var
  xDOM: DomDocumentMM;
  xNodeList: IXMLDOMNodeList;
  i: Integer;
  xNodeName: string;
begin
  if FileExists(ParamStr(1)) then begin
    xDOM := DOMDocumentMMCreate;
    xDOM.load(ParamStr(1));
    CodeSite.Enabled := True;
    codesite.sendstring('', FormatXML(xDom.xml));
    xNodeList := xDOM.SelectNodes('/LoepfeBody/LoepfeObject/*');
    for i := 0 to xNodeList.length - 1 do begin
      xNodeName := xNodeList[i].nodename;
      writeln('Processing ' + xNodeName + ' ...');
      Execute(xNodeName);
    end;// for i := 0 to xNodeList.length - 1 do begin
  end;// if FileExists(ParamStr(1)) then begin*)
end;// procedure Execute;


var
  xResult: HResult;

begin
  xResult := CoInitialize(nil);
  try
    // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
    // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
    if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
      raise Exception.Create('CoInitialize failed');
    if paramCount = 2 then
      Execute(ParamStr(2))
    else if paramCount = 1 then
      ExecuteAll
    else begin
      writeln('Anwendung: Aufruf mit folgenden Parametern');
      writeln('  - Dateiname der LEF XML Datei');
      writeln('  - Name der Struktur aus dem LEF XML');
      writeln('Ausgabedatei ist die Datei mit dem Namen der LEF Datei + Name des Struct');
    end;// if paramCount > 2 then begin

    gResultDOM := nil;

    writeln;
    writeln('Enter zum Beenden');
    readln;
  finally
    CoUninitialize;
  end;
end.

