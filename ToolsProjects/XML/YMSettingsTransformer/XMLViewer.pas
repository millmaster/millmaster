unit XMLViewer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, SynEdit,
  ActnList, ComCtrls, ImgList, mmImageList, mmActionList, ToolWin,
  mmToolBar, IvMlDlgs, mmOpenDialog, StdCtrls, mmStaticText, ExtCtrls,
  mmImage, mmSplitter, mmMemo, mmLabel, mmPanel, mmButton, mmCheckBox, XMLGlobal,
  Menus, mmPopupMenu, VirtualTrees, mmVirtualStringTree, MSXML2_TLB;

type
  TfrmXMLViewer = class(TForm)
    mmToolBar1: TmmToolBar;
    mmActionList1: TmmActionList;
    mmImageList1: TmmImageList;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    acLoadXML: TAction;
    acLoadMapFromDB: TAction;
    mXMLOpenDialog: TmmOpenDialog;
    paXP: TmmPanel;
    paXPHeader: TmmPanel;
    paXPRoot: TmmPanel;
    paXPResult: TmmPanel;
    paXPInput: TmmPanel;
    mMethodNamePanel: TmmPanel;
    mMinusImage: TmmImage;
    bExpand: TmmImage;
    mPlusImage: TmmImage;
    laMethodName: TmmStaticText;
    paXMLRoot: TmmPanel;
    mXPResultSplitter: TmmSplitter;
    mmPanel1: TmmPanel;
    mmPanel2: TmmPanel;
    mmLabel1: TmmLabel;
    memoXP: TmmMemo;
    cbExecuteOnChange: TmmCheckBox;
    mmButton1: TmmButton;
    acExecuteXP: TAction;
    mDescriptionPanel: TmmPanel;
    mmPanel8: TmmPanel;
    mInfoImage: TmmImage;
    mDescriptionMemo: TmmMemo;
    mmLabel2: TmmLabel;
    pmOpenFileMRU: TmmPopupMenu;
    paXMLResultLeft: TmmPanel;
    paXMLResultRight: TmmPanel;
    mmVirtualStringTree1: TmmVirtualStringTree;
    procedure FormCreate(Sender: TObject);
    procedure acLoadXMLExecute(Sender: TObject);
    procedure acLoadMapFromDBExecute(Sender: TObject);
    procedure bExpandClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure mXPResultSplitterMoved(Sender: TObject);
    procedure acExecuteXPExecute(Sender: TObject);
    procedure memoXPChange(Sender: TObject);
    procedure pmOpenFileMRUPopup(Sender: TObject);
    procedure mmVirtualStringTree1GetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
  private
    mMSDOM: DOMDocumentMM;
    FMapfile: string;
    memoMapFile: TSynEdit;
    memoXPResult: TSynEdit;
    mLastXPHeight: Integer;
    procedure SetMapfile(const Value: string);
    procedure LoadDocumentHandler(Sender: TObject);
    function XMLNodeFromVirtualNode(Sender: TBaseVirtualTree; aNode: PVirtualNode): IXMLDOMNode;
    { Private declarations }
  public
    procedure CangeMemoMapfile(Sender: TObject);
    procedure CollapseXP;
    procedure ExpandXP;
    procedure LoadXMLFile(aFileName: string);
    procedure ShowNewMapfile(aMapfile: string; aTitle: string);
    property Mapfile: string read FMapfile write SetMapfile;
    { Public declarations }
  end;

implementation

uses
  SynHighlighterXML, {ChooseMapFromDB,} SchematoolGlobal, mmcs, LoepfeGlobal,
  ConfigurationPersistence;

{$R *.DFM}

procedure TfrmXMLViewer.FormCreate(Sender: TObject);
var
  xHighlighter: TSynXMLSyn;
begin
  memoMapFile          := TSynEdit.Create(self);
  memoMapFile.Parent   := paXMLRoot;
  memoMapFile.Align    := alClient;
  memoMapFile.TabWidth := 2;
  memoMapFile.OnChange := CangeMemoMapfile;

  memoMapFile.Gutter.ShowLineNumbers := true;
  xHighlighter := TSynXMLSyn.Create(memoMapFile);

  xHighlighter.AttributeAttri.Foreground := clRed;
  xHighlighter.ElementAttri.Style        := [fsItalic];
  memoMapFile.Highlighter                := xHighlighter;

  memoXPResult             := TSynEdit.Create(self);
  memoXPResult.Parent      := paXMLResultLeft;
  memoXPResult.Align       := alClient;
  memoXPResult.TabWidth    := 2;
  memoXPResult.Highlighter := xHighlighter;

  CollapseXP;
end;

procedure TfrmXMLViewer.SetMapfile(const Value: string);
begin
  if FMapfile <> Value then
  begin
    FMapfile := Value;
    memoMapFile.Text := FormatXML(Value);
  end;
end;

procedure TfrmXMLViewer.ShowNewMapfile(aMapfile: string; aTitle: string);
begin
  Mapfile := aMapfile;
  Caption:= aTitle;
end;

procedure TfrmXMLViewer.acLoadXMLExecute(Sender: TObject);
begin
  with mXMLOpenDialog do begin
    InitialDir := ExtractFilePath(Application.ExeName) + '\Mapfiles';
    if Execute then
      LoadXMLFile(FileName);
  end;// with mXMLOpenDialog do begin
end;

procedure TfrmXMLViewer.LoadXMLFile(aFileName: string);
begin
  if FileExists(aFileName) then begin
    with TStringList.Create do try
      LoadFromFile(aFileName);
      TSchematoolSettings.Instance.SaveRecentFiles(aFileName, false, TSchematoolSettings.Instance.RecentFilesXML);
      Mapfile := Text;
    finally
      free;
    end;// with TStringList.Create do try
  end;// if FileExists(aFileName) then begin
end;

procedure TfrmXMLViewer.acLoadMapFromDBExecute(Sender: TObject);
begin
//  with TfrmChooseMapFromDB.Create(nil) do try
//    ShowModal;
//    if Mapfile > '' then
//      self.Mapfile := Mapfile;
//  finally
//    free;
//  end;// with TfrmChooseMapFromDB.Create(nil) do try
end;

procedure TfrmXMLViewer.bExpandClick(Sender: TObject);
begin
  if paXPRoot.visible then
    CollapseXP
  else
    ExpandXP;
end;

procedure TfrmXMLViewer.CollapseXP;
begin
  mLastXPHeight := paXP.Height;
  paXP.Height := paXPHeader.Height;
  bExpand.Picture := mPlusImage.Picture;
  paXPRoot.Visible := false;
  mXPResultSplitter.Visible := false;
end;

procedure TfrmXMLViewer.ExpandXP;
begin
  paXP.Height := mLastXPHeight;
  bExpand.Picture := mMinusImage.Picture;
  paXPRoot.Visible := true;
  mXPResultSplitter.Visible := true;
end;

const
  cLastXPHeight = 'LastXPHeight';
  cXPExpanded   = 'XPExpanded';
  cExecuteXPOnChange = 'ExecuteXPOnChange';

procedure TfrmXMLViewer.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
var
  xFormElement: IXMLDOMElement;
begin
  with TSchematoolSettings.Instance do begin
    xFormElement := SaveForm(self, self.Name);
    if assigned(xFormElement) then begin
      xFormElement.setAttribute(cLastXPHeight, mLastXPHeight);
      xFormElement.setAttribute(cXPExpanded, paXPRoot.visible);
      xFormElement.setAttribute(cExecuteXPOnChange, cbExecuteOnChange.Checked);
    end;
  end;//
end;

procedure TfrmXMLViewer.FormShow(Sender: TObject);
var
  xFormElement: IXMLDOMElement;
begin
  with TSchematoolSettings.Instance do begin
    xFormElement := LoadForm(self, self.Name);
    if assigned(xFormElement) then begin
      mLastXPHeight := AttributeToIntDef(cLastXPHeight, xFormElement, mLastXPHeight);
      paXP.Height := mLastXPHeight;
      cbExecuteOnChange.Checked := AttributeToBoolDef(cExecuteXPOnChange, xFormElement, false);

      if AttributeToBoolDef(cXPExpanded, xFormElement, false) then
        ExpandXP
      else
        CollapseXP;
    end;
  end;//
end;

procedure TfrmXMLViewer.mXPResultSplitterMoved(Sender: TObject);
begin
  mLastXPHeight := paXP.Height;
end;

procedure TfrmXMLViewer.acExecuteXPExecute(Sender: TObject);
begin
  if not(assigned(mMSDOM)) then
    mMSDOM := XMLStreamToDOM(memoMapFile.Text);

  try
    if memoXP.SelLength > 0 then
      memoXPResult.Text := FormatXML(mMSDOM.SelectNodes(StringReplace(memoXP.SelText, cCrlf, ' ', [rfReplaceAll])))
    else
      memoXPResult.Text := FormatXML(mMSDOM.SelectNodes(StringReplace(memoXP.Text, cCrlf, ' ', [rfReplaceAll])));
  except
    on e: Exception do begin
      memoXPResult.Text := e.message;
    end;// on e:EOleException do begin
  end;// try except

end;

procedure TfrmXMLViewer.CangeMemoMapfile(Sender: TObject);
begin
  mMSDOM := nil;
end;

procedure TfrmXMLViewer.memoXPChange(Sender: TObject);
begin
  if cbExecuteOnChange.Checked then
    acExecuteXP.Execute;
end;

(* -----------------------------------------------------------
  Handler f�r das Popupmenu mit den letzten geladenen Files
-------------------------------------------------------------- *)
procedure TfrmXMLViewer.LoadDocumentHandler(Sender: TObject);
begin
  LoadXMLFile(copyRight((Sender as TMenuItem).Caption, ' '));
end;// procedure TfrmMainWindow.LoadDocumentHandler(Sender: TObject);

(*---------------------------------------------------------
  Recent Files
----------------------------------------------------------*)
procedure TfrmXMLViewer.pmOpenFileMRUPopup(Sender: TObject);
var
  i: Integer;
  xFileList: IXMLDOMNodeList;
  xNewMenuItem: TMenuItem;
begin
  if Sender is TPopupMenu then begin
    TPopupMenu(Sender).Items.Clear;
    xFileList := TSchematoolSettings.Instance.RecentFilesXML.selectNodes(cRecentFilesElement);
    for i := 0 to xFileList.length - 1 do begin
      xNewMenuItem := TMenuItem.Create(TPopupMenu(Sender));
      xNewMenuItem.Caption := AttributeToStringDef(cAtrRecentFilesName, xFileList.Item[i], '');
      xNewMenuItem.OnClick := LoadDocumentHandler;

      if Caption <> '' then begin
        xNewMenuItem.Caption := '&' + IntToStr(i + 1) + ' ' + xNewMenuItem.Caption;
        TPopupMenu(Sender).Items.Add(xNewMenuItem)
      end else
        FreeAndNil(xNewMenuItem);
    end;// for i := 0 to xFileList.length - 1 do begin
  end;// if Sender is TPopupMenu then begin
end;// procedure TfrmXMLViewer.pmOpenFileMRUPopup(Sender: TObject);

(*---------------------------------------------------------
  Tree
----------------------------------------------------------*)
function TfrmXMLViewer.XMLNodeFromVirtualNode(Sender: TBaseVirtualTree; aNode: PVirtualNode): IXMLDOMNode;
var
  xCurrentNode: PVirtualNode;
  xCurentElementPath: string;
  xCurrentNodeIndex: integer;
begin
(*  xCurentElementPath := '';
  xCurrentNode := Node;
  while assigned(xCurrentNode) do begin
    xCurrentNodeIndex
  end;// while assigend(xCurrentNode) do begin*)

(*  // Stellt den XPath zum entsprechenden Node zusammen
  xCurentElementPath := '';
  xCurrentNode := Node;
  while assigned(xCurrentNode) do begin
    xCurentElementPath := Format('%s[%d]/%s', [cMRUElement, xCurrentNode^.Index + 1 , xCurentElementPath]);
    xCurrentNode := Sender.NodeParent[xCurrentNode];
  end;// while assigend(xCurrentNode) do begin
  xCurentElementPath := cXPSendMRU + '/' + xCurentElementPath;
  system.Delete(xCurentElementPath, Length(xCurentElementPath), 1);

  // Selektiert das gew�nschte Element
  result := TLKTermSettings.Instance.SendMRURoot.selectSingleNode(xCurentElementPath);*)
end;

procedure TfrmXMLViewer.mmVirtualStringTree1GetText(
  Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType; var CellText: WideString);
var
  xNodeElement: IXMLDOMNode;
begin
(*  xNodeElement := nil;
  CellText := '';

  // Selektiertes Element
  xNodeElement := SelectElementFromVTNode(Sender, Node);
  if assigned(xNodeElement) then begin
    case Column of
      cMRUNameCol    : CellText := AttributeToStringDef(cAtrMRUName, xNodeElement, '');
      cMRUTextCol    : begin
        if acHideParameter.Checked then begin
          // Behandelt Parameter im Text (siehe Kommentar in CheckForParameter())
          with TParameterHandler.Create do try
            CellText := GetTextWithDefaultValues(AttributeToStringDef(cAtrMRUText, xNodeElement, ''));
          finally
            free;
          end;// with TParameterHandler.Create(xTextToSend) do try
        end else begin
          CellText := AttributeToStringDef(cAtrMRUText, xNodeElement, '');
        end;// if acHideParameter.Checked then begin
      end;// cMRUTextCol    : begin
      cMRUShortCutCol: CellText := AttributeToStringDef(cAtrMRUShortCut, xNodeElement, '');
    end;// case Column of

    (* Wenn kein Text eigegeben wurde, dann ein Leerzeichen damit der Editor aktiviert werden kann.
       Offenbar kann die Spalte nicht detektiert werden, wenn kein Text da ist. In diesem Fall wird
       einfach der Editor der ersten Spalte aktiviert.
    if (CellText = '') and (Column <> cMRUNameCol) and (IsCommandElement(SelectElementFromVTNode(Sender, Node))) then
      CellText := ' ';
  end;// if assigned(xNodeElement) then begin*)
end;

end.
