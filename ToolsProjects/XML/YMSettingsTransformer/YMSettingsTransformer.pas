{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMSettingsTransformer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains classes for converting old YM-Settings in DB-table t_ym_settings
|                 to new XML YM-Settings in DB-table t_xml_ym_settings.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.2004  1.00  Nue | File created
| 24.03.2005  1.00  Wss | SensingHeadClass war noch nicht richtig behandelt
| 20.04.2005  1.00  Wss | Hashcode wird nun fix in der Klasse TXMLSettingsAccess behandelt
| 27.04.2005  1.00  Wss | - Destroy war nicht "override" -> Destroy wurde nie durchlaufen
                          - DB Init und Process separat aufrufen aus Projekt und nicht mehr direkt im Konstruktor
| 27.06.2006  1.00  Wss | Bin2XMLBeforeDelete: Parameter f�r Short Cluster Length aus Channel �bernehmen
                          ==> Spectra -> Spectra+ Konvertierung
|==========================================================================================}
(*
-  F�r die ZE unterst�tzt 1 Mapfile alle Varianten

-  Folgende Informator Versionen gilt es aus heutiger Sicht zu unterst�tzen :

Informator (68k)	Informator C (PPC)	XML Map File Name	INF-Funktionalit�t	AWE-Funktionalit�t
S 5.40	X	        WSC-1234.MMZ	Spectra	Spectra
S 5.70	X		Spectra	        Spectra
S 6.10	S 6.10		Spectra+,Zenit FP	Spectra+, Zenit FP
V 6.14	V 6.12	        WSC-????.MMZ	Zenit FP	Zenit FP
S 6.20	S 6.20		Zenit FP	Zenit FP
X	S 7.00
*)
unit YMSettingsTransformer;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, XMLSettingsAccess, YMParaDBAccess, ADODBAccess, MMEventLog,
  XMLGlobal, MMXMLConverter, BaseGlobal, MSXML2_TLB, XMLDef, YMParadef, XMLMappingDef,
  LoepfeGlobal;

type
  TTransEvent = procedure(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec) of object;
  TYMSettingsTransformer = class(TObject)
  private
    FOnAfterTransformation: TTransEvent;
    FOnBeforeTransformation: TTransEvent;
    FTransformedSets: Integer;
    mDB: TADODBAccess;
    mEventLog: TEventLogWriter;
    mMapfileId: Integer;
    mMapfileName: string;
    mMaxSWOption: TSWOption;
//    mPatternXMLPath: string;
//    mPatternXMLString: string;
    mSettingsDBAssistant: TSettingsDBAssistant;
    mXMLConverter: TMMXMLConverter;
    mXMLSettingsAccess: TXMLSettingsAccess;
    mSensingHeadClass: TSensingHeadClass;
    mYMSetIds: string;
    procedure Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure WriteLogFile(aEvent: TEventType; aText: string);
  protected
    procedure DoAfterTransformation(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
    procedure DoBeforeTransformation(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
  public
    constructor Create(aSender: TObject = nil);
    destructor Destroy; override;
    function Init: Boolean;
    function Process: Boolean;
    property TransformedSets: Integer read FTransformedSets write FTransformedSets;
  published
    property OnAfterTransformation: TTransEvent read FOnAfterTransformation write FOnAfterTransformation;
    property OnBeforeTransformation: TTransEvent read FOnBeforeTransformation write FOnBeforeTransformation;
  end;

procedure Register;

implementation

uses
  mmCS, FileCtrl, JclStrings, FPatternClasses, YMParaUtils, MMUGlobal;

procedure Register;
begin
end;

//------------------------------------------------------------------------------
function GetLowestSensingHeadFromClass(aSHClass: TSensingHeadClass): TSensingHead;
begin
  case aSHClass of
    shc9xFS:   Result := shTK930F;
    shc9xH:    Result := shTK930H;
    shc9xBD:   Result := shTK940BD;
    shcZenit:  Result := shTKZenit;
    shcZenitF: Result := shTKZenitF;
    shcZenitFP: Result := shTKZenitF;
  else  //   shc8x,
    Result := shTK830;
  end;
end;

//------------------------------------------------------------------------------
constructor TYMSettingsTransformer.Create(aSender: TObject = nil);
var
  i: Integer;
begin
  mYMSetIds     := '';
  mEventLog  := TEventLogWriter.Create('Millmaster', '', ssApplication, 'TYMSettingsTransformer: ', True);

  mDB := TAdoDBAccess.Create(3, True); // 2 Query-handles
  mSettingsDBAssistant := TSettingsDBAssistant.Create(mDB.Query[cSecondaryQuery]);
  mXMLConverter        := TMMXMLConverter.Create(mEventLog);
  mXMLSettingsAccess   := TXMLSettingsAccess.Create();
  mXMLSettingsAccess.ConvertToXMLMode := True; //Damit die ID des urspr�nglichen Settings in XML-Settings erhalten bleibt
  try
//    if not mDB.Init then begin
//      WriteLogFile(etError, 'DB.Init failed: ' + mDB.DBErrorTxt);
//      Exit;
//    end;


    mMapfileName := '';
    mMapfileId   := 0;
    if (aSender = nil) then begin //Aufruf von Commandline
      for i := 1 to ParamCount do begin
        if Pos('n:', ParamStr(i)) = 1 then
          mMapfileName := StrAfter('n:', ParamStr(i)); //Zuweisen Mapfilename
        if Pos('i:', ParamStr(i)) = 1 then
          mMapfileId := StrToIntDef(StrAfter('i:', ParamStr(i)), 0); //Zuweisen MapfileID
      end; //for

//      try
//        Process;
//      except
//        on e: Exception do begin
//          WriteLogFile(etError, 'Create: ' + e.Message);
//          raise EMMException.Create('Create: ' + e.Message);
//        end;
//      end; // try
    end; // if (aSender = nil)
  finally
//    mDB.Free;
  end; // try
end; // TYMSettingsTransformer.Create cat:No category

//------------------------------------------------------------------------------
destructor TYMSettingsTransformer.Destroy;
begin
  mXMLSettingsAccess.Free;
  mXMLConverter.Free;
  mSettingsDBAssistant.Free;
  mDB.Free;
  mEventLog.Free;

  inherited Destroy;
end; // TYMSettingsTransformer.Destroy cat:No category

//------------------------------------------------------------------------------
procedure TYMSettingsTransformer.Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xShortCountNode: IXMLDOMNode;
  xGroupElement: IXMLDOMElement;
  xMachineElement: IXMLDOMElement;
  xFPattern: TSettingsFPBuilder;
  xXMLNodeChannel: IXMLDOMNode;
  xXMLNodeCluster: IXMLDOMNode;
  xStr: string;
begin
  // F�r die Konvertierung von Spectra -> Spectra+ muss f�r die L�nge im Short Cluster
  // die L�nge vom Channel �bernommen werden. Per Facets im Map-File wird dieser pauschal auf 1 gesetzt
  // und wurde somit sehr "scharf" gesetzt.
  xXMLNodeChannel := aMMXMLDoc.selectSingleNode(cXPChannelShortLengthItem);
  xXMLNodeCluster := aMMXMLDoc.selectSingleNode(cXPClusterShortLengthItem);
  xStr := GetElementValueDef(xXMLNodeChannel, '1.0');
  SetElementValue(xXMLNodeCluster, xStr);

  // Nun m�ssen noch die Klassierfelder transformiert werden
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPYMSettingBasicClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFDarkClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPDarkClusterClassificationItem));

  xShortCountNode := aMMXMLDoc.selectSingleNode(cXPFP_ShortCountItem);
  SetElementValue(xShortCountNode, 'False');

  // F�r Texnet und WSC muss das FPattern aus den Settings erzeugt werden
  xFPattern := TSettingsFPBuilder.Create;
  try
    Supports(aMMXMLDoc.SelectSingleNode(cXPMachineNode), IXMLDOMElement, xMachineElement);
    xFPattern.MachineElement := xMachineElement;

    Supports(aMMXMLDoc.SelectSingleNode(cXPGroupNode), IXMLDOMElement, xGroupElement);
    xFPattern.GroupElement := xGroupElement;

    // In der Settings Tabelle ist nur noch der HeadClass gespeichert, zum bilden des FPattern
    // ist aber der SensingHead n�tig -> simulieren
    xFPattern.GroupValue[cXPSensingHeadItem] := GetSensingHeadName(GetLowestSensingHeadFromClass(mSensingHeadClass));
    // Die maximal Software Option vorher noch �berschreiben
    xFPattern.MachValue[cXPSWOptionItem] := GetSWOptionName(mMaxSWOption);

    // und jetzt das FPattern erstellen
    xFPattern.BuildFPatternFromConfig(ntTxn, TMMXMLConverter(Sender).MapSection);
  finally
    xFPattern.Free;
  end;// try finally
end; // TYMSettingsTransformer.Bin2XMLBeforeDelete cat:No category

//------------------------------------------------------------------------------
procedure TYMSettingsTransformer.DoAfterTransformation(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
begin
  if Assigned(FOnAfterTransformation) then FOnAfterTransformation(aSender, aXMLSettingsRec);
end; // TYMSettingsTransformer.DoAfterTransformation cat:No category

//------------------------------------------------------------------------------
procedure TYMSettingsTransformer.DoBeforeTransformation(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
begin
  if Assigned(FOnBeforeTransformation) then FOnBeforeTransformation(aSender, aXMLSettingsRec);
end; // TYMSettingsTransformer.DoBeforeTransformation cat:No category

function TYMSettingsTransformer.Init: Boolean;
begin
  Result := mDB.Init;
  if not Result then
    WriteLogFile(etError, 'mDB.Init failed: ' + mDB.DBErrorTxt);
end;

//------------------------------------------------------------------------------
function TYMSettingsTransformer.Process: Boolean;
const
  cQrySelectMapfileByID = 'select c_mapfile from t_xml_mapfile where c_mapfile_id=:c_mapfile_id ';
  cQrySelectMapfileByName =
    ' select c_mapfile from t_xml_mapfile ' +
    ' where c_mapfile_name like ''ZE-01%'' ' +
    ' order by c_mapfile_name desc';
var
  xId: Integer;
  xYMSettingsByteArr: TYMSettingsByteArr;
  xSize: Word;
  xXMLData: string;
  xXMLSettingsRec: PXMLSettingsRec;
  xMapfile: string;
  xStr: string;
  xSourceSets: Integer;
  xCodeSiteEnabled: Boolean;
  //...............................................................
  procedure GetMaxSWOption;
  var
    xStr: String;
  begin
    mMaxSWOption := swoBase;

    with mDB.Query[cPrimaryQuery] do
    try
      SQL.Text := 'select c_optionCode from t_machine';
      Open;
      if FindFirst then begin
        while (not EOF) and (mMaxSWOption < High(TSWOption)) do begin
          xStr := FieldByName('c_optionCode').AsString;
          if (AnsiPos('Quality', xStr) > 0) and (mMaxSWOption < swoQuality) then mMaxSWOption := swoQuality
          else if (AnsiPos('Imperfection', xStr) > 0) and (mMaxSWOption < swoImperfection) then mMaxSWOption := swoImperfection
          else if (AnsiPos('LabPack', xStr) > 0) and (mMaxSWOption < swoLabPack) then mMaxSWOption := swoLabPack;
          Next;
        end; // while
      end;
      Close;
    except
      on e: Exception do
        raise EMMException.Create('GetMaxSWOption: ' + e.message);
    end;
  end;
  //...............................................................
begin
//  CodeSite.Enabled := False;
  Result           := False;
  xCodeSiteEnabled := CodeSite.Enabled;
  FTransformedSets := 0;

  WriteLogFile(etInformation, 'Start YM-Settings Transformation to XML!');
  if mMapfileName <> '' then begin //Mapfile wurde als Parameter �bergeben
    // File lesen
    if FileExists(mMapfileName) then begin
      with TStringList.Create do
      try
        LoadFromFile(mMapfileName);
        xMapFile := Text;
      finally
        Free;
      end;
    end
    else begin
      WriteLogFile(etError, Format('SettingsTransformation failed: Mapfile [%s] not available!!', [mMapfileName]));
      Exit;
    end;
  end
  else begin //lesen ab DB
    with mDB.Query[cPrimaryQuery] do
    try
      Close;
      //Holen des Mapfile f�r Transformation
      if mMapfileID > 0 then begin
        SQL.Text := cQrySelectMapfileByID;
        ParamByName('c_mapfile_id').AsInteger := mMapfileId;
      end
      else begin
        SQL.Text := cQrySelectMapfileByName;
      end;
      Open;
      if FindFirst then
        xMapfile := FieldByName('c_mapfile').AsString
      else
        raise EMMException.Create(Format('Mapfile with ID=%d not found!!', [mMapfileId]));

    except
      on e: Exception do
        raise EMMException.Create('Process: '+ e.Message);
    end;
  end; //else

  GetMaxSWOption;
  with mDB.Query[cPrimaryQuery] do
  try
    SQL.Text := 'select c_ym_set_id, c_YM_set_name, c_head_class, c_template_set, c_color from t_ym_settings';
//    mYMSetIds := '1,2,3,4,5,6'; //'-26,-25,-24,-23';
    if Trim(mYMSetIDs) = '' then
      mYMSetIds := GetRegString(cRegLM, cRegMMDebug, 'TransformYMSetIDs');

    if mYMSetIds <> '' then
      SQL.Add(Format('where c_ym_set_id in (%s) ', [mYMSetIds]));
    SQL.Add('order by c_ym_set_id');
    Open;

    with mXMLConverter do begin
      // im OnBeforeDeleteElements wird das FPattern "ausgestanzt" weil dies separat auf DB gespeichert wird
      OnBeforeDeleteElements := Bin2XMLBeforeDelete;

      xSourceSets := 0;
      while not EOF do begin
        // alte HeadClass: hcTK8xx=0; neue HeadClass: shcNone=0, shc8x=1
        mSensingHeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger + 1);
        if mSensingHeadClass = shc9xBD then
          mSensingHeadClass := shc8x;

        xId := FieldByName('c_ym_set_id').AsInteger;
        mSettingsDBAssistant.Get(xId, xYMSettingsByteArr, xSize);
        INC(xSourceSets);
        try
          // in mPatternXMLString ist dann das FPattern herauskopiert worden
          xXMLData := BinToXML(PByte(@xYMSettingsByteArr), xSize, xMapfile, msYMSetting, False);

          xXMLSettingsRec := AllocMem(sizeOf(TXMLSettingsRec) + Length(xXMLData));
          with xXMLSettingsRec^ do begin
            ProdGrpID := 0; //0, damit innerhalb von TXMLSettingsAccess.SaveSetting die Tabelle t_prodgroup nicht updated wird
            YMSetID   := xId;
            xStr      := FieldByName('c_ym_set_name').AsString;
            StrLCopy(@YMSetName, PChar(xStr), sizeOf(TText50));
            HeadClass := mSensingHeadClass;
            Color     := FieldByName('c_color').AsInteger;
            // die XMLSettings in Jobbuffer hineinkopieren
            System.Move(PChar(xXMLData)^, XMLData, Length(xXMLData));
          end; //with xXMLSettingsRec^ do begin
          codeSite.SendFmtMsg('SettingsTrans: Name: %s  Alloc-Size: %d', [xStr, sizeOf(TXMLSettingsRec) + Length(xXMLData)]);

          DoBeforeTransformation(self, xXMLSettingsRec);

          try
            CodeSite.Enabled := False;
            Result := (mXMLSettingsAccess.SaveSetting(xXMLSettingsRec, FieldByName('c_template_set').AsBoolean) > 0);
            CodeSite.Enabled := xCodeSiteEnabled;
            if Result then begin
              //Neuer Datensatz konnte in t_xml_ym_settings eingef�gt werden
              INC(FTransformedSets);
              WriteLogFile(etInformation, Format('Dataset %s (id=%d) insert successful on DB (Total new Sets: %d!)', [xStr, xId, FTransformedSets]));
            end else begin
              WriteLogFile(etError, Format('Dataset %s (id=%d) insert failed on DB (Total new Sets: %d!)', [xStr, xId, FTransformedSets]));
            end;
          except
            on e: Exception do begin
              raise EMMException.Create(e.message + Format('Dataset %s (id=%d) insert exception on DB.', [xStr, xId]));
            end;
          end; //Try

          DoAfterTransformation(self, xXMLSettingsRec);
          FreeMem(xXMLSettingsRec);

        except
          on e: Exception do begin
            WriteLogFile(etError, e.Message + ' in Process;');
            raise EMMException.Create(e.message + ' in TYMSettingsTransformer.Process;');
          end;
        end; // try BinToXML(

        Next;
      end;
    end; // with mXMLConverter

    if xSourceSets = FTransformedSets then begin
      WriteLogFile(etInformation, 'Successful end of YM-Settings Transformation to XML!');
    end
    else begin
      raise EMMException.Create(Format('TYMSettingsTransformer.Process: From %d settings in t_ym_settings only %d transformed to t_xml_ym_settings!', [xSourceSets, FTransformedSets]));
    end;

  except
    on e: Exception do
      raise EMMException.Create(e.message + 'in YM-Settings Transformation to XML;');
  end;
end; // TYMSettingsTransformer.Process cat:No category

//------------------------------------------------------------------------------
procedure TYMSettingsTransformer.WriteLogFile(aEvent: TEventType; aText: string);
begin
  mEventLog.Write(aEvent, aText);
end; // TYMSettingsTransformer.WriteLogFile cat:No category

end.

