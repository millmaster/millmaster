unit TestTransformerMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmEdit, mmLabel, Grids, DBGrids, mmDBGrid, Db, ADODB,
  mmADODataSet, mmDataSource, mmADOConnection, YMSettingsTransformer, BaseGlobal,
  ExtCtrls, DBCtrls, XMLViewer;

type
  TForm1 = class(TForm)
    mmADOConnection1: TmmADOConnection;
    mmDataSource1: TmmDataSource;
    mmADODataSet1: TmmADODataSet;
    mmDBGrid1: TmmDBGrid;
    mmDBGrid2: TmmDBGrid;
    labTab1: TmmLabel;
    labTab2: TmmLabel;
    mmADODataSet2: TmmADODataSet;
    mmDataSource2: TmmDataSource;
    edIDs: TmmEdit;
    bIDs: TmmButton;
    bStart: TmmButton;
    labState: TmmLabel;
    labNewSets1: TmmLabel;
    labNewSets2: TmmLabel;
    DBNavigator1: TDBNavigator;
    procedure bStartClick(Sender: TObject);
    procedure DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
    procedure mmDataSource2UpdateData(Sender: TObject);
    procedure mmDBGrid2DblClick(Sender: TObject);
  private
    { Private declarations }
    mMapFileViewer: TfrmXMLViewer;
    mLastExportedMapfile: string;
    mLastExportedMapfileName: string;
    mYMSettingsTransformer: TYMSettingsTransformer;
    procedure AfterTransformationEventHandler(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.bStartClick(Sender: TObject);
begin
  labState.Visible := False;
  labNewSets2.Caption := Capo;
  with TYMSettingsTransformer.Create(Sender) do try
    Init(edIDs.Text);
    OnAfterTransformation := AfterTransformationEventHandler;  // per XPath wird das Bauzustandsfragment extrahiert
    if Process then begin
      labState.Caption := 'Transformation successful!';
      labState.Font.Color := clGreen;
    end
    else begin
      labState.Caption := 'Transformation failed!';
      labState.Font.Color := clRed;
    end; //if
  finally
    Free;
  end; // with TYMSettingsTransformer.Create
  labState.Visible := True;

end;

procedure TForm1.AfterTransformationEventHandler(aSender: TObject; aXMLSettingsRec: PXMLSettingsRec);
var
  xInt: Integer;
begin
  mmADODataSet2.Close;
  mmADODataSet2.Open;
//  mmDBGrid2.Refresh;
  xInt := StrToInt(labNewSets2.Caption);
  INC(xInt);
  labNewSets2.Caption := IntToStr(xInt);
end;

procedure TForm1.DBNavigator1Click(Sender: TObject; Button: TNavigateBtn);
begin
  if Button=nbDelete then
    mmADODataSet2.DeleteRecords(arCurrent);
end;

procedure TForm1.mmDataSource2UpdateData(Sender: TObject);
begin
  mmDBGrid2.Refresh;
end;

procedure TForm1.mmDBGrid2DblClick(Sender: TObject);
var
  xField : TWideStringField;
begin
  xField := TWideStringField(mmDBGrid2.SelectedField);

  if not(assigned(mMapFileViewer)) then
    mMapFileViewer := TfrmXMLViewer.Create(Application);
  mMapFileViewer.ShowNewMapfile(xField.AsString, 'nuee'{mLastExportedMapfileName});

  mMapFileViewer.Show;
//  end;// if acShowLastExportetMapfile.Checked then begin

end;

end.
