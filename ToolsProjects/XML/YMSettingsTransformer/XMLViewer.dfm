object frmXMLViewer: TfrmXMLViewer
  Left = 1777
  Top = 34
  Width = 641
  Height = 781
  Caption = 'frmXMLViewer'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mXPResultSplitter: TmmSplitter
    Left = 0
    Top = 385
    Width = 633
    Height = 9
    Cursor = crVSplit
    Align = alTop
    Beveled = True
    OnMoved = mXPResultSplitterMoved
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 633
    Height = 29
    ButtonWidth = 101
    Caption = 'mmToolBar1'
    Flat = True
    Images = mmImageList1
    List = True
    ParentShowHint = False
    ShowCaptions = True
    ShowHint = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = acLoadXML
      DropdownMenu = pmOpenFileMRU
      Style = tbsDropDown
    end
    object ToolButton2: TToolButton
      Left = 114
      Top = 0
      Action = acLoadMapFromDB
    end
  end
  object paXP: TmmPanel
    Left = 0
    Top = 29
    Width = 633
    Height = 356
    Align = alTop
    BevelOuter = bvNone
    Constraints.MinHeight = 18
    TabOrder = 1
    object paXPHeader: TmmPanel
      Left = 0
      Top = 0
      Width = 633
      Height = 18
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object mMethodNamePanel: TmmPanel
        Left = 0
        Top = 0
        Width = 633
        Height = 18
        Align = alTop
        BevelInner = bvLowered
        BevelOuter = bvNone
        Color = clInfoBk
        TabOrder = 0
        object mMinusImage: TmmImage
          Left = 432
          Top = 0
          Width = 9
          Height = 9
          Picture.Data = {
            07544269746D617032010000424D320100000000000036000000280000000900
            0000090000000100180000000000FC000000120B0000120B0000000000000000
            0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
            78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
            CFD8DCC9D3D8C7D2D7C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
            EDEDE6EBECE3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
            00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7F4F7F7F4F6F6
            EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFDFBFDFDFBFCFCFAFCFCF3
            F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
            7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object bExpand: TmmImage
          Left = 1
          Top = 3
          Width = 12
          Height = 12
          Picture.Data = {
            07544269746D617032010000424D320100000000000036000000280000000900
            0000090000000100180000000000FC000000120B0000120B0000000000000000
            0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
            78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
            CFD8DCC9D3D8000000C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
            EDED000000E3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
            00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7000000F4F6F6
            EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFD000000FBFCFCFAFCFCF3
            F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
            7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
          Stretch = True
          Visible = True
          OnClick = bExpandClick
          AutoLabel.LabelPosition = lpLeft
        end
        object mPlusImage: TmmImage
          Left = 424
          Top = 0
          Width = 9
          Height = 9
          Picture.Data = {
            07544269746D617032010000424D320100000000000036000000280000000900
            0000090000000100180000000000FC000000120B0000120B0000000000000000
            0000D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000B598
            78BFCCD2AEBEC6A8B8C2A7B8C1A7B8C1A6B7C0AABAC3B5987800B59878D9E1E4
            CFD8DCC9D3D8000000C6D1D6C0CCD2BBC8CFB5987800B59878EEF2F2ECF0F0E7
            EDED000000E3E9EAD9E0E3CCD6DBB5987800B59878F1F5F50000000000000000
            00000000000000D2DBDFB5987800B59878F5F7F7F5F7F7F4F7F7000000F4F6F6
            EBF0F1DAE1E5B5987800B59878FBFCFCFBFDFDFBFDFD000000FBFCFCFAFCFCF3
            F6F7B5987800B59878FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB598
            7800D3C2B0B59878B59878B59878B59878B59878B59878B59878D3C2B000}
          Visible = False
          AutoLabel.LabelPosition = lpLeft
        end
        object laMethodName: TmmStaticText
          Left = 19
          Top = 3
          Width = 74
          Height = 17
          Caption = 'XPath Konsole'
          TabOrder = 0
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
    object paXPRoot: TmmPanel
      Left = 0
      Top = 18
      Width = 633
      Height = 338
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object paXPResult: TmmPanel
        Left = 0
        Top = 88
        Width = 633
        Height = 250
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object paXMLResultLeft: TmmPanel
          Left = 0
          Top = 0
          Width = 448
          Height = 250
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
        end
        object paXMLResultRight: TmmPanel
          Left = 448
          Top = 0
          Width = 185
          Height = 250
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object mmVirtualStringTree1: TmmVirtualStringTree
            Left = 0
            Top = 0
            Width = 185
            Height = 250
            Align = alClient
            Header.AutoSizeIndex = 0
            Header.Font.Charset = DEFAULT_CHARSET
            Header.Font.Color = clWindowText
            Header.Font.Height = -11
            Header.Font.Name = 'MS Sans Serif'
            Header.Font.Style = []
            Header.MainColumn = -1
            Header.Options = [hoColumnResize, hoDrag]
            TabOrder = 0
            TreeOptions.AutoOptions = [toAutoDropExpand, toAutoSpanColumns, toAutoTristateTracking, toAutoDeleteMovedNodes]
            TreeOptions.PaintOptions = [toShowButtons, toShowRoot, toShowTreeLines, toThemeAware, toUseBlendedImages]
            TreeOptions.SelectionOptions = [toExtendedFocus, toFullRowSelect, toRightClickSelect, toCenterScrollIntoView]
            OnGetText = mmVirtualStringTree1GetText
            Columns = <>
          end
        end
      end
      object paXPInput: TmmPanel
        Left = 0
        Top = 0
        Width = 633
        Height = 88
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object mmPanel1: TmmPanel
          Left = 0
          Top = 0
          Width = 416
          Height = 88
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object mmLabel1: TmmLabel
            Left = 0
            Top = 0
            Width = 416
            Height = 13
            Align = alTop
            Caption = 'XPath:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object memoXP: TmmMemo
            Left = 0
            Top = 13
            Width = 416
            Height = 75
            Align = alClient
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Courier New'
            Font.Style = []
            ParentFont = False
            ScrollBars = ssVertical
            TabOrder = 0
            Visible = True
            OnChange = memoXPChange
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mmPanel2: TmmPanel
          Left = 416
          Top = 0
          Width = 217
          Height = 88
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object mmLabel2: TmmLabel
            Left = 29
            Top = 7
            Width = 59
            Height = 26
            Caption = 'Beim Tippen'#13#10'ausf�hren'
            FocusControl = cbExecuteOnChange
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object cbExecuteOnChange: TmmCheckBox
            Left = 8
            Top = 11
            Width = 17
            Height = 17
            TabOrder = 0
            Visible = True
            AutoLabel.Control = mmLabel2
            AutoLabel.Distance = 4
            AutoLabel.LabelPosition = lpRight
          end
          object mmButton1: TmmButton
            Left = 96
            Top = 8
            Width = 121
            Height = 25
            Action = acExecuteXP
            TabOrder = 1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mDescriptionPanel: TmmPanel
            Left = 7
            Top = 40
            Width = 210
            Height = 47
            BevelOuter = bvLowered
            TabOrder = 2
            object mmPanel8: TmmPanel
              Left = 1
              Top = 1
              Width = 21
              Height = 45
              Align = alLeft
              BevelOuter = bvNone
              Color = clInfoBk
              TabOrder = 0
              object mInfoImage: TmmImage
                Left = 2
                Top = 0
                Width = 16
                Height = 16
                Picture.Data = {
                  07544269746D617036030000424D360300000000000036000000280000001000
                  0000100000000100180000000000000300000000000000000000000000000000
                  0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                  E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                  FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                  84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                  638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                  FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                  DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                  CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                  FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                  FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                  CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                  716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                  FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                  DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                  695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                  FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                  FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                  7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                  FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                  FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                  FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                  FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                  DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                  FFFF}
                Transparent = True
                Visible = True
                AutoLabel.LabelPosition = lpLeft
              end
            end
            object mDescriptionMemo: TmmMemo
              Left = 22
              Top = 1
              Width = 187
              Height = 45
              Align = alClient
              BorderStyle = bsNone
              Color = clInfoBk
              Ctl3D = False
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              Lines.Strings = (
                'Mit Ctrl+E (oder Button) wird nur '
                'die Selektion ausgef�hrt. Ohne '
                'Selektion wird ales ausgef�hrt.')
              ParentCtl3D = False
              ParentFont = False
              TabOrder = 1
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
      end
    end
  end
  object paXMLRoot: TmmPanel
    Left = 0
    Top = 394
    Width = 633
    Height = 360
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
  end
  object mmActionList1: TmmActionList
    Images = mmImageList1
    Left = 376
    Top = 88
    object acLoadXML: TAction
      Caption = 'XML File �ffnen'
      Hint = '�ffnet ein XML File'
      ImageIndex = 0
      OnExecute = acLoadXMLExecute
    end
    object acLoadMapFromDB: TAction
      Caption = 'Mapfile von DB'
      Hint = '�ffnet ein Mapfile von der DB'
      ImageIndex = 0
      OnExecute = acLoadMapFromDBExecute
    end
    object acExecuteXP: TAction
      Caption = 'XPath ausf�hren'
      Hint = 'F�hrt den Selektierten Text aus (Keine Selektion = alles)'
      ShortCut = 16453
      OnExecute = acExecuteXPExecute
    end
  end
  object mmImageList1: TmmImageList
    Left = 336
    Top = 80
    Bitmap = {
      494C010101000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001001000000000000008
      0000000000000000000000000000000000000000205E205E205E205E205E205E
      205E205E205E205E205E205E205E000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205E846A0E73507F2C7F2C7F2C7F
      2C7F2C7F2C7F2C7F2C7FA76E205E000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205EE972A76A947F4D7F4D7F4D7F
      4D7F4D7F4D7F4D7F4D7FC76E5077205E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205E4E7F205EB57F6F7F6F7F6F7F
      6F7F6F7F6F7F6F7F6F7FC86ED57F205E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205E6F7F6362737B927F907F907F
      907F907F907F907F907FE96ED67F205E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205E907FC86E0C73D57FB17FB17F
      B17FB17FB17FB17F010EE96ED67F2D73205E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205EB17F6E7B6462FF7FD97FD97F
      D97FD97FD97F010EE72E010EFB7FDA7F205E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205ED27FD27F4262205E205E205E
      205E205E010E6A3FAC4B0833010E205E205E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205ED37FD37FD37FD37FD37FD37F
      D37F010E28378B478B47AC4B0833010E00000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000205EFF7FF47FF47FF47FF47FF47F
      010E010E010E010E8A432837010E010E010E0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000205EFF7FF47FF47FF47F205A
      C86EC86EC86E010E693FE62A010E000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000205E205E205E205E0000
      000000000000010E4833010E0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000010EC522E526010E0000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000010EC41E010E00000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      010E010E010E010E000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000010E010E
      010E010E00000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF0080030000000000000003000000000000
      0001000000000000000100000000000000010000000000000000000000000000
      0000000000000000000000000000000000010000000000000000000000000000
      8003000000000000C3C7000000000000FF87000000000000FF8F000000000000
      FE1F000000000000F87F00000000000000000000000000000000000000000000
      000000000000}
  end
  object mXMLOpenDialog: TmmOpenDialog
    DefaultExt = 'map'
    Filter = 
      'Mapfiles (*.map)|*.map|XML Dateien (*.xml)|*.xml|Alle Dateien (*' +
      '.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist]
    Title = 'XML Datei �ffnen'
    Left = 96
    Top = 96
  end
  object pmOpenFileMRU: TmmPopupMenu
    OnPopup = pmOpenFileMRUPopup
    Left = 272
    Top = 24
  end
end
