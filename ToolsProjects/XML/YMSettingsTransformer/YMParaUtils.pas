(*=========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMParaUtils.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Supports initialisation and other helper functions for YarnMaster settings
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.10.1999  1.00  Kr  | Initial Release, Definitive Yarn Count Units fehlt
| 03.11.1999  1.01  Mg  | TVoidDataItem and
| 16.11.1999  1.02NueKr | Swap's bei Get*-Methoden (Aufruf von Extract) rausgenommen.
| 12.04.2000  1.03  Nue | KorrekturHack temporaer!!!!!!!!!! NueWSCPatch added
| 08.05.2000  1.04  Kr  | GetProdGrpYMPara and PutProdGrpYMPara countUnit and threadCnt added
| 05.11.2001  1.05  Nue | "or (MachineConfig.aWEMachType = amtAC338)" added in GetAvailable.
| 20.11.2001  1.06  khp | Anpassung in ConvertSettingsFromAC338: Konvertieren von WSCGarnNr
|                       | in LoepfeGarnNr und WSCGarneinheit in MMStandarteinheit(Nm)
| 28.02.2002  1.07  khp | Zurueck bauen der Anpassung in ConvertSettingsFromAC338: Konvertieren von WSCGarnNr
|                       | in LoepfeGarnNr und WSCGarneinheit in MMStandarteinheit(Nm)
| 18.03.2002  1.08  Kr  | Diverse Aenderungen
| 06.06.2002  1.09  khp | ConvertSettingsFromAC338: WSCGarnNr=EinzelfadenNr, LoepfeGarnNr=GesamtfadenNr
|                       | somit LoepfeGarnNr = WSCGarnNr DIV fadenzahl
| 04.06.2004  1.11  Kr  | TConfigurationCode.ValidateSensingHeadDependency:
|                       | cCCBNoFFAdjAtOfflimitBit and cCCBFFAdjAfterAlarmBit wird nicht mehr default gesetzt
| 13.01.2005  1.11  Wss | SwapClassFields() Funktion hinzugef�gt
|=========================================================================================*)
unit YMParaUtils;

interface

uses
  YMParaDef, BaseGlobal, MMUGlobal, LoepfeGlobal, Windows, sysutils, classes,
  XMLDef, XMLSettingsModel, xmlMaConfigClasses, MSXML2_TLB;

const
//------------------------------------------------------------------------------
// Test initialisation (more or less Memory C)
//------------------------------------------------------------------------------
{
 cYMSettingsDefault: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
      swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( cIP0MDefault, cIP1MFFDefault ); // %% Temp for Bueler Sennhof 9.05.01
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            550;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          cCCADefault or $01;
      configB:          cCCBDefault;
      configC:          cCCCDefault;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		cDefaultYCCheckLength;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultRepetitions;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      cInitLongStopDef;
      lengthWindow:     100;
      lengthMode:       cDRMLast;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              cDefaultRepetitions;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              cDefaultRepetitions;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
{}
  //...........................................................................

  //...........................................................................

{
 cYMSettingsCD: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
     swOption:            0;
      group:            1;
      prodGrpID:        4711;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      Ord(cTK930F);
      machType:         2;
      speedRamp:        5;
      inoperativePara: ( 0, 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps: (
        dia:            550;
        sw:             cChOn;
//        sw:             cChOff;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         13;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          400;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      splice: (
        len:            150;
        sw:             cChOn;
      );
      speed:            600;
      spare:            0;
    );

   splice: (
      neps: (
        dia:            500;
        sw:             cChOn;
      );
      short: (
        dia:            200;
        sw:             cChOn;
      );
      shortLen:         40;
      long: (
        dia:            125;
        sw:             cChOn;
      );
      longLen:          150;
      thin: (
        dia:            80;
        sw:             cChOn;
      );
      thinLen:          900;
      upperYarn: (
        dia:            141;
        sw:             cChOn;
      );
      checkLen:         25;
      spare_:           0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        100; //Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         cDefaultRepetitions;
        siroStartupRep: cDefaultSiroStartupRep;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     cDefaultClusterRep;
        cutRetries:     cDefaultRepetitions;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              cDefaultRepetitions;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
  //...........................................................................

 cYMSettingsTest: TYMSettingsRec = (
   available: (
     assignement:      0;
     restart:          0;
     swOption:            0;
      group:            1;
      prodGrpID:        1291;
      spdl: (
        start:          1;
        stop:           9;
      );
      pilotSpindles:    3;
      sensingHead:      6;
     machType:         2;
      speedRamp:        5;
      inoperativePara: ( 0, 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoSpeedSim or cIPMUpperYarn or cIPMSHTypeF), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn or cIPMNoSpeedSim), 0 );
//      inoperativePara: ( (cIPMNoPlus or cIPMNoUpperYarn), 0 );
      voidData:         0;
      spare:            0;
    );
    channel: (
      neps:     (dia: cNepsDiaMin;      sw:  cChOn);
      short:    (dia: cShortDiaMin;     sw:  cChOn);
      shortLen: cShortLenMin;
      long:     (dia: cLongDiaMin;      sw:  cChOn);
      longLen:  cLongLenMin;
      thin:     (dia: cThinDiaMaxBelow; sw:  cChOn);
      thinLen:  cThinLenMin;
      splice:   (len: 150;              sw:  cChOn);
      speed:    600;
      spare:    0;
//      neps:     (dia: cMinNepsDia;      sw:  cChOn);
//      short:    (dia: cMinShortDia;     sw:  cChOn);
//      shortLen: cMinShortLen;
//      long:     (dia: cMinLongDia;      sw:  cChOn);
//      longLen:  cMinLongLen;
//      thin:     (dia: cMaxThinDiaBelow; sw:  cChOn);
//      thinLen:  cMinThinLen;
//      splice:   (len: 150;              sw:  cChOn);
//      speed:    600;
//      spare:    0;
    );

   splice: (
      neps:      (dia: cNepsDiaMin;      sw: cChOn);
      short:     (dia: cShortDiaMin;     sw: cChOn);
      shortLen:  cShortLenMin;
      long:      (dia: cLongDiaMin;      sw: cChOn);
      longLen:   cLongLenMin;
      thin:      (dia: cThinDiaMaxBelow; sw: cChOn);
      thinLen:   cThinLenMin;
      upperYarn: (dia: 141;              sw: cChOn);
      checkLen:  25;
      spare_:    0;
//      neps:      (dia: cMinNepsDia;      sw: cChOn);
//      short:     (dia: cMinShortDia;     sw: cChOn);
//      shortLen:  cMinShortLen;
//      long:      (dia: cMinLongDia;      sw: cChOn);
//      longLen:   cMinLongLen;
//      thin:      (dia: cMaxThinDiaBelow; sw: cChOn);
//      thinLen:   cMinThinLen;
//      upperYarn: (dia: 141;              sw: cChOn);
//      checkLen:  25;
//      spare_:    0;
    );

    classClear: ( $80, $C0, $F0, $F8, $FC, $FE, $FF, $FF,
                  $F0, $C0, $80, $00, $00, $C0, $E0, $F8);
    siroClear: ( $00, $F0, $F8, $FC, $FC, $FC, $FC, $FC );

   additional: (
     option: (
       diaDiff:        180;
//       yarnCnt:        cInitYarnCount;
       // TODO wss: in record ist Word, YarnCount aber neu Float
       yarnCnt:        Round(cYarnCountInit * 10);
       yarnUnit:       Word(yuNm);
        threadCnt:      1;
        cntRep:         3;
        siroStartupRep: 6;
        clusterDia:     135;
        clusterLength:  1000;
        clusterDefects: 30;
      );
      configA:          $001B;
      configB:          $0020;
      configC:          $0000;
      fAdjConeReduction:  0;
      adjRequest:       0;
      spare: ( 0, 0 );
      extOption: (
       negDiaDiff:     160;
       countLength:		1;
       clusterRep:     3;
        cutRetries:     3;
        fill:           0;
        spare: (0, 0);
      );
    );

    machSet: (
     machBez: ( Ord('S'), Ord('a'), Ord('v'), Ord('i'), Ord('o'), Ord('1'), 0, 0, 0, 0 );
      longStopDef:      10;
      lengthWindow:     11;
      lengthMode:       12;
      yMType:           13;
      spare: ( 0, 0 );
      reinVer:          0;
      reinTech:         0;
      yMSWVersion: (  Ord('K'), Ord('r'), Ord('T'), Ord('u'), Ord('r'), Ord('b'), Ord('o'), 0 );
    );

   sfi: (
      absRef:           0;
      upper:            100;
      lower:            100;
      rep:              3;
      fill0:            0;
    );

   fFCluster: (
     obsLength:        1000;
      defects:          30;
      rep:              3;
      fill0:            0;
//      siroClear: ( $8F, $8F, $CF, $EF, $EF, $FF, $FF, $FF );
      siroClear: ( $00, $00, $00, $00, $00, $00, $00, $00 );
    );
   spare: (
      fill: ( 1, 2, 3, 4, 5, 6 );
    );
 );
{}
  //...........................................................................

{
  cChannelOff: TChannelSettingsRec = (
    neps:     (dia: cNepsDiaMin;      sw: cChOff);
    short:    (dia: cShortDiaMin;     sw: cChOff);
    shortLen: cShortLenMin;
    long:     (dia: cLongDiaMin;      sw: cChOff);
    longLen:  cLongLenMin;
    thin:     (dia: cThinDiaMaxBelow; sw: cChOff);
    thinLen:  cThinLenMin;
    splice:   (len: 150;              sw: cChOff);
    speed:    600;
    spare:    0;
//    neps:     (dia: cMinNepsDia; sw: cChOff);
//    short:    (dia: cMinShortDia; sw: cChOff);
//    shortLen: cMinShortLen;
//    long:     (dia: cMinLongDia; sw: cChOff);
//    longLen:  cMinLongLen;
//    thin:     (dia: cMaxThinDiaBelow; sw: cChOff);
//    thinLen:  cMinThinLen;
//    splice:   (len: 150; sw: cChOff);
//    speed:    600;
//    spare:    0;
  );
{}

  //...........................................................................
// ZE Version V8.12, 9.12, 10.12: ConfigCodeC is not Swapped
  cFSWVConfigCodeCNotSwapped: array[0..2] of TYMSWVersionArr = (
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('2'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('2'),0,0,0)
  );

  // ZE Version V8.12, 9.12, 10.12 and V8.14, 9.14, 10.14: FFCluster.defects &
  // FFCluster.obsLength not Swapped
  cFSWVFFClusterNotSwapped: array[0..5] of TYMSWVersionArr = (
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('2'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('2'),0,0,0),
    (Byte('V'),Byte('1'),Byte('0'),Byte('.'),Byte('1'),Byte('4'),0,0),
    (Byte('V'),Byte('9'),Byte('.'),Byte('1'),Byte('4'),0,0,0),
    (Byte('V'),Byte('8'),Byte('.'),Byte('1'),Byte('4'),0,0,0)
  );

// Informator Version V5.10: inopSettings not available
  cFSWVInopSettingsNotAvailable: array[0..0] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('1'),Byte('0'),0,0)
  );

// Informator Version V5.25: inopSettings FFTK wrong
  cFSWVInopSettingsWrongFFTK: array[0..0] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('2'),Byte('5'),0,0)
  );

  // Informator Version V5.10, V5.25: SWOption not available
  cFSWVSWOptionNotAvailable: array[0..1] of TYMSWVersionArr = (
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('1'),Byte('0'),0,0),
    (Byte('V'),Byte(' '),Byte('5'),Byte('.'),Byte('2'),Byte('5'),0,0)
  );

//------------------------------------------------------------------------------
// Index constant
//------------------------------------------------------------------------------
// Block Bit Index
  cFFBlock                 = 0;
  cFFBlockAvailable        = 1;
  cCutFailBlock            = 2;
  cCutFailBlockAvailable   = 3;
  cClusterBlock            = 4;
  cCountBlock              = 5;
  cSFIBlock                = 6;
  cFFClusterBlock          = 7;
  cFFClusterBlockAvailable = 8;
  cBlockEnabled            = 9;
  //...........................................................................

// Other Config Bit Index
  cCfgAAdjustRemove                = 0;
  cCfgAAdjustRemoveAvailable       = 1;
  cCfgABunchMonitor                = 2;
  cCfgADriftConeChange             = 3;
  cCfgAFFClearingOnSplice          = 4;
  cCfgAFFClearingOnSpliceAvailable = 5;
  cCfgAFFDetection                 = 6;
  cCfgAFFDetectionAvailable        = 7;
  cCfgAKnifePowerHigh              = 8;
  cCfgAKnifePowerHighAvailable     = 9;
  cCfgAOneDrumPuls                 = 10;
  cCfgAOneDrumPulsAvailable        = 11;
  cCfgAZeroAdjust                  = 12;

  cCfgBFFBDDetection            = 13;
  cCfgBFFBDDetectionAvailable   = 14;
  cCfgBConeChangeCondition      = 15;
  cCfgBCutBeforeAdjust          = 16;
  cCfgBCutOnYarnBreak           = 17;
  cCfgBExtMurItf                = 18;
  cCfgBExtMurItfAvailable       = 19;
  cCfgBFFAdjAfterAlarm          = 20;
  cCfgBFFAdjAfterAlarmAvailable = 21;
  cCfgBFFAdjAtOfflimit          = 22;
  cCfgBFFAdjAtOfflimitAvailable = 23;
  cCfgBHeadstockRight           = 24;
  cCfgBKnifeMonitor             = 25;
  cCfgBUpperYarnCheck           = 26;
  //...........................................................................

// Other Sensitivity Bit Index
  cSFSSensitivity = 0;
  cDFSSensitivity = 1;
  //...........................................................................

// TMachineYMConfig: Propery index for Config Code
  cCfgA     = 0;
  cCfgB     = 1;
  cCfgC     = 2;
  cCfgMachA = 3;
  cCfgMachB = 4;
  cCfgMachC = 5;
  cCfgProdA = 6;
  cCfgProdB = 7;
  cCfgProdC = 8;
  //...........................................................................

 //TConfigurationCode: Propery index for spindle range
  cSRFrom = 1;
  cSRTo   = 2;
  //...........................................................................

  // TMachineYMConfig
  cCutRetries          = 1;
  cDefaultSpeedRamp    = 2;
  cDefaultSpeed        = 3;
  cDependendSpdleRange = 4;
  cSpeedSimulation     = 5;
  //...........................................................................

type
  // Listed group numbers
  TGroupListArr = array[0..cZESpdGroupLimit-1] of Integer;
const
  // TGroupListArr
  cNoGroup = High(TGroupListArr) + 1;
  //..............................................................................

type
  // TMachineYMConfig
  TSwitchState = (ssOn, ssOff);
  //...........................................................................

  // TConfigurationCode
  TCfgFilter = (cfNone, cfProduction, cfMachine, cfDiagnosis, cfUnused);

//------------------------------------------------------------------------------
// Parameter Source Indentifier
//------------------------------------------------------------------------------

  TParameterSource = (psUnknown,
    psChannel, // Channel Parameter Box Identifier
    psSplice, // Splice Parameter Box Identifier
    psCluster, // Cluster Parameter Box Identifier
    psYarnCount, // YarnCount Parameter Box Identifier
    psSFI, // SFI Parameter Box Identifier
    psNSLTClass, // NSLT Class Parameter Box Identifier
    psFFClass); // FF Class Parameter Box Identifier
  //...........................................................................

  TOnParameterChange = procedure (aParameterSource: TParameterSource) of object;
  //...........................................................................

//------------------------------------------------------------------------------
// Class Definition
//------------------------------------------------------------------------------
  TConfigurationCode = class(TObject)
  private
    fAWEMachType: TAWEMachType;
    fConfigA: Word;
    fConfigB: Word;
    fConfigC: Word;
    fFilter: TCfgFilter;
    fFrontType: TFrontType;
    fSensingHeadType: TSensingHead;
    function AddConfigCode(const aIndex: Integer; aMachineCfgCode, aProductionCfgCode: Word): Word;
    function CastConfigCode(const aIndex: Integer; aConfigCode1, aConfigCode2: Word): Word;
    function GetBit(const aIndex: Integer): Boolean;
    function GetBlock(const aIndex: Integer): Boolean;
    function GetConfig(const aIndex: Integer): Word;
    function GetDefault(const aIndex: Integer): Word;
    function GetFilterPattern(const aCfgCode: Integer): Word;
    function GetPattern(const aIndex: Integer): Word;
    function GetSensitivity(const aIndex: Integer): Integer;
    procedure SetBit(const aIndex: Integer; const aValue: Boolean);
    procedure SetBlock(const aIndex: Integer; const aValue: Boolean);
    procedure SetConfig(const aIndex: Integer; const aValue: Word);
    procedure SetSensingHeadType(const aValue: TSensingHead);
    procedure SetSensitivity(const aIndex, aValue: Integer);
  public
    constructor Create; overload;
    constructor Create(var aSettings: TYMSettingsRecX); overload;
    constructor Create(aConfigA, aConfigB, aConfigC: Word); overload;
    function AddConfigCodeA(aMachineCfgA, aProductionCfgA: Word): Word;
    function AddConfigCodeB(aMachineCfgB, aProductionCfgB: Word): Word;
    function AddConfigCodeC(aMachineCfgC, aProductionCfgC: Word): Word;
    function CastConfigCodeA(aConfigCode1, aConfigCode2: Word): Word;
    function CastConfigCodeB(aConfigCode1, aConfigCode2: Word): Word;
    function CastConfigCodeC(aConfigCode1, aConfigCode2: Word): Word;
    class function CompatibleConfigCodes(aCfgCA1, aCfgCB1, aCfgCC1, aCfgCA2, aCfgCB2, aCfgCC2: Word): Boolean;
    procedure SetSensingHeadDependency;
    procedure ValidateSensingHeadDependency;
  published
    property A: Word index cCfgA read GetConfig write SetConfig;
    property AdjustRemove: Boolean index cCfgAAdjustRemove read GetBit write SetBit;
    property AdjustRemoveAvailable: Boolean index cCfgAAdjustRemoveAvailable read GetBit;
    property AWEMachType: TAWEMachType read fAWEMachType write fAWEMachType;
    property B: Word index cCfgB read GetConfig write SetConfig;
    property BlockEnabled: Boolean index cBlockEnabled read GetBlock write SetBlock;
    property BunchMonitor: Boolean index cCfgABunchMonitor read GetBit write SetBit;
    property C: Word index cCfgC read GetConfig write SetConfig;
    property ClusterBlock: Boolean index cClusterBlock read GetBlock write SetBlock;
    property ConeChangeCondition: Boolean index cCfgBConeChangeCondition read GetBit write SetBit;
    property CountBlock: Boolean index cCountBlock read GetBlock write SetBlock;
    property CutBeforeAdjust: Boolean index cCfgBCutBeforeAdjust read GetBit write SetBit;
    property CutFailBlock: Boolean index cCutFailBlock read GetBlock write SetBlock;
    property CutFailBlockAvailable: Boolean index cCutFailBlockAvailable read GetBlock;
    property CutOnYarnBreak: Boolean index cCfgBCutOnYarnBreak read GetBit write SetBit;
    property DefaultA: Word index cCfgA read GetDefault;
    property DefaultB: Word index cCfgB read GetDefault;
    property DefaultC: Word index cCfgC read GetDefault;
    property DFSSensitivity: Integer index cDFSSensitivity read GetSensitivity write SetSensitivity;
    property DriftConeChange: Boolean index cCfgADriftConeChange read GetBit write SetBit;
    property ExtMurItf: Boolean index cCfgBExtMurItf read GetBit write SetBit;
    property ExtMurItfAvailable: Boolean index cCfgBExtMurItfAvailable read GetBit;
    property FFAdjAfterAlarm: Boolean index cCfgBFFAdjAfterAlarm read GetBit write SetBit;
    property FFAdjAfterAlarmAvailable: Boolean index cCfgBFFAdjAfterAlarmAvailable read GetBit;
    property FFAdjOnOfflimit: Boolean index cCfgBFFAdjAtOfflimit read GetBit write SetBit;
    property FFAdjOnOfflimitAvailable: Boolean index cCfgBFFAdjAtOfflimitAvailable read GetBit;
    property FFBDDetection: Boolean index cCfgBFFBDDetection read GetBit write SetBit;
    property FFBDDetectionAvailable: Boolean index cCfgBFFBDDetectionAvailable read GetBit;
    property FFBlock: Boolean index cFFBlock read GetBlock write SetBlock;
    property FFBlockAvailable: Boolean index cFFBlockAvailable read GetBlock;
    property FFClearingOnSplice: Boolean index cCfgAFFClearingOnSplice read GetBit write SetBit;
    property FFClearingOnSpliceAvailable: Boolean index cCfgAFFClearingOnSpliceAvailable read GetBit;
    property FFClusterBlock: Boolean index cFFClusterBlock read GetBlock write SetBlock;
    property FFClusterBlockAvailable: Boolean index cFFClusterBlockAvailable read GetBlock;
    property FFDetection: Boolean index cCfgAFFDetection read GetBit write SetBit;
    property FFDetectionAvailable: Boolean index cCfgAFFDetectionAvailable read GetBit;
    property Filter: TCfgFilter read fFilter write fFilter;
    property FilterPatternA: Word index cCfgA read GetFilterPattern;
    property FilterPatternB: Word index cCfgB read GetFilterPattern;
    property FilterPatternC: Word index cCfgC read GetFilterPattern;
    property FrontType: TFrontType read fFrontType write fFrontType;
    property HeadstockRight: Boolean index cCfgBHeadstockRight read GetBit write SetBit;
    property KnifeMonitor: Boolean index cCfgBKnifeMonitor read GetBit write SetBit;
    property KnifePowerHigh: Boolean index cCfgAKnifePowerHigh read GetBit write SetBit;
    property KnifePowerHighAvailable: Boolean index cCfgAKnifePowerHighAvailable read GetBit;
    property MachA: Word index cCfgMachA read GetConfig;
    property MachB: Word index cCfgMachB read GetConfig;
    property MachC: Word index cCfgMachC read GetConfig;
    property MachPatternA: Word index cCfgMachA read GetPattern;
    property MachPatternB: Word index cCfgMachB read GetPattern;
    property MachPatternC: Word index cCfgMachC read GetPattern;
    property OneDrumPuls: Boolean index cCfgAOneDrumPuls read GetBit write SetBit;
    property OneDrumPulsAvailable: Boolean index cCfgAOneDrumPulsAvailable read GetBit;
    property ProdA: Word index cCfgProdA read GetConfig;
    property ProdB: Word index cCfgProdB read GetConfig;
    property ProdC: Word index cCfgProdC read GetConfig;
    property ProdPatternA: Word index cCfgProdA read GetPattern;
    property ProdPatternB: Word index cCfgProdB read GetPattern;
    property ProdPatternC: Word index cCfgProdC read GetPattern;
    property SensingHead: TSensingHead read fSensingHeadType write SetSensingHeadType;
    property SFIBlock: Boolean index cSFIBlock read GetBlock write SetBlock;
    property SFSSensitivity: Integer index cSFSSensitivity read GetSensitivity write SetSensitivity;
    property UpperYarnCheck: Boolean index cCfgBUpperYarnCheck read GetBit write SetBit;
    property ZeroAdjust: Boolean index cCfgAZeroAdjust read GetBit write SetBit;
  end;
  
  //...........................................................................

  TSensingHeadClassList = class(TList)
  private
    function GetItems(Index: Integer): TSensingHeadClass;
    procedure SetItems(Index: Integer; aValue: TSensingHeadClass);
  public
    constructor Create;
    destructor Destroy; override;
    function Add(aSensingHeadClass: TSensingHeadClass): Integer;
    procedure Clear; override;
    procedure Sort;
    property Items[Index: Integer]: TSensingHeadClass read GetItems write SetItems;
  end;
  
type
  //...........................................................................
  TYMSettingsUtils = class(TObject)
  private
    class procedure ChangeEndian(var aSource, aDest: TYMSettingsRecX); overload;
    class procedure ChangeEndian(var aSettings: TYMSettingsByteArr; aSize: Word); overload;
    class procedure CompleteSettingsFromZE(var aSettings: TYMSettingsByteArr; aSize: Word; aYarnUnit: TYarnUnit; aThreadCnt: Byte);
    class procedure ConvertSettingsFromAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
    class procedure ConvertSettingsToAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
    class procedure FixAC338V510V1525SWOption(var aSettings: TYMSettingsRecX);
    class procedure FixVX12VX14FFCluster(var aSettings: TYMSettingsRecX);
    class procedure FixVX12VX14SWOption(var aSettings: TYMSettingsRecX);
    procedure GetDataRules(var aSettings: TYMSettingsRecX; var aDataRules: TYMDataRulesRec);
    class procedure GetProdGrpYMPara(var aSettings: TYMSettingsRecX; var aProdGrpYMPara: TProdGrpYMParaRec);
    class function GetSensingHeadClass(aSettings: TYMSettingsRecX): TSensingHeadClass;
    procedure InitYMSettings(var aSettings: TYMSettingsRecX);
    procedure InsertInopSettingsToSack(var aSettings: TYMSettingsByteArr; aMachinePara: TMachineYMParaRec);
    procedure PutProdGrpYMPara(var aProdGrpYMPara: TProdGrpYMParaRec; var aSettings: TYMSettingsRecX; aAssignment: Byte);
  public
    class function CheckBit(aInteger, aBitPosition: Integer): Boolean;
    class function ConvertClusterDeviation(aShortDia, aShortSwitch, aClusterDeviation: Word): Word;
    class function FitValueXX(aValue, aMin, aMax: Word): Word; overload;
    class function FitValue(aValue, aMin, aMax: Single): Single; overload;
    class function InitGroupSpecificMachConfigRec: TGroupSpecificMachConfigRec;
    class function InitMachineYMConfigRec: TMachineYMConfigRec;
    class function SetSensingHeadDependency(aSensingHead: TSensingHead; aConfigCode: Word; aType: Char): Word;
    class procedure ValidateChannelSettings(var aPara: TChannelSettingsRec);
  end;

  //.......................................................................
  TYMMachineConfig = class(TConfigurationCode)
  private
    fActiveGroup: Integer;
    fBuildMaxMachineConfig: Boolean;
    fMachineConfig: TMachineYMConfigRec;
    fMaxMachineConfig: TMachineYMConfigRec;
    fSensingHeadClassList: TSensingHeadClassList;
    fSpindle: TSpindleRangeRec;
    mRecordedMachineConfig: TMachineYMConfigRec;
    function ConvertSensingHeadClassToType(const aValue: TSensingHeadClass): TSensingHead;
    function GetAvailable(const aIndex: Integer): Boolean;
    function GetChanged: Boolean;
    function GetConfig(const aIndex: Integer): Word;
    function GetGroupSpecificConfig: TGroupSpecificMachConfigRec;
    function GetMachineConfig: TMachineYMConfigRec;
    function GetSensingHead: TSensingHead;
    function GetSensingHeadClass: TSensingHeadClass;
    procedure SetActiveGroup(const aValue: Integer);
    procedure SetBuildMaxMachineConfig(const aValue: Boolean);
    procedure SetCollectMaxMachineConfig(const aValue: TMachineYMConfigRec);
    procedure SetConfig(const aIndex: Integer; const aValue: Word);
    procedure SetGroupSpecificConfig(const aValue: TGroupSpecificMachConfigRec);
    procedure SetMachineConfig(const aValue: TMachineYMConfigRec);
    procedure SetSensingHead(const aValue: TSensingHead);
    procedure SetSensingHeadClass(const aValue: TSensingHeadClass);
    procedure UpdateMachineConfig;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function CastGroupSpecificConfig(aRange: TSpindleRangeRec): TGroupSpecificMachConfigRec;
    class function CastSensingHead(aSensingHead1, aSensingHead2: TSensingHead): TSensingHead;
    class function ConvertSensingHeadTypeToClass(const aValue: TSensingHead): TSensingHeadClass;
    function GetIntersectedGroups(aRange: TSpindleRangeRec): TGroupListArr;
    class function IndexToSensigHeadType(aIndex: Integer): TSensingHead;
    class function IndexToSwitchState(aIndex: Integer): TSwitchState;
    procedure RecordChanges;
    class function SensigHeadTypeToIndex(aSensingHead: TSensingHead): Integer;
    procedure SetDefaultMachineYMConfig;
    class function SwichStateToIndex(aSwitchState: TSwitchState): Integer;
  published
    property A: Word index cCfgA read GetConfig write SetConfig;
    property ActiveGroup: Integer read fActiveGroup write SetActiveGroup;
    property B: Word index cCfgB read GetConfig write SetConfig;
    property BuildMaxMachineConfig: Boolean read fBuildMaxMachineConfig write SetBuildMaxMachineConfig;
    property C: Word index cCfgC read GetConfig write SetConfig;
    property Changed: Boolean read GetChanged;
    property CollectMaxMachineConfig: TMachineYMConfigRec write SetCollectMaxMachineConfig;
    property CutRetriesAvailable: Boolean index cCutRetries read GetAvailable;
    property DefaultSpeedAvailable: Boolean index cDefaultSpeed read GetAvailable;
    property DefaultSpeedRampAvailable: Boolean index cDefaultSpeedRamp read GetAvailable;
    property DependendSpdleRangeAvailable: Boolean index cDependendSpdleRange read GetAvailable;
    property GroupSpecificConfig: TGroupSpecificMachConfigRec read GetGroupSpecificConfig write SetGroupSpecificConfig;
    property MachA: Word index cCfgMachA read GetConfig;
    property MachB: Word index cCfgMachB read GetConfig;
    property MachC: Word index cCfgMachC read GetConfig;
    property MachineConfig: TMachineYMConfigRec read GetMachineConfig write SetMachineConfig;
    property MaxMachineConfig: TMachineYMConfigRec read fMaxMachineConfig;
    property SensingHead: TSensingHead read GetSensingHead write SetSensingHead;
    property SensingHeadClass: TSensingHeadClass read GetSensingHeadClass write SetSensingHeadClass;
    property SensingHeadClassList: TSensingHeadClassList read fSensingHeadClassList;
    property SpeedSimulationAvailable: Boolean index cSpeedSimulation read GetAvailable;
  end;

  //...........................................................................
  TYMMachineSettings = class(TYMMachineConfig)
  private
    class function EqualFrontSWVersion(aFrontSWVersion1, aFrontSWVersion2: TYMSWVersionArr): Boolean;
    class function ExtractAWEMachineType(var aSettings: TYMSettingsRecX): TAWEMachType;
    function ExtractAWEType(var aSettings: TYMSettingsRecX): TAWEType;
    class function ExtractConfigCode(var aSettings: TYMSettingsRecX; aType: Char): Word;
    class function ExtractFrontType(var aSettings: TYMSettingsRecX): TFrontType;
    class function ExtractSensingHead(var aSettings: TYMSettingsRecX): TSensingHead;
    function ExtractYMMachineConfig(var aSettingsArr: TSettingsArr): TMachineYMConfigRec;
    procedure FilterMachineConfigCode(var aSettings: TYMSettingsRecX);
    class function FrontSWVersionIsInSet(var aSettings: TYMSettingsRecX; aFrontSWVersionSet: array of TYMSWVersionArr): Boolean;
    procedure InsertYMMachineConfig(aMachineYMConfig: TMachineYMConfigRec; var aSettings: TYMSettingsRecX);
    procedure UpdateYMMachineConfig(var aMachineYMConfig: TMachineYMConfigRec); overload;
    procedure UpdateYMMachineConfig(var aSettings: TYMSettingsRecX); overload;
    function YMMachineConfigAvailable(var aSettingsArr: TSettingsArr): Boolean;
  public
  end;

  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
function CalculateCountFromDiaDiff(aMinMaxDia, aCount: Single; aByLength: Boolean): Single;
function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): Boolean;
function SensingHeadClassCompare(aItem1, aItem2: Pointer): Integer;
function SensingHeadTypeCompare(aItem1, aItem2: Pointer): Integer;
function ValidateSensingHeadType(aValue: TSensingHead): TSensingHead;

function CalculateDiaDiffFromCounts(aCorseFine, aCount: Single; aByLength: Boolean): Single;

function GetFPattern(aObserver: TBaseXMLSettingsObserver; aXPath: String): Boolean;

// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetSensingHeadClass(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): TSensingHeadClass; overload;

function GetSensingHeadClass(aModel: TXMLSettingsModel): TSensingHeadClass;
    overload;

function IsFSensingHead(aModel: TXMLSettingsModel): Boolean;

procedure SetSensingHeadClass(aModel: TXMLSettingsModel; aSensingHeadClass: TSensingHeadClass);

procedure SwapClassFields(aNode: IXMLDOMNode);

// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEClass(aMaConfigReader: TMaConfigReader; aGroupIndex: Integer):
    TAWEClass;

function GetSensingHeadClass(aSensingHead: TSensingHead): TSensingHeadClass; overload;

function IsZenit(aModel: TXMLSettingsModel): Boolean;

function FitValue(aValue, aMin, aMax: Single): Single;


  //...........................................................................

//------------------------------------------------------------------------------
// Free functions
//------------------------------------------------------------------------------
implementation
uses
  mmMBCS;
//------------------------------------------------------------------------------
const
  cBit0: Integer = 1;

type
  //...........................................................................

  TMachineAttributes = class(TObject)
  private
    fInopSettings0: DWord;
    fInopSettings1: DWord;
    class procedure FixAC338V525InopSettings(var aSettings: TYMSettingsRecX);
    class procedure FixMurIndInvInopSettings(var aSettings: TYMSettingsRecX);
    function GetFFSensingHead: TSensingHead;
    function GetIsFFActive: Boolean;
    function GetIsFFCluster: Boolean;
    function GetIsFFSensingHead: Boolean;
    function GetIsMMMachineConfig: Boolean;
    function GetIsPlus: Boolean;
    function GetIsSensingHeadFType: Boolean;
    function GetIsSFI: Boolean;
    function GetIsSpectra: Boolean;
    function GetIsSpeedSimulation: Boolean;
    function GetIsUpperYarn: Boolean;
    class function InopSettingsAvailable(var aSettings: TYMSettingsRecX): Boolean;
    class procedure SetDefaultInopSettings(var aSettings: TYMSettingsRecX);
  public
    constructor Create(aInopSettings0: DWord; aInopSettings1: DWord); overload;
    constructor Create(var aSettings: TYMSettingsRecX); overload;
    class function CastInopSettings0(aInopSettings01, aInopSettings02: DWord): DWord;
    class function CastInopSettings1(aInopSettings11, aInopSettings12: DWord; aSensingHead: TSensingHead): DWord;
    class function CompatibleInopSettings(aInopSettings01, aInopSettings11, aInopSettings02, aInopSettings12: DWord): Boolean;
    procedure EnableAll;
    class function SetInopSettings1FFSensingHead(aSensingHead: TSensingHead; aInopSettings1: DWord): DWord;
    property InopSettings0: DWord read fInopSettings0 write fInopSettings0;
    property InopSettings1: DWord read fInopSettings1 write fInopSettings1;
    property IsFFActive: Boolean read GetIsFFActive;
    property IsFFCluster: Boolean read GetIsFFCluster;
    property IsFFSensingHead: Boolean read GetIsFFSensingHead;
    property IsMMMachineConfig: Boolean read GetIsMMMachineConfig;
    property IsPlus: Boolean read GetIsPlus;
    property IsSensingHeadFType: Boolean read GetIsSensingHeadFType;
    property IsSFI: Boolean read GetIsSFI;
    property IsSpectra: Boolean read GetIsSpectra;
    property IsSpeedSimulation: Boolean read GetIsSpeedSimulation;
    property IsUpperYarn: Boolean read GetIsUpperYarn;
    property SensingHead: TSensingHead read GetFFSensingHead;
  end;
  
//:-----------------------------------------------------------------------------
function IsFSensingHead(aModel: TXMLSettingsModel): Boolean;
begin
  Result := Boolean(aModel.FPHandler.Value[cXPFP_FSpectraNormalItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FSpectraHighItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FSpectraBDItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FZenitDarkItem]) or
            Boolean(aModel.FPHandler.Value[cXPFP_FZenitBrightItem]);
end;
//:-----------------------------------------------------------------------------
// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetSensingHeadClass(aMaConfigReader: TXMLMaConfigHelper; aGroupIndex: Integer): TSensingHeadClass;
var
  xSensingHead: TSensingHead;
begin
  xSensingHead := GetSensingHeadValue(aMaConfigReader.GroupValueDef[aGroupIndex, cXPSensingHeadItem, cSensingHeadNames[shTK830]]);
  Result := GetSensingHeadClass(xSensingHead);
//  if xSensingHead in [shTKZenit] then
//    Result := shcZenit
//  else if xSensingHead in [shTKZenitF] then
//    Result := shcZenitF
//  else if xSensingHead in [shTKZenitFP] then
//    Result := shcZenitFP
//  else if xSensingHead in [shTK930F, shTK940F, shTK930S] then
//    Result := shc9xFS
//  else if xSensingHead in [shTK930H] then
//    Result := shc9xH
//  else if xSensingHead in [shTK940BD] then
//    Result := shc9xBD
//  else
//    Result := shc8x;
end;
//:-----------------------------------------------------------------------------
function GetSensingHeadClass(aModel: TXMLSettingsModel): TSensingHeadClass;
begin
  if Boolean(aModel.FPHandler.Value[cXPFP_FSpectraNormalItem]) then
    Result := shc9xFS
  else if Boolean(aModel.FPHandler.Value[cXPFP_FSpectraHighItem]) then
    Result := shc9xH
  else if Boolean(aModel.FPHandler.Value[cXPFP_FSpectraBDItem]) then
    Result := shc9xBD
  else if Boolean(aModel.FPHandler.Value[cXPFP_FZenitDarkItem]) or
          Boolean(aModel.FPHandler.Value[cXPFP_FZenitBrightItem]) then begin
    if Boolean(aModel.FPHandler.Value[cXPFP_PItem]) then
      Result := shcZenitFP
    else
      Result := shcZenitF;
  end
  else if Boolean(aModel.FPHandler.Value[cXPFP_ZenitItem]) then
    Result := shcZenit
  else
    Result := shc8x;
end;

//:-----------------------------------------------------------------------------
function CalculateCountFromDiaDiff(aMinMaxDia, aCount: Single; aByLength: Boolean): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    if aByLength then Result := (aMinMaxDia * aMinMaxDia) * aCount
                 else Result := aCount / (aMinMaxDia * aMinMaxDia);

  except
    on e: Exception do
      raise Exception.Create('CalculateCountFromDiaDiff failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

function CalculateCountByWeight(aMinMaxDia, aCount: Single): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    Result     := aCount / (aMinMaxDia * aMinMaxDia);
  except
    on e: Exception do
      raise Exception.Create('CalculateCountByWeight failed: ' + e.Message);
  end;

end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByLength(aCorseFine, aCount: Single): Single;
begin
  try
    Result := Sqrt(aCorseFine / aCount);
    if Result < 1 then Result := 1 - Result
                  else Result := Result - 1;
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    Result := Result * 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffByLength failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

function CalculateDiaDiffByWeight(aCorseFine, aCount: Single): Single;
begin
  try
    Result := Sqrt(aCount / aCorseFine);
    if Result < 1 then Result := 1 - Result
                  else Result := Result - 1;
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    Result := Result * 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffByWeight failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function FitValue(aValue, aMin, aMax: Single): Single;
begin
  Result := aValue;
  if (aMax > aMin) then begin
    if aValue < aMin then
      Result := aMin
    else if aValue > aMax then
      Result := aMax;
  end;
end;
//------------------------------------------------------------------------------
function GetAvailabilityOfDataItem(aVoidData: DWord; aDataItem: TVoidDataItem): Boolean;
begin
  case aDataItem of
    vdCones: Result  := false;
    vdCops: Result   := false;
    vdIPOld: Result  := false;
    vdIPNew: Result  := true;
    vdIPPlus: Result := false;
    vdSFIVar: Result := true;
    vdSFICnt: Result := true;
  else
    Result := true;
  end;
end;
//------------------------------------------------------------------------------
function SensingHeadClassCompare(aItem1, aItem2: Pointer): Integer;
begin
    Result := 0;
{wss
  if TSensingHeadClass(aItem1^) < TSensingHeadClass(aItem2^) then
    Result := 1
  else if TSensingHeadClass(aItem1^) > TSensingHeadClass(aItem2^) then
    Result := -1
  else
    Result := 0;
{}
end;
//------------------------------------------------------------------------------
function SensingHeadTypeCompare(aItem1, aItem2: Pointer): Integer;
begin
    Result := 0;
{wss
  if cSensingHeadTypeOrder[TSensingHead(aItem1)] < cSensingHeadTypeOrder[TSensingHead(aItem2)] then
    Result := -1
  else if cSensingHeadTypeOrder[TSensingHead(aItem1)] > cSensingHeadTypeOrder[TSensingHead(aItem2)] then
    Result := 1
  else
    Result := 0;
{}
end;
//------------------------------------------------------------------------------
procedure SwapClassFields(aNode: IXMLDOMNode);
  //........................................................
  function SwapFields(aClassString: String): String;
  var
    xSrcIndex: Integer;
    xDstIndex: Integer;
    xBaseIndex: Integer;
    xSrcLength: Integer;
    xDestStrArr: Array of Char;
  begin
    xSrcLength := Length(aClassString);
    // Src String muss exakt durch 8 teilbar sein sonst ist es kein g�ltige Klassdefinition
    if (xSrcLength = 64) or (xSrcLength = 128) then begin
      SetLength(xDestStrArr, xSrcLength+1);
      xSrcIndex := 0;
      while xSrcIndex < xSrcLength do begin
        xBaseIndex := (xSrcIndex div 64 ) * 64;
        xDstIndex  := xBaseIndex + (xSrcIndex mod 8) * 8 + ((xSrcIndex-xBaseIndex) div 8);
        xDestStrArr[xDstIndex] := aClassString[xSrcIndex+1];
        inc(xSrcIndex);
      end;
      Result := String(xDestStrArr);
    end else
      Result := aClassString;
  end;
  //........................................................
begin
  if Assigned(aNode) then begin
    if aNode.childNodes.Length = 1 then  //Es darf nur einen Wert pro Element im MMXML haben
      if aNode.childNodes.item[0].nodeValue <> NULL then
        aNode.childNodes.item[0].nodeValue := SwapFields(aNode.childNodes.item[0].nodeValue);
  end;
end;
//------------------------------------------------------------------------------
function ValidateSensingHeadType(aValue: TSensingHead): TSensingHead;
begin
    Result := aValue;
{wss
  if aValue < TSensingHead(cFirstSensingHead) then
    Result := TSensingHead(cFirstSensingHead)
  else if aValue > TSensingHead(cLastSensingHead) then
      Result := TSensingHead(cLastSensingHead)
  else
    Result := aValue;
{}
end;
//------------------------------------------------------------------------------
function CalculateDiaDiffFromCounts(aCorseFine, aCount: Single; aByLength: Boolean): Single;
begin
  try
    // Bei der Berechnung kommt ein Faktor z.B 0.09 heraus, ben�tigt wird aber Prozent 9%
    // Daher die Multiplikation * 100
    if aByLength then Result := Sqrt(aCorseFine / aCount) * 100
                 else Result := Sqrt(aCount / aCorseFine) * 100;

    if Result < 100 then Result := 100 - Result
                    else Result := Result - 100;
  except
    on e: Exception do
      raise Exception.Create('CalculateDiaDiffFromCounts failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function CalculateCountByLength(aMinMaxDia, aCount: Single): Single;
begin
  try
    // aMinMaxData ist in Prozent, hier muss aber mit dem Faktor gerechnet werden 90% -> 0.9
    aMinMaxDia := aMinMaxDia / 100;
    Result     := (aMinMaxDia * aMinMaxDia) * aCount;
  except
    on e: Exception do
      raise Exception.Create('CalculateCountByLength failed: ' + e.Message);
  end;
end;

function GetFPattern(aObserver: TBaseXMLSettingsObserver; aXPath: String): Boolean;
begin
  if aObserver.Model.MaConfigReader.Available then
    Result := aObserver.Model.MaConfigReader.FPHandler.Value[aXPath]
  else
    Result := aObserver.ValueDef[aXPath, False];
end;

//:-----------------------------------------------------------------------------
procedure SetSensingHeadClass(aModel: TXMLSettingsModel; aSensingHeadClass: TSensingHeadClass);
begin
  aModel.FPHandler.Value[cXPFP_FSpectraNormalItem] := (aSensingHeadClass = shc9xFS);
  aModel.FPHandler.Value[cXPFP_FSpectraHighItem]   := (aSensingHeadClass = shc9xH);
  aModel.FPHandler.Value[cXPFP_FSpectraBDItem]     := (aSensingHeadClass = shc9xBD);

  aModel.FPHandler.Value[cXPFP_ZenitItem]          := (aSensingHeadClass in [shcZenit, shcZenitF, shcZenitFP]);

  aModel.FPHandler.Value[cXPFP_FZenitDarkItem]     := (aSensingHeadClass in [shcZenitF, shcZenitFP]);
  aModel.FPHandler.Value[cXPFP_FZenitBrightItem]   := (aSensingHeadClass in [shcZenitF, shcZenitFP]);

  aModel.FPHandler.Value[cXPFP_PItem]              := (aSensingHeadClass = shcZenitFP);
end;

//:-----------------------------------------------------------------------------
// Gibt die Tastkopfklasse der aktuellen Maschinengruppe zur�ck (Nue)
function GetAWEClass(aMaConfigReader: TMaConfigReader; aGroupIndex: Integer):
    TAWEClass;
begin
{ TODO 1 -cLOK/Nue : Aus dem FPAttern die AWE-Klasse extrahieren! }
  Assert(false, 'Noch nicht implementiert'); 
  Result := aweSpectra;  //Tempor�rer Defaultwert
end;

//:-----------------------------------------------------------------------------
function GetSensingHeadClass(aSensingHead: TSensingHead): TSensingHeadClass;
begin
  case aSensingHead of
    shNone:                       Result := shcNone;
    shTK930F, shTK930S, shTK940F: Result := shc9xFS;
    shTK930H:                     Result := shc9xH;
    shTK940BD:                    Result := shc9xBD;
    shTKZenit:                    Result := shcZenit;
    shTKZenitF:                   Result := shcZenitF;
    shTKZenitFP:                  Result := shcZenitFP;
  else // shTK830, shTK840, shTK850, shTK870, shTK880
    Result := shc8x;
  end;
end;

//:-----------------------------------------------------------------------------
function IsZenit(aModel: TXMLSettingsModel): Boolean;
begin
  Result := Boolean(aModel.FPHandler.Value[cXPFP_ZenitItem]);
end;

//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TYMSettingsUtils
//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.ChangeEndian(var aSettings: TYMSettingsByteArr; aSize: Word);
begin
{TODO: TYMSettingsRec Obsolet
  if aSize >= SizeOf(TYMSettingsRec) then
    ChangeEndian(PYMSettings(@aSettings)^, PYMSettings(@aSettings)^);
{}
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.ChangeEndian(var aSource, aDest: TYMSettingsRecX);
begin
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.CheckBit(aInteger, aBitPosition: Integer): Boolean;
var
  xMask: Integer;
begin
  xMask := cBit0 shl aBitPosition;
  Result := (xMask = (xMask and aInteger));
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.CompleteSettingsFromZE(var aSettings: TYMSettingsByteArr; aSize: Word; aYarnUnit: TYarnUnit; aThreadCnt: Byte);
begin
{TODO: TYMSettingsRec Obsolet
  if aSize >= SizeOf(TYMSettingsRec) then begin
    with PYMSettings(@aSettings)^ do begin
      additional.option.yarnUnit  := Word(aYarnUnit);
      additional.option.threadCnt := Word(aThreadCnt);
    end;
  end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.ConvertClusterDeviation(aShortDia, aShortSwitch, aClusterDeviation: Word): Word;
var
  xDDeviation: Double;
begin
// TODO wss: auf single typ berechnen?
  if aShortSwitch = cChOff then
    Result := 0
  else if aClusterDeviation < 100 then begin
    xDDeviation := aClusterDeviation;
    Result      := Round(xDDeviation * (aShortDia - 100) / 100) + 100;
  end
  else
    Result := aShortDia;
end;

//:---------------------------------------------------------------------------
// TODO wss: wird f�r XML nicht mehr ben�tigt
class procedure TYMSettingsUtils.ConvertSettingsFromAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
begin
(*TODO: TYMSettingsRec Obsolet
  if aSize >= SizeOf(TYMSettingsRec) then begin
    with PYMSettings(@aSettings)^ do begin
      with additional.option do begin
        {
        // WSCGarneinheit in MMStandarteinheit(Nm)konvertieren
        try
          yarnCnt := Round(Yarncountconvert(TYarnUnit(yarnUnit),yuNm,yarnCnt,10));
        except
        end;
        yarnUnit:= WORD(yuNM);
        }
      // WSCGarnNr = EinzelfadenNr,
      // LoepfeGarnNr = GesamtfadenNr
      // LoepfeGarnNr = WSCGarnNr DIV fadenzahl
        if threadCnt > 1 then
          yarnCnt := Round(yarnCnt / threadCnt);
      end;
      // Thin Diameter Conversion
      channel.thin.dia                 := 100 - channel.thin.dia;
      splice.thin.dia                  := 100 - splice.thin.dia;

      // Repetition Conversion
      additional.option.cntRep         := additional.option.cntRep + 1;
      additional.option.siroStartupRep := additional.option.siroStartupRep + 1;
      additional.extOption.clusterRep  := additional.extOption.clusterRep + 1;
      additional.extOption.cutRetries  := additional.extOption.cutRetries + 1;
      fFCluster.rep                    := fFCluster.rep + 1;
      sFI.rep                          := sFI.rep + 1;

      // Data Record Mode
      if machSet.lengthMode < cDRMFirst then
        machSet.lengthMode := machSet.lengthMode + cDRMFirst;

      // longStopDef AC338 is using Min./10, (the YM standard is seconds)
      machSet.longStopDef := machSet.longStopDef * 6;

      // InopSettings
      if not TMachineAttributes.InopSettingsAvailable(PYMSettings(@aSettings)^) then
        TMachineAttributes.SetDefaultInopSettings(PYMSettings(@aSettings)^);

      TYMSettingsUtils.FixAC338V510V1525SWOption(PYMSettings(@aSettings)^);
      TMachineAttributes.FixAC338V525InopSettings(PYMSettings(@aSettings)^);
    end;
  end;
(**)
end;

//:---------------------------------------------------------------------------
// TODO wss: wird f�r XML nicht mehr ben�tigt
class procedure TYMSettingsUtils.ConvertSettingsToAC338(var aSettings: TYMSettingsByteArr; aSize: Word);
begin
(*TODO: TYMSettingsRec Obsolet
  if aSize >= SizeOf(TYMSettingsRec) then
  begin
    with PYMSettings(@aSettings)^ do
    begin
  
    // Thin Diameter Conversion
      channel.thin.dia := 100 - channel.thin.dia;
      splice.thin.dia := 100 - splice.thin.dia;
  
    // Repetition Conversion
      additional.option.cntRep := additional.option.cntRep - 1;
      additional.option.siroStartupRep := additional.option.siroStartupRep - 1;
      additional.extOption.clusterRep := additional.extOption.clusterRep - 1;
      additional.extOption.cutRetries := additional.extOption.cutRetries - 1;
      fFCluster.rep := fFCluster.rep - 1;
      sFI.rep := sFI.rep - 1;
  
    // Data Record Mode
      if machSet.lengthMode >= cDRMFirst then
        machSet.lengthMode := machSet.lengthMode - cDRMFirst;

    // longStopDef AC338 is using Min./10, (the YM standard is seconds)
      machSet.longStopDef := machSet.longStopDef div 6;
  
    end;
  end;
(**)
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.FitValueXX(aValue, aMin, aMax: Word): Word;
begin
  Result := aValue;
  if (aMax > aMin) then begin
    if aValue < aMin then
      Result := aMin
    else if aValue > aMax then
      Result := aMax;
  end;
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.FitValue(aValue, aMin, aMax: Single): Single;
begin
  Result := aValue;
  if (aMax > aMin) then begin
    if aValue < aMin then
      Result := aMin
    else if aValue > aMax then
      Result := aMax;
  end;
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.FixAC338V510V1525SWOption(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  if TYMMachineSettings.FrontSWVersionIsInSet(aSettings, cFSWVSWOptionNotAvailable) then
    aSettings.available.swOption := Ord(fSwOEnd)
{}
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.FixVX12VX14FFCluster(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  if TYMMachineSettings.FrontSWVersionIsInSet(aSettings, cFSWVFFClusterNotSwapped) then
  begin

    with aSettings.FFCluster do
    begin
      obsLength := Swap(obsLength);
      defects := Swap(defects);
    end;
  end;
}
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.FixVX12VX14SWOption(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  if TYMMachineSettings.FrontSWVersionIsInSet(aSettings, cFSWVFFClusterNotSwapped) then
    aSettings.available.swOption := Ord(fSwOEnd);
{}  
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsUtils.GetDataRules(var aSettings: TYMSettingsRecX; var aDataRules: TYMDataRulesRec);
begin
{TODO: TYMSettingsRec Obsolet
  with aDataRules, aSettings do
  begin
    voidData := available.voidData;
  end;
{}
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.GetProdGrpYMPara(var aSettings: TYMSettingsRecX; var aProdGrpYMPara: TProdGrpYMParaRec);
begin
{TODO: TYMSettingsRec Obsolet
  with aProdGrpYMPara, aSettings do
  begin
    group         := available.group;
    prodGrpID     := available.prodGrpID;
    spdl.contents := available.spdl.contents;
    pilotSpindles := available.pilotSpindles;
    speedRamp     := available.speedRamp;
    speed         := channel.speed;
    yarnCnt       := additional.option.yarnCnt;
    yarnCntUnit   := additional.option.yarnUnit;
    nrOfThreads   := additional.option.threadCnt;
    lengthWindow  := machSet.lengthWindow;
    lengthMode    := machSet.lengthMode;
  end;
{}  
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.GetSensingHeadClass(aSettings: TYMSettingsRecX): TSensingHeadClass;
var
  xMachineAttributes: TMachineAttributes;
begin
{TODO: TYMSettingsRec Obsolet
  xMachineAttributes := TMachineAttributes.Create(aSettings);
  Result := TYMMachineConfig.ConvertSensingHeadTypeToClass(xMachineAttributes.SensingHead);
  xMachineAttributes.Free;
{}  
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.InitGroupSpecificMachConfigRec: TGroupSpecificMachConfigRec;
begin
{wss
  with Result do
  begin
    spindle.contents   := cSpdlRngNotDefined;
    sensingHead        := htTK830;
    aWEType            := atUnknown;
    configA            := 0;
    configB            := 0;
    configC            := 0;
    inoperativePara[0] := 0;
    inoperativePara[1] := 0;
  end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.InitMachineYMConfigRec: TMachineYMConfigRec;
var
  i: Integer;
begin
{TODO: TYMSettingsRec Obsolet
  for i := 1 to High(Result.spec) do
    Result.spec[i] := InitGroupSpecificMachConfigRec;

  // TODO XML: checkLen auf Single wechseln
  with Result do begin
    checkLen           := Round(cSpliceCheckDefault * 10);
//    checkLen           := cDefaultSpliceCheck;
    cutRetries         := cDefaultRepetitions;
    longStopDef        := cInitLongStopDef * 6;
    defaultSpeedRamp   := 0;
    defaultSpeed       := cDefaultSpeed;
    frontType          := ftNone;
    frontSWOption      := fSwOBasic;
    frontSWVersion[0]  := 0;
    aWEMachType        := amtUnknown;
    machBez[0]         := 0;
    inoperativePara[0] := 0;
    inoperativePara[1] := 0;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsUtils.InitYMSettings(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  FillChar(aSettings, SizeOf(aSettings), 0);
  aSettings.channel.splice.sw := cChExtSp;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsUtils.InsertInopSettingsToSack(var aSettings: TYMSettingsByteArr; aMachinePara: TMachineYMParaRec);
begin
{TODO: TYMSettingsRec Obsolet
  PYMSettings(@aSettings).available.inoperativePara[0] := aMachinePara.inoperativePara[0];
  PYMSettings(@aSettings).available.inoperativePara[1] := aMachinePara.inoperativePara[1];
{}
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsUtils.PutProdGrpYMPara(var aProdGrpYMPara: TProdGrpYMParaRec; var aSettings: TYMSettingsRecX; aAssignment: Byte);
begin
{TODO: TYMSettingsRec Obsolet
  with aProdGrpYMPara, aSettings do
  begin
    available.assignement := aAssignment;
    available.group := group;
    available.prodGrpID := prodGrpID;
    available.spdl.contents := spdl.contents;
    available.pilotSpindles := pilotSpindles;

    available.speedRamp := speedRamp;
    channel.speed := speed;

    additional.option.yarnCnt := yarnCnt;
    additional.option.threadCnt := nrOfThreads;
    additional.option.yarnUnit := yarnCntUnit;

    machSet.lengthWindow := lengthWindow;
    machSet.lengthMode := lengthMode;

  (*    if TYarnUnit(yarnCntUnit) in cYarnUnitsByLength then begin
        additional.option.minCnt := Word(CalculateCountByLength(
                                    1000+additional.option.diaDiff, yarnCnt));
        additional.option.maxCnt := Word(CalculateCountByLength(
                                    1000-additional.extOption.negDiaDiff, yarnCnt));
      end
      else begin
        additional.option.minCnt := Word(CalculateCountByWeight(
                                    1000+additional.option.diaDiff, yarnCnt));
        additional.option.maxCnt := Word(CalculateCountByWeight(
                                    1000-additional.extOption.negDiaDiff, yarnCnt));
      end;
  *)

  end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMSettingsUtils.SetSensingHeadDependency(aSensingHead: TSensingHead; aConfigCode: Word; aType: Char): Word;
begin
Result := 0;
{wss
  Result := aConfigCode;

  aSensingHead := ValidateSensingHeadType(aSensingHead);

  if aSensingHead in cOtherSensingHeadTypes then

    case aType of
      'A':
        Result := aConfigCode and not (cCCAFFSensorActiveBit);
          // or cCCAFFBlockDisabledBit;

      'B':
        Result := aConfigCode and not (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit or cCCBFFAdjAfterAlarmBit);
  
  //      'C':
  //        Result := aConfigCode and not(cCCCFFClusterAlarmBlockBit);
    end
  else if aSensingHead in cFFSensingHeadTypes then
  
    case aType of
      'A':
        Result := aConfigCode or (cCCAFFSensorActiveBit or cCCANoFFClearingOnSpliceBit);
  //        Result := aConfigCode or cCCAFFSensorActiveBit;
          // and not cCCAFFBlockDisabledBit;
  
      'B':
        begin
  
          if aSensingHead = htTK940BD then
            Result := aConfigCode or (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit)
  
          else
            Result := aConfigCode and not (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit);
        end;
  //    'C':
  //        Result := aConfigCode or (cCCCFFClusterAlarmBlockBit);
    end;
{}
end;

//:---------------------------------------------------------------------------
class procedure TYMSettingsUtils.ValidateChannelSettings(var aPara: TChannelSettingsRec);
  
  function ValidateSwitch(aSwitch: Word): Word;
  begin
    if aSwitch <> cChOn then
      Result := cChOff
    else
      Result := cChOn;
  end;
  
begin
// TODO XML: brauchts diese Routine noch?
{
  with aPara do
  begin
  // Neps
    neps.dia := FitValue(neps.dia, cMinNepsDia, cMaxNepsDia);
    neps.sw := ValidateSwitch(neps.sw);
  // Short
    short.dia := FitValue(short.dia, cMinShortDia, cMaxShortDia);
    short.sw := ValidateSwitch(short.sw);
    shortLen := FitValue(shortLen, cMinShortLen, cMaxShortLen);
  // Long
    long.dia := FitValue(long.dia, cMinLongDia, cMaxLongDia);
    long.sw := ValidateSwitch(long.sw);
    longLen := FitValue(longLen, cMinLongLen, cMaxLongLen);
  // Thin
    thinLen := FitValue(thinLen, cMinThinLen, cMaxThinLen);
    if thinLen >= cThinLenThreshold then
      thin.dia := FitValue(thin.dia, cMinThinDia, cMaxThinDiaAbove)
    else
      thin.dia := FitValue(thin.dia, cMinThinDia, cMaxThinDiaBelow);
    thin.sw := ValidateSwitch(thin.sw);
  end;
{}
end;


//:---------------------------------------------------------------------------
//:--- Class: TMachineAttributes
//:---------------------------------------------------------------------------
constructor TMachineAttributes.Create(aInopSettings0: DWord; aInopSettings1: DWord);
begin
  inherited Create;
  InopSettings0 := aInopSettings0;
  InopSettings1 := aInopSettings1;
end;

//:---------------------------------------------------------------------------
constructor TMachineAttributes.Create(var aSettings: TYMSettingsRecX);
begin
  inherited Create;
{TODO: TYMSettingsRec Obsolet
  InopSettings0 := aSettings.available.inoperativePara[0];
  InopSettings1 := aSettings.available.inoperativePara[1];
{}
end;

//:---------------------------------------------------------------------------
class function TMachineAttributes.CastInopSettings0(aInopSettings01, aInopSettings02: DWord): DWord;
begin
  
  Result := aInopSettings01 and aInopSettings02;
end;

//:---------------------------------------------------------------------------
class function TMachineAttributes.CastInopSettings1(aInopSettings11, aInopSettings12: DWord; aSensingHead: TSensingHead): DWord;
begin
  
  Result := SetInopSettings1FFSensingHead(
    aSensingHead, (aInopSettings11 and aInopSettings12));
end;

//:---------------------------------------------------------------------------
class function TMachineAttributes.CompatibleInopSettings(aInopSettings01, aInopSettings11, aInopSettings02, aInopSettings12: DWord): Boolean;
begin
  Result := (aInopSettings11 and cIP1MFFSensingHeads) =
    (aInopSettings12 and cIP1MFFSensingHeads);
end;

//:---------------------------------------------------------------------------
procedure TMachineAttributes.EnableAll;
begin
  InopSettings0 := InopSettings0 or cIP0MAll;
  InopSettings1 := InopSettings1 or cIP1MAll;
end;

//:---------------------------------------------------------------------------
class procedure TMachineAttributes.FixAC338V525InopSettings(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  if TYMMachineSettings.FrontSWVersionIsInSet(aSettings, cFSWVInopSettingsWrongFFTK) then
    with aSettings.available do
      inoperativePara[1] := SetInopSettings1FFSensingHead(
        TYMMachineSettings.ExtractSensingHead(aSettings), inoperativePara[1]);
{}
end;

//:---------------------------------------------------------------------------
class procedure TMachineAttributes.FixMurIndInvInopSettings(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  if TYMMachineSettings.ExtractAWEMachineType(aSettings) = amtMUR_IND_INV then
    with aSettings.available do
      inoperativePara[0] := inoperativePara[0] or cIP0MUpperYarnChannel;
{}
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetFFSensingHead: TSensingHead;
begin
  Result := shTK930F;
{wss
  xScratch := InopSettings1 and cIP1MFFSensingHeads;
  xScratch := xScratch shr 8;
  Result := ValidateSensingHeadType(TSensingHead(xScratch + cFirstFFSensingHead - 1));
{}
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsFFActive: Boolean;
begin
  
  if (IsFFSensingHead or
    (cIP1MFFClearing = (InopSettings1 and cIP1MFFClearing))) and
    (cIP1MFFSensorEnabled = (InopSettings1 and cIP1MFFSensorEnabled)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsFFCluster: Boolean;
begin
  
  if IsFFActive and
    (cIP1MFFCluster = (InopSettings1 and cIP1MFFCluster)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsFFSensingHead: Boolean;
begin
    Result := False;
{wss
  if (GetFFSensingHead in cFFSensingHeadTypes) then
    Result := True
  else
    Result := False;
{}
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsMMMachineConfig: Boolean;
begin
  
  if (cIP0MMMMachineConfig = (InopSettings0 and cIP0MMMMachineConfig)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsPlus: Boolean;
begin
  if (cIP0MOffCountNegDiaDff = (InopSettings0 and cIP0MOffCountNegDiaDff)) or
     (cIP0MOffCountLength = (InopSettings0 and cIP0MOffCountLength)) or
     (cIP0MCutRetries = (InopSettings0 and cIP0MCutRetries)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsSensingHeadFType: Boolean;
begin
    Result := False;
{wss
  if GetFFSensingHead in cFFSensingHeadFTypes then
    Result := True
  else
    Result := False;
{}
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsSFI: Boolean;
begin
  if (cIP0MSFIMonitor = (InopSettings0 and cIP0MSFIMonitor)) or
     (cIP0MSFIRepetitons = (InopSettings0 and cIP0MSFIRepetitons)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsSpectra: Boolean;
begin
  
  Result := GetIsSFI;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsSpeedSimulation: Boolean;
begin
  
  if (cIP0MSpeed = (InopSettings0 and cIP0MSpeed)) or
    (cIP0MSpeedramp = (InopSettings0 and cIP0MSpeedramp)) then
    Result := True
  else
    Result := False;
end;

//:---------------------------------------------------------------------------
function TMachineAttributes.GetIsUpperYarn: Boolean;
begin
  Result := (InopSettings0 and cIP0MUpperYarnChannel) = cIP0MUpperYarnChannel;

//  if (cIP0MUpperYarnChannel = (InopSettings0 and cIP0MUpperYarnChannel)) then
//    Result := True
//  else
//    Result := False;
end;

//:---------------------------------------------------------------------------
class function TMachineAttributes.InopSettingsAvailable(var aSettings: TYMSettingsRecX): Boolean;
begin
{TODO: TYMSettingsRec Obsolet
  with aSettings.available do begin
    if TYMMachineSettings.ExtractAWEMachineType(aSettings) = amtAC338 then
      Result := not TYMMachineSettings.FrontSWVersionIsInSet(aSettings, cFSWVInopSettingsNotAvailable)
    else
      Result := (inoperativePara[0] <> 0) or (inoperativePara[1] <> 0);
  end;
{}
end;

//:---------------------------------------------------------------------------
class procedure TMachineAttributes.SetDefaultInopSettings(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  with aSettings.available do begin
    if TYMMachineSettings.ExtractAWEMachineType(aSettings) = amtAWE_SS then
      inoperativePara[0] := cIP0MSSDefault
    else
      inoperativePara[0] := cIP0MDefault;

    if TYMMachineSettings.ExtractSensingHead(aSettings) in cFFSensingHeadTypes then begin
      inoperativePara[1] := cIP1MFFDefault;
      inoperativePara[1] := SetInopSettings1FFSensingHead(TYMMachineSettings.ExtractSensingHead(aSettings), inoperativePara[1]);
    end
    else
      inoperativePara[1] := cIP1MDefault;
  end;
{}
end;

//:---------------------------------------------------------------------------
class function TMachineAttributes.SetInopSettings1FFSensingHead(aSensingHead: TSensingHead; aInopSettings1: DWord): DWord;
begin
    Result := aInopSettings1;
{wss
  aSensingHead   := ValidateSensingHeadType(aSensingHead);
  aInopSettings1 := aInopSettings1 and not cIP1MFFSensingHeads;

  if Ord(aSensingHead) >= cFirstFFSensingHead then begin
    xScratch := Ord(aSensingHead) - cFirstFFSensingHead + 1;
    xScratch := xScratch shl 8;
    Result   := (xScratch and cIP1MFFSensingHeads) or aInopSettings1;
  end
  else
    Result := aInopSettings1;
{}
end;


//:---------------------------------------------------------------------------
//:--- Class: TConfigurationCode
//:---------------------------------------------------------------------------
constructor TConfigurationCode.Create;
begin
  inherited;

  fConfigA := 0;
  fConfigB := 0;
  fConfigC := 0;

  fAWEMachType     := amtUnknown;
  fFilter          := cfNone;
  fFrontType       := ftNone;
  fSensingHeadType := shTK830;
end;

//:---------------------------------------------------------------------------
constructor TConfigurationCode.Create(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  fConfigA := TYMMachineSettings.ExtractConfigCode(aSettings, 'A');
  fConfigB := TYMMachineSettings.ExtractConfigCode(aSettings, 'B');
  fConfigC := TYMMachineSettings.ExtractConfigCode(aSettings, 'C');

  fAWEMachType     := TYMMachineSettings.ExtractAWEMachineType(aSettings);
  fFilter          := cfNone;
  fFrontType       := TYMMachineSettings.ExtractFrontType(aSettings);
  fSensingHeadType := TYMMachineSettings.ExtractSensingHead(aSettings);
{}
end;

//:---------------------------------------------------------------------------
constructor TConfigurationCode.Create(aConfigA, aConfigB, aConfigC: Word);
begin
  inherited Create;

  fConfigA := aConfigA;
  fConfigB := aConfigB;
  fConfigC := aConfigC;

  fAWEMachType     := amtUnknown;
  fFilter          := cfNone;
  fFrontType       := ftNone;
  fSensingHeadType := shTK830;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.AddConfigCode(const aIndex: Integer; aMachineCfgCode, aProductionCfgCode: Word): Word;
begin
  Result := 0;
  if AWEMachType = amtAC338 then begin
    case aIndex of
      cCfgA:
        Result := (aProductionCfgCode and
                   ((not cCCA338Machine) or (aMachineCfgCode and cCCA338Production))) OR
                  (aMachineCfgCode and (not cCCA338Production));
      cCfgB:
        Result := (aProductionCfgCode and
                   ((not cCCB338Machine) or (aMachineCfgCode and cCCB338Production))) OR
                  (aMachineCfgCode and (not cCCB338Production));
      cCfgC:
        Result := (aProductionCfgCode and
                   ((not cCCC338Machine) or (aMachineCfgCode and cCCC338Production))) OR
                  (aMachineCfgCode and (not cCCC338Production));
    end;
  end
  else begin
    case aIndex of
      cCfgA:
        Result := (aProductionCfgCode and
                   ((not cCCAMachine) or (aMachineCfgCode and cCCAProduction))) OR
                  (aMachineCfgCode and (not cCCAProduction));
      cCfgB:
        Result := (aProductionCfgCode and
                   ((not cCCBMachine) or (aMachineCfgCode and cCCBProduction))) OR
                  (aMachineCfgCode and (not cCCBProduction));
      cCfgC:
        Result := (aProductionCfgCode and
                   ((not cCCCMachine) or (aMachineCfgCode and cCCCProduction))) OR
                  (aMachineCfgCode and (not cCCCProduction));
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.AddConfigCodeA(aMachineCfgA, aProductionCfgA: Word): Word;
begin
  Result := AddConfigCode(cCfgA, aMachineCfgA, aProductionCfgA);
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.AddConfigCodeB(aMachineCfgB, aProductionCfgB: Word): Word;
begin
  Result := AddConfigCode(cCfgB, aMachineCfgB, aProductionCfgB);
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.AddConfigCodeC(aMachineCfgC, aProductionCfgC: Word): Word;
begin
  Result := AddConfigCode(cCfgC, aMachineCfgC, aProductionCfgC);
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.CastConfigCode(const aIndex: Integer; aConfigCode1, aConfigCode2: Word): Word;
begin
// TODO XML: Anpassen
  if aConfigCode1 <> aConfigCode2 then
    Result := aConfigCode1 or aConfigCode2
  else
    Result := aConfigCode1;

  case aIndex of
    cCfgA:
      if SensingHead < shTK930F then
        Result := Result and not cCCAFFSensorActiveBit;

    cCfgB:
      if SensingHead < shTK930F then
        Result := Result and not cCCBFFBDSensorActiveBit;
  end;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.CastConfigCodeA(aConfigCode1, aConfigCode2: Word): Word;
begin
  Result := CastConfigCode(cCfgA, aConfigCode1, aConfigCode2);
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.CastConfigCodeB(aConfigCode1, aConfigCode2: Word): Word;
begin
  Result := CastConfigCode(cCfgB, aConfigCode1, aConfigCode2);
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.CastConfigCodeC(aConfigCode1, aConfigCode2: Word): Word;
begin
  Result := CastConfigCode(cCfgC, aConfigCode1, aConfigCode2);
end;

//:---------------------------------------------------------------------------
class function TConfigurationCode.CompatibleConfigCodes(aCfgCA1, aCfgCB1, aCfgCC1, aCfgCA2, aCfgCB2, aCfgCC2: Word): Boolean;
begin
  Result :=
    ((aCfgCA1 and cCCAUniversalProduction) = (aCfgCA2 and cCCAUniversalProduction)) and
    ((aCfgCB1 and cCCBUniversalProduction) = (aCfgCB2 and cCCBUniversalProduction)) and
    ((aCfgCC1 and cCCCUniversalProduction) = (aCfgCC2 and cCCCUniversalProduction));
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetBit(const aIndex: Integer): Boolean;
begin
  Result := False;
{wss
  Result := False;

  case aIndex of
    //.......................................................................
    // Config A
    //.......................................................................
    cCfgAAdjustRemove:
      Result := (fConfigA and cCCARemoveYarnAfterAdjustBit) <> 0;

    cCfgAAdjustRemoveAvailable:
      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338) or
                (fAWEMachType = amtOrion);

    cCfgABunchMonitor:
      Result := (fConfigA and cCCANoBunchMonitorBit) = 0;

    cCfgADriftConeChange:
      Result := (fConfigA and cCCADriftCompensationOnConeChangeBit) <> 0;

    cCfgAFFClearingOnSplice:
      Result := (fConfigA and cCCANoFFClearingOnSpliceBit) = 0;

    cCfgAFFClearingOnSpliceAvailable:
      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);

    cCfgAFFDetection:
      Result := (fConfigA and cCCAFFSensorActiveBit) <> 0;

    cCfgAFFDetectionAvailable:
      Result := (fSensingHeadType in cFFSensingHeadTypes);

    cCfgAKnifePowerHigh:
      Result := (fConfigA and cCCAKnifePowerHighBit) <> 0;

    cCfgAKnifePowerHighAvailable:
      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338);

    cCfgAOneDrumPuls:
      Result := (fConfigA and cCCAOneDrumPulsBit) <> 0;

    cCfgAOneDrumPulsAvailable:
      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAWE_SS);

    cCfgAZeroAdjust:
      Result := (fConfigA and cCCAZeroTestMonitorDisabledBit) = 0;
    //.......................................................................
    // Config B
    //.......................................................................
    cCfgBFFBDDetection:
      Result := ((fConfigB and cCCBFFBDSensorActiveBit) <> 0) and
                ((fConfigA and cCCAFFSensorActiveBit) <> 0);

    cCfgBFFBDDetectionAvailable:
      Result := (fSensingHeadType in cFFSensingHeadBDTypes);

    cCfgBConeChangeCondition:
      Result := (fConfigB and cCCBConeChangeDetectionDisabledBit) = 0;

    cCfgBCutBeforeAdjust:
      Result := (fConfigB and cCCBCutBeforeAdjustBit) <> 0;

    cCfgBCutOnYarnBreak:
      Result := (fConfigB and cCCBCutOnYarnBreakBit) <> 0;

    cCfgBExtMurItf:
      Result := (fConfigB and cCCBMurataExtendedItfBit) <> 0;

    cCfgBExtMurItfAvailable:
      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtMUR_IND_INV);

    cCfgBFFAdjAfterAlarm:
      Result := (fConfigB and cCCBFFAdjAfterAlarmBit) <> 0;

    cCfgBFFAdjAfterAlarmAvailable:
      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection) and
                not GetBit(cCfgBFFAdjAtOfflimit);

    cCfgBFFAdjAtOfflimit:
      Result := (fConfigB and cCCBNoFFAdjAtOfflimitBit) = 0;

    cCfgBFFAdjAtOfflimitAvailable:
      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);

    cCfgBHeadstockRight:
      Result := (fConfigB and cCCBHeadstockRightBit) <> 0;

    cCfgBKnifeMonitor:
      Result := (fConfigB and cCCBKnifeMonitorBit) <> 0;

    cCfgBUpperYarnCheck:
      Result := (fConfigB and cCCBUpperYarnCheckBit) <> 0;
  end;
{}
end;

//function TConfigurationCode.GetBit(const aIndex: Integer): Boolean;
//begin
//  Result := False;
//
//  case aIndex of
//    //.......................................................................
//    // Config A
//    //.......................................................................
//    cCfgAAdjustRemove:
//      if (fConfigA and cCCARemoveYarnAfterAdjustBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//
//    cCfgAAdjustRemoveAvailable:
//      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338) or
//        (fAWEMachType = amtOrion);
//  
//    cCfgABunchMonitor:
//      if (fConfigA and cCCANoBunchMonitorBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCfgADriftConeChange:
//      if (fConfigA and cCCADriftCompensationOnConeChangeBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgAFFClearingOnSplice:
//      if (fConfigA and cCCANoFFClearingOnSpliceBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCfgAFFClearingOnSpliceAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);
//
//    cCfgAFFDetection:
//      if (fConfigA and cCCAFFSensorActiveBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//
//    cCfgAFFDetectionAvailable:
//      Result := (fSensingHeadType in cFFSensingHeadTypes);
//  
//    cCfgAKnifePowerHigh:
//      if (fConfigA and cCCAKnifePowerHighBit) <> 0 then
//        Result := True
//  //        Result := GetBit(cCfgAKnifePowerHighAvailable)
//      else
//        Result := False;
//  
//    cCfgAKnifePowerHighAvailable:
//      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAC338);
//  
//    cCfgAOneDrumPuls:
//      if (fConfigA and cCCAOneDrumPulsBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgAOneDrumPulsAvailable:
//      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtAWE_SS);
//  
//    cCfgAZeroAdjust:
//      if (fConfigA and cCCAZeroTestMonitorDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//    //.......................................................................
//    // Config B
//    //.......................................................................
//    cCfgBFFBDDetection:
//      if ((fConfigB and cCCBFFBDSensorActiveBit) <> 0) and
//        ((fConfigA and cCCAFFSensorActiveBit) <> 0) then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgBFFBDDetectionAvailable:
//      Result := (fSensingHeadType in cFFSensingHeadBDTypes);
//  
//    cCfgBConeChangeCondition:
//      if (fConfigB and cCCBConeChangeDetectionDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCfgBCutBeforeAdjust:
//      if (fConfigB and cCCBCutBeforeAdjustBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgBCutOnYarnBreak:
//      if (fConfigB and cCCBCutOnYarnBreakBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgBExtMurItf:
//      if (fConfigB and cCCBMurataExtendedItfBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//
//    cCfgBExtMurItfAvailable:
//      Result := (fAWEMachType = amtUnknown) or (fAWEMachType = amtMUR_IND_INV);
//  
//    cCfgBFFAdjAfterAlarm:
//      if (fConfigB and cCCBFFAdjAfterAlarmBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgBFFAdjAfterAlarmAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection)
//        and not GetBit(cCfgBFFAdjAtOfflimit);
//  
//    cCfgBFFAdjAtOfflimit:
//      if (fConfigB and cCCBNoFFAdjAtOfflimitBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCfgBFFAdjAtOfflimitAvailable:
//      Result := GetBit(cCfgAFFDetectionAvailable) and GetBit(cCfgAFFDetection);
//  
//    cCfgBHeadstockRight:
//      if (fConfigB and cCCBHeadstockRightBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cCfgBKnifeMonitor:
//      if (fConfigB and cCCBKnifeMonitorBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//
//    cCfgBUpperYarnCheck:
//      if (fConfigB and cCCBUpperYarnCheckBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  end;
//
//end;
//
//:---------------------------------------------------------------------------
function TConfigurationCode.GetBlock(const aIndex: Integer): Boolean;
begin
  case aIndex of
    cFFBlock:
      Result := (fConfigA and cCCAFFBlockDisabledBit) = 0;

    cFFBlockAvailable:
      Result := ((fConfigA and cCCAFFSensorActiveBit) <> 0) and BlockEnabled;

    cCutFailBlock:
      Result := (fConfigA and cCCACutFailBlockDisabledBit) = 0;

    cCutFailBlockAvailable:
      Result := ((fConfigB and cCCBKnifeMonitorBit) <> 0) and BlockEnabled;

    cClusterBlock:
      Result := (fConfigA and cCCAClusterBlockDisabledBit) = 0;

    cCountBlock:
      Result := (fConfigA and cCCACountBlockDisabledBit) = 0;

    cSFIBlock:
      Result := (fConfigC and cCCCSFIBlockEnabledBit) <> 0;

    cFFClusterBlock:
      Result := (fConfigC and cCCCFFClusterBlockEnabledBit) <> 0;

    cFFClusterBlockAvailable:
      Result := ((fConfigA and cCCAFFSensorActiveBit) <> 0) and BlockEnabled;

    cBlockEnabled:
        Result := ( ((not fConfigA) and (cCCAFFBlockDisabledBit or
                                        cCCACutFailBlockDisabledBit or
                                        cCCAClusterBlockDisabledBit or
                                        cCCACountBlockDisabledBit)) OR
                    (fConfigC and (cCCCSFIBlockEnabledBit or cCCCFFClusterBlockEnabledBit)) ) <> 0;
  else
    Result := False;
  end;
end;
//function TConfigurationCode.GetBlock(const aIndex: Integer): Boolean;
//begin
//  Result := False;
//  
//  case aIndex of
//  
//    cFFBlock:
//      if (fConfigA and cCCAFFBlockDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cFFBlockAvailable:
//      if ((fConfigA and cCCAFFSensorActiveBit) <> 0) and BlockEnabled then
//        Result := True
//      else
//        Result := False;
//  
//    cCutFailBlock:
//      if (fConfigA and cCCACutFailBlockDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCutFailBlockAvailable:
//      if ((fConfigB and cCCBKnifeMonitorBit) <> 0) and BlockEnabled then
//        Result := True
//      else
//        Result := False;
//  
//    cClusterBlock:
//      if (fConfigA and cCCAClusterBlockDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cCountBlock:
//      if (fConfigA and cCCACountBlockDisabledBit) <> 0 then
//        Result := False
//      else
//        Result := True;
//  
//    cSFIBlock:
//      if (fConfigC and cCCCSFIBlockEnabledBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cFFClusterBlock:
//      if (fConfigC and cCCCFFClusterBlockEnabledBit) <> 0 then
//        Result := True
//      else
//        Result := False;
//  
//    cFFClusterBlockAvailable:
//      if ((fConfigA and cCCAFFSensorActiveBit) <> 0) and BlockEnabled then
//        Result := True
//      else
//        Result := False;
//  
//    cBlockEnabled:
//      begin
//  
//        if (((not fConfigA) and (cCCAFFBlockDisabledBit or
//          cCCACutFailBlockDisabledBit or cCCAClusterBlockDisabledBit or
//          cCCACountBlockDisabledBit)) or
//          (fConfigC and (cCCCSFIBlockEnabledBit or cCCCFFClusterBlockEnabledBit))) <> 0 then
//          Result := True
//        else
//          Result := False;
//      end;
//  end;
//end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetConfig(const aIndex: Integer): Word;
var
  xFilter: TCfgFilter;
begin
  xFilter := Filter;
  case aIndex of
    cCfgA:
      Result := fConfigA and GetFilterPattern(aIndex);

    cCfgB:
      Result := fConfigB and GetFilterPattern(aIndex);

    cCfgC:
      Result := fConfigC and GetFilterPattern(aIndex);

    cCfgMachA: begin
        Filter := cfMachine;
        Result := fConfigA and GetFilterPattern(aIndex);
      end;

    cCfgMachB: begin
        Filter := cfMachine;
        Result := fConfigB and GetFilterPattern(aIndex);
      end;

    cCfgMachC: begin
        Filter := cfMachine;
        Result := fConfigC and GetFilterPattern(aIndex);
      end;

    cCfgProdA: begin
        Filter := cfProduction;
        Result := fConfigA and GetFilterPattern(aIndex);
      end;

    cCfgProdB: begin
        Filter := cfProduction;
        Result := fConfigB and GetFilterPattern(aIndex);
      end;

    cCfgProdC: begin
        Filter := cfProduction;
        Result := fConfigC and GetFilterPattern(aIndex);
      end;
  else
    Result := 0;
  end;

  Filter := xFilter;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetDefault(const aIndex: Integer): Word;
begin
  Result := 0;
  case aIndex of
    cCfgA: begin
        case fAWEMachType of
          amtEspero, amtOrion:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCASIDefault;
            else
              Result := cCCADefault;
            end;

          amtMUR_IND_INV:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCAMIDefault;
            else
              Result := cCCADefault;
            end;

          amtAC338:
            Result := cCCAAC338Default;
        else
          Result := cCCADefault;
        end; // case
        Result := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, Result, 'A');
      end; // cCfgA

    cCfgB: begin
        case fAWEMachType of
          amtEspero, amtOrion:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCBSIDefault;
            else
              Result := cCCBDefault;
            end;

          amtMUR_IND_INV:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCBMIDefault;
            else
              Result := cCCBDefault;
            end;

          amtAC338:
            Result := cCCBAC338Default;
        else
          Result := cCCBDefault;
        end; // case
        Result := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, Result, 'B');
      end; // cCfgB

    cCfgC: begin
        case fAWEMachType of
          amtEspero, amtOrion:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCCSIDefault;
            else
              Result := cCCCDefault;
            end;

          amtMUR_IND_INV:
            case fFrontType of
              ftZE80i, ftZE800i: Result := cCCCMIDefault;
            else
              Result := cCCCDefault;
            end;

          amtAC338:
            Result := cCCCAC338Default;
        else
          Result := cCCCDefault;
        end; // case
        Result := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, Result, 'C');
      end; // CfgC
  else
  end; // case aIndex
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetFilterPattern(const aCfgCode: Integer): Word;
var
  xCfgCode: Integer;
begin
  Result := 0;
  case aCfgCode of
    cCfgA, cCfgMachA, cCfgProdA: xCfgCode := cCfgA;
    cCfgB, cCfgProdB, cCfgMachB: xCfgCode := cCfgB;
    cCfgC, cCfgProdC, cCfgMachC: xCfgCode := cCfgC;
  else
    xCfgCode := cCfgA;
  end;

  case fFilter of
    cfNone:
      Result := $FFFF;

    cfProduction:
      case xCfgCode of
        cCfgA:
          case fAWEMachType of
            amtAC338: Result := cCCA338Production;
          else
            Result := cCCAProduction;
          end;

        cCfgB:
          case fAWEMachType of
            amtAC338: Result := cCCB338Production;
          else
            Result := cCCBProduction;
          end;

        cCfgC:
          case fAWEMachType of
            amtAC338: Result := cCCC338Production;
          else
            Result := cCCCProduction;
          end;
      end;

    cfMachine:
      case xCfgCode of
        cCfgA:
          case fAWEMachType of
            amtAC338: Result := cCCA338Machine;
          else
            Result := cCCAMachine;
          end;
        cCfgB:
          case fAWEMachType of
            amtAC338: Result := cCCB338Machine;
          else
            Result := cCCBMachine;
          end;
        cCfgC:
          case fAWEMachType of
            amtAC338: Result := cCCC338Machine;
          else
            Result := cCCCMachine;
          end;
      end;

    cfDiagnosis:
      case xCfgCode of
        cCfgA:
          case fAWEMachType of
            amtAC338: Result := cCCA338Diagnosis;
          else
            Result := cCCADiagnosis;
          end;
        cCfgB:
          case fAWEMachType of
            amtAC338: Result := cCCB338Diagnosis;
          else
            Result := cCCBDiagnosis;
          end;
        cCfgC:
          case fAWEMachType of
            amtAC338: Result := cCCC338Diagnosis;
          else
            Result := cCCCDiagnosis;
          end;
      end;

    cfUnused:
      case xCfgCode of
        cCfgA:
          case fAWEMachType of
            amtAC338: Result := cCCA338Unused;
          else
            Result := cCCAUnused;
          end;
        cCfgB:
          case fAWEMachType of
            amtAC338: Result := cCCB338Unused;
          else
            Result := cCCBUnused;
          end;
        cCfgC:
          case fAWEMachType of
            amtAC338: Result := cCCC338Unused;
          else
            Result := cCCCUnused;
          end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetPattern(const aIndex: Integer): Word;
var
  xFilter: TCfgFilter;
begin
  Result := 0;
  xFilter := Filter;

  case aIndex of
    cCfgMachA, cCfgMachB, cCfgMachC: begin
        Filter := cfMachine;
        Result := GetFilterPattern(aIndex);
      end;

    cCfgProdA, cCfgProdB, cCfgProdC: begin
        Filter := cfProduction;
        Result := GetFilterPattern(aIndex);
      end;
  end;
  Filter := xFilter;
end;

//:---------------------------------------------------------------------------
function TConfigurationCode.GetSensitivity(const aIndex: Integer): Integer;
begin
  case aIndex of
    cSFSSensitivity:
      if (fConfigA and cCCASFSSensitivityBit) <> 0 then
        Result := 2
      else
        Result := 1;

    cDFSSensitivity:
      if (fConfigA and cCCADFSSensitivity1Bit) <> 0 then begin
        if (fConfigB and cCCBDFSSensitivity2Bit) <> 0 then
          Result := 4
        else
          Result := 2;
      end
      else begin
        if (fConfigB and cCCBDFSSensitivity2Bit) <> 0 then
          Result := 2
        else
          Result := 1;
      end;
  else
    Result := 0;
  end;
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetBit(const aIndex: Integer; const aValue: Boolean);
begin
  case aIndex of
    //...................................................................
    // Config A
    //...................................................................
    // cCfgAAdjustRemoveAvailable: read only
    // cCfgAFFClearingOnSpliceAvailable: read only
    // cCfgAFFDetectionAvailable: read only
    // cCfgAKnifePowerHighAvailable: read only
    // cCfgAOneDrumPulsAvailable: read only
    cCfgAAdjustRemove:
      if aValue then
        fConfigA := fConfigA or cCCARemoveYarnAfterAdjustBit
      else
        fConfigA := fConfigA and not cCCARemoveYarnAfterAdjustBit;

    cCfgABunchMonitor:
      if aValue then
        fConfigA := fConfigA and not cCCANoBunchMonitorBit
      else
        fConfigA := fConfigA or cCCANoBunchMonitorBit;

    cCfgADriftConeChange:
      if aValue then
        fConfigA := fConfigA or cCCADriftCompensationOnConeChangeBit
      else
        fConfigA := fConfigA and not cCCADriftCompensationOnConeChangeBit;

    cCfgAFFClearingOnSplice:
      if aValue then
        fConfigA := fConfigA and not cCCANoFFClearingOnSpliceBit
      else
        fConfigA := fConfigA or cCCANoFFClearingOnSpliceBit;

    cCfgAFFDetection: begin
        if aValue then
          fConfigA := fConfigA or cCCAFFSensorActiveBit
        else
          fConfigA := fConfigA and not cCCAFFSensorActiveBit;

        if FFBDDetectionAvailable then
          SetBit(cCfgBFFBDDetection, aValue);
      end;

    cCfgAKnifePowerHigh:
      if aValue then
        fConfigA := fConfigA or cCCAKnifePowerHighBit
      else
        fConfigA := fConfigA and not cCCAKnifePowerHighBit;

    cCfgAOneDrumPuls:
      if aValue then
        fConfigA := fConfigA or cCCAOneDrumPulsBit
      else
        fConfigA := fConfigA and not cCCAOneDrumPulsBit;

    cCfgAZeroAdjust:
      if aValue then
        fConfigA := fConfigA and not cCCAZeroTestMonitorDisabledBit
      else
        fConfigA := fConfigA or cCCAZeroTestMonitorDisabledBit;

    //...................................................................
    // Config B
    //...................................................................
    // cCfgBFFBDDetectionAvailable: read only
    // cCfgBExtMurItfAvailable: read only
    // cCfgBFFAdjAfterAlarmAvailable: read only
    // cCfgBFFAdjAtOfflimitAvailable: read only
    cCfgBFFBDDetection:
      if aValue then
        fConfigB := fConfigB or cCCBFFBDSensorActiveBit
      else
        fConfigB := fConfigB and not cCCBFFBDSensorActiveBit;

    cCfgBConeChangeCondition:
      if aValue then
        fConfigB := fConfigB and not cCCBConeChangeDetectionDisabledBit
      else
        fConfigB := fConfigB or cCCBConeChangeDetectionDisabledBit;

    cCfgBCutBeforeAdjust:
      if aValue then
        fConfigB := fConfigB or cCCBCutBeforeAdjustBit
      else
        fConfigB := fConfigB and not cCCBCutBeforeAdjustBit;
  
    cCfgBCutOnYarnBreak:
      if aValue then
        fConfigB := fConfigB or cCCBCutOnYarnBreakBit
      else
        fConfigB := fConfigB and not cCCBCutOnYarnBreakBit;
  
    cCfgBExtMurItf:
      if aValue then
        fConfigB := fConfigB or cCCBMurataExtendedItfBit
      else
        fConfigB := fConfigB and not cCCBMurataExtendedItfBit;
  
    cCfgBFFAdjAfterAlarm:
      if aValue then
        fConfigB := fConfigB or cCCBFFAdjAfterAlarmBit
      else
        fConfigB := fConfigB and not cCCBFFAdjAfterAlarmBit;

    cCfgBFFAdjAtOfflimit:
      if aValue then begin
        fConfigB := fConfigB and not cCCBNoFFAdjAtOfflimitBit;
        SetBit(cCfgBFFAdjAfterAlarm, False);
      end
      else
        fConfigB := fConfigB or cCCBNoFFAdjAtOfflimitBit;

    cCfgBHeadstockRight:
      if aValue then
        fConfigB := fConfigB or cCCBHeadstockRightBit
      else
        fConfigB := fConfigB and not cCCBHeadstockRightBit;

    cCfgBKnifeMonitor:
      if aValue then
        fConfigB := fConfigB or cCCBKnifeMonitorBit
      else
        fConfigB := fConfigB and not cCCBKnifeMonitorBit;
  
    cCfgBUpperYarnCheck:
      if aValue then
        fConfigB := fConfigB or cCCBUpperYarnCheckBit
      else
        fConfigB := fConfigB and not cCCBUpperYarnCheckBit;
  end;
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetBlock(const aIndex: Integer; const aValue: Boolean);
begin
  
  case aIndex of
    // cFFBlockAvailable: read only
    // cCutFailBlockAvailable: read only
    // cFFClusterBlockAvailable: read only
    cFFBlock:
      if aValue then
        fConfigA := fConfigA and not cCCAFFBlockDisabledBit
      else
        fConfigA := fConfigA or cCCAFFBlockDisabledBit;

    cCutFailBlock:
      if aValue then
        fConfigA := fConfigA and not cCCACutFailBlockDisabledBit
      else
        fConfigA := fConfigA or cCCACutFailBlockDisabledBit;

    cClusterBlock:
      if aValue then
        fConfigA := fConfigA and not cCCAClusterBlockDisabledBit
      else
        fConfigA := fConfigA or cCCAClusterBlockDisabledBit;

    cCountBlock:
      if aValue then
        fConfigA := fConfigA and not cCCACountBlockDisabledBit
      else
        fConfigA := fConfigA or cCCACountBlockDisabledBit;

    cSFIBlock:
      if aValue then
        fConfigC := fConfigC or cCCCSFIBlockEnabledBit
      else
        fConfigC := fConfigC and not cCCCSFIBlockEnabledBit;

    cFFClusterBlock:
      if aValue then
        fConfigC := fConfigC or cCCCFFClusterBlockEnabledBit
      else
        fConfigC := fConfigC and not cCCCFFClusterBlockEnabledBit;

    cBlockEnabled: begin
        if aValue then begin
          fConfigA := fConfigA and not (cCCAFFBlockDisabledBit or
                                        cCCACutFailBlockDisabledBit or
                                        cCCAClusterBlockDisabledBit or
                                        cCCACountBlockDisabledBit);

          fConfigC := fConfigC or (cCCCSFIBlockEnabledBit or cCCCFFClusterBlockEnabledBit);
        end
        else begin
          fConfigA := fConfigA or (cCCAFFBlockDisabledBit or
                                   cCCACutFailBlockDisabledBit or
                                   cCCAClusterBlockDisabledBit or
                                   cCCACountBlockDisabledBit);

          fConfigC := fConfigC and not (cCCCSFIBlockEnabledBit or cCCCFFClusterBlockEnabledBit);
        end;

      end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetConfig(const aIndex: Integer; const aValue: Word);
begin
  case aIndex of
    cCfgA: fConfigA := aValue;
    cCfgB: fConfigB := aValue;
    cCfgC: fConfigC := aValue;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetSensingHeadDependency;
begin
  fConfigA := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, fConfigA, 'A');
  fConfigB := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, fConfigB, 'B');
  fConfigC := TYMSettingsUtils.SetSensingHeadDependency(fSensingHeadType, fConfigC, 'C');
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetSensingHeadType(const aValue: TSensingHead);
begin
  fSensingHeadType := ValidateSensingHeadType(aValue);
  ValidateSensingHeadDependency;
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.SetSensitivity(const aIndex, aValue: Integer);
begin
  case aIndex of
    cSFSSensitivity:
      if aValue = 1 then
        fConfigA := fConfigA and not cCCASFSSensitivityBit
      else
        fConfigA := fConfigA or cCCASFSSensitivityBit;

    cDFSSensitivity:
      case aValue of
        1: begin
            fConfigA := fConfigA and not cCCADFSSensitivity1Bit;
            fConfigB := fConfigB and not cCCBDFSSensitivity2Bit;
          end;

        2: fConfigA := fConfigA or cCCADFSSensitivity1Bit;
      else
        fConfigA := fConfigA or cCCADFSSensitivity1Bit;
        fConfigB := fConfigB or cCCBDFSSensitivity2Bit;
      end; // case aValue
  end; // case aIndex
end;

//:---------------------------------------------------------------------------
procedure TConfigurationCode.ValidateSensingHeadDependency;
begin
{wss
  if SensingHead in cOtherSensingHeadTypes then begin
    fConfigA := fConfigA and not (cCCAFFSensorActiveBit);
    fConfigB := fConfigB and not (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit or cCCBFFAdjAfterAlarmBit);
    //fConfigC := fConfigC and not (cCCCFFClusterAlarmBlockBit);
  end
  else if SensingHead in cFFSensingHeadTypes then begin
    if (SensingHead = htTK940BD) then begin
      if (fConfigA and cCCAFFSensorActiveBit) = cCCAFFSensorActiveBit then
        fConfigB := fConfigB or cCCBFFBDSensorActiveBit
      else
        fConfigB := fConfigB and not (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit or cCCBFFAdjAfterAlarmBit);
    end
    else
      fConfigB := fConfigB and not cCCBFFBDSensorActiveBit;
      //fConfigB := fConfigB and not (cCCBFFBDSensorActiveBit or cCCBNoFFAdjAtOfflimitBit or cCCBFFAdjAfterAlarmBit);
      //fConfigC := fConfigC or (cCCCFFClusterAlarmBlockBit);
  end;
{}
end;


//:---------------------------------------------------------------------------
//:--- Class: TYMMachineConfig
//:---------------------------------------------------------------------------
constructor TYMMachineConfig.Create;
begin
  inherited;
  
  fSensingHeadClassList  := TSensingHeadClassList.Create;
  fMachineConfig         := TYMSettingsUtils.InitMachineYMConfigRec;

  fActiveGroup           := Low(fMachineConfig.spec);
  fBuildMaxMachineConfig := True;
  fMaxMachineConfig      := fMachineConfig;
  fSpindle.contents      := cSpdlRngNotDefined;

  mRecordedMachineConfig := fMachineConfig;

  Filter                 := cfNone;
end;

//:---------------------------------------------------------------------------
destructor TYMMachineConfig.Destroy;
begin
  FreeAndNil(fSensingHeadClassList);
  inherited;
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.CastGroupSpecificConfig(aRange: TSpindleRangeRec): TGroupSpecificMachConfigRec;
var
  i, j: Integer;
  xHitTbl: array[Low(MachineConfig.spec)..High(MachineConfig.spec)] of Integer;
  xConfigCode: TConfigurationCode;
const
  cNoHit = High(xHitTbl) + 1;
  //.....................................................................
  function CastAWEType(aAWEType1, aAWEType2: TAWEType): TAWEType;
  begin
//  atUnknown, atAWE800, atAWESpectra
    if aAWEType1 <> aAWEType2 then
      if aAWEType1 < aAWEType2 then
        Result := aAWEType1
      else
        Result := aAWEType2
    else
      Result := aAWEType1;
  end;
  //.....................................................................
begin
{TODO: TYMSettingsRec Obsolet
  with MachineConfig do begin
    j          := Low(xHitTbl);
    xHitTbl[j] := cNoHit;

    if (aRange.start >= 1) and (aRange.stop <= cMaxSpindeln) and
      (aRange.start <= aRange.stop) then
      for i := Low(spec) to High(spec) do begin
        with spec[i] do begin
          if (spindle.contents <> cSpdlRngNotDefined) and
             (spindle.contents <> 0) and
             (spindle.stop >= aRange.start) and (aRange.stop >= spindle.start) then
          begin
          // there is an intersection (es existiert eine Schnittmenge)
            xHitTbl[j] := i;
            Inc(j);
            if j <= High(xHitTbl) then
              xHitTbl[j] := cNoHit;
          end;
        end;
      end
    else begin
      aRange.contents           := cSpdlRngNotDefined;
      xHitTbl[Low(xHitTbl)]     := fActiveGroup;
      xHitTbl[Low(xHitTbl) + 1] := cNoHit;
    end;

    Result.spindle.contents   := aRange.contents;
    Result.aWEType            := High(TAWEType); //atUnknown;
    Result.sensingHead        := High(TSensingHead);
    Result.configA            := 0;
    Result.configB            := 0;
    Result.configC            := 0;
    Result.inoperativePara[0] := -1; //$FFFFFFFF;
    Result.inoperativePara[1] := -1; //$FFFFFFFF;

    j           := Low(xHitTbl);
    xConfigCode := TConfigurationCode.Create;
    if xHitTbl[j] <> cNoHit then
      Result.sensingHead := spec[xHitTbl[j]].sensingHead
    else
      Result.sensingHead := High(TSensingHead);

    while (j <= High(xHitTbl)) and (xHitTbl[j] <> cNoHit) do begin
      with spec[xHitTbl[j]] do begin

  // %%begin: Need to insert the Cast algorithme !!!
        xConfigCode.AWEMachType   := AWEMachType;
        xConfigCode.FrontType     := FrontType;
        Result.aWEType            := CastAWEType(Result.aWEType, aWEType);
        Result.sensingHead        := CastSensingHead(Result.sensingHead, sensingHead);

        xConfigCode.SensingHead   := Result.sensingHead;
        Result.configA            := xConfigCode.CastConfigCodeA(Result.configA, configA);
        Result.configB            := xConfigCode.CastConfigCodeB(Result.configB, configB);
        Result.configC            := xConfigCode.CastConfigCodeC(Result.configC, configC);
        Result.inoperativePara[0] := TMachineAttributes.CastInopSettings0(Result.inoperativePara[0], inoperativePara[0]);
        Result.inoperativePara[1] := TMachineAttributes.CastInopSettings1(Result.inoperativePara[1], inoperativePara[1], Result.sensingHead);
  // %%end: Need to insert the Cast algorithme !!!
      end; // with
      Inc(j);
    end; // while
    xConfigCode.Free;
  end;
{}
end;

//:---------------------------------------------------------------------------
{* Wenn die zwei Parameter unterschiedlich sind, dann wird der kleinste Sensinghead TK830 zur�ck gegeben.
Ansonsten derjenige wo getestet wurde. Urspr�nglich wurde immer der kleinere zur�ckgegeben. Warum nun nicht mehr? *}
class function TYMMachineConfig.CastSensingHead(aSensingHead1, aSensingHead2: TSensingHead): TSensingHead;
begin
    // htTK830, htTK840, htTK850, htTK870, htTK880, htTK930F, htTK940BD, htTK930H, htTK940F, htTK930S
  if aSensingHead1 <> aSensingHead2 then
    Result := shTK830
  (*
      if aSensingHead1 < aSensingHead2 then
        Result := aSensingHead1
      else
        Result := aSensingHead2
  *)
  else
    Result := aSensingHead1;
  
end;

//:---------------------------------------------------------------------------
{* Gibt den kleinsten SensingHead von einer entsprechenden Klasse zur�ck *}
function TYMMachineConfig.ConvertSensingHeadClassToType(const aValue: TSensingHeadClass): TSensingHead;
begin
// DONE wss: Zenit erweitern
  case aValue of
    shc8x:      Result := shTK830;
    shc9xFS:    Result := shTK930F;
    shc9xH:     Result := shTK930H;
    shc9xBD:    Result := shTK940BD;
    shcZenit:   Result := shTKZenit;
    shcZenitF:  Result := shTKZenitF;
    shcZenitFP: Result := shTKZenitFP;
//    shcZenit, shcZenitF, shcZenitFP: Result := shTKZenit;
  else
    Result := shTK830;
  end;
//  case aValue of
//    hcTK8xx:   Result := htTK830;
//    hcTK9xx:   Result := htTK930F;
//    hcTK9xxH:  Result := htTK930H;
//    hcTK9xxBD: Result := htTK940BD;
//  else
//    Result := htTK830;
//  end;
end;

//:---------------------------------------------------------------------------
{* Gibt die SensingHead Klasse anhand eines SensingHeads zur�ck *}
class function TYMMachineConfig.ConvertSensingHeadTypeToClass(const aValue: TSensingHead): TSensingHeadClass;
//var
//  xSensingHead: TSensingHead;
begin
  case aValue of
    shNone:                       Result := shcNone;
    shTK930F, shTK930S, shTK940F: Result := shc9xFS;
    shTK930H:                     Result := shc9xH;
    shTK940BD:                    Result := shc9xBD;
    shTKZenit:                    Result := shcZenit;
    shTKZenitF:                   Result := shcZenitF;
    shTKZenitFP:                  Result := shcZenitFP;
  else // shTK830, shTK840, shTK850, shTK870, shTK880
    Result := shc8x;
  end;
{
  xSensingHead := ValidateSensingHeadType(aValue);
  if xSensingHead in cFFSensingHeadTypes then begin
    if xSensingHead in cFFSensingHeadFTypes then
      Result := hcTK9xx
    else if xSensingHead in cFFSensingHeadBDTypes then
      Result := hcTK9xxBD
    else
      Result := hcTK9xxH;
  end
  else
    Result := hcTK8xx;
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetAvailable(const aIndex: Integer): Boolean;
begin
{TODO: TYMSettingsRec Obsolet
  case aIndex of
    cCutRetries:
      Result := KnifeMonitor;

    cDefaultSpeedRamp,
      cDefaultSpeed,
      CSpeedSimulation:
      Result := (MachineConfig.aWEMachType = amtUnknown) or
                (MachineConfig.aWEMachType = amtAWE_SS);

    cDependendSpdleRange:
      Result := ((MachineConfig.frontType = ftZE80i) or
                 (MachineConfig.frontType = ftZE800i)) AND

                ((MachineConfig.aWEMachType = amtESPERO) or
                 (MachineConfig.aWEMachType = amtMUR_IND_INV) or
                 (MachineConfig.aWEMachType = amtOrion)) OR

                (MachineConfig.aWEMachType = amtAC338); //Added by Nue 5.11.01
  else
    Result := False;
  end;
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetChanged: Boolean;
begin
  UpdateMachineConfig;
  Result := not CompareMem(@fMachineConfig, @mRecordedMachineConfig, SizeOf(fMachineConfig));
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetConfig(const aIndex: Integer): Word;
begin
  case aIndex of
    cCfgA:     Result := inherited A;
    cCfgB:     Result := inherited B;
    cCfgC:     Result := inherited C;
    cCfgMachA: Result := inherited MachA;
    cCfgMachB: Result := inherited MachB;
    cCfgMachC: Result := inherited MachC;
    cCfgProdA: Result := inherited ProdA;
    cCfgProdB: Result := inherited ProdB;
    cCfgProdC: Result := inherited ProdC;
  else
    Result := 0;
  end;
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetGroupSpecificConfig: TGroupSpecificMachConfigRec;
begin
  UpdateMachineConfig;
  Result := fMachineConfig.spec[fActiveGroup];
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetIntersectedGroups(aRange: TSpindleRangeRec): TGroupListArr;
var
  i, j: Integer;
begin
  with MachineConfig do begin
    for j := Low(Result) to High(Result) do
      Result[j] := cNoGroup;

    j := Low(Result);
    if (aRange.start >= 1) and (aRange.stop <= cMaxSpindeln) and
      (aRange.start <= aRange.stop) then

      for i := Low(spec) to High(spec) do begin
        with spec[i] do begin
          if (spindle.contents <> cSpdlRngNotDefined) and
             (spindle.contents <> 0) and
             (spindle.stop >= aRange.start) and (aRange.stop >= spindle.start) then
          begin
            // there is an intersection (es existiert eine Schnittmenge)
            if j <= High(Result) then begin
              Result[j] := i;
              Inc(j);
            end;
          end;
        end;
      end;
  end;
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetMachineConfig: TMachineYMConfigRec;
begin
  UpdateMachineConfig;
  Result := fMachineConfig;
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetSensingHead: TSensingHead;
begin
{TODO: TYMSettingsRec Obsolet
  Result := fMachineConfig.spec[fActiveGroup].sensingHead;
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineConfig.GetSensingHeadClass: TSensingHeadClass;
begin
  Result := ConvertSensingHeadTypeToClass(GetSensingHead);
end;

//:---------------------------------------------------------------------------
class function TYMMachineConfig.IndexToSensigHeadType(aIndex: Integer): TSensingHead;
begin
  Result := shTK830;
//  Result := TSensingHead(Ord(Low(cSensingHeadTypeNames)) + Ord(aIndex));
end;

//:---------------------------------------------------------------------------
class function TYMMachineConfig.IndexToSwitchState(aIndex: Integer): TSwitchState;
begin
  Result := TSwitchState(Ord(aIndex) + Ord(ssOn));
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.RecordChanges;
begin
  mRecordedMachineConfig := fMachineConfig;
end;

//:---------------------------------------------------------------------------
class function TYMMachineConfig.SensigHeadTypeToIndex(aSensingHead: TSensingHead): Integer;
begin
  Result := 0;
//  Result := Ord(aSensingHead) - Ord(Low(cSensingHeadTypeNames));
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetActiveGroup(const aValue: Integer);
begin
  if (aValue >= Low(fMachineConfig.spec)) and
     (aValue <= High(fMachineConfig.spec)) then
  begin
    fActiveGroup := aValue;
    SetGroupSpecificConfig(fMachineConfig.spec[fActiveGroup]);
  end;
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetBuildMaxMachineConfig(const aValue: Boolean);
//var
//  i, xActiveGroup: Integer;
//  xSensingHead: TSensingHead;
//  xList: TList;
begin
{wss
  if aValue then begin
    xList := TList.Create;
    fBuildMaxMachineConfig := True;
    SensingHeadClassList.Clear;

    i := Low(fMaxMachineConfig.spec);
    while (i <= High(fMaxMachineConfig.spec)) AND
          ((fMaxMachineConfig.spec[i].spindle.contents <> cSpdlRngNotDefined) and
           (fMaxMachineConfig.spec[i].spindle.contents <> 0)) do
    begin
      SensingHeadClassList.Add(ConvertSensingHeadTypeToClass(fMaxMachineConfig.spec[i].sensingHead));
      xList.Add(Pointer(fMaxMachineConfig.spec[i].sensingHead));
      fMaxMachineConfig.spec[i].spindle.contents := cSpdlRngNotDefined;
      Inc(i);
    end;

    SensingHeadClassList.Sort;
    xList.Sort(SensingHeadTypeCompare);

    xActiveGroup := ActiveGroup;
    xSensingHead := SensingHead;
    ActiveGroup  := 1;
    if xSensingHead in cFFSensingHeadTypes then
      SensingHead := htTK830
    else
      SensingHead := htTK930F;

    if xList.Count <> 0 then
      SensingHead := TSensingHead(xList.Items[0]);

    fMaxMachineConfig.inoperativePara[1] := fMaxMachineConfig.inoperativePara[1] and
                                            not cIP1MFFSensingHeads;

    with fMaxMachineConfig.spec[Low(fMaxMachineConfig.spec)] do begin
      spindle.start      := 1;
      spindle.stop       := cMaxSpindeln;
      sensingHead        := SensingHead;

      Filter             := cfProduction;
      configA            := A;
      configB            := B;
      configC            := C;
      Filter             := cfNone;

      inoperativePara[0] := fMaxMachineConfig.inoperativePara[0];
      inoperativePara[1] := TMachineAttributes.SetInopSettings1FFSensingHead(SensingHead, fMaxMachineConfig.inoperativePara[1]);
    end;

    ActiveGroup := xActiveGroup;
    SensingHead := xSensingHead;

    xList.Free;
  end
  else begin
    fBuildMaxMachineConfig := False;
    fMaxMachineConfig := TYMSettingsUtils.InitMachineYMConfigRec;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetCollectMaxMachineConfig(const aValue: TMachineYMConfigRec);
var
  i, j: Integer;
  xFound: Boolean;
begin
{TODO: TYMSettingsRec Obsolet
  if fBuildMaxMachineConfig = True then begin
    fBuildMaxMachineConfig := False;
    fMaxMachineConfig      := TYMSettingsUtils.InitMachineYMConfigRec;
  end;

  j := Low(aValue.spec);
  while (j <= High(aValue.spec)) AND
        ((aValue.spec[j].spindle.contents <> cSpdlRngNotDefined) and
         (aValue.spec[j].spindle.contents <> 0)) do
  begin
    i      := Low(fMaxMachineConfig.spec);
    xFound := False;
    while (xFound = False) and (i <= High(fMaxMachineConfig.spec)) and
          ((fMaxMachineConfig.spec[i].spindle.contents <> cSpdlRngNotDefined) and
           (fMaxMachineConfig.spec[i].spindle.contents <> 0)) do
    begin
      if ConvertSensingHeadTypeToClass(fMaxMachineConfig.spec[i].sensingHead) =
         ConvertSensingHeadTypeToClass(aValue.spec[j].sensingHead) then
        xFound := True;

      Inc(i);
    end;

    if (xFound = False) and (i <= High(fMaxMachineConfig.spec)) then begin
      fMaxMachineConfig.spec[i].spindle.start := 1;
      fMaxMachineConfig.spec[i].spindle.stop  := 1;
      fMaxMachineConfig.spec[i].sensingHead   := aValue.spec[j].sensingHead;
    end;

    fMaxMachineConfig.inoperativePara[0] := fMaxMachineConfig.inoperativePara[0] or aValue.spec[j].inoperativePara[0];
    fMaxMachineConfig.inoperativePara[1] := fMaxMachineConfig.inoperativePara[1] or (aValue.spec[j].inoperativePara[1]);

    Inc(j);
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetConfig(const aIndex: Integer; const aValue: Word);
begin
  with fMachineConfig.spec[fActiveGroup] do begin
    case aIndex of
      cCfgA: begin
          inherited A := aValue;
          configA     := aValue;
        end;

      cCfgB: begin
          inherited B := aValue;
          configB     := aValue;
        end;

      cCfgC: begin
          inherited C := aValue;
          configC     := aValue;
        end;
    else
    end; // case
  end; // with
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetDefaultMachineYMConfig;
begin
{TODO: TYMSettingsRec Obsolet
  A := DefaultA;
  B := DefaultB;
  C := DefaultC;

// TODO XML: checkLen auf Single Typ �ndern
  case fMachineConfig.aWEMachType of
    amtMUR_IND_INV:
//      fMachineConfig.checkLen := cDefaultMurSpliceCheck;
      fMachineConfig.checkLen := Round(cSpliceCheckDefaultMur * 10);

    amtAC338:
//      fMachineConfig.checkLen := cDefaultAC338SpliceCheck;
      fMachineConfig.checkLen := Round(cSpliceCheckDefaultAC338 * 10);
  else
//    fMachineConfig.checkLen := cDefaultSpliceCheck;
    fMachineConfig.checkLen := Round(cSpliceCheckDefault * 10);
  end;

  fMachineConfig.cutRetries  := cDefaultCutRetries;
  fMachineConfig.longStopDef := cInitLongStopDef * 6;

  UpdateMachineConfig;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetGroupSpecificConfig(const aValue: TGroupSpecificMachConfigRec);
begin
{TODO: TYMSettingsRec Obsolet
  fMachineConfig.spec[fActiveGroup] := aValue;

  with fMachineConfig.spec[fActiveGroup] do begin
    self.SensingHead := sensingHead;
    A                := configA;
    B                := configB;
    C                := configC;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetMachineConfig(const aValue: TMachineYMConfigRec);
begin
{TODO: TYMSettingsRec Obsolet
  fMachineConfig := aValue;

  AWEMachType    := fMachineConfig.aWEMachType;
  FrontType      := fMachineConfig.frontType;

  SetGroupSpecificConfig(fMachineConfig.spec[fActiveGroup]);
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetSensingHead(const aValue: TSensingHead);
begin
{TODO: TYMSettingsRec Obsolet
  with fMachineConfig.spec[fActiveGroup] do begin
    sensingHead        := aValue;
    inoperativePara[1] := TMachineAttributes.SetInopSettings1FFSensingHead( aValue, inoperativePara[1]);
  end;

  inherited SensingHead := aValue;
  inherited SetSensingHeadDependency;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.SetSensingHeadClass(const aValue: TSensingHeadClass);
begin
  SensingHead := ConvertSensingHeadClassToType(aValue);
end;

//:---------------------------------------------------------------------------
class function TYMMachineConfig.SwichStateToIndex(aSwitchState: TSwitchState): Integer;
begin
  Result := Ord(aSwitchState) - Ord(ssOn);
end;

//:---------------------------------------------------------------------------
procedure TYMMachineConfig.UpdateMachineConfig;
begin
  with fMachineConfig.spec[fActiveGroup] do begin
    configA := A;
    configB := B;
    configC := C;
  end;
end;


//:---------------------------------------------------------------------------
//:--- Class: TYMMachineSettings
//:---------------------------------------------------------------------------
class function TYMMachineSettings.EqualFrontSWVersion(aFrontSWVersion1, aFrontSWVersion2: TYMSWVersionArr): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i := Low(aFrontSWVersion1) to High(aFrontSWVersion1) do begin
    if aFrontSWVersion1[i] <> aFrontSWVersion2[i] then
      Result := False;
  end;
end;

//:---------------------------------------------------------------------------
class function TYMMachineSettings.ExtractAWEMachineType(var aSettings: TYMSettingsRecX): TAWEMachType;
begin
      Result := amtDemoWinder
{TODO: TYMSettingsRec Obsolet
  with aSettings.available do
    if (machType >= Low(cAWEMachTypXRefTbl)) and (machType <= High(cAWEMachTypXRefTbl)) then
      Result := cAWEMachTypXRefTbl[machType]
    else if machType = cWTAC238SysSpectra then
      Result := amtAC238_SYS
    else if machType = cWTDemoWinder then
      Result := amtDemoWinder
    else
      Result := amtUnknown;
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineSettings.ExtractAWEType(var aSettings: TYMSettingsRecX): TAWEType;
begin
{TODO: TYMSettingsRec Obsolet
  with aSettings.available do
    if ((machType >= cWTFirstSpectra) and (machType <= cWTFirstSpectra)) OR
       (machType = cWTAC238SysSpectra) then
      Result := atAWESpectra
    else if (machType = cWTDemoWinder) or (machType = cWTUnknownWinder) then
      Result := atUnknown
    else
      Result := atAWE800;
{}
end;

//:---------------------------------------------------------------------------
class function TYMMachineSettings.ExtractConfigCode(var aSettings: TYMSettingsRecX; aType: Char): Word;
begin
{TODO: TYMSettingsRec Obsolet
  Result := 0;
  with aSettings.additional do
    case aType of
      'A': Result := configA; // and cCCAMachine;
      'B': Result := configB; // and cCCBMachine;
      'C': begin
          if FrontSWVersionIsInSet(aSettings, cFSWVConfigCodeCNotSwapped) then
            configC := Swap(configC);
          Result := configC; // and cCCCMachine;
        end;
    end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMMachineSettings.ExtractFrontType(var aSettings: TYMSettingsRecX): TFrontType;
begin
Result := ftZE80;
{
  with aSettings do begin
    case machSet.yMType of
      cYM80:   Result := ftZE80;
      cYM800:  Result := ftZE800;
      cYM900:  Result := ftZE800;
      cYM80I:  Result := ftZE80i;
      cYM800I: Result := ftZE800i;
      cYM900I: Result := ftZE800i;
    else
      Result := ftNone;
    end;

    if ExtractAWEMachineType(aSettings) = amtAC338 then
      Result := ftSWSInformatorSBC5;
  end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMMachineSettings.ExtractSensingHead(var aSettings: TYMSettingsRecX): TSensingHead;
begin
{TODO: TYMSettingsRec Obsolet
  Result := ValidateSensingHeadType(TSensingHead(aSettings.available.sensingHead));
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineSettings.ExtractYMMachineConfig(var aSettingsArr: TSettingsArr): TMachineYMConfigRec;

{TODO: TYMSettingsRec Obsolet
    //....................................................................
    function ExtractFrontSWOption(var aSettings: TYMSettingsRec): TFrontSWOption;
    begin
      Result := TFrontSWOption(aSettings.available.swOption);
    end;
    //....................................................................
    function GroupSpecificConfigIsEqual(var aSpec1,
      aSpec2: TGroupSpecificMachConfigRec;
      aAWEMachType: TAWEMachType;
      aFrontType: TFrontType): Boolean;
    var
      xCfgC1, xCfgC2: TConfigurationCode;
    begin
      if (aSpec1.spindle.contents <> cSpdlRngNotDefined) and
         (aSpec2.spindle.contents <> cSpdlRngNotDefined) then
      begin
        xCfgC1 := TConfigurationCode.Create(aSpec1.configA, aSpec1.configB, aSpec1.configC);

        xCfgC1.Filter      := cfMachine;
        xCfgC1.AWEMachType := aAWEMachType;
        xCfgC1.FrontType   := aFrontType;

        xCfgC2 := TConfigurationCode.Create(aSpec2.configA, aSpec2.configB, aSpec2.configC);

        xCfgC2.Filter      := cfMachine;
        xCfgC2.AWEMachType := aAWEMachType;
        xCfgC2.FrontType   := aFrontType;

        Result := (aSpec1.sensingHead = aSpec2.sensingHead) and
                  (aSpec1.aWEType = aSpec2.aWEType) and
                  (xCfgC1.A = xCfgC2.A) and
                  (xCfgC1.B = xCfgC2.B) and
                  (xCfgC1.C = xCfgC2.C) and
                  (aSpec1.inoperativePara[0] = aSpec2.inoperativePara[0]) and
                  ((aSpec1.inoperativePara[1] and not cIP1MFFClearing) =
                   (aSpec2.inoperativePara[1] and not cIP1MFFClearing));

        xCfgC1.Free;
        xCfgC2.Free;
      end
      else
        Result := False;
    end;
    //....................................................................
    function SpindleRangeIsCombined(var aCombined: TSpindleRangeRec;
      aSource1, aSource2: TSpindleRangeRec): Boolean;
    begin
      // to > from
      if (aSource1.stop >= aSource1.start) and (aSource2.stop >= aSource2.start) and
         // there is an intersection (es existiert eine Schnittmenge)
         // oder not((aSource2.stop < aSource1.start) or (aSource1.stop < aSourc2.start))
         ((aSource2.stop + 1 >= aSource1.start) and (aSource1.stop + 1 >= aSource2.start)) then
      begin
        if (aSource1.start < aSource2.start) then
          aCombined.start := aSource1.start
        else
          aCombined.start := aSource2.start;

        if (aSource1.stop > aSource2.stop) then
          aCombined.stop := aSource1.stop
        else
          aCombined.stop := aSource2.stop;

        Result := True;
      end
      else begin
        aCombined.contents := cSpdlRngNotDefined;
        Result             := False;
      end;
    end;
    //....................................................................
    procedure InsertGroupSpecificConfig(var aSpec: TGroupSpecificMachConfigRec;
      var aSettings: TYMSettingsRec);
    var
      xCfgCode: TConfigurationCode;
    begin
      UpdateYMMachineConfig(aSettings);
      with aSpec, aSettings do begin
        spindle.contents := available.spdl.contents;
        sensingHead      := ExtractSensingHead(aSettings);
        aWEType          := ExtractAWEType(aSettings);

            // Set Sensinghead dependency
        xCfgCode := TConfigurationCode.Create(ExtractConfigCode(aSettings, 'A'),
                                              ExtractConfigCode(aSettings, 'B'),
                                              ExtractConfigCode(aSettings, 'C'));

        xCfgCode.Filter      := cfNone;
        xCfgCode.FrontType   := ExtractFrontType(aSettings);
        xCfgCode.AWEMachType := ExtractAWEMachineType(aSettings);
        xCfgCode.SensingHead := sensingHead;
        xCfgCode.SetSensingHeadDependency;

        configA := xCfgCode.A;
        configB := xCfgCode.B;
        configC := xCfgCode.C;
        xCfgCode.Free;

        inoperativePara[0] := available.inoperativePara[0];
        inoperativePara[1] := available.inoperativePara[1];
      end;
    end;
    //....................................................................
{}
var
  i, j, xInserted: Integer;
  xCombined: TSpindleRangeRec;
begin
{TODO: TYMSettingsRec Obsolet
  Result := TYMSettingsUtils.InitMachineYMConfigRec;

  i := Low(aSettingsArr);
  xInserted := High(aSettingsArr) + 1;
  j := Low(Result.spec);

  while i <= High(aSettingsArr) do begin
    if (PYMSettings(@aSettingsArr[i].DataArrayOld)^.available.spdl.contents <> cSpdlRngNotDefined) and
       (PYMSettings(@aSettingsArr[i].DataArrayOld)^.available.spdl.contents <> 0) then
    begin
      if j <= High(Result.spec) then begin
        TYMSettingsUtils.FixVX12VX14SWOption(PYMSettings(@aSettingsArr[i].DataArrayOld)^);
        TMachineAttributes.FixMurIndInvInopSettings(PYMSettings(@aSettingsArr[i].DataArrayOld)^);

        if not TMachineAttributes.InopSettingsAvailable(PYMSettings(@aSettingsArr[i].DataArrayOld)^) then
          TMachineAttributes.SetDefaultInopSettings(PYMSettings(@aSettingsArr[i].DataArrayOld)^);

        InsertGroupSpecificConfig(Result.spec[j], PYMSettings(@aSettingsArr[i].DataArrayOld)^);

        with Result do begin
          inoperativePara[0] := inoperativePara[0] or spec[j].inoperativePara[0];
          inoperativePara[1] := inoperativePara[1] or spec[j].inoperativePara[1];
          inoperativePara[1] := inoperativePara[1] and not cIP1MFFSensingHeads;
        end;

        xInserted := i;
        Inc(j);
      end; // if
    end; // if

    Inc(i);
  end; // while

  if xInserted <= High(aSettingsArr) then begin
    with Result do begin
      i := Low(spec);
      while i <= High(spec) do begin
        if spec[i].spindle.contents <> cSpdlRngNotDefined then begin

          j := Low(spec);
          while (j <= High(spec)) do begin
            if (j <> i) and
               GroupSpecificConfigIsEqual(spec[i], spec[j],
                                          ExtractAWEMachineType(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^),
                                          ExtractFrontType(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^)) then
            begin
              if SpindleRangeIsCombined(xCombined, Result.spec[i].spindle, Result.spec[j].spindle) then begin
                if i < j then begin
                  spec[i].spindle.contents := xCombined.contents;

                  spec[i].inoperativePara[0] := spec[i].inoperativePara[0] or spec[j].inoperativePara[0];
                  spec[i].inoperativePara[1] := spec[i].inoperativePara[1] or spec[j].inoperativePara[1];

                  spec[j] := TYMSettingsUtils.InitGroupSpecificMachConfigRec;
                end
                else begin
                  spec[j].spindle.contents := xCombined.contents;

                  spec[j].inoperativePara[0] := spec[j].inoperativePara[0] or spec[i].inoperativePara[0];
                  spec[j].inoperativePara[1] := spec[j].inoperativePara[1] or spec[i].inoperativePara[1];

                  spec[i] := TYMSettingsUtils.InitGroupSpecificMachConfigRec;
                end;
              end; // if
            end; // if
            Inc(j);
          end; // while
        end; // if

        Inc(i);
      end; // while
    end; // with

    with Result, PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^ do
    begin
      aWEMachType      := ExtractAWEMachineType(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^);
      machBez          := machSet.machBez;
      defaultSpeedRamp := available.speedRamp;
      defaultSpeed     := channel.speed;
      longStopDef      := machSet.longStopDef;
      frontType        := ExtractFrontType(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^);
      frontSWOption    := ExtractFrontSWOption(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^);
      frontSWVersion   := machSet.yMSWVersion;
      checkLen         := splice.checkLen;
      cutRetries       := additional.extOption.cutRetries;
    end;
  end; // if
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineSettings.FilterMachineConfigCode(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  UpdateYMMachineConfig(aSettings);
  Filter := cfMachine;

  with aSettings.additional do begin
    configA := configA and not A;
    configB := configB and not B;
    configC := configC and not C;
  end;
{}
end;

//:---------------------------------------------------------------------------
class function TYMMachineSettings.FrontSWVersionIsInSet(var aSettings: TYMSettingsRecX; aFrontSWVersionSet: array of TYMSWVersionArr): Boolean;
var
  i: Integer;
begin
  Result := False;
{TODO: TYMSettingsRec Obsolet
  for i := Low(aFrontSWVersionSet) to High(aFrontSWVersionSet) do begin
    if EqualFrontSWVersion(aFrontSWVersionSet[i], aSettings.machSet.yMSWVersion) then
      Result := True;
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineSettings.InsertYMMachineConfig(aMachineYMConfig: TMachineYMConfigRec; var aSettings: TYMSettingsRecX);
    //....................................................................
{TODO: TYMSettingsRec Obsolet
    procedure InsertSensingHead(var aSettings: TYMSettingsRec;
      aMachConfig: TGroupSpecificMachConfigRec);
    begin

      aSettings.available.sensingHead := Ord(aMachConfig.sensingHead);
    end;
{}
    //....................................................................
var
  xGroupSpecific: TGroupSpecificMachConfigRec;
begin
{TODO: TYMSettingsRec Obsolet
  with aMachineYMConfig, aSettings do begin

    UpdateYMMachineConfig(aMachineYMConfig);

    xGroupSpecific := CastGroupSpecificConfig(aSettings.available.spdl);

    InsertSensingHead(aSettings, xGroupSpecific);

    additional.configA := AddConfigCodeA(xGroupSpecific.configA, additional.configA);
    additional.configB := AddConfigCodeB(xGroupSpecific.configB, additional.configB);
    additional.configC := AddConfigCodeC(xGroupSpecific.configC, additional.configC);

    if splice.checkLen <> 0 then
      splice.checkLen := checkLen;

    additional.extOption.cutRetries := cutRetries;
    machSet.longStopDef             := longStopDef;

  // %% Soll default speed auf die Settings einfluss nehmen ?
  //    defaultSpeedRamp: Byte;             // r/w: default only machines with Speed Simulation, [s]
  //    defaultSpeed: Word;                 // r/w: default winding speed, (500..1200), [500..1200 m/Min]

    machSet.yMSWVersion[0]       := 0; // r: Initialize
    available.machType           := cWTUnknownWinder; // r: Initialize
    machSet.machBez              := machBez;
    machSet.yMType               := cYM_UNKNOWN; // r: Initialize

    available.inoperativePara[0] := 0; // r: Initialize
    available.inoperativePara[1] := 0; // r: Initialize
  end;
{}
end;

//:---------------------------------------------------------------------------
procedure TYMMachineSettings.UpdateYMMachineConfig(var aMachineYMConfig: TMachineYMConfigRec);
begin
  MachineConfig := aMachineYMConfig;
end;

//:---------------------------------------------------------------------------
procedure TYMMachineSettings.UpdateYMMachineConfig(var aSettings: TYMSettingsRecX);
begin
{TODO: TYMSettingsRec Obsolet
  with aSettings.additional, aSettings.available do begin
    A           := configA;
    B           := configB;
    C           := configC;
    AWEMachType := ExtractAWEMachineType(aSettings);
    FrontType   := ExtractFrontType(aSettings);
  end;
{}
end;

//:---------------------------------------------------------------------------
function TYMMachineSettings.YMMachineConfigAvailable(var aSettingsArr: TSettingsArr): Boolean;
var
  i, xInserted: Integer;
begin
{TODO: TYMSettingsRec Obsolet
  i := Low(aSettingsArr);
  xInserted := High(aSettingsArr) + 1;

  while (i <= High(aSettingsArr)) and (xInserted > High(aSettingsArr)) do begin
    if (PYMSettings(@aSettingsArr[i].DataArrayOld)^.available.spdl.contents <> cSpdlRngNotDefined) and
       (PYMSettings(@aSettingsArr[i].DataArrayOld)^.available.spdl.contents <> 0) then
      xInserted := i;

    Inc(i);
  end;

  if xInserted <= High(aSettingsArr) then begin
    if ExtractAWEMachineType(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^) = amtAC338 then begin
      with TMachineAttributes.Create(PYMSettings(@aSettingsArr[xInserted].DataArrayOld)^) do
      try
        Result := IsMMMachineConfig;
      finally
        Free;
      end;
    end
    else
      Result := True;
  end
  else
    Result := False;
{}
end;


//:---------------------------------------------------------------------------
//:--- Class: TSensingHeadClassList
//:---------------------------------------------------------------------------
constructor TSensingHeadClassList.Create;
begin
  Capacity := cZESpdGroupLimit;
end;

//:---------------------------------------------------------------------------
destructor TSensingHeadClassList.Destroy;
begin
  Clear;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TSensingHeadClassList.Add(aSensingHeadClass: TSensingHeadClass): Integer;
//var
//  xPSensingHeadClass: PTSensingHeadClass;
begin
  Result := inherited Add(Pointer(aSensingHeadClass));
{wss
  New(xPSensingHeadClass);
  xPSensingHeadClass^ := aSensingHeadClass;
  Result              := inherited Add(xPSensingHeadClass);
{}
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    if Assigned(inherited Items[i]) then
      Dispose(inherited Items[i]);

  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TSensingHeadClassList.GetItems(Index: Integer): TSensingHeadClass;
//var
//  xPSensingHeadClass: PTSensingHeadClass;
begin
  Result := TSensingHeadClass(Integer(inherited Items[Index]));
{wss
  xPSensingHeadClass := inherited Items[Index];
  Result             := xPSensingHeadClass^;
{}
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.SetItems(Index: Integer; aValue: TSensingHeadClass);
begin
  inherited Items[Index] := Pointer(aValue);
end;

//:---------------------------------------------------------------------------
procedure TSensingHeadClassList.Sort;
begin
  inherited Sort(SensingHeadClassCompare);
end;

end.
