{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsTransformerCMD.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains classes for converting old YM-Settings in DB-table t_ym_settings
|                 to new XML YM-Settings in DB-table t_xml_ym_settings.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 22.11.2004  1.00  Nue | File created
| 27.04.2005  1.00  Wss | Init und Process werden separat aufgerufen, nicht mehr im Konstruktor direkt
|==========================================================================================}
// Aufruf des Programms wie folgt:
// SettingsTransformerCMD n:<Mapfilename> i:<Mapfile-ID>
// Bei Aufruf: SettingsTransformerCMD  ,werden die Defaultwerte im Programm übernommen. Das heisst, ID=1 aus der
// Tabelle t_xml_mapfile.
program SettingsTransformerCMD;
{$APPTYPE CONSOLE}
uses
{$IFDEF MemCheck}
  MemCheck,
{$ENDIF}
  SysUtils,
  ActiveX,
  Windows,
  u_ISCommon,
  YMParaDef in 'YMParaDef.pas',
  YMSettingsTransformer in 'YMSettingsTransformer.pas',
  YMParaDBAccess in 'YMParaDBAccess.pas',
  YMParaUtils in 'YMParaUtils.pas';

var
  xhresult: Longint;

{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('SettingsTransformerCMD');
{$ENDIF}
  try
    xhresult:= CoInitialize(Nil); //initializes the Component Object Model(COM) library
    if ((xhresult <> S_OK) and (xhresult <> S_FALSE)) then
      raise Exception.Create ( 'CoInitializ failed ');

    with TYMSettingsTransformer.Create do
    try
      if Init then begin
        Process;
        //Schreibt in Installations-Logfile
        WriteMsgToLog(Format('Successful    SettingsTransformerCMD: Successful transformed %d settings from t_ym_settings to t_xml_ym_settings.',[TransformedSets]),False );
        WriteMsgToLog(Format('Successful    SettingsTransformerCMD: Successful transformed %d settings from t_ym_settings to t_xml_ym_settings.',[TransformedSets]),True );
      end;
    finally
      Free;
      CoUninitialize;
    end; // with TYMSettingsTransformer.Create
//    with TYMSettingsTransformer.Create do
//    try
//      //Schreibt in Installations-Logfile
//      WriteMsgToLog(Format('Successful    SettingsTransformerCMD: Successful transformed %d settings from t_ym_settings to t_xml_ym_settings.',[TransformedSets]),False );
//      WriteMsgToLog(Format('Successful    SettingsTransformerCMD: Successful transformed %d settings from t_ym_settings to t_xml_ym_settings.',[TransformedSets]),True );
//    finally
//      Free;
//      CoUninitialize;
//    end; // with TYMSettingsTransformer.Create
  except
    on e:Exception do begin
      //Schreibt Error in Installations-Logfile
      WriteMsgToLog('Error         SettingsTransformerCMD: SettingsTransformer failed: ' + e.Message, False);
      WriteMsgToLog('Error         SettingsTransformerCMD: SettingsTransformer failed: ' + e.Message, True);
    end;
  end;
end.
