object Form1: TForm1
  Left = 264
  Top = 124
  Width = 870
  Height = 836
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object labTab1: TmmLabel
    Left = 16
    Top = 72
    Width = 158
    Height = 20
    Caption = 'Tabelle: t_ym_settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object labTab2: TmmLabel
    Left = 16
    Top = 416
    Width = 190
    Height = 20
    Caption = 'Tabelle: t_xml_ym_settings'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object labState: TmmLabel
    Left = 304
    Top = 408
    Width = 140
    Height = 24
    Caption = 'Transformation'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = False
    AutoLabel.LabelPosition = lpLeft
  end
  object labNewSets1: TmmLabel
    Left = 696
    Top = 416
    Width = 68
    Height = 13
    Caption = 'Inserted Sets: '
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object labNewSets2: TmmLabel
    Left = 768
    Top = 408
    Width = 7
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmDBGrid1: TmmDBGrid
    Left = 16
    Top = 96
    Width = 617
    Height = 297
    DataSource = mmDataSource1
    TabOrder = 0
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'c_YM_set_id'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_YM_set_name'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_head_class'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_template_set'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'c_color'
        Visible = True
      end>
  end
  object mmDBGrid2: TmmDBGrid
    Left = 16
    Top = 440
    Width = 817
    Height = 297
    DataSource = mmDataSource2
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = mmDBGrid2DblClick
  end
  object edIDs: TmmEdit
    Left = 80
    Top = 32
    Width = 153
    Height = 21
    Hint = 'Form: 3,7,8'
    Color = clWindow
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object bIDs: TmmButton
    Left = 256
    Top = 32
    Width = 113
    Height = 25
    Caption = 'Uebernahme der ID'#39's'
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bStart: TmmButton
    Left = 424
    Top = 32
    Width = 121
    Height = 25
    Caption = 'StartConversion'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 4
    Visible = True
    OnClick = bStartClick
    AutoLabel.LabelPosition = lpLeft
  end
  object DBNavigator1: TDBNavigator
    Left = 16
    Top = 744
    Width = 768
    Height = 25
    DataSource = mmDataSource2
    VisibleButtons = [nbFirst, nbPrior, nbNext, nbLast, nbDelete, nbRefresh]
    TabOrder = 5
    OnClick = DBNavigator1Click
  end
  object mmADOConnection1: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;Persist Security Info=Tru' +
      'e;User ID=mmsystemsql;Initial Catalog=MM_Winding;Data Source=WET' +
      'SRVMM5;Use Procedure for Prepare=1;Auto Translate=True;Packet Si' +
      'ze=4096;Workstation ID=WETNUE2000;Use Encryption for Data=False;' +
      'Tag with column collation when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    DBName = 'MM_Winding'
    SQLServerName = 'WETSRVMM5'
    UserName = 'MMSystemSQL'
    Password = 'netpmek32'
    Left = 80
    Top = 48
  end
  object mmDataSource1: TmmDataSource
    DataSet = mmADODataSet1
    Left = 216
    Top = 208
  end
  object mmADODataSet1: TmmADODataSet
    Active = True
    Connection = mmADOConnection1
    CursorType = ctStatic
    CommandText = 'select * from t_ym_settings'
    Parameters = <>
    Left = 176
    Top = 208
  end
  object mmADODataSet2: TmmADODataSet
    Active = True
    Connection = mmADOConnection1
    CursorType = ctStatic
    MaxRecords = 100
    CommandText = 'select  * from t_xml_ym_settings'
    Parameters = <>
    Left = 160
    Top = 480
  end
  object mmDataSource2: TmmDataSource
    DataSet = mmADODataSet2
    OnUpdateData = mmDataSource2UpdateData
    Left = 216
    Top = 480
  end
end
