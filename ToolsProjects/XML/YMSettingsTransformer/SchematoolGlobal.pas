unit SchematoolGlobal;

interface

uses Forms, MSXML2_TLB, XMLGlobal, sysutils, ConfigurationPersistence;


const
  cPrefix: array [0..3] of string = ('ZE-', 'WSC-', 'LZE-', 'Tst-');
  // Nur Bindestrich und ein weiterer Bezeichner erlaubt ==> siehe 'GetNameFragments'
  cPostfix: array [0..1] of string = ('-S', '-C');

  // Name der Sektion im Inifile wo die Nummern hochgez�hlt werden
  cMapfileNumbers = 'Numbers';

type
  TSchematoolSettings = class(TBaseXMLSettings)
  private
    function GetRecentFilesXML: IXMLDOMElement;
  public
    destructor Destroy; override;
    procedure DeleteAllMapForms;
    // Pfad zur Konfigurationsdatei
    function GetSettingsFilePath: string; override;
    class function Instance: TSchematoolSettings;
    function LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement; override;
    function SaveForm(aForm: TForm; aName: string): IXMLDOMElement; override;
    property RecentFilesXML: IXMLDOMElement read GetRecentFilesXML;
  end;

const
  cAtrMapName          = 'MapName';
  cRecentMapfileName   = 'RecentMapfile';
  cSelectedElementName = 'SelectedElement';
  cMapElementInfo      = 'Map Element Info';
  cAtrXMLViewerVisible = 'XMLViewerVisible';

  cRecentFilesXMLRootElement = 'RecentFilesMRU_XML';
    cXPRecentFilesXML        = cXPDocumentBody + '/' + cRecentFilesXMLRootElement;
implementation

uses
  windows, LoepfeGlobal, shlobj, comobj, filectrl{, MapElementInfo};

(*---------------------------------------------------------
  Speichert die Settings am Ende des Programms ab
----------------------------------------------------------*)
destructor TSchematoolSettings.Destroy;
begin
  SaveSettings;
  inherited;
end;// destructor TSchematoolSettings.Destroy;

(*---------------------------------------------------------
  L�scht alle Form Eleemnte
----------------------------------------------------------*)
procedure TSchematoolSettings.DeleteAllMapForms;
var
  i: Integer;
  xNodeList: IXMLDOMNodeList;
begin
  if assigned(DOM) then begin
    xNodeList := DOM.selectNodes(cXPFormElement + Format('[starts-with(@Name,''%s'')]', [cMapElementInfo]));
    for i := 0 to xNodeList.length - 1 do
      xNodeList.item[i].parentNode.removeChild(xNodeList.item[i]);
  end;// if assigned(DOM) then begin
end;// procedure TSchematoolSettings.DeleteAllForms;

function TSchematoolSettings.GetRecentFilesXML: IXMLDOMElement;
begin
  Result := nil;
  if assigned(DOM) then
    result := GetCreateElement(cXPRecentFilesXML, DOM);
end;

(* -----------------------------------------------------------
  Gibt den Dateinamen f�r die Settings zur�ck
-------------------------------------------------------------- *)
function TSchematoolSettings.GetSettingsFilePath: string;
var
  item: PItemIDList;
  buffer: array[0..MAX_PATH] of Char;
begin
  OleCheck(SHGetSpecialFolderLocation(0, CSIDL_APPDATA, item));
  SHGetPathFromIDList(item, buffer);

  SetString(result, buffer, StrLen(buffer));

  result := IncludeTrailingBackslash(result)+ 'Loepfe\Schematool Settings.xml';
end;// function TSchematoolSettings.GetSettingsFilePath: string;

(* -----------------------------------------------------------
  Zugriff auf den Singleton
-------------------------------------------------------------- *)  
class function TSchematoolSettings.Instance: TSchematoolSettings;
begin
  Result := TSchematoolSettings(AccessInstance(1));
end;// class function TSchematoolSettings.Instance: TLKTermSettings;

(* -----------------------------------------------------------
  L�dt die Position und den Status eines Formulars aus dem DOM
-------------------------------------------------------------- *)
function TSchematoolSettings.LoadForm(aForm: TForm; aName: string; aDialog: boolean = false): IXMLDOMElement;
begin
  result := inherited LoadForm(aForm, aName);
//  if (aForm is TfrmMapElementInfo) and (assigned(result)) then
//    TfrmMapElementInfo(aForm).MapVersion := AttributeToStringDef(cAtrMapName, result, '');
end;// procedure TSchematoolSettings.LoadForm(aForm: TForm);

(* -----------------------------------------------------------
  Speichert die Position und den Status eines Formulars im DOM
  Wenn kein Name angegeben ist, dann wird die Form unter dem eigenen Namen gespeichert.
  Der R�ckgabewert ist das DOMElement in das die Parameter gespeichert wurden.
-------------------------------------------------------------- *)
function TSchematoolSettings.SaveForm(aForm: TForm; aName: string): IXMLDOMElement;
begin
  result := inherited SaveForm(aForm, aName);
//  if (aForm is TfrmMapElementInfo) and (assigned(result)) then
//    result.setAttribute(cAtrMapName, TfrmMapElementInfo(aForm).MapVersion);
end;// procedure TSchematoolSettings.SaveForm(aForm: TForm);

initialization
finalization
  // Singleton freigeben und Settings speichern
  TSchematoolSettings.ReleaseInstance;

end.
