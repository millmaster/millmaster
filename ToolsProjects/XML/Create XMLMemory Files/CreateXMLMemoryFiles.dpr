{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: CreateXMLMemoryFiles.dpr
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: - Liest aus der DB die Memory Settings A-Q und erstellt
|                   zu jedem Memory-Set ein XML File.
| Info..........: -
| Develop.system: Windows W2k
| Target.system.: Windows W2k, XP, W2k3
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 02.02.2005  1.00  SDo | File created
|==============================================================================}

program CreateXMLMemoryFiles;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  Windows,
  Dialogs,
  Classes,
  AdoDbAccess,
  ActiveX,
  LoepfeGlobal;

var
    xCoInitializeResult : HResult;
    xFileName : String;
    xXMLFile  : TStringList;
    xXMLData  : String;
begin
  // Insert user code here

  xCoInitializeResult := CoInitialize(Nil); //initializes the Component Object Model(COM) library

  if ((xCoInitializeResult <> S_OK) and (xCoInitializeResult <> S_FALSE)) then
      MessageDlg('CoInitialize failed', mtError, [mbOk], 0);

  try
      xXMLFile :=  TStringList.Create;
      with TAdoDBAccess.Create(1, TRUE) do
      try
         Init;
         with Query[cPrimaryQuery] do begin
              SQL.Text := 'select * from t_xml_ym_settings ' +
                          'where c_YM_set_id < 0 and c_YM_set_id > -27 order by c_YM_set_name';
              Open;
              First;
              while not EOF do begin
                   xFileName := FieldByName('c_YM_set_name').asString;
                   xXMLFile.Text := StringReplace(  FieldByName('c_xml_setting').asString, cCRLF, '', [rfReplaceAll]);
                   xXMLFile.SaveToFile(xFileName + '.xml');
                   Next;
              end;
         end;
      finally
         free;
         xXMLFile.Free;
      end;
  except
     on E: Exception do begin
        MessageDlg('Error : ' + e.Message, mtError, [mbOk], 0);
     end;
  end;
  CoUninitialize;
end.