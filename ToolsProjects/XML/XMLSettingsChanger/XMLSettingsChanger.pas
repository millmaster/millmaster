{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMSettingsTransformer.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains classes for converting old YM-Settings in DB-table t_ym_settings
|                 to new XML YM-Settings in DB-table t_xml_ym_settings.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 10.11.2004  1.00  Nue | File created
| 24.03.2005  1.00  Wss | SensingHeadClass war noch nicht richtig behandelt
| 20.04.2005  1.00  Wss | Hashcode wird nun fix in der Klasse TXMLSettingsAccess behandelt
| 27.04.2005  1.00  Wss | - Destroy war nicht "override" -> Destroy wurde nie durchlaufen
                          - DB Init und Process separat aufrufen aus Projekt und nicht mehr direkt im Konstruktor
|==========================================================================================}
(*
-  F�r die ZE unterst�tzt 1 Mapfile alle Varianten

-  Folgende Informator Versionen gilt es aus heutiger Sicht zu unterst�tzen :

Informator (68k)	Informator C (PPC)	XML Map File Name	INF-Funktionalit�t	AWE-Funktionalit�t
S 5.40	X	WSC-1234.MMZ	Spectra	Spectra
S 5.70	X		Spectra	Spectra
S 6.10	S 6.10		Spectra+, Zenit FP	Spectra+, Zenit FP
V 6.14	V 6.12	WSC-????.MMZ	Zenit FP	Zenit FP
S 6.20	S 6.20		Zenit FP	Zenit FP
X	S 7.00
*)
unit XMLSettingsChanger;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Dialogs, ADODBAccess, MMEventLog,
  XMLGlobal, BaseGlobal, MSXML2_TLB, LoepfeGlobal;

type
  TXMLSettingsChanger = class(TObject)
  private
    mDB: TADODBAccess;
    mEventLog: TEventLogWriter;
//    mPatternXMLPath: string;
//    mPatternXMLString: string;
    mYMSetIDs: string;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    function Process: Boolean;
  published
  end;

implementation

uses
  mmCS, FileCtrl, JclStrings, MMUGlobal;

//------------------------------------------------------------------------------
constructor TXMLSettingsChanger.Create;
begin
  mYMSetIDs          := '';
  mEventLog          := TEventLogWriter.Create('Millmaster', '', ssApplication, 'TYMSettingsTransformer: ', True);
  mDB                := TAdoDBAccess.Create(3, True); // 2 Query-handles
end; // TYMSettingsTransformer.Create cat:No category

//------------------------------------------------------------------------------
destructor TXMLSettingsChanger.Destroy;
begin
  mDB.Free;
  mEventLog.Free;

  inherited Destroy;
end; // TYMSettingsTransformer.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GenerateHashcode
 *  Klasse:           TXMLSettingsAccess
 *  Kategorie:        No category 
 *  Argumente:        (aXMLData, aHash1, aHash2)
 *
 *  Kurzbeschreibung: Generiert den Hashcode und gibt den Normalisierten XMLStream wieder zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)

//------------------------------------------------------------------------------
function TXMLSettingsChanger.Process: Boolean;
var
  i: Integer;
  xXMLDom: DOMDocumentMM;
  xXMLStr: string;
const
  cQrySelectMapfileByID = 'select c_mapfile from t_xml_mapfile where c_mapfile_id=:c_mapfile_id ';
  cQrySelectMapfileByName =
    ' select c_mapfile from t_xml_mapfile ' +
    ' where c_mapfile_name like ''ZE-01%'' ' +
    ' order by c_mapfile_name desc';

  cSelectSettings = 'select c_ym_set_id, c_xml_setting from t_xml_ym_settings ';
  cUpdateSettings =
    'update t_xml_ym_settings set c_hashcode1 =:c_hashcode1, c_hashcode2 =:c_hashcode2, c_xml_setting =:c_xml_setting ' +
    'where c_ym_set_id = :c_ym_set_id';
var
  xID: Integer;
  xSize: Word;
  xXMLData: string;
  xXMLSettingsRec: PXMLSettingsRec;
  xMapfile: string;
  xStr: string;
  xSourceSets: Integer;
  xCodeSiteEnabled: Boolean;
  xXPathList: TStringList;
  xXMLNode: IXMLDOMNode;
  //............................................................
  function GenerateHashcode(aXMLDom: DOMDocumentMM; out aXMLData: String; out aHash1: integer; out aHash2: integer): Boolean;
  var
    xXMLData: String;
  begin
    Result   := False;
    aXMLData := '';
    aHash1   := 0;
    aHash2   := 0;
    if assigned(aXMLDom) then begin
      aXMLData := aXMLDom.xml;
      aXMLData := StringReplace(Trim(aXMLData), #13#10, '', [rfReplaceAll]);
      aXMLData := StringReplace(aXMLData, #10#13, '', [rfReplaceAll]);
      aXMLData := StringReplace(aXMLData, #09, '', [rfReplaceAll]);

      aXMLDom.setProperty('NewParser', true);
      // Stream in das Dokument laden und parsen
      if aXMLDom.loadXML(aXMLData) and Assigned(aXMLDom.DocumentElement) then begin
        // F�r die Berechnung des Hashcodes darf nur das DocumentElement verwendet werden,
        // da der Kommentar und die ProcessingInformations nicht zum Setting geh�ren.
        HashByMM(aXMLDom.DocumentElement, aHash1, aHash2);
        Result := True;
      end;
    end;
  end;
  //............................................................
  procedure UpdateSetting(aID: Integer; aXMLDom: DOMDocumentMM);
  var
    xHash1, xHash2: Integer;
    xXMLData: String;
  begin
    if GenerateHashcode(aXMLDom, xXMLData, xHash1, xHash2) then begin
      with mDB.Query[1] do
      try
        SQL.Text := cUpdateSettings;
        ParamByName('c_hashcode1').AsInteger  := xHash1;
        ParamByName('c_hashcode2').AsInteger  := xHash2;
        ParamByName('c_xml_setting').AsString := xXMLData;
        ParamByName('c_ym_set_id').AsInteger  := aID;
        ExecSQL;
      except
        on e: Exception do
          mEventLog.Write(etError, Format('UpdateSetting %d failed: %s', [xID, e.Message]));
      end;
    end else
      mEventLog.Write(etError, Format('UpdateSetting %d failed by GenerateHashcode', [aID]));
  end;
  //............................................................
begin
  Result     := False;
  xXPathList := TStringList.Create;
  for i:=1 to ParamCount do begin
    if Pos('f:',  LowerCase(ParamStr(i))) = 1 then begin
      xStr := StrAfter('f:', ParamStr(i));
      if FileExists(xStr) then begin
        xXPathList.LoadFromFile(xStr);
        CodeSite.SendString('XPath file', xStr);
        if xXPathList.Count = 0 then
          mEventLog.Write(etError, Format('XPath file %s empty!', [xStr]))
        else
          CodeSite.SendStringList('XPath file', xXPathList);
      end else
        mEventLog.Write(etError, Format('XPath file %s does not exist!', [xStr]));
    end; // if Pos('x:'...

    if Pos('i:', LowerCase(ParamStr(i))) = 1 then begin
      mYMSetIDs := StrAfter('i:', ParamStr(i));
      CodeSite.SendString('Filter at SetID', mYMSetIDs);
    end;
  end; //for

//  xXPathList.Values['/LoepfeBody/YMSetting/Basic/Channel/Splice/Switch'] := 'swOff';
  if xXPathList.Count = 0 then begin
    Writeln('Usage:' + cCRLF);
    Writeln(ExtractFileName(ParamStr(0)) + ' f:conversionfile.txt [ i:YMSetID ]');
    Writeln(cCRLF + 'Contain example of "conversionfile.txt":' + cCRLF);
    Writeln('  XPath=NewValue');
    Writeln('  XPath=NewValue');
    Writeln(cCRLF + 'Filter for YMSetIDs through registry at Loepfe Debug:' + cCRLF);
    Writeln('  ConvertYMSetIDs = "ID1,ID2,IDn"');
    Exit;
  end;

  if mDB.Init then begin
    with mDB.Query[0] do
    try
      SQL.Text := cSelectSettings;
//      mYMSetIds := '-26'; //'-26,-25,-24,-23';
      if Trim(mYMSetIDs) = '' then
        mYMSetIds := GetRegString(cRegLM, cRegMMDebug, 'ConvertYMSetIDs');

      if Trim(mYMSetIDs) <> '' then
        SQL.Add(Format('where c_ym_set_id in (%s)', [mYMSetIDs]));

      SQL.Add('order by c_ym_set_id');
      Open;
      while not EOF do begin
        xID     := FieldByName('c_ym_set_id').AsInteger;
        xXMLStr := FieldByName('c_xml_setting').AsString;
        xXMLDom := XMLStreamToDOM(xXMLStr);

        if Assigned(xXMLDom) then begin
          for i:=0 to xXPathList.Count-1 do begin
            xStr := Trim(xXPathList.Names[i]);
            CodeSite.SendString('XPath', xStr);
            xXMLNode := xXMLDom.selectSingleNode(xStr);
            if Assigned(xXMLNode) then begin
              if xXMLNode.childNodes.Length = 1 then
                if xXMLNode.childNodes.item[0].nodeValue <> NULL then begin
                  CodeSite.SendString('OldValue', xXMLNode.childNodes.item[0].nodeValue);
                  xStr := Trim(xXPathList.Values[xStr]);
                  CodeSite.SendString('NewValue', xStr);
                  xXMLNode.childNodes.item[0].nodeValue := xStr;
                end;
            end;
          end; // for i

          UpdateSetting(xID, xXMLDom);
        end;
        Next;
      end; // while not EOF
    except
      on e: Exception do
        raise EMMException.Create(e.message + 'in YM-Settings Transformation to XML;');
    end;
  end else
    mEventLog.Write(etError, 'mDB.Init failed: ' + mDB.DBErrorTxt);
end;

end.

