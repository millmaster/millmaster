{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsTransformerCMD.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: This unit contains classes for converting old YM-Settings in DB-table t_ym_settings
|                 to new XML YM-Settings in DB-table t_xml_ym_settings.
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.0 / ModelMaker 7.25
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.06.2005  1.00  Wss | File created
|==========================================================================================}
// Aufruf des Programms wie folgt:
// XMLSettingsChangerCMD f:file i:SetID
{
  Datei hat folgenden Aufbau. Die Werte sind immer als String anzusehen und m�ssen XML konform sein -> Floatzahlen
  "XPath"="NeuerWert"
  "XPath"="NeuerWert"
{}

program XMLSettingsChangerCMD;
{$APPTYPE CONSOLE}
uses
  MemCheck,
  Dialogs,
  SysUtils,
  ActiveX,
  Windows,
  XMLSettingsChanger in 'XMLSettingsChanger.pas';

var
  xhresult: Longint;

begin
{$IFDEF MemCheck}
  MemChk('XMLSettingsChangerCMD');
{$ENDIF}
  try
    xhresult:= CoInitialize(Nil); //initializes the Component Object Model(COM) library
    if ((xhresult <> S_OK) and (xhresult <> S_FALSE)) then
      raise Exception.Create ( 'CoInitializ failed ');

    with TXMLSettingsChanger.Create do
    try
      Process;
    finally
      Free;
      CoUninitialize;
    end; //
  except
    on e:Exception do
      ShowMessage('XMLSettingsChanger failed: ' + e.Message);
  end;
end.
