(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SecurTabMgr.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Tool fuer die MM-Security (Controlled Actions)

                  Funktionen:
                        - Erstellt die Tabellen t_security_Work,
                          t_security_Default und t_security_Customer
                        - Generiert ein Query von einer ausgewaehlten Tabelle
                        - Convertiert die Daten aus der Tabelle t_Security in
                          eine ausgewaehlte Tabelle
                        - Zeigt von einer ausgewaehlten Tabelle den FormName und
                          FormCaption, wenn diese gleich sind.
                          (Sollte unterschiedlich sein -> kommt aus Konvertierung)
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 30.10.2002  1.00  SDo | Datei erstellt
|=============================================================================*)
unit SecurTabMgr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, mmRadioGroup, Grids, mmStringGrid, AdvGrid,
  mmExtStringGrid, IvMlDlgs, mmSaveDialog,
  MMSecurity, MMSecurityConfig;

type
  TForm1 = class(TForm)
    mmRadioGroup1: TmmRadioGroup;
    gbConvert: TGroupBox;
    bConvert: TButton;
    gbMakeQuery: TGroupBox;
    bCreateQuery: TButton;
    gbCheckFormCaptions: TGroupBox;
    bCheckForm: TButton;
    sgFormAndCaptions: TmmExtStringGrid;
    Label1: TLabel;
    Label2: TLabel;
    rbCreateQuery: TRadioButton;
    rbDefaultQuery: TRadioButton;
    SaveDialog1: TSaveDialog;
    bCreateTable: TButton;
    Button1: TButton;
    procedure bConvertClick(Sender: TObject);
    procedure mmRadioGroup1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bCheckFormClick(Sender: TObject);
    procedure bCreateQueryClick(Sender: TObject);
    procedure bCreateTableClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    mTable : String;
    mFileSteram  : TFileStream;
  public
    { Public declarations }
    function ConvertSecurityTabTo(aTable: String):Boolean;
    function CreateAQuery(aTable: String):Boolean;
    procedure ShowSameFormAndCaption(aTable: String);
    procedure CreateTable(aTable: String);
    function CheckTables: Boolean;
  end;


  function GetForm(aFormName: string; aControlList: TControlList; xDataList: TStringList): Boolean;
  function Sort_Forms(Item1, Item2: Pointer): Integer;

var
  Form1: TForm1;

const
     cSQLSelect    = 'select * from t_security';
     cSectionForms = '[ControlledForms]';
     cSecurityTab  = 't_security_Work';

     cTableSecurity_Work = 't_security_Work';
     cTableSecurity_Default = 't_security_Default';
     cTableSecurity_Customer = 't_security_Customer';

     cCreateSecurityTable  = 'use MM_Winding ' +
                             'if not exists (select id from sysobjects where name like ''%s'' ) ' +
                             'begin ' +
                             'create table %s ' +
                             '(c_Appl varchar(50) not null, ' +
                             'c_Form varchar(50) not null, ' +
                             'c_Caption varchar(70) not null, ' +
                             'c_Function varchar(50) not null, ' +
                             'c_Info varchar(100) not null, ' +
                             'c_Security varchar(500) ' +
                             'constraint pk_%s primary key clustered(c_Appl,c_Form,c_Function ) ) ' +
                             'end ';

     {
     cCreateAllSecurityTables = 'use MM_Winding ' +
                                'if not exists (select id from sysobjects where name like ''t_Security_Work'' ) ' +
                                'begin ' +
                                '  create table t_Security_Work ' +
                                '  ( c_Appl varchar(50) not null, ' +
                                '    c_Form varchar(50) not null, ' +
                                '    c_Caption varchar(70) not null, ' +
                                '    c_Function varchar(50) not null, ' +
                                '    c_Info varchar(100) not null, ' +
                                '    c_Security varchar(500) ' +
                                '    constraint pk_t_Security_Work primary key clustered(c_Appl,c_Form,c_Function ) '+
                                '   ) '+
                                'end ' +

                                'if not exists (select id from sysobjects where name like ''t_Security_Default'') ' +
                                'begin  ' +
                                '  create table t_Security_Default ' +
                                '  ( c_Appl varchar(50) not null, ' +
                                '    c_Form varchar(50) not null, ' +
                                '    c_Caption varchar(70) not null, ' +
                                '    c_Function varchar(50) not null, ' +
                                '    c_Info varchar(100) not null, ' +
                                '    c_Security varchar(500) ' +
                                '    constraint pk_t_Security_Default primary key clustered(c_Appl,c_Form,c_Function ) ' +
                                '   ) ' +
                                'end ' +

                                'if not exists (select id from sysobjects where name like ''t_Security_Customer'') ' +
                                'begin ' +
                                '  create table t_Security_Customer ' +
                                '  ( c_Appl varchar(50) not null, ' +
                                '    c_Form varchar(50) not null, ' +
                                '    c_Caption varchar(70) not null, ' +
                                '    c_Function varchar(50) not null, ' +
                                '    c_Info varchar(100) not null, ' +
                                '    c_Security varchar(500) ' +
                                '    constraint pk_t_Security_Customer primary key clustered(c_Appl,c_Form,c_Function ) ' +
                                '   ) ' +
                                'end ';

      }



implementation

{$R *.DFM}
uses LoepfeGlobal, AdoDbAccess, Db , FileCtrl, mmList;
//------------------------------------------------------------------------------
function  Sort_Forms(Item1, Item2: Pointer): Integer;
begin
 //Sortiert nach Forms
  if TSecurityRec(Item1^).Appl = TSecurityRec(Item2^).Appl  then begin
     if TSecurityRec(Item1^).ApplForm = TSecurityRec(Item2^).ApplForm  then
        result := 0
     else
       if TSecurityRec(Item1^).ApplForm < TSecurityRec(Item2^).ApplForm  then
          result := -1
       else
          result := 1;
  end else
      if TSecurityRec(Item1^).Appl < TSecurityRec(Item2^).Appl  then
         result := -1
      else
         result := 1;
end;
//------------------------------------------------------------------------------
function GetForm(aFormName: string; aControlList: TControlList; xDataList: TStringList): Boolean;
var
  i, xIndex: Integer;
  xNrOfComp: Integer;
begin
  Result := False;
  // first clear previous stored information in destination list
  aControlList.Clear;
  // are there some security info for this form? Check for block [aFormName].
  // [aForm]    <<--
  // 1
  // FormCaption
  // Control1 ## Control1 comment @@ Grouplist
  xIndex := xDataList.IndexOf(GetSectionString(aFormName));
  if xIndex >= 0 then begin
    // [aForm]      <-- xIndex
    // 1            <-- inc(xIndex)
    // FormCaption
    // Control1 ## Control1 comment @@ Grouplist
    inc(xIndex); // seek to NrOfComponents and read
    try
      try
        // get number of components and copy all lines
        xNrOfComp := StrToInt(xDataList.Strings[xIndex]);
      except
        Exit; // there is something wrong in the format of text block
      end;
      // [aForm]
      // 1
      // FormCaption
      // Control1 ## Control1 comment @@ Grouplist   <---- inc(xIndex, 2)
      inc(xIndex, 1); // seek to first component list
      for i := 0 to xNrOfComp-1 do begin
        aControlList.AddControlGroupString(xDataList.Strings[xIndex]);
        inc(xIndex);
      end;

      // succeeded only on end of filling
      Result := True;
    except
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bConvertClick(Sender: TObject);
var  xMSG : String;
begin
  if ConvertSecurityTabTo(mTable) then begin
     xMSG := Format('Convert table t_Security to %s successful.', [mTable] );
     MessageDLG(xMSG, mtInformation,[mbOk], 0);
  end else begin
     xMSG := Format('Convert table t_Security to %s failed.', [mTable] );
     MessageDLG(xMSG, mtError, [mbOk], 0);
  end;
end;
//------------------------------------------------------------------------------
function TForm1.ConvertSecurityTabTo(aTable: String):Boolean;
var xList, xForms :TStringList;
    x, n, i: Integer;
    xData, xSQL, xTable, xMSG : String;
    xAppl, xForm, xFunction, xInfo, xSecurity :String;
    xRet : Boolean;
    xRec          : PSecurity;
    xControlList  : TControlList;
    xSecurList    : TmmList;   //Liste mit allen aufbereiteten controlled Actions
begin
  Screen.Cursor := crHourGlass;
  xRet := TRUE;

  xList :=  TStringList.Create;
  xList.Sorted:= FALSE;

  xForms :=  TStringList.Create;
  xForms.Sorted:= TRUE;

  xControlList:= TControlList.Create;
  xSecurList := TmmList.Create;

  xTable:= aTable;

  with TAdoDBAccess.Create(2) do begin
      try
        if Init then begin
          with Query[cPrimaryQuery] do begin
            SQL.Text := cSQLSelect;
            try
              Open;
              First;

              while not Eof do begin

                  xList.clear;
                  xForms.clear;

                  //Appl. Name
                  xAppl := FieldByName('c_applicationname').asString;
                  {
                  if CompareText( xAppl , 'MMStyle') = 0 then begin
                     beep;
                     xData :=  FieldByName('c_text').asString;
                  end;
                  }

                  //Data (MemoField)
                  xData := StringReplace( FieldByName('c_text').asString,
                                           #$D#$A,
                                          '$',
                                          [rfReplaceAll] );  

                  //Data in Liste schreiben (Zeilenumbruch = neuer Eintrag)
                  //-> MemoField nachbilden
                  i:= length(xData);
                  n:=0;
                  while n <= i do begin
                     x:= pos('$', xData);
                     if x > 0 then begin
                        xList.Add( copy(xData,1, x-1)   );
                        delete(xData,1, x);
                        inc(n,x);
                     end else begin
                        xList.Add(xData ); //Letztes Element
                        break;
                     end;
                  end;

                  
                  //Alle kontrollierte Forms ermitteln
                  x := xList.IndexOf(cSectionForms);

                  if x < 0 then begin
                     xMSG := Format('No controlled form foud in %s.' + #10#13 +
                                    'Check table %s and application %s.',
                                    ['t_Security', 't_Security', xAppl] );
                     MessageDlg( xMSG , mtWarning, [mbOK], 0);
                  end;

                  if x >= 0 then
                     xForms.CommaText := xList.Strings[x + 1];

                  for x := 0 to xForms.Count - 1 do begin
                    xForm := xForms.Strings[x];

                    // Form-Caption ermitteln
                    //[ControlledForms]
                    //Forms
                    //[Form]       <-- Index n
                    //DataCount
                    //FormCaption  <-- inc(x, 2)
                    //Data         <-- GetForm()
                    n := xList.IndexOf(GetSectionString(xForm));
                    inc(n, 2);

                    //Form-Daten ermitteln
                    GetForm(xForm, xControlList, xList);
                    // bei 1 starten, da hier FormCaption nicht ben�tigt wird
                    for n := 0 to xControlList.Count - 1 do begin
                      new(xRec);

                      xRec.Appl         := xAppl;
                      xRec.ApplForm     := xForm;
                      xRec.FormCaption  := xForm;
                      xRec.ApplFunction := xControlList.Name[n];
                      xRec.Info         := xControlList.Comment[n];

                      xSecurity         := xControlList.GetControlGroupString(n);
                      i:= Pos('@@', xSecurity);
                      delete( xSecurity, 1, i+1 );
                      xRec.SecurityGroups := xSecurity;

                      xSecurList.Add(xRec);
                    end;
                  end;
                next;
              end; //END While

              //Sortieren
              xSecurList.Sort(Sort_Forms);

              //Daten auf DB schreiben
              for n:= 0 to xSecurList.Count-1 do begin
                  xRec       := xSecurList.Items[n];
                  xAppl      := Trim(xRec.Appl);
                  xForm      := Trim(xRec.ApplForm);
                  xFunction  := Trim(xRec.ApplFunction);
                  xInfo      := Trim(xRec.Info);
                  xSecurity  := Trim(xRec.SecurityGroups);

                  xSQL := Format('insert into %s (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security) ' +
                                 ' VALUES(''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'') ',
                                 [xTable, xAppl, xForm, xFunction, xForm, xInfo, xSecurity]);

                  with Query[cSecondaryQuery] do begin
                       close;
                       SQL.Text := xSQL;
                       try
                         //Insert
                         ExecSQL;
                       except
                         //Update
                         xSQL := Format('update %s set c_Security = ''%s'' ' +
                                        'where c_Appl =  ''%s'' and c_Form =  ''%s'' and c_Function = ''%s'' ',
                                        [xTable, xSecurity, xAppl, xForm, xFunction] );
                         SQL.Text := xSQL;
                         try
                           ExecSQL;
                         except
                         end;
                       end;
                  end;
              end;

              close;
            except
                on e: Exception do begin
                  xRet := FALSE;
                end;
            end;
           end;
        end;

      except
        on E: Exception do begin
           //ShowMessage(e.Message);
           xRet := FALSE;
        end;

      end;
      free;
  end;

  xSecurList.Free;
  xControlList.Free;
  xList.free;
  xForms.free;

  Screen.Cursor := crDefault;
  Result := xRet;
end;
//------------------------------------------------------------------------------
procedure TForm1.mmRadioGroup1Click(Sender: TObject);
begin
  case mmRadioGroup1.ItemIndex of
     0: mTable := cTableSecurity_Work;
     1: mTable := cTableSecurity_Default;
     2: mTable := cTableSecurity_Customer;
  end;

  gbConvert.Caption:= Format('Convert from table ''t_Security'' to ''%s'' ',[mTable]);
  //gbMakeQuery.Caption:= Format('Create a query from table ''%s'' ',[mTable]);
  rbCreateQuery.Caption := Format('Create a query from table ''%s'' ',[mTable]);
  rbDefaultQuery.Caption := Format('Create a ''security default'' Query from table ''%s'' ',[mTable]);

  gbCheckFormCaptions.Caption := Format('Check controlled form names and captions in table ''%s''. (Form names and captions must be diffrent) ',[mTable]);

  bCreateTable.Caption := Format('Create table ''%s'' ',[mTable]);

end;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
var xRet : Boolean;
    xSQlServer :String;
begin
  mmRadioGroup1.OnClick(sender);
  Label2.Caption :='';

  mmRadioGroup1.Items.Clear;
  mmRadioGroup1.Items.Add (cTableSecurity_Work);
  mmRadioGroup1.Items.Add (cTableSecurity_Default);
  mmRadioGroup1.Items.Add (cTableSecurity_Customer);
  mmRadioGroup1.ItemIndex:=0;

  xRet:= CheckTables;

  if not xRet then
     Button1.Click
  else begin
     xRet := TRUE;
     bCheckForm.Enabled            := xRet;
     bConvert.Enabled              := xRet;
     bCreateQuery.Enabled          := xRet;
     gbCheckFormCaptions.Enabled   := xRet;
  end;
  {
  xRet:= CheckTables;

  bCheckForm.Enabled            := xRet;
  bConvert.Enabled              := xRet;
  bCreateQuery.Enabled          := xRet;
  gbCheckFormCaptions.Enabled   := xRet;
  }

  xSQlServer := GetRegString(cRegLM, cRegMMCommonPath, cRegSQLServerName, '');
  if xSQlServer = '' then begin
     xSQlServer := GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost, '');
     if xSQlServer <> '' then
         SetRegString(cRegLM, cRegMMCommonPath, cRegSQLServerName, xSQlServer);
  end;


  with TAdoDBAccess.Create(1, false) do begin
        Init;
        Self.Caption :=  Format('%s  [Connection to : %s]',[ Self.Caption,HostName]);
        Free;
  end;

end;
//------------------------------------------------------------------------------
function TForm1.CreateAQuery(aTable: String): Boolean;
var xAppl, xForm, xFunction, xCaption, xInfo, xSecurity, xText, xTxt, xTable : String;
    xList : TStringList;
    xPath, xFileName, xMSG, xSQL, xSQL0, xSQLInsert, xSQLUpdate : String;
    xChar : String[1];
begin


 xFileName := aTable;
 xTable    := aTable;

 if rbDefaultQuery.Checked then
    xFileName := cTableSecurity_Default;

 delete(xFileName ,1,1);

 xFileName := StringReplace(xFileName, '_', '', [rfReplaceAll]);

 xFileName := xFileName + '.qry';

 xChar :=  xFileName[1];
 xChar := UpperCase( xChar );
 xFileName[1] := xChar[1] ;


 xAppl := Application.ExeName;
 xAppl := ExtractFileName(xAppl);

 xList := TStringList.create;


 //Header
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 xText := '/*------------------------------------------------------------------------------------------------';
 xList.Add(xText);

 xText := 'Description: Write MM security in to table ' + xTable;
 if rbDefaultQuery.Checked then
    xText := 'Description: Write MM security in to table ' + cTableSecurity_Default;
 xList.Add(xText);

 xText := 'File crated by ' + xAppl;
 xList.Add(xText);

 xText := 'Date : ' + DateTimeToStr(Now);
 xList.Add(xText);

 xText := '------------------------------------------------------------------------------------------------*/';
 xList.Add(xText);

 xText := '';
 xList.Add(xText);
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


 xText := 'use MM_Winding';
 xList.Add(xText);

 xText := 'go';
 xList.Add(xText);

 xSQL := Format( 'select * from %s order by c_Appl',[xTable]);

 try
    with TAdoDBAccess.Create(1, false) do begin
          if Init then
             with Query[cPrimaryQuery] do begin
                SQL.Text := xSQL;
                Open;
                First;
                if rbDefaultQuery.Checked then xTable := cTableSecurity_Default;

                while not Eof do begin
                    xAppl     := FieldByName('c_Appl').asString;
                    xForm     := FieldByName('c_Form').asString;
                    xFunction := FieldByName('c_Function').asString;
                    xCaption  := FieldByName('c_Caption').asString;
                    xInfo     := FieldByName('c_Info').asString;
                    xSecurity := FieldByName('c_Security').asString;


                    xSQL0:= Format(' if ( select count(c_Appl) from %s ' +
                                        'where  c_Appl = ''%s'' ' +
                                        'and c_Form    = ''%s'' ' +
                                        'and c_Function =''%s'') < 1 ' ,
                                        [xTable, xAppl, xForm, xFunction]);


                    xSQLInsert:= Format('insert into %s (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security) ' +
                                        'values (''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'')',
                                       [xTable, xAppl, xForm, xFunction, xCaption, xInfo, xSecurity]);

                    xSQLUpdate:= Format('update %s set c_Security = ''%s'', c_Caption = ''%s'', c_Info = ''%s'' ' +
                                    'where c_Appl = ''%s'' and  c_Form = ''%s'' and c_Function = ''%s'' ',
                                    [xTable, xSecurity, xCaption, xInfo, xAppl, xForm, xFunction] );

                    xSQL :=  Format(' %s %s else %s',[xSQL0, xSQLInsert, xSQLUpdate]);

                    xList.Add(xSQL);

                    next;
                end;
             end;
           Free;
     end;


 //Footer
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     xTxt := 'Insert / Update';
     xText:='';
     xList.Add(xText);

     xText := 'Print''------------------------------------------------------------------------------------------------'' ';
     xList.Add(xText);

     xText := Format('Print''%s security into %s on DB MM_Winding done on ''+RTRIM(CONVERT(varchar(30), GETDATE())) ', [xTxt, xTable]);
     xList.Add(xText);

     xText := 'Print''------------------------------------------------------------------------------------------------'' ';
     xList.Add(xText);
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

     xPath := GetRegString(cRegLM, cRegMillMasterPath, cRegMillMasterRootPath, 'c:\temp');
     xPath := xPath + '\Queries\Security';

     ForceDirectories(xPath);

     SaveDialog1.FileName:= xFileName ;
     SaveDialog1.InitialDir := xPath;


     if SaveDialog1.Execute then begin
        ForceDirectories(SaveDialog1.InitialDir);
        xPath :=  SaveDialog1.FileName;
        xList.SaveToFile( xPath );
     end;


 except
     on e: Exception do begin
        xMSG := Format('Error: %s', [e.message] );
        MessageDLG(xMSG, mtError	,[mbOk], 0);
     end;
 end;
 xList.Free;

end;
//------------------------------------------------------------------------------
procedure TForm1.bCheckFormClick(Sender: TObject);
begin
  sgFormAndCaptions.ClearNormalCells;
  sgFormAndCaptions.RowCount:=2;
  ShowSameFormAndCaption(mTable);
  sgFormAndCaptions.SetFocus;
  if sgFormAndCaptions.Cells[0,1] = '' then
     Label2.Caption := Format('Form count = %d',[0])
  else
     Label2.Caption := Format('Form count = %d',[sgFormAndCaptions.RowCount-1]);
end;
//------------------------------------------------------------------------------
procedure TForm1.ShowSameFormAndCaption(aTable: String);
var xSQL, xMSG :String;
    xRow: Integer;
begin

 Screen.Cursor :=  crHourGlass;

 xSQL := Format( 'select distinct c_Appl, c_Form, c_Caption  from %s where c_Form = c_Caption order by c_Appl, c_Form',[aTable]);

 try
     with TAdoDBAccess.Create(1, false) do begin
          if Init then
             with Query[cPrimaryQuery] do begin
                SQL.Text := xSQL;
                Open;
                First;
                xRow:=3;
                while not Eof do begin
                     xRow := sgFormAndCaptions.RowCount;
                     sgFormAndCaptions.Cells[0,xRow-1]:=  FieldByName('c_Appl').asString;
                     sgFormAndCaptions.Cells[1,xRow-1]:=  FieldByName('c_Form').asString;
                     sgFormAndCaptions.Cells[2,xRow-1]:=  FieldByName('c_Caption').asString;
                     inc(xRow);
                     sgFormAndCaptions.RowCount := xRow;
                     next;
                end;
             end;
           Free;
     end;
     sgFormAndCaptions.RowCount := xRow-1;

 except
     on e: Exception do begin
        xMSG := Format('Error: %s', [e.message] );
        MessageDLG(xMSG, mtError	,[mbOk], 0);
     end;
 end;

  Screen.Cursor :=  crDefault;
end;
//------------------------------------------------------------------------------
procedure TForm1.bCreateQueryClick(Sender: TObject);
begin
 Screen.Cursor := crHourGlass;
 CreateAQuery(mTable);
 Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
procedure TForm1.CreateTable(aTable: String);
var xSQL, xMSG :String;
    xRow: Integer;
begin

 Screen.Cursor :=  crHourGlass;

 xSQL := Format( 'select * from %s',[aTable]);

 try
     with TAdoDBAccess.Create(1, false) do begin
          if Init then
             try
               with Query[cPrimaryQuery] do begin
                    SQL.Text := xSQL;
                    Open;

                    xMSG := Format('Table %s alraedy exists.', [aTable]);
                    MessageDLG(xMSG, mtInformation	,[mbOk], 0);
               end;
             except
                xSQL := Format(cCreateSecurityTable, [aTable, aTable, aTable] );
               {
                xSQL := Format( 'create table %s ' +
                                '(c_Appl varchar(50) not null, ' +
                                'c_Form varchar(50) not null, ' +
                                'c_Caption varchar(70) not null, ' +
                                'c_Function varchar(50) not null, ' +
                                'c_Info varchar(100) not null, ' +
                                'c_Security varchar(500) ' +
                                'constraint pk_%s primary key clustered(c_Appl,c_Form,c_Function ) ) ',[aTable, aTable] );

                }
                 with Query[cPrimaryQuery] do begin
                      SQL.Text := xSQL;
                      ExecSQL;
                 end;
                 xMSG := Format('Table %s crated successful.', [aTable]);
                 MessageDLG(xMSG, mtInformation	,[mbOk], 0);

             end;
     end;
 except
     on e: Exception do begin
        xMSG := Format('Error in CreateTable : %s', [e.message] );
        MessageDLG(xMSG, mtError	,[mbOk], 0);
     end;
 end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bCreateTableClick(Sender: TObject);
var xRet : Boolean;
begin
  Screen.Cursor := crHourGlass;
  CreateTable(mTable);

  xRet:= CheckTables;

  bCheckForm.Enabled            := xRet;
  bConvert.Enabled              := xRet;
  bCreateQuery.Enabled          := xRet;
  gbCheckFormCaptions.Enabled   := xRet;

  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------
function TForm1.CheckTables: Boolean;
var xSQL, xMSG, xTabError : String;
begin

 Result := TRUE;
 try
     with TAdoDBAccess.Create(1, false) do begin
          if Init then begin
             try
               xSQL := Format( 'select * from %s',[cTableSecurity_Work]);
               with Query[cPrimaryQuery] do begin
                    SQL.Text := xSQL;
                    Open;
                    close;
                end;
             except
                Result := FALSE;
                xTabError := cTableSecurity_Work;
             end;

             try
               xSQL := Format( 'select * from %s',[cTableSecurity_Default]);
               with Query[cPrimaryQuery] do begin
                    SQL.Text := xSQL;
                    Open;
                    close;
                end;
             except
                Result := FALSE;
                xTabError := xTabError + ', ' + cTableSecurity_Default;
             end;

             try
               xSQL := Format( 'select * from %s',[cTableSecurity_Customer]);
               with Query[cPrimaryQuery] do begin
                    SQL.Text := xSQL;
                    Open;
                    close;
                end;
             except
                Result := FALSE;
                xTabError := xTabError + ', ' + cTableSecurity_Customer;
             end;

             if not Result then begin
                if Pos(',', xTabError) = 1 then delete(xTabError, 1, 1);
                xMSG := Format('Table(s) %s does not exists.',[xTabError]);
                MessageDLG(xMSG, mtError	,[mbOk], 0);
             end;


          end;
          free;
       end;
  except
    on e: Exception do begin
        xMSG := Format('Error in CheckTables : %s', [e.message] );
        MessageDLG(xMSG, mtError	,[mbOk], 0);
        Result := FALSE;
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.Button1Click(Sender: TObject);
var xSQL, xMSG :String;
    xRow: Integer;
    xRet : Boolean;
    xTables : String;
begin

 Screen.Cursor :=  crHourGlass;

 //xSQL :=cCreateAllSecurityTables;

 xTables := '';
 with TAdoDBAccess.Create(1, false) do begin
    if Init then begin
       try
         with Query[cPrimaryQuery] do begin

      //        cTableSecurity_Work = 't_security_Work';
     //cTableSecurity_Default = 't_security_Default';
     //cTableSecurity_Customer = 't_security_Customer';
              xTables :=  cTableSecurity_Work;
              xSQL := Format(cCreateSecurityTable, [cTableSecurity_Work, cTableSecurity_Work, cTableSecurity_Work] );
              SQL.Text := xSQL;
              ExecSQL;

              xTables := xTables + '; ' + cTableSecurity_Default;
              xSQL := Format(cCreateSecurityTable, [cTableSecurity_Default, cTableSecurity_Default, cTableSecurity_Default] );
              SQL.Text := xSQL;
              ExecSQL;

              xTables := xTables + '; ' + cTableSecurity_Customer;
              xSQL := Format(cCreateSecurityTable, [cTableSecurity_Customer, cTableSecurity_Customer, cTableSecurity_Customer] );
              SQL.Text := xSQL;
              ExecSQL;

              xMSG := 'Tables created successful.';
              MessageDLG(xMSG, mtInformation	,[mbOk], 0);
              xRet := TRUE;

              Close;
          end;

        except
          on e: Exception do begin
            xMSG := Format('Error in CreateTables : %s', [e.message] );
            MessageDLG(xMSG, mtError	,[mbOk], 0);
            xRet := FALSE;
          end;
        end;
     end;

     Free;
 end;

 bCheckForm.Enabled            := xRet;
 bConvert.Enabled              := xRet;
 bCreateQuery.Enabled          := xRet;
 gbCheckFormCaptions.Enabled   := xRet;

 Screen.Cursor := crDefault;
end;

end.
