object Form1: TForm1
  Left = 162
  Top = 183
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Secuity Table Manager'
  ClientHeight = 533
  ClientWidth = 652
  Color = clAppWorkSpace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000000
    00000000000000001991000000007000199108787880700019910FEFCF807000
    19910FEFCF8070001B910FEFCF8000001BB80FFFCF8091000110FFFFFF809100
    00B0444848400100008077777777000000000000000000000777000000000780
    0888700B000007B78BBBB7B0000007B77BBBBB0000000778877770000000F8FF
    0000300000003000000030000000300000003000000010000000180000001800
    000038000000F8FF00008867000080070000800F0000801F0000803F0000}
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmRadioGroup1: TmmRadioGroup
    Left = 15
    Top = 10
    Width = 230
    Height = 87
    Caption = 'Select a table to prepare / work on'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemIndex = 0
    Items.Strings = (
      't_Security_Work'
      't_Security_Default'
      't_Security_Backup')
    ParentFont = False
    TabOrder = 0
    OnClick = mmRadioGroup1Click
    CaptionSpace = True
  end
  object gbConvert: TGroupBox
    Left = 260
    Top = 10
    Width = 385
    Height = 55
    Caption = 'Convert from tab. t_Security to'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object bConvert: TButton
      Left = 20
      Top = 24
      Width = 350
      Height = 25
      Caption = 'Convert'
      TabOrder = 0
      OnClick = bConvertClick
    end
  end
  object gbMakeQuery: TGroupBox
    Left = 260
    Top = 72
    Width = 385
    Height = 89
    Caption = 'Query generator'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object bCreateQuery: TButton
      Left = 20
      Top = 60
      Width = 350
      Height = 25
      Caption = 'Create a Query'
      TabOrder = 0
      OnClick = bCreateQueryClick
    end
    object rbCreateQuery: TRadioButton
      Left = 20
      Top = 20
      Width = 357
      Height = 17
      Caption = 'Insert Query'
      Checked = True
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      TabStop = True
    end
    object rbDefaultQuery: TRadioButton
      Left = 20
      Top = 36
      Width = 357
      Height = 17
      Caption = 'Create a '#39'security default'#39' Query'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clAqua
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
  end
  object gbCheckFormCaptions: TGroupBox
    Left = 8
    Top = 168
    Width = 640
    Height = 353
    Caption = 'Check Form-Captions'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clAqua
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    object Label1: TLabel
      Left = 10
      Top = 55
      Width = 254
      Height = 13
      Caption = 'Shows all controlled forms with the same form caption.'
      Color = clInfoBk
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentColor = False
      ParentFont = False
    end
    object Label2: TLabel
      Left = 10
      Top = 335
      Width = 32
      Height = 13
      Caption = 'Label2'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object bCheckForm: TButton
      Left = 10
      Top = 20
      Width = 193
      Height = 25
      Caption = 'Check Form name and Captions'
      TabOrder = 0
      OnClick = bCheckFormClick
    end
    object sgFormAndCaptions: TmmExtStringGrid
      Left = 10
      Top = 72
      Width = 620
      Height = 260
      ColCount = 3
      DefaultColWidth = 200
      DefaultRowHeight = 21
      FixedCols = 0
      RowCount = 2
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goDrawFocusSelected, goRowSelect]
      ParentFont = False
      TabOrder = 1
      Visible = True
      AutoNumAlign = False
      AutoSize = False
      VAlignment = vtaTop
      EnhTextSize = False
      EnhRowColMove = False
      SortFixedCols = False
      SizeWithForm = False
      Multilinecells = False
      SortDirection = sdAscending
      SortFull = True
      SortAutoFormat = True
      SortShow = False
      EnableGraphics = False
      SortColumn = 0
      HintColor = clYellow
      SelectionColor = clHighlight
      SelectionTextColor = clHighlightText
      SelectionRectangle = False
      SelectionRTFKeep = False
      HintShowCells = False
      OleDropTarget = False
      OleDropSource = False
      OleDropRTF = False
      PrintSettings.FooterSize = 0
      PrintSettings.HeaderSize = 0
      PrintSettings.Time = ppNone
      PrintSettings.Date = ppNone
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.PageNr = ppNone
      PrintSettings.Title = ppNone
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'MS Sans Serif'
      PrintSettings.Font.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'MS Sans Serif'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'MS Sans Serif'
      PrintSettings.FooterFont.Style = []
      PrintSettings.Borders = pbNoborder
      PrintSettings.BorderStyle = psSolid
      PrintSettings.Centered = False
      PrintSettings.RepeatFixedRows = False
      PrintSettings.RepeatFixedCols = False
      PrintSettings.LeftSize = 0
      PrintSettings.RightSize = 0
      PrintSettings.ColumnSpacing = 0
      PrintSettings.RowSpacing = 0
      PrintSettings.TitleSpacing = 0
      PrintSettings.Orientation = poPortrait
      PrintSettings.FixedWidth = 0
      PrintSettings.FixedHeight = 0
      PrintSettings.UseFixedHeight = False
      PrintSettings.UseFixedWidth = False
      PrintSettings.FitToPage = fpNever
      PrintSettings.PageNumSep = '/'
      PrintSettings.NoAutoSize = False
      PrintSettings.PrintGraphics = False
      HTMLSettings.Width = 100
      Navigation.AllowInsertRow = False
      Navigation.AllowDeleteRow = False
      Navigation.AdvanceOnEnter = False
      Navigation.AdvanceInsert = False
      Navigation.AutoGotoWhenSorted = False
      Navigation.AutoGotoIncremental = False
      Navigation.AutoComboDropSize = False
      Navigation.AdvanceDirection = adLeftRight
      Navigation.AllowClipboardShortCuts = False
      Navigation.AllowSmartClipboard = False
      Navigation.AllowRTFClipboard = False
      Navigation.AdvanceAuto = False
      Navigation.InsertPosition = pInsertBefore
      Navigation.CursorWalkEditor = False
      Navigation.MoveRowOnSort = False
      Navigation.ImproveMaskSel = False
      Navigation.AlwaysEdit = False
      ColumnSize.Save = False
      ColumnSize.Stretch = True
      ColumnSize.Location = clRegistry
      CellNode.Color = clSilver
      CellNode.NodeType = cnFlat
      CellNode.NodeColor = clBlack
      SizeWhileTyping.Height = False
      SizeWhileTyping.Width = False
      MouseActions.AllSelect = False
      MouseActions.ColSelect = False
      MouseActions.RowSelect = False
      MouseActions.DirectEdit = False
      MouseActions.DisjunctRowSelect = False
      MouseActions.AllColumnSize = False
      MouseActions.CaretPositioning = False
      IntelliPan = ipVertical
      URLColor = clBlack
      URLShow = False
      URLFull = False
      URLEdit = False
      ScrollType = ssNormal
      ScrollColor = clNone
      ScrollWidth = 16
      ScrollProportional = False
      ScrollHints = shNone
      OemConvert = False
      FixedFooters = 0
      FixedRightCols = 0
      FixedColWidth = 198
      FixedRowHeight = 21
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'MS Sans Serif'
      FixedFont.Style = []
      WordWrap = False
      ColumnHeaders.Strings = (
        'Application'
        'Form name'
        'Form caption')
      Lookup = False
      LookupCaseSensitive = False
      LookupHistory = False
      BackGround.Top = 0
      BackGround.Left = 0
      BackGround.Display = bdTile
      Hovering = False
      Filter = <>
      FilterActive = False
      AutoLabel.LabelPosition = lpLeft
      ColWidths = (
        198
        192
        223)
      RowHeights = (
        21
        21)
    end
  end
  object bCreateTable: TButton
    Left = 15
    Top = 103
    Width = 230
    Height = 25
    Caption = 'Create Table'
    TabOrder = 4
    OnClick = bCreateTableClick
  end
  object Button1: TButton
    Left = 15
    Top = 136
    Width = 230
    Height = 25
    Caption = 'Create all Security Tables'
    TabOrder = 5
    OnClick = Button1Click
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.qry'
    Filter = 'Queries (*.qry) |*.qry|All Files (*.*)|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofPathMustExist, ofCreatePrompt, ofNoTestFileCreate, ofEnableIncludeNotify]
    Left = 184
    Top = 40
  end
end
