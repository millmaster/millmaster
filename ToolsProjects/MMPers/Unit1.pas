unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, NTCommon, Privileg, UserMan,
  u_UserManagement, u_CodeSite, Registry, ExtCtrls, ImgList, Buttons,
  u_MachineNameFinder, ComCtrls;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    NTUserMan1: TNTUserMan;
    Button2: TButton;
    Edit2: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Panel1: TPanel;
    BitBtn1: TBitBtn;
    ImageList1: TImageList;
    BitBtn2: TBitBtn;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);


  private
    { Private declarations }
    mStatustext: String;
    mMachine, mDomain : String;

    //function InstallMMPersonalToServer_(aServer : String): Boolean;
    //function AssignMMPersonalFromServer_(aServer: String): Boolean;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation


{$R *.DFM}
Uses LoepfeGlobal;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  Edit1.Text := NTUserMan1.LocalComputer;
  try
    Edit2.Text := NTUserMan1.GetPrimaryDomainServerName;
  except
    Edit2.Text := NTUserMan1.LocalComputer;
  end;

  Edit2.Text := StringReplace( Edit2.Text,
                              '\\',
                              '',
                              [rfReplaceAll] );

   ImageList1.GetBitmap(0, BitBtn1.Glyph);
   ImageList1.GetBitmap(0, BitBtn2.Glyph);

   BitBtn1.ShowHint:= TRUE;
   BitBtn2.ShowHint:= TRUE;

   BitBtn1.Hint:= 'Get server machine names';
   BitBtn2.Hint:= BitBtn1.Hint;

   // Setzt Hint Color & Zeit
   Application.HintColor:= $00FFFF80; // HEX -> 00,B,G,R;  //clAqua;
   Application.HintPause:=150;
   Application.HintHidePause := 5000;
end;
//------------------------------------------------------------------------------
{
function TForm1.InstallMMPersonalToServer_(aServer: String): Boolean;
    function HasLabMaster:Boolean;
     var xKey: String;
    begin
      with registry.TRegistry.Create do begin
           result := FALSE;
           RootKey:= HKEY_LOCAL_MACHINE;
           xKey := 'SOFTWARE\Loepfe\LabMaster';
           result := KeyExists(xKey);
      end;
    end;

var xNTUserMan    : TNTUserMan;
    xNTPrivilege  : TNTPrivilege;

    xMachineName, xDomain, xPDCMachineName, xTxt: String;
    xUsersGroup, xNormalUserPlusGroup, xAdminGroup, xDomainAdminGroup : String;

    xIsPDC : Boolean;
    x, n   : Integer;
    xList           : TStrings;
    xGroupPrivilegs : TStringList;

begin

  xNTUserMan:= TNTUserMan.Create(NIL);

  xMachineName := aServer;
  xMachineName := CheckServerName(xMachineName);


  xUsersGroup             :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_USERS);
  xNormalUserPlusGroup    :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_POWER_USERS);
  if xNormalUserPlusGroup = '' then
     xNormalUserPlusGroup :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_SYSTEM_OPS);
  xAdminGroup             :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_ADMINS);

  xNormalUserPlusGroup:= '"' + xNormalUserPlusGroup + '"';

  try
    xDomain         := xNTUserMan.GetDomainNameForServer(xMachineName);
  except
    xDomain :='';
  end;

  try
    xPDCMachineName := xNTUserMan.GetPDCForDomain(xDomain);
  except
    xPDCMachineName := '';
  end;

  xIsPDC := FALSE;
  if CompareText(xMachineName, xPDCMachineName) = 0 then
     xIsPDC := TRUE;

  xNTUserMan.DomainName  := xDomain;
  xNTUserMan.MachineName := xMachineName ;

  //'Domain Admins' Name ermitteln
  for x:= 0 to xNTUserMan.GlobalGroups.Count-1 do begin
      xDomainAdminGroup:= '"' + xNTUserMan.GlobalGroups.strings[x] + '"';
      if Pos('DOM' , UpperCase(xDomainAdminGroup) ) > 0 then
         if Pos('ADM' , UpperCase(xDomainAdminGroup) ) > 0 then
            break;
       xDomainAdminGroup:= '';
  end;

  //Add MM-User
  for x:= 0 to High(cPDC_MMUsers) do begin
     if xNTUserMan.Users.IndexOf( cPDC_MMUsers[x].User ) < 0 then
         xNTUserMan.AddUser(cPDC_MMUsers[x].User, cPDC_MMUsers[x].PWD);

     xNTUserMan.UserName:= cPDC_MMUsers[x].User;
     xNTUserMan.UserInfo.Comment:= cPDC_MMUsers[x].Description;
     xNTUserMan.UserInfo.FullName:= cPDC_MMUsers[x].FullName;

      //xTxt := Format( 'New User %s added on machine %s.', [mNTUserMan.UserName, xMMServerName] ) ;
      //mLog.Write( etInformation, xTxt  );
  end;

  //Add MM-Groups
  if xIsPDC then begin
    for x:= 0 to High(cPDC_MMGroups) do begin
        //Globale gruppen nur auf PDC
        if xNTUserMan.GlobalGroups.IndexOf(cPDC_MMGroups[x].Group)<0 then
            xNTUserMan.GlobalGroups.Add(cPDC_MMGroups[x].Group);

        xNTUserMan.GlobalGroupName := cPDC_MMGroups[x].Group;
        xNTUserMan.GlobalGroupComment := cPDC_MMGroups[x].Description;
    end;
  end else begin
     for x:= 0 to High(cPDC_MMGroups) do begin
        if xNTUserMan.LocalGroups.IndexOf(cPDC_MMGroups[x].Group)<0 then
           xNTUserMan.LocalGroups.Add(cPDC_MMGroups[x].Group);

        xNTUserMan.LocalGroupName := cPDC_MMGroups[x].Group;
        xNTUserMan.LocalGroupComment := cPDC_MMGroups[x].Description;
     end;
  end;

  if HasLabMaster then begin
     //Add MM-LabUser
     for x:= 0 to High(cMMLab_Users) do begin
        if xNTUserMan.Users.IndexOf( cMMLab_Users[x].User ) < 0 then
           xNTUserMan.AddUser(cMMLab_Users[x].User, cPDC_MMUsers[x].PWD);

        xNTUserMan.UserName:= cMMLab_Users[x].User;
        xNTUserMan.UserInfo.Comment:= cMMLab_Users[x].Description;
        xNTUserMan.UserInfo.FullName:= cMMLab_Users[x].FullName;
     end;

       //Add MM-Groups
     if xIsPDC then begin
        for x:= 0 to High(cMMLab_Groups) do begin
            //Globale gruppen nur auf PDC
            if xNTUserMan.GlobalGroups.IndexOf(cMMLab_Groups[x].Group)<0 then
               xNTUserMan.GlobalGroups.Add(cMMLab_Groups[x].Group);

            xNTUserMan.GlobalGroupName := cMMLab_Groups[x].Group;
            xNTUserMan.GlobalGroupComment := cMMLab_Groups[x].Description;
        end;
     end else begin
         for x:= 0 to High(cMMLab_Groups) do begin
            if xNTUserMan.LocalGroups.IndexOf(cMMLab_Groups[x].Group)<0 then
               xNTUserMan.LocalGroups.Add(cMMLab_Groups[x].Group);

            xNTUserMan.LocalGroupName := cMMLab_Groups[x].Group;
            xNTUserMan.LocalGroupComment := cMMLab_Groups[x].Description;
         end;
     end;
  end;


  //Domain Admins , PowerUser Domain Users ersetzen  WindingMaster
  for x:= 0 to High(cAssingUsersToGroups) do begin

     //Domain Admins
     cAssingUsersToGroups[x].MemberOfGroup := StringReplace( cAssingUsersToGroups[x].MemberOfGroup,
                                                             cDomAdminGr,
                                                             xDomainAdminGroup,
                                                             [rfReplaceAll] );
     //Existiert nicht
     if cAssingUsersToGroups[x].MemberOfGroup = cDomAdminGr then
        cAssingUsersToGroups[x].MemberOfGroup := '';

     //Power User oder System Operator
     cAssingUsersToGroups[x].MemberOfGroup := StringReplace( cAssingUsersToGroups[x].MemberOfGroup,
                                                             cPuSoGr,
                                                             xNormalUserPlusGroup,
                                                             [rfReplaceAll] );

     if cAssingUsersToGroups[x].MemberOfGroup = cPuSoGr then
        cAssingUsersToGroups[x].MemberOfGroup := '';

     //Adminstrators
     cAssingUsersToGroups[x].MemberOfGroup := StringReplace( cAssingUsersToGroups[x].MemberOfGroup,
                                                             cAdminGr,
                                                             xAdminGroup,
                                                             [rfReplaceAll] );

     //Existiert nicht
     if cAssingUsersToGroups[x].MemberOfGroup = cAdminGr then
        cAssingUsersToGroups[x].MemberOfGroup := '';

     //,, durch , ersetzen
     cAssingUsersToGroups[x].MemberOfGroup := StringReplace( cAssingUsersToGroups[x].MemberOfGroup,
                                                             ',,',
                                                             ',',
                                                             [rfReplaceAll] );

     //Users
     cAssingUsersToGroups[x].MemberOfGroup := StringReplace( cAssingUsersToGroups[x].MemberOfGroup,
                                                             cLocUser,
                                                             xUsersGroup,
                                                             [rfReplaceAll] );
     //Existiert nicht
     if cAssingUsersToGroups[x].MemberOfGroup = cLocUser then
        cAssingUsersToGroups[x].MemberOfGroup := '';
  end;

    //Domain Admins , PowerUser Domain Users ersetzen   LabMaster
  for x:= 0 to High(cAssingLabUsersToGroups) do begin

     //Domain Admins
     cAssingLabUsersToGroups[x].MemberOfGroup := StringReplace( cAssingLabUsersToGroups[x].MemberOfGroup,
                                                             cDomAdminGr,
                                                             xDomainAdminGroup,
                                                             [rfReplaceAll] );
     //Existiert nicht
     if cAssingLabUsersToGroups[x].MemberOfGroup = cDomAdminGr then
        cAssingLabUsersToGroups[x].MemberOfGroup := '';

     //Power User oder System Operator
     cAssingLabUsersToGroups[x].MemberOfGroup := StringReplace( cAssingLabUsersToGroups[x].MemberOfGroup,
                                                             cPuSoGr,
                                                             xNormalUserPlusGroup,
                                                             [rfReplaceAll] );

     if cAssingLabUsersToGroups[x].MemberOfGroup = cPuSoGr then
        cAssingLabUsersToGroups[x].MemberOfGroup := '';

     //Adminstrators
     cAssingLabUsersToGroups[x].MemberOfGroup := StringReplace( cAssingLabUsersToGroups[x].MemberOfGroup,
                                                             cAdminGr,
                                                             xAdminGroup,
                                                             [rfReplaceAll] );

     //Existiert nicht
     if cAssingLabUsersToGroups[x].MemberOfGroup = cAdminGr then
        cAssingLabUsersToGroups[x].MemberOfGroup := '';

     //,, durch , ersetzen
     cAssingLabUsersToGroups[x].MemberOfGroup := StringReplace( cAssingLabUsersToGroups[x].MemberOfGroup,
                                                             ',,',
                                                             ',',
                                                             [rfReplaceAll] );

     //Users
     cAssingLabUsersToGroups[x].MemberOfGroup := StringReplace( cAssingLabUsersToGroups[x].MemberOfGroup,
                                                             cLocUser,
                                                             xUsersGroup,
                                                             [rfReplaceAll] );
     //Existiert nicht
     if cAssingLabUsersToGroups[x].MemberOfGroup = cLocUser then
        cAssingLabUsersToGroups[x].MemberOfGroup := '';
  end;

  xList:= TStringList.Create;

  //Relation to User  (Assign User to groups)
  if xIsPDC then begin
     for x:= 0 to High(cAssingUsersToGroups) do begin
       try
          xList.CommaText:= cAssingUsersToGroups[x].MemberOfGroup;
          for n:= 0 to xList.Count-1 do begin
              try
                xNTUserMan.GlobalGroupName:= xList.Strings[n];
                if xNTUserMan.GlobalGroupMembers.IndexOf(cAssingUsersToGroups[x].User) <0  then
                   xNTUserMan.GlobalGroupMembers.Add(cAssingUsersToGroups[x].User);
              except
                xNTUserMan.LocalGroupName:= xList.Strings[n];
                if xNTUserMan.LocalGroupMembers.IndexOf(cAssingUsersToGroups[x].User) <0  then
                   try
                     xNTUserMan.LocalGroupMembers.Add(cAssingUsersToGroups[x].User);
                   except
                   end;
              end;
          end;
       except
          //on E: Exception do ShowMessage(e.Message );
       end; //END except
     end; // END for x:= 0 to High(cAssingUsersToGroups) do begin

     if HasLabMaster then begin
        for x:= 0 to High(cAssingLabUsersToGroups) do begin
            try
              xList.CommaText:= cAssingLabUsersToGroups[x].MemberOfGroup;
              ShowMessage(xList.CommaText);
              for n:= 0 to xList.Count-1 do begin
                  try
                    xNTUserMan.GlobalGroupName:= xList.Strings[n];
                    if xNTUserMan.GlobalGroupMembers.IndexOf(cAssingLabUsersToGroups[x].User) <0  then
                       xNTUserMan.GlobalGroupMembers.Add(cAssingLabUsersToGroups[x].User);
                  except
                    xNTUserMan.LocalGroupName:= xList.Strings[n];
                    if xNTUserMan.LocalGroupMembers.IndexOf(cAssingLabUsersToGroups[x].User) <0  then
                       try
                         xNTUserMan.LocalGroupMembers.Add(cAssingLabUsersToGroups[x].User);
                       except
                       end;
                  end;
              end;
            except
            end; //END except
     end; // END for x:= 0 to High(cAssingUsersToGroups) do begin
     end;

  end else begin
     //Nur local auf Add.Server, Standalone
     for x:= 0 to High(cAssingUsersToGroups) do begin
       try
        if xNTUserMan.LocalGroupMembers.IndexOf(cAssingUsersToGroups[x].User) < 0 then
           xList.CommaText:= cAssingUsersToGroups[x].MemberOfGroup;
           for n:= 0 to xList.Count-1 do
              if xList.Strings[n] <> '' then begin
                 xNTUserMan.LocalGroupName:= xList.Strings[n];
                 try
                  if xNTUserMan.LocalGroupMembers.IndexOf(cAssingUsersToGroups[x].User) <0  then
                     xNTUserMan.LocalGroupMembers.Add(cAssingUsersToGroups[x].User);
                 except
                 end;
           end;
        except
        end;
     end;
     if HasLabMaster then
        for x:= 0 to High(cAssingLabUsersToGroups) do begin
           try
            if xNTUserMan.LocalGroupMembers.IndexOf(cAssingLabUsersToGroups[x].User) < 0 then
               xList.CommaText:= cAssingLabUsersToGroups[x].MemberOfGroup;
               for n:= 0 to xList.Count-1 do
                  if xList.Strings[n] <> '' then begin
                     xNTUserMan.LocalGroupName:= xList.Strings[n];
                     try
                      if xNTUserMan.LocalGroupMembers.IndexOf(cAssingLabUsersToGroups[x].User) <0  then
                         xNTUserMan.LocalGroupMembers.Add(cAssingLabUsersToGroups[x].User);
                     except
                     end;
               end;
            except
            end;
        end;
  end;

  //Benutzerrecht setzen
  xNTPrivilege := TNTPrivilege.Create(nil);
  xNTPrivilege.MachineName := xNTUserMan.MachineName;
  xNTPrivilege.DomainName  := xNTUserMan.DomainName;

  xGroupPrivilegs  :=  TStringList.Create;
  xGroupPrivilegs.Sorted:= TRUE;
  xGroupPrivilegs.Duplicates:=dupIgnore;

  for x:= 0 to High(cUserRightWKS) do begin
      try
        xNTPrivilege.Account:= cUserRightWKS[x].GrantTo;
        xGroupPrivilegs.CommaText:= Trim(cUserRightWKS[x].UserRight);
        xNTPrivilege.Privileges.AddStrings(xGroupPrivilegs);
        xNTPrivilege.Enabled := TRUE;
      except
      end;
  end;

  xList.Free;
  xNTUserMan.Free;
  xNTPrivilege.Free;
  xGroupPrivilegs.free;

  result := TRUE;
end;
//------------------------------------------------------------------------------
}
{
function TForm1.AssignMMPersonalFromServer_(aServer: String): Boolean;
var xNTUserMan    : TNTUserMan;
    xNTPrivilege  : TNTPrivilege;

    xMachineName, xDomain, xLocalMachineName, xPDCMachineName: String;
    xUsersGroup, xAdminGroup, xAssignGroup,
    xNormalUserPlusGroup, xDomainAdminGroup, xDomainUserGroup : String;
    xtext : String;
    xIsPDC : Boolean;
    x, n   : Integer;
    xList           : TStrings;
    xGroupPrivilegs : TStringList;
begin

  xNTUserMan   := TNTUserMan.Create(NIL);
  xNTPrivilege := TNTPrivilege.Create(NIL);

  xMachineName := aServer;
  xMachineName := CheckServerName(xMachineName);
  xNTUserMan.MachineName := xMachineName ;

  xLocalMachineName := xNTUserMan.LocalComputer;
  xLocalMachineName := CheckServerName(xLocalMachineName);

  //on User Host
  xUsersGroup             :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_USERS);
  xAdminGroup             :=  GetLocalGroupNameFromMachine(xMachineName, DOMAIN_ALIAS_RID_ADMINS);


  //'Domain Admins' Name ermitteln
  for x:= 0 to xNTUserMan.GlobalGroups.Count-1 do begin
      xDomainAdminGroup:= xNTUserMan.GlobalGroups.strings[x];
      if Pos('DOM' , UpperCase(xDomainAdminGroup) ) > 0 then
         if Pos('ADM' , UpperCase(xDomainAdminGroup) ) > 0 then
            break;
       xDomainAdminGroup:= '';
  end;

  //'Domain Users' Name ermitteln
  for x:= 0 to xNTUserMan.GlobalGroups.Count-1 do begin
      xDomainUserGroup:= xNTUserMan.GlobalGroups.strings[x];
      if Pos('DOM' , UpperCase(xDomainUserGroup) ) > 0 then
         if Pos(UpperCase(xUsersGroup) , UpperCase(xDomainUserGroup) ) > 0 then
            break;
       xDomainUserGroup:= '';
  end;

  // Local
  xUsersGroup             :=  GetLocalGroupNameFromMachine(xLocalMachineName, DOMAIN_ALIAS_RID_USERS);
  xAdminGroup             :=  GetLocalGroupNameFromMachine(xLocalMachineName, DOMAIN_ALIAS_RID_ADMINS);

  xNormalUserPlusGroup    :=  GetLocalGroupNameFromMachine(xLocalMachineName, DOMAIN_ALIAS_RID_POWER_USERS);
  //PDC
  if xNormalUserPlusGroup = '' then
     xNormalUserPlusGroup :=  GetLocalGroupNameFromMachine(xLocalMachineName, DOMAIN_ALIAS_RID_SYSTEM_OPS);
  xNormalUserPlusGroup:= xNormalUserPlusGroup ;

  xNTUserMan.MachineName := xLocalMachineName ;


  try
    xNTUserMan.LocalGroupName:= xNormalUserPlusGroup;
    xAssignGroup := xDomain + '\' + cMMUserGr;
    if xNTUserMan.LocalGroupMembers.IndexOf(xAssignGroup) < 0 then
       xNTUserMan.LocalGroupMembers.Add(xAssignGroup);
  except
  end;

  try
    xNTUserMan.LocalGroupName:= xAdminGroup;
    xAssignGroup := xDomain + '\' + xDomainAdminGroup;
    if xNTUserMan.LocalGroupMembers.IndexOf(xAssignGroup) < 0 then
       xNTUserMan.LocalGroupMembers.Add(xAssignGroup);
  except
  end;

  try
    xNTUserMan.LocalGroupName:= xUsersGroup;
    xAssignGroup := xDomain + '\' + xDomainUserGroup;
    if xNTUserMan.LocalGroupMembers.IndexOf(xAssignGroup) < 0 then
       xNTUserMan.LocalGroupMembers.Add(xAssignGroup);
  except
  end;



  //Benutzerrecht setzen
  xNTPrivilege := TNTPrivilege.Create(nil);
  xNTPrivilege.MachineName := xNTUserMan.MachineName;

  xGroupPrivilegs  :=  TStringList.Create;
  xGroupPrivilegs.Sorted:= TRUE;
  xGroupPrivilegs.Duplicates:=dupIgnore;

  for x:= 0 to High(cUserRightWKS) do begin
      try
        xNTPrivilege.Account:=  xDomain + '\' + cUserRightWKS[x].GrantTo;
        xGroupPrivilegs.CommaText:= Trim(cUserRightWKS[x].UserRight);
        xNTPrivilege.Privileges.AddStrings(xGroupPrivilegs);
        xNTPrivilege.Enabled := TRUE;
      except
      end;
  end;


  xNTPrivilege.Free;
  xNTUserMan.Free;
  xGroupPrivilegs.free;
end;
}
procedure TForm1.Button1Click(Sender: TObject);
var xtext: String;
begin
  try
    xtext:= '';
    Button1.Enabled:= FALSE;
    Screen.Cursor:=  crHourGlass;
    if Boolean ( InstallMMPersonalToServer(PChar(Edit1.Text)) )  =  TRUE then
       xtext := 'successful'
    else
       xtext := 'failed';

    mStatustext:= Format('MM-Pers. installation to %s on domain %s %s ', [mMachine, mDomain, xtext]);
  except
     on E: Exception do
        mStatustext := Format('MM-Pers. installation to %s failed. Error msg: %s', [Edit1.Text, Trim(e.Message)] );
  end;

  StatusBar1.Font.Style:= [fsBold];
  StatusBar1.SimpleText := mStatustext;
  StatusBar1.Hint := StatusBar1.SimpleText;
  Button1.Enabled:= TRUE;
  Screen.Cursor:=  crDefault;
end;
//------------------------------------------------------------------------------
procedure TForm1.Button2Click(Sender: TObject);
var xtext: String;
begin
  Button2.Enabled:= FALSE;
  Screen.Cursor:=  crHourGlass;
  try
    if Boolean ( AssignMMPersonalFromServer(PChar(Edit2.Text)) )  =  TRUE then
       xtext := 'successful'
    else
       xtext := 'failed';

       mStatustext:= Format('Assign MM-Pers. from %s on domain %s %s ', [mMachine, mDomain, xtext]);
  except
     on E: Exception do
        mStatustext := Format('Assign MM-Pers.from %s failed. Error msg: %s', [Edit2.Text, Trim(e.Message)] );
  end;

  StatusBar1.Font.Style:= [fsBold];
  StatusBar1.SimpleText := mStatustext;
  StatusBar1.Hint := StatusBar1.SimpleText;

  Button2.Enabled:= TRUE;
  Screen.Cursor:=  crDefault;
end;
//------------------------------------------------------------------------------
procedure TForm1.BitBtn1Click(Sender: TObject);
begin
  mMachine := '';
  mDomain  := '';
  with TMachineFinder.Create(Self) do begin
     ShowModal;
     if ModalResult = mrOk  then begin
        mMachine := MachineName;
        mDomain  := DomainName;
        Edit1.text:= MachineName;
     end;
     Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.BitBtn2Click(Sender: TObject);
begin
 mMachine := '';
 mDomain  := '';
 with TMachineFinder.Create(Self) do begin
     ShowModal;
     if ModalResult = mrOk  then begin
        mMachine := MachineName;
        mDomain  := DomainName;
        Edit2.text:= MachineName;
     end;
     Free;
  end;
end;

end.
