object frmLKChartMaverikFrame: TfrmLKChartMaverikFrame
  Left = 0
  Top = 0
  Width = 190
  Height = 360
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentFont = False
  TabOrder = 0
  TabStop = True
  object Label1: TLabel
    Left = 3
    Top = 8
    Width = 109
    Height = 13
    Caption = 'Mittelwertberechnung:'
  end
  object bMark: TSpeedButton
    Left = 11
    Top = 320
    Width = 75
    Height = 25
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'Vorschau'
    OnClick = bMarkClick
  end
  object mAVGControlBar: TToolBar
    Left = 3
    Top = 24
    Width = 174
    Height = 29
    Align = alNone
    Caption = 'mAVGControlBar'
    EdgeBorders = []
    TabOrder = 3
  end
  object bApply: TBitBtn
    Left = 99
    Top = 320
    Width = 75
    Height = 25
    Action = acApply
    Caption = 'Anwenden'
    TabOrder = 4
  end
  object GroupBox1: TGroupBox
    Left = 9
    Top = 120
    Width = 176
    Height = 177
    Caption = ' Berechnungsmethoden '
    TabOrder = 2
    object Bevel1: TBevel
      Left = 8
      Top = 97
      Width = 145
      Height = 2
    end
    object paPercentPanel: TPanel
      Left = 40
      Top = 48
      Width = 109
      Height = 41
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 0
        Top = -1
        Width = 100
        Height = 13
        Caption = 'Relative Abweichung'
      end
      object Label3: TLabel
        Left = 0
        Top = 17
        Width = 11
        Height = 16
        Caption = #177
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label4: TLabel
        Left = 92
        Top = 17
        Width = 12
        Height = 16
        Caption = '%'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object edPercentValue: TLKEdit
        Left = 16
        Top = 16
        Width = 49
        Height = 21
        TabOrder = 0
        Text = '1'
        OnChange = edPercentValueChange
        Alignment = taRightJustify
        Decimals = 3
        MaxValue = 100.000000000000000000
        NumMode = True
        ValidateMode = vmInput
      end
      object spPercent: TSpinButton
        Left = 70
        Top = 17
        Width = 17
        Height = 19
        DownGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000000000000080800000808000008080000080
          8000008080000080800000808000000000000000000000000000008080000080
          8000008080000080800000808000000000000000000000000000000000000000
          0000008080000080800000808000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
        TabOrder = 1
        UpGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000000000000000000000000000000000000000000000000000000000000080
          8000008080000080800000000000000000000000000000000000000000000080
          8000008080000080800000808000008080000000000000000000000000000080
          8000008080000080800000808000008080000080800000808000000000000080
          8000008080000080800000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
        OnDownClick = spPercentDownClick
        OnUpClick = spPercentUpClick
      end
    end
    object rbPercent: TRadioButton
      Left = 8
      Top = 24
      Width = 145
      Height = 17
      Caption = 'Prozentuale Abweichung'
      TabOrder = 1
      OnClick = rbPercentClick
    end
    object rbAbsolute: TRadioButton
      Left = 8
      Top = 108
      Width = 153
      Height = 17
      Caption = 'Absolute Abweichung'
      TabOrder = 2
      OnClick = rbAbsoluteClick
    end
    object paAbsolutePanel: TPanel
      Left = 40
      Top = 132
      Width = 105
      Height = 41
      BevelOuter = bvNone
      TabOrder = 3
      object Label6: TLabel
        Left = 0
        Top = -1
        Width = 100
        Height = 13
        Caption = 'Relative Abweichung'
      end
      object Label7: TLabel
        Left = 0
        Top = 17
        Width = 11
        Height = 16
        Caption = #177
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object edAbsoluteValue: TLKEdit
        Left = 16
        Top = 16
        Width = 49
        Height = 21
        TabOrder = 0
        Text = '1'
        OnChange = edAbsoluteValueChange
        Alignment = taRightJustify
        Decimals = 3
        NumMode = True
        ValidateMode = vmInput
      end
      object spAbsolute: TSpinButton
        Left = 62
        Top = 17
        Width = 17
        Height = 19
        DownGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000008080000080800000808000000000000080800000808000008080000080
          8000008080000080800000808000000000000000000000000000008080000080
          8000008080000080800000808000000000000000000000000000000000000000
          0000008080000080800000808000000000000000000000000000000000000000
          0000000000000000000000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
        TabOrder = 1
        UpGlyph.Data = {
          0E010000424D0E01000000000000360000002800000009000000060000000100
          200000000000D800000000000000000000000000000000000000008080000080
          8000008080000080800000808000008080000080800000808000008080000080
          8000000000000000000000000000000000000000000000000000000000000080
          8000008080000080800000000000000000000000000000000000000000000080
          8000008080000080800000808000008080000000000000000000000000000080
          8000008080000080800000808000008080000080800000808000000000000080
          8000008080000080800000808000008080000080800000808000008080000080
          800000808000008080000080800000808000}
        OnDownClick = spAbsoluteDownClick
        OnUpClick = spAbsoluteUpClick
      end
    end
  end
  object edNrValues: TEdit
    Left = 8
    Top = 88
    Width = 49
    Height = 19
    BorderStyle = bsNone
    Color = clBtnFace
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 0
  end
  object edNrValuesPreview: TEdit
    Left = 88
    Top = 88
    Width = 49
    Height = 19
    BorderStyle = bsNone
    Color = clBtnFace
    Ctl3D = False
    ParentCtl3D = False
    ReadOnly = True
    TabOrder = 1
  end
  object mActionList: TActionList
    OnUpdate = mActionListUpdate
    Left = 147
    Top = 16
    object acApply: TAction
      Caption = 'Anwenden'
      OnExecute = bApplyClick
    end
    object acMark: TAction
      Caption = 'Vorschau'
      OnExecute = bMarkClick
    end
  end
end
