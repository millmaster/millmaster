{===========================================================================================
| Copyright:        Innolab GmbH
|-------------------------------------------------------------------------------------------
| Dateiname:        lkChartFrame
| Projekt:          Pr�fstand
| Beschreibung:     Chart f�r den Innolab Pumpenpr�fstand
| Info:             -
| Org. Entw.System: Windows XP SP2
| Compiler/Tools:   Delphi
|-------------------------------------------------------------------------------------------
| History:
| Datum       Vers. Vis.| Grund
|-------------------------------------------------------------------------------------------
| 06.03.2005  1.00  Lok | Datei erzeugt
|==========================================================================================}
unit lkChartFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, Menus, ActnList, ImgList, ToolWin, ExtCtrls, TeeProcs,
  TeEngine, Chart, ChartSeries, LKChartSeries, LKChartDetailInfoForm, StdCtrls,
  ExtDlgs, lkExpandingPanel, LKChartMaverikFrame,
  Spin, LkChartStatisticsFrame, uNumEdit;

type
  TLKChartAction = (caPrint,              // Diagramm drucken
                    caPrintPreview,       // Druckvorschau
                    caEditHeader,         // Header editieren
                    caEditFooter,         // Footer editiern
                    caImport,             // Importieren von Datenfiles
                    caSeriesEnable,       // einzelne Serien verbergen
                    caDeleteAllSeries,    // Alle Serien l�schen
                    caMaverik,            // Ausreisserbehandlung erm�glichen
                    caHideSerie,          // Serien verbergen
                    caShowAllSeries,      // Alle Serien anzeigen
                    caExport,             // Reihe in csv Fiel exportieren
                    caEnableEditValueInfo // Eingabe von Zusatzinfos zu den einzelnen Werten
                    );// TLKChartAction
  TLKChartActionSet = Set of TLKChartAction;

const
  cDefaultLKChartActionSet = [caPrint,
                              caPrintPreview,
                              caEditHeader,
                              caEditFooter,
                              caImport,
                              caDeleteAllSeries,
                              caHideSerie,
                              caShowAllSeries,
                              caExport];

type
  TlkChartFrameList = class;
  TLKChartEditInfo = procedure(Sender: TObject; aSerie: TLKCHartLineSeries;
      aValueIndex: integer; var aAdditionalText: string) of object;

  TfrmlkChartFrame = class(TFrame)
    mButtonImages: TImageList;
    mButtonImages_d: TImageList;
    mButtonImages_h: TImageList;
    ActionList1: TActionList;
    acPrint: TAction;
    acPrintPreview: TAction;
    acDeleteSeries: TAction;
    acImportFile: TAction;
    acShowHideSeries: TAction;
    acEditHeader: TAction;
    acEditFooter: TAction;
    pmSerie: TPopupMenu;
    Serielschen1: TMenuItem;
    pmChart: TPopupMenu;
    Footereditieren1: TMenuItem;
    Footereditieren2: TMenuItem;
    mChart: TChart;
    ToolBar2: TToolBar;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton13: TToolButton;
    ToolButton12: TToolButton;
    ToolButton2: TToolButton;
    ToolButton10: TToolButton;
    ToolButton8: TToolButton;
    Panel1: TPanel;
    FCalcMethodTab: TTabControl;
    acHideSerie: TAction;
    N1: TMenuItem;
    acShowAllSeries: TAction;
    AlleSerienanzeigen1: TMenuItem;
    N2: TMenuItem;
    AlleSerienanzeigen2: TMenuItem;
    acExportSerie: TAction;
    Serieexportieren1: TMenuItem;
    mExportSeriesDialog: TSaveDialog;
    FChartSettingsPanel: TlkExpandingPanel;
    mSettingsContainer: TPageControl;
    tabChart: TTabSheet;
    edChartYMin: TLKEdit;
    Label1: TLabel;
    cbChartAutoAxis: TCheckBox;
    edChartYMax: TLKEdit;
    Label2: TLabel;
    tabCalc: TTabSheet;
    TabSheet1: TTabSheet;
    tabMaverick: TTabSheet;
    mMaverik: TfrmLKChartMaverikFrame;
    MainMenu1: TMainMenu;
    mStatisticsFrame: TfrmLKChartStatisticsFrame;
    Panel2: TPanel;
    edIntervalEnd: TSpinEdit;
    edIntervalStart: TSpinEdit;
    Label4: TLabel;
    Label3: TLabel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    acRefreshStatistics: TAction;
    cbAutorefresh: TCheckBox;
    ToolButton3: TToolButton;
    procedure acRefreshStatisticsExecute(Sender: TObject);
    procedure mChartBeforeDrawSeries(Sender: TObject);
    procedure edIntervalChange(Sender: TObject);
    procedure mSettingsContainerChange(Sender: TObject);
    procedure edChartYMaxValidated(Sender: TLKEdit; aEnteredValue: Extended; var aValidatedValue: Extended);
    procedure edChartYMinValidated(Sender: TLKEdit; aEnteredValue: Extended; var aValidatedValue: Extended);
    procedure cbChartAutoAxisClick(Sender: TObject);
    procedure acExportSerieExecute(Sender: TObject);
    procedure acShowAllSeriesExecute(Sender: TObject);
    procedure acHideSerieExecute(Sender: TObject);
    procedure acEditFooterExecute(Sender: TObject);
    procedure mChartClick(Sender: TObject);
    procedure acEditHeaderExecute(Sender: TObject);
    procedure acPrintPreviewExecute(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure acDeleteSeriesExecute(Sender: TObject);
    procedure acImportFileExecute(Sender: TObject);
    procedure FCalcMethodTabChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FCalcMethodTabChange(Sender: TObject);
    procedure mChartClickLegend(Sender: TCustomChart; Button: TMouseButton; Shift:
        TShiftState; X, Y: Integer);
    procedure mChartClickSeries(Sender: TCustomChart; Series: TChartSeries;
        ValueIndex: Integer; Button: TMouseButton; Shift: TShiftState; X, Y:
        Integer);
  private
    FChartActions: TLKChartActionSet;
    FShowRawSeriesShadowed: Boolean;
    FXAxisDateTime: Boolean;
    FDataContainer: TLKChartSeriesContainer;
    FLinkedCharts: TLkChartFrameList;
    FOnAfterEditInfo: TLKChartEditInfo;
    FOnBeforeEditInfo: TLKChartEditInfo;
    memoEditTitle: TMemo;
    mIsUpdatingSerieInterval: Boolean;
    mLastClickedSerie: TChartSeries;
    mOriginalWndProc: Pointer;
    procedure EditHeaderFooter(aDim: TRect; aTitle: TChartTitle);
    function GetChart: TChart;
    function GetChartFooter: string;
    function GetChartTitle: string;
    procedure SelectLineSerie(Sender: TCustomChart; aSeries: TChartSeries);
    procedure SetChartActions(const Value: TLKChartActionSet);
    procedure SetChartFooter(const Value: string);
    procedure SetChartTitle(const Value: string);
    procedure SetXAxisDateTime(const Value: Boolean);
    procedure SetCurrentSeries(aSerie: TChartSeries);
  protected
    procedure AfterDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);
    procedure ChartWndProc(var Message: TMessage);
    procedure DoAfterEditInfo(aSerie: TLKChartLineSeries; aValueIndex: integer; var
        aAdditionalText: string);
    procedure DoBeforeEditInfo(aSerie: TLKChartLineSeries; aValueIndex: integer;
        var aAdditionalText: string);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function AddPoint(aIndex: integer; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string): integer; overload;
    function AddPoint(aSerie: string; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string): integer; overload;
    function AddPoint(aSerie: string; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string; var aSerieIndex: integer): integer; overload;
    procedure AttachTitle(Sender: TObject);
    function CurrentSeriesType: TSeriesType;
    procedure ExportSeries(aSeries: TChartSeries; aFileName: string; aSeparator:
        string = ';');
    procedure SetBottomChartTitle(aTitle: string);
    procedure SetLeftChartTitle(aTitle: string);
    property CalcMethodTab: TTabControl read FCalcMethodTab;
    property Chart: TChart read GetChart;
    property ChartActions: TLKChartActionSet read FChartActions write
        SetChartActions;
    property ChartFooter: string read GetChartFooter write SetChartFooter;
    property ChartSettingsPanel: TlkExpandingPanel read FChartSettingsPanel;
    property ChartTitle: string read GetChartTitle write SetChartTitle;
    property DataContainer: TLKChartSeriesContainer read FDataContainer;
    property LinkedCharts: TLkChartFrameList read FLinkedCharts;
    property ShowRawSeriesShadowed: Boolean read FShowRawSeriesShadowed write
        FShowRawSeriesShadowed;
    property XAxisDateTime: Boolean read FXAxisDateTime write SetXAxisDateTime;
  published
    property OnAfterEditInfo: TLKChartEditInfo read FOnAfterEditInfo write
        FOnAfterEditInfo;
    property OnBeforeEditInfo: TLKChartEditInfo read FOnBeforeEditInfo write
        FOnBeforeEditInfo;
  end;

  (* -----------------------------------------------------------
    Liste um verlinkte Charts angeben zu k�nnen.
    Einige Funktionen werden dann mit den verlinkten Charts ebenfalls ausgef�hrt
  -------------------------------------------------------------- *)
  TlkChartFrameList = class(TPersistent)
  protected
    mFrames: TList;
    function GetCount: Integer;
    function GetItems(aIndex: Integer): TfrmlkChartFrame;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function Add(aFrame: TfrmlkChartFrame): Integer; virtual;
    procedure Assign(Source: TPersistent); override;
    procedure Clear;
    procedure Delete(aIndex: integer); virtual;
    property Count: Integer read GetCount;
    property Items[aIndex: Integer]: TfrmlkChartFrame read GetItems; default;
  end;

implementation

{$R *.dfm}

uses
  series, LKGlobal, LKChartImportForm, teeprevi, CommCtrl,
  ShellApi, LKDialogs, mmCS;

(* -----------------------------------------------------------
  In der Serienliste werden alle Rohdaten verwaltet
-------------------------------------------------------------- *)
constructor TfrmlkChartFrame.Create(aOwner: TComponent);
var
  i: TSeriesType;
begin
  inherited;
  Name := '';

  // Diese Liste nimmt die Rohdaten auf
  FDataContainer := TLKChartSeriesContainer.Create(mChart);

  // Untere Achse des Chart definieren
  mChart.BottomAxis.DateTimeFormat := Format('%s', [ShortDateFormat]);
  mChart.BottomAxis.Increment := DateTimeStep[dtSixHours];
  mChart.BottomAxis.LabelsMultiLine := True;

  mChart.PrintMargins := Rect(2,2, 2, 2);

  FCalcMethodTab.Tabs.Clear;
  for i := Low(TSeriesType) to High(TSeriesType) do begin
    if i in cUsedCalcMethods then
      FCalcMethodTab.Tabs.AddObject(cCalcMethods[i].MethodCaption, Pointer(i));
  end;// for i := Low(TSeriesType) to High(TSeriesType) do begin

  ChartActions := cDefaultLKChartActionSet;
  FLinkedCharts := TlkChartFrameList.Create;
//  if aOwner is TForm then begin
//    mOriginalWndProc := Pointer(SetWindowLong(TForm(aOwner).Handle, GWL_WNDPROC, Longint(MakeObjectInstance(ChartWndProc))));
//    DragAcceptFiles(TCustomForm(aOwner).Handle, True);
//  end;// if aOwner is TForm then begin

  FChartSettingsPanel.Expanded := false;
  // Setzt die Ausreisser als Default Tab.
  mSettingsContainer.ActivePage := tabMaverick;
  // L�st das neuberechnen der Breite des Containers aus
  mSettingsContainerChange(mSettingsContainer);
end;// constructor TfrmlkChartFrame.Create(aOwner: TComponent);

(* -----------------------------------------------------------
  Die Liste mit den Serien wieder freigeben
-------------------------------------------------------------- *)
destructor TfrmlkChartFrame.Destroy;
begin
//  if Owner is TForm then
//    DragAcceptFiles(TForm(Owner).Handle, false);
//  if assigned(mOriginalWndProc) then
//    SetWindowLong(TForm(Owner).Handle, GWL_WNDPROC, Longint(mOriginalWndProc));

  FreeAndNil(FlinkedCharts);
  FreeAndNil(FDataContainer);
  inherited;
end;// destructor TfrmlkChartFrame.Destroy;

(* -----------------------------------------------------------
  F�gt der Root Serie einen neuen Punkt mit Zusatzinfos hinzu
-------------------------------------------------------------- *)
function TfrmlkChartFrame.AddPoint(aIndex: integer; aXValue: TDateTime;
    aYValue: double; aDetailInfo: string): integer;
begin
  result := FDataContainer.AddPoint(aIndex, aXValue, aYValue, aDetailInfo);
end;// function TfrmlkChartFrame.AddPoint(aIndex: integer; aXValue: TDateTime; ...

(* -----------------------------------------------------------
  Wie oben. Die Serie kann allerdings �ber ihren Namen angesprochen werden
-------------------------------------------------------------- *)
function TfrmlkChartFrame.AddPoint(aSerie: string; aXValue: TDateTime; aYValue:
    double; aDetailInfo: string): integer;
begin
  result := FDataContainer.AddPoint(aSerie, aXValue, aYValue, aDetailInfo);
end;// function TfrmlkChartFrame.AddPoint(aSerie: string; aXValue: TDateTime; aYValue:

(* -----------------------------------------------------------
  Schaltet f�r eine Serie die Pointer ein und f�r alle anderen aus
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SelectLineSerie(Sender: TCustomChart; aSeries:
    TChartSeries);
var
  XSerie: TLKChartLineSeries;
  i: Integer;
begin
  SetCurrentSeries(nil);
  if assigned(aSeries) then begin
    for i := 0 to Sender.SeriesCount - 1 do begin
      if Sender.Series[i] is TLKChartLineSeries then begin
        XSerie := TLKChartLineSeries(Sender.Series[i]);
        // Alle Serien ausser der selektierten deselektieren
        XSerie.Pointer.Visible := (aSeries = Sender.Series[i]) and (not(XSerie.Pointer.Visible));
       if XSerie.Pointer.Visible then
          SetCurrentSeries(xSerie);
      end;// if Sender.Series[i] is TLKChartLineSeries then begin
    end;// for i := 0 to Sender.SeriesCount - 1 do begin
  end;// if assigned(aSeries) then begin
end;// function TfrmlkChartFrame.AddPoint(aSerie: string; aXValue: TDateTime; aYValue: ...

(* -----------------------------------------------------------
  Schaltet die Punkte der angeklickten Serie ein
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.mChartClickLegend(Sender: TCustomChart; Button:
    TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  // Ermittelt die angeklickte Serie
  mLastClickedSerie := Sender.ActiveSeriesLegend(Sender.Legend.Clicked(X, Y));
  if Button = mbLeft then begin

    // Schaltet die Pointer der selektierten Serie ein und die der anderen aus
    SelectLineSerie(Sender, mLastClickedSerie);// if (xIndex >= 0) and (xIndex < Sender.SeriesCount) then begin
  end else begin
    with mChart.ClientToScreen(Point(X, Y)) do
      pmSerie.Popup(X,Y);
  end;// if Button = mbLeft then begin
  if (mLastClickedSerie is TLKChartLineSeries) and (not(TLKChartLineSeries(mLastClickedSerie).Pointer.Visible)) then
    mLastClickedSerie := nil;
end;// procedure TfrmlkChartFrame.mChartClickLegend(Sender: TCustomChart; Button:

(* -----------------------------------------------------------
  Zeigt das Infofenster zu einem Punkt an
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.mChartClickSeries(Sender: TCustomChart; Series:
    TChartSeries; ValueIndex: Integer; Button: TMouseButton; Shift:
    TShiftState; X, Y: Integer);
var
  xText: string;
begin
  mLastClickedSerie := Series;
  if Series is TLKChartLineSeries then begin
    if Button = mbLeft then begin
      // Wenn die Pointer bereits sichtbar sind, dann die Detailinfos anzeigen
      if TLKChartLineSeries(Series).Pointer.Visible then begin
        // Wenn notwendig, das Info Formular erzeugen
        with TfrmDetailInfo.Create(self) do begin
          with mChart.ClientToScreen(Point(X, Y)) do
            ShowInfo(Series, X, Y, ValueIndex);
        end;// with TfrmDetailInfo.Create(self) do begin
      end else begin
        // Wenn die Pointer nicht sichtbar sind, dann die Pointer anzeigen
        SelectLineSerie(Sender, Series);
      end;// if TLKChartLineSeries(Series).Pointer.Visible then begin
    end else begin
      // Zusatzinfos
      if caEnableEditValueInfo in fChartActions then begin
        if TLKChartLineSeries(Series).Pointer.Visible then begin
          DoBeforeEditInfo(TLKChartLineSeries(Series), ValueIndex, xText);
          xText := LKInputBox('Zusatzinfo', 'Eingabe der Notizen zum aktuellen Wert', xText);
          DoAfterEditInfo(TLKChartLineSeries(Series), ValueIndex, xText);
        end;// if TLKChartLineSeries(Series).Pointer.Visible then begin
      end else begin
        with mChart.ClientToScreen(Point(X, Y)) do
          pmSerie.Popup(X,Y);
      end;// if caEnableEditValueInfo in ChartActions then begin
    end;// if Button = mbLeft then begin
  end;// if Series is TLKChartLineSeries then begin
end;// procedure TfrmlkChartFrame.mChartClickSeries(Sender: TCustomChart; Series:

(* -----------------------------------------------------------
  Liefert den Typ der aktuell darzustellenden Serien
-------------------------------------------------------------- *)
function TfrmlkChartFrame.CurrentSeriesType: TSeriesType;
begin
  Result := TSeriesType(FCalcMethodTab.Tabs.Objects[FCalcMethodTab.TabIndex]);
end;// function TfrmlkChartFrame.CurrentSeriesType: TSeriesType;

(* -----------------------------------------------------------
  Wird nach dem Wechsel aufgerufen
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.FCalcMethodTabChange(Sender: TObject);
var
  i: Integer;
  xNotify: TNMHdr;
begin
  FDataContainer.Series[CurrentSeriesType].Active := true;

  FDataContainer.CreateControls(CurrentSeriesType, tabCalc);
  FDataContainer.Calculate(CurrentSeriesType);
  if (fShowRawSeriesShadowed) and (CurrentSeriesType <> stRaw) then begin
    FDataContainer.Series[stRaw].Shadowed := true;
    FDataContainer.Series[stRaw].Active   := true;
  end else begin
    FDataContainer.Series[stRaw].Shadowed := false;
  end;//  if (fShowRawSeriesShadowed) and (CurrentSeriesType <> stRaw) then begin

  for i := 0 to LinkedCharts.Count - 1 do begin
    xNotify.hwndFrom := self.Handle;
    xNotify.code := TCN_SELCHANGING;
    LinkedCharts.Items[i].CalcMethodTab.Perform(CN_NOTIFY, 1, integer(@xNotify));

    LinkedCharts.Items[i].CalcMethodTab.TabIndex := CalcMethodTab.TabIndex;

    xNotify.hwndFrom := self.Handle;
    xNotify.code := TCN_SELCHANGE;
    LinkedCharts.Items[i].CalcMethodTab.Perform(CN_NOTIFY, 1, integer(@xNotify));
  end;
end;// procedure TfrmlkChartFrame.mCalcMethodTabChange(Sender: TObject);

(* -----------------------------------------------------------
  Wird vor dem Wechsel aufgerufen
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.FCalcMethodTabChanging(Sender: TObject; var AllowChange: Boolean);
begin
  SetCurrentSeries(nil);
  FDataContainer.Series[CurrentSeriesType].Active := false;
  FDataContainer.DestroyControls(CurrentSeriesType);
end;// procedure TfrmlkChartFrame.mCalcMethodTabChanging(Sender: TObject; var AllowChange: Boolean);

(* -----------------------------------------------------------
  Importiert eine- oder mehrere Datenreihen
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acImportFileExecute(Sender: TObject);
begin
  with TfrmLKChartImportForm.Create(nil) do try
    Chart := mChart;
    ChartFrame := self;
    ImportFile;
  finally
    free;
  end;// with TfrmLKChartImportForm.Create(nil) do try
end;// procedure TfrmlkChartFrame.acImportFileExecute(Sender: TObject);

(* -----------------------------------------------------------
  L�scht alle Serien
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acDeleteSeriesExecute(Sender: TObject);
begin
  SetCurrentSeries(nil);
  mMaverik.RawSerie := nil;
  FDataContainer.Series[stRaw].Clear;
end;// procedure TfrmlkChartFrame.acDeleteSeriesExecute(Sender: TObject);

(* -----------------------------------------------------------
  Druckt das aktuelle Diagramm
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acPrintExecute(Sender: TObject);
begin
  mChart.PrintMargins := Rect(0,0,0,0);
  mChart.PrintProportional:=false;
  mChart.PrintLandscape;
end;// procedure TfrmlkChartFrame.acPrintExecute(Sender: TObject);

(* -----------------------------------------------------------
  Druckvorschau
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acPrintPreviewExecute(Sender: TObject);
begin
  mChart.PrintMargins := Rect(0,0,0,0);
  mChart.PrintProportional:=false;
  ChartPreview(nil, mChart);
end;// procedure TfrmlkChartFrame.acPrintPreviewExecute(Sender: TObject);

(* -----------------------------------------------------------
  Aktualisiert den Header
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acEditHeaderExecute(Sender: TObject);
var
  xRect: TRect;
begin
  xRect := mChart.Title.TitleRect;
  if (xRect.Top = xRect.Bottom) then begin
    xRect.Left := mChart.MarginLeft;
    xRect.Top  := mChart.MarginTop + 5;
    xRect.Right := mChart.Width - mChart.MarginRight;
    xRect.Bottom := mChart.Top + 21;
  end;// if (xRect.Left = 0) and (xRect.Top = 0) then begin

  if assigned(memoEditTitle) then
    AttachTitle(Sender)
  else
    EditHeaderFooter(xRect, mChart.Title);
  mChart.Title.Visible := true;
end;// procedure TfrmlkChartFrame.acEditHeaderExecute(Sender: TObject);

(* -----------------------------------------------------------
  F�gt Header oder Footer ein
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.AttachTitle(Sender: TObject);
begin
  if assigned(memoEditTitle) then begin
    try
      TChartTitle(memoEditTitle.Tag).Text.Text := memoEditTitle.Text;
    except
    end;
    FreeAndNil(memoEditTitle);
  end;// if assigned(memoEditTitle) then begin
end;// procedure TfrmlkChartFrame.AttachTitle(Sender: TObject);

(* -----------------------------------------------------------
  Erm�glicht das manuelle �ndern des Headers oder des Footers
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.EditHeaderFooter(aDim: TRect; aTitle: TChartTitle);
begin
  if not(assigned(memoEditTitle)) then begin
    aDim.Bottom := aDim.Bottom + 20;
    memoEditTitle := TMemo.Create(self);
    with memoEditTitle do begin
      BoundsRect := aDim;
      Parent := mChart;
      visible := true;
      Text := aTitle.Text.Text;
      Tag := integer(aTitle);
      OnExit := AttachTitle;
      SetFocus;
    end;// with memoEditTitle do begin
  end;// if not(assigned(memoEditTitle)) then begin
end;// procedure EditHeaderFooter(aDim: TRect; aText: string);

(* -----------------------------------------------------------
  Zugriffsmethode f�r den Footer
-------------------------------------------------------------- *)
function TfrmlkChartFrame.GetChartFooter: string;
begin
  result := mChart.Foot.Text.Text;
end;// function TfrmlkChartFrame.GetChartFooter: string;

(* -----------------------------------------------------------
  Zugriffsmethode f�r den Titel
-------------------------------------------------------------- *)
function TfrmlkChartFrame.GetChartTitle: string;
begin
  result := mChart.Title.Text.Text;
end;// function TfrmResultChart.GetChartTitle: string;

(* -----------------------------------------------------------
  Setzt den Namen der unteren Achse
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetBottomChartTitle(aTitle: string);
begin
  mChart.BottomAxis.Title.Caption := aTitle;
end;// procedure TfrmlkChartFrame.SetBottomChartTitle(aTitle: string);

(* -----------------------------------------------------------
  Zugriffsmethode f�r den Footer
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetChartFooter(const Value: string);
begin
  mChart.Foot.Visible := (Value > '');
  mChart.Foot.Text.Text := Value;
end;// procedure TfrmlkChartFrame.SetChartFooter(const Value: string);

(* -----------------------------------------------------------
  Zugriffsmethode f�r den Titel
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetChartTitle(const Value: string);
begin
  mChart.Title.Visible := (Value > '');
  mChart.Title.Text.Text := Value;
end;// procedure TfrmResultChart.SetChartTitle(const Value: string);

(* -----------------------------------------------------------
  Setzt die Beschriftung f�r die linke Achse
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetLeftChartTitle(aTitle: string);
begin
  mChart.LeftAxis.Title.Caption := aTitle;
end;// procedure TfrmlkChartFrame.SetLeftChartTitle(aTitle: string);

(* -----------------------------------------------------------
  Weist den verschiedenen Frames die aktuelle Serie zu
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetCurrentSeries(aSerie: TChartSeries);
var
  xSerie: TLKChartLineSeries;
  xSerieCount: Integer;
begin
  if aSerie is TLKChartLineSeries then begin
    xSerie := TLKChartLineSeries(aSerie);
    // Wenn eine Serie angew�hlt wurde, dann die Behandlungsroutine setzen
    xSerie.OnAfterDeletePoint := AfterDeletePoint;
    xSerieCount := aSerie.Count - 1;
    if xSerieCount < 0 then
      xSerieCount := 0;

    edIntervalStart.MaxValue := xSerieCount;
    edIntervalEnd.MaxValue := xSerieCount;

    // Die Eigenschaften der Serien d�rfen nicht aktualisiert werden, solange nicht bede korrekt gesetzt sind
    mIsUpdatingSerieInterval := true;
    try
      edIntervalStart.Value := xSerie.IntervalStart;
      edIntervalEnd.Value := xSerie.IntervalEnd;
    finally
      mIsUpdatingSerieInterval := false;
    end;// try finally
  end;// if aSerie is TLKChartLineSeries then begin

  mMaverik.RawSerie := aSerie;
  mStatisticsFrame.RawSerie := aSerie;
end;// procedure TfrmlkChartFrame.SetCurrentSeries(aSerie: TChartSeries);

(* -----------------------------------------------------------
  Erm�glicht die Eingabed eines Headers oder eines Footers
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.mChartClick(Sender: TObject);
var
  xMousePos: TPoint;
begin
  if (mChart.Foot.Visible) or (mChart.Title.Visible) then begin
    GetCursorPos(xMousePos);
    xMousePos := mChart.ScreenToClient(xMousePos);

    if assigned(memoEditTitle) then begin
      AttachTitle(Sender);
    end else begin
      if mChart.Foot.Clicked(xMousePos.x, xMousePos.y) then
        EditHeaderFooter(mChart.Foot.TitleRect, mChart.Foot);
      if mChart.Title.Clicked(xMousePos.x, xMousePos.y) then
        EditHeaderFooter(mChart.Title.TitleRect, mChart.Title);
    end;// if assigned(memoEditTitle) then begin
  end;// if mChart.Foot.Visible then begin
end;// procedure TfrmlkChartFrame.mChartClick(Sender: TObject);

(* -----------------------------------------------------------
  manuelle Eingabe eines Footers
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acEditFooterExecute(Sender: TObject);
var
  xRect: TRect;
begin
  xRect := mChart.Foot.TitleRect;
  if (xRect.Top = xRect.Bottom) then begin
    xRect.Left := mChart.MarginLeft;
    xRect.Bottom := mChart.ClientHeight - 35;
    xRect.Top  := xRect.Bottom - 21;
    xRect.Right := mChart.Width - mChart.MarginRight;
  end;// if (xRect.Left = 0) and (xRect.Top = 0) then begin

  if assigned(memoEditTitle) then
    AttachTitle(Sender)
  else
    EditHeaderFooter(xRect, mChart.Foot);
  mChart.Foot.Visible := true;
end;// procedure TfrmlkChartFrame.acEditFooterExecute(Sender: TObject);

(* -----------------------------------------------------------
  Verbirgt die aktuelle Serie
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acHideSerieExecute(Sender: TObject);
begin
  if mLastClickedSerie is TLKChartLineSeries then
    TLKChartLineSeries(mLastClickedSerie).Visible := false;
  mLastClickedSerie := nil;
end;// procedure TfrmlkChartFrame.acHideSerieExecute(Sender: TObject);

(* -----------------------------------------------------------
  Zeigt wieder alle Serien an
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acShowAllSeriesExecute(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to FDataContainer.Series[CurrentSeriesType].SerieCount - 1 do
    FDataContainer.Series[CurrentSeriesType].Line[i].Visible := true;
  mChart.Refresh;
end;// procedure TfrmlkChartFrame.acShowAllSeriesExecute(Sender: TObject);

(* -----------------------------------------------------------
  Wie oben. Die Serie kann allerdings �ber ihren Namen angesprochen werden
-------------------------------------------------------------- *)
function TfrmlkChartFrame.AddPoint(aSerie: string; aXValue: TDateTime; aYValue:
    double; aDetailInfo: string; var aSerieIndex: integer): integer;
begin
  result := FDataContainer.AddPoint(aSerie, aXValue, aYValue, aDetailInfo, aSerieIndex);
end;// function TLKChartSeriesContainer.AddPoint(aSerie: string; aXValue: TDateTime;

(* -----------------------------------------------------------
  Ver�ffentlicht die Chart Komponente
-------------------------------------------------------------- *)
function TfrmlkChartFrame.GetChart: TChart;
begin
  Result := mChart;
end;// function TfrmlkChartFrame.GetChart: TChart;

(* -----------------------------------------------------------
  Schaltet die einzelnen Actions ein oder aus
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetChartActions(const Value: TLKChartActionSet);
begin
  FChartActions := Value;
  acPrint.Enabled          := caPrint in Value;
  acPrintPreview.Enabled   := caPrintPreview in Value;
  acEditHeader.Enabled     := caEditHeader in Value;
  acEditFooter.Enabled     := caEditFooter in Value;
  acImportFile.Enabled     := caImport in Value;
  acShowHideSeries.Enabled := caSeriesEnable in Value;
  acDeleteSeries.Enabled   := caDeleteAllSeries in Value;
  acHideSerie.Enabled      := caHideSerie in Value;
  acShowAllSeries.Enabled  := caShowAllSeries in Value;
  acExportSerie.Enabled    := caExport in Value;
end;// procedure TfrmlkChartFrame.SetChartActions(const Value: TLKChartActionSet);

(* -----------------------------------------------------------
  Stellt die Datums-Achse ein oder aus
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.SetXAxisDateTime(const Value: Boolean);
begin
  FDataContainer.XAxisDateTime := Value;
end;// procedure TfrmlkChartFrame.SetXAxisDateTime(const Value: Boolean);

(* -----------------------------------------------------------
  Exportiert die aktuelle Serie in eine Textdatei
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acExportSerieExecute(Sender: TObject);
begin
  if assigned(mLastClickedSerie) then begin
    mExportSeriesDialog.FileName := mLastClickedSerie.Title;
    if mExportSeriesDialog.Execute then
      ExportSeries(mLastClickedSerie, mExportSeriesDialog.FileName);
  end;// if assigned(mLastClickedSerie) then begin
end;// procedure TfrmlkChartFrame.acExportSerieExecute(Sender: TObject);

(* -----------------------------------------------------------
  Exportiert die angegebene Serie in die angegeben Textdatei
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.ExportSeries(aSeries: TChartSeries; aFileName:
    string; aSeparator: string = ';');
var
  i: Integer;
  xSeriesDetailInfo: TStringList;
  xFile: TextFile;

  procedure WriteFirstLine;
  var
    i: Integer;
  begin
    if aSeries.XValues.DateTime then
      write(xFile, 'Datum', aSeparator, 'Zeit')
    else
      write(xFile, 'Dauer');

    write(xFile, aSeparator, 'Wert');

    if aSeries is TLKChartLineSeries then begin
      xSeriesDetailInfo := TLKLineSeries(aSeries).Items[0];
      if assigned(xSeriesDetailInfo) then begin
        for i := 0 to xSeriesDetailInfo.Count - 1 do begin
          if xSeriesDetailInfo.Names[i] > '' then
            write(xFile, aSeparator, xSeriesDetailInfo.Names[i]);
        end;// for i := 0 to xSeriesDetailInfo.Count - 1 do begin
      end;// if assigned(xSeriesDetailInfo) then begin
    end;// if aSeries is TLKChartLineSeries then begin
    write(xFile, cCrlf);
  end;// procedure WriteFirstLine;

  procedure WriteValueLine(aIndex: integer);
  var
    i: Integer;
  begin
    if aSeries.XValues.DateTime then
      write(xFile, DateToStr(aSeries.XValue[aIndex]), aSeparator, TimeToStr(aSeries.XValue[aIndex]))
    else
      write(xFile, FloatToStr(aSeries.XValue[aIndex]));

    write(xFile, aSeparator, FloatToStr(aSeries.YValue[aIndex]));

    if aSeries is TLKChartLineSeries then begin
      xSeriesDetailInfo := TLKLineSeries(aSeries).Items[aIndex];
      if assigned(xSeriesDetailInfo) then begin
        for i := 0 to xSeriesDetailInfo.Count - 1 do begin
          if xSeriesDetailInfo.Names[i] > '' then begin
            with xSeriesDetailInfo do
              write(xFile, aSeparator, Values[Names[i]]);
          end;
        end;// for i := 0 to xSeriesDetailInfo.Count - 1 do begin
      end;// if assigned(xSeriesDetailInfo) then begin
    end;// if aSeries is TLKChartLineSeries then begin
    write(xFile, cCrlf);
  end;// procedure WriteValueLine(aIndex: integer);

begin
  if aSeries.Count > 0 then begin
    Assignfile(xFile, aFileName);
    Rewrite(xFile);
    try
      WriteFirstLine;
      for i := 0 to aSeries.Count - 1 do
        WriteValueLine(i);
    finally
      Closefile(xFile);
    end;// try finally
  end;// if aSeries.Count > 0 then begin
end;// procedure TfrmlkChartFrame.ExportSeries(aSeries: TLKChartLineSeriesList; aFileName: string);

procedure TfrmlkChartFrame.ChartWndProc(var Message: TMessage);
var
  i:              integer;
  xNumberDropped: integer;
  xPathName:      array[0..260] of char;
  xMsg:           TWMDropFiles;
begin
  case Message.Msg of
    // Nur die Dropfiles Message abfragen
    WM_DROPFILES: begin
      xMsg := TWMDropFiles(Message);
      // Importform generieren
      with TfrmLKChartImportForm.Create(nil) do try
        Chart := mChart;
        ChartFrame := self;
        // Fragt ab wieviele Files "gedroppt" wurden
        xNumberDropped := DragQueryFile(xMsg.Drop, Cardinal(-1), nil, 0);

        // Den Namen f�r jedes File abfragen das importiert werden soll
        for i := 0 to xNumberDropped - 1 do begin
          DragQueryFile(xMsg.Drop, i, xPathName, SizeOf(xPathName));
          // ... und zur Stringlist hinzuf�gen
          Filenames.Add(xPathName);
        end;// for i := 0 to xNumberDropped - 1 do begin
        // Dialog anzeigen
        ImportFile;
      finally
        // Soll so sein (Platform SDK)
        Message.Result := 0;
        // Anzeigen, dass alles erledigt ist
        DragFinish(THandle(Message.wParam));
        // Import Dialog wieder freigeben
        free;
      end;// with TfrmLKChartImportForm.Create(nil) do try
    end;// WM_DROPFILES: begin
  else
    // Defaultbehandlung f�r alle anderen Messages;
    Message.Result := CallWindowProc(mOriginalWndProc, Handle, Message.Msg, Message.WParam, Message.LParam)
  end;// case Message.Msg of
end;// procedure TfrmlkChartFrame.WndProc(var Message: TMessage);

(* -----------------------------------------------------------
  Automatische Skalierung der Achsen ein oder ausschalten
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.cbChartAutoAxisClick(Sender: TObject);
begin
  mChart.LeftAxis.Automatic := cbChartAutoAxis.Checked;
end;// procedure TfrmlkChartFrame.cbChartAutoAxisClick(Sender: TObject);

(* -----------------------------------------------------------
  Setzt das Minimum f�r die Y-Achse
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.edChartYMinValidated(Sender: TLKEdit; aEnteredValue: Extended;
  var aValidatedValue: Extended);
begin
  mChart.LeftAxis.Minimum := TLKEdit(Sender).AsFloat;
end;// procedure TfrmlkChartFrame.edChartYMinValidated(Sender: TLKEdit; aEnteredValue: Extended;...

(* -----------------------------------------------------------
  Setzt das Maximum f�r die Y-Achse
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.edChartYMaxValidated(Sender: TLKEdit; aEnteredValue: Extended;
  var aValidatedValue: Extended);
begin
  mChart.LeftAxis.Maximum := TLKEdit(Sender).AsFloat;
end;// procedure TfrmlkChartFrame.edChartYMaxValidated(Sender: TLKEdit; aEnteredValue: Extended;...

(* -----------------------------------------------------------
  Konstruktor
-------------------------------------------------------------- *)
constructor TlkChartFrameList.Create;
begin
  inherited;
  mFrames := TList.Create;
end;// constructor TlkChartFrameList.Create;

(* -----------------------------------------------------------
  Destruktor
-------------------------------------------------------------- *)
destructor TlkChartFrameList.Destroy;
begin
  mFrames.Clear;
  FreeAndNil(mFrames);
  inherited;
end;// destructor TlkChartFrameList.Destroy;

(* -----------------------------------------------------------
  F�gt einen Frame zur Liste hinzu
-------------------------------------------------------------- *)
function TlkChartFrameList.Add(aFrame: TfrmlkChartFrame): Integer;
begin
  result := -1;
  if assigned(mFrames) then
    mFrames.Add(aFrame);
end;// function TlkChartFrameList.Add(aFrame: TfrmlkChartFrame): Integer;

(* -----------------------------------------------------------
  Im Moment keine Kopie erlaubt
-------------------------------------------------------------- *)
procedure TlkChartFrameList.Assign(Source: TPersistent);
begin
  if Source is TfrmlkChartFrame then begin
    raise Exception.Create('Assign f�r TlkChartFrameList nicht implementiert');
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TlkChartFrames then begin
end;// procedure TlkChartFrameList.Assign(Source: TPersistent);

(* -----------------------------------------------------------
  L�scht alle Frames aus der Liste. Die Frames werden nicht freigegeben
-------------------------------------------------------------- *)
procedure TlkChartFrameList.Clear;
begin
  while Count > 0 do
    delete(0);
end;// procedure TlkChartFrameList.Clear;

(* -----------------------------------------------------------
  Entfernt einen Frame aus der Liste.
  Der Frame wird nicht gel�scht.
-------------------------------------------------------------- *)
procedure TlkChartFrameList.Delete(aIndex: integer);
begin
  if assigned(mFrames) then begin
    if aIndex < mFrames.Count then
      mFrames.delete(aIndex);
  end;// if assigned(mFrames) then begin
end;// procedure TlkChartFrameList.Delete(aIndex: integer);

(* -----------------------------------------------------------
  Gibt ide Anzahl registrierter Frames zur�ck
-------------------------------------------------------------- *)
function TlkChartFrameList.GetCount: Integer;
begin
  result := 0;

  if assigned(mFrames) then
    result := mFrames.Count;
end;// function TlkChartFrameList.GetCount: Integer;

(* -----------------------------------------------------------
  Gibt den gew�nschten Frame zur�ck
-------------------------------------------------------------- *)
function TlkChartFrameList.GetItems(aIndex: Integer): TfrmlkChartFrame;
begin
  result := nil;
  if assigned(mFrames) then
    if mFrames.Count > aIndex then
      result := TfrmlkChartFrame(mFrames.Items[aIndex]);
end;// function TlkChartFrameList.GetItems(aIndex: Integer): TfrmlkChartFrame;

(* -----------------------------------------------------------
   Wird aufgerufen, wenn ein Punkt aus einer Serie gel�scht wurde
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.AfterDeletePoint(aSender: TLKChartLineSeries;
    aValueIndex: integer);
begin
  // Die Serie einfach erneut setzen und das Interval �bernehmen
  if aSender = mLastClickedSerie then
    SetCurrentSeries(aSender);
end;// procedure TfrmlkChartFrame.BeforeDeletePoint(aSender: TLKChartLineSeries;

(* -----------------------------------------------------------
  Wird aufgerufen, wenn der Text editiert wurde
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.DoAfterEditInfo(aSerie: TLKChartLineSeries;
    aValueIndex: integer; var aAdditionalText: string);
begin
  if Assigned(FOnAfterEditInfo) then
    FOnAfterEditInfo(Self, aSerie, aValueIndex, aAdditionalText);
end;// procedure TfrmlkChartFrame.DoAfterEditInfo(aSerie: TLKCHartLineSeries;

(* -----------------------------------------------------------
  Erm�glicht das editieren eines Textes als Zusatzinfo zu einem Pukt
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.DoBeforeEditInfo(aSerie: TLKChartLineSeries;
    aValueIndex: integer; var aAdditionalText: string);
begin
  if Assigned(FOnBeforeEditInfo) then
    FOnBeforeEditInfo(Self, aSerie, aValueIndex, aAdditionalText);
end;// procedure TfrmlkChartFrame.DoBeforeEditInfo(aSerie: TLKCHartLineSeries; ...

(* -----------------------------------------------------------
  Setzt f�r den Container eine neue Breite
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.mSettingsContainerChange(Sender: TObject);
begin
  if assigned(mSettingsContainer.ActivePage) then begin
    if mSettingsContainer.ActivePage.Tag > 50 then
      FChartSettingsPanel.PanelWidth := mSettingsContainer.ActivePage.Tag
    else
      FChartSettingsPanel.PanelWidth := 250;
  end;// if assigned(mSettingsContainer.ActivePage) then begin
end;// procedure TfrmlkChartFrame.mSettingsContainerChange(Sender: TObject);

(* -----------------------------------------------------------
  Setzt das Intervals der aktuellen Serie
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.edIntervalChange(Sender: TObject);
var
  xSerie: TLKChartLineSeries;
begin
  if mLastClickedSerie is TLKChartLineSeries then begin
    if not(mIsUpdatingSerieInterval) then begin
      xSerie := TLKChartLineSeries(mLastClickedSerie);
      xSerie.IntervalStart := edIntervalStart.Value;
      xSerie.IntervalEnd   := edIntervalEnd.Value;
      mChart.Invalidate;
      if cbAutorefresh.Checked then
        acRefreshStatistics.Execute;
    end;// if not(mIsUpdatingSerieInterval) then begin
  end;// if mLastClickedSerie is TLKChartLineSeries then begin
end;// procedure TfrmlkChartFrame.edIntervalEndChange(Sender: TObject);

(* -----------------------------------------------------------
  Zeichnet zusatzinformationen ins Chart
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.mChartBeforeDrawSeries(Sender: TObject);
var
  xCursorXPos: Integer;
  xSerie: TLKChartLineSeries;
begin
  // Zeichnet die Vorschau f�r die Ausreisser
  if assigned(mMaverik) then
    mMaverik.DrawMarkDeviation(Sender);

  // Zeichnet die Cursor f�r das Intervall
  if assigned(mLastClickedSerie) then begin
    if mLastClickedSerie is TLKChartLineSeries then begin
      xSerie := TLKChartLineSeries(mLastClickedSerie);

      if xSerie.FirstValueIndex < xSerie.IntervalStart then begin
        mChart.Canvas.Pen.Width := 2;
        mChart.Canvas.Pen.color := clRed;
        xCursorXPos := xSerie.CalcXPos(xSerie.IntervalStart);
        mChart.Canvas.Line(xCursorXPos, mChart.ChartRect.Top , xCursorXPos, mChart.ChartRect.Bottom);
      end;// if xSerie.FirstValueIndex < xSerie.IntervalStart then begin

      if xSerie.LastValueIndex > xSerie.IntervalEnd then begin
        mChart.Canvas.Pen.Width := 2;
        mChart.Canvas.Pen.color := clRed;
        xCursorXPos := xSerie.CalcXPos(xSerie.IntervalEnd);
        mChart.Canvas.Line(xCursorXPos, mChart.ChartRect.Top , xCursorXPos, mChart.ChartRect.Bottom);
      end;// if xSerie.LastValueIndex > xSerie.IntervalEnd then begin
    end;// if mLastClickedSerie is TLKChartLineSeries then begin
  end;// if assigned(mLastClickedSerie) then begin
end;// procedure TfrmlkChartFrame.mChartBeforeDrawSeries(Sender: TObject);

(* -----------------------------------------------------------
  Aktualisiert die Statistik
-------------------------------------------------------------- *)
procedure TfrmlkChartFrame.acRefreshStatisticsExecute(Sender: TObject);
begin
  if assigned(mStatisticsFrame) then begin
    mIsUpdatingSerieInterval := true;
    try
      if mStatisticsFrame.RawSerie is TLKChartLineSeries then begin
        edIntervalStart.Value := TLKChartLineSeries(mStatisticsFrame.RawSerie).IntervalStart;
        edIntervalEnd.Value := TLKChartLineSeries(mStatisticsFrame.RawSerie).IntervalEnd;
      end;// if mStatisticsFrame.RawSerie is TLKChartLineSeries then begin
    finally
      mIsUpdatingSerieInterval := false;
    end;// try finally
    mStatisticsFrame.RefreshStatistics;
  end;// if assigned(mStatisticsFrame) then begin
end;// procedure TfrmlkChartFrame.acRefreshStatisticsExecute(Sender: TObject);

end.
