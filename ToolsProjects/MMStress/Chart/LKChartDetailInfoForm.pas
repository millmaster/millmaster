unit LKChartDetailInfoForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, ExtCtrls, TeEngine, ToolWin, ActnList, ImgList, Buttons;

type
  TLKChartDetailInfoAction = (daDelPoint, 
                              daPrintPoint, 
                              daCopyPoint);// TLKChartDetailInfoAction
  TLKChartDetailInfoActionSet = Set of TLKChartDetailInfoAction;
   
  (*: Klasse:        TfrmDetailInfo
      Vorg�nger:     TForm
      Kategorie:     No category
      Kurzbeschrieb: Zeigt Detail Infos f�r einen Datenpunkt im Chart an 
      Beschreibung:  
                     - *)
  TfrmDetailInfo = class(TForm)
    pnMain: TPanel;
    timClose: TTimer;
    memoAdditionalText: TRichEdit;
    mButtonImages: TImageList;
    mButtonImages_d: TImageList;
    mButtonImages_h: TImageList;
    ActionList1: TActionList;
    acPrint: TAction;
    acPrintPreview: TAction;
    acDeletePoint: TAction;
    acCopyClipboard: TAction;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton2: TToolButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    acNavigateLeft: TAction;
    acNavigateRight: TAction;
    paInfo: TPanel;
    paColor: TPanel;
    Bevel1: TBevel;
    Label7: TLabel;
    laAVG: TLabel;
    Label5: TLabel;
    laValueCount: TLabel;
    laDuration: TLabel;
    laTime: TLabel;
    laValue: TLabel;
    laDurationCaption: TLabel;
    laTimeCaption: TLabel;
    Label1: TLabel;
    laTitle: TLabel;
    bPinButton: TSpeedButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    acStartInterval: TAction;
    acEndInterval: TAction;
    procedure acEndIntervalExecute(Sender: TObject);
    procedure acStartIntervalExecute(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction; var Handled: Boolean);
    procedure acNavigateRightExecute(Sender: TObject);
    procedure acNavigateLeftExecute(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure acCopyClipboardExecute(Sender: TObject);
    procedure acDeletePointExecute(Sender: TObject);
    //1 Merkt sich wo im Formular gecklickt wurde um anhand dieser Daten das Form zu verschieben 
    procedure ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    //1 Verschiebt das Formular an die aktuelle Position der Maus 
    procedure ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    //1 Verbirgt die Detail Infos wieder 
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    //1 Sobald das Formular nicht mehr das aktive Form ist, wird es ausgeblendet 
    procedure FormDeactivate(Sender: TObject);
    //1 Schliesst das Formular nach einer bestimmten Zeit 
    procedure timCloseTimer(Sender: TObject);
  private
    mSeries: TChartSeries;
    mMousePoint: TPoint;
    mValueIndex: Integer;
    function GetTextInfo: string;
    procedure SetEnableAction(const Value: TLKChartDetailInfoActionSet);
    procedure TimerRestart;
    procedure MoveRight;
    procedure MoveLeft;
    procedure IdentifyPoint(aValueIndex: Integer);
  public
    //1 Knstruktor 
    constructor Create(aOwner: TComponent); reintroduce; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Zeigt die Infos f�r den aktuellen Datenpunkt an 
    procedure ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
    //1 Blendet die Aktionen ein oder aus
    property EnableAction: TLKChartDetailInfoActionSet write SetEnableAction;
  end;

const
 cDefaultLKChartDetailInfoAction = [daDelPoint, daCopyPoint];
  
implementation
{$R *.DFM}
uses
  multimon, ChartSeries, Math, Clipbrd, LKGlobal, LKChartSeries;

const
  cWindowOffset = 5;

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (aOwner)
 *
 *  Kurzbeschreibung: Knstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TfrmDetailInfo.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  EnableAction := cDefaultLKChartDetailInfoAction;
end;// TfrmDetailInfo.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TfrmDetailInfo.Destroy;
begin
  inherited Destroy;
end;// TfrmDetailInfo.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           ControlsMouseDown
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Button, Shift, X, Y)
 *
 *  Kurzbeschreibung: Merkt sich wo im Formular gecklickt wurde um anhand dieser Daten das Form zu verschieben
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ControlsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mMousePoint := Point(X, Y);
end;// TfrmDetailInfo.ControlsMouseDown cat:No category

//:-------------------------------------------------------------------
(*: Member:           ControlsMouseMove
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Shift, X, Y)
 *
 *  Kurzbeschreibung: Verschiebt das Formular an die aktuelle Position der Maus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ControlsMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if ssLeft in Shift then begin
    Left := Left + (X - mMousePoint.x);
    Top  := Top + (Y - mMousePoint.y);
    TimerRestart;
  end;
end;// TfrmDetailInfo.ControlsMouseMove cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormClose
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender, Action)
 *
 *  Kurzbeschreibung: Verbirgt die Detail Infos wieder
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;// TfrmDetailInfo.FormClose cat:No category

//:-------------------------------------------------------------------
(*: Member:           FormDeactivate
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Sobald das Formular nicht mehr das aktive Form ist, wird es ausgeblendet
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.FormDeactivate(Sender: TObject);
begin
  if not(bPinButton.Down) then
    Close;
end;// TfrmDetailInfo.FormDeactivate cat:No category

(* -----------------------------------------------------------
  Anzeigen oder verbergen der Actions
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.SetEnableAction(const Value:
    TLKChartDetailInfoActionSet);
begin
  acPrint.Enabled         := daPrintPoint in Value;
  acDeletePoint.Enabled   := daDelPoint in Value;
  acCopyClipboard.Enabled := daCopyPoint in Value;
end;// procedure TfrmDetailInfo.SetEnableAction(const Value: TLKChartDetailInfoActionSet);

//:-------------------------------------------------------------------
(*: Member:           ShowInfo
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (aClickedSeries, X, Y, aValueIndex)
 *
 *  Kurzbeschreibung: Zeigt die Infos f�r den aktuellen Datenpunkt an
 *  Beschreibung:
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);
var
  xSeriesDetailInfo: TStringList;
  xRight: Integer;
  xBottom: Integer;
  i: Integer;
  xWidth: Integer;
  xLabel: TLabel;
  xCalcWorkareaSuccessful: Boolean;
  xClickPoint: TPoint;
  xMonitorInfo: tagMONITORINFOA;
  xMonitorHandle: THandle;
  xLineHeight: integer;
begin
  mSeries := aClickedSeries;

  if aValueIndex < 0 then
    aValueIndex := 0;
  if aValueIndex >= mSeries.Count then
    aValueIndex := mSeries.Count - 1;

  mValueIndex := aValueIndex;

  // evt. alten Punkt l�schen
  mSeries.ParentChart.Invalidate;
  Application.ProcessMessages;

  // Farbe des Fensters dem Punkt anpassen
  paColor.Color := mSeries.ValueColor[mValueIndex];
  Invalidate;
  
  IdentifyPoint(aValueIndex);

  if (mSeries is TLKLineSeries) then begin
    xSeriesDetailInfo := TLKLineSeries(mSeries).Items[mValueIndex];
    
    if assigned(xSeriesDetailInfo) then begin
      memoAdditionalText.Text := xSeriesDetailInfo.Text;
      memoAdditionalText.Visible := true;
      // Canvas f�r die H�henberechnung missbrauchen
      with TCanvas.Create do try
        Handle := GetDC(self.Handle);
        Font := memoAdditionalText.Font;
        xLineHeight := TextHeight('Mg');
      finally
        ReleaseDC(self.Handle, Handle);
        free;
      end;//  
      ClientHeight := memoAdditionalText.Top + xLineHeight * (memoAdditionalText.Lines.Count + 1);
    end else begin
      // Die H�he nur beim ersten Mal einstellen
      if memoAdditionalText.Visible then
        ClientHeight := paInfo.Top + paInfo.Height;
      memoAdditionalText.Visible := false;
    end;// if assigned(xSeriesDetailInfo) then begin
    
    laTitle.Caption := mSeries.Title;
    laValue.Caption := Format('%6f', [mSeries.YValue[mValueIndex]]) + ' (#' + IntToStr(aValueIndex + 1) +')';
    laValueCount.Caption := IntToStr(mSeries.Count);
    laAVG.Caption := Format('%6f', [TLKLineSeries(mSeries).AVG]);
    
    
    if mSeries.XValues.DateTime then begin
      laTimeCaption.Caption := 'Datum:';
      laTime.Caption := ShortDayNames[DayOfWeek(mSeries.XValue[mValueIndex])] + ' ' + DateToStr(mSeries.XValue[mValueIndex]);
      laDurationCaption.Caption := 'Zeit';
      laDuration.Caption := TimeToStr(mSeries.XValue[mValueIndex]);
    end else begin
      laTimeCaption.Caption := 'Zeit:';
      laTime.Caption  := Format('%.5g [h]', [mSeries.XValue[mValueIndex]]);
      laDurationCaption.Caption := 'Dauer';
      laDuration.Caption := Format('%.5g [h]', [mSeries.XValue[mValueIndex] - mSeries.XValue[0]]);
    end;// if mSeries.XValues.DateTime then begin
    // Nun noch die Koordinaten setzen und Show aufrufen
    // Screensize ohne Taskbar (= Workarea) ermitteln
    xCalcWorkareaSuccessful := false;
  
    xClickPoint.x := X;
    xClickPoint.y := Y;
  
    xRight  := Width;
    xBottom := Height;
  
    xMonitorHandle := MonitorFromPoint(xClickPoint, MONITOR_DEFAULTTONEAREST);
    if not(xMonitorhandle = INVALID_HANDLE_VALUE) then begin
      xMonitorInfo.cbSize := sizeof(xMonitorInfo);
      if (GetMonitorInfo(xMonitorHandle, @xMonitorInfo)) then begin
        xRight := xMonitorInfo.rcWork.Right - Width;
        xBottom := xMonitorInfo.rcWork.Bottom - Height;
        xCalcWorkareaSuccessful := true;
      end;// if (GetMonitorInfo(xMonitorHandle, @xMonitorInfo)) then begin
    end;//if not(xMonitorhandle = INVALID_HANDLE_VALUE) then begin
  
    if not(xCalcWorkareaSuccessful) then begin
      xRight  := Screen.Width - Width;
      xBottom := Screen.Height - Height - 25;
    end;
  
    if X > xRight  then
      X := xRight;
    if Y > xBottom then
      Y := xBottom;
  
    Left := X + cWindowOffset;
    Top  := Y + cWindowOffset;
  
    xWidth := 0;
    for i := 0 to ComponentCount - 1 do begin
      if Components[i] is TLabel then begin
        xLabel := TLabel(Components[i]);
        if (xLabel.Left + xLabel.Width) > xWidth then
          xWidth := xLabel.Left + xLabel.Width;
      end;// if Components[i] is TLabel then begin
    end;// for i := 0 to ComponentCount - 1 do begin
    Width := xWidth + 8;
  
    // Titel zentrieren     
    laTitle.Left := (Width - laTitle.Width) div 2;
  
    Show;
    TimerRestart;
  end;// if (mSeries is TLKLineSeries) then begin  
end;

procedure TfrmDetailInfo.IdentifyPoint(aValueIndex: Integer);
var
  xPointX: Integer;
  xPointY: Integer;
begin
  // Wenn als Koordinaten eine 0 �bergeben wird, dann die Position selber bestimmen
  xPointX := mSeries.CalcXPos(aValueIndex);
  xPointY := mSeries.CalcYPos(aValueIndex);
  mSeries.ParentChart.Canvas.Pen.Width := 5;
  mSeries.ParentChart.Canvas.Pen.Color := clBlack;
  mSeries.ParentChart.Canvas.Ellipse(xPointX - 10, xPointY - 10, xPointX + 10, xPointY + 10);
end;// procedure TfrmDetailInfo.ShowInfo(aClickedSeries: TChartSeries; X, Y, aValueIndex: Integer);

(* -----------------------------------------------------------
  Verschiebt den aktuellen Punkt nach Links
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.MoveLeft;
begin
  ShowInfo(mSeries, Left - cWindowOffset, Top - cWindowOffset, mValueIndex - 1);
end;

procedure TfrmDetailInfo.MoveRight;
begin
  ShowInfo(mSeries, Left - cWindowOffset, Top - cWindowOffset, mValueIndex + 1);
end;// TfrmDetailInfo.ShowInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           timCloseTimer
 *  Klasse:           TfrmDetailInfo
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Schliesst das Formular nach einer bestimmten Zeit
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TfrmDetailInfo.timCloseTimer(Sender: TObject);
begin
  if not(bPinButton.Down) then
    Close
  else
    timClose.Enabled := false;
end;// TfrmDetailInfo.timCloseTimer cat:No category

procedure TfrmDetailInfo.TimerRestart;
begin
  timClose.Enabled := false;
  timClose.Enabled := true;
end;

(* -----------------------------------------------------------
  L�scht den aktuellen Punkt
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acDeletePointExecute(Sender: TObject);
begin
  MessageDlg('Funktion nicht imlementiert', mtError, [mbOK], 0);
(*  if assigned(mSeries) then begin
    if InRange(mValueIndex, 0, mSeries.Count - 1) then begin
      mSeries.Delete(mValueIndex);
      close;
    end;// if InRange(mValueIndex, 0, mSeries.Count - 1) then begin
  end;// if assigned(mSeries) then begin*)
end;// procedure TfrmDetailInfo.acDeletePointExecute(Sender: TObject);

(* -----------------------------------------------------------
  Kopiert die Daten in die Zwischenablage
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acCopyClipboardExecute(Sender: TObject);
begin
  Clipboard.AsText := GetTextInfo;
end;// procedure TfrmDetailInfo.acCopyClipboardExecute(Sender: TObject);

(* -----------------------------------------------------------
  Stellt die Informationen des Punktes als String bereit
-------------------------------------------------------------- *)  
function TfrmDetailInfo.GetTextInfo: string;
begin
  result :=          'Serie Name:   ' + laTitle.Caption + cCrlf;
  result := result + 'Wert:         ' + laValue.Caption + cCrlf;
  result := result + 'Zeit:         ' + laTime.Caption + cCrlf;
  result := result + 'Dauer:        ' + laDuration.Caption + cCrlf;
  result := result + 'Serie AVG:    ' + laAVG.Caption + cCrlf;
  result := result + 'Anzahl Werte: ' + laValueCount.Caption + cCrlf;
  result := result + cCrlf + 'Zusatzinfo:' + cCrlf;
  result := result + memoAdditionalText.Text;
end;// function TfrmDetailInfo.GetTextInfo: string;

(* -----------------------------------------------------------
  Cursortasten f�r die Navigation abfangen.
-------------------------------------------------------------- *)
procedure TfrmDetailInfo.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  case  Key of
    VK_RIGHT: begin
      MoveRight;
      Key := 0;
    end;// VK_RIGHT: begin
    VK_LEFT: begin
      MoveLeft;
      Key := 0;
    end;// VK_LEFT: begin
  end;// case  Key of
end;// procedure TfrmDetailInfo.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

(* -----------------------------------------------------------
  Verschiebt den angezeigten Punkt nach Links
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acNavigateLeftExecute(Sender: TObject);
begin
  MoveLeft;
end;// procedure TfrmDetailInfo.acNavigateLeftExecute(Sender: TObject);

(* -----------------------------------------------------------
  Verschiebt den angezeigten Punkt nach Rechts
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acNavigateRightExecute(Sender: TObject);
begin
  MoveRight;
end;// procedure TfrmDetailInfo.acNavigateRightExecute(Sender: TObject);

(* -----------------------------------------------------------
  GUI updaten
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.ActionList1Update(Action: TBasicAction; var Handled: Boolean);
begin
  acNavigateRight.Enabled := mValueIndex < (mSeries.Count - 1);
  acNavigateLeft.Enabled := mValueIndex > 0;
end;// procedure TfrmDetailInfo.ActionList1Update(Action: TBasicAction; var Handled: Boolean);

(* -----------------------------------------------------------
  Setzt den Startpunkt des Intervalls der aktuellen Serie
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acStartIntervalExecute(Sender: TObject);
begin
  if mSeries is TLKChartLineSeries then begin
    TLKChartLineSeries(mSeries).IntervalStart := mValueIndex;
    if assigned(mSeries.ParentChart) then
      mSeries.ParentChart.Invalidate;
  end;// if mSeries is TLKChartLineSeries then begin
end;// procedure TfrmDetailInfo.acStartIntervalExecute(Sender: TObject);

(* -----------------------------------------------------------
  Setzt den Endpunkt des Intervalls der aktuellen Serie
-------------------------------------------------------------- *)  
procedure TfrmDetailInfo.acEndIntervalExecute(Sender: TObject);
begin
  if mSeries is TLKChartLineSeries then begin
    TLKChartLineSeries(mSeries).IntervalEnd := mValueIndex;
    if assigned(mSeries.ParentChart) then
      mSeries.ParentChart.Invalidate;
  end;// if mSeries is TLKChartLineSeries then begin
end;// procedure TfrmDetailInfo.acEndIntervalExecute(Sender: TObject);

end.
