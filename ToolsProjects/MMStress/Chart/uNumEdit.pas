unit uNumEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TLKEdit = class;
  
  // Zeitpunkt der Validierung
  TValidateMode = (vmNone,   // Es wird nicht Validiert
                   vmInput,  // es wird bei jeder Eingabe validiert
                   vmExit    // es wird errst Validiert, wenn das Element verlassen wird
                   );// TValidateMode

  // Art der Validierung
  TValidateFrom = (vfApp,
                   vfInput,
                   vfExit
                   );// TValidateFrom

  TInvalidInput = procedure(Sender: TObject; aOrgText: string) of object;
  TInvalidRange = procedure(Sender: TObject; aMinValue, aMaxValue, aValue: extended) of object;
  TValidated = procedure(Sender: TLKEdit; aEnteredValue: extended; var aValidatedValue: extended) of object;

  TNumEdit=class(TEdit)
  private
    FOldText:string;
    function GetAsExtended: extended;
    function GetAsInteger: integer;
    procedure SetAsExtended(const Value: extended);
    procedure SetAsInteger(const Value: integer);
  public
    procedure Change; override;

    property asInteger:integer read GetAsInteger write SetAsInteger;
    property asExtended:extended read GetAsExtended write SetAsExtended;
  end;

  TLKEdit = class(TEdit)
  private
    fAlignment: TAlignment;
    FDecimals: Integer;
    fInfoText: String;
    FInvalidInputMessage: string;
    FInvalidRangeMessage: string;
    FMaxValue: Extended;
    FMinValue: Extended;
    fNumMode: Boolean;
    FOnInvalidInput: TInvalidInput;
    FOnInvalidRange: TInvalidRange;
    FOnValidated: TValidated;
    FValidateMode: TValidateMode;
    mDecimalSeparatorPressed: Boolean;
    mFontColor: TColor;
    mOldValue: String;
    function GetAsFloat: Extended;
    function GetAsInteger: Integer;
    function GetValue: Variant;
    procedure SetAsFloat(const Value: Extended);
    procedure SetAsInteger(const Value: Integer);
    procedure SetDecimals(const Value: Integer);
    procedure SetValue(const Value: Variant);
    procedure SetNumMode(const Value: Boolean);
    procedure SetInfoText(const Value: String);
  protected
    procedure Change; override;
    procedure CreateParams(var Params: TCreateParams); override;
    procedure DoEnter; override;
    procedure DoExit; override;
    procedure DoInvalidInput(aOrgText: string);
    procedure DoInvalidRange(aMinValue, aMaxValue, aValue: extended);
    procedure DoValidated(aEnteredValue: extended; aValidatedValue: extended);
    procedure SetAlignment(Value: TAlignment);
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure Loaded; override;
    procedure Validate;
    property AsFloat: Extended read GetAsFloat write SetAsFloat;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property Value: Variant read GetValue write SetValue;
  published
    property Alignment: TAlignment read fAlignment write SetAlignment default taLeftJustify;
    property Decimals: Integer read FDecimals write SetDecimals;
    property InfoText: String read fInfoText write SetInfoText;
    property InvalidInputMessage: string read FInvalidInputMessage write FInvalidInputMessage;
    property InvalidRangeMessage: string read FInvalidRangeMessage write FInvalidRangeMessage;
    property MaxValue: Extended read FMaxValue write FMaxValue;
    property MinValue: Extended read FMinValue write FMinValue;
    property NumMode: Boolean read fNumMode write SetNumMode;
    property OnInvalidInput: TInvalidInput read FOnInvalidInput write FOnInvalidInput;
    property OnInvalidRange: TInvalidRange read FOnInvalidRange write FOnInvalidRange;
    property OnValidated: TValidated read FOnValidated write FOnValidated;
    property ValidateMode: TValidateMode read FValidateMode write FValidateMode;
  end;

procedure Register;

implementation
uses
  LKGlobal;

procedure Register;
begin
  RegisterComponents('LK', [TNumEdit]);
  RegisterComponents('LK', [TLKEdit]);
end;

{ TNumEdit }
procedure TNumEdit.Change;
begin
  inherited Change;
  if (not(csDesigning	 in ComponentState)) then begin
    try
      if Text>'' then begin
        if not((Text='-')or(Text='+'))then
          strToFloat(Text);
      end;
      FOldText:=Text;
    except
      on e:EConvertError do begin
        Text:=FOldText;
        SelStart:=Length(Text);
      end;// on e:EConvertError do begin
    end;// try except
  end;//
end;// procedure TNumEdit.Change;

//******************************************************************************
//
//******************************************************************************
function TNumEdit.GetAsExtended: extended;
begin
  try
    result:=strToFloat(Text);
  except
    on e:EConvertError do
      result:=0;
  end;
end;

//******************************************************************************
//
//******************************************************************************
function TNumEdit.GetAsInteger: integer;
begin
  result:=Trunc(GetAsExtended);
end;


procedure TNumEdit.SetAsExtended(const Value: extended);
begin
  Text:=FloatToStr(Value);
end;

procedure TNumEdit.SetAsInteger(const Value: integer);
begin
  Text:=IntToStr(Value);
end;

//:---------------------------------------------------------------------------
//:--- Class: TmmEdit
//:---------------------------------------------------------------------------
constructor TLKEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAlignment     := taLeftJustify;
  fInfoText      := '';
  fNumMode       := False;
  
  mFontColor := Font.Color;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.Change;
begin
  inherited Change;
  mDecimalSeparatorPressed := (AnsiPos(Decimalseparator, Text) > 0);
  if NumMode then begin
    if FValidateMode = vmInput then
      Validate;
  end else begin
    if fInfoText <> '' then begin
      if Focused or (not AnsiSameText(inherited Text, fInfoText)) then begin
        Font.Color := mFontColor;
      end else
        Font.Color := clGrayText;
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.CreateParams(var Params: TCreateParams);

  const
    Alignments: array[TAlignment] of DWORD = (ES_LEFT, ES_RIGHT, ES_CENTER);
  
begin
  inherited CreateParams(Params);
  Params.Style := Params.Style {or ES_MULTILINE {}or Alignments[Alignment]{};
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.DoEnter;
begin
  if NumMode then
    mOldValue := FloatToStr(AsFloat);
  inherited DoEnter;
  //:...................................................................
  Font.Color := mFontColor;
  if AnsiSameText(inherited Text, fInfoText) then begin
    inherited Text := '';
  end;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.DoExit;
var
  xOldValidateMode: TValidateMode;
begin
  inherited DoExit;

  //:...................................................................
  if FValidateMode in [vmExit, vmInput] then begin
    xOldValidateMode := FValidateMode;
    try
      FValidateMode := vmExit;
      Validate;
    finally
      FValidateMode := xOldValidateMode;
    end;// try finally
  end;// if FValidateMode in [vmExit, vmInput] then begin

  //:...................................................................
  if (inherited Text = '') and (fInfoText <> '') then
    Text := fInfoText;
end;

procedure TLKEdit.DoInvalidInput(aOrgText: string);
begin
  if Assigned(FOnInvalidInput) then FOnInvalidInput(Self, aOrgText);
end;

procedure TLKEdit.DoInvalidRange(aMinValue, aMaxValue, aValue: extended);
begin
  if Assigned(FOnInvalidRange) then FOnInvalidRange(Self, aMinValue, aMaxValue, aValue);
end;

procedure TLKEdit.DoValidated(aEnteredValue: extended; aValidatedValue: extended);
begin
  if Assigned(FOnValidated) then FOnValidated(Self, aEnteredValue, aValidatedValue);
end;

//:---------------------------------------------------------------------------
function TLKEdit.GetAsFloat: Extended;
begin
  try
    if Text > '' then
      result := StrToFloat(Text)
    else
      result := 0;
  except
    result := 0;
  end;
end;

//:---------------------------------------------------------------------------
function TLKEdit.GetAsInteger: Integer;
begin
  result := Trunc(AsFloat);
end;

//:---------------------------------------------------------------------------
function TLKEdit.GetValue: Variant;
begin
  result := Text;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.Loaded;
begin
  inherited Loaded;
  if (inherited Text = '') and (fInfoText <> '') then begin
    inherited Text := fInfoText;
  end;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.SetAlignment(Value: TAlignment);
begin
  if FAlignment <> Value then begin
    FAlignment := Value;
    RecreateWnd;
  end;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.SetAsFloat(const Value: Extended);
begin
  try
    Text := Format('%.18g', [Value]);
  except
    Text := '0';
  end;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.SetAsInteger(const Value: Integer);
begin
  AsFloat := Value;
end;

procedure TLKEdit.SetDecimals(const Value: Integer);
begin
  if FDecimals <> Value then
  begin
    FDecimals := Value;
    Validate;
  end;
end;

procedure TLKEdit.SetInfoText(const Value: String);
begin
  if (Value <> fInfoText) then begin
    if (AnsiSameText(inherited Text, fInfoText)) then
      inherited Text := Value;
    fInfoText := Value;
    Change;
  end;// if (Value <> fInfoText) then begin
end;

procedure TLKEdit.SetNumMode(const Value: Boolean);
begin
  fNumMode := Value;
  if FNumMode then
    Alignment := taRightJustify
  else
    Alignment := taLeftJustify;
end;

//:---------------------------------------------------------------------------
procedure TLKEdit.SetValue(const Value: Variant);
begin
  Text := Value;
end;

procedure TLKEdit.Validate;
var
  xValue: extended;
  xPre: string;
  xPost: string;
  xExponent: string;
  xExponentPos: integer;
  xDecimalSeparator: string;
  xOrgText: string;

  procedure ChangeText(aText: string);
  var
    xOldSelStart: Integer;
  begin
    xOldSelStart := SelStart;// - 1;
    Text := aText;
    SelStart := xOldSelStart;
  end;// procedure ChangeText(aText: string);

begin
  if NumMode then begin
    xOrgText := Text;
    // Beschränkung auf Zahlen
    try
      if Text > '' then begin
        if (Text <> '-') and (Text <> '+')  then
          StrToFloat(Text);
      end;// if Text > '' then begin
      mOldValue := Text;
    except
      if FInvalidInputMessage > '' then
        MessageDlg(FInvalidInputMessage, mtError, [mbOK], 0);
      DoInvalidInput(xOrgText);
      ChangeText(mOldValue);
    end;// try except

    // Anzahl Dezimalstellen
    if FDecimals >= 0 then begin
      xExponent := '';
      xPost := Text;
      xPre := copyLeft(xPost, DecimalSeparator, true);
      xExponentPos := AnsiPos('E', UpperCase(xPost));
      if xExponentPos > 0 then
        xExponent := copy(xPost, xExponentPos, MAXINT);

      xDecimalSeparator := DecimalSeparator;
      if (Length(xPost) - Length(xExponent)) > FDecimals then
        xPost := copy(xPost, 1, FDecimals) + xExponent;

      if (xPost = '') and (not(mDecimalSeparatorPressed)) then
        xDecimalSeparator := '';

      if (FDecimals > 0) then
        ChangeText(xPre + xDecimalSeparator + xPost)
      else
        ChangeText(xPre);
    end;// if FDecimals >= 0 then begin

    // Minimum, Maximum
    if MinValue <> 0 then begin
      if AsFloat < MinValue then begin
        DoInvalidRange(FMinValue, FMaxValue, AsFloat);
        AsFloat := MinValue;
        if FInvalidRangeMessage > '' then
          MessageDlg(FInvalidRangeMessage, mtError, [mbOK], 0);
      end;// if AsFloat < MinValue then begin
    end;// if MinValue <> 0 then begin

    if MaxValue <> 0 then begin
      if AsFloat > MaxValue then begin
        DoInvalidRange(FMinValue, FMaxValue, AsFloat);
        AsFloat := MaxValue;
        if FInvalidRangeMessage > '' then
          MessageDlg(FInvalidRangeMessage, mtError, [mbOK], 0);
      end;// if AsFloat > MaxValue then begin
    end;// if MaxValue <> 0 then begin

    mDecimalSeparatorPressed := (AnsiPos(Decimalseparator, Text) > 0);
    
    xValue := AsFloat;
    DoValidated(StrToFloat(mOldValue), xValue);
    AsFloat := xValue;
  end;// if NumMode then begin
end;

(* -----------------------------------------------------------
  Tastaturbehandlung
-------------------------------------------------------------- *)  
procedure TLKEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN: begin
      Validate;
    end;// VK_RETURN: begin
  end;// case Key of
end;// procedure TLKEdit.KeyDown(var Key: Word; Shift: TShiftState);

end.
