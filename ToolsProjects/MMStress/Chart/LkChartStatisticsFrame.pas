unit LkChartStatisticsFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, LKChartSeries, StdCtrls, ToolWin, ComCtrls, Buttons, Spin, ExtCtrls,
  TeEngine, uNumEdit, ActnList, Grids, JclBase;

type
  TfrmLKChartStatisticsFrame = class(TFrame)
    mAVGControlBar: TToolBar;
    Label1: TLabel;
    laValueCount: TLabel;
    Label3: TLabel;
    laMinValue: TLabel;
    Label5: TLabel;
    Label8: TLabel;
    laMaxValue: TLabel;
    mCompareGrid: TStringGrid;
    Panel1: TPanel;
    Label2: TLabel;
    Label4: TLabel;
    Label6: TLabel;
    laAVG: TLabel;
    laStandardDeviation: TLabel;
    laVariance: TLabel;
  private
    FRawSerie: TChartSeries;
    FUpdateCnt: Integer;
    mAVGMethod: TLKChartAVGMethod;
    procedure FillValues(var aValues: TDynFloatArray);
    procedure SetRawSerie(const Value: TChartSeries);
    { Private-Deklarationen }
  protected
    function IsUpdating: Boolean;
    procedure SetUpdating(Updating: Boolean);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure BeginUpdate;
    procedure EndUpdate;
    procedure RefreshStatistics;
    property RawSerie: TChartSeries read FRawSerie write SetRawSerie;
    { Public-Deklarationen }
  end;

implementation

{$R *.dfm}
uses
  LKGlobal, series, TypInfo, TeeProcs, JclStatistics;

const
  cColFrom  = 0;
  cColTo    = 1;
  cColValue = 2;

  cMaxToMinPercent  = 1;
  cMaxToMinAbsolute = 2;
  cMinToMaxPercent  = 3;

(* -----------------------------------------------------------
  Initialisierung der Mittelwertmethode
-------------------------------------------------------------- *)
constructor TfrmLKChartStatisticsFrame.Create(aOwner: TComponent);
begin
  inherited;
  mAVGMethod := TLKChartAVGMethod.Create(stAVG, nil, nil);
  mAVGMethod.CreateControls(mAVGControlBar);

  // Initialisiert das Grid
  with mCompareGrid do begin
    Cells[cColFrom, 0] := 'von';
    Cells[cColTo, 0] := 'zu';
    Cells[cColValue, 0] := 'Abweichung';

    Cells[cColFrom, cMaxToMinPercent]  := 'Max';
    Cells[cColFrom, cMaxToMinAbsolute] := 'Max';
    Cells[cColFrom, cMinToMaxPercent]  := 'Min';

    Cells[cColTo, cMaxToMinPercent]    := 'Min';
    Cells[cColTo, cMaxToMinAbsolute]   := 'Min';
    Cells[cColTo, cMinToMaxPercent]    := 'Max';
  end;// with mCompareGrid do begin
end;// constructor TfrmLKChartMaverik.Create(aOwner: TComponent);

(* -----------------------------------------------------------
  Aufr�umen
-------------------------------------------------------------- *)
destructor TfrmLKChartStatisticsFrame.Destroy;
begin
  mAVGMethod.DestroyControls;
  FreeAndNil(mAVGMethod);
  inherited;
end;// destructor TfrmLKChartMaverik.Destroy;

procedure TfrmLKChartStatisticsFrame.BeginUpdate;
begin
  Inc(FUpdateCnt);
  if FUpdateCnt = 1 then SetUpdating(True);
end;

procedure TfrmLKChartStatisticsFrame.EndUpdate;
begin
  Dec(FUpdateCnt);
  if FUpdateCnt = 0 then SetUpdating(False);
end;

(* -----------------------------------------------------------
  F�llt das Array mit den Werten im gew�hlten Interval
-------------------------------------------------------------- *)
procedure TfrmLKChartStatisticsFrame.FillValues(var aValues: TDynFloatArray);
var
  xSerie: TLKChartLineSeries;
  i: Integer;
begin
  if FRawSerie is TLKChartLineSeries then begin
    xSerie := TLKChartLineSeries(FRawSerie);
    SetLength(aValues, xSerie.IntervalEnd - xSerie.IntervalStart + 1);
    for i := xSerie.IntervalStart to xSerie.IntervalEnd  do begin
      aValues[i - xSerie.IntervalStart] := FRawSerie.YValues[i];
    end;// for i := StartIndex to EndIndex do begin
  end;// if FRawSerie is TLKChartLineSeries then begin
end;// procedure TfrmLKChartStatisticsFrame.FillValues(aValues: TDynFloatArray);

function TfrmLKChartStatisticsFrame.IsUpdating: Boolean;
begin
  Result := FUpdateCnt > 0;
end;

(* -----------------------------------------------------------
  Berechnet die Statistikdaten neu
-------------------------------------------------------------- *)
procedure TfrmLKChartStatisticsFrame.RefreshStatistics;
var
  xVariance: Extended;
  xMin: Double;
  xMax: Double;
  xValues: TDynFloatArray;
begin
  if not(IsUpdating) then begin
    if assigned(FRawSerie) then begin
      FillValues(xValues);
      xMax := MaxFloatArray(xValues);
      xMin := MinFloatArray(xValues);

      laValueCount.Caption := IntToStr(Length(xValues));
      laMaxValue.Caption := Format('%6f', [xMax]);
      laMinValue.Caption := Format('%6f', [xMin]);
      laAVG.Caption := Format('%6f', [ArithmeticMean(xValues)]);
      xVariance := PopulationVariance(xValues);
      laStandardDeviation.Caption := Format('%6f', [SQRT(xVariance)]);
      laVariance.Caption := Format('%6f', [xVariance]);

      with mCompareGrid.Cols[cColValue] do begin
        Strings[cMaxToMinPercent]  := Format('%6f%%', [100 - xMin * 100 / xMax]);
        Strings[cMaxToMinAbsolute] := Format('%6f', [xMax - xMin]);
        Strings[cMinToMaxPercent]  := Format('%6f%%', [(xMax * 100 / xMin) - 100]);
      end;// with mCompareGrid do begin
    end;// if assigned(FRawSerie) then begin
  end;// if not(IsUpdating) then begin
end;// procedure TfrmLKChartMaverik.RefreshStatistics;

(* -----------------------------------------------------------
  Aktuelle Serie f�r die Statistik
-------------------------------------------------------------- *)
procedure TfrmLKChartStatisticsFrame.SetRawSerie(const Value: TChartSeries);
begin
  FRawSerie := Value;
  RefreshStatistics;
end;// procedure TfrmLKChartStatisticsFrame.SetRawSerie(const Value: TChartSeries);

(* -----------------------------------------------------------
  Wird beim BeginUpdate (=true) und beim Endupdate (=false) aufgerufen
-------------------------------------------------------------- *)
procedure TfrmLKChartStatisticsFrame.SetUpdating(Updating: Boolean);
begin
  RefreshStatistics;
end;// procedure TfrmLKChartStatisticsFrame.SetUpdating(Updating: Boolean);

end.
