unit LKGlobal;

{$I LKCompilers.inc}
interface
uses
  windows, sysutils, classes, typinfo, syncobjs, Graphics{$ifdef DELPHI_5}, FileCtrl{$endif};

// Sendet einen String an die API Funktion OutputDebugString
procedure ODS(aValue: extended; aText: string = ''); overload;
// Gibt eine Anzahl Leerzeichen zur�ck
function Space(aCount: integer): String;

// Liest einen Integer aus dem Inifile
function ReadIntegerFromIni(aFileName: string; aSection: string; aValue:string; aDefault:Integer): integer;
// Schreibt einen Integer in das Inifile
procedure WriteIntegerToIni(aFileName: string; aSection: string; aValue:string; aInteger:Integer);
// Formatiert eine Fehlermeldung die mit GetLastError abgefragt wurde: TranslateError(GetLastError,'');
procedure TranslateError(Error:DWORD;Titel:string);

const
  cDefaultConnectionString = 'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Data Source=%s;Initial Catalog=%s%s%s';
  
  cr   = #13;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;
  cCrlf = crlf;
  
(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  der Original String zur�ckgegeben.
  Ist 'aCut' = true, dann wird der Originalstring gek�rzt.
-------------------------------------------------------------*)
function CopyLeft(var aString: string; aSeparator: string; aCut: boolean): String;  overload;

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  der Original String zur�ckgegeben.
-------------------------------------------------------------*)
function CopyLeft(aString: string; aSeparator: string): String; overload;

(*-----------------------------------------------------------
  Gibt den rechten Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  ein Leerstring zur�ckgegeben.
-------------------------------------------------------------*)
function CopyRight(aString: string; aSeparator: string): String;

// Sendet einen String an die API Funktion OutputDebugString
procedure ODS(aValue: integer; aText: string = ''); overload;

// Sendet einen String an die API Funktion OutputDebugString
procedure ODS(aMsg: string; aText: string = ''); overload;

procedure ODSClear;

procedure ODSAddSeparator;

function GetGradientColor(aColor: TColor; aPercentage: Integer): TColor;

// Liefert den Namen eines neuen tempor�ren Files
function GetTempFilePath: string;

//  Listet alle in der Datei vorhandenen Streams auf
procedure EnumDataStreams(aFilename: string; aNames: TStrings; aComplete:
    boolean = false);

// Fragt ab ob die Datei mit dem gew�nschten Dateistream existiert
function FileDataStreamExists(aFileName: string): Boolean; overload;
function FileDataStreamExists(aFileName: string; aStreamName: string): Boolean; overload;

var
  gSQLServerName: string = 'LKRANERMOBIL';
  gDBName: string = 'Innolab_Versuch';
  gDBPassword: string;
  gDBUserName: string;

implementation

uses
  inifiles, ColorLibrary, JclNTFS, Unicode;

//*********************************************************
//  Formatiert eine Fehlermeldung die mit GetLastError abgefragt wurde
//
//  Aufruf:
//    TranslateError(GetLastError,'');
//*********************************************************
procedure TranslateError(Error:DWORD; Titel:string);
var
  // Puffer einrichten
  lpMeldungsPuffer:array [0..1029]of char;
begin
  // Fehlermeldung in Klartext ausgeben
  FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM,
                // wird bei System Messages ignoriert
                nil,
                // Fehler der mit GetLastError abgefragt wurde
                Error,
                // Sucht die Passende Sprache selber
                0,
                // Puffer f�r den Klartext
                lpMeldungsPuffer,
                // Gr�sse des Puffers
                1024,
                // Keine Formatierungs strings
                nil);
  // Meldung anzeigen
  MessageBox(HWND(nil),lpMeldungsPuffer,PChar(Titel),MB_OK or MB_ICONERROR);
end;// procedure TranslateError(Error:DWORD;Titel:string);

function Space(aCount: integer): String;
var
  i:integer;
begin
  Result := '';
  for i := 1 to aCount do
    result := result + ' ';
end;// function Space(aCount: integer): String;

// Liest einen Integer aus dem Inifile
function ReadIntegerFromIni(aFileName: string; aSection: string; aValue:string; aDefault:Integer): integer;
begin
  with TIniFile.Create(aFileName) do try
    result := ReadInteger(aSection, aValue, aDefault);
  finally
    free;
  end;// with TIniFile.Create(aFileName) do try
end;// function ReadIntegerFromIni(aFileName: string; aSection: string; aValue:string; aDefault:Integer): integer;

// Schreibt einen Integer in das Inifile
procedure WriteIntegerToIni(aFileName: string; aSection: string; aValue:string; aInteger:Integer);
begin
  with TIniFile.Create(aFileName) do try
    WriteInteger(aSection, aValue, aInteger);
  finally
    free;
  end;// with TIniFile.Create(aFileName) do try
end;

//------------------------------------------------------------------------------

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird 
  der Original String zur�ckgegeben. 
  Ist 'aCut' = true, dann wird der Originalstring gek�rzt. 
-------------------------------------------------------------*)
function CopyLeft(var aString: string; aSeparator: string; aCut: boolean): String;
var
  xPos: integer;
begin
  result := aString;
  
  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, result);
  
  // Linke Seite kopieren
  if xPos > 0 then
    result := copy(result, 1, xPos - 1);
    
  // Wenn der String gek�rzt werden soll
  if aCut then begin
    // rechte Seite hinter dem Separator zur�ckgeben
    if xPos > 0 then
      aString := copy(aString, xPos + Length(aSeparator), MAXINT)
    else
      // Ist der Separator nicht im String enthalten, dann Leerstring zur�ckgeben
      aString := ''; 
  end;// if aCut then begin
end;// function CopyLeft(aString: string; aSeparator: string):string;

(*-----------------------------------------------------------
  Gibt den linken Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird 
  der Original String zur�ckgegeben. 
-------------------------------------------------------------*)
function CopyLeft(aString: string; aSeparator: string): String;
var
  xPos: integer;
begin
  result := aString;
  
  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, result);
  
  // Linke Seite kopieren
  if xPos > 0 then
    result := copy(result, 1, xPos - 1);
end;// function CopyLeft(aString: string; aSeparator: string):string;

(*-----------------------------------------------------------
  Gibt den rechten Teilstring bis zum Separator zur�ck.
  Kommt der Separator nicht im String vor, dann wird
  ein Leerstring zur�ckgegeben.
-------------------------------------------------------------*)
function CopyRight(aString: string; aSeparator: string): String;
var
  xPos: integer;
begin
  result := '';
  
  // Position des Separators feststellen
  xPos := AnsiPos(aSeparator, aString);
  
  // Rechte Seite kopieren
  if xPos > 0 then
    result := copy(aString, xPos + Length(aSeparator), MAXINT);
end;// function CopyLeft(aString: string; aSeparator: string):string;

function GetGradientColor(aColor: TColor; aPercentage: Integer): TColor;
var
  xH, xL, xS: Integer;
  xRGB: TRGBTriple;
begin
  Result := aColor;
  with xRGB do begin
    rgbtRed   := GetRValue(aColor);
    rgbtGreen := GetGValue(aColor);
    rgbtBlue  := GetBValue(aColor);
  end;

  RGBTripleToHLS(xRGB, xH, xL, xS);
  xL := Round(xL * (1 + (aPercentage / 100)));
  if xL < 0 then xL := 0
  else if xL > 246 then xL := 246;
//  else if xL > 255 then xL := 255;

  try
    xRGB := HLSToRGBTriple(xH, xL, xS);
    with xRGB do
      Result := RGB(rgbtRed, rgbtGreen, rgbtBlue);
  except
  end;

end;

procedure ODS(aMsg: string; aText: string = '');
begin
  if aText > '' then
    OutputDebugString(PChar(aText + ' = ' + aMsg))
  else
    OutputDebugString(PChar(aMsg));
end;// procedure ODS(aMsg: string);

procedure ODS(aValue: integer; aText: string = '');
begin
  ODS(IntToStr(aValue), aText);
end;// procedure ODS(aMsg: string);

procedure ODS(aValue: extended; aText: string = '');
begin
  ODS(FloatToStr(aValue), aText);
end;// procedure ODS(aMsg: string);

procedure ODSClear;
begin
  ODS('DBGVIEWCLEAR');
end;

procedure ODSAddSeparator;
begin
  ODS('----------------------------------------------------------------------');
end;

(* -----------------------------------------------------------
  Liefert den Namen eines neuen tempor�ren Files
-------------------------------------------------------------- *)  
function GetTempFilePath: string;
var
  xPath: string;
begin
  SetLength(xPath, MAX_PATH);
  SetLength(xPath, GetTempPath(MAX_PATH, PChar(xPath)));
  if Length(xPath) > 0 then begin
    if not(DirectoryExists(xPath)) then
      ForceDirectories(xPath);
    SetLength(result, MAX_PATH);
    GetTempFileName(PChar(xPath), 'Inn', 0, PChar(result));
    result := StrPAs(PChar(result));
  end;// if Length(xPath > 0) then begin
end;// function GetTempFilePath: string;

(* -----------------------------------------------------------
  Fragt ab ob die Datei mit dem gew�nschten Dateistream existiert
  Aufruf: if FileDataStreamExists('C:test.txt', 'Streamname') then ...
-------------------------------------------------------------- *)
function FileDataStreamExists(aFileName: string; aStreamName: string): Boolean;
var
  xNames: TStringList;
begin
  result := false;
  if  FileExists(aFileName) then begin
    xNames := TStringList.Create;
    try
      EnumDataStreams(aFileName, xNames, false);
      result := (xNames.IndexOf(aStreamName) > 0);
    finally
      xNames.Free;
    end;// try finally
  end;// if result then begin
end;// function FileDataStreamExists(aFileName: string; aStreamName: string): Boolean;

(* -----------------------------------------------------------
  Fragt ab ob die Datei mit dem gew�nschten Dateistream existiert
  Aufruf: if FileDataStreamExists('C:test.txt:Streamname') then ...
-------------------------------------------------------------- *)
function FileDataStreamExists(aFileName: string): Boolean;
var
  xFileName: string;
  xStreamName: string;
  xDelimiterPos: integer;
begin
  xFileName := aFilename;
  xDelimiterPos := LastDelimiter(':', aFilename);
  if xDelimiterPos > 2 then begin
    xFileName := copy(aFilename, 1, xDelimiterPos - 1);
    xStreamName := copy(aFilename, xDelimiterPos + 1, MAXINT);
  end;
  result := FileDataStreamExists(xFileName, xStreamName);
end;// function FileDataStreamExists(aFileName: string): Boolean;

(* -----------------------------------------------------------
  Listet alle in der Datei vorhandenen Streams auf
    Beispiel f�r eine Datei mit 5 Streams:
      ':?SummaryInformation:$DATA'
      ':trg:$DATA'
      ':trt:$DATA'
      ':trx:$DATA'
      ':{4c8cc155-6c1e-11d1-8e41-00c04fb9386d}:$DATA'

  Wenn das Argument "aComplete" false ist, dann wird nur der Name zur�ckgegeben (ohne ':' und ';$DATA')
-------------------------------------------------------------- *)  
procedure EnumDataStreams(aFilename: string; aNames: TStrings; aComplete:
    boolean = false);
var
  i: Integer;
  xFileName: string;
  xData: TFindStreamData;
  xNames: TWideStringList;
  procedure AddStreamName(xName: WideString);
  begin
    if xName > '' then 
      xNames.Add(xName);
  end;// procedure AddStreamName(xName: WideString);
begin
  assert(assigned(aNames), 'StringList not assigned');

  xNames := TWideStringList.Create;
  try
    xFileName := aFilename;
    if LastDelimiter(':', aFilename) > 2 then
      xFileName := copy(aFilename, 1, LastDelimiter(':', aFilename) - 1);
    if NtfsFindFirstStream(xFilename, [], xData) then try
      AddStreamName(xData.Name);
      while NtfsFindNextStream(xData) do
        AddStreamName(xData.Name);
    finally
      NtfsFindStreamClose(xData);
    end;
    aNames.Assign(xNames);
    if not(aComplete) then begin
      for i := 0 to xNames.Count - 1 do begin
        aNames[i] := StringReplace(aNames[i], ':$DATA', '', []);
        aNames[i] := StringReplace(aNames[i], ':', '', []);
      end;// for i := 0 to xNames.Count - 1 do begin
    end;// if not(aComplete) then begin

  finally
    xNames.Free;
  end;// try finally
end;

end.
