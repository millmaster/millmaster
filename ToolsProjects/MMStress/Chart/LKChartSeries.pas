unit LKChartSeries;

interface

uses
  Windows, Messages, SysUtils, Classes, TeeProcs, Graphics,
  TeEngine, Chart, ChartSeries, Controls, Spin, StdCtrls, ExtCtrls;

type
  TSeriesType = (stNone,
                 stRaw, 
                 stAVG, 
                 stMed,
                 stAVGTot
                 );// TSeriesType
                 
  TLKChartLineSeries = class;
  TLKChartLineSeriesClass = class of TLKChartLineSeries;
  
  TLKChartBaseCalcMethod = class;
  TLKChartCalcMethodClass = Class of TLKChartBaseCalcMethod;
  
  TLKChartLineSeriesList = class;
  TDeletePoint = procedure(aSender: TLKChartLineSeries; aValueIndex: integer) of
      object;

  TLKChartBaseCalcMethod = class(TPersistent)
  private
    mRawSeries: TLKChartLineSeriesList;
    mSeriesList: TLKChartLineSeriesList;
  protected
    FMethodCaption: string;
    FMethodName: string;
    FMethodType: TSeriesType;
    FSerieRef: TLKChartLineSeriesClass;
    procedure CreateControls(aParentControl: TWinControl); virtual; abstract;
    procedure DestroyControls; virtual; abstract;
    function Execute: Boolean; virtual; abstract;
  public
    constructor Create(aSeriesType: TSeriesType; aSeriesList, aRawSeries:
        TLKChartLineSeriesList); virtual;
    function AddLineSerie(aName: string; aSeriesClass: TLKChartLineSeriesClass):
        TLKChartLineSeries;
    property MethodCaption: string read FMethodCaption;
    property MethodType: TSeriesType read FMethodType;
    property SerieRef: TLKChartLineSeriesClass read FSerieRef;
  end;// TBaseCalcMethod = class(TPersistent)

  TLKChartLineSeries = class(TLKLineSeries)
  private
    FIntervalEnd: Integer;
    FIntervalStart: Integer;
    FLastPeriodCount: Integer;
    FLinkedSerie: TLKChartLineSeries;
    FOnAfterDeletePoint: TDeletePoint;
    FOnBeforeDeletePoint: TDeletePoint;
    FShadowed: Boolean;
    FShadowedColor: TColor;
    FShadowedPenStyle: TPenStyle;
    FShadowedPenWidth: Integer;
    FShowShadowedInLegend: Boolean;
    FVisible: Boolean;
    mOriginalColor: TColor;
    mOriginalPenStyle: TPenStyle;
    mOriginalPenWidth: Integer;
    mOriginalShowInLegend: Boolean;
    procedure SetIntervalEnd(const Value: Integer);
    procedure SetIntervalStart(const Value: Integer);
    procedure SetShadowed(const Value: Boolean);
    procedure SetVisible(const Value: Boolean);
  protected
    procedure DoAfterDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);
    procedure DoBeforeDeletePoint(aSender: TLKChartLineSeries; aValueIndex:
        integer);
  public
    constructor Create(AOwner: TComponent); override;
    //1 F�gt ein Punktepaar in die Serie ein.
    function AddXY(const AXValue, AYValue: Double; const ALabel: String; AColor:
        TColor; AItem: Pointer): LongInt; reintroduce; overload; override;
    //1 L�scht einen Datenpunkt
    procedure Delete(ValueIndex:integer); override;
    property IntervalEnd: Integer read FIntervalEnd write SetIntervalEnd;
    property IntervalStart: Integer read FIntervalStart write SetIntervalStart;
    property LastPeriodCount: Integer read FLastPeriodCount write FLastPeriodCount;
    property LinkedSerie: TLKChartLineSeries read FLinkedSerie write FLinkedSerie;
    property Shadowed: Boolean read FShadowed write SetShadowed;
    property ShadowedColor: TColor read FShadowedColor write FShadowedColor;
    property ShadowedPenStyle: TPenStyle read FShadowedPenStyle write
        FShadowedPenStyle;
    property ShadowedPenWidth: Integer read FShadowedPenWidth write
        FShadowedPenWidth;
    property ShowShadowedInLegend: Boolean read FShowShadowedInLegend write
        FShowShadowedInLegend;
    property Visible: Boolean read FVisible write SetVisible;
  published
    property OnAfterDeletePoint: TDeletePoint read FOnAfterDeletePoint write
        FOnAfterDeletePoint;
    property OnBeforeDeletePoint: TDeletePoint read FOnBeforeDeletePoint write
        FOnBeforeDeletePoint;
  end;// TLKChartLineSeries = class(TLKLineSeries)

  TLKChartLineSeriesList = class(TSeriesList)
  private
    FCalcMethod: TLKChartBaseCalcMethod;
    FRawSeries: TLKChartLineSeriesList;
    FSeriesType: TSeriesType;
    function GetLine(aIndex: Integer): TLKChartLineSeries;
    procedure SetSeriesType(const Value: TSeriesType);
    procedure SetShadowed(const Value: Boolean);
  protected
    procedure SetActive(const aActive: Boolean); override; 
  public
    destructor Destroy; override;
    property CalcMethod: TLKChartBaseCalcMethod read FCalcMethod;
    property Line[aIndex: Integer]: TLKChartLineSeries read GetLine; default;
    property RawSeries: TLKChartLineSeriesList read FRawSeries write FRawSeries;
    property SeriesType: TSeriesType read FSeriesType write SetSeriesType;
    property Shadowed: Boolean write SetShadowed;
  published
  end;// TLKLineSeriesList = class(TSeriesList)
  TSeriesArray= Array[TSeriesType] of TLKChartLineSeriesList;

  TLKChartAVGMethod = class(TLKChartBaseCalcMethod)
  private
    mPeriodLabel: TPanel;
    FPeriods: Integer;
    mPeriodSpin: TSpinEdit;
  protected
  public
    constructor Create(aSeriesType: TSeriesType; aSeriesList, aRawSeries:
        TLKChartLineSeriesList); override;
    function CalculateSingleValue(aRawSeries: TChartSeries; aValueIndex: integer):
        Double;
    procedure CreateControls(aParentControl: TWinControl); override;
    procedure DestroyControls; override;
    function Execute: Boolean; override;
    procedure PeriodChange(Sender: TObject);
    property Periods: Integer read FPeriods;
  end;

  TLKChartRawMethod = class(TLKChartBaseCalcMethod)
  protected
  public
    procedure CreateControls(aParentControl: TWinControl); override;
    procedure DestroyControls; override;
    function Execute: Boolean; override;
  end;

  TLKChartSeriesContainer = class(TPersistent)
  private
    FXAxisDateTime: Boolean;
    mChart: TCustomChart;
    mSeries: TSeriesArray;
    function GetSeries(Index: TSeriesType): TLKChartLineSeriesList;
    procedure SetSeries(Index: TSeriesType; const Value: TLKChartLineSeriesList);
  public
    constructor Create(aChart: TCustomChart);
    destructor Destroy; override;
    function AddLineSerie(aName: string): TLKChartLineSeries;
    function AddPoint(aIndex: integer; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string): integer; overload;
    function AddPoint(aSerie: string; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string): integer; overload;
    function AddPoint(aSerie: string; aXValue: TDateTime; aYValue: double;
        aDetailInfo: string; var aSerieIndex: integer): integer; overload;
    function Calculate(aSeriesType: TSeriesType): Boolean;
    procedure CreateControls(aSeriesType: TSeriesType; aParentControl: TWinControl);
    procedure DestroyControls(aSeriesType: TSeriesType);
    property Series[Index: TSeriesType]: TLKChartLineSeriesList read GetSeries
        write SetSeries;
    property XAxisDateTime: Boolean read FXAxisDateTime write FXAxisDateTime;
  end;// TLKChartSeriesContainer = class(TPersistent)

  TLKChartMedianMethod = class(TLKChartBaseCalcMethod)
  private
    mPeriodLabel: TPanel;
    mPeriods: Integer;
    mPeriodSpin: TSpinEdit;
  protected
  public
    constructor Create(aSeriesType: TSeriesType; aSeriesList, aRawSeries:
        TLKChartLineSeriesList); override;
    procedure CreateControls(aParentControl: TWinControl); override;
    procedure DestroyControls; override;
    function Execute: Boolean; override;
    procedure PeriodChange(Sender: TObject);
  end;

  TCalcMethodRec = record
    ClassRef      : TLKChartCalcMethodClass;
    SerieRef      : TLKChartLineSeriesClass;
    MethodCaption : string;
  end;

const
  cUsedCalcMethods = [stRaw, stAVG, stMed];
  
  cCalcMethods : Array [TSeriesType] of TCalcMethodRec = ( 
           (MethodCaption: ''),
           (ClassRef: TLKChartRawMethod; SerieRef: TLKChartLineSeries; MethodCaption: 'Rohdaten'),
           (ClassRef: TLKChartAVGMethod; SerieRef: TLKChartLineSeries; MethodCaption: 'AVG'),
           (ClassRef: TLKChartMedianMethod; SerieRef: TLKChartLineSeries; MethodCaption: 'Median'),
           (ClassRef: TLKChartRawMethod; SerieRef: TLKChartLineSeries; MethodCaption: 'AVG Tot')
           );// cCalcMethods 

implementation
uses
  series, LKGlobal;
  
(* -----------------------------------------------------------
  Gibt die Berechnungsmethode wieder frei
-------------------------------------------------------------- *)  
destructor TLKChartLineSeriesList.Destroy;
begin
  FreeAndNil(FCalcMethod);
  inherited;
end;// destructor TLKChartLineSeriesList.Destroy;

{ TLKChartLineSeriesList }
(* -----------------------------------------------------------
  �bernimmt die Casterei
-------------------------------------------------------------- *)
function TLKChartLineSeriesList.GetLine(aIndex: Integer): TLKChartLineSeries;
begin
  result := TLKChartLineSeries(Series[aIndex]);
end;// function TLKChartLineSeriesList.GetLine(aIndex: Integer): TLKChartLineSeries;

(* -----------------------------------------------------------
  Setzt alle Serien Aktiv oder Inaktiv
-------------------------------------------------------------- *)  
procedure TLKChartLineSeriesList.SetActive(const aActive: Boolean);
var
  i: Integer;
begin
  inherited SetActive(aActive);

  for i := 0 to SerieCount - 1 do 
    Series[i].Active := aActive and Line[i].Visible;
end;// procedure TSeriesList.SetActive(const Value: Integer);

(* -----------------------------------------------------------
  Setzt den Typ der Serie. Dabei wird auch die entsprechende Berechnungsmethode erzeugt
-------------------------------------------------------------- *)  
procedure TLKChartLineSeriesList.SetSeriesType(const Value: TSeriesType);
begin
  if FSeriesType <> Value then begin
    FSeriesType := Value;
    if assigned(FCalcMethod) then 
      FreeAndNil(FCalcMethod);
    FCalcMethod := cCalcMethods[FSeriesType].ClassRef.Create(FSeriesType, self, RawSeries);
  end;// if FSeriesType <> Value then begin
end;// procedure TLKChartLineSeriesList.SetSeriesType(const Value: TSeriesType);

procedure TLKChartLineSeriesList.SetShadowed(const Value: Boolean);
var
  i: Integer;
begin
  for i := 0 to SerieCount - 1 do
    Line[i].Shadowed := Value;
end;

(* -----------------------------------------------------------
  Der Konstruktor sucht sich aus dem Konstanten Array seine Informationen selbst heraus.
  Die Informationen stehen nachher als ReadOnly Properties zur Verf�gung.
-------------------------------------------------------------- *)  
constructor TLKChartBaseCalcMethod.Create(aSeriesType: TSeriesType;
    aSeriesList, aRawSeries: TLKChartLineSeriesList);
begin
  inherited Create;
  fMethodCaption := cCalcMethods[aSeriesType].MethodCaption;
  fSerieRef      := cCalcMethods[aSeriesType].SerieRef;
  mSeriesList    := aSeriesList;
  mRawSeries     := aRawSeries;
end;// constructor TLKChartBaseCalcMethod.Create;

(*-----------------------------------------------------------
  F�gt eine neue Serie zum Chart hinzu
-------------------------------------------------------------*)
function TLKChartBaseCalcMethod.AddLineSerie(aName: string; aSeriesClass:
    TLKChartLineSeriesClass): TLKChartLineSeries;
begin
  // Die neue Serie erzeugen 
  result := aSeriesClass.Create(mRawSeries[0].ParentChart);
    
  // ... und zur Internen Liste hinzuf�gen
  mSeriesList.Add(result);
  with result do begin
    Title           := aName;
    // Die Serie wird im DataItemChart dargestellt
    ParentChart     := mRawSeries[0].ParentChart;
    Pointer.Visible := false;
    Pointer.Style   := psRectangle;
    LinePen.Width   := 2;
    // Die Farbe der Serie h�ngt von der Position in der Liste ab
    SeriesColor     := cAnalyzeChartColors[mSeriesList.IndexOf(result) mod cMaxAnalyzeColor];
  
    Active          := true;
    ChartItemType   := citObject;
    ItemOwned       := true;
  end;// with result do begin
end;// function AddLineSerie: TQOLineSeries;

(* -----------------------------------------------------------
  Initialisiert die Datenstrukturen
-------------------------------------------------------------- *)  
constructor TLKChartSeriesContainer.Create(aChart: TCustomChart);
var
  i: TSeriesType;
begin
  inherited Create;
  mChart := aChart;
  
  for i := Low(TSeriesType) to High(TSeriesType) do 
    mSeries[i] := TLKChartLineSeriesList.Create;

  for i := Low(TSeriesType) to High(TSeriesType) do begin
    mSeries[i].RawSeries := mSeries[stRaw];
    mSeries[i].SeriesType := i;
  end;// for i := Low(TSeriesType) to High(TSeriesType) do begin
end;// constructor TLKChartSeriesContainer.Create;

(* -----------------------------------------------------------
  Alles wieder aufr�umen
-------------------------------------------------------------- *)  
destructor TLKChartSeriesContainer.Destroy;
var
  i: TSeriesType;
begin
  for i := Low(TSeriesType) to High(TSeriesType) do 
    FreeAndNil(mSeries[i]);
    
  inherited;
end;// destructor TLKChartSeriesContainer.Destroy;

(*-----------------------------------------------------------
  F�gt eine neue Serie zum Chart hinzu
-------------------------------------------------------------*)
function TLKChartSeriesContainer.AddLineSerie(aName: string):
    TLKChartLineSeries;
begin
  // Die neue Serie erzeugen 
  result := TLKChartLineSeries.Create(mChart);
    
  // ... und zur Internen Liste hinzuf�gen
  Series[stRaw].Add(result);
  with result do begin
    XValues.DateTime  := FXAxisDateTime;
    Title             := aName;
    // Die Serie wird im DataItemChart dargestellt
    ParentChart       := mChart;
    Pointer.Visible   := false;
    Pointer.Style     := psRectangle;
    LinePen.Width     := 2;
    // Die Farbe der Serie h�ngt von der Position in der Liste ab
    SeriesColor       := cAnalyzeChartColors[Series[stRaw].IndexOf(result) mod cMaxAnalyzeColor];

    Active            := true;
    ChartItemType     := citObject;
    ItemOwned         := true;

    Pointer.HorizSize := 3;
    Pointer.VertSize  := 3;
  end;// with result do begin
end;// function AddLineSerie: TQOLineSeries;

(* -----------------------------------------------------------
  F�gt der Root Serie einen neuen Punkt mit Zusatzinfos hinzu
-------------------------------------------------------------- *)  
function TLKChartSeriesContainer.AddPoint(aIndex: integer; aXValue: TDateTime;
    aYValue: double; aDetailInfo: string): integer;
var
  xDetailInfo: TStringList;
begin
  result := -1;
  if (aIndex >= 0) and (aIndex < Series[stRaw].SerieCount) then begin
    if aDetailInfo > '' then begin
      // Die Detailinfos zu einer Stringlist zusammensetzen
      xDetailInfo := TStringList.Create;
      try
        // ... und den Text zuweisen
        xDetailInfo.CommaText := aDetailInfo;
        // Jetzt der gew�nschten Serie den Punkt hinzuf�gen und die Detailinfos anh�ngen
        result := Series[stRaw][aIndex].AddXY(aXValue, aYValue, '', clTeeColor, xDetailInfo);
      except
        on e: Exception do
          FreeAndNil(xDetailInfo);
      end;// try except
    end else begin
      result := Series[stRaw][aIndex].AddXY(aXValue, aYValue, '', clTeeColor, nil);
    end;// if aDetailInfo > '' then begin
  end;// if (aIndex >= 0) and (aIndex < mSeriesList.SerieCount) then begin
end;// function TfrmlkChartFrame.AddPoint(aIndex: integer; aXValue: TDateTime; ...

(* -----------------------------------------------------------
  Wie oben. Die Serie kann allerdings �ber ihren Namen angesprochen werden
-------------------------------------------------------------- *)  
function TLKChartSeriesContainer.AddPoint(aSerie: string; aXValue: TDateTime;
    aYValue: double; aDetailInfo: string): integer;
var
  xIndex: integer;
begin
  result:=  AddPoint(aSerie, aXValue, aYValue, aDetailInfo, xIndex);
end;// function TLKChartSeriesContainer.AddPoint(aSerie: string; aXValue: TDateTime;

(* -----------------------------------------------------------
  Wie oben. Die Serie kann allerdings �ber ihren Namen angesprochen werden
-------------------------------------------------------------- *)  
function TLKChartSeriesContainer.AddPoint(aSerie: string; aXValue: TDateTime;
    aYValue: double; aDetailInfo: string; var aSerieIndex: integer): integer;
begin
  // Sucht die Serie anhand ihres Namens in der Liste der verf�gbaren Rohdaten Serien
  aSerieIndex := Series[stRaw].IndexOfName(aSerie);
  if aSerieIndex < 0 then
    aSerieIndex := Series[stRaw].IndexOf(AddLineSerie(aSerie));

  // F�gt den Punkt ein
  result := AddPoint(aSerieIndex, aXValue, aYValue, aDetailInfo);
end;// function TLKChartSeriesContainer.AddPoint(aSerie: string; aXValue: TDateTime;

function TLKChartSeriesContainer.Calculate(aSeriesType: TSeriesType): Boolean;
begin
  Result := Series[aSeriesType].CalcMethod.Execute;
end;

procedure TLKChartSeriesContainer.CreateControls(aSeriesType: TSeriesType;
    aParentControl: TWinControl);
begin
  Series[aSeriesType].CalcMethod.CreateControls(aParentControl);
end;

procedure TLKChartSeriesContainer.DestroyControls(aSeriesType: TSeriesType);
begin
  Series[aSeriesType].CalcMethod.DestroyControls;
end;

(* -----------------------------------------------------------
  Liefert die Liste mit den Serien die zum Typ passen
-------------------------------------------------------------- *)  
function TLKChartSeriesContainer.GetSeries(Index: TSeriesType):
    TLKChartLineSeriesList;
begin
  Result := mSeries[Index];
end;// function TLKChartSeriesContainer.GetSeries(Index: TSeriesType):

(* -----------------------------------------------------------
  Setzt eine Liste mit Serien
-------------------------------------------------------------- *)  
procedure TLKChartSeriesContainer.SetSeries(Index: TSeriesType; const Value:
    TLKChartLineSeriesList);
begin
  mSeries[Index] := Value;
end;// procedure TLKChartSeriesContainer.SetSeries(Index: TSeriesType; const Value:

(* -----------------------------------------------------------
  Der Konstruktor sucht sich aus dem Konstanten Array seine Informationen selbst heraus.
  Die Informationen stehen nachher als ReadOnly Properties zur Verf�gung.
-------------------------------------------------------------- *)  
constructor TLKChartAVGMethod.Create(aSeriesType: TSeriesType; aSeriesList,
    aRawSeries: TLKChartLineSeriesList);
begin
  inherited Create (aSeriesType, aSeriesList, aRawSeries);
  FPeriods := 15;
end;// constructor TLKChartBaseCalcMethod.Create;

(* -----------------------------------------------------------
  Berechnet einen einzelnen Mitelwert anhand der �bergebenen Serie
-------------------------------------------------------------- *)  
function TLKChartAVGMethod.CalculateSingleValue(aRawSeries: TChartSeries;
    aValueIndex: integer): Double;
var
  i: Integer;
  xCalcPointCount: Integer;
  xEndIndex: Integer;
  xStartIndex: Integer;
begin
  xStartIndex := aValueIndex - (FPeriods div 2);
  xEndIndex := xStartIndex + (FPeriods - 1);

  if xStartIndex < 0 then
    xStartIndex := 0;
  if xEndIndex >= aRawSeries.Count then
    xEndIndex := aRawSeries.Count - 1;
  xCalcPointCount := xEndIndex - xStartIndex + 1;

  result := 0;
  for i := xStartIndex to xEndIndex do 
    result := result + aRawSeries.YValue[i];  
  result := result / xCalcPointCount;
end;// function TLKChartAVGMethod.CalculateSingleValue(aRawSeries: TChartSeries; ...

(* -----------------------------------------------------------
  Erzeugt das GU f�r die Berechnungsmethode
-------------------------------------------------------------- *)  
procedure TLKChartAVGMethod.CreateControls(aParentControl: TWinControl);
begin
  if not(assigned(mPeriodLabel)) then begin
    mPeriodLabel := TPanel.Create(aParentControl);
    with mPeriodLabel do begin
      SetBounds(5, 5 , 60, 21);
      Parent     := aParentControl;
      Caption    := 'Perioden';
      BevelInner := bvNone;
      BevelOuter := bvNone;
    end;// with mPeriodLabel do begin
  end;// if not(assigned(mPeriodLabel)) then begin
  
  if not(assigned(mPeriodSpin)) then begin
    mPeriodSpin := TSpinEdit.Create(aParentControl);
    with mPeriodSpin do begin
      SetBounds(70, 5, 60, 21);
      Parent   := aParentControl;
      OnChange := PeriodChange;
      MinValue := 1;
      MaxValue := 300;
      mPeriodSpin.Value := FPeriods;
    end;// with mPeriodSpin do begin
  end;// if not(assigned(mPeriodSpin)) then begin
end;// procedure TLKChartAVGMethod.CreateControls(aParentControl: TWinControl);

(* -----------------------------------------------------------
  Gibt die Controls wieder frei
-------------------------------------------------------------- *)  
procedure TLKChartAVGMethod.DestroyControls;
begin
  FreeAndNil(mPeriodLabel);
  FreeAndNil(mPeriodSpin);
end;// procedure TLKChartAVGMethod.DestroyControls;

(* -----------------------------------------------------------
  Berechnet den Mittelwert f�r jede einzelne Rohjdaten Serie
-------------------------------------------------------------- *)  
function TLKChartAVGMethod.Execute: Boolean;
var
  xNewSerie: TLKChartLineSeries;
  xPointValue: Double;
  xValueIndex: Integer;
  xSerie: Integer;
begin
  Result := true;

  if assigned(mSeriesList) then begin
    // Die alte Berechnung verwerfen
    mSeriesList.Clear;
  
    // ... und neu erzeugen
    for xSerie := 0 to mRawSeries.SerieCount - 1 do begin
      xNewSerie := AddLineSerie(Format('%s (%s [%d Perioden])', [mRawSeries[xSerie].Title, MethodCaption, Periods]), SerieRef);
      xNewSerie.LinkedSerie := mRawSeries[xSerie];
      xNewSerie.XValues.DateTime := mRawSeries[xSerie].XValues.DateTime;
      for xValueIndex := 0 to mRawSeries[xSerie].Count - 1 do begin
        xPointValue := CalculateSingleValue(mRawSeries[xSerie], xValueIndex);
        xNewSerie.AddXY(mRawSeries[xSerie].XValue[xValueIndex], xPointValue);
      end;// for xValueIndex := 0 to mRawSeries[xSerie].Count - 1 do begin
    end;// for xSerie := 0 to mRawSeries.SerieCount - 1 do begin
  end;// if assigned(mSeriesList) then begin
end;// function TLKChartAVGMethod.Execute: Boolean;

(* -----------------------------------------------------------
  Erfasst eine �nderung der Periodenzahl
-------------------------------------------------------------- *)  
procedure TLKChartAVGMethod.PeriodChange(Sender: TObject);
begin
  FPeriods := mPeriodSpin.Value;
  // Bei einer �nderung die Serien neu berechnen
  Execute;
end;// procedure TLKChartAVGMethod.PeriodChange(Sender: TObject);

(* -----------------------------------------------------------
  F�r die Rohdaten m�ssen keine Controls erzeugt werden
-------------------------------------------------------------- *)  
procedure TLKChartRawMethod.CreateControls(aParentControl: TWinControl);
begin
  // Keine Controls zu erzeugen
end;// procedure TLKChartRawMethod.CreateControls(aParentControl: TWinControl);

(* -----------------------------------------------------------
  Gibt die Controls wieder frei
-------------------------------------------------------------- *)  
procedure TLKChartRawMethod.DestroyControls;
begin
  // TODO -cMM: TLKChartRawMethod.DestroyControls default body inserted
end;

(* -----------------------------------------------------------
  Bei den Rohdaten muss nichts gemacht werden
-------------------------------------------------------------- *)  
function TLKChartRawMethod.Execute: Boolean;
begin
  Result := true;
end;// function TLKChartRawMethod.Execute: Boolean;


{ TLKChartLineSeries }
(* -----------------------------------------------------------
  Initialisiert die Linien Serie
-------------------------------------------------------------- *)  
constructor TLKChartLineSeries.Create(AOwner: TComponent);
begin
  inherited;
  ShadowedColor        := clLtGray;
  ShadowedPenWidth     := 1;
  ShadowedPenStyle     := psDashDotDot;
  ShowShadowedInLegend := false;
  Visible              := true;
end;

//:-------------------------------------------------------------------
(*: Member:           AddXY
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (AXValue, AYValue, ALabel, AColor, AItem)
 *
 *  Kurzbeschreibung: F�gt ein Punktepaar in die Serie ein.
 *  Beschreibung:     
                      Zus�tzlich zur normalen Funktion kann ein Pointer mit �bergeben werden. Die Pointer 
                      werden in einer Liste verwaltet und k�nnen mit Items[..] abgefragt werden.
 --------------------------------------------------------------------*)
function TLKChartLineSeries.AddXY(const AXValue, AYValue: Double; const ALabel:
    String; AColor: TColor; AItem: Pointer): LongInt;
var
  xIntervalEndIstLastValue: boolean;
begin
  // Sichern, wenn das Intervalende auf dem letzten Punkt steht
  xIntervalEndIstLastValue := (IntervalEnd = (Count - 1)) or (Count = 0);

  result := inherited AddXY(AXValue, AYValue, ALabel, AColor);
  mItemList.Insert(result, AItem);

  // Das Intervalende wieder auf den letzten Punkt setzen
  if xIntervalEndIstLastValue then
    IntervalEnd := Count - 1;
end;// TBaseQOLineSeries.AddXY cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (ValueIndex)
 *
 *  Kurzbeschreibung: L�scht einen Datenpunkt
 *  Beschreibung:     
                      Gleichzeitig wird auch der Pointer aus der Liste gel�scht.
 --------------------------------------------------------------------*)
procedure TLKChartLineSeries.Delete(ValueIndex:integer);
begin
  // Wenn notwendig, dann Interval anpassen
  if ValueIndex <= FintervalStart then begin
    // Punkt liegt vor dem Interval
    dec(FintervalStart);
    if FIntervalStart < 0 then
      FIntervalStart := 0;
  end;// if ValueIndex <= FintervalStart then begin

  // Punkt liegt vor oder im Interval
  if ValueIndex <= FIntervalEnd then begin
    dec(FIntervalEnd);
    if FIntervalStart > FIntervalEnd then
      FIntervalEnd := FIntervalStart;
  end;// if ValueIndex <= FIntervalEnd then begin

  // Benachrichtigung
  DoBeforeDeletePoint(self, ValueIndex);

  // Jetzt erst den Punkt l�schen
  inherited Delete(ValueIndex);

  // Benachrichtigung
  DoAfterDeletePoint(self, ValueIndex);
end;// TBaseQOLineSeries.Delete cat:No category

(* -----------------------------------------------------------
  Feuert einen Event, nachdem ein Punkt gel�scht wurde
-------------------------------------------------------------- *)
procedure TLKChartLineSeries.DoAfterDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);
begin
  if Assigned(FOnAfterDeletePoint) then
    FOnAfterDeletePoint(aSender, aValueIndex);
end;// procedure TLKChartLineSeries.DoAfterDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);

(* -----------------------------------------------------------
  Feuert den Event, bevor ein Punkt gel�scht wird
-------------------------------------------------------------- *)
procedure TLKChartLineSeries.DoBeforeDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);
begin
  if Assigned(FOnBeforeDeletePoint) then
    FOnBeforeDeletePoint(aSender, aValueIndex);
end;// procedure TLKChartLineSeries.DoBeforeDeletePoint(aSender: TLKChartLineSeries; aValueIndex: integer);

(* -----------------------------------------------------------
  Setzt den Index des Intervall Endes
-------------------------------------------------------------- *)  
procedure TLKChartLineSeries.SetIntervalEnd(const Value: Integer);
begin
  if (Value >= 0) and (Value < Count) then begin
    FIntervalEnd := Value;
    if FIntervalEnd < FIntervalStart then
      IntervalStart := FIntervalEnd;
  end;// if (Value >= 0) and (Value < Count) then begin
end;// procedure TLKChartLineSeries.SetIntervalEnd(const Value: Integer); 

(* -----------------------------------------------------------
  Setzt den Index des Intervall Anfangs
-------------------------------------------------------------- *)  
procedure TLKChartLineSeries.SetIntervalStart(const Value: Integer);
begin
  if (Value >= 0) and (Value < Count) then begin
    FIntervalStart := Value;
    if FIntervalStart > FIntervalEnd then
      IntervalEnd := FIntervalStart;
  end;// if (Value >= 0) and (Value < Count) then begin
end;// procedure TLKChartLineSeries.SetIntervalStart(const Value: Integer);

(* -----------------------------------------------------------
  Wird aufgerufen, wenn eine Serie nur schwach zu sehen sein soll
-------------------------------------------------------------- *)  
procedure TLKChartLineSeries.SetShadowed(const Value: Boolean);
begin
  if FShadowed <> Value then begin
    FShadowed := Value;
    if FShadowed then begin
      mOriginalColor        := SeriesColor;
      mOriginalPenWidth     := LinePen.Width;
      mOriginalPenStyle     := LinePen.Style;
      mOriginalShowInLegend := ShowInLegend;
      SeriesColor           := ShadowedColor;
      LinePen.Width         := ShadowedPenWidth;
      LinePen.Style         := ShadowedPenStyle;
      ShowInLegend          := ShowShadowedInLegend;
    end else begin
      SeriesColor           := mOriginalColor;
      LinePen.Width         := mOriginalPenWidth;
      LinePen.Style         := mOriginalPenStyle;
      ShowInLegend          := mOriginalShowInLegend;
    end;// if FShadowed then begin
  end;// if FShadowed <> Value then begin
end;// procedure TLKChartLineSeries.SetShadowed(const Value: Boolean);

(* -----------------------------------------------------------
  Kann eine Serie verbergen
-------------------------------------------------------------- *)  
procedure TLKChartLineSeries.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  // Wenn die Serie angezeigt wird, dann eventuell ausblenden
  Active := FVisible;
  if assigned(LinkedSerie) then
    LinkedSerie.Visible := Active;
end;// procedure TLKChartLineSeries.SetVisible(const Value: Boolean);

(* -----------------------------------------------------------
  Der Konstruktor sucht sich aus dem Konstanten Array seine Informationen selbst heraus.
  Die Informationen stehen nachher als ReadOnly Properties zur Verf�gung.
-------------------------------------------------------------- *)  
constructor TLKChartMedianMethod.Create(aSeriesType: TSeriesType; aSeriesList,
    aRawSeries: TLKChartLineSeriesList);
begin
  inherited Create (aSeriesType, aSeriesList, aRawSeries);
  mPeriods := 15;
end;// constructor TLKChartBaseCalcMethod.Create;

(* -----------------------------------------------------------
  Erzeugt das GU f�r die Berechnungsmethode
-------------------------------------------------------------- *)  
procedure TLKChartMedianMethod.CreateControls(aParentControl: TWinControl);
begin
  if not(assigned(mPeriodLabel)) then begin
    mPeriodLabel := TPanel.Create(aParentControl);
    with mPeriodLabel do begin
      SetBounds(5, 5 , 60, 21);
      Parent     := aParentControl;
      Caption    := 'Perioden';
      BevelInner := bvNone;
      BevelOuter := bvNone;
    end;// with mPeriodLabel do begin
  end;// if not(assigned(mPeriodLabel)) then begin
  
  if not(assigned(mPeriodSpin)) then begin
    mPeriodSpin := TSpinEdit.Create(aParentControl);
    with mPeriodSpin do begin
      SetBounds(70, 5, 60, 21);
      Parent   := aParentControl;
      OnChange := PeriodChange;
      MinValue := 1;
      MaxValue := 300;
      mPeriodSpin.Value := mPeriods;
    end;// with mPeriodSpin do begin
  end;// if not(assigned(mPeriodSpin)) then begin
end;// procedure TLKChartAVGMethod.CreateControls(aParentControl: TWinControl);

(* -----------------------------------------------------------
  Gibt die Controls wieder frei
-------------------------------------------------------------- *)  
procedure TLKChartMedianMethod.DestroyControls;
begin
  FreeAndNil(mPeriodLabel);
  FreeAndNil(mPeriodSpin);
end;// procedure TLKChartAVGMethod.DestroyControls;

(* -----------------------------------------------------------
  Berechnet den Mittelwert f�r jede einzelne Rohjdaten Serie
-------------------------------------------------------------- *)  
function TLKChartMedianMethod.Execute: Boolean;
var
  xNewSerie: TLKChartLineSeries;
  i: Integer;
  xPointValue: Double;
  xCalcPointCount: Integer;
  xValueIndex: Integer;
  xEndIndex: Integer;
  xStartIndex: Integer;
  xSerie: Integer;
  
  xMedAray: Array of extended;
  xReplaced: boolean;
  xTempValue: extended;
begin
  Result := true;

  // Die alte Berechnung verwerfen
  mSeriesList.Clear;
  
  // ... und neu erzeugen
  for xSerie := 0 to mRawSeries.SerieCount - 1 do begin
    xNewSerie := AddLineSerie(Format('%s (%s [%d Perioden])', [mRawSeries[xSerie].Title, MethodCaption, mPeriods]), SerieRef);
    xNewSerie.LinkedSerie := mRawSeries[xSerie];
    xNewSerie.XValues.DateTime := mRawSeries[xSerie].XValues.DateTime;
    for xValueIndex := 0 to mRawSeries[xSerie].Count - 1 do begin
      xStartIndex := xValueIndex - (mPeriods div 2);
      xEndIndex := xStartIndex + (mPeriods - 1);

      if xStartIndex < 0 then
        xStartIndex := 0;
      if xEndIndex >= mRawSeries[xSerie].Count then
        xEndIndex := mRawSeries[xSerie].Count - 1;
      xCalcPointCount := xEndIndex - xStartIndex + 1;

      SetLength(xMedAray, xCalcPointCount);
      for i := xStartIndex to xEndIndex do
        xMedAray[i - xStartIndex] := mRawSeries[xSerie].YValue[i];

      // sortieren (Austauschverfahren)
      repeat
        xReplaced := false;
        for i := 0 to Length(xMedAray) - 2 do begin
          if xMedAray[i] > xMedAray[i + 1] then begin
            // Tauschen
            xTempValue := xMedAray[i];
            xMedAray[i] := xMedAray[i + 1];
            xMedAray[i + 1] := xTempValue;
            xReplaced := true;
          end;// if xMerList[i] > xMerList[i + 1] then begin 
        end;// for i := 0 to Length(xMerList) - 2 do begin
      until not(xReplaced);  

      xPointValue := 0;
      if (xCalcPointCount mod 2) = 0 then begin
        // gerade Anzahl
        if xCalcPointCount > 0 then
          xPointValue := (xMedAray[(xCalcPointCount div 2) - 1] + xMedAray[xCalcPointCount div 2]) / 2;  
      end else begin
        // ungerade Anzahl  
        xPointValue := xMedAray[(xCalcPointCount - 1) div 2];
      end;// if (xCalcPointCount mod 2) = 0 then begin
      xNewSerie.AddXY(mRawSeries[xSerie].XValue[xValueIndex], xPointValue);
    end;// for xValueIndex := 0 to mRawSeries[xSerie].Count - 1 do begin
  end;// for xSerie := 0 to mRawSeries.SerieCount - 1 do begin
end;// function TLKChartAVGMethod.Execute: Boolean;

(* -----------------------------------------------------------
  Erfasst eine �nderung der Periodenzahl
-------------------------------------------------------------- *)  
procedure TLKChartMedianMethod.PeriodChange(Sender: TObject);
begin
  mPeriods := mPeriodSpin.Value;
  // Bei einer �nderung die Serien neu berechnen
  Execute;
end;// procedure TLKChartAVGMethod.PeriodChange(Sender: TObject);


end.
