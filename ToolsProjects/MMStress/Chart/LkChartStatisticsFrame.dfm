object frmLKChartStatisticsFrame: TfrmLKChartStatisticsFrame
  Left = 0
  Top = 0
  Width = 387
  Height = 280
  TabOrder = 0
  TabStop = True
  object mAVGControlBar: TToolBar
    Left = 0
    Top = 0
    Width = 387
    Height = 29
    Caption = 'mAVGControlBar'
    EdgeBorders = []
    TabOrder = 0
    Visible = False
  end
  object Panel1: TPanel
    Left = 0
    Top = 29
    Width = 387
    Height = 244
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label1: TLabel
      Left = 24
      Top = 16
      Width = 67
      Height = 13
      Caption = 'Anzahl Werte:'
    end
    object laValueCount: TLabel
      Left = 136
      Top = 16
      Width = 3
      Height = 13
      Caption = '-'
    end
    object Label3: TLabel
      Left = 24
      Top = 56
      Width = 69
      Height = 13
      Caption = 'Kleinster Wert:'
    end
    object laMinValue: TLabel
      Left = 136
      Top = 56
      Width = 3
      Height = 13
      Caption = '-'
    end
    object Label5: TLabel
      Left = 0
      Top = 147
      Width = 387
      Height = 13
      Align = alBottom
      Caption = 'Vergleiche:'
    end
    object Label8: TLabel
      Left = 24
      Top = 40
      Width = 65
      Height = 13
      Caption = 'Gr�sster Wert'
    end
    object laMaxValue: TLabel
      Left = 136
      Top = 40
      Width = 3
      Height = 13
      Caption = '-'
    end
    object Label2: TLabel
      Left = 24
      Top = 82
      Width = 48
      Height = 13
      Caption = 'Mittelwert:'
    end
    object Label4: TLabel
      Left = 24
      Top = 112
      Width = 104
      Height = 13
      Caption = 'Standardabweichung:'
    end
    object Label6: TLabel
      Left = 24
      Top = 128
      Width = 38
      Height = 13
      Caption = 'Varianz:'
    end
    object laAVG: TLabel
      Left = 136
      Top = 82
      Width = 3
      Height = 13
      Caption = '-'
    end
    object laStandardDeviation: TLabel
      Left = 136
      Top = 112
      Width = 3
      Height = 13
      Caption = '-'
    end
    object laVariance: TLabel
      Left = 136
      Top = 128
      Width = 3
      Height = 13
      Caption = '-'
    end
    object mCompareGrid: TStringGrid
      Left = 0
      Top = 160
      Width = 387
      Height = 84
      Align = alBottom
      ColCount = 3
      DefaultColWidth = 80
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 4
      TabOrder = 0
      ColWidths = (
        52
        46
        80)
      RowHeights = (
        18
        18
        18
        18)
    end
  end
end
