object frmInputForm: TfrmInputForm
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Input'
  ClientHeight = 178
  ClientWidth = 446
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 137
    Width = 446
    Height = 41
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 0
    object Panel2: TPanel
      Left = 259
      Top = 0
      Width = 187
      Height = 41
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BitBtn1: TBitBtn
        Left = 0
        Top = 8
        Width = 85
        Height = 25
        TabOrder = 0
        Kind = bkOK
      end
      object BitBtn2: TBitBtn
        Left = 95
        Top = 8
        Width = 85
        Height = 25
        TabOrder = 1
        Kind = bkCancel
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 0
    Width = 446
    Height = 45
    Align = alTop
    BevelOuter = bvNone
    Color = clInfoBk
    TabOrder = 1
    DesignSize = (
      446
      45)
    object laPrompt: TLabel
      Left = 8
      Top = 8
      Width = 430
      Height = 33
      Anchors = [akLeft, akTop, akRight]
      AutoSize = False
      WordWrap = True
    end
  end
  object memoInput: TRichEdit
    Left = 0
    Top = 45
    Width = 446
    Height = 92
    Align = alClient
    ScrollBars = ssBoth
    TabOrder = 2
  end
end
