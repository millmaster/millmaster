unit ChartSeries;

interface
uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Series, TeEngine;

type
  TChartItemType = (citPointer, citObject);
  TFindMode = (fmNone, fmEqual, fmless, fmGreater);

  TFindRecord = record
    LeftIndex   : integer;
    MiddleIndex : integer;
    RightIndex  : integer;
  end;// TFindRecord = record
  TFound = (fUnknown, fYes, FNo);
  
  TLKLineSeries = class;
  (*: Klasse:        TSeriesList
      Vorg�nger:     TPersistent
      Kategorie:     No category
      Kurzbeschrieb: Liste f�r Serien f�r das Chart 
      Beschreibung:  
                     - *)
  TSeriesList = class (TPersistent)
  private
    //1 Liefert den gr�sten Y Wert aller Serien 
    function GetMaxYValue: Extended;
    //1 Liefert den gr�sten Y Wert aller Serien 
    function GetMaxXValue: Extended;
    //1 Liefert den kleinsten Y Wert aller Serien
    function GetMinYValue: Extended;
    //1 Liefert den kleinsten Y Wert aller Serien
    function GetMinXValue: Extended;
  protected
    FOnDeleteSerie: TNotifyEvent;
    mSeries: TList;
    //1 Liefert die Anzahl der Lsiten in der Serie 
    function GetSerieCount: Integer;
    //1 Bietet den Zugriff auf eine einzelne Serie 
    function GetSeries(aIndex: Integer): TChartSeries;
    procedure SetActive(const aActive: Boolean); virtual;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Chart Serie hinzu 
    function Add(aSerie: TChartSeries): Integer; virtual;
    //1 Kopiert die Lsite mit den Serien 
    procedure Assign(Source: TPersistent); override;
    //1 L�scht alle Serien 
    procedure Clear;
    //1 L�scht eine Serie 
    procedure Delete(aIndex: integer); virtual;
    function GetLKLineSerie(aName: string): TLKLineSeries;
    //1 Gibt die Position einer bestimmten Serie in der Liste zur�ck
    function IndexOf(aSerie: TChartSeries): Integer; virtual;
    //1 Gibt die Position einer bestimmten Serie in der Liste zur�ck
    function IndexOfName(aSerie: string): Integer; virtual;
    property Active: Boolean write SetActive;
    //1 Liefert den gr�sten Y Wert aller Serien
    property MaxYValue: Extended read GetMaxYValue;
    //1 Liefert den gr�sten Y Wert aller Serien
    property MaxXValue: Extended read GetMaxXValue;
    //1 Liefert den kleinsten Y Wert aller Serien
    property MinYValue: Extended read GetMinYValue;
    //1 Liefert den kleinsten Y Wert aller Serien
    property MinXValue: Extended read GetMinXValue;
    //1 Wird gefeuert, wenn eine Serie entfernt wird
    property OnDeleteSerie: TNotifyEvent read FOnDeleteSerie write FOnDeleteSerie;
    //1 Anzahl der Serien in der Liste 
    property SerieCount: Integer read GetSerieCount;
    //1 Bietet den Zugriff auf eine einzelne Serie 
    property Series[aIndex: Integer]: TChartSeries read GetSeries; default;
  end;

  (*: Klasse:        TBaseQOLineSeries
      Vorg�nger:     TmmLineSeries
      Kategorie:     No category
      Kurzbeschrieb: Ableitung der Line Serie f�r TChart 
      Beschreibung:  
                     - *)
  TLKLineSeries = class(TLineSeries)
  private
    FChartItemType: TChartItemType;
    FItemOwned: Boolean;
  protected
    mItemList: TList;
    //1 L�scht einen Pointer aus der Liste 
    procedure DeleteItem(aIndex: integer); virtual;
    //1 Zugriffsmethode f�r Items 
    function GetItems(aIndex: Integer): Pointer; virtual;
  public
    //1 Konstruktor 
    constructor Create(AOwner: TComponent); override;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt ein Punktepaar in die Serie ein. 
    function AddXY(const AXValue, AYValue: Double; const ALabel: String; AColor: TColor; AItem: Pointer): LongInt; 
      reintroduce; overload; virtual;
    function AVG: extended;
    //1 L�scht alle Datenpunkte
    procedure Clear; override;
    //1 L�scht einen Datenpunkt 
    procedure Delete(ValueIndex:integer); override;
    function FindXValue(aValue: extended): TFindRecord;
    function GetInterpolateValue(aXValue: extended): extended;
    //1 Entscheided ob eine Pointer Liste oder eine Objekt Liste parallel gef�hrt wird
    property ChartItemType: TChartItemType read FChartItemType write FChartItemType;
    //1 True, wenn die Freigabe in der Komponente erfolgen soll 
    property ItemOwned: Boolean read FItemOwned write FItemOwned;
    //1 Array mit einem Pointer f�r jeden Datenpunkt 
    property Items[aIndex: Integer]: Pointer read GetItems;
  end;

const
  // Definiert die Farbe f�r die Serien im Analyse-Chart  
  cMaxAnalyzeColor = 15;
  cAnalyzeChartColors: Array[0..cMaxAnalyzeColor - 1] of TColor = 
                       (clRed, 
                        clGreen, 
                        clBlue, 
                        clFuchsia, 
                        clNavy, 
                        clMaroon, 
                        clDkGray, 
                        clTeal, 
                        clGray, 
                        clLime, 
                        clOlive, 
                        clPurple, 
                        clSilver, 
                        clAqua, 
                        clBlack);// cAnalyzeChartColors
implementation

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TSeriesList.Create;
begin
  inherited;
  mSeries := TList.Create;
end;// TSeriesList.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TSeriesList.Destroy;
begin
  Clear;
  FreeAndNil(mSeries);
  inherited;
end;// TSeriesList.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           Add
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (aSerie)
 *
 *  Kurzbeschreibung: F�gt eine Chart Serie hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.Add(aSerie: TChartSeries): Integer;
begin
  result := -1;
  if assigned(mSeries) then
    result := mSeries.Add(aSerie);
end;// TSeriesList.Add cat:No category

//:-------------------------------------------------------------------
(*: Member:           Assign
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (Source)
 *
 *  Kurzbeschreibung: Kopiert die Lsite mit den Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TSeriesList.Assign(Source: TPersistent);
var
  xSource: TSeriesList;
  xSerie: TChartSeries;
  i: Integer;
begin
  if Source is TSeriesList then begin
    xSource := TSeriesList(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.SerieCount - 1 do begin
      xSerie := TChartSeries.Create(xSource.Series[i].Owner);
      xSerie.Assign(xSource.Series[i]);
      Add(xSerie);
    end;// for i := 0 to xSource.SerieCount - 1 do begin
  
  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TSeriesList then begin
end;// TSeriesList.Assign cat:No category

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TSeriesList.Clear;
begin
  while mSeries.Count > 0 do
    delete(0);
end;// TSeriesList.Clear cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht eine Serie
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TSeriesList.Delete(aIndex: integer);
begin
  if assigned(mSeries) then begin
    if aIndex < mSeries.Count then begin
      if assigned(FOnDeleteSerie) then
        FOnDeleteSerie(Series[aIndex]);
      Series[aIndex].Free;
      mSeries.delete(aIndex);
    end;// if aIndex < mSeries.Count then begin
  end;// if assigned(mSeries) then begin
end;// TSeriesList.Delete cat:No category

function TSeriesList.GetLKLineSerie(aName: string): TLKLineSeries;
var
  i: integer;
begin
  result := nil;
  i := 0;
  while not(assigned(result)) and (i < SerieCount) do begin
    if (Series[i] is TLKLineSeries) then begin
      if AnsiSameText(TLKLineSeries(Series[i]).Title, aName) then
        result := TLKLineSeries(Series[i]);
    end;
    inc(i);
  end;
end;

//:-------------------------------------------------------------------
(*: Member:           GetMaxYValue
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den gr�sten Y Wert aller Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetMaxYValue: Extended;
var
  i: Integer;
  xTempMaxValue: Extended;
begin
  result := 0;

//: ----------------------------------------------
  for i := 0 to SerieCount - 1 do begin
    xTempMaxValue := Series[i].MaxYValue;
    if xTempMaxValue > result then
      result := xTempMaxValue;
  end;// for i := 0 to SerieCount - 1 do begin
end;// TSeriesList.GetMaxYValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMaxYValue
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den gr�sten Y Wert aller Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetMaxXValue: Extended;
var
  i: Integer;
  xTempMaxValue: Extended;
begin
  result := 0;

//: ----------------------------------------------
  for i := 0 to SerieCount - 1 do begin
    xTempMaxValue := Series[i].MaxXValue;
    if xTempMaxValue > result then
      result := xTempMaxValue;
  end;// for i := 0 to SerieCount - 1 do begin
end;// TSeriesList.GetMaxYValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMinYValue
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den kleinsten Y Wert aller Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetMinYValue: Extended;
var
  i: Integer;
  xTempMinValue: Extended;
begin
  if SerieCount > 0 then
    result := Series[0].MinYValue
  else
    result := 0;

//: ----------------------------------------------
  for i := 0 to SerieCount - 1 do begin
    xTempMinValue := Series[i].MinYValue;
    if xTempMinValue < result then
      result := xTempMinValue;
  end;// for i := 0 to SerieCount - 1 do begin
end;// TSeriesList.GetMinYValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetMinYValue
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert den kleinsten Y Wert aller Serien
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetMinXValue: Extended;
var
  i: Integer;
  xTempMinValue: Extended;
begin
  if SerieCount > 0 then
    result := Series[0].MinYValue
  else
    result := 0;

//: ----------------------------------------------
  for i := 0 to SerieCount - 1 do begin
    xTempMinValue := Series[i].MinXValue;
    if xTempMinValue < result then
      result := xTempMinValue;
  end;// for i := 0 to SerieCount - 1 do begin
end;// TSeriesList.GetMinYValue cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSerieCount
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Liefert die Anzahl der Lsiten in der Serie
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetSerieCount: Integer;
begin
  result := 0;
  
  if assigned(mSeries) then
    result := mSeries.Count;
end;// TSeriesList.GetSerieCount cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetSeries
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Bietet den Zugriff auf eine einzelne Serie
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.GetSeries(aIndex: Integer): TChartSeries;
begin
  result := nil;
  if assigned(mSeries) then
    if mSeries.Count > aIndex then
      result := TChartSeries(mSeries.Items[aIndex]);
end;// TSeriesList.GetSeries cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (aSerie)
 *
 *  Kurzbeschreibung: Gibt die Position einer bestimmten Serie in der Liste zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.IndexOf(aSerie: TChartSeries): Integer;
var
  i: Integer;
begin
  result := -1;
  
  i := 0;
  while (i < mSeries.Count) and (result = -1) do begin
    if mSeries[i] = aSerie then
      result := i;
    inc(i);
  end;// while (i < mSeries.Count) and (result = -1) do begin
end;// TSeriesList.IndexOf cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndexOf
 *  Klasse:           TSeriesList
 *  Kategorie:        No category 
 *  Argumente:        (aSerie)
 *
 *  Kurzbeschreibung: Gibt die Position einer bestimmten Serie in der Liste zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TSeriesList.IndexOfName(aSerie: string): Integer;
var
  i: Integer;
begin
  result := -1;

  i := 0;
  while (i < mSeries.Count) and (result = -1) do begin
    if AnsiSameText(Series[i].Title, aSerie) then
      result := i;
    inc(i);
  end;// while (i < mSeries.Count) and (result = -1) do begin
end;// TSeriesList.IndexOf cat:No category

(* -----------------------------------------------------------
  Setzt alle Serien Aktiv oder Inaktiv
-------------------------------------------------------------- *)  
procedure TSeriesList.SetActive(const aActive: Boolean);
var
  i: Integer;
begin
  for i := 0 to SerieCount - 1 do
    Series[i].Active := aActive;
end;// procedure TSeriesList.SetActive(const Value: Integer);

//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (AOwner)
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TLKLineSeries.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  ItemOwned := true;
  ChartItemType := citPointer;
  mItemList := TList.Create;
  Pointer.Style :=  psRectangle;
end;// TBaseQOLineSeries.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TLKLineSeries.Destroy;
begin
  Clear;
  FreeAndNil(mItemList);
  inherited Destroy;
end;// TBaseQOLineSeries.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddXY
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (AXValue, AYValue, ALabel, AColor, AItem)
 *
 *  Kurzbeschreibung: F�gt ein Punktepaar in die Serie ein.
 *  Beschreibung:     
                      Zus�tzlich zur normalen Funktion kann ein Pointer mit �bergeben werden. Die Pointer 
                      werden in einer Liste verwaltet und k�nnen mit Items[..] abgefragt werden.
 --------------------------------------------------------------------*)
function TLKLineSeries.AddXY(const AXValue, AYValue: Double; const ALabel: String; AColor: TColor; AItem: Pointer): 
  LongInt;
begin
  result := inherited AddXY(AXValue, AYValue, ALabel, AColor);
  mItemList.Insert(result, AItem);
end;// TBaseQOLineSeries.AddXY cat:No category

(* -----------------------------------------------------------
  Berechnet den Arithmetischen Mittelwert der Serie
-------------------------------------------------------------- *)  
function TLKLineSeries.AVG: extended;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do 
    result := result + YValue[i];
  if Count > 0 then
    result := result / count;
end;// function TLKLineSeries.AVG: extended;

//:-------------------------------------------------------------------
(*: Member:           Clear
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht alle Datenpunkte
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKLineSeries.Clear;
begin
  if assigned(mItemList) then begin
    while mItemList.Count > 0 do
      DeleteItem(0);
  end;// if assigned(mItemList) then begin
  
  inherited Clear;
end;// TBaseQOLineSeries.Clear cat:No category

//:-------------------------------------------------------------------
(*: Member:           Delete
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (ValueIndex)
 *
 *  Kurzbeschreibung: L�scht einen Datenpunkt
 *  Beschreibung:     
                      Gleichzeitig wird auch der Pointer aus der Liste gel�scht.
 --------------------------------------------------------------------*)
procedure TLKLineSeries.Delete(ValueIndex:integer);
begin
  inherited Delete(ValueIndex);
  DeleteItem(ValueIndex);
end;// TBaseQOLineSeries.Delete cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteItem
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: L�scht einen Pointer aus der Liste
 *  Beschreibung:     
                      Wenn gew�nscht wird der Record oder das Objekt auch freigegeben.
 --------------------------------------------------------------------*)
procedure TLKLineSeries.DeleteItem(aIndex: integer);
begin
  if (ItemOwned) and (assigned(mItemList)) then begin
    case ChartItemType of
      citPointer: begin
        if aIndex < mItemList.Count then begin
          if assigned(mItemList[aIndex]) then
            Dispose(mItemList[aIndex]);
          mItemList.Delete(aIndex);
        end;// if aIndex < mObjectList.Count then begin
      end;// citPointer
      citObject: begin
        if aIndex < mItemList.Count then begin
          if assigned(mItemList[aIndex]) then
            TObject(mItemList[aIndex]).Free;
          mItemList.Delete(aIndex);
        end;// if aIndex < mObjectList.Count then begin
      end;// citObject
    else
      raise Exception.Create('TOverviewLineSeries.DeleteItem: CharItemType not implemented');
    end;// case ChartItemType of
  end else begin
    if (assigned(mItemList)) then
      mItemList.Delete(aIndex);
  end;// if (ItemOwned) and (assigned(mItemList)) then begin
end;// TBaseQOLineSeries.DeleteItem cat:No category

(* -----------------------------------------------------------
  gibt den Index zur�ck der am n�chsten am gesuchten Wert ist
-------------------------------------------------------------- *)  
function TLKLineSeries.FindXValue(aValue: extended): TFindRecord;
var
 xLow, xHigh, xMed: integer;
 xFound: TFound;
 
begin
  result.LeftIndex   := -1;
  result.MiddleIndex := -1;
  result.RightIndex  := -1;
  xMed := -1;

  xFound := fUnknown;
  xHigh := count - 1;
  xLow := 0;
  while (xFound = fUnknown) do begin
    if xHigh >= xLow then begin
      xMed := (xHigh + xLow) div 2;
      if XValues[xMed] = aValue then
        xFound := fYes
      else if XValues[xMed] > aValue then
        xHigh := xMed - 1
      else
        xLow := xMed + 1;
    end else begin
      xFound := fNo;
    end;// if xHigh >= xLow then begin
  end;// while (xFound = fUnknown) do begin

  if xMed >= 0 then begin
    if xFound = fYes then begin
      // Identischer Wert gefundne
      result.MiddleIndex := xMed;
    end;// if xFound = fYes then begin

    result.LeftIndex := xMed;
    if xMed > 0 then 
      result.LeftIndex := xMed - 1;

    result.RightIndex := xMed;
    if xMed < (Count - 1) then 
      result.RightIndex := xMed + 1;
  end;// if xMed >= 0 then begin
end;// function TLKLineSeries.FindXValue(xValue: extended): TFindRecord;

(* -----------------------------------------------------------
  Gibt den Interpolierten Y-Wert f�r den gegebenen X-Wert zur�ck
-------------------------------------------------------------- *)  
function TLKLineSeries.GetInterpolateValue(aXValue: extended): extended;
var
  xNearestValues: TFindRecord;
  xLeftXValue, xRightXValue: extended;
  xLeftYValue, xRightYValue: extended;
begin
  Result := 0;
  xNearestValues := FindXValue(aXValue);
  with xNearestValues do begin
    if MiddleIndex > -1 then begin
      result := YValues[MiddleIndex];
    end else begin
      if (LeftIndex > -1) and (RightIndex > -1) then begin
        xLeftXValue  := XValues[LeftIndex];
        xRightXValue := XValues[RightIndex]; 
        xLeftYValue  := YValues[LeftIndex];
        xRightYValue := YValues[RightIndex]; 
        result := xLeftYValue + ((aXValue - xLeftXValue) / (xRightXValue - xLeftXValue) * (xRightYValue - xLeftYValue));
      end;// if (LeftIndex > -1) and (RightIndex >  -1) then begin 
    end;// if MiddleIndex > -1 then begin
  end;// with xNearestValues do begin
end;

//:-------------------------------------------------------------------
(*: Member:           GetItems
 *  Klasse:           TBaseQOLineSeries
 *  Kategorie:        No category 
 *  Argumente:        (aIndex)
 *
 *  Kurzbeschreibung: Zugriffsmethode f�r Items
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLKLineSeries.GetItems(aIndex: Integer): Pointer;
begin
  result := nil;
  if (aIndex < mItemList.Count) then
    result := mItemList[aIndex];
end;// TBaseQOLineSeries.GetItems cat:No category

end.
