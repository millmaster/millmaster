unit LKDialogs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, ComCtrls;

type
  TfrmInputForm = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    Panel3: TPanel;
    laPrompt: TLabel;
    memoInput: TRichEdit;
  private
    function GetText: string;
    procedure SetPrompt(const Value: string);
    procedure SetText(const Value: string);
    procedure SetTitle(const Value: string);
    { Private-Deklarationen }
  public
{    class function LKInputQuery(const aCaption: string; const aPrompt: string; var aValue: string): Boolean;
    class function LKInputBox(const aCaption: string; const aPrompt: string; aDefault: string): string;}
    property Prompt: string write SetPrompt;
    property Text: string read GetText write SetText;
    property Title: string write SetTitle;
    { Public-Deklarationen }
  end;

function LKInputQuery(const aCaption: string; const aPrompt: string; var aValue: string): Boolean;
function LKInputBox(const aCaption: string; const aPrompt: string; aDefault: string): string;

implementation

{$R *.dfm}
function LKInputQuery(const aCaption: string; const aPrompt: string; var aValue: string): Boolean;
begin
  with TfrmInputForm.Create(nil) do try
    Prompt := aPrompt;
    Text := aValue;
    Title := aCaption; 
    ShowModal;
    aValue := Text;
    Result := (ModalResult = mrOK);
  finally
    free;
  end;// with TfrmInputForm.Create(nil) do try
end;// function LKInputQuery(const ACaption: string, const APrompt:

function LKInputBox(const aCaption: string; const aPrompt: string; aDefault: string): string;
var
  xText: string;
begin
  result := aDefault;
  xText := aDefault;
  if  LKInputQuery(aCaption, aPrompt, xText) then
    result := xText;
end;// function LKInputQuery(const ACaption: string, const APrompt:

function TfrmInputForm.GetText: string;
begin
  Result := memoInput.Text;
end;

procedure TfrmInputForm.SetPrompt(const Value: string);
begin
  laPrompt.Caption := Value;
end;

procedure TfrmInputForm.SetText(const Value: string);
begin
  memoInput.Text := Value;
end;

procedure TfrmInputForm.SetTitle(const Value: string);
begin
  Caption := Value;
end;

end.
