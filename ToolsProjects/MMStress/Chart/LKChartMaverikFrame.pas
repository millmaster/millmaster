unit LKChartMaverikFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, LKChartSeries, StdCtrls, ToolWin, ComCtrls, Buttons, Spin, ExtCtrls,
  TeEngine, uNumEdit, ActnList;

type
  TfrmLKChartMaverikFrame = class(TFrame)
    mAVGControlBar: TToolBar;
    Label1: TLabel;
    bApply: TBitBtn;
    GroupBox1: TGroupBox;
    paPercentPanel: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    edPercentValue: TLKEdit;
    spPercent: TSpinButton;
    edNrValues: TEdit;
    edNrValuesPreview: TEdit;
    rbPercent: TRadioButton;
    rbAbsolute: TRadioButton;
    Bevel1: TBevel;
    paAbsolutePanel: TPanel;
    Label6: TLabel;
    Label7: TLabel;
    edAbsoluteValue: TLKEdit;
    spAbsolute: TSpinButton;
    mActionList: TActionList;
    acApply: TAction;
    acMark: TAction;
    bMark: TSpeedButton;
    procedure edAbsoluteValueChange(Sender: TObject);
    procedure edPercentValueChange(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure spAbsoluteUpClick(Sender: TObject);
    procedure spAbsoluteDownClick(Sender: TObject);
    procedure rbAbsoluteClick(Sender: TObject);
    procedure rbPercentClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure bMarkClick(Sender: TObject);
    procedure spPercentUpClick(Sender: TObject);
    procedure spPercentDownClick(Sender: TObject);
    procedure bApplyClick(Sender: TObject);
  private
    FRawSerie: TChartSeries;
    mAVGMethod: TLKChartAVGMethod;
    mValuesToRemove: Array of Integer;
    procedure SetRawSerie(const Value: TChartSeries);
    { Private-Deklarationen }
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure CalculateValuesToRemove;
    procedure SetControlState(aControl: TWinControl; aEnable: boolean);
    procedure DrawMarkDeviation(Sender: TObject);
    procedure RefreshStatistics;
    procedure UpdateValues;
    property RawSerie: TChartSeries read FRawSerie write SetRawSerie;
    { Public-Deklarationen }
  end;

{var
  frmLKChartMaverikFrame: TfrmLKChartMaverikFrame;
}
implementation

uses
  LKGlobal, series, TypInfo, TeeProcs;

{$R *.dfm}
(* -----------------------------------------------------------
  Initialisierung der Mittelwertmethode
-------------------------------------------------------------- *)  
constructor TfrmLKChartMaverikFrame.Create(aOwner: TComponent);
begin
  inherited;
  mAVGMethod := TLKChartAVGMethod.Create(stAVG, nil, nil);
  mAVGMethod.CreateControls(mAVGControlBar);
  rbPercent.Checked := true;
end;// constructor TfrmLKChartMaverik.Create(aOwner: TComponent);

(* -----------------------------------------------------------
  Aufr�umen
-------------------------------------------------------------- *)  
destructor TfrmLKChartMaverikFrame.Destroy;
begin
  SetLength(mValuesToRemove, 0);
  mAVGMethod.DestroyControls;
  FreeAndNil(mAVGMethod);
  inherited;
end;// destructor TfrmLKChartMaverik.Destroy;

(* -----------------------------------------------------------
  Speichert alle Indizies die entfernt werden sollen
-------------------------------------------------------------- *)  
procedure TfrmLKChartMaverikFrame.CalculateValuesToRemove;
var
  xCurrentIndex: Integer;
  i: Integer;
  xAVGValue: Double;

  (* -----------------------------------------------------------
    Registriert einen Index der entfernt werden soll
  -------------------------------------------------------------- *)  
  procedure MarkIndex(aIndexToRemove: integer; var aCurrentIndex: integer);
  begin
    mValuesToRemove[aCurrentIndex] := aIndexToRemove;
    inc(aCurrentIndex);
  end;// procedure MarkIndex(aIndexToRemove: integer; var aCurrentIndex: integer);

  procedure AddIndexPercent;
  begin
    // obere Grenze
    if FRawSerie.YValue[i] > (xAVGValue * (1 + edPercentValue.AsFloat / 100)) then
      MarkIndex(i, xCurrentIndex);
    // Untere Grenze
    if FRawSerie.YValue[i] < (xAVGValue * (1 - edPercentValue.AsFloat / 100)) then
      MarkIndex(i, xCurrentIndex);
  end;
  
  procedure AddIndexAbsolute;
  begin
    // obere Grenze
    if FRawSerie.YValue[i] > (xAVGValue + edAbsoluteValue.AsFloat) then
      MarkIndex(i, xCurrentIndex);
    // Untere Grenze
    if FRawSerie.YValue[i] < (xAVGValue - edAbsoluteValue.AsFloat) then
      MarkIndex(i, xCurrentIndex);
  end;
begin
  if assigned(FRawSerie) then begin
    SetLength(mValuesToRemove, FRawSerie.Count);
    xCurrentIndex := 0;
    for i := 2 to FRawSerie.Count - 2  do begin
      xAVGValue := mAVGMethod.CalculateSingleValue(FRawSerie, i);

      if rbPercent.Checked then 
        AddIndexPercent;
    
      if rbAbsolute.Checked then 
        AddIndexAbsolute;
    
    end;// for i := 0 to FRawSerie.Count - 1 do begin
    SetLength(mValuesToRemove, xCurrentIndex);
  end;// if assigned(FRawSerie) then begin
end;// procedure TfrmLKChartMaverik.CalculateValuesToRemove;

(* -----------------------------------------------------------
  Entfernt die Ausreisser
-------------------------------------------------------------- *)  
procedure TfrmLKChartMaverikFrame.bApplyClick(Sender: TObject);
var
  i: Integer;
begin
  CalculateValuesToRemove;
  for i := Length(mValuesToRemove) - 1 downto 0 do
    FRawSerie.Delete(mValuesToRemove[i]);
  SetLength(mValuesToRemove, 0);
  edNrValuesPreview.Text := '';
  RefreshStatistics;
end;

procedure TfrmLKChartMaverikFrame.spPercentDownClick(Sender: TObject);
begin
  edPercentValue.AsFloat := edPercentValue.AsFloat - 0.1;
end;

procedure TfrmLKChartMaverikFrame.spPercentUpClick(Sender: TObject);
begin
  edPercentValue.AsFloat := edPercentValue.AsFloat + 0.1;
end;

procedure TfrmLKChartMaverikFrame.bMarkClick(Sender: TObject);
begin
  UpdateValues;
end;

procedure TfrmLKChartMaverikFrame.SetControlState(aControl: TWinControl; aEnable:
    boolean);
var
  i: Integer;
begin
  for i := 0 to aControl.ControlCount - 1 do
    aControl.Controls[i].Enabled := aEnable;
end;

procedure TfrmLKChartMaverikFrame.DrawMarkDeviation(Sender: TObject);
var
  xRect: TRect;
  i: Integer;
  xLT, xRT,
  xLB, xRB: TPoint;
  procedure GetCoordinates(aSerie: TChartSeries; aValueIndex: integer; var aT: TPoint; var aB: TPoint); overload;
  var
    xAVGValue: extended;

    procedure CalcIndexPercent;
    begin
      aT.Y := Round(aSerie.CalcYPosValue(xAVGValue * (1 + edPercentValue.AsFloat / 100)));
      aB.Y := Round(aSerie.CalcYPosValue(xAVGValue * (1 - edPercentValue.AsFloat / 100)));
    end;

    procedure CalcIndexAbsolute;
    begin
      aT.Y := Round(aSerie.CalcYPosValue(xAVGValue + edAbsoluteValue.AsFloat));
      aB.Y := Round(aSerie.CalcYPosValue(xAVGValue - edAbsoluteValue.AsFloat));
    end;
  begin
    xAVGValue := mAVGMethod.CalculateSingleValue(aSerie, aValueIndex);

    aT.X := aSerie.CalcXPos(aValueIndex);
    aB.X := aT.X;
    if rbPercent.Checked then
      CalcIndexPercent;

    if rbAbsolute.Checked then
      CalcIndexAbsolute;
  end;// procedure GetCoordinates(aValueIndex: integer; var aT: TPoint; var aB: TPoint);

  function GetY(xVal: integer; aLeft, aRight: TPoint): integer;
  begin
    // y1 = y2 * x1 / x2
    if (aRight.X - aLeft.X) = 0 then
      result := aRight.Y - aLeft.Y
    else
      result := Round((aRight.Y - aLeft.Y) * (xVal - aLeft.X) / (aRight.X - aLeft.X));
    result := result + aLeft.Y;
  end;// function GetY(xVal: integer; aLeft, aRight: TPoint): integer;

  Procedure CheckInsideChart(var aPoint: TPoint);
  begin
    if aPoint.Y > xRect.Bottom then
      aPoint.Y := xRect.Bottom;
    if aPoint.Y < xRect.Top then
      aPoint.Y := xRect.Top;
  end;
begin
  if (bMark.Down) and assigned(FRawSerie) then begin
    CalculateValuesToRemove;
    edNrValuesPreview.Text := IntToStr(RawSerie.Count - Length(mValuesToRemove));

    if (RawSerie.Count > 1) then begin
      RawSerie.ParentChart.Canvas.Pen.Color := GetGradientColor(RawSerie.SeriesColor, 80);
      RawSerie.ParentChart.Canvas.Brush.Color := GetGradientColor(RawSerie.SeriesColor, 90);
      RawSerie.ParentChart.Canvas.Pen.Mode := pmCopy;
      for i := 2 to FRawSerie.Count - 2 do begin
        GetCoordinates(RawSerie, i, xRT, xRB);
        GetCoordinates(RawSerie, i - 1, xLT, xLB);
        xRect := RawSerie.ParentChart.ChartRect;
        if (xLT.X > xRect.Left) and (xRB.x < xRect.Right) then begin
          CheckInsideChart(xLT);
          CheckInsideChart(xLB);
          CheckInsideChart(xRT);
          CheckInsideChart(xRB);
          RawSerie.ParentChart.Canvas.Polygon([xLT, xRT, xRB, xLB, xLT]);
        end;// if (xLT.X > xRect.Left) and (xRB.x < xRect.Right) then begin
      end;// for i := 2 to FRawSerie.Count - 2 do begin
    end;// if (RawSerie.Count > 1) then begin
  end;// if (bMark.Down) and assigned(FRawSerie)  then begin
end;// procedure TfrmLKChartMaverikFrame.DrawMarkDeviation(Sender: TObject);

procedure TfrmLKChartMaverikFrame.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmLKChartMaverikFrame.RefreshStatistics;
begin
  if assigned(FRawSerie) then
    edNrValues.Text := IntToStr(FRawSerie.Count);
end;// procedure TfrmLKChartMaverik.RefreshStatistics;

procedure TfrmLKChartMaverikFrame.SetRawSerie(const Value: TChartSeries);
begin
  FRawSerie := Value;
  UpdateValues;

  RefreshStatistics;
end;

procedure TfrmLKChartMaverikFrame.rbPercentClick(Sender: TObject);
begin
  SetControlState(paPercentPanel, true);
  SetControlState(paAbsolutePanel, false);
  UpdateValues;
end;

procedure TfrmLKChartMaverikFrame.rbAbsoluteClick(Sender: TObject);
begin
  SetControlState(paAbsolutePanel, true);
  SetControlState(paPercentPanel, false);
  UpdateValues;
end;

procedure TfrmLKChartMaverikFrame.spAbsoluteDownClick(Sender: TObject);
begin
  edAbsoluteValue.Value := edAbsoluteValue.Value - 0.1;
end;

procedure TfrmLKChartMaverikFrame.spAbsoluteUpClick(Sender: TObject);
begin
  edAbsoluteValue.Value := edAbsoluteValue.Value + 0.1;
end;

procedure TfrmLKChartMaverikFrame.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  acMark.Enabled := assigned(RawSerie);
  acApply.Enabled := assigned(RawSerie);
end;

procedure TfrmLKChartMaverikFrame.UpdateValues;
begin
  if RawSerie is TLineSeries then
    TLineSeries(RawSerie).Pointer.Visible := not(bMark.Down);

  if (assigned(FRawSerie)) and (assigned(FRawSerie.ParentChart)) then
    FRawSerie.ParentChart.Refresh;
end;

procedure TfrmLKChartMaverikFrame.edPercentValueChange(Sender: TObject);
begin
  UpdateValues;
end;

procedure TfrmLKChartMaverikFrame.edAbsoluteValueChange(Sender: TObject);
begin
  UpdateValues;
end;

end.
