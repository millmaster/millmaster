unit LKChartImportForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, chart,
  StdCtrls, ExtCtrls, Buttons, ActnList, lkChartFrame;

type
  TfrmLKChartImportForm = class(TForm)
    mOpenDialog: TOpenDialog;
    rgSeparator: TRadioGroup;
    edFilename: TEdit;
    Label1: TLabel;
    rgX: TRadioGroup;
    rgY: TRadioGroup;
    BitBtn1: TBitBtn;
    bImportData: TBitBtn;
    ActionList1: TActionList;
    bChooseFile: TButton;
    procedure bChooseFileClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure UpdateSettings(Sender: TObject);
    procedure ActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure bImportDataClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    FChart: TChart;
    FChartFrame: TfrmlkChartFrame;
    FFilenames: TStringList;
    { Private declarations }
  public
    procedure ImportFile;
    function IsAlpha(aLine: string): Boolean;
    function ReadFirstLine: string;
    property Chart: TChart read FChart write FChart;
    property ChartFrame: TfrmlkChartFrame read FChartFrame write FChartFrame;
    property Filenames: TStringList read FFilenames write FFilenames;
    { Public declarations }
  end;

var
  frmLKChartImportForm: TfrmLKChartImportForm;

implementation

uses
  XMLGlobal, LKGlobal;

{$R *.DFM}

var
  lSeparator: Array [0..3] of string;
{const
  cSeparator: Array [0..3] of String = (#9, '', ' ', #13);}

(* -----------------------------------------------------------
  Zeigt den Dialog an in dem der Import gestartet werden kann.
  Wenn keine Dateien angegeben worden sind (Property "Filenames"), dann
  wird ein Open Dialog angezeigt, in dem dann Dateien ausgew�hlt werden k�nnen.
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.ImportFile;
begin
  // Initialisiert den Dialog
  with mOpenDialog do begin
    if FileName = '' then
      InitialDir := Application.Exename
    else
      InitialDir := FileName;

    // Den OpenDialog nur angeben, wenn keine Dateinamen angegeben sind
    if Filenames.Text = '' then begin
      if Execute then begin
        self.Filenames.Assign(Files);
        // Dialog anzeigen
        ShowModal;
      end;// if Execute then begin
    end else begin
      // Dialog anzeigen
      ShowModal;
    end;// if Filenames.Text > '' then begin   
  end;// with mOpenDialog do begin
end;// procedure TfrmImportForm.ImportFile;

(* -----------------------------------------------------------
  Zeigt die Filenamen im Editfeld an
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.FormShow(Sender: TObject);
begin
  edFilename.Text := Filenames.CommaText;
end;// procedure TfrmLKChartImportForm.FormShow(Sender: TObject);

(* -----------------------------------------------------------
  Aktualisiert die Anzeige der verf�gbaren Spalten
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.UpdateSettings(Sender: TObject);
var
  xFirstLine: string;
  xXItemIndex,
  xYItemIndex: integer;
begin
  // Liste mit den Dateien aktualisieren
  Filenames.CommaText := edFilename.Text;
  // Es wird lediglich das erste File analysiert
  if (Filenames.Count > 0) and FileExists(Filenames[0]) then begin
    // Erste Zeile einlesen
    xFirstLine := ReadFirstLine;

    // Wenn bereits Spalten ausgew�hlt wurden, dann den Index sichern
    xXItemIndex := rgX.ItemIndex;
    xYItemIndex := rgY.ItemIndex;

    // Spalten l�schen
    rgX.Items.Clear;
    // Alle spalten der ersten Zeile anzeigen
    while xFirstLine <> '' do          
      rgX.Items.Add(CopyLeft(xFirstLine, lSeparator[rgSeparator.ItemIndex], true));
    // Die Spalten f�r die Y-Achse syncronisieren
    rgY.Items.Assign(rgX.Items);

    // Wenn vorher keine Spalten gew�hlt wurden, dann dEfault Spalte
    if xXItemIndex =  -1 then
      xXItemIndex := 0;
    // Spalte setzen
    if xXItemIndex < rgX.Items.Count then
      rgX.ItemIndex := xXItemIndex;

    // Wenn vorher keine Spalten gewählt wurden, dann dEfault Spalte
    if xYItemIndex =  -1 then
      xYItemIndex := 1;
    // Spalte setzen  
    if xYItemIndex < rgY.Items.Count then
      rgY.ItemIndex := xYItemIndex;
  end;// if (Filenames.Count > 0) and FileExists(Filenames[0]) then begin
end;// procedure TfrmImportForm.edFilenameChange(Sender: TObject);

(* -----------------------------------------------------------
  Liest die erste Zeile der ersten Datei ein
-------------------------------------------------------------- *)  
function TfrmLKChartImportForm.ReadFirstLine: string;
var     
  xDatei: system.text;
begin
  result := '';
  // Es wird lediglich das erste File analysiert
  if (Filenames.Count > 0) and FileExists(Filenames[0]) then begin
    Assignfile(xDatei, Filenames[0]);
    // Datei �ffnen
    Reset(xDatei);
    try
      if not(EOF(xDatei)) then
        readln(xDatei, result);
    finally 
      closefile(xDatei);
    end;// try finally
  end;// if (Filenames.Count > 0) and FileExists(Filenames[0]) then begin
end;// function TfrmImportForm.ReadFirstLine: string;

(* -----------------------------------------------------------
  Aktualisierungen, wenn sonst nichts passiert
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.ActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
  bImportData.Enabled :=     (Filenames.Count > 0 ) 
                         and (FileExists(Filenames[0])) 
                         and (rgX.ItemIndex >= 0) 
                         and (rgY.ItemIndex >= 0);
end;// procedure TfrmImportForm.ActionList1Update(Action: TBasicAction; ...

(* -----------------------------------------------------------
  Importiert die gew�hlten Dateien in das Chart. Die Datenspalten wurden
  zuvor mit den Radiobuttons ausgew�hlt.

  Wird eine Datumsspalte ausgew�hlt, dann wird kontrolliert ob die folgende Spalte 
  eine Zeit enth�lt. Diese beiden Spalten werden dann kombiniert.

  Ausserdem k�nnen in der ersten Zeile die Spaltennamen angegeben sein. Ist dies der Fall,
  wird erst ab der zweiten Zeile importiert.
  
  Beispieldaten
  '03.08.2004	17:56:38		51.520000000000	0.046995555691	26.500000000000	27.400000000000'
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.bImportDataClick(Sender: TObject);
var
  xAdditinalInfos: string;
  j: Integer;
  xDatei: system.text;
  xLine: string;
  xItem: string;
  xXValue: extended;
  xYValue: extended;
  xFirstXValue: extended;
  xFirstValueAssigned: boolean;
  xXDateTime: boolean;
  i: integer;

  (* -----------------------------------------------------------
    estrahiert die entsprechende Spalte
  -------------------------------------------------------------- *)  
  function GetItem(aText: string; aIndex: integer): string;
  var
    i: integer;
  begin
    result := '';
    for i := 0 to aIndex do
      result := CopyLeft(aText, lSeparator[rgSeparator.ItemIndex], true);
  end;// function GetItem(aText: string): string;

  (* -----------------------------------------------------------
    Die Zeitachse kann eventuell aus zwei Spalten zusammengesetzt sein
  -------------------------------------------------------------- *)  
  function GetXValue(aText: string): double;
  begin
    // Zuerst einmal die Spalte holen
    xItem := GetItem(xLine, rgX.ItemIndex);
    try
      // Es wird versucht in eine Zahl zu verwandeln
      result := StrToFloat(Trim(xItem));
    except
      try
        //Hat die umwandlung in eine Zahl nicht geklappt, dann wird versucht zwei Spalten zu kombinieren
        result := StrToDateTime(Trim(xItem + ' ' + GetItem(xLine, rgX.ItemIndex + 1)));
        // in Stunden Umrechnen
        result := result * 24;
      except
        // Hat auch diese Umwandlung nicht geklappt, dann nur die Datumsspalte verwenden
        result := StrToDateTime(Trim(xItem));
      end;
      // Hier ist klar, dass es sich um eine Datumsspalte handelt
      xXDateTime := true;
    end;// try except
  end;// function GetXValue(aText: string; aIndex: integer): double;
begin
  try
    Screen.Cursor := crHourglass;
    // F�r jede Datei das selbe prozedere
    for i := 0 to Filenames.Count - 1 do begin
      if FileExists(Filenames[i]) then begin
        xFirstXValue := 0;
        // Datei zum lesen �ffnen
        Assignfile(xDatei, Filenames[i]);
        Reset(xDatei);
        xFirstValueAssigned := false;
        xXDateTime := false;
        try
          // F�r jede Zeile ...
          while not(EOF(xDatei)) do begin
            // Aktuelle Zeile einlesen
            readln(xDatei, xLine);
            // Nur weiter, wenn Daten in der Zeile vorhanden sind (erste Zeile mit Header)
            if not(IsAlpha(xLine)) then begin
              if not(xFirstValueAssigned) then begin
                // Bei der ersten Datenzeile wird analysiert ob es sich um eine Datumsspalte handelt
                xFirstValueAssigned := true;
                // Holt den Wert
                xFirstXValue := GetXValue(xLine);
                // Bei Datumsspalten wird der erste Wert gesichert. Alle folgenden Werte werden relativ angezeigt
                if xXDateTime then
                  xXValue := 0
                else
                  xXValue := xFirstXValue;
              end else begin
                // Ab der zweiten Zeile ist klar wie das zu laufen hat (Zeitwert relativ zum ersten Wert bei Datumsspalten)
                if xXDateTime then
                  xXValue :=  GetXValue(xLine) - xFirstXValue
                else
                  xXValue := StrToFloat(Trim(GetItem(xLine, rgX.ItemIndex)));
              end;// if not(xFirstValueAssigned) then begin
              // Eigentlicher Wert
              xYValue := StrToFloat(Trim(GetItem(xLine, rgY.ItemIndex)));

              (* Extrahiert aus den anderen Spalten die Zusatzinfos
                 Das Format wird in der Form Name=Wert angegeben. Damit k�nnen die Zusatzinfos
                 leicht in einer TStringList  abgelegt werden. *)
              xAdditinalInfos := '';
              for j := 0 to rgY.Items.Count - 1 do begin
                if (j <> rgY.ItemIndex) and (j <> rgX.ItemIndex) then
                  xAdditinalInfos := Format('%s,%s=%s', [xAdditinalInfos, rgY.Items[j], Trim(GetItem(xLine, j))]);
              end;// for j := 0 to rgY.Items.Count - 1 do begin
              // F�hrendes Komma entfernen              
              if xAdditinalInfos > '' then
                system.Delete(xAdditinalInfos, 1, 1);
              // Wert zum Chart hinzuf�gen
              ChartFrame.AddPoint(ChangeFileExt(ExtractFileName(FileNames[i]), ''), xXValue, xYValue, xAdditinalInfos);
            end;// if not(IsAlpha(xLine)) then begin
          end;// while not(EOF(xDatei)) do begin
        finally 
          // Datei schliessen, wenn alle ZEilen eingelesen wurden
          closefile(xDatei);
        end;// try finally
      end;//  if FileExists(Filename) then begin
    end;// for i := 0 to Filenames.Count - 1 do begin
  finally
    Screen.Cursor := crDefault;
  end;// try finally
  // Dialog schliessen
  ModalResult := mrOK;
end;// procedure TfrmImportForm.bImportDataClick(Sender: TObject);

(* -----------------------------------------------------------
  Aufr�umen
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.FormDestroy(Sender: TObject);
begin
  FreeAndNil(FFileNames);
end;// procedure TfrmLKChartImportForm.FormDestroy(Sender: TObject);

(* -----------------------------------------------------------
  Initialisierungen
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.FormCreate(Sender: TObject);
begin
  FFileNames := TStringList.Create;
  lSeparator[0] := #9;
  lSeparator[1] := ListSeparator;
  lSeparator[2] := ' ';
  lSeparator[3] := #13;
end;// procedure TfrmLKChartImportForm.FormCreate(Sender: TObject);

(* -----------------------------------------------------------
  Pr�ft ob in der entsprechendnen Zeile Buschstaben vorkommen.
  Diese Information wird gebraucht um den Header von den Daten zu unterscheiden
-------------------------------------------------------------- *)  
function TfrmLKChartImportForm.IsAlpha(aLine: string): Boolean;
var
  i: Integer;
begin
  Result := false;
  i := 1;
  while (not(result)) and (i < Length(aLine)) do begin
    result := aLine[i] in ['A'..'z'];
    inc(i);
  end;// while (not(result)) and (i < Length(aLine)) do begin
end;// function TfrmLKChartImportForm.IsAlpha(aLine: string): Boolean;

(* -----------------------------------------------------------
  Erm�glicht das ausw�hlen von Files zum Importieren
-------------------------------------------------------------- *)  
procedure TfrmLKChartImportForm.bChooseFileClick(Sender: TObject);
begin
  if mOpenDialog.Execute then begin
    edFilename.Text := mOpenDialog.Files.CommaText;
    UpdateSettings(Sender);
  end;// if mOpenDialog.Execute then begin
end;// procedure TfrmLKChartImportForm.bChooseFileClick(Sender: TObject);

end.








                     