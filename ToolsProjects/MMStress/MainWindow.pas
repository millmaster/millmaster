unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseGlobal, StdCtrls, mmButton, SystemStuff, Buttons, mmBitBtn, mmEdit,
  mmLabel, VirtualTrees, mmVirtualStringTree, ExtCtrls, mmPanel, ActnList,
  mmActionList, mmTimer, mmMemo, ComCtrls, mmRichEdit, mmSplitter,
  mmStatusBar, mmCheckBox, mmPageControl, mmGroupBox, CheckLst,
  mmCheckListBox, lkChartFrame, mmLineLabel, Db, ADODB, mmADOConnection,
  Spin, mmSpinEdit, mmImage;

type
  TfrmMainWindow = class(TForm)
    mActionList: TmmActionList;
    acStartCycle: TAction;
    acStopCycle: TAction;
    mCycleTimer: TmmTimer;
    sb: TmmStatusBar;
    mStopWatchTimer: TmmTimer;
    pgMain: TmmPageControl;
    tabSimulation: TTabSheet;
    mmPanel2: TmmPanel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel6: TmmLabel;
    mTimerLabel: TmmLabel;
    s: TmmLabel;
    laStartedSince: TmmLabel;
    butReadConfiguration: TmmBitBtn;
    edMinRuntime: TmmEdit;
    edMaxRunTime: TmmEdit;
    butStart: TmmButton;
    butStop: TmmButton;
    pACQPanel: TmmPanel;
    mmPanel1: TmmPanel;
    vstJobStack: TmmVirtualStringTree;
    sbLotList: TmmStatusBar;
    TabSheet2: TTabSheet;
    mAppMemoryTimer: TmmTimer;
    tabMemoryGuard: TTabSheet;
    paChart: TmmPanel;
    mmPanel7: TmmPanel;
    memoMointoredProcesses: TmmMemo;
    mmLabel8: TmmLabel;
    mmLineLabel1: TmmLineLabel;
    mmPanel6: TmmPanel;
    mmLabel7: TmmLabel;
    mmGroupBox1: TmmGroupBox;
    cbNoAcquisition: TmmCheckBox;
    cbNoDownloadBevoreAcq: TmmCheckBox;
    lbSelectedMachines: TmmCheckListBox;
    mmPanel8: TmmPanel;
    mmLineLabel2: TmmLineLabel;
    mmPanel9: TmmPanel;
    bNewInterval: TmmButton;
    mmLabel9: TmmLabel;
    laNextInterval: TmmLabel;
    cbChangeSpdlRange: TmmCheckBox;
    mmLabel10: TmmLabel;
    laStopWatchTime: TmmLabel;
    mmADOConnection1: TmmADOConnection;
    mmPanel10: TmmPanel;
    mmLineLabel3: TmmLineLabel;
    cbMemoryGuard: TmmCheckBox;
    la1: TmmLabel;
    spedMemoryGuardInterval: TmmSpinEdit;
    la2: TmmLabel;
    pa1: TmmPanel;
    la3: TmmLineLabel;
    bStartConnecting: TmmButton;
    laLxConnectenTestEnableMsg: TmmLabel;
    paDescriptionPanel: TmmPanel;
    pa2: TmmPanel;
    imInfoImage: TmmImage;
    memoDescriptionMemo: TmmMemo;
    edCycles: TmmEdit;
    la5: TmmLabel;
    memoLXHandles: TmmMemo;
    la6: TmmLabel;
    edThreadCount: TmmEdit;
    laCycle: TmmLabel;
    pa3: TmmPanel;
    pa4: TmmPanel;
    pa5: TmmPanel;
    la4: TmmLabel;
    cbOnlyActive: TmmCheckBox;
    acShowInactiveLots: TAction;
    acHideInactiveLots: TAction;
    acOpenSuccessLog: TAction;
    acOpenErrorLog: TAction;
    b1: TmmBitBtn;
    b2: TmmBitBtn;
    paDescriptionPanel1: TmmPanel;
    pa6: TmmPanel;
    imInfoImage1: TmmImage;
    memoDescriptionMemo1: TmmMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure butReadConfigurationClick(Sender: TObject);
    procedure vstJobStackGetText(Sender: TBaseVirtualTree;
      Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
      var CellText: WideString);
    procedure acStartCycleExecute(Sender: TObject);
    procedure acStopCycleExecute(Sender: TObject);
    procedure mActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure mCycleTimerTimer(Sender: TObject);
    procedure mStopWatchTimerTimer(Sender: TObject);
    procedure vstJobStackPaintText(Sender: TBaseVirtualTree;
      const TargetCanvas: TCanvas; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType);
    procedure configureDownloadClick(Sender: TObject);
    procedure mAppMemoryTimerTimer(Sender: TObject);
    procedure memoMointoredProcessesChange(Sender: TObject);
    procedure bNewIntervalClick(Sender: TObject);
    procedure cbMemoryGuardClick(Sender: TObject);
    procedure spedMemoryGuardIntervalChange(Sender: TObject);
    procedure bStartConnectingClick(Sender: TObject);
    procedure cbOnlyActiveClick(Sender: TObject);
    procedure acShowInactiveLotsExecute(Sender: TObject);
    procedure acHideInactiveLotsExecute(Sender: TObject);
    procedure pgMainChange(Sender: TObject);
    procedure acOpenSuccessLogExecute(Sender: TObject);
    procedure acOpenErrorLogExecute(Sender: TObject);
  private
    mAvailability: TAvailabilityChecker;
    mChart: TfrmlkChartFrame;
    mDownloadHandler: TDownloadHandler;
    mErrorCount: Integer;
    mMemoryGuard: TMemoryGuard;
    mRunning: Boolean;
    mStartLotList: TStartLotList;
    mSuccessCount: Integer;
    procedure AddGroupToList(aGroup: TStartLot);
    procedure AttachFilter;
    procedure CheckChartStatus;
    procedure MarkDownloadEnabled(aEnabled: boolean);
    procedure UpdateCycle;
    procedure NewLot(aStartLot: TStartLot; aCurrentProdID: integer);
  protected
    procedure UpdateLXConnectionGUI(aSender:TObject; aCycleCount: Integer; aMsg: string);
    procedure WMCopyData(var Message: TWMCopyData); message WM_COPYDATA;
  public
    procedure JobStackListItemDeleted(Sender: TObject);
    procedure LogError(aError: string);
    procedure LogSuccess(aMsg: string; aStartLot: TStartLot; aProdID: integer);
    function MaIDAllowed(aMaID: integer): boolean;
    procedure ReadConfiguration(aReadMachines: boolean);
  end;

var
  frmMainWindow: TfrmMainWindow;

implementation

uses
  LoepfeGlobal, YMParaDef, XMLDef, SimulatorGlobal, AdoDBAccess, mmcs, JclDateTime,
  JclShell, filectrl, shellapi;

{$R *.DFM}

const
  cStart    = 0;
  cProdID   = 1;
  cGroup    = 2;
  cMachID   = 3;
  cProdName = 4;
  cVon      = 5;
  cBis      = 6;
  cYMSet    = 7;
  cYMSetID  = 8;
  cDLTry    = 9;

  cInvalidProdID = 1;
(*---------------------------------------------------------
  F�gt eine neue Gruppe in die Liste mit den zu Startenden Gruppen ein
----------------------------------------------------------*)
procedure TfrmMainWindow.AddGroupToList(aGroup: TStartLot);
begin
  mStartLotList.Add(aGroup);
  UpdateCycle;
end;// procedure TForm1.AddGroupToList(aGroup: TStartLot);

(*---------------------------------------------------------
  Erzeugt eine neue Partie auf "Vorrat" die zur bestimmten Zeit gestartet wird
----------------------------------------------------------*)
procedure TfrmMainWindow.NewLot(aStartLot: TStartLot; aCurrentProdID: integer);
var
  xIndex: Integer;
  xStartLot: TStartLot;
  xTemp: integer;
begin
  xIndex := mStartLotList.IndexOf(aStartLot);
  if xIndex >= 0 then begin
    xStartLot := mStartLotList.GetCopyFromIndex(xIndex);
    xStartLot.CurrentProdID := aCurrentProdID;

    if (cbChangeSpdlRange.Checked) and (xStartLot.RangeChangeAllowed) then begin
      if (xStartLot.SpindleFirst mod 2) = 0 then begin
        xStartLot.SpindleFirst := xStartLot.SpindleFirst - 1;
        xStartLot.SpindleLast := xStartLot.SpindleLast + 1;
      end else begin
        xStartLot.SpindleFirst := xStartLot.SpindleFirst + 1;
        xStartLot.SpindleLast := xStartLot.SpindleLast - 1;
      end;// if (xStartLot.SpindleFirst mod 2) = 0 then begin
    end;// if (cbChangeSpdlRange.Checked) and (xStartLot.RangeChangeAllowed) then begin

    // Stellt sicher, dass der Range immer g�ltig ist
    if xStartLot.SpindleFirst > xStartLot.SpindleLast then begin
      xTemp := xStartLot.SpindleFirst;
      xStartLot.SpindleFirst := xStartLot.SpindleLast;
      xStartLot.SpindleLast := xTemp;
    end;// if xStartLot.SpindleFirst > xStartLot.SpindleLast then begin

    mStartLotList.Delete(xIndex);
    AddGroupToList(xStartLot);
  end;// if xIndex >= 0 then begin
end;// procedure TForm1.NewLot(aIndex: integer; aCurrentProdID: integer);

(*---------------------------------------------------------
  Wird aufgerufen, wenn eine Best�tigung eingetroffen ist
----------------------------------------------------------*)
procedure TfrmMainWindow.WMCopyData(var Message: TWMCopyData);
var
  xIndex: Integer;
  xReceivedMsg: PResponseRec;
begin
  AttachFilter;
  xReceivedMsg := PResponseRec(Message.CopyDataStruct^.lpData);
  xIndex := mStartLotList.IndexOf(xReceivedMsg^.MachineID, xReceivedMsg^.SpindleFirst);
  if xIndex >= 0 then begin
    if xReceivedMsg^.MsgTyp = rmProdGrpStartedOk then
      mStartLotList[xIndex].DownloadSuccess := mStartLotList[xIndex].DownloadSuccess + 1;
    LogSuccess('End', mStartLotList[xIndex], xReceivedMsg^.ProdGrpID);
    inc(mSuccessCount);
    NewLot(mStartLotList[xIndex], xReceivedMsg^.ProdGrpID);
  end;// // if xIndex >= 0 then begin
end;// procedure TForm1.WMCopyData(var Message: TWMCopyData);

(*---------------------------------------------------------
  Programmstart
----------------------------------------------------------*)
procedure TfrmMainWindow.FormCreate(Sender: TObject);
var
  i: TSubSystemTyp;
begin
  mStartLotList := TStartLotList.Create;
  mStartLotList.OnDeleteItem := JobStackListItemDeleted;
  mAvailability := TAvailabilityChecker.Create;
  mDownloadHandler := TDownloadHandler.Create;
  mMemoryGuard := TMemoryGuard.Create;

  mChart := TfrmlkChartFrame.Create(self);
  with mChart do begin
    Parent := paChart;
    Align := alClient;
    Visible := true;
    ChartTitle := 'Speicherbedarf MM Applikationen';
    ShowRawSeriesShadowed := true;
    XAxisDateTime := true;
    SetLeftChartTitle('[KB]');
  end;// with mChart do begin

  with memoMointoredProcesses do begin
    for i := Low(cApplicationNames) to High(cApplicationNames) do
      Lines.Add(cApplicationNames[i]);
  end;// with memoMointoredProcesses do begin

  Randomize;
  ReadConfiguration(True);

  // Erst jetzt den Download erm�glichen
  mCycleTimer.Enabled := true;
end;// procedure TForm1.FormCreate(Sender: TObject);

(*---------------------------------------------------------
  Programmende
----------------------------------------------------------*)
procedure TfrmMainWindow.FormDestroy(Sender: TObject);
begin
  // Sicherstellen, dass nicht noch ein Event ausgel�st werden k�nnte
  mCycleTimer.Enabled := false;
  FreeAndNil(mChart);
  FreeAndNil(mMemoryGuard);
  FreeAndNil(mAvailability);
  FreeAndNil(mStartLotList);
  FreeAndNil(mDownloadHandler);
end;// procedure TForm1.FormDestroy(Sender: TObject);

(*---------------------------------------------------------
  Liest die laufenden Partien neu voin der DB und generiert Eintr�ge in die Liste
----------------------------------------------------------*)
procedure TfrmMainWindow.butReadConfigurationClick(Sender: TObject);
begin
  ReadConfiguration(False);
end;// procedure TForm1.butReadConfigurationClick(Sender: TObject);

(*---------------------------------------------------------
  Wird aufgerufen, wenn ein "Job" aus der Liste gel�scht wurde
----------------------------------------------------------*)
procedure TfrmMainWindow.JobStackListItemDeleted(Sender: TObject);
begin
  UpdateCycle;
end;// procedure TForm1.JobStackListItemDeleted(Sender: TObject);

(*---------------------------------------------------------
  Liest alle laufenden PArtien von der DB und erzeugt die "Jobs" f�r die Liste.
  Der Start des ersten Downloads erfolgt erst mit dem Button "Start"
----------------------------------------------------------*)
procedure TfrmMainWindow.ReadConfiguration(aReadMachines: boolean);
var
  i: Integer;
  xGroup: TStartLot;
  xMaName: string;
const
  cGetAllStartedGroups = ' SELECT ps.c_prod_id, p.c_prod_name, s.c_YM_set_name, s.c_xml_setting, p.c_machine_id, p.c_YM_set_id, p.c_spindle_first,' +
                         '        p.c_spindle_last, p.c_machineGroup, m.c_net_id, m.c_machine_name' +
                         ' FROM t_prodgroup_state ps, t_prodgroup p, t_xml_ym_settings s, t_machine m' +
                         ' WHERE ps.c_prod_state in (1, 3, 4, 5) ' +
                         '   AND ps.c_prod_id = p.c_prod_id' +
                         '   AND ps.c_YM_set_id = s.c_YM_set_id' +
                         '   AND ps.c_Machine_id = m.c_machine_id';
begin
  mStartLotList.Clear;

  mStartLotList.MinLotRuntime := Trunc(edMinRuntime.AsFloat * 60);
  mStartLotList.MaxLotRuntime := Trunc(edMaxRuntime.AsFloat * 60);

  if aReadMachines then
    lbSelectedMachines.Clear;

  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := cGetAllStartedGroups;
      open;
      while not(EOF) do begin
        xGroup := TStartLot.Create;
        with xGroup do begin
          CurrentProdID := FieldByName('c_prod_id').AsInteger;
          Group         := FieldByName('c_machineGroup').AsInteger;
          MachID        := FieldByName('c_machine_id').AsInteger;
          ProdName      := FieldByName('c_prod_name').AsString;
          SpindleFirst  := FieldByName('c_spindle_first').AsInteger;
          SpindleLast   := FieldByName('c_spindle_last').AsInteger;
          YMSetName     := FieldByName('c_YM_set_name').AsString;
          YMSetID       := FieldByName('c_YM_set_id').AsInteger;
          NetTyp        := TNetTyp(FieldByName('c_net_id').AsInteger);
          XMLSettings   := FieldByName('c_xml_setting').AsString;
          xMaName := Format('%s (ID: %s)', [FieldByName('c_machine_name').AsString, FieldByName('c_machine_id').AsString]);
          if lbSelectedMachines.Items.IndexOf(xMaName) = -1 then
            lbSelectedMachines.Items.AddObject(xMaName, TObject(MachID));
        end;// with xGroup do begin
        AddGroupToList(xGroup);
        next;
      end;// while not(EOF) do begin
    end;// with Query[0] do begin

    if aReadMachines then begin
      // Alle verf�gbaren Maschinen selektieren
      for i := 0 to lbSelectedMachines.Items.Count - 1 do
        lbSelectedMachines.Checked[i] := true;
    end;// if aReadMachines then begin

  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// procedure TForm1.ReadConfiguration;

(*---------------------------------------------------------
  Aktualisiert das Grid
----------------------------------------------------------*)
procedure TfrmMainWindow.UpdateCycle;
begin
  if assigned(mStartLotList) then begin
    vstJobStack.RootNodeCount := mStartLotList.Count;
    vstJobStack.Invalidate;
    sbLotList.SimpleText := Format('Anzahl Partien: %d (Aktiv: %d)', [mStartLotList.Count, mStartLotList.ActiveLotCount]);
  end;// if assigned(mStartLotList) then begin
end;// procedure TForm1.UpdateCycle;

(*---------------------------------------------------------
  F�llt das Grid mit dem entsprechenden Text
----------------------------------------------------------*)
procedure TfrmMainWindow.vstJobStackGetText(Sender: TBaseVirtualTree;
  Node: PVirtualNode; Column: TColumnIndex; TextType: TVSTTextType;
  var CellText: WideString);
var
  xStartLot: TStartLot;
begin
  if (mStartLotList.Count > 0) and (Sender.AbsoluteIndex(Node) < Cardinal(mStartLotList.Count)) then begin
    xStartLot := mStartLotList[Sender.AbsoluteIndex(Node)];
    case Column of
      cStart    : CellText := TimeToStr(xStartLot.NextDownLoad);
      cProdID   : CellText := IntToStr(xStartLot.CurrentProdID);
      cGroup    : CellText := IntToStr(xStartLot.Group);
      cMachID   : CellText := IntToStr(xStartLot.MachID);
      cProdName : CellText := xStartLot.ProdName;
      cVon      : CellText := IntToStr(xStartLot.SpindleFirst);
      cBis      : CellText := IntToStr(xStartLot.SpindleLast);
      cYMSet    : CellText := xStartLot.YMSetName;
      cYMSetID  : CellText := IntToStr(xStartLot.YMSetID);
      cDLTry    : CellText := Format('%d/%d', [xStartLot.DownloadTry, xStartLot.DownloadSuccess]);
    end;// case Column of
  end;// if (mStartLotList.Count > 0) and (Sender.AbsoluteIndex(Node) < mStartLotList.Count) then begin
end;// procedure TForm1.vstJobStackGetText(Sender: TBaseVirtualTree;...

(*---------------------------------------------------------
  Startet die Downloads
----------------------------------------------------------*)
procedure TfrmMainWindow.acStartCycleExecute(Sender: TObject);
begin
  ReadConfiguration(False);
  mRunning := true;
  mErrorCount := 0;
  mSuccessCount := 0;
  laStartedSince.Caption := 'seit: ' + DateTimeToStr(now);
end;// procedure TForm1.acStartCycleExecute(Sender: TObject);

(*---------------------------------------------------------
  Stoppt die automatischen Downloads
----------------------------------------------------------*)
procedure TfrmMainWindow.acStopCycleExecute(Sender: TObject);
begin
  mRunning := false;
  laStartedSince.Caption := '-';
end;// procedure TForm1.acStopCycleExecute(Sender: TObject);

(*---------------------------------------------------------
  GUI Verschr�nkungen
----------------------------------------------------------*)
procedure TfrmMainWindow.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);
var
  xMMRunning: boolean;
begin
  butReadConfiguration.Enabled := not(mRunning);
  edMinRuntime.Enabled := not(mRunning);
  edMaxRunTime.Enabled := not(mRunning);
  acStartCycle.Enabled := not(mRunning);
  acStopCycle.Enabled := mRunning;

  // LX Connection Test sollte nur laufen, wenn das MM gestoppt ist
  xMMRunning := mAvailability.IsSystemRunning;
  bStartConnecting.Visible := not(xMMRunning);
  edCycles.Enabled := not(xMMRunning);
  edThreadCount.Enabled := not(xMMRunning);
  memoLXHandles.Visible := not(xMMRunning);
  laLxConnectenTestEnableMsg.Visible := xMMRunning;

  cbNoDownloadBevoreAcq.Enabled := cbNoAcquisition.Checked;

  CheckChartStatus;
end;// procedure TForm1.mActionListUpdate(Action: TBasicAction; var Handled: Boolean);

(*---------------------------------------------------------
  Wird Zyklisch alle Sekunden aufgerufen, um einen Download zu starten
----------------------------------------------------------*)
procedure TfrmMainWindow.mCycleTimerTimer(Sender: TObject);
var
  xStartLot: TStartLot;
  xDownloadEnabled: boolean;
begin
  xDownloadEnabled := True;
//  xDownloadEnabled := mAvailability.IsDownloadAllowed;
  MarkDownloadEnabled(xDownloadEnabled);

  if mRunning then begin
    if not(mDownloadHandler.IsDownloading) then begin
      if (mStartLotList.Count > 0) and (mStartLotList[0].NextDownload < now) then begin
        xStartLot := mStartLotList[0];

        try
          if (xStartLot.MachineState = msRunning) then begin
            xDownloadEnabled := mAvailability.IsDownloadAllowed;
            MarkDownloadEnabled(xDownloadEnabled);
            if xDownloadEnabled then begin
              if mDownloadHandler.StartLot(xStartLot) then
                LogSuccess('begin', xStartLot, cInvalidProdID)
              else
                NewLot(xStartLot, xStartLot.CurrentProdID);
            end;// if xDownloadEnabled then begin
          end else begin
            NewLot(xStartLot, xStartLot.CurrentProdID);
          end;
        except
          on e: ESystemPortError do begin
            NewLot(xStartLot, xStartLot.CurrentProdID);
            LogError(Format('%s: Schreiben in die Pipe misslungen ', [DateTimeToStr(now)]));
          end;// on e: ESystemPortError do begin
        end;// try except

      end;// if (xDownloadEnabled) and (mStartLotList.Count > 0) and (mStartLotList[0].NextDownload < now) then begin
    end else begin
      // Nach einigen Sekunden ohne Benachrichtigung, einfach wieder auf 0 setzen
      if mDownloadHandler.TimedOut then begin
        mDownloadHandler.Reset;
        if mStartLotList.Count > 0 then begin
          // Pr�fen ob die Partie noch im psStopping Modus ist
          xStartLot := mStartLotList[0];
          LogError(Format('Timeout: %s ' + xStartLot.AsString + ' (%d)', [DateTimeToStr(now), xStartLot.CurrentProdID]));
          if mDownloadHandler.RepairLot(xStartLot) then
            LogError(Format('ProdState repariert f�r ProdID: %d ', [xStartLot.CurrentProdID]));
        end;// if mStartLotList.Count > 0 then begin

        inc(mErrorCount);
      end;// if mDownloadHandler.TimedOut then begin
    end;// if mLastStart = 0 then begin
  end;// if mRunning then begin
  sb.Panels[1].Text := Format('Erfolgreiche Downloads: %d', [mSuccessCount]);
  sb.Panels[0].Text := Format('Error Count: %d', [mErrorCount]);
end;// procedure TForm1.mCycleTimerTimer(Sender: TObject);

(*---------------------------------------------------------
  Dient zur aktualisierung der Stati der "getimten" Jobs. So werden zb. nicht
  verf�gbare Partien (weil Maschine Offline) gekenzeichnet
----------------------------------------------------------*)
procedure TfrmMainWindow.mStopWatchTimerTimer(Sender: TObject);
var
  i: Integer;
  xStart: Cardinal;
const
  cIntervalChange = ' SELECT c_interval_id, c_interval_end' +
                    ' FROM t_interval' +
                    ' WHERE c_interval_start in (SELECT MAX(c_interval_start) ' +
                    '                            FROM t_interval ' +
                    '                            WHERE c_interval_start  < getdate())';
begin
  xStart := GetTickCount;
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      if mStartLotList.Count > 0 then begin
        mStartLotList.MachineStateChanged := false;
        SQL.Text := 'SELECT c_machine_id, c_machine_state FROM t_machine';
        open;
        for i := 0 to mStartLotList.Count - 1 do begin
          Filter(Format('c_machine_id = %d', [mStartLotList[i].MachID]));
          if findFirst then begin
            mStartLotList[i].MachineState := TMachState(FieldByName('c_machine_state').AsInteger);
            if not(MaIDAllowed(mStartLotList[i].MachID)) then
              mStartLotList[i].MachineState := msOffline;
          end;// if findFirst then begin
        end;// for i := 0 to mStartLotList.Count - 1 do begin
        AttachFilter;
      end;// if mStartLotList.Count > 0 then begin
      if mStartLotList.MachineStateChanged then
        UpdateCycle;

      if (mRunning) and (mStartLotList.Count > 0) then
        mTimerLabel.Caption := IntToStr(Trunc((mStartLotList[0].NextDownload - now) * 24* 60 * 60))
      else
        mTimerLabel.Caption := '0';

      if mAvailability.IsSystemRunning then begin
        // Intervall Job
        SQL.Text := cIntervalChange;
        open;
        if not(EOF) then
          laNextInterval.Caption := TimeToStr(FieldByName('c_interval_end').AsDateTime);
      end else begin
        laNextInterval.Caption := '-';
      end;// if mAvailability.IsSystemRunning then begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
  laStopWatchTime.Value := Integer(GetTickCount - xStart);
end;// procedure TForm1.mStopWatchTimerTimer(Sender: TObject);

(*---------------------------------------------------------
  Kennzeichnet nicht startbare Partien (zb. Maschine offline)
----------------------------------------------------------*)
procedure TfrmMainWindow.vstJobStackPaintText(Sender: TBaseVirtualTree;
  const TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  TextType: TVSTTextType);
var
  xStartLot: TStartLot;
begin
  TargetCanvas.Font.Color := clGreen;
  if (mStartLotList.Count > 0) and (Sender.AbsoluteIndex(Node) < Cardinal(mStartLotList.Count)) then begin
    xStartLot := mStartLotList[Sender.AbsoluteIndex(Node)];
    if xStartLot.MachineState <> msRunning then begin
      // Wenn Maschine Offline oder nicht aktiviert f�r Download
      TargetCanvas.Font.Color := clSilver;
      if not(MaIDAllowed(xStartLot.MachID)) then
        TargetCanvas.Font.Color := clRed;
    end else begin
      // Normalfall
      if xStartLot.DownloadTry <> xStartLot.DownloadSuccess then
        TargetCanvas.Font.Style := TargetCanvas.Font.Style + [fsBold];
    end;// if xStartLot.MachineState <> msRunning then begin
  end;// if (mStartLotList.Count > 0) and (Sender.AbsoluteIndex(Node) < mStartLotList.Count) then begin
end;// procedure TForm1.vstJobStackPaintText(Sender: TBaseVirtualTree;

(*---------------------------------------------------------
  Aktualisiert den DownloadChecker
----------------------------------------------------------*)
procedure TfrmMainWindow.configureDownloadClick(Sender: TObject);
begin
  mAvailability.LockDuringACQ := cbNoAcquisition.Checked;
  mAvailability.LockBevoreACQ := cbNoDownloadBevoreAcq.Checked;
end;// procedure TForm1.configureDownloadClick(Sender: TObject);


(*---------------------------------------------------------
  Schreibt eine Fehlermeldung
----------------------------------------------------------*)
procedure TfrmMainWindow.LogError(aError: string);
begin
  with TLogFileWriter.Create(cErrorLogFileName, 10000, 8000) do try
    AppendLogFile(aError);
  finally
    free;
  end;// with TLogFileWriter.Create(cErrorLogFileName, 5000, 4000) do try
end;// procedure TForm1.LogError(aError: string);

(*---------------------------------------------------------
  Schreibt eine Erfolgsmeldung
----------------------------------------------------------*)
procedure TfrmMainWindow.LogSuccess(aMsg: string; aStartLot: TStartLot; aProdID: integer);
var
  xMsg: string;
  xProdID: string;
begin
  with TLogFileWriter.Create(cSuccessLogFileName, 10000, 8000) do try
    Title := 'Art;Zeit;MachID;MachGroup;SpdlFirst;SpdlLast;SetID;ProdID'  + cCrlf;
    xProdID := '';
    if aProdID <> cInvalidProdID then
      xProdID := intToStr(aProdID);
    with aStartLot do
      xMsg := Format('%s;%s;%d;%d;%d;%d;%d;%s',
                     [aMsg, FormatDateTime('dd/mm/yy hh:nn:ss', Now), MachID, Group, SpindleFirst, SpindleLast, YMSetID, xProdID]);
    AppendLogFile(xMsg);
  finally
    free;
  end;// with TLogFileWriter.Create(cSuccessLogFileName, 5000, 4000) do try
end;// procedure TfrmMainWindow.LogSuccess(aMsg: string);

(*---------------------------------------------------------
  Pr�ft, ob eine Maschine f�r den Download gesperrt ist
----------------------------------------------------------*)
function TfrmMainWindow.MaIDAllowed(aMaID: integer): boolean;
var
  xFound: boolean;
  i: Integer;
begin
  result := false;

  i := 0;
  xFound := false;
  while (i < lbSelectedMachines.Items.Count) and(not(xFound)) do begin
    if integer(lbSelectedMachines.Items.Objects[i]) = aMaID then begin
      result := lbSelectedMachines.Checked[i];
      xFound := true;
    end;// if integer(lbSelectedMachines.Items.Objects[i]) = aMaID then begin
    inc(i);
  end;// while (i < lbSelectedMachines.Items.Count) and(not(xFound)) do begin
end;// function TfrmMainWindow.MaIDAllowed(aMaID: integer): boolean;

(*---------------------------------------------------------
  Markiert ob aktuell ein Download m�glich ist, oder nicht
----------------------------------------------------------*)
procedure TfrmMainWindow.MarkDownloadEnabled(aEnabled: boolean);
begin
  // Signalisieren, ob ein Download m�glich ist
  if aEnabled then begin
    pACQPanel.Color := clLime;
    pACQPanel.Caption := 'Download m�glich';
    pACQPanel.Font.Color := clBlack;
  end else begin
    pACQPanel.Color := clRed;
    pACQPanel.Caption := 'Download nicht m�glich';
    pACQPanel.Font.Color := clWhite;
  end;// if aEnabled then begin
end;// procedure TfrmMainWindow.MarkDownloadEnabled(aEnabled: boolean);

(*---------------------------------------------------------
  Liest zyklisch den Speicherbedarf der registrierten Aplikationen
----------------------------------------------------------*)
procedure TfrmMainWindow.mAppMemoryTimerTimer(Sender: TObject);
var
  i: Integer;
  xAppMemory: Integer;
  xApp: TSubsystemTyp;
begin
  if cbMemoryGuard.Checked then begin
    // Laufende Prozesse lesen
    mMemoryGuard.InitProcList;
    for i := 0 to memoMointoredProcesses.Lines.Count - 1 do begin
      xAppMemory := mMemoryGuard.MemUsage(memoMointoredProcesses.Lines[i]);
      if xAppMemory > 0 then
        mChart.AddPoint(memoMointoredProcesses.Lines[i], now, xAppMemory / 1024, 'test');
    end;// for i := 0 to memoMointoredProcesses.Lines.Count - 1 do begin
  end;// if cbMemoryGuard.Checked then begin
end;// procedure TfrmMainWindow.mAppMemoryTimerTimer(Sender: TObject);

(*---------------------------------------------------------
  Bei einer �nderung der Prozessliste die neune Prozesse �bernehmen 
----------------------------------------------------------*)
procedure TfrmMainWindow.memoMointoredProcessesChange(Sender: TObject);
begin
  mMemoryGuard.AddProcess(memoMointoredProcesses.Lines, true);
end;// procedure TfrmMainWindow.memoMointoredProcessesChange(Sender: TObject);

(*---------------------------------------------------------
  Vor dem n�chsten Interval wird ein neues eingef�gt.
  Im Moment nicht m�glich eine Minute vor regul�rem Intervall Wechsel oder
  wenn der Intervall Wechsel auf einen Schichtwechsel f�llt.

  Wenn mehrere Schichtkalender definert sind, ist die Funktion deaktiviert
----------------------------------------------------------*)
procedure TfrmMainWindow.bNewIntervalClick(Sender: TObject);
begin
  with TIntervalHandler.Create do try
    SetNewInterval(now);
  finally
    free;
  end;// with TIntervalHandler.Crreate do try
end;// procedure TfrmMainWindow.bNewIntervalClick(Sender: TObject);

procedure TfrmMainWindow.cbMemoryGuardClick(Sender: TObject);
begin
  CheckChartStatus;
end;

procedure TfrmMainWindow.CheckChartStatus;
begin
  if cbMemoryGuard.Checked then begin
    if not(mChart.Visible) then begin
      tabMemoryGuard.Font.Color := clBlack;
      tabMemoryGuard.Font.Style := [];
      mChart.Visible := true;
      spedMemoryGuardInterval.Enabled := True;
      la2.Enabled := True;
    end;// if not(mChart.Visible) then begin
  end else begin
    tabMemoryGuard.Font.Color := clRed;
    tabMemoryGuard.Font.Style := [fsBold, fsItalic];
    mChart.Visible := false;
    spedMemoryGuardInterval.Enabled := false;
    la2.Enabled := false;
  end;// if cbMemoryGuard.Checked then begin;
end;

procedure TfrmMainWindow.spedMemoryGuardIntervalChange(Sender: TObject);
begin
  mAppMemoryTimer.Interval := spedMemoryGuardInterval.Value * 1000;
end;

(*---------------------------------------------------------
  LX Connection
----------------------------------------------------------*)
procedure TfrmMainWindow.bStartConnectingClick(Sender: TObject);
begin
  with TLXConnectionTester.Create do try
    Screen.Cursor := crHourGlass;
    bStartConnecting.Enabled := False;
    
    OnUpdateLXConnectionGUI := UpdateLXConnectionGUI;
    Execute(edThreadCount.AsInteger, edCycles.AsInteger);
  finally
    Screen.Cursor := crDefault;
    bStartConnecting.Enabled := True;
    laCycle.Caption := 'Zyklus: -';
    Free;
  end;// with TLXConnectionTester.Create do try
end;// procedure TfrmMainWindow.bStartConnectingClick(Sender: TObject);

(*---------------------------------------------------------
  Anzeige aktualisieren
----------------------------------------------------------*)
procedure TfrmMainWindow.UpdateLXConnectionGUI(aSender:TObject; aCycleCount: Integer; aMsg: string);
begin
  laCycle.Caption := 'Zyklus: ' + IntToStr(aCycleCount);
  memoLXHandles.Text := aMsg;
  Application.ProcessMessages;
end;// procedure TfrmMainWindow.UpdateLXConnectionGUI(aSender:TObject; aCycleCount: Integer; aMsg: string);

(*---------------------------------------------------------
  Filter ein- und ausschalten
----------------------------------------------------------*)
procedure TfrmMainWindow.cbOnlyActiveClick(Sender: TObject);
begin
  AttachFilter;
end;// procedure TfrmMainWindow.cbOnlyActiveClick(Sender: TObject);

(*---------------------------------------------------------
  Blendet alle Lots ein
----------------------------------------------------------*)
procedure TfrmMainWindow.acShowInactiveLotsExecute(Sender: TObject);
var
  xNode: PVirtualNode;
begin
  xNode := vstJobStack.GetFirst;
  while Assigned(xNode) do begin
    vstJobStack.IsVisible[xNode] := True;
    xNode := vstJobStack.GetNext(xNode);
  end;// while Assigned(xNode) do begin
  vstJobStack.Update;
end;// procedure TfrmMainWindow.acShowInactiveLotsExecute(Sender: TObject);

(*---------------------------------------------------------
  Blendet inaktive Lots aus
----------------------------------------------------------*)
procedure TfrmMainWindow.acHideInactiveLotsExecute(Sender: TObject);
var
  xNode: PVirtualNode;
begin
  xNode := vstJobStack.GetFirst;
  while Assigned(xNode) do begin
    vstJobStack.IsVisible[xNode] := (mStartLotList[vstJobStack.AbsoluteIndex(xNode)].MachineState = msRunning);
    xNode := vstJobStack.GetNext(xNode);
  end;// while Assigned(xNode) do begin
  vstJobStack.Update;
end;// procedure TfrmMainWindow.acHideInactiveLotsExecute(Sender: TObject);

(*---------------------------------------------------------
  Filtert die Anzeige der Partien
----------------------------------------------------------*)
procedure TfrmMainWindow.AttachFilter;
begin
  If cbOnlyActive.Checked then
    acHideInactiveLots.Execute
  else
    acShowInactiveLots.Execute;
end;

(*---------------------------------------------------------
  Aktualisiert den Filter wenn zur Simulatorseite gewechselt wird
----------------------------------------------------------*)
procedure TfrmMainWindow.pgMainChange(Sender: TObject);
begin
  if pgMain.ActivePage = tabSimulation then
    AttachFilter;
end;

(*---------------------------------------------------------
  �ffnet das Logfile im Editor
----------------------------------------------------------*)
procedure TfrmMainWindow.acOpenSuccessLogExecute(Sender: TObject);
begin
  try
    Screen.Cursor := crHourglass;
    if mRunning or (GetAsyncKeyState(VK_SHIFT) < 0) then begin
        // Explorer in der Exploreransicht
      shellexecute(Application.handle,'explore',PChar(ExtractFileDir(cSuccessLogFileName)), nil, nil, SW_SHOWNORMAL);
    end else begin
      if FileExists(cSuccessLogFileName) then
        ShellExec(cSuccessLogFileName);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

(*---------------------------------------------------------
  �ffnet das Error Log im Editor
----------------------------------------------------------*)
procedure TfrmMainWindow.acOpenErrorLogExecute(Sender: TObject);
begin
  try
    Screen.Cursor := crHourglass;
    if FileExists(cErrorLogFileName) then
      ShellExec(cErrorLogFileName);
  finally
    Screen.Cursor := crDefault;
  end;
end;

end.
