unit SystemStuff;

interface

uses
  windows, LoepfeGlobal, sysutils, IPCClass, mmThread, Classes, BaseGlobal, LxDeviceHandlerClass;

type
  TLXConnectionRec = record
    Address: array[0..15] of Char;
    MachId:  Integer;
  end;
  TLXMachInfoArray = array of TLXConnectionRec;

  TSystemPort = class;
  TSystemPortThread = class;

  // Wird nach einer R�ckmeldung des Systems aufgerufen
  TAfterReceive = procedure(aReceivedMsg: PResponseRec) of object;
  // Leitet die Meldung an die Applikation weiter
  TProcessMsg = procedure(aReceivedMsg: PResponseRec) of object;
  TUpdateLXConnectionGUI = procedure(Sender: TObject; aCycleCount: Integer; aMsg: string) of object;

  TStartLot = class;

  // System Error
  ESystemPortError = class(Exception);

  (*---------------------------------------------------------
    Kommunikations Thread f�r den System Port
    Diese Klasse ist eine leicht abgewandelte aus AssignComp (4.5.05)
  ----------------------------------------------------------*)
  TSystemPortThread = class(TmmThread)
  private
    // Pipe Server
    mReader: TIPCServer;
    fAfterReceive: TAfterReceive;
    fPortName: string;
    procedure SetPortName(aPortName: string);
  protected
    // Threadfunktion
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;
    // Wird aufgerufen, wenn eine Meldung vom System zur�ckkommt
    property AfterReceive: TAfterReceive read fAfterReceive write fAfterReceive;
    // Name der Pipe f�r die R�ckmeldungen
    property PortName: string read fPortName write SetPortName;
  end;// TSystemPortThread = class(TmmThread)

  (*---------------------------------------------------------
    Stellt einen "Port" zum Millmaster System zur Verf�gung.
    Es werden die Notwendigen Pipes erzeugt um mit dem System zu Kommunizieren.
    Diese Klasse ist eine leicht abgewandelte aus AssignComp (4.5.05)
  ----------------------------------------------------------*)
  TSystemPort = class(TObject)
  private
    // Zust�ndig f�r die Antwort vom Jobhandler
    mThread: TSystemPortThread;
    // Schreibt in den Jobhandler
    mWriter: TIPCClient;
    fProcessMsg: TProcessMsg;
    fComputerName: string;
    fPortName: string;
  protected
    // Wird aufgerufen, wenn vom System eine Antwort eintrift
    procedure AfterReceive(aReceivedMsg: PResponseRec);
  public
    constructor Create(aPortName: string); reintroduce; virtual;
    destructor Destroy; override;
    // Setzt einen neuen Job f�r das System auf
    procedure NewJob(aJob: PJobRec); virtual;
    // Name des Computers
    property LocalComputerName: string read fComputerName;
    // Name der Pipe f�r die R�ckantwort
    property PortName: string read fPortName;
    // Event der ausgel�st wird, wenn eine R�ckantwort aus dem system eintrifft
    property ProcessMsg: TProcessMsg read fProcessMsg write fProcessMsg;
  end;// TSystemPort = class(TObject)

  (*---------------------------------------------------------
    Liste mit den verf�gbaren Partien, die neu gestartet werden sollen
  ----------------------------------------------------------*)
  TStartLotList = class(TPersistent)
  private
    FMachineStateChanged: Boolean;
    FMaxLotRuntime: Integer;
    FMinLotRuntime: Integer;
    function GetActiveLotCount: Integer;
    function GetNextEvent: TDateTime;
  protected
    FOnDeleteItem: TNotifyEvent;
    // Liste mit den einzelnen StartLot Objekten
    mItems: TList;
    function GetCount: Integer;
    function GetItems(aIndex: Integer): TStartLot;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    // F�gt ein neues Objekt zur Liste hinzu
    function Add(aItem: TStartLot): Integer; virtual;
    // Kopiert die Liste
    procedure Assign(Source: TPersistent); override;
    // Berechnet eine neue Downloadzeit f�r das aktuelle Objekt
    procedure CalculateNextDownload(aStartLot: TStartLot);
    // L�scht die Liste
    procedure Clear;
    // L�scht einen Eintrag aus der Liste
    procedure Delete(aIndex: integer); virtual;
    // Gibt eine Kopie des Objektes an der Position aIndex
    function GetCopyFromIndex(aIndex: integer): TStartLot;
    // Liefert den Index des gew�nschten Objekts
    function IndexOf(aItem: TStartLot): Integer; overload; virtual;
    // Liefert den Index des Objekts einer bestimmten Maschine von einer bestimmten Gruppe
    function IndexOf(aMachID, aSpindleFirst: integer): Integer; overload; virtual;
    // Wird aufgerufen, wenn der Status der Maschine ge�ndert wurde (Online, Offline)
    procedure OnMachineStateChanged(Sender: TObject);
    // Liefert die Anzahl der Partien die aktiv sind (Online und nicht ausgeschlossen)
    property ActiveLotCount: Integer read GetActiveLotCount;
    // Anzahl der erfassten Partien
    property Count: Integer read GetCount;
    // Indizierter Zugriff auf die einzelnen Partien
    property Items[aIndex: Integer]: TStartLot read GetItems; default;
    // True, wenn irgend eine Maschine den Status ge�ndert hat (Online, Offline)
    property MachineStateChanged: Boolean read FMachineStateChanged write FMachineStateChanged;
    // Maximale Laufzeit in Sekunden
    property MaxLotRuntime: Integer read FMaxLotRuntime write FMaxLotRuntime;
    // Minimale Laufzeit in Sekunden
    property MinLotRuntime: Integer read FMinLotRuntime write FMinLotRuntime;
    // N�chster anstehender Download
    property NextEvent: TDateTime read GetNextEvent;
    // Wird aufgerufen, wenn eine Partie aus der Liste entfernt wird
    property OnDeleteItem: TNotifyEvent read FOnDeleteItem write FOnDeleteItem;
  end;// TStartLotList = class(TPersistent)

  (*---------------------------------------------------------
    Repr�sentiert einen "Job". Ein Job beinhaltet die Einstellungen
    f�r die Partie. Dies sind unter anderem auch wann eine Partie gestartet
    werden soll
  ----------------------------------------------------------*)
  TStartLot = class(TPersistent)
  private
    FCurrentProdID: Integer;
    FDownloadTry: Integer;
    FDownloadSuccess: Integer;
    FGroup: Integer;
    FMachID: Integer;
    FMachineState: TMachState;
    FNetTyp: TNetTyp;
    FNextDownload: TDateTime;
    FOnMachineStateChanged: TNotifyEvent;
    FProdName: string;
    FSpindleFirst: Integer;
    FSpindleLast: Integer;
    FXMLSettings: string;
    FYMSetID: Integer;
    FYMSetName: string;
    function GetAsString: string;
    procedure SetMachineState(const Value: TMachState);
    procedure SetSpindleFirst(const aValue: Integer);
    procedure SetSpindleLast(const aValue: Integer);
  protected
    // Ruft den Eventhandler auf, wenn der Status der Partie ge�ndert wurde (Online, Offline)
    procedure DoMachineStateChanged;
    // Event f�r die Stus�nderung (Online, Offline) (Protected wegen der impliziten Friends Beziehung zur Liste)
    property OnMachineStateChanged: TNotifyEvent read FOnMachineStateChanged write FOnMachineStateChanged;
  public
    // Kopiert eine Partie
    procedure Assign(Source: TPersistent); override;
    // Pr�ft ob der Spulstellenbereich ge�ndert werden darf (Texnet)
    function RangeChangeAllowed: Boolean;
    // Liefert eine Stringeschreibung des Objektes
    property AsString: string read GetAsString;
    // ProdID mit der die Partie das letzte Mal gestartet wurde
    property CurrentProdID: Integer read FCurrentProdID write FCurrentProdID;
    property DownloadTry: Integer read FDownloadTry write FDownloadTry;
    property DownloadSuccess: Integer read FDownloadSuccess write FDownloadSuccess;
    // Gruppe auf der AMschine (0-basiert)
    property Group: Integer read FGroup write FGroup;
    // Maschine zu der die Partie geh�rt
    property MachID: Integer read FMachID write FMachID;
    // Status der Maschine (Online, Offline)
    property MachineState: TMachState read FMachineState write SetMachineState;
    // Typ der MAschine (Texnet, WSC, ...)
    property NetTyp: TNetTyp read FNetTyp write FNetTyp;
    // Zeitpunkt f�r den n�chsten Download der Partie
    property NextDownload: TDateTime read FNextDownload write FNextDownload;
    // Aktueller Name der Partie
    property ProdName: string read FProdName write FProdName;
    // Erste Spindel
    property SpindleFirst: Integer read FSpindleFirst write SetSpindleFirst;
    // Letzte Spindel
    property SpindleLast: Integer read FSpindleLast write SetSpindleLast;
    // Settings der Partie
    property XMLSettings: string read FXMLSettings write FXMLSettings;
    // Aktuelles Setting
    property YMSetID: Integer read FYMSetID write FYMSetID;
    // Name des aktuellen Setting
    property YMSetName: string read FYMSetName write FYMSetName;
  end;// TStartLot = class(TPersistent)

  (*---------------------------------------------------------
    Regelt alles was mit dem Start einer Partie zusammenh�ngt.
    Sobald eine Best�tigung vom System eingetroffen ist, wird das
    Hauptformular mit einer Message (WM_COPY_DATA) benachrichtigt. Dies
    ist notwendig, da die Nacricht �ber einen Eigenen Thread empfangen wird.
  ----------------------------------------------------------*)
  TDownloadHandler = class(TObject)
  private
    FStartDownload: TDateTime;
    FStopDownload: TDateTime;
    FTimeoutTime: Integer;
    // Kapselt die Kommunikation mit dem System
    mSystemPort: TSystemPort;
    function GetIsDownloading: Boolean;
    function GetTimedOut: Boolean;
    // Wird aufgerufen, wenn eine R�ckmeldung vom System eintrifft
    procedure ReceiveMsg(aReceivedMsg: PResponseRec);
  public
    constructor Create;
    destructor Destroy; override;
    // Repariert eine Partie, wenn diese l�nger im Status stopping ist
    function RepairLot(aLot: TStartLot): Boolean;
    // Markiert die Parte als "nicht gestartet" (Start und Stop = 0)
    procedure Reset;
    // Schickt einen StartGroup Job an das System
    function StartLot(aStartLot: TStartLot): Boolean;
    // True, wenn diese Partie bereits abgeschickt wurde - bis jetzt aber keine Antwort eingetroffen ist
    property IsDownloading: Boolean read GetIsDownloading;
    // Zeitpunkt des Starts (0 wenn noch kein Start erfolgt ist)
    property StartDownload: TDateTime read FStartDownload;
    // Zeitpunkt an dem der Download abgeschlossen wurde (0 wenn nicht abgeschlossen)
    property StopDownload: TDateTime read FStopDownload;
    // True, wenn eine bestimmt Zeit keine R�ckmeldung erfolgt
    property TimedOut: Boolean read GetTimedOut;
    // Solange wird ghewartet, bis eine Timeout ausgel�st wird
    property TimeoutTime: Integer read FTimeoutTime write FTimeoutTime;
  end;// TDownloadHandler = class(TObject)

  (*---------------------------------------------------------
    Erm�glicht die Pr�fung, ob ein Download erlaubt ist.
    Die Pr�fung erfolgt anhand der gesetzten "Regeln"
  ----------------------------------------------------------*)
  TAvailabilityChecker = class(TObject)
  private
    FLockBevoreACQ: Boolean;
    FLockBevoreACQ1: Boolean;
    FLockBevoreACQTime: Integer;
    FLockDuringACQ: Boolean;
    // Pr�ft ob eine Akquirierung l�uft (Je nach Einstellung auch ob bald eine Akquirierung ansteht)
    function CheckDuringACQ: Boolean;
    function GetIsDownloadAllowed: Boolean;
  public
    constructor Create;
    // True, wenn der Jobhandler l�uft
    function IsSystemRunning: Boolean;
    // True, wenn aktuell ein Download erlaubt ist (H�ngt von den Einstellungen ab)
    property IsDownloadAllowed: Boolean read GetIsDownloadAllowed;
    // True, wenn eine bestimmte Zeit vor der Akquirirung kein Download erlaubt ist
    property LockBevoreACQ: Boolean read FLockBevoreACQ write FLockBevoreACQ;
    // W�hrend dieser Zeit vor der Qkquirierung ist kein Download erlaubt, wenn FLockBevoreACQ ) true
    property LockBevoreACQTime: Integer read FLockBevoreACQTime write FLockBevoreACQTime;
    // Wenn True, dann kein Download w�hrend eine Akquirierung l�uft
    property LockDuringACQ: Boolean read FLockDuringACQ write FLockDuringACQ;
  end;// TAvailabilityChecker = class(TObject)

  (*---------------------------------------------------------
    �berwacht den Speicherbedarf der gew�nschten Prozesse
  ----------------------------------------------------------*)
  TMemoryGuard = class(TObject)
  private
    // Liste mit den �berwachten Prozesse
    mProcs: TStringList;
  public
    constructor Create;
    destructor Destroy; override;
    // F�gt einen Prozess in die Liste ein
    procedure AddProcess(aProcName: string); overload;
    // F�gt eine Liste mit Prozessen die �berwacht werden sollen ein. (Anh�ngen oder neu)
    procedure AddProcess(aProcNameList: TStrings; aNewList: boolean); overload;
    // Aktualisiert die Liste mit den laufenden Prozessen auf dem Computer
    function InitProcList: Boolean;
    // Liefert den virtuellen Speicher der Anwendung (Private Bytes im Przess Explorer)
    function MemUsage(aProcName: string): DWORD;
  end;// TMemoryGuard = class(TObject)

  (*---------------------------------------------------------
    Erm�glicht Die Manipulation des n�chsten Intervalls
  ----------------------------------------------------------*)
  TIntervalHandler = class(TObject)
    procedure SetNewInterval(aTime: TDateTime);
  end;// TIntervalHandler = class(TObject)

  (*---------------------------------------------------------
    Stellt eine Connection zur LX her und l�st diese wieder 
  ----------------------------------------------------------*)
  TLXConnectionThread = class(TmmThread)
  private
    FEventHandle: THandle;
    FLXHandle: Integer;
    FLXHandleArrayIndex: Integer;
    mLxDevHandler: TLxDevHandler;
    class procedure DecrementThreadCount;
  protected
  public
    constructor Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer; const aIpAdresse: PChar; const aMaschID: Integer);
    destructor Destroy; override;
    procedure execute; override;
    class procedure InitializeThreadCount(aCount: integer);
    property EventHandle: THandle read FEventHandle write FEventHandle;
    property LXHandle: Integer read FLXHandle write FLXHandle;
    property LXHandleArrayIndex: Integer read FLXHandleArrayIndex write FLXHandleArrayIndex;
  end;

  TLXConnectionTester = class(TObject)
    procedure Execute(aThreads: Integer; aCycles: integer);
  private
    FOnUpdateLXConnectionGUI: TUpdateLXConnectionGUI;
    mLXHandles: Array of Cardinal;
    mThreadsFinished: Integer;
  (*---------------------------------------------------------
    IP Adressen abf�llen
  ----------------------------------------------------------*)
    procedure FillIP(var aLXMachInfos: TLXMachInfoArray);
  protected
    procedure DoUpdateLXConnectionGUI(aCycleCount: Integer; aMsg: string);
  public
    procedure LXThreadTerminate(Sender: TObject);
  published
    property OnUpdateLXConnectionGUI: TUpdateLXConnectionGUI read FOnUpdateLXConnectionGUI write FOnUpdateLXConnectionGUI;
  end;

implementation

uses
  SettingsReader, JclDateTime, SimulatorGlobal, forms, messages,
  graphics, MMUGlobal, XMLDef, AdoDBAccess, JclSysInfo, controls, dialogs, mmCS,
  XMLSettingsTools, XMLSettingsModel, xmlMaConfigClasses, MMSeries;

var
  lLXConnectionThreadCount: Integer;

(*---------------------------------------------------------
  Sortiert die Liste mit den zu startenden Partien nach dem Startzeitpunkt
----------------------------------------------------------*)
function StartLotListSort(Item1, Item2: Pointer): Integer;
var
  xItem1: TStartLot;
  xItem2: TStartLot;
begin
  xItem1 := TStartLot(Item1);
  xItem2 := TStartLot(Item2);

  if xItem1.NextDownload = xItem2.NextDownload then
    result := 0
  else if xItem1.NextDownload < xItem2.NextDownload then
    result := -1
  else
    result := 1;
end;// function StartLotListSort(Item1, Item2: Pointer): Integer;

//------------------------------------------------------------------------------
 { TSystemPortThread }
//------------------------------------------------------------------------------
constructor TSystemPortThread.Create;
begin
  inherited Create(True);
  fAfterReceive := nil;
  mReader := nil;
end;

(*---------------------------------------------------------
  Threadfunktion
----------------------------------------------------------*)
procedure TSystemPortThread.Execute;
var
  xMsg: PResponseRec;
  xMsgSize: DWord;
  xRead: DWord;
begin
  try
    xMsgSize := SizeOf(TResponseRec);
    xMsg     := AllocMem(xMsgSize);
    try
      while (not Terminated) do begin
        if not mReader.ReadDynamic(PByte(xMsg), xMsgSize, xRead) then
          raise Exception.Create('Read failed. ' + FormatErrorText(mReader.Error));

        if (Assigned(fAfterReceive)) and (not(terminated)) then
          AfterReceive(xMsg);
      end;// while (not Terminated) do begin
    finally
      FreeMem(xMsg);
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TSystemPortThread.Execute failed. ' + e.Message);
      Windows.ExitProcess(0);
    end;// on e: Exception do begin
  end;// try except
end;// procedure TSystemPortThread.Execute;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TSystemPortThread.Destroy;
var
  xByte: Byte;
begin
  Terminate;
  // Schickt dem Thread eine Meldung damit sich dieser beenden kann
  with TIPCClient.Create('.', fPortName) do try
    write(@xByte, 1);
  finally
    free;
  end;// with TIPCClient.Create('.', fPortName) do try
  WaitForSingleObject(Handle, 5000);
  // Sleep um Speicherloch zu verhindern ???
  Sleep(100);
  mReader.Free;
  inherited Destroy;
end;// destructor TSystemPortThread.Destroy;

(*---------------------------------------------------------
  Zugriffsfunktion f�r Property PortName
----------------------------------------------------------*)
procedure TSystemPortThread.SetPortName(aPortName: string);
begin
  try
    fPortName := aPortName;
    if Assigned(mReader) then
      FreeAndNil(mReader);
    mReader := TIPCServer.Create(aPortName, TMMSettingsReader.Instance.Value[SettingsReader.cUserGroup], sizeof(TResponseRec), 60000, 1);
    if not mReader.Init then
      raise Exception.Create('Init of Reader failed. Error : ' + FormatErrorText(mReader.Error));
    Resume;
  except
    on e: Exception do
      raise Exception.Create('TSystemPortThread.SetPortName failed. ' + e.Message);
  end;
end;// procedure TSystemPortThread.SetPortName(aPortName: string);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TSystemPort.Create(aPortName: string);
begin
  inherited Create;
  fComputerName := LoepfeGlobal.MMGetComputerName;

  mWriter              := TIPCClient.Create(gMMHost, cChannelNames[ttJobManager]);
  fProcessMsg          := nil;
  fPortName            := aPortName + IntToStr(GetTickCount);
  mThread              := TSystemPortThread.Create;
  mThread.AfterReceive := AfterReceive;
  mThread.PortName     := fPortName;
end;// constructor TSystemPort.Create(aPortName: string);

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TSystemPort.Destroy;
begin
  FreeAndNil(mThread);
  FreeAndNil(mWriter);
  inherited Destroy;
end;// destructor TSystemPort.Destroy;

(*---------------------------------------------------------
  Wird aufgerufen, wenn eine Meldung vom System eingetroffen ist
----------------------------------------------------------*)
procedure TSystemPort.AfterReceive(aReceivedMsg: PResponseRec);
begin
  try
    if Assigned(fProcessMsg) then
      fProcessMsg(aReceivedMsg)
    else
      raise Exception.Create('ProcessMsg method not assigned.');
  except
    on e: Exception do begin
      raise Exception.Create('TAssignSystemPort.AfterReceive failed.' + e.Message);
    end;
  end;
end;// procedure TSystemPort.AfterReceive(aReceivedMsg: PResponseRec);

(*---------------------------------------------------------
  Setzt einen neuen Job f�r das Systen auf
----------------------------------------------------------*)
procedure TSystemPort.NewJob(aJob: PJobRec);
begin
  aJob^.JobLen := GetJobDataSize(aJob);
  if not mWriter.Write(PByte(aJob), aJob^.JobLen) then
    raise ESystemPortError.Create('TBaseSystemPort.NewJob failed. ' + FormatErrorText(mWriter.Error));
end;// procedure TSystemPort.NewJob(aJob: PJobRec);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TStartLotList.Create;
begin
  inherited;
  mItems := TList.Create;
end;// constructor TStartLotList.Create;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TStartLotList.Destroy;
begin
  // Liste leeren
  Clear;
  // ... und freigeben
  FreeAndNil(mItems);
  inherited;
end;// destructor TStartLotList.Destroy;

(*---------------------------------------------------------
  F�gt ein neues Objekt zur Liste hinzu
----------------------------------------------------------*)
function TStartLotList.Add(aItem: TStartLot): Integer;
begin
  result := -1;
  if assigned(mItems) then
    result := mItems.Add(aItem);

  // Mit dieser Funktion wird die Liste dann sp�ter benachrichtigt, wenn der MAschinenstatus �ndert (Online Offline)
  if assigned(aItem) then
    aItem.OnMachineStateChanged := OnMachineStateChanged;

  // F�r den n�chsten Download vorsehen
  if result >= 0 then
    CalculateNextDownload(aItem);
  mItems.Sort(StartLotListSort);
end;// function TStartLotList.Add(aItem: TStartLot): Integer;

(*---------------------------------------------------------
  Kopiert die Liste mit allen Unterobjekten
----------------------------------------------------------*)
procedure TStartLotList.Assign(Source: TPersistent);
var
  xSource: TStartLotList;
  xItem: TStartLot;
  i: Integer;
begin
  if Source is TStartLotList then begin
    xSource := TStartLotList(Source);
    Clear;
    // Alle Eigenschaften kopieren
    for i := 0 to xSource.Count - 1 do begin
      xItem := TStartLot.Create;
      xItem.Assign(xSource.Items[i]);
      Add(xItem);
    end;// for i := 0 to xSource.Count - 1 do begin

  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TStartLotList then begin
end;// procedure TStartLotList.Assign(Source: TPersistent);

(*---------------------------------------------------------
  Berechnet den Zeitpunkt f�r den n�chsten Download
----------------------------------------------------------*)
procedure TStartLotList.CalculateNextDownload(aStartLot: TStartLot);
begin
  if assigned(aStartLot) then
    aStartLot.NextDownload := now + ((MinLotRuntime + Random(MaxLotRuntime - MinLotRuntime)) / (60 * 60 * 24));
end;// procedure TStartLotList.CalculateNextDownload(aStartLot: TStartLot);

(*---------------------------------------------------------
  L�scht die Liste
----------------------------------------------------------*)
procedure TStartLotList.Clear;
begin
  while mItems.Count > 0 do
    delete(0);
end;// procedure TStartLotList.Clear;

(*---------------------------------------------------------
  L�scht ein Element aus der Liste
----------------------------------------------------------*)
procedure TStartLotList.Delete(aIndex: integer);
begin
  if assigned(mItems) then begin
    if aIndex < mItems.Count then begin
      if assigned(FOnDeleteItem) then
        FOnDeleteItem(Items[aIndex]);
      Items[aIndex].Free;
      mItems.delete(aIndex);
    end;// if aIndex < mItems.Count then begin
  end;// if assigned(mItems) then begin
end;// procedure TStartLotList.Delete(aIndex: integer);

(*---------------------------------------------------------
  Gibt die Anzahl aktiver Partien zur�ck (Partien die im Moment "gedownloaded" werden k�nnten
----------------------------------------------------------*)
function TStartLotList.GetActiveLotCount: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Count - 1 do begin
    if Items[i].FMachineState = msRunning then
      inc(result);
  end;// for i := 0 to Count - 1 do begin
end;// function TStartLotList.GetActiveLots: LongInt;

(*---------------------------------------------------------
  Gibt eine Kopie des Objektes an der Position aIndex
----------------------------------------------------------*)
function TStartLotList.GetCopyFromIndex(aIndex: integer): TStartLot;
begin
  Result := TStartLot.Create;
  if aIndex < Count then
    result.Assign(Items[aIndex]);
end;// function TStartLotList.GetCopyFromIndex(aIndex: integer): TStartLot;

(*---------------------------------------------------------
  Gibt die Anzahl registrierter Objekte
----------------------------------------------------------*)
function TStartLotList.GetCount: Integer;
begin
  result := 0;

  if assigned(mItems) then
    result := mItems.Count;
end;// function TStartLotList.GetCount: Integer;

(*---------------------------------------------------------
  Zugriffsmethode f�r Items[]
----------------------------------------------------------*)
function TStartLotList.GetItems(aIndex: Integer): TStartLot;
begin
  result := nil;
  if assigned(mItems) then
    if mItems.Count > aIndex then
      result := TStartLot(mItems.Items[aIndex]);
end;// function TStartLotList.GetItems(aIndex: Integer): TStartLot;

(*---------------------------------------------------------
  Liefert den Zeitpunkt des n�chsten Downloads
----------------------------------------------------------*)
function TStartLotList.GetNextEvent: TDateTime;
begin
  result := now;
  if Count > 0 then
    Result := Items[0].NextDownload;
end;// function TStartLotList.GetNextEvent: TDateTime;

(*---------------------------------------------------------
  Liefert den Index des gew�nschten Objekts
----------------------------------------------------------*)
function TStartLotList.IndexOf(aItem: TStartLot): Integer;
var
  i: integer;
begin
  result := -1;

  i := 0;
  while (i < mItems.Count) and (result = -1) do begin
    if mItems[i] = aItem then
      result := i;
    inc(i);
  end;// while (i < mItems.Count) and (result = -1) do begin
end;// function TStartLotList.IndexOf(aItem: TStartLot): Integer;

(*---------------------------------------------------------
  Liefert den Index des Objekts einer bestimmten Maschine von einer bestimmten Gruppe
----------------------------------------------------------*)
function TStartLotList.IndexOf(aMachID, aSpindleFirst: integer): Integer;
var
  i: integer;
begin
  result := -1;

  i := 0;
  // Sucht nach der �bereinstimmenden Partie
  while (i < mItems.Count) and (result = -1) do begin
    if (Items[i].MachID = aMachID) and (Items[i].SpindleFirst = aSpindleFirst) then
      result := i;
    inc(i);
  end;// while (i < mItems.Count) and (result = -1) do begin
end;// function TStartLotList.IndexOf(aMachID, aSpindleFirst: integer): Integer;

(*---------------------------------------------------------
  Wird aufgerufen, wenn der Status der Maschine ge�ndert wurde (Online, Offline)
----------------------------------------------------------*)
procedure TStartLotList.OnMachineStateChanged(Sender: TObject);
begin
  MachineStateChanged := true;
end;// procedure TStartLotList.OnMachineStateChanged(Sender: TObject);

(*---------------------------------------------------------
  Kopiert eine Partie
----------------------------------------------------------*)
procedure TStartLot.Assign(Source: TPersistent);
var
  xSource: TStartLot;
begin
  if Source is TStartLot then begin
    xSource := TStartLot(Source);

    // Alle Daten kopieren
    CurrentProdID   := xSource.CurrentProdID;
    Group           := xSource.Group;
    MachID          := xSource.MachID;
    ProdName        := xSource.ProdName;
    SpindleFirst    := xSource.SpindleFirst;
    SpindleLast     := xSource.SpindleLast;
    YMSetName       := xSource.YMSetName;
    YMSetID         := xSource.YMSetID;
    NetTyp          := xSource.NetTyp;
    MachineState    := xSource.MachineState;
    XMLSettings     := xSource.XMLSettings;
    DownloadTry     := xSource.DownloadTry;
    DownloadSuccess := xSource.DownloadSuccess;

  end else begin
    // Weiterreichen
    inherited Assign(Source);
  end;// if Source is TStartLot then begin
end;// procedure TStartLot.Assign(Source: TPersistent);

(*---------------------------------------------------------
  Ruft den Eventhandler auf, wenn der Status der Partie ge�ndert wurde (Online, Offline)
----------------------------------------------------------*)
procedure TStartLot.DoMachineStateChanged;
begin
  if Assigned(FOnMachineStateChanged) then FOnMachineStateChanged(Self);
end;// procedure TStartLot.DoMachineStateChanged;

(*---------------------------------------------------------
  Liefert eine Stringeschreibung des Objektes
----------------------------------------------------------*)
function TStartLot.GetAsString: string;
begin
  Result := Format('Ma = %d, Group = %d, Von %d bis %d, SetID = %d', [MachID, Group, SpindleFirst, SpindleLast, YMSetID]);
end;// function TStartLot.GetAsString: string;

(*---------------------------------------------------------
  Pr�ft ob der Spulstellenbereich ge�ndert werden darf (Texnet)
----------------------------------------------------------*)
function TStartLot.RangeChangeAllowed: Boolean;
begin
  Result := (NetTyp = ntTXN);
(*  if MachID > 0 then begin
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := 'SELECT c_net_id FROM t_machine WHERE c_machine_id = ' + IntToStr(MachID);
        open;
        // Range�nderungen nur f�r Texnet Maschinen erlaubt
        if not(EOF) then
          result := FieldByName('c_net_id').AsInteger = ord(ntTXN);
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if MachID > 0 then begin*)
end;// function TStartLot.RangeChangeAllowed: Boolean;

(*---------------------------------------------------------
  Wenn der State �ndert, dann soll die �bergeordnete Liste benachrichtigt werden
----------------------------------------------------------*)
procedure TStartLot.SetMachineState(const Value: BaseGlobal.TMachState);
begin
  if Value <> MachineState then begin
    FMachineState := Value;
    // Benachrichtigt die Liste
    DoMachineStateChanged;
  end;// if Value <> MachineState then begin
end;// procedure TStartLot.SetMachineState(const Value: TMachState);

(*---------------------------------------------------------
  Keine Spindel Zulassen die kleiner als 1 ist
----------------------------------------------------------*)
procedure TStartLot.SetSpindleFirst(const aValue: Integer);
begin
  if FSpindleFirst <> aValue then begin
    if aValue > 0 then
      FSpindleFirst := aValue;
  end;// if FSpindleFirst <> aValue then begin
end;// procedure TStartLot.SetSpindleFirst(const aValue: Integer);

(*---------------------------------------------------------
  Keine Spindel zulassen, die gr�sser ist, als die gr�sste Spindel
----------------------------------------------------------*)
procedure TStartLot.SetSpindleLast(const aValue: Integer);
var
  xMaxSpdl: integer;
begin
  if FSpindleLast <> aValue then begin
    if MachID = 0 then
      raise Exception.Create('TStartLot.SetSpindleLast: Machine muss zugewiesen sein, wenn die lletzte Spindel gesetzt wird');

    xMaxSpdl := 0;
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := 'SELECT c_nr_of_spindles FROM t_machine WHERE c_machine_id = ' + IntToStr(MachID);
        open;
        if not(EOF) then
          xMaxSpdl := FieldByName('c_nr_of_spindles').AsInteger;
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try

    if aValue <= xMaxSpdl then
      FSpindleLast := aValue;
  end;// if FSpindleLast <> aValue then begin
end;// procedure TStartLot.SetSpindleLast(const aValue: Integer);

(*---------------------------------------------------------
  Konstruuktor
----------------------------------------------------------*)
constructor TDownloadHandler.Create;
begin
  inherited;

  mSystemPort := TSystemPort.Create(cClosePipeName);
  mSystemPort.ProcessMsg := ReceiveMsg;

  // Default = 2 Minuten
  TimeoutTime := 120;
end;// constructor TDownloadHandler.Create;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TDownloadHandler.Destroy;
begin
  FreeAndNil(mSystemPort);
  inherited;
end;// destructor TDownloadHandler.Destroy;

(*---------------------------------------------------------
  Wenn eine Startzeit ohne Endzeit vorgegeben ist, dann ist die Partie am Download
----------------------------------------------------------*)
function TDownloadHandler.GetIsDownloading: Boolean;
begin
  result := (FStartDownload > FStopDownload);
end;

(*---------------------------------------------------------
  Fragt ab, ob bereits seit einiger Zeit kein Download mehr erfogte
----------------------------------------------------------*)
function TDownloadHandler.GetTimedOut: Boolean;
begin
  Result := (IsDownloading) and (((now - FStartDownload)  * 24 * 3600) > TimeoutTime);
end;// function TDownloadHandler.GetTimedOut: Boolean;

(*---------------------------------------------------------
  Diese Funktion wird im Kontext des Kommunikations Thread aufgerufen.
  Das kann zu Problemen mit der Darstellung f�hren (VCL). Deshalb
  m�ssen die Daten dem Haupthread weitergereicht werden (WM_COPYDATA).
----------------------------------------------------------*)
procedure TDownloadHandler.ReceiveMsg(aReceivedMsg: PResponseRec);
var
  xCopyDataStruct: TCopyDataStruct;
begin
  case aReceivedMsg^.MsgTyp of
    rmProdGrpStoppedOk,
    rmProdGrpStoppedNOk,
    rmProdGrpStartedOk,
    rmProdGrpStartedNOk,
    rmProdGrpStateBad: begin
      // Antwort an die Applikation auslagern
      with xCopyDataStruct do begin
        dwData := 0;
        cbData := SizeOf(TResponseRec);
        lpData := aReceivedMsg;
      end;
      FStopDownload := now;
      // Die Funktion l�uft im Empfangsthread. Die Daten m�ssen im Kontext des Forms ausgewertet werden
      SendMessage(Application.Mainform.Handle, WM_COPYDATA, 0, Cardinal(@xCopyDataStruct));
    end;// ... rmProdGrpStateBad: begin
  end;// case aReceivedMsg^.MsgTyp of
end;// procedure TDownloadHandler.ReceiveMsg(aReceivedMsg: PResponseRec);

(*---------------------------------------------------------
  Manchmal kann es vorkommen, dass eine Partie immer im Status "psStopping" verbleibt.
  Diese PArtien st�ren den Ablauf. Deshalb wird der Status hier korrigiert. Im Hauptprogramm
  wird dann eine entsprechende Fehlermeldung ausgegeben
----------------------------------------------------------*)
function TDownloadHandler.RepairLot(aLot: TStartLot): Boolean;
begin
  result := false;
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := 'SELECT c_prod_id, c_prod_state FROM t_prodgroup_state WHERE c_machine_id = :c_machine_id AND c_spindle_first = :c_spindle_first AND c_spindle_last = :c_spindle_last';
      ParamByName('c_machine_id').AsInteger := aLot.MachID;
      ParamByName('c_spindle_first').AsInteger := aLot.SpindleFirst;
      ParamByName('c_spindle_last').AsInteger := aLot.SpindleLast;
      open;
      if not(EOF) then begin
        // Stopping Modus abbrechen
        if FieldByName('c_prod_state').AsInteger = ord(psStopping) then begin
          // ProdID merken
          aLot.CurrentProdID := FieldByName('c_prod_id').AsInteger;
          // t_prodgroup_state korrigieren
          SQL.Text := Format('UPDATE t_prodgroup_state SET c_prod_state = 4 WHERE c_prod_id = %d', [aLot.CurrentProdID]);
          ExecSQL;
          // t_prodgroup korrigieren
          SQL.Text := Format('Update t_prodgroup SET c_prod_state = 4, c_prod_end = ''%s'' WHERE c_prod_id = %d', [FormatDateTime(cSQLDateTimeFormatString, now), aLot.CurrentProdID]);
          ExecSQL;
          result := true;
        end;// if FiledByName('c_prod_state').AsInteger = ord(psStopping) then begin
      end;// if not(EOF) then begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try
end;// function TDownloadHandler.RepairLot(aLot: TStartLot): Boolean;

(*---------------------------------------------------------
  Markiert die Parte als "nicht gestartet" (Start und Stop = 0)
----------------------------------------------------------*)
procedure TDownloadHandler.Reset;
begin
  FStartDownload := 0;
  FStopDownload  := 0;
end;// procedure TDownloadHandler.Reset;

(*---------------------------------------------------------
  Startet eine Partie und erzeugt gleichzeitig einen neuen "Job"
----------------------------------------------------------*)
function TDownloadHandler.StartLot(aStartLot: TStartLot): Boolean;
var
  xJob: PJobRec;
  xXMLData: string;
  xSize: DWord;
  xModel: TXMLSettingsModel;
  xLongLength: Integer;
  xColorIndex: integer;
  xColor: TColor;
begin
  // Setzt die Start- und Stopp zeit zur�ck
  Reset;

  result := false;
  // Nur weiter, wenn die Maschine einen Download zul�sst
  if aStartLot.MachineState = msRunning then begin
    // Setting �bernehmen
    xXMLData := aStartLot.XMLSettings;

    // Settings mit dem Maschinenteil verschmelzen
    with TXMLSettingsMerger.Create do try
      xModel := TXMLSettingsModel.Create(nil);
      try
        xModel.xmlAsString := xXMLData;
        xLongLength := xModel.ValueDef[cXPChannelLongLengthItem, 6];

        // Bei jedem Download LongLength vergr�ssern
        inc(xLongLength);
        if xLongLength > 200 then
          xLongLength := 6;
        // Ins Modell schreiben ...
        xModel.Value[cXPChannelLongLengthItem] := xLongLength;
        // ... und ins StartLot sichern
        aStartLot.XMLSettings := xModel.xmlAsString;

        xModel.MaConfigReader.MachID := aStartLot.MachID;
        xXMLData := Merge(xModel, aStartLot.SpindleFirst, aStartLot.SpindleLast);
      finally
        FreeAndNil(xModel);
      end;
    finally
      Free;
    end;// with TXMLSettingsMerger.Create do try

    // L�nge des Settings berechnen
    xSize := Length(xXMLData);
    // Speicher bereitstellen (Job + XML Settings)
    xJob  := AllocMem(GetJobHeaderSize(jtStartGrpEv) + xSize);
    try
      with xJob^ do begin
        // Job abf�llen
        JobTyp                  := jtStartGrpEv;
        StartGrpEv.MachineID    := aStartLot.MachID;
        NetTyp                  := aStartLot.NetTyp;
        with StartGrpEv.SettingsRec do begin
          // XML Daten abf�llen
          System.Move(PChar(xXMLData)^, XMLData, xSize);
          SpindleFirst    := aStartLot.SpindleFirst;
          SpindleLast     := aStartLot.SpindleLast;
          Group           := aStartLot.Group;
          ComputerName    := mSystemPort.LocalComputerName;
          MMUserName      := GetCurrentMMUser;
          Port            := mSystemPort.PortName;
          // Zuf�llige Farbe aus dem Default Array holen ...
          xColorIndex     := Random(21);
          xColor          := cMMProdGroupColors[xColorIndex mod 4, xColorIndex div 5];
          // ... und dann noch zuf�llig etwas aufhellen (max. 25%)
          Color           := GetGradientColor(xColor, Random(25));
          YMSetChanged    := false;
          OrderPositionID := 1;
          OrderId         := 1;
          StyleID         := 1;
          Slip            := 1000;  //Nue: 1000 statt 1
          PilotSpindles   := 1;
          YarnCnt         := 50;
          YarnCntUnit     := yuNm;
          NrOfThreads     := 1;
          SpeedRamp       := 100;
          Speed           := 5;
          lengthWindow    := 1000;
          lengthMode      := ord(lwmFirst);
          StrPCopy(@YMSetName, aStartLot.YMSetName);

          if aStartLot.ProdName > '' then
            aStartLot.ProdName := 'Partie Start Simulator';
          StrPCopy(@ProdName, aStartLot.ProdName)
        end;// with StartGrpEv.SettingsRec do begin
      end;// with xJob^ do begin

      // Anzahl erfolgter Downloads erh�hen
      aStartLot.DownloadTry := aStartLot.DownloadTry + 1;

      // Job abschicken
      mSystemPort.NewJob(xJob);
      // Startzeit merken
      FStartDownload := now;
      result := true;
    finally
      FreeMem(xJob);
    end;// try finally
  end;// if aStartLot.Ma<MachineState = msRunning then begin
end;// function TDownloadHandler.StartLot(aStartLot: TStartLot): Boolean;

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TAvailabilityChecker.Create;
begin
  inherited;
  // Default = 1 Minute
  FLockBevoreACQTime := 1;
end;// constructor TDownloadAvailabilityChecker.Create;

(*---------------------------------------------------------
  Pr�ft alle Einstellungen die mit der Akquirierung zusammenh�ngen.
    - Download w�hrend der Akquierierung
    - Download kurz vor der Akquierierung
----------------------------------------------------------*)
function TAvailabilityChecker.CheckDuringACQ: Boolean;
begin
  result := not(LockDuringACQ);

  if not(result) then begin
    // Pr�fen ob gerade ein Interval am laufen ist
    with TAdoDBAccess.Create(1) do try
      Init;
      with Query[0] do begin
        SQL.Text := Format('SELECT c_prod_state FROM t_prodgroup_state WHERE c_prod_state = %d', [ord(psCollectingData)]);
        open;
        if EOF then
          result := true;

        if result and LockBevoreACQ then begin
          // Wenn kein Intervall l�uft, dann noch pr�fen ob das laufende Intervall in einer Minute beendet ist
          SQL.Text := 'SELECT c_interval_start FROM t_interval WHERE c_interval_start > getdate() ORDER BY c_interval_start';
          open;
          if not(EOF) then
            // aktuelle Zeit kleiner als n�chster Intervallstart - "LockTime" in Minuten
            result := (now < (FieldByName('c_interval_start').AsDateTime - (LockBevoreACQTime / 24 / 60)))
          else
            result := true;
        end;// if cbNoDownloadBevoreAcq.Checked then begin
      end;// with Query[0] do begin
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  end;// if not(xDownloadEnabled) then begin
end;// function TDownloadAvailabilityChecker.CheckDuringACQ: Boolean;

(*---------------------------------------------------------
  Pr�ft ob der JobHandler l�uft. Wenn ja wird angenommen, dass das System l�uft
----------------------------------------------------------*)
function TAvailabilityChecker.IsSystemRunning: Boolean;
var
  xList: TStringList;
begin
  result := true;
  if AnsiSameText(gMMHost, GetLocalComputerName) then begin
    result := false;
    xList := TStringList.Create;
    try
      // Holt eine Liste der laufenden Prozesse
      RunningProcessesList(xList, false);
      result := (xList.IndexOf('JOBHANDLER.EXE') > 0);
    finally
      xList.Free;
    end;
  end;// if AnsiSameText(gMMHost, GetLocalComputerName) then begin
end;// function TDownloadAvailabilityChecker.CheckSystemRunning: Boolean;

(*---------------------------------------------------------
  Pr�ft ob zum aktuellen Zeitpunkt der Daownload erlaubt ist
----------------------------------------------------------*)
function TAvailabilityChecker.GetIsDownloadAllowed: Boolean;
begin
  result := IsSystemRunning;
  result := result and (CheckDuringACQ);
end;// function TDownloadAvailabilityChecker.GetIsDownloadAllowed: Boolean;

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TMemoryGuard.Create;
begin
  inherited;
  mProcs := TStringList.Create;
end;// constructor TMemoryGuard.Create;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TMemoryGuard.Destroy;
begin
  FreeAndNil(mProcs);
  inherited;
end;// destructor TMemoryGuard.Destroy;

(*---------------------------------------------------------
  F�gt einen Prozess zur Liste der �berwachten Prozesse hinzu
----------------------------------------------------------*)
procedure TMemoryGuard.AddProcess(aProcName: string);
begin
  if mProcs.IndexOf(aProcName) = - 1 then
    mProcs.Add(aProcName);
end;// procedure TMemoryGuard.AddProcess(aProcName: string);

(*---------------------------------------------------------
  F�gt mehrere Prozesse in die Liste der �berwachten Prozesse ein.
  ist "aNewList" = true, dann werden die bereits erfassten Prozesse entfernt
----------------------------------------------------------*)
procedure TMemoryGuard.AddProcess(aProcNameList: TStrings; aNewList: boolean);
var
  i: Integer;
begin
  // Wenn neue Liste, dann die alten Prozesse verwerfen
  if aNewList then begin
    mProcs.Assign(aProcNameList);
  end else begin
    // Liste zu den aktuellen Prozessen hinzuf�gen
    for i := 0 to aProcNameList.Count - 1 do
      AddProcess(aProcNameList[i]);
  end;// if aNewList then begin
end;// procedure TMemoryGuard.AddProcess(aProcNameList: TStrings; aNewList: boolean);

(*---------------------------------------------------------
  Fordert von Windows die Liste aller laufenden Prozesse an
----------------------------------------------------------*)
function TMemoryGuard.InitProcList: Boolean;
var
  xIndex: Integer;
  i: Integer;
  xProcList: TStringList;
begin
  xProcList := TStringList.Create;
  try
    // Liste mit dem Prozessnamen und der ProzessID in der Eigenschaft Objects[]
    Result := RunningProcessesList(xProcList, false);
    for i := 0 to mProcs.Count - 1 do begin
      // Liefert den Index des �berwachten Prozesses in der Windowsliste
      xIndex := xProcList.IndexOf(mProcs[i]);
      if xIndex >= 0 then
        // Wenn der Prozess l�uft, dann die ProzessID aktualisieren
        mProcs.Objects[i] := xProcList.Objects[xIndex]
      else
        // Wenn der Prozess nicht l�uft, dann die ProzessID initialisieren
        mProcs.Objects[i] := nil;
    end;// for i := 0 to xProcList.Count - 1 do begin
  finally
    FreeAndNil(xProcList);
  end;// try finally
end;// function TMemoryGuard.InitProcList: Boolean;

(*---------------------------------------------------------
  Fragt f�r den aktuellen Prozess die Speichernutzung ab
----------------------------------------------------------*)
function TMemoryGuard.MemUsage(aProcName: string): DWORD;
var
  xIndex: Integer;
begin
  Result := 0;
  xIndex := mProcs.IndexOf(aProcName);
  if xIndex >= 0 then begin
    // ProzessID aus der Liste (Objects[])
    with ProcessMemoryUsage(Cardinal(mProcs.Objects[xIndex])) do
      result := PagefileUsage;
  end;// if xIndex >= 0 then begin
end;// function TMemoryGuard.MemUsage(aProcName: string): DWORD;

(*---------------------------------------------------------
  Vor dem n�chsten Interval wird ein neues eingef�gt.
  Im Moment nicht m�glich eine Minute vor regul�rem Intervall Wechsel oder
  wenn der Intervall Wechsel auf einen Schichtwechsel f�llt.

  Wenn mehrere Schichtkalender definert sind, ist die Funktion deaktiviert
----------------------------------------------------------*)
procedure TIntervalHandler.SetNewInterval(aTime: TDateTime);
var
  xNextIntervalChange: TDateTime;
  xNextShiftChange: TDateTime;
  xQuery: TNativeAdoQuery;

  (*---------------------------------------------------------
    Liefert den n�chsten Schichtwechsel
  ----------------------------------------------------------*)
  function GetNextShiftChange: TDateTime;
  const
    cGetCurrentShift = ' SELECT Top 1 c_shift_id, c_shift_start, c_shift_length ' +
                       ' FROM t_shift ' +
                       ' WHERE c_shift_start < getdate()' +
                       ' ORDER BY c_shift_start desc';
  begin
    with xQuery do begin
      SQL.Text := cGetCurrentShift;
      open;
      if not(EOF) then
        result := FieldByName('c_shift_start').AsDateTime + (FieldByName('c_shift_length').AsInteger / 60 / 24)
      else
        raise EMMStress.Create('Das Ende der aktuellen Schicht kann nicht ermittelt werden');
    end;// with aQuery do begin
  end;// function GetNextShiftChange: TDateTime;

  (*---------------------------------------------------------
    Liefert den n�chsten Intervall Wechsel
  ----------------------------------------------------------*)
  function GetNextIntervalChange: TDateTime;
  const
    cGetNextIntervalChange = ' select distinct c_interval_id, c_interval_start ' +
                             ' from t_interval ' +
                             ' where c_interval_start in ( select max ( c_interval_start ) from t_interval )';
  begin
    with xQuery do begin
      SQL.Text := cGetNextIntervalChange;
      open;
      if not(EOF) then
        result := FieldByName('c_interval_start').AsDateTime
      else
        raise EMMStress.Create('Das n�chste Intervallwechsel kann nicht ermittelt werden');
    end;// with aQuery do begin
  end;// function GetNextIntervalChange: TDateTime;

  (*---------------------------------------------------------
    F�gt einen neues Intervall ein
  ----------------------------------------------------------*)
  procedure InsertNewInterval;
  var
    xTimeJobID: integer;
    xIntID: integer;
    xNewInterval: TDateTime;
    xData: TJobRec;
    xStream: TMemoryStream;
  const
    cUpdateCurrentInterval ='UPDATE t_Interval SET c_Interval_End = :NewIntervalEnd' +
                            ' WHERE c_Interval_End = :OldIntervalEnd AND c_Interval_End <> c_Interval_Start';


    cInsertNewInterval = 'insert t_Interval ( c_interval_id, c_Interval_Start, c_Interval_End )' +
                         ' values ( :c_interval_id, :c_Interval_Start, :c_Interval_End )';

    cUpdteJoblist = 'UPDATE t_time_joblist SET c_time = :c_time, c_last_execute_time = :c_last_execute_time, c_data = :c_data' +
                    ' WHERE c_time_joblist_id = :c_time_joblist_id';
  begin
    xNewInterval := aTime;

    if (xNewInterval - now) > (1 / 60 / 24) then
      ReplaceTime(xNewInterval, EncodeTime(HourOfTime(xNewInterval), MinuteOfTime(xNewInterval), 0, 0))
    else
      ReplaceTime(xNewInterval, EncodeTime(HourOfTime(xNewInterval), MinuteOfTime(xNewInterval) + 2, 0, 0));

    with xQuery do begin
      // Fugt das neue Intervall in die Tabelle t_interval ein
      SQL.Text := cInsertNewInterval;
      Params.ParamByName('c_Interval_Start').AsDateTime := xNewInterval;
      Params.ParamByName('c_Interval_End').AsDateTime   := xNewInterval;
      xIntID := InsertSQL('t_Interval', 'c_interval_id', high(byte), 1);

      // L�scht das bereits eingetragene Interval
      SQL.Text := 'DELETE t_Interval WHERE c_Interval_Start = :c_Interval_Start';
      Params.ParamByName('c_Interval_Start').AsDateTime := xNextIntervalChange;
      ExecSQL;

      // Aktualisiert die Endzeit des aktuell laufenden Intervalls
      SQL.Text := cUpdateCurrentInterval;
      ParamByName('OldIntervalEnd').AsString := FormatDateTime(cSQLDateTimeFormatString, xNextIntervalChange);
      ParamByName('NewIntervalEnd').AsString := FormatDateTime(cSQLDateTimeFormatString, xNewInterval);
      ExecSQL;

      // Tr�gt das neue Interval in die Jobliste des Timehandlers ein (Tabelle t_time_joblist)
      SQL.Text := 'SELECT c_time_joblist_id, c_data FROM t_time_joblist WHERE c_job_type = 18';
      Open;
      if not(eof) then begin
        xTimeJobID := FieldByName('c_time_joblist_id').AsInteger;
        FillChar(xData, SizeOf(xData), 0);

        // Liest die Daten in einen Jobrecord ein
        xStream := TMemoryStream.Create;
        try
          GetBlobFieldData('c_data', xStream);
          xStream.Position := 0;
          xStream.Read(xData, sizeof(TJobRec));
        finally
          xStream.Free;
        end;

        // Aktualisiert die Tabelle t_time_joblist
        SQL.Text := cUpdteJoblist;
        // Zeit in ms seit Mitternacht des Ereignisses in die Tabelle eintragen
        // Dieser Eintrag ist f�r den Timehandler der Zeitstempel an dem der Job stattzufinden hat
        ParamByName('c_time').AsInteger := TimeOfDateTimeToMSecs(xNewInterval);
        ParamByName('c_last_execute_time').AsDateTime := xNewInterval;
        ParamByName('c_time_joblist_id').AsInteger := xTimeJobID;
        xData.DataAquEv.IntID := xIntID;
        ParamByName('c_data').SetBlobData(@xData, sizeof(xData));
        ExecSQL;
      end;// if not(eof) do begin
    end;// with aQuery do begin
  end;// procedure InsertNewInterval;

begin
  try
    Screen.Cursor := crHourglass;
    with TAdoDBAccess.Create(1) do try
      Init;
      xQuery := Query[0];
      StartTransaction;
      try
        // Schicht und Intervallwechsel abfragen
        xNextShiftChange := GetNextShiftChange;
        xNextIntervalChange := GetNextIntervalChange;

        if xNextIntervalChange < (now + (2 / 60 / 24)) then
          raise EMMStress.Create('Keine Aktion da der n�chste regull�re Intervall n�chstens f�llig wird.');
  
        // Keine Aktion, wenn der Schicht- und der Intervallwechsel zusammenfallen
        if xNextShiftChange = xNextIntervalChange then
          raise EMMStress.Create('Schichtwechsel und Intervallwechsel sind identisch. Bitte bis nach Schichtwechsel warten.');
  
        InsertNewInterval;
        Commit;
      except
        on e: Exception do begin
          Rollback;
          MessageDlg(e.Message, mtInformation, [mbOK], 0);
        end;
      end;//try except
    finally
      Free;
    end;// with TAdoDBAccess.Create(1) do try
  finally
    Screen.Cursor := crDefault;
  end;// try finally
end;// procedure TIntervalHandler.SetNewInterval(aTime: TDateTime);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TLXConnectionThread.Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer; const aIpAdresse: PChar; const aMaschID:
    Integer);
begin
  inherited Create(True);
  FreeOnTerminate := True;

  FEventHandle := INVALID_HANDLE_VALUE;

  LXHandle := Integer(INVALID_HANDLE_VALUE);
  mLxDevHandler := TLxDevHandler.Create(aThreadWndHandle, aInformWndMsg, aIpAdresse, aMaschID);
end;

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TLXConnectionThread.Destroy;
begin
  FreeAndNil(mLxDevHandler);
  inherited;
end;

(*---------------------------------------------------------
  F�hrt das Verbinden und wieder L�sen der Verbindung aus
----------------------------------------------------------*)
procedure TLXConnectionThread.execute;
var
  xLastCount: DWORD;
  xError: string;
begin
  while not(mLxDevHandler.Connect) do;
  LXHandle := mLxDevHandler.DB_Handle;
  // Erst mal schlafen legen, damit die anderen Threads auch ein bisschen Zeit bekommen
  Sleep(200);

  CodeSite.SendInteger('LXHandle', LXHandle);
  DecrementThreadCount;
  if lLXConnectionThreadCount = 0 then
    SetEvent(FEventHandle);

  while not(Terminated) do
    Sleep(1);

  mLxDevHandler.Disconnect;
end;// procedure TLXConnectionThread.execute;

class procedure TLXConnectionThread.DecrementThreadCount;
begin
  InterlockedDecrement(lLXConnectionThreadCount);
end;

class procedure TLXConnectionThread.InitializeThreadCount(aCount: integer);
begin
  lLXConnectionThreadCount := aCount;
end;

procedure TLXConnectionTester.DoUpdateLXConnectionGUI(aCycleCount: Integer; aMsg: string);
begin
  if Assigned(FOnUpdateLXConnectionGUI) then FOnUpdateLXConnectionGUI(Self, aCycleCount, aMsg);
end;

(*---------------------------------------------------------
  LX Connection
----------------------------------------------------------*)
procedure TLXConnectionTester.Execute(aThreads: Integer; aCycles: integer);
var
  i: Integer;
  xMsg: string;
  xReplaced: Boolean;
  xTempValue: Integer;
  xDoubles: Integer;
  xThreadArray: array of TLXConnectionThread;
  xSemaphoreHandle: THandle;
  xEventHandle: THandle;
  xThreadcount: Integer;
  xCycles: Integer;
  xGlobalDoubles: Integer;
  xLXMachInfos: TLXMachInfoArray;
  xMachCount: Integer;
  xLXMachInfo: TLXConnectionRec;
begin
  xThreadcount := aThreads;
  SetLength(xThreadArray, xThreadcount);
  SetLength(mLXHandles, xThreadcount);
  FillIP(xLXMachInfos);
  xGlobalDoubles := 0;
  GetAsyncKeyState(VK_ESCAPE);
  for xCycles := 0 to aCycles - 1 do begin
    // Abbrechen, wenn die Escape Taste gedr�ckt wurde
    if (GetAsyncKeyState(VK_ESCAPE) <> 0) then
      Break;
    mThreadsFinished := 0;
    // Threadcount auf 0 setzen
    TLXConnectionThread.InitializeThreadCount(xThreadcount);
    xEventHandle := CreateEvent(nil, True, False, nil);
    try
      // Threads erzeugen
      i := 0;
      while (i < xThreadcount) do begin
        mLXHandles[i] := INVALID_HANDLE_VALUE;
        xMachCount := Length(xLXMachInfos);
        xLXMachInfo := xLxMachInfos[i mod xMachCount];
        xThreadArray[i] := TLXConnectionThread.Create(Application.MainForm.Handle, WM_USER, xLXMachInfo.Address, xLXMachInfo.MachId);
        with xThreadArray[i] do begin
          EventHandle        := xEventHandle;
          LXHandleArrayIndex := i;
          OnTerminate        := LXThreadTerminate;
        end;// with TLXConnectionThread.Create(Handle, WM_USER, '150.158.148.44', 2) do begin
        Inc(i);
      end;// while (i < xThreadcount) do begin

      // Threads starten
      for i := 0 to Length(xThreadArray) - 1 do
        xThreadArray[i].Resume;

      WaitForSingleObject(xEventHandle, 80000);
      // Threads beenden
      for i := 0 to Length(xThreadArray) - 1 do
        xThreadArray[i].Terminate;

      // Warten bis alle Threads fertig sind
      while mThreadsFinished < xThreadcount do begin
        Sleep(1);
        // Notwendig, damit OnTerminate aufgerufen werden kann
        Application.ProcessMessages;
      end;// while mThreadsFinished < cThreadCount do begin
    finally
      CloseHandle(xEventHandle);
    end;// try finally

    // Alle Threads fertig

    // sortieren (Austauschverfahren)
    repeat
      xReplaced := false;
      for i := 0 to Length(mLXHandles) - 2 do begin
        if mLXHandles[i] > mLXHandles[i + 1] then begin
          // Tauschen
          xTempValue := mLXHandles[i];
          mLXHandles[i] := mLXHandles[i + 1];
          mLXHandles[i + 1] := xTempValue;
          xReplaced := true;
        end;// if mLxHandles[i] > mLxHandles[i + 1] then begin
      end;// for i := 0 to Length(mLxHandles) - 2 do begin
    until not(xReplaced);

    xDoubles := 0;
    for i := 0 to Length(mLXHandles) - 2 do begin
      if (mLXHandles[i] = mLXHandles[i + 1]) and (mLXHandles[i] <> INVALID_HANDLE_VALUE) then
        Inc(xDoubles);
    end;// for i := 0 to Length(mLxHandles) - 2 do begin

    xGlobalDoubles := xGlobalDoubles + xDoubles;

    xMsg := '';
    for i := 0 to Length(mLXHandles) - 1 do
      xMsg := xMsg + cCRLF + IntToStr(Integer(mLXHandles[i]));
    xMsg := Format('Doppelte Handels: %d%sDoppelte Handles (letzter Durchlauf): %d%sErteilte Handles:%s',
                                 [xGlobalDoubles, cCrlf,xDoubles, cCrlf + cCrlf, xMsg]);
    DoUpdateLXConnectionGUI(xCycles + 1, xMsg);
    Application.ProcessMessages;
  end;// for xCycles := 0 to edCycles.AsInteger do begin
end;// procedure TfrmMainWindow.bStartConnectingClick(Sender: TObject);

(*---------------------------------------------------------
  IP Adressen abf�llen
----------------------------------------------------------*)
procedure TLXConnectionTester.FillIP(var aLXMachInfos: TLXMachInfoArray);
var
  i: Integer;
begin
  with TAdoDBAccess.Create(1) do try
    Init;
    with Query[0] do begin
      SQL.Text := 'SELECT c_node_id, c_machine_id FROM t_machine WHERE c_net_id = :c_net_id and c_machine_state = :c_machine_state';
      ParamByName('c_net_id').AsInteger := Ord(ntLX);
      ParamByName('c_machine_state').AsInteger := Ord(msRunning);
      open;
      SetLength(aLXMachInfos, Recordset.RecordCount);
      i := 0;
      while not(EOF) do begin
        with aLXMachInfos[i] do begin
          StrCopy(@(Address), PChar(FieldByName('c_node_id').AsString));
          MachId := FieldByName('c_machine_id').AsInteger;
        end;
        Inc(i);
        next;
      end;// while not(EOF) do begin
    end;// with Query[0] do begin
  finally
    Free;
  end;// with TAdoDBAccess.Create(1) do try                              
end;// procedure FillIP;

(*---------------------------------------------------------
  Wird von jedem Thread aufgerufen der fertig wurde
----------------------------------------------------------*)
procedure TLXConnectionTester.LXThreadTerminate(Sender: TObject);
begin
  mLXHandles[TLXConnectionThread(Sender).LXHandleArrayIndex] := TLXConnectionThread(Sender).LXHandle;
  // Da OnTerminate mit Syncronize aufgerufen wird, kann hier direkt incrementiert werden.
  Inc(mThreadsFinished);
end;


end.
