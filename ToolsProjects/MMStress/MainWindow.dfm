object frmMainWindow: TfrmMainWindow
  Left = 253
  Top = 167
  Width = 961
  Height = 686
  Caption = 'Millmaster Stress'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object sb: TmmStatusBar
    Left = 0
    Top = 637
    Width = 953
    Height = 22
    Panels = <
      item
        Width = 150
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object pgMain: TmmPageControl
    Left = 0
    Top = 0
    Width = 953
    Height = 637
    ActivePage = tabSimulation
    Align = alClient
    TabOrder = 0
    OnChange = pgMainChange
    object tabSimulation: TTabSheet
      Caption = 'Partiestart Simulation'
      object mmPanel2: TmmPanel
        Left = 0
        Top = 0
        Width = 353
        Height = 609
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object mmLabel1: TmmLabel
          Left = 216
          Top = 49
          Width = 106
          Height = 13
          Caption = 'Minimale Laufzeit [min]'
          FocusControl = edMinRuntime
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel2: TmmLabel
          Left = 216
          Top = 89
          Width = 109
          Height = 13
          Caption = 'Maximale Laufzeit [min]'
          FocusControl = edMaxRunTime
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel6: TmmLabel
          Left = 16
          Top = 152
          Width = 159
          Height = 20
          Caption = 'N�chster Download in '
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = True
          AutoLabel.Control = mTimerLabel
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpRight
        end
        object mTimerLabel: TmmLabel
          Left = 179
          Top = 152
          Width = 11
          Height = 20
          Caption = '0'
          FocusControl = mmLabel6
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.Control = s
          AutoLabel.LabelPosition = lpRight
        end
        object s: TmmLabel
          Left = 192
          Top = 152
          Width = 10
          Height = 20
          Caption = 's'
          FocusControl = mTimerLabel
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laStartedSince: TmmLabel
          Left = 15
          Top = 126
          Width = 3
          Height = 13
          Caption = '-'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object butReadConfiguration: TmmBitBtn
          Left = 16
          Top = 16
          Width = 153
          Height = 25
          Caption = 'Laufende Partien einlesen'
          TabOrder = 0
          Visible = True
          OnClick = butReadConfigurationClick
          AutoLabel.LabelPosition = lpLeft
        end
        object edMinRuntime: TmmEdit
          Left = 216
          Top = 64
          Width = 113
          Height = 21
          Color = clWindow
          TabOrder = 2
          Text = '0.1'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = mmLabel1
          AutoLabel.LabelPosition = lpTop
          Decimals = 2
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
        object edMaxRunTime: TmmEdit
          Left = 216
          Top = 104
          Width = 113
          Height = 21
          Color = clWindow
          TabOrder = 4
          Text = '2'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = mmLabel2
          AutoLabel.LabelPosition = lpTop
          Decimals = 0
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
        object butStart: TmmButton
          Left = 16
          Top = 64
          Width = 150
          Height = 25
          Action = acStartCycle
          Caption = 'Start (Neu einlesen)'
          TabOrder = 1
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object butStop: TmmButton
          Left = 16
          Top = 96
          Width = 150
          Height = 25
          Action = acStopCycle
          TabOrder = 3
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object pACQPanel: TmmPanel
          Left = 16
          Top = 208
          Width = 313
          Height = 33
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
        end
        object b1: TmmBitBtn
          Left = 16
          Top = 272
          Width = 153
          Height = 25
          Action = acOpenSuccessLog
          Caption = 'Logfile �ffnen'
          TabOrder = 6
          Visible = True
          Glyph.Data = {
            8E020000424D8E020000000000008E0100002800000010000000100000000100
            08000000000000010000120B0000120B0000560000005600000000000000FFFF
            FF00FF00FF00A5696A0095655F009D6B620098686000A36F6400FFFBFA00AD78
            6900A7746600BB7F6A00B47E6B00BF877000BA836D00BB704F00C48C7200D397
            7800CA917400C2805C00D0906B00D59A7900CE957600F4EBE600FEF4ED00FFFC
            FA00FCF4EE00FAEDE100FCF4ED00FEFCFA00EF993800E5A55F00F2D5B500F6E1
            CA00F8E7D400F7E6D400F7E7D500F8EDE100E2A35B00F0D0A900F6E1C900F4E1
            CA00FAEDDE00FAEEE000EECB9E00F4D5AD00F0D1AA00F2D7B400F2D7B500F4DD
            C000F3DCBF00F8E9D500FAEFE000EFCA9600F0CF9F00FCE7C900E3D3BB00FCEB
            D300FEEED400FBE5C000B7AD9C00FFF4E100FCF6EB00FFFBEE00FFFFFB00FFFF
            FC00006A0000016C0100016B01000A760A000D780D001C831C0023892300469F
            46004AA14A004DA34D004EA44E005FAD5F0060AD60006AB46A0078BB7800B4DA
            B400BBDDBB00EEF7EE00FAFCFA00F8F8FE000202020202424242424242424242
            424204030303034201010101010101010142063B2D3635420101010101010101
            0142053720272C4201504247544F424B0142073A31302E420101514346424F01
            01420A3D21322F420101014C424554010142093F2429324201014D424A434853
            01420C40252328420149424E0152444801420E011A1B33420101010101010101
            01420D0108182B4201010101010101010142100101191C424242424242424242
            424212010101191C2A2239383C030202020216010101011D3E34030303030202
            0202110101010101411703261E0F020202021501010101010155031F13020202
            02021414141414141414030B020202020202}
          AutoLabel.LabelPosition = lpLeft
        end
        object b2: TmmBitBtn
          Left = 176
          Top = 272
          Width = 153
          Height = 25
          Action = acOpenErrorLog
          Caption = 'Error Log �ffnen'
          TabOrder = 7
          Visible = True
          Glyph.Data = {
            16030000424D1603000000000000160200002800000010000000100000000100
            08000000000000010000120B0000120B0000780000007800000000000000FFFF
            FF00807AA100FF00FF00B6919300A4908C00F4F0EF00E7CABC00D4805100D991
            6800E0A58400D9834F00E1966600DA926800DA976D00DEAB8B00C5B4AA00E39F
            7300E6A47800EAAC8100E9BC9F00EFD0BC00F7E0D000F0995B00E69D6900F2C0
            9800F3CCAD00EFC9AB00FEE5D000FFF3E900FFFAF600FBF6F200FCC08B00E9D4
            C200FAE5D300FFD1A300F8D5B400FEF4EA00E1D8CF00DAD5D000FEFCFA00AB75
            3D00FCE2C500FCE6CF00FFEAD400FAD3A300FFFAF400FFD7A500FEE6C700CAB7
            9E00F3EDE500FEFBF700FEF3E300D3CBC000B5B0A900FFFAF300FFDDA700FFDD
            A100FFEAC600FFF6E600D5CEC000EAE9E700FFFAEB00FFFEFB00B4B4B100FEFE
            FC00EDEDEB00FFFFFE00BABBB800C2C4C1006EB4590062D18800186F7500CEE2
            F8001A6AC7009DCAFF00A7CFFF00ADD3FF00C0D5EE00D8EAFF003289FF002A68
            B8004D99FF00569EFF00589FFF005BA0FE005C96E5005F98E30080B7FF00A1C7
            F700A6C5EF001673FF001676FF00287FFF00398AFF005D8CD1001873FF001974
            FF001B75FF001C76FF002378FA002D75E9002A69D100317BEB0099B6E3000F59
            E100135DE0001864E3001E60D8003767BF005F8BD7009EA7B7001A57CA002358
            C2001344B400D5D7DC00093BC200415B9F001F42EF00E0E0E000030303030303
            0303030303030303030303030303030303030303030303030303030303030303
            69706D7572030303030303030303675F6F100F1705710303030303035C572D2F
            3823120B20216E6C0303030351313A302A1B0D18242C3E5A6503036329392534
            221408132B1D1E01686A034A463B2E37160A0C1A362741430466034847013F33
            1509111C32444506026403744E010128070E19261F013D5964036B76014F4C58
            50567335407701535B03625E525403030303614B423C495D0303030303030303
            03030303614D5560030303030303030303030303030361030303030303030303
            0303030303030303030303030303030303030303030303030303}
          AutoLabel.LabelPosition = lpLeft
        end
        object paDescriptionPanel1: TmmPanel
          Left = 16
          Top = 312
          Width = 313
          Height = 65
          BevelOuter = bvLowered
          TabOrder = 8
          object pa6: TmmPanel
            Left = 1
            Top = 1
            Width = 21
            Height = 63
            Align = alLeft
            BevelOuter = bvNone
            Color = clInfoBk
            TabOrder = 0
            object imInfoImage1: TmmImage
              Left = 2
              Top = 0
              Width = 16
              Height = 16
              Picture.Data = {
                07544269746D617036030000424D360300000000000036000000280000001000
                0000100000000100180000000000000300000000000000000000000000000000
                0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFF}
              Transparent = True
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
          object memoDescriptionMemo1: TmmMemo
            Left = 22
            Top = 1
            Width = 290
            Height = 63
            Align = alClient
            BorderStyle = bsNone
            Color = clInfoBk
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'SHIFT und Click auf "Logfile �ffnen" �ffnet den Explorer. Das '
              'File soillte w�rend dem Stresstest nicht ge�ffnet werden, da '
              'Excel die Datei exclusiv �ffnet. Deshalb die Datei zuerst '
              'kopieren')
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
      object mmPanel1: TmmPanel
        Left = 353
        Top = 0
        Width = 592
        Height = 609
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object vstJobStack: TmmVirtualStringTree
          Left = 0
          Top = 17
          Width = 592
          Height = 570
          Align = alClient
          Header.AutoSizeIndex = 0
          Header.Font.Charset = DEFAULT_CHARSET
          Header.Font.Color = clWindowText
          Header.Font.Height = -11
          Header.Font.Name = 'MS Sans Serif'
          Header.Font.Style = []
          Header.Options = [hoColumnResize, hoDrag, hoVisible]
          Indent = 0
          Margin = 0
          TabOrder = 0
          TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
          TreeOptions.PaintOptions = [toShowDropmark, toShowHorzGridLines, toShowRoot, toShowVertGridLines, toThemeAware, toUseBlendedImages]
          TreeOptions.SelectionOptions = [toFullRowSelect, toRightClickSelect]
          OnGetText = vstJobStackGetText
          OnPaintText = vstJobStackPaintText
          Columns = <
            item
              Position = 0
              Width = 60
              WideText = 'Start'
            end
            item
              Position = 1
              Width = 75
              WideText = 'ProdID'
            end
            item
              Position = 2
              WideText = 'Group'
            end
            item
              Position = 3
              WideText = 'MachID'
            end
            item
              Position = 4
              Width = 70
              WideText = 'ProdName'
            end
            item
              Position = 5
              Width = 35
              WideText = 'Von'
            end
            item
              Position = 6
              Width = 35
              WideText = 'Bis'
            end
            item
              Position = 7
              Width = 102
              WideText = 'YM Set'
            end
            item
              Position = 8
              Width = 45
              WideText = 'YM ID'
            end
            item
              Position = 9
              WideText = 'try'
            end>
        end
        object sbLotList: TmmStatusBar
          Left = 0
          Top = 587
          Width = 592
          Height = 22
          Panels = <>
          SimplePanel = True
          SizeGrip = False
        end
        object pa3: TmmPanel
          Left = 0
          Top = 0
          Width = 592
          Height = 17
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object pa4: TmmPanel
            Left = 407
            Top = 0
            Width = 185
            Height = 17
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object cbOnlyActive: TmmCheckBox
              Left = 16
              Top = 0
              Width = 161
              Height = 17
              Caption = 'Nur aktive Partien zeigen'
              TabOrder = 0
              Visible = True
              OnClick = cbOnlyActiveClick
              AutoLabel.LabelPosition = lpLeft
            end
          end
          object pa5: TmmPanel
            Left = 0
            Top = 0
            Width = 407
            Height = 17
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object la4: TmmLabel
              Left = 0
              Top = 0
              Width = 407
              Height = 13
              Align = alTop
              Caption = 'N�chste Downloads:'
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Funktionen'
      ImageIndex = 1
      object mmLineLabel1: TmmLineLabel
        Left = 0
        Top = 9
        Width = 945
        Height = 25
        Align = alTop
        AutoSize = False
        Caption = 'Einstellungen Partiestart Simulation'
        Distance = 2
      end
      object mmLineLabel2: TmmLineLabel
        Left = 0
        Top = 169
        Width = 945
        Height = 17
        Align = alTop
        AutoSize = False
        Caption = 'Intervall'
        Distance = 2
      end
      object mmPanel6: TmmPanel
        Left = 0
        Top = 34
        Width = 945
        Height = 135
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object mmLabel7: TmmLabel
          Left = 312
          Top = 1
          Width = 110
          Height = 13
          Caption = 'Verf�gbare Maschinen:'
          FocusControl = lbSelectedMachines
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmGroupBox1: TmmGroupBox
          Left = 16
          Top = 8
          Width = 233
          Height = 65
          Caption = 'Akquirierung'
          TabOrder = 0
          CaptionSpace = True
          object cbNoAcquisition: TmmCheckBox
            Left = 8
            Top = 24
            Width = 209
            Height = 17
            Caption = 'Kein Download w�hrend Akquirierung'
            TabOrder = 0
            Visible = True
            OnClick = configureDownloadClick
            AutoLabel.LabelPosition = lpLeft
          end
          object cbNoDownloadBevoreAcq: TmmCheckBox
            Left = 24
            Top = 40
            Width = 201
            Height = 17
            Caption = 'Kein Download 1min vor Akquirierung'
            TabOrder = 1
            Visible = True
            OnClick = configureDownloadClick
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object lbSelectedMachines: TmmCheckListBox
          Left = 312
          Top = 16
          Width = 505
          Height = 113
          Columns = 3
          ItemHeight = 13
          TabOrder = 1
          Visible = True
          AutoLabel.Control = mmLabel7
          AutoLabel.LabelPosition = lpTop
        end
        object cbChangeSpdlRange: TmmCheckBox
          Left = 16
          Top = 96
          Width = 289
          Height = 17
          Caption = 'Spindelrange ver�ndern (gilt nur f�r Texnet Maschinen)'
          TabOrder = 2
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object mmPanel8: TmmPanel
        Left = 0
        Top = 0
        Width = 945
        Height = 9
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
      end
      object mmPanel9: TmmPanel
        Left = 0
        Top = 186
        Width = 945
        Height = 103
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object mmLabel9: TmmLabel
          Left = 294
          Top = 16
          Width = 184
          Height = 20
          Caption = 'N�chster Intervallwechsel:'
          FocusControl = laNextInterval
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laNextInterval: TmmLabel
          Left = 480
          Top = 16
          Width = 5
          Height = 20
          Caption = '-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          Visible = True
          AutoLabel.Control = mmLabel9
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel10: TmmLabel
          Left = 39
          Top = 64
          Width = 247
          Height = 13
          Caption = 'Intervalwechsel / Maschinenzustand abfragen [ms]: '
          FocusControl = laStopWatchTime
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laStopWatchTime: TmmLabel
          Left = 288
          Top = 64
          Width = 3
          Height = 13
          Caption = '-'
          Visible = True
          AutoLabel.Control = mmLabel10
          AutoLabel.LabelPosition = lpLeft
        end
        object bNewInterval: TmmButton
          Left = 40
          Top = 16
          Width = 225
          Height = 25
          Caption = 'Sofortigen Intervallwechsel erzwingen'
          TabOrder = 0
          Visible = True
          OnClick = bNewIntervalClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object mmPanel10: TmmPanel
        Left = 0
        Top = 289
        Width = 945
        Height = 48
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 3
        object mmLineLabel3: TmmLineLabel
          Left = 0
          Top = 0
          Width = 945
          Height = 17
          Align = alTop
          AutoSize = False
          Caption = 'Speicher�berwachung'
          Distance = 2
        end
        object la1: TmmLabel
          Left = 195
          Top = 20
          Width = 99
          Height = 13
          Caption = 'Interval der Messung'
          FocusControl = spedMemoryGuardInterval
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object la2: TmmLabel
          Left = 352
          Top = 20
          Width = 5
          Height = 13
          Caption = 's'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object cbMemoryGuard: TmmCheckBox
          Left = 8
          Top = 16
          Width = 145
          Height = 17
          Caption = 'Speicher�berwachung'
          TabOrder = 0
          Visible = True
          OnClick = cbMemoryGuardClick
          AutoLabel.LabelPosition = lpLeft
        end
        object spedMemoryGuardInterval: TmmSpinEdit
          Left = 296
          Top = 16
          Width = 57
          Height = 22
          MaxValue = 0
          MinValue = 0
          TabOrder = 1
          Value = 30
          Visible = True
          OnChange = spedMemoryGuardIntervalChange
          AutoLabel.Control = la1
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object pa1: TmmPanel
        Left = 0
        Top = 337
        Width = 945
        Height = 168
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 4
        object la3: TmmLineLabel
          Left = 0
          Top = 0
          Width = 945
          Height = 17
          Align = alTop
          AutoSize = False
          Caption = 'LX Connection Test'
          Distance = 2
        end
        object laLxConnectenTestEnableMsg: TmmLabel
          Left = 0
          Top = 16
          Width = 520
          Height = 29
          Caption = 'Nur verf�gbar, wenn das System gestoppt ist'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 196
          Font.Height = -24
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object la5: TmmLabel
          Left = 89
          Top = 60
          Width = 67
          Height = 13
          Alignment = taRightJustify
          Caption = 'Anzahl Zyklen'
          FocusControl = edCycles
          Visible = True
          AutoLabel.Distance = 70
          AutoLabel.LabelPosition = lpRight
        end
        object la6: TmmLabel
          Left = 15
          Top = 84
          Width = 141
          Height = 13
          Alignment = taRightJustify
          Caption = 'Anzahl Threads pro Maschine'
          FocusControl = edThreadCount
          Visible = True
          AutoLabel.Distance = 70
          AutoLabel.LabelPosition = lpRight
        end
        object laCycle: TmmLabel
          Left = 8
          Top = 144
          Width = 63
          Height = 20
          Caption = 'Zyklus:-'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -16
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bStartConnecting: TmmButton
          Left = 8
          Top = 112
          Width = 209
          Height = 25
          Caption = 'Start Connect Test'
          TabOrder = 0
          Visible = True
          OnClick = bStartConnectingClick
          AutoLabel.LabelPosition = lpLeft
        end
        object paDescriptionPanel: TmmPanel
          Left = 600
          Top = 17
          Width = 345
          Height = 151
          Align = alRight
          BevelOuter = bvLowered
          TabOrder = 1
          object pa2: TmmPanel
            Left = 1
            Top = 1
            Width = 21
            Height = 149
            Align = alLeft
            BevelOuter = bvNone
            Color = clInfoBk
            TabOrder = 0
            object imInfoImage: TmmImage
              Left = 2
              Top = 0
              Width = 16
              Height = 16
              Picture.Data = {
                07544269746D617036030000424D360300000000000036000000280000001000
                0000100000000100180000000000000300000000000000000000000000000000
                0000E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF84695A8C695A
                E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FF
                FFE7FFFFE7FFFF94694ACE9E6BAD794A84695AE7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF94694ACEAE84FFF7C6946142
                84695AE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFF9471
                638C614AE7CFADFFDFB5FFDFB5AD86635228086B413184695AE7FFFFE7FFFFE7
                FFFFE7FFFFE7FFFFBDA69CBDA69CD6B69CE7CFADFFE7BDFFEFC6FFFFD6F7D7B5
                DEBE94AD8E635A281084695AE7FFFFE7FFFFE7FFFFCEB6A5EFDFC6EFD7BDFFEF
                CEFFD7B5C6714AEF9663DE8E5ADEA67BFFE7C6F7D7B5D6B6947341298C6963E7
                FFFFCEBEA5EFDFC6EFDFC6FFEFD6FFEFCEFFFFE7B586637B00008C2800F7E7C6
                FFEFCEFFE7C6F7D7B5DEBE9C5A2810E7FFFFC6B6A5EFDFC6FFF7D6FFEFD6FFE7
                CEFFFFEFCEB6946B0000A55931FFFFF7FFE7CEFFE7C6FFEFCEFFE7C6AD8E6B94
                716BCEBEADFFE7D6FFF7DEFFEFD6FFEFD6FFFFF7CEAE94730000A55131FFFFF7
                FFEFCEFFE7CEFFEFCEFFEFD6E7CFAD8C695ACEC7B5FFEFE7FFF7E7FFEFDEFFF7
                DEFFFFFFD6C7AD730000A55931FFFFFFFFEFD6FFEFD6FFEFD6FFF7DEEFD7C684
                695AD6C7BDFFF7EFFFFFEFFFF7E7FFFFEFEFE7DE8C38186300008C3821FFFFFF
                FFF7DEFFEFD6FFEFDEFFFFE7F7DFCE947973DED7CEF7F7EFFFFFFFFFF7EFFFFF
                FFE7DFCEB59684BDA69CC6AEA5FFFFFFFFF7E7FFEFDEFFFFEFFFFFFFE7CFB594
                7973E7FFFFE7E7E7FFFFFFFFFFFFFFFFF7FFFFFFFFFFFFFFCFA5FFFFEFFFFFFF
                FFF7EFFFFFEFFFFFFFFFFFFFAD9684E7FFFFE7FFFFEFE7E7EFEFEFFFFFFFFFFF
                FFFFFFFF8C514A5A00009C4129FFFFFFFFFFFFFFFFFFFFFFFFD6BEADE7FFFFE7
                FFFFE7FFFFE7FFFFF7EFE7F7F7F7FFFFFFFFFFFFD6CFCE634142C6B6ADFFFFFF
                FFFFFFFFFFFFEFDFD6E7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFE7FFFFEFE7
                DEF7F7EFFFFFFFFFFFFFFFFFFFFFFFF7DECFC6E7FFFFE7FFFFE7FFFFE7FFFFE7
                FFFF}
              Transparent = True
              Visible = True
              AutoLabel.LabelPosition = lpLeft
            end
          end
          object memoDescriptionMemo: TmmMemo
            Left = 22
            Top = 1
            Width = 322
            Height = 149
            Align = alClient
            BorderStyle = bsNone
            Color = clInfoBk
            Ctl3D = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'Funktionsbeschreibung:'
              ''
              'In einem Loop werden alle LZE'#39's die in der Datenbank '
              '(MM_Winding) erfasst sind Connected und dann wieder '
              'Disconnected.'
              'Dies geschieht Multithreaded. Der Test soll pr�fen ob die der '
              'Verbindungs Vorgang Threadsafe ist. Je mehr Maschinen '
              'angeschlossen sind, desto zuverl�ssiger ist der Test.'
              'Gepr�ft wird ob die DBHandels (LZE Connection Handle) pro '
              'Durchlauf unterschiedlich sind.')
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 1
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object edCycles: TmmEdit
          Left = 160
          Top = 56
          Width = 57
          Height = 21
          Color = clWindow
          TabOrder = 2
          Text = '1'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = la5
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          Decimals = 0
          MinValue = 1
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmExit, vmReadWrite]
        end
        object memoLXHandles: TmmMemo
          Left = 351
          Top = 17
          Width = 249
          Height = 151
          Align = alRight
          ScrollBars = ssVertical
          TabOrder = 3
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object edThreadCount: TmmEdit
          Left = 160
          Top = 80
          Width = 57
          Height = 21
          Color = clWindow
          TabOrder = 4
          Text = '10'
          Visible = True
          Alignment = taRightJustify
          AutoLabel.Control = la6
          AutoLabel.Distance = 4
          AutoLabel.LabelPosition = lpLeft
          Decimals = 0
          MaxValue = 1000
          MinValue = 1
          NumMode = True
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmExit, vmReadWrite]
        end
      end
    end
    object tabMemoryGuard: TTabSheet
      Caption = 'Speicher�berwachung'
      ImageIndex = 2
      object paChart: TmmPanel
        Left = 0
        Top = 0
        Width = 945
        Height = 609
        Align = alClient
        BevelOuter = bvNone
        Caption = 'Speicher�berwachung deaktiviert (siehe Tab Einstellungen)'
        TabOrder = 0
        object mmPanel7: TmmPanel
          Left = 806
          Top = 0
          Width = 139
          Height = 609
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object mmLabel8: TmmLabel
            Left = 0
            Top = 0
            Width = 104
            Height = 13
            Align = alTop
            Caption = '�berwachte Prozesse'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object memoMointoredProcesses: TmmMemo
            Left = 0
            Top = 13
            Width = 139
            Height = 596
            Align = alClient
            TabOrder = 0
            Visible = True
            OnChange = memoMointoredProcessesChange
            AutoLabel.LabelPosition = lpLeft
          end
        end
      end
    end
  end
  object mActionList: TmmActionList
    OnUpdate = mActionListUpdate
    Left = 192
    Top = 32
    object acStartCycle: TAction
      Caption = 'Start'
      OnExecute = acStartCycleExecute
    end
    object acStopCycle: TAction
      Caption = 'Stopp'
      OnExecute = acStopCycleExecute
    end
    object acShowInactiveLots: TAction
      Caption = 'acShowInactiveLots'
      OnExecute = acShowInactiveLotsExecute
    end
    object acHideInactiveLots: TAction
      Caption = 'acHideInactiveLots'
      OnExecute = acHideInactiveLotsExecute
    end
    object acOpenSuccessLog: TAction
      Category = 'Logfile'
      Caption = 'Logfile �ffnen'
      OnExecute = acOpenSuccessLogExecute
    end
    object acOpenErrorLog: TAction
      Category = 'Logfile'
      Caption = 'Error Log �ffnen'
      OnExecute = acOpenErrorLogExecute
    end
  end
  object mCycleTimer: TmmTimer
    Enabled = False
    OnTimer = mCycleTimerTimer
    Left = 312
    Top = 32
  end
  object mStopWatchTimer: TmmTimer
    Interval = 2000
    OnTimer = mStopWatchTimerTimer
    Left = 256
    Top = 32
  end
  object mAppMemoryTimer: TmmTimer
    Interval = 30000
    OnTimer = mAppMemoryTimerTimer
    Left = 184
    Top = 80
  end
  object mmADOConnection1: TmmADOConnection
    LoginPrompt = False
    DBName = 'MM_Winding'
    SQLServerName = 'wetsrvmm5'
    UserName = 'MMSystemSQL'
    Password = 'netpmek32'
    Left = 276
    Top = 176
  end
end
