Attribute VB_Name = "Modul1"
Option Explicit

Sub LogMMStress()
'
' LogMMStress Makro
' Makro am 11.11.2005 von Wiss Daniel aufgezeichnet

    Dim xFormula
    
    Range("I2").Select
    
    xFormula = "IF(RC[-8]=""begin"",INT((R[1]C[-7]-RC[-7]) * 60*60*24),"""")"
    ActiveCell.FormulaR1C1 = "=IF(" & xFormula & " > R2C14," & xFormula & ","""")"
    Selection.Copy
    Columns("I:I").Select
    ActiveSheet.Paste
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "Dauer"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "Min. Dauer"
    Range("N2").Select
    ActiveCell.FormulaR1C1 = "20"
    Range("A1").Select
    Selection.AutoFilter Field:=9, Criteria1:="<>"
End Sub
