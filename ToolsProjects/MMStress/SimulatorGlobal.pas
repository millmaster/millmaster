unit SimulatorGlobal;

interface

uses
  BaseGlobal, windows, Messages, filectrl, classes, sysutils, LoepfeGlobal;

type
  EMMStress = class(Exception);

  TLogFileWriter = class(TObject)
  private
    FLogFile: string;
    FMaxSizeKB: Integer;
    FMinimizedSizeKB: Integer;
    FTitle: string;
  public
    constructor Create(aLogFile: string; aMaxSize: integer; aMinimizedSize: integer);
    procedure AppendLogFile(aMsg: string);
  // Pr�ft ob das spezifizierte File die Gr�sse �berschreitet und setzt es auf die neue Gr�sse
    procedure CheckAndChangeFileSize;
    property LogFile: string read FLogFile write FLogFile;
    property MaxSizeKB: Integer read FMaxSizeKB write FMaxSizeKB;
    property MinimizedSizeKB: Integer read FMinimizedSizeKB write FMinimizedSizeKB;
    property Title: string read FTitle write FTitle;
  end;

  PPROCESS_MEMORY_COUNTERS = ^PROCESS_MEMORY_COUNTERS;
  PROCESS_MEMORY_COUNTERS = record
    cb : DWORD;
    PageFaultCount : DWORD;
    PeakWorkingSetSize : DWORD;
    WorkingSetSize : DWORD; //Task managers MemUsage number
    QuotaPeakPagedPoolUsage : DWORD;
    QuotaPagedPoolUsage : DWORD;
    QuotaPeakNonPagedPoolUsage : DWORD;
    QuotaNonPagedPoolUsage : DWORD;
    PagefileUsage : DWORD; //TaskMan's VM Size number
    PeakPagefileUsage : DWORD;
  end;
  TProcessMemoryCounters = PROCESS_MEMORY_COUNTERS;

function GetProcessMemoryInfo(Process : THandle; var MemoryCounters : PROCESS_MEMORY_COUNTERS; cb : DWORD) : BOOL; stdcall;
function ProcessMemoryUsage(ProcessID : DWORD): TProcessMemoryCounters;

function GetProcessMemoryCounters: TProcessMemoryCounters;


const
  WM_Syncronize = WM_APP + 1;

  cClosePipeName = 'GLB_Ass_Close' + cAddOnChannelName;

  cSuccessLogFileName = 'C:\Temp\PartieStartSimulator\Success.csv';
  cErrorLogFileName   = 'C:\Temp\PartieStartSimulator\Error.log';

//  cSQLDateTimeFormatString = 'yyyy''-''mm''-''dd';
  cSQLDateTimeFormatString = 'yyyy''-''mm''-''dd hh'':''nn'':''ss';
  cSetting = '<?xml version="1.0"?>' +
             ' <?MM MapFile="ZE-0002-S" ConverterVersion="1.06"?>' +
             ' <!--Additional LOEPFE Info: Creation date: 22.04.2005 15:20:17-->' +
             ' <LoepfeBody>' +
             '   <YMSetting>' +
             '     <Basic>' +
             '       <Channel>' +
             '         <Neps>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>1.5</Dia>' +
             '         </Neps>' +
             '         <Short>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>1.1</Dia>' +
             '           <Length>1</Length>' +
             '         </Short>' +
             '         <Long>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>1.04</Dia>' +
             '           <Length>6.1</Length>' +
             '         </Long>' +
             '         <Thin>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>0.94</Dia>' +
             '           <Length>5</Length>' +
             '         </Thin>' +
             '         <Splice>' +
             '           <Switch>swOn</Switch>' +
             '           <Length>6</Length>' +
             '         </Splice>' +
             '       </Channel>' +
             '       <Classification>CCCCCCCCUCUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU</Classification>' +
             '       <Splice>' +
             '         <Neps>' +
             '           <Switch>swOff</Switch>' +
             '           <Dia>1.5</Dia>' +
             '         </Neps>' +
             '         <Short>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>1.1</Dia>' +
             '           <Length>1</Length>' +
             '         </Short>' +
             '         <Long>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>1.04</Dia>' +
             '           <Length>6.1</Length>' +
             '         </Long>' +
             '         <Thin>' +
             '           <Switch>swOn</Switch>' +
             '           <Dia>0.94</Dia>' +
             '           <Length>5</Length>' +
             '         </Thin>' +
             '         <UpperYarn>' +
             '           <Switch>swOff</Switch>' +
             '           <Dia>1.04</Dia>' +
             '         </UpperYarn>' +
             '         <SeparateSpliceCheckSettings>True</SeparateSpliceCheckSettings>' +
             '       </Splice>' +
             '       <Cluster>' +
             '         <Short>' +
             '           <Switch>swOff</Switch>' +
             '           <Dia>1.1</Dia>' +
             '           <Length>1</Length>' +
             '           <ObsLength>0.1</ObsLength>' +
             '           <Defects>1</Defects>' +
             '           <Repetition>0</Repetition>' +
             '         </Short>' +
             '         <Long>' +
             '           <Switch>swOff</Switch>' +
             '           <Dia>1.04</Dia>' +
             '           <Length>6.1</Length>' +
             '           <ObsLength>0.1</ObsLength>' +
             '           <Defects>1</Defects>' +
             '           <Repetition>0</Repetition>' +
             '         </Long>' +
             '         <Thin>' +
             '           <Switch>swOff</Switch>' +
             '           <Dia>0.94</Dia>' +
             '           <Length>5</Length>' +
             '           <ObsLength>0.1</ObsLength>' +
             '           <Defects>1</Defects>' +
             '           <Repetition>0</Repetition>' +
             '         </Thin>' +
             '       </Cluster>' +
             '     </Basic>' +
             '     <Foreign>' +
             '       <F>' +
             '         <Dark>' +
             '           <Classification>UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU</Classification>' +
             '           <Cluster>' +
             '             <Switch>swOff</Switch>' +
             '             <ObsLength>0.1</ObsLength>' +
             '             <Defects>1</Defects>' +
             '             <Repetition>5</Repetition>' +
             '             <Classification>UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU</Classification>' +
             '           </Cluster>' +
             '         </Dark>' +
             '         <StartupRepetition>2</StartupRepetition>' +
             '       </F>' +
             '     </Foreign>' +
             '     <SFI>' +
             '       <Switch>swOff</Switch>' +
             '       <Reference>0</Reference>' +
             '       <Upper>5</Upper>' +
             '       <Lower>5</Lower>' +
             '       <Repetition>2</Repetition>' +
             '     </SFI>' +
             '     <Additional>' +
             '       <Yarn>' +
             '         <YarnCount>100</YarnCount>' +
             '         <YarnUnit>yuNm</YarnUnit>' +
             '         <ThreadCount>1</ThreadCount>' +
             '         <Count>' +
             '           <Switch>swOff</Switch>' +
             '           <PosDiaDiff>3</PosDiaDiff>' +
             '           <NegDiaDiff>3</NegDiaDiff>' +
             '           <Repetition>2</Repetition>' +
             '           <ObsLength>10</ObsLength>' +
             '         </Count>' +
             '         <ShortCount>' +
             '           <Switch>swOff</Switch>' +
             '           <PosDiaDiff>3</PosDiaDiff>' +
             '           <NegDiaDiff>3</NegDiaDiff>' +
             '           <Repetition>2</Repetition>' +
             '           <ObsLength>1</ObsLength>' +
             '         </ShortCount>' +
             '       </Yarn>' +
             '       <Adjust>' +
             '         <Reduction>0</Reduction>' +
             '         <Mode>0</Mode>' +
             '       </Adjust>' +
             '     </Additional>' +
             '     <Maintenance>' +
             '       <ConfigCode>' +
             '         <FSensorActive>False</FSensorActive>' +
             '         <DFSSensitivity1>True</DFSSensitivity1>' +
             '         <DFSSensitivity2>False</DFSSensitivity2>' +
             '         <SFSSensitivity>False</SFSSensitivity>' +
             '         <FClearingDuringSplice>False</FClearingDuringSplice>' +
             '         <BunchMonitor>True</BunchMonitor>' +
             '         <FAdjustAtOfflimit>True</FAdjustAtOfflimit>' +
             '         <FAdjustAfterAlarm>False</FAdjustAfterAlarm>' +
             '         <CutAtYarnBreak>False</CutAtYarnBreak>' +
             '       </ConfigCode>' +
             '     </Maintenance>' +
             '   </YMSetting>' +
             '   <Config>' +
             '     <Group>' +
             '       <ReinVer>37</ReinVer>' +
             '       <SensingHead>shTK830</SensingHead>' +
             '       <CutRetries>2</CutRetries>' +
             '       <ProdGrpID>-2147479286</ProdGrpID>' +
             '       <PilotSpindles>1</PilotSpindles>' +
             '       <Group>1</Group>' +
             '       <SpindleFrom>1</SpindleFrom>' +
             '       <SpindleTo>1</SpindleTo>' +
             '       <SpeedRamp>4</SpeedRamp>' +
             '       <Speed>900</Speed>' +
             '       <FPattern>' +
             '         <FP_ClassClearing>True</FP_ClassClearing>' +
             '         <FP_SpliceCheck/>' +
             '         <FP_ShortCount>True</FP_ShortCount>' +
             '         <FP_UpperYarnChannel>True</FP_UpperYarnChannel>' +
             '         <FP_Cluster>True</FP_Cluster>' +
             '         <FP_ExtendedCluster>True</FP_ExtendedCluster>' +
             '         <FP_SFI>True</FP_SFI>' +
             '         <FP_P>False</FP_P>' +
             '         <FP_SpeedSimulation>False</FP_SpeedSimulation>' +
             '         <FP_Zenit>False</FP_Zenit>' +
             '         <FP_FSpectraNormal>False</FP_FSpectraNormal>' +
             '         <FP_FSpectraHigh>False</FP_FSpectraHigh>' +
             '         <FP_FSpectraBD>False</FP_FSpectraBD>' +
             '         <FP_FZenitDark>False</FP_FZenitDark>' +
             '         <FP_FZenitBright>False</FP_FZenitBright>' +
             '         <FP_FClusterDark>False</FP_FClusterDark>' +
             '         <FP_FClusterBright>False</FP_FClusterBright>' +
             '       </FPattern>' +
             '       <ConfigCode>' +
             '         <CutBeforeAdjust>True</CutBeforeAdjust>' +
             '         <RemoveYarnAfterAdjust>False</RemoveYarnAfterAdjust>' +
             '         <ZeroTest>False</ZeroTest>' +
             '         <DriftCompensation>False</DriftCompensation>' +
             '         <KnifeMonitor>False</KnifeMonitor>' +
             '         <SpeedSimulation>False</SpeedSimulation>' +
             '         <BlockAtCutFailure>True</BlockAtCutFailure>' +
             '         <BlockAtClusterAlarm>True</BlockAtClusterAlarm>' +
             '         <BlockAtYarnCountAlarm>True</BlockAtYarnCountAlarm>' +
             '         <BlockAtSFIAlarm>True</BlockAtSFIAlarm>' +
             '         <BlockAtFAlarm>True</BlockAtFAlarm>' +
             '         <BlockAtFClusterAlarm>True</BlockAtFClusterAlarm>' +
             '         <BlockAtShortCountAlarm>True</BlockAtShortCountAlarm>' +
             '         <ShortSFID>False</ShortSFID>' +
             '         <SHZeroCut>True</SHZeroCut>' +
             '         <LongSpliceCheckLength>False</LongSpliceCheckLength>' +
             '         <Values>' +
             '           <ConfigCodeA>26</ConfigCodeA>' +
             '           <ConfigCodeB>32</ConfigCodeB>' +
             '           <ConfigCodeC>35</ConfigCodeC>' +
             '         </Values>' +
             '       </ConfigCode>' +
             '     </Group>' +
             '     <Machine>' +
             '       <YMSwVersion>V9.430 </YMSwVersion>' +
             '       <SWOption>swoLabPack</SWOption>' +
             '       <YMType>ftZE900</YMType>' +
             '       <MachType>255</MachType>' +
             '       <CheckLength>25</CheckLength>' +
             '       <LongStopDef>5</LongStopDef>' +
             '       <LengthWindow>1000</LengthWindow>' +
             '       <LengthMode>lwmFirst</LengthMode>' +
             '       <ConfigCode>' +
             '         <HeadstockRight>False</HeadstockRight>' +
             '         <UpperYarnCheck>False</UpperYarnCheck>' +
             '         <ExtendedInterfaceMurata>False</ExtendedInterfaceMurata>' +
             '         <ConeChangeConditionActive>True</ConeChangeConditionActive>' +
             '         <KnifeCutOffPowerHigh>False</KnifeCutOffPowerHigh>' +
             '       </ConfigCode>' +
             '     </Machine>' +
             '   </Config>' +
             ' </LoepfeBody>';

implementation
uses
  forms;

function GetProcessMemoryInfo; external 'psapi.dll';

function ProcessMemoryUsage(ProcessID : DWORD): TProcessMemoryCounters;
var
  ProcessHandle : THandle;
begin
  FillChar(result, sizeOf(TProcessMemoryCounters), 0);
  ProcessHandle := OpenProcess(PROCESS_QUERY_INFORMATION or PROCESS_VM_READ, false, ProcessID );
  try
    GetProcessMemoryInfo(ProcessHandle, result, sizeof(result))
  finally
    CloseHandle(ProcessHandle);
  end;
end;

function GetProcessMemoryCounters: TProcessMemoryCounters;
begin
  FillChar(result , sizeOf(result), 0);
  GetProcessMemoryInfo(GetCurrentProcess(), result, sizeof(result))
end;

constructor TLogFileWriter.Create(aLogFile: string; aMaxSize: integer; aMinimizedSize: integer);
begin
  inherited Create;

  fLogFile := aLogfile;
  FMaxSizeKB := aMaxSize;
  FMinimizedSizeKB := aMinimizedSize;
end;

(*---------------------------------------------------------
  Schreibt eine Meldung in ein Logfile
----------------------------------------------------------*)
procedure TLogFileWriter.AppendLogFile(aMsg: string);
var
  xMsg: string;
  xFileStream:TFileStream;       // Logfile als stream
const
  cMaxFileSize = 1000; // KB
  cMinFileSize = 750;  // KB
begin
  Assert(fLogFile > '', 'Logfile name has to be assigned');

  xMsg := aMsg + cCrlf;

  xFileStream := nil;
  try
    // Verzeichnisstruktur erzeugen
    if not(DirectoryExists(ExtractFilePath(fLogFile))) then
      ForceDirectories(ExtractFilePath(fLogFile));

    if FileExists(fLogFile) then begin            // Wenn die Datei bereits existiert...
      CheckAndChangeFileSize;

      xFileStream:=TFileStream.Create(fLogFile, fmOpenReadWrite or fmShareDenyWrite);//... dann Datei �ffnen
      xFileStream.Seek(0, soFromEnd);                       //Datenzeiger ans Ende der Datei
    end else begin                                //Wenn die Datei nicht existiert...
      xFileStream:=TFileStream.Create(fLogFile, fmCreate or fmShareDenyWrite);//... dann Datei erzeugen
      if FTitle <> '' then
        xFileStream.WriteBuffer(PAnsiString(FTitle)^, Length(FTitle));
    end;//if FileExists(Filename)then begin

    xFileStream.WriteBuffer(PAnsiString(xMsg)^, Length(xMsg)); // Art des Zugriffs in das Logfile schreiben
  finally
    xFileStream.Free;                                      // Datei schliessen
  end;// try finally
end;// procedure TfrmMainWindow.AppendLogFile(aMsg: string; aLogFile: string; aMaxKB: integer);

(*---------------------------------------------------------
  Stellt sicher, dass die Filegr�sse die angegebene Gr�sse nicht �berschreitet
    - Old Size ist die Gr�sse vor dem Abschneiden
    - NewSize ist die Gr�sse nach dem Abschneiden
----------------------------------------------------------*)
procedure TLogFileWriter.CheckAndChangeFileSize;
var
  SearchRec:TSearchRec;       // Suchergebnis
  xFileStream:TFileStream;     // entsprechende Datei
  xMemoryStream:TMemoryStream; // Buffer f�r die Kopieraktion

  xMaxSize,
  xNewSize: integer;
begin
  // Gr�ssen in Bytes
  xMaxSize := FMaxSizeKB * 1024;
  xNewSize := FMinimizedSizeKB * 1024;

  xFileStream   := nil;
  xMemoryStream := nil;
  try
    if FindFirst(fLogFile, 0, SearchRec) = ERROR_SUCCESS then begin
      // Wenn die Datei gr�sser ist als sie sein sollte
      if SearchRec.Size > xMaxSize then begin
        // ... Datei �ffnen
        xFileStream   := TFileStream.Create(fLogFile, fmOpenRead);
        // Buffer f�r das Kom^pieren der Datei
        xMemoryStream := TMemoryStream.Create;

        // "Zeiger" auf das erste Zeichen der neuen Datei setzen
        xFileStream.Position := SearchRec.Size - xNewSize;
        // Aus dem File in den Speicher kopieren
        xMemoryStream.CopyFrom(xFileStream, xNewSize);
        xMemoryStream.Position := 0;
        // Datei schliessen
        xFileStream.Free;

        // Datei wieder zum schreiben �ffnen
        xFileStream := TFileStream.Create(fLogFile, fmOpenWrite);
        xFileStream.Position := 0;
        // Aus dem Speicher in das File kopieren
        xFileStream.copyFrom(xMemoryStream, xMemoryStream.Size);
        // Gr�sse der Datei neu setzen
        xFileStream.size := xMemoryStream.Size;
      end;// if SearchRec.Size > xMaxSize then begin
    end;// if FindFirst(aPath, 0, SearchRec) = ERROR_SUCCESS then begin
  finally
    FindClose(SearchRec);
    FreeAndNil(xFileStream);
    FreeAndNil(xMemoryStream);
  end;// try finally
end;// procedure TfrmMainWindow.CheckAndChangeFileSize(aPath: string; aMaxKB, aMinimizedKB: integer);


end.
