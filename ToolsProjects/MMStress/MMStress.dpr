program MMStress;

uses
  MemCheck,
  Forms,
  MainWindow in 'MainWindow.pas' {frmMainWindow},
  SystemStuff in 'SystemStuff.pas',
  SimulatorGlobal in 'SimulatorGlobal.pas';

{$R *.RES}

begin
{$IFDEF MemCheck}
  MemChk('MMStress');
{$ENDIF}
  Application.Initialize;
  Application.CreateForm(TfrmMainWindow, frmMainWindow);
  Application.Run;
end.
