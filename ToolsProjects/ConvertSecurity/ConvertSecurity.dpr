{*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ConvertSecurity.pas
| Projectpart...: Convert security
| Subpart.......: -
| Process(es)...: -
| Description...: - Erstellt die Tab. t_security_work, t_security_default,
|                   t_security_default in Database, wenn nicht vorhanden.
|                 - Tab. t_security conevertieren nach den neuen Tabellen.
|
|                 - Erstellt von den neuen Tabellen die Queries
|                   im Verz. d:\MillMaster\queries\security -> aus Reg. lesen
| Info..........: -
| Develop.system: Win2k
| Target.system.: Win2k /XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.06.2003  1.00  SDo | File erstellt
|=============================================================================*}


program ConvertSecurity;
{$APPTYPE CONSOLE}
uses
  SysUtils, classes, Forms, FileCtrl,
  LoepfeGlobal, AdoDbAccess, Db, Dialogs, ActiveX,
  MMSecurity, MMSecurityConfig;

const
     cSQLSelect    = 'select * from t_security';
     cSectionForms = '[ControlledForms]';
     cSecurityTab  = 't_security_Work';

     //Tabellen
     cTableSecurity_Work = 't_security_Work';
     cTableSecurity_Default = 't_security_Default';
     cTableSecurity_Customer = 't_security_Customer';


     //Queries
     cQRY_Security_Work = 'Fill_' + cTableSecurity_Work + '.qry';
     cQRY_Security_Default = 'Fill_' + cTableSecurity_Default + '.qry';
     cQRY_Security_Customer = 'Fill_' + cTableSecurity_Customer + '.qry';

     cCreateSecurityTable  = 'use MM_Winding ' +
                             'if not exists (select id from sysobjects where name like ''%s'' ) ' +
                             'begin ' +
                             'create table %s ' +
                             '(c_Appl varchar(50) not null, ' +
                             'c_Form varchar(50) not null, ' +
                             'c_Caption varchar(70) not null, ' +
                             'c_Function varchar(50) not null, ' +
                             'c_Info varchar(100) not null, ' +
                             'c_Security varchar(500) ' +
                             'constraint pk_%s primary key clustered(c_Appl,c_Form,c_Function ) ) ' +
                             'end ';

var mErrorTxt: String;
    mLogList : TStringList;

//------------------------------------------------------------------------------
function  Sort_Forms(Item1, Item2: Pointer): Integer;
begin
 //Sortiert nach Forms
  if TSecurityRec(Item1^).Appl = TSecurityRec(Item2^).Appl  then begin
     if TSecurityRec(Item1^).ApplForm = TSecurityRec(Item2^).ApplForm  then
        result := 0
     else
       if TSecurityRec(Item1^).ApplForm < TSecurityRec(Item2^).ApplForm  then
          result := -1
       else
          result := 1;
  end else
      if TSecurityRec(Item1^).Appl < TSecurityRec(Item2^).Appl  then
         result := -1
      else
         result := 1;
end;
//------------------------------------------------------------------------------
function GetForm(aFormName: string; aControlList: TControlList; xDataList: TStringList): Boolean;
var
  i, xIndex: Integer;
  xNrOfComp: Integer;
begin
  Result := False;
  // first clear previous stored information in destination list
  aControlList.Clear;
  // are there some security info for this form? Check for block [aFormName].
  // [aForm]    <<--
  // 1
  // FormCaption
  // Control1 ## Control1 comment @@ Grouplist
  xIndex := xDataList.IndexOf(GetSectionString(aFormName));
  if xIndex >= 0 then begin
    // [aForm]      <-- xIndex
    // 1            <-- inc(xIndex)
    // FormCaption
    // Control1 ## Control1 comment @@ Grouplist
    inc(xIndex); // seek to NrOfComponents and read
    try
      try
        // get number of components and copy all lines
        xNrOfComp := StrToInt(xDataList.Strings[xIndex]);
      except
        Exit; // there is something wrong in the format of text block
      end;
      // [aForm]
      // 1
      // FormCaption
      // Control1 ## Control1 comment @@ Grouplist   <---- inc(xIndex, 2)
      inc(xIndex, 1); // seek to first component list
      for i := 0 to xNrOfComp-1 do begin
        aControlList.AddControlGroupString(xDataList.Strings[xIndex]);
        inc(xIndex);
      end;

      // succeeded only on end of filling
      Result := True;
    except
    end;
  end;
end;
//------------------------------------------------------------------------------
function ConvertSecurityTabTo(aTable: String):Boolean;
var xList, xForms :TStringList;
    x, n, i: Integer;
    xData, xSQL, xTable, xMSG : String;
    xAppl, xForm, xFunction, xInfo, xSecurity :String;
    xRet : Boolean;
    xRec          : PSecurity;
    xControlList  : TControlList;
    xSecurList    : TList;   //Liste mit allen aufbereiteten controlled Actions
begin

  xRet := TRUE;

  xList :=  TStringList.Create;
  xList.Sorted:= FALSE;

  xForms :=  TStringList.Create;
  xForms.Sorted:= TRUE;

  xControlList:= TControlList.Create;
  xSecurList := TList.Create;

  xTable:= aTable;

  with TAdoDBAccess.Create(2) do begin
      try
        if Init then begin
          with Query[cPrimaryQuery] do begin
            SQL.Text := cSQLSelect;
            try
              Open;
              First;

              while not Eof do begin

                  xList.clear;
                  xForms.clear;

                  //Appl. Name
                  xAppl := FieldByName('c_applicationname').asString;
                  {
                  if CompareText( xAppl , 'MMStyle') = 0 then begin
                     beep;
                     xData :=  FieldByName('c_text').asString;
                  end;
                  }

                  //Data (MemoField)
                  xData := StringReplace( FieldByName('c_text').asString,
                                           #$D#$A,
                                          '$',
                                          [rfReplaceAll] );  

                  //Data in Liste schreiben (Zeilenumbruch = neuer Eintrag)
                  //-> MemoField nachbilden
                  i:= length(xData);
                  n:=0;
                  while n <= i do begin
                     x:= pos('$', xData);
                     if x > 0 then begin
                        xList.Add( copy(xData,1, x-1)   );
                        delete(xData,1, x);
                        inc(n,x);
                     end else begin
                        xList.Add(xData ); //Letztes Element
                        break;
                     end;
                  end;

                  
                  //Alle kontrollierte Forms ermitteln
                  x := xList.IndexOf(cSectionForms);

                  if x < 0 then begin
                     xMSG := Format('No controlled form foud in %s. ' +
                                    'Check table %s and application %s.',
                                    ['t_Security', 't_Security', xAppl] );

                     mLogList.Add('-  ' + xMSG);
                     xRet := FALSE;
//                     MessageDlg( xMSG , mtWarning, [mbOK], 0);
                  end;

                  if x >= 0 then
                     xForms.CommaText := xList.Strings[x + 1];

                  for x := 0 to xForms.Count - 1 do begin
                    xForm := xForms.Strings[x];

                    // Form-Caption ermitteln
                    //[ControlledForms]
                    //Forms
                    //[Form]       <-- Index n
                    //DataCount
                    //FormCaption  <-- inc(x, 2)
                    //Data         <-- GetForm()
                    n := xList.IndexOf(GetSectionString(xForm));
                    inc(n, 2);

                    //Form-Daten ermitteln
                    GetForm(xForm, xControlList, xList);
                    // bei 1 starten, da hier FormCaption nicht ben�tigt wird
                    for n := 0 to xControlList.Count - 1 do begin
                      new(xRec);

                      xRec.Appl         := xAppl;
                      xRec.ApplForm     := xForm;
                      xRec.FormCaption  := xForm;
                      xRec.ApplFunction := xControlList.Name[n];
                      xRec.Info         := xControlList.Comment[n];

                      xSecurity         := xControlList.GetControlGroupString(n);
                      i:= Pos('@@', xSecurity);
                      delete( xSecurity, 1, i+1 );
                      xRec.SecurityGroups := xSecurity;

                      xSecurList.Add(xRec);
                    end;
                  end;
                next;
              end; //END While

              //Sortieren
              xSecurList.Sort(Sort_Forms);

              //Daten auf DB schreiben
              for n:= 0 to xSecurList.Count-1 do begin
                  xRec       := xSecurList.Items[n];
                  xAppl      := Trim(xRec.Appl);
                  xForm      := Trim(xRec.ApplForm);
                  xFunction  := Trim(xRec.ApplFunction);
                  xInfo      := Trim(xRec.Info);
                  xSecurity  := Trim(xRec.SecurityGroups);

                  xSQL := Format('insert into %s (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security) ' +
                                 ' VALUES(''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'') ',
                                 [xTable, xAppl, xForm, xFunction, xForm, xInfo, xSecurity]);

                  with Query[cSecondaryQuery] do begin
                       close;
                       SQL.Text := xSQL;
                       try
                         //Insert
                         ExecSQL;
                       except
                         //Update
                         xSQL := Format('update %s set c_Security = ''%s'' ' +
                                        'where c_Appl =  ''%s'' and c_Form =  ''%s'' and c_Function = ''%s'' ',
                                        [xTable, xSecurity, xAppl, xForm, xFunction] );
                         SQL.Text := xSQL;
                         try
                           ExecSQL;
                         except
                         end;
                       end;
                  end;
              end;

              close;
            except
                on e: Exception do begin
                  xRet := FALSE;
                end;
            end;
           end;
        end;

      except
        on E: Exception do begin
           //ShowMessage(e.Message);
           xRet := FALSE;
        end;

      end;
      free;
  end;

  xSecurList.Free;
  xControlList.Free;
  xList.free;
  xForms.free;


  Result := xRet;
end;
//------------------------------------------------------------------------------
function CreateAQuery(aTable: String): Boolean;
var xAppl, xForm, xFunction, xCaption, xInfo, xSecurity, xText, xTxt, xTable : String;
    xList : TStringList;
    xPath, xFileName, xMSG, xSQL, xSQL0, xSQLInsert, xSQLUpdate : String;
    xChar : String[1];
begin


 xFileName := aTable;
 xTable    := aTable;


 delete(xFileName ,1,1);

 xFileName := StringReplace(xFileName, '_', '', [rfReplaceAll]);

 xFileName := xFileName + '.qry';

 xChar :=  xFileName[1];
 xChar := UpperCase( xChar );
 xFileName[1] := xChar[1] ;


 xAppl := Application.ExeName;
 xAppl := ExtractFileName(xAppl);

 xList := TStringList.create;


 //Header
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

 xText := '/*------------------------------------------------------------------------------------------------';
 xList.Add(xText);

 xText := 'Description: Write MM security in to table ' + xTable;

 xList.Add(xText);

 xText := 'File crated by ' + xAppl;
 xList.Add(xText);

 xText := 'Date : ' + DateTimeToStr(Now);
 xList.Add(xText);

 xText := '------------------------------------------------------------------------------------------------*/';
 xList.Add(xText);

 xText := '';
 xList.Add(xText);
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


 xText := 'use MM_Winding';
 xList.Add(xText);

 xText := 'go';
 xList.Add(xText);

 xSQL := Format( 'select * from %s order by c_Appl',[xTable]);

 try
    with TAdoDBAccess.Create(1, false) do begin
          if Init then
             with Query[cPrimaryQuery] do begin
                SQL.Text := xSQL;
                Open;
                First;
                //if rbDefaultQuery.Checked then xTable := cTableSecurity_Default;

                while not Eof do begin
                    xAppl     := FieldByName('c_Appl').asString;
                    xForm     := FieldByName('c_Form').asString;
                    xFunction := FieldByName('c_Function').asString;
                    xCaption  := FieldByName('c_Caption').asString;
                    xInfo     := FieldByName('c_Info').asString;
                    xSecurity := FieldByName('c_Security').asString;


                    xSQL0:= Format(' if ( select count(c_Appl) from %s ' +
                                        'where  c_Appl = ''%s'' ' +
                                        'and c_Form    = ''%s'' ' +
                                        'and c_Function =''%s'') < 1 ' ,
                                        [xTable, xAppl, xForm, xFunction]);


                    xSQLInsert:= Format('insert into %s (c_Appl, c_Form, c_Function, c_Caption, c_Info, c_Security) ' +
                                        'values (''%s'', ''%s'', ''%s'', ''%s'', ''%s'', ''%s'')',
                                       [xTable, xAppl, xForm, xFunction, xCaption, xInfo, xSecurity]);

                    xSQLUpdate:= Format('update %s set c_Security = ''%s'', c_Caption = ''%s'', c_Info = ''%s'' ' +
                                    'where c_Appl = ''%s'' and  c_Form = ''%s'' and c_Function = ''%s'' ',
                                    [xTable, xSecurity, xCaption, xInfo, xAppl, xForm, xFunction] );

                    xSQL :=  Format(' %s %s else %s',[xSQL0, xSQLInsert, xSQLUpdate]);

                    xList.Add(xSQL);

                    next;
                end;
             end;
           Free;
     end;


 //Footer
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     xTxt := 'Insert / Update';
     xText:='';
     xList.Add(xText);

     xText := 'Print''------------------------------------------------------------------------------------------------'' ';
     xList.Add(xText);

     xText := Format('Print''%s security into %s on DB MM_Winding done on ''+RTRIM(CONVERT(varchar(30), GETDATE())) ', [xTxt, xTable]);
     xList.Add(xText);

     xText := 'Print''------------------------------------------------------------------------------------------------'' ';
     xList.Add(xText);
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

     xPath := GetRegString(cRegLM, cRegMillMasterPath, cRegMillMasterRootPath, 'c:\temp');
     xPath := xPath + '\Queries\Security';

     ForceDirectories(xPath);

     xPath := xPath +  '\'+ xFileName;
     xList.SaveToFile( xPath );

     xMSG:= Format('Create table %s to %s',[xTable, xPath ]);
     mLogList.Add('-  ' + xMSG);

 except
     on e: Exception do begin
        xMSG := Format('Error: %s', [e.message] );
        MessageDLG(xMSG, mtError	,[mbOk], 0);
     end;
 end;
 xList.Free;

end;
//------------------------------------------------------------------------------




//******************************************************************************
//Ab hier beginnt das Programm
//******************************************************************************
var xSQL, xMSG :String;

    xCount : Integer;
    xRet : Boolean;
    xTables : String;
    xQRYPath :String;
    xKey, xKeyName, xValue :String;

begin


 xTables := '';
 try

   mLogList := TStringList.Create;

   xMSG := 'Begin convering at ' + DateTimeTostr(Now) ;
   mLogList.Add(xMSG);
   mLogList.Add('');

   xValue := '';

   xKey     := 'SOFTWARE\Loepfe\MillMaster';
   xKeyName := 'MillMasterRootPath';

   xValue := GetRegString(cRegLM, xKey, xKeyName, '');
   if xValue = '' then
      xQRYPath := 'd:\MillMaster\Queries\Security'
   else
      xQRYPath := xValue + '\Queries\Security';

   CoInitialize(nil);

   with TAdoDBAccess.Create(1, false) do begin

      if Init then begin
         try
           with Query[cPrimaryQuery] do begin

                //Tabellen erstellen
                //++++++++++++++++++
                xMSG := Format('Connect to database %s on host %s ',[DBName, HostName]);
                mLogList.Add('-  ' + xMSG);

                xTables :=  cTableSecurity_Work;
                xSQL := Format(cCreateSecurityTable, [cTableSecurity_Work, cTableSecurity_Work, cTableSecurity_Work] );
                SQL.Text := xSQL;
                ExecSQL;

                xTables := xTables + '; ' + cTableSecurity_Default;
                xSQL := Format(cCreateSecurityTable, [cTableSecurity_Default, cTableSecurity_Default, cTableSecurity_Default] );
                SQL.Text := xSQL;
                ExecSQL;


                xTables := xTables + '; ' + cTableSecurity_Customer;
                xSQL := Format(cCreateSecurityTable, [cTableSecurity_Customer, cTableSecurity_Customer, cTableSecurity_Customer] );
                SQL.Text := xSQL;
                ExecSQL;

                xMSG := Format('Tables %s created',[xTables]);
                mLogList.Add('-  ' + xMSG);
                xRet := TRUE;

                try
                  SQL.Text := 'SELECT count(c_applicationname) appl FROM  t_security';
                  open;
                  xCount:= FieldByName('appl').AsInteger;
                except
                  xCount := 0;

                  xMSG := 'Table t_security does not exists';
                  mLogList.Add('-  ' + xMSG);
                end;

                //Close;

                //Table t_security exists
                if xCount > 0 then begin

                    //Tab. t_security nach neuen Tabellen convertieren
                    //++++++++++++++++++++++++++++++++++++++++++++++++
                    xTables := cTableSecurity_Work;
                    if ConvertSecurityTabTo(xTables) then
                       xMSG := Format('Convert table t_Security to %s successful.', [xTables] )
                    else
                       xMSG := Format('Convert table t_Security to %s failed.', [xTables] );
                    mLogList.Add('-  ' + xMSG);

                    xTables := cTableSecurity_Default;
                    if ConvertSecurityTabTo(xTables) then
                       xMSG := Format('Convert table t_Security to %s successful.', [xTables] )
                    else
                       xMSG := Format('Convert table t_Security to %s failed.', [xTables] );
                    mLogList.Add('-  ' + xMSG);


                    xTables := cTableSecurity_Customer;
                    if ConvertSecurityTabTo(xTables) then
                       xMSG := Format('Convert table t_Security to %s successful.', [xTables] )
                    else
                       xMSG := Format('Convert table t_Security to %s failed.', [xTables] );
                    mLogList.Add('-  ' + xMSG);
                end;

                CreateAQuery(cTableSecurity_Work);

                CreateAQuery(cTableSecurity_Default);

                CreateAQuery(cTableSecurity_Customer);
            end;

          except
            on e: Exception do begin
              xMSG := Format('Error in CreateTables : %s', [e.message] );
              mLogList.Add('-  ' + xMSG);
              MessageDLG(xMSG, mtError	,[mbOk], 0);
              xRet := FALSE;
            end;
          end;

       end;

       Free;

   end; //END with TAdoDBAccess.Create(1, false) do begin




 except
    on e: Exception do begin
       xMSG := Format('Error in TAdoDBAccess.Create : %s', [e.message] );
       mLogList.Add('-  ' + xMSG);
       MessageDLG(xMSG, mtError	,[mbOk], 0);
       xRet := FALSE;
     end;
 end;


 mLogList.Add('');
 xMSG := 'END convering';
 mLogList.Add('-  ' + xMSG);
 
 GetDir(0,xMSG);
 xMSG := xMSG + '\ConvertSecurity.log';
 mLogList.SaveToFile(xMSG );


 CoUninitialize;
 mLogList.Free;

end.