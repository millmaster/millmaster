Convert Security
****************

Datum: 06.06.2003

Programmtyp:	Konsolenprogramm f�r den Kunden
 
Aufgabe:	Der Kunde kann ab MM-Version 3.0.x die Tabelle t_Security in die neue
		Tabellenstruktur konvertieren. 

		Es werden die Tabellen t_Security_Work, t_Security_Default, 
		t_Security_Custom erstellt, wenn nicht vorhanden.
		Konvertierung der Tabelle t_Security in die neuen Tabellen.
		Erstellung von Queries der neuen Tabellen nach \MillMaster\queries\security

Aufwand:	2-3 h

