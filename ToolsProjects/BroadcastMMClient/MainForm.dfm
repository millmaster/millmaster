object Form1: TForm1
  Left = 351
  Top = 126
  Width = 424
  Height = 332
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object edDomains: TmmEdit
    Left = 40
    Top = 24
    Width = 121
    Height = 21
    Color = clWindow
    TabOrder = 0
    Text = 'wetpc1148'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
    Decimals = 0
    NumMode = False
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object bSend: TmmButton
    Left = 40
    Top = 56
    Width = 75
    Height = 25
    Caption = 'Send'
    TabOrder = 1
    Visible = True
    OnClick = bSendClick
    AutoLabel.LabelPosition = lpLeft
  end
  object meMsg: TmmMemo
    Left = 40
    Top = 88
    Width = 249
    Height = 209
    TabOrder = 2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object rgMMState: TmmRadioGroup
    Left = 216
    Top = 8
    Width = 185
    Height = 73
    Caption = 'MMState'
    ItemIndex = 0
    Items.Strings = (
      'Stoped'
      'Running'
      'Alarm')
    TabOrder = 3
  end
  object bPing: TmmButton
    Left = 128
    Top = 56
    Width = 75
    Height = 25
    Caption = 'bPing'
    TabOrder = 4
    Visible = True
    OnClick = bPingClick
    AutoLabel.LabelPosition = lpLeft
  end
end
