unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Mailslot, StdCtrls, ExtCtrls, mmRadioGroup, mmMemo, mmButton, mmEdit, ICMP,
  IdIcmpClient;

type
  TForm1 = class(TForm)
    edDomains: TmmEdit;
    bSend: TmmButton;
    meMsg: TmmMemo;
    rgMMState: TmmRadioGroup;
    bPing: TmmButton;
    procedure bSendClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bPingClick(Sender: TObject);
  private
    mPing: TIdIcmpClient;
//    mPing: TPing;
    procedure PingPing(sender: TObject; status, ip, roundtime: Integer);
    procedure IdPingEvent(ASender: TComponent; const AReplyStatus: TReplyStatus);
  public
  end;

var
  Form1: TForm1;

implementation
{$R *.DFM}
uses
  BaseGlobal, LoepfeGlobal;

procedure TForm1.bSendClick(Sender: TObject);
var
  xMsg         : TMMClientRec;
  xDomainNames : string;
begin
  case rgMMState.ItemIndex of
    0: xMsg.MsgTyp := ccMMStopped;
    1: xMsg.MsgTyp := ccMMStarted;
    2: xMsg.MsgTyp := ccMMAlert;
  end;
  xMsg.DateTime   := Now;
  xMsg.UserStart  := True;
  xMsg.ServerName := gMMHost;

  //Nue 21.3.01    xMsg.Error := SetError ( 0, etNoError, '' );
  with TBroadcaster.Create(edDomains.Text,  cChannelNames[ttMMClientReader]) do
  try
    if Init() then begin
      meMsg.Lines.Add('Init done');
      if Write(@xMsg, sizeof(xMsg)) then
        meMsg.Lines.Add('Write done')
      else
        meMsg.Lines.Add('Broadcast failed: ' + FormatErrorText(Error) + ' ' + ErrorInformation);
    end else
      meMsg.Lines.Add('Init Falied: ' + FormatErrorText(Error));
  finally
    Free;
  end;

//  if not(assigned(mBroadClient)) then begin
//    xDomainNames  := TMMSettingsReader.Instance.Value[cDomainNames];
//    mBroadClient  := TBroadcaster.Create ( xDomainNames,  cChannelNames[ttMMClientReader] );
//    if not mBroadClient.Init(10) then begin
//      raise Exception.Create ( 'Initialisation of client broadcast failed. Error :'
//                               + FormatErrorText ( mBroadClient.Error)
//                               + ' --- Aditional Informations ---'
//                               + mBroadClient.ErrorInformation);
//    end;
//  end;//

end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  mPing := TIdIcmpClient.Create(Self);
  mPing.ReceiveTimeout := 1000;
  mPing.OnReply := IdPingEvent;

//  mPing := TPing.Create(Self);
//  mPing.OnPing := PingPing;
end;

procedure TForm1.bPingClick(Sender: TObject);
begin
  mPing.Host := edDomains.Text;
  mPing.Ping();
//  mPing.Hostname := edDomains.Text;
//  mPing.action;
  meMsg.lines.add('Ping started')
end;

procedure TForm1.PingPing(sender:TObject; status:Integer; ip,roundtime:Longint);
begin
  case status of
    ip_success:
      meMsg.lines.add(inttostr(roundtime)+'ms');
    else
      meMsg.lines.add('Failed')
    end;
end;

procedure TForm1.IdPingEvent(ASender: TComponent; const AReplyStatus: TReplyStatus);
begin
  if AReplyStatus.ReplyStatusType = rsEcho then
    meMsg.lines.add('Echo: ' + IntToStr(AReplyStatus.MsRoundTripTime))
  else
    meMsg.lines.add('Timeout: ' + IntToStr(AReplyStatus.MsRoundTripTime));
end;

end.
