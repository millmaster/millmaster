unit CorrQuery20401;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mmDataSource, StdCtrls, mmMemo, Mask, DBCtrls, mmDBEdit, mmLabel,
  DBTables, mmQuery, mmDatabase, mmButton;

type
  TForm1 = class(TForm)
    mmButton1: TmmButton;
    mmDatabase1: TmmDatabase;
    mmQuery1: TmmQuery;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmDBEdit1: TmmDBEdit;
    mmDBEdit2: TmmDBEdit;
    mmMemo1: TmmMemo;
    mmQuery2: TmmQuery;
    mmQuery3: TmmQuery;
    mmDataSource1: TmmDataSource;
    mmDataSource2: TmmDataSource;
    procedure mmButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}

procedure TForm1.mmButton1Click(Sender: TObject);
begin
  mmQuery1.ExecSQL;
  mmQuery2.Active := False;
  mmQuery3.Active := False;
  mmQuery2.Active := True;
  mmQuery3.Active := True;
end;

end.
