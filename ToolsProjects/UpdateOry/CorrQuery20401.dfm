object Form1: TForm1
  Left = 284
  Top = 209
  Width = 365
  Height = 217
  Caption = 'CorrectionQuery for MM_Winding 2.04.01'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 67
    Top = 124
    Width = 123
    Height = 13
    Caption = 'Name length in Prodgroup'
    FocusControl = mmDBEdit1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 53
    Top = 156
    Width = 137
    Height = 13
    Caption = 'Name length in OrderPosition'
    FocusControl = mmDBEdit2
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmButton1: TmmButton
    Left = 48
    Top = 16
    Width = 161
    Height = 73
    Caption = 'START'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -27
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = True
    OnClick = mmButton1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mmDBEdit1: TmmDBEdit
    Left = 192
    Top = 120
    Width = 41
    Height = 21
    Color = clWindow
    DataField = 'length'
    DataSource = mmDataSource1
    TabOrder = 1
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mmDBEdit2: TmmDBEdit
    Left = 192
    Top = 152
    Width = 41
    Height = 21
    Color = clWindow
    DataField = 'length'
    DataSource = mmDataSource2
    TabOrder = 2
    Visible = True
    AutoLabel.Control = mmLabel2
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mmMemo1: TmmMemo
    Left = 224
    Top = 16
    Width = 113
    Height = 65
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    Lines.Strings = (
      'Values after '
      'pushing START '
      'should be 50 !')
    ParentFont = False
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmDatabase1: TmmDatabase
    AliasName = 'MM_WindingODBC'
    Connected = True
    DatabaseName = 'DB'
    LoginPrompt = False
    Params.Strings = (
      'USER NAME=MMSystemSQL'
      'PASSWORD=netpmek32')
    SessionName = 'Default'
    Left = 8
    Top = 16
  end
  object mmQuery1: TmmQuery
    DatabaseName = 'DB'
    SQL.Strings = (
      
        'ALTER TABLE t_order_position ALTER COLUMN c_order_position_name ' +
        'varchar(50)'
      
        'ALTER TABLE t_prodgroup ALTER COLUMN c_order_position_name varch' +
        'ar(50)')
    Left = 8
    Top = 56
  end
  object mmQuery2: TmmQuery
    Active = True
    DatabaseName = 'DB'
    SQL.Strings = (
      
        'select length from syscolumns where name like '#39'c_order_position_' +
        'name'#39
      
        '  and id=(select id from sysobjects where name like '#39't_prodgroup' +
        #39')')
    Left = 232
    Top = 120
  end
  object mmQuery3: TmmQuery
    Active = True
    DatabaseName = 'DB'
    SQL.Strings = (
      
        'select length from syscolumns where name like '#39'c_order_position_' +
        'name'#39
      
        '  and id=(select id from sysobjects where name like '#39't_order_pos' +
        'ition'#39')')
    Left = 232
    Top = 152
  end
  object mmDataSource1: TmmDataSource
    DataSet = mmQuery2
    Left = 264
    Top = 120
  end
  object mmDataSource2: TmmDataSource
    DataSet = mmQuery3
    Left = 264
    Top = 152
  end
end
