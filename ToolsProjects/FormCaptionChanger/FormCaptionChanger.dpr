{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FormCaptionChanger.pas
| Projectpart...: Tool
| Subpart.......: -
| Process(es)...: -
| Description...: Convertiert den Caption-Text mit dem Caption-Native-Text, wenn
|                 Caption-Text = Form-Text ist.
|                 Es Tab. t_Security_work, t_Security_default,
|                 t_Security_customer bearbeitet.
| Info..........:
|
| Develop.Installation: Windows NT 4.0
| Target.Installation.: Windows NT / W2k /XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 19.12.2002  1.00  SDo | File created
===============================================================================}

program FormCaptionChanger;
{$APPTYPE CONSOLE}
uses
  SysUtils, AdoDBAccess, ActiveX, RzCSIntf;

type
  TFormCaptionRec = record
    Form    : string;
    Caption : string;
  end;

const
  cFormCaption : Array[0..21] of TFormCaptionRec = (
    //WindingMaster
    (Form     : 'AskClose';
     Caption  : '(50)Stopp von Datenerfassung bestaetigen'; ),

    (Form     : 'Assign';
     Caption  : '(*)Zuordnungen'; ),

    (Form     : 'dwInsMachNode';
     Caption  : '(*)Formular Maschine/Netzknoten'; ),

    (Form     : 'EditExceptDaysDlg';
     Caption  : '(*)Betriebskalender'; ),

    (Form     : 'EditShiftDlg';
     Caption  : '(*)Schichtkalender'; ),

    (Form     : 'Esp_MachineReportDlg';
     Caption  : '(50)Maschinenbericht'; ),

    (Form     : 'Esp_SpindleReportDlg';
     Caption  : '(*)Spulstellen-Bericht'; ),

    (Form     : 'FMMClientMain';
     Caption  : '(*)MMClient'; ),

    (Form     : 'LotParameter';
     Caption  : '(*)Partie Eigenschaften'; ),

    (Form     : 'MaConfigMain';
     Caption  : '(*)Maschinen Konfiguration'; ),

    (Form     : 'OffLimitEditorDlg';
     Caption  : '(100)Offlimit-Einstellungen bearbeiten'; ),

    (Form     : 'OffReportDlg';
     Caption  : '(100)Maschinen-Offlimit Bericht'; ),

    (Form     : 'ProdGrpClose';
     Caption  : '(*)Produktionsgruppen'; ),

    (Form     : 'SetupMain';
     Caption  : '(*)Millmaster Konfiguration'; ),

    (Form     : 'Template';
     Caption  : '(*)Vorlagen Verwaltung fuer Reiniger Einstellungen'; ),

    //LabMaster
    (Form     : 'frmEditAssortment';
     Caption  : '(*)Sortimentstamm bearbeiten'; ),

    (Form     : 'frmEditStyle';
     Caption  : '(*)Artikelstamm bearbeiten'; ),

    (Form     : 'frmViewStyle';
     Caption  : '(*)Artikelliste betrachten'; ),

    (Form     : 'QOAdmin';
     Caption  : '(*)Q-Offlimit Einstellungen'; ),

    (Form     : 'QOGenerator';
     Caption  : '(*)Manuelles generieren von Q-Offlimits'; ),

    (Form     : 'QOPresentation';
     Caption  : '(*)Q-Offlimit Auswertung'; ),

    (Form     : 'Template';
     Caption  : '(*)Vorlagen Verwaltung fuer Reiniger Einstellungen'; )
);


var  xAdoDBAccess : TAdoDBAccess;
     x: Integer;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
     function ChangeCapions(aTable :String; aAccess: TAdoDBAccess):Boolean;
     var xRet : Boolean;
     begin
       xRet := FALSE;
       with xAdoDBAccess.Query[1] do begin
         try
             for x:= 0 to high(cFormCaption) do begin
                 Close;
                 SQL.Text := Format('update %s set c_Caption = ''%s'' ' +
                                    'where c_Form = ''%s'' and c_Caption = ''%s'' ',
                                    [aTable, cFormCaption[x].Caption, cFormCaption[x].Form, cFormCaption[x].Form] );
                 ExecSQL;
                 xRet := TRUE;
             end;
         except
           on E: Exception do begin
              result := FALSE;
              CodeSite.SendError('Error in FormCaptionChanger.exe : Error msg : ' + e.Message);
              exit;
           end;
         end;
       end;
       result := xRet;
     end;
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
begin

  try
    CoInitialize(NIL);
    xAdoDBAccess:=  TAdoDBAccess.Create(2);
    xAdoDBAccess.Init;

    ChangeCapions('t_Security_work', xAdoDBAccess);
    ChangeCapions('t_Security_Default', xAdoDBAccess);
    ChangeCapions('t_Security_Customer', xAdoDBAccess);

    xAdoDBAccess.free;
  finally
    CoUninitialize;
  end;

end.