program CMDPause;
{$APPTYPE CONSOLE}
uses
  SysUtils, Windows;

begin
  if ParamCount = 0 then
    Sleep(1000)
  else
    Sleep(StrToIntDef(ParamStr(1), 1) * 1000);
end.
