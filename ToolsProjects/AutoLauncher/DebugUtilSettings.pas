(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: -
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.10.1998  0.00  Mg | Datei erstellt
| 30.03.1999  1.00  Mg | Default value of timeoutsupport is new true
|=========================================================================================*)

unit DebugUtilSettings;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TfrmSettings = class(TForm)
    GroupBox1: TGroupBox;
    cbKillByExit: TCheckBox;
    cbNoTimeout: TCheckBox;
    edTimeout: TEdit;
    Label1: TLabel;
    bOk: TButton;
    bCancel: TButton;
    procedure FormShow(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure bOkClick(Sender: TObject);
    procedure edTimeoutChange(Sender: TObject);
    procedure cbNoTimeoutClick(Sender: TObject);
  private
    { Private declarations }
    fKillByExit : boolean;
    fNoTimeout  : boolean;
    fTimeout : integer;
  public
    { Public declarations }
    constructor Create ( aOwner : TComponent ); override;
    property KillByExit : boolean read fKillByExit write fKillByExit;
    property NoTimeout : boolean read fNoTimeout write fNoTimeout;
    property Timeout : integer read fTimeout write fTimeout;
  end;

var
  frmSettings: TfrmSettings;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

{ TForm3 }

constructor TfrmSettings.Create(aOwner: TComponent);
begin
  inherited Create ( aOwner );
  fKillByExit := true;
  fNoTimeout := true;
  fTimeout := 40;
end;

procedure TfrmSettings.FormShow(Sender: TObject);
begin
  cbKillByExit.Checked := KillByExit;
  cbNoTimeout.Checked := NoTimeout;
  edTimeout.Text := IntToStr ( Timeout );
  if NoTimeout then
    edTimeout.Enabled := false
  else
    edTimeout.Enabled := true;
end;

procedure TfrmSettings.bCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmSettings.bOkClick(Sender: TObject);
begin
  KillByExit := cbKillByExit.Checked;
  NoTimeout := cbNoTimeout.Checked;
  try
    Timeout := StrToInt ( edTimeout.Text );
  except
    Beep;
  end;
  Close;
end;

procedure TfrmSettings.edTimeoutChange(Sender: TObject);
var  x : integer;
begin
  try
    x := StrToInt ( edTimeout.Text );
  except
    Beep;
    edTimeout.Text := '';
  end;
end;

procedure TfrmSettings.cbNoTimeoutClick(Sender: TObject);
begin
  if cbNoTimeout.Checked  then
    edTimeout.Enabled := false
  else
    edTimeout.Enabled := true;  
end;

end.
