(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Ermoeglicht das einfache debuggen von basisprozessen
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 06.10.1998  0.00  Mg | Datei erstellt
| 11.03.1999  1.00  Mg | New state init inserted
|=========================================================================================*)

unit DebugUtilMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ServiceMain, IPCClass, BaseGlobal, LoepfeGlobal,
  Menus, DebugUtilHelp, DebugUtilSettings, mmThread;

type
  TState =  ( sNoState,
              sCreate,
              sAvailable,
              sConnect,
              sInit,
              sRun );


  TReader = class ( TmmThread )
  private
    mPort   : TIPCServer;
    mWatchDog  : TWatchDog;
    mTimer     : TMMGuardTimer;
    mState     : TState;
    mDebugProc : TSubSystemTyp;
    mMemo      : TMemo;
    mStrBuf    : String;
    fWaitTimeout : integer;
    procedure WriteStrToMemo ( aStr : String );
    procedure SynchProc;
  public
    mProcList  : TProcessList;
    constructor Create ( aProcArray : TProcArray; aNumOfProc : integer; aMemo : TMemo); virtual;
    destructor Destroy; override;
    function Init : boolean;
    procedure Execute; override;
    property Timeout : integer read fWaitTimeout write fWaitTimeout;
  end;

  TfrmMain = class(TForm)
    MeProcesses: TMemo;
    Label1: TLabel;
    MainMenu1: TMainMenu;
    mnuFile: TMenuItem;
    mnuHelp: TMenuItem;
    mnuExit: TMenuItem;
    mnuSettings: TMenuItem;
    procedure mnuHelpClick(Sender: TObject);
    procedure mnuSettingsClick(Sender: TObject);
    procedure mnuExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    mReader    : TReader;
    function Init : boolean;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmRegistry, mmCS, typinfo;
{$R *.DFM}

//--inplementation of TReader---------------------------------------------------
constructor TReader.Create ( aProcArray : TProcArray; aNumOfProc : integer; aMemo : TMemo);
begin
  inherited Create ( true );
   mProcList := TProcessList.Create ( aProcArray, aNumOfProc );
   mPort := TIPCServer.Create ( cChannelNames[ttMMGuardMain],
                                '',
                                sizeof ( TMMGuardRec ),
                                cTimeOut,
                                NMPWAIT_USE_DEFAULT_WAIT );
   mMemo := aMemo;

end;
//------------------------------------------------------------------------------
destructor TReader.Destroy;
begin
  mProcList.Free;
  mTimer.Free;
  mWatchDog.Free;
  mPort.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TReader.Init : boolean;
begin
 mState := sNoState;
 fWaitTimeout := 40;
 Result := mPort.Init;

 if not Result then
   ShowMessage ( 'Init of readport failed' );

 mWatchDog := TWatchDog.Create ( mProcList );
 mTimer := TMMGuardTimer.Create ( 1000 );
 if Result then
   Resume;

end;
//------------------------------------------------------------------------------
procedure TReader.Execute;
var  xMsg  : TMMGuardRec;
     xEnd  : boolean;
     xRead : DWord;
     xTerminateTime : DWord;
     x     : integer;


  function All ( aProcState : TMMProcessState ) : boolean;
  var  x : integer;
       xSubSystem : TSubSystemTyp;
  begin
    Result := true;
    for x := 0 to mProcList.NumOfProc - 1 do begin
      xSubSystem := mProcList.ProcessByIndex ( x );
      if mProcList.GetProcState ( xSubSystem ) <> aProcState then
        Result := false;
    end;
  end;

  function SendToAll ( aMsg : TMMGuardRec ) : boolean;
  var x : integer;
  begin
    Result := true;
    for x := 0 to mProcList.NumOfProc - 1 do begin
        if not mProcList.WriteToProcess ( mProcList.ProcessByIndex ( x ), aMsg ) then
          Result := false;
        Sleep ( 100 );
    end;
  end;

  function ConnectAll : boolean;
  var  x : integer;
  begin
    Result := true;
    for x := 0 to mProcList.NumOfProc - 1 do begin
      if Result then
        if not mProcList.ConnectProc ( mProcList.ProcessByIndex ( x ) ) then
          Result := false;
    end;
  end;


begin
  repeat
    xRead := 0;
    xEnd := false;
    FillChar ( xMsg, sizeof ( TMMGuardRec ), 0 );
    if mPort.Read ( @xMsg, sizeof ( TMMGuardRec ), xRead ) then begin
      case mState of
        sNoState : begin
            case xMsg.MsgTyp of
              gcAvailable : begin  // debug process has started
                  if getTickCount - xTerminateTime > 2000 then begin
                    mMemo.Clear;
                    mDebugProc := xMsg.Transmitter;
                    WriteStrToMemo ( 'Start of ' + cApplicationNames[xMsg.Transmitter] + ' by user...');
                    WriteStrToMemo ( ' ');
                    CodeSite.SendString('DebugProcess', GetEnumName(TypeInfo(TSubSystemTyp), Ord(mDebugProc)));
                    // start all processes except the debug process
                    for x := 0 to mProcList.NumOfProc - 1 do begin
                      CodeSite.SendString('Process to start', GetEnumName(TypeInfo(TSubSystemTyp), Ord(mProcList.ProcessByIndex (x))));
                      if mProcList.ProcessByIndex ( x ) <> mDebugProc then begin
                        if not mProcList.CreateProc ( mProcList.ProcessByIndex ( x ) ) then begin
                          // write the error to the Memo
                          WriteStrToMemo ( 'Create of Process : ' + cApplicationNames[mProcList.processByIndex ( x )] + ' failed. Error : ' +  mProcList.Error.Msg  );
                          xEnd := true;
                        end;
                        CodeSite.SendString('Process started', GetEnumName(TypeInfo(TSubSystemTyp), Ord(mProcList.ProcessByIndex (x))));
                        WriteStrToMemo ( 'Create ' + cApplicationNames[mProcList.processByIndex ( x )] );
                        Sleep ( 200 );
                      end else begin
                        mProcList.SetProcHandle ( mDebugProc, OpenProcess ( PROCESS_ALL_ACCESS , false, xMsg.ProcessID ) );
                        mProcList.SetProcState ( mDebugProc, psAvailable );
                      end;
                    end;
                    WriteStrToMemo ( 'All processes created...' );
                    mState := sCreate;
                    mTimer.Timeout := Timeout;
                    mWatchDog.Resume;
                  end;
                  end;
            else end;
          end;

        sCreate : begin
            case xMsg.MsgTyp of
              gcAvailable : begin
                  mProcList.SetProcState ( xMsg.Transmitter , psAvailable );
                  WriteStrToMemo ( 'Available of ' + cApplicationNames[xMsg.Transmitter] + ' received');
                  // now check if all processes are ready
                  if All ( psAvailable ) then begin
                    WriteStrToMemo ( 'All Available received...' );
                    if not ConnectAll then begin
                      xEnd := true;
                      WriteStrToMemo ( 'Connect to process failed' );
                    end;

                    if not xEnd then begin
                      xMsg.MsgTyp := gcDoConnect;
                      if not SendToAll ( xMsg ) then begin
                        xEnd := true;
                        WriteStrToMemo ( 'Write to process failed' );
                      end;
                      mState := sAvailable;
                      mTimer.Timeout := Timeout;
                    end;
                end;
              end;

            gcProcessStoped : begin
                // if a process stopped write it to the memo
                WriteStrToMemo ( 'Process ' + cApplicationNames[xMsg.Transmitter] + ' stopped');
                xEnd := true;
              end;

            gcTimeout : begin
                // not all processes sent the ready back, trminate
                WriteStrToMemo ( 'Timeout by waiting of Available message...');
                xEnd := true;
              end;
            else end;
          end;

        sAvailable : begin
            case xMsg.MsgTyp of
              gcConnected : begin
                  mProcList.SetProcState ( xMsg.Transmitter , psConnected );
                  WriteStrToMemo ( 'Connect of ' + cApplicationNames[xMsg.Transmitter] + ' done');
                  if All ( psConnected ) then begin
                    mTimer.Timeout := cNO_TIMEOUT;
                    WriteStrToMemo ( 'All connect done... ');
                    xMsg.MsgTyp := gcDoInit;
                    if not SendToAll ( xMsg ) then begin
                      xEnd := true;
                      WriteStrToMemo ( 'Write to process failed' );
                    end;
                    mState := sConnect;
                    mTimer.Timeout := Timeout;
                  end;
                end;

              gcProcessStoped : begin
                  // if a process stopped write it to the memo
                  WriteStrToMemo ( 'Process ' + cApplicationNames[xMsg.Transmitter] + ' stopped');
                  xEnd := true;
                end;

            gcTimeout : begin
                // not all processes sent the ready back, trminate
                WriteStrToMemo ( 'Timeout by waiting of connect message...');
                xEnd := true;
              end;
            else end;
          end;

        sConnect : begin
            case xMsg.MsgTyp of
              gcInitialized : begin
                  mProcList.SetProcState ( xMsg.Transmitter , psInit );
                  WriteStrToMemo ( 'Initialize of ' + cApplicationNames[xMsg.Transmitter] + ' done');
                  if All ( psInit ) then begin
                    WriteStrToMemo ( 'All initializations done... ');
                    xMsg.MsgTyp := gcDoRun;
                    if not SendToAll ( xMsg ) then begin
                      xEnd := true;
                      WriteStrToMemo ( 'Write to process failed' );
                    end;
                    mState := sInit;
                    mTimer.Timeout := Timeout;
                  end;
                end;

              gcProcessStoped : begin
                  // if a process stopped write it to the memo
                  WriteStrToMemo ( 'Process ' + cApplicationNames[xMsg.Transmitter] + ' stopped');
                  xEnd := true;
                end;

              gcTimeout : begin
                  // not all processes sent the ready back, trminate
                  WriteStrToMemo ( 'Timeout by waiting of started message...');
                  xEnd := true;
                end;
            else end;
          end;

        sInit : begin
            case xMsg.MsgTyp of
              gcRun : begin
                  if xMsg.Error.Error = NO_ERROR then begin
                    WriteStrToMemo ( 'Start of ' + cApplicationNames[xMsg.Transmitter] );
                    mProcList.SetProcState ( xMsg.Transmitter , psRun  );
                  end else begin
                    WriteStrToMemo ( 'Start of Process : ' + cApplicationNames[xMsg.Transmitter] + ' failed.');
                  end;
                  if All ( psRun ) then begin
                    mState := sRun;
                    mTimer.Timeout := cNO_TIMEOUT;
                    WriteStrToMemo ( ' ' );
                    WriteStrToMemo ( 'MillMaster processes started successfully' );
                    WriteStrToMemo ( ' ' );
                    Windows.Beep ( 1000, 100 );
                  end;
                end;

              gcProcessStoped : begin
                  // if a process stopped write it to the memo
                  WriteStrToMemo ( 'Process ' + cApplicationNames[xMsg.Transmitter] + ' stopped');
                  xEnd := true;
                end;

              gcTimeout : begin
                  // not all processes sent the ready back, trminate
                  WriteStrToMemo ( 'Timeout by waiting of started message...');
                  xEnd := true;
                end;
            else end;
          end;
        sRun  : begin
            case xMsg.MsgTyp of
              gcProcessStoped : begin
                  // if a process stopped write it to the memo
                  WriteStrToMemo ( 'Process ' + cApplicationNames[xMsg.Transmitter] + ' stopped');
                  xEnd := true;
              end;
            else end;
          end;
      end;
    end else begin
      xEnd := true;
      ShowMessage ( 'Error by reading port. Error :' +  FormatErrorText ( mPort.Error ) );
    end;

    if xEnd then begin  // an error occured kill all processes
      xTerminateTime := getTickCount;
      WriteStrToMemo ( 'All process will be stopped');
      mState := sNoState;
      xMsg.MsgTyp := gcKillSelf;
      SendToAll ( xMsg );
      for x := 0 to mProcList.NumOfProc - 1 do
        mProcList.SetProcState ( mProcList.ProcessByIndex ( x ), psDead );

      Windows.Beep ( 2000, 100 );
      Windows.Beep ( 1000, 100 );
    end;
  until ( false );
end;
//------------------------------------------------------------------------------
//---implementation of TForm1---------------------------------------------------
//------------------------------------------------------------------------------
function TfrmMain.Init : boolean;
var  xNumOfProc : integer;
     xRegistry : TmmRegistry;
     x, x1 : integer;
     xProcArray : TProcArray;
begin
  Result := true;
  // load process list from registry
  xRegistry := TmmRegistry.Create;
  xRegistry.RootKey := HKEY_LOCAL_MACHINE;
  if xRegistry.OpenKey ( '\software\loepfe\millmaster\mmguard', false ) then begin
    try
      xNumOfProc := StrToInt ( xRegistry.ReadString ( 'NumOfProcesses' ));
    except
      Result := false;
      xNumOfProc := 0;
      showMessage('Could not read registry : NumOfProcesses');
    end;
    if Result then begin
      try
        for x := 0 to xNumOfProc - 1 do begin
          xProcArray[x].Name := xRegistry.ReadString ( 'Process' + IntToStr ( 1 + x ) );
          xProcArray[x].Name := UpperCase ( xProcArray[x].Name  );
          // get the type of the process by name
          for x1 := 0 to cNumOfSubSystems - 1 do begin
            if Pos (  UpperCase ( cApplicationNames[TSubSystemTyp ( x1 ) ] ) ,  xProcArray[x].Name  ) > 0 then begin
              xProcArray[x].Typ := TSubSystemTyp ( x1 );
              xProcArray[x].Unknown := false;
            end;
          end;
        end;
      except
        Result := false;
        showMessage('Could not read registry : Process');
      end;
      mReader := TReader.Create ( xProcArray, xNumOfProc, MeProcesses );
      Result := mReader.Init;
      if frmSettings.NoTimeout then
        mReader.Timeout := cNO_TIMEOUT
      else
        mReader.Timeout := frmSettings.Timeout;
    end;
  end else begin
    ShowMessage ( 'You have to prepare the registry, please reade the help');
    frmInfo.ShowModal;
  end;
end;
//------------------------------------------------------------------------------
procedure TReader.WriteStrToMemo ( aStr : String );
begin
  mStrBuf := aStr;
  self.Synchronize ( SynchProc );
end;
//------------------------------------------------------------------------------
procedure TReader.SynchProc;
begin
 mMemo.Lines.Add ( mStrbuf );
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mnuHelpClick(Sender: TObject);
begin
  frmInfo.ShowModal;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mnuSettingsClick(Sender: TObject);
begin
  frmSettings.ShowModal;
  if frmSettings.NoTimeout then
    mReader.Timeout := cNO_TIMEOUT
  else
    mReader.Timeout := frmSettings.Timeout;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mnuExitClick(Sender: TObject);
var   x : integer;
      xMsg  : TMMGuardRec;
begin
  if frmSettings.KillByExit then begin
    xMsg.MsgTyp := gcKillSelf;
    for x := 0 to mReader.mProcList.NumOfProc - 1 do begin
      mReader.mProcList.WriteToProcess ( mReader.mProcList.ProcessByIndex ( x ), xMsg );
    end;
  end;
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormShow(Sender: TObject);
begin
  if not Init then Beep;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
var
  xRect: TRect;
begin
  with TmmRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('\Software\Loepfe\MillMaster\MMGuard\Debug', False) then begin
        if ValueExists('MMAutoLauncherRect') then begin
          ReadBinaryData('MMAutoLauncherRect', xRect, SizeOf(xRect));
          Self.BoundsRect := xRect;
        end;
        CloseKey;
      end;
    finally
      Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
  xRect: TRect;
begin
  with TmmRegistry.Create do begin
    try
      RootKey := HKEY_LOCAL_MACHINE;
      if OpenKey('\Software\Loepfe\MillMaster\MMGuard\Debug', True) then begin
        xRect := Self.BoundsRect;
        WriteBinaryData('MMAutoLauncherRect', xRect, SizeOf(xRect));
        CloseKey;
      end;
    finally
      Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

