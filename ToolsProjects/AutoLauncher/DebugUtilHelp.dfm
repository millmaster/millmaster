object frmInfo: TfrmInfo
  Left = 271
  Top = 108
  Width = 503
  Height = 593
  Caption = 'Help'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Memo1: TMemo
    Left = 8
    Top = 8
    Width = 481
    Height = 505
    Lines.Strings = (
      ' General :'
      ''
      ' One single MillMaster process can not be started, so always all'
      ' Processes has to be started also for tests and debugging.'
      
        ' ***************************************************************' +
        '************************'
      ''
      ' Description :'
      ''
      
        ' The MillMaster auto launcher is useful to debug and test one MM' +
        ' process.'
      
        ' You can start the process you have to debug in the Delphi envir' +
        'onment,'
      ' like a normal way.'
      
        ' This utility will start the other processes automatically and w' +
        'ill them bring in '
      ' the status they need to work.'
      
        ' The non debug processes will be terminated automatically, after' +
        ' terminating '
      ' the debugged process.'
      
        ' ***************************************************************' +
        '************************'
      ''
      ' Parameters :'
      ''
      
        ' The MM auto launcher has to know which processes are concerned ' +
        'on '
      
        ' the debug session, so make sure your computer has the following' +
        ' '
      ' registry entries :'
      ' '
      ' -- HKEY_LOCAL_MASCHINE'
      '     -- SOFTWARE'
      '         -- LOEPFE'
      '              -- MillMaster'
      '                   -- MMGuard'
      ''
      '                                     -NumOfProcesses'
      '                                     -Process1'
      '                                     -Process2'
      '                                     -Process3'
      '                                     -Processn'
      ''
      ' Write the path and the name of the process in the data of the '
      ' Process1 to Processn. '
      ' NumOfProcesses=n.'
      ' The test process is also part of the registry entries.'
      ' Unknown processes can not be started by AutoLauncher.'
      
        ' ***************************************************************' +
        '************************'
      ''
      ' Handling :'
      ' '
      ' - Start the MM auto launcher before you start the first process'
      ' - For normal use you can bring the utility in the background ,'
      
        '   except you want to see the status of the processes during sta' +
        'rt.'
      ' - By starting a new debug session you have to do nothing with '
      '   the utility.'
      ' - If MillMaster is started successfully a beep occures.'
      ' - If an error occured two beep will be generated.'
      
        ' ***************************************************************' +
        '************************'
      ''
      ''
      '')
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object Button1: TButton
    Left = 408
    Top = 528
    Width = 75
    Height = 25
    Caption = 'Close'
    TabOrder = 1
    OnClick = Button1Click
  end
end
