object frmMain: TfrmMain
  Left = 817
  Top = 128
  BorderStyle = bsSingle
  Caption = 'MillMaster Auto Launcher'
  ClientHeight = 381
  ClientWidth = 298
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poDefault
  Scaled = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 120
    Height = 20
    Caption = 'Process status'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object MeProcesses: TMemo
    Left = 8
    Top = 32
    Width = 281
    Height = 337
    Color = clWhite
    Font.Charset = ANSI_CHARSET
    Font.Color = clBlack
    Font.Height = -12
    Font.Name = 'Arial'
    Font.Style = []
    Lines.Strings = (
      '')
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object MainMenu1: TMainMenu
    Left = 264
    object mnuFile: TMenuItem
      Caption = 'File'
      object mnuSettings: TMenuItem
        Caption = 'Settings'
        OnClick = mnuSettingsClick
      end
      object mnuExit: TMenuItem
        Caption = 'Exit'
        OnClick = mnuExitClick
      end
    end
    object mnuHelp: TMenuItem
      Caption = 'Help'
      OnClick = mnuHelpClick
    end
  end
end
