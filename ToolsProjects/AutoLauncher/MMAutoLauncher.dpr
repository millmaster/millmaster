program MMAutoLauncher;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  Forms,
  DebugUtilMain in 'DebugUtilMain.pas' {frmMain},
  DebugUtilHelp in 'DebugUtilHelp.pas' {frmInfo},
  DebugUtilSettings in 'DebugUtilSettings.pas' {frmSettings},
  MMGuard_Util in '..\..\System\MMGuard\MMGuard_Util.pas',
  ServiceMain in '..\..\system\mmguard\ServiceMain.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'MM Auto Launcher';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmInfo, frmInfo);
  Application.CreateForm(TfrmSettings, frmSettings);
  Application.Run;
end.
