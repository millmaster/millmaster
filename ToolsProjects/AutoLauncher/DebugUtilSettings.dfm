object frmSettings: TfrmSettings
  Left = 471
  Top = 200
  BorderStyle = bsDialog
  Caption = 'Settings'
  ClientHeight = 195
  ClientWidth = 321
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 56
    Width = 305
    Height = 81
    Caption = 'Reaction timeout from processes'
    TabOrder = 1
    object Label1: TLabel
      Left = 16
      Top = 56
      Width = 58
      Height = 13
      Caption = 'Timeout [s] :'
    end
    object cbNoTimeout: TCheckBox
      Left = 16
      Top = 24
      Width = 137
      Height = 17
      Caption = 'No timeout support'
      TabOrder = 0
      OnClick = cbNoTimeoutClick
    end
    object edTimeout: TEdit
      Left = 88
      Top = 48
      Width = 65
      Height = 21
      TabOrder = 1
      Text = '40'
      OnChange = edTimeoutChange
    end
  end
  object cbKillByExit: TCheckBox
    Left = 8
    Top = 24
    Width = 297
    Height = 17
    Caption = 'Kill all Processes by exit of MM auto launcher'
    Checked = True
    State = cbChecked
    TabOrder = 0
  end
  object bOk: TButton
    Left = 240
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Ok'
    TabOrder = 2
    OnClick = bOkClick
  end
  object bCancel: TButton
    Left = 152
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Cancel'
    TabOrder = 3
    OnClick = bCancelClick
  end
end
