object frmMainWindow: TfrmMainWindow
  Left = 1328
  Top = 198
  Width = 996
  Height = 567
  Caption = 'BinDiff'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmSplitter2: TmmSplitter
    Left = 635
    Top = 24
    Width = 9
    Height = 516
    Cursor = crHSplit
    Align = alRight
    Beveled = True
  end
  object mmPanel3: TmmPanel
    Left = 644
    Top = 24
    Width = 344
    Height = 516
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 1
    object mmLabel1: TmmLabel
      Left = 16
      Top = 9
      Width = 101
      Height = 13
      Caption = 'Anzahl Unterschiede:'
      FocusControl = laDifferenceCount
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 16
      Top = 49
      Width = 76
      Height = 13
      Caption = 'L�ngendifferenz'
      FocusControl = laLengthDifference
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 8
      Top = 200
      Width = 32
      Height = 13
      Caption = 'Werte:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laDifferenceCount: TmmStaticText
      Left = 16
      Top = 24
      Width = 10
      Height = 17
      Caption = '0'
      TabOrder = 0
      Visible = True
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpTop
    end
    object laLengthDifference: TmmStaticText
      Left = 16
      Top = 64
      Width = 10
      Height = 17
      Caption = '0'
      TabOrder = 1
      Visible = True
      AutoLabel.Control = mmLabel2
      AutoLabel.LabelPosition = lpTop
    end
    object mValueGrid: TmmStringGrid
      Left = 2
      Top = 216
      Width = 341
      Height = 269
      ColCount = 3
      DefaultColWidth = 68
      DefaultRowHeight = 18
      RowCount = 14
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = [fsBold]
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      ParentFont = False
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbSwap1: TmmCheckBox
      Left = 88
      Top = 192
      Width = 73
      Height = 17
      Caption = 'Swap File1'
      TabOrder = 3
      Visible = True
      OnClick = cbSwapClick
      AutoLabel.LabelPosition = lpLeft
    end
    object cbSwap2: TmmCheckBox
      Left = 208
      Top = 192
      Width = 81
      Height = 17
      Caption = 'Swap File 2'
      TabOrder = 4
      Visible = True
      OnClick = cbSwapClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmToolBar1: TmmToolBar
    Left = 0
    Top = 0
    Width = 988
    Height = 24
    ButtonWidth = 94
    Caption = 'mmToolBar1'
    Flat = True
    Images = mButtonImages
    List = True
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Caption = 'Datei 1 �ffnen'
      ImageIndex = 0
      OnClick = acLoadFile1Execute
    end
    object ToolButton2: TToolButton
      Left = 94
      Top = 0
      Caption = 'Datei 2 �ffnen'
      ImageIndex = 0
      OnClick = acLoadFile2Execute
    end
    object ToolButton4: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton4'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object mmSpeedButton1: TmmSpeedButton
      Tag = 1
      Left = 196
      Top = 0
      Width = 55
      Height = 22
      GroupIndex = 1
      Down = True
      Caption = 'HEX'
      Transparent = False
      Visible = True
      OnClick = acToggleHexDecExecute
      AutoLabel.LabelPosition = lpLeft
    end
    object mmSpeedButton2: TmmSpeedButton
      Tag = 3
      Left = 251
      Top = 0
      Width = 55
      Height = 22
      GroupIndex = 1
      Caption = 'DEZ'
      Transparent = False
      Visible = True
      OnClick = acToggleHexDecExecute
      AutoLabel.LabelPosition = lpLeft
    end
    object mmSpeedButton3: TmmSpeedButton
      Tag = 2
      Left = 306
      Top = 0
      Width = 55
      Height = 22
      GroupIndex = 1
      Caption = 'ASCII'
      Transparent = False
      Visible = True
      OnClick = acToggleHexDecExecute
      AutoLabel.LabelPosition = lpLeft
    end
    object mmPanel5: TmmPanel
      Left = 361
      Top = 0
      Width = 40
      Height = 22
      Alignment = taLeftJustify
      BevelOuter = bvNone
      Caption = '  Titel:'
      TabOrder = 0
    end
    object edTitle: TmmEdit
      Left = 401
      Top = 0
      Width = 144
      Height = 22
      Color = clWindow
      TabOrder = 1
      Text = 'BinDiff'
      Visible = True
      OnChange = edTitleChange
      AutoLabel.LabelPosition = lpLeft
      Decimals = -1
      NumMode = False
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      ValidateMode = [vmInput]
    end
  end
  object mmPanel4: TmmPanel
    Left = 0
    Top = 24
    Width = 635
    Height = 516
    Align = alClient
    BevelOuter = bvNone
    Caption = 'mmPanel4'
    TabOrder = 2
    object mmSplitter1: TmmSplitter
      Left = 0
      Top = 239
      Width = 635
      Height = 9
      Cursor = crVSplit
      Align = alBottom
      Beveled = True
    end
    object mmPanel1: TmmPanel
      Left = 0
      Top = 0
      Width = 635
      Height = 239
      Align = alClient
      BevelOuter = bvNone
      Caption = 'mmPanel1'
      TabOrder = 0
      object mBinGrid1: TmmVirtualStringTree
        Left = 0
        Top = 0
        Width = 635
        Height = 217
        Align = alClient
        Header.AutoSizeIndex = -1
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        Header.MainColumn = -1
        Header.Options = [hoColumnResize, hoDrag, hoVisible]
        Header.Style = hsXPStyle
        Indent = 0
        TabOrder = 0
        TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
        TreeOptions.PaintOptions = [toHideFocusRect, toShowDropmark, toShowHorzGridLines, toShowRoot, toShowVertGridLines, toThemeAware, toUseBlendedImages]
        TreeOptions.SelectionOptions = [toDisableDrawSelection, toExtendedFocus, toMultiSelect, toRightClickSelect]
        OnAfterCellPaint = mBinGridAfterCellPaint
        OnBeforeCellPaint = mBinGridBeforeCellPaint
        OnChange = mBinGrid1Change
        OnColumnClick = mBinGridColumnClick
        OnGetText = mBinGrid1GetText
        OnKeyUp = mBinGridKeyUp
        Columns = <>
      end
      object mBinFile1Status: TmmStatusBar
        Left = 0
        Top = 217
        Width = 635
        Height = 22
        Panels = <
          item
            Width = 30
          end
          item
            Width = 110
          end
          item
            Width = 110
          end
          item
            Width = 50
          end>
        SimplePanel = False
      end
    end
    object mmPanel2: TmmPanel
      Left = 0
      Top = 248
      Width = 635
      Height = 268
      Align = alBottom
      BevelOuter = bvNone
      Caption = 'mmPanel2'
      TabOrder = 1
      object mBinGrid2: TmmVirtualStringTree
        Left = 0
        Top = 0
        Width = 635
        Height = 246
        Align = alClient
        Header.AutoSizeIndex = 0
        Header.Font.Charset = DEFAULT_CHARSET
        Header.Font.Color = clWindowText
        Header.Font.Height = -11
        Header.Font.Name = 'MS Sans Serif'
        Header.Font.Style = []
        Header.MainColumn = -1
        Header.Options = [hoColumnResize, hoDrag, hoVisible]
        Header.Style = hsXPStyle
        Indent = 0
        TabOrder = 0
        TreeOptions.MiscOptions = [toAcceptOLEDrop, toFullRepaintOnResize, toGridExtensions, toInitOnSave, toToggleOnDblClick, toWheelPanning]
        TreeOptions.PaintOptions = [toHideFocusRect, toShowDropmark, toShowHorzGridLines, toShowRoot, toShowVertGridLines, toThemeAware, toUseBlendedImages]
        TreeOptions.SelectionOptions = [toDisableDrawSelection, toExtendedFocus, toMultiSelect, toRightClickSelect]
        OnAfterCellPaint = mBinGridAfterCellPaint
        OnBeforeCellPaint = mBinGridBeforeCellPaint
        OnChange = mBinGrid1Change
        OnGetText = mBinGrid1GetText
        Columns = <>
      end
      object mBinFile2Status: TmmStatusBar
        Left = 0
        Top = 246
        Width = 635
        Height = 22
        Panels = <
          item
            Width = 30
          end
          item
            Width = 110
          end
          item
            Width = 110
          end
          item
            Width = 50
          end>
        SimplePanel = False
      end
    end
  end
  object mmActionList1: TmmActionList
    Images = mButtonImages
    Left = 544
    Top = 69
    object acLoadFile1: TAction
      Caption = 'Datei 1 laden'
      ImageIndex = 0
      OnExecute = acLoadFile1Execute
    end
    object acLoadFile2: TAction
      Caption = 'Datei 2 laden'
      ImageIndex = 0
      OnExecute = acLoadFile2Execute
    end
    object acToggleHexDec: TAction
      Caption = 'Toggle Hex - Dez'
      OnExecute = acToggleHexDecExecute
    end
  end
  object mButtonImages: TmmImageList
    Left = 544
    Top = 101
    Bitmap = {
      494C010102000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001001000000000000008
      0000000000000000000000000000000000000000C055C055C055C055C055C055
      C055C055C055C055C055C055C055000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0552262CA722D7FE97AE97AE97A
      E97AE97AE97AE97A097B6466C055000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C055A6724462717F0A7F0A7B0A7F
      0A7B0A7F0A7F0A7F0A7B64662D73C05500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0552A7FC055937F4B7F4B7F4B7F
      4B7F4B7F4B7F4B7F4B7F8566B37BC05500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0554B7F015A50778F7F6D7F6D7F
      6D7F6D7F6D7F6D7F6D7F8566D37BC05500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0556D7F856AC96AB27F8E7F8E7F
      8E7F8E7F8E7F8E7FA005A666D47BEA6AC0550000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C0558E7F4B77025AFF7FD77FD77F
      D77FD77FD77FA005A422A005DA7FD97BC0550000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C055AF7FAF7FE159C055C055C055
      C055C055A0052733883FC526A005C055C0550000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C055D07FD07FD07FD07FD07FD07F
      D07FA005052B67376737883FC526A00500000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000C055FF7FF17FF17FF17FF17FF17F
      A005A005A005A0056737E52AA005A005A0050000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000C055FF7FF17FF17FF17FC051
      856A856A856AA005462F841EA005000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C055C055C055C0550000
      000000000000A0050527A0050000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000A0058216831AA0050000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000A0058212A00500000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      A005A005A005A005000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000A005A005
      A005A00500000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF008003FFFF000000000003BF8700000000
      0001DFBB000000000001EFBD000000000001F7BD000000000000FBBD00000000
      0000FDBB000000000000FE87000000000001BB7F000000000000BBBF00000000
      8003BBDF00000000C3C783EF00000000FF87BBF700000000FF8FBBFB00000000
      FE1FBBFD00000000F87FFFFE0000000000000000000000000000000000000000
      000000000000}
  end
  object OpenDialog: TOpenDialog
    Filter = 
      'Bin�rfiles (*.bin;*.hex;*.tmp)|*.bin;*.hex;*.tmp;*.MMBin|*.bin|*' +
      '.bin|*.hex|*.hex|Alle (*.*)|*.*'
    Left = 328
    Top = 80
  end
end
