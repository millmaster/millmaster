unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBVisualBox, Db, mmDataSource, ADODB, mmADODataSet,
  mmADOConnection, StdCtrls, mmLabel, mmButton, AdoDBAccess, BaseGlobal,
  ComCtrls, mmStatusBar, ActnList, mmActionList, DBGrids, mmDBGrid,
  Buttons, mmBitBtn, MMEventLog, Spin, mmSpinEdit, mmMemo;

type
  TfrmMain = class(TForm)
    conDefault: TmmADOConnection;
    dseProdGrp: TmmADODataSet;
    dsProdGrp: TmmDataSource;
    mmLabel7: TmmLabel;
    laServer: TmmLabel;
    mmLabel8: TmmLabel;
    laDatabase: TmmLabel;
    mmLabel9: TmmLabel;
    laDBVersion: TmmLabel;
    bGo: TmmButton;
    mmLabel1: TmmLabel;
    laInterval: TmmLabel;
    sbMain: TmmStatusBar;
    dgMain: TmmDBGrid;
    mmBitBtn1: TmmBitBtn;
    bGoAll: TmmButton;
    seMaxInterval: TmmSpinEdit;
    mmADODataSet1: TmmADODataSet;
    mmLabel2: TmmLabel;
    meInfo: TmmMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bGoClick(Sender: TObject);
    procedure bGoAllClick(Sender: TObject);
  private
    mJob: TJobRec;
    mDB: TAdoDBAccess;
    mIntervalID: Integer;
    mEventLog: TEventLogWriter;
    function DoOfflimit: Boolean;
    function GetIntervalID(aOffset: Integer): Boolean;
    procedure DeleteMaOfflimit(aMachID: Integer);
  public
  end;

var
  frmMain: TfrmMain;

implementation
{$R *.DFM}
uses
  SettingsReader, LoepfeGlobal, JobEditors;

const
  cCurrentInterval = 'select c_interval_id, c_interval_start from t_interval ' +
                     'where c_interval_end < getdate() ' +
                     'order by c_interval_start desc';

  cDeleteMaOfflimit = 'delete t_machine_offlimit where c_machine_id = %d';
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  laServer.Caption    := gSQLServerName;
  laDatabase.Caption  := gDBName;
  laDBVersion.Caption := TMMSettingsReader.Instance.DBVersion;

  dseProdGrp.Active := True;

  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cEventLogServerNameForSubSystems,
                                      ssApplication, 'MachineOfflimit: ', False);

  mDB := TAdoDBAccess.Create(cMaxQueries);
  mDB.Init;
  with mDB.Query[0] do
  try
    SQL.Text := cCurrentInterval;
    Open;
    laInterval.Caption := FieldByName('c_interval_start').AsString;
  except
    on e:Exception do
      sbMain.SimpleText := 'Error: ' + e.Message;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FreeAndNil(mDB);
  FreeAndNil(mEventLog);
end;
//------------------------------------------------------------------------------
function TfrmMain.GetIntervalID(aOffset: Integer): Boolean;
begin
  Result := False;
  with mDB.Query[0] do
  try
    SQL.Text := cCurrentInterval;
    Open;
    while (not EOF) and (aOffset > 0) do begin
      if aOffset = 1 then begin
        mIntervalID := FieldByName('c_interval_id').AsInteger;
        meInfo.Lines.Add('Do interval: ' + FieldByName('c_interval_start').AsString);
        Result := True;
        Break;
      end;
      dec(aOffset);
      Next;
    end;
  except
    on e:Exception do
      sbMain.SimpleText := 'Error: ' + e.Message;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.DeleteMaOfflimit(aMachID: Integer);
begin
  with mDB.Query[0] do
  try
    SQL.Text := Format(cDeleteMaOfflimit, [aMachID]);
    ExecSQL;
  except
    on e:Exception do
      sbMain.SimpleText := 'Error: ' + e.Message;
  end;
end;
//------------------------------------------------------------------------------
function TfrmMain.DoOfflimit: Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=seMaxInterval.Value downto 1 do begin
    if GetIntervalID(i) then begin
      mJob.GenSpdOfflimits.IntID := mIntervalID;
      with TSpdOfflimitsEditor.Create(mJob, mDB, mEventLog) do
      try
        if Init then
        try
          ProcessJob;
          Result := True;
        except
          on e:Exception do begin
            sbMain.SimpleText := 'Error: ' + e.Message;
          end;
        end;
      finally
        Free;
      end;
    end else
      Break;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bGoClick(Sender: TObject);
begin
  meInfo.Clear;
  Application.ProcessMessages;
  sbMain.SimpleText := '';

  meInfo.Lines.Add('Check: ' + dseProdGrp.FieldByName('c_name').AsString);
  mJob.GenSpdOfflimits.ProdID := dseProdGrp.FieldByName('c_prod_id').AsInteger;

  Screen.Cursor := crHourGlass;
  try
    DeleteMaOfflimit(dseProdGrp.FieldByName('c_machine_id').AsInteger);
    if DoOfflimit then
      sbMain.SimpleText := 'Done';
  finally
    sbMain.Hint := sbMain.SimpleText;
    Screen.Cursor := crDefault;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bGoAllClick(Sender: TObject);
begin
  meInfo.Clear;
  Application.ProcessMessages;
  sbMain.SimpleText := '';

  Screen.Cursor := crHourGlass;
  with dseProdGrp do
  try
    DisableControls;
    First;
    while not EOF do
    try
      meInfo.Lines.Add('Check: ' + dseProdGrp.FieldByName('c_name').AsString);
      mJob.GenSpdOfflimits.ProdID := dseProdGrp.FieldByName('c_prod_id').AsInteger;
      DeleteMaOfflimit(dseProdGrp.FieldByName('c_machine_id').AsInteger);
      if not DoOfflimit then
        Break;
      Next;
      Application.ProcessMessages;
    finally
      sbMain.Hint := sbMain.SimpleText;
      Screen.Cursor := crDefault;
    end;
  finally
    EnableControls;
  end;
end;

end.
