object frmMain: TfrmMain
  Left = 243
  Top = 123
  BorderStyle = bsDialog
  Caption = 'Machine Offlimit Generator'
  ClientHeight = 457
  ClientWidth = 470
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  ShowHint = True
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel7: TmmLabel
    Left = 232
    Top = 8
    Width = 81
    Height = 13
    Caption = 'Connected to:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laServer: TmmLabel
    Left = 320
    Top = 8
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel8: TmmLabel
    Left = 232
    Top = 24
    Width = 59
    Height = 13
    Caption = 'Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDatabase: TmmLabel
    Left = 320
    Top = 24
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel9: TmmLabel
    Left = 232
    Top = 40
    Width = 68
    Height = 13
    Caption = 'DB-Version:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDBVersion: TmmLabel
    Left = 320
    Top = 40
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel1: TmmLabel
    Left = 232
    Top = 56
    Width = 48
    Height = 13
    Caption = 'Interval:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laInterval: TmmLabel
    Left = 320
    Top = 56
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 232
    Top = 81
    Width = 124
    Height = 13
    Caption = 'Check Interval in the past:'
    FocusControl = seMaxInterval
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bGo: TmmButton
    Left = 240
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 0
    Visible = True
    OnClick = bGoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object sbMain: TmmStatusBar
    Left = 0
    Top = 435
    Width = 470
    Height = 22
    Panels = <>
    SimplePanel = True
  end
  object dgMain: TmmDBGrid
    Left = 8
    Top = 8
    Width = 217
    Height = 417
    DataSource = dsProdGrp
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'c_name'
        Title.Caption = 'Name'
        Visible = True
      end>
  end
  object mmBitBtn1: TmmBitBtn
    Left = 304
    Top = 400
    Width = 75
    Height = 25
    TabOrder = 3
    Visible = True
    Kind = bkClose
    AutoLabel.LabelPosition = lpLeft
  end
  object bGoAll: TmmButton
    Left = 368
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Go All'
    TabOrder = 4
    Visible = True
    OnClick = bGoAllClick
    AutoLabel.LabelPosition = lpLeft
  end
  object seMaxInterval: TmmSpinEdit
    Left = 232
    Top = 96
    Width = 57
    Height = 22
    MaxValue = 30
    MinValue = 1
    TabOrder = 5
    Value = 10
    Visible = True
    AutoLabel.Control = mmLabel2
    AutoLabel.LabelPosition = lpTop
  end
  object meInfo: TmmMemo
    Left = 232
    Top = 128
    Width = 225
    Height = 225
    TabOrder = 6
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object conDefault: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=WETWSS2000;Use Procedure for P' +
      'repare=1;Auto Translate=False;Packet Size=4096;Workstation ID=WE' +
      'TWSS2000;Use Encryption for Data=False;Tag with column collation' +
      ' when possible=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 240
    Top = 264
  end
  object dseProdGrp: TmmADODataSet
    Connection = conDefault
    CursorType = ctStatic
    CommandText = 
      'select pg.c_machine_id, pg.c_prod_id, (pg.c_machine_name + '#39'/'#39' +' +
      ' pg.c_prod_name) AS c_name'#13#10'from t_prodgroup_state pgs, t_prodgr' +
      'oup pg'#13#10'where pgs.c_prod_id = pg.c_prod_id'#13#10'order by pg.c_machin' +
      'e_name, pg.c_prod_name'#13#10
    Parameters = <>
    Left = 280
    Top = 264
  end
  object dsProdGrp: TmmDataSource
    DataSet = dseProdGrp
    Left = 280
    Top = 296
  end
  object mmADODataSet1: TmmADODataSet
    Connection = conDefault
    CursorType = ctStatic
    CommandText = 
      'select pg.c_prod_id, (pg.c_machine_name + '#39'/'#39' + pg.c_prod_name) ' +
      'AS c_name'#13#10'from t_prodgroup_state pgs, t_prodgroup pg'#13#10'where pgs' +
      '.c_prod_id = pg.c_prod_id'#13#10'order by pg.c_machine_name, pg.c_prod' +
      '_name'#13#10
    Parameters = <>
    Left = 312
    Top = 264
  end
end
