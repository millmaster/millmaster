object Form1: TForm1
  Left = 346
  Top = 119
  BorderStyle = bsDialog
  Caption = 'DB Date Update Tool'
  ClientHeight = 505
  ClientWidth = 410
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel
    Left = 168
    Top = 61
    Width = 55
    Height = 13
    Caption = 'Hour offset:'
    FocusControl = seHourOffset
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel2: TmmLabel
    Left = 16
    Top = 61
    Width = 51
    Height = 13
    Caption = 'Day offset:'
    FocusControl = seDayOffset
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel3: TmmLabel
    Left = 16
    Top = 105
    Width = 61
    Height = 13
    Caption = 'Interval start:'
    FocusControl = lbInterval
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel4: TmmLabel
    Left = 168
    Top = 105
    Width = 78
    Height = 13
    Caption = 'Interval preview:'
    FocusControl = lbIntervalPreview
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel5: TmmLabel
    Left = 16
    Top = 289
    Width = 47
    Height = 13
    Caption = 'Shift start:'
    FocusControl = lbShift
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel6: TmmLabel
    Left = 168
    Top = 289
    Width = 64
    Height = 13
    Caption = 'Shift preview:'
    FocusControl = lbShiftPreview
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel7: TmmLabel
    Left = 16
    Top = 8
    Width = 81
    Height = 13
    Caption = 'Connected to:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laServer: TmmLabel
    Left = 104
    Top = 8
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel8: TmmLabel
    Left = 16
    Top = 24
    Width = 59
    Height = 13
    Caption = 'Database:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDatabase: TmmLabel
    Left = 104
    Top = 24
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel9: TmmLabel
    Left = 16
    Top = 40
    Width = 68
    Height = 13
    Caption = 'DB-Version:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laDBVersion: TmmLabel
    Left = 104
    Top = 40
    Width = 8
    Height = 13
    Caption = '0'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object seHourOffset: TmmSpinEdit
    Left = 168
    Top = 76
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 0
    Value = 0
    Visible = True
    OnChange = seHourOffsetChange
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpTop
  end
  object seDayOffset: TmmSpinEdit
    Left = 16
    Top = 76
    Width = 121
    Height = 22
    MaxValue = 0
    MinValue = 0
    TabOrder = 1
    Value = 0
    Visible = True
    OnChange = seHourOffsetChange
    AutoLabel.Control = mmLabel2
    AutoLabel.LabelPosition = lpTop
  end
  object lbInterval: TmmDBLookupListBox
    Left = 16
    Top = 120
    Width = 137
    Height = 160
    KeyField = 'c_interval_start'
    ListSource = dsInterval
    TabOrder = 2
    Visible = True
    AutoLabel.Control = mmLabel3
    AutoLabel.LabelPosition = lpTop
  end
  object lbIntervalPreview: TmmListBox
    Left = 168
    Top = 120
    Width = 137
    Height = 161
    Enabled = True
    ItemHeight = 13
    TabOrder = 3
    Visible = True
    AutoLabel.Control = mmLabel4
    AutoLabel.LabelPosition = lpTop
  end
  object bIntervalUpdate: TmmButton
    Left = 320
    Top = 176
    Width = 73
    Height = 25
    Caption = 'Go'
    TabOrder = 4
    Visible = True
    OnClick = bIntervalUpdateClick
    AutoLabel.LabelPosition = lpLeft
  end
  object lbShift: TmmDBLookupListBox
    Left = 16
    Top = 304
    Width = 137
    Height = 173
    KeyField = 'c_shift_start'
    ListSource = dsShift
    TabOrder = 5
    Visible = True
    AutoLabel.Control = mmLabel5
    AutoLabel.LabelPosition = lpTop
  end
  object lbShiftPreview: TmmListBox
    Left = 168
    Top = 304
    Width = 137
    Height = 173
    Enabled = True
    ItemHeight = 13
    TabOrder = 6
    Visible = True
    AutoLabel.Control = mmLabel6
    AutoLabel.LabelPosition = lpTop
  end
  object bShiftGo: TmmButton
    Left = 320
    Top = 368
    Width = 75
    Height = 25
    Caption = 'Go'
    TabOrder = 7
    Visible = True
    OnClick = bShiftGoClick
    AutoLabel.LabelPosition = lpLeft
  end
  object mmStatusBar: TmmStatusBar
    Left = 0
    Top = 483
    Width = 410
    Height = 22
    Panels = <
      item
        Width = 250
      end
      item
        Width = 50
      end>
    SimplePanel = False
  end
  object mProgressBar: TmmProgressBar
    Left = 252
    Top = 485
    Width = 158
    Height = 20
    Min = 0
    Max = 100
    TabOrder = 8
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object bConnect: TmmButton
    Left = 320
    Top = 4
    Width = 75
    Height = 25
    Caption = 'Connect'
    TabOrder = 10
    Visible = True
    OnClick = bConnectClick
    AutoLabel.LabelPosition = lpLeft
  end
  object dsInterval: TmmDataSource
    DataSet = dseInterval
    Left = 112
    Top = 88
  end
  object dsShift: TmmDataSource
    DataSet = dseShift
    Left = 112
    Top = 288
  end
  object conDefault: TmmADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETSRVMM5;Initial Catalog=MM_Win' +
      'ding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 344
    Top = 8
  end
  object dseInterval: TmmADODataSet
    Connection = conDefault
    CursorType = ctStatic
    CommandText = 
      'select c_interval_start from t_interval order by c_interval_star' +
      't desc'
    Parameters = <>
    Left = 80
    Top = 88
  end
  object dseShift: TmmADODataSet
    Connection = conDefault
    CommandText = 
      'select distinct c_shift_start'#13#10'from v_production_shift'#13#10'order by' +
      ' c_shift_start desc'#13#10
    Parameters = <>
    Left = 80
    Top = 288
  end
  object comUpdate: TmmADOCommand
    Connection = conDefault
    Parameters = <>
    Left = 328
    Top = 288
  end
end
