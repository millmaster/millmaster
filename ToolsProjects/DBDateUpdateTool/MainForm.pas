unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmListBox, DBCtrls, mmDBLookupListBox, mmLabel, Spin,
  mmSpinEdit, Db, mmDataSource, DBTables, mmTable, mmDatabase, mmQuery,
  ComCtrls, mmStatusBar, mmProgressBar, ADODB, mmADOCommand, mmADODataSet,
  mmADOConnection;

type
  TForm1 = class(TForm)
    dsInterval: TmmDataSource;
    seHourOffset: TmmSpinEdit;
    seDayOffset: TmmSpinEdit;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    lbInterval: TmmDBLookupListBox;
    mmLabel3: TmmLabel;
    lbIntervalPreview: TmmListBox;
    mmLabel4: TmmLabel;
    bIntervalUpdate: TmmButton;
    lbShift: TmmDBLookupListBox;
    mmLabel5: TmmLabel;
    lbShiftPreview: TmmListBox;
    mmLabel6: TmmLabel;
    dsShift: TmmDataSource;
    bShiftGo: TmmButton;
    mProgressBar: TmmProgressBar;
    mmLabel7: TmmLabel;
    laServer: TmmLabel;
    mmStatusBar: TmmStatusBar;
    bConnect: TmmButton;
    conDefault: TmmADOConnection;
    dseInterval: TmmADODataSet;
    dseShift: TmmADODataSet;
    comUpdate: TmmADOCommand;
    mmLabel8: TmmLabel;
    laDatabase: TmmLabel;
    mmLabel9: TmmLabel;
    laDBVersion: TmmLabel;
    procedure tabIntervalAfterOpen(DataSet: TDataSet);
    procedure seHourOffsetChange(Sender: TObject);
    procedure qryIntervalAfterOpen(DataSet: TDataSet);
    procedure bIntervalUpdateClick(Sender: TObject);
    procedure bShiftGoClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure bConnectClick(Sender: TObject);
  private
    procedure FillData;
  public
  end;

var
  Form1: TForm1;

implementation

uses
  LoepfeGlobal, SettingsReader;
{$R *.DFM}

procedure TForm1.tabIntervalAfterOpen(DataSet: TDataSet);
begin
  FillData;
end;
//------------------------------------------------------------------------------
procedure TForm1.FillData;
var
  xDateTime: TDateTime;
begin
  lbIntervalPreview.Clear;
  with dseInterval do
  try
    DisableControls;
    Open;
    First;
    while not EOF do begin
      xDateTime := FieldByName('c_interval_start').AsDateTime + seDayOffset.Value + seHourOffset.Value/24;
      lbIntervalPreview.Items.Add(DateTimeToStr(xDateTime));
      Next;
    end;
  finally
    First;
    EnableControls;
  end;

  lbShiftPreview.Clear;
  with dseShift do
  try
    DisableControls;
    Open;
    First;
    while not EOF do begin
      xDateTime := FieldByName('c_shift_start').AsDateTime + seDayOffset.Value;
      lbShiftPreview.Items.Add(DateTimeToStr(xDateTime));
      Next;
    end;
  finally
    First;
    EnableControls;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.seHourOffsetChange(Sender: TObject);
begin
  if (Sender as TmmSpinEdit).Text <> '' then
    FillData;
end;
//------------------------------------------------------------------------------
procedure TForm1.qryIntervalAfterOpen(DataSet: TDataSet);
begin
  FillData;
end;
//------------------------------------------------------------------------------
procedure TForm1.bIntervalUpdateClick(Sender: TObject);
var
  xDateTime: TDateTime;
  i: Integer;
begin
  mmStatusBar.Panels.Items[0].Text := 'Interval table';
  mmStatusBar.Update;
  conDefault.BeginTrans;
  with comUpdate do
  try
    try
      CommandText := Format('update t_interval set c_interval_start = DATEADD(day, %d, c_interval_start), c_interval_end = DATEADD(day, %d, c_interval_end)', [seDayOffset.Value, seDayOffset.Value]);
      Execute;

      CommandText := Format('update t_interval set c_interval_start = DATEADD(hour, %d, c_interval_start), c_interval_end = DATEADD(hour, %d, c_interval_end)', [seHourOffset.Value, seHourOffset.Value]);
      Execute;

      conDefault.CommitTrans;
    finally
      mmStatusBar.Panels.Items[0].Text := '';
      mProgressBar.Position := 0;
    end;
  except
    conDefault.RollbackTrans;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bShiftGoClick(Sender: TObject);
const
  cUpdateQuery = 'update %s set %s = DATEADD(day, %d, %s)';

var
  i: Integer;
begin
  mmStatusBar.Panels.Items[0].Text := 'Shift table';
  mmStatusBar.Update;
  conDefault.BeginTrans;
  with comUpdate do
  try
    try
      CommandText := Format(cUpdateQuery, ['t_shift', 'c_shift_start', seDayOffset.Value, 'c_shift_start']);
      Execute;
    finally
      mProgressBar.Position := 0;
    end;

    mmStatusBar.Panels.Items[0].Text := 'FragShiftStart table';
    mmStatusBar.Update;
    try
      CommandText := Format(cUpdateQuery, ['t_fragshift', 'c_fragshift_start', seDayOffset.Value, 'c_fragshift_start']);
      Execute;
    finally
      mProgressBar.Position := 0;
    end;

    mmStatusBar.Panels.Items[0].Text := 'DWProdgroupShift table';
    mmStatusBar.Update;
    try
      CommandText := Format(cUpdateQuery, ['t_dw_prodgroup_shift', 'c_fragshift_start', seDayOffset.Value, 'c_fragshift_start']);
      Execute;
    finally
      mProgressBar.Position := 0;
    end;

    mmStatusBar.Panels.Items[0].Text := 'Prodgroup table';
    mmStatusBar.Update;
    try
      CommandText := Format('update t_prodgroup set c_prod_start = DATEADD(day, %d, c_prod_start), c_prod_end = DATEADD(day, %d, c_prod_end)', [seDayOffset.Value, seDayOffset.Value]);
      Execute;
    finally
      mProgressBar.Position := 0;
    end;
    conDefault.CommitTrans;
  except
    conDefault.RollbackTrans;
  end;
  mmStatusBar.Panels.Items[0].Text := '';
end;
//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
const
  c18h = 18/24;
var
  xHour, xDay: Integer;
  xSingle: Single;
  xDate: TDateTime;
  function CheckForParameter(aStr: String): Boolean;
  var
    i: Integer;
  begin
    Result := False;
    for i:=1 to ParamCount do begin
      Result := (CompareText(ParamStr(i), aStr) = 0);
      if Result then
        Break;
    end;
  end;
begin
  laServer.Caption   := gSQLServerName;
  laDatabase.Caption := gDBName;
  laDBVersion.Caption := TMMSettingsReader.Instance.DBVersion;
  if ParamCount > 0 then begin
    if CheckForParameter('/auto') then begin
      Self.Visible := False;
      if not CheckForParameter('/noinfo') then begin
        if MessageDlg(Format('Update database of %s/%s to actual time/date?', [gSQLServerName, gDBName]), mtConfirmation, mbOKCancel, 0) <> mrOk then begin
          Application.Terminate;
          Exit;
        end;
      end;
      bConnect.Click;
      // Schichtdaten aktuallisieren
      seDayOffset.Value  := Trunc(Now - dseShift.FieldByName('c_shift_start').AsDateTime) + 1;
      bShiftGo.Click;
      // Intervalldaten aktuallisieren
      xDate := dseInterval.FieldByName('c_interval_start').AsDateTime;
      seDayOffset.Value  := Trunc(Now) - Trunc(xDate);

      xSingle := c18h - Frac(xDate);
      seHourOffset.Value := StrToInt(FormatDateTime('hh', (xSingle)));
      if xSingle < 0 then
        seHourOffset.Value := seHourOffset.Value * -1;
      bIntervalUpdate.Click;

      Application.Terminate;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TForm1.bConnectClick(Sender: TObject);
begin
  FillData;
end;

end.

