object frmMain: TfrmMain
  Left = 225
  Top = 132
  Width = 550
  Height = 640
  Caption = 'WSC Tool'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object mPageControl: TmmPageControl
    Left = 0
    Top = 0
    Width = 542
    Height = 613
    ActivePage = tsMapFile
    Align = alClient
    TabOrder = 0
    object tsMapFile: TTabSheet
      Caption = 'Map File'
      object mmLabel1: TmmLabel
        Left = 24
        Top = 17
        Width = 51
        Height = 13
        Caption = 'IP-Address'
        FocusControl = edIPAddress
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel2: TmmLabel
        Left = 24
        Top = 64
        Width = 66
        Height = 13
        Caption = 'Mapfile name:'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laMapfileName: TmmLabel
        Left = 96
        Top = 64
        Width = 3
        Height = 13
        Caption = '-'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edIPAddress: TmmEdit
        Left = 24
        Top = 32
        Width = 121
        Height = 21
        Color = clWindow
        TabOrder = 0
        Visible = True
        AutoLabel.Control = mmLabel1
        AutoLabel.LabelPosition = lpTop
        Decimals = 0
        NumMode = False
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object bConnect: TmmButton
        Left = 152
        Top = 32
        Width = 75
        Height = 25
        Caption = 'Connect'
        TabOrder = 1
        Visible = True
        OnClick = bConnectClick
        AutoLabel.LabelPosition = lpLeft
      end
      object meMsg: TmmMemo
        Left = 24
        Top = 128
        Width = 201
        Height = 321
        TabOrder = 2
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object IdFTP: TIdFTP
    MaxLineAction = maException
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 260
    Top = 24
  end
  object NMFTP: TNMFTP
    Port = 21
    ReportLevel = 0
    OnListItem = NMFTPListItem
    Vendor = 2411
    ParseList = False
    ProxyPort = 0
    Passive = False
    FirewallType = FTUser
    FWAuthenticate = False
    Left = 292
    Top = 26
  end
end
