unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdFTP,
  StdCtrls, mmButton, mmLabel, mmEdit, ComCtrls, mmPageControl, Psock,
  NMFtp, mmMemo;

type
  TfrmMain = class(TForm)
    mPageControl: TmmPageControl;
    tsMapFile: TTabSheet;
    edIPAddress: TmmEdit;
    mmLabel1: TmmLabel;
    bConnect: TmmButton;
    mmLabel2: TmmLabel;
    laMapfileName: TmmLabel;
    IdFTP: TIdFTP;
    meMsg: TmmMemo;
    NMFTP: TNMFTP;
    procedure bConnectClick(Sender: TObject);
    procedure NMFTPListItem(Listing: String);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  frmMain: TfrmMain;

implementation
{$R *.DFM}
const
  cWSCMapfilePath = '/mhc1/mmi/oob';

procedure TfrmMain.bConnectClick(Sender: TObject);
var
  xCollection: TCollection;
  i: Integer;
begin
  NMFTP.Host     := edIPAddress.Text;
  NMFTP.Port     := 21;
  NMFTP.Timeout  := 5000;
  NMFTP.UserID   := 'super';
  NMFTP.Password := 'user';
  try
    NMFTP.Connect;
    NMFTP.ChangeDir(cWSCMapfilePath);
  except
    on E:Exception do
       meMsg.Lines.Add(e.Message);
  end;

{
  with TIdFTP.Create(Nil) do
  try
    Host     := edIPAddress.Text;
    Port     := 21;
    Username := 'super';
    Password := 'user';
    // wss: f�r bin�re Dateien den ByteMode verwenden da ansonsten LineFeeds $0D mit $0D$0A ersetzt werden
    TransferType := ftBinary;
    try
      Connect(False);
      Login;
      xCollection := DirectoryListing;
      for i:=0 to xCollection.Count-1 do begin
        xCollection.
      end;
      Get(edWSCFile.Text, xRcvStream, False);
      Disconnect;
      xRcvStream.Position := 0;
      // TODO: unpack xStream und lege Mapfile in Liste ab
      xDest := TStringStream.Create('');
      try
        xDecompressionStream := TBZDecompressionStream.Create(xRcvStream);
        try
          while True do begin
            xCount := xDecompressionStream.Read(xBuffer, cBufferSize);
            if xCount <> 0 then
              xDest.WriteBuffer(xBuffer, xCount)
            else
              Break;
          end;
        finally
          xDecompressionStream.Free;
        end;
        xDest.Position := 0;
        meWSC.Lines.LoadFromStream(xDest);
      finally
        xDest.Free;
      end;
    except
    end;
  finally
    Free;
    xRcvStream.Free;
  end;
{}
end;

procedure TfrmMain.NMFTPListItem(Listing: String);
begin
  meMsg.Lines.Add(Listing);
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  if DebugHook <> 0 then
    edIPAddress.Text := '150.158.148.90';
end;

end.
