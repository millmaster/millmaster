object frmMain: TfrmMain
  Left = 243
  Top = 123
  Width = 314
  Height = 487
  Caption = 'frmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object bOK: TmmButton
    Left = 115
    Top = 430
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'OK'
    TabOrder = 0
    Visible = True
    OnClick = bOKClick
    AutoLabel.LabelPosition = lpLeft
  end
  object meInfo: TmmMemo
    Left = 0
    Top = 89
    Width = 306
    Height = 305
    Align = alTop
    ScrollBars = ssBoth
    TabOrder = 1
    Visible = True
    WordWrap = False
    AutoLabel.LabelPosition = lpLeft
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 306
    Height = 89
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object mmLabel1: TmmLabel
      Left = 16
      Top = 0
      Width = 31
      Height = 13
      Caption = 'Server'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 16
      Top = 40
      Width = 22
      Height = 13
      Caption = 'User'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 160
      Top = 40
      Width = 46
      Height = 13
      Caption = 'Password'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object edServer: TmmEdit
      Left = 16
      Top = 16
      Width = 121
      Height = 21
      Color = clWindow
      TabOrder = 0
      Text = '.'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object edUser: TmmEdit
      Left = 16
      Top = 56
      Width = 121
      Height = 21
      Color = clWindow
      TabOrder = 1
      Text = 'sa'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object edPassword: TmmEdit
      Left = 160
      Top = 56
      Width = 121
      Height = 21
      Color = clWindow
      PasswordChar = '*'
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object bUpdate: TmmButton
      Left = 160
      Top = 12
      Width = 75
      Height = 25
      Caption = 'Update'
      TabOrder = 3
      Visible = True
      OnClick = bUpdateClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object Button1: TButton
    Left = 8
    Top = 400
    Width = 89
    Height = 25
    Caption = 'Check SQL 7'
    TabOrder = 3
    Visible = False
    OnClick = Button1Click
  end
end
