unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmMemo, mmLabel, mmEdit, ExtCtrls, mmPanel;

type
  TfrmMain = class(TForm)
    bOK: TmmButton;
    meInfo: TmmMemo;
    mmPanel1: TmmPanel;
    edServer: TmmEdit;
    edUser: TmmEdit;
    edPassword: TmmEdit;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    bUpdate: TmmButton;
    Button1: TButton;
    procedure FormCreate(Sender: TObject);
    procedure bOKClick(Sender: TObject);
    procedure bUpdateClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    procedure AddInfo(aString: string; const Args: array of const); overload;
    procedure AddInfo(aString: string); overload;
    procedure UpdateInfo;
  public
  end;

var
  frmMain: TfrmMain;

implementation
{$R *.DFM}
uses
  SQLDMO_TLB, LoepfeGlobal, Registry, NTException;

const
  cNoYesArr: Array[Boolean] of String = ('No', 'Yes');
//------------------------------------------------------------------------------
procedure TfrmMain.AddInfo(aString: string; const Args: array of const);
begin
  meInfo.Lines.Add(Format(aString, Args));
end;
//------------------------------------------------------------------------------
procedure TfrmMain.AddInfo(aString: string);
begin
  meInfo.Lines.Add(aString);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormCreate(Sender: TObject);
begin
  edServer.Text   := gMMHost;
  edUser.Text     := gDBUsername;
  edPassword.Text := gDBPassword;
  UpdateInfo;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bOKClick(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.UpdateInfo;
var
  xSQLServer: SQLServer2;
  xText : String;
  x: integer;

  xGroup : Group;
begin
  meInfo.Clear;
  xSQLServer := CoSQLServer2.Create;

  with xSQLServer do
  try
//    xSQLServer.Connect('.', 'MMSystemSQL', 'netpmek32');
    try


      xSQLServer.LoginTimeout:= 2;

      try
        xSQLServer.Connect(edServer.Text, edUser.Text, edPassword.Text);
      except
        edUser.Text     :=  'sa';
        edPassword.Text :=  'nt50beta';

        xSQLServer.Connect(edServer.Text, edUser.Text, edPassword.Text);
      end;


      AddInfo('SQLServer2 Intervace Info');
      AddInfo('ServiceName: ' + ServiceName);
      AddInfo('InstanceName: ' + InstanceName);
      AddInfo('Collation: ' + Collation);
      AddInfo('ProductLevel: ' + ProductLevel);
      AddInfo('PID: %d', [PID]);
      AddInfo('IsClustered: %d', [Integer(IsClustered)]);
      AddInfo('IsFullTextInstalled: ' + cNoYesArr[IsFullTextInstalled]);
      AddInfo('Isbulkadmin: %d', [Integer(Isbulkadmin)]);

      AddInfo('-------------------------------------');
{
    property CommandTerminator: WideString read Get_CommandTerminator write Set_CommandTerminator;
    property TrueName: WideString read Get_TrueName;
    property ConnectionID: Integer read Get_ConnectionID;
    property TrueLogin: WideString read Get_TrueLogin;
    property IntegratedSecurity: IntegratedSecurity read Get_IntegratedSecurity;
    property Languages: Languages read Get_Languages;
    property RemoteServers: RemoteServers read Get_RemoteServers;
    property Logins: Logins read Get_Logins;
    property UserProfile: SQLDMO_SRVUSERPROFILE_TYPE read Get_UserProfile;
    property MaxNumericPrecision: Integer read Get_MaxNumericPrecision;
    property NextDeviceNumber: Integer read Get_NextDeviceNumber;
    property QueryTimeout: Integer read Get_QueryTimeout write Set_QueryTimeout;
    property LoginTimeout: Integer read Get_LoginTimeout write Set_LoginTimeout;
    property NetPacketSize: Integer read Get_NetPacketSize write Set_NetPacketSize;
    property ApplicationName: WideString read Get_ApplicationName write Set_ApplicationName;
    property LoginSecure: WordBool read Get_LoginSecure write Set_LoginSecure;
    property ProcessID: Integer read Get_ProcessID;
    property Status: SQLDMO_SVCSTATUS_TYPE read Get_Status;
    property Registry: Registry read Get_Registry;
    property Configuration: Configuration read Get_Configuration;
    property JobServer: JobServer read Get_JobServer;
    property ProcessInputBuffer[ProcessID: Integer]: WideString read Get_ProcessInputBuffer;
    property ProcessOutputBuffer[ProcessID: Integer]: WideString read Get_ProcessOutputBuffer;
    property Language: WideString read Get_Language write Set_Language;
    property AutoReConnect: WordBool read Get_AutoReConnect write Set_AutoReConnect;
    property StatusInfoRefetchInterval[StatusInfoType: SQLDMO_STATUSINFO_TYPE]: Integer read Get_StatusInfoRefetchInterval write Set_StatusInfoRefetchInterval;
    property SaLogin: WordBool read Get_SaLogin;
    property AnsiNulls: WordBool read Get_AnsiNulls write Set_AnsiNulls;
    property NetName: WideString read Get_NetName;
    property Replication: Replication read Get_Replication;
    property EnableBcp: WordBool read Get_EnableBcp write Set_EnableBcp;
    property BlockingTimeout: Integer read Get_BlockingTimeout write Set_BlockingTimeout;
    property ServerRoles: ServerRoles read Get_ServerRoles;
    property Isdbcreator: WordBool read Get_Isdbcreator;
    property Isdiskadmin: WordBool read Get_Isdiskadmin;
    property Isprocessadmin: WordBool read Get_Isprocessadmin;
    property Issecurityadmin: WordBool read Get_Issecurityadmin;
    property Isserveradmin: WordBool read Get_Isserveradmin;
    property Issetupadmin: WordBool read Get_Issetupadmin;
    property Issysadmin: WordBool read Get_Issysadmin;
    property QuotedIdentifier: WordBool read Get_QuotedIdentifier write Set_QuotedIdentifier;
    property LinkedServers: LinkedServers read Get_LinkedServers;
    property CodePageOverride: Integer write Set_CodePageOverride;
    property FullTextService: FullTextService read Get_FullTextService;
    property ODBCPrefix: WordBool read Get_ODBCPrefix write Set_ODBCPrefix;
    property RegionalSetting: WordBool read Get_RegionalSetting write Set_RegionalSetting;
    property CodePage: Integer read Get_CodePage;
    property ServerTime: WideString read Get_ServerTime;
    property TranslateChar: WordBool read Get_TranslateChar write Set_TranslateChar;
{}
      AddInfo('SQLServer Intervace Info');
      AddInfo('Hostname: ' + HostName);
      AddInfo('VersionString: ' + VersionString);
      AddInfo('VersionMajor: %d', [VersionMajor]);
      AddInfo('VersionMinor: %d', [VersionMinor]);



      AddInfo('Application.VersionBuild : %d', [ Application.VersionBuild]);

      case xSQLServer.IsPackage of
         SQLDMO_Unknown    :  xText := 'Bad or invalid value' ;
         SQLDMO_MSDE       :  xText := 'Microsoft Data Engine';
         SQLDMO_STANDARD   :  xText := 'Standard';
         SQLDMO_OFFICE     :  xText := 'Desktop';
         SQLDMO_ENTERPRISE :  xText := 'Enterprise';
         else
            xText := 'unknown';
      end;
      AddInfo('Package : %s', [xText]);


      AddInfo('MS SQL Language: ' + Language);



      AddInfo('-------------------------------------');
      AddInfo('Installed Databases');
      AddInfo('');

      for x:=  1 to  xSQLServer.Databases.Count do begin
            xText := xSQLServer.Databases.ItemByID(x).Name;
           AddInfo('Database : %s', [xText]);
      end;

      AddInfo('');
      AddInfo('NetName : %s', [xSQLServer.NetName]);

      for x:=  1 to  xSQLServer.Databases.ItemByID(7).Users.Count do begin
         AddInfo('User : %s', [ xSQLServer.Databases.ItemByID(7).Get_Users.Item(x).Name ] ) ;
      end;
{
      for x:=  1 to  xSQLServer.Databases.ItemByID(6).Groups.Count do begin
       //  AddInfo('Groups : %s', [ xSQLServer.Databases.ItemByID(7).Get_Groups.Item(x).Name ] ) ;
      end;
}

    except
      on e:Exception do begin
        AddInfo('Error: ' + e.Message);

        //AddInfo('%s' + GetNetApiErrorString(GetLastError) );

      end;
    end;
  finally
    xSQLServer := Nil;
  end;


end;
//------------------------------------------------------------------------------
procedure TfrmMain.bUpdateClick(Sender: TObject);
begin
  UpdateInfo;
end;

procedure TfrmMain.Button1Click(Sender: TObject);
var
  xSQLServer: SQLServer;
  x     : word;
  xText : String;
begin
  meInfo.Clear;
  xSQLServer := CoSQLServer.Create;

  with xSQLServer do begin

    try
      xSQLServer.Connect(edServer.Text, edUser.Text, edPassword.Text);


      AddInfo('MS SQL Language: ' + Language);

      AddInfo('MS SQL major version : %d', [xSQLServer.VersionMajor]);
      AddInfo('MS SQL minor version : %d', [xSQLServer.VersionMinor]);

      AddInfo('VersionBuild: %d', [xSQLServer.Application.VersionBuild]);
      AddInfo('xSQLServer.VersionMinor: %s', [xSQLServer.VersionString]);

      x:= xSQLServer.IsPackage;
      case x of
         SQLDMO_Unknown    :  xText := 'Bad or invalid value' ;
         SQLDMO_MSDE       :  xText := 'Microsoft Data Engine';
         SQLDMO_STANDARD   :  xText := 'Standard';
         SQLDMO_OFFICE     :  xText := 'Desktop';
         SQLDMO_ENTERPRISE :  xText := 'Enterprise';
         else
            xText := 'unknown';
      end;
      AddInfo('Package : %s', [xText]);

       xSQLServer.PingSQLServerVersion(edServer.Text, edUser.Text, edPassword.Text);
      xSQLServer := Nil;
    except

      on e:Exception do begin
        AddInfo('Error: ' + e.Message);
        xSQLServer := Nil;
      end;
    end;




end;

end;

end.
