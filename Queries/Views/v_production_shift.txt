SET QUOTED_IDENTIFIER  OFF    SET ANSI_NULLS  ON 
GO

CREATE VIEW dbo.v_production_shift_1
AS
SELECT dbo.t_shift.c_shiftcal_id, dbo.t_shift.c_shift_id, 
    dbo.t_shift.c_shift_start, dbo.t_prodgroup.c_prod_id, 
    dbo.t_prodgroup.c_prod_name, 
    dbo.t_prodgroup.c_spindle_first, 
    dbo.t_prodgroup.c_spindle_last, dbo.t_prodgroup.c_prod_start, 
    dbo.t_prodgroup.c_prod_end, dbo.t_prodgroup.c_machine_id, 
    dbo.t_prodgroup.c_machine_name, 
    dbo.t_prodgroup.c_style_id, dbo.t_prodgroup.c_style_name, 
    dbo.t_prodgroup.c_YM_set_id, 
    dbo.t_dw_prodgroup_shift.c_classCut_id, 
    dbo.t_dw_prodgroup_shift.c_classUncut_id, 
    dbo.t_dw_prodgroup_shift.c_spliceCut_id, 
    dbo.t_dw_prodgroup_shift.c_spliceUncut_id, 
    dbo.t_dw_prodgroup_shift.c_siroCutUncut_id, 
    dbo.t_dw_prodgroup_shift.c_Len, 
    dbo.t_dw_prodgroup_shift.c_Wei, 
    dbo.t_dw_prodgroup_shift.c_Bob, 
    dbo.t_dw_prodgroup_shift.c_Cones, 
    dbo.t_dw_prodgroup_shift.c_Red, 
    dbo.t_dw_prodgroup_shift.c_tRed, 
    dbo.t_dw_prodgroup_shift.c_tYell, 
    dbo.t_dw_prodgroup_shift.c_ManSt, 
    dbo.t_dw_prodgroup_shift.c_tManSt, 
    dbo.t_dw_prodgroup_shift.c_rtSpd, 
    dbo.t_dw_prodgroup_shift.c_otSpd, 
    dbo.t_dw_prodgroup_shift.c_wtSpd, 
    dbo.t_dw_prodgroup_shift.c_Sp, 
    dbo.t_dw_prodgroup_shift.c_RSp, 
    dbo.t_dw_prodgroup_shift.c_YB, 
    dbo.t_dw_prodgroup_shift.c_otProdGrp, 
    dbo.t_dw_prodgroup_shift.c_wtProdGrp, 
    dbo.t_dw_prodgroup_shift.c_LSt, 
    dbo.t_dw_prodgroup_shift.c_LStProd, 
    dbo.t_dw_prodgroup_shift.c_LStBreak, 
    dbo.t_dw_prodgroup_shift.c_LStMat, 
    dbo.t_dw_prodgroup_shift.c_LStRev, 
    dbo.t_dw_prodgroup_shift.c_LStClean, 
    dbo.t_dw_prodgroup_shift.c_LStDef, 
    dbo.t_dw_prodgroup_shift.c_CS, 
    dbo.t_dw_prodgroup_shift.c_CL, 
    dbo.t_dw_prodgroup_shift.c_CT, 
    dbo.t_dw_prodgroup_shift.c_CN, 
    dbo.t_dw_prodgroup_shift.c_CSp, 
    dbo.t_dw_prodgroup_shift.c_CCl, 
    dbo.t_dw_prodgroup_shift.c_LckCl, 
    dbo.t_dw_prodgroup_shift.c_UClS, 
    dbo.t_dw_prodgroup_shift.c_UClL, 
    dbo.t_dw_prodgroup_shift.c_UClT, 
    dbo.t_dw_prodgroup_shift.c_COffCnt, 
    dbo.t_dw_prodgroup_shift.c_LckOffCnt, 
    dbo.t_dw_prodgroup_shift.c_CBu, 
    dbo.t_dw_prodgroup_shift.c_CDBu, 
    dbo.t_dw_prodgroup_shift.c_CSys, 
    dbo.t_dw_prodgroup_shift.c_LckSys, 
    dbo.t_dw_prodgroup_shift.c_CUpY, 
    dbo.t_dw_prodgroup_shift.c_CYTot, 
    dbo.t_dw_prodgroup_shift.c_CSIRO, 
    dbo.t_dw_prodgroup_shift.c_LckSIRO, 
    dbo.t_dw_prodgroup_shift.c_CSIROCl, 
    dbo.t_dw_prodgroup_shift.c_LckSIROCl, 
    dbo.t_dw_prodgroup_shift.c_INeps, 
    dbo.t_dw_prodgroup_shift.c_IThick, 
    dbo.t_dw_prodgroup_shift.c_IThin, 
    dbo.t_dw_prodgroup_shift.c_I2_4, 
    dbo.t_dw_prodgroup_shift.c_I4_8, 
    dbo.t_dw_prodgroup_shift.c_I8_20, 
    dbo.t_dw_prodgroup_shift.c_I20_70, 
    dbo.t_dw_prodgroup_shift.c_SFI, 
    dbo.t_dw_prodgroup_shift.c_SFICnt, 
    dbo.t_dw_prodgroup_shift.c_CSFI, 
    dbo.t_dw_prodgroup_shift.c_LckSFI, 
    dbo.t_dw_prodgroup_shift.c_ISmall
FROM dbo.t_prodgroup INNER JOIN
    dbo.t_dw_prodgroup_shift INNER JOIN
    dbo.t_shift_fragshift INNER JOIN
    dbo.t_shift ON 
    dbo.t_shift_fragshift.c_shift_id = dbo.t_shift.c_shift_id ON 
    dbo.t_dw_prodgroup_shift.c_fragshift_id = dbo.t_shift_fragshift.c_fragshift_id
     ON 
    dbo.t_prodgroup.c_prod_id = dbo.t_dw_prodgroup_shift.c_prod_id

GO
SET QUOTED_IDENTIFIER  OFF    SET ANSI_NULLS  ON 
GO

