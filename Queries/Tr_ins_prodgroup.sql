PRINT 'Creation of trigger "tr_ins_prodgroup"'
/* Added 0n 4.6.09 Nue */
/* This Trigger has been added, because of c_prod_end  and c_prod_start should be the same at insert. There were several situations, when it wasn't this way. Because it was not possible, to simulate the error, i decided to fire this trigger. */ 
GO
create trigger tr_ins_prodgroup
  on t_prodgroup
  for insert
  as
  SET nocount ON
    if (select c_prod_start from inserted)<>(select c_prod_end from inserted)
     BEGIN
       update t_prodgroup set c_prod_end=(select c_prod_start from inserted) where c_prod_id = (select c_prod_id from inserted)
     END
  SET nocount OFF
/*----------------------------------------------------------------*/
GO
