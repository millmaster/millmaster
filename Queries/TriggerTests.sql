drop trigger tr_del_dw_prodgroup_shift
go
create trigger tr_del_dw_prodgroup_shift
  on t_dw_prodgroup_shift
  for delete
  as
declare @start datetime
select @start=getdate()

    delete t_dw_classCut
    from t_dw_classCut, deleted
    where t_dw_classCut.c_classCut_id=deleted.c_classCut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_classCut:  ',datediff(ms,@start,getdate()));   

    delete t_dw_classUncut
    from t_dw_classUncut, deleted
    where t_dw_classUncut.c_classUncut_id=deleted.c_classUncut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_classUnCut:  ',datediff(ms,@start,getdate()));   

    delete t_dw_spliceCut
    from t_dw_spliceCut, deleted
    where t_dw_spliceCut.c_spliceCut_id=deleted.c_spliceCut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_spliceCut:  ',datediff(ms,@start,getdate()));   

    delete t_dw_spliceUncut
    from t_dw_spliceUncut, deleted
    where t_dw_spliceUncut.c_spliceUncut_id=deleted.c_spliceUncut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_spliceUncut:  ',datediff(ms,@start,getdate()));   

    delete t_dw_siroCut
    from t_dw_siroCut, deleted
    where t_dw_siroCut.c_siroCut_id=deleted.c_siroCut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_siroCut:  ',datediff(ms,@start,getdate()));   

    delete t_dw_siroUncut
    from t_dw_siroUncut, deleted
    where t_dw_siroUncut.c_siroUncut_id=deleted.c_siroUncut_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_siroUncut:  ',datediff(ms,@start,getdate()));   

    /*Delete shifts new since 14.10.03 (fires trigger tr_del_shift)*/
    delete t_shift
    from t_shift, deleted
    where datediff(day, t_shift.c_shift_start, getdate())>400/*delete all shifts older than 400 days in the past from now*/
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_shift:  ',datediff(ms,@start,getdate()));   
  if NOT EXISTS (select distinct t_dw_prodgroup_shift.c_prod_id from t_dw_prodgroup_shift, deleted
         where t_dw_prodgroup_shift.c_prod_id= deleted.c_prod_id)
     delete t_prodgroup
      from t_prodgroup, deleted
      where t_prodgroup.c_prod_id = deleted.c_prod_id
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_prodgroup:  ',datediff(ms,@start,getdate()));   
  /*New since 29.1.02:  without data*/
  delete t_prodgroup where c_prod_id in (select c_prod_id where c_prod_start<>c_prod_end and
  c_prod_id NOT in (select distinct c_prod_id from t_dw_prodgroup_shift))
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach Deletes all stopped prodgroup:  ',datediff(ms,@start,getdate()));   


delete from t_dw_prodgroup_shift where c_fragshift_start < '12/23/2004 05:29'

declare @start datetime
select @start=getdate()
insert tmp_log values(RTRIM(CONVERT(varchar(30), getdate())),'Nach delete t_dw_classCut:  ',datediff(ms,@start,getdate()));   
print RTRIM(CONVERT(varchar(30), getdate()))+'Nach Deletes all stopped prodgroup:  '+RTRIM(CONVERT(varchar(30), datediff(ms,@start,getdate()))) +'ms'
select day(38341.2284899306) Day, month(38341.2284899306) DMonth, year(38341.2284899306) Year,
  datepart(hh,38341.2284899306) Std,
datepart(mi,38341.2284899306) Min,
datepart(ss,38341.2284899306) Sec

select * from tmp_log


create table tmp_log
  (c_date varchar(40),
   c_position varchar(60),
   c_millisec int)

create NONCLUSTERD INDEX nci_t_dw_prodgroup_shift on t_dw_prodgroup_shift