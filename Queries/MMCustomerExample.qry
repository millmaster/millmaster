go
set parseonly off
go
/*************************************************************************
** File -----: W:\MillMaster\QUERIES\MMCustomerExample.QRY
** Author ---: Erich N�esch, LOEPFE AG Wetzikon CH
** Date -----: 02.10.2003
** Version --: 1.0 Initial release
**   30.09.03: Initial release.(Nue)
**   06.10.03: After tested by Nue.(Nue)
** Function -: TSQL-Examples for getting data from customer views from DB MM_Winding
****************************************************************************/
/*-------------------- Set the database ----------------------*/
USE MM_Winding
GO
/*Time dependent data access =======================================================================================*/
/*Week style production from a known lot ("Lot id")-------------------------------------------------------*/
SELECT "Style name", SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE c_style_id=(SELECT DISTINCT(c_style_id) FROM v_customer_production_shift WHERE "Lot id"=-2147481733)
        AND "Shift start" BETWEEN '2003-09-29 00:00' AND '2003-10-05 23:59'
  GROUP BY "Style name"
GO
/*Actual shift production from a known lot ("Lot id")-----------------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, ProdKm, ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND DATEADD(minute, "Shift length", "Shift start")>GETDATE()
GO
/*Last complete shift production from a known lot ("Lot id")----------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, ProdKm, ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND "Shift id"=(SELECT MAX("Shift id") FROM v_customer_production_shift
                          WHERE "Shift start"< DATEADD(minute, -"Shift length", GETDATE()))
GO
/*Last 3 complete shift production from a known lot ("Lot id")--------------------------------------------*/
SELECT top 3 "Lot id", ProdStart, ProdEnd, ProdKm, ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND "Shift start"< DATEADD(minute, -"Shift length", GETDATE())
  ORDER BY "Shift start" DESC
GO
/*Last complete day production from a known lot ("Lot id")------------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND DATEDIFF(day,"Shift start",GETDATE())=1
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*Last 3 complete days production from a known lot ("Lot id") 1 entry per "Lot id" -----------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND (DATEDIFF(day, "Shift start", GETDATE())>0) AND (DATEDIFF(day, "Shift start", GETDATE())<4)
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*Last 1 complete week production from a known lot ("Lot id") 1 entry per "Lot id" -----------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND DATEPART(week,"Shift start")=DATEPART(week,(DATEADD(week, -1, GETDATE())))
        AND DATEPART(year,"Shift start")=DATEPART(year,(DATEADD(week, -1, GETDATE())))
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*1 specific day production from a known lot ("Lot id")------------------------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND ShiftStartYear=2003 AND ShiftStartMonthInYear=9 AND ShiftStartDayInMonth=17
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*1 specific week production from a known lot ("Lot id")------------------------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND ShiftStartYear=2003 AND ShiftStartWeekInYear=38
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*1 specific month production from a known lot ("Lot id")------------------------------------------------------------*/
SELECT "Lot id", ProdStart, ProdEnd, SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE "Lot id"=-2147481733
        AND ShiftStartYear=2003 AND =9
  GROUP BY "Lot id", ProdStart, ProdEnd
GO
/*1 specific year production from a known style (c_style_id) on all machines ----------------------------------*/
SELECT "Style name", SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE c_style_id=(SELECT DISTINCT(c_style_id) FROM v_customer_production_shift WHERE "Lot id"=-2147481733)
        AND ShiftStartYear=2003
  GROUP BY "Style name"
GO
/*1 year production from a known style (c_style_id) 1 entry per lot--------------------------------------------*/
SELECT "Style name", "Lot id", SUM(ProdKm) ProdKm, SUM(ProdKg) ProdKg
  FROM v_customer_production_shift
  WHERE c_style_id=(SELECT DISTINCT(c_style_id) FROM v_customer_production_shift WHERE "Lot id"=-2147481733)
        AND ShiftStartYear=2003
  GROUP BY "Style name", "Lot id"
GO
/*Access to basic data =============================================================================================*/
/*Get Clearer settings from a known lot ("Lot id")--------------------------------------------------------*/
SELECT * FROM v_customer_YM_settings 
  WHERE c_ym_set_id=(SELECT DISTINCT(c_YM_set_id) FROM v_customer_production_shift WHERE "Lot id"=-2147482469)
GO
/*Get style information from a known lot ("Lot id")-------------------------------------------------------*/
SELECT * FROM v_customer_style 
  WHERE c_style_id=(SELECT DISTINCT(c_style_id) FROM v_customer_production_shift WHERE "Lot id"=-2147482469)
GO
/*Get data from machines ----------------------------------------------------------------------------------*/
SELECT * FROM v_customer_machines 
GO

