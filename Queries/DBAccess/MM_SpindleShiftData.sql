/*******************************************************************************
** MM_SpindleShiftData.sql
** 25.10.2007, Nue: Initial release
** Description: Handling of spindle/shift-related data storage
** RERUNABLE till 06.11.2007
********************************************************************************/

---------------------------------------------------------------------------------------------------------------
--Remarks:
----Der String "wetsrvmm5" muss in dieser Datei durch den entsprechenden Namen des Datenbankservers ersetzt werden
----auf welchem die MillMaster-DB "MM_Winding" l�uft.
----Die ben�tigten Dataitems m�ssen in den Tabellen "t_spindle_shift_data", "t_tmp_spindle_shift_data", 
----"t_spindle_shift_data_helper" und in den entsprechenden "INSERT INTO"-Statements in den Stored Procedures nach dem 
----Bed�rfnis erweitert werden. Zudem M�SSEN diese Dataitems an allen erw�hnten Stellen KORRELIEREN!!!!!!
----
----Hilfsqueries f�r Tests:-----------------
----drop table t_spindle_shift_data_helper
----drop table t_tmp_spindle_shift_data
----drop table t_spindle_shift_data
----drop table t_tmp_spindle_shift_data_helper
----drop procedure usp_periodicSpindleShiftConsolidation
----drop procedure usp_SpindleShiftConsolidation

---------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------
--Preparation
---------------------------------------------------------------------------------------------------------------
use master
select * from sysservers
--Falls Konsolidierung nicht innerhalb der MM-Datenbank passiert, muss dem "fremden" Server 
----der Server mit der MM-DB bekannt gemacht werden.
sp_addlinkedserver 'wetsrvmm5'
go
---------------------------------------------------------------------------------------------------------------
--Initialization
---------------------------------------------------------------------------------------------------------------
use MM_Winding  --Can be e.g. a RIETER DB

--Haupttabelle erstellen---------------------------------------------------------------------------------------
if not exists(select * from sysobjects where name='t_spindle_shift_data')
   create table t_spindle_shift_data 
  (c_spindle_id tinyint not null,
   c_machine_id smallint not null,
--     foreign key (c_spindle_id,c_machine_id) references t_spindle(c_spindle_id,c_machine_id),
--   c_interval_id tinyint not null,
--     foreign key (c_interval_id) references t_interval(c_interval_id),
   c_prod_id int not null,
--     foreign key (c_prod_id) references t_prodgroup(c_prod_id),
   c_fragshift_id int not null,
     constraint pk_t_spindle_shift_data primary key clustered(c_prod_id,c_fragshift_id,c_spindle_id),
--     foreign key (c_fragshift_id) references t_fragshift(c_fragshift_id),
   c_shift_start datetime not null,        /* Shiftstart */
   c_shift_length smallint not null,       /* Shiftduration in min */
   c_style_name varchar(50),               /* Style name */
   c_assortment_name varchar(50),          /* Assortment name */
   c_Len /*m_prod*/ int null,              /* produced m: Slip already accounted */
   c_Wei /*g_prod*/ numeric(9,2) null,     /* produced g: Slip already accounted */
   c_Bob real null,                        /* used bobins*100 */
   c_AdjustBase real null, /*tesssssstttttt*/                     
   c_Cones real null)                       /* produced cones*100 */


--Hilfstabellen erstellen--------------------------------------------------------------------------------------
if not exists(select * from sysobjects where name='t_tmp_spindle_shift_data')
--ACHTUNG: Die Tabelle "t_tmp_spindle_shift_data" muss eine Kopie von "t_spindle_shift_data" sein. (gleiche Struktur)
   create table t_tmp_spindle_shift_data 
  (c_spindle_id tinyint not null,
   c_machine_id smallint not null,
--     foreign key (c_spindle_id,c_machine_id) references t_spindle(c_spindle_id,c_machine_id),
--   c_interval_id tinyint not null,
--     foreign key (c_interval_id) references t_interval(c_interval_id),
   c_prod_id int not null,
--     foreign key (c_prod_id) references t_prodgroup(c_prod_id),
   c_fragshift_id int not null,
     constraint pk_t_tmp_spindle_shift_data primary key clustered(c_prod_id,c_fragshift_id,c_spindle_id),
--     foreign key (c_fragshift_id) references t_fragshift(c_fragshift_id),
   c_shift_start datetime not null,        /* Shiftstart */
   c_shift_length smallint not null,       /* Shiftduration in min */
   c_style_name varchar(50),               /* Style name */
   c_assortment_name varchar(50),          /* Assortment name */
   c_Len /*m_prod*/ int null,              /* produced m: Slip already accounted */
   c_Wei /*g_prod*/ numeric(9,2) null,     /* produced g: Slip already accounted */
   c_Bob real null,                        /* used bobins*100 */
   c_AdjustBase real null, /*tesssssstttttt*/                     
   c_Cones real null)                       /* produced cones*100 */

if not exists(select * from sysobjects where name='t_spindle_shift_data_helper')
   create table t_spindle_shift_data_helper 
  (c_spindle_id tinyint not null,
   c_machine_id smallint not null,
--     foreign key (c_spindle_id,c_machine_id) references t_spindle(c_spindle_id,c_machine_id),
   c_interval_id tinyint not null,
--     foreign key (c_interval_id) references t_interval(c_interval_id),
   c_prod_id int not null,
--     foreign key (c_prod_id) references t_prodgroup(c_prod_id),
   c_fragshift_id int not null,
     constraint pk_t_spindle_shift_data_helper primary key clustered(c_prod_id,c_fragshift_id,c_interval_id,c_spindle_id),
--     foreign key (c_fragshift_id) references t_fragshift(c_fragshift_id),
   c_shift_start datetime not null,        /* Shiftstart */
   c_shift_length smallint not null,       /* Shiftduration in min */
   c_style_name varchar(50),               /* Style name */
   c_assortment_name varchar(50),          /* Assortment name */
   c_Len /*m_prod*/ int null,              /* produced m: Slip already accounted */
   c_Wei /*g_prod*/ numeric(9,2) null,     /* produced g: Slip already accounted */
   c_Bob real null,                         /* used bobins*100 */
   c_AdjustBase real null, /*tesssssstttttt*/                     
   c_Cones real null)                       /* produced cones*100 */
   
if not exists(select * from sysobjects where name='t_tmp_spindle_shift_data_helper')
   create table t_tmp_spindle_shift_data_helper 
  (c_spindle_id tinyint not null,
   c_machine_id smallint not null,
--     foreign key (c_spindle_id,c_machine_id) references t_spindle(c_spindle_id,c_machine_id),
   c_interval_id tinyint not null,
--     foreign key (c_interval_id) references t_interval(c_interval_id),
   c_prod_id int not null,
--     foreign key (c_prod_id) references t_prodgroup(c_prod_id),
   c_fragshift_id int not null,
     constraint pk_t_tmp_spindle_shift_data_helper primary key clustered(c_prod_id,c_fragshift_id,c_interval_id,c_spindle_id))
   
--Stored Procedures erstellen---------------------------------------------------------------------------------------
go
create proc usp_spindleShiftConsolidation
--Diese Stored Procedure konsolidiert Spindeldaten in Intervalaufl�sung (aus Tabelle "t_spindle_shift_data_helper"),
----in Spindeldaten in (Frag)Schichtaufl�sung (in Tabelle "t_spindle_shift_data" (Hilfstabelle "t_tmp_spindle_shift_data" wird verwendet!)
----Innerhalb dieser Stored Procedure werden die tempor�ren Tabellen "t_tmp_fragshift_id","t_tmp_prod_id" und 
----"t_tmp_spindle_id" erstellt und wieder gel�scht.
AS
declare @c_fragshift_id int,
        @c_spindle_id tinyint,
        @c_prod_id int,
        @c_machine_id smallint,
	@c_shift_length smallint,
	@c_shift_start datetime,
	@c_style_name varchar(50),
        @c_assortment_name varchar(50)


select distinct c_fragshift_id into t_tmp_fragshift_id from t_spindle_shift_data_helper 
   order by c_fragshift_id asc

--LOOP �ber Fragshift
while (select count(*) from t_tmp_fragshift_id)>0
begin
  select @c_fragshift_id=(select MAX(c_fragshift_id) from t_tmp_fragshift_id)
  select distinct c_prod_id into t_tmp_prod_id from t_spindle_shift_data_helper
    where c_fragshift_id=@c_fragshift_id  
    order by c_prod_id asc

----LOOP �ber Prodgroups
  while (select count(*) from t_tmp_prod_id)>0
  begin
    select @c_prod_id=(select MAX(c_prod_id) from t_tmp_prod_id)
    select distinct c_spindle_id into t_tmp_spindle_id from t_spindle_shift_data_helper
      where c_fragshift_id=@c_fragshift_id and c_prod_id=@c_prod_id 
      order by c_spindle_id asc

    select top 1 @c_machine_id=c_machine_id, @c_shift_length=c_shift_length, @c_shift_start=c_shift_start,
                 @c_style_name=c_style_name, @c_assortment_name=c_assortment_name from t_spindle_shift_data_helper
      where c_fragshift_id=@c_fragshift_id and c_prod_id=@c_prod_id 

------LOOP �ber Spindlen
    while (select count(*) from t_tmp_spindle_id)>0
    begin
      select @c_spindle_id=(select MAX(c_spindle_id) from t_tmp_spindle_id)
      --F�llen der Hilfstabelle "t_tmp_spindle_shift_data" mit dem aggregierten Datensatz, weil Aggregationen im UPDATE-Statement nicht erlaubt sind!!
      insert INTO t_tmp_spindle_shift_data 
        select @c_spindle_id,
               @c_machine_id,
               @c_prod_id,
	       @c_fragshift_id,
               @c_shift_start,
               @c_shift_length,
               @c_style_name,
               @c_assortment_name,
	       SUM(c_Len),
	       SUM(c_Wei),
	       SUM(c_Bob),
               SUM(c_AdjustBase), /*tesssssstttttt*/                     
	       SUM(c_Cones)
           from t_spindle_shift_data_helper
           where (c_prod_id=@c_prod_id) and (c_spindle_id=@c_spindle_id) and (c_fragshift_id=@c_fragshift_id)

       --Check und execute von Update oder Insert in die Zieltabelle  
       if exists(select * from t_spindle_shift_data where c_spindle_id=@c_spindle_id and c_prod_id=@c_prod_id and c_fragshift_id=@c_fragshift_id)
         update t_spindle_shift_data 
           SET c_Len=q.c_Len+t.c_Len,
               c_Wei=q.c_Wei+t.c_Wei,
               c_Bob=q.c_Bob+t.c_Bob,
               c_AdjustBase=q.c_AdjustBase+t.c_AdjustBase,  /*tesssssstttttt*/
               c_Cones=q.c_Cones+t.c_Cones
           from t_tmp_spindle_shift_data as t INNER JOIN t_spindle_shift_data as q
             on t.c_spindle_id=q.c_spindle_id and
                t.c_prod_id=q.c_prod_id and
	        t.c_fragshift_id=q.c_fragshift_id
           where (t.c_prod_id=@c_prod_id) and (t.c_spindle_id=@c_spindle_id) and (t.c_fragshift_id=@c_fragshift_id)
       else
         insert INTO t_spindle_shift_data 
          select *
           from t_tmp_spindle_shift_data
--           where (c_prod_id=@c_prod_id) and (c_spindle_id=@c_spindle_id) and (c_fragshift_id=@c_fragshift_id)
       --L�schen des tempor�ren Eintrages 
       truncate table t_tmp_spindle_shift_data     
       
       delete t_tmp_spindle_id 
         where c_spindle_id=@c_spindle_id

    end
    drop table t_tmp_spindle_id
    delete t_tmp_prod_id 
      where c_prod_id=@c_prod_id

  end
  drop table t_tmp_prod_id
  delete t_tmp_fragshift_id 
    where c_fragshift_id=@c_fragshift_id

end
drop table t_tmp_fragshift_id
----End Stored Procedure -------------------------------------------------------------------------------
go
create proc usp_periodicSpindleShiftConsolidation
AS
--L�schen der Datens�tze in "t_tmp_spindle_shift_data_helper", welche nicht mehr in "...v_production_interval" sind
delete t_tmp_spindle_shift_data_helper 
  where c_fragshift_id<(select MIN(c_fragshift_id) from wetsrvmm5.mm_winding.dbo.v_production_interval)

--Kopieren der Datens�tze von "t_spindle_shift_data_helper" mit den relevanten Feldern nach "t_tmp_spindle_shift_data_helper"
  insert INTO t_tmp_spindle_shift_data_helper 
     select c_spindle_id,
	    c_machine_id,
            c_interval_id,
  	    c_prod_id,
	    c_fragshift_id
       from t_spindle_shift_data_helper

  truncate table t_spindle_shift_data_helper

  insert INTO t_spindle_shift_data_helper 
     select a.c_spindle_id,
		a.c_machine_id,
                a.c_interval_id,
		a.c_prod_id,
		a.c_fragshift_id,
                a.c_shift_start,
                a.c_shift_length,
                a.c_style_name,
                a.c_assortment_name,
		a.c_Len,
		a.c_Wei,
		a.c_Bob,
                a.c_AdjustBase, /*tesssssstttttt*/                     
		a.c_Cones
       from wetsrvmm5.mm_winding.dbo.v_production_interval AS a 
       where not exists(select * from t_tmp_spindle_shift_data_helper AS tmp where
              ((a.c_prod_id=tmp.c_prod_id) and 
               (a.c_fragshift_id=tmp.c_fragshift_id) and
               (a.c_machine_id=tmp.c_machine_id) and
               (a.c_interval_id=tmp.c_interval_id) and
               (a.c_spindle_id=tmp.c_spindle_id)))


  -- Konsolidiern der Daten und im Format Spindeldaten pro (Frag)Schicht in der Tabelle "t_spindle_shift_data" einf�gen
  execute usp_spindleShiftConsolidation

----End Stored Procedure -------------------------------------------------------------------------------
go
--Uebernehmen der Initialen Spindeldaten in den Helper
if (select count(*) from t_spindle_shift_data)=0
begin
  insert INTO t_spindle_shift_data_helper 
     select c_spindle_id,
		c_machine_id,
                c_interval_id,
		c_prod_id,
		c_fragshift_id,
                c_shift_start,
                c_shift_length,
                c_style_name,
                c_assortment_name,
		c_Len,
		c_Wei,
		c_Bob,
                c_AdjustBase, /*tesssssstttttt*/                     
		c_Cones
      from wetsrvmm5.mm_winding.dbo.v_production_interval
    --    order by 

  -- Konsolidiern der Daten und im Format Spindeldaten pro (Frag)Schicht in der Tabelle "t_spindle_shift_data" einf�gen
  execute usp_spindleShiftConsolidation
end


       
--LOOP
  -- ==> Mechanismus erstellen, welcher z.B. jede Stunde die Stored Procedure "usp_periodicSpindleShiftConsolidation" aufruft.
  -- Exec usp_periodicSpindleShiftConsolidation
  -- L�schen alter Daten (z.B. �lter als 1 Jahr) aus "t_spindle_shift_data", damit DB nicht voll l�uft!!!