declare @x integer

select @x=2
while @x<32768 begin 
    if NOT EXISTS (select distinct t_prodgroup.c_YM_set_id from t_prodgroup
         where t_prodgroup.c_YM_set_id= @x) AND
     NOT EXISTS (select distinct t_style_settings.c_YM_set_id from t_style_settings
         where t_style_settings.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_order_position.c_YM_set_id from t_order_position
         where t_order_position.c_YM_set_id = @x)
      delete t_ym_settings
        from t_ym_settings
        where (t_ym_settings.c_YM_set_id = @x) AND (t_ym_settings.c_YM_set_id>1) /* because the default set should never be deleted */
              AND (t_ym_settings.c_template_set=0) /* No template_set */
     select @x=@x+1
end
