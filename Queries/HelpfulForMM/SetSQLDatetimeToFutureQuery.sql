-- Nue: 13.1.09: Initial Release for DemoDB MillMaster
-- Mit dem vorliegenden Query werden auf einer Datenbank (hier MM_Winding) alle Zeitfelder vom Typ datetime und smalldatetime um 
-- @AnzTage in die Zukunft gestellt. (Wert '10000' f�r @AnzTage unten bei <select @AnzTage='10000'> entsprechend ersetzen!!)

declare @colname varchar(50), @tblname varchar(50), @cmd varchar(255), @id int, @colid smallint, @AnzTage varchar(10)
-- Eingabe der Anzahl Tage um die alle Datumsfelder der DB MM_Winding in die Zukunft gesetzt werden sollen
select @AnzTage='10000'
if exists (select name from sysobjects where name='tmp_columns')
  drop table tmp_columns
if not exists (select Distinct(c_fragshift_start) from t_dw_prodgroup_shift where datediff(day,c_fragshift_start,getdate())>@AnzTage) 
begin
  print 'Query NOT executed!!! Value for @AnzTage ist to high. Values would be in the future. Give a lower value!'
  select @AnzTage=min(datediff(day,c_fragshift_start,getdate())) from t_dw_prodgroup_shift 
  print 'The higest acceptable value is: '+@AnzTage
end
else begin
  select name,colid,id into tmp_columns from syscolumns where usertype=(select usertype from systypes where name like 'datetime') and id>10000 order by id,colid
  while (select count(*) from tmp_columns)>0 
  begin
    select top 1 @id=id, @colid=colid from tmp_columns order by id, colid
    select @colname=name from tmp_columns where id=@id and colid=@colid
    select @tblname='View'
    select @tblname=name from sysobjects where id=@id and type<>'V'
    delete tmp_columns where id=@id and colid=@colid
    if (@tblname<>'View')
    begin
      select @tblname, @colname
      select @cmd='select '+@colname+'=max('+@colname+') from '+@tblname
      exec (@cmd)
      select @cmd='update '+@tblname+' set '+@colname+'=dateadd(day,'+@AnzTage+','+@colname+') where '+@colname+' IS NOT NULL'
      select @cmd
      exec (@cmd)
    end  
  end
  drop table tmp_columns
end
