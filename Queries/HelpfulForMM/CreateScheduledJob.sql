use MM_Winding
go
/*Zeitrahmen des folgenden Queries ~15-900s; Es reindexiert alle Tabellen der aktuellen Datenbank */
exec sp_msforeachtable "print 'Table: ' + parsename('?',1); dbcc
dbreindex('?','')"
go


----------------------------------------------------------------------------------------------------
/* Add a job, jobstep, jobserver and schedule the job */
/* After ending the job a notification will be sent be e-mail and netsend */
/*   Before the operator 'nue' has to be created and his e-mail and netsend be configured */
use msdb
EXEC sp_add_job @job_name = 'ReindexAllTables', 
   @enabled = 1,
   @description = 'Reindexes all tables in actual database',
   @owner_login_name = 'mmsystemsql',
   @notify_level_eventlog = 3,
   @notify_level_email = 3, 
   @notify_level_netsend = 3, 
   @notify_email_operator_name = 'nue', 
   @notify_netsend_operator_name = 'nue' 
/* E-mail geht mit SQL2000 nur ab OOutlook2000!!!!! (oder man gibt im SQLServerAgent.Properties bei "mail session" irgendetwas ein!!??*/

/*Zeitrahmen des folgenden Queries ~15-900s; Es reindexiert alle Tabellen der aktuellen Datenbank */
EXEC sp_add_jobstep @job_name = 'ReindexAllTables',
   @step_name = 'ReindexAll',
   @subsystem = 'TSQL',
   @command = 'exec sp_msforeachtable "print ''Table: '' + parsename(''?'',1); dbcc dbreindex(''?'','''')"',
--   @server = '(local)',
   @database_name = 'MM_Winding' 

EXEC sp_add_jobserver
  @job_name='ReindexAllTables',
  @server_name='(local)'

EXEC sp_add_jobschedule @job_name = 'ReindexAllTables', 
   @name = 'ScheduledReindex',
   @freq_type = 8, /*weekly*/
   @freq_interval = 16,  /*Thuersday*/
   @freq_recurrence_factor = 1, /*Every 1 week */
   @active_start_time = 155500 /*Start at 15:55 */

-- EXEC sp_start_job @job_name = 'ReindexAllTables'  /* Sudden start of job */

---------------------------------------------------------------------------------------------------------------------
/* Start specified job 20 times every 5s */
declare @x integer
select @x=1
while @x<20 
begin
  WAITFOR DELAY '00:00:05'
  EXEC sp_start_job @job_name = 'ReindexAll'
  select @x=@x+1
end
---------------------------------------------------------------------------------------------------------------------

