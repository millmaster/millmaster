/*******************************************************************************
** DeleteUnusedSettings use for 4-2.qry
** 27.01.2009, Nue: L�scht kaskadiert alle ReinigerSettings in der Tabelle t_xml_ym_settings
**                  auf welche nirgends mehr referenziert wird.
********************************************************************************/
USE MM_Winding
go
select count(*) from t_xml_ym_settings
go

declare @x integer

select @x=2
while @x<10000 begin 
    if NOT EXISTS (select distinct t_prodgroup.c_YM_set_id from t_prodgroup
         where t_prodgroup.c_YM_set_id= @x) AND
     NOT EXISTS (select distinct t_style_settings.c_YM_set_id from t_style_settings
         where t_style_settings.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_order_position.c_YM_set_id from t_order_position
         where t_order_position.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_longterm_week.c_YM_set_id from t_longterm_week
         where t_longterm_week.c_YM_set_id = @x)
      delete t_xml_ym_settings
        from t_xml_ym_settings
        where (t_xml_ym_settings.c_YM_set_id = @x) AND (t_xml_ym_settings.c_YM_set_id>1) /* because the default set should never be deleted */
              AND (t_xml_ym_settings.c_template_set=0) /* No template_set */
     select @x=@x+1
end

go

declare @x integer

select @x=10000
while @x<20000 begin 
    if NOT EXISTS (select distinct t_prodgroup.c_YM_set_id from t_prodgroup
         where t_prodgroup.c_YM_set_id= @x) AND
     NOT EXISTS (select distinct t_style_settings.c_YM_set_id from t_style_settings
         where t_style_settings.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_order_position.c_YM_set_id from t_order_position
         where t_order_position.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_longterm_week.c_YM_set_id from t_longterm_week
         where t_longterm_week.c_YM_set_id = @x)
      delete t_xml_ym_settings
        from t_xml_ym_settings
        where (t_xml_ym_settings.c_YM_set_id = @x) AND (t_xml_ym_settings.c_YM_set_id>1) /* because the default set should never be deleted */
              AND (t_xml_ym_settings.c_template_set=0) /* No template_set */
     select @x=@x+1
end

go

declare @x integer

select @x=20000
while @x<30000 begin 
    if NOT EXISTS (select distinct t_prodgroup.c_YM_set_id from t_prodgroup
         where t_prodgroup.c_YM_set_id= @x) AND
     NOT EXISTS (select distinct t_style_settings.c_YM_set_id from t_style_settings
         where t_style_settings.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_order_position.c_YM_set_id from t_order_position
         where t_order_position.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_longterm_week.c_YM_set_id from t_longterm_week
         where t_longterm_week.c_YM_set_id = @x)
      delete t_xml_ym_settings
        from t_xml_ym_settings
        where (t_xml_ym_settings.c_YM_set_id = @x) AND (t_xml_ym_settings.c_YM_set_id>1) /* because the default set should never be deleted */
              AND (t_xml_ym_settings.c_template_set=0) /* No template_set */
     select @x=@x+1
end

go

declare @x integer

select @x=30000
while @x<32767 begin 
    if NOT EXISTS (select distinct t_prodgroup.c_YM_set_id from t_prodgroup
         where t_prodgroup.c_YM_set_id= @x) AND
     NOT EXISTS (select distinct t_style_settings.c_YM_set_id from t_style_settings
         where t_style_settings.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_order_position.c_YM_set_id from t_order_position
         where t_order_position.c_YM_set_id = @x) AND
     NOT EXISTS (select distinct t_longterm_week.c_YM_set_id from t_longterm_week
         where t_longterm_week.c_YM_set_id = @x)
      delete t_xml_ym_settings
        from t_xml_ym_settings
        where (t_xml_ym_settings.c_YM_set_id = @x) AND (t_xml_ym_settings.c_YM_set_id>1) /* because the default set should never be deleted */
              AND (t_xml_ym_settings.c_template_set=0) /* No template_set */
     select @x=@x+1
end
go
select count(*) from t_xml_ym_settings

go