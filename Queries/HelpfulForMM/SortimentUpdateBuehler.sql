/*Nue/Pke: 8.8.07
ACHTUNG NICHT RERUNABLE!!!!!  ja nicht 2x laufen lassen!! da sonst eine weitere Null engef�gt wird. Update der Style, Lot, Template  
Spezielles Query f�r B�hler Sennhof f�r die neue Sortimentsbenennung */

use MM_Winding
go
/*STYLES*/
declare @ind int
select @ind=(select max(c_style_id) from t_style)
/*ACHTUNG: DefaultStyle=1 beibehalten*/
while (@ind>1)
begin 
  if exists(select * from t_style where c_style_id=@ind)
    update t_style set c_style_name=SUBSTRING(c_style_name, 1, 6)+'0'+SUBSTRING(c_style_name, 7, len(c_style_name)-6)
      where c_style_id=@ind
  select @ind=@ind-1
end  
print 'Styles wurden ge�ndert'  

go
/*PRODGRUPPEN*/
declare @ind int, 
        @ind1 int
select @ind=(select min(c_prod_id) from t_prodgroup)
select @ind1=(select max(c_prod_id) from t_prodgroup)
/* c_prod_id's sind im negativen Bereich!!!!*/
while (@ind<=@ind1)
begin 
  if exists(select * from t_prodgroup where c_prod_id=@ind)
    update t_prodgroup set c_prod_name=SUBSTRING(c_prod_name, 1, 6)+'0'+SUBSTRING(c_prod_name, 7, len(c_prod_name)-6) 
      where c_prod_id=@ind
  select @ind=@ind+1
end  
print 'Lots wurden ge�ndert'  

go
/*REINIGERTEMPLATES*/
declare @ind int
select @ind=(select max(c_ym_set_id) from t_xml_ym_settings)
/*ACHTUNG: DefaultMemorxC=1 beibehalten*/
while (@ind>1)
begin 
  if exists(select * from t_xml_ym_settings where c_ym_set_id=@ind and c_template_set=1)
    update t_xml_ym_settings set c_YM_set_name=SUBSTRING(c_YM_set_name, 1, 5)+'0'+SUBSTRING(c_YM_set_name, 6, len(c_YM_set_name)-5) 
      where c_ym_set_id=@ind
  select @ind=@ind-1
end  
print 'Templates wurden ge�ndert'  


  