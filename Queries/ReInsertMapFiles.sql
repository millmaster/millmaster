USE MM_Winding
go
delete t_xml_mapfile
go
/*-----------------------------------------------------------------------------------------*/
/* Map-Dateien importieren */
/*-----------------------------------------------------------------------------------------*/
/*---- Einf�gen NO_MAP Mapfile ------*/
if not exists(select * from t_xml_mapfile where c_mapfile_id = 0)
  INSERT INTO t_xml_mapfile(c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id)
  VALUES     (0,'None','NO_MAP',0)

/*----Tempor�re Tabelle erstellen------*/
if not exists(select * from sysobjects where name='tmp_mapfile')
   create table tmp_mapfile (c_mapfile text not null)


declare @cmd varchar(1000), @Path varchar(255), @FileName varchar(255)
/*----MM-Pfad ab MasterDB holen------*/
select @cmd = 'D:\Millmaster'
if EXISTS(select * from master..t_MMWindingDBInstallSettings where c_description like 'MillmasterInstallRoot')
  select @cmd = (select distinct c_value from master..t_MMWindingDBInstallSettings where c_description like 'MillmasterInstallRoot')
select @Path=''''+@cmd+'\Queries\XMLFiles\'

/*----Einf�gen ZE-0101 Mapfile------*/
if NOT EXISTS(select * from t_xml_mapfile where c_mapfile_id = 1)
begin
  select @FileName='ZE-0101'
  select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
  select @cmd
  exec (@cmd)

  INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id) 
  SELECT 1, @FileName, c_mapfile, 1 FROM tmp_mapfile
  delete tmp_mapfile
end

/*----Einf�gen ZE-0201 Mapfile------*/
if NOT EXISTS(select * from t_xml_mapfile where c_mapfile_id = 2)
begin
  select @FileName='ZE-0201'
  select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
  select @cmd
  exec (@cmd)

  INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id) 
  SELECT 2, @FileName, c_mapfile, 1 FROM tmp_mapfile
  delete tmp_mapfile
end

/*----Einf�gen WSC-0101 Mapfile------*/
if NOT EXISTS(select * from t_xml_mapfile where c_mapfile_id = 3)
begin
  select @FileName='WSC-0101'
  select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
  select @cmd
  exec (@cmd)

  INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id) 
  SELECT 3, @FileName, c_mapfile, 2 FROM tmp_mapfile
  delete tmp_mapfile
end

/*----Einf�gen WSC-0201 Mapfile------*/
if NOT EXISTS(select * from t_xml_mapfile where c_mapfile_id = 4)
begin
  select @FileName='WSC-0201'
  select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
  select @cmd
  exec (@cmd)

  INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id) 
  SELECT 4, @FileName, c_mapfile, 2 FROM tmp_mapfile
  delete tmp_mapfile
end

/*----Einf�gen WSC-0301 Mapfile------*/
if NOT EXISTS(select * from t_xml_mapfile where c_mapfile_id = 5)
begin
  select @FileName='WSC-0301'
  select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
  select @cmd
  exec (@cmd)

  INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id) 
  SELECT 5, @FileName, c_mapfile, 2 FROM tmp_mapfile
  delete tmp_mapfile
end
GO
/*----L�schen der Tempor�ren Tabelle erstellen------*/
if exists(select * from sysobjects where name='tmp_mapfile')
   drop table tmp_mapfile

select * from t_xml_mapfile
