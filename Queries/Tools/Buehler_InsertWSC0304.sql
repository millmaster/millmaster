/*-----------------------------------------------------------------------------------------*/
/*-31.5.05--- Bugfix von Mapfiles --------*/
/*-----------------------------------------------------------------------------------------*/
/* Map-Dateien importieren */
/*-----------------------------------------------------------------------------------------*/
/*----Tempor�re Tabelle erstellen------*/
if not exists(select * from sysobjects where name='tmp_mapfile')
   create table tmp_mapfile (c_mapfile text not null)


declare @cmd varchar(1000), @Path varchar(255), @FileName varchar(255)
/*----MM-Pfad ab MasterDB holen------*/
select @cmd = 'D:\Millmaster'
if EXISTS(select * from master..t_MMWindingDBInstallSettings where c_description like 'MillmasterInstallRoot')
  select @cmd = (select distinct c_value from master..t_MMWindingDBInstallSettings where c_description like 'MillmasterInstallRoot')
select @Path=''''+@cmd+'\Queries\XMLFiles\'


/*----Einf�gen WSC-0302 Mapfile------*/
delete t_xml_mapfile where c_mapfile_id = /* ID einf�llen */
select @FileName='WSC-0304'
select @cmd='bulk insert tmp_mapfile from '+@Path+@FileName+'.map'' WITH (DATAFILETYPE = ''char'', ROWTERMINATOR=''\n'') '
exec (@cmd)

INSERT INTO t_xml_mapfile (c_mapfile_id, c_mapfile_name, c_mapfile, c_net_id)
SELECT /* ID einf�llen */, @FileName, c_mapfile, 2 FROM tmp_mapfile
delete tmp_mapfile
GO
/*----L�schen der Tempor�ren Tabelle erstellen------*/
if exists(select * from sysobjects where name='tmp_mapfile')
   drop table tmp_mapfile
GO

SELECT * from t_xml_mapfile
