/* Update 3.01.01-3.01.02: From 02.12.2002 Wss */
/* 02.12.2002, Wss: c_shift_in_day added to views v_production_shift, v_production_interval */
/* 12.12.2002, Nue: Datetypes in t_interval from smalldatetime to datetime */
/* 18.12.2002, Nue: exec sp_dboption 'MM_Winding', 'autoshrink', 'TRUE' added (Nue) */
/* RERUNABLE till 18.12.2002 */
/**********************************************************************************************/

USE MM_Winding
go
/*--Nue 18.12.02 --------------------------------------------------------------*/
exec sp_dboption 'MM_Winding', 'autoshrink', 'TRUE'
go
/*--Nue 12.12.02 --------------------------------------------------------------*/
alter table t_interval alter column c_interval_start datetime
alter table t_interval alter column c_interval_end datetime
go
/*-----------------------------------------------------------------------------*/


/*-----------------------------------------------------------------------------*/
/* drop and create views for shift and interval                                */
/*-----------------------------------------------------------------------------*/
if exists (select id from sysobjects where name like 'v_production_shift')
  drop view dbo.v_production_shift
go
if exists (select id from sysobjects where name like 'v_production_interval')
  drop view dbo.v_production_interval
go
PRINT 'Create view "v_production_shift"'
go
CREATE VIEW dbo.v_production_shift
AS
SELECT     dbo.t_shift.c_shiftcal_id, dbo.t_shift.c_shift_id, dbo.t_shift.c_shift_in_day, dbo.t_shift.c_shift_start, dbo.t_prodgroup.c_prod_id, dbo.t_prodgroup.c_prod_name,
                      dbo.t_prodgroup.c_spindle_first, dbo.t_prodgroup.c_spindle_last, dbo.t_prodgroup.c_prod_start, dbo.t_prodgroup.c_prod_end,
                      dbo.t_prodgroup.c_machine_id, dbo.t_prodgroup.c_machine_name, dbo.t_prodgroup.c_style_id, dbo.t_prodgroup.c_style_name,
                      dbo.t_assortment.c_assortment_name, dbo.t_prodgroup.c_act_yarncnt, dbo.t_prodgroup.c_yarncnt_unit, dbo.t_prodgroup.c_start_mode,
                      dbo.t_ym_settings.c_YM_set_id, dbo.t_ym_settings.c_YM_set_name, dbo.t_dw_prodgroup_shift.c_classCut_id,
                      dbo.t_dw_prodgroup_shift.c_classUncut_id, dbo.t_dw_prodgroup_shift.c_spliceCut_id, dbo.t_dw_prodgroup_shift.c_spliceUncut_id,
                      dbo.t_dw_prodgroup_shift.c_siroCutUncut_id, dbo.t_dw_prodgroup_shift.c_Len * 1.0 AS c_Len, dbo.t_dw_prodgroup_shift.c_Wei,
                      dbo.t_dw_prodgroup_shift.c_Bob, dbo.t_dw_prodgroup_shift.c_Cones, dbo.t_dw_prodgroup_shift.c_Red * 1.0 AS c_Red,
                      dbo.t_dw_prodgroup_shift.c_tRed * 1.0 AS c_tRed, dbo.t_dw_prodgroup_shift.c_tYell * 1.0 AS c_tYell,
                      dbo.t_dw_prodgroup_shift.c_ManSt * 1.0 AS c_ManSt, dbo.t_dw_prodgroup_shift.c_tManSt * 1.0 AS c_tManSt,
                      dbo.t_dw_prodgroup_shift.c_rtSpd * 1.0 AS c_rtSpd, dbo.t_dw_prodgroup_shift.c_otSpd * 1.0 AS c_otSpd,
                      dbo.t_dw_prodgroup_shift.c_wtSpd * 1.0 AS c_wtSpd, dbo.t_dw_prodgroup_shift.c_Sp * 1.0 AS c_Sp, dbo.t_dw_prodgroup_shift.c_RSp * 1.0 AS c_RSp,
                      dbo.t_dw_prodgroup_shift.c_YB * 1.0 AS c_YB, dbo.t_dw_prodgroup_shift.c_otProdGrp * 1.0 AS c_otProdGrp,
                      dbo.t_dw_prodgroup_shift.c_wtProdGrp * 1.0 AS c_wtProdGrp, dbo.t_dw_prodgroup_shift.c_LSt * 1.0 AS c_LSt,
                      dbo.t_dw_prodgroup_shift.c_LStProd * 1.0 AS c_LStProd, dbo.t_dw_prodgroup_shift.c_LStBreak * 1.0 AS c_LStBreak,
                      dbo.t_dw_prodgroup_shift.c_LStMat * 1.0 AS c_LStMat, dbo.t_dw_prodgroup_shift.c_LStRev * 1.0 AS c_LStRev,
                      dbo.t_dw_prodgroup_shift.c_LStClean * 1.0 AS c_LStClean, dbo.t_dw_prodgroup_shift.c_LStDef * 1.0 AS c_LStDef, 
                      dbo.t_dw_prodgroup_shift.c_CS * 1.0 AS c_CS, dbo.t_dw_prodgroup_shift.c_CL * 1.0 AS c_CL, dbo.t_dw_prodgroup_shift.c_CT * 1.0 AS c_CT,
                      dbo.t_dw_prodgroup_shift.c_CN * 1.0 AS c_CN, dbo.t_dw_prodgroup_shift.c_CSp * 1.0 AS c_CSp, dbo.t_dw_prodgroup_shift.c_CCl * 1.0 AS c_CCl,
                      dbo.t_dw_prodgroup_shift.c_LckCl * 1.0 AS c_LckCl, dbo.t_dw_prodgroup_shift.c_UClS, dbo.t_dw_prodgroup_shift.c_UClL,
                      dbo.t_dw_prodgroup_shift.c_UClT, dbo.t_dw_prodgroup_shift.c_COffCnt * 1.0 AS c_COffCnt, dbo.t_dw_prodgroup_shift.c_LckOffCnt * 1.0 AS c_LckOffCnt,
                      dbo.t_dw_prodgroup_shift.c_CBu * 1.0 AS c_CBu, dbo.t_dw_prodgroup_shift.c_CDBu * 1.0 AS c_CDBu,
                      dbo.t_dw_prodgroup_shift.c_CSys * 1.0 AS c_CSys, dbo.t_dw_prodgroup_shift.c_LckSys * 1.0 AS c_LckSys, 
                      dbo.t_dw_prodgroup_shift.c_CUpY * 1.0 AS c_CUpY, dbo.t_dw_prodgroup_shift.c_CYTot * 1.0 AS c_CYTot,
                      dbo.t_dw_prodgroup_shift.c_CSIRO * 1.0 AS c_CSIRO, dbo.t_dw_prodgroup_shift.c_LckSIRO * 1.0 AS c_LckSIRO,
                      dbo.t_dw_prodgroup_shift.c_CSIROCl * 1.0 AS c_CSIROCl, dbo.t_dw_prodgroup_shift.c_LckSIROCl * 1.0 AS c_LckSIROCl,
                      dbo.t_dw_prodgroup_shift.c_INeps * dbo.t_style.c_Factor_INeps AS c_INeps, 
                      dbo.t_dw_prodgroup_shift.c_ISmall * dbo.t_style.c_Factor_ISmall AS c_ISmall,
                      dbo.t_dw_prodgroup_shift.c_IThick * dbo.t_style.c_Factor_IThick AS c_IThick, dbo.t_dw_prodgroup_shift.c_IThin * dbo.t_style.c_Factor_IThin AS c_IThin,
                       dbo.t_dw_prodgroup_shift.c_I2_4 * 1.0 AS c_I2_4, dbo.t_dw_prodgroup_shift.c_I4_8 * 1.0 AS c_I4_8, 
                      dbo.t_dw_prodgroup_shift.c_I8_20 * 1.0 AS c_I8_20, dbo.t_dw_prodgroup_shift.c_I20_70 * 1.0 AS c_I20_70,
                      dbo.t_dw_prodgroup_shift.c_SFI * 1.0 AS c_SFI, dbo.t_dw_prodgroup_shift.c_SFICnt * 1.0 AS c_SFICnt, 
                      dbo.t_dw_prodgroup_shift.c_CSFI * 1.0 AS c_CSFI, dbo.t_dw_prodgroup_shift.c_LckSFI * 1.0 AS c_LckSFI,
                      dbo.t_dw_prodgroup_shift.c_AdjustBase * dbo.t_dw_prodgroup_shift.c_AdjustBaseCnt AS c_AdjustBase,
                      dbo.t_dw_prodgroup_shift.c_AdjustBaseCnt * 1.0 AS c_AdjustBaseCnt
FROM         dbo.t_prodgroup INNER JOIN
                      dbo.t_dw_prodgroup_shift INNER JOIN
                      dbo.t_shift_fragshift INNER JOIN
                      dbo.t_shift ON dbo.t_shift_fragshift.c_shift_id = dbo.t_shift.c_shift_id ON
                      dbo.t_dw_prodgroup_shift.c_fragshift_id = dbo.t_shift_fragshift.c_fragshift_id ON 
                      dbo.t_prodgroup.c_prod_id = dbo.t_dw_prodgroup_shift.c_prod_id INNER JOIN
                      dbo.t_ym_settings ON dbo.t_prodgroup.c_YM_set_id = dbo.t_ym_settings.c_YM_set_id INNER JOIN
                      dbo.t_style ON dbo.t_prodgroup.c_style_id = dbo.t_style.c_style_id INNER JOIN
                      dbo.t_assortment ON dbo.t_style.c_assortment_id = dbo.t_assortment.c_assortment_id


/*----------------------------------------------------------------*/
go
PRINT 'Create view "v_production_interval"'
go
CREATE VIEW dbo.v_production_interval
AS
SELECT     dbo.t_shift.c_shiftcal_id, dbo.t_shift.c_shift_id, dbo.t_shift.c_shift_in_day, dbo.t_shift.c_shift_start, dbo.t_interval.c_interval_id, dbo.t_interval.c_interval_start,
                      dbo.t_interval.c_interval_end, dbo.t_prodgroup.c_prod_id, dbo.t_prodgroup.c_prod_name, dbo.t_prodgroup.c_spindle_first,
                      dbo.t_prodgroup.c_spindle_last, dbo.t_prodgroup.c_prod_start, dbo.t_prodgroup.c_prod_end, dbo.t_prodgroup.c_machine_id,
                      dbo.t_prodgroup.c_machine_name, dbo.t_prodgroup.c_style_id, dbo.t_prodgroup.c_style_name, dbo.t_assortment.c_assortment_name, 
                      dbo.t_prodgroup.c_act_yarncnt, dbo.t_prodgroup.c_yarncnt_unit, dbo.t_prodgroup.c_start_mode, dbo.t_spindle_interval_data.c_spindle_id,
                      dbo.t_ym_settings.c_YM_set_id, dbo.t_ym_settings.c_YM_set_name, dbo.t_spindle_interval_data.c_class_image,
                      dbo.t_spindle_interval_data.c_Len * 1.0 AS c_Len, dbo.t_spindle_interval_data.c_Wei, dbo.t_spindle_interval_data.c_Bob, 
                      dbo.t_spindle_interval_data.c_Cones, dbo.t_spindle_interval_data.c_Red * 1.0 AS c_Red, dbo.t_spindle_interval_data.c_tRed * 1.0 AS c_tRed,
                      dbo.t_spindle_interval_data.c_tYell * 1.0 AS c_tYell, dbo.t_spindle_interval_data.c_ManSt * 1.0 AS c_ManSt, 
                      dbo.t_spindle_interval_data.c_tManSt * 1.0 AS c_tManSt, dbo.t_spindle_interval_data.c_rtSpd * 1.0 AS c_rtSpd, 
                      dbo.t_spindle_interval_data.c_otSpd * 1.0 AS c_otSpd, dbo.t_spindle_interval_data.c_wtSpd * 1.0 AS c_wtSpd,
                      dbo.t_spindle_interval_data.c_Sp * 1.0 AS c_Sp, dbo.t_spindle_interval_data.c_RSp * 1.0 AS c_RSp, dbo.t_spindle_interval_data.c_YB * 1.0 AS c_YB, 
                      dbo.t_spindle_interval_data.c_otProdGrp * 1.0 AS c_otProdGrp, dbo.t_spindle_interval_data.c_wtProdGrp * 1.0 AS c_wtProdGrp, 
                      dbo.t_spindle_interval_data.c_LSt * 1.0 AS c_LSt, dbo.t_spindle_interval_data.c_LStProd * 1.0 AS c_LStProd,
                      dbo.t_spindle_interval_data.c_LStBreak * 1.0 AS c_LStBreak, dbo.t_spindle_interval_data.c_LStMat * 1.0 AS c_LStMat, 
                      dbo.t_spindle_interval_data.c_LStRev * 1.0 AS c_LStRev, dbo.t_spindle_interval_data.c_LStClean * 1.0 AS c_LStClean, 
                      dbo.t_spindle_interval_data.c_LStDef * 1.0 AS c_LStDef, dbo.t_spindle_interval_data.c_CS * 1.0 AS c_CS,
                      dbo.t_spindle_interval_data.c_CL * 1.0 AS c_CL, dbo.t_spindle_interval_data.c_CT * 1.0 AS c_CT, dbo.t_spindle_interval_data.c_CN * 1.0 AS c_CN, 
                      dbo.t_spindle_interval_data.c_CSp * 1.0 AS c_CSp, dbo.t_spindle_interval_data.c_CCl * 1.0 AS c_CCl, 
                      dbo.t_spindle_interval_data.c_LckCl * 1.0 AS c_LckCl, dbo.t_spindle_interval_data.c_UClS * 1.0 AS c_UClS,
                      dbo.t_spindle_interval_data.c_UClL * 1.0 AS c_UClL, dbo.t_spindle_interval_data.c_UClT * 1.0 AS c_UClT,
                      dbo.t_spindle_interval_data.c_COffCnt * 1.0 AS c_COffCnt, dbo.t_spindle_interval_data.c_LckOffCnt * 1.0 AS c_LckOffCnt,
                      dbo.t_spindle_interval_data.c_CBu * 1.0 AS c_CBu, dbo.t_spindle_interval_data.c_CDBu * 1.0 AS c_CDBu, 
                      dbo.t_spindle_interval_data.c_CSys * 1.0 AS c_CSys, dbo.t_spindle_interval_data.c_LckSys * 1.0 AS c_LckSys,
                      dbo.t_spindle_interval_data.c_CUpY * 1.0 AS c_CUpY, dbo.t_spindle_interval_data.c_CYTot * 1.0 AS c_CYTot,
                      dbo.t_spindle_interval_data.c_CSIRO * 1.0 AS c_CSIRO, dbo.t_spindle_interval_data.c_LckSIRO * 1.0 AS c_LckSIRO,
                      dbo.t_spindle_interval_data.c_CSIROCl * 1.0 AS c_CSIROCl, dbo.t_spindle_interval_data.c_LckSIROCl * 1.0 AS c_LckSIROCl,
                      dbo.t_spindle_interval_data.c_INeps * dbo.t_style.c_Factor_INeps AS c_INeps,
                      dbo.t_spindle_interval_data.c_ISmall * dbo.t_style.c_Factor_ISmall AS c_ISmall,
                      dbo.t_spindle_interval_data.c_IThick * dbo.t_style.c_Factor_ISmall AS c_IThick,
                      dbo.t_spindle_interval_data.c_IThin * dbo.t_style.c_Factor_ISmall AS c_IThin, dbo.t_spindle_interval_data.c_I2_4 * 1.0 AS c_I2_4, 
                      dbo.t_spindle_interval_data.c_I4_8 * 1.0 AS c_I4_8, dbo.t_spindle_interval_data.c_I8_20 * 1.0 AS c_I8_20,
                      dbo.t_spindle_interval_data.c_I20_70 * 1.0 AS c_I20_70, dbo.t_spindle_interval_data.c_SFI * 1.0 AS c_SFI,
                      dbo.t_spindle_interval_data.c_SFICnt * 1.0 AS c_SFICnt, dbo.t_spindle_interval_data.c_CSFI * 1.0 AS c_CSFI, 
                      dbo.t_spindle_interval_data.c_LckSFI * 1.0 AS c_LckSFI, dbo.t_spindle_interval_data.c_AdjustBase,
                      dbo.t_spindle_interval_data.c_AdjustBaseCnt * 1.0 AS c_AdjustBaseCnt
FROM         dbo.t_shift_fragshift INNER JOIN
                      dbo.t_shift ON dbo.t_shift_fragshift.c_shift_id = dbo.t_shift.c_shift_id INNER JOIN
                      dbo.t_spindle_interval_data ON dbo.t_shift_fragshift.c_fragshift_id = dbo.t_spindle_interval_data.c_fragshift_id INNER JOIN
                      dbo.t_prodgroup ON dbo.t_spindle_interval_data.c_prod_id = dbo.t_prodgroup.c_prod_id INNER JOIN
                      dbo.t_interval ON dbo.t_spindle_interval_data.c_interval_id = dbo.t_interval.c_interval_id INNER JOIN
                      dbo.t_ym_settings ON dbo.t_prodgroup.c_YM_set_id = dbo.t_ym_settings.c_YM_set_id INNER JOIN
                      dbo.t_style ON dbo.t_prodgroup.c_style_id = dbo.t_style.c_style_id INNER JOIN
                      dbo.t_assortment ON dbo.t_style.c_assortment_id = dbo.t_assortment.c_assortment_id



/*-----------------------------------------------------------------------------*/
go
update t_mmuparm set Data = getDate() where AppName = 'MM_WindingDB' and AppKey = 'ActualDateUpdate'
update t_mmuparm set Data = '3.01.02' where AppName = 'MM_WindingDB' and AppKey = 'ActualVersionUpdate'
/*----------------------------------------------------------------*/
go
/**********************************************************************************************/
Print'Database MM_Winding update from 3.01.01-3.01.02 done on '+RTRIM(CONVERT(varchar(30), GETDATE()))
go
/*----------------------------------------------------------------*/

