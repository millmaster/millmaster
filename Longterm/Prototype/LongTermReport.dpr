program LongTermReport;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Forms,
  MainForm in 'MainForm.pas' {frmMain},
  DataModule in 'DataModule.pas' {mDM: TDataModule},
  BaseForm in '\\WETSRVBDE2\develop\LOEPFESHARES\TEMPLATES\BASEFORM.pas' {mmForm},
  LongTermDef in 'LongTermDef.pas',
  LabMasterDef in '\\wetsrvbde2\develop\MillMaster\GUI\MMLabReport\LabMasterDef.pas',
  BASEDialog in '\\wetsrvbde2\develop\LOEPFESHARES\TEMPLATES\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '\\wetsrvbde2\develop\LOEPFESHARES\TEMPLATES\BASEDIALOGBOTTOM.pas' {DialogBottom},
  AddReportDialog in 'AddReportDialog.pas' {dlgAddReport};

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TmDM, mDM);
  Application.Run;
end.
