inherited dlgAddReport: TdlgAddReport
  Left = 424
  Top = 123
  ActiveControl = edReportName
  Caption = 'Add Report'
  ClientHeight = 287
  ClientWidth = 385
  PixelsPerInch = 96
  TextHeight = 13
  object laReportName: TmmLabel [0]
    Left = 8
    Top = 5
    Width = 64
    Height = 13
    Caption = 'Report name:'
    FocusControl = edReportName
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object laComment: TmmLabel [1]
    Left = 8
    Top = 49
    Width = 47
    Height = 13
    Caption = 'Comment:'
    FocusControl = meComment
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  inherited bOK: TmmButton
    Left = 193
    Top = 257
  end
  inherited bCancel: TmmButton
    Left = 293
    Top = 257
  end
  object edReportName: TmmEdit
    Left = 8
    Top = 20
    Width = 121
    Height = 21
    Color = clWindow
    TabOrder = 2
    Visible = True
    AutoLabel.Control = laReportName
    AutoLabel.LabelPosition = lpTop
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object meComment: TmmMemo
    Left = 8
    Top = 64
    Width = 369
    Height = 185
    TabOrder = 3
    Visible = True
    AutoLabel.Control = laComment
    AutoLabel.LabelPosition = lpTop
  end
end
