unit AddReportDialog;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, mmMemo, mmLabel, mmEdit, mmButton;

type
  TdlgAddReport = class(TDialogBottom)
    edReportName: TmmEdit;
    laReportName: TmmLabel;
    laComment: TmmLabel;
    meComment: TmmMemo;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dlgAddReport: TdlgAddReport;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

end.
