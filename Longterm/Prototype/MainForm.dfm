object frmMain: TfrmMain
  Left = 241
  Top = 118
  Width = 950
  Height = 700
  Caption = 'LongTerm Report'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object pcMain: TmmPageControl
    Left = 0
    Top = 217
    Width = 942
    Height = 456
    ActivePage = tsCube
    Align = alClient
    TabOrder = 0
    OnChange = pcMainChange
    object tsCube: TTabSheet
      Caption = 'Cube'
      object mmPanel2: TmmPanel
        Left = 0
        Top = 0
        Width = 934
        Height = 428
        Align = alClient
        TabOrder = 0
        object pnPalette: TmmPanel
          Left = 1
          Top = 1
          Width = 200
          Height = 426
          Align = alLeft
          BevelOuter = bvNone
          BorderWidth = 2
          TabOrder = 0
          object mmLabel1: TmmLabel
            Left = 2
            Top = 2
            Width = 196
            Height = 15
            Align = alTop
            AutoSize = False
            Caption = 'Dimensions:'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object clbDimensions: TmmCheckListBox
            Left = 2
            Top = 17
            Width = 196
            Height = 407
            OnClickCheck = clbDimensionsClickCheck
            Align = alClient
            ItemHeight = 13
            TabOrder = 0
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
        end
        object mAC: TDDActiveCube
          Left = 201
          Top = 1
          Width = 732
          Height = 426
          ParentColor = False
          Align = alClient
          DragMode = dmAutomatic
          TabOrder = 1
          OnClick = mACClick
          OnCellClick = mACCellClick
          OnAxisItemClick = mACAxisItemClick
          ControlData = {
            3412445508000000A84B0000072C00002D040000A0050000D4D0C80001000000
            FFFFC000000000001A040000010000002C01000000000000000000000A246A00
            FFFFFF000A246A00FFFFFF000000010000000100000001000000000000000100
            0000000002000000808080000000000000000000000000000000000000000000
            00000000A0050000A0050000A0050000A0050000D0020000D002000000000000
            010000000100000000000000FFFFFFFF00000000FFFFFF000200000000000000
            000000000000000000000000FFFFFF00120023002C002300230023002E003000
            3000000000000000D4D0C80001000000D4D0C80001000000D4D0C80001000000
            D4D0C800010000001C004D0053002000530061006E0073002000530065007200
            6900660000000800000090010000000000000000000000000000000000000000
            0000B0138A05FFFFFFFF00000000D4D0C8000000000000000000000000000000
            000000000000FFFFFF00000001000000D4D0C80001000000D4D0C80001000000
            D4D0C80001000000D4D0C800010000001C004D0053002000530061006E007300
            2000530065007200690066000000080000009001000000000000000000000000
            00000000000000000000B0308806FFFFFFFF00000000D4D0C800000000000000
            0000000000000000000000000000FFFFFF00000001000000D4D0C80001000000
            D4D0C80001000000D4D0C80001000000D4D0C800010000001C004D0053002000
            530061006E007300200053006500720069006600000008000000900100000000
            000000000000000000000000000000000000B0308806FFFFFFFF0A246A00FFFF
            FF000000000000000000000000000000000000000000FFFFFF00000001000000
            D4D0C80001000000D4D0C80001000000D4D0C80001000000D4D0C80001000000
            1C004D0053002000530061006E00730020005300650072006900660000000800
            0000900100000000000000000000000000000000000000000000B0138A05FFFF
            FFFFD4D0C800808080000000000000000000000000000000000000000000FFFF
            FF00000001000000D4D0C80001000000D4D0C80001000000D4D0C80001000000
            D4D0C800010000001C004D0053002000530061006E0073002000530065007200
            6900660000000800000090010000000000000000000000000000000000000000
            0000B0138A050000000000000000000000000000000000000000000000000000
            00000000000000000000000000000000000000}
        end
      end
    end
    object tsChart: TTabSheet
      Caption = 'Chart'
      ImageIndex = 2
      OnShow = tsChartShow
      object mmPanel4: TmmPanel
        Left = 0
        Top = 0
        Width = 161
        Height = 428
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object laDimensions: TmmLabel
          Left = 0
          Top = 41
          Width = 57
          Height = 13
          Caption = 'Dimensions:'
          FocusControl = tvDimensions
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bUpdateChart: TmmButton
          Left = 0
          Top = 8
          Width = 153
          Height = 25
          Caption = 'Update Chart'
          TabOrder = 0
          Visible = True
          OnClick = bUpdateChartClick
          AutoLabel.LabelPosition = lpLeft
        end
        object tvDimensions: TmmTreeView
          Left = 0
          Top = 56
          Width = 153
          Height = 153
          DragMode = dmAutomatic
          Indent = 19
          TabOrder = 1
          Visible = True
          OnMouseDown = tvDimensionsMouseDown
          AutoLabel.Control = laDimensions
          AutoLabel.LabelPosition = lpTop
        end
      end
      object mmPanel5: TmmPanel
        Left = 161
        Top = 0
        Width = 773
        Height = 428
        Align = alClient
        BevelOuter = bvNone
        BorderWidth = 5
        TabOrder = 1
        object mmPanel6: TmmPanel
          Left = 5
          Top = 5
          Width = 763
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object mmLabel3: TmmLabel
            Left = 8
            Top = 5
            Width = 29
            Height = 13
            Caption = 'Z-Axis'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmSpeedButton1: TmmSpeedButton
            Left = 424
            Top = 0
            Width = 24
            Height = 22
            Action = acDeleteChartZAxis
            Flat = True
            Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00
              FF000000FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF000000FF000000FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000FF000000FF000000FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000FF000000FF0000008000FF00FF00FF00FF00FF00
              FF00FF00FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF000000FF000000FF0000008000FF00FF00FF00
              FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF000000FF00000080000000
              FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF000000FF000000
              8000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF000000FF00000080000000
              FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF000000FF000000FF0000008000FF00FF00FF00
              FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF000000FF000000FF000000FF0000008000FF00FF00FF00FF00FF00
              FF00FF00FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF000000FF000000FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF000000FF0000008000FF00FF00FF00FF00FF00FF00FF00
              FF00808080000000FF0080808000FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF000000FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
              FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00}
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object pnChartZAxis: TmmPanel
            Left = 48
            Top = 0
            Width = 377
            Height = 22
            Alignment = taLeftJustify
            BevelOuter = bvNone
            BorderWidth = 2
            BorderStyle = bsSingle
            Caption = 'Nothing selected'
            Color = clInfoBk
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            OnDragDrop = pnChartXYAxisDragDrop
            OnDragOver = pnChartXYAxisDragOver
          end
        end
        object mmPanel7: TmmPanel
          Left = 5
          Top = 366
          Width = 763
          Height = 57
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object mmLabel2: TmmLabel
            Left = 8
            Top = 5
            Width = 29
            Height = 13
            Caption = 'X-Axis'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object mmLabel4: TmmLabel
            Left = 8
            Top = 37
            Width = 47
            Height = 13
            Caption = 'Slicer Info'
            Visible = True
            AutoLabel.LabelPosition = lpLeft
          end
          object pnChartXAxis: TmmPanel
            Left = 64
            Top = 0
            Width = 377
            Height = 22
            Alignment = taLeftJustify
            BevelOuter = bvNone
            BorderWidth = 2
            BorderStyle = bsSingle
            Caption = 'Nothing selected'
            Color = clInfoBk
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 0
            OnDragDrop = pnChartXYAxisDragDrop
            OnDragOver = pnChartXYAxisDragOver
          end
          object pnSliceAxis: TmmPanel
            Left = 64
            Top = 32
            Width = 377
            Height = 22
            Alignment = taLeftJustify
            BevelOuter = bvNone
            BorderWidth = 2
            BorderStyle = bsSingle
            Caption = 'Nothing selected'
            Color = clInfoBk
            Ctl3D = False
            ParentCtl3D = False
            TabOrder = 1
          end
        end
        object mChart: TmmChart
          Left = 5
          Top = 29
          Width = 763
          Height = 337
          BackWall.Brush.Color = clWhite
          BackWall.Brush.Style = bsClear
          Title.Text.Strings = (
            '')
          BottomAxis.LabelsSeparation = 20
          BottomAxis.MinorTickCount = 4
          Legend.Alignment = laTop
          View3D = False
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          MouseMode = mmNormal
          object Series1: TBarSeries
            Marks.ArrowLength = 20
            Marks.Visible = True
            SeriesColor = clRed
            XValues.DateTime = False
            XValues.Name = 'X'
            XValues.Multiplier = 1
            XValues.Order = loAscending
            YValues.DateTime = False
            YValues.Name = 'Bar'
            YValues.Multiplier = 1
            YValues.Order = loNone
          end
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'TabSheet4'
      ImageIndex = 3
      object mmLabel8: TmmLabel
        Left = 432
        Top = 29
        Width = 50
        Height = 13
        Caption = 'ColAxis (X)'
        FocusControl = lbColAxis
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object mmLabel9: TmmLabel
        Left = 432
        Top = 149
        Width = 41
        Height = 13
        Caption = 'RowAxis'
        FocusControl = lbRowAxis
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laGetCell: TmmLabel
        Left = 8
        Top = 408
        Width = 42
        Height = 13
        Caption = 'laGetCell'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object meInfo: TmmMemo
        Left = 0
        Top = 32
        Width = 401
        Height = 249
        ScrollBars = ssVertical
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bColInfo: TmmButton
        Left = 0
        Top = 0
        Width = 75
        Height = 25
        Caption = 'ColInfo'
        TabOrder = 1
        Visible = True
        OnClick = bColInfoClick
        AutoLabel.LabelPosition = lpLeft
      end
      object bSlicerInfo: TmmButton
        Left = 264
        Top = 0
        Width = 75
        Height = 25
        Caption = 'SlicerInfo'
        TabOrder = 2
        Visible = True
        OnClick = bSlicerInfoClick
        AutoLabel.LabelPosition = lpLeft
      end
      object mmEdit1: TmmEdit
        Left = 8
        Top = 288
        Width = 313
        Height = 21
        Color = clWindow
        TabOrder = 3
        Text = 'mmEdit1'
        Visible = True
        AutoLabel.LabelPosition = lpLeft
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object bGetMembers: TmmButton
        Left = 328
        Top = 288
        Width = 75
        Height = 25
        Caption = 'GetMembers'
        TabOrder = 4
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object lbColAxis: TmmListBox
        Left = 432
        Top = 44
        Width = 113
        Height = 101
        Enabled = True
        ItemHeight = 13
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        Visible = True
        AutoLabel.Control = mmLabel8
        AutoLabel.LabelPosition = lpTop
      end
      object lbRowAxis: TmmListBox
        Left = 432
        Top = 164
        Width = 113
        Height = 109
        Enabled = True
        ItemHeight = 13
        TabOrder = 6
        Visible = True
        AutoLabel.Control = mmLabel9
        AutoLabel.LabelPosition = lpTop
      end
      object bUpdateChartInfo: TmmButton
        Left = 8
        Top = 344
        Width = 75
        Height = 25
        Caption = 'Update Info'
        TabOrder = 7
        Visible = True
        OnClick = bUpdateChartInfoClick
        AutoLabel.LabelPosition = lpLeft
      end
      object meGetCell: TmmMemo
        Left = 88
        Top = 344
        Width = 385
        Height = 73
        TabOrder = 8
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object bGetCell: TmmButton
        Left = 8
        Top = 376
        Width = 75
        Height = 25
        Caption = 'GetCell'
        TabOrder = 9
        Visible = True
        OnClick = bGetCellClick
        AutoLabel.LabelPosition = lpLeft
      end
      object edHighValue: TmmNumEdit
        Left = 88
        Top = 320
        Width = 77
        Height = 21
        Decimals = 2
        Digits = 12
        Masks.PositiveMask = '#,##0'
        NumericType = ntGeneral
        OnKeyDown = edHighValueKeyDown
        TabOrder = 10
        UseRounding = True
        Validate = False
        ValidateString = 
          '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
          's %s'
      end
      object meReport: TmmMemo
        Left = 552
        Top = 48
        Width = 361
        Height = 161
        TabOrder = 11
        Visible = True
        WordWrap = False
        AutoLabel.LabelPosition = lpLeft
      end
      object mmButton4: TmmButton
        Left = 712
        Top = 264
        Width = 75
        Height = 25
        Caption = 'mmButton4'
        TabOrder = 12
        Visible = True
        OnClick = mmButton4Click
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object mToolBar: TmmToolBar
    Left = 0
    Top = 0
    Width = 942
    Height = 29
    Flat = True
    Images = mImageList
    TabOrder = 1
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Action = acExit
    end
    object ToolButton2: TToolButton
      Left = 23
      Top = 0
      Action = acOpenProfile
    end
    object ToolButton3: TToolButton
      Left = 46
      Top = 0
      Action = acSaveProfile
    end
    object ToolButton4: TToolButton
      Left = 69
      Top = 0
      Width = 12
      Caption = 'ToolButton4'
      ImageIndex = 3
      Style = tbsSeparator
    end
    object bCubeChartMode: TToolButton
      Left = 81
      Top = 0
      AllowAllUp = True
      Caption = 'bCubeChartMode'
      ImageIndex = 8
      Style = tbsCheck
    end
    object ToolButton7: TToolButton
      Left = 104
      Top = 0
      Width = 33
      Caption = 'ToolButton7'
      ImageIndex = 5
      Style = tbsSeparator
    end
    object cbCubesPalette: TmmComboBox
      Left = 137
      Top = 0
      Width = 121
      Height = 21
      Color = clWindow
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
      Edit = False
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object ToolButton5: TToolButton
      Left = 258
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object bConnect: TmmSpeedButton
      Left = 266
      Top = 0
      Width = 71
      Height = 22
      Caption = 'Dis/Connect'
      Visible = True
      OnClick = bConnectClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 29
    Width = 942
    Height = 188
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object laMeasures: TmmLabel
      Left = 0
      Top = 1
      Width = 53
      Height = 13
      Caption = 'Data items:'
      FocusControl = lvMeasures
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelMeasures: TmmLabel
      Left = 112
      Top = 1
      Width = 96
      Height = 13
      Caption = 'Selected data items:'
      FocusControl = lvSelMeasures
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laReports: TmmLabel
      Left = 288
      Top = 1
      Width = 40
      Height = 13
      Caption = 'Reports:'
      FocusControl = lbReports
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bChartLine: TmmSpeedButton
      Tag = 101
      Left = 246
      Top = 19
      Width = 25
      Height = 25
      AllowAllUp = True
      GroupIndex = 2
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000C40E0000C40E00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
        FF7FFF7F7F7F7F7F7F7F7700000000000000FF0FFFFFFFFFFFFFF70FFFFFFFFF
        F999FF0FFFFFFFFFF9FF770FFFFFFFF99FFFFF0FFFFFFF9FFFFFF709FFFFF9FF
        FFFFFF0F9FFFF9FFFFFF770FF9FFF9FFFFFFFF0FF9FF9FFFFFFFF70FF9F9FFFF
        FFFFFF0FFF9FFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Visible = True
      OnClick = bChartTypeClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bChartBar: TmmSpeedButton
      Tag = 102
      Left = 246
      Top = 51
      Width = 25
      Height = 25
      AllowAllUp = True
      GroupIndex = 2
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000C40E0000C40E00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
        FF7FFF7F7F7F7F7F7F7F7700000000000000FF0FF22F44F11FFFF70FF22F44F1
        1FFFFF0FF22F44F11FFF770FF22F44F11FFFFF0FF22F44F11FFFF70FFFFF44F1
        1FFFFF0FFFFF44F11FFF770FFFFF44FFFFFFFF0FFFFF44FFFFFFF70FFFFFFFFF
        FFFFFF0FFFFFFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Visible = True
      OnClick = bChartTypeClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bChartArea: TmmSpeedButton
      Tag = 103
      Left = 246
      Top = 83
      Width = 25
      Height = 25
      AllowAllUp = True
      GroupIndex = 2
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000C40E0000C40E00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
        FF7FFF7F7F7F7F7F7F7F7700000000000000FF02222222222222F70222222222
        2222FF022222222222227702222222222222FF0222222222222FF70FF2222222
        22FFFF0FFFF222222FFF770FFFFF2222FFFFFF0FFFFFFFFFFFFFF70FFFFFFFFF
        FFFFFF0FFFFFFFFFFFFF770FFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Visible = True
      OnClick = bChartTypeClick
      AutoLabel.LabelPosition = lpLeft
    end
    object bLeftAxis: TmmSpeedButton
      Tag = 12
      Left = 112
      Top = 164
      Width = 24
      Height = 22
      GroupIndex = 1
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000C40E0000C40E00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FF7FFF7FFF7F
        FF7FFF7F7F7F7F7F7F7F7700000000000000FF0FFFFFFFFFFFFFF70FFFFFFFFF
        FFFFFF0FFFFFFFFFFFFF770FFFFFFFFFFFFFFF0FFFFFFFFFFFFFF70FFFFFFF00
        00FFFF0FFFFFFF0FFFFF770FFFFFFF0FFFFFFF0FFFFFFF0FFFFFF70FFFFFFF0F
        FFFFFF0FFFFFFF0FFFFF770FFFFFFF0FFFFFFFFFFFFFFFFFFFFF}
      Visible = True
      OnClick = bChartTypeClick
      AutoLabel.LabelPosition = lpLeft
    end
    object laAxisOrientation: TmmLabel
      Left = 140
      Top = 164
      Width = 73
      Height = 22
      Alignment = taCenter
      AutoSize = False
      Caption = '(*)Ausrichtung'
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bRightAxis: TmmSpeedButton
      Tag = 13
      Left = 216
      Top = 164
      Width = 24
      Height = 22
      GroupIndex = 1
      Enabled = False
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        04000000000080000000C40E0000C40E00001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00F7FFF7FFF7FF
        F7FFF7F7F7F7F7F7F7FF0000000000000077FFFFFFFFFFFFF0FFFFFFFFFFFFFF
        F07FFFFFFFFFFFFFF0FFFFFFFFFFFFFFF077FFFFFFFFFFFFF0FFFF0FF0FFFFFF
        F07FFF0FF0FFFFFFF0FFFF0F0FFFFFFFF077FF000FFFFFFFF0FFFF0FF0FFFFFF
        F07FFF0FF0FFFFFFF0FFFF000FFFFFFFF077FFFFFFFFFFFFFFFF}
      Visible = True
      OnClick = bChartTypeClick
      AutoLabel.LabelPosition = lpLeft
    end
    object laComment: TmmLabel
      Left = 544
      Top = 16
      Width = 313
      Height = 169
      AutoSize = False
      Color = clInfoBk
      ParentColor = False
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lvMeasures: TmmListView
      Left = 0
      Top = 16
      Width = 105
      Height = 169
      Columns = <>
      ReadOnly = True
      RowSelect = True
      ShowColumnHeaders = False
      SortType = stText
      TabOrder = 0
      ViewStyle = vsList
      Visible = True
      OnDblClick = lvMeasuresDblClick
      AutoLabel.Control = laMeasures
      AutoLabel.LabelPosition = lpTop
    end
    object lbReports: TmmListBox
      Left = 288
      Top = 16
      Width = 145
      Height = 169
      Enabled = True
      ItemHeight = 13
      TabOrder = 1
      Visible = True
      OnClick = lbReportsClick
      AutoLabel.Control = laReports
      AutoLabel.LabelPosition = lpTop
    end
    object bAddReport: TmmButton
      Left = 436
      Top = 16
      Width = 75
      Height = 25
      Action = acAddReport
      Caption = 'Save Report'
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bUpdateReport: TmmButton
      Left = 436
      Top = 96
      Width = 75
      Height = 25
      Action = acShowReport
      Caption = 'Update'
      TabOrder = 3
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmButton3: TmmButton
      Left = 436
      Top = 48
      Width = 75
      Height = 25
      Action = acDeleteReport
      TabOrder = 4
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lvSelMeasures: TmmListView
      Tag = 2
      Left = 112
      Top = 16
      Width = 129
      Height = 144
      Hint = '(*)Ziehen Sie Datenelemente hierher'
      Color = clInfoBk
      Columns = <
        item
          Width = 100
        end>
      Ctl3D = False
      DragMode = dmAutomatic
      HideSelection = False
      MultiSelect = True
      OwnerDraw = True
      ReadOnly = True
      RowSelect = True
      ShowColumnHeaders = False
      SmallImages = imgChartMeasures
      TabOrder = 5
      ViewStyle = vsReport
      Visible = True
      OnDblClick = lvSelMeasuresDblClick
      OnDrawItem = lvSelMeasuresDrawItem
      OnDragDrop = lvSelMeasuresDragDrop
      OnDragOver = lvSelMeasuresDragOver
      OnSelectItem = lvSelMeasuresSelectItem
      AutoLabel.Control = laSelMeasures
      AutoLabel.LabelPosition = lpTop
    end
    object bColorSelect: TmmColorButton
      Tag = 8
      Left = 246
      Top = 115
      Width = 25
      Height = 25
      AutoLabel.Distance = 5
      AutoLabel.LabelPosition = lpTop
      Caption = '&Weitere...'
      Color = clBlue
      OnChangeColor = bChartTypeClick
      Enabled = False
      TabOrder = 6
      TabStop = True
      Visible = True
    end
    object mCS: TCubeSource
      Left = 456
      Top = 136
      Width = 32
      Height = 32
      ControlData = {
        34124455080000004F0300004F0300002D0400000C0043007500620065003100
        00000000}
    end
  end
  object dsCubeInfo: TmmDataSource
    DataSet = mDM.adoCubeInfo
    Left = 496
    Top = 8
  end
  object mActionList: TmmActionList
    Images = mImageList
    Left = 532
    Top = 8
    object acMeasuresUp: TAction
      Tag = -1
      ImageIndex = 5
    end
    object acMeasuresDown: TAction
      Tag = 1
      ImageIndex = 6
    end
    object acExit: TAction
      ImageIndex = 0
      OnExecute = acExitExecute
    end
    object acOpenProfile: TAction
      ImageIndex = 1
      OnExecute = acOpenProfileExecute
    end
    object acSaveProfile: TAction
      ImageIndex = 2
      OnExecute = acSaveProfileExecute
    end
    object acAddReport: TAction
      Caption = 'Add Report'
      OnExecute = acAddReportExecute
    end
    object acShowReport: TAction
      Caption = 'Show Report'
      OnExecute = acShowReportExecute
    end
    object acDeleteReport: TAction
      Caption = 'Delete Report'
      OnExecute = acDeleteReportExecute
    end
    object acDeleteChartZAxis: TAction
      ImageIndex = 3
      OnExecute = acDeleteChartZAxisExecute
    end
  end
  object mImageList: TmmImageList
    Left = 464
    Top = 8
    Bitmap = {
      494C010109000E00040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000004000000001002000000000000040
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000008080000080800000808000008080000080800000808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      800000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000080800000FF
      FF0000FFFF00FFFFFF00FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF000080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF000000000000000000FFFFFF00FFFFFF0000FFFF0000FFFF0000FF
      FF00008080000000000000000000008080000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF0000FF
      FF0000FFFF000080800000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000080800000808000008080000080
      8000008080000080800000000000000000000000000000000000FFFFFF0000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF0000000000000000000000000000000000000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000FFFF0000FFFF0000FFFF0000FF
      FF000080800000000000000000000000000000000000000000000000000000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000080800000000000000000000000000000000000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00FFFFFF0000FFFF0000FF
      FF0000FFFF0000FFFF0000808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FFFFFF00000000000000000000FF
      FF0000FFFF0000FFFF0000FFFF00008080000080800000000000000000000080
      8000008080000080800000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF00008080000080800000FF
      FF0000FFFF0000FFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000FFFFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000808080000000000080808000000000008080800000000000808080000000
      0000808080000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00FF000000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00FF00
      0000FF000000FF000000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000000000000080808000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FF000000FF00
      0000C0C0C000FF000000FF000000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FF000000FF000000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000008080800080808000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FF000000FF000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FF000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000000000000080808000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FF00
      0000FF000000FF000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FF000000FF00
      0000FFFFFF00FF000000FF000000C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000008080800080808000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00FF000000FF000000C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00FF000000FF000000C0C0C000FFFFFF00C0C0
      C000FFFFFF00C0C0C000FFFFFF00000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFFFF00C0C0C000FFFF
      FF00C0C0C000FFFFFF00C0C0C000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF00FFFF0000FFFF
      0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF
      0000FFFFFF00FFFF0000FFFFFF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      8000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000808000000000000000000000000000000000000000FF000000
      FF00000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000080
      8000008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000080
      8000008080000080800000808000008080000080800000808000008080000000
      0000000000000000000000000000000000000000000000000000008080000080
      8000000000000000000000000000000000000000000000000000000000000000
      00000000000000808000000000000000000000000000000000000000FF000000
      FF000000FF000000800000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000000000000008080000080
      8000008080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000FFFF00000000000080
      8000008080000080800000808000008080000080800000808000008080000080
      8000000000000000000000000000000000000000000000000000008080000080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      FF000000FF000000FF0000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000000000000008080000080
      80000080800000000000FFFFFF00FFFFFF000000000000000000000000000000
      00000000000000000000000000000000000000000000FFFFFF0000FFFF000000
      0000008080000080800000808000008080000080800000808000008080000080
      8000008080000000000000000000000000000000000000000000008080000080
      8000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      00000000FF000000FF0000008000000000000000000000000000000000000000
      FF00000080000000000000000000000000000000000000000000008080000080
      80000000000000000000FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008000000000000000000000000000000000FFFF00FFFFFF0000FF
      FF00000000000080800000808000008080000080800000808000008080000080
      8000008080000080800000000000000000000000000000000000008080000080
      8000008080000080800000808000008080000080800000808000008080000080
      8000008080000080800000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000800000000000000000000000FF000000
      8000000000000000000000000000000000000000000000000000008080000080
      80000080800000000000FFFFFF00FFFFFF000000000000000000000000000000
      00008000000080000000000000000000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000080
      8000000000000000000000000000000000000000000000000000000000000000
      0000008080000080800000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF00000080000000FF00000080000000
      0000000000000000000000000000000000000000000000000000008080000080
      80000080800000000000FFFFFF00FFFFFF000000000000000000000000008000
      0000800000008000000080000000800000000000000000FFFF00FFFFFF0000FF
      FF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      00000000000000000000000000000000FF000000FF0000008000000000000000
      0000000000000000000000000000000000000000000000000000008080000080
      80000080800000000000FFFFFF00FFFFFF000000000000000000800000008000
      00008000000080000000800000008000000000000000FFFFFF0000FFFF00FFFF
      FF0000FFFF00FFFFFF0000FFFF00FFFFFF0000FFFF00FFFFFF00000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      000000000000000000000000FF000000FF00000080000000FF00000080000000
      0000000000000000000000000000000000000000000000000000008080000080
      80000080800000000000FFFFFF00FFFFFF000000000000000000000000008000
      0000800000008000000080000000800000000000000000FFFF00FFFFFF0000FF
      FF00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      0000000000000000FF000000FF000000800000000000000000000000FF000000
      8000000000000000000000000000000000000000000000000000008080000080
      800000000000FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000800000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080800000000000000000000000000000000000000000000000
      FF000000FF000000FF0000008000000000000000000000000000000000000000
      FF00000080000000000000000000000000000000000000000000008080000000
      0000FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000008000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000FF000000
      FF000000FF000000800000000000000000000000000000000000000000000000
      00000000FF00000080000000000000000000000000000000000000000000FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000008080000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      FF00808080000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000400000000100010000000000000200000000000000000000
      000000000000000000000000FFFFFF0000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000FFFF000000000000F03F000000000000
      E01F000000000000C00F000000000000C606000000000000FF80000000000000
      03C000000000000007E000000000000007E000000000000003C0000000000000
      01FF0000000000006063000000000000F003000000000000F807000000000000
      FC0F000000000000FFFF000000000000DDDDFFFFFFFF0000D555FFFFFFFF0000
      0000FFFFFFFF0000D927FFFFFFFF00009927F01FFEFF0000D927F01FFEFF0000
      1927F83FFC7F0000D927F83FFC7F00009F27FC7FF83F0000DF27FC7FF83F0000
      1F3FFEFFF01F0000DF3FFEFFF01F00009FFFFFFFFFFF0000DFFFFFFFFFFF0000
      1FFFFFFFFFFF0000FFFFFFFFFFFF0000FBFFFFFFFFFFFFFFF3FFFFFFC001EFFD
      E3FF001F8031C7FFC3FF000F8031C3FB000700078031E3F7807F00038001F1E7
      807B00018001F8CF807300008001FC1F8060001F8FF1FE3F8040001F8FF1FC1F
      8060001F8FF1F8CF80738FF18FF1E1E7807BFFF98FF1C3F3807FFF758FF5C7FD
      807FFF8F8001FFFFFFFFFFFFFFFFFFFF00000000000000000000000000000000
      000000000000}
  end
  object mSettingsIniDB: TSettingsIniDB
    Left = 568
    Top = 10
  end
  object imgChartMeasures: TmmImageList
    Left = 368
    Top = 16
    Bitmap = {
      494C010103000400040010001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000001000000001002000000000000010
      0000000000000000000000000000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000808080000000
      0000000000000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000808080000000
      0000808080000000000080808000000000008080800000000000808080000000
      0000808080000000000080808000000000000000000000000000808080000000
      0000808080000000000080808000000000008080800000000000808080000000
      0000808080000000000080808000000000000000000000000000808080000000
      0000808080000000000080808000000000008080800000000000808080000000
      0000808080000000000080808000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004000E0004000E0004000E0000000000080808000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000080808000000000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004000E00000000000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000004000
      E0004000E0000000000000000000000000008080800080808000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000008080800080808000000000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000008000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000004000E0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000080000000800000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000080
      0000008000000080000000800000008000000080000000800000008000000080
      0000008000000080000000800000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000004000
      E00000000000000000000000000000000000000000004000E000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000080808000000000000000
      0000000000000080000000800000008000000080000000800000008000000080
      0000008000000080000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00004000E000000000000000000000000000000000004000E000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      8000000080000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000008000000080000000800000008000000080
      0000008000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000004000E0000000000000000000000000004000E000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000080000000800000008000000080
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000004000E00000000000000000004000E00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008000000080000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000004000E000000000004000E0000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000004000E000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008080800080808000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000040000000100000000100010000000000800000000000000000000000
      000000000000000000000000FFFFFF00DDDDDDDDDDDD0000D555D555D5550000
      0000000000000000DFFFD927C00000009FF8992780000000DFFBD927C0000000
      1FE7192700000000DFDFD927C00100008FBF9F2798030000D7BFDF27DE070000
      1BBF1F3F1F0F0000DB7FDF3FDFFF00009AFF9FFF9FFF0000DDFFDFFFDFFF0000
      1FFF1FFF1FFF0000FFFFFFFFFFFF000000000000000000000000000000000000
      000000000000}
  end
end
