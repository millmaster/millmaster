unit LongTermDef;

interface
uses
  Classes, SysUtils, mmList, LabMasterDef;

type
  PMeasureDefRec = ^TMeasureDefRec;
  TMeasureDefRec = record
    Name: WideString;
    DisplayName: String;
    InDataArea: Boolean;
    ChartIndex: Integer;
    CubeIndex: Integer;
//    CubePos: Integer;
    ChartType: TChartType;
    Color: Integer;
    Formula: WideString;
    RightAxis: Boolean;
  end;

  TMeasuresList = class(TmmList)
  private
    function GetItems(aIndex: Integer): PMeasureDefRec;
  public
    property Items[aIndex: Integer]: PMeasureDefRec read GetItems;
    procedure AddNew(aMeasure: TMeasureDefRec);
    procedure Assign(Source: TObject); override;
    procedure Delete(Index: Integer); override;
    function IndexOf(const S: string): Integer;
    function PackPropertyString(aIndex: Integer): String;
    function UnpackPropertyString(aString: String): PMeasureDefRec;
  end;

procedure FillFromConstArray(aMeasuresList: TMeasuresList);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmStringList;

const
{
  Color: 16744703)
  Color: 16711808)

  Color: 14079702)
  Color: 204), //
  Color: 183), //
  Color: 7783934),
  Color: 12711922)
  Color: 293070),
  Color: 15573757)
  Color: 14411488)
{}
  cMeasuresArr: Array[0..7] of TMeasureDefRec = (
    (Name: 'Length'; DisplayName: 'Length [km]'; Color: 14802077; Formula:'[C Len]/1000'),
    (Name: 'Weight'; DisplayName: 'Weight [kg]'; Color: 32896;    Formula:'[C Wei]/1000'),
    (Name: 'CYTot';  DisplayName: 'CYTot/DU';    Color: 4210688;  Formula:'[C CYTot]*100000/([C Len]+1)'),
    (Name: 'CFF';    DisplayName: 'CFF/DU';      Color: 12615808; Formula:'[C CSiro]*100000/([C Len]+1)'),
    (Name: 'INeps';  DisplayName: 'INeps/km';    Color: 1341291;  Formula:'[C INeps]*1000/([C Len]+1)'),
    (Name: 'ISmall'; DisplayName: 'ISmall/m';    Color: 1341291;  Formula:'[C ISmall]*1/([C Len]+1)'),
    (Name: 'IThick'; DisplayName: 'IThick/km';   Color: 1424106;  Formula:'[C IThick]*1000/([C Len]+1)'),
    (Name: 'IThin';  DisplayName: 'IThin/km';    Color: 16711680; Formula:'[C IThin]*1000/([C Len]+1)')
  );

//------------------------------------------------------------------------------
procedure FillFromConstArray(aMeasuresList: TMeasuresList);
var
  i: Integer;
begin
  aMeasuresList.Clear;
  for i:=0 to High(cMeasuresArr) do
    aMeasuresList.AddNew(cMeasuresArr[i]);
end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// TMeasuresList
//------------------------------------------------------------------------------
procedure TMeasuresList.AddNew(aMeasure: TMeasureDefRec);
var
  xRec: PMeasureDefRec;
begin
  new(xRec);
  with xRec^ do begin
    Name        := aMeasure.Name;
    DisplayName := aMeasure.DisplayName;
    Formula     := aMeasure.Formula;
    InDataArea  := aMeasure.InDataArea;
    Color       := aMeasure.Color;
    ChartIndex  := -1;
    CubeIndex   := -1;
    ChartType   := ctLine;
  end;
  Add(xRec);
end;
//------------------------------------------------------------------------------
procedure TMeasuresList.Assign(Source: TObject);
var
  i: Integer;
begin
  if Source is TMeasuresList then begin
    Clear;
    with TMeasuresList(Source) do begin
      for i:=0 to Count-1 do
        Self.AddNew(Items[i]^);
    end;
  end else
    inherited Assign(Source);
end;
//------------------------------------------------------------------------------
procedure TMeasuresList.Delete(Index: Integer);
var
  xRec: PMeasureDefRec;
begin
  xRec := Items[Index];
  if Assigned(xRec) then
    Dispose(xRec);
  inherited Delete(Index);
end;
//------------------------------------------------------------------------------
function TMeasuresList.GetItems(aIndex: Integer): PMeasureDefRec;
begin
  Result := inherited Items[aIndex];
end;
//------------------------------------------------------------------------------
function TMeasuresList.IndexOf(const S: string): Integer;
begin
  for Result:=0 to Count - 1 do
    if AnsiCompareText(Items[Result]^.Name, S) = 0 then Exit;
  Result := -1;
end;
//------------------------------------------------------------------------------
function TMeasuresList.PackPropertyString(aIndex: Integer): String;
begin
  with TIniStringList.Create do
  try
    with Items[aIndex]^ do begin
      Values['Name']        := Name;
      Values['DisplayName'] := DisplayName;
      Values['InDataArea']  := InDataArea;
      Values['ChartIndex']  := ChartIndex;
      Values['CubeIndex']   := CubeIndex;
      Values['ChartType']   := ChartType;
      Values['Color']       := Color;
      Values['Formula']     := Formula;
      Values['RightAxis']   := RightAxis;
    end;
    Result := CommaText;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
function TMeasuresList.UnpackPropertyString(aString: String): PMeasureDefRec;
var
  xRec: TMeasureDefRec;
  xIndex: Integer;
begin
  Result := Nil;
  xIndex := -1;
  with TIniStringList.Create do
  try
    CommaText := aString;
    with xRec do begin
      Name        := Values['Name'];
      DisplayName := Values['DisplayName'];
      InDataArea  := Values['InDataArea'];
      ChartIndex  := Values['ChartIndex'];
      CubeIndex   := Values['CubeIndex'];
      ChartType   := Values['ChartType'];
      Color       := Values['Color'];
      Formula     := Values['Formula'];
      RightAxis   := Values['RightAxis'];
      xIndex      := Self.IndexOf(Name);
    end;
  finally
    Free;
  end;
  if xIndex <> -1 then begin
    Result  := Items[xIndex];
    Result^ := xRec;
  end;
{
  xIndex := -1;
  with TIniStringList.Create do
  try
    CommaText := aString;
    with xRec do begin
      Name        := Values['Name'];
      DisplayName := Values['DisplayName'];
      Formula     := Values['Formula'];
      InDataArea  := Values['InDataArea'];
      xIndex := Self.IndexOf(Name);
    end;
  finally
    Free;
  end;
  if xIndex <> -1 then
    Items[xIndex]^ := xRec;
{}
end;
//------------------------------------------------------------------------------
end.
