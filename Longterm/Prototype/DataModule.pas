unit DataModule;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, ADODB, mmADOConnection, mmADODataSet;

type
  TmDM = class(TDataModule)
    mConnection: TmmADOConnection;
    mDataSet: TmmADODataSet;
    adoCubeInfo: TmmADODataSet;
  private
    function GetQueryData(aFields: String): Boolean;
  public
    function GetCubeList(var aList: TStringList): Boolean;
    function GetMeasureList(var aList: TStringList): Boolean;
    function GetDimensionList(var aList: TStringList): Boolean;
  end;

var
  mDM: TmDM;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  RzCSIntf;

//------------------------------------------------------------------------------
// TmDM
//------------------------------------------------------------------------------
function TmDM.GetCubeList(var aList: TStringList): Boolean;
begin
  aList.Clear;
  mConnection.OpenSchema(siCubes, EmptyParam, EmptyParam, mDataSet);
  with mDataSet do begin
    Open;
    while not EOF do begin
      aList.Add(FieldByName('CUBE_NAME').AsString);
      Next;
    end;
    CodeSite.SendString('CubeList', FieldList.CommaText);
  end;
end;
//------------------------------------------------------------------------------
function TmDM.GetQueryData(aFields: String): Boolean;
const
  cQryData = 'select %s, SUM(c_Len*1.0), SUM(c_Wei*1.0) from v_production_shift ' +
             'where c_shift_start >= ''5/1/2002'' ' +
             'group by %s';
begin
  with mDataSet do begin
    Close;
    CommandText := Format(cQryData, [aFields, aFields]);
    Open;
    while not EOF do begin
//      aList.Add(FieldByName('CUBE_NAME').AsString);
      Next;
    end;
    CodeSite.SendString('CubeList', FieldList.CommaText);
  end;
end;
//------------------------------------------------------------------------------
function TmDM.GetDimensionList(var aList: TStringList): Boolean;
begin
  aList.Clear;
  mConnection.OpenSchema(siDimensions, EmptyParam, EmptyParam, mDataSet);
  with mDataSet do begin
    Open;
    while not EOF do begin
      aList.Add(FieldByName('DIMENSION_NAME').AsString);
      Next;
    end;
    CodeSite.SendString('DimensionList', FieldList.CommaText);
  end;
end;
//------------------------------------------------------------------------------
function TmDM.GetMeasureList(var aList: TStringList): Boolean;
begin
  aList.Clear;
  mConnection.OpenSchema(siMeasures, EmptyParam, EmptyParam, mDataSet);
  with mDataSet do begin
    Open;
    while not EOF do begin
      aList.Add(FieldByName('MEASURE_NAME').AsString);
      Next;
    end;
    CodeSite.SendString('MeasuresList', FieldList.CommaText);
  end;
end;
//------------------------------------------------------------------------------
end.

