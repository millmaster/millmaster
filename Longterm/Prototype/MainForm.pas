unit MainForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  OleServer, ActiveCube_TLB, OleCtrls, CubeSource_TLB, StdCtrls, mmMemo,
  mmButton, ComCtrls, mmTreeView, ExtCtrls, mmPanel, mmLabel, Buttons,
  mmSpeedButton, mmListBox, ADOMD_TLB, mmPageControl, Db, mmDataSource,
  mmComboBox, Grids, DBGrids, mmDBGrid, ADODB, CheckLst, mmCheckListBox,
  TeeProcs, TeEngine, Chart, mmChart, Series, mmEdit, mmNumCtrl, ActnList,
  mmActionList, ImgList, mmImageList, ToolWin, mmToolBar, SettingsIniDB,
  mmStringList, Spin, mmImage, mmColorButton, mmListView, IniFiles,
  LongTermDef;

type
  TfrmMain = class(TForm)
    pcMain: TmmPageControl;
    tsCube: TTabSheet;
    dsCubeInfo: TmmDataSource;
    tsChart: TTabSheet;
    mActionList: TmmActionList;
    acMeasuresUp: TAction;
    TabSheet4: TTabSheet;
    meInfo: TmmMemo;
    bColInfo: TmmButton;
    bSlicerInfo: TmmButton;
    mmEdit1: TmmEdit;
    bGetMembers: TmmButton;
    mToolBar: TmmToolBar;
    mmPanel2: TmmPanel;
    pnPalette: TmmPanel;
    mAC: TDDActiveCube;
    mImageList: TmmImageList;
    clbDimensions: TmmCheckListBox;
    mmLabel1: TmmLabel;
    acMeasuresDown: TAction;
    ToolButton1: TToolButton;
    acExit: TAction;
    mSettingsIniDB: TSettingsIniDB;
    acSaveProfile: TAction;
    acOpenProfile: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    acAddReport: TAction;
    acShowReport: TAction;
    acDeleteReport: TAction;
    acDeleteChartZAxis: TAction;
    imgChartMeasures: TmmImageList;
    lbColAxis: TmmListBox;
    mmLabel8: TmmLabel;
    lbRowAxis: TmmListBox;
    mmLabel9: TmmLabel;
    ToolButton4: TToolButton;
    cbCubesPalette: TmmComboBox;
    ToolButton5: TToolButton;
    bConnect: TmmSpeedButton;
    laGetCell: TmmLabel;
    bUpdateChartInfo: TmmButton;
    meGetCell: TmmMemo;
    bGetCell: TmmButton;
    edHighValue: TmmNumEdit;
    mmPanel1: TmmPanel;
    laMeasures: TmmLabel;
    laSelMeasures: TmmLabel;
    laReports: TmmLabel;
    lvMeasures: TmmListView;
    lbReports: TmmListBox;
    bAddReport: TmmButton;
    bUpdateReport: TmmButton;
    mmButton3: TmmButton;
    meReport: TmmMemo;
    mmButton4: TmmButton;
    bChartLine: TmmSpeedButton;
    bChartBar: TmmSpeedButton;
    bChartArea: TmmSpeedButton;
    bLeftAxis: TmmSpeedButton;
    laAxisOrientation: TmmLabel;
    bRightAxis: TmmSpeedButton;
    lvSelMeasures: TmmListView;
    bColorSelect: TmmColorButton;
    mmPanel4: TmmPanel;
    bUpdateChart: TmmButton;
    laDimensions: TmmLabel;
    tvDimensions: TmmTreeView;
    mmPanel5: TmmPanel;
    mmPanel6: TmmPanel;
    mmPanel7: TmmPanel;
    mChart: TmmChart;
    Series1: TBarSeries;
    mmLabel3: TmmLabel;
    pnChartZAxis: TmmPanel;
    mmSpeedButton1: TmmSpeedButton;
    mmLabel2: TmmLabel;
    pnChartXAxis: TmmPanel;
    pnSliceAxis: TmmPanel;
    mmLabel4: TmmLabel;
    laComment: TmmLabel;
    bCubeChartMode: TToolButton;
    ToolButton7: TToolButton;
    mCS: TCubeSource;
    procedure bConnectClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure bSlicerInfoClick(Sender: TObject);
    procedure bColInfoClick(Sender: TObject);
    procedure bUpdateChartInfoClick(Sender: TObject);
    procedure mACAxisItemClick(Sender: TObject; const ClickAxis: IACAxis; const ClickDimension: IACDimension; const ClickMember: IACMember);
    procedure bGetCellClick(Sender: TObject);
    procedure edHighValueKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure clbDimensionsClickCheck(Sender: TObject);
    procedure mACCellClick(Sender: TObject; const ClickCell: IACCell);
    procedure mACClick(Sender: TObject);
    procedure lvMeasuresDblClick(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acOpenProfileExecute(Sender: TObject);
    procedure acSaveProfileExecute(Sender: TObject);
    procedure acAddReportExecute(Sender: TObject);
    procedure acShowReportExecute(Sender: TObject);
    procedure lbReportsClick(Sender: TObject);
    procedure acDeleteReportExecute(Sender: TObject);
    procedure mmButton4Click(Sender: TObject);
    procedure FillChartDimensionList;
    procedure tvDimensionsMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure tsChartShow(Sender: TObject);
    procedure pnChartXYAxisDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure pnChartXYAxisDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure acDeleteChartZAxisExecute(Sender: TObject);
    procedure lvSelMeasuresDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
    procedure lvSelMeasuresDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure lvSelMeasuresSelectItem(Sender: TObject; Item: TListItem; Selected: Boolean);
    procedure bChartTypeClick(Sender: TObject);
    procedure lvSelMeasuresDrawItem(Sender: TCustomListView; Item: TListItem; Rect: TRect; State: TOwnerDrawState);
    procedure bUpdateChartClick(Sender: TObject);
    procedure pcMainChange(Sender: TObject);
    procedure lvSelMeasuresDblClick(Sender: TObject);
  private
    mBaseMeasuresPos: Integer;
    mCubeSelectionChanged: Boolean;
    mEnabledChanges: Boolean;
    mMeasures: TMeasuresList;
    mMemIni: TMemIniFile;
//    procedure FillMeasures;
    function GetCheckCount(aCheckListBox: TCheckListBox): Integer;
    procedure AddMem(aStr: String); overload;
    procedure AddMem(aStr: String; const Args: array of const); overload;
    procedure ShowDim(aLB: TListBox; aDim: IACDimension);
    procedure ReorderingMeasures;
    procedure InitReport;
    procedure MoveMeasure(aShowMeasures: Boolean);
    procedure ShowChartPropFromDataItem(aItemIndex: Integer);
    procedure OnChartItemsChange(aTagId: Integer);
    procedure UpdateChart;
    procedure ShowReport;
    procedure ActivateMeasureList;
    function InCubeMode: Boolean;
    procedure ResetMeasures;
    procedure WriteChartOptions(aMemIni: TMemIniFile);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  RzCSIntf, LoepfeGlobal, MMSeries,
  DataModule, AddReportDialog, LabMasterDef;

const
  cDimPrefix = 'Dimension';
  cMeasuresPrefix = 'Measure';

type
  PDimensionInfo = ^TDimensionInfo;
  TDimensionInfo = class
    DimIndex: Integer;
    Level: Integer;
  end;

{
  TChartItemDef = class
    ImageIndex: Integer;
    ChartType: Integer;
    Position: Integer;
    Color: Integer;
    RightAxis: Boolean;
  end;
{}

resourcestring
  rsNothingSelected = 'Nothing selected';

//------------------------------------------------------------------------------
{
procedure TfrmMain.FillMeasures;
var
  i: Integer;
  xV1, xV2: OLEVariant;
  xItem: TListItem;
begin
//  lbChartMeasures.Clear;
  lvSelMeasures.Items.Clear;
  with mAC.Measures do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      if Item(xV1).InDataArea then begin
//        lbChartMeasures.Items.Add(Item(xV1).Name);
        xItem := lvSelMeasures.Items.Add;
        with xItem do begin
          Caption := Item(xV1).Name;
          ImageIndex := 0;
          Data    := TChartItemDef.Create;
          with TChartItemDef(Data) do begin
            ImageIndex := 0;
            ChartType := 0;
            Position := xItem.Index;
            Color    := clTeeColor;
            RightAxis := False;
          end;
        end;
      end;
    end;
  end;
end;
{}
//------------------------------------------------------------------------------
procedure TfrmMain.bConnectClick(Sender: TObject);
var
  xIDim: IACDimension;
  xV1: OLEVariant;
  xMeasure: IACMeasure;
  i: Integer;
  xItem: TListItem;
  xPRec: PMeasureDefRec;
begin
  if mAC.CubeSource = Nil then begin
    with mCS do begin
      CubeName := cbCubesPalette.Text;
    end;
    mAC.CubeSource := mCS.DefaultInterface;
    mAC.FormulaBarAlwaysVisible := True;
    with mAC do begin
      with Dimensions do
        for i:=0 to Count-1 do begin
          xV1 := i;
          clbDimensions.Items.AddObject(Item(xV1).Name, TObject(i));
        end;

      with Measures do begin
        mBaseMeasuresPos := Count;
        for i:=0 to mMeasures.Count-1 do begin
          xPRec := mMeasures.Items[i];
          with Add(xPRec^.Name) do begin
            InDataArea := False;

            xItem := lvMeasures.Items.Add;
            with xItem do begin
              Caption := Name;
              Data    := xPRec;
            end;
          end;
        end;
      end;
    end;
    ActivateMeasureList;
  end else begin
    mAC.CubeSource := Nil;
    mAC.Clear(False);
  end;

//  FillMeasures;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ActivateMeasureList;
var
  i: Integer;
  xV1: OLEVariant;
  xPRec: PMeasureDefRec;
  xListItem: TListItem;
  xVisible: Boolean;
begin
//  lvMeasures.Items.BeginUpdate;
//  lvSelMeasures.Items.BeginUpdate;
  for i:=0 to mMeasures.Count-1 do begin
    xPRec := mMeasures.Items[i];
    with xPRec^ do begin
      xV1 := Name;
      // Eigenschaften in den Cube uebernehmen
      with mAC.Measures.Item(xV1) do begin
        Caption    := DisplayName;
        Formula    := xPRec^.Formula
      end;

      // wenn selektiert, dann Aktivieren
      xVisible := ((CubeIndex <> -1)  and InCubeMode) or
                  ((ChartIndex <> -1) and not InCubeMode);
      if xVisible then
        xListItem := lvMeasures.FindCaption(0, Name, False, True, False)
      else
        xListItem := lvSelMeasures.FindCaption(0, Name, False, True, False);

      if Assigned(xListItem) then begin
        xListItem.Selected := True;
        MoveMeasure(xVisible);
      end;
    end; // with xPRec
  end; // with mAC
//  lvMeasures.Items.EndUpdate;
//  lvSelMeasures.Items.EndUpdate;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FormShow(Sender: TObject);
var
  i: Integer;
  xOLEVariant: OLEVariant;
  xStrList: TStringList;
begin
  EnterMethod('FormShow');
  cbCubesPalette.Text := 'YarnDataLongterm';
{
  xStrList := TStringList.Create;
  with mDM do begin
    GetCubeList(xStrList);
    with cbCubesPalette do begin
      Items := xStrList;
      ItemIndex := Items.IndexOf('YarnDataLongterm');
    end;
    GetDimensionList(xStrList);
    GetMeasureList(xStrList);
  end;
  xStrList.Free;
{}
  edHighValue.Value := 100000;


  mCS.ConnectionString := 'Data Source=WETSRVBDE2;Initial Catalog=Mehalla';
  mCS.CubeName         := 'YarnData';

  mAC.Clear(False);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bSlicerInfoClick(Sender: TObject);
var
  i: Integer;
  xV1: OLEVariant;
begin   //             mAC.SlicerAxis.Dimensions.
  meInfo.Clear;
  with mAC.SlicerAxis.Dimensions do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      meInfo.Lines.Add(Format('%s = %s', [Item(xV1).Name, Item(xV1).Caption]));
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bColInfoClick(Sender: TObject);
var
  i, j, k: Integer;
  xV1, xV2, xV3: OLEVariant;
  xMem: IACMember;
begin   //             mAC.SlicerAxis.Dimensions.
  meInfo.Clear;
  with mAC.Dimensions do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      with Item(xV1) do begin
        if Orientation <> OTNone then begin
          meInfo.Lines.Add(Format('%s = %s, Lvls: %d', [Name, Caption, Levels.Count]));
          meInfo.Lines.Add(Format(' Position = %d', [Position]));
          if Orientation = OTSlicer then begin
            xMem := SlicerMember;
//            meInfo.Lines.Add(Format(' SlicerMember.Name = %s', [SlicerMember.Name]));
          end;
//          meInfo.Lines.Add(Format(' ActiveHierarchy.Name = %s', [ActiveHierarchy.Name]));
          for j:=0 to Levels.Count-1 do begin
            xV2 := j;
            with Levels.Item(xV2) do begin

              meInfo.Lines.Add(Format('  Dimension %d = %s, %s', [j, Name, Caption]));
              meInfo.Lines.Add(Format('   Numbers = %d', [Number]));
              meInfo.Lines.Add(Format('   UnuqueName = %s', [UniqueName]));
//              meInfo.Lines.Add(Format('   RankExpression = %s', [RankExpression]));
//              meInfo.Lines.Add(Format('   RankDescending = %d', [Integer(RankDescending)]));
              meInfo.Lines.Add(Format('   Orientation = %d', [Integer(Orientation)]));
//              meInfo.Lines.Add(Format('   ActiveHierarchy.Name = %s', [ActiveHierarchy.Name]));

              meInfo.Lines.Add(Format('   Members = %d', [Members.Count]));
            end; // with xV2
          end; // for j
        end;
      end;
    end;
  end;
end;

//------------------------------------------------------------------------------
procedure TfrmMain.AddMem(aStr: String);
begin
  meInfo.Lines.Add(aStr);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.AddMem(aStr: String; const Args: array of const);
begin
  AddMem(Format(aStr, Args));
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bUpdateChartInfoClick(Sender: TObject);
begin
//  FillMeasures;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mACAxisItemClick(Sender: TObject;
  const ClickAxis: IACAxis; const ClickDimension: IACDimension;
  const ClickMember: IACMember);
begin
{
  with ClickAxis do
    CodeSite.SendFmtMsg('AxisItemClick: Depth=%d', [VisibleDepth]);
  with clickDimension do
    CodeSite.SendFmtMsg('  Dim=%s, Unique=%s', [ClickDimension.Name, ClickDimension.UniqueName]);
  with ClickMember do
    CodeSite.SendFmtMsg('  Member=%s, Unuque=%s', [ClickMember.Name, ClickMember.UniqueName]);
//  CodeSite.SendFmtMsg('  ColumnAxix.ExpandState    = %d', [clickaxis.GetAxisVector(ClickMember.Name).ExpandState]);
//  CodeSite.SendFmtMsg('  ColumnAxix.DrilldownState = %d', [clickaxis.GetAxisVector(ClickMember.Name).DrillDownState]);
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bGetCellClick(Sender: TObject);
var
  xWStr: WideString;
begin
  try
    if meGetCell.SelLength > 0 then
      xWStr := meGetCell.SelText
    else
      xWStr := meGetCell.Text;
    laGetCell.Value := mAC.GetCell(xWStr).Value;
  except
    laGetCell.Caption := 'Error';
  end;
end;
//------------------------------------------------------------------------------
function TfrmMain.GetCheckCount(aCheckListBox: TCheckListBox): Integer;
var
  i: Integer;
begin
  Result := 0;
  with aCheckListBox do
    for i:=0 to Items.Count-1 do
      if Checked[i] then inc(Result);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ShowDim(aLB: TListBox; aDim: IACDimension);
var
  i: Integer;
  xV1: OLEVariant;
begin
  aLB.Clear;
  aLB.Tag := aDim.Position;
  with aDim.Levels do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      aLB.Items.Add(Item(xV1).Name);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.edHighValueKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    if edHighValue.AsInteger = 0 then
      mChart.LeftAxis.Automatic := True
    else
      mChart.LeftAxis.SetMinMax(0, edHighValue.Value);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.clbDimensionsClickCheck(Sender: TObject);
var
  i: Integer;
  xV1: OLEVariant;
begin
  mCubeSelectionChanged := True;
  with clbDimensions do begin
    xV1 := Integer(Items.Objects[ItemIndex]);
    if Checked[ItemIndex] then mAC.Dimensions.Item(xV1).Orientation := OTRow
                          else mAC.Dimensions.Item(xV1).Orientation := OTNone;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mACCellClick(Sender: TObject; const ClickCell: IACCell);
begin
//
end;

procedure TfrmMain.mACClick(Sender: TObject);
begin
//
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvMeasuresDblClick(Sender: TObject);
begin
  MoveMeasure(Sender = lvMeasures);

  ReorderingMeasures;
  mCubeSelectionChanged := True;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ReorderingMeasures;
var
  i: Integer;
  xV1: OLEVariant;
begin
{
  with lvSelMeasures.Items do begin
    for i:=0 to Count-1 do begin
      xV1 := PMeasureDefRec(Item[i].Data)^.Name;
      with mAC.Measures.Item(xV1) do begin
        Position := mBaseMeasuresPos + i;
      end;
    end;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acExitExecute(Sender: TObject);
begin
  Close;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acOpenProfileExecute(Sender: TObject);
begin
  with mSettingsIniDB do begin
    if LoadSettings then begin

    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acSaveProfileExecute(Sender: TObject);
begin
  with mSettingsIniDB do begin
    if SaveSettings then begin

    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acAddReportExecute(Sender: TObject);
var
  xName: String;
  xStrList: TStringList;
  //......................................................................
  procedure SaveDimensions;
  var
    i: Integer;
    xV1: OLEVariant;
    xStr: String;
    xCount: Integer;
    xStrList: TmmStringList;
  begin
    xCount := 0;
    xStrList := TmmStringList.Create;
    with mAC.Dimensions do
    try
      for i:=0 to Count-1 do begin
        xV1 := i;
        with Item(xV1) do begin
          // nur die Dimensionen beachten welche auch verwendet sind
          if Orientation <> OTNone then begin
            xStrList.Clear;
            with Item(xV1) do begin
              xStrList.AddVar(Orientation);
              if CompareText(Name, 'Measures') <> 0 then
                xStrList.AddVar(Position);

              mMemIni.WriteString(cDimPrefix, Name, xStrList.CommaText);
            end;
            inc(xCount);
          end; // if
        end; // with
      end; // for
    finally
      xStrList.Free;
    end; // with
  end;
  //......................................................................
  procedure SaveMeasures;
  var
    i: Integer;
    xRec: PMeasureDefRec;
  begin
    with mMeasures do begin
      for i:=0 to Count-1 do begin
        xRec := Items[i];
        mMemIni.WriteString(cMeasuresPrefix, xRec^.Name, PackPropertyString(i));
      end;
    end;
  end;
  //......................................................................
begin
  xName := '';
  with TdlgAddReport.Create(Self) do
  try
    if lbReports.ItemIndex <> -1 then begin
      edReportName.Text    := lbReports.Items.Strings[lbReports.ItemIndex];
      meComment.Lines.Text := Self.laComment.Caption;
    end;

    if ShowModal = mrOK then begin
      xName := edReportName.Text;
      if Trim(xName) <> '' then begin
        lbReports.ItemIndex := lbReports.Items.IndexOf(xName);
        if lbReports.ItemIndex <> -1 then
          acDeleteReport.Execute;

        // neues ConfigIni objekt erstellen und in Liste ablegen
        mMemIni := TMemIniFile.Create(Format('.\%s.ini', [xName]));
        lbReports.Items.AddObject(xName, mMemIni);
        Self.laComment.Caption := meComment.Lines.Text;

        // Konfig in ConfigIni schreiben
        SaveDimensions;
        SaveMeasures;
        with mMemIni do begin
          WriteString('General', 'Comment', meComment.Lines.CommaText);
          WriteBool('General', 'CubeChartMode', bCubeChartMode.Down);
        end;

        WriteChartOptions(mMemIni);

        // debug info
        mMemini.GetStrings(meReport.Lines);
      end;
    end;
  finally
    Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.WriteChartOptions(aMemIni: TMemIniFile);
begin
  with aMemIni do begin
    if pnChartXAxis.Tag <> 0 then
      WriteString('ChartOptions', 'XAxis', pnChartXAxis.Caption);

    if pnChartZAxis.Tag <> 0 then
      WriteString('ChartOptions', 'ZAxis', pnChartZAxis.Caption);
  end;
end;
//------------------------------------------------------------------------------
constructor TfrmMain.Create(aOwner: TComponent);
var
  i: Integer;
  xSections: TStringList;
  xStrList: TStringList;
  xMemIni: TMemIniFile;
  xStr: String;
begin
  EnterMethod('TfrmMain.Create');
  try
    inherited Create(aOwner);

//    TabSheet4.TabVisible := False;
    mMeasures   := TMeasuresList.Create;
    FillFromConstArray(mMeasures);

    with TStringList.Create do
    try
      xStr := '.\Reports.txt';
      if FileExists(xStr) then begin
        LoadFromFile(xStr);
        for i:=0 to Count-1 do begin
          xStr := Format('.\%s.ini', [Strings[i]]);
          if FileExists(xStr) then begin
            xMemIni := TMemIniFile.Create(xStr);
            lbReports.Items.AddObject(Strings[i], xMemIni);
          end;
        end;
      end;
    except
      Free;
    end;
  except
    on e:Exception do
      CodeSite.SendError('Create: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
destructor TfrmMain.Destroy;
var
  i, j: Integer;
  xMemIni: TMemIniFile;
  xReportIni: TIniFile;
  xReportList: TStringList;
  xStrList: TStringList;
begin
  if mAC.CubeSource <> Nil then
    mCS.Close;
  mAC.CubeSource := Nil;

  with lbReports.Items do
  try
    xStrList := TStringList.Create;
    SaveToFile('.\Reports.txt');
    for i:=0 to Count-1 do begin
      xMemIni := TMemIniFile(Objects[i]);
      xMemIni.UpdateFile;
      xMemIni.Free;
    end;
  finally
    xStrList.Free;
  end;

  mMeasures.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
//ocedure TfrmMain.MoveMeasure(aSrc, aDest: TListView; aIndex: Integer; aVisible: Boolean);
procedure TfrmMain.MoveMeasure(aShowMeasures: Boolean);
var
  i: Integer;
  xV1: OLEVariant;
  xItem: TListItem;
  xSrc, xDest: TListView;
  xPRec: PMeasureDefRec;
  xIndex: Integer;
  xIntf: IACMeasure;
begin
  // xSrc und xDest festlegen
  if aShowMeasures then begin
    xSrc  := lvMeasures;
    xDest := lvSelMeasures;
  end else begin
    xSrc  := lvSelMeasures;
    xDest := lvMeasures;
  end;

  // arbeite mit Source als default
  with xSrc do begin
    // Parameterrecord von diesem Item holen
    xPRec := Selected.Data;
    // Information in Cube setzen
    xV1 := xPRec^.Name;
    if InCubeMode then begin
      xIndex := xPRec^.CubeIndex;
      mAC.Measures.Item(xV1).InDataArea := aShowMeasures;
    end else
      xIndex := xPRec^.ChartIndex;

    // Wenn Show und xIndex <> -1 dann ein Item einfuegen (Insert), ansonsten hinten anstellen (Add)
    if aShowMeasures and (xIndex <> -1) and (xIndex < xDest.Items.Count) then begin
      xItem := xDest.Items.Insert(xIndex);
      // Position innerhalb vom Cube auch noch setzen
      if InCubeMode then
        with mAC.Measures.Item(xV1) do
          Position := mBaseMeasuresPos + xIndex;
    end else begin
      // noch keine Position vorhanden oder abgewaehlt
      xItem := xDest.Items.Add;
      xIndex := xItem.Index;
    end;
    xItem.Assign(Selected);

    // wenn Item abgewaehlt wird, Index reseten
    if not aShowMeasures then
      xIndex := -1;

    // Index wieder zurueckspeichern in Record
    if InCubeMode then begin
      xPRec^.CubeIndex  := xIndex;
      if not bCubeChartMode.Down then
        xPRec^.ChartIndex := xIndex;
    end else
      xPRec^.ChartIndex := xIndex;

    // in Source Item noch loeschen
    Selected.Delete;
  end;
end;
//------------------------------------------------------------------------------
function TfrmMain.InCubeMode: Boolean;
begin
  Result := (pcMain.ActivePageIndex = 0) or (not bCubeChartMode.Down);
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ResetMeasures;
var
  i: Integer;
  xV1: OLEVariant;
begin
  // erst die Measures entfernen
  with lvSelMeasures do begin
    while Items.Count > 0 do begin
      Items.Item[0].Selected := True;
      MoveMeasure(False);
    end;
  end;

  with clbDimensions do begin
    for i:=0 to Items.Count-1 do begin
      Checked[i] := False;
    end;
  end;

  // dann die Dimensionen
  with mAC.Dimensions do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      with Item(xV1) do begin
        if CompareText(Name, 'Measures') <> 0 then
          Orientation := OTNone;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.InitReport;
var
  i: Integer;
  xV1: OLEVariant;
begin
  // erst die Measures entfernen
  with lvSelMeasures do begin
    while Items.Count > 0 do begin
      Items.Item[0].Selected := True;
      MoveMeasure(False);
    end;
  end;

  with clbDimensions do begin
    for i:=0 to Items.Count-1 do begin
      Checked[i] := False;
    end;
  end;

  // dann die Dimensionen
  with mAC.Dimensions do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      with Item(xV1) do begin
        if CompareText(Name, 'Measures') <> 0 then
//        if Orientation <> OTNone then
          Orientation := OTNone;
      end;
    end;
  end;
  // und noch alle Measures ausblenden
{
  with mAC.Measures do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      with Item(xV1) do begin
        InDataArea := False;
      end;
    end;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acShowReportExecute(Sender: TObject);
begin
  with lbReports do
  try
    if ItemIndex <> -1 then begin
      with TMemIniFile(Items.Objects[ItemIndex]) do
        WriteBool('General', 'CubeChartMode', bCubeChartMode.Down);
    end;
  except
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ShowReport;
var
  xIni: TStringList;
  xStr: String;
  //......................................................................
  procedure LoadDimensions;
  var
    i: Integer;
    xV1: OLEVariant;
    xName: String;
    xIndex: Integer;
    xNameList: TStringList;
    xValues: TmmStringList;
    xInt: Integer;
  begin
    xNameList := TStringList.Create;
    xValues   := TmmStringList.Create;
    try
      mMemIni.ReadSection(cDimPrefix, xNameList);
      with xNameList do begin
        for i:=0 to Count-1 do begin
          xName := Strings[i];
          xValues.CommaText := mMemIni.ReadString(cDimPrefix, xName, '');
          xV1 := xName;
          with mAC.Dimensions.Item(xV1) do begin
            if CompareText(xName, 'Measures') <> 0 then begin
              Orientation := xValues.Value(0);
              xInt := xValues.Value(1);
              if xInt >= Count then
                xInt := Count;
//              Position := xInt;
              xIndex := clbDimensions.Items.IndexOf(xName);
              if xIndex <> -1 then
                clbDimensions.Checked[xIndex] := True;
            end;
          end; // with mAC
        end; // for
      end; // with xValueList
    finally
      xValues.Free;
      xNameList.Free;
    end;
  end;
  //......................................................................
  procedure LoadMeasures;
  var
    i: Integer;
    xStr: String;
    xName: String;
    xCount: Integer;
    xStrList: TIniStringList;
    xIndex: Integer;
    xItem: TListItem;
    xNameList: TStringList;
    xValues: TmmStringList;
    xV1: OLEVariant;
    xPRec: PMeasureDefRec;
  begin
    xNameList := TStringList.Create;
    xValues   := TmmStringList.Create;
    try
      mMemIni.ReadSection(cMeasuresPrefix, xNameList);
      with xNameList do begin
        for i:=0 to Count-1 do begin
          xName := Strings[i];
          xPRec := mMeasures.UnpackPropertyString(mMemIni.ReadString(cMeasuresPrefix, xName, ''));
          if Assigned(xPRec) then begin
            // Eigenschaften in den Cube uebernehmen
            xV1 := xPRec^.Name;
            with mAC.Measures.Item(xV1) do begin
              Caption    := xPRec^.DisplayName;
              Formula    := xPRec^.Formula;
            end;
          end;
        end; // with i
      end; // with xValueList
    finally
      xValues.Free;
      xNameList.Free;
    end;
  end;
  //......................................................................
begin
  InitReport;
  try
    with lbReports do begin
      if ItemIndex <> -1 then begin
        mMemIni := TMemIniFile(Items.Objects[ItemIndex]);
        bCubeChartMode.Down := mMemIni.ReadBool('General', 'CubeChartMode', False);
        with TStringList.Create do
        try
          CommaText := mMemIni.ReadString('General', 'Comment', '');
          laComment.Caption := Text;
        finally
          Free;
        end;
        LoadMeasures;
        LoadDimensions;
        ActivateMeasureList;
        FillChartDimensionList;
      end;
    end;
  finally
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lbReportsClick(Sender: TObject);
//var
//  xMemIni: TMemIniFile;
begin
//  xMemIni := TMemIniFile.Create;
  with lbReports do begin
    if ItemIndex <> -1 then begin
      TMemIniFile(Items.Objects[ItemIndex]).GetStrings(meReport.Lines);
      ShowReport;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acDeleteReportExecute(Sender: TObject);
var
  xIndex: Integer;
  xStr: String;
begin
  with lbReports do begin
    Items.Objects[ItemIndex].Free;
    xStr := Format('.\%s.ini', [Items.Strings[ItemIndex]]);
    if FileExists(xStr) then
      DeleteFile(xStr);
    Items.Delete(ItemIndex);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.mmButton4Click(Sender: TObject);
var
  xName: String;
  xIni: TIniStringList;
  xCount: Integer;
  //......................................................................
  procedure SaveMeasures;
  var
    i: Integer;
    xStr: String;
  begin
    with lvSelMeasures.Items do begin
      xIni.Values[cMeasuresPrefix + 'Count'] := Count;
      for i:=0 to Count-1 do begin
        xStr := cMeasuresPrefix + IntToStr(i);
        xIni.Values[xStr] := Item[i].Caption;
      end;
    end;
  end;
  //......................................................................
  procedure SaveDimensions(aPrefix: String; aAxis: IACAxis; var aCount: Integer);
  var
    i: Integer;
    xV1, xV2: OLEVariant;
    xStr: String;
  begin
    with aAxis do begin
      xIni.Values[Format('%s%dDepth', [aPrefix, aCount])] := VisibleDepth;
      with Dimensions do begin
        for i:=0 to Count-1 do begin
          xV1 := i;
          with Item(xV1) do begin

            if Orientation <> OTNone then begin
              xStr := cDimPrefix + IntToStr(aCount);
              if VisibleDepth > 0 then begin
                xV2 := VisibleDepth - 1;
                xIni.Values[xStr + 'Unique'] := Members.Item(xV2).UniqueName;
              end;
              xIni.Values[xStr + 'Name'] := Name;
              xIni.Values[xStr + 'Orientation'] := Orientation;
              if CompareText(Name, 'Measures') <> 0 then
                xIni.Values[xStr + 'Position'] := Position;
              inc(aCount);
            end; // if
          end; // with
        end; // for
      end;
    end; // with
  end;
  //......................................................................
begin
//  xName := 'Report1';
  xIni := TIniStringList.Create;

  xCount := 0;
  SaveDimensions(cDimPrefix, mAC.ColumnAxis, xCount);
  SaveDimensions(cDimPrefix, mAC.RowAxis, xCount);
  SaveDimensions(cDimPrefix, mAC.SlicerAxis, xCount);
  xIni.Values[cDimPrefix + 'Count'] := xCount;
  SaveMeasures;
  meReport.Lines := xIni;
  xIni.Free;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.FillChartDimensionList;
var
  i, j: Integer;
  xV1: OLEVariant;
  xDimNode: TTreeNode;
  xMesNode: TTreeNode;
  xMember: Boolean;
  xStr: String;
  xDimInfoP: PDimensionInfo;
  //...............................................................
  procedure AddMembers(aDim: IACDimension; aDimIndex: Integer);
  var
    i: Integer;
    xV1: OLEVariant;
    xNode: TTreeNode;
    xDimInfo: TDimensionInfo;
    //...........................................................
    function GetDimInfo(aDim, aLevel: Integer): TDimensionInfo;
    begin
      Result := TDimensionInfo.Create;
      Result.DimIndex := aDim;
      Result.Level    := aLevel;
    end;
    //...........................................................
  begin
    xNode := Nil;
    with aDim.Levels do begin
      for i:=0 to Count-1 do begin
        xV1 := i;
        with Item(xV1) do begin
          xNode := tvDimensions.Items.AddChildObject(xNode, Name, GetDimInfo(aDimIndex, i));
        end;
      end;
    end;
  end;
  //...............................................................
  function GetDimInfoP(aName: String): PDimensionInfo;
  var
    i: Integer;
  begin
    Result := Nil;
    with tvDimensions.Items do begin
      for i:=0 to Count do begin
        if CompareText(Item[i].Text, aName) = 0 then begin
          Result := Item[i].Data;
          Exit;
        end;
      end;
    end;
  end;
  //...............................................................
begin
  with tvDimensions do begin
    Items.Clear;
  end;

  with mAC.Dimensions do begin
    for i:=0 to Count-1 do begin
      xV1 := i;
      with Item(xV1) do begin
        if (Orientation = OTRow) or (Orientation = OTColumn) then begin
          if(CompareText(Name, 'Measures') <> 0) then
            AddMembers(Item(xV1), i);
        end;
      end;
    end;
  end;

  if Assigned(mMemIni) then begin
    xStr := mMemIni.ReadString('ChartOptions', 'XAxis', '');
    if xStr <> '' then begin
      xDimInfoP := GetDimInfoP(xStr);
      if Assigned(xDimInfoP) then begin
        pnChartXAxis.Caption := xStr;
        pnChartXAxis.Tag     := Integer(xDimInfoP);
      end;
    end else begin
      pnChartXAxis.Caption := rsNothingSelected;
      pnChartXAxis.Tag     := 0;
    end;

    xStr := mMemIni.ReadString('ChartOptions', 'ZAxis', '');
    if xStr <> '' then begin
      xDimInfoP := GetDimInfoP(xStr);
      if Assigned(xDimInfoP) then begin
        pnChartZAxis.Caption := xStr;
        pnChartZAxis.Tag     := Integer(xDimInfoP);
      end;
    end else begin
      pnChartZAxis.Caption := rsNothingSelected;
      pnChartZAxis.Tag     := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.tvDimensionsMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  xNode: TTreeNode;
  xV1, xV2: OLEVariant;
  xDimInfo: TDimensionInfo;
begin
  with tvDimensions do begin
    xNode := GetNodeAt(X, Y);
    CodeSite.SendString('tvChartMouseDown', xNode.Text);
    xDimInfo := xNode.Data;
    if Assigned(xDimInfo) then begin
      xV1 := xDimInfo.DimIndex;
      xV2 := xDimInfo.Level;
      with mAC.Dimensions.Item(xV1).Levels.Item(xV2) do begin
        CodeSite.SendString('DimInfo', UniqueName);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.tsChartShow(Sender: TObject);
var
  i: Integer;
  xV1, xV2: OLEVariant;
begin
  FillChartDimensionList;
//  FillMeasures;
  pnSliceAxis.Caption := '';
  i := 0;
  with mAC.SlicerAxis do begin
    with Dimensions do begin
      while i < Count do begin
        xV1 := i;
        with Item(xV1) do begin
          if Assigned(SlicerMember) then
            if pnSliceAxis.Caption = '' then
              pnSliceAxis.Caption := UniqueName
            else
              pnSliceAxis.Caption := pnSliceAxis.Caption + ',' + UniqueName;
        end; // with
        inc(i);
      end; // while
      pnSliceAxis.Hint := pnSliceAxis.Caption;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.pnChartXYAxisDragOver(Sender, Source: TObject; X, Y: Integer; State: TDragState; var Accept: Boolean);
var
  xDimInfoX, xDimInfoZ: TDimensionInfo;
begin
  if Source = tvDimensions then begin
    if Sender = pnChartXAxis then
      Accept := True
    else if Sender = pnChartZAxis then begin
      xDimInfoX := Pointer(pnChartXAxis.Tag);
      xDimInfoZ := tvDimensions.Selected.Data;
      if Assigned(xDimInfoX) then begin
        Accept := (xDimInfoX.DimIndex <> xDimInfoZ.DimIndex);
      end else
        Accept := True;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.pnChartXYAxisDragDrop(Sender, Source: TObject; X, Y: Integer);
var
  xIndex: Integer;
  xV1, xV2: OLEVariant;
  xDimInfoX, xDimInfoZ: TDimensionInfo;
  xDropPanel: TPanel;
  xCheckPanel: TPanel;
begin
  xDropPanel := TPanel(Sender);
  if xDropPanel = pnChartXAxis then xCheckPanel := pnChartZAxis
                               else xCheckPanel := pnChartXAxis;

  with TTreeView(Source) do begin
    xDropPanel.Caption := Selected.Text;
    xDropPanel.Tag     := Integer(Selected.Data);
    if xDropPanel = pnChartXAxis then begin
      xDimInfoZ := Pointer(pnChartZAxis.Tag);
      if Assigned(xDimInfoZ) then begin
        if TDimensionInfo(Selected.Data).DimIndex = xDimInfoZ.DimIndex then begin
          pnChartZAxis.Caption := rsNothingSelected;
          pnChartZAxis.Tag     := 0;
        end;
      end;
    end;

    // debug infos
    with TDimensionInfo(Selected.Data) do begin
      xV1 := DimIndex;
      xV2 := Level;
      with mAC.Dimensions.Item(xV1).Levels.Item(xV2) do
        xDropPanel.Hint := xDropPanel.Caption + '  ' + UniqueName;
    end;
  end;
  lvSelMeasures.Invalidate;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.UpdateChart;
var
  i, j: Integer;
  xV1, xV2, xV3, xV4: OLEVariant;
  xMDX_X: OLEVariant;
  xMDX_Y: OLEVariant;
  xMDX_V: OLEVariant;
  xXList, xYList: TStringList;
  xMeasuresMode: Boolean;
  xMDXQry: WideString;
  xDimInfoX: TDimensionInfo;
  xDimInfoZ: TDimensionInfo;
  xDimInfo: TDimensionInfo;
  xMembersX: TStringList;
  xMembersZ: TStringList;
  xLevel: IACLevel;

  function GetNewSerie(aTitle: String; aChartType: TChartType; aColor: TColor; aRightAxis: Boolean): TChartSeries;
  begin
    case aChartType of
      ctLine: begin
          Result := TLineSeries.Create(Self);
          with Result as TLineSeries do begin
            SeriesColor   := aColor;
          end;
        end;
      ctBar: begin
          Result := TBarSeries.Create(Self);
          with Result as TBarSeries do begin
            SeriesColor   := aColor;
{
            case fParameter.ChartOptions^.BarStyle of
              bsSide:       MultiBar := mbSide;
              bsStacked:    MultiBar := mbStacked;
              bsStacked100: MultiBar := mbStacked100;
            else
              MultiBar := Series.mbNone;
            end;
{}
          end;
        end;
      ctArea: begin
          Result := T2DAreaSeries.Create(Self);
          with Result as T2DAreaSeries do begin
            MultiArea   := maNone;
            SeriesColor := aColor;
          end;
        end;
    else
    end; // case xChartType

    with Result do begin
      Marks.Visible    := False;
      ParentChart      := mChart;
      Title            := aTitle;
      XValues.DateTime := False;
      if aRightAxis then begin
        Title    := Title + ' (R)';
        VertAxis := TeEngine.aRightAxis
      end else
        VertAxis := TeEngine.aLeftAxis;
    end; // with
  end;
  //......................................................................
  function GetUniqueNameFromPanel(aPanel: TPanel): WideString;
  var
    xV1, xV2: OLEVariant;
    xDimInfo: TDimensionInfo;
  begin
    Result := '';
    xDimInfo := Pointer(aPanel.Tag);
    if Assigned(xDimInfo) then begin
      xV1 := xDimInfo.DimIndex;
      xV2 := xDimInfo.Level;
      Result := mAC.Dimensions.Item(xV1).Levels.Item(xV2).UniqueName;
    end;
  end;
  //......................................................................
  function GetLevelFromDimensionInfoClass(aDimInfo: TDimensionInfo): IACLevel;
  var
    xV1, xV2: OLEVariant;
  begin
    xV1 := aDimInfo.DimIndex;
    xV2 := aDimInfo.Level;
    Result := mAC.Dimensions.Item(xV1).Levels.Item(xV2);
  end;
  //......................................................................
  procedure FillChart(aSerie: TChartSeries; aMDXStr: String);
  var
    i: Integer;
    xV1: OLEVariant;
    xLevel: IACLevel;
    xDimInfo: TDimensionInfo;
    xMDX_X: String;
    xWStr: WideString;
    xSingle: Single;
  begin
    xDimInfo := Pointer(pnChartXAxis.Tag);
    if Assigned(xDimInfo) then begin
      xLevel := GetLevelFromDimensionInfoClass(xDimInfo);
      xMDX_X := xLevel.UniqueName + '.[%s]';
      with xLevel.Members do begin
        for i:=0 to Count-1 do begin
          xV1 := i;
          xWStr := Format(aMDXStr + ',' + xMDX_X, [Item(xV1).Name]);
          meGetCell.Lines.Add(xWStr);
          try
            xSingle := mAC.GetCell(xWStr).Value;
          except
            xSingle := 0;
          end;
          aSerie.AddY(xSingle, Item(xV1).Name, clTeeColor);
        end; // for
      end; // with
    end; // if
  end;
  //......................................................................
  procedure FillMeasuresChart(aLevel: IACLevel);
  var
    i, j: Integer;
    xBar: TBarSeries;
    xSerie: TChartSeries;
    xV1, xV2: OLEVariant;
    xWStr: WideString;
    xStr: String;
    xSingle: Single;
    xDimInfo: TDimensionInfo;
    xMeasureName: String;
    xParent: IACMember;
    xPRec: PMeasureDefRec;
    //...........................................................
    function GetTopParent(aMember: IACMember):IACMember;
    begin
      Result := aMember.Parent;
      if Assigned(Result) then begin
        while Assigned(Result.Parent) do
          Result := Result.Parent;
      end;
    end;
    //...........................................................
  begin
    Screen.Cursor := crHourGlass;
    with lvSelMeasures do
    try
      for i:=0 to Items.Count-1 do begin
        with Items[i] do begin
          xMeasureName := Caption;
          xPRec := PMeasureDefRec(Data);
          with xPRec^ do
            xSerie := GetNewSerie(xMeasureName, ChartType, Color, RightAxis);
        end;

        with aLevel.Members do begin
          for j:=0 to Count-1 do begin
            xV1 := j;
            with Item(xV1) do begin
              if Visible then begin
                xStr := Name;
                xParent := GetTopParent(Item(xV1));
                if Assigned(xParent) then
                  xStr := xStr  + #13 + xParent.Name;
                // 1.Parameter = X-Parameter = Members von Dimension
                // 2.Parameter = Measures = Caption von ListItem
                xWStr := Format(xMDXQry, [Name, xMeasureName]);
                try
                  xSingle := mAC.GetCell(xWStr).Value;
                except
                  meGetCell.Lines.Add(xWStr);
                  xSingle := 0;
                end;
                xSerie.AddY(xSingle, xStr, clTeeColor);
              end; // if Visible
            end; // with Item

          end; // for j
        end; // wtih
        mChart.AddSeries(xSerie);
      end; // for i
    finally
      Screen.Cursor := crDefault;
    end; // with
  end;
  //......................................................................
  procedure FillDimensionsChart;
  var
    i, j: Integer;
    xBar: TBarSeries;
    xSerie: TChartSeries;
    xV1, xV2: OLEVariant;
    xWStr: WideString;
    xStr: String;
    xSingle: Single;
    xPRec: PMeasureDefRec;
//    xChartInfo: TChartItemDef;
  begin
    Screen.Cursor := crHourGlass;
    xPRec := PMeasureDefRec(lvSelMeasures.Items[0].Data);
    with xMembersZ do
    try
      for i:=0 to Count-1 do begin
        with xPRec^ do
          xSerie := GetNewSerie(Strings[i], ChartType, clTeeColor, RightAxis);

        with xSerie, xMembersX do begin
          for j:=0 to Count-1 do begin
            // 1. Parameter = X-Parameter = Members von X-Panel
            // 2. Parameter = Z-Parameter = Members von Z-Panel
            xWStr := Format(xMDXQry, [Strings[j], xMembersZ.Strings[i]]);

            try
              xSingle := mAC.GetCell(xWStr).Value;
            except
              meGetCell.Lines.Add(xWStr);
              xSingle := 0;
            end;
            AddY(xSingle, Strings[j], clTeeColor);
          end; // for
        end; // wtih
        mChart.AddSeries(xSerie);
      end; // for
    finally
      Screen.Cursor := crDefault;
    end; // with
  end;
  //......................................................................
  procedure FillMembersInList(aLevel: IACLevel; aList: TStringList);
  var
    i: Integer;
    xV1: OLEVariant;
  begin
    with aLevel.Members do begin
      for i:=0 to Count-1 do begin
        xV1 := i;
        with Item(xV1) do begin
          if Visible then
            aList.Add(Name);
        end;
      end;
    end;
  end;
  //......................................................................
begin
  meGetCell.Clear;
  mChart.RemoveAllSeries;
  mChart.Title.Text.Text := '';

  if pnChartXAxis.Tag = 0 then
    Exit;

  xMDXQry := '';
  xMeasuresMode := (pnChartZAxis.Tag = 0);
  if pnSliceAxis.Caption <> '' then
    xMDXQry := pnSliceAxis.Caption + ',';

  xMDXQry := xMDXQry + GetUniqueNameFromPanel(pnChartXAxis) + '.[%s]';

  xDimInfo  := TDimensionInfo(pnChartXAxis.Tag);
  xLevel    := GetLevelFromDimensionInfoClass(xDimInfo);
  xMembersX := TStringList.Create;
  FillMembersInList(xLevel, xMembersX);

  if xMeasuresMode then begin
    // die Measures werden in Grafik dargestellt
    xMDXQry := xMDXQry + ',[%s]';
    CodeSite.SendString('xMDXQuery', xMDXQry);
    FillMeasuresChart(xLevel);
  end else begin
    mChart.Title.Text.Text := lvSelMeasures.Items[0].Caption;
    xDimInfo  := TDimensionInfo(pnChartZAxis.Tag);
    xLevel    := GetLevelFromDimensionInfoClass(xDimInfo);
    xMembersZ := TStringList.Create;
    FillMembersInList(xLevel, xMembersZ);
    // Members einer Dimension werden in der Grafik Dargestellt -> Datenwerte = 1. Measure in ListView
    xMDXQry := Format('%s, %s.[%%s], [%s]', [xMDXQry, GetUniqueNameFromPanel(pnChartZAxis), lvSelMeasures.Items[0].Caption]);
    CodeSite.SendString('xMDXQuery', xMDXQry);
    CodeSite.SendStringList('MemberZ', xMembersZ);
    FillDimensionsChart;
  end;


  with mChart.Legend do begin
    Visible := True;
    LegendStyle := lsSeries;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.acDeleteChartZAxisExecute(Sender: TObject);
begin
  with pnChartZAxis do begin
    Caption := rsNothingSelected;
    Tag := 0;
  end;
  lvSelMeasures.Invalidate;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.ShowChartPropFromDataItem(aItemIndex: Integer);
var
//  xStat: TStatistic;
  xBool: Boolean;
begin
  xBool := Assigned(lvSelMeasures.Selected) and not(pcMain.ActivePageIndex = 0);

  bChartLine.Enabled   := xBool;
  bChartBar.Enabled    := xBool;
  bChartArea.Enabled   := xBool;
  bColorSelect.Enabled := xBool;

  bLeftAxis.Enabled    := xBool;
  bRightAxis.Enabled   := xBool;

//  if (aItemIndex >= 0) and not(pcMain.ActivePageIndex = 0) then begin
  if xBool then begin
    with PMeasureDefRec(lvSelMeasures.Selected.Data)^ do begin
      //
      // set standard values
      //
      case ChartType of
        ctLine: bChartLine.Down := True;
        ctBar:  bChartBar.Down  := True;
        ctArea: bChartArea.Down := True;
      else
      end;

      if RightAxis then
        bRightAxis.Down := True
      else
        bLeftAxis.Down := True;
      bColorSelect.Color := Color;

    end; // with
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvSelMeasuresDragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  Accept := False;
  if Sender = Source then begin
    Accept := (Sender = lvSelMeasures);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvSelMeasuresDragDrop(Sender, Source: TObject; X,
  Y: Integer);
var
  xDropPos: Integer;
  xItem: TListItem;
begin
  xDropPos := 0;
  // first get the drop position in the listview
  with Sender as TListView do
  try
    xDropPos := Items.Count;
    // adapt Y value for a better mouse feeling for moving items
    Y := Y  - Font.Height;
    xItem := GetItemAt(X, Y);
    if Assigned(xItem) then
      xDropPos := xItem.Index;
  except
    on e:Exception do begin
      CodeSite.SendError('lvChartSeriesDragDrop: ' + e.Message);
    end;
  end;

  if Sender = Source then begin
    // Move items within ListView -> Move = copy & delete old
    with Sender as TListView do begin
      Items.BeginUpdate;

      xItem := Selected;
      with Items.Insert(xDropPos) do begin
        Assign(xItem);
        Items.Delete(xItem.Index);
        Selected := True;
        PMeasureDefRec(Data)^.CubeIndex := Index;
      end;
      Items.EndUpdate;
    end;
  end;
  if not InCubeMode then
    UpdateChart;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvSelMeasuresSelectItem(Sender: TObject;
  Item: TListItem; Selected: Boolean);
begin
{}
  if not(csDestroying in ComponentState) then begin
    mEnabledChanges := False;

    if Selected and (lvSelMeasures.SelCount = 1) then begin
      ShowChartPropFromDataItem(lvSelMeasures.Selected.Index);
    end else
      ShowChartPropFromDataItem(-1);

    mEnabledChanges := True;
  end;
{}
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bChartTypeClick(Sender: TObject);
var
  xTag: Integer;
begin
  with Sender as TControl do begin
    xTag := Tag
  end;
  OnChartItemsChange(xTag);
  UpdateChart;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.OnChartItemsChange(aTagId: Integer);
var
  i: Integer;
begin
  if mEnabledChanges then begin
    with lvSelMeasures.Items do begin
      for i:=0 to Count-1 do begin
        if Item[i].Selected then begin
          with PMeasureDefRec(Item[i].Data)^ do begin
            case aTagId of
              // ChartType
              101..103: begin
                  ChartType := TChartType(aTagId - 100);
                  Item[i].ImageIndex := Ord(ChartType)-1;
                end;
              // graphic color
              8: Color := bColorSelect.Color;
              // Left or Right axis
              12, 13: RightAxis := (aTagId = 13);
            else
            end;
          end; // with
        end; // if
      end; // for
    end; // with
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvSelMeasuresDrawItem(Sender: TCustomListView;
  Item: TListItem; Rect: TRect; State: TOwnerDrawState);
var
  xEnabled: Boolean;
begin
  with Sender as TListView do begin
    with Canvas do begin
      xEnabled := (pnChartZAxis.Tag = 0) or (Item.Index = 0) or (pcMain.ActivePageIndex = 0);
      if xEnabled then
        Font.Color := clBlack
      else
        Font.Color := clBtnFace;

      if odSelected in State then begin
        Brush.Color := clHighlight;
        Font.Color  := clHighlightText;
      end;

      TextRect(Rect, Rect.Left, Rect.Top + 2, Item.Caption);
      if pcMain.ActivePageIndex = 1 then
        SmallImages.Draw(Canvas, Rect.Right + 2, Rect.Top, Item.ImageIndex, xEnabled);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.bUpdateChartClick(Sender: TObject);
begin
  UpdateChart;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.pcMainChange(Sender: TObject);
begin
  ActivateMeasureList;
  ShowChartPropFromDataItem(-1);
//  lvSelMeasures.Invalidate;
end;
//------------------------------------------------------------------------------
procedure TfrmMain.lvSelMeasuresDblClick(Sender: TObject);
begin
  MoveMeasure(Sender = lvMeasures);
  ReorderingMeasures;
  lvSelMeasures.Invalidate;
  mCubeSelectionChanged := True;
end;
//------------------------------------------------------------------------------
end.

