unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmListBox, mmMemo, ExtCtrls, mmPanel, ComCtrls,
  MSXML2_TLB, mmTreeView, mmLabel, mmEdit, mmImage, mmShape, Buttons,
  mmBitBtn, mmBevel, mmCheckBox, mmGroupBox,
  MSXMLKonverter;

type
  TForm1 = class(TForm)
    bBinXML: TmmButton;
    bGetBinDescript: TmmButton;
    bGetBinInputPath: TmmButton;
    bGetBinOutputPath: TmmButton;
    bGetXMLInput: TmmButton;
    bGetXMLInputPath: TmmButton;
    bXMLBin: TmmButton;
    edBinInput: TmmEdit;
    edBinOutput: TmmEdit;
    edHexEditor: TmmEdit;
    edTexteditor: TmmEdit;
    edXMLDescriptor: TmmEdit;
    edXMLInput: TmmEdit;
    edXMLOutput: TmmEdit;
    laTime: TmmLabel;
    mmBevel1: TmmBevel;
    mmBitBtn1: TmmBitBtn;
    mmBitBtn2: TmmBitBtn;
    mmBitBtn3: TmmBitBtn;
    mmBitBtn4: TmmBitBtn;
    mmBitBtn5: TmmBitBtn;
    mmBitBtn6: TmmBitBtn;
    mmBitBtn7: TmmBitBtn;
    mmBitBtn8: TmmBitBtn;
    mmBitBtn9: TmmBitBtn;
    mmButton1: TmmBitBtn;
    mmImage1: TmmImage;
    mmImage2: TmmImage;
    mmImage3: TmmImage;
    mmImage4: TmmImage;
    mmImage5: TmmImage;
    mmImage6: TmmImage;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    mmPanel1: TmmPanel;
    mmShape1: TmmShape;
    mmShape10: TmmShape;
    mmShape2: TmmShape;
    mmShape3: TmmShape;
    mmShape4: TmmShape;
    mmShape5: TmmShape;
    mmShape6: TmmShape;
    mmShape7: TmmShape;
    mmShape8: TmmShape;
    mmShape9: TmmShape;
    OpenDialog: TOpenDialog;
    mmGroupBox1: TmmGroupBox;
    cbValidate: TmmCheckBox;
    cbEndian: TmmCheckBox;
    cbEvents: TmmCheckBox;
    mmGroupBox2: TmmGroupBox;
    cbValidate2: TmmCheckBox;
    cbEndian2: TmmCheckBox;
    cbEvent2: TmmCheckBox;
    cbOk: TmmCheckBox;
    cbOk2: TmmCheckBox;
    edXMLSchema: TmmEdit;
    bGetXMLSchema: TmmButton;
    mmBitBtn10: TmmBitBtn;
    mmBitBtn11: TmmBitBtn;
    mmLabel5: TmmLabel;
    //1 Transformiert ein bin�res File in ein XML File
    procedure bBinXMLClick(Sender: TObject);
    //1 Transformiert ein XML File in ein bin�res File
    procedure bXMLBinClick(Sender: TObject);
    //1 �ffnet den Ordner mit dem gew�nschten File im Explorer.
    procedure OpenExplorer(Sender: TObject);
    //1 W�hlt ein File aus
    procedure OpenFileDialog(Sender: TObject);
    //1 �ffnet das gew�nschte File im Notepad
    procedure OpenNotepad(Sender: TObject);
  private
    mKonverter: TMSXMLKonverter;
    //1 L�dt das bin�re File als Datenblock in den Speicher
    function LoadBinFile(aFileName: string; out aPointer: Pointer): Integer;
  protected
    //1 L�dt ein XML File in den Speicher
    function LoadXMLFile(aFileName: string): String;
    //1 Speichert den "Datenhaufen" in einem bin�ren File
    procedure saveBinFile(aFileName: string; aBinData: Pointer; aBinLen: integer);
    //1 Speichert den XML Stream in einem Textfile
    procedure saveXMLFile(aFileName: string; aXMLOutput: string);
  end;

var
  Form1: TForm1;

implementation

uses
  LoepfeGlobal, mmcs, filectrl, shellapi;

{$R *.DFM}


//:-------------------------------------------------------------------
(*: Member:           bBinXMLClick
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Transformiert ein bin�res File in ein XML File
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.bBinXMLClick(Sender: TObject);
var
  xBinFile: Pointer;
  xBinLen: Integer;
  xXMLStream: String;
  xXMLSchema: String;
  xDescriptor: String;
  xXMLDocument: DOMDocument40;
  xStart: Cardinal;
begin
  xBinLen := LoadBinFile(edBinInput.Text, xBinFile);
  xXMLSchema := LoadXMLFile(edXMLSchema.Text);
  if xBinLen > 0 then begin
    xDescriptor := LoadXMLFile(edXMLDescriptor.Text);
    if xDescriptor > '' then begin
      with TMSXMLKonverter.Create do begin
//        Init(xXMLSchema);
        Init(edXMLSchema.Text);
        xStart := GetTickCount;
        if BinToXML(xBinFile, xDescriptor, cbEndian.Checked, xXMLStream, cbValidate.Checked) <> 0 then begin
          showMessage('Error')
        end else begin
          laTime.Caption := IntToStr(GetTickCount - xStart) + 'ms';
          saveXMLFile(edXMLOutput.Text, xXMLStream);
        end;// if KonvertBinToXML(xBinFile, xDescriptor, xXMLStream) <> 0 then
        FreeMem(xBinFile);
      end; //with
    end;// if xDescriptor > '' then begin
  end;// if xBinLen > 0 then begin
end;// TForm1.bBinXMLClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           bXMLBinClick
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: Transformiert ein XML File in ein bin�res File
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.bXMLBinClick(Sender: TObject);
var
  xBinFile: Pointer;
  xBinLen: Integer;
  xXMLStream: String;
  xXMLSchema: String;
  xDescriptor: String;
  xXMLDocument: DOMDocument40;
  xStart: Cardinal;
begin
  xXMLStream := LoadXMLFile(edXMLInput.Text);
  xXMLSchema := LoadXMLFile(edXMLSchema.Text);

  xDescriptor := LoadXMLFile(edXMLDescriptor.Text);
  if xDescriptor > '' then begin
    try
      xStart := GetTickCount;
//      if KonvertXMLToBin(xBinFile, xBinLen, , xXMLStream) <> 0 then begin
      with TMSXMLKonverter.Create do begin
        Init(edXMLSchema.Text);
        if XMLToBin(xBinFile, xBinLen, xDescriptor, cbEndian2.Checked, xXMLStream, cbValidate2.Checked) <> 0 then begin
          showMessage('Fehler bei der Konvertierung.')
        end else begin
          laTime.Caption := IntToStr(GetTickCount - xStart) + 'ms';
          saveBinFile(edBinOutput.Text, xBinFile, xBinLen);
        end;// if KonvertXMLToBin(xBinFile, xBinLen, xDescriptor, xXMLStream) <> 0 then begin
      end; //with
    finally
      FreeMem(xBinFile);
    end;// try finally
  end;// if xDescriptor > '' then begin
end;// TForm1.bXMLBinClick cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadBinFile
 *  Klasse:           TForm1
 *  Kategorie:        No category
 *  Argumente:        (aFileName, aPointer)
 *
 *  Kurzbeschreibung: L�dt das bin�re File als Datenblock in den Speicher
 *  Beschreibung:
                      Der Aufrufer ist verantwortlich, dass der reservierte Speicher wieder freigegeben wird.
 --------------------------------------------------------------------*)
function TForm1.LoadBinFile(aFileName: string; out aPointer: Pointer): Integer;
var
  xStream: TFileStream;
  xPointer: Pointer;
begin
  result := -1;
  aPointer := nil;
  if FileExists(aFileName) then begin
    xStream := nil;
    try
      xStream := TFileStream.Create(aFileName, fmOpenRead	or fmShareDenyNone);
      xStream.Position := 0;
      try
        GetMem(aPointer, xStream.Size);
        result := xStream.Read(aPointer^, xStream.Size);
      except
        on e:EOutOfMemory do
          aPointer := nil;
      end;// try except
    finally
      if assigned(xStream) then
        xStream.Free;
    end;// try finally
  end;// if FileExists(aFileName) then begin
end;// TForm1.LoadBinFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           LoadXMLFile
 *  Klasse:           TForm1
 *  Kategorie:        No category
 *  Argumente:        (aFileName)
 *
 *  Kurzbeschreibung: L�dt ein XML File in den Speicher
 *  Beschreibung:
                      -
 --------------------------------------------------------------------*)
function TForm1.LoadXMLFile(aFileName: string): String;
begin
  result := '';
  if FileExists(aFileName) then begin
    with TStringList.Create do try
      LoadFromFile(aFileName);
      result := Text;
    finally
      free;
    end;
  end;// if FileExists(aFileName) then begin
end;// TForm1.LoadXMLFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           OpenExplorer
 *  Klasse:           TForm1
 *  Kategorie:        No category
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: �ffnet den Ordner mit dem gew�nschten File im Explorer.
 *  Beschreibung:
                      -
 --------------------------------------------------------------------*)
procedure TForm1.OpenExplorer(Sender: TObject);
  
  procedure _OpenExplorer(aPath: string);
  var
    i:Integer;
  begin
    // Explorer in der Exploreransicht
    i := shellexecute(Application.handle,'open','Explorer.exe',PChar('/e, /select,' + aPath), nil,SW_SHOWNORMAL);
  
    if i < 32 then
      ShowMessage(Format('Fehler beim �ffnen des Explorers (Fehlernummer: %d).', [i]));
  end;// procedure OpenExplorer(aPath: string);
  
begin
  case TWinControl(Sender).Tag of
    0: _OpenExplorer(edXMLDescriptor.Text);
    1: _OpenExplorer(edBinInput.Text);
    2: _OpenExplorer(edXMLOutput.Text);
    3: _OpenExplorer(edBinOutput.Text);
    4: _OpenExplorer(edXMLInput.Text);
  end;// case TWinControl(Sender).Tag of
end;// TForm1.OpenExplorer cat:No category

//:-------------------------------------------------------------------
(*: Member:           OpenFileDialog
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: W�hlt ein File aus
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.OpenFileDialog(Sender: TObject);
  
  procedure ShowDialog(aEdit: TmmEdit; aFilterIndex: integer = 0);
  begin
    with OpenDialog do begin
      InitialDir := ExtractFilePath(aEdit.Text);
      FileName := ExtractFileName(aEdit.Text);
      FilterIndex := aFilterIndex;
      if Execute then
        aEdit.Text := FileName;
    end;// with TOpenDialog do begin
  end;// ShowDialog
  
begin
  case TWinControl(Sender).Tag of
    0: ShowDialog(edXMLDescriptor, 2);
    1: ShowDialog(edBinInput, 3);
    2: ShowDialog(edXMLOutput, 2);
    3: ShowDialog(edBinOutput, 3);
    4: ShowDialog(edXMLInput, 2);
  end;// case TWinControl(Sender).Tag of
end;// TForm1.OpenFileDialog cat:No category

//:-------------------------------------------------------------------
(*: Member:           OpenNotepad
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (Sender)
 *
 *  Kurzbeschreibung: �ffnet das gew�nschte File im Notepad
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.OpenNotepad(Sender: TObject);
  
  procedure _OpenNotepad(aPath: string; aBinMode: boolean);
  var
    i:Integer;
  begin
    if aBinMode then
      // Bin�rdateien
      i := shellexecute(Application.handle,'open', PChar(edHexeditor.Text), PChar(aPath), nil,SW_SHOWNORMAL)
    else
      // Textdateien
      i := shellexecute(Application.handle,'open',PChar(edTexteditor.Text),PChar(aPath), nil,SW_SHOWNORMAL);
  
    if i < 32 then
      ShowMessage(Format('Fehler beim �ffnen des Editors (Fehlernummer: %d).', [i]));
  end;// procedure OpenExplorer(aPath: string);
  
begin
  case TWinControl(Sender).Tag of
    0: _OpenNotepad(edXMLDescriptor.Text, false);
    1: _OpenNotepad(edBinInput.Text, true);
    2: _OpenNotepad(edXMLOutput.Text, false);
    3: _OpenNotepad(edBinOutput.Text, true);
    4: _OpenNotepad(edXMLInput.Text, false);
  end;// case TWinControl(Sender).Tag of
end;// TForm1.OpenNotepad cat:No category

//:-------------------------------------------------------------------
(*: Member:           saveBinFile
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (aFileName, aBinData, aBinLen)
 *
 *  Kurzbeschreibung: Speichert den "Datenhaufen" in einem bin�ren File
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.saveBinFile(aFileName: string; aBinData: Pointer; aBinLen: integer);
var
  xBinFile: File;
  xStream: TFileStream;
  xPointer: Pointer;
begin
  xStream := nil;
  try
    if FileExists(aFileName) then
      DeleteFile(aFileName);
  
    xStream := TFileStream.Create(aFileName, fmCreate	or fmShareDenyWrite);
    xStream.Position := 0;
    if xStream.Write(aBinData^, aBinLen) <> aBinLen then
      raise Exception.Create('Fehler beim Schreiben des Bin�rfiles');
  finally
    if assigned(xStream) then
      xStream.Free;
  end;// try finally
end;// TForm1.saveBinFile cat:No category

//:-------------------------------------------------------------------
(*: Member:           saveXMLFile
 *  Klasse:           TForm1
 *  Kategorie:        No category 
 *  Argumente:        (aFileName, aXMLOutput)
 *
 *  Kurzbeschreibung: Speichert den XML Stream in einem Textfile
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TForm1.saveXMLFile(aFileName: string; aXMLOutput: string);
begin
  with TStringList.Create do try
    Text := aXMLOutput;
    SaveToFile(aFileName);
  finally
    Free;
  end;// with TStringSlist.Create do try
end;// TForm1.saveXMLFile cat:No category

end.
