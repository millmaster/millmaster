(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxHandlerClass.pas
| Projectpart...: MillMaster EASY, Standard, Pro
| Subpart.......: -
| Process(es)...: T0  Lx_Mainprozess
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 05.06.2003  0.00  khp | Datei erstellt
|==========================================================================================*)

unit LxHandlerClass;

interface

uses
  Classes, mmRegistry, Windows, mmThread,
  LoepfeGlobal, BaseMain, BaseGlobal, mmEventLog, LxWriterClass, SettingsReader;



type
  TLxHandler = class(TBaseMain)
  PRIVATE
  PROTECTED
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; OVERRIDE;
    function GetParamHandle: TMMSettingsReader; OVERRIDE;
  PUBLIC
    constructor Create(aSubSystem: TSubSystemTyp); OVERRIDE;
    destructor Destroy; OVERRIDE;
    function Initialize: Boolean; OVERRIDE;
  end;

  //==============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


{TWSCHandler Class}

constructor TLxHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
end;

//-----------------------------------------------------------------------------

destructor TLxHandler.Destroy;
begin
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TLxHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttLXWriter: begin
        aThread := TLxWriter.Create(mSubSystemDef.Threads[1]);
      end;
  else
    aThread := nil;
    Result := False;
  end;  
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

function TLxHandler.GetParamHandle: TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;
//-----------------------------------------------------------------------------

function TLxHandler.Initialize: Boolean;

begin
  Result := inherited Initialize;
end;
//-----------------------------------------------------------------------------
end.

