//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: LxWriterClass.pas
// Projectpart...: MillMaster EASY, Standard, Pro
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 2000
// Target.system.: Windows 2000
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 05.06.2003  0.00  khp | Datei erstellt
//=========================================================================================*)



unit LxWriterClass;

interface

uses
  windows, Classes, SysUtils, SyncObjs,
  BaseThread, BaseGlobal, mmEventLog, LoepfeGlobal, LxMachThreadClass;

type
  TLxWriter = class(TBaseSubThread)
  PROTECTED
    mCSWriteToMsgHandler: TCriticalSection;
    mNodeList: tNodeList;
    procedure ProcessJob; OVERRIDE;
    procedure ProcessInitJob; OVERRIDE;
  PUBLIC
    procedure WriteToLog(aEvent: TEventType; aText: string);
    procedure WriteToMsgHandler(aPJob: PJobRec = nil);
//    procedure SetMachstatus(aMachID: Integer; aMachstatus: TWscNodeState);
    constructor Create(aThreadDef: TThreadDef); OVERRIDE;
    function DoConnect: Boolean; OVERRIDE;
    function Init: Boolean; OVERRIDE;
    destructor Destroy; OVERRIDE;
  end;

  //=============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf;                             // CodeSite

constructor TLxWriter.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mCSWriteToMsgHandler := TCriticalSection.Create;
end;

//-----------------------------------------------------------------------------

destructor TLxWriter.Destroy;
begin
  mNodeList.StopMachinethread;          // Stop aller MaschThreads
  sleep(100);                          // Warten bis alle Maschinen Threads beendet sind
  mNodeList.free;
  mCSWriteToMsgHandler.Free;
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TLxWriter.DoConnect: Boolean;
begin
  result := inherited DoConnect;
end;

//-----------------------------------------------------------------------------

procedure TLxWriter.WriteToMsgHandler(aPJob: PJobRec);
begin
  mCSWriteToMsgHandler.Enter;
  try
    if Assigned(aPJob) then
      aPJob^.NetTyp := ntWSC
    else
      mJob.NetTyp := ntWSC;
    WriteJobBufferTo(ttMsgDispatcher, aPJob);
  finally
    mCSWriteToMsgHandler.Leave;
  end;
  sleep(1);                             // Threadwechsel erzwingen
end;

//-----------------------------------------------------------------------------

procedure TLxWriter.WriteToLog(aEvent: TEventType; aText: string);
begin
  WriteLog(aEvent, aText);
end;

//-----------------------------------------------------------------------------

function TLxWriter.Init: Boolean;
begin
  Result := inherited Init;
  if Result then begin
    mConfirmStartupImmediately := False;
    try
      mNodeList := tNodelist.create;
    except
      Result := FALSE;
    end;
  end;
end;

//-----------------------------------------------------------------------------

procedure TLxWriter.ProcessJob;
  //..............................................................
  procedure TreatJobToMaThread(aMachID: Integer);
  // Job wird dem betreffenden Maschinen Thread ueber die MsgQueue uebermittelt.
  // Falls Maschine OffLine oder MaschID unbekannt: Meldung an MsgHandler.
  var
    xMachthread:  TLXMachThread;
  begin
    xMachThread := mNodeList.GetMachinethread(nil, aMachID);
    // Job SetMapfile muss weitergeleitet werden auch wenn die Maschine offline ist
    // if (mNodelist.GetMachstate(nil, aMachID) = nsOnline) or (mJob.JobTyp = jtSetMapfile) then
    if mNodelist.GetMachstate(nil, aMachID) = nsOnline then
      if xMachThread <> nil then
        xMachThread.SendJobtoMaThread(mJob)
      else begin                        // Unknow MachNo
        fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'Unknow MachNo');
        mJob.JobResult.JobTyp := mJob.JobTyp;
        mJob.JobTyp           := jtJobResult;
        mJob.JobResult.Error  := Error;
        WriteToMsgHandler;
      end
    else begin                          // Machine OFFLINE
      fError := SetError(ERROR_NOT_READY, etMMError, 'Machine OFFLINE');
      mJob.JobResult.JobTyp := mJob.JobTyp;
      mJob.JobTyp           := jtJobResult;
      mJob.JobResult.Error  := Error;
      WriteToMsgHandler;
    end;
  end;
  //..............................................................
begin
  // This methode is implemented only in derived classes and it is called from
  // the Execute methode of TBaseThread.
  case mJob.JobTyp of
    jtGetNodeList: begin
        // Lesen der mNodeList im WscHandler
 //       mNodeList.GetNodeList(mJob.GetNodeList.NodeItems.LxNetNodeList);
        WriteToMsgHandler;              // NodeList an MsgHandler schicken
      end;
    jtGetMaAssign: begin
        // Get machine Assignment
        TreatJobToMaThread(mJob.GetMaAssign.MachineID);
      end;
    jtClearZESpdData: begin
        // Loescht alle Spindeldaten auf Informator
        TreatJobToMaThread(mJob.ClearZESpdData.MachineID);
      end;
    jtDelZESpdData: begin
        // Acknowledge fuer Spindel Request)
        TreatJobToMaThread(mJob.DelZESpdData.MachineID);
      end;
    jtGetZESpdData: begin
        // regular spindle data aquisition at intervall end
        TreatJobToMaThread(mJob.GetZESpdData.MachineID);
      end;
    jtGetSettings: begin
        // Get settings from prodgroup
        TreatJobToMaThread(mJob.GetSettings.MachineID);
      end;
    jtSetSettings: begin
        // Send settings of a prodgroup to ZE
        TreatJobToMaThread(mJob.SetSettings.MachineID);
      end;
{ TODO -owss : XML: pr�fen; eventuell werden die alle ben�tigten Mapdateien zusammen in 1 Job verschickt}
   { jtSetMapfile: begin
        // Mapdatei wird direkt in der NodeList abgelegt. Schreibzugriff eventuell noch sch�tzen? -> SysUtils.TMutliReadExclusiveWriteSynchronizer
        if mNodeList.Mapfile[mJob.SetMapfile.MapID] <> '' then
          mNodeList.Mapfile[mJob.SetMapfile.MapID] := StrPas(mJob.SetMapfile.Mapfile);
        // Mapinhalt l�schen
        mJob.SetMapfile.Mapfile[0] := #00;
        WriteToMsgHandler;  // Best�tigen
      end;   }
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;
  Sleep(10);
end; {TWSCWriter.ProcessJob}

//-----------------------------------------------------------------------------

procedure TLxWriter.ProcessInitJob;
begin
  case mJob.JobTyp of
    jtSetNodeList: begin
        WriteLog(etInformation, 'SetNodeList received');
        // Load mNodeList to the NetHandler
//        mNodeList.SetNodeList(mJob.SetNodeList.NodeItems.LxNetNodeList);
        // Start aller benoetigten MaschThreads
        mNodeList.StartMachineThread(WriteToMsgHandler, WriteToLog);
// TODO: wss: andere M�glichkeit statt sleep() ?
        sleep(5000);  // Prov!! Warten bis alle Threads Online in der NodeListe nachgefuehrt haben
        // Erhalt der NodeListe bestaetigen
        WriteToMsgHandler;
        // Alle andern Thread freigeben (ControlThread, ReadThread)
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d',
                                   [IPCClientIndex[cMainChannelIndex]^.Error]));
          WriteLog(etError, 'Init confirm failed');
          Exit;
        end;
      end;
  else
    ProcessJob;
  end;
end; {TWSCWriter.ProcessInitJob}

end.

