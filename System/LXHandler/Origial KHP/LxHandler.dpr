//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: LxHandler.pas
// Projectpart...: MillMaster EASY, Standard, Pro
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
// Target.system.: Windows 2000/XP
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.07.2004  0.00  khp | Datei erstellt
//=========================================================================================*)


program LxHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  MemCheck,
  LoepfeGlobal,
  BaseMain,
  BaseGlobal,
  Windows,
  ActiveX,
  SysUtils,
  LxDeviceHandlerClass in 'LxDeviceHandlerClass.pas',
  LxMachThreadClass in 'LxMachThreadClass.pas',
  LxHandlerClass in 'LxHandlerClass.pas',
  LxWriterClass in 'LxWriterClass.pas',
  LxDBAccessDLL_DEF in 'LxDBAccessDLL_DEF.pas';

var
  xLxHandler: TLxHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('WSCHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    xLxHandler := TLxHandler.Create(ssLxHandler);
    if xLxHandler.Initialize then
      xLxHandler.Run;
    xLxHandler.Free;
  finally
    CoUninitialize;
  end;
end.

