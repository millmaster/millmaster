//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: LxDeviceHandlerClass.pas
// Projectpart...: MillMaster EASY, Standard, Pro
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows 2000
// Target.system.: Windows 2000
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.07.2004  0.00  khp | Datei erstellt
// 15.07.2004  0.1   khp | Frame erstellt
//=========================================================================================*)

unit LxDeviceHandlerClass;

interface

uses Sysutils, Windows, Winsock, Forms, Messages, contnrs,
  LxDBAccessDLL_DEF, IPCClass, BaseGlobal, LoepfeGlobal,
  mmEventLog,YMParaDef;

type
  TLxDevHandler = class(TObject)
  private
    mCount: Integer;
//    mDataItemIndex: tdataItemIndex;
    mDb_handle: Integer;
    mInformWndHandle: HWND;
  public
    mConnected: Boolean;
    mInformWndMsg: UINT;
    mInform_socket: Integer;
    mIpAdresse: PChar;
    mJobAcqQueue: TQueue;
    mMachID: Integer;
    constructor Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer;
      const aIpAdresse: PChar; const aMaschID: Integer);
    destructor Destroy; OVERRIDE;
    function CheckInform(aMessage: UINT; aLParam: LPARAM; var aDataItemIndex: Integer;
      var aOffset: Integer): Boolean;
    function Connect(): Boolean;
    function Disconnect(): Boolean;
    function GetData(aDataItemIndex: Integer; aPBuffer: PByte; aBufferSize: Integer;
      aArg1: Integer; aArg2: Integer): Boolean;
    function GetIndex(aDataItemName: PChar): Integer;
    function Inform(aDataItemIndex: Integer): Boolean;
    function InformOff(aDataItemName: PChar): Boolean;
    function InformON(aDataItemName: PChar): Boolean;
    function Lifecheck(): Boolean;
  end;

  //==============================================================================



implementation                          { TWscDevHandler } // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;



constructor TLxDevHandler.Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer;
  const aIpAdresse: PChar; const aMaschID: Integer);
begin
  inherited create;
  mIpAdresse       := aIpAdresse;
  mJobAcqQueue     := nil;
  mMachID          := aMaschID;
  mInformWndMsg    := aInformWndMsg;
  mInformWndHandle := aThreadWndHandle;
  mConnected       := false;
  mCount           := 0;
end;

//------------------------------------------------------------------------------

destructor TLxDevHandler.Destroy;
// Exception werden abgefangen
begin
  try
    if mConnected = True then
      Disconnect;
  except
  end;
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.CheckInform(aMessage: UINT; aLParam: LPARAM;
  var aDataItemIndex, aOffset: Integer): Boolean;
// Bestimmt Index und Offset eines Inform Events
// Exception werden abgefangen
//var
 // xInfomsg: tinfomsg;
begin
  try
    result := false;
    if (aMessage = mInformWndMsg) then begin
      WSAGETSELECTEVENT(aLParam);
      case aLParam of
        FD_READ: begin
                 end;
        FD_WRITE, FD_OOB, FD_ACCEPT, FD_CONNECT, FD_CLOSE: Sleep(1);
      else
      end;
    end;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.Connect: Boolean;
// Baut Verbindung zu LX-ZE auf
// Exception werden abgefangen

var
  xRes: Integer;
  xVal: TDba_result;
begin
  result := false;
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;
  mJobAcqQueue := TQueue.Create;
  try
    mConnected := false;
    try
      xVal := LxDBAccessDLL_DEF.Connect(mIpAdresse, mInformWndHandle, mInformWndMsg, mDb_handle);
    finally
    end;
    if (xVal <> DBA_OK) then
      raise Exception.Create('rdba_init');
    mConnected := true;
    result := true;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.Disconnect: Boolean;
// Schliesst Verbindung zu LX-ZE
// Exception werden abgefangen

begin
  mConnected := false;
  try
    result := false;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.GetData(aDataItemIndex: Integer; aPBuffer: PByte;
  aBufferSize, aArg1, aArg2: Integer): Boolean;
// Zugriff auf LX-ZE DatumItem mittels aDataItemIndex,
// aArg1 Spindel = 1..72 / GruppenNr.= 1..12 oder A..Z; aArg2 Aktueller= 0 / Hintergrundwert = 1
// Resultat: aPBuffer
// Exception werden abgefangen


begin
  try
    result := false;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.Getindex(aDataItemName: PChar): Integer;
// Gibt DataItemIndex zum DataItemName zurueck.
// Exception werden abgefangen
begin
  try
    result := -1;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.Inform(aDataItemIndex: Integer): Boolean;
// Aktiviert Inform auf bestimmtem Datum mittels aDataItemIndex
// Exception werden abgefangen
begin
  try
    result := false;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.InformOff(aDataItemName: PChar): Boolean;
// Deaktiviert Inform auf bestimmtem Datum mittels aDataItemName
// Exception werden abgefangen
begin
  try
    result := false;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.InformON(aDataItemName: PChar): Boolean;
// Aktiviert Inform auf bestimmtem Datum mittels aDataItemName
// Exception werden abgefangen
begin
  try
    result := false;
  except
  end;
end;

//------------------------------------------------------------------------------

function TLxDevHandler.Lifecheck: Boolean;
// Beantwortet Informator-Anfrage mit einem beliebigen RPC Zugriff
// Exception werden abgefangen
begin
  try
    result := false;
  except
  end;
end;


begin
end.

