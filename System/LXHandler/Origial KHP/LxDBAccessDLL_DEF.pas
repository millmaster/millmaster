(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxDBAccessDLL_DEF.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: Definition Unit fuer LX-ZE.dll
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
| Target.system.: Windows 2000/XP
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.07.2004  0.00  khp | Datei erstellt
| 15.07.2004  0.1   khp | Frame erstellt
|=========================================================================================*)

// Konstanten, Datentypen, Functionen und Proceduren fuer den Zugriff
// auf die LX-ZE.dll


unit LxDBAccessDLL_DEF;

interface
uses Windows, BaseGlobal;
//******************************************************************************
// * Const-Deklarationen
//******************************************************************************
const
  DBA_OK = TRUE;
  DBA_ERROR = FALSE;

// DataItems welche auf der LX-datenbank definiert sind
// Spindeldaten
  MM_BASISDATEN = 'mm_basisdaten';
  MM_KLASSIERDATEN = 'mm_klassierdaten';
  MM_DATA_TRANSACT = 'mm_data_transact';

//Gruppendaten
  YM_GRP_SETTINGS = 'ym_grp_settings';
  LOT_STYLE_PARAM  = 'lot_style_param';

// Maschinenspezifische Daten
  MA_CONFIG = 'ma_config';
  MAP_NAME = 'map_name';
  MAP_FILE = 'map_file';

// Declaration von LX-ZE
  MM_DECLARATION = 'mm_declaration';

// DataItems Size nur f�r Test
// Spindeldaten
  MM_BASISDATENSIZE = 128;
  MM_KLASSIERDATENSIZE = 2000;
  MM_DATA_TRANSACTSIZE = 100;

//Gruppendaten
  YM_GRP_SETTINGSSIZE = 200;
  LOT_STYLE_PARAMSIZE  = 100;

// Maschinenspezifische Daten
  MA_CONFIGSIZE = 150;
  MAP_NAMESIZE = 40;
  MAP_FILESIZE = 10000;

// Declaration von LX-ZE
  MM_DECLARATIONSIZE = 400;

//******************************************************************************
// * Type-Deklarationen und Type-Definitionen
//******************************************************************************

{$MINENUMSIZE 4}                        // Achtung alle Enum Types 4 Byte 32 Bit

type
 // LX-ZE DECLARATION
  TDbHandle = Integer;                  // Zugriffsnummer Remote Prozessdatenbank
  TDba_result = LongBool;                // True, wenn Funktion erfolgreich
  PIpAdr = PChar;                       // Zeiger auf IpAdress des Rechners
  PData = PByte;                        // Zeiger auf Datenbuffer
  TDatasize = Integer;                  // Gr�sse des ben�tigten Datenbuffers
  PDataname = PChar;                    // Zeiger auf Variablenname
  TDataItemIndex = Integer;             // Zugriffsnummer fuer Datenelement
  TArg = Integer;                       // Argument fuer PUT-/GET-Funktion
  TMapName = Pchar;                     // Name der Map


  // Declarationen von der LX-ZE
  TLxDecEvent = (eNone, eInitialReset, eReset, eEntryLocked, eEntryUnlocked, eRange, eSettings, eAdjust,
    eAssignComplete, eStart, eStop, ePreSettings);

  // M�gliche Status der Partien
  TLxGroupState = (efree, eDefined, eInProd, eLotChange);

  // Declarationsrecord pro Partie
  PTLxDeclaration = ^TLxDeclaration;
  TLxDeclaration = packed record
    event: TLxDecEvent;                // Typen Definiert in BaseGlobal
    states: BITSET16;                   // Constanten Definiert in BaseGlobal
    modify: BITSET16;                   // Constanten Definiert in BaseGlobal
    GrpState: TLxGroupState;           // FREI, DEFINIERT, PRODUKTION, PARTIEWECHSEL;  Typen Definiert in BaseGlobal
    prodGrpID: Longint;                 // Nach Aenderung der Settings 0
    Spindfrom, Spindto: Byte;           // Nach Kaltstart in beiden FF oder 1..n
  end;



  // MM_DATA_TRANSFER Status f�r Datentransaktion zur LX_ZE
  eMM_data_state = (MM_DATA_IDLE, MM_DATA_STORE, MM_DATA_READY, MM_DATA_RESTORE, MM_DATA_CLEAR);
  PMM_data_trans = ^TMM_data_trans;
  TMM_data_trans = packed record
    access: Word;
    Spindfrom, Spindto: Byte;           // SpindelNo auf Inf. beginnt bei 0
    JobID: DWORD;
  end;

  TEventtype = ( etDeclaration, etYarnSettings, etTransact);



// Datenbank Zugriffsfunktionen fuer LX-Datenbank, welche in der LX_ZE-DLL vorhanden sind
// Achtung alle Aufrufe mit cdecl
//-------------------------------------------------------------------------------------------------

// function Connect:
// Baut Verbindung zur LX-ZE. Ein WinMsgHnd und eine unique DeclMsgID wird �bergeben. Der dBHandle und das Booleanresult wird zur�ckgegeben.
function connect(aIpAdr: PIpAdr; aWndMsgHnd: HWND; aLxDeclMsgID: Integer;
  var aDbHandle: TDbHandle): Tdba_result;{$IFDEF DLL} cdecl; external 'LX-ZE.dll';{$ENDIF}


// function Disconnect:
// Schliesst Verbindung zur LX-ZE. dBHandle wird freigegeben.
function disconnect(aDbHandle: TDbHandle): Tdba_result; {$IFDEF DLL} cdecl; external 'LX-ZE.dll';{$ENDIF}


// function getDataItemIndex:
// Diese Funktion liefert den Dataltemlndex des mit dataIdemName spezifizierten Datenelementes.
// Mit dem Dataltemindex k�nnen dann weitere Operationen auf das Datenelement durchgef�hrt werden
//(z.B. Lesen oder Schreiben).
function getDataItemIndex(aDbHandle: TDbHandle;
  aDataItemName: PDataname;
  var aDataItemIndex: TDataItemIndex;
  var aDatasize: TDatasize): TDba_result;{$IFDEF DLL} cdecl; external 'LX-ZE.dll'; {$ENDIF}


// function getData:
// Das mit index spezifizierte Datenelement wird aus der LX-Datenbank in den vom Anwender �bergebenen
// Buffer kopiert.
function getData(aDbHandle: TDbHandle;
  aDataItemIndex: TDataItemIndex;
  var aData: PData;
  var aDatasize: TDatasize;
  var aMapName: TMapName;
  aArg1: TArg): TDba_result;{$IFDEF DLL} cdecl; external 'LX-ZE.dll'; {$ENDIF}


// function putData:
// Das mit index spezifizierte Datenelement aus der Proze�datenbank wird mit den vom Anwender �bergebenen Daten
// geschrieben.  Die vom Anwender �bergebene datasize wird mit der Beschreibung im Inhaltsverzeichnis der LX-Datenbank verglichen.
// Bei Abweichungen wird der Schreibauftrag nicht ausgef�hrt und ein entsprechender Fehler gemeldet.

function putData(aDbHandle: TdbHandle;
  aDataItemIndex: TDataItemIndex;
  aData: PData;
  aDatasize: Tdatasize;
  aMapName: TMapName;
  aArg1: Targ): Tdba_result;{$IFDEF DLL} cdecl; external 'LX-ZE.dll'; {$ENDIF}




implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
// Wird nur f�r Tests ben�tigt-------------------------------------------------------
{$IFNDEF DLL}

function connect(aIpAdr: PIpAdr; aWndMsgHnd: HWND; aLxDeclMsgID: Integer;
  var aDbHandle: TDbHandle): Tdba_result;
begin
  result:= FALSE;
  if aIpAdr <>' ' then begin
    aDbHandle:= Integer(aWndMsgHnd);
    result:= TRUE;
  end;
end;

function disconnect(aDbHandle: TDbHandle): Tdba_result;
begin
  result:= FALSE;
  if aDbHAndle <> 0 then begin
    result:= TRUE;
  end
end;

function getDataItemIndex(aDbHandle: TDbHandle;
  aDataItemName: PDataname;
  var aDataItemIndex: TDataItemIndex;
  var aDatasize: TDatasize): TDba_result;
begin
  result:= FALSE;
  if (aDbHAndle <> 0) and (aDataItemName <> ' ') then begin
    aDatasize:= 0;
    result:=TRUE;
    if aDataItemName = MM_BASISDATEN then begin aDataItemIndex:= 1;aDatasize:= MM_BASISDATENSIZE end
    else if aDataItemName = MM_KLASSIERDATEN then begin aDataItemIndex:= 2;aDatasize:= MM_KLASSIERDATENSIZE end
    else if aDataItemName = MM_DATA_TRANSACT then begin aDataItemIndex:= 3;aDatasize:= MM_DATA_TRANSACTSIZE end
    else if aDataItemName = YM_GRP_SETTINGS then begin aDataItemIndex:= 4;aDatasize:= YM_GRP_SETTINGSSIZE end
    else if aDataItemName = LOT_STYLE_PARAM then begin aDataItemIndex:= 5;aDatasize:= LOT_STYLE_PARAMSIZE end
    else if aDataItemName = MA_CONFIG then begin aDataItemIndex:= 6;aDatasize:= MA_CONFIGSIZE end
    else if aDataItemName = MAP_NAME then begin aDataItemIndex:= 7;aDatasize:= MAP_NAMESIZE end
    else if aDataItemName = MAP_FILE then begin aDataItemIndex:= 8;aDatasize:= MAP_FILESIZE end
    else if aDataItemName = MM_DECLARATION then begin aDataItemIndex:= 9;aDatasize:= MM_DECLARATIONSIZE end
    else aDataItemIndex:= 10
  end;
end;

function getData(aDbHandle: TDbHandle;
  aDataItemIndex: TDataItemIndex;
  var aData: PData;
  var aDatasize: TDatasize;
  var aMapName: TMapName;
  aArg1: TArg): TDba_result;
begin
  result:= FALSE;
  if (aDbHAndle <> 0) and (aDataItemIndex <> 0) then begin
    result:= TRUE;
    CASE aDataItemIndex of
    1: begin aDatasize:= MM_BASISDATENSIZE end;
    2: begin aDatasize:= MM_KLASSIERDATENSIZE end;
    3: begin aDatasize:= MM_DATA_TRANSACTSIZE end;
    4: begin aDatasize:= YM_GRP_SETTINGSSIZE end;
    5: begin aDatasize:= LOT_STYLE_PARAMSIZE end;
    6: begin aDatasize:= MA_CONFIGSIZE end;
    7: begin aDatasize:= MAP_NAMESIZE end;
    8: begin aDatasize:= MAP_FILESIZE end;
    9: begin aDatasize:= MM_DECLARATIONSIZE end;
    else begin aDatasize:= 0;result:= FALSE;  end;
    end;
  end;
end;

function putData(aDbHandle: TdbHandle;
  aDataItemIndex: TDataItemIndex;
  aData: PData;
  aDatasize: Tdatasize;
  aMapName: TMapName;
  aArg1: Targ): Tdba_result;
begin
result:= FALSE;
end;
{$ENDIF}

end.


