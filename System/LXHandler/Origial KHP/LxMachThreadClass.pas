//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: LxMachThreadClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
// Target.system.: Windows 2000/XP
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 23.06.2004  0.00  khp | Datei erstellt
//------------------------------------------------------------------------------------------


unit LxMachThreadClass;

interface

uses
  Windows, Classes, Dialogs, Messages, Forms, Sysutils, extctrls,
  LxDeviceHandlerClass, LxDBAccessDLL_DEF, BaseGlobal, YMDataDef, YMParaUtils, YMParaDef, MMUGlobal,
  BaseThread, mmEventLog, MMXMLConverter;

const
  cMMJobRequestMsg      = 'MMJobRequestMsg';
  cAcqTimerID           = 100;
  cLifeCheckTimerID     = 101;
  cMachstatusTickerID   = 102;
  cAcqTimeout           = 100000;  // ms Timeout beim Acquisation   60s neu 100s
  cLifeCheckTimeout     = 30000;
  cMachstatusTickertime = 60000;
  cPartieNoHeaderStr    = 'MM';
  cPartieNoDelimiterStr = '-';
  cStringListDelimiter  = ',';
  cComma                = ',';
  cDot                  = '.';


type
  TWSCDaten = packed record
    baseData: TBaseData;
    klassierdaten: packed record
      defectData: TWSCDefectData;
      FFDataDark:   TWSCSiroData;
      FFDataBright: TWSCSiroData; // Neu f�r Informator L 5.61
    end;
  end;

  TWriteToMsgHnd = procedure(aPJob: PJobRec) of object;
  TSetMachstatus = procedure(aMachID: Integer; aMachstatus: TWscNodeState) of object;
  TWriteToLog    = procedure(aEvent: TEventType; aText: string) of object;

  // Forward Deklaration von TInformThread;
  TLXMachThread = class;

  TNodeListType = record
    MaschineThread: TLXMachThread;
    LxNetNodeRec: TLxNetNodeRec;
  end;


  TNodeList = class(TObject)
  PRIVATE
    fLastMachpos: integer;
//TODO wss: hier wird ein statisches Array von 101 [0..100] Elementen erstellt. Warum 101?
    fNodeList: array[cFirstMachPos..cMaxWscMachine] of TNodeListType;
    mMapfileList: TStringList;
    mMapfileSynch: TMultiReadExclusiveWriteSynchronizer;
    function  GetMapfile(aMapID: String): String;
    function  GetMapfileFromMachID(aMachID: Integer): String;
    function  GetMapID(aMachID: Integer): String;
    procedure SetMapfile(aMapID: String; const Value: String);
    procedure SetMapfileFromMachID(aMachID: Integer; const Value: String);
    procedure SetMapID(aMachID: Integer; const Value: String);
  PROTECTED
  PUBLIC
    property LastMachpos: Integer READ fLastMachpos;
    property Mapfile[aMapID: String]: String read GetMapfile write SetMapfile;
    property MapfileFromMachID[aMachID: Integer]: String read GetMapfileFromMachID write SetMapfileFromMachID;
    property MapName[aMachID: Integer]: String read GetMapID write SetMapID;

    constructor Create;
    destructor Destroy; Override;
    procedure  SetNodeList(const aNodeList: TLxNetNodeList);
    procedure  GetNodeList(var aNodeList: TLxNetNodeList);
    function   GetMachstate(aIpdress: Pchar; aMachID: Integer): TLxNodeState;
    procedure  SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TLxNodeState);
    procedure  StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
    function   GetMachinethread(aIpdress: Pchar; aMachID: Integer): TLXMachThread;
    procedure  StopMachinethread();
  end;

  TLXMachThread = class(TThread)
  private
    mJobRequestMsg: UINT;
    mLxDevHandler: TLxDevHandler;
    mLxInformMsg: Integer;
    mMachID: Integer;
    mNodeList: TNodeList;
    mThreadIpAdresse: PChar;
    mWriteToLog: TWriteToLog;           // Methoden Referenz zu WriteLog in LxWriterClass
    mWriteToMsgHnd: TWriteToMsgHnd;     // Methoden Referenz zu WriteJobBufferTo in LxWriterClass
  protected
    procedure BinToXMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec);
    procedure CleanDataAcqQueue();
    function  ConnectingMachine(): Boolean;
    function  DisConnectingMachine(): Boolean;
    procedure Execute; override;
    procedure LXDispatcher(aMsg: TMsg);
    procedure MMJobDispatcher(aMsg: TMsg);
    function  SendMachinenstatus(): Boolean;
    procedure XMLToBinEventHandler(Sender: TObject; var aEventRec: TElementEventRec);
  public
    mThreadWndHandle: HWND;             // Window Handle fuer Thread
    constructor Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
      const aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
    destructor Destroy; OVERRIDE;
    function SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
  end;



implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS,
  IPCClass;

type
  LifecheckEx = class(Exception);
  WSCAccessException = class(Exception);

//==============================================================================
// CLASS TNodeList
//==============================================================================
constructor TNodeList.Create;
begin
  inherited Create;
  mMapfileList    := TStringList.Create;
  mMapfileSynch := TMultiReadExclusiveWriteSynchronizer.Create;
end;
//:-----------------------------------------------------------------------------
destructor TNodeList.Destroy;
begin
  mMapfileSynch.Free;
  mMapfileList.Free;
  inherited Destroy;
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetNodeList(const aNodeList: TLxNetNodeList);
var
  i: Integer;
begin
  // Clean fNodeList
  for i := cFirstMachPos to cMaxWscMachine do begin
    fNodeList[i].LxNetNodeRec.IpAdresse := '';
    fNodeList[i].LxNetNodeRec.MachID    := 0;
    fNodeList[i].LxNetNodeRec.MapName   := '';
    fNodeList[i].LxNetNodeRec.machstate := nsNotValid;
    fNodeList[i].MaschineThread         := nil;
  end;
  i := cFirstMachPos;
  while (aNodelist[i].MachID <> 0) and (i <= cMaxWscMachine) do begin
    fNodeList[i].LxNetNodeRec.IpAdresse := aNodeList[i].IpAdresse;
    fNodeList[i].LxNetNodeRec.MachID    := aNodeList[i].MachID;
    fNodeList[i].LxNetNodeRec.MapName   := aNodeList[i].MapName;
    fNodeList[i].LxNetNodeRec.machstate := aNodeList[i].machstate;
    INC(i);
    fLastMachpos := i - 1;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.GetNodeList(var aNodeList: TLxNetNodeList);
var
  i: integer;
begin
  // Copy fNodeList to aNodelist
  for i := cFirstMachPos to cMaxWscMachine do begin
    aNodeList[i].IpAdresse := fNodeList[i].LxNetNodeRec.IpAdresse;
    aNodeList[i].MachID    := fNodeList[i].LxNetNodeRec.MachID;
    aNodeList[i].MapName     := fNodeList[i].LxNetNodeRec.MapName;
    aNodeList[i].machstate := fNodeList[i].LxNetNodeRec.machstate;
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMachstate(aIpdress: Pchar; aMachID: Integer): TLxNodeState;
var
  i: integer;
begin
  result := nsOffline;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].LxNetNodeRec.machstate;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].LxNetNodeRec.machstate;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TLxNodeState);
var
  i: integer;
begin
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        fNodelist[i].LxNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
        fNodelist[i].LxNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
var
  i: integer;
begin
  for i := cFirstMachPos to LastMachpos do begin
    with fNodeList[i].LxNetNodeRec do
      fNodeList[i].MaschineThread := TLXMachThread.Create(IpAdresse, MachID, aWriteToMsgHnd, Self, aWriteToLog);
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMachinethread(aIpdress: Pchar; aMachID: Integer): TLXMachThread;
var
  i: integer;
begin
  result := nil;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].lxNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.StopMachinethread();
var
  i: integer;
begin
  for i := cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].MaschineThread <> nil then
      //PostMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
      sendMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
  end
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMapfile(aMapID: String): String;
begin
  mMapfileSynch.BeginRead;
  with TStringList.Create do try
    CommaText := mMapfileList.Values[aMapID];
    Result := Text;
  finally
    Free;
    mMapfileSynch.EndRead;
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMapfileFromMachID(aMachID: Integer): String;
begin
  Result := GetMapfile(GetMapID(aMachID));
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMapID(aMachID: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LxNetNodeRec.MapName;
      BREAK;
    end
  end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapfile(aMapID: String; const Value: String);
begin
  mMapfileSynch.BeginWrite;
  try
    mMapfileList.Values[aMapID] := Trim(Value);
  finally
    mMapfileSynch.EndWrite;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapfileFromMachID(aMachID: Integer; const Value: String);
begin
  SetMapfile(GetMapID(aMachID), Value);
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapID(aMachID: Integer; const Value: String);
var
  i: Integer;
begin
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      fNodelist[i].LxNetNodeRec.MapName := Value;
      BREAK;
    end
  end
end;

//:-----------------------------------------------------------------------------

//==============================================================================
// CLASS Informthread
//==============================================================================
constructor TLXMachThread.Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
  const aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
begin
  mMachID          := aMachID;
  mNodeList        := aNodeList;
  mThreadIpAdresse := aIpAdresse;
  mWriteToMsgHnd   := aWriteToMsgHnd;
  mWriteToLog      := aWriteToLog;
  inherited Create(False);
end;

//------------------------------------------------------------------------------

destructor TLXMachThread.Destroy;
begin
  // Free my own window handle to receive windows messages
  if assigned(mLxDevHandler) then mLxDevHandler.Free;
  DeAllocateHWnd(mThreadWndHandle);
  inherited Destroy;
end;

procedure TLXMachThread.BinToXMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec);
begin
//  ShowMessage(Format('Hier bin ich, ich bin dein BinToXML-Event %s ;-xx!',[aEventRec.ArgumentsString]));

  if AnsiSameText(aEventRec.CalcElementName, 'prodGrpID') then begin
    aEventRec.value := 2147483333;
  end
  else if AnsiSameText(aEventRec.CalcElementName, 'thread_cnt') then begin
    try
      aEventRec.value := StrToInt(aEventRec.ArgumentList.Values['yarn_cnt']) * StrToInt(aEventRec.ArgumentList.Values['yarn_unit']);
    except
      aEventRec.value := 1;
    end;
  end;
end;

//..............................................................................

procedure TLXMachThread.CleanDataAcqQueue();
{ Die DataAcquisation Transaktion wird abgebrochen (Rollback).
  Alle Jobs in der Jobqueue des betreffenden Threads werden geloescht.
  Der Timer fuer die und DataAcquisation wird gestoppt.}

begin
  try
    with mLxDevHandler do
  except
    on E: Exception do begin
      KillTimer(mThreadWndHandle, cAcqTimerID);
      raise WSCAccessException.Create('CleanDataAcqQueue ' + e.message);
    end;
  end;
  KillTimer(mThreadWndHandle, cAcqTimerID)
end;

//..............................................................................

function TLXMachThread.ConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsaufbau aufgerufen.
  Dabei wird die Jobid auf allen MachineGruppen auf 0 gesetzt und dann
  die Online Msg an den MsgHandler geschickt. Zuletzt wird die NodeListe aktualisiert
  und der Timer fuer den Lifecheck gestartet, welcher vom Informator vor Ablauf des
  Timeouts zurueckgesetzt wird. Zusaetzlich wird DataTransaction abgebrochen, Rollback}

begin
  result := false;
  try
    // Update NodeList
    mNodeList.SetMachstate(Nil, mMachID, nsOnline);
  except
  end;
  SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
end;

//..............................................................................

function TLXMachThread.DisConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsabbau aufgerufen.
  Falls der Informator vorher Online war wird der Status an den MsgHandler
  geschickt. Die NodeListe wird aktualisiert und die beiden Timer Lifecheck und
  DataAcquisation werden gestoppt.}
var
  xJob: TJobRec;
begin
  result := false;
  try
    if mLxDevHandler.mConnected then begin
      result                   := mLxDevHandler.Disconnect;
      xJob.JobTyp              := jtMaOffline;
      xJob.Maoffline.MachineID := mMachID;
      mWriteToMsgHnd(@xJob);            // Offline nur nach Onlinestatus zu MsgHandler schicken
      CodeSite.SendString('Offline Msg', mLxDevHandler.mIpAdresse);
    end;
  except
  end;
  // Update NodeList
  mNodeList.SetMachstate(Nil, mMachID, nsOffline);
  KillTimer(mThreadWndHandle, cLifeCheckTimerID);
  KillTimer(mThreadWndHandle, cAcqTimerID);
end;

//--Informthread.Execute;-------------------------------------------------------

procedure TLXMachThread.Execute;
// Informs von der LX-ZE und Jobs vom JobHandler werden pro Maschine verarbeitet
var
  xMsg: TMsg;
  xInformThreadMsg: Boolean;
begin
  try
    FreeOnTerminate  := True;
    mJobRequestMsg   := RegisterWindowMessage(cMMJobRequestMsg);
    mLxInformMsg    := RegisterWindowMessage(mThreadIpAdresse); // Pro Thread unique MsgID loesen
    mThreadWndHandle := AllocateHWnd(nil); // Pro Thread Window mit MsgQueue erzeugen
    if not assigned(mLxDevHandler) then begin
      mLxDevHandler := TLxDevHandler.create(mThreadWndHandle, mLxInformMsg, mThreadIpAdresse, mMachID);
      ConnectingMachine;
      SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil); // Machstatus alle Minuten an MM system
      with mLxDevHandler do begin
        //.. Begin Message Loop.........................................................
        while GetMessage(xMsg, mThreadWndHandle, 0, 0) do begin
          Sleep(1);
          try
            with xMsg do begin
              xInformThreadMsg := False;
              // Kontrolle ob MsgID und Informsocket richtig
              if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
                LXDispatcher(xMsg); // Inform von der LX-ZE
                xInformThreadMsg := True;
              end;
              if Message = mJobRequestMsg then begin
                MMJobDispatcher(xMsg);  // Job vom JobHandler
                xInformThreadMsg := True;
              end;
              if Message = WM_Timer then begin   // Timer-> Transaktionstimer,MachstatusTicker, Lifecheck
                if wParam = cAcqTimerID then begin
                  mWriteToLog(etWarning, 'Timeout for dataready signal, rollback Informator data ' + mLxDevHandler.mIpAdresse);
                  CleanDataAcqQueue;    // Timeout der DataAcquisation Queue
                  xInformThreadMsg := True;
                end else
                  if wParam = cMachstatusTickerID then begin
                    // Machstatus senden
                    SendMachinenstatus;
                  end else
                    if wParam = cLifeCheckTimerID then begin
                      raise LifecheckEx.Create('Timeout LifeCheck ');
                      // Kein Lifecheck der 338 daher Disconnect
                    end;
              end;
              if Message = WM_Quit then
                BREAK;  // Abruch der Schleife durch WSCWriter
              if not xInformThreadMsg then
                // Alle andern Meldungen weiterleiten
                DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
            end;
          except
            on E: WSCAccessException do begin
              mWriteToLog(etWarning, e.message + mLxDevHandler.mIpAdresse);
              CodeSite.SendMsg(e.message + mLxDevHandler.mIpAdresse); // Protokollmeldung !!
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
            on E: LifecheckEx do begin
              //nue              mWriteToLog(etInformation, e.message+ mWSCHandler.mIpAdresse);
              CodeSite.SendMsg(e.message + mLxDevHandler.mIpAdresse);
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
          else begin
              mWriteToLog(etError, 'FatalError' + mLxDevHandler.mIpAdresse);
              CodeSite.SendMsg('Unknow Error');
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
          end;
        end;                            // end while
        //.. End Message Loop.........................................................
      end;
    end;
  finally
    DisConnectingMachine;
  end;
end;

//..............................................................................

procedure TLXMachThread.LXDispatcher(aMsg: TMsg);
{ Diese Methode verarbeitet Informs welche von den DataItems DATA_TRANSFER,
  MM_DECLARATION, GRP_SETTINGS ausgeloest werden.
  Datadirection LX-ZE -> LxHandler }
// Exception werden abgefangen und weitergereicht

var
  xInfIndex, xIndex, xGrpNo, xspind: Integer;
  //xMM_data_trans: tMM_data_trans;
  xDecBuffer: TLxDeclaration;
  //xYM_set_id: Word; xYM_Set_Changed: Boolean; // Dummyparameter fuer GetYMSetParm bei eEntryLocked
  //xBasisdaten_index, xKlassierdaten_index: tdataItemIndex;
  //xJob: TJobRec;
  //xWSCDaten: TWSCDaten;
  //xSize: DWord;
  xJobID: DWord;
  xBuffer: PByte;
  //xXMLData: String;
  //xMapID: String;
  xTmpJob: PJobRec;
  //...............................................................
begin
  try
    with mLxDevHandler do
      with aMsg do begin
        CheckInform(Message, lParam, xInfIndex, xGrpNo);
        if xInfIndex > 0 then
            else
              if TRUE then begin
                // Data Acquisiton beendet, Timeout abbrechen
                KillTimer(mThreadWndHandle, cAcqTimerID);
                // Pruefe ob Queue leer ist
                if mJobAcqQueue.Count > 0 then begin
                  // Hole Job von Queue und sende Ihn erneut
                  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(mJobAcqQueue.Pop), 0);
                end;
              end
//..MM_DECLARATION
          else if xInfIndex = 0 then begin
            case xDecBuffer.event of
                eNone: begin
                  end;
                eAssignComplete: begin
                  end;
                eInitialReset: begin
                  end;
                eReset: begin
                  end;
                eEntryLocked: begin     // Bildschirmschoner oder Logoff auf Informator
                  end;
                eEntryUnlocked: begin   // Login auf Informator
                  end;
                eSettings, eRange: begin
                  end;
                eAdjust: begin
                  end;
                eStart: begin
                    // Grp_Setting aus MachGruppe lesen
                    xBuffer := AllocMem(cBinBufferSize);
                    xTmpJob := Nil;
                    try
                      with TMMXMLConverter.Create do
                      try
                      finally
                        FreeMem(xTmpJob);
                        Free;
                      end; // with TMMXMLConverter.Create
                    finally
                      FreeMem(xBuffer);
                    end; // try
                  end;
                eStop: begin
                      // Grp_Setting aus MachGruppe lesen
                      xBuffer := AllocMem(cBinBufferSize);
                      xTmpJob := Nil;
                      try
                        with TMMXMLConverter.Create do
                        try
                        finally
                          FreeMem(xTmpJob);
                          Free;
                        end; // with TMMXMLConverter.Create
                    finally
                      FreeMem(xBuffer);
                    end; // try
                  end;
                ePreSettings: begin
                    // JobID aus Informator-DB lesen
                     if xJobID <> 0 then begin
                      xBuffer := AllocMem(cBinBufferSize);
                      xTmpJob := Nil;
                      try
                        try
                        finally
                          FreeMem(xTmpJob);
                          Free;
                        end; //with TMMXMLConverter.Create
                      finally
                        FreeMem(xBuffer);
                      end;
                    end else // if xJobID <> 0
                  end; // begin
            else
                CodeSite.SendMsg('Receive Unknown Declartaion');
            end;
          end
        else begin
          KillTimer(mThreadWndHandle, cLifeCheckTimerID);
          //  CodeSite.SendInteger('Lifecheck',mWSCHandler.mInform_socket);
          LifeCheck();
          SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
        end;
      end;
  except
    on E: Exception do raise WSCAccessException.Create('InformDispatcher ' + e.message);
    // on E: Exception do raise Exception.Create('InformDispatcher '+ e.message);
  end;
end;

//..............................................................................

procedure TLXMachThread.MMJobDispatcher(aMsg: TMsg);
// Alle Jobs werden in das LX-ZE spezifische Format konvertiert
// Exception werden abgefangen und weitergereicht
// Datadirection JobHandler -> LxHandler


var
  xPJob: PJobRec;
  xIndex : Integer;
  xFreeJobMem: Boolean;
begin
  xIndex      := 0;
  xFreeJobMem := TRUE;
  with aMsg do begin
    xPJob := PJobRec(wParam);
    try
      case xPJob^.JobTyp of
//..GetZESpdData
        jtGetZESpdData: begin
          end;
//..DelZESpdData
        jtDelZESpdData: begin
          end;
//.. GetSettingsAllGroups
//.. GetSettings
        jtGetSettings: begin
          end;
//.. SetSettings
        jtSetSettings: begin
          end;
//.. ClearZESpdData
        jtClearZESpdData: begin
          end;
//..GetMaAssign
        jtGetMaAssign: begin
          end;
      else
      end;
    except
    end;
  end;
end;

//------------------------------------------------------------------------------

function TLXMachThread.SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
var
  xp: PJobRec;
begin
  result := true;
  xp     := AllocMem(aJobRecord.JobLen);
  move(aJobRecord^, xp^, aJobRecord.JobLen);
  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(xp), 0);
end;

//..............................................................................

function TLXMachThread.SendMachinenstatus(): Boolean;
var
  xJob: TJobRec;
begin
  result := false;
  try
    if mLxDevHandler.mConnected = True then
      xJob.JobTyp := jtMaOnline
    else
      xJob.JobTyp := jtMaOffline;
    xJob.Maoffline.MachineID := mMachID;
    mWriteToMsgHnd(@xJob);              // Offline nur nach Onlinestatus zu MsgHandler schicken
  except
  end;
end;

//:-----------------------------------------------------------------------------

procedure TLXMachThread.XMLToBinEventHandler(Sender: TObject; var aEventRec: TElementEventRec);
begin

end;

end.

