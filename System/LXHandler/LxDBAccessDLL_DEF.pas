(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxDBAccessDLL_DEF.pas
| Projectpart...: MillMaster EASY, Standard, Pro
| Subpart.......: -
| Process(es)...: Definition Unit fuer LX-ZE.dll
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
| Target.system.: Windows 2000/XP
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.07.2004  0.00  khp | Datei erstellt
| 15.07.2004  0.1   khp | Frame erstellt
| 10.05.2005  1.00  Khp | Basis Verion 5.x
| 20.04.2006  1.00  Wss | Redundante Konstanten entfernt -> YMParaDef, MMUGlobal
| 01.09.2006  1.00  Wss | - Imperfektionen in BaseDataRec freigegeben (hinten angeh�ngt!)
                          - 2 Konstanten f�r die Verifizierung des BaseDataSize f�r
                            Entscheidung, ob IPI in BaseData drin sind oder nicht
| 08.10.2006  1.01  Nue | - TLxProdGroupState, pgsTemplate {pgsReserve1} eingef�gt.
| 06.02.2008  1.10  Nue | - MM5.04: Erweiterungen mit TmmMonitoriingData2, VCV-Data; cBaseDataBufferSizeWithIPIandVCV added.
|=========================================================================================*)

// Konstanten, Datentypen, Functionen und Proceduren fuer den Zugriff
// auf die LX-ZE.dll gem�ss Entwurfsbeschreibung vom 30.3.2005


unit LxDBAccessDLL_DEF;

interface
uses Windows, BaseGlobal;
//******************************************************************************
// * Const-Deklarationen
//******************************************************************************
const                                                         
  cLX_DLL_Name = 'LX.dll';
  
const
  cDBA_OK = TRUE;
  cDBA_ERROR = FALSE;

// DataItems welche auf der LX-datenbank definiert sind
// Spindeldaten
  cBASICDATA = 'BASICDATA';
  cCLASSDATA = 'CLASSDATA';
  cDATA_TRANSACT = 'DATA_TRANSACT';
  cVISUAL = 'VISUAL';

// Gruppendaten- und Partiedaten
  cGRP_SETTINGS = 'GRP_SETTINGS';
  cGRP_LOT_STYLE_PARAM  = 'GRP_LOT_STYLE_PARAM';
  cGRP_ASSIGNMENT = 'GRP_ASSIGNMENT';
  cGRP_PRODID = 'GRP_PRODID';
  cGRP_MODIFIED = 'GRP_MODIFIED';
  cGRP_YMSET_PARAM = 'GRP_YMSET_PARAM';
  cGRP_JOBID  = 'GRP_JOB';

// Maschinenspezifische Daten
  cMA_ASSIGNMENT = 'MA_ASSIGNMENT';


// Statische Daten
  cMA_CONFIG ='MA_CONFIG';
  cCLR_SECTION = 'CLR_SECTION';
  cMAPFILE = 'MAPFILE';
  cLIVE_CHECK = 'LIVE_CHECK';


// Declaration von LX-ZE
  cMM_DECLARATION = 'MM_DECLARATION';


// DataItems Size nur f�r Test........................................
// Spindeldaten
  cMM_BASISDATENSIZE = 128;
  cMM_KLASSIERDATENSIZE = 2000;
  cMM_DATA_TRANSACTSIZE = 8;

// Gruppendaten
  cYM_GRP_SETTINGSSIZE = 200;
  cLOT_STYLE_PARAMSIZE  = 100;

// Maschinenspezifische Daten
  cMA_CONFIGSIZE = 150;
  cMAP_NAMESIZE = 40;
  cMAP_FILESIZE = 10000;
  cMapBufferSize= 2000;

// Declaration von LX-ZE
  cMM_DECLARATIONSIZE = 1400;

  cLxBinBufferSize = 2000;
//******************************************************************************
// * Type-Deklarationen und Type-Definitionen
//******************************************************************************

{$MINENUMSIZE 2}                        // Achtung alle Enum Types 2 Byte 16 Bit

type
 // LX-ZE DECLARATION
  TDbHandle = Integer;                  // Zugriffsnummer Remote Prozessdatenbank
  TDba_result = LongBool;               // True, wenn Funktion erfolgreich
  PIpAdr = PChar;                       // Zeiger auf IpAdress des Rechners
  PData = PByte;                        // Zeiger auf Datenbuffer
  TDatasize = Integer;                  // Gr�sse des ben�tigten Datenbuffers
  PDataname = PChar;                    // Zeiger auf Variablenname
  TDataItemIndex = Integer;             // Zugriffsnummer fuer Datenelement
  TArg = Integer;                       // Argument fuer PUT-/GET-Funktion
  TMapName = Pchar;                     // Name der Map

//..............................................................................
// Begin Typedefinitionen von LZE (LmLefInterface, DbAccessTypen) �bernommen
//..............................................................................

  TMonitoringData = packed record
    TotalYarnCuts: Smallint; // total "textil" yarn cuts: UPDR_CHANNEL_CUT||UPDR_CLASS_CUT||
                             // UPDR_SIRO_CUT||UPDR_CLUSTER_CUT||UPDR_YARN_CNT_CUT
    NepCuts: Smallint; //       number of nep cuts,   when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT
                       //       [EReinigerNep, EClassNep]
    ShortCuts: Smallint; //     number of short cuts, when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT
                         //     [EReinigerShort, EClassShort]
    LongCuts: Smallint; //      number of long cuts,  when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT
                        //      [EReinigerLong, EClassLong]
    ThinCuts: Smallint; //      number of thin cuts,  when UPDR_CHANNEL_CUT or UPDR_CLASS_CUT
                        //      [EReinigerThin, EClassThin]

    OffCountPlusCuts: Smallint; //  [ENummerPlus, ENummerPlusAlarm]
    OffCountMinusCuts: Smallint; // [ENummerMinus, ENummerMinusAlarm]
    OffCountCuts: Smallint; //      [ENummerPlus, ENummerPlusAlarm,
                            //       ENummerMinus, ENummerMinusAlarm]
    OffCountAlarms: Smallint; //    [ENummerPlusAlarm, ENummerMinusAlarm]

    ShortOffCountPlusCuts: Smallint; //  [EKurzNummerPlus, EKurzNummerPlusAlarm]
    ShortOffCountMinusCuts: Smallint; // [EKurzNummerMinus, EKurzNummerMinusAlarm]
    ShortOffCountCuts: Smallint; //      [EKurzNummerPlus, EKurzNummerPlusAlarm,
                                 //       EKurzNummerMinus, EKurzNummerMinusAlarm]
    ShortOffCountAlarms: Smallint; //    [EKurzNummerPlusAlarm, ENummerMinusAlarm]

    ShortClusterCuts: Smallint; //   number of cluster cuts: UPDR_CLUSTER_CUT
                                //   [EClusterShort, EClusterShortAlarm
    ShortClusterAlarms: Smallint; // number of cluster locks: UPDR_CLUSTER_CU&&SPINDLE_LOCKED_MT */
                                  // [EClusterShortAlarm]
    LongClusterCuts: Smallint; //    [EClusterLong, EClusterLongAlarm]
    LongClusterAlarms: Smallint; //  [EClusterLongAlarm]
    ThinClusterCuts: Smallint; //    [EClusterThin, EClusterThinAlarm]
    ThinClusterAlarms: Smallint; //  [EClusterThinAlarm]

    FFCuts: Smallint; //        number of siro cuts: UPDR_SIRO_CUT
                      //        [EFFDunkel, EFFHell, EFFHellAlarm, EFFDunkelAlarm,
                      //         ESpliceFFDunkel, ESpliceFFHell]
    FFDarkCuts: Smallint; //    [EFFDunkel, EFFDunkelAlarm, ESpliceFFDunkel]
    FFBrightCuts: Smallint; //  [EFFHell, EFFHellAlarm, ESpliceFFHell]
    FFAlarms: Smallint; //      [EFFHellAlarm, EFFDunkelAlarm]
                        //      number of spindle locks because of siro startup cuts
    SynFFCuts: Smallint; //     [ESynFF, ESynFFAlarm]
    SynFFAlarms: Smallint; //   [ESynFFAlarm]
    FFClusterCuts: Smallint; // SIRO Cluster schnitte
                             // [EFFClusterDunkel, EFFClusterHell, EFFClusterDunkelAlarm , EFFClusterHellAlarm]
    FFClusterAlarms: Smallint; // SIRO Cluster blockierungen
                               // [EFFClusterDunkelAlarm , EFFClusterHellAlarm]

    SFIDPlusCuts: Smallint; //  [ESFI_80m_Plus, ESFI_80m_PlusAlarm, ESFI_10m_Plus, ESFI_10m_PlusAlarm]
    SFIDMinusCuts: Smallint; // [ESFI_80m_Minus, ESFI_80m_MinusAlarm, ESFI_10m_Minus, ESFI_10m_MinusAlarm]
    SFIDCuts: Smallint; //      [ESFI_80m_Plus, ESFI_80m_PlusAlarm, ESFI_10m_Plus, ESFI_10m_PlusAlarm,
                        //       ESFI_80m_Minus, ESFI_80m_MinusAlarm, ESFI_10m_Minus, ESFI_10m_MinusAlarm]
    SFIDAlarms: Smallint; //    [ESFI_80m_PlusAlarm, ESFI_10m_PlusAlarm, ESFI_80m_MinusAlarm, ESFI_10m_MinusAlarm]

    SystemCuts: Smallint; //    number of system cuts: UPDR_SYSTEM_CUT||UPDR_ADJUST_CUT||
                          //    UPDR_ADDITIONAL_CUT
                          //    [ESystem, ESystemAddStillstand, ESystemAddLauf,
                          //     ESystemPowerfail, ESystemTasteCut, ESystemSPRError,
                          //     ESystemAdjust, ESystemVorAdjust
    SystemAlarms: Smallint; //  number of spindle locks because of system problem:
                            //  !(CLUSTER_CUT||YARN_CNT_CUT)&&SPINDLE_LOCKED_M

    UpYarnCuts: Smallint; //    number of muratas upper yarn cuts: UPDR_UPPERYARN_CUT
                          //    [EOberDoppelFaden
    SpliceCuts: Smallint; //    number of splice cuts: UPDR_SPLICE_CUT
                          //    [ESpliceShort, ESpliceThin, ESpliceLong, ESpliceNep]
    BunchCuts: Smallint; //     [EBunch] cuts because of jumping yarn
    DBunchCuts: Smallint; //    [EDelayedBunch] delayed bunch cuts
    DrumWrapCuts: Smallint; //  [ETrommelwickel]

    StartUpCuts: Smallint; //   [EAnlaufSchnittNullError, EAnlaufSchnittMesserBlockierung,
                           //    EAnlaufSchnittSpindelBlockierung, EAnlaufSchnittBlockierung, EAnlaufSchnittNullRef
    NSLTAlarms: Smallint; //    [ECHSAlarm, ECHLAlarm, ECHTAlarm, ECHNAlarm,
                          //     ECLSAlarm, ECLLAlarm, ECLTAlarm, ECLNAlarm]

    Bobbins: Smallint; //       number of used cops: ESSADR_M&&EMPTY_COPS_M
    Cones: Smallint;
    Splices: Smallint; //       No spooled splices: UPDR_ADDITIONAL_CUT||UPDR_CLUSTER_CUT||
                             // UPDR_YARN_CNT_CUT||UPDR_CHANNEL_CUT||UPDR_CLASS_CUT||
                             // UPDR_SIRO_CUT||UPDR_ADJUST_CUT||UPDR_BUNCH_CUT||
                             // UPDR_SYSTEM_CUT||UPDR_NO_SPEC_CUT||UPDR_NO_CUT )
    SpliceRepetitions: Smallint; // number of splice repetitions: splrep
    YarnBreaks: Smallint;
  end;

  //MM5.04: VCV-Daten eingef�gt. Nue:6.02.08
  TmmMonitoringData2 = packed record
    VCVPlusCuts: Smallint;
    VCVMinusCuts: Smallint;
    VCVAlarms: Smallint;
  end;

  TImperfections = record // 3.7.1 Imperfektionen
    Neps: Integer; //     * number of nep imperfections */
    Thick: Integer; //    * number of thick imperfections */
    Thin: Integer; //     * number of thin imperfections */
    Small: Integer; //    * number of small imperfections */
    Len2To4: Integer; //  * number of length imperfections 2..4cm */
    Len4To8: Integer; //  * number of length imperfections 4..8cm */
    Len8To20: Integer; // * number of length imperfections 8..20cm */
    Len20To70: Integer; //* number of length imperfections 20..70cm */
  end;

{  // BASICDATA (Readonly)                           DbAccessTypes.TMonitoringDataRec
  // aArg1 = SpindelNummer (1 .. 80)

  // SfiD := Sqrt( SfidSum / SfidNumber ) / 1.45;
  // Sfi := SfiD * AdjustBase;
  TmmSfi = packed record
    SfidSum: Integer;
    SfidNumber: Integer;
    AdjustBase: Double;
  end;}
  TSfi = packed record // 3.7.2. Oberfl�chenindex SFI
    SfidSum: Integer;
    SfidNumber: Integer;
    AdjustBase: Double;
  end;

  TTimeData = packed record
    RunTime: Integer; // sec
    OperatingTime: Integer;
    WatchTime: Integer;
    LongStopTime: Integer;
    LongStops: Integer;
  end; // 20 Byte


(*   WERTE DER TESTDATEN

  xProdDataRec.Length := 1.0;

  with xProdDataRec.MonitoringData do
  begin
    TotalYarnCuts := 1;
    NepCuts := 2;
    ShortCuts := 3;
    LongCuts := 4;
    ThinCuts := 5;
    OffCountPlusCuts := 6;
    OffCountMinusCuts := 7;
    OffCountCuts := 8;
    OffCountAlarms := 9;
    ShortOffCountPlusCuts := 10;
    ShortOffCountMinusCuts := 11;
    ShortOffCountCuts := 12;
    ShortOffCountAlarms := 13;
    ShortClusterCuts := 14;
    ShortClusterAlarms := 15;
    LongClusterCuts := 16;
    LongClusterAlarms := 17;
    ThinClusterCuts := 18;
    ThinClusterAlarms := 19;
    FFCuts := 20;
    FFDarkCuts := 201;
    FFBrightCuts := 202;
    FFAlarms := 21;
    SynFFCuts := 22;
    SynFFAlarms := 23;
    FFClusterCuts := 24;
    FFClusterAlarms := 25;
    SFIDPlusCuts := 26;
    SFIDMinusCuts := 27;
    SFIDCuts := 28;
    SFIDAlarms := 29;
    SystemCuts := 30;
    SystemAlarms := 31;
    UpYarnCuts := 32;
    SpliceCuts := 33;
    BunchCuts := 34;
    DBunchCuts := 35;
    DrumWrapCuts := 36;
    StartUpCuts := 37;
    NSLTAlarms := 38;
    Bobbins := 39;
    Cones := 40;
    Splices := 41;
    SpliceRepetitions := 42;
    YarnBreaks := 43;
  end;

  with xProdDataRec.ClassCutData do
  begin
    for xI := 0 to 127 do
      CC[ xI ] := 1000 + xI;
    for xI := 0 to 127 do
      CSPC[ xI ] := 2000 + xI;
    for xI := 0 to 63 do
      CFFDC[ xI ] := 3000 + xI;
    for xI := 0 to 63 do
      CFFBC[ xI ] := 4000 + xI;
  end;

  with xProdDataRec.ClassUnCutData do
  begin
    for xI := 0 to 127 do
      CUC[ xI ] := 10000 + xI;
    for xI := 0 to 127 do
      CSPUC[ xI ] := 20000 + xI;
    for xI := 0 to 63 do
      CFFDUC[ xI ] := 30000 + xI;
    for xI := 0 to 63 do
      CFFBUC[ xI ] := 40000 + xI;
  end;

  with xProdDataRec.Imperfections do
  begin
    Neps := 111;
    Thick := 222;
    Thin := 333;
    Small := 44400;
    Len2To4 := 5555;
    Len4To8 := 6666;
    Len8To20 := 7777;
    Len20To70 := 8888;
  end;

  with xProdDataRec.TimeData do
  begin
    RunTime := 5001;
    OperatingTime := 5002;
    WatchTime := 5003;
    LongStopTime := 5004;
    LongStops := 5005;
  end;


*)
const
  cBaseDataBufferSizeWithIPIandVCV = 172;
  cBaseDataBufferSizeWithIPI = 166;
  cBaseDataBufferSizeNoIPI   = 134;
type
  PBaseDataBuffer= ^TLxBaseDataRec;
  TLXBaseDataRec = packed record     // 166 Bytes Total {LX --> Unit MMLefInterface.TmmBasicDataRec}
    Length: Double;                  // 8
    TimeData: TTimeData;             // 20 = 5*integer
    Sfi: TSfi;                       // 16 = 2*Integer + 1*Double
    MonitoringData: TMonitoringData; // 90 = 45*smallint
    Imperfections: TImperfections;   // 32 = 8*integer
    MonitoringData2: TmmMonitoringData2; // 6 = 3*smallint     Nue:6.02.08
  end;

  TLxClassCutData = packed record // total 768 Byte
    CC:   array[ 0..127 ] of Word; //       256 Byte   3.4 Klassier-Daten
    CSPC: array[ 0..127 ] of Word; //     256 Byte   3.5 Spleiss-Klassier-Daten
    CFFDC: array[ 0..63 ] of Word; //      128 Byte   3.6 FF-Klassier-Daten-Dark
    CFFBC: array[ 0..63 ] of Word; //      128 Byte   3.6 FF-Klassier-Daten-Bright
  end;

  TLxClassUnCutData = packed record // total 1280 Byte
    CUC:   array[ 0..127 ] of Cardinal; //  512 Byte
    CSPUC: array[ 0..127 ] of Word; //    256 Byte
    CFFDUC: array[ 0..63 ] of Cardinal; // 256 Byte
    CFFBUC: array[ 0..63 ] of Cardinal; // 256 Byte
  end;

  PClassDataBuffer = ^TLxClassDataRec;
  TLxClassDataRec = packed record
    ClassCutData: TLxClassCutData;
    ClassUnCutData: TLxClassUnCutData;
  end;
  // Achtung mspadding wird ben�tigt damit 2 Byte verwendet werden. (BitSet16)
  TModify =  (msRange, msPara, msAdj, msStart, msStop, mspadding6, mspadding7, mspadding8, mspadding9 );
  TModifySet    = set of TModify;

  // Bit 0: State of Key entry: 0 = unlocked, 1 = locked
  TMachState = (msKeylocked, msInitialReset, mspadding13, mspadding14, mspadding15, mspadding16, mspadding17, mspadding18, mspadding19);
  TMachStateSet = set of TMachState;

{$MINENUMSIZE 1} // Standard                       // Achtung alle Enum Types 1 Byte

  TLxGroupEvent = ( geNone, geRange, geSettings, geAdjust, geAssignComplete, geStart, geStop, gePreSettings );
  TLxMaEvent = ( meNone, meInitialReset, meReset, meEntryLocked, meEntryUnlocked, meSettings, meSections );
  TLxProdGroupState = ( pgsInvalid, pgsDefined, pgsInProduction, pgsFree, pgsTemplate {pgsReserve1}, pgsReserve2, pgsReserve3 );
  TLxTransactionState = ( tsMM_NEW_DATA, tsMM_DATA_READY, tsMM_DATA_ACK, tsMM_DATA_IDLE );

  TLxGrpAssignmentRec = packed record
    GroupEvent: TLxGroupEvent; //byte;
    GroupState: TLxProdGroupState; //byte;
    SpindleFirst: smallint;
    SpindleLast: smallint;
  end;

  TLxMaAssignmentRec = packed record
    MaEvent: TLxMaEvent; //Byte;
//    Dummy1: Byte;
    MachState: TMachStateSet; // BITSET16;  Position des Schl�sselschalter O= Unlocked 1= Locked
{ TODO -olab : BITSET16 = 2 Byte nicht 4 Bytes }
    GroupCount: Byte;
    SectionCount: Byte;
    SpindleCount: Byte;
//    Dummy2: Byte;
  end;

  PMM_data_transRec = ^TLxDataTransactionRec;
  TLxDataTransactionRec = packed record
    TransactNo: LongWord;
    Access: TLxTransactionState; //byte;
    SpindleFrom: Smallint;
    SpindleTo: Smallint;
  end;

  TProdGroupId = packed record
    ProdGroupId: LongWord; // LongWord im MM
    SettingsReady: ByteBool; // f�r MM readonly
  end;// TProdGroupId = packed record

//  TProdGroupId = LongWord;
{ LX Deklaration

  TmmGroupProdIdRec = packed record
    ProdGroupId: Integer; // LongWord im MM
    SettingsReady: Boolean; // f�r MM readonly
  end;}

  TJobId = LongWord;


  TLx_YmSet= packed record
    YM_Name: array[ 0..24 ] of WideChar;
    YM_Changed: Byte;
    YM_ID: WORD;
  end;

{  TYarnUnit = (yuNone, // = 0
               yuNm, // = 1
               yuNeB, // = 2
               yutex, // = 3
               yuTd, // = 4
               yuNeL, // = 5
               yuNeK, // = 6
               yuNeW, // = 7
               yuTs, // = 8
               yuNcC);// TYarnUnit}
  TLXYarnCntUnit = ( lxyuNm, lxyuNe, lxyuTex, lxyuDen, lxyuNew );

  TLxLotStyleRec = packed record // 133 Bytes
    StyleID: LongWord;
    StyleName: array[ 0..24 ] of WideChar; // Name des prod. Artikels
    PartieName: array[0..34 ] of WideChar; // Name der prod. Partie
    Color: LongWord;
    YarnCnt: Single;
//    YarnCntUnit: TLXYarnCntUnit;
    NrOfThreads: Byte;
    Slip: Single;
  end;

  //..............................................................................
// End Typedefinitionen von LZE (LmLefInterface, DbAccessTypen)
//..............................................................................


const
   cBufferSpare= 100;
   cLxBasedatabuffersize = Sizeof(TLxBaseDataRec)+ cBufferSpare;
   cLxClassdatabuffersize = Sizeof(TLxClassDataRec)+ cBufferSpare;


// Datenbank Zugriffsfunktionen fuer LX-Datenbank, welche in der LX_ZE-DLL vorhanden sind
// Achtung alle Aufrufe mit cdecl
//------------------------------------------------------------------------------

// function Connect:
// Baut Verbindung zur LX-ZE. Ein WinMsgHnd und eine unique DeclMsgID wird �bergeben. Der dBHandle und das Booleanresult wird zur�ckgegeben.
function Connect(var aDbHandle: Integer;
  aIpAdr: PChar;
  aWndMsgHnd: HWND;
  aLxDeclMsgID: Integer): LongBool;{$IFNDEF WithoutDLL} cdecl; external cLX_DLL_Name name '_Connect';{$ENDIF}

//------------------------------------------------------------------------------
// function Disconnect:b
// Schliesst Verbindung zur LX-ZE. dBHandle wird freigegeben.
function Disconnect(aDbHandle: Integer):  LongBool; {$IFNDEF WithoutDLL} cdecl; external cLX_DLL_Name name '_Disconnect';{$ENDIF}

//------------------------------------------------------------------------------
// function GetDataItemIndex:
// Diese Funktion liefert den Dataltemlndex des mit dataIdemName spezifizierten Datenelementes.
// Mit dem Dataltemindex k�nnen dann weitere Operationen auf das Datenelement durchgef�hrt werden
//(z.B. Lesen oder Schreiben).
function GetDataItemIndex(aDbHandle: Integer;
  aDataItemName: PChar;
  aMapName: PChar;
  var aDataItemIndex: Integer;
  var aDatasize: Integer): LongBool;{$IFNDEF WithoutDLL} cdecl; external cLX_DLL_Name name '_GetDataItemIndex'; {$ENDIF}

//------------------------------------------------------------------------------
// function GetData:
// Das mit index spezifizierte Datenelement wird aus der LX-Datenbank in den vom Anwender �bergebenen
// Buffer kopiert.
function GetData(aDbHandle: Integer;
  aDataItemIndex: Integer;
  aData: PByte;
  aDatasize: Integer;
  aArg1: Integer): LongBool;{$IFNDEF WithoutDLL} cdecl; external cLX_DLL_Name name '_GetData'; {$ENDIF}

//------------------------------------------------------------------------------
// function PutData:
// Das mit index spezifizierte Datenelement aus der Proze�datenbank wird mit den vom Anwender �bergebenen Daten
// geschrieben.  Die vom Anwender �bergebene datasize wird mit der Beschreibung im Inhaltsverzeichnis der LX-Datenbank verglichen.
// Bei Abweichungen wird der Schreibauftrag nicht ausgef�hrt und ein entsprechender Fehler gemeldet.
function PutData(aDbHandle: Integer;
  aDataItemIndex: Integer;
  aData: PByte;
  aDatasize: Integer;
  aArg1: Integer): LongBool;{$IFNDEF WithoutDLL} cdecl; external cLX_DLL_Name name '_PutData'; {$ENDIF}


//------------------------------------------------------------------------------
// procedure UniCodetoCharArray
// Convertiert UniCodeCharArray in MBCSCharArray. Zu lange Source-Arrays werden auf die
// maximale L�nge des Destination-Arrays gek�rzt.
procedure UniCodetoCharArray(aSource: PWideChar; aSourceLen: Integer; var aDest: Array of Char);

//------------------------------------------------------------------------------
// procedure CharArraytoUniCode
// Convertiert MBCSCharArray in UniCodeCharArray. Zu lange Source-Arrays werden auf die
// maximale L�nge des Destination-Arrays gek�rzt.
procedure CharArraytoUniCode(aSource: PChar; var aDest: Array of WideChar);

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, Sysutils;
//..............................................................................


   procedure UniCodetoCharArray(aSource: PWideChar; aSourceLen: Integer; var aDest: Array of Char);
   var
     xstring: STRING;
     xlength: Integer;
   begin
     WideCharLenToStrVar(aSource, aSourceLen, xstring);
     // Check ob Source Array l�nger als Destination Array
     if  length(xstring) > length(aDest) then
       xlength:= length(aDest)
     else
       xlength:= length(xstring);
     StrPLCopy(aDest, xstring, xlength);
   end;

//..............................................................................
   procedure CharArraytoUniCode(aSource: PChar; var aDest: Array of WideChar);
   var
     xString: string;
     xWideCharArray: Array of WideChar;
   begin
     FillChar(aDest, Length(aDest) * SizeOf(WideChar), 0);
     xString := StrPas(aSource);
     // String auf maximale L�nge k�rzen
     xString := Copy(xString, 1, Length(aDest));
     // Tempor�res Array das auch das abschliessende 0-Byte aufnehmen kann
     SetLength(xWideCharArray, (Length(aDest) + 1));
     // Kopiert mit abschliessendem 0-Byte
     StringtoWideChar(xstring, @xWideCharArray[0], length(xWideCharArray));
     // String ohne 0-Byte kopieren
     Move(xWideCharArray[0], aDest, Length(aDest) * SizeOf(WideChar));
   end;


// Wird nur f�r Tests ben�tigt--------------------------------------------------
{$IFDEF WithoutDLL}

function Connect(var aDbHandle: Integer;
  aIpAdr: PChar;
  aWndMsgHnd: HWND;
  aLxDeclMsgID: Integer): LongBool;

begin
  result:= FALSE;
  if aIpAdr <>' ' then begin
    aDbHandle:= Integer(aWndMsgHnd);
    result:= TRUE;
  end;
end;

function Disconnect(aDbHandle: Integer):  LongBool;
begin
  result:= FALSE;
  if aDbHAndle <> 0 then begin
    result:= TRUE;
  end
end;

function GetDataItemIndex(aDbHandle: Integer;
  aDataItemName: PChar;
  aMapName: PChar;
  var aDataItemIndex: Integer;
  var aDatasize: Integer): LongBool;

begin
  result:= FALSE;
  if (aDbHAndle <> 0) and (aDataItemName <> ' ') then begin
    aDatasize:= 0;
    result:=TRUE;
    if aDataItemName = BASICDATA then begin aDataItemIndex:= 1;aDatasize:= MM_BASISDATENSIZE end
    else if aDataItemName = CLASSDATA then begin aDataItemIndex:= 2;aDatasize:= MM_KLASSIERDATENSIZE end
    else if aDataItemName = DATA_TRANSACT then begin aDataItemIndex:= 3;aDatasize:= MM_DATA_TRANSACTSIZE end
    else if aDataItemName = GRP_SETTINGS then begin aDataItemIndex:= 4;aDatasize:= YM_GRP_SETTINGSSIZE end
    else if aDataItemName = GRP_LOT_STYLE_PARAM then begin aDataItemIndex:= 5;aDatasize:= LOT_STYLE_PARAMSIZE end
    else if aDataItemName = MA_CONFIG then begin aDataItemIndex:= 6;aDatasize:= MA_CONFIGSIZE end
    else if aDataItemName = MAP_FILE_NAME then begin aDataItemIndex:= 7;aDatasize:= MAP_NAMESIZE end
    else if aDataItemName = MAPFILE then begin aDataItemIndex:= 8;aDatasize:= MAP_FILESIZE end
    else aDataItemIndex:= 10
  end;
end;

function GetData(aDbHandle: Integer;
  aDataItemIndex: Integer;
  aData: PByte;
  aDatasize: Integer;
  aArg1: Integer): LongBool;

begin
  result:= FALSE;
  if (aDbHAndle <> 0) and (aDataItemIndex <> 0) then begin
    result:= TRUE;
    CASE aDataItemIndex of
    1: begin aDatasize:= MM_BASISDATENSIZE end;
    2: begin aDatasize:= MM_KLASSIERDATENSIZE end;
    3: begin aDatasize:= MM_DATA_TRANSACTSIZE end;
    4: begin aDatasize:= YM_GRP_SETTINGSSIZE end;
    5: begin aDatasize:= LOT_STYLE_PARAMSIZE end;
    6: begin aDatasize:= MA_CONFIGSIZE end;
    7: begin aDatasize:= MAP_NAMESIZE end;
    8: begin aDatasize:= MAP_FILESIZE end;
    9: begin aDatasize:= MM_DECLARATIONSIZE end;
    else begin aDatasize:= 0;result:= FALSE;  end;
    end;
  end;
end;


function PutData(aDbHandle: Integer;
  aDataItemIndex: Integer;
  aData: PByte;
  aDatasize: Integer;
  aArg1: Integer): LongBool;
begin
result:= FALSE;
end;
{$ENDIF}

end.


