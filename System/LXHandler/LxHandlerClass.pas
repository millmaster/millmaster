(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxHandlerClass.pas
| Projectpart...: MillMaster EASY, Standard, Pro
| Subpart.......: -
| Process(es)...: 
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
| Target.system.: Windows 2000/XP
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.07.2004  0.00  khp | Datei erstellt
| 15.07.2004  0.1   khp | Frame erstellt
| 10.05.2005  1.00  Khp | Basis Verion 5.x
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|=========================================================================================*)
unit LxHandlerClass;

interface

uses
  Classes, mmRegistry, Windows, mmThread,
  LoepfeGlobal, BaseMain, BaseGlobal, mmEventLog, LxWriterClass, SettingsReader;



type
  TLxHandler = class(TBaseMain)
  PRIVATE
  PROTECTED
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; OVERRIDE;
    function GetParamHandle: TMMSettingsReader; OVERRIDE;
  PUBLIC
    constructor Create(aSubSystem: TSubSystemTyp); OVERRIDE;
    destructor Destroy; OVERRIDE;
    function Initialize: Boolean; OVERRIDE;
  end;
  

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


{TLxHandler Class}

//:-------------------------------------------------------------------
constructor TLxHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
end;// TLxHandler.Create cat:No category

//:-------------------------------------------------------------------
destructor TLxHandler.Destroy;
begin
  inherited Destroy;
end;// TLxHandler.Destroy cat:No category

//:-------------------------------------------------------------------
function TLxHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttLxWriter: begin
        aThread := TLxWriter.Create(mSubSystemDef.Threads[1]);
      end;
  else
    aThread := nil;
    Result := False;
  end;
end;// TLxHandler.CreateThread cat:No category

//:-------------------------------------------------------------------
function TLxHandler.GetParamHandle: TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;// TLxHandler.GetParamHandle cat:No category

//:-------------------------------------------------------------------
function TLxHandler.Initialize: Boolean;
var
  xFileVersion: string;
begin
  Result := inherited Initialize;
  if Result then begin
    xFileVersion := 'unknown';
    try
      xFileVersion := GetFileVersion(ParamStr(0));
    except
      // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
    end;
    WriteToEventLog(' started with fileversion ' + xFileVersion, '');
  end;
end;// TLxHandler.Initialize cat:No category

end.

