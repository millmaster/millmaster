(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxDeviceHandlerClass.pas
| Projectpart...: MillMaster EASY, Standard, Pro
| Subpart.......: -
| Process(es)...: 
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
| Target.system.: Windows 2000/XP
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.07.2004  0.00  khp | Datei erstellt
| 15.07.2004  0.1   khp | Frame erstellt
| 10.05.2005  1.00  Khp | Basis Verion 5.x
|=========================================================================================*)
unit LxDeviceHandlerClass;

interface

uses Sysutils, Windows, Forms, Messages, contnrs,
  LxDBAccessDLL_DEF, IPCClass, BaseGlobal, LoepfeGlobal,
  mmEventLog,YMParaDef;

type
  TLxDevHandler = class(TObject)
  private
    mCount: Integer;
    FDb_handle: Integer;
    mDebugEnabled: Boolean;
    mInformWndHandle: HWND;
    FMapName: string;
  public
    mConnected: Boolean;
    mInformWndMsg: UINT;
    mInform_socket: Integer;
    mIpAdresse: PChar;
    mJobAcqQueue: TQueue;
    mMachID: Integer;
    constructor Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer; const aIpAdresse: PChar; const aMaschID: Integer);
    destructor Destroy; OVERRIDE;
    function Connect: Boolean;
    function Disconnect: Boolean;
    function GetData(aDataItemIndex: Integer; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer): Boolean; OVERLOAD;
    function GetData(aDataItemName: String; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer): Boolean; OVERLOAD;
    function GetIndex(aDataItemName: PChar; out aDataItemSize: integer): Integer;
    function GetMapfile: string;
    function GetMapFileName: Boolean;
    function Lifecheck: Boolean;
    function MapfileExists: Boolean;
    function PutData(aDataItemName: String; aPBuffer: PByte; aBufferSize, aArg1: Integer): Boolean;
    property MapName: string read FMapName write FMapName;
    property Db_handle: Integer read FDb_handle;
  end;




implementation                          // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmcs, LxHandlerDef;



//:-----------------------------------------------------------------------------
constructor TLxDevHandler.Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer; const aIpAdresse: PChar; const aMaschID:
    Integer);
begin
  inherited create;
  mIpAdresse       := aIpAdresse;
  mJobAcqQueue     := nil;
  mMachID          := aMaschID;
  mInformWndMsg    := aInformWndMsg;
  mInformWndHandle := aThreadWndHandle;
  mConnected       := false;
  mCount           := 0;
  mDebugEnabled    := GetClassDebug(Classname);
  FMapName         := ''; // = Ung�ltig
end;// TLxDevHandler.Create cat:No category

//:-----------------------------------------------------------------------------
destructor TLxDevHandler.Destroy;
  
  // Exception werden abgefangen
  
begin
  try
    if mConnected = True then
      Disconnect;
  except
  end;
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;
  inherited Destroy;
end;// TLxDevHandler.Destroy cat:No category

//:-----------------------------------------------------------------------------
function TLxDevHandler.Connect: Boolean;
  // Baut Verbindung zu LX-ZE auf
  // Exception werden abgefangen
begin
  mConnected := false;
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;
  mJobAcqQueue := TQueue.Create;
  try
    // Connect auf die Maschine --> R�ckgabe DB Handle
    result:= LxDBAccessDLL_DEF.Connect(fDb_handle, mIpAdresse, mInformWndHandle, mInformWndMsg);
    mConnected := result;
  except
    on E: Exception do begin
      raise Exception.Create('Connect ' + e.message);
    end;
  end;
end;// TLxDevHandler.Connect cat:No category

//:-----------------------------------------------------------------------------
function TLxDevHandler.Disconnect: Boolean;

  // Schliesst Verbindung zu LX-ZE
  // Exception werden abgefangen

begin
  result := false;
  if mConnected and (fDB_Handle > 0) then begin
    try
      result := (LxDBAccessDLL_DEF.Disconnect(fDB_Handle) = cDBA_OK);
    except
      on E: Exception do begin
        raise Exception.Create('Disconnect ' + e.message);
      end;
    end;// try except
  end;
  mConnected:= FALSE;
end;// TLxDevHandler.Disconnect cat:No category

//:-----------------------------------------------------------------------------
function TLxDevHandler.GetData(aDataItemIndex: Integer; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer): Boolean;
  // Zugriff auf LX-ZE DatumItem mittels aDataItemIndex,
  // aArg1 Spindel = 1..72 / GruppenNr.= 1..12 oder A..Z; aArg2 Aktueller= 0 / Hintergrundwert = 1
  // Resultat: aPBuffer
  // Exception werden abgefangen
var
    xMsg: string;

begin
  try
    result := false;
    if mConnected then begin
      if mDebugEnabled then begin
        xMsg := Format('Index = %d, aArg1 = %d; aSize = %d',
                      [aDataItemIndex, aArg1, aBufferSize]);
        CodeSite.SendStringEx(csmRed, '', xMsg);
        SendMemoryToCodeSite(csmGreen, 'aPBuffer^', aPBuffer, aBufferSize)
      end;// if mDebugEnabled then begin
      result := LxDBAccessDLL_DEF.GetData(fDB_Handle, aDataItemIndex, aPBuffer, aBufferSize, aArg1);
      if not Result then begin
        Sleep(1000);
        result := LxDBAccessDLL_DEF.GetData(fDB_Handle, aDataItemIndex, aPBuffer, aBufferSize, aArg1);
        if not Result then begin
          Sleep(2000);
          result := LxDBAccessDLL_DEF.GetData(fDB_Handle, aDataItemIndex, aPBuffer, aBufferSize, aArg1);
        end;
      end;
    end;
  except
    on E: Exception do begin
        raise Exception.Create('GetData ' + e.message);
    end;
  end;
end;// TLxDevHandler.GetData cat:No category

//------------------------------------------------------------------------------

function TLxDevHandler.GetData(aDataItemName: String; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer): Boolean;
// Zugriff auf LX DatumItem mittels DataItemName
// aArg1 Spindel = 0..59 / GruppenNr.= 0..5 ; aArg2 Aktueller= 0 / Hintergrundwert = 1
// Exception werden abgefangen
var
  xBufferSize: integer;
  xIndex: Integer;
begin
{ TODO -cIntegration : Fehlermeldung, wenn DataItemIndex < 0? }
  xIndex := Getindex(PChar(aDataItemName), xBufferSize);
  if xBufferSize > aBufferSize then
    raise EBufferSizeException.CreateFMT('Get Buffersize Error (MachID = %d, %s - expected=%d, current=%d)', [mMachId, aDataItemName, xBufferSize, aBufferSize]);
  Result := GetData(xIndex, aPBuffer, aBufferSize, aArg1);
end;

//:-----------------------------------------------------------------------------
function TLxDevHandler.GetIndex(aDataItemName: PChar; out aDataItemSize:
    integer): Integer;

  // Gibt DataItemIndex zum DataItemName zurueck.
  // Exception werden abgefangen
var
  xmapName: Array [0..MAX_PATH] of Char;
  xMsg: string;
begin
    try
      result := -1;
      aDataItemSize := 0;
      if mConnected then begin
        // Index und Gr�sse des erwarteten Buffers abfragen
        GetDataItemIndex(fDB_Handle, aDataItemName, @xmapName[0], result, aDataItemSize);
        if mDebugEnabled then begin
          xMsg := Format('%s; Index = %d aDataItemSize = %d',
                        [aDataItemName, result, aDataItemSize]);
          CodeSite.SendStringEx(csmRed, '', xMsg);
        end;// if mDebugEnabled then begin
      end;//if mConnected
    except
      on E: Exception do begin
        raise Exception.Create('GetIndex ' + e.message);
      end;
    end;
end;// TLxDevHandler.GetIndex cat:No category

function TLxDevHandler.GetMapfile: string;
var
  xMapfileSize: Integer;
  xIndex: Integer;
  xMapfile: PChar;
begin
  Result := '';

  xIndex := GetIndex(cMAPFILE, xMapfileSize);
  if (xIndex >= 0) and (xMapfileSize > 0) then begin
    xMapfile := AllocMem(xMapfileSize);
    try
      if GetData(xIndex, PByte(xMapfile), xMapfileSize, 0) then
        Result := StrPas(xMapfile);
    finally
      FreeMem(xMapfile);
    end;
  end else begin
    WriteToEventLog('TLxDevHandler.GetMapfile: No Mapfile available from Device.', '', etError);
  end;
end;

//:-----------------------------------------------------------------------------
function TLxDevHandler.GetMapFileName: Boolean;


// Holt Mapfilename �ber GetDataItemIndex Funktion von ZE
// Wird sp�ter um gebaut: MapfileName sollte als DataItem mit GetData verf�gbar sein.
var
  xMapFileName: PChar;
  xDataItemSize: Integer;
  xDataItemIndex: Integer;
begin
  Result := false;
  if mConnected then begin
    xMapFileName := AllocMem(MAX_PATH);
    try
      result:= LxDBAccessDLL_DEF.GetDataItemIndex(fDB_Handle, cMAPFILE, Pointer(xMapFileName), xDataItemIndex, xDataItemSize);
      if result then begin
        xMapFileName := AnsiStrUpper(xMapFileName);
        FMapName := StrPas(xMapFileName);
        FMapName := DeleteSubStr(FMapName, '.MAP'); //.MAP rausfilterm Nue:29.11.06
      end;
      CodeSite.SendString('Mapfile from LX', FMapName);
    finally
      FreeMem(xMapFileName);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
function TLxDevHandler.Lifecheck: Boolean;
var
  xDummy: Integer;
  // Exception werden abgefangen
  
begin
  try
    // Es ist wieder einmal Zeit f�r einen Lifecheck
    // Fragt das DataItem ab, das angibt, wann die DLL das letzte Mal mit der LZE Kontakt hatte
    result:= GetData(cLIVE_CHECK, @xDummy, SizeOf(xDummy), 0)
  except
    result := false;
  end;
end;// TLxDevHandler.Lifecheck cat:No category

function TLxDevHandler.MapfileExists: Boolean;
begin
  if FMapName = '' then
    GetMapFileName;
  Result := (FMapName > '');
end;

//------------------------------------------------------------------------------

function TLxDevHandler.PutData(aDataItemName: String; aPBuffer: PByte; aBufferSize, aArg1: Integer): Boolean;
var
// Speichern eines DataItems auf Informator mittels aDataItemName.
// Mittels aDataItemName wird DataItemIndex und Typedescriptor bestimmt.
// aArg1 Spindel / GruppenNr. ; aArg2 Aktueller- / Hintergrundwert
// Exception werden abgefangen
  xBufferSize: integer;
  xDataItemIndex: integer;
  xMsg: string;
  //xMapFileName : array [0..256] of PChar;
begin
  Result := False;
  xDataItemIndex := GetIndex(PChar(aDataItemName), xBufferSize);

  // Wenn gew�nscht, dann die Daten die zur LZE gesendet werden darstellen
  if mDebugEnabled then begin
    xMsg := Format('%s; Index = %d, aArg1 = %d; aSize = %d; xSize = %d',
                  [aDataItemName, xDataItemIndex, aArg1, aBufferSize, xBufferSize]);
    CodeSite.SendStringEx(csmOrange, '', xMsg);
    SendMemoryToCodeSite(csmYellow, 'aPBuffer^', aPBuffer, xBufferSize)
  end;// if mDebugEnabled then begin

  // Bei Erfolg wird unter anderem die erwartete Gr�sse des Buffers zur�ckgegeben
  if xDataItemIndex >= 0 then begin
    if aBufferSize <= xBuffersize then begin
      result := LxDBAccessDLL_DEF.PutData(fDB_Handle, xDataItemIndex, aPBuffer, xBufferSize, aArg1);
      if not Result then begin
        Sleep(1000);
        result := LxDBAccessDLL_DEF.PutData(fDB_Handle, xDataItemIndex, aPBuffer, xBufferSize, aArg1);
        if not Result then begin
          Sleep(2000);
          result := LxDBAccessDLL_DEF.PutData(fDB_Handle, xDataItemIndex, aPBuffer, xBufferSize, aArg1);
        end;
      end;
    end else begin
      raise EBufferSizeException.CreateFMT('Put Buffersize Error (MachID = %d, %s - expected=%d, current=%d)', [mMachID, aDataItemName, xBufferSize, aBufferSize]);
   end;// if xBufferSize >= aBuffersize then begin
  end;// if xDataItemIndex >= 0 then begin
end;

end.

