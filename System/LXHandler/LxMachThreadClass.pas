(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: LxMachThreadClass.pas
| Projectpart...: MillMaster EASY, Standard, Pro
| Subpart.......: -
| Process(es)...:
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows 2000
| Target.system.: Windows 2000/XP
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.07.2004  0.00  khp | Datei erstellt
| 15.07.2004  0.1   khp | Frame erstellt
| 10.05.2005  1.00  Khp | Basis Verion 5.x
| 08.06.2005  1.01  khp | TLXBaseDataRec lokal
| 20.04.2006  1.01  Wss | MaConfig: Handling so modifiziert, dass auch nicht gestossene
                                    Gruppen mit nicht aufstegenden Spindelbereichen nun
                                    funktioniert
| 02.05.2006  1.02  Nue | RemoveConfigCode so modifiziert, dass Zenit und Spectra richtig
|                       | gehandelt werden k�nnen.
| 02.06.2006  1.03  Wss | CopyBaseData(): AdjustBase von LZE ist Faktor 1000 zu klein, da er nicht wie beim WSC
                                          Faktorbehaftet sondern direkt eine Float Zahl ist.
| 01.09.2006  1.03  Wss | IPI Daten werden zuk�nftig von LZE �bernommen
| 27.09.2006  1.04  Nue | Function UploadAndConvertSections �berarbeitet f�r Handling Sections und Groups(Memories)
| 28.11.2006        Nue | Problematik CheckNewerMapfile handled. GetAndHandleMachMapfile and IsNewerMachineMapfile added.
| 06.02.2008        Nue | VCV-Fields added.
| 25.06.2008        Nue | Fehler (seit Fileversion 0.2 (FreeVCS Version) behoben im Zusammenhang mit YMSetName in TLXMachThread.GetYMSetName!!!!
| 16.07.2008        Nue | Fehler behoben: Array index von mSetSettingsJobIDs von cLXSpdGroupLimit auf cMaxGroupMemoryLimit angepasst.
|=========================================================================================*)

unit LxMachThreadClass;

interface

uses
  Windows, Classes, Dialogs, Messages, Forms, Sysutils, extctrls,
  LxDeviceHandlerClass, LxDBAccessDLL_DEF, BaseGlobal, YMParaUtils, YMParaDef, MMUGlobal,
  BaseThread, mmEventLog, MMXMLConverter, MSXML2_TLB;

const
  cMMJobRequestMsg      = 'MMJobRequestMsg';
  cAcqTimerID           = 100;
  cLifeCheckTimerID     = 101;
  cMachstatusTickerID   = 102;
  cEntryLockedTimerID   = 103;
  cAcqTimeout           = 100000;  // ms Timeout beim Acquisation   60s neu 100s
  cMachstatusTickertime = 60000;   // jede Minute wird der Maschinenstatus dem MsgHandler �bermittelt
  cLifeCheckTimeout     = 30000; //4000;   // alle 30 Sekunden wird der Lifecheck ausgef�hrt
  cEntryLockedTimeout   = 6 * 60 * 1000; // Timeout f�r EntryLocked

// Forward Deklaration von TInformThread;


type
  TWriteToMsgHnd = procedure (aPJob: PJobRec) of object;
  TSetMachstatus = procedure (aMachID: Integer; aMachstatus: TLXNodeState) of object;
  TWriteToLog = procedure (aEvent: TEventType; aText: string) of object;

  // Forward Deklaration von TLXMachThread;
  TLXMachThread = class;


  TNodeListType = record
    MaschineThread: TLXMachThread;
    LxNetNodeRec: TLxNetNodeRec;
  end;


  TNodeList = class(TObject)
  PRIVATE
    FLastMachpos: Integer;
    fNodeList: array[cFirstMachPos..cMaxLXMachine] of TNodeListType;
    mMapfileList: TStringList;
    mMapfileSync: TMultiReadExclusiveWriteSynchronizer;
    function GetCheckNewerMapfile(aMachID: Integer): Boolean;
    function GetIpAdresse(aMachID: Integer): String;
    function GetIPICalcOnMachine(aMachID: Integer): Boolean;
    function GetIsMapOverruled(aMachID: Integer): Boolean;
    function GetMapfile(aMapID: String): String;
    function GetMapfileDOM(aMapID: String): DOMDocument40;
    function GetMapfileAvailable(aMapID: String): Boolean;
    function GetMapID(aMachID: Integer): String;
    procedure ReadMapfilesFromDB(aMapnames: String);
    procedure SetMapfile(aMapID: String; const Value: String);
    procedure SetMapID(aMachID: Integer; const Value: String);
    property Mapfile[aMapID: String]: String read GetMapfile write SetMapfile;
  PROTECTED
  PUBLIC
    property CheckNewerMapfile[aMachID: Integer]: Boolean read GetCheckNewerMapfile;
    property IpAdresse[aMachID: Integer]: String read GetIpAdresse;
    property IPICalcOnMachine[aMachID: Integer]: Boolean read GetIPICalcOnMachine;    //Nue: 15.11.06
    property IsMapOverruled[aMachID: Integer]: Boolean read GetIsMapOverruled;
    property LastMachpos: Integer READ fLastMachpos;
    property MapfileDOM[aMapID: String]: DOMDocument40 read GetMapfileDOM;
    property MapfileAvailable[aMapID: String]: Boolean read GetMapfileAvailable;
    property MapID[aMachID: Integer]: String read GetMapID write SetMapID;
    constructor Create;
    destructor Destroy; Override;
    procedure SetNodeList(const aNodeList: TLXNetNodeList);
    procedure GetNodeList(var aNodeList: TLXNetNodeList);
    function GetMachstate(aIpdress: Pchar; aMachID: Integer): TLXNodeState;
    procedure SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TLXNodeState);
    procedure StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
    function GetMachinethread(aIpdress: Pchar; aMachID: Integer): TLXMachThread;
    procedure GetMapfileFromVersion(aMachID: Integer; aVersion: Integer);
    procedure StopMachinethread();

  end;

  TLXMachThread = class(TThread)
  private
    mDebugEnabled: Boolean;
    mJobRequestMsg: UINT;
    mLxDevHandler: TLxDevHandler;
    mLxInformMsg: Cardinal;
    mMachID: Integer;
    mNodeList: TNodeList;
    mIsZenit: Boolean;
    mLastTransactNo: Integer;
    mLXSection: Integer;
    mLXSectionArr: Array of Integer;
    mTempXMLSettings: TXMLSettingsRec;
    mTempXMLString: string;
    mThreadIpAdresse: PChar;
    mThreadWndHandle: HWND;
    mTransactPending:Boolean;
    // Methoden Referenz zu WriteLog in LXWriterClass
    mWriteToLog: TWriteToLog;
    // Methoden Referenz zu WriteJobBufferTo in LXWriterClass
    mWriteToMsgHnd: TWriteToMsgHnd;

//Alt Org    mSetSettingsJobIDs: array [0..cLXSpdGroupLimit-1] of Integer;
    mSetSettingsJobIDs: array [0..cMaxGroupMemoryLimit-1] of Integer;  //Nue:Bugfix 15.07.08
    procedure EncodeLXVersionNr(aVersNr: IXMLDOMNOde);
{  //............................................................................
  // Local Function
  procedure DeleteLxDecl(aGrpNo: Integer);
  var
   xmodified:  TModifySet;
  begin
    with mLxDevHandler do begin
      //GetData(cGRP_MODIFIED, @xmodified, Sizeof(TModifySet), aGrpNo);
      //xmodified:=[];
      //PutData(cGRP_MODIFIED, @xmodified, Sizeof(TModifySet), aGrpNo);
      // Declaration Event und Modify Flag der Gruppe zuruecksetzen
    CodeSite.SendFmtMsg('MM_RESET_DECL: MaGrpNo=%d', [aGrpNo]);
    end;
  end;}
  //............................................................................
  // Local Function
    procedure GetYMSetName(aMMGroup: Integer; var aMaAssign: TMaAssign);
    procedure RefreshLifeCheckTimer;
    procedure RefreshMachstatusTicker;
  //............................................................................
    procedure RemoveConfigCode(const aMMXMLDoc: DOMDocument40);
  //............................................................................
  // V 5.x
  // Local Function
    function UploadAndConvertMachine(var aXMLData: String): Boolean;
  //............................................................................
  // V 5.x
  // Local Function
    function UploadAndConvertSections(var aXMLData: String; aLXGroup: Integer):
        Boolean;
    function UploadAndConvertSectionsAlt(var aXMLData: String; aLXSection: Integer): Boolean;
  //............................................................................
  // V 5.x
  // Local Function
    function UploadAndConvertSectionsBak(var aXMLData: String; aLXSection:
        Integer): Boolean;
    function UploadAndConvertXMLSettings(var aXMLData: String; aMMGroup: Integer; aIgnoreDeleteFlag: Boolean): Boolean;
  protected
    procedure Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure Bin2XMLMaConfigAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure Bin2XMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    procedure Bin2XMLSettingsAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    procedure Execute; OVERRIDE;
    procedure CleanDataAcqQueue(aConfirmMsg: Boolean);
    function ConnectAndInitMachine: Boolean;
    function DisConnectMachine: Boolean;
    procedure GetGrpAssignment(aMMGroup: Integer; var aMaAssign: TMaAssign); overload;
    procedure GetGrpAssignment(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec); overload;
    procedure GetLotandStyleParam(aMMGroup: Integer; var aMaAssign: TMaAssign); overload;
    procedure GetLotandStyleParam(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec); overload;
    procedure DeleteLxDecl(aMMGroup: Integer);
    procedure GetYMSetParam(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec);
    procedure LxMsgDispatcher(aMsg: TMsg);
    procedure MMJobDispatcher(aMsg: TMsg);
    function SendMachinenstatus: Boolean;
  public
    constructor Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd; const
                       aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
    destructor Destroy; override;
    function BuildMaConfig(aSections, aMachine, aDefaults: string): string;
    procedure SendAssignComplete;
    function SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
  end;


implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS,
  IPCClass, typinfo,
  XMLDef, XMLMappingDef, AdoDBAccess, LoepfeGlobal,
  XMLGlobal, activex, xmlMaConfigClasses, FPatternClasses, JclStrings;
type
  LifecheckEx = class(Exception);
  LXAccessException = class(Exception);

//==============================================================================
// CLASS TNodeList
//==============================================================================

function TNodeList.GetIpAdresse(aMachID: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LxNetNodeRec.IpAdresse;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TNodeList.GetIsMapOverruled(aMachID: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LxNetNodeRec.Overruled;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TNodeList.GetMapfile(aMapID: String): String;
begin
  mMapfileSync.BeginRead;
  with TStringList.Create do try
    CommaText := mMapfileList.Values[aMapID];
    Result := Text;
  finally
    Free;
    mMapfileSync.EndRead;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TNodeList.GetMapfileDOM(aMapID: String): DOMDocument40;
var
  xIndex: Integer;
begin
  Result := Nil;
  mMapfileSync.BeginRead;
  try
    xIndex := mMapfileList.IndexOf(aMapID);
    if xIndex >= 0 then begin
      Result := DOMDocument40(Pointer(mMapfileList.Objects[xIndex]));
      if not Assigned(Result) then begin
        WriteToEventLog('No DOM for MapID found ' + aMapID, 'TMsgPoolList: ', etError);
      end;
    end;
  finally
    mMapfileSync.EndRead;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TNodeList.GetMapfileAvailable(aMapID: String): Boolean;
var
  xIndex: Integer;
begin
  Result := False;
  mMapfileSync.BeginRead;
  try
//    Result := (mMapfileList.Values[aMapID] <> '');
    xIndex := mMapfileList.IndexOf(aMapID);
    if xIndex >= 0 then
      Result := Assigned(mMapfileList.Objects[xIndex]);
  finally
    mMapfileSync.EndRead;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TNodeList.GetMapID(aMachID: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LxNetNodeRec.MapName;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TNodeList.ReadMapfilesFromDB(aMapnames: String);
const
  cQrySelectMapfiles = 'select c_mapfile_name, c_mapfile from t_xml_mapfile ' +
                       'where c_mapfile_name in (%s)';
var
  xDOM: DOMDocument40;
begin
  CodeSite.SendString('Read mapfile for Mapnames', aMapnames);
  if aMapnames > '' then begin
    mMapfileSync.BeginWrite;
    with TAdoDBAccess.Create(1) do
    try
      Init;
      with Query[0] do begin
        SQL.Text := Format(cQrySelectMapfiles, [aMapnames]);
        Open;
        if FindFirst then
          while not EOF do begin
            xDOM := XMLStreamToDOM(FieldByName('c_mapfile').AsString);
            mMapfileList.AddObject(FieldByName('c_mapfile_name').AsString, Pointer(xDOM));
            // verhindert, dass das Interface wieder freigegeben wird
            Pointer(xDOM) := Nil;
            Next;
          end;
      end;
    finally
      Free;
      mMapfileSync.EndWrite;
    end;
  end;// if aMapnames > '' then begin
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapfile(aMapID: String; const Value: String);
var
  xDOM: DOMDocument40;
begin
  mMapfileSync.BeginWrite;
  try
    xDOM := XMLStreamToDOM(Value);
    mMapfileList.AddObject(aMapID, Pointer(xDOM));
    // verhindert, dass das Interface wieder freigegeben wird
    Pointer(xDOM) := Nil;
  finally
    mMapfileSync.EndWrite;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TNodeList.SetMapID(aMachID: Integer; const Value: String);
var
  i: Integer;
begin
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
      fNodelist[i].LxNetNodeRec.MapName := Value;
      if not MapfileAvailable[Value] then
        ReadMapfilesFromDB(Format('''%s''', [Value]));
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
constructor TNodeList.Create;
begin
  inherited Create;
  fLastMachpos  := -1;
  mMapfileList  := TStringList.Create;
  mMapfileSync := TMultiReadExclusiveWriteSynchronizer.Create;
end;

//:-----------------------------------------------------------------------------
destructor TNodeList.Destroy;
var
  i: Integer;
  xIntf: DOMDocument40;
begin
  for i:=0 to mMapfileList.Count-1 do begin
    xIntf := DOMDocument40(Pointer(mMapfileList.Objects[i]));
    xIntf := Nil;
  end;

  mMapfileSync.Free;
  mMapfileList.Free;
  inherited Destroy;
end;

function TNodeList.GetCheckNewerMapfile(aMachID: Integer): Boolean;
var
  i: Integer;
begin
  Result := True;
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LXNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LXNetNodeRec.CheckNewerMapfile;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetIPICalcOnMachine(aMachID: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].LXNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].LXNetNodeRec.IPICalcOnMachine;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetNodeList(const aNodeList: TLXNetNodeList);
var
  i: Integer;
  xMapIDList: TStringList;
begin
  // Clean fNodeList
  xMapIDList := TStringList.Create;
  xMapIDList.Sorted     := True;
  xMapIDList.Duplicates := dupIgnore;
  try
    for i := cFirstMachPos to cMaxLXMachine do begin
      if aNodelist[i].MachID <> 0 then begin
        fNodeList[i].LxNetNodeRec := aNodeList[i];
        if aNodeList[i].MapName <> '' then
          xMapIDList.Add(Format('''%s''', [aNodeList[i].MapName]));
        fLastMachpos := i;
      end
      else begin
        fNodeList[i].LxNetNodeRec.IpAdresse := '';
        fNodeList[i].LxNetNodeRec.MachID    := 0;
        fNodeList[i].LxNetNodeRec.Overruled := False;
        fNodeList[i].LxNetNodeRec.MapName     := '';
        fNodeList[i].LxNetNodeRec.machstate := nsNotValid;
        fNodeList[i].MaschineThread          := nil;
        fNodeList[i].LXNetNodeRec.IPICalcOnMachine := FALSE;   //Nue: 15.11.06
      end;
    end;
  finally
    if xMapIDList.Count > 0 then
      ReadMapfilesFromDB(xMapIDList.CommaText);
    xMapIDList.Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.GetNodeList(var aNodeList: TLXNetNodeList);
var
  i: integer;
begin
  // Copy fNodeList to aNodelist
  for i := cFirstMachPos to cMaxLXMachine do
    aNodeList[i] := fNodeList[i].LxNetNodeRec;
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetMachstate(aIpdress: Pchar; aMachID: Integer): TLXNodeState;
var
  i: integer;
begin
  result := nsOffline;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].LxNetNodeRec.machstate;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].LxNetNodeRec.machstate;
        BREAK;
      end
    end
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TLXNodeState);
var
  i: integer;
begin
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        fNodelist[i].LxNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
        fNodelist[i].LxNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
var
  i: integer;
begin
  for i := cFirstMachPos to LastMachpos do begin
    with fNodeList[i].LxNetNodeRec do
      fNodeList[i].MaschineThread := TLXMachThread.Create(IpAdresse, MachID, aWriteToMsgHnd, Self, aWriteToLog);
  end;
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetMachinethread(aIpdress: Pchar; aMachID: Integer): TLXMachThread;
var
  i: integer;
begin
  result := nil;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].LxNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TNodeList.GetMapfileFromVersion(aMachID: Integer; aVersion: Integer);
const
  cQuery =
    ' select c_mapfile_name from t_xml_mapfile ' +
    ' where c_mapfile_name like ''LX-%s'' ' +
    ' order by c_mapfile_name desc';

var
  xMapfilename: string;
begin
  with TAdoDBAccess.Create(1) do
  try
    Init;
    with Query[0] do begin
      SQL.Text := cQuery;
      if aVersion >= 620      then SQL.Text := Format(cQuery, ['03%'])  // V6.20 - ...
      else if aVersion <= 609 then SQL.Text := Format(cQuery, ['01%'])  // ...   - V6.09
                              else SQL.Text := Format(cQuery, ['02%']); // V6.10 - V6.19
      Open;
      if FindFirst then begin
        xMapfilename := FieldByName('c_mapfile_name').AsString;
        // Mapname nur einf�gen, wenn noch nicht in Liste vorhanden ist
        if not AnsiSameText(MapID[aMachID], xMapfilename) then begin
          CodeSite.SendFmtMsg('GetMapfileFromVersion: MachID: %d, Mapfile: %s', [aMachID, xMapfilename]);
          SetMapID(aMachID, xMapfilename);
        end;
      end;
    end;
  finally
    Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.StopMachinethread();
var
  i: integer;
begin
  for i := cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].MaschineThread <> nil then begin
      PostMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
    end;
  end;
end;

//==============================================================================
// CLASS LXMachThread
//==============================================================================

constructor TLXMachThread.Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
  const aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
begin
  inherited Create(False);
  mMachID          := aMachID;
  mNodeList        := aNodeList;
  mThreadIpAdresse := aIpAdresse;
  mWriteToMsgHnd   := aWriteToMsgHnd;
  mWriteToLog      := aWriteToLog;
  mDebugEnabled    := GetClassDebug(Classname);
end;// TLXMachThread.Create cat:No category

//:-------------------------------------------------------------------
destructor TLXMachThread.Destroy;
begin
  // Free my own window handle to receive windows messages
  if assigned(mLxDevHandler) then mLxDevHandler.Free;
  DeAllocateHWnd(mThreadWndHandle);
  inherited Destroy;
end;// TLXMachThread.Destroy cat:No category

//:-------------------------------------------------------------------
function TLXMachThread.SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
var
  xp: PJobRec;
begin
  result := true;
  xp     := AllocMem(aJobRecord.JobLen);
  move(aJobRecord^, xp^, aJobRecord.JobLen);
  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(xp), 0);
end;// TLXMachThread.SendJobtoMaThread cat:No category

//:-----------------------------------------------------------------------------
procedure TLXMachThread.RefreshLifeCheckTimer;
  // Setzt den Lifecheck Timer zur�ck und gleich wieder neu auf
begin
  KillTimer(mThreadWndHandle, cLifeCheckTimerID);
  if mDebugEnabled then
    CodeSite.SendNote('Lifecheck: Timer refreshed');
  SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
end;


//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.RemoveConfigCode(const aMMXMLDoc: DOMDocument40);
var
  xNode: IXMLDOMNode;
  //............................................................................
  // Local Procedure
  procedure DeleteXPath(aXPath: String);
  var
    xNode: IXMLDOMNode;
  begin
    xNode := aMMXMLDoc.selectSingleNode(aXPath);
    if Assigned(xNode) then
      if Assigned(xNode.parentNode) then
        xNode.parentNode.removeChild(xNode);
  end;

// Begin TLXMachThread.RemoveConfigCode ........................................
begin
  // Von Spectra <> Zenit Abh�ngie Elemente behandeln
  //-------------------------------------------------
  xNode := aMMXMLDoc.selectSingleNode(cXPFP_ZenitItem);
  if Assigned(xNode) then
    mIsZenit := GetElementValueDef(xNode, false)
  else
    mIsZenit := False;

  if mIsZenit then begin
    DeleteXPath(cXPZeroTestItem);                // Cfg A4
//    DeleteXPath(cXPBlockAtFAlarmItem);           // Cfg A10
//    DeleteXPath(cXPBlockAtClusterAlarmItem);     // Cfg A14
//    DeleteXPath(cXPBlockAtYarnCountAlarmItem);   // Cfg A15
    DeleteXPath(cXPFAdjustAfterAlarmItem);       // Cfg B4
    DeleteXPath(cXPCutBeforeAdjustItem);         // Cfg B5
//    DeleteXPath(cXPBlockAtSFIAlarmItem);         // Cfg C0
//    DeleteXPath(cXPBlockAtFClusterAlarmItem);    // Cfg C1
//    DeleteXPath(cXPBlockAtShortCountAlarmItem);  // Cfg C5
    DeleteXPath(cXPFAdjustAtOfflimitItem);      //Cfg B2  (Nue:4.5.06)
  end
  else begin
    DeleteXPath(cXPPClearingDuringSpliceItem);   // Cfg A4
//    DeleteXPath(cXPBlockAtAlarmItem);            // Cfg A14
    DeleteXPath(cXPSuckOffLastCutOnlyItem);      // Cfg A15
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
function TLXMachThread.UploadAndConvertXMLSettings(var aXMLData: String; aMMGroup: Integer; aIgnoreDeleteFlag: Boolean): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
  xSectionData: string;
  xMachData: string;
  xSectionNr: Integer;
  xDOM: DOMDocumentMM;
  xResultDOM: DOMDocumentMM;
  (*---------------------------------------------------------
    F�gt ein Element ins Resultat ein
  ----------------------------------------------------------*)
  procedure AddNodesToResult;
  var
    xSelPath: string;
    xNodeList: IXMLDOMNodeList;
    xNodeIndex: Integer;
  begin
    //Selektiert alle Elemente (ohne die Struktur)
    xNodeList := xDOM.SelectNodes('//*[count(*)=0]');
    for xNodeIndex:=0 to xNodeList.Length - 1 do begin
      xSelPath := GetSelectionPath(xNodeList[xNodeIndex]);
      // Nur einf�gen, wenn das Element noch nicht vorhanden ist
      if not(assigned(xResultDOM.selectSingleNode(xSelPath))) then
        GetCreateElement(xSelPath, xResultDOM).text := xNodeList[xNodeIndex].Text;
    end;// for xNodeIndex := 0 to xNodeList.Length - 1 do begin
  end;// procedure AddNodeToResult(aNewNode: IXMLDOMNode);

begin
  Result   := False;
  aXMLData := '';
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    // XPath f�r die Extrahierung vom FPattern Fragment gleich in der Variable ablegen
    xBuffer := AllocMem(cLxBinBufferSize);
    try

      // GrpSetting aus MachGruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
      mLxDevHandler.GetData(cGRP_SETTINGS, xBuffer, cLxBinBufferSize, aMMGroup + 1);

      // nach XML konvertieren
      // Exceptions werden ganz unten pauschal abgefangen
      with TMMXMLConverter.Create do
      try
        IgnoreDeleteFlag       := aIgnoreDeleteFlag;
        // F�r die Settings muss nichts aus dem DOM herauskopiert werden, da das Resultat gleich dem g�ltigen XML ist
        mTempXMLString         := '';
        // ProdGrp Infos werden in mTempXMLSettings extrahiert
        OnCalcExternal         := Bin2XMLEventHandler;
        OnBeforeDeleteElements := Bin2XMLBeforeDelete;
        OnAfterDeleteElements  := Bin2XMLSettingsAfterDelete;
        aXMLData := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msYMSetting, False);

        Result := True;
      finally
        Free;
      end; //with TMMXMLConverter.Create
    finally
      FreeMem(xBuffer);
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)

  (* Wenn das IgnoreDeleteFlag gesetzt ist, dann m�ssen die kompleten Settings geholt werden (Upload aus Assignment).
     Das heisst, auch die Maschinen und Gruppenelemente m�ssen geholt werden. *)
  if aIgnoreDeleteFlag then begin
    xSectionNr := aMMGroup;// SectionFromSpindlerange(mTempXMLSettings.SpindleFirst, mTempXMLSettings.SpindleLast);
//    if xSectionNr > 0 then begin
      xSectionData := '';
      // Da drin wird zwar die Membervariable mLXSection gesetzt, hat hier jedoch
      // keine Bedeutung mehr.
      UploadAndConvertSections(xSectionData, xSectionNr);
      xDOM := XMLStreamToDOM(cXMLHeader + xSectionData + cXMLFooter);

      xResultDOM := XMLStreamToDOM(aXMLData);
//      if xSectionFound then
        AddNodesToResult;
//    end;// if xSectionNr > 0 then begin

    UploadAndConvertMachine(xMachData);
    xDOM := XMLStreamToDOM(xMachData);
    AddNodesToResult;
    aXMLData := xResultDOM.xml;
  end;// if aIgnoreDeleteFlag then begin
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xDFS: Integer;
  xDFS1: IXMLDOMNode;
  xDFS2: IXMLDOMNode;
//  xSFS1:  IXMLDOMNode;
  xFPattern: TBaseFPHandler;
begin
  (* Leider ein kleiner Sch�nheitsfehler im MMSchema.
     DFS Sensitivity kommt urspr�nglich von zwei einzelnen ConfigCode Bits. Diese beiden Bits
     ergeben zusammen den entsprechenden Wert. In der LX-Zentrale kommmt aber nur ein Wert an.
     Dieser Wert muss nun auf die beiden Elemente aufgeteilt werden.

     Tabelle:
       00    = 1x
       10/01 = 2x
       11    = 4x
   *)
  xDFS1 := aMMXMLDoc.selectSingleNode(cXPDFSSensitivity1Item);
  xDFS2 := aMMXMLDoc.selectSingleNode(cXPDFSSensitivity2Item);
  xDFS := ElementToIntDef(xDFS1, 0);
  if xDFS <= 1 then begin
    SetElementValue(xDFS1, 0);
    SetElementValue(xDFS2, 0);
  end else if xDFS <= 2 then begin
    SetElementValue(xDFS1, 1);
    SetElementValue(xDFS2, 0);
  end else begin
    SetElementValue(xDFS1, 1);
    SetElementValue(xDFS2, 1);
  end;

{  // SFS wird von der LZE direkt als Wert angegeben werden
  xSFS1 := aMMXMLDoc.selectSingleNode(cXPSFSSensitivityItem);
  if ElementToIntDef(xSFS1, 0) = 0 then
    SetElementValue(xSFS1, 'false')
  else
    SetElementValue(xSFS1, 'true');}

  // SensingHead Klase aus FPattern ermitteln
  if Assigned(aMMXMLDoc) then begin
    xFPattern := TBaseFPHandler.Create(aMMXMLDoc.SelectSingleNode(cXPFPatternNode));
    try
      mTempXMLSettings.HeadClass := GetSensingHeadClass(xFPattern);
    finally
      FreeAndNil(xFPattern);
    end;// with TBaseFPHandler.Create(aMMXML.SelectSingleNode(cXPFPatternNode) do try
  end;// if Assigned(aMMXMLDoc) then begin
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.Bin2XMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
var
  xSH: TSensingHead;
begin
  if not VarIsNull(aEventRec.Value) then begin
// ---------------------------- Settings Record -----------------------------------------
    // Partie Abh�ngie Elemente behandeln
    //-------------------------------------------------
    if AnsiSameText(aEventRec.CalcElementName, cXPSensingHeadItem) then begin
      xSH := GetSensingHeadValue(aEventRec.Value);
      mTempXMLSettings.HeadClass := GetSensingHeadClass(xSH);
    end else
//    if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
//      { TODO -oLok -cKonvertierung : Gruppe Kontrollieren (nach Doku 1 basiert) }
//      mTempXMLSettings.Group := aEventRec.Value - 1  // LX antwortet 1-basiert - MM erwartet 0-basiert
//    else
    if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
      mTempXMLSettings.ProdGrpID := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
      mTempXMLSettings.PilotSpindles := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
      mTempXMLSettings.SpindleFirst := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
      mTempXMLSettings.SpindleLast := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
      mTempXMLSettings.SpeedRamp := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
      mTempXMLSettings.Speed := aEventRec.Value
    else

// --------------------------------- Konvertierungs Logik -------------------------------------
    // In der LX ist der Referenz Typ ein eigenes Element (0 = constant, 1 = float)
    if AnsiSameText(aEventRec.CalcElementName, cXPReferenceItem) then begin
      if aEventRec.ArgumentList.ValueDef(cXPRefTypeItem, 0) = 1 then
        aEventRec.Value := 0;
    end;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.Bin2XMLMaConfigAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
var
  xNode: IXMLDOMNode;
  xBaseDataBufferSize, xBasedataIndex : Integer;
  xtmp: string;
begin
  // LX Versionsnummer konvertieren (falls vorhanden)
  EncodeLXVersionNr(aMMXMLDoc.selectSingleNode(cXPYMSwVersionItem));

  //Set IPICalculatedOnMachine: Muss LEIDER aufgrund der L�nge der Basisdata bestimmt werden (134 ohne IPI; 166 mit IPI) Nue:15.11.06
  //                                                                                                     (mit VCV 172) Nue:26.02.08
  try
    xBasedataIndex := mLxDevHandler.Getindex(cBASICDATA, xBaseDataBufferSize);
    if xBaseDataBufferSize >= cBaseDataBufferSizeWithIPI then begin
CodeSite.SendStringEx(csmIndigo, 'LX-Bin2XMLMaConfigAfterDelete:', Format('LXRecordSize:%d',[xBaseDataBufferSize]));
      xNode := aMMXMLDoc.selectSingleNode(cXPIPICalculatedOnMachineItem);
      SetElementValue(xNode, cTrueXML);

      if xBaseDataBufferSize > cBaseDataBufferSizeWithIPIandVCV then begin
        //Basisdatenl�nge stimmt nicht mit der erwarteten L�nge �berein (darf nicht vorkommen)
        mWriteToLog(etWarning, Format('MachineID:%d, Basedatalength IS=% and SHOULD=%d is different!!',[mMachID, xBaseDataBufferSize,cBaseDataBufferSizeWithIPI]));
      end;
    end
    else begin
      //Der Defaultwert 'False' wurde bereits eingef�llt
    end;
  except
    on e: Exception do
      mWriteToLog(etError,(Format('MachineID:%d, Setting value for element %s failed! Set (default) to FALSE.',
                            [mMachID, cXPIPICalculatedOnMachineItem])+ e.Message));
  end;


  if mTempXMLString <> '' then begin
    xNode := aMMXMLDoc.selectSingleNode(mTempXMLString);
    if Assigned(xNode) then
      mTempXMLString := xNode.xml;
  end;
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.Bin2XMLSettingsAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
begin
  RemoveConfigCode(aMMXMLDoc);
end;

(*---------------------------------------------------------
  Stellt das MaConfig XML zusammen.
  Als Argumente werden die Sektionen, die Maschine und die Defaults �bergeben.
  Diese Daten werden vorg�ngig geholt und als strings �bergeben.
  Als erstes werden die Sektionen als gesammtes in ein XML eingef�gt. Unten wird
  dann der Maschinenteil angef�gt. Einzelne Elemente, die von der LX-Zentrale als
  Maschinensettings behandelt werden, sind im Millmaster unter Gruppensettings
  erfasst und m�ssen deshalb in die einzelnen Sektionen verschoben werden.
  Am Schluss werden noch die Defaultsettings angeh�ngt. Das ganze wird dann als
  String wieder an die aufrufende Funktioon �bergeben.
----------------------------------------------------------*)
function TLXMachThread.BuildMaConfig(aSections, aMachine, aDefaults: string):
    string;
var
  xMaConfig: DomDocumentMM;
  xTempDOM: DomDocumentMM;
  xNewDefaultElement: IXMLDOMElement;
  xMMGroup: Integer;
  xNodeList: IXMLDOMNodeList;
  xConfigNode: IXMLDOMNode;
  xTempSettigns: string;
  xSettingsConfigCode: IXMLDOMNode;
  xNodeIndex: Integer;
  xSelectionPath: string;
  (*---------------------------------------------------------
    F�gt Knoten aus dem Maschinenteil in jede Sektion ein
  ----------------------------------------------------------*)
  procedure AppendMaNodeToSections(aSelPath: string);
  var
    xNode: IXMLDOMNode;
    xParentPath: string;
    xNodeList: IXMLDOMNodeList;
    xNodeIndex: Integer;
    xParentElement: IXMLDOMElement;
  begin
    // Selektiert das Element das in die Sektionen muss
    xNode := xTempDOM.selectSingleNode(aSelPath);
    // Um das Element am richtigen Ort einzuf�gen, muss der Elter bekannt sein
    xParentPath := GetSelectionPath(xNode.ParentNode);

    // Selektiert alle Gruppenelemente
    xNodeList := xMaConfig.SelectNodes(cXPGroupNode);
    for xNodeIndex:=0 to xNodeList.Length-1 do begin
      // Sicherstellen, dass das Parent Element existiert (Ausgehend vom Gruppenelement
      xParentElement := GetCreateElement(RemoveXPRoot(cXPGroupNode, xParentPath), xMaConfig, xNodeList[xNodeIndex]);
      // Kopie einf�gen
      xParentElement.AppendChild(xNode.cloneNode(true));
    end;// for i := 0 to xNodeList.Length - 1 do begin
  end;// procedure AppendMaNodeToSections(aSelPath: string);
begin
  (* Alle Sektionen werden in ein tempor�res Dom geladen, danach werden alle enthaltenen
     Gruppen Elemente in das endg�ltige MaConfig DOM verschoben. IM MaConfig Dom muss zuerst
     aber noch das Config Element erzeugt werden --> GetCreateElement(). *)
  xTempDOM := XMLStreamToDOM(cXMLHeader + aSections + cXMLFooter);
  InitXMLDocument(xMaConfig, cXMLLoepfeBody);
  xNodeList := xTempDOM.SelectNodes(cXPGroupNode);
  for xNodeIndex:=0 to xNodeList.length-1 do
    GetCreateElement(cXPConfigNode, xMaConfig).AppendChild(xNodeList[xNodeIndex]);

  //: ---------------------------------------------------------------------------
  // Maschinenteil laden
  xTempDOM := XMLStreamToDOM(aMachine);
  if assigned(xTempDOM) then begin
    // Sollte vorhanden sein
    xConfigNode := xMaConfig.selectSingleNode(cXPConfigNode);
    if Assigned(xConfigNode) then
      // Maschinenteil an die Sektionen anh�ngen
      xConfigNode.appendChild(xTempDOM.selectSingleNode(cXPMachineNode))
    else
      raise Exception.Create('TLXMachThread.BuildMaConfig: No Groups in MaConfig');

    // Maschinenelemente die im MM aber als Sektionselemente gehandhabt werden
    AppendMaNodeToSections(cXPBlockAtAlarmItem);
    AppendMaNodeToSections(cXPBlockAtCutFailureItem);
  end;// if assigned(xTempDOM) then begin

  //: ----------------------------------------------------------------------------
  (* Defaults in das MaConfig XML einf�gen.
     Dazu muss das Defaults XML zuerst ver�ndert werden. *)
  xTempDOM := XMLStreamToDOM(aDefaults);
  if assigned(xTempDOM) then begin
    if assigned(xTempDOM.DocumentElement) then begin
      xNewDefaultElement := xMaConfig.CreateElement(cXMLDefaultsElementName);
      while xTempDOM.DocumentElement.childNodes.Length > 0 do
        xNewDefaultElement.appendChild(xTempDOM.DocumentElement.childNodes[0]);
      xMaConfig.DocumentElement.appendChild(xNewDefaultElement);
    end;// if assigned(xTempDOM.DocumentElement) then begin
  end;// if assigned(xTempDOM) then begin

  // Gruppen sollten noch sortiert werden
  with TXMLMaConfigHelper.Create do try
    DOM := xMaConfig;

    // Daten aus Settings und MaConfig Kombinieren (leider ist die YM Abteilung in diesem St�ck zu unflexibel)
    for xMMGroup:=0 to GroupCount-1 do begin
      // F�r jede Gruppe die Settings holen. Das Dyn Array mLXSectionArr[] wird ausserhalb aufgef�llt und
      // enth�lt f�r die Anzahl der Gruppen den Index f�r die g�ltigen Sektionen
      // -> nicht gestossene Gruppen auf der LZE
      if UploadAndConvertXMLSettings(xTempSettigns, mLXSectionArr[xMMGroup], True) then begin
        if xTempDOM.loadXML(xTempSettigns) then begin
          // Unter diesem Element sind alle ConfigCodes abgelegt
          xSettingsConfigCode := xTempDOM.selectSingleNode(cXPGroupConfigCodeNode);
          if Assigned(xSettingsConfigCode) then begin
            // Selektiert alle leeren ConfigCodes
            xNodeList := GroupNode[xMMGroup, cXPGroupConfigCodeNode].SelectNodes('.//*[count(node()) = 0]');
            for xNodeIndex := xNodeList.length-1 downto 0 do begin
              xSelectionPath := RemoveXPRoot(cXPGroupConfigCodeNode, GetSelectionPath(xNodeList[xNodeIndex]));
              xConfigNode := xSettingsConfigCode.selectSingleNode(xSelectionPath);
              if Assigned(xConfigNode) then
                xNodeList[xNodeIndex].text := xConfigNode.Text
              else
                xNodeList[xNodeIndex].parentNode.removeChild(xNodeList[xNodeIndex]);
            end;// for xNodeIndex := xNodeList.length - 1 downto 0 do begin
          end;// if Assigned(xSettingsConfigCode) then begin
        end;// if xTempDOM.loadXML(xTempSettigns) then begin
      end;// if UploadAndConvertXMLSettings(xTempSettigns, i, false) then begin
    end;// for xMMGroup := 0 to GroupCount - 1 do begin

    // Jetzt erst die Gruppen zusammenfassen und aufsteigend sortieren
    ConcentrateGroups;
  finally
    Free;
  end;// with TXMLMaConfigHelper.Create do try

  Result := xMaConfig.xml;
  if mDebugEnabled then
    CodeSite.SendString('BuildMaConfig XML', FormatXML(xMaConfig));
  xMaConfig := nil;
end;// function TLXMachThread.BuildMaConfig(aSections, aMachine, aDefaults: string):

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
var
  xConv: TMMXMLConverter;
  //............................................................................
  // Local Procedure
  procedure WriteConfigCodeToBin(aSelPath: string);
  var
    xValue: integer;
    xNode: IXMLDOMNode;
  begin
    xNode := aMMXMLDoc.SelectSingleNode(aSelPath);
    if assigned(xNode) then begin
      xValue := GetElementValueDef(xNode, -1);
      if xValue >= 0 then begin
        xConv.WriteElementToBinWithoutCheck(aSelPath, xValue);
CodeSite.SendStringEx(csmGreen, 'LX-WriteElementToBinWithoutCheck:', Format('Path:%s Value:%d',[aSelPath, xValue]));  //////////////////////////////////////////NueTmp
      end;
    end;// if assigned(xNode) then begin
  end;

// Begin TLXMachThread.XML2BinAfterConvert......................................
begin
  if Sender is TMMXMLConverter then begin
    xConv := TMMXMLConverter(Sender);
    if assigned(aMMXMLDoc) then begin
CodeSite.SendMsg('LX-XML2BinAfterConvert' + FormatXML(aMMXMLDoc.xml));   //////////////////////////////////////////NueTmp
      WriteConfigCodeToBin(cXPConfigCodeAItem);
      WriteConfigCodeToBin(cXPConfigCodeBItem);
      WriteConfigCodeToBin(cXPConfigCodeCItem);
      WriteConfigCodeToBin(cXPConfigCodeDItem);
    end;// if assigned(aMMXMLDoc) then begin
  end;// if Sender is TMMXMLConverter then begin
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xNode: IXMLDOMNode;
  xDFS1: IXMLDOMNode;
  xDFS2: IXMLDOMNode;
//  xSFS : IXMLDOMNode;
begin
  xNode := aMMXMLDoc.selectSingleNode(cXPFP_ZenitItem);
  if Assigned(xNode) then
    mIsZenit := GetElementValueDef(xNode, false)
  else
    mIsZenit := False;

  (* Leider ein kleiner Sch�nheitsfehler im MMSchema.
     DFS Sensitivity kommt urspr�nglich von zwei einzelnen ConfigCode Bits. Diese beiden Bits
     ergeben zusammen den entsprechenden Wert. In der LX-Zentrale kommmt aber nur ein Wert an.
     Dieser Wert muss nun von den beiden Elementen in enies verschoben werden.

     Tabelle:
       00    = 1x
       10/01 = 2x
       11    = 4x
  *)
  xDFS1 := aMMXMLDoc.selectSingleNode(cXPDFSSensitivity1Item);
  xDFS2 := aMMXMLDoc.selectSingleNode(cXPDFSSensitivity2Item);
  if ElementToBoolDef(xDFS1, false) then begin
    if ElementToBoolDef(xDFS2, false) then
      SetElementValue(xDFS1, 4)
    else
      SetElementValue(xDFS1, 2);
  end else begin
    if ElementToBoolDef(xDFS2, false) then
      SetElementValue(xDFS1, 2)
    else
      SetElementValue(xDFS1, 0);
  end;// if ElementToBoolDef(xDFS1, false) then begin

{  xSFS := aMMXMLDoc.selectSingleNode(cXPSFSSensitivityItem);
  if ElementToBoolDef(xSFS, false) then
    SetElementValue(xSFS, 2)
  else
    SetElementValue(xSFS, 1);}
end;

//:-----------------------------------------------------------------------------
// V 5.x
procedure TLXMachThread.XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
begin
  // Von Spectra <> Zenit Abh�ngie Elemente behandeln
  //-------------------------------------------------
  // Cfg A4
  if AnsiSameText(aEventRec.CalcElementName, cXPZeroTestItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPPClearingDuringSpliceItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg A10
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtFAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg A14
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtClusterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtAlarmItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg A15
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtYarnCountAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSuckOffLastCutOnlyItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg B4
  if AnsiSameText(aEventRec.CalcElementName, cXPFAdjustAfterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg B5
  if AnsiSameText(aEventRec.CalcElementName, cXPCutBeforeAdjustItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C0
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtSFIAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C1
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtFClusterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C5
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtShortCountAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Partiespezifische Parameter behandeln
  //--------------------------------------
  if AnsiSameText(aEventRec.CalcElementName, cXPAssignModeItem) then
    aEventRec.Value := mTempXMLSettings.AssignMode
  else
{  if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
    aEventRec.Value := mTempXMLSettings.Group  // Informator verlangt die Gruppe 0-basiert - antwortet aber 1-basiert - (MM 0-basiert)
  else}
  if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
    aEventRec.Value := mTempXMLSettings.ProdGrpID
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
    aEventRec.Value := mTempXMLSettings.PilotSpindles
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPThreadCountItem) then
    aEventRec.Value := mTempXMLSettings.NrOfThreads
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
    aEventRec.Value := mTempXMLSettings.SpindleFirst
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
    aEventRec.Value := mTempXMLSettings.SpindleLast
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
    aEventRec.Value := mTempXMLSettings.SpeedRamp
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
    aEventRec.Value := mTempXMLSettings.Speed
  else
// --------------------------------- Konvertierungs Logik -------------------------------------
    // In der LX ist der Referenz Typ ein eigenes Element (0 = float, 1 = constant)
    if AnsiSameText(aEventRec.CalcElementName, cXPRefTypeItem) then begin
      // Default auf Constant
      aEventRec.Value := 0;
      if aEventRec.ArgumentList.ValueDef(cXPReferenceItem, 0) = 0 then
        // Wenn der Wert 0 ist, dann Typ = float
        aEventRec.Value := 1;
    end;// if AnsiSameText(aEventRec.CalcElementName, cXPRefTypeItem) then begin
end;

//:-------------------------------------------------------------------
procedure TLXMachThread.CleanDataAcqQueue(aConfirmMsg: Boolean);

  { Die DataAcquisation Transaktion wird abgebrochen.
    Alle Jobs in der Jobqueue des betreffenden Threads werden geloescht.
    Der Timer fuer die DataAcquisation wird gestoppt.}
var
  xPJob: PJobRec;
  xJob: TJobRec;
begin
  try
    with mLxDevHandler do
      while mJobAcqQueue.Count > 0 do begin
        xPJob := mJobAcqQueue.Pop;
        //TODO: wss: bei Fehler -> Fehlermeldung mittels Fehlerjob an System mitteilen statt auf Timeout im MsgHandler warten
        if aConfirmMsg then begin
          xJob.JobTyp           := jtJobResult;
          xJob.JobID            := xPJob^.JobID;
          xJob.JobResult.JobTyp := xPJob^.JobTyp;
          xJob.JobResult.Error  := SetError(ERROR_NOT_READY, etMMError, 'CleanDataAcqQueue: GetZEData timeout');
          mWriteToMsgHnd(@xJob);
        end;
        // diesen Job freigeben
        FreeMem(xPJob, xPJob.JobLen);
      end;
  except
    on E: Exception do begin
      raise LXAccessException.Create('CleanDataAcqQueue ' + e.message);
    end;
  end;
end;// TLXMachThread.CleanDataAcqQueue cat:No category

//:-----------------------------------------------------------------------------
// V 5.x
function TLXMachThread.ConnectAndInitMachine: Boolean;
{ Diese Methode wird fuer den Verbindungsaufbau aufgerufen.
  Dabei wird die Jobid auf allen MachineGruppen auf 0 gesetzt. Die NodeListe wird aktualisiert
  und der Timer fuer den Lifecheck gestartet. Zusaetzlich wird DataTransaction abgebrochen, Rollback}
var
  xMMGroup:Integer;
  xJobIDInitvalue: DWord;
  xMM_data_trans: TLxDataTransactionRec;
//  xBuffer: Array[0..cMapBufferSize-1] of Byte;
//  xMapID: String;
  xMapFile: string;
  //............................................................................
  // Local Function
  function HasMapfile: Boolean;
  begin
    try
      Result := mLxDevHandler.MapfileExists;
    except
      Result := False;
    end;
  end;

  //............................................................................
  function IsNewerMachineMapfile(aSearchStr: string; aMachineMap: string; aOverruleMap: string): Boolean;
  //Detect newer Mapfile
  var
    xStartNumber: Integer;
    xMaStr, xOvStr: string;
  begin
    Result := False;
    try
      xStartNumber := StrFind(aSearchStr, aMachineMap, 1)+Length(aSearchStr);
      xMaStr := StrMid(aMachineMap, xStartNumber,  4);
      xStartNumber := StrFind(aSearchStr, aOverruleMap, 1)+Length(aSearchStr);
      xOvStr := StrMid(aOverruleMap, xStartNumber,  4);
      CodeSite.SendFmtMsg('SetNewerMapfile: MachineMap:%s, OverruleMap:%s',[xMaStr, xOvStr]);
      if StrtoInt(xMaStr) > StrtoInt(xOvStr) then
        Result := True;
    except
      mWriteToLog(etWarning, Format('Error in IsNewerMachineMapfile comparing Mapfileversion (Machine and Overrule). Set Map to %s%s', [aSearchStr, xOvStr]));
    end;

  end;

  //...........................................................
  procedure GetAndHandleMachMapfile(aOverruleCase: Boolean);
  begin
    try      // hole zuerst die MapID
      if mLxDevHandler.MapName <> '' then begin
        CodeSite.SendFmtMsg('Machine:%d, MapID: %s',[mMachID, mLxDevHandler.MapName]);
        // wenn f�r diese MapID noch kein Eintrag in der Mapliste ist, dann die Mapdatei auslesen
        if not mNodeList.MapfileAvailable[mLxDevHandler.MapName] then begin
         //Mapfile von LX Laden
          xMapFile := mLxDevHandler.GetMapfile;
          if xMapFile > '' then begin
            if not aOverrulecase then
              mNodeList.Mapfile[mLxDevHandler.MapName] := xMapfile;
          end
          else
            mWriteToLog(etError, Format('ConnectAndInitMachine: GetMapfile failed! Mapname: %s', [mLxDevHandler.MapName]));
        end;
        if (Not aOverrulecase) or (IsNewerMachineMapfile('LZE-', mLxDevHandler.MapName, mNodeList.MapID[mMachID])) then
          mNodeList.MapID[mMachID] := mLxDevHandler.MapName;
      end else
        mWriteToLog(etWarning, ('ConnectAndInitMachine: Mapfilename empty!'));
    except
      //wss: Exception wurde bereits abgefangen, jedoch noch nicht protokolliert
      on e: Exception do
        mWriteToLog(etError, ('ConnectAndInitMachine failed: ' + e.Message));
    end;
  end;
  //...........................................................

// Begin TLXMachThread.ConnectandInitMachine....................................
begin
  result := false;
  try
    if mLxDevHandler.Connect then begin
      result := true;
      // Jobid auf allen MachineGruppen auf 0 setzen
      xJobIDInitvalue:=0;
      with mLxDevHandler do begin
        for xMMGroup := 0 to cLXSpdGroupLimit - 1 do begin
//          PutData(cGRP_JOBID, @xJobIDInitvalue, Sizeof(xJobIDInitvalue), xMMGroup + 1);
          mSetSettingsJobIDs[xMMGroup] := xJobIDInitvalue;
        end;

        mTransactPending:= FALSE;
        // Pruefe ob DataTransaction laeuft
        GetData(cDATA_TRANSACT, @xMM_data_trans, sizeof(TLxDataTransactionRec), 0);
        if xMM_data_trans.access <> tsMM_DATA_IDLE then begin
        //TODO: wss: gibt es ein Kommando, welches die LZE wieder in den Idle versetzt ohne dass ein Delete simuliert wird?
        // DataTransaction abbrechen ?
        //  xMM_data_trans.access := WORD(dsMM_DATA_ACK);
        //  PutData(cDATA_TRANSACT, @xMM_data_trans, sizeof(TMM_data_transRec), 0);
        end;
      end; // with mmLxDevHandler

      //Handling Mapfile
//      FillChar(xBuffer, cMapBufferSize, 0);
      if not mNodeList.IsMapOverruled[mMachID] then begin
        if HasMapfile then begin
          GetAndHandleMachMapfile(False);
        end else begin
          raise  Exception.Create('ConnectAndInitMachine: Mapfile from LZE not available');
//          f�r WSC: mNodeList.GetMapfileFromVersion(mMachID, xVersionInt);
        end;
      end
      else begin  //Overrule case Nue:29.11.06
        if HasMapfile and mNodeList.CheckNewerMapfile[mMachID] then begin
          // hole zuerst die MapID
          GetAndHandleMachMapfile(True);
        end;
      end;
      // Update NodeList, Maschine bereit f�r Kommunikation
      mNodeList.SetMachstate(Nil, mMachID, nsOnline);
      RefreshMachstatusTicker;
    end else
    // Update NodeList, Maschine nicht bereit
      mNodeList.SetMachstate(Nil, mMachID, nsOffline);

    CodeSite.SendFmtMsg('Machine:%d, Activated MapID: %s ', [mMachID, mNodeList.MapID[mMachID]]);
  except
    on E: Exception do
      raise Exception.Create('ConnectAndInitMachine: ' + e.message);
  end;
  // Start des Timers f�r Lifecheck
  RefreshLifeCheckTimer;
end;

//:-----------------------------------------------------------------------------
function TLXMachThread.DisConnectMachine: Boolean;
{ Diese Methode wird fuer den Verbindungsabbau aufgerufen.
  Die NodeListe wird aktualisiert und der Timer DataAcquisation wird gestoppt.}

begin
  result := false;
  // Stop des Timers f�r Datenaquisation
  KillTimer(mThreadWndHandle, cAcqTimerID);
  KillTimer(mThreadWndHandle, cEntryLockedTimerID);
  mTransactPending:= FALSE;
  //wss: Pendente Jobs in Queue noch l�schen
  CleanDataAcqQueue(False);
  try
    if mLxDevHandler.mConnected then begin
      result:= mLxDevHandler.Disconnect;
      RefreshMachstatusTicker
    end;
    // Update NodeList
    mNodeList.SetMachstate(Nil, mMachID, nsOffline);
  except
    on E: Exception do
      mWriteToLog(etError, e.message + mLxDevHandler.mIpAdresse);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TLXMachThread.GetLotandStyleParam(aMMGroup: Integer; var aMaAssign: TMaAssign);
var
  xSchlupf: Integer;
  xLxLotStyleRec: TLxLotStyleRec;
begin
  with mLxDevHandler, aMaAssign do
  try
    GetData(cGRP_LOT_STYLE_PARAM, @xLxLotStyleRec, Sizeof(TLxLotStyleRec), aMMGroup + 1);    //Nue:14.7.08 Korrektur!!
    with Groups[aMMGroup], ProdGrpParam.ProdGrpInfo do begin
      UniCodetoCharArray(@xLxLotStyleRec.PartieName,length(xLxLotStyleRec.PartieName), c_prod_name);
CodeSite.SendFmtMsg('GetLotandStyleParam: MMGrpNo=%d ProdName:%s', [aMMGroup, string(c_prod_name)]);
      Color               := xLxLotStyleRec.Color;

      c_slip := cDefaultSlip;
      xSchlupf := round(xLxLotStyleRec.Slip); // Wird bereits mit Faktor 1000 abgef�llt
      if (xSchlupf > (cMinSlip * 1000)) and (xSchlupf < (cMaxSlip * 1000)) then   //Nue
        c_Slip := xSchlupf;

//      c_slip              := Trunc(xLxLotStyleRec.Slip); // Wird bereits mit Faktor 1000 abgef�llt
      c_style_id          := xLxLotStyleRec.StyleID;
      c_order_id          := cDefaultOrderID;         // wird vo LZE nicht unterst�tzt
      c_order_position_id := cDefaultOrderPositionID; // wird vo LZE nicht unterst�tzt
    end;
  except
    with Groups[aMMGroup], ProdGrpParam.ProdGrpInfo do begin
     { TODO -okhp : Default Partienam definieren
      ProdName           := ; }
      c_style_id          := 1;
      c_order_id          := cDefaultOrderID;         // wird vo LZE nicht unterst�tzt
      c_order_position_id := cDefaultOrderPositionID; // wird vo LZE nicht unterst�tzt
      c_slip              := cDefaultSlip;            // wird vo LZE nicht unterst�tzt
    end;
    CodeSite.SendFmtError('GetLotandStyleParam, access to GRP_LOT_STYLE_PARAM data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TLXMachThread.GetLotandStyleParam(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec);
var
  xSchlupf: Integer;
  xLxLotStyleRec: TLxLotStyleRec;
begin
  with mLxDevHandler, aSettingsRec do begin
    try
      GetData(cGRP_LOT_STYLE_PARAM, @xLxLotStyleRec, Sizeof(TLxLotStyleRec), aMMGroup + 1);
      UniCodetoCharArray(@xLxLotStyleRec.PartieName,length(xLxLotStyleRec.PartieName), ProdName);
      YarnCnt := 1;

      YarnCntUnit     := yuNm;  // Immer Nm
      NrOfThreads     := xLxLotStyleRec.NrOfThreads;
//      // YarnCount in Nm umrechnen
      if xLxLotStyleRec.YarnCnt > 0 then begin
//        xTempYarnUnit := yuNone;
//        case xLxLotStyleRec.YarnCntUnit of
//          lxyuNm  : xTempYarnUnit := yuNm;
//          lxyuNe  : xTempYarnUnit := yuNeB;
//          lxyuTex : xTempYarnUnit := yutex;
//          lxyuDen : xTempYarnUnit := yuNone;
//          lxyuNew : xTempYarnUnit := yuNeW;
//        end;// case xLxLotStyleRec.YarnCntUnit of
//
//        // Ganzer Faden runterladen
//        YarnCnt := YarnCountConvert(xTempYarnUnit, yuNm, xLxLotStyleRec.YarnCnt);
        YarnCnt := xLxLotStyleRec.YarnCnt;
      end;// if xLxLotStyleRec.YarnCnt > 0 then

      Slip := cDefaultSlip;
      xSchlupf := round(xLxLotStyleRec.Slip); // Wird bereits mit Faktor 1000 abgef�llt
      if (xSchlupf > (cMinSlip * 1000)) and (xSchlupf < (cMaxSlip * 1000)) then   //Nue
        Slip := xSchlupf;

//      Slip            := Trunc(xLxLotStyleRec.Slip); // Wird bereits mit Faktor 100 abgef�llt
      Color           := xLxLotStyleRec.Color;
      StyleID         := xLxLotStyleRec.StyleID;
      OrderID         := cDefaultOrderID;         // wird von LZE nicht unterst�tzt
      OrderPositionID := cDefaultOrderPositionID; // wird von LZE nicht unterst�tzt
    except
      // leerer Partiename
      FillChar(ProdName, SizeOf(ProdName), 0);
      YarnCnt         := 1;
      YarnCntUnit     := yuNm;
      NrOfThreads     := 1;
      StyleID         := 1;
      OrderID         := cDefaultOrderID;         // wird von LZE nicht unterst�tzt
      OrderPositionID := cDefaultOrderPositionID; // wird von LZE nicht unterst�tzt
      Slip            := cDefaultSlip;            // wird von LZE nicht unterst�tzt
      CodeSite.SendFmtError('GetLotandStyleParam, access to GRP_LOT_STYLE_PARAM data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
  end
end;

//:-----------------------------------------------------------------------------
procedure TLXMachThread.GetGrpAssignment(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec);
var
  xProdGroupId: TProdGroupId;
  xGrpAssignment: TLxGrpAssignmentRec;
begin
  with mLxDevHandler, aSettingsRec do begin
    try
      // GRP_ASSIGNMENT DataItem-Record lesen und in SettingsRec konvertieren und abspeichern
      Fillchar(xGrpAssignment,sizeof(xGrpAssignment),0);
      GetData(cGRP_ASSIGNMENT,@xGrpAssignment,Sizeof(TLxGrpAssignmentRec), aMMGroup + 1);

//{ TODO -oNUE -cLX-Memories : ACHTUNG nur tempor�r f�r Tests bis anfang November!! Weil bei Lab noch nicht implementiert!! Nue }
//if aMMGroup>=11 then
//  xGrpAssignment.GroupState:= pgsTemplate;


      // GroupState der Gruppe umkopieren
      Case xGrpAssignment.GroupState of
        pgsDefined:  GroupState:= gsDefined;
        pgsInProduction: GroupState:= gsInProd;
        pgsFree:  GroupState:= gsfree;
        pgsTemplate:  GroupState:= gsMemory;
      else
        CodeSite.SendFmtError('GetGrpAssignment, unknown GroupState: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
      end;
      // Spindelbereich der Gruppe umkopieren
      SpindleFirst := xGrpAssignment.SpindleFirst;
      SpindleLast  := xGrpAssignment.SpindleLAst;
    except
       CodeSite.SendFmtError('GetGrpAssignment, access to GRP_ASSIGNMENT data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
    // ProdgroupID DataItem der Gruppe lesen und in SettingsRec speichern
    try
      Fillchar(xProdGroupId,sizeof(TProdGroupId),0);
      GetData(cGRP_PRODID, @xProdGroupId, Sizeof(TProdGroupId), aMMGroup + 1);
      CodeSite.SendFmtMsg('MMMachID = %d, MMGroup = %d, ProdID = %d, GroupState = %d', [mMachID, aMMGroup, xProdGroupId.ProdGroupId, Ord(xGrpAssignment.GroupState)]);
      ProdGrpID:= xProdGroupId.ProdGroupId;
    except
      CodeSite.SendFmtError('GetGrpAssignment, access to GRP_PRODID data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TLXMachThread.GetGrpAssignment(aMMGroup: Integer; var aMaAssign: TMaAssign);
var
  xProdGroupId: TProdGroupId;
  xModifyFlags: TModifySet;
  xGrpAssignment: TLxGrpAssignmentRec;
  xMaAssignment: TLxMaAssignmentRec;

  (*---------------------------------------------------------
    Modify Flags in Codesite schreiben
  ----------------------------------------------------------*)
  procedure ModifyFlagsToCodeSite(xFlags: TModifySet);
  var
    xModifyFlagsString: string;
    i: TModify;
  begin
    xModifyFlagsString := '';
    for i := Low(TModify) to High(TModify) do begin
      if i in xFlags then
        xModifyFlagsString := ', ' + xModifyFlagsString + GetEnumName(TypeInfo(TModify), Ord(i))
    end;// for i := Low(TModify) to High(TModify) do begin
    if Length(xModifyFlagsString) > 2 then
      System.Delete(xModifyFlagsString, 1, 2);
    if xModifyFlagsString = '' then
      xModifyFlagsString := 'no Flag is set';
    CodeSite.SendMsgEx(csmCheckPoint, Format('Modified for MMGroup %d = %s', [aMMGroup, xModifyFlagsString]));
  end;

begin
  with mLxDevHandler, aMaAssign do begin
    try
      // GRP_ASSIGNMENT DataItem-Record lesen und in MM-MaAssign GroupRecord konvertieren und abspeichern
      Fillchar(xGrpAssignment,sizeof(xGrpAssignment),0);
      GetData(cGRP_ASSIGNMENT,@xGrpAssignment,Sizeof(TLxGrpAssignmentRec), aMMGroup + 1);
      // GroupEvent der Gruppe umkopieren
      Case xGrpAssignment.GroupEvent of
        geRange: Event:= deRange;
        geSettings: Event:= deSettings;
      else
        if xGrpAssignment.GroupEvent <= High(TLxGroupEvent) then
          CodeSite.SendFmtNote('GetGrpAssignment, GroupEvent (Event %s): IpAdr=%s, MaschGrp=%d', [GetEnumName(TypeInfo(TLxGroupEvent), Ord(xGrpAssignment.GroupEvent)), mIpAdresse, aMMGroup])
        else
          CodeSite.SendFmtError('GetGrpAssignment, unknown GroupEvent: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
      end;
      // GroupState der Gruppe umkopieren

//{ TODO -oNUE -cLX-Memories : ACHTUNG nur tempor�r f�r Tests bis anfang November!! Weil bei Lab noch nicht implementiert!! Nue }
//if aMMGroup>=11 then
//  xGrpAssignment.GroupState:= pgsTemplate;




      Case xGrpAssignment.GroupState of
        pgsDefined:  Groups[aMMGroup].GroupState:= gsDefined;
        pgsInProduction: Groups[aMMGroup].GroupState:= gsInProd;
        pgsFree:  Groups[aMMGroup].GroupState:= gsfree;
        pgsTemplate:  Groups[aMMGroup].GroupState:= gsMemory;
      else
        if xGrpAssignment.GroupState <= High(TLxProdGroupState) then
          CodeSite.SendFmtNote('GetGrpAssignment,  GroupState (State %s): IpAdr=%s, MaschGrp=%d', [GetEnumName(TypeInfo(TLxProdGroupState), Ord(xGrpAssignment.GroupState)), mIpAdresse, aMMGroup])
        else
          CodeSite.SendFmtError('GetGrpAssignment, unknown GroupState: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
      end;
      // Spindelbereich der Gruppe umkopieren
      Groups[aMMGroup].SpindleFirst := xGrpAssignment.SpindleFirst;
      Groups[aMMGroup].SpindleLast  := xGrpAssignment.SpindleLAst;
    except
       CodeSite.SendFmtError('GetGrpAssignment, access to GRP_ASSIGNMENT data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
    { TODO -okhp : Machstate wird laut Nue nicht ben�tigt, �berpr�fen }
    // MachState kopieren
     Fillchar(xMaAssignment,sizeof(xMaAssignment),0);
     GetData(cMA_ASSIGNMENT,@xMaAssignment,Sizeof(TLxMaAssignmentRec),0);
     MachState:=[];
     if msKeylocked in xMaAssignment.MachState then MachState:= MachState+[0];
     if msInitialReset in xMaAssignment.MachState then MachState:= MachState+[1];

     // Events f�r den MaAssign Record abf�llen
     case xMaAssignment.MaEvent of
       meNone          : Event := deNone;
       meInitialReset  : Event := deInitialReset;
       meReset         : Event := deReset;
       meEntryLocked   : Event := deEntryLocked;
       meEntryUnlocked : Event := deEntryUnlocked;
     end;// case xMaAssignment.MaEvent of
     {$IFDEF DEBUG_INITIALRESET}
      if xMaAssignment.maEvent = meReset then
        Event := deInitialReset;
     {$ENDIF}
    //
    // Modified Flags DataItem der Gruppe lesen und in MM-MaAssign GroupRecord speichern
    try
     GetData(cGRP_MODIFIED, @xModifyFlags, Sizeof(TModifySet), aMMGroup + 1);
     Groups[aMMGroup].Modified:= BITSET16(xModifyFlags);
     // In Codesite ausgeebn
     ModifyFlagsToCodeSite(xModifyFlags);
    except
      CodeSite.SendFmtError('GetGrpAssignment, access to GRP_MODIFIED data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
    //
    // ProdgroupID DataItem der Gruppe lesen und in MM-MaAssign GroupRecord speichern
    try
      Fillchar(xProdGroupId,sizeof(TProdGroupId),0);
      GetData(cGRP_PRODID, @xProdGroupId, Sizeof(TProdGroupId), aMMGroup + 1);
      Groups[aMMGroup].ProdId:= xProdGroupId.ProdGroupId;
    except
      CodeSite.SendFmtError('GetGrpAssignment, access to GRP_PRODID data item failed: IpAdr=%s, MaschGrp=%d', [mIpAdresse, aMMGroup]);
    end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TLXMachThread.DeleteLxDecl(aMMGroup: Integer);
var
 xmodified:  TModifySet;
begin
  with mLxDevHandler do begin
    // Declaration Event und Modify Flag der Gruppe zuruecksetzen
    xmodified:=[];
    PutData(cGRP_MODIFIED, @xmodified, Sizeof(TModifySet), aMMGroup + 1);
    CodeSite.SendFmtMsg('MM_RESET_DECL: MaschGrp=%d', [aMMGroup]);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TLXMachThread.GetYMSetParam(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec);
var
  xYMSet: TLx_YmSet;
begin
  with mLxDevHandler, aSettingsRec do begin
    try
      FillChar(xYMSet, sizeof(xYMSet), 0);
      // YMSET_PARAM DataItem der Gruppe lesen und in MM-YMSets speichern
      GetData(cGRP_YMSET_PARAM, @xYMSet, Sizeof(TLx_YmSet), aMMGroup + 1);
      // Unicode zu MBCS kovertieren und MM YMSet Name zuordnen
      UniCodetoCharArray(xYMSet.YM_Name, length(xYMSet.YM_Name), YMSetName);
      // LX YMSET Parameter MM YMSet zuordenen
      YMSetID:= xYMSet.YM_ID;
      YMSetChanged:= xYMSet.YM_Changed <> 0;
    except
      on E: Exception do
        CodeSite.SendWarning('GetYMSetParmeter ' + e.message)
    else
      CodeSite.SendError('GetYMSetParmeter failt with unknown error');
  end;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TLXMachThread.Execute;
var
  xJob: TJobRec;
  xCoInitializeResult: HResult;
  xMsg: TMsg;
//  xInformThreadMsg: Boolean;

  // Informs von der LX-ZE und Jobs vom JobHandler werden pro Maschine verarbeitet

  //............................................................................
  // Erneuert Verbindungsaufbau zur LX.DLL (nach Ablauf des Lifechecks oder Auftreten einer Exception)
  // Local Procedure
  procedure ReConnectMachine;
  var
    xJob: TJobRec;
    xOldMachstate, xCurrentMachstate: TNodeState;
  begin
    CodeSite.SendMsg('Reconnect LXHandler --> ' + mLxDevHandler.mIpAdresse);
    // Verbindungsabbau und neuer Verbindungsaufbau zur LZE herstellen
    xOldMachstate:= mNodeList.GetMachstate(Nil, mMachID);
    DisConnectMachine;
    ConnectandInitMachine;
    xCurrentMachstate:= mNodeList.GetMachstate(Nil, mMachID);
    if xCurrentMachstate <> xOldmachstate then begin
      if xCurrentMachstate = nsOnline       then xJob.JobTyp:= jtMaOnline
      else if xCurrentMachstate = nsOffline then xJob.JobTyp:= jtMaOffline;
      xJob.Maoffline.MachineID := mMachID;
      mWriteToMsgHnd(@xJob);      // Aenderung des Maschinenstatus an MsgHandler schicken
    end;
  end;

  //............................................................................
  // Erster Verbindungsaufbau nach MM Start zur LX.DLL
  // Local Procedure
  procedure ConnectMachine;
  var
    xJob: TJobRec;
    xCurrentMachstate: TNodeState;
  begin
    CodeSite.SendMsg('Connecting LXHandler --> ' + mLxDevHandler.mIpAdresse);
    // Neuer Verbindungsaufbau zur LZE herstellen
    ConnectandInitMachine;
    xCurrentMachstate:= mNodeList.GetMachstate(Nil, mMachID);
    if xCurrentMachstate <> nsNotValid then begin
      if xCurrentMachstate = nsOnline then
        xJob.JobTyp:= jtMaOnline
      else
        xJob.JobTyp:= jtMaOffline;
      xJob.Maoffline.MachineID := mMachID;
      mWriteToMsgHnd(@xJob);            // Maschinenstatus an MsgHandler schicken
    end else
      mWriteToLog(etWarning, Format('NotValid Machstae %s', [mLxDevHandler.mIpAdresse]));
    end;

// Begin TLXMachThread.Execute .................................................
begin
  xCoInitializeResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xCoInitializeResult <> S_OK) and (xCoInitializeResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    FreeOnTerminate  := True;
    mJobRequestMsg   := RegisterWindowMessage(cMMJobRequestMsg);
    mLxInformMsg     := RegisterWindowMessage(mThreadIpAdresse); // Pro Thread unique MsgID loesen
    mThreadWndHandle := AllocateHWnd(nil);                       // Pro Thread Window mit MsgQueue erzeugen
    codesite.sendinteger('MessageID', mLxInformMsg);
    codesite.sendinteger('WindowHandle', mThreadWndHandle);
    if not assigned(mLxDevHandler) then begin
      mLxDevHandler := TLxDevHandler.create(mThreadWndHandle, mLxInformMsg, mThreadIpAdresse, mMachID);
      ConnectMachine;
      RefreshLifeCheckTimer;         // Live check der Maschine
      RefreshMachstatusTicker;       // Machstatus alle Minuten an MM system
      //SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil); // Machstatus alle Minuten an MM system
      with mLxDevHandler do begin
        //.. Begin Message Loop.................................................
        while GetMessage(xMsg, mThreadWndHandle, 0, 0) do begin
          Sleep(1);
          try
            with xMsg do begin
              // Kontrolle ob MsgID und Informsocket richtig
              if Message = mLxInformMsg then begin
           //   if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
                LXMsgDispatcher(xMsg); // Inform von der LX-ZE
              end
              else if Message = mJobRequestMsg then begin
                MMJobDispatcher(xMsg);  // Job vom JobHandler
              end
              else if Message = WM_Timer then begin   // Timer-> Transaktionstimer, MachstatusTicker, Lifecheck
                case wParam of
                  // Timeout bei der Datensammlung
                  cAcqTimerID: begin
                      KillTimer(mThreadWndHandle, cAcqTimerID);
                      mTransactPending:= FALSE;
                      mWriteToLog(etWarning, Format('Timeout %ds for dataready signal, rollback LZE data ', [cAcqTimeout div 1000]) + mLxDevHandler.mIpAdresse);

                      // Infos von einer laufenden Transaktion holen
//                      GetData(Getindex(cDATA_TRANSACT, xBufferSize), @xMM_data_trans, Sizeof(TLxDataTransactionRec), 0);
                      // Im Timeoutfall von GetZESpdData und DelZESpdData ist die entsprechende JobID in dieser Variable gespeichert
                      if mLastTransactNo <> 0 then begin
                        xJob.JobTyp           := jtJobResult;
                        xJob.JobID            := mLastTransactNo;
                        xJob.JobResult.JobTyp := jtNone;
                        xJob.JobResult.Error  := SetError(ERROR_NOT_READY, etMMError, 'Timeout: ' + mLxDevHandler.mIpAdresse);
                        mWriteToMsgHnd(@xJob);
                      end;

                      CleanDataAcqQueue(True);    // Timeout der DataAcquisation Queue
                      CodeSite.SendStringEx(csmRed, Format('GetData Transaction Rollback (TransactNo: %d)', [mLastTransactNo]), Format('IpAdr:%s', [mIpAdresse]));
                    end;// cAcqTimerID:begin

                  // Machstatus senden
                  cMachstatusTickerID: SendMachinenstatus;

                  // Zyklisches abfragen der Verf�gbarkeit der Verbindung
                  cLifeCheckTimerID: begin
                                       if mLXDevHandler.LifeCheck then
                                          RefreshLifeCheckTimer
                                       else
                                          ReConnectMachine;
                                     end;// cLifeCheckTimerID: begin

                  // Timer, wenn kein EntryLocked kommt (zB. Vom Kunden deaktiviert)
                  cEntryLockedTimerID: begin
                    SendAssignComplete;
                  end;
                else
                  mWriteToLog(etWarning, Format('Unknown Timer ID (%d) --> %s', [wParam, mLxDevHandler.mIpAdresse]));
                end;  // case wParam
              end else
                // Alle andern Meldungen weiterleiten
                DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
            end; // with xMsg
//            with xMsg do begin
//              xInformThreadMsg := False;
//              // Kontrolle ob MsgID und Informsocket richtig
//              if Message = mLxInformMsg then begin
//           //   if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
//                xInformThreadMsg := True;
//                LXMsgDispatcher(xMsg); // Inform von der LX-ZE
//              end
//              else if Message = mJobRequestMsg then begin
//                xInformThreadMsg := True;
//                MMJobDispatcher(xMsg);  // Job vom JobHandler
//              end
//              else if Message = WM_Timer then begin   // Timer-> Transaktionstimer, MachstatusTicker, Lifecheck
//                xInformThreadMsg := True;
//                case wParam of
//                  // Timeout bei der Datensammlung
//                  cAcqTimerID: begin
//                      KillTimer(mThreadWndHandle, cAcqTimerID);
//                      mTransactPending:= FALSE;
//                      mWriteToLog(etWarning, Format('Timeout %ds for dataready signal, rollback LZE data ', [cAcqTimeout]) + mLxDevHandler.mIpAdresse);
//
//                      // Infos von einer laufenden Transaktion holen
////                      GetData(Getindex(cDATA_TRANSACT, xBufferSize), @xMM_data_trans, Sizeof(TLxDataTransactionRec), 0);
//                      // Im Timeoutfall von GetZESpdData und DelZESpdData ist die entsprechende JobID in dieser Variable gespeichert
//                      if mLastTransactNo <> 0 then begin
//                        xJob.JobTyp           := jtJobResult;
//                        xJob.JobID            := mLastTransactNo;
//                        xJob.JobResult.JobTyp := jtNone;
//                        xJob.JobResult.Error  := SetError(ERROR_NOT_READY, etMMError, 'Timeout: ' + mLxDevHandler.mIpAdresse);
//                        mWriteToMsgHnd(@xJob);
//                      end;
//
//                      CleanDataAcqQueue;    // Timeout der DataAcquisation Queue
//                      CodeSite.SendStringEx(csmRed, Format('GetData Transaction Rollback (TransactNo: %d)', [mLastTransactNo]), Format('IpAdr:%s', [mIpAdresse]));
//                    end;// cAcqTimerID:begin
//
//                  // Machstatus senden
//                  cMachstatusTickerID: SendMachinenstatus;
//
//                  // Zyklisches abfragen der Verf�gbarkeit der Verbindung
//                  cLifeCheckTimerID: begin
//                                       if mLXDevHandler.LifeCheck then
//                                          RefreshLifeCheckTimer
//                                       else
//                                          ReConnectMachine;
//                                     end;// cLifeCheckTimerID: begin
//                else
//                  mWriteToLog(etWarning, Format('Unknown Timer ID (%d) --> %s', [wParam, mLxDevHandler.mIpAdresse]));
//                end;  // case wParam
//              end;
//              if not xInformThreadMsg then
//                // Alle andern Meldungen weiterleiten
//                DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
//            end;
          except
            on E: LXAccessException do begin
              mWriteToLog(etWarning, e.message + mLxDevHandler.mIpAdresse);
              CodeSite.SendMsg(e.message + mLxDevHandler.mIpAdresse); // Protokollmeldung !!
            end;
            else begin
              mWriteToLog(etError, 'FatalError' + mLxDevHandler.mIpAdresse);
              CodeSite.SendMsg('Unknow Error');
            end;
            // Neuer Verbindungsaufbau zu LZE herstellen
            ReConnectMachine;
          end;
        end;
        //.. End Message Loop...................................................
      end; // Abruch des Loops durch senden von WM_QUIT mit PostMessage()
    end;
  finally
    CoUninitialize;
  end;
end;// TLXMachThread.Execute cat:No category

procedure TLXMachThread.EncodeLXVersionNr(aVersNr: IXMLDOMNOde);
var
  xVersNr: Integer;
  xVersText: string;
  xPVers: PByte;
  i: Integer;

begin
  (* Die Versionsnummer ist als 4 Byte Wert abgelegt. Die Bytes sind in der Reihenfolge
     1) Major
     2) Minor
     3) Release
     4) Build
  *)
  if assigned(aVersNr) then begin
    xVersNr := ElementToIntDef(aVersNr, 0);
    xPVers := @xVersNr;
    xVersText := '';
    for i := 0 to 3 do begin
      xVersText := '.' + IntToStr(xPVers^) + xVersText;
      Inc(xPVers);
    end;// for i := 0 to 3 do begin
    // Letzten Punkt l�schen
    if length(xVersText) > 0 then
      System.Delete(xVersText, 1, 1);

    // Versionsnummer zuweisen
    aVersNr.text := xVersText;
  end;// if assigned(aVersNr) then begin
end;// procedure TLXMachThread.EncodeLXVersionNrqr(aVersNr: IXMLDOMNOde);

{  //............................................................................
  // Local Function
  procedure DeleteLxDecl(aGrpNo: Integer);
  var
   xmodified:  TModifySet;
  begin
    with mLxDevHandler do begin
      //GetData(cGRP_MODIFIED, @xmodified, Sizeof(TModifySet), aGrpNo);
      //xmodified:=[];
      //PutData(cGRP_MODIFIED, @xmodified, Sizeof(TModifySet), aGrpNo);
      // Declaration Event und Modify Flag der Gruppe zuruecksetzen
    CodeSite.SendFmtMsg('MM_RESET_DECL: MaGrpNo=%d', [aGrpNo]);
    end;
  end;}
  //............................................................................
  // Local Function
procedure TLXMachThread.GetYMSetName(aMMGroup: Integer; var aMaAssign:
    TMaAssign);
var
  xYMSet: TLx_YmSet;    //War bis 25.06.08 fehlerbehaftet!!!! Nue
begin
  with mLxDevHandler do begin
    try
      FillChar(xYMSet, sizeof(xYMSet), 0);
       // YMSET_PARAM DataItem der Gruppe lesen
      GetData(cGRP_YMSET_PARAM, @xYMSet, Sizeof(TLx_YmSet), aMMGroup + 1);
      // YMSET_Name in MBCC konvertieren und in Assigns abspeichern
      UniCodetoCharArray(xYMSet.YM_Name, length(xYMSet.YM_Name), aMaAssign.Groups[aMMGroup].ProdGrpParam.ProdGrpInfo.c_YM_set_name);
    except
      on E: Exception do
        CodeSite.SendWarning('GetYMSetName ' + e.message)
    else
      CodeSite.SendError('GetYMSetName failt with unknown error');
    end;
  end; // with
end;

//:-----------------------------------------------------------------------------
procedure TLXMachThread.LxMsgDispatcher(aMsg: TMsg);
 { Diese Methode verarbeitet Informs welche von den DataItems DATA_TRANSFER,
    MM_DECLARATION, GRP_SETTINGS ausgeloest werden.
    Datadirection LX-ZE -> LxHandler }
  // Exception werden abgefangen und weitergereicht
var
  xInfIndex, xMMGroup, xspind: Integer;
  xJob: TJobRec;
  xTmpJob: PJobRec;
  xXMLData: String;
  xSize: DWord;
  xJobID: DWord;
  xBasedataIndex, xClassdataIndex : Integer;
  xGrpAssignment: TLxGrpAssignmentRec;
  xMaAssignment: TLxMaAssignmentRec;
  xMM_data_trans: TLxDataTransactionRec;
  xBaseDataBufferSize: Integer;
  xBufferSize: Integer;
  xLxBasedataBuffer: Array [0..cLxBasedatabuffersize] of Byte;
  xLxClassdataBuffer: Array [0..cLxClassdatabuffersize] of Byte;
  //............................................................................
  procedure CopyBaseData(aLxData: PBasedataBuffer; var aMMData: TMMDataRec);
  begin
    // Copiert die Spindeldaten vom LX-Record in den MM-Record
    aMMData.Len  := ROUND(aLxData^.length * 200 * 1000);   // Lze = Km Nicht Laenge; sondern Anzahl Nutentrommel Impulse
    // Overflow wird nicht mehr gepr�ft, da Datentype auf LX = WORD und es somit 10 Tage braucht bis ein Overflow eintreten kann.
    with aLxData^.TimeData do begin
      aMMData.tRun              := RunTime;            // tRun= Laufzeit der Spindel
      aMMData.tOp               := OperatingTime;      // tOp= Zeit in welcher mindestens eine Spindel in der Gruppe laeuft
      aMMData.tWa               := WatchTime;          // tWA= Laufzeit + Stopzeit der Spindel
      aMMData.LSt               := LongStops;
      aMMData.tLStProd          := LongStopTime;       // momentan alle longstops in Prod
    end;
    with aLxData^.MonitoringData do begin
      aMMData.Bob               := Bobbins;            // number of used cops
      aMMData.Cones             := Cones;
      aMMData.Sp                := Splices;            // No spooled splices
      aMMData.RSp               := SpliceRepetitions;  // number of splice repetitions
      aMMData.YB                := yarnBreaks;
      aMMData.CSys              := SystemCuts;         // number of system cuts
      aMMData.LckSys            := SystemAlarms;       // number of spindle locks because of system problem
      aMMData.CUpY              := UpYarnCuts;         // number of muratas upper yarn cuts
      aMMData.CYTot             := TotalYarnCuts;      // total "textil" yarn cuts:
      aMMData.CS                := ShortCuts;          // number of short cuts
      aMMData.CL                := LongCuts;           // number of long cuts,
      aMMData.CT                := ThinCuts;           // number of thin cuts
      aMMData.CN                := NepCuts;            // number of nep cuts
      aMMData.CSp               := SpliceCuts;         // number of splice cuts
      aMMData.CClS              := ShortClusterCuts;   // number of cluster cuts;
      aMMData.LckOffCnt         := OffCountAlarms;
      aMMData.CBu               := BunchCuts;          // cuts because of jumping yarn
      aMMData.CDBu              := DBunchCuts;         // delayed bunch cuts
      // Wird im MM nicht ben�tigt
      //aMMData.UClS            := shortClustArea;
      //aMMData.UClL            := longClustArea;
      //aMMData.UClT            := thinClustArea;
      aMMData.CSIRO             := FFCuts;             // number of siro cuts
      aMMData.LckSiro           := FFAlarms;           // number of spindle locks because of siro startup cuts
      aMMData.CSIROCl           := FFClusterCuts;      // SIRO Cluster schnitte
      aMMData.LckSIROCl         := FFClusterAlarms;    // SIRO Cluster Blockierungen
      aMMData.LckClS            := ShortClusterAlarms; // number of cluster locks;
      aMMData.CSfi              := SFIDCuts;           // Hier werden auf der LZE Minus und Plus Cuts bereits kummuliert. Nue
      aMMData.LckSfi            := SFIDAlarms;
      aMMData.CShortOffCntPlus  := ShortOffCountPlusCuts;
      aMMData.CShortOffCntMinus := ShortOffCountMinusCuts;
      aMMData.CShortOffCnt      := ShortOffCountCuts;
      aMMData.COffCntMinus      := OffCountMinusCuts;
      aMMData.COffCntPlus       := OffCountPlusCuts;
      aMMData.COffCnt           := OffCountCuts;
      aMMData.CP                := SynFFCuts;       // Synthetic FFibre cuts
      aMMData.CClL              := LongClusterCuts;
      aMMData.CClT              := ThinClusterCuts;
      aMMData.LckP              := SynFFAlarms;     // Synthetic FFibre alarmscuts
      aMMData.LckClL            := LongClusterAlarms;
      aMMData.LckClT            := ThinClusterAlarms;
      aMMData.LckShortOffCnt    := ShortOffCountAlarms;
      aMMData.LckNSLT           := NSLTAlarms;
      aMMData.CDrumWrap         := DrumWrapCuts;
     // end;
    end;

  //MM5.04: VCV-Daten eingef�gt. Nue:6.02.08
    with aLxData^.MonitoringData2 do begin
// GL 02/04/2013 VCV is only used for Zenit heads, else should be set to 0 ------------
        if mIsZenit then begin
             aMMData.CVCV              := VCVPlusCuts + VCVMinusCuts;    //Auf MM z.Z. keine seperate Ausweisung von Minus und Plus Cuts (wie SFI) Nue:6.02.08
             aMMData.LckVCV            := VCVAlarms;
        end
        else begin
             aMMData.CVCV              := 0;
             aMMData.LckVCV            := 0;
//-------------------------------------------------------------------------------------
         end
    end;

    with aLxData^.Sfi do begin
      aMMData.AdjustBase   := AdjustBase * 1000;
      if AdjustBase > 0 then aMMData.AdjustBaseCnt := 1
                        else aMMData.AdjustBaseCnt := 0;
      aMMData.SFICnt       := SfidNumber;
      aMMData.SFI          := SfidSum;
    end;
  end;
  //............................................................................
  procedure CopyIPIData(aLxData: PBasedataBuffer; var aMMData: TMMDataRec);
  begin
    with aLxData^.Imperfections do begin
      aMMData.Imp.Neps   := Neps;
      aMMData.Imp.Thick  := Thick;
      aMMData.Imp.Thin   := Thin;
      aMMData.Imp.Small  := Small;
      aMMData.Imp.I2_4   := Len2To4;
      aMMData.Imp.I4_8   := Len4To8;
      aMMData.Imp.I8_20  := Len8To20;
      aMMData.Imp.I20_70 := Len20To70;
//CodeSite.SendStringEx(csmIndigo, 'LX-CopyIPIData:', Format('aMMData.Imp.Neps:%d Neps:%d - aMMData.Imp.Thick:%d Thick:d% - aMMData.Imp.Thin:%d Thin:%d - aMMData.Imp.Small:%d Small:%d - '+
//  'aMMData.Imp.I2_4:%d Len2To4:%d - aMMData.Imp.I4_8:%d Len4To8:%d - aMMData.Imp.I8_20:%d Len8To20:%d - aMMData.Imp.I20_70:%d Len20To70:%d',
//  [aMMData.Imp.Neps, Neps, aMMData.Imp.Thick, Thick, aMMData.Imp.Thin, Thin, aMMData.Imp.Small, Small, aMMData.Imp.I2_4, Len2To4, aMMData.Imp.I4_8, Len4To8,
//   aMMData.Imp.I8_20, Len8To20, aMMData.Imp.I20_70, Len20To70]));  //////////////////////////////////////////NueTmp
    end;
  end;
  //............................................................................
  procedure CopyClassData(aLxData: PClassdataBuffer; var aMMData: TMMDataRec);
  var
    i: Integer;
  begin
    // Klassier-Daten, Lx 0..127; MM: 1..128
    for i := 1 to cClassCutFields do begin
      aMMData.ClassFieldRec.ClassCutField[i]   := aLxData^.ClassCutData.CC[i-1];
      aMMData.ClassFieldRec.ClassUncutField[i] := aLxData^.ClassUnCutData.CUC[i-1];
    end;
    // Spleiss-Klassier-Daten, Lx 0..127; MM: 1..128
    for i := 1 to cSpCutFields do begin
      aMMData.ClassFieldRec.SpCutField[i]   := aLxData^.ClassCutData.CSPC[i-1];
      aMMData.ClassFieldRec.SpUncutField[i] := aLxData^.ClassUnCutData.CSPUC[i-1];
    end;
    // SIRO Dark Data, Dark Lx 0..63; MM: 1..64
    for i := 1 to cMaxSiroFields do begin
      aMMData.ClassFieldRec.SIROCutField[i]   := aLxData^.ClassCutData.CFFDC[i-1];
      aMMData.ClassFieldRec.SIROUncutField[i] := aLxData^.ClassUnCutData.CFFDUC[i-1];
    end;
    // convert the SIRO Bright Data, Bright LX 0..63; MM: 65..128
    for i := 1 to cMaxSiroFields do begin
      aMMData.ClassFieldRec.SIROCutField[cMaxSiroFields+i]   := aLxData^.ClassCutData.CFFBC[i-1];
      aMMData.ClassFieldRec.SIROUncutField[cMaxSiroFields+i] := aLxData^.ClassUnCutData.CFFBUC[i-1];
    end;
  end;
  //..........................................................
begin
  try
    with aMsg, mLxDevHandler do  begin
      FillChar(xJob, sizeof(TJobRec), 0);
      xInfIndex:= wParam;
      xMMGroup:= lParam - 1;
      if mDebugEnabled then
        CodeSite.SendNote(Format('DataItemIndex: %d, MMGroup: %d', [xInfIndex, xMMGroup]));
      if xInfIndex > 0 then
        //..Event von  DATA_TRANSACT ?
        if xInfIndex = Getindex(cDATA_TRANSACT, xBufferSize) then begin
          if GetData(xInfIndex, @xMM_data_trans, Sizeof(TLxDataTransactionRec), 0) then begin
            //.. MM_DATA_READY
            if xMM_data_trans.Access = tsMM_DATA_READY then begin // Daten zum abholen bereit
              xJob.JobID          := xMM_data_trans.TransactNo;
              xJob.JobTyp         := jtGetZESpdData;
              xBasedataIndex := Getindex(cBASICDATA, xBaseDataBufferSize);
              xClassdataIndex := Getindex(cCLASSDATA, xBufferSize);
              for xspind := xMM_data_trans.SpindleFrom to xMM_data_trans.Spindleto do begin
                xJob.GetZeSpdData.SpindID := xspind;

                FillChar(xLxBasedataBuffer, cLxBasedatabuffersize, 0);
                FillChar(xLxClassdataBuffer, cLxClassdatabuffersize, 0);
                // Den MM Datenrecord auch noch gezielt l�schen
                FillChar(xJob.GetZeSpdData.SpdData, SizeOf(xJob.GetZeSpdData.SpdData), 0);

                GetData(xBasedataIndex, @xLxBasedataBuffer, cLxBasedataBuffersize, xspind);
                GetData(xClassdataIndex, @xLxClassdataBuffer, cLxClassdataBuffersize, xspind);
                CopyBaseData(PBasedataBuffer(@xLxBasedataBuffer), xJob.GetZeSpdData.SpdData);
                CopyClassData(PClassdataBuffer(@xLxClassdataBuffer), xJob.GetZeSpdData.SpdData);

                // Imperfektionsdaten sind erst ab einer gewissen Buffergr�sse verf�gbar. Nue: 15.11.06
                //   Check wird bei einem SaveMaConfig in der Routine TLXMachThread.Bin2XMLMaConfigAfterDelete gemacht,
                //   damit Handling gleich wie bei WSC und die Info zudem im XML auf der DB ist.
                if mNodeList.IPICalcOnMachine[mMachID] then
                  CopyIPIData(PBasedataBuffer(@xLxBasedataBuffer), xJob.GetZeSpdData.SpdData);

//                if xBaseDataBufferSize >= cBaseDataBufferSizeWithIPI then
//                  CopyIPIData(PBasedataBuffer(@xLxBasedataBuffer), xJob.GetZeSpdData.SpdData);

                mWriteToMsgHnd(@xJob);
              end;
              // Diese Transaktion ist komplett
              mLastTransactNo  := 0;

              with xMM_data_trans do
                CodeSite.SendStringEx(csmBlue, Format('GetData DataReady (TransactNo: %d)', [TransactNo]),
                                                      Format('IpAdr:%s%sAccess: %s%sFrom: %d%sTo: %d',
                                                      [mIpAdresse, cCrlf,
                                                      GetEnumName(TypeInfo(TLxTransactionState), Ord(Access)), cCrlf,
                                                      Spindlefrom, cCrlf,
                                                      Spindleto]));
            end
            //..MM_DATA_IDLE      Datentransaction abgeschlossen
            else if xMM_data_trans.access = tsMM_DATA_IDLE then begin
              // Timer zur�ckstellen
              KillTimer(mThreadWndHandle, cAcqTimerID);
              mTransactPending := FALSE;
              // Diese Transaktion ist komplett
              mLastTransactNo  := 0;

              // DelJob pos best�tigen
              if xMM_data_trans.TransactNo <> 0 then begin
                // wir haben eine g�tlige JobID, vermutlich vom DelZESpdData Job
                xJob.JobTyp           := jtDelZESpdData;
                xJob.JobID            := xMM_data_trans.TransactNo;
                mWriteToMsgHnd(@xJob);
              end;

              // Pruefe ob Queue leer ist
              if mJobAcqQueue.Count > 0 then begin
                codesite.SendInteger('mJobAcyQueue ' + mIpAdresse, mJobAcqQueue.Count);
                // Hole Job von Queue und sende Ihn
                PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(mJobAcqQueue.Pop), 0);
              end;
              with xMM_data_trans do
                CodeSite.SendStringEx(csmViolet, Format('GetData Transaction Complete (TransactNo: %d)', [TransactNo]),
                                                        Format('IpAdr:%s%sAccess: %s%sFrom: %d%sTo: %d',
                                                        [mIpAdresse, cCrlf,
                                                        GetEnumName(TypeInfo(TLxTransactionState), Ord(Access)), cCrlf,
                                                        Spindlefrom, cCrlf,
                                                        Spindleto]));
  //                CodeSite.SendFmtMsg('MM_DATA_IDLE: IpAdr=%s,TransactNo=%d, Access=%d, SpdFirst=%d, SpdLast=%d',
  //                  [mIpAdresse, TransactNo, Word(Access), SpindleFrom, Spindleto]);
            end else begin
              mTransactPending := FALSE;
              mWriteToLog(etError, Format('Transaction access unknown: %d %s, JobID: %d, LastJobID: %d', [Ord(xMM_data_trans.access), mIpAdresse, xMM_data_trans.TransactNo, mLastTransactNo]));
              // bei Fehler: hole n�chsten Job aus Queue und setze ihn auf.
              if mJobAcqQueue.Count > 0 then
                PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(mJobAcqQueue.Pop), 0);
              // Den aktuellen Job wird am System negativ best�tigt und eine Wiederholung gestartet
              // Diese Wiederholung wird vermutlich jedoch in die Queue geschoben
              if mLastTransactNo <> 0 then begin
                xJob.JobTyp           := jtJobResult;
                xJob.JobID            := mLastTransactNo;
                xJob.JobResult.JobTyp := jtNone;
                xJob.JobResult.Error  := SetError(ERROR_NOT_READY, etMMError, 'Invalid transaction number');
                mWriteToMsgHnd(@xJob);
                mLastTransactNo       := 0;
              end;
            end;
          end else begin // if GetData
            mTransactPending := FALSE;
            mWriteToLog(etError, Format('GetData failed for cDATA_TRANSACT. ID: %s, LastJobID: %d', [mIpAdresse, mLastTransactNo]));
            // bei Fehler: hole n�chsten Job aus Queue und setze ihn auf.
            if mJobAcqQueue.Count > 0 then
              PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(mJobAcqQueue.Pop), 0);
            // Den aktuellen Job wird am System negativ best�tigt und eine Wiederholung gestartet
            // Diese Wiederholung wird vermutlich jedoch in die Queue geschoben
            if mLastTransactNo <> 0 then begin
              xJob.JobTyp           := jtJobResult;
              xJob.JobID            := mLastTransactNo;
              xJob.JobResult.JobTyp := jtNone;
              xJob.JobResult.Error  := SetError(ERROR_NOT_READY, etMMError, 'GetData failed for cDATA_TRANSACT');
              mWriteToMsgHnd(@xJob);
              mLastTransactNo       := 0;
            end;
          end;
        end
//..Event von GRP_ASSIGMENT ?
        else if xInfIndex = Getindex(cGRP_ASSIGNMENT , xBufferSize) then begin
               GetData(xInfIndex, @xGrpAssignment, Sizeof(TLxGrpAssignmentRec), xMMGroup + 1);

               // LX Hack, da manchmal statt PreSettings Settings komemn (Best�tigung)
               if ((xGrpAssignment.GroupEvent = geSettings) and (mSetSettingsJobIDs[xMMGroup] <> 0)) then begin
                 (* Hierher kommen wir, wenn ein SetSettings noch nicht best�tigt wurde
                   (mSetSettingsJobIDs[xMMGroup] <> 0). In diesem Fall soll eine PreSettings
                   Nachricht emuliert werden. Dazu muss die xGrpAssignment Struktur von Hand
                   angepasst werden. *)
                 xGrpAssignment.GroupEvent := gePreSettings;
                 CodeSite.SendFmtNote('SetSettings erhalten aber PreSettings erwartet (JobID = %d)', [mSetSettingsJobIDs[xMMGroup]]);
               end;// if (xGrpAssignment.GroupEvent = geSettings) and (mSetSettingsJobIDs[xMMGroup] <> 0) then begin

               case xGrpAssignment.GroupEvent of
                 geNone: begin
                     CodeSite.SendFmtMsg('GroupEvent.None: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                 end;
        //.. geRange, geSettings
                 geRange, geSettings: begin
                   // Code Site
                   if xGrpAssignment.GroupEvent = geRange then
                     CodeSite.SendMsg('geRange received')
                   else if xGrpAssignment.GroupEvent = geSettings then
                     CodeSite.SendMsg('geSettings received')
                   else
                     CodeSite.SendMsg('geSettings/geRange received');

                   xJob.JobTyp           := jtAssign;
                   xJob.Assign.MachineID := mMachID;
                   FillChar(xJob.Assign.Assigns, Sizeof(xJob.Assign.Assigns), 0);
                   // Grp_Assignment von LZE lesen
                   GetGrpAssignment(xMMGroup, xJob.Assign.Assigns);
                   mWriteToMsgHnd(@xJob); // Assign zu MsgHandler schicken

                   // Timer aufsetzen, f�r den Fall, dass kein EntryLocked kommt
                  KillTimer(mThreadWndHandle, cEntryLockedTimerID);
                  SetTimer(mThreadWndHandle, cEntryLockedTimerID, cEntryLockedTimeout, nil);

                   with xJob.Assign.Assigns.Groups[xMMGroup] do
                      CodeSite.SendFmtMsg('GroupEvent.Settings or Range changed: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                         [mIpAdresse, xMMGroup, ProdId, WORD(Modified), SpindleFirst, SpindleLast]);
                   end;
        //.. geAdjust
                 geAdjust : begin
                   CodeSite.SendMsg('geAdjust received');
                   xJob.JobTyp           := jtAdjust;
                   xJob.Adjust.MachineID := mMachID;
                   mWriteToMsgHnd(@xJob); // Adjust Event MsgHandler schicken
                   CodeSite.SendFmtMsg('GroupEvent.Adjust: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                   end;
        //.. geAssignComplete
                 geAssignComplete : begin
                   CodeSite.SendMsg('geAdjust geAssignComplete');
                   xJob.JobTyp:= jtAssignComplete;
                   xJob.AssignComplete.MachineID:= mMachID;
                   mWriteToMsgHnd(@xJob); // AssignComplete Event MsgHandler schicken
                   CodeSite.SendFmtMsg('GroupEvent.AssignComplete: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                 end;
        //.. geStart
                 geStart : begin
                   CodeSite.SendMsg('eStart received');
                   xTmpJob := Nil;
                   // Grp_Setting aus MachGruppe lesen
                   if UploadAndConvertXMLSettings(xXMLData, xMMGroup, False) then begin
                     try
                       // gen�gend Speicher f�r Job reservieren
                       xSize   := Length(xXMLData);
                       xTmpJob := AllocMem(GetJobHeaderSize(jtStartGrpEvFromMa) + xSize);
                       with xTmpJob^, StartGrpEvFromMa, SettingsRec do begin
                         JobID     := 0;
                         JobTyp    := jtStartGrpEvFromMa;
                         NetTyp    := ntLX;
                         MachineID := mMachID;
                         // die extern gesammelten Parameter nun in Job einf�gen
                         //SettingsRec := mTempXMLSettings;
                         System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                         // XML Settings in den Job kopieren
                         System.Move(PChar(xXMLData)^, XMLData, xSize);
                         // PartieName, Color und StyleID von LZE lesen und in Settingsrecord ablegen
                         GetLotandStyleparam(xMMGroup, SettingsRec);
                          // YMSET-Name, -ID, -Changed von LZE lesen
                         GetYMSetParam(xMMGroup, SettingsRec);
                         // Spindelbereich,, Groupstate und ProdID abholen
                         GetGrpAssignment(xMMGroup, SettingsRec);
                         Group:= xMMGroup; //MM beginnt bei 0
                       end; // with xTmpJob^,
                        // Daten von GetSettings zu MsgHandler schicken
                        mWriteToMsgHnd(xTmpJob);
                        CodeSite.SendFmtMsg('GroupEvent.Start: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                      finally
                        FreeMem(xTmpJob);
                      end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then
                   end else
                      mWriteToLog(etError, 'eStart: UploadAndConvertXMLSettings failed!');
                   end; // eStart
        //.. geStop
                 geStop : begin
                     CodeSite.SendMsg('eStop received');
                     xTmpJob := Nil;
                     // Settings holen, um die ProdID zu haben
                     if UploadAndConvertXMLSettings(xXMLData, xMMGroup, False) then begin
                       try
                         // gen�gend Speicher f�r Job reservieren
                         xSize   := Length(xXMLData);
                         xTmpJob := AllocMem(GetJobHeaderSize(jtStopGrpEvFromMa) + xSize);
                         with xTmpJob^, StopGrpEvFromMa, SettingsRec do begin
                           JobID     := 0;
                           JobTyp    := jtStopGrpEvFromMa;
                           NetTyp    := ntLX;
                           MachineID := mMachID;
                           // die extern gesammelten Parameter nun in Job einf�gen
                           System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                           // XML Settings in den Job kopieren
                           System.Move(PChar(xXMLData)^, XMLData, xSize);
                           // Spindelbereich, Groupstate und ProdID abholen
                           GetGrpAssignment(xMMGroup, SettingsRec);
                           Group:= xMMGroup; //MM beginnt bei 0
                         end; // with xTmpJob^,
                         // Settings Daten zu MsgHandler schicken
                         mWriteToMsgHnd(xTmpJob);
                         CodeSite.SendFmtMsg('GroupEvent.Start: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                       finally
                         FreeMem(xTmpJob);
                       end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then
                       // Nach dem Stop interessiert es das MMSystem nicht mehr was f�r Bits in modify gesetzt sind -> l�schen
                       DeleteLxDecl(xMMGroup);
                     end else
                       mWriteToLog(etError, 'eStop: UploadAndConvertXMLSettings failed!');
                   end; // eStop
        //.. gePreSettings
                 gePreSettings : begin  // Nach SetSettings kommt hier die Best�tigung
                    try
                      with xGrpAssignment do
                        CodeSite.SendFmtMsg('gePreSettings received (%s: %d - %d)', [GetEnumName(TypeInfo(TLxProdGroupState), Ord(GroupState)), SpindleFirst, SpindleLast]);
                    except
                      CodeSite.SendError('gePreSettings: unbekannter TLxProdGroupState');
                    end;// try except

                    xTmpJob := Nil;
                    // JobID aus LZE lesen (Wurde beim Schreiben der Settigns gesetzt)
//                    GetData(cGRP_JOBID, @xJobID, SizeOf(xJobID), xMMGroup + 1);

                    // JobID vom SetSettings (Achtung bei parallelen Downloads)
                    xJobID := mSetSettingsJobIDs[xMMGroup];

                    if xJobID <> 0 then begin
                      if UploadAndConvertXMLSettings(xXMLData, xMMGroup, False) then begin
                        try
                          // gen�gend Speicher f�r Job reservieren
                          xSize   := Length(xXMLData);
                          xTmpJob := AllocMem(GetJobHeaderSize(jtSetSettings) + xSize);
                          with xTmpJob^, SetSettings, SettingsRec do begin
                            JobID     := xJobID;
                            JobTyp    := jtSetSettings;
                            NetTyp    := ntLX;
                            MachineID := mMachID;
                            // die extern gesammelten Parameter nun in Job einf�gen
                            //SettingsRec := mTempXMLSettings;
                            System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                            // XML Settings in den Job kopieren
                            System.Move(PChar(xXMLData)^, XMLData, xSize);
                            // PartieName, Color und StyleID von LZE lesen und in Settingsrecord ablegen
                            GetLotandStyleparam(xMMGroup, SettingsRec);
                            // YMSET-Name, -ID, -Changed von LZE lesen
                            GetYMSetParam(xMMGroup, SettingsRec);
                            // Spindelbereich, Groupstate und ProdID abholen
                            GetGrpAssignment(xMMGroup, SettingsRec);
                            Group:= xMMGroup; //MM beginnt bei 0
                            // Bestaetigung mit Einstellungen werden an MsgHandler geschickt
                            mWriteToMsgHnd(xTmpJob);
                            CodeSite.SendFmtMsg('GroupEvent.PreSettings: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xMMGroup]);
                            // JobID zur�cksetzen
                            xJobID := 0;
//                            PutData(cGRP_JOBID, @xJobID, Sizeof(xJobID), xMMGroup + 1);
                            mSetSettingsJobIDs[xMMGroup] := xJobID;
                          end; // with xTmpJob^,
                        finally
                          FreeMem(xTmpJob);
                        end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then
                      end else
                      //TODO: wss: bei Fehler -> Fehlermeldung mittels Fehlerjob an System mitteilen statt auf Timeout im MsgHandler warten
                        mWriteToLog(etError, 'ePreSettings: UploadAndConvertXMLSettings failed!');
                    end;// if xJobID <> 0
                  end; // ePreSettings
               else
                 CodeSite.SendMsg('Receive Unknown GroupEvent');
               end;
             end
//..Event von MA_ASSIGMENT ?
             else if xInfIndex = Getindex(cMA_ASSIGNMENT , xBufferSize) then begin
                   CodeSite.SendMsg('cMA_ASSIGNMENT received');
                    GetData(xInfIndex, @xMaAssignment, Sizeof(TLxMaAssignmentRec), 0);
                    case xMaAssignment.MaEvent of
                      meNone : begin
                        CodeSite.SendFmtMsg('MaEvent.None: IpAdr=%s',[mIpAdresse]);
                        end;
        //.. meInitialReset         // Kaltstart
                      meInitialReset {$IFDEF DEBUG_INITIALRESET}, meReset {$ENDIF} : begin
                        CodeSite.SendMsg('meInitialReset received');
                        xJob.JobTyp:= jtInitialReset;
                        xJob.InitialReset.MachineID := mMachID;
                        mWriteToMsgHnd(@xJob); // InitialReset zu MsgHandler schicken
                        CodeSite.SendFmtMsg('MaEvent.InitialReset: IpAdr=%s',[mIpAdresse]);
                        end;

{$IFNDEF DEBUG_INITIALRESET}
        //.. meReset
                      meReset : begin   // (Power on)
                        CodeSite.SendMsg('meReset received');
                        xJob.JobTyp:= jtReset;
                        xJob.Reset.MachineID := mMachID;
                        mWriteToMsgHnd(@xJob); // Reset Event zu MsgHandler schicken
                        CodeSite.SendFmtMsg('MaEvent.Reset: IpAdr=%s',[mIpAdresse]);
                        end;
{$ENDIF}

        //.. meEntryLocked
                      meEntryLocked : begin
                        KillTimer(mThreadWndHandle, cEntryLockedTimerID);
                        CodeSite.SendMsg('meEntryLocked received');
                        // Partie und Grp Assignment aller Gruppe abholen und zuruecksetzten
                        xJob.JobTyp:= jtEntryLocked;
                        xJob.EntryLocked.MachineID:= mMachID;
                        with xJob.AssignComplete do begin
                          FillChar(Assigns, Sizeof(Assigns), 0);
                          for xMMGroup := 0 to cLXSpdGroupLimit - 1 do begin
                            // Farbe, Style, OrderID/Position, Schlupf der Gruppe von LZE lesen
                            GetLotandStyleParam(xMMGroup, Assigns);
                            // LX YMSET der Gruppe holen
                            GetYMSetName(xMMGroup, Assigns);
                            // Grp_Assignment von LZE lesen
                            GetGrpAssignment(xMMGroup, Assigns);
                            // Pruefe mittels ProdID und GroupState ob �nderungen an der Gruppe abgeschlossen -> AssignComplete
                            with Assigns.Groups[xMMGroup] do begin
                              if (GroupState = gsInProd) AND (ProdId = 0) then begin
                                xJob.JobTyp                   := jtAssignComplete;
                                xJob.AssignComplete.MachineID := mMachID;
                                CodeSite.SendFmtMsg('AssignComplete after Locked: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                                    [mIpAdresse, xMMGroup, ProdId, WORD(Modified), SpindleFirst, SpindleLast]);
                              end;
                            end;
                            // Modify Flag und Event der Gruppe zuruecksetzen
                            DeleteLxDecl(xMMGroup);
                          end;
                          mWriteToMsgHnd(@xJob); // Event MsgHandler schicken
                          CodeSite.SendFmtMsg('MaEvent.EntryLocked: IpAdr=%s',[mIpAdresse]);
                        end;
                        end;
        //.. meEntryUnlocked
                      meEntryUnlocked : begin
                        CodeSite.SendMsg('meEntryUnlocked received');
                        xJob.JobTyp:= jtEntryUnlocked;
                        xJob.EntryUnlocked.MachineID := mMachID;
                        mWriteToMsgHnd(@xJob); // EntryUnlocked Event MsgHandler schicken
                        CodeSite.SendFmtMsg('MaEvent.EntryUnlocked: IpAdr=%s',[mIpAdresse]);
                        end;
        //.. meSettings
                      meSettings : begin
                        CodeSite.SendMsg('meSettings received');
                        CodeSite.SendFmtMsg('MaEvent.Settings: IpAdr=%s',[mIpAdresse]);
                        end;
        //.. meSections
                      meSections : begin
                        CodeSite.SendMsg('meSections received');
                        CodeSite.SendFmtMsg('MaEvent.Sections: IpAdr=%s',[mIpAdresse]);
                        end;
                    else
                      CodeSite.SendMsg('Receive Unknown MaEvent');
                    end;
             end;
    end;
  except
    on E: Exception do raise LXAccessException.Create('InformDispatcher ' + e.message);
      // on E: Exception do raise Exception.Create('InformDispatcher '+ e.message);
  end;
end;// TLXMachThread.LXDispatcher cat:No category

//:-----------------------------------------------------------------------------
procedure TLXMachThread.MMJobDispatcher(aMsg: TMsg);
  // Alle Jobs werden in das LX-ZE spezifische Format konvertiert
  // Exception werden abgefangen und weitergereicht
  // Datadirection JobHandler -> LxHandler
var
  xBufferSize: Integer;
  xDataTransactionRec: TLxDataTransactionRec;
  xSize: Cardinal;
  xMMGroup: Byte;
  xProdGrpID: Integer;
  xTmpJob: PJobRec;
  xPJob: PJobRec;
//  xIndex: Integer;
  xFreeJobMem: Boolean;
  xXMLData: String;
  xLxMaAssignmentRec: TLxMaAssignmentRec;
  xGroupState:  TLxProdGroupState;
  xStr: string;
//  xTempSettings: TXMLSettingsRec;
  xXMLMachine: string;
  xXMLSections: string;
    xMsg: TMsg;
  //xLxGrpAssignment: TLxGrpAssignmentRec;

  //............................................................................
  // V 5.x
  // Local Function
  function ConvertAndDownloadXMLSettings(const aXMLData: String; aMMGroup: Integer): Boolean;
  var
    xMapfile: DOMDocument40;
    xBuffer: PByte;
    xBufferSize: Integer;
    xMapID: String;
    xStart: Cardinal;
    {$IFDEF DEBUG_LX_HACK}
    {$ENDIF}
    function WaitForReadyFlag(aMMGroup: Integer): Boolean;
    var
      xStart: cardinal;
      xCurrent: Cardinal;
      xSettingsReady: TProdGroupId;
    const
      cWaitReadyFlagTimeout = 5000;
//Test Nue       cWaitReadyFlagTimeout = 10000; //Nue test neu ab 14.7.08
    begin
      CodeSite.SendInteger('Start polling SettingsReady - MMGroup', aMMGroup);
      xStart := GetTickCount;
      repeat
        Sleep(100);
        Fillchar(xSettingsReady, sizeof(TProdGroupId), 0);
        mLxDevHandler.GetData(cGRP_PRODID, @xSettingsReady, Sizeof(TProdGroupId), aMMGroup + 1);
        xCurrent := (GetTickCount - xStart);
      until (xCurrent > cWaitReadyFlagTimeout) or xSettingsReady.SettingsReady;
      Result := (xCurrent < cWaitReadyFlagTimeout);

      if Result then
        CodeSite.SendMsg('Stop polling SettingsReady')
      else
        CodeSite.SendError('Timeout polling SettingsReady');
    end;// function WaitForReadyFlag(aMMGroup: Integer);
  begin
    Result            := False;
    xMapID            := mNodeList.MapID[mMachID];
    xMapfile          := mNodeList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        OnBeforeConvert := XML2BinBeforeConvert;
        OnCalcExternal  := XML2BinEventHandler;  //Zuweisen der Procedure an den Event, welcher im TMMXMLConverter.TMSXMLEventer.Process ausgel�st wird
        OnBeforeDeleteElements := XML2BinAfterConvert;  // ConfigCode schreiben

        xBuffer := AllocMem(cLxBinBufferSize);
        try
          // --------------- Maschinen Settings ----------------------------
          // MachSetting aus Maschgruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings vor GetData(cMA_CONFIG','Nue');
          mLxDevHandler.GetData(cMA_CONFIG, xBuffer, cLxBinBufferSize, aMMGroup + 1);
          // Exceptions werden ganz unten pauschal abgefangen
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach GetData(cMA_CONFIG','Nue');
 CodeSite.SendMsg(FormatXML(aXMLData));   //////////////////////////////////////////NueTmp

          xBufferSize := XMLToBin(xBuffer, cLxBinBufferSize, xMapfile, msMachine, false, aXMLData);
          // Settings der Maschgruppe setzen (Im System: Gruppe 0-basiert -- LX 1-basiert)
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach XMLToBin(xBuffer,','Nue');
          mLxDevHandler.PutData(cMA_CONFIG, xBuffer, xBufferSize, aMMGroup + 1);
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach mLxDevHandler.PutData(cMA_CONFIG,','Nue');

          {$IFDEF DEBUG_LX_HACK}
            Result := True;
            xStart := GetTickCount;
            while ((GetTickCount - xStart) < 2000) do
              Sleep(1);
          {$ELSE}
            // Hack, da das Ready Flag f�r die Maschine noch nicht funktioniert Lok: 2.2.06
            xStart := GetTickCount;
            while ((GetTickCount - xStart) < 2000) do
              Sleep(1);
            // Warten bis die LZE fertig ist :)
//            Result := WaitForReadyFlag(-1); // Maschine
            Result := {Result and} WaitForReadyFlag(aMMGroup); // Runter geladenen Gruppe
          {$ENDIF}

          if Result then begin
            // --------------- Partie Settings ----------------------------
            // GrpSetting aus MachGruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
            mLxDevHandler.GetData(cGRP_SETTINGS, xBuffer, cLxBinBufferSize, aMMGroup + 1);
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach mLxDevHandler.GetData(cGRP_SETTINGS,','Nue');
            // Exceptions werden ganz unten pauschal abgefangen
            xBufferSize := XMLToBin(xBuffer, cLxBinBufferSize, xMapfile, msYMSetting, false, aXMLData);
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach XMLToBin(xBuffer,','Nue');
            // Settings der Maschgruppe setzen (Im System: Gruppe 0-basiert -- LX 1-basiert (Hier aber bereits auf 1-basiert ge�ndert) )
            mLxDevHandler.PutData(cGRP_SETTINGS, xBuffer, xBufferSize, aMMGroup + 1);
CodeSite.SendStringEx(csmViolet,'@@ConvertAndDownloadXMLSettings nach mLxDevHandler.PutData(cGRP_SETTINGS,','Nue');

            {$IFDEF DEBUG_LX_HACK}
              xStart := GetTickCount;
              while ((GetTickCount - xStart) < 2000) do
                Sleep(1);
            {$ELSE}
              // Warten bis die LZE fertig ist :)
              Result := Result and WaitForReadyFlag(aMMGroup); // Runter geladenen Gruppe
              { TODO : Ablauf Pollen noch nicht restlos getestet. }
              // Jetzt alle Messages aus der Message Queue entfernen die in der Zwischenzeit von der LX kamen
              while PeekMessage(xMsg, mThreadWndHandle, mLxInformMsg, mLxInformMsg, PM_REMOVE) do;
            {$ENDIF}
          end;// if Result then begin
        finally
          FreeMem(xBuffer);
        end; // try

      finally
        Free;
        Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
      end; //with
    end; // if Assigned(xMapfile)
  end;

  //............................................................................
  // V 5.x
  // Local Function
  function ExtractDefaultValues: String;
  var
    xMapfile: DOMDocument40;
    xMapID: String;
  begin
    Result   := '';
    xMapID   := mNodeList.MapID[mMachID];
    xMapfile := mNodeList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        IgnoreDeleteFlag := true;
        // Es erfolgt keine weitere Verarbeitung in irgendwelchen Events
        result := BinToXML(Nil, 0, xMapfile, msYMSetting, False);
      finally
        Free;
        Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
      end; //with
    end; // if Assigned(xMapfile)
  end;
  //............................................................................
  // Local Function
  procedure SetYMSetParam(aMMGroup: Integer; aSettingsRec: TXMLSettingsRec);
  var
    xYMSet: TLx_YmSet;
  begin
    with mLxDevHandler, aSettingsRec do begin
      try
        FillChar(xYMSet, sizeof(xYMSet), 0);
        // MBCS zu Unicode kovertieren und LX YMSet zuordnen
        CharArraytoUniCode(@YMSetName, xYMSet.YM_Name);
        { TODO -okhp : Kann YMSET changed zur�ckgesetzt werden? }
        // if YMSetChanged then xYMSet.YM_Changed:= 0 else xYMSet.YM_Changed:= 1;
        xYMSet.YM_ID:= YMSetID;
        // YMSET_PARAM DataItem der Gruppe lesen und in MM-YMSets speichern
        PutData(cGRP_YMSET_PARAM, @xYMSet, Sizeof(TLx_YmSet), aMMGroup + 1);
      except
        on E: Exception do
          CodeSite.SendWarning('SetYMSetParam ' + e.message)
      else
        CodeSite.SendError('SetYMSetParam failt with unknown error');
    end;
    end;
  end;
  //............................................................................
  // Local Function
  procedure GetGrpStateAndSplRange(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec);
  var
    xLxGrpAssignment: TLxGrpAssignmentRec;
  begin
    with aSettingsRec do begin
      try
      // LX Grp Assignment von LZE lesen
      Fillchar(xLxGrpAssignment,sizeof(xLxGrpAssignment),0);
      mLxDevHandler.GetData(cGRP_ASSIGNMENT, @xLxGrpAssignment, Sizeof(TLxGrpAssignmentRec), aMMGroup + 1);

//{ TODO -oNUE -cLX-Memories : ACHTUNG nur tempor�r f�r Tests bis anfang November!! Weil bei Lab noch nicht implementiert!! Nue }
//if aMMGroup>=11 then
//  xLxGrpAssignment.GroupState:= pgsTemplate;



      // LX Grp_Status SettingsRec_Groupstate zuordnen
      Case xLxGrpAssignment.GroupState of
        pgsDefined:  GroupState:= gsDefined;
        pgsInProduction: GroupState:= gsInProd;
        pgsFree:  GroupState:= gsfree;
        pgsInvalid: GroupState:= gsfree;
        pgsTemplate:  GroupState:= gsMemory;
      else
      end;
      // Sicherstellen, dass der Spindelrange mit dem eingestellten Spindle Range �bereinstimmt
      SpindleFirst := xLxGrpAssignment.SpindleFirst;
      SpindleLast  := xLxGrpAssignment.SpindleLast;
      except
       on E: Exception do
          CodeSite.SendWarning('GetGrpAssignment' + e.message)
      end
    end;
  end;

   //............................................................................
   // Local Function
   procedure GetGrpState(aMMGroup: Integer; var aGroupState: TLxProdGroupState);
   var
    xLxGrpAssignment: TLxGrpAssignmentRec;
   begin
     try
       // LX Grp Assignment von LZE lesen
       Fillchar(xLxGrpAssignment,sizeof(xLxGrpAssignment),0);
       mLxDevHandler.GetData(cGRP_ASSIGNMENT, @xLxGrpAssignment, Sizeof(TLxGrpAssignmentRec), aMMGroup + 1);
       // LX Grp_Status zuordnen
       aGroupState:= xLxGrpAssignment.GroupState;
     except
       on E: Exception do
          CodeSite.SendWarning('GetGrpState' + e.message)
     end
   end;
  //............................................................................
  // Local Function
  procedure SetLotandArticleparam(aMMGroup: Integer; var aSettingsRec: TXMLSettingsRec; aGroupState: TLxProdGroupState);
  var
    xLxLotStyle :TLxLotStyleRec;
    xLxGrpAssignment: TLxGrpAssignmentRec;
    xTempYarnCnt: Single;

    (*---------------------------------------------------------
      Hiolt den Artikelnamen von der DB (evt. sp�ter aus TXMLSettingsRec 7.10.2005 Lok/Wss)
    ----------------------------------------------------------*)
    function GetStyleNameFromID(aID: Cardinal): string;
    begin
      Result := '';
      with TAdoDBAccess.Create(1) do try
        Init;
        with Query[0] do begin
          SQL.Text := 'SELECT c_style_name FROM t_style WHERE c_style_id = :c_style_id';
          ParamByName('c_style_id').AsInteger := Integer(aID);
          open;
          if not(EOF) then
            Result := FieldByName('c_style_name').AsString;
        end;// with Query[0] do begin
      finally
        Free;
      end;// with TAdoDBAccess.Create(1) do try                              
    end;// function GetStyleNameFromID(aID: Integer): string;
  begin
    with mLxDevHandler, aSettingsRec do
    try
      FillChar(xLxLotStyle, sizeof(xLxLotStyle),0);
      FillChar(xLxGrpAssignment, sizeof(xLxGrpAssignment),0);
      // Zurest aktuelle Parameter von Grp_Assignment und Grp_LotStyle lesen
      GetData(cGRP_LOT_STYLE_PARAM, @xLxLotStyle, Sizeof(TLxLotStyleRec), aMMGroup + 1);
      // Partiename konvertieren und in LxLotStyleRec speichern
      CharArraytoUniCode(@ProdName, xLxLotStyle.PartieName);
      // LotStyleRec mit StyleID und Color abfuellen OrderID, OrderPosID und Schlupf existieren auf der LX ZE nicht
      xLxLotStyle.StyleID := StyleID;
      xLxLotStyle.Color   := Color;
      xLxLotStyle.Slip    := Slip;
      // Artikelname von der DB holen und Eintragen (evt. sp�ter im SettingsRecord �bertragen 7.10.2005 Lok/Wss)
      CharArraytoUniCode(PChar(GetStyleNameFromID(StyleID)), xLxLotStyle.StyleName);
      if NOT (aGroupState = pgsInProduction) then begin
        // MM kommt immer mit Nm und ganzer Faden
        xTempYarnCnt := YarnCnt;

//        xLxLotStyle.YarnCntUnit := lxyuNm;
        xLxLotStyle.YarnCnt := xTempYarnCnt;

{        // Konvertiere YU in die entsprechende Einheit
        case YarnCntUnit of
          yuNeB : begin
              xLxLotStyle.YarnCntUnit := lxyuNe;
              xLxLotStyle.YarnCnt := YarnCountConvert(yuNm, YarnCntUnit, xTempYarnCnt);
            end;
          yutex : begin
              xLxLotStyle.YarnCntUnit := lxyuTex;
              xLxLotStyle.YarnCnt := YarnCountConvert(yuNm, YarnCntUnit, xTempYarnCnt);
            end;
          yuNeW : begin
              xLxLotStyle.YarnCntUnit := lxyuNew;
              xLxLotStyle.YarnCnt := YarnCountConvert(yuNm, YarnCntUnit, xTempYarnCnt);
            end;
          else
            // In allen anderen F�llen wird Nm runtergeladen
            xLxLotStyle.YarnCntUnit := lxyuNm;
            xLxLotStyle.YarnCnt := xTempYarnCnt;
        end;// case xLxLotStyleRec.YarnCntUnit of}

        xLxLotStyle.NrOfThreads:= NrOfThreads;
      end;
      (* Den Spindelrange in den Record kopieren, da sonst in der Weiterverarbeitung
         der Range der XML Settings verwendet wird. Und der k�nnte falsch sein. *)
      GetData(cGRP_ASSIGNMENT,@xLxGrpAssignment,Sizeof(TLxGrpAssignmentRec), aMMGroup + 1);
      SpindleFirst := xLxGrpAssignment.SpindleFirst;
      SpindleLast  := xLxGrpAssignment.SpindleLast;
      PutData(cGRP_LOT_STYLE_PARAM, @xLxLotStyle, Sizeof(TLxLotStyleRec), aMMGroup + 1);

      CodeSite.SendFmtMsg('SetLotandArticleparam: StyleID=%d,MaschGrp=%d,Color=%d',
                          [StyleID, aMMGroup, Color]);
    except
      on E: Exception do begin
        CodeSite.SendWarning('SetLotandArticleparam ' + e.message);
        raise;
      end;
    end; // with
  end;

  //............................................................................
  // Local Function
  procedure SetProdID(aMMGroup: Integer; aProdGrpID: Integer);
  var
    xProdID: TProdGroupId;
  begin
    FillChar(xProdID, sizeof(TProdGroupId),0);
    xProdID.ProdGroupId := aProdGrpID;
    
    try
      mLxDevHandler.PutData(cGRP_PRODID, @aProdGrpID, Sizeof(TProdGroupId), aMMGroup + 1);
    except
      on E: Exception do
        CodeSite.SendWarning('SetProdID ' + e.message)
    end;
  end;

// Begin TLxMachThread.MMJobDispatcher .........................................
begin
//  xIndex      := 0;
  xFreeJobMem := TRUE;
  with aMsg do begin
    xPJob := PJobRec(wParam);
    try
      case xPJob^.JobTyp of
  //..GetZESpdData
        jtGetZESpdData: begin
          with mLxDevHandler do begin
            // Pruefe ob neue Data Acquisition moeglich
            if NOT mTransactPending then begin
              with xPJob^.GetZESpdData do
                with xDataTransactionRec do begin // Record xMM_data_trans ueber xBuffer legen
                  TransactNo  := xPJob^.JobID;
                  Access      := tsMM_NEW_DATA;
                  Spindlefrom := SpindleFirst;
                  Spindleto   := SpindleLast;
                  // F�r Debugmeldungen die letzte Transaktionsnummer merken (Hat keinen Einfluss auf das System)
                  // wss: wird im Timeout Fall auch verwendet -> nicht entfernen
                  mLastTransactNo := TransactNo;
                  CodeSite.SendStringEx(csmGreen, Format('GetData Start (TransactNo: %d)', [TransactNo]),
                                                          Format('IpAdr:%s%sAccess: %s%sFrom: %d%sTo: %d',
                                                          [mIpAdresse, cCrlf,
                                                          GetEnumName(TypeInfo(TLxTransactionState), Ord(Access)), cCrlf,
                                                          Spindlefrom, cCrlf,
                                                          Spindleto]));
                end;
              // Q+Basis Daten anfordern und Timeouttimer starten
//              with xDataTransactionRec do begin
//                CodeSite.SendFmtMsg('GetZESpdData: IpAdr=%s, TransactNo=%d, SpdFirst=%d, SpdLast=%d',
//                                    [mIpAdresse, TransactNo, Spindlefrom, Spindleto]);
//              end;
              KillTimer(mThreadWndHandle, cAcqTimerID);
              if PutData(cDATA_TRANSACT, @xDataTransactionRec, sizeof(TLxDataTransactionRec), 0) then begin
                Sleep(1000);
                if GetData(Getindex(cDATA_TRANSACT, xBufferSize), @xDataTransactionRec, Sizeof(TLxDataTransactionRec), 0) then begin
                  if xDataTransactionRec.Access <> tsMM_DATA_READY then
                    mWriteToLog(etError, Format('jtGetZESpdData %s verify: %d, JobID: %d ', [mIpAdresse, Ord(xDataTransactionRec.Access), xDataTransactionRec.TransactNo]));
                end;

                SetTimer(mThreadWndHandle, cAcqTimerID, cAcqTimeout, nil);
                mTransactPending:= TRUE;
              end else
                mWriteToLog(etError, 'jtGetZESpdData: PutData failed! ' + mIpAdresse);
            end
            else begin    // Data Acquisition ist pending somit Job in Queue schieben
//              CodeSite.SendMsg('mJobAcqQueue.Push');
              CodeSite.SendStringEx(csmGreen, Format('GetData Push (TransactNo: %d)', [xPJob^.JobID]),
                                                      Format('IpAdr:%s'+cCrLf+'From: %d To: %d',
                                                             [mIpAdresse, xPJob^.GetZESpdData.SpindleFirst, xPJob^.GetZESpdData.SpindleLast]));
              mJobAcqQueue.Push(xPJob);
              xFreeJobMem := False;
            end;
            // Der Timer fuer die DataAcquisation wird gestoppt und neu gestartet.
//            KillTimer(mThreadWndHandle, cAcqTimerID);
//            SetTimer(mThreadWndHandle, cAcqTimerID, cAcqTimeout, nil);
//            CodeSite.SendStringEx(csmGreen, Format('Acq Timer Armed (TransactNo: %d)', [xPJob^.JobID]),
//                                                    Format('IpAdr:%s'+cCrLf+'From: %d To: %d',
//                                                           [mIpAdresse, xPJob^.GetZESpdData.SpindleFirst, xPJob^.GetZESpdData.SpindleLast]));
          end;
        end;
  //..DelZESpdData
        jtDelZESpdData: begin
          with xDataTransactionRec do begin // Record data_trans ueber xBuffer legen
            TransactNo      := xPJob^.JobID;
            Access          := tsMM_DATA_Ack;
            Spindlefrom     := xPJob^.DelZESpdData.SpindleFirst;
            Spindleto       := xPJob^.DelZESpdData.SpindleLast;
            //wss: Aktuelle Transaktionnummer merken
            mLastTransactNo := TransactNo;
          end;
          // Empfang von Q+Basis Daten bestaetigen
          with mLxDevHandler do begin
            if PutData(cDATA_TRANSACT, @xDataTransactionRec, sizeof(TLxDataTransactionRec), 0) then begin
              // Best�tigung wird erst beim Ack IDLE ausgel�st -> JobID wird aus TransactionRec verwendet
              // mWriteToMsgHnd(xPJob);    // Bestaetigung an MsgHandler schicken
              with xDataTransactionRec do begin
                CodeSite.SendStringEx(csmBlue, Format('GetData DeleteData (TransactNo: %d)', [TransactNo]),
                                                      Format('IpAdr:%s%sAccess: %s%sFrom: %d%sTo: %d',
                                                      [mIpAdresse, cCrlf,
                                                      GetEnumName(TypeInfo(TLxTransactionState), Ord(Access)), cCrlf,
                                                      Spindlefrom, cCrlf,
                                                      Spindleto]));
  //              CodeSite.SendFmtMsg('DelZESpdData: IpAdr=%s, TransactNo=%d, Access=%d,SpdFirst=%d, SpdLast=%d',
  //                [mIpAdresse, TransactNo, Word(Access), Spindlefrom + 1, Spindleto + 1]);
              end;
            end else
              mWriteToLog(etError, 'jtDelZESpdData: PutData failed! ' + mIpAdresse);
          end;
        end;
  //.. GetSettingsAllGroups
  //.. GetSettings
        jtGetSettings: begin
          CodeSite.SendMsg('jtGetSettings erhalten');
          xTmpJob := Nil;
          xMMGroup  := xPJob^.GetSettings.SettingsRec.Group;  // Arrays MM=0, LX=1 basierend
          if UploadAndConvertXMLSettings(xXMLData, xMMGroup, xPJob^.GetSettings.AllXMLValues) then begin
            try
              // gen�gend Speicher f�r Job reservieren
              xSize   := Length(xXMLData);
              xTmpJob := AllocMem(GetJobHeaderSize(jtGetSettings) + xSize);
              with xTmpJob^, GetSettings, SettingsRec do begin
                JobID       := xPJob^.JobID;
                JobTyp      := xPJob^.JobTyp;
                NetTyp      := xPJob^.NetTyp;
                MachineID   := mMachID;
                // die extern gesammelten Parameter nun in Job einf�gen
                SettingsRec := mTempXMLSettings;
                // GruppeNo. in SettingsRec kopieren
                Group       := xPJob^.GetSettings.SettingsRec.Group;
                // XML Settings in den Job kopieren
                System.Move(PChar(xXMLData)^, XMLData, xSize);
                // Partiename, Farbe, Style, OrderID/Position, Schlupf von der LX Zentrale lesen und in MM SettingsRec ablegen
                GetLotandStyleParam(xMMGroup, SettingsRec);
                // LX YMSET Parameter holen
                GetYMSetParam(xMMGroup, SettingsRec);
                // LX Grp Assignment von LZE lesen
                GetGrpStateandSplRange(xMMGroup, SettingsRec);
              end; // with xTmpJob^,
              // Daten von GetSettings zu MsgHandler schicken
              mWriteToMsgHnd(xTmpJob);
            finally
              FreeMem(xTmpJob);
            end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo)
          end else begin
            mWriteToLog(etError, 'GetSettings: UploadAndConvertXMLSettings failed!');
          end;// if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then begin
        end;
  //.. SetSettings
        jtSetSettings: begin
          if xPJob^.SetSettings.SettingsRec.AssignMode = cAssignProdGrpIDOnly then begin
            CodeSite.SendMsg('jtSetSettings erhalten (ProdIDOnly)');
          end else begin
            CodeSite.AddSeparator;
            CodeSite.SendMsg('jtSetSettings erhalten');
          end;// if xPJob^.SetSettings.SettingsRec.AssignMode = cAssignProdGrpIDOnly then begin

          FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
          xMMGroup   := xPJob^.SetSettings.SettingsRec.Group; // Arrays MM=0, LX=1 basierend
          xProdGrpID := 0;
          with xPJob^.SetSettings, SettingsRec do begin
            // Grp_State von LZE lesen
            GetGrpState(xMMGroup, xGroupState);
            // Gruppe Frei oder Definiert
            if (xGroupState = pgsFree ) or (xGroupState = pgsDefined) then begin
              // StyleID, Color werden gesetzt
              SetLotandArticleparam(xMMGroup, SettingsRec, xGroupState);
              // YMSet Parameter
              SetYMSetParam(xMMGroup, SettingsRec);
              // Partiedaten umkopieren f�r das Eventhandling...
              xXMLData                    := StrPas(XMLData);
              mTempXMLSettings            := SettingsRec;
              mTempXMLSettings.AssignMode := cAssignGroup;
              // jedoch ohne den MMXML String (Keine Freigabe notwendig, da nur das erste Byte des Strings zum Record geh�rt)
              mTempXMLSettings.XMLData    := #0;
CodeSite.SendStringEx(csmViolet,'@@jtSetSettings nach (xGroupState = pgsFree )','Nue');
            end
            // Gruppe InProduktion
            else begin
              if AssignMode = cAssignProdGrpIDOnly then begin
                // ProdID wird erg�nzt, wenn z.B. eine Partie auf der LZE neu gestartet wurde (Definiert -> InProd).
                xProdGrpID := ProdGrpID; // Bei AssignProdGrpIDOnly wird die neue ProdID in lokale Variable gespeichert
                                         // und nach Settings Download separat geschrieben.
CodeSite.SendStringEx(csmViolet,'@@jtSetSettings nach cAssignProdGrpIDOnly','Nue');
              end
              else begin
                // Hier wird nur der PartieName, StyleID und Color gesetzt, da in einer laufende Produktion weder
                // der Spindelbereich noch die Garnnummer ver�ndert werden darf!!
                SetLotandArticleparam(xMMGroup, SettingsRec, xGroupState);
                // YMSet Parameter
                SetYMSetParam(xMMGroup, SettingsRec);
                // Beim �berstarten einer laufenen Partie wird die neue ProdID
                // aus SettingsRec in lokale Variable gespeichert und nach Settings Download separat geschrieben.
                xProdGrpID                  := SettingsRec.ProdGrpID;
                xXMLData                    := StrPas(XMLData);
                mTempXMLSettings            := SettingsRec;
                mTempXMLSettings.AssignMode := cAssignGroup;
                // jedoch ohne den MMXML String (Keine Freigabe notwendig, da nur das erste Byte des Strings zum Record geh�rt)
                mTempXMLSettings.XMLData    := #0;
CodeSite.SendStringEx(csmViolet,'@@jtSetSettings nach Gruppe InProduktion','Nue');
              end;
            end;

            // Jobid auf YM MachineGrp setzen, wird bei ePreSettings f�r die Weiterverarbeitung ben�tigt
//            mLxDevHandler.PutData(cGRP_JOBID, @xPJob^.JobID, Sizeof(DWord), xMMGroup + 1);
            mSetSettingsJobIDs[xMMGroup] := xPJob^.JobID;
CodeSite.SendStringEx(csmViolet,'@@jtSetSettings nach mSetSettingsJobIDs','Nue');

            // Settings Download nur wenn nicht AssignProdGrpIDOnly Flag gesetzt ist
            try
              if AssignMode <> cAssignProdGrpIDOnly then begin
                // Settingsdownload und nachher ProdID nachschicken
                if ConvertAndDownloadXMLSettings(xXMLData, xMMGroup) then
                  SetProdID(xMMGroup, xProdGrpID)
                else
                  mWriteToLog(etError, 'jtSetSettings: ConvertAndDownloadXMLSettings failed!');
              end else begin
                // Nur ProdID schreiben (Kein Setting)
                SetProdID(xMMGroup, xProdGrpID);
              end;// if AssignMode <> cAssignProdGrpIDOnly then begin
            except
              on e:Exception do begin
                xStr := Format('jtSetSettings: ConvertAndDownloadXMLSettings failed! DecSep:"%s" Msg: %s', [DecimalSeparator, e.Message]);
                mWriteToLog(etError, xStr);
                xPJob.JobTyp           := jtJobResult;
                xPJob.JobResult.JobTyp := jtSetSettings;
                xPJob.JobResult.Error  := SetError(ERROR_BAD_FORMAT, etMMError, xStr);
                mWriteToMsgHnd(xPJob);      // Status zu MsgHandler schicken
              end;
            end;  // try
          end; // with xPJob^.SetSettings
        end;
 //.. SaveMaConfigToDB
        jtSaveMaConfigToDB: begin
            EnterMethod('jtSaveMaConfigToDB');
            xXMLSections := '';
            xXMLMachine := '';
            xTmpJob  := Nil;
            try
              mLxDevHandler.GetData(cMA_ASSIGNMENT, @xLxMaAssignmentRec, SizeOf(TLxMaAssignmentRec), 0);
              // Alle aktiven Gruppen durchgehen und Settings anfordern um zu konvertieren
              mLXSection := 0;
              // Soviele Gruppen hat es -> Array f�r die Section Index erzeugen
              SetLength(mLXSectionArr, xLxMaAssignmentRec.SectionCount);
              for xMMGroup:=0 to xLxMaAssignmentRec.SectionCount-1 do begin // MM 0-basiert, Lx 1-basiert
                // mLXSection wird innerhalb dieser Methode auf DEN Index gesetzt, wo eine g�ltige
                // Gruppe auf der LZE ist. Z.B. Gruppen 0,1,4,11; mLXSection = 1
                // Nach der Methode w�re mLXSection = 4, da die L�cher �bersprungen werden   
                if UploadAndConvertSectionsAlt(xXMLSections, mLXSection) then
                  mLXSectionArr[xMMGroup] := mLXSection
                else
                  mWriteToLog(etError, Format('UploadAndConvertSection (%d) failed!', [xMMGroup]));
                // N�chst m�glicher Index, um diese Gruppe nicht nochmals zu holen
                Inc(mLXSection);
              end; // for xGrpNo

              // Jetzt noch den Maschinenteil holen
              if not UploadAndConvertMachine(xXMLMachine) then
                mWriteToLog(etError, 'UploadAndConvertMachine failed!');

              // Stellt die Maschinen Konfiguration zusammen
              xXMLData := BuildMaConfig(xXMLSections, xXMLMachine, ExtractDefaultValues);
              // anschliessend die Konfigdaten in einen Job verpacken und abschicken
              // gen�gend Speicher f�r Job reservieren
              xSize   := Length(xXMLData);
              xTmpJob := AllocMem(GetJobHeaderSize(jtSaveMaConfigToDB) + xSize);
              with xTmpJob^, SaveMaConfig do begin
                JobID       := xPJob^.JobID;
                JobTyp      := xPJob^.JobTyp;
                NetTyp      := xPJob^.NetTyp;
                MachineID   := mMachID;
                LastSpindle := xLxMaAssignmentRec.SpindleCount;
                UseXMLData  := True;
                // Konfigdaten in den Job kopieren
                if xSize > 0 then
                  System.Move(PChar(xXMLData)^, XMLData, xSize);
              end; // with xTmpJob^,

              // Daten von GetSettings zu MsgHandler schicken
              mWriteToMsgHnd(xTmpJob);
            finally
              FreeMem(xTmpJob);
              SetLength(mLXSectionArr, 0);
            end;
          end; // jtSaveMaConfigToDB
  //.. ClearZESpdData
        jtClearZESpdData: begin
          // Nach Partiestart werden die Daten durch LZE geloescht. Daher
          // nur Job bestaetigen.
          CodeSite.SendMsg('ClearZESpdData ' + mLxDevHandler.mIpAdresse);
          mWriteToMsgHnd(xPJob);      // Bestaetigung zu MsgHandler schicken
          end;
  //..GetMaAssign
        jtGetMaAssign: begin
         { TODO -okhp : JobDispatcher GetMaassign implementieren }
          with mLxDevHandler, xPJob^.GetMaAssign  do begin
            FillChar(Assigns, sizeof(TMaAssign), 0);
            MachineID := mMachID;
            for xMMGroup:=0 to  cLXSpdGroupLimit-1 do begin // Arrays MM=0, LX=1 basierend
              GetGrpAssignment(xMMGroup, Assigns);
              // Modify Flag und Event zuruecksetzen
              DeleteLxDecl(xMMGroup);
                // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
              GetLotandStyleParam(xMMGroup,Assigns);
            end; // for xGrpNo
            mWriteToMsgHnd(xPJob);    // Daten zu MsgHandler schicken
          end; // with mWSCHandler}
        end; // jtGetMaAssign}
      else
      end;
    except
        on E: Exception do raise LXAccessException.Create('MMJobDispatcher ' + e.message);
    end;
    if xFreeJobMem then FreeMem(xPJob, xPJob^.JobLen);
  end;
end;// TLxMachThread.MMJobDispatcher cat:No category

//:-----------------------------------------------------------------------------
procedure TLXMachThread.RefreshMachstatusTicker;
  // Setzt den Lifecheck Timer zur�ck und gleich wieder neu auf
begin
  KillTimer(mThreadWndHandle, cMachstatusTickerID);
  if mDebugEnabled then
    CodeSite.SendNote('MachstatusTicker: Timer refreshed');
  SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil);
end;

(*---------------------------------------------------------
  Sendet ein Assign Complete an den MessageHandler. Diese Methode wird aufgerufen
  wenn nach einer Settings- oder Range�nderung kein EntryLocked empfangen wird.
----------------------------------------------------------*)
procedure TLXMachThread.SendAssignComplete;
var
  xJob: TJobRec;
  xMMGroup: Integer;
begin
  KillTimer(mThreadWndHandle, cEntryLockedTimerID);
  FillChar(xJob, sizeof(TJobRec), 0);
  with xJob.AssignComplete do begin
    FillChar(Assigns, Sizeof(Assigns), 0);
    for xMMGroup := 0 to cLXSpdGroupLimit - 1 do begin
      // Farbe, Style, OrderID/Position, Schlupf der Gruppe von LZE lesen
      GetLotandStyleParam(xMMGroup, Assigns);
      // LX YMSET der Gruppe holen
      GetYMSetName(xMMGroup, Assigns);
      // Grp_Assignment von LZE lesen
      GetGrpAssignment(xMMGroup, Assigns);
      // Pruefe mittels GroupState ob �nderungen an der Gruppe gemacht wurden -> AssignComplete
      with Assigns.Groups[xMMGroup] do begin
        if (GroupState = gsInProd) then begin
          xJob.JobTyp                   := jtAssignComplete;
          xJob.AssignComplete.MachineID := mMachID;
          CodeSite.SendFmtMsg('AssignComplete after EntryLockTimeout: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                              [mLxDevHandler.mIpAdresse, xMMGroup, ProdId, WORD(Modified), SpindleFirst, SpindleLast]);
        end;
      end;
      // Modify Flag und Event der Gruppe zuruecksetzen
      DeleteLxDecl(xMMGroup);
    end;
    mWriteToMsgHnd(@xJob); // Event MsgHandler schicken
    CodeSite.SendFmtMsg('EntryLocked Timeout: IpAdr=%s',[mLxDevHandler.mIpAdresse]);
  end;
end;

//:-----------------------------------------------------------------------------
function TLxMachThread.SendMachinenstatus: Boolean;
var
  xJob: TJobRec;
begin
  result := false;
  try
    if mLxDevHandler.mConnected = True then
      xJob.JobTyp := jtMaOnline
    else
      xJob.JobTyp := jtMaOffline;
    xJob.Maoffline.MachineID := mMachID;
    mWriteToMsgHnd(@xJob);
    RefreshMachstatusTicker;
  except
  end;
end;// TLxMachThread.SendMachinenstatus cat:No category

  //............................................................................
  // V 5.x
  // Local Function
function TLXMachThread.UploadAndConvertMachine(var aXMLData: String): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
begin
  Result   := False;
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    xBuffer := AllocMem(cLxBinBufferSize);
    try
      // Maschinensettings in den Buffer lesen
      mLxDevHandler.GetData(cMA_CONFIG, xBuffer, cLxBinBufferSize, 0);

      // nach XML konvertieren
      with TMMXMLConverter.Create do
      try
        mTempXMLString         := '';
        OnAfterDeleteElements := Bin2XMLMaConfigAfterDelete;
        OnBeforeDeleteElements := Nil;
        OnCalcExternal := Nil;
        // F�r den Bauzustand d�rfen keine Elemente gel�scht werden
        // es werden vollwertige Dokumente erstellt. Ben�tigtes Fragment wird in OnAfterConvert ausgelesen
        aXMLData := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msMachine, False);
//          aXMLData := aXMLData + mTempXMLString;
        Result := True;
      finally
        Free;
      end; //with
    finally
      FreeMem(xBuffer);
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)
end;

(*---------------------------------------------------------
  Holt die Sektionen von der LX. Da in der Zentrale Settings und Bauzustand vermischt werden,
  muss der Bauzustand konvertiert werden. Einige Elemente m�ssen von den Settings in den
  Bauzustand transferiert werden.
  Dies kann geschehen indem im Config Map die entsprechenden Elemente mit dem Datentyp
  dtIgnore versehen werden und in den Settings diese Elemente geholt werden. Der Konverter wird f�r die
  Elemente mit dem Datentyp dtIgnore jeweils ein "leeres" Element erstellen (zB. <CutRetries />).
  Diese "leeren" Elemente werden aus dem Setting in den Bauzustand kopiert.
----------------------------------------------------------*)
function TLXMachThread.UploadAndConvertSections(var aXMLData: String; aLXGroup:
    Integer): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
  xXMLData: string;
  xSettingsGroupNode: IXMLDOMNode;
  xSettingsDom: DOMDocumentMM;
  xMaConfigDOM: DOMDocumentMM;
  xMaConfigXML: string;
  xMaConfigGroupNode: IXMLDOMNode;
  i: Integer;
  xSelPath: string;
  xTempNode: IXMLDOMNode;
  xNodeList: IXMLDOMNodeList;
//  xLXGroup: Integer;
  xXMLConverter: TMMXMLConverter;
  xMaAssignment: TLxMaAssignmentRec;
  xLXSect: Integer;
  xGrpAssignment: TLxGrpAssignmentRec;
  xFound: Boolean;
  xSpindleFrom: Integer;
  xSpindleTo: Integer;
const
  cSelectNullValues = './/*[(count(child::*)=0) and (not(text()))]';
begin
  Result   := False;
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    xBuffer       := AllocMem(cLxBinBufferSize);
    xXMLConverter := TMMXMLConverter.Create;
    try
      Fillchar(xMaAssignment,sizeof(xMaAssignment),0);
      //Holen Maschinensettings
      mLxDevHandler.GetData(cMA_ASSIGNMENT,@xMaAssignment,Sizeof(TLxMaAssignmentRec),0);
      if mDebugEnabled then
        CodeSite.SendStringEx(csmIndigo, Format('GetData cMA_ASSIGNMENT: Groups:%d, Sections:%d, Spindles:%d',
          [xMaAssignment.GroupCount, xMaAssignment.SectionCount, xMaAssignment.SpindleCount]),'');

////      for xLXGroup:=aLXGroup to xMaAssignment.GroupCount {alt cLXSpdGroupLimit-1} do begin
        // GrpSetting aus MachGruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
        Result := mLxDevHandler.GetData(cGRP_SETTINGS, xBuffer, cLxBinBufferSize, aLXGroup+1);
        if Result then begin
          // nach XML konvertieren
          with xXMLConverter do begin
            xXMLData := '';
            // nach XML konvertieren
            // Exceptions werden ganz unten pauschal abgefangen
            IgnoreDeleteFlag       := True;
            OnCalcExternal         := nil;
            OnBeforeDeleteElements := nil;
            OnAfterDeleteElements  := nil;
            xXMLData := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msYMSetting, False);
            xSettingsDom := XMLStreamToDOM(xXMLData);
            xSettingsGroupNode := xSettingsDom.selectSingleNode(cXPGroupNode);

            // GRP_ASSIGNMENT DataItem-Record lesen f�r Spindelbereich
            Fillchar(xGrpAssignment,sizeof(xGrpAssignment),0);
            Result := mLxDevHandler.GetData(cGRP_ASSIGNMENT,@xGrpAssignment,Sizeof(TLxGrpAssignmentRec), aLXGroup + 1);
            if mDebugEnabled then
              CodeSite.SendStringEx(csmIndigo, Format('GetData cGRP_ASSIGNMENT: GrpSpdLast:%d, GrpSpdFirst:%d',
               [xGrpAssignment.SpindleFirst,xGrpAssignment.SpindleLast]),'');

            //Wenn regul�re Gruppen, wird die cCLR_SECTION gesucht und gelesen; Bei Memories (haben Spdl.Range 0-0) nicht
            if (Result) and (xGrpAssignment.SpindleFirst > 0) and (xGrpAssignment.SpindleLast > 0) then begin
              xLXSect := 1;
              xFound := False;
              //Suchen in welcher Sektion die Spindeln der Gruppe liegen
              while (xLXSect <= xMaAssignment.SectionCount) and (xFound = False)do begin
                // Maschinen Gruppen aus MachGruppe in den Buffer lesen
                Result := mLxDevHandler.GetData(cCLR_SECTION, xBuffer, cLxBinBufferSize, xLXSect);
                mTempXMLString         := '';
                OnAfterDeleteElements := Bin2XMLMaConfigAfterDelete;
                OnBeforeDeleteElements := Nil;
                OnCalcExternal := Nil;
                // F�r den Bauzustand d�rfen keine Elemente gel�scht werden
                xMaConfigXML := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msMaConfig, False);

                if mDebugEnabled then
                  CodeSite.SendStringEx(csmIndigo, Formatxml(xMaConfigXML),'');

                xMaConfigDOM := XMLStreamToDOM(xMaConfigXML);
                xMaConfigGroupNode := xMaConfigDOM.selectSingleNode(cXPGroupNode);
                // Wenn dies eine Gruppe mit g�ltigen Spindelbereich ist, dann fortfahren
                xSpindleFrom := GetElementValueDef(xMaConfigGroupNode.selectSingleNode('SpindleFrom'), '0');
                xSpindleTo := GetElementValueDef(xMaConfigGroupNode.selectSingleNode('SpindleTo'), '0');
                  if mDebugEnabled then
                    CodeSite.SendStringEx(csmIndigo, Format('GetData cCLR_SECTION: GrpSpdLast:%d, GrpSpdFirst:%d, SectSpdFirst:%d, SectSpdLast:%d',
                     [xGrpAssignment.SpindleFirst,xGrpAssignment.SpindleLast, xSpindleFrom, xSpindleTo]),'');
                if (xSpindleFrom <= xGrpAssignment.SpindleFirst) and (xGrpAssignment.SpindleFirst <= xSpindleTo) then begin
                  xFound := True;
                end;
                Inc(xLXSect);
              end;

              // Selektiert alle "leeren" Elemente
              xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
              for i := 0 to xNodeList.length - 1 do begin
                xSelPath := GetSelectionPath(xNodeList[i]);
                xTempNode := xSettingsDom.SelectSingleNode(xSelPath);
                if Assigned(xTempNode) then
                  xNodeList[i].Text := xTempNode.text;
              end;// for i := 0 to xNodeList.length - 1 do begin

              // L�scht alle noch �brig gebliebenen "leeren" Elemente
              xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
              for i := 0 to xNodeList.length - 1 do
                xNodeList[i].parentNode.removeChild(xNodeList[i]);
              mTempXMLString := xMaConfigDOM.SelectSingleNode(cXPConfigNode).xml;

              aXMLData   := aXMLData + mTempXMLString;
              // Start von n�chtem Sektionenindex f�r n�chste Gruppenabfrage merken
              mLXSection := aLXGroup;
            end; // if (Result)
          end; //with
        end;// if Result then begin
//        // Wenn alles OK, dann Schlaufe abbrechen, da diese Sektion gelesen wurde
////        if Result then
////          Break;
////      end; // for xLXGroup
    finally
      FreeMem(xBuffer);
      xXMLConverter.Free;
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)
end;

// Bis 27.9.06 generell und fortan nurr noch f�r Call aus Bereich jtSaveMaConfigToDB
function TLXMachThread.UploadAndConvertSectionsAlt(var aXMLData: String; aLXSection: Integer): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
  xXMLData: string;
  xSettingsGroupNode: IXMLDOMNode;
  xSettingsDom: DOMDocumentMM;
  xMaConfigDOM: DOMDocumentMM;
  xMaConfigXML: string;
  xMaConfigGroupNode: IXMLDOMNode;
  i: Integer;
  xSelPath: string;
  xTempNode: IXMLDOMNode;
  xNodeList: IXMLDOMNodeList;
  xLXSection: Integer;
  xInt: Integer;
  xXMLConverter: TMMXMLConverter;
const
  cSelectNullValues = './/*[(count(child::*)=0) and (not(text()))]';
begin
  Result   := False;
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    xBuffer       := AllocMem(cLxBinBufferSize);
    xXMLConverter := TMMXMLConverter.Create;
    try
      for xLXSection:=aLXSection to cLXSpdGroupLimit-1 do begin
        // Maschinen Gruppen aus MachGruppe in den Buffer lesen
        Result := mLxDevHandler.GetData(cCLR_SECTION, xBuffer, cLxBinBufferSize, xLXSection+1);
        if Result then begin
          // nach XML konvertieren
          with xXMLConverter do begin
            mTempXMLString         := '';
            OnAfterDeleteElements := Bin2XMLMaConfigAfterDelete;
            OnBeforeDeleteElements := Nil;
            OnCalcExternal := Nil;
            // F�r den Bauzustand d�rfen keine Elemente gel�scht werden
            xMaConfigXML := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msMaConfig, False);
            xMaConfigDOM := XMLStreamToDOM(xMaConfigXML);
            xMaConfigGroupNode := xMaConfigDOM.selectSingleNode(cXPGroupNode);

            // Wenn dies eine Gruppe mit g�ltigen Spindelbereich ist, dann fortfahren
            xInt := GetElementValueDef(xMaConfigGroupNode.selectSingleNode('SpindleFrom'), '0');
            if xInt > 0 then begin
              xXMLData := '';
              // GrpSetting aus MachGruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
              mLxDevHandler.GetData(cGRP_SETTINGS, xBuffer, cLxBinBufferSize, xLXSection+1);

              // nach XML konvertieren
              // Exceptions werden ganz unten pauschal abgefangen
              IgnoreDeleteFlag       := True;
              OnCalcExternal         := nil;
              OnBeforeDeleteElements := nil;
              OnAfterDeleteElements  := nil;
              xXMLData := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msYMSetting, False);
              xSettingsDom := XMLStreamToDOM(xXMLData);
              xSettingsGroupNode := xSettingsDom.selectSingleNode(cXPGroupNode);

              // Selektiert alle "leeren" Elemente
              xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
              for i := 0 to xNodeList.length - 1 do begin
                xSelPath := GetSelectionPath(xNodeList[i]);
                xTempNode := xSettingsDom.SelectSingleNode(xSelPath);
                if Assigned(xTempNode) then
                  xNodeList[i].Text := xTempNode.text;
              end;// for i := 0 to xNodeList.length - 1 do begin

              // L�scht alle noch �brig gebliebenen "leeren" Elemente
              xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
              for i := 0 to xNodeList.length - 1 do
                xNodeList[i].parentNode.removeChild(xNodeList[i]);
              mTempXMLString := xMaConfigDOM.SelectSingleNode(cXPConfigNode).xml;

              aXMLData   := aXMLData + mTempXMLString;
              // Start von n�chtem Sektionenindex f�r n�chste Gruppenabfrage merken
              mLXSection := xLXSection;
            end else // if xInt > 0
              Result := False;
          end; //with
        end;// if Result then begin
        // Wenn alles OK, dann Schlaufe abbrechen, da diese Sektion gelesen wurde
        if Result then
          Break;
      end; // for xLXSection
    finally
      FreeMem(xBuffer);
      xXMLConverter.Free;
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)
end;

(*---------------------------------------------------------
  Holt die Sektionen von der LX. Da in der Zentrale Settings und Bauzustand vermischt werden,
  muss der Bauzustand konvertiert werden. Einige Elemente m�ssen von den Settings in den
  Bauzustand transferiert werden.
  Dies kann geschehen indem im Config Map die entsprechenden Elemente mit dem Datentyp
  dtIgnore versehen werden und in den Settings diese Elemente geholt werden. Der Konverter wird f�r die
  Elemente mit dem Datentyp dtIgnore jeweils ein "leeres" Element erstellen (zB. <CutRetries />).
  Diese "leeren" Elemente werden aus dem Setting in den Bauzustand kopiert.
----------------------------------------------------------*)
function TLXMachThread.UploadAndConvertSectionsBak(var aXMLData: String; aLXSection: Integer): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
  xXMLData: string;
  xSettingsGroupNode: IXMLDOMNode;
  xSettingsDom: DOMDocumentMM;
  xMaConfigDOM: DOMDocumentMM;
  xMaConfigXML: string;
  xMaConfigGroupNode: IXMLDOMNode;
  i: Integer;
  xSelPath: string;
  xTempNode: IXMLDOMNode;
  xNodeList: IXMLDOMNodeList;

const
  cSelectNullValues = './/*[(count(child::*)=0) and (not(text()))]';
begin
  Result   := False;
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    xBuffer := AllocMem(cLxBinBufferSize);
    try
      // Maschinen Gruppen aus MachGruppe in den Buffer lesen
      Result := mLxDevHandler.GetData(cCLR_SECTION, xBuffer, cLxBinBufferSize, aLXSection+1);

      if Result then begin
        // nach XML konvertieren
        with TMMXMLConverter.Create do try
          mTempXMLString         := '';
          OnAfterDeleteElements := Bin2XMLMaConfigAfterDelete;
          OnBeforeDeleteElements := Nil;
          OnCalcExternal := Nil;
          // F�r den Bauzustand d�rfen keine Elemente gel�scht werden
          xMaConfigXML := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msMaConfig, False);
          xMaConfigDOM := XMLStreamToDOM(xMaConfigXML);
          xMaConfigGroupNode := xMaConfigDOM.selectSingleNode(cXPGroupNode);

          xXMLData := '';
          // GrpSetting aus MachGruppe in den Buffer lesen (Im System: Gruppe 0-basiert -- LX 1-basiert)
          mLxDevHandler.GetData(cGRP_SETTINGS, xBuffer, cLxBinBufferSize, aLXSection + 1);

          // nach XML konvertieren
          // Exceptions werden ganz unten pauschal abgefangen
          IgnoreDeleteFlag       := True;
          OnCalcExternal         := nil;
          OnBeforeDeleteElements := nil;
          OnAfterDeleteElements  := nil;
          xXMLData := BinToXML(xBuffer, cLxBinBufferSize, xMapfile, msYMSetting, False);
          xSettingsDom := XMLStreamToDOM(xXMLData);
          xSettingsGroupNode := xSettingsDom.selectSingleNode(cXPGroupNode);

          // Selektiert alle "leeren" Elemente
          xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
          for i := 0 to xNodeList.length - 1 do begin
            xSelPath := GetSelectionPath(xNodeList[i]);
            xTempNode := xSettingsDom.SelectSingleNode(xSelPath);
            if Assigned(xTempNode) then
              xNodeList[i].Text := xTempNode.text;
          end;// for i := 0 to xNodeList.length - 1 do begin

          // L�scht alle noch �brig gebliebenen "leeren" Elemente
          xNodeList := xMaConfigGroupNode.SelectNodes(cSelectNullValues);
          for i := 0 to xNodeList.length - 1 do
            xNodeList[i].parentNode.removeChild(xNodeList[i]);
          mTempXMLString := xMaConfigDOM.SelectSingleNode(cXPConfigNode).xml;

          aXMLData := aXMLData + mTempXMLString;
        finally
          Free;
        end; //with
      end;// if Result then begin
    finally
      FreeMem(xBuffer);
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)
end;

end.

