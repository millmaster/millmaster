unit LXHandlerDef;

interface

uses sysutils;

type
  // Basisklasse f�r die Exceptions des LX Handler
  ELXException = class(Exception);
  (* Exception f�r einen Buffer Size Error.
     Diese Exception wird ausgel�st wenn f�r die Abfrage eines Dataitems ein zu kleiner
     Buffer zur Verf�gung gestellt wurde.*)
  EBufferSizeException = class(ELXException);

implementation

end.
