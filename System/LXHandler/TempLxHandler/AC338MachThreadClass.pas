(*==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: AC338MachThreadClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 28.03.2000  0.00  khp | Test basedata.length:= 65000; basedata.watchTime:= 1000; entfernt am 10.4 khp
// 06.04.2000  0.00  khp | @1 ProdGrpId immer aus Grp-Settings, nie aus Deklarationsrecord lesen
// 10.04.2000  0.00  khp | Funktionen ConvertSettingsFromAC338 und  ConvertSettingsToAC338 fuer
//                       | Anpassungen an den Settingsdaten eingefuegt und  ProdGrpName behandelt.
//                       | Betroffene Proceduren: Get- SetSettings,Set- GetMaConfig, GetSettingsAllroup,
//                       | Inform: Grp_Settings Declarations eStart, eStop
// 11.04.2000  0.00  khp | Allignment-Problematik (C, DLL) mit xUSBuffer temporaer ueberbrueckt.
// 12.04.2000  0.00  nue | KorrekturHack temporaer!!!!!!!!!!.
// 18.04.2000  0.00  nue | Anpassungen an die Jobstrukturen wegen Einfuegen von ProdGrpInfo!.
// 04.05.2000  0.00  khp | Hack entfernt,Allignment-Problematik in Get_Data/Put_Data geloest,
//                       | Methoden ExtractProdInfo, BuildGrpNoString fuer Order, Orderpos eingefuegt
// 25.05.2000  0.00  khp | Temporaer: GrpBereich und GrpStatus in GetMaAsign von WSC DB lesen
// 06.06.2000  0.00  khp | GetSettingsAllGroups: wurde entfernt
// 13.06.2000  0.00  khp | ConnectingMachine:Falls DataTransaction aktiv, dann abbrechen, Rollbacklaeuft
// 16.06.2000  0.00  khp | Assign Complete nur wenn in einer MaschGrp ProdId=0
// 25.06.2000  0.00  khp | Keine Zugriffe auf WSC Informator Datenbank. Mittels SetMMPartieName,SetMMPartieBereich,
//                       | SetMMPartieNummer wird auf PARTIENAME, PARTIENUMMER, PARTIEBEREICH zugeriffen.
//                       | Schlupf
// 26.06.2000  0.00  khp | ExtractandFillProdInfo und BuildPartieNoString ersetzen ExtractProdInfo, BuildGrpNoString
// 28.06.2000  0.00  khp | DelDecl wird nur nach EntryLocked und in GetMaAssign aufgerufen.
//                       | Test ob Aenderung erfolgten waehrend Offline zwischen zwei Lifechecks
//                       | ist nicht mehr noetig, Abgleich erfolgt bei KeyLocked durch MaAssign.
//                       | Schlupf darf nicht von WSC gelesen werden daher Schlupf in PartieNr einbinden. Der Wertebereich
//                       | wird geprueft,Kommas werden in Punkte konvertiert,bei Fehler wird Defaultschlupf 1.000 gesetzt.
//                       | Syntax PartieNr: MM-OrderID-Orderposition-Schlupf  MM-1-1-1.007
// 08.08.2000  0.00  khp | Timer (cMachstatusTicker) fuer Machstatus eingefuehrt, welcher den Status alle 60 sekunden sendet
// 12.09.2001  1.00  khp | Informator V5.3x: ExtractandFillProdInfo: MM_PARTIENAME(tText100) wird auf tText50 gekuerzt und
//                       | aProdGrpInfo.c_prod_name zugewiesen. Bei jtSetMaConfig,SetSettings umcopieren von tText50 auf tText100
// 14.09.2001  1.01  khp | Behandlung von YMSetParameter in GetSettings,eStart,ePreSettings mit proc. GetYMSetParameter
//                       | jtSetSettings mit proc. SetYMSetParmeter. Exceptions werden gehandelt und mit CodeSide dokumentiert
// 12.12.2001  1.02  khp | Mittels SetMMPartieandArticleparam und GetMMPartieandArticleparam werden die Artikel und Partiewerte
//                       | auf den Informator gespeichert. PartieNr hat nur noch informativen Charakter
// 31.01.2002  1.03  khp | Bei Veraenderungen an einer laufenden Partei muss bei eEntryLocked YM_set_name mit GetYMSetParm geholt werden
// 28.02.2002  1.04  khp | jtSetSettings,jtSetMaConfig Garnummer fuer 338 DB immer auf Nm-float konvertieren
// 15.03.2002  1.05  khp | Anpassung von Partiename auf DB auf TText100 (Chinesisch UTF8)
// 29.05.2002  1.06  khp | jtSetSettings:SetWSCPartieNummer wird nur ausgefuehrt wenn Gruppe nicht inProduktion ist
// 05.06.2002  1.07  khp | In SetMMPartieandArticleparam wird YM-Partienummer wenn Gruppe inProduktion nicht mehr mit 'MM' beschrieben.
// 08.10.2002  1.08  khp | Informatorversion L5.61 (Zenit).FFDataBright-Matrix in Definition und TInformThread.InformDispatcher, TInformThread.MMJobDispatcher
//                       | eingef�gt.Diese Erweiterung ist prov. die vollst�ndige Implementation der Zenit Erweiterungen wird sp�ter erfolgen.
// 20.11.2002  1.09      | AcqTimeout:=100000 (100s) anstatt 60s, Stop Timer nach Event MM_Data_Ready
// 04.08.2003  1.10  nue | Bei Errormeldung in  TInformThread.MMJobDispatcher Format(' xIndex:%d ',[xIndex]) added.
//                       | Korrektur damit Daten von Informator V6.0 (Color PowerPC) Platz haben. (cFixSizeTAC338declaration)
// 05.11.2003  1.11  khp | Abfrage der Informatorversion und Aufruf der Konvertierungsfunktion ParamConverttoMM oder ..toInf aus der DLL AC338RPC
//                       | vor Up und Download der YMSettings. Aufruf der Konvertierungsfunktion DeclConverttoMM. R�ckbau der Korrektur von V 1.10
// 03.03.2005  1.11  Wss | Umbau nach 0-basiertes Gruppen/Settingshandling innerhalb MM-System
// 28.04.2005  1.11  Wss | Ermitteln des Mapfiles erfolgt gem�ss Informator Version nach WSC-01xx,02xx,03xx. Dabei wird
                           immer das Mapfile geladen mit dem h�chsten SubIndex
// 29.04.2005  1.11  Wss | Nach Partie Stop wird modify Flag der Gruppe zur�ckgesetzt
//=========================================================================================*)


unit AC338MachThreadClass;

interface

uses
  Windows, Classes, Dialogs, Messages, Forms, Sysutils, extctrls,
  AC338INFClass, AC338DLL_DEF, BaseGlobal, YMDataDef, YMParaUtils, YMParaDef, MMUGlobal,
  BaseThread, mmEventLog, MMXMLConverter, MSXML2_TLB;

const
  cMMJobRequestMsg      = 'MMJobRequestMsg';
  cAcqTimerID           = 100;
  cLifeCheckTimerID     = 101;
  cMachstatusTickerID   = 102;
  cAcqTimeout           = 100000;  // ms Timeout beim Acquisation   60s neu 100s
  cLifeCheckTimeout     = 30000;
  cMachstatusTickertime = 60000;
  cPartieNoHeaderStr    = 'MM';
//  cPartieNoDelimiterStr = '-';
//  cStringListDelimiter  = ',';
//  cComma                = ',';
//  cDot                  = '.';

//  cYMMapIDOldVersion    = 'WSCYM001';
//  cMCMapIDOldVersion    = 'WSCMC001';
//  cYMMapIDNewVersion    = 'WSCYM002';
//  cMCMapIDNewVersion    = 'WSCMC002';


type
  TWSCDaten = packed record
    baseData: TBaseData;
    klassierdaten: packed record
      defectData: TWSCDefectData;
      FFDataDark:   TWSCSiroData;
      FFDataBright: TWSCSiroData; // Neu f�r Informator L 5.61
    end;
  end;

  TWriteToMsgHnd = procedure(aPJob: PJobRec) of object;
  TSetMachstatus = procedure(aMachID: Integer; aMachstatus: TWscNodeState) of object;
  TWriteToLog    = procedure(aEvent: TEventType; aText: string) of object;

  // Forward Deklaration von TInformThread;
  TInformThread = class;

  TNodeListType = record
    MaschineThread: TInformThread;
    WscNetNodeRec: TWscNetNodeRec;
  end;


  TNodeList = class(TObject)
  PRIVATE
    fLastMachpos: integer;
//TODO wss: hier wird ein statisches Array von 101 [0..100] Elementen erstellt. Warum 101?
    fNodeList: array[cFirstMachPos..cMaxWscMachine] of TNodeListType;
    mMapfileList: TStringList;
    mMapfileSync: TMultiReadExclusiveWriteSynchronizer;
    function GetIpAdresse(aMachID: Integer): String;
    function GetIsMapOverruled(aMachID: Integer): Boolean;
    function GetMapfile(aMapID: String): String;
    function GetMapfileDOM(aMapID: String): DOMDocument40;
    function GetMapfileAvailable(aMapID: String): Boolean;
    function GetMapID(aMachID: Integer): String;
    procedure ReadMapfilesFromDB(aMapnames: String);
    procedure SetMapfile(aMapID: String; const Value: String);
    procedure SetMapID(aMachID: Integer; const Value: String);
    property Mapfile[aMapID: String]: String read GetMapfile write SetMapfile;
  PROTECTED
  PUBLIC
    property IpAdresse[aMachID: Integer]: String read GetIpAdresse;
    property IsMapOverruled[aMachID: Integer]: Boolean read GetIsMapOverruled;
    property LastMachpos: Integer READ fLastMachpos;
    property MapfileDOM[aMapID: String]: DOMDocument40 read GetMapfileDOM;
    property MapfileAvailable[aMapID: String]: Boolean read GetMapfileAvailable;
    property MapID[aMachID: Integer]: String read GetMapID write SetMapID;
    constructor Create;
    destructor Destroy; Override;
    procedure SetNodeList(const aNodeList: TWscNetNodeList);
    procedure GetNodeList(var aNodeList: TWscNetNodeList);
    function GetMachstate(aIpdress: Pchar; aMachID: Integer): TWscNodeState;
    procedure SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TWscNodeState);
    procedure StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
    function GetMachinethread(aIpdress: Pchar; aMachID: Integer): TInformThread;
    procedure GetMapfileFromVersion(aMachID: Integer; aVersion: Integer);
    procedure StopMachinethread();
  end;

  TInformThread = class(TThread)
  private
    mJobRequestMsg: UINT;
    mPJob: PJobRec;
    mNodeList: TNodeList;
    mMachID: Integer;
    mInformatorVersion: tText50;
    mIsZenit: Boolean;
    mTempXMLSettings: TXMLSettingsRec;
    mThreadIpAdresse: PChar;
    mWriteToLog: TWriteToLog;           // Methoden Referenz zu WriteLog in WSCWriterClass
    mWriteToMsgHnd: TWriteToMsgHnd;     // Methoden Referenz zu WriteJobBufferTo in WSCWriterClass
    mWSCHandler: TWscDevHandler;
    mWSCInformMsg: Integer;
    mTempXMLString: String;
  //...............................................................
    procedure DeleteWSCDecl(aGrpNo: Integer);
    function FTPGetMapfile(aIPAdress, aMapname: String; var aMapfile: String): Boolean;
//    procedure SetMMPartieandArticleparam(aProdGrpInfo: TProdGrpInfo;aInProduktion: Boolean; aGrpNo: Integer; aColor: integer);
    procedure SetMMPartieandArticleparam(const aProdGrpInfo: TXMLSettingsRec; aInProduktion: Boolean; aGrpNo: Integer);
    procedure GetMMPartieandArticleparam(var aProdGrpInfo: TXMLSettingsRec; aGrpNo: Integer);
    procedure SetYMSetParm(aYM_Set_Name: tText50; aYM_set_id: Word; aProdGrp: Integer);
    procedure GetYMSetParm(var aYM_Set_Name: tText50; var aYM_set_id: Word; var aYM_Set_Changed: Boolean; aProdGrp: Integer);
    procedure RemoveConfigCode(const aMMXMLDoc: DOMDocument40);
  //......................................................
    function UploadAndConvertXMLSettings(var aXMLData: String; aWSCGroup: Integer; aIgnoreDeleteFlag: Boolean): Boolean;
  protected
    procedure Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure Bin2XMLMaConfigAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure CleanDataAcqQueue();
    function ConnectingMachine(): Boolean;
    procedure Bin2XMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    procedure Bin2XMLSettingsAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    function DisConnectingMachine(): Boolean;
    procedure Execute; override;
    procedure InformDispatcher(aMsg: TMsg);
    procedure MMJobDispatcher(aMsg: TMsg);
    function SendMachinenstatus(): Boolean;
    procedure XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
  public
    mThreadWndHandle: HWND;             // Window Handle fuer Thread
    constructor Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
      const aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
//    constructor Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
//      const aSetMachstatus: TSetMachstatus; const aWriteToLog: TWriteToLog);
    destructor Destroy; OVERRIDE;
    function SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
  end;




implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS,
  XMLDef, XMLMappingDef, AdoDBAccess, LoepfeGlobal, IdFTP, IdFTPCommon,
  BZIP2, XMLGlobal, activex, typinfo, xmlMaConfigClasses, FPatternClasses;

type
  LifecheckEx = class(Exception);
  WSCAccessException = class(Exception);

const
  WM_MMQuit = WM_USER + 1;

//==============================================================================
// CLASS TNodeList
//==============================================================================
constructor TNodeList.Create;
begin
  inherited Create;
  fLastMachpos  := -1;
  mMapfileList  := TStringList.Create;
  mMapfileSync := TMultiReadExclusiveWriteSynchronizer.Create;
end;
//:-----------------------------------------------------------------------------
destructor TNodeList.Destroy;
var
  i: Integer;
  xIntf: DOMDocument40;
begin
  for i:=0 to mMapfileList.Count-1 do begin
    xIntf := DOMDocument40(Pointer(mMapfileList.Objects[i]));
    xIntf := Nil;
  end;

  mMapfileSync.Free;
  mMapfileList.Free;
  inherited Destroy;
end;

function TNodeList.GetIpAdresse(aMachID: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].WscNetNodeRec.IpAdresse;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetIsMapOverruled(aMachID: Integer): Boolean;
var
  i: Integer;
begin
  Result := False;
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].WscNetNodeRec.Overruled;
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetNodeList(const aNodeList: TWscNetNodeList);
var
  i: Integer;
  xMapIDList: TStringList;
begin
  // Clean fNodeList
  xMapIDList := TStringList.Create;
  xMapIDList.Sorted     := True;
  xMapIDList.Duplicates := dupIgnore;
  try
    for i := cFirstMachPos to cMaxWscMachine do begin
      if aNodelist[i].MachID <> 0 then begin
        fNodeList[i].WscNetNodeRec := aNodeList[i];
        if aNodeList[i].MapName <> '' then
          xMapIDList.Add(Format('''%s''', [aNodeList[i].MapName]));
        fLastMachpos := i;
      end
      else begin
        fNodeList[i].WscNetNodeRec.IpAdresse := '';
        fNodeList[i].WscNetNodeRec.MachID    := 0;
        fNodeList[i].WscNetNodeRec.Overruled := False;
        fNodeList[i].WscNetNodeRec.MapName     := '';
        fNodeList[i].WscNetNodeRec.machstate := nsNotValid;
        fNodeList[i].MaschineThread          := nil;
      end;
    end;
  finally
    if xMapIDList.Count > 0 then
      ReadMapfilesFromDB(xMapIDList.CommaText);
    xMapIDList.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.GetNodeList(var aNodeList: TWscNetNodeList);
var
  i: integer;
begin
  // Copy fNodeList to aNodelist
  for i := cFirstMachPos to cMaxWscMachine do 
    aNodeList[i] := fNodeList[i].WscNetNodeRec;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMachstate(aIpdress: Pchar; aMachID: Integer): TWscNodeState;
var
  i: integer;
begin
  result := nsOffline;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].WscNetNodeRec.machstate;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].WscNetNodeRec.machstate;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.SetMachstate(aIpdress: Pchar; aMachID: Integer; aMachstatus: TWscNodeState);
var
  i: integer;
begin
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        fNodelist[i].WscNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
        fNodelist[i].WscNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.StartMachineThread(aWriteToMsgHnd: TWriteToMsgHnd; aWriteToLog: TWriteToLog);
var
  i: integer;
begin
  for i := cFirstMachPos to LastMachpos do begin
    with fNodeList[i].WscNetNodeRec do
      fNodeList[i].MaschineThread := TInformThread.Create(IpAdresse, MachID, aWriteToMsgHnd, Self, aWriteToLog);
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMachinethread(aIpdress: Pchar; aMachID: Integer): TInformThread;
var
  i: integer;
begin
  result := nil;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
end;
//:-----------------------------------------------------------------------------
procedure TNodeList.StopMachinethread();
var
  i: integer;
begin
  for i := cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].MaschineThread <> nil then begin
      fNodelist[i].MaschineThread.Suspend;
//      fNodelist[i].MaschineThread.Terminate;
      fNodelist[i].MaschineThread.Free;
//      fNodelist[i].MaschineThread.Terminate;
//      SendMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
    end;
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMapfile(aMapID: String): String;
begin
  mMapfileSync.BeginRead;
  with TStringList.Create do try
    CommaText := mMapfileList.Values[aMapID];
    Result := Text;
  finally
    Free;
    mMapfileSync.EndRead;
  end;
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetMapfileDOM(aMapID: String): DOMDocument40;
var
  xIndex: Integer;
begin
  Result := Nil;
  mMapfileSync.BeginRead;
  try
    xIndex := mMapfileList.IndexOf(aMapID);
    if xIndex >= 0 then begin
      Result := DOMDocument40(Pointer(mMapfileList.Objects[xIndex]));
      if not Assigned(Result) then begin
        WriteToEventLog('No DOM for MapID found ' + aMapID, 'TMsgPoolList: ', etError);
{
        Result := CoDOMDocument40.Create;
        Result.setProperty('NewParser', true);
//        Result.loadXML(GetMapfile(aMapID));
        if Result.loadXML(mMapfileList.Values[aMapID]) then
          mMapfileList.Objects[xIndex] := Pointer(Result)
        else
          Result := Nil;
{}
      end;
    end;
  finally
    mMapfileSync.EndRead;
  end;
end;
//:-----------------------------------------------------------------------------
function TNodeList.GetMapfileAvailable(aMapID: String): Boolean;
var
  xIndex: Integer;
begin
  Result := False;
  mMapfileSync.BeginRead;
  try
//    Result := (mMapfileList.Values[aMapID] <> '');
    xIndex := mMapfileList.IndexOf(aMapID);
    if xIndex >= 0 then
      Result := Assigned(mMapfileList.Objects[xIndex]);
  finally
    mMapfileSync.EndRead;
  end;
end;

//:-----------------------------------------------------------------------------
function TNodeList.GetMapID(aMachID: Integer): String;
var
  i: Integer;
begin
  Result := '';
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
      Result := fNodelist[i].WscNetNodeRec.MapName;
      Break;
    end
  end
end;

procedure TNodeList.GetMapfileFromVersion(aMachID: Integer; aVersion: Integer);
const
//  cQuery = 'SELECT c_mapfile_name ' +
//           'FROM t_xml_mapfile ' +
//           'WHERE c_mapfile_id = :MapID';
  cQuery =
    ' select c_mapfile_name from t_xml_mapfile ' +
    ' where c_mapfile_name like ''WSC-%s'' ' +
    ' order by c_mapfile_name desc';

var
  xMapfilename: string;
begin
  with TAdoDBAccess.Create(1) do
  try
    Init;
    with Query[0] do begin
      SQL.Text := cQuery;
      if aVersion >= 620      then SQL.Text := Format(cQuery, ['03%'])  // V6.20 - ...
      else if aVersion <= 609 then SQL.Text := Format(cQuery, ['01%'])  // ...   - V6.09
                              else SQL.Text := Format(cQuery, ['02%']); // V6.10 - V6.19
//      SQL.Text := cQuery;
//      if aVersion >= 620      then ParamByName('MapID').AsInteger   := 5
//      else if aVersion <= 609 then ParamByName('MapID').AsInteger   := 3
//                              else ParamByName('MapID').AsInteger   := 4;
      Open;
      if FindFirst then begin
        xMapfilename := FieldByName('c_mapfile_name').AsString;
        // Mapname nur einf�gen, wenn noch nicht in Liste vorhanden ist
        if not AnsiSameText(MapID[aMachID], xMapfilename) then begin
          CodeSite.SendFmtMsg('GetMapfileFromVersion: MachID: %d, Mapfile: %s', [aMachID, xMapfilename]);
          SetMapID(aMachID, xMapfilename);
        end;
      end;
    end;
  finally
    Free;
  end;

//  Result := IsMapOverruled[aMachID];
//  if not Result then
//    with TAdoDBAccess.Create(1) do
//    try
//      Init;
//      with Query[0] do begin
//        SQL.Text := cQuery;
//        ParamByName('NetID').AsInteger   := ORD(ntWSC);
//        ParamByName('Version').AsInteger := VersionToInt(aVersion);
//        Open;
//        Result := FindFirst;
//        if Result then begin
//          xMapfilename := FieldByName('c_mapfile_name').AsString;
//          if not AnsiSameText(MapID[aMachID], xMapfilename) then begin
//            CodeSite.SendFmtMsg('GetMapfileFromVersion: MachID: %d, Mapfile: %s', [aMachID, xMapfilename]);
//            SetMapID(aMachID, xMapfilename);
//          end;
//        end;
//      end;
//    finally
//      Free;
//    end;
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.ReadMapfilesFromDB(aMapnames: String);
const
  cQrySelectMapfiles = 'select c_mapfile_name, c_mapfile from t_xml_mapfile ' +
                       'where c_mapfile_name in (%s)';
var
  xDOM: DOMDocument40;
begin
  CodeSite.SendString('Read mapfile for Mapnames', aMapnames);
  if aMapnames > '' then begin
    mMapfileSync.BeginWrite;
    with TAdoDBAccess.Create(1) do
    try
      Init;
      with Query[0] do begin
        SQL.Text := Format(cQrySelectMapfiles, [aMapnames]);
        Open;
        if FindFirst then
          while not EOF do begin
            xDOM := XMLStreamToDOM(FieldByName('c_mapfile').AsString);
            mMapfileList.AddObject(FieldByName('c_mapfile_name').AsString, Pointer(xDOM));
            // verhindert, dass das Interface wieder freigegeben wird
            Pointer(xDOM) := Nil;
            Next;
          end;
      end;
    finally
      Free;
      mMapfileSync.EndWrite;
    end;
  end;// if aMapnames > '' then begin
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapfile(aMapID: String; const Value: String);
var
  xDOM: DOMDocument40;
begin
  mMapfileSync.BeginWrite;
  try
    xDOM := XMLStreamToDOM(Value);
    mMapfileList.AddObject(aMapID, Pointer(xDOM));
    // verhindert, dass das Interface wieder freigegeben wird
    Pointer(xDOM) := Nil;
  finally
    mMapfileSync.EndWrite;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TNodeList.SetMapID(aMachID: Integer; const Value: String);
var
  i: Integer;
begin
  for i:=cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].WscNetNodeRec.MachID = aMachID then begin
      fNodelist[i].WscNetNodeRec.MapName := Value;
      if not MapfileAvailable[Value] then
        ReadMapfilesFromDB(Format('''%s''', [Value]));
      Break;
    end
  end
end;

//:-----------------------------------------------------------------------------

//==============================================================================
// CLASS Informthread
//==============================================================================
constructor TInformThread.Create(const aIpAdresse: PChar; const aMachID: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
  const aNodeList: TNodeList; const aWriteToLog: TWriteToLog);
begin
  mPJob            := Nil;
  mMachID          := aMachID;
  mNodeList        := aNodeList;
  mThreadIpAdresse := aIpAdresse;
  mWriteToMsgHnd   := aWriteToMsgHnd;
  mWriteToLog      := aWriteToLog;
  inherited Create(False);
end;
//:-----------------------------------------------------------------------------
destructor TInformThread.Destroy;
begin
  // Free my own window handle to receive windows messages
  DeAllocateHWnd(mThreadWndHandle);
  if assigned(mWSCHandler) then mWSCHandler.Free;
  inherited Destroy;
end;
//:-----------------------------------------------------------------------------
function TInformThread.SendJobtoMaThread(var aJobRecord: PJobRec): Boolean;
var
  xp: PJobRec;
begin
  result := true;
  xp     := AllocMem(aJobRecord.JobLen);
  move(aJobRecord^, xp^, aJobRecord.JobLen);
  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(xp), 0);
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.SetYMSetParm(aYM_Set_Name: tText50; aYM_set_id: Word; aProdGrp: Integer);
var
  xConvVal: Dword;
begin
  try
    with mWSCHandler do begin
      xConvVal := aYM_set_id;
      PutData(YMSET_NAME, @aYM_Set_Name, sizeof(aYM_Set_Name), aProdGrp, 0);
      PutData(YMSET_ID, @xConvVal, sizeof(xConvVal), aProdGrp, 0);
    end;
  except
    on E: Exception do
      CodeSite.SendWarning('SetYMSetParmeter ' + e.message)
  else
    CodeSite.SendError('SetYMSetParmeter failt with unknown error');
  end;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.GetYMSetParm(var aYM_Set_Name: tText50; var aYM_set_id: Word;
  var aYM_Set_Changed: Boolean; aProdGrp: Integer);
var
  xConvVal: DWord;
begin
  FillChar(aYM_Set_Name, sizeof(aYM_Set_Name), 0);
  aYM_set_id := 0;
  aYM_Set_Changed := false;
  try
    // YMSET Parameter holen
    with mWSCHandler do begin
      GetData(YMSET_NAME, @aYM_Set_Name, Sizeof(aYM_Set_Name), aProdGrp, 0);

      GetData(YMSET_CHANGED, @xConvVal, Sizeof(xConvVal), aProdGrp, 0);
      aYM_Set_Changed := xConvVal <> 0;

      GetData(YMSET_ID, @xConvVal, Sizeof(xConvVal), aProdGrp, 0);
      if xConvVal <= HIGH(Word) then aYM_set_id := xConvVal
                                else aYM_set_id := 0;

      CodeSite.SendFmtMsg('GetYMSetParm: YM_Set_Changed=%d,YM_Set_Name=%s',
                          [integer(aYM_Set_Changed), String(aYM_Set_Name)]);
    end;
  except
    on E: Exception do
      CodeSite.SendWarning('GetYMSetParmeter ' + e.message)
  else
    CodeSite.SendError('GetYMSetParmeter failt with unknown error');
  end;
end;
//:-----------------------------------------------------------------------------
//procedure TInformThread.SetMMPartieandArticleparam(aProdGrpInfo: TProdGrpInfo; aInProduktion: Boolean; aGrpNo: Integer; aColor: integer);
procedure TInformThread.SetMMPartieandArticleparam(const aProdGrpInfo: TXMLSettingsRec; aInProduktion: Boolean; aGrpNo: Integer);
const
 cStr60= 60;
 cStr40= 40;
var
  xString: string;
  xText100: TText100;
begin
  with mWSCHandler, aProdGrpInfo do
  try
    // Partiename auf Informator speichern
    Fillchar(xText100, sizeof(xText100), 0);
    StrCopy(@xText100, @ProdName);
    PutData(MM_PARTIENAME, @xText100, cStr60, aGrpNo, 0);

    // Partienummer setzen mit 'MM'. Dient als Identifikation auf dem Informator
    // dass dieses Setting vom MillMaster kommen und wird nicht weiter ausgewertet.
    if NOT aInProduktion then begin
      Fillchar(xText100, sizeof(xText100), 0);
      StrPCopy(@xText100, cPartieNoHeaderStr);
      PutData(MM_PARTIENUMMER, @xText100, cStr60, aGrpNo, 0);
    end;

    // Artikel mit StyleID,OrderID,OrderPosID abfuellen
    Fillchar(xText100, sizeof(xText100), 0);
    xString := Format('%d,%d,%d', [StyleID, OrderID, OrderPositionID]);
    StrPCopy(@xText100, xString);
    PutData(MM_ARTIKEL, @xText100, cStr40, aGrpNo, 0);

    // Color auf Informator speichern
    PutData(MM_PARTIECOLOR, @Color, sizeof(Color), aGrpNo, 0);

    // Schlupf auf Informator speichern
    Fillchar(xText100, sizeof(xText100), 0);
    xString := FloatToStrF(Slip / 1000.0, ffFixed, 7, 3); // Schlupf
    StrPCopy(@xText100, xString);
    PutData(MM_SCHLUPF, @xText100, cStr40, aGrpNo, 0);

    CodeSite.SendFmtMsg('SetMMPartieandArticleparam: StyleID=%d,OrderID=%d,OrderPosID=%d,ISlip=%d,MGroup=%d,Color=%d',
                        [StyleID, OrderID, OrderPositionID, Slip, aGrpNo, Color]);
  except
    on E: Exception do
      CodeSite.SendWarning('SetArticleString ' + e.message)
  end; // with
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.GetMMPartieandArticleparam(var aProdGrpInfo: TXMLSettingsRec; aGrpNo: Integer);
var
  xColor: Integer;
  xStrList: TStringList;
  xText100: TText100;
  xSchlupf: Integer;
begin
  with mWSCHandler, aProdGrpInfo do begin
    xStrList := TStringList.Create;
    try
      Fillchar(xText100, sizeof(xText100), 0);
      GetData(MM_ARTIKEL, @xText100, sizeof(xText100), aGrpNo, 0);
      // Stringlist kann nur Kommas als Delimiter erkennen
      xStrList.CommaText := string(xText100);
      if (xStrList.Count = 3) then begin
        StyleID := StrToInt(xStrList.Strings[0]);
        OrderID := StrToInt(xStrList.Strings[1]);
        if StrToInt(xStrList.Strings[2]) <= 255 then
          OrderPositionID := 1  // Nur Test bis Jobhandler ob
        else Abort;
      end else Abort;

      // Schlupf lesen
      Fillchar(xText100, sizeof(xText100), 0);
      GetData(MM_SCHLUPF, @xText100, sizeof(xText100), aGrpNo, 0);
      xSchlupf := round(StrToFloat(xText100) * 1000);  // Schlupf ist Int mit Faktor 1000
      if (xSchlupf > cMinSlip * 1000) and (xSchlupf < cMaxSlip * 1000) then
         Slip := xSchlupf
      else begin
        // Schlupf ausser Bereich
        CodeSite.SendFmtMsg('Schlupf ungueltig : MaGrp=%d, S=%d', [aGrpNo, xSchlupf]);
        Slip := cDefaultSlip; // 1000
      end;
    except
      // Artikel,order_id,order_position_id oder schlupf ungueltig oder nicht vorhanden: default= 1 setzen
      StyleID         := 1;
      OrderID         := 1;
      OrderPositionID := 1;
      Slip            := cDefaultSlip; // 1000
      CodeSite.SendFmtMsg('GetMMPartieandArticleparam: Unknown StyleId Order -Id -position : MaGrp=%d, Text=%s',
                          [aGrpNo, string(xText100)]);
    end; // try
    xStrList.Free;

    // PartieName lesen
    Fillchar(ProdName, sizeof(ProdName), 0);
    Fillchar(xText100, sizeof(xText100), 0);
    GetData(MM_PARTIENAME, @xText100, sizeof(xText100), aGrpNo, 0);
    // String von Informator wird auf TText50 gekuerzt
    StrLCopy(@ProdName, @xText100, Sizeof(ProdName));

    // Color lesen
    GetData(MM_PARTIECOLOR, @xColor, sizeof(xColor), aGrpNo, 0);
    Color := xColor;

    CodeSite.SendFmtMsg('GetMMPartieandArticleparam: StyleID=%d,OrderID=%d,OrderPosID=%d,ISlip=%d,MGroup=%d,Color=%d',
        [StyleID, OrderID, OrderPositionID, Slip, aGrpNo, Color]);
  end; // with mWSCHandler
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.MMJobDispatcher(aMsg: TMsg);
var
  xMinOneGroupInProd: boolean;
// Alle Jobs werden in das 338 spezifische Format konvertiert
// Exception werden abgefangen und weitergereicht
// Datadirection JobHandler -> WSCHandler
type
  TUSBufferstruct = (eRange, eStat, eSetting, eMm_data_trans, eAc338declaration, eFloat);
  TRange = record
    first, last: Integer;
  end;
  // UniversalStandardBuffer = Varianten Record um verschiedene Strukturen zu lesen oder schreiben
  TUSBuffer = packed record
    case TUSBufferstruct of
      eRange: (range: TRange);
      eStat: (stat: TGroupState);
      eSetting: (SettingDataArray: TYMSettingsByteArr);
      eMm_data_trans: (MM_data_trans: tMM_data_trans);
      eAc338declaration: (AC338declaration: TAC338declaration);
      eFloat: (float: Single);
  end;

var
  xUSBuffer: TUSBuffer;
  xGrpNo: Integer;
  xIndex, xStatus: Integer;
  xFreeJobMem: Boolean;
//  xValue: tAcknowledge;
  xSize: DWord;
  xXMLData: String;
  xTmpJob: PJobRec;
  xLastSpindle: Integer;
  //......................................................
  function ConvertAndDownloadXMLSettings(aXMLData: String; aWSCGroup: Integer): Boolean;
  var
    xMapfile: DOMDocument40;
    xBuffer: PByte;
    xBufferSize: Integer;
    xMapID: String;
  begin
    Result            := False;
    xBuffer           := Nil;
    xMapID            := mNodeList.MapID[mMachID];
    xMapfile          := mNodeList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        OnBeforeConvert := XML2BinBeforeConvert;
        OnCalcExternal  := XML2BinEventHandler;  //Zuweisen der Procedure an den Event, welcher im TMMXMLConverter.TMSXMLEventer.Process ausgel�st wird
        OnBeforeDeleteElements := XML2BinAfterConvert;  // ConfigCode schreiben
        // Exceptions werden ganz unten pauschal abgefangen
        xBufferSize := XMLToBin(xBuffer, xMapfile, msYMSetting, false, aXMLData);
        // wss: noch n�tig?
        // YM_Parameterkonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
  //      ParamConvertToInf(@mInformatorVersion, xBuffer, @xBufferSize);

        // Settings der Maschgruppe setzen
        mWSCHandler.PutData(GRP_PRE_SETTINGS, xBuffer, xBufferSize, aWSCGroup, 0);

  //      CodeSite.SendFmtMsg('SetSettings: IpAdr=%s, JobId=%d, MaschGrp=%d, ProdGrp=%d,SpdFirst=%d, SpdLast=%d',
  //                          [mIpAdresse, xPJob^.JobID, MachGrp,
  //                          TYMSettingsUtils.ExtractProdGrpID(DataArray, sizeof(DataArray)),
  //                          SpindleFirst, SpindleLast]);
        Result := True;
      finally
        Free;
        FreeMem(xBuffer); // xBuffer wird in XMLToBin alloziert und muss hier wieder freigegeben werden
        Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
      end; //with
    end; // if Assigned(xMapfile)
  end;
  //......................................................
  function UploadAndConvertMaConfig(var aXMLData: String; aWSCGroup: Integer): Boolean;
  var
    xMapfile: DOMDocument40;
    xBuffer: PByte;
    xMapID: String;
  begin
    Result   := False;
    xMapID   := mNodeList.MapID[mMachID];
    xMapfile := mNodeList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      // XPath f�r die Extrahierung vom Bauzustand Fragment gleich in der Variable ablegen
      xBuffer := AllocMem(cBinBufferSize);
      try
        // GrpSetting aus MachGruppe in den Buffer lesen
        mWSCHandler.GetData(GRP_SETTINGS, xBuffer, cBinBufferSize, aWSCGroup, 0);

        // TODO wss: noch n�tig?
        // YM_Parameterkonvertierung f�r MillMaster anhand Informatorversion(Funktion in DLL AC338RPC)
  //      ParamConvertToMM(@mInformatorVersion, xBuffer, @xBufferSize);

        // TODO wss: brauchts f�r den Bauzustand auch Daten aus dem mTempXMLSettings Record?
        FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
        // nach XML konvertieren
        with TMMXMLConverter.Create do
        try
          mTempXMLString         := cXPConfigNode;
          OnAfterDeleteElements  := Bin2XMLMaConfigAfterDelete;  // mTempXMLString enth�lt das XML Fragment f�r den Teilbauzustand
          OnBeforeDeleteElements := Bin2XMLBeforeDelete;
          OnCalcExternal := Nil;
          // F�r den Bauzustand d�rfen keine Elemente gel�scht werden
          // es werden vollwertige Dokumente erstellt. Ben�tigtes Fragment wird in OnAfterConvert ausgelesen
          BinToXML(xBuffer, cBinBufferSize, xMapfile, msMaConfig, False);
          aXMLData := aXMLData + mTempXMLString;
          Result := True;
        finally
          Free;
        end; //with
      finally
        FreeMem(xBuffer);
        Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
      end; // try
    end; // if Assigned(xMapfile)
  end;
  //......................................................
  function ExtractDefaultValues: String;
  var
    xMapfile: DOMDocument40;
    xMapID: String;
  begin
    Result   := '';
    xMapID   := mNodeList.MapID[mMachID];
    xMapfile := mNodeList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        IgnoreDeleteFlag := true;
        // Es erfolgt keine weitere Verarbeitung in irgendwelchen Events
        result := BinToXML(Nil, 0, xMapfile, msYMSetting, False);
      finally
        Free;
        Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
      end; //with
    end; // if Assigned(xMapfile)
  end;
  //......................................................
begin
  xIndex      := 0;
  xFreeJobMem := TRUE;
  with aMsg do begin
    mPJob := PJobRec(wParam);
    try
      case mPJob^.JobTyp of
        //..GetZESpdData
        jtGetZESpdData: begin
            with mWSCHandler do begin
              GetData(MM_DATA_TRANSFER, @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              // Pruefe ob neue Data Acquisition moeglich
              if xUSBuffer.MM_data_trans.access = Word(MM_DATA_IDLE) then begin
                with mPJob^.GetZESpdData do
                  with xUSBuffer.MM_data_trans do begin // Record xMM_data_trans ueber xBuffer legen
                    JobID     := mPJob^.JobID;
                    access    := WORD(MM_DATA_STORE);
                    Spindfrom := SpindleFirst - 1;
                    Spindto   := SpindleLast - 1;
                  end;
                // Q+Basis Daten anfordern und Timeouttimer starten
                with xUSBuffer.MM_data_trans do begin
                  CodeSite.SendFmtMsg('GetZESpdData: IpAdr=%s, JobId=%d, Acess=%d, SpdFirst=%d, SpdLast=%d',
                                      [mIpAdresse, JobID, access, Spindfrom + 1, Spindto + 1]);
                end;
                PutData(MM_DATA_TRANSFER, @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              end
              else begin                // Job pending
                CodeSite.SendMsg('mJobAcqQueue.Push');
                mJobAcqQueue.Push(mPJob);
                xFreeJobMem := False;
              end;
              KillTimer(mThreadWndHandle, cAcqTimerID);
              SetTimer(mThreadWndHandle, cAcqTimerID, cAcqTimeout, nil);
            end;
          end;
        //..DelZESpdData
        jtDelZESpdData: begin
            with mWSCHandler do begin
              GetData(MM_DATA_TRANSFER, @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              // Falls der Informator einen Rollback gemacht hat d.h. Seit dem MM_DATA_READY sind mehr
              // als 240s vergangen, wird dies im Protokoll b^vermerkt. Es wird jedoch kein Rollback auf dem
              // SQL Server ausgef�hrt da dies zu einer grossen Belastung f�hren w�rde.
              if xUSBuffer.MM_data_trans.access = Word(MM_DATA_IDLE) then begin
                mWriteToLog(etError, 'Rollback on Informator carried out, the reliability of the machine data is not guaranteed. ' + mWSCHandler.mIpAdresse);
              end;
            end;
            with xUSBuffer.MM_data_trans do begin // Record xMM_data_trans ueber xBuffer legen
              JobID     := mPJob^.JobID;
              access    := WORD(MM_DATA_CLEAR);
              Spindfrom := mPJob^.DelZESpdData.SpindleFirst - 1;
              Spindto   := mPJob^.DelZESpdData.SpindleLast - 1;
            end;
            // Empfang von Q+Basis Daten bestaetigen
            with mWSCHandler do begin
              PutData(MM_DATA_TRANSFER, @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              mWriteToMsgHnd(mPJob);    // Bestaetigung an MsgHandler schicken
              with xUSBuffer.MM_data_trans do begin
                CodeSite.SendFmtMsg('DelZESpdData: IpAdr=%s, JobId=%d, Acess=%d,SpdFirst=%d, SpdLast=%d',
                  [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
              end;
            end;
          end;
        //.. GetSettings
        jtGetSettings: begin
            xTmpJob := Nil;
            xGrpNo  := mPJob^.GetSettings.SettingsRec.Group; // MM & WSC 0-basiert
            if UploadAndConvertXMLSettings(xXMLData, xGrpNo, mPJob^.GetSettings.AllXMLValues) then begin
              try
                // gen�gend Speicher f�r Job reservieren
                xSize   := Length(xXMLData);
                xTmpJob := AllocMem(GetJobHeaderSize(jtGetSettings) + xSize);
                with xTmpJob^, GetSettings, SettingsRec do begin
                  JobID     := mPJob^.JobID;
                  JobTyp    := mPJob^.JobTyp;
                  NetTyp    := mPJob^.NetTyp;
                  MachineID := mMachID;

                  // die extern gesammelten Parameter nun in Job einf�gen
//                  SettingsRec := mTempXMLSettings;
                  System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                  // XML Settings in den Job kopieren
                  System.Move(PChar(xXMLData)^, XMLData, xSize);
                  // Partiename, Farbe, Style, OrderID/Position, Schlupf von Informator lesen und in SettingsRec ablegen
                  GetMMPartieandArticleparam(SettingsRec, xGrpNo);
                  // YMSET Parameter holen
                  GetYMSetParm(YMSetName, YMSetID, YMSetChanged, xGrpNo);
                  // Grp Status lesen
                  mWSCHandler.GetData(STATUS_GRP, @xStatus, sizeof(xStatus), xGrpNo, 0);
                  GroupState := TGroupState(xStatus);

                  // Spindelbereich aus MachGruppe lesen
                  mWSCHandler.GetData(BEREICH_GRP, @xUSBuffer, sizeof(TRange), xGrpNo, 0);
                  Group        := xGrpNo; // MM & WSC 0-basiert
                  SpindleFirst := xUSBuffer.range.first + 1; // WSC DataItem beginnt bei 0
                  SpindleLast  := xUSBuffer.range.last + 1;

                end; // with xTmpJob^,

                // Daten von GetSettings zu MsgHandler schicken
                mWriteToMsgHnd(xTmpJob);

  //                  CodeSite.SendFmtMsg('SetSettings: IpAdr=%s, JobId=%d, MaschGrp=%d, ProdGrp=%d,SpdFirst=%d, SpdLast=%d',
  //                                      [mIpAdresse, xPJob^.JobID, MachGrp,
  //                                      TYMSettingsUtils.ExtractProdGrpID(DataArray, sizeof(DataArray)),
  //                                      SpindleFirst, SpindleLast]);
              finally
                FreeMem(xTmpJob);
              end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo)
            end else
              mWriteToLog(etError, 'GetSettings: UploadAndConvertXMLSettings failed!');
          end;

        //..SetSettings
        jtSetSettings: begin
            EnterMethod('JobTyp jtSetSettings received');
            FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
            xGrpNo  := mPJob^.SetSettings.SettingsRec.Group; // MM & WSC 0-basiert
            with mPJob^.SetSettings, SettingsRec do begin
              with mWSCHandler do begin
                GetData(STATUS_GRP, @xUSBuffer, SizeOf(grpstat_typ), xGrpNo, 0);
                // Gruppe Frei oder Definiert
                if (xUSBuffer.stat = gsfree) or (xUSBuffer.stat = gsDefined) then begin
                  CodeSite.SendMsg('Group is free or defined');
                  if (OrderID <> 0) and (OrderPositionID <> 0)  then begin
                    // hier drinn werden auch die PartieName und Nummer (-> 'MM') gesetzt!!
                    SetMMPartieandArticleparam(SettingsRec, False, xGrpNo);
                    // YMSet Parameter
                    SetYMSetParm(YMSetName, YMSetID, xGrpNo);
                  end;
                  // PartieName, PartieNummer und YarnCount setzen. Garnnummer fuer 338 DB immer auf Nm-float konvertieren
                  SetWSCPartieparameter(xGrpNo, ProdName, cPartieNoHeaderStr,
                                        SpindleFirst-1, SpindleLast-1,
                                        Yarncountconvert(YarnCntUnit, yuNm, YarnCnt),   //27.9.04 nue: No more Div cYarnCntFactor
                                        NrOfThreads);
                  // Partiedaten umkopieren f�r das Eventhandling...
                  xXMLData                    := StrPas(XMLData);
                  mTempXMLSettings            := SettingsRec;
                  mTempXMLSettings.AssignMode := cAssignGroup;
                  // jedoch ohne den MMXML String
                  mTempXMLSettings.XMLData    := #0;
                end
                // Gruppe InProduktion
                else begin // gsInProd, gsLotChange
                  CodeSite.SendMsg('Group is InProd or LotChange');
                  // f�r die ProdID werden keine Artikel oder Settings Parameter zum Informator geschrieben
                  if AssignMode = cAssignProdGrpIDOnly then begin
                    CodeSite.SendMsg('Assign ProdID only');
                    // ProdID wird erg�nzt, wenn z.B. eine Partie auf dem
                    // Informator neu gestartet wurde (Definiert -> InProd).
                    xXMLData := cMMXMLDummy;
                    // es werden nur die ben�tigten Parameter in die Variable f�r das Eventhandling kopiert
                    mTempXMLSettings.ProdGrpID    := ProdGrpID;
                    mTempXMLSettings.Group        := xGrpNo;
                    mTempXMLSettings.AssignMode   := cAssignProdGrpIDOnly;
                  end
                  else begin
                    CodeSite.SendMsg('Overstart lot from MM');
                    // Hier wird nur der PartieName gesetzt, da in einer laufende Produktion weder
                    // der Spindelbereich noch die Garnnummer ver�ndert werden darf!!
                    SetWSCPartieName(xGrpNo, ProdName);
                    if (OrderID <> 0) and (OrderPositionID <> 0) then begin
                      // hier drinn werden auch die PartieName und Nummer gesetzt!!
                      SetMMPartieandArticleparam(SettingsRec, True, xGrpNo);
                      // YMSet Parameter
                      SetYMSetParm(YMSetName, YMSetID, xGrpNo);
                    end;
                    // Partiedaten umkopieren f�r das Eventhandling...
                    // Beim �berstarten einer laufenen Partie ist z.B. die neue ProdID
                    // bereits im SettingsRec und wird nicht separat in einem
                    // 2. Step geschrieben.
                    xXMLData                    := StrPas(XMLData);
                    mTempXMLSettings            := SettingsRec;
                    mTempXMLSettings.AssignMode := cAssignGroup;
                    // jedoch ohne den MMXML String
                    mTempXMLSettings.XMLData    := #0;
                  end;
                end; // if (xUSBuffer.stat = gsfree) or (

                // YM-Settings
                //............
                // Jobid auf YM MachineGrp setzen
                PutData(MM_JOB_ID, @mPJob^.JobID, Sizeof(DWord), xGrpNo, 0);
                //...konvertieren und gleich Downloaden
                if not ConvertAndDownloadXMLSettings(xXMLData, xGrpNo) then
                  mWriteToLog(etError, 'jtSetSettings: ConvertAndDownloadXMLSettings failed!');
              end; // with mWSCHandler
            end; // with mPJob^.SetSettings
          end; // jtSetSettings

        jtClearZESpdData: begin
            // Nach Partiestart werden die Daten durch Informator geloescht. Daher
            // nur Job bestaetigen.
            CodeSite.SendMsg('ClearZESpdData ' + mWSCHandler.mIpAdresse);
            mWriteToMsgHnd(mPJob);      // Bestaetigung zu MsgHandler schicken
          end; // jtClearZESpdData

        jtGetMaAssign: begin
            with mWSCHandler do begin
              with mPJob^.GetMaAssign do begin
                FillChar(Assigns, sizeof(TMaAssign), 0);
                MachineID := mMachID;
                for xGrpNo:=0 to cWSCSpdGroupLimit-1 do begin // MM & WSC 0-basiert
                  GetData(MM_DECLARATION, @xUSBuffer, sizeof(xUSBuffer), xGrpNo, 0);

                  // YM_Declarationskonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
                  xsize:= sizeof(xUSBuffer);
                  DeclConvertToMM(@mInformatorVersion,@xUSBuffer,@xsize);
                  with xUSBuffer.AC338declaration do begin
                    Assigns.Event                     := TDecEvent(event);
                    Assigns.MachState                 := states;
                    // TMaAssign Array beginnt bei 1, WSC DataItem aGroup bei 0
                    Assigns.Groups[xGrpNo].Modified := modify;
                    Assigns.Groups[xGrpNo].ProdId   := prodGrpID;
                    CodeSite.SendFmtMsg('GetMaAssign: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                      [mIpAdresse, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                  end; // with xUSBuffer

                  // Modify Flag und Event zuruecksetzen
                  DeleteWSCDecl(xGrpNo);
//                  xValue := MM_RESET_DECL;
//                  mWSCHandler.PutData(MM_ACKNOWLEDGE, @xValue, Sizeof(tAcknowledge), xGrpNo, 0);
//                  CodeSite.SendFmtMsg('GetMaAssign_MM_RESET_DECL: MaGrpNo=%d', [xGrpNo]);

                  // Temporaer von WSC  // Grp Bereich lesen
                  with Assigns.Groups[xGrpNo] do begin// MM & WSC 0-basiert
                    // Spindlebereich auslesen
                    GetData(BEREICH_GRP, @xUSBuffer, sizeof(xUSBuffer.range), xGrpNo, 0);
                    SpindleFirst := xUSBuffer.range.first + 1;
                    SpindleLast  := xUSBuffer.range.last + 1;

                    // Temporaer von WSC  // Grp Status lesen
                    GetData(STATUS_GRP, @xStatus, sizeof(xStatus), xGrpNo, 0);
                    GroupState := TGroupState(xStatus);

                    // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                    GetMMPartieandArticleparam(mTempXMLSettings, xGrpNo);
                    Color := mTempXMLSettings.Color;
                    with ProdGrpParam.ProdGrpInfo do begin
                      c_style_id          := mTempXMLSettings.StyleID;
                      c_order_id          := mTempXMLSettings.OrderID;
                      c_order_position_id := mTempXMLSettings.OrderPositionID;
                      c_slip              := mTempXMLSettings.Slip;
                    end;
                  end;
                end; // for xGrpNo
              end; // with mPJob^.GetMaAssign
              mWriteToMsgHnd(mPJob);    // Daten zu MsgHandler schicken
            end; // with mWSCHandler
          end; // jtGetMaAssign

        jtSaveMaConfigToDB: begin
            xXMLData := '';
            xTmpJob  := Nil;
            try
              xMinOneGroupInProd := False;
              // Alle aktiven Gruppen durchgehen und Settings anfordern um zu konvertieren
              for xGrpNo:=0 to cWSCSpdGroupLimit-1 do begin // MM & WSC 0-basiert
                mWSCHandler.GetData(STATUS_GRP, @xUSBuffer, SizeOf(grpstat_typ), xGrpNo, 0);
                // Gruppe in Produktion?
                if xUSBuffer.stat = gsInProd then begin
                  xMinOneGroupInProd := True;
                  CodeSite.SendFmtMsg('SaveMaConfigToDB: Group %d in Prod', [xGrpNo]);
                  // xXMLData wird in der Funktion zusammengesetzt
                  { TODO : Was tun wenn nicht OK???? }
                  if not UploadAndConvertMaConfig(xXMLData, xGrpNo) then
                    mWriteToLog(etError, 'UploadAndConvertMaConfig failed!');
                end;
              end; // for xGrpNo

              if xMinOneGroupInProd  then begin
                // Transformation in Bauzustandsdaten (Gruppen und Maschine getrennt)
                with TXMLMaConfigHelper.Create do try
                  xXMLData := BuildMaConfigFromHandler(cXMLHeader + xXMLData + cXMLFooter, ExtractDefaultValues, ntWSC, msMaConfig);
                  xLastSpindle := LastSpindle;
                finally
                  free;
                end;// with TStringList.Create do try
              end else begin
                mWriteToLog(etError, 'Upload MaConfig not possible because of no Group started on Machine');
                xLastSpindle := 1;
              end;

              // anschliessend die Konfigdaten in einen Job verpacken und abschicken
              // gen�gend Speicher f�r Job reservieren
              xSize   := Length(xXMLData);
              xTmpJob := AllocMem(GetJobHeaderSize(jtSaveMaConfigToDB) + xSize);
              with xTmpJob^, SaveMaConfig do begin
                JobID       := mPJob^.JobID;
                JobTyp      := mPJob^.JobTyp;
                NetTyp      := mPJob^.NetTyp;
                MachineID   := mMachID;
                LastSpindle := xLastSpindle;
                UseXMLData  := True;
                // Konfigdaten in den Job kopieren
                if xSize > 0 then
                  System.Move(PChar(xXMLData)^, XMLData, xSize);
              end; // with xTmpJob^,

              // Daten von GetSettings zu MsgHandler schicken
              mWriteToMsgHnd(xTmpJob);
            finally
              FreeMem(xTmpJob);
            end;
          end; // jtSaveMaConfigToDB
      else
      end;
    except
      on E: Exception do begin
        raise WSCAccessException.Create(Format('MMJobDispatcher xIndex:%d ',[xIndex]) + e.message);
      end;
    end;
    if xFreeJobMem then FreeMem(mPJob, mPJob^.JobLen);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.InformDispatcher(aMsg: TMsg);
{ Diese Methode verarbeitet Informs welche von den DataItems DATA_TRANSFER,
  MM_DECLARATION, GRP_SETTINGS ausgeloest werden.
  Datadirection Informator -> WSCHandler }
// Exception werden abgefangen und weitergereicht
type
  TUSBufferstruct = (eArray100,eAc338declaration);
  // UniversalStandardBuffer = Varianten Record um verschiedene Strukturen zu lesen oder schreiben
  TUSBuffer = packed record
    case TUSBufferstruct of
      eArray100: (text100: Array [0..99] of Byte);
      eAc338declaration: (AC338declaration: TAC338declaration);
  end;
var
  xInfIndex, xGrpNo, xspind: Integer;
  xMM_data_trans: tMM_data_trans;
  xUSBuffer: TUSBuffer;
  xYM_set_id: Word; xYM_Set_Changed: Boolean; // Dummyparameter fuer GetYMSetParm bei eEntryLocked
  xBasisdatenIndex, xKlassierdatenIndex: tdataItemIndex;
  xJob: TJobRec;
  xWSCDaten: TWSCDaten;
  xSize: DWord;
  xJobID: DWord;
  xXMLData: String;
  xTmpJob: PJobRec;
  //...............................................................
  procedure MoveDecltoJob(aGroup: Integer; adeclaration: TAC338declaration; var aMaAssign: TMaAssign);
  // Der WSC Declaration-Record wird in den Job Declaration-Record eingefuegt
  begin
    FillChar(aMaAssign, Sizeof(aMaAssign), 0);
    with aMaAssign do begin // MM & WSC 0-basiert
      Event := TDecEvent(adeclaration.Event);
      MachState := adeclaration.states;
      // TMaAssign Array beginnt bei 1, aGroup bei 0
      Groups[aGroup].Modified     := adeclaration.modify;
      Groups[aGroup].GroupState   := TGroupState(adeclaration.GrpState);
      Groups[aGroup].ProdId       := adeclaration.prodGrpID;
      Groups[aGroup].SpindleFirst := adeclaration.Spindfrom;
      Groups[aGroup].SpindleLast  := adeclaration.Spindto;
    end;
  end;
  //...............................................................
  function RequestMapnameFromMachine(var aMapname: String): Boolean;
  begin
    // TODO -cMM: RequestMapnameFromMachine default body inserted
    Result := False;
  end;
  //...............................................................
  function RequestMapfileFromMachine(aMapname: String): Boolean;
  begin
    // TODO -cMM: RequestMapfileFromMachine default body inserted
    // 1. ben�tigte Mapname anfragen
    // 2. Mapname
    Result := False;
  end;
  //...............................................................
begin
  try
    with mWSCHandler do
      with aMsg do begin
        CheckInform(Message, lParam, xInfIndex, xGrpNo);
        if xInfIndex > 0 then begin
          //  CodeSite.SendMsg('Inform eingetroffen');
          //.. DATA_TRANSFER
          if xInfIndex = Getindex(MM_DATA_TRANSFER) then begin
            EnterMethod('Index is MM_DATA_TRANSFER');
            GetData(xInfIndex, @xMM_data_trans, Sizeof(xMM_data_trans), 0, 0);
            //.. MM_DATA_READY
            if xMM_data_trans.access = Word(MM_DATA_READY) then begin // Daten zum abholen bereit
              // Data Ready an Informator, Timeout abbrechen
              KillTimer(mThreadWndHandle, cAcqTimerID);
              xJob.JobID          := xMM_data_trans.JobID;
              xJob.JobTyp         := jtGetZESpdData;
              xBasisdatenIndex    := Getindex(PChar(MM_BASISDATEN));
              xKlassierdatenIndex := Getindex(PChar(MM_KLASSIERDATEN));
              for xspind:=xMM_data_trans.Spindfrom to xMM_data_trans.Spindto do begin
                FillChar(xWSCDaten, sizeof(xWSCDaten), 0);
                GetData(xBasisdatenIndex, @xWSCDaten.baseData, Sizeof(xWSCDaten.baseData), xspind, 1);
                GetData(xKlassierdatenIndex, @xWSCDaten.klassierdaten, Sizeof(xWSCDaten.klassierdaten), xspind, 1);
                xJob.GetZeSpdData.SpindID := xspind + 1; // WSC DataItem beginnt bei 0
                // TODO -okhp : Nur solange im Telegram die FFDataBright Matrix nicht implemetiert ist werden diese nicht abgef�llt.
                Move(xWSCDaten, xJob.GetZeSpdData.SpdDataArr, Sizeof(xWSCDaten)-Sizeof(xWSCDaten.klassierdaten.FFDataBright));
                // Move(xWSCDaten, xJob.GetZeSpdData.SpdDataArr, Sizeof(xWSCDaten));
                mWriteToMsgHnd(@xJob);  // Daten zu MsgHandler schicken
              end;
              with xMM_data_trans do begin
                CodeSite.SendFmtMsg('MM_DATA_READY: IpAdr=%s, JobId=%d, Acess=%d,SpdFirst=%d, SpdLast=%d',
                                    [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
              end;
            end
            //..MM_DATA_IDLE
            else
              if xMM_data_trans.access = Word(MM_DATA_IDLE) then begin
                // Data Acquisiton beendet, Timeout abbrechen
                KillTimer(mThreadWndHandle, cAcqTimerID);
                // Pruefe ob Queue leer ist
                if mJobAcqQueue.Count > 0 then begin
                  codesite.SendInteger('mJobAcyQueue ' + mIpAdresse, mJobAcqQueue.Count);
                  // Hole Job von Queue und sende Ihn erneut
                  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(mJobAcqQueue.Pop), 0);
                end;
                with xMM_data_trans do begin
                  CodeSite.SendFmtMsg('MM_DATA_IDLE: IpAdr=%s,JobId=%d, Acess=%d, SpdFirst=%d, SpdLast=%d',
                    [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
                end;
              end;
          end
          //..MM_DECLARATION
          else if xInfIndex = Getindex(MM_DECLARATION) then begin
            EnterMethod('Index is MM_DECLARATION');
            if GetData(xInfIndex, @xUSBuffer, sizeof(xUSBuffer), xGrpNo, 0) then begin
              // YM_Declarationskonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
              xsize:= sizeof(xUSBuffer);
              DeclConvertToMM(@mInformatorVersion,@xUSBuffer,@xsize);
              case xUSBuffer.AC338declaration.event of
                eNone: begin
                    CodeSite.SendFmtMsg('None: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;
                eAssignComplete: begin
                    CodeSite.SendFmtMsg('AssignComplete: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;
                eInitialReset: begin
                    xJob.JobTyp                 := jtInitialReset;
                    xJob.InitialReset.MachineID := mMachID;
                    mWriteToMsgHnd(@xJob); // InitialReset zu MsgHandler schicken
                    CodeSite.SendFmtMsg('InitialReset: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;
                eReset: begin
                    xJob.JobTyp          := jtReset;
                    xJob.Reset.MachineID := mMachID;
                    mWriteToMsgHnd(@xJob); // Reset Event zu MsgHandler schicken
                    CodeSite.SendFmtMsg('Reset: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;
                eEntryLocked: begin     // Bildschirmschoner oder Logoff auf Informator
                    CodeSite.SendFmtMsg('EntryLocked: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                    xJob.JobTyp                := jtEntryLocked;
                    xJob.EntryLocked.MachineID := mMachID;
                    // Declarations aller Gruppe abholen und zuruecksetzten
                    // wss: ist ein leerer GetIndex n�tig?
                    Getindex(PChar(MM_DECLARATION));
                    with xJob.AssignComplete do begin
                      FillChar(Assigns, sizeof(TMaAssign), 0);
                      for xGrpNo:=0 to cWSCSpdGroupLimit-1 do begin
                        // Partiename, Farbe, Style, OrderID/Position, Schlupf von Informator lesen und in SettingsRec ablegen
                        GetMMPartieandArticleparam(mTempXMLSettings, xGrpNo);
                        with Assigns.Groups[xGrpNo], ProdGrpParam.ProdGrpInfo do begin
                          Color := mTempXMLSettings.Color;
                          c_style_id          := mTempXMLSettings.StyleID;
                          c_order_id          := mTempXMLSettings.OrderID;
                          c_order_position_id := mTempXMLSettings.OrderPositionID;
                          c_slip              := mTempXMLSettings.Slip;
                        end;
                        // YM_set_name holen
                        GetYMSetParm(Assigns.Groups[xGrpNo].ProdGrpParam.ProdGrpInfo. c_YM_set_name,xYM_set_id, xYM_Set_Changed, xGrpNo);
                        GetData(xInfIndex, @xUSBuffer, sizeof(xUSBuffer), xGrpNo, 0);
                        // YM_Declarationskonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
                        xsize:= sizeof(xUSBuffer);
                        DeclConvertToMM(@mInformatorVersion,@xUSBuffer,@xsize);
                        with xUSBuffer.AC338declaration do begin
                          Assigns.Event     := TDecEvent(event);
                          Assigns.MachState := states;
                          // TMaAssign Array beginnt bei 1, WSC DataItem aGroup bei 0
                          Assigns.Groups[xGrpNo].Modified     := modify;
                          Assigns.Groups[xGrpNo].GroupState   := TGroupState(GrpState);
                          Assigns.Groups[xGrpNo].ProdId       := prodGrpID;
                          Assigns.Groups[xGrpNo].SpindleFirst := Spindfrom;
                          Assigns.Groups[xGrpNo].SpindleLast  := Spindto;
                          // Pruefe mittels ProdID ob Assign Complete
                          with Assigns.Groups[xGrpNo] do begin
                            if (GroupState = gsInProd) or (GroupState = gsLotChange) then
                              if ProdId = 0 then begin
                                xJob.JobTyp                   := jtAssignComplete;
                                xJob.AssignComplete.MachineID := mMachID;
                                CodeSite.SendFmtMsg('AssignComplete after Locked: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                                    [mIpAdresse, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                              end;
                          end;
                        end;
                        DeleteWSCDecl(xGrpNo);
                        // Modify Flag und Event zuruecksetzen
                      end;
                    end;
                    mWriteToMsgHnd(@xJob); // AssignComplete zu MsgHandler schicken
                  end;
                eEntryUnlocked: begin   // Login auf Informator
                    xJob.JobTyp                  := jtEntryUnlocked;
                    xJob.EntryUnlocked.MachineID := mMachID;
                    mWriteToMsgHnd(@xJob); // EntryUnlocked Event MsgHandler schicken
                    CodeSite.SendFmtMsg('EntryUnlocked: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;
                eSettings, eRange: begin
                    CodeSite.SendMsg('Declaration: eStart/eRange received');
                    xJob.JobTyp           := jtAssign;
                    xJob.Assign.MachineID := mMachID;
                    MoveDecltoJob(xGrpNo, xUSBuffer.AC338declaration, xJob.Assign.Assigns);
                    mWriteToMsgHnd(@xJob); // Assign zu MsgHandler schicken
                    with xUSBuffer.AC338declaration do begin
                      CodeSite.SendFmtMsg('Settings or Range changed: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                          [mIpAdresse, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                    end;
                  end;
                eAdjust: begin
                    xJob.JobTyp           := jtAdjust;
                    xJob.Adjust.MachineID := mMachID;
                    mWriteToMsgHnd(@xJob); // tAdjust Event MsgHandler schicken
                    CodeSite.SendFmtMsg('Adjust: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo]);
                  end;

                //wss: eine Gruppe welche definiert war wurde gestartet. ist das so richtig formuliert?
                eStart: begin
                    CodeSite.SendMsg('Declaration: eStart received');
                    xTmpJob := Nil;
                    // Grp_Setting aus MachGruppe lesen
                    if UploadAndConvertXMLSettings(xXMLData, xGrpNo, False) then begin
                      try
                        // gen�gend Speicher f�r Job reservieren
                        xSize   := Length(xXMLData);
                        xTmpJob := AllocMem(GetJobHeaderSize(jtStartGrpEvFromMa) + xSize);
                        with xTmpJob^, StartGrpEvFromMa, SettingsRec do begin
                          JobID     := 0;
                          JobTyp    := jtStartGrpEvFromMa;
                          NetTyp    := ntWSC;
                          MachineID := mMachID;

                          // die extern gesammelten Parameter nun in Job einf�gen
                          //SettingsRec := mTempXMLSettings;
                          System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                          // XML Settings in den Job kopieren
                          System.Move(PChar(xXMLData)^, XMLData, xSize);
                          // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                          GetMMPartieandArticleparam(SettingsRec, xGrpNo);
                          // YMSET Parameter holen
                          GetYMSetParm(YMSetName, YMSetID, YMSetChanged, xGrpNo);

                          // Declarations aller Gruppe abholen und zuruecksetzten
                          // wss: ist ein leerer GetIndex n�tig?
                          Getindex(PChar(MM_DECLARATION));
                          GetData(xInfIndex, @xUSBuffer, sizeof(xUSBuffer), xGrpNo, 0);

                          // YM_Declarationskonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
                          xSize := sizeof(xUSBuffer);
                          DeclConvertToMM(@mInformatorVersion, @xUSBuffer, @xSize);
                          Group        := xGrpNo; //WSC beginnt bei 0
                          ProdGrpID    := xUSBuffer.AC338declaration.prodGrpID;
                          SpindleFirst := xUSBuffer.AC338declaration.Spindfrom;
                          SpindleLast  := xUSBuffer.AC338declaration.Spindto;
                        end; // with xTmpJob^,

                        // Daten von GetSettings zu MsgHandler schicken
                        mWriteToMsgHnd(xTmpJob);

                        with xUSBuffer.AC338declaration do begin
                          CodeSite.SendFmtMsg('StartGrpEvFromMa: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                              [mIpAdresse, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                        end;
                      finally
                        FreeMem(xTmpJob);
                      end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then
                    end else
                      mWriteToLog(etError, 'eStart: UploadAndConvertXMLSettings failed!');
                  end; // eStart

                eStop: begin
                    CodeSite.SendMsg('Declaration: eStop received');
                    xTmpJob := Nil;
                    //TODO wss: Grunds�chlich: warum wird bei einem Stop die Settings geholt?
                    if UploadAndConvertXMLSettings(xXMLData, xGrpNo, False) then begin
                      try
                        // gen�gend Speicher f�r Job reservieren
                        xSize   := Length(xXMLData);
                        xTmpJob := AllocMem(GetJobHeaderSize(jtStopGrpEvFromMa) + xSize);
                        with xTmpJob^, StopGrpEvFromMa, SettingsRec do begin
                          JobID     := 0;
                          JobTyp    := jtStopGrpEvFromMa;
                          NetTyp    := ntWSC;
                          MachineID := mMachID;

                          // die extern gesammelten Parameter nun in Job einf�gen
                          //SettingsRec := mTempXMLSettings;
                          System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                          // XML Settings in den Job kopieren
                          System.Move(PChar(xXMLData)^, XMLData, xSize);
                          // YM_Parameterkonvertierung f�r MillMaster anhand Informatorversion(Funktion in DLL AC338RPC)
                          Group        := xGrpNo; //WSC beginnt bei 0
                          ProdGrpID    := xUSBuffer.AC338declaration.prodGrpID;
                          SpindleFirst := xUSBuffer.AC338declaration.Spindfrom;
                          SpindleLast  := xUSBuffer.AC338declaration.Spindto;
                        end; // with xTmpJob^,

                        // Settings Daten zu MsgHandler schicken
                        mWriteToMsgHnd(xTmpJob);

                        with xUSBuffer.AC338declaration do
                          CodeSite.SendFmtMsg('StopGrpEvFromMa: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                              [mIpAdresse, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                      finally
                        FreeMem(xTmpJob);
                      end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then

                      // Nach dem Stop interessiert es das MMSystem nicht mehr was f�r Bits in modify gesetzt sind
                      // -> l�schen
                      DeleteWSCDecl(xGrpNo);

                    end else
                      mWriteToLog(etError, 'eStop: UploadAndConvertXMLSettings failed!');
                  end; // eStop

                // Nach SetSettings kommt hier die Best�tigung
                ePreSettings: begin
                    CodeSite.SendMsg('Declaration: ePreSettings received');
                    xTmpJob := Nil;
                    // JobID aus Informator-DB lesen
                    GetData(MM_JOB_ID, @xJobID, SizeOf(xJobID), xGrpNo, 0);
                    if xJobID <> 0 then begin
                      if UploadAndConvertXMLSettings(xXMLData, xGrpNo, False) then begin
                        try
                          // gen�gend Speicher f�r Job reservieren
                          xSize   := Length(xXMLData);
                          xTmpJob := AllocMem(GetJobHeaderSize(jtSetSettings) + xSize);
                          with xTmpJob^, SetSettings, SettingsRec do begin
                            JobID     := xJobID;
                            JobTyp    := jtSetSettings;
                            NetTyp    := ntWSC;
                            MachineID := mMachID;

                            // die extern gesammelten Parameter nun in Job einf�gen
                            //SettingsRec := mTempXMLSettings;
                            System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                            // XML Settings in den Job kopieren
                            System.Move(PChar(xXMLData)^, XMLData, xSize);

                            // Restlichen Parameter noch holen
                            // Partiename, Farbe, Style, OrderID/Position, Schlupf von Informator lesen und in SettingsRec ablegen
                            GetMMPartieandArticleparam(SettingsRec, xGrpNo);
                            // YMSET Parameter holen
                            GetYMSetParm(YMSetName, YMSetID, YMSetChanged, xGrpNo);

                            // wss: ist ein leerer GetIndex n�tig?
                            Getindex(PChar(MM_DECLARATION));
                            GetData(xInfIndex, @xUSBuffer, sizeof(xUSBuffer), xGrpNo, 0);
                            // YM_Declarationskonvertierung f�r Informator anhand Informatorversion(Funktion in DLL AC338RPC)
                            xsize := sizeof(xUSBuffer);
                            DeclConvertToMM(@mInformatorVersion,@xUSBuffer,@xsize);
                            Group        := xGrpNo; //WSC beginnt bei 0
                            ProdGrpID    := xUSBuffer.AC338declaration.prodGrpID;
                            SpindleFirst := xUSBuffer.AC338declaration.Spindfrom;
                            SpindleLast  := xUSBuffer.AC338declaration.Spindto;

                            // Bestaetigung mit Einstellungen werden an MsgHandler geschickt
                            mWriteToMsgHnd(xTmpJob);

                            with xUSBuffer.AC338declaration do begin
                              CodeSite.SendFmtMsg('SetSettings acknowledge: IpAdr=%s,JobId=%d, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                                  [mIpAdresse, xTmpJob.JobID, xGrpNo, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                            end;

                            xJobID := 0;
                            PutData(MM_JOB_ID, @xJobID, Sizeof(xJobID), xGrpNo, 0);
                          end; // with xTmpJob^,
                        finally
                          FreeMem(xTmpJob);
                        end; // try, if UploadAndConvertXMLSettings(xXMLData, xGrpNo) then
                      end else
                        mWriteToLog(etError, 'ePreSettings: UploadAndConvertXMLSettings failed!');
                    end else // if xJobID <> 0
                      CodeSite.SendFmtMsg('SetSettings acknowledge but JobID = 0: IpAdr=%s, JobId=%d, MaschGrp=%d',
                                          [mIpAdresse, xJobID, xGrpNo]);
                  end; // ePreSettings
              else
                CodeSite.SendMsg('Unknown declaration received: ' + IntToStr(Ord(xUSBuffer.AC338declaration.event)));
              end;
            end;
          end
        end
        else begin
          KillTimer(mThreadWndHandle, cLifeCheckTimerID);
          //  CodeSite.SendInteger('Lifecheck',mWSCHandler.mInform_socket);
          LifeCheck();
          SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
        end; // if xInfIndex > 0 then 
      end;
  except
    on E: Exception do raise WSCAccessException.Create('InformDispatcher ' + e.message);
    // on E: Exception do raise Exception.Create('InformDispatcher '+ e.message);
  end;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.CleanDataAcqQueue();
{ Die DataAcquisation Transaktion wird abgebrochen (Rollback).
  Alle Jobs in der Jobqueue des betreffenden Threads werden geloescht.
  Der Timer fuer die und DataAcquisation wird gestoppt.}
var
  xPJob: PJobRec;
  xMM_data_trans: tMM_data_trans;
begin
  try
    with mWSCHandler do begin
      while mJobAcqQueue.Count > 0 do begin
        xPJob := mJobAcqQueue.Pop;
        FreeMem(xPJob, xPJob.JobLen);
      end;
      GetData(MM_DATA_TRANSFER, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      // Pruefe ob DataTransaction laeuft
      if xMM_data_trans.access <> Word(MM_DATA_IDLE) then begin
        // DataTransaction abbrechen, Rollback
        xMM_data_trans.access := WORD(MM_DATA_RESTORE);
        PutData(MM_DATA_TRANSFER, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      end;
    end;
  except
    on E: Exception do begin
      KillTimer(mThreadWndHandle, cAcqTimerID);
      raise WSCAccessException.Create('CleanDataAcqQueue ' + e.message);
    end;
  end;
  CodeSite.SendMsg('RollbackMM_DATA_TRANSFER');
  KillTimer(mThreadWndHandle, cAcqTimerID)
end;
//:-----------------------------------------------------------------------------
function TInformThread.ConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsaufbau aufgerufen.
  Dabei wird die Jobid auf allen MachineGruppen auf 0 gesetzt und dann
  die Online Msg an den MsgHandler geschickt. Zuletzt wird die NodeListe aktualisiert
  und der Timer fuer den Lifecheck gestartet, welcher vom Informator vor Ablauf des
  Timeouts zurueckgesetzt wird. Zusaetzlich wird DataTransaction abgebrochen, Rollback}
var
  xGrpNo, JobInitvalue: Integer;
  xJob: TJobRec;
  xMM_data_trans: tMM_data_trans;
  xBuffer: Array[0..cMapBufferSize-1] of Byte;
  xMapID: String;
  xMapfile: String;
  xVersionInt: Integer;
  //...........................................................
  function HasMapfile: Boolean;
  begin
    Result := False;
    try
      Result := mWSCHandler.GetData(MM_MAP_FILE_NAME, @xBuffer, cMapBufferSize, 0, 0);
    except
    end;
  end;
  //...........................................................
begin
  result := false;
  try
    mWSCHandler.Connect;
    result := true;
    // Jobid auf allen MachineGruppen auf 0 setzen
    JobInitvalue := 0;
    with mWSCHandler do begin
      for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
        PutData(MM_JOB_ID, @JobInitvalue, Sizeof(DWord), xGrpNo, 0);
      end;
      GetData(MM_DATA_TRANSFER, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      // Pruefe ob DataTransaction laeuft
      if xMM_data_trans.access <> Word(MM_DATA_IDLE) then begin
        // DataTransaction abbrechen, Rollback
        xMM_data_trans.access := WORD(MM_DATA_RESTORE);
        PutData(MM_DATA_TRANSFER, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      end;
      // InformatorVersion loeschen
      Fillchar(mInformatorVersion, sizeof(mInformatorVersion), 0);
      // Abfrage der Informatorversion
      GetData(CLEARER_INFO, @mInformatorVersion, sizeof(mInformatorVersion), 0, 0);
      CodeSite.SendString('InformatorVersion ', StrPas(@mInformatorVersion));
    end; // with mWSCHandler

{ TODO -owss : Handling Mapdatei pr�fen}
    if not mNodeList.IsMapOverruled[mMachID] then begin
      xVersionInt := VersionToInt(StrPas(@mInformatorVersion));
      FillChar(xBuffer, cMapBufferSize, 0);
      if (xVersionInt > cInformatorVersionWithMapfiles) and HasMapfile then begin
        // hole zuerst die MapID
        try
//          mWSCHandler.GetData(MM_MAP_FILE_NAME, @xBuffer, cMapBufferSize, 0, 0);
          xMapID := Trim(StrPas(@xBuffer));
          if xMapID <> '' then begin
            CodeSite.SendFmtMsg('Informatorversion: %s, MapID: %%s', [StrPas(@mInformatorversion), xMapID]);
            // wenn f�r diese MapID noch kein Eintrag in der Mapliste ist, dann die Mapdatei auslesen
            if not mNodeList.MapfileAvailable[xMapID] then begin
              if FTPGetMapfile(mNodeList.IpAdresse[mMachID], xMapID, xMapfile) then begin
                mNodeList.Mapfile[xMapID] := xMapfile;
              end;
            end;
            mNodeList.MapID[mMachID] := xMapID;
          end else
            mWriteToLog(etWarning, Format('ConnectingMachine: Mapfilename empty! %s', [StrPas(@mInformatorversion)]));
        except

        end;
      end else begin
        //TODO wss: Anhand der Versioninfo auf der DB nach der richtigen MapID suchen
        mNodeList.GetMapfileFromVersion(mMachID, xVersionInt);
      end;
    end;

    CodeSite.SendString('Online Msg ', mWSCHandler.mIpAdresse);
    xJob.JobTyp             := jtMaOnline;
    xJob.MaOnline.MachineID := mMachID;
    mWriteToMsgHnd(@xJob);              // Online zu MsgHandler schicken

    // Update NodeList
    mNodeList.SetMachstate(Nil, mMachID, nsOnline);
  except
  end;
  SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
end;
//:-----------------------------------------------------------------------------
function TInformThread.DisConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsabbau aufgerufen.
  Falls der Informator vorher Online war wird der Status an den MsgHandler
  geschickt. Die NodeListe wird aktualisiert und die beiden Timer Lifecheck und
  DataAcquisation werden gestoppt.}
var
  xJob: TJobRec;
begin
  result := false;
  try
    if mWSCHandler.mConnected then begin
      result                   := mWSCHandler.Disconnect;
      xJob.JobTyp              := jtMaOffline;
      xJob.Maoffline.MachineID := mMachID;
      mWriteToMsgHnd(@xJob);            // Offline nur nach Onlinestatus zu MsgHandler schicken
      CodeSite.SendString('Offline Msg', mWSCHandler.mIpAdresse);
    end;
  except
  end;
  // Update NodeList
  mNodeList.SetMachstate(Nil, mMachID, nsOffline);
  KillTimer(mThreadWndHandle, cLifeCheckTimerID);
  KillTimer(mThreadWndHandle, cAcqTimerID);
end;
//:-----------------------------------------------------------------------------
function TInformThread.SendMachinenstatus(): Boolean;
var
  xJob: TJobRec;
begin
  result := false;
  try
    if mWSCHandler.mConnected = True then
      xJob.JobTyp := jtMaOnline
    else
      xJob.JobTyp := jtMaOffline;
    xJob.Maoffline.MachineID := mMachID;
    mWriteToMsgHnd(@xJob);              // Offline nur nach Onlinestatus zu MsgHandler schicken
  except
  end;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.Execute;
// Informs vom Informator und Jobs vom JobHandler werden pro Maschine verarbeitet
var
  xCoInitializeResult: HResult;
  xMsg: TMsg;
  xInformThreadMsg: Boolean;
begin
  xCoInitializeResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xCoInitializeResult <> S_OK) and (xCoInitializeResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');

  try
//wss: von aussen wird Free aufgerufen    FreeOnTerminate  := True;
    mJobRequestMsg   := RegisterWindowMessage(cMMJobRequestMsg);
    mWSCInformMsg    := RegisterWindowMessage(mThreadIpAdresse); // Pro Thread unique MsgID loesen
    mThreadWndHandle := AllocateHWnd(nil); // Pro Thread Window mit MsgQueue erzeugen
    if not assigned(mWSCHandler) then begin
      mWSCHandler := TWscDevHandler.create(mThreadWndHandle, mWSCInformMsg, mThreadIpAdresse, mMachID);
      ConnectingMachine;
      SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil); // Machstatus alle Minuten an MM system
      with mWSCHandler do begin
        //.. Begin Message Loop.........................................................
        while GetMessage(xMsg, mThreadWndHandle, 0, 0) {and (not Terminated)} do
        try
          mTempXMLString := '';
          FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
          with xMsg do begin
            xInformThreadMsg := False;
            // Kontrolle ob MsgID und Informsocket richtig
            if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
              InformDispatcher(xMsg); // Inform vom Informator
              xInformThreadMsg := True;
            end
            else if Message = mJobRequestMsg then begin
              MMJobDispatcher(xMsg);  // Job vom JobHandler
              xInformThreadMsg := True;
            end
            else if Message = WM_Timer then begin
              if wParam = cAcqTimerID then begin
                mWriteToLog(etWarning, 'Timeout for dataready signal, rollback Informator data ' + mWSCHandler.mIpAdresse);
                CleanDataAcqQueue;    // Timeout der DataAcquisation Queue
                xInformThreadMsg := True;
              end else
                if wParam = cMachstatusTickerID then begin
                  // Machstatus senden
                  SendMachinenstatus;
                end else
                  if wParam = cLifeCheckTimerID then begin
                    raise LifecheckEx.Create('Timeout LifeCheck ');
                    // Kein Lifecheck der 338 daher Disconnect
                  end;
//              end
//              else if (Message = WM_MMQuit) or (Message = WM_QUIT) then begin
//                BREAK;  // Abruch der Schleife durch WSCWriter
            end;

            if not xInformThreadMsg then
              // Alle andern Meldungen weiterleiten
              DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
          end;
        except
          on E: LifecheckEx do begin
            // Hierher komment wir mit einem k�nstlichen Exeception, wenn Lifecheck Timer abgelaufen ist
            CodeSite.SendMsg(e.message + mWSCHandler.mIpAdresse);
            // Neuer Verbindungsaufbau zu Informator herstellen
            DisConnectingMachine;
            ConnectingMachine;
          end;
          on e:Exception do begin
            mWriteToLog(etError, Format('FatalError: %s, %s', [mWSCHandler.mIpAdresse, e.Message]));
            CodeSite.SendMsg('Unknow Error');
            // Neuer Verbindungsaufbau zu Informator herstellen
            DisConnectingMachine;
            ConnectingMachine;
          end;
        end; // while GetMessage() try...
        //..End Message Loop....................................................
//        Disconnect;
      end; // with mWSCHandler
    end;
  finally
//    DisConnectingMachine;
    CoUninitialize;
  end;
end;
//procedure TInformThread.Execute;
//// Informs vom Informator und Jobs vom JobHandler werden pro Maschine verarbeitet
//var
//  xCoInitializeResult: HResult;
//  xMsg: TMsg;
//  xInformThreadMsg: Boolean;
//begin
//  xCoInitializeResult := CoInitialize(nil);
//  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
//  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
//  if ((xCoInitializeResult <> S_OK) and (xCoInitializeResult <> S_FALSE)) then
//    raise Exception.Create('CoInitialize failed');
//
//  try
//    FreeOnTerminate  := True;
//    mJobRequestMsg   := RegisterWindowMessage(cMMJobRequestMsg);
//    mWSCInformMsg    := RegisterWindowMessage(mThreadIpAdresse); // Pro Thread unique MsgID loesen
//    mThreadWndHandle := AllocateHWnd(nil); // Pro Thread Window mit MsgQueue erzeugen
//    if not assigned(mWSCHandler) then begin
//      mWSCHandler := TWscDevHandler.create(mThreadWndHandle, mWSCInformMsg, mThreadIpAdresse, mMachID);
//      ConnectingMachine;
//      SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil); // Machstatus alle Minuten an MM system
//      with mWSCHandler do begin
//        //.. Begin Message Loop.........................................................
//        while GetMessage(xMsg, mThreadWndHandle, 0, 0) and (not Terminated) do begin
//          codeSite.SendInteger('GetMessage MsgID', xMsg.Message);
//          try
//            mTempXMLString := '';
//            FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
//            with xMsg do begin
//              xInformThreadMsg := False;
//              // Kontrolle ob MsgID und Informsocket richtig
//              if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
//                InformDispatcher(xMsg); // Inform vom Informator
//                xInformThreadMsg := True;
//              end
//              else if Message = mJobRequestMsg then begin
//                  MMJobDispatcher(xMsg);  // Job vom JobHandler
//                  xInformThreadMsg := True;
//                end;
//              end
//              else if Message = WM_Timer then begin
//                if wParam = cAcqTimerID then begin
//                  mWriteToLog(etWarning, 'Timeout for dataready signal, rollback Informator data ' + mWSCHandler.mIpAdresse);
//                  CleanDataAcqQueue;    // Timeout der DataAcquisation Queue
//                  xInformThreadMsg := True;
//                end else
//                  if wParam = cMachstatusTickerID then begin
//                    // Machstatus senden
//                    SendMachinenstatus;
//                  end else
//                    if wParam = cLifeCheckTimerID then begin
//                      raise LifecheckEx.Create('Timeout LifeCheck ');
//                      // Kein Lifecheck der 338 daher Disconnect
//                    end;
//              end
//              else if (Message = WM_MMQuit) or (Message = WM_QUIT) then begin
//                BREAK;  // Abruch der Schleife durch WSCWriter
//              end;
//
//              if not xInformThreadMsg then
//                // Alle andern Meldungen weiterleiten
//                DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
//            end;
//          except
//            on E: LifecheckEx do begin
//              // Hierher komment wir mit einem k�nstlichen Exeception, wenn Lifecheck Timer abgelaufen ist
//              CodeSite.SendMsg(e.message + mWSCHandler.mIpAdresse);
//              // Neuer Verbindungsaufbau zu Informator herstellen
//              DisConnectingMachine;
//              ConnectingMachine;
//            end;
//            on e:Exception do begin
//              mWriteToLog(etError, Format('FatalError: %s, %s', [mWSCHandler.mIpAdresse, e.Message]));
//              CodeSite.SendMsg('Unknow Error');
//              // Neuer Verbindungsaufbau zu Informator herstellen
//              DisConnectingMachine;
//              ConnectingMachine;
//            end;
//          end; // try...except
//        end; // while GetMessage(
//        //..End Message Loop....................................................
//        Disconnect;
//      end; // with mWSCHandler
//    end;
//  finally
////    DisConnectingMachine;
//    CoUninitialize;
//  end;
//end;
//:-----------------------------------------------------------------------------
procedure TInformThread.XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
begin
  // Von Spectra <> Zenit Abh�ngie Elemente behandeln
  //-------------------------------------------------
  // Cfg A4
  if AnsiSameText(aEventRec.CalcElementName, cXPZeroTestItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPPClearingDuringSpliceItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg A10
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtFAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg A14
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtClusterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtAlarmItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg A15
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtYarnCountAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSuckOffLastCutOnlyItem) then
    aEventRec.WriteElementToBin := mIsZenit
  else
  // Cfg B4
  if AnsiSameText(aEventRec.CalcElementName, cXPFAdjustAfterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg B5
  if AnsiSameText(aEventRec.CalcElementName, cXPCutBeforeAdjustItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C0
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtSFIAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C1
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtFClusterAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Cfg C5
  if AnsiSameText(aEventRec.CalcElementName, cXPBlockAtShortCountAlarmItem) then
    aEventRec.WriteElementToBin := not mIsZenit
  else
  // Partiespezifische Parameter behandeln
  //--------------------------------------
  if AnsiSameText(aEventRec.CalcElementName, cXPAssignModeItem) then
    aEventRec.Value := mTempXMLSettings.AssignMode
  else
{  if AnsiSameText(aEventRec.CalcElementName, cXPMachBezItem) then
  mTempXMLSettings.m
    aEventRec.Value :=
  else}
  if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
    (**)
    aEventRec.Value := mTempXMLSettings.Group  // Informator verlangt die Gruppe 0-basiert - antwortet aber 1-basiert - (MM 0-basiert)
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
    aEventRec.Value := mTempXMLSettings.ProdGrpID
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
    aEventRec.Value := mTempXMLSettings.PilotSpindles
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPThreadCountItem) then
    aEventRec.Value := mTempXMLSettings.NrOfThreads
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPLengthWindowItem) then
    aEventRec.Value := mTempXMLSettings.LengthWindow
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPLengthModeItem) then
    (* Im Record kommt z.B. eine 1 (=lwmFirst) aus dem SettingsRecord welches per
       GetEnumName in einen Text umgewandet wird. Dieser Text wird dann per Konverter
       wiederum anhand der Enum-Liste in den richtigen Bin�rwert umgesetzt. *)
    aEventRec.Value := GetEnumName(TypeInfo(TLengthWindowMode), mTempXMLSettings.LengthMode)
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
    aEventRec.Value := mTempXMLSettings.SpindleFirst
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
    aEventRec.Value := mTempXMLSettings.SpindleLast
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
    aEventRec.Value := mTempXMLSettings.SpeedRamp
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
    aEventRec.Value := mTempXMLSettings.Speed;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.Bin2XMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
var
  xSH: TSensingHead;
begin
  if not VarIsNull(aEventRec.Value) then begin
    // Partie Abh�ngie Elemente behandeln
    //-------------------------------------------------
    if AnsiSameText(aEventRec.CalcElementName, cXPSensingHeadItem) then begin
      xSH := GetSensingHeadValue(aEventRec.Value);
      mTempXMLSettings.HeadClass := GetSensingHeadClass(xSH);
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPAssignModeItem) then
      mTempXMLSettings.AssignMode := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
      mTempXMLSettings.Group := aEventRec.Value - 1  // Informator antwortet 1-basiert - MM erwartet 0-basiert
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
      mTempXMLSettings.ProdGrpID := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
      mTempXMLSettings.PilotSpindles := aEventRec.Value
    else
    { TODO : Garnnummernkonvertierung �berpr�fen (Faktorbehaftet --> Single, Unit) }
    if AnsiSameText(aEventRec.CalcElementName, cXPYarnCountItem) then begin
      // Yarncount wird in OnAfterConvert noch nach Nm umkonvertiert
      mTempXMLSettings.YarnCnt := aEventRec.Value;
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPYarnUnitItem) then begin
      // Wert in XML mit Globaler Wert �berschreiben
      mTempXMLSettings.YarnCntUnit := TYarnUnit(GetEnumValue(TypeInfo(TYarnUnit), aEventRec.Value));
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPThreadCountItem) then begin
      // Wert in XML mit default Wert �berschreiben
      mTempXMLSettings.NrOfThreads := aEventRec.Value;
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPLengthWindowItem) then
      mTempXMLSettings.LengthWindow := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPLengthModeItem) then
      // In den Record kommt z.B. eine 1 (=lwmFirst) welche per GetEnumValue in Byte Wert umgewandelt wird
      // Im Konverter wird entsprechend der bin�re Wert vom Inf anhand einer Enum-Liste in den Text gewandelt
      mTempXMLSettings.LengthMode := GetEnumValue(TypeInfo(TLengthWindowMode), aEventRec.Value)
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
      mTempXMLSettings.SpindleFirst := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
      mTempXMLSettings.SpindleLast := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
      mTempXMLSettings.SpeedRamp := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
      mTempXMLSettings.Speed := aEventRec.Value;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TInformThread.Bin2XMLBeforeDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xGroupElement: IXMLDOMElement;
  xMachineElement: IXMLDOMElement;
  xFPattern: TFPBuilder;
begin
  // Die Klassierfelder m�ssen nach der Konvertierung ins MillMaster format gebracht werden
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPYMSettingBasicClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFDarkClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPDarkClusterClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFBrightClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPBrightClusterClassificationItem));

  with mTempXMLSettings do
  try
    YarnCnt := YarnCountConvert(YarnCntUnit, yuNm, YarnCnt);
    YarnCnt := YarnCnt / NrOfThreads;
  except
    // Einfach nichts machen da YarnCount bereits abgef�llt (NrOfThreads = 0)
  end;

  // F�r Texnet und WSC muss das FPattern aus den Settings erzeugt werden
  xFPattern := TFPBuilder.Create;
  try
    Supports(aMMXMLDoc.SelectSingleNode(cXPMachineNode), IXMLDOMElement, xMachineElement);
    xFPattern.MachineElement := xMachineElement;

    Supports(aMMXMLDoc.SelectSingleNode(cXPGroupNode), IXMLDOMElement, xGroupElement);
    xFPattern.GroupElement := xGroupElement;

    // BD-Reiniger wird nicht mehr unterst�tzt!!
    if GetSensingHeadValue(xFPattern.GroupValueDef[cXPSensingHeadItem, cSensingHeadNames[shTK830]]) = shTK940BD then
      xFPattern.GroupValue[cXPSensingHeadItem] := cSensingHeadNames[shTK840];

    xFPattern.BuildFPatternFromConfig(ntWSC, TMMXMLConverter(Sender).MapSection);
  finally
    xFPattern.Free;
  end;// try finally
end;

//:-----------------------------------------------------------------------------
procedure TInformThread.XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xNode: IXMLDOMNode;
begin
  xNode := aMMXMLDoc.selectSingleNode(cXPFP_ZenitItem);
  if Assigned(xNode) then
    mIsZenit := GetElementValueDef(xNode, false)
  else
    mIsZenit := False;

  // Die Klassierfelder m�ssen vor der Konvertierung Maschinenkonform transformiert werden
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPYMSettingBasicClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFDarkClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPDarkClusterClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFBrightClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPBrightClusterClassificationItem));
end;

//:-----------------------------------------------------------------------------
procedure TInformThread.Bin2XMLMaConfigAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
var
  xNode: IXMLDOMNode;
begin
  if mTempXMLString <> '' then begin
    xNode := aMMXMLDoc.selectSingleNode(mTempXMLString);
    if Assigned(xNode) then
      mTempXMLString := xNode.xml;
  end;

  RemoveConfigCode(aMMXMLDoc);
end;

//:-----------------------------------------------------------------------------
procedure TInformThread.Bin2XMLSettingsAfterDelete(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
begin
  RemoveConfigCode(aMMXMLDoc);
end;

  //...............................................................
procedure TInformThread.DeleteWSCDecl(aGrpNo: Integer);
var
  xValue: tAcknowledge;
begin
  // Declaration Event und Modify Flag der Gruppe zuruecksetzen
  xValue := MM_RESET_DECL;
  mWSCHandler.PutData(MM_ACKNOWLEDGE, @xValue, Sizeof(tAcknowledge), aGrpNo, 0);
  CodeSite.SendFmtMsg('MM_RESET_DECL: MaGrpNo=%d', [aGrpNo]);
end;

//:-----------------------------------------------------------------------------
function TInformThread.FTPGetMapfile(aIPAdress, aMapname: String; var aMapfile: String): Boolean;
const
  cBufferSize = 65536;
var
  xRcvStream: TMemoryStream;
  xDest: TStringStream;
  xDecompressionStream: TStream;
  xCount: Integer;
  xBuffer: array[0..cBufferSize-1] of Byte;
begin
  Result := False;
  xRcvStream := TMemoryStream.Create;
  with TIdFTP.Create(Nil) do
  try
    Host     := aIPAdress;
    Port     := 21;
    Username := cFTPUsername;
    Password := cFTPPassword;
    // wss: f�r bin�re Dateien den ByteMode verwenden da ansonsten LineFeeds $0D mit $0D$0A ersetzt werden
    TransferType := ftBinary;
    try
      Connect(False);
      Login;
      Get(aMapname, xRcvStream, False);
      Disconnect;
      xRcvStream.Position := 0;
      // TODO: unpack xStream und lege Mapfile in Liste ab
      xDest := TStringStream.Create('');
      try
        xDecompressionStream := TBZDecompressionStream.Create(xRcvStream);
        try
          while True do begin
            xCount := xDecompressionStream.Read(xBuffer, cBufferSize);
            if xCount <> 0 then
              xDest.WriteBuffer(xBuffer, xCount)
            else
              Break;
          end;
        finally
          xDecompressionStream.Free;
        end;
        xDest.Position := 0;
        aMapfile := xDest.DataString;
        Result   := True;
      finally
        xDest.Free;
      end;
    except
    end;
  finally
    Free;
    xRcvStream.Free;
  end;
end;

procedure TInformThread.RemoveConfigCode(const aMMXMLDoc: DOMDocument40);
var
  xNode: IXMLDOMNode;
  //...............................................................
  procedure DeleteXPath(aXPath: String);
  var
    xNode: IXMLDOMNode;
  begin
    xNode := aMMXMLDoc.selectSingleNode(aXPath);
    if Assigned(xNode) then
      if Assigned(xNode.parentNode) then
        xNode.parentNode.removeChild(xNode);
  end;
  //...............................................................
begin
  // Von Spectra <> Zenit Abh�ngie Elemente behandeln
  //-------------------------------------------------
  xNode := aMMXMLDoc.selectSingleNode(cXPFP_ZenitItem);
  if Assigned(xNode) then
    mIsZenit := GetElementValueDef(xNode, false)
  else
    mIsZenit := False;
  
  if mIsZenit then begin
    DeleteXPath(cXPZeroTestItem);                // Cfg A4
    DeleteXPath(cXPBlockAtFAlarmItem);           // Cfg A10
    DeleteXPath(cXPBlockAtClusterAlarmItem);     // Cfg A14
    DeleteXPath(cXPBlockAtYarnCountAlarmItem);   // Cfg A15
    DeleteXPath(cXPFAdjustAfterAlarmItem);       // Cfg B4
    DeleteXPath(cXPCutBeforeAdjustItem);         // Cfg B5
    DeleteXPath(cXPBlockAtSFIAlarmItem);         // Cfg C0
    DeleteXPath(cXPBlockAtFClusterAlarmItem);    // Cfg C1
    DeleteXPath(cXPBlockAtShortCountAlarmItem);  // Cfg C5
  end
  else begin
    DeleteXPath(cXPPClearingDuringSpliceItem);   // Cfg A4
    DeleteXPath(cXPBlockAtAlarmItem);            // Cfg A14
    DeleteXPath(cXPSuckOffLastCutOnlyItem);      // Cfg A15
  end;
end;

//:-----------------------------------------------------------------------------
function TInformThread.UploadAndConvertXMLSettings(var aXMLData: String; aWSCGroup: Integer; aIgnoreDeleteFlag: Boolean): Boolean;
var
  xMapfile: DOMDocument40;
  xBuffer: PByte;
  xMapID: String;
begin
  Result   := False;
  aXMLData := '';
  xMapID   := mNodeList.MapID[mMachID];
  xMapfile := mNodeList.MapfileDOM[xMapID];
  if Assigned(xMapfile) then begin
    // XPath f�r die Extrahierung vom FPattern Fragment gleich in der Variable ablegen
    xBuffer        := AllocMem(cBinBufferSize);
    try
      // GrpSetting aus MachGruppe in den Buffer lesen
      mWSCHandler.GetData(GRP_SETTINGS, xBuffer, cBinBufferSize, aWSCGroup, 0);

      // todo wss: noch n�tig?
      // YM_Parameterkonvertierung f�r MillMaster anhand Informatorversion(Funktion in DLL AC338RPC)
  //    ParamConvertToMM(@mInformatorVersion, xBuffer, @xBufferSize);

      // nach XML konvertieren
      // Exceptions werden ganz unten pauschal abgefangen
      with TMMXMLConverter.Create do
      try
        IgnoreDeleteFlag       := aIgnoreDeleteFlag;
        // F�r die Settings muss nichts aus dem DOM herauskopiert werden, da das Resultat gleich dem g�ltigen XML ist
        mTempXMLString         := '';
        // ProdGrp Infos werden in mTempXMLSettings extrahiert
        OnCalcExternal         := Bin2XMLEventHandler;
        OnBeforeDeleteElements := Bin2XMLBeforeDelete;
        OnAfterDeleteElements  := Bin2XMLSettingsAfterDelete;
        aXMLData := BinToXML(xBuffer, cBinBufferSize, xMapfile, msYMSetting, False);
//        mTempXMLSettings.Hash1 := Hashcode1;
//        mTempXMLSettings.Hash2 := Hashcode2;

        Result := True;
      finally
        Free;
      end; //with TMMXMLConverter.Create
    finally
      FreeMem(xBuffer);
      Pointer(xMapfile) := Nil;  // verhindern dass Interface freigegeben wird
    end; // try
  end; // if Assigned(xMapfile)
end;

//:-----------------------------------------------------------------------------
procedure TInformThread.XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc:
    DOMDocument40);
var
  xConv: TMMXMLConverter;
  //............................................................
  procedure WriteConfigCodeToBin(aSelPath: string);
  var
    xValue: integer;
    xNode: IXMLDOMNode;
  begin
    xValue := -1;
    xNode := aMMXMLDoc.SelectSingleNode(aSelPath);
    if assigned(xNode) then
      xValue := GetElementValueDef(xNode, -1);
    if xValue >= 0 then
      xConv.WriteElementToBinWithoutCheck(aSelPath, xValue);
  end;// procedure WriteConfigCodeToBin(aSelPath: string);
  //............................................................
begin
  if Sender is TMMXMLConverter then begin
    xConv := TMMXMLConverter(Sender);
    if assigned(aMMXMLDoc) then begin
      WriteConfigCodeToBin(cXPConfigCodeAItem);
      WriteConfigCodeToBin(cXPConfigCodeBItem);
      WriteConfigCodeToBin(cXPConfigCodeCItem);
      WriteConfigCodeToBin(cXPConfigCodeDItem);
    end;// if assigned(aMMXMLDoc) then begin
  end;// if Sender is TMMXMLConverter then begin
end;

//:-----------------------------------------------------------------------------

end.

