unit AC338ControlClass;

interface

uses
  Windows, Classes, AC338DLL_DEF, Messages, Forms;
const
  cPingTimerID= 100;
  cOfflinePingZyklus= 1000; // ms
  cOnLinePingZyklus= 30000;
type
  tWSCControlThread = class(TThread)
  private
  mWSCBroadcastMsg:Integer;
  mThreadWndHandle: HWND;
  mIpList:TStringList;
    { Private declarations }
  protected
    procedure Execute; override;
  public
    constructor Create (const pNodeList:DWord; const ahMsgPipe: DWord);
    destructor Destroy; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;



{ WSCControlthread }

constructor tWSCControlthread.Create(const pNodeList:DWord; const ahMsgPipe: DWord);
begin
  Inherited Create(False);
end;

destructor tWSCControlthread.Destroy;
begin
  // Free my own window handle to receive windows messages
  DeAllocateHWnd( mThreadWndHandle);
  inherited Destroy;
end;

procedure tWSCControlthread.Execute;
 var
   xMsg : TMsg;
   xresult: Integer;
   xControlThreadMsg:Boolean;
   xipAdress: PChar;
   xlength:Integer;
   xtemp: String;
begin
 FreeOnTerminate := True;
  mWSCBroadcastMsg:= RegisterWindowMessage('WSCBroadcastMsg'); // Pro Thread unique MsgID loesen
  mThreadWndHandle := AllocateHWnd( NIL);                  // Pro Thread Window mit MsgQueue erzeugen
  SetTimer(mThreadWndHandle,cPingTimerID,cOfflinePingZyklus,NIL);
  mIpList.Clear;
  ping_open(mThreadWndHandle,mWSCBroadcastMsg);
  ping_send();
   While GetMessage(xMsg,mThreadWndHandle,0,0) do begin
      with xMsg do begin
            xControlThreadMsg:= False;
            // Kontrolle ob MsgID und Informsocket richtig
            if Message = mWSCBroadcastMsg then begin
               xControlThreadMsg:= True;
               ping_read(xipAdress,xlength);
               mIpList.Add(xipAdress);
               // Add to lokalList
            end;
            if Message = WM_Timer then
             if wParam = cPingTimerID then begin
               xControlThreadMsg:= True;
               // copy List to NodeList
               while mIpList.Count > 0 do begin
                 xtemp:= mIpList[0];
                 mIpList.Delete(0);
               // copy List to NodeList
               end;
                mIpList.Clear;
                ping_send();
             end;
            if Not xControlThreadMsg then
              xResult := DefWindowProc( mThreadWndHandle, Message, wParam, lParam);
      end;
   end;  // end while
  ping_close(mThreadWndHandle);
  KillTimer(mThreadWndHandle,cPingTimerID);
  { Place thread code here }
end;

end.
