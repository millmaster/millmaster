(*==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: WSCWriterClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 06.06.2000  0.00  khp | GetSettingsAllGroups: wurde entfernt
// 12.05.2004  0.00  wss | - Copy/Paste Fehler in ProcessJob korrigiert
                           - Get/SetMaConfig ausgeklammert
//=========================================================================================*)



unit WSCWriterClass;

interface

uses windows, Classes, SysUtils,
  BaseThread, BaseGlobal, mmEventLog, LoepfeGlobal, IPCClass, AC338MachThreadClass,
  syncobjs;

type
  TWSCWriter = class(TBaseSubThread)
  PROTECTED
    mCSWriteToMsgHandler: TCriticalSection;
    mNodeList: tNodeList;
    procedure ProcessJob; OVERRIDE;
    procedure ProcessInitJob; OVERRIDE;
  PUBLIC
    procedure WriteToLog(aEvent: TEventType; aText: string);
    procedure WriteToMsgHandler(aPJob: PJobRec = nil);
//    procedure SetMachstatus(aMachID: Integer; aMachstatus: TWscNodeState);
    constructor Create(aThreadDef: TThreadDef); OVERRIDE;
    function DoConnect: Boolean; OVERRIDE;
    function Init: Boolean; OVERRIDE;
    destructor Destroy; OVERRIDE;
  end;

  //=============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf;                             // CodeSite

constructor TWSCWriter.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mCSWriteToMsgHandler := TCriticalSection.Create;
end;

//-----------------------------------------------------------------------------

destructor TWSCWriter.Destroy;
begin
  mNodeList.StopMachinethread;          // Stop aller MaschThreads
  sleep(100);                          // Warten bis alle Maschinen Threads beendet sind
  mNodeList.free;
  mCSWriteToMsgHandler.Free;
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TWSCWriter.DoConnect: Boolean;
begin
  result := inherited DoConnect;
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.WriteToMsgHandler(aPJob: PJobRec);
begin
  mCSWriteToMsgHandler.Enter;
  try
    if Assigned(aPJob) then aPJob^.NetTyp := ntWSC
                       else mJob.NetTyp   := ntWSC;
    WriteJobBufferTo(ttMsgDispatcher, aPJob);
  finally
    mCSWriteToMsgHandler.Leave;
  end;
  sleep(1);                             // Threadwechsel erzwingen
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.WriteToLog(aEvent: TEventType; aText: string);
begin
  WriteLog(aEvent, aText);
end;

//-----------------------------------------------------------------------------

//procedure TWSCWriter.SetMachstatus(aMachID: Integer; aMachstatus: TWscNodeState);
//begin
//  mNodeList.SetMachstate(nil, aMachID, aMachstatus);
//end;

//-----------------------------------------------------------------------------

function TWSCWriter.Init: Boolean;
begin
  Result := inherited Init;
  if Result then begin
    mConfirmStartupImmediately := False;
    try
      mNodeList := tNodelist.create;
    except
      Result := FALSE;
    end;
  end;
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.ProcessJob;
  //..............................................................
  procedure TreatJobToMaThread(aMachID: Integer);
  // Job wird dem betreffenden Maschinen Thread ueber die MsgQueue uebermittelt.
  // Falls Maschine OffLine oder MaschID unbekannt: Meldung an MsgHandler.
  var
    xMachthread: tInformthread;
  begin
    xMachThread := mNodeList.GetMachinethread(nil, aMachID);
    // Job SetMapfile muss weitergeleitet werden auch wenn die Maschine offline ist
//    if (mNodelist.GetMachstate(nil, aMachID) = nsOnline) or (mJob.JobTyp = jtSetMapfile) then
    if mNodelist.GetMachstate(nil, aMachID) = nsOnline then
      if xMachThread <> nil then
        xMachThread.SendJobtoMaThread(mJob)
      else begin                        // Unknow MachNo
        fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'Unknow MachNo');
        mJob.JobResult.JobTyp := mJob.JobTyp;
        mJob.JobTyp           := jtJobResult;
        mJob.JobResult.Error  := Error;
        WriteToMsgHandler;
      end
    else begin                          // Machine OFFLINE
      fError := SetError(ERROR_NOT_READY, etMMError, 'Machine OFFLINE');
      mJob.JobResult.JobTyp := mJob.JobTyp;
      mJob.JobTyp           := jtJobResult;
      mJob.JobResult.Error  := Error;
      WriteToMsgHandler;
    end;
  end;
  //..............................................................
begin
  // This methode is implemented only in derived classes and it is called from
  // the Execute methode of TBaseThread.
  case mJob.JobTyp of
    jtDelJobID: begin
        // Dummy Case um Fehlermeldung im Protokoll zu verhindern
      end;
    jtGetNodeList: begin
        // Lesen der mNodeList im WscHandler
        mNodeList.GetNodeList(mJob^.GetNodeList.NodeItems.WscNetNodeList);
        WriteToMsgHandler;              // NodeList an MsgHandler schicken
      end;
    jtGetMaAssign: begin
        // Get machine Assignment
        TreatJobToMaThread(mJob^.GetMaAssign.MachineID);
      end;
    jtClearZESpdData: begin
        // Loescht alle Spindeldaten auf Informator
        TreatJobToMaThread(mJob^.ClearZESpdData.MachineID);
      end;
    jtDelZESpdData: begin
        // Acknowledge fuer Spindel Request)
        TreatJobToMaThread(mJob^.DelZESpdData.MachineID);
      end;
(** wss: remarket weil nicht ben�tigt
    jtViewZESpdData: begin
        // regular spindle data aquisition at intervall end
        TreatJobToMaThread(mJob.ViewZESpdData.MachineID)
      end;
(**)
    jtGetZESpdData: begin
        // regular spindle data aquisition at intervall end
        TreatJobToMaThread(mJob^.GetZESpdData.MachineID);
      end;
    jtGetSettings: begin
        // Get settings from prodgroup
        TreatJobToMaThread(mJob^.GetSettings.MachineID);
      end;
    jtSetSettings: begin
        // Send settings of a prodgroup to ZE
        TreatJobToMaThread(mJob^.SetSettings.MachineID);
      end;
    jtSaveMaConfigToDB: begin
        // Get machine Configuration
        TreatJobToMaThread(mJob^.SaveMaConfig.MachineID);
      end;
//    jtSetProdID: begin
//        // Get machine Configuration
//        TreatJobToMaThread(mJob^.SetProdID.MachineID);
//      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;
  Sleep(10);
end; {TWSCWriter.ProcessJob}

//-----------------------------------------------------------------------------

procedure TWSCWriter.ProcessInitJob;
begin
  case mJob.JobTyp of
    jtSetNodeList: begin
        WriteLog(etInformation, 'SetNodeList received');
        // Load mNodeList to the NetHandler
        mNodeList.SetNodeList(mJob.SetNodeList.NodeItems.WscNetNodeList);
        // Start aller benoetigten MaschThreads
        mNodeList.StartMachineThread(WriteToMsgHandler, WriteToLog);
// TODO: wss: andere M�glichkeit statt sleep() ?
        sleep(5000);  // Prov!! Warten bis alle Threads Online in der NodeListe nachgefuehrt haben
        // Erhalt der NodeListe bestaetigen
        WriteToMsgHandler;
        // Alle andern Thread freigeben (ControlThread, ReadThread)
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d',
                                   [IPCClientIndex[cMainChannelIndex]^.Error]));
          WriteLog(etError, 'Init confirm failed');
          Exit;
        end;
      end;
  else
    ProcessJob;
  end;
end; {TWSCWriter.ProcessInitJob}

end.

