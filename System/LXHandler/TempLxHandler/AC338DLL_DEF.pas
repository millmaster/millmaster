(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: AC338DLL_DEF.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: DEfinition Unit fuer AC338RPC.dll
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 6.10.1999  0.00  khp | Datei erstellt
|25.06.2000  0.00  khp | GRUPPENNAME_GRP,GRUPPENNUMMER_GRP wird ersetzt durch MM_PARTIENAME, MM_PARTIENUMMER
|                      | Keine Zugriffe auf WSC Informator Datenbank
|12.09.2001  1.0   khp | Neue DatenItens auf Informator YM_SET_ID, YM_SET_CHANGED, YM_SET_NAME, MM_GET_NEXT_JOB.
|05.11.2003  1.1   khp | Erfassung der Informator Versions Nr."Clearer_info" und vier zus�tzliche Funktionen
|                      | ParaConvertToMM, ParamConvertToInf, DeclConvertToMM und DeclConvertToInf
==========================================================================================*)

// Konstanten, Datentypen, Functionen und Proceduren fuer den Zugriff
// auf die ac338rpc.dll


unit AC338DLL_DEF;

interface
uses Windows, BaseGlobal;

//******************************************************************************
// * Const-Deklarationen
//******************************************************************************
const
  // Bin�re Gr�sse der Settings
  cBinBufferSize = 400;

  //..Defines der DataItemNamen der gemeinsamen Daten aus G_Namen.h...............
  SCHLINGE_EIN_BAUZ = 'schlinge ein bauz';
  SCHLINGE_EIN_LEINEN_BAUZ = 'schlinge ein leinen bauz';
  TROMMELFAKTOREN = 'trommelfaktoren';
  FIRMA_BAUZ = 'firma bauz';
  MA_NUMMER_BAUZ = 'ma-nummer bauz';
  SERIEN_NUMMER_BAUZ = 'serien-nummer bauz';
  REINIGER_BAUZ = 'reiniger bauz';
  DRUCKER_BAUZ = 'drucker bauz';
  NETZWERK_BAUZ = 'netzwerk bauz';
  NEUWERT_BAUZ = 'neuwert bauz';
  SPRACHE = 'sprache';
  DIM_LAENGE = 'dim laenge';
  DIM_GEWICHT = 'dim gewicht';
  DIM_GARNFEINHEIT = 'dim garnfeinheit';
  SCHICHTKALENDER = 'schichtkalender';
  SW_MASCHINE_START = 'sw maschine start';
  PRINT_BEI_PW = 'print bei PW';
  WAR_SCHICHTW = 'war schichtw';
  OPERATOR_CODE = 'operator code';
  MEISTER_CODE = 'meister code';
  TECHNIKER_CODE = 'techniker code';
  DIA_EINHEIT = 'dia einheit';
  DIA_WECHSLER = 'dia wechsler';
  DIA_SPULSTELLE = 'dia spulstelle';
  PROZ_INFO = 'proz info';
  XYKURVE = 'xykurve';
  URSTART_SPEICHER_NR = 'urstart speicher nr';
  EINSCHALT_ZEITPUNKT = 'einschalt-zeitpunkt';
  AUSSCHALT_ZEITPUNKT = 'ausschalt-zeitpunkt';
  INF_TEILE_NR = 'inf teile nr';
  TEST_DATENBANK = 'test datenbank';
  FEHLER_MELDUNG = 'fehler meldung';
  FEHLER_BESTAETIGUNG = 'fehler bestaetigung';
  FEHLER_PING_NET1 = 'fehler ping net1';
  FEHLER_PING_NET2 = 'fehler ping net2';
  FEHLER_PING_NET3 = 'fehler ping net3';
  FEHLER_PING_NET4 = 'fehler ping net4';
  FEHLER_BOOT = 'fehler boot';
  MELDUNGSGRUND = 'meldungsgrund';
  FEHLER_RING_INTERN = 'fehler ring intern';
  FEHLER_RING_BOOT = 'fehler ring boot';
  FEHLER_RING_NET = 'fehler ring net';
  SCHICHTDAUER = 'schichtdauer';
  MASCHINE = 'maschine';
  DUNKELSCHALTUNG_AKTIV = 'dunkelschaltung aktiv';
  URSTART_VERLASSEN = 'urstart verlassen';
  VALIDATE_CODE = 'validate code';
  OK_REINIGER = 'ok reiniger';
  REINIGER_TYP = 'reiniger typ';
  OK_DIABOLO = 'ok diabolo';
  OK_ALLE_TASKS = 'ok alle tasks';
  UPDATE_SCREEN = 'update screen';
  MESSAGE_FOR_DIABOLO = 'message for diabolo';
  SCHICHTWECHSEL = 'schichtwechsel';
  UHR_GESTELLT = 'uhr gestellt';
  UHRZEIT = 'uhrzeit';
  DRUCKER_AKTIV = 'drucker aktiv';
  STATUS_TLN = 'status tln';
  FLAG_CAN_TLN = 'flag can tln';
  TEST_CAN = 'test can';
  DRUCK_LISTE = 'druck liste';
  AKT_ZUGRIFF = 'akt zugriff';
  IP_ADDRESS = 'ip address';
  RPC_OK_RNG = 'rpc ok rng';
  // GRUPPENNAME_GRP = 'gruppenname grp';    wird ab 5.1 nicht mehr gebraucht
  // GRUPPENNUMMER_GRP = 'gruppennummer grp'; wird ab 5.1 nicht mehr gebraucht
  GARNFEINHEIT_GRP = 'garnfeinheit grp';
  FADENZAHL_GRP = 'fadenzahl grp';
  STAT_WSC_SCHICHT_GRP = 'stat wsc schicht grp';
  STAT_REIN_SCHICHT_GRP = 'stat rein schicht grp';
  TODO_REIN_SCHICHT_GRP = 'todo rein schicht grp';
  COPY_GRP = 'Copy grp';
  SCHICHTENDE_GRP = 'schichtende grp';
  SCHICHTSTART_GRP = 'schichtstart grp';
  STATUS_GRP = 'status grp';
  BEREICH_GRP = 'bereich grp';
  BEREICH_INPUT_GRP = 'bereich input grp';
  BEREICH_NACH_SW_GRP = 'bereich nach sw grp';
  GRUPPE_KOPIEREN = 'gruppe kopieren';
  ANZAHL_EINHEITEN_BAUZ = 'anzahl einheiten bauz';
  ANZAHL_SPULSTELLEN_BAUZ = 'anzahl spulstellen bauz';
  SPULSTELLEN_EH_BAUZ = 'spulstellen eh-bauz';
  IN_GRUPPE_SPUL = 'in gruppe spul';
  REINIGER_BEREIT_SPUL = 'reiniger bereit spul';
  PSTATUS_SPUL = 'pstatus spul';
  ATTRIBUT_WECHSEL = 'Attribut_Wechsel';
  A_K_NOMENU = 'A_K_NoMenu';
  A_K_NOMENUSERVICE = 'A_K_NoMenuService';
  A_K_NO_SELECT = 'A_K_No_Select';
  A_K_BEDIENERLEVEL = 'A_K_BedienerLevel';
  A_K_MEISTERLEVEL = 'A_K_MeisterLevel';
  A_K_SERVICELEVEL = 'A_K_ServiceLevel';
  A_K_TECHNIKERLEVEL = 'A_K_TechnikerLevel';
  A_K_INGENIEURLEVEL = 'A_K_IngenieurLevel';
  DUMMY = 'dummy';
  PARTIE_START_CHECK_RNG_GRP = 'partie start check rng grp';
  PARTIE_BEREICH_CHECK_RNG_GRP = 'partie bereich check rng grp';
  DIA_GRUPPENSPEICHER = 'dia gruppenspeicher';
  LAENGEKORR_GRP = 'laengekorr grp';

  //..Defines der DataItemNamen der Reiniger-Daten aus r_namen.h .................

  A_K_DIADIFF = 'A_K_DiaDiff';
  A_K_STARTADJUST = 'A_K_StartAdjust';
  A_K_SIROSETTINGS = 'A_K_SiroSettings';
  A_K_LABPACK = 'A_K_LabPack';
  RDATMAID_INIT = 'rdatmaid init';
  SPDL_PARA = 'spdl_para';
  DB_SPDL_CTRL = 'db_spdl_ctrl';
  GRP_INFO = 'grp_info';
  GRP_SETTINGS = 'grp_settings';
  GRP_PRE_SETTINGS = 'grp_pre_settings';
  GRP_SETTINGS_CHANGED = 'grp_settings_changed';
  GRP_ADJUST_STATUS = 'grp_adjust_status';
  RDONGLE_CODE = 'rdongle_code';
  RDONGLE_TIME = 'rdongle_time';
  AWE_COMMAND = 'awe_command';
  RDRKCMD = 'rdrkcmd';
  RDRKSTATUS = 'rdrkstatus';
  GRP_DATA_TBL = 'grp_data_tbl';
  SPDL_DATA_TBL = 'spdl_data_tbl';
  ADJ_COMMAND = 'adj_command';
  CLUSTERDEF = 'clusterDef';
  AWE_SIMU = 'awe_simu';
  MSG_JOURNAL_CNT = 'msg_journal_cnt';
  MSG_JOURNAL_TBL = 'msg_journal_tbl';
  AWE_ERR_LOG_CNT = 'awe_err_log_cnt';
  AWE_ERROR_LOG = 'awe_error_log';
  AWE_VERSION = 'awe_version';
  SFI_GRP_DATA = 'sfi_grp_data';
  GL_REF_SFID = 'gl_ref_sfid';
  SPDL_CNT_BAUZ = 'spdl cnt bauz';
  MM_BASISDATEN = 'mm_basisdaten';
  MM_KLASSIERDATEN = 'mm_klassierdaten';
  MM_DATA_TRANSFER = 'mm_data_transfer';
  MM_TEXT_MELDUNG = 'mm_text_meldung';
  MM_DECLARATION = 'mm_declaration';
  MM_JOB_ID = 'mm_job_id';

  // new fuer Vesion 5.25
  MM_PARTIENAME = 'mm_partiename';
  MM_PARTIENUMMER = 'mm_partienummer';
  MM_PARTIECOLOR = 'mm_partiecolor';
  MM_ACKNOWLEDGE = 'mm_acknowledge';
  MM_ARTIKEL = 'mm_artikel';
  MM_SCHLUPF = 'mm_reserve1';

  // new fuer Version 5.3x              // Typen auf Informator
  YMSET_ID = 'ym_set_id';               // long (4 Byte)
  YMSET_CHANGED = 'ym_set_changed';     // long (4 Byte)
  YMSET_NAME = 'ym_set_name';           // str40_typ (41 Zeichen, mit \0 abgeschlossen, UTF8-Format)
  MM_GET_NEXT_JOB = 'mm_get_next_job';  //� long

  // new f�r Version 6.x
  CLEARER_INFO = 'clearer_info';        // str40_typ (41 Zeichen, mit \0 abgeschlossen, UTF8-Format)

  // Neu f�r Version mit eigenen Mapfiles
  (* Da Schlafhorst eigenm�chtig einen Serienstand erstellt hat, ist
     die Version 6.20 noch ohne Mapfile *)
  cInformatorVersionWithMapfiles = 620;
  cMapBufferSize = 100; // bytes
  MM_MAP_FILE_NAME = 'mm_map_file_name';

  cFTPUsername = 'super';
  cFTPPassword = 'user';

  //Definition maximale Stringlaenge Datenelement-Name, Typbeschreibung
  MAXNAME = 255;
  MAXTYPEDESC = 255;
  MAXDATA = 2048;
  // gemeinsame Konstante
  MAX_TYPE_DESC = Integer(256);
  MAX_GROUPS = Integer(6);

  // Definitionen fuer die Textlabelnummernbereiche der Textmodule
  LABELOFFSET = 8192;
  MAXCOMMON = 8191;
  MAXSCHLAFHORST = 16383;
  MAXREINIGER = 24575;

  // Zugriffsmodi fuer entfernte Rechner
  NET_NO = 0;
  NET_READ = 1;
  NET_WRITE = 2;
  NET_UPDATE = 3;

  // Signalcode fuer Netzwerk-Informdienste
  NETWORKSIGNAL = $FFFF;

  // Indexcode um saemtliche Datenelemente anzusprechen
  ANY_INDEX = -1;

  // Definitionen aus dbank.htm
  DBA_OK = 0;
  DBA_ERROR = -1;

  //******************************************************************************
  // * Type-Deklarationen und Type-Definitionen
  //******************************************************************************

{$MINENUMSIZE 4}                        // Achtung alle Enum Types 4 Byte 32 Bit
  {========================================================}

type

  auth = packed record                  // Defintion aus D32-RPC.h
    typ: Integer;
    PBase: PChar;
    len: Integer;
  end;

  HCLIENT = packed record               // Defintion aus D32-RPC.h
    Client: PByte;
    PAuth: ^auth;
    offset: Byte;
  end;

  PHCLIENT = ^HCLIENT;

  bereich_typ = array[0..1] of Integer;
  grpstat_typ = (FREI, DEFINIERT, PRODUKTION, PARTIEWECHSEL);


  tDba_result = Integer;                // Defintion aus dfehlnum.h :typedef int  tdba_result;  Funktionsrueckgabewert

  tNetworkgroup = Integer;              // Gruppenzugehoerigkeit entfernter Rechner
  tAccesskey = Integer;                 // Berechtigungschluessel Netzwerkzugriff
  tDataItemIndex = Integer;             // Zugriffsnummer fuer Datenelement
  tbuffersize = Integer;                // Maximale Groesse Variablenverzeichnisbuffer
  tNamesize = Integer;                  // Maximale Groesse Variablenname
  tTypesize = Integer;                  // Maximale Groesse Typbeschreibung
  tDatasize = Integer;                  // Maximale Groesse Datenberiereich
  tLimitsize = Integer;                 // Groesse Buffer fuer Grenzwerte
  tSignalcode = Integer;                // Signalcode fuer Inform-Auftrag
  tFunctionnumber = Integer;            // ID-Nummer PUT-/GET-Funktion
  tArg = Integer;                       // Argument fuer PUT-/GET-Funktion
  tTextnummer = Integer;                // ID-Nummer des gewuenschten Textstrings
  tLabelnumber = Integer;               // Label-Nummer des gewuenschten Textstrings
  tInformhandle = Integer;              // Zugriffsnummer fuer Informdienst-Socket
  tTexthandle = Integer;                // Zugriffsnummer auf Textdatei
  tAnzahl_vars = Integer;               // Anzahl der installierten Datenelemente
  tIndexbuffer = Integer;               // Feld mit Variablen-Zugriffsnummern
  tTextbufferlen = Integer;             // Laenge des Textbuffers (nd_get_text)
  tOffset = Integer;                    // Offset des Datenelements fuer Signalisierung
  tReturncode = Integer;                // Rueckgabewert Server-Anw. an Client-Anw.
  tDbhandle = Integer;                  // Zugriffsnummer Remote Prozessdatenbank


  PHostname = PChar;                    // Zeiger auf Rechnername
  PDataname = PChar;                    // Zeiger auf Variablenname
  PTypedesc = PChar;                    // Zeiger auf Typbeschreibung
  PTextstring = PChar;                  // Zeiger auf Buffer fuer Textstrings
  PTextmodname = PChar;                 // Name der gewuenschten Textdatei
  PData = PByte;                        // Zeiger auf Datenbuffer
  PLimit = PByte;                       // Zeiger auf Buffer fuer Ober- bzw. Untergrenze

  tInfomsg = packed record              // Info-Message fuer Netzwerk-Informdienst      */
    hostid: LongWord;                   // u_long
    DataItemIndex: Integer;             // int
    offset: Byte;                       // u_short
  end;

  { TODO 1 -okhp -cSystem : Datentype tnetworkattribut muss noch codiert werden }
  // 32 Bit Type; Bit 0,1 Group1; 2,3 Group2; 4,5 Group3; 6,7 Group4, 8..31 Spare

  tNetworkattribut = packed record
    group1_4: Byte;                     // Zugriffsrechte Netzwerkgruppe    1..4
    spare_1: Byte;
    spare_2: Byte;
    spare_3: Byte;
  end;

  tUserlevel =                          // Zugriffsberechtigunscodes fuer Datenelemente
  (JEDER,                               // 0,
    OPERATOR,                           // 1,
    MEISTER,                            // 2,
    SERVICE,                            // 3,
    TECHNIKER,                          // 4,
    INGENIEUR,                          // 5,
    NIEMAND);                           // 6


  tUserattribut = packed record
    read: tuserlevel;
    write: tuserlevel;
  end;

  tAcknowledge = (MM_ACK_NONE,          // 0
    MM_RESET_DECL);                     // 1

  tDimension =                          // Interne Dimension der Daten  Dimesionscodes fuer Datenelemente
  (NIX,                                 //  0
    MIN_LEN,                            //  1
    MM,                                 //  2
    CM,                                 //  3
    M,                                  //  4
    M_MIN,                              //  5
    KM,                                 //  6
    PRO_KM,                             //  7
    MAX_LEN,                            //  8
    MIN_GEW,                            //  9
    G_H,                                // 10
    G,                                  // 11
    KG,                                 // 12
    T,                                  // 13
    MAX_GEW,                            // 14
    MIN_FEIN,                           // 15
    NM,                                 // 16
    MAX_FEIN,                           // 17
    MIN_DREH,                           // 18
    TM,                                 // 19
    ALPHA,                              // 20
    MAX_DREH,                           // 21
    MIN_TEMP,                           // 22
    C,                                  // 23
    MAX_TEMP,                           // 24
    MIN_DRUCK,                          // 25
    HPA,                                // 26
    MAX_DRUCK,                          // 27
    MIN_FLUSS,                          // 28
    CL,                                 // 29
    L,                                  // 30
    DL,                                 // 31
    HL,                                 // 32
    MAX_FLUSS,                          // 33
    GRAD,                               // 34
    PROZENT,                            // 35
    PROMILLE,                           // 36
    STUNDEN,                            // 37
    MINUTEN,                            // 38
    SEKUNDEN,                           // 39
    CNEWTON,                            // 40
    V,                                  // 41
    mV,                                 // 42
    Hz,                                 // 43
    kW,                                 // 44
    A,                                  // 45
    MAX_DIMENSION);                     // 46






  tInformflag =                         // Anzeiger, ob Informationsauftrag setzen oder loeschen
  (INFORM_SET,                          // 0
    INFORM_CLEAR);                      // 1

  //.. Strukturen aus rdb_type.h..................................................

  // AC338 DECLARATION

  TWSCDecEvent = (eNone, eInitialReset, eReset, eEntryLocked, eEntryUnlocked, eRange, eSettings, eAdjust,
    eAssignComplete, eStart, eStop, ePreSettings);
  TWSCGroupState = (efree, eDefined, eInProd, eLotChange);
  pTAC338declaration = ^TAC338declaration;
  TAC338declaration = packed record
    event: TWSCDecEvent;                // Typen Definiert in BaseGlobal
    states: BITSET16;                   // Constanten Definiert in BaseGlobal
    modify: BITSET16;                   // Constanten Definiert in BaseGlobal
    GrpState: TWSCGroupState;           // FREI, DEFINIERT, PRODUKTION, PARTIEWECHSEL;  Typen Definiert in BaseGlobal
    prodGrpID: Longint;                 // Nach Aenderung der Settings 0
    Spindfrom, Spindto: Byte;           // Nach Kaltstart in beiden FF oder 1..n
  end;



  // MM_DATA_TRANSFER
  eMM_data_state = (MM_DATA_IDLE, MM_DATA_STORE, MM_DATA_READY, MM_DATA_RESTORE, MM_DATA_CLEAR);
  pMM_data_trans = ^tMM_data_trans;
  tMM_data_trans = packed record
    access: Word;
    Spindfrom, Spindto: Byte;           // SpindelNo auf Inf. beginnt bei 0
    JobID: DWORD;
  end;






// Datenbank Zugriffsfunktionen fuer Netzwerk (Bibliothek 'rdbalib')
// Achtung alle Aufrufe mit cdecl, mit stdcall probleme bei verschachtelten Exception
//-------------------------------------------------------------------------------------------------

// function rdba_init:
// Baut Verbindung zu Informator auf.  Ein dBHandle und ein InformHandle werden zur�ckgegeben.
// Bei der ersten Verbindung wird Distinct DLL geladen und in 'Tray Liste' Distinct Kugel angezeigt.
function rdba_init(hostname: PHostname;
  networkgroup: tnetworkgroup;
  var dbhandle: tdbhandle;
  var informhandle: tinformhandle;
  var accesskey: taccesskey;
  var cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_terminate:
// Schliesst Verbindung zu Informator. dBHandle und InformHandle werden freigegeben.  Falls letzte
// Verbindung abebrochen wird verschwindet Distinct Kugel.(Distinct DLL wird entfernt)
function rdba_terminate(var dbhandle: tdbhandle;
  var informsocket: tinformhandle;
  var cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_description:
// Mittels des Dataltemlndex wird der symbolische Name, die Typbeschreibung, die Netwerkzugriffsberechtigung,
// die Benutzerzugriffsberechtigung und die Dimensionierung des mit index spezifizierten Datenelementes abgerufen.
function rdba_description(dbhandle: tdbhandle;
  DataItemIndex: tDataItemIndex;
  namesize: tnamesize;
  datatypesize: ttypesize;
  limittypesize: ttypesize;
  dataname: PDataname;
  datatypedesc: PTypedesc;
  limittypedesc: PTypedesc;
  var networkattribut: tnetworkattribut;
  var userattribut: tuserattribut;
  var dimension: tdimension;
  var limitsize: tlimitsize;
  var datasize: tdatasize;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_ident:
// Diese Funktion liefert den Dataltemlndex des mit dataname spezifizierten Datenelementes.
// Mit dem Dataltemindex k�nnen dann weitere Operationen auf das Datenelement durchgef�hrt werden
//(z.B. Lesen oder Schreiben).
function rdba_ident(dbhandle: tdbhandle;
  dataname: PDataname;
  var DataItemIndex: tDataItemIndex;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_inform:
// Mit dieser Funktion kann bei einem Dataltem die Informfunktion aktiviert bzw deaktiviert werden,
// weiche �ber jede �nderung des Dateninhalts informiert. Die �nderung des Dataltems wird dem
// Anwendungsprogramm via eine Windows Message bekanntgegeben.
function rdba_inform(dbhandle: tdbhandle;
  DataItemIndex: tDataItemIndex;
  signalcode: tsignalcode;
  flag: tinformflag;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


  // function rdba_get_information:
// Mit rdba_get_informationo k�nnen hostid, index und offset des ge�nderten Dataltems bestimmt werden.
// Der index entspricht der Dataltemsindex des ge�nderten Dataltems, der offset der Gruppen- bzw.
// Spindelnummer des ge�nderten Datums.
function rdba_get_information(dbhandle: tdbhandle;
  informhandle: tinformhandle;
  var infomsg: tinfomsg;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_read:
// Das mit index spezifizierte Datenelement wird aus der Proze�datenbank in den vom Anwender �bergebenen
// Buffer kopiert.  Existiert eine mit dem Datenelement verbundene GET-Funktion, so wird diese vorher mit
// den Argumenten argl und arg2 ausgef�hrt. Sollte f�r ein Datenelement keine Typbeschreibung definiert sein,
// so gibt die Funktion einen Leerstring als Typbeschreibung zur�ck.
function rdba_read(dbhandle: tdbhandle;
  DataItemIndex: tDataItemIndex;
  arg1: targ;
  arg2: targ;
  typesize: ttypesize;
  datasize: tdatasize;
  typedesc: PTypedesc;
  data: PData;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_write:
// Das mit index spezifizierte Datenelement aus der Proze�datenbank wird mit den vom Anwender �bergebenen Daten
// geschrieben.  Anschlie�end wird gegebenenfalls (falls eine solche definiert ist) die mit dem Datenelement
// verbundene PUT-Funktion mit den Argumenten argl und arg2 ausgef�hrt. Die vom Anwender �bergebene Typbeschreibung
// wird mit der Beschreibung im Inhaltsverzeichnis der Proze�datenbank verglichen.  Bei Abweichungen wird der
// Schreibauftrag nicht ausgef�hrt und ein entsprechender Fehler gemeldet.  Sollte f�r das Datenelement keine
// Typbeschreibung im Inhaltsverzeichnis der Proze�datenbank vorliegen, so wird die Typ�berpr�fung nicht durchgef�hrt.
// Statt dessen wird �berwacht, ob die �bergebene datasize mit der im Inhaltsverzeichnis vermerkten Datenelementgr��e
// �bereinstimmt.
function rdba_write(dbhandle: tdbhandle;
  DataItemIndex: tDataItemIndex;
  arg1: targ;
  arg2: targ;
  typedesc: PTypedesc;
  datasize: tdatasize;
  data: PData;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_get_userlevel:
// Mit dieser Funktion kann der aktuelle Berechtigungscode f�r Zugriffe auf Datenelemente in der Proze�datenbank gelesen
// werden (entspricht in etwa dem Schl�sselschalter).
function rdba_get_userlevel(dbhandle: tdbhandle;
  var UserGroup: tUserlevel;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function rdba_lifecheck:
// Mit dieser Funktion wird der Lifecheck des Informators beantwortet.  Wenn der Lifecheck des Informators nicht
// beantwortet wird schliesst der Informator die Verbindung nach 1 Minute.
function rdba_lifecheck(dbhandle: tdbhandle;
  cli: PHCLIENT): tdba_result; cdecl; external 'ac338rpc.dll';


// function ParamConvertToMM:
// Mit dieser Funktion wird die vom Informator gelesene Settings Struktur auf die
// aktuell auf dem Millmaster vorhandene Settings Struktur konvertiert.
function ParamConvertToMM(version: PChar;
  settings: PByte; lengh: PByte): tdba_result; cdecl; external 'ac338rpc.dll';


// function ParamConvertToInf:
// Mit dieser Funktion wird die auf dem Millmaster vorhandene Settings Struktur auf
// die auf dem Informator vorhandene Settings Struktur konvertiert.
function ParamConvertToInf(version: PChar;
  settings: PByte; lengh: PByte): tdba_result; cdecl; external 'ac338rpc.dll';


// function DeclConvertToInf:
// Mit dieser Funktion wird die auf dem Millmaster vorhandene Declarations Struktur auf
// die auf dem Informator vorhandene Declarations Struktur konvertiert.
function DeclConvertToInf(version: PChar;
  declarations: PByte; lengh: PByte): tdba_result; cdecl; external 'ac338rpc.dll';


// function DeclConvertToMM:
// Mit dieser Funktion wird die vom Informator gelesene Declarations Struktur auf die
// aktuell auf dem Millmaster vorhandene Declarations Struktur konvertiert.
function DeclConvertToMM(version: PChar;
  declarations: PByte; lengh: PByte): tdba_result; cdecl; external 'ac338rpc.dll';


// Externe Funktionen mit welchen geprueft werden kann welche 338 Online sind oder nicht.
// Nach einen ping_broadcast wird dem Thread ueber wMsg signalisiert, mittels Ping_read den Teilnehmer zu lesen.
// Ping_read ist eine blockierende Funktion.

function ping_open(hwind: HWND; const wMsg: Integer): Boolean; cdecl; external 'ac338rpc.dll';
function ping_broadcast(): Boolean; cdecl; external 'ac338rpc.dll';
function ping_send(): Boolean; cdecl; external 'ac338rpc.dll';
function ping_read(ipAdress: PChar; const len: Integer): Boolean; cdecl; external 'ac338rpc.dll';
function ping_close(hwind: HWND): Boolean; cdecl; external 'ac338rpc.dll';

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

