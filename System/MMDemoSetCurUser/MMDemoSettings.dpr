{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMDemoSettings.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...: Wird anstelle von MMClient im MMDemo ben�tigt.
|                 Fuegt die Security Eintraege DefaultUserName & DefaultUserGroups
|                 in die CurUser Reg. ein. Diese werden fuer die MMSecurity benoetigt.
|                 Funktion wurde aus dem MMClient entnommen.
| Info..........:
|
|
|
| Develop.system: Win2k
| Target.system.: Win2k /XP
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.01.2005  1.00  SDo | File created
===============================================================================}

program MMDemoSettings;
{$APPTYPE CONSOLE}
uses
  SysUtils,
  classes,
  Dialogs,
  mmRegistry,
  LoepfeGlobal,
  mmCS,
  NetAPI32MM,
  Windows;


function EnumProc(hwnd:HWND;lParam:DWORD):BOOL; stdcall;
var
  PID:DWORD;
begin
  Result := False;
  GetWindowThreadProcessID(hwnd,@PID);
  if PID=lparam then
    ShowWindow(hwnd,SW_HIDE)
  else
    Result := True;
end;

{
//******************************************************************************
//Ermittelt den DLL Namen
//******************************************************************************
function GetDLLName:String;
var xSize : Integer;
    xFileName : array[0..500] of char;
    xDLLName :String;
begin
   xSize := GetModuleFileName(HInstance,
                              xFileName,
                              SizeOf(xFileName)
                              );
   if xSize > 0 then begin
      xDLLName := xFileName;
      result := ExtractFileName(xDLLName);
   end else
      result := 'Unknown DLL';
end;
//------------------------------------------------------------------------------
}


var
  mUserGroups: TStringlist;
  mNTUser, mPath: string;
  //SI: TStartupInfo;
begin

  {
  FillChar(SI, SizeOf(TStartupInfo), 0);
  SI.cb         := SizeOf(TStartupInfo);
  SI.dwFlags    := STARTF_USESHOWWINDOW;   // Konsole soll am Anfang unsichtbar sein
  SI.wShowWindow:= SW_HIDE;
  }
  EnumWindows(@EnumProc, GetCurrentProcessID()); // Hide console application


  mUserGroups := TStringlist.Create;
  mUserGroups.Duplicates := dupIgnore;


  try
      mNTUser := GetWindowsUserName;

      if CodeSite.Enabled then begin
        // wss: get user groups from local computer instead of local groups of server
        GetUserLocalGroupName(mNTUser, '', mUserGroups);
        CodeSite.SendStringList('Local User groups', mUserGroups);
        mUserGroups.Clear;

        GetUserGroupName(mNTUser, gMMHost, mUserGroups);
        CodeSite.SendStringList('gMMHost User groups', mUserGroups);
        mUserGroups.Clear;
        // Usergruppen vom DomainController  -> TStringList;
        GetUserGroupName(mNTUser, gDomainController, mUserGroups);
        CodeSite.SendStringList('gDomainController User groups', mUserGroups);
        mUserGroups.Clear;
        CodeSite.AddSeparator;
      end;



      // wss: get user groups from local computer instead of local groups of server
      GetUserLocalGroupName(mNTUser, '', mUserGroups);
      // Usergruppen auf MM-Server  -> TStringList;
      GetUserGroupName(mNTUser, gMMHost, mUserGroups);
      // Usergruppen vom DomainController  -> TStringList;
      GetUserGroupName(mNTUser, gDomainController, mUserGroups);

      with TmmRegistry.Create do
      try
        //Eintrag in Registry
        RootKey := cRegCU;
        OpenKey(cRegMMSecurityPath, True);

        WriteString(cRegDefUserName, mNTUser);
        WriteString(cRegDefUserGroups, mUserGroups.CommaText);
        WriteString(cRegDefLogonTime, DateTimeTostr(Now));

        {
        RootKey := cRegLM;
        OpenKey('Software\Microsoft\Windows\CurrentVersion\Run', True);

        mPath := GetRegString(cRegLM, cRegMillMasterPath, cRegMillMasterRootPath, '');
        if mPath <> '' then begin
           mPath :=  mPath + '\Install\' + GetDLLName ;
        end;
        }

      finally
        Free;
      end;

 finally
   mUserGroups.Free;
 end;

end.