{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_BaseAdmin.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: BasisClass of administrators
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 16.11.1999        SDo | Property RegSettings implemented (Read values from
|                       | Const.Array
| 06.10.2002        LOK | Umbau ADO
|==============================================================================}
unit u_BaseAdmin;

interface

uses
  Classes, SysUtils, Windows,
  BaseGlobal, BaseMain, AdoDBAccess, mmList, mmEventLog,
  u_TimeJobDB, SettingsReader;

type
  TBasexAdministrator = class(TObject)
  private
    fInitialized: Boolean;
    mEventLog: TEventLogWriter;
    mSettings: TMMSettingsReader;
  protected
    mDB: TTimeJobDB;
    procedure WriteLog(aEvent: TEventType; aMsg: string);
    function StartupInit: Boolean; virtual; abstract;
    function InsertJob(aNewJob: TJobListRec): Boolean;
    function GetActTime: TTimeStamp;
    function GetEventTime: TTimeStamp;
    property MMSettings: TMMSettingsReader read mSettings;
  public
    constructor Create(aDB: TTimeJobDB; aEventLog: TEventLogWriter; aSettings: TMMSettingsReader); virtual;
    function Emergency(aTimeStampOld, aTimeStampNew: TTimeStamp): Boolean; virtual; abstract;
    function Init: Boolean; virtual;
    function PrepareJob(aJob: PJobRec): Boolean; virtual; abstract;
    property ActTime: TTimeStamp read GetActTime;
    property EventTime: TTimeStamp read GetEventTime;
    property Initialized: Boolean read fInitialized;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
// TBasexAdministrator
//------------------------------------------------------------------------------
constructor TBasexAdministrator.Create(aDB: TTimeJobDB; aEventLog: TEventLogWriter; aSettings: TMMSettingsReader);
begin
  inherited Create;
  fInitialized := False;
  mDB          := aDB;
  mEventLog    := aEventLog;
  mSettings    := aSettings;
end;
//------------------------------------------------------------------------------
function TBasexAdministrator.Init: Boolean;
begin
  fInitialized := True;
  Result := fInitialized;
end;
//------------------------------------------------------------------------------
function TBasexAdministrator.InsertJob(aNewJob: TJobListRec): boolean;
begin
  Result := mDB.InsertJob(aNewJob);
end;
//------------------------------------------------------------------------------
function TBasexAdministrator.GetActTime: TTimeStamp;
begin
  Result := mDB.ActTime;
end;
//------------------------------------------------------------------------------
function TBasexAdministrator.GetEventTime: TTimeStamp;
begin
  Result := mDB.EventTime;
end;
//------------------------------------------------------------------------------
procedure TBasexAdministrator.WriteLog(aEvent: TEventType; aMsg: string);
begin
  mEventLog.Write(aEvent, ClassName + ': ' + aMsg);
end;
//------------------------------------------------------------------------------

end.

