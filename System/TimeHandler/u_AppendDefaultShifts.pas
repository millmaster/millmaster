(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: u_AppendDefaultShifts.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: TAppendDefaultShifts
| Info..........: -
| Develop.system: Windows 2000 SP 3
| Target.system.: Windows 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date       | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.09.2002 | 0.00 | LOK| Datei erstellt
|            |      |    | Die Klasse TAppendDefaultShifts wurde aus der Datei
|            |      |    | ShiftCalendars.pas herauskopiert um sie mit TAdoDBAccess
|            |      |    | kompatiebel zu machen
| 11.10.2002         LOK | EOF ADO Conform
|=========================================================================================*)
unit u_AppendDefaultShifts;

interface
uses
  Classes, sysUtils, mmCommonLib, AdoDbAccess;

const
  cDaysPerWeek = 7; //bible
  cMaxShiftsPerDay = 5;
  cMonthsPerYear = 12;
  cMinPerHour = 60;
  cHoursPerDay = 24;
  cMinPerDay = cMinPerHour * cHoursPerDay;
  cTimeSeparator = ':'; //always use the same sp.char indpendent from the language settings
  cMinShiftDuration = 30; //minutes
  cMaxShiftDuration = cMinPerDay;
  cEpsilon = 1E-9;

type
  TAppendDefaultShifts = class(TComponent)
  private
    FCalendarID: longint;
    FDatabase: TAdoDBAccess;
    FHideExceptions: boolean;
    procedure SetCalendarID(Value: longint);
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function Execute: shortint;
  published
    property CalendarID: longint read FCalendarID write SetCalendarID default 0;
    property Database: TAdoDBAccess read FDatabase write FDatabase;
    property HideExceptions: boolean read FHideExceptions write FHideExceptions;
  end; //TAppendDefaultShifts

  eScrollDirectionT = (sdPrev, sdNone, sdNext);

  s5 = string[5];

  rDefaultShiftT = record
    StartTime: array[1..cDaysPerWeek, 1..cMaxShiftsPerDay] of s5;
  end; //rDefaultShiftT

  rShiftCheckT = record
    Year,
      Month,
      Day,
      Shift: word;
    StartTime: double;
    Duration: integer;
    ShiftID: longint;
  end; //rShiftCheckT

function GetDS_ShiftDuration(aDSRec: rDefaultShiftT; aDay, aShift: byte): integer;
function GetMinute(s: string): byte;
function GetHour(s: string): byte;
function GetTimeFromStr(aTimeStr: s5): double;
function GetShiftMonthNo(aYear, aMonth: word): longint;
function GetShiftID(aShiftDate: tdatetime; aShiftInDay: byte): longint;
function GetShiftData(aDB: TAdoDBAccess; aCalID: longint; aShiftID: longint; aDirection: eScrollDirectionT): rShiftCheckT;

implementation
uses
  mmMBCS, mmCS, LoepfeGlobal, MMEventLog;

//-----------------------------------------------------------------------------
function GetDS_ShiftDuration(aDSRec: rDefaultShiftT; aDay, aShift: byte): integer;
var
  xTimeBegin,
    xTimeEnd: double;
  xDayNext,
    xShiftNext: integer;

begin
  xDayNext := aDay;
  xShiftNext := aShift + 1;

  xTimeBegin := GetTimeFromStr(aDSRec.StartTime[aDay, aShift]);
  if (xShiftNext > cMaxShiftsPerDay) or (aDSRec.StartTime[aDay, xShiftNext] = '') then begin
    xShiftNext := 1;
    inc(xDayNext);
    if xDayNext > cDaysPerWeek then xDayNext := 1;
  end; //if (xShiftNext > cMaxShiftsPerDay) or (xDSRec.StartTime[xDay,xShiftNext] = '') then

  xTimeEnd := GetTimeFromStr(aDSRec.StartTime[xDayNext, xShiftNext]);

  if xTimeEnd < (xTimeBegin + cEpsilon) then xTimeEnd := xTimeEnd + 1;
  Result := round((xTimeEnd - xTimeBegin) * cMinPerDay);
end; //function GetDS_ShiftDuration

//common functions, procedures
function GetMinute(s: string): byte;
begin
  s := GetLastStr(s, cTimeSeparator);
  try
    Result := strtoint(s);
  except
    Result := 0;
  end; //try..except
end; //function GetMinute
//-----------------------------------------------------------------------------

function GetHour(s: string): byte;
begin
  s := GetFirstStr(s, cTimeSeparator);
  try
    Result := strtoint(s);
  except
    Result := 0;
  end; //try..except
end; //function GetHour

function GetTimeFromStr(aTimeStr: s5): double;
begin
  aTimeStr := trim(aTimeStr);
  if (aTimeStr <> '') and (aTimeStr <> cTimeSeparator) then begin
    Result := (GetHour(aTimeStr) * cMinPerHour + GetMinute(aTimeStr)) / cMinPerDay;
  end //if (aTimeStr <> '') and (aTimeStr <> cTimeSeparator) then
  else
    Result := 0;
end; //function GetTimeFromStr
//-----------------------------------------------------------------------------

//******************************************************************************
// Faktor aus anzahl Monate seit Sept. 1998 erstellen  -> Faktor = anz. Monte * 1000
//******************************************************************************
function GetShiftMonthNo(aYear, aMonth: word): longint;
const
  cStartYear = 1998;
  cStartMonth = 9;
begin
  Result := ((aYear - cStartYear) * cMonthsPerYear + (aMonth - cStartMonth) + 1) * 1000;
end; //function TShiftCalendarGrid.GetShiftMonthNo
//-----------------------------------------------------------------------------

//******************************************************************************
// ShiftID ermitteln; ist zusammen gesetzt aus Datum
//******************************************************************************
function GetShiftID(aShiftDate: tdatetime; aShiftInDay: byte): longint;
var
  xYear, xMonth, xDay: word;
begin
  DecodeDate(aShiftDate, xYear, xMonth, xDay);
  Result := aShiftInDay + xDay * 10 + GetShiftMonthNo(xYear, xMonth);
end; //function GetShiftID

//************************************************************************************
// Ermittelt alle Daten eines Schichtkalenders, welche aelter (sdPrev) sind als
// ein bestimmtes Datum ( ShiftID ) und fuellt diese in einen Record
// (rShiftCheckT) ab.   SDO
//************************************************************************************
function GetShiftData(aDB: TAdoDBAccess; aCalID: longint; aShiftID: longint; aDirection: eScrollDirectionT): rShiftCheckT;
var
  xQuery: TNativeAdoQuery;

begin
  fillchar(Result, sizeof(Result), 0);

// xQuery := TmmQuery.Create(Application);
  xQuery := TNativeAdoQuery.Create;
  try
  // Connection des Aufrufers �bernehmen
 //   xQuery.Connection := aDB.Connection;
    with xQuery do begin
      SQL.Clear;
      SQL.Add(Format('SELECT * FROM t_shift WHERE c_shiftcal_id = %d', [aCalID]));
      case aDirection of
        sdPrev: begin
            SQL.Add(Format('AND c_shift_id < %d', [aShiftID]));
            SQL.Add('ORDER BY c_shift_id DESC');
          end; //sdPrev
        sdNone: SQL.Add(Format('AND c_shift_id = %d', [aShiftID]));
        sdNext: begin
            SQL.Add(Format('AND c_shift_id > %d', [aShiftID]));
            SQL.Add('ORDER BY c_shift_id');
          end; //sdNext
      end; //case aDirection of

      Open;

      if not Eof then begin // ADO Conform
        DecodeDate(FieldByName('c_shift_start').AsDateTime, Result.Year, Result.Month, Result.Day);
        Result.Shift := FieldByName('c_shift_in_day').AsInteger;
        Result.StartTime := FieldByName('c_shift_start').AsDateTime;
        Result.Duration := FieldByName('c_shift_length').AsInteger;
        Result.ShiftID := FieldByName('c_shift_id').AsInteger;
      end; //if not Eof then
    end; //with xQuery do
  finally
    xQuery.Free;
  end; //try..finally
end; //function GetShiftData

//------------------------------------------------------------------------------
//TAppendDefaultShifts ***
constructor TAppendDefaultShifts.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FCalendarID := 0;
  FDatabase := nil;
  FHideExceptions := False;
  ;
end; //constructor TAppendDefaultShifts.Create
//-----------------------------------------------------------------------------
destructor TAppendDefaultShifts.Destroy;
begin
  inherited Destroy;
end; //destructor TAppendDefaultShifts.Destroy
//-----------------------------------------------------------------------------
procedure TAppendDefaultShifts.SetCalendarID(Value: longint);
begin
  if FCalendarID <> Value then FCalendarID := Value;
end; //procedure TAppendDefaultShifts.SetCalendarID
//-----------------------------------------------------------------------------

//******************************************************************************
// Setzt neue Schichten in den Schichtkaleneder ein
// Es werden pro Schichtkalender, Standardschichten fuer 7 Tage in der
// Zukunft auf die DB geschrieben
//******************************************************************************
function TAppendDefaultShifts.Execute: shortint;
const
  cAddDays = 8;
var
  xDay,
    xShift: integer;
  xQuery: TNativeAdoQuery;
  xStartDate, xEndDate,
    xShiftStart, xDateTime: TDateTime;
  xDSRec: rDefaultShiftT;
  xCheckBegin: rShiftCheckT;
  Year, Month, Day: Word;
  xAddDays: Integer;
begin
  Result := 0;

  if not Assigned(FDatabase) then begin
    Result := -1;
    if FHideExceptions then
      Abort
    else
      raise Exception.Create('Database not assigned!');
  end; //if FDatabase = nil then

  // ACHTUNG: hier hat es ein StartTransaction!
  FDatabase.StartTransaction;
  xQuery := TNativeAdoQuery.Create;
  try
    // Connection des Aufrufers �bernehmen
    xQuery.Assign(fDatabase.Query[cPrimaryQuery]);
    try
      // Get Start and End Date from a shiftcal
      xQuery.Close;
      xQuery.SQL.Text := Format('select distinct max ( c_shift_start ) c_shift_start from t_shift where c_shiftcal_id = %d ', [FCalendarID]);
      xQuery.Open;

      // 0. Init from xStartDate and xEndDate
      // Set begin of shiftdate
      xDateTime := xQuery.FieldByName('c_shift_start').AsDateTime;
      if xDateTime <= 0 then
        xDateTime := Sysutils.Now - 1;
      DecodeDate(xDateTime, Year, Month, Day);
      xStartDate := EncodeDate(Year, Month, Day) + EncodeTime(0, 0, 0, 0) + 1; // + 1 Tg

      // Set end of shiftdate
      xAddDays := StrToIntDef(GetRegString(cRegLM, cRegMMDebug, 'ShiftAddDays', '8'), cAddDays);
      xEndDate := Sysutils.Date + xAddDays + EncodeTime(0, 1, 0, 0);

      //1. read default shift data
      fillchar(xDSRec, sizeof(rDefaultShiftT), 0);
      xQuery.Close; //SDO
      xQuery.SQL.Text := Format('SELECT * FROM t_default_shift WHERE c_shiftcal_id = %d ORDER BY c_default_shift_id', [FCalendarID]);
      xQuery.Open;

      if xQuery.Eof then begin // ADO Conform
        Result := -2;
        if FHideExceptions then
          Abort
        else
          raise Exception.Create('No default shifts found!');
      end; //if xQuery.Eof then

      while not xQuery.Eof do begin // ADO Conform
        xDay := xQuery.FieldByName('c_default_shift_id').AsInteger div 10;
        xShift := xQuery.FieldByName('c_default_shift_id').AsInteger mod 10;
        xDSRec.StartTime[xDay, xShift] := trim(xQuery.FieldByName('c_start_time').AsString);
        xQuery.Next;
      end; //while not xQuery.Eof do
      xQuery.Active := False;

      //3. check shift duration of last record before "begin of copy period"
      // Die juengsten Shiftdaten eines shiftcals ermitteln
      xCheckBegin := GetShiftData(FDatabase, FCalendarID, GetShiftID(xStartDate, 1), sdPrev);
      if xCheckBegin.ShiftID > 0 then begin
        // Wochentag ermitteln -> Mo= 1,  So = 7
        xDay := bDOW(DayOfWeek(xStartDate));
        // Typen Umwandlung von Schichtkalenderdatum (Zelle) in TDateTime
        // -> Schichtbegin = Datum + Zeit aus Schichtkalender
        xShiftStart := xStartDate + GetTimeFromStr(xDSRec.StartTime[xDay, 1]);
        xCheckBegin.Duration := round((xShiftStart - xCheckBegin.StartTime) * cMinPerDay);
        //Schichtdauer muss zwischen 30 und 1440
        if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then begin
          Result := -4;
          if FHideExceptions then
            Abort
          else
            raise Exception.CreateFmt('Shift duration of %d/%d/%d - %d', [xCheckBegin.Day, xCheckBegin.Month, xCheckBegin.Year, xCheckBegin.Shift]);
        end; //if (xCheckBegin.Duration < cMinShiftDuration) or (xCheckBegin.Duration > cMaxShiftDuration) then
      end //if xCheckBegin.ShiftID > 0 then
      else begin
        // da ja keine alten Schichtdaten vorhanden sind (Jungfr�uliches Startup) muss der Start f�r die Schichteintragungen
        // einen Tag vorgeschoben werden
        WriteToEventLogDebug('First startup: dec 1 day into past for first shift!!', 'TAppendDefaultShifts.Execute', etWarning);
        xStartDate := xStartDate - 1;
      end;

      //4. write default shift data into t_shift
      while xStartDate <= xEndDate do begin
        xDay := bDOW(DayOfWeek(xStartDate));

        for xShift := 1 to cMaxShiftsPerDay do
          if xDSRec.StartTime[xDay, xShift] <> '' then begin
            xQuery.Close; //SDO
            xQuery.SQL.Text := 'INSERT INTO t_shift(c_shift_id,c_shiftcal_id,c_shift_in_day,c_shift_start,c_shift_length) ' +
                               'VALUES(:shift_id,:shiftcal_id,:shift_in_day,:shift_start,:shift_length)';
            xQuery.Params.ParamByname('shift_id').AsInteger := GetShiftID(xStartDate, xShift);
            xQuery.Params.ParamByname('shiftcal_id').AsInteger := FCalendarID;
            xQuery.Params.ParamByname('shift_in_day').AsInteger := xShift;
            xQuery.Params.ParamByname('shift_start').AsDateTime := xStartDate + GetTimeFromStr(xDSRec.StartTime[xDay, xShift]);
            xQuery.Params.ParamByname('shift_length').AsInteger := GetDS_ShiftDuration(xDSRec, xDay, xShift);
            xQuery.ExecSQL;
          end; //for xShift := 1 to cMaxShiftsPerDay do

        xStartDate := xStartDate + 1;
      end; //while xStartDate <= xEndDate do

      //5. update shift duration of last record before "begin of copy period"
      if xCheckBegin.ShiftID > 0 then begin
        xQuery.Close; //SDO
        xQuery.SQL.Text := Format('UPDATE t_shift SET c_shift_length = %d WHERE c_shiftcal_id = %d AND c_shift_id = %d', [xCheckBegin.Duration, FCalendarID, xCheckBegin.ShiftID]);
        xQuery.ExecSQL;
      end; //if xCheckBegin.ShiftID > 0 then

      FDatabase.Commit;
    except
      FDatabase.Rollback;
      Result := -5;
      if Result < 0 then
        raise
      else
        raise Exception.Create('Error inserting new shifts!');
    end; //try..except
  finally
    xQuery.Free;
    if FDatabase.InTransaction then
      FDatabase.Rollback;
  end; //try..finally
end; //function TAppendDefaultShifts.Execute
//------------------------------------------------------------------------------

end.

 