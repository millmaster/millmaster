(*==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: AC338INFClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 18.04.2000  1.00  wss | Get_Data/Put_Data mit temp. Buffer ausgestattet wegen Alignment von C
// 16.08.2000  1.00  wss | In Connect the mJobAcqQueue wasn't freed at new connect -> memory leak
// 12.09.2001  1.01  khp | Informator V5.3x: SetMMPartieNummer tText100
// 20.11.2001  1.02  khp | Anpassung in SetMMPartieparameter:
// 20.11.2001  1.02  khp | SetMMPartieparameter: Konvertieren von LoepfeGarnNr in WSCGarnNr
// 15.03.2002  1.03  khp | Anpassung an Partiename auf DB, TText100 (Chinesisch UTF8)
// 13.05.2004  1.03  Wss | mMaschID -> mMachID umbenannt
// 19.05.2005  1.03  khp | Detailierte Fehlermeldung f�r CPMMSS DLL
// 08.11.2006  1.04  nue | GetMaxAnzGroups eingef�gt.
//=========================================================================================*)

unit AC338INFClass;

interface

uses Sysutils, Windows, Winsock, Forms, Messages, contnrs,
  AC338DLL_DEF, IPCClass, CPMMSSDLL_DEF, BaseGlobal, LoepfeGlobal,
  mmEventLog,YMParaDef;

type
//  TText100 = array[1..100] of Char;     // Fuer Informator V5.3x   ist neu im MMUGlobal definiert

  TWscDevHandler = class(TObject)
  private
    mAccesskey: taccesskey;
    mClient: PHCLIENT;
  public
    mConnected: Boolean;
  private
    mCount: Integer;
    mDataItemIndex: TDataItemIndex;
    mDb_handle: Integer;
    mGroup: tnetworkgroup;
    mInformWndHandle: HWND;
  public
    mInformWndMsg: UINT;
    mInform_socket: Integer;
    mIpAdresse: PChar;
    mJobAcqQueue: TQueue;
    mMachID: Integer;
  private
    mTypedesc: array[0..1023] of Byte;
  public
    constructor Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer; const aIpAdresse: PChar; const aMaschID: Integer);
    destructor Destroy; override;
    function CheckInform(aMessage: UINT; aLParam: LPARAM; var aDataItemIndex: Integer; var aOffset: Integer): Boolean;
    function Connect(): Boolean;
    function Disconnect(): Boolean;
    function GetData(aDataItemIndex: Integer; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer; aArg2: Integer): Boolean; overload;
    function GetData(aDataItemName: String; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer; aArg2: Integer): Boolean; overload;
    function GetIndex(aDataItemName: PChar): TDataItemIndex;
    function GetInformation(aDataItemName: PChar; var aDataItemIndex: Integer; aTypedescriptor: PChar): Boolean;
    function GetUserlevel(var aUserlevel: tUserlevel): Boolean;
    function Inform(aDataItemIndex: Integer): Boolean;
    function InformOff(aDataItemName: PChar): Boolean;
    function InformON(aDataItemName: PChar): Boolean;
    function Lifecheck(): Boolean;
    function GetMaxAnzGroups: integer;
    function PutData(aDataItemName: String; aPBuffer: PByte;
      aBufferSize, aArg1, aArg2: Integer): Boolean;
    function SetWSCPartieBereich(aGrpNo: Integer; aFirstSpd, aLastSpd: Integer): Boolean;
    function SetWSCPartieFadenzahl(aGrpNo: Integer; aFadenzahl: Integer): Boolean;
    function SetWSCPartieGarnEinheit(aGrpNo: Integer; aDimension: integer): Boolean;
    function SetWSCPartieGarnNr(aGrpNo: Integer; aGarnfeinheit: single): Boolean;
    // Methoden die fuer CSMMSS.Dll benoetigt werden

    function SetWSCPartieName(aGrpNo: Integer; aPartiename: TText100): Boolean;
    function SetWSCPartieNummer(aGrpNo: Integer; aPartieNummer: string): Boolean;
    function SetWSCPartieparameter(aGrpNo: Integer; aPartiename: TText100; aPartienummer: String;
      aFirstSpd, aLastSpd: Integer; aGarnfeinheit: single; aFadenzahl: Integer): Boolean;
  end;

  //==============================================================================



implementation                          { TWscDevHandler } // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmcs;



constructor TWscDevHandler.Create(const aThreadWndHandle: HWND; const aInformWndMsg: Integer;
  const aIpAdresse: PChar; const aMaschID: Integer);
begin
  inherited create;
  mIpAdresse       := aIpAdresse;
  mJobAcqQueue     := nil;
  mMachID          := aMaschID;
  mInformWndMsg    := aInformWndMsg;
  mInformWndHandle := aThreadWndHandle;
  mConnected       := false;
  mCount           := 0;
end;

//------------------------------------------------------------------------------

destructor TWscDevHandler.Destroy;
// Exception werden abgefangen
begin
  try
    if mConnected = True then
      Disconnect;
  except
  end;
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.CheckInform(aMessage: UINT; aLParam: LPARAM;
  var aDataItemIndex, aOffset: Integer): Boolean;
// Bestimmt Index und Offset eines Inform Events
// Exception werden abgefangen
var
  xInfomsg: tinfomsg;
  xRes: tdba_result;
begin
  try
    result := false;
    if (aMessage = mInformWndMsg) then begin
      WSAGETSELECTEVENT(aLParam);
      case aLParam of
        FD_READ: begin                  // welches Datum hat sich geaendert ???
            aDataItemIndex := -1;
            aOffset        := -1;
            try
              xRes := rdba_get_information(mDb_handle, mInform_socket, xInfomsg, mClient);
            finally
            end;
            aDataItemIndex := xInfomsg.DataItemIndex;
            aOffset        := xInfomsg.offset;
            if (xRes = DBA_OK) then result := true
                               else raise Exception.Create('rdba_get_information');
          end;
        FD_WRITE, FD_OOB, FD_ACCEPT, FD_CONNECT, FD_CLOSE: Sleep(1);
      else
      end;
    end;
  except
    on E: Exception do raise Exception.Create('CheckInform failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.Connect: Boolean;
// Baut Verbindung zu Informator auf, ein dBHandle und ein InformHandle werden zur�ckgegeben.
// falls erste Verbindung wird Distinct Dll geladen und in 'Tray Liste' Distinct Kugel angezeigt.
// Alle Informs werden dem spezifizierten Fenster geschickt
// Alle benoetigten Informs werden aktiviert
// Exception werden abgefangen

var
  xRes: Integer;
  xVal: tdba_result;
begin
  if Assigned(mJobAcqQueue) then
    mJobAcqQueue.Free;

  mJobAcqQueue := TQueue.Create;
  try
    mGroup     := 1;
    mConnected := false;
    try
      xVal := rdba_init(mIpAdresse, mGroup, mDb_handle, mInform_socket, mAccesskey, mClient);
      if xVal <> DBA_OK then begin
        CodeSite.SendMsg('Connect Fehlgeschlagen. 1 Sek warten und nochmals probieren');
        Sleep(1000);
        xVal := rdba_init(mIpAdresse, mGroup, mDb_handle, mInform_socket, mAccesskey, mClient);
      end;
    finally
    end;
    if (xVal <> DBA_OK) then
      raise Exception.Create('rdba_init');
    // Socket wird in den nicht blockierenden Mode geschaltet. Das Ereignis FD_READ wird als
    // mInformWndMsg Nachricht gekennzeichnet an das Fenster mit mInformWndHandle geschickt.
    xRes := WSAAsyncSelect(mInform_socket, mInformWndHandle, mInformWndMsg, FD_READ);
    if (xRes < 0) then
      raise Exception.Create('WSAAsyncSelect');
    // Alle Informs aktivieren
    InformOn(PChar(MM_DATA_TRANSFER));
    InformOn(PChar(MM_DECLARATION));
    mConnected := true;
    result := true;
  except
    on E: Exception do begin
      raise Exception.Create('Connect ' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.Disconnect: Boolean;
// Schliesst Verbindung zu Informator, falls letzte Verbindung abebrochen wird
// verschwindet Distinct Kugel.(Distinct DLL wird entfernt)
// Exception werden abgefangen
var
  xRes: tdba_result;
begin

  mConnected := false;
  try
    result := false;
    xRes := DBA_ERROR;
    if (mClient <> nil) then begin
      try
        xRes := rdba_terminate(mDb_handle, mInform_socket, mClient);
      finally
      end;
    end;
    if (xRes = DBA_OK) then result := true;
  except
    on E: Exception do begin
      raise Exception.Create('Disconnect ');
    end;
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.GetData(aDataItemIndex: Integer; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer; aArg2: Integer): Boolean;
// Zugriff auf Informator DatumItem mittels aDataItemIndex,
// aArg1 Spindel = 0..59 / GruppenNr.= 0..5 ; aArg2 Aktueller= 0 / Hintergrundwert = 1
// aTypedescriptor Netzwerk-Beschreibung des Datums
// Resultat: aPBuffer
// Exception werden abgefangen
// 12.04.2000 Nue: Wegen Problem mit geraden Adressen (Alignment) daten
//  in temporaeren Buffer kopiert
var
  xTmpBuff: PByte;
  xRes: tdba_result;
begin
  try
    result := false;
    if (mClient <> nil) then begin
      xTmpBuff := AllocMem(aBufferSize);
      try
        xRes := rdba_read(mDb_handle, aDataItemIndex, aArg1, aArg2, MAX_TYPE_DESC, aBufferSize, @mTypedesc, xTmpBuff, mClient);
        if (xRes = DBA_OK) then begin
          result := true;
          System.Move(xTmpBuff^, aPBuffer^, aBufferSize);
        end
        else
          raise Exception.Create('rdba_read');
      finally
        FreeMem(xTmpBuff, aBufferSize);
      end;
    end;
  except
    on E: Exception do raise Exception.Create('GetData failed ' + e.message)
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.GetData(aDataItemName: String; aPBuffer: PByte; aBufferSize: Integer; aArg1: Integer; aArg2: Integer): Boolean;
// Zugriff auf Informator DatumItem mittels aDataItemIndex,
// aArg1 Spindel = 0..59 / GruppenNr.= 0..5 ; aArg2 Aktueller= 0 / Hintergrundwert = 1
// aTypedescriptor Netzwerk-Beschreibung des Datums
// Resultat: aPBuffer
// Exception werden abgefangen
// 12.04.2000 Nue: Wegen Problem mit geraden Adressen (Alignment) daten
//  in temporaeren Buffer kopiert
var
  xIndex: TDataItemIndex;
begin
  xIndex := Getindex(PChar(aDataItemName));
  Result := GetData(xIndex, aPBuffer, aBufferSize, aArg1, aArg2);
end;

//------------------------------------------------------------------------------

function TWscDevHandler.Getindex(aDataItemName: PChar): TDataItemIndex;
// Gibt DataItemIndex zum DataItemName zurueck.
// Exception werden abgefangen
var
  res: tdba_result;
begin
  try
    result := -1;
    if (mClient <> nil) then begin
      try
        // Index holen
        res := rdba_ident(mDb_handle, aDataItemName, mDataItemIndex, mClient);
      finally
      end;
      if (res = DBA_OK) then result := mDataItemIndex
      else raise Exception.Create('rdba_ident');
    end;
  except
    on E: Exception do raise Exception.Create('Getindex failed ' + e.message)
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.GetInformation(aDataItemName: PChar; var aDataItemIndex: TDataItemIndex;
  aTypedescriptor: PChar): Boolean;
// Diese Methode wird vor Put Data benoetigt um den Typdescriptor des DataItems zu bestimmen
// Mittels aDataItemName wird aDataItemIndex und Typedescriptor bestimmt.
// Resultate: aDataItemIndex und aTypedescriptor
// Exception werden abgefangen
var
  xDataname: array[0..127] of Char;     // Buffer fuer Name Datenelement
  xLimittype: array[0..MAX_TYPE_DESC - 1] of Char; // Buffer Typebeschr. Grenzwerte
  xNetworkattribut: tnetworkattribut;   // Netzwerk-Zugriffsberechtigung
  xUserattribut: tuserattribut;         // Anwenderberechtigung
  xDimension: tdimension;               // Dimension des Datenelements
  xLimitsize: tlimitsize;               // Groesse Grenzwerte
  xDatasize: tdatasize;                 // Groesse Datenwert
  xres: tdba_result;
begin
  try
    result := false;
    if (mClient <> nil) then begin
      try
        // Index holen
        xres := rdba_ident(mDb_handle, aDataItemName, aDataItemIndex, mClient);
      finally
      end;
      if (xres <> DBA_OK) then raise Exception.Create('rdba_ident');
      try
        xres := rdba_description(mDb_handle, aDataItemIndex, sizeof(xDataname),
          MAX_TYPE_DESC, sizeof(xLimittype),
          xDataname, aTypedescriptor, xLimittype,
          xNetworkattribut, xUserattribut,
          xDimension, xLimitsize, xDatasize, mClient);
      finally
      end;
      if (xres = DBA_OK) then result := true
      else raise Exception.Create('rdba_description');
    end;
  except
    on E: Exception do raise Exception.Create('GetInformation failed ' + e.message)
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.GetUserlevel(var aUserlevel: tUserlevel): Boolean;
// Exception werden abgefangen
var
  xRes: tdba_result;
  // Passwortlevel via zugriff auf Datum akt_zugriff
begin
  try
    result := false;
    aUserlevel := NIEMAND;
    if (mClient <> nil) then begin
      try
        xRes := rdba_get_userlevel(mDb_handle, aUserlevel, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := true
      else raise Exception.Create('rdba_get_userlevel');
    end;
  except
    on E: Exception do raise Exception.Create('GetUserlevel failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.Inform(aDataItemIndex: Integer): Boolean;
// Aktiviert Inform auf bestimmtem Datum mittels aDataItemIndex
// Exception werden abgefangen
var
  xRes: tdba_result;
  xSignalcode: tsignalcode;
  xFlag: tinformflag;                   //	0 = setzen, 1 = loeschen
begin
  try
    result := false;
    xSignalcode := -1;                  //	-1 = Network
    xFlag := INFORM_SET;                //	0 = setzen, 1 = loeschen
    if (mClient <> nil) then begin
      try
        // inform setzen
        xRes := rdba_inform(mDb_handle, aDataItemIndex, xSignalcode, xFlag, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := true
      else raise Exception.Create('rdba_inform');
    end;
  except
    on E: Exception do raise Exception.Create('Inform failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.InformOff(aDataItemName: PChar): Boolean;
// Deaktiviert Inform auf bestimmtem Datum mittels aDataItemName
// Exception werden abgefangen
var
  xRes: tdba_result;
  xSignalcode: tsignalcode;
  xFlag: tinformflag;                   // O = setzen, 1 = loeschen
  xDataItemIndex: TDataItemIndex;       // Lokaler DataItemindex
begin
  try
    result := false;
    xSignalcode := -1;                  //	-1 = Network
    xFlag := INFORM_CLEAR;
    if (mClient <> nil) then begin
      try
        xres := rdba_ident(mDb_handle, aDataItemName, xDataItemIndex, mClient);
      finally
      end;
      if (xRes <> DBA_OK) then raise Exception.Create('rdba_ident');
      // inform setzen
      try
        xRes := rdba_inform(mDb_handle, xDataItemIndex, xSignalcode, xFlag, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := true
      else raise Exception.Create('rdba_inform');
    end;
  except
    on E: Exception do raise Exception.Create('InformOff failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.InformON(aDataItemName: PChar): Boolean;
// Aktiviert Inform auf bestimmtem Datum mittels aDataItemName
// Exception werden abgefangen
var
  xRes: tdba_result;
  xSignalcode: tsignalcode;
  xFlag: tinformflag;                   // O = setzen, 1 = loeschen
begin
  try
    result := false;
    xSignalcode := -1;                  //	-1 = Network
    xFlag := INFORM_SET;                //	0 = setzen, 1 = loeschen
    if (mClient <> nil) then begin
      try
        xres := rdba_ident(mDb_handle, aDataItemName, mDataItemIndex, mClient);
      finally
      end;
      if (xRes <> DBA_OK) then raise Exception.Create('rdba_ident');
      // inform setzen
      try
        xRes := rdba_inform(mDb_handle, mDataItemIndex, xSignalcode, xFlag, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := true
      else raise Exception.Create('rdba_inform');
    end;
  except
    on E: Exception do raise Exception.Create('InformON failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.Lifecheck: Boolean;
// Beantwortet Informator-Anfrage mit einem beliebigen RPC Zugriff
// Exception werden abgefangen
var
  xRes: tdba_result;
begin
  try
    result := false;
    if (mClient <> nil) then begin
      try
        xRes := rdba_lifecheck(mDb_handle, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := true
      else raise Exception.Create('rdba_lifecheck');
    end;
  except
     on E: Exception do raise Exception.Create('Lifecheck failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.GetMaxAnzGroups: integer;
// Gibt die Anzahl der maximal m�glichen Gruppen auf der WSC zur�ck.
// Exception werden abgefangen
var
  xRes: tdba_result;
  xAnz: WORD;
begin
  try
    result := cOldWSCSpdGroupLimit;
    if (mClient <> nil) then begin
      try
        xRes := AC338DLL_DEF.GetMaxAnzGroups(mDb_handle, xAnz, mClient);
      finally
      end;
      if (xRes = DBA_OK) then result := xAnz
      else raise Exception.Create('GetMaxAnzGroups');
    end;
  except
     on E: Exception do raise Exception.Create('GetMaxAnzGroups failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.PutData(aDataItemName: String; aPBuffer: PByte;
  aBufferSize, aArg1, aArg2: Integer): Boolean;
// Speichern eines DataItems auf Informator mittels aDataItemName.
// Mittels aDataItemName wird DataItemIndex und Typedescriptor bestimmt.
// aArg1 Spindel / GruppenNr. ; aArg2 Aktueller- / Hintergrundwert
// Exception werden abgefangen
var
  xDataname: array[0..127] of Char;     // Buffer fuer Name Datenelement
  xLimittype: array[0..MAX_TYPE_DESC - 1] of Char; // Buffer Typebeschr. Grenzwerte
  xNetworkattribut: tnetworkattribut;   // Netzwerk-Zugriffsberechtigung
  xUserattribut: tuserattribut;         // Anwenderberechtigung
  xDimension: tdimension;               // Dimension des Datenelements
  xLimitsize: tlimitsize;               // Groesse Grenzwerte
  xDatasize: tdatasize;
  xRes: tdba_result;
  xTmpBuff: PByte;
begin
  try
    result := false;
    if (mClient <> nil) then begin
      // zuerst den Index f�r diesen Befehl holden
      try
        xres := rdba_ident(mDb_handle, PChar(aDataItemName), mDataItemIndex, mClient);
      finally
      end;
      if (xres <> DBA_OK) then raise Exception.Create(' rdba_ident: ' + aDataItemName);
      try
        // bei g�ltigem Index dann Informationen zu diesem Datenelement holen...
        xres := rdba_description(mDb_handle, mDataItemIndex, sizeof(xDataname),
                                 MAX_TYPE_DESC, sizeof(xLimittype),
                                 xDataname, @mTypedesc, xLimittype,
                                 xNetworkattribut, xUserattribut,
                                 xDimension, xLimitsize, xDatasize, mClient);
      finally
      end;
      if (xres <> DBA_OK) then raise Exception.Create('rdba_description');

      //...und dann die Daten schreiben
      xTmpBuff := AllocMem(aBufferSize);
      try
        System.Move(aPBuffer^, xTmpBuff^, aBufferSize);
        xRes := rdba_write(mDb_handle, mDataItemIndex, aArg1, aArg2, @mTypedesc, aBufferSize, xTmpBuff, mClient);
      finally
        FreeMem(xTmpBuff, aBufferSize);
      end;
      if (xRes = DBA_OK) then Result := True
                         else raise Exception.Create('rdba_write');
    end;
  except
    on E: Exception do raise Exception.Create('PutData failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieBereich(aGrpNo: Integer; aFirstSpd, aLastSpd: Integer): Boolean;
var
  xRes: cpmmss_result;
begin
  result := false;
  try
    xRes := set_partiebereich(mIpAdresse, aGrpNo, aFirstSpd, aLastSpd);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieBereich failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
    on E: Exception do raise Exception.Create('SetWSCPartieBereich failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieFadenzahl(aGrpNo: Integer; aFadenzahl: Integer): Boolean;
var
  xRes: cpmmss_result;
begin
  result := false;
  try
    xRes := set_fadenzahl(mIpAdresse, aGrpNo, aFadenzahl);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieFadenzahl failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
    on E: Exception do raise Exception.Create('SetWSCPartieFadenzahl failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieGarnEinheit(aGrpNo: Integer; aDimension: integer): Boolean;
var
  xRes: cpmmss_result;
begin
  result := false;
  try
    xRes := set_dim_garnfeinheit(mIpAdresse, aGrpNo, aDimension);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieGarnEinheit failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
    on E: Exception do raise Exception.Create('SetWSCPartieGarnEinheit failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieGarnNr(aGrpNo: Integer; aGarnfeinheit: single): Boolean;
var
  xRes: cpmmss_result;
begin
  result := false; 
  try
    xRes := set_garnfeinheit(mIpAdresse, aGrpNo, aGarnfeinheit);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieGarnNr failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
    on E: Exception do raise Exception.Create('SetWSCPartieGarnN failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

// Bei der Verwendung der CPMMSS.DLL muss folgedes beachtet werden
// Jeder fehelerhaft Zugriff darf nicht zu einem Disconnect der YM Datenbank fuehren.
// Daher werden die Exception nicht nach oben weiter gefuehrt.
// Der Fehler wird protokolliert.
// Falls CPMMSS.DLL nicht existiert wir die Funktion trotzdem mit ok gestaetigt.

function TWscDevHandler.SetWSCPartieName(aGrpNo: Integer; aPartiename: TText100): Boolean;
var
  xRes: cpmmss_result;
  xPartiename: TText100;
begin
  result := false;
  try
    //Umcopieren von tText50 auf tText100
    //Fillchar(xPartiename, sizeof(xPartiename), 0);
    //strcopy(@xPartiename, @aPartiename);
    xPartiename:= aPartiename;
    xRes := set_partiename(mIpAdresse, aGrpNo, @xPartiename);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieName failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
    on E: Exception do raise Exception.Create('SetWSCPartieName failed ' + e.message);
  end;
end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieNummer(aGrpNo: Integer; aPartieNummer: string): Boolean;
var
  xRes: cpmmss_result;
  xPartienummer: TText100;
begin
  result := false;
  //Umcopieren von string auf tText100
  Fillchar(xPartienummer, sizeof(xPartienummer), 0);
  StrPCopy(@xPartienummer, aPartienummer);
  try
    xRes := set_partienummer(mIpAdresse, aGrpNo, @xPartienummer);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if result = false then
      WriteToEventLog('SetWSCPartieNummer failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
   on E: Exception do raise Exception.Create('SetWSCPartieNummer failed ' + e.message);
  end;
 end;

//------------------------------------------------------------------------------

function TWscDevHandler.SetWSCPartieparameter(aGrpNo: Integer; aPartiename: TText100; aPartienummer: string;
  aFirstSpd, aLastSpd: Integer; aGarnfeinheit: single; aFadenzahl: Integer): Boolean;
// WSC arbeitet auf der Datenbank nur mit Nm. Daher 'aGarnfeinheit' immer in Nm uebergeben.
// WSCGarnNr = EinzelfadenNr, LoepfeGarnNr = GesamtfadenNr
// WSCGarnNr = LoepfeGarnNr * fadenzahl
var
  xRes: cpmmss_result;
  xPartiename, xPartienummer: TText100;
begin
  Result := false;
  xPartiename:= aPartiename;
  //Umcopieren von string auf tText100
  Fillchar(xPartienummer, sizeof(xPartienummer), 0);
  StrPCopy(@xPartienummer, aPartienummer);
  try
    xRes := set_group_parameter(mIpAdresse, aGrpNo, @xPartiename, @xPartienummer,
                                aFirstSpd, aLastSpd, aGarnfeinheit * aFadenzahl, aFadenzahl);
    result := (xRes = CPMMSS_OK) or (xRes = CPMMSS_CP_NOTTHERE);
    if not Result then
      WriteToEventLog('SetWSCPartieParameter failed, '+ Getcpmmss_Errortext(xRes), ' CPMMSS.DLL ' + mIpAdresse, etError, '', '', ssWSCHandler);
  except
     on E: Exception do raise Exception.Create('SetWSCPartieParameter failed ' + e.message);
  end;
end;


begin
end.

