Hallo Dani

Habe die AC338RPC.DLL erweitert um die Funktion GetMaxAnzGroups() zur Bestimmung der maximalen Anzahl Gruppen auf den Informatoren.

Die neue DLL hat die Versionsnummer 9.0.0.0 .

Gruss
Isidor

tdba_result GetMaxAnzGroups(const	tdbhandle dbhandle, short* pMaxAnzGroups, const	PHCLIENT	pClient)
