{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_DelShiftByTimeAdmin.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Delete all shift
| Info..........: Read from Reg. via mSettings
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function Emergency() inserted
| 17.11.1999  1.02  SDO | Reading from ConstArray (TSettingsReader)
|                       | MMSettings.Value in GetDelShiftDate
| 05.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 25.11.2005        Lok | Hints und Warnings entfernt
|==============================================================================}
unit u_DelShiftByTimeAdmin;

interface

uses Classes, SysUtils, Windows,
  MMBaseClasses, BaseGlobal, BaseMain,
  mmEventLog, u_BaseAdmin, u_TimeJobDB, SettingsReader;

type
  TDelShiftByTimeAdmin = class(TBasexAdministrator)
  private
    mTimeDelShiftEV: Integer;
    mExecuteTyp: TExecuteTypEnum;
    mJobTyp: TJobTyp;
    mPriority: Byte;
    function GetDelShiftDate: Integer;
    function InsertDelShiftByTime(aTime: Integer; aExecuteTyp: TExecuteTypEnum; aJobTyp: TJobTyp; aPriority: Byte): Boolean;
  public
    function Init: Boolean; override;
    function StartupInit: Boolean; override;
    function Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean; override;
    function PrepareJob(aJob: PJobRec): Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  LoepfeGlobal, AdoDBAccess;

const
  cDelDaysFromActDate = 'select dateadd ( Day, :DelDays, getdate () ) newDate';

//******************************************************************************
// Prepare only a job, if the time has change
//******************************************************************************
function TDelShiftByTimeAdmin.Emergency(aTimeStampOld,
  aTimeStampAct: TTimeStamp): Boolean;
begin
  // ?
  Result := Init;
  if Result then Result := StartupInit;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Read days from registry
//******************************************************************************
function TDelShiftByTimeAdmin.GetDelShiftDate: Integer;
//var xregist : TRegistry;
begin
  Result := 0;
  try
    Result := StrToInt(MMSettings.Value[cDaysForDelShift]);
  except
    on E: Exception do
      WriteLog(etError, 'TDelShiftByTimeAdmin.GetDelShiftDate failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function TDelShiftByTimeAdmin.Init: Boolean;
var
//  xHour, xMin: Word;
  xTimeMs: TTimeStamp;
begin
  if not Initialized then begin
    mExecuteTyp := etDAILY;
    mJobTyp     := jtDelShiftByTime;
    mPriority   := 1;

    // Execute-Time  from DelShiftByTime Event
    xTimeMs := DateTimeToTimeStamp(EncodeTime(5, 30, 0, 0));
    mTimeDelShiftEV := xTimeMs.Time;
  end;
  Result := inherited Init;

{
  Result:= inherited init;

  mExecuteTyp:=etDAILY;        // ExequteTyp
  mJobTyp:=jtDelShiftByTime; // JobTyp
  mPriority:=1;              // Priority

  // Execute-Time  from DelShiftByTime Event
  xHour:=5; xMin:=30;
  xTimeMs:=DateTimeToTimeStamp(EncodeTime(xHour, xMin , 0, 0));
  mTimeDelShiftEV:=xTimeMs.Time;

  //MMSettings.Init;

  if Result then
     mInitialized:=TRUE
  else
     mAdminEventLog.Write(etWarning, ' Init not initialized in ' +
                                self.ClassName + '.');

  Result:=TRUE;
{}
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a new entry of DelShiftByTime in table t_Time_JobList
//******************************************************************************
function TDelShiftByTimeAdmin.InsertDelShiftByTime(aTime: Integer;
  aExecuteTyp: TExecuteTypEnum; aJobTyp: TJobTyp;
  aPriority: Byte): Boolean;
begin
  Result := False;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('INSERT INTO t_time_joblist ');
    SQL.Add(' (c_time_joblist_id, c_execute_type, c_time, c_week_day, ');
    SQL.Add('  c_month_day, c_month, c_priority, c_Job_type) ');
    SQL.Add('VALUES( :c_time_joblist_id, :c_execute_type, :c_time, :c_week_day, ');
    SQL.Add('       :c_month_day, :c_month, :c_priority, :c_Job_type ) ');
    ParamByName('c_execute_type').AsInteger := ord(aExecuteTyp);
    ParamByName('c_time').AsInteger := aTime;
    ParamByName('c_priority').AsInteger := aPriority;
    ParamByName('c_Job_type').AsInteger := ord(aJobTyp);

    ParamByName('c_week_day').asString := '';
    ParamByName('c_month_day').asString := '';
    ParamByName('c_month').asString := '';

    InsertSQL('t_time_joblist', 'c_time_joblist_id', MaxInt, 1);
    Result := True;
  except
    on e: Exception do begin
      WriteLog(etError, 'TDelShiftByTimeAdmin.InsertDelShiftByTime failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Read DelShiftByTime days from registry
//******************************************************************************
function TDelShiftByTimeAdmin.PrepareJob(aJob: PJobRec): Boolean;
var
  xText : string;
begin
  Result := False;
  xText := '';
  case Initialized of
    True: xText := 'Initialized = TRUE;   ';
    False: xText := 'Initialized = FALSE;   ';
  end;

  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cDelDaysFromActDate;
    Params.ParamByName('DelDays').ADOType   := adInteger;
    Params.ParamByName('DelDays').AsInteger := -GetDelShiftDate;
    Open;
    aJob.JobTyp := jtDelShiftByTime;
    aJob.DelShiftByTime.DateTime := FieldByName('newDate').AsDateTime;
    Close;
    Result := True;
  except
    on e: Exception do begin
      WriteLog(etError, 'TDelShiftByTimeAdmin.PrepareJob failed: ' + e.Message);
//      Result := False;
//      xText := 'TDelShiftByTimeAdmin.PrepareJob failed. Error msg : ' + e.message;
//      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, xText);
//      mAdminEventLog.Write(etError, xText);
    end;
  end;
//  Result := True;
end;

//------------------------------------------------------------------------------

//******************************************************************************
// Check entry of DelShiftByTime in table t_Time_JobList.
// If doesn't exist then insert the DelShiftByTime-days
//******************************************************************************
function TDelShiftByTimeAdmin.StartupInit: Boolean;
begin
  Result := False;

  // Überprüfen, ob DelShiftByTime in der JobList Tabelle vorhanden ist
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
//    SQL.Add('SELECT c_Job_Type FROM t_time_joblist ');
//    SQL.Add('WHERE c_Job_Type = :c_Job_type');
    SQL.Text := 'SELECT c_Job_Type FROM t_time_joblist WHERE c_Job_Type = :c_Job_type';
    ParamByName('c_Job_type').AsInteger := ord(mJobTyp);
    Open;
    // Insert DelShiftByTime in table 't_time_joblist if not exists'
    Result := FindFirst;
    if not Result then // ADO Conform
      // wenn nicht, dann neuen Eintrag in Tabelle machen
      Result := InsertDelShiftByTime(mTimeDelShiftEV, mExecuteTyp, mJobTyp, mPriority);
    Close;
  except
    on e: Exception do begin
      WriteLog(etError, 'TDelShiftByTimeAdmin.StartupInit failed: ' + e.Message);
    end;
  end; // with mDB

{
  Result := True;

 // Exists 'DelShiftByTime EV' in table 't_time_joblist' ?

  with mDB.Query[cPrimaryQuery] do begin
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('SELECT c_Job_Type FROM t_time_joblist ');
    SQL.Add('WHERE c_Job_Type = :c_Job_type');
    ParamByName('c_Job_type').AsInteger := ord(mJobTyp);
    try
      OPEN;
    except
      on e: Exception do begin
        mAdminEventLog.Write(etError, ' StartupInit error in ' +
          self.ClassName + '. Could not open database. ' + e.Message);
        Result := False;
        exit;
      end;
    end;

//   FetchAll;
   // Insert DelShiftByTime in table 't_time_joblist if not exists'
    if EOF then // ADO Conform
      Result := InsertDelShiftByTime(mTimeDelShiftEV, mExecuteTyp,
        mJobTyp, mPriority);
  end; // end with
{}
end;
//------------------------------------------------------------------------------

end.

