{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_TimeHandler.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: MainClass from TimeHandler,
|                 Build TJobExecute- and TTimeTicker-Thread
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 11.04.2001  2.06  Nue | TBaseSettingsReader ==> TMMSettingsReader
| 05.10.2002        LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|==============================================================================}
unit u_TimeHandler;

interface

uses
  mmThread, Classes, SysUtils, BaseMain, BaseGlobal, mmEventLog, LoepfeGlobal,
  SettingsReader;


type
  TTimeHandler =class(TBaseMain)
  private
  protected
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; override;
    function getParamHandle : TMMSettingsReader; override;
  public
    function Initialize: Boolean;  override;
    constructor Create(aSubSystemTyp: TSubSystemTyp); override; //virtual;
    destructor Destroy; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  u_JobExecute, u_TimeTicker;

//-----------------------------------------------------------------------------
constructor TTimeHandler.Create(aSubSystemTyp: TSubSystemTyp);
begin
  inherited Create(aSubSystemTyp);
end;
//-----------------------------------------------------------------------------
function TTimeHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttJobExecute: begin
        aThread := TJobExecute.Create(mSubSystemDef.Threads[1]);
      end;
    ttTimeTicker: begin
        aThread := TTimeTicker.Create(mSubSystemDef.Threads[2]);
      end;
  else
    aThread := Nil;
    Result := False;
  end;
 end;
//------------------------------------------------------------------------------
destructor TTimeHandler.Destroy;
begin
 inherited Destroy;
end;
//------------------------------------------------------------------------------
function TTimeHandler.Initialize: Boolean;
var
  xFileVersion: string;
begin
  Result := inherited Initialize;
  if Result then begin
    xFileVersion := 'unknown';
    try
      xFileVersion := GetFileVersion(ParamStr(0));
    except
      // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
    end;
    WriteLog(etInformation, 'TimeHandler started with fileversion ' + xFileVersion);
  end;
end;
//------------------------------------------------------------------------------
function TTimeHandler.getParamHandle:TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;

end.
