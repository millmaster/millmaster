{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_TimeTicker.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Kick on each full minute the TJobExecute
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 80.07.1999  1.01  SDO | TimeChange inserted
|==============================================================================}
unit u_TimeTicker;

interface

uses
  Classes, Windows, SysUtils,
  MMBaseClasses, BaseMain, BaseGlobal;

type
  TTimeTicker = class(TTimeOutTicker)
  private
    mMinOld: DWord;
    mMinNew: DWord;
    mTicker: Word;
    mFirstInit: Boolean;
    mTimeNew, mTimeOld: TDateTime;
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
  public
    constructor Create(aThreadDef: TThreadDef); override;
    destructor Destroy; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,
  Forms, u_TimeJobDB, mmEventLog;

const
  cTimeForwardRestart = 'The time was move forward. MillMaster will restart. Time change from '; // ivlm
  cTimeBackwardRestart = 'The time was move backward. MillMaster will restart. Time change from '; // ivlm
  cTimeForwardEmergency = 'The time was move forward. Time change from  '; // ivlm
  cTimeForwardEmergency1 = ' . MillMaster will call the emergency procedure '; // ivlm
  cTimeBackward = 'The time was move backward. Time change from  '; // ivlm

//------------------------------------------------------------------------------
// TTimeTicker
//------------------------------------------------------------------------------
constructor TTimeTicker.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mFirstInit := True;
  mTicker := 2000; // ms
end;
//------------------------------------------------------------------------------
destructor TTimeTicker.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TTimeTicker.ProcessInitJob;
begin
  // immediately request data after first ProcessInitJob (once)
  if mFirstInit then
  try
    mJob.JobTyp                      := jtTimerEvent;
    mJob.TimerEvnet.TimeStamp        := DateTimeToTimeStamp(SysUtils.now);
    mJob.TimerEvnet.TimeStampOld     := mJob.TimerEvnet.TimeStamp;
    mJob.TimerEvnet.FirstAsynchEvent := True;
    WriteJobBufferTo(ttJobExecute);

    mFirstInit := False;
    mMinOld    := TimeStampToMin(SysUtils.now);
    mMinNew    := 0;
    mTimeOld   := SysUtils.now;
    Exit;
  except
    on e:Exception do begin
      WriteLog(etError, 'TTimeTicker.ProcessInitJob failed: ' + e.Message);
    end;
  end;
  ProcessJob;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//  Read the time every 2. sec. and kick on the TjobHandler every full min.
//******************************************************************************
procedure TTimeTicker.ProcessJob;
var
  xTimeChange: string;
  xTimeDiff: Integer;
  xyear0, xDay0, xMonth0: Word;
  xyear1, xDay1, xMonth1: Word;
begin
  try
    // is looping
    Sleep(mTicker); // 2 sec. sleeping

    mJob.TimerEvnet.TimeChange := False;

    mMinNew := TimeStampToMin(SysUtils.now);
    mTimeNew := SysUtils.now;
    xTimeDiff := integer(mMinNew) - integer(mMinOld);

    // CPU-Timer has changed (time moved forward by hand)
    xTimeChange := Format('[%s to %s]', [DateTimeToStr(mTimeNew - (xTimeDiff / 1440)), DateTimeToStr(mTimeNew)]);
    if (xTimeDiff > 1) and (xTimeDiff <= cPreRestartOffset) then begin
       // All Jobs between Now + 2 Min  and Now + 4 must be execute
      MessageBeep(MB_ICONERROR);
      WriteLog(etWarning, cTimeForwardEmergency + xTimeChange + cTimeForwardEmergency1);

       // Emegrency Work for jobs
      mJob.TimerEvnet.TimeStamp := DateTimeToTimeStamp(mTimeNew);
      mJob.TimerEvnet.TimeStampOLD := DateTimeToTimeStamp(mTimeOld);
      mJob.TimerEvnet.TimeChange := True;

       // Send Msg. to JobExecute for ProcessJob
      WriteJobBufferTo(ttJobExecute);

      mMinOld := mMinNew;
      mTimeNew := mTimeOld;
    end
    else if (xTimeDiff > cPreRestartOffset) then begin // CPU-Timer has changed more than 5 min. forward. Restart MillMaster
      MessageBeep(MB_ICONERROR);
      WriteLog(etWarning, cTimeForwardRestart + xTimeChange);
      mMinOld := mMinNew;
      mTimeNew := mTimeOld;
      WriteToMain(gcRestartMM, 0);
    end;

    // CPU-Timer has changed max. 4 min. backward
    if (xTimeDiff >= -cPreRestartOffset) and (xTimeDiff <= -1) then begin
      MessageBeep(MB_ICONERROR);
      WriteLog(etWarning, cTimeBackward + xTimeChange);
      mMinOld := mMinNew;
      mTimeNew := mTimeOld;
    end
    else if (xTimeDiff < -cPreRestartOffset) then begin // CPU-Timer has changed more than 5 min. backward. Restart MillMaster
      MessageBeep(MB_ICONERROR);
      WriteLog(etWarning, cTimeBackwardRestart + xTimeChange);
      mMinOld := mMinNew;
      mTimeNew := mTimeOld;
      WriteToMain(gcRestartMM, 0);
    end;

    // normal work
    if (xTimeDiff = 1) then begin // Time = 1 min. 2 sec.
      mJob.TimerEvnet.TimeStamp := DateTimeToTimeStamp(mTimeNew);
      // Time for emergency jobs
      mJob.TimerEvnet.TimeStampOLD := DateTimeToTimeStamp(mTimeOld);
      mJob.TimerEvnet.TimeChange := False;

      DecodeDate(mTimeOld, xYear0, xMonth0, xDay0);
      DecodeDate(mTimeNew, xYear1, xMonth1, xDay1);

      mMinOld := mMinNew;
      mTimeOld := mTimeNew;

      // New Year
      if (xYear0 < xYear1) and (xMonth0 = 12) and (xMonth1 = 1) and
        (xDay0 = 31) and (xDay1 = 1) then
        WriteLog(etInformation, 'Happy new year ' + IntToStr(xYear1) + ' ! ');

      // Send Msg. to JobExecute for ProcessJob
      WriteJobBufferTo(ttJobExecute);
    end;
  except
    on e:Exception do begin
      WriteLog(etError, 'TTimeTicker.ProcessJob failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

