{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_JobExecute.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Read the cache and send the single cache record to
|                 the msg-handler.
|                 Request new data from table t_time_joblist.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 28.06.1999  1.01  SDO | ONCE-JobTypes will be deleted after sending (SendProcessJob)
| 03.02.2000  1.02  SDO | If the Execute_Typ <> ONCE or <> NOW then will execute
|                       | the func. ExecutedJob -> Attention: keep a eye of the
|                       | column c_last_execute_time in table t_time_joblist
|==============================================================================}
unit u_JobExecute;

interface

uses
  Classes, SysUtils, BaseGlobal, u_BaseJobs, u_TimeJobDB, mmEventLog;

type
  TJobExecute = class(TBaseJobExecute)
  private
    mStartupCheck: Boolean;
    mProcessInitDone: Boolean;
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
    procedure SendProcessJob(aCacheRec: pCacheRec); override;
  public
    constructor Create(aThreadDef: TThreadDef); reintroduce; //override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,
  u_JobDispatch, Windows;

{ TJobExecute }

//------------------------------------------------------------------------------
constructor TJobExecute.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mStartupCheck := False;
  mProcessInitDone := False;
end;
//------------------------------------------------------------------------------
procedure TJobExecute.ProcessInitJob;
begin
  CodeSite.SendMsg('TJobExecute.ProcessInitJob');
  try
    inherited ProcessInitJob;
    if not mProcessInitDone then begin
      CodeSite.SendMsg('Execute WriteToMain(gcInitialized, NO_ERROR)');
      WriteToMain(gcInitialized, NO_ERROR);
    end;
    mProcessInitDone := True;
  except
    on e:Exception do begin
      WriteLog(etError, '[TJobExecute.ProcessInitJob]:' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Read jobs from Cache and send the jobrec to JobManager
//******************************************************************************
procedure TJobExecute.ProcessJob;
begin
  try
    inherited ProcessJob;
    if not mStartupCheck then begin
      // Check the DataEventJob (possibility failed by StartupInit)
      if not (mJobDispatch as TJobDispatch).CheckAfterStartup then begin
        WriteToMain(gcRestartMM, 0);
      end;
      mStartupCheck := True;
    end;
  except
    on e:Exception do begin
      WriteLog(etError, 'TJobExecute.ProcessJob failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Send a MillMaster-job to JobManager
//******************************************************************************
procedure TJobExecute.SendProcessJob(aCacheRec: pCacheRec);
var
  xMsg: string;
begin
  try
    case aCacheRec.JobTyp of
      //Millmaster jobs
      jtDelInt, jtDelShiftByTime, jtDataAquEv: begin
          // Send to JobMgr
          //todo: JobRec verwendung
          mJob^ := aCacheRec.Data;
          WriteJobBufferTo(ttJobManager);

          // Write TimeStamp into t_Time_JobList,
          // not for ONCE and NOW execute types
          if (aCacheRec.Execute_Typ <> etONCE) and
             (aCacheRec.Execute_Typ <> etNOW) then
            mJobDispatch.mDB.ExecutedJob(aCacheRec.ID, SysUtils.now);

          case aCacheRec.JobTyp of
            jtDelInt: begin
                xmsg := 'Job jtDelInt ; Data: IntID = ' + IntToStr(self.mJob.DelInt.IntID);
                // NOW - JobTypes will be deleted after sending
                if (aCacheRec.Execute_Typ = etNOW) then
                  // TODO: Uiuiui, hier wird auf die Membervariable von Unterobject DIREKT zugegriffen!!
                  // w�re eine Methode in diesem Fall nicht besser?
                  mJobDispatch.mDB.DeleteJob(aCacheRec.ID);
              end;
            jtDelShiftByTime: begin
                xmsg := 'Job jtDelShiftByTime ; Data:  DateTime = ' + DateTimeToStr(mJob.DelShiftByTime.DateTime);

                // ONCE and NOW - JobTypes will be deleted after sending
                if (aCacheRec.Execute_Typ = etONCE) or (aCacheRec.Execute_Typ = etNOW) then
                  // TODO: Uiuiui, hier wird auf die Membervariable von Unterobject DIREKT zugegriffen!!
                  // w�re eine Methode in diesem Fall nicht besser?
                  mJobDispatch.mDB.DeleteJob(aCacheRec.ID);
              end;
            jtDataAquEv: begin
                xmsg := 'Job jtDataAquEv ; Data: ' +
                  'IntID = ' + IntToStr(self.mJob.DataAquEv.IntID) +
                  ' / FragID = ' + IntToStr(self.mJob.DataAquEv.FragID) +
                  ' / IntIDOld = ' + IntToStr(self.mJob.DataAquEv.OldIntID) +
                  ' / FragIDOld = ' + IntToStr(self.mJob.DataAquEv.OldFragID) +
                  '   RegularInt = ' + IntToStr(Integer(self.mJob.DataAquEv.RegularInt)) +
                  ' ShiftEv = ' + IntToStr(Integer(self.mJob.DataAquEv.ShiftEv));
                                                  // NOW - JobTypes will be deleted after sending
                if (aCacheRec.Execute_Typ = etNOW) then
                  // TODO: Uiuiui, hier wird auf die Membervariable von Unterobject DIREKT zugegriffen!!
                  // w�re eine Methode in diesem Fall nicht besser?
                  mJobDispatch.mDB.DeleteJob(aCacheRec.ID);
              end;
          else
          end; // case

          xmsg := 'TimeHandler send: ' + xmsg;

          WriteToEventLogDebug(xmsg);
        end;
    end;
  except
    on e:Exception do
      WriteLog(etError, Format('TJobExecute.SendProcessJob [%d] failed: %s', [Ord(aCacheRec.JobTyp), e.Message]));
  end;
end;
//------------------------------------------------------------------------------

end.

