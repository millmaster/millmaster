{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_DelInt.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:  Delete the oldest intervaldata on DB.
|                  Insert the oldest Interval-ID into the Cache-Record.
|                  Send each hour (xx:45) a DelIntEvent to Jobmanager.
|                  After sending will deleted the actual DelIntEvent and a
|                  new DelIntEvent will be insert in the table t_time_joblist.
|                  Attention:
|                             Sending DelInt.IntID = 0 -> StoreX will be
|                                                         ingnore this job.
| Info..........:  Used tables: t_interval, t_time_JobList
|                  Read from Reg. via mSettings
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 12.04.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function Emergency() inserted
| 17.11.1999  1.02  SDO | Reading from ConstArray (TSettingsReader)
|                       | MMSettings.Value in GetMaxInterval
| 05.10.2002        LOK | Umbau ADO
| 25.11.2005        Lok | Hints und Warnings entfernt
|==============================================================================}
unit u_DelIntAdmin;

interface

uses
  Classes, SysUtils, Windows, BaseMain,
  MMBaseClasses, BaseGlobal, mmEventLog, u_BaseAdmin, u_TimeJobDB,
  SettingsReader;

type

  TDelIntAdmin = class(TBasexAdministrator)
  private
    fError: TErrorRec;
    mStartTimeMinPart: Byte;
    mAddMinutes: Byte;
    mExecuteTyp: TExecuteTypEnum;
    mJobTyp: TJobTyp;
    mPriority: Byte;

    function CheckDelInt(aTimestamp: TTimeStamp): boolean;
    function CountInterval: Integer;
    function GetMaxInterval: Integer;
    function GetOldIntID(aMinAnzInt: Integer): Integer; // if no id is found the result is -1
    function InsertDelInt(aTimestamp: TTimeStamp; aEmergency: Boolean): boolean;
  public
    property Error: TErrorRec read fError;
    function Init: Boolean; override;
    function StartupInit: Boolean; override;
    function Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean; override;
    function PrepareJob(aData: pJobRec): Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  LoepfeGlobal, AdoDbAccess;

const
  cGetIntIDsolderNow =
    'select c_interval_id ID from t_Interval ' +
    'where c_interval_start < getdate () ' +
    'order by c_interval_start desc ';

  cAddMintoTime =
    'select dateadd ( minute, :AddTime, :ActTime ) newTime';

  cCheckDelInt =
    'select * from t_time_jobList ' +
    'where c_Job_type = :c_Job_type and c_execute_type = :c_execute_type ' +
    'and c_Time > :c_Time and convert ( int, c_month_day ) = :c_month_day ' +
    'and convert ( int, c_month ) = :c_month';

  cCountInterval =
    'select count (c_interval_id) Counts from t_Interval ' +
    'where c_interval_start < getdate () ';

//------------------------------------------------------------------------------

//******************************************************************************
// Check the DelInt-Event in the t_time_joblist for the future
//******************************************************************************
function TDelIntAdmin.CheckDelInt(aTimestamp: TTimeStamp): boolean;
var xYear, xMonth, xDay: Word;
begin
  DecodeDate(TimeStampToDateTime(aTimestamp), xYear, xMonth, xDay);
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cCheckDelInt;
    Params.ParamByName('c_execute_type').AsInteger := ord(TExecuteTypEnum(mExecuteTyp));
    Params.ParamByName('c_Job_type').AsInteger := ord(TJobTyp(mJobTyp));
    Params.ParamByName('c_Time').AsInteger := aTimestamp.Time;
    Params.ParamByName('c_Month').AsInteger := xMonth;
    Params.ParamByName('c_Month_Day').AsInteger := xDay;
    Open;
    Result := FindFirst;
  except
    on e:Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDelIntAdmin.CheckDelInt failed: ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Count the interval in table t_intreval
//******************************************************************************
function TDelIntAdmin.CountInterval: Integer;
begin
  Result := -1;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cCountInterval;
    Open;
    if FindFirst then
      Result := FieldByName('Counts').AsInteger;
  except
    on e:Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDelIntAdmin.CountInterval failed: ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare only a job, if the time has change
//******************************************************************************
function TDelIntAdmin.Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean;
begin
  // job in cache !!!
  mDB.ActTime := aTimeStampAct;
  Result := Init;
  if Result then begin
    Result := InsertDelInt(aTimeStampAct, True);
  end;
end;
//------------------------------------------------------------------------------

function TDelIntAdmin.GetMaxInterval: Integer;
begin
  Result := 0;
  try
    Result := MMSettings.Value[cMaxInterval];
  except
    on e:Exception do
      WriteLog(etError, 'TDelIntAdmin.GetMaxInterval failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function TDelIntAdmin.GetOldIntID(aMinAnzInt: Integer): Integer;
begin
  Result := 0;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cGetIntIDsolderNow; //cGetOldestIntID;
    Open;
    if FindFirst then begin
      while (aMinAnzInt > 0) and not EOF do begin
        Next;
        dec(aMinAnzInt);
      end;
      if not EOF then
        Result := FieldByName('ID').AsInteger;
    end;
  except
    on e:Exception do begin
      Result := -1;
      WriteLog(etError, 'TDelIntAdmin.GetOldIntID failed: ' + e.Message);
    end;
  end;

{wss: Das ist ein schlechtes Beispiel f�r eine Datenbankabfrage!!
      Es wird eine fixe Anzahl von Records per Next durchgesteppt, auch wenn
      es gar nicht soviele haben k�nnte. Anschliessend wird versucht, auf den
      letzten m�glichen Eintrag zuzugreifen, OHNE dass auf EOF gepr�ft wird.
      BDE verhielt sich hier toleranter, ADO gef�llt dies gar nicht und l�st eine
      Exception aus!!
  Result := -1;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cGetIntIDsolderNow; //cGetOldestIntID;
    Open;
    if not FindFirst then begin
      Exit;
    end;
    First;
    for xcounter := 0 to aMinAnzInt - 1 do
      Next;

    Result := FieldByName('ID').AsInteger;
  except
    on e:Exception do begin
      WriteLog(etError, 'TDelIntAdmin.GetOldIntID failed: ' + e.Message);
    end;
  end;
{}
end;
//------------------------------------------------------------------------------
function TDelIntAdmin.Init: Boolean;
begin
  if not Initialized then begin
    mStartTimeMinPart := 45; // Minutes; Time for next DelIntEvent => each xx:45
    mAddMinutes       := 60; // Minutes; Next hour
    mExecuteTyp       := etONCE; // ExequteTyp
    mJobTyp           := jtDelInt; // JobTyp
    mPriority         := 1; // Priority
  end;
  Result := inherited Init;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a DelInt-Event into the table t_time_joblist without the Interval-ID
//******************************************************************************
function TDelIntAdmin.InsertDelInt(aTimestamp: TTimeStamp; aEmergency: Boolean): boolean;
var
  xHour, xMin, xSec, xMSec,
  xYear, xMonth, xDay: Word;
  xJobRec: TJobRec;
  xNewJob: TJobListRec;
  xTempTime: TDateTime;
begin
  Result := True;
  try
    DecodeDate(TimeStampToDateTime(aTimestamp), xYear, xMonth, xDay);
    DecodeTime(TimeStampToDateTime(aTimestamp), xHour, xMin, xSec, xMSec);
    // Event-Time = x:45:00 00  +  Date
    if aEmergency then begin
      // Emergency
      // zuerst wird eine sauberes Datum aus dem aTimeStamp erstellt, wo garantiert keine Sekunden
      // und Hunderstelsekunden drin sind...
      xTempTime := EncodeTime(xHour, xMin, 0, 0) + EncodeDate(xYear, xMonth, xDay);
      WriteLog(etError, 'TDelIntAdmin.InsertDelInt Emergency at : ' + DateTimeToStr(xTempTime));
      mAddMinutes := 3; // beachte neustart-zeit
    end
    else
      xTempTime := EncodeTime(xHour, mStartTimeMinPart, 0, 0) + EncodeDate(xYear, xMonth, xDay);

    Result := False;
    with mDB.Query[cPrimaryQuery] do
    try
      // Add 60 min. to xTempTime
      Close;
      SQL.Text := cAddMintoTime;
      (*  Im Zusammenhang mit Datunsfunktionen werden die Datentypen
          der Parameter explizit angegeben um eine Fehlinterpretation des Parametertyps
          unter ADO zu vermeiden *)
      Params.ParamByName('AddTime').AsInteger := mAddMinutes;
      Params.ParamByName('AddTime').AdoType := adInteger;
      Params.ParamByName('ActTime').AsDateTime := xTempTime;
      Params.ParamByName('ActTime').AdoType := adDate;
      Open;

        // Next Event-Time
      if xMin >= mStartTimeMinPart then
        xTempTime := FieldByName('newTime').AsDateTime;
           //xTempTime:= xTempTime + (1/24); // + 1h
      Close;
      if xTempTime = 0 then begin
        WriteLog(etError, 'TDelIntAdmin.InsertDelInt failed: No Interval-ID available');
        Exit;
      end;

      // new time
      DecodeDate(xTempTime, xYear, xMonth, xDay);
      DecodeTime(xTempTime, xHour, xMin, xSec, xMSec);

        // Insert a new DelIntEvent
        // - Record Initialize
      FillChar(xJobRec, sizeof(xJobRec), 0);
      xJobRec.JobTyp := jtDelInt;

        // Fillup the 'xNewJob'-Record
      fillchar(xNewJob, sizeof(xNewJob), 0);
      with xNewJob do begin
        Time        := xTempTime;
        WeekDay     := '';
        MonthDay    := inttostr(xDay);
        Month       := inttostr(xMonth);
        Priority    := mPriority; // Const.
        Execute_Typ := mExecuteTyp;
        JobTyp      := mJobTyp;
        Data        := xJobRec;
      end;

      // Delete jtDelInt from table t_Time_JobList before insert
      mDB.JobMutation('DELETE FROM t_Time_JobList WHERE c_Job_type = ' + IntToStr(ord(jtDelInt)));
      // - Record into DB (t_Time_JobList)
      // TODO: InsertJob w�rde Boolean zur�ckgeben. Warum wird das nicht ausgewertet?
      mDB.InsertJob(xNewJob);
      Result := True;
    except
      on e:Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDelIntAdmin.InsertDelInt failed: ' + e.Message);
      end;
    end;
  except
    on e:Exception do
      WriteLog(etError, Format('TDelIntAdmin.InsertDelInt( %s ) failed: %s', [DateTimeToStr(TimeStampToDateTime(aTimestamp)), e.Message]));
  end;
end;
//------------------------------------------------------------------------------
function TDelIntAdmin.PrepareJob(aData: pJobRec): Boolean;
var
  xCountInterval, xMaxIntervalFromReg, xIntID: Integer;
  xText: string;
begin
  // TODO: Error protikollierung �berarbeiten
  WriteToEventLogDebug('TDelIntAdmin.PrepareJob. begin');

  case Initialized of
    True: xText := 'Initialized = TRUE;   ';
    False: xText := 'Initialized = FALSE;  ';
  end;

  xCountInterval := CountInterval; // counts from table t_interval

  // Insert the next DelInt-Event into the table t_time_joblist
  if not InsertDelInt(EventTime, False) then
    WriteLog(etError, FormatMMErrorText(fError));

  WriteToEventLogDebug('TDelIntAdmin.PrepareJob.InsertDelInt done');

  xMaxIntervalFromReg := GetMaxInterval; // max. number from registry
  if xMaxIntervalFromReg < 0 then begin
    aData.DelInt.IntID := 0; // this job will be ignore by StoreX
    WriteLog(etError, 'No Intervals available in table t_interval [TDelIntAdmin.PrepareJob]' + xText);
    Result := True;
    Exit;
  end;

  WriteToEventLogDebug('TDelIntAdmin.PrepareJob.GetMaxInterval done');

  // delete all intervals older than the last xMaxIntervalFromReg
  if xCountInterval >= xMaxIntervalFromReg + 1 then
    // set the oldest Interval-ID in Cache-Record
    xIntID := GetOldIntID(xMaxIntervalFromReg + 1)
  else
    xIntID := 0; // this job will be ignore by StoreX

  // Error, no Int-ID found
  if xIntID < 0 then begin
    xIntID := 0; // this job will be ignore by StoreX
    WriteLog(etWarning, 'No old Interval-ID found in TDelIntAdmin.PrepareJob ' + xText);
  end;

  aData.JobTyp := jtDelInt;
  aData.DelInt.IntID := xIntID; // Set DelInt-ID

  Result := True;
  WriteToEventLogDebug('TDelIntAdmin.PrepareJob end');
end;
//------------------------------------------------------------------------------
function TDelIntAdmin.StartupInit: Boolean;
begin
  Result := False;
  try
    // Check the t_time_joblist for DelInt-Event
    if not CheckDelInt(ActTime) then
      if not InsertDelInt(ActTime, False) then begin
        // Error InsertDelInt
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
    Result := True;
  except // Error CheckDelInt
    on e:Exception do
      WriteLog(etError, FormatMMErrorText(fError) + e.Message);
  end;
end;
//------------------------------------------------------------------------------

end.

