{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_CleanUpEvAdmin.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Insert two CleanUp-Evnts, if no CleanUp-Evnts exists in DB
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function Emergency() inserted
| 17.11.1999  1.02  SDO | Reading from ConstArray (TSettingsReader)
|                       | MMSettings.Value in GetTimePeriod
| 06.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
|==============================================================================}
unit u_CleanUpEvAdmin;

interface

uses
  Classes, SysUtils, Windows,
  MMBaseClasses, BaseGlobal, BaseMain, mmList,
  u_TimeJobDB, mmEventLog, u_BaseAdmin, SettingsReader;

type
  TCleanUpEvAdmin = class(TBasexAdministrator)
  private
    mTimeCleanEV1: Integer;
    mTimeCleanEV2: Integer;
    mExecuteTyp: TExecuteTypEnum;
    mJobTyp: TJobTyp;
    mPriority: Byte;
    function InsertCleanUp(aTime: Integer; aExecuteTyp: TExecuteTypEnum; aJobTyp: TJobTyp; aPriority: Byte): Boolean;
    function GetTimePeriod: DWord;
  public
    function Init: Boolean; override;
    function StartupInit: Boolean; override;
    function Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean; override;
    function PrepareJob(aJob: PJobRec): Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  LoepfeGlobal, AdoDBAccess;

//------------------------------------------------------------------------------
// TCleanUpEvAdmin
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare only a job, if the time has change
//******************************************************************************
function TCleanUpEvAdmin.Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean;
begin
  // In Basisklasse ist diese Methode als abstrakt definiert, daher Notwendig
  Result := False;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Read a timeperiod from registry for the CleanUp event
//******************************************************************************
function TCleanUpEvAdmin.GetTimePeriod: DWord;
begin
  Result := 0;
  try
    Result := MMSettings.Value[cCUpTimePeriod];
  except
    on E: Exception do
      WriteLog(etError, 'Error in TCleanUpEvAdmin.GetTimePeriod: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function TCleanUpEvAdmin.Init: Boolean;
var
//  xHour, xMin: Word;
  xTimeMs: TTimeStamp;
begin
  mExecuteTyp := etDAILY;
  mJobTyp     := jtCleanUpEv;
  mPriority   := 0;

  // 1. Start-Time  CleanUp Event
  xTimeMs       := DateTimeToTimeStamp(EncodeTime(5, 30, 0, 0));
  mTimeCleanEV1 := xTimeMs.Time;

  // 2. Start-Time CleanUp Event
  xTimeMs       := DateTimeToTimeStamp(EncodeTime(23, 0, 0, 0));
  mTimeCleanEV2 := xTimeMs.Time;

  Result := inherited Init;

{
  Result := inherited init;

  mExecuteTyp := etDAILY; // ExequteTyp
  mJobTyp := jtCleanUpEv; // JobTyp
  mPriority := 0; // Priority

  // 1. Start-Time  CleanUp Event
  xHour := 5;
  xMin := 30;
  xTimeMs := DateTimeToTimeStamp(EncodeTime(xHour, xMin, 0, 0));
  mTimeCleanEV1 := xTimeMs.Time;

  // 2. Start-Time CleanUp Event
  xHour := 23;
  xMin := 0;
  xTimeMs := DateTimeToTimeStamp(EncodeTime(xHour, xMin, 0, 0));
  mTimeCleanEV2 := xTimeMs.Time;

  if Result then
    mInitialized := True
  else
    mAdminEventLog.Write(etWarning, ' Init not initialized in ' + self.ClassName + '.');

  Result := True;
{}
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a CleanUpEvent in database (insert direct into db)
//******************************************************************************
function TCleanUpEvAdmin.InsertCleanUp(aTime: Integer; aExecuteTyp: TExecuteTypEnum; aJobTyp: TJobTyp; aPriority: Byte): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := False;

  FillChar(xJobRec, sizeof(xJobRec), 0);
  xJobRec.JobTyp               := jtCleanUpEv;
  xJobRec.CleanUpEv.TimePeriod := GetTimePeriod;

  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('INSERT INTO t_time_joblist ');
    SQL.Add(' (c_time_joblist_id, c_execute_type, c_time, c_week_day, ');
    SQL.Add('  c_month_day, c_month, c_priority, c_Job_type, c_data) ');
    SQL.Add('VALUES(:c_time_joblist_id, :c_execute_type, :c_time, :c_week_day, ');
    SQL.Add('       :c_month_day, :c_month, :c_priority, :c_Job_type,:c_data ) ');

    Params.ParamByName('c_execute_type').AsInteger := ord(aExecuteTyp);
    Params.ParamByName('c_time').AsInteger := aTime;
    Params.ParamByName('c_priority').AsInteger := aPriority;
    Params.ParamByName('c_Job_type').AsInteger := ord(aJobTyp);
    Params.ParamByName('c_data').SetBlobData(@xJobRec, sizeof(xJobRec));

    Params.ParamByName('c_week_day').asString := '';
    Params.ParamByName('c_month_day').asString := '';
    Params.ParamByName('c_month').asString := '';

    InsertSQL('t_time_joblist', 'c_time_joblist_id', MaxInt, 1);
    Result := True;
  except
    on e: Exception do
      WriteLog(etError, 'TCleanUpEvAdmin.InsertCleanUp failed: ' + e.Message);
  end;
{
  Result := True;

  fillchar(xJobRec, sizeof(xJobRec), 0);
  xJobRec.JobTyp := jtCleanUpEv;
  xJobRec.CleanUpEv.TimePeriod := GetTimePeriod;

  with mDB.Query[cPrimaryQuery] do begin
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('INSERT INTO t_time_joblist ');
    SQL.Add(' (c_time_joblist_id, c_execute_type, c_time, c_week_day, ');
    SQL.Add('  c_month_day, c_month, c_priority, c_Job_type, c_data) ');
    SQL.Add('VALUES(:c_time_joblist_id, :c_execute_type, :c_time, :c_week_day, ');
    SQL.Add('       :c_month_day, :c_month, :c_priority, :c_Job_type,:c_data ) ');
    try
      Params.ParamByName('c_execute_type').AsInteger := ord(aExecuteTyp);
      Params.ParamByName('c_time').AsInteger := aTime;
      Params.ParamByName('c_priority').AsInteger := aPriority;
      Params.ParamByName('c_Job_type').AsInteger := ord(aJobTyp);
      Params.ParamByName('c_data').SetBlobData(@xJobRec, sizeof(xJobRec));

      Params.ParamByName('c_week_day').asString := '';
      Params.ParamByName('c_month_day').asString := '';
      Params.ParamByName('c_month').asString := '';

      InsertSQL('t_time_joblist', 'c_time_joblist_id', MaxInt, 1);

    except
      on e: Exception do begin
        mEventLog.Write(etError,
          'InsertCleanUp error in ' + self.ClassName +
          '. ' + e.Message +
          ' ID = ' + IntToStr(e.HelpContext));
        Result := False;
      end;
    end;
  end;
{}
end;
//------------------------------------------------------------------------------
function TCleanUpEvAdmin.PrepareJob(aJob: PJobRec): Boolean;
begin
  Result := True;

  aJob.JobTyp := jtCleanUpEv;
  if aJob.CleanUpEv.TimePeriod <= 0 then
    aJob.CleanUpEv.TimePeriod := GetTimePeriod; // TimePeriod from reg.
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Check CleanUp Events in database
//******************************************************************************
function TCleanUpEvAdmin.StartupInit: Boolean;
//var
//  xMSG: string;
begin
  Result := False;
  // Exists 'CleanUp EV' in table 't_time_joblist' ?
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
//    SQL.Add('SELECT c_Job_type FROM t_time_joblist ');
//    SQL.Add('WHERE c_Job_type = :c_Job_type ');
    SQL.Text := 'SELECT c_job_type FROM t_time_joblist WHERE c_job_type = :c_job_type ';
    Params.ParamByName('c_job_type').AsInteger := ord(mJobTyp);
    Open;
    // Insert CleanUpEv in table 't_time_joblist if not exists'
    if not FindFirst then begin // ADO Conform
      InsertCleanUp(mTimeCleanEV1, mExecuteTyp, mJobTyp, mPriority);
      InsertCleanUp(mTimeCleanEV2, mExecuteTyp, mJobTyp, mPriority);
    end;
    Result := True;
  except
    on e: Exception do
      WriteLog(etError, 'TCleanUpEvAdmin.StartupInit failed: ' + e.Message);
  end;
{
  Result := True;

 // Exists 'CleanUp EV' in table 't_time_joblist' ?
  with mDB.Query[cPrimaryQuery] do begin
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('SELECT c_Job_type FROM t_time_joblist ');
    SQL.Add('WHERE c_Job_type = :c_Job_type ');
    Params.ParamByName('c_Job_type').AsInteger := ord(mJobTyp);
    try
      Open;
    except
      on e: Exception do begin
        mEventLog.Write(etError, 'StartupInit error in ' +
          self.ClassName + '. Could not open database. ');
        Result := False;
        Exit;
      end;
    end;
    // Insert CleanUpEv in table 't_time_joblist if not exists'
    if EOF then begin // ADO Conform
      InsertCleanUp(mTimeCleanEV1, mExecuteTyp, mJobTyp, mPriority);
      InsertCleanUp(mTimeCleanEV2, mExecuteTyp, mJobTyp, mPriority);
    end;
  end;
{}
end;
//------------------------------------------------------------------------------

end.

