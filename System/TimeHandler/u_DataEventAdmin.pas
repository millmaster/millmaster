{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_DataEventAdmin.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: - Used tables: t_shift, t_fragshift, t_shift_fragshift,
|                                t_time_joblist, t_interval, t_machien_shiftcal
|                 - Read from Reg. via mSettings
|
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 23.03.1999  1.00  MG, SDO | File created
| 17.06.1999  1.01      Mg  | TruncateNumOfIntervals inserted
| 07.07.1999  1.02      SDO | Additional at func. CheckInterval
|                           | SysUtils.Now + (1/86400*30) inserted, because
|                           | the SQL-Server round the time.
|                           | -> 10:10:31 round to 10:11:00 (note the Interval)
| 09.07.1999  1.03      SDO | function GetLastFragID, function GetLastIntID and
|                           | function Emergency inserted
| 12.08.1999  1.03      SDO | function GetLastFragID, function GetLastIntID deleted
|                           | function GetLastEventTime, GetLastFragAndIntID inserted
| 08.09.1999  1.04      SDO | NextIntervalStart and NextShiftStart extend with aStartUp-Flag and
|                           | new SQL-Statemet cNextStartUpInterval, cNextStartUpShift which
|                           | is needed for the Startup
| 14.09.1999  1.05      SDO | New SQL cDeleteOldInterval for Interval there
|                           | never will use in the past. func CheckInterval
| 08.11.1999  1.06      SDO | All shifts which has no times, will be print in
|                           | the logfile. (proc. CheckShifts)
| 17.11.1999  1.06      SDO | Execute OLE-Automation for the shiftcalendar, if
|                           | there is not declareted. (proc. CheckShifts)
|                           | Comes in the future. Now, it is realisized with
|                           | the class TAppendDefaultShifts
| 17.11.1999  1.07      SDO | Reading from ConstArray (TSettingsReader)
|                           | MMSettings.Value in GetIntLen and GetMaxInt
| 08.02.2000  1.08      SDO | xFullHour:=StrToDateTime(DateTostr(SysUtils.Now) +
|                           | ' ' + TimeToStr(xFullHour)); replaced by
|                           | xFullHour:=Date + xFullHour;
| 26.03.2001  1.09      Nue | Additionals in the contents of additional parameters in DataAquEv-Job
| 17.04.2001  1.10      SDo | Es ist schon 2-3 mal vorgekommen, dass bei einem
                              Systemstillstand von ca. 4 Tagen der TimeHandler nicht mehr starten konnte, weil
                              die DB-Tabellen t_Shift, shift_fragshift und t_fragshift 'gelockt' waren. ??!!
| 23.04.2001  1.10      SDo | in Proc. InsertShifts geht's schief nur in StartupInit !!!!
| 22.01.2002  1.11      SDo | Aenderung in GetFragID
                            | Query in cGetFragID geaendert -> Sortieren nach juengsten Datum und c_fragshift_length =0
|                           | (Kunde hatte mehere Eintraege in DB mit c_fragshift_length = 0 -> Fatal )
| 27.02.2002  1.12      SDo | In Try execpt Error on e:Exception eingefuegt,
|                           | Errormeldung: falls Stored procedure xp_DeleteMMData fehlt oder MMWindingLink.dll
|                           | (Die Stored procedure xp_DeleteMMData benoetigt MMWindingLink.dll)
| 27.03.2002  1.12      SDo | e:Exception Msg. in Funk. InsertFirstFragshift () erweitert
| 02.05.2002  1.13      SDo | WriteToEventLogDebug Meldungen eingebaut.
| 13.06.2002  1.13      Wss | - InitTimerange: Abfrage fuer t_fragshift genaendert -> count(*)
                              - Weitere CodeSite Meldungen implementiert
| 13.10.2002            LOK | Umbau ADO
| 11.10.2002            LOK | EOF ADO Conform
| 21.11.2002            SDO | Func. InsertFrags() & InsertShiftFrags() : Temporaere ADO Instanz erstellt,
|                           | weil Access violation, wenn viele Eintraege.
|                           | (Gueltig nur in diesen Func.)
| 19.12.2002  1.14   Wss/Nue| CheckShifts mit Startup Parameter erweitert, um zu unterscheiden,
                              ob beim Aufstarten die Schichtkalender mit EventTime oder GetDate()
                              gepr�ft werden m�ssen, da beim Aufstarten EventTime noch nicht
                              verf�gbar ist.
| 14.03.2003  1.14      Wss | Bug beim ersten Start, wenn Zeit < 06:00 ist behoben
| 17.06.2003  1.14      Wss | Timeout beim aufstarten, wenn MM gestanden ist. TimeHandler
                              versuchte alte Intervale zu l�schen, welche per Trigger
                              die entspr. Daten mitl�schen sollte. Dies konnte jedoch l�nger
                              dauern als das Timeout vom MMGuard (1 min), wodurch der MMGuard
                              ein Restart ausl�ste und es wieder von vorne los ging. Nun wird
                              in CheckInterval gel�scht a) wenn MM l�nger als MaxIntervale gestanden ist
                              (per Truncate) b) beim auff�llen von Intervallen, wenn die Anzahl 200
                              �berschreiten w�rden (TruncateNumOfIntervals).
                              Das eigentliche L�schen wird nun vollst�ndig im StorageHandler per
                              DelInt Job ausgef�hrt.
| 25.11.2005            Lok | Hints und Warnings entfernt
|==============================================================================}
unit u_DataEventAdmin;            

interface

uses
  Classes, SysUtils, Windows, BaseMain, AdoDBAccess,
  MMBaseClasses, BaseGlobal, mmEventLog, u_BaseAdmin,
  MMUGlobal, SettingsReader, u_AppendDefaultShifts, u_TimeJobDB;

type
  TDataEventAdmin = class(TBasexAdministrator)
  private
    mQuery: TNativeAdoQuery;
    mQuery2: TNativeAdoQuery;
    mLastFragshiftStart: TDateTime;
    mError: TErrorRec;
    mExecuteTyp: TExecuteTypEnum;
    mJobTyp: TJobTyp;
    mPriority: Byte;
    mDefaultDebugLogTxt: string;
    function GetIntLen: Integer; // Minutes
    function GetMaxInt: Integer;
    function InitTimerange: Boolean;
    function GetShiftsInTimerange: Boolean;
    function GetActFragStart(var aFragStart: TDateTime; aEventTime: TDateTime): Boolean;
    function GetFragID: Integer; // if no id is found the result is -1
    function GetIntID(aEventTime: TDateTime): Integer; // if no id is found the result is -1
    function GetLastEventTime(var aEventTime: TDateTime): Boolean;
    function GetLastFragAndIntID(aEventTime: TDateTime; var aFragID: Integer; var aIntID: Integer): Boolean; // if no id is found the result is -1
    function InsertFirstFragshift(var aLastFragshiftStart: TDateTime): Boolean;
    function InsertInterval(var aInsertedInterval: TDateTime): Boolean;
    function InsertFrags(aActShift, aLastShift: TDateTime; var aFragShiftID: integer): Boolean;
    function InsertShiftFrags(aActShift: TDateTime; aFragShiftID: integer): Boolean;
    function InsertShifts(var aNumShifts: integer): Boolean;
    function InsertNextDataEvent(aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
    function InsertShiftCal(aCalendarID: LongInt; aShiftName: string; aShiftCal: TAppendDefaultShifts): Boolean;
    function NextIntervalStart(var aIntervalStart: TDateTime; aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
    //Nue: 26.03.01 var aShiftcalID: Byte; var aShiftInDay: Byte; added
    function NextShiftStart(var aIntervalStart: TDateTime; var aShiftcalID: Byte; var aShiftInDay: Byte; aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
    function CheckFragshifts: Boolean;
    function CheckPastFragshifts: Boolean;
    function CheckShifts(aStartup: Boolean): Boolean;
    function TruncateNumOfIntervals: Boolean; // checks the number of records and delets the oldest if count > 255
    function CheckInterval: Boolean;
    function CheckDataEvent(aTimestamp: TTimeStamp): Boolean;
    procedure WriteToEventLogDebug(aMsg: string; aDefaultText: string = ''; aEvent: TEventType = etInformation);
  public
    function PrepareJob(aJob: pJobRec): Boolean; override;
    constructor Create(aDB: TTimeJobDB; aEventLog: TEventLogWriter; aSettings: TMMSettingsReader); override;
    destructor Destroy; override;
    function Init: Boolean; override;
    function StartupInit: Boolean; override;
    function CheckAfterStartup: Boolean;
    function Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  LoepfeGlobal, ActiveX;

const
  cInitTimerange =
    'select max ( c_fragshift_start ) c_fragshift_start from t_fragshift';
  cGetShiftsInTimerange =
    'select distinct c_shift_start from t_shift ' +
    'where c_shift_start > :LastFragshiftStart ' +
    'and c_shift_start <= :ActTime';
  cGetLastShift = // only used by initialstart after installation
    'select min ( c_shift_start ) c_shift_start from t_shift ' +
    'where c_shift_start <= getdate ()';
  cUpdateFrag =
    'update t_fragshift set c_fragshift_length = :c_fragshift_length ' +
    'where c_fragshift_start=:c_fragshift_start';
  cInsertFrag =
    'insert t_fragshift ( c_fragshift_id, c_fragshift_start, c_fragshift_length ) ' +
    'values ( ' +
    ':c_fragshift_id, :c_fragshift_start, 0 )';
  cGetShiftsAtTime =
    'select c_shift_id, c_shiftcal_id from t_shift ' +
    'where c_shift_start <= :ActShift and ' +
    'dateadd ( minute, c_shift_length, c_shift_start ) > :ActShift';
  cInsertShiftFragshift =
    'insert t_shift_fragshift ( c_fragshift_id, c_shift_id, c_shiftcal_id ) ' +
    'values ( ' +
    ':c_fragshift_id, :c_shift_id, :c_shiftcal_id )';
  cCheckShiftAvailability =
    'select distinct c_shiftcal_id, c_shiftcal_name from t_shiftcal ' +
    'where c_shiftcal_id not in ( select distinct c_shiftcal_id ' +
    'from t_shift where c_shift_start > :EventTime)';
  cCheckIntervalAvailability =
    'select max ( c_interval_start ) last_interval from t_interval';
  cGetLastShiftTime =
    'select distinct max ( c_shift_start ) c_shift_start from t_shift where ' +
    'c_shift_start < getdate ()';
  cGetLastInterval =
    'select distinct c_interval_id, c_interval_start from t_interval ' +
    'where c_interval_start in ( select max ( c_interval_start ) from t_interval )';
  cAddIntLenToIntStart =
    'select dateadd ( minute, :IntLength, :c_interval_start ) newInterval';
  cInsertNewInterval =
    'insert t_Interval ( c_interval_id, c_Interval_Start, c_Interval_End ) ' +
    'values ( :c_interval_id, :c_Interval_Start, :c_Interval_End )';
  cUpdateInterval =
    'update t_Interval set c_Interval_End = :c_Interval_End ' +
    'where c_interval_id = :c_interval_id ';

  cNextInterval =
    'select min ( c_interval_Start ) new_interval_Start from t_interval ' +
    'where c_interval_Start > :start';

  cNextStartUpInterval =
    'select max ( c_interval_Start ) new_interval_Start from t_interval ';

  cNextShift =
    'select distinct min ( c_shift_Start ) new_shift_Start from t_shift ' +
    'where c_shift_Start > :start';

  cNextStartUpShift =
    'select distinct min ( c_shift_Start ) new_shift_Start from t_shift ' +
    'where c_shift_Start > (Select max (c_fragshift_start) from t_fragshift) ';

//Next query new by Nue 26.03.01
  cActShiftInDay =
    'select c_shiftcal_id, c_shift_in_day from t_shift ' +
    'where dateadd(minute,c_shift_length,c_shift_start) =(select distinct min ( dateadd(minute,c_shift_length,c_shift_start) ) from t_shift ' +
    'where dateadd(minute,c_shift_length,c_shift_start) > :start) ';

  cCheckTimeJobList = // only Execute_typ = 'Once'
    'select * from t_time_JobList ' +
    'where c_Job_Type = :c_Job_Type and c_execute_type = :c_execute_type ' +
    'and ( ( c_Time > :c_Time and convert ( int, c_month_day ) = :c_month_day ' +
    '        and convert ( int, c_month ) = :c_month ) ' +
    '       or ( convert ( int, c_month_day ) > :c_month_day ' +
    '           and convert ( int, c_month ) >= :c_month ) )';

  cCheckFragshift =
    'select c_fragshift_id from t_fragshift ' +
    'where c_fragshift_start in ( ' +
    'select max ( c_shift_start ) from t_shift ' +
    'where c_shift_start < getdate () )';

  cGetFragShiftAtTime =
    'select max (c_fragshift_start) c_fragshift_start from t_fragshift ' +
    'where c_fragshift_start <= :EventTime';

  cGetIntID =
    'select c_interval_id ID from t_Interval ' +
    'where c_interval_start < :EventTime ' +
    'and c_interval_end >= :EventTime';

  cGetFragID =
    'select * from t_fragshift where c_fragshift_length = 0 ' +
    'order by  c_fragshift_id desc';

  cGetNumIntervalRecords =
    'select count ( c_interval_id ) NumIntervals from t_interval';

  cDelOldestInterval =
    'delete from t_interval where c_interval_start = ' +
    '(select min ( c_interval_start ) from t_interval)';

  cGetOldFragIDandIntID =
    'select F.c_fragshift_id FId, I.c_Interval_id IId ' +
    'from t_fragshift F , t_interval I ' +
    'where F.c_Fragshift_Start = ' +
    '   ( ' +
    '     select distinct max(F.c_Fragshift_Start) ' +
    '     from t_fragshift F ' +
    '     where F.c_Fragshift_Start < :Time ' +
    '   ) ' +
    ' and I.c_Interval_Start = ' +
    '   ( select distinct max(c_Interval_Start) ' +
    '     from t_interval ' +
    '     where c_Interval_Start < :Time ' +
    '   ) ';

  cGetDataEventStartTime =
    'select max(c_dataevent_start) StartTime ' +
    'from v_DataEvents ' +
    'where c_dataevent_start < :EventTime ';

  cDeleteOldInterval =
    ' delete t_interval where ' +
    ' c_interval_end < ( select dateadd ( minute, :Min, :EventTime ) ) ';

//------------------------------------------------------------------------------
constructor TDataEventAdmin.Create(aDB: TTimeJobDB; aEventLog: TEventLogWriter; aSettings: TMMSettingsReader);
begin
  inherited Create(aDB, aEventLog, aSettings);
  mQuery  := TNativeAdoQuery.Create;
  mQuery2 := TNativeAdoQuery.Create;
  mDefaultDebugLogTxt := ClassName + ':  ';
  WriteToEventLogDebug('DataEventAdmin created', mDefaultDebugLogTxt, etSuccess);
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.GetLastFragAndIntID(aEventTime: TDateTime; var aFragID: Integer; var aIntID: Integer): Boolean;
begin
  Result  := True;
  aFragID := 0;
  aIntID  := 0;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetOldFragIDandIntID;
    Params.ParamByName('Time').AsDateTime := aEventTime;
    Open;
    if FindFirst then begin
      aFragID := FieldByName('FId').AsInteger;
      aIntID  := FieldByName('IId').AsInteger;
    end else begin
      WriteLog(etWarning, 'No old FragID and no old IntID available. Set both id�s to 0. Only acceptable by first startup after installation.');
    end;
  except
    on e:Exception do begin
      Result := False;
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.GetLastFragAndIntID failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
destructor TDataEventAdmin.Destroy;
begin
  mQuery.Free;
  mQuery2.Free;
  try
    if mDB.DataBase.InTransaction then
      mDB.DataBase.Commit;
  except
    on e:Exception do
      WriteLog(etError, 'In CommitTransaction. [TDataEventAdmin.Destroy]: ' + e.Message);
  end;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.Init: Boolean;
begin
  mExecuteTyp := etONCE;
  mJobTyp     := jtDataAquEv;
  mPriority   := 0;
  try
    // Transaction handling �berpr�fen
    if mDB.DataBase.InTransaction then
      WriteLog(etError, 'mDB is in transaction [TDataEventAdmin.Init]');

    mQuery2.Assign(mDB.Query[cSecondaryQuery]);
    mQuery.Assign(mDB.Query[cSecondaryQuery]);
    inherited Init;
  except
    on e:Exception do
      WriteLog(etError, 'Assign of Query failed: ' + e.Message);
  end;
  Result := Initialized;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.StartupInit: Boolean;
begin
  // Check the availability of the shifts and fill automatic up the shifts
  WriteToEventLogDebug('TDataEventAdmin.StartupInit begin: ', mDefaultDebugLogTxt);

  // Transaction ist in CheckShifts OK
  Result := CheckShifts(True);
  if not Result then begin
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('StartupInit : CheckShifts done  ', mDefaultDebugLogTxt);
  // Hier Error
  // Check the fragment shifts and fill them up if not available
  Result := CheckFragshifts;
  if not Result then begin
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('StartupInit : CheckFragshifts done  ', mDefaultDebugLogTxt);

  // Check the intervaltabel if all intervals are availabel
  Result := CheckInterval;
  if not Result then begin
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('StartupInit : CheckInterval done  ', mDefaultDebugLogTxt);

  // Check the next data event in TimeJobList
  try
    Result := InsertNextDataEvent(TimeStampToDateTime(ActTime), True);
  except
    Result := False;
  end;

  if not Result then
    WriteLog(etError, FormatMMErrorText(mError));

  WriteToEventLogDebug('TDataEventAdmin.StartupInit end: ', mDefaultDebugLogTxt);
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.CheckFragshifts: Boolean;
var
  xNumShifts: integer;
begin
  Result := True;
  if not InitTimerange then begin
    Result := False;
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('TDataEventAdmin.StartupInit[CheckFragshifts] : InitTimerange done  ', mDefaultDebugLogTxt);

  // Hier wird das secondary Query initialisiert...
  if not GetShiftsInTimerange then begin // the shifts will be stored in buffer of the secondary query
    Result := False;
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('TDataEventAdmin.StartupInit[CheckFragshifts] : GetShiftsInTimerange done  ', mDefaultDebugLogTxt);

  //...und hier wird das Resultset verwendet.
  if mDB.Query[cSecondaryQuery].FindFirst then begin
    // In dieser Methode wird StartTransaction aufgerufen
    // -> in InsertShifts OK
    if not InsertShifts(xNumShifts) then begin
      Result := False;
      WriteLog(etError, FormatMMErrorText(mError));
      Exit;
    end;
    WriteToEventLogDebug('Tables t_fragshift and t_shift_fragshift updated: ' + IntToStr(xNumShifts) + ' insert(s) done.', mDefaultDebugLogTxt);
  end;
  WriteToEventLogDebug('TDataEventAdmin.StartupInit[CheckFragshifts]: InsertShifts done', mDefaultDebugLogTxt);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Check Fragshift in the past
//******************************************************************************
function TDataEventAdmin.CheckPastFragshifts: Boolean;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cCheckFragshift;
    Open;
    Result := FindFirst;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.CheckPastFragshifts failed: ' + e.Message);
      CodeSite.SendError(mError.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.CheckShifts(aStartup: Boolean): Boolean;
var
  xCalId: Integer;
  xShiftNames: string;
  xDefShift: TAppendDefaultShifts;
begin
  // Hier wird gepr�ft, ob es f�r ein Schichtkalender keine g�ltigen Schichteintr�ge
  // in der Tabelle t_shift hat. Wenn noch alles OK ist (leere Resultmenge),
  // dann wird nichts gemacht, wenn aus dem Query ein Schichtkalender auftaucht,
  // dann fehlen f�r diesen Schichtkalender Eintr�ge in der t_shift Tabelle und
  // werden demzufolge noch hinzugef�gt. Per Registry (HKLM\...\MillMaster\Debug)
  // kann definiert werden (�bersteuerung per ShiftAddDays: String) wieviele Schichten in die Zukunft
  // hinzugef�gt werden sollen (default = 8)
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cCheckShiftAvailability;
    ParamByName('EventTime').ADOType := adDate;
    if aStartup then
      ParamByName('EventTime').AsDateTime := Now
    else
      ParamByName('EventTime').AsDateTime := TimeStampToDateTime(EventTime);
    WriteToEventLogDebug(Format('TDataEventAdmin.CheckShifts: SQL.Text := %s, EventTime = %s', [SQL.Text, DateTimeToStr(ParamByName('EventTime').AsDateTime)]), mDefaultDebugLogTxt);

    Open;
    if FindFirst then begin
      // FindFirst ist immer auf FALSE
      xDefShift := TAppendDefaultShifts.Create(nil);
      with xDefShift do
      try
        //DecodeDate(Date, xYear, xMonth, xDay);
        Database := mDB;
        while not EOF do begin // ADO Conform
          xCalId := FieldByName('c_shiftcal_id').AsInteger;
          xShiftNames := FieldByName('c_shiftcal_name').AsString;

          WriteToEventLogDebug('TDataEventAdmin.CheckShifts: InsertShiftCal(' + xShiftNames + ')', mDefaultDebugLogTxt);
          // Transaction ist in InsertShiftCal OK
          Result := InsertShiftCal(xCalId, xShiftNames, xDefShift);
          WriteToEventLogDebug('TDataEventAdmin.CheckShifts: InsertShiftCal() done ', mDefaultDebugLogTxt);
          if not Result then
            break;
          Next;
        end;
      finally
        Free;
      end; // with xDefShift
    end;
    Close;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.CheckShifts failed.' + e.Message);
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.TruncateNumOfIntervals: Boolean;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetNumIntervalRecords;
    Open;
    if FindFirst then begin
      if FieldByName('NumIntervals').AsInteger > cMaxNumberOfIntervals then begin
        Close;
        SQL.Text := cDelOldestInterval;
        ExecSQL;
      end;
      Result := True;
    end;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.TruncateNumOfIntervals failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.CheckInterval: Boolean;
var
  xHour, xMin, xSec, xMSec: Word;
  xIntEnd,
  xLastIntStart: TDateTime;
  xIntPastTime: TDateTime;
  //............................................................
  function GetCalcDateTime(aMinutes: Integer; aDateTime: TDateTime): TDateTime;
  begin
    with mDB.Query[cSecondaryQuery] do begin
      Close;
      Params.Clear;
      SQL.Text := 'select dateadd(minute, -:Min, :DateTime ) NewDateTime ';
      Params.ParamByName('Min').ADOType := adInteger; // wss added
      Params.ParamByName('Min').AsInteger := aMinutes;
      Params.ParamByName('DateTime').ADOType := adDate; // wss added
      Params.ParamByName('DateTime').AsDateTime := aDateTime;
      Open;
      Result := FieldByName('NewDateTime').AsDateTime;
    end;
  end;
  //............................................................
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    // Hole neustes/aktuellstes Interval
    Params.Clear;
    Close;
    SQL.Text := cCheckIntervalAvailability;
    Open;
    xLastIntStart := FieldByName('last_interval').AsDateTime;

    // wenn keines vorhanden war, ist xLastIntStart = 0 (NULL)
    if xLastIntStart < cDateTime1970 then begin
      WriteToEventLogDebug('CheckInterval: Last interval start < 1970 -> invalid', mDefaultDebugLogTxt);
      // da nichts auf der DB vorhanden ist, wird ein fiktiver IntervalStart von der
      // angebrochener Stunde erstellt
      DecodeTime(SysUtils.Now, xHour, xMin, xSec, xMSec);
      xLastIntStart := Date + EncodeTime(xHour, 0, 0, 0);
      xIntEnd := GetCalcDateTime(GetIntLen, xLastIntStart);

      // nun noch ein fiktives Interval, welches bereits "abgeschlossen ist" einf�gen
      // abgeschlossen = IntStart <> IntEnd
      Close;
      SQL.Text := cInsertNewInterval;
      Params.ParamByName('c_Interval_Start').AsDateTime := xLastIntStart;
      Params.ParamByName('c_Interval_End').AsDateTime   := xIntEnd;
      WriteToEventLogDebug('InsertInterval [Insert new interval SQL.Text = ' + SQL.Text + ' ] ', mDefaultDebugLogTxt);
      InsertSQL('t_Interval', 'c_interval_id', high(byte), 1);
    end // if xLastIntStart = 0
    // Eintrag wurde gefunden in der Tabelle t_interval
    else begin
      WriteToEventLogDebug('CheckInterval: Last interval start = ' + DateTimeToStr(xLastIntStart), mDefaultDebugLogTxt);
      // G�ltige L�nge der gesamten Intervalzeiten
      // Now - MaxIntervalZeit ergibt das letzte g�ltige Intervaldatum, welches noch auf
      // der DB geduldet wird...
      xIntPastTime := GetCalcDateTime((GetIntLen * GetMaxInt), SysUtils.Now);

      WriteToEventLogDebug('CheckInterval. LastIntStart =  ' + DateTimeToStr(xLastIntStart) +
                           ' IntPastTime = ' + DateTimeToStr(xIntPastTime), mDefaultDebugLogTxt);

      // ...wenn das letzte vorhandene Intervaldatum �lter ist als das Berechnete, dann
      // wird die komplette Spindeldatentabelle gel�scht.
      if xLastIntStart < xIntPastTime then begin
        Close;
        SQL.Text := 'truncate table t_spindle_interval_data ';
        ExecSQL;
        WriteToEventLogDebug('CheckInterval. t_spindle_interval_data truncated, because xLastIntStart < xIntPastTime ', mDefaultDebugLogTxt);
      end;
    end;

    // Solange xLastIntStart kleiner ist als aktuelle Zeit + 30 Sek wird ein Eintrag
    // in die Tabelle t_interval eingef�gt... bis 1 Interval in die Zukunft
    while (xLastIntStart <= SysUtils.Now + (1 / 86400 * 30)) do begin
      // sicherstellen, dass nie mehr als 200 Eintr�ge in Tabelle vorhanden ist, damit die
      // ID nicht �berlauft!!
      if not TruncateNumOfIntervals then begin
        WriteLog(etError, 'TruncateNumOfIntervals: ' + FormatMMErrorText(mError));
        Exit;
      end;
      // xLastIntStart enh�lt nachher die neue Startzeit vom eingef�gten Interval
      Result := InsertInterval(xLastIntStart);
      if not Result then begin
        WriteLog(etError, 'TDataEventAdmin.CheckInterval: ' + FormatMMErrorText(mError));
        Exit;
      end;
      WriteToEventLogDebug('InsertInterval ok: ' + DateTimeToStr(xLastIntStart), mDefaultDebugLogTxt);
    end;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.CheckInterval failed: ' + e.Message);
    end;
  end;
  if not Result then
    WriteLog(etError, FormatMMErrorText(mError));
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Check DataAquEvent for the future (min. one)
//******************************************************************************
function TDataEventAdmin.CheckDataEvent(aTimestamp: TTimeStamp): Boolean;
var
  xHour, xMin, xSec, xMSec,
    xYear, xMonth, xDay: Word;
begin
  Result := False;

  DecodeDate(TimeStampToDateTime(aTimestamp), xYear, xMonth, xDay);
  DecodeTime(TimeStampToDateTime(aTimestamp), xHour, xMin, xSec, xMSec);

  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cCheckTimeJobList;
    Params.ParamByName('c_execute_type').AsInteger := ord(TExecuteTypEnum(mExecuteTyp));
    Params.ParamByName('c_Job_Type').AsInteger := ord(TJobTyp(mJobTyp));
    Params.ParamByName('c_Time').AsInteger := aTimestamp.Time; //xTimeStamp.Time - (xSec * 1000) - xMSec;
    Params.ParamByName('c_Month').AsInteger := xMonth;
    Params.ParamByName('c_Month_Day').AsInteger := xDay;

    Open;
    Result := FindFirst;
    if not Result then
      mError := SetError(ERROR_NO_DATA, etMMError, 'TDataEventAdmin.CheckDataEvent failed: no data records on DB');
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.CheckDataEvent failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.GetIntLen: Integer;
begin
  Result := 0;
  try
    Result := MMSettings.Value[cIntervalLen];
  except
    on e:Exception do
      WriteLog(etError, 'Error in TDataEventAdmin.GetIntLen. ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get the time of actual fragshift
//******************************************************************************
function TDataEventAdmin.GetActFragStart(var aFragStart: TDateTime; aEventTime: TDateTime): Boolean;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    // Get the time of last shift in the past
    SQL.Text := cGetFragShiftAtTime;
    Params.ParamByName('EventTime').AsDateTime := aEventTime;
    Open;

    aFragStart := FieldByName('c_fragshift_start').AsDateTime;
    Close;
    if aFragStart = 0 then begin
      Result := False;
      mError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'GetActFragStart : No Fragshift available.');
      Exit;
    end;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'GetActFragStart failed.' + e.Message);
      CodeSite.SendError('GetActFragStart failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.InsertFirstFragshift(var aLastFragshiftStart: TDateTime): Boolean;
var
  xFragShiftID: integer;
  xLastQuery, xMSG: string;
begin
  Result := False;
  // get the start time of the latest fragshift
  WriteToEventLogDebug('Begin InsertFirstFragshift', mDefaultDebugLogTxt);
  with mDB.Query[cSecondaryQuery] do
  try
    // Get the time of last shift in the past
    SQL.Text := cGetLastShiftTime;
    xLastQuery := 'cGetLastShiftTime. ';
    Open;
    // Dieses Query sollte immer einen Wert zur�ckgeben, da MAX() verwendet wird
    if not FindFirst then begin
      mError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'InsertFirstFragshift : No Shift available.');
      Exit;
    end;
    // beim ersten mal aufstarten kommt ein NULL Wert zur�ck
    aLastFragshiftStart := FieldByName('c_shift_start').AsDateTime;
    if aLastFragshiftStart <= 0 then begin
      mError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'Error in TDataEventAdmin.InsertFirstFragshift. ' +
        'Wrong Date in table t_shift  c_shift_start = ' + DateTimeToStr(aLastFragshiftStart) +
        'Check the CPU Date !');
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
      Exit;
    end;

    Close;
    CodeSite.SendMsg('Now insert the first fragshift');
    // Now insert the first fragshift
    SQL.Text := cInsertFrag;
    Params.ParamByName('c_fragshift_start').AsDateTime := aLastFragshiftStart;
    xLastQuery := 'cInsertFrag. ';

    WriteToEventLogDebug('InsertFirstFragshift. Before  InsertSQL(' + SQL.Text + ') ', mDefaultDebugLogTxt);

    xFragShiftID := InsertSQL('t_fragshift', 'c_fragshift_id', high(integer), 1);
    Close;

    //Check
    if xFragShiftID = 0 then begin
      xMSG := 'InsertFirstFragshift failed. Can not insert fragshift in t_fragshift. ' +
              'Please check MMWindingLink.dll and stored procedure xp_DeleteMMData.';
      mError := SetError(ERROR_FILE_NOT_FOUND, etDBError, xMSG);
      WriteToEventLogDebug('InsertFirstFragshift. (xFragShiftID = 0)' + xMSG, mDefaultDebugLogTxt, etError);
      Exit;
    end;

    // Insert all shifts which are active in time of the inserted fragshift
    if not InsertShiftFrags(aLastFragshiftStart, xFragShiftID) then begin
      WriteToEventLogDebug('InsertShiftFrags( LastFragshiftStart : ' +
        DateTimeToStr(aLastFragshiftStart) +
        ', FragShiftID : ' + IntToStr(xFragShiftID) +
        ' ) = FALSE ', mDefaultDebugLogTxt, etError);
      Exit;
    end;
    Result := True;
    WriteToEventLogDebug('InsertShiftFrags( LastFragshiftStart : ' +
      DateTimeToStr(aLastFragshiftStart) +
      ', FragShiftID : ' + IntToStr(xFragShiftID) +
      ' ) = TRUE ', mDefaultDebugLogTxt);
  except
    on e:Exception do begin
      if pos(UpperCase('MMWindingLink.dll'), UpperCase(e.Message)) > 0 then
        xMSG := 'InsertFirstFragshift failed. Last query = ' + xLastQuery +
                '(Check exists MMWindingLink.dll and borland.dll)' +
                'System msg: ' + e.Message
      else
        xMSG := 'InsertFirstFragshift failed. Last query = ' + xLastQuery +
                'System msg: ' + e.Message;
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, xMSG);
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.InitTimerange: Boolean;
begin
  Result := False;
  // get the start time of the latest fragshift
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := 'select Count(*) ShiftCount from t_fragshift';
    Open;
    if FieldByName('ShiftCount').AsInteger = 0 then begin
      // First startup of MillMaster after installation
      WriteLog(etWarning, 'No fragshift available. Only acceptable by first startup after installation.');
      Result := InsertFirstFragshift(mLastFragshiftStart);
    end
    else begin
      Close;
      SQL.Text := cInitTimerange;
      Open;
      mLastFragshiftStart := FieldByName('c_fragshift_start').AsDateTime;
      WriteToEventLogDebug('InitTimerange. Last fragshift start = ' + DateTimeToStr(mLastFragshiftStart), mDefaultDebugLogTxt);
      Close;
      Result := True;
    end;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.InitTimerange failed: ' + e.Message);
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get the Fragshift-ID from the actual fragshift
//******************************************************************************
function TDataEventAdmin.GetFragID: Integer;
var
  xStartDate: TDateTime;
  xID, xDiffDays: integer;
  xMSG: string;
  xRecordCount: integer;
begin
  Result := -1;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetFragID;
    Open;
    if FindFirst then begin
      // Neuster Eintrag aus DB holen
      xID := FieldByName('c_fragshift_id').AsInteger;
      // Nun noch ermitteln, ob es fehlerhafte Eintr�ge auf der DB gibt, wo die
      // L�nge von c_fragshift_length ebenfalls = 0 sind
      xRecordCount := 0;
      while not EOF do begin // ADO Conform
        inc(xRecordcount);
        Next;
      end;

      // More than one ID
      // Sollte normalerweise nicht vorkommen!! -> Desshalb Spezialbehandlung
      if xRecordCount > 1 then begin
        FindFirst;
        xStartDate := FieldByName('c_fragshift_start').AsDateTime;

        Close;
        SQL.Text := 'select datediff(day, getdate(), :StartDate ) Days';
        //->  Days <0 => Vergangenheit
        //->  Days =0 => am gleichen Tag
        //->  Days >0 => Zukunft
        Params.ParamByName('StartDate').AsDateTime := xStartDate;
        Params.ParamByName('StartDate').AdoType    := adDate;
        Open;
        xDiffDays := FieldByName('Days').AsInteger;

        // juengste c_fragshift_id ist in der Zukunft
        if xDiffDays > 0 then begin
          xMSG := 'TimeHandler found more than one unfinished fragshifts ' +
                  '(c_fragshift_length = 0 in table t_fragshift). ' +
                  'Fragshift start is one or more days in the future. ' +
                  'Please call MillMaster support.';
          mError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, xMSG);
          WriteLog(etError, xMSG);
          Exit;
        end
        else if xDiffDays < -1 then begin //juengste c_fragshift_id ist in der Vergangenheit
          xMSG := 'TimeHandler found more than one unfinished fragshifts ' +
                  '(c_fragshift_length = 0 in table t_fragshift). ' +
                  'Fragshift start is in the past. ' +
                  'Please call MillMaster support.';
          mError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, xMSG);
          WriteLog(etError, xMSG);
          Exit;
        end
        else //OK: Fragshift start in aktueller oder letzter Schicht xDiffDays=[-1,0]
          Result := xID;
      end else // if xRecordCount > 1
        Result := xID;
    end; // if FindFirst
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.GetFragID failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get the Interval-ID from the actual interval
//******************************************************************************
function TDataEventAdmin.GetIntID(aEventTime: TDateTime): Integer;
begin
  Result := -1;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetIntID;
    Params.ParamByName('EventTime').AsDateTime := aEventTime;
    Open;
    if FindFirst then
      Result := FieldByName('ID').AsInteger
    else
      mError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'No IntId found at ' + DateTimeToStr(aEventTime));
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.GetIntID failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.GetShiftsInTimerange: Boolean;
var
  xActTime: TDateTime;
begin
  // ge�ffnetes Query wird Ausserhalb verwendet. Hier wird es lediglich Initialisiert.
  Result := False;
  xActTime := TimeStampToDateTime(ActTime);
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetShiftsInTimerange;
    Params.ParamByName('LastFragshiftStart').AsDateTime := mLastFragshiftStart;
    Params.ParamByName('ActTime').AsDateTime            := xActTime;
    Open;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'GetShiftsInTimerange failed.' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a new Interval and update the last Interval
//******************************************************************************
function TDataEventAdmin.InsertInterval(var aInsertedInterval: TDateTime): Boolean;
var
  xLastIntervalID: Integer;
  xLastIntervalStart: TDateTime;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    // Get the start of last interval and id
    Close;
    SQL.Text := cGetLastInterval; // Start and ID
    Open;
    xLastIntervalID    := FieldByName('c_interval_id').AsInteger;
    xLastIntervalStart := FieldByName('c_interval_start').AsDateTime;

   (*  Im Zusammenhang mit Datumsfunktionen werden die Datentypen
       der Parameter explizit angegeben um eine Fehlinterpretation des Parametertyps
       unter ADO zu vermeiden *)
    Close;
    SQL.Text := cAddIntLenToIntStart;
    Params.ParamByName('IntLength').AdoType           := adInteger;
    Params.ParamByName('IntLength').AsInteger         := GetIntLen;
    Params.ParamByName('c_interval_start').AdoType    := adDate;
    Params.ParamByName('c_interval_start').AsDateTime := xLastIntervalStart;
    Open;
    aInsertedInterval := FieldByName('newInterval').AsDateTime;

    // Insert new interval
    Close;
    SQL.Text := cInsertNewInterval;
    Params.ParamByName('c_Interval_Start').AsDateTime := aInsertedInterval;
    Params.ParamByName('c_Interval_End').AsDateTime   := aInsertedInterval;
    InsertSQL('t_Interval', 'c_interval_id', high(byte), 1);

    // Update of finished interval with aInsertedInterval
    Close;
    SQL.Text := cUpdateInterval;
    Params.ParamByName('c_Interval_End').AsDateTime := aInsertedInterval;
    Params.ParamByName('c_interval_id').AsInteger := xLastIntervalID;
    ExecSQL;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.InsertInterval failed: ' + e.Message);
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.InsertFrags(aActShift, aLastShift: TDateTime; var aFragShiftID: integer): Boolean;
var
  xMSG: string;
begin
  Result := False;
  aFragShiftID := 0;
  // first update the aLast shift with the length
  with mDB.Query[cThirdQuery] do
  try
    Close; //Nue 3.7.01
    SQL.Text := cUpdateFrag;
    Params.ParamByName('c_fragshift_length').AsInteger := Trunc((TimeStampToMSecs(DateTimeToTimeStamp(aActShift)) -
                                                                 TimeStampToMSecs(DateTimeToTimeStamp(aLastShift))) / (60.0 * 1000.0));
    Params.ParamByName('c_fragshift_start').AsDateTime := aLastShift;
    Params.ParamByName('c_fragshift_start').AdoType    := adDate;
    ExecSQL;
  except
    on e:Exception do
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.InsertFrag failed by update: ' + e.Message);
  end;

  // TODO: was passiert, wenn die oberer Abfrage eine Exception gibt? Sollte dann wirklich hier weitergemacht werden?
  // insert the ActShift
  // Neuer Eintrag in t_fragshift f�r laufende Schicht (ShiftLen = 0)
  with mDB.Query[cThirdQuery] do
  try
    Close; //Nue 3.7.01
    SQL.Text := cInsertFrag;

    WriteToEventLogDebug(Format('TDataEventAdmin.InsertFrags: Insert Fragshift at %s (c_fragshift_start)', [DateTimeToStr(aActShift)]), mDefaultDebugLogTxt);

    // aufgrund diesem Insert wird eine External Procedure aufgerufen, welche Schichtdaten l�scht...
    Params.ParamByName('c_fragshift_start').AsDateTime := aActShift;
    aFragShiftID := InsertSQL('t_fragshift', 'c_fragshift_id', high(integer), 1);

    if aFragShiftID = 0 then begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBerror, 'InsertFrag failed by insert. Please check ' +
                'MMWindingLink.dll and stored procedure xp_DeleteMMData. Perhaps are they not available.');
      Exit;
    end;
    Result := True;
  except
    on e:Exception do begin
      xMSG := 'InsertFrag failed by insert. Error msg: ' + e.Message;
      //...darum wird hier in der Fehlermeldung nach diesem Funktionsnamen gesucht um dies entsprechend zu Protokollieren
      if Pos(Uppercase('.xp_DeleteMMData'), Uppercase(xMSG)) > 0 then
        xMsg := '. Please check MMWindingLink.dll.' + xMsg;
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, xMsg);
    end
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert the next DataEvent into table t_Time_JobList
//******************************************************************************

function TDataEventAdmin.InsertNextDataEvent(aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
var
  xIntervalStart, xShiftStart: TDateTime;
  xInterval, xShiftEv: Boolean;
  xShiftcalID, xShiftInDay: Byte; //Nue: 26.03.01
  xJobRec: TJobRec;
  xNewJob: TJobListRec;
  xHour, xMin, xSec, xMSec,
    xYear, xMonth, xDay: Word;
  xDataEventTimeStamp: TTimestamp;
begin
  Result := True;
  // - Naechster Intervallstart holen (t_interval); Retour -> Zeit
  // Wenn NextInterval ODER NextShiftStart fehl schl�gt, dann wir das ganze abgebrochen,
  // da die ben�tigten Informationen nicht ausgelesen werden k�nnen.
  // Es M�SSEN beide Funktionen mit True zur�ckkommen, damit es weiterl�uft!!
  if not NextIntervalStart(xIntervalStart, aStartSearchTime, aStartUp) or
     // - Naechster Shiftstart holen (t-Shift); Retour -> Zeit
     not NextShiftStart(xShiftStart, xShiftcalID, xShiftInDay, aStartSearchTime, aStartUp) then begin //Nue: 26.03.01 xShiftcalID, xShiftInDay added
    Result := False;
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;

  // - Zeit vergleichen; kleinere Startzeit suchen und
  //   ShiftEV oder RegularInt vom TDataAquEv anhand des Vergleichs setzen
  if xIntervalStart <> xShiftStart then
    if xIntervalStart < xShiftStart then begin
      // Interval < Shift
      xInterval := True;
      xShiftEv := False;
      DecodeDate(xIntervalStart, xYear, xMonth, xDay);
      DecodeTime(xIntervalStart, xHour, xMin, xSec, xMSec);
    end
    else begin
      // Interval > Shift
      xInterval := False;
      xShiftEv := True;
      DecodeDate(xShiftStart, xYear, xMonth, xDay);
      DecodeTime(xShiftStart, xHour, xMin, xSec, xMSec);
    end
  else begin // Interval = Shift
    xInterval := True;
    xShiftEv := True;
    DecodeDate(xShiftStart, xYear, xMonth, xDay);
    DecodeTime(xShiftStart, xHour, xMin, xSec, xMSec);
  end;

  xDataEventTimeStamp := DateTimeToTimeStamp(EncodeTime(xHour, xMin, 0, 0));

  // - Record Initialisieren

  fillchar(xJobRec, sizeof(xJobRec), 0);
  xJobRec.JobTyp := jtDataAquEv;

  xJobRec.DataAquEv.IntID := 0; // ID will be insert in PrepareJob
  xJobRec.DataAquEv.FragID := 0; // ID will be insert in PrepareJob
  xJobRec.DataAquEv.OldIntID := 0; // ID will be insert in PrepareJob
  xJobRec.DataAquEv.OldFragID := 0; // ID will be insert in PrepareJob

  xJobRec.DataAquEv.RegularInt := xInterval;
  xJobRec.DataAquEv.ShiftEv := xShiftEv;

  //Nue: 26.03.01 xShiftcalID, xShiftInDay added
  xJobRec.DataAquEv.ShiftcalID := xShiftcalID;
  xJobRec.DataAquEv.ShiftInDay := xShiftInDay;

  // Fillup the 'xNewJob'-Record
  fillchar(xNewJob, sizeof(xNewJob), 0);
  with xNewJob do begin
    Time := EncodeTime(xHour, xMin, 0, 0) + EncodeDate(xYear, xMonth, xDay);
    WeekDay := '';
    MonthDay := inttostr(xDay);
    Month := inttostr(xMonth);
    Priority := mPriority;
    Execute_Typ := mExecuteTyp;
    JobTyp := mJobTyp;
    Data := xJobRec;
  end;
  // Delete jtDataAquEv from table t_Time_JobList before insert
  mDB.JobMutation('DELETE FROM t_Time_JobList WHERE c_job_type = ' + IntToStr(ord(jtDataAquEv)));
  // - Record into DB (t_Time_JobList)
  mDB.InsertJob(xNewJob);
  WriteLog(etInformation, Format('Next interval/shift: %s', [DateTimeToStr(xNewJob.Time)]));
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.InsertShiftFrags(aActShift: TDateTime; aFragShiftID: integer): Boolean;
begin
  Result := False;
  // FourthQuery 1x Initialisieren
  mDB.Query[cFourthQuery].Close;
  mDB.Query[cFourthQuery].SQL.Text := cInsertShiftFragshift;
  // all shifts which are active in time of aActShift will be inserted in t_shift_fragshift
  with mDB.Query[cThirdQuery] do
  try
    Close;
    SQL.Text := cGetShiftsAtTime;
    Params.ParamByName('ActShift').AsDateTime := aActShift;
    Params.ParamByName('ActShift').AdoType    := adDate;
    Open;
    if not FindFirst then begin
      // Annahme: es wird vorausgesetzt, dass vorhergehend gepr�ft wird, ob Schichten in der Tabelle t_shift
      // vorhanden ist. Hier wird darauf gesetzt. Wenn nicht, dann wird die ganze Aktion abgebrochen, OHNE dass
      // eine Fehlermeldung weitergegeben wird!!
      Result := True;
      mError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'InsertShiftFrags : no shift available');
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt, etError);
      Exit;
    end;

    WriteToEventLogDebug('InsertShiftFrags: SQL.txt = ' + SQL.Text, mDefaultDebugLogTxt);
    // insert every shift concerned on the actual shift
    while (not Eof) do begin // ADO Conform
      mDB.Query[cFourthQuery].ParamByName('c_fragshift_id').AsInteger := aFragShiftID;
      mDB.Query[cFourthQuery].ParamByName('c_shift_id').AsInteger     := FieldByName('c_shift_id').AsInteger;
      mDB.Query[cFourthQuery].ParamByName('c_shiftcal_id').AsInteger  := FieldByName('c_shiftcal_id').AsInteger;
      WriteToEventLogDebug('InsertShiftFrags: xQuery1[0].SQL.Text = ' + mDB.Query[cFourthQuery].SQL.Text, mDefaultDebugLogTxt);
      mDB.Query[cFourthQuery].ExecSQL;
      Next;
    end;
    Close;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.InsertShiftFrags failed: ' + e.Message);
      WriteToEventLogDebug(mError.Msg, mDefaultDebugLogTxt);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.InsertShifts(var aNumShifts: integer): Boolean;
var
  xActShift: TDateTime;
  xLastShift: TDateTime;
  xFragShiftID: integer;
begin
  Result     := False;
  aNumShifts := 0;
  xLastShift := mLastFragshiftStart;

  // Hier geht's schief (nur StartupInit)
  if mDB.DataBase.InTransaction then
    WriteToEventLogDebug('TDataEventAdmin.InsertShifts : mDB.DataBase.InTransaction = TRUE ', mDefaultDebugLogTxt)
  else
    WriteToEventLogDebug('TDataEventAdmin.InsertShifts : mDB.DataBase.InTransaction = FALSE ', mDefaultDebugLogTxt, etWarning);

  // ACHTUNG: hier hat es ein StartTransaction!
  mDB.DataBase.StartTransaction;
  with mDB.Query[cSecondaryQuery] do
  try
    First; //SDO 19.11.02
    while not EOF do begin // ADO Conform
      xActShift := FieldByName('c_shift_start').AsDateTime;
      if not InsertFrags(xActShift, xLastShift, xFragShiftID) then
        Break;

      if not InsertShiftFrags(xActShift, xFragShiftID) then
        Break;

      inc(aNumShifts);
      xLastShift := xActShift;
      Next;
    end;
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.InsertShifts failed: ' + e.Message);
    end;
  end;

  if Result then begin
    mDB.DataBase.Commit;
    WriteLog(etInformation, Format('TDataEventAdmin.InsertShifts done [%d]', [aNumShifts]));
  end
  else begin
    WriteLog(etError, FormatMMErrorText(mError));
    mDB.DataBase.Rollback;
    aNumShifts := 0;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get the next possible Interval
//******************************************************************************
function TDataEventAdmin.NextIntervalStart(var aIntervalStart: TDateTime; aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    if aStartUp then
      SQL.Text := cNextStartUpInterval
    else begin
      SQL.Text := cNextInterval;
      Params.ParamByName('start').AsDateTime := aStartSearchTime;
    end;

    Open;
    if FindFirst then begin
      aIntervalStart := FieldByName('new_interval_Start').AsDateTime;
      Result := True;
    end else begin
      mError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TDataEventAdmin.NextIntervalStart failed. No Interval available. SearchTime = ' + TimeToStr(aStartSearchTime));
    end;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('TDataEventAdmin.NextIntervalStart failed [%s]: ', [TimeToStr(aStartSearchTime)]) + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.NextShiftStart(var aIntervalStart: TDateTime; var aShiftcalID: Byte; var aShiftInDay: Byte;
  aStartSearchTime: TDateTime; aStartUp: Boolean): Boolean;
begin
  Result      := False;
  aShiftcalID := 1;
  aShiftInDay := 1;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    if aStartUp then
      SQL.Text := cNextStartUpShift
    else begin
      SQL.Text := cNextShift;
      Params.ParamByName('start').AsDateTime := aStartSearchTime;
    end;
    Open;
    if FindFirst then begin
      if FieldByName('new_shift_Start').AsDateTime <> 0 then begin
        aIntervalStart := FieldByName('new_shift_Start').AsDateTime;
  //Start nue 26.03.01
        Close;
        SQL.Text := cActShiftInDay;
        Params.ParamByName('start').AsDateTime := aStartSearchTime;
        Params.ParamByName('start').AdoType    := adDate;
        Open;
        if FindFirst then begin // ADO Conform
          aShiftcalID := FieldByName('c_shiftcal_id').AsInteger;
          aShiftInDay := FieldByName('c_shift_in_day').AsInteger;
        end;
        Result := True;
  //End nue 26.03.01
      end
      else begin
        mError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('NextShiftStart : no shift available, new_shift_start = 0 with [%s]', [DateTimeToStr(aStartSearchTime)]));
      end; // if FieldByName
    end else begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('NextShiftStart : FindFirst is False with [%s]', [DateTimeToStr(aStartSearchTime)]));
    end; // if FindFirst
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'NextShiftStart failed.' + e.Message);
      CodeSite.SendError(mError.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ueberprueft nochmals Eintraege auf vorhandensein in der Zukunft
// in den Tabellen t_Fragshift, t_Interval und t_Time_JobList.
// -> Wird nur 1 mal im ProcessJob durchlaufen.
//******************************************************************************
function TDataEventAdmin.CheckAfterStartup: Boolean;
var
  xTimeDumy, xTime: TDateTime;
begin
  Result := False;
  WriteToEventLogDebug('CheckAfterStartup : begin  ', mDefaultDebugLogTxt);
  try
    // 1) Fragmentschicht ueberpruefen
    // dieses Result wird weiter unten einfach �berschrieben!!
    Result := CheckPastFragshifts;
  except
    WriteLog(etError, FormatMMErrorText(mError));
    Exit;
  end;
  WriteToEventLogDebug('CheckAfterStartup done' + mError.Msg, mDefaultDebugLogTxt);

  xTime := TimeStampToDateTime(ActTime);

  // 2)Intervall ueberpruefen eines in der Zukunft
  TruncateNumOfIntervals; // 04.08.1999
  WriteToEventLogDebug('CheckAfterStartup : TruncateNumOfIntervals done  ', mDefaultDebugLogTxt);

  Result := NextIntervalStart(xTimeDumy, xTime, False);
  WriteToEventLogDebug('CheckAfterStartup : NextIntervalStart done  ', mDefaultDebugLogTxt);
  if Result then begin
    // 3)DataEvent in TimeJob Tabelle ueberpruefen; muss ein Eintrag in der Zukunft haben
    Result := CheckDataEvent(ActTime);
    WriteToEventLogDebug('CheckAfterStartup : CheckDataEvent done  ', mDefaultDebugLogTxt);
  end;

  // Im Fehlerfall immer Protokollieren
  if not Result then
    WriteLog(etError, FormatMMErrorText(mError));

  WriteToEventLogDebug('CheckAfterStartup done.');
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.PrepareJob(aJob: pJobRec): Boolean;
var
  xDummyTime, xActFragStart, xEventDateTime: TDateTime;
  xFragID, xIntID: Integer;
begin
  Result := True;

//  xIntID := 0;
//  xFragID := 0;

  if not Initialized then begin
    WriteLog(etError, 'Not Initialized. [TDataEventAdmin.PrepareJob] ');
    Exit;
  end;

  // Fehlende Schichten werden kontinuierlich hinzugef�gt
  // Tieferliegende StartTransaction ist OK
  if not CheckShifts(False) then begin
    WriteLog(etError, 'Check or Insert of Shifts failed. ' + mError.Msg);
    Exit;
  end;
  WriteToEventLogDebug('CheckShifts done', 'TimeHandler: ', etSuccess);

  // IntEvent + ShiftEvent sind schon gesetzt
  aJob.JobTyp := jtDataAquEv;

  // aktuelle FragID holen und in aJob eintragen
  aJob.DataAquEv.FragID := GetFragID;
  if aJob.DataAquEv.FragID < 0 then begin
    WriteLog(etError, 'No Fragshift-ID found in TDataEventAdmin.PrepareJob. ' + mError.Msg);
    Exit;
  end;
  WriteToEventLogDebug('GetFragID done', 'TimeHandler: ', etSuccess);

(*  aJob.DataAquEv.FragID := xFragID;*)
(*  xFragID := 0;*)

  WriteToEventLogDebug('StartTransaction', 'TimeHandler: ', etSuccess);
  // ACHTUNG: Hier ist StartTransacton!! Ist zweifelsfrei sichergestellt, dass
  // keine verschachtelten Transaktionen in der Tiefe verwendet werden?
  Result := False;
  mDB.DataBase.StartTransaction;
  try
    //Insert a new Interval into t_interval and update the last Interval
    if aJob.DataAquEv.RegularInt then begin
      WriteToEventLogDebug('TruncateNumOfIntervals', 'TimeHandler: ', etSuccess);
      TruncateNumOfIntervals; // 04.08.1999
      WriteToEventLogDebug('InsertInterval', 'TimeHandler: ', etSuccess);
      if not InsertInterval(xDummyTime) then begin
        WriteLog(etError, FormatMMErrorText(mError));
        Exit;
      end;
    end;
    WriteToEventLogDebug('RegulatInt done', 'TimeHandler: ', etSuccess);

    // Convert the EventTime (exequte time from a event)
    xEventDateTime := TimeStampToDateTime(EventTime);

    // naechster ShiftEvent in Tab. t_fragshift eintragen und
    // t_shift_fragshift ergaenzen
    if aJob.DataAquEv.ShiftEv then begin
       // get the last fragshift
      if not GetActFragStart(xActFragStart, xEventDateTime) then begin
        WriteLog(etError, FormatMMErrorText(mError));
        Exit;
      end;
      WriteToEventLogDebug('GetActFragStart done', 'TimeHandler: ', etSuccess);
      // - 1) neuer Eintrag in t_fragshift mit Event-Time
      // - 2) aktuelle fragshift (t_Fagshift) mit Laenge aus Reg. updaten
      // In xFragID kommt ein ID-Wert zur�ck, mit welcher in InserShiftFrags Eintr�ge in der
      // Datenbank gemacht werden. Im Fehlerfall xFragID = 0
      if not InsertFrags(xEventDateTime, xActFragStart, xFragID) then begin
        WriteLog(etError, FormatMMErrorText(mError));
        Exit;
      end;
      WriteToEventLogDebug('InsertFrags done', 'TimeHandler: ', etSuccess);

       // - 3) - alle Schichten, welche noch aktiv sind holen
      if not InsertShiftFrags(xEventDateTime, xFragID) then begin
        WriteLog(etError, FormatMMErrorText(mError));
        Exit;
      end;
      WriteToEventLogDebug('InsertShiftFrags done', 'TimeHandler: ', etSuccess);
    end;

    // IntID holen und in aJob eintragen
    // Hier xIntID verwenden, da Member in aJob = Byte ist
    xIntID := GetIntID(TimeStampToDateTime(EventTime));
    if xIntID < 0 then begin
      WriteLog(etError, 'No Interval-ID found in TDataEventAdmin.PrepareJob. ' + mError.Msg);
      Exit;
    end;
    WriteToEventLogDebug('GetIntID done', 'TimeHandler: ', etSuccess);
    aJob.DataAquEv.IntID := xIntID;

    // Convert the EventTime (exequte time from a event)
    xEventDateTime := TimeStampToDateTime(EventTime);
    if not GetLastEventTime(xEventDateTime) then begin
      WriteLog(etError, FormatMMErrorText(mError));
      Exit;
    end;

    // Vorletzte FragID & IntID holen und in aJob eintragen
    if not GetLastFragAndIntID(xEventDateTime, xFragID, xIntID) then begin
      WriteLog(etError, 'No Interval-ID (last) or Fragshift-ID (last) found in TDataEventAdmin.PrepareJob. EventTime = ' + DateTimeToStr(xEventDateTime) + mError.Msg);
      Exit;
    end;
    WriteToEventLogDebug('GetLastFragAndIntID done', 'TimeHandler: ', etSuccess);
    aJob.DataAquEv.OldIntID  := xIntID;
    aJob.DataAquEv.OldFragID := xFragID;

    // Insert next event
    if not InsertNextDataEvent(TimeStampToDateTime(EventTime), False) then begin
      WriteLog(etError, FormatMMErrorText(mError));
      Exit;
    end;
    Result := True;
  finally
    if Result then mDB.Commit
              else mDB.Rollback;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare only a job, if the time has change
//******************************************************************************
function TDataEventAdmin.Emergency(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean;
begin
  mDB.ActTime := aTimeStampAct;
  Result := Init;
  if Result then
    Result := StartupInit;
  WriteToEventLogDebug('Call TDataEventAdmin.Emergency', mDefaultDebugLogTxt);
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.GetLastEventTime(var aEventTime: TDateTime): Boolean;
begin
  Result := False;
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetDataEventStartTime;
    Params.ParamByName('EventTime').AsDateTime := aEventTime;
    Open;

    Result := FindFirst;
    if Result then
      aEventTime := FieldByName('StartTime').AsDateTime
    else
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.GetLastEventTime: No EventTime before ActEventTime found');
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TDataEventAdmin.GetLastEventTime failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAdmin.GetMaxInt: Integer;
begin
  Result := 0;
  try
    Result := MMSettings.Value[cMaxInterval];
  except
    on e:Exception do
      WriteLog(etError, 'Error in TDataEventAdmin.GetMaxInt: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a Default ShiftCalendar ( later as OLE-Automation ) ; CheckShifts
// To a shift must a deafultshift assigned
//******************************************************************************
function TDataEventAdmin.InsertShiftCal(aCalendarID: LongInt; aShiftName: string; aShiftCal: TAppendDefaultShifts): Boolean;
var
  xret: integer;
begin
  Result := False;
  with aShiftCal do
  try
    CalendarID := aCalendarID;
    WriteToEventLogDebug('InsertShiftCal   call TAppendDefaultShifts.Execute', mDefaultDebugLogTxt);
    xret := Execute;

    if xret = 0 then
      WriteLog(etInformation, 'Default Shifts inserted.  Shiftcalendar: ' + aShiftName)
    else
      WriteLog(etError, 'Error in TDataEventAdmin.InsertShiftCal Execute. Errorcode = ' +
        IntToStr(xret) + '   Shiftcalendar : ' + aShiftName);
    Result := True;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etMMError, Format('TDataEventAdmin.InsertShiftCal [%s] failed: %s', [aShiftName, e.Message]));
      WriteLog(etError, mError.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Schreibt Debugmeldungen ins Eventlog
//******************************************************************************
procedure TDataEventAdmin.WriteToEventLogDebug(aMsg, aDefaultText: string; aEvent: TEventType);
begin
  MMEventLog.WriteToEventLogDebug(aMsg, aDefaultText, aEvent, ssTimeHandler);
end;
//------------------------------------------------------------------------------

end.

