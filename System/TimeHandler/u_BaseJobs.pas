{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_BaseJobs.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: BaseClasses :  TBaseJobDispatch, TBaseJobExecute
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function PrepareEmergencyJobs inserted
| 12.01.2000  1.02  SDO | Changed params in func. PrepareNextJob and
|                       | PrepeareNextBaseJob to (aCacheRec: PCacheRec)
| 31.01.2000  1.03  SDO | mSettings.Init in TBaseJobDispatch.Init added
| 25.02.2000  1.03  SDO | mSettings.Init in TBaseJobDispatch.Init deleted ->
|                       | mSettings.Init will exequting in TBaseThread
| 29.02.2000  1.04  SDO | register msg 'Emegency jobs prepared.' in Logfile. -> func GetNextJobs;
| 05.10.2002        LOK | Umbau ADO
| 25.11.2005        Lok | Hints und Warnings entfernt
|==============================================================================}
unit u_BaseJobs;

interface

uses
  Classes, SysUtils, Windows,
  BaseGlobal, BaseMain, BaseThread, u_TimeJobDB, mmEventLog,
  SettingsReader, AdoDBAccess;

type
  TBaseJobDispatch = class(TObject)
  private
    mCache: TCache;
    mCacheItemCounter: Integer;
    fStartupInitDone: Boolean;
    function setDataIntoCache: Boolean;
  protected
    mEventLog: TEventLogWriter;
    mTimeChange: Boolean;
    mSettings: TMMSettingsReader;

    procedure DelOldJob(aCacheRec: PCacheRec);
    function PrepareNextJob(aCacheRec: PCacheRec): Boolean; virtual; abstract;
    function PrepeareNextBaseJob(aCacheRec: PCacheRec): Boolean; virtual;
    function PrepareEmergencyJobs(aQuery: TNativeAdoQuery; aTimeStampOld, aTimeStampNew: TTimeStamp): Boolean; virtual; abstract;
    function Init: Boolean; virtual;
    //property Settings : TBaseSettingsReader read fSettings;
  public
    mDB: TTimeJobDB;
    constructor Create(aEventLog: TEventLogWriter; aSettings: TMMSettingsReader); virtual;
    destructor Destroy; override;

    function GetNextJobs(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean; virtual;
    function ReadNextJob(var aJob: PCacheRec): Boolean; virtual;
    function CallStartupInit: Boolean; virtual;
    property StartupInitDone: boolean read fStartupInitDone;
  end;
  //--------------------------------------------------------------------------
  TBaseJobExecute = class(TBaseSubThread)
  private
  protected
    mJobDispatch: TBaseJobDispatch;
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
    procedure SendBaseProcessJob(aCacheRec: PCacheRec);
    procedure SendProcessJob(aCacheRec: PCacheRec); virtual; abstract;
  public
    constructor Create(aThreadDef: TThreadDef); override;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  u_CleanUpEvAdmin, u_JobDispatch;

//------------------------------------------------------------------------------
// TBaseJobDispatch
//------------------------------------------------------------------------------

//******************************************************************************
// Call Administrators for pre preparing and pre checks
//******************************************************************************
function TBaseJobDispatch.CallStartupInit: Boolean;
begin
  if not fStartupInitDone then begin
    // Prepare base xAdministrators before working
    with TCleanUpEvAdmin.Create(self.mDB, mEventLog, mSettings) do
    try
      if Init then
        StartupInit;
    finally
      Free;
    end;
    fStartupInitDone := True;
  end; // if not fStartupInitdone
  Result := fStartupInitDone;
end;
//------------------------------------------------------------------------------
constructor TBaseJobDispatch.Create(aEventLog: TEventLogWriter; aSettings: TMMSettingsReader);
begin
  inherited Create;
  mCacheItemCounter := 0;
  mEventLog         := aEventLog;
  mSettings         := aSettings;
  fStartupInitDone  := False;
  mCache            := TCache.Create;
  mDB               := TTimeJobDB.Create(aEventLog);
end;
//------------------------------------------------------------------------------
procedure TBaseJobDispatch.DelOldJob(aCacheRec: PCacheRec);
begin
 // mDB.DeleteJob(aCacheRec.ID); 02.07.1999
end;
//------------------------------------------------------------------------------
destructor TBaseJobDispatch.Destroy;
begin
  mDB.Free;
  mCache.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get all jobs for the next minutes from database  (normally)
//******************************************************************************
function TBaseJobDispatch.GetNextJobs(aTimeStampOld, aTimeStampAct: TTimeStamp): Boolean;
var
  xQuery: TNativeAdoQuery;
  xTimeStamp: TTimeStamp;
  xDateTime, xDateTimeAct, xDateTimeOld: TDateTime;
begin
  Result := False;

  if mCache = nil then begin
    mEventLog.Write(etError, 'TBaseJobDispatch.GetNextJobs: The cache isn�t prepared.');
    Exit;
  end;

  try
    // Hole jobs g�ltig f�r n�chste Minute
    Result := mDB.RequestData(mCache, aTimeStampAct);
    if not Result then
      mEventLog.Write(etError, 'TBaseJobDispatch.GetNextJobs: A unknown error in mDB.RequestData');
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TBaseJobDispatch.GetNextJobs failed: ' + e.Message);
      Exit;
    end;
  end;

  // TODO: setDataIntoCache
  Result := setDataIntoCache;
  if not Result then
    mEventLog.Write(etError, 'TBaseJobDispatch.GetNextJobs.setDataIntoCache failed');

  // mCache is ready and complete for reading now (to Jobexecute)

  // Emergency: time has changed
  if mTimeChange then // 4.8.1999
  try
    Result := False;
    // Prepare unexecuted jobs which are max. 5 min. old from now
    xDateTimeAct := TimeStampToDateTime(aTimeStampAct);
    xDateTimeOld := TimeStampToDateTime(aTimeStampOld);
    xDateTime    := xDateTimeAct - xDateTimeOld;

    if (xDateTime > 0) and (xDateTime < (1 / 1440 * cPreRestartOffset)) then
      xDateTime := TimeStampToDateTime(aTimeStampAct) - (1 / 1440 * cPreRestartOffset)
    else if (xDateTime = 0) then
      xDateTime := xDateTimeAct
    else
      xDateTime := xDateTimeOld;

    xTimeStamp := DateTimeToTimeStamp(xDateTime);

    // Bei Zeitumstellung innerhalb von 5 Minuten werden verpasste Aufgaben geholt und
    // trotzdem ausgef�hrt.
    // TODO: Ist dieses tempor�re Query noch n�tig? mDB enth�lt nun 4 Queries...
    xQuery := TNativeAdoQuery.Create;
    xQuery.Assign(mDB.Query[cPrimaryQuery]);
    try
      // Datensets holen...
      if mDB.RequestEmergencyData(xTimeStamp, aTimeStampAct, xQuery) then
        //...und ausf�hren (in der abgeleiteten Klasse)
        Result := PrepareEmergencyJobs(xQuery, xTimeStamp, aTimeStampAct);
    finally
      xQuery.Free;
    end;
    mEventLog.Write(etWarning, 'TBaseJobDispatch.GetNextJobs: Emegency jobs prepared.');
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TBaseJobDispatch.GetNextJobs failed: Assign of Query failed in TBaseJobDispatch.GetNextJobs.' + e.Message);
    end;
  end; // if mTimeChanged
end;
//------------------------------------------------------------------------------
function TBaseJobDispatch.Init: Boolean;
begin
  Result := mDB.Init;
  if not Result then
    mEventLog.Write(etError, 'TBaseJobDispatch.Init failed: ' + mDB.DBErrorTxt);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Read a job from cache and put a Item in a cacherecord
//******************************************************************************
function TBaseJobDispatch.ReadNextJob(var aJob: PCacheRec): Boolean;
begin
  Result := False;
  try
    if mCacheItemCounter < (mCache.Count) then begin
      aJob := mCache.Items[mCacheItemCounter];
      inc(mCacheItemCounter);
      Result := True;
    end
    else begin
      mCacheItemCounter := 0;
      mCache.Clear;
    end;
  except
    on e:Exception do begin
      raise Exception.Create('TBaseJobDispatch.ReadNextJob failed: ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare a basejob
//******************************************************************************
//function TBaseJobDispatch.PrepeareNextBaseJob(aJob: TJobTyp;
//  aData: PJobRec): Boolean;
function TBaseJobDispatch.PrepeareNextBaseJob(aCacheRec: PCacheRec): Boolean;
begin
  Result := False;
  case aCacheRec.JobTyp of
    jtCleanUpEv:
      with TCleanUpEvAdmin.Create(self.mDB, mEventLog, mSettings) do
      try
        try
          if Init then
            Result := PrepareJob(@aCacheRec.Data);
        except
          on e:Exception do begin
            mEventLog.Write(etError, 'TBaseJobDispatch.PrepeareNextBaseJob failed: TCleanUpEvAdmin ' + e.Message);
          end;
        end;
      finally
        Free;
      end;
  else
    Result := PrepareNextJob(aCacheRec); // Prepare MM-Jobs
  end;
end;
//------------------------------------------------------------------------------
function TBaseJobDispatch.setDataIntoCache: Boolean;
var
  x: Integer;
  xDaten: PCacheRec;
begin
  Result := True;
  for x := 0 to mCache.count - 1 do begin
    xDaten := mCache.Items[x];
    //Result:=PrepeareNextBaseJob(xDaten.JobTyp, @xDaten.Data);
    Result := PrepeareNextBaseJob(xDaten);
    if not Result then begin
      WriteToEventLogDebug(Format('PrepareNextBaseJob failed: ID=%d, JobTyp=%d', [xDaten.ID, ord(xDaten.JobTyp)]), 'TimeHandler: ', etSuccess);
      {
        TCacheRec = record
          ID: Integer;
          JobTyp: TJobTyp;
          Data: TJobRec;
          Execute_Typ: TExecuteTypEnum;
        end;
      {}
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++ TBaseJobExecute
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

constructor TBaseJobExecute.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mJobDispatch := Nil;
end;
//------------------------------------------------------------------------------
destructor TBaseJobExecute.Destroy;
begin
  mJobDispatch.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseJobExecute.Init: Boolean;
begin
  Result := inherited Init;
  //mJobDispatch.fSettings:= Params;
  if not Result then begin
    mEventLog.Write(etError, 'Error in baseclasses');
    Exit;
  end;
  // Create Timehandler Base-Classes and connect DB
  mJobDispatch := TJobDispatch.Create(mEventLog, Params);
  Result       := mJobDispatch.Init;

  mConfirmStartupImmediately := False;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Send a Base-job to JobManager
//******************************************************************************
procedure TBaseJobExecute.SendBaseProcessJob(aCacheRec: PCacheRec);
begin
  try
    case aCacheRec.JobTyp of
      jtCleanupEv: begin
          // Send to JobMgr
          //todo: JobRec verwendung
          mJob^ := aCacheRec.Data;
          WriteJobBufferTo(ttJobManager);

          mJobDispatch.mDB.ExecutedJob(aCacheRec.ID, SysUtils.now);

          WriteLog(etInformation, 'Cleanup job done');
          WriteToEventLogDebug(Format('Data: %d', [mJob.CleanUpEv.TimePeriod]), 'jtCleanupEv: ');

          // ONCE and NOW - JobTypes will be deleted after sending
          if (aCacheRec.Execute_Typ = etONCE) or (aCacheRec.Execute_Typ = etNOW) then
            // TODO: Uiuiui, hier wird auf die Membervariable von Unterobject DIREKT zugegriffen!!
            // w�re eine Methode in diesem Fall nicht besser?
            mJobDispatch.mDB.DeleteJob(aCacheRec.ID);
        end;
    else
    end;
  except
    on e:Exception do
      WriteLog(etError, 'TBaseJobExecute.SendBaseProcessJob failed: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseJobExecute.ProcessInitJob;
var
  x: Integer;
  xCacheRec: PCacheRec;
  xTimeOld, xTimeAct: TTimeStamp;
  xResult: Boolean;
begin
  try
    xTimeAct := mJob.TimerEvnet.TimeStamp;
    xTimeOld := mJob.TimerEvnet.TimeStampOld;
  except
    on e:Exception do begin
      WriteLog(etError, 'Error in TBaseJobExecute.ProcessInitJob with timestamp : Error msg : ' + e.Message);
      Exit;
    end;
  end;

  if mJob.TimerEvnet.FirstAsynchEvent = True then begin
    CodeSite.SendMsg('TBaseJobExecute.ProcessInitJob: FirstAsynchEvent');
    // Set ActTime- and EventTime-Property
    // (Time from TTicker to TTimeJobDB)
    mJobDispatch.mDB.ActTime := xTimeAct;
    if not mJobDispatch.CallStartupInit then begin
      WriteLog(etError, 'Restart MillMaster because an error is in a TAdministrator.[TBaseJobExecute.ProcessJob]. ');
      WriteToMain(gcRestartMM, ERROR_INVALID_DATA);
      Exit;
    end;
  end;
  WriteToEventLogDebug('TBaseJobExecute.ProcessInitJob: before GetNextJobs', '');

  // Read next Jobs every min. (next actual jobs as minutespack)
  xResult := mJobDispatch.GetNextJobs(xTimeOld, xTimeAct);
  if not xResult then
    WriteLog(etWarning, 'TBaseJobExecute.ProcessInitJob: GetNextJobs not ok');

  // Delete old Jobs from table t_time_joblist
  for x := 0 to mJobDispatch.mCache.count - 1 do begin
    xCacheRec := mJobDispatch.mCache.Items[x];
    // Delete the Once and Now jobs from database
    case xCacheRec.Execute_Typ of
      etNOW: mJobDispatch.DelOldJob(xCacheRec);
    else
    end;
  end;

  if not xResult then begin
    // Error in a TAdministrator -> Restart MillMaster
    WriteLog(etError, 'Restart MillMaster because an error is in a ' +
                      'TAdministrator. [TBaseJobExecute.ProcessJob]. ' +
                      'Can not prepare BaseJobs in TBaseJobDispatch.setDataIntoCache.');
    WriteToMain(gcRestartMM, ERROR_INVALID_DATA);
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseJobExecute.ProcessJob;
var
  xCacheRec: PCacheRec;
  xTimeOld, xTimeAct: TTimeStamp;
begin
  xTimeAct := mJob.TimerEvnet.TimeStamp;
  xTimeOld := mJob.TimerEvnet.TimeStampOld;
  // Read Cache
  try
    // Aktuelle Jobs in Liste abarbeiten, welche von der vorhergehenden Minute
    // in den Cache gelesen wurden.
    while mJobDispatch.ReadNextJob(xCacheRec) do begin
      case xCacheRec.JobTyp of
        jtCleanupEv: SendBaseProcessJob(xCacheRec); // Base jobs
      else
        SendProcessJob(xCacheRec);
      end; // End Case
    end;

    mJobDispatch.mTimeChange := mJob.TimerEvnet.TimeChange;
    // Neue Jobs f�r n�chste Minute in Cache einlesen
    if not mJobDispatch.GetNextJobs(xTimeOld, xTimeAct) then begin
      // Error in a TAdministrator -> Restart MillMaster
      WriteLog(etError, 'Restart MillMaster because an error is in a ' +
        'TAdministrator. [TBaseJobExecute.ProcessJob]. ' +
        'Can not prepare BaseJobs in TBaseJobDispatch.setDataIntoCache. ' +
        ' MillMaster will restart !');
      WriteToMain(gcRestartMM, ERROR_INVALID_DATA);
    end;
  except
    on e:Exception do begin
      // nur dann die Exception protokollieren und anschliessend ein Restart beantragen,
      // wenn nicht schon im Stopp Mode
      if not Terminated then begin
        WriteLog(etError, 'TBaseJobExecute.ProcessJob failed: ' + e.Message);
        WriteToMain(gcRestartMM, ERROR_INVALID_DATA);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------

end.

