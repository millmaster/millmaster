{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_JobDispatch.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Manage the administrators and call them if required.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function PrepareEmergencyJobs inserted
| 12.01.2000  1.02  SDO | Jobs,  which has no Admin will be deleted added in
|                       | PrepareNextJob and changed params to (aCacheRec: PCacheRec)
| 20.04.2001  1.03  SDO | Codesite eingebaut (nach stillstand ist die DB gelockt
| 05.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
|==============================================================================}
unit u_JobDispatch;

interface

uses
  Classes, SysUtils, BaseGlobal, BaseMain, mmList, AdoDBAccess,
  u_BaseJobs, u_TimeJobDB, mmEventLog, SettingsReader;

type
  TJobDispatch = class(TBaseJobDispatch)
  private
  protected
    function PrepareNextJob(aCacheRec: PCacheRec): Boolean; override;
    function PrepareEmergencyJobs(aQuery: TNativeAdoQuery; aTimeStampOld, aTimeStampNew: TTimeStamp): Boolean; override;
    function Init: Boolean; override;
  public
    constructor Create(aEventLog: TEventLogWriter; aSettings: TMMSettingsReader); override;
    destructor Destroy; override;
    function CallStartupInit: Boolean; override;
    function CheckAfterStartup: Boolean;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  u_DataEventAdmin, u_DelShiftByTimeAdmin, u_DelIntAdmin,
  u_CleanUpEvAdmin;

//******************************************************************************
// Initialy the administrators at first start after installation or start
//******************************************************************************
function TJobDispatch.CallStartupInit: Boolean;
begin
  Result := True;

  // Delete old Jobs (Typ: Now, Once)
  mDB.DeleteOldJobs(DateTimeToTimeStamp(SysUtils.Now));

  if StartupInitDone then
    Exit;
//  if not Result then Exit;

  // Administrator: TDelShiftByTimeAdmin
  with TDelShiftByTimeAdmin.Create(mDB, mEventLog, mSettings) do
  try
    Result := Init;
    if Result then
      Result := StartupInit;
  finally
    Free;
  end;

  if Result then
    CodeSite.SendMsg('TJobDispatch.CallStartupInit : TDelShiftByTimeAdmin.StartupInit done')
  else
    Exit;

  // Administrator: TDelIntAdmin
  with TDelIntAdmin.Create(mDB, mEventLog, mSettings) do
  try
    Result := Init;
    if Result then
      Result := StartupInit;
  finally
    Free;
  end;

  if not Result then
    Exit;

  // Administrator: TDataEventAdmin
  with TDataEventAdmin.Create(mDB, mEventLog, mSettings) do
  try
    Result := Init;
    if Result then
      Result := StartupInit;
  finally
    Free;
  end;

  if not Result then
    Result := inherited CallStartupInit;
end;
//------------------------------------------------------------------------------
constructor TJobDispatch.Create(aEventLog: TEventLogWriter; aSettings: TMMSettingsReader);
begin
//  inherited Create(aEventLog, TTimeJobDB.Create(aEventLog), aSettings);
  inherited Create(aEventLog, aSettings);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare MM xAdministrators before working
//******************************************************************************
function TJobDispatch.CheckAfterStartup: boolean;
begin
  Result := False;
  try
    // Administrator: TDataEventAdmin
    with TDataEventAdmin.Create(mDB, mEventLog, mSettings) do
    try
      Result := Init;
      if Result then
        Result := CheckAfterStartup;
    finally
      Free;
    end;
    if not Result then Exit;

  except
    mEventLog.Write(etError, 'Can�t create the TDataEventAdmin. [TJobDispatch.CheckAfterStartup]')
  end;
end;
//------------------------------------------------------------------------------
destructor TJobDispatch.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TJobDispatch.Init: Boolean;
begin
  Result := inherited init;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Distribute a specify job and send it to a xAdministrator for preparing
//******************************************************************************
function TJobDispatch.PrepareNextJob(aCacheRec: PCacheRec): Boolean;
begin
  Result := True;
  case aCacheRec.JobTyp of
    jtDelInt:
      with TDelIntAdmin.Create(mDB, mEventLog, mSettings) do
      try
        try
          Result := Init;
          if Result then
            Result := PrepareJob(@aCacheRec.Data); // FALSE = MMRestart
        except
          on e: exception do begin
            Result := False; // FALSE = MMRestart
            mEventLog.Write(etError, 'TJobDispatch.PrepareNextJob failed: TDelIntAdmin: ' + e.Message);
          end;
        end;
      finally
        Free;
      end;

    jtDelShiftByTime:
        with TDelShiftByTimeAdmin.Create(mDB, mEventLog, mSettings) do
        try
          try
            Result := Init;
            if Result then
              Result := PrepareJob(@aCacheRec.Data);
          except
            on e:Exception do begin
              Result := False;
              mEventLog.Write(etError, 'TJobDispatch.PrepareNextJob failed: TDelShiftByTimeAdmin: ' + e.Message);
            end;
          end;
        finally
          Free;
        end;

    jtDataAquEv:
        with TDataEventAdmin.Create(mDB, mEventLog, mSettings) do
        try
          try
            Result := Init;
            if Result then
              Result := PrepareJob(@aCacheRec.Data);
          except
            on e:Exception do begin
              Result := False;
              mEventLog.Write(etError, 'TJobDispatch.PrepareNextJob failed: TDataEventAdmin: ' + e.Message);
            end;
          end;
        finally
          Free;
        end;
  else
    // Jobs, which has no Admin will be deleted
    mEventLog.Write(etWarning, 'Unkown Job has no administrator. Job will be delete immediately. JobtypeID = ' + IntToStr(Ord(aCacheRec.JobTyp)));
    mDB.DeleteJob(aCacheRec.ID);
  end; // end Case
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Prepare jobs, if the time was change
//******************************************************************************
function TJobDispatch.PrepareEmergencyJobs(aQuery: TNativeAdoQuery; aTimeStampOld, aTimeStampNew: TTimeStamp): Boolean;
var
  xJobTyp: TJobTyp;
  xMsg: string;
begin
  Result := False;
  with aQuery do
  try
    First;
    while not EOF do begin // ADO Conform
      xJobTyp := aQuery.FieldByName('c_Job_type').Value;
      xMsg := '';
      case xJobTyp of
        jtDelInt: // Administrator: TDelIntAdmin
          with TDelIntAdmin.Create(mDB, mEventLog, mSettings) do
          try
            xMsg := 'DelIntAdmin failed: ';
            Result := Emergency(aTimeStampOld, aTimeStampNew);
          finally
            Free;
          end;
        jtDelShiftByTime: // Administrator:  TDelShiftByTimeAdmin
          with TDelShiftByTimeAdmin.Create(mDB, mEventLog, mSettings) do
          try
            xMsg := 'DelShiftByTimeAdmin failed: ';
            Result := Emergency(aTimeStampOld, aTimeStampNew);
          finally
            Free;
          end;
        jtDataAquEv: // Administrator: TDataEventAdmin
          with TDataEventAdmin.Create(mDB, mEventLog, mSettings) do
          try
            xMsg := 'DataEventAdmin failed: ';
            Result := Emergency(aTimeStampOld, aTimeStampNew);
          finally
            Free;
          end;
        jtCleanupEv: // CleanUpEv
          with TCleanUpEvAdmin.Create(mDB, mEventLog, mSettings) do
          try
            xMsg := 'CleanUpEvAdmin failed: ';
            Result := Emergency(aTimeStampOld, aTimeStampNew);
          finally
            Free;
          end;
      else
      end; // case
      Next;
    end; // end while
  except
    on e:Exception do begin
      xMsg := 'TJobDispatch.PrepareEmergencyJobs.' + xMsg + e.Message;
    end;
  end; // end with

  if not Result then
    mEventLog.Write(etError, xMsg);
end;
//------------------------------------------------------------------------------
end.

