{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_TimeJobDB.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: BaseClass   :  TTimeJobDB
|
|                 Additionals
|                 -----------
|                 Class       :  TCache
|                 Enumeration :  TExecuteTypEnum ; Enumtypes for
|                                                  table 'TimeJobList'
|                 Records     :  TJobListRec, TCacheRec
|                 Function    :  TimeStampToMin ; Calculate the minutes
|                                                 since Big Bang (1899)
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.1999  1.00  SDO | File created
| 12.07.1999  1.01  SDO | function RequestEmergencyData  (Timechange; +-5 Min. from now)
| 02.09.1999  1.02  SDO | function CheckLosedEvents      (older than now)
| 01.02.2000  1.03  SDO | - Inserting a timestamp for each job in func.
|                       |   InsertJob (c_Last_Execute_Time)
|                       | - function CheckLostEvents has a new filter. It check then
| 06.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 11.12.2002        Wss | Wird nun mit 4 Queries initialisiert (statt nur 2)
| 25.11.2005        Lok | Hints und Warnings entfernt
|==============================================================================}
unit u_TimeJobDB;
//  Classes: TBasexAdministrator, TTimeJobDB, TCache
//  Def: TExecuteTypEnum, TCacheRec, TJobListRec
interface

uses
  Classes, SysUtils, Windows, BaseGlobal, BaseMain, AdoDBAccess,
  mmList, mmEventLog;

const
  cPreRestartOffset = 4; // Min

type
  TExecuteTypEnum = (etONCE, etperSHIFT, etDAILY, etWEEKLY, etMONTHLY, etREPEATED, etNOW);
  //..........................................................
  TJobListRec = record
    Execute_Typ: TExecuteTypEnum;
    Time: TDateTime;
    WeekDay: string;
    MonthDay: string;
    Month: string;
    Priority: Byte;
    JobTyp: TJobTyp;
    Last_ExecuteTime: TDateTime;
    ErrorFlag: Boolean;
    Data: TJobRec;
  end;
  pJobListRec = ^TJobListRec;
  //..........................................................
  PCacheRec = ^TCacheRec;
  TCacheRec = record
    ID: Integer;
    JobTyp: TJobTyp;
    Data: TJobRec;
    Execute_Typ: TExecuteTypEnum;
  end;
  //..........................................................
  TCache = class(TmmList)
  private
    function GetItems(aIndex: Integer): PCacheRec;
  public
    property Items[aIndex: Integer]: PCacheRec read GetItems;
    procedure Clear; override;
  end;
  //..........................................................
  TTimeJobDB = class(TAdoDBAccess)
  private
    fEventTime: TTimeStamp;
    fActTime: TTimeStamp;
    function AddTime(aMin: integer; aActualTime: TDateTime; var aNewTime: TDateTime): Boolean;
    function FillCache(var aCache: TCache; aQuery: TNativeAdoQuery): Boolean;
    procedure SetActTime(const Value: TTimeStamp);
  protected
    mEventLog: TEventLogWriter;
    mError: TErrorRec;
  public
    constructor Create(aEventLog: TEventLogWriter); virtual;
    function RequestData(var aCache: TCache; aTimeStamp: TTimeStamp): Boolean; virtual; // can raise an exception
    function RequestEmergencyData(aTimeOld, aTimeNew: TTimeStamp; aQuery: TNativeAdoQuery): Boolean; virtual; // can raise an exception
    function InsertJob(aNewJob: TJobListRec): Boolean; virtual;
    procedure JobMutation(aSQLText: string);
    procedure DeleteJob(aId: Integer); virtual;
//    procedure UpdateJob(aId: Integer; aUpdatedJob: TJobListRec); virtual;
    procedure DeleteOldJobs(aTimeStamp: TTimeStamp); virtual;
    procedure ExecutedJob(aId: Integer; aDateTime: TDateTime); virtual;
    function CheckLostEvents(var aCache: TCache; aTimeStamp: TTimeStamp): Boolean; virtual; // can raise an exception
    property EventTime: TTimeStamp read fEventTime;
    property ActTime: TTimeStamp read fActTime write SetActTime;
  end;
  //..........................................................
const
  cQryDeleteAllOldJobs = 'Delete t_time_joblist ' +
    'where DATEDIFF(day, c_last_execute_time, getdate() ) >= %d ' +
    'Or c_last_execute_time IS NULL';

  cQryDeleteExecuteTyp = 'Delete From t_time_joblist Where c_execute_type = %d';

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS;

//------------------------------------------------------------------------------
// TTimeJobDB
//------------------------------------------------------------------------------
function TTimeJobDB.AddTime(aMin: integer; aActualTime: TDateTime; var aNewTime: TDateTime): Boolean;
begin
  with Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := 'select dateadd ( minute, :addMin, :ActualTime ) newTime';
    ParamByName('addMin').ADOType   := adInteger;
    ParamByName('addMin').AsInteger := aMin;
    ParamByName('ActualTime').ADOType    := adDate;
    ParamByName('ActualTime').AsDateTime := aActualTime;
    Open;
    aNewTime := FieldByName('newTime').AsDateTime;
    Result := True;
  except
    on e:Exception do begin
      raise Exception.Create('TTimeJobDB.AddTime failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Check the losed events there are older than now
//******************************************************************************
function TTimeJobDB.CheckLostEvents(var aCache: TCache; aTimeStamp: TTimeStamp): Boolean;
var
  xYearNow, xMonthDayNow, xMonthNow,
  xHourNow, xMinNow, xSec, xMSec: Word;
  xDateTimeNow: TDateTime;
  xExequteTyp: Integer;
  xJobTypText: string;
  xMsg: String;
begin
  try
    xMsg := '[Conversion]';
    xDateTimeNow := TimeStampToDateTime(aTimeStamp);
    DecodeTime(xDateTimeNow, xHourNow, xMinNow, xSec, xMSec);
    DecodeDate(xDateTimeNow, xYearNow, xMonthNow, xMonthDayNow);
    xDateTimeNow := EncodeTime(xHourNow, xMinNow, 0, 0) + EncodeDate(xYearNow, xMonthNow, xMonthDayNow);
    xExequteTyp := ord(TExecuteTypEnum(etONCE));

    // SQL Time-Job FILTER
    xMsg := '[Query]';
    with Query[cPrimaryQuery] do begin
      Close;
      Params.Clear;
      SQL.Text := 'SELECT * FROM t_time_joblist' +
                  ' WHERE c_execute_type  = ' + IntToStr(xExequteTyp) +
                  ' AND c_last_execute_time < :DateTimeNow ' + //+ DateTimeToStr( xDateTimeNow ) +
                  ' ORDER BY c_priority  ASC ';
      Params.ParamByName('DateTimeNow').asDateTime := xDateTimeNow - (1 / 1440); // - 1Min.
      Open;
      while not EOF do begin // ADO Conform
        case TJobTyp(self.Query[cPrimaryQuery].FieldByName('c_Job_type').Value) of
          jtDelInt: xJobTypText := 'DelInt';
          jtDataAquEv: xJobTypText := 'DataAquEv';
          jtDelShiftByTime: xJobTypText := 'DelShiftByTime';
          jtCleanUpEv: xJobTypText := 'CleanUpEv';
        else
          xJobTypText := 'Unknown';
        end;

        mEventLog.Write(etWarning, 'Lost ' + xJobTypText +
          ' job found. Job will execute as next. ' +
          '[Forgotten execute time: ' + DateTimeToStr(self.Query[cPrimaryQuery].FieldByName('c_last_execute_time').AsDateTime) + ']');
        Next;
      end; // while
      Result := FillCache(aCache, Query[cPrimaryQuery]);
    end; //with
  except
    on e:Exception do begin
      raise Exception.Create(Format('TTimeJobDB.CheckLostEvents %s failed: %s', [xMsg, e.Message]));
    end;
  end; // try
end;
//------------------------------------------------------------------------------
constructor TTimeJobDB.Create(aEventLog: TEventLogWriter);
begin
  inherited Create(4);
  fEventTime := DateTimeToTimeStamp(0); // Date = 30/12/1899 0:00
  mEventLog  := aEventLog;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Delete a specific job from the database
//******************************************************************************
procedure TTimeJobDB.DeleteJob(aId: Integer);
begin
  with Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('DELETE ');
    SQL.Add('FROM t_time_joblist ');
    SQL.Add('WHERE c_time_joblist_id = :c_time_joblist_id ');
    Params.ParamByName('c_time_joblist_id').AsInteger := aID;
    ExecSQL;
  except
    on e:Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'DeleteJob failed. TimeJob ID = ' +
        Inttostr(aID) + ' [TTimeJobDB.DeleteJob]  ' + e.Message);
      mEventLog.Write(etError, FormatMMErrorText(mError));
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Delete all old jobs (execute_typ: now) from database
// (Delete with TCacheRec.ID need more database access)
//******************************************************************************
procedure TTimeJobDB.DeleteOldJobs(aTimeStamp: TTimeStamp);
var
  xYear, xMonthDay, xMonth: Word;
begin
  DecodeDate(TimeStampToDateTime(aTimeStamp), xYear, xMonth, xMonthDay);
  with Query[cPrimaryQuery] do
  try
    // Event is older then now  ->
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Text := Format(cQryDeleteExecuteTyp, [ord(etNOW)]);
    ExecSQL;

    // Alles loeschen, was aelter als 10 Tage ist oder kein Zeitstempel besizten
    Close;
    Params.Clear;
    SQL.Text := Format(cQryDeleteAllOldJobs, [10]); // alles was �lter ist als 10 Tage l�schen
    ExecSQL;
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TTimeJobDB.DeleteOldJobs failed: ' + e.Message);
    end;
  end; // End try
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Set a Timestamp in to t_time_joblist after sending Job to Jobmanager
//******************************************************************************
procedure TTimeJobDB.ExecutedJob(aId: Integer; aDateTime: TDateTime);
begin
  with Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('UPDATE t_time_joblist ');
    SQL.Add('SET c_last_execute_time = :c_last_execute_time ');
    SQL.Add('WHERE c_time_joblist_id = :c_time_joblist_id ');

    Params.ParamByName('c_time_joblist_id').AsInteger    := aId;
    Params.ParamByName('c_last_execute_time').asDateTime := aDateTime;
    ExecSQL;
  except
    on e:Exception do
      mEventLog.Write(etError, 'TTimeJobDB.ExecutedJob failed: ' + e.Message);
  end;
end;

//******************************************************************************
// Fillup the Cache with a query-result
//******************************************************************************
function TTimeJobDB.FillCache(var aCache: TCache; aQuery: TNativeADoQuery): Boolean;
var
  xDaten: PCacheRec;
  xStream: TMemoryStream;
begin
  with aQuery do
  try
    First;
    while not EOF do begin // ADO Conform
      New(xDaten);
      ZeroMemory(xDaten, sizeof(TCacheRec));
      xDaten^.ID          := FieldByName('c_time_joblist_id').Value;
      xDaten^.JobTyp      := FieldByName('c_Job_type').Value;
      xDaten^.Execute_Typ := FieldByName('c_execute_type').Value;
//      FillChar(xDaten^.Data, SizeOf(xDaten^.Data), 0);
//      xDaten^.ID          := Query[cPrimaryQuery].FieldByName('c_time_joblist_id').Value;
//      xDaten^.JobTyp      := Query[cPrimaryQuery].FieldByName('c_Job_type').Value;
//      xDaten^.Execute_Typ := Query[cPrimaryQuery].FieldByName('c_execute_type').Value;

         // BlobField  ADO
      xStream := TMemoryStream.Create;
      try
        GetBlobFieldData('c_data', xStream);
        xStream.Position := 0;
        xStream.Read(xDaten^.Data, sizeof(TJobRec));
      finally
        xStream.Free;
      end;

      // add to Cache
      aCache.Add(xDaten);
      Next;
    end;
    Result := True;
  except
    on e:Exception do begin
      raise Exception.Create('TTimeJobDB.FillCache failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Insert a new job in the database
//******************************************************************************
function TTimeJobDB.InsertJob(aNewJob: TJobListRec): Boolean;
var
  xTimeMs: TTimeStamp;
begin
  Result := False;

  aNewJob.Data.JobTyp := aNewJob.JobTyp;
  with Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('INSERT INTO t_time_joblist ');
    SQL.Add(' (c_time_joblist_id, c_execute_type, c_time, c_week_day, ');
    SQL.Add('  c_month_day, c_month, c_priority, c_Job_type, c_data, c_last_execute_time ) ');

    SQL.Add('VALUES(:c_time_joblist_id, :c_execute_type, :c_time, :c_week_day, ');
    SQL.Add('       :c_month_day, :c_month, :c_priority, :c_Job_type,:c_data,:c_last_execute_time ) ');

    xTimeMs := DateTimeToTimeStamp(aNewJob.Time);

    Params.ParamByName('c_execute_type').AsInteger := ord(aNewJob.Execute_Typ);
    Params.ParamByName('c_time').AsInteger := xTimeMs.Time;
    Params.ParamByName('c_week_day').asString := aNewJob.WeekDay;
    Params.ParamByName('c_month_day').asString := aNewJob.MonthDay;
    Params.ParamByName('c_month').asString := aNewJob.Month;
    Params.ParamByName('c_priority').AsInteger := aNewJob.Priority;
    Params.ParamByName('c_Job_type').AsInteger := ord(aNewJob.JobTyp);
    Params.ParamByName('c_data').SetBlobData(@aNewJob.Data, sizeof(aNewJob.Data));
    Params.ParamByName('c_last_execute_time').asDateTime := aNewJob.Time;
    InsertSQL('t_time_joblist', 'c_time_joblist_id', MaxInt, 1);
    Result := True;
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TTimeJobDB.InsertJob failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Mutation a specific job from database (Insert, Update and Delete)
//******************************************************************************
procedure TTimeJobDB.JobMutation(aSQLText: string);
begin
  with Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add(aSQLText);
    ExecSQL;
  except
    on e: Exception do begin
      mError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TTimeJobDB.JobMutation failed: ' + e.message);
      mEventLog.Write(etError, FormatMMErrorText(mError));
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get current Jobs for the next minute and put the result in a TCache (TList)
//******************************************************************************
function TTimeJobDB.RequestData(var aCache: TCache; aTimeStamp: TTimeStamp): Boolean;
var
  xYear, xWeekDay0, xMonthDay0, xMonth0,
    xWeekDay1, xMonthDay1, xMonth1,
    xHour0, xMin0, xSec, xMSec,
    xHour1, xMin1: Word;
  xDateTime0, xDateTime1: TDateTime;
  xMsg: string;
begin
  try
    xMsg := '[Conversion]';
    fActTime := aTimeStamp;
    //Eventtime part 0 ( TickerTime )
    // zuerst wird eine sauberes Datum aus dem aTimeStamp erstellt, wo garantiert keine Sekunden
    // und Hunderstelsekunden drin sind...
    xDateTime0 := TimeStampToDateTime(aTimeStamp);
    DecodeDate(xDateTime0, xYear, xMonth0, xMonthDay0);
    DecodeTime(xDateTime0, xHour0, xMin0, xSec, xMSec);
    // Round time to Hour, Min, 0, 0
    xDateTime0 := EncodeTime(xHour0, xMin0, 0, 0) + EncodeDate(xYear, xMonth0, xMonthDay0);

    //Eventtime part 1 ( add 1 min. to TickerTime )
    //...nun wird eine zeit ermittelt, welche die n�chste Minute betrifft
    AddTime(1, xDateTime0, xDateTime1);
    //...anschliessend wird der Wochentag vom "sauberen Datum" ermittelt...
    xWeekDay0 := DayOfWeek(xDateTime0); // 1= Sunnday, 7 = Saturday
    xWeekDay1 := DayOfWeek(xDateTime1); // 1= Sunnday, 7 = Saturday

    DecodeDate(xDateTime1, xYear, xMonth1, xMonthDay1);
    DecodeTime(xDateTime1, xHour1, xMin1, xSec, xMSec);

    fEventTime := DateTimeToTimeStamp(xDateTime1);

    // SQL Time-Job FILTER
    xMsg := '[Query]';
    with Query[cPrimaryQuery] do begin
      Close;
      Params.Clear;
      SQL.Clear;

      SQL.Add('SELECT * ');
      SQL.Add('FROM t_time_joblist');

      if fEventTime.Time > 0 then
          //change 00:01 to 00:02
        SQL.Add('WHERE ( c_time > :c_time0 AND c_time <= :c_time1 ) ') //org
      else
          // change form 23:59 to 00:00
        SQL.Add('WHERE ( c_time > :c_time0 OR c_time = :c_time1 ) ');

        // Daily [0]
      SQL.Add('AND ( (    c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etDAILY))) + ' ');
      SQL.Add('     AND  c_month_day  = ''''  ');
      SQL.Add('     AND  c_week_day = ''''    ');
      SQL.Add('     AND  c_month  = '''' )    ');
        // Monthly [1]
      SQL.Add('OR (     c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etMONTHLY))) + ' ');
      SQL.Add('     AND c_month_day  Like :c_month_day0 OR c_month_day  Like :c_month_day1 ');
      SQL.Add('     AND c_week_day = ''''          ');
      SQL.Add('     AND c_month  = '''' )          ');
        // Weekly [2]
      SQL.Add('OR (     c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etWEEKLY))) + ' ');
      SQL.Add('     AND c_month_day  = ''''      ');
      SQL.Add('     AND c_week_day Like :c_week_day0 OR c_week_day Like :c_week_day1 ');
      SQL.Add('     AND c_month  = '''' )        ');
        // Once, Repeated, Now [3,4,5]
      SQL.Add('OR  ((   c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etONCE))) + ' ');
      SQL.Add('      OR c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etNOW))) + ' ');
      SQL.Add('      OR c_execute_type  = ' + inttostr(ord(TExecuteTypEnum(etREPEATED))) + ' ) ');
      SQL.Add('     AND c_month_day  Like  :c_month_day0 OR c_month_day Like  :c_month_day1 ');
      SQL.Add('     AND c_week_day = ''''           ');
      SQL.Add('     AND c_month  Like :c_month0 OR c_month Like  :c_month1 ) )');

      SQL.Add('ORDER BY c_priority  ASC ');


      ParamByName('c_time0').AsInteger := fActTime.Time;
      ParamByName('c_time1').AsInteger := fEventTime.Time;

      ParamByName('c_month_day0').asString := '%' + inttostr(xMonthDay0) + '%';
      ParamByName('c_week_day0').asString := '%' + inttostr(xWeekDay0) + '%';
      ParamByName('c_month0').asString := '%' + inttostr(xMonth0) + '%';

      ParamByName('c_month_day1').asString := '%' + inttostr(xMonthDay1) + '%';
      ParamByName('c_week_day1').asString := '%' + inttostr(xWeekDay1) + '%';
      ParamByName('c_month1').asString := '%' + inttostr(xMonth1) + '%';

      Open;
      aCache.Clear;
      FillCache(aCache, Query[cPrimaryQuery]);
      CheckLostEvents(aCache, fActTime);
      Result := True;
    end;
  except
    on e:Exception do begin
      raise Exception.Create(Format('TTimeJobDB.RequestData %s failed: ', [xMsg]) + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Get all Jobs after CPU-Time change (timechange forward)
//******************************************************************************
function TTimeJobDB.RequestEmergencyData(aTimeOld, aTimeNew: TTimeStamp; aQuery: TNativeAdoQuery): Boolean;
var
  xDateTime0, xDateTime1: TDateTime;
begin
  Result := False;
  xDateTime0 := TimeStampToDateTime(aTimeNew);
  //Eventtime part 1 ( add 1 min. to TickerTime )
  if not AddTime(1, xDateTime0, xDateTime1) then begin
    mEventLog.Write(etError, FormatMMErrorText(mError));
  end;

  fActTime   := aTimeNew;
  fEventTime := DateTimeToTimeStamp(xDateTime1);

  // SQL Time-Job FILTER
  with aQuery do
  try
    Close;
    Params.Clear;
    SQL.Clear;

    SQL.Add('SELECT * ');
    SQL.Add('FROM t_time_joblist');

    if aTimeOld.Time > 0 then
      //change 00:01 to 00:02
      SQL.Add('WHERE ( c_time > :c_timeOld AND c_time <= :c_timeNew ) ')
    else
      // change from 23:59 to 00:00
      SQL.Add('WHERE ( c_time > :c_timeOld OR c_time = :c_timeNew ) ');

    SQL.Add('ORDER BY c_priority  ASC ');

    Params.ParamByName('c_timeOld').AsInteger := aTimeOld.Time;
    Params.ParamByName('c_timeNew').AsInteger := aTimeNew.Time;
    Open;
    Result := True;
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TTimeJobDB.RequestEmergencyData failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TTimeJobDB.SetActTime(const Value: TTimeStamp);
begin
  fActTime := Value;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//  Update a specific job in the database
//******************************************************************************
{
procedure TTimeJobDB.UpdateJob(aId: Integer; aUpdatedJob: TJobListRec);
var
  xTimeMs: TTimeStamp;
begin
  with Query[cPrimaryQuery] do
  try
    Close;
    Params.Clear;
    SQL.Clear;
    SQL.Add('UPDATE t_time_joblist ');

    SQL.Add('SET c_execute_type = :c_execute_type, ');
    SQL.Add(' c_time        = :c_time, ');
    SQL.Add(' c_week_day    = :c_week_day, ');
    SQL.Add(' c_month_day   = :c_month_day, ');
    SQL.Add(' c_month       = :c_month, ');
    SQL.Add(' c_priority    = :c_priority, ');
    SQL.Add(' c_Job_type    = :c_Job_type, ');
    SQL.Add(' c_last_execute_time    = :Last_execute_time, ');
    SQL.Add(' c_data        = :c_data ');

    SQL.Add('WHERE c_time_joblist_id = :c_time_joblist_id ');

    xTimeMs := DateTimeToTimeStamp(aUpdatedJob.Time);
    Params.ParamByName('c_time_joblist_id').AsInteger := aId;
    Params.ParamByName('c_execute_type').AsInteger := ord(aUpdatedJob.Execute_Typ);
    Params.ParamByName('c_time').AsInteger := xTimeMs.Time;
    Params.ParamByName('c_week_day').asString := aUpdatedJob.WeekDay;
    Params.ParamByName('c_month_day').asString := aUpdatedJob.MonthDay;
    Params.ParamByName('c_month').asString := aUpdatedJob.Month;
    Params.ParamByName('c_priority').AsInteger := aUpdatedJob.Priority;
    Params.ParamByName('c_Job_type').AsInteger := ord(aUpdatedJob.JobTyp);
    Params.ParamByName('c_data').SetBlobData(@aUpdatedJob.Data, sizeof(aUpdatedJob.Data));
    Params.ParamByName('Last_execute_time').asDateTime := aUpdatedJob.Time;
    ExecSQL;
  except
    on e:Exception do begin
      mEventLog.Write(etError, 'TTimeJobDB.UpdateJob failed: ' + e.Message);
      raise;
    end;
  end;
end;
{}
//------------------------------------------------------------------------------

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++ TCache
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//******************************************************************************
// Clear TmmList Items
//******************************************************************************
procedure TCache.Clear;
begin
  while Count > 0 do begin
    Dispose(Items[0]);
    Delete(0);
  end;
  inherited Clear;
end;
//------------------------------------------------------------------------------
function TCache.GetItems(aIndex: Integer): PCacheRec;
begin
  Result := inherited Items[aIndex];
end;
//------------------------------------------------------------------------------

end.

