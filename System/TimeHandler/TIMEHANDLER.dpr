program TIMEHANDLER;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  MemCheck,
  Forms,
  LoepfeGlobal,
  BaseGlobal,
  SysUtils,
  BaseMain,
  ActiveX,
  Windows,
  u_TimeTicker in 'u_TimeTicker.pas',
  u_BaseJobs in 'u_BaseJobs.pas',
  u_TimeJobDB in 'u_TimeJobDB.pas',
  u_CleanUpEvAdmin in 'u_CleanUpEvAdmin.pas',
  u_DataEventAdmin in 'u_DataEventAdmin.pas',
  u_JobDispatch in 'u_JobDispatch.pas',
  u_JobExecute in 'u_JobExecute.pas',
  u_BaseAdmin in 'u_BaseAdmin.pas',
  u_TimeHandler in 'u_TimeHandler.pas',
  u_DelIntAdmin in 'u_DelIntAdmin.pas',
  u_DelShiftByTimeAdmin in 'u_DelShiftByTimeAdmin.pas',
  u_AppendDefaultShifts in 'u_AppendDefaultShifts.pas';

var
  xTimeHandler: TTimeHandler;
  xResult: HResult;
{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('TimeHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    xTimeHandler := TTimeHandler.Create(ssTimeHandler);
    if xTimeHandler.Initialize then
      xTimeHandler.Run;
    xTimeHandler.Free;
  finally
    CoUninitialize;
  end;
end.
