//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: WSChandler.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 05.10.2002        LOK | Umbau ADO
//=========================================================================================*)


program WSCHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  MemCheck,
  LoepfeGlobal,
  BaseMain,
  BaseGlobal,
  Windows,
  ActiveX,
  SysUtils,
  AC338DLL_DEF in 'AC338DLL_DEF.pas',
  AC338INFClass in 'AC338INFClass.pas',
  AC338MachThreadClass in 'AC338MachThreadClass.pas',
  WSCHandlerClass in 'WSCHandlerClass.pas',
  WSCWriterClass in 'WSCWriterClass.pas',
  CPMMSSDLL_DEF in 'CPMMSSDLL_DEF.pas';

// AC338ControlClass in 'AC338ControlClass.pas' // Nur fuer Lifecheck mittels Ping;

var
  xWSCHandler: TWSCHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('WSCHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    xWSCHandler := TWSCHandler.Create(ssWSCHandler);
    if xWSCHandler.Initialize then
      xWSCHandler.Run;
    xWSCHandler.Free;
  finally
    CoUninitialize;
  end;
end.

