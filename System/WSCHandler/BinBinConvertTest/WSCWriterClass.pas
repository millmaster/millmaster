//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: WSCWriterClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 06.06.2000  0.00  khp | GetSettingsAllGroups: wurde entfernt
//=========================================================================================*)



unit WSCWriterClass;

interface

uses windows, Classes, SysUtils,
  BaseThread, BaseGlobal, mmEventLog, LoepfeGlobal, IPCClass, AC338MachThreadClass;

type
  TWSCWriter = class(TBaseSubThread)
  PROTECTED
    mCSWriteToMsgHandler: TCriticalSection;
    mNodeList: tNodeList;
    procedure ProcessJob; OVERRIDE;
    procedure ProcessInitJob; OVERRIDE;
  PUBLIC
    procedure WriteToLog(aEvent: TEventType; aText: string);
    procedure WriteToMsgHandler(aPJob: PJobrec = nil);
    procedure SetMachstatus(aMachID: Integer; aMachstatus: TWscNodeState);
    constructor Create(aThreadDef: TThreadDef); OVERRIDE;
    function DoConnect: Boolean; OVERRIDE;
    function Init: Boolean; OVERRIDE;
    destructor Destroy; OVERRIDE;
  end;

  //=============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf;                             // CodeSite

constructor TWSCWriter.Create(aThreadDef: TThreadDef);
begin
  inherited Create(aThreadDef);
  mCSWriteToMsgHandler := TCriticalSection.Create;
end;

//-----------------------------------------------------------------------------

destructor TWSCWriter.Destroy;
begin
  mNodeList.StopMachinethread;          // Stop aller MaschThreads
  sleep(100);                          // Warten bis alle Maschinen Threads beendet sind
  mNodeList.free;
  mCSWriteToMsgHandler.Free;
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TWSCWriter.DoConnect: Boolean;
begin
  result := inherited DoConnect;
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.WriteToMsgHandler(aPJob: PJobrec);
begin
  mCSWriteToMsgHandler.EnterCriticSection;
  if Assigned(aPJob) then
    aPJob^.NetTyp := ntWSC
  else
    mJob.NetTyp := ntWSC;
  WriteJobBufferTo(ttMsgDispatcher, aPJob);
  mCSWriteToMsgHandler.LeaveCriticSection;
  sleep(1);                             // Threadwechsel erzwingen
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.WriteToLog(aEvent: TEventType; aText: string);
begin
  writelog(aEvent, aText);
end;

//-----------------------------------------------------------------------------

procedure TWSCWriter.SetMachstatus(aMachID: Integer; aMachstatus: TWscNodeState);
begin
  mNodeList.SetMachstate(nil, aMachID, aMachstatus);
end;

//-----------------------------------------------------------------------------

function TWSCWriter.Init: Boolean;
begin
  Result := inherited Init;
  if Result then begin
    mConfirmStartupImmediately := False;
    try
      mNodeList := tNodelist.create;
    except
      Result := FALSE;
    end;
  end;
end;


//-----------------------------------------------------------------------------

procedure TWSCWriter.ProcessJob;

  procedure TreatJobtoMaThread(aMachID: Integer);
    // Job wird dem betreffenden Maschinen Thread ueber die MsgQueue uebermittelt.
    // Falls Maschine OffLine oder MaschID unbekannt: Meldung an MsgHandler.
  var
    xMachthread: tInformthread;
  begin
    xMachThread := mNodeList.GetMachinethread(nil, aMachID);
    if mNodelist.GetMachstate(nil, aMachID) = nsOnline then
      if xMachThread <> nil then
        xMachThread.SendJobtoMaThread(mjob)
      else begin                        // Unknow MachNo
        fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'Unknow MachNo');
        mJob.JobResult.JobTyp := mJob.JobTyp;
        mJob.JobTyp := jtJobResult;
        mJob.JobResult.Error := Error;
        WriteToMsgHandler;
      end
    else begin                          // Machine OFFLINE
      fError := SetError(ERROR_NOT_READY, etMMError, 'Machine OFFLINE');
      mJob.JobResult.JobTyp := mJob.JobTyp;
      mJob.JobTyp := jtJobResult;
      mJob.JobResult.Error := Error;
      WriteToMsgHandler;
    end;
  end;

begin
  // This methode is implemented only in derived classes and it is called from
  // the Execute methode of TBaseThread.
  case mJob.JobTyp of
    jtGetNodeList: begin
        // Lesen der mNodeList im WscHandler
        mNodeList.GetNodeList(mJob.GetNodeList.NodeItems.WscNetNodeList);
        WriteToMsgHandler;              // NodeList an MsgHandler schicken
      end;
    jtGetMaAssign: begin
        // Get machine Assignment
        TreatJobtoMaThread(mJob.GetMaAssign.MachineID);
      end;
    jtGetMaConfig: begin
        // Get machine Configuration
        TreatJobtoMaThread(mJob.GetMaConfig.MaID);
      end;
    jtSetMaConfig: begin
        // Set machine Configuration
        TreatJobtoMaThread(mJob.SetMaConfig.MachineID);
      end;
    jtClearZESpdData: begin
        // Loescht alle Spindeldaten auf Informator
        TreatJobtoMaThread(mJob.SetMaConfig.MachineID);
      end;
    jtDelZESpdData: begin
        // Acknowledge fuer Spindel Request)
        TreatJobtoMaThread(mJob.GetZESpdData.MachineID);
      end;
    jtViewZESpdData: begin
        // regular spindle data aquisition at intervall end
        TreatJobtoMaThread(mJob.GetZESpdData.MachineID)
      end;
    jtGetZESpdData: begin
        // regular spindle data aquisition at intervall end
        TreatJobtoMaThread(mJob.GetZESpdData.MachineID);
      end;
    jtGetSettings: begin
        // Get settings from prodgroup
        TreatJobtoMaThread(mJob.GetZESpdData.MachineID);
      end;
    {     jtGetSettingsAllGroups: begin
            // Get settings from prodgroup
              TreatJobtoMaThread(mJob.GetSettingsAllGroups.MaID);
            end;
    }
    jtSetSettings: begin
        // Send settings of a prodgroup to ZE
        TreatJobtoMaThread(mJob.GetZESpdData.MachineID);
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;
  Sleep(10);
end;                                    {TWSCWriter.ProcessJob}

//-----------------------------------------------------------------------------

procedure TWSCWriter.ProcessInitJob;

begin
  case mJob.JobTyp of
    jtSetNodeList: begin
        writelog(etInformation, 'SetNodeList received');
        // Load mNodeList to the NetHandler
        mNodeList.SetNodeList(mJob.SetNodeList.NodeItems.WscNetNodeList);
        // Start aller benoetigten MaschThreads
        mNodeList.StartMachineThread(WriteToMsgHandler, SetMachstatus, WriteToLog);
        sleep(5000);                    // Prov!! Warten bis alle Threads Online in der NodeListe nachgefuehrt haben
        // Erhalt der NodeListe bestaetigen
        WriteToMsgHandler;
        // Alle andern Thread freigeben (ControlThread, ReadThread)
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d',
            [IPCClientIndex[cMainChannelIndex]^.Error]));
          writelog(etError, 'Init confirm failed');
          Exit;
        end;
      end;
  else
    ProcessJob;
  end;
end;                                    {TWSCWriter.ProcessInitJob}
end.

