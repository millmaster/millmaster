(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: CPMMSSDLL_DEF.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: DEfinition Unit fuer CPMMSSDLL_DEF.dll
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 19.06.2000  0.00  khp | Datei erstellt
| 13.09.2000  1.00  khp | Anpassung an first Release CPMMSS.DLL
| 01.11.2000  1.00  khp | Imported function for set_group_parameter changed to "SetGroupParameter"
| 10.12.2001  1.01  khp | CPMMSS.dll wird dynamisch zur Laufzeit geladen und muss daher nicht mehr
|                       | zwingend vorhanden sein. Es wird eine Meldung ausgegeben wenn die Dll nicht vorhanden ist.
| 19.05.2005  1.01  khp | Funktion Getcpmmss_Errortext f�r detailierte Fehlermeldung der CPMMSS DLL eingef�gt.
|==========================================================================================*)

// Konstanten, Datentypen, Functionen und Proceduren fuer den Zugriff
// auf die CPMMSSDLL_DEF.dll


unit CPMMSSDLL_DEF;

interface
uses Windows, Classes, Sysutils,
  BaseGlobal, MMEventlog, LoepfeGlobal;

//******************************************************************************
// * Const-Deklarationen
//******************************************************************************




//******************************************************************************
// * Type-Deklarationen und Type-Definitionen
//******************************************************************************

{$MINENUMSIZE 4}                        // Achtung alle Enum Types 4 Byte 32 Bit
{========================================================}

type
  PHostname = PChar;                    // Zeiger auf Rechnername
  PTextstring = PChar;                  // Zeiger auf Buffer fuer Textstrings


  tYarncounttype =
    (D_NM,                              // 0
    D_TEX,                              // 1
    D_NE,                               // 2
    D_ECC,                              // 3
    D_EWC,                              // 4
    D_EWS,                              // 5
    D_RUN,                              // 6
    D_CUT,                              // 7
    D_NCA);                             // 8

  cpmmss_result =
    (CPMMSS_OK,                         // 0,	 Funktion einwandfrei ausgefuehrt
    CPMMSS_ERROR_MUTEX,                 // 1,	 Wartezeit auf Mutex abgelaufen
    CPMMSS_ERROR_INIT,                  // 2,	 Verbindung zur Informatordatenbank gescheitert
    CPMMSS_ERROR_HANDLE,                // 3,	 Datum in der Informatordatenbank nicht bekannt
    CPMMSS_ERROR_PUT,                   // 4,	 Wertebereich nicht eingehalten
    CPMMSS_CP_THERE,                    // 5,	 Conerpilot uebernimmt schreiben in die Informatordatenbank
    CPMMSS_CP_NOTTHERE);                // 6,	 CpMmSs     uebernimmt schreiben in die Informatordatenbank



  // Conerpilot Zugriffsfunktionen auf WSC Database.
  // Achtung alle Aufrufe mit cdecl, mit stdcall probleme bei verschachtelten Exception


  {
    Achtung bei der Verwendung von Funktionen der  WSC-DLL !!!
    Bei jedem Aufruf wird ein Connect/Disconnet der WSC Datenbank ausgefuehrt.
    Dieser Zyklus kann bis zu 3 Sekunden dauern.
    Daher ist der Einsatz der Funktion SetMMPartieparameter am effektivsten.
  }



function set_partiename(const hostname: PHostname;
  const GrpNo: Integer;
  const Text: PTextstring): cpmmss_result;

function set_partienummer(const hostname: PHostname;
  const GrpNo: Integer;
  const Text: PTextstring): cpmmss_result;

function set_partiebereich(const hostname: PHostname;
  const GrpNo: Integer;
  const firstSpd, lastSpd: Integer): cpmmss_result;

function set_garnfeinheit(const hostname: PHostname;
  const GrpNo: Integer;
  const garnfeinheit: single): cpmmss_result;


function set_fadenzahl(const hostname: PHostname;
  const GrpNo: Integer;
  const fadenzahl: Integer): cpmmss_result;


function set_dim_garnfeinheit(const hostname: PHostname;
  const GrpNo: Integer;
  const Dimension: integer): cpmmss_result;


function set_group_parameter(const hostname: PHostname;
  const GrpNo: Integer;
  const Partiename: PTextstring;
  const Partienummer: PTextstring;
  const firstSpd, lastSpd: Integer;
  const garnfeinheit: single;
  const fadenzahl: Integer): cpmmss_result;

function Getcpmmss_Errortext(aRes:cpmmss_result): String;


{  Externe Funktionen fuer das statische Laden der CPMMSS.DLL

function set_partiename(const hostname: PHostname;
  const GrpNo: Integer;
  const Text: PTextstring): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetPartiename';

function set_partienummer(const hostname: PHostname;
  const GrpNo: Integer;
  const Text: PTextstring): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetPartienummer';

function set_partiebereich(const hostname: PHostname;
  const GrpNo: Integer;
  const firstSpd, lastSpd: Integer): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetPartiebereich';

function set_garnfeinheit(const hostname: PHostname;
  const GrpNo: Integer;
  const garnfeinheit: single): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetGarnfeinheit';


function set_fadenzahl(const hostname: PHostname;
  const GrpNo: Integer;
  const fadenzahl: Integer): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetFadenzahl';


function set_dim_garnfeinheit(const hostname: PHostname;
  const GrpNo: Integer;
  const Dimension: integer): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetDimGarnfeinheit';


function set_group_parameter(const hostname: PHostname;
  const GrpNo: Integer;
  const Partiename: PTextstring;
  const Partienummer: PTextstring;
  const firstSpd, lastSpd: Integer;
  const garnfeinheit: single;
  const fadenzahl: Integer): cpmmss_result; cdecl; external 'cpmmss.dll' name 'SetGroupParameter';
}

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

type
  tset_partiename = function (const hostname: PHostname;
                              const GrpNo: Integer;
                              const Text: PTextstring): cpmmss_result; cdecl;

  tset_partienummer = function (const hostname: PHostname;
                                const GrpNo: Integer;
                                const Text: PTextstring): cpmmss_result; cdecl;

  tset_partiebereich = function (const hostname: PHostname;
                                 const GrpNo: Integer;
                                 const firstSpd, lastSpd: Integer): cpmmss_result; cdecl;

  tset_garnfeinheit = function (const hostname: PHostname;
                                const GrpNo: Integer;
                                const garnfeinheit: single): cpmmss_result; cdecl;


  tset_fadenzahl = function (const hostname: PHostname;
                             const GrpNo: Integer;
                             const fadenzahl: Integer): cpmmss_result; cdecl;


  tset_dim_garnfeinheit = function (const hostname: PHostname;
                                    const GrpNo: Integer;
                                    const Dimension: integer): cpmmss_result; cdecl;


  tset_group_parameter = function (const hostname: PHostname;
                                   const GrpNo: Integer;
                                   const Partiename: PTextstring;
                                   const Partienummer: PTextstring;
                                   const firstSpd, lastSpd: Integer;
                                   const garnfeinheit: single;
                                   const fadenzahl: Integer): cpmmss_result; cdecl;


var
  hCPMMSSDLL: THandle;
  gSet_partiename: tset_partiename;
  gSet_partienummer: tset_partienummer;
  gSet_partiebereich: tset_partiebereich;
  gSet_garnfeinheit: tset_garnfeinheit;
  gSet_fadenzahl: tset_fadenzahl;
  gSet_dim_garnfeinheit: tset_dim_garnfeinheit;
  gSet_group_parameter: tset_group_parameter;

function Getcpmmss_Errortext(aRes: cpmmss_result): String;
begin
  case aRes of
    CPMMSS_ERROR_MUTEX: Result:= 'mutex expired.';                      // Wartezeit auf Mutex abgelaufen
    CPMMSS_ERROR_INIT:  Result:= 'connection to Informator DB failt.';  // Verbindung zur Informatordatenbank gescheitert
    CPMMSS_ERROR_HANDLE: Result:= 'unknown data item.';                 // Datum in der Informatordatenbank nicht bekannt
    CPMMSS_ERROR_PUT: Result:= 'range overflow.';                       // Wertebereich nicht eingehalten
  else
    Result:= 'unknown errror';
  end;
end;

function set_partiename(const hostname: PHostname;
                        const GrpNo: Integer;
                        const Text: PTextstring): cpmmss_result;
begin
  if Assigned(gSet_partiename)then
    result:= gSet_partiename( hostname, GrpNo, Text)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_partienummer(const hostname: PHostname;
                          const GrpNo: Integer;
                          const Text: PTextstring): cpmmss_result;
begin
  if Assigned(gSet_partienummer)then
    result:= gSet_partienummer(hostname,GrpNo,Text)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_partiebereich(const hostname: PHostname;
                           const GrpNo: Integer;
                           const firstSpd, lastSpd: Integer): cpmmss_result;
begin
  if Assigned(gSet_partiebereich)then
    result:= gSet_partiebereich(hostname,GrpNo,firstSpd,lastSpd)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_garnfeinheit(const hostname: PHostname;
                          const GrpNo: Integer;
                          const garnfeinheit: single): cpmmss_result;
begin
  if Assigned(gSet_garnfeinheit)then
    result:= gSet_garnfeinheit(hostname,GrpNo,garnfeinheit)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_fadenzahl(const hostname: PHostname;
                       const GrpNo: Integer;
                       const fadenzahl: Integer): cpmmss_result;
begin
  if Assigned(gSet_fadenzahl)then
    result:= gSet_fadenzahl(hostname,GrpNo,fadenzahl)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_dim_garnfeinheit(const hostname: PHostname;
                              const GrpNo: Integer;
                              const Dimension: integer): cpmmss_result;
begin
  if Assigned(gSet_dim_garnfeinheit)then
    result:= set_dim_garnfeinheit(hostname,GrpNo,Dimension)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

function set_group_parameter(const hostname: PHostname;
                             const GrpNo: Integer;
                             const Partiename: PTextstring;
                             const Partienummer: PTextstring;
                             const firstSpd, lastSpd: Integer;
                             const garnfeinheit: single;
                             const fadenzahl: Integer): cpmmss_result;
begin
  if Assigned(gSet_group_parameter)then
    result:= gSet_group_parameter(hostname,GrpNo,Partiename,Partienummer,firstSpd,lastSpd,garnfeinheit,fadenzahl)
  else
    result:= CPMMSS_CP_NOTTHERE;
end;

initialization
 
  hCPMMSSDLL := LoadLibrary('cpmmss.dll');
  if hCPMMSSDLL<> 0 then begin
    @gSet_partiename:= GetProcAddress(hCPMMSSDLL,'SetPartiename');
    @gSet_partienummer:= GetProcAddress(hCPMMSSDLL,'SetPartienummer');
    @gSet_partiebereich:= GetProcAddress(hCPMMSSDLL,'SetPartiebereich');
    @gSet_garnfeinheit:= GetProcAddress(hCPMMSSDLL,'SetGarnfeinheit');
    @gSet_fadenzahl:= GetProcAddress(hCPMMSSDLL,'SetFadenzahl');
    @gSet_dim_garnfeinheit:= GetProcAddress(hCPMMSSDLL,'SetDimGarnfeinheit');
    @gSet_group_parameter:= GetProcAddress(hCPMMSSDLL,'SetGroupParameter');
  end else
    writeToEventLog('Library CPMMSS.DLL not found, probable Conerpilot is installed', '', etWarning, '', '', ssWSCHandler);
finalization
  if hCPMMSSDLL<> 0 then begin
    FreeLibrary(hCPMMSSDLL);
    gSet_partiename:= nil;
    gSet_partienummer:= nil;
    gSet_partiebereich:= nil;
    gSet_garnfeinheit:= nil;
    gSet_fadenzahl:= nil;
    gSet_dim_garnfeinheit:= nil;
    gSet_group_parameter:= nil;
  end;
end.

