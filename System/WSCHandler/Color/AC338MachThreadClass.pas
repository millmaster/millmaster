//==========================================================================================
// Project.......: L O E P F E 'S   M I L L M A S T E R
// Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
//-------------------------------------------------------------------------------------------
// Filename......: AC338MachThreadClass.pas
// Projectpart...: MillMaster NT Spulerei
// Subpart.......: -
// Process(es)...: -
// Description...:
// Info..........: -
// Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0  SP6
// Target.system.: Windows NT
// Compiler/Tools: Delphi 5.01
//------------------------------------------------------------------------------------------
// History:
// Date        Vers. Vis.| Reason
//------------------------------------------------------------------------------------------
// 15.03.2000  0.00  khp | Datei erstellt
// 28.03.2000  0.00  khp | Test basedata.length:= 65000; basedata.watchTime:= 1000; entfernt am 10.4 khp
// 06.04.2000  0.00  khp | @1 ProdGrpId immer aus Grp-Settings, nie aus Deklarationsrecord lesen
// 10.04.2000  0.00  khp | Funktionen ConvertSettingsFromAC338 und  ConvertSettingsToAC338 fuer
//                       | Anpassungen an den Settingsdaten eingefuegt und  ProdGrpName behandelt.
//                       | Betroffene Proceduren: Get- SetSettings,Set- GetMaConfig, GetSettingsAllroup,
//                       | Inform: Grp_Settings Declarations eStart, eStop
// 11.04.2000  0.00  khp | Allignment-Problematik (C, DLL) mit xUSBuffer temporaer ueberbrueckt.
// 12.04.2000  0.00  nue | KorrekturHack temporaer!!!!!!!!!!.
// 18.04.2000  0.00  nue | Anpassungen an die Jobstrukturen wegen Einfuegen von ProdGrpInfo!.
// 04.05.2000  0.00  khp | Hack entfernt,Allignment-Problematik in Get_Data/Put_Data geloest,
//                       | Methoden ExtractProdInfo, BuildGrpNoString fuer Order, Orderpos eingefuegt
// 25.05.2000  0.00  khp | Temporaer: GrpBereich und GrpStatus in GetMaAsign von WSC DB lesen
// 06.06.2000  0.00  khp | GetSettingsAllGroups: wurde entfernt
// 13.06.2000  0.00  khp | ConnectingMachine:Falls DataTransaction aktiv, dann abbrechen, Rollbacklaeuft
// 16.06.2000  0.00  khp | Assign Complete nur wenn in einer MaschGrp ProdId=0
// 25.06.2000  0.00  khp | Keine Zugriffe auf WSC Informator Datenbank. Mittels SetMMPartieName,SetMMPartieBereich,
//                       | SetMMPartieNummer wird auf PARTIENAME, PARTIENUMMER, PARTIEBEREICH zugeriffen.
//                       | Schlupf
// 26.06.2000  0.00  khp | ExtractandFillProdInfo und BuildPartieNoString ersetzen ExtractProdInfo, BuildGrpNoString
// 28.06.2000  0.00  khp | DelDecl wird nur nach EntryLocked und in GetMaAssign aufgerufen.
//                       | Test ob Aenderung erfolgten waehrend Offline zwischen zwei Lifechecks
//                       | ist nicht mehr noetig, Abgleich erfolgt bei KeyLocked durch MaAssign.
//                       | Schlupf darf nicht von WSC gelesen werden daher Schlupf in PartieNr einbinden. Der Wertebereich
//                       | wird geprueft,Kommas werden in Punkte konvertiert,bei Fehler wird Defaultschlupf 1.000 gesetzt.
//                       | Syntax PartieNr: MM-OrderID-Orderposition-Schlupf  MM-1-1-1.007
// 08.08.2000  0.00  khp | Timer (cMachstatusTicker) fuer Machstatus eingefuehrt, welcher den Status alle 60 sekunden sendet
// 12.09.2001  1.00  khp | Informator V5.3x: ExtractandFillProdInfo: MM_PARTIENAME(tText100) wird auf tText50 gekuerzt und
//                       | aProdGrpInfo.c_prod_name zugewiesen. Bei jtSetMaConfig,SetSettings umcopieren von tText50 auf tText100
// 14.09.2001  1.01  khp | Behandlung von YMSetParameter in GetSettings,eStart,ePreSettings mit proc. GetYMSetParameter
//                       | jtSetSettings mit proc. SetYMSetParmeter. Exceptions werden gehandelt und mit CodeSide dokumentiert
// 12.12.2001  1.02  khp | Mittels SetMMPartieandArticleparam und GetMMPartieandArticleparam werden die Artikel und Partiewerte
//                       | auf den Informator gespeichert. PartieNr hat nur noch informativen Charakter 
// 31.01.2002  1.03  khp | Bei Veraenderungen an einer laufenden Partei muss bei eEntryLocked YM_set_name mit GetYMSetParm geholt werden
// 28.02.2002  1.04  khp | jtSetSettings,jtSetMaConfig Garnummer fuer 338 DB immer auf Nm-float konvertieren
// 15.03.2002  1.05  khp | Anpassung von Partiename auf DB auf TText100 (Chinesisch UTF8)
// 29.05.2002  1.06  khp | jtSetSettings:SetWSCPartieNummer wird nur ausgefuehrt wenn Gruppe nicht inProduktion ist 
//=========================================================================================*)


unit AC338MachThreadClass;

interface

uses
  Windows, Classes, Dialogs, Messages, Forms, Sysutils, extctrls,
  AC338INFClass, AC338DLL_DEF, BaseGlobal, YMDataDef, YMParaUtils, YMParaDef, MMUGlobal,
  BaseThread, mmEventLog;

const
  cMMJobRequestMsg = 'MMJobRequestMsg';
  cAcqTimerID = 100;
  cLifeCheckTimerID = 101;
  cMachstatusTickerID = 102;
  cAcqTimeout = 60000;                  // ms Timeout beim Acquisation
  cLifeCheckTimeout = 30000;
  cMachstatusTickertime = 60000;
  cPartieNoHeaderStr = 'MM';            //
  cPartieNoDelimiterStr = '-';
  cStringListDelimiter = ',';
  cComma = ',';
  cDot = '.';




type
  TWSCDaten = packed record
    baseData: TBaseData;
    klassierdaten: packed record
      defectData: TWSCDefectData;
      siroData: TWSCSiroData;
      siroDataBright: TWSCSiroData;
    end;
  end;
type
  TWriteToMsgHnd = procedure(aPJob: PJobrec) of object;
  TSetMachstatus = procedure(aMachID: Integer; aMachstatus: TWscNodeState) of object;
  TWriteToLog = procedure(aEvent: TEventType; aText: string) of object;

  tInformthread = class(TThread)
  PRIVATE
    mJobRequestMsg: UINT;
    mWSCInformMsg: Integer;
    mWriteToMsgHnd: TWriteToMsgHnd;     // Methoden Referenz zu WriteJobBufferTo in WSCWriterClass
    mSetMachstatus: TSetMachstatus;     // Methoden Referenz zu SetMachstate in WSCWriterClass
    mThreadIpAdresse: PChar;
    mMaschID: Integer;
    mWSCHandler: tWscDevHandler;
    mWriteToLog: TWriteToLog;           // Methoden Referenz zu WriteLog in WSCWriterClass
    procedure SetMMPartieandArticleparam(aProdGrpInfo: TProdGrpInfo; aGrpNo: Integer; aColor: integer);
    procedure GetMMPartieandArticleparam(var aProdGrpInfo: TProdGrpInfo; aGrpNo: Integer; var aColor: integer);
    procedure SetYMSetParm(aYM_Set_Name: tText50; aYM_set_id: Word; aProdGrp: Integer);
    procedure GetYMSetParm(var aYM_Set_Name: tText50; var aYM_set_id: Word;
      var aYM_Set_Changed: Boolean; aProdGrp: Integer);
  PROTECTED
    function ConnectingMachine(): Boolean;
    function DisConnectingMachine(): Boolean;
    function SendMachinenstatus(): Boolean;
    procedure CleanDataAcqQueue();
    procedure InformDispatcher(aMsg: TMsg);
    procedure MMJobDispatcher(aMsg: TMsg);
    procedure Execute; OVERRIDE;
  PUBLIC
    mThreadWndHandle: HWND;             // Window Handle fuer Thread
    constructor Create(const aIpAdresse: PChar; const aMaschNo: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
      const aSetMachstatus: TSetMachstatus; const aWriteToLog: TWriteToLog);
    destructor Destroy; OVERRIDE;
    function SendJobtoMaThread(var ajobrecord: tJobRec): Boolean;
  end;

  tNodelisttype = record
    MaschineThread: tInformThread;
    WscNetNodeRec: TWscNetNodeRec;
  end;


  tNodeList = class(TObject)
  PRIVATE
    fLastMachpos: integer;
    fNodeList: array[cFirstMachPos..cMaxWscMachine] of tNodelisttype;
  PROTECTED
  PUBLIC
    property LastMachpos: Integer READ fLastMachpos;
    procedure SetNodeList(const aNodeList: TWscNetNodeList);
    procedure GetNodeList(var aNodeList: TWscNetNodeList);
    function GetMachstate(aIpdress: Pchar; aMaschNo: Integer): TWscNodeState;
    procedure SetMachstate(aIpdress: Pchar; aMaschNo: Integer; aMachstatus: TWscNodeState);
    procedure StartMachinethread(aWriteToMsgHnd: TWriteToMsgHnd; aSetMachstatus: TSetMachstatus;
                                 aWriteToLog: TWriteToLog);
    function GetMachinethread(aIpdress: Pchar; aMaschNo: Integer): tInformThread;
    procedure StopMachinethread();
  end;




implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,                             // Unit fuer CodeSite
  IPCClass;
type
  LifecheckEx = class(Exception);
  WSCAccessException = class(Exception);
//==============================================================================

// CLASS Informthread

//==============================================================================

constructor tInformthread.Create(const aIpAdresse: PChar; const aMaschNo: Integer; const aWriteToMsgHnd: TWriteToMsgHnd;
  const aSetMachstatus: TSetMachstatus; const aWriteToLog: TWriteToLog);
begin
  mSetMachstatus := aSetMachstatus;
  mWriteToMsgHnd := aWriteToMsgHnd;
  mThreadIpAdresse := aIpAdresse;
  mMaschID := aMaschNo;
  mWriteToLog := aWriteToLog;
  inherited Create(False);
end;

//------------------------------------------------------------------------------

destructor tInformthread.Destroy;
begin
  // Free my own window handle to receive windows messages
  if assigned(mWSCHandler) then mWSCHandler.Free;
  DeAllocateHWnd(mThreadWndHandle);
  inherited Destroy;
end;

//------------------------------------------------------------------------------

function tInformthread.SendJobtoMaThread(var ajobrecord: tjobRec): Boolean;
var
  xp: PJobRec;
begin
  result := true;
  xp := AllocMem(ajobrecord.JobLen);
  move(ajobrecord, xp^, ajobrecord.JobLen);
  PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(xp), 0);
end;

//------------------------------------------------------------------------------

procedure tInformthread.SetYMSetParm(aYM_Set_Name: tText50; aYM_set_id: Word; aProdGrp: Integer);
var
  xConvVal: Dword;
begin
  try
    with mWSCHandler do begin
      xConvVal := aYM_set_id;
      PutData(PChar(YMSET_NAME), @aYM_Set_Name, sizeof(aYM_Set_Name), aProdGrp, 0);
      PutData(PChar(YMSET_ID), @xConvVal, sizeof(xConvVal), aProdGrp, 0);
    end;
  except
    on E: Exception do
      CodeSite.SendWarning('SetYMSetParmeter ' + e.message)
  else
    CodeSite.SendError('SetYMSetParmeter failt with unknown error');
  end;
end;

//------------------------------------------------------------------------------

procedure tInformthread.GetYMSetParm(var aYM_Set_Name: tText50; var aYM_set_id: Word;
  var aYM_Set_Changed: Boolean; aProdGrp: Integer);
var
  xConvVal: DWord;
  xIndex: Integer;
begin
  FillChar(aYM_Set_Name, sizeof(aYM_Set_Name), 0);
  aYM_set_id := 0;
  aYM_Set_Changed := false;
  try
    // YMSET Parameter holen
    with mWSCHandler do begin
      xIndex := Getindex(PChar(YMSET_NAME));
      GetData(xIndex, @aYM_Set_Name, Sizeof(aYM_Set_Name), aProdGrp, 0);
      xIndex := Getindex(PChar(YMSET_CHANGED));
      GetData(xIndex, @xConvVal, Sizeof(xConvVal), aProdGrp, 0);
      aYM_Set_Changed := xConvVal <> 0;
      xIndex := Getindex(PChar(YMSET_ID));
      GetData(xIndex, @xConvVal, Sizeof(xConvVal), aProdGrp, 0);
      if xConvVal <= HIGH(Word) then
        aYM_set_id := xConvVal
      else
        aYM_set_id := 0;
      CodeSite.SendFmtMsg('GetYMSetParm: YM_Set_Changed=%d,YM_Set_Name=%s',
                         [integer(aYM_Set_Changed), String(aYM_Set_Name)]);
    end;
  except
    on E: Exception do
      CodeSite.SendWarning('GetYMSetParmeter ' + e.message)
  else
    CodeSite.SendError('GetYMSetParmeter failt with unknown error');
  end;
end;

//..............................................................................

procedure  tInformthread.SetMMPartieandArticleparam(aProdGrpInfo: TProdGrpInfo; aGrpNo: Integer; aColor: integer);
const
 str60= 60;
 str40= 40;
var
  xString: string;
  xText: TText100;
  //xText60: TText60;
begin
  try
    with mWSCHandler do begin
      // Partiename auf Informator speichern
      Fillchar(xText, sizeof(xText), 0);
      strcopy(@xText, @aProdGrpInfo.c_prod_name);
      PutData(PChar(MM_PARTIENAME), @xText, str60, aGrpNo, 0);
      // Partienummer setzen mit 'MM'
      Fillchar(xText, sizeof(xText), 0);
      xString:= cPartieNoHeaderStr;
      StrPCopy(@xText, xString);
      PutData(PChar(MM_PARTIENUMMER), @xText, str60, aGrpNo, 0);
      // Artikel mit StyleID,OrderID,OrderPosID abfuellen
      Fillchar(xText, sizeof(xText), 0);
      xString := inttostr(aProdGrpInfo.c_style_id) +cStringListDelimiter +
                 inttostr(aProdGrpInfo.c_order_id) +cStringListDelimiter +
                 inttostr(aProdGrpInfo.c_order_position_id);
      StrPCopy(@xText, xString);
      PutData(PChar(MM_ARTIKEL), @xText, str40, aGrpNo, 0);
      // Color auf Informator speichern
      PutData(PChar(MM_PARTIECOLOR), @aColor, sizeof(aColor), aGrpNo, 0);
      // Schlupf auf Informator speichern
      Fillchar(xText, sizeof(xText), 0);
      xString := floattostrf(aProdGrpInfo.c_slip / 1000.0, ffFixed, 7, 3); // Schlupf
      StrPCopy(@xText, xString);
      PutData(PChar(MM_SCHLUPF), @xText, str40, aGrpNo, 0);
      CodeSite.SendFmtMsg('SetMMPartieandArticleparam: StyleID=%d,OrderID=%d,OrderPosID=%d,ISlip=%d,MGroup=%d,Color=%d',
        [aProdGrpInfo.c_style_id, aProdGrpInfo.c_order_id, aProdGrpInfo.c_order_position_id,
         aProdGrpInfo.c_slip, aGrpNo, aColor]);
    end;
  except
    on E: Exception do
      CodeSite.SendWarning('SetArticleString ' + e.message)
    else
    CodeSite.SendError('SetArticleString failt with unknown error');
  end;
end;

//..............................................................................
procedure tInformthread.GetMMPartieandArticleparam(var aProdGrpInfo: TProdGrpInfo; aGrpNo: Integer; var aColor: integer);
var
  xIndex, xColor: Integer;
  xstring: TstringList;
  xText: TText100;
  xschlupf: LongInt;
begin
  with mWSCHandler do begin
    try
      Fillchar(xText, sizeof(xText), 0);
      xIndex := GetIndex(PChar(MM_ARTIKEL));
      GetData(xIndex, @xText, sizeof(xText), aGrpNo, 0);
      xstring := TstringList.Create;
      // Stringlist kann nur Kommas als Delimiter erkennen
      xstring.CommaText := string(xText);
      if (xstring.Count = 3) then begin
        aProdGrpInfo.c_style_id := strtoInt(xstring.Strings[0]);
        aProdGrpInfo.c_order_id := strtoInt(xstring.Strings[1]);
        if strtoInt(xstring.Strings[2]) <= 255 then
          // aProdGrpInfo.c_order_position_id := strtoInt(xstring.Strings[2])
          aProdGrpInfo.c_order_position_id := 1  // Nur Test bis Jobhandler ob
        else Abort;
      end else Abort;
      // Schlupf lesen
      Fillchar(xText, sizeof(xText), 0);
      xIndex := GetIndex(PChar(MM_SCHLUPF));
      GetData(xIndex, @xText, sizeof(xText), aGrpNo, 0);
      xschlupf := round(strtofloat(xText) * 1000);
      if (xSchlupf < cMaxSlip * 1000) and (xSchlupf > cMinSlip * 1000) then
         aProdGrpInfo.c_slip := xSchlupf
      else begin
         // Schlupf ausser Bereich
        CodeSite.SendFmtMsg('Schlupf ungueltig : MaGrp=%d,S=%d', [aGrpNo, xschlupf]);
        aProdGrpInfo.c_slip := cDefaultSlip; // 1000
        // Schlupf auf Informator setzen
      end;
    except
      // Artikel,order_id,order_position_id oder schlupf ungueltig oder nicht vorhanden: default= 1 setzen
      aProdGrpInfo.c_style_id := 1;
      aProdGrpInfo.c_order_id := 1;
      aProdGrpInfo.c_order_position_id := 1;
      aProdGrpInfo.c_slip := cDefaultSlip; // 1000
      CodeSite.SendFmtMsg('GetMMPartieandArticleparam: Unknown StyleId Order -Id -position : MaGrp=%d, Text=%s', [aGrpNo, string(xText)]);
    end;
    xstring.Free;
    // PartieName lesen
    Fillchar(xText, sizeof(xText), 0);
    xIndex := GetIndex(PChar(MM_PARTIENAME));
    GetData(xIndex, @xText, sizeof(xText), aGrpNo, 0);
    // String von Informator wird auf Ttext50 gekuerzt
    Fillchar(aProdGrpInfo.c_prod_name, sizeof(aProdGrpInfo.c_prod_name), 0);
    strLCopy(@aProdGrpInfo.c_prod_name, @xText, Sizeof(aProdGrpInfo.c_prod_name));
    // Color lesen
    xIndex := GetIndex(PChar(MM_PARTIECOLOR));
    GetData(xIndex, @xColor, sizeof(xColor), aGrpNo, 0);
    aColor := xColor;
    CodeSite.SendFmtMsg('GetMMPartieandArticleparam: StyleID=%d,OrderID=%d,OrderPosID=%d,ISlip=%d,MGroup=%d,Color=%d',
        [aProdGrpInfo.c_style_id, aProdGrpInfo.c_order_id, aProdGrpInfo.c_order_position_id,
         aProdGrpInfo.c_slip, aGrpNo, aColor]);

  end;
end;
//..............................................................................

procedure tInformthread.MMJobDispatcher(aMsg: TMsg);
// Alle Jobs werden in das 338 spezifische Format konvertiert
// Exception werden abgefangen und weitergereicht
// Datadirection JobHandler -> WSCHandler
type
  USBufferstruct = (eRange, eStat, eSetting, eMm_data_trans, eAc338declaration, eFloat);
  trange = record
    first, last: Integer;
  end;
  // UniversalStandardBuffer = Varianten Record um verschiedene Strukturen zu lesen oder schreiben
  tUSBuffer = packed record
    case USBufferstruct of
   //   eText100: (text100: TText100);
      eRange: (range: trange);
      eStat: (stat: TGroupState);
      eSetting: (SettingDataArray: TYMSettingsByteArr);
      eMm_data_trans: (MM_data_trans: tMM_data_trans);
      eAc338declaration: (AC338declaration: TAC338declaration);
      eFloat: (float: Single);
  end;

var
  xPJob: PJobRec;
  xWSCDaten: TWSCDaten;
  xUSBuffer: tUSBuffer;
  xGrpNo: Integer;
  xIndex, xIndex1, xspind, xstatus: Integer;
  xFreeJobMem: Boolean;
  xProdGrpYMParameter: TProdGrpYMParaRec;
  xValue: tAcknowledge;


begin
  xFreeJobMem := TRUE;
  with aMsg do begin
    xPJob := PJobRec(wParam);
    try
      case xPJob^.JobTyp of
        //..GetZESpdData
        jtGetZESpdData: begin
            with mWSCHandler do begin
              xIndex := GetIndex(PChar(MM_DATA_TRANSFER));
              GetData(xIndex, @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              // Pruefe ob neue Data Acquisition moeglich
              if xUSBuffer.MM_data_trans.access = Word(MM_DATA_IDLE) then begin
                with xPJob^.GetZESpdData do
                  with xUSBuffer.MM_data_trans do begin // Record xMM_data_trans ueber xBuffer legen
                    Spindfrom := SpindleFirst - 1;
                    Spindto := SpindleLast - 1;
                    access := WORD(MM_DATA_STORE);
                    JobID := xPJob^.JobID;
                  end;
                // Q +Basis Daten anfordern und Timeouttimer starten
                with xUSBuffer.MM_data_trans do begin
                  CodeSite.SendFmtMsg('GetZESpdData: IpAdr=%s, JobId=%d, Acess=%d, SpdFirst=%d, SpdLast=%d',
                    [mIpAdresse, JobID, access, Spindfrom + 1, Spindto + 1]);
                end;
                PutData(PChar(MM_DATA_TRANSFER), @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              end
              else begin                // Job pending
                CodeSite.SendMsg('mJobAcqQueue.Push');
                mJobAcqQueue.Push(xPJob);
                xFreeJobMem := False;
              end;
              KillTimer(mThreadWndHandle, cAcqTimerID);
              SetTimer(mThreadWndHandle, cAcqTimerID, cAcqTimeout, nil);
            end;
          end;
        //..DelZESpdData
        jtDelZESpdData: begin
            with xUSBuffer.MM_data_trans do begin // Record xMM_data_trans ueber xBuffer legen
              access := WORD(MM_DATA_CLEAR);
              JobID := xPJob^.JobID;
              Spindfrom := xPJob^.DelZESpdData.SpindleFirst - 1;
              Spindto := xPJob^.DelZESpdData.SpindleLast - 1;
            end;
            // Empfang von Q+Basis Daten bestaetigen
            with mWSCHandler do begin
              PutData(PChar(MM_DATA_TRANSFER), @xUSBuffer, sizeof(tMM_data_trans), 0, 0);
              mWriteToMsgHnd(xPjob);    // Bestaetigung an MsgHandler schicken
              with xUSBuffer.MM_data_trans do begin
                CodeSite.SendFmtMsg('DelZESpdData: IpAdr=%s, JobId=%d, Acess=%d,SpdFirst=%d, SpdLast=%d',
                  [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
              end;
            end;
          end;
        //.. GetSettingsAllGroups
        //.. GetSettings
        jtGetSettings: begin
            xPJob^.GetSettings.MachineID := mWSCHandler.mMaschID;
            with xPJob^.GetSettings.SettingsRec do
              with mWSCHandler do begin
                // Grp_Setting aus MachGruppe lesen
                xIndex := GetIndex(PChar(GRP_SETTINGS));
                GetData(xIndex, @DataArray, sizeof(DataArray), MachGrp, 0);
                // Anpassung von 338 Settings auf Standart Settings
                TYMSettingsUtils.ConvertSettingsFromAC338(DataArray, sizeof(DataArray));
                ProdGrpInfo.c_prod_id := TYMSettingsUtils.ExtractProdGrpID(DataArray, sizeof(DataArray));
                // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                GetMMPartieandArticleparam(ProdGrpInfo,MachGrp,Color);
                // YMSET Parameter holen
                GetYMSetParm(YM_Set_Name, ProdGrpInfo.c_YM_set_id, YM_Set_Changed, MachGrp);
                // Grp Status lesen
                xIndex := GetIndex(PChar(STATUS_GRP));
                GetData(xIndex, @xstatus, sizeof(xstatus), MachGrp, 0);
                GroupState := TGroupState(xstatus);
                // Spindelbereich aus MachGruppe lesen
                xIndex := GetIndex(PChar(BEREICH_GRP));
                GetData(xIndex, @xUSBuffer, sizeof(trange), MachGrp, 0);
                SpindleFirst := xUSBuffer.range.first + 1; // WSC DataItem beginnt bei 0
                SpindleLast := xUSBuffer.range.last + 1;
                CodeSite.SendInteger('GetSettings ' + mIpAdresse, ProdGrpInfo.c_prod_id);
              end;
            mWriteToMsgHnd(xPjob);      // Daten von GetSettings zu MsgHandler schicken
          end;
        //..SetSettings
        jtSetSettings: begin
            with xPJob^.SetSettings do
              with mWSCHandler do begin
                // Kopieren der Reinigerparameter aus dem Reinigersack
                xProdGrpYMParameter := TYMSettingsUtils.ExtractProdGrpYMPara(DataArray, LengthData);
                xIndex := GetIndex(PChar(STATUS_GRP));
                GetData(xIndex, @xUSBuffer, sizeof(grpstat_typ), MachineGrp - 1, 0);
                // Gruppe Frei oder Definiert
                if (xUSBuffer.stat = gsfree) or (xUSBuffer.stat = gsDefined) then begin
                  if (ProdGrpInfo.c_order_id <> 0) and (ProdGrpInfo.c_order_position_id <> 0)  then begin
                    SetMMPartieandArticleparam(ProdGrpInfo, MachineGrp-1, Color);
                    // YMSet Parameter
                    SetYMSetParm(YM_Set_Name, ProdGrpInfo.c_YM_set_id, MachineGrp - 1);
                  end;
                  // Garnummer fuer 338 DB immer auf Nm-float konvertieren
                  SetWSCPartieparameter(MachineGrp - 1, ProdGrpInfo.c_prod_name, cPartieNoHeaderStr,
 // GL 14.11.2013 -----------------------------------------------------------
                  mWriteLog(etWarning, Format('[GL] WSCHandler MMJobDispatcher jtSettings: Group:%d ',[MachineGrp -1]));
//--------------------------------------------------------------------------
                   SpindleFirst - 1, SpindleLast - 1,
                    Yarncountconvert(TYarnUnits(xProdGrpYMParameter.yarnCntUnit),yuNm,xProdGrpYMParameter.yarnCnt / cYarnCntFactor),
                    //  xProdGrpYMParameter.yarnCnt / 10.0,
                    xProdGrpYMParameter.nrOfThreads);
                end else begin
                  // Gruppe InProduktion
                  SetWSCPartieName(MachineGrp - 1, ProdGrpInfo.c_prod_name);
                  if (ProdGrpInfo.c_order_id <> 0) and (ProdGrpInfo.c_order_position_id <> 0) then begin
                    SetMMPartieandArticleparam(ProdGrpInfo, MachineGrp-1, Color);
                    // SetWSCPartieNummer(MachineGrp - 1, cPartieNoHeaderStr);
                    // YMSet Parameter
                    SetYMSetParm(YM_Set_Name, ProdGrpInfo.c_YM_set_id, MachineGrp - 1);
                  end;
                end;
                // YM-Settings
                // Jobid auf YM MachineGrp setzen
                PutData(PChar(MM_JOB_ID), @xPJob^.JobID, Sizeof(DWord), MachineGrp - 1, 0);
                // Anpassung von Standart Settings auf 338 Settings
                TYMSettingsUtils.ConvertSettingsToAC338(DataArray, LengthData);
                // Settings der Maschgruppe setzen
                PutData(PChar(GRP_PRE_SETTINGS), @DataArray, LengthData, MachineGrp - 1, 0);
                CodeSite.SendFmtMsg('SetSettings: IpAdr=%s, JobId=%d, MaschGrp=%d, ProdGrp=%d,SpdFirst=%d, SpdLast=%d',
                  [mIpAdresse, xPJob^.JobID, MachineGrp, TYMSettingsUtils.ExtractProdGrpID(DataArray, sizeof(DataArray)),
                  SpindleFirst, SpindleLast]);
              end;
            // Bestaetigung werden mit Einstellungen mittels Inform an MsgHandler geschickt
          end;
        jtClearZESpdData: begin
            // Nach Partiestart werden die Daten durch Informator geloescht. Daher
            // nur Job bestaetigen.
            CodeSite.SendMsg('ClearZESpdData' + mWSCHandler.mIpAdresse);
            mWriteToMsgHnd(xPjob);      // Bestaetigung zu MsgHandler schicken
          end;
        jtGetMaConfig: begin
            with xPJob^.GetMaConfig do
              with mWSCHandler do begin
                // Grp_Setting aus allen MachGruppe lesen
                for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
                  with Settings[xGrpNo] do begin
                    LengthData := 0;
                    // Settings aus Machgruppe lesen
                    xIndex := GetIndex(PChar(GRP_SETTINGS));
                    GetData(xIndex, @DataArray, sizeof(TYMSettingsByteArr), xGrpNo, 0);
                    // Anpassung von 338 Settings auf Standart Settings
                    TYMSettingsUtils.ConvertSettingsFromAC338(DataArray, sizeof(DataArray));
                    ProdGrpInfo.c_prod_id := TYMSettingsUtils.ExtractProdGrpID(DataArray, sizeof(DataArray));
                     // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                    GetMMPartieandArticleparam(ProdGrpInfo,xGrpNo,Color);
                    //ExtractandFillProdInfo(xGrpNo, ProdGrpInfo, Color);
                    // Grp Status lesen
                    xIndex := GetIndex(PChar(STATUS_GRP));
                    GetData(xIndex, @xstatus, sizeof(xstatus), xGrpNo, 0);
                    GroupState := TGroupState(xstatus);
                    // Spindelbereich aus MachGruppe lesen
                    xIndex := GetIndex(PChar(BEREICH_GRP));
                    GetData(xIndex, @xUSBuffer, sizeof(trange), xGrpNo, 0);
                    SpindleFirst := xUSBuffer.range.first + 1; // WSC DataItem beginnt bei 0
                    SpindleLast := xUSBuffer.range.last + 1;
                    CodeSite.SendFmtMsg('GetMaConfig: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, SpdFirst=%d, SpdLast=%d',
                      [mIpAdresse, xGrpNo + 1, ProdGrpInfo.c_prod_id, SpindleFirst, SpindleLast]);
                  end;
                end;
              end;
            mWriteToMsgHnd(xPjob);      // Bestaetigung zu MsgHandler schicken
          end;
        jtSetMaConfig:begin             // Es werden nur Maschinengruppen beschrieben welche  Frei oder Definiert sind
            with xPJob^.SetMaConfig do
              with mWSCHandler do begin
                // Setzen der PartieGarnEinheit ueber WSC DLL
                //xProdGrpYMParameter := TYMSettingsUtils.ExtractProdGrpYMPara(Settings[1].DataArray, Settings[1].LengthData);
                //with xProdGrpYMParameter do begin
                  //SetMMPartieGarnEinheit(MachineGrp-1,xProdGrpYMParameter.yarnCntUnit);
                //end;
                xIndex := GetIndex(PChar(STATUS_GRP));
                // Alle GroupSettings
                for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
                  GetData(xIndex, @xUSBuffer, sizeof(grpstat_typ), xGrpNo, 0);
                  // Spindelbereich setzen: Nur wenn Gruppe Frei oder Definiert
                  if (xUSBuffer.stat = gsfree) or (xUSBuffer.stat = gsDefined) then begin
                    // Alle ReinigerSettings
                    with Settings[xGrpNo] do begin
                      // Kopieren der Reinigerparameter aus dem Reinigersack
                      xProdGrpYMParameter := TYMSettingsUtils.ExtractProdGrpYMPara(Settings[xGrpNo].DataArray,
                        Settings[xGrpNo].LengthData);
                      SetMMPartieandArticleparam(ProdGrpInfo, xGrpNo, Color);
                      // Garnummer fuer 338 DB immer auf Nm-float konvertieren
                      SetWSCPartieparameter(xGrpNo, ProdGrpInfo.c_prod_name, cPartieNoHeaderStr,
 // GL 14.11.2013 -----------------------------------------------------------
                  mWriteLog(etWarning, Format('[GL] WSCHandler MMJobDispatcher jtSetMaConfig: Group:%d ',xGrpNo));
//--------------------------------------------------------------------------
                       SpindleFirst - 1, SpindleLast - 1,
                        Yarncountconvert(TYarnUnits(xProdGrpYMParameter.yarnCntUnit),yuNm,xProdGrpYMParameter.yarnCnt / cYarnCntFactor),
                        // xProdGrpYMParameter.yarnCnt / 10.0,
                        xProdGrpYMParameter.nrOfThreads);
                      TYMSettingsUtils.ConvertSettingsToAC338(DataArray, LengthData);
                      PutData(PChar(GRP_PRE_SETTINGS), @DataArray, LengthData, xGrpNo, 0);
                      CodeSite.SendFmtMsg('SetMaConfig: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, SpdFirst=%d, SpdLast=%d',
                        [mIpAdresse, xGrpNo + 1, ProdGrpInfo.c_prod_id, SpindleFirst, SpindleLast]);
                    end;
                  end;
                end;
              end;
            mWriteToMsgHnd(xPjob);      // Bestaetigung zu MsgHandler schicken
          end;
        jtViewZESpdData:begin           // Achtung Allignment
            with mWSCHandler do begin
              xIndex := Getindex(PChar(MM_BASISDATEN));
              xIndex1 := Getindex(PChar(MM_KLASSIERDATEN));
              with xPJob^.ViewZESpdData do
                for xspind := SpindleFirst - 1 to SpindleFirst - 1 do begin // WSC DataItem beginnt bei 0
                  GetData(xindex, @xWSCDaten.baseData, Sizeof(xWSCDaten.baseData), xspind, 1);
                  GetData(xindex1, @xWSCDaten.klassierdaten, Sizeof(xWSCDaten.klassierdaten), xspind, 1);
                  SpindID := xspind + 1;
                  Move(xWSCDaten, SpdDataArr, Sizeof(xWSCDaten)-Sizeof(xWSCDaten.klassierdaten.siroDataBright));
                  mWriteToMsgHnd(xPJob); // Daten zu MsgHandler schicken
                end;
            end;
            CodeSite.SendMsg('ViewZESpdData ' + mWSCHandler.mIpAdresse);
          end;
        jtGetMaAssign: begin
            with mWSCHandler do begin
              with xPJob^.GetMaAssign do begin
                FillChar(Assigns, sizeof(TMaAssign), 0);
                MachineID := mMaschID;
                for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
                  xIndex := Getindex(PChar(MM_DECLARATION));
                  GetData(xIndex, @xUSBuffer, Sizeof(TAC338declaration), xGrpNo, 0);
                  with xUSBuffer.AC338declaration do begin
                    Assigns.Event := TDecEvent(event);
                    Assigns.MachState := states;
                    // TMaAssign Array beginnt bei 1, WSC DataItem aGroup bei 0
                    Assigns.Groups[xGrpNo + 1].Modified := modify;
                    // Wird von WSC gelesen / Assigns.Groups[xGrpNo+1].GroupState:= TGroupState(GrpState);
                    Assigns.Groups[xGrpNo + 1].ProdId := prodGrpID;
                    // Wird von WSC gelesen / Assigns.Groups[xGrpNo+1].SpindleFirst:= Spindfrom;
                    // Wird von WSC gelesen / Assigns.Groups[xGrpNo+1].SpindleLast:= Spindto;
                    CodeSite.SendFmtMsg('GetMaAssign: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                      [mIpAdresse, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                  end;
                  // Modify Flag und Event zuruecksetzen
                  xValue := MM_RESET_DECL;
                  mWSCHandler.PutData(PChar(MM_ACKNOWLEDGE), @xValue, Sizeof(tAcknowledge), xGrpNo, 0);
                  CodeSite.SendFmtMsg('GetMaAssign_MM_RESET_DECL: MaGrpNo=%d', [xGrpNo]);
                  // Temporaer von WSC  // Grp Bereich lesen
                  with Assigns.Groups[xGrpNo + 1] do begin
                    xIndex := GetIndex(PChar(BEREICH_GRP));
                    GetData(xIndex, @xUSBuffer, sizeof(xUSBuffer.range), xGrpNo, 0);
                    SpindleFirst := xUSBuffer.range.first + 1;
                    SpindleLast := xUSBuffer.range.last + 1;
                    // Temporaer von WSC  // Grp Status lesen
                    xIndex := GetIndex(PChar(STATUS_GRP));
                    GetData(xIndex, @xstatus, sizeof(xstatus), xGrpNo, 0);
                    GroupState := TGroupState(xstatus);
                    // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                    GetMMPartieandArticleparam(ProdGrpParam.ProdGrpInfo,xGrpNo,Color);
                    //ExtractandFillProdInfo(xGrpNo, ProdGrpParam.ProdGrpInfo, Color);
                  end;
                end;
              end;
              mWriteToMsgHnd(xPJob);    // Daten zu MsgHandler schicken
            end;
          end
      else
      end;
    except
      on E: Exception do begin
        xFreeJobMem := True;
        raise WSCAccessException.Create('MMJobDispatcher ' + e.message);
      end;
    end;
    if xFreeJobMem then FreeMem(xPJob, xPJob.JobLen);
  end;
end;

//..............................................................................

procedure tInformthread.InformDispatcher(aMsg: TMsg);
{ Diese Methode verarbeitet Informs welche von den DataItems DATA_TRANSFER,
  MM_DECLARATION, GRP_SETTINGS ausgeloest werden.
  Datadirection Informator -> WSCHandler }
// Exception werden abgefangen und weitergereicht
var
  xInfIndex, xIndex, xGrpNo, xspind, xSet_id, xSet_modify: Integer;
  xfloat: single;
  xModify: Boolean;
  xMM_data_trans: tMM_data_trans;
  xMM_Declaration: TAC338declaration;
  xYM_set_id: Word; xYM_Set_Changed: Boolean; // Dummyparameter fuer GetYMSetParm bei eEntryLocked
  xBasisdaten_index, xKlassierdaten_index: tdataItemIndex;
  xjob: TJobRec;
  xWSCDaten: TWSCDaten;


  procedure MoveDecltoJob(aGroup: Integer; adeclaration: TAC338declaration; var aMaAssign: TMaAssign);
    // Der WSC Declaration-Record wird in den Job Declaration-Record eingefuegt
  var
    xIndex: Integer;
    xDataArray: TYMSettingsByteArr;
  begin
    FillChar(aMaAssign, Sizeof(aMaAssign), 0);
    with aMaAssign do begin
      Event := TDecEvent(adeclaration.Event);
      MachState := adeclaration.states;
      // TMaAssign Array beginnt bei 1, aGroup bei 0
      Groups[aGroup + 1].Modified := adeclaration.modify;
      Groups[aGroup + 1].GroupState := TGroupState(adeclaration.GrpState);
      Groups[aGroup + 1].ProdId := adeclaration.prodGrpID;
      Groups[aGroup + 1].SpindleFirst := adeclaration.Spindfrom;
      Groups[aGroup + 1].SpindleLast := adeclaration.Spindto;
    end;
  end;

  procedure DeleteWSCDecl(aGrpNo: Integer);
  var
    xValue: tAcknowledge;
  begin
    // Declaration Event und Modify Flag der Gruppe zuruecksetzen
    xValue := MM_RESET_DECL;
    mWSCHandler.PutData(PChar(MM_ACKNOWLEDGE), @xValue, Sizeof(tAcknowledge), aGrpNo, 0);
    CodeSite.SendFmtMsg('MM_RESET_DECL: MaGrpNo=%d', [aGrpNo]);
  end;

begin
  try
    with mWSCHandler do
      with aMsg do begin
        CheckInform(Message, lParam, xInfIndex, xGrpNo);
        if xInfIndex > 0 then begin
          //  CodeSite.SendMsg('Inform eingetroffen');
 //.. DATA_TRANSFER
          if xInfIndex = Getindex(MM_DATA_TRANSFER) then begin
            GetData(xInfIndex, @xMM_data_trans, Sizeof(xMM_data_trans), 0, 0);
            //.. MM_DATA_READY
            if xMM_data_trans.access = Word(MM_DATA_READY) then begin // Daten zum abholen bereit
              xJob.JobTyp := jtGetZESpdData;
              xjob.JobID := xMM_data_trans.JobID;
              xBasisdaten_index := Getindex(PChar(MM_BASISDATEN));
              xKlassierdaten_index := Getindex(PChar(MM_KLASSIERDATEN));
              for xspind := xMM_data_trans.Spindfrom to xMM_data_trans.Spindto do begin
                GetData(xBasisdaten_index, @xWSCDaten.baseData, Sizeof(xWSCDaten.baseData), xspind, 1);
                GetData(xklassierdaten_index, @xWSCDaten.klassierdaten, Sizeof(xWSCDaten.klassierdaten), xspind, 1);
                xJob.GetZeSpdData.SpindID := xspind + 1; // WSC DataItem beginnt bei 0
                Move(xWSCDaten, xJob.GetZeSpdData.SpdDataArr, Sizeof(xWSCDaten)-Sizeof(xWSCDaten.klassierdaten.siroDataBright));
                mWriteToMsgHnd(@xjob);  // Daten zu MsgHandler schicken
              end;
              with xMM_data_trans do begin
                CodeSite.SendFmtMsg('MM_DATA_READY: IpAdr=%s, JobId=%d, Acess=%d,SpdFirst=%d, SpdLast=%d',
                  [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
              end;
            end
              //..MM_DATA_IDLE
            else if xMM_data_trans.access = Word(MM_DATA_IDLE) then begin
              // Data Acquisiton beendet, Timeout abbrechen
              KillTimer(mThreadWndHandle, cAcqTimerID);
              // Pruefe ob Queue leer ist
              if mJobAcqQueue.Count > 0 then begin
                codesite.SendInteger('mJobAcyQueue ' + mIpAdresse, mJobAcqQueue.Count);
                // Hole Job von Queue und sende Ihn erneut
                PostMessage(mThreadWndHandle, mJobRequestMsg, Integer(MjobAcqQueue.Pop), 0);
              end;
              with xMM_data_trans do begin
                CodeSite.SendFmtMsg('MM_DATA_IDLE: IpAdr=%s,JobId=%d, Acess=%d, SpdFirst=%d, SpdLast=%d',
                  [mIpAdresse, JobId, access, Spindfrom + 1, Spindto + 1]);
              end;
            end;
          end
            //..MM_DECLARATION
          else if xInfIndex = Getindex(MM_DECLARATION) then begin
            if GetData(xInfIndex, @xMM_Declaration, Sizeof(TAC338declaration), xGrpNo, 0) then begin
              case xMM_Declaration.event of
                eNone: begin
                    CodeSite.SendFmtMsg('None: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eAssignComplete: begin
                    CodeSite.SendFmtMsg('AssignComplete: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eInitialReset: begin
                    xJob.JobTyp := jtInitialReset;
                    xJob.InitialReset.MachineID := mMaschID;
                    mWriteToMsgHnd(@xjob); // InitialReset zu MsgHandler schicken
                    CodeSite.SendFmtMsg('InitialReset: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eReset: begin
                    xJob.JobTyp := jtReset;
                    xJob.Reset.MachineID := mMaschID;
                    mWriteToMsgHnd(@xjob); // Reset Event zu MsgHandler schicken
                    CodeSite.SendFmtMsg('Reset: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eEntryLocked: begin     // Bildschirmschoner oder Logoff auf Informator
                    CodeSite.SendFmtMsg('EntryLocked: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                    xJob.JobTyp := jtEntryLocked;
                    xJob.EntryLocked.MachineID := mMaschID;
                    // Declarations aller Gruppe abholen und zuruecksetzten
                    xIndex := Getindex(PChar(MM_DECLARATION));
                    with xJob.AssignComplete do begin
                      FillChar(Assigns, sizeof(TMaAssign), 0);
                      for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
                        // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                        GetMMPartieandArticleparam( Assigns.Groups[xGrpNo + 1].ProdGrpParam.ProdGrpInfo, xGrpNo, Assigns.Groups[xGrpNo + 1].Color);
                        // YM_set_name holen
                        GetYMSetParm(Assigns.Groups[xGrpNo + 1].ProdGrpParam.ProdGrpInfo. c_YM_set_name,xYM_set_id, xYM_Set_Changed, xGrpNo);
                        GetData(xIndex, @xMM_Declaration, Sizeof(TAC338declaration), xGrpNo, 0);
                        with xMM_Declaration do begin
                          Assigns.Event := TDecEvent(event);
                          Assigns.MachState := states;
                          // TMaAssign Array beginnt bei 1, WSC DataItem aGroup bei 0
                          Assigns.Groups[xGrpNo + 1].Modified := modify;
                          Assigns.Groups[xGrpNo + 1].GroupState := TGroupState(GrpState);
                          Assigns.Groups[xGrpNo + 1].ProdId := prodGrpID;
                          Assigns.Groups[xGrpNo + 1].SpindleFirst := Spindfrom;
                          Assigns.Groups[xGrpNo + 1].SpindleLast := Spindto;
                          // Pruefe mittels ProdID ob Assign Complete
                          with Assigns.Groups[xGrpNo + 1] do begin
                            if (GroupState = gsInProd) or (GroupState = gsLotChange) then
                              if ProdId = 0 then begin
                                xJob.JobTyp := jtAssignComplete;
                                xJob.AssignComplete.MachineID := mMaschID;
                                CodeSite.SendFmtMsg('AsignComplete after Locked: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                                  [mIpAdresse, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                              end;
                          end;
                        end;
                        DeleteWSCDecl(xGrpNo);
                        // Modify Flag und Event zuruecksetzen
                      end;
                    end;
                    mWriteToMsgHnd(@xjob); // AssignComplete zu MsgHandler schicken
                  end;
                eEntryUnlocked: begin   // Login auf Informator
                    xJob.JobTyp := jtEntryUnlocked;
                    xJob.EntryUnlocked.MachineID := mMaschID;
                    mWriteToMsgHnd(@xjob); // EntryUnlocked Event MsgHandler schicken
                    CodeSite.SendFmtMsg('EntryUnlocked: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eSettings, eRange: begin
                    xJob.JobTyp := jtAssign;
                    xJob.Assign.MachineID := mMaschID;
                    MoveDecltoJob(xGrpNo, xMM_Declaration, xJob.Assign.Assigns);
                    mWriteToMsgHnd(@xjob); // Assign zu MsgHandler schicken
                    with xMM_Declaration do begin
                      CodeSite.SendFmtMsg('Settings or Range changed: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                        [mIpAdresse, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                    end;
                  end;
                eAdjust: begin
                    xJob.JobTyp := jtAdjust;
                    xJob.Adjust.MachineID := mMaschID;
                    mWriteToMsgHnd(@xjob); // tAdjust Event MsgHandler schicken
                    CodeSite.SendFmtMsg('Adjust: IpAdr=%s, MaschGrp=%d', [mIpAdresse, xGrpNo + 1]);
                  end;
                eStart: begin
                    xJob.JobTyp := jtStartGrpEvFromMa;
                    with xJob.StartGrpEvFromMa do begin
                      // Grp_Setting aus MachGruppe(xGrpNo) lesen
                      xIndex := Getindex(PChar(GRP_SETTINGS));
                      GetData(xIndex, @DataArray, sizeof(DataArray), xGrpNo, 0);
                      TYMSettingsUtils.ConvertSettingsFromAC338(DataArray, sizeof(DataArray));
                      MachineID := mMaschID;
                      // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                      GetMMPartieandArticleparam(ProdGrpParam.ProdGrpInfo,xGrpNo,Color);
                      MachineGrp := xGrpNo + 1; // WSC DataItem beginnt bei 0
                      ProdGrpParam.ProdGrpInfo.c_machine_id := mMaschID;
                      xIndex := Getindex(PChar(MM_DECLARATION));
                      GetData(xIndex, @xMM_Declaration, Sizeof(TAC338declaration), xGrpNo, 0);
                      ProdGrpID := xMM_Declaration.prodGrpID;
                      SpindleFirst := xMM_Declaration.Spindfrom;
                      SpindleLast := xMM_Declaration.Spindto;
                      // YMSET Parameter holen
                      GetYMSetParm(YM_Set_Name, ProdGrpParam.ProdGrpInfo.c_YM_set_id, YM_Set_Changed, xGrpNo);
                    end;
                    mWriteToMsgHnd(@xjob); // Settings zu MsgHandler schicken
                    with xMM_Declaration do begin
                      CodeSite.SendFmtMsg('StartGrpEvFromMa: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                        [mIpAdresse, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                    end;
                  end;
                eStop: begin
                    xJob.JobTyp := jtStopGrpEvFromMa;
                    with xJob.StopGrpEvFromMa do begin
                      // Grp_Setting aus MachGruppe(xGrpNo) lesen
                      xIndex := Getindex(PChar(GRP_SETTINGS));
                      GetData(xIndex, @DataArray, sizeof(DataArray), xGrpNo, 0);
                      TYMSettingsUtils.ConvertSettingsFromAC338(DataArray, sizeof(DataArray));
                      MachineGrp := xGrpNo + 1; // WSC DataItem beginnt bei 0
                      MachineID := mMaschID;
                      ProdGrpID := xMM_Declaration.prodGrpID;
                      SpindleFirst := xMM_Declaration.Spindfrom;
                      SpindleLast := xMM_Declaration.Spindto;
                    end;
                    mWriteToMsgHnd(@xjob); // Settings zu MsgHandler schicken
                    with xMM_Declaration do begin
                      CodeSite.SendFmtMsg('StopGrpEvFromMa: IpAdr=%s, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                        [mIpAdresse, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                    end;
                  end;
                ePreSettings: begin
                    xIndex := Getindex(PChar(MM_JOB_ID));
                    GetData(xIndex, @xJob.JobID, sizeof(xJob.JobID), xGrpNo, 0);
                    if xJob.JobID <> 0 then begin
                      xJob.JobTyp := jtSetSettings;
                      with xJob.SetSettings do begin
                        codesite.SendFmtMsg('xJob at %p, xJob.SetSettings.DataArray at %p', [@xJob, @DataArray]);
                        // Grp_Setting aus MachGruppe(xGrpNo) lesen
                        xIndex := Getindex(PChar(GRP_SETTINGS));
                        GetData(xIndex, @DataArray, sizeof(DataArray), xGrpNo, 0);
                        // Anpassung von 338 Settings auf Standard Settings
                        TYMSettingsUtils.ConvertSettingsFromAC338(DataArray, sizeof(DataArray));
                        // Partiename, Partienummer, Schlupf von Informator lesen und in ProdInfo ablegen
                        GetMMPartieandArticleparam(ProdGrpInfo,xGrpNo,Color);
                        MachineGrp := xGrpNo + 1; // WSC DataItem beginnt bei 0
                        ProdGrpInfo.c_machine_id := mMaschID;
                        xIndex := Getindex(PChar(MM_DECLARATION));
                        GetData(xIndex, @xMM_Declaration, Sizeof(TAC338declaration), xGrpNo, 0);
                        ProdGrpInfo.c_prod_id := xMM_Declaration.prodGrpID;
                        SpindleFirst := xMM_Declaration.Spindfrom;
                        SpindleLast := xMM_Declaration.Spindto;
                        // YMSET Parameter holen
                        GetYMSetParm(YM_Set_Name, ProdGrpInfo.c_YM_set_id, YM_Set_Changed, xGrpNo);
                      end;
                      // Bestaetigung mit Einstellungen werden an MsgHandler geschickt
                      mWriteToMsgHnd(@xjob);
                      with xMM_Declaration do begin
                        CodeSite.SendFmtMsg('SetSettings acknowledge: IpAdr=%s,JobId=%d, MaschGrp=%d, ProdGrp=%d, Modify=%d, SpdFirst=%d, SpdLast=%d',
                          [mIpAdresse, xJob.JobID, xGrpNo + 1, prodGrpID, WORD(modify), Spindfrom, Spindto]);
                      end;
                      xJob.JobID := 0;
                      PutData(PChar(MM_JOB_ID), @xJob.JobID, Sizeof(DWord), xGrpNo, 0);
                    end else
                      CodeSite.SendFmtMsg('SetSettings acknowledge: IpAdr=%s,JobId=%d, MaschGrp=%d',
                        [mIpAdresse, xJob.JobID, xGrpNo + 1]);
                  end;
              else
                CodeSite.SendMsg('Receive Unknown Declartaion');
              end;
            end;
          end
        end
        else begin
          KillTimer(mThreadWndHandle, cLifeCheckTimerID);
          //  CodeSite.SendInteger('Lifecheck',mWSCHandler.mInform_socket);
          LifeCheck();
          SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
        end;
      end;
  except
    on E: Exception do raise WSCAccessException.Create('InformDispatcher ' + e.message);
    // on E: Exception do raise Exception.Create('InformDispatcher '+ e.message);
  end;
end;

//..............................................................................

procedure tInformthread.CleanDataAcqQueue();
{ Die DataAcquisation Transaktion wird abgebrochen (Rollback).
  Alle Jobs in der Jobqueue des betreffenden Threads werden geloescht.
  Der Timer fuer die und DataAcquisation wird gestoppt.}
var
  xIndex: Integer;
  xPJob: PJobRec;
  xMM_data_trans: tMM_data_trans;
begin
  try
    with mWSCHandler do begin
      while mJobAcqQueue.Count > 0 do begin
        xPJob := mjobAcqQueue.Pop;
        FreeMem(xPJob, xPJob.JobLen);
      end;
      xIndex := GetIndex(PChar(MM_DATA_TRANSFER));
      GetData(xIndex, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      // Pruefe ob DataTransaction laeuft
      if xMM_data_trans.access <> Word(MM_DATA_IDLE) then begin
        // DataTransaction abbrechen, Rollback
        xMM_data_trans.access := WORD(MM_DATA_RESTORE);
        PutData(PChar(MM_DATA_TRANSFER), @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      end;
    end;
  except
    on E: Exception do begin
      KillTimer(mThreadWndHandle, cAcqTimerID);
      raise WSCAccessException.Create('CleanDataAcqQueue ' + e.message);
    end;
  end;
  CodeSite.SendMsg('RollbackMM_DATA_TRANSFER');
  KillTimer(mThreadWndHandle, cAcqTimerID)
end;

//..............................................................................

function tInformthread.ConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsaufbau aufgerufen.
  Dabei wird die Jobid auf allen MachineGruppen auf 0 gesetzt und dann
  die Online Msg an den MsgHandler geschickt. Zuletzt wird die NodeListe aktualisiert
  und der Timer fuer den Lifecheck gestartet, welcher vom Informator vor Ablauf des
  Timeouts zurueckgesetzt wird. Zusaetzlich wird DataTransaction abgebrochen, Rollback}
var
  xGrpNo, JobInitvalue: Integer;
  xjob: TJobRec;
  xIndex: Integer;
  xMM_data_trans: tMM_data_trans;
begin
  result := false;
  try
    mWSCHandler.Connect;
    result := true;
    // Jobid auf allen MachineGruppen auf 0 setzen
    JobInitvalue := 0;
    with mWSCHandler do begin
      for xGrpNo := 0 to cWSCSpdGroupLimit - 1 do begin
        PutData(PChar(MM_JOB_ID), @JobInitvalue, Sizeof(DWord), xGrpNo, 0);
      end;
      xIndex := GetIndex(PChar(MM_DATA_TRANSFER));
      GetData(xIndex, @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      // Pruefe ob DataTransaction laeuft
      if xMM_data_trans.access <> Word(MM_DATA_IDLE) then begin
        // DataTransaction abbrechen, Rollback
        xMM_data_trans.access := WORD(MM_DATA_RESTORE);
        PutData(PChar(MM_DATA_TRANSFER), @xMM_data_trans, sizeof(tMM_data_trans), 0, 0);
      end;
    end;
    xJob.JobTyp := jtMaOnline;
    xJob.MaOnline.MachineID := mMaschID;
    mWriteToMsgHnd(@xjob);              // Online zu MsgHandler schicken
    CodeSite.SendString('Online Msg', mWSCHandler.mIpAdresse);
    // Update NodeList
    mSetMachstatus(mMaschID, nsOnline); //(nsOffline, nsOnline, nsNotValid);
  except
  end;
  SetTimer(mThreadWndHandle, cLifeCheckTimerID, cLifeCheckTimeout, nil);
end;

//..............................................................................

function tInformthread.DisConnectingMachine(): Boolean;
{ Diese Methode wird fuer den Verbindungsabbau aufgerufen.
  Falls der Informator vorher Online war wird der Status an den MsgHandler
  geschickt. Die NodeListe wird aktualisiert und die beiden Timer Lifecheck und
  DataAcquisation werden gestoppt.}
var
  xjob: TJobRec;
begin
  result := false;
  try
    if mWSCHandler.mConnected = True then begin
      result := mWSCHandler.Disconnect;
      xJob.JobTyp := jtMaOffline;
      xJob.Maoffline.MachineID := mMaschID;
      mWriteToMsgHnd(@xjob);            // Offline nur nach Onlinestatus zu MsgHandler schicken
      CodeSite.SendString('Offline Msg', mWSCHandler.mIpAdresse);
    end;
  except
  end;
  // Update NodeList
  mSetMachstatus(mMaschID, nsOffline);  //(nsOffline, nsOnline, nsNotValid);
  KillTimer(mThreadWndHandle, cLifeCheckTimerID);
  KillTimer(mThreadWndHandle, cAcqTimerID);
end;

//..............................................................................

function tInformthread.SendMachinenstatus(): Boolean;

var
  xjob: TJobRec;
begin
  result := false;
  try
    if mWSCHandler.mConnected = True then
      xJob.JobTyp := jtMaOnline
    else
      xJob.JobTyp := jtMaOffline;
    xJob.Maoffline.MachineID := mMaschID;
    mWriteToMsgHnd(@xjob);              // Offline nur nach Onlinestatus zu MsgHandler schicken
  except
  end;
end;

//--Informthread.Execute;-------------------------------------------------------

procedure tInformthread.Execute;

// Informs vom Informator und Jobs vom JobHandler werden pro Maschine verarbeitet

var
  xMsg: TMsg;
  xInformThreadMsg: Boolean;
begin
  try
    FreeOnTerminate := True;
    mJobRequestMsg := RegisterWindowMessage(cMMJobRequestMsg);
    mWSCInformMsg := RegisterWindowMessage(mThreadIpAdresse); // Pro Thread unique MsgID loesen
    mThreadWndHandle := AllocateHWnd(nil); // Pro Thread Window mit MsgQueue erzeugen
    if not assigned(mWSCHandler) then begin
      mWSCHandler := TWscDevHandler.create(mThreadWndHandle, mWSCInformMsg, mThreadIpAdresse, mMaschID);
      ConnectingMachine;
      SetTimer(mThreadWndHandle, cMachstatusTickerID, cMachstatusTickertime, nil); // Machstatus alle Minuten an MM system
      with mWSCHandler do begin
        //.. Begin Message Loop.........................................................
        while GetMessage(xMsg, mThreadWndHandle, 0, 0) do begin
          Sleep(1);
          try
            with xMsg do begin
              xInformThreadMsg := False;
              // Kontrolle ob MsgID und Informsocket richtig
              if (Message = mInformWndMsg) and (wParam = mInform_socket) then begin
                InformDispatcher(xMsg); // Inform vom Informator
                xInformThreadMsg := True;
              end;
              if Message = mJobRequestMsg then begin
                MMJobDispatcher(xMsg);  // Job vom JobHandler
                xInformThreadMsg := True;
              end;
              if Message = WM_Timer then begin
                if wParam = cAcqTimerID then begin
                  CleanDataAcqQueue;    // Timeout der DataAcquisation Queue
                  xInformThreadMsg := True;
                end else
                  if wParam = cMachstatusTickerID then begin
                    // Machstatus senden
                    SendMachinenstatus;
                  end else
                    if wParam = cLifeCheckTimerID then begin
                      raise LifecheckEx.Create('Timeout LifeCheck ');
                      // Kein Lifecheck der 338 daher Disconnect
                    end;
              end;
              if Message = WM_Quit then
                BREAK;  // Abruch der Schleife durch WSCWriter
              if not xInformThreadMsg then
                // Alle andern Meldungen weiterleiten
                DefWindowProc(mThreadWndHandle, Message, wParam, lParam);
            end;
          except
            on E: WSCAccessException do begin
              mWriteToLog(etWarning, e.message + mWSCHandler.mIpAdresse);
              CodeSite.SendMsg(e.message + mWSCHandler.mIpAdresse); // Protokollmeldung !!
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
            on E: LifecheckEx do begin
              //nue              mWriteToLog(etInformation, e.message+ mWSCHandler.mIpAdresse);
              CodeSite.SendMsg(e.message + mWSCHandler.mIpAdresse);
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
          else begin
              mWriteToLog(etError, 'FatalError' + mWSCHandler.mIpAdresse);
              CodeSite.SendMsg('Unknow Error');
              // Neuer Verbindungsaufbau zu Informator herstellen
              DisConnectingMachine;
              ConnectingMachine;
            end;
          end;
        end;                            // end while
        //.. End Message Loop.........................................................
      end;
    end;
  finally
    DisConnectingMachine;
  end;
end;


//==============================================================================

// CLASS tNodeList

//==============================================================================

procedure tNodeList.SetNodeList(const aNodeList: TWscNetNodeList);
var
  i: Integer;
begin
  // Clean fNodeList
  for i := cFirstMachPos to cMaxWscMachine do begin
    fNodeList[i].WscNetNodeRec.IpAdresse := '';
    fNodeList[i].WscNetNodeRec.MaschNo := 0;
    fNodeList[i].WscNetNodeRec.machstate := nsNotValid;
    fNodeList[i].MaschineThread := nil;
  end;
  i := cFirstMachPos;
  while (aNodelist[i].maschNo <> 0) and (i <= cMaxWscMachine) do begin
    fNodeList[i].WscNetNodeRec.IpAdresse := aNodeList[i].IpAdresse;
    fNodeList[i].WscNetNodeRec.MaschNo := aNodeList[i].maschNo;
    fNodeList[i].WscNetNodeRec.machstate := aNodeList[i].machstate;
    INC(i);
    fLastMachpos := i - 1;
  end;
end;

//..............................................................................

procedure tNodeList.GetNodeList(var aNodeList: TWscNetNodeList);
var
  i: integer;
begin
  // Copy fNodeList to aNodelist
  for i := cFirstMachPos to cMaxWscMachine do begin
    aNodeList[i].IpAdresse := fNodeList[i].WscNetNodeRec.IpAdresse;
    aNodeList[i].maschNo := fNodeList[i].WscNetNodeRec.MaschNo;
    aNodeList[i].machstate := fNodeList[i].WscNetNodeRec.machstate;
  end;
end;

//..............................................................................

function tNodeList.GetMachstate(aIpdress: Pchar; aMaschNo: Integer): TWscNodeState;
var
  i: integer;
begin
  result := nsOffline;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].WscNetNodeRec.machstate;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.maschNo = aMaschNo then begin
        result := fNodelist[i].WscNetNodeRec.machstate;
        BREAK;
      end
    end
end;

//..............................................................................

procedure tNodeList.SetMachstate(aIpdress: Pchar; aMaschNo: Integer; aMachstatus: TWscNodeState);
var
  i: integer;
begin
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        fNodelist[i].WscNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.maschNo = aMaschNo then begin
        fNodelist[i].WscNetNodeRec.machstate := aMachstatus;
        BREAK;
      end
    end
end;

//..............................................................................

procedure tNodeList.StartMachinethread(aWriteToMsgHnd: TWriteToMsgHnd; aSetMachstatus: TSetMachstatus;
  aWriteToLog: TWriteToLog);
var
  i: integer;
begin
  for i := cFirstMachPos to LastMachpos do begin
    with fNodeList[i].WscNetNodeRec do
      fNodeList[i].MaschineThread := tInformthread.Create(IpAdresse, maschNo, aWriteToMsgHnd, aSetMachstatus, aWriteToLog);
  end;
end;

//..............................................................................

function tNodeList.GetMachinethread(aIpdress: Pchar; aMaschNo: Integer): tInformThread;
var
  i: integer;
begin
  result := nil;
  // Vergleich IPAdress nur wenn <> NIL
  if aIpdress <> nil then
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.IpAdresse = aIpdress then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
  else
    // Vergleich MaNo wenn IPAdress = NIL
    for i := cFirstMachPos to fLastMachpos do begin
      if fNodelist[i].WscNetNodeRec.maschNo = aMaschNo then begin
        result := fNodelist[i].MaschineThread;
        BREAK;
      end
    end
end;

//..............................................................................

procedure tNodeList.StopMachinethread();
var
  i: integer;
begin
  for i := cFirstMachPos to fLastMachpos do begin
    if fNodelist[i].MaschineThread <> nil then
      //PostMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
      sendMessage(fNodelist[i].MaschineThread.mThreadWndHandle, WM_QUIT, 0, 0);
  end
end;

//..............................................................................
end.

