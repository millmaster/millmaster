(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: WSCHandlerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: T0  WSC_Mainprozess
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0  SP6
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.03.2000  0.00  khp | Datei erstellt
| 11.04.2001  2.06  Nue | TBaseSettingsReader ==> TMMSettingsReader
| 05.10.2002        LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|==========================================================================================*)

unit WSCHandlerClass;

interface

uses
  Classes, mmRegistry, Windows, mmThread,
  LoepfeGlobal, BaseMain, BaseGlobal, mmEventLog, WSCWriterClass, SettingsReader;



type
  TWSCHandler = class(TBaseMain)
  PRIVATE
  PROTECTED
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; OVERRIDE;
    function GetParamHandle: TMMSettingsReader; OVERRIDE;
  PUBLIC
    constructor Create(aSubSystem: TSubSystemTyp); OVERRIDE;
    destructor Destroy; OVERRIDE;
    function Initialize: Boolean; OVERRIDE;
  end;

  //==============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;


{TWSCHandler Class}

constructor TWSCHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
end;

//-----------------------------------------------------------------------------

destructor TWSCHandler.Destroy;
begin
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TWSCHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttWSCWriter: begin
        aThread := TWSCWriter.Create(mSubSystemDef.Threads[1]);
      end;
  else
    aThread := nil;
    Result := False;
  end;
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

function TWSCHandler.GetParamHandle: TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;
//-----------------------------------------------------------------------------

function TWSCHandler.Initialize: Boolean;
var
  xFileVersion: string;
begin
  Result := inherited Initialize;
  if Result then begin
    xFileVersion := 'unknown';
    try
      xFileVersion := GetFileVersion(ParamStr(0));
    except
      // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
    end;
    WriteToEventLog(' started with fileversion ' + xFileVersion, '');
  end;
end;
//-----------------------------------------------------------------------------
end.

