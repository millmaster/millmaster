SeverityNames=(Success=0x0:STATUS_SEVERITY_SUCCESS
               Informational=0x1:STATUS_SEVERITY_INFORMATIONAL
               Warning=0x2:STATUS_SEVERITY_WARNING
               Error=0x3:STATUS_SEVERITY_ERROR
               )


MessageId=
Severity=Informational
SymbolicName=MM_DEFAULTMESSAGE
Language=English
%1
.


MessageId=
Severity=Success
SymbolicName=CAT_MMGUARD
Language=English
MMGuard
.

MessageId=
Severity=Success
SymbolicName=CAT_MMCLIENT
Language=English
MMClient
.

MessageId=
Severity=Success
SymbolicName=CAT_JOBHANDLER
Language=English
JobHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_MSGHANDLER
Language=English
MsgHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_STORAGEHANDLER
Language=English
StorageHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_TIMEHANDLER
Language=English
TimeHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_TXNHANDLER
Language=English
TXNHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_WSCHANDLER
Language=English
WSCHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_LXHANDLER
Language=English
LXHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_CIHANDLER
Language=English
CIHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_APPLICATION
Language=English
Application
.

MessageId=
Severity=Success
SymbolicName=CAT_WSCRPCHANDLER
Language=English
WSCRPCHandler
.

MessageId=
Severity=Success
SymbolicName=CAT_MMSETUP
Language=English
MMSetup
.


