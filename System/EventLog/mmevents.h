//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//
#define STATUS_SEVERITY_WARNING          0x2
#define STATUS_SEVERITY_SUCCESS          0x0
#define STATUS_SEVERITY_INFORMATIONAL    0x1
#define STATUS_SEVERITY_ERROR            0x3


//
// MessageId: MM_DEFAULTMESSAGE
//
// MessageText:
//
//  %1
//
#define MM_DEFAULTMESSAGE                0x40000001L

//
// MessageId: CAT_MMGUARD
//
// MessageText:
//
//  MMGuard
//
#define CAT_MMGUARD                      0x00000002L

//
// MessageId: CAT_MMCLIENT
//
// MessageText:
//
//  MMClient
//
#define CAT_MMCLIENT                     0x00000003L

//
// MessageId: CAT_JOBHANDLER
//
// MessageText:
//
//  JobHandler
//
#define CAT_JOBHANDLER                   0x00000004L

//
// MessageId: CAT_MSGHANDLER
//
// MessageText:
//
//  MsgHandler
//
#define CAT_MSGHANDLER                   0x00000005L

//
// MessageId: CAT_STORAGEHANDLER
//
// MessageText:
//
//  StorageHandler
//
#define CAT_STORAGEHANDLER               0x00000006L

//
// MessageId: CAT_TIMEHANDLER
//
// MessageText:
//
//  TimeHandler
//
#define CAT_TIMEHANDLER                  0x00000007L

//
// MessageId: CAT_TXNHANDLER
//
// MessageText:
//
//  TXNHandler
//
#define CAT_TXNHANDLER                   0x00000008L

//
// MessageId: CAT_WSCHANDLER
//
// MessageText:
//
//  WSCHandler
//
#define CAT_WSCHANDLER                   0x00000009L

//
// MessageId: CAT_LXHANDLER
//
// MessageText:
//
//  LXHandler
//
#define CAT_LXHANDLER                    0x0000000AL

//
// MessageId: CAT_CIHANDLER
//
// MessageText:
//
//  CIHandler
//
#define CAT_CIHANDLER                    0x0000000BL

//
// MessageId: CAT_APPLICATION
//
// MessageText:
//
//  Application
//
#define CAT_APPLICATION                  0x0000000CL

//
// MessageId: CAT_WSCRPCHANDLER
//
// MessageText:
//
//  WSCRPCHandler
//
#define CAT_WSCRPCHANDLER                0x0000000DL

//
// MessageId: CAT_MMSETUP
//
// MessageText:
//
//  MMSetup
//
#define CAT_MMSETUP                      0x0000000EL

