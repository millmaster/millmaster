unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Consts, EventLog, ExtCtrls, Spin;

type
  TForm1 = class(TForm)
    Button1: TButton;
    Label1: TLabel;
    Event: TRadioGroup;
    Edit1: TEdit;
    System: TRadioGroup;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  LoepfeGlobal;

{$R *.DFM}


procedure TForm1.Button1Click(Sender: TObject);
var
  szStrings: Array[1..50] of Byte;
begin
  with TEventLogWriter.Create('MillMaster', '.', TSubSystemTyp(System.ItemIndex), 'TestText: ', True) do begin
    if not Write(TEventType(Event.ItemIndex), Edit1.Text) then
      Label1.Caption := GetLastErrorText;
    StrPCopy(@szStrings, Edit1.Text);
    if not WriteBin(TEventType(Event.ItemIndex), Edit1.Text, @szStrings, Length(Edit1.Text)) then
      Label1.Caption := GetLastErrorText;
    Free;
  end;
end;

end.
