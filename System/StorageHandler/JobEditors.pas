(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobEditors.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.10.1998  0.00  Mg  | Datei erstellt
| 19.04.1998  1.00  Mg  | Transactions in Spindle data acquisition inserted.
| 15.06.1999  1.01  Mg  | GetExpSpdData... inserted.
| 12.07.1999  1.02  Mg  | UnDo jobs for spindle data implemented.
| 02.08.1999  1.10  Mg  | Changes for JobQueue. StoreX to Editor.
| 17.09.1999  1.11  Mg  | UpdateOfProdGroupState in SpindleDataEditors and UndoEditor inserted
| 30.09.1999  1.12  Mg  | Select of available Data into 6 Queries splitted
| 27.01.2000  1.13  Mg  | CorrectData in ShiftDataEditors inserted
| 02.02.2000  1.14  Mg  | TUnDoGetZESpdDataEditor.ProcessJob : if IDs = 0 then Exit inserted
| 15.02.2000  1.15  Mg  | TGenExpShiftDataEditor.FillUpList : Warning in Log File
|                       | if no Fragshifts available.
| 28.02.2000  1.16  Mg  | TSpdOfflimitsEditor.ProcessJob : Save Average always
|                       | No Offlimit generation for System ProdGrps
|                       | Hide of Spindles inserted
| 15.03.2000  1.17  Mg  | BugFix : Wrong initialized Property IntID in UpdateProdGrpStateTable -> new parameter
|                       | in case of Exp Editors
| 16.03.2000  1.18  Mg  | TGetSettingsAllGroupsEditor implemented
| 27.04.2000  1.03  Mg  | TBaseGetExpZESpdData: SFI inserted
| 05.05.2000  1.04  Mg  | TProdGrpUpdateEditor : ThreadCnt inserted
| 06.06.2000  1.05  Mg  | TBaseZESpdDataEditor.ProcessJob : Wait 2 seconds (old=1 second)
| 11.07.2000  1.06  Mg  | TBaseGetExpZESpdDataEditor.FillUpIntervalList : no exception if no data events available
| 21.07.2000  1.07  Mg  | TDBDumpEditor.ProcessJob : backup of log inserted to truncate the log
| 04.09.2000  1.08  Mg  | TSendMsgToApplEditor : Port and Computername in log
| 27.11.2000  1.09  Mg  | TMachType in TAWEMachType converted
| 08.03.2001  1.09  khp | TGenQOfflimitEditor Alarmierung erweitert um Printer und Screen
| 21.03.2001  1.10  nue | TMaAssChangedEditor Error adds no more added to broadcast msg (cause of msg length>425Byte)
| 26.03.2001  1.11  khp | TGenDWDataeditor,TGenQOfflimiteditor Parameterliste um Params erweitert bzw.
|                       | Construktor eingefuegt. Unmittelbar nach TGenDWDataeditor wird Event
|                       | Shiftchange fuer den Automatischen Printout mit der Floor ausgeloest}
| 10.04.2001  1.12  khp | TGetExpDataStopZESpdEditor.ProcessJob Rollback eingefuegt.
|                       | Alarmierung aktiv!
| 23.04.2001  1.13  khp | Testmeldung in   TGenExpShiftDataEditor.FillUpList
| 07.06.2001  1.14  khp | TMaAssChangedEditor ersetzt durch TSendMsgToClients,
|                       | ccMMApplMsg :(ApplMsg: TApplMsgRec);in TMMClientRec angepasst
|                       | Nur Event Q-Offlimit Alarm an MMClient
| 12.06.2001  1.15  khp | GetZESpdDataEditor,TGetExpZESpdDataEditor,TGetExpDataStopZESpdEditor,TBaseGenShiftDataEditor
|                       | CachedUpdates := False: CachedUpdates der BDE kein Vorteil auf Server, bis zu 30% langsamer
| 05.07.2001  1.16  nue | Fetch Backup-path for Database Dumps from registry.
| 24.07.2001  1.17  khp | Transaction in TSpdOfflimitsEditor.ProcessJob entfernt,
|                       | da StoreAverages InsertSQL aufruft welche mittels einer Transaction die Tabelle
|                       | t_machine_offlimit_average with ( tablockx holdlock) sperrt, was zu deadlocks fuehren kann.
| 31.01.2002  1.18  khp | Meldung Exp Spindledata acquisition... nur noch im Debugmode
| 01.02.2002  1.19  khp | GetLenToWeightFactor wird nicht mehr benoetigt da auf dem System immer mit NM gerechnet wird
|                       | In TProdGrpAssistant.GetYarnCnt 'fYarnCnt DIV cYarnCntFactor' durch 'fYarnCnt / cYarnCntFactor' ersetzt
| 28.02.2002  1.20  khp | Garnnummer ist neu auf der Datenbank float Nm, im Ze-Sack immer Garnnummer mit Garneinheit verwenden.
| 13.03.2002  1.21  khp | TBaseGenShiftDataEditor.CorrectData: tWa,tOp und tRun werden gerundet.
| 20.03.2002  1.22  nue | In TGenDWDataEditor.ProcessJob, execution of query cDelMachOfflimitsAverUnreferenced added.
| 21.03.2002  1.23  khp | In ProdGrpUpdateEditor function UpdateLen neues property LotSlip
| 11.04.2002  1.23  Wss | TSpdOfflimitsEditor.ProcessJob: wenn AverageOn aber die Zeit seit start
                          der Prodgruppe noch nicht erreicht (c_average_time) wird AverageOn auf False
                          gesetzt, damit der FixBorder trotzdem durchlaeuft
| 06.06.2002  1.23  Wss | TProdGrpUpdateEditor: Partie-Korrektur ohne Fadenzahl!! LoepfeGarnNr = Endprodukt
| 05.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 07.11.2002        Wss | - MaschinenOfflimit an neue Konfiguration angepasst
                          - Daten f�r RedLight aus den 3 Offlimit Tabellen entfernt
                          - % Settings f�r tLSt und LSt entfernt
| 15.11.2002  1.41  Wss | Enumeration amMaAssChanged -> amMachineStateChange
| 18.11.2002  1.42  khp | GenDWData: Update Longterm database with sp usp_longterm_week
| 27.11.2002  1.43  nue | Run extended stored procedure xp_SendMMMachStateChanged in DB master after
                          TProdGrpUpdateEdit, TGenDWDataEditor: Broadcast f�r Floor wird noch ausgef�hrt
| 04.12.2002  1.43  Wss | TSendMsgToClient wird nun ErrorInformation Property im Fehlerfall ausgegeben
| 11.12.2002        Wss | In ReadSpindleData Berechnung f�r Eff darf nicht direkt im Query
                          erfolgen: Berechnung nun beim Auslesen der Daten mit �berpr�fung
| 11.12.2002        Wss | In TSpdOfflimitsEditor.ProdGrpProducedInActInt fehler, wenn kein Record
                          vorhanden war behoben mit FindFirst!!
| 17.12.2002        Nue | Command.CommandTimeout := (cDefJobTimeout-cDefOffset) div 1000; In TDBDumpEditor.ProcessJob
| 18.12.2002        Nue | No further LogDump for MM_Winding.
| 05.02.2003  1.44  khp | Check mit mMMParam.IsComponentLongterm ob Longtermpackage installiert ist
| 17.06.2003        Wss | TDelIntEditor: l�schen von Intervallen nun vollst�ndig im
                          StorageHandler und nicht mehr im TimeHandler wegen Timeouts beim
                          Aufstarten des MM. Es wird eine Liste von IntervalIDs geholt, welche
                          alle einzeln gel�scht werden, so dass jeder Delete eine eigene
                          Transaktion ist und keine Timeouts mehr ablaufen (ADO)
| 25.09.2003    Nue/Wss | Bug beim generieren von Longterm behoben. Am NativeAdoQuery wurde
                          keine Connection zugewiesen, wodurch das Query nicht funktionieren konnte.
| 05.01.2004        Lok | Einbau neuer QOfflimit.
|                       |   Der QOfflimit wird neu �ber COM aufgerufen und erledigt die Benachrichtigung,
|                       |   und den Serverseitigen Ausdruck selber ( Aufruf in 'TGenQOfflimitEditor.ProcessJob').
| 06.01.2004        Nue | Command.CommandTimeout := cDefOffset*5; bei Speicherung Longterm.
| 19.01.2004        Wss | TGenDWDataEditor: Autoprint hat kein QOfflimit event mehr -> code angepasst
| 22.03.2004        Wss | Anpassungen an neue IPC mit dynamischem JobBuffer
| 23.03.2005        Wss | TSaveMaConfigToDBEditor: MaConfigXML wird wiederum an MaConfig geschickt und nicht direkt gespeichert
| 13.04.2005        Wss | TSpdOfflimitsEditor.CheckOfflimitRecords: Tabelle t_spindle wird nicht mehr ben�tigt
| 09.09.2005        Lok | Aufrufe von Autoprint und QOfflimit mit Fehlermeldungen versehen, wenn der OLE Aufruf nicht klappt
| 05.10.2005        Wss | Generell: Param AsDateTime mit AdoType := atDate erg�nzt
| 23.01.2006    Lok/Wss | TDelShiftByTimeEditor.ProcessJob: Query Timeout erh�ht
|                       |    Timeout erh�hen, da Trigger infolge Subqueries l�nger dauern kann (L�schen von Partien ohne Daten
| 30.03.2006        Nue | TDelShiftByTimeEditor.ProcessJob: Query Timeout erh�ht (von 2 auf 9min)
| 20.06.2006        Nue | Methode TBaseDataEditor.CheckInteger ausgeklammert, wird nirgends gebraucht!?
|                       | CheckLongWordToInt-Aufrufe eingef�gt.
| 05.09.2006        Wss | TBaseSpdDataEditor.GetSpindleData() Imperfektionen nur berechnen,
                          wenn diese nicht von der Maschine direkt kommen.
| 22.11.2007        Nue | Schlupfkorrekturberechnung: Queries cUpdateSpindleDataLen und cUpdateShiftDataLen angepasst,
|                       | weil verm.ADO einen Fehler macht: update t_x set c_len=c_len*Factor Wenn c_len vom Typ INT ist
|                       | und Factor vom Typ Double, wird nach ADO (Profiler) der Factor auf einen INT-Wert gesetzt???
| 26.02.2008        Nue | MM5.04: Bedingung f�r die Berechnung der IPI-Werte auf die L�ngenfelder ausgedehnt. (In TBaseSpdDataEditor.GetSpindleData)
|=========================================================================================*)

{Pendenzen :

}
unit JobEditors;
interface
uses
  Windows, SysUtils, classes, LoepfeGlobal, Mailslot,
  DataPoolClass, mmEventLog, IPCClass, BaseGlobal, MMBaseClasses, MMUGlobal,
  YMParaDef, MMAlert,
  ADODBAccess, SettingsReader,
  EditorQueries, EditorServiceClasses, BaseThread, ADODB_TLB, XMLDef;

const
  cBaseData_qry     = cPrimaryQuery; // to identificate the Query containing Base Data
  cClassData_qry    = cPrimaryQuery + 1; // to identificate the Query containing Base Data

type
//............................................................................
  TGetSettingsAllGroupsEditor = class(TBaseDataPoolLineEditor)
  private
    mClientWriter: TIPCClient;
  public
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function Init: boolean; override;
    function ProcessJob: TErrorRec; override;
  end;
//............................................................................
  TBaseDataEditor = class(TBaseDataPoolLineEditor) // Base class for getting spindledata
  private
    fMachNetAssistant: TMachNetAssistant;
    fDBDataAssistant: TDBDataAssistant;
  protected
    function GetMachID: Word; virtual; abstract;
//    function CheckInteger(aPar: longword; aParTyp: ShortString): integer;
    property DBDataAssistant: TDBDataAssistant read fDBDAtaAssistant write fDBDataAssistant;
    property MachNetAssistant: TMachNetAssistant read fMachNetAssistant write fMachNetAssistant;
  public
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TBaseSpdDataEditor = class(TBaseDataEditor)
  private
    mSpindleData: TMMDataRec;
    fImpAssistant: TIPAssistant;
  protected
    function CheckParams: boolean; virtual;
    function GetProdID: longint; virtual; abstract;
    function GetSpdFirst: Byte; virtual; abstract;
    function GetSpdLast: Byte; virtual; abstract;
    function GetSpdID(aJobRec: PJobRec): Word; virtual; abstract; // Extracts the spindle ID from the JobRec
    function GetBadDataFlag: boolean; virtual; abstract;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec); virtual; abstract;
    procedure InitQueryByClassFields(aQuery: TNativeADOQuery); // can raise an exception
    procedure GetSpindleData(aSpindleID: Word);
    property ImpAssistant: TIPAssistant read fImpAssistant;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
{
  TGetZEGrpDataEditor = class(TBaseSpdDataEditor)
  private
    mSpdClientData: POneGrpDataRec;
    mPosition: Word;
    mWriter: TIPCClient;
  protected
    function GetMachTyp: TAWEMachType; override;
    function GetMachID: Word; override;
    function GetProdID: longint; override;
    function GetSpdFirst: Byte; override;
    function GetSpdLast: Byte; override;
    function GetSpdID(aJobRec: PJobRec): Word; override;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdDataArr: TSpdDataArr); override;
    function GetBadDataFlag: boolean; override;
  public
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
{}
//..............................................................................
  TBaseZESpdDataEditor = class(TBaseSpdDataEditor)
  private
    fIntID: Word;
    fFragShiftID: longint;
  protected
    procedure UpdateProdGrpStateTable(aIntID: integer; aFragshiftID: integer);
    function GetIntID: Word; virtual; abstract;
    function GetFragShiftID: longint; virtual; abstract;
//wss    procedure ProcessBaseZESpdData; virtual; abstract;
    procedure StoreSpindleData(aSpindleID: Word);
    property IntID: Word read fIntID;
    property FragShiftID: longint read fFragShiftID;
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGetZESpdDataEditor = class(TBaseZESpdDataEditor)
  protected
    function GetMachID: Word; override;
    function GetProdID: longint; override;
    function GetSpdFirst: Byte; override;
    function GetSpdLast: Byte; override;
    function GetSpdID(aJobRec: PJobRec): Word; override;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec); override;
    function GetIntID: Word; override;
    function GetFragShiftID: longint; override;
    function GetBadDataFlag: boolean; override;
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGetDataStopZESpdEditor = class(TBaseZESpdDataEditor)
  protected
    function GetMachID: Word; override;
    function GetProdID: longint; override;
    function GetSpdFirst: Byte; override;
    function GetSpdLast: Byte; override;
    function GetSpdID(aJobRec: PJobRec): Word; override;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec); override;
    function GetIntID: Word; override;
    function GetFragShiftID: longint; override;
    function GetBadDataFlag: boolean; override;
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  PExpIntRec = ^TExpIntRec;
  TExpIntRec = record
    IntervalID: integer;
    FragshftID: integer;
    DataEventLen: integer; // Minutes
  end;
  TBaseGetExpZESpdDataEditor = class(TBaseZESpdDataEditor)
  private
    mMaxRecoveryDataEvents: integer;
    mActDataEvent: TDateTime;
    mLastOKDataEvent: TDateTime;
    mSpdDBBuffer: TMMDataRec;
    mIntervalList: TList;
    mTotDataTime: integer;
    mDataEvent: TDataEventAssistant;
    procedure InitDataEvents;
    procedure InitMaxRecoveryDataEvents;
    procedure FillUpIntervalList();
    procedure InitDataPart(aDBDataRec: PMMDataRec; aPartOfData: single); // Prozentualer Anteil von mSpdDBRec in aDBDataRec abfuellen
  protected
    function GetLastStoredFragID: integer; virtual; abstract;
    function GetLastStoredIntID: Word; virtual; abstract;
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGetExpZESpdDataEditor = class(TBaseGetExpZESpdDataEditor)
  protected
    function GetMachID: Word; override;
    function GetProdID: longint; override;
    function GetSpdFirst: Byte; override;
    function GetSpdLast: Byte; override;
    function GetSpdID(aJobRec: PJobRec): Word; override;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec); override;
    function GetIntID: Word; override;
    function GetFragShiftID: longint; override;
    function GetBadDataFlag: boolean; override;
    function GetLastStoredFragID: integer; override;
    function GetLastStoredIntID: Word; override;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    function ProcessJob: TerrorRec; override;
  end;
//..............................................................................
  TGetExpDataStopZESpdEditor = class(TBaseGetExpZESpdDataEditor)
  protected
    function GetMachID: Word; override;
    function GetProdID: longint; override;
    function GetSpdFirst: Byte; override;
    function GetSpdLast: Byte; override;
    function GetSpdID(aJobRec: PJobRec): Word; override;
    procedure ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec); override;
    function GetIntID: Word; override;
    function GetFragShiftID: longint; override;
    function GetBadDataFlag: boolean; override;
    function GetLastStoredFragID: integer; override;
    function GetLastStoredIntID: Word; override;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    function ProcessJob: TerrorRec; override;
  end;
//..............................................................................
  TBaseGenShiftDataEditor = class(TBaseJobEditor)
  private
    mClassCutID: integer;
    mSiroCutID: integer;
    mClassUncutID: integer;
    mSiroUncutID: integer;
    mSpliceCutID: integer;
    mSpliceUncutID: integer;
    mProdGrpDataRec: TMMDataRec;
    mShiftDataAssistant: TDBDataAssistant;
    function AddSpindleData: boolean;
    function AddProdGrpData: boolean;
    function GetFragInfo(aFragID: integer; var aFragshiftStart: TDateTime): boolean;
    function InsertGrpBaseData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean;
    function InsertGrpClassCutData: boolean;
    function InsertGrpClassUncutData: boolean;
    function InsertGrpSpliceCutData: boolean;
    function InsertGrpSpliceUncutData: boolean;
    function UpdateGrpBaseData(aProdID: integer; aFragShiftID: integer): boolean;
    function UpdateGrpClassCutData: boolean;
    function UpdateGrpClassUncutData: boolean;
    function UpdateGrpSpliceCutData: boolean;
    function UpdateGrpSpliceUncutData: boolean;
    function FragShiftAvailable(aProdID: integer; aFragShiftID: integer; var aAvailable: boolean): boolean;
    procedure CheckAvailabilityOfClassFieldsByDataBase;
    function InsertData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean;
    function InsertGrpSiroCutData: boolean;
    function InsertGrpSiroUncutData: boolean;
    function UpdateData(aProdID: integer; aFragShiftID: integer): boolean;
    function UpdateGrpSiroCutData: boolean;
    function UpdateGrpSiroUncutData: boolean;
    function UpdateProdGrpState(aProdID, aFragID, aIntID: integer): boolean;
  protected
    procedure StartTransaction;
    procedure CommitTransaction;
    procedure RollBack;
    function CorrectData(aNumSpindles: integer): boolean;
    function GetProdGrpData(aProdID: integer; aIntervallID: Word; aFragShiftID: integer): boolean; // Stores the Data in the mProdGrpDataRec
    function StoreProdGrpData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean; // stores the Data from mProdGrpDataRec on the database
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    destructor Destroy; override;
  end;
//..............................................................................
  TGenShiftDataEditor = class(TBaseGenShiftDataEditor)
  private
    mFragStart: TDateTime;
  protected
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  PFragIntRec = ^TFragIntRec;
  TFragIntRec = record
    FragID: integer;
    IntList: TList;
  end;
  TGenExpShiftDataEditor = class(TBaseGenShiftDataEditor)
  private
    mFragList: TList;
    function FillUpList: boolean;
  public
    { TODO -cUmbau : Konstruktor virtuell (vorher statisch) }
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGenDWDataEditor = class(TBaseJobEditor)
  private
    mMMParam: TMMSettingsReader;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aMMParam: TMMSettingsReader); reintroduce;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGenExpDWDataEditor = TGenDWDataEditor;
//..............................................................................
  TTestEditor = class(TBaseJobEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    destructor Destroy; override;
    function Init: boolean; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TSendMsgToClients = class(TBaseJobEditor)
  private
    mBroadcaster: TBroadcaster;
    mMMParam: TMMSettingsReader;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aMMParam: TMMSettingsReader); reintroduce;
    destructor Destroy; override;
    function Init: boolean; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TSpdOfflimitsEditor = class(TBaseJobEditor)
  private
    mOfflimitAssistant: TMachineOfflimitAssistant;
    mProdGrpAssistant: TProdGrpAssistant;
    mSpindleOfflimitDataRec: TSpindleOfflimitDataRec; // contains new offlimit data
    mOfflimitRec: TSpindleOfflimitRec; // contains old offlimit data and offlimit times
    mIntervallLen: integer;
    mAverageID: integer;
    mAverageStored: boolean;
    procedure InitMembers; // can raise an exception
    function ProdGrpStartedInActInt: boolean; // can raise an exception
    function ProdGrpProducedInActInt: boolean; // can raise an exception
    function IntervalsAvailableForAverageCalc {(aProdGrpDepend: boolean)}: boolean; // can raise an exception
    function CheckOfflimitRecords: Boolean;
    function OpenAllSpindleData: Boolean; // stores all spindledate in the 2. Query, can raise an exception
    function ReadSpindleData: integer; // stores the data of the actual spindle in the offlimitrecord, can raise an exception
    function SettingsAvailable: boolean;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGenQOfflimitEditor = class(TBaseJobEditor)
  private
    mMMParam: TMMSettingsReader;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aMMParam: TMMSettingsReader); reintroduce;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TDelIntEditor = class(TBaseJobEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TDelShiftByTimeEditor = class(TBaseJobEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TSendMsgToApplEditor = class(TBaseJobEditor)
  private
    mWriter: TIPCClient;
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TProdGrpUpdateEditor = class(TBaseJobEditor)
  private
    mProdGrpAssistant: TprodGrpAssistant;
    mMMParam: TMMSettingsReader;
//alt bis 22.11.07 Nue    function GetSlipCorrFactor(aNewSlip: Real; aOldSlip: Real): Double;
    procedure UpdateLen(aSlipNew: Integer; aSlipOld: Integer);
    procedure UpdateWeightByLen(aFactor: Double);
//wss    procedure UpdateWeightByFactor(aFactor: Double);
    procedure UpdateProdGrpSlip(aSlip: integer);
    procedure UpdateProdGrpYarnCnt(aYarnCnt: Double);
//wss    procedure UpdateProdGrpThreadCnt(aThreadCnt: integer);
    procedure UpdateProdGrpColor(aColor: integer);
    procedure UpdateProdGrpName(aName: string);
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aMMParam: TMMSettingsReader); reintroduce;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TDiagnosticEditor = class(TBaseDataPoolEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); reintroduce;
    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TGetNodeListEditor = class(TBaseDataPoolEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); reintroduce;    destructor Destroy; override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TUnDoGetZESpdDataEditor = class(TBaseJobEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    function ProcessJob: TErrorRec; override;
  end;
//..............................................................................
  TDBDumpEditor = class(TBaseJobEditor)
  private
  public
    constructor Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter); override;
    function ProcessJob: TErrorRec; override;
  end;

//............................................................................
  TSaveMaConfigToDBEditor = class(TBaseDataPoolLineEditor)
  private
    mClientWriter: TIPCClient;
  public
    constructor Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool); override;
    destructor Destroy; override;
    function Init: boolean; override;
    function ProcessJob: TErrorRec; override;
  end;

//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,

  MsgGlobal, TypInfo, comobj, XMLGlobal;
//------------------------------------------------------------------------------
 { TGetSettingsAllGroupsEditor }
//------------------------------------------------------------------------------
constructor TGetSettingsAllGroupsEditor.Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  mClientWriter := TIPCClient.Create(aJob.GetSettingsAllGroups.ComputerName, aJob.GetSettingsAllGroups.Port);
end;
//------------------------------------------------------------------------------
destructor TGetSettingsAllGroupsEditor.Destroy;
begin
  mClientWriter.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TGetSettingsAllGroupsEditor.Init: boolean;
begin
  Result := inherited Init;
  if Result then begin
    Result := mClientWriter.Connect;
    if not Result then
      fError := SetError(mClientWriter.Error, etNTError, 'TGetSettingsAllGroupsEditor.Init failed. ');
  end;
end;
//------------------------------------------------------------------------------
function TGetSettingsAllGroupsEditor.ProcessJob: TErrorRec;
var
  i: integer;
  xSize: Integer;
  xPJobRec: PJobRec;
  xPResponseRec: PResponseRec;
  xXMLList: TStringList;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    WriteLogDebug(Format('GetSettingsAllGroups received. Num Grps = %d', [DataList.Count]));
    // Es hat nur soviele Elemente im DataPool, wie es auch g�ltige Spindelbereiche bei der
    // Abfrage im MsgHandler war (0 < First/Last < FF).
    xPResponseRec := Nil;
    xXMLList      := TStringList.Create;
    try
      // Zuerst die XMLSettings behandeln, damit wir die dynamische Gr�sse ermitteln k�nnen
      for i:=0 to DataList.Count-1 do begin
        xPJobRec := DataList.Items[i];
        // Schreibe die XMLSettings als "Settings0=XMLData, Settings1=XMLData" in die StringList
        xXMLList.Values[Format(cXMLSettingIniName, [i])] := StrPas(xPJobRec^.GetSettingsAllGroups.SettingsRec.XMLData);
      end; //for

      // Anhand der Settingsgr�sse den Speicher reservieren und die Daten zusammensetzen
      xSize                 := Length(xXMLList.CommaText);
      xPResponseRec         := AllocMem(SizeOf(TResponseRec) + xSize);
      xPResponseRec^.MsgTyp := rmGetSettingsOK;

      // komplette XMLSettings hinein kopieren
      System.Move(PChar(xXMLList.CommaText)^, xPResponseRec^.XMLSettingsCollection.XMLData, xSize);

      // Erst jetzt k�nnen die Recordfelder abgef�llt werden
      // Wenn nur z.B. 3 Gruppen angefragt wurden, dann werden auch nur die ersten 3 Eintr�ge
      // vom Array verwendet
      for i:=0 to DataList.Count-1 do begin
        xPJobRec                                            := DataList.Items[i];
        xPResponseRec^.MachineID                            := xPJobRec^.GetSettingsAllGroups.MachineID;
        xPResponseRec^.XMLSettingsCollection.SettingsArr[i] := xPJobRec^.GetSettingsAllGroups.SettingsRec;
      end;

      // komplette Meldung verschicken
      if not mClientWriter.Write(PByte(xPResponseRec), SizeOf(TResponseRec) + xSize) then
        raise Exception.Create('Write failed. ' + FormatErrorText(mClientWriter.Error));
    finally
      FreeMem(xPResponseRec);
      xXMLList.Free;
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TGetSettingsAllGroupsEditor.ProcessJob failed. ' + e.Message);
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TBaseDataEditor }
//------------------------------------------------------------------------------
constructor TBaseDataEditor.Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  fMachNetAssistant := TMachNetAssistant.Create(mJob.NetTyp, GetMachID{, GetMachTyp});
  fDBDataAssistant := TDBDataAssistant.Create(aEventLog);
end;
//------------------------------------------------------------------------------
destructor TBaseDataEditor.Destroy;
begin
  fMachNetAssistant.Free;
  fDBDataAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
//function TBaseDataEditor.CheckInteger(aPar: longword; aParTyp: ShortString): integer;
//begin
//  if aPar <= LongWord(MAXINT) then begin
//    Result := aPar;
//  end
//  else begin
//    Result := MAXINT;
//    WriteLog(etWarning, 'Value to high for integer type. Parameter : ' + aParTyp);
//  end;
////  if aPar > high(integer) then begin
////    Result := high(integer);
////    WriteLog(etWarning, 'Value to high for integer type. Parameter : ' + aParTyp);
////  end
////  else begin
////    Result := aPar;
////  end;
//end;
//------------------------------------------------------------------------------
function TBaseDataEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    MachNetAssistant.Init(mDB.Query[cPrimaryQuery]);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TBaseDataEditor.ProcessJob init of MachNet... failed. ' + e.Message);
      Result := Error;
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TBaseSpdDataEditor }
//------------------------------------------------------------------------------
constructor TBaseSpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDatapool);
  fImpAssistant := TIPAssistant.Create(MachNetAssistant.ProdGrpAssistant);
end;
//------------------------------------------------------------------------------
destructor TBaseSpdDataEditor.Destroy;
begin
  fImpAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TBaseSpdDataEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    MachNetAssistant.ProdGrpAssistant.ProdGrpID := GetProdID;
  except
    fError := MachNetAssistant.ProdGrpAssistant.Error;
    fError.Msg := 'TBaseSpdData...ProcessData failed.' + fError.Msg;
    Result := Error;
    WriteLog(etError, FormatMMErrorText(Error));
    Exit;
  end;
  if not CheckParams then begin
    Result := fError;
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
function TBaseSpdDataEditor.CheckParams: boolean;
begin
  Result := True;
  if MachNetAssistant.ProdGrpAssistant.MachineID <> GetMachID then begin
    Result := False;
    fError.Msg := 'Parmameter check failed. GrpInfo.MachID: ' +
      IntToStr(MachNetAssistant.ProdGrpAssistant.MachineID) + ' Job.MachID: ' +
      IntToStr(GetMachID);
  end;
  if Result then
    if MachNetAssistant.ProdGrpAssistant.SpindFirst <> GetSpdFirst then begin
      Result := False;
      fError.Msg := 'Parmameter check failed. GrpInfo.SpindFirst: ' +
        IntToStr(MachNetAssistant.ProdGrpAssistant.SpindFirst) + ' Job.SpindFirst: ' +
        IntToStr(GetSpdFirst);
    end;
  if Result then
    if MachNetAssistant.ProdGrpAssistant.SpindLast <> GetSpdLast then begin
      Result := False;
      fError.Msg := 'Parmameter check failed. GrpInfo.SpindLast: ' +
        IntToStr(MachNetAssistant.ProdGrpAssistant.SpindLast) + ' Job.SpindLast: ' +
        IntToStr(GetSpdLast);
    end;
  if not Result then begin
    fError.ErrorTyp := etMMError;
    fError.Error := ERROR_INVALID_PARAMETER;
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseSpdDataEditor.GetSpindleData(aSpindleID: Word);
var
  i: integer;
  xPJobRec: PJobRec;
  xSpdFound: boolean;
begin
  xSpdFound := False;
    // get the spindle data from the list
  for i:=0 to DataList.Count-1 do begin
    xPJobRec := DataList.Items[i];
    if GetSpdID(xPJobRec) = aSpindleID then begin
      xSpdFound := True;
      ReadSpdDataFromDataPool(xPJobRec, mSpindleData);
      Break;
    end;
  end;
  if not xSpdFound then begin
    fError := SetError(ERROR_INVALID_PARAMETER, etMMError, Format('GetSpindleData failed : Spindle Nr. %d in list not found. ProdID = %d', [aSpindleID, GetProdID]));
    raise EMMException.Create(fError.Msg);
  end;
  
  try
    // Korrigiert und Berechnet noch gewisse Daten
    MachNetAssistant.ProcessSpindleData({@mDataArr, }@mSpindleData);
  except
    fError := MachNetAssistant.Error;
    fError.Msg := 'GetSpindleData failed by MachNetAs... . ' + fError.Msg;
    raise EMMException.Create(Error.Msg);
  end;

  // Imperfektionen nur berechnen, wenn diese nicht von der Maschine direkt kommen.
  // LZE: BaseDataSize = 134Byte. Ab 166Byte sind die 8 IPI Datenwerte vorhanden
  // WSC: DataItem 'mm_imp_data' nicht vorhanden
  // ZE:  Immer berechnen
  with mSpindleData.Imp do begin
    if (Neps = 0) and (Thick = 0) and (Thin = 0) and (Small = 0)
//Neu check auch auf L�ngen-Felder, da es auf LZE vorkommen konnte, dass die Diameter-Felder alle 0 waren. Nue:26.02.08
       and (I2_4 = 0) and (I4_8 = 0) and (I8_20 = 0) and (I20_70 = 0) then begin   //Neu check auch auf L�ngen-Felder, da es auf LZE vorkommen konnte, Nue
      if not ImpAssistant.CalcImperfections(mSpindleData.Imp, mSpindleData.ClassFieldRec.ClassUncutField) then begin
        fError := ImpAssistant.Error;
        fError.Msg := 'GetSpindleData failed by ImpAs... . ' + fError.Msg;
        raise EMMException.Create(fError.Msg);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseSpdDataEditor.InitQueryByClassFields(aQuery: TNativeAdoQuery);
var
  xClassFieldRec: TClassFieldRec;
begin
  try
    xClassFieldRec := mSpindleData.ClassFieldRec;
    aQuery.Params.ParamByName('c_class_image').SetBlobData(@xClassFieldRec, sizeof(TClassFieldRec));
  except
    on e: Exception do begin
      fError := SetError(ERROR_NOT_ENOUGH_MEMORY, etNTError, 'Blob error in InitQueryByClassFields.' + e.message);
      raise EMMException.Create(Error.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------
// TGetZEGrpDataEditor
//------------------------------------------------------------------------------
{
constructor TGetZEGrpDataEditor.Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  mWriter := TIPCClient.Create(aJob.ViewZESpdData.ComputerName, aJob.ViewZESpdData.Port);
  mPosition := 0;
  mSpdClientData := nil;
end;
//------------------------------------------------------------------------------
destructor TGetZEGrpDataEditor.Destroy;
begin
  mWriter.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetMachTyp: TAWEMachType;
begin
  Result := mJob.ViewZESpdData.MachineTyp;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetMachID: Word;
begin
  Result := mJob.ViewZESpdData.MachineID;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetProdID: longint;
begin
  Result := mJob.ViewZESpdData.ProdID;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetSpdFirst: Byte;
begin
  Result := mJob.ViewZESpdData.SpindleFirst;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetSpdLast: Byte;
begin
  Result := mJob.ViewZESpdData.SpindleLast;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetSpdID(aJobRec: PJobRec): Word;
begin
  Result := aJobRec.ViewZESpdData.SpindID;
end;
//------------------------------------------------------------------------------
procedure TGetZEGrpDataEditor.ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdDataArr: TSpdDataArr);
begin
  aSpdDataArr := aJobRec.ViewZESpdData.SpdDataArr;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.GetBadDataFlag: boolean;
begin
  Result := mJob.GetDataStopZEGrp.BadData;
end;
//------------------------------------------------------------------------------
function TGetZEGrpDataEditor.ProcessJob: TErrorRec;
var
  xSpd: Word;
  xDataSize: integer;
  procedure FreeMemory;
  begin
    if mSpdClientData <> nil then try
      FreeMem(mSpdClientData, (MachNetAssistant.ProdGrpAssistant.SpindLast - MachNetAssistant.ProdGrpAssistant.SpindFirst + 1) * sizeof(TMMDataRec) + cOneGrpDataHeaderSize);
      mSpdClientData := nil;
    except
      WriteLog(etWarning, 'FreeMem failed');
    end;
  end;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  if mWriter.Connect then begin
    try
      mSpdClientData := AllocMem((MachNetAssistant.ProdGrpAssistant.SpindLast -
        MachNetAssistant.ProdGrpAssistant.SpindFirst + 1) * sizeof(TMMDataRec) + cOneGrpDataHeaderSize);
    except
      fError := SetError(ERROR_NOT_ENOUGH_MEMORY, etNTError, 'TGetOneGrp...ProcessJob Allocate of Spindle data memory failed.');
      Result := Error;
      WriteLog(etError, FormatMMErrorText(Error));
      FreeMemory;
      Exit;
    end;
    mSpdClientData.SpindleFirst := MachNetAssistant.ProdGrpAssistant.SpindFirst;
    mSpdClientData.SpindleLast := MachNetAssistant.ProdGrpAssistant.SpindLast;
    mSpdClientData.BadData := GetBadDataFlag;
    for xSpd := MachNetAssistant.ProdGrpAssistant.SpindFirst to MachNetAssistant.ProdGrpAssistant.SpindLast do begin
      try
        GetSpindleData(xSpd);
      except
        on e: Exception do begin
          fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TGetOneGrpEditor.ProcessJob : GetSpindleData failed. ' + e.Message);
          Result := Error;
          WriteLog(etError, FormatMMErrorText(Error));
          FreeMemory;
          Exit;
        end;
      end;
      mSpdClientData.Data[mPosition] := mSpdDBRec;
      inc(mPosition);
    end;
    xDataSize := (mPosition * sizeof(TMMDataRec)) + cOneGrpDataHeaderSize;
    if not mWriter.Write(PByte(mSpdClientData), xDataSize) then begin
      fError := SetError(mWriter.Error, etNTError, 'TGetOnGrpEditor.ProcessJob : Write of spindle data failed.');
      Result := Error;
      WriteLog(etError, FormatMMErrorText(Error));
//        FreeMemory;
    end;
    FreeMemory;
  end
  else begin
    fError := SetError(mWriter.Error, etNTError, 'TGetOnGrpEditor.ProcessJob : Connect of pipe failed');
    Result := Error;
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;
{}
//------------------------------------------------------------------------------
{ TBaseZESpdDataEditor }
//------------------------------------------------------------------------------
constructor TBaseZESpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
function TBaseZESpdDataEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  fIntID := GetIntID;
  fFragShiftID := GetFragShiftID;
    // the following part is to make sure all spindle data are in datapool
  if (DataList.Count <> (GetSpdLast - GetSpdFirst + 1)) then begin
      //WriteLog ( etWarning, Format ( 'Count of Spindles in DataList = %d, Count Spindles of ProdGrp = %d.',[DataList.Count, ( GetSpdLast - GetSpdFirst + 1 ) ] ) );
    Sleep(2000);
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseZESpdDataEditor.UpdateProdGrpStateTable(aIntID: integer; aFragshiftID: integer);
begin
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cUpdateProdgroupStateIntervalData;
    Params.ParamByName('c_int_id_ok').AsInteger := aIntID;
    Params.ParamByName('c_fragshift_id_ok').AsInteger := aFragShiftID;
    Params.ParamByName('c_prod_id').AsInteger := GetProdID;
    ExecSQL;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'UpdateProdGrpStateTable failed.' + e.Message);
      raise EMMException.Create(Error.Msg);
    end;
  end;

  // 19.01.2004 wss, von Schichtwechsel hierher verschoben
  // Nun noch einen Broadcast ausf�hren lassen, damit die Floor aktuallisiert werden
  with TAdoDBAccess.Create(1) do
  try
    try
      DBName := 'master';
      Init;
      Query[cPrimaryQuery].SQL.Text := 'xp_SendMMMachStateChanged';
      Query[cPrimaryQuery].Command.CommandType := adCMDStoredProc;
      Query[cPrimaryQuery].ExecSQL;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'UpdateProdGrpStateTable xp_SendMMMachStateChanged ' + e.Message);
        raise EMMException.Create(Error.Msg);
      end; // on e: Exception do begin
    end; // try except
  finally
    Free;
  end; // with TAdoDBAccess.Create(1) do try
end;
//------------------------------------------------------------------------------
procedure TBaseZESpdDataEditor.StoreSpindleData(aSpindleID: Word);
  //.....................................................
  function IDStr: string;
  begin
    Result := Format('Int=%d Frag=%d Mach=%d Prod=%d Spd=%d', [IntID, FragshiftID, MachNetAssistant.MachID, MachNetAssistant.ProdGrpAssistant.ProdGrpID, aSpindleID]);
  end;
  //.....................................................
begin
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cInsertSpindleIntervalData;
    Params.ParamByName('c_Spindle_id').AsInteger   := aSpindleID;
    Params.ParamByName('c_Machine_id').AsInteger   := MachNetAssistant.MachID;
    Params.ParamByName('c_prod_id').AsInteger      := MachNetAssistant.ProdGrpAssistant.ProdGrpID;
    Params.ParamByName('c_interval_id').AsInteger  := IntID;
    Params.ParamByName('c_fragshift_id').AsInteger := FragShiftID;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'StoreSpindleData failed (1).' + IDStr + e.message);
      raise EMMException.Create(Error.Msg);
    end;
  end;

  if not DBDataAssistant.InitQueryByBaseData(mDB.Query[cPrimaryQuery], @mSpindleData) then begin
    fError := DBDataAssistant.Error;
    fError.Msg := fError.Msg + IDStr;
    raise EMMException.Create(Error.Msg);
  end;

  // bei SpindelIntervalData wird der Klassierblock als Image gespeichert
  InitQueryByClassFields(mDB.Query[cPrimaryQuery]);
  try
    mDB.Query[cPrimaryQuery].ExecSQL;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'StoreSpindleData failed (3).' + IDStr + e.message);
      raise EMMException.Create(Error.Msg);
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TGetZESpdDataEditor }
//------------------------------------------------------------------------------
constructor TGetZESpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetMachID: Word;
begin
  Result := mJob.GetZESpdData.MachineID;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetProdID: longint;
begin
  Result := mJob.GetZESpdData.ProdID;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetSpdFirst: Byte;
begin
  Result := mJob.GetZESpdData.SpindleFirst;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetSpdLast: Byte;
begin
  Result := mJob.GetZESpdData.SpindleLast;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetSpdID(aJobRec: PJobRec): Word;
begin
  Result := aJobRec.GetZESpdData.SpindID;
end;
//------------------------------------------------------------------------------
procedure TGetZESpdDataEditor.ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec);
begin
  aSpdData := aJobRec^.GetZESpdData.SpdData;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetIntID: Word;
begin
  Result := mJob.GetZESpdData.IntID;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetFragShiftID: longint;
begin
  Result := mJob.GetZESpdData.FragID;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.GetBadDataFlag: boolean;
begin
  Result := mJob.GetZESpdData.BadData;
end;
//------------------------------------------------------------------------------
function TGetZESpdDataEditor.ProcessJob: TErrorRec;
var
  xSpd: integer;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    mDB.DataBase.StartTransaction;
    // store every single spindle data into t_spindle_interval_data
    for xSpd:=MachNetAssistant.ProdGrpAssistant.SpindFirst to MachNetAssistant.ProdGrpAssistant.SpindLast do begin
      GetSpindleData(xSpd);
      StoreSpindleData(xSpd);
      Sleep(1);
    end;
    UpdateProdGrpStateTable(IntID, FragshiftID);
    mDB.DataBase.Commit;
  except
    if mDB.DataBase.InTransaction then
      mDB.DataBase.Rollback;
    Result := Error;
    WriteLog(etError, 'ProcessJob failed : ' + FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
{ TGetDataStopZESpdEditor }
//------------------------------------------------------------------------------
constructor TGetDataStopZESpdEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetMachID: Word;
begin
  Result := mJob.GetDataStopZESpd.MachineID;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetProdID: longint;
begin
  Result := mJob.GetDataStopZESpd.ProdID;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetSpdFirst: Byte;
begin
  Result := mJob.GetDataStopZESpd.SpindleFirst;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetSpdLast: Byte;
begin
  Result := mJob.GetDataStopZESpd.SpindleLast;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetSpdID(aJobRec: PJobRec): Word;
begin
  Result := aJobRec.GetDataStopZESpd.SpindID;
end;
//------------------------------------------------------------------------------
procedure TGetDataStopZESpdEditor.ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec);
begin
  aSpdData := aJobRec^.GetDataStopZESpd.SpdData;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetIntID: Word;
begin
  Result := mJob.GetDataStopZESpd.IntID;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetFragShiftID: longint;
begin
  Result := mJob.GetDataStopZESpd.FragID;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.GetBadDataFlag: boolean;
begin
  Result := mJob.GetDataStopZESpd.BadData;
end;
//------------------------------------------------------------------------------
function TGetDataStopZESpdEditor.ProcessJob: TErrorRec;
var
  xSpd: integer;
  xErr: boolean;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  WriteLogDebug('jtGetDataStopZESpd received from JobController');
  xErr := False;
  for xSpd := MachNetAssistant.ProdGrpAssistant.SpindFirst to MachNetAssistant.ProdGrpAssistant.SpindLast do begin
    try
      GetSpindleData(xSpd);
    except
      xErr := True;
      WriteLog(etError, 'GetSpindleData failed: ' + FormatMMErrorText(Error));
    end;

    if not xErr then begin
      try
        StoreSpindleData(xSpd);
      except
        xErr := True;
        WriteLog(etError, 'StoreSpindleData failed: ' + FormatMMErrorText(Error));
      end;
    end;
    if xErr then
      Result := Error;
    xErr := False;
  end;
  try
    UpdateProdGrpStateTable(IntID, FragshiftID);
  except
    WriteLog(etError, 'ProcessJob failed: ' + FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
{ TBaseGetExpZESpdDataEditor }
//------------------------------------------------------------------------------
constructor TBaseGetExpZESpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  mIntervalList := TList.Create;
  mDataEvent := TDataEventAssistant.Create(mDB.Query[0]);
  mMaxRecoveryDataEvents := high(integer);
  fPriority := tpNormal;
end;
//------------------------------------------------------------------------------
destructor TBaseGetExpZESpdDataEditor.Destroy;
var
  xIntRec: PExpIntRec;
begin
  while (mIntervalList.Count > 0) do begin
    xIntRec := mIntervalList.Items[0];
    Dispose(xIntRec);
    mIntervalList.Delete(0);
  end;
  mIntervalList.Clear;
  mIntervalList.Free;
  mDataEvent.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TBaseGetExpZESpdDataEditor.InitDataEvents;
begin
  try
    mActDataEvent := mDataEvent.GetDataEventByID(IntID, FragShiftID);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'InitDataEvents failed : ' + e.Message);
      raise;
    end;
  end;

  try
    mLastOKDataEvent := mDataEvent.GetDataEventByID(GetLastStoredIntID, GetLastStoredFragID);
  except
    // the dataevent has hot been found in range of intervals
    // in this case take the oldest dataevent
    try
      mLastOKDataEvent := mDataEvent.GetOldestDataEvent;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'InitDataEvents (GetOldestDataEvent) failed : ' + e.Message);
        raise;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseGetExpZESpdDataEditor.InitMaxRecoveryDataEvents;
begin
  mMaxRecoveryDataEvents := 5;
end;
//------------------------------------------------------------------------------
function TBaseGetExpZESpdDataEditor.ProcessJob: TErrorRec;

begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    WriteLogDebug(Format('Exp Spindledata acquisition. MachId = %d, ProdID = %d, ActIntID = %d, ActFragID = %d, LastIntID = %d, LastFragID = %d  ',
      [GetMachID, GetProdID, IntID, FragShiftID, GetLastStoredIntID, GetLastStoredFragID]));
    InitDataEvents;
    InitMaxRecoveryDataEvents;
    FillUpIntervalList;
  except
    Result := Error;
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseGetExpZESpdDataEditor.FillUpIntervalList();
var
  xIntRec: PExpIntRec;
begin
  with mDB.Query[cPrimaryQuery] do begin
    try
      Close;
      SQL.Text := cGetExpDataEventList;
      Params.ParamByName('ActDataEvent').AsDateTime    := mActDataEvent;
      Params.ParamByName('ActDataEvent').AdoType       := adDate;
      Params.ParamByName('LastOKDataEvent').AsDateTime := mLastOKDataEvent;
      Params.ParamByName('LastOKDataEvent').AdoType    := adDate;
      Open;
      mIntervalList.Clear;
        // ADO Conform
      if EOF then // 1.06 raise EMMException.Create ( Format ( 'No DataEvents available. ActDataEvent=%s, LastOKDataEvent=%s', [DateTimeToStr ( mActDataEvent ) , DateTimeToStr ( mLastOKDataEvent ) ]));
        WriteLog(etWarning, 'No DataEvents avilable. Only accepted by Assign during data acquisition.');
      mTotDataTime := 0;
      while ((not EOF) and (mMaxRecoveryDataEvents > 0)) do begin // ADO Conform
        new(xIntRec);
        xIntRec^.IntervalID := FieldByName('c_interval_id').AsInteger;
        xIntRec^.FragshftID := FieldByName('c_fragshift_id').AsInteger;
        xIntRec^.DataEventLen := mDataEvent.GetDataEventLenByDataEvent(FieldByName('c_dataevent_start').AsDateTime);
        mTotDataTime := mTotDataTime + xIntRec^.DataEventLen;
        mIntervalList.Add(xIntRec);
        dec(mMaxRecoveryDataEvents);
        Next;
      end;
      Close;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'FillUpIntervalList failed : ' + e.Message);
        raise;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseGetExpZESpdDataEditor.InitDataPart(aDBDataRec: PMMDataRec; aPartofData: single);
var
  i: integer;
begin
  with aDBDataRec^ do begin
    Len          := Round(mSpdDBBuffer.Len * aPartOfData);
    Wei          := Round(mSpdDBBuffer.Wei * aPartOfData);
    Bob          := Round(mSpdDBBuffer.Bob * aPartOfData);
    Cones        := Round(mSpdDBBuffer.Cones * aPartOfData);
    Sp           := Round(mSpdDBBuffer.Sp * aPartOfData);
    RSp          := Round(mSpdDBBuffer.RSp * aPartOfData);
    YB           := Round(mSpdDBBuffer.YB * aPartOfData);
    CSys         := Round(mSpdDBBuffer.CSys * aPartOfData);
    LckSys       := Round(mSpdDBBuffer.LckSys * aPartOfData);
    CUpY         := Round(mSpdDBBuffer.CUpY * aPartOfData);
    CYTot        := Round(mSpdDBBuffer.CYTot * aPartOfData);
    LSt          := Round(mSpdDBBuffer.Lst * aPartOfData);
    tLStProd     := Round(mSpdDBBuffer.tLStProd * aPartOfData);
    tLStBreak    := Round(mSpdDBBuffer.tLStBreak * aPartOfData);
    tLStMat      := Round(mSpdDBBuffer.tLStMat * aPartOfData);
    tLStRev      := Round(mSpdDBBuffer.tLStRev * aPartOfData);
    tLStClean    := Round(mSpdDBBuffer.tLStClean * aPartOfData);
    tLStDef      := Round(mSpdDBBuffer.tLStDef * aPartOfData);
    Red          := Round(mSpdDBBuffer.Red * aPartOfData);
    tRed         := Round(mSpdDBBuffer.tRed * aPartOfData);
    tYell        := Round(mSpdDBBuffer.tYell * aPartOfData);
    ManSt        := Round(mSpdDBBuffer.ManSt * aPartOfData);
    tManSt       := Round(mSpdDBBuffer.tManSt * aPartOfData);
    tRun         := Round(mSpdDBBuffer.tRun * aPartOfData);
    tOp          := Round(mSpdDBBuffer.tOp * aPartOfData);
    tWa          := Round(mSpdDBBuffer.tWa * aPartOfData);
    totProdGrp   := Round(mSpdDBBuffer.totProdGrp * aPartOfData);
    twtProdGrp   := Round(mSpdDBBuffer.twtProdGrp * aPartOfData);
    CS           := Round(mSpdDBBuffer.CS * aPartOfData);
    CL           := Round(mSpdDBBuffer.CL * aPartOfData);
    CT           := Round(mSpdDBBuffer.CT * aPartOfData);
    CN           := Round(mSpdDBBuffer.CN * aPartOfData);
    CSp          := Round(mSpdDBBuffer.CSp * aPartOfData);
    CClS         := Round(mSpdDBBuffer.CClS * aPartOfData);
    COffCntPlus  := Round(mSpdDBBuffer.COffCntPlus * aPartOfData);
    LckOffCnt    := Round(mSpdDBBuffer.LckOffCnt * aPartOfData);
//    COffCnt    := Round(mSpdDBBuffer.COffCnt * aPartOfData);
//    LckOffCnt  := Round(mSpdDBBuffer.LckOffCnt * aPartOfData);
    CBu          := Round(mSpdDBBuffer.CBu * aPartOfData);
    CDBu         := Round(mSpdDBBuffer.CDBu * aPartOfData);
    UClS         := Round(mSpdDBBuffer.UClS * aPartOfData);
    UClL         := Round(mSpdDBBuffer.UClL * aPartOfData);
    UClT         := Round(mSpdDBBuffer.UClT * aPartOfData);
    CSIRO        := Round(mSpdDBBuffer.CSIRO * aPartOfData);
    LckSiro      := Round(mSpdDBBuffer.LckSiro * aPartOfData);
    CSIROCl      := Round(mSpdDBBuffer.CSIROCl * aPartOfData);
    LckSIROCl    := Round(mSpdDBBuffer.LckSIROCl * aPartOfData);
    LckClS       := Round(mSpdDBBuffer.LckClS * aPartOfData);
    CSFI         := Round(mSpdDBBuffer.CSFI * aPartOfData);
    LckSFI       := Round(mSpdDBBuffer.LckSFI * aPartOfData);
    SFI          := Round(mSpdDBBuffer.SFI * aPartOfData);
    SFICnt       := Round(mSpdDBBuffer.SFICnt * aPartOfData);
//wss: hier w�ren noch die zus�tzlichen Spectra+ und Zenit Felder. Da ExpJob ausf�llt nicht mehr n�tig
//     VCV (Nue)
    Imp.Neps     := Round(mSpdDBBuffer.Imp.Neps * aPartOfData);
    Imp.Thick    := Round(mSpdDBBuffer.Imp.Thick * aPartOfData);
    Imp.Thin     := Round(mSpdDBBuffer.Imp.Thin * aPartOfData);
    Imp.Small    := Round(mSpdDBBuffer.Imp.Small * aPartOfData);
    Imp.I2_4     := Round(mSpdDBBuffer.Imp.I2_4 * aPartOfData);
    Imp.I4_8     := Round(mSpdDBBuffer.Imp.I4_8 * aPartOfData);
    Imp.I8_20    := Round(mSpdDBBuffer.Imp.I8_20 * aPartOfData);
    Imp.I20_70   := Round(mSpdDBBuffer.Imp.I20_70 * aPartOfData);
    for i:=1 to cClassUncutFields do
      ClassFieldRec.ClassUncutField[i] := Round(mSpdDBBuffer.ClassFieldRec.ClassUncutField[i] * aPartOfData);
    for i:=1 to cClassCutFields do
      ClassFieldRec.ClassCutField[i] := Round(mSpdDBBuffer.ClassFieldRec.ClassCutField[i] * aPartOfData);
    for i:=1 to cSpUncutFields do
      ClassFieldRec.SpUncutField[i] := Round(mSpdDBBuffer.ClassFieldRec.SpUncutField[i] * aPartOfData);
    for i:=1 to cSpCutFields do
      ClassFieldRec.SpCutField[i] := Round(mSpdDBBuffer.ClassFieldRec.SpCutField[i] * aPartOfData);
    for i:=1 to cSIROUncutFields do
      ClassFieldRec.SIROUncutField[i] := Round(mSpdDBBuffer.ClassFieldRec.SIROUncutField[i] * aPartOfData);
    for i:=1 to cSIROCutFields do
      ClassFieldRec.SIROCutField[i] := Round(mSpdDBBuffer.ClassFieldRec.SIROCutField[i] * aPartOfData);
  end;
end;
//------------------------------------------------------------------------------
{ TGetExpZESpdDataEditor }
//------------------------------------------------------------------------------
constructor TGetExpZESpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetMachID: Word;
begin
  Result := mJob.GetExpZESpdData.MachineID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetProdID: longint;
begin
  Result := mJob.GetExpZESpdData.ProdID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetSpdFirst: Byte;
begin
  Result := mJob.GetExpZESpdData.SpindleFirst;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetSpdLast: Byte;
begin
  Result := mJob.GetExpZESpdData.SpindleLast;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetSpdID(aJobRec: PJobRec): Word;
begin
  Result := aJobRec.GetExpZESpdData.SpindID;
end;
//------------------------------------------------------------------------------
procedure TGetExpZESpdDataEditor.ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec);
begin
  aSpdData := aJobRec^.GetExpZESpdData.SpdData;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetIntID: Word;
begin
  Result := mJob.GetExpZESpdData.EndIntID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetFragShiftID: longint;
begin
  Result := mJob.GetExpZESpdData.EndFragID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetBadDataFlag: boolean;
begin
  Result := mJob.GetExpZESpdData.BadData;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetLastStoredFragID: integer;
begin
  Result := mJob.GetExpZESpdData.StartFragID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.GetLastStoredIntID: Word;
begin
  Result := mJob.GetExpZESpdData.StartIntID;
end;
//------------------------------------------------------------------------------
function TGetExpZESpdDataEditor.ProcessJob: TErrorRec;
var
  x: integer;
  xSpd: integer;
  xIntRec: PExpIntRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    mDB.DataBase.StartTransaction;
    for xSpd := MachNetAssistant.ProdGrpAssistant.SpindFirst to MachNetAssistant.ProdGrpAssistant.SpindLast do begin
      GetSpindleData(xSpd);
      mSpdDBBuffer := mSpindleData;
      for x := 0 to mIntervalList.Count - 1 do begin
        xIntRec := mIntervalList.Items[x];
        fIntID := xIntRec^.IntervalID;
        fFragShiftID := xIntRec^.FragshftID;
        InitDataPart(@mSpindleData, xIntRec^.DataeventLen / mTotDataTime);
        StoreSpindleData(xSpd);
      end;
    end;
    UpdateProdGrpStateTable(GetIntID, GetFragshiftID);
    mDB.DataBase.Commit;
  except
    if mDB.DataBase.InTransaction then
      mDB.DataBase.Rollback;
    Result := Error;
    WriteLog(etError, 'ProcessJob failed : ' + FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
{ TGetExpDataStopZESpdEditor }
//------------------------------------------------------------------------------
constructor TGetExpDataStopZESpdEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetMachID: Word;
begin
  Result := mJob.GetExpDataStopZESpd.MachineID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetProdID: longint;
begin
  Result := mJob.GetExpDataStopZESpd.ProdID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetSpdFirst: Byte;
begin
  Result := mJob.GetExpDataStopZESpd.SpindleFirst;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetSpdLast: Byte;
begin
  Result := mJob.GetExpDataStopZESpd.SpindleLast;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetSpdID(aJobRec: PJobRec): Word;
begin
  Result := aJobRec.GetExpDataStopZESpd.SpindID;
end;
//------------------------------------------------------------------------------
procedure TGetExpDataStopZESpdEditor.ReadSpdDataFromDataPool(aJobRec: PJobRec; var aSpdData: TMMDataRec);
begin
  aSpdData := aJobRec^.GetExpDataStopZESpd.SpdData;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetIntID: Word;
begin
  Result := mJob.GetExpDataStopZESpd.EndIntID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetFragShiftID: longint;
begin
  Result := mJob.GetExpDataStopZESpd.EndFragID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetBadDataFlag: boolean;
begin
  Result := mJob.GetExpDataStopZESpd.BadData;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetLastStoredFragID: integer;
begin
  Result := mJob.GetExpDataStopZESpd.StartFragID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.GetLastStoredIntID: Word;
begin
  Result := mJob.GetExpDataStopZESpd.StartIntID;
end;
//------------------------------------------------------------------------------
function TGetExpDataStopZESpdEditor.ProcessJob: TErrorRec;
var
  x: integer;
  xSpd: integer;
  xIntRec: PExpIntRec;
  xErr: boolean;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  WriteLogdebug('jtGetExpDataStopZESpd received from JobController');
  try
    xErr := False;
    mDB.DataBase.StartTransaction;
    for xSpd:=MachNetAssistant.ProdGrpAssistant.SpindFirst to MachNetAssistant.ProdGrpAssistant.SpindLast do begin
      try
        GetSpindleData(xSpd);
      except
        xErr := True;
        WriteLog(etError, 'GetSpindleData failed : ' + FormatMMErrorText(Error));
      end;
      mSpdDBBuffer := mSpindleData;
      for x := 0 to mIntervalList.Count - 1 do begin
        xIntRec := mIntervalList.Items[x];
        fIntID := xIntRec^.IntervalID;
        fFragShiftID := xIntRec^.FragshftID;
        InitDataPart(@mSpindleData, xIntRec^.DataEventLen / mTotDataTime);
        try
          if not xErr then
            StoreSpindleData(xSpd);
        except
          xErr := True;
          WriteLog(etError, 'StoreSpindleData failed : ' + FormatMMErrorText(Error));
        end;
      end;
      if xErr then
        Result := Error;
      xErr := False;
    end;
    try
      UpdateProdGrpStateTable(GetIntID, GetFragshiftID);
    except
      WriteLog(etError, 'ProcessJob failed : ' + FormatMMErrorText(Error));
    end;
    mDB.DataBase.Commit;
  except
    if mDB.DataBase.InTransaction then
      mDB.DataBase.Rollback;
    Result := Error;
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
{ TBaseShiftDataEditor }
//------------------------------------------------------------------------------
constructor TBaseGenShiftDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
  mShiftDataAssistant := TDBDataAssistant.Create(aEventLog);
  FillChar(mProdGrpDataRec, sizeof(TMMDataRec), 0);
end;
//------------------------------------------------------------------------------
destructor TBaseGenShiftDataEditor.Destroy;
begin
  mShiftDataAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TBaseGenShiftDataEditor.StartTransaction;
begin
  if not mDB.DataBase.InTransaction then try
    mDB.DataBase.StartTransaction;
  except
    WriteLog(etError, 'StartTransaction failed.');
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseGenShiftDataEditor.CommitTransaction;
begin
  if mDB.DataBase.InTransaction then try
    mDB.DataBase.Commit;
  except
    WriteLog(etError, 'Commit failed.');
  end;
end;
//------------------------------------------------------------------------------
procedure TBaseGenShiftDataEditor.RollBack;
begin
  if mDB.DataBase.InTransaction then try
    mDB.DataBase.Rollback;
  except
    WriteLog(etError, 'Rollback failed.');
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.GetProdGrpData(aProdID: integer; aIntervallID: Word; aFragShiftID: integer): boolean;
begin
  Result := True;
  mDB.Query[cPrimaryQuery].Close;
  mDB.Query[cPrimaryQuery].SQL.Text := cGetGrpDataFromSpindleData;
  try
    mDB.Query[cPrimaryQuery].Params.ParamByName('c_prod_id').AsInteger := aProdID;
    mDB.Query[cPrimaryQuery].Params.ParamByName('c_fragshift_id').AsInteger := aFragShiftID;
    mDB.Query[cPrimaryQuery].Params.ParamByName('c_interval_id').AsInteger := aIntervallID;
    mDB.Query[cPrimaryQuery].Open;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_PARAMETER, etDBError, 'GetProdGrpData failed.' + e.Message);
      Exit;
    end;
  end;
  try
    if not mDB.Query[cPrimaryQuery].FindFirst then begin
      Result := False;
      fError := SetError(ERROR_INVALID_PARAMETER, etMMError, Format('No spindledata available. ProdID=%d, FragID=%d, IntID=%d', [aProdID, aFragshiftID, aIntervallID]));
      Exit;
    end;
    while (not mDB.Query[cPrimaryQuery].Eof) and Result do begin // ADO Conform
      Sleep(1);
      Result := AddSpindleData;
      mDB.Query[cPrimaryQuery].Next;
    end;

    mDB.Query[cPrimaryQuery].Close;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'Add spindledata failed.' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.AddSpindleData: boolean;
var
  xClassFieldRec: TClassFieldRec;
  x: integer;
  xBuffer: TBlobByteData;
begin
  Result := True;
  // add the base date to the mProdGrpDataRec
  if not mShiftDataAssistant.AddBaseData(@mProdGrpDataRec, mDB.Query[cPrimaryQuery]) then begin
    Result := False;
    Exit;
  end;
  // add the class data to the mProdGrpDataRec
  with mProdGrpDataRec, mDB.Query[cPrimaryQuery], mShiftDataAssistant do begin
    try
      FillChar(xClassFieldRec, sizeof(TClassFieldRec), 0);
      GetBlobFieldData(FindField('c_class_image').FieldNo, xBuffer);
      xClassFieldRec := PClassFieldRec(xBuffer)^;
      for x := 1 to cSpCutFields do
        ClassFieldRec.SpCutField[x] := AddSmallInt(CheckLongWordToInt(ClassFieldRec.SpCutField[x],'SpCutField'),
          xClassFieldRec.SpCutField[x],
          'SpCutField : ' + intToStr(x));
      for x := 1 to cSpUncutFields do
        ClassFieldRec.SpUncutField[x] := AddInt(ClassFieldRec.SpUncutField[x],
          xClassFieldRec.SpUncutField[x],
          'SpUncutField : ' + intToStr(x));
      for x := 1 to cClassCutFields do
        ClassFieldRec.ClassCutField[x] := AddInt(ClassFieldRec.ClassCutField[x],
          xClassFieldRec.ClassCutField[x],
          'ClassCutField : ' + intToStr(x));
      for x := 1 to cClassUncutFields do
        ClassFieldRec.ClassUncutField[x] := AddInt(ClassFieldRec.ClassUncutField[x],
          xClassFieldRec.ClassUncutField[x],
          'ClassUncutField : ' + intToStr(x));
      for x := 1 to cSIROCutFields do
        ClassFieldRec.SIROCutField[x] := AddInt(ClassFieldRec.SIROCutField[x],
          xClassFieldRec.SIROCutField[x],
          'SIROCutField : ' + intToStr(x));
      for x := 1 to cSIROUncutFields do
        ClassFieldRec.SIROUncutField[x] := AddInt(ClassFieldRec.SIROUncutField[x],
          xClassFieldRec.SIROUncutField[x],
          'SIROUncutField : ' + intToStr(x));
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'AddSpdData failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.GetFragInfo(aFragID: integer; var aFragshiftStart: TDateTime): boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery] do begin
    try
      Close;
      SQL.Text := cGetFragInfo;
      Params.ParamByName('c_fragshift_id').AsInteger := aFragID;
      Open;
      aFragshiftStart := FieldByName('c_fragshift_start').AsDateTime;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'GetFragInfo failed. ' + e.message);
        Exit;
      end;
    end;
    if not FindFirst then begin
      Result := False;
      fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'GetFragInfo : No values available.');
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpBaseData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean;
begin
  Result := True;
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cInsertProdGrpData;
      ParamByName('c_prod_id').AsInteger          := aProdID;
      ParamByName('c_fragshift_id').AsInteger     := aFragShiftID;
      ParamByName('c_fragshift_start').AsDateTime := aFragShiftStart; //DateTimeToStr ( aFragShiftStart );
      ParamByName('c_fragshift_start').AdoType    := adDate;
      ParamByName('c_classCut_id').AsInteger      := mClassCutID;
      ParamByName('c_classUncut_id').AsInteger    := mClassUncutID;
      ParamByName('c_splicecut_id').AsInteger     := mSpliceCutID;
      ParamByName('c_spliceuncut_id').AsInteger   := mSpliceUncutID;
      ParamByName('c_sirocut_id').AsInteger       := mSiroCutID;
      ParamByName('c_sirouncut_id').AsInteger     := mSiroUncutID;
    end;
    if not mShiftDataAssistant.InitQueryByBaseData(mDB.Query[cPrimaryQuery], @mProdGrpDataRec) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    mDB.Query[cPrimaryQuery].ExecSQL;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of base data failed.' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpClassCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cInsertProdGrpClassCutFields;
      if not mShiftDataAssistant.InitQueryByClassCutData(mDB.Query[cPrimaryQuery], @ClassCutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      mClassCutID := InsertSQL('t_dw_classCut', 'c_classcut_id', high(integer), 1);
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of ClassCut data failed. ' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpClassUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cInsertProdGrpClassUnCutFields;
      if not mShiftDataAssistant.InitQueryByClassUncutData(mDB.Query[cPrimaryQuery], @ClassUncutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      mClassUncutID := InsertSQL('t_dw_classUnCut', 'c_classuncut_id', high(integer), 1);
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of ClassUncut data failed. ' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpSpliceCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cInsertProdGrpSpliceCutFields;
      if not mShiftDataAssistant.InitQueryBySpliceCutData(mDB.Query[cPrimaryQuery], @SpCutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      mSpliceCutID := InsertSQL('t_dw_SpliceCut', 'c_splicecut_id', high(integer), 1);
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of SpliceCut failed. ' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpSpliceUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cInsertProdGrpSpliceUnCutFields;
      if not mShiftDataAssistant.InitQueryBySpliceUncutData(mDB.Query[cPrimaryQuery], @SpUncutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      mSpliceUncutID := InsertSQL('t_dw_SpliceUncut', 'c_spliceuncut_id', high(integer), 1);
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of SpliceUncut failed. ' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpBaseData(aProdID: integer; aFragShiftID: integer): boolean;
begin
  Result := True;
  try
    mDB.Query[cPrimaryQuery].Close;
    mDB.Query[cPrimaryQuery].SQL.Text                                := cUpdateProdGrpData;
    mDB.Query[cPrimaryQuery].ParamByName('c_prod_id').AsInteger      := aProdID;
    mDB.Query[cPrimaryQuery].ParamByName('c_fragshift_id').AsInteger := aFragShiftID;
    if not mShiftDataAssistant.InitQueryByBaseData(mDB.Query[cPrimaryQuery], @mProdGrpDataRec) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    mDB.Query[cPrimaryQuery].ExecSQL;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of base data failed.' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpClassCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cUpdateProdGrpClassCutFields;
      if not mShiftDataAssistant.InitQueryByClassCutData(mDB.Query[cPrimaryQuery], @ClassCutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      ParamByName('c_classcut_id').AsInteger := mClassCutID;
      ExecSQL;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of ClassCut data failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpClassUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cUpdateProdGrpClassUnCutFields;
      if not mShiftDataAssistant.InitQueryByClassUncutData(mDB.Query[cPrimaryQuery], @ClassUncutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      ParamByName('c_classuncut_id').AsInteger := mClassUncutID;
      ExecSQL;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of ClassUncut data failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpSpliceCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cUpdateProdGrpSpliceCutFields;
      if not mShiftDataAssistant.InitQueryBySpliceCutData(mDB.Query[cPrimaryQuery], @SpCutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      ParamByName('c_splicecut_id').AsInteger := mSpliceCutID;
      mDB.Query[cPrimaryQuery].ExecSQL;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of SpliceCut data failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpSpliceUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do begin
    try
      Close;
      SQL.Text := cUpdateProdGrpSpliceUnCutFields;
      if not mShiftDataAssistant.InitQueryBySpliceUncutData(mDB.Query[cPrimaryQuery], @SpUncutField) then begin
        Result := False;
        fError := mShiftDataAssistant.Error;
        Exit;
      end;
      ParamByName('c_spliceuncut_id').AsInteger := mSpliceUncutID;
      ExecSQL;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of SpliceUnCut data failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.FragShiftAvailable(aProdID: integer; aFragShiftID: integer; var aAvailable: boolean): boolean;
begin
  Result := True;
  aAvailable := False;
  with mDB.Query[cBaseData_qry] do begin
    try
      Close;
      SQL.Text := cGetAvailableFragShiftData;
      Params.ParamByName('c_fragshift_id').AsInteger := aFragShiftID;
      Params.ParamByName('c_prod_id').AsInteger := aProdID;
      Open;
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBerror, 'FragShiftAvailable failed (1). ' + e.message);
        Exit;
      end;
    end;
    if FindFirst then begin
      try
        mClassCutID       := FieldByName('c_classcut_id').AsInteger;
        mClassUncutID     := FieldByName('c_classuncut_id').AsInteger;
        mSpliceCutID      := FieldByName('c_splicecut_id').AsInteger;
        mSpliceUncutID    := FieldByName('c_spliceuncut_id').AsInteger;
        mSiroCutID        := FieldByName('c_sirocut_id').AsInteger;
        mSiroUncutID      := FieldByName('c_sirouncut_id').AsInteger;
        //wss: pr�ft lediglich, ob Klassierdaten vorhanen sind. Exception wenn nicht
        CheckAvailabilityOfClassFieldsByDataBase;
      except
        on e: Exception do begin
          Result := False;
          fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'FragShiftAvailable failed (2). ' + e.message);
          Exit;
        end;
      end;
      aAvailable := True;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.AddProdGrpData: boolean;
  //.........................................................
  procedure PrepareClassDataQuery(aID: Integer);
  begin
    with mDB.Query[cClassData_qry] do
    try
      case aID of
        1: begin
            SQL.Text := cGetClassCutData;
            Params.ParamByName('c_classcut_id').AsInteger := mClassCutID;
          end;
        2: begin
            SQL.Text := cGetClassUncutData;
            Params.ParamByName('c_classuncut_id').AsInteger := mClassUncutID;
          end;
        3: begin
            SQL.Text := cGetSpliceCutData;
            Params.ParamByName('c_splicecut_id').AsInteger := mSpliceCutID;
          end;
        4: begin
            SQL.Text := cGetSpliceUncutData;
            Params.ParamByName('c_spliceuncut_id').AsInteger := mSpliceUncutID;
          end;
        5: begin
            SQL.Text := cGetSiroCutData;
            Params.ParamByName('c_sirocut_id').AsInteger := mSiroCutID;
          end;
        6: begin
            SQL.Text := cGetSiroUncutData;
            Params.ParamByName('c_sirouncut_id').AsInteger := mSiroUncutID;
          end;
      else
      end;
      Open;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBerror, 'PrepareClassDataQuery: ' + e.Message);
        raise Exception.Create(Error.Msg);
      end;
    end;
  end;
  //.........................................................
begin
  Result := True;
  if not mShiftDataAssistant.AddBaseData(@mProdGrpDataRec, mDB.Query[cBaseData_qry]) then begin
    Result := False;
    fError := mShiftDataAssistant.Error;
    fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
    Exit;
  end;
    // now the summary of AdjustBase and AdjustBaseCnt has to be calcualte for average
  with mProdGrpDataRec do
    if AdjustBaseCnt > 0 then
      AdjustBase := AdjustBase / AdjustBaseCnt;

  with mProdGrpDataRec.ClassFieldRec do begin
    PrepareClassDataQuery(1);
    if not mShiftDataAssistant.AddClassCutData(@ClassCutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
      Exit;
    end;
    PrepareClassDataQuery(2);
    if not mShiftDataAssistant.AddClassUnCutData(@ClassUncutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
      Exit;
    end;
    PrepareClassDataQuery(3);
    if not mShiftDataAssistant.AddSpliceCutData(@SpCutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
      Exit;
    end;
    PrepareClassDataQuery(4);
    if not mShiftDataAssistant.AddSpliceUnCutData(@SpUncutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
      Exit;
    end;
    PrepareClassDataQuery(5);
    if not mShiftDataAssistant.AddSiroCutData(@SiroCutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
      Exit;
    end;
    PrepareClassDataQuery(6);
    if not mShiftDataAssistant.AddSiroUnCutData(@SiroUncutField, mDB.Query[cClassData_qry]) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      fError.Msg := 'AddProdGrpData failed.' + fError.Msg;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean;
begin
  Result := True;
  if not InsertGrpClassCutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpClassCutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpClassUncutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpClassUncutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpSpliceCutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpSpliceCutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpSpliceUnCutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpSpliceUnCutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpSiroCutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpSiroCutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpSiroUncutData then begin
    Sleep(1000);
    // Try it again
    if not InsertGrpSiroUncutData then begin
      Result := False;
      Exit;
    end;
  end;
  if not InsertGrpBaseData(aProdID, aFragShiftID, aFragShiftStart) then begin
    Sleep(1000);
      // Try it again
    if not InsertGrpBaseData(aProdID, aFragShiftID, aFragShiftStart) then begin
      Result := False;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateData(aProdID: integer; aFragShiftID: integer): boolean;
var
  x: integer;
begin
  if not UpdateGrpClassCutData then begin
    Result := False;
    Exit;
  end;
  if not UpdateGrpClassUncutData then begin
    Result := False;
    Exit;
  end;
  if not UpdateGrpSpliceCutData then begin
    Result := False;
    Exit;
  end;
  if not UpdateGrpSpliceUncutData then begin
    Result := False;
    Exit;
  end;
  if not UpdateGrpSiroCutData then begin
    Result := False;
    Exit;
  end;
  if not UpdateGrpSiroUncutData then begin
    Result := False;
    Exit;
  end;
  // because deadlock problems this command is to do again if it failes
  for x := 1 to 3 do begin
    Result := UpdateGrpBaseData(aProdID, aFragShiftID);
    if Result then
      Exit
    else
      WriteLog(etInformation, 'UpdateGrpData failed.' + FormatMMErrorText(Error));
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateProdGrpState(aProdID, aFragID, aIntID: integer): boolean;
begin
  try
    Result := True;
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateProdGrpState;
      Params.ParamByName('c_prod_id').AsInteger := aProdID;
      Params.ParamByName('c_shift_fragshift_id_ok').AsInteger := aFragID;
      Params.ParamByName('c_shift_int_id_ok').AsInteger := aIntID;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'UpdateProdGrpState failed : ' + e.Message)
    end;
  end;
end;
//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.CorrectData(aNumSpindles: integer): boolean;
begin
  Result := True;
  if aNumSpindles > 0 then begin
    with mProdGrpDataRec do begin
      tWa := Round(tWa / aNumSpindles);
      tOp := Round(tOp / aNumSpindles);
      tRun := Round(tRun / aNumSpindles);
      if AdjustBaseCnt > 0 then begin
        AdjustBase := AdjustBase / AdjustBaseCnt;
          // now the average of AdjustBase over all spindles in group defines one value yet!!
        AdjustBaseCnt := 1;
      end;
    end;
  end
  else begin
    Result := False;
    fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'TBaseGenShiftDataEditor.CorrectData failed.');
  end;
end;

//------------------------------------------------------------------------------
procedure TBaseGenShiftDataEditor.CheckAvailabilityOfClassFieldsByDataBase;
begin
  with mDB.Query[cClassData_qry] do
  try
    SQL.Text := cGetClassCutData;
    Params.ParamByName('c_classcut_id').AsInteger := mClassCutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No classcut data available. c_classcut_id = %d', [mClassCutID]);

    SQL.Text := cGetClassUncutData;
    Params.ParamByName('c_classuncut_id').AsInteger := mClassUncutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No classuncut data available. c_classuncut_id = %d', [mClassUnCutID]);
    //......................................................
    SQL.Text := cGetSpliceCutData;
    Params.ParamByName('c_splicecut_id').AsInteger := mSpliceCutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No splicecut data available. c_splicecut_id = %d', [mSpliceCutID]);

    SQL.Text := cGetSpliceUncutData;
    Params.ParamByName('c_spliceuncut_id').AsInteger := mSpliceUncutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No spliceuncut data available. c_spliceuncut_id = %d', [mSpliceUnCutID]);
    //......................................................
    SQL.Text := cGetSiroCutData;
    Params.ParamByName('c_sirocut_id').AsInteger := mSiroCutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No sirocut data available. c_sirocut_id = %d', [mSiroCutID]);

    SQL.Text := cGetSiroUncutData;
    Params.ParamByName('c_sirouncut_id').AsInteger := mSiroUncutID;
    Open;
    if EOF then // ADO Conform
      raise Exception.CreateFmt('No sirouncut data available. c_sirouncut_id = %d', [mSiroUnCutID]);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBerror, 'CheckAvailabilityOfClassFieldsByDataBase: ' + e.Message);
      raise Exception.Create(Error.Msg);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpSiroCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do
  try
    Close;
    SQL.Text := cInsertProdGrpSiroCutFields;
    if not mShiftDataAssistant.InitQueryBySiroCutData(mDB.Query[cPrimaryQuery], @SIROCutField) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    mSiroCutID := InsertSQL('t_dw_siroCut', 'c_sirocut_id', high(integer), 1);
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of SiroCut data failed. ' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.InsertGrpSiroUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do
  try
    Close;
    SQL.Text := cInsertProdGrpSiroUnCutFields;
    if not mShiftDataAssistant.InitQueryBySiroUnCutData(mDB.Query[cPrimaryQuery], @SIROUncutField) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    mSiroUncutID := InsertSQL('t_dw_siroUncut', 'c_sirouncut_id', high(integer), 1);
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Insert of SiroUncut data failed. ' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.StoreProdGrpData(aProdID: integer; aFragShiftID: integer; aFragShiftStart: TDateTime): boolean;
var
  xFragshiftAvailable: boolean;
begin
  Result := False;
  try
    if not FragShiftAvailable(aProdID, aFragShiftID, xFragshiftAvailable) then begin
      raise EMMException.Create('FragShiftAvailable: ' + Error.Msg);
    end;
    if xFragshiftAvailable then begin
        // add the existing prodgrp data to the new intervall data
      if not AddProdGrpData then begin
        raise EMMException.Create('AddProdGrpData: ' + Error.Msg);
      end;
        // update the prodgrp data
      if not UpdateData(aProdID, aFragShiftID) then begin
        raise EMMException.Create('UpdateData: ' + Error.Msg);
      end;
    end
    else begin
        // insert the new fragshift data
      if not InsertData(aProdID, aFragShiftID, aFragShiftStart) then begin
        raise EMMException.Create('InsertData: ' + Error.Msg);
      end;
    end;
    Result := True;
  except
    on e: Exception do begin
      fError.Msg := 'StoreProdGrpData failed: ' + e.Message;
    end;
  end;
end;

//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpSiroCutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do
  try
    Close;
    SQL.Text := cUpdateProdGrpSiroCutFields;
    if not mShiftDataAssistant.InitQueryBySiroCutData(mDB.Query[cPrimaryQuery], @SIROCutField) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    ParamByName('c_sirocut_id').AsInteger := mSiroCutID;
    ExecSQL;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of SiroCut data failed.' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TBaseGenShiftDataEditor.UpdateGrpSiroUncutData: boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery], mProdGrpDataRec.ClassFieldRec do
  try
    Close;
    SQL.Text := cUpdateProdGrpSiroUnCutFields;
    if not mShiftDataAssistant.InitQueryBySiroUncutData(mDB.Query[cPrimaryQuery], @SIROUncutField) then begin
      Result := False;
      fError := mShiftDataAssistant.Error;
      Exit;
    end;
    ParamByName('c_sirouncut_id').AsInteger := mSiroUncutID;
    ExecSQL;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Update of SiroUncut data failed.' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------
{ TGenShiftDataEditor }
//------------------------------------------------------------------------------
constructor TGenShiftDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------
function TGenShiftDataEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    with mJob.GenShiftData do begin
        // get summarized spindle data into data record
      if not GetProdGrpData(ProdID, IntID, FragID) then begin
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
        // calculate some average values from spindle data
      if not CorrectData(SpindleLast - SpindleFirst + 1) then begin
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
        // get FragShiftStart time stamp
      if not GetFragInfo(FragID, mFragStart) then begin
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
      StartTransaction;
      if not StoreProdGrpData(ProdID, FragID, mFragStart) then begin
        RollBack;
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
      if not UpdateProdGrpState(ProdID, FragID, IntID) then begin
        RollBack;
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
      CommitTransaction;
    end;
  except
    on e: Exception do begin
      RollBack;
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'ProcessJob failed : ' + e.Message + Error.Msg);
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TGenExpShiftDataEditor }
//------------------------------------------------------------------------------
constructor TGenExpShiftDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
  mFragList := TList.Create;
end;
//------------------------------------------------------------------------------
destructor TGenExpShiftDataEditor.Destroy;
var
  x1: integer;
  xIntList: PFragIntRec;
begin
  for x1 := 0 to mFragList.Count - 1 do begin
    xIntList := mFragList.Items[x1];
    if Assigned(xIntList.IntList) then begin
      xIntList.IntList.Free;
    end;
    FreeMem(xIntList, sizeof(TFragIntRec));
  end;

  mFragList.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TGenExpShiftDataEditor.FillUpList: boolean;
var
  x: integer;
  xIntList: PFragIntRec;
begin
  Result := True;
  try
    with mDB.Query[cPrimaryQuery] do begin
        // fill up all fragshifts in first dimension
      Close;
      SQL.Text := cFillUpFragshifts;
      Params.ParamByName('Start_fragshift_id').AsInteger := mJob.GenExpShiftData.StartFragID;
      Params.ParamByName('End_fragshift_id').AsInteger := mJob.GenExpShiftData.EndFragID;
      Open;
      if EOF then // ADO Conform
        WriteLog(etWarning, 'TGenExpShiftDataEditor.FillUpList no Fragshifts in List. Start/End = ' +
          IntToStr(mJob.GenExpShiftData.StartFragID) + '/' +
          IntToStr(mJob.GenExpShiftData.EndFragID));
      while (not EOF) do begin // ADO Conform
        GetMem(xIntList, sizeof(TFragIntRec));
        xIntList^.FragID := FieldByName('c_fragshift_id').AsInteger;
        xIntList^.IntList := TList.Create;
        mFragList.Add(xIntList);
        Next;
      end;
        // fill up all intervalls containing to the fragshifts and between the Start and End Interval
      for x := 0 to mFragList.Count - 1 do begin
        xIntList := mFragList.Items[x];
        Close;
          // take only intervalls containing to the FragID and newer then the StartIntID and older ( or equeal ) then EndIntID in Job
        SQL.Text := cGetIntervalsInFragshift;
        Params.ParamByName('Start_interval_id').AsInteger := mJob.GenExpShiftData.StartIntID;
        Params.ParamByName('End_interval_id').AsInteger := mJob.GenExpShiftData.EndIntID;
        Params.ParamByName('c_fragshift_id').AsInteger := xIntList^.FragID;
        Params.ParamByName('c_prod_id').AsInteger := mJob.GenExpShiftData.ProdID;
        Open;
        while (not EOF) do begin // ADO Conform
          // c_interval_id direkt in IntList speichern anstelle eines Pointers daher Typecast Pointer()
          xIntList^.IntList.Add(Pointer(FieldByName('c_interval_id').AsInteger));
          Next;
        end;
      end;
    end;
  except
    on e: Exception do begin
      Result := False;
        // Meldung 'too many sessions' oder 'Network user limit exceeded' wird von der BDE erzeugt und kann
        // nur mit einem Neustart des Servers behoben werden. Tritt meistens nach laengerem debuggen auf.
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'FillUpList failed : ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TGenExpShiftDataEditor.ProcessJob: TErrorRec;
var
  x1, x2: integer;
  xIntList: PFragIntRec;
  xInt: Integer;
  xFragStart: TDateTime;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    if not FillUpList then begin
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
      Exit;
    end;
    StartTransaction;
    for x1 := 0 to mFragList.Count - 1 do begin
      xIntList := mFragList.Items[x1];
      FillChar(mProdGrpDataRec, sizeof(TMMDataRec), 0);
      for x2 := 0 to xIntList^.IntList.Count - 1 do begin
        xInt := Integer(xIntList^.IntList.Items[x2]);
        if not GetProdGrpData(mJob.GenExpShiftData.ProdID, xInt, xIntList^.FragID) then begin
          RollBack;
          Result := fError;
          WriteLog(etError, FormatMMErrorText(fError));
          Exit;
        end;
        if not CorrectData(mJob.GenExpShiftData.SpindleLast - mJob.GenExpShiftData.SpindleFirst + 1) then begin
          RollBack;
          Result := fError;
          WriteLog(etError, FormatMMErrorText(fError));
          Exit;
        end;
      end;
      if not GetFragInfo(xIntList^.FragID, xFragStart) then begin
        RollBack;
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
      if not StoreProdGrpData(mJob.GenExpShiftData.ProdID, xIntList^.FragID, xFragStart) then begin
        RollBack;
        Result := fError;
        WriteLog(etError, FormatMMErrorText(fError));
        Exit;
      end;
    end;
    if not UpdateProdGrpState(mJob.GenExpShiftData.ProdID, mJob.GenExpShiftData.EndFragID, mJob.GenExpShiftData.EndIntID) then begin
      RollBack;
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
      Exit;
    end;
    CommitTransaction;
  except
    on e: Exception do begin
      RollBack;
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'ProcessJob failed : ' + e.Message + Error.Msg);
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TGenDWDataEditor }
//------------------------------------------------------------------------------
constructor TGenDWDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter;
  aMMParam: TMMSettingsReader);
var
  xBroadcaster: TBroadcaster;
  xMsg: TMMClientRec;
begin
  inherited Create(aJob, aDB, aEventLog);
  mMMParam := aMMParam;

  WriteLog(etInformation, Format('End Dataaquisation: %s ', [DateTimeToStr(Now)]));

  //Signalisieren an alle Clients, dass Aquirierung abgeschlossen ist
  xBroadcaster := TBroadcaster.Create(mMMParam.value[cDomainNames], cChannelNames[ttMMClientReader]);
  try
    if not xBroadcaster.Init then
      WriteLog(etError, 'Init of Broadcaster (in TGenDWDataEditor.Create) failed.')
    else begin
      FillChar(xMsg, sizeof(xMsg), 0);
      xMsg.MsgTyp := ccMMApplMsg;
      xMsg.ServerName := mMMParam.value[cMMHost];
      xMsg.ApplMsg.MachineID := 0; //Nicht relevant in diesem Context
      xMsg.ApplMsg.ApplMsgEvent := amEndAquisation;
      if not xBroadcaster.Write(@xMsg, sizeof(xMsg)) then
         WriteLog(etError, 'Broadcast.Write (in TGenDWDataEditor.Create) failed. ' + xBroadcaster.ErrorInformation);
    end;
  finally
    xBroadcaster.Free;
  end;

end;
//------------------------------------------------------------------------------
function TGenDWDataEditor.ProcessJob: TErrorRec;
var
  xError: TErrorRec;
  xParameter: _Parameter;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;

  //Nue:20.3.02: Loescht nach der Behandlung aller Offlimits-Jobs alle Eintraege aus der Tabelle t_machine_offlimit_average
  //  welche nicht von der Tabelle t_machine_offlimit referenziert werden.
  if mJob.GenDWData.ShiftEv then begin // Nur bei Schichtwechsel ausf�hren
    with mDB.Query[cPrimaryQuery] do try
      Close;
      SQL.Text := cDelMachOfflimitsAverUnreferenced;
      ExecSQL;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TGenDWDataEditor.ProcessJob cDelMachOfflimitsAverUnreferenced ' + e.message);
        raise EMMException.Create(Error.Msg);
      end;
    end;
  end;

    {Event Shiftchange fuer Automatic Printout mit der Floor.
     Es wird nur bei einem Schiohtwechsel der default Schicht ein Event mit der betreffenden
     ShiftinDay Nummer abgesetzt. Gueltige ShiftinDay Nummer 1..5}
  if mJob.GenDWData.ShiftEv and (mJob.GenDWData.ShiftCalID = mMMParam.value[cDefaultShiftCalID]) then begin
    WriteLog(etInformation, Format('ShiftChange successfully finished! ShiftInDay = %d', [mJob.GenDWData.ShiftInDay]));
    case mJob.GenDWData.ShiftInDay of
      1, 2, 3, 4, 5: begin
          try
            xError := TriggerEventToAutoprint(TReportEvent(mJob.GenDWData.ShiftInDay));
          except
            on e: Exception do
              WriteLog(etWarning, 'Call to autoprint failed with error: ' + e.Message);
          end;
          if (xError.ErrorTyp <> etNoError) then
            WriteLog(etWarning, FormatMMErrorText(xError));
        end
    else
      WriteLog(etWarning, 'Unknow ShiftInDay Number');
    end;
  end;

  // Update Longterm database if SP usp_longterm_week available
  if mJob.GenDWData.ShiftEv then begin // Nur bei Schichtwechsel ausf�hren
    with TNativeADOQuery.Create do
    try
      try
        if mMMParam.IsComponentLongterm then begin
          Connection := mDB.Query[cPrimaryQuery].Connection;
          SQL.Text := 'usp_longterm_week';
          Command.CommandType := adCMDStoredProc;
          xParameter := Command.CreateParameter('RETURN VALUE', adInteger, adParamReturnValue, 4, EmptyParam);
          Command.Parameters.Append(xParameter);
          Command.CommandTimeout := (cDefOffset div 1000) * 5; //z.Z.5Minuten  (Nue:6.1.04)
          ExecSQL;
          if Command.Parameters[0].Value = 0 then
            WriteLog(etInformation, 'Update longterm database successful');
        end;
      except
        on e: Exception do begin
          WriteLog(etError, 'Store procedure usp_longterm_week failed' + e.message);
        end; // on e: Exception do begin
      end; // try except
    finally
      Free;
    end; // with TADONativeQuery.Create do try
  end;
end;
//------------------------------------------------------------------------------
{ TTestEditor }
//------------------------------------------------------------------------------
constructor TTestEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------
destructor TTestEditor.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TTestEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  with mDB.Query[0] do begin
    Close;
    SQL.Text := 'select c_interval_id  from t_interval';
    open;
    close;
  end;
end;
//------------------------------------------------------------------------------
function TTestEditor.Init: boolean;
begin
  Result := inherited Init;
end;
//------------------------------------------------------------------------------
{ TSendMsgToClients }
//------------------------------------------------------------------------------
constructor TSendMsgToClients.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aMMParam: TMMSettingsReader);
begin
  inherited Create(aJob, aDB, aEventLog);
  mMMParam := aMMParam;
  mBroadcaster := TBroadcaster.Create(mMMParam.value[cDomainNames], cChannelNames[ttMMClientReader]);
end;
//------------------------------------------------------------------------------
destructor TSendMsgToClients.Destroy;
begin
  mBroadcaster.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TSendMsgToClients.Init: boolean;
begin
  Result := inherited Init;
  if Result then begin
    Result := mBroadcaster.Init;
    if not Result then
      fError := SetError(mBroadcaster.Error, etNTError, 'Init of Broadcaster failed.');
  end;
end;
//------------------------------------------------------------------------------
function TSendMsgToClients.ProcessJob: TErrorRec;
var
  xMsg: TMMClientRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    FillChar(xMsg, sizeof(xMsg), 0);
    xMsg.MsgTyp := ccMMApplMsg;
    xMsg.ServerName := mMMParam.value[cMMHost];
    xMsg.ApplMsg.MachineID := mJob.SendEventToClients.MachineID;
    xMsg.ApplMsg.ApplMsgEvent := mJob.SendEventToClients.Event;
    case mJob.SendEventToClients.Event of
      amNoMsg: begin
        end;
      amMachineStateChange: begin
        end;
      amMulMaDataReady: begin
        end;
      amOneMaDataReady: begin
        end;
      amOfflimitsReady: begin
        end;
      amStartAquisation: begin
        end;
      amEndAquisation: begin   //Sollte hier nicht mehr auftretenm. (Wird in TGenDWDataEditor.Create abgehandelt!)
        end;
      amQOfflimit: begin
        end;
      amSystemInfo:
        with mDB.Query[cPrimaryQuery] do begin
          try
              // get the start of the interval in job
            Close;
            SQL.Text := cQrySelNextShiftAndInterval;
            Open;
            if not FindFirst then begin
              fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'No interval- and shiftstart found.');
              raise EMMException.Create(fError.Msg);
            end;
            xMsg.ApplMsg.NextInterval := FieldByName('interval_start').AsDateTime;
            xMsg.ApplMsg.NextShift := FieldByName('shift_start').AsDateTime;
          except
            on e: Exception do begin
              fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TSendMsgToClients.ProcessJob cQrySelNextShiftAndInterval failed' + e.Message);
              Result := fError;
              WriteLog(etError, FormatMMErrorText(fError));
            end;
          end;
        end;
    else
    end;
    if not mBroadcaster.Write(@xMsg, sizeof(xMsg)) then
      raise Exception.Create('Broadcast.Write failed. ' + mBroadcaster.ErrorInformation);
  except
    on e: Exception do begin
      Result := SetError(mBroadcaster.Error, etNTError, 'TSendMsgToClients.ProcessJob failed.' + e.Message);
      WriteLog(etError, FormatMMErrorText(Result));
    end;
  end;
end;

//------------------------------------------------------------------------------
{ TDelIntEditor }
//------------------------------------------------------------------------------
constructor TDelIntEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------
function TDelIntEditor.ProcessJob: TErrorRec;
var
  xIntStart: TDateTime;
  xIdList: TList;
  i: Integer;
begin
  // Das Aufr�umen von alten Intervallen wird nun vollst�ndig durch den DelInt Job
  // erledigt. Entsprechende Ausf�hrungen im TimeHanlder wurden entfernt, da dies
  // beim Aufstarten unter Umst�nden zu Timeouts gef�hrt hatten, welche der MMGuard
  // dazu veranlasste, das System erneut zu starten. So konnte es vorkommen, dass nach
  // dem X-ten mal das System schlussendlich ohne Fehler aufstarten konnte, war jedoch
  // nicht befriedigend.
  // Hier wird nun anhand der IntID, welche vom TimeHandler ermittelt wird, die Vergangenheit
  // in die lokale Liste eingelesen und Record f�r Record gel�scht. Auch hier k�nnte es
  // vorkommen, dass der Job nicht innerhalb der verf�gbaren Zeit von 3 Minuten fertig wird.
  // Dies sollte jedoch kein Problem f�r das System verursachen und der JobHandler l�st einfach
  // einen 2. oder 3. Versuch aus.
  WriteLogDebug('>>> Start job DelIntEditor');
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  if mJob.DelInt.IntID = 0 then Exit; // nothing to do if intID = 0

  xIdList := TList.Create;
  with mDB.Query[cPrimaryQuery] do
  try
    // get the start of the interval in job
    Close;
    SQL.Text := cGetIntStartByIntID;
    Params.ParamByName('c_interval_id').AsInteger := mJob.DelInt.IntID;
    Open;
    if not FindFirst then begin
      fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'No interval found.');
      raise EMMException.Create(fError.Msg);
    end;
    xIntStart := FieldByName('c_interval_start').AsDateTime;
    WriteLogDebug('Delete all intervals older than ' + DateTimeToStr(xIntStart));

    // now get all interval IDs in the past
    Close;
    SQL.Text := cGetOldIntIDsFromIntStart;
    Params.ParamByName('c_interval_start').AsDateTime := xIntStart;
    Params.ParamByName('c_interval_start').AdoType    := adDate;
    Open;
    while not EOF do begin
      xIdList.Add(Pointer(FieldByName('c_interval_id').AsInteger));
      Next;
    end;
    WriteLogDebug('Number to delete intervals in xIdList: ' + IntToStr(xIdList.Count));

    // delete records in t_interval. The Datarecords will be deleted by SQL Trigger
    SQL.Text := cDelInterval;
    for i:=0 to xIdList.Count-1 do begin
      Params.ParamByName('c_interval_id').AsInteger := Integer(xIdList.Items[i]);
      ExecSQL;
    end;
  except
    on e: Exception do begin
      mDB.DataBase.Rollback;
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'Delete of intervals failed.' + e.Message);
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
    end;
  end;
  xIdList.Free;
  WriteLogDebug('<<< End job DelIntEditor');
end;
//------------------------------------------------------------------------------
{ TDelShiftByTimeEditor }
//------------------------------------------------------------------------------
constructor TDelShiftByTimeEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------
function TDelShiftByTimeEditor.ProcessJob: TErrorRec;
var
   xOldTimeout: Integer;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  with mDB.Query[cPrimaryQuery] do try
    Close;
    // Timeout erh�hen, da Trigger infolge Subqueries l�nger dauern kann (L�schen von Partien ohne Daten)
    xOldTimeout := Command.CommandTimeout;
    try
      SQL.Text := cDelShiftByTimeQuery;
      Command.CommandTimeout := (cDefJobTimeout - cDefOffset) div 1000; //z.Z.9Minuten
      WriteLog(etInformation, 'Time im Job : ' + FloatToStr(mJob.DelShiftByTime.DateTime));
      Params.ParamByName('c_shift_start').AdoType    := adDate;
      Params.ParamByName('c_shift_start').AsDateTime := mJob.DelShiftByTime.DateTime;
      ExecSQL;
    finally
      Command.CommandTimeout := xOldTimeout;
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Delete of shifts failed. ' + e.Message);
      Result := fError;
      WriteLog(etError, FormatMMerrorText(fError));
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TSendMsgToApplEditor }
//------------------------------------------------------------------------------
constructor TSendMsgToApplEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
  mWriter := TIPCClient.Create(aJob.SendMsgToAppl.ComputerName, aJob.SendMsgToAppl.Port);
end;
//------------------------------------------------------------------------------
destructor TSendMsgToApplEditor.Destroy;
begin
  mWriter.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TSendMsgToApplEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  if mWriter.Write(@mJob.SendMsgToAppl.ResponseMsg, sizeof(TResponseRec)) then begin
    with mJob.SendMsgToAppl do begin
      WriteLog(etInformation, Format('SentMsgtoAppl:%s  Port:%s  RespMsg:%s  MachID:%d  SpdRange:%d-%d  ProdID:%d ',
        [ComputerName, Port, GetEnumName(TypeInfo(TResponseMsgTyp), Ord(ResponseMsg.MsgTyp)),
        ResponseMsg.MachineID,
          ResponseMsg.SpindleFirst,
          ResponseMsg.SpindleLast,
          ResponseMsg.ProdGrpID]));
    end;
  end
  else begin
    Result := SetError(mWriter.Error, etNTError, '');
    WriteLog(etError, FormatMMErrorText(Result) + ' Connect to client failed. Port = ' + mJob.SendMsgToAppl.ComputerName + '/' + mJob.SendMsgToAppl.Port);
  end;
end;
//------------------------------------------------------------------------------
 { TProdGrpUpdateEditor }
//------------------------------------------------------------------------------
constructor TProdGrpUpdateEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter;
  aMMParam: TMMSettingsReader);
begin
  inherited Create(aJob, aDB, aEventLog);
  mProdGrpAssistant := TProdGrpAssistant.Create;
  mMMParam := aMMParam;
end;
//------------------------------------------------------------------------------
destructor TProdGrpUpdateEditor.Destroy;
begin
  mProdGrpAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TProdGrpUpdateEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    mProdGrpAssistant.Init(mDB.Query[cPrimaryQuery]);
    mProdGrpAssistant.ProdGrpID := mJob.ProdGrpUpdate.ProdGrpID;
    mDB.DataBase.StartTransaction;

    if mJob.ProdGrpUpdate.SlipValid or mJob.ProdGrpUpdate.YarnCntValid then begin
      if mJob.ProdGrpUpdate.SlipValid then begin
          // correct Len and Weight of spindle and shift data and correct the slip of the actual prodgrp
//alt bis 22.11.07 Nue        UpdateLen(GetSlipCorrFactor(mJob.ProdGrpUpdate.Slip / MMUGlobal.cSlipFactor, mProdGrpAssistant.LotSlip));
        UpdateLen(mJob.ProdGrpUpdate.Slip, Round(MMUGlobal.cSlipFactor*mProdGrpAssistant.LotSlip));
        UpdateProdGrpSlip(mJob.ProdGrpUpdate.Slip);
      end;

      if mJob.ProdGrpUpdate.YarnCntValid then begin
          // Da auf der DB immer NM, muss gegebenfalls Umrechnung erfolgen

          // PPW 19/06/2017: issue #14
          //   Use YarnCnt unit that is defined via mmConfiguration instead of saved in DB
          //   because the saved unit in DB is always Nm
        UpdateProdGrpYarnCnt(Yarncountconvert(TYarnUnit(mMMParam.value[cYarnCntUnit]), yuNm, mJob.ProdGrpUpdate.YarnCnt));   //27.9.04 nue: No more Div cYarnCntFactor
  {
          if mJob.ProdGrpUpdate.ThreadCntValid then begin
            UpdateWeightByFactor ( mJob.ProdGrpUpdate.ThreadCnt );
            UpdateProdGrpThreadCnt ( mJob.ProdGrpUpdate.ThreadCnt );
            mJob.ProdGrpUpdate.ThreadCntValid := false;
          end else begin
            UpdateWeightByFactor ( mProdGrpAssistant.ThreadCnt );
          end;
  {}
      end;
        // Da auf der DB immer NM, muss gegebenfalls Umrechnung erfolgen
      UpdateWeightByLen(1.0 / Yarncountconvert(TYarnUnit(mProdGrpAssistant.YarnUnit), yuNm, mJob.ProdGrpUpdate.YarnCnt));  //27.9.04 nue: No more Div cYarnCntFactor
    end;

{
      if mJob.ProdGrpUpdate.ThreadCntValid then begin
        // correct the Weight of spindle data and shift data of the actual prodgrp and update the prodgrp threadcnt
        UpdateWeightByFactor ( mJob.ProdGrpUpdate.ThreadCnt / mProdGrpAssistant.ThreadCnt );
        UpdateProdGrpThreadCnt ( mJob.ProdGrpUpdate.ThreadCnt );
      end;
{}

    if mJob.ProdGrpUpdate.ColorValid then begin
      UpdateProdGrpColor(mJob.ProdGrpUpdate.Color);
    end;

    if mJob.ProdGrpUpdate.ProdNameValid then begin
      UpdateProdGrpName(mJob.ProdGrpUpdate.ProdName);
    end;

    mDB.DataBase.Commit;

    // 25.11.2002, wss/nue
    // Nun noch einen Broadcast ausf�hren lassen, damit die Floor aktuallisiert werden
    with TAdoDBAccess.Create(1) do try
      try
        DBName := 'master';
        Init;
        Query[cPrimaryQuery].SQL.Text := 'xp_SendMMMachStateChanged';
        Query[cPrimaryQuery].Command.CommandType := adCMDStoredProc;
        Query[cPrimaryQuery].ExecSQL;
      except
        on e: Exception do begin
          fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TProdGrpUpdateEditor.ProcessJob xp_SendMMMachStateChanged ' + e.message);
          raise EMMException.Create(Error.Msg);
        end; // on e: Exception do begin
      end; // try except
    finally
      free;
    end; // with TAdoDBAccess.Create(1) do try
  except
    on e: Exception do begin
      if mDB.DataBase.InTransaction then
        mDB.DataBase.Rollback;
      Result := SetError(ERROR_INVALID_FUNCTION, etNTError, 'TProdGrpUpdateEditor.ProcessJob failed. ' + e.Message);
      WriteLog(etError, FormatMMErrorText(Result));
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateProdGrpSlip(aSlip: integer);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateProdGrpSlip;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ParamByName('c_slip').AsInteger := aSlip;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateProdGrpSlip failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateProdGrpYarnCnt(aYarnCnt: Double);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateProdGrpYarnCnt;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      // c_act_yarncnt immer Nm Float ohne Faktor
      ParamByName('c_act_yarncnt').AsFloat := aYarnCnt;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateProdGrpYarnCnt failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
//procedure TProdGrpUpdateEditor.UpdateProdGrpThreadCnt(aThreadCnt: integer);
//begin
//  with mDB.Query[cPrimaryQuery] do
//  try
//    Close;
//    SQL.Text := cUpdateProdGrpThreadCnt;
//    ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
//    ParamByName('c_nr_of_threads').AsInteger := aThreadCnt;
//    ExecSQL;
//  except
//    on e: Exception do
//      raise Exception.Create('TProdGrpUpdateEditor.UpdateProdGrpThreadCnt failed. ' + e.Message);
//  end;
//end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateLen(aSlipNew: Integer; aSlipOld: Integer);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateSpindleDataLen;
      Params.ParamByName('slipnew').AsInteger := aSlipNew;
      Params.ParamByName('slipold').AsInteger := aSlipOld;
      Params.ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ExecSQL;
      Close;
      SQL.Text := cUpdateShiftDataLen;
      Params.ParamByName('slipnew').AsInteger := aSlipNew;
      Params.ParamByName('slipold').AsInteger := aSlipOld;
      Params.ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateLen failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
//alt bis 22.11.07 Nue
//function TProdGrpUpdateEditor.GetSlipCorrFactor(aNewSlip: Real; aOldSlip: Real): Double;
//begin
//  try
//    Result := aNewSlip / aOldSlip;
//  except
//    on e: Exception do begin
//      raise Exception.Create('TProdGrpUpdateEditor.GetSlipCorrFactor failed. ' + e.Message);
//    end;
//  end;
//end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateProdGrpColor(aColor: integer);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateProdGrpColor;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ParamByName('c_color').AsInteger := aColor;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateProdGrpColor failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateProdGrpName(aName: string);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateProdGrpName;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ParamByName('c_prod_name').AsString := aName;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateProdGrpColor failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TProdGrpUpdateEditor.UpdateWeightByLen(aFactor: Double);
begin
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUpdateSpindleDataWeightByLen;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ParamByName('factor').AsFloat := aFactor;
      ExecSQL;
      Close;
      SQL.Text := cUpdateShiftDataWeightByLen;
      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
      ParamByName('factor').AsFloat := aFactor;
      ExecSQL;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TProdGrpUpdateEditor.UpdateWeight failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
//procedure TProdGrpUpdateEditor.UpdateWeightByFactor(aFactor: Double);
//begin
//  try
//    with mDB.Query[cPrimaryQuery] do begin
//      Close;
//      SQL.Text := cUpdateSpindleDataWeightByFactor;
//      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
//      ParamByName('factor').AsFloat := aFactor;
//      ExecSQL;
//      Close;
//      SQL.Text := cUpdateShiftDataWeightbyFactor;
//      ParamByName('c_prod_id').AsInteger := mJob.ProdGrpUpdate.ProdGrpID;
//      ParamByName('factor').AsFloat := aFactor;
//      ExecSQL;
//    end;
//  except
//    on e: Exception do begin
//      raise Exception.Create('TProdGrpUpdateEditor.UpdateWeight (integer) failed. ' + e.Message);
//    end;
//  end;
//end;
//------------------------------------------------------------------------------
{ TSpdOfflimitsEditor }
//------------------------------------------------------------------------------
constructor TSpdOfflimitsEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
  mOfflimitAssistant := TMachineOfflimitAssistant.Create(mDB.Query[cPrimaryQuery]);
  mProdGrpAssistant  := TProdGrpAssistant.Create;
  mAverageStored     := False;
  mAverageID         := 0;
end;
//------------------------------------------------------------------------------
destructor TSpdOfflimitsEditor.Destroy;
begin
  mProdGrpAssistant.Free;
  mOfflimitAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.ProcessJob: TErrorRec;
var
  xInOfflimit: boolean;
  xSpindleID: integer;
begin
  Result := inherited ProcessJob;
  //  WriteLogDebug ( Format ( 'Offlimit generation of : ProdGrp = %d', [mJob.GenSpdOfflimits.ProdID]));
  if IsError(Result) then Exit;
    // Informationen der Partie holen
  try
    mProdGrpAssistant.Init(mDB.Query[cPrimaryQuery]);
    mProdGrpAssistant.ProdGrpID := mJob.GenSpdOfflimits.ProdID;
  except
    Result := mProdGrpAssistant.Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
    Exit;
  end;
  WriteLogDebug(Format('Offlimit generation of : ProdGrp = %d machid= %d.%d-%d ', [mJob.GenSpdOfflimits.ProdID,
    mProdGrpAssistant.MachineId,
      mProdGrpAssistant.SpindFirst,
      mProdGrpAssistant.SpindLast]));
    // Hilfsobjekt f�r Offlimit erstellen
  try
    mOfflimitAssistant.Init(mDB.Query[cPrimaryQuery]);
    mOfflimitAssistant.InitProdGrp(mJob.GenSpdOfflimits.ProdID, mProdGrpAssistant.MachineID, mJob.GenSpdOfflimits.IntID);
  except
    Result := mOfflimitAssistant.Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
    Exit;
  end;
  // Intervalldauer ermitteln von aktuellem Interval
  try
    InitMembers;
  except
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
    Exit;
  end;
  // Alle Spulstellen in t_machine_offlimit auf hide setzen
  try
    mOfflimitAssistant.UpdateAsHide(mProdGrpAssistant.SpindFirst, mProdGrpAssistant.SpindLast);
  except
    fError := mOfflimitAssistant.Error;
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
    Exit;
  end;

  try
    // Es muss eine g�ltige Konfiguration f�r diese Maschine vorhanden sein
    if not SettingsAvailable then begin
      WriteLog(etWarning, 'No generation of Machine Offlimit because no Settings available.');
      Exit;
    end;
  except
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
    Exit;
  end;

  // Diese Partie darf nicht in diesem Interval gestartet sein
  try
    if ProdGrpStartedInActInt then begin // no offlimitgeneration in aktual intervall
      WriteLogDebug(Format('No Offlimit generation, Started in act Interval : ProdGrp = %d', [mJob.GenSpdOfflimits.ProdID]));
      Exit;
    end;
  except
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
    Exit;
  end;
  // Diese Partie muss eine Mindestl�nge produziert haben: SpdCount * 2km
  try
    if not ProdGrpProducedInActInt then begin // no offlimit generation if nothing is produced
      WriteLogDebug(Format('No Offlimit generation, nothing is produced : ProdGrp = %d', [mJob.GenSpdOfflimits.ProdID]));
      Exit;
    end;
  except
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
    Exit;
  end;

  // Is average on in offlimitsettings
  try
    // Zu diesem Zeitpunkt kann AverageOn overruled werden, falls die Zeit noch nicht
    // erreicht wurde. Damit der FixBorder trotzdem greifft, wird beim Check, ob
    // fuer Average genuegend Intervalle vorhanden sind nicht mehr abgebrochen. Es wird
    // lediglich das Property AverageOn wieder auf False gesetzt, damit der FixBorder
    // trotzdem zum Zuge kommt. (11.4.2002, wss)
    if mOfflimitAssistant.AverageOn then begin
      try
        // if not enough intervals with data -> no average offlimit generation
        // but keep FixBorder calculation
        if not IntervalsAvailableForAverageCalc {(mOfflimitAssistant.ProdGrpDepend)} then
          mOfflimitAssistant.AverageOn := False;
      except
        Result := Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
        Exit;
      end;
    end;
  except
    Result := mOfflimitAssistant.Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
    Exit;
  end;
  // Check that every Spindle is available in t_machine_offlimit else fill them with 0 values
  if not CheckOfflimitRecords then begin
    Result := Error;
    WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
    Exit;
  end;
  // Hole die Daten dieser Partie im cSecondaryQuery. Datenset wird nur ge�ffnet!!
  try
    if not OpenAllSpindleData then begin // from t_interval_data
      WriteLog(etWarning, 'No Spindeldata found for Machine Offlimit');
      Exit;
    end;
  except
    Result := Error;
    WriteLog(etWarning, FormatMMErrorText(Error));
    Exit;
  end;

  // Nun wird jede einzelne Spindeldaten verarbeitet
  xInOfflimit := False;
  xSpindleID  := 0;
  while not mDB.Query[cSecondaryQuery].EOF do begin // ADO Conform
    Sleep(1);
    // Datenrecord von 1 Spindle lesen
    try
      xSpindleID := ReadSpindleData;
    except
      Result := Error;
      WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
      Exit;
    end;
    // Spindeldaten auf Offlimit pr�fen
    try
      xInOfflimit := mOfflimitAssistant.SpindleInOfflimit(@mSpindleOfflimitDataRec);
    except
      Result := mOfflimitAssistant.Error;
      WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
      Exit;
    end;
    // Berechnete l�ngenbasierte Werte und tLst, Lst werden in der Average Tabelle gespeichert
    // damit diese im Offlimitreport angezeigt werden k�nnen.
    if not mAverageStored then begin
      try
        mAverageID := mOfflimitAssistant.StoreAverages;
        mAverageStored := True;
      except
        Result := mOfflimitAssistant.Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Result));
        Exit;
      end;
    end;

    // Wenn diese Spindel von dieser Partie im Offlimit ist (egal ob fix oder avg Grenze)...
    if xInOfflimit then begin
      // ...dann m�ssen erst noch die vorhergehenden Werte holen...
      try
        mOfflimitAssistant.ReadOfflimitValuesFromDB(xSpindleID, @mOfflimitRec);
      except
        fError := mOfflimitAssistant.Error;
        Result := Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
        Exit;
      end;

      // ...und mit den neuen Werten aktualisieren. Dabei werden zugleich die On/Offline Zeitwerte
      // gepr�ft und aktualisiert.
      try
        mOfflimitAssistant.UpdateOfflimitValuesInRecord(@mOfflimitRec, @mSpindleOfflimitDataRec, mIntervallLen);
      except
        fError := mOfflimitAssistant.Error;
        Result := Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
        Exit;
      end;
      // Speichere neue Daten von dieser Spindel in der Tabelle t_machine_offlimit
      try
        mOfflimitAssistant.SaveOfflimitValuesToDB(xSpindleID, mAverageID, @mOfflimitRec);
      except
        fError := mOfflimitAssistant.Error;
        Result := Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
        Exit;
      end;
    end
    else begin // if xInOfflimit
      // Wenn diese Spindel mit keinem Parameter im Offlimit ist, dann alle OnlineTime
      // werte zur�cksetzen. Zugleich wird die OfflineWatchTime um die Intervall�nge
      // erh�ht.
      try
        mOfflimitAssistant.UpdateAsHealthy(xSpindleID, mIntervallLen, mAverageID);
      except
        fError := mOfflimitAssistant.Error;
        Result := Error;
        WriteLog(etWarning, 'Machine Offlimit failed : ' + FormatMMErrorText(Error));
        Exit;
      end;
    end; // if xInOfflimit
    mDB.Query[cSecondaryQuery].Next;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.ProdGrpStartedInActInt: boolean;
begin
  Result := True;
  // the prodgrp is started in the actual intervall if the prodgrp start is after the start of act interval
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cProdGrpStartCheck;
      Params.ParamByName('c_prod_id').AsInteger := mJob.GenSpdOfflimits.ProdID;
      Params.ParamByName('c_interval_id').AsInteger := mJob.GenSpdOfflimits.IntID;
      Open;
      if not FindFirst then Result := False;
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProdGrpStartedInActInt failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.ProdGrpProducedInActInt: boolean;
var
  xLen: integer;
begin
  Result := False;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cProdGrpProducedCheck;
    Params.ParamByName('c_prod_id').AsInteger := mJob.GenSpdOfflimits.ProdID;
    Params.ParamByName('c_interval_id').AsInteger := mJob.GenSpdOfflimits.IntID;
    Open;
    if FindFirst then begin
      xLen := FieldByName('len').AsInteger;
      Result := (xLen > ((mProdGrpAssistant.SpindLast - mProdGrpAssistant.SpindFirst + 1) * cMinSpuledLen));
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProdGrpProducedInActInt failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.IntervalsAvailableForAverageCalc {(aProdGrpDepend: boolean)}: boolean;
var
  xLen,
    xNumIntervalsExpected,
    xNumIntervalsAvailable: integer;
begin
  Result := False;
  xNumIntervalsAvailable := 0;
  try
    xNumIntervalsExpected := mOfflimitAssistant.AverageTime div mIntervallLen;
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      // get the produced len of every interval in the timeperiod of average
      SQL.Text := cGetAverageIntervalLen;
      Params.ParamByName('c_prod_id').AsInteger    := mJob.GenSpdOfflimits.ProdID;
      Params.ParamByName('start_range').AsDateTime := mOfflimitAssistant.AverageStart;
      Params.ParamByName('start_range').AdoType    := adDate;
      Params.ParamByName('end_range').AsDateTime   := mOfflimitAssistant.AverageEnd;
      Params.ParamByName('end_range').AdoType      := adDate;
      Open;
      while (not EOF) do begin // ADO Conform
        xLen := FieldByName('len').AsInteger;
        // In jedem Intervall muss die Mindestl�nge erreicht sein (SpdCount * 2km) ...
        if xLen < ((mProdGrpAssistant.SpindLast - mProdGrpAssistant.SpindFirst + 1) * cMinSpuledLen) then
          Exit; // In one interval the minimum of meters is not reached
        Inc(xNumIntervalsAvailable);
        Next;
      end;
      // ...und die gesamte Zeitdauer z.B. 480 Minuten muss vorhanden sein
      if xNumIntervalsAvailable >= xNumIntervalsExpected then
        Result := True; // Every interval is available
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'IntervalsAvailableForAverageCalc failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.CheckOfflimitRecords: Boolean;
var
  xStrList: TStringList;
  i: Integer;
begin
  Result := False;
  FillChar(mOfflimitRec, sizeof(TSpindleOfflimitRec), 0);
  xStrList := TStringList.Create;
  try
    with mDB.Query[cPrimaryQuery] do
    try
      Close;
      // Erst mal alle Spindeln ermitteln welche schon in der Tabelle t_machine_offlimit sind...
      SQL.Text := cQrySelectSpindlesInMachineOfflimit;
      Params.ParamByName('c_machine_id').AsInteger  := mProdGrpAssistant.MachineID;
      Params.ParamByName('spindle_first').AsInteger := mProdGrpAssistant.SpindFirst;
      Params.ParamByName('spindle_last').AsInteger  := mProdGrpAssistant.SpindLast;
      Open;
      while not EOF do begin
        xStrList.Add(FieldByName('c_spindle_id').AsString);
        Next;
      end;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'CheckOfflimitRecords failed: ' + e.Message);
        Exit;
      end;
    end;

    //...dann noch nicht verf�gbare OfflimitRecords pro Spindle in Tabelle t_machine_offlimit leer einf�gen
    for i:=mProdGrpAssistant.SpindFirst to mProdGrpAssistant.SpindLast do
    try
      if xStrList.IndexOf(IntToStr(i)) = -1 then
        mOfflimitAssistant.SaveOfflimitValuesToDB(i, 0, @mOfflimitRec);
    except
      fError := mOfflimitAssistant.Error;
      Exit;
    end;
    Result := True;
  finally
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpdOfflimitsEditor.InitMembers;
begin
  // Ermittelt die Dauer vom aktuellem Interval
  with mDB.Query[cPrimaryQuery] do try
    Close;
    SQL.Text := cGetIntervalLen;
    Params.ParamByName('c_interval_id').AsInteger := mJob.GenSpdOfflimits.IntID;
    Params.ParamByName('c_interval_id').AdoType := adInteger;
    Open;
    mIntervallLen := FieldByName('interval_len').AsInteger;
    if mIntervallLen = 0 then begin
      mIntervallLen := 10;
      WriteLog(etWarning, 'IntervalLen is Null, it will be set to 10.');
    end;
    Close;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Init failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.OpenAllSpindleData: Boolean;
begin
  with mDB.Query[cSecondaryQuery] do
  try
    Close;
    SQL.Text := cGetMaOffliSpindleData;
    Params.ParamByName('c_prod_id').AsInteger := mProdGrpAssistant.ProdGrpID;
    Params.ParamByName('c_interval_id').AsInteger := mJob.GenSpdOfflimits.IntID;
    Open;
    Result := FindFirst;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'OpenAllSpindleData failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.ReadSpindleData: integer;
var
  xotSpd: Integer;
begin
  FillChar(mSpindleOfflimitDataRec, sizeof(TSpindleOfflimitDataRec), 0);
  // Datenset wurde vorher schon initialisiert ge�ffnet. Schlaufe l�uft ausserhalb
  // dieser Methode per Next durch.
  with mDB.Query[cSecondaryQuery], mSpindleOfflimitDataRec do
  try
    Result := FieldByName('c_spindle_id').AsInteger;
    Len    := FieldByName('c_Len').AsInteger;
    xotSpd := FieldByName('c_otSpd').AsInteger;
    if xotSpd > 0 then
      Eff := Round(FieldByName('c_rtSpd').AsInteger * 100.0 / xotSpd);
    tLSt  := FieldByName('c_tLSt').AsInteger div 60; // sind auf DB Sekunden -> in Minuten umrechnen!!
    LSt   := FieldByName('c_LSt').AsInteger;

    CYTot := FieldByName('c_CYTot').AsFloat;
    CSIRO := FieldByName('c_CSIRO').AsFloat;
    CSp   := FieldByName('c_CSp').AsFloat;
    RSp   := FieldByName('c_RSp').AsFloat;
    CBu   := FieldByName('c_CBu').AsFloat;
    CUpY  := FieldByName('c_CUpY').AsFloat;
    YB    := FieldByName('c_YB').AsFloat;
//        Red   := FieldByName ( 'c_Red' ).AsInteger;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitOfflimitRec failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpdOfflimitsEditor.SettingsAvailable: boolean;
begin
  try
    with mDB.Query[cSecondaryQuery] do begin
      SQL.Text := cCheckOfflimitSettings;
      ParamByName('c_machine_id').AsInteger := mProdGrpAssistant.MachineID;
      Open;
      Result := FindFirst; // ADO Conform
    end;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'SettingsAvailable failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
  { TGenQOfflimitEditor }
//------------------------------------------------------------------------------

constructor TGenQOfflimitEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter;
  aMMParam: TMMSettingsReader);
begin
  inherited Create(aJob, aDB, aEventLog);
  mMMParam := aMMParam;
end;
//------------------------------------------------------------------------------
function TGenQOfflimitEditor.ProcessJob: TErrorRec;
var
  xError: TErrorRec;
  xQOfflimit: OleVariant;
  xAlarmCount: OleVariant;
const
  cOLEQOfflimit = 'LOEPFE.MMQOfflimitServer';
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    xError := setError(NO_ERROR);

    try
      // Interface zum QOfflimit erzeugen
      xQOfflimit := CreateOLEObject(cOLEQOfflimit);
    except
      on e: Exception do
        WriteLog(etError, 'Call to QOfflimit failed with error: ' + e.Message);
    end;
    // QOfflimit starten (Analysiert die gerade abgeschlossene Schicht)
    xQOfflimit.AnalyzeRecentShift(true, xAlarmCount);
    if xAlarmCount > 0 then begin
      WriteLog(etInformation, 'Quality Offlimit Count: ' + intToStr(xAlarmCount));
    end;
  except
    on e: Exception do begin
      Result := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TGenQOfflimitEditor.ProcessJob failed. Error : ' + e.Message);
      WriteLog(etError, 'QOfflimit failed. Error : ' + Result.Msg);
    end;
  end;
end;

//------------------------------------------------------------------------------
{ TDiaEditor }
//------------------------------------------------------------------------------
constructor TDiagnosticEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  fPriority := tpHigher;
end;
//------------------------------------------------------------------------------
destructor TDiagnosticEditor.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TDiagnosticEditor.ProcessJob: TErrorRec;
var
  xJobIDs: TJobIDList;
  xNumJobs: Word;
  x: integer;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
    //case mJob.Diagnostic.DiagnosticTyp of
    //  dtShowList : begin
  if GetAllJobIDs(xJobIDs, xNumJobs) then begin
    WriteLog(etSuccess, 'DataPool Jobs at : ' + DateTimeToStr(Now));
    for x := 0 to xNumJobs - 1 do begin
      WriteLog(etSuccess, 'Job ID = ' + IntToStr(xJobIDs[x].JobID) + '    JobTyp = ' + IntToStr(xJobIDs[x].JobTyp));
    end;
  end
  else begin
    WriteLog(etError, 'Access to DataPool failed. Error : ' + FormatMMErrorText(Error));
    Result := Error;
  end;
end;
//------------------------------------------------------------------------------
{ TGetNodeListEditor }
//------------------------------------------------------------------------------
constructor TGetNodeListEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
end;
//------------------------------------------------------------------------------
destructor TGetNodeListEditor.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TGetNodeListEditor.ProcessJob: TErrorRec;
begin
  // Wird in JobListClasses.GetNodeList abgehandelt ( 30.08.99 Mg/Nue )
{
    Dieser Editor muss auf der DB nachschauen, ob allen im DataPool abgelegten Nodes,
    (fuer jede NetId kommt eine Msg (GetNodeListJob))
    welche dort als ONLINE markiert sind, eine Maschine zugeordnet ist. Wenn nicht muss ein
    Minimaleintrag auf der DB gemacht werden. Wenn fuer alle vorhandenen NetId's eine
    Msg. abgehandelt wurde, sendet der JobHandler (sofern der Initiator eine Applikation war)
    einen SendMsgToApplJob, welche von hier an die entsprechende Applikation weitergeleitet
    werden muss. (5.5.99, Nue)
{}
end;
//------------------------------------------------------------------------------
{ TUnDoGetZESpdDataEditor }
//------------------------------------------------------------------------------
constructor TUnDoGetZESpdDataEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------
function TUnDoGetZESpdDataEditor.ProcessJob: TErrorRec;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;

  if (mJob.UnDoGetZESpdData.IntID = 0) or (mJob.UnDoGetZESpdData.FragID = 0) then
    Exit; // Nothing to delete

  WriteLog(etInformation, Format('UndoGetZESpdDataEditor : ProdID=%d, data newer then IntID=%d, FragID=%d will be deleted.',
    [mJob.UnDoGetZESpdData.ProdID,
    mJob.UnDoGetZESpdData.IntID,
      mJob.UnDoGetZESpdData.FragID]));

  try
    mDB.DataBase.StartTransaction;
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      SQL.Text := cUnDoGetZESpdData;
      Params.ParamByName('c_prod_id').AsInteger := mJob.UnDoGetZESpdData.ProdID;
      Params.ParamByName('c_interval_id').AsInteger := mJob.UnDoGetZESpdData.IntID;
      Params.ParamByName('c_fragshift_id').AsInteger := mJob.UnDoGetZESpdData.FragID;
      ExecSQL;
      Close;
      SQL.Text := cUpdateProdGrpStateTableUndo;
      Params.ParamByName('c_prod_id').AsInteger := mJob.UnDoGetZESpdData.ProdID;
      ExecSQL;
    end;
    mDB.DataBase.Commit;
  except
    on e: Exception do begin
      mDB.DataBase.Rollback;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, e.Message);
      Result := Error;
      WriteLog(etError, FormatMMErrorText(Error));
    end;
  end;
end;
//------------------------------------------------------------------------------
  { TDBDumpEditor }
//------------------------------------------------------------------------------
constructor TDBDumpEditor.Create(aJob: PJobRec; aDB: TAdoDBAccess; aEventLog: TEventLogWriter);
begin
  inherited Create(aJob, aDB, aEventLog);
end;
//------------------------------------------------------------------------------

function TDBDumpEditor.ProcessJob: TErrorRec;
var
  xbackupPath: string;
 // GL 16-02-2016
  SQLProductVersion: string;
  SQLMajorVersion: integer;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;

// GL 05-04-2016----------------------------------------------
 SQLMajorVersion := 0;
 with TAdoDBAccess.Create(1) do
    try
       try
           init;
           Query[cPrimaryQuery].SQL.Text := 'SELECT SERVERPROPERTY(''ProductVersion'') as ProductVersion';
           Query[cPrimaryQuery].Open;
           SQLProductVersion :=  Query[cPrimaryQuery].FieldByName('ProductVersion').AsString;
           SQLMajorVersion := strtoint( Copy(SQLProductVersion,1,Pos('.',SQLProductVersion)-1));
           WriteLog(etInformation, Format('Microsoft SQL Version: %d.', [SQLMajorVersion]));
       except
          on e: Exception do
             WriteLog(etError, 'SQL VERSION failed'+ e.Message);
          end;// try except
    finally
        free;
    end;// try finally  
//------------------------------------------------------------------------------

  xbackupPath := getregstring(cRegLM, cRegMMCommonPath, 'BackupPath');
  try
    with TAdoDBAccess.create(1) do try
      init;
      with Query[cPrimaryQuery] do begin
        if xbackupPath <> '' then begin
          try
            Command.CommandTimeout := (cDefJobTimeout - cDefOffset) div 1000; //z.Z.9Minuten
            // Parameter fuer Pfad spaeter von Registry
            SQL.Text := Format(cDBDump, [mJob.DBDump.DBName, xbackupPath + '\FullDB_', mJob.DBDump.DBName, mJob.DBDump.DBName]);
            ExecSQL;
          except
            on e: Exception do begin
              WriteLog(etError, Format('Could not dump database %s!! NO ACTUAL DB-BACKUP AVAILABLE! ' + e.Message, [mJob.DBDump.DBName]));
            end;
          end;

          try
              // Parameter fuer Pfad spaeter von Registry
 // GL 5-04-2016 -------------------------------------
               if SQLMajorVersion <=9 then  // SQL Server 2005 en ouder
                        SQL.Text := Format(cDumpTransactionNoLog, [mJob.DBDump.DBName])

                else
                        SQL.Text := cDumpTransactionNew;
                ExecSQL;
          except
            on e: Exception do begin
              WriteLog(etError,'Could not truncate transaction log;  take care of increasing transaction log!!'+ e.Message);
            end;
          end;
        end
        else begin
          WriteLog(etError, 'NO BACKUP-PATH found in Registry, DumpTransaction with NOLOG made');
 // GL 5-04-2016 -------------------------------------
 //       SQL.Text := Format(cDumpTransactionNoLog, [mJob.DBDump.DBName]);
          if SQLMajorVersion <=9 then  // SQL Server 2005 en ouder
              SQL.Text := Format(cDumpTransactionNoLog, [mJob.DBDump.DBName])
          else
              SQL.Text := cDumpTransactionNew;
          ExecSQL;
        end;
      end; //with
    finally
      free;
    end; //with
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, e.Message);
      Result := Error;
      WriteLog(etError, FormatMMErrorText(Error));
    end;
  end;
end;

//------------------------------------------------------------------------------
 { TGetSettingsAllGroupsEditor }
//------------------------------------------------------------------------------
constructor TSaveMaConfigToDBEditor.Create(aJob: PJobRec; aDB: TADODBAccess; aEventLog: TEventLogWriter; aDataPool: TDataPool);
begin
  inherited Create(aJob, aDB, aEventLog, aDataPool);
  if (aJob^.SaveMaConfig.ComputerName <> '') and (aJob^.SaveMaConfig.Port <> '') then begin
    WriteLogDebug(Format('ComputerName: %s',[aJob^.SaveMaConfig.ComputerName]));
    mClientWriter := TIPCClient.Create(aJob.SaveMaConfig.ComputerName, aJob.SaveMaConfig.Port);
  end else begin
    WriteLogDebug('No notification to application because this job was created at startup!');
    mClientWriter := Nil;
  end;
end;

//------------------------------------------------------------------------------
destructor TSaveMaConfigToDBEditor.Destroy;
begin
  mClientWriter.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------
function TSaveMaConfigToDBEditor.Init: boolean;
begin
  Result := inherited Init;
  if Result and Assigned(mClientWriter) then begin
    Result := mClientWriter.Connect;
    if not Result then
      fError := SetError(mClientWriter.Error, etNTError, 'TSaveMaConfigToDBEditor.Init failed. ');
  end;
end;
//:-----------------------------------------------------------------------------
function TSaveMaConfigToDBEditor.ProcessJob: TErrorRec;
var
  xSize: Integer;
  xPJobRec: PJobRec;
  xResponseRec: PResponseRec;
  xXMLData: String;
begin
  Result := inherited ProcessJob;
  if IsError(Result) then Exit;
  try
    if DataList.Count = 1 then begin
      xPJobRec     := DataList.Items[0];
      xXMLData  := StrPas(xPJobRec^.SaveMaConfig.XMLData);
      WriteLogDebug(Format('Do SaveMaConfigToDB for MachID = %d', [xPJobRec^.SaveMaConfig.MachineID]));

      // nun noch pos/neg Best�tigung zum Auftraggeber schicken, sofern einer angegeben wurde
      if Assigned(mClientWriter) then begin
        xSize := Length(xXMLData);

        xResponseRec := AllocMem(sizeof(TResponseRec) + xSize);
        with xResponseRec^ do
        try
          MsgTyp    := rmSaveMaConfigOk;
          MachineID := xPJobRec^.SaveMaConfig.MachineID;
          if xSize > 0 then
            System.Move(PChar(xXMLData)^, XMLMaConfig, xSize);

          if not mClientWriter.Write(PByte(xResponseRec), SizeOf(TResponseRec) + xSize) then
            raise Exception.Create('mClientWriter.Write failed. ' + FormatErrorText(mClientWriter.Error));
        finally
          FreeMem(xResponseRec);
        end;
      end; // if Assigned
    end; // if DataList.Count = 1 then
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'TSaveMaConfigToDBEditor.ProcessJob failed. ' + e.Message);
      Result := fError;
      WriteLog(etError, FormatMMErrorText(fError));
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

