(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: EditorServiceClasses.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.11.1998  0.00  Mg  | Datei erstellt
| 30.03.1999  1.00  Mg  | Anpassungen am TXN Spindelrecors
| 03.05.1999  1.01  Mg  | In TMachNetAssistant.ProcessTXNNetSpdData Round eingefuegt
| 03.08.1999  1.20  Mg  | Name von StoreXServiceClasses zu EditorServiceClasses
| 13.08.1999  1.21  Mg  | TDataEvetAssistant eingefuegt
| 03.11.1999  1.22  Mg  | Konvertierung von voiddata zu IPOption eingefuegt
|                       | TIPOption vom BaseGlobal zu diesem Modul
| 16.02.2000  1.23  Mg  | cProdGrpInfoQuery : YarnUnit and YarnCnt new from table t_prodgroup
| 18.02.2000  1.24  Mg  | TProdGrpAssistant : property IsSystemProdGrp inserted
| 28.02.2000  1.25  Mg  | TMachineOfflimitAssistant.UpdateAsHealthy : new Parameter aAverageID
|                       | BugFix and new procedure UpdateAsHide inserted
| 29.02.2000  1.26  Mg  | Spindle Data Times  ( WT, OT, RT and LongStop )in seconds on DB and CorrectTime inserted
| 03.03.2000  1.27  Mg  | TMachineOfflimitAssistant : new working with ( tablock holdlock ) by updates and inserts
| 16.03.2000  1.28  Mg  | Implementation of WSC Part in TMachineNetAssistant
| 20.03.2000  1.29  Mg  | TXN and WSC Data Records moved to YMDataDef
| 23.03.2000  1.30  Mg  | TMachineOfflimitAssistant : mAveragesLoaded inserted
| 25.04.2000  1.31  Mg  | TDBDataAssistant : SFI inserted
| 04.05.2000  1.32  Mg  | TProdGrpAssistant : property ThreadCnt inserted and Style depended YarnUnit ec. deleted
| 17.07.2000  1.33  Mg  | TDDataAssistant : AddFloat and CheckFloat inserted
| 30.10.2000  1.34  Mg  | TMachineOfflimitAssistant : property ProdGrpDepend inserted
| 27.11.2000  1.35  Mg  | TMachType in TAWEMachType converted
| 05.06.2001  1.36Khp/Nu| //Rausgenommen am 5.6.01 Nue/Khp           Close
| 12.11.2001  1.37 khp  | Bei AC338 werden Cops und Konen vom Informator uebernommen. (CalcWSCSpdValues)
                        | Neu: AdjustBase,AdjustBaseCnt im BaseRecord
| 26.11.2001  1.38 Wss  | - calculation for CalcNumCones, CalcNumBob changed for use single data type
                          - CalcTXNSpdValues: factor for bob, cones removed
                          - ProcessWSCNetSpdData: factor for bob, cones removed
                          - InitQueryByBaseData: factor for bob, cones removed
                          - AddBaseData: changed to float data type addition of bob, cones
| 04.02.2002  1.39 Wss  | TMachineOfflimitAssistant.CalcToBase: tLSt and LSt not normalized because
                          settings says Value/interval
| 28.02.2002  1.40 khp  | Garnnummer ist neu auf der Datenbank float Nm, im Ze-Sack immer Garnnummer mit Garneinheit verwenden.
                          ProdgrpAssistent neu fYarnCnt= Single;
| 12.03.2002  1.39 Wss  | In LoadAverages Berechnungen auf Float geaendert, da bei BaseLen 1'000'000km
                          ein Ueberlauf bei Integer erfolgt, wo das Resultat einer Minuszahl entspricht:
                          c_YB = 2425, c_Len = 7110992 -> 2425 * 1'000'000 = 2'425'000'000 ($908A9040, MaxInt = 2'147'483'647)
                          Wenn hoechstes Bit gesetzt ist in $908A9040 dann entspricht dies einer Minuszahl in Integer: -1869967296
| 13.03.2002  1.40 khp  | TMachNetAssistant.CorrectTime : Add, Nullabgleichkorrektur 0.4 Sekunden pro Zyklus
| 18.03.2002  1.41 khp  | Calculate Len with LotSlip and DrumCorrfactor.
| 06.06.2002  1.39 Wss  | - In TProdGrpAssistant.CalcWeight wird ThreadCount nicht beruecksichtigt, da
                            die GarnNummer das Endprodukt definiert
                          - Property und Membervariable ThreadCnt entfernt
| 04.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 07.11.2002        Wss | - MaschinenOfflimit an neue Konfiguration angepasst
                          - Daten f�r RedLight aus den 3 Offlimit Tabellen entfernt
                          - % Settings f�r tLSt und LSt entfernt
| 11.12.2002        Wss | In LoadAverages Berechnung f�r Eff darf nicht direkt im Query
                          erfolgen: Berechnung nun beim Auslesen der Daten mit �berpr�fung
| 27.05.2005        Wss | Reinigerdaten werden nun in den entsprechenden NetHandler umkopiert um eine normierte
                          Datenstruktur innerhalb vom MMSystem zu haben (MMDataRec)
| 05.10.2005        Wss | Generell: Param AsDateTime mit AdoType := atDate erg�nzt
| 24.10.2005        Lok | Overflow Meldungen in den ADD...() Methoden neu als Debug Meldungen definiert (st�rend bei Demodaten)
| 02.03.2006        Wss | cLoadAverages f�r SpdOfflimits verwendet nun v_production_interval
                            - sum(feld) f�hrte zu arithm. overflow wenn t_interval, t_spindle_interval_data benutzt wurde
                              ohne Float-Konvertierung im Query
| 20.06.2006        Nue | Neue Methode CheckLongWordToInt added und bei CheckInt Parameter aPar von longword zu longint ge�ndert.
|                       | Methoden Addxxx modifiziert.
| 12.02.2008  1.50  Nue | VCV-Fields added.
|=========================================================================================*)
{$MINENUMSIZE 2}
{ Pendenzen

}
unit EditorServiceClasses;
interface
uses BaseGlobal, Windows, MMUGlobal, SysUtils, YMParaDef,
  AdoDBAccess, LoepfeGlobal,
  mmEventLog, YMParaUtils, XMLDef;

//******************************************************************************
//..........CONSTANTS TYPES AND PROCEDURES FOR NET TYPES....................
//******************************************************************************

//------------------------------------------------------------------------------
   // Convertes the texnet class format into the DB class format
   // The procedure can raise an exception
//------------------------------------------------------------------------------

//******************************************************************************
//..........CONSTANTS AND TYPES FOR ALL OFFLIMITS...............................
//******************************************************************************

//***************** M A C H I N E  O F F L I M I T *****************************
// For NULL the value in the record is -1
type
  TMachineOfflimitSettings = record
    AverageOn: boolean; // Calculate border by average
    FixOn: boolean; // Calculate border by fix values
    AverageTime: integer; // Time to calculate the average
    Base: TLenBase; // i.e. 100 km, 1000km
//    Base            : integer; // i.e. 100 km, 1000km
//    ProdGrpDepend: boolean; // false = Calculate the average over the start of prodgrp
    Eff_PC_min: integer; // Min. differenze between Eff and average in %
    Eff_PC_max: integer; // Max. differenz between Eff and average in %
    CYTot_PC_min: integer;
    CYTot_PC_max: integer;
    CSIRO_PC_max: integer;
    CSp_PC_max: integer;
    RSp_PC_max: integer;
    CBu_PC_max: integer;
    CUpY_PC_max: integer;
    YB_PC_max: integer; // Max. differenz between YB and average in %

    Eff_min: integer; // Min. of Eff
    Eff_max: integer; // Max. of Eff
    tLSt_max: integer;
    LSt_max: integer;
    CYTot_min: Single;
    CYTot_max: Single;
    CSIRO_max: Single;
    CSp_max: Single;
    RSp_max: Single;
    YB_Max: Single; // Max. number of YB per Base
    CBu_max: Single;
    CUpY_max: Single;
    { DONE -owss -cUmbau : Braucht es Prozentuale Grenzwerte f�r tLst und Lst �berhaupt noch? }
    { DONE -owss -cUmbau : RedL Berechnung �berfl�ssig da keine Konfiguration mehr? }
//    tLSt_PC_max: integer;
//    LSt_PC_max: integer;
//    RedL_PC_max: integer;
//    RedL_max: integer;
  end;
//..............................................................................
  PSpindleOfflimitDataRec = ^TSpindleOfflimitDataRec;
  TSpindleOfflimitDataRec = record
    Len: integer;
    Len_On: boolean;
    Eff: integer;
    Eff_On: boolean;

    { DONE -owss -cUmbau : tLst umbauen auf Float Datentyp ?!?!}
    tLSt: Integer;
    tLSt_On: boolean;
    LSt: integer;
    LSt_On: boolean;

    CYTot: Single;
    CYTot_On: boolean;
    CSIRO: Single;
    CSIRO_On: boolean;
    CSp: Single;
    CSp_On: boolean;
    RSp: Single;
    RSp_On: boolean;
    CBu: Single;
    CBu_On: boolean;
    CUpY: Single;
    CUpY_On: boolean;
    YB: Single; // Values/Border
    YB_On: boolean; // true=position in offlimit  false=position NOT in offlimit
    { DONE -owss -cUmbau : RedL Berechnung �berfl�ssig da keine Konfiguration mehr? }
//    Red: integer;
//    Red_On: boolean;
  end;
//..............................................................................
  PSpindleOfflimitBorderRec = ^TSpindleOfflimitBorderRec;
  TSpindleOfflimitBorderRec = record
    Eff_min: Single;
    Eff_max: Single;
    tLSt_max: Single;
    LSt_max: Single;

    CYTot_min: Single;
    CYTot_max: Single;
    CSIRO_max: Single;
    CSp_max: Single;
    RSp_max: Single;
    CBu_max: Single;
    CUpY_max: Single;
    YB_max: Single;
//    Red_max: integer;
  end;
//..............................................................................
  // this record is almost equal with the data base tabel t_machine_offlimits
  PSpindleOfflimitRec = ^TSpindleOfflimitRec;
  TSpindleOfflimitRec = record
    max_online_offlimit_time: integer; // Max. time of the online params
    offline_offlimit_time: integer; // Numbers of minutes spindle is in offlimit sinc last reset
    offline_watchtime: integer; // Numbers of minutes since last reset
    Eff: integer; // Number of Eff
    Eff_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    Eff_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset

    tLSt: integer; // Time of Longstops
    tLSt_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    tLSt_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    LSt: integer; // Number of Longstops
    LSt_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    LSt_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    OutOfProduction: boolean; // Spindle is out of production (the min number of meters is not reached)
    OutOfProduction_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    OutOfProduction_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset

    CYTot: Single; // Number of total Yarn cuts
    CYTot_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    CYTot_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    CSIRO: Single; // Number of SIRO Cuts
    CSIRO_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    CSIRO_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    YB: Single; // Number of Yarn Breakes
    YB_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    YB_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    CSp: Single; // Number of Spleis cuts
    CSp_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    CSp_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    RSp: Single; // Number of Splice repetitions
    RSp_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    RSp_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    CBu: Single; // Number of Bunch cuts
    CBu_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    CBu_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
    CUpY: Single; // Number of Murata Uper yarn cuts
    CUpY_online_time: integer; // Number of minutes the parameter is permanent in offlimit
    CUpY_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
//    RedL: integer; // Number of Red lights
//    RedL_online_time: integer; // Number of minutes the parameter is permanent in offlimit
//    RedL_offline_time: integer; // Number of minutes the parameter is in offlimit since last reset
  end;
//***************** E N D  M A C H I N E  O F F L I M I T **********************

//***************** Q U A L I T I   O F F L I M I T ****************************

//***************** E N D  Q U A L I T I   O F F L I M I T *********************

//..............................................................................
  TIPOption = (ioNotDefined, ioIPNO, ioIPOld, ioIPNew, ioIPPlus);
//Abstraction layer is ProdGrp and the types are useful for one Shift of 80 spindles
  TProdGrpAssistant = class(TObject)
  private
    mQuery: TNativeAdoQuery;
    mInitialized: boolean;
    fError: TErrorRec;
    fMachineID: Word;
//    fMachineTyp: TAWEMachType;
    fProdGrpID: longint;
    fSpindFirst: Byte;
    fSpindLast: Byte;
    fYarnUnit: TYarnUnit;
    fYarnCnt: Single;
    fLotslip: Double;
    fDrumCorrFact: Single; // Wert: 0.5-1.5 Korrekturfaktor welcher in der Maschinenkonfiguration pro Maschine angegeben wird.
    fLengthCorrFact: Double;
    fIPOption: TIPOption;
    fVoidData: DWord;
    fMeterPerBob: longword;
    fMeterPerCone: longword;
    //fIsSystemProdGrp : boolean;
    procedure SetProdGrpID(aProdGrpID: Longint);
    function GetLotslip: Double;
    function GetYarnCnt: Single;
    function GetLengthCorrFact: Double;
//NUE1    property MachineTyp: TAWEMachType read fMachineTyp;
  protected
  public
    constructor Create;
    destructor Destroy; override;
    procedure Init(aQuery: TNativeAdoQuery);
    function CalcRealLen(var aLen: longword): boolean;
    function CalcWeight(var aWeight: longword; aLen: longword): boolean; // Gramm
    function CalcNumBob(var aNumBob: Single; aLen: longword): boolean;
    function CalcNumCones(var aNumCones: Single; aLen: longword): boolean;
    property Error: TErrorRec read fError;
    property MachineID: Word read fMachineID;
    property ProdGrpID: longint read fProdGrpID write SetProdGrpID; // this property can raise an exception
    property SpindFirst: Byte read fSpindFirst;
    property SpindLast: Byte read fSpindLast;
    property YarnUnit: TYarnUnit read fYarnUnit;
    property YarnCnt: Single read GetYarnCnt;
    property LengthCorrFact: Double read GetLengthCorrFact;
    property Lotslip: Double read GetLotslip;
    property IPOption: TIPOption read fIPOption;
    property VoidData: DWord read fVoidData;
    property MeterPerBob: longword read fMeterPerBob;
    property MeterPerCone: longword read fMeterPerCone;
    //property    IsSystemProdGrp : boolean read fIsSystemProdGrp;
  end;
//..............................................................................
  TMachNetAssistant = class(TObject)
  private
    fProdGrpAssistant: TProdGrpAssistant;
    fNetTyp: TNetTyp;
    fMachID: Word;
    fError: TErrorRec;
    // Time Correcture of WT, OT and RT because AWE
    function CorrectTime(aTime: integer; aSplices: integer; aSpliceRep: integer): integer;
    // TXN Procedures
    procedure ProcessTXNNetSpdData(aSpindleData: PMMDataRec);
    procedure CalcTXNSpdValues(aSpindleData: PMMDataRec);
//    procedure ProcessTXNNetProdGrpData(aSpdDataArr: PSpdDataArr; aProdGrpDBRec: PMMDataRec);
//    procedure CalcTXNProdGrpValues(aProdGrpDBRec: PMMDataRec);
    // WSC Ethernet Procedures
    procedure ProcessWSCNetSpdData(aSpindleData: PMMDataRec);
//    procedure ProcessWSCNetProdGrpData(aSpdDataArr: PSpdDataArr; aProdGrpDBRec: PMMDataRec);
    procedure CalcWSCSpdValues(aSpindleData: PMMDataRec);
//    procedure ProcessWSCNetProdGrpData(aSpdDataArr: PSpdDataArr; aProdGrpDBRec: PMMDataRec);
    procedure CalcLXSpdValues(aSpindleData: PMMDataRec);
//    procedure ProcessTXNNetProdGrpData(aSpdDataArr: PSpdDataArr; aProdGrpDBRec: PMMDataRec);
//    procedure CalcTXNProdGrpValues(aProdGrpDBRec: PMMDataRec);
    // WSC Ethernet Procedures
    procedure ProcessLXNetSpdData(aSpindleData: PMMDataRec);
  public
    constructor Create(aNetTyp: TNetTyp; aMachID: Word);
    destructor Destroy; override;
    procedure Init(aQuery: TNativeAdoQuery);
    procedure ProcessSpindleData({aSpdDataArr: PSpdDataArr; }aSpdDBRec: PMMDataRec);
    property NetTyp: TNetTyp read fNetTyp;
    property MachID: Word read fMachID;
    property ProdGrpAssistant: TProdGrpAssistant read fProdGrpAssistant write fProdGrpAssistant;
    property Error: TErrorRec read fError;
  end;
//..............................................................................
  TIPAssistant = class(TObject)
  private
    mFactNeps: Double;
    mFactSmall: Double;
    mFactThick: Double;
    mFactThin: Double;
    mFact2_4: Double;
    mFact4_8: Double;
    mFact8_20: Double;
    mFact20_70: Double;
    mIPOption: TIPOption;
    mProdGrpAssistant: TProdGrpAssistant;
    fError: TErrorRec;
    function GetIPAvailable: boolean;
    procedure InitFactorsByConstants;
    function InitFactorsByDatabase: boolean;
    function GetImperfections(var aImpRec: TImpRec; aClassFields: TMMClassUncutField): boolean;
  public
    constructor Create(aProdGrpAssistant: TProdGrpAssistant);
    function CalcImperfections(var aImpRec: TImpRec; aClassFields: TMMClassUncutField): boolean;
    property IPAvailable: boolean read GetIPAvailable;
    property Error: TErrorRec read fError;
  end;
//..............................................................................
  TDBDataAssistant = class(TObject)
  private
    mEventLog: TEventLogWriter; // only used to report overflow warnings in AddInt
    fDefaultErrStr: string;
    fError: TErrorRec;
  public
    function CheckInt(aPar: longInt; aParTyp: ShortString): integer;
    function CheckFloat(aPar, aMaxValue: Single; aParTyp: Shortstring): Single;
    function CheckLongWordToInt(aPar: longword; aParTyp: ShortString): Integer;
    function AddInt(aSum, aAdd: integer; aName: string): integer; // for adding integer and check the range
    function AddSmallInt(aSum, aAdd: integer; aName: string): integer; // for adding integer and check the range
    function AddFloat(aSum, aAdd, aMaxValue: Single; aName: string): Single; // for adding floats and check the range
    function InitQueryByBaseData(aQuery: TNativeAdoQuery; aRec: PMMDataRec): boolean;
    function InitQueryByClassCutData(aQuery: TNativeAdoQuery; aClassCut: PMMClassCutField): boolean;
    function InitQueryByClassUnCutData(aQuery: TNativeAdoQuery; aClassUnCut: PMMClassUnCutField): boolean;
    function InitQueryBySpliceCutData(aQuery: TNativeAdoQuery; aSpliceCut: PMMSpCutField): boolean;
    function InitQueryBySpliceUnCutData(aQuery: TNativeAdoQuery; aSpliceUnCut: PMMSpUnCutField): boolean;
    function InitQueryBySiroCutData(aQuery: TNativeAdoQuery; aSiroCut: PMMSiroCutField): boolean;
    function InitQueryBySiroUnCutData(aQuery: TNativeAdoQuery; aSiroUnCut: PMMSiroUnCutField): boolean;
    function InitRecByClassCutData(aClassCut: PMMClassCutField; aQuery: TNativeAdoQuery): boolean;
    function InitRecByClassUnCutData(aClassUnCut: PMMClassUnCutField; aQuery: TNativeAdoQuery): boolean;
    function InitRecBySpliceCutData(aSpliceCut: PMMSpCutField; aQuery: TNativeAdoQuery): boolean;
    function InitRecBySpliceUnCutData(aSpliceUnCut: PMMSpUnCutField; aQuery: TNativeAdoQuery): boolean;
    function InitRecBySiroCutData(aSiroCut: PMMSiroCutField; aQuery: TNativeAdoQuery): boolean;
    function InitRecBySiroUnCutData(aSiroUnCut: PMMSiroUnCutField; aQuery: TNativeAdoQuery): boolean;
    function AddBaseData(aRec: PMMDataRec; aQuery: TNativeAdoQuery): boolean;
    function AddClassCutData(aClassCut: PMMClassCutField; aQuery: TNativeAdoQuery): boolean;
    function AddClassUnCutData(aClassUnCut: PMMClassUnCutField; aQuery: TNativeAdoQuery): boolean;
    function AddSpliceCutData(aSpliceCut: PMMSpCutField; aQuery: TNativeAdoQuery): boolean;
    function AddSpliceUnCutData(aSpliceUnCut: PMMSpUnCutField; aQuery: TNativeAdoQuery): boolean;
    function AddSiroCutData(aSiroCut: PMMSiroCutField; aQuery: TNativeAdoQuery): boolean;
    function AddSiroUnCutData(aSiroUnCut: PMMSiroUnCutField; aQuery: TNativeAdoQuery): boolean;
    property DefaultErrStr: string read fDefaultErrStr write fDefaultErrStr; // this string will be written in the log file by overflow in Add function
    property Error: TErrorRec read fError;
    constructor Create(aEventLog: TEventLogWriter);
  end;
//..............................................................................
  TMachineOfflimitAssistant = class(TObject)
  private
    mQuery: TNativeAdoQuery;
    mSettings: TMachineOfflimitSettings;
    mBorders: TSpindleOfflimitBorderRec;
    mAverages: TSpindleOfflimitDataRec;
    mSettingsLoaded: boolean;
    mBordersLoaded: boolean;
    mAveragesLoaded: boolean;
    mActIntervalID: integer;
    fProdGrpID: integer;
    fMachID: Word;
    fError: TErrorRec;
    fAverageStart: TDateTime;
    fAverageEnd: TDateTime;
    function GetAverageOn: boolean;
    function GetAverageStart: TDateTime;
    function GetAverageEnd: TDateTime;
    procedure SetAverageOn(Value: boolean);
    procedure LoadSettings; // gets the offlimit settings from the database and stores them in the mSettintgs
    procedure LoadBorders; // calculates the borders
    procedure LoadAverages; // gets the averages from the database
    procedure CheckAvgBorderValues;
    procedure CheckFixBorderValues;
    function CheckSpindle(aSpindleOfflimitRec: PSpindleOfflimitDataRec): boolean; // result is true if spindle is in offlimit
    procedure CalcToBase(aSpindleOfflimitRec: PSpindleOfflimitDataRec; var aProduced: boolean);
  public
    // every property is able to raise an exception
    property AverageOn: boolean read GetAverageOn write SetAverageOn;
    property AverageTime: integer read mSettings.AverageTime; // Number of minutes for average
    property AverageStart: TDateTime read GetAverageStart; // Start of the first interval for calculation of average
    property AverageEnd: TDateTime read GetAverageEnd; // End of last interval for calculation of average
    property Error: TErrorRec read fError write fError;
    property ProdGrpID: integer read fProdGrpID;
    property MachID: Word read fMachID;
    // every function and procedure is able to raise an exception
    function SpindleInOfflimit(aSpindleOfflimitRec: PSpindleOfflimitDataRec): boolean; // result is true if spindle is in offlimit
    procedure ReadOfflimitValuesFromDB(aSpindleID: integer; aOfflimitRec: PSpindleOfflimitRec); // gets the Offlimit values of the spezified spindle
    procedure UpdateOfflimitValuesInRecord(aOfflimitRec: PSpindleOfflimitRec; aSpindleOfflimitDataRec: PSpindleOfflimitDataRec; aIntervalLen: integer);
    procedure SaveOfflimitValuesToDB(aSpindleID, aAverageID: integer; aOfflimitRec: PSpindleOfflimitRec); // stores the offlimit values on the database, can raise an exception
    procedure UpdateAsHealthy(aSpindleID, aIntervalLen: integer; aAverageID: integer);
    procedure UpdateAsHide(aSpindleFirst, aSpindleLast: integer);
    function StoreAverages: integer;
    procedure InitProdGrp(aProdID: integer; aMachID: integer; aActIntervalID: integer);
    procedure Init(aQuery: TNativeAdoQuery); // can raise an exception
    constructor Create(aQuery: TNativeAdoQuery);
    destructor Destroy; override;
  end;
//..............................................................................
  TDataEventAssistant = class(TObject)
  private
    mQuery: TNativeAdoQuery;
  public
    // all methods can raise an exception
    function GetDataEventByID(aIntervalID, aFragshiftID: integer): TDateTime;
    function GetOldestDataEvent: TDateTime;
    function GetIntervalIDByDataEvent(aDataEvent: TDateTime): integer;
    function GetFragshiftIDByDataEvent(aDataEvent: TDateTime): integer;
    function GetDataEventLenByDataEvent(aDataEvent: TDateTime): integer;
    constructor Create(aQuery: TNativeAdoQuery);
    destructor Destroy; override;
  end;
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  classes;
//------------------------------------------------------------------------------
const
  cProdGrpInfoQuery =
    'select m.c_machine_id, p.c_spindle_first, ' +
    'p.c_spindle_last, p.c_yarncnt_unit, p.c_act_yarncnt, p.c_nr_of_threads, ' +
    'p.c_slip Slip, o.c_act_mm_per_Bob, o.c_act_mm_per_Cone, p.c_voiddata, p.c_style_id ,m.c_slip DrumCorrFact ' +
    'from t_prodgroup p, t_machine m, t_order_position o ' +
    'where p.c_machine_id = m.c_machine_id and ' +
    'p.c_order_position_id *= o.c_order_position_id and ' +
    'p.c_prod_id = :c_prod_id';
//------------------------------------------------------------------------------
//TODO wss: Ist dieses Query noch n�tig? Relikt noch von fr�her? -> Imp Berechnung  TIPAssistant.InitFactorsByDatabase
  cFactorQuery =
    'select data from t_MmuParm where AppName=''imperfections'' and ' +
    'AppKey=''N,S,Tk,Tn,L24,L48,L820,L2070''';
//------------------------------------------------------------------------------
  cGetMaOffliSettings =
    'select c_OffBorderByAverage, c_OffBorderByFixValues, c_Average_time, c_Base, c_ProdGrp_depend, ' +
    'c_Eff_PC_max, c_Eff_PC_min, c_Eff_max, c_Eff_min, c_CYTot_PC_max, ' +
    'c_CYTot_PC_min, c_CYTot_max, c_CYTot_min, c_CSIRO_PC_max, c_CSIRO_max, ' +
    'c_CSp_PC_max, c_CSp_max, c_RSp_PC_max, c_RSp_max, c_YB_PC_max, c_YB_Max, ' +
    'c_CBu_PC_max, c_CBu_max, c_CUpY_PC_max, c_CUpY_max, c_tLSt_max, ' +
    'c_LSt_max ' +
    'from  t_machine m, t_machine_offlimit_settings mos ' +
    'where m.c_machine_id = :c_machine_id ' +
    'and m.c_maoffset_id = mos.c_maoffset_id';
//------------------------------------------------------------------------------
  cGetMaOffliAverageEndTime =
    'select c_interval_end end_time from t_interval where c_interval_id = :c_interval_id';
//------------------------------------------------------------------------------
  cGetMaOffliAverageStartTime =
    'select dateadd ( minute, - :average_len , :end_time ) start_time';
//------------------------------------------------------------------------------
  cLoadAverages =
    'select c_prod_id, sum ( c_YB ) c_YB , ' +  // c_Red entfernt
    'sum(c_rtSpd) c_rtSpd, sum(c_otSpd) c_otSpd, sum(c_CSp) c_CSp, ' +
    'sum(c_RSp ) c_RSp, sum ( c_CBu ) c_CBu, sum ( c_CUpY ) c_CUpY, ' +
    'sum(c_LStProd ) c_LStProd , sum ( c_LSt ) c_LSt, sum ( c_CSIRO ) c_CSIRO, ' +
    'sum(c_CYTot ) c_CYTot, sum ( c_Len ) c_Len ' +
    'from v_production_interval ' +
//    'from t_spindle_interval_data id, t_interval i ' +
    'where c_prod_id = :c_prod_id ' +
//    'and id.c_interval_id = i.c_interval_id ' +
    'and c_interval_start >= :start ' +
    'and c_interval_start < :end ' +
    'group by c_prod_id';
//------------------------------------------------------------------------------
  cStoreAverages =
    'insert into t_machine_offlimit_average ' +
    '( c_maoffaverage_id, c_YB,c_Red,c_Eff, ' +
    'c_CSp, c_RSp, c_CBu, c_CUpY, c_tLSt, c_LSt, c_CSIRO, c_CYTot ) ' +
    'values ( :c_maoffaverage_id, :c_YB, 0, :c_Eff, :c_CSp, :c_RSp, :c_CBu, ' +
    ':c_CUpY, :c_tLSt, :c_LSt, :c_CSIRO, :c_CYTot)';
//------------------------------------------------------------------------------
  cGetSpindleOfflimitData =
    'select * from t_machine_offlimit where ' +
    'c_machine_id = :c_machine_id and ' +
    'c_spindle_id = :c_spindle_id';
//------------------------------------------------------------------------------
  cDeleteMachineOfflimitSpindle =
    'delete from t_machine_offlimit ' +
    'where c_machine_id = :c_machine_id and ' +
    'c_spindle_id = :c_spindle_id';
//------------------------------------------------------------------------------
  cInsertMachineOfflimitSpindle =
    'insert t_machine_offlimit ' +
    '( c_spindle_id, c_machine_id, c_maoffaverage_id, ' +
    'c_update_time, c_max_online_offlimit_time,  c_offline_offlimit_time, c_offline_watchtime, ' +
    'c_YB,c_YB_online_time,c_YB_offline_time,c_RedL,c_RedL_online_time,c_RedL_offline_time, ' +
    'c_Eff, c_Eff_online_time, c_Eff_offline_time, c_CSp,c_CSp_online_time, c_CSp_offline_time,' +
    'c_RSp, c_RSp_online_time, c_RSp_offline_time, c_CBu, c_CBu_online_time,c_CBu_offline_time,' +
    'c_CUpY, c_CUpY_online_time, c_CUpY_offline_time, c_tLSt, c_tLSt_online_time, c_tLSt_offline_time,' +
    'c_LSt, c_LSt_online_time, c_LSt_offline_time, c_CSIRO, c_CSIRO_online_time, c_CSIRO_offline_time,' +
    'c_CYTot, c_CYTot_online_time, c_CYTot_offline_time, c_OutOfProduction, c_OutOfProduction_online_time,' +
    'c_OutOfProduction_offline_time ) ' +
    'values ( :c_spindle_id, :c_machine_id, :c_maoffaverage_id, ' +
    'getdate(), :c_max_online_offlimit_time,  :c_offline_offlimit_time, :c_offline_watchtime, ' +
    ':c_YB,:c_YB_online_time,:c_YB_offline_time, 0, 0, 0, ' +  // f�r RedL Werte auf 0 setzen
    ':c_Eff,:c_Eff_online_time,:c_Eff_offline_time,:c_CSp,:c_CSp_online_time,:c_CSp_offline_time,' +
    ':c_RSp,:c_RSp_online_time,:c_RSp_offline_time,:c_CBu,:c_CBu_online_time,:c_CBu_offline_time,' +
    ':c_CUpY,:c_CUpY_online_time,:c_CUpY_offline_time,:c_tLSt,:c_tLSt_online_time,:c_tLSt_offline_time,' +
    ':c_LSt, :c_LSt_online_time, :c_LSt_offline_time, :c_CSIRO, :c_CSIRO_online_time, :c_CSIRO_offline_time,' +
    ':c_CYTot,:c_CYTot_online_time,:c_CYTot_offline_time,:c_OutOfProduction, :c_OutOfProduction_online_time,' +
    ':c_OutOfProduction_offline_time )';

//------------------------------------------------------------------------------
  cUpdateUpdateAsHealthy =
    'update t_machine_offlimit set ' +
    'c_update_time = getdate(), c_max_online_offlimit_time = 0, ' +
    'c_maoffaverage_id = :c_maoffaverage_id, ' +
    'c_offline_watchtime = c_offline_watchtime + :interval_len, ' +
    'c_YB_online_time = 0, c_RedL_online_time = 0, c_Eff_online_time = 0, ' +
    'c_CSp_online_time = 0, c_RSp_online_time = 0,c_CBu_online_time = 0, ' +
    'c_CUpY_online_time = 0, c_tLSt_online_time = 0, c_LSt_online_time = 0, ' +
    'c_CSIRO_online_time = 0,c_CYTot_online_time = 0, c_OutOfProduction_online_time = 0 ' +
    'where c_machine_id = :c_machine_id and ' +
    'c_spindle_id = :c_spindle_id';

//------------------------------------------------------------------------------
  cUpdateUpdateAsHide =
    'update t_machine_offlimit set ' +
    'c_update_time = getdate(), c_max_online_offlimit_time = 0 ' +
    'where c_machine_id = :c_machine_id ' +
    'and c_spindle_id >= :c_spindle_first ' +
    'and c_spindle_id <= :c_spindle_last';

//------------------------------------------------------------------------------
  cGetDataEventByID =
    'select c_dataevent_start ' +
    'from v_DataEvents de ' +
    'where de.c_interval_id = :c_interval_id ' +
    'and   de.c_fragshift_id = :c_fragshift_id';
//------------------------------------------------------------------------------
  cGetIntervalIDByDataEvent =
    'select c_interval_id ' +
    'from v_DataEvents ' +
    'where c_dataevent_start = :DataEvent';
//------------------------------------------------------------------------------
  cGetFragshiftIDByDataEvent =
    'select c_fragshift_id ' +
    'from v_DataEvents ' +
    'where c_dataevent_start = :DataEvent';
//------------------------------------------------------------------------------
  cGetDataEventLenByDataEvent =
    'select datediff ( minute, :DataEvent, ( select min ( c_dataevent_start) from v_DataEvents where c_dataevent_start > :DataEvent2 ) ) Len';
//------------------------------------------------------------------------------
  cGetOldestDataEvent =
    'select min ( c_dataevent_start ) c_dataevent_start from v_dataevents';
//------------------------------------------------------------------------------

//-----------Start implementation of T E X N E T procedures---------------------
// wss, 27.5.2005: obsolet
//procedure ConvertTXNDefectData(aClassFieldRec: PClassFieldRec;
//  aDefectData: PTXNDefectData;
//  aSiroData: PTXNSiroData);
//var x: integer;
//  xMemStream: TMemoryStream;
//begin
//  xMemStream := TMemoryStream.Create;
//  try
//    FillChar(aClassFieldRec^, sizeof(TClassFieldRec), 0);
//       // convert Yarn Defect Data
//    xMemStream.Write(aDefectData^.yarnDefect, cYarnDefectSize);
//    xMemStream.Position := 0;
//    for x := 1 to cClassCutFields do begin
//      aClassFieldRec^.ClassCutField[x] := aDefectData^.cutYarnDef[x];
//      xMemStream.Read(aClassFieldRec^.ClassUncutField[x], cTXNYarnDefXArray[x]);
//    end;
//    xMemStream.Clear;
//       // convert the Splice Defect Data
//    xMemStream.Write(aDefectData^.spliceDefect, cSpliceDefectSize);
//    xMemStream.Position := 0;
//    for x := 1 to cSpCutFields do begin
//      aClassFieldRec^.SpCutField[x] := aDefectData^.cutSpliceDef[x];
//      xMemStream.Read(aClassFieldRec^.SpUncutField[x], cTXNSpliceDefXArray[x]);
//    end;
//    xMemStream.Clear;
//       // convert the SIRO Defect Data
//    xMemStream.Write(aSiroData^.siroDefect, cSiroDefectSize);
//    xMemStream.Position := 0;
//    for x := 1 to cSIROCutFields do begin
//      aClassFieldRec^.SIROCutField[x] := aSIROData^.cutSiroDef[x];
//      xMemStream.Read(aClassFieldRec^.SIROUncutField[x], cTXNSIRODefXArray[x]);
//    end;
//  except
//    on e: Exception do begin
//      xMemStream.Free;
//      raise Exception.Create('ConvertTXNDefectData failed. ' + e.Message);
//    end;
//  end;
//  xMemStream.Free;
//end;
// -----------End implementation of T E X N E T procedures----------------------

// -----------Start implementation of E T H E R N E T 3 3 8 procedures----------
// wss, 27.5.2005: obsolet
//procedure ConvertWSCDefectData(aClassFieldRec: PClassFieldRec; aDefectData: PWSCDefectData; aSiroData: PWSCSiroData);
//var x: integer;
//begin
//  try
//    FillChar(aClassFieldRec^, sizeof(TClassFieldRec), 0);
//       // convert Yarn Defect Data
//    for x := 1 to cClassCutFields do begin
//      aClassFieldRec^.ClassCutField[x] := aDefectData^.cutYarnDef[x];
//      aClassFieldRec^.ClassUncutField[x] := aDefectData^.yarnDefect[x];
//    end;
//
//       // convert the Splice Defect Data
//    for x := 1 to cSpCutFields do begin
//      aClassFieldRec^.SpCutField[x] := aDefectData^.cutSpliceDef[x];
//      aClassFieldRec^.SpUncutField[x] := aDefectData^.spliceDefect[x];
//    end;
//
//       // convert the SIRO Defect Data
//    for x := 1 to cSIROCutFields do begin
//      aClassFieldRec^.SIROCutField[x] := aSIROData^.cutSiroDef[x];
//      aClassFieldRec^.SIROUncutField[x] := aSIROData^.siroDefect[x];
//    end;
//  except
//    on e: Exception do
//      raise Exception.Create('ConvertWSCDefectData failed. ' + e.Message);
//  end;
//end;
// -----------End implementation of E T H E R N E T 3 3 8 procedures------------

//------------------------------------------------------------------------------
{ TProdGrpInfo }
//------------------------------------------------------------------------------
constructor TProdGrpAssistant.Create;
begin
  inherited Create;
  mQuery          := TNativeAdoQuery.Create;
  fProdGrpID      := 0;
  mInitialized    := False;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
end;
//------------------------------------------------------------------------------
destructor TProdGrpAssistant.Destroy;
begin
  mQuery.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TProdGrpAssistant.Init(aQuery: TNativeAdoQuery);
begin
  mQuery.Assign(aQuery as TPersistent);
end;
//------------------------------------------------------------------------------
procedure TProdGrpAssistant.SetProdGrpID(aProdGrpID: Longint);
begin
  if (fProdGrpID <> aProdGrpID) or not mInitialized then begin
    with mQuery do begin
      Close;
      SQL.Text := cProdGrpInfoQuery;
      Params.ParamByName('c_prod_id').AsInteger := aProdGrpID;
      try
        Open;
      except
        on E: Exception do begin
          fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TProdGrpAssistant.SetProdGrpID failed. ' + e.Message);
          raise EMMException.Create(Error.Msg);
        end;
      end;
      if not FindFirst then begin
           // No ProdGrp available
        fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'TProdGrpAssistant.SetProdGrpID :Invalid ProdGrp parameter. ProdGrpID : ' + IntToStr(aProdGrpID));
        raise EMMException.Create(fError.Msg);
      end;
      try
        fMachineID      := FieldByName('c_machine_id').AsInteger;
        fSpindFirst     := FieldByName('c_spindle_first').AsInteger;
        fSpindLast      := FieldByName('c_spindle_Last').AsInteger;
        fLotSlip        := FieldByName('Slip').AsInteger / cSlipFactor;
        fDrumCorrFact   := FieldByName('DrumCorrFact').AsInteger / cSlipFactor;
        fVoidData       := FieldByName('c_voiddata').AsInteger;
        fMeterPerBob    := FieldByName('c_act_mm_per_Bob').AsInteger div 1000;
        fMeterPerCone   := FieldByName('c_act_mm_per_Cone').AsInteger div 1000;
        fYarnUnit       := TYarnUnit(FieldByName('c_yarncnt_unit').AsInteger); // from prodgrp
        //Garnnummer auf DB immer Nm
        fYarnCnt        := FieldByName('c_act_yarncnt').AsFloat; // from prodgrp
        // Convert of voiddata to IPOption
        fIPOption       := ioIPNO;
        fLengthCorrFact := (fLotSlip * fDrumCorrFact) / 200;
        if GetAvailabilityOfDataItem(VoidData, vdIPOld) then fIPOption := ioIPNO;
        if GetAvailabilityOfDataItem(VoidData, vdIPNew) then fIPOption := ioIPNew;
        if GetAvailabilityOfDataItem(VoidData, vdIPPlus) then fIPOption := ioIPPlus;
      except
        on E: Exception do begin
          fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'SetProdGrpID failed.' + e.message);
          raise EMMException.Create(Error.msg);
        end;
      end;
    end;
    mInitialized := True;
    fProdGrpID := aProdGrpID;
  end;
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.CalcRealLen(var aLen: longword): boolean;
begin
  Result := False;
  fError.ErrorTyp := etNoError;
  fError.Error := NO_ERROR;
  if mInitialized then begin
    try
      aLen   := Round(aLen * LengthCorrFact); // fLengthCorrFact := (fLotslip * fDrumKorrFakt) / 200
      Result := True;
    except
      fError := SetError(ERROR_BUFFER_OVERFLOW, etMMError, 'CalcRealLen failed.');
    end;
  end else
    fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'CalcRealLen failed. Object is not initialized.');
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.CalcNumBob(var aNumBob: Single; aLen: longword): boolean;
begin
  Result          := False;
  aNumBob         := 0;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
  if mInitialized then begin
    if MeterPerBob <> 0 then try
//wss          aNumBob := Round ( aLen  div MeterPerBob  );
      aNumBob := aLen / MeterPerBob;
      Result  := True;
    except
      fError := SetError(ERROR_BUFFER_OVERFLOW, etMMError, 'CalcNumBob failed.');
    end;
  end else
    fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'CalcNumBob failed. Object is not initialized.');
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.CalcNumCones(var aNumCones: Single; aLen: longword): boolean;
begin
  Result          := False;
  aNumCones       := 0;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
  if mInitialized then begin
    if MeterPerCone <> 0 then try
//wss          aNumCones := Round ( aLen div MeterPerCone );
      aNumCones := aLen / MeterPerCone;
      Result    := True;
    except
      fError := SetError(ERROR_BUFFER_OVERFLOW, etMMError, 'CalcNumCones failed.');
    end;
  end else
    fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'CalcNumCones failed. Object is not initialized.');
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.CalcWeight(var aWeight: longword; aLen: longword): boolean;
begin
  Result          := False;
  aWeight         := 0;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
  if mInitialized then begin
    try
      // Yarncount auf DB sollte immer Nm sein
      if YarnCnt <> 0 then begin
        aWeight := Round(aLen / YarnCnt);
        Result  := True;
      end else
        // somthing got wrong with the YarnCnt
        fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'CalcWeight failed. YarnCnt=0.');
    except
      fError := SetError(ERROR_BUFFER_OVERFLOW, etMMError, 'CalcWeight failed.');
    end;
  end else
    fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'CalcWeight failed. Object is not initialized.');
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.GetYarnCnt: Single;
begin
  Result := fYarnCnt;
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.GetLotslip: Double;
begin
  Result := fLotSlip;
end;
//------------------------------------------------------------------------------
function TProdGrpAssistant.GetLengthCorrFact: Double;
begin
  Result := fLengthCorrFact;
end;
//------------------------------------------------------------------------------
{ TMachNetAssistant }
//------------------------------------------------------------------------------
constructor TMachNetAssistant.Create(aNetTyp: TNetTyp; aMachID: Word);
begin
  inherited Create;
  fProdGrpAssistant := TProdGrpAssistant.Create;
  fMachID           := aMachID;
  fNetTyp           := aNetTyp;
  fError.ErrorTyp   := etNoError;
  fError.Error      := NO_ERROR;
  fError.Msg        := '';
end;
//------------------------------------------------------------------------------
destructor TMachNetAssistant.Destroy;
begin
  fProdGrpAssistant.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.Init(aQuery: TNativeAdoQuery);
begin
  ProdGrpAssistant.Init(aQuery);
end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.ProcessSpindleData({aSpdDataArr: PSpdDataArr; }aSpdDBRec: PMMDataRec);
begin
  case NetTyp of
    ntNone: begin
        fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'No net type.');
        raise EMMException.Create(fError.Msg);
      end;
    ntTXN: begin
        ProcessTXNnetSpdData(aSpdDBRec);
      end;
    ntWSC: begin
        ProcessWSCNetSpdData(aSpdDBRec);
      end;
    ntLX: begin
        ProcessLXNetSpdData(aSpdDBRec);
      end;
    ntCI: begin
        fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'No CI support for single spindle data.');
        raise EMMException.Create(fError.Msg);
      end;
  else
    fError := SetError(ERROR_INVALID_PARAMETER, etMMError, 'Unknown NetTyp : ' + IntToStr(Ord(NetTyp)));
    raise EMMException.Create(fError.Msg);
  end;
end;
//------------------------------------------------------------------------------
function TMachNetAssistant.CorrectTime(aTime: integer; aSplices: integer; aSpliceRep: integer): integer;
    { Korrektur der Spindelzeiterfassung
      Der Reiniger erfasst die Laufzeit in ms gibt sie aber in Sekunden an die ZE weiter.
      Dabei wird immer abgerundet:z.B. 11'995 -> 11 Sekunden
      Bei jedem Zyklus wird laut G.Schuerch  1.05 Sekunden (Nullabgleich) der Stillstandzeit nicht erfasst.
      Ab Version 69 1996 wurde die Nullabgleich optimiert und hiermit der Wert im Mittel nur noch 0.4- 0.5 Sekunden
      betraegt.}
    { Um es nicht unnoetig kompliziert zu machen wird eine Nullabgleichkorrektur von 0.4 Sekunden pro Zyklus gemacht khp}
var
  xBuf: integer;
begin
  try
    xBuf := Round((aSplices + aSpliceRep) * 0.9); // The Correcture is 0.5 sec per Splice and 0.4 sec Nullabgleich
    // Overflow checken
    if (MAXINT - xBuf) < aTime then Result := MAXINT
                               else Result := aTime + xBuf;
  except
    on e: Exception do
      raise Exception.Create('TMachNetAssistant.CorrectTime failed. ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.ProcessTXNNetSpdData(aSpindleData: PMMDataRec);
begin
{26.5.05 wss: Umkopieren wird nun bereits im TXNHandler gemacht
  FillChar(aSpdDBRec^, sizeof(TDBDataRec), 0);
    // Put the data from the machine spindle record to the DB spindle record
  with PSpdTXNRec(aSpdDataArr)^.baseData do begin
      //aSpdDBRec^.Len  := Round ( length / 200 );
    aSpdDBRec^.Len := length; // Nicht Laenge; sondern Anzahl Nutentrommel Impulse
    aSpdDBRec^.tRun := runTime;
    aSpdDBRec^.tOp := operatingTime;
    aSpdDBRec^.tWa := watchTime;
      // In case of overflow take only the produced len and the times
    if status = 0 then begin // no overflow if status = 0
//        aSpdDBRec^.Bob       := cops * cBobbinsFactor;
//        aSpdDBRec^.Cones     := cones * cConesFactor;
      aSpdDBRec^.Bob := cops;
      aSpdDBRec^.Cones := cones;
      aSpdDBRec^.Sp := splices;
      aSpdDBRec^.RSp := spliceRep;
      aSpdDBRec^.YB := yarnBreaks;
      aSpdDBRec^.CSys := systemCuts;
      aSpdDBRec^.LckSys := systemLocks;
      aSpdDBRec^.CUpY := upperYarnCuts;
      aSpdDBRec^.CYTot := totalYarnCuts;
      aSpdDBRec^.LSt := longStops;
      aSpdDBRec^.tLStProd := longStopTime; // momentan alle longstops in Prod
      aSpdDBRec^.CS := shortCuts;
      aSpdDBRec^.CL := longCuts;
      aSpdDBRec^.CT := thinCuts;
      aSpdDBRec^.CN := nepCuts;
      aSpdDBRec^.CSp := spliceCuts;
      aSpdDBRec^.CCl := clusterCuts;
      aSpdDBRec^.COffCnt := offcountCuts;
      aSpdDBRec^.LckOffCnt := offcountLocks;
      aSpdDBRec^.CBu := bunchCuts;
      aSpdDBRec^.CDBu := delBunchCuts;
      aSpdDBRec^.UClS := shortClustArea;
      aSpdDBRec^.UClL := longClustArea;
      aSpdDBRec^.UClT := thinClustArea;
      aSpdDBRec^.CSIRO := siroCuts;
      aSpdDBRec^.LckSiro := siroLocks;
      aSpdDBRec^.CSIROCl := siroClusterCuts;
      aSpdDBRec^.LckSIROCl := siroClusterLocks;
      aSpdDBRec^.LckCl := clusterLocks;
      aSpdDBRec^.CSfi := sfiCuts;
      aSpdDBRec^.LckSfi := sfiLocks;
      aSpdDBRec^.SFICnt := variCnt;
      aSpdDBrec^.SFI := variTotal;
      aSpdDBRec^.AdjustBase := AdjustBase; // Wss
      if AdjustBase > 0 then
        aSpdDBRec^.AdjustBaseCnt := 1
      else
        aSpdDBRec^.AdjustBaseCnt := 0;

        // initialize the class cut and uncut data
      try
        ConvertTXNDefectData(@aSpdDBRec^.ClassFieldRec,
          @PSpdTXNRec(aSpdDataArr)^.defectData,
          @PSpdTXNRec(aSpdDataArr)^.siroData);
      except
        fError := SetError(ERROR_NOT_ENOUGH_MEMORY, etNTError, 'Can not convert defect cross table.');
        raise EMMException.Create(fError.Msg);
      end;
    end;
  end;
{}

  //wss: Diese Zeilen jedoch m�ssen bleiben, damit gewisse Produktionsdaten noch berechnet/korrigiert werden k�nnen
  // Korrektur der Spindelzeiterfassung
  // Der Reiniger erfasst die Laufzeit in ms gibt sie aber in Sekunden an die ZE weiter.
  // Dabei wird immer abgerundet:z.B. 11'995 -> 11 Sekunden
  aSpindleData^.tWa  := CorrectTime(aSpindleData^.tWa,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tOp  := CorrectTime(aSpindleData^.tOp,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tRun := CorrectTime(aSpindleData^.tRun, aSpindleData^.Sp, aSpindleData^.RSp);

  //     In Abh�ngigkeit von Partieinformationen. Eventuell kann dies dann generell in
  //     GetSpdDBData() gemacht werden
  // calculate the values which are not served by machine and
  // Calculate Len with LengthCorrFact:= (LotSlip and DrumCorrfact)/200.
  CalcTXNSpdValues(aSpindleData);
end;
//------------------------------------------------------------------------------
//procedure TMachNetAssistant.CalcTXNProdGrpValues(aProdGrpDBrec: PMMDataRec);
//begin
//
//end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.CalcWSCSpdValues(aSpindleData: PMMDataRec);
begin
  try
    // Calculate Len with Slip
    if not ProdGrpAssistant.CalcRealLen(aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
    // Calculate Weight according to YarnCount
    if not ProdGrpAssistant.CalcWeight(aSpindleData^.Wei, aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
  except
    on e: Exception do
      raise Exception.Create('TMachNetAssistant.CalcWSCSpdValues failed. ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.ProcessWSCNetSpdData(aSpindleData: PMMDataRec);
begin
{26.5.05 wss: Umkopieren wird nun bereits im WSCHandler gemacht
  FillChar(aSpdDBRec^, sizeof(TDBDataRec), 0);
    // Put the data from the machine spindle record to the DB spindle record
  with PSpdWSCRec(aSpdDataArr)^.baseData do begin
      // aSpdDBRec^.Len  := Round ( length / 200 );
    aSpdDBRec^.Len := length; // Nicht Laenge; sondern Anzahl Nutentrommel Impulse
    aSpdDBRec^.tRun := runTime; // tRun= Laufzeit der Spindel
    aSpdDBRec^.tOp := operatingTime; // tOp= Zeit in welcher mindestens eine Spindel in der Gruppe laeuft
    aSpdDBRec^.tWa := watchTime; // tWA= Laufzeit + Stopzeit der Spindel
      // In case of overflow take only the produced len and the times
    if status = 0 then begin // no overflow if status = 0
//wss        aSpdDBRec^.Bob       := cops * cBobbinsFactor;
//wss        aSpdDBRec^.Cones     := cones * cConesFactor;
      aSpdDBRec^.Bob := cops;
      aSpdDBRec^.Cones := cones;
      aSpdDBRec^.Sp := splices;
      aSpdDBRec^.RSp := spliceRep;
      aSpdDBRec^.YB := yarnBreaks;
      aSpdDBRec^.CSys := systemCuts;
      aSpdDBRec^.LckSys := systemLocks;
      aSpdDBRec^.CUpY := upperYarnCuts;
      aSpdDBRec^.CYTot := totalYarnCuts;
      aSpdDBRec^.LSt := longStops;
      aSpdDBRec^.tLStProd := longStopTime; // momentan alle longstops in Prod
      aSpdDBRec^.CS := shortCuts;
      aSpdDBRec^.CL := longCuts;
      aSpdDBRec^.CT := thinCuts;
      aSpdDBRec^.CN := nepCuts;
      aSpdDBRec^.CSp := spliceCuts;
      aSpdDBRec^.CCl := clusterCuts;
      aSpdDBRec^.COffCnt := offcountCuts;
      aSpdDBRec^.LckOffCnt := offcountLocks;
      aSpdDBRec^.CBu := bunchCuts;
      aSpdDBRec^.CDBu := delBunchCuts;
      aSpdDBRec^.UClS := shortClustArea;
      aSpdDBRec^.UClL := longClustArea;
      aSpdDBRec^.UClT := thinClustArea;
      aSpdDBRec^.CSIRO := siroCuts;
      aSpdDBRec^.LckSiro := siroLocks;
      aSpdDBRec^.CSIROCl := siroClusterCuts;
      aSpdDBRec^.LckSIROCl := siroClusterLocks;
      aSpdDBRec^.LckCl := clusterLocks;
      aSpdDBRec^.CSfi := sfiCuts;
      aSpdDBRec^.LckSfi := sfiLocks;
      aSpdDBRec^.SFICnt := variCnt;
      aSpdDBrec^.SFI := variTotal;
      aSpdDBRec^.AdjustBase := AdjustBase; // Wss
      if AdjustBase > 0 then
        aSpdDBRec^.AdjustBaseCnt := 1
      else
        aSpdDBRec^.AdjustBaseCnt := 0;
        // initialize the class cut and uncut data
      try
        ConvertWSCDefectData(@aSpdDBRec^.ClassFieldRec,
          @PSpdWSCRec(aSpdDataArr)^.defectData,
          @PSpdWSCRec(aSpdDataArr)^.siroData);
      except
        fError := SetError(ERROR_NOT_ENOUGH_MEMORY, etNTError, 'Can not convert defect cross table.');
        raise EMMException.Create(fError.Msg);
      end;
    end;
  end;
{}
  //wss: Diese Zeilen jedoch m�ssen bleiben, damit gewisse Produktionsdaten noch berechnet/korrigiert werden k�nnen
  // Korrektur der Spindelzeiterfassung
  // Der Reiniger erfasst die Laufzeit in ms gibt sie aber in Sekunden an die ZE weiter.
  // Dabei wird immer abgerundet:z.B. 11'995 -> 11 Sekunden
  aSpindleData^.tWa  := CorrectTime(aSpindleData^.tWa,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tOp  := CorrectTime(aSpindleData^.tOp,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tRun := CorrectTime(aSpindleData^.tRun, aSpindleData^.Sp, aSpindleData^.RSp);
  //     In Abh�ngigkeit von Partieinformationen. Eventuell kann dies dann generell in
  //     GetSpdDBData() gemacht werden
  CalcWSCSpdValues(aSpindleData);
end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.CalcTXNSpdValues(aSpindleData: PMMDataRec);
var
  xCones: Single;
  xBobbins: Single;
begin
  try
    // Calculate Len with Slip
    if not ProdGrpAssistant.CalcRealLen(aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
    // Calculate Weight according to YarnCount
    if not ProdGrpAssistant.CalcWeight(aSpindleData^.Wei, aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
    // calculate cones if not available in data record
    if not GetAvailabilityOfDataItem(ProdGrpAssistant.VoidData, vdCones) then begin
      if not ProdGrpAssistant.CalcNumCones(xCones, aSpindleData^.Len) then begin
        fError := ProdGrpAssistant.Error;
        raise EMMException.Create(fError.Msg);
      end;
      aSpindleData^.Cones := xCones;
    end;
    // calculate bobbins if not available in data record
    if not GetAvailabilityOfDataItem(ProdGrpAssistant.VoidData, vdCops) then begin
      if not ProdGrpAssistant.CalcNumBob(xBobbins, aSpindleData^.Len) then begin
        fError := ProdGrpAssistant.Error;
        raise EMMException.Create(fError.Msg);
      end;
      aSpindleData^.Bob := xBobbins;
    end;
  except
    on e: Exception do
      raise Exception.Create('TMachNetAssistant.CalcTXNSpdValues failed. ' + e.Message);
  end;
end;

//------------------------------------------------------------------------------
//procedure TMachNetAssistant.CalcTXNProdGrpValues(aProdGrpDBrec: PMMDataRec);
//begin
//
//end;
//------------------------------------------------------------------------------
procedure TMachNetAssistant.CalcLXSpdValues(aSpindleData: PMMDataRec);
begin
  try
    // Calculate Len with Slip
    if not ProdGrpAssistant.CalcRealLen(aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
{ TODO 1 -oNue -cLX-Einbau : Kontrolle YarnCount mit KHP }    // Calculate Weight according to YarnCount
    if not ProdGrpAssistant.CalcWeight(aSpindleData^.Wei, aSpindleData^.Len) then begin
      fError := ProdGrpAssistant.Error;
      raise EMMException.Create(fError.Msg);
    end;
  except
    on e: Exception do
      raise Exception.Create('TMachNetAssistant.CalcLXSpdValues failed. ' + e.Message);
  end;
end;

//------------------------------------------------------------------------------
procedure TMachNetAssistant.ProcessLXNetSpdData(aSpindleData: PMMDataRec);
begin
  //wss: Diese Zeilen jedoch m�ssen bleiben, damit gewisse Produktionsdaten noch berechnet/korrigiert werden k�nnen
  // Korrektur der Spindelzeiterfassung
  // Der Reiniger erfasst die Laufzeit in ms gibt sie aber in Sekunden an die ZE weiter.
  // Dabei wird immer abgerundet:z.B. 11'995 -> 11 Sekunden
  aSpindleData^.tWa  := CorrectTime(aSpindleData^.tWa,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tOp  := CorrectTime(aSpindleData^.tOp,  aSpindleData^.Sp, aSpindleData^.RSp);
  aSpindleData^.tRun := CorrectTime(aSpindleData^.tRun, aSpindleData^.Sp, aSpindleData^.RSp);
  //     In Abh�ngigkeit von Partieinformationen. Eventuell kann dies dann generell in
  //     GetSpdDBData() gemacht werden
  CalcLXSpdValues(aSpindleData);
end;

//------------------------------------------------------------------------------
// TImpAssistant
//------------------------------------------------------------------------------
constructor TIPAssistant.Create(aProdGrpAssistant: TProdGrpAssistant);
begin
  inherited Create;
  mProdGrpAssistant := aProdGrpAssistant;
  mIPOption         := ioNotDefined;
end;
//------------------------------------------------------------------------------
function TIPAssistant.CalcImperfections(var aImpRec: TImpRec; aClassFields: TMMClassUncutField): boolean;
begin
  Result          := True;
  fError.ErrorTyp := etNoError;
  fError.Error    := NO_ERROR;
  FillChar(aImpRec, sizeof(TImpRec), 0);
  case mProdGrpAssistant.IPOption of
    ioIPNO: begin
          // the machine has no Imperfections
      end;
    ioIPOld: begin
          // the imperfections get the factor from the database
        if (mProdGrpAssistant.IPOption <> mIPOption) then begin
          mIPOption := mProdGrpAssistant.IPoption;
          Result    := InitFactorsByDatabase;
        end;
        if Result then
          Result := GetImperfections(aImpRec, aClassFields);
      end;
    ioIPNew: begin
          // all the factors are one
        if (mProdGrpAssistant.IPOption <> mIPOption) then begin
          mIPOption := mProdGrpAssistant.IPoption;
          InitFactorsByConstants;
        end;
        if Result then
          Result := GetImperfections(aImpRec, aClassFields);
      end;
  else
      // somthing got wrong with the IPOption
    Result := False;
    fError := SetError(ERROR_INVALID_HANDLE, etMMError, 'Not supported IP option');
  end;
end;
//------------------------------------------------------------------------------
procedure TIPAssistant.InitFactorsByConstants;
begin
  mFactNeps  := 1.0;
  mFactSmall := 1.0;
  mFactThick := 1.0;
  mFactThin  := 1.0;
  mFact2_4   := 1.0;
  mFact4_8   := 1.0;
  mFact8_20  := 1.0;
  mFact20_70 := 1.0;
end;
//------------------------------------------------------------------------------
function TIPAssistant.InitFactorsByDatabase: boolean;
var
  xStrList: TStringList;
begin
//TODO wss: F�r was wird die Korrektur innerhalb des StorageHandlers ben�tigt? Wird das �berhaupt irgendwo verwendet?
// -> Problem: StrToFloat() mit Regional Settings ber�cksichtigen -> wenn schon MMStrToFloat
  Result := True;
  xStrList := TStringList.Create;
  with mProdGrpAssistant.mQuery do
  try
    Close;
    SQL.Text := cFactorQuery;
    try
      Open;
      if not FindFirst then begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitFactorsByDatabase failed. No Imperfection Factors available.');
        Result := False;
        Exit;
      end;
      xStrList.CommaText := FieldByName('data').AsString;
    except
      on E: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitFactorsByDatabase failed.' + e.message);
        Result := False;
      end;
    end;
    //Order : AppKey="N,S,Tk,Tn,L24,L48,L820,L2070"
    if Result then
    try
      mFactNeps  := StrToFloat(xStrList.Strings[0]) / cImpFactFactor;
      mFactSmall := StrToFloat(xStrList.Strings[1]) / cImpFactFactor;
      mFactThick := StrToFloat(xStrList.Strings[2]) / cImpFactFactor;
      mFactThin  := StrToFloat(xStrList.Strings[3]) / cImpFactFactor;
      mFact2_4   := StrToFloat(xStrList.Strings[4]) / cImpFactFactor;
      mFact4_8   := StrToFloat(xStrList.Strings[5]) / cImpFactFactor;
      mFact8_20  := StrToFloat(xStrList.Strings[6]) / cImpFactFactor;
      mFact20_70 := StrToFloat(xStrList.Strings[7]) / cImpFactFactor;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_DATA, etMMError, 'Read of Imperfection factores failed.' + e.message);
        Result := False;
      end;
    end;
  finally
    xStrList.Free;
  end;
end;
//------------------------------------------------------------------------------
function TIPAssistant.GetImperfections(var aImpRec: TImpRec; aClassFields: TMMClassUncutField): boolean;
var
  xBuf: Double;
  i: Word;
  //........................................................
  function CheckAndSetBuf(aBuf: Double; var aImp: longword): boolean;
  begin
    Result := (xBuf <= High(longword));
    if Result then
      aImp   := Round(xBuf)
    else begin
      aImp            := High(longword);
      fError.ErrorTyp := etMMError;
      fError.Error    := ERROR_BUFFER_OVERFLOW;
    end;
  end;
  //........................................................
begin
  Result := True;
  with aImpRec do
  try
    (* Neps *)
    xBuf := 0.0;
    for i:=1 to 8 do
      xBuf := xBuf + aClassFields[i];
    xBuf := xBuf * mFactNeps;
    Result := Result and CheckAndSetBuf(xBuf, Neps);

    (* Thick *)
    xBuf := 0.0;
    i := 70;
    while (i <= 118) do begin
      xBuf := xBuf + aClassFields[i];
      xBuf := xBuf + aClassFields[i + 1];
      xBuf := xBuf + aClassFields[i + 2];
      Inc(i, 8);
    end;
    xBuf := xBuf * mFactThick;
    Result := Result and CheckAndSetBuf(xBuf, Thick);

    (* Small *)
    xBuf := 0.0;
    i := 67;
    while (i <= 115) do begin
      xBuf := xBuf + aClassFields[i];
      xBuf := xBuf + aClassFields[i + 2]; // wss: die Zeile direkt unter dem Faden wird bewusst ausgelassen!!
      Inc(i, 8);
    end;
    xBuf := xBuf * mFactSmall;
    Result := Result and CheckAndSetBuf(xBuf, Small);

    (* Thin *)
    xBuf := 0.0;
    i := 65;
    while (i <= 113) do begin
      xBuf := xBuf + aClassFields[i];
      xBuf := xBuf + aClassFields[i + 1];
      Inc(i, 8);
    end;
    xBuf := xBuf * mFactThin;
    Result := Result and CheckAndSetBuf(xBuf, Thin);

    (* I2_4 *)
    xBuf := 0.0;
    for i:=33 to 48 do
      xBuf := xBuf + aClassFields[i];
    for i:=69 to 72 do
      xBuf := xBuf + aClassFields[i];
    for i:=65 to 67 do
      xBuf := xBuf + aClassFields[i];

    xBuf   := xBuf * mFact2_4;
    Result := Result and CheckAndSetBuf(xBuf, I2_4);

    (* I4_8 *)
    xBuf := 0.0;
    for i:=49 to 56 do
      xBuf := xBuf + aClassFields[i];
    for i:=73 to 88 do
      if (i <> 76) and (i <> 84) then
        xBuf := xBuf + aClassFields[i];

    xBuf   := xBuf * mFact4_8;
    Result := Result and CheckAndSetBuf(xBuf, I4_8);

    (* I8_20 *)
    xBuf := 0.0;
    for i:=89 to 104 do
      if (i <> 92) and (i <> 100) then
        xBuf := xBuf + aClassFields[i];

    xBuf   := xBuf * mFact8_20;
    Result := Result and CheckAndSetBuf(xBuf, I8_20);

    (* I20_70 *)
    xBuf := 0.0;
    for i:=105 to 120 do
      if (i <> 108) and (i <> 116) then
        xBuf := xBuf + aClassFields[i];

    xBuf   := xBuf * mFact20_70;
    Result := Result and CheckAndSetBuf(xBuf, I20_70);
  except
    fError := SetError(ERROR_INVALID_FUNCTION, etMMError, 'GetImperfections failed.');
    Result := False;
  end;
end;
//------------------------------------------------------------------------------
function TIPAssistant.GetIPAvailable: boolean;
begin
  case mProdGrpAssistant.IPOption of
    ioIPNO:  Result := False;
    ioIPOld: Result := True;
    ioIPNew: Result := True;
  else
    Result := False;
  end;
end;
//------------------------------------------------------------------------------
// TDBDataAssistant
//------------------------------------------------------------------------------
constructor TDBDataAssistant.Create(aEventLog: TEventLogWriter);
begin
  inherited Create;
  mEventLog := aEventLog;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryByBaseData(aQuery: TNativeAdoQuery; aRec: PMMDataRec): boolean;
begin
  Result := True;
  with aQuery, aRec^ do
  try
    Params.ParamByName('c_Len').AsInteger               := CheckLongWordToInt(Len, 'c_Len');
//TODO 1 -onue/wss -cSystem : DDen Datentyp f�r c_wei �berarbeiten (zur Zeit auf DB numeric(9,2)). Es kann hier mit der Demoversion der ZE �berlauffehler geben!
    Params.ParamByName('c_Wei').AsInteger               := CheckLongWordToInt(Wei, 'c_Wei');
    Params.ParamByName('c_Bob').AsFloat                 := Bob;
    Params.ParamByName('c_Cones').AsFloat               := Cones;
    Params.ParamByName('c_Sp').AsInteger                := CheckInt(Sp, 'c_Sp');
    Params.ParamByName('c_RSp').AsInteger               := CheckInt(RSp, 'c_RSp');
    Params.ParamByName('c_YB').AsInteger                := CheckInt(YB, 'c_YB');
    Params.ParamByName('c_CSys').AsInteger              := CheckInt(CSys, 'c_CSys');
    Params.ParamByName('c_LckSys').AsInteger            := CheckInt(LckSys, 'c_LckSys');
    Params.ParamByName('c_CUpY').AsInteger              := CheckInt(CUpY, 'c_CUpY');
    Params.ParamByName('c_CYTot').AsInteger             := CheckInt(CYTot, 'c_CYTot');
    Params.ParamByName('c_LSt').AsInteger               := CheckInt(LSt, 'c_LSt');
    Params.ParamByName('c_LStProd').AsInteger           := CheckInt(tLStProd, 'c_LStProd');
    Params.ParamByName('c_LStBreak').AsInteger          := CheckInt(tLStBreak, 'c_LStBreak');
    Params.ParamByName('c_LStMat').AsInteger            := CheckInt(tLStMat, 'c_LStMat');
    Params.ParamByName('c_LStRev').AsInteger            := CheckInt(tLStRev, 'c_LStRev');
    Params.ParamByName('c_LStClean').AsInteger          := CheckInt(tLStClean, 'c_LStClean');
    Params.ParamByName('c_LStDef').AsInteger            := CheckInt(tLStDef, 'c_LStDef');
    Params.ParamByName('c_Red').AsInteger               := CheckInt(Red, 'c_Red');
    Params.ParamByName('c_tRed').AsInteger              := CheckInt(tRed, 'c_tRed');
    Params.ParamByName('c_tYell').AsInteger             := CheckInt(tYell, 'c_tYell');
    Params.ParamByName('c_ManSt').AsInteger             := CheckInt(ManSt, 'c_ManSt');
    Params.ParamByName('c_tManSt').AsInteger            := CheckInt(tManSt, 'c_tManSt');
    Params.ParamByName('c_rtSpd').AsInteger             := CheckInt(tRun, 'c_rtSpd');
    Params.ParamByName('c_otSpd').AsInteger             := CheckInt(tOp, 'c_otSpd');
    Params.ParamByName('c_wtSpd').AsInteger             := CheckInt(tWa, 'c_wtSpd');
    Params.ParamByName('c_otProdGrp').AsInteger         := CheckInt(totProdGrp, 'c_otProdGrp');
    Params.ParamByName('c_wtProdGrp').AsInteger         := CheckInt(twtProdGrp, 'c_wtProdGrp');
    Params.ParamByName('c_CS').AsInteger                := CheckInt(CS, 'c_CS');
    Params.ParamByName('c_CL').AsInteger                := CheckInt(CL, 'c_CL');
    Params.ParamByName('c_CT').AsInteger                := CheckInt(CT, 'c_CT');
    Params.ParamByName('c_CN').AsInteger                := CheckInt(CN, 'c_CN');
    Params.ParamByName('c_CSp').AsInteger               := CheckInt(CSp, 'c_CSp');
    Params.ParamByName('c_CClS').AsInteger              := CheckInt(CClS, 'c_CClS');  // Mai 2005
    Params.ParamByName('c_LckClS').AsInteger            := CheckInt(LckClS, 'c_LckClS'); // Mai 2005
//TODO wss: Daten pr�fen und abgleichen mit DB-Feldern -> Sp+, Zenit
    Params.ParamByName('c_COffCnt').AsInteger           := CheckInt(COffCnt, 'c_COffCnt');
    Params.ParamByName('c_LckOffCnt').AsInteger         := CheckInt(LckOffCnt, 'c_LckOffCnt');

    Params.ParamByName('c_CBu').AsInteger               := CheckInt(CBu, 'c_CBu');
    Params.ParamByName('c_CDBu').AsInteger              := CheckInt(CDBu, 'c_CDBu');
    Params.ParamByName('c_UClS').AsFloat                := CheckFloat(UClS, 99999999999, 'c_UClS');
    Params.ParamByName('c_UClL').AsFloat                := CheckFloat(UClL, 99999999999, 'c_UClL');
    Params.ParamByName('c_UClT').AsFloat                := CheckFloat(UClT, 99999999999, 'c_UClT');
    // Siro
    Params.ParamByName('c_CSIRO').AsInteger             := CheckInt(CSIRO, 'c_CSIRO');
    Params.ParamByName('c_LckSIRO').AsInteger           := CheckInt(LckSiro, 'c_LckSIRO');
    Params.ParamByName('c_CSIROCl').AsInteger           := CheckInt(CSiroCl, 'c_CSIROCl');
    Params.ParamByName('c_LckSIROCl').AsInteger         := CheckInt(LckSIROCl, 'c_LckSIROCl');
    // SFI
    Params.ParamByName('c_CSfi').AsInteger              := CheckInt(CSfi, 'c_CSfi');
    Params.ParamByName('c_LckSfi').AsInteger            := CheckInt(LckSfi, 'c_LckSfi');
    Params.ParamByName('c_SFI').AsInteger               := CheckInt(SFI, 'c_SFI');
    Params.ParamByName('c_SFICnt').AsInteger            := CheckInt(SFICnt, 'c_SFICnt');
    //Wss: valid range for AdjustBase: 120..6250 (Isidor Kuestner)
    Params.ParamByName('c_AdjustBase').AsFloat          := CheckFloat(AdjustBase, 99999999999, 'c_AdjustBase');
    Params.ParamByName('c_AdjustBaseCnt').AsInteger     := AdjustBaseCnt;

    // Imperfections
    Params.ParamByName('c_INeps').AsInteger             := CheckInt(Imp.Neps, 'c_INeps');
    Params.ParamByName('c_IThick').AsInteger            := CheckInt(Imp.Thick, 'c_IThick');
    Params.ParamByName('c_IThin').AsInteger             := CheckInt(Imp.Thin, 'c_IThin');
    Params.ParamByName('c_ISmall').AsInteger            := CheckInt(Imp.Small, 'c_ISmall');
    Params.ParamByName('c_I2_4').AsInteger              := CheckInt(Imp.I2_4, 'c_I2_4');
    Params.ParamByName('c_I4_8').AsInteger              := CheckInt(Imp.I4_8, 'c_I4_8');
    Params.ParamByName('c_I8_20').AsInteger             := CheckInt(Imp.I8_20, 'c_I8_20');
    Params.ParamByName('c_I20_70').AsInteger            := CheckInt(Imp.I20_70, 'c_I20_70');
    // Mai 2005
    Params.ParamByName('c_CP').AsInteger                := CheckInt(CP, 'c_CP');
    Params.ParamByName('c_CClL').AsInteger              := CheckInt(CClL, 'c_CClL');
    Params.ParamByName('c_CClT').AsInteger              := CheckInt(CClT, 'c_CClT');
    Params.ParamByName('c_CShortOffCntPlus').AsInteger  := CheckInt(CShortOffCntPlus, 'c_CShortOffCntPlus');
    Params.ParamByName('c_LckP').AsInteger              := CheckInt(LckP, 'c_LckP');
    Params.ParamByName('c_LckClL').AsInteger            := CheckInt(LckClL, 'c_LckClL');
    Params.ParamByName('c_LckClT').AsInteger            := CheckInt(LckClT, 'c_LckClT');
    Params.ParamByName('c_LckShortOffCnt').AsInteger    := CheckInt(LckShortOffCnt, 'c_LckShortOffCnt');
    Params.ParamByName('c_LckNSLT').AsInteger           := CheckInt(LckNSLT, 'c_LckNSLT');
    Params.ParamByName('c_COffCntPlus').AsInteger       := CheckInt(COffCntPlus, 'c_COffCntPlus');
    Params.ParamByName('c_COffCntMinus').AsInteger      := CheckInt(COffCntMinus, 'c_COffCntMinus');
    Params.ParamByName('c_CShortOffCnt').AsInteger      := CheckInt(CShortOffCnt, 'c_CShortOffCnt');
    Params.ParamByName('c_CShortOffCntMinus').AsInteger := CheckInt(CShortOffCntMinus, 'c_CShortOffCntMinus');
    Params.ParamByName('c_CDrumWrap').AsInteger         := CheckInt(CDrumWrap, 'c_CDrumWrap');
    // Februar 2008 VCV, Nue:12.02.08
    Params.ParamByName('c_CVCV').AsInteger              := CheckInt(CVCV, 'c_CVCV');
    Params.ParamByName('c_LckVCV').AsInteger            := CheckInt(LckVCV, 'c_LckVCV');
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'IntQueryByBaseData failed.' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryByClassCutData(aQuery: TNativeAdoQuery; aClassCut: PMMClassCutField): boolean;
var
  i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cClassCutFields do
      Params.ParamByName('c_cC' + IntToStr(i)).AsInteger := aClassCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryByClassCutData failed: ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryByClassUnCutData(aQuery: TNativeAdoQuery; aClassUnCut: PMMClassUnCutField): boolean;
var
  i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cClassUnCutFields do
      Params.ParamByName('c_cU' + IntToStr(i)).AsInteger := aClassUnCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryByClassUncutData failed: ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryBySpliceCutData(aQuery: TNativeAdoQuery; aSpliceCut: PMMSpCutField): boolean;
var
  i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cSpCutFields do
      Params.ParamByName('c_spC' + IntToStr(i)).AsInteger := aSpliceCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryBySpliceCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryBySpliceUnCutData(aQuery: TNativeAdoQuery; aSpliceUnCut: PMMSpUnCutField): boolean;
var i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cSpUnCutFields do
      Params.ParamByName('c_spU' + IntToStr(i)).AsInteger := aSpliceUnCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryBySpliceUnCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryBySiroCutData(aQuery: TNativeAdoQuery; aSiroCut: PMMSiroCutField): boolean;
var
  i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cSiroCutFields do
      Params.ParamByName('c_siC' + IntToStr(i)).AsInteger := aSiroCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryBySiroCutData failed: ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitQueryBySiroUnCutData(aQuery: TNativeAdoQuery; aSiroUnCut: PMMSiroUnCutField): boolean;
var
  i: integer;
begin
  Result := True;
  with aQuery do
  try
    for i:=1 to cSiroUnCutFields do
      Params.ParamByName('c_siU' + IntToStr(i)).AsInteger := aSiroUnCut^[i];
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitQueryBySiroUnCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.CheckInt(aPar: longInt; aParTyp: ShortString):
    integer;
begin
//Alt bis 20.6.06(Nue): Neue Methode CheckLongWordToInt added und bei CheckInt Parameter aPar von longword zu longint ge�ndert.
//  if aPar <= LongWord(MAXINT) then
//    Result := aPar
//  else begin
//    Result := MAXINT;
//    mEventLog.Write(etWarning, fDefaultErrStr + 'Value to high for integer type. Parameter : ' + aParTyp);
//  end;
  Result := aPar
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.CheckFloat(aPar, aMaxValue: Single; aParTyp: Shortstring): Single;
begin
  if aPar > aMaxValue then begin
    Result := aMaxValue;
    mEventLog.Write(etWarning, fDefaultErrStr + 'Value to high for integer type in CheckFloat. Parameter : ' + aParTyp);
  end else
    Result := aPar;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.CheckLongWordToInt(aPar: longword; aParTyp:
    ShortString): Integer;
begin
  if aPar <= LongWord(MAXINT) then
    Result := aPar
  else begin
    Result := MAXINT;
    mEventLog.Write(etWarning, fDefaultErrStr + 'Value to high for integer type. Parameter : ' + aParTyp);
  end;
end;

//------------------------------------------------------------------------------
function TDBDataAssistant.AddInt(aSum, aAdd: integer; aName: string): integer;
begin
  Result := 0;
//Alt bis 21.6.06
//  if (aSum < 0) or (aAdd < 0) then begin
//    mEventLog.Write(etWarning, fDefaultErrStr + ' Negative value : ' + aName);
//    aSum := 0;
//    aAdd := 0;
//  end;
//  try
//    if (high(integer) - aSum) >= aAdd then begin
//      Result := aSum + aAdd;
//    end
//    else begin
//      Result := high(integer);
////      WriteToEventLogDebug(Format(fDefaultErrStr + ' Overflow by adding values in AddInt. Value: %s, %d, %d', [aName, aSum, aAdd]), '', etWarning, ssStorageHandler);
////      mEventLog.Write(etWarning, Format(fDefaultErrStr + ' Overflow by adding values. Value: %s, %d, %d', [aName, aSum, aAdd]));
//    end;
//  except
//    mEventLog.Write(etError, fDefaultErrStr + ' Fatalerror by adding values. Value : ' + aName);
//  end;

  if (aSum < 0) or (aAdd < 0) then begin
    mEventLog.Write(etWarning, Format(fDefaultErrStr + ' Negative value for Sum (%d) or Add (%d) in AddInt. Value: %s', [aSum, aAdd, aName]));
  end;

  try
    if (aSum >= 0) then begin
      //Check Positiver Ueberlauf
      if (high(integer) - aSum) >= aAdd then begin
        Result := aSum + aAdd;
      end
      else begin
        Result := high(integer);
      end;
    end
    else begin
      //Check Negativer Ueberlauf
      if (low(integer) - aSum) <= aAdd then begin
        Result := aSum + aAdd;
      end
      else begin
        Result := low(integer);
      end;
    end;
  except
    mEventLog.Write(etError, fDefaultErrStr + ' Fatalerror by adding values in AddInt. Value : ' + aName);
  end;
end;
//------------------------------------------------------------------------------
  { TODO 1 -oLOK -cDebug : Tempor�r eingef�gt }
function TDBDataAssistant.AddSmallInt(aSum, aAdd: integer; aName: string): integer;
begin
  Result := 0;
//  if (aSum < 0) or (aAdd < 0) then begin
//    mEventLog.Write(etWarning, fDefaultErrStr + ' Negative value : ' + aName);
//    aSum := 0;
//    aAdd := 0;
//  end;
//  try
//    if (high(smallint) - aSum) >= aAdd then begin
//      Result := aSum + aAdd;
//    end
//    else begin
//      Result := high(smallint);
////      WriteToEventLogDebug(Format(fDefaultErrStr + ' Overflow by adding values in AddSmallInt. Value: %s, %d, %d', [aName, aSum, aAdd]), '', etWarning, ssStorageHandler);
////      mEventLog.Write(etWarning, Format(fDefaultErrStr + ' Overflow by adding values. Value: %s, %d, %d', [aName, aSum, aAdd]));
//    end;
//  except
//    mEventLog.Write(etError, fDefaultErrStr + ' Fatalerror by adding values. Value : ' + aName);
//  end;

  //aSum auf smallint limitieren
  if aSum > high(smallint) then
    aSum := high(smallint)
  else if aSum < low(smallint) then
    aSum := low(smallint);

  //aAdd auf smallint limitieren
  if aAdd > high(smallint) then
    aAdd := high(smallint)
  else if aAdd < low(smallint) then
    aAdd := low(smallint);


  if (aSum < 0) or (aAdd < 0) then begin
    mEventLog.Write(etWarning, Format(fDefaultErrStr + ' Negative value for Sum (%d) or Add (%d) in AddSmallint. Value: %s', [aSum, aAdd, aName]));
  end;

  try
    if (aSum >= 0) then begin
      //Check Positiver Ueberlauf
      if (high(smallint) - aSum) >= aAdd then begin
        Result := aSum + aAdd;
      end
      else begin
        Result := high(smallint);
      end;
    end
    else begin
      //Check Negativer Ueberlauf
      if (low(smallint) - aSum) <= aAdd then begin
        Result := aSum + aAdd;
      end
      else begin
        Result := low(smallint);
      end;
    end;
  except
    mEventLog.Write(etError, fDefaultErrStr + ' Fatalerror by adding values in AddSmallInt. Value : ' + aName);
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddFloat(aSum, aAdd, aMaxValue: Single; aName: string): Single; // for adding floats and check the range
//ACHTUNG: Negative Werte werden auf 0.0 modifiziert!!!!!
begin
  Result := 0.0;
  if (aSum < 0.0) or (aAdd < 0.0) then begin
    mEventLog.Write(etWarning, fDefaultErrStr + ' Negative value in AddFloat : ' + aName);
    aSum := 0;
    aAdd := 0;
  end;
  try
    if (aSum + aAdd) <= aMaxValue then begin
      Result := aSum + aAdd;
    end
    else begin
      Result := aMaxValue;
//      WriteToEventLogDebug(Format(fDefaultErrStr + ' Overflow by adding values in AddFloat. Value: %s, %d, %d', [aName, aSum, aAdd]), '', etWarning, ssStorageHandler);
//      mEventLog.Write(etWarning, Format(fDefaultErrStr + ' Overflow by adding values in AddFloat. Value: %s, %f, %f, %f', [aName, aSum, aAdd, aMaxValue]));
    end;
  except
    mEventLog.Write(etError, fDefaultErrStr + ' Fatalerror by adding values in AddFloat. Value : ' + aName);
  end;


end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddBaseData(aRec: PMMDataRec; aQuery: TNativeAdoQuery): boolean;
var
  xAdjustBaseCnt: Integer;
begin
  Result := True;
  with aRec^, aQuery do begin
    try
//Alt bis 20.6.06 (nue)     Len        := AddInt(Len, FieldByName('c_Len').AsInteger, 'c_Len');
      Len        := AddInt(CheckLongWordToInt(Len,'c_Len'), FieldByName('c_Len').AsInteger, 'c_Len');
{ TODO 1 -onue/wss -cSystem : DDen Datentyp f�r c_wei �berarbeiten (zur Zeit auf DB numeric(9,2)). Es kann hier mit der Demoversion der ZE �berlauffehler geben! }
      Wei        := Trunc(AddFloat(Wei, FieldByName('c_Wei').AsInteger, 4294967295, 'c_Wei')); // wss: 4 giga da im Record als LongWord definiert
//    Wei        := Trunc(AddFloat(Wei, FieldByName('c_Wei').AsInteger, 9999999, 'c_Wei'));
      Bob        := Bob + FieldByName('c_Bob').AsFloat;
      Cones      := Cones + FieldByName('c_Cones').AsFloat;
      Sp         := AddInt(Sp, FieldByName('c_Sp').AsInteger, 'c_Sp');
      RSp        := AddInt(RSp, FieldByName('c_RSp').AsInteger, 'c_RSp');
      YB         := AddInt(YB, FieldByName('c_YB').AsInteger, 'c_YB');
      CSys       := AddInt(CSys, FieldByName('c_CSys').AsInteger, 'c_CSys');
      LckSys     := AddInt(LckSys, FieldByName('c_LckSys').AsInteger, 'c_CSys');
      CUpY       := AddInt(CUpY, FieldByName('c_CUpY').AsInteger, 'c_CUpY');
      CYTot      := AddInt(CYTot, FieldByName('c_CYTot').AsInteger, 'c_CYTot');
      LSt        := AddInt(LSt, FieldByName('c_LSt').AsInteger, 'c_LSt');
      tLStProd   := AddInt(tLStProd, FieldByName('c_LStProd').AsInteger, 'c_SStProd');
      tLStBreak  := AddInt(tLStBreak, FieldByName('c_LStBreak').AsInteger, 'c_LStBreak');
      tLStMat    := AddInt(tLStMat, FieldByName('c_LStMat').AsInteger, 'c_LStMat');
      tLStRev    := AddInt(tLStRev, FieldByName('c_LStRev').AsInteger, 'c_LStRev');
      tLStClean  := AddInt(tLStClean, FieldByName('c_LStClean').AsInteger, 'c_LStClean');
      tLStDef    := AddInt(tLStDef, FieldByName('c_LStDef').AsInteger, 'c_LStDef');
      Red        := AddInt(Red, FieldByName('c_Red').AsInteger, 'c_Red');
      tRed       := AddInt(tRed, FieldByName('c_tRed').AsInteger, 'c_tRed');
      tYell      := AddInt(tYell, FieldByName('c_tYell').AsInteger, 'c_tYell');
      ManSt      := AddInt(ManSt, FieldByName('c_ManSt').AsInteger, 'c_ManSt');
      tManSt     := AddInt(tManSt, FieldByName('c_tManSt').AsInteger, 'c_tManSt');
      tRun       := AddInt(tRun, FieldByName('c_rtSpd').AsInteger, 'c_rtSpd');
      tOp        := AddInt(tOp, FieldByName('c_otSpd').AsInteger, 'c_otSpd');
      tWa        := AddInt(tWa, FieldByName('c_wtSpd').AsInteger, 'c_wtSpd');
      totProdGrp := AddInt(totProdGrp, FieldByName('c_otProdGrp').AsInteger, 'c_otProdGrp');
      twtProdGrp := AddInt(twtProdGrp, FieldByName('c_wtProdGrp').AsInteger, 'c_wtProdGrp');
      CS         := AddInt(CS, FieldByName('c_CS').AsInteger, 'c_CS');
      CL         := AddInt(CL, FieldByName('c_CL').AsInteger, 'c_CL');
      CT         := AddInt(CT, FieldByName('c_CT').AsInteger, 'c_CT');
      CN         := AddInt(CN, FieldByName('c_CN').AsInteger, 'c_CN');
      CSp        := AddInt(CSp, FieldByName('c_CSp').AsInteger, 'c_CSp');
//TODO wss: Daten pr�fen und abgleichen mit DB-Feldern -> Sp+, Zenit
      CClS        := AddInt(CClS, FieldByName('c_CClS').AsInteger, 'c_CClS');
      COffCnt     := AddInt(COffCnt, FieldByName('c_COffCnt').AsInteger, 'c_COffCnt');
      LckOffCnt   := AddInt(LckOffCnt, FieldByName('c_LckOffCnt').AsInteger, 'c_LckOffCnt');
      LckClS      := AddInt(LckClS, FieldByName('c_LckClS').AsInteger, 'c_LckClS');
      CBu        := AddInt(CBu, FieldByName('c_CBu').AsInteger, 'c_CBu');
      cDBu       := AddInt(cDBu, FieldByName('c_CDBu').AsInteger, 'cDBu');
      UClS       := AddFloat(UClS, FieldByName('c_UClS').AsInteger, 99999999999, 'c_UClS');
      UClL       := AddFloat(UClL, FieldByName('c_UClL').AsInteger, 99999999999, 'c_UClL');
      UClT       := AddFloat(UClT, FieldByName('c_UClT').AsInteger, 99999999999, 'c_UClT');
      CSIRO      := AddInt(CSIRO, FieldByName('c_CSIRO').AsInteger, 'c_CSIRO');
      LckSIRO    := AddInt(LckSiro, FieldByName('c_LckSIRO').AsInteger, 'c_LckSIRO');
      CSIROCl    := AddInt(CSIROCL, FieldByName('c_CSIROCL').AsInteger, 'c_CSIROCL');
      LckSIROCl  := AddInt(LckSIROCl, FieldByName('c_LckSIROCl').AsInteger, 'c_LckSIROCl');
      CSFI       := AddInt(CSfi, FieldByName('c_CSfi').AsInteger, 'c_CSfi');
      LckSFI     := AddInt(LckSfi, FieldByName('c_LckSfi').AsInteger, 'c_LckSfi');
      SFI        := AddInt(SFI, FieldByName('c_SFI').AsInteger, 'c_SFI');
      SFICnt     := AddInt(SFICnt, FieldByName('c_SFICnt').AsInteger, 'c_SFICnt');
      // Wss: add all AdjustBase together. Average calculated after this is done: AdjustBase / Count
      // SpindleData: AdjustBaseCnt = 1
      // ShiftData:   AdjustBaseCnt = number of saved intervals
      xAdjustBaseCnt := FieldByName('c_AdjustBaseCnt').AsInteger;
      AdjustBase     := AdjustBase + (FieldByName('c_AdjustBase').AsFloat * xAdjustBaseCnt);
      AdjustBaseCnt  := AdjustBaseCnt + xAdjustBaseCnt;

      Imp.Neps       := AddInt(Imp.Neps, FieldByName('c_INeps').AsInteger, 'c_INeps');
      Imp.Thick      := AddInt(Imp.Thick, FieldByName('c_IThick').AsInteger, 'c_IThin');
      Imp.Thin       := AddInt(Imp.Thin, FieldByName('c_IThin').AsInteger, 'c_IThin');
      Imp.Small      := AddInt(Imp.Small, FieldByName('c_ISmall').AsInteger, 'c_ISmall');
      Imp.I2_4       := AddInt(Imp.I2_4, FieldByName('c_I2_4').AsInteger, 'c_I2_4');
      Imp.I4_8       := AddInt(Imp.I4_8, FieldByName('c_I4_8').AsInteger, 'c_I4_8');
      Imp.I8_20      := AddInt(Imp.I8_20, FieldByName('c_I8_20').AsInteger, 'c_I8_20');
      Imp.I20_70     := AddInt(Imp.I20_70, FieldByName('c_I20_70').AsInteger, 'c_I20_70');
      // Mai 2005
      CP                := AddInt(CP, FieldByName('c_CP').AsInteger, 'c_CP');
      CClL              := AddInt(CClL, FieldByName('c_CClL').AsInteger, 'c_CClL');
      CClT              := AddInt(CClT, FieldByName('c_CClT').AsInteger, 'c_CClT');
      CShortOffCntPlus  := AddInt(CShortOffCntPlus, FieldByName('c_CShortOffCntPlus').AsInteger, 'c_CShortOffCntPlus');
      LckP              := AddInt(LckP, FieldByName('c_LckP').AsInteger, 'c_LckP');
      LckClL            := AddInt(LckClL, FieldByName('c_LckClL').AsInteger, 'c_LckClL');
      LckClT            := AddInt(LckClT, FieldByName('c_LckClT').AsInteger, 'c_LckClT');
      LckShortOffCnt    := AddInt(LckShortOffCnt, FieldByName('c_LckShortOffCnt').AsInteger, 'c_LckShortOffCnt');
      LckNSLT           := AddInt(LckNSLT, FieldByName('c_LckNSLT').AsInteger, 'c_LckNSLT');
      COffCntPlus       := AddInt(COffCntPlus, FieldByName('c_COffCntPlus').AsInteger, 'c_COffCntPlus');
      COffCntMinus      := AddInt(COffCntMinus, FieldByName('c_COffCntMinus').AsInteger, 'c_COffCntMinus');
      CShortOffCnt      := AddInt(CShortOffCnt, FieldByName('c_CShortOffCnt').AsInteger, 'c_CShortOffCnt');
      CShortOffCntMinus := AddInt(CShortOffCntMinus, FieldByName('c_CShortOffCntMinus').AsInteger, 'c_CShortOffCntMinus');
      CDrumWrap         := AddInt(CDrumWrap, FieldByName('c_CDrumWrap').AsInteger, 'c_CDrumWrap');
      // Februar 2008 VCV, Nue:12.02.08
      CVCV       := AddInt(CVCV, FieldByName('c_CVCV').AsInteger, 'c_CVCV');
      LckVCV     := AddInt(LckVCV, FieldByName('c_LckVCV').AsInteger, 'c_LckVCV');
    except
      on e: Exception do begin
        Result := False;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddData failed.' + e.message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddClassCutData(aClassCut: PMMClassCutField; aQuery: TNativeAdoQuery): boolean;
var
  i: integer;
begin
  Result := True;
  try
    for i:=1 to cClassCutFields do
      aClassCut^[i] := AddInt(aClassCut^[i],
        aQuery.FieldByName('c_cC' + IntToStr(i)).AsInteger, 'ClassCutField: ' + intToStr(i));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddClassCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddClassUnCutData(aClassUnCut: PMMClassUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cClassUncutFields do
      aClassUncut^[x] := AddInt(aClassUnCut^[x],
        aQuery.FieldByName('c_cU' + IntToStr(x)).AsInteger,
        'ClassUncutField : ' + intToStr(x));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddClassUnCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddSpliceCutData(aSpliceCut: PMMSpCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSpCutFields do
      aSpliceCut^[x] := AddSmallInt(CheckLongWordToInt(aSpliceCut^[x],'SpCutField'),
        aQuery.FieldByName('c_spC' + IntToStr(x)).AsInteger,
        'SpCutField : ' + intToStr(x));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddSpliceCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddSpliceUnCutData(aSpliceUnCut: PMMSpUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSpUncutFields do
      aSpliceUncut^[x] := AddInt(aSpliceUncut^[x],
        aQuery.FieldByName('c_spU' + IntToStr(x)).AsInteger,
        'SpUncutField : ' + intToStr(x));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddSpliceUnCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddSiroCutData(aSiroCut: PMMSiroCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSIROCutFields do
      aSiroCut^[x] := AddInt(aSiroCut^[x],
        aQuery.FieldByName('c_siC' + IntToStr(x)).AsInteger,
        'SIROCutField : ' + intToStr(x));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddSiroCutData failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.AddSiroUnCutData(aSiroUnCut: PMMSiroUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSIROUnCutFields do
      aSiroUnCut^[x] := AddInt(aSiroUnCut[x],
        aQuery.FieldByName('c_siU' + IntToStr(x)).AsInteger,
        'SIROUnCutField : ' + intToStr(x));
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'AddSiroUnCutData failed. ' + e.message);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecByClassCutData(aClassCut: PMMClassCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cClassCutFields do
      aClassCut^[x] := aQuery.FieldByName('c_cC' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecByClassCutData failed. failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecByClassUnCutData(aClassUnCut: PMMClassUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cClassUncutFields do
      aClassUncut^[x] := aQuery.FieldByName('c_cU' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecByClassUnCutData failed. failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecBySpliceCutData(aSpliceCut: PMMSpCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSpCutFields do
      aSpliceCut^[x] := aQuery.FieldByName('c_spC' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecBySpliceCutData failed. failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecBySpliceUnCutData(aSpliceUnCut: PMMSpUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSpUncutFields do
      aSpliceUncut^[x] := aQuery.FieldByName('c_spU' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecBySpliceUnCutData failed. failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecBySiroCutData(aSiroCut: PMMSiroCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSIROCutFields do
      aSiroCut^[x] := aQuery.FieldByName('c_siC' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecBySiroCutData failed. failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDBDataAssistant.InitRecBySiroUnCutData(aSiroUnCut: PMMSiroUnCutField; aQuery: TNativeAdoQuery): boolean;
var x: integer;
begin
  Result := True;
  try
    for x := 1 to cSIROUnCutFields do
      aSiroUnCut^[x] := aQuery.FieldByName('c_siU' + IntToStr(x)).AsInteger;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'InitRecBySiroUnCutData failed. failed. ' + e.message);
    end;
  end;
end;
//---implementation of TMaOfflimitAssistant-------------------------------------
constructor TMachineOfflimitAssistant.Create(aQuery: TNativeAdoQuery);
begin
  inherited Create;
  mQuery          := TNativeAdoQuery.Create;
  mSettingsLoaded := False;
  mBordersLoaded  := False;
  mAveragesLoaded := False;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.InitProdGrp(aProdID: integer; aMachID: integer; aActIntervalID: integer);
begin
  if fProdGrpID <> aProdID then begin
    mSettingsLoaded := False;
    mBordersLoaded  := False;
    mAveragesLoaded := False;
  end;
  fProdGrpID     := aProdID;
  fMachID        := aMachID;
  mActIntervalID := aActIntervalID;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.Init(aQuery: TNativeAdoQuery);
begin
  mQuery.Assign(aQuery);
end;
//------------------------------------------------------------------------------
destructor TMachineOfflimitAssistant.Destroy;
begin
  mQuery.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.SpindleInOfflimit(aSpindleOfflimitRec: PSpindleOfflimitDataRec): boolean;
var
  xProduced: boolean;
begin
  if not mBordersLoaded then
    LoadBorders;

  CalcToBase(aSpindleOfflimitRec, xProduced);
  if xProduced then begin
    Result := CheckSpindle(aSpindleOfflimitRec);
  end else begin
    // Wenn nichts produziert wurde...
    Result := True;
    // ...Daten l�schen...
    FillChar(aSpindleOfflimitRec^, Sizeof(TSpindleOfflimitDataRec), 0);
    // ...und die Offlimitursache auf die L�nge schieben! Diese wird dann verwendet,
    // um diese als OutOfProduction zu deklarieren!!
    aSpindleOfflimitRec^.Len_On := True;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.ReadOfflimitValuesFromDB(aSpindleID: integer; aOfflimitRec: PSpindleOfflimitRec);
begin
  // Hier werden bestehende Offlimitdaten von dieser Spindel gelesen. Die Daten
  // werden dann in der Methode UpdateOfflimitValuesInRecord() auf den neusten Stand gebracht,
  // bevor sie dann wieder auf die DB gespeichert werden.
  FillChar(aOfflimitRec^, sizeof(TSpindleOfflimitRec), 0);
  with mQuery, aOfflimitRec^ do
  try
    Close;
    SQL.Text := cGetSpindleOfflimitData;
    Params.ParamByName('c_machine_id').AsInteger := MachID;
    Params.ParamByName('c_spindle_id').AsInteger := aSpindleID;
    Open;
    if not FindFirst then begin
      fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'ReadOfflimitValuesFromDB: Offlimit spindle not found on database. MachID = ' + IntToStr(MachID) + ' SpdID = ' + IntToStr(aSpindleID));
      raise EMMException.Create(fError.Msg);
    end;
    max_online_offlimit_time := FieldByName('c_max_online_offlimit_time').AsInteger;
    offline_offlimit_time    := FieldByName('c_offline_offlimit_time').AsInteger;
    offline_watchtime        := FieldByName('c_offline_watchtime').AsInteger;
    Eff                      := FieldByName('c_Eff').AsInteger;
    Eff_online_time          := FieldByName('c_Eff_online_time').AsInteger;
    Eff_offline_time         := FieldByName('c_Eff_offline_time').AsInteger;

    tLSt                         := FieldByName('c_tLSt').AsInteger;
    tLSt_online_time             := FieldByName('c_tLSt_online_time').AsInteger;
    tLSt_offline_time            := FieldByName('c_tLSt_offline_time').AsInteger;
    LSt                          := FieldByName('c_LSt').AsInteger;
    LSt_online_time              := FieldByName('c_LSt_online_time').AsInteger;
    LSt_offline_time             := FieldByName('c_LSt_offline_time').AsInteger;
    OutOfProduction              := FieldByName('c_OutOfProduction').AsBoolean;
    OutOfProduction_online_time  := FieldByName('c_OutOfProduction_online_time').AsInteger;
    OutOfProduction_offline_time := FieldByName('c_OutOfProduction_offline_time').AsInteger;

    CYTot                        := FieldByName('c_CYTot').AsFloat;
    CYTot_online_time            := FieldByName('c_CYTot_online_time').AsInteger;
    CYTot_offline_time           := FieldByName('c_CYTot_offline_time').AsInteger;
    CSIRO                        := FieldByName('c_CSIRO').AsFloat;
    CSIRO_online_time            := FieldByName('c_CSIRO_online_time').AsInteger;
    CSIRO_offline_time           := FieldByName('c_CSIRO_offline_time').AsInteger;
    YB                           := FieldByName('c_YB').AsFloat;
    YB_online_time               := FieldByName('c_YB_online_time').AsInteger;
    YB_offline_time              := FieldByName('c_YB_offline_time').AsInteger;
    CSp                          := FieldByName('c_CSp').AsFloat;
    CSp_online_time              := FieldByName('c_CSp_online_time').AsInteger;
    CSp_offline_time             := FieldByName('c_CSp_offline_time').AsInteger;
    RSp                          := FieldByName('c_RSp').AsFloat;
    RSp_online_time              := FieldByName('c_RSp_online_time').AsInteger;
    RSp_offline_time             := FieldByName('c_RSp_offline_time').AsInteger;
    CBu                          := FieldByName('c_CBu').AsFloat;
    CBu_online_time              := FieldByName('c_CBu_online_time').AsInteger;
    CBu_offline_time             := FieldByName('c_CBu_offline_time').AsInteger;
    CUpY                         := FieldByName('c_CUpY').AsFloat;
    CUpY_online_time             := FieldByName('c_CUpY_online_time').AsInteger;
    CUpY_offline_time            := FieldByName('c_CUpY_offline_time').AsInteger;

    Close;
  except
    on e: Exception do begin
      fError.Error := ERROR_INVALID_FUNCTION;
      fError.ErrorTyp := etDBError;
      fError.Msg := 'GetSpindleValues failed. ' + e.Message;
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.UpdateOfflimitValuesInRecord(aOfflimitRec: PSpindleOfflimitRec; aSpindleOfflimitDataRec: PSpindleOfflimitDataRec; aIntervalLen: integer);
var
  xMaxOnlineTime: integer;
  //..................................................
  procedure SetMaxTime(aPar: integer);
  begin
    if aPar > xMaxOnlineTime then
      xMaxOnlineTime := aPar;
  end;
  //..................................................
  procedure AddIntLen(var aPar: integer);
  begin
    aPar := aPar + aIntervalLen;
  end;
  //..................................................
begin
  // Hier werden die total Online/Offline Zeitwerte inkrementiert und bei den
  // einzelnen Werten individuell ebenfalls. Ist ein Parameter die Ursache f�r
  // den Offlimitstatus, werden die on/offline Zeit um die L�nge des Intervalls
  // vergr�ssert.
  // Ist ein Parameter nicht im Offlimit, dann wird die Onlinezeit gel�scht (=0).
  // In xMaxOnlineTime wird die l�ngste OfflimitOnlineTime ermittelt, welche dann
  // im Feld t_machine_offlimit.c_max_online_offlimit_time gespeichert wird.
  xMaxOnlineTime := 0;
  with aOfflimitRec^ do begin
    offline_watchtime     := offline_watchtime + aIntervalLen;
    offline_offlimit_time := offline_offlimit_time + aIntervalLen;

    OutOfProduction := aSpindleOfflimitDataRec.Len_On;
    if aSpindleOfflimitDataRec.Len_On then begin
      AddIntLen(OutOfProduction_online_time);
      AddIntLen(OutOfProduction_offline_time);
    end else
      OutOfProduction_online_time := 0;
    SetMaxTime(OutOfProduction_online_time);

    Eff := aSpindleOfflimitDataRec.Eff;
    if aSpindleOfflimitDataRec.Eff_On then begin
      AddIntLen(Eff_online_time);
      AddIntLen(Eff_offline_time);
    end else
      Eff_online_time := 0;
    SetMaxTime(Eff_online_time);

    tLSt := aSpindleOfflimitDataRec.tLSt;
    if aSpindleOfflimitDataRec.tLSt_On then begin
      AddIntLen(tLSt_online_time);
      AddIntLen(tLSt_offline_time);
    end else
      tLSt_online_time := 0;
    SetMaxTime(tLSt_online_time);

    LSt := aSpindleOfflimitDataRec.LSt;
    if aSpindleOfflimitDataRec.LSt_On then begin
      AddIntLen(LSt_online_time);
      AddIntLen(LSt_offline_time);
    end else
      LSt_online_time := 0;
    SetMaxTime(LSt_online_time);

    CYTot := aSpindleOfflimitDataRec.CYTot;
    if aSpindleOfflimitDataRec.CYTot_On then begin
      AddIntLen(CYTot_online_time);
      AddIntLen(CYTot_offline_time);
    end else
      CYTot_online_time := 0;
    SetMaxTime(CYTot_online_time);

    CSIRO := aSpindleOfflimitDataRec.CSIRO;
    if aSpindleOfflimitDataRec.CSIRO_On then begin
      AddIntLen(CSIRO_online_time);
      AddIntLen(CSIRO_offline_time);
    end else
      CSIRO_online_time := 0;
    SetMaxTime(CSIRO_online_time);

    CSp := aSpindleOfflimitDataRec.CSp;
    if aSpindleOfflimitDataRec.CSp_On then begin
      AddIntLen(CSp_online_time);
      AddIntLen(CSp_offline_time);
    end else
      CSp_online_time := 0;
    SetMaxTime(CSp_online_time);

    RSp := aSpindleOfflimitDataRec.RSp;
    if aSpindleOfflimitDataRec.RSp_On then begin
      AddIntLen(RSp_online_time);
      AddIntLen(RSp_offline_time);
    end else
      RSp_online_time := 0;
    SetMaxTime(RSp_online_time);

    CBu := aSpindleOfflimitDataRec.CBu;
    if aSpindleOfflimitDataRec.CBu_On then begin
      AddIntLen(CBu_online_time);
      AddIntLen(CBu_offline_time);
    end else
      CBu_online_time := 0;
    SetMaxTime(CBu_online_time);

    CUpY := aSpindleOfflimitDataRec.CUpY;
    if aSpindleOfflimitDataRec.CUpY_On then begin
      AddIntLen(CUpY_online_time);
      AddIntLen(CUpY_offline_time);
    end else
      CUpY_online_time := 0;
    SetMaxTime(CUpY_online_time);

    YB := aSpindleOfflimitDataRec.YB;
    if aSpindleOfflimitDataRec.YB_On then begin
      AddIntLen(YB_online_time);
      AddIntLen(YB_offline_time);
    end else
      YB_online_time := 0;
    SetMaxTime(YB_online_time);

    max_online_offlimit_time := xMaxOnlineTime;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.SaveOfflimitValuesToDB(aSpindleID, aAverageID: integer; aOfflimitRec: PSpindleOfflimitRec);
begin
  with mQuery, aOfflimitRec^ do
  try
    Close;
    // Diese Spindel erst l�schen
    SQL.Text := cDeleteMachineOfflimitSpindle;
    Params.ParamByName('c_machine_id').AsInteger := MachID;
    Params.ParamByName('c_Spindle_id').AsInteger := aSpindleID;
    ExecSQL;
    Close;
    // und wieder mit neuen Werten einf�gen
    SQL.Text := cInsertMachineOfflimitSpindle;
    Params.ParamByName('c_machine_id').AsInteger                   := MachID;
    Params.ParamByName('c_Spindle_id').AsInteger                   := aSpindleID;
    Params.ParamByName('c_maoffaverage_id').AsInteger              := aAverageID;
    Params.ParamByName('c_max_online_offlimit_time').AsInteger     := max_online_offlimit_time;
    Params.ParamByName('c_offline_watchtime').AsInteger            := offline_watchtime;
    Params.ParamByName('c_offline_offlimit_time').AsInteger        := offline_offlimit_time;
    Params.ParamByName('c_YB').AsFloat                             := YB;
    Params.ParamByName('c_YB_online_time').AsInteger               := YB_Online_time;
    Params.ParamByName('c_YB_offline_time').AsInteger              := YB_offline_time;
    Params.ParamByName('c_Eff').AsInteger                          := Eff;
    Params.ParamByName('c_Eff_online_time').AsInteger              := Eff_online_time;
    Params.ParamByName('c_Eff_offline_time').AsInteger             := Eff_offline_time;

    Params.ParamByName('c_tLSt').AsInteger                         := tLSt;
    Params.ParamByName('c_tLSt_online_time').AsInteger             := tLSt_online_time;
    Params.ParamByName('c_tLSt_offline_time').AsInteger            := tLSt_offline_time;
    Params.ParamByName('c_LSt').AsInteger                          := LSt;
    Params.ParamByName('c_LSt_online_time').AsInteger              := LSt_online_time;
    Params.ParamByName('c_LSt_offline_time').AsInteger             := LSt_offline_time;
    Params.ParamByName('c_OutOfProduction').AsBoolean              := OutOfProduction;
    Params.ParamByName('c_OutOfProduction_online_time').AsInteger  := OutOfProduction_online_time;
    Params.ParamByName('c_OutOfProduction_offline_time').AsInteger := OutOfProduction_offline_time;

    Params.ParamByName('c_CYTot').AsFloat                          := CYTot;
    Params.ParamByName('c_CYTot_online_time').AsInteger            := CYTot_online_time;
    Params.ParamByName('c_CYTot_offline_time').AsInteger           := CYTot_offline_time;
    Params.ParamByName('c_CSIRO').AsFloat                          := CSIRO;
    Params.ParamByName('c_CSIRO_online_time').AsInteger            := CSIRO_online_time;
    Params.ParamByName('c_CSIRO_offline_time').AsInteger           := CSIRO_offline_time;
    Params.ParamByName('c_CSp').AsFloat                            := CSp;
    Params.ParamByName('c_CSp_online_time').AsInteger              := CSp_online_time;
    Params.ParamByName('c_CSp_offline_time').AsInteger             := CSp_offline_time;
    Params.ParamByName('c_RSp').AsFloat                            := RSp;
    Params.ParamByName('c_RSp_online_time').AsInteger              := RSp_online_time;
    Params.ParamByName('c_RSp_offline_time').AsInteger             := RSp_offline_time;
    Params.ParamByName('c_CBu').AsFloat                            := CBu;
    Params.ParamByName('c_CBu_online_time').AsInteger              := CBu_online_time;
    Params.ParamByName('c_CBu_offline_time').AsInteger             := CBu_offline_time;
    Params.ParamByName('c_CUpY').AsFloat                           := CUpY;
    Params.ParamByName('c_CUpY_online_time').AsInteger             := CUpY_online_time;
    Params.ParamByName('c_CUpY_offline_time').AsInteger            := CUpY_offline_time;

    ExecSQL;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'SaveOfflimitValuesToDB failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.UpdateAsHealthy(aSpindleID, aIntervalLen: integer; aAverageID: integer);
begin
  with mQuery do
  try
    Close;
    SQL.Text := cUpdateUpdateAsHealthy;
    ParamByName('c_machine_id').AsInteger      := MachID;
    ParamByName('c_spindle_id').AsInteger      := aSpindleID;
    ParamByName('interval_len').AsInteger      := aIntervalLen;
    ParamByName('c_maoffaverage_id').AsInteger := aAverageID;
    ExecSQL;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'UpdateAsHealthy failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.UpdateAsHide(aSpindleFirst, aSpindleLast: integer);
begin
  with mQuery do
  try
    Close;
    SQL.Text := cUpdateUpdateAsHide;
    ParamByName('c_machine_id').AsInteger    := MachID;
    ParamByName('c_spindle_first').AsInteger := aSpindleFirst;
    ParamByName('c_spindle_last').AsInteger  := aSpindleLast;
    ExecSQL;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'UpdateAsHide failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.StoreAverages: integer;
begin
  if not mAveragesLoaded then
    LoadAverages;

  with mQuery, mAverages do
  try
    Close;
    SQL.Text := cStoreAverages;
    Params.ParamByName('c_Eff').AsInteger  := Eff;
    Params.ParamByName('c_tLSt').AsInteger := tLSt; // tLSt werden als Minuten behandelt
    Params.ParamByName('c_LSt').AsInteger  := LSt;

    Params.ParamByName('c_CYTot').AsFloat  := CYTot;
    Params.ParamByName('c_CSIRO').AsFloat  := CSIRO;
    Params.ParamByName('c_CSp').AsFloat    := CSp;
    Params.ParamByName('c_RSp').AsFloat    := RSp;
    Params.ParamByName('c_CBu').AsFloat    := CBu;
    Params.ParamByName('c_CUpY').AsFloat   := CUpY;
    Params.ParamByName('c_YB').AsFloat     := YB;

    Result := InsertSQL('t_machine_offlimit_average', 'c_maoffaverage_id', high(integer), 1);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'StoreAverages failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.GetAverageOn: boolean;
begin
  if not mSettingsLoaded then
    LoadSettings;
  Result := mSettings.AverageOn;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.SetAverageOn(Value: boolean);
begin
  mSettings.AverageOn := Value;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.GetAverageStart: TDateTime;
begin
  if not mSettingsLoaded then
    LoadSettings;
  Result := fAverageStart;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.GetAverageEnd: TDateTime;
begin
  if not mSettingsLoaded then
    LoadSettings;
  Result := fAverageEnd;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.LoadSettings;
begin
  FillChar(mSettings, sizeof(TMachineOfflimitSettings), 0);
  with mQuery, mSettings do
  try
    Close;
      // load the offlimitsettings
    SQL.Text := cGetMaOffliSettings;
    Params.ParamByName('c_machine_id').AsInteger := fMachID;
    Open;
    if not FindFirst then begin
      fError := SetError(ERROR_BAD_ENVIRONMENT, etMMError, 'No machine offlimit settings available');
      raise EMMException.Create(fError.Msg);
    end;

    AverageOn := FieldByName('c_OffBorderByAverage').AsBoolean;
    FixOn     := FieldByName('c_OffBorderByFixValues').AsBoolean;
    Base      := TLenBase(FieldByName('c_Base').AsInteger);
    // Falls unter umst�nden irgendwie wie auch immer trotzdem lbAbsolute auf der DB
    // w�re, dann diesen umbiegen auf 100km
    if Base = lbAbsolute then
      Base := lb100km;
    AverageTime := FieldByName('c_Average_time').AsInteger;

    //--------
    // Prozetuale Settings
    //--------
    if FieldByName('c_Eff_PC_max').IsNull then Eff_PC_max := -1
                                          else Eff_PC_max := FieldByName('c_Eff_PC_max').AsInteger;

    if FieldByName('c_Eff_PC_min').IsNull then Eff_PC_min := -1
                                          else Eff_PC_min := FieldByName('c_Eff_PC_min').AsInteger;

    if FieldByName('c_CYTot_PC_min').IsNull then CYTot_PC_min := -1
                                            else CYTot_PC_min := FieldByName('c_CYTot_PC_min').AsInteger;

    if FieldByName('c_CYTot_PC_max').IsNull then CYTot_PC_max := -1
                                            else CYTot_PC_max := FieldByName('c_CYTot_PC_max').AsInteger;

    if FieldByName('c_CSIRO_PC_max').IsNull then CSIRO_PC_max := -1
                                            else CSIRO_PC_max := FieldByName('c_CSIRO_PC_max').AsInteger;

    if FieldByName('c_CSp_PC_max').IsNull then CSp_PC_max := -1
                                          else CSp_PC_max := FieldByName('c_CSp_PC_max').AsInteger;

    if FieldByName('c_RSp_PC_max').IsNull then RSp_PC_max := -1
                                          else RSp_PC_max := FieldByName('c_RSp_PC_max').AsInteger;

    if FieldByName('c_CBu_PC_max').IsNull then CBu_PC_max := -1
                                          else CBu_PC_max := FieldByName('c_CBu_PC_max').AsInteger;

    if FieldByName('c_CUpY_PC_max').IsNull then CUpY_PC_max := -1
                                           else CUpY_PC_max := FieldByName('c_CUpY_PC_max').AsInteger;

    if FieldByName('c_YB_PC_max').IsNull then YB_PC_max := -1
                                         else YB_PC_max := FieldByName('c_YB_PC_max').AsInteger;

    //--------
    // Absolute Settings
    //--------
    if FieldByName('c_Eff_min').IsNull then Eff_min := -1
                                       else Eff_min := FieldByName('c_Eff_min').AsInteger;

    if FieldByName('c_Eff_max').IsNull then Eff_max := -1
                                       else Eff_max := FieldByName('c_Eff_max').AsInteger;

    if FieldByName('c_tLSt_max').IsNull then tLSt_max := -1
                                        else tLSt_max := FieldByName('c_tLSt_max').AsInteger;

    if FieldByName('c_LSt_max').IsNull then LSt_max := -1
                                       else LSt_max := FieldByName('c_LSt_max').AsInteger;

    if FieldByName('c_CYTot_min').IsNull then CYTot_min := -1
                                         else CYTot_min := FieldByName('c_CYTot_min').AsFloat;

    if FieldByName('c_CYTot_max').IsNull then CYTot_max := -1
                                         else CYTot_max := FieldByName('c_CYTot_max').AsFloat;

    if FieldByName('c_CSIRO_max').IsNull then CSIRO_max := -1
                                         else CSIRO_max := FieldByName('c_CSIRO_max').AsFloat;

    if FieldByName('c_CSp_max').IsNull then CSp_max := -1
                                       else CSP_max := FieldByName('c_CSp_max').AsFloat;

    if FieldByName('c_RSp_max').IsNull then RSp_max := -1
                                       else RSp_max := FieldByName('c_RSp_max').AsFloat;

    if FieldByName('c_CBu_max').IsNull then CBu_max := -1
                                       else CBu_max := FieldByName('c_CBu_max').AsFloat;

    if FieldByName('c_CUpY_max').IsNull then CUpY_max := -1
                                        else CUpY_max := FieldByName('c_CUpY_max').AsFloat;

    if FieldByName('c_YB_max').IsNull then YB_max := -1
                                      else YB_max := FieldByName('c_YB_max').AsFloat;

    Close;
    // initialize the range of average
    // get the end of the average time period (is the same as the end of the actual interval)
    SQL.Text := cGetMaOffliAverageEndTime;
    Params.ParamByName('c_interval_id').AsInteger := mActIntervalID;
    Open;
    fAverageEnd := FieldByName('end_time').ASDateTime;
    Close;

      // get the start of the average period (is the end of act interval less the len of average)
   (*  Im Zusammenhang mit Datunsfunktionen werden die Datentypen
       der Parameter explizit angegeben um eine Fehlinterpretation des Parametertyps
       unter ADO zu vermeiden *)
    SQL.Text := cGetMaOffliAverageStartTime;
    Params.ParamByName('average_len').AsInteger := mSettings.AverageTime;
    Params.ParamByName('average_len').AdoType   := adInteger;
    Params.ParamByName('end_time').AsDateTime   := fAverageEnd;
    Params.ParamByName('end_time').AdoType      := adDate;
    Open;
    fAverageStart := FieldByName('start_time').ASDateTime;
    Close;
    mSettingsLoaded := True;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'LoadSettings failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.LoadBorders;
begin
  // Grenwerte mal mit "OutOfRange" initialisieren
  with mBorders do begin
    Eff_min   := -1;
    Eff_max   := MAXINT;
    tLSt_max  := MAXINT;
    LSt_max   := MAXINT;

    CYTot_min := -1;
    CYTot_max := MAXINT;
    CSIRO_max := MAXINT;
    CSp_max   := MAXINT;
    RSp_max   := MAXINT;
    CBu_max   := MAXINT;
    CUpY_max  := MAXINT;
    YB_max    := MAXINT;
  end;
  // Die Durchschnittswerte m�ssen in jedem fall geholt werden, da diese ja auch
  // f�r die absoluten Berechnungen n�tig sind!!
  if not mAveragesLoaded then
    LoadAverages;

  // Zu diesem Zeitpunkt koennte AverageOn overruled sein, da die Zeit noch nicht
  // erreicht wurde. Damit der FixBorder trotzdem greifft, wird beim Check, ob
  // fuer Average genuegend Intervalle vorhanden sind nicht mehr abgebrochen. Es wird
  // lediglich das Property AverageOn wieder auf False gesetzt, damit der FixBorder
  // trotzdem zum Zuge kommt. (11.4.2002, wss)
  if mSettings.AverageOn then begin
    // Die Grenwerte werden nun danach ermitteln
    CheckAvgBorderValues;
  end;
  // Wenn Absolute Grenzsettings eingeschalten...
  if mSettings.FixOn then
    // ...dann die Grenzwerte eventuell weiter danach einschr�nken
    CheckFixBorderValues;
  mBordersLoaded := True;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.LoadAverages;
var
  xotSpd: Integer;
begin
  FillChar(mAverages, sizeof(TSpindleOfflimitDataRec), 0);
  with mQuery, mAverages do
  try
    Close;
    SQL.Text := cLoadAverages;
    Params.ParamByName('c_prod_id').AsInteger := fProdGrpID;
    Params.ParamByName('start').AsDateTime    := fAverageStart;
    Params.ParamByName('start').AdoType       := adDate;
    Params.ParamByName('end').AsDateTime      := fAverageEnd;
    Params.ParamByName('end').AdoType         := adDate;
    Open;
    { TODO : LoadAverages failed }
    // mAveragesLoaded ?
    if FindFirst then begin // ADO Conform
//      Eff := FieldByName('Eff').AsInteger;
      xotSpd := FieldByName('c_otSpd').AsInteger;
      if xotSpd > 0 then
        Eff := Round(FieldByName('c_rtSpd').AsInteger * 100.0 / xotSpd);
      Len := FieldByName('c_Len').AsInteger;
      if Len = 0 then
        Len := 1;
      // DONE: Berechnungen m�ssen auf die Basis der OfflimitSettings gerechnet werden
      // tLSt und LSt d�rfen nicht normiert werden !?!?
      CYTot := CalcValue(mSettings.Base, FieldByName('c_CYTot').AsFloat, Len);
      CSIRO := CalcValue(mSettings.Base, FieldByName('c_CSIRO').AsFloat, Len);
      CSp   := CalcValue(mSettings.Base, FieldByName('c_CSp').AsFloat, Len);
      RSp   := CalcValue(mSettings.Base, FieldByName('c_RSp').AsFloat, Len);
      CBu   := CalcValue(mSettings.Base, FieldByName('c_CBu').AsFloat, Len);
      CUpY  := CalcValue(mSettings.Base, FieldByName('c_CUpY').AsFloat, Len);
      YB    := CalcValue(mSettings.Base, FieldByName('c_YB').AsFloat, Len);
    end;
    Close;
    mAveragesLoaded := True;
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'LoadAverages failed. ' + e.Message);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.CheckAvgBorderValues;
  //..........................................................
  function CalcMax(aAverage: Single; aDifferenze: integer): Single;
  begin
    if mAverages.Len > 0 then
      Result := aAverage + (aAverage * aDifferenze / 100)
    else
      Result := high(integer);
  end;
  //..........................................................
  function CalcMin(aAverage: Single; aDifferenze: integer): Single;
  begin
    if mAverages.Len > 0 then
      Result := aAverage - (aAverage * aDifferenze / 100)
    else
      Result := low(integer);
  end;
  //..........................................................
begin
  try
    // Hier werden die prozentualen Grenzwerte anhand der Durchschnittswerten berechnet.
    // Absolute Grenzwerte werden zu einem sp�teren Zeitpunkt dar�bergelagert!
    if mSettings.Eff_PC_max > -1 then
      mBorders.Eff_max := mAverages.Eff + (mAverages.Eff * mSettings.Eff_PC_max / 100);
    if mSettings.Eff_PC_min > -1 then
      mBorders.Eff_min := mAverages.Eff - (mAverages.Eff * mSettings.Eff_PC_min / 100);
    if mSettings.CYTot_PC_max > -1 then
      mBorders.CYTot_max := CalcMax(mAverages.CYTot, mSettings.CYTot_PC_max);
    if mSettings.CYTot_PC_min > -1 then
      mBorders.CYTot_min := CalcMin(mAverages.CYTot, mSettings.CYTot_PC_min);
    if mSettings.CSIRO_PC_max > -1 then
      mBorders.CSIRO_max := CalcMax(mAverages.CSIRO, mSettings.CSIRO_PC_max);
    if mSettings.CSp_PC_max > -1 then
      mBorders.CSp_max := CalcMax(mAverages.CSp, mSettings.CSp_PC_max);
    if mSettings.RSp_PC_max > -1 then
      mBorders.RSp_max := CalcMax(mAverages.RSp, mSettings.RSp_PC_max);
    if mSettings.YB_PC_max > -1 then
      mBorders.YB_max := CalcMax(mAverages.YB, mSettings.YB_PC_max);
    if mSettings.CBu_PC_max > -1 then
      mBorders.CBu_max := CalcMax(mAverages.CBu, mSettings.CBu_PC_max);
    if mSettings.CUpY_PC_max > -1 then
      mBorders.CUpY_max := CalcMax(mAverages.CUpY, mSettings.CUpY_PC_max);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etNTError, 'CheckAvgBorderValues failed. ' + fError.Msg);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.CheckFixBorderValues;
begin
  try
    // Hier werden die absoluten Grenzwerte �bernommen, wenn diese eine strengere
    // Grenze beschreiben als die prozentuale Grenzwerte.
//TODO wss: k�nnte man auch vereinfachen mit lokaler Procedure mit var Result, Higher, Lower Parametern
    if mSettings.Eff_min > -1 then
      if mSettings.Eff_min > mBorders.Eff_min then
        mBorders.Eff_min := mSettings.Eff_min;

    if mSettings.Eff_max > -1 then
      if mSettings.Eff_max < mBorders.Eff_max then
        mBorders.Eff_max := mSettings.Eff_max;

    // tLSt werden als Minuten behandelt
    if mSettings.tLSt_max > -1 then
      if mSettings.tLSt_max < mBorders.tLSt_max then
        mBorders.tLSt_max := mSettings.tLSt_max;

    if mSettings.LSt_max > -1 then
      if mSettings.LSt_max < mBorders.LSt_max then
        mBorders.LSt_max := mSettings.LSt_max;

    if mSettings.CYTot_min > -1 then
      if mSettings.CYTot_min > mBorders.CYTot_min then
        mBorders.CYTot_min := mSettings.CYTot_min;

    if mSettings.CYTot_max > -1 then
      if mSettings.CYTot_max < mBorders.CYTot_max then
        mBorders.CYTot_max := mSettings.CYTot_max;

    if mSettings.CSIRO_max > -1 then
      if mSettings.CSIRO_max < mBorders.CSIRO_max then
        mBorders.CSIRO_max := mSettings.CSIRO_max;

    if mSettings.CSp_max > -1 then
      if mSettings.CSp_max < mBorders.CSp_max then
        mBorders.CSp_max := mSettings.CSp_max;

    if mSettings.RSp_max > -1 then
      if mSettings.RSp_max < mBorders.RSp_max then
        mBorders.RSp_max := mSettings.RSp_max;

    if mSettings.CBu_max > -1 then
      if mSettings.CBu_max < mBorders.CBu_max then
        mBorders.CBu_max := mSettings.CBu_max;

    if mSettings.CUpY_max > -1 then
      if mSettings.CUpY_max < mBorders.CUpY_max then
        mBorders.CUpY_max := mSettings.CUpY_max;

    if mSettings.YB_Max > -1 then
      if mSettings.YB_max < mBorders.YB_max then
        mBorders.YB_max := mSettings.YB_max;
  except
    on e: Exception do begin
      fError := SetError(ERROR_ARITHMETIC_OVERFLOW, etNTError, 'CheckAvgBorderValues failed. ' + fError.Msg);
      raise;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMachineOfflimitAssistant.CheckSpindle(aSpindleOfflimitRec: PSpindleOfflimitDataRec): boolean;
begin
  Result := False;
  with aSpindleOfflimitRec^ do begin
    if (Eff > mBorders.Eff_max) or (Eff < mBorders.Eff_min) then begin
      Eff_On := True;
      Result := True;
    end;
    // tLSt werden als Minuten behandelt
    if tLSt > mBorders.tLSt_max then begin
      tLSt_On := True;
      Result := True;
    end;
    if LSt > mBorders.LSt_max then begin
      LSt_On := True;
      Result := True;
    end;

    if (CYTot > mBorders.CYTot_max) or (CYTot < mBorders.CYTot_min) then begin
      CYTot_On := True;
      Result := True;
    end;
    if CSIRO > mBorders.CSIRO_max then begin
      CSIRO_On := True;
      Result := True;
    end;
    if CSp > mBorders.CSp_max then begin
      CSp_On := True;
      Result := True;
    end;
    if RSp > mBorders.RSp_max then begin
      RSp_On := True;
      Result := True;
    end;
    if CBu > mBorders.CBu_max then begin
      CBu_On := True;
      Result := True;
    end;
    if CUpY > mBorders.CUpY_max then begin
      CUpY_On := True;
      Result := True;
    end;
    if YB > mBorders.YB_max then begin
      YB_On := True;
      Result := True;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachineOfflimitAssistant.CalcToBase(aSpindleOfflimitRec: PSpindleOfflimitDataRec; var aProduced: boolean);
  //..................................................
  function Calc(aPar: Single): Single;
  begin
    try
      Result := CalcValue(mSettings.Base, aPar, aSpindleOfflimitRec^.Len);
    except
      Result := MAXINT;
    end;
  end;
  //..................................................
begin
  // Jede Spindle muss die Minimall�nge produziert haben. Ansonsten keine �berpr�fung!
  aProduced := (aSpindleOfflimitRec^.Len >= cMinSpuledLen);
  if aProduced then begin
    with aSpindleOfflimitRec^ do begin
      // Die Daten tLSt und LSt werden in den Settings als Absolut pro Interval angebeben
      // und werden nicht Normiert! Daher muss der aktuelle Wert, welcher von dieser
      // Spindel in diesem Interval vorhanden ist in die mAverage umkopiert werden,
      // damit dieser dann mit den Werten aus mBorder verglichen werden kann.
      mAverages.tLSt := tLSt; // tLSt wird als Minuten behandelt -> wird beim auslesen bereits dividiert!
      mAverages.LSt  := LSt;
      // alle anderen Daten werden auf die L�nge normiert
      CYTot := Calc(CYTot);
      CSIRO := Calc(CSIRO);
      CSp   := Calc(CSp);
      RSp   := Calc(RSp);
      CBu   := Calc(CBu);
      CUpY  := Calc(CUpY);
      YB    := Calc(YB);
    end;
  end;
end;
//---implementation of TDataEventAssistant--------------------------------------
constructor TDataEventAssistant.Create(aQuery: TNativeAdoQuery);
begin
  inherited Create;
  mQuery := TNativeAdoQuery.Create;
  mQuery.Assign(aQuery);
end;
//------------------------------------------------------------------------------
destructor TDataEventAssistant.Destroy;
begin
  mQuery.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TDataEventAssistant.GetDataEventByID(aIntervalID, aFragshiftID: integer): TDateTime;
begin
  with mQuery do
  try
    Close;
    SQL.Text := cGetDataEventByID;
    Params.ParamByName('c_interval_id').AsInteger  := aIntervalID;
    Params.ParamByName('c_fragshift_id').AsInteger := aFragshiftID;
    Open;
    if FindFirst and (FieldByName('c_dataevent_start').AsDateTime <> 0) then // ADO Conform
      Result := FieldByName('c_dataevent_start').AsDateTime
    else
      raise EMMException.Create('No DataEvent available. ');
  except
    on e: Exception do
      raise EMMException.Create(Format('TDataEventAssistant.GetDataEventByID failed. IntID = %d, FragID = %d: %s', [aIntervalID, aFragshiftID, e.message]));
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAssistant.GetOldestDataEvent: TDateTime;
begin
  with mQuery do
  try
    Close;
    SQL.Text := cGetOldestDataEvent;
    Open;
    // ADO Conform
    if FindFirst and (FieldByName('c_dataevent_start').AsDateTime <> 0) then //raise EMMException.Create ( Format ( 'no Data Event found. IntervalID=%d, FragshiftID=%d', [aIntervalID, aFragshiftID] ) );
      Result := FieldByName('c_dataevent_start').AsDateTime
    else
      raise EMMException.Create('not DataEvent available.');
  except
    on e: Exception do
      raise EMMException.Create('TDataEventAssistant.GetOldestDataEvent failed : ' + e.message);
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAssistant.GetIntervalIDByDataEvent(aDataEvent: TDateTime): integer;
begin
  with mQuery do
  try
    Close;
    SQL.Text := cGetIntervalIDByDataEvent;
    Params.ParamByName('DataEvent').AsDateTime := aDataEvent;
    Params.ParamByName('DataEvent').AdoType    := adDate;
    Open;
    if not FindFirst then
      raise EMMException.Create('no Interval ID found.');
    Result := FieldByName('c_interval_id').AsInteger;
  except
    on e: Exception do
      raise EMMException.Create('TDataEventAssistant.GetIntervalIDByDataEvent failed : ' + e.message);
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAssistant.GetFragshiftIDByDataEvent(aDataEvent: TDateTime): integer;
begin
  with mQuery do
  try
    Close;
    SQL.Text := cGetFragshiftIDByDataEvent;
    Params.ParamByName('DataEvent').AsDateTime := aDataEvent;
    Params.ParamByName('DataEvent').AdoType    := adDate;
    Open;
    if not FindFirst then
      raise EMMException.Create('no Fragshift ID found.');
    Result := FieldByName('c_fragshift_id').AsInteger;
  except
    on e: Exception do begin
      raise EMMException.Create('TDataEventAssistant.GetFragshiftIDByDataEvent : ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDataEventAssistant.GetDataEventLenByDataEvent(aDataEvent: TDateTime): integer;
begin
  with mQuery do
  try
    Close;
    SQL.Text := cGetDataEventLenByDataEvent;
    Params.ParamByName('DataEvent').AsDateTime  := aDataEvent;
    Params.ParamByName('DataEvent').AdoType     := adDate;
    Params.ParamByName('DataEvent2').AsDateTime := aDataEvent;
    Params.ParamByName('DataEvent2').AdoType    := adDate;
    Open;
    if not FindFirst {or ( FieldByName ( 'Len' ).AsInteger = 0 )} then
      raise EMMException.Create('no DataEvent found.');
    Result := FieldByName('Len').AsInteger;
  except
    on e: Exception do begin
      raise EMMException.Create('TDataEventAssistant.GetDataEventLenByDataEvent failed : ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

