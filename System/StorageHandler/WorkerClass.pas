(*|-------------------------------------------------------------------------------------------
| Filename......: DataPoolWriterClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 22.07.1999  1.00  Mg  | Datei erstellt
| 19.05.2000  1.01  Mg  | TransIsolationLevel new DirtyRead because of deadlocks
| 27.03.2001  1.02  khp | jtGenDWData, jtGenExpDWData, jtGenQOfflimit Parameterliste um Params erweitert
| 12.06.2001  1.03  khp | TWorker.Init : TransIsolation := tiReadCommitted anstatt tiDirtyRead
|                       | zugleich wurde bei Inserts von TGetZESpdDataEditor.StoreSpdData  tablockx holdlock entfernt
| 05.10.2002        LOK | Umbau ADO
|=========================================================================================*)
unit WorkerClass;

interface
uses
  MMBaseClasses, DataPoolClass, mmEventLog, BaseGlobal, LoepfeGlobal, JobEditors,
  sysutils, Windows, ADODbAccess;

type
//------------------------------------------------------------------------------
  TWorker = class(TBaseWorker)
  protected
    function getJobEditor: TBaseJobEditor; override;
  public
    function Init: Boolean; override;
  end;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  SettingsReader;
//------------------------------------------------------------------------------
function TWorker.Init: boolean;
begin
  Result := inherited Init;
   //  mDB.DataBase.TransIsolation := tiDirtyRead;
  mDB.DataBase.TransIsolation := tiReadCommitted;
{ TODO -oLok -cADO Umbau : DataBaseTransIsolation }
end;
//------------------------------------------------------------------------------
function TWorker.getJobEditor: TBaseJobEditor;
begin
  Result := nil;
  case mJob.JobTyp of
//    jtOEEventJobID:         Result := TOEEventJobIDEditor.Create ( mJob, mDB, mEventLog, mDataPool );
//    jtGetZEGrpData:         Result := TGetZEGrpDataEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtDBDump:               Result := TDBDumpEditor.Create(mJob, mDB, mEventLog);
    jtDelInt:               Result := TDelIntEditor.Create(mJob, mDB, mEventLog);
    jtDelShiftByTime:       Result := TDelShiftByTimeEditor.Create(mJob, mDB, mEventLog);
    jtDiagnostic:           Result := TDiagnosticEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtGenDWData:            Result := TGenDWDataEditor.Create(mJob, mDB, mEventLog, Params);
    jtGenExpDWData:         Result := TGenExpDWDataEditor.Create(mJob, mDB, mEventLog, Params);
    jtGenExpShiftData:      Result := TGenExpShiftDataEditor.Create(mJob, mDB, mEventLog);
    jtGenQOfflimit:         Result := TGenQOfflimitEditor.Create(mJob, mDB, mEventLog, Params);
    jtGenShiftData:         Result := TGenShiftDataEditor.Create(mJob, mDB, mEventLog);
    jtGenSpdOfflimits:      Result := TSpdOfflimitsEditor.Create(mJob, mDB, mEventLog);
    jtGetDataStopZESpd:     Result := TGetDataStopZESpdEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtGetExpDataStopZESpd:  Result := TGetExpDataStopZESpdEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtGetExpZESpdData:      Result := TGetExpZESpdDataEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtGetSettingsAllGroups: Result := TGetSettingsAllGroupsEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtGetZESpdData:         Result := TGetZESpdDataEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtProdGrpUpdate:        Result := TProdGrpUpdateEditor.Create(mJob, mDB, mEventLog, Params);
    jtSaveMaConfigToDB:     Result := TSaveMaConfigToDBEditor.Create(mJob, mDB, mEventLog, mDataPool);
    jtSendEventToClients:   Result := TSendMsgToClients.Create(mJob, mDB, mEventLog, Params);
    jtSendMsgToAppl:        Result := TSendMsgToApplEditor.Create(mJob, mDB, mEventLog);
    jtTest:                 Result := TTestEditor.Create(mJob, mDB, mEventLog);
    jtUnDoGetZESpdData:     Result := TUnDoGetZESpdDataEditor.Create(mJob, mDB, mEventLog);
  else
    WriteLog(etWarning, 'Wrong Job Typ received. Job Typ = ' + GetJobName(mJob.JobTyp));
  end;
end;
//------------------------------------------------------------------------------
end.

