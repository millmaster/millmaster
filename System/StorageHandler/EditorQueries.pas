(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: EditorQueries.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 15.10.1998  0.00  Mg  | Datei erstellt
| 03.08.1999  1.00  Mg  | Unitname von StoreXQueries zu EditorQueries
| 14.03.2000  1.01  Mg  | cGetExpDataEventList : Bug Fix order by inserted
| 16.03.2000  1.02  Mg  | Updates and inserts : with ( tablock holdlock ) locked
| 27.04.2000  1.03  Mg  | SFI inserted
| 05.05.2000  1.04  Mg  | ThreadCnt inserted
| 23.05.2000  1.05  Mg  | Machine type from t_machine_type.c_machine_class
| 10.08.2000  1.06  Nue | cLOGDump modified, because of diskspace problems.
| 31.05.2001  1.07  Nue | cQrySelNextShiftAndInterval eingefuegt
| 12.06.2001  1.08  khp | Updates and inserts : with ( tablock holdlock ) entfernt dafuer transaktion mit read commited
| 15.06.2001  1.09  khp | Korrektur in cUnDoGetZESpdData  "start >= (select" statt 2 ">"
| 18.09.2001  1.10  Wss | c_AdjustBase fields added in several queries
| 20.03.2002  1.11  Nue | Query cDelMachOfflimitsAverUnreferenced added.
| 07.11.2002        Wss | - MaschinenOfflimit an neue Konfiguration angepasst
                          - Daten f�r RedLight aus den 3 Offlimit Tabellen entfernt
                          - % Settings f�r tLSt und LSt entfernt
| 18.12.2002  1.12  Nue | Query cLOGDump no further used.
| 17.06.2003  1.12  Wss | Queries angepasst f�r DelIntEditor Job
| 22.11.2007  1.13  Nue | Schlupfkorrekturberechnung: Queries cUpdateSpindleDataLen und cUpdateShiftDataLen angepasst,
|                       | weil verm.ADO einen Fehler macht: update t_x set c_len=c_len*Factor Wenn c_len vom Typ INT ist
|                       | und Factor vom Typ Double, wird nach ADO (Profiler) der Factor auf einen INT-Wert gesetzt???
| 12.02.2008  1.20  Nue | VCV-Fields added.
|=========================================================================================*)
                       
unit EditorQueries;

interface

const
//..............................................................................
  cQrySelNextShiftAndInterval =

  'select MIN(t_shift.c_shift_start) shift_start, MIN(t_interval.c_interval_start) interval_start from t_shift, t_interval ' +
    'where (t_shift.c_shift_start>getdate()) AND (t_interval.c_interval_start>getdate()) ';

//..............................................................................
  cGetIntStartByIntID =
  'select c_interval_start from t_interval ' +
    'where c_interval_id = :c_interval_id';

  cGetOldIntIDsFromIntStart =
  'select c_interval_id from t_interval where c_interval_start <= :c_interval_start';

//..............................................................................
//  cDelIntervalData =
//  'delete from t_spindle_interval_data ' +
//    'where c_interval_id not in ( select c_interval_id from t_interval )';

//..............................................................................
  cDelInterval =
  'delete from t_interval where c_interval_id = :c_interval_id';
//  cDelInterval =
//  'delete from t_interval ' +
//    'where c_interval_start <= :c_interval_start';

//..............................................................................
  cInsertSpindleIntervalData =
    'insert t_spindle_interval_data ( ' +
    'c_prod_id, c_spindle_id, c_machine_id, c_interval_id, c_fragshift_id,' +
    'c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CClS, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
    'c_LckClS, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, c_I20_70, ' +
    'c_CP, c_CClL, c_CClT, c_CShortOffCntPlus, c_LckP, c_LckClL, c_LckClT, c_LckShortOffCnt, ' +
    'c_LckNSLT, c_COffCntPlus, c_COffCntMinus, c_CShortOffCnt, c_CShortOffCntMinus, c_CDrumWrap, ' +
    'c_CVCV, c_LckVCV, ' +         //Nue:12.02.08
    'c_class_image ) ' +
    ' values ( ' +
    ':c_prod_id, :c_spindle_id, :c_machine_id, :c_interval_id,:c_fragshift_id,' +
    ':c_Len, :c_Wei, :c_Bob, :c_Cones, :c_red, :c_tRed, :c_tYell, :c_ManSt, :c_tManSt, ' +
    ':c_rtSpd, :c_otSpd, :c_wtSpd, :c_Sp, :c_RSp, :c_YB, :c_otProdGrp, :c_wtProdGrp, ' +
    ':c_LSt, :c_LStProd, :c_LStBreak, :c_LStMat, :c_LStRev, :c_LStClean, :c_LStDef, ' +
    ':c_CS, :c_CL, :c_CT, :c_CN, :c_CSp, :c_CClS, :c_UClS, :c_UClL, :c_UClT, :c_COffCnt, ' +
    ':c_LckOffCnt, :c_CBu, :c_CDBu, :c_CSys, :c_LckSys, :c_CUpY, :c_CYTot, :c_CSIRO, ' +
    ':c_LckClS, :c_CSiroCl, :c_LckSiroCl, :c_CSFI, :c_LckSFI, :c_SFI, :c_SFICnt, :c_AdjustBase, :c_AdjustBaseCnt, ' +
    ':c_LckSIRO, :c_INeps, :c_IThick, :c_IThin, :c_ISmall, :c_I2_4, :c_I4_8, :c_I8_20, :c_I20_70, ' +
    ':c_CP, :c_CClL, :c_CClT, :c_CShortOffCntPlus, :c_LckP, :c_LckClL, :c_LckClT, :c_LckShortOffCnt, ' +
    ':c_LckNSLT, :c_COffCntPlus, :c_COffCntMinus, :c_CShortOffCnt, :c_CShortOffCntMinus, :c_CDrumWrap, ' +
    ':c_CVCV, :c_LckVCV, '+         //Nue:12.02.08
    ':c_class_image )';

//  cInsertSpindleIntervalData =
//    'insert t_spindle_interval_data ( ' +
//    'c_prod_id, c_spindle_id, c_machine_id, c_interval_id, c_fragshift_id,' +
//    'c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
//    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
//    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
//    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CCl, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
//    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
//    'c_LckCl, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
//    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, ' +
//    'c_I20_70, c_class_image ) ' +
//    ' values ( ' +
//    ':c_prod_id, :c_spindle_id, :c_machine_id, :c_interval_id,:c_fragshift_id,' +
//    ':c_Len, :c_Wei, :c_Bob, :c_Cones, :c_red, :c_tRed, :c_tYell, :c_ManSt, :c_tManSt, ' +
//    ':c_rtSpd, :c_otSpd, :c_wtSpd, :c_Sp, :c_RSp, :c_YB, :c_otProdGrp, :c_wtProdGrp, ' +
//    ':c_LSt, :c_LStProd, :c_LStBreak, :c_LStMat, :c_LStRev, :c_LStClean, :c_LStDef, ' +
//    ':c_CS, :c_CL, :c_CT, :c_CN, :c_CSp, :c_CCl, :c_UClS, :c_UClL, :c_UClT, :c_COffCnt, ' +
//    ':c_LckOffCnt, :c_CBu, :c_CDBu, :c_CSys, :c_LckSys, :c_CUpY, :c_CYTot, :c_CSIRO, ' +
//    ':c_LckCl, :c_CSiroCl, :c_LckSiroCl, :c_CSFI, :c_LckSFI, :c_SFI, :c_SFICnt, :c_AdjustBase, :c_AdjustBaseCnt, ' +
//    ':c_LckSIRO, :c_INeps, :c_IThick, :c_IThin, :c_ISmall, :c_I2_4, :c_I4_8, :c_I8_20, ' +
//    ':c_I20_70, :c_class_image )';
//
//..............................................................................
  cGetGrpDataFromSpindleData =
    'select c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CClS, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
    'c_LckClS, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, c_I20_70, ' +
    'c_CP, c_CClL, c_CClT, c_CShortOffCntPlus, c_LckP, c_LckClL, c_LckClT, c_LckShortOffCnt, ' +
    'c_LckNSLT, c_COffCntPlus, c_COffCntMinus, c_CShortOffCnt, c_CShortOffCntMinus, c_CDrumWrap, ' +
    'c_CVCV, c_LckVCV, ' +         //Nue:12.02.08
    'c_class_image ' +
    'from t_spindle_interval_data ' +
    'where c_prod_id=:c_prod_id and ' +
    'c_fragshift_id=:c_fragshift_id and ' +
    'c_interval_id=:c_interval_id';

//  cGetGrpDataFromSpindleData =
//    'select c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
//    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
//    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
//    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CCl, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
//    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
//    'c_LckCl, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
//    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, ' +
//    'c_I20_70, c_class_image ' +
//    'from t_spindle_interval_data ' +
//    'where c_prod_id=:c_prod_id and ' +
//    'c_fragshift_id=:c_fragshift_id and ' +
//    'c_interval_id=:c_interval_id';
//
//..............................................................................
  cInsertProdGrpData =
    'insert t_dw_prodgroup_shift ( ' +
    'c_prod_id,c_fragshift_id,c_fragshift_start,c_classCut_id,c_classUncut_id, ' +
    'c_splicecut_id,c_spliceuncut_id,c_sirocut_id,c_sirouncut_id, ' +
    'c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CClS, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
    'c_LckClS, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, c_I20_70, ' +
    'c_CP, c_CClL, c_CClT, c_CShortOffCntPlus, c_LckP, c_LckClL, c_LckClT, c_LckShortOffCnt, ' +
    'c_LckNSLT, c_COffCntPlus, c_COffCntMinus, c_CShortOffCnt, c_CShortOffCntMinus, c_CDrumWrap, '+
    'c_CVCV, c_LckVCV ) '+         //Nue:12.02.08

    'values ( ' +
    ':c_prod_id,:c_fragshift_id,:c_fragshift_start,:c_classCut_id,:c_classUncut_id, ' +
    ':c_splicecut_id,:c_spliceuncut_id,:c_sirocut_id, :c_sirouncut_id, ' +
    ':c_Len, :c_Wei, :c_Bob, :c_Cones, :c_red, :c_tRed, :c_tYell, :c_ManSt, :c_tManSt, ' +
    ':c_rtSpd, :c_otSpd, :c_wtSpd, :c_Sp, :c_RSp, :c_YB, :c_otProdGrp, :c_wtProdGrp, ' +
    ':c_LSt, :c_LStProd, :c_LStBreak, :c_LStMat, :c_LStRev, :c_LStClean, :c_LStDef, ' +
    ':c_CS, :c_CL, :c_CT, :c_CN, :c_CSp, :c_CClS, :c_UClS, :c_UClL, :c_UClT, :c_COffCnt, ' +
    ':c_LckOffCnt, :c_CBu, :c_CDBu, :c_CSys, :c_LckSys, :c_CUpY, :c_CYTot, :c_CSIRO, ' +
    ':c_LckClS, :c_CSiroCl, :c_LckSiroCl, :c_CSFI, :c_LckSFI, :c_SFI, :c_SFICnt, :c_AdjustBase, :c_AdjustBaseCnt, ' +
    ':c_LckSIRO, :c_INeps, :c_IThick, :c_IThin, :c_ISmall, :c_I2_4, :c_I4_8, :c_I8_20, :c_I20_70, ' +
    ':c_CP, :c_CClL, :c_CClT, :c_CShortOffCntPlus, :c_LckP, :c_LckClL, :c_LckClT, :c_LckShortOffCnt, ' +
    ':c_LckNSLT, :c_COffCntPlus, :c_COffCntMinus, :c_CShortOffCnt, :c_CShortOffCntMinus, :c_CDrumWrap, '+
    ':c_CVCV, :c_LckVCV )';         //Nue:12.02.08

//  cInsertProdGrpData =
//    'insert t_dw_prodgroup_shift ( ' +
//    'c_prod_id,c_fragshift_id,c_fragshift_start,c_classCut_id,c_classUncut_id, ' +
//    'c_splicecut_id,c_spliceuncut_id,c_sirocutuncut_id, ' +
//    'c_Len, c_Wei, c_Bob, c_Cones, c_red, c_tRed, c_tYell, c_ManSt, c_tManSt, ' +
//    'c_rtSpd, c_otSpd, c_wtSpd, c_Sp, c_RSp, c_YB, c_otProdGrp, c_wtProdGrp, ' +
//    'c_LSt, c_LStProd, c_LStBreak, c_LStMat, c_LStRev, c_LStClean, c_LStDef, ' +
//    'c_CS, c_CL, c_CT,c_CN, c_CSp, c_CCl, c_UClS, c_UClL, c_UClT, c_COffCnt, ' +
//    'c_LckOffCnt, c_CBu, c_CDBu, c_CSys, c_LckSys, c_CUpY, c_CYTot, c_CSIRO, ' +
//    'c_LckCl, c_CSiroCl, c_LckSiroCl, c_CSFI, c_LckSFI, c_SFI, c_SFICnt, c_AdjustBase, c_AdjustBaseCnt, ' +
//    'c_LckSIRO, c_INeps, c_IThick, c_IThin, c_ISmall, c_I2_4, c_I4_8, c_I8_20, ' +
//    'c_I20_70 ) ' +
//    'values ( ' +
//    ':c_prod_id,:c_fragshift_id,:c_fragshift_start,:c_classCut_id,:c_classUncut_id, ' +
//    ':c_splicecut_id,:c_spliceuncut_id,:c_sirocutuncut_id, ' +
//    ':c_Len, :c_Wei, :c_Bob, :c_Cones, :c_red, :c_tRed, :c_tYell, :c_ManSt, :c_tManSt, ' +
//    ':c_rtSpd, :c_otSpd, :c_wtSpd, :c_Sp, :c_RSp, :c_YB, :c_otProdGrp, :c_wtProdGrp, ' +
//    ':c_LSt, :c_LStProd, :c_LStBreak, :c_LStMat, :c_LStRev, :c_LStClean, :c_LStDef, ' +
//    ':c_CS, :c_CL, :c_CT, :c_CN, :c_CSp, :c_CCl, :c_UClS, :c_UClL, :c_UClT, :c_COffCnt, ' +
//    ':c_LckOffCnt, :c_CBu, :c_CDBu, :c_CSys, :c_LckSys, :c_CUpY, :c_CYTot, :c_CSIRO, ' +
//    ':c_LckCl, :c_CSiroCl, :c_LckSiroCl, :c_CSFI, :c_LckSFI, :c_SFI, :c_SFICnt, :c_AdjustBase, :c_AdjustBaseCnt, ' +
//    ':c_LckSIRO, :c_INeps, :c_IThick, :c_IThin, :c_ISmall, :c_I2_4, :c_I4_8, :c_I8_20, ' +
//    ':c_I20_70 )';
//
//..............................................................................
  cUpdateProdGrpData =
    'update t_dw_prodgroup_shift set ' +
    'c_Len = :c_Len, c_Wei = :c_Wei, c_Bob = :c_Bob, c_Cones = :c_Cones, c_red = :c_red, ' +
    'c_tRed = :c_tRed, c_tYell = :c_tYell, c_ManSt = :c_ManSt, c_tManSt = :c_tManSt, ' +
    'c_rtSpd = :c_rtSpd, c_otSpd = :c_otSpd, c_wtSpd = :c_wtSpd, c_Sp = :c_Sp, c_RSp = :c_RSp, ' +
    'c_YB = :c_YB, c_otProdGrp = :c_otProdGrp, c_wtProdGrp = :c_wtProdGrp , ' +
    'c_LSt = :c_LSt, c_LStProd = :c_LStProd, c_LStBreak = :c_LStBreak, c_LStMat = :c_LStMat, ' +
    'c_LStRev = :c_LStRev, c_LStClean = :c_LStClean, c_LStDef = :c_LStDef, ' +
    'c_CS = :c_CS, c_CL = :c_CL, c_CT = :c_CT, c_CN = :c_CN, c_CSp = :c_CSp, c_CClS = :c_CClS, ' +
    'c_UClS = :c_UClS, c_UClL = :c_UClL, c_UClT = :c_UClT, c_COffCnt = :c_COffCnt, ' +
    'c_LckOffCnt = :c_LckOffCnt, c_CBu = :c_CBu, c_CDBu = :c_CDBu, c_CSys = :c_CSys, ' +
    'c_LckSys = :c_LckSys, c_CUpY = :c_CUpY, c_CYTot = :c_CYTot, c_CSIRO = :c_CSIRO, ' +
    'c_LckClS = :c_LckClS, c_CSiroCl = :c_CSiroCl, c_LckSiroCl = :c_LckSiroCl, ' +
    'c_CSFI = :c_CSFI, c_LckSFI = :c_LckSFI, c_SFI = :c_SFI, c_SFICnt = :c_SFICnt, c_AdjustBase = :c_AdjustBase, c_AdjustBaseCnt = :c_AdjustBaseCnt, ' +
    'c_LckSIRO = :c_LckSIRO, c_INeps = :c_INeps, c_IThick = :c_IThick, c_IThin = :c_IThin, ' +
    'c_ISmall = :c_ISmall, c_I2_4 = :c_I2_4, c_I4_8 = :c_I4_8, c_I8_20 = :c_I8_20, c_I20_70 = :c_I20_70, ' +
    'c_CP = :c_CP, c_CClL = :c_CClL, c_CClT = :c_CClT, c_CShortOffCntPlus = :c_CShortOffCntPlus, ' +
    'c_LckP = :c_LckP, c_LckClL = :c_LckClL, c_LckClT = :c_LckClT, c_LckShortOffCnt = :c_LckShortOffCnt, ' +
    'c_LckNSLT = :c_LckNSLT, c_COffCntMinus = :c_COffCntMinus, c_COffCntPlus = :c_COffCntPlus, c_CShortOffCnt = :c_CShortOffCnt, c_CShortOffCntMinus = :c_CShortOffCntMinus, c_CDrumWrap = :c_CDrumWrap, ' +
    'c_CVCV = :c_CVCV, c_LckVCV = :c_LckVCV '+         //Nue:12.02.08
    'where c_prod_id = :c_prod_id and ' +
    'c_fragshift_id = :c_fragshift_id';

//  cUpdateProdGrpData =
//    'update t_dw_prodgroup_shift set ' +
//    'c_Len = :c_Len, c_Wei = :c_Wei, c_Bob = :c_Bob, c_Cones = :c_Cones, c_red = :c_red, ' +
//    'c_tRed = :c_tRed, c_tYell = :c_tYell, c_ManSt = :c_ManSt, c_tManSt = :c_tManSt, ' +
//    'c_rtSpd = :c_rtSpd, c_otSpd = :c_otSpd, c_wtSpd = :c_wtSpd, c_Sp = :c_Sp, c_RSp = :c_RSp, ' +
//    'c_YB = :c_YB, c_otProdGrp = :c_otProdGrp, c_wtProdGrp = :c_wtProdGrp , ' +
//    'c_LSt = :c_LSt, c_LStProd = :c_LStProd, c_LStBreak = :c_LStBreak, c_LStMat = :c_LStMat, ' +
//    'c_LStRev = :c_LStRev, c_LStClean = :c_LStClean, c_LStDef = :c_LStDef, ' +
//    'c_CS = :c_CS, c_CL = :c_CL, c_CT = :c_CT, c_CN = :c_CN, c_CSp = :c_CSp, c_CCl = :c_CCl, ' +
//    'c_UClS = :c_UClS, c_UClL = :c_UClL, c_UClT = :c_UClT, c_COffCnt = :c_COffCnt, ' +
//    'c_LckOffCnt = :c_LckOffCnt, c_CBu = :c_CBu, c_CDBu = :c_CDBu, c_CSys = :c_CSys, ' +
//    'c_LckSys = :c_LckSys, c_CUpY = :c_CUpY, c_CYTot = :c_CYTot, c_CSIRO = :c_CSIRO, ' +
//    'c_LckCl = :c_LckCl, c_CSiroCl = :c_CSiroCl, c_LckSiroCl = :c_LckSiroCl, ' +
//    'c_CSFI = :c_CSFI, c_LckSFI = :c_LckSFI, c_SFI = :c_SFI, c_SFICnt = :c_SFICnt, c_AdjustBase = :c_AdjustBase, c_AdjustBaseCnt = :c_AdjustBaseCnt, ' +
//    'c_LckSIRO = :c_LckSIRO, c_INeps = :c_INeps, c_IThick = :c_IThick, c_IThin = :c_IThin, ' +
//    'c_ISmall = :c_ISmall, c_I2_4 = :c_I2_4, c_I4_8 = :c_I4_8, c_I8_20 = :c_I8_20, ' +
//    'c_I20_70 = :c_I20_70 ' +
//    'where c_prod_id = :c_prod_id and ' +
//    'c_fragshift_id = :c_fragshift_id';

  //..........................................................................
  // Class
  //..........................................................................
  cInsertProdGrpClassCutFields =
    'insert t_dw_classCut ( ' +
    'c_classcut_id,' +
    'c_cC1,c_cC2,c_cC3,c_cC4,c_cC5,c_cC6,c_cC7,c_cC8,c_cC9,c_cC10,c_cC11,c_cC12,c_cC13,' +
    'c_cC14, c_cC15,c_cC16,c_cC17,c_cC18,c_cC19,c_cC20,c_cC21,c_cC22,c_cC23,c_cC24,c_cC25,' +
    'c_cC26,c_cC27,c_cC28,c_cC29,c_cC30,c_cC31,c_cC32,c_cC33,c_cC34,c_cC35,c_cC36,c_cC37,' +
    'c_cC38,c_cC39,c_cC40,c_cC41,c_cC42,c_cC43,c_cC44,c_cC45,c_cC46,c_cC47,c_cC48,c_cC49,' +
    'c_cC50,c_cC51,c_cC52,c_cC53,c_cC54,c_cC55,c_cC56,c_cC57,c_cC58,c_cC59,c_cC60,c_cC61,' +
    'c_cC62,c_cC63,c_cC64,c_cC65,c_cC66,c_cC67,c_cC68,c_cC69,c_cC70,c_cC71,c_cC72,c_cC73,' +
    'c_cC74,c_cC75,c_cC76,c_cC77,c_cC78,c_cC79,c_cC80,c_cC81,c_cC82,c_cC83,c_cC84,c_cC85,' +
    'c_cC86,c_cC87,c_cC88,c_cC89,c_cC90,c_cC91,c_cC92,c_cC93,c_cC94,c_cC95,c_cC96,c_cC97,' +
    'c_cC98,c_cC99,c_cC100,c_cC101,c_cC102,c_cC103,c_cC104,c_cC105,c_cC106,c_cC107,c_cC108,' +
    'c_cC109,c_cC110,c_cC111,c_cC112,c_cC113,c_cC114,c_cC115,c_cC116,c_cC117,c_cC118,' +
    'c_cC119,c_cC120,c_cC121,c_cC122,c_cC123,c_cC124,c_cC125,c_cC126,c_cC127,c_cC128 ) ' +
    'values ( ' +
    ':c_classcut_id, ' +
    ':c_cC1,:c_cC2,:c_cC3,:c_cC4,:c_cC5,:c_cC6,:c_cC7,:c_cC8,:c_cC9,:c_cC10,:c_cC11,:c_cC12,:c_cC13,' +
    ':c_cC14,:c_cC15,:c_cC16,:c_cC17,:c_cC18,:c_cC19,:c_cC20,:c_cC21,:c_cC22,:c_cC23,:c_cC24,:c_cC25,' +
    ':c_cC26,:c_cC27,:c_cC28,:c_cC29,:c_cC30,:c_cC31,:c_cC32,:c_cC33,:c_cC34,:c_cC35,:c_cC36,:c_cC37,' +
    ':c_cC38,:c_cC39,:c_cC40,:c_cC41,:c_cC42,:c_cC43,:c_cC44,:c_cC45,:c_cC46,:c_cC47,:c_cC48,:c_cC49,' +
    ':c_cC50,:c_cC51,:c_cC52,:c_cC53,:c_cC54,:c_cC55,:c_cC56,:c_cC57,:c_cC58,:c_cC59,:c_cC60,:c_cC61,' +
    ':c_cC62,:c_cC63,:c_cC64,:c_cC65,:c_cC66,:c_cC67,:c_cC68,:c_cC69,:c_cC70,:c_cC71,:c_cC72,:c_cC73,' +
    ':c_cC74,:c_cC75,:c_cC76,:c_cC77,:c_cC78,:c_cC79,:c_cC80,:c_cC81,:c_cC82,:c_cC83,:c_cC84,:c_cC85,' +
    ':c_cC86,:c_cC87,:c_cC88,:c_cC89,:c_cC90,:c_cC91,:c_cC92,:c_cC93,:c_cC94,:c_cC95,:c_cC96,:c_cC97,' +
    ':c_cC98,:c_cC99,:c_cC100,:c_cC101,:c_cC102,:c_cC103,:c_cC104,:c_cC105,:c_cC106,:c_cC107,:c_cC108,' +
    ':c_cC109,:c_cC110,:c_cC111,:c_cC112,:c_cC113,:c_cC114,:c_cC115,:c_cC116,:c_cC117,:c_cC118,' +
    ':c_cC119,:c_cC120,:c_cC121,:c_cC122,:c_cC123,:c_cC124,:c_cC125,:c_cC126,:c_cC127,:c_cC128 )';
  //..........................................................................
  cInsertProdGrpClassUncutFields =
    'insert t_dw_classUncut ( ' +
    'c_classuncut_id, ' +
    'c_cU1,c_cU2,c_cU3,c_cU4,c_cU5,c_cU6,c_cU7,c_cU8,c_cU9,c_cU10,c_cU11,c_cU12,c_cU13,' +
    'c_cU14, c_cU15,c_cU16,c_cU17,c_cU18,c_cU19,c_cU20,c_cU21,c_cU22,c_cU23,c_cU24,c_cU25,' +
    'c_cU26,c_cU27,c_cU28,c_cU29,c_cU30,c_cU31,c_cU32,c_cU33,c_cU34,c_cU35,c_cU36,c_cU37,' +
    'c_cU38,c_cU39,c_cU40,c_cU41,c_cU42,c_cU43,c_cU44,c_cU45,c_cU46,c_cU47,c_cU48,c_cU49,' +
    'c_cU50,c_cU51,c_cU52,c_cU53,c_cU54,c_cU55,c_cU56,c_cU57,c_cU58,c_cU59,c_cU60,c_cU61,' +
    'c_cU62,c_cU63,c_cU64,c_cU65,c_cU66,c_cU67,c_cU68,c_cU69,c_cU70,c_cU71,c_cU72,c_cU73,' +
    'c_cU74,c_cU75,c_cU76,c_cU77,c_cU78,c_cU79,c_cU80,c_cU81,c_cU82,c_cU83,c_cU84,c_cU85,' +
    'c_cU86,c_cU87,c_cU88,c_cU89,c_cU90,c_cU91,c_cU92,c_cU93,c_cU94,c_cU95,c_cU96,c_cU97,' +
    'c_cU98,c_cU99,c_cU100,c_cU101,c_cU102,c_cU103,c_cU104,c_cU105,c_cU106,c_cU107,c_cU108,' +
    'c_cU109,c_cU110,c_cU111,c_cU112,c_cU113,c_cU114,c_cU115,c_cU116,c_cU117,c_cU118,' +
    'c_cU119,c_cU120,c_cU121,c_cU122,c_cU123,c_cU124,c_cU125,c_cU126,c_cU127,c_cU128 ) ' +
    'values ( ' +
    ':c_classuncut_id, ' +
    ':c_cU1,:c_cU2,:c_cU3,:c_cU4,:c_cU5,:c_cU6,:c_cU7,:c_cU8,:c_cU9,:c_cU10,:c_cU11,:c_cU12,:c_cU13,' +
    ':c_cU14,:c_cU15,:c_cU16,:c_cU17,:c_cU18,:c_cU19,:c_cU20,:c_cU21,:c_cU22,:c_cU23,:c_cU24,:c_cU25,' +
    ':c_cU26,:c_cU27,:c_cU28,:c_cU29,:c_cU30,:c_cU31,:c_cU32,:c_cU33,:c_cU34,:c_cU35,:c_cU36,:c_cU37,' +
    ':c_cU38,:c_cU39,:c_cU40,:c_cU41,:c_cU42,:c_cU43,:c_cU44,:c_cU45,:c_cU46,:c_cU47,:c_cU48,:c_cU49,' +
    ':c_cU50,:c_cU51,:c_cU52,:c_cU53,:c_cU54,:c_cU55,:c_cU56,:c_cU57,:c_cU58,:c_cU59,:c_cU60,:c_cU61,' +
    ':c_cU62,:c_cU63,:c_cU64,:c_cU65,:c_cU66,:c_cU67,:c_cU68,:c_cU69,:c_cU70,:c_cU71,:c_cU72,:c_cU73,' +
    ':c_cU74,:c_cU75,:c_cU76,:c_cU77,:c_cU78,:c_cU79,:c_cU80,:c_cU81,:c_cU82,:c_cU83,:c_cU84,:c_cU85,' +
    ':c_cU86,:c_cU87,:c_cU88,:c_cU89,:c_cU90,:c_cU91,:c_cU92,:c_cU93,:c_cU94,:c_cU95,:c_cU96,:c_cU97,' +
    ':c_cU98,:c_cU99,:c_cU100,:c_cU101,:c_cU102,:c_cU103,:c_cU104,:c_cU105,:c_cU106,:c_cU107,:c_cU108,' +
    ':c_cU109,:c_cU110,:c_cU111,:c_cU112,:c_cU113,:c_cU114,:c_cU115,:c_cU116,:c_cU117,:c_cU118,' +
    ':c_cU119,:c_cU120,:c_cU121,:c_cU122,:c_cU123,:c_cU124,:c_cU125,:c_cU126,:c_cU127,:c_cU128 )';
  //..........................................................................
  cUpdateProdGrpClassCutFields =
    'update t_dw_classCut set ' +
    'c_cC1 = :c_cC1,c_cC2 = :c_cC2,c_cC3 = :c_cC3,c_cC4 = :c_cC4,c_cC5 = :c_cC5,c_cC6 = :c_cC6, ' +
    'c_cC7 = :c_cC7,c_cC8 = :c_cC8,c_cC9 = :c_cC9,c_cC10 = :c_cC10,c_cC11 = :c_cC11,c_cC12 = :c_cC12, ' +
    'c_cC13 = :c_cC13,c_cC14 = :c_cC14,c_cC15 = :c_cC15,c_cC16 = :c_cC16,c_cC17 = :c_cC17, ' +
    'c_cC18 = :c_cC18,c_cC19 = :c_cC19,c_cC20 = :c_cC20,c_cC21 = :c_cC21,c_cC22 = :c_cC22, ' +
    'c_cC23 = :c_cC23,c_cC24 = :c_cC24,c_cC25 = :c_cC25,c_cC26 = :c_cC26,c_cC27 = :c_cC27, ' +
    'c_cC28 = :c_cC28,c_cC29 = :c_cC29,c_cC30 = :c_cC30,c_cC31 = :c_cC31,c_cC32 = :c_cC32, ' +
    'c_cC33 = :c_cC33,c_cC34 = :c_cC34,c_cC35 = :c_cC35,c_cC36 = :c_cC36,c_cC37 = :c_cC37, ' +
    'c_cC38 = :c_cC38,c_cC39 = :c_cC39,c_cC40 = :c_cC40,c_cC41 = :c_cC41,c_cC42 = :c_cC42, ' +
    'c_cC43 = :c_cC43,c_cC44 = :c_cC44,c_cC45 = :c_cC45,c_cC46 = :c_cC46,c_cC47 = :c_cC47, ' +
    'c_cC48 = :c_cC48,c_cC49 = :c_cC49,c_cC50 = :c_cC50,c_cC51 = :c_cC51,c_cC52 = :c_cC52, ' +
    'c_cC53 = :c_cC53,c_cC54 = :c_cC54,c_cC55 = :c_cC55,c_cC56 = :c_cC56,c_cC57 = :c_cC57, ' +
    'c_cC58 = :c_cC58,c_cC59 = :c_cC59,c_cC60 = :c_cC60,c_cC61 = :c_cC61,c_cC62 = :c_cC62, ' +
    'c_cC63 = :c_cC63,c_cC64 = :c_cC64,c_cC65 = :c_cC65,c_cC66 = :c_cC66,c_cC67 = :c_cC67, ' +
    'c_cC68 = :c_cC68,c_cC69 = :c_cC69,c_cC70 = :c_cC70,c_cC71 = :c_cC71,c_cC72 = :c_cC72, ' +
    'c_cC73 = :c_cC73,c_cC74 = :c_cC74,c_cC75 = :c_cC75,c_cC76 = :c_cC76,c_cC77 = :c_cC77, ' +
    'c_cC78 = :c_cC78,c_cC79 = :c_cC79,c_cC80 = :c_cC80,c_cC81 = :c_cC81,c_cC82 = :c_cC82, ' +
    'c_cC83 = :c_cC83,c_cC84 = :c_cC84,c_cC85 = :c_cC85,c_cC86 = :c_cC86,c_cC87 = :c_cC87, ' +
    'c_cC88 = :c_cC88,c_cC89 = :c_cC89,c_cC90 = :c_cC90,c_cC91 = :c_cC91,c_cC92 = :c_cC92, ' +
    'c_cC93 = :c_cC93,c_cC94 = :c_cC94,c_cC95 = :c_cC95,c_cC96 = :c_cC96,c_cC97 = :c_cC97, ' +
    'c_cC98 = :c_cC98,c_cC99 = :c_cC99,c_cC100 = :c_cC100,c_cC101 = :c_cC101,c_cC102 = :c_cC102, ' +
    'c_cC103 = :c_cC103,c_cC104 = :c_cC104,c_cC105 = :c_cC105,c_cC106 = :c_cC106,c_cC107 = :c_cC107, ' +
    'c_cC108 = :c_cC108,c_cC109 = :c_cC109,c_cC110 = :c_cC110,c_cC111 = :c_cC111,c_cC112 = :c_cC112, ' +
    'c_cC113 = :c_cC113,c_cC114 = :c_cC114,c_cC115 = :c_cC115,c_cC116 = :c_cC116,c_cC117 = :c_cC117, ' +
    'c_cC118 = :c_cC118,c_cC119 = :c_cC119,c_cC120 = :c_cC120,c_cC121 = :c_cC121,c_cC122 = :c_cC122, ' +
    'c_cC123 = :c_cC123,c_cC124 = :c_cC124,c_cC125 = :c_cC125,c_cC126 = :c_cC126,c_cC127 = :c_cC127, ' +
    'c_cC128 = :c_cC128 ' +
    'where c_classcut_id = :c_classcut_id';
  //..........................................................................
  cUpdateProdGrpClassUncutFields =
    'update t_dw_classUncut set ' +
    'c_cU1 = :c_cU1,c_cU2 = :c_cU2,c_cU3 = :c_cU3,c_cU4 = :c_cU4,c_cU5 = :c_cU5,c_cU6 = :c_cU6, ' +
    'c_cU7 = :c_cU7,c_cU8 = :c_cU8,c_cU9 = :c_cU9,c_cU10 = :c_cU10,c_cU11 = :c_cU11,c_cU12 = :c_cU12, ' +
    'c_cU13 = :c_cU13,c_cU14 = :c_cU14,c_cU15 = :c_cU15,c_cU16 = :c_cU16,c_cU17 = :c_cU17, ' +
    'c_cU18 = :c_cU18,c_cU19 = :c_cU19,c_cU20 = :c_cU20,c_cU21 = :c_cU21,c_cU22 = :c_cU22, ' +
    'c_cU23 = :c_cU23,c_cU24 = :c_cU24,c_cU25 = :c_cU25,c_cU26 = :c_cU26,c_cU27 = :c_cU27, ' +
    'c_cU28 = :c_cU28,c_cU29 = :c_cU29,c_cU30 = :c_cU30,c_cU31 = :c_cU31,c_cU32 = :c_cU32, ' +
    'c_cU33 = :c_cU33,c_cU34 = :c_cU34,c_cU35 = :c_cU35,c_cU36 = :c_cU36,c_cU37 = :c_cU37, ' +
    'c_cU38 = :c_cU38,c_cU39 = :c_cU39,c_cU40 = :c_cU40,c_cU41 = :c_cU41,c_cU42 = :c_cU42, ' +
    'c_cU43 = :c_cU43,c_cU44 = :c_cU44,c_cU45 = :c_cU45,c_cU46 = :c_cU46,c_cU47 = :c_cU47, ' +
    'c_cU48 = :c_cU48,c_cU49 = :c_cU49,c_cU50 = :c_cU50,c_cU51 = :c_cU51,c_cU52 = :c_cU52, ' +
    'c_cU53 = :c_cU53,c_cU54 = :c_cU54,c_cU55 = :c_cU55,c_cU56 = :c_cU56,c_cU57 = :c_cU57, ' +
    'c_cU58 = :c_cU58,c_cU59 = :c_cU59,c_cU60 = :c_cU60,c_cU61 = :c_cU61,c_cU62 = :c_cU62, ' +
    'c_cU63 = :c_cU63,c_cU64 = :c_cU64,c_cU65 = :c_cU65,c_cU66 = :c_cU66,c_cU67 = :c_cU67, ' +
    'c_cU68 = :c_cU68,c_cU69 = :c_cU69,c_cU70 = :c_cU70,c_cU71 = :c_cU71,c_cU72 = :c_cU72, ' +
    'c_cU73 = :c_cU73,c_cU74 = :c_cU74,c_cU75 = :c_cU75,c_cU76 = :c_cU76,c_cU77 = :c_cU77, ' +
    'c_cU78 = :c_cU78,c_cU79 = :c_cU79,c_cU80 = :c_cU80,c_cU81 = :c_cU81,c_cU82 = :c_cU82, ' +
    'c_cU83 = :c_cU83,c_cU84 = :c_cU84,c_cU85 = :c_cU85,c_cU86 = :c_cU86,c_cU87 = :c_cU87, ' +
    'c_cU88 = :c_cU88,c_cU89 = :c_cU89,c_cU90 = :c_cU90,c_cU91 = :c_cU91,c_cU92 = :c_cU92, ' +
    'c_cU93 = :c_cU93,c_cU94 = :c_cU94,c_cU95 = :c_cU95,c_cU96 = :c_cU96,c_cU97 = :c_cU97, ' +
    'c_cU98 = :c_cU98,c_cU99 = :c_cU99,c_cU100 = :c_cU100,c_cU101 = :c_cU101,c_cU102 = :c_cU102, ' +
    'c_cU103 = :c_cU103,c_cU104 = :c_cU104,c_cU105 = :c_cU105,c_cU106 = :c_cU106,c_cU107 = :c_cU107, ' +
    'c_cU108 = :c_cU108,c_cU109 = :c_cU109,c_cU110 = :c_cU110,c_cU111 = :c_cU111,c_cU112 = :c_cU112, ' +
    'c_cU113 = :c_cU113,c_cU114 = :c_cU114,c_cU115 = :c_cU115,c_cU116 = :c_cU116,c_cU117 = :c_cU117, ' +
    'c_cU118 = :c_cU118,c_cU119 = :c_cU119,c_cU120 = :c_cU120,c_cU121 = :c_cU121,c_cU122 = :c_cU122, ' +
    'c_cU123 = :c_cU123,c_cU124 = :c_cU124,c_cU125 = :c_cU125,c_cU126 = :c_cU126,c_cU127 = :c_cU127, ' +
    'c_cU128 = :c_cU128 ' +
    'where c_classuncut_id = :c_classuncut_id';

  //..........................................................................
  // Splice
  //..........................................................................
  cInsertProdGrpSpliceCutFields =
    'insert t_dw_spliceCut ( ' +
    'c_splicecut_id, ' +
    'c_spC1,c_spC2,c_spC3,c_spC4,c_spC5,c_spC6,c_spC7,c_spC8,c_spC9,c_spC10,c_spC11,c_spC12,c_spC13,' +
    'c_spC14, c_spC15,c_spC16,c_spC17,c_spC18,c_spC19,c_spC20,c_spC21,c_spC22,c_spC23,c_spC24,c_spC25,' +
    'c_spC26,c_spC27,c_spC28,c_spC29,c_spC30,c_spC31,c_spC32,c_spC33,c_spC34,c_spC35,c_spC36,c_spC37,' +
    'c_spC38,c_spC39,c_spC40,c_spC41,c_spC42,c_spC43,c_spC44,c_spC45,c_spC46,c_spC47,c_spC48,c_spC49,' +
    'c_spC50,c_spC51,c_spC52,c_spC53,c_spC54,c_spC55,c_spC56,c_spC57,c_spC58,c_spC59,c_spC60,c_spC61,' +
    'c_spC62,c_spC63,c_spC64,c_spC65,c_spC66,c_spC67,c_spC68,c_spC69,c_spC70,c_spC71,c_spC72,c_spC73,' +
    'c_spC74,c_spC75,c_spC76,c_spC77,c_spC78,c_spC79,c_spC80,c_spC81,c_spC82,c_spC83,c_spC84,c_spC85,' +
    'c_spC86,c_spC87,c_spC88,c_spC89,c_spC90,c_spC91,c_spC92,c_spC93,c_spC94,c_spC95,c_spC96,c_spC97,' +
    'c_spC98,c_spC99,c_spC100,c_spC101,c_spC102,c_spC103,c_spC104,c_spC105,c_spC106,c_spC107,c_spC108,' +
    'c_spC109,c_spC110,c_spC111,c_spC112,c_spC113,c_spC114,c_spC115,c_spC116,c_spC117,c_spC118,' +
    'c_spC119,c_spC120,c_spC121,c_spC122,c_spC123,c_spC124,c_spC125,c_spC126,c_spC127,c_spC128 ) ' +
    'values ( ' +
    ':c_splicecut_id,' +
    ':c_spC1,:c_spC2,:c_spC3,:c_spC4,:c_spC5,:c_spC6,:c_spC7,:c_spC8,:c_spC9,:c_spC10,:c_spC11,:c_spC12,:c_spC13,' +
    ':c_spC14,:c_spC15,:c_spC16,:c_spC17,:c_spC18,:c_spC19,:c_spC20,:c_spC21,:c_spC22,:c_spC23,:c_spC24,:c_spC25,' +
    ':c_spC26,:c_spC27,:c_spC28,:c_spC29,:c_spC30,:c_spC31,:c_spC32,:c_spC33,:c_spC34,:c_spC35,:c_spC36,:c_spC37,' +
    ':c_spC38,:c_spC39,:c_spC40,:c_spC41,:c_spC42,:c_spC43,:c_spC44,:c_spC45,:c_spC46,:c_spC47,:c_spC48,:c_spC49,' +
    ':c_spC50,:c_spC51,:c_spC52,:c_spC53,:c_spC54,:c_spC55,:c_spC56,:c_spC57,:c_spC58,:c_spC59,:c_spC60,:c_spC61,' +
    ':c_spC62,:c_spC63,:c_spC64,:c_spC65,:c_spC66,:c_spC67,:c_spC68,:c_spC69,:c_spC70,:c_spC71,:c_spC72,:c_spC73,' +
    ':c_spC74,:c_spC75,:c_spC76,:c_spC77,:c_spC78,:c_spC79,:c_spC80,:c_spC81,:c_spC82,:c_spC83,:c_spC84,:c_spC85,' +
    ':c_spC86,:c_spC87,:c_spC88,:c_spC89,:c_spC90,:c_spC91,:c_spC92,:c_spC93,:c_spC94,:c_spC95,:c_spC96,:c_spC97,' +
    ':c_spC98,:c_spC99,:c_spC100,:c_spC101,:c_spC102,:c_spC103,:c_spC104,:c_spC105,:c_spC106,:c_spC107,:c_spC108,' +
    ':c_spC109,:c_spC110,:c_spC111,:c_spC112,:c_spC113,:c_spC114,:c_spC115,:c_spC116,:c_spC117,:c_spC118,' +
    ':c_spC119,:c_spC120,:c_spC121,:c_spC122,:c_spC123,:c_spC124,:c_spC125,:c_spC126,:c_spC127,:c_spC128 )';
  //..........................................................................
  cInsertProdGrpSpliceUncutFields =
    'insert t_dw_spliceUncut ( ' +
    'c_spliceuncut_id,' +
    'c_spU1,c_spU2,c_spU3,c_spU4,c_spU5,c_spU6,c_spU7,c_spU8,c_spU9,c_spU10,c_spU11,c_spU12,c_spU13,' +
    'c_spU14, c_spU15,c_spU16,c_spU17,c_spU18,c_spU19,c_spU20,c_spU21,c_spU22,c_spU23,c_spU24,c_spU25,' +
    'c_spU26,c_spU27,c_spU28,c_spU29,c_spU30,c_spU31,c_spU32,c_spU33,c_spU34,c_spU35,c_spU36,c_spU37,' +
    'c_spU38,c_spU39,c_spU40,c_spU41,c_spU42,c_spU43,c_spU44,c_spU45,c_spU46,c_spU47,c_spU48,c_spU49,' +
    'c_spU50,c_spU51,c_spU52,c_spU53,c_spU54,c_spU55,c_spU56,c_spU57,c_spU58,c_spU59,c_spU60,c_spU61,' +
    'c_spU62,c_spU63,c_spU64,c_spU65,c_spU66,c_spU67,c_spU68,c_spU69,c_spU70,c_spU71,c_spU72,c_spU73,' +
    'c_spU74,c_spU75,c_spU76,c_spU77,c_spU78,c_spU79,c_spU80,c_spU81,c_spU82,c_spU83,c_spU84,c_spU85,' +
    'c_spU86,c_spU87,c_spU88,c_spU89,c_spU90,c_spU91,c_spU92,c_spU93,c_spU94,c_spU95,c_spU96,c_spU97,' +
    'c_spU98,c_spU99,c_spU100,c_spU101,c_spU102,c_spU103,c_spU104,c_spU105,c_spU106,c_spU107,c_spU108,' +
    'c_spU109,c_spU110,c_spU111,c_spU112,c_spU113,c_spU114,c_spU115,c_spU116,c_spU117,c_spU118,' +
    'c_spU119,c_spU120,c_spU121,c_spU122,c_spU123,c_spU124,c_spU125,c_spU126,c_spU127,c_spU128 ) ' +
    'values ( ' +
    ':c_spliceuncut_id,' +
    ':c_spU1,:c_spU2,:c_spU3,:c_spU4,:c_spU5,:c_spU6,:c_spU7,:c_spU8,:c_spU9,:c_spU10,:c_spU11,:c_spU12,:c_spU13,' +
    ':c_spU14,:c_spU15,:c_spU16,:c_spU17,:c_spU18,:c_spU19,:c_spU20,:c_spU21,:c_spU22,:c_spU23,:c_spU24,:c_spU25,' +
    ':c_spU26,:c_spU27,:c_spU28,:c_spU29,:c_spU30,:c_spU31,:c_spU32,:c_spU33,:c_spU34,:c_spU35,:c_spU36,:c_spU37,' +
    ':c_spU38,:c_spU39,:c_spU40,:c_spU41,:c_spU42,:c_spU43,:c_spU44,:c_spU45,:c_spU46,:c_spU47,:c_spU48,:c_spU49,' +
    ':c_spU50,:c_spU51,:c_spU52,:c_spU53,:c_spU54,:c_spU55,:c_spU56,:c_spU57,:c_spU58,:c_spU59,:c_spU60,:c_spU61,' +
    ':c_spU62,:c_spU63,:c_spU64,:c_spU65,:c_spU66,:c_spU67,:c_spU68,:c_spU69,:c_spU70,:c_spU71,:c_spU72,:c_spU73,' +
    ':c_spU74,:c_spU75,:c_spU76,:c_spU77,:c_spU78,:c_spU79,:c_spU80,:c_spU81,:c_spU82,:c_spU83,:c_spU84,:c_spU85,' +
    ':c_spU86,:c_spU87,:c_spU88,:c_spU89,:c_spU90,:c_spU91,:c_spU92,:c_spU93,:c_spU94,:c_spU95,:c_spU96,:c_spU97,' +
    ':c_spU98,:c_spU99,:c_spU100,:c_spU101,:c_spU102,:c_spU103,:c_spU104,:c_spU105,:c_spU106,:c_spU107,:c_spU108,' +
    ':c_spU109,:c_spU110,:c_spU111,:c_spU112,:c_spU113,:c_spU114,:c_spU115,:c_spU116,:c_spU117,:c_spU118,' +
    ':c_spU119,:c_spU120,:c_spU121,:c_spU122,:c_spU123,:c_spU124,:c_spU125,:c_spU126,:c_spU127,:c_spU128 )';
  //..........................................................................
  cUpdateProdGrpSpliceCutFields =
    'update t_dw_spliceCut set ' +
    'c_spC1 = :c_spC1,c_spC2 = :c_spC2,c_spC3 = :c_spC3,c_spC4 = :c_spC4,c_spC5 = :c_spC5,c_spC6 = :c_spC6, ' +
    'c_spC7 = :c_spC7,c_spC8 = :c_spC8,c_spC9 = :c_spC9,c_spC10 = :c_spC10,c_spC11 = :c_spC11,c_spC12 = :c_spC12, ' +
    'c_spC13 = :c_spC13,c_spC14 = :c_spC14,c_spC15 = :c_spC15,c_spC16 = :c_spC16,c_spC17 = :c_spC17, ' +
    'c_spC18 = :c_spC18,c_spC19 = :c_spC19,c_spC20 = :c_spC20,c_spC21 = :c_spC21,c_spC22 = :c_spC22, ' +
    'c_spC23 = :c_spC23,c_spC24 = :c_spC24,c_spC25 = :c_spC25,c_spC26 = :c_spC26,c_spC27 = :c_spC27, ' +
    'c_spC28 = :c_spC28,c_spC29 = :c_spC29,c_spC30 = :c_spC30,c_spC31 = :c_spC31,c_spC32 = :c_spC32, ' +
    'c_spC33 = :c_spC33,c_spC34 = :c_spC34,c_spC35 = :c_spC35,c_spC36 = :c_spC36,c_spC37 = :c_spC37, ' +
    'c_spC38 = :c_spC38,c_spC39 = :c_spC39,c_spC40 = :c_spC40,c_spC41 = :c_spC41,c_spC42 = :c_spC42, ' +
    'c_spC43 = :c_spC43,c_spC44 = :c_spC44,c_spC45 = :c_spC45,c_spC46 = :c_spC46,c_spC47 = :c_spC47, ' +
    'c_spC48 = :c_spC48,c_spC49 = :c_spC49,c_spC50 = :c_spC50,c_spC51 = :c_spC51,c_spC52 = :c_spC52, ' +
    'c_spC53 = :c_spC53,c_spC54 = :c_spC54,c_spC55 = :c_spC55,c_spC56 = :c_spC56,c_spC57 = :c_spC57, ' +
    'c_spC58 = :c_spC58,c_spC59 = :c_spC59,c_spC60 = :c_spC60,c_spC61 = :c_spC61,c_spC62 = :c_spC62, ' +
    'c_spC63 = :c_spC63,c_spC64 = :c_spC64,c_spC65 = :c_spC65,c_spC66 = :c_spC66,c_spC67 = :c_spC67, ' +
    'c_spC68 = :c_spC68,c_spC69 = :c_spC69,c_spC70 = :c_spC70,c_spC71 = :c_spC71,c_spC72 = :c_spC72, ' +
    'c_spC73 = :c_spC73,c_spC74 = :c_spC74,c_spC75 = :c_spC75,c_spC76 = :c_spC76,c_spC77 = :c_spC77, ' +
    'c_spC78 = :c_spC78,c_spC79 = :c_spC79,c_spC80 = :c_spC80,c_spC81 = :c_spC81,c_spC82 = :c_spC82, ' +
    'c_spC83 = :c_spC83,c_spC84 = :c_spC84,c_spC85 = :c_spC85,c_spC86 = :c_spC86,c_spC87 = :c_spC87, ' +
    'c_spC88 = :c_spC88,c_spC89 = :c_spC89,c_spC90 = :c_spC90,c_spC91 = :c_spC91,c_spC92 = :c_spC92, ' +
    'c_spC93 = :c_spC93,c_spC94 = :c_spC94,c_spC95 = :c_spC95,c_spC96 = :c_spC96,c_spC97 = :c_spC97, ' +
    'c_spC98 = :c_spC98,c_spC99 = :c_spC99,c_spC100 = :c_spC100,c_spC101 = :c_spC101,c_spC102 = :c_spC102, ' +
    'c_spC103 = :c_spC103,c_spC104 = :c_spC104,c_spC105 = :c_spC105,c_spC106 = :c_spC106,c_spC107 = :c_spC107, ' +
    'c_spC108 = :c_spC108,c_spC109 = :c_spC109,c_spC110 = :c_spC110,c_spC111 = :c_spC111,c_spC112 = :c_spC112, ' +
    'c_spC113 = :c_spC113,c_spC114 = :c_spC114,c_spC115 = :c_spC115,c_spC116 = :c_spC116,c_spC117 = :c_spC117, ' +
    'c_spC118 = :c_spC118,c_spC119 = :c_spC119,c_spC120 = :c_spC120,c_spC121 = :c_spC121,c_spC122 = :c_spC122, ' +
    'c_spC123 = :c_spC123,c_spC124 = :c_spC124,c_spC125 = :c_spC125,c_spC126 = :c_spC126,c_spC127 = :c_spC127, ' +
    'c_spC128 = :c_spC128 ' +
    'where c_splicecut_id = :c_splicecut_id';
  //..........................................................................
  cUpdateProdGrpSpliceUncutFields =
    'update t_dw_spliceUncut set ' +
    'c_spU1 = :c_spU1,c_spU2 = :c_spU2,c_spU3 = :c_spU3,c_spU4 = :c_spU4,c_spU5 = :c_spU5,c_spU6 = :c_spU6, ' +
    'c_spU7 = :c_spU7,c_spU8 = :c_spU8,c_spU9 = :c_spU9,c_spU10 = :c_spU10,c_spU11 = :c_spU11,c_spU12 = :c_spU12, ' +
    'c_spU13 = :c_spU13,c_spU14 = :c_spU14,c_spU15 = :c_spU15,c_spU16 = :c_spU16,c_spU17 = :c_spU17, ' +
    'c_spU18 = :c_spU18,c_spU19 = :c_spU19,c_spU20 = :c_spU20,c_spU21 = :c_spU21,c_spU22 = :c_spU22, ' +
    'c_spU23 = :c_spU23,c_spU24 = :c_spU24,c_spU25 = :c_spU25,c_spU26 = :c_spU26,c_spU27 = :c_spU27, ' +
    'c_spU28 = :c_spU28,c_spU29 = :c_spU29,c_spU30 = :c_spU30,c_spU31 = :c_spU31,c_spU32 = :c_spU32, ' +
    'c_spU33 = :c_spU33,c_spU34 = :c_spU34,c_spU35 = :c_spU35,c_spU36 = :c_spU36,c_spU37 = :c_spU37, ' +
    'c_spU38 = :c_spU38,c_spU39 = :c_spU39,c_spU40 = :c_spU40,c_spU41 = :c_spU41,c_spU42 = :c_spU42, ' +
    'c_spU43 = :c_spU43,c_spU44 = :c_spU44,c_spU45 = :c_spU45,c_spU46 = :c_spU46,c_spU47 = :c_spU47, ' +
    'c_spU48 = :c_spU48,c_spU49 = :c_spU49,c_spU50 = :c_spU50,c_spU51 = :c_spU51,c_spU52 = :c_spU52, ' +
    'c_spU53 = :c_spU53,c_spU54 = :c_spU54,c_spU55 = :c_spU55,c_spU56 = :c_spU56,c_spU57 = :c_spU57, ' +
    'c_spU58 = :c_spU58,c_spU59 = :c_spU59,c_spU60 = :c_spU60,c_spU61 = :c_spU61,c_spU62 = :c_spU62, ' +
    'c_spU63 = :c_spU63,c_spU64 = :c_spU64,c_spU65 = :c_spU65,c_spU66 = :c_spU66,c_spU67 = :c_spU67, ' +
    'c_spU68 = :c_spU68,c_spU69 = :c_spU69,c_spU70 = :c_spU70,c_spU71 = :c_spU71,c_spU72 = :c_spU72, ' +
    'c_spU73 = :c_spU73,c_spU74 = :c_spU74,c_spU75 = :c_spU75,c_spU76 = :c_spU76,c_spU77 = :c_spU77, ' +
    'c_spU78 = :c_spU78,c_spU79 = :c_spU79,c_spU80 = :c_spU80,c_spU81 = :c_spU81,c_spU82 = :c_spU82, ' +
    'c_spU83 = :c_spU83,c_spU84 = :c_spU84,c_spU85 = :c_spU85,c_spU86 = :c_spU86,c_spU87 = :c_spU87, ' +
    'c_spU88 = :c_spU88,c_spU89 = :c_spU89,c_spU90 = :c_spU90,c_spU91 = :c_spU91,c_spU92 = :c_spU92, ' +
    'c_spU93 = :c_spU93,c_spU94 = :c_spU94,c_spU95 = :c_spU95,c_spU96 = :c_spU96,c_spU97 = :c_spU97, ' +
    'c_spU98 = :c_spU98,c_spU99 = :c_spU99,c_spU100 = :c_spU100,c_spU101 = :c_spU101,c_spU102 = :c_spU102, ' +
    'c_spU103 = :c_spU103,c_spU104 = :c_spU104,c_spU105 = :c_spU105,c_spU106 = :c_spU106,c_spU107 = :c_spU107, ' +
    'c_spU108 = :c_spU108,c_spU109 = :c_spU109,c_spU110 = :c_spU110,c_spU111 = :c_spU111,c_spU112 = :c_spU112, ' +
    'c_spU113 = :c_spU113,c_spU114 = :c_spU114,c_spU115 = :c_spU115,c_spU116 = :c_spU116,c_spU117 = :c_spU117, ' +
    'c_spU118 = :c_spU118,c_spU119 = :c_spU119,c_spU120 = :c_spU120,c_spU121 = :c_spU121,c_spU122 = :c_spU122, ' +
    'c_spU123 = :c_spU123,c_spU124 = :c_spU124,c_spU125 = :c_spU125,c_spU126 = :c_spU126,c_spU127 = :c_spU127, ' +
    'c_spU128 = :c_spU128 ' +
    'where c_spliceuncut_id = :c_spliceuncut_id';

  //..........................................................................
  // SIRO
  //..........................................................................
  cInsertProdGrpSiroCutFields =
    'insert t_dw_siroCut ( ' +
    'c_sirocut_id,' +
    'c_siC1,c_siC2,c_siC3,c_siC4,c_siC5,c_siC6,c_siC7,c_siC8,c_siC9,c_siC10,c_siC11,c_siC12,c_siC13,' +
    'c_siC14, c_siC15,c_siC16,c_siC17,c_siC18,c_siC19,c_siC20,c_siC21,c_siC22,c_siC23,c_siC24,c_siC25,' +
    'c_siC26,c_siC27,c_siC28,c_siC29,c_siC30,c_siC31,c_siC32,c_siC33,c_siC34,c_siC35,c_siC36,c_siC37,' +
    'c_siC38,c_siC39,c_siC40,c_siC41,c_siC42,c_siC43,c_siC44,c_siC45,c_siC46,c_siC47,c_siC48,c_siC49,' +
    'c_siC50,c_siC51,c_siC52,c_siC53,c_siC54,c_siC55,c_siC56,c_siC57,c_siC58,c_siC59,c_siC60,c_siC61,' +
    'c_siC62,c_siC63,c_siC64,c_siC65,c_siC66,c_siC67,c_siC68,c_siC69,c_siC70,c_siC71,c_siC72,c_siC73,' +
    'c_siC74,c_siC75,c_siC76,c_siC77,c_siC78,c_siC79,c_siC80,c_siC81,c_siC82,c_siC83,c_siC84,c_siC85,' +
    'c_siC86,c_siC87,c_siC88,c_siC89,c_siC90,c_siC91,c_siC92,c_siC93,c_siC94,c_siC95,c_siC96,c_siC97,' +
    'c_siC98,c_siC99,c_siC100,c_siC101,c_siC102,c_siC103,c_siC104,c_siC105,c_siC106,c_siC107,c_siC108,' +
    'c_siC109,c_siC110,c_siC111,c_siC112,c_siC113,c_siC114,c_siC115,c_siC116,c_siC117,c_siC118,' +
    'c_siC119,c_siC120,c_siC121,c_siC122,c_siC123,c_siC124,c_siC125,c_siC126,c_siC127,c_siC128 ) ' +
    'values ( ' +
    ':c_sirocut_id,' +
    ':c_siC1,:c_siC2,:c_siC3,:c_siC4,:c_siC5,:c_siC6,:c_siC7,:c_siC8,:c_siC9,:c_siC10,:c_siC11,:c_siC12,:c_siC13,' +
    ':c_siC14,:c_siC15,:c_siC16,:c_siC17,:c_siC18,:c_siC19,:c_siC20,:c_siC21,:c_siC22,:c_siC23,:c_siC24,:c_siC25,' +
    ':c_siC26,:c_siC27,:c_siC28,:c_siC29,:c_siC30,:c_siC31,:c_siC32,:c_siC33,:c_siC34,:c_siC35,:c_siC36,:c_siC37,' +
    ':c_siC38,:c_siC39,:c_siC40,:c_siC41,:c_siC42,:c_siC43,:c_siC44,:c_siC45,:c_siC46,:c_siC47,:c_siC48,:c_siC49,' +
    ':c_siC50,:c_siC51,:c_siC52,:c_siC53,:c_siC54,:c_siC55,:c_siC56,:c_siC57,:c_siC58,:c_siC59,:c_siC60,:c_siC61,' +
    ':c_siC62,:c_siC63,:c_siC64,:c_siC65,:c_siC66,:c_siC67,:c_siC68,:c_siC69,:c_siC70,:c_siC71,:c_siC72,:c_siC73,' +
    ':c_siC74,:c_siC75,:c_siC76,:c_siC77,:c_siC78,:c_siC79,:c_siC80,:c_siC81,:c_siC82,:c_siC83,:c_siC84,:c_siC85,' +
    ':c_siC86,:c_siC87,:c_siC88,:c_siC89,:c_siC90,:c_siC91,:c_siC92,:c_siC93,:c_siC94,:c_siC95,:c_siC96,:c_siC97,' +
    ':c_siC98,:c_siC99,:c_siC100,:c_siC101,:c_siC102,:c_siC103,:c_siC104,:c_siC105,:c_siC106,:c_siC107,:c_siC108,' +
    ':c_siC109,:c_siC110,:c_siC111,:c_siC112,:c_siC113,:c_siC114,:c_siC115,:c_siC116,:c_siC117,:c_siC118,' +
    ':c_siC119,:c_siC120,:c_siC121,:c_siC122,:c_siC123,:c_siC124,:c_siC125,:c_siC126,:c_siC127,:c_siC128 )';
  //..........................................................................
  cInsertProdGrpSiroUncutFields =
    'insert t_dw_siroUncut ( ' +
    'c_sirouncut_id,' +
    'c_siU1,c_siU2,c_siU3,c_siU4,c_siU5,c_siU6,c_siU7,c_siU8,c_siU9,c_siU10,c_siU11,c_siU12,c_siU13,' +
    'c_siU14, c_siU15,c_siU16,c_siU17,c_siU18,c_siU19,c_siU20,c_siU21,c_siU22,c_siU23,c_siU24,c_siU25,' +
    'c_siU26,c_siU27,c_siU28,c_siU29,c_siU30,c_siU31,c_siU32,c_siU33,c_siU34,c_siU35,c_siU36,c_siU37,' +
    'c_siU38,c_siU39,c_siU40,c_siU41,c_siU42,c_siU43,c_siU44,c_siU45,c_siU46,c_siU47,c_siU48,c_siU49,' +
    'c_siU50,c_siU51,c_siU52,c_siU53,c_siU54,c_siU55,c_siU56,c_siU57,c_siU58,c_siU59,c_siU60,c_siU61,' +
    'c_siU62,c_siU63,c_siU64,c_siU65,c_siU66,c_siU67,c_siU68,c_siU69,c_siU70,c_siU71,c_siU72,c_siU73,' +
    'c_siU74,c_siU75,c_siU76,c_siU77,c_siU78,c_siU79,c_siU80,c_siU81,c_siU82,c_siU83,c_siU84,c_siU85,' +
    'c_siU86,c_siU87,c_siU88,c_siU89,c_siU90,c_siU91,c_siU92,c_siU93,c_siU94,c_siU95,c_siU96,c_siU97,' +
    'c_siU98,c_siU99,c_siU100,c_siU101,c_siU102,c_siU103,c_siU104,c_siU105,c_siU106,c_siU107,c_siU108,' +
    'c_siU109,c_siU110,c_siU111,c_siU112,c_siU113,c_siU114,c_siU115,c_siU116,c_siU117,c_siU118,' +
    'c_siU119,c_siU120,c_siU121,c_siU122,c_siU123,c_siU124,c_siU125,c_siU126,c_siU127,c_siU128 ) ' +
    'values ( ' +
    ':c_sirouncut_id,' +
    ':c_siU1,:c_siU2,:c_siU3,:c_siU4,:c_siU5,:c_siU6,:c_siU7,:c_siU8,:c_siU9,:c_siU10,:c_siU11,:c_siU12,:c_siU13,' +
    ':c_siU14,:c_siU15,:c_siU16,:c_siU17,:c_siU18,:c_siU19,:c_siU20,:c_siU21,:c_siU22,:c_siU23,:c_siU24,:c_siU25,' +
    ':c_siU26,:c_siU27,:c_siU28,:c_siU29,:c_siU30,:c_siU31,:c_siU32,:c_siU33,:c_siU34,:c_siU35,:c_siU36,:c_siU37,' +
    ':c_siU38,:c_siU39,:c_siU40,:c_siU41,:c_siU42,:c_siU43,:c_siU44,:c_siU45,:c_siU46,:c_siU47,:c_siU48,:c_siU49,' +
    ':c_siU50,:c_siU51,:c_siU52,:c_siU53,:c_siU54,:c_siU55,:c_siU56,:c_siU57,:c_siU58,:c_siU59,:c_siU60,:c_siU61,' +
    ':c_siU62,:c_siU63,:c_siU64,:c_siU65,:c_siU66,:c_siU67,:c_siU68,:c_siU69,:c_siU70,:c_siU71,:c_siU72,:c_siU73,' +
    ':c_siU74,:c_siU75,:c_siU76,:c_siU77,:c_siU78,:c_siU79,:c_siU80,:c_siU81,:c_siU82,:c_siU83,:c_siU84,:c_siU85,' +
    ':c_siU86,:c_siU87,:c_siU88,:c_siU89,:c_siU90,:c_siU91,:c_siU92,:c_siU93,:c_siU94,:c_siU95,:c_siU96,:c_siU97,' +
    ':c_siU98,:c_siU99,:c_siU100,:c_siU101,:c_siU102,:c_siU103,:c_siU104,:c_siU105,:c_siU106,:c_siU107,:c_siU108,' +
    ':c_siU109,:c_siU110,:c_siU111,:c_siU112,:c_siU113,:c_siU114,:c_siU115,:c_siU116,:c_siU117,:c_siU118,' +
    ':c_siU119,:c_siU120,:c_siU121,:c_siU122,:c_siU123,:c_siU124,:c_siU125,:c_siU126,:c_siU127,:c_siU128 )';
  //..........................................................................
  cUpdateProdGrpSiroCutFields =
    'update t_dw_siroCut set ' +
    'c_siC1 = :c_siC1,c_siC2 = :c_siC2,c_siC3 = :c_siC3,c_siC4 = :c_siC4,c_siC5 = :c_siC5,c_siC6 = :c_siC6, ' +
    'c_siC7 = :c_siC7,c_siC8 = :c_siC8,c_siC9 = :c_siC9,c_siC10 = :c_siC10,c_siC11 = :c_siC11,c_siC12 = :c_siC12, ' +
    'c_siC13 = :c_siC13,c_siC14 = :c_siC14,c_siC15 = :c_siC15,c_siC16 = :c_siC16,c_siC17 = :c_siC17, ' +
    'c_siC18 = :c_siC18,c_siC19 = :c_siC19,c_siC20 = :c_siC20,c_siC21 = :c_siC21,c_siC22 = :c_siC22, ' +
    'c_siC23 = :c_siC23,c_siC24 = :c_siC24,c_siC25 = :c_siC25,c_siC26 = :c_siC26,c_siC27 = :c_siC27, ' +
    'c_siC28 = :c_siC28,c_siC29 = :c_siC29,c_siC30 = :c_siC30,c_siC31 = :c_siC31,c_siC32 = :c_siC32, ' +
    'c_siC33 = :c_siC33,c_siC34 = :c_siC34,c_siC35 = :c_siC35,c_siC36 = :c_siC36,c_siC37 = :c_siC37, ' +
    'c_siC38 = :c_siC38,c_siC39 = :c_siC39,c_siC40 = :c_siC40,c_siC41 = :c_siC41,c_siC42 = :c_siC42, ' +
    'c_siC43 = :c_siC43,c_siC44 = :c_siC44,c_siC45 = :c_siC45,c_siC46 = :c_siC46,c_siC47 = :c_siC47, ' +
    'c_siC48 = :c_siC48,c_siC49 = :c_siC49,c_siC50 = :c_siC50,c_siC51 = :c_siC51,c_siC52 = :c_siC52, ' +
    'c_siC53 = :c_siC53,c_siC54 = :c_siC54,c_siC55 = :c_siC55,c_siC56 = :c_siC56,c_siC57 = :c_siC57, ' +
    'c_siC58 = :c_siC58,c_siC59 = :c_siC59,c_siC60 = :c_siC60,c_siC61 = :c_siC61,c_siC62 = :c_siC62, ' +
    'c_siC63 = :c_siC63,c_siC64 = :c_siC64,c_siC65 = :c_siC65,c_siC66 = :c_siC66,c_siC67 = :c_siC67, ' +
    'c_siC68 = :c_siC68,c_siC69 = :c_siC69,c_siC70 = :c_siC70,c_siC71 = :c_siC71,c_siC72 = :c_siC72, ' +
    'c_siC73 = :c_siC73,c_siC74 = :c_siC74,c_siC75 = :c_siC75,c_siC76 = :c_siC76,c_siC77 = :c_siC77, ' +
    'c_siC78 = :c_siC78,c_siC79 = :c_siC79,c_siC80 = :c_siC80,c_siC81 = :c_siC81,c_siC82 = :c_siC82, ' +
    'c_siC83 = :c_siC83,c_siC84 = :c_siC84,c_siC85 = :c_siC85,c_siC86 = :c_siC86,c_siC87 = :c_siC87, ' +
    'c_siC88 = :c_siC88,c_siC89 = :c_siC89,c_siC90 = :c_siC90,c_siC91 = :c_siC91,c_siC92 = :c_siC92, ' +
    'c_siC93 = :c_siC93,c_siC94 = :c_siC94,c_siC95 = :c_siC95,c_siC96 = :c_siC96,c_siC97 = :c_siC97, ' +
    'c_siC98 = :c_siC98,c_siC99 = :c_siC99,c_siC100 = :c_siC100,c_siC101 = :c_siC101,c_siC102 = :c_siC102, ' +
    'c_siC103 = :c_siC103,c_siC104 = :c_siC104,c_siC105 = :c_siC105,c_siC106 = :c_siC106,c_siC107 = :c_siC107, ' +
    'c_siC108 = :c_siC108,c_siC109 = :c_siC109,c_siC110 = :c_siC110,c_siC111 = :c_siC111,c_siC112 = :c_siC112, ' +
    'c_siC113 = :c_siC113,c_siC114 = :c_siC114,c_siC115 = :c_siC115,c_siC116 = :c_siC116,c_siC117 = :c_siC117, ' +
    'c_siC118 = :c_siC118,c_siC119 = :c_siC119,c_siC120 = :c_siC120,c_siC121 = :c_siC121,c_siC122 = :c_siC122, ' +
    'c_siC123 = :c_siC123,c_siC124 = :c_siC124,c_siC125 = :c_siC125,c_siC126 = :c_siC126,c_siC127 = :c_siC127, ' +
    'c_siC128 = :c_siC128 ' +
    'where c_sirocut_id = :c_sirocut_id';
  //..........................................................................
  cUpdateProdGrpSiroUncutFields =
    'update t_dw_siroUncut set ' +
    'c_siU1 = :c_siU1,c_siU2 = :c_siU2,c_siU3 = :c_siU3,c_siU4 = :c_siU4,c_siU5 = :c_siU5,c_siU6 = :c_siU6, ' +
    'c_siU7 = :c_siU7,c_siU8 = :c_siU8,c_siU9 = :c_siU9,c_siU10 = :c_siU10,c_siU11 = :c_siU11,c_siU12 = :c_siU12, ' +
    'c_siU13 = :c_siU13,c_siU14 = :c_siU14,c_siU15 = :c_siU15,c_siU16 = :c_siU16,c_siU17 = :c_siU17, ' +
    'c_siU18 = :c_siU18,c_siU19 = :c_siU19,c_siU20 = :c_siU20,c_siU21 = :c_siU21,c_siU22 = :c_siU22, ' +
    'c_siU23 = :c_siU23,c_siU24 = :c_siU24,c_siU25 = :c_siU25,c_siU26 = :c_siU26,c_siU27 = :c_siU27, ' +
    'c_siU28 = :c_siU28,c_siU29 = :c_siU29,c_siU30 = :c_siU30,c_siU31 = :c_siU31,c_siU32 = :c_siU32, ' +
    'c_siU33 = :c_siU33,c_siU34 = :c_siU34,c_siU35 = :c_siU35,c_siU36 = :c_siU36,c_siU37 = :c_siU37, ' +
    'c_siU38 = :c_siU38,c_siU39 = :c_siU39,c_siU40 = :c_siU40,c_siU41 = :c_siU41,c_siU42 = :c_siU42, ' +
    'c_siU43 = :c_siU43,c_siU44 = :c_siU44,c_siU45 = :c_siU45,c_siU46 = :c_siU46,c_siU47 = :c_siU47, ' +
    'c_siU48 = :c_siU48,c_siU49 = :c_siU49,c_siU50 = :c_siU50,c_siU51 = :c_siU51,c_siU52 = :c_siU52, ' +
    'c_siU53 = :c_siU53,c_siU54 = :c_siU54,c_siU55 = :c_siU55,c_siU56 = :c_siU56,c_siU57 = :c_siU57, ' +
    'c_siU58 = :c_siU58,c_siU59 = :c_siU59,c_siU60 = :c_siU60,c_siU61 = :c_siU61,c_siU62 = :c_siU62, ' +
    'c_siU63 = :c_siU63,c_siU64 = :c_siU64,c_siU65 = :c_siU65,c_siU66 = :c_siU66,c_siU67 = :c_siU67, ' +
    'c_siU68 = :c_siU68,c_siU69 = :c_siU69,c_siU70 = :c_siU70,c_siU71 = :c_siU71,c_siU72 = :c_siU72, ' +
    'c_siU73 = :c_siU73,c_siU74 = :c_siU74,c_siU75 = :c_siU75,c_siU76 = :c_siU76,c_siU77 = :c_siU77, ' +
    'c_siU78 = :c_siU78,c_siU79 = :c_siU79,c_siU80 = :c_siU80,c_siU81 = :c_siU81,c_siU82 = :c_siU82, ' +
    'c_siU83 = :c_siU83,c_siU84 = :c_siU84,c_siU85 = :c_siU85,c_siU86 = :c_siU86,c_siU87 = :c_siU87, ' +
    'c_siU88 = :c_siU88,c_siU89 = :c_siU89,c_siU90 = :c_siU90,c_siU91 = :c_siU91,c_siU92 = :c_siU92, ' +
    'c_siU93 = :c_siU93,c_siU94 = :c_siU94,c_siU95 = :c_siU95,c_siU96 = :c_siU96,c_siU97 = :c_siU97, ' +
    'c_siU98 = :c_siU98,c_siU99 = :c_siU99,c_siU100 = :c_siU100,c_siU101 = :c_siU101,c_siU102 = :c_siU102, ' +
    'c_siU103 = :c_siU103,c_siU104 = :c_siU104,c_siU105 = :c_siU105,c_siU106 = :c_siU106,c_siU107 = :c_siU107, ' +
    'c_siU108 = :c_siU108,c_siU109 = :c_siU109,c_siU110 = :c_siU110,c_siU111 = :c_siU111,c_siU112 = :c_siU112, ' +
    'c_siU113 = :c_siU113,c_siU114 = :c_siU114,c_siU115 = :c_siU115,c_siU116 = :c_siU116,c_siU117 = :c_siU117, ' +
    'c_siU118 = :c_siU118,c_siU119 = :c_siU119,c_siU120 = :c_siU120,c_siU121 = :c_siU121,c_siU122 = :c_siU122, ' +
    'c_siU123 = :c_siU123,c_siU124 = :c_siU124,c_siU125 = :c_siU125,c_siU126 = :c_siU126,c_siU127 = :c_siU127, ' +
    'c_siU128 = :c_siU128 ' +
    'where c_sirouncut_id = :c_sirouncut_id';

//  cInsertProdGrpSiroCutUncutFields =
//    'insert t_dw_siroCutUncut ( ' +
//    'c_sirocutuncut_id,' +
//    'c_siC1,c_siC2,c_siC3,c_siC4,c_siC5,c_siC6,c_siC7,c_siC8,c_siC9,c_siC10,' +
//    'c_siC11,c_siC12,c_siC13,c_siC14,c_siC15,c_siC16,c_siC17,c_siC18,c_siC19,' +
//    'c_siC20,c_siC21,c_siC22,c_siC23,c_siC24,c_siC25,c_siC26,c_siC27,c_siC28,' +
//    'c_siC29,c_siC30,c_siC31,c_siC32,c_siC33,c_siC34,c_siC35,c_siC36,c_siC37,' +
//    'c_siC38,c_siC39,c_siC40,c_siC41,c_siC42,c_siC43,c_siC44,c_siC45,c_siC46,' +
//    'c_siC47,c_siC48,c_siC49,c_siC50,c_siC51,c_siC52,c_siC53,c_siC54,c_siC55,' +
//    'c_siC56,c_siC57,c_siC58,c_siC59,c_siC60,c_siC61,c_siC62,c_siC63,c_siC64,' +
//    'c_siU1,c_siU2,c_siU3,c_siU4,c_siU5,c_siU6,c_siU7,c_siU8,c_siU9,c_siU10,' +
//    'c_siU11,c_siU12,c_siU13,c_siU14,c_siU15,c_siU16,c_siU17,c_siU18,c_siU19,' +
//    'c_siU20,c_siU21,c_siU22,c_siU23,c_siU24,c_siU25,c_siU26,c_siU27,c_siU28,' +
//    'c_siU29,c_siU30,c_siU31,c_siU32,c_siU33,c_siU34,c_siU35,c_siU36,c_siU37,' +
//    'c_siU38,c_siU39,c_siU40,c_siU41,c_siU42,c_siU43,c_siU44,c_siU45,c_siU46,' +
//    'c_siU47,c_siU48,c_siU49,c_siU50,c_siU51,c_siU52,c_siU53,c_siU54,c_siU55,' +
//    'c_siU56,c_siU57,c_siU58,c_siU59,c_siU60,c_siU61,c_siU62,c_siU63,c_siU64 ) ' +
//    'values ( ' +
//    ':c_sirocutuncut_id, ' +
//    ':c_siC1,:c_siC2,:c_siC3,:c_siC4,:c_siC5,:c_siC6,:c_siC7,:c_siC8,:c_siC9,:c_siC10,' +
//    ':c_siC11,:c_siC12,:c_siC13,:c_siC14,:c_siC15,:c_siC16,:c_siC17,:c_siC18,:c_siC19,' +
//    ':c_siC20,:c_siC21,:c_siC22,:c_siC23,:c_siC24,:c_siC25,:c_siC26,:c_siC27,:c_siC28,' +
//    ':c_siC29,:c_siC30,:c_siC31,:c_siC32,:c_siC33,:c_siC34,:c_siC35,:c_siC36,:c_siC37,' +
//    ':c_siC38,:c_siC39,:c_siC40,:c_siC41,:c_siC42,:c_siC43,:c_siC44,:c_siC45,:c_siC46,' +
//    ':c_siC47,:c_siC48,:c_siC49,:c_siC50,:c_siC51,:c_siC52,:c_siC53,:c_siC54,:c_siC55,' +
//    ':c_siC56,:c_siC57,:c_siC58,:c_siC59,:c_siC60,:c_siC61,:c_siC62,:c_siC63,:c_siC64,' +
//    ':c_siU1,:c_siU2,:c_siU3,:c_siU4,:c_siU5,:c_siU6,:c_siU7,:c_siU8,:c_siU9,:c_siU10,' +
//    ':c_siU11,:c_siU12,:c_siU13,:c_siU14,:c_siU15,:c_siU16,:c_siU17,:c_siU18,:c_siU19,' +
//    ':c_siU20,:c_siU21,:c_siU22,:c_siU23,:c_siU24,:c_siU25,:c_siU26,:c_siU27,:c_siU28,' +
//    ':c_siU29,:c_siU30,:c_siU31,:c_siU32,:c_siU33,:c_siU34,:c_siU35,:c_siU36,:c_siU37,' +
//    ':c_siU38,:c_siU39,:c_siU40,:c_siU41,:c_siU42,:c_siU43,:c_siU44,:c_siU45,:c_siU46,' +
//    ':c_siU47,:c_siU48,:c_siU49,:c_siU50,:c_siU51,:c_siU52,:c_siU53,:c_siU54,:c_siU55,' +
//    ':c_siU56,:c_siU57,:c_siU58,:c_siU59,:c_siU60,:c_siU61,:c_siU62,:c_siU63,:c_siU64 )';
//
////..............................................................................
//  cUpdateProdGrpSiroCutUncutFields =
//    'update t_dw_siroCutUncut set ' +
//    'c_siC1 = :c_siC1,c_siC2 = :c_siC2,c_siC3 = :c_siC3,c_siC4 = :c_siC4,c_siC5 = :c_siC5,c_siC6 = :c_siC6, ' +
//    'c_siC7 = :c_siC7,c_siC8 = :c_siC8,c_siC9 = :c_siC9,c_siC10 = :c_siC10,c_siC11 = :c_siC11,c_siC12 = :c_siC12, ' +
//    'c_siC13 = :c_siC13,c_siC14 = :c_siC14,c_siC15 = :c_siC15,c_siC16 = :c_siC16,c_siC17 = :c_siC17, ' +
//    'c_siC18 = :c_siC18,c_siC19 = :c_siC19,c_siC20 = :c_siC20,c_siC21 = :c_siC21,c_siC22 = :c_siC22, ' +
//    'c_siC23 = :c_siC23,c_siC24 = :c_siC24,c_siC25 = :c_siC25,c_siC26 = :c_siC26,c_siC27 = :c_siC27, ' +
//    'c_siC28 = :c_siC28,c_siC29 = :c_siC29,c_siC30 = :c_siC30,c_siC31 = :c_siC31,c_siC32 = :c_siC32, ' +
//    'c_siC33 = :c_siC33,c_siC34 = :c_siC34,c_siC35 = :c_siC35,c_siC36 = :c_siC36,c_siC37 = :c_siC37, ' +
//    'c_siC38 = :c_siC38,c_siC39 = :c_siC39,c_siC40 = :c_siC40,c_siC41 = :c_siC41,c_siC42 = :c_siC42, ' +
//    'c_siC43 = :c_siC43,c_siC44 = :c_siC44,c_siC45 = :c_siC45,c_siC46 = :c_siC46,c_siC47 = :c_siC47, ' +
//    'c_siC48 = :c_siC48,c_siC49 = :c_siC49,c_siC50 = :c_siC50,c_siC51 = :c_siC51,c_siC52 = :c_siC52, ' +
//    'c_siC53 = :c_siC53,c_siC54 = :c_siC54,c_siC55 = :c_siC55,c_siC56 = :c_siC56,c_siC57 = :c_siC57, ' +
//    'c_siC58 = :c_siC58,c_siC59 = :c_siC59,c_siC60 = :c_siC60,c_siC61 = :c_siC61,c_siC62 = :c_siC62, ' +
//    'c_siC63 = :c_siC63,c_siC64 = :c_siC64, ' +
//    'c_siU1 = :c_siU1,c_siU2 = :c_siU2,c_siU3 = :c_siU3,c_siU4 = :c_siU4,c_siU5 = :c_siU5,c_siU6 = :c_siU6, ' +
//    'c_siU7 = :c_siU7,c_siU8 = :c_siU8,c_siU9 = :c_siU9,c_siU10 = :c_siU10,c_siU11 = :c_siU11,c_siU12 = :c_siU12, ' +
//    'c_siU13 = :c_siU13,c_siU14 = :c_siU14,c_siU15 = :c_siU15,c_siU16 = :c_siU16,c_siU17 = :c_siU17, ' +
//    'c_siU18 = :c_siU18,c_siU19 = :c_siU19,c_siU20 = :c_siU20,c_siU21 = :c_siU21,c_siU22 = :c_siU22, ' +
//    'c_siU23 = :c_siU23,c_siU24 = :c_siU24,c_siU25 = :c_siU25,c_siU26 = :c_siU26,c_siU27 = :c_siU27, ' +
//    'c_siU28 = :c_siU28,c_siU29 = :c_siU29,c_siU30 = :c_siU30,c_siU31 = :c_siU31,c_siU32 = :c_siU32, ' +
//    'c_siU33 = :c_siU33,c_siU34 = :c_siU34,c_siU35 = :c_siU35,c_siU36 = :c_siU36,c_siU37 = :c_siU37, ' +
//    'c_siU38 = :c_siU38,c_siU39 = :c_siU39,c_siU40 = :c_siU40,c_siU41 = :c_siU41,c_siU42 = :c_siU42, ' +
//    'c_siU43 = :c_siU43,c_siU44 = :c_siU44,c_siU45 = :c_siU45,c_siU46 = :c_siU46,c_siU47 = :c_siU47, ' +
//    'c_siU48 = :c_siU48,c_siU49 = :c_siU49,c_siU50 = :c_siU50,c_siU51 = :c_siU51,c_siU52 = :c_siU52, ' +
//    'c_siU53 = :c_siU53,c_siU54 = :c_siU54,c_siU55 = :c_siU55,c_siU56 = :c_siU56,c_siU57 = :c_siU57, ' +
//    'c_siU58 = :c_siU58,c_siU59 = :c_siU59,c_siU60 = :c_siU60,c_siU61 = :c_siU61,c_siU62 = :c_siU62, ' +
//    'c_siU63 = :c_siU63,c_siU64 = :c_siU64 ' +
//    'where c_sirocutuncut_id = :c_sirocutuncut_id';
//
//..............................................................................
  cDelShiftByTimeQuery =
    'delete from t_dw_prodgroup_shift where c_fragshift_start < :c_shift_start';

//..............................................................................
  cGetFragInfo =
    'select c_fragshift_start from t_fragshift where c_fragshift_id = :c_fragshift_id';

//..............................................................................
  cGetAvailableFragShiftData =
    'select * from t_dw_prodgroup_shift ps ' +
    'where ps.c_prod_id = :c_prod_id ' +
    'and   ps.c_fragshift_id = :c_fragshift_id';

//..............................................................................
  cGetClassCutData    = 'select * from t_dw_Classcut where c_classcut_id = :c_classcut_id';
  cGetClassUnCutData  = 'select * from t_dw_classuncut where c_classuncut_id = :c_classuncut_id';
//..............................................................................
  cGetSpliceCutData   = 'select * from t_dw_splicecut where c_splicecut_id = :c_splicecut_id';
  cGetSpliceUnCutData = 'select * from t_dw_spliceuncut where c_spliceuncut_id = :c_spliceuncut_id';

//..............................................................................
  cGetSiroCutData     = 'select * from t_dw_sirocut where c_sirocut_id = :c_sirocut_id';
  cGetSiroUnCutData   = 'select * from t_dw_sirouncut where c_sirouncut_id = :c_sirouncut_id';

//..............................................................................
//  cGetSiroCutUnCutData =
//    'select * from t_dw_sirocutuncut where c_sirocutuncut_id = :c_sirocutuncut_id';

//..............................................................................
  cInitTimerange =
    'select max ( c_fragshift_start ) c_fragshift_start from t_fragshift';

//..............................................................................
  cGetShiftsInTimerange =

  'select distinct c_shift_start from t_shift ' +
    'where c_shift_start > :LastFragshiftStart and ' +
    'c_shift_start <= :ActTime';

//..............................................................................
  cUpdateFrag =

  'update t_fragshift set c_fragshift_length = :c_fragshift_length ' +
    'where c_fragshift_start=:c_fragshift_start';

//..............................................................................
  cInsertFrag =

  'insert t_fragshift ( c_fragshift_id, c_fragshift_start, c_fragshift_length ) ' +
    'values ( ' +
    ':c_fragshift_id, :c_fragshift_start, 0 )';

//..............................................................................
  cGetShiftsAtTime =

  'select c_shift_id, c_shiftcal_id from t_shift ' +
    'where c_shift_start <= :ActShift and ' +
    'dateadd ( minute, c_shift_length, c_shift_start ) > :ActShift';

//..............................................................................
  cInsertShiftFragshift =

  'insert t_shift_fragshift ( c_fragshift_id, c_shift_id, c_shiftcal_id ) ' +
    'values ( ' +
    ':c_fragshift_id, :c_shift_id, :c_shiftcal_id )';

//..............................................................................
  cProdGrpStartCheck =

  'select c_prod_id from t_prodgroup p, t_interval i ' +
    'where p.c_prod_id = :c_prod_id and ' +
    'i.c_interval_id = :c_interval_id and ' +
    'p.c_prod_start >= i.c_interval_start';

//..............................................................................
  cProdGrpProducedCheck =

  'select c_prod_id, sum ( c_len ) len from t_spindle_interval_data ' +
    'where c_prod_id = :c_prod_id and ' +
    'c_interval_id = :c_interval_id ' +
    'group by c_prod_id';

//..............................................................................
  cGetIntervalLen =

  'select datediff ( minute, c_interval_start, c_interval_end ) interval_len from t_interval ' +
    'where c_interval_id = :c_interval_id';

//..............................................................................
  cGetAverageIntervalLen =

  'select id.c_interval_id, sum ( c_len ) len from t_spindle_interval_data id, t_interval i ' +
    'where id.c_interval_id = i.c_interval_id and ' +
    'id.c_prod_id = :c_prod_id and ' +
    'i.c_interval_start >= :start_range and ' +
    'i.c_interval_start < :end_range ' +
    'group by id.c_interval_id';

//..............................................................................
  cGetMaOffliSpindleData =
  { DONE : c_tLst Wert sind noch Sekungen -> Minuten direkt im Query? }
  'select c_spindle_id, sum(c_YB) c_YB , ' + // sum ( c_Red ) c_Red, ' +
//  '(( sum ( c_rtSpd ) * 100 ) / sum ( c_otSpd )) Eff, sum ( c_CSp ) c_CSp, ' +
  'sum(c_rtSpd) c_rtSpd, sum(c_otSpd) c_otSpd, sum(c_CSp) c_CSp, ' +
  'sum(c_RSp) c_RSp, sum(c_CBu) c_CBu, sum(c_CUpY) c_CUpY, ' +
 (*@@@@*)'sum(c_LStProd) c_tLSt, sum(c_LSt) c_LSt, sum(c_CSIRO) c_CSIRO, ' +
  'sum(c_CYTot) c_CYTot, sum(c_Len) c_Len ' +
  'from t_spindle_interval_data ' +
  'where c_prod_id = :c_prod_id and ' +
  'c_interval_id = :c_interval_id ' +
  'group by c_spindle_id';

//..............................................................................
  cQrySelectSpindlesInMachineOfflimit =
    'select c_spindle_id from t_machine_offlimit ' +
    'where c_machine_id = :c_machine_id ' +
    'and c_spindle_id >= :spindle_first ' +
    'and c_spindle_id <= :spindle_last ';

//  cCheckSpindleOfflimitRecords =
//    'select c_spindle_id from t_spindle ' +
//    'where c_machine_id = :c_machine_id and ' +
//    'c_spindle_id >= :spindle_first and ' +
//    'c_spindle_id <= :spindle_last and ' +
//    'c_spindle_id not in ( ' +
//    'select c_spindle_id from t_machine_offlimit ' +
//    'where c_machine_id = :c_machine_id and ' +
//    'c_spindle_id >= :spindle_first and ' +
//    'c_spindle_id <= :spindle_last )';

//..............................................................................
  cCheckOfflimitSettings =

  'select mos.c_maoffset_id ' +
    'from  t_machine m, t_machine_offlimit_settings mos ' +
    'where m.c_machine_id = :c_machine_id ' +
    'and m.c_maoffset_id = mos.c_maoffset_id';

//..............................................................................
  cDelMachOfflimitsAverUnreferenced = //Nue:20.3.02

  'delete t_machine_offlimit_average ' +
    'where c_maoffaverage_id not in (select c_maoffaverage_id from t_machine_offlimit)';

//..............................................................................
  cGetExpDataEventList =

  'select c_dataevent_start, c_interval_id, c_fragshift_id ' +
    'from v_DataEvents ' +
    'where c_dataevent_start <= :ActDataEvent ' +
    'and   c_dataevent_start >  :LastOKDataEvent ' +
    'order by c_dataevent_start desc';

//..............................................................................
  cUnDoGetZESpdData =

  'delete t_spindle_interval_data ' +
    'from t_spindle_interval_data t_id, t_interval t_i, t_fragshift t_fs ' +
    'where c_prod_id = :c_prod_id ' +
    'and   t_i.c_interval_id = t_id.c_interval_id ' +
    'and   t_id.c_fragshift_id = t_fs.c_fragshift_id ' +
    'and ( ( t_i.c_interval_start >= ( select c_interval_start from t_interval where c_interval_id = :c_interval_id )  ) ' +
    '   or ( t_fs.c_fragshift_start  >= ( select c_fragshift_start from t_fragshift where c_fragshift_id = :c_fragshift_id ) ) )';

//..............................................................................
  cUpdateProdGrpStateTableUndo =

  'update t_prodgroup_state ' +
    'set c_fragshift_id_ok = c_old_fragshift_id_ok, ' +
    'c_int_id_ok = c_old_int_id_ok, ' +
    'c_data_ok = 1 ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateProdgroupStateIntervalData =

  'update t_prodgroup_state set c_fragshift_id_ok = :c_fragshift_id_ok, ' +
    'c_int_id_ok = :c_int_id_ok, ' +
    'c_data_ok = 0 ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateProdGrpState =

  'update t_prodgroup_state ' +
    'set c_shift_fragshift_id_ok = :c_shift_fragshift_id_ok, ' +
    'c_shift_int_id_ok = :c_shift_int_id_ok ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cFillUpFragshifts =

  'select c_fragshift_id, c_fragshift_start ' +
    'from t_fragshift ' +
    'where c_fragshift_start >= ' +
    '(select c_fragshift_start from t_fragshift where c_fragshift_id = :Start_fragshift_id ) ' +
    'and c_fragshift_start <= ' +
    '(select c_fragshift_start from t_fragshift where c_fragshift_id = :End_fragshift_id ) ' +
    'order by c_fragshift_start';

//..............................................................................
  cGetIntervalsInFragshift =

  'select distinct t_id.c_interval_id, t_i.c_interval_start ' +
    'from t_spindle_interval_data t_id, t_interval t_i ' +
    'where t_i.c_interval_id = t_id.c_interval_id ' +
    'and   t_id.c_fragshift_id = :c_fragshift_id ' +
    'and   t_id.c_prod_id = :c_prod_id ' +
    'and   t_i.c_interval_start > ' +
    '(select c_interval_start from t_interval where c_interval_id = :Start_interval_id) ' +
    'and   t_i.c_interval_start <= ' +
    '(select c_interval_start from t_interval where c_interval_id = :End_interval_id) ' +
    'order by t_i.c_interval_start';

//..............................................................................
  cUpdateProdGrpYarnCnt =
    'update t_prodgroup ' +
    'set c_act_yarncnt = :c_act_yarncnt ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................

  cUpdateProdGrpThreadCnt =
    'update t_prodgroup ' +
    'set c_nr_of_threads = :c_nr_of_threads ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateProdGrpSlip =
    'update t_prodgroup ' +
    'set c_slip = :c_slip ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateSpindleDataLen =
    'update t_spindle_interval_data ' +
//    'set c_len = c_len * :factor ' +
    'set c_len =((CAST(c_len as BIGINT) * :slipnew) / :slipold) ' +   //Nue:22.11.07
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateShiftDataLen =
    'update t_dw_prodgroup_shift ' +
//    'set c_len = c_len * :factor ' +
    'set c_len =((CAST(c_len as BIGINT) * :slipnew) / :slipold) ' +  //Nue:22.11.07
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateSpindleDataWeightByLen =
    'update t_spindle_interval_data ' +
    'set c_wei = c_len * :factor ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateShiftDataWeightByLen =
    'update t_dw_prodgroup_shift ' +
    'set c_wei = c_len * :factor ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateSpindleDataWeightByFactor =
    'update t_spindle_interval_data ' +
    'set c_wei = c_wei * :factor ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateShiftDataWeightByFactor =
    'update t_dw_prodgroup_shift ' +
    'set c_wei = c_wei * :factor ' +
    'where c_prod_id = :c_prod_id';

//..............................................................................
  cUpdateProdGrpColor =
    'UPDATE t_prodgroup_state ' +
    'SET c_color=:c_color ' +
    'WHERE c_prod_id=:c_prod_id ';

//..............................................................................
  cUpdateProdGrpName =
    'UPDATE t_prodgroup ' +
    'SET c_prod_name=:c_prod_name ' +
    'WHERE c_prod_id=:c_prod_id ';

//..............................................................................
  cDBDump =
    'BACKUP DATABASE [%s]' +
    'TO  DISK = N''%s%s.bak'' WITH  INIT , ' +
    'NOUNLOAD ,  NAME = N''%s backup'',  NOSKIP ,  STATS = 10,  NOFORMAT';

//..............................................................................
{alt
 cLOGDump =
 'BACKUP LOG [%s] TO  DISK = N''%s%s.bak'' WITH  NOINIT , ' +
 'NOUNLOAD ,  NAME = N''%s backup'',  SKIP ,  STATS = 10,  NOFORMAT';
{}
// No further used 18.12.02 Nue
////neu Nue..............................................................................
//  cLOGDump =
//    'BACKUP LOG [%s] TO  DISK = N''%s%s.bak'' WITH  INIT , ' +
//    'NOUNLOAD ,  NAME = N''%s backup'',  NOSKIP ,  STATS = 10,  NOFORMAT';
//
//..............................................................................
  cDumpTransactionNoLog = 'Dump tran %s with no_log';
// GL 16-02-2016
  cDumpTransactionNew = 'DBCC SHRINKFILE (MM_WInding_log, 1)';
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

