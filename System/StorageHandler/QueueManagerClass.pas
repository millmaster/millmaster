(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: QueueManagerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 15.09.1998  0.00  Wss | Standardumgebung fuer Entwicklung erstellt
| 02.10.1998  0.01  Mg  | Implementation von ProcessJob
| 26.10.1998  1.00  Wss | Ableitung auf Baseklasse umgestellt
| 21.04.1999  1.01  Mg  | WriteJobFailedToJobController in ProcessJob eingefuegt
| 24.06.1999  1.02  Mg  | Umbau : alle StoreX verwenden neu den IPC-Kanal vom ThreadLauncher
| 22.07.1999  1.10  Mg  | Umbau auf JobQueue
| 28.11.2000  1.11  khp | jtGenQOfflimit inserted
| 22.03.2004  1.11  Wss | Anpassungen an neue IPC mit dynamischem JobBuffer
|=========================================================================================*)
unit QueueManagerClass;

interface

uses
  Classes, SysUtils, Windows,
  BaseThread, MMBaseClasses, DataPoolClass, mmEventLog, BaseGlobal,
  IPCClass, JobQueueClass;

type
  TQueueManager = class(TBaseQueueManager)
  private
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aJobQueue : TJobQueue ); override;
    destructor Destroy; override;
    function Init: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//-----------------------------------------------------------------------------
constructor TQueueManager.Create( aThreadDef: TThreadDef; aJobQueue : TJobQueue );
begin
  inherited Create( aThreadDef, aJobQueue );
end;
//-----------------------------------------------------------------------------
destructor TQueueManager.Destroy;
begin
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TQueueManager.Init: Boolean;
begin
  //mAccessGroup := 'Users'; // provisorisch zum debuggen
  Result := inherited Init;
  if Result then begin
  end;
end;
//-----------------------------------------------------------------------------
procedure TQueueManager.ProcessJob;
  //...........................................................
  procedure ProcessError;
  begin
    mJob.JobTyp := jtJobFailed;
    WriteJobBufferTo(ttJobController);
    WriteLog(etError, 'TQueueManager.ProcessJob putJob failed. ' + FormatMMErrorText(mJobQueue.QueueError));
  end;
  //...........................................................
begin
  try
    case mJob.JobTyp of
      // Jobs getting low priority
      jtTest,
      jtDelInt,
      jtProdGrpUpdate,
      jtOEEventJobID,
      jtGenDWData,
      jtGenExpDWData,
      jtGenSpdOfflimits,
      jtDelShiftByTime,
      jtGenQOfflimit,
      jtDBDump           :
        if not mJobQueue.putJob(mJob, qpLow) then
          ProcessError;
      // Jobs getting Normal priority
      jtSendEventToClients,
      jtGenShiftData,
      jtGenExpShiftData,
      jtUnDoGetZESpdData      :
        if not mJobQueue.putJob ( mJob, qpNormal ) then
          ProcessError;
      // Jobs getting High priority
      jtGetZESpdData,
      jtGetExpZESpdData,
      jtGetZEGrpData          :
        if not mJobQueue.putJob ( mJob, qpHigh ) then
          ProcessError;
      // Jobs getting Express priority
      jtGetDataStopZESpd,
      jtGetExpDataStopZESpd,
      jtSendMsgToAppl,
      jtGetSettingsAllGroups,
      jtSaveMaConfigToDB, //Nue:29.11.04
      jtDiagnostic            :  begin
        if not mJobQueue.putJob ( mJob, qpExpress ) then
          ProcessError;
        end;
    else
      // call inherited ProcessJob for unknown or common JobTyp
      inherited ProcessJob;
    end;
  except
    on e:Exception  do begin
      mJob.JobTyp := jtJobFailed;
      WriteJobBufferTo ( ttJobController );
      WriteLog ( etError, 'TQueuManager.ProcessJob failed : ' + e.Message );
    end;
  end;
end;
//-----------------------------------------------------------------------------
 procedure TQueueManager.ProcessInitJob;
 begin
   ProcessJob;
 end;
//-----------------------------------------------------------------------------
begin
end.

