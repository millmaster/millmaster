(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataPoolWriterClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 15.09.1998  0.00  Wss | Standardumgebung fuer Entwicklung erstellt
| 26.10.1998  1.00  Wss | Ableitung auf Baseklasse umgestellt
| 28.04.1999  1.01  Mg  | try except in processJob inserted.
| 06.06.2000  1.02  Mg  | New Priority = tpHighest
| 22.03.2004  1.02  Wss | Anpassungen an neue IPC mit dynamischem JobBuffer
|=========================================================================================*)
unit DataPoolWriterClass;

interface

uses
  DataPoolClass, mmEventLog, BaseGlobal, LoepfeGlobal,
  sysutils, Windows, MMBaseClasses;

type
  TDataPoolWriter = class(TBaseDataPoolWriter)
  private
    procedure CleanUp;
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
  public
    function Init: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  Classes;
//-----------------------------------------------------------------------------
function TDataPoolWriter.Init: Boolean;
begin
  Result := inherited Init;
  if Result then begin
    Priority := tpHighest;
  end;
end;
//-----------------------------------------------------------------------------
procedure TDataPoolWriter.CleanUp;
var
  xJobIDs: TJobIDList;
  xNumJobs: Word;
  i, j: integer;
  xActPoolJobID: Word;
  xFound: boolean;
begin
  if mDataPool.GetAllJobIDs(xJobIDs, xNumJobs) then begin
    // outer loop counts through JobID from DataPool
    for i:=0 to xNumJobs-1 do begin
      xFound        := False;
      xActPoolJobID := xJobIDs[i].JobID;
      // inner loop counts through valid JobID from MsgHandler
      for j:=0 to mJob.CleanUpEv.NumOfJobs-1 do begin
        if xActPoolJobID = mJob.CleanUpEv.JobIDList[j].JobID then begin
          xFound := True;
          break;
        end;
      end;
      // if DataPool-JobID not found in valid list from MsgHandler -> delete this JobID
      if not xFound then
        if not mDataPool.DelJobID(xActPoolJobID) then begin
          fError := SetError(mDataPool.ListError, etNTError, 'Delete of job failed.');
          WriteLog(etWarning, FormatMMErrorText(Error));
          break;
        end;
    end;
  end
  else begin
    fError := SetError(mDataPool.ListError, etNTError, 'Access to DataPool failed.');
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;
//-----------------------------------------------------------------------------
procedure TDataPoolWriter.ProcessJob;
begin
  case mJob.JobTyp of
    jtCleanUpEv: CleanUp;
    jtDelJobID: mDataPool.DelJobID(mJob.JobID);
  else try
      if not mDataPool.Add(mJob) then
        WriteLog(etError, 'Add failed to DataPool. ' + FormatErrorText(mDataPool.ListError));
    except
      on e: Exception do begin
        WriteLog(etError, 'Add failed to DataPool -> restart. JobTyp=' + GetJobName(mJob.JobTyp) + '. ' + e.Message);
        WriteToMain(gcRestartMM, ERROR_INVALID_FUNCTION);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TDataPoolWriter.ProcessInitJob;
begin
  ProcessJob;
end;
//-----------------------------------------------------------------------------
end.

