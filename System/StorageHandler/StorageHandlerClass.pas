(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: StorageHandlerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 15.09.1998  0.00  Wss | Standardumgebung fuer Entwicklung erstellt
| 21.07.1999  1.00  Mg  | Umbau auf JobQueue
| 27.09.1999  1.01  Mg  | initialisation of com library inserted
| 15.11.1999  1.02  Mg  | getParamHandle inserted
| 05.12.2000  1.03  Mg  | Initialisation von Com Library ausgeklammert
| 05.10.2002        LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 19.11.2002  1.10  khp | TStorageHandler.Initialize: Message to Protocoll if SP usp_longterm_week available
| 05.02.2003  1.11  khp | Check mit MMSettings.IsComponentLongterm ob Longtermpackage installiert ist
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|=========================================================================================*)
unit StorageHandlerClass;

interface

uses
  Classes,  comobj,
  BaseMain, DataPoolClass, DataPoolWriterClass, QueueManagerClass,BaseGlobal,
  mmThread, LoepfeGlobal, JobQueueClass, WorkerClass, MMBaseClasses,mmEventLog,
  SettingsReader,
  ADODBAccess, Sysutils;

type
  TStorageHandler = class(TBaseMain)
  private
    mDataPool : TDataPool;
    mJobQueue : TJobQueue;
    mJobCtrlPort : TJobCtrlPort;
  protected
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; override;
    function ConnectThreads: Boolean; override;
    function getParamHandle:TMMSettingsReader; override;
  public
    constructor Create(aSubSystem: TSubSystemTyp); override;
    destructor Destroy; override;
    function Initialize: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
 ActiveX,Windows;
//-----------------------------------------------------------------------------
constructor TStorageHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
  mDataPool := TDataPool.Create;
  mJobQueue := TJobQueue.Create;
  mJobCtrlPort := TJobCtrlPort.Create;
end;
//-----------------------------------------------------------------------------
destructor TStorageHandler.Destroy;
begin
  mJobCtrlPort.Free;
  mJobQueue.Free;
  mDataPool.Free;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TStorageHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttDataPoolWriter: begin
        aThread := TDataPoolWriter.Create(mSubSystemDef.Threads[1], mDataPool );
      end;
    ttQueueManager: begin
        aThread := TQueueManager.Create(mSubSystemDef.Threads[2], mJobQueue );
      end;
    ttExpressWorker : begin
        aThread := TWorker.Create ( mSubSystemDef.Threads[3], mJobQueue, mDataPool, mJobCtrlPort );
      end;
    ttWorker : begin
        aThread := TWorker.Create ( mSubSystemDef.Threads[5], mJobQueue, mDataPool, mJobCtrlPort );
      end;
  else
    aThread := Nil;
    Result := False;
  end;
end;
//-----------------------------------------------------------------------------
function TStorageHandler.ConnectThreads:boolean;
begin
  Result := inherited ConnectThreads;
  if Result then
    Result := mJobCtrlPort.Connect;
end;
//-----------------------------------------------------------------------------
function TStorageHandler.Initialize: Boolean;
var
  xtext:String;
  xFileVersion: string;

begin
  Result := true;
  xtext:='.';
  if Result then
    Result := inherited Initialize;
  if Result then
    Result := mJobQueue.Init;
  if Result then begin
    if MMSettings.IsComponentLongterm then
      xtext:=' and Longterm database is available.';
  // Message to Protocoll if SP usp_longterm_week available
    with TAdoDBAccess.Create(1) do
    try
      try
        init;
        Query[cPrimaryQuery].SQL.Text := 'select * from sysobjects where name=''usp_longterm_week'' and type like ''P''';
        Query[cPrimaryQuery].Open;
        if Query[cPrimaryQuery].Findfirst then
           xtext:=' and Longterm database is available.';
      except
       on e: Exception do
          WriteLog(etError, 'Select from sysobjects failed'+ e.Message);
      end;// try except
    finally
      free;
    end;// try finally
    xFileVersion := 'unknown';
    try
      xFileVersion := GetFileVersion(ParamStr(0));
    except
      // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
    end;
    WriteLog ( etInformation , Format('StorageHandler with fileversion %s started successful%s', [xFileVersion, xtext]));
  end;
end;

function TStorageHandler.getParamHandle:TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;
//------------------------------------------------------------------------------
end.

