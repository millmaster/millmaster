(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: StorageHandler.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Projektdatei des StorageHandlers
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 27.04.1999  1.00  Mg  | Exception and ErrorLog implemented
| 22.07.1999  1.10  Mg  | JobQueue inserted
| 05.10.2002        LOK | Umbau ADO
|=========================================================================================*)

program StorageHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
{$IFDEF MemCheck}
  MemCheck,
{$ENDIF}
  MMEventLog,
  SysUtils,
  BaseGlobal,
  LoepfeGlobal,
  BaseMain,
  ActiveX,
  mmCs,
  Windows,
  StorageHandlerClass in 'StorageHandlerClass.pas',
  DataPoolWriterClass in 'DataPoolWriterClass.pas',
  QueueManagerClass in 'QueueManagerClass.pas',
  JobEditors in 'JobEditors.pas',
  EditorQueries in 'EditorQueries.pas',
  EditorServiceClasses in 'EditorServiceClasses.pas',
  JobQueueClass in '..\..\Common\JobQueueClass.pas',
  WorkerClass in 'WorkerClass.pas',
  MsgGlobal in '..\..\Common\MsgGlobal.pas',
  MMBaseClasses in '..\..\Common\MMBaseClasses.pas';

var
  xStorageHandler : TStorageHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}
begin
{$IFDEF MemCheck}
  MemChk('StorageHandler');
{$ENDIF}

  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');

  try
    xStorageHandler := TStorageHandler.Create(ssStorageHandler);
    if xStorageHandler.Initialize then
    try
      xStorageHandler.Run;
    except
      on e:Exception do begin
        WriteToEventLog('StorageHandler terminated: ' + e.message, 'StorageHandler: ', etError, cEventLogClassForSubSystems, '', ssStorageHandler);
      end;
    end;
    xStorageHandler.Free;
  finally
    CoUninitialize;
  end;
end.

