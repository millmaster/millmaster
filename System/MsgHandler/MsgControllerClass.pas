(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MsgControllerClass.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 20.10.1998  1.00  Wss | Basic functionality implemented and ready to test
| 26.10.1998  1.01  Wss | Deriving changed to base classes
| 16.03.1999  1.02  Wss | Implementation completed
| 16.03.2000  1.02  Wss | jtGetSettingsAllGroups implemented
| 08.11.2001  1.02  Wss | jtGetZEReady implemented
| 10.05.2004  1.02  Wss | jtSetMapfile implemented
| 26.07.2005  1.02  Wss | ntLX Unterst�tzung
| 28.09.2006  1.03  Nue | cMaxGroupMemoryLimit und cLXSpdMemoryLimit behandelt.
|=========================================================================================*)
unit MsgControllerClass;

interface

uses
  SysUtils, Windows, YMParaDef,
  MMBaseClasses, MMEventLog, IPCClass, BaseGlobal;

type
  TMsgController = class(TBaseMsgController)
  private
  protected
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
    function RepeatMessage(aIndex: Integer; aSingleMessage: Boolean = True): Boolean; override;
  public
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  LoepfeGlobal, MsgListClass;

//-----------------------------------------------------------------------------
procedure TMsgController.ProcessInitJob;
begin
{$IFDEF MsgDebug}
  WriteLog(etError, 'MsgDebug for interval data tests is enabled!!!!')
{$ENDIF}
  ProcessJob;
end;
//-----------------------------------------------------------------------------
procedure TMsgController.ProcessJob;
var
//  xIndex: Integer;
  xStr: String;
  i: Integer;
  xDebugJobTyp: set of TJobTyp;
  xValue: Integer;
  xJob: PJobRec;
  xGrpLimit: Integer;
  xNetTyp: TNetTyp;
begin
  xStr := '';
  xDebugJobTyp := [];
  case mJob^.JobTyp of
//    jtGetSettings,
    jtClearZEGrpData,
    jtClearZESpdData,
    jtDelZEGrpData,
    jtGetDataStopZEGrp,
    jtGetExpDataStopZEGrp,
    jtGetExpZEGrpData,
    jtGetNodeList,
    jtGetZEGrpData,
    jtGetZEReady,
    jtMaNotFound,
    jtSaveMaConfigToDB,
    jtSetNodeList,
//    jtSetProdID, wss 9.9.04: wieder entfernt da doch ein SetSettings Job ben�tigt wird
    jtSetSettings,
    jtStartGrpEv,
    jtStopGrpEv,
    jtViewZEGrpData: begin
        if AddJobToMsgList then begin
          WriteJobToNet;
        end;
      end;

    jtDelJobID: begin
//        CodeSite.SendFmtMsg('MC %d: DelJobID received', [mJob^.JobID]);
        for xNetTyp:=ntTXN to ntLX do begin
          if Params.HasNetTyp(xNetTyp) then begin
            mJob^.NetTyp := xNetTyp;
            WriteJobToNet;
          end;
        end;
        inherited ProcessJob;
      end;
    jtDiagnostic: begin
        if mMsgList.Count = 0 then
          WriteLog(etInformation, 'Diagnostic: no jobs in list')
        else
          for i:=0 to mMsgList.Count-1 do
            WriteLog(etInformation, Format('Diagnostic: JobID:%d, JobTyp:%s, Value:%d, RepeatCount:%d, Received:%d, Confirmed:%d',
                     [mMsgList.Items[i]^.JobID,
                      GetJobName(mMsgList.Items[i]^.Job^.JobTyp),
                      mMsgList.Items[i]^.Value,
                      mMsgList.Items[i]^.RepeatCount,
                      Integer(mMsgList.Items[i]^.Received),
                      Integer(mMsgList.Items[i]^.Confirmed)]));
      end;

    // request spindle data in delete mode
    jtGetDataStopZESpd,        // irregular spindle data aquisition at ProdGrp stop
    jtGetExpDataStopZESpd,     // regular spindle data aquisition at intervall end
    jtGetExpZESpdData,         // regular spindle data aquisition at ProdGrp stop
    jtGetZESpdData: begin      // irregular spindle data aquisition at shift end
        xStr := Format('GetZESpdData: %d/%d/%d/%d/%d/%d', [mJob^.GetZESpdData.MachineID,
                                            mJob^.GetZESpdData.SpindleFirst,
                                            mJob^.GetZESpdData.SpindleLast,
                                            mJob^.GetZESpdData.IntID,
//                                            Ord(mJob^.GetZESpdData.MachineTyp),
                                            mJob^.GetZESpdData.ProdID,
                                            mJob^.GetZESpdData.FragID]);
{$IFDEF MsgDebug}
        if (mJob^.GetZESpdData.MachineID = 1) then begin
          inc(gMachID1);
        end
        else if (mJob^.GetZESpdData.MachineID = 6) then begin
          inc(gMachID6);
        end;
{$ENDIF}

        // NetHandler has to know only this job type
        mJob^.JobTyp := jtGetZESpdData;
        if AddJobToMsgList(mJob^.GetZESpdData.SpindleFirst, mJob^.GetZESpdData.SpindleLast) then begin
          WriteJobToNet;
        end;
      end;
    // request spindle data in view mode
//    jtViewZESpdData: begin
//        // NetHandler has to know only this job type
//        mJob^.JobTyp := jtViewZESpdData;
//        if AddJobToMsgList(mJob^.GetZESpdData.SpindleFirst, mJob^.GetZESpdData.SpindleLast) then begin
//          WriteJobToNet;
//        end;
//      end;
    jtDelZESpdData: begin
        xStr := 'Mach = ' + IntToStr(mJob^.DelZESpdData.MachineID);
        if AddJobToMsgList then begin
          WriteJobToNet;
        end;
      end;
    // some other one message jobs
    jtGetMaAssign,
    jtMaEqualize: begin
        if AddJobToMsgList then begin
          // job has to be saved as original type
          mJob^.JobTyp := jtGetMaAssign;
          WriteJobToNet;
        end;
      end;
    //-----------------------------------------------------------
    jtGetSettingsAllGroups: begin
{$IFDEF MsgDebug}
  gMachID1 := 0; // Job wird immer beblockt gegen�ber JobHandler (mit 1x Wiederholung im MsgHandler)
  gMachID6 := 0; // Job wird 1x beblockt gegen�ber JobHandler (mit 1x Wiederholung im MsgHandler)
{$ENDIF}
        // get max group number
        case mJob^.NetTyp of
          ntTXN: xGrpLimit := cZESpdGroupLimit;
          ntWSC: xGrpLimit := cWSCSpdGroupLimit;
//          ntLX:  xGrpLimit := cLXSpdGroupLimit;
          ntLX:  xGrpLimit := cLXSpdGroupLimit + cLXSpdMemoryLimit;
        else
          raise Exception.CreateFmt('MsgController GetSettingsAllGroups unknow NetTyp: %d', [Ord(mJob^.NetTyp)]);
        end;
        // copy original job
        CloneJob(mJob, xJob);
        try
          // make new job for WSC/TXN handlers -> doesn't know jtGetSettingsAllGroups
          mJob^.JobTyp := jtGetSettings;
          mJob^.GetSettings.MachineID    := xJob.GetSettingsAllGroups.MachineID;
          mJob^.GetSettings.AllXMLValues := xJob.GetSettingsAllGroups.AllXMLValues;
          // count through all groups and find valid spindle ranges
          for i:=0 to xGrpLimit-1 do begin
            with xJob.GetSettingsAllGroups do begin
              if (xJob.NetTyp = ntWSC) OR (xJob.NetTyp = ntLX) OR
                 ((xJob.NetTyp = ntTXN) and
                  (SpindleRanges[i].SpindleFirst > 0) and (SpindleRanges[i].SpindleFirst < $FF) and
                  (SpindleRanges[i].SpindleLast  > 0) and (SpindleRanges[i].SpindleLast  < $FF)) then begin
                // set members of this job
                case xJob.NetTyp of
                  ntTXN: begin
                      mJob^.GetSettings.SettingsRec.SpindleFirst := SpindleRanges[i].SpindleFirst;
                      mJob^.GetSettings.SettingsRec.SpindleLast  := SpindleRanges[i].SpindleLast;
                    end;
                  ntWSC: mJob^.GetSettings.SettingsRec.Group := i;
                  ntLX:  mJob^.GetSettings.SettingsRec.Group := i;
                else
                end; // case xJob.NetTyp
                // create debug string
                with mJob^.GetSettings.SettingsRec do begin
                  CodeSite.SendFmtMsg('MC %d: %s received: MachID: %d,  GrpNr: %d,  First: %d,  Last: %d. NetTyp: %d', [mJob^.JobID, GetJobName(mJob^.JobTyp), mJob^.GetSettings.MachineID, i, SpindleFirst, SpindleLast, Integer(mJob^.NetTyp)]);
                end;
                // now add single jobs to MsgList with SpindleFirst or GrpNr as identifier...
                case mJob^.NetTyp of
                  ntTXN: xValue := SpindleRanges[i].SpindleFirst;
                  ntWSC: xValue := i;
                  ntLX:  xValue := i;
                else
                  xValue := -1;
                end;
                AddJobToMsgList(xValue, xValue);
                // ...and write single jobs to net handlers
                WriteJobToNet;
              end; // if
            end; // with
          end; // for i
        finally
          FreeMem(xJob);
        end;
      end;
    //-----------------------------------------------------------
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;

  if mJob^.JobTyp in xDebugJobTyp then
    WriteLogDebug(Format('MC %d: %s received: %s. NetTyp: %d', [mJob^.JobID, GetJobName(mJob^.JobTyp), xStr, Integer(mJob^.NetTyp)]));
end;
//-----------------------------------------------------------------------------
function TMsgController.RepeatMessage(aIndex: Integer; aSingleMessage: Boolean): Boolean;
var
  xJobTyp: TJobTyp;
begin
  Result := False;
  xJobTyp := mMsgList.Items[aIndex]^.Job^.JobTyp;
  case xJobTyp of
    jtGetZESpdData,
    jtGetDataStopZESpd,
    jtGetExpZESpdData,
    jtGetExpDataStopZESpd: begin
        if mMsgList.GetOriginalJob(aIndex, mJob, mJobSize) then begin
          if aSingleMessage then begin
            with mMsgList.Items[aIndex]^ do begin
              // set new timeout for GetZESpdData job
              Timeout := cMsgRepeatTimeout;
              CodeSite.SendFmtMsg('MC %d: Timeout: Repeat message: %s/%d', [mJob^.JobID, GetJobName(mJob^.JobTyp), Value]);
              // repeat request only for one spindle at a time
              case xJobTyp of
                jtGetZESpdData: begin
                  mJob^.GetZESpdData.SpindleFirst := Value;
                  mJob^.GetZESpdData.SpindleLast  := Value;
                end;
                jtGetDataStopZESpd : begin
                  mJob^.GetDataStopZESpd.SpindleFirst := Value;
                  mJob^.GetDataStopZESpd.SpindleLast  := Value;
                end;
                jtGetExpZESpdData: begin
                  mJob^.GetExpZESpdData.SpindleFirst := Value;
                  mJob^.GetExpZESpdData.SpindleLast  := Value;
                end;
                jtGetExpDataStopZESpd: begin
                  mJob^.GetExpDataStopZESpd.SpindleFirst := Value;
                  mJob^.GetExpDataStopZESpd.SpindleLast  := Value;
                end;
//                jtViewZESpdData : begin
//                  mJob^.ViewZESpdData.SpindleFirst := Value;
//                  mJob^.ViewZESpdData.SpindleLast  := Value;
//                end;
              end; // case xJobTyp
            end; // end with mMsgList
          end; // if aSingleMessage
          WriteJobToNet;
          Result := True;
        end; // if mMsgList.GetOriginalJob
      end;
    jtMaEqualize: begin
        if mMsgList.GetOriginalJob(aIndex, mJob, mJobSize) then begin
          CodeSite.SendFmtMsg('MC %d: Timeout: Repeat message: %s', [mJob^.JobID, GetJobName(mJob^.JobTyp)]);
          mJob^.JobTyp := jtGetMaAssign;
          WriteJobToNet;
          Result := True;
        end;
      end;
  else
    Result := inherited RepeatMessage(aIndex);
  end;
end;

end.


