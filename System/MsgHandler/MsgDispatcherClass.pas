(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MsgDispatcherClass.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | File created
| 20.10.1998  1.00  Wss | Basic functionality implemented and ready to test
| 26.10.1998  1.01  Wss | Deriving changed to base classes
| 22.06.1999  1.02  Wss | MsgList is now shared. Check GetZESpdData if it is really in MsgList
| 16.03.2000  1.02  Wss | jtGetSettingsAllGroups implemented
| 30.03.2000  1.02  Nue | jtStartGrpEvFromMa and jtStopGrpEvFromMa implemented
| 29.05.2001  1.02  Wss | Check in MsgList returns True only if the msg is not already received
                          -> ignores duplicates
| 08.11.2001  1.02  Wss | jtGetZEReady implemented
| 19.03.2002  1.02  Wss | jtGetZEReady: wenn verzoegert empfangen in Protokoll schreiben
| 31.10.2002  1.02  Wss | jtGetMaAssign wird bei Wiederholung im MD erst gepr�ft damit
                          doppelte Best�tigungen unterdr�ckt werden
| 10.12.2002  1.02  Wss | in jtMaEqualize, jtMaAssign methode GetJobTypFromJobID used because
                          of atomar problems with index from MsgList
| 10.05.2004  1.02  Wss | jtSetMapfile implemented
| 18.05.2004  1.02  Wss | jtGet/SetMaConfig disabled
| 26.07.2005  1.02  Wss | ntLX Unterst�tzung
|=========================================================================================*)
unit MsgDispatcherClass;

interface

uses
  Windows,
  MMBaseClasses, MMEventLog, LoepfeGlobal, BaseGlobal;

type
  TMsgDispatcher = class(TBaseMsgDispatcher)
  private
  protected
    procedure ConfirmMsgReceipt(aValue: DWord = 0); override;
    procedure ProcessJob; override;
    procedure ProcessInitJob; override;
  public
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS, mmCS,
  SysUtils, MsgListClass;

//-----------------------------------------------------------------------------
procedure TMsgDispatcher.ConfirmMsgReceipt(aValue: DWord);
begin
  // set Job dependent value in subrecord
  case mJob^.JobTyp of
    jtGetZESpdData:
        inherited ConfirmMsgReceipt(mJob^.GetZESpdData.SpindID);
//    jtViewZESpdData:
//        inherited ConfirmMsgReceipt(mJob^.ViewZESpdData.SpindID);
  else
    inherited ConfirmMsgReceipt(aValue);
  end;
end;
//-----------------------------------------------------------------------------
procedure TMsgDispatcher.ProcessInitJob;
begin
  ProcessJob;
end;
//-----------------------------------------------------------------------------
procedure TMsgDispatcher.ProcessJob;
var
{$IFDEF MsgDebug}
  xDoExit: Boolean;
  xInt: Integer;
  xPJob: PJobRec;
  xJobSize: DWord;
{$ENDIF}
  xStr: String;
  xValue: Integer;
  xDebugJobTyp: set of TJobTyp;
begin
  xDebugJobTyp := [];
  xStr    := '';
{$IFDEF MsgDebug}
  xPJob   := Nil;
  xJobSize := 0;
{$ENDIF}
  case mJob^.JobTyp of
    // Asynchrone events from machine -> send directly to JobManager
    jtAdjust,
    jtAssign,
    jtAssignComplete,
    jtEntryLocked,
    jtEntryUnlocked,
    jtInitialReset,
    jtMaNotFound,
    jtMaOffline,
    jtMaOnline,
    jtReset,
    jtStartGrpEvFromMa,        //AC338
    jtStopGrpEvFromMa: begin   //AC338
        WriteJobBufferTo(ttJobManager);
      end;

    // NO DATA response of any request or downloads to a machine
    jtClearZEGrpData,
    jtClearZESpdData,
    jtDelZEGrpData,
    jtDelZESpdData,
    jtSetNodeList,
//    jtSetProdID, wss 9.9.04: wieder entfernt da doch ein SetSettings Job ben�tigt wird
    jtStartGrpEv,
    jtStopGrpEv: begin
        ConfirmMsgReceipt;
      end;

    jtGetNodeList,
    jtSetSettings: begin
        WriteJobBufferTo(ttJobManager);
        ConfirmMsgReceipt;
      end;

//    jtGetSettingsAllGroups: begin
    jtGetSettings: begin
        case mJob^.NetTyp of
          ntTXN: xValue := mJob^.GetSettings.SettingsRec.SpindleFirst;
          ntWSC: xValue := mJob^.GetSettings.SettingsRec.Group;
          ntLX:  xValue := mJob^.GetSettings.SettingsRec.Group;
        else
          raise Exception.CreateFmt('MsgDispatcher GetSettings unknow NetTyp: %d', [Ord(mJob^.NetTyp)]);
        end;

        if mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, xValue) then begin
          // if TXN the GroupNr has to be set, in all other cases GroupNr is overwritten again
//          if mMsgList.GetOriginalJobLink(mMsgList.MsgIndex, xJob) then
//            mJob^.GetSettings.SettingsRec.Group := xJob.GetSettings.SettingsRec.Group;

          WriteJobBufferTo(ttDataPoolWriter);
          // create debug string
          with mJob^.GetSettings.SettingsRec do
            xStr := Format('GrpNr: %d,  First: %d,  Last: %d', [Group, SpindleFirst, SpindleLast]);
          // now confirm message to MsgController with xValue as CompareValue
          ConfirmMsgReceipt(xValue);
        end else begin
          xStr := 'Message not found in list';
        end;
      end;

    jtMaEqualize,  // shouldn't appear here !
    jtGetMaAssign: begin     // Get machine configuration
        // get original job type from saved job
        mJob^.JobTyp := mMsgList.GetJobTypFromJobID(mJob^.JobID);
        if mJob^.JobTyp <> jtNone then begin
          if mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, 0) then begin
            WriteJobBufferTo(ttJobManager);
            ConfirmMsgReceipt;
          end;
        end;
      end;
    //-----------------------------------------------------------
    // WITH DATA response of any request or downloads to a machine
    //jtViewZEGrpData
    jtGetZEGrpData,
    jtSaveMaConfigToDB: begin
        WriteJobBufferTo(ttDataPoolWriter);
        ConfirmMsgReceipt;
      end;
    jtGetZESpdData, jtViewZESpdData: begin
{$IFDEF MsgDebug}
        xDoExit := False;
        xInt := mMsgList.MsgIndexFromJobID[mJob^.JobID];
        if xInt <> -1 then begin
          if mMsgList.GetOriginalJob(xInt, xPJob, xJobSize) then begin
            xValue  := StrToIntDef(GetRegString(cRegLM, cRegMMDebug, 'MsgIgnoreSpd' + IntToStr(xPJob^.GetZESpdData.MachineID), '0'), 0);
            if xValue <> 0 then begin
              xInt := xPJob^.GetZESpdData.SpindleLast;
              case xValue of
                // Spindel 1 von Maschine wird immer blockiert
                1: xDoExit := (mJob^.GetZESpdData.SpindID = 1);
                // Spindel 1 von Maschine wird 1x blockiert
                // Annahme: Spd 2 wird nach Spd 1 empfangen. So ist beim 2. Durchgang Spd 2 bereits abgehackt und Spd1 kann nun auch best�tigt werden
                2: xDoExit := (mJob^.GetZESpdData.SpindID = 1) and mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, xInt-1);
                // Erste Spindel von Partie wird immer blockiert
                3: xDoExit := (mJob^.GetZESpdData.SpindID = xInt);
                // Erste Spindel von Partie wird 1x blockiert
                4: xDoExit := (mJob^.GetZESpdData.SpindID = xInt) and mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, xInt-1);
              end; // case xValue
            end; // if GetOriginalJob
          end; // if xInt <> -1
        end; // if xValue <> 0
        if xDoExit then begin
          if (xPJob^.GetZESpdData.MachineID = 1) then begin
            xDoExit := (gMachID1 < 3);
          end
          else if (xPJob^.GetZESpdData.MachineID = 6) then begin
            xDoExit := (gMachID6 < 2);
          end;
        end;

        if xDoExit then begin
          WriteLogDebug(Format('MD %d: Spindel %d blockiert: Mode=%d',[mJob^.JobID, mJob^.GetZESpdData.SpindID, xValue]));
          Exit;
        end;
{$ENDIF}
        xStr := Format('%s %d:', [GetJobName(mJob^.JobTyp), mJob^.GetZESpdData.SpindID]);
        if mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, mJob^.GetZESpdData.SpindID) then begin
          WriteJobBufferTo(ttDataPoolWriter);
          ConfirmMsgReceipt;
        end;
      end;
    jtJobResult: begin
        xStr := 'JobResult';
        WriteJobBufferTo(ttMsgController);
      end;
    jtGetZEReady: begin
        if mMsgList.MsgPending(mJob^.JobTyp, mJob^.JobID, 0) then
          ConfirmMsgReceipt
        else begin
          WriteLog(etWarning, Format('MD %d: DELAYED GetZEReady received from MaID:%d !!',[mJob^.JobID, mJob^.GetZEReady.MachineID]));
        end;
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;

  if mJob^.JobTyp in xDebugJobTyp then
    WriteLogDebug(Format('MD %d: %s received: %s. NetTyp: %d',[mJob^.JobID, GetJobName(mJob^.JobTyp), xStr, Integer(mJob^.NetTyp)]));
end;
//-----------------------------------------------------------------------------
end.

