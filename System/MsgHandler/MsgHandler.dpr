(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MsgHandler.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Projektdatei des MsgHandlers
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | File created
| 22.06.1999  1.01  Wss | MsgList is shared  for MsgController and MsgDispatcher
| 21.01.2002  1.26  Wss | Link to CodeSite changed to CodeSite2 with unit CSIntf (instead of RzCSIntf) 
| 10.10.2002        LOK | Umbau ADO
|=========================================================================================*)
program MsgHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  SysUtils,
  BaseMain,
  LoepfeGlobal,
  BaseGlobal,
  RzCSIntf,
  ActiveX,
  Windows,
  MsgListClass in '..\..\Common\MsgListClass.pas',
  MsgHandlerClass in 'MsgHandlerClass.pas',
  MsgDispatcherClass in 'MsgDispatcherClass.pas',
  MsgControllerClass in 'MsgControllerClass.pas',
  BaseThread in '..\..\Common\BaseThread.pas',
  MMBaseClasses in '..\..\Common\MMBaseClasses.pas';

var
  xMsgHandler: TMsgHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('MsgHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  try
    // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
    // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
    if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
      raise Exception.Create('CoInitialize failed');
    xMsgHandler := TMsgHandler.Create(ssMsgHandler);
    if xMsgHandler.Initialize then
    try
      xMsgHandler.Run;
    except
      CodeSite.SendError(Format('MsgHandler.Run: %s', [GetLastErrorText]));
    end;
    xMsgHandler.Free;
  finally
    CoUninitialize;
  end;
end.

