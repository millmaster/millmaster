(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MsgHandlerClass.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | File created
| 11.04.2001  2.06  Nue | TBaseSettingsReader ==> TMMSettingsReader
| 13.10.2002        LOK | Umbau ADO
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|=========================================================================================*)
unit MsgHandlerClass;

interface

uses
  mmThread, Classes, SettingsReader,
  MMBaseClasses, BaseMain, MsgListClass, MsgControllerClass, MsgDispatcherClass, BaseGlobal;

type
  TMsgHandler = class(TBaseMain)
  private
  protected
    mMsgList: TMsgList;
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; override;
    function GetParamHandle: TMMSettingsReader; override;
  public
    destructor Destroy; override;
    function Initialize: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  MMEventLog,
  RzCSIntf,
  LoepfeGlobal;

//-----------------------------------------------------------------------------
function TMsgHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttMsgController: begin
        aThread := TMsgController.Create(mSubSystemDef.Threads[1], mMsgList);
      end;
    ttMsgDispatcher: begin
        aThread := TMsgDispatcher.Create(mSubSystemDef.Threads[2], mMsgList);
      end;
    ttMsgTimer: begin
        aThread := TTimeoutTicker.Create(mSubSystemDef.Threads[3]);
      end;
  else
    aThread := Nil;
    Result := False;
  end;
end;
//-----------------------------------------------------------------------------
destructor TMsgHandler.Destroy;
begin
  mMsgList.Free;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------
function TMsgHandler.GetParamHandle: TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;

function TMsgHandler.Initialize: Boolean;
var
  xFileVersion: string;
begin
  mMsgList := TMsgList.Create;
  Result := inherited Initialize;
  xFileVersion := 'unknown';
  try
    xFileVersion := GetFileVersion(ParamStr(0));
  except
    // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
  end;
  WriteLog ( etInformation , 'MsgHandler started with Fileversion ' + xFileVersion);
{
  if Result then begin
  end;
{}
//  CodeSite.DestinationDetails := CodeSiteConnectionString;
end;
//-----------------------------------------------------------------------------
end.

