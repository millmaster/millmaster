{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMWindingLink.pas   -> MMWindingLink.DLL
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: - Zugriffsroutienen
|                   Funktionen : - DBConnect
|                                - DBDisconnect
|                                - xp_DeleteMMData

| Info..........: -  Boolean Results : TRUE  = 1 (Integer)
|                                      FALSE = 0 (Integer)
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.2001  1.00  SDo | File created
| 10.02.2001  1.00  Nue | Aditional code and DB-Queries added.
| 13.03.2001  1.01  Nue | Delete Query angepasst.
| 01.10.2001  1.02  Nue | if NOT GetRegBoolean(cRegLM, cRegMillMasterPath, cRegLongTimeMMWinding, '') then begin added.
| 18.06.2002  1.03  Nue | Call of AllowedToDelete moved. No MsgDialog within Open.
===============================================================================}
library SettingsReaderTest;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  SysUtils,
//  Classes,
  Windows,
//  dbtables,
//  db,
  mmSetupModul,
  DBAccessClass,
  mmEventLog,
  BaseGlobal,
  LoepfeGlobal,
  SrvStrucDef in '\\wetsrvbde2\develop\MillMaster\System\MMWindingLink\SrvStrucDef.pas';

{$R *.RES}

var
  gLDB: TDBAccess;
  gEventLog: TEventLogWriter;

//******************************************************************************
// Konvertiert einen Boolean-Wert in eine Integer-Zahl   TRUE=1; FALSE=0
//******************************************************************************
function ConvertBoolean(aValue: Boolean): Integer;
begin
  case aValue of
    True: Result := 1;
  else
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Verbindung mit der MM-DB
//******************************************************************************
function DBConnect: Integer; stdcall;
var
  xRet: Integer;
begin
  xRet := 0;

  gLDB := nil;
  gLDB := TDBAccess.Create(1, True);
  try
    xRet := ConvertBoolean(gLDB.Init);
    with gLDB.DataBase do begin
      Close;
      Open;
    end;
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'DBConnect: ' + e.Message);
      FreeAndNil(gLDB);
    end;
  end;
  Result := xRet;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loest die DB-Verbindung
//******************************************************************************
function DBDisconnect: Integer; stdcall;
begin
  Result := 1;
  try
    FreeAndNil(gLDB);
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'DBDisconnect: ' + e.Message);
      Result := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Fuehrt ein Exec SQL-Statement aus; DB muss erstellt und initialisiert sein
//******************************************************************************
function ExecSQL(aSQLTxt: string): Boolean;
begin
  Result := False;
  try
    with gLDB.Query[cPrimaryQuery] do begin
      SQL.Text := aSQLTxt;
      ExecSQL;
      Result := True;
    end;
  except
    on e:Exception do begin
      gEventLog.Write(etError, 'DBEngine error in DLL MMWindingLink [ExecSQL]. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Fuehrt ein Open SQL-Statement aus; DB muss erstellt und initialisiert sein
//******************************************************************************
function Open(aSQLTxt: string; aFetchField: string; var aResultString: string): Boolean;
var
  xText: string;
begin
  Result := False;
  try
    with gLDB.Query[cPrimaryQuery] do begin
      gEventLog.Write(etError, Format('xp_DeleteMMWinding:Open %s %s', [aSQLTxt, aFetchField]));

      SQL.Clear;
      SQL.Add(aSQLTxt);

      Open;
      if not EOF then begin
        aResultString := FieldByName(aFetchField).AsString;
      end
      else
        aResultString := 'NO Result!';
      Result := True;
    end;
  except
    on e:Exception do begin
      gEventLog.Write(etError, 'DBEngine error in DLL MMWindingLink [Open]. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loescht die Daten eines oder mehrerer alter Monate aus DB
//  Falls Parameter (in/out) bei dieser Routine dazukommen, wird auf Beispiel
//  in Bde_Dev\Delphi\act\tests\dll verwiesen f�r Codebeispiele!
//******************************************************************************
{
function xp_DeleteMMData(aSrvProc: PSRV_PROC): Integer; cdecl; export;
//function xp_DeleteMMData:Integer; stdcall;
var
  xRet: Boolean;
  xText, xText1, xText2, xText3: string;
  //.................................................................
  function AllowedToDelete: Boolean;
  var
    xCurrentVersion, xLastUpdateVersion: string;
    xLastUpdateDate: Double;
  begin
    Result := False;
    if not GetRegBoolean(cRegLM, cRegMillMasterPath, cRegLongTimeMMWinding, False) then begin //Nue:1.10.01
      xCurrentVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegCurrentVersion, '');
      if xCurrentVersion <> '' then begin
        xLastUpdateVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, '');
        if (xLastUpdateVersion <> '') and (xLastUpdateVersion = xCurrentVersion) then begin
          xLastUpdateDate := GetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, 0.0);
          if xLastUpdateDate <> 0.0 then begin
            if ((Double(Now) - xLastUpdateDate) > 10) then //After more than 10 days after last update, allow data deletion
              Result := True
            else
              gEventLog.Write(etInformation, Format(':  %d days since last MillMaster update.', [Trunc(Double(Now) - xLastUpdateDate)]));
          end
          else
            SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end
        else begin
          SetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, xCurrentVersion);
          SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end;
      end;
    end;
  end;
  //.................................................................
begin
  // EventLog create
  // TMMSettingsReader create
  // DB open
  // SQL exec
  // DB close
  // Free's

  xRet := False;

  // EventLog erstellen
  try
    gEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
      cEventLogServerNameForSubSystems,
      ssApplication, 'xp_SettingsReadertest', True);
    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach Eventlog create');
  except
    Result := ConvertBoolean(False);
    exit;
  end;

  if AllowedToDelete then begin //Nue:18.06.2002 Moved to this place
    // TMMSettingsReader erstellen
    try
      gSettingsReader := TMMSettingsReader.Create;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader create');
      gSettingsReader.Init;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader init');
    except
      on E: Exception do begin
        xText := 'TMMSettingsReader.Create error in DLL MMWindingLink [xp_DeleteMMData]. ' +
          #13#10 + #13#10 + e.Message;

        gEventLog.Write(etError, xText);

        gSettingsReader.Free;
        Result := ConvertBoolean(False);
        exit; //Nue:18.06.2002
      end;
    end;

    // DB verbinden
    if DBConnect = 1 then begin
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader DBConnect');
        // SQL Statement ausfuehren
           // Packages abfragen
      if (not (gSettingsReader.IsPackageLabMaster) and not (gSettingsReader.IsPackageDispoMaster)) then begin
  //       MessageDlg('IsPackageWindingMaster is inst. ' , mtInformation	, [mbOk], 0);
        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (then)');
        xText1 := '?';
        xText2 := '?';
        xText3 := '?';

        xText := 'select max(c_shift_start) start from t_shift where datediff(month,c_shift_start,getdate()) > 1 ';
        xRet := Open(xText, 'start', xText1);
        if xRet then begin
          xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
          xRet := Open(xText, 'nrof', xText2);
          if xRet then begin
            xText := 'delete t_dw_prodgroup_shift where c_fragshift_start < ' +
              '(select max(c_shift_start) from t_shift where datediff(month,c_shift_start,getdate()) > 1) ';
                // >1 keeps fragshifts younger 2 months

            xRet := ExecSQL(xText);
            if xRet then begin
              xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
              xRet := Open(xText, 'nrof', xText3);
              xRet := True;
            end;
          end;
        end;

        if xRet then begin
          gEventLog.Write(etInformation, ': Succesful deleted oldest month data! (Data older: ' + xText1 + ' deleted!. Datasets in t_dw_prodgroup_shift before/after ' + xText2 + '/' + xText3 + ')');
        end
        else begin
          gEventLog.Write(etInformation, ': Deleting oldest month data failed in DB-Proc (xp_DeleteMMData)! (on executing query: ' + xText + ')');
        end;
      end
      else begin
        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (else)');
      end;
      DBDisconnect;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach DBDisconnect ');
    end;
    gSettingsReader.Free;
    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach gSettingsReader.Free ');
  end;

  // Free
  gEventLog.Free;
  Result := ConvertBoolean(xRet);
end;
{}
//------------------------------------------------------------------------------
function xp_SettingsReaderTest(aSrvProc: PSRV_PROC): Integer; cdecl; export;
//function xp_DeleteMMData:Integer; stdcall;
var
  xRet: Boolean;
  xText, xText1, xText2, xText3: string;
  lSettingsReader: TMMSettingsReader;
  //.................................................................
  function AllowedToDelete: Boolean;
  var
    xCurrentVersion, xLastUpdateVersion: string;
    xLastUpdateDate: Double;
  begin
    Result := False;
    if not GetRegBoolean(cRegLM, cRegMillMasterPath, cRegLongTimeMMWinding, False) then begin //Nue:1.10.01
      xCurrentVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegCurrentVersion, '');
      if xCurrentVersion <> '' then begin
        xLastUpdateVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, '');
        if (xLastUpdateVersion <> '') and (xLastUpdateVersion = xCurrentVersion) then begin
          xLastUpdateDate := GetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, 0.0);
          if xLastUpdateDate <> 0.0 then begin
            if ((Double(Now) - xLastUpdateDate) > 10) then //After more than 10 days after last update, allow data deletion
              Result := True
            else
              gEventLog.Write(etInformation, Format(':  %d days since last MillMaster update.', [Trunc(Double(Now) - xLastUpdateDate)]));
          end
          else
            SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end
        else begin
          SetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, xCurrentVersion);
          SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end;
      end;
    end;
  end;
  //.................................................................
begin
  // EventLog create
  // TMMSettingsReader create
  // DB open
  // SQL exec
  // DB close
  // Free's

  xRet := False;

  // EventLog erstellen
  try
    gEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
      cEventLogServerNameForSubSystems,
      ssApplication, 'xp_DeleteMMData', True);
    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach Eventlog create');
  except
    Result := ConvertBoolean(False);
    exit;
  end;

  if AllowedToDelete then begin //Nue:18.06.2002 Moved to this place
    // TMMSettingsReader erstellen
    try
      lSettingsReader := TMMSettingsReader.Create;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader create');
      lSettingsReader.Init;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader init');
    except
      on E: Exception do begin
        xText := 'TMMSettingsReader.Create error in DLL MMWindingLink [xp_DeleteMMData]. ' +
          #13#10 + #13#10 + e.Message;

        gEventLog.Write(etError, xText);

        lSettingsReader.Free;
        Result := ConvertBoolean(False);
        exit; //Nue:18.06.2002
      end;
    end;

    // DB verbinden
    if DBConnect = 1 then begin
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader DBConnect');
        // SQL Statement ausfuehren
           // Packages abfragen
      if (not (lSettingsReader.IsPackageLabMaster) and not (lSettingsReader.IsPackageDispoMaster)) then begin
  //       MessageDlg('IsPackageWindingMaster is inst. ' , mtInformation	, [mbOk], 0);
        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (then)');
        xText1 := '?';
        xText2 := '?';
        xText3 := '?';

        xText := 'select max(c_shift_start) start from t_shift where datediff(month,c_shift_start,getdate()) > 1 ';
        xRet := Open(xText, 'start', xText1);
        if xRet then begin
          xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
          xRet := Open(xText, 'nrof', xText2);
          if xRet then begin
            xText := 'delete t_dw_prodgroup_shift where c_fragshift_start < ' +
              '(select max(c_shift_start) from t_shift where datediff(month,c_shift_start,getdate()) > 1) ';
                // >1 keeps fragshifts younger 2 months

            xRet := ExecSQL(xText);
            if xRet then begin
              xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
              xRet := Open(xText, 'nrof', xText3);
              xRet := True;
            end;
          end;
        end;

        if xRet then begin
          gEventLog.Write(etInformation, ': Succesful deleted oldest month data! (Data older: ' + xText1 + ' deleted!. Datasets in t_dw_prodgroup_shift before/after ' + xText2 + '/' + xText3 + ')');
        end
        else begin
          gEventLog.Write(etInformation, ': Deleting oldest month data failed in DB-Proc (xp_DeleteMMData)! (on executing query: ' + xText + ')');
        end;
      end
      else begin
        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (else)');
      end;
      DBDisconnect;
      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach DBDisconnect ');
    end else // if DBConnect
      gEventLog.Write(etError, 'xp_DeleteMMWinding: DBConnect else');
    lSettingsReader.Free;
    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach lSettingsReader.Free ');
  end else // if AllowDelete
    gEventLog.Write(etError, 'xp_DeleteMMWinding: AllowDelete else');

  // Free
{}
  gEventLog.Write(etError, 'xp_DeleteMMWinding: Vor gEventLog.Free ');
  FreeAndNil(gEventLog);
  Result := ConvertBoolean(xRet);
end;
//------------------------------------------------------------------------------

exports

   //Int. Name         ext. Name
  DBConnect name 'DBConnect',
  DBDisconnect name 'DBDisconnect',
  xp_SettingsReaderTest name 'xp_SettingsReaderTest';

begin

end.

