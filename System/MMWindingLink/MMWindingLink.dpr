{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMWindingLink.pas   -> MMWindingLink.DLL
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: - Zugriffsroutienen
|                   Funktionen : - DBConnect
|                                - DBDisconnect
|                                - xp_DeleteMMData
|                                - xp_SendMMMachStateChanged
| Info..........: -  Boolean Results : TRUE  = 1 (Integer)
|                                      FALSE = 0 (Integer)
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.02.2001  1.00  SDo | File created
| 10.02.2001  1.00  Nue | Aditional code and DB-Queries added.
| 13.03.2001  1.01  Nue | Delete Query angepasst.
| 01.10.2001  1.02  Nue | if NOT GetRegBoolean(cRegLM, cRegMillMasterPath, cRegLongTimeMMWinding, '') then begin added.
| 18.06.2002  1.03  Nue | Call of AllowedToDelete moved. No MsgDialog within Open.
| 10.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 13.11.2002  1.04  Nue | xp_SendMMMachStateChanged added.
| 22.09.2003  1.05  Nue | xp_SendMMMachStateChanged: xBroadcaster.Write-Handling modifiziert.
| 04.12.2003  1.06  Nue | FreeAndNil(xBroadcaster) added.
| 17.02.2004  1.07  Nue | New text: ': In %d days, old MillMaster data will be deleted.' added. (Modified)
| 11.02.2005        Wss | CodeSite Kontrolle f�r Debug Meldungen
| 07.06.2007  1.08  Nue | Anpassen der Easy-Datenhaltung auf 14 Monate (statt 2 wie bisher!)
===============================================================================}
library MMWindingLink;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  //  ShareMem,
  SysUtils,
  //  Classes,
  Windows,
  Dialogs,
  //  dbtables,
  //  db,
  SettingsReader,
  ActiveX,
  mmCS,
  AdoDBAccess,
  mmEventLog,
  BaseGlobal,
  LoepfeGlobal,
  Mailslot,                             //Nue:12.11.02
  SrvStrucDef in 'SrvStrucDef.pas';

{$R *.RES}
{$R Version.res}

var
  gLDB: TAdoDBAccess;
  gEventLog: TEventLogWriter;

  //******************************************************************************
  // Konvertiert einen Boolean-Wert in eine Integer-Zahl   TRUE=1; FALSE=0
  //******************************************************************************

function ConvertBoolean(aValue: Boolean): Integer;
begin
  if aValue then Result := 1
            else Result := 0;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Verbindung mit der MM-DB
//******************************************************************************

function DBConnect: Integer; STDCALL;
begin
  try
    Result := 0;
    gLDB := TAdoDBAccess.Create(1, True);
    try
      if gLDB.Init then begin
        // Nicht ganz klar warum das unter BDE implementiert wurde (LOK)
    (*      with gLDB.DataBase do begin
            Close;
            Open;
          end;*)
        Result := 1;
      end else
        gEventLog.Write(etError, 'MMWindingLink.DBConnect: gLDB.Init failed: ' + gLDB.DBErrorTxt);
    except
      on e: Exception do begin
        gEventLog.Write(etError, 'Exception in DLL MMWindingLink [DBConnect]: ' + e.Message);
        FreeAndNil(gLDB);
      end;
    end;
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'Exception in DLL MMWindingLink [DBConnect]: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loest die DB-Verbindung
//******************************************************************************

function DBDisconnect: Integer; STDCALL;
begin
  try
    gLDB.Free;
    gLDB := nil;
    Result := 1;
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'Exception in DLL MMWindingLink [DBDisconnect]: ' + e.Message);
      Result := 0;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Fuehrt ein Exec SQL-Statement aus; DB muss erstellt und initialisiert sein
//******************************************************************************

function ExecSQL(aSQLTxt: string): Boolean;
begin
  with gLDB.Query[cPrimaryQuery] do try
    Command.CommandTimeout := 240; //Timeout wait for 240s
    SQL.Text := aSQLTxt;
    ExecSQL;
    Result := True;
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'Exception in DLL MMWindingLink [ExecSQL]: ' + e.Message);
      Result := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Fuehrt ein Open SQL-Statement aus; DB muss erstellt und initialisiert sein
//******************************************************************************

function Open(aSQLTxt: string; aFetchField: string; var aResultString: string): Boolean;
begin
  with gLDB.Query[cPrimaryQuery] do try
    //    gEventLog.Write(etError, Format('xp_DeleteMMWinding:Open %s %s', [aSQLTxt, aFetchField]));
    SQL.Text := aSQLTxt;
    Open;
    if not EOF then                     // ADO Conform
      aResultString := FieldByName(aFetchField).AsString
    else
      aResultString := 'NO Result!';
    Result := True;
  except
    on e: Exception do begin
      gEventLog.Write(etError, 'Exception in DLL MMWindingLink [Open]: ' + e.Message);
      Result := False;
    end;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Loescht die Daten eines oder mehrerer alter Monate aus DB
//  Falls Parameter (in/out) bei dieser Routine dazukommen, wird auf Beispiel
//  in Bde_Dev\Delphi\act\tests\dll verwiesen f�r Codebeispiele!
//******************************************************************************

function xp_DeleteMMData(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
var
  xRet: Boolean;
  xText, xText1, xText2, xText3: string;
  xSettingsReader: TMMSettingsReader;
  //..........................................................
  function AllowedToDelete: Boolean;
  var
    xCurrentVersion, xLastUpdateVersion: string;
    xLastUpdateDate: Double;
  begin
    Result := False;
    if not GetRegBoolean(cRegLM, cRegMillMasterPath, cRegLongTimeMMWinding, False) then begin //Nue:1.10.01
      xCurrentVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegCurrentVersion, '');
      //      gEventLog.Write(etInformation, 'xCurrentVersion: ' + xCurrentVersion);
      if xCurrentVersion <> '' then begin
        xLastUpdateVersion := GetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, '');
        //        gEventLog.Write(etInformation, 'xLastUpdateVersion: ' + xLastUpdateVersion);
        if (xLastUpdateVersion <> '') and (xLastUpdateVersion = xCurrentVersion) then begin
          xLastUpdateDate := GetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, 0.0);
          //          gEventLog.Write(etInformation, 'xLastUpdateDate: ' + DateTimeToStr(xLastUpdateDate));
          if xLastUpdateDate <> 0.0 then begin
            if ((Double(Now) - xLastUpdateDate) > 10) then //After more than 10 days after last update, allow data deletion
              Result := True
            else
              gEventLog.Write(etInformation, Format(': In %d days, old MillMaster data will be deleted.', [10-(Trunc(Double(Now) - xLastUpdateDate))]));
          end
          else
            SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end
        else begin
          SetRegString(cRegLM, cRegMillMasterPath, cRegLastUpdateVersion, xCurrentVersion);
          SetRegFloat(cRegLM, cRegMillMasterPath, cRegLastUpdateDate, Double(Now));
        end;
      end;
    end;
  end;
  //..........................................................
begin
  // EventLog create
  // TMMSettingsReader create
  // DB open
  // SQL exec
  // DB close
  // Free's

  Result := 0;
  // EventLog erstellen
  try
    gEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
      cEventLogServerNameForSubSystems,
      ssApplication, 'xp_DeleteMMData', True);
    //    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach Eventlog create');
  except
    Result := ConvertBoolean(False);
    exit;
  end;

  xRet := True;
  xSettingsReader := nil;

  if AllowedToDelete then try           //Nue:18.06.2002 Moved to this place
    //    gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete');
        // DB verbinden
    if DBConnect = 1 then begin
      //      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader DBConnect');
      try
        xSettingsReader := TMMSettingsReader.Instance;
        //      gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader create');
        try
          if not xSettingsReader.Init then begin
            gEventLog.Write(etError, 'xp_DeleteMMWinding: TMMSettingsReader.Init = False');
            Exit;
          end;
          //          gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader init');
        except
          on E: Exception do begin
            gEventLog.Write(etError, 'TMMSettingsReader.Init error in DLL MMWindingLink [xp_DeleteMMData]: ' + e.Message);
            xRet := False;
            Exit;                       //Nue:18.06.2002
          end;
        end;

        // Packages abfragen
//        if (not (xSettingsReader.IsPackageLabMaster) and not (xSettingsReader.IsPackageDispoMaster)) then begin
        if xSettingsReader.IsPackageEasy then begin
          //          gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (then)');
                    // SQL Statement ausfuehren
          xText1 := '?';
          xText2 := '?';
          xText3 := '?';

          xText := 'select max(c_shift_start) start from t_shift where datediff(month,c_shift_start,getdate()) > 13 ';  //Nue:7.6.07 von 1 auf 13
          xRet := Open(xText, 'start', xText1);
          if xRet then begin
            xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
            xRet := Open(xText, 'nrof', xText2);
            if xRet then begin
              xText := 'delete t_dw_prodgroup_shift where c_fragshift_start < ' +
                '(select max(c_shift_start) from t_shift where datediff(month,c_shift_start,getdate()) > 13) ';  //Nue:7.6.07 von 1 auf 13
              // >1 keeps fragshifts younger 2 months

              xRet := ExecSQL(xText);
              if xRet then begin
                xText := 'select count(*) nrof from t_dw_prodgroup_shift ';
                xRet := Open(xText, 'nrof', xText3);
                xRet := True;
              end;
            end;
          end;                          // if xRet

          if xRet then
            gEventLog.Write(etInformation, ': Succesful deleted oldest month data! (Data older: ' + xText1 + ' deleted!. Datasets in t_dw_prodgroup_shift before/after ' + xText2 + '/' + xText3 + ')')
          else
            gEventLog.Write(etInformation, ': Deleting oldest month data failed in DB-Proc (xp_DeleteMMData)! (on executing query: ' + xText + ')');
        end else begin
          //          gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach AllowedToDelete (else)');
        end;                            // if
        //        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach DBDisconnect ');
      finally
        // bei Singelton nicht mehr notwendig
  //        xSettingsReader.Free;
  //        gEventLog.Write(etError, 'xp_DeleteMMWinding: Nach gSettingsReader.Free ');
        DBDisconnect;
      end;                              // try
    end;                                // if DBConnect
  finally
    // Free
    gEventLog.Free;
    Result := ConvertBoolean(xRet);
  end;                                  // if AllowedToDelete
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Diese Stored Proc wird aufgerufen, wenn der Status einer Maschine wechselt.
// (Im Context von Trigger tr_upd_machine) Sie sendet dem MMClient eine Meldung
// �ber den Statuswechsel.
//******************************************************************************

function xp_SendMMMachStateChanged(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
var
  xRet: Boolean;

var
  xMachId: Word;
  bType: BYTE;
  fNull: Boolean;
  cbMaxLen, cbActualLen: ULONG;
  cbData: pInteger;
  xEventLog: TEventLogWriter;
  xSettingsReader: TMMSettingsReader;
  xMsg: TMMClientRec;
  xBroadcaster: TBroadcaster;
begin
  // EventLog create
  // TMMSettingsReader instance; init
  // TBroadcaster.Create; init
  // TBroadcaster.Write
  // Free's

  Result := 0;
  // EventLog erstellen
  try
//    if not Assigned(xEventLog) then
    xEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems,
                   cEventLogServerNameForSubSystems,
                   ssApplication, 'xp_SendMMMachStateChanged', True);
  except
//    xEventLog.Write(etError, 'xp_SendMMMachStateChanged: Nach Eventlog create');
    Result := ConvertBoolean(False);
    exit;
  end;

  xRet            := True;
  xBroadcaster    := nil;
  xSettingsReader := nil;
  try // ...finally
    xSettingsReader := TMMSettingsReader.Instance;
    try
      if not xSettingsReader.Init then begin
        xEventLog.Write(etError, 'xp_SendMMMachStateChanged: TMMSettingsReader.Init = False');
        Exit;
      end;
//                xEventLog.Write(etError, 'xp_DeleteMMWinding: Nach TMMSettingsReader init');
    except
      on E: Exception do begin
        xEventLog.Write(etError, 'TMMSettingsReader.Init error in DLL MMWindingLink [xp_SendMMMachStateChanged]: ' + e.Message);
        xRet := False;
        Exit;                           //Nue:18.06.2002
      end;
    end;

{$IFNDEF BroadcasterNOK}
    try
//                             xEventLog.Write(etInformation, Format('xp_SendMMMachStateChanged: Broadcast to %s', [xSettingsReader.value[cDomainNames]]));
      xBroadcaster := TBroadcaster.Create(xSettingsReader.value[cDomainNames], cChannelNames[ttMMClientReader]);
      if not xBroadcaster.Init then begin
        xRet := False;
        xEventLog.Write(etError, 'xp_SendMMMachStateChanged: xBroadcaster.Init = False');
        Exit;
      end;

      try

        // Count the number of parameters.
        xMachId := 0;
        {wss
                    if srv_rpcparams(aSrvProc) = 1 then
                    begin
                      // Use srv_paraminfo to get data, type, and length information of the parameter.
                      // The parameter data should contain the machine_id
                      srv_paraminfo(aSrvProc, 1, @bType, @cbMaxLen, @cbActualLen, Integer(@cbData), fNull);
                      cbData :=
                      xMachId:=Word(cbData);
                    end
                    else begin //No parameter data available
                      xMachId:=0;
                      xEventLog.Write(etWarning, 'xp_SendMMMachStateChanged: no explicit machine_id in parameter list -> machine_id set to 0');
                    end;
        {}

        FillChar(xMsg, sizeof(xMsg), 0);
        xMsg.MsgTyp := ccMMApplMsg;
        xMsg.ServerName := xSettingsReader.value[cMMHost];
        //wss            xMsg.ApplMsg.MachineID := xMachId;
        xMsg.ApplMsg.MachineID := 0;
        xMsg.ApplMsg.ApplMsgEvent := amMachineStateChange;
//        xEventLog.WriteLogDebug(etInformation, Format('xp_SendMMMachStateChanged: MachID=%d', [xMachId]));
        if not xBroadcaster.Write(@xMsg, sizeof(xMsg)) then begin
          xEventLog.Write(etWarning, 'xp_SendMMMachStateChanged: xBroadcaster.Write failed: ' + xBroadcaster.ErrorInformation);
//Nue/Kr:22.9.03 N�chste 2 Zeilen in Comment und obige Msg als Warning, fr�her Error
//          xRet := False;
//          Exit;
        end;
      except
        on e: Exception do begin
          xEventLog.Write(etError, 'xp_SendMMMachStateChanged: srv_paraminfo-call or xBroadcaster.Write crash');
          xRet := False;
          Exit;
        end;
      end;

    except
      on E: Exception do begin
        xEventLog.Write(etError, 'TBroadcaster.Create error in DLL MMWindingLink [xp_SendMMMachStateChanged]: ' + e.Message);
        xRet := False;
        Exit;                           //Nue:18.06.2002
      end;
    end;
{$ENDIF}

  finally
    // Free
    FreeAndNil(xBroadcaster);
    xEventLog.Free;
    Result := ConvertBoolean(xRet);
  end;
end;
//------------------------------------------------------------------------------
//*********************************************************
//  Eintritt in die DLL
//*********************************************************

procedure DLLEntry(dwReason: DWORD);
const
  xResult: HResult = -1;
begin
  case dwReason of
    DLL_PROCESS_ATTACH: begin
        xResult := CoInitialize(nil);
        // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
        // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
        if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
          raise Exception.Create('CoInitialize failed');
      end;                              // DLL_PROCESS_ATTACH: begin
    DLL_PROCESS_DETACH: begin
        if (xResult = S_OK) or (xResult = S_FALSE) then
          CoUninitialize;
      end;                              // DLL_PROCESS_DETACH: begin
  end;                                  // case dwReason of
end;                                    // procedure DLLEntry(dwReason:DWORD);

exports

  //Int. Name         ext. Name
  DBConnect name 'DBConnect',
  DBDisconnect name 'DBDisconnect',
  xp_DeleteMMData name 'xp_DeleteMMData',
  xp_SendMMMachStateChanged name 'xp_SendMMMachStateChanged';

begin
{$IFDEF BroadcasterNOK}
  WriteToEventLog('TESTVERSION DLL MMWindingLink [xp_SendMMMachStateChanged]: Broadcaster ausgeschaltet!!', 'MMWindingLinkDLL: ', etError);
{$ENDIF}

  gApplicationName := 'MMWindingLink';
  CodeSite.Enabled := CodeSiteEnabled;

  DLLProc := @DLLEntry;
  // Process Attach erst mal von Hand aufrufen, da die Procedur erst nach dem
  // Eintritt des Prozesses gesetzt wird
  DLLEntry(DLL_PROCESS_ATTACH);
end.

