object Form1: TForm1
  Left = 315
  Top = 234
  Width = 532
  Height = 256
  Caption = 'MMWindingLink.dll Tester'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 208
    Top = 128
    Width = 32
    Height = 13
    Caption = 'Label3'
  end
  object Label4: TmmLabel
    Left = 208
    Top = 168
    Width = 32
    Height = 13
    Caption = 'Label4'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmLabel1: TmmLabel
    Left = 35
    Top = 204
    Width = 131
    Height = 13
    Caption = 'Machine ID, witch changed'
    FocusControl = mmEdit1
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object GroupBox1: TGroupBox
    Left = 16
    Top = 8
    Width = 257
    Height = 97
    Caption = 'DB'
    TabOrder = 0
    object Label1: TLabel
      Left = 137
      Top = 32
      Width = 32
      Height = 13
      Caption = 'Label1'
    end
    object Label2: TLabel
      Left = 137
      Top = 68
      Width = 32
      Height = 13
      Caption = 'Label2'
    end
    object Button1: TButton
      Left = 16
      Top = 24
      Width = 97
      Height = 25
      Caption = 'DB connect'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 16
      Top = 56
      Width = 97
      Height = 25
      Caption = 'DB disconnect'
      TabOrder = 1
      OnClick = Button2Click
    end
  end
  object Button3: TButton
    Left = 16
    Top = 120
    Width = 177
    Height = 25
    Caption = 'xp_DeleteData'
    TabOrder = 1
    OnClick = Button3Click
  end
  object mmButton1: TmmButton
    Left = 16
    Top = 160
    Width = 177
    Height = 25
    Caption = 'xp_SendMMMachStateChanged'
    TabOrder = 2
    Visible = True
    OnClick = mmButton1Click
    AutoLabel.LabelPosition = lpLeft
  end
  object mmEdit1: TmmEdit
    Left = 168
    Top = 200
    Width = 25
    Height = 21
    Color = clWindow
    TabOrder = 3
    Text = '1'
    Visible = True
    AutoLabel.Control = mmLabel1
    AutoLabel.LabelPosition = lpLeft
    ReadOnlyColor = clInfoBk
    ShowMode = smNormal
  end
  object mmADOConnection1: TmmADOConnection
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=wetsrvmm;Use Procedure for Pre' +
      'pare=1;Auto Translate=True;Packet Size=4096;Workstation ID=WETNU' +
      'E2000;Use Encryption for Data=False;Tag with column collation wh' +
      'en possible=False'
    DefaultDatabase = 'MM_Winding'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    DBName = 'MM_Winding'
    SQLServerName = 'wetsrvmm'
    UserName = 'MMSystemSQL'
    Password = 'netpmek32'
    Left = 392
    Top = 32
  end
  object mmADOCommand1: TmmADOCommand
    Connection = mmADOConnection1
    ExecuteOptions = [eoAsyncExecute]
    Parameters = <>
    Left = 408
    Top = 120
  end
end
