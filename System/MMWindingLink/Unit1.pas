unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, mmLabel, mmEdit, ADODB, mmADOCommand, Db,
  mmADOConnection;

type
  TForm1 = class(TForm)
    GroupBox1: TGroupBox;
    Button1: TButton;
    Button2: TButton;
    Label1: TLabel;
    Label2: TLabel;
    Button3: TButton;
    Label3: TLabel;
    mmButton1: TmmButton;
    Label4: TmmLabel;
    mmEdit1: TmmEdit;
    mmLabel1: TmmLabel;
    mmADOConnection1: TmmADOConnection;
    mmADOCommand1: TmmADOCommand;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure mmButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function DBConnect:Boolean; cdecl; external 'MMWindingLink.dll';
  function DBDisconnect:Boolean; cdecl; external 'MMWindingLink.dll';
  function xp_DeleteMMData:Boolean; cdecl; external 'MMWindingLink.dll';
  function xp_SendMMMachStateChanged:Boolean; cdecl; external 'MMWindingLink.dll';

var
  Form1: TForm1;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{$R *.DFM}

//------------------------------------------------------------------------------
procedure TForm1.FormCreate(Sender: TObject);
begin
  Label1.Caption := '';
  Label2.Caption := '';
  Label3.Caption := '';
end;
//------------------------------------------------------------------------------
procedure TForm1.Button1Click(Sender: TObject);
begin
  Label2.Caption := '';
  if DBConnect then
    Label1.Caption := 'DB is connected'
  else
    Label1.Caption := 'DB is not connected';
end;
//------------------------------------------------------------------------------
procedure TForm1.Button2Click(Sender: TObject);
begin
  Label1.Caption := '';
  if DBDisconnect then
    Label2.Caption := 'DB is disconnected'
  else
    Label2.Caption := 'DB disconnect error';
end;
//------------------------------------------------------------------------------
procedure TForm1.FormDestroy(Sender: TObject);
begin
  DBDisconnect;
end;
//------------------------------------------------------------------------------
procedure TForm1.Button3Click(Sender: TObject);
var xSQLText: String;
begin
 if xp_DeleteMMData then
    Label3.Caption := 'xp_DeleteData ok'
 else
    Label3.Caption := 'xp_DeleteData n.i.o';
end;
//------------------------------------------------------------------------------

procedure TForm1.mmButton1Click(Sender: TObject);
begin
//  mmADOCommand1.CommandText := 'exec master.dbo.xp_SendMMMachStateChanged 7';
//  mmADOCommand1.Execute;
 if (xp_SendMMMachStateChanged StrToInt(mmEdit1.Text)) then
    Label4.Caption := 'xp_SendMMMachStateChanged ok'
 else
    Label4.Caption := 'xp_SendMMMachStateChanged n.i.o';
end;

end.
