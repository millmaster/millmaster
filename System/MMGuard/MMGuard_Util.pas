(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMGuard_Util.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: MMGuard
| Process(es)...: MMGuard
| Description...: Dienst des MillMasters, startet und stoppt die MM Prozesse
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 06.07.98 | 1.00 | Mg | Datei erstellt
| 09.10.00 | 1.10 | Mg | InstallMMService und RemoveMMService so erweitert,
|          |      |    | dass das Setup diese verwenden kann um den Service zu installieren.
| 26.07.01 | 1.11 |SDo | InstallMMService ohne Netlogon dependency -> MMGuard
|          |      |    | kann sonst als Standalone-Server nicht starten
|=============================================================================*)
unit MMGuard_Util;
interface

uses  Windows, SysUtils, NTApis, BaseGlobal, mmEventLog, LoepfeGlobal;

const
  cServiceName = cAddOnServiceName + ' MillMaster';
//------------------------------------------------------------------------------
 function InstallMMService ( aService  : String; aUserName : String; aPassword : String ) : boolean;
        // Parameter Beschreibung
        // aService  : Name mit Pfad des Services. z.B. d:\millmaster\system\mmguard.exe
        // aUserName : Name des Users unter dem der Service laeuft z.B. mmtest\mmsystemuser
        // aPassword : Passwort des Users
        // Wenn Service erfolgreich installiert result = true else result = false
//------------------------------------------------------------------------------
 function RemoveMMService () : boolean;
        // Wenn Service erfolgreich deinstalliert result = true else result = false
//------------------------------------------------------------------------------
 function NetMessageBufferSend ( aServername : PChar; aMsgname : PChar; aFromname : PChar;
                                 aBuf : array of byte; aBuflen : DWORD ): DWORD;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//--implementation of InstallService-------------------------------------------
 function InstallMMService ( aService : String; aUserName : String; aPassword : String ) : boolean;
 var
  xLog      : TEventLogWriter;
  xService,
  xSCManager: TSC_Handle;
 begin
  Result := true;
  xLog := TEventLogWriter.Create ( cEventLogClassForSubSystems , '.', ssMMGuard,
                                   cEventLogDefaultText[ttMMGuardMain], true );
  try
    xSCManager:= OpenSCManager(Nil, Nil, SC_MANAGER_ALL_ACCESS);
    If ( xSCManager<>0 ) and Result then Begin
      xService:= CreateService(  xSCManager,
                                 PChar( cServiceName ),
                                 PChar( cServiceName ),
                                 SERVICE_ALL_ACCESS,
                                 SERVICE_WIN32_OWN_PROCESS ,
                                 SERVICE_AUTO_START,
                                 SERVICE_ERROR_NORMAL,
                                 PChar ( aService ),
                                 Nil,
                                 Nil,
                                // 'Netlogon' + char ( 0 ) + 'EventLog' + char ( 0 ) + char ( 0 ) + char ( 0 ),  -> orginal
                                 'EventLog' + char ( 0 ) + char ( 0 ) + char ( 0 ),
                                 PChar ( aUserName ),
                                 PChar ( aPassword ) );
      if xService<>0 Then Begin
        xLog.Write( etSuccess, Format( '%s installed.', [cServiceName] ) );
        CloseServiceHandle( xService );
      end else  begin
        xLog.Write( etError,  Format( 'CreateService %s failed - %d', [aService, GetLastError] ) );
        Result := false;
      end;
      CloseServiceHandle( xSCManager )
    end else begin
     xLog.Write( etError, Format( 'OpenSCManager failed - %d', [GetLastError] ) );
     Result := false;
    end;
  except
    on e:Exception do begin
      Result := false;
      xLog.Write ( etError, 'Error in InstallMMService. Error : ' + e.message );
    end;
  end;
  xLog.Free;
 end;
//------------------------------------------------------------------------------
//--implementation of DeInstallService------------------------------------------
 function RemoveMMService () : boolean;
 var
  xLog      : TEventLogWriter;
  schService,
  schSCManager: TSC_Handle;
  xStatus : TService_Status;
 begin
  Result := true;
  xLog := TEventLogWriter.Create ( cEventLogClassForSubSystems , '.', ssMMGuard,
                                   cEventLogDefaultText[ttMMGuardMain], true );
  try
    schSCManager:= OpenSCManager(Nil, Nil, SC_MANAGER_ALL_ACCESS);
    if schSCManager<>0 then begin
      schService:= OpenService(schSCManager, PChar(cServiceName), SERVICE_ALL_ACCESS);
      if schService<>0 then begin
        { try to stop the service }
        if ControlService(schService, SERVICE_CONTROL_STOP, xStatus ) then begin
          // Sleep(1000);
          while QueryServiceStatus(schService, xStatus) do begin
            if xStatus.dwCurrentState = SERVICE_STOP_PENDING then begin
              Sleep(500)
            end else
              Break
          end;
          if xStatus.dwCurrentState = SERVICE_STOPPED  then
            xLog.Write ( etInformation,Format(#13#10'%s stopped.', [cServiceName]))
          else
            xLog.Write ( etError, Format(#13#10'%s failed to stop.', [cServiceName]))
        end;
        { now remove the service }
        if DeleteService(schService) then begin
          xLog.Write ( etInformation, Format('%s removed.', [cServiceName]));
        end else
          xLog.Write ( etError, Format('DeleteService failed - %d.', [GetLastError]));
          CloseServiceHandle(schService)
        end else
        xLog.Write ( etError, Format('OpenService failed - %d.', [GetLastError]));
        CloseServiceHandle(schSCManager)
    End Else
    xLog.Write ( etError, Format('OpenSCManager failed - %d.', [GetLastError]))
  except
    on e:Exception do begin
      Result := false;
      xLog.Write ( etError, 'Error in RemoveMMService. Error : ' + e.message );
    end;
  end;
  xLog.Free;
 end;
//------------------------------------------------------------------------------
 const
 NETAPI32 = 'NETAPI32.DLL';
 // API functions
 Function  NetMessageBufferSend;             External NETAPI32 Name 'NetMessageBufferSend';
end.
