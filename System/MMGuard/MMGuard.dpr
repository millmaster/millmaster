(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMGuard.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: MMGuard
| Process(es)...: MMGuard
| Description...: Dienst des MillMasters, startet und stoppt die MM Prozesse
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 06.07.98 | 1.00 | Mg | File created
| 03.03.99 | 1.01 | Mg | InstallService : cAddOnServiceName inserted
| 18.05.00 | 1.10 | Mg | New Parameter p to run MMguard as Process
| 09.10.00 | 1.20 | Mg | Wegen Problemen mit WriteLn nur noch MsgBoxes
| 12.10.02         LOK | Umbau ADO
| 04.11.02         LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 11.02.05         Wss | CodeSite Kontrolle f�r Debug Meldungen
|=============================================================================*)
program MMGuard; // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS,
  Windows,
  SysUtils,
  IPCCLASS,
  NTApis,
  BaseGlobal,
  Dialogs,
  ActiveX,
  LoepfeGlobal,
  MMGuard_Util in 'MMGuard_Util.pas',
  ServiceMain in 'ServiceMain.pas',
  mmRegistry,
  SettingsReader;

{$R *.RES}
{$R Version.RES}
type
  PServiceTable = ^TServiceTable;
  TServiceTable = Array[0..0] Of TService_Table_Entry;
var
  gDispatchTable: PServiceTable;
  g : integer;
  gPassword : String;
  // Debuging Variables
  gGuard    : TMMGuard;
  gGroup    : String;
  gSettings : TMMSettingsReader;
  xResult: HResult;
  i: integer;
begin
  gApplicationName := 'MMGuard';
  CodeSite.Enabled := CodeSiteEnabled;

  xResult := CoInitialize(nil);
  try
    // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
    // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
    if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
      raise Exception.Create('CoInitialize failed');
    case ParamCount of
    0 : begin // Ran the Service
          GetMem(gDispatchTable, 2 * SizeOf(TService_Table_Entry));
          try
            { DONE -oLOK -cDebug : Testcode wieder entfernen }
            // --> Debugcode
//            for i:= 0 to 15 * 10 do   // 15 Sekunden warten
//              sleep(100);
            // <-- End Debugcode

            g := 1;
            gDispatchTable[0].lpServiceName :=  cAddOnServiceName + ' MillMaster';
            gDispatchTable[0].lpServiceProc :=  @ServiceMainMMGuard;
            gDispatchTable[g].lpServiceName :=  Nil;
            gDispatchTable[g].lpServiceProc :=  Nil;
            if not StartServiceCtrlDispatcher(gDispatchTable^) Then
             ;// This event will be written in the log file by NT
          finally
            FreeMem(gDispatchTable, 2 * SizeOf(TService_Table_Entry))
          end;
          Halt ( 0 );
        end;
    1 : begin
          if ( ParamStr ( 1 ) = 'P' ) or ( ParamStr ( 1 ) = 'p' ) then begin
          // Run as Process
//          ShowMessage( 'MMGuard is used as Process, not as Service.' );
          try
            gSettings := TMMSettingsReader.Instance;
            gSettings.Init;
            gGroup := gSettings.value[ cUserGroup ];
          except
            on e:Exception do begin
              ShowMessage ( 'Read of Usergroup failed. ' + e.Message );
              Exit;
            end;
          end;
          try
            gGuard := TMMGuard.Create ( gGroup );
            if gGuard.Initialize then
              gGuard.Process
            else
              ShowMessage ( 'Initialisation of MMGuard failed. Use NT Event Log. ' );
          except
            on e:Exception do begin
              ShowMessage ( 'MMGuard failed. Error : ' + e.Message );
              Exit;
            end;
          end;
          end else begin
            // deinstall the service
            if ( ParamStr ( 1 ) = 'D' ) or ( ParamStr ( 1 ) = 'd' ) then begin
              if RemoveMMService ( ) then
                ShowMessage ( 'The Service ' + cServiceName + ' is deinstalled successfully' )
              else
                ShowMessage ( 'The Service ' + cServiceName + ' could not been deinstalled. Use NT Event Log.' );
            end else begin
               ShowMessage( 'Parameter is not correct!' );
            end;
          end;
        end;
    3,4 : begin // install the service
            if ( ParamStr ( 1 ) = 'I' ) or ( ParamStr ( 1 ) = 'i' ) then begin
              if ParamCount = 4 then
                gPassword := ParamStr ( 4 )
              else
                gPassword := '';
              if InstallMMService (ParamStr ( 2 ),
                                   ParamStr ( 3 ),
                                   gPassword )      then
                ShowMessage( 'The Service ' + cServiceName + ' is installed successfully.' )
              else
                ShowMessage( 'The Service ' + cServiceName + ' could not been installed. Use NT Event Log.' );
            end else begin
              ShowMessage( 'Wrong parameters.' );
            end;
          end;
    else
      ShowMessage( 'Wrong parameters.' );
    end;
  finally
    CoUninitialize;
  end;
end.


