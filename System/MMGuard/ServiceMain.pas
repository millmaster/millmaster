(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ServiceMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: MMGuard
| Process(es)...: MMGuard
| Description...: Dienst des MillMasters, startet und stoppt die MM Prozesse
| Info..........: -
| Develop.system: Compaq Deskpro Pentium Pro 200, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 07.07.98 | 1.00 | Mg | Datei erstellt
| 13.10.98 | 1.01 | Mg | New part of MMClient inserted
| 11.03.99 | 1.10 | Mg | New state init inserted.
|                        Terminator inserted.
|                        Parts of unknown processes inserted.
| 12.03.99 | 1.11 | Mg | SQL-Server available and reboot part inserted.
| 06.07.99 | 1.12 | Mg | Small changes
| 04.08.99 | 1.20 | Mg | Reboot of SQLServer by Restart of MM inserted.
|                        Substates of Restart deleted.
| 06.09.99 | 1.21 | Mg | gcSQLRebooted inserted and TBaseMMGuardThread inserted
| 15.09.99 | 1.22 | Mg | Initiator of StartMM and StopMM in Log
| 12.11.99 | 1.23 | Mg | Settings Read over MMSettings component
| 18.11.99 | 1.24 | Mg | In TProcList TErrorRec inserted
|          |      |    | In TMMGuard some errors more in log file
| 03.01.00 | 1.25 | Mg | Some try except inserted
| 09.03.00 | 1.26 | Mg | In all Threads main loop with while ( not terminated ) inserted
|          |      |    | ServiceMainMMGuard : new implementation
|          |      |    | Resume of all Threads and Sleep in Destroy of MMGuard inserted. To
|          |      |    | be shure the Threads can correct trminate.
|          |      |    | ServiceHndMMGuard : try except and local EventLog Handle inserted
| 13.03.00 | 1.27 | Mg | TBroadcaster in Mailslot moved
| 24.03.00 | 1.28 | Mg | cINITIALIZED_TIMEOUT from 25 to 45 seconds
| 25.05.00 | 1.29 | Mg | User Group in NT Log File when PipeServer is created
| 12.07.00 | 1.30 | Mg | StartStopLog deleted, only use of normal Log
| 13.07.00 | 1.31 | Mg | Bug Fix : TStopTimeoutResult inserted
| 21.03.01 | 1.32 |nue | TMMGuard.BroadcastActState: Error adds no more added to broadcast msg (cause of msg length>425Byte)
| 19.04.01 | 1.33 |Wss | TProcessList.CreateProc: call of CreateProcess changed to not use InheritedHandles
| 06.03.02 | 1.34 |LOK | MMGuard.Destroy:        Terminate Threads with .Terminated instead of .DoTerminate
|          | VCS  |    | TMMGuard.SendKillToAll: changed cKill_Timeout to 1 second if no Process is running
|          | 0.3  |    | ServiceMainMMGuard:     Only create logfile if it is needed (in case of exception)
|          |      |    | ServiceHndMMGuard:      Initialize the local var xCurrentState and send 0 to the service
|          |      |    |                         control Manager if the controlword is not SERVICE_CONTROL_STOP or     : begin
|          |      |    |                         SERVICE_CONTROL_SHUTDOWN
|          |      |    | ReportState:            the modul locale variable lServiceState holds the last known state
|          |      |    |                         of the service reported to the service control manager
|          |      |    | Replace '.Destroy' with '.Free'
|          |      |    |
|          |      |    | TMMGuardTimer.Execute:  ResetTime now in seconds
| 30.04.02 | 1.35 |LOK | force mBroadClient.Init to try 10 times to initialize the mailslot
|          | VCS  |    |
|          | 0.4  |    |
| 03.05.02 | 1.35 |LOK | Extended Error Message if write or init to Mailslot failed
|          | VCS  |    |
|          | 0.5  |    |
| 13.10.02 | 2.00 |LOK | ADO Version
| 04.11.02 |      |LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 10.12.02 |      |Wss | - Wenn bei gcRestartMM vom TimeHandler NoError in Record ist (Time changed)
|          |      |    |     dann wird kein Fehler protokolliert
|          |      |    |   - Timeout anpassen f�r die Zeit in der der Hauptthread schl�ft
|          |      |    |     -> msMMRestartStop, gcKillInProgress
|          |      |    |
| 16.12.02 |      |LOK | Beim Beenden des Services wurden bisher die Threads mit der Sequenz
|          |      |    |              xx.Terminate -- xx.Resume -- xx.Free
|          |      |    | freigegeben(TMMGuard.Destroy). In dieser Sequenz wird in der VCL mit 'WaitFor' auf
|          |      |    | das Ende der Threadfunktion gewartet. Daher konnte kein Timeout
|          |      |    | angegeben werden. Um diesen Umstand zu umgehen, wurde die Sequenz ge�ndert.
|          |      |    | Neu sieht die Sequenz folgendermassen aus:
|          |      |    |          xx.Terminate -- xx.Resume -- WaitForSingleObject(xx.Handle) -- xx.Free
|          |      |    | Mit 'WaitForSingleObject' kann nun ein Timeout angegeben werden und nach dem Ablauf dieses
|          |      |    | Timeouts der Thread mit TerminateThread hart "abgeschossen" werden. Im Normalfall
|          |      |    | passiert sonst nichts weiter.
|          |      |    |
|          |      |    | Ist das Symbol 'WriteStatusToLog' definiert, dann werden zus�tzliche Meldungen
|          |      |    | ins DebugLog geschrieben, die den Status und den Zeitpunkt von Status�nderung angeben
| 19.02.03 |      |LOK | InitiateRestart um einen Parameter erweitert (alt: TMMGuard.InitiateRestart;)
|          |      |    |   neu: TMMGuard.InitiateRestart(aTransmitter: TSubSystemTyp = ssMMGuard)
|          |      |    |   Wenn ein Restart von einer Anwendung beantragt wird, dann soll es egal sein wieviele
|          |      |    |   Automatische Restarts bereits erfolgt sind. Deshalb wird in diesem Fall kein Alarm gesetzt
| 25.09.03 |      |lok | in TestDBConnection() wird gepr�ft ob die DB Connection etabliert werden kann
|          |      |    |   Der Test wird solange ausgef�hrt bis die Connection etabliert ist,
|          |      |    |   oder das Timeout (cTryTimeInSeconds) abgelaufen ist.
|          |      |    |   Hintergrund:
|          |      |    |   Offenbar ist der SQLServer manchmal langsamer als der Millmaster System.
|          |      |    |   In diesem Fall ist zu verhindern, dass das System hochf�hrt, bevor eine Verbindung
|          |      |    |   zur Datenbank hergestellt werden kann.
| 01.09.04 |      |nue | Einbau mInstalledHandlers (Einf�gen der aktuellen NetHandler auf die DB (t_MMUParm)
| 26.11.04 |      |lok | in 'TMMGuard.ReadSettingsANDCreateAssistants' die Konstante 'cNumOfSubSystems' mit
|                      |   'ord(High(TSubSystemTyp))' ersetzt
| 25.05.05 |      |wss | TMMGuard.InitIPCs: Connect konnte fehlschlagen, wenn �ber Servicecontrol der Stop/Start Button
                         das System neu gestartet wird. Beim Connect wurde vermutlich noch eine alte Instanz der Main-Pipe
                         gefunden, welche noch im "Beenden" war "pipe has been ended". Durch das nochmalige Probieren mit
                         etwas warten funktioniert es nun.
| 07.04.10 |      |nue | Erh�hung cMaxStarts von 2 auf 10 wegen Startproblemen von MM bei neuen Prozessoren
|=============================================================================*)
{$DEFINE _WriteStatusToLog}

unit ServiceMain;
interface
uses mmThread, mmStringList, Windows, MMGuard_Util, classes, IPCClass, BaseGlobal,
  mmEventLog, SysUtils, Mailslot, NTApis, extctrls, shellapi, AdoDBAccess,
  LoepfeGlobal, activeX;
//..............................................................................
procedure ServiceMainMMGuard(aNumArgs: DWord; var Args: PChar); StdCall;
//..............................................................................
const
  cMMGuardVersion        = 'V 2.00';
  cTimeOut               = 2000; // timeout for IPC Server
  cServer                = '.';
  cMaxProc               = 15;
  cProcListMuxTimeout    = 2000;
  cMaxRestarts           = 10;     //Nue 07.4.2010: vorher 2 (nachher mal mit 10 )
  cProcessCreateWaitTime = 100;
  cUnknownFlag           = 'UNKNOWN_PROCESS';
  cSqlServiceName        = 'MSSQLServer';
  cSQLAgentName          = 'SQLServerAgent';
  cOLAPServiceName       = 'MSSQLServerOLAPService';

      // Timeouts by waiting of response from processes in seconds
  cNO_TIMEOUT          = 0;
  cAVAILABLE_TIMEOUT   = 90; // after this time all processes has to send the available command
  cCONNECT_TIMEOUT     = 25; // after this time all processes has to send the connected command
  cINITIALIZED_TIMEOUT = 45; // after this time all processes has to send the initialized command
  cRUN_TIMEOUT         = 10; // after this time all processes has to send the run command
  cKILL_TIMEOUT        = 15; // after this time all processes has to be killed

      // SQL-Server timeouts
  cSQLSERVER_STARTUP_TIMEOUT = 40; // timeout to start the sqlserver
  cSQLSERVER_REBOOT_TIMEOUT  = 30; // timeout for rebooting SQL-Server

      // reset and restart time in seconds
  cDEFAULT_ERROR_RESET_TIME  = 20000; // after this time an restart is vergotton
  cDEFAULT_RESTART_TIME      = 1800; // after this time MMGuard tries to restart the MillMaster
  cREBOOT_WAIT_TIME          = 10; // displais a msgbox durin this time by reboot
//..............................................................................
type
//..............................................................................
  TStopTimeoutResult = (
    stAllProcessesTerminated, // All Processes are terminated
    stTerminationPending, // Termination of processes is pending
    stCanNotTerminateProcess); // Processes can not be terminated
//..............................................................................
      // States of MMGuard
  TMMGuardState =
    (msMMSleep, // MillMaster is stopped, MMGuard is running
    msMMStartCreate, // The processes are created
    msMMStartAvailable, // The processes are created and got a msg. back
    msMMStartConnected, // The processes has done the connects
    msMMStartInitialized, // The processes has finished the initializing
    msMMRun, // MillMaster is running
    msMMRestartStop, // The processes has to be stopped
    msMMRestartRebootSQLServer, // Waiting while SQL-Server is rebooted
    msMMStop, // MillMaster is stopping
    msMMStopService, // Stop of MMGuard in progress
    msMMAlarm); // Start of MillMaster unpossible
//..............................................................................
      // Element of process list
  TProcessInfo = record
    Typ: TSubSystemTyp; // Typ of the MM process like ssJobHandler
    Name: string; // Name and path of the process for startup
    Unknown: boolean; // If the process is not inherited from BaseMain
    Info: TProcessInformation; // NT process information record
    State: TMMProcessState; // State of the process
    Port: TIPCClient; // Port to the process for writing
  end;
//..............................................................................
      // Array of process list
  TProcArray = array[0..cMaxProc] of TProcessInfo;
//..............................................................................
//    The class of TProcessList administrates the processes
  TProcessList = class(TObject)
  private
    mMux: THandle; // Mutex for using to protect the process list
    mList: TProcArray; // List of all processes
    fNumOfProc: integer; // Number of processes to administrate
    fError: TErrorRec;
  public
    constructor Create(aProcArr: TProcArray; aNumOfProc: integer); virtual; // The Subsystem and the Name has to be initialized
    destructor Destroy; override;
    function CreateProc(aSubSystem: TSubsystemTyp): boolean; // The Process in aSubSystem will be created
    function WaitForProcExit(var aProcTyp: TSubSystemTyp): boolean; // This function is used by the WatchDog, the function returns when one process terminates
    function GetProcState(aSubSystem: TSubSystemTyp): TMMProcessState;
    procedure SetProcState(aSubSystem: TSubSystemTyp; aProcState: TMMProcessState);
    procedure SetProcHandle(aSubSystem: TSubSystemTyp; aProcHandle: THandle);
    function GetProcHandle(aSubSystem: TSubSystemTyp): THandle;
    function ConnectProc(aSubSystem: TSubSystemTyp): boolean; // The Port to the process will be connected
    function WriteToProcess(aSubSystem: TSubSystemTyp; aMsg: TMMGuardRec): boolean; // The message aMsg will be written to the aSubSystem process
    function ProcessByIndex(xIndex: integer): TSubSystemTyp; // This function returns the SysbSysten typ with the index
    property Error: TErrorRec read fError;
    property NumOfProc: integer read fNumOfProc;
  end;
//..............................................................................
  TBaseMMGuardThread = class(TmmThread)
  private
    mLog: TEventLogWriter; // Handle to the log system
    mMMGuardWriter: TIPCClient; // Handle to the pipe of MMGuard
  protected
  public
    constructor Create(aSuspended: boolean);
    destructor Destroy; override;
  end;
//..............................................................................
//    TWatchDog waits until one process terminates
  TWatchDog = class(TBaseMMGuardThread)
  private
    mProcList: TProcessList; // Only use of WaitForProcExit, the same
  protected // instance like process list of TMMGuard
    procedure Execute; override;
  public
    constructor Create(aProcList: TProcessList); virtual;
    destructor Destroy; override;
  end;
//..............................................................................
//    TGuardTimer implements a timer
  TMMGuardTimer = class(TBaseMMGuardThread)
  private
    mAlertTime: integer; // After this time the beep sound will bee heard if state is alert
    fResetTime: integer; // After this time the restart counter willbe set on 0
    fRestartTime: integer; // After this time a restart will be initialized if state=alert
    fTimeout: integer; // Timeout by waiting on process responses
    fTimeoutCount: integer; // Number of timeout
    procedure SetTimeout(aTimeout: integer);
  protected
    procedure Execute; override;
  public
    constructor Create(aAlertTime: integer); virtual;
    destructor Destroy; override;
    property Timeout: integer read fTimeout write SetTimeout; // by write of Timeout TimeoutCount will be NULL
    property TimeoutCount: integer read fTimeout write fTimeout;
    property ResetTime: integer read fResetTime write fResetTime;
    property RestartTime: integer read fRestartTime write fRestartTime;
  end;
//..............................................................................
      // used to terminate the unknown and not responding processes
  TTerminator = class(TBaseMMGuardThread)
  private
    mProcList: TProcessList; // Only used to get the process handles
  protected
    procedure Execute; override;
  public
    constructor Create(aProcList: TProcessList); virtual;
    destructor Destroy; override;
  end;
//..............................................................................
      // used to terminate the unknown and not responding processes
  TSQLServerRebooter = class(TBaseMMGuardThread)
  private
    mSQLServiceName: string;
    mSQLAgentName: string;
    mOLAPServiceName: string;
    fDoReboot: boolean;
    function IsServiceRunning(aService: TSC_Handle): boolean;
    function StopService(aService: TSC_Handle): boolean; // this function does not return until the service is stopped
    function StartService(aService: TSC_Handle): boolean;
    function RebootSQLServer: boolean; // result is true if SQL-Server is rebooted successfuly
  protected
    procedure Execute; override;
  public
    constructor Create(aSQLServiceName, aSQLAgentName, aOLAPServiceName: string);
    destructor Destroy; override;
    property DoReboot: boolean read fDoReboot write fDoReboot;
  end;
//..............................................................................
//    TMMGuard implements the MillMaster service
  TMMGuard = class(TObject)
  private
    mReadPort: TIPCServer; // Port of MMGuard ( pipe )
    mMsg: TMMGuardRec; // Message from the ReadPort
    mWatchDog: TWatchDog; // Instance of TWatchDog
    mTimer: TMMGuardTimer; // Instance of TGuardTimer
    mTerminator: TTerminator; // Used to terminate unknown and not responding processes
    mSQLRebooter: TSQLServerRebooter; // Used to reboot SQLServer
    mEventLog: TEventLogWriter; // Event log for errors ec.
    mDebugLog: TEventLogWriter; // Event log for debug mode
    mStartStopLog: TEventLogWriter; // Event log for start and stopp of MillMaster
    mMMGuardWriter: TIPCClient; // Handle to write to the read port
    mBroadClient: TBroadcaster; // Handle to broadcast mailbox of MMClient
    mProcList: TProcessList; // Instance of TProcList
    mMMState: TMMGuardState; // State of MMGuard
    mNumStarts: integer; // Num of starts since the last manual start
    mStartTime: TDateTime; // Time of last MM start
    mUserStart: boolean; // If true an user stated the MM else it was an autostart
    mStartMMIm: boolean; // If true the MM will be started imediatly after start of server
    mAlertSound: boolean; // Sound in alert state
    mDebug: boolean; // Debug mode on/off
    mInstalledHandlers: string; // Enth�lt alle installierten NetHandler in der Form (NH1.EXE,NH2.EXE)
        // functions for initialisation
    function InitIPCs: boolean;
    function ReadSettingsANDCreateAssistants(var aProcArr: TProcArray; var aNumOfProc: integer): boolean;
        // miscellaneous function
    function ProcessMsg: boolean; // This function implements the state event machine of MMGuard
    function CreateAllProcesses: boolean;
    procedure TerminateAllProcesses; // only used by unknown and not responding processes
    procedure SetState(aState: TMMGuardState);
    function WritToSelf(aMsg: TMMGuardCommand): boolean;
    procedure BroadcastActState;
    procedure InitiateRestart(aTransmitter: TSubSystemTyp = ssMMGuard);
    procedure InitiateStopService;
    procedure InitiateRebootServer;
        // Log writing
    procedure Log(aEvent: TEventType; aText: string);
    procedure DebugLog(aText: string);
        // V1.30 procedure StartStopLog ( aText : String );
        // procedures used in start and restart state
    function ProcessAvailableMsg(aState: TMMGuardState): boolean;
    function ProcessConnectedMsg(aState: TMMGuardState): boolean;
    function ProcessInitializedMsg(aState: TMMGuardState): boolean;
    function ProcessRunMsg: boolean;
    procedure ProcessRebootSQLServer;
    procedure ProcessRestart;
    function ProcessStopTimeout(aRestart: boolean): TStopTimeoutResult;
    function GetFailedProcesses(aExpProcessStatus: TMMProcessState): string; // the Result is a String containing the processes wich not have the status like aExpProcessStatus
    function AllProcAvailable: boolean;
    function AllProcConnected: boolean;
    function AllProcInitialized: boolean;
    function AllProcRun: boolean;
    function AllProcTerminated: boolean;
    function IsSQLServerAvailable: boolean;
    function ConnectAll: boolean;
    function SendDoConnectToAll: boolean;
    function SendDoInitToAll: boolean;
    function SendDoRunToAll: boolean;
    procedure SendKillToAll;
    procedure SetAndSendDebugToAll(aDebugMode: Byte);
    procedure WriteLogRestartFromProcess;
  public
    constructor Create(aUserOrGroup: string); virtual;
    destructor Destroy; override;
    function Initialize: boolean;
    procedure Process;
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf, TypInfo, mmRegistry, SettingsReader, ADODB_TLB;
var
  // this global variable is used because the Windows NT service model
  gStateHandle: TService_Status_Handle;
  // Holds the last state of the service reported to the service control manager
  lServiceState: DWord = 0;

//--implementation of TProcessList----------------------------------------------
//------------------------------------------------------------------------------

constructor TProcessList.Create(aProcArr: TProcArray; aNumOfProc: integer);
var x: integer;
begin
  inherited Create;
  mList := aProcArr;
  fNumOfProc := aNumOfProc;
  mMux := CreateMutex(nil, false, nil);
  if mMux = INVALID_HANDLE_VALUE then
    fError := SetError(getLastError, etNTError, 'TProcessList.Create failed');
  if fNumOfProc > cMaxProc then fNumOfProc := cMaxProc;
  for x := 0 to fNumOfProc - 1 do begin
    if not mList[x].Unknown then
      mList[x].Port := TIPCClient.Create('.', cChannelNames[cSubSystemDefinitions[mList[x].Typ].ThreadTyp]);
    mList[x].State := psNotAvailable;
  end;
  fError := SetError(NO_ERROR, etNOError, ''); ;
end;
//------------------------------------------------------------------------------

destructor TProcessList.Destroy;
var x: integer;
begin
  for x := 0 to fNumOfProc - 1 do begin
    mList[x].Port.Free;
  end;
  inherited Destroy;
end;
//------------------------------------------------------------------------------

function TProcessList.CreateProc(aSubSystem: TSubSystemTyp): boolean;
var x: integer;
  xStartupInfo: TStartupInfo;
  xSecurityAttrib: TSecurityAttributes;
begin
  Result := true;

  xSecurityAttrib.lpSecurityDescriptor := nil;
  xSecurityAttrib.bInheritHandle := true;
  xSecurityAttrib.nLength := sizeof(xSecurityAttrib);

  GetStartupInfo(xStartupInfo);
  xStartupInfo.wShowWindow := SW_SHOWMINIMIZED;

  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do
    begin
      if mList[x].Typ = aSubSystem then begin
        if CreateProcess(PChar(mList[x].Name),
          nil,
          @xSecurityAttrib,
          nil, False,
          DETACHED_PROCESS or NORMAL_PRIORITY_CLASS,
          nil, nil,
          xStartupInfo,
          mList[x].Info) then begin
          if mList[x].Unknown then
            mList[x].State := psRun
          else
            mList[x].State := psCreated;
        end else begin
          fError := SetError(getLastError, etNTError, 'TProcessList.CreateProc failed');
          Result := false;
        end;
        ReleaseMutex(mMux);
        Exit;
      end;
    end;
    ReleaseMutex(mMux);
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.CreateProc failed by waiting of single object');
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TProcessList.WaitForProcExit(var aProcTyp: TSubSystemTyp): boolean;
var x: integer;
  xProcNumber: DWord;
  xHandleArray: TWOHandleArray;
begin
  Result := true;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
      // Fill the handle array
    for x := 0 to fNumOfProc - 1 do
      xHandleArray[x] := mList[x].Info.hProcess;
    ReleaseMutex(mMux);
      // Now wait for an exit of a process
    xProcNumber := WaitForMultipleObjects(fNumOfProc, @xHandleArray, false, INFINITE);
    if xProcNumber <> WAIT_FAILED then begin
        // get the name of the process
      try
        aProcTyp := mList[xProcNumber].Typ;
      except
        Result := false;
        fError := SetError(ERROR_INVALID_HANDLE, etMMError, 'TProcessList.WaitForProcExit : Read of Process type failed');
      end;
    end else begin
      Result := false;
      fError := SetError(getLastError, etNTError, 'TProcessList.WaitForProcExit : Wait for multiple object failed');
    end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.WaitForProcExit : Wait for single object failed');
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TProcessList.GetProcState(aSubSystem: TSubSystemTyp): TMMProcessState;
var x: integer;
  xExitCode: DWord;
begin
  Result := psNotAvailable;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do begin
      if mList[x].Typ = aSubSystem then begin
        Result := mList[x].State;
          // maybe the process got exit since the last state was set, so let's check the state
        if (Result <> psNotAvailable) and (Result <> psDead) then begin
            // the process should be at work
          if GetExitCodeProcess(mList[x].Info.hProcess, xExitCode) then begin
            if xExitCode <> STILL_ACTIVE then begin
              mList[x].State := psDead;
              Result := psDead;
            end;
          end else begin
            mList[x].State := psNotAvailable;
            Result := psNotAvailable;
          end;
        end;
        ReleaseMutex(mMux);
        Exit;
      end;
    end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.GetProcState : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

procedure TProcessList.SetProcHandle(aSubSystem: TSubSystemTyp; aProcHandle: THandle);
var x: integer;
begin
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do
      if mList[x].Typ = aSubSystem then begin
        mList[x].Info.hProcess := aProcHandle;
        Break;
      end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.SetProcHandle : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

function TProcessList.GetProcHandle(aSubSystem: TSubSystemTyp): THandle;
var x: integer;
begin
  Result := INVALID_HANDLE_VALUE;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do
      if mList[x].Typ = aSubSystem then begin
        Result := mList[x].Info.hProcess;
        Break;
      end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.GetProcHandle : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

procedure TProcessList.SetProcState(aSubSystem: TSubSystemTyp; aProcState: TMMProcessState);
var x: integer;
begin
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do
      if mList[x].Typ = aSubSystem then
        mList[x].State := aProcState;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.SetProcState : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

function TProcessList.ConnectProc(aSubSystem: TSubSystemTyp): boolean;
var x: integer;
begin
  Result := false;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do begin
      if (mList[x].Typ = aSubSystem) and (not mList[x].Unknown) then begin
        Result := mList[x].Port.Connect;
        if not Result then
          fError := SetError(mList[x].Port.Error, etNTError, 'TProcessList.ConnectProc : Connect failed. Process : ' + cApplicationNames[aSubSystem]);
        ReleaseMutex(mMux);
        Exit;
      end;
      if (mList[x].Typ = aSubSystem) and mList[x].Unknown then begin
        Result := true;
        ReleaseMutex(mMux);
        Exit;
      end;
    end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.ConnectProc : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

function TProcessList.WriteToProcess(aSubSystem: TSubSystemTyp; aMsg: TMMGuardRec): boolean;
var x: integer;
begin
  Result := false;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    for x := 0 to fNumOfProc - 1 do begin
      if (mList[x].Typ = aSubSystem) and (not mList[x].Unknown) then begin
        Result := mList[x].Port.Write(@aMsg, sizeof(aMsg));
        if not Result then
          fError := SetError(mList[x].Port.Error, etNTError, 'TProcessList.WriteToProcess : Write failed. Process : Process : ' + cApplicationNames[aSubSystem]);
        ReleaseMutex(mMux);
        Exit;
      end;
      if (mList[x].Typ = aSubSystem) and mList[x].Unknown then begin
        Result := true;
        ReleaseMutex(mMux);
        Exit;
      end;
    end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.WriteToProcess : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;
//------------------------------------------------------------------------------

function TProcessList.ProcessByIndex(xIndex: integer): TSubSystemTyp;
begin
  Result := mList[0].Typ;
  if WaitForSingleObject(mMux, cProcListMuxTimeout) <> WAIT_FAILED then begin
    if (xIndex >= 0) and (xIndex <= fNumOfProc) then begin
      Result := mList[xIndex].Typ;
    end else begin
      fError := SetError(ERROR_BAD_ARGUMENTS, etMMError, 'TProcessList.ProcessByIndex : Parameter out of range');
    end;
  end else begin
    fError := SetError(getLastError, etNTError, 'TProcessList.ProcessByIndex : Wait for single object failed');
  end;
  ReleaseMutex(mMux);
end;

//--implementation of TBaseMMGuardThread----------------------------------------
//------------------------------------------------------------------------------

constructor TBaseMMGuardThread.Create(aSuspended: boolean);
begin
  inherited Create(aSuspended);
  mMMGuardWriter := TIPCClient.Create('.', cChannelNames[ttMMGuardMain]);
  mLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cServer, ssMMGuard, cEventLogDefaultText[ttMMGuardMain], true);
end;
//------------------------------------------------------------------------------

destructor TBaseMMGuardThread.Destroy;
begin
  mMMGuardWriter.Free;
  mLog.Free;
  inherited Destroy;
end;

//--implementation of TWatchDog-------------------------------------------------
//------------------------------------------------------------------------------

constructor TWatchDog.Create(aProcList: TProcessList);
begin
  inherited Create(true);
  mProcList := aProcList;
end;
//------------------------------------------------------------------------------

destructor TWatchDog.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TWatchDog.Execute;
var xProc: TSubSystemTyp;
  xMsg: TMMGuardRec;
begin
  while (not Terminated) do begin
    try
        // wait until any process of the MM terminates
      if mProcList.WaitForProcExit(xProc) then begin
        xMsg.MsgTyp := gcProcessStoped;
        xMsg.Transmitter := xProc;
        xMsg.Error.ErrorTyp := etNoError;
        xMsg.Error.Error := NO_ERROR;
        xMsg.ThreadTyp := ttMMGuardWatchDog;
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then begin
          mLog.Write(etError, 'Write by process exit failed : ' + FormatErrorText(mMMGuardWriter.Error));
        end;
      end else begin
        mLog.Write(etError, 'Wait of process exit failed : ' + FormatMMErrorText(mProcList.Error));
      end;
      Sleep(1000); // Sleep is necessary to terminate the Write to MMGuard
      if not Terminated then
        Suspend; // Suspend until the MM ist started again
    except
      on e: Exception do begin
        mLog.Write(etError, 'Error in TWatchDog.Execute. ' + e.Message);
        Sleep(1000);
        Suspend;
      end;
    end;
  end; (* loop *)
end;

//--implementatio of TGuardTimer-----------------------------------------------------
//------------------------------------------------------------------------------

constructor TMMGuardTimer.Create(aAlertTime: integer);
begin
  inherited Create(false);
  fResetTime := cDEFAULT_ERROR_RESET_TIME;
  fRestartTime := cDEFAULT_RESTART_TIME;
  fTimeout := cNO_TIMEOUT;
  fTimeoutCount := 0;
  mAlertTime := aAlertTime;
end;
//------------------------------------------------------------------------------

destructor TMMGuardTimer.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TMMGuardTimer.SetTimeout(aTimeout: integer);
begin
  fTimeout := aTimeout;
  fTimeoutCount := 0;
{$IFDEF WriteStatusToLog}
      (* Wenn der Status mit dem Timeout ins Log geschrieben wird, dann kann unter Umst�nden besser
         erruiert werden wo ein Fehler im Ablauf auftritt *)
  WriteToEventLogDebug('Timeout: ' + IntToStr(aTimeout));
{$ENDIF}
end;
//------------------------------------------------------------------------------

procedure TMMGuardTimer.Execute;
var xResetTime: integer;
  xRestartTime: integer;
  xAlertCounter: integer;
  xMsg: TMMGuardRec;
begin
  if not mMMGuardWriter.Connect then begin
    mLog.Write(etError, 'Connect from Timer to MMGuard failed. Error : ' +
      IntToStr(mMMGuardWriter.Error));
  end;
    // The Reset Time is written in seconds in the registry. MMConfiguration allows the user to
    // specify this time in minutes and calculates the seconds for the registry entry
    // The reset time is the time after that Millmaster "forgets" the quantity
    // of the previous automatic (re)starts. (In the past this time was specified in hours)
  xResetTime := ResetTime; // * 3600;
  xRestartTime := RestartTime * 60;
  xAlertCounter := 1;

  while (not Terminated) do begin
    try
      Sleep(1000);
      inc(xAlertCounter);
      if xAlertCounter = mAlertTime then begin
        xAlertCounter := 1;
        xMsg.MsgTyp := gcAlert;
        xMsg.Transmitter := ssMMGuard;
        xMsg.Error.ErrorTyp := etNoError;
        xMsg.Error.Error := NO_ERROR;
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then begin
          mLog.Write(etError, 'Write by Alert failed. Error : ' +
            IntToStr(mMMGuardWriter.Error));
        end;
      end;

      if Timeout = 1 then begin
        Timeout := cNO_TIMEOUT;
        xMsg.MsgTyp := gcTimeout;
        xMsg.Transmitter := ssMMGuard;
        xMsg.Count := TimeoutCount;
        TimeoutCount := 0;
        xMsg.Error.ErrorTyp := etNoError;
        xMsg.Error.Error := NO_ERROR;
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then begin
          mLog.Write(etError, 'Write by Timeout failed. Error : ' +
            IntToStr(mMMGuardWriter.Error));
        end;
      end;

      if Timeout > 1 then
        dec(fTimeout);

      if xResetTime < 1 then begin
          // In the past this time was specified in hours
        xResetTime := ResetTime; // * 3600;
        xMsg.MsgTyp := gcResetTimeout;
        xMsg.Transmitter := ssMMGuard;
        xMsg.Error.ErrorTyp := etNoError;
        xMsg.Error.Error := NO_ERROR;
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then begin
          mLog.Write(etError, 'Write by ResetTime failed. Error : ' +
            IntToStr(mMMGuardWriter.Error));
        end;
      end;

      if xRestartTime < 1 then begin
        xRestartTime := RestartTime * 60;
        xMsg.MsgTyp := gcAlertRestart;
        xMsg.Transmitter := ssMMGuard;
        xMsg.Error.ErrorTyp := etNoError;
        xMsg.Error.Error := NO_ERROR;
        xMsg.ComputerAndUser := gMMHost + '/MMGuard';
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then begin
          mLog.Write(etError, 'Write by RestartTime failed. Error : ' +
            IntToStr(mMMGuardWriter.Error));
        end;
      end;

      dec(xRestartTime);
      dec(xResetTime);
    except
      on e: Exception do begin
        mLog.Write(etError, 'Error in TMMGuardTimer.Execute. ' + e.Message);
      end;
    end;
  end;
end;

//--implementation of TTerminator-----------------------------------------------
//------------------------------------------------------------------------------

constructor TTerminator.Create(aProcList: TProcessList);
begin
  inherited Create(true);
  mProcList := aProcList;
end;
//------------------------------------------------------------------------------

destructor TTerminator.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------

procedure TTerminator.Execute;
var x: integer;
  xProcessHandle: THandle;
  xThreadHandle: THandle;
  xThread: DWORD;
  xMsg: TMMGuardRec;

  procedure Killer; stdcall;
  var xExitCode: CARDINAL;
  begin
    Beep;
    ExitProcess(xExitCode);
  end;

begin
  while (not Terminated) do begin
    try
      for x := 0 to mProcList.NumOfProc - 1 do begin
        if ((mProcList.GetProcState(mProcList.ProcessByIndex(x)) <> psDead) and
          (mProcList.GetProcState(mProcList.ProcessByIndex(x)) <> psNotAvailable)) then begin
            // the process is still alive -> kill them
          xProcessHandle := mProcList.GetProcHandle(mProcList.ProcessByIndex(x));
          xThreadHandle := 0;
          TerminateProcess(xProcessHandle, 0);
{           xThreadHandle := CreateRemoteThread ( xProcessHandle,
                                                Nil,
                                                0,
                                                @Killer,
                                                Nil,
                                                0,
                                                xThread );
           if xThreadHandle = 0 then begin
             Beep;
           end;
           xThread := GetLastError;
 }
        end;
      end;
      xMsg.MsgTyp := gcAllProcessStoped;
      xMsg.Transmitter := ssMMGuard;
      mMMGuardWriter.Write(@xMsg, sizeof(xMsg));
      if not Terminated then
        Suspend;
    except
      on e: Exception do begin
        mLog.Write(etError, 'Error in Terminateor.Execute. ' + e.Message);
      end;
    end;
  end;
end;

//--imlementation of TSQLServerRebooter-----------------------------------------
//------------------------------------------------------------------------------

constructor TSQLServerRebooter.Create(aSQLServiceName, aSQLAgentName, aOLAPServiceName: string);
begin
  inherited Create(true);
  mSQLServiceName := aSQLServiceName;
  mSQLAgentName := aSQLAgentName;
  mOLAPServiceName := aOLAPServiceName;
  fDoReboot := true;
end;
//------------------------------------------------------------------------------

destructor TSQLServerRebooter.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------

function TSQLServerRebooter.IsServiceRunning(aService: TSC_Handle): boolean;
var xStatus: TService_Status;
begin
  QueryServiceStatus(aService, xStatus);
  Result := (xStatus.dwCurrentState = SERVICE_RUNNING);
end;
//------------------------------------------------------------------------------

function TSQLServerRebooter.StopService(aService: TSC_Handle): boolean;
var xNumSec: integer;
  xStatus: TService_Status;
begin
  Result := true;
    // first check service status maybe the service is already stopped
  if not QueryServiceStatus(aService, xStatus) then begin
    Result := false;
    Exit;
  end;
  if xStatus.dwCurrentState = SERVICE_STOPPED then Exit;
  if ControlService(aService, SERVICE_CONTROL_STOP, xStatus) then begin
    xNumSec := 0;
      // wait until the service is stopped
    repeat
      inc(xNumSec);
      Sleep(1000);
      if not QueryServiceStatus(aService, xStatus) then begin
        Result := false;
        break;
      end;
    until (xStatus.dwCurrentState = SERVICE_STOPPED) or (xNumSec > 8)
  end else begin
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TSQLServerRebooter.StartService(aService: TSC_Handle): boolean;
var xStr: PChar;
begin
  Result := NTApis.StartService(aService, 0, xStr);
end;
//------------------------------------------------------------------------------

function TSQLServerRebooter.RebootSQLServer: boolean;
var xSCManager,
  xSQLService,
    xSQLAgentService,
    xOLAPService: TSC_Handle;
begin
  Result := true;
  xSCManager := 0;
  xSQLService := 0;
  xSQLAgentService := 0;
  xOLAPService := 0;
  xSCManager := OpenSCManager(nil, nil, SC_MANAGER_ALL_ACCESS);
  if xSCManager <> 0 then begin
    xSQLService := OpenService(xSCManager, PChar(mSqlServiceName), SERVICE_ALL_ACCESS);
    xSQLAgentService := OpenService(xSCManager, PChar(mSQLAgentName), SERVICE_ALL_ACCESS);
    xOLAPService := OpenService(xSCManager, PChar(mOLAPServiceName), SERVICE_ALL_ACCESS);
    if (xSQLService = 0) then begin
      mLog.Write(etWarning, 'TSQLServerRebooter : can not open Service. ' + FormatErrorText(getLastError));
      Result := false;
    end;
    if Result then begin
        // Stopp all SQL services
      if xOLAPService <> 0 then
        if not StopService(xOLAPService) then
          mLog.Write(etWarning, 'TSQLServerRebooter : can not stop OLAP Service. ' + FormatErrorText(getLastError));
      if xSQLAgentService <> 0 then
        if not StopService(xSQLAgentService) then
          mLog.Write(etWarning, 'TSQLServerRebooter : can not stop SQLAgent Service. ' + FormatErrorText(getLastError));
      if not StopService(xSQLService) then begin
        mLog.Write(etWarning, 'TSQLServerRebooter : can not stop SQL Service. ' + FormatErrorText(getLastError));
        Result := false;
      end;

      if Result then begin
          // Start all sql services
        if not StartService(xSQLService) then begin
          mLog.Write(etError, 'TSQLServerRebooter : can not start SQL Service. ' + FormatErrorText(getLastError));
          Result := false;
        end;
        if xSQLAgentService <> 0 then
          if not StartService(xSQLAgentService) then
            mLog.Write(etWarning, 'TSQLServerRebooter : can not start SQLAgent Service. ' + FormatErrorText(getLastError));
        if xOLAPService <> 0 then
          if not StartService(xOLAPService) then
            mLog.Write(etWarning, 'TSQLServerRebooter : can not start OLAP Service. ' + FormatErrorText(getLastError));
      end;
    end;
    CloseServiceHandle(xSQLService);
    CloseServiceHandle(xSQLAgentService);
    CloseServiceHandle(xOLAPService);
    CloseServiceHandle(xSCManager);
  end else begin
    Result := false;
    mLog.Write(etWarning, 'TSQLServerRebooter : can not open SCManager. ' + FormatErrorText(getLastError));
  end;
end;
//------------------------------------------------------------------------------

procedure TSQLServerRebooter.Execute;
var xMsg: TMMGuardRec;
  xError: boolean;
begin
  while (not Terminated) do begin
    try
      xError := false;
      if DoReboot then
        xError := not RebootSQLServer;
        // Do also in case of DoReboot = false send a Message to MMGuard
      if not xError then begin
        xMsg.MsgTyp := gcSQLRebooted;
        xMsg.Transmitter := ssMMGuard;
        xMsg.Error := SetError(NO_ERROR, etNoError, '');
        if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then
          mLog.Write(etError, 'TSQLServerRebooter : can not write to MMGuard.' + FormatErrorText(mMMGuardWriter.Error));
      end;
      if not Terminated then
        Suspend;
    except
      on e: Exception do begin
        mLog.Write(etError, 'Error in TSQLServerRebooter.Execute. ' + e.Message);
      end;
    end;

  end;
end;

//--implementation of TMMGuard--------------------------------------------------
//------------------------------------------------------------------------------

constructor TMMGuard.Create(aUserOrGroup: string);
begin
  inherited Create;
  mBroadClient := nil;
  mEventLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cServer, ssMMGuard,
    cEventLogDefaultText[ttMMGuardMain], true);
  mDebugLog := TEventLogWriter.Create(cEventLogClassForSubSystemsDebug, cServer, ssMMGuard,
    cEventLogDefaultText[ttMMGuardMain], true);
  mStartStopLog := TEventLogWriter.Create(cEventLogClassForMMGuard, cServer, ssMMGuard,
    cEventLogDefaultText[ttMMGuardMain], true);
  mReadPort := TIPCServer.Create(cChannelNames[ttMMGuardMain], aUserOrGroup, sizeof(TMMGuardRec),
    cTimeOut, NMPWAIT_USE_DEFAULT_WAIT);
  mEventLog.Write(etInformation, 'Access control list will be enlarged with user/group : ' + aUserOrGroup);
  mMMGuardWriter := TIPCClient.Create('.', cChannelNames[ttMMGuardMain]);
  mNumStarts := 1;
  mDebug := false;
  mUserStart := true;
  mStartTime := -1.00;
  mStartMMIm := true;
  mAlertSound := true;
  mMMState := msMMSleep;
  mInstalledHandlers := '';
end;
//------------------------------------------------------------------------------

destructor TMMGuard.Destroy;
var
  xStart: cardinal;
const
  cTerminateThreadTimeout = 5000;
begin
  mReadPort.Free;

  mTimer.Terminate;
  mTimer.Resume;
  xStart := GetTickCount;
  if WaitForSingleObject(mTimer.Handle, cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
    if not (TerminateThread(mTimer.Handle, 0)) then
      WriteToEventLogDebug(FormatErrorText(GetLastError), cEventLogDefaultText[ttMMGuardMain], etError, ssMMGuard)
    else
      WriteToEventLogDebug('Thread mTimer terminated by TerminateThread', cEventLogDefaultText[ttMMGuardMain], etWarning, ssMMGuard)
  end; // if WaitForSingleObject(mTimer.Handle,cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
  mTimer.Free;

  mWatchDog.Terminate;
  mWatchDog.Resume;
  if WaitForSingleObject(mWatchDog.Handle, cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
    if not (TerminateThread(mWatchDog.Handle, 0)) then
      WriteToEventLogDebug(FormatErrorText(GetLastError), cEventLogDefaultText[ttMMGuardMain], etError, ssMMGuard)
    else
      WriteToEventLogDebug('Thread mWatchDog terminated by TerminateThread', cEventLogDefaultText[ttMMGuardMain], etWarning, ssMMGuard)
  end; // if WaitForSingleObject(mWatchDog.Handle,cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
  mWatchDog.Free;

  mTerminator.Terminate;
  mTerminator.Resume;
  if WaitForSingleObject(mTerminator.Handle, cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
    if not (TerminateThread(mTerminator.Handle, 0)) then
      WriteToEventLogDebug(FormatErrorText(GetLastError), cEventLogDefaultText[ttMMGuardMain], etError, ssMMGuard)
    else
      WriteToEventLogDebug('Thread mTerminator terminated by TerminateThread', cEventLogDefaultText[ttMMGuardMain], etWarning, ssMMGuard)
  end; // if WaitForSingleObject(mTerminator.Handle,cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
  mTerminator.Free;

  mSQLRebooter.Terminate;
  mSQLRebooter.Resume;
  if WaitForSingleObject(mSQLRebooter.Handle, cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
    if not (TerminateThread(mSQLRebooter.Handle, 0)) then
      WriteToEventLogDebug(FormatErrorText(GetLastError), cEventLogDefaultText[ttMMGuardMain], etError, ssMMGuard)
    else
      WriteToEventLogDebug('Thread mSQLRebooter terminated by TerminateThread', cEventLogDefaultText[ttMMGuardMain], etWarning, ssMMGuard)
  end; // if WaitForSingleObject(mSQLRebooter.Handle,cTerminateThreadTimeout) = WAIT_TIMEOUT then begin
  mSQLRebooter.Free;

  mEventLog.Free;
  mMMGuardWriter.Free;
  mProcList.Free;
  mBroadClient.Free;
  Sleep(10); // make shure the Threads can correct terminate
  inherited Destroy;
(*
   mReadPort.Free;
    mTimer.Terminate;
    mTimer.Resume;
    mTimer.Free;
    mWatchDog.Terminate;
    mWatchDog.Resume;
    mWatchDog.Free;
    mTerminator.Terminate;
    mTerminator.Resume;
    mTerminator.Free;
    mSQLRebooter.Terminate;
    mSQLRebooter.Resume;
    mSQLRebooter.Free;
    mEventLog.Free;
    mMMGuardWriter.Free;
    mProcList.Free;
    mBroadClient.Free;
    Sleep ( 10 ); // make shure the Threads can correct terminate
    inherited Destroy;*)
end;
//------------------------------------------------------------------------------

function TMMGuard.Initialize: boolean;
var x, x1, x2: integer;
  xPIDList: PInteger;
  xNumOfProc: integer;
  xProcArr: TProcArray;
  xNeeded: Integer;
  xPIDCount: Integer;
  xHandle: THandle;
  xName: array[0..30] of Char;
  xStr1: string;
  xStr2: string;

  function getPID(var aPIDArr: array of Integer; aIndex: integer): integer;
  begin
    Result := aPIDArr[aIndex];
  end;

begin
  Result := InitIPCs;
  if Result then
    Result := ReadSettingsANDCreateAssistants(xProcArr, xNumOfProc);
    // Check if there are some MillMaster processes existent ( should not, but...)
  if Result then begin
    try
      xPIDList := nil;
      ReallocMem(xPIDList, 65536);
      if not EnumProcesses(xPIDList, 65536, xNeeded) then begin
        xNeeded := 0;
      end;
      ReallocMem(xPIDList, xNeeded);
      xPIDCount := xNeeded div sizeof(Integer);
    except
      ReallocMem(xPIDList, 0);
      Log(etError, 'Can not allocate memory for process list');
      Result := false;
    end;
  end;
  if Result then begin
    for x := 0 to xPIDCount - 1 do begin
      x2 := getPID(xPIDList^, x);
      xHandle := OpenProcess(PROCESS_ALL_ACCESS, False, x2);
      if xHandle <> 0 then begin
        xName := '';
        try
          if GetModuleBaseName(xHandle, 0, xName, sizeof(xName)) > 0 then begin
            xStr2 := UpperCase(StrPas(xName));
            for x1 := 0 to xNumOfProc - 1 do begin
              xStr1 := UpperCase(StrPas(PChar(xProcArr[x1].Name)));
              if Pos(xStr2, xStr1) > 0 then begin
                if not TerminateProcess(xHandle, 0) then begin
                  Log(etError, 'Can not terminate process ' + StrPas(xName));
                  Result := false;
                end;
              end;
            end;
          end;
        except
          Result := false;
          Log(etError, 'Can not get the process names');
          Exit;
        end;
      end;
    end;
    ReallocMem(xPIDList, 0);
  end;
  if Result then begin
{$IFDEF WriteStatusToLog}
    Log(etInformation, 'Start of MillMaster Service (testversion with symbol ''WriteStatusToLog'')');
{$ELSE}
    Log(etInformation, 'Start of MillMaster Service (ADO Version)');
{$ENDIF}

    SetState(msMMSleep);
  end else begin
    Log(etError, 'Start of MillMaster Service failed. Please consult the system administrator.');
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.InitIPCs: boolean;
begin
  Result := true;
  if (mReadPort <> nil) then begin
    if not mReadPort.Init then begin
      Result := false;
      Log(etError, 'Initialisation of read port failed. Error: ' + IntToStr(mReadPort.Error));
      Exit;
    end;
  end;
  sleep(1000); // Taskswitch erzwingen
  if not mMMGuardWriter.Connect then begin
    sleep(StrToIntDef(GetRegString(cRegLM, cRegMMDebug, 'MMGuardConnectTimeout', '1000'), 1000)); // Taskswitch erzwingen
    Log(etWarning, '2. Versuch mMMGuardWriter.Connect');
    if not mMMGuardWriter.Connect then begin
      Result := false;
      Log(etError, Format('Connect to port failed.Error: %s', [FormatErrorText(MMMGuardWriter.Error)]));
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ReadSettingsANDCreateAssistants(var aProcArr: TProcArray; var aNumOfProc: integer): boolean;
var xSettingsReader: TMMSettingsReader;
  x, x1, x2: integer;
  xProcNameFound: boolean;
  xDomainNames: string;
  xActParameter: string;
  xTmpList: TmmStringList;
  xFound: Boolean;
begin
  Result := true;
  xSettingsReader := TMMSettingsReader.Instance;
  try
    xSettingsReader.Init;
    xActParameter := cNumOfProcess;
      // Liest die Anzahl der Prozesse (Handler) aus der REgistry
    aNumOfProc := xSettingsReader.Value[xActParameter];
    xActParameter := cProcess;
    mInstalledHandlers := ''; //Reset
    xTmpList := TmmStringList.Create; //Construct the list object
    try
      xTmpList.Clear;
      for x := 0 to aNumOfProc - 1 do begin
          // Liest den Pfad f�r den Prozess (Handler) aus der Registry
        aProcArr[x].Name := xSettingsReader.Value[xActParameter + IntToStr(1 + x)];
        aProcArr[x].Name := UpperCase(aProcArr[x].Name);
          // get the type of the process by name
        xProcNameFound := false;
//          for x1 := 0 to cNumOfSubSystems - 1 do begin
        for x1 := 0 to ord(High(TSubSystemTyp)) do begin
          if Pos(UpperCase(cApplicationNames[TSubSystemTyp(x1)]), aProcArr[x].Name) > 0 then begin
            xProcNameFound := true;
            aProcArr[x].Typ := TSubSystemTyp(x1);
            if Pos(cUnknownFlag, UpperCase(aProcArr[x].Name)) > 0 then begin
                // delete the UNKNOWN flag from the name
              Delete(aProcArr[x].Name, Pos(cUnknownFlag, aProcArr[x].Name), StrLen(cUnknownFlag));
              aProcArr[x].Unknown := true;
            end else begin
                //Abf�llen der NetHandler
              x2 := ORD(Low(TNetTyp)) + 1;
              xFound := False;
              while (not xFound) and (x2 <= ORD(High(TNetTyp))) do begin
                if (cNetHandlerNames[TNetTyp(x2)] = TSubSystemTyp(x1)) then begin
                  xTmpList.Add(cApplicationNames[TSubSystemTyp(x1)]);
                  xFound := True;
                end; //if
                INC(x2);
              end; //for x2

              aProcArr[x].Unknown := false;
            end;
          end;
        end;
        if not xProcNameFound then begin
          raise Exception.Create('Undefined process name in registry found. Name : ' + aProcArr[x].Name);
        end;
      end;
      mInstalledHandlers := xTmpList.CommaText;
    finally
      xTmpList.Free; { destroy the list object }
    end;
    mProcList := TProcessList.Create(aProcArr, aNumOfProc);
    mWatchDog := TWatchDog.Create(mProcList);
    mTerminator := TTerminator.Create(mProcList);
    mSQLRebooter := TSQLServerRebooter.Create(cSQLServiceName, cSQLAgentName, cOLAPServiceName);
      { DONE -oLOK -cDebug : Testcode entfernen }
(*      xActParameter := cDomainNames;
      xDomainNames  := xSettingsReader.Value[xActParameter];
      mBroadClient  := TBroadcaster.Create ( xDomainNames,  cChannelNames[ttMMClientReader] );
      if not mBroadClient.Init(10) then begin
        raise Exception.Create ( 'Initialisation of client broadcast failed. Error :'
                                 + FormatErrorText ( mBroadClient.Error)
                                 + ' --- Aditional Informations ---'
                                 + mBroadClient.ErrorInformation);
      end;*)
    xActParameter := cRebootSQLServer;
    mSQLRebooter.DoReboot := xSettingsReader.Value[xActParameter];
    xActParameter := cAlertTime;
    mTimer := TMMGuardTimer.Create(xSettingsReader.Value[xActParameter]);
    xActParameter := cErrResetTime;
    mTimer.ResetTime := xSettingsReader.Value[xActParameter];
    xActParameter := cRetrayTime;
    mTimer.RestartTime := xSettingsReader.Value[xActParameter];
    xActParameter := cStartMMIm;
    mStartMMIm := xSettingsReader.Value[xActParameter];
    xActParameter := cAlertSound;
    mAlertSound := xSettingsReader.Value[xActParameter];
  except
    on e: Exception do begin
      Result := false;
      Log(etWarning, 'TMMGuard.ReadSettingsANDCreateAssistants: Can not read parameters. Position : ' + xActParameter + '  ' + e.Message);
    end;
  end;
    // Mit Singelton nicht mehr notwendig
//    xSettingsReader.Free;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.WriteLogRestartFromProcess;
begin
  if mMsg.Error.ErrorTyp <> etNoError then
    Log(etError, 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText(mMsg.Error));
end;
//------------------------------------------------------------------------------

procedure TMMGuard.Process;
var xRead: DWord;
begin
  try
    if mStartMMIm then begin
      xRead := 0;
      FillChar(mMsg, sizeof(TMMGuardRec), 0);
      mMsg.MsgTyp := TMMGuardCommand(gcStartMM);
      mMsg.Transmitter := ssMMGuard;
      mMsg.Error := SetError(NO_ERROR, etNoError, '');
      mMsg.ThreadTyp := ttMMGuardMain;
      mMsg.ComputerAndUser := gMMHost + '/MMGuard';
      ProcessMsg;
    end;
    repeat
      xRead := 0;
      FillChar(mMsg, sizeof(TMMGuardRec), 0);
      if not mReadPort.Read(@mMsg, sizeof(TMMGuardRec), xRead) then
      begin
        Log(etError, 'Read of Glb_MMGuard failed error : ' + IntToStr(mReadPort.Error));
      end;
      DebugLog(' ');
    until (not ProcessMsg);
  except
    on e: Exception do begin
      Log(etError, 'Error in TMMGuard.Process. ' + e.Message);
    end;
  end;
end;
//--State Event Machine of MMGuard--------------------------------------------

function TMMGuard.ProcessMsg: boolean;
begin
  Result := true;
    // process Msg not depending on a status
  case mMsg.MsgTyp of
    gcDebug: begin SetAndSendDebugToAll(mMsg.DebugMode); Exit end;
    gcGetState: begin BroadcastActState; Exit; end;
    gcStopService: begin InitiateStopService; Exit; end;
      // not working yet gcRebootServer : begin InitiateRebootServer; Exit; end;
  else end;

{$IFDEF WriteStatusToLog}
      (* Wenn der Status ins Log geschrieben wird, dann kann unter Umst�nden besser
         erruiert werden wo ein Fehler im Ablauf auftritt *)
  WriteToEventLogDebug('Status: ' + GetEnumName(TypeInfo(TMMGuardState), ord(mMMState)));
  WriteToEventLogDebug('Message Typ: ' + GetEnumName(TypeInfo(TMMGuardCommand), ord(mMsg.MsgTyp)));
{$ENDIF}

  case mMMState of
    msMMSleep: begin
        case mMsg.MsgTyp of
          gcStartMM: begin
              if IsSQLServerAvailable then begin
                mNumStarts := 1;
                SetState(msMMStartCreate);
                if not CreateAllProcesses then
                  InitiateRestart;
                mTimer.Timeout := cAVAILABLE_TIMEOUT;
              end else begin
                mTimer.Timeout := cSQLSERVER_STARTUP_TIMEOUT;
              end;
            end;
          gcTimeout: begin
              if IsSQLServerAvailable then begin
                SetState(msMMStartCreate);
                if not CreateAllProcesses then
                  InitiateRestart;
                mTimer.Timeout := cAVAILABLE_TIMEOUT;
              end else begin
                  // SQL-Server is not available
                SetState(msMMAlarm);
                Log(etError, 'Unable to start MillMaster. SQL-Server is not available or SQL drivers are not available.');
              end;
            end;
        else end;
      end;

    msMMStartCreate: begin
        case mMsg.MsgTyp of
          gcRestartMM: begin
              WriteLogRestartFromProcess;
//                Log ( etError , 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText ( mMsg.Error ) );
              InitiateRestart;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcAvailable: begin
              if not ProcessAvailableMsg(msMMStartAvailable) then begin
                InitiateRestart;
              end;
            end;
          gcProcessStoped: begin
              Log(etError, 'Process stopped : ' + cApplicationNames[mMsg.Transmitter]);
              InitiateRestart;
            end;
          gcTimeout: begin
                // One or more processes did not response in time, lets do a restart
              Log(etError, 'Timeout by waiting of processes available msg. Process(es) : ' + GetFailedProcesses(psAvailable));
              InitiateRestart;
            end;
        else end;
      end;

    msMMStartAvailable: begin
        case mMsg.MsgTyp of
          gcRestartMM: begin
              WriteLogRestartFromProcess;
//                Log ( etError , 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText ( mMsg.Error ));
              InitiateRestart;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcConnected: begin
              if not ProcessConnectedMsg(msMMStartConnected) then begin
                InitiateRestart;
              end;
            end;
          gcProcessStoped: begin
              Log(etError, 'Process stopped : ' + cApplicationNames[mMsg.Transmitter]);
              InitiateRestart;
            end;
          gcTimeout: begin
                // One or more processes did not response in time, lets do a restart
              Log(etError, 'Timeout by waiting of processes connected msg. Process(es) : ' + GetFailedProcesses(psConnected));
              InitiateRestart;
            end;
        else end;
      end;

    msMMStartConnected: begin
        case mMsg.MsgTyp of
          gcRestartMM: begin
              WriteLogRestartFromProcess;
//                Log ( etError , 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText ( mMsg.Error ) );
              InitiateRestart;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcInitialized: begin
              if not ProcessInitializedMsg(msMMStartInitialized) then begin
                InitiateRestart;
              end;
            end;
          gcProcessStoped: begin
              Log(etError, 'Process terminated : ' + cApplicationNames[mMsg.Transmitter]);
              InitiateRestart;
            end;
          gcTimeout: begin
                // One or more processes did not response in time, lets do a restart
              Log(etError, 'Timeout by waiting of processes initialized msg. Process(es) : ' + GetFailedProcesses(psInit));
              InitiateRestart;
            end;
        else end;
      end;

    msMMStartInitialized: begin
        case mMsg.MsgTyp of
          gcRestartMM: begin
              WriteLogRestartFromProcess;
//                  Log ( etError , 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText ( mMsg.Error ) );
              InitiateRestart;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcRun: begin
              if not ProcessRunMsg then begin
                InitiateRestart;
              end;
            end;
          gcProcessStoped: begin
              Log(etError, 'Process terminated : ' + cApplicationNames[mMsg.Transmitter]);
              InitiateRestart;
            end;
          gcTimeout: begin
                  // One or more processes did not response in time, lets do a restart
              Log(etError, 'Timeout by waiting of processes run msg. Process(es) : ' + GetFailedProcesses(psRun));
              InitiateRestart;
            end;
        else end;
      end;

    msMMRun: begin
        case mMsg.MsgTyp of
          gcRestartMM: begin
              WriteLogRestartFromProcess;
//                Log ( etError , 'Restart from process : ' + cApplicationNames[mMsg.Transmitter] + FormatMMErrorText ( mMsg.Error ) );
              InitiateRestart(mMsg.Transmitter);
            end;
          gcProcessStoped: begin
              Log(etError, 'Process terminated : ' + cApplicationNames[mMsg.Transmitter]);
              InitiateRestart;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcResetTimeout: begin mNumStarts := 1; end;
        else end;
      end;

    msMMRestartStop: begin
        case mMsg.MsgTyp of
          gcKillInProgress: begin
                // Timeout anpassen f�r die Zeit in der der Hauptthread schl�ft
                // Timeout in Sekunden -- Sleep() in Millisekunden
              mTimer.Timeout := mTimer.Timeout + cKill_Timeout;
//              Sleep(cKill_Timeout * 1000);
              Sleep(1000);
              if AllProcTerminated then begin
                mTimer.Timeout := cNO_TIMEOUT;
                ProcessRebootSQLServer;
              end;
//              else Sleep(cKill_Timeout * 1000);
            end;
          gcTimeout: begin
              if ProcessStopTimeout(true) = stCanNotTerminateProcess then begin
                SetState(msMMAlarm);
              end;
            end;
          gcAllProcessStoped: begin
              mTimer.Timeout := cNO_TIMEOUT;
              ProcessRebootSQLServer;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
        else end;
      end;

{1.20} msMMRestartRebootSQLServer: begin
        case mMsg.MsgTyp of
          gcTimeout: begin
              SetState(msMMAlarm);
              Log(etError, 'Timeout by reboot of SQL-Server.');
            end;
          gcSQLRebooted: begin
              if IsSQLServerAvailable then begin
                ProcessRestart;
              end else begin
                SetState(msMMAlarm);
                Log(etError, 'SQL-Server is not available after reboot of them.');
              end;
            end;
          gcStopMM: begin
              SetState(msMMSleep);
            end;
        else end;
      end;

    msMMStop: begin
        case mMsg.MsgTyp of
          gcKillInProgress: begin
              Sleep(100); // time for process to terminat
              if AllProcTerminated then begin
                mTimer.Timeout := cNO_TIMEOUT;
                SetState(msMMSleep);
              end;
            end;
          gcTimeout: begin
              case ProcessStopTimeout(false) of // V1.31
                stAllProcessesTerminated: SetState(msMMSleep);
                stTerminationPending: ;
                stCanNotTerminateProcess: SetState(msMMAlarm);
              end
            end;
          gcAllProcessStoped: begin
              mTimer.Timeout := cNO_TIMEOUT;
              SetState(msMMSleep);
            end;
        else end;
      end;

    msMMStopService: begin
        case mMsg.MsgTyp of
          gcKillInProgress: begin
              Sleep(100); // time for process to terminat
              if AllProcTerminated then begin
                mTimer.Timeout := cNO_TIMEOUT;
                Result := false;
              end;
            end;
          gcTimeout: begin
              Log(etWarning, 'Timeout in Stop Service');
              ProcessStopTimeout(false);
              Result := false;
            end;
          gcAllProcessStoped: begin
              mTimer.Timeout := cNO_TIMEOUT;
              Result := false;
            end;
        else end;
      end;

    msMMAlarm: begin
        case mMsg.MsgTyp of
          gcStartMM: begin
              if not AllProcTerminated then begin
                  // kill the still existing processes
                TerminateAllProcesses;
              end;
              SetState(msMMSleep);
              if not WritToSelf(gcStartMM) then begin
                Log(etError, 'Write for start failed. Error : ' + IntToStr(mMMGuardWriter.Error));
              end;
            end;
          gcAlertRestart: begin
              if not AllProcTerminated then begin
                  // kill the still existing processes
                TerminateAllProcesses;
              end else begin
                mNumStarts := 1; // should be two but there is an increment in ProcessRestart
                ProcessRebootSQLServer;
              end;
            end;
          gcStopMM: begin
              SetState(msMMStop);
              SendKillToAll;
            end;
          gcKillInProgress: begin
              if AllProcTerminated then
                mTimer.Timeout := cNO_TIMEOUT;
            end;
          gcTimeout: begin
              ProcessStopTimeout(false);
            end;
          gcAllProcessStoped: begin
              mTimer.Timeout := cNO_TIMEOUT;
            end;
          gcAlert: begin
              if mAlertSound then begin
                Windows.Beep(1000, 600);
                Windows.Beep(2000, 800);
              end;
            end;
        else end;
      end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ProcessAvailableMsg(aState: TMMGuardState): boolean;
begin
  Result := true;
  if mMsg.Error.ErrorTyp = etNoError then begin
    mProcList.SetProcState(mMsg.Transmitter, psAvailable);
       // Check if all processes are ready
    if AllProcAvailable then begin
      mTimer.Timeout := cNO_TIMEOUT;
         // Connect to the port of the processes
      if ConnectAll then begin
           // Send the connect message to the processes
        if SendDoConnectToAll then begin
          SetState(aState);
          mTimer.Timeout := cCONNECT_TIMEOUT;
        end else begin
          Log(etError, 'Send Connect to processes failed.');
          Result := false;
        end;
      end else begin
        Log(etError, 'Connect to processes failed.');
        Result := false;
      end;
    end;
  end else begin
    Log(etError, 'Available message from process contains an error. Process : ' +
      cApplicationNames[mMsg.Transmitter] + FormatMMErrorText(mMsg.Error));
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ProcessConnectedMsg(aState: TMMGuardState): boolean;
begin
  Result := true;
  if mMsg.Error.ErrorTyp = etNoError then begin
    mProcList.SetProcState(mMsg.Transmitter, psConnected);
       // Check if all processes are connected
    if AllProcConnected then begin
      mTimer.Timeout := cNO_TIMEOUT;
         // Send the connect message to the processes
      if SendDoInitToAll then begin
        SetState(aState);
        mTimer.Timeout := cINITIALIZED_TIMEOUT;
      end else begin
        Log(etError, 'Connect to processes failed');
        Result := false;
      end;
    end;
  end else begin
    Log(etError, 'Connected message from process contains an error. Process : ' +
      cApplicationNames[mMsg.Transmitter] + FormatMMErrorText(mMsg.Error));
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ProcessInitializedMsg(aState: TMMGuardState): boolean;
begin
  Result := true;
  if mMsg.Error.ErrorTyp = etNoError then begin
    mProcList.SetProcState(mMsg.Transmitter, psInit);
       // Check if all processes are initialized
    if AllProcInitialized then begin
      mTimer.Timeout := cNO_TIMEOUT;
         // Send the connect message to the processes
      if SendDoRunToAll then begin
        SetState(aState);
        mTimer.Timeout := cRUN_TIMEOUT;
      end else begin
        Log(etError, 'Connect to processes failed');
        Result := false;
      end;
    end;
  end else begin
    Log(etError, 'Initialized message from process contains an error. Process : ' +
      cApplicationNames[mMsg.Transmitter] + FormatMMErrorText(mMsg.Error));
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ProcessRunMsg: boolean;
begin
  Result := true;
  if mMsg.Error.ErrorTyp = etNoError then begin
    mProcList.SetProcState(mMsg.Transmitter, psRun);
       // Check if all processes are running
    if AllProcRun then begin
      mTimer.Timeout := cNO_TIMEOUT;
      SetState(msMMRun);
    end;
  end else begin
    Log(etError, 'ThreadStarted message from process contains an error. Process : ' +
      cApplicationNames[mMsg.Transmitter] + FormatMMErrorText(mMsg.Error));
    Result := false;
  end;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.ProcessRebootSQLServer;
begin
  SetState(msMMRestartRebootSQLServer);
  mTimer.Timeout := cSQLSERVER_REBOOT_TIMEOUT;
  mSQLRebooter.Resume;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.ProcessRestart;
begin
    //SetState ( msMMRestartCreate  );
  SetState(msMMStartCreate);
  if not CreateAllProcesses then begin
    InitiateRestart;
  end;
  mTimer.Timeout := cAVAILABLE_TIMEOUT;
end;
//------------------------------------------------------------------------------

function TMMGuard.ProcessStopTimeout(aRestart: boolean): TStopTimeoutResult;
begin
  Result := stAllProcessesTerminated;
  if mMsg.Count = 0 then begin // First timeout
      // At this time all processes should be terminated
    if not AllProcTerminated then begin
      TerminateAllProcesses;
      mTimer.Timeout := cKILL_TIMEOUT;
      mTimer.TimeoutCount := 1;
      Result := stTerminationPending;
    end else begin
      Result := stAllProcessesTerminated;
      if aRestart then
        ProcessRebootSQLServer;
    end;
  end else begin
      // one process could not be terminated
    Log(etError, 'Unable to terminate processes.');
    if not (mMMState = msMMStopService) then
      Result := stCanNotTerminateProcess;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.CreateAllProcesses: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.CreateProc(mProcList.ProcessByIndex(x)) then begin
      Log(etError, 'Create of ' + cApplicationNames[mProcList.ProcessByIndex(x)] + ' failed. Error : ' + IntToStr(getLastError));
      Result := false;
    end;
    Sleep(cProcessCreateWaitTime);
  end;
  if Result then
      // Start the WatchDog
    mWatchDog.Resume;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.TerminateAllProcesses;
begin
    // kick on the Terminator
  mTerminator.Resume;
end;
//------------------------------------------------------------------------------

function TMMGuard.WritToSelf(aMsg: TMMGuardCommand): boolean;
var xMsg: TMMGuardRec;
begin
  FillChar(xMsg, sizeof(xMsg), 0);
  xMsg.MsgTyp := aMsg;
  xMsg.Transmitter := ssMMGuard;
  xMsg.ComputerAndUser := gMMHost + '/MMGuard';
  Result := mMMGuardWriter.Write(@xMsg, sizeof(xMsg));
end;
//------------------------------------------------------------------------------

procedure TMMGuard.SetState(aState: TMMGuardState);
var xMsg: TMMClientRec;
  xCount: DWord;
begin
  mMMState := aState;

{$IFDEF WriteStatusToLog}
      (* Wenn der Status ins Log geschrieben wird, dann kann unter Umst�nden besser
         erruiert werden wo ein Fehler im Ablauf auftritt *)
  WriteToEventLogDebug('SetState: ' + GetEnumName(TypeInfo(TMMGuardState), ord(mMMState)), cEventLogDefaultText[ttMMGuardMain], etSuccess);
{$ENDIF}

  case aState of
    msMMSleep: begin end;
    msMMStartCreate: begin
        mStartTime := Now;
        mUserStart := true;
        if mNumStarts = 1 then begin // Userstart
          Log(etInformation, 'Start of MillMaster. Initiated by: ' + mMsg.ComputerAndUser);
        end else begin // Restart
          Log(etWarning, 'Restart of MillMaster.');
        end;
      end;
    msMMRun: begin
        try
          Log(etSuccess, 'Start of MillMaster successfully. ' + DateTimeToStr(mStartTime));
        except end;
      end;
    msMMStop: begin
        Log(etInformation, 'Stop of MillMaster. Initiated by: ' + mMsg.ComputerAndUser);
      end;
    msMMStopService: begin
        Log(etInformation, 'Stop of MillMaster Service.');
      end;
    msMMAlarm: begin
      end;
    msMMRestartStop: begin
        if mMsg.Transmitter = ssApplication then
          Log(etInformation, 'Restart of MillMaster initiated by: ' + mMsg.ComputerAndUser);
      end;
  end;
    // message to MMClient
    { DONE -oLOK -cDebug : Komentar entfernen }
  BroadcastActState;
end;
//------------------------------------------------------------------------------

function TMMGuard.GetFailedProcesses(aExpProcessStatus: TMMProcessState): string;
var x: integer;
begin
  Result := '';
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if aExpProcessStatus <> mProcList.GetProcState(mProcList.ProcessByIndex(x)) then begin
      Result := Result + cApplicationNames[mProcList.ProcessByIndex(x)];
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.AllProcAvailable: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do
      // in case of an unknown process the state is run
    if not ((mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psAvailable) or
      (mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psRun)) then begin
      Result := false;
      Exit;
    end;
end;
//------------------------------------------------------------------------------

function TMMGuard.ConnectAll: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.ConnectProc(mProcList.ProcessByIndex(x)) then begin
      Log(etError, 'Connect failed. Process : ' + cApplicationNames[mProcList.ProcessByIndex(x)] + ' Error : ' + FormatMMErrorText(mProcList.Error));
      Result := false;
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.SetAndSendDebugToAll(aDebugMode: Byte);
var x: integer;
  xMsg: TMMGuardRec;
begin
  mDebug := boolean(aDebugMode);
  xMsg.MsgTyp := gcDebug;
  xMsg.DebugMode := aDebugMode;
  xMsg.Transmitter := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error := NO_ERROR;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.WriteToProcess(mProcList.ProcessByIndex(x), xMsg) then begin
      Log(etWarning, 'Write DebugMode to process failed');
    end;
  end;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.BroadcastActState;
var
  xMsg: TMMClientRec;
  xDomainNames: string;
begin
  case mMMState of
    msMMStartCreate,
    msMMStartAvailable,
    msMMStartConnected,
    msMMStartInitialized,
    msMMRestartStop: xMsg.MsgTyp := ccMMStarting;
    msMMSleep:       xMsg.MsgTyp := ccMMStopped;
    msMMRun:         xMsg.MsgTyp := ccMMStarted;
    msMMStop:        xMsg.MsgTyp := ccMMStopping;
    msMMAlarm:       xMsg.MsgTyp := ccMMAlert;
    msMMStopService: xMsg.MsgTyp := ccMMServiceStopped;
  end;
  xMsg.DateTime   := mStartTime;
  xMsg.UserStart  := mUserStart;
  xMsg.ServerName := gMMHost;

  if not Assigned(mBroadClient) then begin
    xDomainNames := TMMSettingsReader.Instance.Value[cDomainNames];
    mBroadClient := TBroadcaster.Create(xDomainNames, cChannelNames[ttMMClientReader]);
    if not mBroadClient.Init then begin
      raise Exception.Create('Initialisation of client broadcast failed. Error: '
        + FormatErrorText(mBroadClient.Error)
        + ' --- Aditional Informations ---'
        + mBroadClient.ErrorInformation);
    end;
  end; //

  CodeSite.SendMsgEx(csmCheckPoint, 'Send Broacast: ' + GetEnumName(TypeInfo(TMMClientCommand), ord(xMsg.MsgTyp)));
  if not mBroadClient.Write(@xMsg, sizeof(xMsg)) then begin
    Log(etWarning, 'TMMGuard.BroadcastActState: Broadcast failed.'
      + FormatErrorText(mBroadClient.Error)
      + ' --- Aditional Informations ---'
      + mBroadClient.ErrorInformation);
  end;
  // wss: auf schnellen PCs kam es vor, dass sozusagen zur gleichen Zeit der Status �bermittelt werden sollte
  // -> MMStarting -> MMStarted. Im MailslotWriter wird jeder Nachricht mittels GetTickCount jede neue Nachricht
  // markiert. Es konnte jedoch sein, dass durch unmittelbare Statuswechsel der GetTickCount jeweils f�r
  // das letzte MMStarting und den neuen MMStarted den gleichen Wert erhalten hatte und somit der MMClient
  // die letzte Nachricht ignorierte (Siehe Implementierungsdetails von Mailslot Write/Read mit TimeStamp).
  Sleep(100);
end;
//------------------------------------------------------------------------------

procedure TMMGuard.SendKillToAll;
var x: integer;
  xMsg: TMMGuardRec;
  xProcAvailable: boolean;
begin
  xProcAvailable      := false;
  xMsg.MsgTyp         := gcKillSelf;
  xMsg.Transmitter    := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error    := NO_ERROR;
  with mProcList do begin
    for x:=0 to mProcList.NumOfProc-1 do begin
      xProcAvailable := xProcAvailable or (not (mProcList.GetProcState(mProcList.ProcessByIndex(x)) in [psNotAvailable, psDead]));
      mProcList.WriteToProcess(mProcList.ProcessByIndex(x), xMsg);
    end;
  end;
  if xProcAvailable then begin
    mTimer.Timeout := cKILL_TIMEOUT;
  end else begin
    mTimer.Timeout := 2;
  end;

    // Unterrichtet sich selber davon, dass jetzt die Prozesse gestoppt werden
  xMsg.MsgTyp         := gcKillInProgress;
  xMsg.Transmitter    := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error    := NO_ERROR;
  if not mMMGuardWriter.Write(@xMsg, sizeof(xMsg)) then
    Log(etError, Format('Write by Alert failed. Error : %d', [mMMGuardWriter.Error]));
end;
//------------------------------------------------------------------------------

function TMMGuard.SendDoConnectToAll: boolean;
var x: integer;
  xMsg: TMMGuardRec;
begin
  Result := true;
  xMsg.MsgTyp := gcDoConnect;
  xMsg.Transmitter := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error := NO_ERROR;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.WriteToProcess(mProcList.ProcessByIndex(x), xMsg) then begin
      Result := false;
      Log(etError, 'TMMGuard.SendDoConnectToAll failed. ' + FormatMMErrorText(mProcList.Error));
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.SendDoInitToAll: boolean;
var x: integer;
  xMsg: TMMGuardRec;
begin
  Result := true;
  xMsg.MsgTyp := gcDoInit;
  xMsg.Transmitter := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error := NO_ERROR;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.WriteToProcess(mProcList.ProcessByIndex(x), xMsg) then begin
      Result := false;
      Log(etError, 'TMMGuard.SendDoInitToAll failed. ' + FormatMMErrorText(mProcList.Error));
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.AllProcConnected: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do
      // in case of unknown process the state is run
    if not ((mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psConnected) or
      (mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psRun)) then begin
      Result := false;
      Exit;
    end;
end;
//------------------------------------------------------------------------------

function TMMGuard.AllProcInitialized: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do
      // in case of unknown process the state is run
    if not ((mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psInit) or
      (mProcList.GetProcState(mProcList.ProcessByIndex(x)) = psRun)) then begin
      Result := false;
      Exit;
    end;
end;
//------------------------------------------------------------------------------

function TMMGuard.SendDoRunToAll: boolean;
var x: integer;
  xMsg: TMMGuardRec;
begin
  Result := true;
  xMsg.MsgTyp := gcDoRun;
  xMsg.Transmitter := ssMMGuard;
  xMsg.Error.ErrorTyp := etNoError;
  xMsg.Error.Error := NO_ERROR;
  for x := 0 to mProcList.NumOfProc - 1 do begin
    if not mProcList.WriteToProcess(mProcList.ProcessByIndex(x), xMsg) then begin
      Result := false;
      Log(etError, 'TMMGuard.SendDoRunToAll failed. ' + FormatMMErrorText(mProcList.Error));
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.AllProcRun: boolean;
var x: integer;
begin
  Result := true;
  for x := 0 to mProcList.NumOfProc - 1 do
    if mProcList.GetProcState(mProcList.ProcessByIndex(x)) <> psRun then begin
      Result := false;
      Exit;
    end;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.InitiateRestart(aTransmitter: TSubSystemTyp = ssMMGuard);
begin
  case aTransmitter of
     (* Wenn eine Application einen Resatrt beantragt,
        dann keinen Fehler ausl�sen, sondern nur den neuen
        Status setzen *)
    ssApplication:
      SetState(msMMRestartStop);
  else
     (* In allen anderen F�llen liegt ein Fehler vor
        deshalb muss �berpr�ft werden, ob es noch sinnvoll ist
        neu zu starten (mNumStarts) *)
    inc(mNumStarts);
    if mNumStarts <= cMaxRestarts then begin
      SetState(msMMRestartStop);
    end else begin
       // no restart -> alert
      SetState(msMMAlarm);
    end;
  end;
  SendKillToAll;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.InitiateStopService;
begin
  SetState(msMMStopService);
  SendKillToAll;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.InitiateRebootServer;
begin
  SendKillToAll;
  SetState(msMMStop);
  if not InitiateSystemShutdown(nil,
    PChar('Server will be restarted in ' + IntToStr(cREBOOT_WAIT_TIME)),
    cREBOOT_WAIT_TIME,
    true,
    true) then
    Log(etError, 'Can not reboot Server. Error :' + IntToStr(GetLastError))
  else
    Log(etWarning, 'Reboot of MillMaster Server by : ' + cApplicationNames[mMsg.Transmitter]);
end;
//------------------------------------------------------------------------------

function TMMGuard.AllProcTerminated: boolean;
var x: integer;
  xProcState: TMMProcessState;
begin
  Result := true;
  for x:=0 to mProcList.NumOfProc-1 do begin
    xProcState := mProcList.GetProcState(mProcList.ProcessByIndex(x));
    if not ((xProcState = psDead) or (xProcState = psNotAvailable)) then begin
      Result := false;
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------

function TMMGuard.IsSQLServerAvailable: boolean;
var xDB: TAdoDBAccess;
begin
  Result := false;
  xDB := TAdoDBAccess.Create(1, true);
  try
    Result := xDB.Init;

      //Einf�gen der verf�gbaren NetHandler in t_MMUParms  (Nue:1.9.04)
      // Diese wurden zuvor in ReadSettingsANDCreateAssistants auds Registry gelesen und in mInstalledHandlers abgef�llt.
    with xDB.Query[0] do
    try
      SQL.Text := 'if exists(select * from t_MMUParm where Appkey = ''MMInstalledHandlers'' and AppName = ''MMConfiguration'') ' +
        '  update t_MMUParm set Data=:Data where Appkey = ''MMInstalledHandlers'' and AppName = ''MMConfiguration'' ' +
        'else insert into t_MMUParm (AppName, AppKey, Data) values(''MMConfiguration'',''MMInstalledHandlers'',:Data)';

      Params.ParamByName('Data').AsString := mInstalledHandlers;
      ExecSQL;
    except
      Result := false;
      Log(etError, 'Can not write to MillMaster database!');
    end;

  except end;
  xDB.Free;
end;
//------------------------------------------------------------------------------

procedure TMMGuard.Log(aEvent: TEventType; aText: string);
var xBuffer: array[0..3] of byte;
begin
    // fill the actual state into the buffer
  FillChar(xBuffer, sizeof(xBuffer), 0);
  xBuffer[0] := Ord(mMMState);
  xBuffer[1] := Ord(mMsg.MsgTyp);
  xBuffer[2] := Ord(mMsg.Transmitter);
  mEventLog.WriteBin(aEvent, aText, @xBuffer, sizeof(xBuffer));
end;
//------------------------------------------------------------------------------

procedure TMMGuard.DebugLog(aText: string);
var xStr: string;
  xBuffer: array[0..3] of byte;
begin
  if mDebug then begin
      // fill the actual state into the buffer
    FillChar(xBuffer, sizeof(xBuffer), 0);
    xBuffer[0] := Ord(mMMState);
    xBuffer[1] := Ord(mMsg.MsgTyp);
    xBuffer[2] := Ord(mMsg.Transmitter);

    xStr := 'State = ' + GetEnumName(TypeInfo(TMMGuardState), Ord(mMMState)) +
      '  Message typ = ' + GetEnumName(TypeInfo(TMMGuardCommand), Ord(mMsg.MsgTyp)) +
      '  Transmitter = ' + GetEnumName(TypeInfo(TSubSystemTyp), Ord(mMsg.Transmitter));
    mDebugLog.WriteBin(etInformation, aText + '  Debug info : ' + xStr, @xBuffer, sizeof(xBuffer));
  end;
end;
//--procedures for service------------------------------------------------------
//------------------------------------------------------------------------------

procedure ReportState(aCurrentState: DWord; aWin32ExitCode: DWord;
  aCheckPoint: DWord; aWaitHint: DWord);
var xState: TService_Status;
begin
    // lServiceState holds the last state of the service reported to the service control manager
  if aCurrentState = 0 then
    aCurrentState := lServiceState;

  if aCurrentState = SERVICE_START_PENDING then
    xState.dwControlsAccepted := SERVICE_ACCEPT_SHUTDOWN
  else
    xState.dwControlsAccepted := SERVICE_ACCEPT_STOP or SERVICE_ACCEPT_SHUTDOWN;
  xState.dwCurrentState := aCurrentState;
  xState.dwWin32ExitCode := aWin32ExitCode;
  xState.dwCheckPoint := aCheckPoint;
  xState.dwServiceType := SERVICE_WIN32_OWN_PROCESS;
  xState.dwServiceSpecificExitCode := 0;
  xState.dwWaitHint := aWaitHint;
  SetServiceStatus(gStateHandle, xState);
  lServiceState := aCurrentState; // memorize the last reported state
end;
//------------------------------------------------------------------------------

procedure ServiceHndMMGuard(aCtrlCode: DWord); stdcall;
var xCurrentState: DWord;
  xWriter: TIPCClient;
  xMsg: TMMGuardRec;
  xLog: TEventLogWriter;
begin
  xCurrentState := 0;
  try
    xLog := TEventLogWriter.Create(cEventLogClassForSubSystems, cServer, ssMMGuard,
      cEventLogDefaultText[ttMMGuardMain], false);
  except Exit; end;
  try
    case aCtrlCode of
      SERVICE_CONTROL_PAUSE: begin xLog.Write(etWarning, 'SERVICE_CONTROL_PAUSE') end;
      SERVICE_CONTROL_CONTINUE: begin xLog.Write(etWarning, 'SERVICE_CONTROL_CONTINUE') end;
      SERVICE_CONTROL_INTERROGATE: begin
        (* in der Modulglobalen Variable 'lServiceState' wird der letzte Status gesichert.
           Dieser Status wird gesendet, wenn der erste Parameter = 0 ist *)
          ReportState(0, NO_ERROR, 0, 0);
        end;
      SERVICE_CONTROL_STOP,
        SERVICE_CONTROL_SHUTDOWN: begin
          // mesage to the service
          xWriter := TIPCClient.Create('.', cChannelNames[ttMMGuardMain]);
          if not xWriter.Connect then begin
            xLog.Write(etError, 'Connect in ServiceHandler failed. ' + FormatErrorText(xWriter.Error));
          end;
          xMsg.MsgTyp := gcStopService;
          xMsg.Transmitter := ssMMGuard;
          xMsg.Error.ErrorTyp := etNoError;
          xMsg.Error.Error := NO_ERROR;
          if not xWriter.Write(@xMsg, sizeof(TMMGuardRec)) then begin
            xLog.Write(etError, 'Write in ServiceHandler failed.' + FormatErrorText(xWriter.Error));
          end;
          xWriter.Free;

          xCurrentState := SERVICE_STOP_PENDING;
          // if xCurrentState = 0 (initialized) the last known State is sent to the service control manager
          ReportState(xCurrentState, NO_ERROR, 1, 3000);
        end;
    else
      xLog.Write(etWarning, 'Unexpected Job in ServiceHndMMGuard.');
    end;
    ReportState(xCurrentState, NO_ERROR, 0, 0);
    xLog.Free
  except
    on e: Exception do begin
      xLog.Write(etError, 'ServiceHndMMGuard failed. ' + e.Message);
      xLog.Free;
    end;
  end;
end;
//------------------------------------------------------------------------------
(*---------------------------------------------------------
  Testet ob die Datenbank Verbindung aufgebaut werden kann
  Der Test wird solange ausgef�hrt bis die Connection etabliert ist,
  oder das Timeout (cTryTimeInSeconds) abgelaufen ist.
  Hintergrund:
  Offenbar ist der SQLServer manchmal langsamer als der Millmaster System.
  In diesem Fall ist zu verhindern, dass das System hochf�hrt, bevor eine Verbindung
  zur Datenbank hergestellt werden kann.
----------------------------------------------------------*)

function TestDBConnection: boolean;
var
  i: integer;
  j: integer;
  xStart: cardinal;
  xNextMessage: cardinal;
  xTryTime: cardinal;
const
    // Timeout
  cTryTimeInSeconds = 120;
    // Abstand in dem eine Warning ausgegeben wird (in ms)
  cNextMessage = 30000;

    // Versucht die Verbindung herzustellen
  function ConnectDB: boolean;
  var
    xCon: _Connection;
  begin
      // DBConnection erstellen (ADO native)
    xCon := CoConnection.Create;
    try
        // Connectionstring aus LoepfeGlobal
      xCon.ConnectionString := GetDefaultConnectionString;
      try
        xCon.CursorLocation := adUseClient;
        xCon.Open(xCon.ConnectionString, '', '', integer(adConnectUnspecified));
        result := (xCon.State = adStateOpen);
      except
        on e: Exception do
          result := false;
      end; // try except
    finally
        // Connection schliessen
      if (xCon.State = adStateOpen) then
        xCon.Close;
      xCon := nil;
    end; // try finally
  end; // function ConnectDB: boolean;
begin
  i := 0;
  result := false;
  xTryTime := cTryTimeInSeconds * 1000;

  xStart := GetTickCount;
    // nach 30 Sekunden eine Warning
  xNextMessage := xStart + cNextMessage;

    // Verbindung herstellen bis hergestellt oder Timeout
  while ((GetTickCount - xStart) < xTryTime) and (not (ConnectDB)) do begin
      // 5 Sekunden warten bis wieder eine Connection versucht wird
    for j := 0 to 10 do begin
      sleep(500);
      ReportState(SERVICE_START_PENDING, NO_ERROR, 1, 3000);
    end; // for j := 0 to 10  do begin
      // N�chster Versuch
    inc(i);
      // Jede Minute eine Warnung herausgeben
    if GetTickCount > xNextMessage then begin
      xNextMessage := GetTickCount + cNextMessage;
      WriteToEventLog('MMGuardMain: DBConnection failed', cEventLogDefaultText[ttMMGuardMain], etWarning);
    end; // if GetTickCount > xNextMessage then begin
  end; // while (i < xTryCount) and (not(ConnectDB)) do begin
    // Erfolgreich, wenn die Connection etabliert werden konnte
  result := ConnectDB;
end; // function TestDBConnection: boolean;
//------------------------------------------------------------------------------

procedure ServiceMainMMGuard(aNumArgs: DWord; var Args: PChar); stdcall;
var xMMGuard: TMMGuard;
  xSettings: TMMSettingsReader;
  xGroup: string;
  xResult: HResult;
  i, j: integer;
begin
  xMMGuard := nil;
  xResult := CoInitialize(nil);
  try
    // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
    // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
    if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
      raise Exception.Create('CoInitialize failed');
    try
        // definition of the service handler routine
      gStateHandle := RegisterServiceCtrlHandler(PChar('MillMaster'), @ServiceHndMMGuard);
      if gStateHandle = 0 then
        raise Exception.Create('Registration of MillMaster Service failed. ' + getLastErrorText);

// ---------------------- Test DB Connection -----------------------
      if not (TestDBConnection) then
        WriteToEventLog('MMGuardMain: DBConnection failed', cEventLogDefaultText[ttMMGuardMain], etError);
// ---------------------- Test DB Connection Ende -----------------------

        // Report state to service control manager
      ReportState(SERVICE_START_PENDING, NO_ERROR, 1, 3000);
        // get UserGroup from MMSettings
      xSettings := TMMSettingsReader.Instance;
      try
        xSettings.Init;
        xGroup := xSettings.value[cUserGroup];
      except
        on e: Exception do begin
          // Mit Singelton nicht mehr notwendig
//            xSettings.Free;
          raise Exception.Create('Read of Usergroup failed. ' + e.Message);
        end;
      end;
        // Mit Singelton nicht mehr notwendig
//        xSettings.Free;
        // Create of Service
      xMMGuard := TMMGuard.Create(xGroup);
      if not Assigned(xMMGuard) then begin
        ReportState(SERVICE_STOPPED, ERROR_BAD_ENVIRONMENT, 0, 0);
        CloseHandle(gStateHandle);
        raise Exception.Create('Create of MMGuard failed.');
      end;
        // Initialize of Service
      if not xMMGuard.Initialize then begin
        ReportState(SERVICE_STOPPED, ERROR_BAD_ENVIRONMENT, 0, 0);
        CloseHandle(gStateHandle);
        raise Exception.Create('Init of MMGuard failed.');
      end;
      ReportState(SERVICE_RUNNING, NO_ERROR, 0, 0);
        // Mainloop of Service
      xMMGuard.Process;
        // Service is Stopping
      ReportState(SERVICE_STOP_PENDING, NO_ERROR, 0, 0);
      xMMGuard.Free;
        // Service is Stopped
      ReportState(SERVICE_STOPPED, NO_ERROR, 0, 0);

      CloseHandle(gStateHandle);
    except
      on e: Exception do
        WriteToEventLog('Error in MillMaster Service. Error : ' + e.Message, cEventLogDefaultText[ttMMGuardMain], etError);
    end;
  finally
    CoUninitialize;
  end;
end;
//------------------------------------------------------------------------------
end.

