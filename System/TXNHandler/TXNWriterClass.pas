(*===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXNWriterClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: T2
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 14.07.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
| 16.04.1999  2.0t  khp | Schreibversuchwiederholung in SendTelegram eingefuegt
| 19.04.1999  2.01t khp | Schreibversuchwiederholung In TxnWrite verlegt
| 01.06.1999  2.02t khp | Zusaetzliche Funktion GetNodeList
| 22.03.2000  2.03t khp | ChangeEndian von YMParaUtils bei SetSettings und SetMaConfig
| 29.03.2000  2.04  khp | SetMaConfig und GetMaConfig ausgeklammert
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 07.08.2002  3.01  khp | MachID und NodeID bei Texnetwrite-ErrorMsg hinzugef�gt
| 28.10.2002  3.04  khp | Kaltstart: jtGetZEReady wird nicht mehr ben�tigt da LastEvent aus Decl Record von eDECLARATIONS gelesen wird.
| 03.03.2005  1.11  Wss | Umbau nach 0-basiertes Gruppen/Settingshandling innerhalb MM-System
| 15.07.2005  1.11  Wss | YarnUnit wird nun als Text z.B. 'yuNm' in den Konverter gegeben (statt Enum Wert) (in XML2BinEventHandler)
| 08.03.2006  1.11  Wss | XML2BinEventHandler: YarnCount muss noch umgerechnet werden, damit diese auf der ZE ebenfalls richtig
                                               angezeigt wird und um Coarse/Fine richtig zu berechnen
| 15.05.2008  1.12  Nue | XML2BinAfterConvert: Setzt den LowerWert des SFI auf 0 falls der Switch auf swOff=0 gesetzt ist
|=========================================================================================*)


unit TXNWriterClass;

interface

uses windows, Classes, SysUtils,
  BaseThread, BaseGlobal, TXNPoolClass, TXN_glbDef, MMEventLog, LoepfeGlobal,
  YMParaUtils, Txn_ISA_DevClass, Txn_PCI_DevClass, MMXMLConverter, comobj, MSXML2_TLB;

type
  TTXNWriter = class(TBaseSubThread)
  protected
    TxnDevHandler: array[1..cMaxTxnAdapter] of TTxnBaseDevHandler;
    mMsgPoolList: TMsgPoolList;
    mTelegramHeader: TTelegramHeader;
    mTelegramStream: TMemoryStream;
    mXMLSettings: TXMLSettingsRec;
    mDevMode: eDevModeT;
    procedure XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    function SendTelegram(aMachNo: WORD): Boolean;
    procedure SetTelegramHeader(aTelegramID: eTelegramIdT; aTelegramSubID: eTelegramSubIdT;
      aSpindleLast: Byte; aSpindlefirst: Byte;
      aFlagRegister: BITSET8; aJobID: DWord; alength: Word);
    procedure ProcessJob; OVERRIDE;
    procedure ProcessInitJob; OVERRIDE;
    procedure XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
  public
    constructor Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList;
      aPCIDEVHandlerArr: TPCIDEVHandlerArr; aDevMode: eDevModeT); REINTRODUCE;
    function DoConnect: Boolean; OVERRIDE;
    function Init: Boolean; OVERRIDE;
    destructor Destroy; OVERRIDE;
  end;

  //=============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, XMLDef, XMLMappingDef, XMLGlobal, YMParaDef, typinfo,
  XMLSettingsTools, MMUGlobal;                             // Unit fuer CodeSite;

constructor TTXNWriter.Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList;
  aPCIDEVHandlerArr: TPCIDEVHandlerArr; aDevMode: eDevModeT);
var
  xadapter: Integer;
begin
  inherited Create(aThreadDef);
  mMsgPoolList := aMsgPoolList;
  mDevMode := aDevMode;
  // PCIDEVHandler wurde schon in der TXNHAndlerClass instanziert daher nur zuordnen
  if mDevMode = ePCI then
    for xadapter := 1 to cMaxTxnAdapter do
      TxnDevHandler[xadapter] := aPCIDEVHandlerArr[xadapter]
  else
    for xadapter := 1 to cMaxTxnAdapter do
      TxnDevHandler[xadapter] := nil;
  mTelegramStream := TMemoryStream.Create;
  //    Priority := tpTimeCritical;
end;

//-----------------------------------------------------------------------------

destructor TTXNWriter.Destroy;
{Disconnect aller TexnetDevice}
var
  xAdapterNo: Integer;
begin
  if mDevMode = eISA then
    for xAdapterNo:=1 to mMsgPoolList.AdapterCount do begin
      TxnDevHandler[xAdapterNo].TxnDisconnect;
      TxnDevHandler[xAdapterNo].Free;
    end;
  mTelegramStream.free;
  inherited Destroy;
end;

//:-----------------------------------------------------------------------------
procedure TTXNWriter.XML2BinBeforeConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
begin
  // Die Klassierfelder m�ssen vor der Konvertierung Maschinenkonform transformiert werden
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPYMSettingBasicClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFDarkClassificationItem));
  SwapClassFields(aMMXMLDoc.selectSingleNode(cXPDarkClusterClassificationItem));

  // �ndert bei bedarf die CheckLength anhand des ConfigCode C4 (Division durch 32)
  TCheckLengthCalculator.CalculateCheckLengthForDownload(aMMXMLDoc);
end;

//-----------------------------------------------------------------------------

function TTXNWriter.DoConnect: Boolean;
{Connect aller definierter TexnetDevice}
var
  xAdapterNo, xAdapter: Integer;
begin
  result := inherited DoConnect;
  xAdapter := 1;
  if Result then begin
    try
      if mDevMode = eISA then           // Nur bei ISA Texnetkarte noetig
        for xAdapterNo:=1 to mMsgPoolList.AdapterCount do begin
          xAdapter := xAdapterNo;
          TxnDevHandler[xAdapterNo].ISAConnect(cTexnetDeviceName[xAdapterNo], omWrite);
        end;
    except
      Result := FALSE;
      // Error vom TexnetDriver hochziehen und ab ins Log
      fError := TxnDevHandler[xAdapter].ErrorRec;
      WriteLog(etError, FormatMMErrorText(Error));
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TTXNWriter.Init: Boolean;
var
  xAdapterNo: Integer;
begin
  Result := inherited Init;
  if Result then begin
    mConfirmStartupImmediately := False;
    try
      if mDevMode = eISA then           // // Nur bei ISA Texnetkarte noetig
        for xAdapterNo:=1 to mMsgPoolList.AdapterCount do begin
          TxnDevHandler[xAdapterNo] := TTxnDevHandler.Create;
        end;
    except
      Result := FALSE;
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TTXNWriter.SendTelegram(aMachNo: WORD): Boolean;
var
  xNodeRec: TTxnNetNodeRec;
  xBlock: TdatablockRec;
  xblockcount: Byte;
  xNodeID: Integer;
  xDatablockLengh: Integer;
  xMachstate: TTxnNodeState;
begin
  Result := FALSE;
  // BlockHeader setzen
  xBlock.headerRec.sendNode := 1;
  xBlock.headerRec.blockNumber := 1;
  // Node Record lesen
  xNodeID := mMsgPoolList.GetNodeRecord(aMachNo, xNodeRec);
  if xNodeID <> cNotValid then begin
    // Nur wenn gueltige NodeID
    xBlock.headerRec.receiveNode := xNodeID;
    xMachstate := xNodeRec.machstate;
    if xMachstate = nsOnline then begin
      // Anzahl benoetigte Blocks bestimmen
      xBlock.headerRec.TotalBlocks := mTelegramStream.Size div sizeof(TdatenArray);
      // Angefangener Block dazuzaehlen
      if (mTelegramStream.Size mod sizeof(TdatenArray)) > 0 then INC(xBlock.headerRec.TotalBlocks);
      // Setze Leseposition Stream
      mTelegramStream.Position := 0;
      for xblockcount := 1 to xBlock.headerRec.TotalBlocks do begin
        xBlock.headerRec.blockNumber := xblockcount;
        if mTelegramStream.Size - mTelegramStream.Position >= sizeof(TdatenArray) then begin
          // Vollstaendiger Block
          xDatablockLengh := sizeof(TdatenArray);
          mTelegramStream.ReadBuffer(xBlock.datenArray, xDatablockLengh);
        end
        else begin
          // Nicht vollstaendiger Block
          xDatablockLengh := mTelegramStream.Size - mTelegramStream.Position;
          mTelegramStream.ReadBuffer(xBlock.datenArray, xDatablockLengh);
        end;
        // Senden an TxnDriver 1..3
        //xPriority := Priority;
        //Priority := tpTimeCritical;
        Result := TxnDevHandler[xNodeRec.adapterNo].TxnWrite(xBlock, xDatablockLengh + sizeof(TheaderRec));
        //Priority := xPriority;
        if not Result then begin
          // Error vom TexnetDriver hochziehen und ab ins Log
          fError := TxnDevHandler[xNodeRec.adapterNo].ErrorRec;
          if fError.ErrorTyp <> etNoError then
          WriteLog(etWarning,Format('MachID:%d NodeID:%d %s',[xNodeRec.MachID, xNodeID, FormatMMErrorText(Error)]));
          //Zusammenfassen aller Write Fehler fuer MsgDisp
          fError := SetError(ERROR_WRITE_FAULT, etMMError, 'Write to Machine failed');
          break;
        end; ;
      end;
    end
    else begin
      // OffLine
      fError := SetError(ERROR_NOT_READY, etMMError, 'Machine OFFLINE');
    end;
  end else begin
    // Ungueltige NodeID
    fError := SetError(ERROR_DEV_NOT_EXIST, etMMError, 'Unknow MachNo');
  end;
end;                                    {TTXNWriter.SendTelegram}

//-----------------------------------------------------------------------------

procedure TTXNWriter.SetTelegramHeader(aTelegramID: eTelegramIdT; aTelegramSubID: eTelegramSubIdT;
  aSpindleLast: Byte; aSpindlefirst: Byte; aFlagRegister: BITSET8;
  aJobID: DWord; alength: Word);
// Kopiere mJobHeader in mTxnTelegramHeader
begin
  with mTelegramHeader do begin
    telegramID                := aTelegramID;
    telegramSubID             := aTelegramSubID;
    machineNumber             := cMachDummy;
    spindleRange.lastSpindle  := aSpindlefirst;      // Swap fuer ZE da SpindleRange 16Bit
    spindleRange.firstSpindle := aSpindleLast;       // Swap ""
    flagRegister              := aFlagRegister;
    JobID                     := SwapDWord(aJobID);  // SwapDWord JobID MM/NT
    length                    := swap(alength);      // Laenge des Telegrams mit Kopf
  end;
end;

//-----------------------------------------------------------------------------

procedure TTXNWriter.ProcessJob;
var
  xMapfile: DOMDocument40;
  xSendTxnTelegram: Boolean;
  xSizeofTelegram: Word;
  xConfirmTelegram: Boolean;
  xMachID: Word;
  xMapID: String;
  xBuffer: PByte;
  xBufferSize: Integer;
  xStr: String;
begin
  // This methode is implemented only in derived classes and it is called from
  // the Execute methode of TBaseThread.
  xSendTxnTelegram := FALSE;
  xConfirmTelegram := FALSE;
  xMachID          := 0;
  case mJob^.JobTyp of
    // Juni 2004 wss: neu wird dieser Job ebenfalls beim Aufr�umen hierher geschickt um JobValues aufzur�umen
    jtDelJobID: mMsgPoolList.DeleteJobIDValue(mJob^.JobID);

    jtSetNodeList: begin
        // Setzen der NodeList im TexnetHandler
        mMsgPoolList.SetNodeList(mJob.SetNodeList.NodeItems.TxnNetNodeList);
        // Erhalt der NodeListe bestaetigen
        // mJob.JobID wird vom eingegangenem Job uebernommen
        WriteLogDebug('SetNodeList', nil, 0, TRUE);
        xConfirmTelegram := TRUE;
      end;
    jtGetNodeList: begin
        // Lesen der Nodelist im TexnetHandler
        mMsgPoolList.GetNodeList(mJob.GetNodeList.NodeItems.TxnNetNodeList);
        WriteLogDebug('GetNodeList', nil, 0, TRUE);
        xConfirmTelegram := TRUE;
      end;
    // SaveMaConfigrToDB ben�tigt erst die Gruppenkonfiguration von der Maschine
    jtSaveMaConfigToDB,
    jtGetMaAssign: begin
        // Get machine Assignment
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.GetMaAssign do begin
          xMachID := MachineID;
          SetTelegramHeader(eREQUEST_TELEGRAM, eDECLARATIONS, cDummySpindle,
                            cDummySpindle, [cView], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        // JobTyp f�r den TXNReader in JobValueList schreiben
        if mJob^.JobTyp = jtSaveMaConfigToDB then
          // JobTyp f�r den TXNReader in JobValueList schreiben...
          mMsgPoolList.JobIDValue[mJob^.JobID, cJobValueJobTyp] := mJob^.JobTyp;

        WriteLogDebug(Format('%s %d ', [GetJobName(mJob^.JobTyp), xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
      end;
    jtClearZESpdData: begin
        // Loescht alle Spindeldaten auf ZE
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.ClearZESpdData do begin
          xMachID := MachineID;
          SetTelegramHeader(eREQUEST_TELEGRAM, eMONITOR_DATA, SpindleLast,
                            Spindlefirst, [cDelete], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        WriteLogDebug(Format('ClearZESpdData %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
        xConfirmTelegram := TRUE;
      end;
    jtClearZEGrpData: begin
        // Loescht alle Gruppendaten auf ZE
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.ClearZEGrpData do begin
          xMachID := MachineID;
          SetTelegramHeader(eREQUEST_TELEGRAM, eGROUP_DATA, SpindleLast,
                            Spindlefirst, [cDelete], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        WriteLogDebug(Format('ClearZEGrpData %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
        xConfirmTelegram := TRUE;
      end;
    jtDelZESpdData: begin
        // Acknowledge fuer Spindel Request)
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.DelZESpdData do begin
          xMachID := MachineID;
          SetTelegramHeader(eACKNOWLEDGE, eMONITOR_DATA, SpindleLast,
                            Spindlefirst, [cSynchron], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        WriteLogDebug(Format('DelZESpdData %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
        xConfirmTelegram := TRUE;
      end;
    jtGetZESpdData: begin
        // regular spindle data aquisition at intervall end
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.GetZESpdData do begin
          xMachID := MachineID;
          SetTelegramHeader(eREQUEST_TELEGRAM, eMONITOR_DATA, SpindleLast,
                            Spindlefirst, [cSynchron], mJob.JobID, xSizeofTelegram);
          WriteLogDebug(Format('GetZESpdData MaID=%d, Spd:%d-%d ', [xMachID, SpindleFirst, SpindleLast]), nil, 0, TRUE);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        xSendTxnTelegram := TRUE;
      end;
    jtDelZEGrpData: begin
        // Acknowledge fuer Grp. Request)
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.DelZEGrpData do begin
          xMachID := MachineID;
          SetTelegramHeader(eACKNOWLEDGE, eGROUP_DATA, SpindleLast,
                            Spindlefirst, [cSynchron], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        WriteLogDebug(Format('DelZEGrpData %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
        xConfirmTelegram := TRUE;
      end;
    jtGetZEGrpData: begin
        // Get all group data with delete
        xSizeofTelegram := sizeof(mTelegramHeader);
        with mJob.GetZEGrpData do begin
          xMachID := MachineID;
          SetTelegramHeader(eREQUEST_TELEGRAM, eGROUP_DATA, SpindleLast,
                            Spindlefirst, [cSynchron], mJob.JobID, xSizeofTelegram);
        end;
        with mTelegramStream do begin
          clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;
        WriteLogDebug(Format('GetZEGrpData %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
      end;
    jtGetSettings: begin
        // Get settings from prodgroup
        xSizeofTelegram := sizeof(mTelegramHeader);
        xMachID         := mJob.GetSettings.MachineID;
        with mJob.GetSettings.SettingsRec do begin
          SetTelegramHeader(eREQUEST_TELEGRAM, eSETTINGS, SpindleLast,
                            Spindlefirst, [cView], mJob.JobID, xSizeofTelegram);
        end;

        with mTelegramStream do begin
          Clear;
          writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
        end;

        // bei GetSettingsAllGroups werden mehrere GetSettings ausgel�st, jedoch immer mit gleicher JobID
        // -> Anzahl �ber JobList verwalten
        mMsgPoolList.ChangeAllXMLDataCounter(mJob^.JobID, mJob^.GetSettings.AllXMLValues);
//        if mJob^.GetSettings.AllXMLValues then begin
//          xStr := Trim(mMsgPoolList.JobIDValue[mJob^.JobID, cJobValueAllXMLValues]);
//          if xStr <> '' then
//            mMsgPoolList.JobIDValue[mJob^.JobID, cJobValueAllXMLValues] := StrToIntDef(xStr, 0) + 1;
//        end;

        WriteLogDebug(Format('GetSettings %d ', [xMachID]), nil, 0, TRUE);
        xSendTxnTelegram := TRUE;
      end;
    // Um wenn nur die ProdID zur Maschine gesendet werden muss, dann m�ssen keine g�ltier/vollst�ndiger Settingsdaten
    // geschickt werden. ProdID Spindlerange reicht aus (Mail von Roli)
//    jtSetProdID, wss 9.9.04: wieder entfernt da doch ein SetSettings Job ben�tigt wird
    jtSetSettings: begin
        xBuffer := Nil;
        FillChar(mXMLSettings, sizeof(mXMLSettings), 0);

        xMachID      := mJob^.SetSettings.MachineID;
        // es werden nur die ben�tigten Parameter in die Variable f�r das Eventhandling verwendet
        mXMLSettings := mJob^.SetSettings.SettingsRec;
        if mXMLSettings.AssignMode = cAssignProdGrpIDOnly then begin
          xStr := cMMXMLDummy;
        end else begin
          xStr := StrPas(mJob^.SetSettings.SettingsRec.XMLData);
          mXMLSettings.XMLData := #00;
        end;
        codesite.sendMsg(xStr);
//        if mJob^.SetSettings.SettingsRec.AssignMode = cAssignProdGrpIDOnly then begin
//          xStr                      := cMMXMLDummy;
//          // es werden nur die ben�tigten Parameter in die Variable f�r das Eventhandling kopiert
//          mXMLSettings.ProdGrpID    := mJob^.SetSettings.SettingsRec.ProdGrpID;
//          mXMLSettings.SpindleFirst := mJob^.SetSettings.SettingsRec.SpindleFirst;
//          mXMLSettings.SpindleLast  := mJob^.SetSettings.SettingsRec.SpindleLast;
//          mXMLSettings.AssignMode   := cAssignProdGrpIDOnly;
//          //...und auch noch die ben�tigte ProdID
//          mMsgPoolList.JobIDValue[mJob^.JobID, cJobValueProdID] := mJob^.SetSettings.SettingsRec.ProdGrpID;
//        end else begin
//          xStr                    := StrPas(mJob^.SetSettings.SettingsRec.XMLData);
//          // es wird der komplete Record in die Variable f�r das Eventhanlding kopiert
//          mXMLSettings            := mJob^.SetSettings.SettingsRec;
//          mXMLSettings.AssignMode := cAssigGroup;
//          // jedoch ohne den MMXML String
//          mXMLSettings.XMLData    := #00;
//        end;

        // XML Settings nach bin�r konvertieren...
        xMapID   := mMsgPoolList.MapID[xMachID];
        xMapfile := mMsgPoolList.MapfileDOM[xMapID];
        if Assigned(xMapfile) then begin
          with TMMXMLConverter.Create do
          try
            OnCalcExternal  := XML2BinEventHandler;
            OnBeforeConvert := XML2BinBeforeConvert;
            OnBeforeDeleteElements := XML2BinAfterConvert;  // ConfigCode schreiben
            try
              // erst nach konvertieren weiss man wie gross der bin�re Buffer ist
              xBufferSize := XMLToBin(xBuffer, xMapfile, msYMSetting, True, xStr);
              //...und ebenfalls in Send Stream abf�llen

              // Telegram Header vorbereiten
              xSizeofTelegram := sizeof(mTelegramHeader) + xBufferSize;
              SetTelegramHeader(eMESSAGE_TELEGRAM, eSETTINGS, mXMLSettings.SpindleLast,
                                mXMLSettings.Spindlefirst, [cSynchron], mJob^.JobID, xSizeofTelegram);

              // Sendstream abf�llen
              with mTelegramStream do begin
                Clear;
                // Header...
                writeBuffer(mTelegramHeader, sizeof(mTelegramHeader));
                //...und Daten schreiben
                writeBuffer(xBuffer^, xBufferSize);
              end;

              // zu Debug Zwecken
              if GetRegBoolean(HKEY_LOCAL_MACHINE, cRegMMDebug, cWriteInOutDebug, false) then begin
                SaveBinFile(xBuffer, xBufferSize, 'Dw');
                SaveBinFile(mTelegramStream.memory, mTelegramStream.size, 'TH');
              end;

              WriteLogDebug(Format('%s %d ', [GetJobName(mJob^.JobTyp), xMachID]), nil, 0, TRUE);
              xSendTxnTelegram := TRUE;
            except
              on e:Exception do
                WriteLog(etError, 'TTXNWriter.ProcessJob.jtSetSettings: ' + e.Message)
            end;
          finally
            Pointer(xMapfile) := nil;  // verhindern dass Interface freigegeben wird
            FreeMem(xBuffer);
            Free;
          end; // with TMMXMLConverter.Create
        end; // if Assigned(xMapfile)
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;

//wss  gMachine := xMachID;
  if xSendTxnTelegram then begin
    if not sendTelegram(xMachID) then begin
      // IF sendtelegram fails
      mJob.JobResult.JobTyp := mJob.JobTyp;
      mJob.JobTyp           := jtJobResult;
      mJob.JobResult.Error  := Error;
      xConfirmTelegram      := TRUE;
    end;
  end;

  if xConfirmTelegram then begin
    WriteJobBufferTo(ttMsgDispatcher);
  end;
  Sleep(10);
end;                                    {TTXNWriter.ProcessJob}

//-----------------------------------------------------------------------------

procedure TTXNWriter.ProcessInitJob;
var
  xAdapterNo: Integer;
  xNewMachstate: Boolean;
  xTempJobID: Integer;
  xNewMachstateArr: array[cMinTxnNodeID..cMaxTxnNodeID] of TTxnNetNodeRec;
begin
  case mJob.JobTyp of
    jtSetNodeList: begin
        // JobId zwischenspeichern fuer Bestaetigung
        xTempJobID := mJob.JobID;
        // Load NodeList to the NetHandler
        mMsgPoolList.SetNodeList(mJob.SetNodeList.NodeItems.TxnNetNodeList);
        // Von allen Adaptern Machstate lesen und Msgpool updaten
        mJob.JobID := cUnknownJobID;
        for xAdapterNo:=1 to mMsgPoolList.AdapterCount do begin
          if not mMsgPoolList.UpdateMachState(TxnDevHandler[xAdapterNo], xAdapterNo, xNewMachstateArr, xNewMachstate) then begin
            // Error vom TexnetDriver hochziehen und ab ins Log
            fError := TxnDevHandler[xAdapterNo].ErrorRec;
            WriteLog(etError, FormatMMErrorText(Error));
          end;
        end;
        // Erhalt der NodeListe bestaetigen, JobHeader vorbereiten
        mJob.JobID  := xTempJobID;
        mJob.JobTyp := jtSetNodeList;
        WriteJobBufferTo(ttMsgDispatcher);
        // Alle andern Thread freigeben (ControlThread, ReadThread)
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d', [IPCClientIndex[cMainChannelIndex]^.Error]));
          Exit;
        end;
      end;
  else
    ProcessJob;
  end;
end;                                    {TTXNWriter.ProcessInitJob}

//:-----------------------------------------------------------------------------
procedure TTXNWriter.XML2BinAfterConvert(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xConv: TMMXMLConverter;

  procedure WriteLowerSFIToBin(aSelPath: string);
  //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  //Diese Routine setzt den LowerWert des SFI auf 0 falls der Switch auf swOff=0 gesetzt ist.
  //Dies ist ein Workaround der hier gel�st wird, aber eigentlich in der ZE-Software umgesetzt werden m�sste.!!!!
  // (Nue:15.5.08)
  var
    xValue: string;
    xNode: IXMLDOMNode;
  begin
    xValue := cSwitchNames[swOn];
    xNode := aMMXMLDoc.SelectSingleNode(aSelPath);
    if assigned(xNode) then
      xValue := GetElementValueDef(xNode, cSwitchNames[swOn]);
    if xValue = cSwitchNames[swOff] then begin
      xNode := aMMXMLDoc.SelectSingleNode(cXPSFILowerItem);
      if assigned(xNode) then
        xConv.WriteElementToBinWithoutCheck(cXPSFILowerItem, 0);
    end;
  end;// procedure WriteLowerSFIToBin(aSelPath: string);

  procedure WriteConfigCodeToBin(aSelPath: string);
  var
    xValue: integer;
    xNode: IXMLDOMNode;
  begin
    xValue := -1;
    xNode := aMMXMLDoc.SelectSingleNode(aSelPath);
    if assigned(xNode) then
      xValue := GetElementValueDef(xNode, -1);
    if xValue >= 0 then
      xConv.WriteElementToBinWithoutCheck(aSelPath, xValue);
  end;// procedure WriteConfigCodeToBin(aSelPath: string);

begin
  if Sender is TMMXMLConverter then begin
    xConv := TMMXMLConverter(Sender);
    if assigned(aMMXMLDoc) then begin
      WriteConfigCodeToBin(cXPConfigCodeAItem);
      WriteConfigCodeToBin(cXPConfigCodeBItem);
      WriteConfigCodeToBin(cXPConfigCodeCItem);
      WriteConfigCodeToBin(cXPConfigCodeDItem);
      WriteLowerSFIToBin(cXPYMSettingSFISwitchItem);  //Nue:15.5.08  !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    end;// if assigned(aMMXMLDoc) then begin
  end;// if Sender is TMMXMLConverter then begin
end;

//:-----------------------------------------------------------------------------
procedure TTXNWriter.XML2BinEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
begin
  // TODO wss: XML: Prodgrp abh�ngige Daten/Events abf�llen
  if AnsiSameText(aEventRec.CalcElementName, cXPAssignModeItem) then
    aEventRec.Value := mXMLSettings.AssignMode
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
    aEventRec.Value := mXMLSettings.Group // F�r ZE muss die Gruppe nicht angepasst werden, da die Berechnung im Mapfile definiert ist
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
    aEventRec.Value := mXMLSettings.ProdGrpID
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
    aEventRec.Value := mXMLSettings.PilotSpindles
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPYarnCountItem) then
    // wss: Beim Download muss die Garnnummer noch von Nm zur global eingestellten Garnunit umgerechnet werden
    //      damit die ZE die Coarse/Fine Werte richtig berechnen kann
    // Bedingung: im mXMLSettings Record ist die globale Garnunit
    aEventRec.Value := YarnCountConvert(yuNm, mXMLSettings.YarnCntUnit, mXMLSettings.YarnCnt)
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPYarnUnitItem) then
    aEventRec.Value := cYarnUnitNames[mXMLSettings.YarnCntUnit]
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPThreadCountItem) then
    aEventRec.Value := mXMLSettings.NrOfThreads
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPLengthWindowItem) then
    aEventRec.Value := mXMLSettings.LengthWindow
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPLengthModeItem) then
    (* Im Record kommt z.B. eine 1 (=lwmFirst) aus dem SettingsRecord welches per
       GetEnumName in einen Text umgewandet wird. Dieser Text wird dann per Konverter
       wiederum anhand der Enum-Liste in den richtigen Bin�rwert umgesetzt. *)
    aEventRec.Value := GetEnumName(TypeInfo(TLengthWindowMode), mXMLSettings.LengthMode)
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
    (* In der ZE wird offenbar der Spindel Bereich als 16Bit Wert (Word) behandelt. Deshalb
       m�sste beim Download ein Swapping stattfinden. Da die beiden Werte bei uns aber als
       zwei einzelne Bytes behandelt werden kann dies nicht stattfinden, weil sonst der Offset f�r
       Down- und Upload verschieden w�ren. Deshalb wird hier der jeweils andere Wert abgef�llt.
       (siehe auch SetTelegrammHeader()) *)
    aEventRec.Value := mXMLSettings.SpindleLast
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
    // Kommentar bei cFirstSpindleItem beachten
    aEventRec.Value := mXMLSettings.SpindleFirst
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
    aEventRec.Value := mXMLSettings.SpeedRamp
  else
  if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
    aEventRec.Value := mXMLSettings.Speed;
end;
//:-----------------------------------------------------------------------------
end.

