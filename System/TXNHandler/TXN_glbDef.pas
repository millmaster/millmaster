(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXN_glbDef.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 14.07.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 03.03.2005  1.11  Wss | Umbau nach 0-basiertes Gruppen/Settingshandling innerhalb MM-System
| 27.04.2005  1.11  Wss | BlockTimeOut auf 5 Sekunden erh�ht (Siehe Kommentar bei der Konstante)
| 08.06.2005  1.12  khp | Alle Texnetspez. Datentypen von YMDataDef �bernommen
| 28.06.2005  1.12  Wss | Texnet Blocktimeout kann �ber die Registry �bersteuert werden
| 04.07.2005  1.13  khp | Standard: BlockTimeOut neu 10 anstatt 5 Sekunden
|=========================================================================================*)

unit TXN_glbDef;

interface

uses windows,
  IpcClass, BaseGlobal, TXN_PCI_Def;

const
  cWriteInOutDebug = 'WriteBinaryInOutDebug';

const

  // TexnetHandler Parameter =====================================================
  cMaxZESpdGroups       = 12;
  cMinTxnThread         = 2;    // Minimal Thread Anzahl TxnWriter, TxnControl
  cMaxTelegramsize      = 2000;
  cMaxErrorCount        = 10;
  // 5 Sekunden (alt 3s), um etwas spazig auf dem TXN Netz zu haben (Rabisch oder ZE besch�ftigt)
  // Auf dem TXN Transfer (Kabel) sind die Blocks der Maschinen wild durcheinander
  // -> kein fortlaufender Empfang eines kompletten Telegramms
  //TXN Block: Sender:133, Receiver:1, BlockNr:1, TotBlocks:3
  //TXN Block: Sender:128, Receiver:1, BlockNr:3, TotBlocks:3
  //TXN Block: Sender:133, Receiver:1, BlockNr:2, TotBlocks:3
  //TXN Block: Sender:133, Receiver:1, BlockNr:3, TotBlocks:3
  //TXN Block: Sender:128, Receiver:1, BlockNr:1, TotBlocks:3
  cBlockTimeOut: Integer = 10;
  cAssinTimeOut         = 120;  // 120 Sekunden
  cMachinetickertime    = 6;    // 6*10 Sekunden
  cOnOffLineRefreshtime = 10;   // 1 Sekunden

  cBinBufferSize        = 400;  // Bytes

  // Texnet Adapter Parameter ====================================================
    { Kontrollcodes }
  IOCTL_TEXNET_ROMCHECKSUM = $0222004;
  IOCTL_TEXNET_VERSION = $0222008;
  IOCTL_TEXNET_ADDRESS = $022200C;
  IOCTL_TEXNET_ERRSTATE = $0222010;
  IOCTL_TEXNET_NNSTABLE = $0222014;
  IOCTL_TEXNET_MEMACCESS = $0222018;
  IOCTL_TEXNET_RESET = $022201C;
  IOCTL_TEXNET_LASTERROR = $0222020;
  IOCTL_TEXNET_CANCELREAD = $0222024;

  { Texnet Errorcodes }
type
  TXNErrortype = record
    code: Byte;
    MsgText: string;
  end;

  TErrorCountRec = record
    PaFrameError: Byte;
    BCCError: Byte;
  end;

const
  cErrorCountInit: TErrorCountRec = (PaFrameError: 0; BCCError: 0);

  TxnErrors: array[0..20] of TXNErrortype =
    ((code: $00; MsgText: 'Invalid Texnet error code'),
    (code: $01; MsgText: 'Invalid Texnet error code'),
    (code: $02; MsgText: 'Invalid Texnet error code'),
    (code: $03; MsgText: 'Invalid Texnet error code'),
    (code: $04; MsgText: 'Invalid Texnet error code'),
    (code: $05; MsgText: 'Driver Mapped Memory could not installed'),
    (code: $06; MsgText: 'Driver IRQ could not installed'),
    (code: $07; MsgText: 'Driver invalid values in Registry : IRQ / IO Range'),
    (code: $08; MsgText: 'Invalid Texnet error code'),
    (code: $09; MsgText: 'Driver Invalid command code (DeviceIoControl)'),
    (code: $0A; MsgText: 'Driver Not enough user buffer'),
    (code: $0B; MsgText: 'Driver Timeout, wait for Inputsemaphore (pending read-/write-process)'),
    (code: $0C; MsgText: 'Driver Timeout, Device not ready'),
    (code: $0D; MsgText: 'Driver Timeout, read or write command (wait for Device response)'),
    (code: $0E; MsgText: 'Device Request could not be done'),
    (code: $0F; MsgText: 'Device Receiver from ZE is busy, probability no response from ZE'),
    (code: $10; MsgText: 'Device Receiver from ZE does not response'),
    (code: $11; MsgText: 'Device Invalid function call'),
    (code: $12; MsgText: 'Device Protocol failure in network'),
    (code: $13; MsgText: 'Driver Operation failt (Thread termination)'),
    (code: $14; MsgText: 'Invalid Texnet error code'));

  // Texnetadapter Registry-Parameter ============================================

  cTexnetRegName: array[1..cMaxTxnAdapter] of
    string = ('Device0',
    'Device1',
    'Device2');

  cTexnetDeviceName: array[1..cMaxTxnAdapter] of
    string = ('TexNet0',
    'TexNet1',
    'TexNet2');

  cTexnetRegPath = '\System\CurrentControlSet\Services\TexNet\Parameters\';

  // Telegram flagRegister BITSET8 ===============================================

  cSynchron = 0;                        // Bit 0: synchronized to acqInterval  0=async, 1=sync
  cView = 1;                            // Bit 1: data read or viewed          0=read,  1=view
  cCopsEOT = 2;                         // Bit 2: END OF transmission    flag 0=none,  1=last transmission
  cConfirmDisable = 3;                  // BIT 3: ConfirmDisable         flag 0=enable,1=disable
  cDelete = 4;                          // Bit 4: Create new interval without transmiting data

  // MsgList======================================================================

  cNotValid = -1;
  cMachDummy = 0;
  cDummySpindle = 0;
  // name identifiers for access in JobListValues
  cJobValueJobTyp        = 'JobTyp';
  cJobValueAllXMLValues  = 'AllXMLValues';
//  cJobValueProdID        = 'ProdID';
  cJobValueSettingsCount = 'SettingsCount';
  cJobValueXMLMaConfig   = 'XMLMaConfig';

type
  eOpenMode = (omRead, omWrite, omReadWrite);


  // Datenblock Record des Texnet-Telegramms =====================================

  TBlockStatus = (bsNotvalidblock, bsValidblock, bsTelegramComplete);
  TdatenArray = array[1..284] of Byte;
  THeaderRec = record
    sendNode: Byte;                     // Absender
    receiveNode: Byte;                  // Empfaenger
    blockNumber: Byte;                  // Laufende Block Nummer
    TotalBlocks: Byte;                  // Anzahl Bloecke in diesem Telegramm
  end;
  TDatablockRec = record
    headerRec: THeaderRec;
    datenArray: TdatenArray;
  end;


  // Telegram Header record ======================================================

  eTelegramIdT = (eEMPTY_TELEGRAM,      // 0
    eREQUEST_TELEGRAM,                  // 1
    eEXPRESS_TELEGRAM,                  // 2
    eCONFIRM_TELEGRAM,                  // 3
    eMESSAGE_TELEGRAM,                  // 4
    eACKNOWLEDGE);                      // 5

  eTelegramSubIdT = (OS2_eDUMMY_DATA,   // 0
    OS2_eMONITOR_DATA,                  // 1
    OS2_eBASE_DATA,                     // 2
    OS2_eDEFECT_DATA,                   // 3   // base and defect data
    OS2_eSIRO_DATA,                     // 4   // base and defect and siro data
    OS2_eGROUP_DATA,                    // 5
    OS2_eSETTINGS,                      // 6
    OS2_eDECLARATIONS,                  // 7
    OS2_eGRP_SCREENS,                   // 8    // not used for MillMaster
    OS2_eSPDL_SCREENS,                  // 9    // not used for MillMaster
    OS2_eCOPS_DATA,                     // 10   // Copsdata from each Spinn Unit
    OS2_eMACONFIG_DATA,                 // 11   // Machine Configuration data
    f12, f13, f14, f15, f16, f17, f18, f19, //Spare
    eDUMMY_DATA,                        // 20
    eMONITOR_DATA,                      // 21
    eBASE_DATA,                         // 22
    eDEFECT_DATA,                       // 23   // base and defect data
    eSIRO_DATA,                         // 24   // base and defect and siro data
    eGROUP_DATA,                        // 25
    eSETTINGS,                          // 26
    eDECLARATIONS,                      // 27
    eGRP_SCREENS,                       // 28    // not used for MillMaster
    eSPDL_SCREENS,                      // 29    // not used for MillMaster
    eCOPS_DATA,                         // 30   // Copsdata from each Spinn Unit
    eMACONFIG_DATA);                    // 31   // Machine Configuration data

  // Spindle range ==============================================================


  TSpindleRange = record
    firstSpindle: Byte;
    lastSpindle: Byte;
  end;

  // Declaration Record ==========================================================

  eDecEventT = (eEVT_NONE,              // 1 Byte
    eEVT_INITIAL_RESET,
    eEVT_RESET,
    eEVT_ENTRY_LOCKED,
    eEVT_ENTRY_UNLOCKED,
    eEVT_MODIFIED_RANGE,                // not used by MM
    eEVT_MODIFIED_SETTINGS,             // not used by MM
    eEVT_ADJUST);

  TmodifyReg = BITSET16;                // 0 = noChange,  1 = modified
  // Bit 0: spindle range
  // Bit 1: parameter
  // Bit 2: adjusted
  // Bit 3:
  // Bit 4:
  // Bit 5:
  // Bit 6:
  // Bit 7:

  Tstate = BITSET16;
  // Bit 0: state of Key entry: 0 = unlocked, 1 = locked
  // Bit 1:
  // Bit 2:
  // Bit 3:
  // Bit 4:
  // Bit 5:
  // Bit 6:
  // Bit 7:

// OS2 Declaration Struktur
  POS2declaration = ^TOS2declaration;
  TOS2declaration = packed record
    event: eDecEventT;
    fill: BYTE;
    states: Tstate;
    spare: array[1..3] of WORD;
    validity: array[0..cMaxZESpdGroups-1] of packed record
      modify: TmodifyReg;
      GrpID: Word;                      // MMOS2 GrpID: Word
      range: TSpindleRange;
    end;
  end;

  // NT Declaration Struktur
  PDeclaration = ^TDeclaration;
  TDeclaration = packed record
    event: eDecEventT;
    fill: BYTE;
    states: Tstate;
    spare: array[1..3] of WORD;
    validity: array[0..cMaxZESpdGroups-1] of packed record
      modify: TmodifyReg;
      prodGrpID: Longint;               // MM/NT prodGrpID: Longint
      range: TSpindleRange;
    end;
  end;

  // Texnet Basisrecord = 118 byte ============================================
  TTxnBaseData = packed record
    status: Byte;                      // bit0=0: konsistent, bit0=1: inkonsistent
    fill0: Byte;
    watchTime: Cardinal;
    operatingTime: Cardinal;
    runTime: Cardinal;
    length: Cardinal;
    nepCuts: Byte;
    shortCuts: Byte;
    longCuts: Byte;
    thinCuts: Byte;
    shortClusterCuts: Byte;  //SpectraPlus  former clusterCuts: Byte;
    offCountCuts: Byte;
    offCountLocks: Byte;
    siroCuts: Byte;
    totalYarnCuts: Word;
    systemCuts: Byte;
    systemLocks: Byte;
    bunchCuts: Byte;
    cones: Byte;
    cops: Word;
    yarnBreaks: Byte;
    delBunchCuts: Byte;
    splices: Word;
    spliceCuts: Byte;
    spliceRep: Byte;
    upperYarnCuts: Byte;
    longStops: Byte;
    longStopTime: Cardinal;
    shortClustArea: Cardinal;
    longClustArea: Cardinal;
    thinClustArea: Cardinal;
    shortOffCountCuts: Byte;      //former shortCountCuts: Byte;  former warnings: Byte;
    shortOffCountLocks: Byte;     //former shortCountLocks: Byte; former alarms: Byte;
    systemEvents: Byte;
    siroLocks: Byte;
    diaMeanValue: Word;           //diameter mean value nicht MillMaster
    diaStrdDevi: Word;            //diameter standard deviation nicht MillMaster
    AdjustBase: Word;
    shortClusterLocks: Byte;      //SpectraPlus  fromer clusterLocks: Byte;
    sfiCuts: Byte;
    sfiLocks: Byte;
    fill1: Byte;
    variCnt: Word;
    variTotal: Cardinal;
    siroClusterCuts: Byte;
    siroClusterLocks: Byte;
    spare1: array[1..10] of Byte;
    longClusterCuts: Byte;         //SpectraPlus  former fill2: Word;
    longClusterLocks: Byte;        //SpectraPlus  former fill2: Word;
    thinClusterCuts: Byte;         //SpectraPlus  former spare: array[9] of CARDINAL;
    thinClusterLocks: Byte;        //SpectraPlus  former spare: array[9] of CARDINAL;
    spare2: array[1..6] of CARDINAL;
  end;

// Size for Cut and Uncut data arrays...........................................
const
  cYarnDefectSize      = 264;
  cYarnCutDefectSize   = 128;
  cSpliceDefectSize    = 144;
  cSpliceCutDefectSize = 128;
  cSIRODefectSize      = 90;
  cSIROCutDefectSize   = 64;

//...Constant Arrays for unpacking of Cut and Uncut data........................
// The arrays defines how many bytes on which position has to be read
// Only for ZE (TXNHandler) Nue/Kue:26.05.05
  cTXNYarnDefXArray: array[1..128] of byte =
    (4, 4, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,

    4, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 4, 4, 2,
    2, 4, 4, 4, 4, 2, 2, 2,
    2, 2, 2, 4, 4, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 1, 1,
    2, 2, 2, 2, 2, 2, 1, 1,
    1, 1, 2, 2, 2, 2, 1, 1);

  cTXNSpliceDefXArray: array[1..128] of byte =
    (1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,

    2, 2, 2, 2, 2, 2, 2, 2,
    1, 1, 2, 2, 2, 2, 1, 1,
    1, 1, 2, 2, 2, 2, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1);

  cTXNSIRODefXArray: array[1..64] of byte =
    (4, 4, 4, 2, 2, 2, 1, 1,
    4, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 2, 2, 1, 1, 1, 1,
    2, 2, 1, 1, 1, 1, 1, 1,
    2, 2, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1);

type
//------------------------------------------------------------------------------
  PTXNDefectData = ^TTXNDefectData;
  TTXNDefectData = packed record
    yarnDefect: array[1..cYarnDefectSize] of BYTE;
    cutYarnDef: array[1..cYarnCutDefectSize] of BYTE;
    spliceDefect: array[1..cSpliceDefectSize] of BYTE;
    cutSpliceDef: array[1..cSpliceCutDefectSize] of BYTE;
  end;

//------------------------------------------------------------------------------
  PTXNSiroData = ^TTXNSiroData;
  TTXNSiroData = packed record
    siroDefect: array[1..cSIRODefectSize] of BYTE;
    cutSiroDef: array[1..cSIROCutDefectSize] of BYTE;
  end;
  
//------------------------------------------------------------------------------
  PSpdTXNRec = ^TSpdTXNRec;
  TSpdTXNRec = packed record
    baseData: TTxnBaseData;
    defectData: TTXNDefectData;
    siroData: TTXNSiroData;
  end;

// Texnet Telegramm ============================================================

  TOS2telegramHeader = packed record    // 10 Byte
    telegramID: eTelegramIdT;
    telegramSubID: eTelegramSubIdT;
    spindleRange: TSpindleRange;
    machineNumber: BYTE;
    flagRegister: BITSET8;
    confirmID: Word;                    // MM/OS2
    length: WORD;                       // The sender must swap the value
  end;                                  // Length = TelegramHead and TelegramBody

  TTelegramHeader = packed record
    telegramID: eTelegramIdT;
    telegramSubID: eTelegramSubIdT;
    spindleRange: TSpindleRange;
    machineNumber: BYTE;
    flagRegister: BITSET8;
    JobID: DWord;                       // The sender must swap the value -- MM/NT JobID: DWord
    length: WORD;                       // The sender must swap the value
  end;                                  // Length = TelegramHead and TelegramBody


  TTelegram = packed record
    case Integer of
      0: (Header: TTelegramHeader;
          Daten: array[1..cMaxTelegramsize - sizeof(TTelegramHeader)] of Byte);
      1: (All: array[1..cMaxTelegramsize] of Byte);
      2: (OS2Header: TOS2telegramHeader;
          OS2Daten: array[1..cMaxTelegramsize - sizeof(TOS2TelegramHeader)] of Byte);
  end;


//------------------------------------------------------------------------------
//Alle Methode dieser Basisklasse sind abstract dh.Die Implementierung wird erst spaeter
// in einer abgeleiteten Klasse durchgefuehrt.

  eDevModeT = (eNone, ePCI, eISA);      // Wird gesetzt sobald der Kartentype erkannt ist

  TTXNBaseDevHandler = class(TObject)
  PROTECTED
    fReadTimeout: Integer;
    fErrorRec: TErrorRec;
    fErrorCounters: TErrorCountRec;
    fDeviceErrorCount: Integer;
  PUBLIC
    constructor Create; VIRTUAL;
    function ISAConnect(aName: string; aOpenMode: eOpenMode): Boolean; VIRTUAL; ABSTRACT;
    function PCIConnect(aPCIDevInfTab: DEVICE_LOCATION): Boolean; VIRTUAL; ABSTRACT;
    function TxnRead(var aMsgBlock: TDatablockRec; var aMsglengh: Integer): Boolean; VIRTUAL; ABSTRACT;
    function TxnWrite(aMsgBlock: TDatablockRec; aDatablockLengh: Integer): Boolean; VIRTUAL; ABSTRACT;
    function TxnDisconnect: Boolean; VIRTUAL; ABSTRACT;
    function TxnDevControl(aCtrlCode: DWord; out aOutBuf: array of Byte;
      out aOutBufLength: Integer): Boolean; VIRTUAL; ABSTRACT;
    function GetLastTxnError(): Integer; VIRTUAL; ABSTRACT;
    property ErrorRec: TErrorRec READ fErrorRec;
    property ReadTimeout: Integer READ fReadTimeout WRITE fReadTimeout;
    property ErrorCounters: TErrorCountRec READ fErrorCounters WRITE fErrorCounters;
    property DeviceErrorCount: Integer READ fDeviceErrorCount WRITE fDeviceErrorCount;
  end;

procedure SaveBinFile(aBinData: PByte; aLen: integer; aPrefix: string);


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, filectrl, classes, sysutils, LoepfeGlobal;

//:-----------------------------------------------------------------------------
function GetNewTempFileName(aPrefix: string): string;
var
  xTempPath: string;
begin
  result := '';
  // Ein Unterordner wird f�r das Millmaster erstellt
  xTempPath := 'C:\Temp\MMConverter\';
  if not(DirectoryExists(xTempPath)) then
    ForceDirectories(xTempPath);

  SetLength(result, MAX_PATH);
  // Erzeugt einen neuen Filenamen
  GetTempFileName(PChar(xTempPath), PChar(aPrefix), 0, PChar(result));
end;

procedure SaveBinFile(aBinData: PByte; aLen: integer; aPrefix: string);
var
  xFileName: string;
  xStream: TFileStream;
begin
  (* Zu Debugzwecken kann eine Datei im Temp-Verzeichnis angelegt werden. Diese Einstellung wird mit
     einem eintrag in der Registry festgelegt (cWriteInOutDebug=true). Die Einstellung gilt f�r In- und Output *)
  aPrefix := copy(aPrefix, 1, 2);
  // liefert den kompleten Dateinamen mit Pfad zum Temp Verzeichnis
  xFileName := GetNewTempFileName(aPrefix + 'B');

  // File schreiben
  xStream := nil;
  try
    xStream := TFileStream.Create(xFileName, fmCreate	or fmShareDenyWrite);
    // Schreibt den gesammten Buffer in das File
    xStream.Write(aBinData^, aLen);
  finally
    xStream.Free;
  end;// try finally
end;

{ TTxnBaseDevHandler }

constructor TTXNBaseDevHandler.Create;
begin
  inherited Create;
  FReadTimeout := cPCIReadtimeoutinit;
  with fErrorRec do begin
    ErrorTyp := etNoError;
    Error := 0;                         // Contains error from NT error list or DB error list
    Msg := '';
  end;
end;

initialization
  // Texnet Blocktimeout kann �ber die Registry �bersteuert werden
  cBlockTimeOut := GetRegInteger(cRegLM, cRegMMTimeouts, 'TXNBlockTimeOut', cBlockTimeOut);
end.

