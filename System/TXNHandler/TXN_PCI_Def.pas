(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: TXN_PCI_Def.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: Definition Unit PCITexnet NAD
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.08.2001  0.00  khp | Datei erstellt
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 22.04.2004  3.01  khp | Timeout an Schlafhorst Spezi angepasst.
|                       |  cPCIWritetimeout, cPCIControltimeout von 0.2 auf 0.4 sek angepasst.
|
|==========================================================================================*)
unit TXN_PCI_Def;

interface
uses windows;
const

  // NAD (Network Adapter) specific defines
  TEXBASE_NAD_DUALMEMSIZE = $400;
  TEXBASE_MAX_LGDLENGTH = 288;          // Max Long DATA Length, [byte]

  // NAD datatypes
  TEXBASE_NAD_LONGDATA = 0;             // Langdaten Record an Texnet Teilnehmer
  TEXBASE_NAD_SHORTDATA = 1;            // Kurzdaten Record an Texnet Teilnehmer


  // NAD register bits
  TEXBASE_NAD_INTO = $80;               // Schreiben durch Host erzeugt Interrupt beim NAD, REGO
  TEXBASE_NAD_INTI = $80;               // Schreiben durch NAD erzeugt Interrupt beim Host, REGI

  TEXBASE_NAD_OINTO = $80;              // Host signalisiert Anforderung im Out-Kanal an NAD, OREGO
  TEXBASE_NAD_OINTI = $80;              // NAD bestaetigt einen Schreibeauftrag beim Host, OREGI
  TEXBASE_NAD_RMOEMP = $01;             // NAD quittiert die Uebernahme der Daten, OREGI

  TEXBASE_NAD_IINTO = $80;              // Host signalisiert Leseanforderung im IN-Kanal an NAD, IREGO
  TEXBASE_NAD_RMIEMP = $01;             // Input Handshake RAM EMPTY Flag, IREGO
  TEXBASE_NAD_IINTI = $80;              // NAD bestatigt einen Leseauftrag beim HOST, IREGI

  TEXBASE_NAD_RFLAG = $80;              // RESET Flag


  // NAD error codes
  TEXBASE_NAD_NOERR = $00;              // kein Fehler
  TEXBASE_NAD_ERRUPRK = $8E;            // nicht erfuellbare Anforderung
  TEXBASE_NAD_ERRBSY = $90;             // Block konnte nicht uebertragen werden Empfaenger busy
  TEXBASE_NAD_ERRQUIT = $91;            // Block konnte nicht uebertragen werden,Zieladresse dreimal hintereinander keine Antwort
  TEXBASE_NAD_ERRFKT = $94;             // Ungueltiger Funktionsaufruf
  TEXBASE_NAD_ERRNPRK = $99;            // Protokollfehler auf Netzwerk


  // function commands
  TEXBASE_NAD_FUNCROMCHECKSUM = $02;    // Rom Checksumme
  TEXBASE_NAD_FUNCVERSION = $03;        // Version
  TEXBASE_NAD_FUNCADDRESS = $04;        // Eingestellte Adresse
  TEXBASE_NAD_FUNCERRSTATE = $05;       // Fehler-Status
  TEXBASE_NAD_FUNCNNSTABLE = $07;       // NNS-Tabelle lesen
  TEXBASE_NAD_FUNCMEMACCESS = $1B;      // Liest Block BL aus Hauptspeicher des Adapters


type
  TDevError = (ewrite, eread);
  // Layout of Output Control Block
  tTEXBASE_OUTCTRL = packed record
    soerr: Byte;                        // Error Code
    unused: array[0..10] of Byte;       // 11Byte unused
    solenh: Byte;                       // High byte: length of Output Data
    solenl: Byte;                       // Low byte: length of Output Data
    soprtk: Byte;                       // Protokolltype
    sodead: byte;                       // Destination Address
  end;

  // Layout of Input Control Block
  tTEXBASE_INPCTRL = packed record
    sierr: Byte;                        // Error Code
    unused: array[0..10] of Byte;       // 11Byte unused
    silenh: Byte;                       // High byte: length of Input Data
    silenl: Byte;                       // Low byte: length of Input Data
    siprtk: Byte;                       // Protokolltype
    sisoad: Byte;                       // Source Address
  end;

  // Layout of TEXNET Dual Port Ram
  pTEXBASE_DUALPORT = ^tTEXBASE_DUALPORT;
  tTEXBASE_DUALPORT = packed record
    dataIn: array[0..TEXBASE_MAX_LGDLENGTH - 1] of Byte; // Input Data Block
    unused0: array[0..94] of Byte;      // 95Byte unused
    reset: Byte;                        // Reset Location
    dataOut: array[0..TEXBASE_MAX_LGDLENGTH - 1] of Byte; // Ouput Data Block
    unused1: array[0..314 - 1] of Byte; // 314Byte unused
    stati: tTEXBASE_INPCTRL;            // Input Control Block
    stato: tTEXBASE_OUTCTRL;            // Output Control Block
    iregi: Byte;                        // Acknolege to Input Channel, from NAD
    irego: Byte;                        // Input Handshake Register, to NAD
    oregi: Byte;                        // Output Handshake Register, to NAD
    orego: Byte;                        // Demand to Output Channel, to NAD
    regi: Byte;                         // NAD erzeugt Input Interrupt beim Host
    rego: Byte;                         // Host erzeugt Output Interrupt beim NAD
  end;





  // Konstanten, Datentypen, Functionen und Proceduren fuer den Zugriff
  // auf die PLXAPI.DLL welche fuer den Zuriff auf die PCITexnetkarte benoetigt wird.


  //******************************************************************************
  //  TypeConvert to Delphi
  //******************************************************************************
type

  //*++++++++++ Define PLX PCI SDK Basic Types ++++++++++++++++++++++++++++++++++*/
  U8 = Byte;                            //8 Bit, ohne Vorzeichen
  U16 = Word;                           //16 Bit ohne Vorzeichen,
  U32 = Longword;                       //32 Bit, ohne Vorzeichen,
  U64 = Longword;                       //32 Bit, ohne Vorzeichen

  DEVHANDLE = THandle;
  PDEVHANDLE = ^DEVHANDLE;

  //******************************************************************************
  //  Const-Deklarationen
  //******************************************************************************
const
  // PLX Deviceinfo const
  DEVICE_ID = $3001;
  VENDOR_ID = $10B5;
  cPCIReadtimeout = 60000;              // 60 Sekunden
  cPCIReadtimeoutinit = 1000;           // 1 Sekunden
  cPCIWritetimeout = 400;               // 0,2 Sekunde  ->new 0.4 sek
  cPCIControltimeout = 400;             // 0,2 Sekunde  ->new 0,4 sek
  cPCITexnetRegPath = '\System\CurrentControlSet\Services\Pci9030\';
  cPCITexnetValue = 'RequestedSystemResources';
  clastPCISlot = 32; 

  FIND_AMOUNT_MATCHED = U32(80001);
  // Define -1 values for the different types */
  MINUS_ONE_LONG = U32($FFFFFFFF);
  MINUS_ONE_SHORT = U16($FFFF);
  MINUS_ONE_CHAR = U8($FF);
  ApiSuccess = 512;
  ApiFailed = 513;
  ApiAccessDenied = 514;
  {  ApiDmaChannelUnavailable,
     ApiDmaChannelInvalid,
     ApiDmaChannelTypeError,
     ApiDmaInProgress,
     ApiDmaDone,
     ApiDmaPaused,520
     ApiDmaNotPaused,
     ApiDmaCommandInvalid,
     ApiDmaManReady,
     ApiDmaManNotReady,
     ApiDmaInvalidChannelPriority,
     ApiDmaManCorrupted,
     ApiDmaInvalidElementIndex,
     ApiDmaNoMoreElements,
     ApiDmaSglInvalid,
     ApiDmaSglQueueFull,530
     ApiNullParam,
     ApiInvalidBusIndex,
     ApiUnsupportedFunction,
     ApiInvalidPciSpace,
     ApiInvalidIopSpace,
     ApiInvalidSize,
     ApiInvalidAddress,
     ApiInvalidAccessType,
     ApiInvalidIndex,                /* Invalid IOP Outbound Index */
     ApiMuNotReady,540
     ApiMuFifoEmpty,
     ApiMuFifoFull,
     ApiInvalidRegister,
     ApiDoorbellClearFailed,
     ApiInvalidUserPin,
     ApiInvalidUserState,
     ApiEepromNotPresent,
     ApiEepromTypeNotSupported,
     ApiEepromBlank,
     ApiConfigAccessFailed,550 }
  ApiInvalidDeviceInfo = 551;
  ApiNoActiveDriver = 552;
  ApiInsufficientResources = 553;
  ApiObjectAlreadyAllocated = 554;
  ApiAlreadyInitialized = 555;
  ApiNotInitialized = 556;
  ApiBadConfigRegEndianMode = 557;
  ApiInvalidPowerState = 558;
  ApiPowerDown = 559;
  ApiInvalidHandle = 572;
  ApiBufferNotReady = 573;
  ApiInvalidData = 574;

// PLX_INTR_MASK= $200003000000;  // PciMainInt(bit24,25) & IopToPciInt(bit45)
  PLX_INTR_MASK = $200001000000;        // PciMainInt(bit24) & IopToPciInt(bit45)



  //******************************************************************************
  //  Type-Deklarationen und Type-Definitionen
  //******************************************************************************
type
  //*++++++++++ PCI SDK Defined Structures ++++++++++++++++++++++++++++++++++++++*/

  // PLX Interrupt Structure
  PLX_INTR = int64;                     // 8 Byte Struktur

  // Device Location Structure
  PDEVICE_LOCATION = ^DEVICE_LOCATION;
  DEVICE_LOCATION = packed record
    DeviceId: U32;
    VendorId: U32;
    BusNumber: U32;
    SlotNumber: U32;
    SerialNumber: array[0..15] of U8;
  end;

  // Virtual Addresses Structure
  PVIRTUAL_ADDRESSES = ^VIRTUAL_ADDRESSES;
  VIRTUAL_ADDRESSES = packed record
    Va0: U32;
    Va1: U32;
    Va2: U32;
    Va3: U32;
    Va4: U32;
    Va5: U32;
    VaRom: U32;
  end;

  // PCI Memory  Structure
  PPCI_MEMORY = ^PCI_MEMORY;
  PCI_MEMORY = packed record
    UserAddr: U32;
    PhysicalAddr: U64;
    Size: U32;
  end;



{$MINENUMSIZE 4}                        // Achtung alle Enum Types 4 Byte 32 Bit
  {========================================================}

  // Exported function from PLXAPI.DLL
  // Immer VAR bei C-Pointer !!!

function PlxPciDeviceFind(var deviceinfo: DEVICE_LOCATION; var plxDeviceCount: integer): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxPciDeviceFind';


function PlxPciDeviceOpen(var deviceinfo: DEVICE_LOCATION; var devicehandle: DEVHANDLE): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxPciDeviceOpen';

function PlxPciDeviceClose(devicehandle: DEVHANDLE): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxPciDeviceClose';


function PlxPciBaseAddressesGet(devicehandle: DEVHANDLE; var virtAddr: VIRTUAL_ADDRESSES): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxPciBaseAddressesGet';


function PlxIntrDisable(devicehandle: DEVHANDLE; var plxIntr: PLX_INTR): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxIntrDisable';


function PlxIntrEnable(devicehandle: DEVHANDLE; var plxIntr: PLX_INTR): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxIntrEnable';


function PlxIntrAttach(devicehandle: DEVHANDLE; plxIntr: PLX_INTR; var EventHandle: DEVHANDLE): Integer;
cdecl; external 'PLXAPI.DLL' name 'PlxIntrAttach';


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.

 
