(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXN_ISA_DevClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: P1,P2 mit allem Zugriffsroutinen
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 12.01.1999  0.00  khp | Datei erstellt
| 12.01.1999  0.00  khp | Inital Release
| 16.04 1999  2.01  khp | In TxnWrite und TxnDeviceControl 2. Versuch nach 50 msec
| 12.07.1999  2.02  khp | Driver Timeout, Device not ready'Driver Timeout,read or write command
|                       | (wait for Device response) fuehren zu keiner Fehlermeldung),
| 03.10.2001  2.1   khp | TTxnDevHandler Class in seperater Unit 'TXN_ISA_DevClass'
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
|==========================================================================================*)

unit TXN_ISA_DevClass;
interface

uses windows, Classes, SysUtils,
  IpcClass, TXN_glbDef, Baseglobal, TXN_PCI_Def;     // MM System units

type

  //------------------------------------------------------------------------------
  { TTxnDevHandler class}

  TMsgIOControl = array[0..256] of Byte; //prov

  TTxnDevHandler = class(TTxnBaseDevHandler) // TObjekt
  PRIVATE
    mTxnHandle: THandle;
    mOpenMode: eOpenMode;
  PUBLIC
    constructor Create; override;
    function ISAConnect(aName: string; aOpenMode: eOpenMode): Boolean; override;
    function PCIConnect(aPCIDevInfTab: DEVICE_LOCATION): Boolean; override;
    function TxnRead(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean; override;
    function TxnWrite(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean; override;
    function TxnDisconnect: Boolean; override;
    function TxnDevControl(aCtrlCode: DWord; out aOutBuf: array of Byte;
      out aOutBufLength: Integer): Boolean; override;
    function GetLastTxnError(): Integer; override;
  end;



implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//==============================================================================

{ TTxnDevHandler class}

constructor TTxnDevHandler.Create;
begin
  inherited Create;
  mTxnHandle := INVALID_HANDLE_VALUE;
end;

function TTxnDevHandler.ISAConnect(aName: string; aOpenMode: eOpenMode): Boolean;
const
  cTxnDevRef = '\\.\';                  // Referenzierung fuer Treiber
var
  xTxnDevName: string;
  xAccess, xShareMode, xAttribute: DWord;
begin
  case aOpenMode of
    omRead: begin
        mOpenMode := omRead;
        xAccess := GENERIC_READ;
        xShareMode := FILE_SHARE_WRITE; // allows to write
      end;
    omWrite: begin
        mOpenMode := omWrite;
        xAccess := GENERIC_WRITE;
        xShareMode := FILE_SHARE_READ;  // allows to read
      end;
  else                                  // omReadWrite
    mOpenMode := omReadWrite;
    xAccess := GENERIC_READ or GENERIC_WRITE;
    xShareMode := FILE_SHARE_READ or FILE_SHARE_WRITE; // allows to read and write
  end;

  xAttribute := FILE_ATTRIBUTE_NORMAL;
  xTxnDevName := cTxnDevRef + aName;
  mTxnHandle := CreateFile(PChar(xTxnDevName),
    xAccess,                            // AccessMode
    xShareMode,                         // ShareMode
    nil,                                // SecurityAttributes
    OPEN_EXISTING,                      // CreationDistribution
    xAttribute,                         // Flags and Attributes
    0);                                 // TemplateFile
  if (mTxnHandle = INVALID_HANDLE_VALUE) then begin
    with fErrorRec do begin
      ErrorTyp := etTXNDrvError;
      Error := GetLastError;            // Nt Error
      Msg := 'TxnConnect';
    end;
    Result := FALSE;
  end
  else begin
    Result := TRUE;
  end;
end;                                    {TTxnDevHandler.TxnConnect}

//------------------------------------------------------------------------------

function TTxnDevHandler.TxnDevControl(aCtrlCode: DWord; out aOutBuf: array of Byte;
  out aOutBufLength: Integer): Boolean;
var
  xres: Boolean;
  i: Byte;
begin
  Result := FALSE;
  for i := 1 to 2 do begin
    xres := DeviceIoControl(mTxnHandle, // Handle to driver
      actrlCode,                        // IO Control Code
      nil,                              // Not Used Adress from aInBuf
      0,                                // Not used Size of the Input buffer
      @aOutBuf,                         // Adress from aOutBuf
      Sizeof(aOutBuf),                  // Size of the Output buffer
      DWord(aOutBufLength),             // Totally read number of byte
      nil);
    if xres then begin
      Result := TRUE;
      break
    end else Sleep(50);                 // 2 Versuch
  end;
  if not xres then begin                // TxnDevControl failt
    with fErrorRec do begin
      ErrorTyp := etNTError;
      Error := GetLastError;            // Nt Error
      Msg := 'TxnDevControl';
    end;
  end
end;                                    {TTxnDevHandler.TxnDevControl}

//------------------------------------------------------------------------------

function TTxnDevHandler.TxnDisconnect(): Boolean;
var
  xok: Boolean;
begin
  try
    xok := CloseHandle(mTxnHandle);
  except
    xok := FALSE
  end;
  result := xok;
end;

//------------------------------------------------------------------------------

function TTxnDevHandler.TxnRead(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean;
var
  xres: Boolean;
  xErrorCode: Byte;
begin
  if (mOpenMode = omRead) or (mOpenMode = omReadWrite) then begin
    xres := ReadFile(mTxnHandle,        // Handle to driver
      aMsgBlock,                        // Pointer to aMsgBlock
      Sizeof(aMsgBlock),                // Size of the aMsgBlock
      DWord(aMsglengh),                 // Totally read number of byte
      nil);                             // optional structur for asyn. transfer
    if xres then begin
      Result := TRUE;
    end
    else begin                          // Read failed or TimeOut
      Result := FALSE;
      xErrorCode := GetLastTxnError;
      with fErrorRec do begin
        if (xErrorCode = TxnErrors[$0D].code)
          or (xErrorCode = TxnErrors[$0C].code)
          or (xErrorCode = 0) then begin // TimeOut or Unknown
          ErrorTyp := etNoError;
        end else begin
          ErrorTyp := etTxnDrvError;
        end;
        Error := xErrorCode;
        Msg := TxnErrors[xErrorCode].MsgText;
      end;
    end;
  end
  else begin                            // wrong open mode
    Result := FALSE;
    with fErrorRec do begin
      ErrorTyp := etTXNDrvError;
      Error := ERROR_NOT_SUPPORTED;
      Msg := 'TxnRead, invalid OpenMode';
    end;
  end;
end;                                    {TTxnDevHandler.TxnRead}

//------------------------------------------------------------------------------

function TTxnDevHandler.TxnWrite(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean;
var
  xres: Boolean;
  xErrorCode, i: Byte;
  xpaMsgBlock: Pointer;
  xWrittenbyte: DWord;
begin
  result := FALSE;
  if (mOpenMode = omWrite) or (mOpenMode = omReadWrite) then begin
    xpaMsgBlock := @aMsgBlock;
    for i := 1 to 2 do begin
      xres := WriteFile(mTxnHandle,     // Handle to driver
        xpaMsgBlock^,                   // Pointer to aMsgBlock
        aDatablockLengh,                // Size of the aMsgBlock
        xWrittenByte,                   // Totally written number of byte
        nil);                           // optional structur for asyn. transfer
      if xres then begin
        Result := TRUE;
        break
      end else Sleep(50);               // 2 Versuch nach 50 ms
    end;
    if not xres then begin              // Write failed
      xErrorCode := GetLastTxnError;
      with fErrorRec do begin
        ErrorTyp := etTXNDrvError;
        Error := xErrorCode;
        Msg := TxnErrors[xErrorCode].MsgText;
      end;
    end;
  end
  else begin                            // wrong open mode
    with fErrorRec do begin
      ErrorTyp := etTXNDrvError;
      Error := ERROR_NOT_SUPPORTED;
      Msg := 'TxnWrite, invalid OpenMode';
    end;
  end;
end;                                    {TTxnDevHandler.TxnWrite}

//------------------------------------------------------------------------------

function TTxnDevHandler.GetLastTxnError(): Integer;
var
  xOutBuf: TMsgIOControl;
  xOutBuflengh: Integer;
begin
  result := 0;
  if TxnDevControl(IOCTL_TEXNET_LASTERROR, xOutBuf, xOutBuflengh) then begin
    result := xOutBuf[0];
  end;
  if (result < 0) or (result > 20) then result := 0;
end;

function TTxnDevHandler.PCIConnect(aPCIDevInfTab: DEVICE_LOCATION): Boolean;
begin
  // bleibt leer, nur wegen Compiler Warnings implementiert
  Result := False;
end;

end.

 
