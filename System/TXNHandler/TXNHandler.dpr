(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXNhandler.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 14.07.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion f�r alte ZE
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 05.10.2002        LOK | Umbau ADO
|=========================================================================================*)


program TXNHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
{$IFDEF MemCheck}
  MemCheck,
{$ENDIF}
  mmMBCS,
  LoepfeGlobal,
  BaseMain,
  BaseGlobal,
  ActiveX,
  mmCs,
  Windows,
  SysUtils,
  TXNWriterClass in 'TXNWriterClass.pas',
  TXNReaderClass in 'TXNReaderClass.pas',
  TXNControlClass in 'TXNControlClass.pas',
  TXNHandlerClass in 'TXNHandlerClass.pas',
  TXN_glbDef in 'TXN_glbDef.pas',
  TXNPoolClass in 'TXNPoolClass.pas',
  TXN_PCI_DevClass in 'TXN_PCI_DevClass.pas',
  TXN_PCI_Def in 'TXN_PCI_Def.pas',
  TXN_ISA_DevClass in 'TXN_ISA_DevClass.pas';

var
  xTXNHandler: TTXNHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('TxnHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
  // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
  if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
    raise Exception.Create('CoInitialize failed');
  try
    xTXNHandler := TTXNHandler.Create(ssTXNHandler);
    if xTXNHandler.Initialize then
      xTXNHandler.Run;
    xTXNHandler.Free;
  finally
    CoUninitialize;
  end;

end.

