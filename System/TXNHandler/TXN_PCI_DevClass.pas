(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: TXN_PCI_DevClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: Definition Unit PCITexnet NAD
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.08.2001  0.00  khp | Datei erstellt
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 09.10.2001  3.01  khp | InterruptHandler auf Timecritical gesetzt.
|                       | Ruecksetzen der Auftragsanforderung in CompleteWriteIoRequest
|                       | mCSTexnetIOMem: TCriticalSection bei WriteDataToNAD,ReadIoRequest,
|                       | CancelReadIoRequest,SendCtrlCodeToNAD
| 08.08.2002  3.02  khp | Bei Block�bertragungsfehlern, Fehlermeldung in englisch
| 16.09.2002  3.1   khp | Interruptbehandlung mittels PLXAPI.DLL f�hrt zu enormen Speicherfrass und kann daher zZ. nicht verwendet werden.
|                       | Daher wird in der Interruptroutine das Interruptflag des NAD's alle 10 ms gelesen.(Polling)
| 22.10.2002  3.11  khp | Neue Methode ClearErrorRec() und SetErrorRec()erstellt.
|                       | Nach Exit wird im finally von TxnWrite, TxnRead, NADfunctionCall Leave immer abgearbeitet.
| 25.11.2002  3.12  khp | Differenzierte Fehlermeldungen
| 27.02.2004  1.10  khp | Unterst�tzung von mehreren PCI-BusControllern pro PC
|
|==========================================================================================*)
unit TXN_PCI_DevClass;

interface

uses Sysutils, windows, Classes, Baseglobal, TXN_glbDef, TXN_PCI_DEF, IpcClass,LoepfeGlobal,
  syncobjs;

type
  ByteInt = record
    case boolean of
      True: (w: Word);
      False: (bytelow, bytehigh: Byte);
  end;
  TAccessMode = (eOUT, eIN);

  TTXNNadinterface = class(TObject)
  PRIVATE
    mErrorRec: TErrorRec;
    mVirMemAdr: VIRTUAL_ADDRESSES;
    mTexnetIOMem: pTEXBASE_DUALPORT;
    mCSHWAccess: TCriticalSection;
  PUBLIC
    function WaitForHardwareAccess(aAccessMode: TAccessMode): Boolean;
    function MapHWtoTexnetIOMem(aPlxDeviceHnd: DEVHANDLE): Boolean;

    function WriteDataToNAD(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean;
    function CompleteWriteIoRequest(): Boolean;

    function ReadDataFromNAD(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean;
    function ReadIoRequest(): Boolean;
    function CancelReadIoRequest(): Boolean;
    function CompleteReadIoRequest(): Boolean;

    function SendCtrlCodeToNAD(afunctionCode: Byte): Boolean;
    function ReadFunctionDataFromNAD(var aOutBuf: array of Byte; var aOutBufLength: Integer): Boolean;

    function CheckDeviceError(aDevError: TDevError): Boolean;
    function SoftReset(): Boolean;
    function DisableAllPlxIntr(aPlxDeviceHnd: DEVHANDLE): Boolean;
    function EnablePlxIntr(aPlxDeviceHnd: DEVHANDLE): Boolean;
    procedure ClearErrorRec();
    procedure SetErrorRec( aErrorTyp: TErrorTyp ;aError: DWord; aMsg: shortstring);
  end;

  Intrsignaltype = (ePlxintr, eExit);

  TPCITxnDevHandler = class(TTxnBaseDevHandler) //(TObject)
  PRIVATE
    mPlxDeviceHnd: DEVHANDLE;
    mPlxDevice: DEVICE_LOCATION;
    mPLXIntrHandler: tHandle;
    mIntrThreadready: Integer;
    mThreadID: DWORD;
    mIntrThreadExit: THandle;
    mTXNNadinterface: TTXNNadinterface;
    mNADReadIntr: THandle;
    mNADWriteIntr: THandle;
    mCSRead: TCriticalSection;
    mCSWrite: TCriticalSection;
    function IRTexNetServiceRoutine(aNumber: Pointer): LongInt; STDCALL;
  PUBLIC
    constructor Create; override;
    destructor Destroy; OVERRIDE;

    function ISAConnect(aName: string; aOpenMode: eOpenMode): Boolean; override;
    function PCIConnect(aPCIDevInfTab: DEVICE_LOCATION): Boolean; OVERRIDE;
    function TxnDisconnect: Boolean; OVERRIDE;
    function TxnRead(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean; OVERRIDE;
    function TxnWrite(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean; OVERRIDE;
    function TxnDevControl(aCtrlCode: DWord; out aOutBuf: array of Byte;
      out aOutBufLength: Integer): Boolean; OVERRIDE;

    function GetLastTxnError: Integer; OVERRIDE;
  end;

  TPCIDEVHandlerArr = array[1..cMaxTxnAdapter] of TPCITxnDevHandler;

function Amount_PCITexnetNAD_MATCHED(var aPlxDevice: DEVICE_LOCATION): Integer;
function GetPCITexnetDeviceInfo(var aPlxDevice: DEVICE_LOCATION): Boolean;



implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf;                             // CodeSite


//==============================================================================
//
// TTXNNadinterface class
// Alle Methoden dieser Klasse werden mit try-except behandelt
//==============================================================================


// This function checks, whether the nad is busy and may not be accessed

function TTXNNadinterface.WaitForHardwareAccess(aAccessMode: TAccessMode): Boolean;
var
  xcount: Integer;
begin
  // initialize waiting time to 100ms
  xcount := 0;
//  result := True;  // Wird nicht gebraucht, da Exception oder true (LOK 15.12.04)
  try
    // Warte bis kein Interrupt fuer den Ausgabeblock mehr ansteht und
    // er Schreibberechtigung auf das Dualport hat
    while (((mTexnetIOMem.rego and TEXBASE_NAD_INTO) = TEXBASE_NAD_INTO) or not ((mTexnetIOMem.oregi and TEXBASE_NAD_RMOEMP) = TEXBASE_NAD_RMOEMP)) do begin
      // wait for 100ms
      sleep(100);
      INC(xcount);
      if xcount > 10 then break;        // Total 1 Sekunde
    end;
    try
      mCSHWAccess.Enter;
      result := xcount <= 10;
      if result then begin
        if (aAccessMode = eOUT) then
          // Host belegt Ausgangsblock exklusiv
          mTexnetIOMem.oregi := mTexnetIOMem.oregi and (not TEXBASE_NAD_RMOEMP)
        else begin
          // Host gibt Leseauftrag
          result := ReadIoRequest;
        end;
      end else begin
        // Timeout von 1 Sekunde ueberschritten
        if (aAccessMode = eOUT) then begin
          CodeSite.SendFmtError('WaitForWriteAccess timeout > 1sec. rego=%d oregi=%d orego=%d', [mTexnetIOMem.rego, mTexnetIOMem.oregi, mTexnetIOMem.orego]);
          SetErrorRec(etTXNDrvError,0,'WaitForWriteAccess: timeout > 1sec');
        end else begin
          CodeSite.SendFmtError('WaitForReadAccess timeout > 1sec. rego=%d oregi=%d orego=%d', [mTexnetIOMem.rego, mTexnetIOMem.oregi, mTexnetIOMem.orego]);
          SetErrorRec(etTXNDrvError,0,'WaitForReadAccess: timeout > 1sec, Check and correct the termination of the Texnetcard, then reboot the System');
        end;
      end;
    finally
      mCSHWAccess.Leave;
    end;
  except
    on E: Exception do raise Exception.Create('WaitForHardwareAccess ' + e.message);
  end;
end;



function TTXNNadinterface.MapHWtoTexnetIOMem(aPlxDeviceHnd: DEVHANDLE): Boolean;
var
  rc: integer;
begin
  try
    rc := PlxPciBaseAddressesGet(aPlxDeviceHnd, mVirMemAdr);
    if (rc = ApiSuccess) and (mVirMemAdr.Va2 <> 0) and (mVirMemAdr.Va2 <> Longword(-1)) then begin
      // Mappen der mTexnetIOMem Struktur direkt auf die Hardware
      mTexnetIOMem := pTEXBASE_DUALPORT(mVirMemAdr.Va2);
      result := True;
    end else
      result := false;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('MapHWtoTexnetIOMem failed');
end;


// This function sends starts a write IO request in the NAD
// synchronized with the interrupt object attach to the current device,
// meaning that this function won't be interrupted

function TTXNNadinterface.WriteDataToNAD(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean;
var
  xtransfer: ByteInt;
begin
  result := true;
  try
    // copy user data to datao
    Move(aMsgBlock, mTexnetIOMem.dataOut, aDatablockLengh);
    // specify data provided in datao
    mTexnetIOMem.stato.sodead := mTexnetIOMem.dataOut[1]; // receiver address
    mTexnetIOMem.stato.soprtk := TEXBASE_NAD_LONGDATA;
    xtransfer.w := aDatablockLengh;
    mTexnetIOMem.stato.solenl := xtransfer.bytelow;
    mTexnetIOMem.stato.solenh := xtransfer.bytehigh;
    //Alle Daten gueltig,Host signalisiert Anforderung im Out-Kanal an NAD
    mTexnetIOMem.orego := mTexnetIOMem.orego or TEXBASE_NAD_OINTO;
    // Schreiben durch Host erzeugt Interrupt beim NAD
    mTexnetIOMem.rego := TEXBASE_NAD_INTO;
  except
    result := false;
  end;
  if result = false then begin
    SetErrorRec(etTXNDrvError,0,'Write to NAD failed');
    CodeSite.SendError('WriteDataToNAD failed');
  end;
end;

// This function releases ownership of the given semaphore object. To make
// this process multi processor safe a spin lock is used

function TTXNNadinterface.CompleteWriteIoRequest(): Boolean;
begin
  result := true;
  try
    // NAD-Bestaetigung des Schreibeauftrag zuruecksetzen falls gesetzt
    if ((mTexnetIOMem.oregi and TEXBASE_NAD_OINTI) = TEXBASE_NAD_OINTI) then
      mTexnetIOMem.oregi := mTexnetIOMem.oregi and (not (TEXBASE_NAD_OINTI));
    // Temp: ruecksetzen der Auftragsanforderung
    if not ((mTexnetIOMem.oregi and TEXBASE_NAD_RMOEMP) = TEXBASE_NAD_RMOEMP) then
      mTexnetIOMem.oregi := mTexnetIOMem.oregi or TEXBASE_NAD_RMOEMP;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('CompleteWriteIoRequest failed');
end;




function TTXNNadinterface.ReadDataFromNAD(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean;
var
  xNadDataSize: ByteInt;
begin
  Fillchar(aMsgBlock, SizeOf(TdatablockRec), 0);
  aMsglengh := 0;
  result := true;
  try
    // bytes, presented by the NAD
    xNadDataSize.bytelow := mTexnetIOMem.stati.silenl;
    xNadDataSize.bytehigh := mTexnetIOMem.stati.silenh;
    // check buffer size
    if (xNadDataSize.w <= TEXBASE_MAX_LGDLENGTH) then begin
      // copy data to user
      move(mTexnetIOMem.dataIn, aMsgBlock, xNadDataSize.w);
      // set number of bytes read
      aMsglengh := xNadDataSize.w;
    end;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('ReadDataFromNAD failed');
end;

// This function sends starts a read IO request to the NAD
// synchronized with the interrupt object attach to the current device,
// meaning that this function won't be interrupted

function TTXNNadinterface.ReadIoRequest(): Boolean;
begin
  result := true;
  try
    // set input handshake
    mTexnetIOMem.irego := mTexnetIOMem.irego or TEXBASE_NAD_RMIEMP;
    // Host signalisiert Leseanforderung im IN-Kanal an NAD
    mTexnetIOMem.irego := mTexnetIOMem.irego or TEXBASE_NAD_IINTO;
    // Schreiben durch Host erzeugt Interrupt beim NAD
    mTexnetIOMem.rego := TEXBASE_NAD_INTO;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('ReadIoRequest failed');
end;

function TTXNNadinterface.CancelReadIoRequest(): Boolean;
begin
  result := true;
  try
    // clear input handshake
    mTexnetIOMem.irego := mTexnetIOMem.irego and (not TEXBASE_NAD_RMIEMP);
    // Host signalisiert Leseanforderung im IN-Kanal an NAD
    mTexnetIOMem.irego := mTexnetIOMem.irego or TEXBASE_NAD_IINTO;
    // Schreiben durch Host erzeugt Interrupt beim NAD
    mTexnetIOMem.rego := TEXBASE_NAD_INTO;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('CancelReadIoRequest failed');
end;


function TTXNNadinterface.CompleteReadIoRequest(): Boolean;
begin
  result := true;
  try
    mTexnetIOMem.iregi := mTexnetIOMem.iregi and (not TEXBASE_NAD_IINTI);
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('CompleteReadIoRequest failed');
end;



// This function sends an CTRL code to the nad. This function is
// synchronized with the interrupt object attach to the current device,
// meaning that this function won't be interrupted

function TTXNNadinterface.SendCtrlCodeToNAD(afunctionCode: Byte): Boolean;
begin
  result := true;
  try
    // Kopieren der Daten in dataOut
    mTexnetIOMem.dataOut[0] := afunctionCode;
    // Setzen des AusgangsControlblocks
    mTexnetIOMem.stato.sodead := 0;     // ZielAdresse fuer Funktionsaufrufe = 0
    mTexnetIOMem.stato.soprtk := TEXBASE_NAD_LONGDATA;
    mTexnetIOMem.stato.solenl := 1;     // LowByte Anzahl Datenbyte in dataOut
    mTexnetIOMem.stato.solenh := 0;     // HighByte Anzahl Datenbyte in dataOut
    // Alle Daten gueltig, Host signalisiert Anforderung im Out-Kanal an NAD
    mTexnetIOMem.orego := mTexnetIOMem.orego or TEXBASE_NAD_OINTO;
    // Ausloesung eines interrupts beim Nad
    mTexnetIOMem.rego := TEXBASE_NAD_INTO;
  except
    result := false;
  end;
  if result = false then begin
    SetErrorRec(etTXNDrvError,0,'SendCtrlCodeToNAD failed');
    CodeSite.SendError('SendCtrlCodeToNAD failed');
  end;
end;


function TTXNNadinterface.ReadFunctionDataFromNAD(var aOutBuf: array of Byte;
  var aOutBufLength: Integer): Boolean;
var
  xNadDataSize: ByteInt;
begin
  aOutBufLength := 0;
  result := true;
  try
    // bytes, presented by the NAD
    xNadDataSize.bytelow := mTexnetIOMem.stato.solenl;
    xNadDataSize.bytehigh := mTexnetIOMem.stato.solenh;
    // check buffer size
    if (xNadDataSize.w <= TEXBASE_MAX_LGDLENGTH) then begin
      // copy data to user
      move(mTexnetIOMem.dataOut, aOutBuf, xNadDataSize.w);
      // set number of bytes read
      aOutBufLength := xNadDataSize.w;
    end;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('ReadFunctionDataFromNAD failed');
end;


function TTXNNadinterface.CheckDeviceError(aDevError: TDevError): Boolean;
var
  xtext: string;
  xerror: Byte;
begin
  result := false;
  try
    if aDevError = ewrite then
      xerror := mTexnetIOMem.stato.soerr
    else
      xerror := mTexnetIOMem.stati.sierr;
    if xerror <> 0 then begin
      case mTexnetIOMem.stato.soerr of
        TEXBASE_NAD_ERRUPRK: xtext := ' Request could not be done';
        TEXBASE_NAD_ERRBSY:  xtext := ' Block could not be transmited, probable Receiver from ZE is busy';
        TEXBASE_NAD_ERRQUIT: xtext := ' Block could not be transmited';
        TEXBASE_NAD_ERRFKT:  xtext := ' Invalid function call';
        TEXBASE_NAD_ERRNPRK: xtext := ' Protocol failure in network';
      else
        xtext := ' Invalid Texnet error code';
      end;
      if aDevError = ewrite then begin
        SetErrorRec(etTXNDrvError,0,'Write ' + xtext);
        CodeSite.SendError('Write ' + xtext)
      end else begin
        SetErrorRec(etTXNDrvError,0,'Read ' + xtext);
        CodeSite.SendError('Read ' + xtext);
      end;  
    end else
      result := true;
  except
    CodeSite.SendError('CheckDeviceError failed');
    result := false;
  end;
end;



// This function sends a software reset to the nad and waits for its confirmation

function TTXNNadinterface.SoftReset: Boolean;
var
  xcount: Integer;
begin
  xcount := 0;
  try
    // Zuruecksetzen des Nad Interrupts
    mTexnetIOMem.regi := mTexnetIOMem.regi and (not TEXBASE_NAD_INTI);
    // Setzen der Resetflags Bit7
    mTexnetIOMem.reset := mTexnetIOMem.reset or TEXBASE_NAD_RFLAG;
    sleep(100);
    // warten max 1 sec. bis Resetflag durch NAD zurueckgesetzt wird
    while ((mTexnetIOMem.reset and TEXBASE_NAD_RFLAG) = TEXBASE_NAD_RFLAG) and
      (xcount <= 10) do begin
      // wait for 100ms
      sleep(100);
      INC(xcount);
    end;
    result := xcount <= 10;
  except
    result := false;
  end;
  if result = false then CodeSite.SendError('Reset Texnet failed');
end;


// This function disables all interrupts of a PCI device with PLX chip

function TTXNNadinterface.DisableAllPlxIntr(aPlxDeviceHnd: DEVHANDLE): Boolean;
var
  xPlxRes: Integer;
  xPlxIntrfield: PLX_INTR;
begin
  // Fill the interrupt bitfield with 1's
  xPlxIntrfield := $FFFFFFFFFFFFFFFF;   //High(int64);
  try
    xPlxRes := PlxIntrDisable(aPlxDeviceHnd, xPlxIntrfield);
    result := (xPlxRes = ApiSuccess);
    sleep(10);
  except
    Result := FALSE;
  end;
  if result = false then CodeSite.SendError('DisableAllPlxIntr failed');
end;


// This function enable specific interrupts of a PCI device with PLX chip

function TTXNNadinterface.EnablePlxIntr(aPlxDeviceHnd: DEVHANDLE): Boolean;
var
  xPlxRes: Integer;
  xPlxIntrfield: PLX_INTR;
begin
  try
    // Initialize the interrupt bitfield with 0's
    xPlxIntrfield := PLX_INTR_MASK;
    xPlxRes := PlxIntrEnable(aPlxDeviceHnd, xPlxIntrfield);
    result := (xPlxRes = ApiSuccess);
  except
    result := true;
  end;
  if result = false then CodeSite.SendError('EnablePlxIntr failed');
end;


procedure TTXNNadinterface.ClearErrorRec();
begin
  with mErrorRec do begin  // Zur�cksetzen der Fehler Meldung im mErrorRec
    ErrorTyp := etNoError;
    Error := 0;
    Msg := '';
  end;
end;

procedure TTXNNadinterface.SetErrorRec( aErrorTyp: TErrorTyp ;aError: DWord; aMsg: shortstring);
begin
  with mErrorRec do begin  // Setzen der Fehler Meldung im mErrorRec
    ErrorTyp := aErrorTyp;
    Error := aError;
    Msg := aMsg;
  end;

end;

//==============================================================================
//
// TPCITxnDevHandler class
//
//==============================================================================
{privat}

function TPCITxnDevHandler.IRTexNetServiceRoutine(aNumber: Pointer): LongInt; STDCALL;
var
  xNTres: Integer;
begin
  xNTres := 0;                          // init
  try
    // Interruptthread ready-Flag setzen
    interlockedExchange(mIntrThreadready, 1);
    // Interrupt Flag im NAD Shared Memory gesetzt?
    Repeat
      if  mTXNNadinterface.mTexnetIOMem.regi = TEXBASE_NAD_INTI then begin
        // NAD bestatigt einen Leseauftrag beim HOST
        if (mTXNNadinterface.mTexnetIOMem.iregi and TEXBASE_NAD_IINTI) = TEXBASE_NAD_IINTI then begin
          // signal waiting reading operation, that it may go on
          SetEvent(mNADReadIntr);
        end;
        // NAD bestaetigt einen Schreibeauftrag beim Host, OREGI
        if (mTXNNadinterface.mTexnetIOMem.oregi and TEXBASE_NAD_OINTI) = TEXBASE_NAD_OINTI then begin
          // signal waiting writing operation, that it may go on
          SetEvent(mNADwriteIntr);
        end;
        // Zuruecksetzen des interrupts welcher durch NAD ausgeloest wurde
        mTXNNadinterface.mTexnetIOMem.regi := mTXNNadinterface.mTexnetIOMem.regi and (not TEXBASE_NAD_INTI);
      end else begin
         xNTres := WaitForSingleObject(mIntrThreadExit, 10);      // Warten 10 ms
      end;
    Until  xNTres = WAIT_OBJECT_0;    // exit Signal f�r Abbruch
  except
    on E: Exception do CodeSite.SendError(e.message);
  end;
  interlockedExchange(mIntrThreadready, 0);
  // Read und Writr Funktion Abbruch signalisieren
  SetEvent(mNADReadIntr);
  SetEvent(mNADwriteIntr);
  result := 0;
  CodeSite.SendFmtMsg('Exit PLXIntrServiceRoutine, PlxDeviceHnd= %d', [mPlxDeviceHnd]);
end;





{public}

constructor TPCITxnDevHandler.Create;
begin
  inherited Create;
  // Init aller Handles
  mPlxDeviceHnd := INVALID_HANDLE_VALUE;
  mPLXIntrHandler := INVALID_HANDLE_VALUE;
  mNADWriteIntr := INVALID_HANDLE_VALUE;
  mNADReadIntr := INVALID_HANDLE_VALUE;
  mIntrThreadExit:= INVALID_HANDLE_VALUE;;
  mTXNNadinterface := nil;
  mCSRead := nil;
  mCSWrite := nil;
  try
    mCSWrite := TCriticalSection.Create;
    mCSRead := TCriticalSection.Create;
    mTXNNadinterface := TTXNNadinterface.Create;
    mTXNNadinterface.mCSHWAccess := nil;
    mTXNNadinterface.mCSHWAccess := TCriticalSection.Create;
    mIntrThreadExit:= CreateEvent(nil, false, false, nil);
    if  mIntrThreadExit = INVALID_HANDLE_VALUE then
      raise Exception.Create('CreateEvent  mIntrThreadExit,exit failed ');
    mNADWriteIntr := CreateEvent(nil, false, false, nil);
    if mNADWriteIntr = INVALID_HANDLE_VALUE then
      raise Exception.Create('CreateEvent mNADWriteIntr failed ');
    mNADReadIntr := CreateEvent(nil, false, false, nil);
    if mNADReadIntr = INVALID_HANDLE_VALUE then
      raise Exception.Create('CreateEvent  mNADReadIntr failed ');
  except
    on E: Exception do begin
      raise Exception.Create('TxnDevHandler.Create ' + e.message);
    end;
  end;
end;

destructor TPCITxnDevHandler.Destroy;
begin
  try
    TxnDisconnect;
    closeHandle(mIntrThreadExit);
    closeHandle(mNADWriteIntr);
    closeHandle(mNADReadIntr);
    mTXNNadinterface.mCSHWAccess.Free;
    mTXNNadinterface.Free;
    mCSWrite.Free;
    mCSRead.Free;
    // Thread Handle freigeben
    if mPLXIntrHandler <> 0 then CloseHandle(mPLXIntrHandler);
  except
  end;
  inherited Destroy;
end;

function TPCITxnDevHandler.PCIConnect(aPCIDevInfTab: DEVICE_LOCATION): Boolean;
var
  xplxDeviceCount: Integer;
  xPlxRes: Integer;
begin
//  Result := False;  // Wird nicht gebraucht, da Exception oder true (LOK 15.12.04)
  try
    interlockedExchange(mIntrThreadready, 0); // Interrupt Thread  not ready yet
    // Umkopi�ren der DeviceInfo
    mPlxDevice := aPCIDevInfTab;
    xplxDeviceCount := FIND_AMOUNT_MATCHED;
    xPlxRes := PlxPciDeviceFind(mPlxDevice, xplxDeviceCount);
    if (xPlxRes <> ApiSuccess) or (xplxDeviceCount <> 1) then
      raise Exception.Create('PlxPciDeviceFind failed');
    if PlxPciDeviceOpen(mPlxDevice, mPlxDeviceHnd) <> ApiSuccess then
      raise Exception.Create('PlxPciDeviceOpen failed ');
    // Mappen der HArdware auf das TexnetIOMem
    if not mTXNNadinterface.MapHWtoTexnetIOMem(mPlxDeviceHnd) then
      raise Exception.Create('MapHWtoTexnetIOMem failed ');
    //NAD INIT Reset beim Aufstarten des TxnHandlers
    mTXNNadinterface.SoftReset;
    // Alle Interrupts der PCI Karte disable
    if not mTXNNadinterface.DisableAllPlxIntr(mPlxDeviceHnd) then
      raise Exception.Create('DisableAllPlxIntr failed ');
    // InterruptThread (zum primaeren Thread des Prozesses) erzeugen
    mPLXIntrHandler := CreateThread(nil, 0, TFNThreadStartRoutine(@TPCITxnDevHandler.IRTexNetServiceRoutine), Self, 0, mThreadID);
    SetThreadPriority(mPLXIntrHandler, THREAD_PRIORITY_TIME_CRITICAL);
    sleep(100);
    if mIntrThreadready <> 1 then
      raise Exception.Create('CreateIntrThread failed ');
    Result := True;
  except
    on E: Exception do begin
      raise Exception.Create('TxnDevHandler.PCIConnect ' + e.message);
    end;
  end;
end;


function TPCITxnDevHandler.TxnDisconnect: Boolean;
var
  xPlxRes: Integer;
begin
  Result := False;
  try
    // Interrupt Thread wird mittels Exit Event beendet. Dieser signalisiert der Read- und Writefunktion
    // das Ende des InterruptThreads durch Signale und den Zustand von mIntrThreadready.
    SetEvent(mIntrThreadExit);
    // Event Handle mNADWriteIntr, mNADReadIntr werden nicht mehr benoetigt -> freigeben
    sleep(100);                         // Warten bis Thread beendet ist
    try
      xPlxRes := PlxPciDeviceClose(mPlxDeviceHnd);
      CodeSite.SendMsg('The device was Disconnected properly');
      if xPlxRes <> ApiSuccess then CodeSite.SendError('PlxPciDeviceClose failed');
      Result := True;
    except
      CodeSite.SendError('The device was not closed properly');
    end;
  except
  end;
end;

// This function is called when a user thread calls TxnRead and starts a
// reading operation with have a timeout from 60 sec.

function TPCITxnDevHandler.TxnRead(var aMsgBlock: TdatablockRec; var aMsglengh: Integer): Boolean;
var
  xNTres: Integer;
begin
  Result := False;
  mTXNNadinterface.ClearErrorRec;
  // Enter Critical Section
  mCSRead.Enter;
  try
    try
      if mTXNNadinterface.WaitForhardwareAccess(eIN) then begin
        ResetEvent(mNADReadIntr);
        xNTres := WaitForSingleObject(mNADReadIntr, ReadTimeout);
        if mIntrThreadready = 0 then exit; // Abbruch signalisiert von Interruptthread
        case xNTres of
          WAIT_OBJECT_0:  if mTXNNadinterface.CheckDeviceError(eRead) then
                            Result := mTXNNadinterface.ReadDataFromNAD(aMsgBlock, aMsglengh);
          WAIT_TIMEOUT:   mTXNNadinterface.CancelReadIoRequest();

          WAIT_ABANDONED: begin
                           mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Read WAIT_ABANDONED');
                           codesite.SendFmtError('Read WAIT_ABANDONED slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                          end;
        else begin
          mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Unknown Read Error');
          codesite.SendFmtError('Unknown Read Error slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
          end;
        end;
      end;
    except
      on E: Exception do begin
        mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'TxnRead failed ');
        codeSite.SendError('TxnRead failed ' + e.message);
      end;
    end;
    // reset input handshake
    mTXNNadinterface.CompleteReadIoRequest();
  finally
    fErrorRec := mTXNNadinterface.mErrorRec;
    // Leave Critical Section
    mCSRead.Leave;
    sleep(1);
  end;                          // Threadwechsel erzwingen
end;


// This function is called when a user thread calls TxnWrite and starts a
// writing operation

function TPCITxnDevHandler.TxnWrite(aMsgBlock: TdatablockRec; aDatablockLengh: Integer): Boolean;
var
  xNTres, i: Integer;
begin
  Result := False;
  mTXNNadinterface.ClearErrorRec;
   // Enter Critical Section
  mCSWrite.Enter;
  try
    for i := 1 to 2 do begin
      try
        if mTXNNadinterface.WaitForHardwareAccess(eOUT) then begin
          ResetEvent(mNADWriteIntr);
          if mTXNNadinterface.WriteDataToNAD(aMsgBlock, aDatablockLengh) then begin
            xNTres := WaitForSingleObject(mNADWriteIntr, cPCIWritetimeout);
            if mIntrThreadready = 0 then exit; // Abbruch signalisiert von Interruptthread
            case xNTres of
              WAIT_OBJECT_0:  Result := mTXNNadinterface.CheckDeviceError(eWrite);
              WAIT_TIMEOUT:   begin
                                mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Write WAIT_TIMEOUT');
                                codesite.SendFmtWarning('Write WAIT_TIMEOUT slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                              end;
              WAIT_ABANDONED: begin
                                mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Write WAIT_ABANDONED');
                                codesite.SendFmtError('Write WAIT_ABANDONED slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                              end;
            else begin
              mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Unknown Write Error');
              codesite.SendFmtError('Unknown Write Error slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
              end;
            end;
          end;
        end;
      except
        on E: Exception do begin
          mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'TxnWrite failed');
          CodeSite.SendError(e.message + 'TxnWrite failed');
        end;
      end;
      // reset output handshake
      mTXNNadinterface.CompleteWriteIoRequest();
      if Result = True then Break;
      sleep(50);                          // Verzoegerung fuer 2 Versuch
    end;
  finally
    fErrorRec := mTXNNadinterface.mErrorRec;
    // Leave Critical Section
    mCSWrite.Leave;
    sleep(1);
  end;                            // Threadwechsel erzwingen
end;


function TPCITxnDevHandler.TxnDevControl(aCtrlCode: DWord; out aOutBuf: array of Byte;
  out aOutBufLength: Integer): Boolean;
  //............................................................
  function NADfunctionCall(aCtrlCode: DWord; var aOutBuf: array of Byte; var aOutBufLength: Integer): Boolean;
  var
    xNTres, i: Integer;
  begin
    result := false;
    mTXNNadinterface.ClearErrorRec;
    // Enter Critical Section
    mCSWrite.Enter;
    try
      for i := 1 to 2 do begin
        try
          if mTXNNadinterface.WaitForHardwareAccess(eOUT) then begin
            ResetEvent(mNADWriteIntr);
            if mTXNNadinterface.SendCtrlCodeToNAD(aCtrlCode) then begin
              xNTres := WaitForSingleObject(mNADWriteIntr, cPCIControltimeout);
              if mIntrThreadready = 0 then exit; // Abbruch signalisiert von Interruptthread
              case xNTres of
                WAIT_OBJECT_0: if mTXNNadinterface.CheckDeviceError(eWrite) then begin
                                 Result := mTXNNadinterface.ReadFunctionDataFromNAD(aOutBuf, aOutBufLength);
                               end;
                WAIT_TIMEOUT:  begin
                                 mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Control WAIT_TIMEOUT');
                                 codesite.SendFmtWarning('Control WAIT_TIMEOUT slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                               end;
                WAIT_ABANDONED:begin
                                 mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Control WAIT_ABANDONED');
                                 codesite.SendFmtError('Control WAIT_ABANDONED slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                               end;
              else begin
                mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'Unknown Control Error');
                codesite.SendFmtError('Unknown Control Error slotNo=%d DevHnd= %d', [mPlxDevice.SlotNumber, mPlxDeviceHnd]);
                end;
              end;
            end;
          end;
          fErrorRec := mTXNNadinterface.mErrorRec;
        except
          on E: Exception do begin
            mTXNNadinterface.SetErrorRec(etTXNDrvError,0,'NADfunctionCall failed');
            CodeSite.SendError(e.message + 'NADfunctionCall failed');
          end;
        end;
        // reset output handshake
      mTXNNadinterface.CompleteWriteIoRequest();
      if Result = true then Break;
        sleep(50);
      end;
    finally
      // Leave Critical Section
      mCSWrite.Leave;
      fErrorRec := mTXNNadinterface.mErrorRec;
      sleep(1);
    end;                          // Threadwechsel erzwingen
  end;
  //............................................................
begin
  result := False;
  try
    case aCtrlCode of
      IOCTL_TEXNET_ROMCHECKSUM: begin
          result := NADfunctionCall(IOCTL_TEXNET_ROMCHECKSUM, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_VERSION: begin
          result := NADfunctionCall(TEXBASE_NAD_FUNCVERSION, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_ADDRESS: begin
          result := NADfunctionCall(TEXBASE_NAD_FUNCADDRESS, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_ERRSTATE: begin
          result := NADfunctionCall(TEXBASE_NAD_FUNCERRSTATE, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_NNSTABLE: begin
          result := NADfunctionCall(TEXBASE_NAD_FUNCNNSTABLE, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_MEMACCESS: begin
          result := NADfunctionCall(TEXBASE_NAD_FUNCMEMACCESS, aOutBuf, aOutBufLength); end;
      IOCTL_TEXNET_RESET: begin
          // Neu Initialiserung des NAD's
          result := mTXNNadinterface.SoftReset;
          // eventuell init von PLX
          mTXNNadinterface.CompleteWriteIoRequest;
        end;
      IOCTL_TEXNET_LASTERROR: begin
        end;
      IOCTL_TEXNET_CANCELREAD: begin
        end;
    else
      result := false;
    end;
  except
    CodeSite.SendError('TxnDevControl failed');
    aOutBufLength := 0;
    result := false;
  end;
end;

function TPCITxnDevHandler.GetLastTxnError(): Integer;
begin
  result := 0;
end;


// Function: Amount_PCITexnetNAD_MATCHED Sucht Anzahl der PCI-Texnetkarte mit entsprechender Slotnummmer und BusNumber
// IN: aPlxDevice.SlotNumber, aPlxDevice.BusNumber
// OUT : Anzahl der gefundenen PlxDevices

function Amount_PCITexnetNAD_MATCHED(var aPlxDevice: DEVICE_LOCATION): Integer;
var
  xPlxRes: Integer;
begin
  xPlxRes:= FIND_AMOUNT_MATCHED;
  with aPlxDevice do begin
    DeviceId := DEVICE_ID;
    VendorId := VENDOR_ID;
    SerialNumber[0] := $0;              //   means wildcard
  end;
  try
    if PlxPciDeviceFind(aPlxDevice, xPlxRes) = ApiSuccess then
      result:= xPlxRes
    else
      result:= 0;
  except
    CodeSite.SendError('FindPCITexnetNAD failed, probable missing PLXAPI.DLL');
    result:= -1;
  end;
end;


// Function: GetPCITexnetDeviceInfo Sucht PCI-Texnetkarte  mit entsprechender Slotnummmer und BusNumber
// IN: aPlxDevice.SlotNumber, aPlxDevice.BusNumber
// OUT : Device Parameter in aPlxDevice
function GetPCITexnetDeviceInfo(var aPlxDevice: DEVICE_LOCATION): Boolean;
var
  xplxDeviceCount: Integer;
  xPlxRes: Integer;
begin
  result := false;
  xplxDeviceCount := 0;
  with aPlxDevice do begin
    DeviceId := DEVICE_ID;
    VendorId := VENDOR_ID;
    SerialNumber[0] := $0;              //   means wildcard
  end;
  try
    xPlxRes := PlxPciDeviceFind(aPlxDevice, xplxDeviceCount);
    if (xPlxRes = ApiSuccess) then  result := true;
  except
    CodeSite.SendError('FindPCITexnetNAD failed, probable missing PLXAPI.DLL');
  end;
end;

function TPCITxnDevHandler.ISAConnect(aName: string; aOpenMode: eOpenMode): Boolean;
begin
  // bleibt leer, nur wegen Compiler Warnings implementiert
  Result := False;
end;

end.

