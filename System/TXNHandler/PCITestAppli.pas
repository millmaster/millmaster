(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: PCITestAppli.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: Definition Unit PCITexnet NAD
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic Pro M7, Pentium II 450, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.0
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.08.2001  0.00  khp | Datei erstellt
| 29.08.2002  1.00  khp | PCIslot's von 0 bis 32 k�nnen erkannt werden
| 27.02.2004  1.10  khp | Unterst�tzung von mehreren PCI-BusControllern pro PC
| 12.04.2005  2.00  khp | Zus�tzliche Diagnosen: Texnetdriver installed and running,
|                       | TXNHANDLER.EXE isn't running, sep. Fehleranzeigefenster
|==========================================================================================*)
unit PCITestAppli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, TXN_PCI_Def, TXN_PCI_DevClass,TXN_glbDef,TXN_ISA_DevClass,
  BaseGlobal,LoepfeGlobal,ExtCtrls,mmRegistry, ComCtrls, Typinfo, mmLabel,Netware,
  ActnList, mmActionList, mmListBox, mmMemo;

type
  TTesterState= (tsNone,tsInit,tsSelAdaptertype,tsSelAdapter,tsSelNode,tsSelData);
  tDev_loc_Table= array [1.. cMaxTxnAdapter] of DEVICE_LOCATION;

  TReadThread = class(TThread)
  private
    { Private declarations }
    TxnReadHandler: TTxnBaseDevHandler;
    mText:String;
  protected
    procedure WriteMemo2Insert;
    procedure Execute; override;
  public
    constructor Create(aAdapter:Integer;TxnMainHandler: TTxnBaseDevHandler);
    destructor Destroy; override;
  end;

  TControllerThread = class(TThread)
  private
    { Private declarations }
    TxnContrHandler: TTxnBaseDevHandler;
    mText:String;
    mValue: Integer;
  protected
    procedure WriteListBox1Add;
    procedure WriteMemo2Insert;
    procedure writeMemo2Text;
    procedure SetProgressbar1position;
    procedure Execute; override;
  public
    constructor Create(aAdapter:Integer;TxnMainHandler: TTxnBaseDevHandler);
    destructor Destroy; override;
  end;

  TForm1 = class(TForm)
    Button3: TButton;
    Button5: TButton;
    Button6: TButton;
    ListBox1: TListBox;
    Button8: TButton;
    Button1: TButton;
    Button4: TButton;
    Button9: TButton;
    ListBox3: TListBox;
    Label3: TLabel;
    StaticText2: TStaticText;
    ProgressBar1: TProgressBar;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Button2: TButton;
    Label4: TLabel;
    Timer: TLabel;
    StaticText1: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    mmActionList1: TmmActionList;
    acReset: TAction;
    acVersion: TAction;
    acErrorcount: TAction;
    acPCI: TAction;
    acISA: TAction;
    acClear: TAction;
    acGetMaConfig: TAction;
    acGetLastDecl: TAction;
    acBasedata: TAction;
    acMonitordata: TAction;
    mmLabel1: TmmLabel;
    Memo1: TmmMemo;
    Memo4: TmmMemo;
    Memo2: TmmMemo;
    procedure FormDestroy(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ListBox3Click(Sender: TObject);
    procedure ListBox1Click(Sender: TObject);
    procedure acResetExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
    procedure acVersionExecute(Sender: TObject);
    procedure acErrorcountExecute(Sender: TObject);
    procedure acPCIExecute(Sender: TObject);
    procedure acISAExecute(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure acGetMaConfigExecute(Sender: TObject);
    procedure acGetLastDeclExecute(Sender: TObject);
    procedure acBasedataExecute(Sender: TObject);
    procedure acMonitordataExecute(Sender: TObject);
  private
    mTesterState: TTesterState;
  public
  end;

var
  Form1: TForm1;
  TxnDevHandler: TTxnBaseDevHandler;
  Dev_loc_Table:  tDev_loc_Table;
  ReadThread: TReadThread;
  ControllerThread: TControllerThread;
  gMsg: TdatablockRec;
  glength:Integer;
  isConnected:Boolean;
  actualAdapter:Integer;
  DevMode: eDevModeT;
  gMsgIOControl: array [0..256] of Byte;
  gNode:Integer;
  gRecCount:LongInt;
  gRecRequest:Longint;
  gDateTime : TDateTime;
  gTabs: Array [0..1] of DWORD;

implementation

uses
  JclSysInfo;
{$R *.DFM}

//------------------------------------------------------------------------------------------------------
constructor TReadThread.Create(aAdapter:Integer;TxnMainHandler: TTxnBaseDevHandler);
begin
  try
    if DevMode= eISA then begin
      TxnReadHandler:= TTxnDevHandler.Create;
      TxnReadHandler.ISAConnect(cTexnetDeviceName[aAdapter],omRead);
    end else begin  // PCI
      TxnReadHandler:= TxnMainHandler;
    end;
    inherited Create(false);
  except
    // fehlermeldung und abbruch
  end;
end;


destructor TReadThread.Destroy;
begin
  if DevMode = eISA then begin
    TxnReadHandler.TxnDisconnect;
    TxnReadHandler.free;
    end;
 inherited Destroy;
end;


procedure TReadThread.WriteMemo2Insert;
begin
  Form1.Memo2.lines.Insert(0, mText);
end;



procedure TReadThread.Execute;
var
  xtext:String;
  xtelegram: Ttelegram;
  xMsg: TdatablockRec;
  xlength, xGroup: Integer;
begin
  try
    mText:='ReadThread is running';
    Synchronize(WriteMemo2Insert);
    FreeOnTerminate:= false;
    While   NOT Terminated do begin   // Loop Forever
      fillChar(xMsg, sizeof(xMsg),0);
      if TxnReadHandler.TxnRead(xMsg,xlength) then begin
        move(xMsg.datenArray,xtelegram.all,sizeof(TdatenArray));
        case xTelegram.Header.telegramID of
          eCONFIRM_TELEGRAM:begin
                              case xTelegram.Header.telegramSubID of
                                OS2_eDUMMY_DATA..OS2_eMACONFIG_DATA: begin xtext:='CONFIRM_TELEGRAM OS2'; end;
                                eBASE_DATA:    begin xtext:='CONFIRM_TELEGRAM BASE_DATA Spindle: '+InttoStr(xTelegram.Header.SpindleRange.firstSpindle);end;
                                eDEFECT_DATA:  begin xtext:='CONFIRM_TELEGRAM DEFECT_DATA Spindle: '+InttoStr(xTelegram.Header.SpindleRange.firstSpindle);end;
                                eSIRO_DATA:    begin xtext:='CONFIRM_TELEGRAM SIRO_DATA Spindle: '+InttoStr(xTelegram.Header.SpindleRange.firstSpindle);end;
                                eSETTINGS:     begin xtext:='CONFIRM_TELEGRAM SETTINGS Spindle: '+InttoStr(xTelegram.Header.SpindleRange.firstSpindle);end;
                                eDECLARATIONS: begin xtext:='CONFIRM_TELEGRAM DECLARATIONS,';
                                                xtext:=xtext+ ' '+ GetEnumName(TypeInfo(eDecEventT),ord((pDeclaration(@xTelegram.Daten)^.event)));
                                                for xgroup:= 1 to 12 do begin
                                                  xtext:= xtext+' ' + inttostr(word(pDeclaration(@xTelegram.Daten)^.validity[xgroup].modify));
                                                 end;
                                               end;
                                eMACONFIG_DATA:begin xtext:='CONFIRM_TELEGRAM MACONFIG_DATA';
                                               end;
                              else
                                 xtext:='Unknown CONFIRM_TELEGRAM';
                              end;
                            end;

          eMESSAGE_TELEGRAM:begin
                              case xTelegram.Header.telegramSubID of
                                OS2_eDUMMY_DATA..OS2_eMACONFIG_DATA: begin xtext:='MSG_TELEGRAM OS2'; end;
                                eDUMMY_DATA: begin xtext:='Empty MSG_TELEGRAM'; end;
                                eDECLARATIONS:begin
                                                case pDeclaration(@xTelegram.Daten)^.event of
                                                  eEVT_INITIAL_RESET: begin xtext:='MSG_TELEGRAM DECL INITIAL_RESET';end;
                                                  eEVT_RESET: begin xtext:='MSG_TELEGRAM DECL RESET';end;
                                                  eEVT_ENTRY_LOCKED:begin xtext:='MSG_TELEGRAM DECL ENTRY_LOCKED,';
                                                             for xgroup:= 1 to 12 do begin
                                                              xtext:= xtext+' ' + inttostr(word(pDeclaration(@xTelegram.Daten)^.validity[xgroup].modify));
                                                             end;
                                                                    end;
                                                  eEVT_ENTRY_UNLOCKED: begin xtext:='MSG_TELEGRAM DECL ENTRY_UNLOCKED';end;
                                                  eEVT_MODIFIED_RANGE: begin xtext:='MSG_TELEGRAM DECL MODIFIED_RANGE';end;
                                                  eEVT_MODIFIED_SETTINGS: begin xtext:='MSG_TELEGRAM DECL SETTINGS,';
                                                            for xgroup:= 1 to 12 do begin
                                                            xtext:= xtext+' ' + inttostr(word(pDeclaration(@xTelegram.Daten)^.validity[xgroup].modify));
                                                           end;
                                                  end;
                                                  eEVT_ADJUST : begin xtext:='MSG_TELEGRAM DECL ADJUST';end;
                                                else
                                                  xtext:='Unknown MSG_TELEGRAM DECL';
                                                end;
                                              end;
                                eGRP_SCREENS, eSPDL_SCREENS: begin xtext:='Not supported MSG_TELEGRAM'; end;
                              else
                                xtext:='Unknown MSG_TELEGRAM';
                              end;
                            end;
           eACKNOWLEDGE: begin
                           xtext:='ACKNOWLEDGE';
                         end;
        else // case
           xtext:='Unknown TELEGRAM';
        end;
        if xMsg.headerRec.blockNumber = 1 then begin
         INC(gRecCount);
         Form1.label4.Caption:= inttostr(gRecCount);
         Form1.Timer.Caption:= TimeToStr((Time)-gDateTime);
         mText:='>  Node= '+InttoStr(xMsg.headerRec.sendNode)+
               ', TotBlk= '+InttoStr(xMsg.headerRec.TotalBlocks)+', '+xtext;
         Synchronize(WriteMemo2Insert);
        end;
      end else
    end; // while
    mText:='ReadThread is stopped';
    Synchronize(WriteMemo2Insert);
  except
    on E: Exception do begin
      isConnected := false;
      mText:=e.message;
      Synchronize(WriteMemo2Insert);
    end;
  end;
end;

//------------------------------------------------------------------------------------------------------

constructor  TControllerThread.Create (aAdapter:Integer;TxnMainHandler: TTxnBaseDevHandler);
begin
  try
    if DevMode= eISA then begin
      TxnContrHandler:= TTxnDevHandler.Create;
      TxnContrHandler.ISAConnect(cTexnetDeviceName[aAdapter],omWrite);
    end else begin  // PCI
      TxnContrHandler:= TxnMainHandler;
    end;
    inherited Create(false);
  except
    // fehlermeldung und abbruch
  end;
end;

destructor TControllerThread.Destroy;
begin
  if DevMode = eISA then  begin
    TxnContrHandler.TxnDisconnect;
    TxnContrHandler.Free;
  end;
  inherited Destroy;
end;

procedure TControllerThread.WriteListBox1Add;
begin
  Form1.ListBox1.Items.Add(mText);
end;

procedure TControllerThread.WriteMemo2Insert;
begin
  Form1.Memo2.Lines.Insert(0,mText);
end;

procedure TControllerThread.writeMemo2Text;
begin
  Form1.Memo2.Lines.text:= mText;
end;

procedure TControllerThread.SetProgressbar1position;
begin
  Form1.Progressbar1.position:= mValue;
end;

procedure TControllerThread.execute;
var
  xMsgIOControl: array [0..256] of Byte;
  x: Integer;
  xtext: String;
begin
  try
    mText:= 'ControlThread is running';
    Synchronize(WriteMemo2Insert);
    FreeOnTerminate:= false;
    While  NOT Terminated do begin   // Loop Forever bis exit Signal
      Form1.Listbox1.Clear;
      if TxnContrHandler.TxnDevControl(IOCTL_TEXNET_NNSTABLE,xMsgIOControl,glength)then begin
        if (glength <= 239) OR (glength >= 128) then begin
          for x:=128 to 239 do begin
            sleep(5);
            mValue:= x div 3;
            Synchronize(SetProgressbar1position);
            case  xMsgIOControl[x] of
                 $0: xtext:=  'Node '+inttostr(x)+ ' OFF';
                 $1..$80:   Form1.ListBox1.Items.Add(inttostr(x));
            else
                   xtext:=  'Node '+inttostr(x)+ ' not valid Node status';
            end;
          end;
        end else begin
           mText:= ('wrong length ofNodelist');
           Synchronize(WriteMemo2Text);
        end;
      end else begin
          mText:= 'Nodelist not accessible';
          Synchronize(WriteMemo2Text);
      end;
      sleep(500);
    end;
    mText:= 'ControlThread is stopped';
    Synchronize(WriteMemo2Insert);
  except
    on E: Exception do begin
      isConnected := false;
      mText:=e.message;
      Synchronize(WriteMemo2Text);
    end;
  end;
end;

//------------------------------------------------------------------------------------------------------

procedure SetTelegramHeader( aTelegramID: eTelegramIdT; aTelegramSubID: eTelegramSubIdT;
                                        aSpindleLast: Byte; aSpindlefirst: Byte; aFlagRegister: BITSET8;
                                        aJobID: DWord; alength: Word;var atelegram: Ttelegram);
  begin
    with atelegram.Header do begin
      telegramID:= aTelegramID;
      telegramSubID:= aTelegramSubID;
      machineNumber:= cMachDummy;
      spindleRange.lastSpindle:= aSpindlefirst;   // Swap f�r ZE da SpindleRange 16Bit
      spindleRange.firstSpindle:= aSpindleLast;   // Swap ""
      flagRegister:= aFlagRegister;
      JobID:= SwapDWord(aJobID);    // SwapDWord JobID MM/NT
      length:=swap(alength); // Laenge des Telegrams mit Kopf
    end;
  end;



procedure TForm1.FormCreate(Sender: TObject);
var
  xPList: TStringList;
begin
  // Tabstop f�r mmMemo1
  gTabs[0] := 80;
  gTabs[1] := 95;
  SendMessage(Memo1.Handle, EM_SETTABSTOPS, 2, Integer(@gTabs));
  mTesterState:= tsNone;
  isConnected:= False;
  TxnDevHandler:= NIL;
  ListBox3.Items.Clear;
  actualAdapter:= 0;
  gRecCount:=0;
  gRecRequest:=0;
  Form1.Label4.Caption:='0';
  Form1.Label5.Caption:='0';
  ReadThread:= NIL;
  ControllerThread:= NIL;
  if IsTexNetDriverInstalled then
    if IsTexNetDriverRunning then begin
      xPList := TStringList.Create;
      try
        RunningProcessesList(xPList, false);
        if xPList.IndexOf('TXNHANDLER.EXE') < 0 then begin
         mTesterState:= tsInit;
         Memo4.Lines.Text:= ' -> Choose Adaptertype ISA or PCI'
        end else
         Memo4.Lines.Text:= ' ! Millmaster is runnig   -> Stop MillMaster';
      finally
        xPList.free;
      end;
    end else
       Memo4.Lines.Text:= ' ! TexNetDriver is not runnig'
  else
     Memo4.Lines.Text:= ' ! TexNet Driver is not installed';
end;


procedure TForm1.FormDestroy(Sender: TObject);
begin
  if isConnected= true then begin
    Form1.Listbox1.Clear;
    Form1.Memo4.clear;
    Form1.Label4.Caption:='';
    Form1.Label5.Caption:='';
    TxnDevHandler.TxnDisconnect;
    sleep(200);
    isConnected:=false;
  end;
end;



// Connect des zu �berwachenden Adapters

procedure TForm1.ListBox3Click(Sender: TObject);

begin
  try
    if isConnected = true then begin
      // Read und Control Thread mittels Event abschiessen
      Form1.Listbox1.Clear;
      Form1.Memo4.clear;
      Form1.Label4.Caption:='0';
      Form1.Label5.Caption:='0';
      ReadThread.Terminate;
      ReadThread.Waitfor;// Warten bis ReadThread finish
      ReadThread.Free;
      ControllerThread.Terminate;
      ControllerThread.WaitFor;
      ControllerThread.Free;
      beep;
      Form1.Progressbar1.visible:=false;
      isConnected:=false;
      Memo4.Lines.Text:= ' -> Choose Adapter number  in adapter-list';
      mTesterState:= tsSelAdapter;
      IF TxnDevHandler <> NIL then begin
        TxnDevHandler.Free;
        TxnDevHandler:=NIL;
      end;
    end;
    if actualAdapter <> ListBox3.ItemIndex+1 then begin
        if (ListBox3.ItemIndex < 3) and  (ListBox3.ItemIndex >= 0) then begin
          Form1.Memo4.clear;
          Form1.Progressbar1.visible:=True;
          gRecCount:=0;
          gRecRequest:=0;
          actualAdapter:= ListBox3.ItemIndex+1;
          if DevMode= ePCI then begin
            TxnDevHandler:= TPCITxnDevHandler.Create();
            isConnected:= TxnDevHandler.PCIConnect(Dev_loc_Table[actualAdapter]);
          end else begin  // ISA
            TxnDevHandler:= TTxnDevHandler.Create;
            isConnected:= TxnDevHandler.ISAConnect(cTexnetDeviceName[actualAdapter],omWrite);
          end;
          sleep(300);
          if isConnected= true then begin
            // ReadThread erzeugen
            ReadThread:=TReadThread.Create(actualAdapter,TxnDevHandler);
            // ControlThread erzeugen
            ControllerThread:= TControllerThread.Create(actualAdapter,TxnDevHandler);
            Memo4.Lines.Text:= ' -> Choose node from node-list';
            mTesterState:= tsSelNode;
          end else begin
            actualAdapter:=0;
            Memo4.Lines.Text:= ' ! Connect failed.  -> Choose new adaptertype ISA or PCI';
          end;
        end else
          Form1.Memo4.Lines.Text:=('! Adapter number not valid');
    end else
      actualAdapter:=0;
  except
     on E: Exception do begin
       isConnected:= false;
       Form1.Memo4.Lines.text:=e.message;
     end;
  end;
end;


// Auswahl eines Nodes aus Liste welcher ON-LINE
procedure TForm1.ListBox1Click(Sender: TObject);
var
 //xtext:String;
 xindex: integer;
begin
  xindex:=Listbox1.Itemindex;
  if xindex <> -1 then begin
    gNode:= strtoInt(ListBox1.Items[xindex]);
    if (gNode > 127) AND (gNode < 200) then begin
      mTesterState:= tsSelData;
      Memo2.Lines.Clear;
      Memo4.Lines.Text:= ' Node '+ inttostr(gNode) +' is choosen'
    end else begin
      mTesterState:= tsSelNode;
      Memo4.Lines.Text:= ' ! Node not valid!';
    end;
  end;
end;


// Reset Adapter
procedure TForm1.acResetExecute(Sender: TObject);
begin
  fillChar(gMsgIOControl, sizeof(gMsgIOControl),0);
  Memo1.Clear;
  Memo2.Clear;
  gRecCount:= 0;
  gRecRequest:= 0;
  Form1.label4.Caption:= inttostr(gRecCount);
  Form1.label5.Caption:= inttostr( gRecRequest);
  Form1.Timer.Caption:= '';
  if TxnDevHandler.TxnDevControl(IOCTL_TEXNET_RESET,gMsgIOControl,glength) then begin
    mTesterState:= tsSelNode;
    Memo4.Lines.Text:= ' -> Choose node from node-list';
    Memo2.Lines.text:='Reset Texnet done'
  end else begin
    mTesterState:= tsSelAdapter;
    Memo2.Lines.Insert(0,'! Reset Texnet failed')
  end;
end;

// Abfrage Version der Netzwerkarte
procedure TForm1.acVersionExecute(Sender: TObject);

begin
  fillChar(gMsgIOControl, sizeof(gMsgIOControl),0);
  if TxnDevHandler.TxnDevControl(IOCTL_TEXNET_VERSION, gMsgIOControl,glength) then
    Memo2.Lines.Insert(0,Copy(PChar(@gMsgIOControl),0,glength+1))
   else begin
    mTesterState:= tsSelAdapter;
    Memo2.Lines.Insert(0,'Version not accessible')
  end;
end;

// Abfrage der Fehlerz�hler der Hostkarte
procedure TForm1.acErrorcountExecute(Sender: TObject);
begin
  fillChar(gMsgIOControl, sizeof(gMsgIOControl),0);
  if TxnDevHandler.TxnDevControl( IOCTL_TEXNET_ERRSTATE, gMsgIOControl,glength) then begin
     Memo1.Lines.Insert(0, datetimetostr(Now)+':'+#9+inttostr(gMsgIOControl[0])+#9+inttostr(gMsgIOControl[1]));
  end else begin
    mTesterState:= tsSelAdapter;
    Memo2.Lines.Text:= ' ! HostNode-ID not accessible'
  end;
end;


// Auswahl PCI Adapter
procedure TForm1.acPCIExecute(Sender: TObject);
  // lokale Funktion
  function Initialize(var alastPCISlot:Integer):Boolean;
    var
      xBinValue: array [0..512] of byte;
    begin
      Result := False;
      alastPCISlot := 0;
      // letzter Texnet PCI-Slot aus Registry lesen, nur bei NT !
      with TmmRegistry.Create do try
        RootKey := HKEY_LOCAL_MACHINE;
        if OpenkeyReadonly(cPCITexnetRegPath) then begin
          if ValueExists(cPCITexnetValue) then begin
            readbinarydata(cPCITexnetValue, xBinValue, sizeof(xBinValue));
            alastPCISlot := xBinValue[12]
          end else  // Bei Windows 2000 ist kein Eintrag vorhanden
            alastPCISlot := clastPCISlot;
          CloseKey;
          result:= true;
        end;
      finally
        Free;
      end;
    end;

 var
   xlastPCISlot:Integer;
   xPlxDevice:DEVICE_LOCATION;
   xpcislot,xAdapterNo:Integer;
   xbusNumber: integer;
   xTotalAvailablePCITexnetNAD: Integer;
   xTotalfoundPCITexnetNAD: Integer;
   xPerBusAvailablePCITexnetNAD: Integer;
   xPerBusfoundPCITexnetNAD: Integer;
begin
  DevMode:= ePCI;
  ListBox3.Clear;
  // Es werden mehrere SlotNo pro PCI-Bus unterst�tzt, somit ist SlotNo nicht unique!
  if Initialize(xlastPCISlot) then begin
    fillchar(Dev_loc_Table,sizeof(Dev_loc_Table),0);
    xPlxDevice.SlotNumber:= MINUS_ONE_LONG;  // alle
    xPlxDevice.BusNumber:= MINUS_ONE_LONG;   // alle
    xTotalAvailablePCITexnetNAD:= Amount_PCITexnetNAD_MATCHED(xPlxDevice);  // wieviele PCITExnetadapter pro PC
    if xTotalAvailablePCITexnetNAD > 0 then begin
      xAdapterNo:= 1;  // Dev_loc_Table Array beginnt bei 1
      xbusNumber:= 0;
      xTotalfoundPCITexnetNAD:= 0;
      while xTotalfoundPCITexnetNAD < xTotalAvailablePCITexnetNAD do begin
        xPlxDevice.SlotNumber:= MINUS_ONE_LONG; // alle
        xPlxDevice.BusNumber:= xbusNumber;
        xPerBusAvailablePCITexnetNAD:= Amount_PCITexnetNAD_MATCHED(xPlxDevice); // wieviele PCITExnetadapterpro PCI-BUS
        if xPerBusAvailablePCITexnetNAD = -1 then break; // Abruch bei Exception
        xPerBusfoundPCITexnetNAD:= 0;
        xpcislot:= 0;
        While (xPerBusfoundPCITexnetNAD < xPerBusAvailablePCITexnetNAD) and
              (xPerBusfoundPCITexnetNAD < xlastPCISlot) do begin
          xPlxDevice.SlotNumber:= xpcislot;
          xPlxDevice.BusNumber:= xbusNumber;
          if GetPCITexnetDeviceInfo(xPlxDevice) then begin
            Dev_loc_Table[xAdapterNo]:= xPlxDevice;   // umkopieren in Liste
            INC(xPerBusfoundPCITexnetNAD);
            INC(xTotalfoundPCITexnetNAD);
            INC(xAdapterNo);
            ListBox3.Items.Add('AdapterNo: '+ inttoStr(xAdapterNo-1)+'  PCI-SlotNo: '+ inttoStr(xpcislot)+
            '  PCI-BusNo: '+ inttoStr(xbusNumber));
          end;
          INC(xpcislot);
        end;
        INC(xbusNumber);
      end;
      mTesterState:= tsSelAdapter;
      Memo4.Lines.Text:= ' '+ inttostr(xTotalfoundPCITexnetNAD)+' PCI adapter found.'+
                                                '  -> Choose adapter in adapter-list'
    end else Memo4.Lines.Text:= ' ! No PCI adapter on the system found';
  end;
end;


// Auswahl ISA Adapter
procedure TForm1.acISAExecute(Sender: TObject);
var
    xAdapterNo,xCount: SmallInt;
  begin
    DevMode:= eISA;
    ListBox3.Clear;
    xCount := 0;
    // Anzahl der ISA Treiber aus Registry lesen
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      Access := KEY_READ;
      for  xAdapterNo:= 1  to cMaxTxnAdapter do begin
        if (KeyExists(cTexnetRegPath+cTexnetRegName[xAdapterNo])) then begin
          ListBox3.Items.Add('AdapterNo: '+ inttoStr(xAdapterNo));
          INC(xCount);
        end
        else begin
          Break;
        end;
      end;
      if xCount >= 1 then begin
        mTesterState:= tsSelAdapter;
        Memo4.Lines.Text:= inttostr(xcount)+' ISA Adapter in Registry found.'+
                                                ' -> Choose adapter in adapter-list'
      end else
        Memo4.Lines.Text:= ' ! No ISA adapter adapter on the system found';
    finally
      Free;
    end;
  end;

// Reset des Request Counters
procedure TForm1.acClearExecute(Sender: TObject);
begin
  gRecCount:= 0;
  gRecRequest:= 0;
  Form1.label4.Caption:= inttostr(gRecCount);
  Form1.label5.Caption:= inttostr( gRecRequest);
  Form1.Timer.Caption:= '';
  Memo2.Clear;
  gDateTime :=Time;
end;


// Get MaConfig all Groups
procedure TForm1.acGetMaConfigExecute(Sender: TObject);
var
  xtelegram: Ttelegram;
  x:Integer;
begin
  fillChar(gMsg, sizeof(gMsg),0);
  fillChar(xtelegram, sizeof(xtelegram), 0);
  gMsg.headerRec.sendNode:= 1;
  gMsg.headerRec.receiveNode:= gNode;
  gMsg.headerRec.blockNumber:= 1;
  gMsg.headerRec.TotalBlocks:= 1;
  for x:=1 to 1 do begin
    SetTelegramHeader(eREQUEST_TELEGRAM, eMACONFIG_DATA, 0
                            ,0, [cView], 1, sizeof(TtelegramHeader), xtelegram);
    move(xtelegram.Header, gMsg.datenArray,sizeof(TtelegramHeader));
    if TxnDevHandler.TxnWrite(gMsg,sizeof(TtelegramHeader)+ sizeof(TheaderRec))then begin
      Memo2.Lines.Insert(0,'Request GetMaConfig done');
      INC(gRecRequest);
      Form1.Timer.Caption:= '';
      gDateTime:= Time;
      Form1.Label5.Caption:= inttostr(gRecRequest);
    end else Memo2.Lines.Insert(0,'Request GetMaConfig failed');
  end;
end;

// Get LastDeclaraion
procedure TForm1.acGetLastDeclExecute(Sender: TObject);
var
  xtelegram: Ttelegram;
  xgroup:Integer;
begin
  fillChar(gMsg, sizeof(gMsg),0);
  fillChar(xtelegram, sizeof(xtelegram), 0);
  gMsg.headerRec.sendNode:= 1;
  gMsg.headerRec.receiveNode:= gNode;
  gMsg.headerRec.blockNumber:=1;
  gMsg.headerRec.TotalBlocks:=1;
  for xgroup:=1 to 1do begin
    SetTelegramHeader(eREQUEST_TELEGRAM, eDECLARATIONS, xgroup*10
                            ,(xgroup*10)-9, [cView], 1, sizeof(TtelegramHeader), xtelegram);
    move(xtelegram.Header,gMsg.datenArray,sizeof(TtelegramHeader));
    if TxnDevHandler.TxnWrite(gMsg,sizeof(TtelegramHeader)+ sizeof(TheaderRec))then begin
      Memo2.Lines.Insert(0,'Request GetLastDecl done');
      INC(gRecRequest);
      Form1.Timer.Caption:= '';
      gDateTime:= Time;
      Form1.Label5.Caption:= inttostr(gRecRequest);
    end else Memo2.Lines.Insert(0,'Request GetLatDecl failed');
  end;
end;

// Abruf der Base-Daten
procedure TForm1.acBasedataExecute(Sender: TObject);
var
  xtelegram: Ttelegram;
begin
  fillChar(gMsg, sizeof(gMsg),0);
  fillChar(xtelegram, sizeof(xtelegram), 0);
  gMsg.headerRec.sendNode:= 1;
  gMsg.headerRec.receiveNode:= gNode;
  gMsg.headerRec.blockNumber:=1;
  gMsg.headerRec.TotalBlocks:=1;
  SetTelegramHeader(eREQUEST_TELEGRAM, eBASE_DATA, 60
                          ,1, [cView], 1, sizeof(TtelegramHeader), xtelegram);
  move(xtelegram.Header, gMsg.datenArray, sizeof(TtelegramHeader));
  if TxnDevHandler.TxnWrite(gMsg,sizeof(TtelegramHeader)+ sizeof(TheaderRec))then begin
    Memo2.Lines.Insert(0,'Request: 60 Spdl. Base-Data');
    gRecRequest:= gRecRequest+60;
    Form1.Timer.Caption:= '';
    gDateTime:= Time;
    Form1.Label5.Caption:= inttostr(gRecRequest);
  end else Memo2.Lines.Insert(0,'Request Base-Data failed');
end;

// Abfrage von aller Spindeldaten
procedure TForm1.acMonitordataExecute(Sender: TObject);

var
  xtelegram: Ttelegram;
begin
  fillChar(gMsg, sizeof(gMsg),0);
  fillChar(xtelegram, sizeof(xtelegram), 0);
  gMsg.headerRec.sendNode:= 1;
  gMsg.headerRec.receiveNode:= gNode;
  gMsg.headerRec.blockNumber:=1;
  gMsg.headerRec.TotalBlocks:=1;
  SetTelegramHeader(eREQUEST_TELEGRAM, eMONITOR_DATA, 60
                          ,1, [cView], 1, sizeof(TtelegramHeader),xtelegram);
  move(xtelegram.Header, gMsg.datenArray, sizeof(TtelegramHeader));
  if TxnDevHandler.TxnWrite(gMsg, sizeof(TtelegramHeader)+ sizeof(TheaderRec))then begin
    Memo2.Lines.Insert(0,'Request: 60 Spdl. All-Data');
    gRecRequest:= gRecRequest+60;
    Form1.Timer.Caption:= '';
    gDateTime:= Time;
    Form1.Label5.Caption:= inttostr(gRecRequest);
  end else Memo2.Lines.Insert(0,'Request All-Data failed');
end;


procedure TForm1.mmActionList1Update(Action: TBasicAction;

var Handled: Boolean);
begin
  Handled := True;
  acPCI.Enabled:= mTesterState = tsInit;
  acISA.Enabled:= mTesterState = tsInit;
  acVersion.Enabled := mTesterState >= tsSelNode;
  acReset.Enabled:= mTesterState >= tsSelNode;
  acErrorcount.Enabled:= mTesterState >= tsSelNode;
  acGetMaConfig.Enabled:= mTesterState = tsSelData;
  acGetLastDecl.Enabled:= mTesterState = tsSelData;
  acBasedata.Enabled:= mTesterState = tsSelData;
  acMonitordata.Enabled:= mTesterState = tsSelData;
  acClear.Enabled:= mTesterState = tsSelData;
end;
end.
