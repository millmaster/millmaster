program PCITest;
 // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Forms,
  PCITestAppli in 'PCITestAppli.pas' {Form1},
  TXN_PCI_Def in 'TXN_PCI_Def.pas',
  TXN_PCI_DevClass in 'TXN_PCI_DevClass.pas';

{$R *.RES}

begin
  Application.Initialize;
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
