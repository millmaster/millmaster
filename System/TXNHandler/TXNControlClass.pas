(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXNControlClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: T3
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 01.12.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
|  3.11.1999  2.03  khp | AssigTimeout fuer Msg AssignComplete eingefuehrt
| 20.03.2000  2.05  khp | constructor Create new  reintroduce;
| 08.05.2000  2.04  khp | Bei AssignTimeout wird mit GetlastDeclEvent die Declaration in den JobRec uebertragen und
|                       | ClearLastDeclEvent ausgefuehrt.
| 08.08.2000  2.05  khp | Neu werden mttels SendMaschinenStatus die Maschinen (ON/OFF)Line jede Minute gesendet
| 12.09.2000  2.06  khp | Nach 5 aufeinanderfolgende TxnDevControl-Errors wird ein Reset des Texnet Device ausgeloest
|                       | nach weiteren 5 Versuchen  ein MMrestart eingeleitet
| 30.05.2000  2.12 khp  | Alle ON/Offline mit WriteLogDebug ausgeben.
|                       | On/Offline timeout 10 Sekunden
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 09.11.2001  3.13 khp  | CheckErrorcounters schreibt alle Minuten bei Aenderung der ErrorZaehler diese ins Protokoll.
|=========================================================================================*)


unit TXNControlClass;

interface

uses
  Windows, SysUtils,
  BaseThread, BaseGlobal, TXNPoolClass, TXN_glbDef, MMEventLog,
  TXN_ISA_DevClass, Txn_PCI_DevClass;

type
  TTXNControl = class(TBaseSubThread)
  PROTECTED
    TxnDevHandler: array[1..cMaxTxnAdapter] of TTxnBaseDevHandler;
    mMsgPoolList: TMsgPoolList;
    mAllMachineticker: Integer;
    //  mDeviceErrorCount: Integer;
    mOnOffLineRefreshcount: Integer;
    mDevMode: eDevModeT;
    procedure ProcessJob; OVERRIDE;
    procedure ProcessInitJob; OVERRIDE;
  PUBLIC
    constructor Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList;
      aPCIDEVHandlerArr: TPCIDEVHandlerArr; aDevMode: eDevModeT); REINTRODUCE;
    function DoConnect: Boolean; OVERRIDE;
    function Init: Boolean; OVERRIDE;
    destructor Destroy; OVERRIDE;
  end;

  //==============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  RzCSIntf,                             // Unit fuer CodeSite;
  YMParaDef;

constructor TTXNControl.Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList;
  aPCIDEVHandlerArr: TPCIDEVHandlerArr; aDevMode: eDevModeT);
var
  xadapter: Integer;
begin
  inherited Create(aThreadDef);
  mMsgPoolList := aMsgPoolList;
  mDevMode := aDevMode;
  // PCIDEVHandler wurde schon in der TXNHAndlerClass instanziert daher nur zuordnen
  if mDevMode = ePCI then
    for xadapter := 1 to mMsgPoolList.AdapterCount do begin
      TxnDevHandler[xadapter] := aPCIDEVHandlerArr[xadapter];
      TxnDevHandler[xadapter].DeviceErrorCount := 0;
      TxnDevHandler[xadapter].ErrorCounters := cErrorCountInit;
    end else
    for xadapter := 1 to cMaxTxnAdapter do
      TxnDevHandler[xadapter] := nil;
end;

//-----------------------------------------------------------------------------

destructor TTXNControl.Destroy;
{Disconnect aller TexnetDevice}
var
  xAdapterNo: Integer;
begin
  if mDevMode = eISA then
    for xAdapterNo := 1 to mMsgPoolList.AdapterCount do begin
      TxnDevHandler[xAdapterNo].TxnDisconnect;
      TxnDevHandler[xAdapterNo].Free;
    end;
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TTXNControl.DoConnect: Boolean;
{Connect aller definierter TexnetDevice}
var
  xAdapterNo, xAdapter: Integer;
begin
  result := inherited DoConnect;
  xAdapter := 1;
  if Result then begin
    try
      if mDevMode = eISA then           // Nur bei ISA Texnetkarte noetig
        for xAdapterNo := 1 to mMsgPoolList.AdapterCount do begin
          xAdapter := xAdapterNo;
          TxnDevHandler[xAdapterNo].ISAConnect(cTexnetDeviceName[xAdapterNo], omWrite);
        end;
    except
      Result := FALSE;
      // Error vom TexnetDriver hochziehen und ab ins Log
      fError := TxnDevHandler[xAdapter].ErrorRec;
      WriteLog(etError, FormatMMErrorText(Error));
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TTXNControl.Init: Boolean;
var
  xAdapterNo: Integer;
begin
  mAllMachineticker := 0;
  mOnOffLineRefreshcount := 0;
  Result := inherited Init;
  if Result then begin
    try
      if mDevMode = eISA then           // Nur bei ISA Texnetkarte noetig
        for xAdapterNo := 1 to mMsgPoolList.AdapterCount do begin
          TxnDevHandler[xAdapterNo] := TTxnDevHandler.Create;
          TxnDevHandler[xadapterNo].DeviceErrorCount := 0;
          TxnDevHandler[xadapterNo].ErrorCounters := cErrorCountInit;
        end;
    except
      Result := FALSE;
    end;
  end;
end;

//-----------------------------------------------------------------------------

procedure TTXNControl.ProcessInitJob;
begin
  // Timeout Control
  sleep(1000);
  if mMsgPoolList.BlockTimeout then begin
    ;
    WriteLog(etWarning, mMsgPoolList.ErrorRec.Msg);
  end;
end;

//-----------------------------------------------------------------------------

procedure TTXNControl.ProcessJob;
var
  xAdapterNo, xnode: Integer;
  xNewMachstate: Boolean;
  xNewMachstateArr: array[cMinTxnNodeID..cMaxTxnNodeID] of TTxnNetNodeRec;
  xOutBuf: TMsgIOControl;
  xOutBuflengh: Integer;

  procedure MoveMaAssigntoJob(aDecEvent: TDecEvent; adeclaration: Tdeclaration;
    var aMaAssign: TMaAssign);
    // Der Texnet Declaration-Record wird in den Job Declaration-Record umkopiert
  var
    xGroup: Integer;
  begin
    aMaAssign.Event := aDecEvent;
    with aMaAssign do begin
      for xgroup:=0 to cZESpdGroupLimit-1 do begin
        Groups[xgroup].GroupState   := gsInProd;
        Groups[xgroup].Modified     := adeclaration.validity[xgroup].modify;
        Groups[xgroup].ProdId       := adeclaration.validity[xgroup].prodGrpID;
        Groups[xgroup].SpindleFirst := adeclaration.validity[xgroup].range.firstSpindle;
        Groups[xgroup].SpindleLast  := adeclaration.validity[xgroup].range.lastSpindle;
        with Groups[xgroup] do begin
          CodeSite.SendFmtMsg('Decl-Rec: MaGroup=%d, ProdGrp=%d, Modifiy=%d, SpdFirst=%d, SpdLast=%d',
            [xgroup, ProdId, WORD(Modified), SpindleFirst, SpindleLast]);
        end;
      end;
    end;
  end;

  procedure SendAllMachineStatus;
    // Senden aller Maschinenstatus  an MsgDispatcher
  var
    xnode: Integer;
    xNodeList: TTxnNetNodeList;
  begin
    mMsgPoolList.GetNodeList(xNodeList);
    for xnode := cMinTxnNodeID to cMaxTxnNodeID do
      if xNodeList[xnode].MachID <> 0 then begin
        if xNodeList[xnode].MachState = nsOnline then
          mJob.JobTyp := jtMaOnline
        else
          mJob.JobTyp := jtMaOffline;
        mJob.MaOnline.MachineID := xNodeList[xnode].MachID;
        WriteJobBufferTo(ttMsgDispatcher);
      end;
  end;

  procedure CheckErrorcounters(aTxnDevHandler: TTxnBaseDevHandler; aAdapterNo: Integer);
    // Fehlerzaehler lesen und wenn veraendert bei Debugmode ins Protkoll schreiben  !
  var
    xOutBuf: TMsgIOControl;
    xOutBuflengh: Integer;
    xChange: Boolean;
    xErrorCounter: TErrorCountRec;
  begin
    if aTxnDevHandler.TxnDevControl(IOCTL_TEXNET_ERRSTATE, xOutBuf, xOutBuflengh) then begin
      xChange := false;
      with aTxnDevHandler do begin
        xErrorCounter := ErrorCounters;
        if xOutBuf[0] <> xErrorCounter.PaFrameError then xChange := true;
        if xOutBuf[1] <> xErrorCounter.BCCError then xChange := true;
        if xChange then begin
          xErrorCounter.PaFrameError := xOutBuf[0];
          xErrorCounter.BCCError := xOutBuf[1];
          ErrorCounters := xErrorCounter;
          WriteLog(etWarning, Format('Texnet AdapterNo: %d Parity/Frame Error= %d  BCC Error= %d ',
            [aAdapterNo, xErrorCounter.PaFrameError, xErrorCounter.BCCError]));
          // WriteLogDebug( Format('Texnet AdapterNo: %d Parity/Frame Error= %d  BCC Error= %d ',
          //                      [aAdapterNo, xErrorCounter.PaFrameError, xErrorCounter.BCCError] ), NIL, 0, TRUE);
        end;
      end;
    end;
  end;

begin
  sleep(1000);                          //  Je Sekunde do it
  // JobHeader vorbereiten
  mJob.JobID := cUnknownJobID;
  mJob.NetTyp := ntTXN;
  // TimeOut dekrementiern und Meldung wenn AssignTimeout abgelaufen ist
  for xnode := cMinTxnNodeID to cMaxTxnNodeID do begin
    { DONE -onue -csystem : Test ohne Timeout }
    if mMsgPoolList.AssignTimeoutExpired(xnode) then begin
      mJob.JobTyp := jtAssignComplete;
      mJob.AssignComplete.MachineID := mMsgPoolList.GetMachID(xnode);
      MoveMaAssigntoJob(deEntryLocked, mMsgPoolList.GetlastDeclEvent(xNode),
        mJob.Assign.Assigns);
      CodeSite.SendMsg('Timeout: jtAssignComplete');
      WriteJobBufferTo(ttMsgDispatcher);
      mMsgPoolList.ClearLastDeclEvent(xNode);
    end;
  end;
  // NetnodeListe und dann von allen Adaptern MachState lesen und Msgpool updaten
  INC(mOnOffLineRefreshcount);
  if mOnOffLineRefreshcount >= cOnOffLineRefreshtime then begin
    // On-OffLineControl nur alle 10 Sekunden
    mOnOffLineRefreshcount := 0;        // Zurueckstellen
    INC(mAllMachineticker);
    for xAdapterNo := 1 to mMsgPoolList.AdapterCount do begin
      if mMsgPoolList.UpdateMachState(TxnDevHandler[xAdapterNo], xAdapterNo, xNewMachstateArr, xNewMachstate) then begin
        // Jede Minute Maschinenstatus aller Maschinen an MsgDispatcher senden und
        // Fehlerstatus des Adapters ins Protokoll schreiben
        if mAllMachineticker >= cMachinetickertime then begin {6 * mOnOffLineRefreshcount }
          SendAllMachineStatus;
          if Debugmode then
            CheckErrorcounters(TxnDevHandler[xAdapterNo], xAdapterNo);
          // Zurueckstellen  von mAllMachineticker bei letzter maschine
          if xAdapterNo = mMsgPoolList.AdapterCount then mAllMachineticker := 0;
        end else
          // Auf Aenderung des Maschinenstatus pruefen und MsgDispatcher melden
          if xNewMachstate then begin
            for xnode := cMinTxnNodeID to cMaxTxnNodeID do begin
              if xNewMachstateArr[xnode].MachID <> 0 then begin
                case xNewMachstateArr[xnode].MachState of
                  nsOnline: begin
                      mJob.JobTyp := jtMaOnline;
                      mJob.MaOnline.MachineID := xNewMachstateArr[xnode].MachID;
                      WriteLogDebug(Format('Machine %d ONLINE', [mJob.MaOnline.MachineID]), nil, 0, TRUE);
                      WriteJobBufferTo(ttMsgDispatcher);
                    end;
                  nsOffline: begin
                      mJob.JobTyp := jtMaOffline;
                      mJob.MaOffline.MachineID := xNewMachstateArr[xnode].MachID;
                      WriteLogDebug(Format('Machine %d OFFLINE', [mJob.MaOffline.MachineID]), nil, 0, TRUE);
                      WriteJobBufferTo(ttMsgDispatcher);
                    end;
                else ;
                end;
              end;
            end;                        // for
          end;
        // Ruecksetzen des DeviceErrorCount da UpdateMachState ok
        TxnDevHandler[xAdapterNo].DeviceErrorCount := 0;
      end else begin
        // Error vom TexnetDriver hochziehen und ab ins Log
        fError := TxnDevHandler[xAdapterNo].ErrorRec;
        WriteLog(etWarning, 'TTXNControl.ProcessJob' + FormatMMErrorText(Error));
        // Wenn 5 TxnDevControl-Errors eingetreten sind wird ein TEXNET_RESET ausgefuehrt
        TxnDevHandler[xAdapterNo].DeviceErrorCount := TxnDevHandler[xAdapterNo].DeviceErrorCount + 1;
        if TxnDevHandler[xAdapterNo].DeviceErrorCount = 5 then begin
          TxnDevHandler[xAdapterNo].TxnDevControl(IOCTL_TEXNET_RESET, xOutBuf, xOutBuflengh);
          WriteLog(etWarning, 'TTXNControl.ProcessJob' + ' Texnet Reset');
          TxnDevHandler[xadapterNo].ErrorCounters := cErrorCountInit;
        end;
        // Wenn 10 TxnDevControl-Errors eingetreten sind wird ein MMRestart eingeleitet
        if TxnDevHandler[xAdapterNo].DeviceErrorCount >= 10 then begin
          WriteLog(etError, 'TTXNControl.ProcessJob' + ' RestartMM');
          WriteToMain(gcRestartMM, Error.Error);
        end;
      end;
    end;
  end;                                  // OnOffLineControl
  // Timeout Control
  if mMsgPoolList.BlockTimeout then begin
    WriteLog(etWarning, mMsgPoolList.ErrorRec.Msg);
  end;
end;
//-----------------------------------------------------------------------------
end.

