object Form1: TForm1
  Left = 91
  Top = 144
  BorderStyle = bsDialog
  Caption = 'PCI Texnet Tester'
  ClientHeight = 411
  ClientWidth = 808
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object Label3: TLabel
    Left = 137
    Top = 65
    Width = 69
    Height = 13
    Caption = 'Adapter List'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object mmLabel1: TmmLabel
    Left = 718
    Top = 72
    Width = 56
    Height = 13
    Caption = 'Parity - BCC'
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object GroupBox1: TGroupBox
    Left = 554
    Top = 161
    Width = 241
    Height = 72
    Caption = ' Request Counter '
    TabOrder = 12
    object Label5: TLabel
      Left = 23
      Top = 50
      Width = 7
      Height = 16
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 81
      Top = 50
      Width = 7
      Height = 16
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Timer: TLabel
      Left = 148
      Top = 50
      Width = 7
      Height = 16
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Button2: TButton
      Left = 202
      Top = 13
      Width = 33
      Height = 27
      Action = acClear
      Caption = 'Clear'
      TabOrder = 0
    end
    object StaticText1: TStaticText
      Left = 20
      Top = 33
      Width = 24
      Height = 17
      Caption = 'sent'
      TabOrder = 1
    end
    object StaticText3: TStaticText
      Left = 78
      Top = 33
      Width = 45
      Height = 17
      Caption = 'received'
      TabOrder = 2
    end
    object StaticText4: TStaticText
      Left = 150
      Top = 33
      Width = 30
      Height = 17
      Caption = 'Timer'
      TabOrder = 3
    end
  end
  object Button5: TButton
    Left = 420
    Top = 127
    Width = 52
    Height = 20
    Action = acVersion
    Caption = 'Version'
    TabOrder = 1
  end
  object Button6: TButton
    Left = 552
    Top = 96
    Width = 49
    Height = 20
    Action = acErrorcount
    Caption = 'Errors'
    TabOrder = 2
  end
  object ListBox1: TListBox
    Left = 13
    Top = 59
    Width = 98
    Height = 345
    Color = clWhite
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 16
    ParentFont = False
    TabOrder = 3
    OnClick = ListBox1Click
  end
  object Button8: TButton
    Left = 420
    Top = 96
    Width = 53
    Height = 20
    Action = acReset
    TabOrder = 4
  end
  object Button1: TButton
    Left = 273
    Top = 189
    Width = 66
    Height = 20
    Action = acBasedata
    Caption = 'Base data'
    TabOrder = 6
  end
  object Button3: TButton
    Left = 345
    Top = 189
    Width = 66
    Height = 20
    Action = acMonitordata
    Caption = 'All data'
    TabOrder = 0
  end
  object Button4: TButton
    Left = 202
    Top = 189
    Width = 66
    Height = 20
    Action = acGetLastDecl
    Caption = 'GetLastDecl'
    TabOrder = 7
  end
  object Button9: TButton
    Left = 130
    Top = 189
    Width = 66
    Height = 20
    Action = acGetMaConfig
    Caption = 'GetMaConfig'
    TabOrder = 8
  end
  object ListBox3: TListBox
    Left = 137
    Top = 85
    Width = 272
    Height = 72
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ItemHeight = 13
    ParentFont = False
    TabOrder = 9
    OnClick = ListBox3Click
  end
  object ProgressBar1: TProgressBar
    Left = 13
    Top = 33
    Width = 98
    Height = 13
    Min = 40
    Max = 80
    Position = 40
    TabOrder = 11
    Visible = False
  end
  object StaticText2: TStaticText
    Left = 13
    Top = 13
    Width = 96
    Height = 17
    Caption = 'Available Nodes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 10
  end
  object StaticText5: TStaticText
    Left = 124
    Top = 215
    Width = 103
    Height = 17
    Caption = 'Message Window'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 14
  end
  object RadioButton1: TRadioButton
    Left = 137
    Top = 33
    Width = 52
    Height = 13
    Action = acISA
    Caption = 'ISA'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 15
  end
  object RadioButton2: TRadioButton
    Left = 215
    Top = 33
    Width = 46
    Height = 13
    Action = acPCI
    Caption = 'PCI'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 16
  end
  object Memo1: TmmMemo
    Left = 608
    Top = 85
    Width = 185
    Height = 72
    ScrollBars = ssVertical
    TabOrder = 17
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object Memo4: TmmMemo
    Left = 408
    Top = 32
    Width = 387
    Height = 17
    BorderStyle = bsNone
    Color = clInfoBk
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 13
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object Memo2: TmmMemo
    Left = 124
    Top = 234
    Width = 669
    Height = 170
    Color = cl3DLight
    Lines.Strings = (
      '')
    ScrollBars = ssVertical
    TabOrder = 5
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmActionList1: TmmActionList
    OnUpdate = mmActionList1Update
    Left = 272
    Top = 8
    object acReset: TAction
      Caption = 'Reset'
      OnExecute = acResetExecute
    end
    object acVersion: TAction
      Caption = 'acVersion'
      OnExecute = acVersionExecute
    end
    object acErrorcount: TAction
      Caption = 'acErrorcount'
      OnExecute = acErrorcountExecute
    end
    object acPCI: TAction
      Caption = 'acPCI'
      OnExecute = acPCIExecute
    end
    object acISA: TAction
      Caption = 'acISA'
      OnExecute = acISAExecute
    end
    object acClear: TAction
      Caption = 'acClear'
      OnExecute = acClearExecute
    end
    object acGetMaConfig: TAction
      Caption = 'acGetMaConfig'
      OnExecute = acGetMaConfigExecute
    end
    object acGetLastDecl: TAction
      Caption = 'acGetLastDecl'
      OnExecute = acGetLastDeclExecute
    end
    object acBasedata: TAction
      Caption = 'acBasedata'
      OnExecute = acBasedataExecute
    end
    object acMonitordata: TAction
      Caption = 'acMonitordata'
      OnExecute = acMonitordataExecute
    end
  end
end
