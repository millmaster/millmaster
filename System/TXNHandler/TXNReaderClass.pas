(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXNReaderClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: T1
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 6T PCI, Pentium 450, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 01.12.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
| 20.04.1999  2.01t khp | Wenn Property Terminated gesetzt-> keine Fehlermeldungen mehr
|  3.11.1999  2.03  khp | AssigTimeout fuer Msg AssignComplete eingefuehrt, ACKNOWLEDGE bei
|                       | Setsettings
| 11.11.1999  2.04  khp | Spindelbereich, MaschID(xmachId)und Length bei Confirm.eMACONFIG_DATA
|                       | und Ack. Telegramm eingefuegt
| 23.11.1999  2.05  khp | Der Spindelbereich im TexnetHeader wird in erster Linie fuer Request-Telegramms
|                       | welche einen Bereich abfragen benoetigt. Daher wird dieser Spindelbereich nicht
|                       | in den JobHeader kopiert.
| 20.03.2000  2.05  khp | constructor Create new  reintroduce;
| 29.03.2000  2.06  khp | In MoveMaAssigntoJob GroupState:= gsInProd
|                       | MACONFIG_DATA in CONFIRM_TELEGRAM ausgeklammert
| 08.05.2000  2.07  khp | Bei AssignComplete wird mit GetlastDeclEvent die Declaration in den JobRec uebertragen und
|                       | ClearLastDeclEvent ausgefuehrt.
|                       | Bei Assign wird mit UpdateLastDecl die Declaration zwischengespeichert.
| 25.05.2000  2.08khpNue| GetlastDeclEvent wird nicht mehr benoetigt (Modify wird bei AssignComplete
|                       | nicht mehr ausgewertet in GetMaAssign)
| 13.06.2000  2.09 khp  | jtGetMaAssign; Neu UpdateProdGrp,wenn Modify gesetzt ist muss Prodgruppe auf 0 gesetzt werden
| 11.06.2000  2.10 khp  | AssignComplete: Neu SetProdGrpInfoValues  fuellt default Werte ein
| 14.07.2000  2.11 khp  | ProdGrpInfo.c_prod_name wird bei GetSettings auf 0 gesetzt.
| 30.05.2000  2.12 khp  | Alle Telegramtypen von ZE mit WriteLogDebug ausgeben.
|                       | eEVT_INITIAL_RESET auch im NT Format bearbeitet
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 05.12.2001  3.01  khp | TTXNReader.ProcessJob:  mJob mit 0 initialisiert (FillChar)
| 31.01.2002  3.02  khp | Bei Declaration eEVT_ENTRY_LOCKED c_YM_set_id:= 0 und c_YM_set_name:='';
| 05.03.2002  3.03  khp | Confirm telegram: mJob.GetSettings mit CompleteSettingsFromZE wird System Garnunit in RE-Sack geschrieben
| 05.10.2002        LOK | Umbau ADO
| 28.10.2002  3.04  khp | Kaltstart: jtGetZEReady wird nicht mehr ben�tigt da LastEvent aus Decl Record von eDECLARATIONS gelesen wird.
| 05.11.2002  3.05  khp | Bei Msg EVT_ENTRY_LOCKED wird in mMsgPoolList.GetlastDeclEvent das modify Flag in allen Gruppen �bernommen.
| 03.03.2005  1.11  Wss | Umbau nach 0-basiertes Gruppen/Settingshandling innerhalb MM-System
| 06.04.2005  1.11  Wss | Umlenken des BD-Reinigers zu TK840
| 15.07.2005  1.11  Wss | Von ZE kommt als YarnUnit IMMER yuNm!!! Nun wird der Wert mit der globalen Einstellung
|                       | per TSettingsReader �berschrieben (in BinToXMLEventHandler).
| 09.09.2005        Lok | Wenn keine Gruppe definiert ist (Kaltstart) wird beim Upload neu eine aussagekr�ftige
|                       |   Fehlermeldung ausgegeben.
|=========================================================================================*)

unit TXNReaderClass;

interface

uses
  Windows,
  BaseThread, BaseGlobal, TXNPoolClass, TXN_glbDef, MMEventLog, MMUGlobal,
  YMParaUtils, YMParaDef, SettingsReader, TXN_ISA_DevClass, TXN_PCI_DevClass,
  TXN_PCI_Def, MMXMLConverter, MSXML2_TLB;

type
  TTXNReader = class(TBaseSubThread)
  private
    mAdapterNo: Integer;
    mDevMode: eDevModeT;
    mEmptyTxnBuffer: Boolean;
    mErrorCount: Integer;
    mMsgPoolList: TMsgPoolList;
    mTempXMLSettings: TXMLSettingsRec;
    mTempXMLString: String;
    TxnDevHandler: TTxnBaseDevHandler;
  protected
    procedure BeforeDeleteElementsEventHandler(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure AfterDeleteElementsEventHandler(Sender: TObject; const aMapDoc:
        DOMDocument40; const aMMXMLDoc: DOMDocument40);
    procedure BinToXMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
    procedure ProcessInitJob; override;
    procedure ProcessJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList; aAdapterNo: Integer; aPCIDEVHandler: TPCITxnDevHandler; aDevMode: eDevModeT);
        reintroduce;
    destructor Destroy; override;
    function DoConnect: Boolean; override;
    function Init: Boolean; override;
  end;

  //==============================================================================

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  sysutils, Classes, typinfo, XMLDef, XMLMappingDef, XMLGlobal, xmlMaConfigClasses,
  FPatternClasses, XMLSettingsModel, LoepfeGlobal, XMLSettingsTools;                             // Unit fuer CodeSite;

//:-----------------------------------------------------------------------------
constructor TTXNReader.Create(aThreadDef: TThreadDef; aMsgPoolList: TMsgPoolList; aAdapterNo: Integer;
  aPCIDEVHandler: TPCITxnDevHandler; aDevMode: eDevModeT);
begin
  inherited Create(aThreadDef);
  mAdapterNo     := aAdapterNo;           // Uebergabe der AdapterNo
  mDevMode       := aDevMode;
  mMsgPoolList   := aMsgPoolList;         // Uebergabe des MsgPoolHandle
  // PCIDEVHandler wurde schon in der TXNHAndlerClass instanziert daher nur zuordnen
  if mDevMode = ePCI then TxnDevHandler := aPCIDEVHandler
                     else TxnDevHandler := nil;
end;

//:-----------------------------------------------------------------------------
destructor TTXNReader.Destroy;
begin
  Terminate;                            // Terminate Reader Thread
  if mDevMode = eISA then begin
    TxnDevHandler.TxnDisconnect;
    TxnDevHandler.Free;
  end;
  inherited Destroy;
end;
//:-----------------------------------------------------------------------------
procedure TTXNReader.BeforeDeleteElementsEventHandler(Sender: TObject; const aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xGroupElement: IXMLDOMElement;
  xMachineElement: IXMLDOMElement;
  xFPattern: TFPBuilder;
begin
  // nur bei Settings konvertieren die YarnUnit/Count bearbeiten
  if TMMXMLConverter(Sender).MapSection = msYMSetting then begin
    // Nun m�ssen noch die Klassierfelder transformiert werden
    SwapClassFields(aMMXMLDoc.selectSingleNode(cXPYMSettingBasicClassificationItem));
    SwapClassFields(aMMXMLDoc.selectSingleNode(cXPFDarkClassificationItem));
    SwapClassFields(aMMXMLDoc.selectSingleNode(cXPDarkClusterClassificationItem));
    with mTempXMLSettings do
    try
      YarnCnt := YarnCountConvert(YarnCntUnit, yuNm, YarnCnt);
      if NrOfThreads > 1 then
        YarnCnt := YarnCnt / NrOfThreads;
    except
      // Einfach nichts machen da YarnCount bereits abgef�llt (NrOfThreads = 0)
    end;
  end;

  // F�r Texnet und WSC muss das FPattern aus den Settings erzeugt werden
  xFPattern := TFPBuilder.Create;
  try
    Supports(aMMXMLDoc.SelectSingleNode(cXPMachineNode), IXMLDOMElement, xMachineElement);
    xFPattern.MachineElement := xMachineElement;

    Supports(aMMXMLDoc.SelectSingleNode(cXPGroupNode), IXMLDOMElement, xGroupElement);
    xFPattern.GroupElement := xGroupElement;

    // BD-Reiniger wird nicht mehr unterst�tzt!!
    if GetSensingHeadValue(xFPattern.GroupValueDef[cXPSensingHeadItem, cSensingHeadNames[shTK830]]) = shTK940BD then
      xFPattern.GroupValue[cXPSensingHeadItem] := cSensingHeadNames[shTK840];

    xFPattern.BuildFPatternFromConfig(ntTxn, TMMXMLConverter(Sender).MapSection);
  finally
    xFPattern.Free;
  end;// try finally

  // �ndert bei bedarf die CheckLength anhand des ConfigCode C4 (Multiplikation mit 32)
  TCheckLengthCalculator.CalculateCheckLengthForUpload(aMMXMLDoc);
end;

//:-----------------------------------------------------------------------------
procedure TTXNReader.AfterDeleteElementsEventHandler(Sender: TObject; const
    aMapDoc: DOMDocument40; const aMMXMLDoc: DOMDocument40);
var
  xPatternNode: IXMLDOMNode;
begin
  if mTempXMLString <> '' then begin
    xPatternNode := aMMXMLDoc.selectSingleNode(mTempXMLString);
    if Assigned(xPatternNode) then
      mTempXMLString := xPatternNode.xml;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TTXNReader.BinToXMLEventHandler(Sender: TObject; var aEventRec: TElementEventRec; var aDeleteFlag: Boolean);
var
  xSH: TSensingHead;
begin
  if not VarIsNull(aEventRec.Value) then begin
    if AnsiSameText(aEventRec.CalcElementName, cXPSensingHeadItem) then begin
      xSH := GetSensingHeadValue(aEventRec.Value);
      mTempXMLSettings.HeadClass := GetSensingHeadClass(xSH);
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPGroupItem) then
      mTempXMLSettings.Group := aEventRec.Value // F�r ZE muss die Gruppe nicht angepasst werden, da die Berechnung im Mapfile definiert ist
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPProdGrpIDItem) then
      mTempXMLSettings.ProdGrpID := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPPilotSpindlesItem) then
      mTempXMLSettings.PilotSpindles := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPYarnCountItem) then
      // Yarncount wird in OnAfterConvert noch nach Nm umkonvertiert
      mTempXMLSettings.YarnCnt := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPYarnUnitItem) then begin
      mTempXMLSettings.YarnCntUnit := TYarnUnit(TMMSettingsReader.Instance.Value[cYarnCntUnit]);
//      mTempXMLSettings.YarnCntUnit := TYarnUnit(GetEnumValue(TypeInfo(TYarnUnit), aEventRec.Value));
//      if mTempXMLSettings.YarnCntUnit = yuNone then
//        mTempXMLSettings.YarnCntUnit := yuNm;
    end else
    if AnsiSameText(aEventRec.CalcElementName, cXPThreadCountItem) then
      // Wert in XML mit default Wert �berschreiben
      mTempXMLSettings.NrOfThreads := c_DefaultNr_of_threads
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPLengthWindowItem) then
      mTempXMLSettings.LengthWindow := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPLengthModeItem) then
      // In den Record kommt z.B. eine 1 (=lwmFirst) welche per GetEnumValue in Byte Wert umgewandelt wird
      // Im Konverter wird entsprechend der bin�re Wert vom Inf anhand einer Enum-Liste in den Text gewandelt
      mTempXMLSettings.LengthMode := GetEnumValue(TypeInfo(TLengthWindowMode), aEventRec.Value)
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleFromItem) then
      mTempXMLSettings.SpindleFirst := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpindleToItem) then
      mTempXMLSettings.SpindleLast := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedRampItem) then
      mTempXMLSettings.SpeedRamp := aEventRec.Value
    else
    if AnsiSameText(aEventRec.CalcElementName, cXPSpeedItem) then
      mTempXMLSettings.Speed := aEventRec.Value
    else if AnsiSameText(aEventRec.CalcElementName, cXPReinVerItem) then
      if (aEventRec.Value = 0) then begin
        aEventRec.Value := cZEDefaultSpectraReinVer;
        if aEventRec.ArgumentList.ValueDef(cXPFP_ShortCountItem, false) then
          aEventRec.Value := cZEDefaultSpectraPlusReinVer;
      end;// if (aEventRec.Value = 0) then begin
  end;
end;

//:-----------------------------------------------------------------------------
function TTXNReader.DoConnect: Boolean;
begin
  result := inherited DoConnect;
  if Result then
  try
    if mDevMode = eISA then
      TxnDevHandler.ISAConnect(cTexnetDeviceName[mAdapterNo], omReadWrite);
//      TxnDevHandler.ISAConnect(cTexnetDeviceName[mAdapterNo], omRead);
  except
    Result := FALSE;
    // Error vom TexnetDriver hochziehen und ab ins Log
    fError := TxnDevHandler.ErrorRec;
    WriteLog(etError, FormatMMErrorText(Error));
  end;
end;

//:-----------------------------------------------------------------------------
function TTXNReader.Init: Boolean;
begin
  Result := inherited Init;
  if Result then
  try
    if mDevMode = eISA then
      TxnDevHandler := TTxnDevHandler.Create;
    mEmptyTxnBuffer := FALSE;
    mErrorCount := 0;
  except
    Result := FALSE;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TTXNReader.ProcessInitJob;
var
  xInBuf: TdatablockRec;
  xInBufLengh: Integer;
  xOutBuf: TMsgIOControl;
  xOutBuflengh: Integer;
begin
  if not mEmptyTxnBuffer then begin
    TxnDevHandler.TxnDevControl(IOCTL_TEXNET_RESET, xOutBuf, xOutBuflengh);
    repeat                              // Leere Texnet Buffer der Zentralen
    until TxnDevHandler.TxnRead(xInBuf, xInBufLengh) = False;
    mEmptyTxnBuffer := TRUE;
  end;
  TxnDevHandler.ReadTimeout := cPCIReadtimeout;
  ProcessJob;
end;

//:-----------------------------------------------------------------------------
procedure TTXNReader.ProcessJob;
var
  xOrgJob: TJobTyp;
  xTelegram: Ttelegram;
  xInBuf: TdatablockRec;
  xInBufLengh: Integer;
  xTelgramsize: Integer;
  xNodeID: TNodeID;
  xMachID: Integer;
  xValidJob: Boolean;
  xDecEvent: TDecEvent;
  xXMLData: String;
  xJobID: DWord;
  //.......................................................
  procedure MoveMaAssigntoJob(aDecEvent: TDecEvent; aDeclaration: TDeclaration; var aMaAssign: TMaAssign);
  // Der Texnet Declaration-Record wird in den Job Declaration-Record umkopiert
  var
    xGroup: Integer;
  begin
    aMaAssign.Event := aDecEvent;
    with aMaAssign do begin
      for xGroup:=0 to cZESpdGroupLimit-1 do begin
        Groups[xGroup].GroupState     := gsInProd;
        Groups[xGroup].Modified       := aDeclaration.validity[xGroup].modify;
        Groups[xGroup].ProdId         := aDeclaration.validity[xGroup].prodGrpID;
        Groups[xGroup].SpindleFirst   := aDeclaration.validity[xGroup].range.firstSpindle;
        Groups[xGroup].SpindleLast    := aDeclaration.validity[xGroup].range.lastSpindle;
        with Groups[xgroup] do begin
          CodeSite.SendFmtMsg('Decl-Rec: MaGroup=%d, ProdGrp=%d, Modifiy=%d, SpdFirst=%d, SpdLast=%d',
            [xgroup, ProdId, WORD(Modified), SpindleFirst, SpindleLast]);
        end;
      end;
    end;
  end;
  //.......................................................
  procedure SetProdGrpID(var aDeclaration: TDeclaration);
  // Wenn Modify gesetzt ist muss Prodgruppe auf 0 gesetzt werden
  var
    i: Integer;
  begin
    for i:=0 to cMaxZESpdGroups-1 do
      with aDeclaration.validity[i] do begin
        if modify <> [] then begin
          prodGrpID := 0;
          modify    := [];
        end;
      end;
  end;
  //.......................................................
  procedure SetProdGrpInfoValues(var aGetMaAssign: TMaAssign; aMachID: Integer);
  var
    xGroup: Integer;
  begin
    for xGroup:=0 to cZESpdGroupLimit-1 do begin
      with aGetMaAssign.Groups[xGroup] do begin
        Color := cDefaultStyleIDColor;
        ProdGrpParam.ProdGrpInfo.c_order_position_id   := cDefaultOrderPositionID;
        ProdGrpParam.ProdGrpInfo.c_order_id            := cDefaultOrderID;
        ProdGrpParam.ProdGrpInfo.c_machine_id          := aMachID;
//NUETMP        ProdGrpParam.ProdGrpYMPara.group               := xGroup;
        ProdGrpParam.ProdGrpInfo.c_slip                := cDefaultSlip;
        ProdGrpParam.ProdGrpInfo.c_YM_set_id           := 0;
        FillChar(ProdGrpParam.ProdGrpInfo.c_YM_set_name, sizeof(ProdGrpParam.ProdGrpInfo.c_YM_set_name), 0);
        ProdGrpParam.ProdGrpInfo.c_YM_set_name[1]      := CHR(32);
        Move(cDefaultStartName, ProdGrpParam.ProdGrpInfo.c_prod_name, sizeof(cDefaultStartName));
      end;
    end;
  end;
  //.......................................................
  // Maschinenkonfiguration wurde empfangen. Die Gruppendaten auswerten und Settings anfordern
  procedure HandleSaveMaConfigToDB_Step1;
  var
    i: Integer;
    xAssign: TMaAssign;
    xCount: Integer;
  begin
    // Empfangene Daten aus Telegramm zwischenkopieren, da der Telegrammbuffer �berschrieben wird
    xCount := 0;
    MoveMaAssigntoJob(deNone, PDeclaration(@xTelegram.Daten)^, xAssign);
    // Jobheader mal vordefinieren
    with mJob^, GetSettings do begin
      JobTyp       := jtGetSettings;
      JobID        := xJobID;
      MachineID    := xMachID;
      AllXMLValues := False;
    end;
    // alle Gruppen durchz�hlen und die mit den g�ltigen Spindelbereichen verarbeiten
    for i:=0 to cZESpdGroupLimit-1 do begin
      with xAssign.Groups[i] do begin
        // nur Gruppen mit g�ltigen/aktiven Spindelbereichen ber�cksichtigen
        if (SpindleFirst > 0) and (SpindleFirst < $FF) and
           (SpindleLast  > 0) and (SpindleLast  < $FF) then begin
          // Get settings from spindle range
          mJob^.GetSettings.SettingsRec.SpindleFirst := SpindleFirst;
          mJob^.GetSettings.SettingsRec.SpindleLast  := SpindleLast;
          mJob^.GetSettings.SettingsRec.XMLData      := #00;
          // Die Anfrage nun an den TXNWriter schicken
          WriteJobBufferTo(ttTXNWriter);
          // Z�hler um 1 erh�hen f�r die Abarbeitung der Antworten
          inc(xCount);
        end; // if Spindelbereich
      end; // with xAssign
    end; // for i
    if xCount > 0 then
      mMsgPoolList.JobIDValue[xJobID, cJobValueSettingsCount] := xCount
    else
      WriteLog(etWarning, 'No valid/active group defined from machine');
  end;
  //.......................................................
  // Settings der aktiven Gruppen werden empfangen. Bauzustand ermitteln und verarbeiten
  procedure HandleSaveMaConfigToDB_Step2;
  var
    xDefaultsXML: string;
    xLastSpindle: Integer;
    xInt: Integer;
    xTmpJob: PJobRec;
    xMapID: String;
    xMapfile: DOMDocument40;
    xXMLData: String;
  begin
    xMapID   := mMsgPoolList.MapID[xMachID];
    xMapfile := mMsgPoolList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        // XPath f�r AfterConvert setzen
        mTempXMLString         := cXPConfigNode;
        // Gilt f�r MaConfig und Defaults (Elemente werden nicht aus dem XML gel�scht)
        OnAfterDeleteElements  := AfterDeleteElementsEventHandler;  // per XPath wird das Bauzustandsfragment extrahiert
        OnBeforeDeleteElements := BeforeDeleteElementsEventHandler;
        try
          // TODO wss: Mapdatei f�r Bauzustand ermitteln
          BinToXML(@xTelegram.Daten, Sizeof(xTelegram.Daten), xMapfile, msMaConfig, False);
          // XML Daten f�r diese Gruppe zwischenspeichern
          xXMLData := mMsgPoolList.JobIDValue[xJobID, cJobValueXMLMaConfig];
          xXMLData := xXMLData + mTempXMLString;
          CodeSite.SendString('XMLData', xXMLData);
          mMsgPoolList.JobIDValue[xJobID, cJobValueXMLMaConfig] := xXMLData;

          // Z�hler um 1 verkleinern
          xInt := mMsgPoolList.JobIDValue[xJobID, cJobValueSettingsCount];
          dec(xInt);
          // sind alle angeforderten Guppen empfanen?
          if xInt = 0 then begin
            IgnoreDeleteFlag             := True; // bewirkt, dass alle Elemente im XML drin bleiben
            mTempXMLSettings.YarnCntUnit := yuNm; // verhindert error in EventHandler bei Garnnummerumrechnung
            // jupp, nun noch die Default Werte per Konverter ermitteln
            OnCalcExternal         := Nil;
            OnAfterDeleteElements  := Nil; //GetDefaultsAfterDelete;  // Default werden fix aus dem DOM in mTempXMLString herauskopiert
            OnBeforeDeleteElements := Nil;
            // wenn alle GetSettings empfangen wurden, dann die Bauzustandsdaten parsern und speichern
            // aber zuerst noch die Defaults ermitteln und mit dem Bauzustand ablegen
            xDefaultsXML := BinToXML(Nil, 0, xMapfile, msYMSetting, False);
            // Zusammengefasste Bauzust�nde noch weiter verarbeiten
            xXMLData := mMsgPoolList.JobIDValue[xJobID, cJobValueXMLMaConfig];

            // Transformnation in Bauzustandsdaten (Gruppen und Maschine getrennt)
            with TXMLMaConfigHelper.Create do
            try
              xXMLData     := BuildMaConfigFromHandler(cXMLHeader + xXMLData + cXMLFooter, xDefaultsXML, ntWSC, msMaConfig);
              xLastSpindle := LastSpindle;
            finally
              Free;
            end;// with TStringList.Create do try

            xInt    := Length(xXMLData);
            xTmpJob := AllocMem(GetJobHeaderSize(jtSaveMaConfigToDB) + cardinal(xInt));
            try
              with xTmpJob^, SaveMaConfig do begin
                // JobID muss nochmals gesetzt werden, da bei einem ReallocMem gel�scht wird
                JobTyp      := jtSaveMaConfigToDB;
                JobID       := xJobID;
                NetTyp      := ntTXN;
                MachineID   := xMachID;
                LastSpindle := xLastSpindle;
                UseXMLData  := True;
                // die XMLSettings in Jobbuffer hineinkopieren
                System.Move(PChar(xXMLData)^, XMLData, xInt);
              end;
              WriteJobBufferTo(ttMsgDispatcher, xTmpJob);
              mMsgPoolList.DeleteJobIDValue(xJobID);
            finally
              FreeMem(xTmpJob);
            end;
          end else // if xInt = 0
            // wenn noch nicht alle empfangen, dann halt den Counter wieder in die Liste speichern
            mMsgPoolList.JobIDValue[xJobID, cJobValueSettingsCount] := xInt;
        except
          on e:Exception do
            WriteLog(etError, 'TTXNReader.ProcessJob.eSETTINGS.HandleSaveMaConfigToDB_Step2:' + e.Message);
        end; // try BinToXML(
      finally
        pointer(xMapfile) := nil;  // verhindern dass Interface freigegeben wird
        Free;
      end; // with TMMXMLConverter.Create
    end; // if Assigned(xMapfile)
  end;
  //.......................................................
  procedure HandleGetMaAssign;
  begin
    mJob.JobTyp := jtGetMaAssign;
    CodeSite.SendMsg('Beg.GetMaAssign');
    mJob.GetMaAssign.MachineID := xMachID;
    SetProdGrpID(PDeclaration(@xTelegram.Daten)^);
    SetProdGrpInfoValues(mJob.GetMaAssign.Assigns, xMachID);
    // Der Texnet Event-Type wird in den Job Event-Type konvertiert
    case Pdeclaration(@xTelegram.Daten)^.event of
       eEVT_NONE:              xDecEvent  := deNone;
       eEVT_INITIAL_RESET:     xDecEvent  := deInitialReset;
       eEVT_RESET:             xDecEvent  := deReset;
       eEVT_ENTRY_LOCKED:      xDecEvent  := deEntryLocked;
       eEVT_ENTRY_UNLOCKED:    xDecEvent  := deEntryUnlocked;
       eEVT_MODIFIED_RANGE:    xDecEvent  := deSettings;
       eEVT_MODIFIED_SETTINGS: xDecEvent  := deSettings;
       eEVT_ADJUST:            xDecEvent  := deAdjust;
    else
      xDecEvent:= deNone;
    end;
    MoveMaAssigntoJob(xDecEvent, PDeclaration(@xTelegram.Daten)^, mJob.GetMaAssign.Assigns);
    CodeSite.SendMsg('End.GetMaAssign');
    xValidJob := TRUE;
  end;
  //.......................................................
  function ConvertToXMLSetting(aBinData: PByte; aBinDataSize: Integer; aJobTyp: TJobTyp; var aXMLData: String): Boolean;
  var
    xMapfile: DOMDocument40;
    xMapID: String;
    xSize: DWORD;
  begin
    Result   := False;
    xMapID   := mMsgPoolList.MapID[xMachID];
    xMapfile := mMsgPoolList.MapfileDOM[xMapID];
    if Assigned(xMapfile) then begin
      with TMMXMLConverter.Create do
      try
        // Wert wird von GetSettings Job in TXNWriter in Liste geschrieben
        IgnoreDeleteFlag := mMsgPoolList.ChangeAllXMLDataCounter(mJob^.JobID, False);
//        xVariant := mMsgPoolList.JobIDValue[xJobID, cJobValueAllXMLValues];
//        if xVariant <> '' then begin
//          IgnoreDeleteFlag := (xVariant > 0);
//          xVariant := xVariant - 1;
//          if xVariant > 0 then
//            mMsgPoolList.JobIDValue[xJobID, cJobValueAllXMLValues] := xVariant
//          else
//            mMsgPoolList.DeleteJobIDValue(xJobID);
//        end;
        // F�r die Settings muss nichts aus dem DOM herauskopiert werden, da das Resultat gleich dem g�ltigen XML ist
        mTempXMLString         := '';
        // ProdGrp Infos werden in mTempXMLSettings extrahiert
        OnCalcExternal         := BinToXMLEventHandler;
        OnBeforeDeleteElements := BeforeDeleteElementsEventHandler;
        try
          aXMLData := BinToXML(aBinData, aBinDataSize, xMapfile, msYMSetting, False);
//          aHash1   := Hashcode1;
//          aHash2   := Hashcode2;
          xSize    := GetJobHeaderSize(aJobTyp) + DWord(Length(aXMLData));
          // wenn die konvertierten Settings nicht Platz haben im Jobbuffer -> Buffer vergr�ssern
          if xSize > mJobSize then begin
            mJobSize := xSize;
            ReallocMem(mJob, mJobSize);
            FillChar(mJob^, mJobSize, 0);
          end;
          // JobHeader schon mal vordefinieren
          with mJob^ do begin
            // JobID muss nochmals gesetzt werden, da bei einem ReallocMem gel�scht wird
            JobTyp := aJobTyp;
            JobID  := xJobID;
            NetTyp := ntTXN;
          end;

          Result := TRUE;
        except
          on e:Exception do
            WriteLog(etError, 'TTXNReader.ProcessJob.ConvertToXMLSetting: ' + e.Message);
        end; // try BinToXML(
      finally
        pointer(xMapfile) := nil;  // verhindern dass Interface freigegeben wird
        Free;
      end; // with TMMXMLConverter.Create
    end; // if Assigned(xMapfile)
  end;
  //.......................................................
  procedure CopyClassData(aSrcData: PSpdTXNRec; var aMMData: TMMDataRec);
  var
    i: integer;
    xMemStream: TMemoryStream;
  begin
    xMemStream := TMemoryStream.Create;
    try
      try
        // convert Yarn Defect Data
        xMemStream.Write(aSrcData^.defectData.yarnDefect, cYarnDefectSize);
        xMemStream.Position := 0;
        for i:=1 to cClassCutFields do begin
          // Cut Werte sind fix in einem Byte-Array -> direktes umkopieren m�glich
          aMMData.ClassFieldRec.ClassCutField[i] := aSrcData^.defectData.cutYarnDef[i];
          // Defect Werte sind in einem variablen Datenblock und muss individuell ausgelesen werden
          xMemStream.Read(aMMData.ClassFieldRec.ClassUncutField[i], cTXNYarnDefXArray[i]);
        end;
        xMemStream.Clear;

        // convert the Splice Defect Data
        xMemStream.Write(aSrcData^.defectData.spliceDefect, cSpliceDefectSize);
        xMemStream.Position := 0;
        for i:=1 to cSpCutFields do begin
          // Cut Werte sind fix in einem Byte-Array -> direktes umkopieren m�glich
          aMMData.ClassFieldRec.SpCutField[i] := aSrcData^.defectData.cutSpliceDef[i];
          // Defect Werte sind in einem variablen Datenblock und muss individuell ausgelesen werden
          xMemStream.Read(aMMData.ClassFieldRec.SpUncutField[i], cTXNSpliceDefXArray[i]);
        end;
        xMemStream.Clear;

        // convert the SIRO Defect Data
        xMemStream.Write(aSrcData^.siroData, cSiroDefectSize);
        xMemStream.Position := 0;
        for i:=1 to cSIROCutFields do begin
          // Cut Werte sind fix in einem Byte-Array -> direktes umkopieren m�glich
          aMMData.ClassFieldRec.SIROCutField[i] := aSrcData^.siroData.cutSiroDef[i];
          // Defect Werte sind in einem variablen Datenblock und muss individuell ausgelesen werden
          xMemStream.Read(aMMData.ClassFieldRec.SIROUncutField[i], cTXNSIRODefXArray[i]);
        end;
      except
        on e: Exception do begin
          fError := SetError(ERROR_NOT_ENOUGH_MEMORY, etMMError, 'ConvertTXNDefectData failed. ' + e.Message);
          raise EMMException.Create(fError.Msg);
        end;
      end;
    finally
      xMemStream.Free;
    end;
  end;
  //.......................................................
  procedure CopyBaseData(aSrcData: PSpdTXNRec; var aMMData: TMMDataRec);
  begin
    // Hiermit werden auch die Klassierdatenfelder gel�scht
    FillChar(aMMData, sizeof(TMMDataRec), 0);
    // Put the data from the machine spindle record to the DB spindle record
    with aSrcData^.baseData do begin
      aMMData.Len  := length; // Nicht Laenge; sondern Anzahl Nutentrommel Impulse. Umrechung erfolgt im StorageHandler
      aMMData.tRun := runTime;
      aMMData.tOp  := operatingTime;
      aMMData.tWa  := watchTime;
      // In case of overflow take only the produced len and the times
      if status = 0 then begin // no overflow if status = 0
        aMMData.Bob          := cops;
        aMMData.Cones        := cones;
        aMMData.Sp           := splices;
        aMMData.RSp          := spliceRep;
        aMMData.YB           := yarnBreaks;
        aMMData.CSys         := systemCuts;
        aMMData.LckSys       := systemLocks;
        aMMData.CUpY         := upperYarnCuts;
        aMMData.CYTot        := totalYarnCuts;
        aMMData.LSt          := longStops;
        aMMData.tLStProd     := longStopTime; // momentan alle longstops in Prod
        aMMData.CS           := shortCuts;
        aMMData.CL           := longCuts;
        aMMData.CT           := thinCuts;
        aMMData.CN           := nepCuts;
        aMMData.CSp          := spliceCuts;
        aMMData.CClS         := shortClusterCuts; //clusterCuts;
        aMMData.COffCnt      := offCountCuts;
        aMMData.LckOffCnt    := offCountLocks;
        aMMData.CBu          := bunchCuts;
        aMMData.CDBu         := delBunchCuts;
        aMMData.UClS         := shortClustArea;
        aMMData.UClL         := longClustArea;
        aMMData.UClT         := thinClustArea;
        aMMData.CSIRO        := siroCuts;
        aMMData.LckSiro      := siroLocks;
        aMMData.CSIROCl      := siroClusterCuts;
        aMMData.LckSIROCl    := siroClusterLocks;
        aMMData.LckClS       := shortClusterLocks; //clusterLocks;
        aMMData.CSfi         := sfiCuts;
        aMMData.LckSfi       := sfiLocks;
        aMMData.SFICnt       := variCnt;
        aMMData.SFI          := variTotal;
        aMMData.AdjustBase   := AdjustBase; // Wss
        if AdjustBase > 0 then aMMData.AdjustBaseCnt := 1
                          else aMMData.AdjustBaseCnt := 0;
        // Mai 2005
        aMMData.CClL              := longClusterCuts;
        aMMData.CClT              := thinClusterCuts;
        aMMData.CShortOffCnt      := shortOffCountCuts;
        aMMData.LckClL            := longClusterLocks;
        aMMData.LckClT            := thinClusterLocks;
        aMMData.LckShortOffCnt    := shortOffCountLocks;
        // dem MMDataRecord nicht zugewiesen werden:
        // systemEvents, diaMeanValue, diaStrdDevi
      end;
    end; // width aSrcData^.baseData
  end;
  //.......................................................
begin
  if TxnDevHandler.TxnRead(xInBuf, xInBufLengh) then begin
    xMachID          := 0;
    xValidJob        := FALSE;
    FillChar(mTempXMLSettings, sizeof(mTempXMLSettings), 0);
    // Case of ..Pruefe ob Telegramm komplett
    case mMsgPoolList.AddBlock(xInBuf, xInBufLengh, xNodeID) of
      bsNotvalidblock: begin
        end;
      bsValidblock: begin
        end;
      bsTelegramComplete: begin
          FillChar(mJob^, mJobSize, 0);  // MJob mit 0 initialisieren
          xMachID := mMsgPoolList.GetMachID(xNodeID); // Wird nur fuer Declaration benoetigt
          if xMachID <> cNotValid then begin
            // Gueltige NodeID bzw. MachNo
            mMsgPoolList.GetTelegram(xNodeID, xTelegram.All, xTelgramsize);
            xJobID       := xTelegram.Header.JobID;
            mJob^.JobID  := xJobID;
            mJob^.NetTyp := ntTXN;
            xOrgJob      := TJobTyp(StrToIntDef(mMsgPoolList.JobIDValue[xJobID, cJobValueJobTyp], 0));
            case xTelegram.Header.telegramID of
              eEMPTY_TELEGRAM: begin
                end;
              // For MM not used
              eREQUEST_TELEGRAM: begin
                end;
              // For MM not used
              eEXPRESS_TELEGRAM: begin
                end;
              // Synchrone Meldungen als Antwort auf bestimmte Anfragen (angeforderte Meldungen vom TXNWriter)
              eCONFIRM_TELEGRAM: begin
                  // nun es ist eine Best�tigung, aber auf was? -> SubID
                  case xTelegram.Header.telegramSubID of
                    eDUMMY_DATA: begin
                      end;
                    eMONITOR_DATA: begin
                      end;              // Not used
                    eBASE_DATA,
                      eDEFECT_DATA,
                      eSIRO_DATA: begin
                        if cView in xTelegram.Header.flagRegister then begin
                          mJob.JobTyp := jtViewZeSpdData;
                        end else begin
                          mJob.JobTyp := jtGetZeSpdData;
                        end;
                        mJob.GetZeSpdData.SpindID := xTelegram.Header.spindleRange.firstSpindle;
//                        Move(xTelegram.Daten, mJob.GetZeSpdData.SpdDataArr, Sizeof(xTelegram.Daten));
                        // SpdData wird in dieser Methode genullt!!
                        CopyBaseData(PSpdTXNRec(@xTelegram.Daten), mJob^.GetZeSpdData.SpdData);
                        // initialize the class cut and uncut data
                        CopyClassData(PSpdTXNRec(@xTelegram.Daten), mJob^.GetZeSpdData.SpdData);
                        xValidJob := TRUE;
                      end;
                    eGROUP_DATA: begin
                      end;
                    //Not implemented

                    // Antwort auf Anfrage von GetSettings
                    eSETTINGS: begin
                        if xOrgJob = jtSaveMaConfigToDB then begin
                          // nach Auswerten von GetMaAssign und senden von GetSettings an TXNWriter
                          HandleSaveMaConfigToDB_Step2;
                        end
                        else begin
                          xValidJob := ConvertToXMLSetting(@xTelegram.Daten, Sizeof(xTelegram.Daten), jtGetSettings, xXMLData);
                          // Tempor�re Variable f�r AllXMLValues aus MsgList l�schen (wird f�r Assignment ben�tigt)
                          if xValidJob then begin
                            with mJob^.GetSettings, SettingsRec do begin
                              MachineID := xMachID;
                              // Gesammelte ProdGrp Infos vom Converter Event hineinkopieren
                              System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                              // die XMLSettings in Jobbuffer hineinkopieren
                              System.Move(PChar(xXMLData)^, XMLData, Length(xXMLData));
                            end; // with mJob^.GetSettings,
                          end; // if xValidJob
                        end; // if xOrgJob = jtSaveMaConfigToDB
                      end; // eSETTINGS
                    // Antwort auf das GetMaAssign vom TXNWriter
                    // Auch ausgel�st durch jtSaveMaConfigToDB f�r den Bauzustand zum weiterverarbeiten
                    eDECLARATIONS: begin
                        if xOrgJob = jtSaveMaConfigToDB then begin
                          HandleSaveMaConfigToDB_Step1;
                        end else begin
                          HandleGetMaAssign;
                        end;
                      end;
                  else
                  end;
                end;
              eMESSAGE_TELEGRAM: begin
                  mJob.JobID := cUnknownJobID;
                  case xTelegram.Header.telegramSubID of
                    // Initial Declaration ist immer im OS2 Format, daher muss auch mit OS2daten zugegriffen werden
                    OS2_eDECLARATIONS: begin
                        case POS2Declaration(@xTelegram.OS2Daten)^.event of
                          eEVT_INITIAL_RESET: begin
                              mJob.JobTyp := jtInitialReset;
                              mJob.InitialReset.MachineID := xMachID;
                              xValidJob := TRUE;
                            end
                        else
                        end;
                      end;
                    // Der Rest im NT Format
                    // Asynchrone Events werden hier behandelt (unaufgeforderte Meldungen)
                    eDECLARATIONS: begin
                        case Pdeclaration(@xTelegram.Daten)^.event of
                          eEVT_INITIAL_RESET: begin
                              mJob.JobTyp := jtInitialReset;
                              mJob.InitialReset.MachineID := xMachID;
                              xValidJob := TRUE;
                            end;
                          eEVT_RESET: begin
                              mJob.JobTyp := jtReset;
                              mJob.Reset.MachineID := xMachID;
                              xValidJob := TRUE;
                            end;
                          eEVT_ENTRY_LOCKED: begin
                              // Zusaetzlich AssignComplete Meldung mit akt. MachAssign
                              { DONE -okhp -cSystem : Wenn laenger als eine Minute kein Assign erfolgt wird
                                automatsch ein Assigncomlete gesendet. }
                              if mMsgPoolList.AssignTimeoutPending(xNodeID) then begin
                                mMsgPoolList.AssignTimeoutStop(xNodeID);
                                mJob.JobTyp := jtAssignComplete;
                                CodeSite.SendMsg('Beg.ENTRY_LOCKED');
                                mJob.AssignComplete.MachineID := xMachID;
                                SetProdGrpInfoValues(mJob.AssignComplete.Assigns, xMachID);
                                MoveMaAssigntoJob(deEntryLocked, mMsgPoolList.GetlastDeclEvent(xNodeID), mJob.Assign.Assigns);
                                WriteJobBufferTo(ttMsgDispatcher);
                                CodeSite.SendMsg('End.ENTRY_LOCKED');
                                mMsgPoolList.ClearLastDeclEvent(xNodeID);
                              end;
                              mJob.JobTyp := jtEntryLocked;
                              mJob.EntryLocked.MachineID := xMachID;
                              xValidJob := TRUE;
                            end;
                          eEVT_ENTRY_UNLOCKED: begin
                              mJob.JobTyp := jtEntryUnlocked;
                              mJob.EntryUnlocked.MachineID := xMachID;
                              xValidJob := TRUE;
                            end;
                          eEVT_MODIFIED_RANGE: begin
                              mMsgPoolList.AssignTimeoutBegin(xNodeID); // Timeout fuer Assign starten
                              mJob.JobTyp := jtAssign;
                              CodeSite.SendMsg('Beg.MODIFIED_RANGE');
                              mJob.Assign.MachineID := xMachID;
                              mMsgPoolList.UpdateLastDecl(xNodeID, PDeclaration(@xTelegram.Daten)^);
                              MoveMaAssigntoJob(deRange, PDeclaration(@xTelegram.Daten)^, mJob.Assign.Assigns);
                              CodeSite.SendMsg('End.MODIFIED_RANGE');
                              xValidJob := TRUE;
                            end;
                          eEVT_MODIFIED_SETTINGS: begin
                              mMsgPoolList.AssignTimeoutBegin(xNodeID); // Timeout fuer Assign starten
                              mJob.JobTyp := jtAssign;
                              CodeSite.SendMsg('Beg.MODIFIED_SETTINGS');
                              mJob.Assign.MachineID := xMachID;
                              mMsgPoolList.UpdateLastDecl(xNodeID, PDeclaration(@xTelegram.Daten)^);
                              MoveMaAssigntoJob(deSettings, PDeclaration(@xTelegram.Daten)^, mJob.Assign.Assigns);
                              CodeSite.SendMsg('End.MODIFIED_SETTINGS');
                              xValidJob := TRUE;
                            end;
                          eEVT_ADJUST: begin
                              mJob.JobTyp := jtAdjust;
                              mJob.Adjust.MachineID := xMachID;
                              xValidJob := TRUE;
                            end;
                        else
                          mJob.JobTyp := jtNone;
                        end;
                      end;
                  end;
                end;
              // Acknowlege nach SetSettings uebermittelt neu die bereinigten Einstellungen
              // welche als Array uebermittelt werden.
              eACKNOWLEDGE: begin
                  // Richtige Settings oder ProdID wurde downloaded -> korrigierte Settings konvertieren und weiterleiten
                  xValidJob := ConvertToXMLSetting(@xTelegram.Daten, Sizeof(xTelegram.Daten), jtSetSettings, xXMLData);
                  if xValidJob then begin
                    with mJob^.SetSettings, SettingsRec do begin
                      MachineID := xMachID;

                      // Gesammelte ProdGrp Infos vom Converter Event hineinkopieren
                      System.Move(mTempXMLSettings, SettingsRec, sizeof(mTempXMLSettings));
                      Color           := cDefaultStyleIDColor;
                      OrderPositionID := cDefaultOrderPositionID;
                      OrderID         := cDefaultOrderID;
                      // die XMLSettings in Jobbuffer hineinkopieren
                      System.Move(PChar(xXMLData)^, XMLData, Length(xXMLData));
                    end;
 codesite.SendFmtMsg('TTXNReader.ProcessJob eACKNOWLEDGE: Vor senden an MsgDispatcher mJob^.SetSettings.SettingsRec.Group=%d', [mJob^.SetSettings.SettingsRec.Group]);
                  end;
                end; // eACKNOWLEDGE
            else
            end; // case xTelegram.Header.telegramID
          end else // if xMachID <> cNotValid
            // Meldung: keine MachNo zu NodeID vorhanden.
            CodeSite.SendError(Format('Invalid MachID: %d  NodeID: %d', [xMachID, xNodeID]));

          if xValidJob then begin
            WriteJobBufferTo(ttMsgDispatcher);
            case mJob.JobTyp of
              jtGetZeReady:     WriteLogDebug(Format('GetZeReady MachID %d ', [xMachID]), nil, 0, TRUE);
              jtGetSettings:    WriteLogDebug(Format('GetSettings MachID %d ', [xMachID]), nil, 0, TRUE);
              jtGetMaAssign:    WriteLogDebug(Format('GetMaAssign MachID %d ', [xMachID]), nil, 0, TRUE);
              jtInitialReset:   WriteLogDebug(Format('InitialReset MachID %d ', [xMachID]), nil, 0, TRUE);
              jtReset:          WriteLogDebug(Format('Reset MachID %d ', [xMachID]), nil, 0, TRUE);
              jtEntryLocked:    WriteLogDebug(Format('EntryLocked MachID %d ', [xMachID]), nil, 0, TRUE);
              jtEntryUnlocked:  WriteLogDebug(Format('EntryUnlocked MachID %d ', [xMachID]), nil, 0, TRUE);
              jtAssignComplete: WriteLogDebug(Format('AssignComplete MachID %d ', [xMachID]), nil, 0, TRUE);
              jtAssign:         WriteLogDebug(Format('Assign MachID %d ', [xMachID]), nil, 0, TRUE);
              jtAdjust:         WriteLogDebug(Format('Adjust MachID %d ', [xMachID]), nil, 0, TRUE);
              jtNone:           WriteLogDebug(Format('None MachID %d ', [xMachID]), nil, 0, TRUE);
            else
            end;
          end; // if xValidJob
        end; // bsTelegramComplete
    else
    end; // case
  end else begin
    if not Terminated then begin
      // Error vom TexnetDriver hochziehen und ab ins Log falls nicht Timeout
      fError := TxnDevHandler.ErrorRec;
      if fError.ErrorTyp <> etNoError then begin
        if mErrorCount < cMaxErrorCount then begin
          INC(mErrorCount);
          WriteLog(etError, FormatMMErrorText(Error));
        end;
      end else begin                    // Ruecksetzen nach jedem TimeOut
        mErrorCount := 0;
      end;
    end;
  end;
end;

//-----------------------------------------------------------------------------
end.

