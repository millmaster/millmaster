(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: TXNHandlerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: T0  TXN_Mainprozess
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 14.07.1998  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
| 15.11.1999  2.01t khp | Zugriffsroutinen fuer Setup mittels BaseSetup und mmSetupModul
| 20.03.2000  2.05  khp | constructor Create new  reintroduce;
| 04.10.2000  2.05  Wss | Initialize: Read problem from Registry -> changed to Access := KEY_READ
| 11.04.2001  2.06  Nue | TBaseSettingsReader ==> TMMSettingsReader
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
|                       | TTXNHandler.Initialize sucht zuerst nach PCI und wenn negativ nach ISA Texnetkarte
| 08.08.2002  3.01  khp | In TTXNHandler.Initialize werden PCI Karten ab pcislot 0 gesucht,fr�her ab 1
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 25.11.2002  3.12  khp | Differenzierte Fehlermeldungen
| 02.03.2004  3.12  khp | Unterst�tzung von mehreren PCI-BusControllern pro PC
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|==========================================================================================*)

unit TXNHandlerClass;

interface

uses
  Classes, mmRegistry, Windows, mmThread,
  LoepfeGlobal, BaseMain, BaseGlobal, MMEventLog,
  TXN_glbDef, TXNWriterClass, TXNControlClass, TXNReaderClass, TXNPoolClass,
  TXN_PCI_DevClass, TXN_PCI_Def, SettingsReader;


type

  TTXNHandler = class(TBaseMain)
  PRIVATE
  PROTECTED
    mMsgPoolList: TMsgPoolList;
    mPCIDEVHandlerArr: TPCIDEVHandlerArr;
    mDevMode: eDevModeT;
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; OVERRIDE;
    function getParamHandle: TMMSettingsReader; OVERRIDE;
  PUBLIC
    constructor Create(aSubSystem: TSubSystemTyp); OVERRIDE;
    destructor Destroy; OVERRIDE;
    function Initialize: Boolean; OVERRIDE;
  end;

  //==============================================================================

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  SysUtils;

{TTXNHandler Class}

constructor TTXNHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
  mDevMode := eNone;
  mMsgPoolList := TMsgPoolList.Create;
  mMsgPoolList.InitMsgPoolList;
end;

//-----------------------------------------------------------------------------

destructor TTXNHandler.Destroy;
var
  xAdapterNo: Integer;
begin
  if mDevMode = ePCI then
    for xAdapterNo := 1 to cMaxTxnAdapter do
      if mPCIDEVHandlerArr[xAdapterNo] <> nil then
        mPCIDEVHandlerArr[xAdapterNo].Free;
  mMsgPoolList.Free;
  inherited Destroy;
end;

//-----------------------------------------------------------------------------

function TTXNHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttTXNWriter: begin
        aThread := TTXNWriter.Create(mSubSystemDef.Threads[1], mMsgPoolList, mPCIDEVHandlerArr, mDevMode);
      end;
    ttTXNControl: begin
        aThread := TTXNControl.Create(mSubSystemDef.Threads[2], mMsgPoolList, mPCIDEVHandlerArr, mDevMode);
      end;
    ttTXNRead1: begin
        aThread := TTXNReader.Create(mSubSystemDef.Threads[3], mMsgPoolList, 1, mPCIDEVHandlerArr[1], mDevMode);
      end;
    ttTXNRead2: begin
        aThread := TTXNReader.Create(mSubSystemDef.Threads[4], mMsgPoolList, 2, mPCIDEVHandlerArr[2], mDevMode);
      end;
    ttTXNRead3: begin
        aThread := TTXNReader.Create(mSubSystemDef.Threads[5], mMsgPoolList, 3, mPCIDEVHandlerArr[3], mDevMode);
      end;
  else
    aThread := nil;
    Result := False;
  end;
end;

//-----------------------------------------------------------------------------

function TTXNHandler.Initialize: Boolean;
var
  //xAdapterNo,
  xCount: SmallInt;
  xBinValue: array[0..512] of byte;
  xlastPCISlot, xpcislot: Integer;
  xPlxDevice: DEVICE_LOCATION;
  xTotalAvailablePCITexnetNAD: Integer;
  xTotalfoundPCITexnetNAD: Integer;
  xbusNumber, xAdapterNo: Integer;
  xPerBusAvailablePCITexnetNAD: Integer;
  xPerBusfoundPCITexnetNAD: Integer;
  xOutputMsgstring: String;
  xFileVersion: string;
begin
  Result       := False;
  xlastPCISlot := 0;
  // letzter Texnet PCI-Slot aus Registry lesen, nur bei NT !
  with TmmRegistry.Create do
  try
    RootKey := HKEY_LOCAL_MACHINE;
    if OpenkeyReadonly(cPCITexnetRegPath) then begin
      if ValueExists(cPCITexnetValue) then begin
        readbinarydata(cPCITexnetValue, xBinValue, sizeof(xBinValue));
        xlastPCISlot := xBinValue[12]
      end else  // Bei Windows 2000 ist kein Eintrag vorhanden
        xlastPCISlot := clastPCISlot;
      CloseKey;
    end;
  finally
    Free;
  end;

  try
    xOutputMsgstring := '';
    // Init & Clear  mPCIDEVHandlerArr
    for xAdapterNo := 1 to cMaxTxnAdapter do begin
      mPCIDEVHandlerArr[xAdapterNo] := nil;
    end;
    xPlxDevice.SlotNumber:= MINUS_ONE_LONG;  // alle
    xPlxDevice.BusNumber:= MINUS_ONE_LONG;   // alle
    xTotalAvailablePCITexnetNAD:= Amount_PCITexnetNAD_MATCHED(xPlxDevice);  // wieviele PCITExnetadapter pro PC
    if xTotalAvailablePCITexnetNAD > 0 then begin
      xAdapterNo:= 1;  // Dev_loc_Table Array beginnt bei 1
      xbusNumber:= 0;
      xTotalfoundPCITexnetNAD:= 0;
      while xTotalfoundPCITexnetNAD < xTotalAvailablePCITexnetNAD do begin
        //Dev_loc_Table[xAdapterNo].BusNumber := xbusNumber;
        xPlxDevice.SlotNumber:= MINUS_ONE_LONG; // alle
        xPlxDevice.BusNumber:= xbusNumber;
        xPerBusAvailablePCITexnetNAD:= Amount_PCITexnetNAD_MATCHED(xPlxDevice); // wieviele PCITExnetadapterpro PCI-BUS
        if xPerBusAvailablePCITexnetNAD = -1 then begin
          mDevMode := eNone;
          WriteLog(etError, 'PCIHandler Initialize failed: Exception in PLXAPI.DLL function: PlxPciDeviceFind');
          break; // Abruch bei Exception
        end;
        xPerBusfoundPCITexnetNAD:= 0;
        xpcislot:= 0;
        While (xPerBusfoundPCITexnetNAD < xPerBusAvailablePCITexnetNAD) and
              (xPerBusfoundPCITexnetNAD < xlastPCISlot) do begin
          xPlxDevice.SlotNumber:= xpcislot;
          xPlxDevice.BusNumber:= xbusNumber;
          if GetPCITexnetDeviceInfo(xPlxDevice) then begin
            mDevMode := ePCI;
            mPCIDEVHandlerArr[xAdapterNo] := TPCITxnDevHandler.Create();
            mPCIDEVHandlerArr[xAdapterNo].PCIConnect(xPlxDevice);
            ThreadCount := cMinTxnThread + xAdapterNo;
            mMsgPoolList.AdapterCount := xAdapterNo;
            INC(xPerBusfoundPCITexnetNAD);
            INC(xTotalfoundPCITexnetNAD);
            INC(xAdapterNo);
            xOutputMsgstring := Format('%s, Adapter:%d Slot:%d Bus:%d', [xOutputMsgstring, xAdapterNo-1, xpcislot, xbusNumber]);
          end;
          INC(xpcislot);
        end; // while
        INC(xbusNumber);
      end; // while
      if mDevMode = ePCI then begin
        Delete(xOutputMsgstring, 1, 1); // erstes Komma entfernen
        xFileVersion := 'unknown';
        try
          xFileVersion := GetFileVersion(ParamStr(0));
        except
          // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
        end;
        WriteLog(etInformation, Format('PCI_Texnetadapter(s) found and initialized (Handler Version %s):%s', [xFileVersion, xOutputMsgstring]));
      end;
    end;
  (* try
   (* if xlastPCISlot > 0 then begin
      // Init & Clear  mPCIDEVHandlerArr
      for xAdapterNo := 1 to cMaxTxnAdapter do begin
        mPCIDEVHandlerArr[xAdapterNo] := nil;
      end;
      // Suchen aller PCI Karten ab Slot 0, abfuellen der SlotID's und Create der PCITxnDevHandler pro Adapter
      xAdapterNo := 1;
      for xpcislot := 0 to xlastPCISlot do begin
        xDevInfo.SlotNumber := xpcislot;
        if FindPCITexnetNAD(xDevInfo) then begin
          mDevMode := ePCI;
          mPCIDEVHandlerArr[xAdapterNo] := TPCITxnDevHandler.Create();
          mPCIDEVHandlerArr[xAdapterNo].PCIConnect(xDevInfo);
          ThreadCount := cMinTxnThread + xAdapterNo;
          mMsgPoolList.AdapterCount := xAdapterNo;
          INC(xAdapterNo);
          if xAdapterNo > cMaxTxnAdapter then Break;
          //if xAdapterNo > 2 then Break;
        end;
      end; {for}
      if mDevMode = ePCI then WriteLog(etInformation, Format('%d PCI_Texnetadapter found and Initialized', [xAdapterNo - 1]));
    end;  *)
  except
    mDevMode := eNone;
    WriteLog(etError, 'PCIHandler Initialize failed');
  end;

  if mDevMode = eNone then begin
    xCount := 0;
    // Anzahl der ISA Treiber aus Registry lesen
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      Access := KEY_READ;
      for xAdapterNo := 1 to cMaxTxnAdapter do begin
        if (KeyExists(cTexnetRegPath + cTexnetRegName[xAdapterNo])) then begin
          xCount := xAdapterNo;
          mDevMode := eISA;
        end
        else begin
          Break;
        end;
      end;
      if mDevMode = eISA then begin     // Mind. einen Texnet Adapter gefunden
        ThreadCount := cMinTxnThread + xCount;
        mMsgPoolList.AdapterCount := xCount;
        WriteLog(etInformation, Format('%d ISA_Texnetadapter found in registry', [xCount]));
      end;
    finally
      Free;
    end; // with TmmRegistry.Create
  end;
  if (mDevMode = eISA) or (mDevMode = ePCI) then begin
    Result := inherited Initialize;
    if not Result then WriteLog(etError, 'inherited Initialize failed');
  end else
    WriteLog(etError, 'Texnetdriver is not started/installed or wrong/no entry in registry')
end;

function TTXNHandler.getParamHandle: TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;

//-----------------------------------------------------------------------------
end.

