(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|--------------------------------------------------------------------------------------------
| Filename......: PLXAPIDLL_DEF.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: Interface zu PLXAPI.DLL
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 450, Windows NT 4.0 Sp6a
| Target.system.: NT
| Compiler/Tools: Delphi 5.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 29.05.2001  0.00  khp | Inital Release
|==========================================================================================*)
unit PLXAPIDLL_DEF;

interface

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
