#ifndef __PCIAPI_H
#define __PCIAPI_H

/*******************************************************************************
 * Copyright (c) 2000 PLX Technology, Inc.
 * 
 * PLX Technology Inc. licenses this software under specific terms and
 * conditions.  Use of any of the software or derviatives thereof in any
 * product without a PLX Technology chip is strictly prohibited. 
 * 
 * PLX Technology, Inc. provides this software AS IS, WITHOUT ANY WARRANTY,
 * EXPRESS OR IMPLIED, INCLUDING, WITHOUT LIMITATION, ANY WARRANTY OF
 * MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE.  PLX makes no guarantee
 * or representations regarding the use of, or the results of the use of,
 * the software and documentation in terms of correctness, accuracy,
 * reliability, currentness, or otherwise; and you rely on the software,
 * documentation and results solely at your own risk.
 * 
 * IN NO EVENT SHALL PLX BE LIABLE FOR ANY LOSS OF USE, LOSS OF BUSINESS,
 * LOSS OF PROFITS, INDIRECT, INCIDENTAL, SPECIAL OR CONSEQUENTIAL DAMAGES
 * OF ANY KIND.  IN NO EVENT SHALL PLX'S TOTAL LIABILITY EXCEED THE SUM
 * PAID TO PLX FOR THE PRODUCT LICENSED HEREUNDER.
 * 
 ******************************************************************************/

/******************************************************************************
 *
 * File Name:
 *
 *     PciApi.h
 *
 * Description:
 *
 *     This file contains all the PCI API function prototypes.
 *
 * Revision:
 *
 *     05-10-00 : PCI Host SDK v3.10
 *
 ******************************************************************************/


#include <wtypes.h>
#include "PlxError.h"
#include "PlxTypes.h"


#ifdef __cplusplus
extern "C" {
#endif


// DLL support
#ifndef EXPORT
    #define EXPORT __declspec(dllexport)
#endif



/******************************************
* Miscellaneous Functions
******************************************/
RETURN_CODE EXPORT
PlxSdkVersion(
    U8 *VersionMajor,
    U8 *VersionMinor,
    U8 *VersionRevision
    );

RETURN_CODE EXPORT
PlxPciCommonBufferGet(
    IN HANDLE drvHandle,
    OUT PPCI_MEMORY pMemoryInfo
    );


/******************************************
* PLX Device Management Functions
******************************************/
RETURN_CODE EXPORT
PlxPciDeviceOpen(
    IN DEVICE_LOCATION *pDevice,
    OUT HANDLE         *pDrvHandle
    );

RETURN_CODE EXPORT
PlxPciDeviceClose(
    IN HANDLE drvHandle
    );

RETURN_CODE EXPORT
PlxPciDeviceFind(
    IN PDEVICE_LOCATION device,
    IN OUT PU32 requestLimit
    );

RETURN_CODE EXPORT 
PlxPciBusSearch(
    IN OUT PDEVICE_LOCATION pDevData
    );

RETURN_CODE EXPORT
PlxPciBaseAddressesGet(
    IN HANDLE drvHandle,
    OUT PVIRTUAL_ADDRESSES virtAddr
    );

RETURN_CODE EXPORT
PlxPciBarRangeGet(
    IN HANDLE drvHandle,
    IN U32 barRegisterNumber,
    OUT PU32 data
    );

    
/******************************************
* Register Access Functions
******************************************/
U32 EXPORT
PlxPciConfigRegisterRead(
    IN U32 bus,
    IN U32 slot,
    IN U32 registerNumber,
    OUT PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxPciConfigRegisterWrite(
    IN U32 bus,
    IN U32 slot,
    IN U32 registerNumber,
    IN PU32 data
    );

RETURN_CODE EXPORT
PlxPciConfigRegisterReadAll(
    IN U32 bus,
    IN U32 slot,
    OUT PU32 buffer
    );

U32 EXPORT
PlxRegisterRead(
    IN HANDLE drvHandle,
    IN U32 registerOffset,
    OUT PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxRegisterWrite(
    IN HANDLE drvHandle,
    IN U32 registerOffset,
    IN U32 data
    );

RETURN_CODE EXPORT
PlxRegisterReadAll(
    IN HANDLE drvHandle,
    IN U32 startOffset,
    IN U32 registerCount,
    OUT PU32 buffer
    );

U32 EXPORT
PlxRegisterMailboxRead(
    IN HANDLE drvHandle,
    IN MAILBOX_ID mailboxId,
    OUT PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxRegisterMailboxWrite(
    IN HANDLE drvHandle,
    IN MAILBOX_ID mailboxId,
    IN U32 data
    );

U32 EXPORT 
PlxRegisterDoorbellRead(
    IN HANDLE pDeviceExtension,
    OUT PRETURN_CODE pReturnCode
    );
        
RETURN_CODE EXPORT
PlxRegisterDoorbellSet(
    IN HANDLE drvHandle,
    IN U32 data
    );


/******************************************
* Interrupt Support Functions
******************************************/
RETURN_CODE EXPORT
PlxIntrEnable(
    IN HANDLE    drvHandle,
    IN PLX_INTR *plxIntr
    );

RETURN_CODE EXPORT
PlxIntrDisable(
    IN HANDLE    drvHandle,
    IN PLX_INTR *plxIntr
    );

RETURN_CODE EXPORT
PlxIntrAttach(
    IN HANDLE    drvHandle,
    IN PLX_INTR  intrTypes, 
    OUT HANDLE  *pEventHdl
    );

RETURN_CODE EXPORT
PlxIntrStatusGet(
    IN HANDLE     drvHandle,
    OUT PLX_INTR *plxIntr
    );

U32 EXPORT
PlxPciAbortAddrRead(
    IN HANDLE        drvHandle,
    OUT RETURN_CODE *pReturnCode
    );


/******************************************
* Bus Memory and I/O Functions
******************************************/
RETURN_CODE EXPORT
PlxBusIopRead(
    IN HANDLE drvHandle,
    IN IOP_SPACE iopSpace,
    IN U32 address,
    IN BOOLEAN remapAddress,
    OUT PU32 destination,
    IN U32 transferSize,
    IN ACCESS_TYPE accessType
    );

RETURN_CODE EXPORT
PlxBusIopWrite(
    IN HANDLE drvHandle,
    IN IOP_SPACE iopSpace,
    IN U32 address,
    IN BOOLEAN remapAddress,
    IN PU32 source,
    IN U32 transferSize,
    IN ACCESS_TYPE accessType
    );

RETURN_CODE EXPORT 
PlxIoPortRead(
    IN HANDLE drvHandle,
    IN U32 address,
    IN ACCESS_TYPE bits,
    OUT PVOID pOutData
    );

RETURN_CODE EXPORT
PlxIoPortWrite(
    IN HANDLE drvHandle,
    IN U32 address,
    IN ACCESS_TYPE bits,
    IN PVOID pValue
    );


/******************************************
* Serial EEPROM Access Functions
******************************************/
BOOLEAN EXPORT 
PlxSerialEepromPresent(
    IN HANDLE drvHandle,
    PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxSerialEepromRead(
    IN HANDLE drvHandle,
    IN EEPROM_TYPE eepromType,
    OUT PU32 buffer,
    IN U32 size
    );

RETURN_CODE EXPORT
PlxSerialEepromWrite(
    IN HANDLE drvHandle,
    IN EEPROM_TYPE eepromType,
    IN PU32 buffer,
    IN U32 size
    );


/******************************************
* DMA Functions
******************************************/
RETURN_CODE EXPORT
PlxDmaControl(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel, 
    IN DMA_COMMAND dmaCommand
    );

RETURN_CODE EXPORT
PlxDmaStatus(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel
    );


/******************************************
* Block DMA Functions
******************************************/
RETURN_CODE EXPORT
PlxDmaBlockChannelOpen(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel, 
    IN PDMA_CHANNEL_DESC dmaChannelDesc
    );

RETURN_CODE EXPORT
PlxDmaBlockTransfer(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel,
    IN PDMA_TRANSFER_ELEMENT dmaData,
    IN BOOLEAN returnImmediate
    );

RETURN_CODE EXPORT
PlxDmaBlockTransferRestart(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel,
    IN U32 transferSize,
    IN BOOLEAN returnImmediate
    );

RETURN_CODE EXPORT 
PlxDmaBlockChannelClose(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel
    );


/******************************************
* SGL DMA Functions
******************************************/
RETURN_CODE EXPORT
PlxDmaSglChannelOpen(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel, 
    IN PDMA_CHANNEL_DESC dmaChannelDesc
    );

RETURN_CODE EXPORT
PlxDmaSglTransfer(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel,
    IN PDMA_TRANSFER_ELEMENT dmaData,
    IN BOOLEAN returnImmediate
    );

RETURN_CODE EXPORT 
PlxDmaSglChannelClose(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel
    );


/******************************************
* Shuttle DMA Functions
******************************************/
RETURN_CODE EXPORT
PlxDmaShuttleChannelOpen(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel, 
    IN PDMA_CHANNEL_DESC dmaChannelDesc
    );

RETURN_CODE EXPORT
PlxDmaShuttleTransfer(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel,
    IN PDMA_TRANSFER_ELEMENT dmaData
    );

RETURN_CODE EXPORT 
PlxDmaShuttleChannelClose(
    IN HANDLE drvHandle,
    IN DMA_CHANNEL dmaChannel
    );


/******************************************
* Messaging Unit Functions
******************************************/
RETURN_CODE EXPORT
PlxMuInboundPortRead(
    IN HANDLE drvHandle,
    IN OUT PU32 framePointer
    );

RETURN_CODE EXPORT
PlxMuInboundPortWrite(
    IN HANDLE drvHandle,
    IN PU32 framePointer
    );

RETURN_CODE EXPORT
PlxMuOutboundPortRead(
    IN HANDLE drvHandle,
    IN OUT PU32 framePointer
    );

RETURN_CODE EXPORT
PlxMuOutboundPortWrite(
    IN HANDLE drvHandle,
    IN PU32 framePointer
    );

U32 EXPORT
PlxMuHostOutboundIndexRead(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxMuHostOutboundIndexWrite(
    IN HANDLE drvHandle,
    IN U32 indexValue
    );


/******************************************
* Power Management Functions
******************************************/
PLX_POWER_LEVEL EXPORT
PlxPowerLevelGet(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE returnCode
    );

RETURN_CODE EXPORT
PlxPowerLevelSet(
    IN HANDLE drvHandle,
    IN PLX_POWER_LEVEL plxPowerLevel
    );

U8 EXPORT
PlxPmIdRead(
    IN HANDLE drvHandle,
    IN PRETURN_CODE pReturnCode
    );

U8 EXPORT 
PlxPmNcpRead(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE pReturnCode
    );


/******************************************
* Hot Swap Functions
******************************************/
U8 EXPORT
PlxHotSwapIdRead(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE pReturnCode
    );

U8 EXPORT
PlxHotSwapNcpRead(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE pReturnCode
    );

U8 EXPORT 
PlxHotSwapStatus(
    IN HANDLE drvHandle,
    OUT PRETURN_CODE pReturnCode
    );


/******************************************
* VPD Functions
******************************************/
U8 EXPORT
PlxVpdIdRead(
    IN HANDLE drvHandle,
    IN PRETURN_CODE pReturnCode
    );

U8 EXPORT
PlxVpdNcpRead(
    IN HANDLE drvHandle,
    IN PRETURN_CODE pReturnCode
    );

U32 EXPORT 
PlxVpdRead(
    IN HANDLE drvHandle,
    IN U32 offset,
    OUT PRETURN_CODE pReturnCode
    );

RETURN_CODE EXPORT
PlxVpdWrite(
    IN HANDLE drvHandle,
    IN U32 offset,
    IN U32 vpdData
    );


/******************************************
* USER Pins Fuctions
******************************************/
PIN_STATE EXPORT
PlxUserRead(
    IN HANDLE drvHandle,
    IN USER_PIN userPin,
    OUT PRETURN_CODE returnCode
    );

RETURN_CODE EXPORT
PlxUserWrite(
    IN HANDLE drvHandle,
    IN USER_PIN userPin,
    IN PIN_STATE pinState
    );



#ifdef __cplusplus
}
#endif

#endif
