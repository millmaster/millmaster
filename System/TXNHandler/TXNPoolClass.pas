(*/==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: TXNPoolClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: P1,P2 mit allem Zugriffsroutinen
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 4.02
|------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|------------------------------------------------------------------------------------------
| 12.01.1999  0.00  khp | Datei erstellt
| 12.01.1999  0.00  khp | Inital Release
| 23.03.1999  1.0t  khp | Erste TestVersion fuer alte ZE
| 08.04.1999  2.0t  khp | Testversion fuer neue ZE Version 7.59e
| 14.04 1999  2.01  khp | Neu wird Class CriticalSection eingesetzt
| 16.04 1999  2.01  khp | In TxnWrite und TxnDeviceControl 2. Versuch nach 50 msec
| 29.04.1999  2.02  khp | AddBlock memStream.writeBuffer -> Blocklengh - (sizeof ( TheaderRec))
| 12.07.1999  2.02  khp | Driver Timeout, Device not ready'Driver Timeout,read or write command
|                       | (wait for Device response) fuehren zu keiner Fehlermeldung),
|  3.11.1999  2.03  khp | AssigTimeout in MsgPoolList mit functionen  AssignTimeoutExpired,
|                       | AssignTimeoutBegin, AssignTimeoutStop, AssignTimeoutPending
| 08.05.2000  2.04  khp | In MsgPoolRec LastDeclEvent mit den Methoden UpdateLastDecl,GetlastDeclEvent,
|                       | ClearLastDeclEvent eingefuegt. In InitMsgPoolList,ClearMsgPool wird die
|                       | Declaration auf 0 gesetzt.
| 01.10.2001  3.0   khp | Unterstuetzung PCI Texnet-Adapter
| 05.11.2002  3.05  khp | Bei Msg EVT_ENTRY_LOCKED wird in mMsgPoolList.GetlastDeclEvent das modify Flag in allen Gruppen �bernommen.
|  Juni 2002  3.05  Wss | Erweiterungen f�r XML Handling
| 24.01.2005  3.05  Wss | SetNodeList: es wird der Mapname verwendet welcher fix unter ID=2 vorhanden ist
| 28.04.2005  3.05  Wss | SetNodeList: GetDefaultMapname sucht nun das neuste ZE-02xx Mapfile 
| 28.06.2005  3.05  Wss | Msg bei Blocktimeout mit Zeit erg�nzt
| 25.11.2005        Lok | Warnings und Hints entfernt
|=========================================================================================*)

unit TXNPoolClass;

interface

uses
  windows, Classes, SysUtils,
  IpcClass, TXN_glbDef, Baseglobal, TXN_ISA_DevClass, BaseThread, mmStringList, MSXML2_TLB,
  syncobjs; // MM System units

type
  //...............................................................
  TMsgPoolRec = record
    netNodeRec: TTxnNetNodeRec;         // Definition im BaseInclude.INC
    blockNoCount: Byte;                 // Anzahl Bloecke des Telegramms
    blockToutCount: Byte;               // Timeoutzeit zwischen zwei Bloecken
    nextBlockNo: Byte;                  // Naechste gueltige BlockNo
    assign: Byte;                       // Timeout nach Assign "Auto-KeyLock"
    LastDeclEvent: Tdeclaration;        // Letzte Declaration vor Assign Complete
    memStream: TMemoryStream;           // Stream in welcher die Bloecke gespeichert werden
  end;
  TMsgPool = array[cMinTxnNodeID..cMaxTxnNodeID] of TMsgPoolRec;
  //...............................................................
  TMsgPoolList = class(TCriticalSection)
  private
    fAdapterCount: Integer;
    fErrorRec: TErrorRec;
    mJobIDValues: TmmStringList;
    mJobIDValuesSync: TMultiReadExclusiveWriteSynchronizer;
    mMapfileSync: TMultiReadExclusiveWriteSynchronizer;
    mMapfileList: TStringList;
    function ClearMsgPoolRec(aNodeID: TNodeID): Boolean;
    function GetJobIDValue(aJobID: DWord; aName: String): Variant;
    function GetMachState(aNodeID: TNodeID): TTxnNodeState;
    function GetMapfileDOM(aMapID: String): DOMDocument40;
    function GetMapID(aMachID: Integer): String;
    procedure ReadMapfilesFromDB(aMapnames: String);
    function GetDefaultMapname: string;
    procedure SetJobIDValue(aJobID: DWord; aName: String; const aValue: Variant);
    procedure SetMachState(aNodeID: TNodeID; aNodeState: TTxnNodeState);
  public
    mMsgPoolList: TMsgPool;
    constructor Create;
    destructor Destroy; override;
    function AddBlock(aBlock: TdatablockRec; aBlocklengh: Integer; out aNodeID: TNodeID): TBlockStatus;
    procedure AssignTimeoutBegin(aNodeID: TNodeID);
    function AssignTimeoutExpired(aNodeID: TNodeID): Boolean;
    function AssignTimeoutPending(aNodeID: TNodeID): Boolean;
    procedure AssignTimeoutStop(aNodeID: TNodeID);
    function BlockTimeout: Boolean;
    function ChangeAllXMLDataCounter(aJobID: Integer; aIncrement: Boolean): Boolean;
    procedure ClearLastDeclEvent(aNodeID: TNodeID);
    function ClearMsgPool: Boolean;
    procedure DeleteJobIDValue(aJobID: DWord);
    function GetlastDeclEvent(aNodeID: TNodeID): Tdeclaration;
    function GetMachID(aNodeID: TNodeID): Integer;
    function GetNodeID(aMachID: Integer): Integer;
    procedure GetNodeList(var aNodeList: TTxnNetNodeList);
    function GetNodeRecord(aMachID: Integer; out aTTxnNetNodeRec: TTxnNetNodeRec): Integer;
    procedure GetTelegram(aNodeID: TNodeID; out aTelegram: array of Byte; out aTelegramsize: Integer);
    function InitMsgPoolList: Boolean;
    procedure SetNodeList(aNodeList: TTxnNetNodeList);
    procedure UpdateLastDecl(aNodeID: TNodeID; aLastDecl: Tdeclaration);
    function UpdateMachState(aTxnDevHandler: TTxnBaseDevHandler; aAdapterNo: Integer; out aNewMachstateArr: array of TTxnNetNodeRec; out aNewMachstate: Boolean):
        Boolean;
    property AdapterCount: Integer read fAdapterCount write fAdapterCount;
    property ErrorRec: TErrorRec read fErrorRec;
    property JobIDValue[aJobID: DWord; aName: String]: Variant read GetJobIDValue write SetJobIDValue;
    property MachState[aNodeID: TNodeID]: TTxnNodeState read GetMachState write SetMachState;
    property MapfileDOM[aMapID: String]: DOMDocument40 read GetMapfileDOM;
    property MapID[aMachID: Integer]: String read GetMapID;
  end;

  //==============================================================================
//var
//wss  gJobID: DWord;
//wss  gJobTyp: TJobTyp;
//wss  gMachine: Integer;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, LoepfeGlobal, AdoDBAccess, MMEventLog, mmcs, XMLGlobal;

const
  cQrySelectMapfile = 'select c_mapfile from t_xml_mapfile where c_mapfile_name = ''%s''';

//:-----------------------------------------------------------------------------
// TMsgPoolList
//:-----------------------------------------------------------------------------
function TMsgPoolList.GetMachState(aNodeID: TNodeID): TTxnNodeState;
// Gibt den Maschinenstatus des betreffenden Nodes zurueck
begin
  Enter;
  try
    result := mMsgPoolList[aNodeID].netNodeRec.MachState;
  finally
    Leave;                   // ...................................
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.SetMachState(aNodeID: TNodeID; aNodeState: TTxnNodeState);
// Setzt den Maschinenstatus des betreffenden Nodes
begin
  Enter;
  try
    mMsgPoolList[aNodeID].netNodeRec.MachState := aNodeState;
  finally
    Leave;                   // ...................................
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.InitMsgPoolList: Boolean;
// Initialisert InMsgPoolList und erstellt alle Instanzen von TMemoryStream in fInMsgPoolList
var
  xNode: Integer;
begin
  Result := False;
  Enter;                   // .....................................
  try
    try
      for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
        with mMsgPoolList[xNode] do begin
          netNodeRec.adapterNo := 0;
          netNodeRec.MachState := nsOffLine;
          netNodeRec.MapName   := '';
          blockNoCount         := 0;
          blockToutCount       := 0;
          assign               := 0;    // AssignTimeout inaktiv
          nextBlockNo          := 1;
          memStream            := TmemoryStream.Create;
          fillchar(LastDeclEvent, SizeOf(LastDeclEvent), 0);
        end;
      end;
      result := TRUE;
    except
      for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
        with mMsgPoolList[xNode] do begin
          FreeAndNil(memStream);
        end;
      end;
    end;
  finally
    Leave;                   // ...................................
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.ClearMsgPool: Boolean;
// Alle DatenRecords in mMsgPoolList werden geloescht und der
// gesamte zugeordnete Speicher freigegeben. Memory = NIL
// Position Size =0
var
  xNode: Integer;
begin
  Enter;                   // .....................................
  try
    try
      for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
        with mMsgPoolList[xNode] do begin
          netNodeRec.adapterNo := 0;
          netNodeRec.MachState := nsOffLine;
          blockNoCount := 0;
          blockToutCount := 0;
          assign := 0;                    // AssignTimeOut inaktiv
          fillchar(LastDeclEvent, SizeOf(LastDeclEvent), 0);
          nextBlockNo := 1;
          memStream.Clear;
        end;
      end;
      result := TRUE;
    except
      result := FALSE;
    end;
  finally
    Leave;                   // ...................................
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.SetNodeList(aNodeList: TTxnNetNodeList);
var
  xNode: Integer;
  xMapIDList: TStringList;
  xDefaultMapname: String;
begin
  Enter;
  xDefaultMapname := GetDefaultMapname;  // liest Mapname von ID 2 aus der DB
  xMapIDList      := TStringList.Create;
  try
    xMapIDList.Sorted     := True;
    xMapIDList.Duplicates := dupIgnore;
    for xNode:=cMinTxnNodeID to cMaxTxnNodeID do begin
      mMsgPoolList[xNode].netNodeRec := aNodeList[xNode];
      // wenn nicht Overrouled wird, dann die Default Mapdatei verwenden
      if mMsgPoolList[xNode].netNodeRec.MapName = '' then
        mMsgPoolList[xNode].netNodeRec.MapName := xDefaultMapname;
      // alle Mapnamen in StringListe sammeln
      if mMsgPoolList[xNode].netNodeRec.MapName <> '' then
        xMapIDList.Add(Format('''%s''', [mMsgPoolList[xNode].netNodeRec.MapName]));
    end;
  finally
    ReadMapfilesFromDB(xMapIDList.CommaText);
    xMapIDList.Free;
    Leave;
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.GetNodeList(var aNodeList: TTxnNetNodeList);
var
  xNode: Integer;
begin
  Enter;
  try
    for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
      with mMsgPoolList[xNode].netNodeRec do begin
        aNodeList[xNode].adapterNo := adapterNo;
        aNodeList[xNode].MachID    := MachID;
        aNodeList[xNode].MachState := MachState;
        aNodeList[xNode].MapName   := MapName;
      end;
    end;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.UpdateMachState(aTxnDevHandler: TTxnBaseDevHandler; aAdapterNo: Integer;
  out aNewMachstateArr: array of TTxnNetNodeRec;
  out aNewMachstate: Boolean): Boolean;
{ Ueberprueft den Status aller Nodes eines bestimmten Adapters. In aNewMachstate werden alle
Maschinen mit MachNr.festgehalten bei welchen eine Statusaenderung erfolgt ist.}
var
  xNode: Integer;
  xOutBuf: TMsgIOControl;
  xOutBuflengh: Integer;
begin
  // NetnodeListe lesen und Msgpool updaten
  aNewMachstate := FALSE;
  FillChar(aNewMachstateArr, (high(aNewMachstateArr) + 1) * sizeof(TTxnNetNodeRec), 0);
  Result := aTxnDevHandler.TxnDevControl(IOCTL_TEXNET_NNSTABLE, xOutBuf, xOutBuflengh);
  if Result then begin
    Enter;
    try
      for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
        if (mMsgPoolList[xNode].netNodeRec.adapterNo = aAdapterNo) then begin
          if (xOutBuf[xNode] > 0) and (xOutBuf[xNode] < $80) then begin
            if mMsgPoolList[xNode].netNodeRec.MachState <> nsOnline then begin
              aNewMachstate := TRUE;
              mMsgPoolList[xNode].netNodeRec.MachState := nsOnline;
              aNewMachstateArr[xNode - cMinTxnNodeID]  := mMsgPoolList[xNode].netNodeRec;
            end;
          end else begin
            if mMsgPoolList[xNode].netNodeRec.MachState <> nsOffLine then begin
              aNewMachstate := TRUE;
              mMsgPoolList[xNode].netNodeRec.MachState := nsOffLine;
              aNewMachstateArr[xNode - cMinTxnNodeID]  := mMsgPoolList[xNode].netNodeRec;
            end;
          end;
        end;
      end;
    finally
      Leave;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.GetNodeID(aMachID: Integer): Integer;
{ Gibt die zur MachId gehoerende NodeID zurueck. Falls aMachID nicht
 gefunden wird. Returnwert: -1 }
var
  xNode: Integer;
begin
  result := cNotValid;
  Enter;
  try
    for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
      if mMsgPoolList[xNode].netNodeRec.MachID = aMachID then begin
        result := xNode;
        Break;
      end;
    end;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.GetMachID(aNodeID: TNodeID): Integer;
{ Gibt die zur NodeID gehoerende MachId zurueck. Falls aMachID =0 Returnwert: -1 }
begin
  Enter;
  try
    if mMsgPoolList[aNodeID].netNodeRec.MachID <> 0 then begin
      result := mMsgPoolList[aNodeID].netNodeRec.MachID;
    end else begin
      result := cNotValid;
    end;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.GetNodeRecord(aMachID: Integer; out aTTxnNetNodeRec: TTxnNetNodeRec): Integer;
{ Gibt die  NodeID und den NodeRecord ( MachID,AdapterNo und Status) zurueck. Falls aMachID nicht
 gefunden wird. Returnwert: -1}

var
  xNode: Integer;
begin
  result := cNotValid;
  Enter;
  try
    for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
      if mMsgPoolList[xNode].netNodeRec.MachID = aMachID then begin
        aTTxnNetNodeRec := mMsgPoolList[xNode].netNodeRec;
        result := xNode;
        Break;
      end;
    end;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.BlockTimeout(): Boolean;
{ Ueberprueft alle MsgPoolRec ob das timeout abgelaufen ist}
var
  xNode: Integer;
  xtext: string;
begin
  result := FALSE;
  Enter;
  try
    for xNode := cMinTxnNodeID to cMaxTxnNodeID do begin
      if mMsgPoolList[xNode].blockToutCount > 1 then begin // Timeout laeuft
        DEC(mMsgPoolList[xNode].blockToutCount);
      end else begin
        if mMsgPoolList[xNode].blockToutCount > 0 then begin // Timeout abgelaufen
          xtext := Format('Blocktimeout (%d s) at MachID: %d', [cBlockTimeOut, mMsgPoolList[xNode].netNodeRec.MachID]);
          ClearMsgPoolRec(xNode);
          result := TRUE;
        end;
      end;
    end;
  finally
    Leave;
  end;
  if result = TRUE then begin           // Timeout mittels ErrorRec
    with fErrorRec do begin
      ErrorTyp := etMMError;
      Error := 0;
      Msg := xtext;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.AssignTimeoutExpired(aNodeID: TNodeID): Boolean;
// Ueberprueft MsgPoolRec ob das AssignTimeout laeuft
begin
  result := FALSE;                      // Timeout laueft oder inaktiv
  Enter;
  try
    if mMsgPoolList[aNodeID].assign > 1 then begin // Timeout laeuft
      DEC(mMsgPoolList[aNodeID].assign);
    end else begin
      if mMsgPoolList[aNodeID].assign > 0 then begin // Timeout abgelaufen
        mMsgPoolList[aNodeID].assign := 0; // Timeout inaktiv setzen
        result := TRUE;
      end;
    end;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.ClearMsgPoolRec(aNodeID: TNodeID): Boolean;
{ Alle Meldungsdaten im Datenrecord[aNodeID] werden geloescht und der
gesamte zugeordnete Speicher freigegeben. Memory = NIL
Position Size =0}
{ !!! Keine CriticSection da diese Methode (Private) nur innerhalb der Klasse gebraucht wird }
{ AssignTimeout wird nicht geloescht da Telegrammuebergreifend}
begin
  with mMsgPoolList[aNodeID] do
  try
    blockNoCount := 0;
    blockToutCount := 0;
    nextBlockNo := 1;
    memStream.Clear;
    result := TRUE;
  except
    result := FALSE;
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.AssignTimeoutBegin(aNodeID: TNodeID);
// Aktiviert AssignTimeout
begin
  Enter;
  try
    mMsgPoolList[aNodeID].assign := cAssinTimeOut; // Start Timeout
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.AssignTimeoutPending(aNodeID: TNodeID): Boolean;
{ Ueberprueft ob Timeout laeuft}
begin
  Enter;
  try
    result := mMsgPoolList[aNodeID].assign <> 0;
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.AssignTimeoutStop(aNodeID: TNodeID);
{ Inaktiviert Timeout}
begin
  Enter;
  try
    mMsgPoolList[aNodeID].assign := 0;    // Timeout Inaktiv
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.Addblock(aBlock: TdatablockRec; aBlocklengh: Integer;
  out aNodeID: TNodeID): TBlockStatus;
{Ein gueltiger empfangener Block wird am stream angehaengt.Falls es der letzte Block ist
wird result:= bsTelegramcomplete.Ungueltige Bloecke werden verworfen result:= bsNotvalidBlock und der
Stream wird geloescht.}
begin
  Enter;
  with aBlock.headerRec do
  try
    try
      if TotalBlocks > blockNumber then begin // 1..n Block aber nicht letzter
        if blockNumber = mMsgPoolList[sendNode].nextBlockNo then begin
          // !!! Add to stream
          mMsgPoolList[sendNode].memStream.writeBuffer(aBlock.datenArray, aBlocklengh - sizeof(TheaderRec));
          // Set TimeOut
          mMsgPoolList[sendNode].blockToutCount := cBlockTimeOut;
          INC(mMsgPoolList[sendNode].nextBlockNo);
          result := bsValidblock;
        end
        else begin
          ClearMsgPoolRec(sendNode);
          result := bsNotvalidblock;
        end;
      end
      else begin                        // letzter oder einiger Block des Telegramms
        if blockNumber = mMsgPoolList[sendNode].nextBlockNo then begin
          // !!! Add to stream
          mMsgPoolList[sendNode].memStream.writeBuffer(aBlock.datenArray, aBlocklengh - sizeof(TheaderRec));
          aNodeID := sendNode;
          result := bsTelegramComplete;
        end
        else begin
          ClearMsgPoolRec(sendNode);
          result := bsNotValidBlock;
        end;
      end;
    except
      result := bsNotValidBlock;
    end;
  finally
    Leave;
  end;
end;                                    {TMsgPoolList.Addblock}
//------------------------------------------------------------------------------
procedure TMsgPoolList.GetTelegram(aNodeID: TNodeID; out aTelegram: array of Byte;
  out aTelegramsize: Integer);
// Liest Telegramm aus memoryStream und loescht Streaminhalt
begin
  Enter;
  try
    with mMsgPoolList[aNodeID].memStream do
    try
      Position := 0;                    // Anfang von Stream
      ReadBuffer(aTelegram, size);
      aTelegramsize := size;
    except
      aTelegramsize := 0;
    end;
    ClearMsgPoolRec(aNodeID);
  finally
    Leave;
  end;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.UpdateLastDecl(aNodeID: TNodeID; aLastDecl: Tdeclaration);
// Wenn Modify gesetzt ist muss Prodgruppe auf 0 gesetzt werden
var
  i: Integer;
begin
  for i:=0 to cMaxZESpdGroups-1 do begin
    with mMsgPoolList[aNodeID].LastDeclEvent.validity[i] do begin
      if aLastDecl.validity[i].modify <> [] then begin
        prodGrpID := 0;
      end else begin
        prodGrpID := aLastDecl.validity[i].prodGrpID;
      end;
//Khp/Nue:4.11.02      modify := [];
      modify := aLastDecl.validity[i].modify;
      range  := aLastDecl.validity[i].range;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.GetlastDeclEvent(aNodeID: TNodeID): Tdeclaration;
begin
  result := mMsgPoolList[aNodeID].LastDeclEvent;
end;
//------------------------------------------------------------------------------
procedure TMsgPoolList.ClearLastDeclEvent(aNodeID: TNodeID);
begin
  fillchar(mMsgPoolList[aNodeID].LastDeclEvent, SizeOf(mMsgPoolList[aNodeID].LastDeclEvent), 0);
end;
//------------------------------------------------------------------------------
constructor TMsgPoolList.Create;
begin
  inherited Create;
  mJobIDValuesSync   := TMultiReadExclusiveWriteSynchronizer.Create;
  mJobIDValues       := TmmStringList.Create;
  mMapfileList       := TStringList.Create;
  mMapfileSync       := TMultiReadExclusiveWriteSynchronizer.Create;
end;
//------------------------------------------------------------------------------
destructor TMsgPoolList.Destroy;
// Entfernt alle Instanzen von TMemoryStream in fInMsgPoolList und MsgPoolList selbst}
var
  i: Integer;
  xIntf: DOMDocument40;
begin
  for i:=cMinTxnNodeID to cMaxTxnNodeID do begin
    with mMsgPoolList[i] do begin
      memStream.Free;
    end;
  end;

  for i:=0 to mMapfileList.Count-1 do begin
    xIntf := DOMDocument40(Pointer(mMapfileList.Objects[i]));
    xIntf := Nil;
  end;

  mJobIDValuesSync.Free;
  mJobIDValues.Free;
  mMapfileList.Free;
  mMapfileSync.Free;

  inherited Destroy;
end;

function TMsgPoolList.ChangeAllXMLDataCounter(aJobID: Integer; aIncrement: Boolean): Boolean;
var
  xInt: Integer;
begin
  mJobIDValuesSync.BeginWrite;
  try
    with TmmStringList.Create do
    try
      CommaText := mJobIDValues.Values[IntToStr(aJobID)];
      xInt   := ValueDef(cJobValueAllXMLValues, 0);
      Result := (xInt > 0);
      if aIncrement then inc(xInt)
                    else dec(xInt);

// Wss (klein aber oho!!) --> Aber funktioniert nicht....aber mit 1 parameter funktioniert es wohl. Stimmt(Lok)
//      if Result then begin
//        Value[cJobValueAllXMLValues]          := xInt;
//        mJobIDValues.Values[IntToStr(aJobID)] := CommaText;
//      end
//      else begin
//        xInt := mJobIDValues.IndexOfName(IntToStr(aJobID));
//        if xInt >= 0 then
//          mJobIDValues.Delete(xInt);
//      end;
// LOK (gross und prozig!!) --> Aber funktioniert
      if xInt > 0 then begin
        Values[cJobValueAllXMLValues] := xInt;
      end
      else begin
        xInt := IndexOfName(cJobValueAllXMLValues);
        if xInt >= 0 then
          Delete(xInt);
      end;

      if Count > 0 then
        mJobIDValues.Values[IntToStr(aJobID)] := CommaText
      else begin
        xInt := mJobIDValues.IndexOfName(IntToStr(aJobID));
        if xInt >= 0 then
          mJobIDValues.Delete(xInt);
      end;
    finally
      Free;
    end;
  finally
    mJobIDValuesSync.EndWrite;
  end;
end;

//------------------------------------------------------------------------------
procedure TMsgPoolList.DeleteJobIDValue(aJobID: DWord);
var
  xIndex: Integer;
begin
  mJobIDValuesSync.BeginWrite;
  try
    xIndex := mJobIDValues.IndexOfName(IntToStr(aJobID));
    if xIndex >= 0 then
      mJobIDValues.Delete(xIndex);
  finally
    mJobIDValuesSync.EndWrite;
  end;
end;
//------------------------------------------------------------------------------
function TMsgPoolList.GetJobIDValue(aJobID: DWord; aName: String): Variant;
begin
  mJobIDValuesSync.BeginRead;
  try
    with TmmStringList.Create do
    try
      CommaText := mJobIDValues.Values[IntToStr(aJobID)];
      Result := Values[aName];
    finally
      Free;
    end;
  finally
    mJobIDValuesSync.EndRead;
  end;
end;
//:-----------------------------------------------------------------------------
function TMsgPoolList.GetMapfileDOM(aMapID: String): DOMDocument40;
var
  xIndex: Integer;
//  xInterface: Pointer;
begin
  Result := Nil;
  mMapfileSync.BeginRead;
  try
    xIndex := mMapfileList.IndexOf(aMapID);
    if xIndex >= 0 then
      Result := DOMDocument40(Pointer(mMapfileList.Objects[xIndex]));
  finally
    mMapfileSync.EndRead;
    if not Assigned(Result) then
      WriteToEventLog('No DOM for MapID found ' + aMapID, 'TMsgPoolList: ', etError);
  end;
end;
//:-----------------------------------------------------------------------------
function TMsgPoolList.GetMapID(aMachID: Integer): String;
begin
  Result := mMsgPoolList[GetNodeID(aMachID)].netNodeRec.MapName;
end;
//:-----------------------------------------------------------------------------
procedure TMsgPoolList.ReadMapfilesFromDB(aMapnames: String);
const
  cQrySelectMapfiles = 'select c_mapfile_name, c_mapfile from t_xml_mapfile ' +
                       'where c_mapfile_name in (%s)';
var
  xDOM: DOMDocument40;
begin
  // Die Stringbegrenzungszeichen m�ssen in jedem Fall ins Query (Auch wenn der String leer ist)
  if aMapnames <> '' then begin
    CodeSite.SendString('Read mapfile for Mapnames', aMapnames);
    mMapfileSync.BeginWrite;
    with TAdoDBAccess.Create(1) do
    try
      Init;
      with Query[0] do begin
        SQL.Text := Format(cQrySelectMapfiles, [aMapnames]);
        Open;
        if FindFirst then
          while not EOF do begin
            xDOM := XMLStreamToDOM(FieldByName('c_mapfile').AsString);
            mMapfileList.AddObject(FieldByName('c_mapfile_name').AsString, Pointer(xDOM));
            // verhindert, dass das Interface wieder freigegeben wird
            Pointer(xDOM) := Nil;
            Next;
          end;
      end;
    finally
      Free;
      mMapfileSync.EndWrite;
    end;
  end else
    CodeSite.SendMsg('ReadMapfilesFromDB: aMapnames = emmpty string');
end;

//:-----------------------------------------------------------------------------
function TMsgPoolList.GetDefaultMapname: string;
const
//  cQrySelectMapfiles = 'select c_mapfile_name from t_xml_mapfile where c_mapfile_id = 2';
  cQrySelectMapfiles =
    ' select c_mapfile_name from t_xml_mapfile ' +
    ' where c_mapfile_name like ''ZE-02%'' ' +
    ' order by c_mapfile_name desc';

begin
  Result := '';
  with TAdoDBAccess.Create(1) do
  try
    Init;
    with Query[0] do begin
      SQL.Text := cQrySelectMapfiles;
      Open;
      if FindFirst then
        Result := FieldByName('c_mapfile_name').AsString;
    end;
  finally
    Free;
  end;
end;

//------------------------------------------------------------------------------
procedure TMsgPoolList.SetJobIDValue(aJobID: DWord; aName: String; const aValue: Variant);
begin
  mJobIDValuesSync.BeginWrite;
  try
    with TmmStringList.Create do
    try
      CommaText := mJobIDValues.Values[IntToStr(aJobID)];
      Values[aName] := aValue;
      mJobIDValues.Values[IntToStr(aJobID)] := CommaText;
    finally
      Free;
    end;
  finally
    mJobIDValuesSync.EndWrite;
  end;
end;
//:-----------------------------------------------------------------------------
end.

