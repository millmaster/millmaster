(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobHandlerUtil.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.04.1999  0.00  Nue | Datei erstellt
| 29.11.1999  1.00  Nue | Queries bei where auf c_machine_state von IN (1,3) auf IN (1) umgestellt!
| 28.02.2000  1.01  Nue | Bei cQrySelMaProdGrps "order by c_spindle_first" eingefuegt
| 28.03.2000  1.02  Nue | Bei cQrySelMaProdGrps "order by c_machineGroup" eingefuegt
| 06.06.2000  1.03  Nue | Bei cQrySelFragAndIntID "(c_data_ok=0) and " eingefuegt
| 27.11.2000  1.04  Nue | Bei cQrySelNetNodes "and nn.c_net_id<>0 " und cQrySelMachine eingefuegt
| 11.06.2001  1.05  Nue | Modifications because of DataCollection-Handling.
| 11.07.2001  1.06  Nue | Query cxQryGetActFragID modifiziert.
| 13.12.2001  1.07  Nue | Query cQrySelStyle added; Take over StyleID when start from ZE(WSC).
| 31.01.2002  1.08  Nue | Take over YmSetName when start (reassign) from ZE(WSC) an add -X if used.
| 28.02.2002  1.09  Nue | Changings because of yarnCnt-Handling. (c_act_yarncnt)
| 04.04.2002  1.10  Nue | Take over YM_Set_name from previous prodgroup, when start on ZE
| 27.09.2002         Wss| Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 06.11.2002  1.11  Nue | Start- Endzeit bei einf�gen einer Prodgruppe auf DB verschieden  getdate()->:nowdate
| 11.11.2002  1.12  Nue | Test t_prodgroup_state replaced with v_prodgroup_state
| 18.11.2002  1.13  Nue | Changes because of changing c_machine_state from t_prodgroup_state to t_machine_state.
| 10.12.2002  1.14  Nue | 'AND (c_machine_state<>0)' added in Query cQrySetMachAndNodeState.
| 09.09.2004  1.15  Nue | PrepareSendMsgToAppl modified.
| 22.09.2004  1.16  Nue | At all queries wich contain more then 1 statement ";" added, between the statements. (Because of ADO-Error?)
| 21.06.2005  1.20  Nue | V.5.01: MaOffline Handling
| 10.08.2005  1.21  Nue | Fix Error with styleID=0 after starting MM, if Settings were modified at the ZE, while MM was down.
|                       | in InsertDummyProdGrp.
| 25.11.2005        Lok | Hints und Warnings entfernt
| 28.11.2006        Nue | c_use_newer_mapfile added.
| 12.06.2008  1.22  Nue | In InsertDummyProdGrp cQryGetProdID durch cQryGetProdID3 ersetzt. (Stopping-Mode ber�cksichtigen!)
|=========================================================================================*)
unit JobHandlerUtil;

interface

uses
  ADODBAccess,
  SysUtils, BaseGlobal, MMUGlobal, Windows, YMParaDef, XMLDef;

const
  //Queries JobListClasses
  cQrySelOrderPosition =
  'SELECT * FROM t_order_position WHERE c_order_id=:c_order_id AND c_order_position_id=:c_order_position_id ';
{ TODO 1 -oNue -cDispomaster : Wenn Dispomaster mit orders implementiert wird, muessen die Queries cQrySelOrderPosition und cQrySelStyle zusammengefasst werden. }
  cQrySelStyle =
  'SELECT * FROM t_style WHERE c_style_id=:c_style_id';

  cQryDelProdGrp =
    'DELETE t_prodgroup WHERE c_prod_id=:c_prod_id';

  cQrySelProdGrp =
  'SELECT * FROM t_prodgroup WHERE c_prod_id=:c_prod_id ';

  cQrySelMachine =
  'SELECT * FROM t_machine WHERE c_machine_id=:c_machine_id ';

  cQryGetProdID =
     'SELECT * FROM v_prodgroup_state WHERE c_machine_id=:c_machine_id AND c_spindle_first=:c_spindle_first '+
     'AND c_spindle_last=:c_spindle_last ';    //Nue 10.05.00

  cQryGetProdID2 =
     'SELECT c_prod_id FROM t_prodgroup WHERE c_machine_id=:c_machine_id AND c_spindle_first=:c_spindle_first '+
     'AND c_spindle_last=:c_spindle_last '+    //Nue 10.05.00
     'AND c_prod_state in (1,3)';

  cQryGetProdID3 =     //Nue 12.06.08
     'SELECT * FROM v_prodgroup_state WHERE c_machine_id=:c_machine_id AND c_spindle_first=:c_spindle_first '+
     'AND c_spindle_last=:c_spindle_last '+
     'AND c_prod_state not in (6)';   //stopping

  cQryGetYMSetName = //Nue:4.4.02
    'SELECT c_ym_set_name FROM t_xml_ym_settings WHERE c_ym_set_id=:c_ym_set_id ';

  cQrySelBadProdGrpState =  //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE (c_prod_state not in (1,3)) and (c_machine_state<>4) AND '+
    '(c_prod_id=:c_prod_id)';

  cQrySelCollectingState =  //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_prod_state=5 ';  //5= Collecting data

  cQryMachStateOffline =
    'SELECT c_machine_state FROM t_machine WHERE c_machine_id=:c_machine_id AND '+
    'c_machine_state IN (0,2,3) '; // 0=Undefined, 1=Running, 2=No data collection, 3=Offline, 4=In synchronisation

  cQrySelMachState =
    'SELECT c_machine_state FROM t_machine WHERE c_machine_id=:c_machine_id  ';

  cQrySelNodeState =
    'SELECT * FROM t_net_node '+
    'WHERE (c_node_id like (SELECT c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id))';

//  cQrySelSpindle =
//    'SELECT * FROM t_spindle WHERE (c_machine_id=:c_machine_id) AND (c_spindle_id=:c_spindle_id) ';

//Nue27.11.00  cQrySelMachineType =
//    'SELECT mt.*,m.c_data_collection,m.c_net_id FROM t_machine_type mt, t_machine m '+
//    'WHERE (mt.c_machine_type=m.c_machine_type) AND (m.c_machine_id=:c_machine_id)';

  cQrySelNetMachinesOnline =
    'SELECT c_machine_id FROM v_prodgroup_state '+
    'WHERE (c_net_id=:c_net_id) AND (c_machine_state=:c_machine_state)';

  cQrySelMaProdGrps =
    'SELECT * FROM v_prodgroup_state WHERE '+
    'c_prod_state<>4 AND '+  //Nue 14.06.00
    'c_machine_id= :c_machine_id order by c_machineGroup';

  cQrySelFragAndIntID =
    'SELECT c_prod_id, c_fragshift_id_ok, c_int_id_ok FROM v_prodgroup_state '+
    'WHERE (c_data_ok=0) and (c_machine_id= :c_machine_id) and '+
    '(c_spindle_first=:c_spindle_first) and (c_spindle_last=:c_spindle_last)';

//  cQrySelUnassignedSpindles =
//    'SELECT s.c_spindle_id FROM t_spindle s WHERE s.c_machine_id=:c_machine_id AND s.c_spindle_id NOT IN '+
//    '(SELECT s.c_spindle_id FROM t_spindle s, v_prodgroup_state ps '+
//    'WHERE (s.c_machine_id=:c_machine_id) AND (s.c_machine_id=ps.c_machine_id) AND '+
//    '((s.c_spindle_id <= ps.c_spindle_last) AND '+
//    '(s.c_spindle_id >= ps.c_spindle_first))) '; //Rest of query concat in code

  cQrySelGrpStateSingle =    //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_prod_id= :c_prod_id ';

  cQrySelSameMachGroup =    //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_machine_id=:c_machine_id AND c_machineGroup=:c_machineGroup '+
    'AND c_spindle_first=:c_spindle_first AND c_spindle_last=:c_spindle_last ';

   cQrySelNoSpdInGrpState =    //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_machine_id=:c_machine_id AND c_machineGroup=:c_machineGroup '+
    'AND (((c_spindle_first=:c_spindle_first AND c_spindle_last=:c_spindle_last)) OR '+
    '((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //First and last within the new range
    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //First within existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last)))';      //Last within existing ProdGrp

  cQrySelGrpState =    //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_prod_state=5 AND c_machine_state IN (1) ';

  cQrySelGrpStateAtStartup1 =   //Used in JobManagerClass
    'SELECT * FROM t_prodgroup WHERE c_prod_state not in (1,3,4,5,6) ';

  cQrySelGrpStateAtStartup2 =  //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state WHERE c_prod_state not in (1,3,4) ';

  cQrySelGrpStateNotCollecting =
    'SELECT * FROM v_prodgroup_state WHERE c_prod_state NOT IN (5) ';

  cQrySelGrpNotAllowedInRange =  //Used in JobManagerClass
    'SELECT * FROM v_prodgroup_state '+
    'WHERE c_machine_id=:c_machine_id AND (c_prod_state NOT IN (1,3,4) OR c_machine_state NOT IN (1)) AND '+
    '(c_prod_id<>:c_prod_id) AND '+
    '((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //First and last within the new range
    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //First within existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last))';      //Last within existing ProdGrp

  cQrySelGrpAffectedInRange =
    'SELECT * FROM v_prodgroup_state '+
    'WHERE c_prod_state IN (6) AND c_machine_state IN (1) AND (c_machine_id=:c_machine_id) AND '+
    '((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //First and last within the new range
    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //First within existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last))';      //Last within existing ProdGrp

  cQrySelNetNodes =
    ' SELECT m.c_machine_ID, nn.c_node_id, nn.c_net_id, nn.c_adapt_or_name, nn.c_node_stat, x.c_mapfile_name, m.c_use_newer_mapfile' +
    ' FROM t_machine m, t_net_node nn, t_xml_mapfile x ' +
    ' WHERE nn.c_net_id <> 0 ' +
    ' AND m.c_node_id = nn.c_node_id' +
    ' AND m.c_overrule_mapfile_id *= x.c_mapfile_id' +
    ' ORDER BY nn.c_net_id, nn.c_node_id';
//    ' SELECT m.c_machine_ID, nn.c_node_id, nn.c_net_id, nn.c_adapt_or_name, nn.c_node_stat, x.c_mapfile_name' +
//    ' FROM t_machine m, t_net_node nn, t_xml_mapfile x WHERE m.c_node_id LIKE nn.c_node_id' +
//    ' and nn.c_net_id<>0 and (m.c_overrule_mapfile_id=x.c_mapfile_id)  ' +
//    ' ORDER BY nn.c_net_id, nn.c_node_id';

  cQrySelNetIDs =
      'SELECT DISTINCT(c_net_id) FROM t_machine ';

  cQrySelNetID =
      'SELECT c_net_id FROM t_machine WHERE c_machine_id=:c_machine_id ';

  cQrySelMaxSpd =
//NUE1      'SELECT c_spindle_id=MAX(c_spindle_id) FROM t_spindle WHERE c_machine_id=:c_machine_id';
      'SELECT c_spindle_id=c_nr_of_spindles FROM t_machine WHERE c_machine_id=:c_machine_id';

  cQrySelDataOkFlagFalse =
    'SELECT * FROM v_prodgroup_state WHERE c_data_ok=0';

//  cQrySetDataOkFlagTrueP =
//    'UPDATE v_prodgroup_state SET c_data_ok=1 WHERE c_prod_id=:c_prod_id';

{alt Delete  cQrySetGrpStateBackM =
    'UPDATE v_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state, '+
    'c_old_fragshift_id_ok=c_fragshift_id_ok, c_old_int_id_ok=c_int_id_ok, c_data_ok=1 '+
    'WHERE c_machine_id= :c_machine_id AND c_spindle_first= :c_spindle_first '+
    'AND c_spindle_last= :c_spindle_last';
}
  cQrySetGrpStateBackMProdGrp =
    'UPDATE t_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state, '+
    'c_old_fragshift_id_ok=c_fragshift_id_ok, c_old_int_id_ok=c_int_id_ok, c_data_ok=1 '+
    'WHERE c_prod_id=:c_prod_id ';

  //Need for stops and starts, when succeding action are following. Then it's important
  // that the c_prod_state stays on STOPPING, that no other action can interrupt the running one.
  cQrySetGrpStateBackM2 =
    'UPDATE t_prodgroup_state set '+
    'c_old_fragshift_id_ok=c_fragshift_id_ok, c_old_int_id_ok=c_int_id_ok, c_data_ok=1 '+
    'WHERE c_machine_id= :c_machine_id AND c_spindle_first= :c_spindle_first '+
    'AND c_spindle_last= :c_spindle_last';

{  cQrySetGrpStateBackP =
    'UPDATE t_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state, c_data_ok=1 '+
    'WHERE c_prod_id= :c_prod_id';
{}
  cQrySetOnlyGrpStateBackP =
    'UPDATE t_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state '+
    'WHERE c_prod_id= :c_prod_id';

  cQrySetOnlyGrpStateBackAtStartup =
    'UPDATE t_prodgroup_state set c_prod_state=c_old_prod_state, c_old_prod_state=c_prod_state '+
    'WHERE (c_prod_state in (6,5)); '+   //5=collecting; 6=stopping

    'DELETE t_prodgroup WHERE (c_prod_state in (0,2)) OR '+
    '(c_prod_id in (SELECT c_prod_id FROM t_prodgroup_state WHERE c_prod_state in (0,2))); '+

    'DELETE t_prodgroup_state WHERE c_prod_state in (0,2) ';  //0=Undefined; 2=not started

{Following 2 queries replaced by the following both. Nue 11.6.01
  cQrySetMachState =
    'UPDATE t_prodgroup_state set c_machine_state=:c_machine_state '+
    'WHERE (c_machine_id=:c_machine_id) AND (c_machine_state<>2) '; //2=msNoDataCollection

  cQrySetMachAndNodeState =
    'UPDATE t_prodgroup_state set c_machine_state=:c_machine_state '+
    'WHERE (c_machine_id=:c_machine_id) AND (c_machine_state<>2) '+ //2=msNoDataCollection
    'UPDATE t_net_node set c_node_stat=:c_node_stat '+
    'WHERE (c_node_id like (SELECT c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id))';
}
  cQrySetMachState =
{ DONE 3 -oNUE -cSystem : MachineState }
    'UPDATE t_machine set c_machine_state=:c_machine_state '+
    'WHERE (c_machine_id=:c_machine_id) ';

//  cQrySetMachAndNodeState =
//{ DONE 3 -oNUE -cSystem : MachineState }
////    'UPDATE v_prodgroup_state set c_machine_state=:c_machine_state '+
//    'UPDATE t_machine set c_machine_state=:c_machine_state '+
//    'WHERE (c_machine_id=:c_machine_id) '+
//    'AND (c_machine_state<>0) '+
//    +  //2=msUndefined Nue:10.12.02
//    'UPDATE t_net_node set c_node_stat=:c_node_stat '+
//    'WHERE (c_node_id like (SELECT c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id))';

  cQrySetMachAndNodeState =
{ DONE 3 -oNUE -cSystem : MachineState }
//    'UPDATE v_prodgroup_state set c_machine_state=:c_machine_state '+
    'UPDATE t_machine set c_machine_state=:c_machine_state '+
    'WHERE (c_machine_id=:c_machine_id) '+
    'AND (c_machine_state<>0); '+ //2=msUndefined Nue:10.12.02
    'UPDATE t_net_node set c_node_stat=:c_node_stat '+
    'WHERE (c_node_id like (SELECT c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id))';

  cQryStopProdGrp =
    'UPDATE t_prodgroup SET c_prod_state= :c_prod_state, c_prod_end=:c_prod_end '+
    'WHERE c_prod_id= :c_prod_id; '+
//Nue 13.06.00    'DELETE v_prodgroup_state WHERE c_prod_id=:c_prod_id'; //Replaces the following 2 lines Nue 8.5.2000
    'UPDATE t_prodgroup_state SET c_prod_state=:c_prod_state,c_old_prod_state=c_prod_state '+
    'WHERE c_prod_id=:c_prod_id';

  cQryCheckMaGroupConfig =
    ' SELECT m.c_magroup_config_id ' +
    ' FROM t_machine m, t_magroup_config mc' +
    ' WHERE m.c_machine_id = :MachID' +
    ' AND m.c_magroup_config_id = mc.c_magroup_config_id';

  cQryStopProdGrpForced =
    'UPDATE t_prodgroup SET c_prod_state=:c_prod_state, c_prod_end=:c_prod_end '+
    'WHERE c_prod_id=:c_prod_id; '+
    'DELETE t_prodgroup_state WHERE c_prod_id=:c_prod_id'; //Replaces the following 2 lines Nue 8.5.2000
//    'UPDATE t_prodgroup_state SET c_prod_state=:c_prod_state,c_old_prod_state=:c_old_prod_state '+
//    'WHERE c_prod_id=:c_prod_id';

  cQryUpdProdGrpState =
    'UPDATE t_prodgroup_state set c_color=:c_color WHERE c_prod_id=:c_prod_id ';

  cQryUpdProdGrpStopped =  //Nue 14.6.00
    'UPDATE t_prodgroup set c_prod_end=getdate(), c_prod_state=4 '+ //4=stopped
    'WHERE (c_prod_start=c_prod_end) AND (c_prod_id NOT IN '+
    '(SELECT c_prod_id FROM t_prodgroup_state WHERE c_prod_state<>4)) ';

  cQryUpdDummyProdGrp =
    'UPDATE t_prodgroup set c_prod_state=:c_prod_state, c_YM_set_id=:c_YM_set_id, c_start_mode=:c_start_mode '+
    'WHERE c_prod_id=:c_prod_id; '+

    'UPDATE t_prodgroup set c_prod_end=getdate(), c_prod_state=4 '+ //4=stopped
    'WHERE ((c_prod_id<>:c_prod_id) AND (c_machine_id=:c_machine_id) AND '+
    '(((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //Nue 15.6.00 First and last within the new range
    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //Nue 13.6.00 First in range of existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last)) '+ //Nue 13.6.00 Last in range of existing ProdGrp
    'OR (c_machineGroup=:c_machineGroup)) '+  //Nue 23.05.00
    'AND (c_prod_start=c_prod_end)) '+
    'OR  (c_prod_id=:c_old_prod_id); '+

 { TODO 1 -oNue -cBasis : Ueberlegen wegen Konsistenz von Verschachteltetn Starts! }
    'INSERT into t_prodgroup_state(c_prod_id, c_order_position_id, c_order_id, c_machine_id, c_net_id, '+
    'c_style_id, c_clear_type, c_YM_set_id, c_spindle_first, c_spindle_last, c_machineGroup, '+
    'c_assignment, c_prod_state, c_old_prod_state, c_color, c_start_mode, c_prod_start, c_data_collection, '+
    'c_fragshift_id_ok, c_int_id_ok, c_old_fragshift_id_ok, c_old_int_id_ok, c_data_ok, c_shift_fragshift_id_ok, c_shift_int_id_ok) '+
    'SELECT DISTINCT c_prod_id,c_order_position_id,c_order_id,:c_machine_id,:c_net_id, '+
    'c_style_id,0,c_YM_set_id,c_spindle_first,c_spindle_last,c_machineGroup,'+
    'c_assignment,c_prod_state,c_prod_state,:c_color,c_start_mode,c_prod_start,:c_data_collection, '+
    ':c_fragshift_id_ok,:c_int_id_ok,:c_old_fragshift_id_ok,:c_old_int_id_ok,1/*io*/,:c_shift_fragshift_id_ok,:c_shift_int_id_ok '+
    'FROM t_prodgroup '+
    'WHERE (c_prod_id=:c_prod_id); '+

    'DELETE t_prodgroup_state WHERE (c_prod_id<>:c_prod_id) AND (c_machine_id=:c_machine_id) AND '+
    '((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //Nue 15.6.00 First and last within the new range
    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //Nue 13.6.00 First in range of existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last) '+ //Nue 13.6.00 Last in range of existing ProdGrp
    'OR (c_machineGroup=:c_machineGroup)) ';  //Nue 23.05.00  / Nue15.06.00

//  cQryUpdDummyProdGrp =
//    'UPDATE t_prodgroup set c_prod_state=:c_prod_state, c_YM_set_id=:c_YM_set_id, c_start_mode=:c_start_mode '+
//    'WHERE c_prod_id=:c_prod_id; '+
//
//    'UPDATE t_prodgroup set c_prod_end=getdate(), c_prod_state=4 '+ //4=stopped
//    'WHERE ((c_prod_id<>:c_prod_id) AND (c_machine_id=:c_machine_id) AND '+
//    '(((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //Nue 15.6.00 First and last within the new range
//    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //Nue 13.6.00 First in range of existing ProdGrp
//    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last)) '+ //Nue 13.6.00 Last in range of existing ProdGrp
//    'OR (c_machineGroup=:c_machineGroup)) '+  //Nue 23.05.00
//    'AND (c_prod_start=c_prod_end)) '+
//    'OR  (c_prod_id=:c_old_prod_id); '+
//
// { TODO 1 -oNue -cBasis : Ueberlegen wegen Konsistenz von Verschachteltetn Starts! }
//    'INSERT into t_prodgroup_state(c_prod_id, c_order_position_id, c_order_id, c_machine_id, c_net_id, '+
//    'c_style_id, c_clear_type, c_YM_set_id, c_spindle_first, c_spindle_last, c_machineGroup, '+
//    'c_assignment, c_prod_state, c_old_prod_state, c_color, c_start_mode, c_prod_start, c_data_collection, '+
//    'c_fragshift_id_ok, c_int_id_ok, c_old_fragshift_id_ok, c_old_int_id_ok, c_data_ok, c_shift_fragshift_id_ok, c_shift_int_id_ok) '+
//    'SELECT DISTINCT c_prod_id,c_order_position_id,c_order_id,:c_machine_id,:c_net_id, '+
//    'c_style_id,:c_clear_type,c_YM_set_id,c_spindle_first,c_spindle_last,c_machineGroup,'+
//    'c_assignment,c_prod_state,c_prod_state,:c_color,c_start_mode,c_prod_start,:c_data_collection, '+
//    ':c_fragshift_id_ok,:c_int_id_ok,:c_old_fragshift_id_ok,:c_old_int_id_ok,1/*io*/,:c_shift_fragshift_id_ok,:c_shift_int_id_ok '+
//    'FROM t_prodgroup '+
//    'WHERE (c_prod_id=:c_prod_id); '+
//
//    'DELETE t_prodgroup_state WHERE (c_prod_id<>:c_prod_id) AND (c_machine_id=:c_machine_id) AND '+
//    '((c_spindle_first >= :c_spindle_first AND c_spindle_last <= :c_spindle_last) OR '+ //Nue 15.6.00 First and last within the new range
//    '(c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR '+ //Nue 13.6.00 First in range of existing ProdGrp
//    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last) '+ //Nue 13.6.00 Last in range of existing ProdGrp
//    'OR (c_machineGroup=:c_machineGroup)) ';  //Nue 23.05.00  / Nue15.06.00
//

  cQryInsDummyProdGrp =
    'INSERT into t_prodgroup(c_prod_id, c_order_position_id, c_order_id, c_machine_id, c_style_id, '+
    'c_clear_type, c_YM_set_id, c_prod_name, c_spindle_first, c_spindle_last, '+
//wss/nue    c_m_to_prod,
    'c_act_yarncnt, c_yarncnt_unit, '+
    'c_nr_of_threads, c_slip, c_order_position_prod_id, c_prod_state, c_prod_start, c_prod_end, c_start_mode,c_machine_name, '+
    'c_style_name,c_order_position_name,c_adjustFactor,c_assignment,c_machineGroup,c_pilotSpindles, '+
    'c_speedRamp, c_voidData, c_speed, c_lengthMode, c_lengthWindow) '+
    'SELECT :c_prod_id,:c_order_position_id,:c_order_id,m.c_machine_id,:c_style_id,'+
    '0,1,:c_prod_name,:c_spindle_first,:c_spindle_last,'+
//wss/nue    :c_m_to_prod,
    ':c_act_yarncnt,:c_yarncnt_unit,'+
//Alt    ':c_nr_of_threads,:c_slip,0,:c_prod_state,getdate(),getdate(),:c_start_mode,m.c_machine_name,'+
//Nue:06.11.02 Start- Endzeit bei einf�gen einer Prodgruppe auf DB verschieden
    ':c_nr_of_threads,:c_slip,0,:c_prod_state,:nowdate,:nowdate,:c_start_mode,m.c_machine_name,'+
    ':c_style_name,:c_order_position_name,1000,1,:c_machineGroup,:c_pilotSpindles,'+
    ':c_speedRamp,0,:c_speed,:c_lengthMode,:c_lengthWindow '+
//NUE1    'FROM t_machine m WHERE m.c_machine_id=:c_machine_id AND s.c_machine_id=:c_machine_id ';
    'FROM t_machine m WHERE m.c_machine_id=:c_machine_id ';

//  cQryInsDummyProdGrp =
//    'INSERT into t_prodgroup(c_prod_id, c_order_position_id, c_order_id, c_machine_id, c_style_id, '+
//    'c_clear_type, c_YM_set_id, c_prod_name, c_spindle_first, c_spindle_last, '+
////wss/nue    c_m_to_prod,
//    'c_act_yarncnt, c_yarncnt_unit, '+
//    'c_nr_of_threads, c_slip, c_order_position_prod_id, c_prod_state, c_prod_start, c_prod_end, c_start_mode,c_machine_name, '+
//    'c_style_name,c_order_position_name,c_adjustFactor,c_assignment,c_machineGroup,c_pilotSpindles, '+
//    'c_speedRamp, c_voidData, c_speed, c_lengthMode, c_lengthWindow) '+
//    'SELECT :c_prod_id,:c_order_position_id,:c_order_id,m.c_machine_id,:c_style_id,'+
//    's.c_clear_type,1,:c_prod_name,:c_spindle_first,:c_spindle_last,'+
////wss/nue    :c_m_to_prod,
//    ':c_act_yarncnt,:c_yarncnt_unit,'+
////Alt    ':c_nr_of_threads,:c_slip,0,:c_prod_state,getdate(),getdate(),:c_start_mode,m.c_machine_name,'+
////Nue:06.11.02 Start- Endzeit bei einf�gen einer Prodgruppe auf DB verschieden
//    ':c_nr_of_threads,:c_slip,0,:c_prod_state,:nowdate,:nowdate,:c_start_mode,m.c_machine_name,'+
//    ':c_style_name,:c_order_position_name,1000,1,:c_machineGroup,:c_pilotSpindles,'+
//    ':c_speedRamp,0,:c_speed,:c_lengthMode,:c_lengthWindow '+
//    'FROM t_machine m, t_spindle s WHERE m.c_machine_id=:c_machine_id AND s.c_machine_id=:c_machine_id '+
//    'AND s.c_spindle_id=:c_spindle_id ';
{ DONE 1 -oNUE -cSystem : getdate()  Parameter im Code �bergeben, mit lokaler Variable, wo Zeit nur einmal gelesen wird! }

  cQrySetGrpStateCollecting =
(*    'UPDATE v_prodgroup_state set c_old_prod_state=c_prod_state, c_prod_state=5 '+  //5=collecting data
    'WHERE c_prod_state IN (1,3) AND c_machine_state IN (1) ';*)
    'UPDATE t_prodgroup_state set c_old_prod_state=c_prod_state, c_prod_state=5 '+
    'FROM t_prodgroup_state p, t_machine m '+
    'WHERE p.c_prod_state IN (1,3) AND m.c_machine_state IN (1) and m.c_machine_id=p.c_machine_id';

  cQrySetGrpState =
    'UPDATE t_prodgroup_state set c_old_prod_state=c_prod_state, c_prod_state=:c_prod_state '+
    'WHERE c_prod_id= :c_prod_id ';

  cQrySetGrpAffectedState =
    'UPDATE t_prodgroup_state set c_old_prod_state=c_prod_state, c_prod_state=:c_prod_state '+
    'FROM t_prodgroup_state p, t_machine m '+
    'WHERE p.c_prod_state IN (1,3) AND m.c_machine_state IN (1) and (m.c_machine_id=p.c_machine_id)  AND (p.c_machine_id=:c_machine_id) AND '+
//    'FROM v_prodgroup_state '+
//    'WHERE c_prod_state IN (1,3) AND c_machine_state IN (1) AND (c_machine_id=:c_machine_id) AND '+
    '((p.c_machineGroup=:c_machineGroup) OR '+ //Nue 8.5.2000
    '((p.c_spindle_first >= :c_spindle_first AND p.c_spindle_last <= :c_spindle_last) OR '+ //First and last within the new range
    '(p.c_spindle_first <= :c_spindle_first AND p.c_spindle_last >= :c_spindle_first) OR '+ //First within existing ProdGrp
    '(p.c_spindle_first <= :c_spindle_last AND p.c_spindle_last >= :c_spindle_last)))';      //Last within existing ProdGrp

  cQrySetGrpAffectedStateAC338 =
    'UPDATE t_prodgroup_state set c_old_prod_state=c_prod_state, c_prod_state=:c_prod_state '+
    'FROM t_prodgroup_state p, t_machine m '+
    'WHERE p.c_prod_state IN (1,3) AND m.c_machine_state IN (1) and (m.c_machine_id=p.c_machine_id)  AND (p.c_machine_id=:c_machine_id) AND '+
//    'FROM v_prodgroup_state '+
//    'WHERE c_prod_state IN (1,3) AND c_machine_state IN (1) AND (c_machine_id=:c_machine_id) AND '+
    '(p.c_machineGroup=:c_machineGroup ) AND '+
    '(p.c_spindle_first >= :c_spindle_first AND p.c_spindle_last <= :c_spindle_last)';      //First and last within the new range

  cQryDelProdGrpStateStopped =
    'DELETE t_prodgroup_state WHERE c_machine_id=:c_machine_id AND c_prod_state=4 '; //4=Stopped


  cQryDelProdGrpStateStoppedStartup =
    'DELETE t_prodgroup_state WHERE ((c_prod_state=4) OR '+ //4=Stopped
    '(c_prod_id NOT IN (SELECT c_prod_id FROM t_prodgroup WHERE c_prod_state<>4))) ';  //Nue 13.06.00

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

function GetActIntIDAndFragID(aQuery: TNativeAdoQuery; var aIntID: Byte; var aFragID: Longint;
                              var aError: TErrorRec): Boolean;

function GetPrevActIntIDAndFragID(aQuery: TNativeAdoQuery; var aIntID: Byte; var aFragID: Longint;
                              var aError: TErrorRec): Boolean;

function GetProdGrpData(aQuery: TNativeAdoQuery; aProdID: Longint; var aMachineID: Word; var aSettingsRec: TXMLSettingsRec; var aError: TErrorRec): Boolean;

{Nue 9.5.00
function StopOldStartInconsProdGrp(aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aProdID, aNewDummyProdID: Longint;
                                   aYarnCntUnit: Word; var aError: TErrorRec): Boolean;
}
function IsMaOffline(aQuery: TNativeAdoQuery; aMachineID: Word; var aError:
    TErrorRec): Boolean;

procedure OldSetDefaultProdGrpData(var aProdGrpData: TProdGrpData;
                                   var aColor: Integer );  //Color for representation on floor

procedure SetDefaultProdGrpData(//var aProdGrpData: TProdGrpData;
                                var aSettingsRec: TXMLSettingsRec;
                                var aColor: Integer );  //Color for representation on floor

function InsertDummyProdGrp(aQuery: TNativeAdoQuery; aMachineID, aSpindleFirst, aSpindleLast: Word; aYarnCntUnit: TYarnUnit;
                         aOldProdId: Integer; aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
//                         var aProdGrpData: TProdGrpData;
                         var aSettingsRec: TXMLSettingsRec;
                         var aColor: Integer;   //Color for representation on floor
                         var aYMSetName: TText50;   //Nue:2.10.01
                         var aError: TErrorRec): Integer;

procedure PrepareSendMsgToAppl(aMsgTyp: TResponseMsgTyp; aMachineID, aSpindleFirst, aSpindleLast: Word;
                               aComputerName: TString30; aPort : TString30; var aJobRec: TJobRec);

{Nue 9.5.00
function StopOldStartInconsProdGrp(aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aProdID, aNewDummyProdID: Longint;
                                   aYarnCntUnit: Word; var aError: TErrorRec): Boolean;
}
function StopOldProdGrp(aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
    aProdID: Longint; var aError: TErrorRec): Boolean;


implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS;  // Unit fuer CodeSite


//-----------------------------------------------------------------------------
function GetActIntIDAndFragID(aQuery: TNativeAdoQuery; var aIntID: Byte; var aFragID: Longint;
                                var aError: TErrorRec): Boolean;
  const
    cxQryGetActIntID =
   'select c_interval_id from t_Interval ' +
//ALt   'where c_interval_start < getdate() and c_interval_end >= getdate() ';
//Nue:06.11.02 Start- Endzeit bei einf�gen einer Prodgruppe auf DB verschieden
   'where c_interval_start < :nowdate and c_interval_end >= :nowdate ';

 cxQryGetActFragID =
   'select c_fragshift_id from t_fragshift ' +
   'where c_fragshift_length = 0 order by c_fragshift_id desc';  //Nue 11.7.01 desc eingefuegt

begin
  Result := False;
  aIntID := 0;
  aFragID := 0;

  try
    aQuery.Close;
    aQuery.SQL.Text := cxQryGetActIntID;
    aQuery.Params.ParamByName('nowdate').AsDateTime := Now;
    aQuery.Open;
    if aQuery.FindFirst then begin
      aIntID := aQuery.FieldByName('c_interval_id').AsInteger;
      Result := True;
    end;

    aQuery.Close;
    aQuery.SQL.Text := cxQryGetActFragID;
    aQuery.Open;
    if aQuery.FindFirst then begin
      aFragID := aQuery.FieldByName('c_fragshift_id').AsInteger;
      Result := True;
    end;
  except
    on e:Exception do begin
      aError := SetError (ERROR_INVALID_FUNCTION, etDBError, 'GetActIntIDAndFragID failed, probably no intervals or fragshifts on DB available.' + e.message );
      raise Exception.Create ( aError.Msg );
    end;
  end;
end;
//-----------------------------------------------------------------------------
function GetPrevActIntIDAndFragID(aQuery: TNativeAdoQuery; var aIntID: Byte; var aFragID: Longint;
                                  var aError: TErrorRec): Boolean;
const
  cxQryGetPrevActIntID =
   'select c_interval_id, c_interval_start from t_interval where c_interval_end = '+
   '(select max (c_interval_end) from t_interval where '+
   'c_interval_end < getdate()) and c_interval_start<c_interval_end ';

 cxQryGetPrevActFragID =
   'select c_fragshift_id from t_fragshift where c_fragshift_start = ' +
   '(select max ( c_fragshift_start ) from t_fragshift where c_fragshift_start<=:c_prev_IntStart ) ';
//   'select c_fragshift_id from t_fragshift where c_fragshift_start = ' +
//   '(select max ( c_fragshift_start ) from t_fragshift where c_fragshift_length <> 0 )';

var
  xIntervalStart: TDatetime;
begin
  aIntID  := 0;
  aFragID := 0;
  try
    aQuery.Close;
    aQuery.SQL.Text := cxQryGetPrevActIntID;
    aQuery.Open;
    if aQuery.FindFirst then begin
      aIntID := aQuery.FieldByName('c_interval_id').AsInteger;
      xIntervalStart := aQuery.FieldByName('c_interval_start').AsDateTime;

      aQuery.Close;
      aQuery.SQL.Text := cxQryGetPrevActFragID;
      aQuery.ParamByName('c_prev_IntStart').AsDateTime := xIntervalStart;
      aQuery.Open;
      if aQuery.FindFirst then begin
        aFragID := aQuery.FieldByName('c_fragshift_id').AsInteger;
      end;
    end;
    Result := True;
  except
    on e:Exception do begin
      aError := SetError (ERROR_INVALID_FUNCTION, etDBError, 'GetPrevActIntIDAndFragID failed, probably no intervals or fragshifts on DB available.' + e.message );
      raise Exception.Create ( aError.Msg );
    end;
  end;
end;

//-----------------------------------------------------------------------------
function GetProdGrpData(aQuery: TNativeAdoQuery; aProdID: Longint; var aMachineID: Word; var aSettingsRec: TXMLSettingsRec; var aError: TErrorRec): Boolean;
begin
  Result := False;
  with aQuery do
  try
    Close;
    SQL.Text := cQrySelProdGrp;
    Params.ParamByName('c_prod_id').AsInteger := INTEGER(aProdID);
    Open;
    if FindFirst then begin    // ADO Conform
      aMachineID := FieldByName('c_machine_id').AsInteger;   //Has to be filled when calling InsertDummyProdGrp
      with aSettingsRec do begin
        ProdGrpID       := aProdId;
        OrderPositionID := FieldByName('c_order_position_id').AsInteger;
        OrderID         := FieldByName('c_order_id').AsInteger;
        StyleID         := FieldByName('c_style_id').AsInteger;
        YMSetID         := FieldByName('c_YM_set_id').AsInteger;   //not really used now
        StrPCopy(@ProdName, FieldByName('c_prod_name').AsString);
//wss        c_m_to_prod              := FieldByName('c_m_to_prod').AsFloat;
        Slip            := FieldByName('c_slip').AsInteger;
//wss        c_order_position_prod_id := FieldByName('c_order_position_prod_id').AsInteger;  //not really used now
//        StrPCopy(@aProdGrpData.ProdGrpInfo.c_style_name, FieldByName('c_style_name').AsString);
//        StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, FieldByName('c_order_position_name').AsString);

//Nue:19.4.05        Group           := FieldByName('c_machineGroup').AsInteger;
        Group           := FieldByName('c_machineGroup').AsInteger;    //Wieder rein Nue:3.5.05
        SpindleFirst    := FieldByName('c_spindle_first').AsInteger;;
        SpindleLast     := FieldByName('c_spindle_last').AsInteger;;
        PilotSpindles   := FieldByName('c_pilotSpindles').AsInteger;
        SpeedRamp       := FieldByName('c_speedRamp').AsInteger;
        Speed           := FieldByName('c_speed').AsInteger;
        YarnCnt         := FieldByName('c_act_yarncnt').AsInteger;
        YarnCntUnit     := TYarnUnit(FieldByName('c_yarncnt_unit').AsInteger);
        NrOfThreads     := FieldByName('c_nr_of_threads').AsInteger;
        LengthWindow    := FieldByName('c_lengthWindow').AsInteger;
        LengthMode      := FieldByName('c_lengthMode').AsInteger;
      end; //with
      Result := True;
    end
    else begin
      // Should never happen
      //        WriteLog(etWarning, Couldn't find ProdGrpData for new ProdGrp! Start with SystemStyle))
    end;
  except
    on e:Exception do begin
      aError := SetError (ERROR_INVALID_FUNCTION, etDBError, 'JobHandlerUtil.GetProdGrpInfo failed.' + e.message );
      raise Exception.Create ( aError.Msg );
    end;
  end;

  end;
//function GetProdGrpData(aQuery: TNativeAdoQuery; aProdID: Longint; var aProdGrpData: TProdGrpData; var aError: TErrorRec): Boolean;
//begin
//  Result := False;
//  try
//    with aQuery do begin
//      Close;
//      SQL.Text := cQrySelProdGrp;
//      Params.ParamByName('c_prod_id').AsInteger := INTEGER(aProdID);
//      Open;
//      if not EOF then begin    // ADO Conform
//        with aProdGrpData.ProdGrpInfo do begin
//          c_prod_id := aProdId;
//          OrderPositionID := FieldByName('c_order_position_id').AsInteger;
//          OrderID := FieldByName('c_order_id').AsInteger;
//          MachineID := FieldByName('c_machine_id').AsInteger;   //Has to be filled when calling InsertDummyProdGrp
//          StyleID := FieldByName('c_style_id').AsInteger;
//          c_YM_set_id := FieldByName('c_YM_set_id').AsInteger;   //not really used now
//          StrPCopy(@aProdGrpData.ProdGrpInfo.ProdName, FieldByName('ProdName').AsString);
//          c_m_to_prod := FieldByName('c_m_to_prod').AsFloat;
//          Slip := FieldByName('Slip').AsInteger;
//          c_order_position_prod_id := FieldByName('c_order_position_prod_id').AsInteger;  //not really used now
//          StrPCopy(@aProdGrpData.ProdGrpInfo.c_style_name, FieldByName('c_style_name').AsString);
//          StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, FieldByName('c_order_position_name').AsString);
//        end; //with
//        with aProdGrpData.ProdGrpYMPara do begin
//          Group := FieldByName('c_machineGroup').AsInteger;
//          prodGrpID := aProdId;
//          spdl.start := FieldByName('c_spindle_first').AsInteger;;
//          spdl.stop := FieldByName('c_spindle_last').AsInteger;;
//          PilotSpindles := FieldByName('c_pilotSpindles').AsInteger;
//          SpeedRamp := FieldByName('c_speedRamp').AsInteger;
//          Speed := FieldByName('c_speed').AsInteger;
//          yarnCnt := FieldByName('c_act_yarncnt').AsInteger;
//          YarnCntUnit := FieldByName('c_yarncnt_unit').AsInteger;
//          NrOfThreads := FieldByName('c_nr_of_threads').AsInteger;
//          LengthWindow := FieldByName('c_lengthWindow').AsInteger;
//          LengthMode := FieldByName('c_lengthMode').AsInteger;
//        end; //with
//        Result := True;
//      end
//      else begin
//        // Should never happen
//        //        WriteLog(etWarning, Couldn't find ProdGrpData for new ProdGrp! Start with SystemStyle))
//      end;
//    end
//  except
//    on e:Exception do begin
//      aError := SetError (ERROR_INVALID_FUNCTION, etDBError, 'JobHandlerUtil.GetProdGrpInfo failed.' + e.message );
//      raise Exception.Create ( aError.Msg );
//    end;
//  end;
//end;
//-----------------------------------------------------------------------------
function IsMaOffline(aQuery: TNativeAdoQuery; aMachineID: Word; var aError:
    TErrorRec): Boolean;
begin
  Result := False;
  try
    aQuery.Close;
    aQuery.SQL.Text := cQryMachStateOffline;
    aQuery.Params.ParamByName('c_machine_id' ).AsInteger := aMachineID;
    aQuery.Open;
    if aQuery.FindFirst then
      Result := True;
  except
    on e:Exception do begin
       aError := SetError ( ERROR_INVALID_FUNCTION , etDBError,'IsMaOffline failed.'+ e.message );
       raise Exception.Create ( aError.Msg );
    end;
  end;
end;

//-----------------------------------------------------------------------------

procedure SetDefaultProdGrpData(//var aProdGrpData: TProdGrpData;
                               var aSettingsRec: TXMLSettingsRec;
                               var aColor: Integer );  //Color for representation on floor
begin
  aColor := cDefaultStyleIDColor;
  with aSettingsRec do begin
    ProdGrpID       := 0;
    OrderPositionID := cDefaultOrderPositionID;
    OrderID         := cDefaultOrderID;
    StyleID         := cDefaultStyleID;
    StrPCopy(@ProdName, cDefaultStartName);
//wss    c_m_to_prod        := cDefaultM_to_prod;
    Slip            := cDefaultSlip;
//wss    StrPCopy(@aProdGrpData.ProdGrpInfo.c_style_name, cDefaultStyleName);
//wss    StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, cDefaultOrderPositionName);

    PilotSpindles   := ((SpindleLast - SpindleFirst + 1) DIV 10) + 1;
    SpeedRamp       := cDefaultSpeedRamp;
    Speed           := cDefaultSpeed;
    YarnCnt         := cDefaultYarn_cnt;
    NrOfThreads     := c_DefaultNr_of_threads;
    LengthWindow    := cDefaultLengthWindow;
    LengthMode      := cDefaultLengthMode;
  end; //with
end;

procedure OldSetDefaultProdGrpData(var aProdGrpData: TProdGrpData;
                               var aColor: Integer );  //Color for representation on floor
begin
// TODO -cProdGrpInfo -oNUE: Wenn m�glich eliminieren
  aColor := cDefaultStyleIDColor;
{Has to be filled when calling: c_machine_id, group, spdl.start, spdl.stop, yarnCntUnit
{}
  with aProdGrpData.ProdGrpInfo do begin
    c_prod_id           := 0;
    c_order_position_id := cDefaultOrderPositionID;
    c_order_id          := cDefaultOrderID;
    c_style_id          := cDefaultStyleID;
    c_m_to_prod         := cDefaultM_to_prod;
    c_slip              := cDefaultSlip;
    StrPCopy(@aProdGrpData.ProdGrpInfo.c_prod_name, cDefaultStartName);
    StrPCopy(@aProdGrpData.ProdGrpInfo.c_style_name, cDefaultStyleName);
    StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, cDefaultOrderPositionName);
  end; //with

//NueTmp
//  with aProdGrpData.ProdGrpYMPara do begin
//// TODO -cProdGrpInfo -oNUE: prodGrpID kann eliminiert werden.
//    prodGrpID     := 0;
//    pilotSpindles := ((spdl.stop+1-spdl.start) DIV 10)+1;
//    speedRamp     := cDefaultSpeedRamp;
//    speed         := cDefaultSpeed;
//    yarnCnt       := cDefaultYarn_cnt;
//    nrOfThreads   := c_DefaultNr_of_threads;
//    lengthWindow  := cDefaultLengthWindow;
//    lengthMode    := cDefaultLengthMode;
//    // Unused fields: has to be filled when calling
//  end; //with
end;


//-----------------------------------------------------------------------------

function InsertDummyProdGrp(aQuery: TNativeAdoQuery; aMachineID, aSpindleFirst, aSpindleLast: Word; aYarnCntUnit: TYarnUnit;
           aOldProdId: Integer; aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
//           var aProdGrpData: TProdGrpData;
           var aSettingsRec: TXMLSettingsRec;
           var aColor: Integer;   //Color for representation on floor
           var aYMSetName: TText50;
           var aError: TErrorRec): Integer;
var
  xSettingsRec: TXMLSettingsRec;
  xColor: Integer;
  xYMSetName,
  xStyleName,
  xOrderPositionName: string;
  xFromZE : Boolean;
begin
  try
    with aSettingsRec do begin
      //Fill in the always relevant fields
      SpindleFirst  := aSpindleFirst;
      SpindleLast   := aSpindleLast;
      YarnCntUnit   := aYarnCntUnit;

      case aStartMode of
        smUndefined: begin   //Start from Undefined Spindleranges
            SetDefaultProdGrpData(aSettingsRec, aColor);
          end;
        smZE: begin
            // tempor�re Kopie
            xSettingsRec := aSettingsRec;
            xColor       := aColor;
            SetDefaultProdGrpData(aSettingsRec, aColor);
            // und spezifische Infos wieder �bernehmen
            ProdName := xSettingsRec.ProdName;
            Slip     := xSettingsRec.Slip;

            codesite.SendFmtMsg('InsertDummyProdGrp1: ProdName %s; Slip %d; aColor %d; OrderID %d; OrderPositionID %d; StyleID %d ',
                                [StrPas(@ProdName), Slip, aColor, OrderID, OrderPositionID, StyleID]);

  //            aQuery.Close;
  //            aQuery.SQL.Text := 'select YMSetName from t_ym_settings where c_ym_set_id=:c_ym_set_id';
  //            aQuery.Params.ParamByName('c_ym_set_id').AsInteger := aProdGrpData.ProdGrpInfo.c_YM_set_id;
  //            aQuery.Open;
  //            if not aQuery.EOF then
  //              xYMSetName := aQuery.FieldByName('YMSetName').AsString
  //            else
  //              xYMSetName := '';
  //            StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));

            xFromZE    := (YMSetID = 0);
            aYMSetName := YMSetName;

            aQuery.Close;
//            aQuery.SQL.Text := cQryGetProdID;
            //Nue:12.06.08: Hier wird neu der Status "stopping" ausgeklammert damit in der Situation, in der auf der LZE ein Assign passiert
            //  (=> Gruppe geht in "stopping") und danach sofort der Schl�sselschalter (EntryLocked) bet�tigt wird, die StyleProdGrpInfo-Daten nicht
            //  von der gestoppten Gruppe �bernommen werden.
            aQuery.SQL.Text := cQryGetProdID3;
            aQuery.Params.ParamByName('c_machine_id').AsInteger    := aMachineID;
            aQuery.Params.ParamByName('c_spindle_first').AsInteger := aSpindleFirst;
            aQuery.Params.ParamByName('c_spindle_last').AsInteger  := aSpindleLast;
            aQuery.Open;
            // Hole Informationen von der aktuell laufenen Partie
//wss            if not aQuery.EOF then begin
              codesite.SendFmtMsg('InsertDummyProdGrp1a: ProdName %s; Slip %d; aColor %d; YMSetName %s; OrderID %d; OrderPositionID %d; MachId:%d; SpindFirst:%d; SpindLast:%d; StyleID von LZE:%d',
                                  [StrPas(@ProdName),Slip, aColor, xYMSetName, OrderID, OrderPositionID,aMachineID,aSpindleFirst,aSpindleLast, xSettingsRec.StyleID ]);
            if aQuery.FindFirst then begin
              aColor     := aQuery.FieldByName('c_color').AsInteger;  //Color for representation on floor
              aOldProdId := aQuery.FieldByName('c_prod_id').AsInteger;
              if NOT GetProdGrpData(aQuery, aOldProdId, aMachineID, aSettingsRec, aError) then
                SetDefaultProdGrpData(aSettingsRec, aColor)
              else if xFromZE then begin
               //Changed by ZE no Name is available; Get Name from previous prodgroup
                aQuery.Close;
                aQuery.SQL.Text := cQryGetYMSetName;
                aQuery.Params.ParamByName('c_YM_set_id').AsInteger := YMSetID;
                aQuery.Open;
                if aQuery.FindFirst then  // ADO Conform
                  xYMSetName := aQuery.FieldByName('c_ym_set_name').AsString
                else
                  xYMSetName := '';
                StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));
              end;

              codesite.SendFmtMsg('InsertDummyProdGrp2a: ProdName %s; Slip %d; aColor %d; YMSetName %s; OrderID %d; OrderPositionID %d ',
                                  [StrPas(@ProdName),Slip, aColor, xYMSetName, OrderID, OrderPositionID]);
            end
            // keine Partie vorhanden -> Spindelbereich war nicht in Produktion
            else begin
              //Start from WSC or ZE
              aQuery.Close;
              aQuery.SQL.Text := cQrySelOrderPosition;
              aQuery.Params.ParamByName('c_order_position_id').AsInteger := OrderPositionID;
              aQuery.Params.ParamByName('c_order_id').AsInteger          := OrderID;
              aQuery.Open;
              // Informationen �ber diese "Auftragsposition" ermitteln (wss: ist ja eigentlich immer ID 1)
              if aQuery.FindFirst then begin  //Entry on DB: Modify values with values from machine  (ADO Conform)
                aQuery.Close;
                aQuery.SQL.Text := cQrySelStyle;
                aQuery.Params.ParamByName('c_style_id').AsInteger := StyleID;
                aQuery.Open; // ADO Conform
                if aQuery.FindFirst then begin  //Entry on DB: Modify values with values from machine
                  aColor := xColor;

                  xYMSetName := StrPas(@YMSetName);
                  if (xYMSetName <> '') and (AnsiPos(cX1, xYMSetName) = 0) then begin
                    xYMSetName := xYMSetName+cX1;
                    StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));
                  end;

                  if xSettingsRec.StyleID>=cDefaultStyleID then begin
                    StyleID := xSettingsRec.StyleID;   //Nue 13.12.01
                  end
                  else begin  //Kann vorkommen auf der ZE, wenn MM offline ist, Settings an der ZE ver�ndert werden, und danach MM wieder online kommt (bei MaEqualize)
                    codesite.SendFmtMsg('InsertDummyProdGrp: StyleID %d does not exists on DB -> took over DefaultStyleID.', [xSettingsRec.StyleID]);
                    //StyleID := cDefaultStyleID; //wurde oben schon gesetzt
                  end;

                  OrderID := xSettingsRec.OrderID;
                  OrderPositionID := xSettingsRec.OrderPositionID;
                  codesite.SendFmtMsg('InsertDummyProdGrp2b: Values OrderID %d; OrderPositionID %d; StyleID %d exists on DB -> took over from ZE(WSC) record.',
                                      [xSettingsRec.OrderID, xSettingsRec.OrderPositionID, xSettingsRec.StyleID]);
                end; // if aQuery.FindFirst
              end;

            end;

          end;
        smRange: begin
            if aOldProdId <> 0 then begin //Start from MM
              if GetProdGrpData(aQuery, aOldProdId, aMachineID, aSettingsRec, aError) then
                aColor := cDefaultStyleIDColor  //Nue:25.2.04 Damit die alte, nicht wirklich ver�nderte Gruppe, aber wegen RangeChange trotzdem neu gestartete Gruppe auch cDefaultStyleIDColor erh�lt
              else
                SetDefaultProdGrpData(aSettingsRec, aColor);
            end else  //Start Range from ZE ??
              SetDefaultProdGrpData(aSettingsRec, aColor);
            xSettingsRec := aSettingsRec;
          end;
        smMMStop, smZEStop: begin
            SetDefaultProdGrpData(aSettingsRec, aColor);
          end;
        smMM: begin
            //Nothing to do; is filled with data from GUI
          end;
      else
      end; //case

      //Get StyleName
      aQuery.SQL.Text := cQrySelStyle;  //Holen Style Data
      aQuery.Params.ParamByName('c_style_id').AsInteger := StyleID;
      aQuery.Open;
      if aQuery.FindFirst then
        xStyleName := aQuery.FieldByName('c_style_name').AsString
      else
        xStyleName := '';

      //Get OrderPositionName
      aQuery.SQL.Text := cQrySelOrderPosition;  //Holen OrderPosition Data
      aQuery.Params.ParamByName('c_order_position_id').AsInteger  := OrderPositionID;
      aQuery.Params.ParamByName('c_order_id').AsInteger           := OrderID;
      aQuery.Open;
      if aQuery.FindFirst then  // ADO Conform
        xOrderPositionName := aQuery.FieldByName('c_order_position_name').AsString
      else
        xOrderPositionName := '';

      codesite.SendFmtMsg('InsertDummyProdGrp3: ProdName %s; Slip %d; aColor %d; OrderID %d; OrderPositionID %d ',
             [StrPas(@ProdName), Slip, aColor, xSettingsRec.OrderID, xSettingsRec.OrderPositionID]);

      aQuery.SQL.Text := cQryInsDummyProdGrp;  //Insert t_prodgroup entry
      aQuery.Params.ParamByName('c_machine_id').AsInteger       := aMachineID;
      aQuery.Params.ParamByName('c_order_position_id').AsInteger := OrderPositionID;
      aQuery.Params.ParamByName('c_order_id').AsInteger         := OrderID;
      aQuery.Params.ParamByName('c_style_id').AsInteger         := StyleID;

      aQuery.Params.ParamByName('c_prod_name').AsString      := ProdName;
      aQuery.Params.ParamByName('c_spindle_first').AsInteger := aSpindleFirst;
      aQuery.Params.ParamByName('c_spindle_last').AsInteger  := aSpindleLast;

      aQuery.Params.ParamByName('c_act_yarncnt').AsFloat     := YarnCountConvert(TYarnUnit(YarnCntUnit), yuNm, YarnCnt); //27.9.04 nue: No more Div cYarnCntFactor
      aQuery.Params.ParamByName('c_yarncnt_unit').AsInteger  := Ord(YarnCntUnit);
      aQuery.Params.ParamByName('c_nr_of_threads').AsInteger := NrOfThreads;

      aQuery.Params.ParamByName('c_slip').AsInteger          := Slip;
      aQuery.Params.ParamByName('c_prod_state').AsInteger    := ORD(aProdGrpState);
      aQuery.Params.ParamByName('nowdate').AsDateTime        := Now;     //Nue:6.11.02
      aQuery.Params.ParamByName('nowdate').ADOType           := adDate; //Nue:10.8.05
      aQuery.Params.ParamByName('c_start_mode').AsInteger    := ORD(aStartMode);

      aQuery.Params.ParamByName('c_style_name').AsString          := xStyleName;
      aQuery.Params.ParamByName('c_order_position_name').AsString := xOrderPositionName;

      aQuery.Params.ParamByName('c_machineGroup').AsInteger  := Group;
      aQuery.Params.ParamByName('c_pilotSpindles').AsInteger := PilotSpindles;
      aQuery.Params.ParamByName('c_speedRamp').AsInteger     := SpeedRamp;
      aQuery.Params.ParamByName('c_speed').AsInteger         := Speed;
      aQuery.Params.ParamByName('c_lengthMode').AsInteger    := LengthMode;
      aQuery.Params.ParamByName('c_lengthWindow').AsInteger  := LengthWindow;

      ProdGrpID := aQuery.InsertSQL('t_prodgroup','c_prod_id', cMaxProdID, cMinProdID);
      Result    := ProdGrpID;
    end; //with
 except
   on e:Exception do begin
      aError := SetError ( ERROR_INVALID_FUNCTION , etDBError,'JobHandlerUtil.InsertDummyProdGrp failed.'+ e.message );
      raise Exception.Create ( aError.Msg );
   end;
 end;
end;
//function InsertDummyProdGrp(aQuery: TNativeAdoQuery; aMachineID, aSpindleFirst, aSpindleLast: Word; aYarnCntUnit: TYarnUnit;
//           aOldProdId: Integer; aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
////           var aProdGrpData: TProdGrpData;
//           var aSettingsRec: TXMLSettingsRec;
//           var aColor: Integer;   //Color for representation on floor
//           var aYMSetName: TText50;
//           var aError: TErrorRec): Integer;
//var
////  xProdGrpData: TProdGrpData;
//  xSettingsRec: TXMLSettingsRec;
//  xColor: Integer;
//  xYMSetName,
//  xStyleName,
//  xOrderPositionName: string;
//  xFromZE : Boolean;
//begin
//  Result := 0;
//  try
////    with aProdGrpData do begin
//    with aSettingsRec do begin
//      //Fill in the always relevant fields
////      ProdGrpInfo.MachineID  := aProdGrpData.ProdGrpInfo.MachineID;
////      ProdGrpYMPara.Group       := aProdGrpData.ProdGrpYMPara.Group;
//      SpindleFirst  := aSpindleFirst;
//      SpindleLast   := aSpindleLast;
//      YarnCntUnit   := aYarnCntUnit;
//
//      case aStartMode of
//        smUndefined: begin   //Start from Undefined Spindleranges
//            SetDefaultProdGrpData(aSettingsRec, aColor);
////            SetDefaultProdGrpData({var}aProdGrpData, {var}aColor);
//          end;
//        smZE: begin
//            xSettingsRec := aSettingsRec;
////            xProdGrpData := aProdGrpData;
//            xColor       := aColor;
//            SetDefaultProdGrpData(aSettingsRec, aColor);
////            SetDefaultProdGrpData({var}aProdGrpData, {var}aColor);
//            //ProdGrpName and slip will always taken over from the machine
//            aSettingsRec.ProdName := xSettingsRec.ProdName;
//            aSettingsRec.Slip     := xSettingsRec.Slip;
//
//            codesite.SendFmtMsg('InsertDummyProdGrp1: ProdName %s; Slip %d; aColor %d; OrderID %d; OrderPositionID %d; StyleID %d ',
//                                [StrPas(@ProdName), Slip, aColor, OrderID, OrderPositionID, StyleID]);
//
//  //            aQuery.Close;
//  //            aQuery.SQL.Text := 'select YMSetName from t_ym_settings where c_ym_set_id=:c_ym_set_id';
//  //            aQuery.Params.ParamByName('c_ym_set_id').AsInteger := aProdGrpData.ProdGrpInfo.c_YM_set_id;
//  //            aQuery.Open;
//  //            if not aQuery.EOF then
//  //              xYMSetName := aQuery.FieldByName('YMSetName').AsString
//  //            else
//  //              xYMSetName := '';
//  //            StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));
//
//            xFromZE    := (YMSetID = 0);
//            aYMSetName := YMSetName;
//
//            aQuery.Close;
//            aQuery.SQL.Text := cQryGetProdID;
//            aQuery.Params.ParamByName('c_machine_id').AsInteger    := aMachineID;
//            aQuery.Params.ParamByName('c_spindle_first').AsInteger := aSpindleFirst;
//            aQuery.Params.ParamByName('c_spindle_last').AsInteger  := aSpindleLast;
//            aQuery.Open;
//            if not aQuery.EOF then begin  // ADO Conform
//              aColor     := aQuery.FieldByName('c_color').AsInteger;  //Color for representation on floor
//              aOldProdId := aQuery.FieldByName('c_prod_id').AsInteger;
//              if NOT GetProdGrpData(aQuery, aOldProdId, aMachineID, aSettingsRec, aError) then
//                SetDefaultProdGrpData(aSettingsRec, aColor)
//              else if xFromZE then begin
//               //Changed by ZE no Name is available; Get Name from previous prodgroup
//                aQuery.Close;
//                aQuery.SQL.Text := cQryGetYMSetName;
//                aQuery.Params.ParamByName('c_YM_set_id').AsInteger := YMSetID;
//                aQuery.Open;
//                if aQuery.FindFirst then  // ADO Conform
//                  xYMSetName := aQuery.FieldByName('c_ym_set_name').AsString
//                else
//                  xYMSetName := '';
//                StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));
//              end;
//
//            codesite.SendFmtMsg('InsertDummyProdGrp2a: ProdName %s; Slip %d; aColor %d; YMSetName %s; OrderID %d; OrderPositionID %d ',
//                                [StrPas(@ProdName),Slip, aColor, xYMSetName, OrderID, OrderPositionID]);
//            end
//            else begin
//              //Start from WSC or ZE
//              aQuery.Close;
//              aQuery.SQL.Text := cQrySelOrderPosition;
//              aQuery.Params.ParamByName('c_order_id').AsInteger := OrderID;
//              aQuery.Params.ParamByName('c_order_position_id').AsInteger := OrderPositionID;
//              aQuery.Open;
//              if aQuery.FindFirst then begin  //Entry on DB: Modify values with values from machine  (ADO Conform)
//                aQuery.Close;
//                aQuery.SQL.Text := cQrySelStyle;
//                aQuery.Params.ParamByName('c_style_id').AsInteger := StyleID;
//                aQuery.Open; // ADO Conform
//                if aQuery.FindFirst then begin  //Entry on DB: Modify values with values from machine
//                  aColor := xColor;
//
//                  xYMSetName := StrPas(@YMSetName);
//                  if (xYMSetName <> '') and (AnsiPos(cX1, xYMSetName) = 0) then begin
//                    xYMSetName := xYMSetName+cX1;
//                    StrPLCopy(@aYMSetName, xYMSetName, sizeOf(aYMSetName));
//                  end;
//
//                  StyleID := xSettingsRec.StyleID;   //Nue 13.12.01
////wss                  StrPLCopy(@aProdGrpData.ProdGrpInfo.c_style_name, aQuery.FieldByName('c_style_name').AsString, sizeOf(aProdGrpData.ProdGrpInfo.c_style_name));//Nue 17.12.01
//                  OrderID := xSettingsRec.OrderID;
//                  OrderPositionID := xSettingsRec.OrderPositionID;
////wss                  StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, xProdGrpData.ProdGrpInfo.c_order_position_name);
//                  codesite.SendFmtMsg('InsertDummyProdGrp2b: Values OrderID %d; '+
//                    'OrderPositionID %d; StyleID %d exists on DB -> took over from ZE(WSC) record.',
//                     [xSettingsRec.OrderID, xSettingsRec.OrderPositionID, xSettingsRec.StyleID]);
//                end; // if aQuery.FindFirst
//              end;
//
//            end;
//
//          end;
//        smRange: begin
//            if aOldProdId <> 0 then  //Start from MM
//              if NOT GetProdGrpData(aQuery, aOldProdId, aMachineID, aSettingsRec, aError) then
//                SetDefaultProdGrpData(aSettingsRec, aColor)
//              else
//                aColor := cDefaultStyleIDColor  //Nue:25.2.04 Damit die alte, nicht wirklich ver�nderte Gruppe, aber wegen RangeChange trotzdem neu gestartete Gruppe auch cDefaultStyleIDColor erh�lt
//            else  //Start Range from ZE ??
//              SetDefaultProdGrpData(aSettingsRec, aColor);
//          end;
//        smMMStop, smZEStop: begin
//            SetDefaultProdGrpData(aSettingsRec, aColor);
//          end;
//        smMM: begin
//            //Nothing to do; is filled with data from GUI
//          end;
//      else
//      end; //case
//
//      //Get StyleName
//      aQuery.SQL.Text := cQrySelStyle;  //Holen Style Data
//      aQuery.Params.ParamByName('c_style_id').AsInteger    := StyleID;
//      aQuery.Open;
//      if aQuery.FindFirst then  // ADO Conform
//        xStyleName := aQuery.FieldByName('c_style_name').AsString
//      else
//        xStyleName := '';
//
//      //Get OrderPositionName
//      aQuery.SQL.Text := cQrySelOrderPosition;  //Holen OrderPosition Data
//      aQuery.Params.ParamByName('c_order_position_id').AsInteger  := OrderPositionID;
//      aQuery.Params.ParamByName('c_order_id').AsInteger           := OrderID;
//      aQuery.Open;
//      if aQuery.FindFirst then  // ADO Conform
//        xOrderPositionName := aQuery.FieldByName('c_order_position_name').AsString
//      else
//        xOrderPositionName := '';
//
//      codesite.SendFmtMsg('InsertDummyProdGrp3: ProdName %s; Slip %d; aColor %d; OrderID %d; OrderPositionID %d ',
//             [StrPas(@ProdName), Slip, aColor, xSettingsRec.OrderID, xSettingsRec.OrderPositionID]);
//
//      aQuery.SQL.Text := cQryInsDummyProdGrp;  //Insert t_prodgroup entry
//      aQuery.Params.ParamByName('c_machine_id').AsInteger       := aMachineID;
//      aQuery.Params.ParamByName('c_spindle_id').AsInteger    := aSpindleFirst;
//
//      aQuery.Params.ParamByName('c_order_position_id').AsInteger := OrderPositionID;
//      aQuery.Params.ParamByName('c_order_id').AsInteger         := OrderID;
//      aQuery.Params.ParamByName('c_style_id').AsInteger         := StyleID;
//
//      aQuery.Params.ParamByName('c_prod_name').AsString         := ProdName;
//      aQuery.Params.ParamByName('c_spindle_first').AsInteger := aSpindleFirst;
//      aQuery.Params.ParamByName('c_spindle_last').AsInteger  := aSpindleLast;
////wss      aQuery.Params.ParamByName('c_m_to_prod').AsFloat            := ProdGrpInfo.c_m_to_prod;
//
//      aQuery.Params.ParamByName('c_act_yarncnt').AsFloat     := YarnCountConvert(TYarnUnit(YarnCntUnit), yuNm, YarnCnt); //27.9.04 nue: No more Div cYarnCntFactor
//      aQuery.Params.ParamByName('c_yarncnt_unit').AsInteger  := Ord(YarnCntUnit);
//      aQuery.Params.ParamByName('c_nr_of_threads').AsInteger := NrOfThreads;
//
//      aQuery.Params.ParamByName('c_slip').AsInteger            := Slip;
//      aQuery.Params.ParamByName('c_prod_state').AsInteger    := ORD(aProdGrpState);
//      aQuery.Params.ParamByName('nowdate').AsDateTime        := Now;     //Nue:6.11.02
//      aQuery.Params.ParamByName('c_start_mode').AsInteger    := ORD(aStartMode);
//{ DONE -oNue -cSystem : Richtige Werte f�r c_style_name und c_order_position_name einf�gen }
//      aQuery.Params.ParamByName('c_style_name').AsString          := xStyleName;
//      aQuery.Params.ParamByName('c_order_position_name').AsString := xOrderPositionName;
//
//      aQuery.Params.ParamByName('c_machineGroup').AsInteger  := Group;
//      aQuery.Params.ParamByName('c_pilotSpindles').AsInteger := PilotSpindles;
//      aQuery.Params.ParamByName('c_speedRamp').AsInteger     := SpeedRamp;
//      aQuery.Params.ParamByName('c_speed').AsInteger         := Speed;
//      aQuery.Params.ParamByName('c_lengthMode').AsInteger    := LengthMode;
//      aQuery.Params.ParamByName('c_lengthWindow').AsInteger  := LengthWindow;
//
//      ProdGrpID := aQuery.InsertSQL('t_prodgroup','c_prod_id', cMaxProdID, cMinProdID);
//      Result    := ProdGrpID;
//    end; //with
// except
//   on e:Exception do begin
//      aError := SetError ( ERROR_INVALID_FUNCTION , etDBError,'JobHandlerUtil.InsertDummyProdGrp failed.'+ e.message );
//      raise Exception.Create ( aError.Msg );
//   end;
// end;
//end;
//-----------------------------------------------------------------------------

procedure PrepareSendMsgToAppl(aMsgTyp: TResponseMsgTyp; aMachineID, aSpindleFirst, aSpindleLast: Word; aComputerName: TString30; aPort : TString30;
                               var aJobRec: TJobRec);
begin
  with aJobRec, SendMsgToAppl do begin
    JobTyp       := jtSendMsgToAppl;
    ComputerName := aComputerName;
    Port         := aPort;
    ResponseMsg.MsgTyp       := aMsgTyp;
    ResponseMsg.MachineID    := aMachineID;
    ResponseMsg.SpindleFirst := aSpindleFirst;
    ResponseMsg.SpindleLast  := aSpindleLast;
    ResponseMsg.ProdGrpID    := 0;
  end;
end;

//function GetProdGrpData(aQuery: TNativeAdoQuery; aProdID: Longint; var aProdGrpData: TProdGrpData; var aError: TErrorRec): Boolean;
//begin
//  Result := False;
//  try
//    with aQuery do begin
//      Close;
//      SQL.Text := cQrySelProdGrp;
//      Params.ParamByName('c_prod_id').AsInteger := INTEGER(aProdID);
//      Open;
//      if not EOF then begin    // ADO Conform
//        with aProdGrpData.ProdGrpInfo do begin
//          c_prod_id := aProdId;
//          OrderPositionID := FieldByName('c_order_position_id').AsInteger;
//          OrderID := FieldByName('c_order_id').AsInteger;
//          MachineID := FieldByName('c_machine_id').AsInteger;   //Has to be filled when calling InsertDummyProdGrp
//          StyleID := FieldByName('c_style_id').AsInteger;
//          c_YM_set_id := FieldByName('c_YM_set_id').AsInteger;   //not really used now
//          StrPCopy(@aProdGrpData.ProdGrpInfo.ProdName, FieldByName('ProdName').AsString);
//          c_m_to_prod := FieldByName('c_m_to_prod').AsFloat;
//          Slip := FieldByName('Slip').AsInteger;
//          c_order_position_prod_id := FieldByName('c_order_position_prod_id').AsInteger;  //not really used now
//          StrPCopy(@aProdGrpData.ProdGrpInfo.c_style_name, FieldByName('c_style_name').AsString);
//          StrPCopy(@aProdGrpData.ProdGrpInfo.c_order_position_name, FieldByName('c_order_position_name').AsString);
//        end; //with
//        with aProdGrpData.ProdGrpYMPara do begin
//          Group := FieldByName('c_machineGroup').AsInteger;
//          prodGrpID := aProdId;
//          spdl.start := FieldByName('c_spindle_first').AsInteger;;
//          spdl.stop := FieldByName('c_spindle_last').AsInteger;;
//          PilotSpindles := FieldByName('c_pilotSpindles').AsInteger;
//          SpeedRamp := FieldByName('c_speedRamp').AsInteger;
//          Speed := FieldByName('c_speed').AsInteger;
//          yarnCnt := FieldByName('c_act_yarncnt').AsInteger;
//          YarnCntUnit := FieldByName('c_yarncnt_unit').AsInteger;
//          NrOfThreads := FieldByName('c_nr_of_threads').AsInteger;
//          LengthWindow := FieldByName('c_lengthWindow').AsInteger;
//          LengthMode := FieldByName('c_lengthMode').AsInteger;
//        end; //with
//        Result := True;
//      end
//      else begin
//        // Should never happen
//        //        WriteLog(etWarning, Couldn't find ProdGrpData for new ProdGrp! Start with SystemStyle))
//      end;
//    end
//  except
//    on e:Exception do begin
//      aError := SetError (ERROR_INVALID_FUNCTION, etDBError, 'JobHandlerUtil.GetProdGrpInfo failed.' + e.message );
//      raise Exception.Create ( aError.Msg );
//    end;
//  end;
//end;
//-----------------------------------------------------------------------------
function StopOldProdGrp(aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
    aProdID: Longint; var aError: TErrorRec): Boolean;
begin
  Result := True;
  try
    aQuery.Close;
    aDataBase.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
    aQuery.SQL.Text := cQryStopProdGrp;
    aQuery.Params.ParamByName('c_prod_state').AsInteger := ORD(psStopped); //4=Stopped
    aQuery.Params.ParamByName('c_prod_end').AsDateTime := Now;
    aQuery.Params.ParamByName('c_prod_id').AsInteger := aProdID;
    aQuery.Params.ParamByName('c_prod_state' ).AsInteger := ORD(psStopped); //4=Stopped
    aQuery.Params.ParamByName('c_prod_id' ).AsInteger := aProdID;
    aQuery.ExecSQL;
    aDataBase.Commit;
  except
    on e:Exception do begin
       aError := SetError ( ERROR_INVALID_FUNCTION , etDBError,'StopOldProdGrp failed.'+ e.message );
       aDataBase.Rollback;
       raise Exception.Create ( aError.Msg );
    end;
  end;
end;

//-----------------------------------------------------------------------------
end.
