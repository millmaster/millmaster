{$A+,B-,C+,D+,E-,F-,G+,H+,I+,J+,K-,L+,M-,N+,O-,P+,Q-,R-,S-,T-,U-,V+,W+,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE CONSOLE}
(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobManagerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system:
 Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 26.10.1998  1.00  Wss | Ableitung auf Baseklasse umgestellt
| 01.03.2000  1.01  Nue | jtProdGrpUpdate implemented.
| 06.06.2000  1.01  Nue | GetMaAssign renamed to MaEqualize. Old GetMaAssign only for getting Ma assigns used.
| 11.07.2000  1.02  Nue | New Job added: jtDBDump added.
| 07.08.2000  1.03  Nue | SameMachineState asking bei MaOffline or MaOnline added again.
| 27.11.2000  1.04  Nue | TMachType changed to TAWEMachType. (YMParaDef); Job GenQOfflimit added.
| 25.03.2001  1.28  Nue | Fields ShiftcalID,DefaultShiftcal and ShiftInDay in
|                       | TDataAquEv and TGenQOfflimit added.
| 09.04.2001  1.29  Nue | 'c_node_id not like ''0'' AND ' in SameMachineState because of consistency
|                       | with Maconfig (Connect/Disconnect Maschine/Node.
| 07.06.2001  1.30  Nue | jtBroadcastToClients and amSystemInfo added.
| 11.06.2001  1.31  Nue | Modifications because of DataCollection-Handling.
| 21.06.2001  1.32  Nue | 2.Trial for jobs StartGrpEv and StopGrpEv no more done.
| 26.06.2001  1.33  Nue | Additional Modifications because of DataCollection-Handling.
| 10.07.2001  1.34  Nue | SendEventToClients moved to correct calling place.
| 03.10.2001  1.35  Nue | YM_Set-Name and YMSetChanged handling added.
| 22.11.2001  1.36  Nue | In jtStopGrpEvFromMa section with jtMaEqualize added.
| 12.03.2002  1.37  Nue | Log message in ProcAssignFromZE modified and new etInformation instead etError.
|                       | SpindleFirst and SpindleLast set (not 0) in GenExpShiftData and GenShiftData
| 19.03.2002  1.38  Nue | Repetitions for GetZeReady-Job from 0 to 2.
| 30.04.2002  1.39  Nue | Handling in ProcMaOnline if machine wasn't uploaded before.(c_AWE_mach_type=ORD(amtUnknown)).
| 13.09.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 31.10.2002  1.40  Nue | TGetZEReadyJob no further used.
|                       | (because cold start problems with Murata Inside) No restart MM if no JobId found for MaEqualize.
|                       | No restart MM if no JobId found for MaEqualize.
| 15.11.2002  1.41  Wss | Enumeration amMaAssChanged -> amMachineStateChange
| 18.11.2002  1.42  Nue | Changes because of changing c_machine_state from t_prodgroup_state to t_machine_state.
| 02.12.2002  1.43  Nue | jtBroadcastToClients handled in ProcessJobInit, that no error happens in Eevntlog.
| 24.02.2003  1.44  Nue | "if (FieldByName('c_machine_state').AsInteger <= ORD(msUndefined))...." added in SameMachineState.
|             1.44  Nue | InfoMessage if no productiongroup is in production at data aquisation time.
| 27.02.2003  1.44  Wss | TJobManager.Startup: Wenn f�r ein Netztyp keine Maschinen auf DB definiert
                          sind, dann muss eine leere Nodelist trotzdem an jeden Hanlder verschickt werden,
                          da dieser sonst nicht aus dem Init Status kommt und somit der MMGuard dies als
                          Timeout definiert.
| 18.03.2003  1.44  Nue | Unterbinden ung�ltige aMachineID  added in SameMachineState.
| 20.11.2003  1.45  Nue | Nur noch EINE OFFLINE Meldung pro Maschine bei Datenaquisation.
| 26.02.2004  1.46  Nue | {xColor}xDummyColor in CheckAdditionalProdGrpToRestart, dass bestehende
                          (nur verkleinerte Gruppe) mit hellgelb gestartet wird. (Nicht mit Farbe des Vorg�ngers)
|=========================================================================================*)
unit JobManagerClass;
{ TODO 1 -oNue -cSystem : Setzen der richtigen Maschinenstati in t_net_node und t_prodgroup, beim aufstarten des Systems }
interface

uses
  SysUtils, Windows, LoepfeGlobal, JobHandlerUtil, YMParaDef, {YMParaDBAccess,}
  mmEventLog, MMBaseClasses, BaseGlobal, MMUGlobal, JobListClasses,
  AdoDBAccess,
  IPCClass, SettingsReader, syncobjs, XMLDef;

type
  TJobManager = class;

  TBuildJobQueue = class(TObject)
  private
    mAnchorJob: TBaseJob;
    mBaseJob1: TBaseJob;
    mExpSpdData, mExpShiftData: Boolean;
    mDatabase: TAdoDBAccessDB;
    mOldIntID: Byte;
    mOldFragID: Longint;
    mIntID: Byte;
    mFragID: Longint;
    mShiftcalID: Byte;
    mShiftEv: Boolean;
    mShiftInDay: Byte;
    fQuery: TNativeAdoQuery;
    fQuery2: TNativeAdoQuery;
    mOwner: TJobManager;
    fSuccessorJob: DWord;
    mQueueType: TJobTyp;
    mDummyProdID: Longint;
    mProdID: Longint;
    mComputerName: TString30;
    mPort: TString30;
    mMachineID: Word;
    mSpindleFirst: Word;
    mSpindleLast: Word;
    mMachineGrp: Word;
    mParams: TMMSettingsReader;
    function Prepare(aJob: TBaseJob; aAnchor: Boolean; aLast: Boolean;
      var aListID: DWord; aJobState: TJobState = jsValid): Boolean;
    function SetExp: Boolean;
  protected
    function SendEventToClients(aAnchor: Boolean; aLast: Boolean; aEvent: TApplMsg;
      aJobState: TJobState = jsValid): DWord;
    function UnDoGetZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
    function GetZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
    function DelZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
    function FindFirst: Boolean;
    function EOF: Boolean;
    function GenShiftData(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState = jsValid): DWord;
    function GenSpdOfflimits(aAnchor: Boolean; aLast: Boolean): Boolean;
    function GenDwData(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState = jsValid): DWord;
    function DBDump(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState = jsValid): DWord;
    function GenQOfflimit(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState = jsValid): DWord;
    function Next: Boolean;
    function Prior: Boolean;
    function SetNetTyp: Boolean;
    property Query: TNativeAdoQuery read fQuery write fQuery;
    property Query2: TNativeAdoQuery read fQuery2 write fQuery2;
    property SuccessorJob: DWord read fSuccessorJob write fSuccessorJob;

  public
    constructor Create(aQueueType: TJobTyp; aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery;
      aParams: TMMSettingsReader);
    destructor Destroy; override;
  end;

  TBuildJobQueueDataAqu = class(TBuildJobQueue)
  protected
    constructor Create(aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery; aParams: TMMSettingsReader;
      aOldIntID: Byte; aOldFragID: Longint; aIntID: Byte; aFragID: Longint;
      aShiftcalID: Byte; aShiftEv: Boolean; aShiftInDay: Byte); //Added on 25.03.01 Nue
  end;

  TBuildJobQueueStartStop = class(TBuildJobQueue)
  private
    mStartGrpEv: PJobRec;
    mYMSetID: Integer;
    fMachineOffline: Boolean;
    fDataCollection: Boolean;
    mNetType: TNetTyp;
    function SetSettings(aMachineID, aSpindleFirst, aSpindleLast: Word;
        aDummyProdID: Longint; aAnchor: Boolean; aLast: Boolean; aAssignment: Byte;
        aColor: Integer; aYMSetName: TText50; aYMSetChanged: Boolean; aStartMode:
        TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState =
        jsValid; aYMSetID: Integer = 1; aProdIDOrgRange: Longint = 0): DWord;
        overload;
    function SetSettingsWss(aMachineID, aSpindleFirst, aSpindleLast: Word;
        aDummyProdID: Longint; aAnchor: Boolean; aLast: Boolean; aAssignment: Byte;
        aColor: Integer; aYMSetName: TText50; aYMSetChanged: Boolean; aStartMode:
        TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState =
        jsValid; aYMSetID: Integer = 1; aProdIDOrgRange: Longint = 0): DWord;
  protected
    property MachineOffline: Boolean read fMachineOffline;
    property DataCollection: Boolean read fDataCollection; //Nue: 12.06.01
    function CheckAdditionalProdGrpToRestart(aAnchor: Boolean; aLast: Boolean; aAssignment: Byte; //cAssignGroup, cAssigProdIDOnly
      aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState = jsValid): DWord;
    function GetDataStopZESpd(aAnchor: Boolean; aLast: Boolean): Boolean;
    function SendMsgToAppl(aAnchor: Boolean; aLast: Boolean; aMsg: TResponseMsgTyp; aJobState: TJobState = jsValid): DWord;
    function SetSettings(aAnchor: Boolean; aLast: Boolean; aAssignment: Byte; //cAssignGroup, cAssigProdIDOnly
      aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
      aJobState: TJobState = jsValid): DWord; overload;
  public
    constructor Create(aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1,
        aQuery2: TNativeAdoQuery; aParams: TMMSettingsReader; aJob: PJobRec; var
        aBadProdGrpState: TResponseMsgTyp);
    destructor Destroy; override;
        //cAssignGroup, cAssigProdIDOnly aColor: Integer; aYMSetName: TText50; //Nue:12.9.01 aYMSetChanged: Boolean; //Nue:03.10.01 aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState; aYMSetID: Integer; aProdIDOrgRange: Longint);
        //cAssignGroup, cAssigProdIDOnly aColor: Integer; aYMSetName: TText50; //Nue:12.9.01 aYMSetChanged: Boolean; //Nue:03.10.01 aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState = jsValid; aYMSetID: Integer = 1; aProdIDOrgRange: Longint = 0);
  end;

  TJobManager = class(TBaseJobManager)
  private
    mMaEqualizeJobCritSec: TCriticalSection;
    mBuildJobQueue: TBuildJobQueue;
    function IsDataCollectionOn(aMachineID: Word): Boolean;
    function ProcAnalyseAndFinish(aText: string = ''): Boolean;
    function ProcAssignFromZE: Boolean; //Handles only assigns concerning ProdGrps not assigned since last AssignComplete
    function ProcDataAquEv: Boolean; //Used to aquire data on the machines
//    procedure ProcGetNodeList;
    function ProcMaOffline: Boolean;
    function ProcMaOnline(aCallType: TMaEqualizeReason; aText: string = ''): Boolean;
    function GetNetTyp(aMachineID: Word): TNetTyp;
    function ProcStopGrpEv: TResponseMsgTyp; //Used to stop (and restart(only for GUI)) a ProdGroup
    function ProcStartGrpEv: TResponseMsgTyp;
    function SameMachineState(aMachineID: Word; aOldMachState: LongInt): Boolean;
//wss    function SameMachineState(aMachineID: Word; aOldMachState: Word): Boolean;
  protected
    procedure ProcessInitJob; override;
    procedure ProcessJob; override;
    function Startup: Boolean; override;
  public
    constructor Create(aThreadDef: TThreadDef; aJobList: TJobList); override;
    destructor Destroy; override;
    function DoConnect: Boolean; override; // wss: override hat gefehlt
    function Init: Boolean; override;
  end;

  TJobManagerTimeoutTicker = class(TTimeoutTicker)
  protected
    procedure ProcessInitJob; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmCS, mmMBCS,
  mmRegistry,
  XMLSettingsAccess;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.SetSettings(aMachineID, aSpindleFirst,
    aSpindleLast: Word; aDummyProdID: Longint; aAnchor: Boolean; aLast: Boolean;
    aAssignment: Byte; aColor: Integer; aYMSetName: TText50; aYMSetChanged:
    Boolean; aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
    aJobState: TJobState = jsValid; aYMSetID: Integer = 1; aProdIDOrgRange:
    Longint = 0): DWord;
var
  xJob: TBaseJob;
  xFirstReceiver: TThreadTyp;
  xYMSetID: Integer;
  xSize: DWord;
  xJobRec: PJobRec;
begin
  if MachineOffline then xFirstReceiver := ttJobManager
                    else xFirstReceiver := ttMsgController;

  case aStartMode of
    smRange: begin
        // ProdGrp has changed because of range change, but the settings of the old still existing
        // spindles remain the same
        xYMSetID := aYMSetID;
      end;
    smMMStop: begin
        //Damit auf der ZE die ProdID nach dem Stop noch vorhanden bleibt
        mOwner.mJob^.SetSettings.SettingsRec.ProdGrpID := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID; //Nue 24.05.00
        aProdIDOrgRange                                := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
        xYMSetID                                       := mYMSetID;
      end;
    smZEStop: begin
        aProdIDOrgRange := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
        xYMSetID        := mYMSetID;
      end;
    smZE: begin //For start from WSC
        xYMSetID := mYMSetID;           
      end;
  else // smMM
// DONE wss: hier Job Handling noch anpassen. wegen ReallocMem noch die vorherigen Daten retten
    xYMSetID := cDummyYMSetID;
    xSize := GetJobHeaderSize(jtSetSettings) + DWORD(Length(StrPas(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData)));
    // neuer Job ist gr�sser als der alte Platz hat -> vergr�ssern
    if xSize > mOwner.mJobSize then begin
      // Speicher allozieren und die alten Jobdaten hineinkopieren
      xJobRec := AllocMem(xSize);
      CopyJob(mOwner.mJob, xJobRec);
      // alten Speicher freigeben...
      FreeMem(mOwner.mJob);
      //...und den neuen Speicher zuweisen, inkl. neuer Gr�sse
      mOwner.mJob     := xJobRec;
      mOwner.mJobSize := xSize;
    end;
      //DONE wss: XML: wie kommen die XMLSettings in den fJob.XMLData Buffer? - Werden beim Starten der neuen ProdGroup ab GUI oder ZE/WSC abgef�llt. (Nue)
//Wird unten zugewiesen
//    System.Move(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData, mOwner.mJob^.SetSettings.SettingsRec.XMLData, StrLen(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData) + 1); // +1 = inkl. abschliessende 0-Char

//      xYMSetID := cDummyYMSetID;
//      mOwner.mJob^.SetSettings.DataArray := fJob.DataArray;    //Dummy for not GUI calls
  end;

  //DONE wss: XML pr�fen. Erst hier weiss man, wie gross der Buffer sein wird. Darum wird der Job erst hier abgef�llt
  mOwner.mJob^.JobTyp := jtSetSettings;
  with mOwner.mJob^.SetSettings, SettingsRec do begin
    MachineID    := mStartGrpEv^.StartGrpEv.MachineID;
    SettingsRec  := mStartGrpEv^.StartGrpEv.SettingsRec;
    ProdGrpID    := aDummyProdID;

    SpindleFirst := aSpindleFirst;
    SpindleLast  := aSpindleLast;
    Group        := mMachineGrp; // Nue Ist dieser Wert richtig gesetzt bei RangeChange (Aufruf von CheckAdditionalProdGrpToRestart)
    Color        := aColor;
    YMSetName    := aYMSetName;
    YMSetChanged := aYMSetChanged;
    System.Move(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData, XMLData, Length(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData));
codesite.SendFmtMsg('TBuildJobQueueStartStop.SetSettings, JobLen:%d %s', [mOwner.mJob^.JobLen, mStartGrpEv^.StartGrpEv.SettingsRec.XMLData]);
  end;
//  with mOwner.mJob^.SetSettings, SettingsRec do begin
//    SpindleFirst   := aSpindleFirst;
//    SpindleLast    := aSpindleLast;
//    Group          := fMachineGrp;    //@@Nue Ist dieser Wert richtig gesetzt bei RangeChange (Aufruf von CheckAdditionalProdGrpToRestart)
//    Color          := aColor;
//    YMSetName      := aYMSetName;  //Nue:12.9.01
//    YMSetChanged   := aYMSetChanged;  //Nue:03.10.01
////wss    LengthData     := fJob.LengthData; //Dummy for not GUI calls
////    ProdGrpInfo    := fJob.ProdGrpParam.ProdGrpInfo;
//    ProdGrpID       := aDummyProdID; //fJob.ProdGrpParam.ProdGrpInfo.c_prod_id;
//    MachineID       := fJob.ProdGrpParam.ProdGrpInfo.c_machine_id;
//    OrderPositionID := fJob.ProdGrpParam.ProdGrpInfo.c_order_position_id;
//    OrderID         := fJob.ProdGrpParam.ProdGrpInfo.c_order_id;
//    StyleID         := fJob.ProdGrpParam.ProdGrpInfo.c_style_id;
//    YMSetID         := fJob.ProdGrpParam.ProdGrpInfo.c_YM_set_id;
//    YMSetName       := fJob.ProdGrpParam.ProdGrpInfo.c_YM_set_name;
//    ProdName        := fJob.ProdGrpParam.ProdGrpInfo.c_prod_name;
//    Slip            := fJob.ProdGrpParam.ProdGrpInfo.c_slip;
////      c_m_to_prod
////      c_clear_type
////      c_order_position_prod_id
////      c_style_name
////      c_order_position_name
////    ProdGrpInfo.c_machine_id := aMachineID;
////    ProdGrpInfo.c_prod_id    := aDummyProdID;
//  end;

  // add Job StartGrpEv to queue list. Starts inkonsistent Prodgroup.
  xJob := TSetSettingsJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, aAssignment,
    aStartMode, aProdGrpState, xYMSetID, mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
    mParams, mOldIntID, mOldFragID, xFirstReceiver, mBaseJob1, mComputerName, mPort, aProdIDOrgRange);

  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;
//-----------------------------------------------------------------------------

//constructor TBuildJobQueueStartStop.Create(aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery;
//  aParams: TMMSettingsReader;
//  aQueueType: TJobTyp; aJob: TStartGrpEv; var aBadProdGrpState: TResponseMsgTyp);
constructor TBuildJobQueueStartStop.Create(aOwner: TJobManager; aDatabase:
    TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery; aParams:
    TMMSettingsReader; aJob: PJobRec; var aBadProdGrpState: TResponseMsgTyp);
var
//  xYMParaDBAccess: TSettingsDBAssistant;
  xXMLSettingsAccess: TXMLSettingsAccess;
  xDummyProdID: Longint;
// xDummyColor: Integer;
  xDummyYMSetName: TText50;
begin
//NUE1  inherited create(aQueueType, aOwner, aDatabase, aQuery1, aQuery2, aParams);
  inherited create(aJob^.JobTyp, aOwner, aDatabase, aQuery1, aQuery2, aParams);
  fMachineOffline  := False;
  fDataCollection  := True; //Nue 26.06.01

  // Sicherheitskopie vom Originalen Job
  CloneJob(aJob, mStartGrpEv);

//NUE1  mStartGrpEv^.StartGrpEv             := aJob;
codesite.SendFmtMsg('TBuildJobQueueStartStop.Create, JobLen:%d %s', [mOwner.mJob^.JobLen, mStartGrpEv^.StartGrpEv.SettingsRec.XMLData]);
  mComputerName    := mStartGrpEv^.StartGrpEv.SettingsRec.ComputerName; //Overwrite defaultvalue '' from Baseclass
  mPort            := mStartGrpEv^.StartGrpEv.SettingsRec.Port; //Overwrite defaultvalue '' from Baseclass
  mMachineID       := mStartGrpEv^.StartGrpEv.MachineID; //Overwrite defaultvalue 0 from Baseclass
  mSpindleFirst    := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst; //Overwrite defaultvalue 0 from Baseclass
  mSpindleLast     := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast; //Overwrite defaultvalue 0 from Baseclass
  mMachineGrp      := mStartGrpEv^.StartGrpEv.SettingsRec.Group; //@@ Overwrite defaultvalue High(Word) from Baseclass. Handle in Context with AC338 and MachGrp on ZE
  mYMSetID         := 1; //Dummy, is only used for StopProdGrp, and will then be overwritten
  mNetType         := ntNone;
  aBadProdGrpState := rmNone;
  xDummyProdID     := 0; //Only that nothing wrong can be destroyed

  try
    try
      Query.Close;
      Query.SQL.Text := cQrySelMachine; //select AWE_mach_type

      Query.Params.ParamByName('c_machine_id').AsInteger := mMachineID;
      Query.Open;
      if Query.FindFirst then begin
        mNetType   := TNetTyp(Query.FieldByName('c_net_id').AsInteger);
        fDataCollection := (Query.FieldByName('c_data_collection').AsInteger <> 0);
      end;
      //TODO wss: was ist wenn diese MaschineID nicht vorhanden ist?
    except
      on e: Exception do begin
        mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create (Error in cQrySelMachineType) ' + e.message);
        raise EMMException.Create(mOwner.Error.Msg);
      end;
    end;

    try
      case aJob^.JobTyp of
        jtSendMsgToAppl: begin
          end;
//--------------------------
        jtStopGrpEv, jtStopGrpEvFromMa: begin
            aBadProdGrpState := rmProdGrpStoppedOk;
            try
              if (aJob^.JobTyp = jtStopGrpEv) and ((mNetType = ntWSC)) then begin
                mOwner.WriteLog(etError, Format('Not allowed to stop groups on Millmaster for AC338 machines! (StopGrpEv from MachID:%d, SpindFirst:%d, SpindLast:%d',
                                                [mStartGrpEv^.StopGrpEv.MachineID, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleLast]));
//wss                                                [mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast]));
                aBadProdGrpState := rmBadMachineGrp;
                EXIT;
              end;

              Query.Close;
              Query.SQL.Text := cQrySelBadProdGrpState; //select bad prod_state and machine_state<>msInSynchronisation
              Query.Params.ParamByName('c_prod_ID').AsInteger := mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID;
//wss              Query.Params.ParamByName('c_prod_ID').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
              Query.Open;
              if Query.FindFirst then begin
                //wss: denke erst schliessen und dann 5 Sekunden warten ist besser
                Query.Close;
                Sleep(5000);
                Query.SQL.Text := cQrySelBadProdGrpState;
                Query.Params.ParamByName('c_prod_ID').AsInteger := mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID;
//wss                Query.Params.ParamByName('c_prod_ID').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
                Query.Open;
                if Query.FindFirst then
                  if (Query.FieldByName('c_prod_state').AsInteger = ORD(psStopped)) then begin
                    mOwner.WriteLog(etWarning, Format('No proper prod_state (%s) for ProdGrp %d. Stop it anyway(StopGrpEv)! MachID:%d, SpindFirst:%d, SpindLast:%d',
                                                      [cProdGrpStateArr[TProdGrpstate(Query.FieldByName('c_prod_state').AsInteger)],
                                                      mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StopGrpEv.MachineID, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleLast]));
//wss                                                      mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast]));
                  end
                  else begin
                    mOwner.WriteLog(etError, Format('Bad prod_state(%s) or machine_state(%s) for ProdGrp %d.Impossible to stop(StopGrpEv)! MachID:%d, SpindFirst:%d, SpindLast:%d',
                                                    [cProdGrpStateArr[TProdGrpstate(Query.FieldByName('c_prod_state').AsInteger)],
                                                    cMachStateArr[TMachState(Query.FieldByName('c_machine_state').AsInteger)],
                                                    mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StopGrpEv.MachineID, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleLast]));
//wss                                                    mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast]));
                    aBadProdGrpState := rmProdGrpStateBad;
                    if Query.FieldByName('c_machine_state').AsInteger = ORD(msOffline) then
                      fMachineOffline := True;
                    EXIT;
                  end;
              end;

            //Check if machine OFFLINE
              Query.Close;
              Query.SQL.Text := cQrySelMachState;
              Query.Params.ParamByName('c_machine_id').AsInteger := mStartGrpEv^.StopGrpEv.MachineID;
//wss              Query.Params.ParamByName('c_machine_id').AsInteger := mStartGrpEv^.StartGrpEv.MachineID;
              Query.Open;
              if Query.FindFirst then begin
                if (Query.FieldByName('c_machine_state').AsInteger = ORD(msUndefined)) or
                   (Query.FieldByName('c_machine_state').AsInteger = ORD(msOffline)) then
                  fMachineOffline := True;
              end else
                fMachineOffline := True;
            except
              on e: Exception do begin
                mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create (Getting c_prod_state) failed.' + e.message);
                raise EMMException.Create(mOwner.Error.Msg);
              end;
            end;

            xDummyProdID := 0; //must be 0 for Stops from Machine
            mDataBase.StartTransaction; //??Nue Abhandeln wenn bereits eine transaction aktiv
            if mOwner.mJob^.JobTyp = jtStopGrpEv then
            try
              //Stop from GUI, for Stop from Machine never start a new Dummy ProdGrp automatically
              Query.Close;
              //Check if ProdGrp still exists!!
              Query.SQL.Text := cQrySelGrpStateSingle;
              Query.Params.ParamByName('c_prod_id').AsInteger := mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID;
//wss              Query.Params.ParamByName('c_prod_id').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
              Query.Open;
              if Query.FindFirst then begin // ADO Conform
                // Set fYMSetID for start Dummy Grps with the settings of the stopped group
                mYMSetID := Query.FieldByName('c_YM_set_id').AsInteger;
              end
              else begin
                mOwner.WriteLog(etError, Format('TBuildJobQueueStartStop.Create: Non existent ProdGrp %d.Impossible to stop(StopGrpEv)! MachID:%d, SpindFirst:%d, SpindLast:%d',
                                                [mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StopGrpEv.MachineID, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StopGrpEv.SettingsRec.SpindleLast]));
//                                                [mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast]));
                aBadProdGrpState := rmProdGrpStartedNOk;
                mDatabase.Commit; // other commit further down
                EXIT;
              end;

            except
              on e: Exception do begin
                mDataBase.Rollback;
                mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create (Insert dummy to t_prodgroup) failed.' + e.message);
                raise EMMException.Create(mOwner.Error.Msg);
              end;
            end;

            Query.Close;
            GetActIntIDAndFragID(Query, mIntID, mFragID, mOwner.fError);
            GetPrevActIntIDAndFragID(Query, mOldIntID, mOldFragID, mOwner.fError);

            Query.Close;
            Query.SQL.Text := cQrySetGrpState;
            Query.Params.ParamByName('c_prod_state').AsInteger := ORD(psStopping); //6=stopping
            Query.Params.ParamByName('c_prod_id').AsInteger    := mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID;
//wss            Query.Params.ParamByName('c_prod_id').AsInteger    := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
            Query.ExecSQL;

            //TODO wss: �hm f�r was wird hier noch ein select gemacht? wo wird das Result ben�tigt?
            Query.Close;
            Query.SQL.Text := cQrySelGrpStateSingle;
            Query.Params.ParamByName('c_prod_id').AsInteger := mStartGrpEv^.StopGrpEv.SettingsRec.ProdGrpID;
//wss            Query.Params.ParamByName('c_prod_id').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
            Query.Open;
            mDataBase.Commit;
          end;
//--------------------------
        jtStartGrpEv, jtStartGrpEvFromMa: begin
            aBadProdGrpState := rmProdGrpStartedOk;
            Query.Close;
            mDatabase.StartTransaction;
          //Related commit is further down

          (* Wenn ein Datensatz vorhanden ist, dann ist ein Datensatz einer vorhergehenden
             Partie vorhanden. In diesem Fall wird nach 5 Sekunden (sleep(5000)) noch einmal
             gepr�ft. Ist die Partie noch immer eingetragen, dann wird der Start abgebrochen.
             Die entsprechende Maschine wird auf Offline gesetzt. *)
            Query.SQL.Text := cQrySelGrpNotAllowedInRange;
            Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
            Query.Params.ParamByName('c_prod_id').AsInteger       := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
            Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
            Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
            Query.Open;

            if Query.FindFirst then begin
              Query.Close;
              Sleep(5000);
              //Query.SQL.Text und Parameter sind von vorhin schon drin.
              Query.Open;
              if Query.FindFirst then begin
                mOwner.WriteLog(etError, Format('Bad state(%s) for ProdGrp %d.Impossible to stop (StartGrpEv)! MachID:%d, SpindFirst:%d, SpindLast:%d',
                                               [cProdGrpStateArr[TProdGrpstate(Query.FieldByName('c_prod_state').AsInteger)],
                                                mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast]));
                aBadProdGrpState := rmProdGrpStateBad;
                if Query.FieldByName('c_machine_state').AsInteger = ORD(msOffline) then
                  fMachineOffline := True;
                mDatabase.Commit; // other commit in else case
                EXIT;
              end;
            end;

            try
//            fJob.ProdGrpParam.ProdGrpInfo.c_machine_id := fJob.MachineID;
//            fJob.ProdGrpParam.ProdGrpYMPara.Group      := fJob.SettingsRec.Group;
              if ((mOwner.mJob^.JobTyp = jtStartGrpEv) and (mNetType = ntWSC)) or (not DataCollection) then begin
// Start from GUI for AC338-Machines or for machine with NoDataCollection
                try
                  Query.Close;
                  Query.SQL.Text := cQrySelSameMachGroup;
                  Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
                  Query.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
                  Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
                  Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
                  Query.Open;
                //if NOT Query.EOF then begin  // ADO Conform
                  if Query.FindFirst then begin
                    //ChangeSettings from GUI
                    xDummyProdID := InsertDummyProdGrp(Query, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast,
                                                       TYarnUnit(mParams.Value[cYarnCntUnit]), 0, smMM, psNotStarted,
                                                       mStartGrpEv^.StartGrpEv.SettingsRec, mStartGrpEv^.StartGrpEv.SettingsRec.Color, xDummyYMSetName, mOwner.fError);
//                                                     fJob.ProdGrpParam, fJob.Color{var}, xDummyYMSetName{var}, mOwner.fError);

                  // Update state of affected prodgrps to STOPPING (only if MachId, MachGrp and SpdRange are equal!)
                    Query.Close;
                    Query.SQL.Text := cQrySetGrpAffectedStateAC338;
                    Query.Params.ParamByName('c_prod_state').AsInteger    := ORD(psStopping); //6=stopping
                    Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
                    Query.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
                    Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
                    Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
                    Query.ExecSQL;
                  end
                  else begin
                    Query.Close;
                    Query.SQL.Text := cQrySelNoSpdInGrpState;
                    Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
                    Query.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
                    Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
                    Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
                    Query.Open;
                    if Query.FindFirst then begin
                      //ProdGrp found with spindles in new range (but not equal range and MachGrpNr)
                      //-> Not allowed!!
                      aBadProdGrpState := rmBadSpdRange;
                      mDatabase.Commit; // other commit in else case
                      EXIT;
                    end
                    else begin
                      //Load settings to machine; No entry on DB because MachGrp in state "Definition"
                      //Produktionsgruppenabhaengige Parameter aus den XMLSettings wurden in AssignHandlerComp eingef�gt
                      mStartGrpEv^.StartGrpEv.SettingsRec.AssignMode := cAssignGroup;  //Setzen des AssignMode
//Alt
//                        //xDummyProdID = 0
//                      xYMParaDBAccess := TSettingsDBAssistant.Create(Query); // Create of class for settings acces
//                      try
//                      //DONE wss: XML: woher kommen die Produktionsgruppenabhaengige Parameter aus den XMLSettings?
////                      xYMParaDBAccess.AddProdGrpYMPara(fJob.DataArray, fJob.ProdGrpParam.ProdGrpYMPara, cAssignGroup); // - Fuegt Produktionsgruppen-Abhaengige Parameter in DataArray ein.
//                      finally
//                        xYMParaDBAccess.Free;
//                      end;

                    end;
                  end
                except
                  on e: Exception do begin
                    mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create (Query for AC338-check) failed.' + e.message);
                    raise EMMException.Create(mOwner.Error.Msg);
                  end;
                end
              end
              else begin
// Start from Machine and GUI for NON AC338-Machines
              // insert new prodgrp into t_prodgroup to get the prod_id
                try
                  if mOwner.mJob^.JobTyp = jtStartGrpEv then begin //GUI
                    xDummyProdID := InsertDummyProdGrp(Query, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast,
                                                       TYarnUnit(mParams.Value[cYarnCntUnit]), 0, smMM, psNotStarted,
                                                       mStartGrpEv^.StartGrpEv.SettingsRec, mStartGrpEv^.StartGrpEv.SettingsRec.Color, xDummyYMSetName, mOwner.fError);
//                                                     fJob.ProdGrpParam, fJob.Color{var}, xDummyYMSetName{var}, mOwner.fError);
                  end
                  else begin // From ZE or WSC
                    xDummyProdID := InsertDummyProdGrp(Query, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast,
                                                       TYarnUnit(mParams.Value[cYarnCntUnit]), 0, smZE, psNotStarted,
                                                       mStartGrpEv^.StartGrpEv.SettingsRec, mStartGrpEv^.StartGrpEv.SettingsRec.Color, xDummyYMSetName, mOwner.fError);
//                                                     fJob.ProdGrpParam, fJob.Color{var}, xDummyYMSetName{var}, mOwner.fError);

                    if (aJob^.JobTyp = jtStartGrpEvFromMa) and ((mNetType = ntWSC)) then begin
                    //The correct settings are already in fJob: -> Save on DB, update previous inserted DummyProdGrp
//WSS org                    xYMParaDBAccess := TSettingsDBAssistant.Create(Query2);  // Create of class for settings acces

// DONE wss:                   xXMLSettingsHandler := TXMLSettingsHandler.Create;
//                    try
//                      fYMSetID := xXMLSettingsHandler.SaveSetting(nil, StrPas(fJob.XMLData), False);
//                    finally
//                      xXMLSettingsHandler.Free;
//                    end;

                    // DONE wss: XML: Settings speichern
                      xXMLSettingsAccess := TXMLSettingsAccess.Create(Query);
                      try
                      mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID := xDummyProdID;
                      mYMSetID := xXMLSettingsAccess.SaveSettingAndProdGrpValues(@mStartGrpEv^.StartGrpEv.SettingsRec);
//                      fYMSetID := xYMParaDBAccess.SaveAll(fJob.DataArray, xDummyProdID, fJob.MachineID,
//                                                          fJob.SpindleFirst, fJob.SpindleLast, fJob.YMSetName, fJob.YMSetChanged, False);
                      finally
                        xXMLSettingsAccess.Free;
                      end;
                    end;
                  end;
                except
                  on e: Exception do begin
                    mDataBase.Rollback;
                    mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create (Insert dummy to t_prodgroup) failed.' + e.message);
                    raise EMMException.Create(mOwner.Error.Msg);
                  end;
                end;

            // Update state of affected prodgrps to STOPPING
                Query.Close;
                Query.SQL.Text := cQrySetGrpAffectedState;
                Query.Params.ParamByName('c_prod_state').AsInteger    := ORD(psStopping); //6=stopping
                Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
                Query.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
                Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
                Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
                Query.ExecSQL;
              end; //if

            //Get Info of affected ProdGrps
              Query.Close;
              GetActIntIDAndFragID(Query, mIntID, mFragID, mOwner.fError);
              GetPrevActIntIDAndFragID(Query, mOldIntID, mOldFragID, mOwner.fError);

              Query.SQL.Text := cQrySelGrpAffectedInRange;
              Query.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
              Query.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
              Query.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
              Query.Open;
            //Do not use Query in the further live of the instance of this class, because outside the class
            // it will be looped over this result-set!!!!
            except
              on e: Exception do begin
                mDatabase.Rollback;
                mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create(3) failed.' + e.message);
                raise EMMException.Create(mOwner.Error.Msg);
              end;
            end;
            mDatabase.Commit; // other commit in if case
          end; // jtStartGrpEv, jtStartGrpEvFromMa
      else
      end; // case

    except
      on e: Exception do begin
        mDataBase.Rollback;
        mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create ' + e.message);
        raise EMMException.Create(mOwner.Error.Msg);
      end;
    end;
  finally
    mDummyProdID := xDummyProdID;
    mProdID      := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
  end;
end;

//-----------------------------------------------------------------------------
destructor TBuildJobQueueStartStop.Destroy;
begin
  inherited Destroy;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.CheckAdditionalProdGrpToRestart(aAnchor,
  aLast: Boolean; aAssignment: Byte; aStartMode: TProdGrpStartMode;
  aProdGrpState: TProdGrpState; aJobState: TJobState): DWord;
const
  cQrySelCheckSpdRange =
    'SELECT y.c_ym_set_name,ps.* FROM v_prodgroup_state ps, t_xml_YM_settings y WHERE ps.c_ym_set_id=y.c_ym_set_id AND c_machine_id=:c_machine_id AND c_prod_state IN (1,3,6) AND ' +
    'c_machineGroup <> :c_machineGroup AND ' +
    '((c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR ' + //First in range of existing ProdGrp
    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last)) AND ' + //Last in range of existing ProdGrp
    'NOT (c_spindle_first =:c_spindle_first AND c_spindle_last=:c_spindle_last) '; //Not the same range

var
  xFirst: Boolean;
  xDummyProdID,
    xProdIDOrgRange: Longint;
  xDummyColor: Integer;
  xQuery1, xQuery2: TNativeAdoQuery;
  xDBSpindleFirst, xDBSpindleLast: Word;
  xJobSpindleFirst, xJobSpindleLast: Word;
  xYMSetID: Integer;
//  xColor: Integer;
  xYM_Set_Name, xDummyYMSetName: TText50; //Nue:2.10.01
  xJobRec: PJobRec;
begin
  Result  := 0; //wss: sonst kein R�ckgabewert -> compiler warning
  xFirst  := aAnchor;
  xQuery1 := TNativeAdoQuery.Create;
  xQuery1.Assign(Query);

  xQuery2 := TNativeAdoQuery.Create;
  xQuery2.Assign(Query);

  xJobSpindleFirst := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
  xJobSpindleLast  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;

  xQuery1.Close;
  xQuery1.SQL.Text := cQrySelCheckSpdRange;
  xQuery1.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
  xQuery1.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
  xQuery1.Params.ParamByName('c_spindle_first').AsInteger := xJobSpindleFirst;
  xQuery1.Params.ParamByName('c_spindle_last').AsInteger  := xJobSpindleLast;
  xQuery1.Open;

  if xQuery1.FindFirst then begin // ADO Conform
    // Sicherheitskopie vom Originalen Job
    CloneJob(mStartGrpEv, xJobRec);
    try
      mStartGrpEv^.StartGrpEv.SettingsRec.Group := xQuery1.FieldByName('c_machineGroup').AsInteger; //Nue 10.05.00
      while not xQuery1.EOF do begin // ADO Conform
        StrPCopy(@xYM_Set_Name, xQuery1.FieldByName('c_YM_set_name').AsString); //Nue:13.9.01
        xDBSpindleFirst := xQuery1.FieldByName('c_spindle_first').AsInteger;
        xDBSpindleLast  := xQuery1.FieldByName('c_spindle_last').AsInteger;
        xYMSetID        := xQuery1.FieldByName('c_YM_set_id').AsInteger;
  //      xColor          := xQuery1.FieldByName('c_color').AsInteger;
        xProdIDOrgRange := xQuery1.FieldByName('c_prod_id').AsInteger;

        if ((xDBSpindleFirst < xJobSpindleFirst) and (xDBSpindleLast > xJobSpindleLast)) then begin
          //New ProdGrp fully within the range of a existing ProdGrp. -> Start lower part of existing Grp new
          xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, TYarnUnit(mParams.Value[cYarnCntUnit]),
                                             xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);

          Result := SetSettingsWss(mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, xDummyProdID,
                                xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID);
          xFirst := False;
        end
        else if (xDBSpindleFirst < xJobSpindleFirst) then begin
          //New ProdGrp with his first spindle in the range of an existing ProdGrp. -> Start 1 Grp new
          xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, TYarnUnit(mParams.Value[cYarnCntUnit]),
                                             xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);

          Result := SetSettingsWss(mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, xDummyProdID,
                                xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
          xFirst := False;
        end
        else if (xDBSpindleFirst = xJobSpindleFirst) and (xJobSpindleFirst = xJobSpindleLast) then begin
          //Single spindle at the beginning of range
          xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xJobSpindleLast+1, xDBSpindleLast, TYarnUnit(mParams.Value[cYarnCntUnit]),
                                             xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);

          Result := SetSettingsWss(mStartGrpEv^.StartGrpEv.MachineID, xJobSpindleLast+1, xDBSpindleLast, xDummyProdID,
                                xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
          xFirst := False;
        end
        else if (xDBSpindleLast = xJobSpindleLast) and (xJobSpindleFirst = xJobSpindleLast) then begin
          //Single spindle at the end of range
          xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, TYarnUnit(mParams.Value[cYarnCntUnit]),
                                             xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);

          Result := SetSettingsWss(mStartGrpEv^.StartGrpEv.MachineID, xDBSpindleFirst, xJobSpindleFirst-1, xDummyProdID,
                                xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
          xFirst := False;
        end
        else if (xDBSpindleLast > xJobSpindleLast) then begin
          //New ProdGrp with his last spindle in the range of an existing ProdGrp. -> Start 1 Grp new
          xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xJobSpindleLast+1, xDBSpindleLast, TYarnUnit(mParams.Value[cYarnCntUnit]),
                                             xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);

          // in mStartGrpEv sind jetzt die Daten von der originalen Partie -> Spindelbereich noch f�r gek�rzte Partie setzen
//          mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst := xJobSpindleLast+1;
          Result := SetSettingsWss(mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, xDBSpindleLast, xDummyProdID,
                                xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, xJobSpindleLast+1, xDBSpindleLast, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
          xFirst := False;
        end;
        xQuery1.Next;
      end; //while
    finally
      // Da im mStartGrpEv nun die Daten von der angepassten "alten" Partie sind, muss der Originaljob wieder hergestellt werden
      CopyJob(xJobRec, mStartGrpEv);
      FreeMem(xJobRec);
    end;
  end;
  xQuery1.Close;
  xQuery2.Close;
  xQuery1.Free;
  xQuery2.Free;
end;
//function TBuildJobQueueStartStop.CheckAdditionalProdGrpToRestart(aAnchor,
//  aLast: Boolean; aAssignment: Byte; aStartMode: TProdGrpStartMode;
//  aProdGrpState: TProdGrpState; aJobState: TJobState): DWord;
//const
//  cQrySelCheckSpdRange =
//    'SELECT y.c_ym_set_name,ps.* FROM v_prodgroup_state ps, t_xml_YM_settings y WHERE ps.c_ym_set_id=y.c_ym_set_id AND c_machine_id=:c_machine_id AND c_prod_state IN (1,3,6) AND ' +
//    'c_machineGroup <> :c_machineGroup AND ' +
//    '((c_spindle_first <= :c_spindle_first AND c_spindle_last >= :c_spindle_first) OR ' + //First in range of existing ProdGrp
//    '(c_spindle_first <= :c_spindle_last AND c_spindle_last >= :c_spindle_last)) AND ' + //Last in range of existing ProdGrp
//    'NOT (c_spindle_first =:c_spindle_first AND c_spindle_last=:c_spindle_last) '; //Not the same range
//
//var
//  xFirst: Boolean;
//  xDummyProdID,
//    xProdIDOrgRange: Longint;
//  xDummyColor: Integer;
//  xQuery1, xQuery2: TNativeAdoQuery;
//  xSpindleFirst, xSpindleLast: Word;
//  xYMSetID: Integer;
////  xColor: Integer;
//  xYM_Set_Name, xDummyYMSetName: TText50; //Nue:2.10.01
//begin
//  Result  := 0; //wss: sonst kein R�ckgabewert -> compiler warning
//  xFirst  := aAnchor;
//  xQuery1 := TNativeAdoQuery.Create;
//  xQuery1.Assign(Query);
//
//  xQuery2 := TNativeAdoQuery.Create;
//  xQuery2.Assign(Query);
//
//  xQuery1.Close;
//  xQuery1.SQL.Text := cQrySelCheckSpdRange;
//  xQuery1.Params.ParamByName('c_machine_id').AsInteger    := mStartGrpEv^.StartGrpEv.MachineID;
//  xQuery1.Params.ParamByName('c_machineGroup').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.Group;
//  xQuery1.Params.ParamByName('c_spindle_first').AsInteger := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
//  xQuery1.Params.ParamByName('c_spindle_last').AsInteger  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
//  xQuery1.Open;
//
//  if xQuery1.FindFirst then begin // ADO Conform
//    mStartGrpEv^.StartGrpEv.SettingsRec.Group := xQuery1.FieldByName('c_machineGroup').AsInteger; //Nue 10.05.00
//    while not xQuery1.EOF do begin // ADO Conform
//      StrPCopy(@xYM_Set_Name, xQuery1.FieldByName('c_YM_set_name').AsString); //Nue:13.9.01
//      xSpindleFirst   := xQuery1.FieldByName('c_spindle_first').AsInteger;
//      xSpindleLast    := xQuery1.FieldByName('c_spindle_last').AsInteger;
//      xYMSetID        := xQuery1.FieldByName('c_YM_set_id').AsInteger;
////      xColor          := xQuery1.FieldByName('c_color').AsInteger;
//      xProdIDOrgRange := xQuery1.FieldByName('c_prod_id').AsInteger;
//
//      if ((xSpindleFirst < mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst) and (xSpindleLast > mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast)) then begin
//        //New ProdGrp fully within the range of a existing ProdGrp. -> Start lower part of existing Grp new
//        xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, TYarnUnit(mParams.Value[cYarnCntUnit]),
//                                           xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);
//
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID);
//        xFirst := False;
//      end
//      else if (xSpindleFirst < mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst) then begin
//        //New ProdGrp with his first spindle in the range of an existing ProdGrp. -> Start 1 Grp new
//        xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, TYarnUnit(mParams.Value[cYarnCntUnit]),
//                                           xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);
//
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
//        xFirst := False;
//      end
//      else if (xSpindleFirst = mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst) and (mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst = mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast) then begin
//        //Single spindle at the beginning of range
//        xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast + 1, xSpindleLast, TYarnUnit(mParams.Value[cYarnCntUnit]),
//                                           xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);
//
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast + 1, xSpindleLast, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
//        xFirst := False;
//      end
//      else if (xSpindleLast = mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast) and (mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst = mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast) then begin
//        //Single spindle at the end of range
//        xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, TYarnUnit(mParams.Value[cYarnCntUnit]),
//                                           xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);
//
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, xSpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst - 1, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
//        xFirst := False;
//      end
//      else if (xSpindleLast > mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast) then begin
//        //New ProdGrp with his last spindle in the range of an existing ProdGrp. -> Start 1 Grp new
//        xDummyProdID := InsertDummyProdGrp(xQuery2, mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast + 1, xSpindleLast, TYarnUnit(mParams.Value[cYarnCntUnit]),
//                                           xProdIDOrgRange, aStartMode, aProdGrpState, mStartGrpEv^.StartGrpEv.SettingsRec, xDummyColor, xDummyYMSetName, mOwner.fError);
//
//        Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast + 1, xSpindleLast, xDummyProdID,
//                              xFirst, aLast, aAssignment, xDummyColor, xYM_Set_Name, False, aStartMode, aProdGrpState, aJobState, xYMSetID, xProdIDOrgRange);
//        xFirst := False;
//      end;
//      xQuery1.Next;
//    end; //while
//  end;
//  xQuery1.Close;
//  xQuery2.Close;
//  xQuery1.Free;
//  xQuery2.Free;
//end;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.GetDataStopZESpd(aAnchor: Boolean; aLast: Boolean): Boolean;
var
  xJob: TBaseJob;
  xListID: DWord;
begin
  if mExpSpdData then begin
    mOwner.mJob^.JobTyp := jtGetExpDataStopZESpd;
    with mOwner.mJob^.GetExpDataStopZESpd do begin
      MachineID    := Query.FieldByName('c_machine_id').AsInteger;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
//NUE1      MachineTyp   := TWinderType(Query.FieldByName('c_AWE_mach_type').AsInteger);
      EndIntID     := mIntID;
      EndFragID    := mFragID;
      StartIntID   := Query.FieldByName('c_int_id_ok').AsInteger;
      StartFragID  := Query.FieldByName('c_fragshift_id_ok').AsInteger;
    end;
    // add Job GetExpZESpdData to queue list.
    xJob := TGetExpDataStopZESpdJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
      mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
      mBaseJob1, mComputerName, mPort)
  end
  else begin
    mOwner.mJob^.JobTyp := jtGetDataStopZESpd;
    with mOwner.mJob^.GetDataStopZESpd do begin
      MachineID    := Query.FieldByName('c_machine_id').AsInteger;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
//NUE1      MachineTyp   := TWinderType(Query.FieldByName('c_AWE_mach_type').AsInteger);
      IntID        := mIntID;
      FragID       := mFragID;
    end;
    // add Job GetZESpdData to queue list.
    xJob := TGetDataStopZESpdJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                        mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
                                        mBaseJob1, mComputerName, mPort);
  end;

  Result := Prepare(xJob, aAnchor, aLast, xListID);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.SendMsgToAppl(aAnchor: Boolean; aLast: Boolean; aMsg: TResponseMsgTyp; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  Result := 0; //wss: sonst kein R�ckgabewert -> compiler warning

  if mStartGrpEv^.StartGrpEv.SettingsRec.ComputerName <> '' then begin
  // Job initialized by an application
    mOwner.mJob^.JobTyp := jtSendMsgToAppl;
    with mOwner.mJob^.SendMsgToAppl do begin
      ComputerName := mStartGrpEv^.StartGrpEv.SettingsRec.ComputerName;
      Port         := mStartGrpEv^.StartGrpEv.SettingsRec.Port;
    end;
    with mOwner.mJob^.SendMsgToAppl.ResponseMsg do begin
      MsgTyp       := aMsg;
      MachineID    := mStartGrpEv^.StartGrpEv.MachineID;
      SpindleFirst := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst;
      SpindleLast  := mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast;
      ProdGrpID    := mDummyProdID;
    end;
    // Job initialized by an user the basesystem or the ZE
    // add Job SendMsgToAppl to queue list.
    xJob := TSendMsgToApplJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                     mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database, mBaseJob1);

    Prepare(xJob, aAnchor, aLast, Result, aJobState);
  end
end;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.SetSettings(aAnchor: Boolean; aLast: Boolean; aAssignment: Byte;
  aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState; aJobState: TJobState): DWord;
begin
  // fDummyProd wird im Create abgef�llt
  Result := SetSettings(mStartGrpEv^.StartGrpEv.MachineID, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleFirst, mStartGrpEv^.StartGrpEv.SettingsRec.SpindleLast, mDummyProdID,
                        aAnchor, aLast, aAssignment, mStartGrpEv^.StartGrpEv.SettingsRec.Color, mStartGrpEv^.StartGrpEv.SettingsRec.YMSetName,
                        mStartGrpEv^.StartGrpEv.SettingsRec.YMSetChanged, aStartMode, aProdGrpState, aJobState);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueueStartStop.SetSettingsWss(aMachineID, aSpindleFirst,
    aSpindleLast: Word; aDummyProdID: Longint; aAnchor: Boolean; aLast: Boolean;
    aAssignment: Byte; aColor: Integer; aYMSetName: TText50; aYMSetChanged:
    Boolean; aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState;
    aJobState: TJobState = jsValid; aYMSetID: Integer = 1; aProdIDOrgRange:
    Longint = 0): DWord;
var
  xJob: TBaseJob;
  xFirstReceiver: TThreadTyp;
  xYMSetID: Integer;
  xSize: DWord;
  xJobRec: PJobRec;
begin
  if MachineOffline then xFirstReceiver := ttJobManager
                    else xFirstReceiver := ttMsgController;

  case aStartMode of
    smRange: begin
        // ProdGrp has changed because of range change, but the settings of the old still existing
        // spindles remain the same
        xYMSetID := aYMSetID;
      end;
    smMMStop: begin
        //Damit auf der ZE die ProdID nach dem Stop noch vorhanden bleibt
        mOwner.mJob^.SetSettings.SettingsRec.ProdGrpID := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID; //Nue 24.05.00
        aProdIDOrgRange                                := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
        xYMSetID                                       := mYMSetID;
      end;
    smZEStop: begin
        aProdIDOrgRange := mStartGrpEv^.StartGrpEv.SettingsRec.ProdGrpID;
        xYMSetID        := mYMSetID;
      end;
    smZE: begin //For start from WSC
        xYMSetID := mYMSetID;
      end;
  else // smMM
    xYMSetID := cDummyYMSetID;
    xSize := GetJobHeaderSize(jtSetSettings) + DWORD(Length(StrPas(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData)));
    // neuer Job ist gr�sser als der alte Platz hat -> vergr�ssern
    if xSize > mOwner.mJobSize then begin
      // Speicher allozieren und die alten Jobdaten hineinkopieren
      xJobRec := AllocMem(xSize);
      CopyJob(mOwner.mJob, xJobRec);
      // alten Speicher freigeben...
      FreeMem(mOwner.mJob);
      //...und den neuen Speicher zuweisen, inkl. neuer Gr�sse
      mOwner.mJob     := xJobRec;
      mOwner.mJobSize := xSize;
    end;
  end;

  xJobRec := Nil;
  CloneJob(mStartGrpEv, xJobRec);
  xJobRec^.JobTyp := jtSetSettings;
  with xJobRec^.SetSettings, SettingsRec do begin
    MachineID    := mStartGrpEv^.StartGrpEv.MachineID;
    SettingsRec  := mStartGrpEv^.StartGrpEv.SettingsRec;
    Color        := aColor;
    System.Move(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData, XMLData, Length(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData));
codesite.SendFmtMsg('TBuildJobQueueStartStop.SetSettings, JobLen:%d %s', [xJobRec^.JobLen, mStartGrpEv^.StartGrpEv.SettingsRec.XMLData]);
  end;
//  mOwner.mJob^.JobTyp := jtSetSettings;
//  with mOwner.mJob^.SetSettings, SettingsRec do begin
//    MachineID    := mStartGrpEv^.StartGrpEv.MachineID;
//    SettingsRec  := mStartGrpEv^.StartGrpEv.SettingsRec;
//    ProdGrpID    := aDummyProdID;
//
//    SpindleFirst := aSpindleFirst;
//    SpindleLast  := aSpindleLast;
//    Group        := mMachineGrp; // Nue Ist dieser Wert richtig gesetzt bei RangeChange (Aufruf von CheckAdditionalProdGrpToRestart)
//    Color        := aColor;
//    YMSetName    := aYMSetName;
//    YMSetChanged := aYMSetChanged;
//    System.Move(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData, XMLData, Length(mStartGrpEv^.StartGrpEv.SettingsRec.XMLData));
//codesite.SendFmtMsg('TBuildJobQueueStartStop.SetSettings, JobLen:%d %s', [mOwner.mJob^.JobLen, mStartGrpEv^.StartGrpEv.SettingsRec.XMLData]);
//  end;

  // add Job StartGrpEv to queue list. Starts inkonsistent Prodgroup.
//  xJob := TSetSettingsJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, aAssignment,
  xJob := TSetSettingsJob.Create(mOwner.mJobList, xJobRec, cNrOfTrials, aAssignment,
    aStartMode, aProdGrpState, xYMSetID, mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
    mParams, mOldIntID, mOldFragID, xFirstReceiver, mBaseJob1, mComputerName, mPort, aProdIDOrgRange);

  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
//function TBuildJobQueueStartStop.SetProdID(aAnchor, aLast: Boolean): Boolean;
//var
//  xJob: TBaseJob;
//  xListID: DWord;
//begin
//  Result := True;
//  // mDummyProd wird im Create abgef�llt
//  // MachID, SpindleRange und Group wurden bereits in TBuildJobQueueStartStop.Create in mOwner.mJob abgef�llt.
//  mOwner.mJob.SetProdID.ProdGrpID := mDummyProdID;
//  xJob   := TSetProdIDJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database, mBaseJob1);
//  Result := Prepare(xJob, aAnchor, aLast, xListID);
//end;
//
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

constructor TBuildJobQueueDataAqu.Create(aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery;
  aParams: TMMSettingsReader;
  aOldIntID: Byte; aOldFragID: Longint; aIntID: Byte; aFragID: Longint;
  aShiftcalID: Byte; aShiftEv: Boolean; aShiftInDay: Byte); //Added on 25.03.01 Nue
var
  xMachineAct,
  xMachineLast: Integer;
begin
  inherited create(jtDataAquEv, aOwner, aDatabase, aQuery1, aQuery2, aParams);
  fSuccessorJob := 0;
  mOwner        := aOwner;
  mAnchorJob    := nil;
  mBaseJob1     := nil;
  mIntID        := aIntID;
  mFragID       := aFragID;
  mOldIntID     := aOldIntID;
  mOldFragID    := aOldFragID;
  mShiftcalID   := aShiftcalID;
  mShiftEv      := aShiftEv;
  mShiftInDay   := aShiftInDay;
  mDummyProdID  := 0 {Dummy};
  try
    Query.Close;
    mDataBase.StartTransaction; //??Nue Abhandeln wenn bereits eine transaction aktiv
    //Set all prodgroups with state 1 and 3 on collecting(5)
    //In a transaction in the StorageHandler: c_fragshift_id_ok=c_old_fragshift_id_ok,
    //  c_int_id_ok=c_old_int_id_ok and set c_data_ok to FALSE
    Query.SQL.Text := cQrySetGrpStateCollecting;
    Query.ExecSQL;

    Query.Close;
    Query.SQL.Text := cQrySelGrpStateNotCollecting + 'order by c_machine_id ';
    Query.Open;
    xMachineLast := 0;
    while not Query.EOF do begin // ADO Conform
      xMachineAct := Query.FieldByName('c_machine_id').AsInteger;
      if xMachineLast <> xMachineAct then begin
        mOwner.WriteLog(etInformation, Format('MachineID:%d is %s!',
                                              [Query.FieldByName('c_machine_id').AsInteger,
                                               cMachStateArr[TMachState(Query.FieldByName('c_machine_state').AsInteger)]]));
      end;
      xMachineLast := xMachineAct;
      Query.Next;
    end;

    Query.SQL.Text := cQrySelGrpState;
    Query.Open;
    mDataBase.Commit;
  except
    on e: Exception do begin
      mDataBase.Rollback;
      mOwner.fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueDataAqu.Create ' + e.message);
      raise EMMException.Create(mOwner.Error.Msg);
    end;
  end;
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
constructor TBuildJobQueue.Create(aQueueType: TJobTyp; aOwner: TJobManager; aDatabase: TAdoDBAccessDB; aQuery1, aQuery2: TNativeAdoQuery;
  aParams: TMMSettingsReader);
begin
  inherited Create;
  mDatabase     := aDatabase;
  mOwner        := aOwner;
  fQuery        := aQuery1;
  fQuery2       := aQuery2;
  mQueueType    := aQueueType;
  mDummyProdID  := 0;
  mProdID       := 0;
  mComputerName := '';
  mPort         := '';
  mMachineID    := 0; //Overwritten when not used in usual aquisation
  mSpindleFirst := 0; //Overwritten when not used in usual aquisation
  mSpindleLast  := 0; //Overwritten when not used in usual aquisation
  mMachineGrp   := High(Word); //@@ Handle in Context with AC338 and MachGrp on ZE
  mParams       := aParams;
end;

//-----------------------------------------------------------------------------
destructor TBuildJobQueue.Destroy;
begin
  inherited Destroy;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.FindFirst: Boolean;
begin
  Result := Query.FindFirst;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.EOF: Boolean;
begin
  Result := Query.EOF;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.Next: Boolean;
begin
  Query.Next;
  Result := True;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.Prior: Boolean;
begin
  Query.Prior;
  Result := True;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.SetNetTyp: Boolean;
begin
  mOwner.mJob^.NetTyp := TNetTyp(Query.FieldByName('c_net_id').AsInteger);
  Result := True;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.SetExp: Boolean;
begin
  Result := True; //wss: sonst kein R�ckgabewert -> compiler warning

  if (Query.FieldByName('c_int_id_ok').AsInteger = mOldIntID) AND
     (Query.FieldByName('c_fragshift_id_ok').AsInteger = mOldFragID) OR (Query.FieldByName('c_int_id_ok').AsInteger = 0) then //Used for new ProdGrps, where data has never been collected before
    mExpSpdData := False
  else
    mExpSpdData := True;

  if (not mExpSpdData) and (Query.FieldByName('c_shift_int_id_ok').AsInteger = mOldIntID) and
     (Query.FieldByName('c_shift_fragshift_id_ok').AsInteger = mOldFragID) then
    mExpShiftData := False
  else
    mExpShiftData := True;
end;

//-----------------------------------------------------------------------------

function TBuildJobQueue.Prepare(aJob: TBaseJob; aAnchor: Boolean; aLast: Boolean; var aListID: DWord; aJobState: TJobState): Boolean;
begin
  Result := True; //wss: sonst kein R�ckgabewert -> compiler warning

  // Wenn mit Ankerjob und dies ist der erste Job dann diesen Merken
  if aAnchor then begin
    if mAnchorJob = nil then begin
      mAnchorJob := aJob;
    end;
    mBaseJob1 := aJob;
  end;

  if not aAnchor and not aLast then begin
    mBaseJob1 := aJob;
  end;

  if aLast then begin
    // wenn der Letzte Job in die Liste eingef�gt wurde, dann JobController benachrichtigen
    aListID := mAnchorJob.AddSelfToJobList(aJobState, fSuccessorJob);

    mOwner.mJob^.JobID  := mAnchorJob.JobID;
    mOwner.mJob^.JobTyp := jtNewJobEv;
    mOwner.WriteJobBufferTo(ttJobController);

    mAnchorJob    := nil;
    mBaseJob1         := nil;
    fSuccessorJob := 0;
  end;
end;
//-----------------------------------------------------------------------------
function TBuildJobQueue.SendEventToClients(aAnchor: Boolean; aLast: Boolean;
  aEvent: TApplMsg; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  mOwner.mJob^.JobTyp                       := jtSendEventToClients;
  mOwner.mJob^.SendEventToClients.Event     := aEvent;
  mOwner.mJob^.SendEventToClients.MachineID := mMachineID;
  xJob := TSendEventToClientsJob.Create(mOwner.mJobList, jtSendEventToClients, mOwner.mJob, cNrOfTrials,
                                        mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
                                        ttQueueManager, mBaseJob1);
  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.UnDoGetZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
var
  xJob: TBaseJob;
  xListID: DWord;
begin
  Result := True; //wss: sonst kein R�ckgabewert -> compiler warning

  if (Query.FieldByName('c_data_ok').AsInteger) = 0 then begin //0=False; data_aqu in last interval not i.O.
    mOwner.mJob^.JobTyp := jtUnDoGetZESpdData;
    with mOwner.mJob^.UnDoGetZESpdData do begin
      MachineID    := mMachineID;
      SpindleFirst := mSpindleFirst;
      SpindleLast  := mSpindleLast;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
      // Get the last, successful on DB saved, Frag- and Int-ID
      IntID  := Query.FieldByName('c_int_id_ok').AsInteger;
      FragID := Query.FieldByName('c_fragshift_id_ok').AsInteger;
    end;
      // add Job GetZESpdData to queue list.
    xJob := TUnDoGetZESpdDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, mOwner.mDB.Query[cSecondaryQuery],
                                        mOwner.mDB.Database, mParams, mQueueType, mDummyProdID,
                                        mComputerName, mPort);
    Result := Prepare(xJob, aAnchor, aLast, xListID);
  end;
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.GetZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
var
  xJob: TBaseJob;
  xListID: DWord;
begin
  if mExpSpdData then begin
    mOwner.mJob^.JobTyp := jtGetExpZESpdData;
    with mOwner.mJob^.GetExpZESpdData do begin
      MachineID    := Query.FieldByName('c_machine_id').AsInteger;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
//NUE1      MachineTyp   := TWinderType(Query.FieldByName('c_AWE_mach_type').AsInteger);
      EndIntID     := mIntID;
      EndFragID    := mFragID;
      StartIntID   := Query.FieldByName('c_int_id_ok').AsInteger;
      StartFragID  := Query.FieldByName('c_fragshift_id_ok').AsInteger;
    end;
    // add Job GetExpZESpdData to queue list.
    xJob := TGetExpZESpdDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                       mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
                                       mBaseJob1, mComputerName, mPort);
  end
  else begin
    mOwner.mJob^.JobTyp := jtGetZESpdData;
    with mOwner.mJob^.GetZESpdData do begin
      MachineID    := Query.FieldByName('c_machine_id').AsInteger;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
//NUE1      MachineTyp   := TWinderType(Query.FieldByName('c_AWE_mach_type').AsInteger);
      IntID        := mIntID;
      FragID       := mFragID;
    end;
    // add Job GetZESpdData to queue list.
    xJob := TGetZESpdDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                    mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
                                    mBaseJob1, mComputerName, mPort);
  end;

  Result := Prepare(xJob, aAnchor, aLast, xListID);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.DelZESpdData(aAnchor: Boolean; aLast: Boolean): Boolean;
var
  xJob: TBaseJob;
  xListID: DWord;
begin
  mOwner.mJob^.JobTyp := jtDelZESpdData;
  with mOwner.mJob^.DelZESpdData do begin
    MachineID    := Query.FieldByName('c_machine_id').AsInteger;
    SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
    SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
  end;
  // add Job DelZEData to queue list.
  xJob := TDelZESpdDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                  mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database,
                                  mParams, mQueueType, mProdID, mDummyProdID, mBaseJob1, mComputerName, mPort);

  Result := Prepare(xJob, aAnchor, aLast, xListID);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.GenShiftData(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  if not mExpShiftData then begin
    mOwner.mJob^.JobTyp := jtGenExpShiftData;
    with mOwner.mJob^.GenExpShiftData do begin
      MachineID    := mMachineID;
      SpindleFirst := mSpindleFirst;
      SpindleLast  := mSpindleLast;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      EndIntID     := mIntID;
      EndFragID    := mFragID;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
      StartIntID   := Query.FieldByName('c_shift_int_id_ok').AsInteger;
      StartFragID  := Query.FieldByName('c_shift_fragshift_id_ok').AsInteger;
    end;
    // add Job GenExpShiftData to queue list.
    xJob := TGenExpShiftDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, mOwner.mDB.Query[cSecondaryQuery],
                                       mOwner.mDB.Database, mParams, mQueueType, mDummyProdID,
                                       mBaseJob1, mComputerName, mPort);
  end
  else begin
    mOwner.mJob^.JobTyp := jtGenShiftData;
    with mOwner.mJob^.GenShiftData do begin
      MachineID    := mMachineID;
      SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;

      IntID        := mIntID;
      FragID       := mFragID;
      ProdID       := Query.FieldByName('c_prod_id').AsInteger;
    end;
    // add Job GenShiftData to queue list.
    xJob := TGenShiftDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials, mOwner.mDB.Query[cSecondaryQuery],
                                    mOwner.mDB.Database, mParams, mQueueType, mDummyProdID,
                                    mBaseJob1, mComputerName, mPort);
  end;
  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.GenSpdOfflimits(aAnchor: Boolean; aLast: Boolean): Boolean;
var
  xJob: TBaseJob;
  xListID: DWord;
begin
  mOwner.mJob^.JobTyp                 := jtGenSpdOfflimits;
  mOwner.mJob^.GenSpdOfflimits.IntID  := mIntID;
  mOwner.mJob^.GenSpdOfflimits.ProdID := Query.FieldByName('c_prod_id').AsInteger;
  // add Job GenSpdOfflimits to queue list.
  xJob := TGenSpdOfflimitsJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                     mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database, mBaseJob1);

  Result := Prepare(xJob, aAnchor, aLast, xListID);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.GenDwData(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  // For the following code lines it used to be: TGenDWData and TGenExpDWData are equal!!!
  //Exp muss noch kodiert werden
  mOwner.mJob^.JobTyp := jtGenDWData;
  with mOwner.mJob^.GenDwData do begin
    IntID      := mIntID;
    FragID     := mFragID;
    ProdID     := Query.FieldByName('c_prod_id').AsInteger;
    ShiftEv    := mShiftEv; //Added on 25.03.01 Nue
    ShiftcalID := mShiftcalID; //Added on 25.03.01 Nue
    ShiftInDay := mShiftInDay; //Added on 25.03.01 Nue
  end;
  if not mExpShiftData then //fExpDWData muss noch erstellt werden??
    // add Job GenDWData to queue list.
    xJob := TGenDWDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                 mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database)
  else //Exp
    // add Job GenExpDWData to queue list.
    xJob := TGenExpDWDataJob.Create(mOwner.mJobList, mOwner.mJob, cNrOfTrials,
                                    mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database);

  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.DBDump(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  mOwner.mJob^.JobTyp        := jtDBDump;
  mOwner.mJob^.DBDump.DBName := cDBPhysicalName;
  xJob := TDBDumpJob.Create(mOwner.mJobList, jtDBDump, mOwner.mJob, cNrOfTrials,
                            mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database, ttQueueManager);

  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
function TBuildJobQueue.GenQOfflimit(aAnchor: Boolean; aLast: Boolean; aJobState: TJobState): DWord;
var
  xJob: TBaseJob;
begin
  mOwner.mJob^.JobTyp                   := jtGenQOfflimit;
  mOwner.mJob^.GenQOfflimit.FragshiftID := mFragID;
  xJob := TGenQOfflimitJob.Create(mOwner.mJobList, jtGenQOfflimit, mOwner.mJob, cNrOfTrials,
                                  mOwner.mDB.Query[cSecondaryQuery], mOwner.mDB.Database, ttQueueManager);

  Prepare(xJob, aAnchor, aLast, Result, aJobState);
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
constructor TJobManager.Create(aThreadDef: TThreadDef; aJobList: TJobList);
begin
  mMaEqualizeJobCritSec := TCriticalSection.Create;
  inherited Create(aThreadDef, aJoblist);
end;
//-----------------------------------------------------------------------------
destructor TJobManager.Destroy;
begin
  mMaEqualizeJobCritSec.Free;
  inherited Destroy;
end;
//-----------------------------------------------------------------------------

function TJobManager.IsDataCollectionOn(aMachineID: Word): Boolean;
begin
  Result := True;
  with mDB.Query[cPrimaryQuery] do
  try
    Close;
    SQL.Text := cQrySelMachine;
    Params.ParamByName('c_machine_id').AsInteger := aMachineID;
    Open;
    if FindFirst then
      Result := (FieldByName('c_data_collection').AsInteger <> 0);
  except
    on e: Exception do begin
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'JobManager.IsDataCollectionOn' + e.message);
      WriteLog(etError, Error.msg);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TJobManager.ProcAnalyseAndFinish(aText: string): Boolean;
var
  xBaseJob: TBaseJob;
  xTrials: Integer;
  xJobRec: PJobRec;
begin
  Result := False;
  // hole den originalen Job aus der JobList anhand der JobID welche �ber die Pipe empfangen wurde
  if mJobList.GetJob(xBaseJob, mJob^.JobID, mDB.Query[cSecondaryQuery], mDB.Database) then begin
    // CheckOut/CheckIn is only used here at the moment, because MsgDispatcher sends Msgs to JobManager
    // and also to (MsgController) JobController. So it can be that the same job will be worked
    // twice at the same time!
    xTrials := 0;
    while (mJobList.CheckOut(mJob^.JobID) = nil) and (xTrials < 5) do begin
      INC(xTrials);
      Sleep(1000);
    end;

    if xTrials < 5 then begin
      xJobRec := nil;
      try
        //wss: Sicherheitskopie vom originalen Job
        CloneJob(xBaseJob.JobRec, xJobRec);
        //vvvv don't use the Move methode of TList class
//wss      System.Move(xBaseJob.JobRec^, xJobRec, xBaseJob.JobRec^.JobLen); //Save job from joblist

        { TODO -owss :
        XML: Achtung: hier k�nnte es zu Problemen kommen. Falls die Antwort gr�sser
        ist als der vorhandene Jobbuffer, muss dieser noch vergr�ssert werden. Wie
        verh�lt es sich dann innerhalb der verarbeitenden Klassen, wenn da pl�tzlich
        eine neue Adresse drin steht, wenn mit ReallocMem() ein neuer Speicherplatz
        zugeordnet wurde? Ebenfalls zu beachten: intern wird die Variable mJobSize gef�hrt,
        welche nicht mitaktuallisiert wird, wenn hier einfach das Property JobRec vergr�ssert wird.
        Eventuell muss mJobSize ebenfalls als Property nach draussen gef�hrt werden, um pr�fen zu k�nnen
        ob der interne JobRec genug Platz zur Verf�gung stellt. }
//wss      System.Move(mJob, xBaseJob.JobRec^, mJob^.JobLen);  //Copy data into class
        //wss: empfangene Daten von diesem Job in die Jobdaten der Klasse kopieren
        xBaseJob.AssignJob(mJob);
//        CopyJob(mJob, xBaseJob.JobRec);

        //Copy data from JobRec not inserted in NetHandler from original job from JobList
        case mJob^.JobTyp of
          jtSetSettings: begin
              with xBaseJob.JobRec^.SetSettings, SettingsRec do begin
                MachineID    := xJobRec.SetSettings.MachineID;
                SpindleFirst := xJobRec.SetSettings.SettingsRec.SpindleFirst;
                SpindleLast  := xJobRec.SetSettings.SettingsRec.SpindleLast;
                Group        := xJobRec.SetSettings.SettingsRec.Group;
                ProdGrpID    := xJobRec.SetSettings.SettingsRec.ProdGrpID;
              end; // with
            end; // jtSetSettings
        else
        end; //case

        try
          //wss: die entsprechend zust�ndige Klasse analysiert die empfangenen Daten
          xBaseJob.AnalyseJobData;
        except
          on e: Exception do begin
            fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TJobManager.ProcAnalyseAndFinish failed.' + e.message);
            WriteLog(etError, 'TJobManager.ProcAnalyseAndFinish failed.' + e.message + aText);
          end;
        end;
        mJobList.CheckIn(mJob^.JobID);
        Result := True;

        xBaseJob.WriteBufferTo(ttJobController, jtJobFinished);
      finally
        FreeMem(xJobRec); // Job von CloneJob freigeben
      end; // try
    end
    else begin
      WriteJobBufferTo(ttJobManager);
      WriteLog(etWarning, Format('ProcAnalyseAndFinish (%s): JobID %d in use. -> New trial', [aText, mJob^.JobID]));
    end; // if xTrials < 5
  end;
end;

//-----------------------------------------------------------------------------
function TJobManager.ProcAssignFromZE: Boolean;
var
  xGroup: Word;
  xJob: TJobRec;
//  xJobTmp: PJobRec;
begin
  Result := True;
  for xGroup:=0 to cZESpdGroupLimit-1 do begin
    with mJob^.Assign.Assigns.Groups[xGroup] do begin
//      if (0 in Modified) or (1 in Modified) then begin // Bit 0: spindle range; Bit 1: parameter
      if (Modified * [0,1]) <> [] then begin // Bit 0: spindle range; Bit 1: parameter
        if ProdId <> 0 then begin //<>0 means: first Assign for this Group in a Assigns cycle
          // beim ersten Assign von der ZE wird f�r jede betroffene Partie ein k�nstlicher StopGrpEvFromMa Job ausgel�st,
          // damit die Daten geholt werden.
          //Stop ProdGrp > aquire data
          try
            mDB.Query[cPrimaryQuery].Close;
            mDB.Query[cPrimaryQuery].SQL.Text := cQrySelGrpStateSingle;
            mDB.Query[cPrimaryQuery].Params.ParamByName('c_prod_id').AsInteger := ProdId;
            mDB.Query[cPrimaryQuery].Open;

            if mDB.Query[cPrimaryQuery].FindFirst then begin
              FillChar(xJob, sizeof(xJob), 0);
              xJob.JobTyp := jtStopGrpEvFromMa;
              xJob.StopGrpEvFromMa.MachineID                := mJob^.Assign.MachineID;
              xJob.StopGrpEvFromMa.SettingsRec.SpindleFirst := SpindleFirst;
              xJob.StopGrpEvFromMa.SettingsRec.SpindleLast  := SpindleLast;
              xJob.StopGrpEvFromMa.SettingsRec.Group        := xGroup;
              xJob.StopGrpEvFromMa.SettingsRec.ProdGrpID    := ProdId;
              xJob.StopGrpEvFromMa.SettingsRec.ComputerName := '';
              xJob.StopGrpEvFromMa.SettingsRec.MMUserName   := '';
              xJob.StopGrpEvFromMa.SettingsRec.Port         := '';

              WriteJobBufferTo(ttJobManager, @xJob); //jtStopGrpEvFromMa
            end
            else begin
              WriteLog(etInformation, Format('TJobManager.ProcAssignFromZE: ProdId:%d on Machine:%d (not found to stop for ZE machines). Probably already stopped.',
                                             [ProdId, mJob^.Assign.MachineID])); //Nue:12.3.02 Modified
            end; // if mDB.Query[cPrimaryQuery].FindFirst
          except
            on e: Exception do begin
              fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TJobManager.ProcAssignFromZE failed.' + e.message);
              WriteLog(etError, fError.msg);
              Result := False;
            end;
          end;
        end; // if ProdId <> 0
      end; // if (0 in Modified)
    end; // with mJob^.Assign.Assigns.Groups[xGroup]
  end;

end;
//-----------------------------------------------------------------------------

function TJobManager.ProcDataAquEv: Boolean;
var
//  xJob: TJobRec;
  xJobID: DWord;
  xDWJobInserted: Boolean;
//wss  xQOfflimitJobInserted,
//wss  xExpJob : Boolean;
//wss  xBaseJob1: TBaseJob;
begin
  Result         := True;
  xJobID         := 0; //wss: compiler warning uninitialized
  xDWJobInserted := False;
//wss  xQOfflimitJobInserted := False;
// DONE: XML: warum wird in lokal kopiert aber mJob wird nicht mehr ver�ndert?
//wss  xJob := mJob;

  WriteLog(etInformation, Format('Start Dataaquisation: %s', [DateTimeToStr(Now)]));
  try

    mBuildJobQueue := TBuildJobQueueDataAqu.Create(Self, mDB.Database, mDB.Query[cPrimaryQuery], mDB.Query[cThirdQuery],
                                                   Params, mJob^.DataAquEv.OldIntID, mJob^.DataAquEv.OldFragID,
                                                   mJob^.DataAquEv.IntID, mJob^.DataAquEv.FragID,
                                                   mJob^.DataAquEv.ShiftcalID, mJob^.DataAquEv.ShiftEv, mJob^.DataAquEv.ShiftInDay); //Added on 25.03.01 Nue
//wss    mBuildJobQueue := TBuildJobQueueDataAqu.Create(Self, mDB.Database, mDB.Query[cPrimaryQuery], mDB.Query[cThirdQuery],
//                           Params, xJob.DataAquEv.OldIntID, xJob.DataAquEv.OldFragID,
//                          xJob.DataAquEv.IntID, xJob.DataAquEv.FragID,
//                          xJob.DataAquEv.ShiftcalID, xJob.DataAquEv.ShiftEv, xJob.DataAquEv.ShiftInDay);  //Added on 25.03.01 Nue
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Create in ProcDataAquEv failed.' + e.message);
      WriteLog(etError, fError.msg);
    end;
  end;

  try
    try
      if mBuildJobQueue.FindFirst then begin
        mBuildJobQueue.SendEventToClients(True, True, amStartAquisation);
        mBuildJobQueue.SendEventToClients(True, True, amSystemInfo); //Neu: Nue 7.6.01

        while not mBuildJobQueue.EOF do begin // ADO Conform
          mBuildJobQueue.SetNetTyp;
          if not xDWJobInserted then begin // add Job GenDWData to queue list.
            if mJob^.DataAquEv.ShiftEv then begin
              if mJob^.DataAquEv.ShiftEv and Params.IsComponentQOfflimit then begin
                xJobID := mBuildJobQueue.GenQOfflimit(True, True, jsInvalid); // add Job GenQOfflimit to queue list.
                if (xJobID = 0) then
                  WriteLog(etError, 'Adding job GenQOfflimit to joblist failed: ' + FormatMMErrorText(mJobList.ListError, etNTError))
                else
                  mBuildJobQueue.SuccessorJob := xJobID;
              end;
              xJobID := mBuildJobQueue.DBDump(True, True, jsInvalid); // add Job DBDump to queue list.
              if (xJobID = 0) then
                WriteLog(etError, 'Adding job DBDump to joblist failed: ' + FormatMMErrorText(mJobList.ListError, etNTError))
              else
                mBuildJobQueue.SuccessorJob := xJobID;
            end;
            xJobID := mBuildJobQueue.SendEventToClients(True, True, amEndAquisation, jsInvalid);
            if (xJobID = 0) then
              WriteLog(etError, 'Adding job SendEventToClients to joblist failed: ' + FormatMMErrorText(mJobList.ListError, etNTError))
            else begin
              mBuildJobQueue.SuccessorJob := xJobID;
              xJobID := mBuildJobQueue.GenDwData(True, True, jsInvalid);
              if (xJobID = 0) then
                WriteLog(etError, 'Adding job GenDWData to joblist failed: ' + FormatMMErrorText(mJobList.ListError, etNTError))
              else
                xDWJobInserted := True;
            end;
          end;
          mBuildJobQueue.SuccessorJob := xJobID;
          mBuildJobQueue.SetExp;
          mBuildJobQueue.UnDoGetZESpdData(True, False);
          mBuildJobQueue.GetZESpdData(True, False);
        //??Nue  jtGetZEGrpData / jtGetExpZEGrpData
          mBuildJobQueue.DelZESpdData(False, False);
        //??Nue  jtDelZEGrpData
          if mJob^.DataAquEv.RegularInt then begin // RegularInterval Event
            mBuildJobQueue.GenShiftData(False, False);
            mBuildJobQueue.GenSpdOfflimits(False, True);
          end
          else begin
            mBuildJobQueue.GenShiftData(False, True);
          end;
          mBuildJobQueue.Next;
        end; //while
//wss        while not mBuildJobQueue.EOF do begin  // ADO Conform
//          mBuildJobQueue.SetNetTyp;
//          if not xDWJobInserted then begin  // add Job GenDWData to queue list.
//            if xJob.DataAquEv.ShiftEv then begin
//              if (xJob.DataAquEv.ShiftEv) AND (Params.IsComponentQOfflimit) then begin
//                xJobID :=  mBuildJobQueue.GenQOfflimit(True,True,jsInvalid);  // add Job GenQOfflimit to queue list.
//                if (xJobID = 0) then
//                  WriteLog(etError, 'Adding job GenQOfflimit to joblist failed: '+FormatMMErrorText(mJobList.ListError,etNTError))
//                else
//                  mBuildJobQueue.SuccessorJob := xJobID;
//              end;
//              xJobID :=  mBuildJobQueue.DBDump(True,True,jsInvalid);  // add Job DBDump to queue list.
//              if (xJobID = 0) then
//                WriteLog(etError, 'Adding job DBDump to joblist failed: '+FormatMMErrorText(mJobList.ListError,etNTError))
//              else
//                mBuildJobQueue.SuccessorJob := xJobID;
//            end;
//            xJobID :=  mBuildJobQueue.SendEventToClients(True,True,amEndAquisation,jsInvalid);
//            if (xJobID = 0) then
//              WriteLog(etError, 'Adding job SendEventToClients to joblist failed: '+FormatMMErrorText(mJobList.ListError,etNTError))
//            else begin
//              mBuildJobQueue.SuccessorJob := xJobID;
//              xJobID := mBuildJobQueue.GenDwData(True,True,jsInvalid);
//              if (xJobID = 0) then
//                WriteLog(etError, 'Adding job GenDWData to joblist failed: '+FormatMMErrorText(mJobList.ListError,etNTError))
//              else
//                xDWJobInserted := True;
//            end;
//          end;
//          mBuildJobQueue.SuccessorJob := xJobID;
//          mBuildJobQueue.SetExp;
//          mBuildJobQueue.UnDoGetZESpdData(True,False);
//          mBuildJobQueue.GetZESpdData(True,False);
//        //??Nue  jtGetZEGrpData / jtGetExpZEGrpData
//          mBuildJobQueue.DelZESpdData(False,False);
//        //??Nue  jtDelZEGrpData
//          if xJob.DataAquEv.RegularInt then begin // RegularInterval Event
//            mBuildJobQueue.GenShiftData(False,False);
//            mBuildJobQueue.GenSpdOfflimits(False,True);
//          end else begin
//            mBuildJobQueue.GenShiftData(False,True);
//          end;
//          mBuildJobQueue.Next;
//        end; //while
      end //if
      else begin
        WriteLog(etInformation, Format('No dataaquisation- and succeeding-jobs will be executed, because no prodgroup in production: %s. ', [DateTimeToStr(Now)]));
      end;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProcDataAquEv failed.' + e.message);
        WriteLog(etError, fError.msg);
        Result := False;
      end;
    end;
  finally
    mBuildJobQueue.Free; //wss Destroy;
  end;
end;

//-----------------------------------------------------------------------------
function TJobManager.ProcMaOffline: Boolean;
var
  xBaseJob: TBaseJob;
  xMachID: Word;
begin
  Result  := False;
  xMachID := mJob^.MaOnline.MachineID;
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      mDB.DataBase.StartTransaction; //??Nue Abhandeln wenn bereits eine transaction aktiv
      SQL.Text := cQrySetMachAndNodeState;
      Params.ParamByName('c_machine_state').AsInteger := ORD(msOffline);
      Params.ParamByName('c_machine_id').AsInteger    := xMachID;
      Params.ParamByName('c_node_stat').AsInteger     := ORD(nsOffline);
      ExecSQL;
      mDB.DataBase.Commit;
    end;

    mJob^.JobTyp                       := jtSendEventToClients;
    mJob^.SendEventToClients.Event     := amMachineStateChange;
    mJob^.SendEventToClients.MachineID := xMachID;
    xBaseJob := TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, mJob, cNrOfTrials,
                                              mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager);
    // Add to JobList
    xBaseJob.AddSelfToJobList;

    mJob^.JobID  := xBaseJob.JobID;
    mJob^.JobTyp := jtNewJobEv;
    WriteJobBufferTo(ttJobController);
    Result := True;
  except
    on e: Exception do begin
      mDB.DataBase.Rollback;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProcMaOffline failed.' + e.message);
      WriteLog(etError, fError.msg);
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TJobManager.ProcMaOnline(aCallType: TMaEqualizeReason; aText: string): Boolean;
var
  xBaseJob: TBaseJob;
//  xSaveMaConfigJob: TBaseJob;
  xMachineUploadedYet: Boolean;
  xMachID: Word;
  xJob: TJobRec;
begin
  Result  := True;
  xMachID := mJob^.MaOnline.MachineID;
  try
    with mDB.Query[cPrimaryQuery] do begin
      Close;
      mDB.DataBase.StartTransaction; //??Nue Abhandeln wenn bereits eine transaction aktiv
      SQL.Text := cQrySetMachAndNodeState;
      Params.ParamByName('c_machine_state').AsInteger := ORD(msInSynchronisation);
      Params.ParamByName('c_machine_id').AsInteger    := xMachID;
      Params.ParamByName('c_node_stat').AsInteger     := ORD(nsOnline);
      ExecSQL;
      mDB.DataBase.Commit;

      Close;
      SQL.Text := cQrySelMachine;
      Params.ParamByName('c_machine_id').AsInteger := xMachID;
      Open;
    end;

//    xMachineUploadedYet := False;
//    if mDB.Query[cPrimaryQuery].FindFirst then begin
//      if not (mDB.Query[cPrimaryQuery].FieldByName('c_AWE_mach_type').AsInteger = ORD(amtUnknown)) then begin
//        xMachineUploadedYet := True;
//      end
//      else begin
//        WriteLog(etWarning, Format('Attention: Machine %s (MachineID:%d) not uploaded and saved on DB in application MachineConfiguration yet! No dataaquisation on this machine available!',
//                                   [mDB.Query[cPrimaryQuery].FieldByName('c_machine_name').AsString, mJob^.MaOnline.MachineID]));
//      end;
//    end;

    // Hat diese Maschine einen g�ltigen Bauzustandseintrag auf der DB?
    with mDB.Query[cSecondaryQuery] do
    try
      SQL.Text := cQryCheckMaGroupConfig;
      Params.ParamByName('MachID').AsInteger := xMachID;
      Open;
      xMachineUploadedYet := FindFirst;
{M�rz 2005 wss: bis auf weiteres kein Automat f�r das Speichern des Bauzustandes
      if not FindFirst then begin
        CodeSite.SendFmtMsg('Create Job SaveMaConfigToDB for MachID=%d', [xMachID]);
        xJob.JobTyp := jtSaveMaConfigToDB;
        xJob.NetTyp := mJob^.NetTyp;
        xJob.SaveMaConfig.MachineID    := xMachID;
        xJob.SaveMaConfig.UseXMLData   := False;
        xJob.SaveMaConfig.ComputerName := '';
        xJob.SaveMaConfig.Port         := '';
        xSaveMaConfigJob := TSaveMaConfigToDB.Create(mJobList, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database);
      end else
        xSaveMaConfigJob := Nil;
{}
    finally
      Close;
    end;

    if xMachineUploadedYet and IsDataCollectionOn(xMachID) then begin
      // Maschine hat einen g�ltigen Bauzustand auf der DB und DataCollect Flag ist gesetzt -> Abgleichen
      xJob.JobTyp               := jtMaEqualize;
      xJob.NetTyp               := mJob^.NetTyp;
      xJob.MaEqualize.MachineID := xMachID;
      xBaseJob := TMaEqualizeJob.Create(mJobList, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],
                                              mDB.Database, Params, mMaEqualizeJobCritSec, aCallType{, ttMsgController, xSaveMaConfigJob});

      //wss: dieser Job wird als Folgejob von xMaEqualizeJob in Queue eingef�gt
      xJob.JobTyp                       := jtSendEventToClients;
      xJob.SendEventToClients.Event     := amMachineStateChange;
      xJob.SendEventToClients.MachineID := xMachID;
      TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager, xBaseJob);
    end
    else begin
      xJob.JobTyp                       := jtSendEventToClients;
      xJob.SendEventToClients.Event     := amMachineStateChange;
      xJob.SendEventToClients.MachineID := xMachID;
      xBaseJob := TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager{, xSaveMaConfigJob});
    end;

//    if not IsDataCollectionOn(mJob^.MaOnline.MachineID) {or (xMachineUploadedYet = False) }then begin
//      xJob.JobTyp                       := jtSendEventToClients;
//      xJob.SendEventToClients.Event     := amMachineStateChange;
//      xJob.SendEventToClients.MachineID := xMachID;
//      xBaseJob := TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager{, xSaveMaConfigJob});
//    end
//    else begin
//      // wenn noch kein MaConfig XML auf der DB f�r diese Maschine ist, dann MaEqualize als Folgejob ausf�hren
//      xJob.JobTyp               := jtMaEqualize;
//      xJob.NetTyp               := mJob^.NetTyp;
//      xJob.MaEqualize.MachineID := xMachID;
//      xBaseJob := TMaEqualizeJob.Create(mJobList, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],
//                                              mDB.Database, Params, mMaEqualizeJobCritSec, aCallType{, ttMsgController, xSaveMaConfigJob});
//
//      //wss: dieser Job wird als Folgejob von xMaEqualizeJob in Queue eingef�gt
//      xJob.JobTyp                       := jtSendEventToClients;
//      xJob.SendEventToClients.Event     := amMachineStateChange;
//      xJob.SendEventToClients.MachineID := xMachID;
//      TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager, xBaseJob);
//    end;

    // Add to JobList
//    if Assigned(xSaveMaConfigJob) then
//      xSaveMaConfigJob.AddSelfToJobList
//    else
    xBaseJob.AddSelfToJobList;

    mJob^.JobID  := xBaseJob.JobID;
    mJob^.JobTyp := jtNewJobEv;
    WriteJobBufferTo(ttJobController);
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('ProcMaOnline %s failed.', [aText]) + e.message);
      mDB.DataBase.Rollback;
      WriteLog(etError, fError.msg);
    end;
  end;
end;

//-----------------------------------------------------------------------------
function TJobManager.GetNetTyp(aMachineID: Word): TNetTyp;
begin
  with mDB.Query[cPrimaryQuery] do try
    Close;
    SQL.Text := cQrySelNetID;
    Params.ParamByName('c_machine_id').AsInteger := aMachineID;
    Open;
    if FindFirst then Result := TNetTyp(FieldByName('c_net_id').AsInteger)
                 else Result := ntNone; //Should never happen!!
  except
    on e: Exception do begin
      Result := ntNone; //Should never happen
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TJobManager.GetNetTyp failed.' + e.message);
      WriteLog(etWarning, fError.msg);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TJobManager.ProcStopGrpEv: TResponseMsgTyp;
var
  xBadProdGrpState: TResponseMsgTyp;
  xJobQueue: TBuildJobQueueStartStop;
begin
  Result := rmNone;
  mJob^.NetTyp := GetNetTyp(mJob^.StopGrpEv.MachineID);
  try
    try
      mBuildJobQueue := TBuildJobQueueStartStop.Create(Self, mDB.Database, mDB.Query[cPrimaryQuery], mDB.Query[cThirdQuery],
                                                       Params, mJob, xBadProdGrpState);
      xJobQueue := TBuildJobQueueStartStop(mBuildJobQueue);
      if xBadProdGrpState <> rmProdGrpStoppedOk then begin //Stop action because ProdGrp in invalid state to access (not in (1,3))
        if xJobQueue.MachineOffline then
          Result := rmMachineOffline
        else
          Result := xBadProdGrpState;
        EXIT;
      end;
    except
      on e: Exception do begin
        Result := rmNone;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Create in ProcStopGrpEv failed.' + e.message);
        WriteLog(etError, fError.msg);
      end;
    end;

    try
      if mBuildJobQueue.FindFirst then begin
        while not mBuildJobQueue.EOF do begin // ADO Conform
          if (xJobQueue.MachineOffline) and (mJob^.StopGrpEv.SettingsRec.ComputerName <> '') then begin
            xJobQueue.SetSettings(True, False, cAssignProdGrpIDOnly, smMMStop, psInconsistent);
            xJobQueue.SendMsgToAppl(False, False, rmProdGrpStoppedOk);
          end
          else begin
            xJobQueue.SetExp;
            xJobQueue.UnDoGetZESpdData(True, False);
            xJobQueue.GetDataStopZESpd(True, False);
            //??Nue  jtGetDataStopZEGrp / jtGetExpDataStopZEGrp
            xJobQueue.DelZESpdData(False, False);
            //??Nue  jtDelZEGrpData
            if mJob^.StopGrpEv.SettingsRec.ComputerName <> '' then begin
              // Job initialized by an application (GUI)
              xJobQueue.GenShiftData(False, False);
              xJobQueue.SetSettings(False, False, cAssignProdGrpIDOnly, smMMStop, psInconsistent);
              xJobQueue.SendMsgToAppl(False, False, rmProdGrpStoppedOk);
            end
            else begin
              xJobQueue.GenShiftData(False, False);
            end;
          end;
          xJobQueue.SendEventToClients(False, True, amMachineStateChange);
          xJobQueue.Next;
        end; //while
      end; //if
      Result := rmProdGrpStoppedOk;
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProcStopGrpEv failed (1).' + e.message);
        WriteLog(etError, fError.msg);
        Result := rmNone;
      end;
    end;
  finally
    FreeAndNil(mBuildJobQueue);
  end;
end;

//-----------------------------------------------------------------------------
function TJobManager.ProcStartGrpEv: TResponseMsgTyp;
var
  xJobID: DWord;
  xJob: PJobRec;
  xBadProdGrpState: TResponseMsgTyp;
  xJobState: TJobState;
  xLastSpindle: Word;
  xJobQueue: TBuildJobQueueStartStop;
begin
  Result         := rmNone;
  mBuildJobQueue := nil;
  mJob^.NetTyp   := GetNetTyp(mJob^.StartGrpEv.MachineID);

// XML: Warum wird mit Kopie gearbeitet? -> Weil sp�ter auf den OriginalJob zugegriffen werden muss. (Daten�bernahme in neue ProdGrp!)
  CloneJob(mJob, xJob);
  try // try..finally �ber alles wegen Aufruf von FreeMem() f�r xJob
    // Check spindle range on machine
    with mDB.Query[cPrimaryQuery] do
    try
      Close;
      SQL.Text := cQrySelMaxSpd;
      Params.ParamByName('c_machine_id').AsInteger := xJob^.StartGrpEv.MachineID;
      Open;
      if FindFirst then begin // ADO Conform
        xLastSpindle := FieldByName('c_spindle_id').AsInteger;
        Close;
        if (xJob^.JobTyp = jtStartGrpEv) AND
          ((xJob^.StartGrpEv.SettingsRec.SpindleFirst > xLastSpindle) or (xJob^.StartGrpEv.SettingsRec.SpindleLast > xLastSpindle)) then begin
          // Job initialized by an application (GUI)
          Result := rmBadSpdRange;
          EXIT;
        end
        else begin
          if xJob^.StartGrpEv.SettingsRec.SpindleFirst > xLastSpindle then begin
            Result := rmBadSpdRange;
            EXIT;
          end;
          if xJob.StartGrpEv.SettingsRec.SpindleLast > xLastSpindle then begin
            WriteLog(etWarning, Format('Bad spindle range(%d-%d) from machineID %d from ZE. Last spindle modified to %d!',
                                       [xJob^.StartGrpEv.SettingsRec.SpindleFirst, xJob^.StartGrpEv.SettingsRec.SpindleLast,
                                        xJob^.StartGrpEv.MachineID, xLastSpindle]));
            xJob^.StartGrpEv.SettingsRec.SpindleLast := xLastSpindle;
          end;
        end; // if xJob^.JobqrTyp
      end; // if FindFirst
    except
// DONE: wss: exception wird abgefangen, trotzdem weitermachen? - Nein, wir machen hier neu ein EXIT. (nue)
      on e: Exception do begin
        Result := rmNone;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProcStartGrpEv failed while checking spindle range.' + e.message);
        WriteLog(etError, fError.msg);
        EXIT;
      end;
    end; //with

    try
//      mBuildJobQueue := TBuildJobQueueStartStop.Create(Self, mDB.Database, mDB.Query[cPrimaryQuery], mDB.Query[cThirdQuery], Params,
//                                                       xJob^.JobTyp, xJob^.StartGrpEv, xBadProdGrpState);

      codesite.SendFmtMsg('Vor TBuildJobQueueStartStop.Create: SpeedRamp:%d, LengthWindow:%d',[Integer(xJob^.StartGrpEv.SettingsRec.SpeedRamp),xJob^.StartGrpEv.SettingsRec.LengthWindow]);
      mBuildJobQueue := TBuildJobQueueStartStop.Create(Self, mDB.Database, mDB.Query[cPrimaryQuery], mDB.Query[cThirdQuery], Params,
                                                        xJob, xBadProdGrpState);
      xJobQueue := TBuildJobQueueStartStop(mBuildJobQueue);
      if xBadProdGrpState <> rmProdGrpStartedOk then begin //Stop action because ProdGrp in invalid state to access (not in (1,3,4))
        if xJobQueue.MachineOffline then
          Result := rmMachineOffline
        else
          Result := xBadProdGrpState;
        EXIT;
      end;
    except
      on e: Exception do begin
        Result := rmNone;
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'TBuildJobQueueStartStop.Create in ProcStartGrpEv failed.' + e.message);
        WriteLog(etError, fError.msg);
      end;
    end;

    try
      if (xJobQueue.EOF) or (xJob.JobTyp = jtStartGrpEvFromMa) then
        xJobState := jsValid
      else
        xJobState := jsInvalid;

      if (xJob.JobTyp = jtStartGrpEv) then begin
        // Job initialized by an application (GUI)

        if not xJobQueue.DataCollection then begin
          xJobQueue.SetSettings(True, False, cAssignGroup, smMM, psOrdinaryAquis);
          xJobQueue.SendEventToClients(False, False, amMachineStateChange);
          xJobID := xJobQueue.SendMsgToAppl(False, True, rmProdGrpStartedOk, xJobState);
        end
        else begin
          case xJobQueue.mNetType of
            ntWSC: begin
                xJobQueue.SetSettings(True, False, cAssignGroup, smMM, psOrdinaryAquis);
                xJobQueue.SendEventToClients(False, False, amMachineStateChange);
                xJobID := xJobQueue.SendMsgToAppl(False, True, rmProdGrpStartedOk, xJobState);
              end
          else
            xJobQueue.CheckAdditionalProdGrpToRestart(True, False, cAssignGroup, smRange, psOrdinaryAquis);
            xJobQueue.SetSettings(True, False, cAssignGroup, smMM, psOrdinaryAquis);
            xJobQueue.SendEventToClients(False, False, amMachineStateChange);
            xJobID := xJobQueue.SendMsgToAppl(False, True, rmProdGrpStartedOk, xJobState);
          end; //case
        end; // if NOT
      end
      else begin
        // Job initialized by machine
        case xJobQueue.mNetType of
          ntWSC: begin
              xJobQueue.SetSettings(True, False, cAssignProdGrpIDOnly, smZE, psInconsistent);
              xJobQueue.SendEventToClients(False, True, amMachineStateChange, xJobState);
            end
        else
          xJobQueue.CheckAdditionalProdGrpToRestart(True, False, cAssignProdGrpIDOnly, smRange, psInconsistent, xJobState);
          xJobQueue.SetSettings(True, False, cAssignProdGrpIDOnly, smZE, psInconsistent);
          xJobQueue.SendEventToClients(False, True, amMachineStateChange, xJobState);
          // Bei ZE ist zu diesem Zeitpunkt immer ein Stop auf die laufende ProdGrp ausgefuehrt
          // worden beim jtAssign. Daher muss hier nicht nochmals aquiriert werden!
        end; // case
        Result := rmProdGrpStartedOk;
        EXIT;
      end; // if (xJob.JobTyp = jtStartGrpEv)

      if xJobQueue.FindFirst then begin
        // bis zum letzen Eintrag sind alle Jobs invalid. Erst beim letzten wird der Status auf valid gesetzt
        xJobState := jsInvalid;
        while not xJobQueue.EOF do begin // ADO Conform
          xJobQueue.SetExp;
          xJobQueue.UnDoGetZESpdData(True, False);
          xJobQueue.GetDataStopZESpd(True, False);
          xJobQueue.DelZESpdData(False, False);
          xJobQueue.SuccessorJob := xJobID;
          // n�chster Datensatz selektieren
          xJobQueue.Next;
          // Ups!! Ist schon das Ende da?
          if xJobQueue.EOF then
            // Jupp, JobState auf Valid setzen, damit die Maschinerie weiterlaufen kann -> JobController
            xJobState := jsValid;

          // Okokok, dann  1 Zur�ck um f�r diesen Eintrag die Schichtdaten zu generieren
          xJobQueue.Prior;
          xJobID := xJobQueue.GenShiftData(False, True, xJobState);

          // nun doch wieder weitermachen mit n�chstem Eintrag, damit EOF wieder funktioniert
          xJobQueue.Next;
        end; //while
      end; //if xJobQueue.FindFirst

      Result := rmProdGrpStartedOk;

    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'ProcStartGrpEv failed (1).' + e.message);
        WriteLog(etError, fError.msg);
        Result := rmNone;
      end;
    end;
  finally
    if Assigned(xJobQueue) then begin
      CopyJob(xJob, mJob); // Restore mJob
      FreeMem(xJob);
      FreeAndNil(mBuildJobQueue);
      xJobQueue := Nil;
    end;
  end; // CloneJob(mJob, xJob), try
end;

//-----------------------------------------------------------------------------

function TJobManager.SameMachineState(aMachineID: Word; aOldMachState: LongInt): Boolean;
const
  cxQryCheckNodeState = 'SELECT c_node_stat FROM t_net_node WHERE c_node_id=:c_node_id';
  cxQryCheckMachState = 'SELECT c_machine_state, c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id';
//  'SELECT * FROM t_net_node '+
//    'WHERE (c_node_id like (SELECT c_node_id FROM t_machine WHERE c_machine_id=:c_machine_id)) AND '+
//    'c_node_id not like ''0'' AND '+    //Nue added 9.4.2001 because of consistency with Maconfig (Connect/Disconnect Maschine/Node
//    'c_node_stat=:c_node_stat ';
(*  'SELECT * FROM t_machine WHERE c_machine_id=:c_machine_id AND '+
  'c_machine_state=:c_machine_state ';*)
var
  xNodeId: string;
begin
  // Unterbinden ung�ltige aMachineID Nue:18.03.03
  Result := (aMachineID = 0);
  if not Result then begin
    with mDB.Query[cPrimaryQuery] do
    try
      Close;
      SQL.Text := cxQryCheckMachState;
      Params.ParamByName('c_machine_id').AsInteger := aMachineID;
      Open;
      if FindFirst then begin
        if (FieldByName('c_machine_state').AsInteger <= ORD(msUndefined)) then begin //Nue:24.2.03
          xNodeId  := FieldByName('c_node_id').AsString;
          Close;
          SQL.Text := cxQryCheckNodeState;
          Params.ParamByName('c_node_id').AsString := xNodeId;
          Open;
          if FindFirst then
            if (FieldByName('c_node_stat').AsInteger = ORD(nsOffline)) and (aOldMachState = ORD(msOffline)) then
              Result := True;
        end
        else
          Result := (FieldByName('c_machine_state').AsInteger = aOldMachState);
      end; //if FindFirst
    except
      on e: Exception do begin
        fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'JobManager.SameMachineState' + e.message);
        WriteLog(etError, fError.msg);
      end;
    end; // if not Result, try
  end;
end;

//-----------------------------------------------------------------------------
function TJobManager.Startup: Boolean;
var
  xFirst: Boolean;
  xOldNetTyp, xTmpNetTyp: TNetTyp;
  xIPNodeCount: Word;
  xNetTypSet: set of TNetTyp;
  xJob: TJobRec;
  //.......................................................
  procedure CreateNodeListJobs(aNetTyp: TNetTyp);
  var
    xSetNodeListJob: TBaseJob;
  begin
    // New SetNodeListJob entry in JobList
    xJob.JobTyp := jtSetNodeList;
    xJob.NetTyp := aNetTyp;
    xSetNodeListJob := TSetNodeListJob.Create(mJobList, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.DataBase, aNetTyp);
    // add Job GetNodeList to queue list.
    // Folgejob GetNodeList an SetNodeList Job anh�ngen
    xJob.JobTyp := jtGetNodeList;
    FillChar(xJob.GetNodeList.NodeItems, sizeof(xJob.GetNodeList.NodeItems), 0);
    //wss: dieser Job wird als Folgejob von xSetNodeListJob in Queue eingef�gt
    TGetNodeListJob.Create(mJobList, @xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database,
                           Self.Params, mMaEqualizeJobCritSec, aNetTyp, xSetNodeListJob);

    // diese JobQueue in JobListe eintragen
    xSetNodeListJob.AddSelfToJobList;
    // xJob Buffer wieder l�schen
    FillChar(xJob.SetNodeList.NodeItems, sizeof(xJob.SetNodeList.NodeItems), 0);

    // und JobController benachrichtigen, dass neue Eintr�ge in der JobList vorhanden sind
    xJob.JobID := xSetNodeListJob.JobID;
    xJob.JobTyp := jtNewJobEv;
    WriteJobBufferTo(ttJobController, @xJob);

//    // New SetNodeListJob entry in JobList
//    mJob^.JobTyp := jtSetNodeList;
//    mJob^.NetTyp := aNetTyp;
//    xSetNodeListJob := TSetNodeListJob.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.DataBase, aNetTyp);
//    // add Job GetNodeList to queue list.
//    // Folgejob GetNodeList an SetNodeList Job anh�ngen
//    mJob^.JobTyp := jtGetNodeList;
//    FillChar(mJob^.GetNodeList.NodeItems, sizeof(mJob^.GetNodeList.NodeItems), 0);
//    //wss: dieser Job wird als Folgejob von xSetNodeListJob in Queue eingef�gt
//    TGetNodeListJob.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database,
//                           Self.Params, mMaEqualizeJobCritSec, aNetTyp, xSetNodeListJob);
//
//    // diese JobQueue in JobListe eintragen
//    xSetNodeListJob.AddSelfToJobList;
//
//    // und JobController benachrichtigen, dass neue Eintr�ge in der JobList vorhanden sind
//    mJob^.JobID := xSetNodeListJob.JobID;
//    mJob^.JobTyp := jtNewJobEv;
//    WriteJobBufferTo(ttJobController);
//    // mJob Buffer wieder l�schen
//    FillChar(mJob^.SetNodeList.NodeItems, sizeof(mJob^.SetNodeList.NodeItems), 0);
  end;
  //.......................................................
begin
  Result := True;
  xFirst := False;
  xIPNodeCount := cFirstMachPos;
  // diese NetTyp m�ssen behandelt werden, egal ob es Maschinen auf DB hat oder nicht
  xNetTypSet := [ntTXN, ntWSC];
  //Clean up DB table v_prodgroup_state
  with mDB.Query[cPrimaryQuery] do
  try
    //Check table t_prodgroup
    Close;
    SQL.Text := cQrySelGrpStateAtStartup1;
    Open;
    if FindFirst then begin
      xFirst := True;
      while not EOF do begin
        WriteLog(etWarning, Format('Bad c_prod_state (%s) in t_prodgroup for ProdID:%d, Machine:%d, Spindle:%d-%d. ProdID deleted for states(0,2).',
                                   [cProdGrpStateArr[TProdGrpstate(FieldByName('c_prod_state').AsInteger)],
                                    FieldByName('c_prod_id').AsInteger, FieldByName('c_machine_id').AsInteger,
                                    FieldByName('c_spindle_first').AsInteger, FieldByName('c_spindle_last').AsInteger]));
        Next;
      end;
    end;

    //Check table v_prodgroup_state
    Close;
    SQL.Text := cQrySelGrpStateAtStartup2;
    Open;
    if FindFirst then begin
      xFirst := True;
      while not EOF do begin
        WriteLog(etWarning, Format('Bad c_prod_state (%s) in v_prodgroup_state for ProdID:%d, Machine:%d, Spindle:%d-%d. ProdID deleted for states(0,2) or c_prod_state updated for states(5,6).',
                                   [cProdGrpStateArr[TProdGrpstate(FieldByName('c_prod_state').AsInteger)],
                                    FieldByName('c_prod_id').AsInteger, FieldByName('c_machine_id').AsInteger,
                                    FieldByName('c_spindle_first').AsInteger, FieldByName('c_spindle_last').AsInteger]));
        Next;
      end;
    end;

    if xFirst then begin
      mDB.DataBase.StartTransaction;
      try
        Close;
        //Update or delete entries in v_prodgroup_state (dependant on c_prod_state)
        SQL.Text := cQrySetOnlyGrpStateBackAtStartup;
        ExecSQL;
        Close;
        SQL.Text := cQryDelProdGrpStateStoppedStartup; //Nue 13.06.00
        ExecSQL;
      except
        on e: Exception do begin
          fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Cleanup (t_prodgroup and t_prodgroup_state) failed' + e.message);
          mDB.DataBase.Rollback;
          raise EMMException.Create(fError.Msg);
          Result := False;
        end;
      end;
      mDB.DataBase.Commit;
    end; // if xFirst

    //Get Net-Nodes and prepare new jobs
    Close;
    SQL.Text := cQrySelNetNodes;
    Open;

    if FindFirst then begin
//      FillChar(mJob^.SetNodeList.NodeItems, sizeof(mJob^.SetNodeList.NodeItems), 0);
      FillChar(xJob.SetNodeList.NodeItems, sizeof(xJob.SetNodeList.NodeItems), 0);
       // dieser Nodetyp wird als erster bearbeitet: Momentan TXN oder WSC
      xOldNetTyp := TNetTyp(FieldByName('c_net_id').AsInteger);
      xTmpNetTyp := xOldNetTyp;
      while not EOF do begin
        // Informationen von aktuellem Record in entsprechende Struktur abf�llen
        case xOldNetTyp of
          ntTXN: begin
//              with mJob^.SetNodeList.NodeItems.TxnNetNodeList[FieldByName('c_node_id').AsInteger] do begin
              with xJob.SetNodeList.NodeItems.TxnNetNodeList[FieldByName('c_node_id').AsInteger] do begin
                AdapterNo := FieldByName('c_adapt_or_name').AsInteger; // TexnetadapterNo 1..3
                MachID    := FieldByName('c_machine_id').AsInteger; // Maschinen ID
                MachState := nsOffline; // All machines to offline; Online machines will be sent by the handler
                MapName   := FieldByName('c_mapfile_name').AsString;
              end;
            end;
          ntWSC:
//            with mJob^.SetNodeList.NodeItems.WscNetNodeList[xIPNodeCount] do begin
            with xJob.SetNodeList.NodeItems.WscNetNodeList[xIPNodeCount] do begin
              StrPCopy(IpAdresse, FieldByName('c_node_id').AsString);
              MachID    := FieldByName('c_machine_id').AsInteger; // Maschinen ID
              MachState := nsOffline; // All machines to offline; Online machines will be sent by the handler
              MapName   := FieldByName('c_mapfile_name').AsString;
              Overruled := NOT(MapName='');
            end;
          ntLX:
//            with mJob^.SetNodeList.NodeItems.LXNetNodeList[xIPNodeCount] do begin
            with xJob.SetNodeList.NodeItems.LXNetNodeList[xIPNodeCount] do begin
              StrPCopy(IpAdresse, FieldByName('c_node_id').AsString);
              MachID    := FieldByName('c_machine_id').AsInteger; // Maschinen ID
              MachState := nsOffline; // All machines to offline; Online machines will be sent by the handler
              MapName   := FieldByName('c_mapfile_name').AsString;
              Overruled := NOT(MapName='');
            end;
          ntCI: ;
        else
        end;
         // wird eigentlich nur f�r WSC verwendet
        INC(xIPNodeCount);
        Next;
        // wenn noch nicht EOF dann den Nodetyp vom n�chsten feld bereits auslesen
        if not EOF then // ADO Conform
          xTmpNetTyp := TNetTyp(FieldByName('c_net_id').AsInteger);

        if (xOldNetTyp <> xTmpNetTyp) or EOF then begin // ADO Conform
          CreateNodeListJobs(xOldNetTyp);
          Exclude(xNetTypSet, xOldNetTyp);
          xIPNodeCount := 0;
          xOldNetTyp    := xTmpNetTyp;
        end; // if <> NetTyp

      end; // while not EOF
    end;

    // nun noch f�r diejenigen Netztypen, wo keine Maschine auf DB gefunden wurde
    // eine leere NodeList versenden
    for xTmpNetTyp := ntTXN to ntWSC do begin
      if xTmpNetTyp in xNetTypSet then
        CreateNodeListJobs(xTmpNetTyp);
    end;
  except
    on e: Exception do begin
      Result := False;
      fError := SetError(ERROR_INVALID_FUNCTION, etDBError, 'Startup failed.' + e.message);
      WriteLog(etError, fError.msg);
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TJobManager.DoConnect: Boolean;
begin
  Result := inherited DoConnect;
end;
//-----------------------------------------------------------------------------
function TJobManager.Init: Boolean;
begin
  // Nue:14.11.01 Wird aufgrund von Registry-Eintrag gesetzt oder nicht (Kann mit DebugConfig-Tool manipuliert werden.  DebugMode := True;
  try
    mAccessGroup := Params.Value[cUserGroup];
  except
    WriteLog(etError, 'Reading security entries in registry failed.');
  end;
  Result := inherited Init;
end;
//-----------------------------------------------------------------------------
procedure TJobManager.ProcessInitJob;
begin
  case mJob^.JobTyp of
    jtTest: begin
        ProcessJob;
      end;

    // jtDataAquEv, in Init nicht zugelassen
    // jtMaEqualize, in Init nicht zugelassen
    jtGetNodeList,
    jtMaOffline,
    jtMaOnline,
    jtSetSettings, // Used because it will be call during MaEqualize
    jtSynchronize: begin
        ProcessJob;
      end;

    jtInitialized: begin
        //Senden von gcInitialized an MM_Guard um System von ProcessInitJob zu ProcessJob zu avancieren.
        if not WriteToMain(gcInitialized, NO_ERROR) then begin
          WriteLog(etError, Format('ConfirmStartup failed: %d', [IPCClientIndex[cMainChannelIndex]^.Error]));
          Exit;
        end;
{ TODO 2 -oNue -cSystem :
Einfuegen, das "DefaultMemoryC"-Settings von der Klasse YMUtils geholt werden.
Wenn kein Eintrag mit ID 1 auf DB einen INSERT machen, sonst einen UPDATE.
Da Kr die Werte in YMUtils wartet, werden es so immer die aktuellen Werte sein.
Die Methoden um die Daten aus dem Typ auf die DB abzulebgen, muessen noch kodiert werden! }
        //Nochmaliges Senden des Initialized-Job, bis das System im Zustand psRun ist. Dann
        // wird unter jtInitialized in ProcessJob das ProcAndAnalyse ausgefuehrt.
        //Das mehrmalige Senden von gcInitialized an WriteToMain ist egal.
        WriteJobBufferTo(ttJobManager);
      end;

    jtBroadcastToClients: ; //Nue:2.12.02    Shouldn't happen here
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessInitJob;
  end;
end;
//-----------------------------------------------------------------------------

procedure TJobManager.ProcessJob;
var
  xBaseJob: TBaseJob;
  xJob: PJobRec;
  xx, xxNumJobs: Word;
  xBool: Boolean;
  xInt: Integer;
  xResponseMsg: TResponseRec;
begin
//  OutputDebugString(PChar(cEventLogDefaultText[mThreadDef.ThreadTyp]));
{@@Nue on work 21.05.01
      jtAdjust
      jtAssign
      jtAssignComplete
      jtClearZEGrpData
      jtClearZESpdData
      jtDelZEGrpData
      jtDelZESpdData
      jtEntryLocked
      jtEntryUnlocked
      jtGenShiftData
      jtGenExpShiftData
      jtGetDataStopZEGrp
      jtGetDataStopZESpd
      jtGetExpDataStopZEGrp
      jtGetExpDataStopZESpd
      jtGetExpZEGrpData
      jtGetExpZESpdData
      jtGetZEGrpData
      jtGetZESpdData
      jtGetMaAssign
      jtGetMaConfig
      jtGetSettings
      jtInitialReset
      jtMaNotFound
      jtMaOffline
      jtMaOnline
      jtReset
      jtSetMaConfig
      jtSetSettings
      jtStartGrpEv
      jtStopGrpEv
      jtSynchronize
      jtUplAutoMaData
      jtViewZEGrpData
      jtViewZESpdData
      jtUnDoGetZESpdData
//      jtSetProdID
      jtStartGrpEvFromMa
      jtStopGrpEvFromMa
      jtProdGrpUpdate
      jtGetSettingsAllGroups
      jtMaEqualize
}

//  xBaseJob := Nil;
  try
    case mJob^.JobTyp of
      jtNone, jtTest, jtTestConfirm, jtTestStore:;
      jtDelJobID: begin
          WriteJobBufferTo(ttJobController);
        end;

      jtDiagnostic: begin
          mJobList.GetNumJobs(jsAll, xxNumJobs);
          WriteLogDebug(Format('Jobs in joblist.JobManager: xxNumJobs %d ', [xxNumJobs]));
          if xxNumJobs > 0 then
            for xx := 0 to mJobList.Count - 1 do begin
              with PJobListItem(mJobList.Items[xx])^ do
                WriteLogDebug(Format('Jobs in joblist.JobManager:: Count: %d, JobId: %d, PJob: %p, Job.JobRec.JobID: %d, Job.JobRec.JobTyp: %s ',
                                     [mJobList.Count, JobID, @Job, Job.JobRec^.JobID, GetJobName(Job.JobRec^.JobTyp)]));
            end;
        end;

      jtAdjust: begin
          if not IsDataCollectionOn(mJob^.Adjust.MachineID) then EXIT;
          WriteLog(etInformation, Format('Adjust from machine %d received', [mJob^.Adjust.MachineID]));
        end;

      jtAssign: begin // Arrives for every Assign on the ZE
          if not IsDataCollectionOn(mJob^.Assign.MachineID) then EXIT;
          ProcAssignFromZE;
        end;

      // Arrives if an Assignment-Set has finished (forced by Key locked or forced by Net-Handler)
      jtAssignComplete: begin
          if not IsDataCollectionOn(mJob^.AssignComplete.MachineID) then EXIT;

//        for xx:=0 to cZESpdGroupLimit-1 do begin
//          with mJob^.MaEqualize.Assigns.Groups[xx] do begin
//            codesite.SendFmtMsg('jtAssignComplete: Mach %d Grp%d: Modify=%d, GroupState=%d, ProdId=%d, SpdFirst=%d, SpdLast=%d, ProdName=%s',
//               [mJob^.MaEqualize.MachineID, xx, Word(Modified), ORD(GroupState), ProdId, SpindleFirst, SpindleLast, StrPas(@mJob^.MaEqualize.Assigns.Groups[xx].ProdGrpParam.ProdGrpInfo.c_prod_name) ]);
//          end;
//        end;

          // MaschinenID zwischenspeichern
          xInt := mJob^.AssignComplete.MachineID;
          //Send the data directly as a TMaEqualizeJob to JobManager
          mJob^.JobTyp := jtMaEqualize;
          // Send to JobManager that it looks like it comes from ZE
          xBaseJob := TMaEqualizeJob.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database,
                                            Self.Params, mMaEqualizeJobCritSec, grZEAssigComplete, ttJobManager);
          mJob^.JobTyp                       := jtSendEventToClients;
          mJob^.SendEventToClients.Event     := amMachineStateChange;
          mJob^.SendEventToClients.MachineID := xInt;
          //wss: dieser Job wird als Folgejob von xBaseJob (=jtMaEqualize) eingef�gt
          TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager, xBaseJob);
          // Add to JobList
          xBaseJob.AddSelfToJobList;
          xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
        end;

      jtBroadcastToClients: begin
//wss        xJob := mJob;
          case mJob^.BroadcastToClients.Event of
            bcClientStarted: begin
                mJob^.JobTyp                       := jtSendEventToClients;
                mJob^.SendEventToClients.Event     := amSystemInfo;
                mJob^.SendEventToClients.MachineID := 0; // Dummy
                with TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, mJob, cNrOfTrials,
                                                   mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager) do begin
                  AddSelfToJobList;
                  WriteBufferTo(ttJobController, jtNewJobEv);
                end;
//              xBaseJob := TSendEventToClientsJob.Create(mJobList, jtSendEventToClients, mJob, cNrOfTrials,
//                                                 mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager);
//              // Add to JobList
//              xBaseJob.AddSelfToJobList;
//              xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
              end;
          else
          end;
        end;

      jtDataAquEv: begin // 21.7.99
          if mJob^.DataAquEv.MulMaData then begin
   { TODO 2 -oNue -cSystem : MulMaData Teil muss hier noch codiert werden }
          end;
          if not ProcDataAquEv then
          //Second trial
            if not ProcDataAquEv then begin
              WriteLog(etError, Format('Last trial for ProcDataAquEv failed: FragID:%d, IntID:%d',
                [mJob^.DataAquEv.FragID, mJob^.DataAquEv.IntID]));
            end;
        end;

      jtDelInt: begin
          xBaseJob := TDelIntJob.Create(mJobList, jtDelInt, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager);
          JobToJobList(xBaseJob);
        end;

      jtDelShiftByTime: begin
          xBaseJob := TDelShiftByTimeJob.Create(mJobList, jtDelShiftByTime, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager);
          JobToJobList(xBaseJob);
        end;

      jtEntryLocked: begin
        end;

      jtEntryUnlocked: begin
        end;

      jtMaEqualize: begin //ok 3.8.99
EnterMethod('JobManager: jtMaEqualize');
          if not IsDataCollectionOn(mJob^.MaEqualize.MachineID) then EXIT;
          if not ProcAnalyseAndFinish('jtMaEqualize') then
          //Second trial
            if not ProcAnalyseAndFinish('jtMaEqualize') then begin
              WriteLog(etError, Format('Last trial for ProcAnalyseAndFinish (jtMaEqualize) failed (Job not found in JobList!): MachineID:%d, JobID: %d',
                [mJob^.MaEqualize.MachineID, mJob^.JobID]));
            end;
        end;

      jtGetMaAssign: begin //ok 3.8.99
          if not ProcAnalyseAndFinish('jtGetMaAssign') then
          //Second trial
            if not ProcAnalyseAndFinish('jtGetMaAssign') then begin
              WriteLog(etError, Format('Last trial for ProcAnalyseAndFinish (jtGetMaAssign) failed: MachineID:%d, JobID: %d -> Restart MM',
                [mJob^.GetMaAssign.MachineID, mJob^.JobID]));
            end;
        end;

      jtGetNodeList: begin //ok 3.8.99
          if not ProcAnalyseAndFinish('jtGetNodeList') then
            //Second trial
            if not ProcAnalyseAndFinish('jtGetNodeList') then begin
              WriteLog(etError, 'Last trial for ProcAnalyseAndFinish (jtGetNodeList) failed!');
            end;
        end;

      //wss: wird vom Assignmetn ausgel�st. Im MsgController werden einzelne Meldungen vom Typ GetSettings verschickt
      // und im StorageHandler gesammelt.
      jtGetSettingsAllGroups: begin
          // MachID zwischenspeichern...
          xInt := mJob^.GetSettingsAllGroups.MachineID;
          // XMLData Array auf sicher leeren
          mJob^.GetSettingsAllGroups.SettingsRec.XMLData[0] := #00;
          case mJob^.NetTyp of
            // bei Texnet muss erste in MaAssign Job erfolgen, damit wir wissen welche Spindelbereiche �berhaupt g�ltig sind
            // Anschliessend wird darauf dann reagiert (im AnalyseJobData) und ein GetSettingsAllGroups Job erstellt
            ntTXN: begin
                mJob^.JobTyp                := jtGetMaAssign;
                mJob^.GetMaAssign.MachineID := xInt; //...und hier wieder setzen
                // aus der antwort von diesem Job wird dann ein GetSettingsAllGroups erstellt.
                xBaseJob := TGetMaAssignJob.Create(mJobList, mJob, mJob^.GetSettingsAllGroups, cNrOfTrials, mDB.Query[cSecondaryQuery],
                  mDB.Database);
                JobToJobList(xBaseJob);
              end;
            // beim Informator wird direkt die fixen Gruppen angefragt.
            ntWSC: begin
//                FillChar(mJob^.GetSettingsAllGroups.SpindleRanges, sizeof(mJob^.GetSettingsAllGroups.SpindleRanges), 0);
                xBaseJob := TGetSettingsAllGroupsJob.Create(mJobList, mJob, mJob^.GetSettingsAllGroups, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database);
                JobToJobList(xBaseJob);
              end;
          else
            WriteLog(etError, 'Unknown NetTyp in job jtGetSettingsAllGroups!');
          end; // case
        end;
//      jtGetSettingsAllGroups:begin
//        xJob := mJob;
//        xInt := mJob^.GetSettingsAllGroups.MachineID;
//        case mJob^.NetTyp of
//          ntTXN : begin
//            xJob.JobTyp := jtGetMaAssign;
//            xJob.GetMaAssign.MachineID := mJob^.GetSettingsAllGroups.MachineID;
//            xBaseJob := TGetMaAssignJob.Create(mJobList, xJob, mJob^.GetSettingsAllGroups, cNrOfTrials, mDB.Query[cSecondaryQuery],
//                                                        mDB.Database, xBaseJob);
//            JobToJobList(xBaseJob);
//          end;
//          ntWSC : begin
//            FillChar(xJob.GetSettingsAllGroups.SpindleRanges, sizeof(xJob.GetSettingsAllGroups.SpindleRanges),0);
//            xBaseJob := TGetSettingsAllGroupsJob.Create(mJobList, xJob, mJob^.GetSettingsAllGroups, cNrOfTrials, mDB.Query[cSecondaryQuery],
//                                                        mDB.Database, NIL);
//            JobToJobList(xBaseJob);
//          end;
//          else
//            WriteLog(etError, 'Unknown NetTyp in job jtGetSettingsAllGroups!');
//        end;
//      end;

      jtInitialized: begin
        //This Job has to be here because of we have more then one NetTyps, the job would be thrown away
        // and couldn't set the dependent MaEqualize on valid (Nue 1.5.2000
          codesite.SendFmtMsg('jtInitialized: JobID=%d', [mJob^.JobID]);
          ProcAnalyseAndFinish;
        end;

      jtInitialReset: begin
          WriteLog(etInformation, Format('InitialReset received from machine: MachineID:%d',
            [mJob^.InitialReset.MachineID]));
          if not ProcMaOnline(grZEColdStart, 'InitialReset') then
          //Second trial
            if not ProcMaOnline(grZEColdStart, 'InitialReset') then begin
              WriteLog(etError, Format('Last trial for ProcMaOnline (InitialReset on machine) failed: MachineID:%d',
                [mJob^.MaOnline.MachineID]));
            end;
        end;

      jtMaNotFound: begin
        end;

      jtMaOffline: begin
        //Every 3 minutes the nethandlers send jtMaOffline- or jtMaOnline-Msg for every
        // machine, undependent of it has changed his state
  //      if not SameMachineState(mJob^.MaOffline.MachineID,ORD(nsOnline)) then begin  //DB-Access
          if not SameMachineState(mJob^.MaOffline.MachineID, ORD(msOffline)) then begin //DB-Access
            WriteLog(etInformation, Format('OFFLINE received from machine: MachineID:%d',
              [mJob^.MaOffline.MachineID]));
            if not ProcMaOffline then
            //Second trial
              if not ProcMaOffline then begin
                WriteLog(etError, Format('Last trial for ProcMaOffline failed: MachineID:%d',
                  [mJob^.MaOffline.MachineID]));
              end;
          end;
        end;

      jtMaOnline: begin
        //Every 3 minutes the nethandlers send jtMaOffline- or jtMaOnline-Msg for every
        // machine, undependent of it has changed his state
  //      if not SameMachineState(mJob^.MaOnline.MachineID,ORD(nsOffline)) then begin  //DB-Access
  //      if SameMachineState(mJob^.MaOnline.MachineID,ORD(msOffline)) then begin  //DB-Access
          xBool := SameMachineState(mJob^.MaOnline.MachineID, ORD(msOffline));
          if xBool then begin //DB-Access
            WriteLog(etInformation, Format('ONLINE received from machine: MachineID:%d',
              [mJob^.MaOnline.MachineID]));
            if not ProcMaOnline(grZEOnline) then
            //Second trial
              if not ProcMaOnline(grZEOnline) then begin
                WriteLog(etError, Format('Last trial for ProcMaOnline failed: MachineID:%d',
                  [mJob^.MaOnline.MachineID]));
              end;
          end;
        end;

      jtMulGrpDataEv: begin
        end;

      jtProdGrpUpdate: begin
          xBaseJob := TProdGrpUpdateJob.Create(mJobList, jtProdGrpUpdate, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database, ttQueueManager);
          JobToJobList(xBaseJob);
        end;

      jtReset: begin
          WriteLog(etInformation, Format('Reset received from machine: MachineID:%d',
            [mJob^.Reset.MachineID]));
        end;

      jtSetSettings: begin
          if not ProcAnalyseAndFinish('jtSetSettings') then
          //Second trial
            if not ProcAnalyseAndFinish('jtSetSettings') then begin
              WriteLog(etError, 'Last trial for ProcAnalyseAndFinish (jtSetSettings) failed!');
            end;
        end;

      jtSetProdID: begin
        end;

      jtStartGrpEv: begin
        // Job initialized by an application
codesite.SendFmtMsg('ProcessJob1:jtStartGrpEv , JobLen:%d %s', [mJob^.JobLen, StrPas(mJob^.StartGrpEv.SettingsRec.XMLData)]);
          WriteLog(etInformation, Format('StartGrpEv arrived (Jobtyp=%s) Mach:%d Spd:%d-%d MMUser:%s YMSetName:%s', [GetJobName(mJob^.JobTyp),
            mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SettingsRec.SpindleFirst, mJob^.StartGrpEv.SettingsRec.SpindleLast, mJob^.StartGrpEv.SettingsRec.MMUserName, string(mJob^.StartGrpEv.SettingsRec.YMSetName)]));
//DONE wss: XML: kontrollieren
          CloneJob(mJob, xJob);
          with xJob^, SendMsgToAppl do
          try
            JobTyp := jtSendMsgToAppl;
codesite.SendFmtMsg('ProcessJob2:jtStartGrpEv , JobLen:%d %s', [mJob^.JobLen, mJob^.StartGrpEv.SettingsRec.XMLData]);
            ResponseMsg.MsgTyp := ProcStartGrpEv;
codesite.SendFmtMsg('ProcessJob3:jtStartGrpEv , JobLen:%d %s', [mJob^.JobLen, mJob^.StartGrpEv.SettingsRec.XMLData]);
            if ResponseMsg.MsgTyp <> rmProdGrpStartedOk then begin
              WriteLog(etWarning, Format('ProcStartGrpEv (Jobtyp=%s) failed: MachID:%d, SpindFirst:%d, SpindLast:%d',
                [GetJobName(mJob^.JobTyp), mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SettingsRec.SpindleFirst, mJob^.StartGrpEv.SettingsRec.SpindleLast]));
              ResponseMsg.MachineID := mJob^.StartGrpEv.MachineID;
              ComputerName := mJob^.StartGrpEv.SettingsRec.ComputerName;
              Port := mJob^.StartGrpEv.SettingsRec.Port;
              ResponseMsg.SpindleFirst := mJob^.StartGrpEv.SettingsRec.SpindleFirst;
              ResponseMsg.SpindleLast := mJob^.StartGrpEv.SettingsRec.SpindleLast;
              ResponseMsg.ProdGrpID := mJob^.StartGrpEv.SettingsRec.ProdGrpID;
            // add Job SendMsgToAppl to queue list.
              with TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database) do begin
              // add queue SendMsgToAppl to job list.
                AddSelfToJobList;
                WriteBufferTo(ttJobController, jtNewJobEv);
              end;

//              xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
            end;
          finally
            FreeMem(xJob);
          end; //with
        end;

//      jtStartGrpEv:begin
//      // Job initialized by an application
//  //Test Nue
//            WriteLog(etInformation, Format('StartGrpEv arrived (Jobtyp=%s) Mach:%d Spd:%d-%d MMUser:%s YMSetName:%s',[GetJobName(mJob^.JobTyp),
//               mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SpindleFirst, mJob^.StartGrpEv.SpindleLast, mJob^.StartGrpEv.MMUserName, string(mJob^.StartGrpEv.YMSetName)]));
//  //End Test Nue
//        xJob := mJob;
//  {Delete
//  //Neu Start Nue 26.06.01
//        if NOT IsDataCollectionOn(mJob^.StartGrpEv.MachineID) then begin
//          xJob.JobTyp := jtSetSettings;
//          with xJob.SetSettings do begin
//            MachineID := mJob^.StartGrpEv.MachineID;
//            SpindleFirst := mJob^.StartGrpEv.SpindleFirst;
//            SpindleLast := mJob^.StartGrpEv.SpindleLast;
//            MachineGrp := mJob^.StartGrpEv.MachineGrp;
//            Color := mJob^.StartGrpEv.Color;
//            ProdGrpInfo := mJob^.StartGrpEv.ProdGrpParam.ProdGrpInfo;
//            xBaseJob := TSetSettingsJob.Create(mJobList, xJob, cNrOfTrials, cAssignGroup,
//                                      smMM, psOrdinaryAquis,  mJob^.StartGrpEv.ProdGrpParam.ProdGrpInfo.c_YM_set_id,
//                                      mDB.Query[cSecondaryQuery],mDB.Database, Params,
//                                      1(*Dummy*), 1(*Dummy*), ttMsgController);
//          end;//With
//          xJob := mJob;
//          xJob.JobTyp := jtSendMsgToAppl;
//          with xJob.SendMsgToAppl do begin
//            ResponseMsg.MsgTyp:=rmProdGrpStartedOk;
//            ComputerName := mJob^.StartGrpEv.ComputerName;
//            Port := mJob^.StartGrpEv.Port;
//            ResponseMsg.MachineID := mJob^.StartGrpEv.MachineID;
//            ResponseMsg.SpindleFirst := mJob^.StartGrpEv.SpindleFirst;
//            ResponseMsg.SpindleLast := mJob^.StartGrpEv.SpindleLast;
//            ResponseMsg.ProdGrpID := mJob^.StartGrpEv.ProdGrpID;        //@@Nue 26.06.01
//            // add Job SendMsgToAppl to queue list.
//            xBaseJob2 := TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],mDB.Database,xBaseJob);
//          end; //with
//          // add queue SetSettings to job list.
//          xBaseJob.AddSelfToJobList();
//          if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
//          end;
//        end
//        else begin
//  //Neu End Nue 26.06.01
//  end Delete}
//
//  {Nue 21.06.01: Zweiter Versuch rausgenommen
//        if ProcStartGrpEv<>rmProdGrpStartedOk then begin
//          Sleep(5000);
//          //Second trial
//          xJob := mJob;
//          xJob.JobTyp := jtSendMsgToAppl;
//          with xJob.SendMsgToAppl do begin
//            ResponseMsg.MsgTyp := ProcStartGrpEv;
//            if ResponseMsg.MsgTyp<>rmProdGrpStartedOk then begin
//              WriteLog(etError, Format('Last trial for ProcStartGrpEv (Jobtyp=%s) failed: MachID:%d, SpindFirst:%d, SpindLast:%d',
//                                        [GetJobName(mJob^.JobTyp),mJob^.StartGrpEv.MachineID,mJob^.StartGrpEv.SpindleFirst,mJob^.StartGrpEv.SpindleLast]));
//              ComputerName := mJob^.StartGrpEv.ComputerName;
//              Port := mJob^.StartGrpEv.Port;
//              ResponseMsg.MachineID := mJob^.StartGrpEv.MachineID;
//              ResponseMsg.SpindleFirst := mJob^.StartGrpEv.SpindleFirst;
//              ResponseMsg.SpindleLast := mJob^.StartGrpEv.SpindleLast;
//              ResponseMsg.ProdGrpID := mJob^.StartGrpEv.ProdGrpID;
//              // add Job SendMsgToAppl to queue list.
//              xBaseJob := TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],mDB.Database);
//              // add queue SendMsgToAppl to job list.
//              xBaseJob.AddSelfToJobList();
//
//              if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
//              end;
//            end;
//          end; //with
//        end; //'Successful Start' message will be sent within the StartProdGrp-JobListQueue
//  {}
//        xJob.JobTyp := jtSendMsgToAppl;
//        with xJob.SendMsgToAppl do begin
//          ResponseMsg.MsgTyp := ProcStartGrpEv;
//          if ResponseMsg.MsgTyp<>rmProdGrpStartedOk then begin
//            WriteLog(etWarning, Format('ProcStartGrpEv (Jobtyp=%s) failed: MachID:%d, SpindFirst:%d, SpindLast:%d',
//                                      [GetJobName(mJob^.JobTyp),mJob^.StartGrpEv.MachineID,mJob^.StartGrpEv.SpindleFirst,mJob^.StartGrpEv.SpindleLast]));
//            ComputerName := mJob^.StartGrpEv.ComputerName;
//            Port := mJob^.StartGrpEv.Port;
//            ResponseMsg.MachineID := mJob^.StartGrpEv.MachineID;
//            ResponseMsg.SpindleFirst := mJob^.StartGrpEv.SpindleFirst;
//            ResponseMsg.SpindleLast := mJob^.StartGrpEv.SpindleLast;
//            ResponseMsg.ProdGrpID := mJob^.StartGrpEv.ProdGrpID;
//            // add Job SendMsgToAppl to queue list.
//            xBaseJob := TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],mDB.Database);
//            // add queue SendMsgToAppl to job list.
//            xBaseJob.AddSelfToJobList();
//
//            if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
//            end;
//          end;
//        end; //with
//      end;
      jtStartGrpEvFromMa: begin
  //Test Nue
          WriteLog(etInformation, Format('StartGrpEvFromMa arrived (Jobtyp=%s) Mach:%d Spd:%d-%d',
                                         [GetJobName(mJob^.JobTyp), mJob^.StartGrpEvFromMa.MachineID,
                                          mJob^.StartGrpEvFromMa.SettingsRec.SpindleFirst, mJob^.StartGrpEvFromMa.SettingsRec.SpindleLast]));
  //End Test Nue
          if not IsDataCollectionOn(mJob^.StartGrpEvFromMa.MachineID) then EXIT;

  //      mJob^.StartGrpEvFromMa.Color := cDefaultStyleIDColor; //brightyellow

          mJob^.StartGrpEvFromMa.SettingsRec.Port := '';
          mJob^.StartGrpEvFromMa.SettingsRec.ComputerName := '';
          mJob^.StartGrpEvFromMa.SettingsRec.MMUserName := '';

          if ProcStartGrpEv <> rmProdGrpStartedOk then begin
            Sleep(5000);
          //Second trial
//            CloneJob(mJob, xJob);
//            xJob.JobTyp := jtSendMsgToAppl;
//            with xJob.SendMsgToAppl do try
//            // in ProcStartGrpEv wird bereits umkopiert
//              ResponseMsg.MsgTyp := ProcStartGrpEv;
//              if xResponseMsg.MsgTyp <> rmProdGrpStartedOk then begin
//                WriteLog(etError, Format('Last trial for ProcStartGrpEv (Jobtyp=%s) failed: MachID:%d, SpindFirst:%d, SpindLast:%d. Impossible to start! Start ignored! (rmTyp=%d)',
//                  [GetJobName(mJob^.JobTyp), mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SettingsRec.SpindleFirst, mJob^.StartGrpEv.SettingsRec.SpindleLast, ORD(xResponseMsg.MsgTyp)]));
//              end;
//            finally
//              FreeMem(xJob);
//            end; //with

            // in ProcStartGrpEv wird bereits umkopiert
            xResponseMsg.MsgTyp := ProcStartGrpEv;
            if xResponseMsg.MsgTyp <> rmProdGrpStartedOk then begin
              WriteLog(etError, Format('Last trial for ProcStartGrpEv (Jobtyp=%s) failed: MachID:%d, SpindFirst:%d, SpindLast:%d. Impossible to start! Start ignored! (rmTyp=%d)',
                [GetJobName(mJob^.JobTyp), mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SettingsRec.SpindleFirst, mJob^.StartGrpEv.SettingsRec.SpindleLast, ORD(xResponseMsg.MsgTyp)]));
            end;

          end; //'Successful Start' message will be sent within the StartProdGrp-JobListQueue
        end;

      jtStopGrpEv: begin
      // Job initialized by an application
  //Test Nue
          WriteLog(etInformation, Format('%s arrived (ProdID:%d) Mach:%d Spd:%d-%d MMUser:%s',
            [GetJobName(mJob^.JobTyp), mJob^.StartGrpEv.SettingsRec.ProdGrpID,
            mJob^.StartGrpEv.MachineID, mJob^.StartGrpEv.SettingsRec.SpindleFirst, mJob^.StartGrpEv.SettingsRec.SpindleLast, mJob^.StartGrpEv.SettingsRec.MMUserName]));
  //End Test Nue
          if not IsDataCollectionOn(mJob^.StopGrpEv.MachineID) then EXIT;
          mJob^.StopGrpEv.SettingsRec.Color := cDefaultStyleIDColor; //white
  {Nue 21.06.01: Zweiter Versuch rausgenommen
        if ProcStopGrpEv<>rmProdGrpStoppedOk then begin
          Sleep(5000);
          //Second trial
          xJob := mJob;
          xJob.JobTyp := jtSendMsgToAppl;
          with xJob.SendMsgToAppl do begin
            ResponseMsg.MsgTyp := ProcStopGrpEv;
            if ResponseMsg.MsgTyp<>rmProdGrpStoppedOk then begin
              WriteLog(etError, Format('Last trial for ProcStopGrpEv failed: MachID:%d, SpindFirst:%d, SpindLast:%d',
                                        [mJob^.StopGrpEv.MachineID,mJob^.StopGrpEv.SpindleFirst,mJob^.StopGrpEv.SpindleLast]));
              ComputerName := mJob^.StopGrpEv.ComputerName;
              Port := mJob^.StopGrpEv.Port;
              ResponseMsg.MachineID := mJob^.StopGrpEv.MachineID;
              ResponseMsg.SpindleFirst := mJob^.StopGrpEv.SpindleFirst;
              ResponseMsg.SpindleLast := mJob^.StopGrpEv.SpindleLast;
              ResponseMsg.ProdGrpID := mJob^.StopGrpEv.ProdGrpID;
              // add Job SendMsgToAppl to queue list.
              xBaseJob := TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery],mDB.Database);
              // add queue SendMsgToAppl to job list.
              xBaseJob.AddSelfToJobList();

              if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
              end;
            end;
          end; //with
  {}
// DONE: XML: kontrollieren!
//wss        xJob := mJob;
          CloneJob(mJob, xJob);
          xJob.JobTyp := jtSendMsgToAppl;
          with xJob.SendMsgToAppl do
          try
          // in ProcStartGrpEv wird bereits umkopiert
            ResponseMsg.MsgTyp := ProcStopGrpEv;
            if ResponseMsg.MsgTyp <> rmProdGrpStoppedOk then begin
              WriteLog(etWarning, Format('ProcStopGrpEv failed: MachID:%d, SpindFirst:%d, SpindLast:%d',
                [mJob^.StopGrpEv.MachineID, mJob^.StopGrpEv.SettingsRec.SpindleFirst, mJob^.StopGrpEv.SettingsRec.SpindleLast]));
              ComputerName             := mJob^.StopGrpEv.SettingsRec.ComputerName;
              Port                     := mJob^.StopGrpEv.SettingsRec.Port;
              ResponseMsg.MachineID    := mJob^.StopGrpEv.MachineID;
              ResponseMsg.SpindleFirst := mJob^.StopGrpEv.SettingsRec.SpindleFirst;
              ResponseMsg.SpindleLast  := mJob^.StopGrpEv.SettingsRec.SpindleLast;
              ResponseMsg.ProdGrpID    := mJob^.StopGrpEv.SettingsRec.ProdGrpID;
            // add Job SendMsgToAppl to queue list.
              xBaseJob := TSendMsgToApplJob.Create(mJobList, xJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database);
            // add queue SendMsgToAppl to job list.
              xBaseJob.AddSelfToJobList;

              xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
            end;
          finally
            FreeMem(xJob);
          end; //with

        end;

      jtStopGrpEvFromMa: begin //ok 20.7.99
        // wenn f�r diese Maschine die Datensammlung deaktiviert ist, dann nichts machen.
          if not IsDataCollectionOn(mJob^.StopGrpEvFromMa.MachineID) then EXIT;
//Test Nue
          if mJob^.StopGrpEvFromMa.SettingsRec.SpindleFirst <> $FF then
          // Weil Kr beim assignen eines laufenden SpdBereich auf eine neue MaschGrp
          // den Spindelbereich der alten Gruppe bereits auf $FF gesetzt hat
            WriteLog(etInformation, Format('%s arrived (ProdID:%d) Mach:%d Spd:%d-%d',
              [GetJobName(mJob^.JobTyp), mJob^.StopGrpEvFromMa.SettingsRec.ProdGrpID,
              mJob^.StopGrpEvFromMa.MachineID, mJob^.StopGrpEvFromMa.SettingsRec.SpindleFirst, mJob^.StopGrpEvFromMa.SettingsRec.SpindleLast]))
          else
            WriteLog(etInformation, Format('%s arrived (ProdID:%d) Mach:%d',
              [GetJobName(mJob^.JobTyp), mJob^.StopGrpEvFromMa.SettingsRec.ProdGrpID, mJob^.StopGrpEvFromMa.MachineID]));
  //End Test Nue
          mJob^.StopGrpEvFromMa.SettingsRec.Color := cDefaultStyleIDColor; //white
          mJob^.StopGrpEvFromMa.SettingsRec.Port := '';
          mJob^.StopGrpEvFromMa.SettingsRec.ComputerName := '';
          mJob^.StopGrpEvFromMa.SettingsRec.MMUserName := '';

          if ProcStopGrpEv <> rmProdGrpStoppedOk then begin

            WriteLog(etWarning, Format('%s: Not possible to accept stop (ProdID:%d) Mach:%d Spd:%d-%d during aquisation. -> MaEqualize-Job added.',
              [GetJobName(mJob^.JobTyp), mJob^.StopGrpEvFromMa.SettingsRec.ProdGrpID,
              mJob^.StopGrpEvFromMa.MachineID, mJob^.StopGrpEvFromMa.SettingsRec.SpindleFirst, mJob^.StopGrpEvFromMa.SettingsRec.SpindleLast]));

          //That no StopJob will get lost anfd no further jobs during aquisation have to be queued, a MaEqualize Job for the
          // given machine will be added to joblist.
          // MaEqualize Job into JobList
// DONE: XML: kontrollieren!
            xInt                       := mJob^.StopGrpEvFromMa.MachineID;
            mJob^.JobTyp               := jtMaEqualize;
            mJob^.MaEqualize.MachineID := xInt;
            with TMaEqualizeJob.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery],
                                       mDB.Database, Params, mMaEqualizeJobCritSec, grZEStopDuringAquisation) do begin
              // Add to JobList
              AddSelfToJobList;

              mJob^.JobID  := JobID;
            end;
            mJob^.JobTyp := jtNewJobEv;
            WriteJobBufferTo(ttJobController);


//            xBaseJob := TMaEqualizeJob.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery],
//              mDB.Database, Params, mMaEqualizeJobCritSec, grZEStopDuringAquisation);
//
//          // Add to JobList
//            xBaseJob.AddSelfToJobList(jsValid);
//
//            mJob^.JobID := xBaseJob.JobID;
//            mJob^.JobTyp := jtNewJobEv;
//            if not WriteJobBufferTo(ttJobController) then begin
//            end;

          end; //'Successful Stop' message will be sent within the StopProdGrp-JobListQueue
        end;

      jtSynchronize: begin //ok 3.8.99
          if not IsDataCollectionOn(mJob^.Synchronize.MachineID) then EXIT;
          if not ProcAnalyseAndFinish('jtSynchronize') then
          //Second trial
            if not ProcAnalyseAndFinish('jtSynchronize') then begin
              WriteLog(etError, Format('Last trial for ProcAnalyseAndFinish (jtSynchronize) failed: MachineID:%d',
                [mJob^.Synchronize.MachineID]));
            end;
        end;

      jtUplAutoMaData: begin
        end;

      jtSaveMaConfigToDB: begin
          //XMLData NICHT nullen, da dieses Feld nicht an JobManager �bergeben wird. An dieser Position steht der ComputerName!
          with TSaveMaConfigToDB.Create(mJobList, mJob, cNrOfTrials, mDB.Query[cSecondaryQuery], mDB.Database) do begin
            AddSelfToJobList;
            WriteBufferTo(ttJobController, jtNewJobEv);
          end;
        end;
    else
      // call inherited ProcessJob for unknown or common JobTyp
      inherited ProcessJob;
    end;
  except
    WriteLog(etError, Format('Crash in TJobManager.ProcessJob: JobTyp: %s', [GetJobName(mJob^.JobTyp)]));
  end;

end;
//-----------------------------------------------------------------------------
{ TJobManagerTimeoutTicker }

procedure TJobManagerTimeoutTicker.ProcessInitJob;
begin
 // DO NOT CALL INHERITED PROCESSJOB. IT HAS NO JOB TO BE HANDLED
//  Sleep(SleepTime);
  Sleep(2000);

  mJob^.JobTyp := jtTimerEvent;
  mJob^.NetTyp := ntNone;

  // send TimerEvent always to the second IPCClient definition
  // --> the first IPCClient connects to main program
  if not WriteJobBufferTo(ConnectToIndex[2]) then begin
  end;
//?? For tests to 10Min instead of 1Min
// TODO wss: f�r das ist dieses hohe Timeout?
  SleepTime := 600000; // Set Timouttime to 10Min
end;
//-----------------------------------------------------------------------------

end.

