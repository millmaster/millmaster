(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobControllerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 26.10.1998  1.00  Wss | Ableitung auf Baseklasse umgestellt
| 09.09.2003  1.01  Nue | jtMsgNotComplete: bei ProcessInitJob eingebaut. (Wegen Problem mit GetNodeList)
|=========================================================================================*)
unit JobControllerClass;

interface

uses
  Windows, mmEventLog, MMBaseClasses, BaseGlobal, SysUtils, JobListClasses;

type
  TJobController = class(TBaseJobController)
  protected
    procedure ProcessInitJob; override;
    procedure ProcessJob; override;
  public
    constructor Create(aThreadDef: TThreadDef; aJobList: TJobList); override;
    function Init: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//-----------------------------------------------------------------------------
constructor TJobController.Create(aThreadDef: TThreadDef; aJobList: TJobList);
begin
  inherited Create(aThreadDef, aJobList);
end;
//-----------------------------------------------------------------------------
function TJobController.Init : Boolean;
begin
//Nue:14.11.01 Wird aufgrund von Registry-Eintrag gesetzt oder nicht (Kann mit DebugConfig-Tool manipuliert werden.  DebugMode := True;
  Result := inherited Init;
  if Result then begin
  end;
end;
//-----------------------------------------------------------------------------
procedure TJobController.ProcessInitJob;
begin
  case mJob.JobTyp of
    jtInitialized: begin
      StartNext(False);
    end;
    jtMsgNotComplete: begin
      if (mJob.MsgNotComplete.Error.Error = ERROR_PATH_NOT_FOUND) and
         (mJob.MsgNotComplete.Error.ErrorTyp = etMMError) then begin
        //Hiermit wird dieser Job gel�scht und der n�chste geholt.
        //Passiert z.B. bei GetNodeList, wenn der gew�nschte Netzhandler nicht vorhanden ist.(Nue:9.9.03)
        try
          mJobList.DelJobID(mJob.JobID);
          mJob.JobTyp := jtDelJobID;
          WriteJobBufferTo(ttMsgController);
          StartNext(True);
        except
          on e:Exception do begin
            fError := SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TJobController.ProcessInitJob ' + e.message );
            raise EMMException.Create ( 'TJobController.ProcessInitJob ' + e.message);
          end;
        end;
      end
      else
       // call inherited ProcessJob for unknown or common JobTyp
       inherited ProcessInitJob;
    end;   
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessInitJob;
  end;
end;
//-----------------------------------------------------------------------------
procedure TJobController.ProcessJob;
begin
//  OutputDebugString(PChar(cEventLogDefaultText[mThreadDef.ThreadTyp]));
  case mJob.JobTyp of
    jtTestConfirm: begin
//Start Test Nue
//          mTestStr := Format('TesConfirmJC: %d JobTyp: %d',
//          [mJob.JobID,ORD(mJob.JobTyp)]);
//          OutputDebugString(PChar(mTestStr));
        if not WriteJobBufferTo(ttQueueManager) then begin
        end;
//End Test Nue
//        if not WriteJobBufferTo(ttJobManager) then begin
//        end;
      end;
    jtTestStore: begin
        if not WriteJobBufferTo(ttQueueManager) then begin
        end;
      end;
  else
    // call inherited ProcessJob for unknown or common JobTyp
    inherited ProcessJob;
  end;



end;
//-----------------------------------------------------------------------------
end.
