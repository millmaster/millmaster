(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobHandler.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Projektdatei des JobHandlers
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 10.10.2002        LOK | Umbau ADO
|=========================================================================================*)
program JobHandler;
 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  BaseMain,
  BaseGlobal,
  LoepfeGlobal,
  ActiveX,
  SysUtils,
  Windows,
  JobHandlerClass in 'JobHandlerClass.pas',
  JobControllerClass in 'JobControllerClass.pas',
  JobListClasses in 'JobListClasses.pas',
  JobHandlerUtil in 'JobHandlerUtil.pas',
  JobManagerClass in 'JobManagerClass.pas',
  MMBaseClasses in '..\..\Common\MMBaseClasses.pas';

var
  xJobHandler: TJobHandler;
  xResult: HResult;

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('JobHandler');
{$ENDIF}
  xResult := CoInitialize(nil);
  try
    // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
    // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
    if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
      raise Exception.Create('CoInitialize failed');
    xJobHandler := TJobHandler.Create(ssJobHandler);
    if xJobHandler.Initialize then
      xJobHandler.Run;
    xJobHandler.Free;
  finally
    CoUninitialize;
  end;
end.
