(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobHandlerClass.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Siemens Nixdorf Scenic 5T PCI, Pentium 133, Windows NT 4.0
| Target.system.: Windows 95/NT
| Compiler/Tools: Delphi 3.02
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 13.07.1998  0.00  Wss | Datei erstellt
| 15.11.1999  1.00  Nue | getParamHandle eingebaut.
| 13.10.2002        LOK | Umbau ADO
| 04.11.2002        NUE | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 11.11.2002  1.01  Nue | Test t_prodgroup_state replaced with v_prodgroup_state
| 16.06.2004  1.01  Wss | mJobList nach Destroy freigeben hinzugef�gt
| 09.09.2005        Lok | Fileversion bneim Programmstart ins Eventlog
|=========================================================================================*)
unit JobHandlerClass;

interface

uses
  Classes, mmThread, LoepfeGlobal, Windows, SettingsReader,
   MMBaseClasses, BaseGlobal, BaseMain, JobManagerClass, JobControllerClass, ActiveX;

type

  TJobHandler = class(TBaseMain)
  private
  protected
    mJobList: TJobList;
    function CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean; override;
    function getParamHandle:TMMSettingsReader; override;
  public
    constructor Create(aSubSystem: TSubSystemTyp); override;
    destructor Destroy; override;
    function Initialize: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmEventLog;

//-----------------------------------------------------------------------------
function TJobHandler.getParamHandle:TMMSettingsReader;
begin
  Result := TMMSettingsReader.Instance;
end;

//-----------------------------------------------------------------------------
constructor TJobHandler.Create(aSubSystem: TSubSystemTyp);
begin
  inherited Create(aSubSystem);
  mJobList := TJoblist.Create;
end;
//-----------------------------------------------------------------------------
destructor TJobHandler.Destroy;
begin
  inherited Destroy;
  //wss: erst nach Destroy die Liste freigeben, da zuerst die Threads gestoppt werden m�ssen.
  // so sollte es keine Probleme mit A/V geben
  mJobList.Free;
end;
//-----------------------------------------------------------------------------
function TJobHandler.CreateThread(var aThread: TmmThread; aThreadTyp: TThreadTyp): Boolean;
begin
  Result := True;
  case aThreadTyp of
    ttJobManager: begin
        aThread := TJobManager.Create(mSubSystemDef.Threads[1], mJobList);
      end;
    ttJobController: begin
        aThread := TJobController.Create(mSubSystemDef.Threads[2], mJobList);
      end;
    ttJobTimer: begin
        aThread := TJobManagerTimeoutTicker.Create(mSubSystemDef.Threads[3]);
      end;
  else
    aThread := Nil;
    Result := False;
  end;
end;
//-----------------------------------------------------------------------------
function TJobHandler.Initialize: Boolean;
var
  xFileVersion: string;
begin
  Result := inherited Initialize;
  if Result then
    Result := mJobList.Init;
  if Result then begin
    xFileVersion := 'unknown';
    try
      xFileVersion := GetFileVersion(ParamStr(0));
    except
      // Exceptions unterdr�cken, da die Funktion unwichtig f�r die Funktion ist
    end;
    WriteLog(etInformation, 'JobHandler started with fileversion ' + xFileVersion);
  end;
end;
//-----------------------------------------------------------------------------
end.

