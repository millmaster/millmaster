(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: JobList.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Compaq Deskpro Pentium 200Pro, Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.11.1998  0.00  Nue | Initial release, start implementation
| 14.01.1999  0.01  Nue | New functions SetDependence,SetJobItem,DelAllDependentJobs
|                       | StartJobAgain,SetJobGroupValid.
|                       | New fields in TJobListItem: Trial, JobGroup
| 03.02.1999  0.10  Nue | Reconstruction to class organisation (List and queues)
| 23.03.1999  0.11  Nue | Base classes moved to MMBaseClasses
| 13.01.2000  1.00  Nue | fDatabase in TBaseJob inserted (and dependent coding in all jobs)
| 06.06.2000  1.01  Nue | GetMaAssign renamed to MaEqualize. Old GetMaAssign only for getting Ma assigns used.
| 27.06.2000  1.02  Nue | ProdGrpInfo.c_order_position_id := 0; and ProdGrpInfo.c_order_id := 0; filled in
|                       | in TSetSettings.Create
| 23.11.2000  1.03  Nue | Anpassungen an neuen Typ TAWEMachType (amt338)
| 08.03.2001  1.04  Nue | Modification because of coldstart ZE in TGetMaAssignJob.AnalyseJobData.
| 05.04.2001  1.05  Nue | TDBDumpJob.DispatchContinuousJob and TGenQOfflimitJob.DispatchContinuousJob added.
| 28.05.2001  1.06  Nue | System.Move in TGetNodeListJob.AnalyseJobData replaced.
| 11.06.2001  1.07  Nue | Modifications because of DataCollection-Handling.
| 29.10.2001  1.08  Nue | codesite.SendFmtMsg in TMaEqualizeJob.EmergencyJM.
| 07.11.2001  1.09  Nue | NewJob TGetZEReadyJob added.
| 12.11.2001  1.10  Nue | For Inside machines: Take over ProdGrps from MM only if spindle ranges on MM correspond with those on ZE.
| 06.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 04.11.2002  1.11  Nue | Check field Event (deInitialReset) in TMaEqualizeJob.AnalyseJobData
|                       | (adding because cold start problems with Murata Inside)
|                       | (GetZEReadyJob not further used!)
| 11.11.2002  1.12  Nue | Test t_prodgroup_state replaced with v_prodgroup_state
| 18.11.2002  1.13  Nue | Changes because of changing c_machine_state from t_prodgroup_state to t_machine_state.
| 19.11.2002  1.14  Nue | No Prodgroups for AC338 machines on MM after Coldstart.
| 21.11.2002  1.15  Nue | Modifications in TMaEqualizeJob.AnalyseJobData (again SpindleFist<>}FF checking ??????).
| 11.12.2002        Nue | Made query 'cQrySelSpindle' in TSetSettingsJob.AnalyseJobData ADO-conform.
| 21.06.2005  1.20  Nue | Verschiedene Anpassungen f�r Version V5.01. /MaOffline Handling
| 15.08.2005  1.21  Nue | Erweiterungen wegen implementierung LX-Handler.
| 25.11.2005        Lok | Hints und Warnings entfernt
| 28.09.2006  1.22  Nue | cMaxGroupMemoryLimit und cLXSpdMemoryLimit behandelt.
|=========================================================================================*)
unit JobListClasses;
{$MINENUMSIZE 2}


interface

uses
  Classes, Windows, SysUtils, LoepfeGlobal, JobHandlerUtil,
  IPCClass, BaseGlobal,MMUGlobal, MMBaseClasses, mmEventLog,
  ADODBAccess,
  YMParaDef, YMParaUtils,
  SettingsReader, syncobjs;

// Description:
//   The Methode DispatchContinuousJob will be used for further proceeding the job,
//     if it returns from MsgController (with MsgComplete) or from QueueManager (with JobSuccessful)
//   The Methode AnalyseJobData will be used for further proceeding the job,
//     if it returns from MsgDispatcher with data to JobManager. Usually, jobs containing AnalyseJob
//     uses the the DispatchContinuousJob methode from TBaseJob

type
//  TJobQueueType = set (jtDataAquEv, jtStartGrpEv, jtStopGrpEv, jtStartGrpEvFromMa,
//                   jtStopGrpEvFromMa, jtSendMsgToAppl) of TJobTyp;


  TGetZEReadyJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

  TGetZESpdDataJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TGetExpZESpdDataJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TGetDataStopZESpdJob = class(TBaseJob)
    private
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TGetExpDataStopZESpdJob = class(TBaseJob)
    private
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TGetSettingsJob = class(TBaseJob)
  // This Job is at the moment only used during startup of the system in a special case
  //  within MaEqualize.AnalyseJob
    private
      fNewProdGrpOnDB : Boolean;
//wss      fSettings : array of byte;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil; aNewProdGrpOnDB: Boolean=False); virtual;
      function AnalyseJobData: Boolean; override;

    published
    end;

  TGetSettingsAllGroupsJob = class(TBaseJob)
    private
      fOrgJobRec: TGetSettingsAllGroups;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aOrgJobRec: TGetSettingsAllGroups; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TClearZEGrpDataJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

  TClearZESpdDataJob = class(TClearZEGrpDataJob);  // Only true if TClearZESpdData = TClearZEGrpData

  TDBDumpJob = class(TBaseJob)
    public
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    end;

  TDelZESpdDataJob = class(TBaseJob)
    private
      fNewDummyProdID: Longint;
      fProdID: Longint;
      fZERange: Boolean;
      fParams : TMMSettingsReader;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
                    aParams : TMMSettingsReader; aQueueType: TJobTyp; aProdID: Longint; aNewDummyProdID: Longint; aParentJob: TBaseJob=Nil;
                    aComputerName: TString30=''; aPort : TString30=''; aZERange: Boolean=False); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    published
    end;


  TDelIntJob = class(TBaseJob);

  TDelShiftByTimeJob = class(TBaseJob);

  TGenShiftDataJob = class(TBaseJob)
    private
      fNewDummyProdID: Longint;
      fParams : TMMSettingsReader;
      function Finish(aJobError: TJobTyp; aEmergency: Boolean): Boolean;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp; aNewDummyProdID: Longint; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    published
    end;

  TGenExpShiftDataJob = class(TBaseJob)
    private
      fNewDummyProdID: Longint;
      fParams : TMMSettingsReader;
      function Finish(aJobError: TJobTyp; aEmergency: Boolean): Boolean;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp; aNewDummyProdID: Longint; aParentJob: TBaseJob=Nil;
                         aComputerName: TString30=''; aPort : TString30=''); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    published
    end;

  TGenQOfflimitJob = class(TBaseJob)
    public
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    end;

  TGenSpdOfflimitsJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
    published
    end;

  TGenDWDataJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;   //??Nue: Methode only for tests 26.4.99
    published
    end;

  TGenExpDWDataJob = class(TGenDWDataJob);   // Only true if TGenExpDWDataJob = TGenDWDataJob

  TGetMaAssignJob = class(TBaseJob)
    private
      fOrgJobRec: TGetSettingsAllGroups;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aOrgJobRec: TGetSettingsAllGroups; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function AnalyseJobData: Boolean; override;
      published
    end;

  TMaEqualizeJob = class(TBaseJob)
    private
      fCritSec: TCriticalSection;
      fCallReason: TMaEqualizeReason;
      fParams : TMMSettingsReader;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aCritSec: TCriticalSection; aCallReason: TMaEqualizeReason;
                         aFirstReceiver: TThreadTyp=ttMsgController; aParentJob: TBaseJob=Nil); virtual;
      function AnalyseJobData: Boolean; override;
      function AddSelfToJobList(aJobState: TJobState=jsValid; aJobNext: DWord=0):
          DWord; override;
      function AddSelfToJobList1(aJobState: TJobState=jsValid; aJobNext: DWord=0;
          aJobGroupID: DWord=0): DWord; reintroduce; virtual;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TAssignComplete = class(TMaEqualizeJob); //Same as MaEqualize but asynchronous forced by ZE

  TGetNodeListJob = class(TBaseJob)
    private
      fNetTyp: TNetTyp;
      fMaEqualizeCritSec: TCriticalSection;
      fParams : TMMSettingsReader;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
                         aParams : TMMSettingsReader; aCritSec: TCriticalSection; aNetTyp: TNetTyp; aParentJob: TBaseJob=Nil); virtual;
      property NetTyp: TNetTyp read fNetTyp;
      function AnalyseJobData: Boolean; override;
    published
    end;

  TInitializedJob = class(TBaseJob)         // To signalize that a handler is initialized
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function AnalyseJobData: Boolean; override;
    published
    end;

  TProdGrpUpdateJob = class(TBaseJob);
  { TODO 1 -onue -cWSC : 
Klasse implementieren fuer die Abhandlung der einzelnen, mitgegebenen Felder.
Speziell fuer WSC. }
  TSendEventToClientsJob = class(TBaseJob);

  TSendMsgToApplJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

  TSetNodeListJob = class(TBaseJob)
    private
      fNetTyp: TNetTyp;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aNetTyp: TNetTyp; aParentJob: TBaseJob=Nil); virtual;
      property NetTyp: TNetTyp read fNetTyp;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

  TSetSettingsJob = class(TBaseJob)
    private
      mAssignment: Byte; //cAssigGroup = Assign Settings and ProdID
                         //cAssigProdGrpIDOnly = Settings were loading to a inconsistent ProdGrp; Set state in
                         // v_prodgroup_state after successful confirm
      mColor: Integer;  // Color for prodgrp representation on floor
      mYMSetName: TText50;  //Nue:13.9.01
      mYMSetChanged: Boolean;  //Nue:02.10.01
      fStartMode: TProdGrpStartMode; //Contains caller from this job (smMM,smZE)
      mProdGrpState: TProdGrpState;
      fFirstReceiver: TThreadTyp; //If FirstReceiver is JobManager machine is offline. The values form this
                                  // (old) ProdGrp will be copied into the new Dummy (inconsistent)ProdGrp
      fOldIntID: Byte;            //IntID before the actual
      fOldFragID: Longint;        //FragID before the actual
      fProdIDOrgRange: Longint;
      fParams: TMMSettingsReader;
//wss wird nur im constructor verwendet      mYMParaDBAccess: TSettingsDBAssistant;
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte;
          aAssignment: Byte; aStartMode: TProdGrpStartMode; aProdGrpState:
          TProdGrpState; aYMSetID: Integer; aQuery: TNativeAdoQuery; aDatabase:
          TAdoDBAccessDB; aParams : TMMSettingsReader; aOldIntID: Byte; aOldFragID:
          Longint; aFirstReceiver: TThreadTyp=ttMsgController; aParentJob:
          TBaseJob=Nil;  aComputerName: TString30=''; aPort : TString30='';
          aProdIDOrgRange:Longint=0); virtual;
//      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
      function AnalyseJobData: Boolean; override;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    published
    end;

  TStartGrpEvJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

  TSynchronizeJob = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function AnalyseJobData: Boolean; override;
    published
    end;

  TUnDoGetZESpdDataJob = class(TBaseJob)
    private
      fNewDummyProdID: Longint;
      fParams : TMMSettingsReader;
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                          aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp;
                          aNewDummyProdID: Longint;
                          aComputerName: TString30=''; aPort : TString30=''); virtual;
      function EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean; override;
    end;

  TSaveMaConfigToDB = class(TBaseJob)
    private
    protected
    public
      constructor Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                         aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob=Nil); virtual;
      function DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState; override;
    published
    end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  TypInfo,
  mmCS, XMLSettingsAccess, XMLDef, XMLGlobal, XMLSettingsModel, XMLSettingsTools;  // Unit fuer CodeSite

//-----------------------------------------------------------------------------
           
{ TGetZEReadyJob }

constructor TGetZEReadyJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                      aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtGetZEReady, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, aJobRec^.GetZEReady.MachineID);
end;

//-----------------------------------------------------------------------------

function TGetZEReadyJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  // Only jtMsgComplete possible
  inherited DispatchContinuousJob(aJobTyp);
  Result := dcFalse;
end;
//-----------------------------------------------------------------------------

{ TGetZESpdDataJob }

constructor TGetZESpdDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                    aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob;
                                    aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtGetZESpdData, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.GetZESpdData.MachineID, False, aComputerName, aPort);
end;
//-----------------------------------------------------------------------------

function TGetZESpdDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  Result := dcFalse;
  case aJobTyp of
    jtMsgComplete: begin
//Start Test nue
      mTestStr := Format('ContJob: %d JobTyp: %s JobNext: %d',
      [Self.JobID,mClassname,Self.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
//      Self.TimeStamp := Now;    //Reset Timestamp for new Timeout
      Self.TimeStamp := GetTickCount;    //Reset Timestamp for new Timeout
      if WriteJobBufferTo(ttQueueManager) then begin
//Nue?? Wieder disablen. Nur fuer Tests
//WriteLog(etInformation, Format('JobToQueueManager: %d JobTyp: %d',
//[JobRec^.JobID,ORD(JobRec^.JobTyp)]),NIL,0);

        Result := inherited DispatchContinuousJob(aJobTyp);
      end;
    end;
    jtJobSuccessful: begin
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TGetZESpdDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: begin
      try
        ResetProdGrpState(JobRec^.GetZESpdData.ProdID);
        if fComputerName<>'' then begin
          PrepareSendMsgToAppl(rmProdGrpStartedNOk, JobRec^.GetZESpdData.MachineID, JobRec^.GetZESpdData.SpindleFirst,
                               JobRec^.GetZESpdData.SpindleLast, fComputerName, fPort, xJobRec);

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;
        inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  // Calls free from this Job
      except
        on e:Exception do begin
          SetError (ERROR_INVALID_FUNCTION, etDBError, 'TGetZESpdDataJob.EmergencyJM ' + e.message );
          raise EMMException.Create ( fError.Msg );
        end;
      end;
    end;
  end;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TGetExpZESpdDataJob }

constructor TGetExpZESpdDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                       aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob;
                                       aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtGetExpZESpdData, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.GetExpZESpdData.MachineID, False, aComputerName, aPort);
end;

//-----------------------------------------------------------------------------

function TGetExpZESpdDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  Result := dcFalse;
  case aJobTyp of
    jtMsgComplete: begin
//Start Test nue
      mTestStr := Format('ContJob: %d JobTyp: %s JobNext: %d',
      [Self.JobID,mClassname,Self.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
//      Self.TimeStamp := Now;    //Reset Timestamp for new Timeout
      Self.TimeStamp := GetTickCount;    //Reset Timestamp for new Timeout
      if WriteJobBufferTo(ttQueueManager) then begin
//Nue?? Wieder disablen. Nur fuer Tests
//WriteLog(etInformation, Format('JobToQueueManager: %d JobTyp: %d',
//[JobRec^.JobID,ORD(JobRec^.JobTyp)]),NIL,0);
        Result := inherited DispatchContinuousJob(aJobTyp);
      end;
    end;
    jtJobSuccessful: begin
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TGetExpZESpdDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: try
        ResetProdGrpState(JobRec^.GetExpZESpdData.ProdID);
        if fComputerName <> '' then begin
          PrepareSendMsgToAppl(rmProdGrpStartedNOk, JobRec^.GetExpZESpdData.MachineID, JobRec^.GetExpZESpdData.SpindleFirst,
                               JobRec^.GetExpZESpdData.SpindleLast, fComputerName, fPort, xJobRec);

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;
        inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  // Calls free from this Job

      except
        on e:Exception do begin
          SetError (ERROR_INVALID_FUNCTION, etDBError, 'TGetExpZESpdDataJob.EmergencyJM ' + e.message );
          raise EMMException.Create ( fError.Msg );
        end;
      end; // try
  end; // case
  if aLogEnabled then
    WriteLog(etWarning, Format('EmergencyJM.GetExpZESpdData: MachID:%d SpindFirst:%d SpindLast:%d',
                               [JobRec^.GetExpZESpdData.MachineID,JobRec^.GetExpZESpdData.SpindleFirst,JobRec^.GetExpZESpdData.SpindleLast]));
end;
//-----------------------------------------------------------------------------
{ TGetDataStopZESpdJob }

constructor TGetDataStopZESpdJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                        aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob;
                                        aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtGetDataStopZESpd, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.GetDataStopZESpd.MachineID, False, aComputerName, aPort);
end;
//-----------------------------------------------------------------------------

function TGetDataStopZESpdJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
//Code von TGetDataStopZESpdJob.DispatchContinuousJob ist gleich wie TGetExpDataStopZESpdJob.DispatchContinuousJob
begin
  Result := dcFalse;
  case aJobTyp of
    jtDelJobID: begin // TGetDataJobZESpdJob.Dispatch.....  case
    //Initiated by the EmergencyJM-Methode of Self, to delete the Job
      Result := dcFalse;
    end;
    jtMsgComplete: begin
//Start Test nue
      mTestStr := Format('ContJob: %d JobTyp: %s JobNext: %d',
      [Self.JobID,mClassname,Self.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
//      Self.TimeStamp := Now;    //Reset Timestamp for new Timeout
      Self.TimeStamp := GetTickCount;    //Reset Timestamp for new Timeout
      if WriteJobBufferTo(ttQueueManager) then begin
//        WriteLog(etInformation, Format('JobToQueueManager: %d JobTyp: %d',
//        [JobRec^.JobID,ORD(JobRec^.JobTyp)]),NIL,0);
        Result := inherited DispatchContinuousJob(aJobTyp);
      end;
    end;
    jtJobSuccessful: begin
    //Stop ProdGrp on DB
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TGetDataStopZESpdJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
//Code von TGetDataStopZESpdJob.EmergencyJM ist gleich wie TGetExpDataStopZESpdJob.EmergencyJM
var
  xIsMaOffline: Boolean;
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: begin
      try
        xIsMaOffline := IsMaOffline(Query, JobRec^.GetDataStopZESpd.MachineID, fError);

        StopOldProdGrp(Query, Database, JobRec^.GetDataStopZESpd.ProdID, fError);
        if fComputerName <> '' then begin
          if xIsMaOffline then
            PrepareSendMsgToAppl(rmProdGrpStoppedNOk, JobRec^.GetDataStopZESpd.MachineID, JobRec^.GetDataStopZESpd.SpindleFirst,
                                 JobRec^.GetDataStopZESpd.SpindleLast, fComputerName, fPort, xJobRec)
          else
            PrepareSendMsgToAppl(rmProdGrpStoppedOk, JobRec^.GetDataStopZESpd.MachineID, JobRec^.GetDataStopZESpd.SpindleFirst,
                                 JobRec^.GetDataStopZESpd.SpindleLast, fComputerName, fPort, xJobRec);

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;
      except
        on e:Exception do begin
           fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
           raise EMMException.Create ( fError.Msg );
        end;
      end;
      if aLogEnabled then
        WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the TGetZESpdDataJob job for ProdID %d correctly. Stopped it anyway.',
                                   [mClassname,JobRec^.GetDataStopZESpd.ProdID]));
//      Result := inherited EmergencyJM(aJobError, False, aLogEnabled);
      Result := inherited EmergencyJM(aJobError, xIsMaOffline, aLogEnabled);
    end;
  end;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
{ TGetExpDataStopZESpdJob }

constructor TGetExpDataStopZESpdJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                        aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob;
                                        aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtGetExpDataStopZESpd, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.GetExpDataStopZESpd.MachineID, False, aComputerName, aPort);
end;
//-----------------------------------------------------------------------------

function TGetExpDataStopZESpdJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
//Code von TGetDataStopZESpdJob.DispatchContinuousJob ist gleich wie TGetExpDataStopZESpdJob.DispatchContinuousJob
begin
  Result := dcFalse;
  case aJobTyp of
    jtDelJobID: begin  // TGetExpDataStopZESpdJob.Dispatch.... case
    //Initiated by the EmergencyJM-Methode of Self, to delete the Job
      Result := dcFalse;
    end;
    jtMsgComplete: begin
//Start Test nue
      mTestStr := Format('ContJob: %d JobTyp: %s JobNext: %d',
      [Self.JobID,mClassname,Self.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
//      Self.TimeStamp := Now;    //Reset Timestamp for new Timeout
      Self.TimeStamp := GetTickCount;    //Reset Timestamp for new Timeout
      if WriteJobBufferTo(ttQueueManager) then begin
//        WriteLog(etInformation, Format('JobToQueueManager: %d JobTyp: %d',
//        [JobRec^.JobID,ORD(JobRec^.JobTyp)]),NIL,0);
        Result := inherited DispatchContinuousJob(aJobTyp);
      end;
    end;
    jtJobSuccessful: begin
    //Stop ProdGrp on DB
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TGetExpDataStopZESpdJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
//Code von TGetDataStopZESpdJob.EmergencyJM ist gleich wie TGetExpDataStopZESpdJob.EmergencyJM
var
  xIsMaOffline: Boolean;
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: begin
      try
        xIsMaOffline := IsMaOffline(Query, JobRec^.GetExpDataStopZESpd.MachineID, fError);

        StopOldProdGrp(Query, Database, JobRec^.GetExpDataStopZESpd.ProdID, fError);
        if fComputerName <> '' then begin
          if xIsMaOffline then
            PrepareSendMsgToAppl(rmProdGrpStoppedNOk, JobRec^.GetExpDataStopZESpd.MachineID, JobRec^.GetExpDataStopZESpd.SpindleFirst,
                                 JobRec^.GetExpDataStopZESpd.SpindleLast, fComputerName, fPort, xJobRec)
          else
            PrepareSendMsgToAppl(rmProdGrpStoppedOk, JobRec^.GetExpDataStopZESpd.MachineID, JobRec^.GetExpDataStopZESpd.SpindleFirst,
                                 JobRec^.GetExpDataStopZESpd.SpindleLast, fComputerName, fPort, xJobRec);

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;
      except
        on e:Exception do begin
           fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
           raise EMMException.Create ( fError.Msg );
        end;
      end;
      if aLogEnabled then
        WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the TGetExpZESpdDataJob job for ProdID %d correctly. Stopped it anyway.',
                                   [mClassname,JobRec^.GetDataStopZESpd.ProdID]));
//      Result := inherited EmergencyJM(aJobError, False, aLogEnabled);
      Result := inherited EmergencyJM(aJobError, xIsMaOffline, aLogEnabled);
    end;
  end;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TGetNodeListJob }

constructor TGetNodeListJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
                                   aParams : TMMSettingsReader; aCritSec: TCriticalSection; aNetTyp: TNetTyp; aParentJob: TBaseJob);
begin
  fMaEqualizeCritSec := aCritSec;
  fParams := aParams;
  inherited create(aJobList, jtSetNodeList, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, cNoMachineID);
  fNetTyp := aNetTyp;
end;
//-----------------------------------------------------------------------------

function TGetNodeListJob.AnalyseJobData: Boolean;
var
  xMaEqualizeJob, xInitializedJob : TBaseJob;
//  xSaveMaConfigJob: TBaseJob;
  xJob : TJobRec;
  xCount : DWord;
  xInitializedJobID : DWord;
  xMachID : Integer;
  xMachState : TTxnNodeState;
  xMin, xMax : Integer;

begin
  Result := False;
  WriteLogDebug('Entry:  TGetNodeListJob.AnalyseJobData');
  //Jobs in Jobliste einfuegen
  xCount := sizeof(xJob);
  if JobRec^.JobLen < xCount then
    xCount := JobRec^.JobLen;
  System.Move(JobRec^, xJob, xCount);

  xJob.JobTyp      := jtInitialized;
  xInitializedJob   := TInitializedJob.Create(mJobList, @xJob, cNrOfTrials, Query, Database);
  xInitializedJobID := xInitializedJob.AddSelfToJobList(jsInvalid);  //On jsValid at the end of this methode

  case fNetTyp of
    ntTXN : begin
        xMin := cMinTxnNodeID;
        xMax := cMaxTxnNodeID;
      end;
    ntWSC: begin
        xMin := cFirstMachPos;
        xMax := cMaxWscMachine;
      end;
    ntLX: begin   //Nue
        xMin := cFirstMachPos;
        xMax := cMaxLXMachine;
      end;
  else // ntCI
    Exit; // wss
  end;

  for xCount:=xMin to xMax do begin
    case fNetTyp of
      ntTXN: begin
          xMachID    := JobRec^.GetNodeList.NodeItems.TxnNetNodeList[xCount].MachID;
          xMachState := JobRec^.GetNodeList.NodeItems.TxnNetNodeList[xCount].MachState;
        end;
      ntWSC: begin
          xMachID    := JobRec^.GetNodeList.NodeItems.WscNetNodeList[xCount].MachID;
          xMachState := JobRec^.GetNodeList.NodeItems.WscNetNodeList[xCount].MachState;
        end;
      ntLX: begin //Nue
          xMachID    := JobRec^.GetNodeList.NodeItems.LXNetNodeList[xCount].MachID;
          xMachState := JobRec^.GetNodeList.NodeItems.LXNetNodeList[xCount].MachState;
        end;
    else // ntCI
      xMachID    := 0;
      xMachState := nsNotValid;
    end;

    if xMachID <> 0 then begin
      Query.Close;
      try
        //Status von Maschine und Node auf DB nachfuehren
//WriteLogDebug(Format('TGetNodeListJob.AnalyseJobData: Vor Database.StartTransaction: Ma: %d NetID:%d',[xMachID,ORD(fNetTyp)]));
        Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
        Query.SQL.Text := cQrySetMachAndNodeState;
        Query.Params.ParamByName('c_machine_id').AsInteger := xMachID;
        Query.Params.ParamByName('c_node_stat').AsInteger  := ORD(xMachState);
        if xMachState = nsOnline then
          Query.Params.ParamByName('c_machine_state').AsInteger := ORD(msInSynchronisation)
        else
          Query.Params.ParamByName('c_machine_state').AsInteger := ORD(msOffline);

        Query.ExecSQL;
        Database.Commit;
//WriteLogDebug(Format('TGetNodeListJob.AnalyseJobData: Nach Database.Commit: Ma: %d NetID:%d',[xMachID,ORD(fNetTyp)]));
      except
        on e:Exception do begin
          fError := SetError ( ERROR_INVALID_FUNCTION , etDBError, 'GetNodeListJob.AnalyseJob failed.' + e.message );
          Database.Rollback;
          raise EMMException.Create ( fError.Msg );
        end;
      end;

      // Jobs aller Online Maschinen mit MaEqualizeJobs in JobList aufsetzten
      // Erst der InitializedJob setzt die MaEqualizeJobs auf jsValid
      if xMachState = nsOnline then begin
        // Falls es kein Bauzustand f�r diese Maschine gibt, dass wird der Job MaEqualize als Folgejob in Queue eingef�gt
        xJob.JobTyp               := jtMaEqualize;
        xJob.MaEqualize.MachineID := xMachID;

        xMaEqualizeJob := TMaEqualizeJob.Create(mJobList, @xJob, cNrOfTrials, Query, Database, fParams, fMaEqualizeCritSec, grGetNodeList, ttMsgController{, xSaveMaConfigJob});
        xMaEqualizeJob.JobGroup := xInitializedJobID;
        TMaEqualizeJob(xMaEqualizeJob).AddSelfToJobList(jsInvalid, 0);
      end;
{M�rz 2005 wss: bis auf weiteres kein Automat f�r das Speichern des Bauzustandes
      if xMachState = nsOnline then begin
        Query.SQL.Text := cQryCheckMaGroupConfig;
        Query.Params.ParamByName('MachID').AsInteger := xMachID;
        try
          Query.Open;
          if not Query.FindFirst then begin
            CodeSite.SendFmtMsg('Create Job SaveMaConfigToDB for MachID=%d', [xMachID]);
            xJob.JobTyp := jtSaveMaConfigToDB;
            xJob.SaveMaConfig.MachineID    := xMachID;
            xJob.SaveMaConfig.UseXMLData   := False;
            xJob.SaveMaConfig.ComputerName := '';
            xJob.SaveMaConfig.Port         := '';
            xSaveMaConfigJob := TSaveMaConfigToDB.Create(mJobList, @xJob, cNrOfTrials, Query, Database);
            xSaveMaConfigJob.JobGroup := xInitializedJobID;
          end else begin
            CodeSite.SendFmtMsg('XML MaConfig found for MachID=%d, ID=%d ', [xMachID, Query.FieldByName('c_magroup_config_id').AsInteger]);
            xSaveMaConfigJob := Nil;
          end;
        finally
          Query.Close;
        end;
        // Falls es kein Bauzustand f�r diese Maschine gibt, dass wird der Job MaEqualize als Folgejob in Queue eingef�gt
        xJob.JobTyp               := jtMaEqualize;
        xJob.MaEqualize.MachineID := xMachID;

        xMaEqualizeJob := TMaEqualizeJob.Create(mJobList, @xJob, cNrOfTrials, Query, Database, fParams, fMaEqualizeCritSec, grGetNodeList, ttMsgController, xSaveMaConfigJob);
        xMaEqualizeJob.JobGroup := xInitializedJobID;

        if Assigned(xSaveMaConfigJob) then
          xSaveMaConfigJob.AddSelfToJobList(jsInvalid, 0)
        else
//            xMaEqualizeJob.AddSelfToJobList;
//            TMaEqualizeJob(xMaEqualizeJob).AddSelfToJobList(jsInvalid, 0);
          TMaEqualizeJob(xMaEqualizeJob).AddSelfToJobList(jsInvalid, 0, xInitializedJobID);
      end;
{}
    end
  end;

  //InitializedJob on jsValid
  mJobList.SetJobState(xInitializedJobID, jsValid);
  xInitializedJob.WriteBufferTo(ttJobController, jtNewJobEv);

  Result := True;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TGetSettingsJob }

constructor TGetSettingsJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob; aNewProdGrpOnDB: Boolean);
begin
  inherited create(aJobList, jtGetSettings, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, aJobRec^.GetSettings.MachineID);
  fNewProdGrpOnDB := aNewProdGrpOnDB;
end;
//-----------------------------------------------------------------------------

//Not used now
function TGetSettingsJob.AnalyseJobData(): Boolean;
begin
  Result := False;
  if fNewProdGrpOnDB then
  try
    Query.Close;
    Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
    // Die Funktion ist offenbar nicht mehr in Gebrauch (Query Parameter fehlen)
    Query.SQL.Text := cQryInsDummyProdGrp;
    Query.InsertSQL('t_prodgroup','c_prod_id', cMaxProdID, cMinProdID);
    Database.Commit;
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TGetSettingsJob.AnalyseJobData failed.' + e.message );
      Database.Rollback;
      raise EMMException.Create ( fError.Msg );
    end;
  end;
end;
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
{ TGetSettingsAllGroupsJob }

constructor TGetSettingsAllGroupsJob.Create(aJobList: TJobList; aJobRec: PJobRec; aOrgJobRec: TGetSettingsAllGroups; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                    aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtGetSettingsAllGroups, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.GetSettingsAllGroups.MachineID, False, aJobRec.GetSettingsAllGroups.ComputerName, aJobRec.GetSettingsAllGroups.Port);
  fOrgJobRec := aOrgJobRec;
end;
//-----------------------------------------------------------------------------

function TGetSettingsAllGroupsJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  Result := dcFalse;
  case aJobTyp of
    jtMsgComplete: begin
//Start Test nue
      mTestStr := Format('ContJob: %d JobTyp: %s JobNext: %d',
      [Self.JobID,mClassname,Self.JobNext]);
      OutputDebugString(PChar(mTestStr));
//End Test Nue
//      Self.TimeStamp := Now;    //Reset Timestamp for new Timeout
      Self.TimeStamp := GetTickCount;    //Reset Timestamp for new Timeout
      Self.JobRec^.GetSettingsAllGroups :=  fOrgJobRec;  //Umkopieren des OriginalJobs fuer Queue-Manager
      if WriteJobBufferTo(ttQueueManager) then begin
        Result := inherited DispatchContinuousJob(aJobTyp);
      end;
    end;
    jtJobSuccessful: begin
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;
//-----------------------------------------------------------------------------

function TGetSettingsAllGroupsJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: begin
      try
        if fComputerName<>'' then begin
//TODO wss: w�re hier die Funktion PrepareSendMsgToAppl() auch einsetzbar?
//          with JobRec^.GetSettingsAllGroups do
//            PrepareSendMsgToAppl(rmGetSettingsNotOk, MachineID, 0, 0, fComputerName, fPort, xJobRec);
          with xJobRec, SendMsgToAppl do begin
            JobTyp                := jtSendMsgToAppl;
            ComputerName          := fComputerName;
            Port                  := fPort;
            ResponseMsg.MsgTyp    := rmGetSettingsNotOk;
            ResponseMsg.MachineID := JobRec^.GetSettingsAllGroups.MachineID;
          end; // with

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end; // if fComputerName
        inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  // Calls free from this Job
      except
        on e:Exception do begin
          SetError (ERROR_INVALID_FUNCTION, etDBError, 'TGetSettingsAllGroupsJob.EmergencyJM ' + e.message );
          raise EMMException.Create ( fError.Msg );
        end;
      end;
    end;
  end;
end;
//-----------------------------------------------------------------------------

{ TClearZEGrpDataJob }

constructor TClearZEGrpDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                      aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtClearZEGrpData, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, aJobRec^.ClearZEGrpData.MachineID);
end;
//-----------------------------------------------------------------------------

function TClearZEGrpDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  // Only jtMsgComplete possible
  Result := dcFalse;
  inherited DispatchContinuousJob(aJobTyp);
end;
//-----------------------------------------------------------------------------
{ TDBDumpJob }

function TDBDumpJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;  //Nue: Methode new 4.4.01
begin
//@@Nue  WriteLogDebug(etInformation, Format('Start DBDumpJob (send to StorageHandler)!: %s ', [DateTimeToStr(Now)]), nil, 0, True);
  Result := inherited DispatchContinuousJob(aJobTyp);
end;

//-----------------------------------------------------------------------------
{ TDelZESpdDataJob }

constructor TDelZESpdDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB;
                    aParams : TMMSettingsReader; aQueueType: TJobTyp; aProdID: Longint; aNewDummyProdID: Longint; aParentJob: TBaseJob;
                    aComputerName: TString30; aPort : TString30; aZERange: Boolean);
begin
//DONE wss: �hm warum Members vor dem Konstrukt initialisieren?
  inherited Create(aJobList, jtClearZEGrpData, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.ClearZEGrpData.MachineID, False, aComputerName, aPort);
  QueueType      := aQueueType;
  fNewDummyProdID := aNewDummyProdID;
  fProdID         := aProdID;
  fZERange        := aZERange;
  fParams         := aParams;
end;
//-----------------------------------------------------------------------------

function TDelZESpdDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  // Only jtMsgComplete possible
  Result := dcFalse;
  inherited DispatchContinuousJob(aJobTyp);
  //Reset state of prodgroup on DB
  try
    Query.Close;
{Nue 25.06.01 Moved to TGenShiftDataJob and TGenExpShiftDataJob
    if fQueueType=jtDataAquEv then begin
      Query.SQL.Text := cQrySetGrpStateBackM;
      Query.Params.ParamByName('c_machine_id' ).AsInteger := JobRec^.DelZESpdData.MachineID;
      Query.Params.ParamByName('c_spindle_first' ).AsInteger := JobRec^.DelZESpdData.SpindleFirst;
      Query.Params.ParamByName('c_spindle_last' ).AsInteger := JobRec^.DelZESpdData.SpindleLast;
    end
    else
{}
    if (QueueType = jtStopGrpEvFromMa) OR fZERange then begin
      //Rangechange or stopp on ZE: Stopp the old, modified groups and delete v_prodgroup_state entry
      case JobRec^.NetTyp of
        ntWSC : begin//Do nothing because the ProdGrpInfoValues are filled from WSC
          Query.SQL.Text := cQryStopProdGrpForced;
        end;
        ntTXN : begin
          Query.SQL.Text := cQryStopProdGrp;
        end;
        ntLX : begin
          Query.SQL.Text := cQryStopProdGrpForced;
        end
      else
        raise EMMException.Create ( 'Tried to handle unknown nettyp in TDelZESpdDataJob.DispatchContinuousJob ');
      end;
      Query.Params.ParamByName('c_prod_state').AsInteger := ORD(psStopped); //4=Stopped
      Query.Params.ParamByName('c_prod_end').AsDateTime  := Now;
      Query.Params.ParamByName('c_prod_id').AsInteger    := fProdID;
      Query.ExecSQL;
    end
    else if QueueType <> jtDataAquEv then begin   //Nue 25.06.01
      //Need for stops and starts, when succeding action are following. Then it's important
      // that the c_prod_state stays on STOPPING, that no other action can interrupt the running one.
      Query.SQL.Text := cQrySetGrpStateBackM2;
      Query.Params.ParamByName('c_machine_id' ).AsInteger    := JobRec^.DelZESpdData.MachineID;
      Query.Params.ParamByName('c_spindle_first' ).AsInteger := JobRec^.DelZESpdData.SpindleFirst;
      Query.Params.ParamByName('c_spindle_last' ).AsInteger  := JobRec^.DelZESpdData.SpindleLast;
      Query.ExecSQL;
    end; // if
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TDelZESpdDataJob.DispatchContinuousJob failed.' + e.message );
      raise EMMException.Create ( fError.Msg );
    end;
  end;

end;
//-----------------------------------------------------------------------------

function TDelZESpdDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
var
  xIsMaOffline: Boolean;
  xBaseJob: TBaseJob;
  xJobRec: TJobRec;
begin
  Result := True;
  case aJobError of
    jtMsgNotComplete: begin
      case QueueType of
        jtDataAquEv: begin
            // if exists, next Job in queue will be started.
            if aLogEnabled then
              WriteLog(etWarning, Format('Couldn''t delete ZESpdData Ma:%d, SpdFirst:%d, SpdLast:%d correctly. JobTyp: %s',
                                         [JobRec^.DelZESpdData.MachineID, JobRec^.DelZESpdData.SpindleFirst, JobRec^.DelZESpdData.SpindleLast, mClassname]));

            //Undo Job for this ProdID has to be started
            //Reset state of prodgroup on DB will happen inside UnDoJob
            try
              Query.Close;
              Query.SQL.Text := cQrySelFragAndIntID;
              Query.Params.ParamByName('c_machine_id' ).AsInteger    := JobRec^.DelZESpdData.MachineID;
              Query.Params.ParamByName('c_spindle_first' ).AsInteger := JobRec^.DelZESpdData.SpindleFirst;
              Query.Params.ParamByName('c_spindle_last' ).AsInteger  := JobRec^.DelZESpdData.SpindleLast;

              Query.Open;
              if Query.FindFirst then begin
                //Undo Job for this ProdID has to be started
                xJobRec.JobTyp := jtUnDoGetZESpdData;
                xJobRec.UnDoGetZESpdData.MachineID    := JobRec^.DelZESpdData.MachineID;
                xJobRec.UnDoGetZESpdData.SpindleFirst := JobRec^.DelZESpdData.SpindleFirst;
                xJobRec.UnDoGetZESpdData.SpindleLast  := JobRec^.DelZESpdData.SpindleLast;
                xJobRec.UnDoGetZESpdData.ProdID       := Query.FieldByName('c_prod_id').AsInteger;;

                // Get the last, successful on DB saved, Frag- and Int-ID
                xJobRec.UnDoGetZESpdData.IntID  := Query.FieldByName('c_int_id_ok').AsInteger;
                xJobRec.UnDoGetZESpdData.FragID := Query.FieldByName('c_fragshift_id_ok').AsInteger;

                xBaseJob := TUnDoGetZESpdDataJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database, fParams,
                                                        QueueType, 0 {irrelevant}, fComputerName, fPort );
                // add UnDoGetZESpdDataJob to job list.
                xBaseJob.AddSelfToJobList(jsValid);
                xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
              end;
            Result := inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  //Calls free of this Job
            except
              on e:Exception do begin
                SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TDelZESpdDataJob.EmergencyJM setup UnDo-Job failed.' + e.message );
                raise EMMException.Create ( fError.Msg );
              end;
            end;
          end;

        jtStopGrpEv, jtStopGrpEvFromMa: begin
            try
              StopOldProdGrp(Query, Database, fProdID, fError); //Nue 9.5.00
  //            StopOldStartInconsProdGrp(Query, Database, fProdID, fNewDummyProdID, StrToInt(fParams.Value[cYarnCntUnit]), fError);
              if fComputerName<>'' then begin
                with JobRec^.DelZESpdData do
                  PrepareSendMsgToAppl(rmProdGrpStoppedOk, MachineID, SpindleFirst, SpindleLast, fComputerName, fPort, xJobRec);

                with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
                  AddSelfToJobList();

                WriteBufferTo(ttJobController, jtNewJobEv);
              end;
            except
              on e:Exception do begin
                 fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
                 raise EMMException.Create ( fError.Msg );
              end;
            end;
            if aLogEnabled then
              WriteLog(etWarning, Format('%s.EmergencyJM: Could not do DelZESpdData for ProdID %d correctly. Stopped it anyway.',
                                         [mClassname,fProdID]));

            Result := inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  //Calls free of this Job
          end;

        jtStartGrpEv, jtStartGrpEvFromMa: begin
            try
              xIsMaOffline := IsMaOffline(Query, JobRec^.GetExpDataStopZESpd.MachineID, fError);

              StopOldProdGrp(Query, Database, fProdID, fError);
              if fComputerName<>'' then begin
                with JobRec^.DelZESpdData do begin
                  if xIsMaOffline then
                     PrepareSendMsgToAppl(rmProdGrpStoppedNOk, MachineID, SpindleFirst, SpindleLast, fComputerName, fPort, xJobRec)
                  else
                     PrepareSendMsgToAppl(rmProdGrpStartedNOk, MachineID, SpindleFirst, SpindleLast, fComputerName, fPort, xJobRec)//                with xJobRec, SendMsgToAppl do begin
                end; //with

                with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
                  AddSelfToJobList();

                WriteBufferTo(ttJobController, jtNewJobEv);
              end;
            except
              on e:Exception do begin
                 fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
                 raise EMMException.Create ( fError.Msg );
              end;
            end;
            if aLogEnabled then
              WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the DelZESpdData job for ProdID %d correctly. Stopped it anyway.',
                                         [mClassname,fProdID]));
//            Result := inherited EmergencyJM(aJobError, False, aLogEnabled); //Calls free of this Job
            Result := inherited EmergencyJM(aJobError, xIsMaOffline, aLogEnabled); //Calls free of this Job
          end;
      else
      end; //case QueueType
    end;
  else
  end; //case aJobError of
end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TGenShiftDataJob }

constructor TGenShiftDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                    aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp; aNewDummyProdID: Longint; aParentJob: TBaseJob;
                                    aComputerName: TString30; aPort : TString30);
begin
//DONE wss: Initialisierung der Members vor dem Konstruktor?
  inherited create(aJobList, jtGenShiftData, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager,
                   aParentJob, aJobRec^.GenShiftData.MachineID, False, aComputerName, aPort);
  QueueType      := aQueueType;
  fNewDummyProdID := aNewDummyProdID;
  fParams         := aParams;
end;
//-----------------------------------------------------------------------------
function TGenShiftDataJob.Finish(aJobError: TJobTyp; aEmergency: Boolean): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := True;
  case QueueType of
    jtStartGrpEv, jtStartGrpEvFromMa: begin
      if aEmergency then begin
        try
          StopOldProdGrp(Query, Database, JobRec^.GenShiftData.ProdID, fError);
        except
          on e:Exception do begin
             fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.Finish ',[mClassname])+ e.message );
             raise EMMException.Create ( fError.Msg );
          end;
        end;
        if fComputerName<>'' then begin
          PrepareSendMsgToAppl(rmProdGrpStartedNOk, JobRec^.GenShiftData.MachineID, JobRec^.GenShiftData.SpindleFirst,
                               JobRec^.GenShiftData.SpindleLast, fComputerName, fPort, xJobRec);

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;

        WriteLog(etWarning, Format('%s.Finish: Could not  do the GenShiftData job for ProdID %d correctly. Stopped it anyway.',
          [mClassname,JobRec^.GenShiftData.ProdID]));
        Result := inherited EmergencyJM(aJobError, False, True);  //Calls free of this Job
      end;
    end;
    jtStopGrpEv, jtStopGrpEvFromMa: begin
      if aEmergency then begin
        try
          StopOldProdGrp(Query, Database, JobRec^.GenShiftData.ProdID, fError); //Nue 9.5.00
//          StopOldStartInconsProdGrp(Query, Database, JobRec^.GenShiftData.ProdID, fNewDummyProdID,
//                                    StrToInt(fParams.Value[cYarnCntUnit]), fError);
        except
          on e:Exception do begin
             fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.Finish ',[mClassname])+ e.message );
             raise EMMException.Create ( fError.Msg );
          end;
        end;
        WriteLog(etWarning, Format('%s.Finish: Could not  do the GenShiftData job for ProdID %d correctly. Stopped it anyway.',
          [mClassname,JobRec^.GenShiftData.ProdID]));
        //??Nue EmergencyJM-Call war bis 1.2.2000 hier nicht drin?
        Result := inherited EmergencyJM(aJobError, False, True);  //Calls free of this Job
      end;
    end;
  else
    if aEmergency then begin
      Result := inherited EmergencyJM(aJobError, True, True);  //Calls free of this Job
    end;
  end; //case
end;

//-----------------------------------------------------------------------------
function TGenShiftDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  Result := dcFalse;
  case aJobTyp of
  //May only jtJobSuccessful happen
    jtJobSuccessful: begin
        try
//Nue 25.06.01 Moved from TDelZESpdDataJob
          if QueueType=jtDataAquEv then begin
            Query.Close;
            Query.SQL.Text := cQrySetGrpStateBackMProdGrp;
            Query.Params.ParamByName('c_prod_id' ).AsInteger := JobRec^.GenShiftData.ProdID;
            Query.ExecSQL;
          end;
//End Moved from
          Finish(aJobTyp, False);
        except
          on e:Exception do begin
             fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.DispatchContinuousJob ',[mClassname])+ e.message );
             raise EMMException.Create ( fError.Msg );
          end;
        end;
      end;
  else
  end;
end;

//-----------------------------------------------------------------------------

function TGenShiftDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
//var
//  xBaseJob    : TBaseJob;
//  xJobRec     : TJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed: begin
      try
        Result:= Finish(aJobError,True);
      except
        on e:Exception do begin
           fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
           raise EMMException.Create ( fError.Msg );
        end;
      end;
    end;
  end; //case
end;

//-----------------------------------------------------------------------------
{ TGenExpShiftDataJob }

constructor TGenExpShiftDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                       aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp; aNewDummyProdID: Longint; aParentJob: TBaseJob;
                                       aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtGenExpShiftData, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager,
                   aParentJob, aJobRec^.GenExpShiftData.MachineID, False, aComputerName, aPort);
  QueueType      := aQueueType;
  fNewDummyProdID := aNewDummyProdID;
  fParams         := aParams;
end;

//-----------------------------------------------------------------------------
function TGenExpShiftDataJob.Finish(aJobError: TJobTyp; aEmergency: Boolean): Boolean;
var
  xJobRec: TJobRec;
begin
  Result := True;
  case QueueType of
    jtStartGrpEv, jtStartGrpEvFromMa: begin
      if aEmergency then begin
        try
          StopOldProdGrp(Query, Database, JobRec^.GenExpShiftData.ProdID, fError);
        except
          on e:Exception do begin
             fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.Finish ',[mClassname])+ e.message );
             raise EMMException.Create ( fError.Msg );
          end;
        end;
        if fComputerName<>'' then begin
//TODO wss: w�re hier die Funktion PrepareSendMsgToAppl() auch einsetzbar?
//              with JobRec^.GenExpShiftData do
//                PrepareSendMsgToAppl(rmProdGrpStartedNOk, MachineID, SpindleFirst, SpindleLast, fComputerName, fPort, xJobRec);
          with xJobRec, SendMsgToAppl do begin
            JobTyp       := jtSendMsgToAppl;
            ComputerName := fComputerName;
            Port         := fPort;
            ResponseMsg.MsgTyp       := rmProdGrpStartedNOk;
            ResponseMsg.MachineID    := JobRec^.GenExpShiftData.MachineID;
            ResponseMsg.SpindleFirst := JobRec^.GenExpShiftData.SpindleFirst;
            ResponseMsg.SpindleLast  := JobRec^.GenExpShiftData.SpindleLast;
            ResponseMsg.ProdGrpID    := 0;
          end;

          with TSendMsgToApplJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database) do
            AddSelfToJobList();

          WriteBufferTo(ttJobController, jtNewJobEv);
        end;
        WriteLog(etWarning, Format('%s.Finish: Could not  do the GenExpShiftData job for ProdID %d correctly. Stopped it anyway.',
                                   [mClassname,JobRec^.GenExpShiftData.ProdID]));
        Result := inherited EmergencyJM(aJobError, False, True);  //Calls free of this Job
      end;
    end;
    jtStopGrpEv, jtStopGrpEvFromMa: begin
      if aEmergency then begin
        try
          StopOldProdGrp(Query, Database, JobRec^.GenExpShiftData.ProdID, fError); //Nue 9.5.00
//          StopOldStartInconsProdGrp(Query, Database, JobRec^.GenExpShiftData.ProdID, fNewDummyProdID,
//                                    StrToInt(fParams.Value[cYarnCntUnit]), fError);
        except
          on e:Exception do begin
             fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.Finish ',[mClassname])+ e.message );
             raise EMMException.Create ( fError.Msg );
          end;
        end;
        WriteLog(etWarning, Format('%s.Finish: Could not  do the GenExpShiftData job for ProdID %d correctly. Stopped it anyway.',
                                   [mClassname,JobRec^.GenExpShiftData.ProdID]));
        //??Nue EmergencyJM-Call war bis 1.2.2000 hier nicht drin?
        Result := inherited EmergencyJM(aJobError, False, True);  //Calls free of this Job
      end;
    end;
  else
    if aEmergency then begin
      Result := inherited EmergencyJM(aJobError, True, True);  //Calls free of this Job
    end;
  end; //case
end;

//-----------------------------------------------------------------------------
function TGenExpShiftDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  Result := dcFalse;
  case aJobTyp of
  //May only jtJobSuccessful happen
    jtJobSuccessful: begin
      try
//Nue 25.06.01 Moved from TDelZESpdDataJob
    if QueueType=jtDataAquEv then begin
      Query.Close;
      Query.SQL.Text := cQrySetGrpStateBackMProdGrp;
      Query.Params.ParamByName('c_prod_id' ).AsInteger := JobRec^.GenExpShiftData.ProdID;
{alt      Query.Params.ParamByName('c_machine_id' ).AsInteger := JobRec^.GenExpShiftData.MachineID;
      Query.Params.ParamByName('c_spindle_first' ).AsInteger := JobRec^.GenExpShiftData.SpindleFirst;
      Query.Params.ParamByName('c_spindle_last' ).AsInteger := JobRec^.GenExpShiftData.SpindleLast;
}
      Query.ExecSQL;
    end;
//End Moved from
        Finish(aJobTyp, False);
      except
        on e:Exception do begin
           fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.DispatchContinuousJob ',[mClassname])+ e.message );
           raise EMMException.Create ( fError.Msg );
        end;
      end;
    end;
  end;
end;

//-----------------------------------------------------------------------------

function TGenExpShiftDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
//var
//  xBaseJob    : TBaseJob;
//  xJobRec     : PJobRec;
begin
  Result := True;
  case aJobError of
    jtJobFailed: begin
      try
        Result:= Finish(aJobError, True);
      except
        on e:Exception do begin
           fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
           raise EMMException.Create ( fError.Msg );
        end;
      end;
    end;
  end; //case
end;
//-----------------------------------------------------------------------------
{ TGenQOfflimitJob }

function TGenQOfflimitJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;  //Nue: Methode new 4.4.01
begin
//@@Nue  WriteLogDebug(etInformation, Format('Start GenQOfflimitJob (send to StorageHandler)!: %s ', [DateTimeToStr(Now)]), True);
  Result := inherited DispatchContinuousJob(aJobTyp);
end;

//-----------------------------------------------------------------------------

{ TGenSpdOfflimitsJob }

constructor TGenSpdOfflimitsJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                       aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtGenSpdOfflimits, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager, aParentJob, aJobRec^.GenSpdOfflimits.MachineID);
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TGenDWDataJob }

constructor TGenDWDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                 aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtGenDWData, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager, aParentJob, aJobRec^.GenDWData.MachineID);
end;
//-----------------------------------------------------------------------------

//??Nue: Methode only for tests 26.4.99
function TGenDWDataJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;  //??Nue: Methode only for tests 26.4.99
begin
//  Result := dcFalse;
//  WriteLog(etInformation, Format('All Intervaljobs done!: %s ', [DateTimeToStr(Now)]));
//  WriteLog(etInformation, Format('End Dataaquisation: %s ', [DateTimeToStr(Now)]));
  Result := inherited DispatchContinuousJob(aJobTyp);
//  WriteBufferTo(ttJobManager,jtTest);
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
{ TSetNodeListJob }

constructor TSetNodeListJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aNetTyp: TNetTyp; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtSetNodeList, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, cNoMachineID);
  fNetTyp := aNetTyp;
end;
//-----------------------------------------------------------------------------

function TSetNodeListJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  WriteLogDebug('Enter: TSetNodeListJob.DispatchContinuousJob');
  {wss Result := }inherited DispatchContinuousJob(aJobTyp);
  Result := dcFalse;
end;
//-----------------------------------------------------------------------------
{ TGetMaAssignJob }

constructor TGetMaAssignJob.Create(aJobList: TJobList; aJobRec: PJobRec; aOrgJobRec: TGetSettingsAllGroups; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtGetMaAssign, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController, aParentJob, aJobRec^.GetMaAssign.MachineID);
  fOrgJobRec := aOrgJobRec;

end;
//-----------------------------------------------------------------------------

function TGetMaAssignJob.AnalyseJobData: Boolean;
var
  xJob: TJobRec;
  x : Integer;
  xFound: Boolean;
begin
  Result := True;
  xFound := False;

  xJob.NetTyp := JobRec^.NetTyp;
  xJob.JobTyp := jtGetSettingsAllGroups;
  xJob.GetSettingsAllGroups.MachineID := JobRec^.GetMaAssign.MachineID;
  xJob.GetSettingsAllGroups.AllXMLValues := fOrgJobRec.AllXMLValues;   //AllXMLValues aus OriginalJob in neuen (xJob) �bernehmen. (Nue)
  for x:=0 to cMaxGroupMemoryLimit-1 do begin  //?????????????????????
//  for x:=0 to cZESpdGroupLimit do begin
    with JobRec^.GetMaAssign.Assigns.Groups[x] do begin
      xJob.GetSettingsAllGroups.SpindleRanges[x].SpindleFirst := SpindleFirst;
      xJob.GetSettingsAllGroups.SpindleRanges[x].SpindleLast  := SpindleLast;
      //Modification because of coldstart ZE (Nue: 8.3.2001)
      if (SpindleFirst <> $FF) or (SpindleLast <> $FF) then
        xFound := True;
    end;
  end;

  //Modification because of coldstart ZE (Nue: 8.3.2001)
  if not xFound then begin
    xJob.GetSettingsAllGroups.SpindleRanges[0].SpindleFirst := 1;
    xJob.GetSettingsAllGroups.SpindleRanges[0].SpindleLast := cMaxSpindeln;
  end;


  with TGetSettingsAllGroupsJob.Create(mJobList, @xJob, fOrgJobRec, cNrOfTrials, Query, Database, NIL) do begin
    AddSelfToJobList(jsValid);
    WriteBufferTo(ttJobController, jtNewJobEv);
  end;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TMaEqualizeJob }

constructor TMaEqualizeJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aCritSec: TCriticalSection; aCallReason: TMaEqualizeReason;
                                   aFirstReceiver: TThreadTyp; aParentJob: TBaseJob);
var
  x : Integer;
begin
  fCritSec    := aCritSec;
  fCallReason := aCallReason;
  fParams     := aParams;

  //Setting default values
  with aJobRec.MaEqualize do begin
//    for x:=0 to cZESpdGroupLimit-1 do begin // MM: 0-basiert
    for x:=0 to cMaxGroupMemoryLimit-1 do begin // MM: 0-basiert   ??????????????????
      Assigns.Groups[x].ProdGrpParam.ProdGrpInfo.c_machine_id := MachineID;
//NUETMP      Assigns.Groups[x].ProdGrpParam.ProdGrpYMPara.group      := x;
      case aJobRec.NetTyp of
        ntWSC,ntLX : ;//Do nothing because the ProdGrpInfoValues are filled from WSC (LX)
        ntTXN : begin
            OldSetDefaultProdGrpData({var}Assigns.Groups[x].ProdGrpParam, {var}Assigns.Groups[x].Color);
            // TODO -cProdGrpInfo -oNUE: Hier muss noch (OldSetDefaultProdGrpData({var}Assigns.Groups[x].ProdGrpParam, {var}Assigns.Groups[x].Color) )eliminiert werden. Das hat jedoch Umbau im ganzen Basissystem zur Folge. ProdGrpInfo=> Nur noch wirklich notwendige Felder bearbeiten.
          end;
      else
        raise EMMException.Create ( 'Tried to handle unknown nettyp in TMaEqualizeJob.Create ');
      end;
    end;
  end;

  inherited create(aJobList, jtMaEqualize, aJobRec, aMaxTrials, aQuery, aDatabase, aFirstReceiver, aParentJob, aJobRec^.MaEqualize.MachineID);
end;
//-----------------------------------------------------------------------------

function TMaEqualizeJob.AddSelfToJobList(aJobState: TJobState=jsValid;
    aJobNext: DWord=0): DWord;
var
  xJob: TBaseJob;
  xFoundJobs: Word;
begin
  Result := 0;
  fCritSec.Enter;
  try
    if (JobGroup > 0) and mJobList.GetSameMaEqualizeJob(xJob, JobRec^.MaEqualize.MachineID, xFoundJobs) then
      if xJob.JobState=jsValid then begin
        Result := inherited AddSelfToJobList(jsInvalid, aJobNext);
      end
      else begin // xJobState=jsInvalid
        //A waiting MaEqualize is already in JobList; Throw away the actual!!
        WriteLog(etInformation, Format('MaEqualize job for machine %d not inserted, because a same job is already in process', [JobRec^.MaEqualize.MachineID]));
      end
    else begin
      Result := inherited AddSelfToJobList(aJobState, aJobNext);
    end;
    codesite.SendFmtMsg('TMaEqualizeJob.AddSelfToJobList: MachID=%d, JobID=%d, JobState=%d, JobNext=%d, JobGroupID=%d',
                        [JobRec^.MaEqualize.MachineID, JobRec^.JobID, ORD(aJobState), aJobNext, JobGroup]);
  finally
    fCritSec.Leave;
  end;
end;

//-----------------------------------------------------------------------------

function TMaEqualizeJob.AddSelfToJobList1(aJobState: TJobState; aJobNext: DWord;
    aJobGroupID: DWord): DWord;
var
  xJob: TBaseJob;
  xFoundJobs: Word;
begin
  Result := 0;
  fCritSec.Enter;
  try
    Self.JobGroup := aJobGroupID;
    if mJobList.GetSameMaEqualizeJob(xJob, JobRec^.MaEqualize.MachineID, xFoundJobs) then
      if xJob.JobState=jsValid then begin
        Result := inherited AddSelfToJobList(jsInvalid, aJobNext);
      end
      else begin // xJobState=jsInvalid
        //A waiting MaEqualize is already in JobList; Throw away the actual!!
        WriteLog(etInformation, Format('MaEqualize job for machine %d not inserted, because a same job is already in process', [JobRec^.MaEqualize.MachineID]));
      end
    else begin
      Result := inherited AddSelfToJobList(aJobState, aJobNext);
    end;
    codesite.SendFmtMsg('TMaEqualizeJob.AddSelfToJobList: MachID=%d, JobID=%d, JobState=%d, JobNext=%d, JobGroupID=%d',
                        [JobRec^.MaEqualize.MachineID, JobRec^.JobID, ORD(aJobState), aJobNext, aJobGroupID]);
  finally
    fCritSec.Leave;
  end;
end;

//-----------------------------------------------------------------------------


function TMaEqualizeJob.AnalyseJobData: Boolean;
type
  TWho = (wtMM, wtZE, wtOk, wtNone);
  TAction = (atDone,
             atMMStop,           // Overloading spindle ranges on MM; stop on MM; Start MM Grp with
                                 //  settings from ZE; reload MM settings to ZE with new ProdID
             atMMStart,          // No simular MM-entry found in ZE-entries; Start MM Grp with
                                 //  settings from ZE; reload MM settings to ZE with new ProdID
             atMMStopAndStart,   // A stop and a succeeding start happend
             atMMZEEqual,        // ZE and MM are equal;
             atZEStopReloadMM,   // ??Nue  Not used
             atMMStopReloadZE,   // Stop Grp on MM; restart new one with the ZE settings on MM;
                                 //  reload MM settings to ZE
             atZEInitialReset );
  TProdGrpItem = record
                   MMProdId, ZEProdId : Longint;
                   SpindleFirst, SpindleLast : Word;
                   GroupState : TGroupState;  //Nue 29.03.20000
                   YMSetId : Word;      //Nue 18.11.99
                   ColorId : Integer;   //Nue19.01.2000
                   Action : TAction;
                 end;
  TProdGrpList = array[0..cZESpdGroupLimit-1] of TProdGrpItem;

var
  xMachID: Word;
  x,y          : DWord;
  xNetTyp: TNetTyp; //wss Word;
  xMMList,
  xZEList,
  xDefList    : TProdGrpList;
//wss  xVirginStart,
  xZEColdstart: Boolean;
  xBaseJob: TBaseJob;
//wss  xBaseJob1,
//wss  xQuery1     : TNativeAdoQuery;
  xJobRec     : TJobRec;
//wss  xDummyProdID: Longint;
  xSynchJobInserted : Boolean;
  xOldIntID: Byte;            //IntID before the actual
  xOldFragID: Longint;        //FragID before the actual
  xColor: Integer;   //Color for representation on floor
  xProdGrpInfo: TProdGrpInfo;
  xYMSetName: TText50;
  xSettingsRec: TXMLSettingsRec;
// DONE: XML: kann hier eventuell trotzdem TJobRec verwendet bleiben?
  //...............................................................
  function SynchronizeJob(aJobState: TJobState=jsInvalid): DWord;  // Local function
  var
    xBaseJob   : TBaseJob;
  begin
    Result := 0;
    if not xSynchJobInserted then begin

//Nue:10.4.08
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData.SynchronizeJob: Adding SynchronizeJob: MachID=%d', [xMachID]);

      xJobRec.JobTyp := jtSynchronize;
      xJobRec.Synchronize.MachineID := xMachID;
      xBaseJob := TSynchronizeJob.Create(mJobList, @xJobRec, 2, Query, Database);
      Result   := xBaseJob.AddSelfToJobList(aJobState);
      if Result = 0 then
        WriteLog(etError, 'Adding job Synchronize to joblist failed: '+FormatMMErrorText(mJobList.ListError,etNTError));
      xSynchJobInserted := True;
    end;
  end;

  //...............................................................
  function ClearZEAndSetSettings(aSpindleFirst, aSpindleLast, aMachineGrp: Word; aJobRec: TJobRec;
                 aMachineID: Word; aSettingsRec: TXMLSettingsRec; aStartMode: TProdGrpStartMode; aProdState: TProdGrpState; aColor: Integer; aYMSetId: Word; aAssignment: Byte): Boolean;
  begin
// DONE wss: XML kontrollieren
    Result := True;
    aJobRec.JobTyp                      := jtClearZESpdData;
    aJobRec.ClearZESpdData.MachineID    := aMachineID;
//    aJobRec.ClearZESpdData.MachineID    := aProdGrpData.ProdGrpInfo.c_machine_id;
    aJobRec.ClearZESpdData.SpindleFirst := aSpindleFirst;
    aJobRec.ClearZESpdData.SpindleLast  := aSpindleLast;
    xBaseJob := TClearZESpdDataJob.Create(mJobList, @aJobRec, cNrOfTrials, Query, Database);

    //Fill settings job
    with aJobRec, SetSettings, SettingsRec do begin
      JobTyp          := jtSetSettings;
      // wss: MachineID wurde im Original nicht gesetzt! Warum nicht?
      MachineID       := aMachineID;
      SettingsRec     := aSettingsRec;
      Group           := aMachineGrp;
      Color           := aColor;
      YMSetChanged    := True; //Nue:20.9.01
//TODO wss: warum kein Spindelbereich setzen?
//      SpindleFirst    := aSpindleFirst;
//      SpindleLast     := aSpindleLast;
//wss      ProdGrpInfo    := aProdGrpData.ProdGrpInfo;
//      ProdGrpID       := aProdGrpData.ProdGrpInfo.c_prod_id;
//      OrderPositionID := aProdGrpData.ProdGrpInfo.c_order_position_id;
//      OrderID         := aProdGrpData.ProdGrpInfo.c_order_id;
//      StyleID         := aProdGrpData.ProdGrpInfo.c_style_id;
//      YMSetID         := aProdGrpData.ProdGrpInfo.c_YM_set_id;
//      YMSetName       := aProdGrpData.ProdGrpInfo.c_YM_set_name;
//      ProdName        := aProdGrpData.ProdGrpInfo.c_prod_name;
//      Slip            := aProdGrpData.ProdGrpInfo.c_slip;
//      c_m_to_prod
//      c_clear_type
//      c_order_position_prod_id
//      c_style_name
//      c_order_position_name
    end;
    // dieser Job wird als Folgejob von ClearZESpdData in Queue eingef�gt
    TSetSettingsJob.Create(mJobList, @aJobRec, cNrOfTrials, aAssignment,
                                        aStartMode, aProdState, aYMSetId, Query, Database, fParams,
                                        xOldIntID, xOldFragID, ttMsgController, xBaseJob);

//Nue:10.4.08
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData.ClearZEAndSetSettings: MachID=%d, Grp=%d First=%d Last=%d, After TSetSettingsJob.Create and before xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob)',
     [aMachineID,aMachineGrp,aSpindleFirst, aSpindleLast]);

    // add (queue) ClearZESpdData to job list.
    xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob);

   //Send new job msg
    xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
  end;

  //  function ClearZEAndSetSettings(aSpindleFirst, aSpindleLast, aMachineGrp: Word; aJobRec: TJobRec;
//                 aProdGrpData: TProdGrpData; aStartMode: TProdGrpStartMode; aProdState: TProdGrpState; aColor: Integer; aYMSetId: Word; aAssignment: Byte): Boolean;
//  begin
//    Result := True;
//    aJobRec.JobTyp                      := jtClearZESpdData;
//    aJobRec.ClearZESpdData.MachineID    := aProdGrpData.ProdGrpInfo.c_machine_id;
//    aJobRec.ClearZESpdData.SpindleFirst := aSpindleFirst;
//    aJobRec.ClearZESpdData.SpindleLast  := aSpindleLast;
//    xBaseJob := TClearZESpdDataJob.Create(mJobList, @aJobRec, cNrOfTrials, Query, Database);
//
//    //Fill settings job
//    with aJobRec, SetSettings, SettingsRec do begin
//      JobTyp          := jtSetSettings;
//      // wss: MachineID wurde im Original nicht gesetzt! Warum nicht?
//      SpindleFirst    := aSpindleFirst;
//      SpindleLast     := aSpindleLast;
//      Group           := aMachineGrp;
//      Color           := aColor;
//      YMSetChanged    := True; //Nue:20.9.01
////wss      ProdGrpInfo    := aProdGrpData.ProdGrpInfo;
//      MachineID       := aProdGrpData.ProdGrpInfo.c_machine_id;
//      ProdGrpID       := aProdGrpData.ProdGrpInfo.c_prod_id;
//      OrderPositionID := aProdGrpData.ProdGrpInfo.c_order_position_id;
//      OrderID         := aProdGrpData.ProdGrpInfo.c_order_id;
//      StyleID         := aProdGrpData.ProdGrpInfo.c_style_id;
//      YMSetID         := aProdGrpData.ProdGrpInfo.c_YM_set_id;
//      YMSetName       := aProdGrpData.ProdGrpInfo.c_YM_set_name;
//      ProdName        := aProdGrpData.ProdGrpInfo.c_prod_name;
//      Slip            := aProdGrpData.ProdGrpInfo.c_slip;
////      c_m_to_prod
////      c_clear_type
////      c_order_position_prod_id
////      c_style_name
////      c_order_position_name
//    end;
//    // dieser Job wird als Folgejob von ClearZESpdData in Queue eingef�gt
//    {wss xBaseJob2 := }TSetSettingsJob.Create(mJobList, @aJobRec, cNrOfTrials, aAssignment,
//                                        aStartMode, aProdState, aYMSetId, Query, Database, fParams,
//                                        xOldIntID, xOldFragID, ttMsgController, xBaseJob);
//    // add (queue) ClearZESpdData to job list.
//    xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob());
//
//   //Send new job msg
//    xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
//  end;

//  function ClearZEAndSetSettings(aSpindleFirst, aSpindleLast, aMachineGrp: Word; aJobRec: TJobRec;
//                 aProdGrpData: TProdGrpData; aStartMode: TProdGrpStartMode; aProdState: TProdGrpState; aColor: Integer; aYMSetId: Word; aAssignment: Byte): Boolean;
//  begin
//    Result := True;
//    aJobRec.JobTyp := jtClearZESpdData;
//    aJobRec.ClearZESpdData.MachineID := aProdGrpData.ProdGrpInfo.c_machine_id;
//    aJobRec.ClearZESpdData.SpindleFirst := aSpindleFirst;
//    aJobRec.ClearZESpdData.SpindleLast := aSpindleLast;
//    xBaseJob := TClearZESpdDataJob.Create(mJobList, @aJobRec, cNrOfTrials, Query, Database);
////    xBaseJob := TClearZESpdDataJob.Create(mJobList, aJobRec, cNrOfTrials, Query, Database);
//
//  //  xJobRec.JobTyp := jtClearZEGrpData;
//  //  xBaseJob1 := TClearZEGrpDataJob.Create(mJobList, xJobRec, cNrOfTrials, Query, xBaseJob);
//
//    //Fill settings job
//    aJobRec.JobTyp := jtSetSettings;
//    aJobRec.SetSettings.SpindleFirst := aSpindleFirst;
//    aJobRec.SetSettings.SpindleLast := aSpindleLast;
//    aJobRec.SetSettings.MachineGrp := aMachineGrp;
//    aJobRec.SetSettings.Color := aColor;
//  //  StrPCopy(@aJobRec.SetSettings.YMSetName, '');  //Nue:20.9.01
//  //  FillChar(aJobRec.SetSettings.YMSetName, sizeof(aJobRec.SetSettings.YMSetName),0);  //Nue:20.9.01
//    aJobRec.SetSettings.YMSetChanged := True; //Nue:20.9.01
//    aJobRec.SetSettings.ProdGrpInfo := aProdGrpData.ProdGrpInfo;
////    xBaseJob2 := TSetSettingsJob.Create(mJobList, aJobRec, cNrOfTrials, aAssignment,
//    xBaseJob2 := TSetSettingsJob.Create(mJobList, @aJobRec, cNrOfTrials, aAssignment,
//                                        aStartMode, aProdState, aYMSetId, Query, Database, fParams,
//                                        xOldIntID, xOldFragID, ttMsgController, xBaseJob);
//    // add (queue) ClearAllZEData to job list.
//    xBaseJob.AddSelfToJobList(jsValid,SynchronizeJob());
//
//   //Send new job msg
//    if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
//    end;
//  end;
  //...............................................................
begin
  xSynchJobInserted := False;
  xZEColdstart      := True;
  xMachID           := JobRec^.MaEqualize.MachineID;
EnterMethod('TMaEqualizeJob.AnalyseJobData: MachID=' + IntToStr(xMachID));

  try //Cleaning up already stopped prodgroups on this machine in v_prodgroup_state
    GetPrevActIntIDAndFragID(Query, xOldIntID, xOldFragID, fError);

    Query.Close;
    Query.SQL.Text := cQryUpdProdGrpStopped;    //Nue 14.06.00
    Query.ExecSQL;
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData failed on cleaning up stopped prodgroup(s).' + e.message );
      raise EMMException.Create ( fError.Msg );
    end;
  end;

  try
//NUE1
//    Query.Close;
//    Query.SQL.Text := cQrySelMachine;
//    Query.Params.ParamByName('c_machine_id').AsInteger := xMachID;
//    Query.Open;
//    xMachineClass := Query.FieldByName('c_AWE_mach_type').AsInteger;

    Query.Close;
    Query.SQL.Text := cQrySelMachine;
    Query.Params.ParamByName('c_machine_id').AsInteger := xMachID;
    Query.Open;
    xNetTyp := TNetTyp(Query.FieldByName('c_net_id').AsInteger);

    Query.Close;
    Query.SQL.Text := cQrySelMaProdGrps;
    Query.Params.ParamByName('c_machine_id' ).AsInteger := xMachID;
    Query.Open;
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData cQrySelMachine or cQrySelMaProdGrps failed.' + e.message );
      raise EMMException.Create ( fError.Msg );
    end;
  end;

  FillChar(xDefList, sizeof(xDefList),0);
  //Fill ZE-Grps in ZEList
  FillChar(xZEList, sizeof(xZEList),0);

  for x:=LOW(xZEList) to HIGH(xZEList)do begin
    with JobRec^.MaEqualize.Assigns.Groups[x] do begin
      xZEList[x].ZEProdId     := ProdId;
      xZEList[x].SpindleFirst := SpindleFirst;
      xZEList[x].SpindleLast  := SpindleLast;
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: JobRec^.MaEqualize.Assigns.Groups[%d] MachID=%d, ProdId=%d, SpFirst=%d, SpLast=%d',
       [x,JobRec^.MaEqualize.MachineID, xZEList[x].ZEProdId, xZEList[x].SpindleFirst, xZEList[x].SpindleLast]);

      if (SpindleFirst<>$FF) AND (SpindleLast<>$FF) then   //30.10.02: Nue: Check auf FF, weil es auf ZE vorkommen kann (Kaltstart bei offenem TEXNET)
                                                           //  dass der Bereich FF ist und der Event kein deInitialReset ist (Kaltstart verschluckt)
        xZEList[x].GroupState := GroupState
      else                                    //30.10.02: Nue
        xZEList[x].GroupState := gsFree;      //30.10.02: Nue
      xZEList[x].YMSetId := cDummyYMSetID;
      xZEList[x].ColorId := cDefaultStyleIDColor;
//Alt bis 12.11.01      if (SpindleFirst<>$FF) then

      if (JobRec^.MaEqualize.Assigns.Event<>deInitialReset) OR //Nue: 28.10.02
         ((JobRec^.MaEqualize.Assigns.Event=deInitialReset) and (JobRec^.MaEqualize.Assigns.Groups[x].ProdId<>0)) then begin//Nue: 28.10.02
        xZEColdstart := False;
      end;
    end;
  end; // for x

  if Query.FindFirst then begin
    //Fill MM-Grps in MMList
    FillChar(xMMList, sizeof(xMMList),0);
    while not Query.EOF do begin  // ADO Conform
      x := Query.FieldByName('c_machineGroup').AsInteger;  // MM, DB: 0-basiert
      xMMList[x].MMProdId     := Query.FieldByName('c_prod_id').AsInteger;
      xMMList[x].SpindleFirst := Query.FieldByName('c_spindle_first').AsInteger;
      xMMList[x].SpindleLast  := Query.FieldByName('c_spindle_last').AsInteger;
      xMMList[x].GroupState   := gsInProd; //All groups on DB are in state InProd
      xMMList[x].YMSetId      := Query.FieldByName('c_YM_set_id').AsInteger;
      xMMList[x].ColorId      := Query.FieldByName('c_color').AsInteger;
      Query.Next;
    end;

    //Handling if no group assigned (coldstart) on ZE

    if xZEColdstart then begin
      WriteLog(etInformation, Format('TMaEqualizeJob.AnalyseJobData: Ma:%d: Reload Settings from MM (declEvent InitialReset detected!)',
                                     [JobRec^.MaEqualize.MachineID]));

      //Coldstart take over assignements from MM
      //It's never possible, that a spindle isn't assigned on MM because on creation
      // of a new maschine every spindle will be assigned (inconsistent prodgroup)!
      for x:=LOW(xDefList) to HIGH(xDefList)do begin
        xDefList[x].MMProdId     := xMMList[x].MMProdId;
        xDefList[x].SpindleFirst := xMMList[x].SpindleFirst;
        xDefList[x].SpindleLast  := xMMList[x].SpindleLast;
        xDefList[x].GroupState   := xMMList[x].GroupState;
        xDefList[x].YMSetId      := xMMList[x].YMSetId;
        xDefList[x].ColorId      := xMMList[x].ColorId;

        if (xDefList[x].MMProdId = 0) or (xNetTyp = ntWSC) then    //Nue:21.09.02 After Coldstart on AC338, no Prodgroup will be in state 'In Production' -> No Prodgroups on MM after Coldstart
          xDefList[x].Action := atDone //No group defined on MM
        else if (xNetTyp = ntLX) then begin
          //For LX all running groups on MM have to be stopped, because on LX is only 1 group with TK Unknown defined
          //  and we are not able, to set TK from MM!! Nue:9.11.05
          xDefList[x].Action := atMMStop;
          codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: LXColdstart, stopp all ProdGrps on MM: MachID=%d, MMSpFirst=%d, MMSpLast=%d, JobID=%d',
                    [JobRec^.MaEqualize.MachineID, xMMList[x].SpindleFirst, xMMList[x].SpindleLast, JobRec^.JobID]);
        end
        else begin
          xDefList[x].Action := atZEInitialReset;
          //For Inside machines: Take over ProdGrps from MM only if spindle ranges on MM correspond with those on ZE (Nue:12.11.01)
          if fCallReason=grZEColdstart then
            for y:=LOW(xZEList) to HIGH(xZEList)do begin
              if (xZEList[y].SpindleFirst=xMMList[x].SpindleFirst) and (xZEList[y].SpindleLast<>xMMList[x].SpindleLast) then begin
                xDefList[x].Action := atDone; //No group defined on MM
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: atZEInitialReset: MachID=%d, MMSpFirst=%d, ZESpFirst=%d, MMSpLast=%d, ZESpLast=%d, JobID=%d',
                    [JobRec^.MaEqualize.MachineID, xMMList[x].SpindleFirst, xZEList[y].SpindleFirst, xMMList[x].SpindleLast, xZEList[y].SpindleLast, JobRec^.JobID]);
              end;
            end; //for
        end;
      end;
    end

    else begin
      //At the moment ZE is always master
      //Join ZEList and MMList
      for x:=LOW(xDefList) to HIGH(xDefList)do begin
        if (xZEList[x].ZEProdId = xMMList[x].MMProdId) AND
//           ((xZEList[x].GroupState = gsInProd) or (xZEList[x].GroupState=gsLotChange)) AND
           (xZEList[x].GroupState in [gsInProd, gsLotChange]) AND
           (xZEList[x].ZEProdId <> 0) AND
           (xZEList[x].SpindleFirst = xMMList[x].SpindleFirst) AND  //Nue 10.05.2000
           (xZEList[x].SpindleLast = xMMList[x].SpindleLast) then begin //Nue 10.05.2000
          //ProdId equal -> ZE and MM are equal (probably settings changed -> check Modify)
          xDefList[x].ZEProdId     := xMMList[x].MMProdId;
          xDefList[x].MMProdId     := xMMList[x].MMProdId;
          xDefList[x].SpindleFirst := xMMList[x].SpindleFirst;
          xDefList[x].SpindleLast  := xMMList[x].SpindleLast;
          xDefList[x].GroupState   := xMMList[x].GroupState;
          xDefList[x].YMSetId      := xMMList[x].YMSetId;
          xDefList[x].ColorId      := xMMList[x].ColorId;
          with JobRec^.MaEqualize.Assigns.Groups[x] do begin
            if (0 IN Modified) OR (1 IN Modified) then begin // Bit 0: spindle range; Bit 1: parameter
              //Nur bei WSC relevant: Settings Changed
              xDefList[x].Action := atMMStopReloadZE;
            end
// TODO wss: nach WSC Partiestart, Off/Online hat Modified den Wert [3..4]. Warum? -> WSCHanlder pr�fen
            else if 4 IN Modified then begin // Bit 4: Partie Stop WSC;
              //Nur bei WSC relevant: No Aquisation of the data
              xDefList[x].Action := atMMStop;
            end
            else begin
              //No Changes
              xDefList[x].Action := atMMZEEqual;
            end;
          end;
        end
//        else if  (xZEList[x].GroupState=gsInProd) OR (xZEList[x].GroupState=gsLotChange) then begin
        else if xZEList[x].GroupState in [gsInProd, gsLotChange] then begin
        //No simular MM-entry found in ZE-entries -> Fill ZE-entry in DefList
          with JobRec^.MaEqualize.Assigns.Groups[x] do begin
            xDefList[x].ZEProdId     := xZEList[x].ZEProdId;
            xDefList[x].SpindleFirst := xZEList[x].SpindleFirst;
            xDefList[x].SpindleLast  := xZEList[x].SpindleLast;
            xDefList[x].GroupState   := xZEList[x].GroupState;
            xDefList[x].YMSetId      := xZEList[x].YMSetId;
            xDefList[x].ColorId      := xZEList[x].ColorId;

            if (xNetTyp = ntWSC) or (xNetTyp = ntLX) then begin
                xDefList[x].Action := atMMStart;
                //Auf MM noch laufende ProdGrps auf dieser Maschine und MaschinenGruppe werden im
                // Query cQryUpdDummyProdGrp sauber aufgeraeumt (ohne Aquisation!) siehe Aenderung von 23.05.00 im Query
            end
                //Auf MM noch laufende ProdGrps auf dieser Maschine und MaschinenGruppe werden im
                // Query cQryUpdDummyProdGrp sauber aufgeraeumt (ohne Aquisation!) siehe Aenderung von 23.05.00 im Query
 { TODO 1 -oNue : 21.11.02: Check because of Inside!!!!!??????? }


            else if xZEList[x].ZEProdId=0 then begin
              if xZEList[x].SpindleFirst<>$FF then begin // ZE
                xDefList[x].Action := atMMStart;
                //Auf MM noch laufende ProdGrps auf dieser Maschine und MaschinenGruppe werden im
                // Query cQryUpdDummyProdGrp sauber aufgeraeumt (ohne Aquisation!) siehe Aenderung von 23.05.00 im Query
              end
              else begin //Group wurde "geschluckt" (nur ZE)
                xDefList[x].Action := atDone;  //Nue:25.5.00 For groups already ordinarly stopped on MM; may not filled in again
              end;
            end
            else begin
              xDefList[x].Action := atDone;  //Nue:4.5.00 For groups already ordinarly stopped on MM; may not filled in again
            end;
          end; //with
        end

        else begin //NOT ((xZEList[x].GroupState=gsInProd) OR (xZEList[x].GroupState=gsLotChange))
          if xMMList[x].MMProdId<>0 then begin
            //Stoppen von Prodgrps only on MM
            xDefList[x].MMProdId     := xMMList[x].MMProdId;
            xDefList[x].SpindleFirst := xMMList[x].SpindleFirst;
            xDefList[x].SpindleLast  := xMMList[x].SpindleLast;
            xDefList[x].Action       := atMMStop;
          end;
        end;
      end; (*for x*)

    end;
  end
  else begin
    //No entries on DB take over groups from machine in state InProd
    xDefList := xZEList;
    for x:=LOW(xDefList) to HIGH(xDefList)do begin
//      if (xZEList[x].GroupState=gsInProd) OR (xZEList[x].GroupState=gsLotChange) then begin
      if xZEList[x].GroupState in [gsInProd, gsLotChange] then begin
        with JobRec^.MaEqualize.Assigns.Groups[x] do begin
          case xNetTyp of
            ntWSC, ntLX: begin
                xDefList[x].GroupState := gsInProd;
                xDefList[x].Action     := atMMStart;
              end;
            else begin
{ TODO 1 -oNue : 25.11.02: Check because of Inside!!!!!??????? }
              if (ProdId=0) AND ((SpindleFirst<>$FF) AND (SpindleLast<>$FF)) then begin
                xDefList[x].GroupState := gsInProd;
                xDefList[x].Action     := atMMStart;
              end
              else begin
                xDefList[x].Action := atDone;
              end;
            end;
          end;
        end;
      end;
    end;
  end;

  //List with all accessable prodgroups from ZE and MM built in xDefList
  //Mainloop to access all prodgroups having state atMMStop first
  for x:=LOW(xDefList) to HIGH(xDefList)do begin
    with xDefList[x] do begin
      FillChar(xJobRec, sizeof(xJobRec),0);
      xJobRec.NetTyp := JobRec^.NetTyp;
      case Action of
        atMMStop, atMMStopAndStart        : begin   //ok 17.6.99
        //ProdGrp only on MM -> Stop on MM, without dataaquisation on ZE
          try
            Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
            Query.Close;
            Query.SQL.Text := cQryStopProdGrpForced;
            Query.Params.ParamByName('c_prod_state' ).AsInteger := ORD(psStopped); //4=Stopped
            Query.Params.ParamByName('c_prod_end').AsDateTime   := Now;
            Query.Params.ParamByName('c_prod_id' ).AsInteger    := MMProdID;
            Query.ExecSQL;
            if Action = atMMStopAndStart then begin
              Action := atMMStart;
              WriteLog(etWarning, Format('ProdGrp %d (Machine:%d) only on MM -> Stop on MM, '+
                'without dataaquisation on ZE! Start new ProdGrp (Spindle:%d-%d) on same MachineGrp(%d)',
                [MMProdID,xMachID,SpindleFirst,SpindleLast,x]));
            end
            else begin
              Action := atDone;
              WriteLog(etWarning, Format('ProdGrp %d (Machine:%d Spindle:%d-%d,MachineGrp:%d) only on MM -> Stop on MM, '+
                'without dataaquisation on ZE!',[MMProdID,xMachID,SpindleFirst,SpindleLast,x]));
            end;
            Database.Commit;
          except
            on e:Exception do begin
              SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData failed (2)(Case MMStop).' + e.message );
              Database.Rollback;
              raise EMMException.Create ( fError.Msg );
            end;
          end;
        end;
      else (* *);
      end;
    end; (*with*)
  end; (*for x*)

  //Mainloop to access all prodgroups
  for x:=LOW(xDefList) to HIGH(xDefList)do begin
    with xDefList[x] do begin
      FillChar(xJobRec, sizeof(xJobRec), 0);
// TODO wss: Warum wird hier der Inhalt vom Pointer in xJobRec kopiert? Welche Informationen werden aus JobRec ben�tigt?
//      xJobRec := JobRec^;
      y := sizeof(xJobRec);
      if JobRec^.JobLen < y then
        y := JobRec^.JobLen;
      System.Move(JobRec^, xJobRec, y);

      case Action of
        atMMStart       : begin  //Nue??   19.7.99
        //In this situation data on ZE will be thrown away. (No data aquisation)
        //ProdGrp only on ZE -> Start new ProdGrp(inconsistent) on MM
          xJobRec.JobTyp                      := jtClearZESpdData;
          xJobRec.ClearZESpdData.MachineID    := xMachID;
          xJobRec.ClearZESpdData.SpindleFirst := SpindleFirst;
          xJobRec.ClearZESpdData.SpindleLast  := SpindleLast;
          xBaseJob := TClearZESpdDataJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database);
          try
// TODO wss: Warum wird hier wieder der Inhalt vom Pointer nach lokaler Variable kopiert? Was wird ben�tigt?
//            xJobRec := JobRec^;
            y := sizeof(xJobRec);
            if JobRec^.JobLen < y then
              y := JobRec^.JobLen;
            System.Move(JobRec^, xJobRec, y);

            xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpInfo.c_machine_id := xMachID;
//NUETMP            xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpYMPara.group      := x;

            //Kopieren von xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam nach xSettings
            with xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam do begin   //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//NUETMP              xSettingsRec.group           := ProdGrpYMPara.group;  //Neu nue 23.9.04
              xSettingsRec.group           := x;  //NUETMP
              xSettingsRec.OrderPositionID := ProdGrpInfo.c_order_position_id;
              xSettingsRec.OrderID         := ProdGrpInfo.c_order_id;
              xSettingsRec.StyleID         := ProdGrpInfo.c_style_id;
              xSettingsRec.Slip            := ProdGrpInfo.c_slip;
              xSettingsRec.YMSetID         := ProdGrpInfo.c_ym_set_id;    //??Check nicht notwendig  Nue:21.07.05
              xSettingsRec.YMSetName       := ProdGrpInfo.c_YM_set_name;
              xSettingsRec.ProdName       := ProdGrpInfo.c_prod_name;
            end; //with

            //Neue ProdGrpID wird in InsertDummyProdGrp bereits in xSettingsRec abgef�llt (nue)
            InsertDummyProdGrp(Query, xMachID, SpindleFirst, SpindleLast, TYarnUnit(fParams.Value[cYarnCntUnit]),
                               0, smZE, psNotStarted, xSettingsRec,
                               xJobRec.MaEqualize.Assigns.Groups[x].Color, xYMSetName, fError);
            // DONE wss: Parameter umkopieren von ProdGrpParam nach xSettingsRec
            xProdGrpInfo := xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpInfo;
            xColor       := xJobRec.MaEqualize.Assigns.Groups[x].Color;

          except
            on e:Exception do begin
              SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData(cQryInsDummyProdGrp) atMMStart failed.' + e.message );
              raise EMMException.Create ( fError.Msg );
            end;
          end;

          xJobRec.JobTyp := jtSetSettings;
          with xJobRec.SetSettings, SettingsRec do begin
            // XMLData zur�ck setzen
            XMLData[0]     := #00;
            SpindleFirst   := xSettingsRec.SpindleFirst;
            SpindleLast    := xSettingsRec.SpindleLast;
            Group          := x;
            Color          := xColor;
//          xJobRec.SetSettings.SettingsRec.ProdGrpInfo    := xProdGrpInfo;
            MachineID       := xProdGrpInfo.c_machine_id;
            ProdGrpID       := xSettingsRec.ProdGrpID;
            OrderPositionID := xProdGrpInfo.c_order_position_id;
            OrderID         := xProdGrpInfo.c_order_id;
            StyleID         := xProdGrpInfo.c_style_id;
            YMSetID         := xProdGrpInfo.c_YM_set_id;
            YMSetName       := xYMSetName;
            YMSetChanged    := True; //Nue:20.9.01
            ProdName        := xProdGrpInfo.c_prod_name;
            Slip            := xProdGrpInfo.c_slip;
//      c_m_to_prod
//      c_clear_type
//      c_order_position_prod_id
//      c_style_name
//      c_order_position_name
          end;
          //Start neue ProdGrp on MM und download neue ProdID auf ZE (mit SetSettings(assignment=None))
codesite.SendFmtMsg('MaEqualizeJob.AnalyseJobData: atMMStart: Ma: %d FirstSpd: %d LastSpd: %d YMSetName: %s Slip1: %d Slip2: %d',
                    [xMachID,SpindleFirst,SpindleLast,strPas(@xJobRec.SetSettings.SettingsRec.YMSetName),
                    xProdGrpInfo.c_slip,xJobRec.SetSettings.SettingsRec.Slip]);

          // dieser Job wird als Folgejob von xBaseJob eingeschlauft, daher wird dies nicht auch noch
          // per AddSelfToJobList in die JobListe hinzugef�gt
          //wss: xBaseJob kommt von TClearZESpdDataJob.Create weiter oben
          TSetSettingsJob.Create(mJobList, @xJobRec, cNrOfTrials, cAssignProdGrpIDOnly,
                                 smZE, psInconsistent, YMSetID, Query, Database, fParams,
                                 xOldIntID, xOldFragID, ttMsgController, xBaseJob);

//Nue:10.4.08
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: atMMStart: MachID=%d MachID(Grp)=%d, Grp=%d First=%d Last=%d, After TSetSettingsJob.Create and before xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob)',
     [xMachID,xProdGrpInfo.c_machine_id,x,SpindleFirst, SpindleLast]);

          // add (queue) SetSettings to job list.
          xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob);

          //Send new job msg
          xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
        end;
//        atMMStart       : begin  //Nue??   19.7.99
//        //In this situation data on ZE will be thrown away. (No data aquisation)
//        //ProdGrp only on ZE -> Start new ProdGrp(inconsistent) on MM
//          xJobRec.JobTyp := jtClearZESpdData;
//          xJobRec.ClearZESpdData.MachineID := xMachID;
//          xJobRec.ClearZESpdData.SpindleFirst := SpindleFirst;
//          xJobRec.ClearZESpdData.SpindleLast := SpindleLast;
//          xBaseJob := TClearZESpdDataJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database);
////          xBaseJob := TClearZESpdDataJob.Create(mJobList, xJobRec, cNrOfTrials, Query, Database);
//          try
//            xJobRec := JobRec^;
//            xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpInfo.c_machine_id := xMachID;
//            xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpYMPara.group := x;
//            xDummyProdID := InsertDummyProdGrp(Query, SpindleFirst, SpindleLast, StrToInt(fParams.Value[cYarnCntUnit]),
//                                        0{No OldProdID},smZE, psNotStarted, xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam,
//                                  xJobRec.MaEqualize.Assigns.Groups[x].Color, xYMSetName, fError);
//            xProdGrpInfo := xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpInfo;
//            xColor := xJobRec.MaEqualize.Assigns.Groups[x].Color;
//          except
//            on e:Exception do begin
//              SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData(cQryInsDummyProdGrp) atMMStart failed.' + e.message );
//              raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//
//          xJobRec.JobTyp := jtSetSettings;
//          xJobRec.SetSettings.ProdGrpInfo := xProdGrpInfo;
//          xJobRec.SetSettings.SpindleFirst := SpindleFirst;
//          xJobRec.SetSettings.SpindleLast := SpindleLast;
//          xJobRec.SetSettings.MachineGrp := x;
//          xJobRec.SetSettings. Color := xColor;
////          StrPCopy(@xJobRec.SetSettings.YMSetName, '');  //Nue:20.9.01
////          FillChar(xJobRec.SetSettings.YMSetName, sizeof(xJobRec.SetSettings.YMSetName),0);  //Nue:20.9.01
//          xJobRec.SetSettings.YMSetName := xYMSetName; //Nue:2.10.01
//          xJobRec.SetSettings.YMSetChanged := True; //Nue:20.9.01
//          //Start neue ProdGrp on MM und download neue ProdID auf ZE (mit SetSettings(assignment=None))
//codesite.SendFmtMsg('MaEqualizeJob.AnalyseJobData: atMMStart: Ma: %d FirstSpd: %d LastSpd: %d YMSetName:%s',
//[xMachID,SpindleFirst,SpindleLast,strPas(@xJobRec.SetSettings.YMSetName)]);
//          xBaseJob1 := TSetSettingsJob.Create(mJobList, @xJobRec, cNrOfTrials, cAssigProdGrpIDOnly,
////          xBaseJob1 := TSetSettingsJob.Create(mJobList, xJobRec, cNrOfTrials, cAssigProdGrpIDOnly,
//                                    smZE, psInconsistent, YMSetID, Query, Database, fParams,
//                                    xOldIntID, xOldFragID, ttMsgController, xBaseJob);
//
//          // add (queue) SetSettings to job list.
//          xBaseJob.AddSelfToJobList(jsValid,SynchronizeJob());
//
//          //Send new job msg
//          if not xBaseJob.WriteBufferTo(ttJobController,jtNewJobEv) then begin
//          end;
//        end;

        atMMZEEqual: begin  //ok 16.11.99
          // ProdGrp equal on MM and on ZE
          // Because the settings should be the same, no SetSettingsJob is really required
          if fCallReason = grGetNodeList then begin  //indicates the startup of MM, the UnDo-Jobs should only be done then Nue:6.6.00
            if MMProdId <> 0 then begin
codesite.SendFmtMsg('MaEqualizeJob.AnalyseJobData: MMReload: Ma: %d FirstSpd: %d LastSpd: %d',
                    [xMachID,SpindleFirst,SpindleLast]);

              try
                Query.Close;
                Query.SQL.Text := cQrySelFragAndIntID;
                Query.Params.ParamByName('c_machine_id' ).AsInteger    := xMachID;
                Query.Params.ParamByName('c_spindle_first' ).AsInteger := SpindleFirst;
                Query.Params.ParamByName('c_spindle_last' ).AsInteger  := SpindleLast;
                Query.Open;
              except
                on e:Exception do begin
                  SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData(cQrySelFragAndIntID) atMMZEEqual failed.' + e.message );
                  raise EMMException.Create ( fError.Msg );
                end;
              end;

              if Query.FindFirst then begin
                //Undo Job for this ProdID has to be started
                xJobRec.JobTyp := jtUnDoGetZESpdData;
                xJobRec.UnDoGetZESpdData.MachineID    := xMachID;
                xJobRec.UnDoGetZESpdData.SpindleFirst := SpindleFirst;
                xJobRec.UnDoGetZESpdData.SpindleLast  := SpindleLast;
                xJobRec.UnDoGetZESpdData.ProdID       := MMProdId;
                xJobRec.UnDoGetZESpdData.IntID        := Query.FieldByName('c_int_id_ok').AsInteger;
                xJobRec.UnDoGetZESpdData.FragID       := Query.FieldByName('c_fragshift_id_ok').AsInteger;

                xBaseJob := TUnDoGetZESpdDataJob.Create(mJobList, @xJobRec, cNrOfTrials, Query, Database, fParams,
                                                        jtDataAquEv, 0{Dummy});
//Nue:10.4.08
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: atMMStart: MachID=%d, First=%d Last=%d, After TUnDoGetZESpdDataJob.Create and before xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob)',
     [xMachID,SpindleFirst, SpindleLast]);

                // add UnDoGetZESpdDataJob to job list.
                xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob);

                //Send new job msg
                xBaseJob.WriteBufferTo(ttJobController, jtNewJobEv);
              end;
            end;
          end;
        end;

        atMMStopReloadZE: begin   //ok 21.6.99 (Achtung ueberschreibt Originaljob!)
        // Stop Grp on MM; restart new one (inconsistent) with the ZE settings on MM;
        // UnDo-Job check required (handled within ProcStartGrpEv)
        //  reload MM settings to ZE
          //Undo-Job
//          xJobRec := JobRec^;
          y := sizeof(xJobRec);
          if JobRec^.JobLen < y then
            y := JobRec^.JobLen;
          System.Move(JobRec^, xJobRec, y);

//NUETMP          xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpInfo.c_machine_id := xMachID;
//NUETMP          xJobRec.MaEqualize.Assigns.Groups[x].ProdGrpParam.ProdGrpYMPara.group      := x;
          //Neue ProdGrpID wird in InsertDummyProdGrp bereits in xSettingsRec abgef�llt (nue)
          InsertDummyProdGrp(Query, xMachID, SpindleFirst, SpindleLast, TYarnUnit(fParams.Value[cYarnCntUnit]),
                             0{No OldProdID},smZE, psInconsistent, xSettingsRec,
                             xJobRec.MaEqualize.Assigns.Groups[x].Color, xYMSetName, fError);
            // DONE wss: Parameter umkopieren von ProdGrpParam nach xSettingsRec.- Passiert in InsertDummyProdGrp
codesite.SendFmtMsg('MaEqualizeJob.AnalyseJobData: atMMStopReloadZE: Ma: %d FirstSpd: %d LastSpd: %d xYM_Set_Name:%s',
                    [xMachID,SpindleFirst,SpindleLast,strPas(@xYMSetName)]);
          ClearZEandSetSettings(SpindleFirst, SpindleLast, x, xJobRec,
              xMachID, xSettingsRec, smZE, psInconsistent,
              xJobRec.MaEqualize.Assigns.Groups[x].Color, cDummyYMSetID, cAssignProdGrpIDOnly);
        end;

        atZEInitialReset      : begin //ok 16.11.99
        //Load ProdGrp from MM, no UnDo-Job check required
codesite.SendFmtMsg('MaEqualizeJob.AnalyseJobData: ZEInitialReset: Ma: %d, MachGrp: %d, FirstSpd: %d, LastSpd: %d',
                    [xMachID,x,SpindleFirst,SpindleLast]);
          try
            GetProdGrpData(Query, MMProdID, xMachID, xSettingsRec, fError);

            ClearZEAndSetSettings(SpindleFirst, SpindleLast, x, xJobRec,
                xMachID, xSettingsRec, smUndefined{that no DB update happens in SetSettings.AnalyseJob},
                psOrdinaryAquis, ColorId, YMSetId, cAssignGroup);
          except
            on e:Exception do begin
              SetError(ERROR_INVALID_FUNCTION , etDBError, 'TMaEqualizeJob.AnalyseJobData atZEInitialReset failed.' + e.message );
              raise EMMException.Create ( fError.Msg );
            end;
          end;
        end;

      else // atDone
      end;
    end; // with
  end; // for x

  //Will only start the SynchronizeJob if it hasn't done yet, to reset machine_state from msInSynchronisation
  SynchronizeJob(jsValid);
  Result := True;

//Nue:10.4.08
codesite.SendFmtMsg('TMaEqualizeJob.AnalyseJobData: Root: MachID=%d,  After TUnDoGetZESpdDataJob.Create and before xBaseJob.AddSelfToJobList(jsValid, SynchronizeJob)',
     [xMachID]);

end;

//-----------------------------------------------------------------------------

function TMaEqualizeJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
begin
EnterMethod(Format('TMaEqualizeJob.EmergencyJM: MachID=%d', [JobRec^.MaEqualize.MachineID]));
  Result := True;
  case aJobError of
    jtJobFailed,
    jtMsgNotComplete: begin
      // Set Machine on DB to Offline
      Query.Close;
      try
        codesite.SendFmtMsg('TMaEqualizeJob.EmergencyJM: Before Start transaction',[]);
        Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
        codesite.SendFmtMsg('TMaEqualizeJob.EmergencyJM: After Start transaction',[]);
        Query.SQL.Text := cQrySetMachAndNodeState;
        Query.Params.ParamByName('c_machine_state' ).AsInteger := ORD(msOffline); //3=Offline
        Query.Params.ParamByName('c_machine_id' ).AsInteger    := JobRec^.MaEqualize.MachineID;
        Query.Params.ParamByName('c_node_stat' ).AsInteger     := ORD(nsOffline); //0=Offline
        codesite.SendFmtMsg('TMaEqualizeJob.EmergencyJM: MachID=%d, JobID=%d, Query.SQL.Text=%s',
                            [JobRec^.MaEqualize.MachineID, JobRec^.JobID, Query.SQL.Text]);
        Query.ExecSQL;
        Database.Commit;
      except
        on e:Exception do begin
          codesite.SendFmtMsg('TMaEqualizeJob.EmergencyJM: Exception part',[]);
          SetError(ERROR_INVALID_FUNCTION, etDBError, 'TMaEqualizeJob.EmergencyJM failed: ' + e.message);
          Database.Rollback;
          raise EMMException.Create(fError.Msg);
        end;
      end;
    end;
  end;
  if aLogEnabled then
    WriteLog(etWarning, Format('EmergencyJM.MaEqualize: MachID:%d set to OFFLINE', [JobRec^.MaEqualize.MachineID]));
  inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TInitializedJob }

constructor TInitializedJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtInitialized, aJobRec, aMaxTrials, aQuery, aDatabase, ttJobManager, aParentJob, cNoMachineID);
end;
//-----------------------------------------------------------------------------
function TInitializedJob.AnalyseJobData: Boolean;
begin
EnterMethod('TInitializedJob.AnalyseJobData');
  Result := True;
  //Set all from Initialized-Job dependent jobs to valid
  mJobList.SetJobGroupValid(JobID);
  //Deletes Job in JobController and starts new one
  WriteBufferTo(ttJobController, jtNewJobEv);
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TSendMsgToApplJob }

constructor TSendMsgToApplJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                     aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtSendMsgToAppl, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager, aParentJob, aJobRec^.SendMsgToAppl.ResponseMsg.MachineID);
end;
//-----------------------------------------------------------------------------
function TSendMsgToApplJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  {wssResult := }inherited DispatchContinuousJob(aJobTyp);
  Result := dcFalse;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TSetSettingsJob }

constructor TSetSettingsJob.Create(aJobList: TJobList; aJobRec: PJobRec;
    aMaxTrials: Byte; aAssignment: Byte; aStartMode: TProdGrpStartMode;
    aProdGrpState: TProdGrpState; aYMSetID: Integer; aQuery: TNativeAdoQuery;
    aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aOldIntID: Byte;
    aOldFragID: Longint; aFirstReceiver: TThreadTyp=ttMsgController;
    aParentJob: TBaseJob=Nil;  aComputerName: TString30=''; aPort :
    TString30=''; aProdIDOrgRange:Longint=0);
var
  xXMLSettingsHandler: TXMLSettingsAccess;
  xXMLData, xYMSetName : String;
  xSize: DWord;
  xTmpJob: PJobRec;
  xModel :TXMLSettingsModel;
begin
  mAssignment := aAssignment; //cAssigProdGrpIDOnly means, that settings on ZE shouldn't be overwritten; just load ProdID
//  mColor := aColor;   Nue 25.4.2000
  mColor          := aJobRec.SetSettings.SettingsRec.Color;
  mYMSetName      := aJobRec.SetSettings.SettingsRec.YMSetName;  //Nue:12.9.01
  mYMSetChanged   := aJobRec.SetSettings.SettingsRec.YMSetChanged;  //Nue:02.10.01
  fStartMode      := aStartMode;
  mProdGrpState   := aProdGrpState;
  fFirstReceiver  := aFirstReceiver;
  fOldIntID       := aOldIntID;
  fOldFragID      := aOldFragID;
  fProdIDOrgRange := aProdIDOrgRange;
  fParams         := aParams;
{ DONE -owss :
XML: Hier sollten die XML Settings von der DB gelesen werden, dynamisch
den Jobbuffer bei Bedarf anpassen (ReallocMem) und Prodgruppeninfos hinzuf�gen.
Offen: eventuell muss hier mit einem neuen, eigenen PJobRec gearbeitet werden,
da der Parameter aJobRec nur den Pointer darstellt und die original JobSize unbekannt ist.
Weiter unten im inherited Aufruf wird ja sowieso der komplette Job geklont, womit wir wieder
auf dem richtigen Weg w�ren.
Also: Vermutlich erst mal die XML Daten zusammensuchen (Settings, ProdGrpInfo, etc), dann
einen dynamischen tempor�ren JobRec erstellen und die Daten einf�llen, inherited
aufrufen und den tempor�ren JobRec wieder freigeben. }

//DONE wss: XML Settings laden und in Job abf�llen
  xTmpJob             := Nil;
  aJobRec^.JobLen := GetJobDataSize(aJobRec);
 codeSite.SendFmtMsg('TSetSettingsJob.Create JobLen:%d',[aJobRec^.JobLen]);

//Neu Nue
  if fStartMode = smMM then begin
    Codesite.SendFmtMsgEx(csmNote, 'CopyJob: JobTyp: %s, JobLen = %d', [GetJobName(aJobRec^.JobTyp), aJobRec^.JobLen]);
    CodeSite.SendString('XMLData', FormatXML(StrPas(aJobRec^.SetSettings.SettingsRec.XMLData)));
    CodeSite.SendInteger('GetJobDataSize', GetJobDataSize(aJobRec));

      aJobRec^.SetSettings.SettingsRec.AssignMode := aAssignment;
//        System.Move(aJobRec^.SetSettings.SettingsRec.XMLData, XMLData, (GetJobHeaderSize(jtSetSettings) + StrLen(aJobRec^.SetSettings.SettingsRec.XMLData)));
        //Be sure that the field aJobRec.DataArray is filled with all necessary YMSettings-data
      inherited create(aJobList, jtSetSettings, aJobRec, aMaxTrials, aQuery, aDatabase, aFirstReceiver,
                       aParentJob, aJobRec^.SetSettings.MachineID, False, aComputerName, aPort);
  end
  else begin  // fStartMode <> smMM
    // Kaltstart oder von ZE gestartet oder automatischer Neustart da Bereichs�berschneidung einer anderen Partie
    xXMLSettingsHandler := TXMLSettingsAccess.Create(aQuery);
    try
      if xXMLSettingsHandler.ReadSetting(aYMSetID, itXMLSetID, xXMLData, xYMSetName) then begin
        //Settings-Model erstellen um die ausgelesenen Settings mit dem Bauzustand der Maschine zu verschmelzen
        CodeSite.SendString('XMLData vor Merge', FormatXML(xXMLData));
        xModel := TXMLSettingsModel.Create(nil);
        try
          xModel.xmlAsString := xXMLData;
          xModel.MaConfigReader.LoadMachConfigFromDB(aJobRec.SetSettings.MachineID);
          //Ab XML: Hier findet die Verschmelzung zwischen den Settings und dem Bauzustand statt
          with TXMLSettingsMerger.Create do
          try
            xXMLData := Merge(xModel, aJobRec.SetSettings.SettingsRec.SpindleFirst, aJobRec.SetSettings.SettingsRec.SpindleLast);
            CodeSite.SendString('XMLData nach Merge', FormatXML(xXMLData));
          finally
            Free;
          end;// with TStringList.Create do try
        finally
          xModel.Free;
        end;// try

        // AAAlso: hier wird f�r die bestehende Settings (Partie wird automatisch neu gestartet) der ben�tigte Speicher geholt
        // Anschliessend wird aber der originale Job von der aJobRec hineinkopiert, damit XMLSettingsRec �bernommen wird
        // -> Wenn aJobRec.JobLen > (GetJobHeaderSize(jtSetSettings) + xSize) dann hat es zuwenig Platz im xTmpJob
        // => Eigentlich ist es nur n�tig den JobHeader + XMLSettingsRec zu kopieren...

        // Speicher allozieren und die alten Jobdaten hineinkopieren
        xSize           := Length(xXMLData);
        xTmpJob         := AllocMem(GetJobHeaderSize(jtSetSettings) + xSize);
//wss        CopyJob(aJobRec, xTmpJob);
//        with xTmpJob^ do begin
//          JobID := aJobRec^.JobID;
//          NetTyp := aJobRec^.NetTyp;
//          SetSettings := aJobRec^.SetSettings;
//          SetSettings.SettingsRec.XMLData[0] := #0;
//        end;

{ DONE 1 -oNue/wss -cUrgent : F�r Dani: Hier geht was mit dem Umkopieren schief! Wenn alter Job gr�sser als Neuer Job }
        with xTmpJob^, SetSettings, SettingsRec do begin
          JobID       := aJobRec^.JobID;
          NetTyp      := aJobRec^.NetTyp;
          JobTyp      := aJobRec^.JobTyp;
          SetSettings := aJobRec^.SetSettings;
          SetSettings.SettingsRec.XMLData[0] := #0;

          System.Move(PChar(xXMLData)^, XMLData, xSize);
          xTmpJob^.JobLen := GetJobDataSize(xTmpJob);

          AssignMode := aAssignment;

          // Weg vermutlich f�r WSC (und LX?)
          if ProdGrpID <> 0 then    //Nue 3.5.2000, because of ProdGroups in state "Definiert" on AC338
            // - Fuegt Produktionsgruppen-Abhaengige Parameter in DataArray ein.
            xXMLSettingsHandler.FillProdGrpValues(@SettingsRec, ProdGrpID) //Nue  ????
          //Weg vermutlich f�r Texnet
          else if (ProdGrpID = 0) AND (aAssignment = cAssignProdGrpIDOnly) then //Nue 9.5.2000, because of setting ProdID=0 after Stop on Texnet
            ProdGrpID := fProdIDOrgRange;
        end; // with xTmpJob^

        inherited create(aJobList, jtSetSettings, xTmpJob, aMaxTrials, aQuery, aDatabase, aFirstReceiver,
                         aParentJob, xTmpJob^.SetSettings.MachineID, False, aComputerName, aPort);
      end; // if ReadSettings
    finally
      FreeMem(xTmpJob);
      xXMLSettingsHandler.Free;
    end;
  end; //if fStartMode = smMM
end;

//constructor TSetSettingsJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aAssignment: Byte;
//                                   aStartMode: TProdGrpStartMode; aProdGrpState: TProdGrpState; aYMSetID: Integer;
//                                   aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aOldIntID: Byte; aOldFragID: Longint;
//                                   aFirstReceiver: TThreadTyp; aParentJob: TBaseJob;
//                                   aComputerName: TString30; aPort : TString30; aProdIDOrgRange:Longint);
//var
//  xProdGrpYMParaRec: TProdGrpYMParaRec;
//begin
//  mAssignment := aAssignment; //cAssigProdGrpIDOnly means, that settings on ZE shouldn't be overwritten; just load ProdID
////  mColor := aColor;   Nue 25.4.2000
//  mColor          := aJobRec.SetSettings.Color;
//  mYMSetName    := aJobRec.SetSettings.YMSetName;  //Nue:12.9.01
//  mYMSetChanged := aJobRec.SetSettings.YMSetChanged;  //Nue:02.10.01
//  fStartMode      := aStartMode;
//  fProdGrpState   := aProdGrpState;
//  fFirstReceiver  := aFirstReceiver;
//  fOldIntID       := aOldIntID;
//  fOldFragID      := aOldFragID;
//  fProdIDOrgRange := aProdIDOrgRange;
//  fParams         := aParams;
//  mYMParaDBAccess := TSettingsDBAssistant.Create(aQuery);
//  with aJobRec.SetSettings do begin
//    MachineID := ProdGrpInfo.c_machine_id;
//    if (fStartMode<>smMM) then
//      mYMParaDBAccess.Get(aYMSetID, DataArray, LengthData);
//
//    if (ProdGrpInfo.c_prod_id<>0) then begin   //Nue 3.5.2000, because of ProdGroups in state "Definiert" on AC338
//      // - Fuegt Produktionsgruppen-Abhaengige Parameter in DataArray ein.
//      mYMParaDBAccess.AddProdGrpYMPara(DataArray, mYMParaDBAccess.GetProdGrpYMPara(ProdGrpInfo.c_prod_id), aAssignment);
//    end
//    else if (ProdGrpInfo.c_prod_id=0) AND (aAssignment=cAssigProdGrpIDOnly) then begin //Nue 9.5.2000, because of setting ProdID=0 after Stop on Texnet
//      with xProdGrpYMParaRec do begin
//        group      := MachineGrp; // Local Group (1..6: AC338 Informator, 1..12 ZE)
//        prodGrpID  := ProdGrpInfo.c_prod_id; // MillMaster's group ID, 0=not defined
//        spdl.start := SpindleFirst; //Spindle first
//        spdl.stop  := SpindleLast; //Spindle first
//       end;
//      mYMParaDBAccess.AddProdGrpYMPara(DataArray, xProdGrpYMParaRec, aAssignment); // - Fuegt Produktionsgruppen-Abhaengige Parameter in DataArray ein.
//    end;
//
//    mYMParaDBAccess.AddMachineYMConfig(DataArray, mYMParaDBAccess.GetMachineYMConfig(ProdGrpInfo.c_machine_id){TMachineYMParaRec});
//
////    //Nue 27.06.00
////    if (fStartMode=smZE) then begin
////      //This 0-Values will be checked in WSC-Handler. If 0-Values the field "Partienummer"
////      // will not be overwritten by this dataset. (Otherwise MM-1-1 will be displayed on WSC)
////      ProdGrpInfo.c_order_position_id := 0;
////      ProdGrpInfo.c_order_id := 0;
////    end;
//
//  end;
//
//  //Be sure that the field aJobRec.DataArray is filled with all necessary YMSettings-data
//  inherited create(aJobList, jtSetSettings, aJobRec, aMaxTrials, aQuery, aDatabase, aFirstReceiver,
//                   aParentJob, False, aComputerName, aPort);
//  mYMParaDBAccess.Destroy;
//end;
//-----------------------------------------------------------------------------

function TSetSettingsJob.AnalyseJobData: Boolean;
var
//  xYMParaDBAccess: TSettingsDBAssistant;
  xXMLSettingsHandler: TXMLSettingsAccess;
  xYMSetId : Integer;
//Nicht gebraucht Nue 21.06.01  xProdGrpYMPara : TProdGrpYMParaRec;
//  xMachState,
//  xClearerTyp,
  xDataCollection: Integer;
  xNetTyp: TNetTyp;
//wss  xForcedStop : Boolean;

begin
 codesite.SendFmtMsg('TSetSettingsJob.AnalyseJobData:Eintritt JobRec^.SetSettings.SettingsRec.Group=%d', [JobRec^.SetSettings.SettingsRec.Group]);
 Result := True; //wss: Result wird gar nirgends gesetzt!! -> default = True
  try
    Query.SQL.Text := cQrySelMachine;
    Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.MachineID;
//wss    Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
    Query.Open;
// TODO: XML: bem Download zu AC338 hier wird z.T. eine Exception ausgel�st, warum?
//NUE1    xMachineClass   := Query.FieldByName('c_AWE_mach_type').AsInteger;
    xDataCollection := Query.FieldByName('c_data_collection').AsInteger;
    xNetTyp         := TNetTyp(Query.FieldByName ('c_net_id' ).AsInteger);
    Query.Close;
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSetSettingsJob.AnalyseJobData cQrySelMachineType failed.' + e.message );
      raise EMMException.Create ( fError.Msg );
    end;
  end;

  if ((xNetTyp = ntWSC) or (xNetTyp = ntLX)) AND (fStartMode = smUndefined) then begin
    //for synchronisation after coldstart on WSC (LX) (only prodgrps with state "InProduction" on DB)
    //Stop ProdGrp in t_prodgroup and delete entry in v_prodgroup_state
    try
      Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
      Query.Close;
      Query.SQL.Text := cQryStopProdGrpForced;
      Query.Params.ParamByName('c_prod_state' ).AsInteger := ORD(psStopped); //4=Stopped
      Query.Params.ParamByName('c_prod_end').AsDateTime   := Now;
      Query.Params.ParamByName('c_prod_id' ).AsInteger    := JobRec^.SetSettings.SettingsRec.ProdGrpID;
      Query.ExecSQL;
      Database.Commit;
    except
      on e:Exception do begin
        SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSetSettingsJob.AnalyseJobData cQryStopProdGrpForced(1) failed.' + e.message );
        Database.Rollback;
        raise EMMException.Create ( fError.Msg );
      end;
    end;
    EXIT;
  end;

  if JobRec^.SetSettings.SettingsRec.ProdGrpID = 0 then begin
      //This will happen, when a Gruppenspeicher will be setted from MM on the AC338 wich is in state DEFINIERT
      // Nothing has to be done, because there is no existing ProdID on the DB yet.
    EXIT;
  end;


  if fStartMode = smMMStop then begin
    // Delete entry in v_prodgroup_state and update entry in t_prodgroup
    try
      Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
      Query.Close;
      Query.SQL.Text := cQryStopProdGrpForced;
      Query.Params.ParamByName('c_prod_state' ).AsInteger := ORD(psStopped); //4=Stopped
      Query.Params.ParamByName('c_prod_end').AsDateTime   := Now;
      Query.Params.ParamByName('c_prod_id' ).AsInteger    := fProdIDOrgRange;
      //wss: �hm, warum 2x zuweisen?
      //Query.Params.ParamByName('c_prod_id' ).AsInteger    := fProdIDOrgRange;
      Query.ExecSQL;
      Database.Commit;
    except
      on e:Exception do begin
        SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSetSettingsJob.AnalyseJobData cQryStopProdGrpForced(2) failed.' + e.message );
        Database.Rollback;
        raise EMMException.Create ( fError.Msg );
      end;
    end;
    EXIT;
  end;


  if ((mAssignment=cAssignGroup) or //Settings have been loaded to ZE; definitive settings in message
      (mAssignment=cAssignProdGrpIDOnly))  AND //Only ProdGrpID has been loaded to ZE; definitive settings in message
     (fStartMode<>smUndefined) //smUndefined means: SetSettings forced by MaEqualize.ZeInitialReset; mustn't update DB.
  then begin
  //Set v_prodgroup_state entry to the new values
    if fStartMode=smMM then begin
      JobRec^.SetSettings.SettingsRec.Color := mColor;  //Uebernahme der Color von MM, weil dieses Feld auf einigen
//      JobRec^.SetSettings.Color := mColor;  //Uebernahme der Color von MM, weil dieses Feld auf einigen
                                            // MaschinenHandler(z.B.Texnet) nicht geschlauft wird!
    end;

    JobRec^.SetSettings.SettingsRec.YMSetName    := mYMSetName;  //Nue:2.10.01
    JobRec^.SetSettings.SettingsRec.YMSetChanged := mYMSetChanged;  //Nue:2.10.01

    try
      Database.StartTransaction;      //??Nue Abhandeln wenn bereits eine transaction aktiv
      with JobRec^.SetSettings, SettingsRec do begin
        Query.Close;
        if fFirstReceiver = ttJobManager then begin  // Means machine=Offline
          //This means a new DummyProdGrp was already inserted with all data from the previous stopped (old) ProdGrp
          // so the old YMSetId has to be saved, before next update
          Query.SQL.Text := cQrySelProdGrp;
          Query.Params.ParamByName('c_prod_id' ).AsInteger := ProdGrpID;
          Query.Open;
          if not Query.EOF then  // ADO Conform
            xYMSetId := Query.FieldByName ('c_YM_set_id' ).AsInteger
          else
            xYMSetId := 1; //Dummy
          Query.Close;
        end
        else begin
          //This means Data comes from the machine, so YMParaDBAccess methodes can be called
//DONE wss: XML Settings speichern
//          xYMParaDBAccess := TSettingsDBAssistant.Create(Query);  // Create of class for settings acces
//          xYMSetId := xYMParaDBAccess.SaveAll(DataArray, ProdGrpInfo.c_prod_id, ProdGrpInfo.c_machine_id,
//                          SpindleFirst, SpindleLast, YMSetName, YMSetChanged, False{MachineUpdate});
          xXMLSettingsHandler := TXMLSettingsAccess.Create(Query);
          try
  codesite.SendFmtMsg('TSetSettingsJob.AnalyseJobData: MachID=%d, ProdID=%d, YMSetName=%s, YMSetChanged:%d  SpeedRamp:%d',
                      [MachineID, ProdGrpID, StrPas(@YMSetName),Integer(YMSetChanged), Integer(SpeedRamp)]);
            xYMSetID := xXMLSettingsHandler.SaveSettingAndProdGrpValues(@SettingsRec);
  codesite.SendMsg('TSetSettingsJob.AnalyseJobData:Nach SaveAll');
          finally
            xXMLSettingsHandler.Free;
          end;
        end;

//wss: c_clear_type wurde bis jetzt nie richtig behandelt. In der Tabelle t_spindle sind alle Eintr�ge = 0. Daher wird
//     nun vorl�ufig fix 0 abgef�llt und das auslesen aus der t_spindle Tabelle entf�llt (wird in MaConfig auch nur noch
//     stiefm�tterlich behandelt)
//        Query.Close;
//        Query.SQL.Text := cQrySelSpindle;
//        Query.Params.ParamByName('c_machine_id' ).AsInteger := MachineID;
//        Query.Params.ParamByName('c_spindle_id' ).AsInteger := SpindleFirst;
////Query.SendDebugQuery;
//codesite.SendFmtMsg('TSetSettingsJob.AnalyseJobData:Vor Open cQrySelSpindle Machine: %d, Spindle: %d', [MachineID, SpindleFirst]);
//        Query.Open;
//        if Query.FindFirst then  // ADO Conform
////wss        if not Query.EOF then  // ADO Conform
//          xClearerTyp := Query.FieldByName ('c_clear_type' ).AsInteger
//        else
//          xClearerTyp:= 1; //Dummy: Kann im DemoMode der ZE passieren Nue:11.12.02

        Query.Close;
        Query.SQL.Text := cQryUpdDummyProdGrp;
        Query.Params.ParamByName('c_prod_state' ).AsInteger            := ORD(mProdGrpState); //1=Ordinary Aquis.3=Inconsistent
        Query.Params.ParamByName('c_YM_set_id' ).AsInteger             := xYMSetId;
        Query.Params.ParamByName('c_start_mode' ).AsInteger            := ORD(fStartMode);
        Query.Params.ParamByName('c_prod_id' ).AsInteger               := ProdGrpID;
        Query.Params.ParamByName('c_spindle_first' ).AsInteger         := SpindleFirst;
        Query.Params.ParamByName('c_spindle_last' ).AsInteger          := SpindleLast;
        Query.Params.ParamByName('c_machineGroup' ).AsInteger          := Group;
        Query.Params.ParamByName('c_old_prod_id' ).AsInteger           := fProdIDOrgRange; //For reset old prodgrp at rangechange on GUI (defaultvalue =0)

        Query.Params.ParamByName('c_machine_id' ).AsInteger            := MachineID;
        Query.Params.ParamByName('c_net_id' ).AsInteger                := ord(xNetTyp);
//wss        Query.Params.ParamByName('c_clear_type' ).AsInteger            := xClearerTyp; // Wurde nie richtig behandelt -> fix 0 in Query
//NUE1        Query.Params.ParamByName('c_AWE_mach_type' ).AsInteger         := xMachineClass;
        Query.Params.ParamByName('c_color' ).AsInteger                 := Color;
        Query.Params.ParamByName('c_data_collection' ).AsInteger       := xDataCollection;
{ DONE 3 -oNUE -cSystem : MachineState }
//Wird nicht mehr gebraucht, da direkt �ber t_machine geupdatet wird 18.11.02
//        Query.Params.ParamByName('c_machine_state' ).AsInteger       := xMachState;
        Query.Params.ParamByName('c_fragshift_id_ok' ).AsInteger       := fOldFragID;
        Query.Params.ParamByName('c_int_id_ok' ).AsInteger             := fOldIntID;
        Query.Params.ParamByName('c_old_fragshift_id_ok' ).AsInteger   := fOldFragID;
        Query.Params.ParamByName('c_old_int_id_ok' ).AsInteger         := fOldIntID;
        Query.Params.ParamByName('c_shift_fragshift_id_ok' ).AsInteger := fOldFragID;
        Query.Params.ParamByName('c_shift_int_id_ok' ).AsInteger       := fOldIntID;
codesite.SendFmtMsg('TSetSettingsJob.AnalyseJobData:Vor Exec cQryUpdDummyProdGrp YMSetId: %d',[xYMSetId]);
//Query.SendDebugQuery;
        Query.ExecSQL;
        codesite.SendFmtMsg('TSetSettingsJob.AnalyseJobData:Nach Exec cQryUpdDummyProdGrp YMSetId: %d',[xYMSetId]);
      end; //with
      Database.Commit;
    except
      on e:Exception do begin
        codesite.SendMsg('EXEPCTION:TSetSettingsJob.AnalyseJobData:Nach Exec cQryUpdDummyProdGrp');
        SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSetSettingsJob.AnalyseJobData failed.' + e.message );
        Database.Rollback;
        raise EMMException.Create ( 'TSetSettingsJob.AnalyseJobData failed.' + e.message );
      end;
    end;
  end;

  //Set fields in v_prodgroup_state not containing in the "Settings-Bag"
  if JobRec^.SetSettings.SettingsRec.ProdGrpID <> 0 then
  try
    Query.Close;
    Query.SQL.Text := cQryUpdProdGrpState;
    Query.Params.ParamByName('c_color' ).AsInteger   := mColor;
    Query.Params.ParamByName('c_prod_id' ).AsInteger := JobRec^.SetSettings.SettingsRec.ProdGrpID;
    Query.ExecSQL;
  except
    on e:Exception do begin
      codesite.SendMsg('EXEPCTION:TSetSettingsJob.AnalyseJobDataNach Exec cQryUpdProdGrpState');
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSetSettingsJob.AnalyseJobData cQryUpdProdGrpState failed.' + e.message );
      raise EMMException.Create ( 'TSetSettingsJob.AnalyseJobData cQryUpdProdGrpState failed.' + e.message  );
    end;
  end;

end;
//-----------------------------------------------------------------------------

function TSetSettingsJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
var
  xProdIdOld : Longint;
begin
  Result := True;

  case aJobError of
    jtMsgNotComplete: begin
      Query.Close;
      Query.SQL.Text := cQryGetProdID;
      Query.Params.ParamByName('c_machine_id').AsInteger    := JobRec^.SetSettings.MachineID;
      Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SettingsRec.SpindleFirst;
      Query.Params.ParamByName('c_spindle_last').AsInteger  := JobRec^.SetSettings.SettingsRec.SpindleLast;
      Query.Open;
      if Query.EOF then begin  // ADO Conform
        Query.Close;
        Query.SQL.Text := cQryGetProdID2;
        Query.Params.ParamByName('c_machine_id').AsInteger    := JobRec^.SetSettings.MachineID;
        Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SettingsRec.SpindleFirst;
        Query.Params.ParamByName('c_spindle_last').AsInteger  := JobRec^.SetSettings.SettingsRec.SpindleLast;
        Query.Open;
        if Query.EOF then begin  // ADO Conform
          Result := inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  //Calls free of this Job
          Exit;
        end
      end;
      xProdIdOld := Query.FieldByName('c_prod_id').AsInteger;

      case fStartMode of
        smMM, smZE, smRange: begin
            try
              StopOldProdGrp(Query, Database, xProdIdOld, fError);
            except
              on e:Exception do begin
                 fError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('%s.EmergencyJM %s',[mClassname, e.message]));
                 raise EMMException.Create(fError.Msg);
              end;
            end;
            if aLogEnabled then
              WriteLog(etWarning, Format('%s.EmergencyJM: Could not do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
                                         [mClassname,JobRec^.SetSettings.SettingsRec.ProdGrpID, xProdIdOld]));
          end;
        smMMStop, smZEStop: begin
            try
              StopOldProdGrp(Query, Database, xProdIdOld, fError); //Nue 9.5.00
  //            StopOldStartInconsProdGrp(Query, Database, xProdIdOld, JobRec^.SetSettings.ProdGrpInfo.c_prod_id,
  //                                      StrToInt(fParams.Value[cYarnCntUnit]), fError);
            except
              on e:Exception do begin
                 fError := SetError(ERROR_INVALID_FUNCTION, etDBError, Format('%s.EmergencyJM %s',[mClassname, e.message]));
                 raise EMMException.Create(fError.Msg);
              end;
            end;
            if aLogEnabled then
              WriteLog(etWarning, Format('%s.EmergencyJM: Could not do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
                                         [mClassname,JobRec^.SetSettings.SettingsRec.ProdGrpID, xProdIdOld]));
          end;
      else
      end; //case

      Result := inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  //Calls free of this Job
    end;
  end;
end;
//function TSetSettingsJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList: Boolean): Boolean;
//var
//  xProdIdOld : Longint;
//begin
//  Result := True;
//
//  case aJobError of
//    jtMsgNotComplete: begin
//      Query.Close;
//      Query.SQL.Text := cQryGetProdID;
//      Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//      Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//      Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//      Query.Open;
//      if Query.EOF then begin  // ADO Conform
//        Query.Close;
//        Query.SQL.Text := cQryGetProdID2;
//        Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//        Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//        Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//        Query.Open;
//        if Query.EOF then begin  // ADO Conform
//          Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//          Exit;
//        end
//      end;
//      xProdIdOld := Query.FieldByName ('c_prod_id' ).AsInteger;
//
//      case fStartMode of
//        smMM, smZE, smRange: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//        smMMStop, smZEStop: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError); //Nue 9.5.00
////            StopOldStartInconsProdGrp(Query, Database, xProdIdOld, JobRec^.SetSettings.ProdGrpInfo.c_prod_id,
////                                      StrToInt(fParams.Value[cYarnCntUnit]), fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//      end; //case
//
//      Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//    end;
//  end;
//end;
//-----------------------------------------------------------------------------

{ TStartGrpEvJob }

constructor TStartGrpEvJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                  aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtStartGrpEv, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager, aParentJob, aJobRec^.StartGrpEv.MachineID);
end;
//-----------------------------------------------------------------------------

function TStartGrpEvJob.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
  {wss Result := }inherited DispatchContinuousJob(aJobTyp);
  Result := dcFalse;
end;
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TSynchronizeJob }

constructor TSynchronizeJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                   aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtSynchronize, aJobRec, aMaxTrials, aQuery, aDatabase, ttJobManager, aParentJob, aJobRec^.Synchronize.MachineID);

//Nue:10.4.08
codesite.SendFmtMsg('TSynchronizeJob.Create: SynchronizeJob added',[]);

end;
//-----------------------------------------------------------------------------

function TSynchronizeJob.AnalyseJobData: Boolean;
begin
  WriteLog(etInformation, Format('TSynchronizeJob.AnalyseJobData: MachineID:%d ',
                                 [JobRec^.Synchronize.MachineID]), NIL, 0);

  //After synchronisation is done set machine_state
  Query.Close;
  try
    Query.SQL.Text := cQrySetMachState;
{ DONE 3 -oNUE -cSystem : MachineState }
    Query.Params.ParamByName('c_machine_state').AsInteger := ORD(msRunning);
    Query.Params.ParamByName('c_machine_id').AsInteger    := JobRec^.Synchronize.MachineID;
//    Query.Params.ParamByName('c_machine_state').AsInteger := ORD(msNoDataCollection);
    Query.ExecSQL;
    Result := True;
  except
    on e:Exception do begin
      SetError ( ERROR_INVALID_FUNCTION , etDBError, 'TSynchronizeJob.AnalyseJobData cQrySetMachState failed.' + e.message );
      raise EMMException.Create ( fError.Msg );
    end;
  end;

end;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

{ TUnDoGetZESpdDataJob }

constructor TUnDoGetZESpdDataJob.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery;
                                        aDatabase: TAdoDBAccessDB; aParams : TMMSettingsReader; aQueueType: TJobTyp; aNewDummyProdID: Longint;
                                        aComputerName: TString30; aPort : TString30);
begin
  inherited create(aJobList, jtUnDoGetZESpdData, aJobRec, aMaxTrials, aQuery, aDatabase, ttQueueManager,
                   NIL, aJobRec^.UnDoGetZESpdData.MachineID, False, aComputerName, aPort);
  QueueType      := aQueueType;
  fNewDummyProdID := aNewDummyProdID;
  fParams         := aParams;
end;
//-----------------------------------------------------------------------------

function TUnDoGetZESpdDataJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList, aLogEnabled: Boolean): Boolean;
begin
  Result := True;
  case aJobError of
    jtJobFailed: begin
      case QueueType of
        jtStartGrpEv, jtStartGrpEvFromMa: begin
          try
            StopOldProdGrp(Query, Database, JobRec^.UnDoGetZESpdData.ProdID, fError);
          except
            on e:Exception do begin
               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
               raise EMMException.Create ( fError.Msg );
            end;
          end;
          if aLogEnabled then
            WriteLog(etWarning, Format('%s.EmergencyJM: Could not do the UnDo job for ProdID %d correctly. Stopped it anyway.',
                                       [mClassname,JobRec^.UnDoGetZESpdData.ProdID]));
        end;
        jtStopGrpEv, jtStopGrpEvFromMa: begin
          try
            StopOldProdGrp(Query, Database, JobRec^.UnDoGetZESpdData.ProdID, fError); //Nue 9.5.00
//            StopOldStartInconsProdGrp(Query, Database, JobRec^.UnDoGetZESpdData.ProdID, fNewDummyProdID,
//                                      StrToInt(fParams.Value[cYarnCntUnit]), fError);
          except
            on e:Exception do begin
               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
               raise EMMException.Create ( fError.Msg );
            end;
          end;
          if aLogEnabled then
            WriteLog(etWarning, Format('%s.EmergencyJM: Could not do the UnDo job for ProdID %d correctly. Stopped it anyway.',
                                       [mClassname,JobRec^.UnDoGetZESpdData.ProdID]));
        end;
      end; //case

      Result := inherited EmergencyJM(aJobError, aDelAllDependentJobsInList, aLogEnabled);  //Calls free of this Job
    end;
  end;
end;

//function TSetSettingsJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList: Boolean): Boolean;
//var
//  xProdIdOld : Longint;
//begin
//  Result := True;
//
//  case aJobError of
//    jtMsgNotComplete: begin
//      Query.Close;
//      Query.SQL.Text := cQryGetProdID;
//      Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//      Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//      Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//      Query.Open;
//      if Query.EOF then begin  // ADO Conform
//        Query.Close;
//        Query.SQL.Text := cQryGetProdID2;
//        Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//        Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//        Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//        Query.Open;
//        if Query.EOF then begin  // ADO Conform
//          Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//          Exit;
//        end
//      end;
//      xProdIdOld := Query.FieldByName ('c_prod_id' ).AsInteger;
//
//      case fStartMode of
//        smMM, smZE, smRange: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//        smMMStop, smZEStop: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError); //Nue 9.5.00
////            StopOldStartInconsProdGrp(Query, Database, xProdIdOld, JobRec^.SetSettings.ProdGrpInfo.c_prod_id,
////                                      StrToInt(fParams.Value[cYarnCntUnit]), fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//      end; //case
//
//      Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//    end;
//  end;
//end;
//-----------------------------------------------------------------------------

constructor TSaveMaConfigToDB.Create(aJobList: TJobList; aJobRec: PJobRec; aMaxTrials: Byte; aQuery: TNativeAdoQuery; aDatabase: TAdoDBAccessDB; aParentJob: TBaseJob);
begin
  inherited create(aJobList, jtSaveMaConfigToDB, aJobRec, aMaxTrials, aQuery, aDatabase, ttMsgController,
                   aParentJob, aJobRec^.SaveMaConfig.MachineID, False, aJobRec^.SaveMaConfig.ComputerName, aJobRec^.SaveMaConfig.Port);
end;

//-----------------------------------------------------------------------------

function TSaveMaConfigToDB.DispatchContinuousJob(aJobTyp: TJobTyp): TDispContState;
begin
EnterMethod('TSaveMaConfigToDB.DispatchContinuousJob');
  Result := dcFalse;
  case aJobTyp of
    jtMsgComplete: begin
      // Im StorageHandler wird die XML Bauzustands Informationen auf die DB gespeichert
      if WriteJobBufferTo(ttQueueManager) then
        Result := inherited DispatchContinuousJob(aJobTyp);
    end;
    jtJobSuccessful: begin
      Result := inherited DispatchContinuousJob(aJobTyp);
    end;
  end;
end;

//function TSetSettingsJob.EmergencyJM(aJobError: TJobTyp; aDelAllDependentJobsInList: Boolean): Boolean;
//var
//  xProdIdOld : Longint;
//begin
//  Result := True;
//
//  case aJobError of
//    jtMsgNotComplete: begin
//      Query.Close;
//      Query.SQL.Text := cQryGetProdID;
//      Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//      Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//      Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//      Query.Open;
//      if Query.EOF then begin  // ADO Conform
//        Query.Close;
//        Query.SQL.Text := cQryGetProdID2;
//        Query.Params.ParamByName('c_machine_id').AsInteger := JobRec^.SetSettings.ProdGrpInfo.c_machine_id;
//        Query.Params.ParamByName('c_spindle_first').AsInteger := JobRec^.SetSettings.SpindleFirst;
//        Query.Params.ParamByName('c_spindle_last').AsInteger := JobRec^.SetSettings.SpindleLast;
//        Query.Open;
//        if Query.EOF then begin  // ADO Conform
//          Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//          Exit;
//        end
//      end;
//      xProdIdOld := Query.FieldByName ('c_prod_id' ).AsInteger;
//
//      case fStartMode of
//        smMM, smZE, smRange: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//        smMMStop, smZEStop: begin
//          try
//            StopOldProdGrp(Query, Database, xProdIdOld, fError); //Nue 9.5.00
////            StopOldStartInconsProdGrp(Query, Database, xProdIdOld, JobRec^.SetSettings.ProdGrpInfo.c_prod_id,
////                                      StrToInt(fParams.Value[cYarnCntUnit]), fError);
//          except
//            on e:Exception do begin
//               fError := SetError ( ERROR_INVALID_FUNCTION , etDBError,Format('%s.EmergencyJM ',[mClassname])+ e.message );
//               raise EMMException.Create ( fError.Msg );
//            end;
//          end;
//          WriteLog(etWarning, Format('%s.EmergencyJM: Could not  do the SetSetting job for ProdID %d correctly. Stopped ProdIdOld %d anyway.',
//            [mClassname,JobRec^.SetSettings.ProdGrpInfo.c_prod_id, xProdIdOld]));
//        end;
//      end; //case
//
//      Result := inherited EmergencyJM(aJobError);  //Calls free of this Job
//    end;
//  end;
//end;
//-----------------------------------------------------------------------------

end.

