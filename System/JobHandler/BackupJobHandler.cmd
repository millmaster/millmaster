@ECHO OFF
REM *******************************************************************
REM * Command file to automatically backup all project files.
REM *
REM * Author:	  Nue
REM * DevSystem:  Windows NT, Delphi 5.0
REM *------------------------------------------------------------------
REM * Changes:
REM * 05.10.1999 | Nue | Update to Delphi5
REM *******************************************************************

REM Initialize environment variables
  SET xBaseDir=\Delphi
  SET xWorkDir=\Delphi\Act\System\JobHandler

  SET xCopyDir=%xBaseDir%\SourceBackup\JobHandler

  cd %xCopyDir%
  if not exist %1 md %1


 copy %xWorkDir%\*.pas %xCopyDir%\%1\*.pas
 copy %xBaseDir%\act\baseunits\*.pas %xCopyDir%\%1\*.pas
 copy %xBaseDir%\act\baseunits\*.inc %xCopyDir%\%1\*.inc
 copy %xBaseDir%\act\global\*.pas %xCopyDir%\%1\*.pas
 copy %xBaseDir%\act\global\*.inc %xCopyDir%\%1\*.inc

