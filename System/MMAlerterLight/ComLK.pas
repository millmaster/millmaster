//******************************************************************************
// TComLK                
// (c) 1998 Lothar Kraner lk@email.ch
//
//******************************************************************************
unit ComLK;
                  
interface

uses
 Windows,Classes,ExtCtrls,SysUtils ,Forms,Dialogs,Messages, Graphics, Controls,stdctrls,
 Buttons,commdlg,winspool;

type
 TParity=(paNone,paOdd,paEven,paMark,paSpace);
 TStopBit=(sb1_0,sb1_5,sb2_0);
 TEinstControl=(ecOK,ecAktual,ecCancel,ecPort,ecProp);

 T_QCCom32 = class(TComponent)

 private
  FNT		:Boolean;			    // True wenn NT l�uft
  FFlag:LongInt;                                    // Handshakes
  hPort: LongInt;                                   // Handle des ge�ffneten Ports
  nPort: Integer;                                   // port #, 1-based
  lBaud: LongInt;                                   // baud rate
  lDataBits:Integer;                                // L�nge des Datenbits
  lStopBits:TStopBit;                               // Anzahl Stopbits
  lParity:TParity;                                  // Art der Parit�t
  sInTerminator: String;                            // Terminierung eines empfangen Strings
  boolExpired: Boolean;                             // True wenn ein Timeout eingetreten
  Timer: TTimer;                                    // Timeout Timer
  boolShowErrors: Boolean;                          // Wenn True dann erscheinen Fehlermeldungen
  boolInUse: Boolean;                               // True solange Port in gebrauch
  FBreakOff:boolean;                                // True wenn ein leseforgang abgebrochen werden soll
  FRTS: boolean;                                    // True wenn RTS gesetzt

  {Ereignisroutinen}
  pmOnTimeout: TNotifyEvent;
  pmOnParamChange: TNotifyEvent;
    FDTR: boolean;

  procedure SetTimeout(lTimeout: LongInt);
  procedure SetBaud(lBaudToSet: LongInt);
  procedure SetStopBits(lStopBitsToSet: TStopBit);
	procedure SetDataBits(lDataBitsToSet: Integer);
  procedure SetParity(lParityToSet: TParity);
  procedure SetPort(nPortToSet: Integer);
  procedure TimesUp(Sender: TObject);               // Timeout  Event ausf�hren
  function GetInCount: LongInt;                     // Gibt die Anzahl Zeichen zur�ck die in der Warteschlange sind
  function GetTimeout: LongInt;                     // Fragt die Timeoutzeit ab
  function IsOpen: Boolean;                         // True wenn der Port ge�ffnet ist
  procedure CommSetRTS(const Value: boolean);
  procedure CommSetDTR(const Value: boolean);       // Setzt oder l�scht die Leitung RTS

 protected
  procedure Timeout; dynamic;                       // Eventhandler f�r TimesUp
  procedure ChangeParameters;dynamic;               // Wird aufgerufen wenn die Schnittstellenparameter ge�ndert haben

 public { Public declarations }
	//Methodendefinitionen               .
  constructor Create(AOwner: TComponent); override;
  destructor Destroy; override;

	function Open: Boolean;                           // �ffnet einen Com-Port
	function Write(const sData: String): Boolean;     // Schreibt Daten auf einen ge�ffneten Port
	function Read: String;                            // Liest Daten von einem ge�ffneten Port
	function properties:Boolean;                      // Bringt den Eigenschaftsdialog des aktuellen Ports auf den Bildschirm
	procedure Flush;                                  // Leert den Empfangspuffer
	procedure Close;                                  // Schliesst den aktuellen Port
	procedure SetWriteTimeOut(toChar,toMax:Cardinal); // Setzt den WriteTimeout

  property RTS:boolean read FRTS write CommSetRTS;  // True wenn das RTS signal beim �ffnen gesetzt werden soll
  property DTR:boolean read FDTR write CommSetDTR;  // True wenn das DTR signal beim �ffnen gesetzt werden soll
  property BreakOff:Boolean read FBreakOff write FBreakOff; // auf True setzen wenn lesevorgang unterbrochen werden soll
	property Handshake:LongInt read FFlag write FFlag;                            // Art des Handshakes

 published { Published declarations }
// Eigenschaften
  property OnParamChange: TNotifyEvent read pmOnParamChange write pmOnParamChange;// Wird aufgerufen wenn die Schnittstellenparameter wechseln
  property ResponseTime: LongInt read GetTimeout write SetTimeout;              // Wartet solange auf Antwort (Millisekunden)
  property OnTimeout: TNotifyEvent read pmOnTimeout write pmOnTimeout;          // Timeout Event
	property TimedOut: Boolean read boolExpired;                                  // True wenn ein Timeout stattgefunden hat
	property Baud: LongInt read lBaud write SetBaud;                              // Baudrate
	property DataBits: LongInt read lDataBits write SetDataBits;                  // L�nge des Datenbits
	property StopBits: TStopBit read lStopBits write SetStopBits;                 // Anzahl der Stopbits
	property Parity: TParity read lParity write SetParity;                        // Art der Parit�t
	property Port: Integer read nPort write SetPort;                              // Nummer des Com-Port (Port 1 ist der erste Port)
	property InCount: LongInt read GetInCount;                                    // Anzahl der Zeichen im Empfangspuffer
	property EndOfResponse: String read sInTerminator write sInTerminator;        // Endzeichen eines Strings der Empfangen wird
	property ShowErrors: Boolean read boolShowErrors write boolShowErrors;        // Zeigt diverse Fehlermeldungen an wenn True
	property Opened: Boolean read IsOpen;                                         // True wenn Port ge�ffnet
	property InUse: Boolean read boolInUse;                                       // True wenn gerade gelesen oder geschrieben wird
	property handle:Integer read hPort;                                           // Handle des ge�ffneten Ports
	property Flag:Integer read FFLag write FFlag;
end;

type
 TEinstellungen = class(TForm)
	RadioGb: TRadioGroup;
	paButtons: TPanel;
	Panel1: TPanel;
	butOK: TBitBtn;
	butEigenschaften: TBitBtn;
  Panel2: TPanel;
	procedure butOKClick(Sender: TObject);
	procedure butEigenschaftenClick(Sender: TObject);
	procedure RadioGbClick(Sender: TObject);

 private
// merkt sich die alten Einstellungen
	nPortAlt: Integer;                                // port #, 1-based
	lBaudAlt: LongInt;                                // baud rate
	lDataBitsAlt:Integer;                             // L�nge des Datenbits
	lStopBitsAlt:TStopBit;                            // Anzahl Stopbits
	lParityAlt:TParity;                               // Art der Parit�t
	{ Private-Deklarationen }
 public
	{ Public-Deklarationen }
 end;

 TCharArray=array[0..1024] of Byte;

type
 TComLK = class(T_QCCom32)
 private
	Einstellungen: TEinstellungen;                    // Einstellungen Dialog
	FEigenschaftenCaption:TCaption;                   // Textstrings f�r die Beschriftung mit Fremdsprachen
	FOKCaption:TCaption;
	FAbbruchCaption:TCaption;
	FAktualisierenCaption:TCaption;
	FRadioGBCaption:TCaption;
	FFormCaption:TCaption;
    FLocked: Boolean;

	function GetCommStatus:String;                    // Holt die Porteinstellungen mit dem Port
	function GetPortString: string;
	{ Private-Deklarationen }
 protected
  FWithCaption:Boolean;

	function PortEnable:TStringList;virtual;          // Holt die Verf�gbaren Ports von windows
  function PortEnableNT: TStringList;               // Holt den Portstring (zB: '9600,8,N,1')
	{ Protected-Deklarationen }
 public
	constructor Create(AOwner: TComponent); override;
	destructor Destroy; override;
                                                    // Setzt f�r das angebene Control den Hilfecontext
	procedure Eigenschaften;virtual;                  // Bringt den Schnittstellnauswahl Dialog auf den Bildschirm
	procedure Delay(Millisekunden:Integer);           // Wartet die angegebene Anzahl Millisekunden
	procedure SetHelpContext(Control:TEinstControl;Index:Integer);
  function ReadBinary(var Count:integer; var aCharArrray: TCharArray): boolean;
  function WriteBinary(Data: Byte): Boolean;
	{ Public-Deklarationen }
	property CaptionProperties:TCaption read FEigenschaftenCaption write FEigenschaftenCaption;
	property CaptionOK:TCaption read FOKCaption write FOKCaption;
  property CaptionCancel:TCaption read FAbbruchCaption write FAbbruchCaption;
	property CaptionAktual:TCaption read FAktualisierenCaption write FAktualisierenCaption;
  property CaptionPort:TCaption read FRadioGBCaption write FRadioGBCaption;
	property CaptionForm:TCaption read FFormCaption write FFormCaption;
  property frmProp:TEinstellungen read Einstellungen write Einstellungen;
  property Locked:Boolean read FLocked write FLocked;                           // Kann von aussen gesetzt werden
 published
	property CommStatus:String read GetCommStatus;
	property PortString:string read GetPortString;
	{ Published-Deklarationen }
 end;

procedure Register;


implementation

{$R *.DFM}

//******************************************************************************
//******************************************************************************
//
// T_QCom32
//
//******************************************************************************
//******************************************************************************

//******************************************************************************
// Konstruktor T_QCCom32
//******************************************************************************
constructor T_QCCom32.Create(AOwner: TComponent);
var
    os : TOSVERSIONINFO;
begin
 inherited Create(AOwner);
 hPort := LongInt(INVALID_HANDLE_VALUE);  //Initialisiert die Eigenschaften
 lBaud := 9600;
 lDataBits:=8;                                      // 8 Datenbits
 lParity:=paNone;                                   // Keine Parit�t
 lStopBits:=sb1_0;                                  // 1 Stopbit
 nPort := 1;                                        // Port 1
 boolExpired := False;                              // Antwort nicht �berf�llig
 boolShowErrors := false;                           // Keine internen Fehlermeldungen anzeigen
 boolInUse := False;                                // Port nicht in gebrauch
 os.dwOSVersionInfoSize := sizeof(os);
 GetVersionEx(os);                                  // Windowsversion abfragen
 case os.dwPlatformId of
	 VER_PLATFORM_WIN32s:FNT:=false;
	 VER_PLATFORM_WIN32_WINDOWS:FNT:=false;
	 VER_PLATFORM_WIN32_NT: FNT:=True;                // Wenn Bertiebssystem NT dann FNT auf true
 else FNT:=false;
 end;
 Timer := TTimer.Create(Self);                      // Timer wird gebraucht f�r Timeout
 Timer.Enabled := False;                            // nur aktiv beim Lesen
 Timer.Interval := 1000;                            // 1 Sekunde Timeout
 Timer.OnTimer := TimesUp;                          // Ereignisroutine f�r den Timeout
 FRTS:=False;                                       // Per Default kein Handshake einschalten
end;//constructor T_QCCom32.Create(AOwner: TComponent);

//******************************************************************************
// Destructor T_QCCom32
//******************************************************************************
destructor T_QCCom32.Destroy;
begin
  CommSetRTS(False);                                // Gegebenenfalls Handshakeleitung zur�ckseten
  Timer.Free;                                       // Timer wieder freigeben
  Timer := nil;
  Close;                                            // evt ge�ffnete Schnittstelle wieder schliessen
  inherited Destroy;                                // Vorg�nger Zerst�ren
end;//destructor T_QCCom32.Destroy;

//******************************************************************************
// Setzt die Timeoutzeit in Millisekuden
//******************************************************************************
procedure T_QCCom32.SetTimeout(lTimeout: LongInt);
begin
 if Assigned(Timer) then Timer.Interval := lTimeout;
end;//procedure T_QCCom32.SetTimeout(lTimeout: LongInt);

//******************************************************************************
// Liest die aktuelle Timeoutzeit aus
//******************************************************************************
function T_QCCom32.GetTimeout: LongInt;
begin
 Result := 0;                                       //Wenn der Timer nicht vorhanden ist, Resultat = 0
 if Assigned(Timer) then Result := Timer.Interval;
end;//function T_QCCom32.GetTimeout: LongInt;

//******************************************************************************
// True wenn der aktuelle Port offen ist
//******************************************************************************
function T_QCCom32.IsOpen: Boolean;
begin
 Result := (hPort <> LongInt(INVALID_HANDLE_VALUE));
end;//function T_QCCom32.IsOpen: Boolean;

//******************************************************************************
// Baudrate setzen
//******************************************************************************
procedure T_QCCom32.SetBaud(lBaudToSet: LongInt);
begin
 if lBaudToSet <> lBaud then begin                  // Wenn neue Baudrate ungleich alte Baudrate
  lBaud := lBaudToSet;                              // Neue Baudrate zuweisen
  ChangeParameters;
  if IsOpen then begin                              // Wenn der Port bereits offen ist
   Close;                                           // Port schliessen
   Open;                                            // und wieder �ffnen. Damit wird die neue Baudrate �bernommen
  end;//if IsOpen then begin
 end;//if lBaudToSet <> lBaud then begin
end;//procedure T_QCCom32.SetBaud(lBaudToSet: LongInt);

//******************************************************************************
// Anzahl der Stopbits setzen
//******************************************************************************
procedure T_QCCom32.SetStopBits(lStopBitsToSet: TStopBit);
begin
 if lStopBitsToSet <> lStopBits then begin          // Wenn neuer Wert ungleich alter Wert
  lStopBits := lStopBitsToSet;                      // Neuen Wert setzen
  ChangeParameters;
  if IsOpen then begin                              // Wenn der Port bereits offen ist
   Close;                                           // Port schliessen
   Open;                                            // und wieder �ffnen. Damit wird der neue Wert �bernommen
  end;//if IsOpen then begin
 end;//if lStopBitsToSet <> lStopBits then begin
end;//procedure T_QCCom32.SetStopBits(lStopBitsToSet: TStopBit);

//******************************************************************************
// Anzahl der Datenbits setzen
//******************************************************************************
procedure T_QCCom32.SetDataBits(lDataBitsToSet: Integer);
begin
 if lDataBitsToSet <> lDataBits then begin          // Wenn neuer Wert ungleich alter Wert
  if ((lDataBits>=3)and(lDataBits<=8))then begin    // Bereichs�berpr�fung
   lDataBits := lDataBitsToSet;                     // Neuen Wert setzen
   ChangeParameters;                                // Ereignisroutine aufrufen
  end;//if ((lDataBits>=3)and(lDataBits<=8))then
  if IsOpen then begin                              // Wenn der Port bereits offen ist
   Close;                                           // Port schliessen
   Open;                                            // und wieder �ffnen. Damit wird der neue Wert �bernommen
  end;//if IsOpen then begin
 end;//if lDataBitsToSet <> lDataBits then begin
end;//procedure T_QCCom32.SetDataBits(lDataBitsToSet: Integer);

//******************************************************************************
// Parit�t setzen
//******************************************************************************
procedure T_QCCom32.SetParity(lParityToSet: TParity);
begin
 if lParityToSet <> lParity then begin              // Wenn neuer Wert ungleich alter Wert
  lParity := lParityToSet;                          // Neuen Wert setzen
  ChangeParameters;                                 // Ereignisroutine aufrufen
  if IsOpen then begin                              // Wenn der Port bereits offen ist
   Close;                                           // Port schliessen
   Open;                                            // und wieder �ffnen. Damit wird der neue Wert �bernommen
  end;//if IsOpen then begin
 end;//if lParityToSet <> lParity then begin
end;//procedure T_QCCom32.SetParity(lParityToSet: TParity);

//******************************************************************************
// Setzt den aktuellen Com-Port
//******************************************************************************
procedure T_QCCom32.SetPort(nPortToSet: Integer);
begin
 if nPortToSet <> nPort then begin                  // Wenn neuer Wert ungleich alter Wert
  nPort := nPortToSet;                              // Neuen Wert setzen
  ChangeParameters;                                 // Ereignisroutine aufrufen
  if IsOpen then begin                              // Wenn der Port bereits offen ist
   Close;                                           // Port schliessen
   Open;                                            // und wieder �ffnen. Damit wird der neue Wert �bernommen
  end;//if IsOpen then begin
 end;//if nPortToSet <> nPort then begin
end;//procedure T_QCCom32.SetPort(nPortToSet: Integer);

//******************************************************************************
// Eigenschaftsdialog zeigen und die Parameter setzen
//******************************************************************************
function T_QCCom32.properties:Boolean;
var
 cc:TCommConfig;   //Konfigurations Struktur (Windows)
begin
 Result:=false;                                     // Initialisieren
 cc.dwSize:=SizeOf(cc);                             // Struktur CommConfig initialisieren
 if isOpen then begin                               // Nur aufrufen wenn der Port bereits ge�ffnet ist
  GetCommConfig(hPort,cc,cc.dwSize);                // Aktuelle Einstellungen holen
  if lParity<>paNone then
    cc.dcb.Flags:=cc.dcb.Flags or 2;                  // Parit�t erm�glichen
	Result:=CommConfigDialog(Pchar('Com'+IntToStr(nPort)),Application.Handle,cc);// Konfigurations Dialog
  If Result then begin                              // Wenn nicht abruch geklickt
   SetBaud(cc.dcb.BaudRate);                        // Baudrate setzen
   SetStopBits(TStopBit(cc.dcb.StopBits));          // Stopbits setzen
   SetParity(TParity(cc.dcb.Parity));               // Parit�t setzen
   SetDataBits(cc.dcb.ByteSize);                    // Datenbits setzen
   if FFlag<>cc.dcb.Flags then begin
    FFlag:=cc.dcb.Flags;                            // Handshake
    ChangeParameters;                               // Ereignisroutine aufrufen
		if IsOpen then begin                            // Wenn der Port bereits offen ist
		 Close;                                         // Port schliessen
     Open;                                          // und wieder �ffnen. Damit wird der neue Wert �bernommen
    end;//if IsOpen then begin
	 end;//If Result then begin
  end;//If Result then begin
 end;//if (hPort<>INVALID_HANDLE_VALUE) then
end;//procedure T_QCCom32.properties;

//******************************************************************************
//  Setzt die TimeOuts der Schnittstelle
//  toChar: Dieser Wert in ms wird mit der Anzahl Zeichen die gesendet werden multipliziert
//  toMax : Dieser Wert wird zu toChar * Anzahl Bytes dazuaddiert
//
//  Der gesammte Timeout ergibt sich aus toChar * Anzahl Bytes + toMax
//******************************************************************************
procedure T_QCCom32.SetWriteTimeOut(toChar, toMax: Cardinal);
var
 CommTimeOut:TCommTimeOuts;
begin
	if GetCommTimeOuts(hPort,CommTimeOut) then begin  // Holt die alten Einstellungen
		CommTimeOut.WriteTotalTimeoutMultiplier:=toChar;
		CommTimeOut.WriteTotalTimeoutConstant:=toMax;
		SetCommTimeOuts(hPort,CommTimeOut);             // Schreibt die neuen Einstellungen
	end;//if GetCommTimeOuts(hPort,CommTimeOut) then begin
end;//procedure T_QCCom32.SetWriteTimeOut(toChar, toMax: Cardinal);

//******************************************************************************
// Aktuellen Port �ffnen
//******************************************************************************
function T_QCCom32.Open: Boolean;
var
 sCom: String;        // zu �ffnenden Port (z.B: 'Com1')
 dcbPort: TDCB;       // device control block
 boolAbort: Boolean;  // True wenn abbruch
 sErrMsg: String;     // Fehlermeldung
begin
 boolAbort := True;    		// Initialisieren
 if isOpen then Close;       	// Port schliessen wenn er bereits offen war

 repeat begin  //until (isOpen) or (boolAbort = True);
	sCom := 'COM' + IntToStr(nPort);                	// Portstring erzeugen
	if FNT then sCom:='\\?\'+sCom;
  try
	hPort := CreateFile(PChar(sCom), GENERIC_READ or GENERIC_WRITE, FILE_SHARE_READ	or FILE_SHARE_WRITE, nil,
		 OPEN_EXISTING , FILE_ATTRIBUTE_NORMAL, LongInt(0)); // Port �ffnen

  except
    on e:ERangeError do begin
      hPort:=LongInt(INVALID_HANDLE_VALUE);
    end;
  end;
	if (Not(isOpen)) and boolShowErrors then begin 	// Wenn Port nicht ge�ffnet werden kann
	 if MessageDlg('Error opening COM' + IntToStr(nPort) + ': ' + sErrMsg,
		mtWarning, [mbAbort, mbRetry], 0) = idAbort then// Fehlermeldung anzeigen
		boolAbort := True                           	// Abbrechen
	 else
		boolAbort := False;                         	// weiter versuchen
	 end;//if MessageDlg('Error opening COM'...
	end;//if (Not(isOpen)) and boolShowErrors then begin
	if Not(IsOpen) then boolAbort:=True; 	// Wenn keine Fehlermeldungen angezeigt werden, dann abbrechen
 until ((isOpen) or (boolAbort));              	// Solange versuchen bis Port ge�ffnet werden kann

 if (isOpen) then begin                             // Wenn Port ge�ffnet werden konnte
	if GetCommState(hPort, dcbPort) then begin      	// aktuelle Einstellungen holen
    dcbPort.BaudRate := lBaud;                    	// Baudrate
    dcbPort.ByteSize := lDataBits;  	// Anzahl Datenbits
    dcbPort.Parity := ord(lParity);  	// Parit�t
    dcbPort.StopBits := ord(lStopBits);  	// StopBits
    dcbPort.Flags := FFlag or 3;         	// fBinary(muss immer True sein) und fParity und AbortOnError
    if (dcbPort.Flags and 768) > 0 then
      dcbPort.Flags := dcbPort.Flags or 16;
    if ((not(SetCommState(hPort, dcbPort))){and (boolShowErrors) })then
      showMessage('Error setting DCB');                 	// Parameter setzen
    if FRTS then                                    // Wenn RTS gesetzt bleiben soll
      EscapeCommFunction(hPort,SETRTS);             // RTS setzen
    if FDTR then                                    // Wenn DTR gesetzt bleiben soll
      EscapeCommFunction(hPort,SETDTR);             // DTR setzen
	end;//if (isOpen) then begin
 end;//if GetCommState(hPort, dcbPort) then begin

 Result := IsOpen;
 if IsOpen then
   ChangeParameters;                                  // Ereignisroutine aufrufen
end;//function T_QCCom32.Open: Boolean;

//******************************************************************************
// schliesst den aktuellen Port
//******************************************************************************
procedure T_QCCom32.Close;
begin
 if isOpen then CloseHandle(hPort);                 // Port schliessen wenn er ge�ffnet ist
 hPort := LongInt(INVALID_HANDLE_VALUE);            // ung�liges Handle zuweisen
end;//procedure T_QCCom32.Close;

//******************************************************************************
// einen String auf einem Port ausgeben
//******************************************************************************
function T_QCCom32.Write(const sData: String): Boolean;
var
 dwCharsWritten: DWord; //Anzahl der geschriebenen Bytes
begin
 Result := False;                                  // Initialisieren
 dwCharsWritten := 0;                              //      "
 if isOpen then begin                              // Wenn Port ge�ffnet ist
	WriteFile(hPort, PChar(sData)^, Length(sData), dwCharsWritten, nil);// Zeichen Schreiben
  if dwCharsWritten = DWord(Length(sData)) then Result := True;// nur erfolgreich wenn alle Zeichen geschrieben
 end;//if isOpen then begin
end;//function T_QCCom32.Write(const sData: String): Boolean;

//******************************************************************************
// Z�hlt die Bytes im Empfangspuffer
//******************************************************************************
function T_QCCom32.GetInCount: LongInt;
var
 statPort: TCOMSTAT;    //Struktur (Windows)
 dwErrorCode: DWord;    //Fehlermeldung
begin
 Result := 0;                                       //Initialisieren
 if isOpen then begin                               //Wenn Port ge�ffnet ist
  ClearCommError(hPort, dwErrorCode, @statPort);    //Status des Portes auslesen
  Result := statPort.cbInQue;                       //Anzahl der empfangenen Zeichen
 end;//if isOpen then begin
end;//function T_QCCom32.GetInCount: LongInt;

//******************************************************************************
// Liest vom aktuellen Port
//******************************************************************************
function T_QCCom32.Read: String;
const
 BUF_LEN = 1024;        // Gr�sse des Input-Puffers
var
 cbCharsAvailable,      // Anzahl empfangener Zeichen
 cbCharsRead: DWord;    // Anzahl gelesene Zeichen
 boolExit: Boolean;     // True nach Timeout
 sBuffer: String;       // Empfangspuffer
begin
 FBreakOff:=false;
 SetLength(sBuffer, BUF_LEN);                       // Initialisieren
 Result := '';                                      //      "
 if boolInUse then begin                            // Wenn bereits eine Leseaktion in Gange ist
  if boolShowErrors then ShowMessage('Port is in use -- operation aborted');
  Exit;                                             // Fehler anzeigen und Routine verlassen
 end else
  boolInUse := True;                                // Leseaktion beginnt

 if isOpen then begin                               // Wenn der Port ge�ffnet ist
  if Length(sInTerminator) = 0 then begin           // Wenn kein Terminator vorhanden ist
   cbCharsAvailable := GetInCount;                  // Anzahl empfangene Zeichen lesen
   if cbCharsAvailable > 0 then begin               // Wenn Zeichen vorhanden sind
  	SetLength(Result, cbCharsAvailable);            // L�nge des Puffers setzen
		ReadFile(hPort, PChar(Result)^, cbCharsAvailable, cbCharsRead, nil);// Zeichen lesen
		SetLength(Result, cbCharsRead);                 // L�nge auf Anzahl gelesen Zeichen begrenzen (nur interressant bei einem Fehler)
   end;//if cbCharsAvailable > 0 then begin
  end else begin                                    // Wenn ein Terminator definiert ist dann Port lesen bis Terminator erreicht ist (oder Timeout)
   boolExit := False;                               // Initialisieren
   repeat //until boolExit = True...
    boolExpired := False;                           // Antwort noch nicht �berf�llig
    Timer.Enabled := True;                          // Timeoutimer starten
    repeat //until (boolExpired) or...
     cbCharsAvailable := GetInCount;                // Anzahl Zeichen z�hlen
     if cbCharsAvailable > 0 then begin             // Wenn Zeichen vorhanden
			SetLength(sBuffer, cbCharsAvailable);         // Platz reservieren
			ReadFile(hPort, PChar(sBuffer)^, cbCharsAvailable, cbCharsRead, nil); // Port lesen
      Result := Result + Copy(sBuffer, 0, StrLen(PChar(sBuffer)));  // String zusammenf�hren mit vorheigem String
     end;//if cbCharsAvailable > 0 then begin

     if Pos(sInTerminator, Result) <> 0 then begin  // Wenn der Terminator im String enthalten ist
      Result:=copy(result,1,Pos(sInTerminator,Result)-1); // Nur bis zum Terminator lesen
      boolExit := True;                             // Leseroutine beenden
     end else
      Application.ProcessMessages;                  // Messages verarbeiten
    until (boolExpired) or (boolExit) or(isOpen) or (FBreakOff);// Abbrechen wenn Antwort �berf�llig oder Antwort erhalten oder Port geschlossen
    If boolExpired then boolExit:=True;             // Aussteigen wenn Timeout
    if boolExpired and boolShowErrors then          // Bei Timeout Fehlermeldung ausgeben
     if MessageDlg('Wrtite Timeout', mtWarning,[mbAbort, mbRetry], 0) = idAbort then boolExit := True   until (boolExit) or (FBreakOff); //solange bis fertig gelesen oder Timeout
  end;//if Length(sInTerminator) = 0 then begin
 end;//if isOpen then begin
 Timer.Enabled := False;                            // Timer wird nicht mehr ben�tigt
 boolInUse := False;                                // Fertig gelesen
end;//function T_QCCom32.Read: String;

//******************************************************************************
// Timeout
//******************************************************************************
procedure T_QCCom32.TimesUp(Sender: TObject);
begin
 boolExpired := True;                               // Timeout Flag
 Timer.Enabled := False;                            // Timer wird nicht mehr gebraucht nach einem Timeout
 Timeout;                                           // Anwendercode ausf�hren
end;//procedure T_QCCom32.TimesUp(Sender: TObject);

//******************************************************************************
// Anwendercode ausf�hren
//******************************************************************************
procedure T_QCCom32.Timeout;
begin
 if Assigned(pmOnTimeout) then pmOnTimeout(self);
end;//procedure T_QCCom32.Timeout;

//******************************************************************************
// Ruft den Ereigniscode auf in OnParamChange
//******************************************************************************
procedure T_QCCom32.ChangeParameters;
begin
 if Assigned(pmOnParamChange) then pmOnParamChange(self);
end;//procedure TComLK.ChangeParameters

//******************************************************************************
//  Empfangspuffer leeren
//******************************************************************************
procedure T_QCCom32.Flush;
begin
 if isOpen then begin
	PurgeComm(hPort, PURGE_TXABORT or PURGE_RXABORT or PURGE_TXCLEAR or PURGE_RXCLEAR);
 end;//if isOpen then begin
end;//procedure T_QCCom32.Flush;

//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************
//
// TComLK
//
//******************************************************************************
//******************************************************************************
//******************************************************************************
//******************************************************************************

//******************************************************************************
// Registrieren in der Delphi IDE
//******************************************************************************
procedure Register;
begin
 RegisterComponents('LK', [TComLK]);
end;//procedure Register

//******************************************************************************
// Konstruktor TComLK
//******************************************************************************
Constructor TComLK.Create(AOwner: TComponent);
begin
 inherited Create(AOwner);                          // Konstruktor des Vorg�ngers aufrufen
 Einstellungen:=nil;        // Schnittstellenauswahldialog
 FWithCaption:=false;
end;//Constructor TComLK.Create(AOwner: TComponent);

//******************************************************************************
// Destruktor TComLK
//******************************************************************************
Destructor TComLK.Destroy;
begin
 Einstellungen.free;                                // Formular f�r die Einstellungen wieder freigeben
 inherited Destroy;                                 // Destruktor des Vorg�ngers aufrufen
end;//Destructor TComLK.Destroy;
//******************************************************************************
// Schnittstellenauswahl Dialog aufrufen
//******************************************************************************
procedure TComLK.Eigenschaften;
var
 i:Integer;             // Laufzeitvariable
 p:Integer;             // aktueller Port
 temp:String;           // Laufzeitvariable
 TextBreite:integer;    // Breite der Beschriftung f�r den Port
 Wert,Code:integer;     // F�r Funktion Val gebraucht
 comm  : String;    		// Wird an API geschickt (FileCreate)
 ComId : THandle;      	// Handle der jeweils offenen Schnittstelle
begin
  if Einstellungen=nil then
    Einstellungen:=TEinstellungen.Create(self);
 with Einstellungen do begin
   butEigenschaften.Caption:=CaptionProperties;     // Beschriftungen setzen
   butOK.Caption:=CaptionOK;
   RadioGB.Caption:=CaptionPort;
   Caption:=CaptionForm;
	 nPortAlt:=Port;                                  // Alte Einstellungen merken
	 lBaudAlt:=Baud;
	 lDataBitsAlt:=Databits;
	 lStopBitsAlt:=StopBits;
	 lParityAlt:=Parity;
	 p:=Port;                                         // Aktueller Port

	 with RadioGb do begin   	// GroupBox mit den verf�gbaren Schnittstellen
		if FNT
    	then Items:=PortEnableNT                      // Vorhandene Schnittstellen (Win NT) abfragen
      else Items:=PortEnable;               	// Vorhandene Schnittstellen (Win 9x) abfragen
		Columns:=(Items.Count div 6)+1;                 // Anzahl Spalten f�r die Schnittstellen Box
		If Items.Count<5 then Height:=30*Items.Count;   // Pro Port 30 Pixel Platz...
    if Height<50 then Height:=50;                   // ... mindestens aber 50 Pixel (1 Port)
    TextBreite:=0;
    for i:=0 to Items.count-1 do begin              // Breite des Breitesten eintrages ermitteln
      if canvas.TextWidth(Items[i])>TextBreite then TextBreite:=canvas.TextWidth(Items[i]);
    Application.ProcessMessages;

    Temp:=copy(Items[i],4,Length(Items[i]));        // Nummer des Ports extrahieren
    Val(Temp,Wert,Code);                            // String in einen Integer umwandeln

    Comm:='COM'+IntToStr(Wert);                  	// Port festlegen
    if FNT then Comm:='\\?\'+Comm;                  // F�r NT mit anderem String aufrufen
    ComId:=CreateFile(PChar(Comm),0, FILE_SHARE_READ or FILE_SHARE_WRITE, nil, OPEN_EXISTING , 0, 0);// �ffnet die Schnittstelle
    if ComId<>INVALID_HANDLE_VALUE then             // Wenn Handle ung�ltig
      CloseHandle(ComId);                           // Port schliessen (Handle wieder freigeben)
    Controls[i].Enabled:=not((ComId=INVALID_HANDLE_VALUE)and(Wert<>p)); // Aktuellen Port nicht greyed
    TRadioButton(controls[i]).Checked:=(Wert=p);    // Aktuellen Port selektieren
    end;//for i:=0 to Items.count-1 do begin
	 end;//with RadioGb.Items do
   if ((TextBreite+30)*RadioGb.Columns) > 300 then  // Breite des Formulars bestimmen (mindestens 300 Pixel)
	    ClientWidth:=RadioGb.Left+((TextBreite+30)*RadioGb.Columns)+RadioGb.Left;
	 ClientHeight:=RadioGb.Top+RadioGb.Height+paButtons.Height+30;
 end;//with (Owner as TComLK) do
 Einstellungen.showModal;                           // Eigenschaften und Portnummer einstellen
 Einstellungen.Free;
 Einstellungen:=nil;
end;//procedure TComLK.Einstellungen;

//*****************************************************************
//  Ports auf Verf�gbarkeit testen
//*****************************************************************
function TComLK.PortEnable:TStringList;
var
  Buffer, PortInfo: PChar;   // Wird von der Funktion PPortInfo2 gefordert
  Count,                     // Gibt die Anzahl der ben�tigten Bytes an
  NumInfo: DWORD;            // Gibt die Anzahl Ports aus
  i: Integer;                // Laufzeitvariable
  Level: Byte;               // Level der API Funktion (1 oder 2)
begin
  result:=TStringList.Create;                       // StringListe initialisieren
  Level:=2;               	// PortInfo 2 aufrufen
  EnumPorts(nil,Level,nil,0,Count,NumInfo);         // In Count steht die Anzahl Bytes die gefordert sind
  if Count = 0 then Exit;                           // Bei 0 Fehler
  GetMem(Buffer, Count);                            // Platz reservieren
  EnumPorts(nil,Level,PByte(Buffer),Count,Count,NumInfo); // Ports holen von Windows
  PortInfo:=Buffer;                                 // Ergebnis sichern
  for i := 0 to NumInfo - 1 do begin                // Alle Ports durchlaufen
    with PPortInfo2(PortInfo)^ do begin           	// Record �ber PortInfo legen
      if pos('com',LowerCase(pPortName))>0 then   	// Nur Com - Ports auflisten
        result.Add(pPortName+' '+pDescription);   	// Portbeschreibung auslesen
      Inc(PortInfo, sizeof(TPortInfo2));          	// N�chsten Port
    end;// with PPortInfo1(PortInfo)^ do
  end;// for i := 0 to NumInfo - 1 do begin
end; //Procedure PortEnable:

//******************************************************************************
// �nderungen �bernehmen
//******************************************************************************
procedure TEinstellungen.butOKClick(Sender: TObject);
begin
 Close;
end;//procedure TEinstellungen.butOKClick(Sender: TObject)

//******************************************************************************
// Eigenschaften �ndern
//******************************************************************************
procedure TEinstellungen.butEigenschaftenClick(Sender: TObject);
begin
 With (Owner as TComLK) do begin 	// Aktuelle Schnittstelle
	If Not(isOpen) then begin                       	// Wenn Schnittstelle geschlossen ist
	 If Not(open) then                             	// Wenn schnittstelle nicht zu verf�gung steht, dann Fehlermeldung ausgeben
    MessageBox (0, PChar(Format('Port %s steht im Moment nicht zu Verf�gung',[IntToStr(Port)])) , 'Fehler beim Zugriff auf Schnittstelle' , MB_ICONEXCLAMATION or MB_OK);
  end;//If Not(isOpen) then begin
	properties;       			// Eigenschaftsdialog aufrufen
 end;//With (Owner as TComLK) do begin
end;

//******************************************************************************
// Aktuellen Port ausw�hlen
//******************************************************************************
procedure TEinstellungen.RadioGbClick(Sender: TObject);
var
 Code:Integer;      // Laufzeitvariable
 Wert:Integer;      // Gew�hlter Com Port
 Temp:String;       // Name des Ports als String (z.B 'Com 4')
begin
 If RadioGB.ItemIndex>=0 then begin                 // Wenn ein Index aktiv ist
  Temp:=RadioGB.Items[RadioGB.Itemindex];           // Text des Eintages auslesen
  Temp:=copy(Temp,4,Length(Temp)-3);                // Nummer des Ports extrahieren
  Val(Temp,Wert,Code);                              // String in einen Integer umwandeln
  If (Code=0)or(Code>1) then begin                  // Wenn Wert g�ltig
   (Owner as TComLK).close;                         // Alten Port Schliessen
   (Owner as TComLK).Port:=Wert;                    // Wert �bernehmen
  end;//If Code=0 then begin
 end;//If RadioGB.ItemIndex>=0 then
end;

//******************************************************************************
// Wartet eine bestimmte Anzahl Millisekunden mit der weiteren Ausf�hrung des Programmes
//******************************************************************************
procedure TComLK.Delay(Millisekunden:LongInt);
var
 start:LongInt;
begin
 start:=GetTickCount;
 while (LongInt(GetTickCount)-Start)<Millisekunden do
  Application.ProcessMessages;
end; //procedure Delay(Millisekunden:Integer)

//******************************************************************************
// Status und Parameter der seriellen Schnittstelle als string
//******************************************************************************
function TComLK.GetCommStatus:String;
begin
 if IsOpen
  then result:='Port: '+IntToStr(port)+' - '+ GetPortString
  else result:='Port: '+IntToStr(port)+' - '+ 'Not Open';
end;//function TComLK.GetCommStatus:String

//******************************************************************************
//  Setzt die Helpcontexts der Buttons (Gleichzeitig wird die Popup Hilfe aktiviert)
//******************************************************************************
procedure TComLK.SetHelpContext(Control: TEinstControl; Index: Integer);
begin
	case Control of
		ecOK:Einstellungen.butOK.HelpContext:=Index;
		ecPort:Einstellungen.RadioGb.HelpContext:=Index;
		ecProp:Einstellungen.butEigenschaften.HelpContext:=Index;
	end;//case Control of
	Einstellungen.BorderIcons:=Einstellungen.BorderIcons+[biHelp];
end;//procedure TComLK.SetHelpContext(Control: TEinstControl; Index: Integer);

//******************************************************************************
//	Gibt die Parameter des Ports in der Form (Baudrate,Databits,Parity,Stopbit) zur�ck
//******************************************************************************
function TComLK.GetPortString: string;
var
	s:string;	//Nimmt das Zeichen f�r die Parit�t auf
begin
	result:=IntTostr(lBaud)+', '+IntTostr(lDataBits);
	case lParity of                                   // Parit�t auslesen
		paNone:s:='N';
		paOdd:s:='O' ;
		paEven:s:='E';
		paMark:s:='M';
		paSpace:s:='S';
	end;//case comm.Parity of
  result:=result+', '+s+', '+FloatToStr(1+ord(StopBits)/2);// Und noch die StopBits hinzuf�gen
end;//function TComLK.GetPortString: string;

//******************************************************************************
//  Setzt die RTS-Leitung auf High(True) oder Low(False)
//******************************************************************************
procedure T_QCCom32.CommSetRTS(const Value: boolean);
begin
  FRTS := Value;                                    // Wert sichern
  if opened then begin                              // Wenn der Port ge�ffnet ist, dann ...
    if (FRTS)
      then EscapeCommFunction(hPort,SETRTS)         // ... die RTS leitung auf den entsprechenden Pegel setzen
      else EscapeCommFunction(hPort,CLRRTS);
  end;// if opened then begin
end;// procedure T_QCCom32.CommSetRTS(const Value: boolean);

//******************************************************************************
//  Setzt die DTR-Leitung auf High(True) oder Low(False)
//******************************************************************************
procedure T_QCCom32.CommSetDTR(const Value: boolean);
begin
  FDTR := Value;
  if opened then begin                              // Wenn der Port ge�ffnet ist, dann ...
    if (FRTS)
      then EscapeCommFunction(hPort,SETDTR)         // ... die RTS leitung auf den entsprechenden Pegel setzen
      else EscapeCommFunction(hPort,CLRDTR);
  end;// if opened then begin
end;// procedure T_QCCom32.CommSetDTR(const Value: boolean);

//******************************************************************************
//  Port Enable f�r NT (PortInfo2 geht nicht unter NT)
//******************************************************************************
function TComLK.PortEnableNT: TStringList;
var
 comm  : String;    		//Wird an API geschickt
 ComId : THandle;      	//Handle der jeweils offenen Schnittstelle
 CC    : TCOMMCONFIG;  	//Struktur f�r CreateFile
 c:Integer;          		//Laufzeitvariable
 Counter:Integer;    		//Inkrementiert bei jedem nicht vorhandenen Port
 i:Integer;
const
 PortFehlt=2;          	//R�ckgabewert der Funktion GetLastError
 PortBesetzt=5;        	//R�ckgabewert der Funktion GetLastError
begin
 Result:=TStringList.Create; //Initialisieren
 Counter:=0;c:=1;
 CC.dwSize:= SizeOf(TCOMMCONFIG);          	//Gr�sse der Struktur ermitteln und eintragen
 While (Counter<3)do begin                         	//erst abbrechen wenn 3 Ports nacheinander nicht vorhanden waren
	Application.ProcessMessages;
	Comm:='\\?\'+'COM'+IntToStr(c);                  	//Port festlegen
	ComId:=CreateFile(PChar(Comm),0, FILE_SHARE_READ or FILE_SHARE_WRITE, nil, OPEN_EXISTING , 0, 0);  //�ffnet die Schnittstelle
	if ComId <> INVALID_HANDLE_VALUE then begin       //Wenn Handle g�ltig
	 Result.Add('Com '+IntToStr(c));         	//Port frei
	 CloseHandle(ComId); 			//Port schliessen (Handle wieder freigeben)
	end else begin //IF ComId <> INVALID_HANDLE_VALUE...
	 i:=GetLastError;
	 Case i of                    	//Fehlernummer bei Windows abholen
		PortFehlt:If (c>4)then inc(Counter);        	//Port ist nicht vorhanden  Erst ab Port 5 hochz�hlen
		PortBesetzt: begin                         	//Port ist bereits ge�ffnet
		 Result.Add('Com '+IntToStr(c));    	//alle anderen Ports als besetzt markieren
		 Counter:=0;                       	//Counter wieder zur�cksetzen
		end; //PortBesetzt
	 end; //case GetLastError of
	end; //IF ComId <> INVALID_HANDLE_VALUE...
	inc(c);                                  	//n�chster Port
 end; // While (Counter<3)do
end;// function TComLK.PortEnableNT: TStringList;

//******************************************************************************
// Liest vom aktuellen Port
//******************************************************************************
function TComLK.ReadBinary(var Count:integer; var aCharArrray: TCharArray): boolean;
var
 cbCharsAvailable,      // Anzahl empfangener Zeichen
 cbCharsRead: DWord;    // Anzahl gelesene Zeichen
begin
  result := false;
  cbCharsRead:=0;                                   // initialisieren
  if isOpen then begin                              // Wenn der Port ge�ffnet ist
    cbCharsAvailable := GetInCount;                 // Anzahl empfangene Zeichen lesen
    // Array f�r alle Zeichen plus die abschliessende 0
    if cbCharsAvailable > 0 then begin              // Wenn Zeichen vorhanden sind
		  result := ReadFile(hPort, aCharArrray, cbCharsAvailable, cbCharsRead, nil);// Zeichen lesen
    end;//if cbCharsAvailable > 0 then begin
  end;//if isOpen then begin
  Count:=cbCharsRead;
end;//function TComLK.Read: String;

//******************************************************************************
// einen String auf einem Port ausgeben
//******************************************************************************
function TComLK.WriteBinary(Data: Byte): Boolean;
var
 dwCharsWritten: DWord; //Anzahl der geschriebenen Bytes
 p:pointer;
begin
  Result := False;                                   // Initialisieren
  dwCharsWritten := 0;                               //      "
  p:=@Data;
  if isOpen then begin                               // Wenn Port ge�ffnet ist
	  WriteFile(hPort, p^{PChar(Data)^}, 1, dwCharsWritten, nil);  // Zeichen Schreiben
    Result := (dwCharsWritten = 1);                   // nur erfolgreich wenn alle Zeichen geschrieben
  end;//if isOpen then begin
end;//function TComLK.WriteBinary(Data: Byte): Boolean;
end.
