unit u_MMAlerterConfigForm;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, mmLabel, mmCheckBox, mmNumCtrl, Buttons, mmBitBtn,
  mmGroupBox, mmRadioGroup, mmSpeedButton, ImgList, ActnList, mmActionList,
  mmTimer,
  LoepfeGlobal, ADODBAccess, IvDictio, IvMulti, IvEMulti, mmTranslator,
  IvAMulti, IvBinDic, mmDictionary,
  u_AlerterConfig, mmEventLog;

type

  TAlertConfiguration = class(TForm)
    gbQOfflimitAlert: TmmGroupBox;
    bSave: TmmBitBtn;
    bCancel: TmmBitBtn;
    neMaxQOAlert: TmmNumEdit;
    cbtActivateQOAlert: TmmCheckBox;
    lbMaxQOAlert: TmmLabel;
    gbSerialInterface: TmmRadioGroup;
    gbAlertTimer: TmmGroupBox;
    lbSwitchOffTime: TmmLabel;
    neSwitchOffTime: TmmNumEdit;
    gbConnectionCHeck: TmmGroupBox;
    bbCheckConnection: TmmBitBtn;
    lbConnectionStatus: TmmLabel;
    ImageList: TImageList;
    mmActionList: TmmActionList;
    acAlarmLightOFF: TAction;
    acAlarmLightON: TAction;
    sbAlarmLight: TmmSpeedButton;
    tTestlightTimer: TmmTimer;
    mmDictionary1: TmmDictionary;
    mmTranslator1: TmmTranslator;
    mmLabel1: TmmLabel;
    Button1: TButton;
    Button2: TButton;

    procedure FormCreate(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure neMaxQOAlertExit(Sender: TObject);
    procedure neSwitchOffTimeExit(Sender: TObject);
    procedure bbCheckConnectionClick(Sender: TObject);
    procedure bbLightONClick(Sender: TObject);
    procedure bbLightOFFClick(Sender: TObject);
    procedure acAlarmLightOFFExecute(Sender: TObject);
    procedure acAlarmLightONExecute(Sender: TObject);
    procedure tTestlightTimerTimer(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
    procedure neSwitchOffTimeEnter(Sender: TObject);
    procedure neMaxQOAlertEnter(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    function GetShowCOMInterface: Boolean;
  private
    { Private declarations }
    mADODBAccess : TADODBAccess;

    mExistsDBData : Boolean;
    mLog: TEventLogWriter;

    procedure GetComPortsList;
    property ShowCOMInterface: Boolean read GetShowCOMInterface;
    property ExistsDBData : Boolean read mExistsDBData;
  public
    { Public declarations }
  end;

   procedure CannotLoadLibraryException;


//   function Init():Integer ; stdcall; external 'MMAlerter.dll';
   function ComPortCheck(aComPortNr: WORD):Boolean; stdcall; external 'MMAlerter.dll';
   function AlarmON():Boolean ; stdcall; external 'MMAlerter.dll';
   function AlarmOFF():Boolean ; stdcall; external 'MMAlerter.dll';
   function QOfflimitAlarm: Boolean; stdcall; external 'MMAlerter.dll';



var
  AlertConfiguration: TAlertConfiguration;

resourcestring
   cNoComPort         = '(*)Kein COM Port f�r die Alarmierungs-Lampe gewaehlt!'; //ivlm
   cChooseComPort     = '(*)Bitte waehlen Sie einen COM Port oder druecken Sie ''%s''.'; //ivlm
   cNoDBDataExists    = '(*)Es wuren keine Daten gespeichert!'; //ivlm
   cNoMMExists        = '(*)MillMaster ist nicht installiert!'; //ivlm

implementation

{$R *.DFM}
uses NTException, Registry,
     RelaisCard, ComLK;




procedure CannotLoadLibraryException;
begin
  Raise ENTException.Create('Cannot load library "'+ cDllName + '" or linked libraries', 0);
end;




//------------------------------------------------------------------------------
procedure TAlertConfiguration.FormCreate(Sender: TObject);
var xComPort: Integer;
begin

 try
     mLog := TEventLogWriter.Create('MillMaster', '.', ssApplication, 'MillMaster Alerter: ', True);

     lbConnectionStatus.Caption := '';
     
      with TRegistry.create do
      try
        Rootkey:= cRegLM;
        if not Keyexists( cRegMillMasterPath ) then begin
           MessageDlg(cNoMMExists, mtError,  [mbAbort], 0);
           Application.Terminate;
           exit;
        end;
      finally
        free;
      end;

      if not GetShowCOMInterface then begin
         AlertConfiguration.Height := AlertConfiguration.Height - gbSerialInterface.Height;
         gbSerialInterface.Visible:= FALSE;
      end;

      mExistsDBData := FALSE;

      mADODBAccess := TADODBAccess.Create(1);
      mADODBAccess.Init;

      //Alle Com-Ports auslesen
      GetComPortsList;

      //Einstellungen von DB lesen
      with mADODBAccess.Query[0] do begin
         Close;

         SQL.Text := cGetRelaisData;
         ParamByName('c_Relais_Nr').AsInteger := GetOfflimitRelais; //cQOfflimitRelais;

         Open;
         First;

         if not Eof then begin
            cbtActivateQOAlert.Checked := FieldByName('c_Active').AsBoolean;
            neSwitchOffTime.Value      := FieldByName('c_SwitchOffTime').AsInteger;
            neMaxQOAlert.Value         := FieldByName('c_MaxError').AsInteger;
            mExistsDBData := TRUE;
         end;
      end;

      xComPort := GetRegInteger(cRegLM, cAlertRegPath, cComPort, 0 ) - 1;

      if (xComPort > gbSerialInterface.Items.Count-1) or (xComPort < 0) then
         gbSerialInterface.ItemIndex := -1
      else
         gbSerialInterface.ItemIndex := xComPort;



 except
    on E: Exception do begin
       MessageDLG( 'Error in TAlertConfiguration.FormCreate(). Error msg: ' + e.Message, mtError, [mbOk], 0);
    end;
 end;

end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.bSaveClick(Sender: TObject);
var xText : String;
begin


  if (gbSerialInterface.ItemIndex < 0) and ShowCOMInterface then begin
     xText := Format(cChooseComPort, [bbCheckConnection.caption] );
     MessageDLG( cNoComPort + cLRRT + cLRRT + xText, mtError, [mbOk], 0);
     gbSerialInterface.SetFocus;
     exit;
  end;

  if ShowCOMInterface then
     SetRegInteger(cRegLM, cAlertRegPath, cComPort, gbSerialInterface.ItemIndex + 1 );

  //Einstellungen auf DB schreiben
  with mADODBAccess.Query[0] do begin
     Close;
     if not mExistsDBData then begin
        SQL.Text := cInsertNewRelaisData;

        ParamByName('c_Relais_Nr').AsInteger     := GetOfflimitRelais;//cQOfflimitRelais;
        ParamByName('c_Active').AsBoolean        := cbtActivateQOAlert.Checked;
        ParamByName('c_MaxError').AsInteger      := neMaxQOAlert.AsInteger;
        ParamByName('c_ActualError').AsInteger   := 0;
        ParamByName('c_SwitchOffTime').AsInteger := neSwitchOffTime.AsInteger;

        ExecSQL;
     end else begin
        SQL.Text := cUpdateRelaisData;

        ParamByName('c_Relais_Nr').AsInteger     := GetOfflimitRelais;//cQOfflimitRelais;
        ParamByName('c_Active').AsBoolean        := cbtActivateQOAlert.Checked;
        ParamByName('c_MaxError').AsInteger      := neMaxQOAlert.AsInteger;
        ParamByName('c_SwitchOffTime').AsInteger := neSwitchOffTime.AsInteger;

        ExecSQL;
     end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.neMaxQOAlertExit(Sender: TObject);
begin
  neMaxQOAlert.CheckValue;
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.neSwitchOffTimeExit(Sender: TObject);
begin
  neSwitchOffTime.CheckValue;
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.bbCheckConnectionClick(Sender: TObject);
var xOutputSet : TOutput;
    x, xPort : Word;
    xRet : Boolean;
    xText : String;
begin

  for x:= 0 to gbSerialInterface.Items.Count-1 do begin
      xText :=  StringReplace(gbSerialInterface.Items.Strings[x], 'COM', '',[rfReplaceAll]);
      try
         xPort := StrToInt(xText);
      except
         xPort := x+1;
      end;

      xRet := ComPortCheck(xPort);

      //mLog.Write(etInformation, 'Com Port : ' + xText  + ';  '  + IntToStr(Integer(xRet))  );

      if xRet then break;
  end;

  if not xRet then
     //lbConnectionStatus.Caption := Format('No connection on COM-Port %d',[xPort])
     lbConnectionStatus.Caption := Format('No connection on all COM-Ports',[xPort])
  else begin
     lbConnectionStatus.Caption := Format('Connection on COM-Port %d',[xPort]);
     gbSerialInterface.ItemIndex := x;
     SetRegInteger(cRegLM, cAlertRegPath, cComPort, gbSerialInterface.ItemIndex + 1 );
     mLog.Write(etInformation, lbConnectionStatus.Caption);
  end;

end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.bbLightONClick(Sender: TObject);
begin

end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.bbLightOFFClick(Sender: TObject);
begin

end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.acAlarmLightOFFExecute(Sender: TObject);
var xBMP : TBitmap;
begin
  //Von OFF nach ON
  sbAlarmLight.Action :=  acAlarmLightON;

  xBMP := TBitmap.Create;
  ImageList.GetBitmap( TAction(sbAlarmLight.Action).ImageIndex, xBMP);

  sbAlarmLight.Glyph.Assign(xBMP);
  xBMP.Free;

  tTestlightTimer.Enabled := TRUE;

  lbConnectionStatus.Caption := '';
  if not AlarmON then
     lbConnectionStatus.Caption := 'No connection. Check COM-Port'
  else
     mLog.Write(etInformation, 'Check flash light: ON');

end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.acAlarmLightONExecute(Sender: TObject);
var xBMP : TBitmap;
begin
  //Von ON nach OFF
  sbAlarmLight.Action :=  acAlarmLightOFF;

  xBMP := TBitmap.Create;
  ImageList.GetBitmap( TAction(sbAlarmLight.Action).ImageIndex, xBMP);

  sbAlarmLight.Glyph.Assign(xBMP);
  xBMP.Free;

  tTestlightTimer.Enabled := FALSE;
  if AlarmOFF then
     mLog.Write(etInformation, 'Check flash light: OFF');
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.tTestlightTimerTimer(Sender: TObject);
begin
  //BL-Test aus
  sbAlarmLight.Down := FALSE;
  acAlarmLightON.Execute;
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.GetComPortsList;
begin
  gbSerialInterface.Items.Clear;
  gbSerialInterface.Items.CommaText := GetComPortsListAsCommaText;
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.bCancelClick(Sender: TObject);
begin

  if not mExistsDBData then
     MessageDlg(cNoDBDataExists, mtWarning,   [mbOk], 0);


 Close;
end;
//------------------------------------------------------------------------------
function TAlertConfiguration.GetShowCOMInterface: Boolean;
var xPCName, xMMHost : String;
begin
  xMMHost:= GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost,'');
  xPCName :=MMGetComputerName;

  if CompareText(xMMHost, xPCName) = 0 then
     Result := TRUE
  else
     Result := FALSE;

  Result := TRUE;
end;
//------------------------------------------------------------------------------  
procedure TAlertConfiguration.neSwitchOffTimeEnter(Sender: TObject);
begin
  neSwitchOffTime.Text:= StringReplace(neSwitchOffTime.Text, '--', '-',[rfReplaceAll]);
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.neMaxQOAlertEnter(Sender: TObject);
begin
   neMaxQOAlert.Text:= StringReplace(neMaxQOAlert.Text, '--', '-',[rfReplaceAll]);
end;
//------------------------------------------------------------------------------
procedure TAlertConfiguration.FormDestroy(Sender: TObject);
begin
  mADODBAccess.Free;
  mLog.Free;
end;

procedure TAlertConfiguration.Button1Click(Sender: TObject);
begin
  QOfflimitAlarm;
  {
  if AlarmOFF then
     mLog.Write(etInformation, 'Quit alert by hand');
  }
end;

procedure TAlertConfiguration.Button2Click(Sender: TObject);
begin
    if AlarmOFF then
       mLog.Write(etInformation, 'Quit alert by hand');
end;

initialization
   {
   CoInitialize(nil);

   Dll_HModule := LoadLibrary( cDllName );
   if Dll_HModule <> 0 then begin

       ComPortCheck:= GetProcAddress(Dll_HModule,'ComPortCheck');
       Init:= GetProcAddress(Dll_HModule,'Init');
       AlarmON:= GetProcAddress(Dll_HModule,'AlarmON');
       AlarmOFF:= GetProcAddress(Dll_HModule,'AlarmOFF');

   end;

   }
finalization
   {
   FreeLibrary(Dll_HModule);

   CoUnInitialize;
   }

end.



