//TConradRelais.GetActive() //SDO


unit RelaisCard;

interface

uses
  classes, ComLK, sysutils, windows;

type
  ERelaisException = class(Exception);

  TOutput = (o1, o2 , o3, o4, o5, o6, o7, o8);
  TOutputSet = Set of TOutput;

  TRelaisCommand = (rcNoOp, rcSetup, rcGetPort, rcSetPort, rcGetOption, rcSetOption);

  TConradRelaisCommandFrame = record
    Command  : Byte;
    Address  : Byte;
    Data     : Byte;
    Checksum : Byte;
  end;// TConradRelaisCommandFrame = record

  TConradCom = class(TComLK)
  public
    constructor Create(AOwner: TComponent);override;
  end;

  TConradRelais = class
  private
    FOutputs: TOutputSet;
    FAddress: Byte;
    FMaxOutput: TOutput;
    FInitialized: boolean;

    function CheckComOpen: boolean;
    function WriteCom(aFrame: TConradRelaisCommandFrame; var aAnswer: TCharArray): boolean;
    function CalculateCommand(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;
    function GetPortString: string;
    procedure SetComPort(const Value: integer);
    function GetComPort: integer;
    function CheckResult(aAnswer: TCharArray; aCommand: TRelaisCommand): boolean;
    function getComHandle: THandle;
    function GetISCOMOpen: Boolean;
  protected
    mCom: TComLK;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure ShowComProperties;

    function Init: boolean;

    function SetActive(aOutputs: TOutputSet): boolean; overload;
    function SetActive: boolean; overload;
    function ResetOutputs: boolean;
    function GetActive(aOutputs: TOutputSet): boolean;  //SDO

    procedure CloseCOM;

    property Outputs: TOutputSet read FOutputs write FOutputs;
    property Address:Byte read FAddress write FAddress;
    property ComPort: integer read GetComPort write SetComPort;
    property PortString: string read GetPortString;
    property ComHandle: THandle read GetComHandle;
    property ISCOMOpen: Boolean read GetISCOMOpen;


  end;// TConradRelais = class

implementation

uses
  math, dialogs; //  u_MMAlerter, mmEventLog;

{ TConradRelais }

function TConradRelais.CheckComOpen: boolean;
begin
  {$ifdef DEBUG_COM}
    result := true;
    exit;
  {$endif}

	result:=false;					// Initialisieren
  if assigned(mCom) then begin
    with mCom do begin
      // Wenn die Schnittstelle noch nicht offen ist, dann �ffnen
      if not(opened) then
        open;     
                                            
      // Nochmals versuchen
      if not(opened) then
        open;                                         
      result:=opened;                                 // true wenn Schnittstelle offen ist
    end;//with FComm do begin
  end;// if assigned(mCom) then begin
  
  if not(result) then                               // Fehler wenn die Schnittstelle nicht ge�ffnet werden kann
    raise ERelaisException.Create(Format('TConradRelais.CheckComOpen: Konnte Port nicht �ffnen (Settings = %s)', [mCom.PortString]));
end;

constructor TConradRelais.Create;
begin
  inherited;
  mCom := TConradCom.Create(nil);  
  with mCom do begin
    Baud := 19200;
    Parity := paNone;
    DataBits := 8;
    StopBits := sb1_0;

  end;
  
  FAddress := 1;
  FMaxOutput := o8;
end;

destructor TConradRelais.Destroy;
begin
  mCom.Close;
  FreeAndNil(mCom);
  inherited;
end;

function TConradRelais.SetActive(aOutputs: TOutputSet): boolean;
var
  xPortByte: Byte;
  i: TOutput;
  xAnswer: TCharArray;
begin
  result := false;
  if Init then begin
    xPortByte := 0;
    for i:= o1 to FMaxOutput do begin
      if i in aOutputs then
        xPortByte := xPortByte or (1 shl ord(i));
    end;// for i:= Low(TOutput) to FMaxOutput do begin

    result := WriteCom(CalculateCommand(rcSetPort, xPortByte), xAnswer);
    result := result and CheckResult(xAnswer, rcSetPort);
  end;
end;

function TConradRelais.CheckResult(aAnswer: TCharArray; aCommand: TRelaisCommand): boolean;
  function CheckCheckSum(aCount: integer): boolean;
  var
    i: integer;
    xCheckSum: integer;
  begin
    xCheckSum := aAnswer[0];
    for i := 1 to aCount - 2 do
      xCheckSum := xCheckSum xor aAnswer[i];
    result := xCheckSum = aAnswer[aCount - 1];
  end;// function CheckCheckSum(aCount: integer): boolean; 

  function CheckCommandAddress: boolean;
  begin
    result := (aAnswer[0] = (Byte(not(ord(aCommand))))) and (aAnswer[1] = FAddress);
  end;
begin
  if aCommand = rcSetup then
    result := checkCheckSum(8) and CheckCommandAddress
  else
    result := checkCheckSum(4) and CheckCommandAddress;
end;
 
function TConradRelais.ResetOutputs: boolean;
begin
  result := SetActive([]);
end;

function TConradRelais.SetActive: boolean;
begin
  result := SetActive(FOutputs);
end;

procedure TConradRelais.ShowComProperties;
begin
  mCom.Eigenschaften;
end;

function TConradRelais.WriteCom(aFrame: TConradRelaisCommandFrame; var aAnswer: TCharArray): boolean;
var
  xAnswer: TCharArray;
  xAnswerCount: integer;
  xmsg: string;
  i: integer;
begin
  {$ifdef DEBUG_COM}
    result := true;
  {$else}
    result := false;
    if CheckComOpen then begin
      result := mCom.WriteBinary(aFrame.Command);
      result := result and mCom.WriteBinary(aFrame.Address);
      result := result and mCom.WriteBinary(aFrame.Data);
      result := result and mCom.WriteBinary(aFrame.Checksum);

      if result then begin
        mCom.Delay(50);
        result := mCom.ReadBinary(xAnswerCount, aAnswer);
      end;
    end;    
  {$endif}
end;

function TConradRelais.CalculateCommand(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;
begin
  with result do begin
    Command  := ord(aCommand);
    Address  := FAddress;
    Data     := aData;
    Checksum := Command xor Address xor Data;
  end;// with result do begin
end;// function TConradRelais.CalculateCommand(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;

function TConradRelais.GetPortString: string;
begin
  result := mCom.CommStatus;
end;

procedure TConradRelais.SetComPort(const Value: integer);
begin
  mCom.Port := Value;
end;

function TConradRelais.GetComPort: integer;
begin
  result := mCom.Port;
end;

function TConradRelais.Init: boolean;
var
  xAnswer: TCharArray;
begin
  if not(FInitialized) then
    FInitialized := WriteCom(CalculateCommand(rcSetup, 0), xAnswer)
                    and CheckResult(xAnswer, rcSetup);
  result := FInitialized;
end;
//------------------------------------------------------------------------------
function TConradRelais.GetActive(aOutputs: TOutputSet): boolean;
var
  xPortByte: Byte;
  i: TOutput;
  xAnswer: TCharArray;
  xState : integer;
  //xMsg : String;
const cState = 2;
begin
  Result := FALSE;
  FillChar(xAnswer, sizeof(xAnswer), 0);
  try
    if Init then begin
      xPortByte := 0;

      for i:= o1 to FMaxOutput do begin
        if i in aOutputs then
          xPortByte := xPortByte or (1 shl ord(i));
      end;// for i:= Low(TOutput) to FMaxOutput do begin

      result := WriteCom(CalculateCommand(rcGetPort, xPortByte), xAnswer);
      result := result and CheckResult(xAnswer, rcGetPort);

      xState:= xAnswer[cState];
      if xState > 0 then
         Result := TRUE
      else
         Result := FALSE ;
     end;
   except
   end;
end;
//------------------------------------------------------------------------------

function TConradRelais.getComHandle: THandle;
begin
  result := mCom.handle;
end;

function TConradRelais.GetISCOMOpen: Boolean;
begin
  result := mCom.Opened;
end;


procedure TConradRelais.CloseCOM;
begin
  mCom.Close;
end;

{ TConradCom }

constructor TConradCom.Create(AOwner: TComponent);
begin
  inherited;
  
	CaptionProperties := 'Eigenschaften';
	CaptionOK         := 'OK';
  CaptionCancel     := 'Abbruch';
	CaptionAktual     := 'Aktualisieren';
  CaptionPort       := 'Port';
	CaptionForm       := 'Einstellungen';
end;

end.
 