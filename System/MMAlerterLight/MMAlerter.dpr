(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMAlerter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: - MMAlerter.DLL
| Info..........:
| Develop.system: Windows 2000 prof.
| Target.system.: W2k / XP / W2k3
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2004  0.01  Sdo | Projekt erstellt
|=============================================================================*)
library MMAlerter;


uses
  MemCheck,
  SysUtils,
  Classes,
  ActiveX,
  Windows,
  u_MMAlerter in 'u_MMAlerter.pas',
  u_AlerterConfig in 'u_AlerterConfig.pas',
  RelaisCard in 'RelaisCard.pas',
  ComLK in 'ComLK.pas' {Einstellungen};

{$R *.RES}
{$R 'Version.res'}

procedure DLLEntry(dwReason: DWORD);
const
  xResult: HResult = -1;
begin
  case dwReason of
    DLL_PROCESS_ATTACH: begin
        xResult := CoInitialize(nil);
        // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
        // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
        if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
          raise Exception.Create('CoInitialize failed');
      end; // DLL_PROCESS_ATTACH: begin
    DLL_PROCESS_DETACH: begin
        if (xResult = S_OK) or (xResult = S_FALSE) then
          CoUninitialize;
      end; // DLL_PROCESS_DETACH: begin
  end; // case dwReason of
end;


exports
  //Int. Name           ext. Name
  xp_QOfflimitAlarm     name 'xp_QOfflimitAlarm',
  xp_AlarmON            name 'xp_AlarmON',
  xp_AlarmOFF           name 'xp_AlarmOFF',

  xp_FlashLightTestON   name 'xp_FlashLightTestON',
  xp_FlashLightTestOFF  name 'xp_FlashLightTestOFF',
  xp_ConnectionCheck    name 'xp_ConnectionCheck',


  QOfflimitAlarm        name 'QOfflimitAlarm',
  ComPortCheck          name 'ComPortCheck',
  AlarmON               name 'AlarmON',
  AlarmOFF              name 'AlarmOFF';


begin


{$IFDEF MemCheck}
  MemChk('MMAlerterDLL');
{$ENDIF}


  DLLProc := @DLLEntry;
  // Process Attach erst mal von Hand aufrufen, da die Procedur erst nach dem
  // Eintritt des Prozesses gesetzt wird
  DLLEntry(DLL_PROCESS_ATTACH);
end.

