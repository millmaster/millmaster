(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMAlerterMain.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Main Applikation MMAlerter
|
| Info..........: MMAlerter.exe auf SRV und/oder WKS
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.04.2004  1.00  Sdo | Datei erstellt
|=============================================================================*)
unit u_MMAlerterMain;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Menus, mmMainMenu, ExtCtrls, mmImage, Buttons, mmSpeedButton,
  mmEventLog, ActiveX, Loepfeglobal, ImgList, mmImageList,
  ActnList, mmActionList, IvDictio, IvAMulti, IvBinDic, mmDictionary,
  IvMulti, IvEMulti, mmTranslator;

type
  TAlerterMain = class(TForm)
    sbQuitAlarm: TmmSpeedButton;
    mmMainMenu1: TmmMainMenu;
    miKonfiguration: TMenuItem;
    miQOfflimitAlarm: TMenuItem;
    miHardware: TMenuItem;
    miExit: TMenuItem;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acConfigQOfflimitAlarm: TAction;
    acConfigHardware: TAction;
    mmImageList1: TmmImageList;
    mmDictionary1: TmmDictionary;
    mmTranslator1: TmmTranslator;
    procedure sbQuitAlarmClick(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acConfigQOfflimitAlarmExecute(Sender: TObject);
    procedure acConfigHardwareExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    function GetShowHardwareMenue: Boolean;
  public
    { Public declarations }
  end;

 {
type


  TComPortCheck         = function  (aComPortNr: WORD): Boolean; stdcall;
  TAlarmON              = function: Boolean; stdcall;
  TAlarmOFF             = function: Boolean; stdcall;
  TQOfflimitAlarm       = function: Boolean; stdcall;
 }
var
  AlerterMain: TAlerterMain;

  {
  Dll_HModule : THandle;

  ComPortCheck        : TComPortCheck;
  AlarmON             : TAlarmON;
  AlarmOFF            : TAlarmOFF;
  QOfflimitAlarm      : TQOfflimitAlarm;
  }

  gLog: TEventLogWriter;


implementation

{$R *.DFM}

uses u_QOffAlarmConfig, u_HardwareConfig, mmRegistry, ADODBAccess, u_AlerterConfig;



//------------------------------------------------------------------------------
procedure TAlerterMain.sbQuitAlarmClick(Sender: TObject);
var xRelaisNr: Integer;
begin
  sbQuitAlarm.Down := TRUE;

  try
    //RelaisNr. Ermitteln
    xRelaisNr:= GetOfflimitRelais;

    //Alarm Quit
    with TADODBAccess.Create(1, TRUE) do begin
         try
            if Init then
               with Query[0] do begin
                    Close;
                    SQL.Text := cQuitQOfflimitAlarm;
                    ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
                    ExecSQL;
                    //Application.ProcessMessages;
               end;
         finally
            Free;
         end;
    end;
  except
  end;
  sbQuitAlarm.Down := FALSE;
end;
//------------------------------------------------------------------------------
procedure TAlerterMain.acExitExecute(Sender: TObject);
begin
  close;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Q-Offlimit Alarm Config. Fenster zeigen
//******************************************************************************
procedure TAlerterMain.acConfigQOfflimitAlarmExecute(Sender: TObject);
begin

  with TQOffAlarmConfig.Create(Self) do begin

       IsWKS := GetShowHardwareMenue;

       Left := Self.Left + 20;
       Top  := Self.Top + 50;

       if Top + Height > Screen.Height then
          Top := Top - Height;

       if Left + Width >  Screen.Width then
          Left :=   Left - Width;

       ShowModal;
       Free;
  end;

end;
//------------------------------------------------------------------------------

//******************************************************************************
// Hardware Config. Fenster zeigen
//******************************************************************************
procedure TAlerterMain.acConfigHardwareExecute(Sender: TObject);
begin

  with THardwareConfig.Create(Self) do begin

       Left := Self.Left + 20;
       Top  := Self.Top + 50;

       if Top + Height > Screen.Height then
          Top := Top - Height;

       if Left + Width >  Screen.Width then
          Left :=   Left - Width;

       ShowModal;
       Free;
  end;

end;
//------------------------------------------------------------------------------
function TAlerterMain.GetShowHardwareMenue: Boolean;
var xPCName, xMMHost : String;
begin
  Result := TRUE;

  xMMHost:= GetRegString(cRegLM, cRegMMCommonPath, cRegMMHost,'');
  xPCName :=MMGetComputerName;

  if xMMHost <> '.' then
     if CompareText(xMMHost, xPCName) = 0 then
        Result := TRUE
     else
        Result := FALSE;

  //Result := TRUE;
  {
  if not Result then
    with TmmRegistry.Create do try
      RootKey := cRegLM;
      if KeyExists(cAlertRegPath) then Result := TRUE;
    finally
      Free;
    end;
  }
end;
//------------------------------------------------------------------------------
procedure TAlerterMain.FormCreate(Sender: TObject);
begin
 mmDictionary1.FileName := 'MMAlerterConfig.mld';
 mmDictionary1.Init;
 mmTranslator1.Translate;
 if not GetShowHardwareMenue then begin
    acConfigHardware.Visible:= FALSE;
 end;
end;
//------------------------------------------------------------------------------


initialization

 CoInitialize(nil);

 gLog := TEventLogWriter.Create('MillMaster', '.', ssApplication, 'MillMaster Alerter: ', True);

 (*
 Dll_HModule := 0;

   try
     Dll_HModule := LoadLibrary( cDllName );
   except
      on E: Exception do  begin
         ShowMessage('Error in LoadLibrary(' +  cDllName + '). ' +  e.Message );
         Dll_HModule := 0;
      end;
   end;

   if Dll_HModule <> 0 then begin
       ComPortCheck           := GetProcAddress(Dll_HModule,'ComPortCheck');
       AlarmON                := GetProcAddress(Dll_HModule,'AlarmON');
       AlarmOFF               := GetProcAddress(Dll_HModule,'AlarmOFF');
       QOfflimitAlarm         := GetProcAddress(Dll_HModule,'QOfflimitAlarm');
   end;
  *)

finalization

   //FreeLibrary(Dll_HModule);
   gLog.Free;

   CoUnInitialize;
end.
