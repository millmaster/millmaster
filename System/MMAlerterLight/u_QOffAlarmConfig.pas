(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_QOffAlarmConfig.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Config. Q-Offlimit Alarm Window zu Applikation MMAlerter
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.04.2004  1.00  Sdo | Datei erstellt
|=============================================================================*)
unit u_QOffAlarmConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, mmBitBtn, mmCheckBox, mmNumCtrl, mmLabel, mmGroupBox,
  ADODBAccess, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  TQOffAlarmConfig = class(TForm)
    gbQOfflimitAlert: TmmGroupBox;
    lbMaxQOAlert: TmmLabel;
    neMaxQOAlert: TmmNumEdit;
    cbtActivateQOAlert: TmmCheckBox;
    gbAlertTimer: TmmGroupBox;
    lbSwitchOffTime: TmmLabel;
    neSwitchOffTime: TmmNumEdit;
    bSave: TmmBitBtn;
    bCancel: TmmBitBtn;
    mmTranslator1: TmmTranslator;
    cbQuitbyHand: TmmCheckBox;
    procedure bCancelClick(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure neMaxQOAlertEnter(Sender: TObject);
    procedure neMaxQOAlertExit(Sender: TObject);
    procedure neSwitchOffTimeEnter(Sender: TObject);
    procedure neSwitchOffTimeExit(Sender: TObject);
    procedure cbQuitbyHandClick(Sender: TObject);
    procedure neMaxQOAlertChange(Sender: TObject);
    procedure neSwitchOffTimeChange(Sender: TObject);
    procedure cbtActivateQOAlertClick(Sender: TObject);

  private
    { Private declarations }
    mADODBAccess : TADODBAccess;
    mExistsDBData : Boolean;
    fIsWKS: Boolean;
    mRelayNrChanged : Boolean;
    procedure SetIsWKS(const Value: Boolean);
    procedure Check_cbQuitbyHand;
  public
    { Public declarations }
    property IsWKS: Boolean read fIsWKS write SetIsWKS default TRUE;
  end;

var
  QOffAlarmConfig: TQOffAlarmConfig;




implementation

{$R *.DFM}
Uses u_AlerterConfig;

//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.bCancelClick(Sender: TObject);
begin
 if mRelayNrChanged then
    if AskSaveDLG = mrYes then begin
       bSave.Click;
    end;
 Close;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.bSaveClick(Sender: TObject);
var xMSG :String;
begin

  //Einstellungen auf DB schreiben
  with mADODBAccess.Query[0] do begin
     Close;

     //Table Check
     SQL.Text :=  cExistsTabt_MMAlerter;
     Open;
      if EOF then begin
         xMSG := Format( cTableNotExists, ['t_MMAlerter']);
         MessageDlg(xMSG, mtError, [mbOk], 0);
         exit;
      end;

     Close;
     if not mExistsDBData then begin
        SQL.Text := cInsertNewRelaisData;

        ParamByName('c_Relais_Nr').AsInteger     := GetOfflimitRelais;//cQOfflimitRelais;
        ParamByName('c_Active').AsBoolean        := cbtActivateQOAlert.Checked;
        ParamByName('c_MaxError').AsInteger      := neMaxQOAlert.AsInteger;
        ParamByName('c_ActualError').AsInteger   := 0;
        ParamByName('c_SwitchOffTime').AsInteger := neSwitchOffTime.AsInteger;
        ParamByName('c_SwitchOffAutomatic').AsBoolean := cbQuitbyHand.Checked;

        ExecSQL;
     end else begin
        SQL.Text := cUpdateRelaisData;

        ParamByName('c_Relais_Nr').AsInteger     := GetOfflimitRelais;//cQOfflimitRelais;
        ParamByName('c_Active').AsBoolean        := cbtActivateQOAlert.Checked;
        ParamByName('c_MaxError').AsInteger      := neMaxQOAlert.AsInteger;
        ParamByName('c_SwitchOffTime').AsInteger := neSwitchOffTime.AsInteger;
        ParamByName('c_SwitchOffAutomatic').AsBoolean := cbQuitbyHand.Checked;

        ExecSQL;
     end;

  end;
  mRelayNrChanged := FALSE;
  //close;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.FormCreate(Sender: TObject);
var xRelaisNr : Word;
    xMSG : String;
begin
  mExistsDBData := FALSE;

  mADODBAccess := TADODBAccess.Create(1);

  if mADODBAccess.Init then
     //Einstellungen von DB lesen
     with mADODBAccess.Query[0] do begin

          //Table Check
          SQL.Text :=  cExistsTabt_MMAlerter;
          Open;
          if EOF then begin
            xMSG := Format( cTableNotExists, ['t_MMAlerter']);
            MessageDlg(xMSG, mtError, [mbOk], 0);
            exit;
          end;

          //RelaisNr. Ermitteln
          Close;
          SQL.Text := cGetNewQOffRelaisNr;
          Open;
          if not Eof then begin
             xRelaisNr := FieldByName('Data').AsInteger;
          end else
             xRelaisNr:= cQOfflimitRelaisNr;

          //Relais Daten ermitteln
          Close;
          SQL.Text := cGetRelaisData;
          ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;

          Open;
          First;

          if not Eof then begin
             cbtActivateQOAlert.Checked := FieldByName('c_Active').AsBoolean;
             neSwitchOffTime.Value      := FieldByName('c_SwitchOffTime').AsInteger;
             neMaxQOAlert.Value         := FieldByName('c_MaxError').AsInteger;
             cbQuitbyHand.Checked       := FieldByName('c_SwitchOffAutomatic').AsBoolean;
             mExistsDBData := TRUE;
          end;
     end; //END with

   mRelayNrChanged := FALSE;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.FormDestroy(Sender: TObject);
begin
  mADODBAccess.Free;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neMaxQOAlertEnter(Sender: TObject);
begin
  neMaxQOAlert.Text:= StringReplace(neMaxQOAlert.Text, '--', '-',[rfReplaceAll]);
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neMaxQOAlertExit(Sender: TObject);
begin
  neMaxQOAlert.CheckValue;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neSwitchOffTimeEnter(Sender: TObject);
begin
  neSwitchOffTime.Text:= StringReplace(neSwitchOffTime.Text, '--', '-',[rfReplaceAll]);
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neSwitchOffTimeExit(Sender: TObject);
begin
  neSwitchOffTime.CheckValue;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.SetIsWKS(const Value: Boolean);
begin
  fIsWKS := Value;
  gbQOfflimitAlert.Enabled := fIsWKS;
  gbAlertTimer.Enabled := fIsWKS;
  bSave.Enabled := fIsWKS;

  cbQuitbyHand.Enabled := fIsWKS;
  lbSwitchOffTime.Enabled := fIsWKS;

  cbtActivateQOAlert.Enabled := fIsWKS;
  lbMaxQOAlert.Enabled := fIsWKS;     
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.cbQuitbyHandClick(Sender: TObject);
begin
  Check_cbQuitbyHand;
  mRelayNrChanged := TRUE;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.Check_cbQuitbyHand;
begin
 neSwitchOffTime.Enabled:=  not cbQuitbyHand.Checked;
 lbSwitchOffTime.Enabled:= neSwitchOffTime.Enabled;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neMaxQOAlertChange(Sender: TObject);
begin
  mRelayNrChanged := TRUE;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.neSwitchOffTimeChange(Sender: TObject);
begin
   mRelayNrChanged := TRUE;
end;
//------------------------------------------------------------------------------
procedure TQOffAlarmConfig.cbtActivateQOAlertClick(Sender: TObject);
begin
  mRelayNrChanged := TRUE;
end;
//------------------------------------------------------------------------------
end.
