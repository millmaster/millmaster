unit MainWindow;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, RelaisCard, Buttons;

type
  TThreadType = (ttNone, ttImpuls);
  
  TTimeThread = class(TThread)
  private
    FTimeLabel: TLabel;
    mRestTime: extended;
    FTime: integer;
    FThreadType: TThreadType;
  public
    procedure Execute;override;

    procedure UpdateTimeLabel;
    
    property TimeLabel: TLabel read FTimeLabel write FTimeLabel;
    property Time: integer read FTime write FTime;
    property ThreadType: TThreadType read FThreadType write FThreadType;
  end;// TImpulsThread = class(TThread)
  
  TfrmMainWindow = class(TForm)
    gbOutput: TGroupBox;
    cbOut1: TCheckBox;
    cbOut2: TCheckBox;
    cbOut3: TCheckBox;
    cbOut4: TCheckBox;
    cbOut5: TCheckBox;
    cbOut6: TCheckBox;
    cbOut7: TCheckBox;
    cbOut8: TCheckBox;
    bOn: TButton;
    bReset: TButton;
    bImpulse: TButton;
    edImpuls: TSpinEdit;
    Label1: TLabel;
    Label2: TLabel;
    bComPort: TButton;
    laRestTime: TLabel;
    bStopImpuls: TSpeedButton;
    procedure bResetClick(Sender: TObject);
    procedure bOnClick(Sender: TObject);
    procedure SelectOutput(Sender: TObject);
    procedure bComPortClick(Sender: TObject);
    procedure bImpulseClick(Sender: TObject);
    procedure ImpulsTerminate(Sender: TObject);
    procedure bStopImpulsClick(Sender: TObject);
    procedure SetOutputs(aOutputs: TOutputSet);
  private
    { Private declarations }
    mRelais: TConradRelais;
    mSelectedOutputs: TOutputSet;
    mImpulseThread: TTimeThread;
    mIniFilePath: string;

    function GetIniInteger(aValue: string; aDefault: integer): integer;
    procedure SetIniInteger(aValue: string; aInteger: integer);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    { Public declarations }
  end;

var
  frmMainWindow: TfrmMainWindow;

implementation

uses
  LKGlobal, filectrl;

{$R *.DFM}

const
  cGlobalSection = 'Global';
  
  cComPort    = 'ComPort';
  cImpulsTime = 'ImpulsTime';
  cOutputSet  = 'Outputs';
  
constructor TfrmMainWindow.Create(aOwner: TComponent);
var
  xByte: Byte;
  xPByte: PByte;
begin
  inherited;
  
  mRelais := TConradRelais.Create;

  mIniFilePath := ChangeFileExt(Application.ExeName, '.ini');

  mRelais.ComPort := GetIniInteger(cComPort, 2);
  edImpuls.Value :=  GetIniInteger(cImpulsTime, 120);
  
  xPByte := @mSelectedOutputs;
  xPByte^ := GetIniInteger(cOutputSet, 0);
  SetOutputs(mSelectedOutputs);
  
  bComPort.Caption := mRelais.PortString;
end;

destructor TfrmMainWindow.Destroy;
var
  xPByte: PByte;
begin
  SetIniInteger(cComPort, mRelais.ComPort);
  SetIniInteger(cImpulsTime, edImpuls.Value);
  xPByte := @mSelectedOutputs;
  SetIniInteger(cOutputSet, xPByte^);
  
  FreeAndNil(mRelais);
  inherited;
end;

procedure TfrmMainWindow.SetOutputs(aOutputs: TOutputSet);
begin
  if o1 in aOutputs then
    cbOut1.Checked := true;  
    
  if o2 in aOutputs then
    cbOut2.Checked := true;  
    
  if o3 in aOutputs then
    cbOut3.Checked := true;  
    
  if o4 in aOutputs then
    cbOut4.Checked := true;  
    
  if o5 in aOutputs then
    cbOut5.Checked := true;  
    
  if o6 in aOutputs then
    cbOut6.Checked := true;  
    
  if o7 in aOutputs then
    cbOut7.Checked := true;  
    
  if o8 in aOutputs then
    cbOut8.Checked := true;  
end;

function TfrmMainWindow.GetIniInteger(aValue: string; aDefault: integer): integer;
begin
  result := ReadIntegerFromIni(mIniFilePath, cGlobalSection, aValue, aDefault);
end;

procedure TfrmMainWindow.SetIniInteger(aValue: string; aInteger: integer);
begin
  WriteIntegerToIni(mIniFilePath, cGlobalSection, aValue, aInteger);
end;

procedure TfrmMainWindow.bResetClick(Sender: TObject);
begin
  if not(mRelais.ResetOutputs) then
    ShowMessage('Fehler beim Zurücksetzen der Outputs');
  bComPort.Caption := mRelais.PortString;
end;

procedure TfrmMainWindow.bOnClick(Sender: TObject);
begin
  if not(mRelais.SetActive(mSelectedOutputs)) then
    ShowMessage('Fehler beim Setzen der Ports');
  bComPort.Caption := mRelais.PortString;
end;

procedure TfrmMainWindow.SelectOutput(Sender: TObject);
begin
  if Sender is TCheckbox then begin
    with (Sender as TCheckbox) do begin
      if Checked then 
        include(mSelectedOutputs, TOutput(Tag))
      else
        exclude(mSelectedOutputs, TOutput(Tag));
    end;// with (Sender as TCheckbox) do begin
  end;// if Sender is TCheckbox then begin
end;

procedure TfrmMainWindow.bComPortClick(Sender: TObject);
begin
  mRelais.ShowComProperties;
  bComPort.Caption := mRelais.PortString;
end;

{ TImpulsThread }

procedure TTimeThread.Execute;
var
  xStart: cardinal;
begin
  mRestTime := FTime;
  xStart := GetTickCount;
  while not(Terminated) do begin
    Synchronize(UpdateTimeLabel);
    sleep(100);
    mRestTime := FTime - ((GetTickCount - xStart) / 1000);
    if mRestTime <= 0 then
      Terminate;
  end;// while not(Terminated) do begin
  mRestTime := FTime - ((GetTickCount - xStart) / 1000);
  Synchronize(UpdateTimeLabel);
end;

procedure TTimeThread.UpdateTimeLabel;
begin
  if assigned(FTimeLabel) then
    FTimeLabel.Caption := FloatToStr(mRestTime) + 's';
end;

procedure TfrmMainWindow.bImpulseClick(Sender: TObject);
begin
  if mRelais.SetActive(mSelectedOutputs) then begin
    bComPort.Caption := mRelais.PortString;

    mImpulseThread := TTimeThread.Create(true);
    with mImpulseThread do begin
      Time := edImpuls.Value;
      FreeOnTerminate := true;
      TimeLabel := laRestTime;
      OnTerminate := ImpulsTerminate;
      resume;
    end;
    bStopImpuls.Enabled := true;
  end else begin
    ShowMessage('Fehler beim Setzen des Outputs');
  end;
end;

procedure TfrmMainWindow.ImpulsTerminate(Sender: TObject);
begin
  bStopImpuls.Enabled := false;
  if not(mRelais.ResetOutputs) then
    ShowMessage('Fehler beim Zurücksetzen der Outputs');
end;

procedure TfrmMainWindow.bStopImpulsClick(Sender: TObject);
begin
  if assigned(mImpulseThread) then
    mImpulseThread.Terminate;
end;

end.
