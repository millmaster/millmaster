unit LKGlobal;

interface
uses
  windows, sysutils, classes, typinfo, syncobjs;

type
  // !!! WICHTIG !!! Neue Typen immer nur hinten anh�ngen
  TODSType = (odsNone, odsBool, odsBreak, odsError, odsInteger, odsFloat, odsNote, odsObject, 
              odsString, odsEnterMethode, odsLeaveMethode);

  //1 Stellt ein Klasse zu Verf�gung die Ausgaben in das Debugfenster umleitet 
  TLKSendDebugInfo = class (TObject)
  private
    FCritSect: TCriticalSection;
    FEnabled: Boolean;
    FHoldIndent: Boolean;
    FShowTree: Boolean;
    FStackLevel: Integer;
    FStackLevelIndent: Integer;
    //1 Setzt einen Headerstring zusammen 
    function BuildHeader(aType: TODSType): string;
    //1 R�ckt aCount Stack Level ein 
    function IndentStackLevel(aCount: integer): string;
    //1 Sendet einen String an die Funktion OutputDebugString 
    procedure SendToODS(aName:string; aText: string; aType: TODSType = odsNone);
  protected
    //1 Konstruktor (Singleton) 
    constructor CreateInstance;
    //1 Zugriff auf die einzige Instanz dieses Objektes 
    class function AccessInstance(Request: Integer): TLKSendDebugInfo;
  public
    //1 Konstruktor 
    constructor Create;
    //1 Destruktor 
    destructor Destroy; override;
    //1 F�gt eine Linie zum Log hinzu 
    procedure AddSeparator;
    //1 F�gt einen Eintrag zur globalen Liste hinzu 
    procedure AddToList(aType: TODSType; aName: string; aText: string);
    //1 CallStack f�r das Debug Window 
    procedure EnterMethode(aMsg: string);
    //1 Zugriff auf die einzige Instanz dieser Klasse (singleton) 
    class function Instance: TLKSendDebugInfo;
    //1 CallStack f�r das Debug Window 
    procedure LeaveMethode(aMsg: string);
    //1 Gibt die Instanz dieser Klasse frei (singleton) 
    class procedure ReleaseInstance;
    //1 Sendet einen Boolean Wert an das Debug Window 
    procedure SendBoolean(aName:string; aValue: boolean);
    //1 Sendet einen Boolean Wert an das Debug Window 
    procedure SendBooleanEx(aType: integer; aName:string; aValue: boolean);
    //1 F�gt einen Trennstrich in das Protokoll ein 
    procedure SendBreak;
    //1 Sendet einen Datumswert an das Debug Window 
    procedure SendDateTime(aName: string; aValue: TDateTime);
    //1 Sendet einen Datumswert an das Debug Window 
    procedure SendDateTimeEx(aType: integer; aName: string; aValue: TDateTime);
    //1 Sendet einen Fehlertext an das Debug Windows 
    procedure SendError(aValue: string);
    //1 Sendet einen Fehlertext an das Debug Windows 
    procedure SendErrorEx(aType: integer; aValue: string);
    //1 Sendet einen Fliesskommawert an das Debug Window 
    procedure SendFloat(aName:string; aValue: extended);
    //1 Sendet einen Fliesskommawert an das Debug Window 
    procedure SendFloatEx(aType: integer; aName:string; aValue: extended);
    //1 Sendet einen Integer an das Debug Window 
    procedure SendInteger(aName:string; aValue: integer);
    //1 Sendet einen Integer an das Debug Window 
    procedure SendIntegerEx(aType: integer; aName:string; aValue: integer);
    //1 Sendet eine String Message an das Debug Window 
    procedure SendMsg(aValue: string);
    //1 Sendet eine String Message an das Debug Window 
    procedure SendMsgEx(aType: integer; aValue: string);
    //1 Sendet einen Text an das Debug Window 
    procedure SendNote(aValue: string);
    //1 Sendet einen Text an das Debug Window 
    procedure SendNoteEx(aType: integer; aValue: string);
    //1 Sendet einen Integer an das Debug Window 
    procedure SendObject(aName:string; aValue: TPersistent; aRecursive: boolean = false);
    //1 Sendet einen Integer an das Debug Window 
    procedure SendObjectEx(aType: integer; aName:string; aValue: TPersistent; aRecursive: boolean = false);
    //1 Sendet einen String an das Debug Window 
    procedure SendString(aName:string; aValue: string);
    //1 Sendet einen String mit einer anderen Kennung 
    procedure SendStringEx(aType: integer; aName: string; aValue: string);
    property CritSect: TCriticalSection read FCritSect write FCritSect;
    //1 True wenn ein Output erfolgen soll 
    property Enabled: Boolean read FEnabled write FEnabled;
    //1 True, wenn die Ausgaben entsprechend dem Stacklevel einger�ckt werden sollen 
    property HoldIndent: Boolean read FHoldIndent write FHoldIndent;
    //1 True, wenn f�r jeden Stack Level ein '|' gezeichnet werden soll 
    property ShowTree: Boolean read FShowTree write FShowTree;
    //1 Level der Aufrufe 
    property StackLevel: Integer read FStackLevel;
    //1 Anzahl der Zeichen die pro Stack Level einger�ckt werden soll 
    property StackLevelIndent: Integer read FStackLevelIndent write FStackLevelIndent;
  end;
  
  //1 Gibt zugriff auf Laufzeitinformationen 
  TLkRttiInfo = class (TPersistent)
  private
    FIndent: Integer;
    mLevel: Integer;
    mPropInfo: string;
  protected
    FProps: PPropList;
    //1 Holt die Liste mit den Properties per RTTI 
    function GetProps(aInstance:TPersistent): Integer;
    //1 Schreibt alle Properties ins IniFile 
    procedure PropInfo(aInstance:TPersistent; aRecursive: boolean); virtual;
  public
    //1 Konstruktor 
    constructor Create; virtual;
    //1 Destruktor 
    destructor Destroy; override;
    //1 Gibt die RTTI Informationen des �bergebenen Objektes in einem String zur�ck 
    function GetPropInfo(aInstance: TPersistent; aRecursive: boolean = true): string; virtual;
    //1 Gibt die Anzahl Zeichen an um die bei verschachtelten Objekten einger�ckt werden soll 
    property Indent: Integer read FIndent write FIndent;
  end;
  

// Sendet einen String an die API Funktion OutputDebugString
procedure ODS(aMsg: string);
// Gibt die Instanz von TLKSendDebugInfo zur�ck (singleton)
function LKODS: TLKSendDebugInfo;
// Gibt eine Anzahl Leerzeichen zur�ck
function Space(aCount: integer): String;

// Liest einen Integer aus dem Inifile
function ReadIntegerFromIni(aFileName: string; aSection: string; aValue:string; aDefault:Integer): integer;
// Schreibt einen Integer in das Inifile
procedure WriteIntegerToIni(aFileName: string; aSection: string; aValue:string; aInteger:Integer);

const
  cDefaultConnectionString = 'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Data Source=%s;Initial Catalog=%s%s%s';
  
  cr   = #13;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;
  cCrlf = crlf;
  
  // Alle Bezeichner m�ssen gleich lang sein
  cIDLength      = 5;
  // L�nge des Teils der die Anzahl Zeichen angibt
  cCounterLength = 6;
  cHeaderLength  = '7';
  cCountID       = 'Size:';  // Anzahl der Header Eintr�ge
  cTypeID        = 'Type:';  // Typ des Textes
  cTimeStampID   = 'TStp:';  // Zeitstempel des Sendens
  cTID           = 'ThID:';  // Thread ID
  cPID           = 'PzID:';  // Process ID
  cNameID        = 'Name:';  // Kennung f�r den zu sendeneden Namen
  cTextID        = 'Text:';  // Zu sendender Text
  cLevelID       = 'Levl:';  // Darzustellender Level

  csmError = 0;
  
type
  PODSHeader = ^TODSHeader;
  TODSHeader = record
    TypeID    : TODSType;   // Typ des Textes
    TimeStamp : TDateTime;  // Zeitstempel des Sendens
    TID       : Cardinal;   // Thread ID
    PID       : Cardinal;   // Process ID
  end;// TODSHeader = record

  PLinkListEntry = ^TLinkListEntry;
  TLinkListEntry = record
    ReadyToSend : boolean;         // True sobald die Daten bereit sind
    ODSHeader   : PODSHeader;      // Header mit Zusatzinformationen
    Name        : string;          // Name des Eintrages (Variable oder Kennung)
    Text        : string;          // Meldung
    Level       : integer;         // Einr�ckung
    Next        : PLinkListEntry;  // Zeiger auf das n�chste Element der Liste
  end;// TLinkListEntry = record
  
var
  gSQLServerName: string = 'LKRANERMOBIL';
  gDBName: string = 'Innolab_Versuch';
  gDBPassword: string;
  gDBUserName: string;

implementation

uses
  inifiles;

type
  //1 Dieser Thread sendet dauernd die aktuellen Eintr�ge die ind er Liste gesammelt werden 
  TODSSendThread = class (TThread)
  private
    //1 Setzt einen Headerstring zusammen 
    function BuildString(aLinkListEntry: PLinkListEntry): string;
    //1 Sendet einen String an das DebugWindow (Win API) 
    procedure ODS(aTextToSend: string);
  public
    //1 Threadfunktion 
    procedure execute; override;
  end;
  
  TLinkedODSList = class (TObject)
  private
    FCriticalSection: TCriticalSection;
    FFirstLinkedEntry: PLinkListEntry;
    FLastLinkedEntry: PLinkListEntry;
    function GetEntryExists: Boolean;
  protected
    constructor CreateInstance;
    class function AccessInstance(Request: Integer): TLinkedODSList;
    //1 F�gt einen Eintrag zur Lsite hinzu 
    function AddEntry: PLinkListEntry;
    //1 L�scht den ersten Eintrag aus der Liste 
    procedure DeleteEntry;
  public
    constructor Create;
    destructor Destroy; override;
    //1 Holt den ersten Eintrag 
    function Get: PLinkListEntry;
    class function Instance: TLinkedODSList;
    //1 Legt einen neuen Eintrag am Ende der Liste an 
    procedure Put(aType: TODSType; aName: string; aText: string; aStackLevel: integer);
    class procedure ReleaseInstance;
    //1 Critical Section f�r den Zugriff auf die Liste 
    property CriticalSection: TCriticalSection read FCriticalSection write FCriticalSection;
    //1 True, wenn mindestens ein Eintrag vorhanden ist 
    property EntryExists: Boolean read GetEntryExists;
    //1 Erster Eintrag 
    property FirstLinkedEntry: PLinkListEntry read FFirstLinkedEntry write FFirstLinkedEntry;
    //1 Letzter Eintrag 
    property LastLinkedEntry: PLinkListEntry read FLastLinkedEntry write FLastLinkedEntry;
  end;
  

var
  lListCriticalSection : TCriticalSection = nil;
  ODSSendThread        : TODSSendThread = nil;

function GetString(aString: string):string;
var
   xCount: string;
begin
  xCount := format('%' + IntToStr(cCounterLength) + 'd',[Length(aString)]);
  result := xCount + aString;
end;// function GetString(aString: string):string;

// Singleton
function ODSList: TLinkedODSList;
begin
  Result := TLinkedODSList.Instance;
end;// function ODSList: TLinkedODSList;

procedure ODS(aMsg: string);
begin
  OutputDebugString(PChar(aMsg));
end;// procedure ODS(aMsg: string);

function LKODS: TLKSendDebugInfo;
begin
  Result := TLKSendDebugInfo.Instance;
end;// function LKSD: TLKSendDebugInfo;

function Space(aCount: integer): String;
var
  i:integer;
begin
  Result := '';
  for i := 1 to aCount do
    result := result + ' ';
end;// function Space(aCount: integer): String;

// Liest einen Integer aus dem Inifile
function ReadIntegerFromIni(aFileName: string; aSection: string; aValue:string; aDefault:Integer): integer;
begin
  result := aDefault;
  with TIniFile.Create(aFileName) do try
    result := ReadInteger(aSection, aValue, aDefault);
  finally
    free;
  end;// with TIniFile.Create(aFileName) do try
end;

// Schreibt einen Integer in das Inifile
procedure WriteIntegerToIni(aFileName: string; aSection: string; aValue:string; aInteger:Integer);
begin
  with TIniFile.Create(aFileName) do try
    WriteInteger(aSection, aValue, aInteger);
  finally
    free;
  end;// with TIniFile.Create(aFileName) do try
end;

//1 Stellt ein Klasse zu Verf�gung die Ausgaben in das Debugfenster umleitet 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TLKSendDebugInfo.Create;
begin
  inherited Create;
  
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TLKSendDebugInfo.Create cat:No category

//:-------------------------------------------------------------------
constructor TLKSendDebugInfo.CreateInstance;
begin
  inherited Create;
  
  FStackLevel       := 0;
  FStackLevelIndent := 4;
  FShowTree         := false;
  FEnabled          := true;
  FHoldIndent       := true;
  FCritSect         := TCriticalSection.Create;
end;// TLKSendDebugInfo.CreateInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TLKSendDebugInfo.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);
  
  FreeAndNil(FCritSect);
  
  inherited Destroy;
end;// TLKSendDebugInfo.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           AccessInstance
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (Request)
 *
 *  Kurzbeschreibung: Zugriff auf die einzige Instanz dieses Objektes
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TLKSendDebugInfo.AccessInstance(Request: Integer): TLKSendDebugInfo;
  const FInstance: TLKSendDebugInfo = nil;
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TLKSendDebugInfo.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddSeparator
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�gt eine Linie zum Log hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.AddSeparator;
begin
  if not(FEnabled) then
    exit;
  
  ODS('------------------------------------------------------');
end;// TLKSendDebugInfo.AddSeparator cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddToList
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aText)
 *
 *  Kurzbeschreibung: F�gt einen Eintrag zur globalen Liste hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.AddToList(aType: TODSType; aName: string; aText: string);
var
  xLinkListEntry: PLinkListEntry;
begin
  CritSect.Enter;
  try
    ODSList.Put(aType, aName, aText, FStackLevel);
  finally
    CritSect.Leave;
  end;// try finally
end;// TLKSendDebugInfo.AddToList cat:No category

//:-------------------------------------------------------------------
(*: Member:           BuildHeader
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType)
 *
 *  Kurzbeschreibung: Setzt einen Headerstring zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLKSendDebugInfo.BuildHeader(aType: TODSType): string;
var
  xTemp: string;
begin
  result := cCountID + GetString(cHeaderLength);
  
  result := result + cTypeID + GetString(IntToStr(ord(aType)));
  result := result + cTimeStampID + GetString(DatetimeToStr(now));
  result := result + cTID + GetString(IntToStr(GetCurrentThreadID));
  result := result + cPID + GetString(IntToStr(GetCurrentProcessID));
end;// TLKSendDebugInfo.BuildHeader cat:No category

//:-------------------------------------------------------------------
(*: Member:           EnterMethode
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aMsg)
 *
 *  Kurzbeschreibung: CallStack f�r das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.EnterMethode(aMsg: string);
var
  xMsg: string;
begin
  if not(FEnabled) then
    exit;
  xMsg := IndentStackLevel(FStackLevel);
  inc(FStackLevel);
  xMsg := xMsg + '-->: ' + aMsg;
  SendToODS('', xMsg, odsEnterMethode);
end;// TLKSendDebugInfo.EnterMethode cat:No category

//:-------------------------------------------------------------------
(*: Member:           IndentStackLevel
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aCount)
 *
 *  Kurzbeschreibung: R�ckt aCount Stack Level ein
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLKSendDebugInfo.IndentStackLevel(aCount: integer): string;
var
  i: Integer;
begin
  result := '';
  
  if FStackLevelIndent < 2 then
    FShowTree := false;
  
  for i := 1 to aCount do begin
    if FShowTree then
      result := result + '|' + Space(FStackLevelIndent - 1)
    else
      result := result + Space(FStackLevelIndent);
  end;// for i := 1 to aCount do begin
end;// TLKSendDebugInfo.IndentStackLevel cat:No category

//:-------------------------------------------------------------------
(*: Member:           Instance
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Zugriff auf die einzige Instanz dieser Klasse (singleton)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class function TLKSendDebugInfo.Instance: TLKSendDebugInfo;
begin
  Result := AccessInstance(1);
end;// TLKSendDebugInfo.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           LeaveMethode
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aMsg)
 *
 *  Kurzbeschreibung: CallStack f�r das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.LeaveMethode(aMsg: string);
var
  xMsg: string;
begin
  if not(FEnabled) then
    exit;
  xMsg := '';
  
  if FStackLevel > 0 then
    dec(FStackLevel);
  
  xMsg := IndentStackLevel(FStackLevel);
  
  xMsg := xMsg + '<--: ' + aMsg;
  SendToODS('', xMsg, odsLeaveMethode);
end;// TLKSendDebugInfo.LeaveMethode cat:No category

//:-------------------------------------------------------------------
(*: Member:           ReleaseInstance
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Gibt die Instanz dieser Klasse frei (singleton)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
class procedure TLKSendDebugInfo.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TLKSendDebugInfo.ReleaseInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendBoolean
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Boolean Wert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendBoolean(aName:string; aValue: boolean);
begin
  if not(FEnabled) then
    exit;
  
  if aValue then
    SendToODS(aName, 'True')
  else
    SendToODS(aName, 'False');
end;// TLKSendDebugInfo.SendBoolean cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendBooleanEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Boolean Wert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendBooleanEx(aType: integer; aName:string; aValue: boolean);
begin
  SendBoolean(aName, aValue);
end;// TLKSendDebugInfo.SendBooleanEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendBreak
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�gt einen Trennstrich in das Protokoll ein
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendBreak;
begin
  if not(FEnabled) then
    exit;
  
  SendToODS('','-----------------------------------------------------');
end;// TLKSendDebugInfo.SendBreak cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendDateTime
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Datumswert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendDateTime(aName: string; aValue: TDateTime);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS('', DateTimeToStr(aValue));
end;// TLKSendDebugInfo.SendDateTime cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendDateTimeEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Datumswert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendDateTimeEx(aType: integer; aName: string; aValue: TDateTime);
begin
  SendDateTime(aName, aValue);
end;// TLKSendDebugInfo.SendDateTimeEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendError
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aValue)
 *
 *  Kurzbeschreibung: Sendet einen Fehlertext an das Debug Windows
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendError(aValue: string);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS('', aValue, odsError);
end;// TLKSendDebugInfo.SendError cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendErrorEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Fehlertext an das Debug Windows
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendErrorEx(aType: integer; aValue: string);
begin
  SendError(aValue);
end;// TLKSendDebugInfo.SendErrorEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendFloat
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Fliesskommawert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendFloat(aName:string; aValue: extended);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS(aName, FloatToStr(aValue), odsFloat);
end;// TLKSendDebugInfo.SendFloat cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendFloatEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Fliesskommawert an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendFloatEx(aType: integer; aName:string; aValue: extended);
begin
  SendFloat(aName, aValue);
end;// TLKSendDebugInfo.SendFloatEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendInteger
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Integer an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendInteger(aName:string; aValue: integer);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS(aName, IntToStr(aValue), odsInteger);
end;// TLKSendDebugInfo.SendInteger cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendIntegerEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Integer an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendIntegerEx(aType: integer; aName:string; aValue: integer);
begin
  SendInteger(aName, aValue);
end;// TLKSendDebugInfo.SendIntegerEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendMsg
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aValue)
 *
 *  Kurzbeschreibung: Sendet eine String Message an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendMsg(aValue: string);
begin
  if not(FEnabled) then
    exit;
  
  SendString('', aValue);
end;// TLKSendDebugInfo.SendMsg cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendMsgEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aValue)
 *
 *  Kurzbeschreibung: Sendet eine String Message an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendMsgEx(aType: integer; aValue: string);
begin
  SendMsg(aValue);
end;// TLKSendDebugInfo.SendMsgEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendNote
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aValue)
 *
 *  Kurzbeschreibung: Sendet einen Text an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendNote(aValue: string);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS('', aValue, odsNote);
end;// TLKSendDebugInfo.SendNote cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendNoteEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aValue)
 *
 *  Kurzbeschreibung: Sendet einen Text an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendNoteEx(aType: integer; aValue: string);
begin
  SendNote(aValue);
end;// TLKSendDebugInfo.SendNoteEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendObject
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue, aRecursive)
 *
 *  Kurzbeschreibung: Sendet einen Integer an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendObject(aName:string; aValue: TPersistent; aRecursive: boolean = false);
begin
  if not(FEnabled) then
    exit;
  
  with TLKRttiInfo.Create do try
    SendToODS(aName, GetPropInfo(aValue, aRecursive), odsObject);
  finally
    free;
  end;// with TLKRttiInfo.Create do try
end;// TLKSendDebugInfo.SendObject cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendObjectEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue, aRecursive)
 *
 *  Kurzbeschreibung: Sendet einen Integer an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendObjectEx(aType: integer; aName:string; aValue: TPersistent; aRecursive: boolean = false);
begin
  SendObject(aName, aValue, aRecursive);
end;// TLKSendDebugInfo.SendObjectEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendString
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen String an das Debug Window
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendString(aName:string; aValue: string);
begin
  if not(FEnabled) then
    exit;
  
  SendToODS(aName, aValue, odsString);
end;// TLKSendDebugInfo.SendString cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendStringEx
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aValue)
 *
 *  Kurzbeschreibung: Sendet einen String mit einer anderen Kennung
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendStringEx(aType: integer; aName: string; aValue: string);
begin
  SendString(aName, aValue);
end;// TLKSendDebugInfo.SendStringEx cat:No category

//:-------------------------------------------------------------------
(*: Member:           SendToODS
 *  Klasse:           TLKSendDebugInfo
 *  Kategorie:        No category 
 *  Argumente:        (aName, aText, aType)
 *
 *  Kurzbeschreibung: Sendet einen String an die Funktion OutputDebugString
 *  Beschreibung:     
                      Der endg�ltige String setzt sich aus einem Header, dem 
                      Namen und dem Text zusammen.
 --------------------------------------------------------------------*)
procedure TLKSendDebugInfo.SendToODS(aName:string; aText: string; aType: TODSType = odsNone);
var
  xMsg: string;
begin
  if not(FEnabled) then
    exit;
  
  xMsg := '';
  if FHoldIndent then
    xMsg := IndentStackLevel(FStackLevel);
  // ODS(xMsg + aName + ' = ' + aText);
  AddToList(aType, aName, aText);
end;// TLKSendDebugInfo.SendToODS cat:No category

//1 Gibt zugriff auf Laufzeitinformationen 
//:-------------------------------------------------------------------
(*: Member:           Create
 *  Klasse:           TLkRttiInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Konstruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
constructor TLkRttiInfo.Create;
begin
  FProps:=nil;
  FIndent := 4;
end;// TLkRttiInfo.Create cat:No category

//:-------------------------------------------------------------------
(*: Member:           Destroy
 *  Klasse:           TLkRttiInfo
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Destruktor
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
destructor TLkRttiInfo.Destroy;
begin
  // Property Liste freigeben
  if FProps <> nil then
    FreeMem(FProps);
  inherited;
end;// TLkRttiInfo.Destroy cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetPropInfo
 *  Klasse:           TLkRttiInfo
 *  Kategorie:        No category 
 *  Argumente:        (aInstance, aRecursive)
 *
 *  Kurzbeschreibung: Gibt die RTTI Informationen des �bergebenen Objektes in einem String zur�ck
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLkRttiInfo.GetPropInfo(aInstance: TPersistent; aRecursive: boolean = true): string;
begin
  mLevel    := 0;
  mPropInfo := '';
  try
    PropInfo(aInstance, aRecursive);
  finally
    result := mPropInfo;
  end;// try finally
end;// TLkRttiInfo.GetPropInfo cat:No category

//:-------------------------------------------------------------------
(*: Member:           GetProps
 *  Klasse:           TLkRttiInfo
 *  Kategorie:        No category 
 *  Argumente:        (aInstance)
 *
 *  Kurzbeschreibung: Holt die Liste mit den Properties per RTTI
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLkRttiInfo.GetProps(aInstance:TPersistent): Integer;
var
  TypeData: PTypeData;
begin
  // Initialisieren
  result:=0;
  // Liste freigeben, wenn bereits belegt
  if FProps<>nil then begin
    FreeMem(FProps);
    FProps:=nil;
  end;// if FProps<>nil then begin
  
  // Informationen zur Klasse holen und aussteigen wenn die Infos ung�ltig sind
  TypeData := GetTypeData(aInstance.ClassInfo);
  if (TypeData = nil) or (TypeData^.PropCount = 0) then
    Exit;
  
  // Speicher reservieren
  GetMem(FProps, TypeData^.PropCount * sizeof(Pointer));
  // Holt Infos zu den Properties
  GetPropInfos(aInstance.ClassInfo, FProps);
  // Anzahl der Properties zur�ckgeben
  result:=TypeData^.PropCount;
end;// TLkRttiInfo.GetProps cat:No category

//:-------------------------------------------------------------------
(*: Member:           PropInfo
 *  Klasse:           TLkRttiInfo
 *  Kategorie:        No category 
 *  Argumente:        (aInstance, aRecursive)
 *
 *  Kurzbeschreibung: Schreibt alle Properties ins IniFile
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLkRttiInfo.PropInfo(aInstance:TPersistent; aRecursive: boolean);
var
  xPropCount: Integer;
  i: Integer;
  xValue: Variant;
  xName: Variant;
begin
  // Liste der Properties abfragen und in FProps ablegen
  xPropCount := GetProps(aInstance);
  // Durch alle gefundenen Properties
  for i := 0 to xPropCount-1 do begin
    with FProps^[i]^ do begin
      // Typen die von Variant in String gewandelt werden k�nnen
      case PropType^^.Kind of
        tkInteger,
        tkChar,
        tkEnumeration,
        tkFloat,
        tkString,
        tkSet,
        tkWChar,
        tkLString,
        tkWString,
        tkVariant,
        tkRecord,
        tkInt64: begin
          xName  := FProps^[i]^.Name;
          varCast(xValue, GetPropValue(aInstance, xName), varString);
          // Wert des Properties in das Ini File schreiben
          mPropInfo := mPropInfo + Space(FIndent * mLevel) + xName + ' = ' + xValue + crlf
        end;// tkInteger, tkChar, ...
  
        // Eigenschaft ist eine Klasse
        tkClass:begin
          if aRecursive then begin
            // Level erh�hen
            inc(mLevel);
            // Funktion rekursiv aufrufen f�r die Klasse des Properties
            PropInfo(TPersistent(GetObjectProp(aInstance,Name)), aRecursive);
            // Level wieder zur�cksetzen
            dec(mLevel);
            // Liste mit den Properties der aktuellen Instanz erneuern
            GetProps(aInstance);
          end else begin
            mPropInfo := mPropInfo + Name + ' = Class' + crlf;
          end;// if aRecursive then begin
        end;// tkClass:begin
      end;// case PropType^^.Kind of
    end;// with FProps^[I]^ do begin
  end;// for i := 0 to xPropCount-1 do begin
end;// TLkRttiInfo.PropInfo cat:No category

//1 Dieser Thread sendet dauernd die aktuellen Eintr�ge die ind er Liste gesammelt werden 
//:-------------------------------------------------------------------
(*: Member:           BuildString
 *  Klasse:           TODSSendThread
 *  Kategorie:        No category 
 *  Argumente:        (aLinkListEntry)
 *
 *  Kurzbeschreibung: Setzt einen Headerstring zusammen
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TODSSendThread.BuildString(aLinkListEntry: PLinkListEntry): string;
begin
  (*result := cCountID + ',';
  result := result + cTypeID + IntToStr(ord(aLinkListEntry^.ODSHeader^.TypeID)) + ',';
  result := result + cTimeStampID + DatetimeToStr(aLinkListEntry^.ODSHeader^.TimeStamp) + ',';
  result := result + cTID + IntToStr(aLinkListEntry^.ODSHeader^.TID) + ',';
  result := result + cPID + IntToStr(aLinkListEntry^.ODSHeader^.PID) + ',';
  result := result + cNameID + aLinkListEntry^.Name + ',';
  result := result + cTextID + aLinkListEntry^.Text + ',';
  result := result + cLevelID + IntToStr(aLinkListEntry^.Level);
  *)
  
  result := cCountID + GetString(cHeaderLength);
  
  result := result + cTypeID + GetString(IntToStr(ord(aLinkListEntry^.ODSHeader^.TypeID)));
  result := result + cTimeStampID + GetString(DatetimeToStr(aLinkListEntry^.ODSHeader^.TimeStamp));
  result := result + cTID + GetString(IntToStr(aLinkListEntry^.ODSHeader^.TID));
  result := result + cPID + GetString(IntToStr(aLinkListEntry^.ODSHeader^.PID));
  result := result + cNameID + GetString(aLinkListEntry^.Name);
  result := result + cTextID + GetString(aLinkListEntry^.Text);
  result := result + cLevelID + GetString(IntToStr(aLinkListEntry^.Level));
end;// TODSSendThread.BuildString cat:No category

//:-------------------------------------------------------------------
(*: Member:           execute
 *  Klasse:           TODSSendThread
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Threadfunktion
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TODSSendThread.execute;
begin
  while not(terminated) do begin
    sleep(1000);
    while (ODSList.EntryExists) do begin
      // Wenn der Eintrag noch nicht bereit ist, weiter probieren
      if not(ODSList.Get^.ReadyToSend) then
        break;
  
      try
        lListCriticalSection.Enter;
        ODS(BuildString(ODSList.Get));
        ODSList.DeleteEntry;
      finally
        lListCriticalSection.Leave;
      end;// try finally
    end;// while (ODSList.EntryExists) do begin
  
  end;// while not(terminated) do begin
end;// TODSSendThread.execute cat:No category

//:-------------------------------------------------------------------
(*: Member:           ODS
 *  Klasse:           TODSSendThread
 *  Kategorie:        No category 
 *  Argumente:        (aTextToSend)
 *
 *  Kurzbeschreibung: Sendet einen String an das DebugWindow (Win API)
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TODSSendThread.ODS(aTextToSend: string);
begin
  OutputDebugString(PChar(aTextToSend));
end;// TODSSendThread.ODS cat:No category

//:-------------------------------------------------------------------
constructor TLinkedODSList.Create;
begin
  inherited Create;

//: ----------------------------------------------
  raise Exception.CreateFmt('Access class %s through Instance only', [ClassName]);
end;// TLinkedODSList.Create cat:No category

//:-------------------------------------------------------------------
constructor TLinkedODSList.CreateInstance;
begin
  inherited Create;

//: ----------------------------------------------
  FFirstLinkedEntry := nil;
  FLastLinkedEntry  := nil;

//: ----------------------------------------------
  assert(lListCriticalSection <> nil,'Critical Section vorher erzeugen');
  
  FCriticalSection := lListCriticalSection;
end;// TLinkedODSList.CreateInstance cat:No category

//:-------------------------------------------------------------------
destructor TLinkedODSList.Destroy;
begin
  if AccessInstance(0) = Self then AccessInstance(2);

//: ----------------------------------------------
  inherited Destroy;
end;// TLinkedODSList.Destroy cat:No category

//:-------------------------------------------------------------------
class function TLinkedODSList.AccessInstance(Request: Integer): TLinkedODSList;
  const FInstance: TLinkedODSList = nil;
begin
  case Request of
    0 : ;
    1 : if not Assigned(FInstance) then FInstance := CreateInstance;
    2 : FInstance := nil;
  else
    raise Exception.CreateFmt('Illegal request %d in AccessInstance', [Request]);
  end;
  Result := FInstance;
end;// TLinkedODSList.AccessInstance cat:No category

//:-------------------------------------------------------------------
(*: Member:           AddEntry
 *  Klasse:           TLinkedODSList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: F�gt einen Eintrag zur Lsite hinzu
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLinkedODSList.AddEntry: PLinkListEntry;
begin
  result := nil;

//: ----------------------------------------------
  // Speicher holen
  new(result);
  result^.Next := nil;
  result^.ReadyToSend := false;
  new(result^.ODSHeader);

//: ----------------------------------------------
  try
    // Code Segment sch�tzen
    FCriticalSection.Enter;
    // Anfang der Liste
    if not(assigned(FFirstLinkedEntry)) then
      FFirstLinkedEntry := result;
  
    // Dem letzten Eintrag einen verweis auf den n�chsten anh�ngen
    if assigned(FLastLinkedEntry)then
      FLastLinkedEntry^.Next := result;
    // Ende der Lsite
    FLastLinkedEntry := result;
  finally
    // Die CriticalSection wieder freigeben
    FCriticalSection.Leave;
  end;// try finally
end;// TLinkedODSList.AddEntry cat:No category

//:-------------------------------------------------------------------
(*: Member:           DeleteEntry
 *  Klasse:           TLinkedODSList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: L�scht den ersten Eintrag aus der Liste
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLinkedODSList.DeleteEntry;
var
  xLinkListEntry: PLinkListEntry;
begin
    try
      // CodeSegment sch�tzen
      FCriticalSection.Enter;
      // ersten Eintrag holen
      xLinkListEntry := FFirstLinkedEntry;
  
      // Wenn ein Eintrag existiert, dann den Eintrag freigeben
      if assigned(xLinkListEntry) then begin
        // zweites Element als Anfang der Lsite
        FFirstLinkedEntry := xLinkListEntry^.Next;
        // Wenn der letzte Eintrag freigegeben wird, Zeiger auf das Ende der Lsite anpassen
        if not(assigned(FFirstLinkedEntry)) then
          FLastLinkedEntry := nil;
  
        // ODSHeader freigeben
        Dispose(xLinkListEntry^.ODSHeader);
        // Entry freigeben
        Dispose(xLinkListEntry);
      end;// if assigend(xLinkListEntry) then begin
    finally
      // Die CriticalSection wieder freigeben
      FCriticalSection.Leave;
    end;// try finally
end;// TLinkedODSList.DeleteEntry cat:No category

//:-------------------------------------------------------------------
(*: Member:           Get
 *  Klasse:           TLinkedODSList
 *  Kategorie:        No category 
 *  Argumente:        
 *
 *  Kurzbeschreibung: Holt den ersten Eintrag
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
function TLinkedODSList.Get: PLinkListEntry;
begin
  result := FFirstLinkedEntry;
end;// TLinkedODSList.Get cat:No category

//:-------------------------------------------------------------------
function TLinkedODSList.GetEntryExists: Boolean;
begin
  result := FFirstLinkedEntry <> nil;
end;// TLinkedODSList.GetEntryExists cat:No category

//:-------------------------------------------------------------------
class function TLinkedODSList.Instance: TLinkedODSList;
begin
  Result := AccessInstance(1);
end;// TLinkedODSList.Instance cat:No category

//:-------------------------------------------------------------------
(*: Member:           Put
 *  Klasse:           TLinkedODSList
 *  Kategorie:        No category 
 *  Argumente:        (aType, aName, aText, aStackLevel)
 *
 *  Kurzbeschreibung: Legt einen neuen Eintrag am Ende der Liste an
 *  Beschreibung:     
                      -
 --------------------------------------------------------------------*)
procedure TLinkedODSList.Put(aType: TODSType; aName: string; aText: string; aStackLevel: integer);
var
  xLinkListEntry: PLinkListEntry;
begin
  // Einen neuen Eintrag holen und direkt in die Liste einf�gen
  xLinkListEntry := AddEntry;
  try
    with xLinkListEntry^.ODSHeader^ do begin
      TypeID    := aType;               // Typ des Textes
      TimeStamp := now;                 // Zeitstempel des Sendens
      TID       := GetCurrentThreadID;  // Thread ID
      PID       := GetCurrentProcessID; // Process ID
    end;// xLinkListEntry^.ODSHeader^
  
    with xLinkListEntry^ do begin
      Name  := aName;
      Text  := aText;
      Level := aStackLevel;
    end;// with xLinkListEntry^ do begin
  finally
    xLinkListEntry.ReadyToSend := true;
  end;// try finally
end;// TLinkedODSList.Put cat:No category

//:-------------------------------------------------------------------
class procedure TLinkedODSList.ReleaseInstance;
begin
  AccessInstance(0).Free;
end;// TLinkedODSList.ReleaseInstance cat:No category


initialization
  lListCriticalSection := TCriticalSection.Create;
  ODSSendThread := TODSSendThread.Create(true);
  ODSSendThread.FreeOnTerminate := true;
  ODSSendThread.resume;

finalization
  TLKSendDebugInfo.ReleaseInstance;
  FreeAndNil(lListCriticalSection);
  ODSSendThread.Terminate;

end.
