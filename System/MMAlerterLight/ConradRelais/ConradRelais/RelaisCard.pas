unit RelaisCard;

interface

uses
  classes, ComLK, sysutils, windows;

type
  ERelaisException = class(Exception);
  
  TOutput = (o1, o2 , o3, o4, o5, o6, o7, o8);
  TOutputSet = Set of TOutput;

  TRelaisCommand = (rcNoOp, rcSetup, rcGetPort, rcSetPort, rcGetOption, rcSetOption);

  TConradRelaisCommandFrame = record
    Command  : Byte;
    Address  : Byte;
    Data     : Byte;
    Checksum : Byte;
  end;// TConradRelaisCommandFrame = record

  TConradCom = class(TComLK)
  public
    constructor Create(AOwner: TComponent);override;
  end; 
  
  TConradRelais = class
  private
    mCom: TComLK;
    FOutputs: TOutputSet;
    FAddress: Byte;
    FMaxOutput: TOutput;

    function CheckComOpen: boolean;
    function WriteCom(aFrame: TConradRelaisCommandFrame): boolean;
    function CalculateCommandString(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;
    function GetPortString: string;
    procedure SetComPort(const Value: integer);
    function GetComPort: integer;
  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure ShowComProperties;
    
    procedure SetActive(aOutputs: TOutputSet); overload;
    procedure SetActive; overload;
    procedure ResetOutputs;

    property Outputs: TOutputSet read FOutputs write FOutputs;
    property Address:Byte read FAddress write FAddress;
    property ComPort: integer read GetComPort write SetComPort;
    property PortString: string read GetPortString;
  end;// TConradRelais = class

implementation

uses
  math;

{ TConradRelais }

function TConradRelais.CheckComOpen: boolean;
begin
  {$ifdef DEBUG_COM}
    result := true;
    exit;
  {$endif}

	result:=false;					// Initialisieren
  if assigned(mCom) then begin
    with mCom do begin
      // Wenn die Schnittstelle noch nicht offen ist, dann �ffnen
      if not(opened) then
        open;     
                                            
      // Nochmals versuchen
      if not(opened) then
        open;                                         
      result:=opened;                                 // true wenn Schnittstelle offen ist
    end;//with FComm do begin
  end;// if assigned(mCom) then begin
  
  if not(result) then                               // Fehler wenn die Schnittstelle nicht ge�ffnet werden kann
    raise ERelaisException.Create(Format('TConradRelais.CheckComOpen: Konnte Port nicht �ffnen (Settings = %s)', [mCom.PortString]));
end;

constructor TConradRelais.Create;
begin
  inherited;
  mCom := TConradCom.Create(nil);  
  with mCom do begin
    Baud := 19200;
    Parity := paNone;
    DataBits := 8;
    StopBits := sb1_0;

  end;
  
  FAddress := 1;
  FMaxOutput := o8;
end;

destructor TConradRelais.Destroy;
begin
  FreeAndNil(mCom);
  inherited;
end;

procedure TConradRelais.SetActive(aOutputs: TOutputSet);
var
  xPortByte: Byte;
  i: TOutput;
begin
  xPortByte := 0;
  for i:= o1 to FMaxOutput do begin
    if i in aOutputs then 
      xPortByte := xPortByte or (1 shl ord(i));
  end;// for i:= Low(TOutput) to FMaxOutput do begin
  
  WriteCom(CalculateCommandString(rcSetPort, xPortByte));  
end;

procedure TConradRelais.ResetOutputs;
begin
  SetActive([]);
end;

procedure TConradRelais.SetActive;
begin
  SetActive(FOutputs);
end;

procedure TConradRelais.ShowComProperties;
begin
  mCom.Eigenschaften;
end;

function TConradRelais.WriteCom(aFrame: TConradRelaisCommandFrame): boolean;
begin
  {$ifdef DEBUG_COM}
    result := true;
  {$else}
    result := false;
    if CheckComOpen then begin
      result := mCom.WriteBinary(aFrame.Command);
      result := result and mCom.WriteBinary(aFrame.Address);
      result := result and mCom.WriteBinary(aFrame.Data);
      result := result and mCom.WriteBinary(aFrame.Checksum);
    end;    
  {$endif}
end;

function TConradRelais.CalculateCommandString(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;
begin
  with result do begin
    Command  := ord(aCommand);
    Address  := FAddress;
    Data     := aData;
    Checksum := Command xor Address xor Data;
  end;// with result do begin
end;// function TConradRelais.CalculateCommandString(aCommand: TRelaisCommand; aData: Byte): TConradRelaisCommandFrame;

function TConradRelais.GetPortString: string;
begin
  result := mCom.CommStatus;
end;

procedure TConradRelais.SetComPort(const Value: integer);
begin
  mCom.Port := Value;
end;

function TConradRelais.GetComPort: integer;
begin
  result := mCom.Port;
end;

{ TConradCom }

constructor TConradCom.Create(AOwner: TComponent);
begin
  inherited;
  
	CaptionProperties := 'Eigenschaften';
	CaptionOK         := 'OK';
  CaptionCancel     := 'Abbruch';
	CaptionAktual     := 'Aktualisieren';
  CaptionPort       := 'Port';
	CaptionForm       := 'Einstellungen';
end;

end.
 