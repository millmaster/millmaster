(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMAlerter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: - MMAlerter.DLL
| Info..........:
| Develop.system: Windows 2000 prof.
| Target.system.: W2k / XP / W2k3
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2004  0.01  Sdo | Projekt erstellt
| 16.07.2004  0.02  Sdo | User Privileg eingebaut
|=============================================================================*)
unit u_MMAlerter;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, SrvStrucDef,
  mmEventLog, ADODBAccess, u_AlerterConfig, RelaisCard, ComLK,
  mmThread, syncobjs ;

type


  TAutoSwitchOFFTimer = class(TmmThread)
  private
    mEnduranceTime : DWord;
    mComHandle : THandle;

    procedure SetEnduranceTimeAsMsSec(const Value: DWord);
    procedure SetComHandle(const Value: THandle);

  public
    procedure Execute; override;
    constructor Create(aEnduranceTimeAsMsSec : DWord ); overload;
    constructor Create; overload;
    destructor Destroy; override;
    property EnduranceTimeAsMsSec : DWord read mEnduranceTime write SetEnduranceTimeAsMsSec;
    property ComHandle: THandle write SetComHandle;
  end;


  TCOMPortAlerter = Class(TConradRelais)
  private
    mEventLogWriter : TEventLogWriter;
    procedure SetEventLog(const Value: TEventLogWriter);
    function GetEventLog: TEventLogWriter;
  public
    constructor Create; override;
    destructor Destroy; override;
    property EventLog : TEventLogWriter read GetEventLog write SetEventLog;
  end;


//Interne Hilfsfunktionen
function IsRelaisActive: Boolean;
function GetComPort: Integer;
function FlashLightOFF: Boolean; //Relais ausschalten


// *** Exportierte Funktionen f�r den SQL-Server ***
function xp_QOfflimitAlarm(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_AlarmON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_AlarmOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;

//- GUI Funktionen
function xp_FlashLightTestON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_FlashLightTestOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_ConnectionCheck(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;




// *** Exportierte Funktionen f�r die Testapplikation ***
function QOfflimitAlarm: Boolean; stdcall;
function ComPortCheck(aComPortNr: WORD): Boolean; stdcall;
function AlarmON: Boolean; stdcall;
function AlarmOFF: Boolean; stdcall;


function SetPrivileg: Boolean;


var

  lAutoSwitchOFFTimer : TAutoSwitchOFFTimer;

  lAlerterCriticalSection : TCriticalSection;
  lAutoSwitchOFFTimerCriticalSection : TCriticalSection;
  lEventLogCriticalSection : TCriticalSection;

  lIntFuncCriticalSection : TCriticalSection;

  lCOMPortAlerter : TCOMPortAlerter;

implementation

uses
  mmCS, LoepfeGlobal, mmRegistry, ActiveX, RzCSIntf;



//******************************************************************************
// User Privileg fuer Funktion setzen -> logon as
//******************************************************************************
function SetPrivileg: Boolean;
var
  {xToken,} xUserToken: THandle;
  //xBufLen: DWord;
  //xTp: TTokenPrivileges;
  xUsername, xPassword0, xPassword1, xDomain: PChar;
  xError : DWORD;
begin
  Result := True;
  SetLastError(0);



  xUsername := 'MMSystemUser';
  xPassword0 := 'tub5zoe';   //alt
  xPassword1 := 'Sup5erMM';  //neu
  xDomain := '';

  //Der LogonUser muss bereits das Privileg SE_TCB_NAME besitzen !!!

  //Logon mit neuem PWD
  if LogonUser(xUsername, xDomain, xPassword1, LOGON32_LOGON_INTERACTIVE,
               LOGON32_PROVIDER_DEFAULT, xUserToken) then begin
    Result := ImpersonateLoggedOnUser(xUserToken);
    CloseHandle(xUserToken);
    CodeSite.SendMsg('MMAlerter.dll,  MMSystemUser Logon with new PWD in SetPrivileg(). ')
  end else begin
    SetLastError(0);
    //Logon mit altem PWD
    if LogonUser(xUsername, xDomain, xPassword0, LOGON32_LOGON_INTERACTIVE,
               LOGON32_PROVIDER_DEFAULT, xUserToken) then begin
       Result := ImpersonateLoggedOnUser(xUserToken);
       CloseHandle(xUserToken);
       CodeSite.SendMsg('MMAlerter.dll,  MMSystemUser Logon with old PWD in SetPrivileg(). ')
     end else begin
        xError := GetLastError();
        CodeSite.SendError('MMAlerter.dll,  Error in SetPrivileg(). Error msg: ' + FormatErrorText(xError) );
        Result := False;
     end;
  end;
end;
//------------------------------------------------------------------------------
function ConvertBoolean(aValue: Boolean): Integer;
begin
  if aValue then Result := 1
  else Result := 0;
end;
//******************************************************************************
// Exportierte Funktionen f�r den SQL-Server
//******************************************************************************
function xp_QOfflimitAlarm(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT
begin
  lAlerterCriticalSection.Enter;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_QOfflimitAlarm() ');
    Result := ConvertBoolean(QOfflimitAlarm);
  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------
function xp_AlarmON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  lAlerterCriticalSection.Enter;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_AlarmON() ');
    Result := ConvertBoolean(AlarmON);
  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------
function xp_AlarmOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  lAlerterCriticalSection.Enter;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_AlarmOFF() ');
    Result := ConvertBoolean(AlarmOFF);
  finally
    lAlerterCriticalSection.Leave;
  end;
  //lLog.Write(etInformation, 'Quit alert by hand');
end;
//------------------------------------------------------------------------------

//- GUI Funktionen
//******************************************************************************
//Flashlight on
//******************************************************************************
function xp_FlashLightTestON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  lAlerterCriticalSection.Enter;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_FlashLightTestON() ');
    Result := ConvertBoolean(AlarmON);
  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Flashlight off
//******************************************************************************
function xp_FlashLightTestOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  lAlerterCriticalSection.Enter;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_FlashLightTestOFF() ');
    Result := ConvertBoolean(FlashLightOFF);
  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Com Port Check -> Aufruf von DB via GUI; wird fuer alle COM-Ports aufgerufen
//******************************************************************************
function xp_ConnectionCheck(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
var xComPort : Word;
    xRet : Boolean;
begin
  lAlerterCriticalSection.Enter;
  xRet := FALSE;
  try
    CodeSite.SendMsg('MMAlerter.dll,  Call xp_ConnectionCheck() ');
    SetPrivileg;
    //Get ComPort aus Tab.
      try
        with TADODBAccess.Create(1, TRUE) do begin
             try
                if Init then
                   with Query[0] do begin

                        //Com-Port aus Tab. t_MMAlerterTempSettings lesen, welche vom GUI in DB geschrieben wurde
                        Close;
                        SQL.Text := cGetFlashLightTestResult;
                        Open;
                        First;
                        xComPort :=FieldByName('c_ComPort').AsInteger;

                        //Com-Port check
                        xRet   := ComPortCheck(xComPort);

                        //Result in Tab. t_MMAlerterTempSettings schreiben; Result wird vom GUI ausgelesen
                        Close;
                        SQL.Text := cSetFlashLightTestResult;
                        ParamByName('c_ComPort').AsInteger := xComPort;
                        ParamByName('c_Result').AsBoolean := xRet;
                        ExecSQL;
                   end;
             finally
                Free;
             end;
        end;
      except
      end;
     Result := ConvertBoolean(xRet);
  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------



//******************************************************************************
// Exportierte Funktionen f�r die Test-Applikation
//******************************************************************************
function ComPortCheck(aComPortNr: WORD): Boolean; stdcall;
var
  xMsg : String;
begin
  Result := FALSE;
  lAlerterCriticalSection.Enter;
  try
    SetPrivileg;

    lCOMPortAlerter.ComPort := aComPortNr;

    if not lCOMPortAlerter.Init then begin
       xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [lCOMPortAlerter.ComPort]);
       lCOMPortAlerter.EventLog.Write(etError, xMsg);
       exit;
    end;

    Result := lCOMPortAlerter.SetActive;

  finally
    lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------
function AlarmOFF: Boolean; stdcall;
begin

  Result := FALSE;
  lAlerterCriticalSection.Enter;
  try
      SetPrivileg;
      try
        //Timer stoppen
        if Assigned(lAutoSwitchOFFTimer) then
           if not lAutoSwitchOFFTimer.Suspended then begin
              lAutoSwitchOFFTimer.Terminate;
              lAutoSwitchOFFTimer:=NIL;
           end;
      except
      end;

      try
        if IsRelaisActive then begin
           //lLog.Write(etInformation, 'Quit alert by hand');
           lCOMPortAlerter.EventLog.Write(etInformation, 'Quit alert by hand');
           Result := FlashLightOFF;
        end else Result := TRUE;
      except
      end;

  finally
      lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------
function AlarmON: Boolean; stdcall;
var
  xSelectedOutput: TOutput;
  xMsg: string;
  xRet: Boolean;
begin
  Result := False;

  lAlerterCriticalSection.Enter;
  try
      SetPrivileg;
      lCOMPortAlerter.ComPort := GetComPort;

      if not lCOMPortAlerter.Init then begin
         xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [lCOMPortAlerter.ComPort]);
         lCOMPortAlerter.EventLog.Write(etError, xMsg);
         lAlerterCriticalSection.Leave;
         exit;
      end;

      lCOMPortAlerter.ResetOutputs;

      try
        xSelectedOutput := TOutput(GetOfflimitRelais - 1) ;
      except
        begin
          xSelectedOutput := TOutput(0);
          lCOMPortAlerter.EventLog.Write(etWarning, 'Default relay 1 is active');
        end;
      end;

      try
        xRet := lCOMPortAlerter.SetActive([xSelectedOutput]);
      except
        xRet := False;
      end;

      if not xRet then begin
        xMsg := Format('No connection to relay card on Com-Port %d for alert test', [lCOMPortAlerter.ComPort]);
        lCOMPortAlerter.EventLog.Write(etWarning, xMsg);
      end;

      Result := xRet;
  finally
     lAlerterCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------
function QOfflimitAlarm: Boolean; stdcall;
var xAdoDBAccess: TAdoDBAccess;
    xSwitchOffTime: DWord;
    xRelaisNr : Integer;
    xMsg : String;
    xQuitByHand, xRet :  Boolean;
begin
  xRet := TRUE;
  xQuitByHand := FALSE;
  lAlerterCriticalSection.Enter;
  try
    SetPrivileg;
    try

      xSwitchOffTime := cSwitchOffTimeValue * 1000; // 5 Sec.
      xRelaisNr := GetOfflimitRelais;
      xQuitByHand := FALSE;

      xAdoDBAccess := TAdoDBAccess.Create(1, True);

      if not xAdoDBAccess.Init then
         lCOMPortAlerter.EventLog.Write(etWarning, 'TAdoDBAccess.Init = FALSE')
      else
        with xAdoDBAccess.Query[cPrimaryQuery] do try
          Close;

          SQL.Text := cGetRelaisData;
          ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
          Open;
          First;

          if not EOF then begin
            xSwitchOffTime := FieldByName('c_SwitchOffTime').AsInteger * 1000;
            xQuitByHand    := FieldByName('c_SwitchOffAutomatic').AsBoolean;

          end else begin
            xMsg := Format('Worng or no relay number (%d) in database found.', [xRelaisNr]);
            lCOMPortAlerter.EventLog.Write(etError, xMsg);
          end;

         xAdoDBAccess.Free;

        except
          on E: Exception do
            lCOMPortAlerter.EventLog.Write(etError, 'TAdoDBAccess error : ' + e.Message);
        end;

      AlarmON;

      lCOMPortAlerter.EventLog.Write(etInformation, 'Q-Offlimit alert');


      if xQuitByHand then
         xSwitchOffTime := 0;

      try
        if Assigned(lAutoSwitchOFFTimer) then begin
           lAutoSwitchOFFTimer.Terminate;
           lAutoSwitchOFFTimer.WaitFor;
           lAutoSwitchOFFTimer:= NIL;
        end;
           //Timer erstellen
           lAutoSwitchOFFTimer:= TAutoSwitchOFFTimer.Create(xSwitchOffTime);
           lAutoSwitchOFFTimer.ComHandle := lCOMPortAlerter.ComHandle;
           lAutoSwitchOFFTimer.Resume;
      finally
      end;



(*
      if not xQuitByHand then begin
         //Alarm wird autom. quittiert
         try
            if Assigned(lAutoSwitchOFFTimer) then begin
               lAutoSwitchOFFTimer.Terminate;
               lAutoSwitchOFFTimer.WaitFor;
               lAutoSwitchOFFTimer:= NIL;
            end;

            //Timer erstellen
            lAutoSwitchOFFTimer:= TAutoSwitchOFFTimer.Create(xSwitchOffTime);
            lAutoSwitchOFFTimer.ComHandle := lCOMPortAlerter.ComHandle;
            lAutoSwitchOFFTimer.Resume;
         except
         end;
      end;
*)
    except
        on E: Exception do begin
              lCOMPortAlerter.EventLog.Write(etError, 'QOfflimitAlarm error : ' + e.Message);
              xRet := FALSE;
        end;
    end;
  finally
    lAlerterCriticalSection.Leave;
  end;
  Result := xRet;
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Alarm Off; ohne Log-Event
//******************************************************************************
function FlashLightOFF: Boolean;
var
  xMsg: string;
  xRet: Boolean;
begin

  Result := FALSE;

  lIntFuncCriticalSection.Enter;
  try
      SetPrivileg;
      lCOMPortAlerter.ComPort := GetComPort;

      if not lCOMPortAlerter.Init then begin
         xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [lCOMPortAlerter.ComPort]);
         lCOMPortAlerter.EventLog.Write(etError, xMsg);
         exit;
      end;

      try
        xRet := lCOMPortAlerter.ResetOutputs; //Reset Relais
      except
        xRet := False;
      end;

      if not xRet then begin
        xMsg := Format('No connection to relay card on Com-Port %d for Quit alert', [lCOMPortAlerter.ComPort]);
        //lLog.Write(etWarning, xMsg);
        lCOMPortAlerter.EventLog.Write(etWarning, xMsg);
      end;

      Result := xRet;
  finally
      lIntFuncCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------

function GetComPort: Integer;
var
  xComPort, xPort, x: Integer;
  xList : TStringlist;
begin
  xPort := 0;
  lIntFuncCriticalSection.Enter;
  try
      SetPrivileg;
      xComPort := GetRegInteger(cRegLM, cAlertRegPath, cComPort, 0);

      if xComPort <= 0 then begin
         //Kein Com Port in Reg. definiert, muss jetzt ermittelt werden
         xList := TStringlist.Create;
         xList.Sorted := TRUE;
         xList.Duplicates := dupIgnore;

         //Alle Com Ports ermitteln
         xList.CommaText := StringReplace(GetComPortsListAsCommaText, 'COM', '',[rfReplaceAll]);
         for x:= 0 to xList.Count-1 do begin
             try
               xPort := StrToInt(xList.Strings[x]);
             except
               xPort := x+1;
             end;
             if ComPortCheck(xPort) then break;
         end;
         xComPort := xPort;

         //Aktiv Com Port in Reg. schreiben
         SetRegInteger(cRegLM, cAlertRegPath, cComPort, xComPort);

         xList.Free;
      end;

      Result := xComPort;

  finally
      lIntFuncCriticalSection.Leave;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermmitelt den Status vom Q-Offlimit Relais (An/Aus)
//******************************************************************************
function IsRelaisActive: Boolean;
var
  xMsg : String;
  xSelectedOutput : TOutput;
begin
  Result := FALSE;

  lIntFuncCriticalSection.Enter;
  try
      SetPrivileg;
      try
        xSelectedOutput := TOutput( GetOfflimitRelais - 1 );
      except
        begin
          xSelectedOutput := TOutput(0);
          lCOMPortAlerter.EventLog.Write(etWarning, 'Default relay 1 is active');
        end;
      end;

      try

        lCOMPortAlerter.ComPort := GetComPort;

        if not lCOMPortAlerter.Init then begin
           xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [lCOMPortAlerter.ComPort]);
           lCOMPortAlerter.EventLog.Write(etError, xMsg);
           exit;
        end;

        Result := lCOMPortAlerter.GetActive([xSelectedOutput]);

      except
         on E: Exception do begin
            lCOMPortAlerter.EventLog.Write( etError, 'Error in IsRelaisActive. Errro msg: ' + e.Message  );
         end;
      end;

  finally
     lIntFuncCriticalSection.Leave;
  end;
end;

//------------------------------------------------------------------------------




{ TAutoSwitchOFFTimer }
//------------------------------------------------------------------------------
constructor TAutoSwitchOFFTimer.Create(aEnduranceTimeAsMsSec: DWord);
begin
  mEnduranceTime := aEnduranceTimeAsMsSec;
  FreeOnTerminate := TRUE;

  //Create and not start
  inherited Create(TRUE);
end;
//------------------------------------------------------------------------------
constructor TAutoSwitchOFFTimer.Create;
begin
  FreeOnTerminate := TRUE;
  //Create and not start
  inherited Create(TRUE);
end;
//------------------------------------------------------------------------------
destructor TAutoSwitchOFFTimer.Destroy;
begin
  inherited;
  lAutoSwitchOFFTimer := NIL;
end;
//------------------------------------------------------------------------------
procedure TAutoSwitchOFFTimer.Execute;
var xSwitchOffByHand : Boolean;
    xStatus: Cardinal;  //Status der Inputleitungen
    //xTime : DWord;// Int64;
    xStopTime, xStartTime : DWord;
    //xCounter :DWord;

//const
//    Max_DWORD = 4294967295;
begin


  try
      xSwitchOffByHand := mEnduranceTime <= 0;


      xStartTime := GetTickCount;
      xStopTime  := xStartTime + mEnduranceTime; // Overflow:  Werte immer > 0
      //Bsp. Wertebereich von x : 0..100,  x= 90  y = 50 -> 90 + 50 = 40

      //xTime := xStartTime;

      //xCounter := 0;
      while not Terminated do  begin
         sleep(1);
         if not xSwitchOffByHand then begin

            //xTime := GetTickCount - xTime;  //Differenz
            //inc(xCounter, xTime);

            if GetTickCount >= xStopTime then begin
            //if xCounter > mEnduranceTime then begin
               //AutoswitchOff Zeit abgelaufen
               try
                 if IsRelaisActive then
                    lCOMPortAlerter.EventLog.Write(etInformation, 'Q-Offlimit alert automatic switch off');
               except
                  on E: Exception do begin
                     lCOMPortAlerter.EventLog.Write(etWarning,  'Error : Q-Offlimit alert automatic switch off; ' + e.Message );
                  end;
               end;
               FlashLightOFF;
               Terminate;
            end;

            {
            //DSR von RS232 dedektieren
            if (GetCommModemStatus( Cardinal(mComHandle), xStatus)) then begin
               if (xStatus and MS_DSR_ON) > 0  then begin
                 lCOMPortAlerter.EventLog.Write(etInformation, 'Quit alert by hand (Hardware)');
                 FlashLightOFF;
                 Terminate;
               end;
            end;
            }
         end;

         //DSR von RS232 dedektieren
         if (GetCommModemStatus( Cardinal(mComHandle), xStatus)) then begin
            if (xStatus and MS_DSR_ON) > 0  then begin
                lCOMPortAlerter.EventLog.Write(etInformation, 'Quit alert by hand (Hardware)');
                FlashLightOFF;
                Terminate;
             end;
         end;
      end;//END While

  finally
  end;
end;
//------------------------------------------------------------------------------
procedure TAutoSwitchOFFTimer.SetComHandle(const Value: THandle);
begin
  mComHandle := Value;
end;

procedure TAutoSwitchOFFTimer.SetEnduranceTimeAsMsSec(const Value: DWord);
begin
  mEnduranceTime := Value;
end;
//------------------------------------------------------------------------------




{ TCOMPortAlerter }
constructor TCOMPortAlerter.Create;
begin
  inherited;
  mCom.RTS := true;
  try
    mEventLogWriter :=  TEventLogWriter.Create('MillMaster', '.', ssApplication, 'MillMaster Alerter: ', True);;
    Init;
    CloseCOM;
  except
    on e: Exception do begin
       mEventLogWriter.Write( etError, 'Error in TCOMPortAlerter.Create. Error msg: ' + e.Message );
    end;
  end;
end;
//------------------------------------------------------------------------------
destructor TCOMPortAlerter.Destroy;
begin
  mEventLogWriter.Free;
  inherited;
end;
//------------------------------------------------------------------------------
function TCOMPortAlerter.GetEventLog: TEventLogWriter;
begin
 lEventLogCriticalSection.Enter;
 try
    Result := mEventLogWriter;
 finally
   lEventLogCriticalSection.Leave;
 end;
end;
//------------------------------------------------------------------------------
procedure TCOMPortAlerter.SetEventLog(const Value: TEventLogWriter);
begin
 lEventLogCriticalSection.Enter;
 try
   mEventLogWriter := Value;
 finally
   lEventLogCriticalSection.Leave;
 end;
end;
//------------------------------------------------------------------------------


initialization


  // beim 1. Aufruf von xp_ Funktionen wird diese Instanz erstellt

  lAutoSwitchOFFTimer := NIL;

  lCOMPortAlerter := TCOMPortAlerter.Create;

  lAlerterCriticalSection  := TCriticalSection.Create;
  lEventLogCriticalSection := TCriticalSection.Create;
  lIntFuncCriticalSection  := TCriticalSection.Create;

finalization

  if Assigned(lAutoSwitchOFFTimer) then begin
     lAutoSwitchOFFTimer.Terminate;
     lAutoSwitchOFFTimer.WaitFor;
  end;
  lAutoSwitchOFFTimer := NIL;


  lCOMPortAlerter.Free;


  lAlerterCriticalSection.Free;
  lEventLogCriticalSection.Free;
  lIntFuncCriticalSection.Free;

end.

