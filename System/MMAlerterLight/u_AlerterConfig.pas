(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_AlerterConfig.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: gemeinsame Daten zu Applikation MMAlerter
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.04.2004  1.00  Sdo | Datei erstellt
|=============================================================================*)
unit u_AlerterConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, LoepfeGlobal;


const

   //Settings
   //++++++++

   cDllName = 'MMAlerter.dll';

   cMsgAlerter = 'MMAlerter';

   //Defaults
   cMaxQOfflimitAlarmValue = 3;
   cSwitchOffTimeValue     = 5; // sec.    

   //Registry
   cAlertRegPath           = cRegMillMasterPath + '\Alerter';
   cComPort                = 'ComPort';
   cQOfflimitRelais        = 'QOfflimitRelais';

   //Definition Relais
   cQOfflimitRelaisNr      = 1;

   //Diverses
   cLRRT = #10#13;  //new line


   //Settings aus GUI
   cLightON  = -10;
   cLightOFF = -20;
   cComCheck = -30;

   //Settings aus DLL
   cLightOFFDLL  = '0';

   //SQL
   cGetRelaisData = 'select * from t_MMAlerter with (READUNCOMMITTED) where c_Relais_Nr = :c_Relais_Nr';

   cInsertNewRelaisData =
     'insert t_MMAlerter ( c_Relais_Nr, c_Active, c_ActualError, c_MaxError, c_SwitchOffTime, c_SwitchOffAutomatic ) ' +
     'values ( :c_Relais_Nr, :c_Active, :c_ActualError, :c_MaxError, :c_SwitchOffTime, :c_SwitchOffAutomatic )';


   cUpdateRelaisData =
      'update t_MMAlerter set c_Active = :c_Active, c_MaxError = :c_MaxError, c_SwitchOffTime =  :c_SwitchOffTime, c_SwitchOffAutomatic = :c_SwitchOffAutomatic ' +
      'where c_Relais_Nr = :c_Relais_Nr ';

   //cUpdateRelaisNr = 'update t_MMAlerter set c_Relais_Nr = :c_Relais_NrNew where c_Relais_Nr = :c_Relais_NrOld';
   cUpdateRelaisNr = 'update t_MMAlerter set c_Relais_Nr = :c_Relais_NrNew';


   cInsertNewQOffRelaisNr =
     'insert t_MMUParm (AppName, AppKey, Data) ' +
             'values ( ''MMAlerter'', ''QOfflimitRelais'', :Data)';

   cUpdateNewQOffRelaisNr =
     'update t_MMUParm set Data = :Data where AppKey = ''QOfflimitRelais'' ';

   cGetNewQOffRelaisNr =
     'select Data from t_MMUParm where AppName = ''MMAlerter'' and AppKey = ''QOfflimitRelais'' ';


   cQuitQOfflimitAlarm =
     'update t_MMAlerter set c_ActualError = ' + cLightOFFDLL + ' where c_Relais_Nr = :c_Relais_Nr ';


   //Hilfs-Tabellen fuer GUI
   //Master Tab. -> Connection check
   cSetComPort =
      'update t_MMAlerterTempSettings set c_Result = 0, c_ComPort = :c_ComPort';

   cFlashLightTest =
      'update t_MMAlerter set c_ActualError = :c_ActualError '; //where c_Relais_Nr = :c_Relais_Nr';

   cFlashLightTestAndRelais =
      'update t_MMAlerter set c_ActualError = :c_ActualError where c_Relais_Nr = :c_Relais_Nr';

   //Master Tab. -> Return Werte fuer GUI
   cGetFlashLightTestResult =  'select * from t_MMAlerterTempSettings '; //where c_Relais_Nr >= :c_Relais_Nr';

   cSetFlashLightTestResult =
      'update t_MMAlerterTempSettings set c_Result = :c_Result, c_ComPort = :c_ComPort';

   cExistsTabt_MMAlerter = 'select name c_TableName from dbo.sysobjects where id = object_id(N''[dbo].[t_MMAlerter]'')' ;

function GetComPortsListAsCommaText: String;
function GetOfflimitRelais: Integer;
function AskSaveDLG: Word;


resourcestring
  cSaveMSG           = '(*)Wollen Sie den geaenderten Wert speichern?'; //ivlm
  cTableNotExists    = '(*)Tabelle %s existiert nicht!'; //ivlm

implementation

uses Registry, ADODBAccess, ActiveX;


//------------------------------------------------------------------------------

//******************************************************************************
//Ermittelt  alle COM-Ports aus der Reg. als Commatext aus
//******************************************************************************
function GetComPortsListAsCommaText: String;
var i:integer;
    xSerialList, xSerialPortList : TStringlist;
    xKey : String;
begin
 xSerialList :=  TStringlist.Create;
 xSerialList.Sorted := TRUE;
 xSerialList.Duplicates := dupIgnore;

 xSerialPortList :=  TStringlist.Create;
 xSerialPortList.Sorted := TRUE;
 xSerialPortList.Duplicates := dupIgnore;

 xKey := 'HARDWARE\DEVICEMAP\SERIALCOMM';
 with TRegistry.create do
  try
   Rootkey:= cRegLM;
   if Keyexists(xKey) then
        if Openkey(xKey, False)then begin
           GetValueNames(xSerialList);

           for i:=0 to xSerialList.Count-1 do
               xSerialPortList.Add( ReadString(xSerialList.Strings[i]) );
        end;
  finally
    free;
  end;

  Result := xSerialPortList.CommaText;

  xSerialPortList.Free;
  xSerialList.Free;
end;
//------------------------------------------------------------------------------

//******************************************************************************
//Liest die Q-Offlimit Relais Nr. aus
//******************************************************************************
function GetOfflimitRelais: Integer;
var xADODBAccess : TADODBAccess;
begin
  result := cQOfflimitRelaisNr;

  try
     xADODBAccess := TADODBAccess.Create(1);
     xADODBAccess.Init;

     Application.ProcessMessages;
     with xADODBAccess.Query[0] do begin
       //RelaisNr. Ermitteln
       Close;
       SQL.Text := cGetNewQOffRelaisNr;
       Open;
       Application.ProcessMessages;
       if not Eof then begin
          result := FieldByName('Data').AsInteger;
          if result < 1 then result := cQOfflimitRelaisNr;
       end;
     end;
  finally
    xADODBAccess.Free;
  end;
end;
//------------------------------------------------------------------------------
function AskSaveDLG: Word;
begin
  Result:= MessageDlg(cSaveMSG, mtConfirmation, [mbYes, mbNo], 0);
end;
//------------------------------------------------------------------------------

end.
