(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMAlerter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: - MMAlerter.DLL
| Info..........:
| Develop.system: Windows 2000 prof.
| Target.system.: W2k / XP / W2k3
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2004  0.01  Sdo | Projekt erstellt
|=============================================================================*)
program MMAlerterConfig;

uses
  MemCheck,
  mmCS,
  Forms,
  u_MMAlerterMain in 'u_MMAlerterMain.pas' {AlerterMain},
  u_QOffAlarmConfig in 'u_QOffAlarmConfig.pas' {QOffAlarmConfig},
  u_HardwareConfig in 'u_HardwareConfig.pas' {HardwareConfig},
  u_AlerterConfig in 'u_AlerterConfig.pas',
  u_MMAlerterConfigForm in 'u_MMAlerterConfigForm.pas' {AlertConfiguration};

{$R *.RES}
{$R 'Version.res'}

begin

{$IFDEF MemCheck}
  MemChk('MMAlerterConfig');
{$ENDIF}

  Application.Initialize;
  Application.CreateForm(TAlerterMain, AlerterMain);
  Application.Run;
end.
