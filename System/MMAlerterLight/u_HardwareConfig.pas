(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_HardwareConfig.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Config. Hardware Window zu Applikation MMAlerter
|
| Info..........:
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.04.2004  1.00  Sdo | Datei erstellt
|=============================================================================*)
unit u_HardwareConfig;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ADODBAccess, StdCtrls, mmNumCtrl, ExtCtrls, mmRadioGroup, mmTimer,
  ImgList, ActnList, mmActionList, Buttons, mmBitBtn, mmSpeedButton,
  mmLabel, mmGroupBox, IvDictio, IvMulti, IvEMulti, mmTranslator;

type
  THardwareConfig = class(TForm)
    gbConnectionCHeck: TmmGroupBox;
    lbConnectionStatus: TmmLabel;
    sbAlarmLight: TmmSpeedButton;
    mmLabel1: TmmLabel;
    bbCheckConnection: TmmBitBtn;
    mmActionList: TmmActionList;
    acAlarmLightOFF: TAction;
    acAlarmLightON: TAction;
    ImageList: TImageList;
    tTestlightTimer: TmmTimer;
    gbRelais: TmmGroupBox;
    lbQOfflimitRelais: TmmLabel;
    neQOfflimitRelais: TmmNumEdit;
    bCancel: TmmBitBtn;
    bSave: TmmBitBtn;
    mmTranslator1: TmmTranslator;
    gbSerialInterface: TmmGroupBox;
    lbCom: TmmLabel;
//    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bbCheckConnectionClick(Sender: TObject);
    procedure acAlarmLightOFFExecute(Sender: TObject);
    procedure acAlarmLightONExecute(Sender: TObject);
    procedure neQOfflimitRelaisEnter(Sender: TObject);
    procedure neQOfflimitRelaisExit(Sender: TObject);
    procedure tTestlightTimerTimer(Sender: TObject);
    procedure bSaveClick(Sender: TObject);
    procedure neQOfflimitRelaisChange(Sender: TObject);
    procedure bCancelClick(Sender: TObject);
  private
    { Private declarations }
    mADODBAccess : TADODBAccess;
    mExistsDBData : Boolean;
    mComPort : Integer;
    mComportList :  TStringList;
    mOrgQOffLRelaisNr : Integer;
    mRelayNrChanged : Boolean;
    mAfterCreated : Boolean;

    procedure GetComPortsList;
    function FlashLightON(var aRelaisNr: Word):Boolean;
    function FlashLightOFF:Boolean;
    function ComPortCheck(mComPort: Integer; aRelaisNr: Word):Boolean;
    procedure SaveRelayNr;
    //procedure CheckTableAlerter;

  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  end;



resourcestring
   cNoComPort         = '(*)Kein COM Port fuer die Alarmierungs-Lampe definiert!'; //ivlm
   cNoConnection      = '(*)Keine Verbindung auf allen %d seriellen Schnittstellen'; //ivlm
   cConnection        = '(*)Verbindung auf %s'; //ivlm
   cCheckConnection   = '(*)Keine Verbindung. COM-Port ueberpruefen'; //ivlm
   cComPortSet        = '(*)%s definiert'; //ivlm
  

var
  HardwareConfig: THardwareConfig;

implementation

{$R *.DFM}
Uses u_AlerterConfig, u_MMAlerterMain, LoepfeGlobal, MMEventLog, RelaisCard;

    // u_MMAlerter;


//------------------------------------------------------------------------------
procedure THardwareConfig.FormDestroy(Sender: TObject);
begin
  mADODBAccess.Free;
  mComportList.Free;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.bbCheckConnectionClick(Sender: TObject);
var //xOutputSet : TOutput;
    x, xRelaisNr     : Word;
    xRet : Boolean;
    xText: String;
begin
  xRet := FALSE;
  xRelaisNr := GetOfflimitRelais;

  bbCheckConnection.Enabled := FALSE;
  for x:= 0 to mComportList.Count-1 do begin
      xText :=  StringReplace(mComportList.Strings[x], 'COM', '',[rfReplaceAll]);
      try
         mComPort := StrToInt(xText);
      except
         mComPort := x+1;
      end;
      xRet := ComPortCheck(mComPort, xRelaisNr ); //u_MMAlerterMain.ComPortCheck(mComPort);
      if xRet then break;
  end;

  if not xRet then begin
     xText:= Format(cNoConnection,[ mComportList.Count] );
     lbConnectionStatus.Caption := xText;
     lbCom.Caption := xText;
  end else begin
     xText:= Format(cConnection, [ mComportList.Strings[x] ]);
     lbConnectionStatus.Caption := xText;
     lbCom.Caption := xText;
     SetRegInteger(cRegLM, cAlertRegPath, cComPort, mComPort );

     gLog.Write(etInformation, 'Connection on ' + mComportList.Strings[x] );
  end;

    bbCheckConnection.Enabled := TRUE;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.acAlarmLightOFFExecute(Sender: TObject);
var xBMP : TBitmap;
    xRelaisNr : Word;
    xText : String;
begin
  //Von OFF nach ON
  sbAlarmLight.Action :=  acAlarmLightON;

  xBMP := TBitmap.Create;
  ImageList.GetBitmap( TAction(sbAlarmLight.Action).ImageIndex, xBMP);

  sbAlarmLight.Glyph.Assign(xBMP);
  xBMP.Free;

  tTestlightTimer.Enabled := TRUE;
  xRelaisNr := 0;
  lbConnectionStatus.Caption := '';
  if not FlashLightOn(xRelaisNr) then
     lbConnectionStatus.Caption := cCheckConnection
  else begin
     xText := Format('Check flash light on relay %d : ON',[xRelaisNr]);
     gLog.Write(etInformation, xText);
  end;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.acAlarmLightONExecute(Sender: TObject);
var xBMP : TBitmap;
begin
  //Von ON nach OFF
  sbAlarmLight.Action :=  acAlarmLightOFF;

  xBMP := TBitmap.Create;
  ImageList.GetBitmap( TAction(sbAlarmLight.Action).ImageIndex, xBMP);

  sbAlarmLight.Glyph.Assign(xBMP);
  xBMP.Free;

  tTestlightTimer.Enabled := FALSE;

  if FlashLightOFF then
     gLog.Write(etInformation, 'Check flash light: OFF');
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.neQOfflimitRelaisEnter(Sender: TObject);
begin
  neQOfflimitRelais.Text:= StringReplace(neQOfflimitRelais.Text, '--', '-',[rfReplaceAll]);
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.neQOfflimitRelaisExit(Sender: TObject);
begin
  neQOfflimitRelais.CheckValue;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.GetComPortsList;
begin
  mComportList.CommaText := GetComPortsListAsCommaText;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.tTestlightTimerTimer(Sender: TObject);
begin
  //BL-Test aus
  sbAlarmLight.Down := FALSE;
  acAlarmLightON.Execute;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.bSaveClick(Sender: TObject);
var xText : String;
begin

  SaveRelayNr;

  xText := neQOfflimitRelais.AsString;
  gLog.Write(etInformation, 'Set Q-Offlimit alert to relay nr. ' + xText);
  SetRegInteger(cRegLM, cAlertRegPath, cComPort, mComPort ); //
  mRelayNrChanged := FALSE;
end;
//------------------------------------------------------------------------------
function THardwareConfig.FlashLightON(var aRelaisNr: Word):Boolean;
var xRelaisNr : Integer;
begin

 //gLog.Write(etInformation, ' THardwareConfig.FlashLightOn; Enter');

 xRelaisNr := GetOfflimitRelais;
 aRelaisNr := xRelaisNr;

 Result := FALSE;
 try

    //Alarm Quit
    with TADODBAccess.Create(1, TRUE) do begin
         try
            if Init then
               with Query[0] do begin
                    Close;
                    //SQL.Text := cFlashLightTest;
                    SQL.Text := cFlashLightTestAndRelais;
                    ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
                    ParamByName('c_ActualError').AsInteger := cLightON;
                    ExecSQL;

                    Close;
                    SQL.Text := cGetFlashLightTestResult;
                    Open;
                    First;
                    Result :=FieldByName('c_Result').AsBoolean;
               end;
         finally
            Free;
         end;
    end;
  except
  end;
  //gLog.Write(etInformation, ' THardwareConfig.FlashLightOn; Exit');
end;
//------------------------------------------------------------------------------
function THardwareConfig.FlashLightOFF: Boolean;
var xRelaisNr : Integer;
begin

// gLog.Write(etInformation, ' THardwareConfig.FlashLightOFF; Enter');


 xRelaisNr := GetOfflimitRelais;

 Result := FALSE;
 try

    //Alarm Quit
    with TADODBAccess.Create(1, TRUE) do begin
         try
            if Init then
               with Query[0] do begin
                    Close;
                    SQL.Text := cFlashLightTest;
                    SQL.Text := cFlashLightTestAndRelais;
                    ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
                    ParamByName('c_ActualError').AsInteger := cLightOFF;
                    ExecSQL;

                    Close;
                    SQL.Text := cGetFlashLightTestResult;
                    Open;
                    First;
                    Result :=FieldByName('c_Result').AsBoolean;
               end;
         finally
            Free;
         end;
    end;
  except
  end;

  //gLog.Write(etInformation, ' THardwareConfig.FlashLightOFF; Exit');

end;
//------------------------------------------------------------------------------
function THardwareConfig.ComPortCheck(mComPort: Integer; aRelaisNr: Word):Boolean;
var //xRelaisNr : Integer;
    xMsg : String;
begin

 //gLog.Write(etInformation, ' THardwareConfig.ComPortCheck( ' + IntToStr(mComPort) + ' ; Enter');
 //xRelaisNr := GetOfflimitRelais;
 //xRelaisNr := aRelaisNr;
 Result := FALSE;
 try

    with TADODBAccess.Create(1, TRUE) do begin
         try
            if Init then
               with Query[0] do begin
                    Close;
                    SQL.Text := cSetComPort;
                    ParamByName('c_ComPort').AsInteger := mComPort;
                    ExecSQL;

                    Close;
                    SQL.Text := cFlashLightTest;
                   // ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
                    ParamByName('c_ActualError').AsInteger := cComCheck;
                    ExecSQL;

                    Close;
                    SQL.Text := cGetFlashLightTestResult;
                    Open;
                    First;
                    Result :=FieldByName('c_Result').AsBoolean;
               end;
         finally
            Free;
         end;
    end;
  except
     on E: Exception do  begin
           xMsg:= Format('Error in ComPortCheck( %d ). Error msg: %s',[mComPort ,e.Message ] );
           MessageDLG(xMsg, mtError, [mbOk], 0);
     end;
  end;
end;
//------------------------------------------------------------------------------

procedure THardwareConfig.SaveRelayNr;
var xText, xMSG : String;
begin

  //SetRegInteger(cRegLM, cAlertRegPath, cComPort, mComPort ); //
  if mADODBAccess.DBError <> 0 then exit;

  xText :=   neQOfflimitRelais.AsString;
  try

    //Relais Nr. in DB schreiben  -> fuer GUI (SRV & WKS)
    with mADODBAccess.Query[0] do begin
       Close;
       if not mExistsDBData then begin
          SQL.Text := cInsertNewQOffRelaisNr;  //Tab. : t_MMUParm
          ParamByName('Data').AsString  := xText;
          ExecSQL;
       end else begin
          SQL.Text := cUpdateNewQOffRelaisNr; //Tab. : t_MMUParm
          ParamByName('Data').AsString  := xText;
          ExecSQL;
       end;

       try

          //Table Check
          SQL.Text :=  cExistsTabt_MMAlerter;
          Open;
          if EOF then begin
            xMSG := Format( cTableNotExists, ['t_MMAlerter']);
            MessageDlg(xMSG, mtError, [mbOk], 0);
            exit;
          end;

         Close;
         SQL.Text := cUpdateRelaisNr;  //Tab. : t_MMAlerter
         ParamByName('c_Relais_NrNew').AsInteger := neQOfflimitRelais.AsInteger;
         //ParamByName('c_Relais_NrOld').AsInteger := mOrgQOffLRelaisNr;
         ExecSQL;
       except
       end;
    end;
    except
  end; 
end;
 //------------------------------------------------------------------------------
procedure THardwareConfig.neQOfflimitRelaisChange(Sender: TObject);
begin
 if not mAfterCreated then exit;
 mRelayNrChanged := TRUE;
end;
//------------------------------------------------------------------------------
procedure THardwareConfig.bCancelClick(Sender: TObject);
begin
 if mRelayNrChanged then
    if AskSaveDLG = mrYes then begin
       bSave.Click;
    end;

 if sbAlarmLight.Down then
    acAlarmLightON.Execute; //-> On to OFF
 Close;
end;
//------------------------------------------------------------------------------
constructor THardwareConfig.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Screen.Cursor :=  crHourGlass;

  mExistsDBData := FALSE;
  mAfterCreated := FALSE;

  neQOfflimitRelais.Value := cQOfflimitRelaisNr;
  try
    mADODBAccess := TADODBAccess.Create(1);

  if mADODBAccess.Init then
     with mADODBAccess.Query[0] do begin
          //RelaisNr. Ermitteln
          Close;
          SQL.Text := cGetNewQOffRelaisNr;
          Open;
          if not Eof then begin
             mExistsDBData := TRUE;
             neQOfflimitRelais.Value := FieldByName('Data').AsInteger;
          end;
     end;
  except
  end;

  if mADODBAccess.DBError <> 0 then
     MessageDlg('Error in ADODBAccess.' + #10#13 + 'Error msg :' + #10#13 +  mADODBAccess.DBErrorTxt, mtError, [mbOk], 0);

  mOrgQOffLRelaisNr:=  neQOfflimitRelais.AsInteger;

  lbConnectionStatus.Caption := '';

  mComportList := TStringList.Create;
  mComportList.Sorted := TRUE;
  mComportList.Duplicates := dupIgnore;

  //Alle Com-Ports auslesen
  GetComPortsList;


  mComPort := GetRegInteger(cRegLM, cAlertRegPath, cComPort, 0 )-1;

  if (mComPort > mComportList.Count) or (mComPort < 0) then begin
     lbCom.Caption := cNoComPort;
     lbConnectionStatus.Caption := cNoComPort;
  end else begin
     lbCom.Caption := Format(cComPortSet,[ mComportList.Strings[mComPort] ] );
     lbConnectionStatus.Caption :=  Format(cComPortSet,[ mComportList.Strings[mComPort] ] );
  end;

  inc(mComPort);

  mRelayNrChanged := FALSE;
  mAfterCreated := TRUE;

  Screen.Cursor := crDefault;
end;
//------------------------------------------------------------------------------

end.
