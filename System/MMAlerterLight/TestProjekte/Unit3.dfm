object Form1: TForm1
  Left = 419
  Top = 456
  Width = 625
  Height = 260
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object butCTS: TSpeedButton
    Left = 8
    Top = 72
    Width = 50
    Height = 22
    AllowAllUp = True
    GroupIndex = 1
    Caption = 'CTS'
    OnClick = butCTSClick
  end
  object butDSR: TSpeedButton
    Left = 8
    Top = 96
    Width = 50
    Height = 22
    AllowAllUp = True
    GroupIndex = 2
    Caption = 'DSR'
    OnClick = butCTSClick
  end
  object butRING: TSpeedButton
    Left = 8
    Top = 120
    Width = 50
    Height = 22
    AllowAllUp = True
    GroupIndex = 3
    Caption = 'RING'
    OnClick = butCTSClick
  end
  object Label1: TLabel
    Left = 24
    Top = 160
    Width = 32
    Height = 13
    Caption = 'Label1'
  end
  object Button1: TButton
    Left = 104
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Start'
    TabOrder = 0
    OnClick = Button1Click
  end
  object mmNumEdit1: TmmNumEdit
    Left = 104
    Top = 72
    Width = 85
    Height = 21
    Decimals = 2
    Digits = 12
    Masks.PositiveMask = '#,##0'
    Max = 20
    NumericType = ntGeneral
    TabOrder = 1
    UseRounding = True
    Validate = False
    ValidateString = 
      '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
      's %s'
  end
  object Button2: TButton
    Left = 200
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Stop'
    TabOrder = 2
    OnClick = Button2Click
  end
  object mmTimer1: TmmTimer
    Enabled = False
    Interval = 30
    OnTimer = mmTimer1Timer
    Left = 16
    Top = 16
  end
end
