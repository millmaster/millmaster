unit Unit3;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, mmTimer, RelaisCard, ComLK, Buttons, mmNumCtrl;

type
  TForm1 = class(TForm)
    mmTimer1: TmmTimer;
    butCTS: TSpeedButton;
    butDSR: TSpeedButton;
    butRING: TSpeedButton;
    Button1: TButton;
    mmNumEdit1: TmmNumEdit;
    Button2: TButton;
    Label1: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure mmTimer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure butCTSClick(Sender: TObject);
  private
    { Private declarations }
    Comm : T_QCCom32;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}


procedure TForm1.FormCreate(Sender: TObject);
begin
  try
  Comm :=  T_QCCom32.Create(NIL);
 // Comm.Port := mmNumEdit1.AsInteger;
  Comm.Baud := 19200;
 // Comm.Open;



{
  if not Comm.Opened then
     ShowMessage('COM Close: COM = '  + IntToSTr(Comm.Port) );

  mmTimer1.Enabled := TRUE;
 } 
  except
    on e: exception do begin
         ShowMessage(e.Message);
    end;
  end;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
  mmTimer1.Enabled := FALSE;
  Comm.Close;
  Comm.Free;
end;

procedure TForm1.mmTimer1Timer(Sender: TObject);
var  Status: Cardinal;  //Status der Inputleitungen
     xText: STring;
begin
  xText := '';
  if (Comm.Opened) and (GetCommModemStatus(Cardinal(Comm.handle), Status)) then begin

      butCTS.Down := (Status and MS_CTS_ON) > 0;     // Status der Inputleitungen abfragen
      butDSR.Down := (Status and MS_DSR_ON) > 0;
      butRING.Down := (Status and MS_RING_ON) > 0;

      if (Status and MS_CTS_ON) > 0 then xText := 'MS_CTS_ON';
      if (Status and MS_DSR_ON) > 0 then xText := 'MS_DSR_ON';
      if (Status and MS_RING_ON) > 0 then xText := 'MS_RING_ON';
   end;

  label1.Caption:=  xText;

end;

procedure TForm1.Button1Click(Sender: TObject);
begin

 try
  Comm.Port := mmNumEdit1.AsInteger;
  Comm.Open;

  if not Comm.Opened then
     ShowMessage('COM Close: COM = '  + IntToSTr(Comm.Port) );

  mmTimer1.Enabled := TRUE;
  except
    on e: exception do begin
         ShowMessage(e.Message);
    end;
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
   mmTimer1.Enabled := FALSE;
   Comm.Close;
end;

procedure TForm1.butCTSClick(Sender: TObject);
begin
  if Sender is TSpeedButton then
  (Sender as TSpeedButton).Down := FALSE;
end;

end.
 