unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TFormMain = class(TForm)
    Button1: TButton;
    Button2: TButton;
    Edit1: TEdit;
    Edit2: TEdit;
    Button3: TButton;
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  function CreateCOMConnection(aCom : Word): THandle;


var
  FormMain: TFormMain;

implementation

{$R *.DFM}
uses Unit2;


function CreateCOMConnection(aCom : Word): THandle;
var xComHandle : THandle;
begin
   xComHandle := 0;
   xComHandle := CreateFile( 'Com1',
                             GENERIC_READ or GENERIC_WRITE,
                             0,
                             NIL,
                             OPEN_EXISTING,
                             FILE_FLAG_OVERLAPPED,
                             0);



   if xComHandle = INVALID_HANDLE_VALUE then begin

      ShowMessage('xx');

   end;

   Result := xComHandle;

end;


procedure TFormMain.Button1Click(Sender: TObject);
var xComHandle, xHandle : THandle;
    xRet : Boolean;
    xOVERLAPPED : OVERLAPPED;
    xEvtMask : DWORD;

    xdcb : DCB;

    xCommTimeouts : COMMTIMEOUTS ;
    xPoverlapped: Poverlapped;
begin
  xComHandle := CreateCOMConnection(1);

  xEvtMask :=  EV_CTS OR EV_DSR ;
  xRet := SetCommMask( xComHandle, xEvtMask );


  fillchar(xOVERLAPPED,sizeof(Toverlapped),0);
  xOVERLAPPED.hEvent := CreateEvent( NIL,
                                     FALSE,
                                     FALSE,
                                     0     );

  Assert(Boolean(xOVERLAPPED.hEvent));


  try

    xRet := GetCommState(xComHandle, xdcb);

    xdcb.BaudRate  := CBR_19200;
    xdcb.ByteSize  := 8;
    xdcb.Parity    := NOPARITY;
    xdcb.StopBits  := ONESTOPBIT;

    xRet := SetCommState(xComHandle, xdcb);

    xRet := SetCommMask(xComHandle, EV_CTS or EV_DSR or EV_RXCHAR);


    GetCommTimeouts(xComHandle, xCommTimeouts);
    xCommTimeouts.ReadIntervalTimeout := 10;

    SetCommTimeouts(xComHandle, xCommTimeouts);

    xEvtMask := 0;
    if WaitCommEvent( xComHandle,
                      xEvtMask,
                      @xOVERLAPPED) then

    if (xEvtMask = EV_DSR) then
       beep;

  except
    on E: Exception do begin
            ShowMessage(e.Message);
          end;
  end;



  beep;
end;

procedure TFormMain.Button3Click(Sender: TObject);
begin
   With TFormCOM.Create(NIL) do begin

      show;

   end;
end;

end.
