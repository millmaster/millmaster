object QOffAlarmConfig: TQOffAlarmConfig
  Left = 1569
  Top = 396
  BorderStyle = bsDialog
  Caption = '(*)Q-Offlimit Alarm Konfiguration'
  ClientHeight = 231
  ClientWidth = 367
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object gbQOfflimitAlert: TmmGroupBox
    Left = 10
    Top = 5
    Width = 345
    Height = 85
    Caption = '(*)Q-Offlimit Alarm'
    TabOrder = 0
    object lbMaxQOAlert: TmmLabel
      Left = 65
      Top = 55
      Width = 222
      Height = 13
      Caption = '(*)Minimale Anzahl Alarme bis Alarmlampe blinkt'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object neMaxQOAlert: TmmNumEdit
      Left = 20
      Top = 50
      Width = 33
      Height = 21
      Decimals = 0
      Digits = 3
      Masks.NegativeMask = '-###'
      Masks.PositiveMask = '###'
      Masks.ZeroMask = '0'
      Max = 100
      Min = 1
      NumericType = ntGeneral
      OnChange = neMaxQOAlertChange
      OnEnter = neMaxQOAlertEnter
      OnExit = neMaxQOAlertExit
      TabOrder = 1
      UseRounding = True
      Validate = True
      ValidateString = 
        '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
        's %s'
    end
    object cbtActivateQOAlert: TmmCheckBox
      Left = 20
      Top = 20
      Width = 201
      Height = 17
      Caption = '(*)Alarm aktivieren'
      TabOrder = 0
      Visible = True
      OnClick = cbtActivateQOAlertClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object gbAlertTimer: TmmGroupBox
    Left = 10
    Top = 95
    Width = 345
    Height = 80
    Caption = '(*) Automatische Ausschaltzeit der Alarmlampe'
    TabOrder = 1
    object lbSwitchOffTime: TmmLabel
      Left = 65
      Top = 53
      Width = 167
      Height = 13
      Caption = '(*)Ausschltzeit der Blinklampe [sek.]'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object neSwitchOffTime: TmmNumEdit
      Left = 20
      Top = 50
      Width = 33
      Height = 21
      Decimals = 0
      Digits = 4
      HideSelection = False
      Masks.NegativeMask = '-####'
      Masks.PositiveMask = '####'
      Masks.ZeroMask = '1'
      Max = 3600
      Min = 1
      NumericType = ntGeneral
      OnChange = neSwitchOffTimeChange
      OnEnter = neSwitchOffTimeEnter
      OnExit = neSwitchOffTimeExit
      ParentShowHint = False
      ShowHint = False
      TabOrder = 0
      UseRounding = False
      Validate = True
      ValidateString = 
        '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bi' +
        's %s'
    end
    object cbQuitbyHand: TmmCheckBox
      Left = 24
      Top = 20
      Width = 313
      Height = 17
      Caption = '(*)Quittierung immer von Hand'
      TabOrder = 1
      Visible = True
      OnClick = cbQuitbyHandClick
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object bSave: TmmBitBtn
    Left = 145
    Top = 198
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = '(*)Speichern'
    Default = True
    TabOrder = 2
    Visible = True
    OnClick = bSaveClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    AutoLabel.LabelPosition = lpLeft
  end
  object bCancel: TmmBitBtn
    Left = 255
    Top = 198
    Width = 100
    Height = 25
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = '(9)Abbrechen'
    ModalResult = 2
    TabOrder = 3
    Visible = True
    OnClick = bCancelClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
    AutoLabel.LabelPosition = lpLeft
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'Dictionary1'
    Left = 274
    Top = 21
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        'TmmCheckBox'
        '*'
        0))
  end
end
