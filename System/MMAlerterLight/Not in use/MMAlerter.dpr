library MMAlerter;

{ Important note about DLL memory management: ShareMem must be the
  first unit in your library's USES clause AND your project's (select
  Project-View Source) USES clause if your DLL exports any procedures or
  functions that pass strings as parameters or function results. This
  applies to all strings passed to and from your DLL--even those that
  are nested in records and classes. ShareMem is the interface unit to
  the BORLNDMM.DLL shared memory manager, which must be deployed along
  with your DLL. To avoid using BORLNDMM.DLL, pass string information
  using PChar or ShortString parameters. }

uses
//wss: f�r was ShareMem?
//  ShareMem,
  SysUtils,
  Classes,
  ActiveX,
  Windows,

  u_MMAlerter in 'u_MMAlerter.pas';

//u_MMAlerter;

{$R *.RES}

procedure DLLEntry(dwReason: DWORD);
const
  xResult: HResult = -1;
begin
  case dwReason of
    DLL_PROCESS_ATTACH: begin
        xResult := CoInitialize(nil);
        // S_OK    --> Com Umgebung wurde zum ersten mal initialisiert
        // S_FALSE --> COM Umgebung wurde bereits vorher initialisiert
        if ((xResult <> S_OK) and (xResult <> S_FALSE)) then
          raise Exception.Create('CoInitialize failed');
      end; // DLL_PROCESS_ATTACH: begin
    DLL_PROCESS_DETACH: begin
        if (xResult = S_OK) or (xResult = S_FALSE) then
          CoUninitialize;
      end; // DLL_PROCESS_DETACH: begin
  end; // case dwReason of
end;

exports
  //Int. Name     ext. Name
  xp_QOfflimitAlarm  name 'xp_QOfflimitAlarm',
  xp_AlarmON         name 'xp_AlarmON',
  xp_AlarmOFF        name 'xp_AlarmOFF',

  QOfflimitAlarm     name 'QOfflimitAlarm',
  ComPortCheck       name 'ComPortCheck',
  AlarmON            name 'AlarmON',
  AlarmOFF           name 'AlarmOFF';


begin
  //LibraryProc( DLL_THREAD_ATTACH );
  //DLLProc := @DLLEntry;
  // Process Attach erst mal von Hand aufrufen, da die Procedur erst nach dem
  // Eintritt des Prozesses gesetzt wird
  //DLLEntry(DLL_PROCESS_ATTACH);
end.

