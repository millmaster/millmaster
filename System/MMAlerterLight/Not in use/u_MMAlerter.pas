(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: u_MMAlerter.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: - MMAlerter.DLL
| Info..........:
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 29.03.2004  0.01  Sdo | Projekt erstellt
|=============================================================================*)
unit u_MMAlerter;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, mmTimer, SrvStrucDef,
  mmEventLog, ADODBAccess, SimpleTimer, u_AlerterConfig, RelaisCard, ComLK,
  mmThread;

type
  TMMAlerter = class(TObject)
  private
    mAdoDBAccess: TAdoDBAccess;
    mBLAutoSwitchOFFTimer: TSimpleTimer;
    mBLTestTimer: TSimpleTimer;
    mCOM: TConradRelais;
    mLog: TEventLogWriter;
    function GetComPort: Integer;
  protected
    function ComCheck(aRelaisNr: Word): Word;
    function LightOff(aRelaisNr: Word): Word;
    procedure LightON(aRelaisNr: Word);
    function SendSignalToRelais(aRelaisNr: Word): Word;
  public
    constructor Create; virtual;
    destructor Destroy; override;
    function ComPortCheck(aComPortNr: Word): Boolean; virtual;
    procedure QOfflimitAlarm; virtual;
    procedure QuitAlarm; virtual;
    function TestFlashLight: Boolean; virtual;
    property ComPort: Integer read GetComPort;
  end;


  TAutoSwitchOFFTimer = class(TmmThread)
  private
    mEnduranceTime : DWord;
    mMessageID : THandle;
    procedure SetEnduranceTimeAsMsSec(const Value: DWord);
    function CheckRelais: Boolean;
  public
    procedure Execute; override;
    constructor Create(aEnduranceTimeAsMsSec : DWord ); overload;
    constructor Create; overload;
    procedure StopByHand;
    property  EnduranceTimeAsMsSec : DWord read mEnduranceTime write SetEnduranceTimeAsMsSec;
  end;



function GetComPort: Integer;
function GetOfflimitRelais: Integer;
function BLOFF: Boolean;


// *** Exportierte Funktionen f�r den SQL-Server ***
function xp_QOfflimitAlarm(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_AlarmON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
function xp_AlarmOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;

// *** Exportierte Funktionen f�r die Testapplikation ***
function QOfflimitAlarm: Boolean; stdcall;
function ComPortCheck(aComPortNr: WORD): Boolean; stdcall;
function AlarmON: Boolean; stdcall;
function AlarmOFF: Boolean; stdcall;


threadvar

  lAutoSwitchOFFTimer : TAutoSwitchOFFTimer;

implementation

uses
  mmCS, LoepfeGlobal, mmRegistry, ActiveX;


var
  lLog: TEventLogWriter;



//------------------------------------------------------------------------------
function ConvertBoolean(aValue: Boolean): Integer;
begin
  if aValue then Result := 1
  else Result := 0;
end;
//******************************************************************************
// Exportierte Funktionen f�r den SQL-Server
//******************************************************************************
function xp_QOfflimitAlarm(aSrvProc: PSRV_PROC): Integer; cdecl; export
begin
  Result := ConvertBoolean(QOfflimitAlarm);
end;
//------------------------------------------------------------------------------
function xp_AlarmON(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  Result := ConvertBoolean(AlarmON);
end;
//------------------------------------------------------------------------------
function xp_AlarmOFF(aSrvProc: PSRV_PROC): Integer; CDECL; EXPORT;
begin
  Result := ConvertBoolean(AlarmOFF);
end;
//******************************************************************************
// Exportierte Funktionen f�r die Test-Applikation
//******************************************************************************
function ComPortCheck(aComPortNr: WORD): Boolean; stdcall;
var
  xCOM : TConradRelais;
  xRet : Boolean;
  xMsg : String;
begin
  Result := FALSE;

  xCOM         := TConradRelais.Create;
  xCOM.ComPort := aComPortNr;

  if not xCOM.Init then begin
     xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [xCOM.ComPort]);
     lLog.Write(etError, xMsg);
     xCOM.Free;
     exit;
  end;

  Result       := xCOM.SetActive;
  xCOM.Free;

  Result := xRet;
end;
//------------------------------------------------------------------------------
function AlarmOFF: Boolean; stdcall;
begin

  Result := FALSE;
  try
    //Timer stoppen
    if Assigned(lAutoSwitchOFFTimer) then
       if not lAutoSwitchOFFTimer.Suspended then
          lAutoSwitchOFFTimer.StopByHand;
  except
  end;

  Result := BLOFF;
end;
//------------------------------------------------------------------------------
function AlarmON: Boolean; stdcall;
var
  xSelectedOutput: TOutput;
  xMsg: string;
  xRet: Boolean;
  xCOM: TConradRelais;
  xPort : Word;
begin
  Result := False;

  xCOM := TConradRelais.Create;
  xCOM.ComPort := GetComPort;

  if not xCOM.Init then begin
     xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [xCOM.ComPort]);
     lLog.Write(etError, xMsg);
     xCOM.Free;
     exit;
  end;

  xCOM.ResetOutputs;

  try
    xSelectedOutput := TOutput(GetOfflimitRelais - 1) ;
  except
    begin
      xSelectedOutput := TOutput(0);
      lLog.Write(etWarning, 'Default relay 1 is active');
    end;
  end;

  try
    xRet := xCOM.SetActive([xSelectedOutput]);
  except
    xRet := False;
  end;

  if not xRet then begin
    xMsg := Format('No connection to relay card on Com-Port %d for alert test', [xCOM.ComPort]);
    lLog.Write(etWarning, xMsg);
  end;

  xCOM.Free;

  Result := xRet;
end;
//------------------------------------------------------------------------------
function QOfflimitAlarm: Boolean; stdcall;
var xAdoDBAccess: TAdoDBAccess;
    xSwitchOffTime: DWord;
    xRelaisNr : Integer;
    xMsg : String;
begin
  xSwitchOffTime := 300000; // 5 Min.
  xSwitchOffTime := 10000; // 10 Sec.


  xAdoDBAccess := TAdoDBAccess.Create(1, True);

  if not xAdoDBAccess.Init then
     lLog.Write(etWarning, 'TAdoDBAccess.Init = FALSE');

  with xAdoDBAccess.Query[cPrimaryQuery] do try
    Close;

    SQL.Text := cGetRelaisData;

    
    xRelaisNr := GetOfflimitRelais;
    ParamByName('c_Relais_Nr').AsInteger := xRelaisNr;
    Open;
    First;

    if not EOF then begin
      xSwitchOffTime := FieldByName('c_SwitchOffTime').AsInteger * 1000;
    end else begin
      xMsg := Format('Worng or no relay number (%d) in database found.', [xRelaisNr]);
      lLog.Write(etError, xMsg);
    end;


    xAdoDBAccess.Free;

  except
    on E: Exception do
      lLog.Write(etError, 'TAdoDBAccess error : ' + e.Message);
  end;

  lLog.Write(etInformation, 'Q-Offlimit Alarm');
  AlarmON;

  if xSwitchOffTime > 0 then begin
     try
        if Assigned(lAutoSwitchOFFTimer) then begin
           lAutoSwitchOFFTimer.Suspend;
           lAutoSwitchOFFTimer.Free;
           lAutoSwitchOFFTimer:= NIL;
        end;

        //Timer erstellen
        lAutoSwitchOFFTimer:= TAutoSwitchOFFTimer.Create(xSwitchOffTime);
        lAutoSwitchOFFTimer.Resume;
     except
     end;
  end;
end;
//------------------------------------------------------------------------------


//******************************************************************************
// Alarm Off; ohne Log-Event
//******************************************************************************
function BLOFF: Boolean;
var
  xMsg: string;
  xRet: Boolean;
  xCOM: TConradRelais;
begin

  Result := FALSE;

  xCOM := TConradRelais.Create;
  xCOM.ComPort := GetComPort;

  if not xCOM.Init then begin
     xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [xCOM.ComPort]);
     lLog.Write(etError, xMsg);
     xCOM.Free;
     exit;
  end;

  try
    xRet := xCOM.ResetOutputs;
  except
    xRet := False;
  end;

  if not xRet then begin
    xMsg := Format('No connection to relay card on Com-Port %d for Quit alert', [xCOM.ComPort]);
    lLog.Write(etWarning, xMsg);
  end;

  xCOM.Free;

  Result := xRet;
end;
//------------------------------------------------------------------------------
function GetComPort: Integer;
var
  xComPort, xPort, x: Integer;
  xComPortTxt : String;
  xList : TStringlist;
begin
  xComPort := GetRegInteger(cRegLM, cAlertRegPath, cComPort, 0);


  if xComPort <= 0 then begin
     //Kein Com Port in Reg. definiert, muss jetzt ermittelt werden
     xList := TStringlist.Create;
     xList.Sorted := TRUE;
     xList.Duplicates := dupIgnore;

     //Alle Com Ports ermitteln
     xList.CommaText := StringReplace(GetComPortsListAsCommaText, 'COM', '',[rfReplaceAll]);
     for x:= 0 to xList.Count-1 do begin
         try
           xPort := StrToInt(xList.Strings[x]);
         except
           xPort := x+1;
         end;
         if ComPortCheck(xPort) then break;
     end;
     xComPort := xPort;

     //Aktiv Com Port in Reg. schreiben
     SetRegInteger(cRegLM, cAlertRegPath, cComPort, xComPort);

     xList.Free;
  end;

  Result := xComPort;
end;
//------------------------------------------------------------------------------
function GetOfflimitRelais: Integer;
var xRelaisNr: integer;
begin
  xRelaisNr := GetRegInteger(cRegLM, cAlertRegPath, cQOfflimitRelais, -1);

  if (xRelaisNr <= 0) and (xRelaisNr >8) then
      xRelaisNr := cQOfflimitRelaisNr;
  result := xRelaisNr;
end;
//------------------------------------------------------------------------------



{
********************************** TMMAlerter **********************************
}
constructor TMMAlerter.Create;
begin
  mLog := TEventLogWriter.Create('MillMaster','.', ssApplication, 'MillMaster Alerter: ', True);

  mBLAutoSwitchOFFTimer := TSimpleTimer.Create;
  mBLTestTimer          := TSimpleTimer.Create;
  mAdoDBAccess          := TAdoDBAccess.Create(1);
  mAdoDBAccess.Init;

  mCOM         := TConradRelais.Create;
  mCOM.ComPort := ComPort;
//wss: warum wird Init nicht ausgewertet?
  mCOM.Init;
  mCOM.ResetOutputs;
end;

//:-----------------------------------------------------------------------------
destructor TMMAlerter.Destroy;
begin
  mBLAutoSwitchOFFTimer.Free;
  mBLTestTimer.Free;
  mCOM.Free;
  mLog.Free;

  try
//wss: warum wird DB Klasse nicht freigegeben? -> Speicherloch
   //FreeAndNil(mAdoDBAccess);
  except
  end;
  {-------------------------------------------------------------------------------}
  inherited Destroy;
end;

//:-----------------------------------------------------------------------------
function TMMAlerter.ComCheck(aRelaisNr: Word): Word;
begin
end;
//:-----------------------------------------------------------------------------
function TMMAlerter.ComPortCheck(aComPortNr: Word): Boolean;
begin
  mCOM.ComPort := aComPortNr;
  Result := mCOM.SetActive;
end;

//:-----------------------------------------------------------------------------
function TMMAlerter.GetComPort: Integer;

var
  xComPort: Integer;

begin
  xComPort := GetRegInteger(cRegLM, cAlertRegPath, cComPort, 0);

  if xComPort <= 0 then begin
     //xComPort := ComCheck(xComPort);
    xComPort := 2;
  end;

  Result := xComPort;
end;

//:-----------------------------------------------------------------------------
function TMMAlerter.LightOff(aRelaisNr: Word): Word;
begin
end;

//:-----------------------------------------------------------------------------
procedure TMMAlerter.LightON(aRelaisNr: Word);
begin
end;

//:-----------------------------------------------------------------------------
procedure TMMAlerter.QOfflimitAlarm;
var
  xSelectedOutput: TOutput;
  xMsg: string;
  xRet: Boolean;
begin
  try
    xSelectedOutput := TOutput(cQOfflimitRelaisNr - 1);
  except
    xSelectedOutput := TOutput(0);
  end;

  mCOM.ComPort := ComPort;

  try
    xRet := mCOM.SetActive([xSelectedOutput]);
  except
    xRet := False;
  end;

  if xRet then
    mLog.Write(etInformation, 'Q-Offlimit Alert on')
  else begin
    xMsg := Format('No connection to relay card on Com-Port %d for Q-Offlmit alert', [mCOM.ComPort]);
    mLog.Write(etWarning, xMsg);
  end;
end;

//:-----------------------------------------------------------------------------
procedure TMMAlerter.QuitAlarm;
var
  xMsg: string;
  xRet: Boolean;
begin
  try
    xRet := mCOM.ResetOutputs;
  except
    xRet := False;
  end;

  if xRet then
    mLog.Write(etInformation, 'Quit alert')
  else begin
    xMsg := Format('No connection to relay card on Com-Port %d for Quit alert', [mCOM.ComPort]);
    mLog.Write(etWarning, xMsg);
  end;
end;

//:-----------------------------------------------------------------------------
function TMMAlerter.SendSignalToRelais(aRelaisNr: Word): Word;
begin
end;

//:-----------------------------------------------------------------------------
function TMMAlerter.TestFlashLight: Boolean;
var
  xSelectedOutput: TOutput;
  xMsg: string;
  xRet: Boolean;
begin
  try
    xSelectedOutput := TOutput(cQOfflimitRelaisNr - 1);
  except
    xSelectedOutput := TOutput(0);
  end;

  mCOM.ComPort := ComPort;
  try
    xRet := mCOM.SetActive([xSelectedOutput]);
  except
    xRet := False;
  end;

  if xRet then
    mLog.Write(etInformation, 'Start alert test')
  else begin
    xMsg := Format('No connection to relay card on Com-Port %d for alert test', [mCOM.ComPort]);
    mLog.Write(etWarning, xMsg);
  end;

  Result := xRet;
end;

{ TAutoSwitchOFFTimer }
//------------------------------------------------------------------------------
constructor TAutoSwitchOFFTimer.Create(aEnduranceTimeAsMsSec: DWord);
begin
  mEnduranceTime := aEnduranceTimeAsMsSec;
  FreeOnTerminate := TRUE;

  //Create and not start
  inherited Create(TRUE);
end;
//------------------------------------------------------------------------------
function TAutoSwitchOFFTimer.CheckRelais : Boolean;
var
  xCOM : TConradRelais;
  xRet : Boolean;
  xMsg : String;
  xSelectedOutput : TOutput;
begin
  Result := FALSE;
  try
    xSelectedOutput := TOutput(GetOfflimitRelais - 1);
  except
    begin
      xSelectedOutput := TOutput(0);
      lLog.Write(etWarning, 'Default relay 1 is active');
    end;
  end;

  xCOM         := TConradRelais.Create;
  xCOM.ComPort := GetComPort;

  if not xCOM.Init then begin
     xMsg := Format('Com port error. Can not initialize Com port %d. Maybe no MM-Alerter Box connectet on Com port.', [xCOM.ComPort]);
     lLog.Write(etError, xMsg);
     xCOM.Free;
     exit;
  end;

  xCOM.ResetOutputs;
  Result := xCOM.GetActive([xSelectedOutput]);

  xCOM.Free;
end;
//------------------------------------------------------------------------------
constructor TAutoSwitchOFFTimer.Create;
begin
  FreeOnTerminate := TRUE;
  //Create and not start
  inherited Create(TRUE);
end;
//------------------------------------------------------------------------------
procedure TAutoSwitchOFFTimer.Execute;
var xEnduranceTime : DWord;
    xShitchOffByHand, xActive : Boolean;
begin
  xShitchOffByHand := mEnduranceTime <= 0;
  xEnduranceTime := GetTickCount + mEnduranceTime;

  while not Terminated do  begin
     Application.ProcessMessages;
     if not xShitchOffByHand then
        if GetTickCount > xEnduranceTime then begin
           if CheckRelais then
             lLog.Write(etInformation, 'Q-Offlimit alert automatic switch off');
           BLOFF;
           Suspend;
        end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAutoSwitchOFFTimer.SetEnduranceTimeAsMsSec(const Value: DWord);
begin
  mEnduranceTime := Value;
end;
//------------------------------------------------------------------------------
procedure TAutoSwitchOFFTimer.StopByHand;
begin
  Suspend;
end;
//------------------------------------------------------------------------------







initialization
  CoInitialize(nil);

  // beim 1. Aufruf von xp_QOfflimtAlarm wird diese Instanz erstellt
  lAutoSwitchOFFTimer := NIL;
  lLog := TEventLogWriter.Create('MillMaster', '.', ssApplication, 'MillMaster Alerter: ', True);
//  lLog.Write(etInformation, 'initialization');

finalization

  lLog.Write(etInformation, 'finalization');

  lAutoSwitchOFFTimer.Free;
  lAutoSwitchOFFTimer := NIL;

//   lMMAlerter.Free;
//   lMMAlerter:= NIL;

  lLog.Free;
  CoUninitialize;
end.

