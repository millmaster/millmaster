                   Release Notes  PN 93000362

               Digi RealPort Driver & Setup Wizard
                  Version 3.0.226.0, 03/18/2004

                     Microsoft Windows 2000
                      Microsoft Windows XP
                  Microsoft Windows Server 2003

                      Software Package PNs
                       40002164, 40002360


  CONTENTS

  Section      Description
  -------------------------------
  1            Introduction
  2            Supported Operating Systems
  3            Supported Products
  4            Known Limitations
  5            Installing RealPort Driver
  6            Updating RealPort Software
  7            Removing RealPort Device
  8            Upgrading Driver from FAS to RealPort
  9            Advanced Features
 10            Copyright Notices
 11            Additional Notes
 12            History


  1. INTRODUCTION

     This document contains general information as well as 
     last minute changes for the Digi RealPort driver and
     Setup Wizard.

     Refer to the following numbers when searching the Digi
     Web site (www.digi.com) or FTP site (ftp.digi.com) for 
     the latest software package. The Digi RealPort driver 
     and Setup Wizard is available in one of two formats.

       o As a single binary: 

         40002360_<highest revision letter>

       o As an INF distribution: 

         40002164_<highest revision letter>

    If you are unsure which package to download, download the
    single binary.


  2. SUPPORTED OPERATING SYSTEMS

     Microsoft Windows 2000
     Microsoft Windows XP
     Microsoft Windows Server 2003


  3. SUPPORTED PRODUCTS

     Digi Connect Family
     Digi EtherLite Family
     Digi Flex Family
     Digi One Family
     Digi PortServer Family
     Digi PortServer TS Family


  4. KNOWN LIMITATIONS

     If you plan to use the RealPort driver with serial printers, you
     have to reboot Windows or restart the print spooler after 
     installing a device or renaming ports to see the new ports listed 
     in the Print Spooler's list of available ports for printing. This 
     is a known limitation of the Print Spooler, which does not detect 
     changes in the list of available ports automatically. To restart 
     the spooler service, open a Command Window (as Administrator) and 
     type "net stop spooler" <ENTER> followed by "net start spooler" 
     <ENTER>.

     If you experience problems printing to a serial printer from a 
     port, go to the device's Device Properties dialog in Device 
     Manager and enable "Complete Write Requests Immediately" for that 
     port.


  5. INSTALLING REALPORT DRIVER

     You need administrative privileges to install device drivers.
     Please be sure you are logged into Windows as Administrator
     or as a user that is a member of the Administrators group.

     To install a Digi RealPort device and Digi RealPort Software for 
     Windows, run the RealPort Setup Wizard, Setup.exe.

     The RealPort Setup Wizard searches the network for RealPort 
     devices and displays those devices it finds on the device 
     discovery page. Use the I.P. address and product information to 
     locate the device you wish to install.

     You do not need to manually enter a device's I.P. address 
     unless the device you wish to install is off-line or does not 
     support device discovery (for example, Digi EtherLite, Digi 
     PortServer and Digi PortServer II products do not support device 
     discovery). To install these devices, choose <Device not listed> 
     from the device discovery page and follow the Wizard prompts.

     TROUBLESHOOTING DEVICE DISCOVERY
     --------------------------------

     If your network or host computer has firewalls or proxies 
     installed, your device may not be detected during device 
     discovery.

     If you do not see your device listed in the device discovery 
     list, first verify that your device is powered on and connected 
     to the network.

     Next, verify your computer does not have a firewall or proxy 
     installed. If your computer does have a firewall or proxy 
     installed, either install the device manually by selecting 
     <Device not listed> or, if feasible, disable the firewall and/or
     proxy before installing RealPort.


  6. UPDATING REALPORT SOFTWARE

     You need administrative privileges to update device drivers.
     Please be sure you are logged into Windows as Administrator
     or as a user that is a member of the Administrators group.

     Run Setup.exe and select the option, "Update Digi RealPort 
     Software". The Wizard will update the Digi RealPort device 
     drivers and software. 


  7. REMOVING REALPORT DEVICE

     You need administrative privileges to remove devices. Please be 
     sure you are logged into Windows as Administrator or as a user 
     that is a member of the Administrators group.

     Run Setup.exe and select the option, "Remove an existing device". 
     Select the device or devices you want to remove and follow the 
     Wizard prompts.


  8. UPGRADING DRIVER FROM FAS TO REALPORT

     If you have previously installed the EtherLite driver for an
     EtherLite device (EtherLite 2/8/16/32/80/160/162), follow these
     steps to upgrade to the current RealPort driver:

     a. Uninstall the old EtherLite driver and reboot the system.

     b. Update the EtherLite firmware using the DGIPSERV utility,
        which can be downloaded along with the latest firmware 
        from the Digi Support Web site, http://support.digi.com.

     c. Install the new RealPort driver by running Setup.exe.

     d. Note that EtherLite devices are not detected by the RealPort
        Setup Wizard during device discovery. To install an Etherlite
        device, you will have to choose <Device not listed> and 
        manually enter its I.P. address. See the preceding section, 
        Installing RealPort Driver, for more details.


  9. ADVANCED FEATURES

     The RealPort driver supports an additional set of advanced
     features, each of which can be enabled or disabled from the
     Device Manager. Some features affect the operation of the 
     device itself (and all ports), while other features affect 
     the operation of individual ports.

     * DEVICE-SPECIFIC FEATURES *
     ----------------------------

     To locate the device-specific features, follow these steps:

         o From Device Manager, access the device's Property Page
         o Activate the Advanced tab and click "Properties..."
         o Make sure that the device is selected on the left-
           hand side of the window
         o You are now viewing the device features
         o To view the device's network features, click on the 
           Network tab

     Each of the features discussed here is disabled (or blank) by 
     default.

     DEVICE NAME [PROPERTIES TAB]
     ----------------------------

     Normally, the name of the device is the name of the product 
     followed by its I.P. address in parentheses.

      E.g., "Digi Connect ME (192.168.0.123)"

     To replace the I.P. address with text of your choice, enter a
     short name here; it will be displayed in the Device Manager in
     place of the I.P. address.

      E.g., "Digi Connect ME (My first device)"

     DEVICE DESCRIPTION [PROPERTIES TAB]
     -----------------------------------

     The device description can be used any way you would like to 
     store additional information about the device. It is only 
     displayed on the property page.

     RENAME PORTS... [PROPERTIES TAB]
     --------------------------------

     Click the Rename Ports... button to change the COM numbers of all
     of a device's ports. A dialog will pop up with a drop-down list 
     of available COM ports. From the list, choose a COM port number 
     with which to begin renumbering.

     NETWORK SETTINGS [NETWORK TAB]
     ------------------------------

     If the I.P. address of a device has changed, or if a device has 
     been swapped with another device, update the network settings on 
     this page to alert the driver of the changes.

     You can manually enter a device's I.P. address and TCP port on 
     this page. 

     Alternatively, you can instead search for your device using the
     Device Discovery Wizard. Click on the Browse... button to use the
     wizard.

     ENCRYPT NETWORK TRAFFIC [NETWORK TAB]
     -------------------------------------

     Use this feature to encrypt network data sent between the 
     RealPort driver and the remote device. By default, data is 
     transmitted as plaintext across the network.

     This checkbox only appears if the remote RealPort device supports
     Encrypted RealPort.

     If you do not see this checkbox, you may need to update the 
     firmware on your device. Visit http://support.digi.com and look 
     for Firmware Updates. Read the release notes of the newer 
     firmware and look for an indication that the firmware supports 
     Encrypted RealPort. Follow the directions to update the firmware.

     The Digi RealPort Encryption Service is automatically installed
     by the Digi RealPort Setup Wizard when you install a device. It
     encrypts data using TLS v1.0 and employs a 128-bit AES cipher.

     TCP PORT FOR ENCRYPTED TRAFFIC [NETWORK TAB]
     --------------------------------------------

     Use this field to change the TCP port that the RealPort driver
     uses to establish an Encrypted RealPort session with the remote 
     device. The default port is 1027.

     This checkbox is not displayed if your device does not support
     Encrypted RealPort. See the preceding section, Encrypt Network 
     Traffic, for more information.

     * PORT-SPECIFIC FEATURES *
     --------------------------

     To locate the port-specific features, follow these steps:

         o From Device Manager, access the device's Property Page
         o Activate the Advanced tab and click "Properties..."
         o Choose the port you wish to configure from the list in the 
           left-hand side of the window
         o You are now viewing the port features
         o To view the ports's Serial settings, click on the Serial 
           tab
         o To view the ports's Advanced settings, click on the 
           Advanced tab
         o For each port you want to configure, remember to choose the 
           port from the list in the left-hand side of the window 

     Each of the features discussed here is disabled (or blank) by 
     default.

     COM PORT NUMBER [PROPERTIES TAB]
     --------------------------------

     To change a port's COM name, select a different one from the 
     drop-down list of available COM ports.

     PORT NAME [PROPERTIES TAB]
     --------------------------

     Normally, the name of the port is displayed as the name of the 
     product followed by the logical port number and the port's COM 
     name.

      E.g., "Digi Connect ME Port 1 (COM5)"

     To replace the COM name with text of your choosing, enter a brief
     name here; it will be displayed in the Device Manager in place of 
     the COM name.

      E.g., "Digi Connect ME Port 1 (My first port)"

     PORT DESCRIPTION [PROPERTIES TAB]
     ---------------------------------

     The port description can be used any way you would like to store
     additional information about the port. It is only displayed on 
     the property page.

     SERIAL SETTINGS [SERIAL TAB]
     ----------------------------

     These settings are displayed on the Serial tab of the port's
     property page.

     The Serial settings are used by the Windows Print Spooler to 
     determine the default line settings of the port when printing. 
     The line settings include baud rate, number of data bits, type of 
     parity, number of stop bits, and type of flow control.

     To change the default print spooler settings, adjust the 
     Baud Rate, Data Bits, Parity, Stop Bits, and Flow Control
     settings using the drop-down lists.

     SEND KEEP-ALIVE PACKETS [ADVANCED TAB]
     --------------------------------------

     Sending keep-alive packets is a mechanism that allows the 
     Digi RealPort driver to periodically monitor its network 
     connection with the remote RealPort device. This allows the 
     driver to detect network disconnects that would otherwise go
     undetected.

     A keep-alive packet is simply an empty TCP packet sent to the 
     device.

     If keep-alive is enabled, the driver periodically sends a keep-
     alive packet to the device. If the driver does not receive an 
     acknowledgement from the device, it assumes the network 
     connection is broken.

     If the network connection is broken, the driver logs this
     information to the Event Viewer. Next, it tries to establish a 
     new connection. The driver will continue to try to establish a 
     new connection until it succeeds, until the driver is disabled, 
     or until keep-alive is turned off.

     Once a new network session is established, the driver logs 
     another event to the Event Viewer to inform you that a new 
     network session has been established with the device.

     If keep-alive is not enabled, the driver will still detect and
     log network events, but only when a port is open and there is 
     serial activity.

     ALWAYS GRANT PORT OPEN REQUESTS [ADVANCED TAB]
     ----------------------------------------------

     (Formerly, "Allow ports to be opened when network is 
     unavailable".)

     Turn on this feature if you want the RealPort driver to always 
     grant open requests, even if the driver cannot communicate with 
     the remote device at the time of the open, or even if the remote
     device rejects the open request.

     By default, the driver will only grant an open request if it can 
     communicate with the remote device, and only if the remote device 
     okays the open request. For instance, if you disconnect the  
     remote device from the network and then try to open one of its 
     ports, the RealPort driver will reject the open request, and your 
     application will not be able to open the port until the device is
     back on-line.

     If this option is enabled, note that the RealPort driver may 
     grant an open request even if the physical port on the remote 
     device is in use by another application. The RealPort driver will 
     continue to try to claim that port, but there is no guarantee 
     that it will ever succeed. To the local application, it has 
     opened the port, and it can read and write data, but the data 
     will not really be written until the RealPort driver can claim 
     the physical port, and the local application will not see any 
     read data until that time, either.

     COMPLETE WRITE REQUESTS IMMEDIATELY [ADVANCED TAB]
     --------------------------------------------------

     Checking this field means that the driver confirms the completion 
     of a write request as soon as the data is written to the buffer. 
     The default is to wait until the data is sent out the port.

     This option may cause an application to behave unexpectedly. For 
     instance, some applications need to know when data has been 
     physically written, and by telling the application that data has
     been written when it has not may confuse the application or cause
     it to fail.

     Conversely, some applications have strict timing requirements and 
     may fail if this option *is not* enabled.

     This feature is disabled by default.

     USE SERIAL API TO ALERT APPLICATION IF DEVICE GOES OFF-LINE [CUSTOM DIALOG]
     ---------------------------------------------------------------------------
     (Formerly, "Return error codes when network is unavailable".)

     ***************************************************************
     *** WARNING!! *************************************************
     This feature is for use only by custom applications. Enabling 
     this feature will likely cause an application to behave 
     erratically, unless it has been written exclusively to use this 
     feature. Please read these instructions carefully to learn how 
     use this feature with your custom application.
     ***************************************************************

     To access this feature, click the "Custom..." button, located 
     on the Advanced Tab of the port's property page.

     The RealPort driver supports two methods to handle Wide Area
     Network (WAN) errors, such as those generated when the
     RealPort driver loses its TCP/IP session with the remote
     RealPort device.

     By default, this feature is disabled and the RealPort driver
     seamlessly and transparently handles any and all network
     errors. The most common network error is that the driver
     loses its TCP/IP session with the remote RealPort device,
     either because the RealPort device is unreachable due to
     general network failures or because the RealPort device is
     rebooted or powered off.

     In the event that the RealPort driver loses its TCP/IP
     session with the remote RealPort device, the driver will
     attempt to reestablish a new connection with the device once
     every second. The driver will also simulate a loss of
     hardware flow control lines by lowering each of the following
     signals -- DCD, DSR, and CTS. It will also simulate receipt
     of an Xoff character, if software flow control is being used.

     In this mode, client applications will not and cannot detect
     network errors. Client applications are free to continue
     reading from and writing to RealPort ports, and to continue 
     using the ports as they otherwise normally would. In this way, 
     network errors are handled transparently from the client 
     application.

     However, it is sometimes useful for a client to know that a
     remote RealPort device is unreachable because the TCP/IP
     session is dropped. In this case, you can enable this feature, 
     and the RealPort driver will then communicate network errors to 
     the client application when the network is unreachable.

     If this feature is enabled, when the network connection is 
     dropped and a client initiates a read or a write (ReadFile or
     WriteFile request), the driver will immediately complete the 
     operation and return the error, ERROR_NETWORK_UNREACHABLE. 
     The driver will likewise complete all serial communication
     calls with the same response (i.e., ClearCommError, PurgeComm,
     SetCommState, etc.). A client application can be specifically 
     written to recognize this error code and to respond accordingly, 
     such as by logging the error to a file. The driver will continue 
     to return this error code until it is able to reestablish a 
     network connection with the remote RealPort device, at which 
     point the driver will resume normal processing of read, write,
     and serial requests.


 10. COPYRIGHT NOTICES

     The OpenSSL Project

     This product includes software developed by the OpenSSL Project 
     for use in the OpenSSL Toolkit (http://www.openssl.org/), 
     Copyright (c) 1998-2003 The OpenSSL Project. All rights reserved.

     Redistribution and use in source and binary forms, with or 
     without modification, are permitted provided that the following 
     conditions are met:

     1. Redistributions of source code must retain the above copyright 
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above 
        copyright notice, this list of conditions and the following 
        disclaimer in the documentation and/or other materials 
        provided with the distribution.

     3. All advertising materials mentioning features or use of this 
        software must display the following acknowledgment: "This 
        product includes software developed by the OpenSSL Project for 
        use in the OpenSSL Toolkit. (http://www.openssl.org/)"

     4. The names "OpenSSL Toolkit" and "OpenSSL Project" must not be 
        used to endorse or promote products derived from this software 
        without prior written permission. For written permission, 
        please contact openssl-core@openssl.org.

     5. Products derived from this software may not be called 
        "OpenSSL" nor may "OpenSSL" appear in their names without 
        prior written permission of the OpenSSL Project.

     6. Redistributions of any form whatsoever must retain the 
        following acknowledgment: "This product includes software 
        developed by the OpenSSL Project for use in the OpenSSL 
        Toolkit (http://www.openssl.org/)"

     THIS SOFTWARE IS PROVIDED BY THE OpenSSL PROJECT "AS IS" AND ANY 
     EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, 
     THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
     PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE OpenSSL 
     PROJECT OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
     INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
     (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS 
     OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
     INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
     WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
     NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF 
     THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

     This product includes cryptographic software written by Eric 
     Young (eay@cryptsoft.com). This product includes software written 
     by Tim Hudson (tjh@cryptsoft.com).


     Eric Young

     This product includes cryptographic software written by Eric 
     Young, Copyright (C) 1995-1998 Eric Young (eay@cryptsoft.com). 
     All rights reserved.

     Redistribution and use in source and binary forms, with or 
     without modification, are permitted provided that the following 
     conditions are met:

     1. Redistributions of source code must retain the copyright 
        notice, this list of conditions and the following disclaimer.

     2. Redistributions in binary form must reproduce the above 
        copyright notice, this list of conditions and the following 
        disclaimer in the documentation and/or other materials 
        provided with the distribution.

     3. All advertising materials mentioning features or use of this 
        software must display the following acknowledgement: "This 
        product includes cryptographic software written by Eric Young 
        (eay@cryptsoft.com)". The word 'cryptographic' can be left out 
        if the routines from the library being used are not 
        cryptographic related :-).

     4. If you include any Windows specific code (or a derivative 
        thereof) from the apps directory (application code) you must 
        include an acknowledgement: "This product includes software 
        written by Tim Hudson (tjh@cryptsoft.com)"

     THIS SOFTWARE IS PROVIDED BY ERIC YOUNG "AS IS" AND ANY EXPRESS 
     OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
     WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR 
     PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR 
     CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
     SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
     LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
     USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED 
     AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
     LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN 
     ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
     POSSIBILITY OF SUCH DAMAGE.

 
 11. ADDITIONAL NOTES

     Installation documentation can be found on the Digi Web site,
     http://www.digi.com, or on the FTP server, ftp://ftp.digi.com.


 12. HISTORY

     Version 3.0.226.0, 03/18/2004 (Rev. P)
     --------------------------------------------
     o Added Digi RealPort Setup Wizard for installing, updating,
       and removing RealPort device driver and devices
     o Updated Device Manager user interface.
     o Added registry option to force baud rate.
     o Added registry option to disable DOSMODE.
     o Improved Event Log messages in case of network disconnect.
     o Resolved issues with Complete write requests immediately.
     o Improved handling of network outage.
     o Added support for IOCTL_SERIAL_XOFF_COUNTER.
     o Added registry option to force xedata (ignore serial errors).
	 o Fixed support for IOCTL_SERIAL_IMMEDIATE_CHAR.
     o Fixed a condition where a network disconnect under certain
       conditions may cause a bugcheck.

     Version 2.8.94.0, 06/10/2003 (Rev. N)
     --------------------------------------------
     o Added support for Digi Connect ME.
     o Fixed a problem processing write requests after the input
       buffer is cleared with PurgeComm during network disconnect.
     o Corrected break event signaling.
     o Fixed detection of modem line transitions for EtherLite 2.

     Version 2.7.90.0, 04/22/2003 (Rev. M)
     --------------------------------------------
     o Added support for Microsoft Windows Server 2003
     o Improved management of outgoing network resources.
     o Fixed a condition where a network disconnect under certain
       conditions may cause a bugcheck.
     o Fixed RTS Toggle functionality. A RTS pre- and post-delay
       can be configured through the command line interface or the
       web interface of your Digi One or Digi PortServer.
     o Modified the Complete Writes Immediately feature to allow
       queued data to drain when the port is closed.
     o Fixed framing error detection to not insert bogus data
       into the read data stream.
     o Modified driver to default uninitialized ports to 1200,7,e,1,
       like the standard serial driver, and to maintain baud rate,
       line settings and flow control across opens.
     o Added code to force RTS low when device is closed; sometimes,
       if a port was using RTS/CTS flow control and was closed, RTS
       would remain high.
     o Added support for Digi One TS H, Digi PortServer TS 2 H,
       and Digi PortServer TS 4 H.
     o Added support for Digi One TS W, Digi PortServer TS 2 W,
       and Digi PortServer TS 4 W.

     Version 2.6.82.0, 01/31/2003 (Rev. L)
     --------------------------------------------
     o Added support for Digi One EM, Digi One IA and Digi One SP.
     o Fixed race condition when switching parity states.
     o Modified Complete Write Requests Immediately feature to work
       irregardless of flow control.
     o Added user interface option "Send keep alive packets" to control
       keep alive packets sent to the remote device. The default setting
       is disabled.
     o Fixed condition where default Xon and Xoff characters were
       not initialized correctly.
     o Fixed detection of parity and framing errors.
     o Fixed Xon/Xoff limit bounds checking.
     o Exposed GUI option to allow serial ports to be opened
       even if the driver cannot communicate with the remote device.
     o Improved driver load time when network is disconnected.
     o Changed event reporting to begin accumulating events once
       SetCommMask is called, instead of waiting for WaitCommMask.
