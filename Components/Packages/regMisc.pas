unit regMisc;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units

{$R mmVirtualStringTree.dcr}
{$R VirtualTreesReg.dcr}
{$R DataItemTree.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf,
  VCLThread, mmVirtualStringTree, DataItemTree;

//------------------------------------------------------------------------------
procedure MMRegister;
begin
  RegisterComponents('MillMaster', [TVCLThread, TmmVirtualStringTree, TDataItemTree]);
end;
//------------------------------------------------------------------------------
end.
