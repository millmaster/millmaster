unit regMillMaster;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R AssignComp.dcr}
{$R AssignHandlerComp.dcr}
{$R CloseHandlerComp.dcr}
{$R LotParameterHandlerComp.dcr}
{$R MaGrpNrFrame.dcr}
{$R MiscFrame.dcr}
{$R SettingsFrame.dcr}
{$R SettingsNavFrame.dcr}
{$R SpindleFieldsFrame.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf,
  AssignComp,
  AssignHandlerComp,
  CloseHandlerComp,
  LotParameterHandlerComp,
  MaGrpNrFrame,
  MiscFrame,
  SettingsFrame,
  SettingsNavFrame,
  SpindleFieldsFrame;

//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit AssignComp
  RegisterComponents('Assignments', [TAssEdit, TSpindleRangeList,TMachine,TGetSettingsHandler]);
  // Unit AssignHandlerComp
  RegisterComponents('Assignments', [TAssignHandler]);
  // Unit CloseHandlerComp
  RegisterComponents('Assignments', [TCloseHandler]);
  // Unit LotParameterHandlerComp
  RegisterComponents('Assignments', [TLotParameterHandler]);
  // Unit MaGrpNrFrame
  RegisterComponents('Assignments', [TMaGrpNr]);
  // Unit MiscFrame
  RegisterComponents('Assignments', [TMisc]);
  // Unit SettingsFrame
  RegisterComponents('Assignments', [TSettings]);
  // Unit SettingsNavFrame
  RegisterComponents('Assignments', [TSettingsNav]);
  // Unit SpindleFieldsFrame
  RegisterComponents('Assignments', [TSpindleFields]);
end;
//------------------------------------------------------------------------------
end.
