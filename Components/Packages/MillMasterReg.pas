(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MillMasterReg.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages from different units
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.08.2000  1.00  Wss | Initial Release
|=========================================================================================*)
unit MillMasterReg;

interface
//------------------------------------------------------------------------------
procedure Register;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  regAssignments,
  regMMConfiguration,
  regMisc,
  regMMU,
  regYMClearer,
  regYMClearerPrint,
  regDataLayer,
  regWorbisMMU,
  regXMLSettings;

//------------------------------------------------------------------------------
procedure Register;
begin
  regAssignments.MMRegister;
  regMMConfiguration.MMRegister;
  regMisc.MMRegister;
  regMMU.MMRegister;
  regYMClearer.MMRegister;
  regYMClearerPrint.MMRegister;
  regDataLayer.MMRegister;
  regWorbisMMU.MMRegister;
  regXMLSettings.MMRegister;
end;
//------------------------------------------------------------------------------
end.
