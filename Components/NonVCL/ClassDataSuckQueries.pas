(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: ClassDataStuck.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt die Queries fuer den DB-Zugriff der der Klassiermatrix
|
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 19.05.1999  1.00  Mg  | Datei erstellt
| 16.02.2000  1.01  Mg  | where Klauseln mit style_id <> 2 erweitert
| 29.08.2000  1.02  Mg  | style_id <> 2 aus where klausel entfernt
|                       | Alle PrimID in IN (...) an stelle c_yyyyxxx = :PrimID
| 05.09.2000  1.03  Mg  | Queries fuer Schichten angepasst : kein group by mehr
|=============================================================================*)
unit ClassDataSuckQueries;

interface
uses MMUGlobal;
type
//..............................................................................
   TParRec = record
     Table      : String;
     IDColumn   : String;
     ColumnPart : String;
     ColumnSum  : String;
   end;
const
//..............................................................................

//********************Queries for TClassDataSuck********************************
//..............................................................................
  cQuerySpindleInterval =
    'select i.c_interval_start,  c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   p.c_prod_id = sp.c_prod_id ' +
    'and   sp.c_spindle_id in ( _PrimID ) ' +
    'and   sp.c_machine_id = :SecID ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryProdGrpInterval =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   p.c_prod_id = sp.c_prod_id ' +
    'and   sp.c_prod_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryMachineInterval =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   p.c_prod_id = sp.c_prod_id ' +
    'and   sp.c_machine_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryOrderPositionInterval =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.c_prod_id ' +
    'and   p.c_order_position_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryStyleInterval =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.prod_id ' +
    'and   p.c_style_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryAssortmentInterval =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.prod_id ' +
    'and   p.c_assortment_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryTableInterval : array [dlSpindle .. dlAssortment] of PChar =
    ( cQuerySpindleInterval, cQueryProdGrpInterval, cQueryMachineInterval,
      cQueryOrderPositionInterval, cQueryStyleInterval, cQueryAssortmentInterval );
//..............................................................................
//..............................................................................
  cQueryPartClassUncut =
    'sum ( convert ( float, c_cU1) ) c_cU1, sum ( convert ( float, c_cU2) ) c_cU2,' +
    'sum ( convert ( float, c_cU3) ) c_cU3, sum ( convert ( float, c_cU4) ) c_cU4,' +
    'sum ( convert ( float, c_cU5) ) c_cU5, sum ( convert ( float, c_cU6) ) c_cU6,' +
    'sum ( convert ( float, c_cU7) ) c_cU7, sum ( convert ( float, c_cU8) ) c_cU8,' +
    'sum ( convert ( float, c_cU9) ) c_cU9, sum ( convert ( float, c_cU10) ) c_cU10,' +
    'sum ( convert ( float, c_cU11) ) c_cU11, sum ( convert ( float, c_cU12) ) c_cU12,' +
    'sum ( convert ( float, c_cU13) ) c_cU13, sum ( convert ( float, c_cU14) ) c_cU14,' +
    'sum ( convert ( float, c_cU15) ) c_cU15, sum ( convert ( float, c_cU16) ) c_cU16,' +
    'sum ( convert ( float, c_cU17) ) c_cU17, sum ( convert ( float, c_cU18) ) c_cU18,' +
    'sum ( convert ( float, c_cU19) ) c_cU19, sum ( convert ( float, c_cU20) ) c_cU20,' +
    'sum ( convert ( float, c_cU21) ) c_cU21, sum ( convert ( float, c_cU22) ) c_cU22,' +
    'sum ( convert ( float, c_cU23) ) c_cU23, sum ( convert ( float, c_cU24) ) c_cU24,' +
    'sum ( convert ( float, c_cU25) ) c_cU25, sum ( convert ( float, c_cU26) ) c_cU26,' +
    'sum ( convert ( float, c_cU27) ) c_cU27, sum ( convert ( float, c_cU28) ) c_cU28,' +
    'sum ( convert ( float, c_cU29) ) c_cU29, sum ( convert ( float, c_cU30) ) c_cU30,' +
    'sum ( convert ( float, c_cU31) ) c_cU31, sum ( convert ( float, c_cU32) ) c_cU32,' +
    'sum ( convert ( float, c_cU33) ) c_cU33, sum ( convert ( float, c_cU34) ) c_cU34,' +
    'sum ( convert ( float, c_cU35) ) c_cU35, sum ( convert ( float, c_cU36) ) c_cU36,' +
    'sum ( convert ( float, c_cU37) ) c_cU37, sum ( convert ( float, c_cU38) ) c_cU38,' +
    'sum ( convert ( float, c_cU39) ) c_cU39, sum ( convert ( float, c_cU40) ) c_cU40,' +
    'sum ( convert ( float, c_cU41) ) c_cU41, sum ( convert ( float, c_cU42) ) c_cU42,' +
    'sum ( convert ( float, c_cU43) ) c_cU43, sum ( convert ( float, c_cU44) ) c_cU44,' +
    'sum ( convert ( float, c_cU45) ) c_cU45, sum ( convert ( float, c_cU46) ) c_cU46,' +
    'sum ( convert ( float, c_cU47) ) c_cU47, sum ( convert ( float, c_cU48) ) c_cU48,' +
    'sum ( convert ( float, c_cU49) ) c_cU49, sum ( convert ( float, c_cU50) ) c_cU50,' +
    'sum ( convert ( float, c_cU51) ) c_cU51, sum ( convert ( float, c_cU52) ) c_cU52,' +
    'sum ( convert ( float, c_cU53) ) c_cU53, sum ( convert ( float, c_cU54) ) c_cU54,' +
    'sum ( convert ( float, c_cU55) ) c_cU55, sum ( convert ( float, c_cU56) ) c_cU56,' +
    'sum ( convert ( float, c_cU57) ) c_cU57, sum ( convert ( float, c_cU58) ) c_cU58,' +
    'sum ( convert ( float, c_cU59) ) c_cU59, sum ( convert ( float, c_cU60) ) c_cU60,' +
    'sum ( convert ( float, c_cU61) ) c_cU61, sum ( convert ( float, c_cU62) ) c_cU62,' +
    'sum ( convert ( float, c_cU63) ) c_cU63, sum ( convert ( float, c_cU64) ) c_cU64,' +
    'sum ( convert ( float, c_cU65) ) c_cU65, sum ( convert ( float, c_cU66) ) c_cU66,' +
    'sum ( convert ( float, c_cU67) ) c_cU67, sum ( convert ( float, c_cU68) ) c_cU68,' +
    'sum ( convert ( float, c_cU69) ) c_cU69, sum ( convert ( float, c_cU70) ) c_cU70,' +
    'sum ( convert ( float, c_cU71) ) c_cU71, sum ( convert ( float, c_cU72) ) c_cU72,' +
    'sum ( convert ( float, c_cU73) ) c_cU73, sum ( convert ( float, c_cU74) ) c_cU74,' +
    'sum ( convert ( float, c_cU75) ) c_cU75, sum ( convert ( float, c_cU76) ) c_cU76,' +
    'sum ( convert ( float, c_cU77) ) c_cU77, sum ( convert ( float, c_cU78) ) c_cU78,' +
    'sum ( convert ( float, c_cU79) ) c_cU79, sum ( convert ( float, c_cU80) ) c_cU80,' +
    'sum ( convert ( float, c_cU81) ) c_cU81, sum ( convert ( float, c_cU82) ) c_cU82,' +
    'sum ( convert ( float, c_cU83) ) c_cU83, sum ( convert ( float, c_cU84) ) c_cU84,' +
    'sum ( convert ( float, c_cU85) ) c_cU85, sum ( convert ( float, c_cU86) ) c_cU86,' +
    'sum ( convert ( float, c_cU87) ) c_cU87, sum ( convert ( float, c_cU88) ) c_cU88,' +
    'sum ( convert ( float, c_cU89) ) c_cU89, sum ( convert ( float, c_cU90) ) c_cU90,' +
    'sum ( convert ( float, c_cU91) ) c_cU91, sum ( convert ( float, c_cU92) ) c_cU92,' +
    'sum ( convert ( float, c_cU93) ) c_cU93, sum ( convert ( float, c_cU94) ) c_cU94,' +
    'sum ( convert ( float, c_cU95) ) c_cU95, sum ( convert ( float, c_cU96) ) c_cU96,' +
    'sum ( convert ( float, c_cU97) ) c_cU97, sum ( convert ( float, c_cU98) ) c_cU98,' +
    'sum ( convert ( float, c_cU99) ) c_cU99, sum ( convert ( float, c_cU100) ) c_cU100,' +
    'sum ( convert ( float, c_cU101) ) c_cU101, sum ( convert ( float, c_cU102) ) c_cU102,' +
    'sum ( convert ( float, c_cU103) ) c_cU103, sum ( convert ( float, c_cU104) ) c_cU104,' +
    'sum ( convert ( float, c_cU105) ) c_cU105, sum ( convert ( float, c_cU106) ) c_cU106,' +
    'sum ( convert ( float, c_cU107) ) c_cU107, sum ( convert ( float, c_cU108) ) c_cU108,' +
    'sum ( convert ( float, c_cU109) ) c_cU109, sum ( convert ( float, c_cU110) ) c_cU110,' +
    'sum ( convert ( float, c_cU111) ) c_cU111, sum ( convert ( float, c_cU112) ) c_cU112,' +
    'sum ( convert ( float, c_cU113) ) c_cU113, sum ( convert ( float, c_cU114) ) c_cU114,' +
    'sum ( convert ( float, c_cU115) ) c_cU115, sum ( convert ( float, c_cU116) ) c_cU116,' +
    'sum ( convert ( float, c_cU117) ) c_cU117, sum ( convert ( float, c_cU118) ) c_cU118,' +
    'sum ( convert ( float, c_cU119) ) c_cU119, sum ( convert ( float, c_cU120) ) c_cU120,' +
    'sum ( convert ( float, c_cU121) ) c_cU121, sum ( convert ( float, c_cU122) ) c_cU122,' +
    'sum ( convert ( float, c_cU123) ) c_cU123, sum ( convert ( float, c_cU124) ) c_cU124,' +
    'sum ( convert ( float, c_cU125) ) c_cU125, sum ( convert ( float, c_cU126) ) c_cU126,' +
    'sum ( convert ( float, c_cU127) ) c_cU127, sum ( convert ( float, c_cU128) ) c_cU128 ';
//..............................................................................
  cQueryPartClassCut =
    'sum ( convert ( float, c_cC1) ) c_cC1, sum ( convert ( float, c_cC2) ) c_cC2,' +
    'sum ( convert ( float, c_cC3) ) c_cC3, sum ( convert ( float, c_cC4) ) c_cC4,' +
    'sum ( convert ( float, c_cC5) ) c_cC5, sum ( convert ( float, c_cC6) ) c_cC6,' +
    'sum ( convert ( float, c_cC7) ) c_cC7, sum ( convert ( float, c_cC8) ) c_cC8,' +
    'sum ( convert ( float, c_cC9) ) c_cC9, sum ( convert ( float, c_cC10) ) c_cC10,' +
    'sum ( convert ( float, c_cC11) ) c_cC11, sum ( convert ( float, c_cC12) ) c_cC12,' +
    'sum ( convert ( float, c_cC13) ) c_cC13, sum ( convert ( float, c_cC14) ) c_cC14,' +
    'sum ( convert ( float, c_cC15) ) c_cC15, sum ( convert ( float, c_cC16) ) c_cC16,' +
    'sum ( convert ( float, c_cC17) ) c_cC17, sum ( convert ( float, c_cC18) ) c_cC18,' +
    'sum ( convert ( float, c_cC19) ) c_cC19, sum ( convert ( float, c_cC20) ) c_cC20,' +
    'sum ( convert ( float, c_cC21) ) c_cC21, sum ( convert ( float, c_cC22) ) c_cC22,' +
    'sum ( convert ( float, c_cC23) ) c_cC23, sum ( convert ( float, c_cC24) ) c_cC24,' +
    'sum ( convert ( float, c_cC25) ) c_cC25, sum ( convert ( float, c_cC26) ) c_cC26,' +
    'sum ( convert ( float, c_cC27) ) c_cC27, sum ( convert ( float, c_cC28) ) c_cC28,' +
    'sum ( convert ( float, c_cC29) ) c_cC29, sum ( convert ( float, c_cC30) ) c_cC30,' +
    'sum ( convert ( float, c_cC31) ) c_cC31, sum ( convert ( float, c_cC32) ) c_cC32,' +
    'sum ( convert ( float, c_cC33) ) c_cC33, sum ( convert ( float, c_cC34) ) c_cC34,' +
    'sum ( convert ( float, c_cC35) ) c_cC35, sum ( convert ( float, c_cC36) ) c_cC36,' +
    'sum ( convert ( float, c_cC37) ) c_cC37, sum ( convert ( float, c_cC38) ) c_cC38,' +
    'sum ( convert ( float, c_cC39) ) c_cC39, sum ( convert ( float, c_cC40) ) c_cC40,' +
    'sum ( convert ( float, c_cC41) ) c_cC41, sum ( convert ( float, c_cC42) ) c_cC42,' +
    'sum ( convert ( float, c_cC43) ) c_cC43, sum ( convert ( float, c_cC44) ) c_cC44,' +
    'sum ( convert ( float, c_cC45) ) c_cC45, sum ( convert ( float, c_cC46) ) c_cC46,' +
    'sum ( convert ( float, c_cC47) ) c_cC47, sum ( convert ( float, c_cC48) ) c_cC48,' +
    'sum ( convert ( float, c_cC49) ) c_cC49, sum ( convert ( float, c_cC50) ) c_cC50,' +
    'sum ( convert ( float, c_cC51) ) c_cC51, sum ( convert ( float, c_cC52) ) c_cC52,' +
    'sum ( convert ( float, c_cC53) ) c_cC53, sum ( convert ( float, c_cC54) ) c_cC54,' +
    'sum ( convert ( float, c_cC55) ) c_cC55, sum ( convert ( float, c_cC56) ) c_cC56,' +
    'sum ( convert ( float, c_cC57) ) c_cC57, sum ( convert ( float, c_cC58) ) c_cC58,' +
    'sum ( convert ( float, c_cC59) ) c_cC59, sum ( convert ( float, c_cC60) ) c_cC60,' +
    'sum ( convert ( float, c_cC61) ) c_cC61, sum ( convert ( float, c_cC62) ) c_cC62,' +
    'sum ( convert ( float, c_cC63) ) c_cC63, sum ( convert ( float, c_cC64) ) c_cC64,' +
    'sum ( convert ( float, c_cC65) ) c_cC65, sum ( convert ( float, c_cC66) ) c_cC66,' +
    'sum ( convert ( float, c_cC67) ) c_cC67, sum ( convert ( float, c_cC68) ) c_cC68,' +
    'sum ( convert ( float, c_cC69) ) c_cC69, sum ( convert ( float, c_cC70) ) c_cC70,' +
    'sum ( convert ( float, c_cC71) ) c_cC71, sum ( convert ( float, c_cC72) ) c_cC72,' +
    'sum ( convert ( float, c_cC73) ) c_cC73, sum ( convert ( float, c_cC74) ) c_cC74,' +
    'sum ( convert ( float, c_cC75) ) c_cC75, sum ( convert ( float, c_cC76) ) c_cC76,' +
    'sum ( convert ( float, c_cC77) ) c_cC77, sum ( convert ( float, c_cC78) ) c_cC78,' +
    'sum ( convert ( float, c_cC79) ) c_cC79, sum ( convert ( float, c_cC80) ) c_cC80,' +
    'sum ( convert ( float, c_cC81) ) c_cC81, sum ( convert ( float, c_cC82) ) c_cC82,' +
    'sum ( convert ( float, c_cC83) ) c_cC83, sum ( convert ( float, c_cC84) ) c_cC84,' +
    'sum ( convert ( float, c_cC85) ) c_cC85, sum ( convert ( float, c_cC86) ) c_cC86,' +
    'sum ( convert ( float, c_cC87) ) c_cC87, sum ( convert ( float, c_cC88) ) c_cC88,' +
    'sum ( convert ( float, c_cC89) ) c_cC89, sum ( convert ( float, c_cC90) ) c_cC90,' +
    'sum ( convert ( float, c_cC91) ) c_cC91, sum ( convert ( float, c_cC92) ) c_cC92,' +
    'sum ( convert ( float, c_cC93) ) c_cC93, sum ( convert ( float, c_cC94) ) c_cC94,' +
    'sum ( convert ( float, c_cC95) ) c_cC95, sum ( convert ( float, c_cC96) ) c_cC96,' +
    'sum ( convert ( float, c_cC97) ) c_cC97, sum ( convert ( float, c_cC98) ) c_cC98,' +
    'sum ( convert ( float, c_cC99) ) c_cC99, sum ( convert ( float, c_cC100) ) c_cC100,' +
    'sum ( convert ( float, c_cC101) ) c_cC101, sum ( convert ( float, c_cC102) ) c_cC102,' +
    'sum ( convert ( float, c_cC103) ) c_cC103, sum ( convert ( float, c_cC104) ) c_cC104,' +
    'sum ( convert ( float, c_cC105) ) c_cC105, sum ( convert ( float, c_cC106) ) c_cC106,' +
    'sum ( convert ( float, c_cC107) ) c_cC107, sum ( convert ( float, c_cC108) ) c_cC108,' +
    'sum ( convert ( float, c_cC109) ) c_cC109, sum ( convert ( float, c_cC110) ) c_cC110,' +
    'sum ( convert ( float, c_cC111) ) c_cC111, sum ( convert ( float, c_cC112) ) c_cC112,' +
    'sum ( convert ( float, c_cC113) ) c_cC113, sum ( convert ( float, c_cC114) ) c_cC114,' +
    'sum ( convert ( float, c_cC115) ) c_cC115, sum ( convert ( float, c_cC116) ) c_cC116,' +
    'sum ( convert ( float, c_cC117) ) c_cC117, sum ( convert ( float, c_cC118) ) c_cC118,' +
    'sum ( convert ( float, c_cC119) ) c_cC119, sum ( convert ( float, c_cC120) ) c_cC120,' +
    'sum ( convert ( float, c_cC121) ) c_cC121, sum ( convert ( float, c_cC122) ) c_cC122,' +
    'sum ( convert ( float, c_cC123) ) c_cC123, sum ( convert ( float, c_cC124) ) c_cC124,' +
    'sum ( convert ( float, c_cC125) ) c_cC125, sum ( convert ( float, c_cC126) ) c_cC126,' +
    'sum ( convert ( float, c_cC127) ) c_cC127, sum ( convert ( float, c_cC128) ) c_cC128 ';
    
//..............................................................................
  cQueryPartSpliceUncut =
    'sum ( convert ( float, c_spU1) ) c_spU1, sum ( convert ( float, c_spU2) ) c_spU2,' +
    'sum ( convert ( float, c_spU3) ) c_spU3, sum ( convert ( float, c_spU4) ) c_spU4,' +
    'sum ( convert ( float, c_spU5) ) c_spU5, sum ( convert ( float, c_spU6) ) c_spU6,' +
    'sum ( convert ( float, c_spU7) ) c_spU7, sum ( convert ( float, c_spU8) ) c_spU8,' +
    'sum ( convert ( float, c_spU9) ) c_spU9, sum ( convert ( float, c_spU10) ) c_spU10,' +
    'sum ( convert ( float, c_spU11) ) c_spU11, sum ( convert ( float, c_spU12) ) c_spU12,' +
    'sum ( convert ( float, c_spU13) ) c_spU13, sum ( convert ( float, c_spU14) ) c_spU14,' +
    'sum ( convert ( float, c_spU15) ) c_spU15, sum ( convert ( float, c_spU16) ) c_spU16,' +
    'sum ( convert ( float, c_spU17) ) c_spU17, sum ( convert ( float, c_spU18) ) c_spU18,' +
    'sum ( convert ( float, c_spU19) ) c_spU19, sum ( convert ( float, c_spU20) ) c_spU20,' +
    'sum ( convert ( float, c_spU21) ) c_spU21, sum ( convert ( float, c_spU22) ) c_spU22,' +
    'sum ( convert ( float, c_spU23) ) c_spU23, sum ( convert ( float, c_spU24) ) c_spU24,' +
    'sum ( convert ( float, c_spU25) ) c_spU25, sum ( convert ( float, c_spU26) ) c_spU26,' +
    'sum ( convert ( float, c_spU27) ) c_spU27, sum ( convert ( float, c_spU28) ) c_spU28,' +
    'sum ( convert ( float, c_spU29) ) c_spU29, sum ( convert ( float, c_spU30) ) c_spU30,' +
    'sum ( convert ( float, c_spU31) ) c_spU31, sum ( convert ( float, c_spU32) ) c_spU32,' +
    'sum ( convert ( float, c_spU33) ) c_spU33, sum ( convert ( float, c_spU34) ) c_spU34,' +
    'sum ( convert ( float, c_spU35) ) c_spU35, sum ( convert ( float, c_spU36) ) c_spU36,' +
    'sum ( convert ( float, c_spU37) ) c_spU37, sum ( convert ( float, c_spU38) ) c_spU38,' +
    'sum ( convert ( float, c_spU39) ) c_spU39, sum ( convert ( float, c_spU40) ) c_spU40,' +
    'sum ( convert ( float, c_spU41) ) c_spU41, sum ( convert ( float, c_spU42) ) c_spU42,' +
    'sum ( convert ( float, c_spU43) ) c_spU43, sum ( convert ( float, c_spU44) ) c_spU44,' +
    'sum ( convert ( float, c_spU45) ) c_spU45, sum ( convert ( float, c_spU46) ) c_spU46,' +
    'sum ( convert ( float, c_spU47) ) c_spU47, sum ( convert ( float, c_spU48) ) c_spU48,' +
    'sum ( convert ( float, c_spU49) ) c_spU49, sum ( convert ( float, c_spU50) ) c_spU50,' +
    'sum ( convert ( float, c_spU51) ) c_spU51, sum ( convert ( float, c_spU52) ) c_spU52,' +
    'sum ( convert ( float, c_spU53) ) c_spU53, sum ( convert ( float, c_spU54) ) c_spU54,' +
    'sum ( convert ( float, c_spU55) ) c_spU55, sum ( convert ( float, c_spU56) ) c_spU56,' +
    'sum ( convert ( float, c_spU57) ) c_spU57, sum ( convert ( float, c_spU58) ) c_spU58,' +
    'sum ( convert ( float, c_spU59) ) c_spU59, sum ( convert ( float, c_spU60) ) c_spU60,' +
    'sum ( convert ( float, c_spU61) ) c_spU61, sum ( convert ( float, c_spU62) ) c_spU62,' +
    'sum ( convert ( float, c_spU63) ) c_spU63, sum ( convert ( float, c_spU64) ) c_spU64,' +
    'sum ( convert ( float, c_spU65) ) c_spU65, sum ( convert ( float, c_spU66) ) c_spU66,' +
    'sum ( convert ( float, c_spU67) ) c_spU67, sum ( convert ( float, c_spU68) ) c_spU68,' +
    'sum ( convert ( float, c_spU69) ) c_spU69, sum ( convert ( float, c_spU70) ) c_spU70,' +
    'sum ( convert ( float, c_spU71) ) c_spU71, sum ( convert ( float, c_spU72) ) c_spU72,' +
    'sum ( convert ( float, c_spU73) ) c_spU73, sum ( convert ( float, c_spU74) ) c_spU74,' +
    'sum ( convert ( float, c_spU75) ) c_spU75, sum ( convert ( float, c_spU76) ) c_spU76,' +
    'sum ( convert ( float, c_spU77) ) c_spU77, sum ( convert ( float, c_spU78) ) c_spU78,' +
    'sum ( convert ( float, c_spU79) ) c_spU79, sum ( convert ( float, c_spU80) ) c_spU80,' +
    'sum ( convert ( float, c_spU81) ) c_spU81, sum ( convert ( float, c_spU82) ) c_spU82,' +
    'sum ( convert ( float, c_spU83) ) c_spU83, sum ( convert ( float, c_spU84) ) c_spU84,' +
    'sum ( convert ( float, c_spU85) ) c_spU85, sum ( convert ( float, c_spU86) ) c_spU86,' +
    'sum ( convert ( float, c_spU87) ) c_spU87, sum ( convert ( float, c_spU88) ) c_spU88,' +
    'sum ( convert ( float, c_spU89) ) c_spU89, sum ( convert ( float, c_spU90) ) c_spU90,' +
    'sum ( convert ( float, c_spU91) ) c_spU91, sum ( convert ( float, c_spU92) ) c_spU92,' +
    'sum ( convert ( float, c_spU93) ) c_spU93, sum ( convert ( float, c_spU94) ) c_spU94,' +
    'sum ( convert ( float, c_spU95) ) c_spU95, sum ( convert ( float, c_spU96) ) c_spU96,' +
    'sum ( convert ( float, c_spU97) ) c_spU97, sum ( convert ( float, c_spU98) ) c_spU98,' +
    'sum ( convert ( float, c_spU99) ) c_spU99, sum ( convert ( float, c_spU100) ) c_spU100,' +
    'sum ( convert ( float, c_spU101) ) c_spU101, sum ( convert ( float, c_spU102) ) c_spU102,' +
    'sum ( convert ( float, c_spU103) ) c_spU103, sum ( convert ( float, c_spU104) ) c_spU104,' +
    'sum ( convert ( float, c_spU105) ) c_spU105, sum ( convert ( float, c_spU106) ) c_spU106,' +
    'sum ( convert ( float, c_spU107) ) c_spU107, sum ( convert ( float, c_spU108) ) c_spU108,' +
    'sum ( convert ( float, c_spU109) ) c_spU109, sum ( convert ( float, c_spU110) ) c_spU110,' +
    'sum ( convert ( float, c_spU111) ) c_spU111, sum ( convert ( float, c_spU112) ) c_spU112,' +
    'sum ( convert ( float, c_spU113) ) c_spU113, sum ( convert ( float, c_spU114) ) c_spU114,' +
    'sum ( convert ( float, c_spU115) ) c_spU115, sum ( convert ( float, c_spU116) ) c_spU116,' +
    'sum ( convert ( float, c_spU117) ) c_spU117, sum ( convert ( float, c_spU118) ) c_spU118,' +
    'sum ( convert ( float, c_spU119) ) c_spU119, sum ( convert ( float, c_spU120) ) c_spU120,' +
    'sum ( convert ( float, c_spU121) ) c_spU121, sum ( convert ( float, c_spU122) ) c_spU122,' +
    'sum ( convert ( float, c_spU123) ) c_spU123, sum ( convert ( float, c_spU124) ) c_spU124,' +
    'sum ( convert ( float, c_spU125) ) c_spU125, sum ( convert ( float, c_spU126) ) c_spU126,' +
    'sum ( convert ( float, c_spU127) ) c_spU127, sum ( convert ( float, c_spU128) ) c_spU128 ';
//..............................................................................
  cQueryPartSpliceCut =
    'sum ( convert ( float, c_spC1) ) c_spC1, sum ( convert ( float, c_spC2) ) c_spC2,' +
    'sum ( convert ( float, c_spC3) ) c_spC3, sum ( convert ( float, c_spC4) ) c_spC4,' +
    'sum ( convert ( float, c_spC5) ) c_spC5, sum ( convert ( float, c_spC6) ) c_spC6,' +
    'sum ( convert ( float, c_spC7) ) c_spC7, sum ( convert ( float, c_spC8) ) c_spC8,' +
    'sum ( convert ( float, c_spC9) ) c_spC9, sum ( convert ( float, c_spC10) ) c_spC10,' +
    'sum ( convert ( float, c_spC11) ) c_spC11, sum ( convert ( float, c_spC12) ) c_spC12,' +
    'sum ( convert ( float, c_spC13) ) c_spC13, sum ( convert ( float, c_spC14) ) c_spC14,' +
    'sum ( convert ( float, c_spC15) ) c_spC15, sum ( convert ( float, c_spC16) ) c_spC16,' +
    'sum ( convert ( float, c_spC17) ) c_spC17, sum ( convert ( float, c_spC18) ) c_spC18,' +
    'sum ( convert ( float, c_spC19) ) c_spC19, sum ( convert ( float, c_spC20) ) c_spC20,' +
    'sum ( convert ( float, c_spC21) ) c_spC21, sum ( convert ( float, c_spC22) ) c_spC22,' +
    'sum ( convert ( float, c_spC23) ) c_spC23, sum ( convert ( float, c_spC24) ) c_spC24,' +
    'sum ( convert ( float, c_spC25) ) c_spC25, sum ( convert ( float, c_spC26) ) c_spC26,' +
    'sum ( convert ( float, c_spC27) ) c_spC27, sum ( convert ( float, c_spC28) ) c_spC28,' +
    'sum ( convert ( float, c_spC29) ) c_spC29, sum ( convert ( float, c_spC30) ) c_spC30,' +
    'sum ( convert ( float, c_spC31) ) c_spC31, sum ( convert ( float, c_spC32) ) c_spC32,' +
    'sum ( convert ( float, c_spC33) ) c_spC33, sum ( convert ( float, c_spC34) ) c_spC34,' +
    'sum ( convert ( float, c_spC35) ) c_spC35, sum ( convert ( float, c_spC36) ) c_spC36,' +
    'sum ( convert ( float, c_spC37) ) c_spC37, sum ( convert ( float, c_spC38) ) c_spC38,' +
    'sum ( convert ( float, c_spC39) ) c_spC39, sum ( convert ( float, c_spC40) ) c_spC40,' +
    'sum ( convert ( float, c_spC41) ) c_spC41, sum ( convert ( float, c_spC42) ) c_spC42,' +
    'sum ( convert ( float, c_spC43) ) c_spC43, sum ( convert ( float, c_spC44) ) c_spC44,' +
    'sum ( convert ( float, c_spC45) ) c_spC45, sum ( convert ( float, c_spC46) ) c_spC46,' +
    'sum ( convert ( float, c_spC47) ) c_spC47, sum ( convert ( float, c_spC48) ) c_spC48,' +
    'sum ( convert ( float, c_spC49) ) c_spC49, sum ( convert ( float, c_spC50) ) c_spC50,' +
    'sum ( convert ( float, c_spC51) ) c_spC51, sum ( convert ( float, c_spC52) ) c_spC52,' +
    'sum ( convert ( float, c_spC53) ) c_spC53, sum ( convert ( float, c_spC54) ) c_spC54,' +
    'sum ( convert ( float, c_spC55) ) c_spC55, sum ( convert ( float, c_spC56) ) c_spC56,' +
    'sum ( convert ( float, c_spC57) ) c_spC57, sum ( convert ( float, c_spC58) ) c_spC58,' +
    'sum ( convert ( float, c_spC59) ) c_spC59, sum ( convert ( float, c_spC60) ) c_spC60,' +
    'sum ( convert ( float, c_spC61) ) c_spC61, sum ( convert ( float, c_spC62) ) c_spC62,' +
    'sum ( convert ( float, c_spC63) ) c_spC63, sum ( convert ( float, c_spC64) ) c_spC64,' +
    'sum ( convert ( float, c_spC65) ) c_spC65, sum ( convert ( float, c_spC66) ) c_spC66,' +
    'sum ( convert ( float, c_spC67) ) c_spC67, sum ( convert ( float, c_spC68) ) c_spC68,' +
    'sum ( convert ( float, c_spC69) ) c_spC69, sum ( convert ( float, c_spC70) ) c_spC70,' +
    'sum ( convert ( float, c_spC71) ) c_spC71, sum ( convert ( float, c_spC72) ) c_spC72,' +
    'sum ( convert ( float, c_spC73) ) c_spC73, sum ( convert ( float, c_spC74) ) c_spC74,' +
    'sum ( convert ( float, c_spC75) ) c_spC75, sum ( convert ( float, c_spC76) ) c_spC76,' +
    'sum ( convert ( float, c_spC77) ) c_spC77, sum ( convert ( float, c_spC78) ) c_spC78,' +
    'sum ( convert ( float, c_spC79) ) c_spC79, sum ( convert ( float, c_spC80) ) c_spC80,' +
    'sum ( convert ( float, c_spC81) ) c_spC81, sum ( convert ( float, c_spC82) ) c_spC82,' +
    'sum ( convert ( float, c_spC83) ) c_spC83, sum ( convert ( float, c_spC84) ) c_spC84,' +
    'sum ( convert ( float, c_spC85) ) c_spC85, sum ( convert ( float, c_spC86) ) c_spC86,' +
    'sum ( convert ( float, c_spC87) ) c_spC87, sum ( convert ( float, c_spC88) ) c_spC88,' +
    'sum ( convert ( float, c_spC89) ) c_spC89, sum ( convert ( float, c_spC90) ) c_spC90,' +
    'sum ( convert ( float, c_spC91) ) c_spC91, sum ( convert ( float, c_spC92) ) c_spC92,' +
    'sum ( convert ( float, c_spC93) ) c_spC93, sum ( convert ( float, c_spC94) ) c_spC94,' +
    'sum ( convert ( float, c_spC95) ) c_spC95, sum ( convert ( float, c_spC96) ) c_spC96,' +
    'sum ( convert ( float, c_spC97) ) c_spC97, sum ( convert ( float, c_spC98) ) c_spC98,' +
    'sum ( convert ( float, c_spC99) ) c_spC99, sum ( convert ( float, c_spC100) ) c_spC100,' +
    'sum ( convert ( float, c_spC101) ) c_spC101, sum ( convert ( float, c_spC102) ) c_spC102,' +
    'sum ( convert ( float, c_spC103) ) c_spC103, sum ( convert ( float, c_spC104) ) c_spC104,' +
    'sum ( convert ( float, c_spC105) ) c_spC105, sum ( convert ( float, c_spC106) ) c_spC106,' +
    'sum ( convert ( float, c_spC107) ) c_spC107, sum ( convert ( float, c_spC108) ) c_spC108,' +
    'sum ( convert ( float, c_spC109) ) c_spC109, sum ( convert ( float, c_spC110) ) c_spC110,' +
    'sum ( convert ( float, c_spC111) ) c_spC111, sum ( convert ( float, c_spC112) ) c_spC112,' +
    'sum ( convert ( float, c_spC113) ) c_spC113, sum ( convert ( float, c_spC114) ) c_spC114,' +
    'sum ( convert ( float, c_spC115) ) c_spC115, sum ( convert ( float, c_spC116) ) c_spC116,' +
    'sum ( convert ( float, c_spC117) ) c_spC117, sum ( convert ( float, c_spC118) ) c_spC118,' +
    'sum ( convert ( float, c_spC119) ) c_spC119, sum ( convert ( float, c_spC120) ) c_spC120,' +
    'sum ( convert ( float, c_spC121) ) c_spC121, sum ( convert ( float, c_spC122) ) c_spC122,' +
    'sum ( convert ( float, c_spC123) ) c_spC123, sum ( convert ( float, c_spC124) ) c_spC124,' +
    'sum ( convert ( float, c_spC125) ) c_spC125, sum ( convert ( float, c_spC126) ) c_spC126,' +
    'sum ( convert ( float, c_spC127) ) c_spC127, sum ( convert ( float, c_spC128) ) c_spC128 ';
//..............................................................................
  cQueryPartSIROUncut =
    'sum ( convert ( float, c_siU1) ) c_siU1, sum ( convert ( float, c_siU2) ) c_siU2,' +
    'sum ( convert ( float, c_siU3) ) c_siU3, sum ( convert ( float, c_siU4) ) c_siU4,' +
    'sum ( convert ( float, c_siU5) ) c_siU5, sum ( convert ( float, c_siU6) ) c_siU6,' +
    'sum ( convert ( float, c_siU7) ) c_siU7, sum ( convert ( float, c_siU8) ) c_siU8,' +
    'sum ( convert ( float, c_siU9) ) c_siU9, sum ( convert ( float, c_siU10) ) c_siU10,' +
    'sum ( convert ( float, c_siU11) ) c_siU11, sum ( convert ( float, c_siU12) ) c_siU12,' +
    'sum ( convert ( float, c_siU13) ) c_siU13, sum ( convert ( float, c_siU14) ) c_siU14,' +
    'sum ( convert ( float, c_siU15) ) c_siU15, sum ( convert ( float, c_siU16) ) c_siU16,' +
    'sum ( convert ( float, c_siU17) ) c_siU17, sum ( convert ( float, c_siU18) ) c_siU18,' +
    'sum ( convert ( float, c_siU19) ) c_siU19, sum ( convert ( float, c_siU20) ) c_siU20,' +
    'sum ( convert ( float, c_siU21) ) c_siU21, sum ( convert ( float, c_siU22) ) c_siU22,' +
    'sum ( convert ( float, c_siU23) ) c_siU23, sum ( convert ( float, c_siU24) ) c_siU24,' +
    'sum ( convert ( float, c_siU25) ) c_siU25, sum ( convert ( float, c_siU26) ) c_siU26,' +
    'sum ( convert ( float, c_siU27) ) c_siU27, sum ( convert ( float, c_siU28) ) c_siU28,' +
    'sum ( convert ( float, c_siU29) ) c_siU29, sum ( convert ( float, c_siU30) ) c_siU30,' +
    'sum ( convert ( float, c_siU31) ) c_siU31, sum ( convert ( float, c_siU32) ) c_siU32,' +
    'sum ( convert ( float, c_siU33) ) c_siU33, sum ( convert ( float, c_siU34) ) c_siU34,' +
    'sum ( convert ( float, c_siU35) ) c_siU35, sum ( convert ( float, c_siU36) ) c_siU36,' +
    'sum ( convert ( float, c_siU37) ) c_siU37, sum ( convert ( float, c_siU38) ) c_siU38,' +
    'sum ( convert ( float, c_siU39) ) c_siU39, sum ( convert ( float, c_siU40) ) c_siU40,' +
    'sum ( convert ( float, c_siU41) ) c_siU41, sum ( convert ( float, c_siU42) ) c_siU42,' +
    'sum ( convert ( float, c_siU43) ) c_siU43, sum ( convert ( float, c_siU44) ) c_siU44,' +
    'sum ( convert ( float, c_siU45) ) c_siU45, sum ( convert ( float, c_siU46) ) c_siU46,' +
    'sum ( convert ( float, c_siU47) ) c_siU47, sum ( convert ( float, c_siU48) ) c_siU48,' +
    'sum ( convert ( float, c_siU49) ) c_siU49, sum ( convert ( float, c_siU50) ) c_siU50,' +
    'sum ( convert ( float, c_siU51) ) c_siU51, sum ( convert ( float, c_siU52) ) c_siU52,' +
    'sum ( convert ( float, c_siU53) ) c_siU53, sum ( convert ( float, c_siU54) ) c_siU54,' +
    'sum ( convert ( float, c_siU55) ) c_siU55, sum ( convert ( float, c_siU56) ) c_siU56,' +
    'sum ( convert ( float, c_siU57) ) c_siU57, sum ( convert ( float, c_siU58) ) c_siU58,' +
    'sum ( convert ( float, c_siU59) ) c_siU59, sum ( convert ( float, c_siU60) ) c_siU60,' +
    'sum ( convert ( float, c_siU61) ) c_siU61, sum ( convert ( float, c_siU62) ) c_siU62,' +
    'sum ( convert ( float, c_siU63) ) c_siU63, sum ( convert ( float, c_siU64) ) c_siU64 ';
//..............................................................................
  cQueryPartSIROCut =
    'sum ( convert ( float, c_siC1) ) c_siC1, sum ( convert ( float, c_siC2) ) c_siC2,' +
    'sum ( convert ( float, c_siC3) ) c_siC3, sum ( convert ( float, c_siC4) ) c_siC4,' +
    'sum ( convert ( float, c_siC5) ) c_siC5, sum ( convert ( float, c_siC6) ) c_siC6,' +
    'sum ( convert ( float, c_siC7) ) c_siC7, sum ( convert ( float, c_siC8) ) c_siC8,' +
    'sum ( convert ( float, c_siC9) ) c_siC9, sum ( convert ( float, c_siC10) ) c_siC10,' +
    'sum ( convert ( float, c_siC11) ) c_siC11, sum ( convert ( float, c_siC12) ) c_siC12,' +
    'sum ( convert ( float, c_siC13) ) c_siC13, sum ( convert ( float, c_siC14) ) c_siC14,' +
    'sum ( convert ( float, c_siC15) ) c_siC15, sum ( convert ( float, c_siC16) ) c_siC16,' +
    'sum ( convert ( float, c_siC17) ) c_siC17, sum ( convert ( float, c_siC18) ) c_siC18,' +
    'sum ( convert ( float, c_siC19) ) c_siC19, sum ( convert ( float, c_siC20) ) c_siC20,' +
    'sum ( convert ( float, c_siC21) ) c_siC21, sum ( convert ( float, c_siC22) ) c_siC22,' +
    'sum ( convert ( float, c_siC23) ) c_siC23, sum ( convert ( float, c_siC24) ) c_siC24,' +
    'sum ( convert ( float, c_siC25) ) c_siC25, sum ( convert ( float, c_siC26) ) c_siC26,' +
    'sum ( convert ( float, c_siC27) ) c_siC27, sum ( convert ( float, c_siC28) ) c_siC28,' +
    'sum ( convert ( float, c_siC29) ) c_siC29, sum ( convert ( float, c_siC30) ) c_siC30,' +
    'sum ( convert ( float, c_siC31) ) c_siC31, sum ( convert ( float, c_siC32) ) c_siC32,' +
    'sum ( convert ( float, c_siC33) ) c_siC33, sum ( convert ( float, c_siC34) ) c_siC34,' +
    'sum ( convert ( float, c_siC35) ) c_siC35, sum ( convert ( float, c_siC36) ) c_siC36,' +
    'sum ( convert ( float, c_siC37) ) c_siC37, sum ( convert ( float, c_siC38) ) c_siC38,' +
    'sum ( convert ( float, c_siC39) ) c_siC39, sum ( convert ( float, c_siC40) ) c_siC40,' +
    'sum ( convert ( float, c_siC41) ) c_siC41, sum ( convert ( float, c_siC42) ) c_siC42,' +
    'sum ( convert ( float, c_siC43) ) c_siC43, sum ( convert ( float, c_siC44) ) c_siC44,' +
    'sum ( convert ( float, c_siC45) ) c_siC45, sum ( convert ( float, c_siC46) ) c_siC46,' +
    'sum ( convert ( float, c_siC47) ) c_siC47, sum ( convert ( float, c_siC48) ) c_siC48,' +
    'sum ( convert ( float, c_siC49) ) c_siC49, sum ( convert ( float, c_siC50) ) c_siC50,' +
    'sum ( convert ( float, c_siC51) ) c_siC51, sum ( convert ( float, c_siC52) ) c_siC52,' +
    'sum ( convert ( float, c_siC53) ) c_siC53, sum ( convert ( float, c_siC54) ) c_siC54,' +
    'sum ( convert ( float, c_siC55) ) c_siC55, sum ( convert ( float, c_siC56) ) c_siC56,' +
    'sum ( convert ( float, c_siC57) ) c_siC57, sum ( convert ( float, c_siC58) ) c_siC58,' +
    'sum ( convert ( float, c_siC59) ) c_siC59, sum ( convert ( float, c_siC60) ) c_siC60,' +
    'sum ( convert ( float, c_siC61) ) c_siC61, sum ( convert ( float, c_siC62) ) c_siC62,' +
    'sum ( convert ( float, c_siC63) ) c_siC63, sum ( convert ( float, c_siC64) ) c_siC64 ';
//..............................................................................
 cParameterTable : array [TClassDataTyp] of TParRec =
  ( ( Table : 't_dw_classUncut';   IDColumn : 'c_classUncut_id';    ColumnPart : 'c_cU';  ColumnSum : cQueryPartClassUncut ),
    ( Table : 't_dw_classCut';     IDColumn : 'c_classCut_id';      ColumnPart : 'c_cC';  ColumnSum : cQueryPartClassCut   ),
    ( Table : 't_dw_spliceUncut';  IDColumn : 'c_spliceUncut_id';   ColumnPart : 'c_spU'; ColumnSum : cQueryPartSpliceUncut),
    ( Table : 't_dw_spliceCut';    IDColumn : 'c_spliceCut_id';     ColumnPart : 'c_spC'; ColumnSum : cQueryPartSpliceCut  ),
    ( Table : 't_dw_siroCutUncut'; IDColumn : 'c_siroCutUncut_id';  ColumnPart : 'c_siU'; ColumnSum : cQueryPartSIROUncut  ),
    ( Table : 't_dw_siroCutUncut'; IDColumn : 'c_siroCutUncut_id';  ColumnPart : 'c_siC'; ColumnSum : cQueryPartSIROCut    ) );

//..............................................................................
  cQueryProdGrpShift =
    'select sum ( convert ( float, c_Len ) ) c_Len, %s ' +
    'from %s t, t_dw_prodgroup_shift ps, t_prodgroup p ' +
    'where ps.%s = t.%s ' +
    'and   ps.c_prod_id = p.c_prod_id ' +
    'and   ps.c_prod_id in ( _PrimID ) ' +
    'and   ps.c_fragshift_start >= :Start ' +
    'and   ps.c_fragshift_start <  :End ';
//..............................................................................
  cQueryMachineShift =
    'select sum ( convert ( float, c_Len ) ) c_Len, %s ' +
    'from %s t, t_dw_prodgroup_shift ps, t_prodgroup p ' +
    'where ps.%s = t.%s ' +
    'and   p.c_prod_id = ps.c_prod_id ' +
    'and   p.c_machine_id in ( _PrimID ) ' +
    'and   ps.c_fragshift_start >= :Start ' +
    'and   ps.c_fragshift_start <  :End ';
//..............................................................................
  cQueryOrderPositionShift =
    'select sum ( convert ( float, c_Len ) ) c_Len, %s ' +
    'from %s t, t_dw_prodgroup_shift ps, t_prodgroup p ' +
    'where ps.%s = t.%s ' +
    'and   p.c_prod_id = ps.c_prod_id ' +
    'and   p.c_order_position_id in ( _PrimID ) ' +
    'and   ps.c_fragshift_start >= :Start ' +
    'and   ps.c_fragshift_start <  :End ';
//..............................................................................
  cQueryStyleShift =
    'select sum ( convert ( float, c_Len ) ) c_Len, %s ' +
    'from %s t, t_dw_prodgroup_shift ps, t_prodgroup p ' +
    'where ps.%s = t.%s ' +
    'and   p.c_prod_id = ps.c_prod_id ' +
    'and   p.c_style_id in ( _PrimID ) ' +
    'and   ps.c_fragshift_start >= :Start ' +
    'and   ps.c_fragshift_start <  :End ';
//..............................................................................
  cQueryAssortmentShift =
    'select sum ( convert ( float, c_Len ) ) c_Len, %s ' +
    'from %s t, t_dw_prodgroup_shift ps, t_prodgroup p ' +
    'where ps.%s = t.%s ' +
    'and   p.c_prod_id = ps.c_prod_id ' +
    'and   p.c_assortment_id in ( _PrimID ) ' +
    'and   ps.c_fragshift_start >= :Start ' +
    'and   ps.c_fragshift_start <  :End ';
//..............................................................................
  cQueryTableShift : array [dlProdGrp .. dlAssortment] of PChar =
    ( cQueryProdGrpShift, cQueryMachineShift, cQueryOrderPositionShift,
      cQueryStyleShift, cQueryAssortmentShift );
//..............................................................................

//**************Queries for TClassTrendDataSuck*********************************
//..............................................................................
  cQuerySpindleIntervalTrend =
    'select i.c_interval_start,  c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.c_prod_id ' +
    'and   sp.c_spindle_id in ( _PrimID ) ' +
    'and   sp.c_machine_id = :SecID ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryProdGrpIntervalTrend =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.c_prod_id ' +
    'and   sp.c_prod_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryMachineIntervalTrend =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.c_prod_id ' +
    'and   sp.c_machine_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryOrderPositionIntervalTrend =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.prod_id ' +
    'and   p.c_order_position_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryStyleIntervalTrend =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.prod_id ' +
    'and   p.c_style_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
  cQueryAssortmentIntervalTrend =
    'select i.c_interval_start, c_Len, c_class_image ' +
    'from t_spindle_interval_data sp, t_interval i, t_prodgroup p ' +
    'where sp.c_interval_id = i.c_interval_id ' +
    'and   sp.c_prod_id = p.prod_id ' +
    'and   p.c_assortment_id in ( _PrimID ) ' +
    'and   i.c_interval_start >= :Start ' +
    'and   i.c_interval_start <  :End ' +
    'order by i.c_interval_start';
//..............................................................................
   cQueryTableIntervalTrend : array [dlSpindle .. dlAssortment] of PChar =
    ( cQuerySpindleIntervalTrend, cQueryProdGrpIntervalTrend, cQueryMachineIntervalTrend,
      cQueryOrderPositionIntervalTrend, cQueryStyleIntervalTrend, cQueryAssortmentIntervalTrend );
//..............................................................................
//..............................................................................
  cQueryProdGrpTrend =
    'select c_shift_start, sum ( convert ( float, c_Len ) ) c_Len , sum ( convert ( float , %s ) ) FieldSum ' +
    'from  %s t, t_dw_prodgroup_shift ps, t_shift s, t_shift_fragshift sf, t_prodgroup p ' +
    'where ps.c_prod_id = :id ' +
    'and   sp.c_prod_id = p.c_prod_id ' +
    'and   s.c_shiftcal_id = :c_shift_cal_id ' +
    'and   s.c_shift_start >= :Start ' +
    'and   s.c_shift_start < :End ' +
    'and   ps.%s = t.%s ' +
    'and   ps.c_fragshift_id = sf.c_fragshift_id ' +
    'and   s.c_shift_id = sf.c_shift_id ' +
    'group by c_shift_start';
//..............................................................................
  cQueryMachineTrend =
    'select c_shift_start, sum ( convert ( float, c_Len ) ) c_Len , sum ( convert ( float , %s ) ) FieldSum ' +
    'from %s t, t_dw_prodgroup_shift ps, t_shift s, t_shift_fragshift sf, t_prodgroup p ' +
    'where p.c_machine_id in ( _PrimID ) ' +
    'and   s.c_shiftcal_id = :c_shift_cal_id ' +
    'and   s.c_shift_start >= :Start ' +
    'and   s.c_shift_start < :End ' +
    'and   ps.c_prod_id = p.c_prod_id ' +
    'and   ps.%s = t.%s ' +
    'and   ps.c_fragshift_id = sf.c_fragshift_id ' +
    'and   s.c_shift_id = sf.c_shift_id ' +
    'group by c_shift_start';
//..............................................................................
  cQueryOrderPositionTrend =
    'select c_shift_start, sum ( convert ( float, c_Len ) ) c_Len , sum ( convert ( float , %s ) ) FieldSum ' +
    'from %s t, t_dw_prodgroup_shift ps, t_shift s, t_shift_fragshift sf, t_prodgroup p ' +
    'where p.c_order_position_id in ( _PrimID ) ' +
    'and   s.c_shiftcal_id = :c_shift_cal_id ' +
    'and   s.c_shift_start >= :Start ' +
    'and   s.c_shift_start < :End ' +
    'and   ps.c_prod_id = p.c_prod_id ' +
    'and   ps.%s = t.%s ' +
    'and   ps.c_fragshift_id = sf.c_fragshift_id ' +
    'and   s.c_shift_id = sf.c_shift_id ' +
    'group by c_shift_start';
//..............................................................................
  cQueryStyleTrend =
    'select c_shift_start, sum ( convert ( float, c_Len ) ) c_Len , sum ( convert ( float , %s ) ) FieldSum ' +
    'from %s t, t_dw_prodgroup_shift ps, t_shift s, t_shift_fragshift sf, t_prodgroup p ' +
    'where p.c_style_id = in ( _PrimID ) ' +
    'and   s.c_shiftcal_id = :c_shift_cal_id ' +
    'and   s.c_shift_start >= :Start ' +
    'and   s.c_shift_start < :End ' +
    'and   ps.c_prod_id = p.c_prod_id ' +
    'and   ps.%s = t.%s ' +
    'and   ps.c_fragshift_id = sf.c_fragshift_id ' +
    'and   s.c_shift_id = sf.c_shift_id ' +
    'group by c_shift_start';
//..............................................................................
  cQueryAssortmentTrend =
    'select c_shift_start, sum ( convert ( float, c_Len ) ) c_Len , sum ( convert ( float , %s ) ) FieldSum ' +
    'from %s t, t_dw_prodgroup_shift ps, t_shift s, t_shift_fragshift sf, t_prodgroup p ' +
    'where p.c_assortment_id in ( _PrimID ) ' +
    'and   s.c_shiftcal_id = :c_shift_cal_id ' +
    'and   s.c_shift_start >= :Start ' +
    'and   s.c_shift_start < :End ' +
    'and   ps.c_prod_id = p.c_prod_id ' +
    'and   ps.%s = t.%s ' +
    'and   ps.c_fragshift_id = sf.c_fragshift_id ' +
    'and   s.c_shift_id = sf.c_shift_id ' +
    'group by c_shift_start';
//..............................................................................
  cQueriesTableShiftTrend : array [TDataLevel] of PChar =
    ( 'Spindle is not available',
      cQueryProdGrpTrend,
      cQueryMachineTrend,
      cQueryOrderPositionTrend,
      cQueryStyleTrend,
      cQueryAssortmentTrend );
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

end.
