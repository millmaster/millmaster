(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: ClassDataStuck.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enthaelt die Klasse fuer den DB-Zugriff der Klassiermatrix
|
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 17.05.1999  1.00  Mg  | Datei erstellt
| 22.05.1999  1.01  PW  | property "len" added (=> KlassierMatrix)
| 02.06.1999  1.02  Mg  | Spitting in to 3 Classes and implementation of Shift Trend
| 08.12.1999  1.03  Mg  | Constructors with TmmQuery inserted
| 28.06.2000  1.04  Mg  | New implementation for design with SettingsFinder
| 29.08.2000  1.05  Mg  | PrimIDStr inserted
| 17.09.2002  1.06  Nue | Umbau auf ADO
| 03.10.2002         LOK| Umbau ADO2 (dbTables entfernt und TBlobStream erstetzt)
|=========================================================================================*)
unit ClassDataSuck;
interface
uses  SysUtils, mmADODataSet, Windows, Classes, DB,
      LoepfeGlobal, MMUGlobal, BaseGlobal, ClassDataSuckQueries, mmADOConnection;
type
//..............................................................................
  TClassDataArr = array of Single;
//..............................................................................
  TTrendPosRec = record
    DateTime : TDateTime;
    Value    : Single;
  end;
//..............................................................................
  TTrendArr = array of TTrendPosRec;
//..............................................................................
  TClassRec = record
    Data : array [TClassDataTyp] of TClassDataArr;
    Len         : Single;
  end;
type
//..............................................................................
  TBaseClassDataSuck = class ( TComponent )
  protected
    mDataSet     : TmmADODataSet;         // Query for DB Access
    fShiftData   : boolean;          // True = get shift data; false = get interval data
    fStartTime   : TDateTime;        // Start time of time range
    fEndTime     : TDateTime;        // End time of time range
    fDataLevel   : TDataLevel;       // Used data level like : Spindle, Machine, Style ec...
    fPrimID      : integer;          // Primary ID of the data object. The object level is given in DataLevel
    fPrimIDStr   : String;           // Primary ID's of the data objects. The object level is given in DataLevel
    fSecID       : integer;          // Secundary ID of the data object. Only used if data level is Spindle so the machine ID is also used
    fLenBase     : TLenBase;         // Base of the length like : 100 km, 1000 km ec..
    fDataBase    : TmmADOConnection;
//Alt    fDataBaseName: String;
    function  CalcBase ( aValue : Single; aLen : Single ): Single;
    procedure SetStartTime ( aStartTime : TDateTime ); virtual;
    procedure SetEndTime ( aEndTime : TDateTime ); virtual;
    procedure SetDataLevel ( aDataLevel : TDataLevel ); virtual;
    procedure SetPrimID ( aID : integer ); virtual;
    procedure SetPrimIDStr ( aPrimIDs : String );virtual;
    procedure SetSecID ( aID : integer ); virtual;
    procedure SetLenBase ( aLenBase : TLenBase ); virtual;
    procedure SetShiftData ( aShiftData : boolean ); virtual;
//Alt    procedure SetDataBaseName ( aName : String );
    function  IsInitialized : boolean; virtual;// Check if the Object is initialized
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    // all properties and procedures can raise an exception except Create and Destroy
    property StartTime : TDateTime read fStartTime write SetStartTime;   // Start time of data range
    property EndTime   : TDateTime read fEndTime write SetEndTime;       // End time of data range
    property ShiftData : boolean read fShiftData write SetShiftData;     // ShiftData or Interval data; default is false
    property DataLevel : TDataLevel read fDataLevel write SetDataLevel;  // Interested data level like Spindle, ProdGrp ec..
    property LenBase   : TLenBase read fLenBase write SetLenBase;        // Base of length like 100km, 1000km ec...
    property PrimID    : integer read fPrimID write SetPrimID;           // Primary ID if the data object. If PrimID and PrimIDStr is initialized so PrimIDStr will be taken
    property PrimIDStr : String read fPrimIDStr write SetPrimIDStr;      // Primary ID's as String i.e. : 34,2,56,87,98
    property SecID     : integer read fSecID write SetSecID;             // Secundary ID of the data object (only used if data level is Spindle, so PrimID = Spindle ID and SecID = Machine ID )
    procedure Loaded;override;
    constructor Create ( aOwner : TComponent ); override;
    destructor  Destroy; override;
  published
//Alt    property DataBaseName  : String read fDataBaseName write SetDataBaseName;
  end;

  { Using of PrimID and SecID

    DataLevel        | PrimID(Str)        | SecID
    -----------------|--------------------|-----------
    dlSpindle        | SpindleID(s)       | MachineID
    dlProdGrp        | ProdGrpID(s)       | ---------
    dlMachine        | MachineID(s)       | ---------
    dlOrderPosition  | OrderPosID(s)      | ---------
    dlStyle          | StyleID(s)         | ---------
    dlAssortment     | AssortmentID(s)    | ---------
    dlShiftGroup     | ShiftGroupID(s)    | ShiftCalendarID // noch zu implementieren
  }
//..............................................................................
  TClassDataSuck = class ( TBaseClassDataSuck )
  protected
    mClassRec    : TClassRec;        // Buffer for the read data
    mAvData      : TClassDataSet;        // Map which data are available in Buffer
    function  NeedsDataRefresh ( aDataType : TClassDataTyp ) : boolean;  // Check if the Object needs a data refresh
    procedure DataRefresh ( aDataTyp : TClassDataTyp ); // Refresh the Buffer of aDataTyp
    procedure LoadDataFromShift ( aClassData : TClassDataTyp );
    procedure LoadDataFromInterval;
    procedure SetStartTime ( aStartTime : TDateTime ); override;
    procedure SetEndTime ( aEndTime : TDateTime ); override;
    procedure SetDataLevel ( aDataLevel : TDataLevel ); override;
    procedure SetPrimID ( aID : integer ); override;
    procedure SetPrimIDStr ( aPrimIDs : String ); override;
    procedure SetSecID ( aID : integer ); override;
    procedure SetLenBase ( aLenBase : TLenBase ); override;
    procedure SetShiftData ( aShiftData : boolean ); override;
    procedure FillZero ( aClassSet : TClassDataSet );
    procedure Init;
    procedure GetData (aDataTyp : TClassDataTyp );
    function  GetClassCut ( Index : integer ):Single;
    function  GetClassUnCut ( Index : integer ):Single;
    function  GetSiroCut ( Index : integer ):Single;
    function  GetSiroUnCut ( Index : integer ):Single;
    function  GetSpliceCut ( Index : integer ):Single;
    function  GetSpliceUnCut ( Index : integer ):Single;
  public
    // all properties and procedures can raise an exception except Create and Destroy
    property    Len : single read mClassRec.Len;
    constructor Create ( aOwner : TComponent ); override;
    property    ClassCut[Index: Integer]: Single read GetClassCut;
    property    ClassUnCut[Index: Integer]: Single read GetClassUnCut;
    property    SiroCut[Index: Integer]: Single read GetSiroCut;
    property    SiroUnCut[Index: Integer]: Single read GetSiroUnCut;
    property    SpliceCut[Index: Integer]: Single read GetSpliceCut;
    property    SpliceUnCut[Index: Integer]: Single read GetSpliceUnCut;
  end;
//..............................................................................
  TClassTrendDataSuck = class ( TBaseClassDataSuck )
  protected
    fShiftCalID : integer;
    procedure SetShiftCalID ( aShiftCalID : integer );
    function  IsInitialized : boolean; override;
    procedure GetShiftTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
    procedure GetIntervalTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
  public
    // all properties and procedures can raise an exception except Create and Destroy
    property  ShiftCalID : integer read fShiftCalID write SetShiftCalID;
    constructor Create ( aOwner : TComponent );override;
    procedure GetTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS;

//------------------------------------------------------------------------------
 { TBaseClassDataSuck }
//------------------------------------------------------------------------------
  constructor TBaseClassDataSuck.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    fDatabase := Nil;
    mDataSet := TmmADODataSet.Create ( Nil );
    mDataSet.ConnectionString := GetDefaultConnectionString;
    fPrimIDStr := '';
  end;
//------------------------------------------------------------------------------
  destructor TBaseClassDataSuck.Destroy;
  begin
    mDataSet.Free;
    inherited destroy;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.Loaded;
  begin
    inherited Loaded;
//Alt    xDB := Session.FindDataBase ( fDataBaseName );
//Alt    if Assigned(xDB) then
//Alt      mQuery.DatabaseName := xDB.DatabaseName;
{
    if fDataBaseName <> '' then
      mQuery.DatabaseName := Session.FindDataBase ( fDataBaseName ).DatabaseName;
{}
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
{ DONE : Wie ??? }//    if ( Operation = opRemove ) then begin
//      if AComponent is TDatabase then
//        if CompareText((AComponent as TDatabase).DatabaseName, DatabaseName) = 0 then
//          DataBaseName := '';
//    end;
  end;
//------------------------------------------------------------------------------
  function TBaseClassDataSuck.IsInitialized : boolean;
  begin
    Result := true;
    if ( PrimID = 0 ) and ( PrimIDStr = '' ) then Result := false;
    if ( DataLevel = dlSpindle ) and ( SecID = 0 ) then Result := false;
    if StartTime = 0 then Result := false;
    if EndTime = 0 then Result := false;
  end;
//------------------------------------------------------------------------------
  function TBaseClassDataSuck.CalcBase ( aValue : Single; aLen : Single ) : Single;
  var xLenBase : Single;
  begin
    xLenBase := aLen;
    case LenBase of
       lbAbsolute : xLenBase := aLen;
       lb100KM    : xLenBase := 100000;
       lb1000KM   : xLenBase := 1000000;
       lb100000Y  : xLenBase := cYdToM * 100000.0;
       lb1000000Y : xLenBase := cYdToM * 1000000.0;
    end;
    if aLen > cMinSpuledLen then Result := ( aValue * xLenBase ) / aLen
    else Result := 0;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetStartTime ( aStartTime : TDateTime );
  begin
    fStartTime := aStartTime;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetEndTime ( aEndTime : TDateTime );
  begin
    fEndTime := aEndTime;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetDataLevel ( aDataLevel : TDataLevel );
  begin
    case aDataLevel of
      dlSpindle :
          if fShiftData then raise EMMException.Create ( 'TClassDataSuck : DataLevel Spindle not allowed by ShiftData.' )
          else fDataLevel := aDataLevel;
      dlProdGrp,
      dlMachine,
      dlOrderPosition,
      dlStyle,
      dlAssortment: fDataLevel := aDataLevel;
    else
      raise EMMException.Create ( 'Unknown DataLevel in TClassDataSuck.' );
    end;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetPrimID ( aID : integer );
  begin
    fPrimID := aID;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetPrimIDStr ( aPrimIDs : String );
  begin
    fPrimIDStr := aPrimIDs;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetSecID ( aID : integer );
  begin
    fSecID := aID;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetLenBase ( aLenBase : TLenBase );
  begin
    case aLenBase of
      lbAbsolute, lb100KM, lb1000KM, lb100000Y, lb1000000Y : ;
    else
      raise EMMException.Create ( 'TClassDataSuck : Unknown LenBase.' );
    end;
    fLenBase := aLenBase;
  end;
//------------------------------------------------------------------------------
  procedure TBaseClassDataSuck.SetShiftData ( aShiftData : boolean );
  begin
    fShiftData := aShiftData;
  end;
//------------------------------------------------------------------------------
//  procedure TBaseClassDataSuck.SetDataBaseName ( aName : String );
////  var
////    xDB: TDatabase;
//  begin
//    if aName <> fDatabaseName then begin
//      fDatabaseName := aName;
//{
//      if (fDatabaseName <> '') and not(csDesigning in ComponentState) then begin
//        xDB := Session.FindDataBase ( fDataBaseName );
//        if Assigned(xDB) then
//          mQuery.DatabaseName := xDB.DatabaseName;
//      end else
//        mQuery.DatabaseName := '';
//{}
//    end;
//  end;
//------------------------------------------------------------------------------
 { TClassDataSuck }
//------------------------------------------------------------------------------
  constructor TClassDataSuck.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    init;
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.GetData ( aDataTyp : TClassDataTyp );
//  var   x : integer;
  begin
    try
      if not IsInitialized then raise EMMException.Create ( 'Object is not initialized yet.' );
      if NeedsDataRefresh ( aDataTyp ) then begin
        DataRefresh ( aDataTyp );
      end;
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetData failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetClassCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdClassCut );
      Result := CalcBase ( mClassRec.Data[cdClassCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetClassCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetClassUnCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdClassUnCut );
      Result := CalcBase ( mClassRec.Data[cdClassUnCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetClassUnCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetSiroCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdSiroCut );
      Result := CalcBase ( mClassRec.Data[cdSiroCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetSiroCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetSiroUnCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdSiroUnCut );
      Result := CalcBase ( mClassRec.Data[cdSiroUnCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetSiroUnCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetSpliceCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdSpliceCut );
      Result := CalcBase ( mClassRec.Data[cdSpliceCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetSpliceCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  function  TClassDataSuck.GetSpliceUnCut ( Index : integer ):Single;
  begin
    try
      GetData ( cdSpliceUnCut );
      Result := CalcBase ( mClassRec.Data[cdSpliceUnCut][Index], mClassRec.Len );
    except
      on e:Exception do begin
        raise Exception.Create ( 'TClassDataSuck.GetSpliceUnCut failed. Error : ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.Init;
  var  x : TClassDataTyp;
  begin
    fShiftData  := false;
    fStartTime  := 0;
    fEndTime    := 0;
    fDataLevel  := dlSpindle;
    fLenBase    := lb1000KM;
    fPrimID     := 0;
    fSecID      := 0;
    mAvData     := [];
    for x := cdClassUncut to cdSIROCut do
      SetLength ( mClassRec.Data [x], cNumOfClassFields [x] );
    FillZero ( [cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut] );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.DataRefresh ( aDataTyp : TClassDataTyp );
  begin
    if ShiftData then begin
      LoadDataFromShift ( aDataTyp );
      Include ( mAvData, aDataTyp );
    end else begin
      LoadDataFromInterval;
      mAvData := [ cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut ];
    end;
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.FillZero ( aClassSet : TClassDataSet );
  var  xClassTyp : TClassDataTyp;
       x         : integer;
  begin
    for xClassTyp := cdClassUncut to cdSIROCut do
      if ( xClassTyp in aClassSet ) then
        for x := 0 to cNumOfClassFields[xClassTyp] - 1 do
          mClassRec.Data[xClassTyp][x] := 0.0;
    mClassRec.Len := 0.0;
  end;
//------------------------------------------------------------------------------
   procedure TClassDataSuck.LoadDataFromShift ( aClassData : TClassDataTyp );
   var   x : integer;
         xTmpStr : String;
   begin
     with mDataSet, cParameterTable[aClassData] do begin
       try
         Close;
         CommandText := Format ( cQueryTableShift[DataLevel], [ColumnSum, Table, IDColumn, IDColumn ] );
         xTmpStr := CommandText;

         if PrimIDStr <> '' then
           CommandText := StringReplace ( CommandText, '_PrimID', PrimIDStr,[rfReplaceAll] )
         else
           CommandText := StringReplace ( CommandText, '_PrimID', IntToStr ( PrimID ), [rfReplaceAll] );
         Parameters.ParamByName ( 'Start' ).DataType := ftDateTime;    //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
         Parameters.ParamByName ( 'Start' ).Value := StartTime;
         Parameters.ParamByName ( 'End' ).DataType := ftDateTime;      //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
         Parameters.ParamByName ( 'End' ).Value := EndTime;
         Open;
         FillZero ( [aClassData] );
         if FindFirst then begin
           mClassRec.Len := FieldByName ( 'c_Len' ).AsFloat;
           for x := 0 to cNumOfClassFields[aClassData] - 1 do
             mClassRec.Data[aClassData][x] := FieldByName ( cParameterTable[aClassData].ColumnPart + IntToStr ( x + 1 ) ).AsFloat;
         end;
       except
         on e:Exception do begin
           raise EMMException.Create ( 'TClassDataSuck.LoadClassUncutData failed. Error : ' + e.Message );
         end;
       end;
     end;
   end;
//------------------------------------------------------------------------------
   procedure TClassDataSuck.LoadDataFromInterval;
   var  xStream        : TMemoryStream;
        xClassFieldRec : TClassFieldRec;
        x              : integer;
        xData          : Variant;
        xBuffer        : PByte;
   begin
     with mDataSet do begin
       try
         Close;
         CommandText := cQueryTableInterval[DataLevel];
         case DataLevel of
           dlSpindle : begin
             Parameters.ParamByName ( 'SecID' ).Value := SecID;
           end;
         end;
         if PrimIDStr <> '' then
           CommandText := StringReplace ( CommandText, '_PrimID', PrimIDStr, [rfReplaceAll] )
         else
           CommandText := StringReplace ( CommandText, '_PrimID', INtToStr ( PrimID ), [rfReplaceAll] );
         Parameters.ParamByName ( 'Start' ).DataType := ftDateTime;    //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
         Parameters.ParamByName ( 'Start' ).Value := StartTime;
         Parameters.ParamByName ( 'End' ).DataType := ftDateTime;      //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
         Parameters.ParamByName ( 'End' ).Value := EndTime;

         Open;
         FillZero ( [cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut] );
         while not EOF do begin
          // Umbau ADO (LOK)
(*           if not GetFieldData(FieldByName('c_class_image'), @xClassFieldRec) then
            CodeSite.SendError('TClassDataSuck.LoadDataFromInterval: Replacement of TBlobStream doesn''t work');
          xStream := TBlobStream.Create( FieldByName ('c_class_image') as TBlobField, bmRead);
           xStream.Read( xClassFieldRec , sizeof ( TClassFieldRec ) );
           xStream.Free;*)

          xStream := TMemoryStream.Create;
          try
            // Um ein Variant Array zu bekommen, muss �ber das native ADO Objekt abgefragt werden
            xData := mDataSet.Recordset.Fields['c_class_image'].Value;
//            xData := mDataSet.Recordset.Fields[mDataSet.FieldByName('c_class_image').FieldNo].Value;
//            xData := mDataSet.FieldByName('c_class_image').Value;
            if xData <> NULL then begin
              xBuffer := VarArrayLock(xData);
              try
                xStream.Write(xBuffer^, mDataSet.Recordset.Fields['c_class_image'].ActualSize);
              finally
                VarArrayUnLock(xData);
              end;
            end;// if xData <> NULL then begin
            xStream.Position := 0;
            xStream.Read( xClassFieldRec , sizeof ( TClassFieldRec ) );
          finally
            xStream.Free;
          end;// try finally

           mClassRec.Len := mClassRec.Len + FieldByName ( 'c_Len' ).AsFloat;
           for x := 1 to cSpCutFields do
             mClassRec.Data[cdSpliceCut][x - 1] := mClassRec.Data[cdSpliceCut][x - 1] + xClassFieldRec.SpCutField[x];
           for x := 1 to cSpUncutFields do
             mClassRec.Data[cdSpliceUnCut][x - 1] := mClassRec.Data[cdSpliceUnCut][x - 1] + xClassFieldRec.SpUncutField[x];
           for x := 1 to cClassCutFields do
             mClassRec.Data[cdClassCut][x - 1] := mClassRec.Data[cdClassCut][x - 1] + xClassFieldRec.ClassCutField[x];
           for x := 1 to cClassUncutFields do
             mClassRec.Data[cdClassUnCut][x - 1] := mClassRec.Data[cdClassUnCut][x - 1] + xClassFieldRec.ClassUncutField[x];
           for x := 1 to cSIROCutFields do
             mClassRec.Data[cdSIROCut][x - 1] := mClassRec.Data[cdSIROCut][x - 1] + xClassFieldRec.SIROCutField[x];
           for x := 1 to cSIROUncutFields do
             mClassRec.Data[cdSIROUnCut][x - 1] := mClassRec.Data[cdSIROUnCut][x - 1] + xClassFieldRec.SIROUncutField[x];
           Next;
         end;
       except
         on e:Exception do begin
           raise EMMException.Create ( e.Message );
         end;
       end;
     end;
   end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetStartTime ( aStartTime : TDateTime );
  begin
    if aStartTime <> fStartTime then mAvData := [];
    inherited SetStartTime ( aStartTime );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetEndTime ( aEndTime : TDateTime );
  begin
    if aEndTime <> fEndTime then mAvData := [];
    inherited SetEndTime ( aEndTime );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetDataLevel ( aDataLevel : TDataLevel );
  begin
    if fDataLevel <> aDataLevel then mAvData := [];
    inherited SetDataLevel ( aDataLevel );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetPrimID ( aID : integer );
  begin
    if fPrimID <> aID then mAvData := [];
    inherited SetPrimID ( aID );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetPrimIDStr ( aPrimIDs : String );
  begin
    if fPrimIDStr <> aPrimIDs then mAvData := [];
    inherited SetPrimIDStr ( aPrimIDs );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetSecID ( aID : integer );
  begin
    if fSecID <> aID then mAvData := [];
    inherited SetSecID ( aID );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetLenBase ( aLenBase : TLenBase );
  begin
    if fLenBase <> aLenBase then mAvData := [];
    inherited SetLenBase ( aLenBase );
  end;
//------------------------------------------------------------------------------
  procedure TClassDataSuck.SetShiftData ( aShiftData : boolean );
  begin
    if fShiftData <> aShiftData then mAvData := [];
    inherited SetShiftData ( aShiftData );
  end;
//------------------------------------------------------------------------------
  function TClassDataSuck.NeedsDataRefresh ( aDataType : TClassDataTyp ) : boolean;
  begin
    Result := false;
    if not ( aDataType in mAvData ) then Result := true;
  end;
//------------------------------------------------------------------------------
{ TClassTrendDataSuck }
//------------------------------------------------------------------------------
  constructor TClassTrendDataSuck.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    fShiftCalID := 0;
  end;
//------------------------------------------------------------------------------
  procedure  TClassTrendDataSuck.GetTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
  begin
     if not IsInitialized then raise EMMException.Create ( 'TClassDataSuck : Object is not initialized yet.' );
     if ShiftData then begin
       GetShiftTrend ( aDataArr, aDataTyp, aFields );
     end else begin
       GetIntervalTrend ( aDataArr, aDataTyp, aFields );
     end;
  end;
//------------------------------------------------------------------------------
  procedure TClassTrendDataSuck.SetShiftCalID ( aShiftCalID : integer );
  begin
    fShiftCalID := aShiftCalID;
  end;
//------------------------------------------------------------------------------
  function  TClassTrendDataSuck.IsInitialized : boolean;
  begin
    Result := inherited IsInitialized;
    if fShiftCalID = 0 then
      Result := false;
  end;
//------------------------------------------------------------------------------
  procedure TClassTrendDataSuck.GetShiftTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
  var   xParString : String;
        x          : integer;
        xPos       : integer;
  begin
    with mDataSet, cParameterTable [aDataTyp] do begin
      try
        xParString := '';
        for x := 0 to Length ( aFields ) - 1 do begin
          xParString := xParString +  ColumnPart + IntToStr ( aFields[x] );
          if x < Length ( aFields ) - 1 then xParString := xParString + ' + ';
        end;
        CommandText := Format ( cQueriesTableShiftTrend [ DataLevel ], [ xParString, Table, IDColumn, IDColumn ] );

        if PrimIDStr <> '' then
          Parameters.ParamByName ( 'PrimID' ).Value := PrimIDStr
        else
          Parameters.ParamByName ( 'PrimID' ).Value := PrimID;
        Parameters.ParamByName ( 'c_shift_cal_id' ).Value := ShiftCalID;
        Parameters.ParamByName ( 'Start' ).DataType := ftDateTime;    //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        Parameters.ParamByName ( 'Start' ).Value := StartTime;
        Parameters.ParamByName ( 'End' ).DataType := ftDateTime;      //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        Parameters.ParamByName ( 'End' ).Value := EndTime;
        Open;
        SetLength ( aDataArr, 0 );
        xPos := 0;
        while ( not EOF ) do begin
          SetLength ( aDataArr, Length ( aDataArr ) + 1 );
          aDataArr[xPos].DateTime := FieldByName ( 'c_shift_start' ).AsDateTime;
          aDataArr[xPos].Value :=  CalcBase ( FieldByName ( 'FieldSum' ).AsFloat, FieldByName ( 'c_Len' ).AsFloat );
          Inc ( xPos );
          Next;
        end;
      except
         on e:Exception do begin
           raise EMMException.Create ( 'TClassDataSuck.GetShiftTrend failed : ' + e.Message );
        end;
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TClassTrendDataSuck.GetIntervalTrend ( var aDataArr : TTrendArr; aDataTyp : TClassDataTyp; aFields : array of byte );
  var   xActDateTime   : TDateTime;
        xLen           : Single;
        xPos           : integer;
        xStream        : TMemoryStream;
        xClassFieldRec : TClassFieldRec;
        xData          : Variant;
        xBuffer        : PByte;
     procedure AddFields ( var aValue : Single );
     var  x : integer;
     begin
       for x := 0 to Length ( aFields )  - 1 do
         case aDataTyp of
           cdClassUncut : aValue := aValue + xClassFieldRec.ClassUncutField[aFields[x]];
           cdClassCut   : aValue := aValue + xClassFieldRec.ClassCutField[aFields[x]];
           cdSpliceUncut: aValue := aValue + xClassFieldRec.SpUncutField[aFields[x]];
           cdSpliceCut  : aValue := aValue + xClassFieldRec.SpCutField[aFields[x]];
           cdSIROUncut  : aValue := aValue + xClassFieldRec.SIROUncutField[aFields[x]];
           cdSIROCut    : aValue := aValue + xClassFieldRec.SIROCutField[aFields[x]];
         end;
     end;
  begin
    with mDataSet do begin
      try
        SetLength ( aDataArr, 0 );
        CommandText := cQueryTableIntervalTrend[DataLevel];
        case DataLevel of
          dlSpindle : begin
            Parameters.ParamByName ( 'SecID' ).Value := SecID;
          end;
        end;

        if PrimIDStr <> '' then
          Parameters.ParamByName ( 'PrimID' ).Value := PrimIDStr
        else
          Parameters.ParamByName ( 'PrimID' ).Value := PrimID;
        Parameters.ParamByName ( 'Start' ).DataType := ftDateTime;    //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        Parameters.ParamByName ( 'Start' ).Value := StartTime;
        Parameters.ParamByName ( 'End' ).DataType := ftDateTime;      //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
        Parameters.ParamByName ( 'End' ).Value := EndTime;
        Open;
        xPos := 0;
        xLen := 0;
        xActDateTime := 0;
        if FindFirst then begin
          SetLength ( aDataArr, 1 );
          aDataArr[0].Value := 0;
          aDataArr[xPos].DateTime := FieldByName ( 'c_Interval_start' ).AsDateTime;
          xActDateTime := aDataArr[xPos].DateTime;
        end;
        while not EOF do begin
          // Umbau ADO (LOK)
(*          if not GetFieldData(FieldByName('c_class_image'), @xClassFieldRec) then
            CodeSite.SendError('TClassTrendDataSuck.GetIntervalTrend: Replacement of TBlobStream doesn''t work');
           xStream := TBlobStream.Create( FieldByName ('c_class_image') as TBlobField, bmRead);
           xStream.Read( xClassFieldRec , sizeof ( TClassFieldRec ) );
           xStream.Free;*)

          xStream := TMemoryStream.Create;
          try
            if xData <> NULL then begin
              xBuffer := VarArrayLock(xData);
              try
                xStream.Write(xBuffer^, mDataSet.Recordset.Fields['c_class_image'].ActualSize);
              finally
                VarArrayUnLock(xData);
              end;
            end;// if xData <> NULL then begin
            xStream.Position := 0;
            xStream.Read( xClassFieldRec , sizeof ( TClassFieldRec ) );
          finally
            xStream.Free;
          end;// try finally

          if xActDateTime = FieldByName ( 'c_Interval_start' ).AsDateTime then begin
             AddFields ( aDataArr[xPos].Value );
             xLen := xLen + FieldByName ( 'c_Len' ).AsInteger;
          end else begin
            aDataArr[xPos].Value := CalcBase ( aDataArr[xPos].Value, xLen );
            xLen := 0;
            SetLength ( aDataArr, Length ( aDataArr ) + 1 );
            Inc ( xPos );
            aDataArr[xPos].Value := 0;
            aDataArr[xPos].DateTime := FieldByName ( 'c_Interval_start' ).AsDateTime;
            xActDateTime := aDataArr[xPos].DateTime;
            xLen := xLen + FieldByName ( 'c_Len' ).AsInteger;
            AddFields ( aDataArr[xPos].Value );
          end;
          Next;
        end;
      except
        on e:Exception do begin
          raise EMMException.Create ( 'TClassDataSuck.GetIntervalTrend failed : ' + e.Message );
        end;
      end;
    end;
  end;
//------------------------------------------------------------------------------
end.





