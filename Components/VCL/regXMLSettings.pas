unit regXMLSettings;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  YMQRUnit,
  VCLXMLSettingsModel;

procedure MMRegister;
begin
  // Unit VCLXMLSettingsModel
  RegisterComponents('XMLSettings', [TVCLXMLSettingsModel, TQRXMLSettings]);
end;

end.
