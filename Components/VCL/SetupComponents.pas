{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SetupComponents.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Definition und Implementation der Setup-Komoponenten
|                 TSetupEdit
|                 TSetupSpinEdit
|                 TSetupComboBox
|                 TSetupLabel
|                 TSetupCheckBox
|
|                 TSetupPropertyEditor -> fuer alle Komponenten
|
|
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 03.08.1999  1.00  SDo | File created
| 07.09.1999  1.01  SDo | Property-Editor ins SetupPropEdit.pas gebaut und Div.
|                       | Proceduren gekuerzt und geloescht
|                       | DB-Access noch nicht implementiert
| 22.02.2001  1.02  SDo | Neue Methode Printable eingebaut -> druckbarer Wert
|                       | im Report.
| 23.02.2001  1.03  SDo | GetTabSheet() Ermittelt das Parent-TabSheet einer Komp.
| 02.05.2001  1.04  SDo | TSetupSpinEdit direkt editierbar
| 16.07.2002  1.04  LOK | TSetupComboBox.GetValue Typecast auf String fuer Uebersetzung
===============================================================================}
unit SetupComponents;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, BaseSetup, Mask,
  mmMaskEdit, mmSpinEdit, mmComboBox, mmCheckBox, mmGroupBox,
  IvDictio, IvAMulti, IvBinDic, mmDictionary, IvMulti, IvEMulti,
  mmTranslator, LoepfeGlobal, SetupPropEdit, mmRadioGroup;

type

  //TSetupValues = procedure of object;

  TSaveEvent = procedure(Sender: TObject) of object;
  TGetValueEvent = procedure(Sender: TObject) of object;
  TGetDefaultEvent = procedure(Sender: TObject) of object;
  TranslateEvent = procedure(Sender: TObject) of object;

  TSetupSpinEdit = class(TmmSpinEdit)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    mUserValue,
      mDefaultValue {, mNativeValue}: Integer;
    FRestart: Boolean;
    mRegPath, mRegValueName, mHintText: string;
    mRootKey: HKEY;
    mRegValueNameExists, mUserValueChange: Boolean;
    mCursorPos: Integer;
    mMinValue, mMaxValue: Variant;
    mValueType: TDataType;
    mAbsValueNameID: Integer;
    mLocation: TLocation;
    mFactor: Variant;
    mPCType: TClientServerType;
    mNTProd: Word;
    mValueText: string;
    procedure DeleteErrorKey;
    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);

  protected
    { Protected declarations }
    procedure Change; override;
    procedure Click; override;
    procedure DoExit; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DownClick(Sender: TObject); override;
    procedure UpClick(Sender: TObject); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property ValueChange: Boolean read mUserValueChange;
    property NeedRestart: Boolean read FRestart;
  published
    { Published declarations }
    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
    procedure GetDefault;                            // Aus Konst. Array
    procedure GetValue;                              // Aus Reg. oder DB
    procedure GetArrayValue;                         // UserValue aus Konst.Array
    procedure Save;                                  // In Reg. oder DB
    procedure Translation;
    procedure Printable;

  end;

  TSetupLabel = class(TLabel)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    mAbsValueNameID: Integer;
    mValueText: string;

    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
  protected
    { Protected declarations }
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
    procedure Translation;
  end;

  TSetupEdit = class(TmmMaskEdit)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    FRestart: Boolean;
    mRegPath, mRegValueName, mDefaultValue,
      mUserValue, mHintText: string;
    mRootKey: HKEY;
    mRegValueNameExists, mUserValueChange: Boolean;
    mCursorPos: Integer;
    mMinValue, mMaxValue: Variant;
    mValueType: TDataType;
    mAbsValueNameID: Integer;
    mLocation: TLocation;
    mPCType: TClientServerType;
    mNTProd: Word;
    mValueText: string;

    procedure DeleteErrorKey;
    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
    property MaxLength;
  protected
    { Protected declarations }
    procedure Change; override;
    procedure DoExit; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property ValueChange: Boolean read mUserValueChange;
    property NeedRestart: Boolean read FRestart;
  published
    { Published declarations }
    procedure GetDefault;
    procedure GetValue;
    procedure GetArrayValue;                         // UserValue aus Konst.Array
    procedure Translation;
    procedure Save;
    procedure Printable;
    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
    property OEMConvert;
  end;

  TSetupComboBox = class(TmmComboBox)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    FRestart: Boolean;
    mRegPath, mRegValueName, mHintText,
      mUserValue, mDefaultValue: string;
    mRootKey: HKEY;
    mRegValueNameExists, mUserValueChange,
      mReadOnlyData: Boolean;
    mValueType: TDataType;
    mAbsValueNameID: Integer;
    mLastComboText: string;
    mLocation: TLocation;
    mPCType: TClientServerType;
    mNTProd: Word;
    mArrayData: TStringList;
    mValueText: string;
    mData: string;

    fOnSave: TSaveEvent;
    fOnGetValue: TGetValueEvent;
    fOnGetDefault: TGetDefaultEvent;
    FSaveItemData: Boolean;

    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
    function ItemsToString: string;
    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
    procedure StringToItems(aData, aTextForIndex: string);
    function GetData: string;
    procedure SetData(const Value: string);

  protected
    { Protected declarations }
    procedure Click; override;
    procedure Change; override;
    procedure DoExit; override;
    procedure DropDown; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure RefreshValues;
    property ValueChange: Boolean read mUserValueChange;
    property NeedRestart: Boolean read FRestart;
  published
    { Published declarations }
    procedure GetDefault;
    procedure GetValue;
    procedure GetArrayValue;                         // UserValue aus Konst.Array // ??
    procedure Save;
    procedure Translation;
    procedure Printable;
    property NativeData: string read GetData write SetData;
    property OnSave: TSaveEvent read fOnSave write fOnSave;
    property OnGetValue: TGetValueEvent read fOnGetValue write fOnGetValue;
    property OnGetDefault: TGetDefaultEvent read fOnGetDefault write fOnGetDefault;
    property SaveItemData: Boolean read fSaveItemData write FSaveItemData default FALSE;

    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
  end;

  TSetupCheckBox = class(TmmCheckBox)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    mAbsValueNameID: Integer;
    mUserValue: TCheckBoxState;
    mDefaultValue: TCheckBoxState;
    FRestart: Boolean;
    mRegPath, mRegValueName, mHintText: string;
    mRootKey: HKEY;
    mRegValueNameExists, mUserValueChange: Boolean;
    mValueType: TDataType;
    mLocation: TLocation;
    mStarted: Boolean;
    mPCType: TClientServerType;
    mNTProd: Word;
    mValueText: string;

    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
  protected
    { Protected declarations }
    procedure Click; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    { Published declarations }
  published
    procedure GetDefault;
    procedure GetArrayValue;                         // UserValue aus Konst.Array
    procedure GetValue;
    procedure Save;
    procedure Translation;
    procedure Printable;
    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
  end;

  TSetupRadioGroup = class(TmmRadioGroup)
  private
    { Private declarations }
    FBaseSetup: TBaseSetupModul;
    FUniqueValueName: string;
    mAbsValueNameID: Integer;

    mUserValue: Integer;
    mDefaultValue: Integer;
    mValueText: string;
    mData: string;

    mRegPath, mRegValueName, mHintText: string;
    mRootKey: HKEY;
    mRegValueNameExists, mUserValueChange: Boolean;
    mValueType: TDataType;
    mLocation: TLocation;
    mPCType: TClientServerType;
    mNTProd: Word;
    //    FOnChange                              : TNotifyEvent;

    procedure SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
    procedure GetRecordValues(aId: Word);
    function GetValueName: string;
    function HasValueChange(aNewValue: Integer): Boolean;

  protected
    { Protected declarations }
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure Click; override;
  public
    { Public declarations }
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published declarations }
    property ValueName: string read GetValueName write FUniqueValueName;
    property SetupModul: TBaseSetupModul read FBaseSetup write SetBaseSetupModul;
    procedure Translation;
    procedure GetValue;
    procedure GetDefault;
    procedure Save;
    procedure Printable;
    //property  Items;
  end;

function GetTabSheet(aComponent: TComponent): string;

const
  cRestart = '(*)Bitte starten Sie nach dem Speichern das MillMaster neu!'; //ivlm

implementation                                       // 15.07.2002 added mmMBCS to imported units

{$R SetupComponents.dcr}

uses
  mmMBCS,
  TypInfo, registry, comctrls;

//------------------------------------------------------------------------------
function ConvertValueToUserValue(aValue, aFactor: Variant): Variant;
begin
  try
    Result := aValue / aFactor;
  except
  end;
end;
//------------------------------------------------------------------------------
function ConvertUserValueToValue(aValue, aFactor: Variant): Variant;
begin
  Result := aValue * aFactor;
end;
//------------------------------------------------------------------------------
function GetOneRegValue(aKey: HKEY; aRegPath, aRegValueName: string): string;
begin
  Result := GetRegString(aKey, aRegPath, aRegValueName, '');
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Allgemeine SetupModul Property-Funktion
// Ermittelt die auf der Form platzierten SetupModule
//******************************************************************************
function SetOneBaseSetupModul(const aSetupModul: TBaseSetupModul;
  var aBaseSetup: TBaseSetupModul): Boolean;

begin
  Result := TRUE;
  if (aBaseSetup <> aSetupModul) then
    aBaseSetup := aSetupModul
  else begin
    aBaseSetup := nil;
    Result := FALSE;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Allgemeine SetupModul Property-Funktion
// Ermittelt ob das SetupModule mit der Komp. verbunden ist
//******************************************************************************
function CompNotification(AComponent: TComponent; Operation: TOperation;
  var aBaseSetup: TBaseSetupModul): Boolean;
var
  x, xcount: integer;
begin
  Result := TRUE;
  xcount := 0;
  if (Operation = opRemove) and (AComponent.ClassParent = TBaseSetupModul) and
    (aBaseSetup <> nil) then begin
    for x := 0 to AComponent.ComponentCount - 1 do
      if AComponent.Components[x].ClassType = TBaseSetupModul then inc(xcount);
    if xcount > 0 then exit;
    aBaseSetup := nil;
    Result := FALSE;
  end;

end;
//------------------------------------------------------------------------------
function RunTime(aComponent: TComponent): Boolean;
begin
  Result := not (csDesigning in aComponent.ComponentState);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ermittelt den Parent-TabSheet einer Komponente
//******************************************************************************
function GetTabSheet(aComponent: TComponent): string;
var
  xComp   : TComponent;
begin
  Result := '';
  xComp := aComponent;
  while Assigned(xComp) do begin
    xComp := xComp.GetParentComponent;
    if (xComp is TTabSheet) then begin
      Result := (xComp as TTabSheet).Caption;
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------

{ TSetupEdit }

//------------------------------------------------------------------------------
procedure TSetupEdit.Change;
var
  xText   : string;
begin
  inherited Change;
  if FBaseSetup <> nil then begin
    xText := FBaseSetup.UserValue[mAbsValueNameID];
    mUserValueChange := Text <> xText;
    // Daten ins Array schreiben
    FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
    FBaseSetup.UserValue[mAbsValueNameID] := Text;
    FBaseSetup.NativeValue[mAbsValueNameID] := Text;
  end;
end;
//------------------------------------------------------------------------------
constructor TSetupEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FBaseSetup := nil;
  mCursorPos := 0;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.DeleteErrorKey;
var
  xText   : string;
begin
  xText := EditText;
  Delete(xText, mCursorPos + 1, 1);
  EditText := xText;
  SetSelStart(mCursorPos);
  MessageBeep(MB_IconError);
end;
//------------------------------------------------------------------------------
destructor TSetupEdit.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.DoExit;
begin
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
  case mValueType of
    dtWord: begin
        if (StrToInt(Text) < mMinValue) then begin
          Text := mMinValue;
          MessageBeep(MB_IconError);
          MessageDlg('Min. Value = ' + mMinValue, mtWarning, [mbOk], 0);
          SetFocus;
        end;
      end;
  end;
  inherited DoExit;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.GetArrayValue;
begin
  Text := FBaseSetup.UserValue[mAbsValueNameID];
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.GetDefault;
begin
  if Text <> mDefaultValue then begin
    mUserValueChange := TRUE;
    FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
    Change;
  end;

  FBaseSetup.UserValue[mAbsValueNameID] := mDefaultValue;
  Text := mDefaultValue;
  mUserValue := mDefaultValue;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.GetRecordValues(aId: Word);
var                                                  {xValueText,}
  xMask   : string;
begin

  with FBaseSetup do begin
    mRegValueName := ValueName[aId];
    mDefaultValue := DefaultValue[aId];
    mUserValue := UserValue[aId];
    mHintText := HintText[aId];
    mRegPath := LocationName[aId];
    mRootKey := LocationKey[aId];
    mMinValue := MinValue[aId];
    mMaxValue := MaxValue[aId];
    mValueType := ValueType[aId];
    mValueText := ValueText[aId];
    xMask := Mask[aId];
    FRestart := Restart[aId];
    mLocation := Location[aId];
    mPCType := ClientServer[aId];

    Group[aId] := GetTabSheet(Self);
  end;

  {
  if xMask <> '' then
     beep;

//  if pos(mDecimalSep, mUserValue) > 0 then
  mDecimalSep:= DecimalSeparator;
  xMask := EditMask;
  xMask:= StringReplace(xMask, '.', mDecimalSep, [rfReplaceAll]) ;
  if EditMask <> '' then
     EditMask := xMask;
  }

  mNTProd := GetNTProduct;

  {
  if ((mPCType = csServer) and  (mNTProd > 1)) or
     ((mPCType = csClient) and  (mNTProd = 1)) or
     ( mPCType = csClientServer) then begin
   }
  begin
    if mValueType = dtWord then MaxLength := length(mMaxValue);

    if mLocation = lReg then
      mRegValueNameExists := FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);

    if not mRegValueNameExists and (mLocation = lReg) then begin
      Text := mDefaultValue;
      //ConvertDecimalSymbol;
    end
    else
      GetValue;

    Hint := Translate(mHintText);
    AutoLabel.Control.Caption := Translate(mValueText);
    mUserValueChange := FALSE;
    FBaseSetup.ValueChange[aId] := mUserValueChange;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.GetValue;
var
  xText   : string;
begin
  try
    if not Visible then exit;
    case mLocation of
      // Aus Reg. lesen
      lReg: xText := GetOneRegValue(mRootKey, mRegPath, mRegValueName);
      lDB: begin
          //text:= FBaseSetup.mSettings.
          xText := FBaseSetup.mSettings.DBValue[mAbsValueNameID];

          //.DBValue[14];
          //Text:=FBaseSetup.LocationName[14];

        // FBaseSetup.mSettings.ValueText[14];
          //mSettingsReader:= TBaseSettingsReader.Create;
          //mSettingsReader.Value[FUniqueValueName];
          //Text:=FBaseSetup.UserValue[14];
          //mSettingsReader.Free;
        end;
    end;
  except
    on E: Exception do begin
      MessageDLG('Error in ' + Self.Name + ' : ValueName = ' + mRegValueName + ' !' +
        #13#10 + 'Set Default Value.' + #13#10 +
        E.Message, mtError, [mbOk], 0);
      Text := mDefaultValue;
    end;
  end;

  mUserValue := xText;

  if Text <> mUserValue then begin
    mUserValueChange := FALSE;
    FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
    Change;
  end;

  FBaseSetup.UserValue[mAbsValueNameID] := mUserValue;
  FBaseSetup.NativeValue[mAbsValueNameID] := mUserValue;
  Text := mUserValue;
end;
//------------------------------------------------------------------------------
function TSetupEdit.GetValueName: string;
begin
  Result := FUniqueValueName;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.KeyUp(var Key: Word; Shift: TShiftState);
var
  xIntValue: Integer;
begin
  case mValueType of
    dtWord,
      dtDWord,
      dtDouble,
      dtInteger
      : begin
        if not (Key in [Ord('0')..Ord('9'), VK_CANCEL..VK_DOWN, VK_DELETE,
          VK_NUMPAD0..VK_NUMPAD9, VK_NUMLOCK]) then
          DeleteErrorKey
        else try
          if GetSelStart = 0 then exit;

          xIntValue := StrToInt(Text);
          if (xIntValue > mMaxValue) then begin
            DeleteErrorKey;
            // MessageDlg('Max. Value = ' + mMaxValue, mtWarning, [mbOk], 0);
          end;
        except
          on E: EConvertError do begin
            DeleteErrorKey;
            MessageDlg(e.Message, mtWarning, [mbOk], 0);
          end;
        end;
      end;

  end;
  mCursorPos := GetSelStart;
  if Text <> '' then begin
    //FBaseSetup.UserValue[mAbsValueNameID]:= ConvertUserValueToValue(Value,mFactor);
    //FBaseSetup.NativeValue[mAbsValueNameID]:= FBaseSetup.UserValue[mAbsValueNameID];
  end;
  inherited KeyUp(Key, Shift);
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.MouseUp(Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  mCursorPos := GetSelStart;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.Notification(AComponent: TComponent;
  Operation: TOperation);
var
  xResult : Boolean;
begin
  inherited Notification(aComponent, Operation);
  xResult := CompNotification(AComponent, Operation, FBaseSetup);
  if not xResult then Text := '';
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.Printable;
begin
  FBaseSetup.mSettings.Printable[mAbsValueNameID] := TRUE;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.Save;
begin
  mNTProd := FBaseSetup.NTProduct;
  if ((mPCType = csServer) and (mNTProd > 1)) or
    ((mPCType = csClient) and (mNTProd = 1)) or
    (mPCType = csServerEdit) or
    (mPCType = csClientServer) then begin
    mUserValueChange := FALSE;

    // Ins Konst.Array schreiben
    FBaseSetup.UserValue[mAbsValueNameID] := Text;
    FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
    case mLocation of
      // In die Reg. schreiben
      lReg: SetRegString(mRootKey, mRegPath, mRegValueName, Text);
      // In die DB schreiben
      lDB: FBaseSetup.mSettings.DBValue[mAbsValueNameID] := Text;
    end;
  end;
  mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
  //GetRecordValues(mAbsValueNameID);
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.SetBaseSetupModul(const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin

  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  //if not aSetupModul.DBConnected then exit;

  if SetupModul = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
  end;

  if xReturn and (ValueName <> '') then begin
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        exit;
      end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupEdit.Translation;
begin
  Hint := Translate(mHintText);
  AutoLabel.Control.Caption := Translate(mValueText);
end;
//------------------------------------------------------------------------------

{ TSetupLabel }

constructor TSetupLabel.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FBaseSetup := nil;
  AutoSize := FALSE;
  WordWrap := TRUE;
  Constraints.MinWidth := 30;
end;
//------------------------------------------------------------------------------
destructor TSetupLabel.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSetupLabel.GetRecordValues(aId: Word);
begin
  mValueText := FBaseSetup.Description[aId];
  Caption := Translate(mValueText);
end;
//------------------------------------------------------------------------------
function TSetupLabel.GetValueName: string;
begin
  Result := FUniqueValueName;
end;
//------------------------------------------------------------------------------
procedure TSetupLabel.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  CompNotification(AComponent, Operation, FBaseSetup);
end;
//------------------------------------------------------------------------------
procedure TSetupLabel.SetBaseSetupModul(
  const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin
  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  if SetupModul = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
  end;

  if xReturn and (ValueName <> '') then
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        break;
      end;
end;
//------------------------------------------------------------------------------

procedure TSetupLabel.Translation;
begin
//  Caption := Translate(mValueText);
end;
//------------------------------------------------------------------------------

{ TSetupSpinEdit }

procedure TSetupSpinEdit.Change;
begin
  inherited Change;
  if FBaseSetup <> nil then begin
    // Daten ins Array schreiben
    if (Value <> mUserValue) and (Value >= 0) then begin
      mUserValueChange := TRUE;
      mUserValue := Value;
    end;
    FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.Click;
begin
  Value := mUserValue;
  inherited;
end;
//------------------------------------------------------------------------------
constructor TSetupSpinEdit.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FBaseSetup := nil;
  AutoSize := TRUE;
  mFactor := 1.0;
  ShowHint := TRUE;
  mUserValueChange := FALSE;
  EditorEnabled := TRUE;
  ReadOnly := FALSE;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.DeleteErrorKey;
var
  xText   : string;
begin
  xText := Text;
  if mCursorPos = 1 then
    Delete(xText, mCursorPos, 1)
  else
    Delete(xText, mCursorPos + 1, 1);
  Text := xText;
  SetSelStart(mCursorPos);
  MessageBeep(MB_IconError);
end;
//------------------------------------------------------------------------------
destructor TSetupSpinEdit.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.DoExit;
var
  xVal, xUserVal: Integer;
  xErrorText, xText: string;
begin
  {
   case mValueType of
    dtWord, dtDWord,
    dtFloat, dtDouble,
    dtInteger
                      : begin
   }

  try
    xVal := StrToInt(Text);
  except
    if Text = '' then Text := '??';
    xText := Text;
    MessageBeep(MB_IconError);
    GetValue;
    xUserVal := mUserValue;
    Text := xText;
    xErrorText := Format('%s' +
      #10#13 +
      #10#13 +
      'Error with value : %s .' +
      #10#13 +
      'Setting value to uservalue !  = %d',
      [Translate(mValueText), xText, xUserVal]);

    MessageDlg(xErrorText, mtError, [mbOk], 0);

    xVal := xUserVal;

    Text := IntToStr(xVal);
    SetFocus;
    inherited DoExit;
    exit;
  end;

  Text := IntToStr(xVal);

  if (xVal < mMinValue) then begin
    Value := mMinValue;
    MessageBeep(MB_IconError);
    xErrorText := Format('%s' +
      #10#13 +
      #10#13 +
      'Min. Value = %s.',
      [Translate(mValueText), string(mMinValue)]);
    MessageDlg(xErrorText, mtWarning, [mbOk], 0);
    SetFocus;
  end;
  //       end;
//end;
  inherited DoExit;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.DownClick(Sender: TObject);
begin
  inherited;
  if Value <= mMinValue then
    Value := mMaxValue;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.GetArrayValue;
begin
  Value := ConvertValueToUserValue(FBaseSetup.UserValue[mAbsValueNameID], mFactor);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.GetDefault;
begin
  mUserValue := mDefaultValue;
  Value := mUserValue;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.GetRecordValues(aId: Word);
var
  xUserValue, xNativeValue: string;
begin
  with FBaseSetup do begin
    mRegValueName := ValueName[aId];
    mDefaultValue := DefaultValue[aId];
    xUserValue := UserValue[aId];
    xNativeValue := NativeValue[aId];
    mHintText := HintText[aId];
    mRegPath := LocationName[aId];
    mRootKey := LocationKey[aId];
    mMinValue := MinValue[aId];
    mMaxValue := MaxValue[aId];
    mValueType := ValueType[aId];
    mValueText := ValueText[aId];
    mLocation := Location[aId];
    FRestart := Restart[aId];
    mFactor := Factor[aId];
    mPCType := ClientServer[aId];

    Group[aId] := GetTabSheet(Self);
  end;

  if mFactor = 0 then
    mFactor := 1;
  {
  mNTProd :=  GetNTProduct;

  if ((mPCType = csServer) and  (mNTProd > 1)) or
     ((mPCType = csClient) and  (mNTProd = 1)) or
     ( mPCType = csClientServer) then begin
      mRegValueNameExists:= FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);
  end;
  }
  if mLocation = lReg then
    mRegValueNameExists := FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);

  //  mRegValueNameExists:= FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);

  case mValueType of
    dtWord, dtDWord,
      dtFloat, dtDouble,
      dtInteger: begin
        mMaxValue := Integer(Round(ConvertValueToUserValue(mMaxValue, mFactor)));
        mMinValue := Integer(Round(ConvertValueToUserValue(mMinValue, mFactor)));
        mDefaultValue := Integer(Round(ConvertValueToUserValue(mDefaultValue, mFactor)));
      end;
  end;

  {
  if not mRegValueNameExists then begin
       mUserValue:= mDefaultValue;
       Value := mUserValue;   // ruft onChange auf
     end
  else
     GetValue;    // ruft onChange auf
  }
  if not mRegValueNameExists and (mLocation = lReg) then begin
    mUserValue := mDefaultValue;
    Value := mUserValue;                             // ruft onChange auf
  end
  else
    GetValue;

  // GetValue;

  MinValue := mMinValue;
  MaxValue := mMaxValue;
  MaxLength := length(mMaxValue);

  Hint := Translate(mHintText);
  AutoLabel.Control.Caption := Translate(mValueText);

  mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.GetValue;
var
  xValue  : Variant;
  xText   : string;
begin
  try
    case mLocation of
      lReg: xText := GetOneRegValue(mRootKey, mRegPath, mRegValueName);
      lDB: xText := FBaseSetup.mSettings.DBValue[mAbsValueNameID];
    end;

    // mUserValue muss ein Int. sein
    if (xText = '') then
      mUserValue := mDefaultValue
    else
      mUserValue := StrToInt(xText);

    xValue := ConvertValueToUserValue(mUserValue, mFactor);
    FBaseSetup.UserValue[mAbsValueNameID] := xValue;

  except
    on E: Exception do begin
      MessageDLG('Error in ' + Self.Name + ' : ValueName = ' + mRegValueName + ' !' +
        #13#10 + 'Set Default Value.' + #13#10 +
        E.Message, mtError, [mbOk], 0);
      xValue := mDefaultValue;
    end;
  end;

  mUserValue := xValue;
  Value := xValue;
end;
//------------------------------------------------------------------------------
function TSetupSpinEdit.GetValueName: string;
begin
  Result := FUniqueValueName;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.KeyUp(var Key: Word; Shift: TShiftState);
var
  xValue  : Integer;
  xText   : Variant;
  xTemp   : string;
  x       : Integer;
  xVal1Digit, xMax1Digit: Integer;
begin
  {
   case mValueType of
     dtWord, dtDWord, dtByte,
     dtDouble, dtInteger
               : begin
                   if not (Key in [ Ord('0')..Ord('9'), VK_CANCEL .. VK_DOWN, VK_DELETE,
                     VK_NUMPAD0..VK_NUMPAD9, VK_NUMLOCK ] ) then
                     DeleteErrorKey
                   else
                     try
                       if GetSelStart = 0 then exit;
                       if (Value > mMaxValue) then begin
                         DeleteErrorKey;
                         // MessageDlg('Max. Value = ' + mMaxValue, mtWarning, [mbOk], 0);
                       end;
                     except
                       on E: EConvertError do begin
                          DeleteErrorKey;
                          MessageDlg(e.Message, mtWarning, [mbOk], 0);
                          end;
                     end;
                 end;

   end;
  {}
  {
   mCursorPos := GetSelStart;
   if Text <> '' then begin
     FBaseSetup.UserValue[mAbsValueNameID]:= ConvertUserValueToValue(Value,mFactor);
     FBaseSetup.NativeValue[mAbsValueNameID]:= FBaseSetup.UserValue[mAbsValueNameID];
   end;
  }
  try
    xText := Text;
    xValue := xText;
  except
    inherited KeyUp(Key, Shift);
    exit;
  end;

  if xValue > MaxValue then begin
    xVal1Digit := StrToInt(Text[1]);
    xTemp := IntToStr(MaxValue);
    xMax1Digit := StrToInt(xTemp[1]);

    xTemp := xText;

    if xVal1Digit > xMax1Digit then
      Delete(xTemp, 1, length(xTemp))
    else
      Delete(xTemp, SelStart, 1);

    Text := xTemp;
    SelStart := Length(Text);
  end;
  inherited KeyUp(Key, Shift);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  inherited MouseUp(Button, Shift, X, Y);
  mCursorPos := GetSelStart;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  CompNotification(AComponent, Operation, FBaseSetup);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.Printable;
begin
  FBaseSetup.mSettings.Printable[mAbsValueNameID] := TRUE;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.Save;
var
  xValue  : Variant;
begin

  mNTProd := FBaseSetup.NTProduct;

  if ((mPCType = csServer) and (mNTProd > 1)) or
    ((mPCType = csClient) and (mNTProd = 1)) or
    (mPCType = csServerEdit) or
    (mPCType = csClientServer) then begin

    xValue := ConvertUserValueToValue(Value, mFactor);
    // Ins Konst.Array schreiben
    FBaseSetup.UserValue[mAbsValueNameID] := xValue;
    FBaseSetup.NativeValue[mAbsValueNameID] := xValue;
    FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
    mUserValue := Value;
    case mLocation of
      // In die Reg. schreiben
      lReg: SetRegString(mRootKey, mRegPath, mRegValueName, xValue);
      // In die DB schreiben
      lDb: FBaseSetup.mSettings.DBValue[mAbsValueNameID] := xValue;
    end;
    mUserValueChange := FALSE;
  end;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// SetupModul mit Komp. linken
//******************************************************************************
procedure TSetupSpinEdit.SetBaseSetupModul(
  const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin
  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  // if not aSetupModul.DBConnected then exit;

  if SetupModul = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
  end;

  if xReturn and (ValueName <> '') then
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        break;
      end;
  mUserValueChange := FALSE;
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.Translation;
begin
  Hint := Translate(mHintText);
  AutoLabel.Control.Caption := Translate(mValueText);
end;
//------------------------------------------------------------------------------
procedure TSetupSpinEdit.UpClick(Sender: TObject);
begin
  inherited;
  if Value >= mMaxValue then
    Value := mMinValue;
end;
//------------------------------------------------------------------------------

{ TSetupComboBox }

procedure TSetupComboBox.Change;
var
  xChange : Boolean;
begin
  if Text = '' then exit;

  xChange := False;
  if FBaseSetup <> nil then begin
    mUserValueChange := TRUE;
    // nur lesen
    if mReadOnlyData then begin
      if ItemIndex > -1 then mLastComboText := Items.Strings[ItemIndex];
      Text := mLastComboText;
    end;

    xChange := (mUserValue <> Text);

    mUserValue := Text;
    mLastComboText := mUserValue;

    // Daten ins Array schreiben

    with FBaseSetup do begin
      ValueChange[mAbsValueNameID] := mUserValueChange;
      if mValueType = dtByte then
        mUserValue := IntToStr(ItemIndex)
      else
        UserValue[mAbsValueNameID] := Text;

      Data[mAbsValueNameID] := ItemsToString;
    end;
  end;

  if FRestart and xChange then
    MessageDlg(Translate(cRestart), mtWarning, [mbOk], 0);

  inherited Change;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.Click;
begin
end;

constructor TSetupComboBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FBaseSetup := nil;
  mArrayData := TStringList.Create;
end;
//------------------------------------------------------------------------------
destructor TSetupComboBox.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
  mArrayData.free;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.DoExit;
begin
  DropDown;
  if mValueType = dtByte then begin
    mUserValue := IntToStr(ItemIndex);
    FBaseSetup.UserValue[mAbsValueNameID] := mUserValue;
  end
  else begin
    FBaseSetup.UserValue[mAbsValueNameID] := Text;
    FBaseSetup.Data[mAbsValueNameID] := ItemsToString;
  end;
  //FBaseSetup.UserValue[mAbsValueNameID]:= Text;
 //FBaseSetup.Data[mAbsValueNameID]:= ItemsToString;
  inherited DoExit;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Neuer Text nur in CB einfuellen, wenn dieser noch nicht existiert
//******************************************************************************
procedure TSetupComboBox.DropDown;
var
  x       : integer;
  xExists : Boolean;
begin
  if Text = '' then exit;
  x := 0;
  xExists := FALSE;
  repeat
    if Items.Strings[x] = Text then xExists := TRUE;
    inc(x);
  until (x >= Items.Count) or xExists;

  if not xExists then Items.Add(Text);

  if mValueType = dtByte then
    mUserValue := IntToStr(ItemIndex)
  else
    mUserValue := Text;
  inherited DropDown;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.GetArrayValue;
begin
  {
    if mValueType = dtByte then
       beep
    else
      Text:=FBaseSetup.UserValue[mAbsValueNameID];
   }
end;
//------------------------------------------------------------------------------
function TSetupComboBox.GetData: string;
begin
  Result := mData;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.GetDefault;
var
  xValue  : string;
  xItemIndex: Integer;
begin
  xItemIndex := ItemIndex;
  if Assigned(fOnGetDefault) then begin
    fOnGetDefault(self);
    if Text <> '' then
      mUserValueChange := xItemIndex <> ItemIndex;
    exit;
  end;

  if mValueType = dtByte then begin
    xValue := FBaseSetup.DefaultValue[mAbsValueNameID];
    ItemIndex := StrToInt(xValue);
    mUserValue := xValue;
    //Text:= items.Names[StrToInt(xValueText)]
  end
  else begin
    if mValueType = dtSaveComboItemID then begin
      mUserValueChange := ItemIndex <> StrToInt(mDefaultValue);
      try
        self.ItemIndex := StrToInt(mDefaultValue);
        mUserValue := Text;
      except
        on EConvertError do begin
          self.ItemIndex := -1;
          mUserValue := Text;
        end;
      end;

    end
    else begin
      mUserValueChange := Text <> Translate(mDefaultValue);
      Text := Translate(mDefaultValue);
      mUserValue := Translate(mDefaultValue);
    end;
  end;

end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.GetRecordValues(aId: Word);
var
  xText   : TStringList;
  xPos    : integer;
  xValue  : string;
begin

  with FBaseSetup do begin
    mRegValueName := ValueName[aId];
    mDefaultValue := DefaultValue[aId];
    mUserValue := UserValue[aId];
    mHintText := HintText[aId];
    mRegPath := LocationName[aId];
    mRootKey := LocationKey[aId];
    mReadOnlyData := ReadOnlyData[aId];
    mValueType := ValueType[aId];
    mValueText := ValueText[aId];
    mData := Data[aId];
    FRestart := Restart[aId];
    mLocation := Location[aId];
    mPCType := ClientServer[aId];

    Group[aId] := GetTabSheet(Self);
  end;

  mRegValueNameExists := FALSE;

  xText := TStringList.Create;
  xText.CommaText := mData;

  // x:= -1;
  if (mData <> '') and (items.Count <= 1) then try
    //     x:=  StrToInt(mUserValue);
    StringToItems(mData, mUserValue);
  except
    StringToItems(mData, mUserValue);
  end;

  if mLocation = lReg then begin
    mRegValueNameExists := FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);

    // noch kein Reg.eintrag -> Defaultwert setzen
    if not mRegValueNameExists {and (mLocation = lReg)} then begin
      if mValueType = dtSaveComboItemID then begin
        Itemindex := StrToInt(mDefaultValue);
      end;
      //if mData <> '' then StringToItems(mData, mDefaultValue);
      //Text:= Translate ( mDefaultValue );
    end
    else begin
      try
        Itemindex := StrToInt(mUserValue);
      except
        xValue := '';
        for xpos := 1 to length(mUserValue) - 1 do
          if (Ord(mUserValue[xpos]) >= 48) and
            (Ord(mUserValue[xpos]) <= 57) then
            xValue := xValue + mUserValue[xpos]
          else
            break;

        if xValue = '' then
          ItemIndex := 0
        else try
          ItemIndex := StrToInt(xValue);
        except
          on EConvertError do begin
            ItemIndex := 0;
          end;
        end;
        mUserValue := IntToStr(ItemIndex);
        SetRegString(mRootKey, mRegPath, mRegValueName, mUserValue);
      end;

      // Reg.eintrag vorhanden -> Werte aus mmSettings holen
     {
      x:= -1;
      if (mData <> '') and (items.Count <=1 )then
         try
           x:=  StrToInt(mUserValue);
           StringToItems(mData, mUserValue);
         except
           StringToItems(mData, mUserValue);
         end;
      GetValue;
      if x> -1 then itemindex := x;
      }
    end;

  end
  else begin
    GetValue;
    if (mData <> '') and (items.Count <= 1) then
      StringToItems(mData, mUserValue);
  end;

  {
   // ItemIndex setzen
   if (mData <> '') and (items.Count > 0 )then begin
      xText           := TStringList.Create;
      xText.CommaText := mData;

      for x:=0 to xText.Count-1 do  begin
          if mUserValue = '' then
            if xText.Strings[x] = mDefaultValue then Itemindex := x
          else
            if xText.Strings[x] = mUserValue then Itemindex := x
      end;
      xText.Free;

   end;
   }

  if mValueType = dtByte then try
    ItemIndex := StrToInt(mUserValue);
  except
    ItemIndex := Items.IndexOf(mDefaultValue);
  end
  else
    mUserValue := Text;

  mUserValueChange := FALSE;

  Hint := Translate(mHintText);
  AutoLabel.Control.Caption := Translate(mValueText);
  mLastComboText := Text;

  if Text = '' then GetDefault;
  //
  Repaint;
  {}
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.GetValue;
var
  xData, xText, xValueText: string;
  xpos, xItemID: integer;

begin
  xItemID := ItemIndex;
  if Assigned(OnGetValue) then begin
    OnGetValue(self);
    if Text <> '' then
      mUserValueChange := xItemID <> ItemIndex;
    mUserValue := text;
    exit;
  end;

  try
    case mLocation of
      // Aus Reg. lesen
      lReg: begin
          if mValueType = dtSaveComboItemID then begin
            try
              xItemID := StrToInt(FBaseSetup.UserValue[mAbsValueNameID]);
              ItemIndex := xItemID;
            except
              xText := '';
              xValueText := Trim(GetRegString(mRootKey, mRegPath, mRegValueName, '0'));

              for xpos := 0 to length(xValueText) - 1 do
                if (xValueText[xpos] >= '0') and
                  (xValueText[xpos] <= '9') then
                  xText := xText + xValueText[xpos]
                else
                  break;

              if xText = '' then
                ItemIndex := 0
              else try
                ItemIndex := StrToInt(xText);
              except
                on EConvertError do begin
                  ItemIndex := 0;
                end;
              end;

            end;
          end
          else begin
            xValueText := Translate(string(FBaseSetup.UserValue[mAbsValueNameID]));
            ItemIndex := Items.IndexOfName(xValueText);
          end;

          //xData:= FBaseSetup.Data[mAbsValueNameID];
          //xValueText:= Translate(FBaseSetup.UserValue[mAbsValueNameID]);
          //xValueText:= xValueText;

        end;
      lDB: begin
          if Assigned(OnGetValue) then
            OnGetValue(self)
          else begin
            xText := FBaseSetup.mSettings.DBValue[mAbsValueNameID];
            xpos := pos('@', xText);
            if xpos <= 0 then
              xValueText := Translate(xText)
            else
              xValueText := Copy(xText, 0, xpos - 1);

            xData := Copy(xText, xpos + 1, length(xText));
            ItemIndex := Items.IndexOfName(xValueText);
          end;
        end;
    end;
  except
    on E: Exception do begin
      MessageDLG('Error in ' + Self.Name + ' : ValueName = ' + mRegValueName + ' !' +
        #13#10 + 'Set Default Value.' + #13#10 +
        E.Message, mtError, [mbOk], 0);
      xValueText := mDefaultValue;
    end;
  end;

  if xData <> '' then StringToItems(xData, mDefaultValue);
  // xpos:= Items.Count;

  mUserValueChange := Text <> xValueText;

  if mValueType = dtByte then begin
    ItemIndex := StrToInt(xValueText);
    mUserValue := xValueText;
  end
  else begin
    Text := xValueText;
    mUserValue := Text;
  end;
  FBaseSetup.Data[mAbsValueNameID] := xData;
  FBaseSetup.UserValue[mAbsValueNameID] := mUserValue;

end;
//------------------------------------------------------------------------------
function TSetupComboBox.GetValueName: string;
begin
  Result := FUniqueValueName;
end;
//------------------------------------------------------------------------------
function TSetupComboBox.ItemsToString: string;
var
  x       : integer;
  xText   : string;
  xList   : TStringList;
begin

  if Items.Count <= 0 then exit;

  xList := TStringList.Create;
  xList.Sort;
  xList.Duplicates := dupIgnore;                     // nur einmaliege Items einfuegen
  xList.AddStrings(Items);

  if (mValueType = dtByte) or (mValueType = dtDWord) then begin
    xText := Items.Strings[StrToInt(mDefaultValue)];
    xList.Find(xText, x);
  end
  else
    xList.Find(mDefaultValue, x);

  if x > 0 then xList.Delete(x);

  xText := '';
  // StringList in String kopieren
  x := xList.Count;
  repeat
    dec(x);
    xText := '"' + xList.Strings[x] + '",' + xText;
  until x = 0;

  // self.Text aus String loeschen
  //xText:=  DeleteSubStr(xText, Text + ',' );

  // letztes Komma loeschen
  if xText[length(xText)] = ',' then
    delete(xText, length(xText) - 1, 2);

  Result := xText;
  xList.Free;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  CompNotification(AComponent, Operation, FBaseSetup);
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.Printable;
begin
  FBaseSetup.mSettings.Printable[mAbsValueNameID] := TRUE;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.RefreshValues;
begin
  GetRecordValues(mAbsValueNameID);
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.Save;
var
  xData {, xText{}, xIndex: string;
  xSaveValue: string;
  x       : integer;
begin

  if Assigned(OnSave) then begin
    OnSave(self);
    FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
    mUserValueChange := FALSE;
    exit;
  end;

  //xItemIndex := ItemIndex;
  mNTProd := FBaseSetup.NTProduct;
  if ((mPCType = csServer) and (mNTProd > 1)) or
    ((mPCType = csClient) and (mNTProd = 1)) or
    (mPCType = csServerEdit) or
    (mPCType = csClientServer) then begin

    mUserValueChange := FALSE;

    for x := 0 to Items.Count - 1 do
      if Text = Self.Items.Strings[x] then ItemIndex := x;

    if mValueType = dtByte then begin
      // Wert als ItemIndex abspeichern
      xSaveValue := IntToStr(ItemIndex);
    end
    else
      xSaveValue := Text;

    if Assigned(fOnSave) then begin
      fOnSave(self);
      mUserValueChange := FALSE;
      FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
      FBaseSetup.UserValue[mAbsValueNameID] := xSaveValue;
      exit;
    end;

    // Ins Konst.Array schreiben
    FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;

    FBaseSetup.UserValue[mAbsValueNameID] := xSaveValue;
    //  FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
    xData := ItemsToString;

    case mLocation of
      // In die Reg. schreiben
      lReg: begin
          if mValueType = dtSaveComboItemID then begin
            //Wert als ItemId in Reg. schreiben
            SetRegString(mRootKey, mRegPath, mRegValueName, IntToStr(self.itemIndex));
            FBaseSetup.UserValue[mAbsValueNameID] := IntToStr(self.itemIndex);
          end
          else begin

          end;

          //Data in Reg. schreiben
          SetRegString(mRootKey, mRegPath, mRegValueName + 'Data', mData);
          FBaseSetup.Data[mAbsValueNameID] := mData;

          // if not FSaveItemData then Beep;
           //   BaseSetup.DeleteRegValue(mRootKey, mRegPath , mRegValueName + 'Data');

           {
           SetRegString(mRootKey, mRegPath, mRegValueName, xSaveValue );
           SetRegString(mRootKey, mRegPath, mRegValueName + 'Data', xData );
           FBaseSetup.Data[mAbsValueNameID]:= xData;
           FBaseSetup.UserValue[mAbsValueNameID]:=xSaveValue;
           }
        end;
      lDB: begin
          if mValueType = dtSaveComboItemID then begin
            FBaseSetup.mSettings.DBValue[mAbsValueNameID] := IntToStr(itemIndex) + '@' + xData;
          end
          else begin
            FBaseSetup.mSettings.DBValue[mAbsValueNameID] := xSaveValue + '@' + xData;
          end;
        end;
    end;

    Clear;
    GetRecordValues(mAbsValueNameID);
  end;
  //ItemIndex := xItemIndex;
  Translate(mDefaultValue);

  mUserValueChange := FALSE;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.SetBaseSetupModul(
  const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin
  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  // if not aSetupModul.DBConnected then exit;
  if SetupModul = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
  end;

  if xReturn and (ValueName <> '') then
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        break;
      end;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.SetData(const Value: string);
begin
  mData := Value;
  if mData <> Value then FBaseSetup.Data[mAbsValueNameID] := mData;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.StringToItems(aData, aTextForIndex: string);
var
  xText   : TStringList;
  xTransText: TStringList;
  x, xindex: integer;
begin
  Clear;
  xTransText := TStringList.Create;
  xText := TStringList.Create;
  xText.Sorted := FALSE;
  xText.CommaText := aData;
  xindex := 0;

  for x := 0 to xText.Count - 1 do begin
    xTransText.Add(Translate(xText.Strings[x]));
    if xText.Strings[x] = aTextForIndex then
      xindex := x
    else
      xindex := 0;
  end;

  Items.AddStrings(xTransText);
  ItemIndex := xindex;

  xText.Free;
  xTransText.Free;
end;
//------------------------------------------------------------------------------
procedure TSetupComboBox.Translation;
var                                                  //x : integer;
  xText, xText1: string;
  xId     : integer;
begin
  Hint := Translate(mHintText);
  AutoLabel.Control.Caption := Translate(mValueText);

  xText := mUserValue;
  xText1 := Text;

  xId := ItemIndex;
  if mData <> '' then begin
    clear;
    StringToItems(mData, mUserValue);
    ItemIndex := xID;
    //Text:= mUserValue;
  end;
end;

{ TSetupCheckBox }

procedure TSetupCheckBox.Click;
var
  xValue  : Boolean;
  xText   : string;
begin
  inherited Click;

  xText := FBaseSetup.UserValue[mAbsValueNameID];
  if xText = '' then exit;
  xValue := Boolean(StrToInt(xText));

  mUserValueChange := xValue <> Checked;
  FBaseSetup.UserValue[mAbsValueNameID] := Checked;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
end;
//------------------------------------------------------------------------------
constructor TSetupCheckBox.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FBaseSetup := nil;
  ShowHint := TRUE;
  State := cbUnchecked;
  mUserValueChange := FALSE;
  mStarted := FALSE;
end;
//------------------------------------------------------------------------------
destructor TSetupCheckBox.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.GetArrayValue;
var
  xRet    : Boolean;
begin
  xRet := FBaseSetup.UserValue[mAbsValueNameID];
  if xRet <> Checked then
    mUserValueChange := TRUE
  else
    mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
  Checked := xRet;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.GetDefault;
begin
  if State <> mDefaultValue then
    mUserValueChange := TRUE
  else
    mUserValueChange := FALSE;

  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;

  mUserValue := mDefaultValue;
  State := mUserValue;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.GetRecordValues(aId: Word);
var
  xUserValue: Variant;
  xDefaultValue: Boolean;
begin
  with FBaseSetup do begin
    mRegValueName := ValueName[aId];
    xDefaultValue := Boolean(DefaultValue[aId]);
    xUserValue := UserValue[aId];
    mHintText := HintText[aId];
    mRegPath := LocationName[aId];
    mRootKey := LocationKey[aId];
    mValueType := ValueType[aId];
    mValueText := ValueText[aId];
    mLocation := Location[aId];
    FRestart := Restart[aId];
    mPCType := ClientServer[aId];

    Group[aId] := GetTabSheet(Self);
  end;
  mRegValueNameExists := FALSE;
  //  mNTProd := GetNTProduct;
    {
    if ((mPCType = csServer) and  (mNTProd > 1)) or
       ((mPCType = csClient) and  (mNTProd = 1)) or
       ( mPCType = csClientServer) then begin
    }

  if mLocation = lReg then begin
    mRegValueNameExists := FBaseSetup.CheckRegExists(mRootKey, mRegPath, mRegValueName);

    if not mRegValueNameExists then begin
      case Boolean(xDefaultValue) of
        TRUE: mUserValue := cbChecked;
        FALSE: mUserValue := cbUnChecked;
      end;
      State := mUserValue;
    end
    else
      GetValue;                                      // -> on Change
  end
  else
    GetValue;

  case Boolean(xDefaultValue) of
    TRUE: mDefaultValue := cbChecked;
    FALSE: mDefaultValue := cbUnChecked;
  end;

  Hint := Translate(mHintText);
  Caption := Translate(mValueText);
  mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.GetValue;
var
  xUserValue: Byte;
  xValue  : string;
begin
  xUserValue := 0;

  try
    case mLocation of
      lReg: xValue := GetOneRegValue(mRootKey, mRegPath, mRegValueName);
      lDB: xValue := FBaseSetup.mSettings.DBValue[mAbsValueNameID];
    end;

    if xValue = '' then
      xUserValue := 0
    else
      xUserValue := StrToInt(xValue);

    case Boolean(xUserValue) of
      TRUE: State := cbChecked;
      FALSE: State := cbUnChecked;
    end;

  except
    on E: Exception do begin
      MessageDLG('Error in ' + Self.Name + ' : ValueName = ' + mRegValueName + ' !' +
        #13#10 + 'Set Default Value.' + #13#10 +
        E.Message, mtError, [mbOk], 0);
      mUserValue := mDefaultValue;
      State := mUserValue;
    end;
  end;

  if (State <> mUserValue) then
    mUserValueChange := TRUE
  else
    mUserValueChange := FALSE;

  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;

  mUserValue := State;

  FBaseSetup.UserValue[mAbsValueNameID] := abs(xUserValue);
  Checked := FBaseSetup.UserValue[mAbsValueNameID];

end;
//------------------------------------------------------------------------------
function TSetupCheckBox.GetValueName: string;
begin
  Result := FUniqueValueName;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  CompNotification(AComponent, Operation, FBaseSetup);
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.Printable;
begin
  FBaseSetup.mSettings.Printable[mAbsValueNameID] := TRUE;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.Save;
var
  xValue  : Variant;
begin
  mNTProd := FBaseSetup.NTProduct;

  if ((mPCType = csServer) and (mNTProd > 1)) or
    ((mPCType = csClient) and (mNTProd = 1)) or
    (mPCType = csServerEdit) or
    (mPCType = csClientServer) then begin

    // Ins Konst.Array schreiben

    case State of
      cbChecked: xValue := TRUE;
      cbUnChecked: xValue := FALSE;
    end;

    xValue := ABS(xValue);
    FBaseSetup.UserValue[mAbsValueNameID] := xValue;
    FBaseSetup.NativeValue[mAbsValueNameID] := xValue;
    FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;
    mUserValue := State;
    case mLocation of
      // In die Reg. schreiben
      lReg: SetRegString(mRootKey, mRegPath, mRegValueName, xValue);
      lDb: FBaseSetup.mSettings.DBValue[mAbsValueNameID] := xValue;
    end;
    // mUserValueChange :=FALSE;
  end;
  mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.SetBaseSetupModul(
  const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin
  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  // if not aSetupModul.DBConnected then exit;
  if SetupModul = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
  end;

  if xReturn and (ValueName <> '') then
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        break;
      end;
end;
//------------------------------------------------------------------------------
procedure TSetupCheckBox.Translation;
begin
  Hint := Translate(mHintText);
  //AutoLabel.Control.Caption:= Translate(mValueText);
  Caption := Translate(mValueText);
end;

{ TSetupRadioGroup }

constructor TSetupRadioGroup.Create(aOwner: TComponent);
begin
  inherited;
  FBaseSetup := nil;
  ShowHint := TRUE;
  ItemIndex := -1;
  mUserValueChange := FALSE;
end;
//------------------------------------------------------------------------------
destructor TSetupRadioGroup.Destroy;
begin
  FBaseSetup := nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.GetValue;
var
  xUserValue: string;
  xIndex  : Integer;
begin
  xUserValue := '';
  try
    case mLocation of
      lReg: xUserValue := GetOneRegValue(mRootKey, mRegPath, mRegValueName);
      lDB: xUserValue := FBaseSetup.mSettings.DBValue[mAbsValueNameID];
    end;

    if xUserValue = '' then
      xIndex := mDefaultValue
    else
      xIndex := StrToInt(xUserValue);

    mUserValueChange := HasValueChange(ItemIndex);

  except
    on E: Exception do begin
      MessageDLG('Error in ' + Self.Name + ' : ValueName = ' + mRegValueName + ' !' +
        #13#10 + 'Set Default Value.' + #13#10 +
        E.Message, mtError, [mbOk], 0);
      ItemIndex := mDefaultValue;
    end;
  end;
  ItemIndex := xIndex;

  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
  FBaseSetup.UserValue[mAbsValueNameID] := IntToStr(ItemIndex);
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.GetRecordValues(aId: Word);
var
  xUserValue: Variant;
begin
  with FBaseSetup do begin
    mRegValueName := ValueName[aId];
    mDefaultValue := StrToInt(DefaultValue[aId]);

    xUserValue := UserValue[aId];
    mHintText := HintText[aId];
    mValueText := ValueText[aId];
    mRegPath := LocationName[aId];
    mRootKey := LocationKey[aId];
    mValueType := ValueType[aId];
    mLocation := Location[aId];
    mPCType := ClientServer[aId];
    Group[aId] := GetTabSheet(Self);
  end;

  mRegValueNameExists := FALSE;

  GetValue;

  Translation;
  mUserValueChange := FALSE;
end;
//------------------------------------------------------------------------------
function TSetupRadioGroup.GetValueName: string;
var
  i       : integer;
begin
  Result := FUniqueValueName;
  Items.Clear;
  if FUniqueValueName <> '' then begin
    for i := 0 to FBaseSetup.NumberOfValues - 1 do
      if FBaseSetup.UniqueValueName[i] = FUniqueValueName then begin
        mAbsValueNameID := i;
      end;
    //    if Items.Count <= 0 then begin
    mData := FBaseSetup.Data[mAbsValueNameID];
    Items.CommaText := mData;
    //   end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  CompNotification(AComponent, Operation, FBaseSetup);
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.SetBaseSetupModul(
  const aSetupModul: TBaseSetupModul);
var
  xReturn : Boolean;
  i       : integer;
begin

  xReturn := SetOneBaseSetupModul(aSetupModul, FBaseSetup);

  if FBaseSetup = nil then begin
    ValueName := '';
    Hint := '';
    Caption := 'no SetupModul';
    Items.Clear;
    exit;
  end;

  if xReturn and (ValueName <> '') then
    // Array Index ermitteln fuer weitere Daten
    for i := 0 to FBaseSetup.NumberOfValues do
      if FBaseSetup.UniqueValueName[i] = ValueName then begin
        mAbsValueNameID := i;
        // Benoetigte Werte aus Array lesen
        if RunTime(Self) then GetRecordValues(mAbsValueNameID);
        break;
      end;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.Translation;
begin

  Hint := Translate(mHintText);
  Caption := Translate(mValueText);

  //  Items.CommaText := Translate(mData);

    //for x:= 0 to Items.Count do

    //Items.CommaText:= Translate(Items.CommaText);

    //AutoLabel.Control.Caption:= Translate(mValueText);
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.Save;
var
  xValue  : Variant;
begin
  mNTProd := FBaseSetup.NTProduct;

  if ((mPCType = csServer) and (mNTProd > 1)) or
    ((mPCType = csClient) and (mNTProd = 1)) or
    (mPCType = csServerEdit) or
    (mPCType = csClientServer) then begin
    // Ins Konst.Array schreiben

    xValue := IntToStr(ItemIndex);
    FBaseSetup.UserValue[mAbsValueNameID] := xValue;
    FBaseSetup.NativeValue[mAbsValueNameID] := xValue;
    FBaseSetup.ValueChange[mAbsValueNameID] := FALSE;

    case mLocation of
      // In die Reg. schreiben
      lReg: SetRegString(mRootKey, mRegPath, mRegValueName, xValue);
      lDb: FBaseSetup.mSettings.DBValue[mAbsValueNameID] := xValue;
    end;

  end;
  mUserValueChange := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := mUserValueChange;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.GetDefault;
begin
  mUserValueChange := HasValueChange(mDefaultValue);
  ItemIndex := mDefaultValue;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.Click;
var
  xIndex  : Integer;
  xText   : string;
begin
  inherited Click;

  xText := FBaseSetup.UserValue[mAbsValueNameID];
  if xText = '' then exit;
  xIndex := StrToInt(xText);
  mUserValueChange := HasValueChange(xIndex);
  FBaseSetup.UserValue[mAbsValueNameID] := ItemIndex;
end;
//------------------------------------------------------------------------------
function TSetupRadioGroup.HasValueChange(aNewValue: Integer): Boolean;
var
  xRet    : Boolean;
begin
  if aNewValue <> ItemIndex then
    xRet := TRUE
  else
    xRet := FALSE;
  FBaseSetup.ValueChange[mAbsValueNameID] := xRet;
  Result := xRet;
end;
//------------------------------------------------------------------------------
procedure TSetupRadioGroup.Printable;
begin
  FBaseSetup.mSettings.Printable[mAbsValueNameID] := TRUE;
end;
//------------------------------------------------------------------------------

end.

