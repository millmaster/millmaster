unit mmEditTests;

interface

uses
  TestFramework, sysutils, windows, mmEdit;

type
  TBaseMMEditTest = class(TTestCase)
  private
  protected
    mEdit: TmmEdit;
  public
    procedure SetUp; override;
    procedure TearDown; override;
  end;

  TGeneral = class(TBaseMMEditTest)
  published
    procedure Erzeugen;
    procedure Wert;
  end;

  TInput = class(TBaseMMEditTest)
  published
    procedure Value;
  end;

implementation

uses
  TestForm, SendKeys, forms;

//: ---------------------------------------------------------
procedure TBaseMMEditTest.SetUp;
begin
  inherited;

  frmMainWindow := TfrmMainWindow.Create(Application);
  frmMainWindow.Visible := true;

  mEdit := TmmEdit.Create(frmMainWindow);
  with mEdit do begin
    Parent := frmMainWindow;
    Left := 104;
    Top := 24;
    Width := 65;
    Height := 21;
    TabOrder := 0;
    Text := '0';
    Visible := True;
    Decimals := 5;
    NumMode := True;
  end;
  frmMainWindow.ActiveControl := mEdit;
end;

//: ---------------------------------------------------------
procedure TBaseMMEditTest.TearDown;
begin
  FreeAndNil(mEdit);
  FreeAndNil(frmMainWindow);
  inherited;
end;

procedure TGeneral.Erzeugen;
begin
  CheckTrue(mEdit.Visible, 'Editfeld ist offenbar nicht sichtbar');
end;

procedure TGeneral.Wert;
var
  xExtended: extended;
begin
  CheckEquals(0, mEdit.AsInteger, 'Wert sollte nach dem Erzeugen 0 sein');

  mEdit.AsInteger := 5;
  CheckEquals(5, mEdit.AsInteger, 'Es wurde der Wert 5 zugewiesen. Stimmt leider nicht :(');

  mEdit.AsFloat := 6.5;
  CheckEquals(6,5, mEdit.AsFloat, 'Es wurde der Wert 6.5 zugewiesen. Stimmt leider nicht :(');

  mEdit.Value := 6.5;
  xExtended := mEdit.Value;
  CheckEquals(6.5, xExtended, 'Es wurde der Wert 6.5 zugewiesen. Stimmt leider nicht :(');
end;

procedure TInput.Value;
begin
  mEdit.Text := '';
  SendTextToHandle(frmMainWindow.Handle, '1234');
  CheckEquals(1234, mEdit.AsFloat, 'Es wurde der Wert 1234 zugewiesen. Stimmt leider nicht :(');
end;

initialization
  RegisterTest('Basis Tests', TGeneral.Suite);
  RegisterTest('GUI Inputs', TInput.Suite);

end.

