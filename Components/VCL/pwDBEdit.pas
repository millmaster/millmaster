(*=============================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: pwDBEdit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.04.2001  0.00  PW  | Initial Release
|                       | Show component state Edit/ReadOnly implemented
| 19.12.2001  1.01  Wss | Extended ShowMode changed to use ReadOnly state
|============================================================================================*)

unit pwDBEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, wwdbedit, mmCtrls, LoepfeGlobal;

type
  TpwDBEdit = class(TwwDBEdit)
  private
    fColor: TColor;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    function GetColor: TColor;
    procedure SetColor(Value: TColor);
    procedure SetReadOnlyColor(Value: TColor);
    procedure SetShowMode(Value: TShowMode);
    procedure UpdateProperties;
    function GetReadOnly: Boolean;
    procedure SetReadOnly(const Value: Boolean);
  protected
//    function GetEnabled: Boolean; reintroduce; virtual;
    procedure SetEnabled(Value: Boolean); override; //reintroduce; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
  published
    property Color: TColor read GetColor write SetColor;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
  end; //TpwDBEdit

procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('mmWorbis', [TpwDBEdit]);
end; //procedure Register
//-----------------------------------------------------------------------------
constructor TpwDBEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fColor         := inherited Color;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;
end; //constructor TpwDBEdit.Create
//-----------------------------------------------------------------------------
destructor TpwDBEdit.Destroy;
begin
  inherited Destroy;
end; //destructor TpwDBEdit.Destroy
//-----------------------------------------------------------------------------
function TpwDBEdit.GetColor: TColor;
begin
  Result := fColor;
end; //function TpwDBEdit.GetColor
//-----------------------------------------------------------------------------
{
function TpwDBEdit.GetEnabled: Boolean;
begin
  Result := inherited Enabled;
end; //function TpwDBEdit.GetEnabled
{}
//-----------------------------------------------------------------------------
function TpwDBEdit.GetReadOnly: Boolean;
begin
  Result := inherited ReadOnly;
end;
//------------------------------------------------------------------------------
procedure TpwDBEdit.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
end; //procedure TpwDBEdit.Loaded
//-----------------------------------------------------------------------------
procedure TpwDBEdit.SetColor(Value: TColor);
begin
  if fColor <> Value then begin
    fColor := Value;
    UpdateProperties;
  end; //if fColor <> Value then begin
end; //procedure TpwDBEdit.SetColor
//-----------------------------------------------------------------------------
procedure TpwDBEdit.SetEnabled(Value: Boolean);
begin
//  inherited Enabled := Value;
  inherited SetEnabled(Value);
  UpdateProperties;
end; //procedure TpwDBEdit.SetEnabled
//-----------------------------------------------------------------------------
procedure TpwDBEdit.SetReadOnly(const Value: Boolean);
begin
  inherited ReadOnly := Value;
  UpdateProperties;
end;
//------------------------------------------------------------------------------
procedure TpwDBEdit.SetReadOnlyColor(Value: TColor);
begin
  if fReadOnlyColor <> Value then begin
    fReadOnlyColor := Value;
    UpdateProperties;
  end; //if fReadOnlyColor <> Value then begin
end; //procedure TpwDBEdit.SetReadOnlyColor
//-----------------------------------------------------------------------------
procedure TpwDBEdit.SetShowMode(Value: TShowMode);
begin
  if fShowMode <> Value then begin
    fShowMode := Value;
    UpdateProperties;
  end; //if fShowMode <> Value then begin
end; //procedure TpwDBEdit.SetShowMode
//-----------------------------------------------------------------------------
procedure TpwDBEdit.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then begin
    if (fShowMode = smExtended) and ReadOnly then
      inherited Color := fReadOnlyColor
    else
      inherited Color := fColor;
  end;
end; //procedure TpwDBEdit.UpdateProperties

end. //pwDBEdit

