(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: 
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enth�lt die Klasse f�r den einfachen Zugang zu den UserFields von MMStyle.
|
| Info..........: 
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.07.2003        Wss | Klasse erstellt
|=========================================================================================*)
unit StyleUserFields;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, ADODBAccess;

type
  TStyleUserFields = class (TComponent)
  private
    fColList: TStringList;
    fStyleID: Integer;
    fTitleList: TStringList;
    fValueList: TStringList;
    mDB: TADODBAccess;
    mInitialized: Boolean;
    function GetFieldCount: Integer;
    function GetTitle(Index: Integer): string;
    function GetValue(Index: Integer): string;
    function Init: Boolean;
    procedure SetStyleID(const Value: Integer);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property FieldCount: Integer read GetFieldCount;
    property StyleID: Integer read fStyleID write SetStyleID;
    property Title[Index: Integer]: string read GetTitle;
    property Value[Index: Integer]: string read GetValue;
  end;
  

implementation
uses
  IniFiles;
  
const
  cNumFieldCount    = 3;
  cStringFieldCount = 7;

  cQrySelectUserValues = 'select c_num1, c_num2, c_num3, c_string1, c_string2, c_string3, c_string4, c_string5, c_string6, c_string7 from t_style ' +
                         'where c_style_id = %d'; 

  cQrySelectStyleProfile = 'select c_text from t_settings ' +
                           'where c_username = ''ALL_USERS'' ' +
                           'and c_applicationname = ''MMSTYLE'' ' +
                           'and c_formname = ''T_STYLE'' ' +
                           'and c_profilename = ''USER_FIELDS'' ';
//-----------------------------------------------------------------------------                           
function SortActiveFields(List: TStringList; Index1, Index2: Integer): Integer;
var
  xInt1, xInt2: Integer;
begin
  with List do begin
    xInt1 := Integer(Objects[Index1]);
    xInt2 := Integer(Objects[Index2]);
    if xInt1 > xInt2 then Result := 1
    else if xInt1 < xInt2 then Result := -1
    else Result := 0;
  end;
end;
//-----------------------------------------------------------------------------                           

//:---------------------------------------------------------------------------
//:--- Class: TStyleUserFields
//:---------------------------------------------------------------------------
constructor TStyleUserFields.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fStyleID      := 0;
  mInitialized  := False;
  
  fColList   := TStringList.Create;
  fTitleList := TStringList.Create;
  fValueList := TStringList.Create;
  
  if not(csDesigning in ComponentState) then begin
    mDB := TADODBAccess.Create(1, False);
  end;
end;

//:---------------------------------------------------------------------------
destructor TStyleUserFields.Destroy;
begin
  fColList.Free;
  fTitleList.Free;
  fValueList.Free;
  mDB.Free;
  //:...................................................................
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TStyleUserFields.GetFieldCount: Integer;
begin
  Init;
  Result := fColList.Count;
end;

//:---------------------------------------------------------------------------
function TStyleUserFields.GetTitle(Index: Integer): string;
begin
  Init;
  if (Index >= 0) and (Index < fTitleList.Count) then
    Result := fTitleList.Strings[Index]
  else
    Result := '';
end;

//:---------------------------------------------------------------------------
function TStyleUserFields.GetValue(Index: Integer): string;
begin
  if (Index >= 0) and (Index < fValueList.Count) then
    Result := fValueList.Strings[Index]
  else
    Result := '';
end;

//:---------------------------------------------------------------------------
function TStyleUserFields.Init: Boolean;
var
  xIniList: TMemIniFile;
  xStrList: TStringList;
  i: Integer;
  
  //...............................................................
  procedure AddTitleString(aIndex: Integer; aSection: String);
  var
    xPos: Integer;
    xTitle: String;
    xField: String;
  begin
    xField := aSection + IntToStr(aIndex);
    with xIniList do begin
      if ReadBool(xField, 'Active', False) then begin
        xTitle := ReadString(xField, 'FieldName', '');
        xPos   := ReadInteger(xField, 'Position', 0);
        fColList.AddObject(xField, Pointer(xPos));
        fTitleList.AddObject(xTitle, Pointer(xPos));
      end; // if ReadBool
    end; // with xIniList
  end;
  //...............................................................
  
begin
  if not mInitialized then begin
    if mDB.Init then begin
      with mDB.Query[0] do
      try
        Close;
        SQL.Text := cQrySelectStyleProfile;
        Open;
        if FindFirst then begin
          xIniList := TMemIniFile.Create('');
          xStrList := TStringList.Create;
          try

            xStrList.Text := FieldByName('c_Text').AsString;
            xIniList.SetStrings(xStrList);

            for i:=1 to cNumFieldCount do
              AddTitleString(i, 'c_num');
            for i:=1 to cStringFieldCount do
              AddTitleString(i, 'c_string');

            fColList.CustomSort(SortActiveFields);
            fTitleList.CustomSort(SortActiveFields);
          finally
            xStrList.Free;
            xIniList.Free;
          end;
        end;
        Close;
      except
      end;
    end;
    mInitialized := True;
  end;
  Result := mInitialized;
end;

//:---------------------------------------------------------------------------
procedure TStyleUserFields.SetStyleID(const Value: Integer);
var
  i: Integer;
begin
  if fStyleID <> Value then
  begin
  //:...................................................................
    fStyleID := Value;
  //:...................................................................
  fValueList.Clear;
  if (fStyleID > 0) and Init then begin
    with mDB.Query[0] do
    try
      Close;
      SQL.Text := Format(cQrySelectUserValues, [fStyleID]);
      Open;
      if FindFirst then begin
        for i:=0 to fColList.Count-1 do
          fValueList.Add(FieldByName(fColList.Strings[i]).AsString);
      end;
      Close;
    except
    end;
  end;
  //:...................................................................
  end;
end;


end.
