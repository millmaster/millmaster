(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SpindleFieldsFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.12.1999  1.00  Mg  | Datei erstellt
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 18.09.2002  1.11  Nue | Umbau auf ADO.
| 07.11.2002        LOK | In TSpindleFields.UpdateComponent wird visible auf true gesetzt. F�r Maschinen
|                       | mit Fix Spindle Range kann visible = false sein, da die Daten zuerst von der Maschine
|                       | geladen werden m�ssen und w�hrend dieser Zeit die Settings nicht angezeigt werden d�rfen.
|                       | (siehe 'TAssign.SetSpdRange()')
| 27.11.2002  1.12  Nue | mInitialized added and more (Nue/Lok).
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 22.04.2003  1.13  Nue | if csDesigning in ComponentState then exit; eingef�gt.
| 27.08.2003  1.20  Nue | Ueberarbeitung Assignment: Unit MiscFrame und MaGrpNrFrame in SpindleFieldsFrame verschmolzen.
| 13.10.2003  1.21  Nue | Lowerlimit f�r Eingabe YarnCount von 2.0 auf 0.4
| 19.08.2004  1.22  Nue | MachGrp changed to Group. (Neue Typen wegen Anpassung auf XML)
| 03.10.2005  1.23  Nue | Bei FixSpdRanges wird lbSections invisible dargestellt.
|                       | EnableNetDependentFields added.
| 13.10.2005        Nue | Anpassungen weil bei FixSpdRange-Maschinen beim Aufruf des AskAssign.FormShow ein upload from machine gemacht wird.
| 07.06.2006        Nue | Richtiges Handling von leeren Spindelbereichen.
| 21.02.2008  1.30  Nue | Anpassungen wegen neuer Maschinentypen. (mtAC5, mtSavioEsperoNuovo, mtSavioExcello, mtSavioPolar)
===============================================================================*)
unit SpindleFieldsFrame;

interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmEdit, mmLabel, mmGroupBox, AssignComp, Mask, mmMaskEdit,
  ExtCtrls, mmPanel, SettingsReader, ComCtrls, mmUpDown,
  NumCtrl, IvDictio, BaseGlobal, LoepfeGlobal, SettingsFrame, mmComboBox,
  MMUGlobal, YMParaDef, MMSecurity, XMLDef, mmListBox;

resourcestring
  cFieldMaGrpNrNotSet = '(*)Bitte Maschinen Gruppe angeben.'; // ivlm
  cSettingsNotFromMachine = '(*)Keine Verbindung zur Maschine! Reinigereinstellungen werden ab Datenbank geladen!'; // ivlm

  cFieldYarnCntNotSet = '(*)Bitte eine Garn Nummer angeben.'; //ivlm
  cFieldThreadCntNotSet = '(*)Bitte eine Fadenzahl angeben.'; //ivlm
  cFieldSlipNotSet = '(*)Bitte einen Schlupf angeben.'; //ivlm

  cFieldFirstNotSet = '(*)Bitte ein Wert fuer Erste Spulstelle angeben.'; //ivlm
  cFieldLastNotSet = '(*)Bitte ein Wert fuer Letzte Spulstelle angeben.'; //ivlm
  cFieldPilotNotSet = '(*)Bitte ein Wert fuer Pilot Spulstellen angeben.'; //ivlm

type
//..............................................................................
  TSpindleFields = class(TFrame, INavChange)
    mmPanel1: TmmPanel;
    laFirst: TmmLabel;
    edLast: TmmMaskEdit;
    laLast: TmmLabel;
    laPilot: TmmLabel;
    edPilot: TmmMaskEdit;
    laSpindleRange: TmmLabel;
    udFirst: TmmUpDown;
    udLast: TmmUpDown;
    udPilot: TmmUpDown;
    edFirst: TmmMaskEdit;
    laGrpNr: TmmLabel;
    cobGrpNr: TmmComboBox;
    laAssign: TmmLabel;
    mmPanel2: TmmPanel;
    laYarnCount: TmmLabel;
    laSlip: TmmLabel;
    lbYarnCountUnit: TmmLabel;
    laThreads: TmmLabel;
    edYarnCnt: TmmMaskEdit;
    edSlip: TmmMaskEdit;
    edThreadCnt: TmmMaskEdit;
    mmUpDown1: TmmUpDown;
    laSections: TmmLabel;
    lbSections: TmmListBox;
    cobMemories: TmmComboBox;
    labMemories: TmmLabel;
    procedure edChange(Sender: TObject);
    procedure edExit(Sender: TObject);
    procedure edKeyPress(Sender: TObject; var Key: Char);
    procedure cobGrpNrChange(Sender: TObject);
    procedure FrameClick(Sender: TObject);
    procedure edYarnCntKeyPress(Sender: TObject; var Key: Char);
    procedure edSlipKeyPress(Sender: TObject; var Key: Char);
    procedure edThreadCntKeyPress(Sender: TObject; var Key: Char);
    procedure lbSectionsClick(Sender: TObject);
    procedure mmPanel1Exit(Sender: TObject);
  private
    //MaGrp
    mMachineSettings: PXMLSettingsCollection;
//    mMachineSettings: TSettingsArr;
    fFixSpdRange: boolean;
    mSavedMaGrpNr: Integer; //Nue:9.7.03
    fOnChanged: TNotifyEvent;

    //SpindleFields
    mNumSpindles: integer;
    fSpdRangeList: TSpindleRangeList;
    fOnValueChanged: TOnValueChange;
    fSettings: TSettings;
    mInitialized: Boolean; //Nue/lok:27.11.02

    //Misc
    fYarnCntUnit: TYarnUnit;
    fOnFocusChanged: TOnFocusChange;
    fFieldsInLine: boolean;
    fRunGrpData: boolean;
    fFixSpdRangeInProd: Boolean;
//    fYarnCnt: Double;   //Nue:7.3.06
    mMachineType: TMachineType;
    mMaxGrps: Integer;

    //MaGrp
    function GetGrpNr: integer;
    procedure SetItemIndex(aGrpNr: integer);
    procedure SetAssignedMark(aMark: string);
    function GetAssignedMark(): string;
//    procedure FillCob(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr);
    procedure FillCob(aSpdRange: TBaseSpindleRange; aMachSettings:
        PXMLSettingsCollection; aAdditionalMemories: Boolean);

    //SpindleFields
    function toInt(aVal: string): integer;
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    procedure SetSpindleFirst(aSpindle: integer);
    procedure SetSpindleLast(aSpindle: integer);
    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    function CheckUserValue: boolean;
    procedure Check;
    function GetSpindleFirst: integer;
    function GetSpindleLast: integer;
    function GetPilotSpindle: integer;

    //Misc
    procedure SetYarnCnt(aYarnCnt: Double);
    procedure SetYarnCntUnit(aYarnCntUnit: TYarnUnit);
    procedure SetSlip(aSlip: Single);
    procedure SetFieldsInLine(aInLine: boolean);
    procedure SetThreadCnt(aCnt: integer);
    function GetYarnCnt: Double;
    function GetSlip: Single;
    function GetThreadCnt: integer;
    function toFloat(aStr: string): Single;
    procedure SetPilotSpindle(const Value: integer);

    //SpindleFields
//wss    property NumSpindles: integer write mNumSpindles;

  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;

    //MaGrp
    property GrpNr: integer read GetGrpNr;
    property AssignedMark: string read GetAssignedMark write SetAssignedMark;
    property FixSpdRangeInProd: Boolean read fFixSpdRangeInProd;
    property FixSpdRange: Boolean read fFixSpdRange;

    //SpindleFields
    property SpindleFirst: integer read GetSpindleFirst write SetSpindleFirst;
    property SpindleLast: integer read GetSpindleLast write SetSpindleLast;
    property PilotSpindle: integer read GetPilotSpindle write SetPilotSpindle; //Nue:8.7.03 Neu SetPilotSpindle
//    property NumSpindles: integer write mNumSpindles;   //Nue:21.05.03
//
    //Misc
    property YarnCnt: Double read GetYarnCnt write SetYarnCnt;
    property YarnCntUnit: TYarnUnit read fYarnCntUnit write SetYarnCntUnit;
    property Slip: Single read GetSlip write SetSlip;
    property ThreadCnt: integer read GetThreadCnt write SetThreadCnt;
    procedure EnableNetDependentFields;
    procedure RestoreMaGrpNr;
  published
    //SpindleFields
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
    property Settings: TSettings read fSettings write fSettings;
    property OnChanged: TNotifyEvent read fOnChanged write fOnChanged;
    //Misc
    property FieldsInLine: boolean read fFieldsInLine write SetFieldsInLine;
    property RunGrpData: boolean read fRunGrpData write fRunGrpData;

  end;
//..............................................................................
//procedure Register;
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, typinfo, SettingsNavFrame;
{$R *.DFM}
//------------------------------------------------------------------------------
{
  procedure Register;
  begin
    RegisterComponents('Assignments', [TSpindleFields]);
  end;
{}
//------------------------------------------------------------------------------
constructor TSpindleFields.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fOnValueChanged := nil;
  fOnChanged := nil;
  fFixSpdRangeInProd := False;

  mInitialized := False; //Nue/lok:27.11.02
  mMachineSettings := nil;
  try
    edPilot.Text := IntToStr(TMMSettingsReader.Instance.Value[cPilotSpindles]);
  except
  end;

  //Misc
  fOnFocusChanged := nil;
  fFieldsInLine := False;
  mMachineType := mtGeneric;
  mMaxGrps := cWSCSpdGroupLimit; //Nue:7.6.06

  try
    fYarnCntUnit := TMMSettingsReader.Instance.Value[cYarnCntUnit];
    lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit]; //Neu 4.3.2002 Nue
//      fYarnCntUnit := TYarnUnit ( StrToInt ( TMMSettingsReader.Instance.Value[cYarnCntUnit]) );
      //fYarnCntUnit := yuNm;
    edSlip.Text := Format('%1.3f', [1.000]);
  except
    on e: Exception do begin
      SystemErrorMsg_('TMisc.Create failed. ' + e.message);
    end;
  end;

end;
//------------------------------------------------------------------------------
destructor TSpindleFields.Destroy;
begin
  if Assigned(mMachineSettings) then
    FreeMem(mMachineSettings);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if Assigned(fSpdRangeList) then begin
    fSpdRangeList.add(self);
  end; //if Assigned(fSpdRangeList)
end;

//------------------------------------------------------------------------------
procedure TSpindleFields.SetSpindleFirst(aSpindle: integer);
begin
  if udFirst.Position <> aSpindle then begin
    udFirst.Position := aSpindle;
    edFirst.Text := IntToStr(aSpindle);
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetSpindleLast(aSpindle: integer);
begin
  if udLast.Position <> aSpindle then begin
    udLast.Position := aSpindle;
    edLast.Text := IntToStr(aSpindle);
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetPilotSpindle(const Value: integer);
begin
  if (Value <= udPilot.Max) and (Value <= (StrToInt(edLast.Text) - StrToInt(edFirst.Text) + 1)) and (Value > 0) then
    edPilot.Text := IntToStr(Value)
  else
    edPilot.Text := IntToStr(1);
end;
//------------------------------------------------------------------------------

procedure TSpindleFields.UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss procedure TSpindleFields.UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
var
  xSize: DWord;
  x: Integer;
  xCobGrp: Integer;
  xNewIndex: Integer;
  y: Integer;
  //.............................................................
  procedure InitByOrderPosition(aOrderPosition: TOrderPosition);
  begin
//      YarnCntUnit := aOrderPosition.YarnCntUnit;
    YarnCnt := (aOrderPosition as TOrderPosition).YarnCnt; //FloatToStr ( aOrderPosition.YarnCnt );
//      YarnCnt     := YarnCountConvert(aOrderPosition.YarnCntUnit, fYarnCntUnit, (aOrderPosition as TOrderPosition).YarnCnt);//FloatToStr ( aOrderPosition.YarnCnt );

      //Slip added: Nue 21.05.01
    if aOrderPosition.Slip <> '' then
      edSlip.Text := aOrderPosition.Slip
    else if aSpdRange.Slip <> 0 then
      edSlip.Text := Format('%1.3f', [aSpdRange.Slip / cSlipFactor])
    else
      edSlip.Text := Format('%1.3f', [1.000]);

  end;
  //.............................................................
  procedure InitByProdGrp(aProdGrp: TProdGrp);
  begin
//    YarnCntUnit := aProdGrp.YarnCntUnit; //Nue:28.8.03 Before this line was active //Nue:4.4.02 Before this line was disabled
    YarnCnt := (aSpdRange as TProdGrp).YarnCnt; //FloatToStr ( aProdGrp.YarnCnt );
//      YarnCnt     := YarnCountConvert(aProdGrp.YarnCntUnit, fYarnCntUnit, (aSpdRange as TProdGrp).YarnCnt);

    edThreadCnt.Text := Format('%d', [(aSpdRange as TProdGrp).getThreadCnt]);
    edSlip.Text := (aSpdRange as TProdGrp).Slip;
  end;
  //.............................................................
  procedure InitByMiscInfo(aMiscInfo: IMiscInfo);
  begin
//Nue 25.06.01 TTemplate
//      YarnCntUnit := aMiscInfo.getYarnCntUnit;
    if aMiscInfo.getYarnCnt < 0.001 then
      edYarnCnt.Text := ''
    else
      YarnCnt := aMiscInfo.getYarnCnt;
//          YarnCnt := YarnCountConvert(aMiscInfo.getYarnCntUnit, fYarnCntUnit, aMiscInfo.getYarnCnt);

    edThreadCnt.Text := Format('%d', [aMiscInfo.getThreadCnt]);
      //Slip added: Nue 21.05.01
    if aMiscInfo.getSlip = '@' then //Nue 25.06.01 TPreselect
//      edSlip.Text := ''
      edSlip.Text := Format('%1.3f', [1.000]) //Nue:17.7.03
    else if aMiscInfo.getSlip <> '' then
      edSlip.Text := aMiscInfo.getSlip
//Alt bis 19.3.02 Nue: No longer show slip from machine to assignment GUI
//      else if SpdRangeList.AssMachine.Slip<>0 then    //@@Nue darf so zugegriffen werden?
//        edSlip.Text := Format ( '%1.3f',[SpdRangeList.AssMachine.Slip/cSlipFactor] )
    else
      edSlip.Text := Format('%1.3f', [1.000]); //@@Nue 21.06.01 '' ausgeben wenn TPreselect (codieren!!)

  end;
  //.............................................................
  procedure InitByNil;
  begin
    edYarnCnt.Text := '';
    edThreadCnt.Text := '1';
    edSlip.Text := Format('%1.3f', [1.000]);
//      fYarnCntUnit := TYarnUnit ( StrToInt ( TMMSettingsReader.Instance.Value[cYarnCntUnit]) );
    lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit];
  end;
  //.............................................................
begin
  try // -> finally

    // Misc
    try // -> except
      edYarnCnt.Enabled := False;  //Nue: 23.11.05 von False auf True
      edSlip.Enabled := True;
      edThreadCnt.Enabled := False;

      if not(cobGrpNr.Enabled) then begin
        //Weil bei FixSpdRange-Maschinen beim Aufruf des AskAssign.FormShow
        //dieses Feld diasabled wird (wegen upload from machine). Nue:13.10.05
        cobGrpNr.Enabled := True;
        //Event an AskAssign, um abh�ngige Objects zu behandeln
        fOnChanged(Self);
      end;

      if Assigned(aSpdRange) then begin
        mMachineType := aSpdRange.MachineType;
        if mMachineType in [mtAC338, mtAC5] then begin   //Nue:21.02.08  mtAC5
          //edSlip.Enabled := false;
          if (aSpdRange is TProdGrp) then
            InitByProdGrp(aSpdRange as TProdGrp)
          else
            InitByNil;
        end
        else begin
          edThreadCnt.Enabled := False;
          if (aSpdRange is TProdGrp) then
            InitByProdGrp(aSpdRange as TProdGrp)
        end;
//        case aSpdRange.FrontType of
//          ftInf68K, ftInfPPC: begin
//              //edSlip.Enabled := false;
//              if (aSpdRange is TProdGrp) then
//                InitByProdGrp(aSpdRange as TProdGrp)
//              else
//                InitByNil;
//            end;
//        else
//          edThreadCnt.Enabled := False;
//          if (aSpdRange is TProdGrp) then
//            InitByProdGrp(aSpdRange as TProdGrp)
//        end;
      end;

      if Assigned(aMiscInfo) then
        InitByMiscInfo(aMiscInfo);
    except
      on e: Exception do begin
        SystemErrorMsg_('TMisc.UpdateComponent failed. ' + e.message);
      end;
    end;

    // MaGrp
    if Assigned(aSpdRange) then begin
      if Assigned(mMachineSettings) then begin
        // Funktioniert FreeAndNil() auch mit Pointer oder nur mit Objekten?
        FreeMem(mMachineSettings);
        mMachineSettings := Nil;
      end;
      if Assigned(aXMLSettingsCollection) then begin

        xSize := SizeOf(TXMLSettingsCollection) + StrLen(aXMLSettingsCollection^.XMLData);
        mMachineSettings := AllocMem(xSize);
        System.Move(aXMLSettingsCollection^, mMachineSettings^, xSize);
        cobGrpNrChange(Self);  //Refresh ausl�sen Nue 21.11.05
      end;
      fFixSpdRange := aSpdRange.FixSpdRanges;
      FillCob(aSpdRange, mMachineSettings, True);

      if (aSpdRange is TProdGrp) then begin
        SetItemIndex((aSpdRange as TProdGrp).GrpNr);
        mSavedMaGrpNr := (aSpdRange as TProdGrp).GrpNr;
        mmPanel2.Color := (aSpdRange as TProdGrp).Color;
      end
      else begin
        if fFixSpdRange then
//          cobGrpNr.ItemIndex := cobGrpNr.Items.Count
          cobGrpNr.ItemIndex := 0
        else if not aSpdRange.IsInProduction then
          cobGrpNr.ItemIndex := 0
        else
          cobGrpNr.ItemIndex := cobGrpNr.Items.Count - 1;
        mmPanel2.Color := clWhite;
      end;
    end;

    // SpindleFields
    if Assigned(aSpdRange) then begin
      if aSpdRange.FixSpdRanges then begin
        edFirst.Enabled := False;
        edLast.Enabled := False;
        udFirst.Enabled := False;
        udLast.Enabled := False;
      end;
      mNumSpindles := aSpdRange.NumSpindles;

      if Assigned(fSpdRangeList.AssMachine) then begin
        if aSpdRange.NoRunningProdGrp then begin
//        //Erste nicht belegte Gruppe suchen und  cobGrpNr.ItemIndex setzen
//          for x := 0 to cobGrpNr.Items.Count - 1 do begin // -2 weil letzter Eintrag ist Leerstring
//            if NOT fSpdRangeList.CheckGrpNr(StrToInt(cobGrpNr.Items[x])-1) then begin
//              cobGrpNr.ItemIndex := x;
//              mSavedMaGrpNr := StrToInt(cobGrpNr.Items[x])-1;
//              BREAK; //!!!!!!!!!!
//            end;
//          end;

          if (Assigned(mMachineSettings) and fFixSpdRange) then begin
            for x:=0 to mMaxGrps-1 do begin
              //Durch alle m�glichen Gruppen gehen und die Gruppe finden bei der die gesuchte FirstSpindel ist
              if mMachineSettings.SettingsArr[x].SpindleFirst = aSpdRange.SpindleFirst then begin
                xCobGrp := mMachineSettings.SettingsArr[x].Group+1;
                for y := 0 to cobGrpNr.Items.Count - 2 do begin // -2 weil letzter Eintrag ist Leerstring
                  //Suchen, welcher Index in cobGrpNr die gesuchte Gruppe (xCobGrp) enth�lt -> Index setzen
                  if xCobGrp=StrToInt(cobGrpNr.Items[y]) then begin
                    cobGrpNr.ItemIndex := y;
                    mSavedMaGrpNr := StrToInt(cobGrpNr.Items[y])-1;
                    BREAK; //!!!!!!!!!!
                  end;
                end;
                BREAK; //!!!!!!!!!!
              end;
            end;
          end
          else begin
          //Erste nicht belegte Gruppe suchen und  cobGrpNr.ItemIndex setzen
            for x := 0 to cobGrpNr.Items.Count - 1 do begin // -2 weil letzter Eintrag ist Leerstring
              if fSpdRangeList.CheckGrpNr(StrToInt(cobGrpNr.Items[x])-1, aSpdRange.SpindleFirst, xNewIndex) then begin
                cobGrpNr.ItemIndex := xNewIndex;
                mSavedMaGrpNr := StrToInt(cobGrpNr.Items[xNewIndex])-1;
                BREAK; //!!!!!!!!!!
              end;
            end;

          end; //else
        end;
      end;

      udFirst.Max := mNumSpindles;
      udLast.Max := mNumSpindles;
      udPilot.Max := mNumSpindles;
      udFirst.Position := aSpdRange.SpindleFirst;
      udLast.Position := aSpdRange.SpindleLast;
//      edFirst.Text := IntToStr(aSpdRange.SpindleFirst);  //Nue:17.7.03
//      edLast.Text := IntToStr(aSpdRange.SpindleLast);  //Nue:17.7.03
      if aSpdRange is TProdGrp then
        udPilot.Position := (aSpdRange as TProdGrp).PilotSpindles
      else
        udPilot.Position := TMMSettingsReader.Instance.Value[cPilotSpindles];

    end;
  finally
      // LOK (7.11.2002): TSpindleFields wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
      // ==> Hier wieder einblenden
    visible := True;
    mInitialized := True; //Nue/lok:27.11.02
  end
////Misc
//  try
//    try
//  //      InitByNil;
//      edYarnCnt.Enabled := True;
//      edSlip.Enabled := True;
//      edThreadCnt.Enabled := False;
//
//      if Assigned(aSpdRange) then begin
//        case aSpdRange.FrontType of
//          ftSWSInformatorSBC5: begin
//  //Nue: 23.01.02 Wozu???            edYarnCnt.Enabled := not fRunGrpData;
//  //Nue: 23.01.02 Wozu???            edThreadCnt.Enabled := not fRunGrpData;
//              //edSlip.Enabled := false;
//              if (aSpdRange is TProdGrp) then begin
//                InitByProdGrp(aSpdRange as TProdGrp);
//  {
//              else if ( aSpdRange is TOrderPosition) then
//                InitByOrderPosition ( aSpdRange is TOrderPosition)
//  }
//              end
//              else
//                InitByNil;
//
//            end;
//        else
//          edThreadCnt.Enabled := False;
//          if (aSpdRange is TProdGrp) then
//            InitByProdGrp(aSpdRange as TProdGrp)
//          else begin
//  //Nue alt      InitByMiscInfo ( aMiscInfo );
//  //Nue neu alt             InitByNil;
//  //            edThreadCnt.Text := '1';
//          end;
//        end;
//      end;
//      if Assigned(aMiscInfo) then begin
//        InitByMiscInfo(aMiscInfo);
//      end;
//    except
//      on e: Exception do begin
//        SystemErrorMsg_('TMisc.UpdateComponent failed. ' + e.message);
//      end;
//    end;
//
//////MaGrp
////    try
//      if Assigned ( aSpdRange ) then begin
//        if Assigned ( aMachSettings ) then
//          mMachineSettings := aMachSettings^;
//        fFixSpdRange := aSpdRange.FixSpdRanges;
//        FillCob ( aSpdRange, aMachSettings );
//        if ( aSpdRange is TProdGrp ) then begin
//          SetItemIndex ( (aSpdRange as TProdGrp).GrpNr );
//          mSavedMaGrpNr := (aSpdRange as TProdGrp).GrpNr;
//          mmPanel2.Color := (aSpdRange as TProdGrp).Color;
//        end else begin
//          if fFixSpdRange then
//            cobGrpNr.ItemIndex := cobGrpNr.Items.Count
//          else
//            if NOT aSpdRange.IsInProduction then
//              cobGrpNr.ItemIndex := 0
//            else
//              cobGrpNr.ItemIndex := cobGrpNr.Items.Count-1;
//          mmPanel2.Color := clWhite;
//        end;
//      end;
//
////SpindleFields
//    if Assigned(aSpdRange) then begin
//      if aSpdRange.FixSpdRanges then begin
//        edFirst.Enabled := False;
//        edLast.Enabled := False;
//        udFirst.Enabled := False;
//        udLast.Enabled := False;
//      end;
//      mNumSpindles := aSpdRange.NumSpindles;
//      udFirst.Max := mNumSpindles;
//      udLast.Max := mNumSpindles;
//      udPilot.Max := mNumSpindles;
//      udFirst.Position := aSpdRange.SpindleFirst;
//      udLast.Position := aSpdRange.SpindleLast;
////      edFirst.Text := IntToStr(aSpdRange.SpindleFirst);  //Nue:17.7.03
////      edLast.Text := IntToStr(aSpdRange.SpindleLast);  //Nue:17.7.03
//      if aSpdRange is TProdGrp then
//        udPilot.Position := (aSpdRange as TProdGrp).PilotSpindles
//      else
//        udPilot.Position := TMMSettingsReader.Instance.Value[cPilotSpindles];
//
//    end;
//  finally
//      // LOK (7.11.2002): TSpindleFields wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
//      // ==> Hier wieder einblenden
//    visible := True;
//    mInitialized := True; //Nue/lok:27.11.02
//  end
end;
//------------------------------------------------------------------------------
function TSpindleFields.CheckUserValue: boolean;
begin
  Result := True;

  //SpindleFields
  if edFirst.Text = '' then begin
    edFirst.SetFocus;
    InfoMsg(Translate(cFieldFirstNotSet));
    Result := False;
  end;

  if edLast.Text = '' then begin
    edLast.SetFocus;
    InfoMsg(Translate(cFieldLastNotSet));
    Result := False;
  end;

  if edPilot.Text = '' then begin
    edPilot.SetFocus;
    InfoMsg(Translate(cFieldPilotNotSet));
    Result := False;
  end;

  //MaGrp
  if Trim(cobGrpNr.Text) = '' then begin
    cobGrpNr.SetFocus;
    InfoMsg(cFieldMaGrpNrNotSet);
    Result := False;
//?    Exit;
  end;

  //Misc
  if edYarnCnt.Text = '' then begin
    edYarnCnt.SetFocus;
    InfoMsg(Translate(cFieldYarnCntNotSet));
    Result := False;
  end;

  if edThreadCnt.Text = '' then begin
    edThreadCnt.SetFocus;
    InfoMsg(Translate(cFieldThreadCntNotSet));
    Result := False;
  end;

  if edSlip.Text = '' then begin
    edSlip.SetFocus;
    InfoMsg(Translate(cFieldSlipNotSet));
    Result := False;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.Check;
begin
  try
    //SpindleFields
    if edFirst.Text = '' then
      edFirst.Text := '1';
    if edLast.Text = '' then edLast.Text := '1';
    if edPilot.Text = '' then edPilot.Text := '1';
    if toInt(edFirst.Text) > mNumSpindles then
      edFirst.Text := IntToStr(mNumSpindles);
    if toInt(edLast.Text) > mNumSpindles then edLast.Text := IntToStr(mNumSpindles);
    if toInt(edFirst.Text) = 0 then
      edFirst.Text := '1';
    if toInt(edFirst.Text) > toInt(edLast.Text) then edLast.Text := edFirst.Text;
    if toInt(edPilot.Text) > (toInt(edLast.Text) - toInt(edFirst.Text) + 1) then
      edPilot.Text := IntToStr(TMMSettingsReader.Instance.Value[cPilotSpindles]);
//      edPilot.Text := InttoStr(toInt(edLast.Text) - toInt(edFirst.Text) + 1);

    //Misc
    if edYarnCnt.Text = '' then
      edYarnCnt.Text := '120';
    if edSlip.Text = '' then
      edSlip.Text := '1.000';
    if toFloat(edSlip.Text) > cMaxSlip then
      edSlip.Text := FloatToStr(cMaxSlip);
    if toFloat(edSlip.Text) < cMinSlip then
      edSlip.Text := FloatToStr(cMinSlip);
//    if toFloat(edYarnCnt.Text) < 2 then
//      edYarnCnt.Text := '2';

    //Check YarnCnt: F�r WSC gilt gem�ss K� vom 16.5.05: Bereich ist zwischen 1Nm - 500Nm m�glich
    // f�r ZE sind g�ltige Werte zwischen 0.4 und 3200 (Nicht YarnUnit behaftet!)
    if mMachineType in [mtAC338, mtAC5] then begin   //Nue:21.02.08  mtAC5
      if YarnCountConvert(fYarnCntUnit, yuNm, toFloat(edYarnCnt.Text))<1.0 then
        edYarnCnt.Text := FloatToStr(YarnCountConvert(yuNm, fYarnCntUnit, 1.0));
      if YarnCountConvert(fYarnCntUnit, yuNm, toFloat(edYarnCnt.Text))>500.0 then
        edYarnCnt.Text := FloatToStr(YarnCountConvert(yuNm, fYarnCntUnit, 500.0));
    end    
    else begin
      if toFloat(edYarnCnt.Text) < 0.4 then
        edYarnCnt.Text := '0.4';
      if toFloat(edYarnCnt.Text) > 3200 then
        edYarnCnt.Text := '3200';
    end;

    if StrToInt(edThreadCnt.Text) > 9 then
      edThreadCnt.Text := IntToStr(9);
    if StrToInt(edThreadCnt.Text) < 1 then
      edThreadCnt.Text := IntToStr(1);

  except
  end;
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetSpindleFirst: integer;
begin
  Check;
  Result := StrToInt(edFirst.Text);
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetSpindleLast: integer;
begin
  Check;
  Result := StrToInt(edLast.Text);
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetPilotSpindle: integer;
begin
  Check;
  Result := StrToInt(edPilot.Text);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.edChange(Sender: TObject);
var
  xItem: TUserChanges;
begin
  if (csDesigning in ComponentState) then exit; //Nue:22.4.03
  xItem := ucNone;
  if (Sender = edFirst) or (Sender = edLast) then begin
    if Assigned(Settings) then begin

//Nue:20.8.03 Nachfolgenden Bereich geklammert???
//      if edFirst.Text <> '' then
//        Settings.SpindleFirst := StrToInt(edFirst.Text)
//      else
//        Settings.SpindleFirst := 1;
//
//      if edLast.Text <> '' then
//        Settings.SpindleLast := StrToInt(edLast.Text)
//      else
//        Settings.SpindleLast := 1;

    end
    else begin
      SystemErrorMsg_('Settings not assigned!!');
    end;
    if mInitialized and TEdit(sender).showing then //Nue/lok:27.11.02
        //Has to check the condition because set the frame to visible, the system generates a message
        // CM_VISIBLECHANGED wich calls edChange!!
      xItem := ucSpindleRange;
  end;
  if Sender = edPilot then
    xItem := ucPilotSpindles;

  if Assigned(fSpdRangeList) then
    fSpdRangeList.ItemChanged(xItem);

  if Sender is TNumEdit then
    TNumEdit(Sender).Value := StrToInt(TCustomEdit(Sender).Text);

  //Misc
  if Sender = edYarnCnt then
    xItem := ucYarnCnt;

  if Sender = edThreadCnt then
    xItem := ucThreadCnt;

  if Sender = edSlip then
    xItem := ucSlip;

  if Assigned(fOnValueChanged) then
    fOnValueChanged(xItem, True);

end;
//------------------------------------------------------------------------------
function TSpindleFields.toInt(aVal: string): integer;
begin
  Result := StrToIntDef(aVal, 1);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Assigned(fSpdRangeList) then
    if (Operation = opRemove) and (AComponent = fSpdRangeList) then
      fSpdRangeList := nil;
  if Assigned(fSettings) then
    if (Operation = opRemove) and (AComponent = fSettings) then
      fSettings := nil;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.edExit(Sender: TObject);
begin
  Check;
//Unused since 4.8.2003 Nue
//  Check;
//  //Misc
//  if not Assigned(fSpdRangeList) then Exit;
//  if Sender = edYarnCnt then
//    fSpdRangeList.ItemFocusChanged(ucYarnCnt);
//
//  if Sender = edThreadCnt then
////    fSpdRangeList.ItemFocusChanged(ucYarnCnt);
//    fSpdRangeList.ItemFocusChanged(ucThreadCnt);
//
//  if Sender = edSlip then
//    fSpdRangeList.ItemChanged(ucSlip);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.edKeyPress(Sender: TObject; var Key: Char);
begin
  CheckIntChar(Key);
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------

function TSpindleFields.GetGrpNr: integer;
begin
  if Trim(cobGrpNr.Text) <> '' then
    if cobGrpNr.Visible then
      Result := StrToIntDef(cobGrpNr.Text,1)-1
    else
      Result := StrToIntDef(cobMemories.Text,1)-1+cLXSpdGroupLimit
  else
    Result := 0
end;
//------------------------------------------------------------------------------

//procedure TSpindleFields.FillCob(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr);
procedure TSpindleFields.FillCob(aSpdRange: TBaseSpindleRange; aMachSettings:
    PXMLSettingsCollection; aAdditionalMemories: Boolean);
var
  x: integer;
  xOldIndex: integer;
  i: Integer;
  xSpindleFrom,
  xSpindleTo: integer;
  xSensingHead: TSensingHead;
  xMaxMemories: Integer;
  xStartMemories: Integer;
  useOldGroupLimit: Boolean;
  ymVersion: String;
begin
  xStartMemories := 0;
  xMaxMemories := 0;
  try
    cobGrpNr.Clear;
    cobMemories.Clear;

    useOldGroupLimit := False;

    //MM-20: YM Software v7.53 is a specific version that has only 6 groups
    //       All other mtACaSpdRange.MachineType = mtAC3385 machines have 30 groups

    if (aSpdRange.YMType = ftInfPPCAC5) and Assigned(aSpdRange.MaConfigReader) then
    begin
      ymVersion := aSpdRange.MaConfigReader.MachValue[cXPYMSwVersionItem];
      if AnsiCompareText(ymVersion, cYMSwVersionWithOldGroupLimit) = 0 then
      begin
        useOldGroupLimit := True;
      end;
    end;

   if (aSpdRange.MachineType = mtAC338) or ((aSpdRange.YMType = ftInfPPCAC5) and useOldGroupLimit) then
      mMaxGrps := cOldWSCSpdGroupLimit
   else if aSpdRange.YMType = ftInfPPCAC5 then                    //Check auf AC05 Nue:8.2.07
      mMaxGrps := cWSCSpdGroupLimit
   else
      mMaxGrps := cZESpdGroupLimit;

//    case aSpdRange.FrontType of
//      ftInf68K, ftInfPPC: xMaxGrps := cWSCSpdGroupLimit;
//    else
//      xMaxGrps := cZESpdGroupLimit;
//    end;

    if (not aAdditionalMemories) then begin
        
    end
    else begin  //Memories
{ TODO 1 -oNue -cMemories : Was bei ZE, wo keine Memories vorhanden sind? }
      xStartMemories := cLXSpdGroupLimit;
      xMaxMemories := cLXSpdMemoryLimit;
    end;


    if aSpdRange.FixSpdRanges then begin
      if not Assigned(aMachSettings) then begin
//Nue:11.5.05        InfoMsg(cSettingsNotFromMachine);
        for x:=1 to mMaxGrps do
          cobGrpNr.Items.Add(IntToStr(x));
      end
      else begin
        for x:=0 to mMaxGrps-1 do
          if aMachSettings.SettingsArr[x].SpindleFirst <> 0 then
            cobGrpNr.items.Add(IntToStr(aMachSettings.SettingsArr[x].Group + 1));
        for x:=xStartMemories to (xMaxMemories-1+xStartMemories) do  //Memories
          cobMemories.items.Add(IntToStr((aMachSettings.SettingsArr[x].Group + 1)-xStartMemories));
        cobMemories.ItemIndex := ((aMachSettings.SettingsArr[xStartMemories].Group)-xStartMemories);
      end;
    end
    else begin
      for x:=1 to mMaxGrps do
        cobGrpNr.Items.Add(IntToStr(x));
    end;
    cobGrpNr.Items.Add(' '); // um leeres Feld zeigen zu koennen
    cobGrpNr.DropDownCount := cobGrpNr.Items.Count; //Nue:5.3.02


    //Ab XML: Fill Cob mit Sections
    if Assigned(fSpdRangeList.AssMachine) then begin
      xOldIndex := lbSections.ItemIndex;
      lbSections.Clear;
(*      for i := 0 to fSpdRangeList.AssMachine.Model.MaConfigReader.GroupCount - 1 do begin
        xSpindleFrom := fSpdRangeList.AssMachine.Model.MaConfigReader.GroupValueDef[i, cXPSpindleFromItem, 0];
        xSpindleTo := fSpdRangeList.AssMachine.Model.MaConfigReader.GroupValueDef[i, cXPSpindleToItem, 0];
        xSensingHead := GetSensingHeadValue(fSpdRangeList.AssMachine.Model.MaConfigReader.GroupValueDef[i, cXPSensingHeadItem, 0]);
        lbSections.Items.Add(Format('%d  (%d - %d)  %s', [i, xSpindleFrom, xSpindleTo, cGUISensingHeadNames[xSensingHead]]));
      end;// for i := 0 to mModel.MaConfigReader.GroupCount - 1 do begin*)
      for i := 0 to fSpdRangeList.AssMachine.MaConfigReader.GroupCount - 1 do begin
        xSpindleFrom := fSpdRangeList.AssMachine.MaConfigReader.GroupValueDef[i, cXPSpindleFromItem, 0];
        xSpindleTo   := fSpdRangeList.AssMachine.MaConfigReader.GroupValueDef[i, cXPSpindleToItem, 0];
        xSensingHead := GetSensingHeadValue(fSpdRangeList.AssMachine.MaConfigReader.GroupValueDef[i, cXPSensingHeadItem, GetEnumName(TypeInfo(TSensingHead),0)]);
//        lbSections.Items.Add(Format('%d  (%d - %d)  %s', [i+1, xSpindleFrom, xSpindleTo, cGUISensingHeadNames[xSensingHead]]));
        lbSections.Items.Add(Format('(%d - %d)  %s', [xSpindleFrom, xSpindleTo, cGUISensingHeadNames[xSensingHead]]));
      end;// for i := 0 to mModel.MaConfigReader.GroupCount - 1 do begin

      if lbSections.Items.Count > xOldIndex then
        lbSections.ItemIndex := xOldIndex;

      //Visible setzen
      if (lbSections.Items.Count > 1) and not(fSpdRangeList.AssMachine.FixSpdRanges) then begin
        lbSections.Visible := True;
        laSections.Visible := True;
      end
      else begin
        lbSections.Visible := False;
        laSections.Visible := False;
      end; //if lbSections.Items.Count > 1 then begin

    end; //if Assigned(fSpdRangeList.AssMachine.Model) then begin
  except
    on e: Exception do begin
      raise Exception.Create('TMaGrpNr.FillCob failed. Error : ' + e.Message);
    end;
  end;

//  try
//    cobGrpNr.Clear;
//    case aSpdRange.FrontType of
//      ftSWSInformatorSBC5: xMaxGrps := cWSCSpdGroupLimit;
//    else
//      xMaxGrps := cZESpdGroupLimit;
//    end;
//
//    if aSpdRange.FixSpdRanges then begin
//      if not Assigned(aMachSettings) then begin
//        InfoMsg(cSettingsNotFromMachine); //Nue:23.01.02
//        for x := 1 to xMaxGrps do //Nue:23.01.02
//          cobGrpNr.items.Add(IntToStr(x)); //Nue:23.01.02
////Nue:23.1.02            raise Exception.Create ( 'Machine Settings not assigned.' );
//      end
//      else
//        for x := 0 to cZESpdGroupLimit - 1 do
//          if aMachSettings[x].SpindleFirst <> 0 then
//            cobGrpNr.items.Add(IntToStr(aMachSettings[x].MachGrp + 1));
//    end
//    else begin
//      for x := 1 to xMaxGrps do begin
//        cobGrpNr.items.Add(IntToStr(x));
//      end; //For
//    end;
//    cobGrpNr.items.Add(' '); // um leeres Feld zeigen zu koennen
//    cobGrpNr.DropDownCount := cobGrpNr.items.Count; //Nue:5.3.02
//  except
//    on e: Exception do begin
//      raise Exception.Create('TMaGrpNr.FillCob failed. Error : ' + e.Message);
//    end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetItemIndex(aGrpNr: integer);
var
  x: integer;
begin
  for x := 0 to cobGrpNr.Items.Count - 2 do begin // -2 weil letzter Eintrag ist Leerstring
    if StrToInt(cobGrpNr.Items[x]) = aGrpNr+1 then begin
      cobGrpNr.ItemIndex := x;
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetAssignedMark(aMark: string);
begin
  laAssign.caption := aMark;
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetAssignedMark(): string;
begin
  Result := laAssign.Caption;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.cobGrpNrChange(Sender: TObject);
var
  xSettingsNav: TSettingsNav;
  x: integer;
  xMachGrp: integer;
  xSpdRangeFound: Boolean;
begin
  if Assigned(fSpdRangeList) then begin
    SpdRangeList.ItemChanged(ucMaGrpNr);

    if fFixSpdRange then begin
      if Assigned(mMachineSettings) then begin
        xMachGrp := StrToIntDef(cobGrpNr.Items[cobGrpNr.ItemIndex], 1) - 1; // Bei Fehler ergibt dies default-1 = 0

//alt 17.5.06        for x:=0 to cobGrpNr.Items.Count-1 do begin
        for x:=0 to xMachGrp do begin
//          if xMachGrp = mMachineSettings.SettingsArr[x].MachGrp then begin
          if xMachGrp = mMachineSettings.SettingsArr[x].Group then begin
            SpindleFirst       := mMachineSettings.SettingsArr[x].SpindleFirst;
            SpindleLast        := mMachineSettings.SettingsArr[x].SpindleLast;
            fFixSpdRangeInProd := (mMachineSettings.SettingsArr[x].GroupState in [gsInProd, gsLotChange]);
            if FixSpdRangeInProd then begin
              //Nur wenn inProd �berschreiben der Werte mit den Werten von der Maschine
//              YarnCnt            := mMachineSettings.SettingsArr[x].YarnCnt;  //Nue:22.11.05
              YarnCnt      := YarnCountConvert(yuNm, fYarnCntUnit, mMachineSettings.SettingsArr[x].YarnCnt);
              xSettingsNav := (fSpdRangeList.SettingsNavObject as TSettingsNav);
              if (xSettingsNav.ActCobChooserItem is TStyleItem) then
                Slip               := StrToFloat((xSettingsNav.ActCobChooserItem as TStyleItem).Slip); //Nue:23.1.06
              if (xSettingsNav.ActCobChooserItem is TProdGrpItem) then
                Slip               := StrToFloat((xSettingsNav.ActCobChooserItem as TProdGrpItem).Slip); //Nue:23.1.06
///              Slip               := mMachineSettings.SettingsArr[x].Slip/cSlipFactor;  //Nue:22.11.05
            end;

            Break;
          end; // if
        end; // for
      end; // if Assigned
    end
    else begin
//NUE1
//      SpindleFirst   := 1;
//      SpindleLast    := 1;
//      PilotSpindle   := 1;
      xSpdRangeFound := False;
      //Suchen des Spindleranges f�r die gew�hlte MaGrp
      for x:=0 to SpdRangeList.ProdGrpList.Count-1 do begin
{ DONE 1 -oNue -cXMLSettings : Ueberarbeiten MaGrp und Spindlen!!?? }
        if (SpdRangeList.ProdGrpList.Objects[x] is TProdGrp) then
          if TProdGrp(SpdRangeList.ProdGrpList.Objects[x]).GrpNr = cobGrpNr.ItemIndex then begin
            SpindleFirst   := TProdGrp(SpdRangeList.ProdGrpList.Objects[x]).SpindleFirst;
            SpindleLast    := TProdGrp(SpdRangeList.ProdGrpList.Objects[x]).SpindleLast;
            PilotSpindle   := TProdGrp(SpdRangeList.ProdGrpList.Objects[x]).PilotSpindles;
            xSpdRangeFound := True;
            Break
          end;
      end;

      //Noch nicht zugeordneter Spindelbereich
      if (not xSpdRangeFound) and (SpdRangeList.ActSpdRange is TSpindleRange) then begin
        SpindleFirst := TSpindleRange(SpdRangeList.ActSpdRange).SpindleFirst;
        SpindleLast  := TSpindleRange(SpdRangeList.ActSpdRange).SpindleLast;
        PilotSpindle := (TMMSettingsReader.Instance.Value[cPilotSpindles]);
      end; //IF

      //Auf Maschine ist nichts zugeordnet
      if (SpdRangeList.ActSpdRange is TSpindleRange) then begin
        if (not xSpdRangeFound) and (TSpindleRange(SpdRangeList.ActSpdRange).NoRunningProdGrp) then begin
          SpindleFirst := TSpindleRange(SpdRangeList.ActSpdRange).SpindleFirst;
          SpindleLast  := TSpindleRange(SpdRangeList.ActSpdRange).SpindleLast;
          PilotSpindle := (TMMSettingsReader.Instance.Value[cPilotSpindles]);
        end; //IF
      end;
    end; //IF
  end; //IF
    //ChangeEvent f�r AskAssign Form
  if Assigned(fOnChanged) then
    fOnChanged(Self);

//  if Assigned(fSpdRangeList) then begin
//    SpdRangeList.ItemChanged(ucMaGrpNr);
//
//    if fFixSpdRange then begin
//      if Trim(cobGrpNr.Items[cobGrpNr.ItemIndex]) <> '' then
//        xItem := StrToInt(cobGrpNr.Items[cobGrpNr.ItemIndex]) - 1
//      else
//        xItem := 0;
//
//      for x:=0 to cobGrpNr.Items.Count - 1 do begin
//        if xItem = mMachineSettings[x].MachGrp then begin
//          SpindleFirst := mMachineSettings[x].SpindleFirst;
//          SpindleLast := mMachineSettings[x].SpindleLast;
//
//          if (mMachineSettings[x].GroupState = gsInProd) or (mMachineSettings[x].GroupState = gsLotChange) then begin
//            fFixSpdRangeInProd := True;
//          end
//          else begin
//            fFixSpdRangeInProd := False;
//          end;
//          BREAK;
//        end; //if
//      end; //for
//    end
//    else begin
//      SpindleFirst := 1;
//      SpindleLast := 1;
//      PilotSpindle := 1;
//      xSpdRangeFound := False;
//        //Suchen des Spindleranges f�r die gew�hlte MaGrp
//      for x := 0 to SpdRangeList.ProdGrpList.Count - 1 do begin
//        if (SpdRangeList.ProdGrpList.Objects[x] is TProdGrp) then
//          if (SpdRangeList.ProdGrpList.Objects[x] as TProdGrp).GrpNr = cobGrpNr.ItemIndex + 1 then begin
//            SpindleFirst := (SpdRangeList.ProdGrpList.Objects[x] as TProdGrp).SpindleFirst;
//            SpindleLast := (SpdRangeList.ProdGrpList.Objects[x] as TProdGrp).SpindleLast;
//            PilotSpindle := (SpdRangeList.ProdGrpList.Objects[x] as TProdGrp).PilotSpindles;
//            xSpdRangeFound := True;
//          end;
//      end;
//        //Noch nicht zugeordneter Spindelbereich
//      if (not xSpdRangeFound) and (SpdRangeList.ActSpdRange is TSpindleRange) then begin
//        SpindleFirst := (SpdRangeList.ActSpdRange as TSpindleRange).SpindleFirst;
//        SpindleLast := (SpdRangeList.ActSpdRange as TSpindleRange).SpindleLast;
//        PilotSpindle := (TMMSettingsReader.Instance.Value[cPilotSpindles]);
//      end; //IF
//        //Auf Maschine ist nichts zugeordnet
//      if (SpdRangeList.ActSpdRange is TSpindleRange) then
//        if (not xSpdRangeFound) and ((SpdRangeList.ActSpdRange as TSpindleRange).NoRunningProdGrp) then begin
//          SpindleFirst := (SpdRangeList.ActSpdRange as TSpindleRange).SpindleFirst;
//          SpindleLast := (SpdRangeList.ActSpdRange as TSpindleRange).SpindleLast;
//          PilotSpindle := (TMMSettingsReader.Instance.Value[cPilotSpindles]);
//        end; //IF
//    end; //IF
//  end; //IF
//    //ChangeEvent f�r AskAssign Form
//  if Assigned(fOnChanged) then
//    fOnChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TSpindleFields.FrameClick(Sender: TObject);
begin

end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
//Misc
//------------------------------------------------------------------------------
procedure TSpindleFields.SetYarnCnt(aYarnCnt: Double);
begin
  edYarnCnt.Text := Format('%4.2f', [aYarnCnt]);  //Nue 21.12.06 Format von 4.1 auf 4.2
//  fYarnCnt := aYarnCnt;
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetYarnCntUnit(aYarnCntUnit: TYarnUnit);
begin
  fYarnCntUnit := aYarnCntUnit;
  lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit];
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetSlip(aSlip: Single);
begin
  edSlip.Text := Format('%1.3f', [aSlip]);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetThreadCnt(aCnt: integer);
begin
  edThreadCnt.Text := IntToStr(aCnt);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.SetFieldsInLine(aInLine: boolean);
begin
  if aInLine <> fFieldsInLine then begin
    fFieldsInLine := aInLine;
    if fFieldsInLine then begin
      self.Width := 320;
      self.Height := 73;
      edYarnCnt.Left := 80;
      edThreadCnt.Left := 80;
      edThreadCnt.Top := 40;
      edSlip.Left := 270;
      edSlip.Top := edYarnCnt.Top;
    end
    else begin
      self.Width := 153;
      self.Height := 94;
      edThreadCnt.Top := 32;
      edSlip.Left := 88;
      edSlip.Top := 64;
      edThreadCnt.Left := 88;
      edYarnCnt.Left := 88;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetYarnCnt: Double;
begin
  Check;
//    Result := YarnCountConvert(fYarnCntUnit, yuNm, StrToFloat(edYarnCnt.Text));
  Result := StrToFloat(edYarnCnt.Text);
//Result := fYarnCnt;  
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetSlip: Single;
begin
  Check;
  Result := StrToFloat(edSlip.Text);
end;
//------------------------------------------------------------------------------
function TSpindleFields.GetThreadCnt: integer;
begin
  Result := StrToInt(edThreadCnt.text);
end;
//------------------------------------------------------------------------------
function TSpindleFields.toFloat(aStr: string): Single;
begin
  try
    Result := StrToFloat(aStr);
  except
    Result := 1.0;
  end;
end;
//------------------------------------------------------------------------------

procedure TSpindleFields.edYarnCntKeyPress(Sender: TObject; var Key: Char);
begin
  CheckFloatChar(Key);
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------

procedure TSpindleFields.edThreadCntKeyPress(Sender: TObject; var Key: Char);
begin
  CheckIntChar(Key);
  inherited KeyPress(key);
end;
//------------------------------------------------------------------------------

procedure TSpindleFields.edSlipKeyPress(Sender: TObject; var Key: Char);
begin
  CheckFloatChar(Key);
  inherited KeyPress(Key);
end;

//------------------------------------------------------------------------------
procedure TSpindleFields.EnableNetDependentFields;
begin
//  if (AssignedMark = 'A') and (fSpdRangeList.AssMachine.NetTyp in [ntWSC,ntLX]) then
//  //Wenn Gruppe Assigned ist und  Netztyp in [ntWSC,ntLX], dann darf beim �berstarten der YarnCount nicht ver�ndert werden. 6.10.05 Nue
    edYarnCnt.Enabled := not fFixSpdRangeInProd;
end;

//------------------------------------------------------------------------------

procedure TSpindleFields.RestoreMaGrpNr;
begin
  //Speichert die urspr�nglich �bergebene MaGrpNr zur�ck, unabh�ngig von den Aenderungen in cobGrpNr
  //Bei Show des Owners (AskAssignForm) aufrufen.
  SetItemIndex(mSavedMaGrpNr);
end;
//------------------------------------------------------------------------------
procedure TSpindleFields.lbSectionsClick(Sender: TObject);
var
  xSpindleFrom,
  xSpindleTo: integer;
begin
  xSpindleFrom := fSpdRangeList.AssMachine.MaConfigReader.GroupValueDef[lbSections.ItemIndex, cXPSpindleFromItem, 0];
  xSpindleTo := fSpdRangeList.AssMachine.MaConfigReader.GroupValueDef[lbSections.ItemIndex, cXPSpindleToItem, 0];
(*  xSpindleFrom := fSpdRangeList.AssMachine.Model.MaConfigReader.GroupValueDef[lbSections.ItemIndex, cXPSpindleFromItem, 0];
  xSpindleTo := fSpdRangeList.AssMachine.Model.MaConfigReader.GroupValueDef[lbSections.ItemIndex, cXPSpindleToItem, 0];*)

  udFirst.Min    := xSpindleFrom;
  udFirst.Max    := xSpindleTo;
  edFirst.Text   := IntToStr(xSpindleFrom);

  udLast.Min     := xSpindleFrom;
  udLast.Max     := xSpindleTo;
  edLast.Text   := IntToStr(xSpindleTo);
end;

procedure TSpindleFields.mmPanel1Exit(Sender: TObject);
begin
 Check; //Nue:12.5.5
end;

end.

