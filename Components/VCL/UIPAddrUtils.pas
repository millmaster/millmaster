unit UIPAddrUtils;

interface

type
    TTriplet = 1..4;

procedure IP_ADDRESS_Validate( var aString : string );
function IP_ADDRESS_TripletAsNumber( AString: string; WhichTriplet : TTriplet ) : byte;

implementation

uses
  SysUtils;

const
    STR_IP_ADDRESS_DELIMITER  = '.';

type
    STR_CharSet = set of char;

function STR_PieceOfString (
                                S               : string;
                                WhichPiece      : integer;
                                Delimiter       : char
                             ) : string;
var
    P, PieceCounter : integer;
    Piece : string;
begin
    result := '';
    PieceCounter := 0;
    repeat
        if length(S) = 0 then break;
        P := pos( Delimiter, S );
        if p = 0 then P := Length( S ) + 1;
        Piece := Copy (S, 1, P -1 );
        Delete (S, 1, pred (P + Length( Delimiter ) ) );
        inc ( PieceCounter );
        if PieceCounter = WhichPiece then begin
           result := Piece;
           break;
        end;
    until false;
end;      //  STR_PieceOfString

function STR_CountStringDelimiters (
                                     S               : string;
                                     Delimiter       : char
                                   ) : Integer;
var
   Counter : Integer;
begin
     Result := 0;
     for Counter := 1 to Length (S) do
         if S[Counter] = Delimiter then
            Inc(Result);
end;

function STR_DiscardChars( const AString : string; CharsToDiscard : STR_CharSet ) : string;
var
    AResultStr  : string;
    c           : char;
    x,
    StrLen      :  integer;
begin
    AResultStr := '';
    StrLen := Length( AString );
    for x := 1 to StrLen do begin
        c := AString[x];
        if not( c in CharsToDiscard ) then
            AResultStr := AResultStr + c;
    end;        //  for
    result := AResultStr;
end;
  
function ConvertNewFormatToOldFormat( AIpString : string ) : string;
var
    Piece : string;
    x, NumberOfDots : integer;
begin
    assert( Length( AIpString ) <= 15, Format( 'Illegal length for IPString![%s(%d)]', [AIpString, Length(AIpString)] ) );

    NumberOfDots := STR_CountStringDelimiters (AIpString,STR_IP_ADDRESS_DELIMITER);
    assert ( NumberOfDots = 3 , Format( 'Must include exactly 3 dots! [%s]', [AIpString] ) );

    for x := 1 to 4 do begin

        Piece := STR_PieceOfString ( AIpString, x, STR_IP_ADDRESS_DELIMITER );

        if (length(Piece)<1) or (length(Piece)>3) then begin
                raise Exception.Create(Format( 'IPAddress must include 4 pieces of length 1..3. IPAddressString[%s]',
                        [AIpString] )
            );
        end;

        if ( Length( Piece ) > 1 ) and ( Piece[1] = '0' ) then begin
            raise Exception.Create(Format( 'Leading zero not allowed. IPAddressString[%s]', [AIpString] )
            );
        end;


        while length( piece ) < 3 do
            Piece := '0' + Piece;
        result :=  result + Piece;

    end;        //  for

    result := STR_DiscardChars(result , ['.']);

end;    //  ConvertNewFormatToOldFormat

function STR_IsDecNumber(aString : string) : Boolean;
var
   aDigitPos : Integer;
begin
     Result := True;
     for aDigitPos := 1 to Length(aString) do
     begin
          if Not (aString[aDigitPos] in ['0'..'9']) then
          begin
               Result := False;
               Break;
          end;
     end;
end;

procedure CheckFormatOfString( AString, ADisplayString : string );
var
    x       : byte;
    number  : integer;
    piece : string;
const
     ERR_ILLEGAL_IP_ADDRESS_SYMBOL = 'IP Address contains illegal symbols';
begin
    assert( Length( AString ) = 12, Format( 'Illegal length for IPString![%s (Length:%d) ]', [ADisplayString, Length(AString)] ) );

    for x := 1 to 4 do begin
        piece := copy ( AString, succ(3 * pred(x)) , 3) ;
        if not STR_IsDecNumber( piece ) then
            raise Exception.Create(Format( '%s. IPAddressString[%s]',
                [ERR_ILLEGAL_IP_ADDRESS_SYMBOL, ADisplayString] )
            );

        number := StrToInt(piece);
        if (number < 0) or (number > 255) then
            raise Exception.Create(Format( 'Triplets must be of range 0..255  IPAddressString[%s]',
                [ADisplayString] )
            );
    end;        //  for

end;        //  CheckFormatOfString

procedure IP_ADDRESS_Validate( var aString : string );
var
    AOldFormatString : string;
begin
    AOldFormatString := ConvertNewFormatToOldFormat( AString );
    CheckFormatOfString( AOldFormatString, AString );
end;        //  IP_ADDRESS_Validate

function IP_ADDRESS_TripletAsNumber( AString: string; WhichTriplet : TTriplet ) : byte;
var
    AOldFormatString : string;
begin
    Result := 0;
    AOldFormatString := ConvertNewFormatToOldFormat( AString );
    CheckFormatOfString( AOldFormatString, AString );
     case WhichTriplet of
        1: result := StrToInt( copy ( AOldFormatString, 1, 3) );
        2: result := StrToInt( copy ( AOldFormatString, 4, 3) );
        3: result := StrToInt( copy ( AOldFormatString, 7, 3) );
        4: result := StrToInt( copy ( AOldFormatString, 10, 3) );
     end;        //  case
end;        //  TripletAsNumber

end.
