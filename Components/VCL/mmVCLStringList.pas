(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: DataForm.pas
| Projectpart...: Parameter-Dialog fuer die Spulerei und Einzelspindelbericht
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 15.10.2003 1.00 Wss | TStringList ersetzt durch TmmStringList
|=============================================================================*)
unit mmVCLStringList;

interface

uses
  Classes, SysUtils, mmStringList;

type
  TmmVCLStringList = class(TComponent)
  private
    mStringList: TmmStringList;
    function GetStrings: TmmStringList;
    procedure SetStrings(const Value: TmmStringList);
    function GetCommaText: String;
    function GetText: String;
    procedure SetCommaText(const Value: String);
    procedure SetText(const Value: String);
  protected
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property Text: String read GetText write SetText;
    property CommaText: String read GetCommaText write SetCommaText;
  published
    property Strings: TmmStringList read GetStrings write SetStrings;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
// TmmVCLStringList
//------------------------------------------------------------------------------
constructor TmmVCLStringList.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mStringList := TmmStringList.Create;
end;
//------------------------------------------------------------------------------
destructor TmmVCLStringList.Destroy;
begin
  FreeAndNil(mStringList);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TmmVCLStringList.GetCommaText: String;
begin
  Result := mStringList.CommaText;
end;
//------------------------------------------------------------------------------
function TmmVCLStringList.GetStrings: TmmStringList;
begin
  Result := mStringList;
end;
//------------------------------------------------------------------------------
function TmmVCLStringList.GetText: String;
begin
  Result := mStringList.Text;
end;
//------------------------------------------------------------------------------
procedure TmmVCLStringList.SetCommaText(const Value: String);
begin
  mStringList.CommaText := Value;
end;
//------------------------------------------------------------------------------
procedure TmmVCLStringList.SetStrings(const Value: TmmStringList);
begin
  mStringList.Assign(Value);
end;
//------------------------------------------------------------------------------
procedure TmmVCLStringList.SetText(const Value: String);
begin
  mStringList.Text := Value;
end;
//------------------------------------------------------------------------------
end.
