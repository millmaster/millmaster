(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: QRPrintGrid.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
  TQRPrintDBGrid und TQRPrintStringGrid erstellen auf einem QuickReport fuer
  jede Spalte ein Datenlabel. Bei TQRPrintDBGrid laeuft die Datenaktuallisierung
  automatisch ab, waerend beim TQRPrintStringGrid die Datenaufbereitung ueber die
  Events BeforePrint und OnNeedData des QuickReports erfolgen.
|-------------------------------------------------------------------------------
| 13.03.2001 1.00 Wss | File created
| 03.07.2001 1.00 Wss | CalcZoomedWidth added
| 11.10.2002 1.00 Wss | - Korrektur f�r VertPos in AddLabelToBand
                        - In TBaseQRPrintGrid Property OnGetBandColor hinzugef�gt
                          und den Event BeforPrint f�r's Detailband abgefangen
                          -> LocDetailBandBeforePrint
                        - CalcTopPosition Funktion implementiert
| 31.01.2003 1.00 Wss | mRedirectDone Variable bei StringGrid hinzugef�t
                        Wird in PrepareReport verwendet.
| 08.07.2003 1.00 Wss | Property HeaderMultiLine hinzugef�gt
| 30.09.2003 1.00 Wss | Erweiterungen, damit per Events eigene Komponenten statt nur
                        der TQRText und TQRDBText verwendet werden k�nnen. Bei 
                        AddColumns wird f�r's DetailBand der Event OnAddQRComponent
                        aufgerufen, damit von ausserhalb eine eigene QR-Komponente
                        verwendet werden kann. So ist z.B. das F�rben von Spalten,
                        wie sie bei der Artikelliste in MMStyle ersichtlich ist, 
                        m�glich, jedoch muss das setzen und �berwachen der Datenwerte
                        komplett ausserhalb der Komponente erfolgen. 
|=============================================================================*)
unit QRPrintGrid;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBGrids, Grids, QuickRpt, QRCtrls, StdCtrls, mmList, mmQRLabel, mmQRDBText;

type
  TQRBandType = (rbTitle, rbPageHeader, rbDetail, rbPageFooter, rbSummary,
                 rbGroupHeader, rbGroupFooter, rbSubDetail, rbColumnHeader,
                 rbOverlay, rbChild);

  TBaseQRBand = class (TPersistent)
  private
    fBand: TQRCustomBand;
    fMargin: Integer;
    fOnChange: TNotifyEvent;
    fVertPos: TTextLayout;
    mBandType: TQRBandType;
    procedure SetBand(Value: TQRCustomBand); virtual;
    procedure SetMargin(Value: Integer);
    procedure SetVertPos(Value: TTextLayout);
  protected
    procedure Changed; dynamic;
  public
    constructor Create(aBandType: TQRBandType); virtual;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  published
    property Band: TQRCustomBand read fBand write SetBand;
    property Margin: Integer read fMargin write SetMargin;
    property VertPos: TTextLayout read fVertPos write SetVertPos;
  end;
  
  TDetailBand = class (TBaseQRBand)
  end;
  
  THeaderBand = class (TBaseQRBand)
  end;
  
  TAddQRComponentEvent = procedure (Sender: TObject; aColIndex: Integer; var aWidth: Integer; var aQRComponent: TQRPrintable) of object;
  TGetBandColorEvent = procedure (Sender: TQRCustomBand; aIndex: Integer; var BandColor: TColor) of object;
  TNeedComponentDataEvent = procedure (Sender: TObject; aComponent: TQRPrintable; aText: String) of object;
  TAfterScrollEvent = procedure (DataSet: TDataSet; aField: TField; aQRPrintable: TQRPrintable) of object;
  TBaseQRPrintGrid = class (TComponent)
  private
    fColDistance: Integer;
    fDataGrid: TCustomGrid;
    fDataMultiLine: Boolean;
    fDetailBand: TDetailBand;
    fFitToWidth: Boolean;
    fHeaderBand: THeaderBand;
    fHeaderMultiLine: Boolean;
    fLeftMargin: Integer;
    fOnAddQRComponent: TAddQRComponentEvent;
    fOnGetBandColor: TGetBandColorEvent;
    fQuickReport: TQuickRep;
    mOnBandBeforePrintEvent: TQRBandBeforePrintEvent;
    function SizeToZoom(aInteger: Integer): Integer;
    function ZoomToSize(aInteger: Integer): Integer;
  protected
    mDataList: TmmList;
    mFactor: Single;
    mHeaderList: TmmList;
    mRow: Integer;
    procedure AddColumns; virtual; abstract;
    function AddQRComponents(aBand: TBaseQRBand; aColIndex, aPos: Integer; aCaption: String; aWidth: Integer; aAlign: TAlignment; aMultiLine: Boolean): 
            TQRPrintable;
    procedure CalcTopPosition(aBand: TBaseQRBand; aQRComp: TQRPrintable);
    function CalcZoomedWidth(aWidth: Integer): Integer;
    procedure Clear;
    procedure DoRedirection(aRedirect: Boolean); virtual;
    function GetTotalColCount: Integer; virtual; abstract;
    function GetTotalColWidth: Integer; virtual; abstract;
    procedure LocDetailBandBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function PrepareReport: Boolean; virtual;
    function Preview: Boolean; virtual;
    function Print: Boolean; virtual;
  published
    property ColDistance: Integer read fColDistance write fColDistance;
    property DataGrid: TCustomGrid read fDataGrid write fDataGrid;
    property DataMultiLine: Boolean read fDataMultiLine write fDataMultiLine;
    property DetailBand: TDetailBand read fDetailBand write fDetailBand;
    property FitToWidth: Boolean read fFitToWidth write fFitToWidth;
    property HeaderBand: THeaderBand read fHeaderBand write fHeaderBand;
    property HeaderMultiLine: Boolean read fHeaderMultiLine write fHeaderMultiLine;
    property LeftMargin: Integer read fLeftMargin write fLeftMargin default 2;
    property OnAddQRComponent: TAddQRComponentEvent read fOnAddQRComponent write fOnAddQRComponent;
    property OnGetBandColor: TGetBandColorEvent read fOnGetBandColor write fOnGetBandColor;
    property QuickReport: TQuickRep read fQuickReport write fQuickReport;
  end;
  
  TQRPrintDBGrid = class (TBaseQRPrintGrid)
  private
    fOnAfterScroll: TAfterScrollEvent;
    mAfterScrollProc: TDataSetNotifyEvent;
    function GetGrid: TDBGrid;
  protected
    procedure AddColumns; override;
    procedure DoRedirection(aRedirect: Boolean); override;
    function GetTotalColCount: Integer; override;
    function GetTotalColWidth: Integer; override;
    property Grid: TDBGrid read GetGrid;
  public
    constructor Create(aOwner: TComponent); override;
    procedure LocAfterScroll(DataSet: TDataSet);
  published
    property OnAfterScroll: TAfterScrollEvent read fOnAfterScroll write fOnAfterScroll;
  end;
  
  TQRPrintStringGrid = class (TBaseQRPrintGrid)
  private
    fOnNeedComponentData: TNeedComponentDataEvent;
    mBeforePrintProc: TQRReportBeforePrintEvent;
    mOnNeedDataProc: TQROnNeedDataEvent;
    function GetGrid: TStringGrid;
  protected
    procedure AddColumns; override;
    procedure DoRedirection(aRedirect: Boolean); override;
    function GetTotalColCount: Integer; override;
    function GetTotalColWidth: Integer; override;
    procedure LocBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
    procedure LocOnNeedData(Sender : TObject; var MoreData : Boolean);
    property Grid: TStringGrid read GetGrid;
  public
    constructor Create(aOwner: TComponent); override;
  published
    property OnNeedComponentData: TNeedComponentDataEvent read fOnNeedComponentData write fOnNeedComponentData;
  end;
  

implementation
uses
  mmMBCS, mmCS;

//:---------------------------------------------------------------------------
//:--- Class: TBaseQRBand
//:---------------------------------------------------------------------------
constructor TBaseQRBand.Create(aBandType: TQRBandType);
begin
  mBandType := aBandType;
  inherited Create;
  fBand   := Nil;
  fMargin := 2;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRBand.Changed;
begin
  if Assigned(fOnChange) then fOnChange(Self);
end;

//:---------------------------------------------------------------------------
procedure TBaseQRBand.SetBand(Value: TQRCustomBand);
begin
  if fBand <> Value then begin
    if Assigned(Value) then begin
      fBand := Value;
      Changed;
    end else
      fBand := Nil;
  end;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRBand.SetMargin(Value: Integer);
begin
  if fMargin <> Value then
  begin
    fMargin := Value;
    Changed;
  end;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRBand.SetVertPos(Value: TTextLayout);
begin
  if fVertPos <> Value then
  begin
    fVertPos := Value;
    Changed;
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TBaseQRPrintGrid
//:---------------------------------------------------------------------------
constructor TBaseQRPrintGrid.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fDetailBand := TDetailBand.Create(rbDetail);
  fHeaderBand := THeaderBand.Create(rbColumnHeader);
  
  fColDistance     := 2;
  fDataGrid        := Nil;
  fDataMultiLine   := False;
  fFitToWidth      := False;
  fHeaderMultiLine := False;
  fLeftMargin      := 2;
  
  mDataList     := TmmList.Create;
  mHeaderList   := TmmList.Create;
  mRow          := 0;
end;

//:---------------------------------------------------------------------------
destructor TBaseQRPrintGrid.Destroy;
begin
  FreeAndNil(mDataList);
  FreeAndNil(mHeaderList);
  
  FreeAndNil(fDetailBand);
  FreeAndNil(fHeaderBand);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.AddQRComponents(aBand: TBaseQRBand; aColIndex, aPos: Integer; aCaption: String; aWidth: Integer; aAlign: TAlignment; aMultiLine: 
        Boolean): TQRPrintable;
begin
  Result := Nil;
  if (aBand = fDetailBand) and Assigned(fOnAddQRComponent) then
    fOnAddQRComponent(Self, aColIndex, aWidth, Result);
  
  // wenn von "aussen" keine eigene Komponente erstellt wurde, dann ein QRLabel erstellen
  if Result = Nil then begin
    Result  := TmmQRLabel.Create(Nil);
    with TmmQRLabel(Result) do begin
      Alignment   := aAlign;
      AutoSize    := False;
      AutoStretch := aMultiLine;
      Caption     := aCaption;
      Transparent := True;
    end;
  end;
  
  // Allgemeine Properties setzen
  with Result do begin
    Parent := aBand.Band;
    Left   := aPos;
    Width  := aWidth;
  end;
  
  // Vertikal ausrichten innerhalb vom Band
  CalcTopPosition(aBand, Result);
end;

//:---------------------------------------------------------------------------
procedure TBaseQRPrintGrid.CalcTopPosition(aBand: TBaseQRBand; aQRComp: TQRPrintable);
var
  xTop: Integer;
  xHeight: Integer;
begin
  // von der Originalh�he ausgehen
  aQRComp.Height := 17;
  // wir brauchen erst mal den 100% Wert f�r die Bandh�he
  xHeight := ZoomToSize(aBand.Band.Height);
  // vertikale Position ausrechnen
  case aBand.VertPos of
    tlBottom: xTop := xHeight - aQRComp.Height - aBand.Margin;
    tlCenter: xTop := (xHeight - aQRComp.Height) div 2;
  else // tlTop
    xTop := aBand.Margin;
  end;
  // nun muss die Position wieder gem�ss Zoomfaktor umgerechnet werden
  xTop := SizeToZoom(xTop);
  // fill in properties
  aQRComp.Top := xTop;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.CalcZoomedWidth(aWidth: Integer): Integer;
begin
  if Assigned(fQuickReport) then
    Result := Round(aWidth * fQuickReport.Zoom / 100.0)
  else
    Result := aWidth;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRPrintGrid.Clear;
  
  procedure ClearComponentList(aList: TmmList);
  var
    i: Integer;
  begin
    with aList do begin
      // alle erzeugten Komponenten freigeben
  //    i := 0;
      while Count > 0 do begin
        TQRPrintable(Items[0]).Free;
        Delete(0);
  //      inc(i);
      end;
  //     for i:=0 to Count-1 do begin
  //       TQRPrintable(Items[i]).Free;
  //     end;
      // Liste l�schen
  //    Clear;
    end;
  end;
  
begin
  ClearComponentList(mHeaderList);
  ClearComponentList(mDataList);
end;

//:---------------------------------------------------------------------------
procedure TBaseQRPrintGrid.DoRedirection(aRedirect: Boolean);
begin
  if aRedirect then begin
    // save property values from QuickReport
    mOnBandBeforePrintEvent     := DetailBand.Band.BeforePrint;
    // LINK IN this methodes to receives events to fill in data
    DetailBand.Band.BeforePrint := LocDetailBandBeforePrint;
  end else begin
    DetailBand.Band.BeforePrint := mOnBandBeforePrintEvent;
    mOnBandBeforePrintEvent     := Nil;
  end;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRPrintGrid.LocDetailBandBeforePrint(Sender: TQRCustomBand; var PrintBand: Boolean);
var
  xColor: TColor;
  xIndex: Integer;
begin
  xColor := Sender.Color;
  if Assigned(fOnGetBandColor) then begin
    // BeforePrint wird NACH NeedData aufgerufen. Desshalb ist mRow um 1 zu hoch
    // im Zusammenhang mit TStringGrid!!
    xIndex := mRow-1;
    if DataGrid is TDBGrid then begin
      if Assigned(fQuickReport) then
        if Assigned(fQuickReport.DataSet) then
          xIndex := fQuickReport.DataSet.RecNo
    end;
    fOnGetBandColor(Sender, xIndex, xColor);
  end;
  Sender.Color := xColor;
end;

//:---------------------------------------------------------------------------
procedure TBaseQRPrintGrid.Notification(AComponent: TComponent; Operation: TOperation);
var
  xIndex: Integer;
begin
  inherited Notification(AComponent, Operation);
  if Operation = opRemove then begin
    if Assigned(fHeaderBand) and (aComponent = fHeaderBand.Band) then
      fHeaderBand.Band := Nil
    else if Assigned(fDetailBand) and (aComponent = fDetailBand.Band) then
      fDetailBand.Band := Nil
    else if aComponent = fQuickReport then
      fQuickReport := Nil
    else if aComponent = fDataGrid then
      fDataGrid := Nil;
  end;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.PrepareReport: Boolean;
var
  xWidth: Integer;
  xTotalColWidth: Integer;
begin
  Result := False;
  // In design time or run mode (but after csLoading is reset)
  // calls to PrepareReport are valid
  if (ComponentState * [csLoading]) = [] then begin
    Clear;
    Result := Assigned(fHeaderBand.Band) and Assigned(fDetailBand.Band) and
              Assigned(fDataGrid) and Assigned(fQuickReport);
    if Result then begin
      // calc the left pixel space we have for all columns
      xWidth := fHeaderBand.Band.Width - fLeftMargin - ((GetTotalColCount-1) * ColDistance);
      // get the individual needed space for all columns in grid
      xTotalColWidth := GetTotalColWidth;
      // if it doesn't have enough space for all columns and FitToWidth is set
      // calc a factor to reduce size of each column
      if fFitToWidth and (xTotalColWidth > xWidth) then
        mFactor := xWidth / xTotalColWidth
      else
        mFactor := 1.0;
      // now call methode to add the header/data columns
      AddColumns;
    end; // if Result
  end; // if (ComponentState
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.Preview: Boolean;
begin
  Result := True;
  DoRedirection(True);
  try
    if PrepareReport then
      QuickReport.Preview
    else
      Result := False;
  finally
    DoRedirection(False);
    Clear;
  end;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.Print: Boolean;
begin
  Result := True;
  DoRedirection(True);
  try
    if PrepareReport then
      QuickReport.Print
    else
      Result := False;
  finally
    DoRedirection(False);
    Clear;
  end;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.SizeToZoom(aInteger: Integer): Integer;
begin
  if Assigned(fQuickReport) then
    Result := Round(aInteger * fQuickReport.Zoom / 100.0)
  else
    Result := aInteger;
end;

//:---------------------------------------------------------------------------
function TBaseQRPrintGrid.ZoomToSize(aInteger: Integer): Integer;
begin
  if Assigned(fQuickReport) then
    Result := Trunc(aInteger * (100.0 / fQuickReport.Zoom))
  else
    Result := aInteger;
end;

//:---------------------------------------------------------------------------
//:--- Class: TQRPrintDBGrid
//:---------------------------------------------------------------------------
constructor TQRPrintDBGrid.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  //:...................................................................
  mAfterScrollProc := Nil;
end;

//:---------------------------------------------------------------------------
procedure TQRPrintDBGrid.AddColumns;
var
  i: Integer;
  xPos: Integer;
  xWidth: Integer;
  xStr: string;
  xQRPrintable: TQRPrintable;
begin
  xPos := LeftMargin;
  for i:=0 to Grid.Columns.Count-1 do begin
    xWidth := Trunc(CalcZoomedWidth(Grid.Columns[i].Width) * mFactor);
    // nur weitermachen, wenn noch gen�gend Platz in der Breite vorhanden ist
    if (xPos + xWidth) <= fHeaderBand.Band.Width then begin
      xStr   := Grid.Columns[i].Title.Caption;
      // Titellabel erstellen und in Verwaltungsliste hinzuf�gen
      mHeaderList.Add(AddQRComponents(fHeaderBand, i, xPos, xStr, xWidth, Grid.Columns[i].Alignment, fHeaderMultiLine));
  
      // Datenkomponente behandeln...
      xQRPrintable := Nil;
      if Assigned(fOnAddQRComponent) then
        fOnAddQRComponent(Self, i, xWidth, xQRPrintable);
  
      // wenn von "aussen" keine eigene Komponente erstellt wurde, dann ein QRDBText erstellen
      if xQRPrintable = Nil then begin
        xQRPrintable := TmmQRDBText.Create(Nil);
        // und die ensprechenden Properties abf�llen
        with TmmQRDBText(xQRPrintable) do begin
          Alignment   := Grid.Columns[i].Alignment;
          AutoSize    := False;
          AutoStretch := DataMultiLine;
          Caption     := xStr;
          DataSet     := Grid.DataSource.DataSet;
          DataField   := Grid.Columns[i].FieldName;
        end;
      end;
      // hier noch die allgemeinen Properties setzen
      with xQRPrintable do begin
        Left        := xPos;
        Parent      := fDetailBand.Band;
        Width       := xWidth;
      end;
      // Vertikal ausrichten innerhalb vom Band
      CalcTopPosition(fDetailBand, xQRPrintable);
      // diese Komponente in die Verwaltungsliste aufnehmen
      mDataList.Add(xQRPrintable);
      inc(xPos, xWidth + ColDistance);
    end else
      Break;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRPrintDBGrid.DoRedirection(aRedirect: Boolean);
begin
  inherited DoRedirection(aRedirect);
  //:...................................................................
  if aRedirect then begin
    // make sure QuickReports use the same DataSet as the linked DBGrid
    QuickReport.DataSet := Grid.DataSource.DataSet;
    // nun noch den AfterScroll Event umbiegen
    mAfterScrollProc := QuickReport.DataSet.AfterScroll;
    QuickReport.DataSet.AfterScroll := LocAfterScroll;
  end else begin
    // nun noch den AfterScroll Event wieder zur�ckbiegen
    QuickReport.DataSet.AfterScroll := mAfterScrollProc;
    QuickReport.DataSet := Nil;
  end;
end;

//:---------------------------------------------------------------------------
function TQRPrintDBGrid.GetGrid: TDBGrid;
begin
  Result := DataGrid as TDBGrid;
end;

//:---------------------------------------------------------------------------
function TQRPrintDBGrid.GetTotalColCount: Integer;
begin
  Result := Grid.Columns.Count;
end;

//:---------------------------------------------------------------------------
function TQRPrintDBGrid.GetTotalColWidth: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i:=0 to Grid.Columns.Count-1 do
    Result := Result + Grid.Columns[i].Width;
  
  Result := CalcZoomedWidth(Result);
end;

//:---------------------------------------------------------------------------
procedure TQRPrintDBGrid.LocAfterScroll(DataSet: TDataSet);
var
  xQRPrintable: TQRPrintable;
  i: Integer;
begin
  if Assigned(mAfterScrollProc) then
    mAfterScrollProc(DataSet);
  
  if Assigned(fOnAfterScroll) then
    for i:=0 to mDataList.Count-1 do begin
      xQRPrintable := TQRPrintable(mDataList.Items[i]);
      if xQRPrintable is TQRDBText then
        fOnAfterScroll(DataSet, TQRDBText(xQRPrintable).DataSet.FieldByName(TQRDBText(xQRPrintable).DataField), xQRPrintable)
      else
        fOnAfterScroll(DataSet, Nil, xQRPrintable);
    end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TQRPrintStringGrid
//:---------------------------------------------------------------------------
constructor TQRPrintStringGrid.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fOnNeedComponentData := Nil;
  
  mBeforePrintProc     := Nil;
  mOnNeedDataProc      := Nil;
end;

//:---------------------------------------------------------------------------
procedure TQRPrintStringGrid.AddColumns;
var
  i: Integer;
  xPos: Integer;
  xWidth: Integer;
  xStr: string;
begin
  xPos := LeftMargin;
  for i:=Grid.FixedCols to Grid.ColCount-1 do begin
    xWidth := Trunc(CalcZoomedWidth(Grid.ColWidths[i]) * mFactor);
    // nur weitermachen, wenn noch gen�gend Platz in der Breite vorhanden ist
    if (xPos + xWidth) <= fHeaderBand.Band.Width then begin
      xStr   := Grid.Cells[i, 0];
      // add labels to header band
      mHeaderList.Add(AddQRComponents(fHeaderBand, i, xPos, xStr, xWidth, taLeftJustify, fHeaderMultiLine));
      // add labels to detail band and add them to mDataLabels list
      mDataList.Add(AddQRComponents(fDetailBand, i, xPos, xStr, xWidth, taLeftJustify, fDataMultiLine));
  
      inc(xPos, xWidth + ColDistance);
    end else
      Break;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRPrintStringGrid.DoRedirection(aRedirect: Boolean);
begin
  inherited DoRedirection(aRedirect);
  //:...................................................................
  if aRedirect then begin
    // save property values from QuickReport
    mBeforePrintProc := QuickReport.BeforePrint;
    mOnNeedDataProc  := QuickReport.OnNeedData;
    // LINK IN this methodes to receives events to fill in data
    QuickReport.BeforePrint := LocBeforePrint;
    QuickReport.OnNeedData  := LocOnNeedData;
  end else begin
    QuickReport.BeforePrint := mBeforePrintProc;
    QuickReport.OnNeedData  := mOnNeedDataProc;
    mBeforePrintProc        := Nil;
    mOnNeedDataProc         := Nil;
  end;
end;

//:---------------------------------------------------------------------------
function TQRPrintStringGrid.GetGrid: TStringGrid;
begin
  Result := DataGrid as TStringGrid;
end;

//:---------------------------------------------------------------------------
function TQRPrintStringGrid.GetTotalColCount: Integer;
begin
  Result := Grid.ColCount{ - Grid.FixedCols{};
end;

//:---------------------------------------------------------------------------
function TQRPrintStringGrid.GetTotalColWidth: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i:=0 to Grid.ColCount-1 do
    Result := Result + Grid.ColWidths[i];
  
  Result := CalcZoomedWidth(Result);
end;

//:---------------------------------------------------------------------------
procedure TQRPrintStringGrid.LocBeforePrint(Sender: TCustomQuickRep; var PrintReport: Boolean);
begin
  mRow := Grid.FixedRows;
  if Assigned(mBeforePrintProc) then
    mBeforePrintProc(Sender, PrintReport);
end;

//:---------------------------------------------------------------------------
procedure TQRPrintStringGrid.LocOnNeedData(Sender : TObject; var MoreData : Boolean);
var
  i: Integer;
  xQRComp: TQRPrintable;
begin
  MoreData := mRow < Grid.RowCount;
  if MoreData then
  try
    for i:=0 to mDataList.Count-1 do begin
      xQRComp := mDataList.Items[i];
      if xQRComp is TmmQRLabel then begin
        // Offset von FixedCols ber�cksichtigen
        TmmQRLabel(xQRComp).Caption := Grid.Cells[i+Grid.FixedCols, mRow];
      end
      // bei "nicht QRLabel" Komponenten den NeedDataEvent aufrufen
      else if Assigned(fOnNeedComponentData) then
        fOnNeedComponentData(Self, xQRComp, Grid.Cells[i+Grid.FixedCols, mRow]);
    end;
    inc(mRow);
  except
    on e:Exception do
      CodeSite.SendError('LocOnNeedData error: ' + e.Message);
  end;
  if Assigned(mOnNeedDataProc) then
    mOnNeedDataProc(Sender, MoreData);
end;


initialization
end.
