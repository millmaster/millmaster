(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SizingPanel.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Verwaltet ein darauf platziertes Control und passt Hoehe und Breite
                  mit Hilfe eines gegebenen Faktors an. Verhaeltnis ist dadurch immer
                  gleich.
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 08.02.2002  1.00  Wss | Erste Version
                          H2WFactor wird mit Konstante cQMH2WFactor aus MMUGlobal initialisiert
|=============================================================================*)
unit SizingPanel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls;

type
  TSizingPanel = class(TCustomPanel)
  private
    fControl: TControl;
    fH2WFactor: Single;
    procedure SetControl(const Value: TControl);
    procedure SetH2WFactor(const Value: Single);
    procedure ResizeControl;
  protected
    procedure Resize; override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property H2WFactor: Single read fH2WFactor write SetH2WFactor;
  published
    property Align;
    property Anchors;
    property BevelInner;
    property BevelOuter;
    property BevelWidth;
    property BiDiMode;
    property BorderWidth;
    property BorderStyle;
    property Caption;
    property Color;
    property Enabled;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property ShowHint;
    property Visible;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnResize;
    property Control: TControl read fControl write SetControl;
{
    property Alignment;
    property AutoSize;
    property Constraints;
    property Ctl3D;
    property UseDockManager default True;
    property DockSite;
    property DragCursor;
    property DragKind;
    property DragMode;
    property FullRepaint;
    property Font;
    property Locked;
    property ParentBiDiMode;
    property ParentCtl3D;
    property PopupMenu;
    property TabOrder;
    property TabStop;
    property OnCanResize;
    property OnClick;
    property OnConstrainedResize;
    property OnContextPopup;
    property OnDockDrop;
    property OnDockOver;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGetSiteInfo;
    property OnStartDock;
    property OnStartDrag;
    property OnUnDock;
{}
  end;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  MMUGlobal;

//------------------------------------------------------------------------------
// TSizingPanel
//------------------------------------------------------------------------------
constructor TSizingPanel.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fControl := Nil;
  fH2WFactor := cH2WFactorQMatrix;
end;
//------------------------------------------------------------------------------
destructor TSizingPanel.Destroy;
begin
  fControl := Nil;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSizingPanel.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if (Operation = opRemove) and (aComponent = fControl) then
    fControl := Nil;
end;
//------------------------------------------------------------------------------
procedure TSizingPanel.Resize;
begin
  inherited Resize;
  ResizeControl;
end;
//------------------------------------------------------------------------------
procedure TSizingPanel.ResizeControl;
begin
  if Assigned(fControl) then begin
    fControl.Top  := BorderWidth;
    fControl.Left := BorderWidth;
    if (ClientWidth / ClientHeight) > fH2WFactor then begin
      fControl.Height := ClientHeight - (2 * BorderWidth);
      fControl.Width  := Trunc(fControl.Height * fH2WFactor);
    end else begin
      fControl.Width  := ClientWidth - (2 * BorderWidth);
      fControl.Height := Trunc(fControl.Width / fH2WFactor);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSizingPanel.SetControl(const Value: TControl);
begin
  if (Value <> fControl) and (Value <> Self) then begin
    if Assigned(Value) then begin
      if Value.Parent = Self then begin
        fControl := Value;
        fControl.Align := alNone;
        ResizeControl;
      end;
    end else
      fControl := Nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TSizingPanel.SetH2WFactor(const Value: Single);
begin
  if Value <> fH2WFactor then begin
    fH2WFactor := Value;
    ResizeControl;
  end;
end;
//------------------------------------------------------------------------------
end.
