unit regMMConfiguration;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R mmSetupModul.dcr}
//{$R MMJobAdmin.dcr}
{$R TimeEdit.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  NTReg,  // loaded from ThirdParty directory
  mmSetupModul,
  SetupComponents,
  SetupPropEdit,
  TimeEdit{,
  MMJobAdmin{};

procedure MMRegister;
begin
  // Unit NTReg
  NTReg.Register;
  // Unit mmSetupModul
  RegisterComponents('MillMaster', [TMMSetupModul]);
  // Unit SetupPropEdit with property editors
  SetupPropEdit.MMRegister;
  // Unit TimeEdit
  RegisterComponents('LOEPFE',[TMMTimeEdit]);
  // Unit MMJobAdmin
//  RegisterComponents('MillMaster', [TJobAdministrator]);

end;

end.
