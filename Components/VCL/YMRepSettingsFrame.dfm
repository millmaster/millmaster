object fraRepSettings: TfraRepSettings
  Left = 0
  Top = 0
  Width = 659
  Height = 451
  TabOrder = 0
  OnResize = FrameResize
  object laWarningMsg: TmmLabel
    Left = 0
    Top = 0
    Width = 659
    Height = 16
    Align = alTop
    Caption = 'WarningMsg'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clRed
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object pnRight: TmmPanel
    Left = 199
    Top = 16
    Width = 460
    Height = 435
    Align = alRight
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 0
    Visible = False
    object mFCluster: TFFClusterEditBox
      Left = 224
      Top = 290
      Width = 136
      Height = 54
      Anchors = [akLeft, akBottom]
      Caption = '(*)Fremdfasern'
      TabOrder = 0
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
    object mSplice: TSpliceEditBox
      Left = 2
      Top = 144
      Width = 88
      Height = 164
      Caption = '(8)Spleiss'
      TabOrder = 1
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
    object mChannel: TChannelEditBox
      Left = 2
      Top = 0
      Width = 88
      Height = 144
      Caption = '(8)Kanal'
      TabOrder = 2
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
    object mYarnCount: TYarnCountEditBox
      Left = 2
      Top = 290
      Width = 220
      Height = 144
      Anchors = [akLeft, akBottom]
      Caption = '(11)Garnnummer'
      TabOrder = 3
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
    object mmPanel2: TmmPanel
      Left = 224
      Top = 1
      Width = 235
      Height = 283
      Anchors = [akLeft, akTop, akRight, akBottom]
      BevelOuter = bvNone
      TabOrder = 4
      object mFMatrix: TQualityMatrix
        Left = 1
        Top = 1
        Width = 235
        Height = 130
        Color = clWhite
        LastCutColor = clLime
        LastCutField = 0
        LastCutMode = lcNone
        MatrixSubType = mstSiroF
        MatrixType = mtSiro
        ActiveColor = clAqua
        ActiveVisible = True
        Anchors = [akLeft, akTop, akRight]
        ChannelColor = clSilver
        ChannelStyle = psSolid
        ChannelVisible = False
        ClusterColor = clSilver
        ClusterStyle = psSolid
        ClusterVisible = False
        CutsColor = clRed
        DefectsColor = clBlack
        DisableMessage = 'Quality Matrix disabled'
        DisplayMode = dmValues
        DotsColor = clBlack
        Enabled = True
        InactiveColor = 9502719
        MatrixMode = mmSelectSettings
        Model = mXMLSettings
        SpliceColor = clSilver
        SpliceStyle = psSolid
        SpliceVisible = False
        SubFieldX = 0
        SubFieldY = 0
        ZeroLimit = 0.01
      end
    end
    object mRepetition: TRepetitionEditBox
      Left = 92
      Top = 0
      Width = 131
      Height = 259
      Caption = '(12)Wiederholung'
      TabOrder = 5
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
    object mPSettings: TPEditBox
      Left = 224
      Top = 380
      Width = 142
      Height = 54
      Anchors = [akLeft, akBottom]
      Caption = '(11)P Einstellungen'
      TabOrder = 6
      CaptionSpace = True
      Model = mXMLSettings
      Printable = True
      ReadOnly = False
    end
  end
  object pnLeft: TmmPanel
    Left = 0
    Top = 16
    Width = 199
    Height = 435
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 1
    TabOrder = 1
    object pnBottom: TmmPanel
      Left = 1
      Top = 278
      Width = 197
      Height = 156
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object mFaultCluster: TFaultClusterEditBox
        Left = 0
        Top = 0
        Width = 262
        Height = 104
        Caption = '(11)Fehlerschwarm'
        TabOrder = 0
        CaptionSpace = True
        Model = mXMLSettings
        Printable = True
        ReadOnly = False
      end
      object mSFI: TSFIEditBox
        Left = 264
        Top = 0
        Width = 136
        Height = 74
        Caption = '(11)SFI/D'
        TabOrder = 1
        CaptionSpace = True
        Model = mXMLSettings
        Printable = True
        ReadOnly = False
      end
      object mVCV: TVCVEditBox
        Left = 264
        Top = 75
        Width = 136
        Height = 74
        Caption = '(11)VCV'
        TabOrder = 2
        CaptionSpace = True
        Model = mXMLSettings
        Printable = True
        ReadOnly = False
      end
    end
    object mmPanel1: TmmPanel
      Left = 1
      Top = 1
      Width = 197
      Height = 277
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object mQMatrix: TQualityMatrix
        Left = 1
        Top = 1
        Width = 197
        Height = 151
        Color = clWhite
        LastCutColor = clLime
        LastCutField = 0
        LastCutMode = lcNone
        MatrixSubType = mstNone
        MatrixType = mtShortLongThin
        ActiveColor = clAqua
        ActiveVisible = True
        Anchors = [akLeft, akTop, akRight]
        ChannelColor = clRed
        ChannelStyle = psSolid
        ChannelVisible = True
        ClusterColor = clPurple
        ClusterStyle = psSolid
        ClusterVisible = False
        CutsColor = clRed
        DefectsColor = clBlack
        DisableMessage = 'Quality Matrix disabled'
        DisplayMode = dmValues
        DotsColor = clBlack
        Enabled = True
        InactiveColor = 9502719
        MatrixMode = mmDisplayData
        Model = mXMLSettings
        SpliceColor = clBlue
        SpliceStyle = psSolid
        SpliceVisible = False
        SubFieldX = 0
        SubFieldY = 0
        ZeroLimit = 0.01
      end
      object mSpliceQMatrix: TQualityMatrix
        Left = 1
        Top = 1
        Width = 197
        Height = 151
        Color = clWhite
        LastCutColor = clLime
        LastCutField = 0
        LastCutMode = lcNone
        MatrixSubType = mstNone
        MatrixType = mtSplice
        ActiveColor = clFuchsia
        ActiveVisible = True
        Anchors = [akLeft, akTop, akRight]
        ChannelColor = clRed
        ChannelStyle = psSolid
        ChannelVisible = False
        ClusterColor = clPurple
        ClusterStyle = psSolid
        ClusterVisible = False
        CutsColor = clRed
        DefectsColor = clBlack
        DisableMessage = 'Quality Matrix disabled'
        DisplayMode = dmValues
        DotsColor = clBlack
        Enabled = True
        InactiveColor = 9502719
        MatrixMode = mmDisplayData
        Model = mXMLSettings
        SpliceColor = clBlue
        SpliceStyle = psSolid
        SpliceVisible = True
        SubFieldX = 0
        SubFieldY = 0
        ZeroLimit = 0.01
      end
      object mCurveSelection: TmmPanel
        Left = 268
        Top = 6
        Width = 132
        Height = 62
        BevelOuter = bvNone
        TabOrder = 2
        object mcbSpliceCurve: TmmCheckBox
          Left = 6
          Top = 38
          Width = 123
          Height = 17
          Caption = '(13)Spleiss Kurve'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          Visible = True
          OnClick = mcbSpliceCurveClick
          AutoLabel.LabelPosition = lpLeft
        end
        object mcbChannelCurve: TmmCheckBox
          Left = 6
          Top = 6
          Width = 123
          Height = 17
          Caption = '(13)Kanal Kurve'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 0
          Visible = True
          OnClick = mcbChannelCurveClick
          AutoLabel.LabelPosition = lpLeft
        end
        object mcbClusterCurve: TmmCheckBox
          Left = 6
          Top = 22
          Width = 123
          Height = 17
          Caption = '(13)Fehlerschwarm'
          Checked = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          State = cbChecked
          TabOrder = 1
          Visible = True
          OnClick = mcbClusterCurveClick
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
  end
  object mXMLSettings: TVCLXMLSettingsModel
    Left = 67
    Top = 214
  end
end
