unit regDataLayer;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R ClassDataReader.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf,
  ClassDataReader;

//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit ClassDataReader
  RegisterComponents('MillMaster', [TClassDataReader]);
end;
//------------------------------------------------------------------------------
end.
