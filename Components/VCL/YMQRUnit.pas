(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMQRUnit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Components of YMSettings and QualityMatrix for QuickReport
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 26.06.2000  1.00  Wss | Initial Release
| 12.01.2001  1.00  Wss | PutYMSettings added to show clearer settings without SettingsFinder
| 24.01.2001  1.00  Wss | Derivation changed to TmmQRImage because of printing problems (empty control)
| 09.03.2001  1.00  Wss | Derivation changed back to TQRPrintable with use of an TmmQRImage
                          Visibility is now scaled in design, preview and print mode
| 29.06.2001  1.00  Wss | In SetMatrixTyp set size of QMatrix component depenting of SLT or Siro
| 04.07.2001  1.00  Wss | SFIEdit and FFClusterEdit added
| 18.09.2001  1.00  Wss | Property DisplayMode added; in Create set to coarse mode default
| 08.02.2002  1.07  Wss | QMatrix wird nun in richtiger Proportion (Faktor 1.3)ausgegeben
| 03.10.2002        LOK | Umbau ADO
| 22.10.2002  1.07  Wss | Bug beim Printen der QMatrix behoben (hoffentlich!). In Paint Methode
                          wird die PrepareControlSize Methode nur aufgerufen, wenn nicht im
                          IsPrinting Mode ist. Fehler führte dazu, dass z.T. die QMatrix
                          ganz klein auf dem Ausdruck erschien.
| 06.02.2003  1.07  Wss | Beim drucken erschien immer ein Schatten bei der Matrix. Kam vom Befehl
                          Dormant -> in Procedure Print verschoben und nur noch 1x aufrufen
| 03.02.2005  1.08  SDo | TQualityMatrix durch TMMQualityMatrix substituiert
| 10.01.2008  1.09  Nue | VCV eingefügt.
|=========================================================================================*)
unit YMQRUnit;

interface

uses
  Classes, Controls, Graphics, Quickrpt,
  LoepfeGlobal, MMUGlobal, QualityMatrixDef, YMParaEditBox,
  YMParaDef, mmQRImage, XMLDef, VCLXMLSettingsModel, QualityMatrix, mmTranslator, mmDictionary,
  QualityMatrixBase, MMQualityMatrixClass;

const
  cChannelVisible       = 1;
  cSpliceVisible        = 2;
  cClusterVisible       = 3;
  cActiveVisible        = 4;

  cChannelColor         = 1;
  cSpliceColor          = 2;
  cClusterColor         = 3;
  cActiveColor          = 4;
  cInactiveColor        = 5;
  cCutsColor            = 6;
  cDefectsColor         = 7;

  cChannelStyle         = 1;
  cSpliceStyle          = 2;
  cClusterStyle         = 3;

  cChannelEdit          = 1;
  cSpliceEdit           = 2;
  cClusterEdit          = 3;
  cYarnCountEdit        = 4;
  cSFIEdit              = 5;
  cFFClusterEdit        = 6;
  cVCVEdit              = 7;

type
  //*************************** class TQRYMBase ***************************
  TQRBaseClass = class(TQRPrintable)
  private
    fModel: TVCLXMLSettingsModel;
    fShowWarningMsg: Boolean;
    mQRImg: TmmQRImage;
    function GetDictionary: TmmDictionary;
    procedure SetDictionary(const Value: TmmDictionary);
  protected
    mRepClass: TWinControl;
    mTranslator: TmmTranslator;
    procedure PrepareControlSize(aHeight, aWidth: Integer); virtual;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;
    procedure SetModel(const aValue: TVCLXMLSettingsModel); virtual;
    procedure TranslateComponent;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Paint; override;
    procedure Print(aOfsX, aOfsY: Integer); override;
    property Dictionary: TmmDictionary read GetDictionary write SetDictionary;
  published
    property Alignment;
    property AlignToBand;
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
    property OnResize;
    property ShowWarningMsg: Boolean read fShowWarningMsg write fShowWarningMsg;
  end;
  
  //*************************** class TQRQMatrix ***************************
  TQRQMatrix = class(TQRBaseClass)
  private
    fBlackWhite: Boolean;
    fClusterColor: Integer;
    fActiveColor: Integer;
    fChannelColor: Integer;
    fCutsColor: Integer;
    fDefectsColor: Integer;
    fInactiveColor: Integer;
    fSpliceColor: Integer;
    function GetColor(const Index: Integer): TColor;
    function GetDisplayMode: TDisplayMode;
    function GetMatrixType: TMatrixType;
    function GetFModeBD: Boolean;
    function GetPenStyle(const Index: Integer): TPenStyle;
    function GetQMatrix: TMMQualityMatrix;
    function GetVisible(const Index: Integer): Boolean;
    procedure SetBlackWhite(const aValue: Boolean);
    procedure SetColor(const Index: Integer; const Value: TColor);
    procedure SetDisplayMode(const Value: TDisplayMode);
    procedure SetMatrixType(const Value: TMatrixType);
    procedure SetPenStyle(const Index: Integer; const Value: TPenStyle);
    procedure SetVisible(const Index: Integer; const Value: Boolean);
    procedure SetColorsToMatrix;
  protected
    procedure PrepareControlSize(aHeight, aWidth: Integer); override;
    procedure SetModel(const aValue: TVCLXMLSettingsModel); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure Assign(Source: TPersistent); override;
    property BlackWhite: Boolean read fBlackWhite write SetBlackWhite;
    property FModeBD: Boolean read GetFModeBD;
    property QMatrix: TMMQualityMatrix read GetQMatrix;
  published
    property ActiveColor: TColor index cActiveColor read GetColor write SetColor;
    property ActiveVisible: Boolean index cActiveVisible read GetVisible write SetVisible;
    property ChannelColor: TColor index cChannelColor read GetColor write SetColor;
    property ChannelStyle: TPenStyle index cChannelStyle read GetPenStyle write SetPenStyle;
    property ChannelVisible: Boolean index cChannelVisible read GetVisible write SetVisible;
    property ClusterColor: TColor index cClusterColor read GetColor write SetColor;
    property ClusterStyle: TPenStyle index cClusterStyle read GetPenStyle write SetPenStyle;
    property ClusterVisible: Boolean index cClusterVisible read GetVisible write SetVisible;
    property CutsColor: TColor index cCutsColor read GetColor write SetColor;
    property DefectsColor: TColor index cDefectsColor read GetColor write SetColor;
    property DisplayMode: TDisplayMode read GetDisplayMode write SetDisplayMode;
    property InactiveColor: TColor index cInactiveColor read GetColor write SetColor;
    property MatrixType: TMatrixType read GetMatrixType write SetMatrixType;
    property SpliceColor: TColor index cSpliceColor read GetColor write SetColor;
    property SpliceStyle: TPenStyle index cSpliceStyle read GetPenStyle write SetPenStyle;
    property SpliceVisible: Boolean index cSpliceVisible read GetVisible write SetVisible;
  end;
  
  //*************************** class TQRCStandard ***************************
  TQRXMLSettings = class(TQRBaseClass)
  private
    fSettingsGroup: TSettingsGroup;
    function GetYarnCount: Single;
    function GetYarnUnit: TYarnUnit;
    procedure SetSettingsGroup(aValue: TSettingsGroup);
    procedure SetYarnCount(const Value: Single);
    procedure SetYarnUnit(const Value: TYarnUnit);
  protected
    procedure PrepareControlSize(aHeight, aWidth: Integer); override;
    procedure SetModel(const aValue: TVCLXMLSettingsModel); override;
  public
    constructor Create(aOwner: TComponent); override;
    property YarnCount: Single read GetYarnCount write SetYarnCount;
  published
    property SettingsGroup: TSettingsGroup read fSettingsGroup write SetSettingsGroup;
    property YarnUnit: TYarnUnit read GetYarnUnit write SetYarnUnit;
  end;
  
  TQRCSBasic = class(TQRBaseClass)
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
  end;
  
//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS,
  mmImage, IvMulti, 
  mmStringGrid,
  SysUtils, EditBox;

//:---------------------------------------------------------------------------
//:--- Class: TQRBaseClass
//:---------------------------------------------------------------------------
constructor TQRBaseClass.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  mRepClass       := Nil;
  fShowWarningMsg := False;

  mQRImg := TmmQRImage.Create(Self);
  with mQRImg do begin
    AutoSize := False;
    Enabled  := False;
    Left     := 1;
    Parent   := Self;
    Stretch  := True;
    Transparent := False;
    Top      := 1;
  end;
end;

//:---------------------------------------------------------------------------
destructor TQRBaseClass.Destroy;
begin
  FreeAndNil(mRepClass);
  FreeAndNil(mQRImg);
  inherited Destroy;
end;

//------------------------------------------------------------------------------
function TQRBaseClass.GetDictionary: TmmDictionary;
begin
  if Assigned(mTranslator) then
    Result := TmmDictionary(mTranslator.Dictionary)
  else
    Result := Nil;
end;

//:---------------------------------------------------------------------------
procedure TQRBaseClass.Paint;
begin
  // wenn IsPrinting nicht abgefragt wird, dann kann es vorkommen, dass per Print
  // die QMatrix sich nochmals zeichnen möchte. In diesem Falls darf ich aber
  // die Dimensionen nicht mehr anpassen
  if not IsPrinting then
    PrepareControlSize(Height - 3, Width - 3);
  inherited Paint;
end;

//:---------------------------------------------------------------------------
procedure TQRBaseClass.PrepareControlSize(aHeight, aWidth: Integer);
begin
  mQRImg.Height := aHeight;
  mQRImg.Width  := aWidth;

  if Assigned(mRepClass) then begin
    with mQrImg.Picture.Bitmap do begin
      Height := mRepClass.Height;
      Width  := mRepClass.Width;
      mRepClass.PaintTo(Canvas.Handle, 0, 0);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRBaseClass.Print(aOfsX, aOfsY: Integer);
var
  CalcLeft, CalcTop, CalcRight, CalcBottom: LongInt;
begin
  with ParentReport.QRPrinter do begin
    CalcLeft := XPos(aOfsX + Size.Left);
    CalcTop  := YPos(aOfsY + Size.Top);
    CalcRight := XPos(aOfsX + Size.Left + Size.Width);
    CalcBottom := YPos(aOfsY + Size.Top + Size.Height);

    PrepareControlSize(CalcBottom-CalcTop, CalcRight-CalcLeft);
    mQRImg.Picture.Bitmap.Dormant;
    mQRImg.PaintTo(Canvas.Handle, CalcLeft, CalcTop);
  end;
  inherited Print(aOfsX, aOfsY);
end;

//:---------------------------------------------------------------------------
procedure TQRBaseClass.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);

  if Assigned(mRepClass) then begin
    // move component out of visible range
    mRepClass.Top := Height;
  end;
end;

//------------------------------------------------------------------------------
procedure TQRBaseClass.SetDictionary(const Value: TmmDictionary);
begin
  if not Assigned(mTranslator) then begin
    mTranslator := TmmTranslator.Create(Self);
    with mTranslator do begin
      Targets.Add(TIvTargetProperty.Create('', 'Caption', ivttInclude));
      Targets.Add(TIvTargetProperty.Create('', 'Hint', ivttInclude));
      Targets.Add(TIvTargetProperty.Create('', 'Items', ivttInclude));
    end;
  end;
  mTranslator.Dictionary := Value;
  TranslateComponent;
end;

//:---------------------------------------------------------------------------
procedure TQRBaseClass.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  fModel := aValue;
end;

procedure TQRBaseClass.TranslateComponent;
begin
  if Assigned(mTranslator) and Assigned(mTranslator.Dictionary) then
    mTranslator.Translate;
end;

//:---------------------------------------------------------------------------
//:--- Class: TQRQMatrix
//:---------------------------------------------------------------------------
constructor TQRQMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mRepClass := TMMQualityMatrix.Create(Self);
  mRepClass.Parent := mQRImg; // Self;

  with QMatrix do begin
    FixedSize     := True;
    Left          := -Width; // aus dem sichtbaren Bereich schieben
    fChannelColor := ChannelColor;
    fSpliceColor   := SpliceColor;
    fClusterColor  := ClusterColor;
    fActiveColor   := ActiveColor;
    fInactiveColor := InactiveColor;
    fCutsColor     := CutsColor;
    fDefectsColor  := DefectsColor;
  end;
end;

//------------------------------------------------------------------------------

//******************************************************************************
// TRepQMatrix
//******************************************************************************
procedure TQRQMatrix.Assign(Source: TPersistent);
var
  xSourceMatrix: TMMQualityMatrix;
begin
  if Source is TMMQualityMatrix then begin
    // erst mal alle Properties und Daten übernehmen lassen
    QMatrix.Assign(Source);

    // dann die eigenen Properties, welche eine sonderbehandlung haben noch einzeln kopieren
    xSourceMatrix := TMMQualityMatrix(Source);
    MatrixType     := xSourceMatrix.MatrixType;
    ActiveColor    := xSourceMatrix.ActiveColor;
    InactiveColor  := xSourceMatrix.InactiveColor;
    CutsColor      := xSourceMatrix.CutsColor;
    DefectsColor   := xSourceMatrix.DefectsColor;

    ChannelColor   := xSourceMatrix.ChannelColor;
    ClusterColor   := xSourceMatrix.ClusterColor;
    SpliceColor    := xSourceMatrix.SpliceColor;
  end else
    inherited Assign(Source);
end;

//:---------------------------------------------------------------------------
function TQRQMatrix.GetColor(const Index: Integer): TColor;
begin
  case Index of
    cChannelColor:  Result := fChannelColor;
    cSpliceColor:   Result := fSpliceColor;
    cClusterColor:  Result := fClusterColor;
    cActiveColor:   Result := fActiveColor;
    cInactiveColor: Result := fInactiveColor;
    cCutsColor:     Result := fCutsColor;
    cDefectsColor:  Result := fDefectsColor;
  else
    Result := clWhite;
  end;
end;

//:---------------------------------------------------------------------------
function TQRQMatrix.GetDisplayMode: TDisplayMode;
begin
  Result := QMatrix.DisplayMode;
end;

//:---------------------------------------------------------------------------
function TQRQMatrix.GetMatrixType: TMatrixType;
begin
  Result := QMatrix.MatrixType;
end;

//:---------------------------------------------------------------------------
//Nue:23.06.08 eingefügt um in den Printkomponenten den FMode abfragen zu können
function TQRQMatrix.GetFModeBD: Boolean;
begin
  Result := QMatrix.FModeBD;
end;
//:---------------------------------------------------------------------------

function TQRQMatrix.GetPenStyle(const Index: Integer): TPenStyle;
begin
  case Index of
    cChannelStyle: Result := QMatrix.ChannelStyle;
    cSpliceStyle:  Result := QMatrix.SpliceStyle;
    cClusterStyle: Result := QMatrix.ClusterStyle;
  else
    Result := psSolid;
  end;
end;

//:---------------------------------------------------------------------------
function TQRQMatrix.GetQMatrix: TMMQualityMatrix;
begin
  Result := (mRepClass as TMMQualityMatrix);
end;

//:---------------------------------------------------------------------------
function TQRQMatrix.GetVisible(const Index: Integer): Boolean;
begin
  case Index of
    cChannelVisible: Result := QMatrix.ChannelVisible;
    cSpliceVisible:  Result := QMatrix.SpliceVisible;
    cClusterVisible: Result := QMatrix.ClusterVisible;
    cActiveVisible:  Result := QMatrix.ActiveVisible;
  else
    Result := True;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.PrepareControlSize(aHeight, aWidth: Integer);
var
  xFactor: Single;
  xImgHeight: Integer;
  xImgWidth: Integer;
begin
  with mQrImg.Picture.Bitmap do begin
    Height := mRepClass.Height;
    Width  := mRepClass.Width;
    xFactor := Width / Height;
  end;
  Self.Height := Round(Self.Width / xFactor);

  xImgHeight := Trunc(aWidth / xFactor);
  if xImgHeight > aHeight then begin
    xImgHeight := aHeight;
    xImgWidth  := Trunc(aHeight * xFactor);
  end else
    xImgWidth := aWidth;

  mQRImg.Height := xImgHeight;
  mQRImg.Width  := xImgWidth;

  with mQRImg.Picture.Bitmap do
    mRepClass.PaintTo(Canvas.Handle, 0, 0);
end;
//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetBlackWhite(const aValue: Boolean);
begin
  if fBlackWhite <> aValue then begin
    fBlackWhite := aValue;
    with QMatrix do begin
//      fYarnFaultClassFont := YarnFaultClassFont.Color;
//      fCutsColor          := CutsColor;
      if fBlackWhite then begin
        ActiveColor   := clSilver;
        InactiveColor := clWhite;
        DefectsColor  := clBlack;
        CutsColor     := clBlack;
        ChannelColor  := clBlack;
        SpliceColor   := clBlack;
        ClusterStyle  := psDot;
        SpliceStyle   := psDash;

        YarnFaultClassFont.Color := clGray;
        FontColorGroupSubField   := clSilver;
        
      end
      else begin
        SetColorsToMatrix;
      end; // if fBlackWhite
    end; // with
    Invalidate;
  end; // if Value <>
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetColorsToMatrix;
begin
  if not fBlackWhite then begin
    with QMatrix do begin
      ActiveColor   := fActiveColor;
      InactiveColor := fInactiveColor;
      DefectsColor  := fDefectsColor;
      CutsColor     := fCutsColor;
      ChannelColor  := fChannelColor;
      ClusterColor  := fClusterColor;
      SpliceColor   := fSpliceColor;

      ClusterStyle  := psSolid;
      SpliceStyle   := psSolid;

//      YarnFaultClassFont.Color := fYarnFaultClassFont;
    end; // with
    Invalidate;
  end; // if Value <>
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetColor(const Index: Integer; const Value: TColor);
begin
  case Index of
    cChannelColor:  fChannelColor  := Value;
    cSpliceColor:   fSpliceColor   := Value;
    cClusterColor:  fClusterColor  := Value;
    cActiveColor:   fActiveColor   := Value;
    cInactiveColor: fInactiveColor := Value;
    cCutsColor:     fCutsColor     := Value;
    cDefectsColor:  fDefectsColor  := Value;
  else
  end;
  SetColorsToMatrix;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetDisplayMode(const Value: TDisplayMode);
begin
  QMatrix.DisplayMode := Value;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetMatrixType(const Value: TMatrixType);
begin
  // Ein vorhandenes Bild vorher freigeben, damit es keine Clipping-Probleme mit Restbildern gibt
  mQRImg.Picture := Nil;

  QMatrix.MatrixType := Value;
  Invalidate;

//  mQRImg.Picture := Nil;
//
//  if Value = mtSiro then begin
////    QMatrix.Height := 200;
//    QMatrix.Width  := 360;
//  end else begin
//    QMatrix.Height := 400;
//    QMatrix.Width  := Trunc(QMatrix.Height * cH2WFactorQMatrix);
//  end;
//  QMatrix.MatrixType := Value;
//  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  inherited SetModel(aValue);
  if Assigned(mRepClass) then
    TMMQualityMatrix(mRepClass).Model := Self.Model;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetPenStyle(const Index: Integer; const Value: TPenStyle);
begin
  case Index of
    cChannelStyle: QMatrix.ChannelStyle := Value;
    cSpliceStyle:  QMatrix.SpliceStyle  := Value;
    cClusterStyle: QMatrix.ClusterStyle := Value;
  else
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRQMatrix.SetVisible(const Index: Integer; const Value: Boolean);
begin
  case Index of
    cChannelVisible: QMatrix.ChannelVisible := Value;
    cSpliceVisible:  QMatrix.SpliceVisible  := Value;
    cClusterVisible: QMatrix.ClusterVisible := Value;
    cActiveVisible:  QMatrix.ActiveVisible  := Value;
  else
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TQRXMLSettings
//:---------------------------------------------------------------------------
constructor TQRXMLSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Height := 60;
  Width  := 100;
  
  fSettingsGroup := sgNone;
end;

//:---------------------------------------------------------------------------
function TQRXMLSettings.GetYarnCount: Single;
begin
  if fSettingsGroup = sgYarnCount then
    Result := TYarnCountEditBox(mRepClass).YarnCount
  else
    Result := 0;
end;

//:---------------------------------------------------------------------------
function TQRXMLSettings.GetYarnUnit: TYarnUnit;
begin
  if fSettingsGroup = sgYarnCount then
    Result := TYarnCountEditBox(mRepClass).YarnUnit
  else
    Result := yuNone;
end;

//:---------------------------------------------------------------------------
procedure TQRXMLSettings.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  inherited SetModel(aValue);
  if Assigned(mRepClass) then
    TGroupEditBox(mRepClass).Model := Self.Model;
end;

//:---------------------------------------------------------------------------
//Nue
procedure TQRXMLSettings.PrepareControlSize(aHeight, aWidth: Integer);
var
  xFactor: Single;
  xImgHeight: Integer;
  xImgWidth: Integer;
begin
  with mQrImg.Picture.Bitmap do begin
    Height := mRepClass.Height;
    Width  := mRepClass.Width;
    xFactor := Width / Height;
  end;
  Self.Height := Round(Self.Width / xFactor);

  xImgHeight := Trunc(aWidth / xFactor);
  if xImgHeight > aHeight then begin
    xImgHeight := aHeight;
    xImgWidth  := Trunc(aHeight * xFactor);
  end else
    xImgWidth := aWidth;

  mQRImg.Height := xImgHeight;
  mQRImg.Width  := xImgWidth;

  with mQRImg.Picture.Bitmap do
    mRepClass.PaintTo(Canvas.Handle, 0, 0);
end;
//:---------------------------------------------------------------------------
procedure TQRXMLSettings.SetSettingsGroup(aValue: TSettingsGroup);
  //..........................................................
  procedure PrepareRepClass;
  begin
    with TGroupEditBox(mRepClass) do begin
      Font.Name := 'Arial';
      Top       := -Height; // aus dem sichtbaren Bereich schieben
      Printable := True;
      Parent    := Self;
      Model     := Self.Model;
    end;
    TranslateComponent;
    // Wenn der QuickReport per Zoom verkleinert dargestellt wird, dann dies in der
    // Grössenanpassung der Komponente berücksichtigen
    Self.Width  := Round((Self.ParentReport.Zoom / 100.0) * mRepClass.Width);
    Self.Height := Round((Self.ParentReport.Zoom / 100.0) * mRepClass.Height);
  end;
  //..........................................................
begin
  if fSettingsGroup <> aValue then begin
    fSettingsGroup := aValue;

    if Assigned(mRepClass) then
      FreeAndNil(mRepClass);

    case fSettingsGroup of
      sgNone:       FreeAndNil(mRepClass);
      sgChannel:    mRepClass := TChannelEditBox.Create(Self);
      sgSplice:     mRepClass := TSpliceEditBox.Create(Self);
      sgCluster:    mRepClass := TFaultClusterEditBox.Create(Self);
      sgFCluster:   mRepClass := TFFClusterEditBox.Create(Self);
      sgSFI:        mRepClass := TSFIEditBox.Create(Self);
      sgVCV:        mRepClass := TVCVEditBox.Create(Self);
      sgYarnCount:  mRepClass := TYarnCountEditBox.Create(Self);
      sgP:          mRepClass := TPEditBox.Create(Self);
      sgPExt:       mRepClass := TPExtEditBox.Create(Self);
      sgRepetition: mRepClass := TRepetitionEditBox.Create(Self);
    else
    end;

    if Assigned(mRepClass) then
      PrepareRepClass;
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TQRXMLSettings.SetYarnCount(const Value: Single);
begin
  if fSettingsGroup = sgYarnCount then
    TYarnCountEditBox(mRepClass).YarnCount := Value;
end;

//:---------------------------------------------------------------------------
procedure TQRXMLSettings.SetYarnUnit(const Value: TYarnUnit);
begin
  if fSettingsGroup = sgYarnCount then
    TYarnCountEditBox(mRepClass).YarnUnit := Value;
end;

//:---------------------------------------------------------------------------
//:--- Class: TQRCSBasic
//:---------------------------------------------------------------------------
constructor TQRCSBasic.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  Height := 60;
  Width  := 100;
  
//  mRepClass := TRepCSBasic.Create(Self);
//  mRepClass.Parent := Self;
end;

//:---------------------------------------------------------------------------
destructor TQRCSBasic.Destroy;
begin
  inherited Destroy;
end;

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------

end.

