{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SetupPropEdit.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: TSetupPropertyEditor -> fuer alle Setup-Komponenten
|
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.09.1999  1.00  SDo | File created
===============================================================================}
unit SetupPropEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, dsgnintf, BaseSetup, LoepfeGlobal, Mask,
  mmMaskEdit, mmSpinEdit, mmComboBox, mmCheckBox, mmGroupBox;

type

  TSetupPropertyEditor = class(TStringProperty)
  private
   // procedure Activate; override;
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
    function GetValue: string; override;
    procedure GetValues(Proc: TGetStrProc); override;
    procedure SetValue(const Value: string); override;
  end;

procedure MMRegister;


implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
 TypInfo, SetupComponents ;


procedure MMRegister;
begin

  RegisterComponents('MillMaster', [TSetupEdit]);
  RegisterPropertyEditor(TypeInfo(String), TSetupEdit, 'ValueName',
                                                         TSetupPropertyEditor);

  RegisterComponents('MillMaster', [TSetupLabel]);
  RegisterPropertyEditor(TypeInfo(String), TSetupLabel, 'ValueName',
                                                         TSetupPropertyEditor);

  RegisterComponents('MillMaster', [TSetupSpinEdit]);
  RegisterPropertyEditor(TypeInfo(String), TSetupSpinEdit, 'ValueName',
                                                         TSetupPropertyEditor);
  RegisterComponents('MillMaster', [TSetupComBobox]);
  RegisterPropertyEditor(TypeInfo(String), TSetupComBobox, 'ValueName',
                                                         TSetupPropertyEditor);

  RegisterComponents('MillMaster', [TSetupCheckBox]);
  RegisterPropertyEditor(TypeInfo(String), TSetupCheckBox, 'ValueName',
                                                         TSetupPropertyEditor);

  RegisterComponents('MillMaster', [TSetupRadioGroup]);
  RegisterPropertyEditor(TypeInfo(String), TSetupRadioGroup, 'ValueName',
                                                         TSetupPropertyEditor);

end;
//------------------------------------------------------------------------------


{ TSetupPropertyEditor }

//------------------------------------------------------------------------------
procedure TSetupPropertyEditor.Edit;
begin
 { Do nothing }
end;
//------------------------------------------------------------------------------
function TSetupPropertyEditor.GetAttributes: TPropertyAttributes;
begin
 Result := [paValueList, paSortList, paAutoUpdate, paRevertable	];
end;
//------------------------------------------------------------------------------
function TSetupPropertyEditor.GetValue: string;
var xValue : String;
begin
  xValue := GetStrValue;

  if xValue <> '' then
    Result := xValue
  else
    Result := '';
end;
//------------------------------------------------------------------------------

//******************************************************************************
// UniqueValueNames aus Konst.Array lesen
//******************************************************************************
procedure TSetupPropertyEditor.GetValues(Proc: TGetStrProc);
var
 I    : Integer;
 xBSM : TBaseSetupModul;
begin

 if GetComponent(0) is TSetupEdit then begin
   xBSM := (GetComponent(0) as TSetupEdit).SetupModul;
   if xBSM= NIL then exit;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] = ctEdit then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;

 if GetComponent(0) is TSetupSpinEdit then begin
   xBSM := (GetComponent(0) as TSetupSpinEdit).SetupModul;
   if xBSM= NIL then exit;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] = ctSpinEdit then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;

 if GetComponent(0) is TSetupCombobox then begin
   xBSM := (GetComponent(0) as TSetupCombobox).SetupModul;
   if xBSM= NIL then exit;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] = ctCombobox then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;

 if GetComponent(0) is TSetupCheckBox then begin
   xBSM := (GetComponent(0) as TSetupCheckBox).SetupModul;
   if xBSM= NIL then exit;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] = ctCheckBox then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;

 if GetComponent(0) is TSetupRadioGroup then begin
   xBSM := (GetComponent(0) as TSetupRadioGroup).SetupModul;
   if xBSM= NIL then exit;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] = ctRadioGroup then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;

 // Alle UniqueValueNames auslesen
 if GetComponent(0) is TSetupLabel then begin
   xBSM := (GetComponent(0) as TSetupLabel).SetupModul;
   if xBSM= NIL then begin
     SetStrValue('');
     exit;
   end;
   for i:=0 to xBSM.NumberOfValues-1 do begin
     if xBSM.ComponentType[i] <> ctNone then begin
       Proc(xBSM.UniqueValueName[i]);
     end;
   end;
   exit;
 end;
end;
//------------------------------------------------------------------------------
procedure TSetupPropertyEditor.SetValue(const Value: string);
var i      : Integer;
    xBSM   : TBaseSetupModul;
    xValue : String;
begin

 if Value = '' then begin
    SetStrValue('');
    exit;
 end;

 // TSetupSpinEdit
 if GetComponent(0) is TSetupSpinEdit then begin
    xBSM := (GetComponent(0) as TSetupSpinEdit).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupSpinEdit) do begin
                MaxValue := xBSM.MaxValue[i] / xBSM.Factor[i];
                MinValue := xBSM.MinValue[i] / xBSM.Factor[i];
                Hint     := xBSM.HintText[i];
                AutoLabel.Control.Caption:= xBSM.ValueText[i];
           end;
           exit;
        end;
 end;

 // TSetupEdit
 if GetComponent(0) is TSetupEdit then begin
    xBSM := (GetComponent(0) as TSetupEdit).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupEdit) do begin
                Hint     := xBSM.HintText[i];
                Text    := '';
                AutoLabel.Control.Caption:= xBSM.ValueText[i];
           end;
           exit;
        end;
 end;

 // TSetupCombobox
 if GetComponent(0) is TSetupCombobox then begin
    xBSM := (GetComponent(0) as TSetupCombobox).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupCombobox) do begin
                Hint     := xBSM.HintText[i];
                AutoLabel.Control.Caption:= xBSM.ValueText[i];
           end;
           exit;
        end;
 end;

 // TSetupCheckBox
 if GetComponent(0) is TSetupCheckBox then begin
    xBSM := (GetComponent(0) as TSetupCheckBox).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupCheckBox) do begin
                Hint     := xBSM.HintText[i];
                Caption  := xBSM.ValueText[i];
                AutoLabel.Control.Caption:= xBSM.ValueText[i];
          end;
          exit;
        end;
 end;

 // TSetupLabel
 if GetComponent(0) is TSetupLabel then begin
    xBSM := (GetComponent(0) as TSetupLabel).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupLabel) do begin
                Hint     := xBSM.HintText[i];
                Caption  := xBSM.Description[i];
           end;
           break;
        end;
 end;

 // TSetupRadioGroup
 if GetComponent(0) is TSetupRadioGroup then begin
    xBSM := (GetComponent(0) as TSetupRadioGroup).SetupModul;
    for i:=0 to xBSM.NumberOfValues do
        if xBSM.UniqueValueName[i] = Value then begin
           xValue:= xBSM.UniqueValueName[i];
           SetStrValue(xValue);
           with (GetComponent(0) as TSetupRadioGroup) do begin
                Hint     := xBSM.HintText[i];
                Caption  := xBSM.ValueText[i];
           end;
           exit;
        end;
 end;

end;
//------------------------------------------------------------------------------

end.
