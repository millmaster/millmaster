unit VCLThread;

interface

uses
  Classes, SysUtils;

type
  TLocalThread = class(TThread)
  private
    mOwner : TComponent;
    fOnExecution: TNotifyEvent;
    fOnAfterExecution: TNotifyEvent;
    fAutoSuspend: Boolean;
  public
    constructor Create(aOwner: TComponent); reintroduce;
    property AutoSuspend: Boolean read fAutoSuspend write fAutoSuspend;
    procedure Execute; override;
    property OnExecution: TNotifyEvent read fOnExecution write fOnExecution;
    property OnAfterExecution: TNotifyEvent read fOnAfterExecution write fOnAfterExecution;
  end;

  TVCLThread = class(TComponent)
  private
    mThread: TLocalThread;
    FCreateSuspended: Boolean;
    fOnExecution: TNotifyEvent;
    fOnAfterExecution: TNotifyEvent;
    fFreeOnTerminate: Boolean;
    fAutoSuspend: Boolean;
//    function GetFreeOnTerminate: Boolean;
    function GetSuspended: Boolean;
    procedure SetCreateSuspended(const Value: Boolean);
    procedure SetFreeOnTerminate(const Value: Boolean);
    procedure SetAutoSuspend(const Value: Boolean);
    procedure SetOnAfterExecution(const Value: TNotifyEvent);
  public
    property Suspended: Boolean read GetSuspended;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
    procedure Resume;
    procedure Suspend;
    procedure Terminate;
  published
    property AutoSuspend: Boolean read fAutoSuspend write SetAutoSuspend;
    property CreateSuspended: Boolean read FCreateSuspended write SetCreateSuspended default True;
    property FreeOnTerminate: Boolean read fFreeOnTerminate write SetFreeOnTerminate;
    property OnExecution: TNotifyEvent read fOnExecution write fOnExecution;
    property OnAfterExecution: TNotifyEvent read fOnAfterExecution write SetOnAfterExecution;
  end;
implementation
uses
  mmCS;

{ TLocalThread }

constructor TLocalThread.Create(aOwner: TComponent);
begin
  inherited Create(True);
  mOwner := aOwner;
end;

procedure TLocalThread.Execute;
begin
  while not Terminated do
  try
    if Assigned(fOnExecution) then begin
      fOnExecution(mOwner);
      if fAutoSuspend then
        Suspend;
    end else
      Suspend;
  except
    on e:Exception do begin
      CodeSite.SendError('TLocalThread.Execute: ' + e.Message);
    end;
  end;
  if Assigned(fOnAfterExecution) then
    fOnAfterExecution(mOwner);
end;


{ TVCLThread }

constructor TVCLThread.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAutoSuspend     := False;
  FCreateSuspended := True;
  fFreeOnTerminate := False;
  fOnExecution     := Nil;

  mThread := Nil
end;

destructor TVCLThread.Destroy;
begin
  mThread.Free;
  inherited Destroy;
end;


{
function TVCLThread.GetFreeOnTerminate: Boolean;
begin
  Result := fFreeOnTerminate;
end;
{}
function TVCLThread.GetSuspended: Boolean;
begin
  Result := False;
  if Assigned(mThread) then
    Result := mThread.Suspended;
end;

procedure TVCLThread.Loaded;
begin
  inherited Loaded;
  if not(csDesigning in ComponentState) then begin
    mThread := TLocalThread.Create(Self);
    mThread.AutoSuspend     := fAutoSuspend;
    mThread.FreeOnTerminate := fFreeOnTerminate;
    mThread.OnExecution     := fOnExecution;
    mThread.OnAfterExecution := fOnAfterExecution;
    if not FCreateSuspended then
      mThread.Resume;
  end;
end;

procedure TVCLThread.Resume;
begin
  if Assigned(mThread) then
    mThread.Resume;
end;

procedure TVCLThread.SetAutoSuspend(const Value: Boolean);
begin
  fAutoSuspend := Value;
  if Assigned(mThread) then
    mThread.AutoSuspend := fAutoSuspend;
end;

procedure TVCLThread.SetCreateSuspended(const Value: Boolean);
begin
  FCreateSuspended := Value;
end;

procedure TVCLThread.SetFreeOnTerminate(const Value: Boolean);
begin
  fFreeOnTerminate := Value;
  if Assigned(mThread) then
    mThread.FreeOnTerminate := fFreeOnTerminate;
end;

procedure TVCLThread.SetOnAfterExecution(const Value: TNotifyEvent);
begin
  fOnAfterExecution := Value;
  if Assigned(mThread) then
    mThread.OnAfterExecution := fOnAfterExecution;
end;

procedure TVCLThread.Suspend;
begin
  if Assigned(mThread) then
    mThread.Suspend;
end;

procedure TVCLThread.Terminate;
begin
  if Assigned(mThread) then
    mThread.Terminate;
end;

end.
