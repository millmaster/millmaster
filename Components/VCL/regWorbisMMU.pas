unit regWorbisMMU;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R wwQRPrintDBGrid.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  pwDBEdit,
  pwDBLookupCombo,
  wwQRPrintDBGrid;

//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit pwDBEdit, pwDBLookupCombo, wwQRPrintDBGrid
  RegisterComponents('Worbis', [TpwDBEdit, TpwDBLookupCombo, TwwQRPrintDBGrid]);
end;
//------------------------------------------------------------------------------
end.
