(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: UEditIPAddr.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 21.03.2005  1.00  Wss  | Methode Changed eingefügt und in OnTripletEditChange() aufgerufen
| 15.06.2005        Lok  | Methode ResizeTriplets() eingeführt und in der überschriebenen Methode
|                        |   ConstrainedResize() aufgerufen. --> Passt das Eingabefeld der
|                        |   Breite der Komponente an (Probleme mit Chinesischem System)
|=============================================================================*)

unit UEditIPAddr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, ExtCtrls;

type
    TTriplets = array[0..3] of TMemo;
    TDots = array[0..2] of TLabel;

type
  CEditIPAddr = class(TWinControl)
  private
    FAcceptOctal : Boolean;
    FTriplets : TTriplets;
    FDots : TDots;
    FPanel : TPanel;
    function CreateTripletEdit(WhichTriplet : Byte) : TMemo;
    function CreateDotLabel(WhichDot : Byte) : TLabel;

    function getText : String;
    procedure setText(aIPAddr : String);
    procedure OnTripletEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure OnTripletEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure OnSelfExit(Sender : TObject);
    procedure OnTripletEditChange(Sender : TObject);
    procedure WMSetFocus(var Message: TWMSetFocus); message WM_SETFOCUS;
    procedure OnPanelClick(Sender : TObject);
    procedure OnDotLabelClick(Sender : TObject);
    procedure ResizeTriplets;
  protected
    procedure Changed; dynamic;
    procedure ConstrainedResize(var MinWidth, MinHeight, MaxWidth, MaxHeight: Integer); override;
  public
    constructor create(aOwner : TComponent); override;
  published
    property Align;
    property Anchors;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop default true;
    property Visible;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnStartDock;
    property OnStartDrag;
    property Text : String read getText write setText;
    property AcceptOctal : Boolean read FAcceptOctal write FAcceptOctal default false;
    property Font;
  end;

procedure Register;

implementation

uses
  UIPAddrUtils;

procedure Register;
begin
  RegisterComponents('NDSUtils', [CEditIPAddr]);
end;

constructor CEditIPAddr.create(aOwner : TComponent);
var
    i : Byte;
begin
    inherited Create(aOwner);

    FPanel := TPanel.Create(Self);
    FPanel.Height := 21;
    FPanel.Width := 100;
    FPanel.BevelOuter := bvNone;
    FPanel.BorderStyle := bsSingle;
    FPanel.Color := clWindow;
    FPanel.Left := 0;
    FPanel.Top := 0;
    FPanel.Parent := self;
    FPanel.OnExit := OnSelfExit;
    FPanel.Align := alClient;
    FPanel.OnClick := OnPanelClick;
    FPanel.Cursor := crIBeam; 

    for i := 0 to 3 do
        FTriplets[i] := CreateTripletEdit(i);
    for i := 0 to 2 do
        FDots[i] := CreateDotLabel(i);

    Height := 21;
    Width := 100;
    Constraints.MinWidth := 100;
end;

procedure CEditIPAddr.Changed;
begin
  inherited Changed;
end;

(*---------------------------------------------------------
  Wird beim Resize aufgerufen und Berechnet die Felder proportional
  zur Breite des gesamten Feldes.
----------------------------------------------------------*)
procedure CEditIPAddr.ResizeTriplets;
var
  i: integer;
const
  cLeftMargin = 1;
var
  xEditWidth: integer;
  xDotWidth: integer;
begin
  // Breite des gesamten Feldes
  xEditWidth := (Width - cLeftMargin) div 4;
  // Breite des Punktes
  xDotWidth := 4;
  // Resultierend die Breite des einzelnen Eingabefeldes
  xEditWidth := xEditWidth - xDotWidth;

  // Panel anpassen (Auf dem Panel liegen die Eingabefelder)
  FPanel.Width := Width;

  // Die eingabefelder verteilen
  for i := 0 to High(TTriplets) do begin
    FTriplets[i].Left := cLeftMargin + ((xEditWidth + xDotWidth) * i);
    FTriplets[i].Width := xEditWidth;
  end;// for i := 0 to High(TTriplets) do begin

  // Die Labels (Punkte) verteilen
  for i := 0 to High(TDots) do begin
    FDots[i].Left := cLeftMargin + xEditWidth + ((xEditWidth + xDotWidth) * i);
    FDots[i].Width := xDotWidth;
  end;// for i := 0 to High(TDots) do begin
end;

function CEditIPAddr.CreateTripletEdit(WhichTriplet : Byte) : TMemo;
begin
    Result := TMemo.Create(Self);
    Result.Parent := FPanel;
    Result.Width := 21;
    Result.BorderStyle := bsNone;
    if (WhichTriplet = 0) then
        Result.TabOrder := 0
    else
        Result.TabStop := False;
    Result.Text := '';
    Result.Left := 1+24*WhichTriplet;
    Result.ParentFont := true;
    Result.Visible := true;
    Result.Enabled := true;
    Result.MaxLength := 3;
    Result.Alignment := taCenter;
    Result.OnKeyDown := OnTripletEditKeyDown;
    Result.OnKeyUp := OnTripletEditKeyUp;
    Result.Tag := WhichTriplet;
    Result.WantReturns := false;
    Result.WantTabs := false;
    Result.WordWrap := false;
    Result.OnChange := OnTripletEditChange;
    Result.Align := alLeft;
(*
    Result := TMemo.Create(Self);
    Result.Parent := FPanel;
    Result.Width := 150;
    Result.BorderStyle := bsNone;
    if (WhichTriplet = 0) then
        Result.TabOrder := 0
    else
        Result.TabStop := False;
    Result.Text := '';
    Result.Left := 1+154*WhichTriplet;
    Result.ParentFont := true;
    Result.Visible := true;
    Result.Enabled := true;
    Result.MaxLength := 3;
    Result.Alignment := taCenter;
    Result.OnKeyDown := OnTripletEditKeyDown;
    Result.OnKeyUp := OnTripletEditKeyUp;
    Result.Tag := WhichTriplet;
    Result.WantReturns := false;
    Result.WantTabs := false;
    Result.OnChange := OnTripletEditChange;
    Result.Align := alLeft;*)
end;

function CEditIPAddr.CreateDotLabel(WhichDot : Byte) : TLabel;
begin
(*    Result := TLabel.Create(Self);
    Result.Parent := FPanel;
    Result.Width := 3;
    Result.Caption := '.';
    Result.ParentFont := true;
    Result.Left := 22+153*WhichDot;
    Result.Visible := true;
    Result.Enabled := true;
    Result.Align := alLeft;
    Result.OnClick := OnDotLabelClick;
    Result.Tag := WhichDot;*)

    Result := TLabel.Create(Self);
    Result.Parent := FPanel;
    Result.Width := 3;
    Result.Caption := '.';
    Result.ParentFont := true;
    Result.Left := 22+24*WhichDot;
    Result.Visible := true;
    Result.Enabled := true;
    Result.Align := alLeft;
    Result.OnClick := OnDotLabelClick;
    Result.Tag := WhichDot;
end;

function CEditIPAddr.getText : String;
var
    i : Byte;
    isBlank : Boolean;
begin
    isBlank := true;
    for i := 0 to 3 do
        if (FTriplets[i].Text <> '') then
            isBlank := false;
    if isBlank then begin
        Result := '';
        exit;
    end;
    for i := 0 to 3 do
        if (FTriplets[i].Text = '') then
            FTriplets[i].Text := '0'
        else if (not FAcceptOctal) then
            FTriplets[i].Text := IntToStr(StrToInt(FTriplets[i].Text));
    Result := FTriplets[0].Text + '.' + FTriplets[1].Text + '.' + FTriplets[2].Text +
              '.' + FTriplets[3].Text;
end;

procedure CEditIPAddr.setText(aIPAddr : String);
var
    i : byte;
begin
    if (aIPAddr = '') then begin
        for i := 0 to 3 do
            FTriplets[i].Text := '';
        exit;
    end;
    try
        IP_ADDRESS_Validate(aIPAddr);
    except
        on E : Exception do
            assert(false, 'Bad IP address string given to component');
    end;
    for i := 0 to 3 do
        FTriplets[i].Text := IntToStr(IP_ADDRESS_TripletAsNumber(aIPAddr, i+1));
    FTriplets[0].SelectAll;
end;

procedure CEditIPAddr.OnTripletEditKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
const
    DOT = 190;
var
    WhichTriplet : Byte;
    aText : String;
begin
    if (Key = VK_SHIFT) or (Key = VK_CONTROL) or
       (Key = VK_LSHIFT) or (Key = VK_RSHIFT) or
       (Key = VK_LCONTROL) or (Key = VK_RCONTROL) then
        Exit;
    if ((Key >= VK_NUMPAD0) and (Key <= VK_NUMPAD9)) then
        Key := Ord('0') + Key - VK_NUMPAD0;
    if (Key = VK_DECIMAL) then
        Key := DOT;

    WhichTriplet := (Sender as TMemo).Tag;

    if ((Key = VK_RIGHT) and (WhichTriplet < 3) and
        ((Sender as TMemo).SelStart = Length((Sender as TMemo).Text))) then begin
        FTriplets[WhichTriplet+1].SetFocus;
        FTriplets[WhichTriplet+1].SelStart := 0;
    end
    else if ((Key = VK_LEFT) and (WhichTriplet > 0) and ((Sender as TMemo).SelStart = 0)) then begin
        FTriplets[WhichTriplet-1].SetFocus;
        FTriplets[WhichTriplet-1].SelStart := Length(FTriplets[WhichTriplet -1].Text);
    end
    else if ((Key = VK_BACK) and (WhichTriplet > 0) and ((Sender as TMemo).SelStart = 0)) then begin
        FTriplets[WhichTriplet-1].SetFocus;
        aText := FTriplets[WhichTriplet-1].Text;
        if (Length(aText) <> 0) then begin
            Delete(aText, Length(aText)-1, 1);
            FTriplets[WhichTriplet-1].Text := aText;
        end;
        FTriplets[WhichTriplet-1].SelStart := Length(FTriplets[WhichTriplet -1].Text);
    end
    else if (((Key = DOT) or (Key = VK_SPACE))and ((Sender as TMemo).Text <> '') and (WhichTriplet <3)) then begin
        FTriplets[WhichTriplet+1].SetFocus;
        FTriplets[WhichTriplet+1].SelectAll;
    end;
    if  (ssShift in Shift) or (ssAlt in Shift) or (ssCtrl in Shift) or (ssLeft in Shift) or
        (((Key < ord('0')) or (Key > ord('9'))) and (Key <> VK_BACK) and (Key <> VK_DELETE)) then
        (Sender as TMemo).ReadOnly := True;

end;

procedure CEditIPAddr.OnTripletEditKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
var
    i : byte;
begin
    for i := 0 to 3 do
        FTriplets[i].ReadOnly := False;
end;

procedure CEditIPAddr.OnSelfExit(Sender : TObject);
begin
    FTriplets[0].SelectAll;
end;

procedure CEditIPAddr.OnTripletEditChange(Sender : TObject);
var
    aText : String;
    WhichTriplet : Byte;
begin
    WhichTriplet := (Sender as TMemo).Tag;
    aText := FTriplets[WhichTriplet].Text;
    if ((aText <> '') and (StrToInt(aText) > 255)) then begin
        FTriplets[WhichTriplet].Text := '255';
        FTriplets[WhichTriplet].SelectAll;
        MessageDlg(aText + ' is not a valid entry. Please specify a value between 0 and 255 for this field.',mtError,[mbOK],-1);
        exit;
    end;
    if ((WhichTriplet < 3) and (Length(aText) = 3) and ((Sender as TMemo).SelStart = 3)) then begin
        FTriplets[WhichTriplet+1].SetFocus;
        FTriplets[WhichTriplet+1].SelStart := 0;
    end else
      Changed;
end;

procedure CEditIPAddr.WMSetFocus(var Message: TWMSetFocus);
begin
    FTriplets[0].SetFocus;
    FTriplets[0].SelectAll;
end;

procedure CEditIPAddr.OnPanelClick(Sender : TObject);
begin
    if (Sender = FPanel) then begin
        FTriplets[3].SetFocus;
        FTriplets[3].SelStart := Length(FTriplets[3].Text);
    end;
end;

procedure CEditIPAddr.OnDotLabelClick(Sender : TObject);
begin
    FTriplets[(Sender as TLabel).Tag].SetFocus;
    FTriplets[(Sender as TLabel).Tag].SelStart := Length(FTriplets[(Sender as TLabel).Tag].Text);
end;

procedure CEditIPAddr.ConstrainedResize(var MinWidth, MinHeight, MaxWidth,
  MaxHeight: Integer);
begin
  inherited;
  ResizeTriplets;
end;

end.
