(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: InsMachNode.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Nue  | Datei erstellt
| 11.04.2003  1.06  Nue  | ucDrumCorr added and dependent handling (only 0.5-1.5 allowed) default=1.0
| 29.01.2003  1.07  Nue  | Mask and TmmBaseMaskEdit added.
|=============================================================================*)
unit MachNodeComp;

interface

uses
  Windows, Messages, SysUtils, Classes,  mmLabel, mmEdit, mmList,
  mmComboBox, mmCheckBox, BaseGlobal,
  mmMaskEdit,UEditIPAddr;

type
//..............................................................................
  TUserChanges = ( ucNone, ucReadAllData, ucWriteAllData, ucSpindleFirst, ucSpindleLast, ucMaNr, ucMachineYMConfigRec, ucNetType, ucDrumCorr, ucNoUpdate);
//..............................................................................
  TChangedItemsByUser = set of TUserChanges;
//..............................................................................
  TOnValueChange = procedure( aItme : TUserChanges; aChanged : boolean ) of object;
//..............................................................................
  TOnFocusChange = procedure( aSender : TUserChanges ) of object;
//..............................................................................
//..............................................................................
  INavChange = interface
    procedure UpdateComponent ;
    function  CheckUserValue:boolean; // Anweisung an Komponente, die Eingabefelder zu pruefen.
                                      // Result = false wenn zwingende Eingaben noch nicht gemacht wurden
  end;
//..............................................................................
  TAdminMachNode = class ( TComponent)
  private
    mComponentList : TmmList;
    fChangedItems  : TChangedItemsByUser;
    fSavedAfterChange : Boolean;
    fNewMachineInserted : Boolean;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
    constructor Create ( aOwner : TComponent );override;
    destructor  Destroy;override;
    procedure   Add ( aComponent : INavChange );
    function    CheckUserValues:boolean; // Ueberprueft die User Eingaben jeder Komponente
    procedure   UpdateAllComponents;
    procedure   ItemChanged ( aChanged : TUserChanges );
//    procedure   ItemFocusChanged ( aChanged : TUserChanges );
    property    ChangedItems  : TChangedItemsByUser read fChangedItems;
    property    SavedAfterChange  : Boolean read fSavedAfterChange write fSavedAfterChange;
    property    NewMachineInserted  : Boolean read fNewMachineInserted write fNewMachineInserted;
  published

  end;

//..............................................................................
  TBaseCheckBox = class ( TmmCheckBox, INavChange )
  private
    FAdminMachNode: TAdminMachNode;
    fOnMachNodeChanged: TNotifyEvent;
    procedure UpdateComponent;
    function  CheckUserValue:boolean;
    procedure SetAdminMachNode(const Value: TAdminMachNode);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
  published
    property AdminMachNode : TAdminMachNode read FAdminMachNode write SetAdminMachNode;
    property OnMachNodeChanged : TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
  end;
//..............................................................................
  TBaseEdit = class ( TmmEdit, INavChange )
  private
    FAdminMachNode: TAdminMachNode;
    fOnMachNodeChanged: TNotifyEvent;
    procedure UpdateComponent;
    function  CheckUserValue:boolean;
    procedure SetAdminMachNode(const Value: TAdminMachNode);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
  published
    property AdminMachNode : TAdminMachNode read FAdminMachNode write SetAdminMachNode;
    property OnMachNodeChanged : TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
  end;
//..............................................................................
  TBaseMaskEdit = class ( TmmMaskEdit, INavChange )
  private
    FAdminMachNode: TAdminMachNode;
    fOnMachNodeChanged: TNotifyEvent;
    procedure UpdateComponent;
    function  CheckUserValue:boolean;
    procedure SetAdminMachNode(const Value: TAdminMachNode);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
  published
    property AdminMachNode : TAdminMachNode read FAdminMachNode write SetAdminMachNode;
    property OnMachNodeChanged : TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
  end;
//..............................................................................
  TBaseComboBox = class ( TmmComboBox, INavChange )
  private
    FAdminMachNode: TAdminMachNode;
    fOnMachNodeChanged: TNotifyEvent;
    procedure UpdateComponent;
    function  CheckUserValue:boolean;
    procedure SetAdminMachNode(const Value: TAdminMachNode);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
  published
    property AdminMachNode : TAdminMachNode read FAdminMachNode write SetAdminMachNode;
    property OnMachNodeChanged : TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
  end;
//..............................................................................
  TBaseEditIPAddr = class ( CEditIPAddr, INavChange )
  private
    FAdminMachNode: TAdminMachNode;
    fOnChange: TNotifyEvent;
    fOnMachNodeChanged: TNotifyEvent;
    procedure UpdateComponent;
    function  CheckUserValue:boolean;
    procedure SetAdminMachNode(const Value: TAdminMachNode);
    procedure WMKeyDown(var Message: TWMKeyDown); message WM_KEYDOWN;
  protected
    procedure Changed; override; //dynamic;
    procedure Notification(AComponent: TComponent; Operation: TOperation);override;
  public
  published
    property AdminMachNode : TAdminMachNode read FAdminMachNode write SetAdminMachNode;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMachNodeChanged : TNotifyEvent read fOnMachNodeChanged write fOnMachNodeChanged;
  end;
//..............................................................................
//..............................................................................
  procedure Register;
//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//------------------------------------------------------------------------------
  procedure Register;
  begin
    RegisterComponents('MaConfig', [TAdminMachNode, TBaseEdit, TBaseMaskEdit, TBaseComboBox, TBaseCheckBox]);
  end;

//------------------------------------------------------------------------------

{TAdminMachNode}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  constructor TAdminMachNode.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    mComponentList := TmmList.Create;
//    fFirst := true;
//    fLast  := false;
    fChangedItems := [];
    fSavedAfterChange := True;
    fNewMachineInserted := False;
  end;

 //------------------------------------------------------------------------------

  destructor TAdminMachNode.Destroy;
  begin
    mComponentList.Free;
    inherited Destroy;
  end;

//------------------------------------------------------------------------------
  procedure TAdminMachNode.Add ( aComponent : INavChange );
  begin

    if Assigned ( mComponentList ) and not(csDesigning in ComponentState) then
      mComponentList.Add ( pointer ( aComponent ) );
  end;

//------------------------------------------------------------------------------
  function TAdminMachNode.CheckUserValues : boolean;
  var  x : integer;
    function p ( interf : INavChange ):boolean;
    begin
      Result := interf.CheckUserValue;
    end;
  begin
    Result := true;
    if Assigned ( mComponentList ) then
      for x := 0 to mComponentList.Count - 1 do begin
        Result := p(  INavChange ( mComponentList.Items[x] ) );
//        if not Result then
//          Break;
      end;
  end;

  //------------------------------------------------------------------------------
  procedure TAdminMachNode.UpdateAllComponents;
  var  x : integer;
    procedure p ( interf : INavChange );
    begin
      if mComponentList.Count > 0 then
        interf.UpdateComponent;
    end;
  begin
    if Assigned ( mComponentList ) then
      for x := 0 to mComponentList.Count - 1 do begin
       p(  INavChange ( mComponentList.Items[x] ) );
      end;
    fChangedItems := [];
  end;
//------------------------------------------------------------------------------

  procedure TAdminMachNode.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
//      if Assigned ( fQuery ) and ( AComponent = fQuery ) then
//        fQuery := nil;
    end;
  end;
//------------------------------------------------------------------------------

  procedure   TAdminMachNode.ItemChanged ( aChanged : TUserChanges );
  begin
    if (aChanged<>ucNoUpdate) then begin
      Include (  fChangedItems, aChanged );
      UpdateAllComponents;  //Nue 11.12.00
    end;
    if Not (aChanged in [ucNone, ucReadAllData, ucWriteAllData]) then
      fSavedAfterChange := False;
  end;
//------------------------------------------------------------------------------

{TBaseCheckBox}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function TBaseCheckBox.CheckUserValue: boolean;
begin

end;
//------------------------------------------------------------------------------

procedure TBaseCheckBox.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
      if Assigned ( FAdminMachNode ) and ( AComponent = FAdminMachNode ) then
        FAdminMachNode := nil;
    end;
  end;
//------------------------------------------------------------------------------

procedure TBaseCheckBox.SetAdminMachNode(const Value: TAdminMachNode);
begin
  if Value <> FAdminMachNode then begin
    FAdminMachNode := Value;
    if Assigned (FAdminMachNode) then
      FAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TBaseCheckBox.UpdateComponent;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;

{TBaseEdit}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function TBaseEdit.CheckUserValue: boolean;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TBaseEdit.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
      if Assigned ( FAdminMachNode ) and ( AComponent = FAdminMachNode ) then
        FAdminMachNode := nil;
    end;
  end;
//------------------------------------------------------------------------------

procedure TBaseEdit.SetAdminMachNode(const Value: TAdminMachNode);
begin
  if Value <> FAdminMachNode then begin
    FAdminMachNode := Value;
    if Assigned (FAdminMachNode) then
      FAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TBaseEdit.UpdateComponent;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;

{TBaseMaskEdit}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function TBaseMaskEdit.CheckUserValue: boolean;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TBaseMaskEdit.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
      if Assigned ( FAdminMachNode ) and ( AComponent = FAdminMachNode ) then
        FAdminMachNode := nil;
    end;
  end;
//------------------------------------------------------------------------------

procedure TBaseMaskEdit.SetAdminMachNode(const Value: TAdminMachNode);
begin
  if Value <> FAdminMachNode then begin
    FAdminMachNode := Value;
    if Assigned (FAdminMachNode) then
      FAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TBaseMaskEdit.UpdateComponent;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;

{TBaseComboBox}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function TBaseComboBox.CheckUserValue: boolean;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TBaseComboBox.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
      if Assigned ( FAdminMachNode ) and ( AComponent = FAdminMachNode ) then
        FAdminMachNode := nil;
    end;
  end;
//------------------------------------------------------------------------------

procedure TBaseComboBox.SetAdminMachNode(const Value: TAdminMachNode);
begin
  if Value <> FAdminMachNode then begin
    FAdminMachNode := Value;
    if Assigned (FAdminMachNode) then
      FAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TBaseComboBox.UpdateComponent;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;

procedure TBaseEditIPAddr.Changed;
begin
  inherited Changed;
  if Assigned(fOnChange) then fOnChange(Self);
end;

{TBaseEditIPAddr}
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
  function TBaseEditIPAddr.CheckUserValue: boolean;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;
//------------------------------------------------------------------------------

procedure TBaseEditIPAddr.Notification(AComponent: TComponent; Operation: TOperation);
  begin
    inherited Notification ( AComponent, Operation );
    if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
      if Assigned ( FAdminMachNode ) and ( AComponent = FAdminMachNode ) then
        FAdminMachNode := nil;
    end;
  end;
//------------------------------------------------------------------------------

procedure TBaseEditIPAddr.SetAdminMachNode(const Value: TAdminMachNode);
begin
  if Value <> FAdminMachNode then begin
    FAdminMachNode := Value;
    if Assigned (FAdminMachNode) then
      FAdminMachNode.Add(Self);
  end;
end;
//------------------------------------------------------------------------------

procedure TBaseEditIPAddr.UpdateComponent;
begin
  if Assigned(fOnMachNodeChanged) then
    fOnMachNodeChanged(Self);
end;

procedure TBaseEditIPAddr.WMKeyDown(var Message: TWMKeyDown);
begin
  inherited;
end;

end.
