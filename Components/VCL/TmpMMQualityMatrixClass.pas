(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MMQualityMatrixClass.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Anzeige von "Fault Classification"
|                 Wird in ClearerAssistiant und ClassDesigner verwendet
| Info..........: Ableitung von TQualityMatrix
|                 Diese Abhandlung wird sp�ter von Kr. in die TQualityMatrix
|                 implementiert.
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 17.11.2003  1.00  SDo  | Initial Release
| 02.06.2004  1.00  SDo  | Neue Func. PutYMSettings und Property  HasSiroData
| 17.11.2004  1.00  Wss  | Anpassungen an Zenit (mtColor entf�llt)
| 21.01.2005  5.00  SDo  | Anpassung an neue YM-Klassierungen (mstZenitF)
|                        |  -> mstSiroBD wird nicht mehr offiziell gebraucht
| 26.04.2005  5.00  Wss  | Bugfix in zusammenhang mit QuickReport
                           - Properties MatrixType und MatrixSubType entfernt
                           - daf�r Propertymethoden �berschrieben
                           - Abfangen von mYMFaultClassification wenn diese Nil ist
| 30.05.2005  5.00  SDo  | - Anpassung in SetMatrixType(); fType setzen
|                        | - Anpassung Bezeichnung Klassierung fuer Siro
| 18.01.2008  5.00  Roda | PrepareFaultClassification: Klassen -C1-1 und -C2-2 vertauscht

|=============================================================================*)
unit MMQualityMatrixClass;

//Mit ModelMaker bearbeiten!!

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  QualityMatrixBase, QualityMatrix, QualityMatrixDesc, QualityMatrixDef,
  MMList, YMParaDef;

const
  cText = 'MillMaster MMQMatrix Vers. 5.0';

type

  TNotifyEvent = procedure(Sender: TObject) of object;
  TGroupFields = set of 1..cTotSLTFields;

  TSubFields = array of Integer;

  TFaultClassRec = record
    ClassName: string;
    GroupFields: TGroupFields;
    SubFields: TSubFields;
    ShowLine: Boolean;
  end;

  pFaultClassRec = ^TFaultClassRec;

  TYMFaultClassification = class(TmmList)
  end;

  TMMQualityMatrix = class(TQualityMatrix)
  private
    FFontColorGroupSubField: TColor;
    FShowGroupSubFieldNr: Boolean;
    FShowYarnFaultClass: Boolean;
    fType: TMatrixType;
    fVersion: string;
    fYarnFaultClassFont: TFont;
    mYMFaultClassification: TYMFaultClassification;
    procedure ClearYMFaultClassificationList;
    function GetFontColorGroupSubField: TColor;
    function GetYarnFaultClassFont: TFont;
    procedure PrepareFaultClassification;
    procedure SetFontColorGroupSubField(const Value: TColor);
    procedure SetShowGroupSubFieldNr(const Value: Boolean);
    procedure SetShowYarnFaultClass(const Value: Boolean);
    procedure SetText(const Value: string);
    procedure SetYarnFaultClassFont(const Value: TFont);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure SetMatrixSubType(aValue: TMatrixSubType); override;
    procedure SetMatrixType(const aValue: TMatrixType); override;
    procedure WndProc(var Message: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function GetFaultClassFieldNr(aFieldNr: integer): Word;
    function GetFaultClassName(aFieldNr: integer): string;
    procedure Paint; override;
  published
    property FontColorGroupSubField: TColor read GetFontColorGroupSubField
      write SetFontColorGroupSubField;
    property ShowGroupSubFieldNr: Boolean read FShowGroupSubFieldNr write
      SetShowGroupSubFieldNr;
    property ShowYarnFaultClass: Boolean read FShowYarnFaultClass write
      SetShowYarnFaultClass;
    property Version: string read fVersion write SetText;
    property YarnFaultClassFont: TFont read GetYarnFaultClassFont write
      SetYarnFaultClassFont;
  end;

//wss  TMMList = class(TObject)
//  end;

implementation

{$R MMQMatrix.dcr}
uses RzCSIntf;

{ TMMQualityMatrix }
{
******************************* TMMQualityMatrix *******************************
}
//:-------------------------------------------------------------------
constructor TMMQualityMatrix.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  FShowYarnFaultClass := False;
  mYMFaultClassification := TYMFaultClassification.Create;

  //  fYarnFaultClassFont := Font.Create; //Font nur fuer Klassenbezeichnung
  fYarnFaultClassFont       := TFont.Create;
  fYarnFaultClassFont.Name  := 'Arial';
  fYarnFaultClassFont.Color := clSilver;
  fYarnFaultClassFont.Style := [fsBold];
  fYarnFaultClassFont.Size  := 10;

  FFontColorGroupSubField := clRed;

  Width := 460;
  Height := 380;

  fVersion := cText;

  PrepareFaultClassification;
end;

//:-------------------------------------------------------------------
destructor TMMQualityMatrix.Destroy;
begin
  ClearYMFaultClassificationList;
  mYMFaultClassification.Free;

  fYarnFaultClassFont.Free;
  fYarnFaultClassFont := nil;
  inherited Destroy;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.ClearYMFaultClassificationList;
var
  xFaultClassRec: pFaultClassRec;
  xSubField: TSubFields;
begin
  while mYMFaultClassification.Count > 0 do begin
    xFaultClassRec := mYMFaultClassification.Items[0];
    xSubField      := xFaultClassRec.SubFields;
    SetLength(xSubField, 0);
    Dispose(xFaultClassRec);
    mYMFaultClassification.Delete(0);
  end;
end;

//:-------------------------------------------------------------------
function TMMQualityMatrix.GetFaultClassFieldNr(aFieldNr: integer): Word;
var
  x, i: Integer;
  xFaultClassRec: pFaultClassRec;
begin
  Result := 0;
  if Assigned(mYMFaultClassification) then
    for x:=0 to mYMFaultClassification.Count-1 do begin
      xFaultClassRec := mYMFaultClassification.Items[x];
      for i := Low(xFaultClassRec.SubFields) to High(xFaultClassRec.SubFields) do
        if aFieldNr = xFaultClassRec.SubFields[i] then begin
          Result := i + 1;
          break;
        end;
    end;
end;

//:-------------------------------------------------------------------
function TMMQualityMatrix.GetFaultClassName(aFieldNr: integer): string;
var
  x, i: Integer;
  xFaultClassRec: pFaultClassRec;
begin
  if Assigned(mYMFaultClassification) then
    for x:=0 to mYMFaultClassification.Count-1 do begin
      xFaultClassRec := mYMFaultClassification.Items[x];

      for i := Low(xFaultClassRec.SubFields) to High(xFaultClassRec.SubFields) do
        if aFieldNr = xFaultClassRec.SubFields[i] then begin
          Result := xFaultClassRec.ClassName;
          exit;
        end;
    end;
end;

//:-------------------------------------------------------------------
function TMMQualityMatrix.GetFontColorGroupSubField: TColor;
begin
  Result := FFontColorGroupSubField;
end;

//:-------------------------------------------------------------------
function TMMQualityMatrix.GetYarnFaultClassFont: TFont;
begin
  Result := fYarnFaultClassFont;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.Notification(AComponent: TComponent; Operation:
  TOperation);
begin
  inherited;
  if Operation = opInsert then
    fVersion := cText;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.Paint;
var
  i, x, y: Integer;
  xRect: TRect;
  xText: string;
  xFaultClassRec: pFaultClassRec;
  xGroupFields: TGroupFields;
  xPos, yPos: Integer;
  yPosMin, yPosMax, xPosMin, xPosMax: Integer;
  xLineFrom, xLineTo, yLineFrom, yLineTo: Integer;
  xTextWidth, xTextHight: Integer;
  xCount: Integer;
  xShowHorLine, xShowVertLine: Boolean;
begin
  inherited Paint;

  if Enabled and FShowYarnFaultClass then begin
     //Max. Font-Groesse ermitteln
    xRect := Field[1];
    Canvas.Font        := fYarnFaultClassFont;
    Canvas.Pen.Color   := clGray;
    Canvas.Pen.Width   := 3;
    Canvas.Brush.Style := bsClear;

    //Line Pos.
    xLineFrom := 0;
    xLineTo   := 0;
    yLineFrom := 0;
    yLineTo   := 0;

    if Canvas.TextHeight('A') >= (xRect.Bottom - xRect.Top) then
      Canvas.Font.Size := (xRect.Bottom - xRect.Top) - 6;

    case fType of
      mtSplice: xCount := cTotSLTFields;
      mtSiro:
        if MatrixSubType = mstZenitF then
          xCount := cTotColorFields
        else
          xCount := cTotSIROFields;
    else // mtShortLongThin
      xCount := cTotSLTFields;
    end;

     //Punkte (x, y) fuer Ausgabetext ermitteln
    for i:=0 to mYMFaultClassification.Count-1 do begin

      xFaultClassRec := mYMFaultClassification.Items[i];
      xGroupFields   := xFaultClassRec.GroupFields;

      yPosMin := 0;
      yPosMax := 0;

      xPosMin := 0;
      xPosMax := 0;

      for x:=0 to xCount-1 do
        if x in xGroupFields then begin
          xRect := Field[x];

          //Gruppen-Feld Groesse ermitteln
          if (yPosMin = 0) then yPosMin := xRect.Bottom;
          if (yPosMax = 0) then
            yPosMax := xRect.Top
          else if yPosMax > xRect.Top then
            yPosMax := xRect.Top;

          if (xPosMin = 0) then xPosMin := xRect.Left;
          if (xPosMax = 0) then
            xPosMax := xRect.Right
          else if xPosMax < xRect.Right then
            xPosMax := xRect.Right;

                //Line Pos.
          xLineFrom := xPosMin;
          xLineTo := xPosMax;

          yLineFrom := yPosMin;
          yLineTo := yPosMax;
        end;

         //SubFields
      if FShowGroupSubFieldNr then begin
        Canvas.Font.Size := fYarnFaultClassFont.Size - 3;
        Canvas.Font.Style := [];
        Canvas.Font.Color := FFontColorGroupSubField;
        for y := low(xFaultClassRec.SubFields) to high(xFaultClassRec.SubFields) do begin
          xRect := Field[xFaultClassRec.SubFields[y]];
          if fType = mtSiro then
            xText := Format('   %d', [y + 1])
          else
            xText := Format('%d', [y + 1]);
          Canvas.TextOut(xRect.Left + 8, xRect.Bottom - Abs(Canvas.Font.Height) - 2, xText);
        end;
      end;

         //Font
      Canvas.Font.Size := fYarnFaultClassFont.Size;
      Canvas.Font.Style := fYarnFaultClassFont.Style;
      Canvas.Font.Color := fYarnFaultClassFont.Color;

         //Text positionieren und ausgeben
      xText := xFaultClassRec.ClassName;
      xTextWidth := Canvas.TextWidth(xText);
      xTextHight := Canvas.TextHeight(xText);

         //Gruppenfeld-Mitte
      yPos := yPosMin - ((yPosMin - yPosMax) div 2);
      xPos := xPosMax - ((xPosMax - xPosMin) div 2);

         //Positionierung mit Text-Korrektur
      xPos := xPos - (xTextWidth div 2) - 2;
      yPos := yPos - (xTextHight div 2) - 1;

      if (Pos('DI', xText) >= 1) or (Pos('BI', xText) >= 1) then xPos := xPos - 2; //Text etwas verschieben, weil I auf Linie

         //TextAusgabe
      Canvas.TextOut(xPos, yPos, xText);

         //Linien zeichnen
      if (fType = mtShortLongThin) or (fType = mtSplice) then
        if xFaultClassRec.ShowLine then begin
              {
                //Horizontal
                Canvas.MoveTo( xLineFrom  , yLineTo - 1  );
                Canvas.LineTo( xLineTo  , yLineTo -1 );

                //Verikal
                Canvas.MoveTo( xLineTo + 1, yLineTo+1);
                Canvas.LineTo( xLineTo + 1, yLineFrom);
              }

                //Linien Pos. korrigieren (leider; Spez.Behandlung) !!!!
          xPos := 0;

          if Pos('N', xFaultClassRec.ClassName) > 0 then
            xPos := 1;

          if Pos('D', xFaultClassRec.ClassName) > 0 then
            xPos := 1;

          if Pos('-D0', xFaultClassRec.ClassName) > 0 then
            xPos := 0;

          if Pos('D00', xFaultClassRec.ClassName) > 0 then
            xPos := 0;

          yPos := -1;
          if Pos('C0', xFaultClassRec.ClassName) > 0 then
            yPos := -2;

          if Pos('F00', xFaultClassRec.ClassName) > 0 then
            yPos := -2;

          if Pos('C00', xFaultClassRec.ClassName) > 0 then
            yPos := -1;

          xShowHorLine := True;
          xShowVertLine := True;

          if Pos('N4', xFaultClassRec.ClassName) > 0 then
            xShowHorLine := False;

          if Pos('-C0', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;
          if Pos('-D0', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;
          if Pos('H0', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;
          if Pos('I0', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;
          if Pos('C0', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;
          if Pos('C00', xFaultClassRec.ClassName) > 0 then xShowHorLine := True;
          if Pos('F00', xFaultClassRec.ClassName) > 0 then xShowHorLine := False;

          if Pos('N', xFaultClassRec.ClassName) > 0 then xShowVertLine := False;
          if Pos('-D1', xFaultClassRec.ClassName) > 0 then xShowVertLine := False;
          if Pos('-D2', xFaultClassRec.ClassName) > 0 then xShowVertLine := False;

          if xShowHorLine then begin
                   //Horizontal
            Canvas.MoveTo(xLineFrom, yLineTo + yPos);
            Canvas.LineTo(xLineTo, yLineTo + yPos);

                   //Bottom line
            if Pos('N1', xFaultClassRec.ClassName) > 0 then begin
              yPos := (xRect.Bottom - xRect.Top) * 2 + 1;
              Canvas.MoveTo(xLineFrom, yLineTo + yPos);
              Canvas.LineTo(xLineTo, yLineTo + yPos);
            end;
          end;

                //Verikal
          if xShowVertLine then begin
            Canvas.MoveTo(xLineTo + xPos, yLineTo);
            Canvas.LineTo(xLineTo + xPos, yLineFrom);
          end;
        end;
    end; //END for i:= 0
  end; //END if Enabled and FShowYarnFaultClass then
  //Font := mOrgFont;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.PrepareFaultClassification;
var
  xFaultClassRec: pFaultClassRec;
  xSubField: TSubFields;
  x, n, i, xStartField: Integer;
  xClassNameFragment, xClassName: string;
begin
  if Assigned(mYMFaultClassification) then begin
    ClearYMFaultClassificationList;

    //  if (fType = mtColor) or (fType = mtSiro) then begin
    if fType = mtSiro then begin
      if MatrixSubType = mstNone then
        exit;

      xStartField := 0;
      for i := 0 to 1 do begin //Dark -> i=0 ; Bright -> i=1
        //Col
        for x := 0 to 3 do begin

          case x of
            0: xClassNameFragment := 'S';
            1: xClassNameFragment := 'I';
            2: xClassNameFragment := 'R';
            3: xClassNameFragment := 'O';
          end;

          //Row
          for n := 0 to 3 do begin
            new(xFaultClassRec);

            if (MatrixSubType = mstZenitF) then begin
              if i = 0 then
                xClassName := 'D' + xClassNameFragment // Top Matix  -> Dark
              else
                xClassName := 'B' + xClassNameFragment; // Bottom Matix -> Bright

            end else
              xClassName := 'D' + xClassNameFragment;

            if i = 0 then
              xFaultClassRec.ClassName := Format('%s%d', [xClassName, n + 1])
            else
              xFaultClassRec.ClassName := Format('%s%d', [xClassName, 4 - n]);

            SetLength(xSubField, 4);
            xFaultClassRec.GroupFields := [];
            Include(xFaultClassRec.GroupFields, xStartField);
            Include(xFaultClassRec.GroupFields, xStartField + 1);
            Include(xFaultClassRec.GroupFields, xStartField + 8);
            Include(xFaultClassRec.GroupFields, xStartField + 9);

            xSubField[0] := xStartField;
            xSubField[1] := xStartField + 1;
            xSubField[2] := xStartField + 8;
            xSubField[3] := xStartField + 9;
            xFaultClassRec.SubFields := xSubField;

            xFaultClassRec.ShowLine := False;
            mYMFaultClassification.Add(xFaultClassRec);
            inc(xStartField, 2);
          end; //End Row

          inc(xStartField, 8); // + 8 Felder
        end; //END Col

        if (MatrixSubType <> mstZenitF) then break;
             //ab hier Bright-Bereich oder Spiegelung fuer Color
        xStartField := 64; //Felder ab 64 - 128
      end; //END for
      Exit;
    end; //END Siro

    if fType = mtSplice then exit;

      //nur Matrixtype mtShortLongThin
    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'N1';
    xFaultClassRec.GroupFields := [0, 1];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 0; //Wert in Feld 0
    xSubField[1] := 1; //Wert in Feld 1
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'N2';
    xFaultClassRec.GroupFields := [2, 3];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 2; //Wert in Feld 2
    xSubField[1] := 3; //Wert in Feld 3
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'N3';
    xFaultClassRec.GroupFields := [4, 5];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 4;
    xSubField[1] := 5;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'N4';
    xFaultClassRec.GroupFields := [6, 7];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 6;
    xSubField[1] := 7;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'A1';
    xFaultClassRec.GroupFields := [8, 9];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 8;
    xSubField[1] := 9;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'A2';
    xFaultClassRec.GroupFields := [10, 11];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 10;
    xSubField[1] := 11;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'A3';
    xFaultClassRec.GroupFields := [12, 13];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 12;
    xSubField[1] := 13;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'A4';
    xFaultClassRec.GroupFields := [14, 15];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 14;
    xSubField[1] := 15;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'B1';
    xFaultClassRec.GroupFields := [16, 17, 24, 25];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 16;
    xSubField[1] := 17;
    xSubField[2] := 24;
    xSubField[3] := 25;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'B2';
    xFaultClassRec.GroupFields := [18, 19, 26, 27];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 18;
    xSubField[1] := 19;
    xSubField[2] := 26;
    xSubField[3] := 27;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'B3';
    xFaultClassRec.GroupFields := [29, 20, 28, 21];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 20;
    xSubField[1] := 21;
    xSubField[2] := 28;
    xSubField[3] := 29;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'B4';
    xFaultClassRec.GroupFields := [22, 23, 30, 31];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 22;
    xSubField[1] := 23;
    xSubField[2] := 30;
    xSubField[3] := 31;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C1';
    xFaultClassRec.GroupFields := [32, 33, 40, 41];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 32;
    xSubField[1] := 33;
    xSubField[2] := 40;
    xSubField[3] := 41;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C2';
    xFaultClassRec.GroupFields := [34, 35, 42, 43];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 34;
    xSubField[1] := 35;
    xSubField[2] := 42;
    xSubField[3] := 43;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C3';
    xFaultClassRec.GroupFields := [36, 37, 44, 45];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 36;
    xSubField[1] := 37;
    xSubField[2] := 44;
    xSubField[3] := 45;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C4';
    xFaultClassRec.GroupFields := [38, 39, 46, 47];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 38;
    xSubField[1] := 39;
    xSubField[2] := 46;
    xSubField[3] := 47;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D1';
    xFaultClassRec.GroupFields := [48, 49];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 48;
    xSubField[1] := 49;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D2';
    xFaultClassRec.GroupFields := [50, 51];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 50;
    xSubField[1] := 51;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D3';
    xFaultClassRec.GroupFields := [52, 53];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 52;
    xSubField[1] := 53;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D4';
    xFaultClassRec.GroupFields := [54, 55];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 54;
    xSubField[1] := 55;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'E';
    xFaultClassRec.GroupFields := [56, 57, 58, 59, 60, 61, 62, 63];
      //SubFelder
    x := 8;
    SetLength(xSubField, x);
    xSubField[0] := 56;
    xSubField[1] := 57;
    xSubField[2] := 58;
    xSubField[3] := 59;
    xSubField[4] := 60;
    xSubField[5] := 61;
    xSubField[6] := 62;
    xSubField[7] := 63;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-C2';
    xFaultClassRec.GroupFields := [64];
      //SubFelder
    x := 1;
    SetLength(xSubField, x);
    xSubField[0] := 64;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-C1';
    xFaultClassRec.GroupFields := [65, 66];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    // Korrektur Roda 18.01.2008
    xSubField[0] := 66;
    xSubField[1] := 65;
    //xSubField[0] := 65;
    //xSubField[1] := 66;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-C0';
    xFaultClassRec.GroupFields := [67];
      //SubFelder
    x := 1;
    SetLength(xSubField, x);
    xSubField[0] := 67;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C00';
    xFaultClassRec.GroupFields := [68];
      //SubFelder
    x := 1;
    SetLength(xSubField, x);
    xSubField[0] := 68;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'C0';
    xFaultClassRec.GroupFields := [69, 70, 71];
      //SubFelder
    x := 3;
    SetLength(xSubField, x);
    xSubField[0] := 69;
    xSubField[1] := 70;
    xSubField[2] := 71;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-D2';
    xFaultClassRec.GroupFields := [72, 80, 88];
      //SubFelder
    x := 3;
    SetLength(xSubField, x);
    xSubField[0] := 72;
    xSubField[1] := 80;
    xSubField[2] := 88;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-D1';
    xFaultClassRec.GroupFields := [73, 81, 89, 74, 82, 90];
      //SubFelder
    x := 6;
    SetLength(xSubField, x);
    xSubField[0] := 74;
    xSubField[1] := 73;
    xSubField[2] := 82;
    xSubField[3] := 81;
    xSubField[4] := 90;
    xSubField[5] := 89;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := '-D0';
    xFaultClassRec.GroupFields := [75, 83, 91];
      //SubFelder
    x := 3;
    SetLength(xSubField, x);
    xSubField[0] := 75;
    xSubField[1] := 83;
    xSubField[2] := 91;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D00';
    xFaultClassRec.GroupFields := [76, 84];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 76;
    xSubField[1] := 84;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'D0';
    xFaultClassRec.GroupFields := [77, 85, 78, 86, 79, 87];
      //SubFelder
    x := 6;
    SetLength(xSubField, x);
    xSubField[0] := 77;
    xSubField[1] := 78;
    xSubField[2] := 79;
    xSubField[3] := 85;
    xSubField[4] := 86;
    xSubField[5] := 87;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'H2';
    xFaultClassRec.GroupFields := [96, 104];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 96;
    xSubField[1] := 104;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'H1';
    xFaultClassRec.GroupFields := [97, 105, 98, 106];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 98;
    xSubField[1] := 97;
    xSubField[2] := 106;
    xSubField[3] := 105;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'H0';
    xFaultClassRec.GroupFields := [99, 107];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 99;
    xSubField[1] := 107;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'F00';
    xFaultClassRec.GroupFields := [92, 100, 108];
      //SubFelder
    x := 3;
    SetLength(xSubField, x);
    xSubField[0] := 92;
    xSubField[1] := 100;
    xSubField[2] := 108;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := True;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'F';
    xFaultClassRec.GroupFields := [93, 101, 93, 109, 94, 102, 110, 95, 103, 111];
      //SubFelder
    x := 9;
    SetLength(xSubField, x);
    xSubField[0] := 93;
    xSubField[1] := 94;
    xSubField[2] := 95;
    xSubField[3] := 101;
    xSubField[4] := 102;
    xSubField[5] := 103;
    xSubField[6] := 109;
    xSubField[7] := 110;
    xSubField[8] := 111;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'I2';
    xFaultClassRec.GroupFields := [112, 120];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 112;
    xSubField[1] := 120;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'I1';
    xFaultClassRec.GroupFields := [113, 121, 114, 122];
      //SubFelder
    x := 4;
    SetLength(xSubField, x);
    xSubField[0] := 114;
    xSubField[1] := 113;
    xSubField[2] := 122;
    xSubField[3] := 121;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'I0';
    xFaultClassRec.GroupFields := [115, 123];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 115;
    xSubField[1] := 123;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'G00';
    xFaultClassRec.GroupFields := [116, 124];
      //SubFelder
    x := 2;
    SetLength(xSubField, x);
    xSubField[0] := 116;
    xSubField[1] := 124;
    xFaultClassRec.SubFields := xSubField;
    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);

    new(xFaultClassRec);
    xFaultClassRec.ClassName := 'G';
    xFaultClassRec.GroupFields := [117, 125, 118, 126, 119, 127];
      //SubFelder
    x := 6;
    SetLength(xSubField, x);
    xSubField[0] := 117;
    xSubField[1] := 118;
    xSubField[2] := 119;
    xSubField[3] := 125;
    xSubField[4] := 126;
    xSubField[5] := 127;
    xFaultClassRec.SubFields := xSubField;

    xFaultClassRec.ShowLine := False;
    mYMFaultClassification.Add(xFaultClassRec);
  end;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.SetFontColorGroupSubField(const Value: TColor);
begin
  FFontColorGroupSubField := Value;
  Invalidate;
end;

//:---------------------------------------------------------------------------
procedure TMMQualityMatrix.SetMatrixSubType(aValue: TMatrixSubType);
begin
  if MatrixSubType <> aValue then begin
    inherited SetMatrixSubType(aValue);
    PrepareFaultClassification;
    Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TMMQualityMatrix.SetMatrixType(const aValue: TMatrixType);
begin
  if MatrixType <> aValue then begin
    inherited SetMatrixType(aValue);
    fType :=  aValue;
    PrepareFaultClassification;
    Invalidate;
  end;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.SetShowGroupSubFieldNr(const Value: Boolean);
begin
  FShowGroupSubFieldNr := Value;
  Paint;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.SetShowYarnFaultClass(const Value: Boolean);
begin
  if FShowYarnFaultClass <> Value then begin
    FShowYarnFaultClass := Value;
    PrepareFaultClassification;
    Invalidate;
  end;

//   if FShowYarnFaultClass <> Value then
//      PrepareFaultClassification;
//   FShowYarnFaultClass := Value;
//
//  Paint;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.SetText(const Value: string);
begin
  fVersion := cText;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.SetYarnFaultClassFont(const Value: TFont);
begin
  if fYarnFaultClassFont <> Value then begin
    fYarnFaultClassFont.Assign(Value);

    if csDesigning in ComponentState then
      Paint;
  end;
end;

//:-------------------------------------------------------------------
procedure TMMQualityMatrix.WndProc(var Message: TMessage);
begin

  case Message.Msg of
    WM_LBUTTONUP,
      WM_LBUTTONDOWN: begin
        if ShowYarnFaultClass then
                          // if (MatrixMode = mmSelectSettings) or (MatrixMode = mmSelectSettings) then
          if (MatrixMode <> mmDisplayData) then begin
            Invalidate;
          end;
      end;
  end;

  inherited WndProc(Message);
end;

end.

