(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmNumCtrl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: Developed from Peter Worbis
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.08.2000  1.00  Wss | used in Floor on first call with empty trend report an
                          'Invalid floating point operation' occurs in FormatText method
| 04.12.2000  1.00  Wss | - value wasn't set after OnChange event of component
                          - KeyPress methode overworked
                          - function AsXXX now converts the text always -> OnChange problem
| 26.03.2001  1.00  Wss | Over worked: value will be checked only at
                            - Readout the value through Value property or AsXXX function
|=========================================================================================*)
unit mmNumCtrl;
interface
uses
 SysUtils,WinTypes,WinProcs,Messages,Classes,Graphics,Controls,Forms,Dialogs,
 StdCtrls,Menus;

type
 TNumericType = (ntGeneral,ntCurrency,ntPercentage);
 TMaskString  = string[25];

type
  {:----------------------------------------------------------------------------
   Masks object                                                           }
  TMasks = class (TPersistent)
  private
    fNegativeMask: TMaskString;
    fOnChange: TNotifyEvent;
    fPositiveMask: TMaskString;
    fZeroMask: TMaskString;
  protected
    procedure SetNegativeMask(Value: TMaskString);
    procedure SetPositiveMask(Value: TMaskString);
    procedure SetZeroMask(Value: TMaskString);
  public
    constructor Create;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
  published
    property NegativeMask: TMaskString read fNegativeMask write SetNegativeMask;
    property PositiveMask: TMaskString read fPositiveMask write SetPositiveMask;
    property ZeroMask: TMaskString read fZeroMask write SetZeroMask;
  end;
  
  {:----------------------------------------------------------------------------
   }
  TmmCustomStrEdit = class (TCustomEdit)
  private
    fAlignment: TAlignment;
    FOldAlignment: TAlignment;
    FTextMargin: Integer;
    function CalcTextMargin: Integer;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure SetAlignment(Value: TAlignment);
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
  protected
    property Alignment: TAlignment read fAlignment write SetAlignment default taRightJustify;
  public
    constructor Create(AOwner: TComponent); override;
  end;
  
  {:----------------------------------------------------------------------------
   Custom Numeric Edit}
  TmmNumEdit = class (TmmCustomStrEdit)
  private
    fDecimals: Word;
    fDigits: Word;
    fMasks: TMasks;
    fMax: Extended;
    fMin: Extended;
    fNumericType: TNumericType;
    fUseRounding: Boolean;
    fValidate: Boolean;
    fValidateString: string;
    mValue: Extended;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure ConvertValue;
    function GetValue: Extended;
    procedure SetDecimals(Value: Word);
    procedure SetDigits(Value: Word);
    procedure SetMasks(Mask: TMasks);
    procedure SetMax(Value: Extended);
    procedure SetMin(Value: Extended);
    procedure SetNumericType(Value: TNumericType);
    procedure SetValidate(Value: Boolean);
    procedure SetValidateString(const Value: string);
    procedure SetValue(Value: Extended);
  protected
    procedure FormatText; dynamic;
    procedure KeyPress(var Key: Char); override;
    procedure ShowErrorMessage;
    procedure UnFormatText; dynamic;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function AsInteger: Integer; dynamic;
    function AsString: string; dynamic;
    function CheckValue: Boolean;
    function IsValid: Boolean;
    procedure MaskChanged(Sender: TObject);
    function Valid(aValue: extended): Boolean; dynamic;
    property Value: Extended read GetValue write SetValue;
  published
    property AutoSize;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property Decimals: Word read fDecimals write SetDecimals;
    property Digits: Word read fDigits write SetDigits;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property Masks: TMasks read fMasks write SetMasks;
    property Max: Extended read fMax write SetMax;
    property Min: Extended read fMin write SetMin;
    property NumericType: TNumericType read fNumericType write SetNumericType default ntCurrency;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property UseRounding: Boolean read fUseRounding write fUseRounding;
    property Validate: Boolean read fValidate write SetValidate;
    property ValidateString: string read fValidateString write SetValidateString;
    property Visible;
  end;
  
procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
const
//  DefValidateStr = 'Wert ist nicht im erlaubten Eingabebereich.' +#10#13 +'Bereich von %s bis %s';
  DefValidateStr = '(*)Wert ist nicht im erlaubten Eingabebereich. Bereich von %s bis %s';

type
 TSetOfChar = set of char;

var
 OldMaxLength: integer;

function Power(x,y: integer): real;
begin
 result := exp(ln(x) *y);
end; //function Power
//-----------------------------------------------------------------------------
function StripChars(const Text: string; ValidChars: TSetOfChar): string;
var
 S        : string;
 i        : integer;
 Negative : boolean;

begin
  Negative := false;
  Result := '';
  if Text <> '' then
  try
    if (Text[1] = '-') or (Text[length(Text)] = '-') then Negative := true;
    S := '';
    for i := 1 to length(Text) do
     if Text[i] in ValidChars then
       S := S + Text[i];

    if Negative then Result := '-' +S
                else Result := S;
  except
  end; //try..except
end; //function StripChars
//-----------------------------------------------------------------------------

{:------------------------------------------------------------------------------
 TMasks}
{:-----------------------------------------------------------------------------}
constructor TMasks.Create;
begin
  inherited Create;
  FPositiveMask := '#,##0';
  FNegativeMask := '';
  FZeroMask     := '';
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetNegativeMask(Value: TMaskString);
begin
  if FNegativeMask <> Value then
  begin
   FNegativeMask := Value;
   OnChange(Self);
  end; //if FNegativeMask <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetPositiveMask(Value: TMaskString);
begin
  if FPositiveMask <> Value then
  begin
   FPositiveMask := Value;
   OnChange(Self);
  end; //if FPositiveMask <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TMasks.SetZeroMask(Value: TMaskString);
begin
  if FZeroMask <> Value then
  begin
   FZeroMask := Value;
   OnChange(Self);
  end; //procedure TMasks.SetZeroMask
end;

{:------------------------------------------------------------------------------
 TmmCustomStrEdit}
{:-----------------------------------------------------------------------------}
function TmmCustomStrEdit.CalcTextMargin: Integer;
var
  DC: HDC;
  SaveFont: HFont;
  I: Integer;
  SysMetrics, Metrics: TTextMetric;
begin
  DC := GetDC(0);
  GetTextMetrics(DC,SysMetrics);
  SaveFont := SelectObject(DC,Font.Handle);
  GetTextMetrics(DC,Metrics);
  SelectObject(DC,SaveFont);
  ReleaseDC(0,DC);
  I := SysMetrics.tmHeight;
  if I > Metrics.tmHeight then
    I := Metrics.tmHeight;
  result := I div 4;
end;

{:-----------------------------------------------------------------------------}
procedure TmmCustomStrEdit.CMEnter(var Message: TCMEnter);
begin
  inherited;
  FOldAlignment := FAlignment;
  Alignment := taLeftJustify;
end;

{:-----------------------------------------------------------------------------}
procedure TmmCustomStrEdit.CMExit(var Message: TCMExit);
begin
  inherited;
  Alignment := FOldAlignment;
end;

{:-----------------------------------------------------------------------------}
constructor TmmCustomStrEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FAlignment  := taLeftJustify;
  FTextMargin := CalcTextMargin;
end;

{:-----------------------------------------------------------------------------}
procedure TmmCustomStrEdit.SetAlignment(Value: TAlignment);
begin
  if FAlignment <> Value then begin
    FAlignment := Value;
    Invalidate;
  end; //if FAlignment <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmCustomStrEdit.WMPaint(var Message: TWMPaint);
var
  Width, Indent, Left: Integer;
  R: TRect;
  DC: HDC;
  PS: TPaintStruct;
  S: string;
  Canvas: TControlCanvas;
begin
  if FAlignment = taLeftJustify then begin
    inherited;
    Exit;
  end; //if FAlignment = taLeftJustify then
  
  Canvas := TControlCanvas.Create;
  try
    Canvas.Control := Self;
    DC := Message.DC;
    if DC = 0 then
      DC := BeginPaint(Handle,PS);
    Canvas.Handle := DC;
  
    Canvas.Font := Font;
    with Canvas do begin
      R := ClientRect;
      Brush.Color := Color;
      S := Text;
      Width := TextWidth(S);
      if BorderStyle = bsNone then Indent := 0
                              else Indent := FTextMargin;
      if FAlignment = taRightJustify then Left := R.Right - Width - Indent
                                     else Left := (R.Left + R.Right - Width) div 2;
      TextRect(R,Left,Indent,S);
    end; //with Canvas do
  finally
    Canvas.Handle := 0;
    if Message.DC = 0 then
      EndPaint(Handle,PS);
  end; //try..finally
end;

{:------------------------------------------------------------------------------
 TmmNumEdit}
{:-----------------------------------------------------------------------------}
function TmmNumEdit.AsInteger: Integer;
var
  xFloat: Extended;
begin
  Result := 0;
  xFloat := Value;
  if (xFloat > Low(Integer)) and (xFloat < High(Integer)) then
    if FUseRounding then Result := Round(xFloat)
                    else Result := Trunc(xFloat);
end;

{:-----------------------------------------------------------------------------}
function TmmNumEdit.AsString: string;
begin
  Result := '';
  if CheckValue then begin
    Result := StripChars(Text, ['0'..'9', DecimalSeparator, ThousandSeparator]);
    if mValue < 0 then
      Result := '-' + Result;
  end;
  
  {
  xExt := Value;
  Result := StripChars(Text, ['0'..'9', DecimalSeparator, ThousandSeparator]);
  if xExt < 0 then begin
    Result := '-' + Result;
  {}
end;

{:-----------------------------------------------------------------------------}
function TmmNumEdit.CheckValue: Boolean;
var
  xExt: Extended;
begin
  Result := True;
  ConvertValue;
  if Validate then begin
    if FNumericType = ntPercentage then xExt := mValue * 100
                                   else xExt := mValue;
    if (xExt < FMin) or (xExt > FMax) then begin
      Result := False;
      ShowErrorMessage;
    end;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.CMEnter(var Message: TCMEnter);
begin
  UnFormatText;
  OldMaxLength := MaxLength;
  MaxLength    := FDigits;
  inherited;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.CMExit(var Message: TCMExit);
begin
  MaxLength := OldMaxLength;
  try
    // performs to fill the mValue property value
    ConvertValue;
    FormatText;
    inherited CMExit(Message)
  except
    SelectAll;
    SetFocus;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.ConvertValue;
var
  xStr: string;
begin
  mValue := 0;
  xStr := StripChars(Text, ['0'..'9', DecimalSeparator]);
  if xStr <> '' then
  try
    mValue := StrToFloat(xStr);
    if FNumericType = ntPercentage then
      mValue := mValue / 100
  except
  end;
end;

{:-----------------------------------------------------------------------------}
constructor TmmNumEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  
  AutoSelect := True;
  Width := 85;
  
  FAlignment := taRightJustify;
  FNumericType := ntCurrency;
  FDigits := 12;
  FDecimals := 2;
  FMax := 0.0;
  FMin := 0.0;
  FTextMargin := CalcTextMargin;
  FValidate := false;
  FValidateString := DefValidateStr;
  FUseRounding := true;
  
  FMasks := TMasks.Create;
  FMasks.OnChange := MaskChanged;
  
  mValue := 0.0;
  
  FormatText;
end;

{:-----------------------------------------------------------------------------}
destructor TmmNumEdit.Destroy;
begin
  FMasks.Free;
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.FormatText;
var
  xExt: Extended;
  Multiplier: Real;
begin
  try
    Multiplier := Power(10,Decimals);
    if FNumericType = ntPercentage then xExt := mValue * 100
                                   else xExt := mValue;
    if UseRounding then xExt := round(xExt * Multiplier) /Multiplier
                   else xExt := trunc(xExt * Multiplier) /Multiplier;
  except
    on e:Exception do begin
      xExt := mValue;
    end;
  end; //try..except
  
  case FNumericType of
    ntCurrency   : Text := FloatToStrF(xExt, ffCurrency, FDigits, FDecimals);
    ntPercentage : Text := FloatToStrF(xExt, ffFixed, FDigits, FDecimals) + '%';
    ntGeneral    : with Masks do
                     Text := FormatFloat(PositiveMask + ';' + NegativeMask + ';' + ZeroMask, xExt);
  end; //case
end;

{:-----------------------------------------------------------------------------}
function TmmNumEdit.GetValue: Extended;
begin
  Result := 0;
  if CheckValue then
    Result := mValue;
end;

{:-----------------------------------------------------------------------------}
function TmmNumEdit.IsValid: Boolean;
begin
  //  Result := Valid(Value);
  Result := (Value >= FMin) and (Value <= FMax);
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.KeyPress(var Key: Char);
begin
  if Key in ['0'..'9', '-', DecimalSeparator, ThousandSeparator, #8] then begin
    // special handling for special characters
    if Key in ['-', DecimalSeparator] then begin
      if ((Key = DecimalSeparator) and (Pos(DecimalSeparator,Text) > 0)) OR
         ((Key = '-') and (Pos('-', Text) > 0)) then
        Key := #0;
      // at special character inc MaxLength
      if Key <> #0 then
        MaxLength := MaxLength + 1;
    end;
    // call inherited with valid characters only
    if Key <> #0 then
      inherited KeyPress(Key);
  end else
    Key := #0;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.MaskChanged(Sender: TObject);
begin
  FormatText;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetDecimals(Value: Word);
begin
  if FDecimals <> Value then begin
    FDecimals := Value;
    FormatText;
  end; //if FDecimals <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetDigits(Value: Word);
begin
  if FDigits <> Value then begin
    FDigits := Value;
    FormatText;
  end; //if FDigits <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetMasks(Mask: TMasks);
begin
  if fMasks <> Mask then begin
    fMasks := Masks;
    Invalidate;
  end; //if fMasks <> Mask then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetMax(Value: Extended);
begin
  if FMax <> Value then begin
    FMax := Value;
    if fValidate and (mValue > FMax) then
      mValue := FMax;
  end; //if FMax <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetMin(Value: Extended);
begin
  if FMin <> Value then begin
    FMin := Value;
    if fValidate and (mValue < FMin) then
      mValue := FMin;
  end; //if FMin <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetNumericType(Value: TNumericType);
begin
  if FNumericType <> Value then begin
    FNumericType := Value;
    FormatText;
  end; //if FNumericType <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetValidate(Value: Boolean);
begin
  if FValidate <> Value then begin
    FValidate := Value;
    if FValidate then begin
      if mValue < FMin then
        mValue := FMin
      else if mValue > FMax then
        mValue := FMax;
      FormatText;
    end; //if FValidate and ((mValue < FMin) or (mValue > FMax)) then
  end; //if FValidate <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetValidateString(const Value: string);
  const
   EngStr = 'Value must be between %g and %g';
begin
  if FValidateString <> Value then begin
    if Value <> '' then FValidateString := Value
                   else FValidateString := DefValidateStr;
  end; //if FValidateString <> Value then
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.SetValue(Value: Extended);
begin
  if (mValue <> Value) then begin
    mValue := Value;
    FormatText;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.ShowErrorMessage;
var
  sMin, sMax: string;
begin
  with Masks do begin
    sMin := FormatFloat(PositiveMask+';'+NegativeMask+';'+ZeroMask,FMin);
    sMax := FormatFloat(PositiveMask+';'+NegativeMask+';'+ZeroMask,FMax);
  end; //with
  
  MessageBeep(0);
  MessageDlg(Format(FValidateString, [sMin, sMax]), mtError, [mbOk], 0);
  
  SelectAll;
  SetFocus;
  // Abort performs to interupt the execution of outer code with a silent exception
  Abort;
end;

{:-----------------------------------------------------------------------------}
procedure TmmNumEdit.UnFormatText;
begin
  Text := StripChars(Text, ['0'..'9', DecimalSeparator, ThousandSeparator]);
  if mValue < 0 then
    Text := '-' + Text;
end;

{:-----------------------------------------------------------------------------}
function TmmNumEdit.Valid(aValue: extended): Boolean;
begin
  {
  Result := True;
  if Validate then begin
    Result := (aValue >= FMin) and (aValue <= FMax);
    if not Result then
      ShowErrorMessage;
  end;
  
  {}
  Result := True;
  if Validate and (not Isvalid) then
  try
    ShowErrorMessage;
  finally
    Alignment := FOldAlignment;
  end; //try..finally
  {}
end;

procedure Register;
begin
  RegisterComponents('MillMaster', [TmmNumEdit]);
end; //procedure Register

end. //NumCtrl.pas
