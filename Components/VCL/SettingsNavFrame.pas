(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: SettingsNavFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.12.1999  1.00  Mg  | Datei erstellt
| 20.03.2000  1.01  Mg  | Umbau auf WndProc und Einbau von WSC - Preselect
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 08.05.2000  1.12  Mg  | Group State inserted
| 14.07.2000  1.13  Mg  | cProdGrpQuery : top 300 inserted
| 30.08.2000  1.14  Mg  | TSettingsNav.CobSelectionChanged : YarnCnt initialized with 0.0
| 12.09.2000  1.15  Mg  | cobChooserDrawItem TextRect inserted
| 18.01.2001  1.16 NueMg| Several changes cause of implementing style in SettingsNav
| 14.02.2001  1.17  Nue | Within the style selection the ym_settings-Color has to be set
|                         instead of the style-Color.
| 12.07.2001  1.18  Nue | Several changes for release 2.01.01
| 20.09.2001  1.19  Nue | YMSetName in cobChooser and new pbchooserHeader added.
| 01.10.2001  1.20  Nue | In TPreselectItem YMSetName and IsOnlyTemplateSetsAvailable added.
| 04.10.2001  1.21  Nue | SpdRangeList.ItemChanged ( ucSettings) set inactive?? .
| 14.11.2001  1.22  Nue | ActCobChooserItem and cChangedOnZE added.
| 19.12.2001  1.23  Nue | mYMSettings.LoadFromDB from SettingsNavFrame to SettingsFrame.
| 20.12.2001        Nue | Filter in Query for cobChooser Style and Template.
| 06.03.2002  1.24  Nue | Additional FillListCall in TSettingsNav.WndProc
| 08.04.2002  1.25  Nue | cPreselectQuery added and dependent code added. (Add YM_Set_Name to PreselectItem)
| 25.04.2002  1.26  Nue | FillList: Check for showing TK-Classes in cases stTemplate and stStyle
|                       | made easier because of problems in WindingMaster (empty box because of wrong
|                       | access on upper class property SpindleFirst and SpindleLast)
| 02.10.2002        LOK | Umbau ADO
| 11.10.2002        LOK | EOF ADO Conform
| 07.11.2002        LOK | In TSettingsNav.UpdateComponent wird visible auf true gesetzt. F�r Maschinen
|                       | mit Fix Spindle Range kann visible = false sein, da die Daten zuerst von der Maschine
|                       | geladen werden m�ssen und w�hrend dieser Zeit die Settings nicht angezeigt werden d�rfen.
|                       | (siehe 'TAssign.SetSpdRange()')
| 29.01.2003        Wss | Diverse anpassungen an neue Package angepasst
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 06.05.2003  1.27  Nue | Diverse Anpassungen wegen Vereinfachung Asiignment GUI
| 27.08.2003  1.30  Nue | Ueberarbeitung Assignment: Klassen TxxItem neu jetzt in AssignComp.
| 18.09.2003  1.31  Nue | Procedure Loaded ueberarbeitet.
| 09.12.2003  1.32  Nue | CobChooserClearAndFree  added.
| 28.01.2004  1.33  Nue | fActCobChooserItem in create initialisiert auf NIL;
|                       | in CobChooserClearAndFree fActCobChooser=NIL gesetzt;
| 15.07.2005  1.34  Wss | Beim Maschinenupload ist im System der YarnCount Nm. Nach dem Upload muss demzufolge
                          der YarnCount noch in die globale Einheit umgewandelt werden. Gleich wie wenn der Wert
                          von der DB geladen wurde. Auf der DB ist der gespeicherte YarnCount IMMER in Nm
| 19.07.2005  1.35  Nue |  AssignComp.TPreselectItem.SettingsLen eliminated.
| 12.06.2006  1.36  Nue | Minor changes in CobSourceChanged.
|=============================================================================*)

unit SettingsNavFrame;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmRadioButton, mmComboBox, mmGroupBox, AssignComp,
  MMUGlobal, SettingsFrame, BaseGlobal, IvDictio, YMParaDef, LoepfeGlobal,
  mmLabel, ExtCtrls, mmPaintBox, mmAdoDataSet, Grids, AdvGrid,
  wwdbdatetimepicker, mmButton, ActnList,
  mmActionList, ComCtrls, ImgList, mmImageList, ToolWin, mmToolBar,
  mmStatusBar, mmPanel, Buttons, mmBitBtn, mmEdit,
  Db, ADODB, mmADOConnection, mmMemo, mmUpDown, //FilterCobchooserForm, //2.4.03
  ListCobchooserForm, mmExtStringGrid, mmSpeedButton, fcButton, fcImgBtn,
  fcShapeBtn, mmLineLabel, XMLDef; //14.5.03
//------------------------------------------------------------------------------

resourcestring

  cNotFound = '(*)Objekt konnte nicht gefunden werden:'; //ivlm
  cNoEntry = '(*)Kein Suchkriterium eingegeben!'; //ivlm
//------------------------------------------------------------------------------

type
  TTabArr = array[1..12] of Integer;

const
//Def. in BaseGlobal:  TProdGrpState = (psUndefined, psOrdinaryAquis, psNotStarted, psInconsistent,
//                        psStopped, psCollectingData, psStopping, psForcedDBStop, psForcedZEStop);
//  cProdGrpStateArr: Array[TProdGrpState] of String = ('?','AssMM','?','AssZE','Ended','?','Ended','Ended','Ended');
  cProdGrpStateArr: array[TProdGrpState] of string = ('?', 'A', '?', 'A', '-', '?', '-', '-', '-');

// F�r Dialogformat
  cP0 = 105; //vor SpindleFirst
  cP1 = 123; //vor -SpindleLast
  cP2 = 148; //vor (GrpNr)
  cP3 = 180; //vor LotName
  cP4 = 335; //vor Artikel
  cP5 = 471; //vor TK-Klasse
  cP6 = 543; //vor Vorlage
  cP7 = 661; //vor LotStart
  cP8 = 767; //vor Status
  cP9 = 812; //vor YarnCnt
  cP10 = 890; //vor Slip
  cP11 = 952; //Ende Control
  cT1 = 180;
  cS1 = 210; //vor Vorlage
  cS2 = 390; //vor TK-Klasse
  cS3 = 480; //vor Sortiment
  cS4 = 660; //vor YarnCnt
  cS5 = 770; //vor Slip
  cM1 = 30;
  cM2 = 70;
// F�r Tabformat
//  cP0 = 70;    //vor SpindleFirst
//  cP1 = 80;    //vor -SpindleLast
//  cP2 = 96;    //vor (GrpNr)
//  cP3 = 117;   //vor LotName
//  cP4 = 227;   //vor Artikel
//  cP5 = 317;   //vor TK-Klasse
//  cP6 = 357;   //vor Vorlage
//  cP7 = 447;   //vor LotStart
//  cP8 = 517;   //vor Status
//  cP9 = 547;   //vor YarnCnt
//  cP10= 597;   //vor Slip
//  cT1 = 120;
//  cS1 = 140;
//  cS2 = 260;
//  cS3 = 320;
//  cM1 = 15;
//  cM2 = 36;
//------------------------------------------------------------------------------
  cProdGrpTabs: TTabArr = (cP0, cP1, cP2, cP3, cP4, cP5, cP6, cP7, cP8, cP9, cP10, cP11);
  cStyleTabs: TTabArr = (cS1, cS2, cS3, cS4, cS5, cP11, 0, 0, 0, 0, 0, 0);
  cPreselectTabs: TTabArr = (cM1, cM2, cP3, cP4, cP6, cP7, cP11, 0, 0, 0, 0, 0);
  cTemplateTabs: TTabArr = (cT1, cP11, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
//------------------------------------------------------------------------------
//..............................................................................

type
//..............................................................................

//..............................................................................
  TChangeCobSelectionEvent = procedure(Sender: TObject; aLengthMode: Byte;
      aLenghtWindow: Word) of object;

  TSettingsNav = class(TFrame, INavChange)
    acFilter: TAction;
    acHistory: TAction;
    acInitial: TAction;
    acLot: TAction;
    acLotParameter: TAction;
    acMachine: TAction;
    acOrderpos: TAction;
    acShowAssigned: TAction;
    acStyle: TAction;
    acTemplate: TAction;
    bHistory: TfcShapeBtn;
    bLot: TfcShapeBtn;
    bMachine: TfcShapeBtn;
    bOrderpos: TfcShapeBtn;
    bStyle: TfcShapeBtn;
    bTemplate: TfcShapeBtn;
    cobChooser: TmmComboBox;
    conDefault: TmmADOConnection;
    dseQuery1: TmmADODataSet;
    mmActionList1: TmmActionList;
    mmGroupBox1: TmmLineLabel;
    mmImageList1: TmmImageList;
    pbchooserHeader: TmmPaintBox;
    procedure acInitialExecute(Sender: TObject);
    procedure acShapeBtnExecute(Sender: TObject);
    procedure cobChooserChange(Sender: TObject);
    procedure cobChooserDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
//    procedure mExtStringGridClickCell(Sender: TObject; Arow, Acol: Integer);
//    procedure mExtStringGridClickSort(Sender: TObject; aCol: Integer);
//    procedure mExtStringGridDblClickCell(Sender: TObject; Arow, Acol: Integer);
//    procedure mExtStringGridGetFormat(Sender: TObject; ACol: Integer; var AStyle: TSortStyle; var aPrefix, aSuffix: string);
//    procedure mmLabel2Click(Sender: TObject);
    procedure pbchooserHeaderPaint(Sender: TObject);
    procedure rbClick(Sender: TObject);
    procedure ShapeBtnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
  private
    fActCobChooserItem: TBaseItem;
    fActColor: TColor;
    fActSourceType: TSourceTypes;
    FOnChangeCobSelection: TChangeCobSelectionEvent;
    FOnEventEnableButtons: TNotifyEvent;
    fOrderPosition: TOrderPosition;
    fPrevSourceType: TSourceTypes;
    fProdGrpName: string;
    fQuery: TmmAdoDataSet;
    fSettings: TSettings;
    FSettingsHandler: TGetSettingsHandler;
    fSpdRangeList: TSpindleRangeList;
//wss    fTimePanelVisible: Boolean;
    fUnMachineCobChooser: Boolean;
    fYarnUnit: TYarnUnit;
    mAssWindowMsg: DWord;
    mDateSortDirectionDesc: Boolean;
    mDroppedDownCobChooserAllowed: Boolean;
    mFirstShow: Boolean;
    mIntWindowMsg: DWord;
    mListForm: TListCobChooser;
//wss    mPrevButton: TAction;
    mProdGrp: TProdGrp;
    mSortStart: Boolean;
    mTestNueColor: TColor;
//wss    mTimePanelVisible: Boolean;
    function CheckUserValue: Boolean;
    procedure CobChooserClearAndFree;
    procedure CobSelectionChanged;
    procedure CobSourceChanged(Sender: TObject);
    procedure DrawCanvas(aObject: TObject; aCanvas: TCanvas; aRect: TRect; aStrAll:
        string; aSeparator: Boolean; aCallForHeader: Boolean; aState:
        TOwnerDrawState = []; aGroupState: TGroupState = gsfree);
    procedure EnableButtons(aEnable: Boolean);
    function FillList(aSource: TSourceTypes): Boolean;
    function GetActColor: TColor;
    function GetActPreselectItemSettings: TYMSettingsByteArr;
    function GetMiscInfo: IMiscInfo;
    function GetProdGrpID: Integer;
    procedure GetSelection(Sender: TObject);
//wss    function GetTabulatedString(aBaseItem: TBaseItem): string;
    procedure InitShapeBtn(Sender: TObject);
    procedure SetActCobChooserItem(aActCobChooserItem: TBaseItem);
    procedure SetDropDownCobChooser(const Value: Boolean);
    procedure SetProdGrpID(const aProdGrpID: Integer);
    procedure SetProdGrpName(aProdGrpName: string);
    procedure SetQuery(aQuery: TmmAdoDataSet);
    procedure SetSettingsHandler(const Value: TGetSettingsHandler);
    function SetSourceTypeTemplateOrStyle: TSourceTypes;
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    procedure SetUnMachineCobChooser(const Value: Boolean);
    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
  protected
    procedure DoChangeCobSelection(Sender: TObject; aLengthMode: Byte;
        aLengthWindow: Word);
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure WndProc(var msg: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure RefreshList;
    procedure UpdateActList;
    property ActCobChooserItem: TBaseItem read fActCobChooserItem write SetActCobChooserItem;
    property ActColor: TColor read GetActColor;
    property ActPreselectItemSettings: TYMSettingsByteArr read GetActPreselectItemSettings;
    property ActSourceType: TSourceTypes read fActSourceType write fActSourceType;
    property DropDownCobChooser: Boolean write SetDropDownCobChooser;
    property MiscInfo: IMiscInfo read GetMiscInfo;
    property OrderPosition: TOrderPosition read fOrderPosition;
    property ProdGrpID: Integer read GetProdGrpID write SetProdGrpID;
    property ProdGrpName: string read fProdGrpName write SetProdGrpName;
    property UnMachineCobChooser: Boolean read fUnMachineCobChooser write SetUnMachineCobChooser;
    property YarnUnit: TYarnUnit read fYarnUnit write fYarnUnit;
  published
    property OnChangeCobSelection: TChangeCobSelectionEvent read
        FOnChangeCobSelection write FOnChangeCobSelection;
    property OnEventEnableButtons: TNotifyEvent read FOnEventEnableButtons write
        FOnEventEnableButtons;
    property Query: TmmAdoDataSet read fQuery write SetQuery;
    property Settings: TSettings read fSettings write fSettings;
    property SettingsHandler: TGetSettingsHandler read FSettingsHandler write
        SetSettingsHandler;
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
  end;

//..............................................................................

  //procedure Register;
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  YMParaUtils, SettingsReader,
  mmCS,
  AssignForm, XMLGlobal;
{$R *.DFM}
//..............................................................................
const
  NAV_WND_REG_STR = 'nav_wnd_reg_str';
  COB_SOURCE_CHANGED = 1;
  COB_DATA_AVAILABLE = 2;
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
//:---------------------------------------------------------------------------
//:--- Class: TSettingsNav
//:---------------------------------------------------------------------------
constructor TSettingsNav.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mIntWindowMsg := RegisterWindowMessage(NAV_WND_REG_STR);
  mAssWindowMsg := RegisterWindowMessage(ASSIGN_REG_STR);
  //  bTemplate.SetFocus;
  //  acTemplate.Checked := True;
  fProdGrpName := '';
  bOrderPos.Visible := False;

  acStyle.Enabled := TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable;
  if acStyle.Enabled then begin
    bTemplate.Visible := False;
    bStyle.Left := bTemplate.Left;
  end
  else begin
    bStyle.Visible := False;
  end;

  fActColor := 0;
  fActSourceType := stProdGrp; //Nue:2.7.03
  fPrevSourceType := SetSourceTypeTemplateOrStyle;
  fSpdRangeList := nil;
  fYarnUnit := TMMSettingsReader.Instance.Value[cYarnCntUnit];
  fActCobChooserItem := nil; //Nue:28.1.04

  mDateSortDirectionDesc := True;
  mDroppedDownCobChooserAllowed := False; //Um zu verhindern, dass cobChooser bereits DroppedDown kann
  mFirstShow := True;
  mListForm := nil; // wird in FillList initialisiert
  mProdGrp := TProdGrp.Create(dseQuery1); // wird in SetQuery ev. �berschrieben
  mSortStart := True;
end;

//:---------------------------------------------------------------------------
destructor TSettingsNav.Destroy;
begin
  //  CobChooserClearAndFree;
  //  { Free sollte gemacht werden, wird aber darauf verzichtet wegen Crash bei
  //  Programmende.
  //    if Assigned ( fOrderPosition ) then
  //      fOrderPosition.Free;
  //    if Assigned ( mProdGrp ) then
  //      mProdGrp.Free;
  //  }
  //  if Assigned(mListForm) then try
  //    FreeAndNil(mListForm);
  //    inherited Destroy;
  //  except
  //    on e: Exception do begin
  //      raise Exception.Create('TSettingsNav.Destroy failed. ' + e.Message);
  //    end;
  //  end;

  CobChooserClearAndFree;
  if Assigned(fOrderPosition) then
    FreeAndNil(fOrderPosition);
  if Assigned(mProdGrp) then
    FreeAndNil(mProdGrp);
  if Assigned(mListForm) then
    FreeAndNil(mListForm);

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.acInitialExecute(Sender: TObject);
begin
  //  ToolbarManager(taBInitialDown);
  CobSourceChanged(acLot);
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.acShapeBtnExecute(Sender: TObject);
begin
  InitShapeBtn(Sender);

  CobSourceChanged(Sender);

  //Settings wieder visible schalten (Wird auch noch in FillList nach mListForm.ShowModal gemacht!)
  if ((Sender = acMachine) or (Sender = acLot)) and (Self.Owner is TAssign) and (not mFirstShow) then
    if (Sender = acMachine) or (SpdRangeList.ActSpdRange.IsInProduction) then
      (Self.Owner as TAssign).SettingsVisible := True
    else
      (Self.Owner as TAssign).SettingsVisible := False;

  if (cobChooser.Showing) and (cobChooser.Visible) and (cobChooser.Enabled) then begin
    cobChooser.SetFocus;
  end;
end;

//:---------------------------------------------------------------------------
function TSettingsNav.CheckUserValue: Boolean;
begin
  Result := True;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.cobChooserChange(Sender: TObject);
begin
  CobSelectionChanged;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.CobChooserClearAndFree;
var
  i: Integer;
begin
  //L�schen der Listbox-Objekte
  with cobChooser.Items do begin
    for i := 0 to (Count - 1) do begin
      if Assigned(Objects[i]) then
        Objects[i].Free;
    end;
  end; //for
  //L�schen der Listboxinhalte
  cobChooser.Clear;
  fActCobChooserItem := nil;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.cobChooserDrawItem(Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  xStr: string;
  xGrpItem: TProdGrpItem;
  xGroupState: TGroupState;
  xMachItem: TPreselectItem;
begin
  xGroupState := gsfree;
  if fUnMachineCobChooser and ((fActSourceType = stProdGrp) or (fActSourceType = stHistory)) then begin
    //Fall, wenn AskAssign visible wird: maschinenabh�ngige Infos werden unterdr�ckt.
    if ActCobChooserItem is TProdGrpItem then begin
      xGrpItem := TProdGrpItem(ActCobChooserItem);
      if xGrpItem.State in [1,3,5] then
        xGroupState := gsInProd;
      xStr := Format(';;;;%s;%s;%s;%s;%s;;%4.1f;%s;',
        [xGrpItem.Name, xGrpItem.StyleName, cGUISensingHeadClassNames[xGrpItem.HeadClass],
        xGrpItem.YMSetName, xGrpItem.ProdStart, xGrpItem.YarnCnt, xGrpItem.Slip]);
      DrawCanvas(cobChooser, cobChooser.Canvas, Rect, xStr, True {Trennzeichen}, False {HeaderCall}, State, xGroupState);
    end;
  end
  else begin
    //Normalfall
    ActCobChooserItem := (cobChooser.Items.Objects[Index] as TBaseItem); //Nue:20.12.06

    if ActCobChooserItem is TProdGrpItem then begin
      xGrpItem := TProdGrpItem(ActCobChooserItem);
      if xGrpItem.State in [1,3,5] then
        xGroupState := gsInProd;
    end
    else if ActCobChooserItem is TPreselectItem then begin
      xMachItem := TPreselectItem(ActCobChooserItem);
      xGroupState := xMachItem.State;
    end;

    DrawCanvas(cobChooser, cobChooser.Canvas, Rect, cobChooser.Items[Index], True {Trennzeichen},
               False {HeaderCall}, State, xGroupState);
////alt bis 20.12.06   ActCobChooserItem := (cobChooser.Items.Objects[Index] as TBaseItem); //Nue:13.11.01
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.CobSelectionChanged;
//var
//  xItem: TBaseItem;
begin
  if Assigned(SpdRangeList) then
    //Nue:4.10.01???????? SpdRangeList.ItemChanged ( ucSettings );
    fProdGrpName := '';

  if (fActSourceType = stStyle) or (fActSourceType = stTemplate) or
    (fActSourceType = stHistory) or (fActSourceType = stOrderPosition) then begin
    //Nue:21.5.03 Wenn keine Elemente in Grid wird Row=0
    if (mListForm.mExtStringGrid.RowCount > mListForm.mExtStringGrid.FixedRows) then begin
      SpdRangeList.ProdUnit := nil; //Nue
      fSettings.YarnCnt := 0.0;
      if Assigned(fSettings) then begin
        if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, 1] is TBaseSetIDItem then begin
          fActColor := TBaseSetIDItem(mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row]).Color;
        end;
      end;

      if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, 1] is TOrderPositionItem then begin
        fOrderPosition.OrderPositionID := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TOrderPositionItem).OrderPositionID;
      end;

      if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, 1] is TStyleItem then begin //Style
        fOrderPosition.StyleID := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TStyleItem).StyleID;
      end;

      if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, 1] is TProdGrpItem then begin //History
        fProdGrpName := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TProdGrpItem).Name;
        fOrderPosition.StyleID := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TProdGrpItem).StyleID;
      end;

      if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, 1] is TTemplateItem then begin
        fProdGrpName := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TTemplateItem).Name;
        fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
      end;
      SpdRangeList.ItemChanged(ucProdUnit);
    end;
  end
  else begin //rbProdGrp, rbMachine
    if cobChooser.ItemIndex = -1 then
      EXIT;

    if cobChooser.Items.Objects[0] is TPreselectItem then begin
//NUE1      fSettings.SetScreenSettings((cobChooser.Items.Objects[cobChooser.ItemIndex] as TPreselectItem).Settings);
      //DOM-Baum des Cobchooser-Elementes ins Model �bernehmen
      fSettings.Model.xmlAsString := ((cobChooser.Items.Objects[cobChooser.ItemIndex] as TPreselectItem).Settings);

      //Notwendig um im Fenster AddYMSettings Werte zu updaten (LengthMode, LengthWindow)
      with (cobChooser.Items.Objects[cobChooser.ItemIndex] as TPreselectItem) do begin
        DoChangeCobSelection(self, LengthMode, LengthWindow);
      end;
      fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
    end
    else if cobChooser.Items.Objects[0] is TProdGrpItem then begin
      fProdGrpName := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TProdGrpItem).Name;
      fOrderPosition.StyleID := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TProdGrpItem).StyleID;
    end;
    SpdRangeList.ItemChanged(ucProdUnit);

  end; //else
  pbchooserHeaderPaint(self); //Nue:19.9.01
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.CobSourceChanged(Sender: TObject);
begin
  if csDesigning in ComponentState then exit;

  if Assigned(SpdRangeList) then begin
    fPrevSourceType := fActSourceType;
    fSettings.ConfigCodeVisible := False;

    if Sender = acTemplate then begin
//?? Nue:12.06.06      fPrevSourceType := SetSourceTypeTemplateOrStyle; //Nue 27.04.05?
      fActSourceType := stTemplate;
      cobChooser.Enabled := False;
      mListForm.InitGrid(cTemplateGrid);
      FillList(fActSourceType);
    end
    else if Sender = acStyle then begin
//?? Nue:28.11.05      fPrevSourceType := SetSourceTypeTemplateOrStyle; //Nue 27.04.05?
      fActSourceType := stStyle;
      cobChooser.Enabled := False;
      mListForm.InitGrid(cStyleGrid);
      FillList(fActSourceType);
    end
    else if Sender = acOrderPos then begin
      fPrevSourceType := SetSourceTypeTemplateOrStyle; //Nue 27.04.05?
      fActSourceType := stStyle;
      cobChooser.Enabled := False;
      mListForm.InitGrid(cOrderPositionGrid);
      FillList(fActSourceType);
    end
    else if Sender = acLot then begin
      fPrevSourceType := SetSourceTypeTemplateOrStyle; //Nue 19.06.01
      fActSourceType := stProdGrp;
      cobChooser.Enabled := True;
      SendMessage(self.Handle, mIntWindowMsg, COB_SOURCE_CHANGED, ord(fActSourceType));
    end
    else if Sender = acMachine then begin
      if fPrevSourceType = stMaMemory then //Nur setzen, wenn verschieden Nue:31.1.06
        fPrevSourceType := stProdGrp;
      fActSourceType := stMaMemory;
      cobChooser.Enabled := True;
      SendMessage(self.Handle, mIntWindowMsg, COB_SOURCE_CHANGED, ord(fActSourceType));
      if not SpdRangeList.AssMachine.IsOnline then begin //Neu:22.03.05
        fActSourceType := fPrevSourceType;
      end
      else begin
        fSettings.ConfigCodeVisible := True;
      end;
    end
    else if Sender = acHistory then begin
//??      fPrevSourceType := SetSourceTypeTemplateOrStyle; //Nue 8.3.05??
      fActSourceType := stHistory;
      cobChooser.Enabled := False;
      mListForm.InitGrid(cProdGrpGrid);
      FillList(fActSourceType);
    end;
  end;
end;

procedure TSettingsNav.DoChangeCobSelection(Sender: TObject; aLengthMode: Byte;
    aLengthWindow: Word);
begin
  if Assigned(FOnChangeCobSelection) then FOnChangeCobSelection(Self, aLengthMode, aLengthWindow);
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.DrawCanvas(aObject: TObject; aCanvas: TCanvas; aRect:
    TRect; aStrAll: string; aSeparator: Boolean; aCallForHeader: Boolean;
    aState: TOwnerDrawState = []; aGroupState: TGroupState = gsfree);
var
  xStrVal, xStrAll: string;
  xCount, xPos1: Integer;
  xRc: TRect;
  xArrWidth: TTabArr;
  xIsSelected: Boolean;
begin
  case fActSourceType of
    stTemplate:           xArrWidth := cTemplateTabs;
    stOrderPosition:      xArrWidth := cStyleTabs;
    stStyle:              xArrWidth := cStyleTabs;
    stProdGrp, stHistory: xArrWidth := cProdGrpTabs;
    stMaMemory:           xArrWidth := cPreselectTabs;
  end; //case

  aCanvas.Brush.Style := bsSolid;

  if not(odSelected in aState) then
    case aGroupState of
      gsInProd, gsLotChange: aCanvas.Brush.Color := TColor($C0FFC0);
      gsMemory:              aCanvas.Brush.Color := TColor($C0FFFF);
    else {gsfree, gsDefined};
    end;

  aCanvas.FillRect(aRect);

  xIsSelected := odSelected in aState;

  if xIsSelected then
    aCanvas.Font.Color := clHighlightText
  else
    aCanvas.Font.Color := clWindowText;

  // Die einzelnen Spalten m�ssen durch ';' getrennt sein
  xStrAll    := aStrAll;
  xRc.Top    := aRect.Top;
  xRc.Bottom := aRect.Bottom;
  xPos1      := 0; //Nue

  xCount := Low(xArrWidth);
  while xCount <= High(xArrWidth) do begin

    // Zeichenbereich f�r Spalte xCount+1 --------------------------------------------
    if xCount = Low(xArrWidth) then begin
      if aCallForHeader and (fActSourceType = stMaMemory) then begin
        xCount := 3; //ACHTUNG!!!! Aenderung der Laufvariablen f�r spez.Fall!!!!!!!!
        xRc.Left := aRect.Left + 2;
        xRc.Right := aRect.Left + xArrWidth[xCount] - 2;
      end
      else begin
        xRc.Left := aRect.Left + 2;
        xRc.Right := aRect.Left + xArrWidth[xCount] - 2;
      end;
    end
    else if (xCount = High(xArrWidth)) or (xArrWidth[xCount + 1] = 0) then begin
      xRc.Left := aRect.Left + xArrWidth[xCount - 1] + 2;
      xRc.Right := aRect.Right - 2;
      xStrAll := Copy(xStrAll, xPos1 + 1, Length(xStrAll) - xPos1);
    end
    else begin
      xRc.Left := aRect.Left + xArrWidth[xCount - 1] + 2;
      xRc.Right := aRect.Left + xArrWidth[xCount] - 2;
      xStrAll := Copy(xStrAll, xPos1 + 1, Length(xStrAll) - xPos1);
    end;

    // Text f�r Spalte ausfiltern
    xPos1 := Pos(';', xStrAll);
    xStrVal := Copy(xStrAll, 1, xPos1 - 1);

    // Text ausgeben
    aCanvas.TextRect(xRc, xRc.Left, xRc.Top, xStrVal);

    // Trennlinie zwischen Spalten zeichnen
    if (xCount <> High(xArrWidth)) and (aSeparator) and
      //Zwischen SpindelBereich und-Gruppe keine Trennzeichen
    not ((((fActSourceType = stProdGrp) or (fActSourceType = stHistory)) and (xCount > 1) and (xCount < 4)) or //Korrektur Spindelbereich bei History und ProdGrp
      ((fActSourceType = stMaMemory) and (xCount > 0) and (xCount < 3))) then begin //Korrektur Spindelbereich bei History und ProdGrp
      aCanvas.MoveTo(xRc.Right, xRc.Top);
      aCanvas.LineTo(xRc.Right, xRc.Bottom);
    end;

    if (xCount < High(xArrWidth)) and (xArrWidth[xCount + 1] = 0) then
      BREAK; //ACHTUNG!!!! Break in WHILE-Loop
    INC(xCount);
  end; //while

  xStrAll := Copy(xStrAll, xPos1 + 1, Length(xStrAll) - xPos1);
////  aCanvas.Brush.Color := clWindow; ///Random($7FFFFFFF); //mTestNueColor; //////
end;

//:---------------------------------------------------------------------------
//Nue: 20.12.06
procedure TSettingsNav.EnableButtons(aEnable: Boolean);
begin
  bMachine.Enabled := aEnable;
  bLot.Enabled := aEnable;
  bTemplate.Enabled := aEnable;
  bStyle.Enabled := aEnable;
  bHistory.Enabled := aEnable;
  bOrderpos.Enabled := aEnable;

  //Nue:9.1.07
  cobChooser.Enabled := aEnable;
  if cobChooser.Enabled then cobChooser.SetFocus;
  if Assigned(FOnEventEnableButtons) then FOnEventEnableButtons(Self);  //Event f�r AssignForm
end;

//:---------------------------------------------------------------------------
function TSettingsNav.FillList(aSource: TSourceTypes): Boolean;
var
  xMaItem: TPreselectItem;
  xProdGrpItem: TProdGrpItem;
//wss  xTemplate: TTemplateItem;
//wss  xStyle: TStyleItem;
  x: Integer;
//  xProdGrpYMPara: TProdGrpYMParaRec;
//  xSettings: TYMSettingsByteArr;
//wss  xTmpcobChooserText: string;
//wss  xTmpchooserHeaderText: string;
  xState: string;
  xXMLSettings: TStringList;
  xGrpNrOrMemory: string;
  xMemoryNr: Integer;

  //local....................................................................
  procedure InitListForm;
  begin
    if not Assigned(mListForm) then
      mListForm := TListCobChooser.Create(TAssign(Self.Owner), fSpdRangeList);
//      mListForm := TListCobChooser.Create(Self, fSpdRangeList);

    if Assigned(fSettings.Model) then 
      mListForm.ActTKClass := YMParaUtils.GetSensingHeadClass(fSettings.Model);

    //mListForm.Settings := fSettings;
    mListForm.SettingsHandler := fSettingsHandler;
  end;
  //end local....................................................................

begin
  Result := False; // wss
  with Query do begin
    Close;
    case aSource of
      stProdGrp: try

  //Start: Keine laufende ProdGrp selektiert  Nue:6.8.03
          if mFirstShow and (SpdRangeList.ActSpdRange is TBaseSpindleRange) and not (SpdRangeList.ActSpdRange is TProdGrp) then begin
            if (SpdRangeList.ActSpdRange as TBaseSpindleRange).NoRunningProdGrp then begin

              CobChooserClearAndFree;
              mFirstShow := False;
              if (Self.Owner is TAssign) then
                (Self.Owner as TAssign).SettingsVisible := False;

  //        InitListForm;
  //        CobChooserClearAndFree;
  //        if (TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable) then begin //acStyle.Enabled
  //          xStyle := mListForm.FirstStyleItem;
  //          if NOT(xStyle=NIL) then begin
  //            cobChooser.Items.AddObject(Format('%s;%s;%s;%s;',
  //              [xStyle.Name, xStyle.YMSetName, cGUISensingHeadClassNames[xStyle.HeadClass], xStyle.AssortmentName]),xStyle);
  //            acStyle.Checked := False;  //Button rausholen
  //            ActSourceType := stStyle;
  //            mFirstShow := False;
  //            cobChooser.ItemIndex := 0;
  //            cobChooser.Enabled := False;
  //            ActCobChooserItem := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TBaseItem); //Nue:13.11.01
  //          end
  //          else begin
  //          end; //else
  //        end
  //        else begin //Template.Enabled
  //          xTemplate := mListForm.FirstTemplateItem;
  //          if NOT(xTemplate=NIL) then begin
  //            cobChooser.Items.AddObject(Format('%s;%s;',
  //              [xTemplate.Name, cGUISensingHeadClassNames[xTemplate.HeadClass]]),xTemplate);
  //            acTemplate.Checked := False;  //Button rausholen
  //            ActSourceType := stTemplate;
  //            mFirstShow := False;
  //            cobChooser.ItemIndex := 0;
  //            cobChooser.Enabled := False;
  //            ActCobChooserItem := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TBaseItem); //Nue:13.11.01
  //          end
  //          else begin
  //          end; //else
  //        end; //ELSE
            end;
          end
  //End: Keine laufende ProdGrp selektiert
          else begin

            CobChooserClearAndFree;
            CommandText := cProdGrpQuery;
            if Assigned(SpdRangeList.ActSpdRange as TMachine) then
              Parameters.ParamByName('c_machine_id').Value := (SpdRangeList.ActSpdRange as TMachine).MachineID
            else
              EXIT; //!!!!!!!!!!!!
            Open;
//wss        x := 0;
            while not EOF do begin // ADO Conform
              xProdGrpItem := TProdGrpItem.Create(FieldByName('c_YM_set_id').AsInteger,
                FieldByName('c_prod_id').AsInteger,
                FieldByName('c_order_position_id').AsInteger,
                FieldByName('c_style_id').AsInteger);
              xProdGrpItem.Name := FieldByName('c_prod_name').AsString;
              xProdGrpItem.Color := FieldByName('c_color').AsInteger;
              xProdGrpItem.MachineName := FieldByName('c_machine_name').AsString;
              xProdGrpItem.StyleName := FieldByName('c_style_name').AsString;
              xProdGrpItem.ProdStart := FieldByName('c_prod_start').AsString;
              xProdGrpItem.SpindleFirst := FieldByName('c_spindle_first').AsString;
              xProdGrpItem.SpindleLast := FieldByName('c_spindle_last').AsString;
              xProdGrpItem.GrpNr := FieldByName('c_machineGroup').AsInteger;
              xProdGrpItem.State := FieldByName('c_prod_state').AsInteger;
              xProdGrpItem.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger);
              xProdGrpItem.YMSetName := FieldByName('c_ym_set_name').AsString;
              xProdGrpItem.YarnCnt := YarnCountConvert(yuNm, fYarnUnit, FieldByName('c_act_yarncnt').AsFloat);
              xProdGrpItem.Slip := Format('%4.3f', [FieldByName('c_slip').AsFloat / 1000.0]);
              xProdGrpItem.StartMode := FieldByName('c_start_mode').AsInteger;

              if xProdGrpItem.State in [1, 3, 5] then
                xState := 'A'
              else
                xState := '-';
  //          cobChooser.Items.AddObject(Format('%s';'%s';'-%s';'(%d)';'%s';'%s';'%s';'%s';'%s';'%s';'%4.1f';'%s',
              cobChooser.Items.AddObject(Format('%s;%s;-%s;(%d);%s;%s;%s;%s;%s;%s;%4.2f;%s;',    //Nue 21.12.06 Format von 4.1 auf 4.2
                [xProdGrpItem.MachineName, xProdGrpItem.SpindleFirst, xProdGrpItem.SpindleLast, xProdGrpItem.GrpNr+1, xProdGrpItem.Name, xProdGrpItem.StyleName,
                cGUISensingHeadClassNames[xProdGrpItem.HeadClass], xProdGrpItem.YMSetName, xProdGrpItem.ProdStart, xState,
                  xProdGrpItem.YarnCnt, xProdGrpItem.Slip]),
                  xProdGrpItem);
                      //          cobChooser.Items.AddObject(xProdGrpItem.Name, xProdGrpItem);
  //          memoCobChooser.Lines.Insert(x, GetTabulatedString(xProdGrpItem));
  //          memoCobChooserList.Insert(x, xProdGrpItem);
            //        cobChooser.Items.AddObject(xProdGrpItem.Name, xProdGrpItem);
              Next;
//wss          inc(x);
            end;

            cobChooser.ItemIndex := 0; //Wenn Count=0 ist, ist cobChooser.ItemIndex nach dieser Anweisung automatisch wieder -1!

            if mDroppedDownCobChooserAllowed then
              cobChooser.DroppedDown := True //Aufklappen der Combobox
            else if cobChooser.ItemIndex >= 0 then //Initialaufruf, wenn Eintrag vorhanden
              ActCobChooserItem := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TBaseItem); //Nue:16.7.03

            acLot.Checked := False; //Button rausholen

          end; //else
        except
          on e: Exception do begin
            raise Exception.Create('TSettingsNav.FillList cProdGrpQuery failed. ' + e.Message);
          end;
        end;
      stMaMemory: try
          CobChooserClearAndFree;
          if StrPas(SettingsHandler.XMLSettingsCollection.XMLData) <> '' then begin
//NUE1          if SettingsHandler.XMLSettingsCollection <> nil then begin
            xXMLSettings := TStringList.Create;
            try
              // Die Liste enth�lt die Informationen wie folgt: "Settings0=XMLData,Settings1=XMLData,etc"
              xXMLSettings.CommaText := StrPas(SettingsHandler.XMLSettingsCollection^.XMLData);
              codesite.SendFmtMsg('TSettingsNav.FillList(stMaMemory): %s',[xXMLSettings.Text]);

              cobChooser.DropDownCount := xXMLSettings.Count; //=>Keine Scrollbar bei cobChooser Nue:10.10.06
              xMemoryNr := 1;

              for x:=0 to xXMLSettings.Count-1 do begin
{ DONE 1 -oNue -cLX-Handler :
F�r LX-Handler: spezielle Behandlung von MemorySettings 
(Unterscheidungskriterium im XML zwischen Memory und regul�ren Settings)
ev. Memories anders einf�rben? }
                with SettingsHandler.XMLSettingsCollection^ do begin
                  if SettingsArr[x].SpindleFirst <> -1 then begin
                    xMaItem := TPreselectItem.Create;
                  //Hier werden die von der Maschine geholten Settings zugewiesen und koennen bei
                  // der Selektion eines Items in der cobChooser zugegriffen werden.
                    xMaItem.Settings     := xXMLSettings.Values[Format(cXMLSettingIniName, [x])];
                    if CodeSite.Enabled then
                      codesite.SendFmtMsg('TSettingsNav.FillList(stMaMemory): GRP:%d %s',[x,FormatXML(xXMLSettings.Values[Format(cXMLSettingIniName, [x])])]);
//                    xMaItem.SettingsLen  := cBinBufferSize;
                    xMaItem.SpindleFirst := IntToStr(SettingsArr[x].SpindleFirst);
                    xMaItem.SpindleLast  := IntToStr(SettingsArr[x].SpindleLast);
                    xMaItem.Name         := Trim(StrPas(@(Settingsarr[x].ProdName)));
                    xMaItem.GrpNr        := SettingsArr[x].Group;
                    xMaItem.LengthMode   := SettingsArr[x].LengthMode;
                    xMaItem.LengthWindow := SettingsArr[x].LengthWindow;
                    xMaItem.PilotSpindles:= SettingsArr[x].PilotSpindles;

                    if not SettingsArr[x].YMSetChanged then begin
                      Close;
                      CommandText := cPreselectQuery;
//NUE1                      Parameters.ParamByName('c_prod_id').Value := xProdGrpYMPara.prodGrpID;
                      Parameters.ParamByName('c_prod_id').Value := SettingsArr[x].ProdGrpID;
                      Open;
                      if not EOF then // ADO Conform
                        xMaItem.YMSetName := FieldByName('c_YM_set_name').AsString;
                    end
                    else
                      xMaItem.YMSetName := cChangedOnZE;

                    // 15.07.2005 wss: Beim Upload wird ja der YarnCount nach Nm konvertiert.
                    // Demzufolge muss hier von Nm nach Global umgerechnet werden wie wenn der Wert von der DB gelesen wird
                    xMaItem.YarnCnt := YarnCountConvert(yuNm, fYarnUnit, SettingsArr[x].YarnCnt);
//                    xMaItem.YarnCnt := YarnCountConvert(SettingsArr[x].YarnCntUnit, fYarnUnit, SettingsArr[x].YarnCnt);  //27.9.04 nue: No more cYarnCntFactor
                    xMaItem.State := SettingsArr[x].GroupState; //Nue:8.10.06

                    xGrpNrOrMemory := IntToStr(xMaItem.GrpNr+1);
                    if SettingsArr[x].GroupState in [gsInProd, gsLotChange] then begin
                      xState := 'A';
                    end
                    else if SettingsArr[x].GroupState in [gsMemory] then begin
                      xState := 'M';
                      xGrpNrOrMemory := Format('Memory%d',[xMemoryNr]);
                      Inc(xMemoryNr);
                    end
                    else begin
                      xState := '';
                    end;

//                    cobChooser.Items.AddObject(Format('%s; -%s;(%d);%s;%s;%s;%4.1f;',
//                      [xMaItem.SpindleFirst, xMaItem.SpindleLast, xMaItem.GrpNr+1, xMaItem.Name,
//                      xMaItem.YMSetName, xState, xMaItem.YarnCnt]), xMaItem);
                    cobChooser.Items.AddObject(Format('%s; -%s;(%s);%s;%s;%s;%4.2f;',    //Nue 21.12.06 Format von 4.1 auf 4.2
                      [xMaItem.SpindleFirst, xMaItem.SpindleLast, xGrpNrOrMemory, xMaItem.Name,
                      xMaItem.YMSetName, xState, xMaItem.YarnCnt]), xMaItem);
                  end; //if
                end; // with SettingsHandler.XMLSettingsCollection^
              end; //for

              cobChooser.ItemIndex := 0;
              if mDroppedDownCobChooserAllowed then begin
                cobChooser.DroppedDown := True; //Aufklappen der Combobox
//Nue:9.1.07                cobChooser.SetFocus;
              end;
            finally
              xXMLSettings.Free;
              acMachine.Checked := False; //Button rausholen
            end; //with try
          end //if SettingsHandler.SettingsArr
          else begin
            raise Exception.Create('Settings Array is nil. ');
          end;
    cobChooser.Invalidate; //Nue:10.10.06
    pbchooserHeaderPaint(self); //Nue:14.12.06
        except
          on e: Exception do
            raise Exception.Create('TSettingsNav.FillList cPreselectQuery failed. ' + e.Message);
        end; // stMaMemory

    else // case
        //Alle Sourcen welche �ber ListCobChooser gehen (nicht �ber cobChooser)
      //      if (Assigned(fSpdRangeList)) then begin
      InitListForm;
      mListForm.FillList(aSource);

          //Aufruf ListcobChooser f�r alle enstprechenden Sourcen
      if (mListForm.ShowModal = mrOk) then begin
        GetSelection(Self);

            //Settings wieder visible schalten (Wird auch noch in acShapeBtnExecute gemacht!)
        if (Self.Owner is TAssign) and (not mFirstShow) then
          (Self.Owner as TAssign).SettingsVisible := True;

        CobSelectionChanged; //Nue:03.07.2003
    cobChooser.Invalidate; //Nue:10.10.06
    pbchooserHeaderPaint(self); //Nue:14.12.06
      end
      else begin //Cancel
            //Zur�cksetzen des Sourcestatus
        fActSourceType := fPrevSourceType;
        if (fActSourceType = stProdGrp) or (fActSourceType = stMaMemory) then
          cobChooser.Enabled := True;
      end;

    end; //case
  end; //with Query
end;

//:---------------------------------------------------------------------------
function TSettingsNav.GetActColor: TColor;
begin
  Result := fActColor;
end;

//:---------------------------------------------------------------------------
function TSettingsNav.GetActPreselectItemSettings: TYMSettingsByteArr;

  //Nue:29.08.01

begin
  if (fActSourceType = stMaMemory) then
//NUE1 Wieder ausklammern    Result := (cobChooser.Items.Objects[cobChooser.ItemIndex] as TPreselectItem).Settings
  else
    FillChar(Result, sizeof(Result), 0);
end;

//:---------------------------------------------------------------------------
function TSettingsNav.GetMiscInfo: IMiscInfo;
begin
  Result := fOrderPosition;
  if cobChooser.Items.Count > 0 then begin
    if cobChooser.Items.Objects[0] is TProdGrpItem then begin
      mProdGrp.ProdGrpID := TProdGrpItem(cobChooser.Items.Objects[cobChooser.ItemIndex]).ProdGrpID;
      Result := mProdGrp;
    end
        //Neu Nue 22.05.01
    else if cobChooser.Items.Objects[0] is TStyleItem then begin
      fOrderPosition.StyleID := TStyleItem(cobChooser.Items.Objects[cobChooser.ItemIndex]).StyleID;
      Result := fOrderPosition;
    end
        //@@Nue 20.06.01  temporaer!!
    else if cobChooser.Items.Objects[0] is TTemplateItem then begin
      fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
      fOrderPosition.YarnCnt := 0;
      Result := fOrderPosition;
    end
    else if cobChooser.Items.Objects[0] is TPreselectItem then begin
      fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
      fOrderPosition.YarnCnt := TPreselectItem(cobChooser.Items.Objects[cobChooser.ItemIndex]).YarnCnt; //27.9.04 nue: No more Div cYarnCntFactor
      fOrderPosition.Slip := '@';
      Result := fOrderPosition;
    end;
  //@@Nue 20.06.01 ueberarbeiten mit TTemplateItem usw. neuer Typ TStyle und TTemplate im AssignComp erstellen analog TOrderPosition

  end;
end;

//:---------------------------------------------------------------------------
function TSettingsNav.GetProdGrpID: Integer;
begin
  Result := mProdGrp.ProdGrpID;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.GetSelection(Sender: TObject);
var
//wss  xIsSelected: Boolean;
  xTemplate: TTemplateItem;
  xStyle: TStyleItem;
  xProdGrp: TProdGrpItem;
  xState: string;
begin

  EnterMethod('TSettingsNav.GetSelection');
  //  //Neu hier (statt am Ende der Procedure) wegen Timeproblems 15.04.03
  //  ActCobChooserItem := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol,mListForm.mExtStringGrid.Row] as TBaseItem); //Nue:13.11.01

  CobChooserClearAndFree;

      //History
  if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] is TProdGrpItem then begin
    xProdGrp := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TProdGrpItem);
    if xProdGrp.State in [1, 3, 5] then
      xState := 'A'
    else
      xState := '-';
    cobChooser.Items.AddObject(Format('%s;%s;-%s;(%d);%s;%s;%s;%s;%s;%s;%4.1f;%s;',
      [xProdGrp.MachineName, xProdGrp.SpindleFirst, xProdGrp.SpindleLast, xProdGrp.GrpNr+1, xProdGrp.Name, xProdGrp.StyleName,
      cGUISensingHeadClassNames[xProdGrp.HeadClass], xProdGrp.YMSetName, xProdGrp.ProdStart, xState, xProdGrp.YarnCnt, xProdGrp.Slip]),
        xProdGrp);
    acHistory.Checked := False; //Button rausholen
  end;
      //Template
  if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] is TTemplateItem then begin
    xTemplate := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TTemplateItem);
  //      cobChooser.Text := Format('%s'#9'%s',
    cobChooser.Items.AddObject(Format('%s;%s;',
      [xTemplate.Name, cGUISensingHeadClassNames[xTemplate.HeadClass]]), xTemplate);
    acTemplate.Checked := False; //Button rausholen
  end;
      //Style
  if mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] is TStyleItem then begin
    xStyle := (mListForm.mExtStringGrid.Objects[mListForm.GridObjectCol, mListForm.mExtStringGrid.Row] as TStyleItem);
  //      cobChooser.Text := Format('%s'#9'%s'#9'%s'#9'%s',
  //      cobChooser.Items.AddObject(Format('%s;%s;%s;%s;',
  //        [xStyle.Name, xStyle.YMSetName, cGUISensingHeadClassNames[xStyle.HeadClass], xStyle.AssortmentName]),xStyle);
    cobChooser.Items.AddObject(Format('%s;%s;%s;%s;%4.1f;%s;',
      [xStyle.Name, xStyle.YMSetName, cGUISensingHeadClassNames[xStyle.HeadClass], xStyle.AssortmentName, xStyle.YarnCnt, xStyle.Slip]), xStyle);
    acStyle.Checked := False; //Button rausholen
  end;
  cobChooser.ItemIndex := 0;

  ///////    ActCobChooserItem := (mListForm.mExtStringGrid.Objects[GridObjectCol,mListForm.mExtStringGrid.Row] as TBaseItem); //Nue:13.11.01
end;

//:---------------------------------------------------------------------------
//wss function TSettingsNav.GetTabulatedString(aBaseItem: TBaseItem): string;
//var
//  xProdGrp: TProdGrpItem;
//  xPreselect: TPreselectItem;
//  xState: string;
//begin
//  if aBaseItem is TProdGrpItem then begin
//    xProdGrp := (aBaseItem as TProdGrpItem);
//    if xProdGrp.State in [1, 3, 5] then
//      xState := 'A'
//    else
//      xState := '-';
//    Result := Format('%s'#9'%s'#9'-%s'#9'(%d)'#9'%s'#9'%s'#9'%s'#9'%s'#9'%s'#9'%s'#9'%4.1f'#9'%s',
//      [xProdGrp.MachineName,xProdGrp.SpindleFirst,xProdGrp.SpindleLast,xProdGrp.GrpNr,xProdGrp.Name,xProdGrp.StyleName,
//       cGUISensingHeadClassNames[xProdGrp.HeadClass],xProdGrp.YMSetName,xProdGrp.ProdStart,xState,xProdGrp.YarnCnt,xProdGrp.Slip]);
//  end;
//  if aBaseItem is TPreselectItem then begin
//    xPreselect := (aBaseItem as TPreselectItem);
//    Result := Format('%s'#9' -%s'#9'(%d)'#9'%s'#9'%s'#9'%s',
//      [xPreselect.SpindleFirst, xPreselect.SpindleLast, xPreselect.GrpNr, xPreselect.Name,
//       xPreselect.YMSetName, xPreselect.State]);
//  end;
//end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.InitShapeBtn(Sender: TObject);
begin
  bTemplate.Color := clBtnFace;
  bLot.Color := clBtnFace;
  bStyle.Color := clBtnFace;
  bMachine.Color := clBtnFace;
  bOrderPos.Color := clBtnFace;
  bHistory.Color := clBtnFace;

  bTemplate.Down := False;
  bLot.Down := False;
  bStyle.Down := False;
  bMachine.Down := False;
  bOrderPos.Down := False;
  bHistory.Down := False;

  acTemplate.Checked := False;
  acLot.Checked := False;
  acStyle.Checked := False;
  acMachine.Checked := False;
  acOrderPos.Checked := False;
  acHistory.Checked := False;

  (Sender as TAction).Checked := True;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.Loaded;
var
  xTick: Cardinal;
begin
  inherited Loaded;
  mFirstShow := True;

  xTick := GetTickCount;
  if not (csDesigning in ComponentState) then begin
    while 1 = 1 do begin
      if (GetTickCount - xTick) > 10000 then begin
        ODS('TSettingsNav.Loaded: Timeout'); //OutputDebugString   (Kann z.B. mit Sysinternals-DebugView visualisiert werden)
        codesite.SendMsg('TSettingsNav.Loaded: Timeout');
        BREAK;
      end;
      if Self.Owner <> nil then begin
        if (Self.Owner is TAssign) then begin
          ODS('TSettingsNav.Loaded: Self.Owner is TAssign'); //OutputDebugString   (Kann z.B. mit Sysinternals-DebugView visualisiert werden)
          codesite.SendMsg('TSettingsNav.Loaded: Self.Owner is TAssign');
          acShapeBtnExecute(acLot);
          BREAK;
        end;
      end;
      Sleep(0); //Gibt Processkontrolle kurz ab
      Application.ProcessMessages; //Damit MessageQueue behandelt wird
      ODS('TSettingsNav.Loaded: Application.ProcessMessages'); //OutputDebugString   (Kann z.B. mit Sysinternals-DebugView visualisiert werden)
      codesite.SendMsg('TSettingsNav.Loaded: Application.ProcessMessages');
    end;
  end;
end;


//:---------------------------------------------------------------------------
procedure TSettingsNav.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then begin
    if Assigned(fSpdRangeList) and (AComponent = fSpdRangeList) then
      fSpdRangeList := nil;
    if Assigned(fQuery) and (AComponent = fQuery) then
      fQuery := nil;
    if Assigned(fSettings) and (AComponent = fSettings) then
      fSettings := nil;
    if Assigned(fSettingsHandler) and (AComponent = fSettingsHandler) then
      fSettingsHandler := nil;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.pbchooserHeaderPaint(Sender: TObject);
var
  xStr: string;
  xRect: TRect;
begin
  if (csDesigning in ComponentState) then exit;

  xRect.Left := 0;
  xRect.Top := 0;
  xRect.Right := pbchooserHeader.Width;
  xRect.Bottom := pbchooserHeader.Height;

  case fActSourceType of
    stProdGrp, stHistory:
      xStr := Format('%s;;;;%s;%s;%s;%s;%s;%s;%s;%s;',
        [cMachine, cLotName, cStyle1, cTKClass, cTemplateName, cLotStart, cState,
        Format(cYarnCount, [cYarnUnitsStr[fYarnUnit]]), cSlip]);
    stTemplate:
      xStr := Format('%s;%s;', [cTemplateName, cTKClass]);
    stStyle:
      xStr := Format('%s;%s;%s;%s;%s;%s;', [cStyle1, cTemplateName, cTKClass, cAssortment, Format(cYarnCount, [cYarnUnitsStr[fYarnUnit]]), cSlip]);
    stMaMemory:
      xStr := Format('%s;%s;%s;%s;%s;', [cMachine, cLotName, cTemplateName, cState, Format(cYarnCount, [cYarnUnitsStr[fYarnUnit]])]);
  end; //case

  DrawCanvas(pbChooserHeader, pbChooserHeader.Canvas, xRect, xStr, False {Trennzeichen}, True {HeaderCall});

end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.rbClick(Sender: TObject);
begin
  CobSourceChanged(Sender);
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.RefreshList;
//var
//wss  xIndex: Integer;
begin
  ///  xIndex := mListForm.mExtStringGrid.;
  UpdateActList;
  ///  cobChooser.ItemIndex := xIndex;
  CobSelectionChanged; //Nue:04.09.2001
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetActCobChooserItem(aActCobChooserItem: TBaseItem);

  //Represents the actual selected item in cobChooser Nue:13.11.01

begin
  fActCobChooserItem := aActCobChooserItem;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetDropDownCobChooser(const Value: Boolean);
begin
  { TODO -oNue -c1 : Ueberarbeiten  mit LOK }
//    if mFirstShow and SpdRangeList.CallByMachine then begin  //Wenn Aufruf von Maschine, Combobox zur Auswahl bereitstellen
//      cobChooser.DroppedDown := True;   //Aufklappen der Combobox
//      cobChooser.SetFocus;
//      mFirstShow := False;
//    end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetProdGrpID(const aProdGrpID: Integer);
begin
  mProdGrp.ProdGrpID := aProdGrpID;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetProdGrpName(aProdGrpName: string);
begin
  fProdGrpName := aProdGrpName;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetQuery(aQuery: TmmAdoDataSet);
begin
  try
    fQuery := aQuery;
    if Assigned(fQuery) then begin
      if Assigned(fOrderPosition) then
        fOrderPosition.Free;
      fOrderPosition := TOrderPosition.Create(fQuery);
      if Assigned(mProdGrp) then
        mProdGrp.Free;
      mProdGrp := TProdGrp.Create(fQuery);
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TSettingsNav.SetQuery failed. ' + e.Message);
    end;
  end;
end;

procedure TSettingsNav.SetSettingsHandler(const Value: TGetSettingsHandler);
begin
  fSettingsHandler := Value;
end;

//:---------------------------------------------------------------------------
function TSettingsNav.SetSourceTypeTemplateOrStyle: TSourceTypes;

  //Nue:6.3.02

begin
  if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then
    Result := stStyle
  else
    Result := stTemplate;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if not (csDesigning in ComponentState) then begin
    if Assigned(fSpdRangeList) then begin
      fSpdRangeList.add(self);
      if not Assigned(mListForm) then
        mListForm := TListCobChooser.Create(self, fSpdRangeList);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.SetUnMachineCobChooser(const Value: Boolean);
begin
  if Value <> fUnMachineCobChooser then begin
    fUnMachineCobChooser := Value;
    cobChooser.Invalidate;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.ShapeBtnMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mDroppedDownCobChooserAllowed := True;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.UpdateActList;
begin
  try
    case fActSourceType of
      stTemplate: CobSourceChanged(acTemplate);
      stOrderPosition: CobSourceChanged(acOrderPos);
      stStyle: CobSourceChanged(acStyle);
      stProdGrp: CobSourceChanged(acLot);
      stHistory: CobSourceChanged(acHistory);
      stMaMemory: CobSourceChanged(acMachine);
    end; //case
  except
    on e: Exception do begin
      SystemErrorMsg_('TSettingsNav.UpdateList failed. ' + e.Message);
    end;
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss procedure TSettingsNav.UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
var
  x: Integer;
  xProdID: Integer;
begin
  EnterMethod('TSettingsNav.UpdateComponent');
  try
    if csDesigning in ComponentState then Exit;

    if Assigned(aSpdRange) and not (fActSourceType = stMaMemory) and not (fPrevSourceType = stMaMemory) then begin
      if (aSpdRange is TProdGrp) then begin
        fPrevSourceType := SetSourceTypeTemplateOrStyle;
        mDroppedDownCobChooserAllowed := False; //Um zu verhindern, dass cobChooser bereits DroppedDown kann

        if not((SpdRangeList.AssMachine.FixSpdRanges) and (SpdRangeList.GettingSettingsFromMachine)) then begin //Nue:15.11.05
          FillList(stProdGrp);
          ActSourceType := stProdGrp;
          cobChooser.Invalidate; //Nue:8.7.03
          pbchooserHeaderPaint(self); //Nue:8.7.03

          // Temp bis Beziehungen von Style und OrderPosition klar fOrderPosition.OrderPositionID := (aSpdRange as TProdGrp).OrderPositionID;
          fOrderPosition.StyleID := (aSpdRange as TProdGrp).StyleID;
          //@@Nue Style parts einbauen?
          fProdGrpName := (aSpdRange as TProdGrp).Name;
          fActColor := (aSpdRange as TProdGrp).Color;

        end;

        // Mark actual ProdGrp in Combobox
        codesite.SendMsg('TSettingsNav.UpdateComponent: (1) acLot.Checked');

        if (cobChooser.Items.Objects[0] is TProdGrpItem) then begin //Nue: 16.11.05  nur if neu

          for x := 0 to cobChooser.Items.Count - 1 do begin
            xProdID := (cobChooser.Items.Objects[x] as TProdGrpItem).ProdGrpID;
            if xProdID = (aSpdRange as TProdGrp).ProdGrpID then begin
              cobChooser.ItemIndex := x;
            end;
          end; //for
        end; //if

//        // Temp bis Beziehungen von Style und OrderPosition klar fOrderPosition.OrderPositionID := (aSpdRange as TProdGrp).OrderPositionID;
//        fOrderPosition.StyleID := (aSpdRange as TProdGrp).StyleID;
//        //@@Nue Style parts einbauen?
//        fProdGrpName := (aSpdRange as TProdGrp).Name;
//        fActColor := (aSpdRange as TProdGrp).Color;
      end //if (aSpdRange is TProdGrp) then begin
      //Start: Keine laufende ProdGrp selektiert  Nue:6.8.03
      else if (aSpdRange is TBaseSpindleRange) and mFirstShow then begin
        //Wenn keine ProdGrp gestartet ist, wird Style oder Template angezeigt
        mDroppedDownCobChooserAllowed := False; //Um zu verhindern, dass cobChooser bereits DroppedDown kann
        FillList(stProdGrp);
        //ActSourceType := (Wird in FillList gesetzt);
        cobChooser.Invalidate; //Nue:8.7.03
        pbchooserHeaderPaint(self); //Nue:8.7.03
      end; //else if (aSpdRange is TBaseSpindleRange) then begin
      //End: Keine laufende ProdGrp selektiert
    end //if Assigned(aSpdRange)

    else if Assigned(aSpdRange) and (aSpdRange is TProdGrp) and ((fActSourceType = stMaMemory) or (fPrevSourceType = stMaMemory)) then begin
      // Mark actual MaGrp in Combobox
      if (not (fActSourceType = stMaMemory)
          and not (fPrevSourceType = stMaMemory)) then  //Nue:15.11.05 neu
        FillList(stMaMemory);
    end

    else if not Assigned(aMiscInfo) then begin
      if (fActSourceType = stMaMemory) then begin
        fPrevSourceType := stMaMemory;
        codesite.SendMsg('TSettingsNav.UpdateComponent: (3) acMachine.Checked (No find ProdID)');
      end
      else begin
        fPrevSourceType := SetSourceTypeTemplateOrStyle;
        codesite.SendMsg('fPrevSourceType := SetSourceTypeTemplateOrStyle (4)');
      end;
      if (fActSourceType = stStyle) then begin
        acShapeBtnExecute(acStyle);
        codesite.SendMsg('TSettingsNav.UpdateComponent: (5) acStyle.Checked');
      end;
      if (fActSourceType = stTemplate) then begin
        fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
        fProdGrpName := fOrderPosition.OrderPositionName;
        acShapeBtnExecute(acTemplate);
        codesite.SendMsg('TSettingsNav.UpdateComponent: (6) acTemplate.Checked');
      end;
    end;

    if Assigned(aSpdRange) then begin
      acMachine.Enabled := aSpdRange.IsAbleToGetMachineSettings;
      codesite.SendMsg('TSettingsNav.UpdateComponent: (7) acMachine.Enabled := aSpdRange.IsAbleToGetMachineSettings');
    end;
  finally
   // LOK (7.11.2002): TSettingsNav wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
   // ==> Hier wieder einblenden
    visible := True;
  end;

//  EnterMethod('TSettingsNav.UpdateComponent');
//  try
//    if csDesigning in ComponentState then Exit;
//
//  //    if Assigned(aSpdRange) and (aSpdRange is TProdGrp)
//  //      and not (fActSourceType=stMaMemory) and not (fPrevSourceType = stMaMemory) then begin
//    if Assigned(aSpdRange)
//      and not (fActSourceType = stMaMemory) and not (fPrevSourceType = stMaMemory) then begin
//      if (aSpdRange is TProdGrp) then begin
//        fPrevSourceType := SetSourceTypeTemplateOrStyle;
//        mDroppedDownCobChooserAllowed := False; //Um zu verhindern, dass cobChooser bereits DroppedDown kann
//    //Nue:9.7.03      if not(fActSourceType=stProdGrp) then begin // Wenn ProdGrp nicht aktiv, wird die Liste automatisch erneuert
//        FillList(stProdGrp);
//        ActSourceType := stProdGrp;
//        cobChooser.Invalidate; //Nue:8.7.03
//        pbchooserHeaderPaint(self); //Nue:8.7.03
//    //       acLot.Checked := True;
//    //Nue:9.7.03      end;
//
//          // Mark actual ProdGrp in Combobox
//        codesite.SendMsg('TSettingsNav.UpdateComponent: (1) acLot.Checked');
//        for x := 0 to cobChooser.Items.Count - 1 do begin
//          xProdID := (cobChooser.Items.Objects[x] as TProdGrpItem).ProdGrpID;
//          if xProdID = (aSpdRange as TProdGrp).ProdGrpID then begin
//            cobChooser.ItemIndex := x;
//          end;
//        end; //for
//
//          // Temp bis Beziehungen von Style und OrderPosition klar fOrderPosition.OrderPositionID := (aSpdRange as TProdGrp).OrderPositionID;
//        fOrderPosition.StyleID := (aSpdRange as TProdGrp).StyleID;
//      //@@Nue Style parts einbauen?
//        fProdGrpName := (aSpdRange as TProdGrp).Name;
//        fActColor := (aSpdRange as TProdGrp).Color;
//      end //if (aSpdRange is TProdGrp) then begin
//  //Start: Keine laufende ProdGrp selektiert  Nue:6.8.03
//      else if (aSpdRange is TBaseSpindleRange) and mFirstShow then begin
//        //Wenn keine ProdGrp gestartet ist, wird Style oder Template angezeigt
//        mDroppedDownCobChooserAllowed := False; //Um zu verhindern, dass cobChooser bereits DroppedDown kann
//        FillList(stProdGrp);
//          //ActSourceType := (Wird in FillList gesetzt);
//        cobChooser.Invalidate; //Nue:8.7.03
//        pbchooserHeaderPaint(self); //Nue:8.7.03
//      end; //else if (aSpdRange is TBaseSpindleRange) then begin
//  //End: Keine laufende ProdGrp selektiert
//    end //if Assigned(aSpdRange)
//
//    else if Assigned(aSpdRange) and (aSpdRange is TProdGrp) and ((fActSourceType = stMaMemory) or (fPrevSourceType = stMaMemory)) then begin
//          // Mark actual MaGrp in Combobox
//      if not (fActSourceType = stMaMemory) then begin
//        FillList(stMaMemory);
//  //        acMachine.Checked := True;
//      end;
//      for x := 0 to cobChooser.Items.Count - 1 do begin
//        xProdID := (cobChooser.Items.Objects[x] as TPreselectItem).GrpNr;
//        if xProdID = (aSpdRange as TProdGrp).GrpNr then begin
//          cobChooser.ItemIndex := x;
//        end;
//      end;
//    end
//
//    else if not Assigned(aMiscInfo) then begin
//      if (fActSourceType = stMaMemory) then begin
//        fPrevSourceType := stMaMemory;
//        codesite.SendMsg('TSettingsNav.UpdateComponent: (3) acMachine.Checked (No find ProdID)');
//      end
//      else begin
//        fPrevSourceType := SetSourceTypeTemplateOrStyle;
//        codesite.SendMsg('fPrevSourceType := SetSourceTypeTemplateOrStyle (4)');
//      end;
//      if (fActSourceType = stStyle) then begin
//  //        CobSourceChanged(acStyle);
//        acShapeBtnExecute(acStyle); //Nue:12.06.03 notwendig???
//
//        codesite.SendMsg('TSettingsNav.UpdateComponent: (5) acStyle.Checked');
//      end;
//      if (fActSourceType = stTemplate) then begin
//        fOrderPosition.OrderPositionID := cDefaultOrderPositionID;
//        fProdGrpName := fOrderPosition.OrderPositionName;
//  //        CobSourceChanged(acTemplate);
//        acShapeBtnExecute(acTemplate); //Nue:12.06.03 notwendig???
//  //        CobSourceChanged(acTemplate); //Nue:15.01.02: Ohne diesen call werden beim die TK8xx-Template in cobchooser
//                                               //   geladen anstelle hoeherer TK-Klassen, wenn die gewaehlte Gruppe nicht assignt ist!!!!????
//        codesite.SendMsg('TSettingsNav.UpdateComponent: (6) acTemplate.Checked');
//      end
//      else begin
//  //        Sleep(1000);
//  //        acShapeBtnExecute(acLot);
//      end;
//    end;
//
//    if Assigned(aSpdRange) then begin
//      acMachine.Enabled := aSpdRange.IsAbleToGetMachineSettings;
//        //else
//         //rbPreselect.Enabled  := false;
//      codesite.SendMsg('TSettingsNav.UpdateComponent: (7) acMachine.Enabled := aSpdRange.IsAbleToGetMachineSettings');
//    end;
//  finally
//        // LOK (7.11.2002): TSettingsNav wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
//        // ==> Hier wieder einblenden
//    visible := True;
//  ///    (Owner as TAssign).Settings1.UpdateComponent(aSpdRange,aMachSettings,aMiscInfo); //////////////////////////////
//  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsNav.WndProc(var msg: TMessage);
begin
  if msg.msg = mIntWindowMsg then begin
    case msg.wParam of
      COB_SOURCE_CHANGED:
        case TSourceTypes(msg.LParam) of
          stTemplate,
            stOrderPosition,
            stHistory,
            stStyle,
            stProdGrp: SendMessage(self.Handle, mIntWindowMsg, COB_DATA_AVAILABLE, msg.LParam);
          stMaMemory: begin
              EnableButtons(False);  //Nue:5.11.06
              SettingsHandler.StartGetSettings((self as TWinControl).Handle);
            end;
        else
        end;
      COB_DATA_AVAILABLE: begin
          FillList(TSourceTypes(msg.LParam));
          CobSelectionChanged;
  //            FillList(TSourceTypes(msg.LParam)); //Nue:6.3.02 Added because of timeproblem; some parts not ready at first call above
        end;
    else
    end;
  end
  else if msg.Msg = mAssWindowMsg then begin
    case msg.WParam of
      WM_GETSETTINGS_DONE: begin
          FillList(stMaMemory);
          CobSelectionChanged;
          EnableButtons(True);  //Nue:5.11.06
        end;
      WM_GETSETTINGS_TIMEOUT: begin
          UserErrorMsg(cTimeoutMsg, Owner);
              //Nue: 8.4.02
          if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then
            bStyle.SetFocus
          else
            bTemplate.SetFocus;
          EnableButtons(True);  //Nue:5.11.06
        end;
      WM_GETSETTINGS_FAILED: begin
          UserErrorMsg(cSettingsNotOK, Owner);
          Screen.Cursor := crDefault; //Nue:11.12.02
              //Nue: 8.4.02
          if TMMSettingsReader.Instance.IsComponentStyle and not TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then
            bStyle.SetFocus
          else
            bTemplate.SetFocus;
          EnableButtons(True);  //Nue:5.11.06
        end;
    else
    end;
  end
  else
    inherited WndProc(msg);
end;

end.

