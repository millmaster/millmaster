(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmDbValCb.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
  Componente wurde von Peter Worbis fuer MMStyle eingefuehrt. Sie enthaelt eine
  wichtiges zusaetzliches Propertie: Values. In Items werden die Strings
  eingefuegt, welche in der Auswahlliste erscheinen. In Values werden ID's
  eingefuegt, welche dem Wert in Feld DataField entspricht. Auf der DB sind z.B.
  folgende Daten forhanden, welche einen Zustand als Aufzaehlung definiert: ID =
  1 oder 2

  Mit folgenden Property Werten werden die ID-Werte in Texte konvertiert und
  dargestellt.
  Values     Bedeutung
  1          Maschine laeuft
  2          Mascine offline


  Funktion wurde so Erweitert, dass der Zustand Edit/Read Only visualisiert
  wird.

  ShowMode = smNormal
    Komponente verhaelt sich gleich wie TmmComboBox plus zusaetzliche
    Funktionalitaet

  ShowMode = smExtended
  Komponente ist im erweiterten Modus. In diesem Mode ist das inherited Enabled
  immer auf True, da die Darstellung bei False "geisterhaft" erscheint.

  Enabled = False
    Die Farbe wird auf clMMReadOnlyColor gesetzt und darunter ReadOnly = True
  Enabled = True
      Die Farbe wird auf die Originalfarbe gesetzt und darunter ReadOnly = False

|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 10.04.2001  1.00  Wss | Initial Release
                          Show component state Edit/ReadOnly implemented
| 19.12.2001  1.03  Wss | Extended ShowMode changed to use ReadOnly state
| 17.01.2002  1.04  Wss | Extended ShowMode moved to descendant mmComboBox
|=========================================================================================*)
unit mmDbValCb;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Menus, Dialogs, StdCtrls, DB, DBCtrls,
  LoepfeGlobal, mmComboBox;

type
  {:----------------------------------------------------------------------------
   }
  TmmDBValComboBox = class(TmmComboBox)
  private
    fDataLink: TFieldDataLink;
    fOnChange: TNotifyEvent;
    FValue: PString;
    fValues: TStrings;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure DataChange(Sender: TObject);
    function GetButtonValue(Index: Integer): string;
    function GetDataField: string;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetValue: string;
    procedure SetDataField(const Value: string);
    procedure SetDataSource(Value: TDataSource);
    procedure SetValue(const Value: string);
    procedure SetValues(Value: TStrings);
    procedure UpdateData(Sender: TObject);
  protected
    procedure Change; override;
    procedure Click; override;
    procedure DropDown; override;
    procedure KeyPress(var Key: Char); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    property DataLink: TFieldDataLink read fDataLink;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Field: TField read GetField;
    property ItemIndex;
    property Value: string read GetValue write SetValue;
  published
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property OnChange: TNotifyEvent read fOnChange write fOnChange;
    property Values: TStrings read fValues write SetValues;
  end;


procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('MillMaster', [TmmDBValComboBox]);
end;

{:------------------------------------------------------------------------------
 TmmDBValComboBox}
{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.Change;
begin
  inherited Change;
  if Assigned(FOnChange) then FOnChange(Self);
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.Click;
begin
  if FDataLink.Edit then begin
    inherited Click;
    FDataLink.Modified;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.CMExit(var Message: TCMExit);
begin
  try
    FDataLink.UpdateRecord;
  except
    SetFocus;
    raise;
  end;
  inherited;
end;

{:-----------------------------------------------------------------------------}
constructor TmmDBValComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Style := csDropDownList;

  FDataLink := TFieldDataLink.Create;
  FDataLink.Control := Self;
  FDataLink.OnDataChange := DataChange;
  FDataLink.OnUpdateData := UpdateData;

  FValue := NullStr;
  FValues := TStringList.Create;

end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.DataChange(Sender: TObject);
begin
  if FDataLink.Field <> nil then
    ItemIndex := FValues.IndexOf(FDataLink.Field.Text)
  else
    ItemIndex := -1;
end;

{:-----------------------------------------------------------------------------}
destructor TmmDBValComboBox.Destroy;
begin
  FDataLink.Free;
  FDataLink := nil;
  DisposeStr(FValue);
  FValues.Free;
  inherited Destroy;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.DropDown;
begin
  FDataLink.Edit;
  inherited DropDown;
end;

{:-----------------------------------------------------------------------------}
function TmmDBValComboBox.GetButtonValue(Index: Integer): string;
begin
  if (Index < FValues.Count) and (FValues[Index] <> '') then
    Result := FValues[Index]
  else if (Index < Items.Count) then
    Result := Items[Index]
  else
    Result := '';
end;

{:-----------------------------------------------------------------------------}
function TmmDBValComboBox.GetDataField: string;
begin
  Result := FDataLink.FieldName;
end;

{:-----------------------------------------------------------------------------}
function TmmDBValComboBox.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;

{:-----------------------------------------------------------------------------}
function TmmDBValComboBox.GetField: TField;
begin
  Result := FDataLink.Field;
end;

{:-----------------------------------------------------------------------------}
function TmmDBValComboBox.GetValue: string;
begin
  Result := FValue^;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  case Key of
    #32..#255:
      if not FDataLink.Edit then Key := #0;
    #27:
      FDataLink.Reset;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.Notification(AComponent: TComponent; Operation:
  TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and
    (AComponent = DataSource) then DataSource := nil;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.SetDataField(const Value: string);
begin
  FDataLink.FieldName := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.SetDataSource(Value: TDataSource);
begin
  FDataLink.DataSource := Value;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.SetValue(const Value: string);
var
  I: Integer;
begin
  AssignStr(FValue, Value);
  if (ItemIndex < 0) or (GetButtonValue(ItemIndex) <> Value) then begin
    if (ItemIndex >= 0) then ItemIndex := -1;
    for I := 0 to Items.Count - 1 do begin
      if GetButtonValue(I) = Value then begin
        ItemIndex := I;
        break;
      end;
    end;
    Change;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.SetValues(Value: TStrings);
begin
  FValues.Assign(Value);
  DataChange(Self);
end;

{:-----------------------------------------------------------------------------}
procedure TmmDBValComboBox.UpdateData(Sender: TObject);
begin
  if items.IndexOf(Text) <> -1 then
    FDataLink.Field.Text := FValues[items.IndexOf(Text)]
  else
    FDataLink.Field.Text := '';
end;

{:-----------------------------------------------------------------------------}

end.

