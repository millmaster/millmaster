(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: CloseHandlerComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 14.07.2000  1.13  Mg  | ProcessCloseDoneMsg : First ErrMsg then Close Animation Window
| 09.04.2002  1.14  Wss | In TCloseHandler.Create distinguish between Floor and normal use of subwindows
| 26.01.2004  1.15  Wss | Folgefenster werden nur zur Laufzeit erstellt und im Destroy ignoriert, da
                          diese �ber die Komponentenverwaltung automatisch wieder freigegeben werden
| 26.02.2004  1.16  Nue | MMUserName in StopMeldung eingef�hrt.
| 13.10.2004  1.17  Nue | cCanNotStopGrpOnAC338 modified and check on NetTyp not on machine.
| 20.03.2006  1.18  Nue | SettingsRec.YarnCntUnit  := TMMSettingsReader.Instance.Value[cYarnCntUnit]; Added
|=============================================================================*)
unit CloseHandlerComp;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseGlobal, AssignComp, AssignAnimationForm, mmTimer, IvDictio, TypInfo, AskCloseForm,
  YMParaDef, LoepfeGlobal;

const
  cClosePipeName = cAssignmentsPipeName + 'Close' + cAddOnChannelName;

resourcestring
  cAskCloseProdGrp =
    '(*)Datenerfassung der Partie %s, Maschine %s,  Spulstellenbereich %s  wird gestoppt?'; //ivlm

  cBadSpdRangeMsg =
    '(*)Der von Ihnen gewaehlte Spulstellenbereich kann nicht geschlossen werden.'; // ivlm

  cCloseFailedMsg =
    '(*)Datenerfassung der Partie konnte nicht gestoppt werden.'; //ivlm

  cCloseProdGrpBusy =
    '(*)Von den gewaehlten Partien werden zur Zeit Daten akquiriert. Bitte versuchen Sie die Partie erneut zu stoppen.'; // ivlm

  cCloseTimeoutMsg =
    '(*)Das MillMaster Server System reagiert nicht. Bitte versuchen Sie die Datenerfassung der Partie erneut zu stoppen.'; // ivlm

  cCanNotStopStoppedProdGrp =
    '(*)Eine gestoppte Partie kann nicht gestoppt werden.'; //ivlm

  cCanNotStopGrpOnAC338 =
    '(*)Datenerfassung auf Maschine mit Netztyp "%s" kann nicht beendet werden.'; //ivlm

  cMachOfflineForStopp =
    '(*)Die Maschine ist offline. Wenn Datenerfassung jetzt beendet wird, kann das zu Datenverlust fuehren. Wollen Sie trotzdem stoppen?'; //ivlm
type
//..............................................................................
  TCloseHandler = class(TBaseHandler)
  private
    mCloseAnimation: TAssignAnimation;
//    mAskClose        : TAskClose;
    fSpdRange: TBaseSpindleRange;
    procedure DoClose;
    procedure ProcessCloseDoneMsg(aResMsg: TResponseMsgTyp);
    procedure ProcessCloseTimeoutMsg;
  protected
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); override;
    procedure OnTimeout(aSender: TObject); override;
    procedure OnWndMsg(msg: TMessage); override;
    function AskClose: boolean;
    function GetIsClosePending: boolean;
  public
    mAskClose: TAskClose;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure CloseProdGrp;
    property SpdRange: TBaseSpindleRange read fSpdRange write fSpdRange;
    property IsClosePending: boolean read GetIsClosePending;
  published
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  BASEFORM, XMLDef, SettingsReader;
//------------------------------------------------------------------------------
const
  cCLOSE_TIMEOUT = 60000;
//------------------------------------------------------------------------------
  { TCloseHandler }
//------------------------------------------------------------------------------
constructor TCloseHandler.Create(aOwner: TComponent);
begin
  inherited Create(aOwner, cClosePipeName);
  if not (csDesigning in ComponentState) then begin
    mAskClose := TAskClose.Create(aOwner);
    mCloseAnimation := TAssignAnimation.Create(aOwner);
    mCloseAnimation.AnimationTyp := atClose;
  end;
  mTimer.Interval := cCLOSE_TIMEOUT;
  fSpdRange := nil;
end;
//------------------------------------------------------------------------------
destructor TCloseHandler.Destroy;
begin
{wss, Jan 2004: da diese Fenster entweder mit aOwner oder Self erstellt werden,
                m�ssen diese Fenster nicht manuell freigegeben werden.
    FreeAndNil(mCloseAnimation);
    FreeAndNil(mAskClose);
{}
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.OnWndMsg(msg: TMessage);
begin
  case msg.wParam of
    WM_DOCLOSE: DoClose;
    WM_CLOSE_DONE: ProcessCloseDoneMsg(TResponseMsgTyp(msg.lParam));
    WM_CLOSE_TIMEOUT: ProcessCloseTimeoutMsg;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
begin
  inherited;
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_CLOSE_DONE, ORD(aReceivedMsg.MsgTyp));
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.OnTimeout(aSender: TObject);
begin
  inherited;
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_CLOSE_TIMEOUT, 0);
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.CloseProdGrp;
begin
  try
    if not fSpdRange.IsAbleToClose then begin
      InfoMsg(Format(cCanNotStopGrpOnAC338, [cNetTypNames[fSpdRange.NetTyp]]));
//      InfoMsg(Format(cCanNotStopGrpOnAC338, [cMachineTypeArr[fSpdRange.MachineType].Name]));
//      InfoMsg(Format(cCanNotStopGrpOnAC338, [cFrontTypeNames[fSpdRange.FrontType]]));
      Exit;
    end;

    if not fSpdRange.IsOnline then begin
      if not (MessageDlg(cMachOfflineForStopp, mtWarning, [mbYes, mbNo], 0) = mrYes) then
        Exit;
    end;

    if fSpdRange is TProdGrp then begin
      if AskClose then begin
        SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_DOCLOSE, 0);
        mCloseAnimation.ParentWindow := (Owner as TWinControl).Handle;
        mCloseAnimation.ShowModal;
      end;
    end
    else begin
      InfoMsg(cCanNotStopStoppedProdGrp);
      fSpdRange := nil;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TCloseHandler.CloseProdGrp failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TCloseHandler.AskClose: boolean;
begin
  Result := False;
  mAskClose.ProdGrpName := (SpdRange as TProdGrp).Name;
  mAskClose.MachineName := (SpdRange as TProdGrp).MachineName;
  mAskClose.SpindleRange := Format('%d - %d', [SpdRange.spindleFirst, SpdRange.SpindleLast]);
  mAskClose.StyleName := (SpdRange as TProdGrp).StyleName; //Nue 11.12.01
  if mAskClose.ShowModal = mrOK then
    Result := True;
end;
//------------------------------------------------------------------------------
function TCloseHandler.GetIsClosePending: boolean;
begin
  Result := not (mState = asNoJob);
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.DoClose;
var
  xJob: TJobRec;
begin
  FillChar(xJob, sizeOf(xJob), 0);
  // wss: hier ist es nicht n�tig einen dynamischen Job zu verwenden. -> @xJob wird weitergegeben
  try
    if not Assigned(fSpdRange) then
      raise Exception.Create('ProdGrp not assigned.');

    if not Assigned(AssMachine) then //Nue:13.9.05
      AssMachine := (fSpdRange as TMachine);

    if AssMachine.NetTyp = ntNone then
      AssMachine.MachineID := SpdRange.MachineID;

    xJob.JobTyp := jtStopGrpEv;
    xJob.NetTyp := AssMachine.NetTyp;
    with xJob.StopGrpEv do begin
      MachineID    := SpdRange.MachineID;
      SettingsRec.Group        := (SpdRange as TProdGrp).GrpNr;
      SettingsRec.SpindleFirst := SpdRange.SpindleFirst;
      SettingsRec.SpindleLast  := SpdRange.SpindleLast;
      SettingsRec.ProdGrpID    := (SpdRange as TProdGrp).ProdGrpID;
      SettingsRec.ComputerName := mPort.LocalComputerName;
      SettingsRec.MMUserName   := mAskClose.MMSecurityControl1.CurrentMMUser;
      SettingsRec.Port         := mPort.PortName;
      SettingsRec.YarnCntUnit  := TMMSettingsReader.Instance.Value[cYarnCntUnit];   //Nue:20.3.06
    end;
    NewJob(@xJob);
    fSpdRange := nil;
  except
    on e: Exception do begin
      raise Exception.Create('TCloseHandler.DoClose failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.ProcessCloseDoneMsg(aResMsg: TResponseMsgTyp);
begin
  try
    if mState <> asWaitJobRespons then
      Exit;
    mState := asNoJob;
      //mCloseAnimation.StoppAnimation;
      //mCloseAnimation.Close;

    case aResMsg of
      rmBadSpdRange: begin
          UserErrorMsg(cBadSpdRangeMsg, Owner);
          mCloseAnimation.Close;
        end;
      rmMachineOffline: begin
          mCloseAnimation.Close;
          raise Exception.Create('Unexpectred MsgTyp : rmMachineOffline.'); //UserErrorMsg ( cAssMachineOfflineMsg );
        end;
      rmProdGrpStoppedOk: begin
          //InfoMsg ( cCloseSuccessfulMsg  );
          mState := asNoJob;
          if Assigned(Owner) then
            SendMessage((Owner as TForm).Handle, RegisterWindowMessage(ASSIGN_REG_STR), WM_CLOSE_DONE, 0);
          SendMessage((mCloseAnimation as TForm).Handle, RegisterWindowMessage(ASSIGN_REG_STR), WM_CLOSE_DONE, 0);
        end;
      rmProdGrpStoppedNOk: begin
          UserErrorMsg(cCloseFailedMsg, Owner);
          mCloseAnimation.Close;
        end;
      rmProdGrpStateBad: begin
          InfoMsg(cCloseProdGrpBusy);
          mCloseAnimation.Close;
        end;
      rmProdGrpStartedOk, rmProdGrpStartedNOk,
        rmNodeListOnDBOk, rmNodeListOnDBNotOk: begin
          mCloseAnimation.Close;
          raise Exception.Create('Unexpected response. Type = ' + GetEnumName(TypeInfo(TResponseMsgTyp), Ord(aResMsg)));
        end;
    else
      mCloseAnimation.Close;
      raise Exception.Create('MsgTyp not defined. Type = ' + GetEnumName(TypeInfo(TResponseMsgTyp), Ord(aResMsg)));
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TCloseHandler.ProcessCloseDoneMsg failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TCloseHandler.ProcessCloseTimeoutMsg;
begin
  mState := asNoJob;
  mCloseAnimation.Close;
  UserErrorMsg(cCloseTimeoutMsg, Owner);
end;
//------------------------------------------------------------------------------
end.

