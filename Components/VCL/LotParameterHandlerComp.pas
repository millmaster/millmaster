(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: LotParameterHandlerComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 02.02.2000  1.00  Mg  | Datei erstellt
| 01.03.2002  1.00  Wss | Beim Abfuellen von JobRec Funktion ConvertYarnCountFloatToInt verwenden
|=============================================================================*)
unit LotParameterHandlerComp;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  AssignComp, BaseGlobal, MMUGlobal;
const
  cLotParameterPipeName = cAssignmentsPipeName + 'LotPar' + cAddOnChannelName;
type
//..............................................................................
  TLotParameterHandler = class(TBaseHandler)
  private
    fColor: TColor;
    fColorValid: boolean;
    fProdName: string;
    fProdNameValid: boolean;
    fSlip: Single;
    fSlipValid: boolean;
    fYarnCnt: Single;
    fYarnCntValid: boolean;
    fThreadCnt: integer;
    fThreadCntValid: boolean;
    procedure SetColor(aColor: TColor);
    procedure SetProdName(aProdName: string);
    procedure SetSlip(aSlip: Single);
    procedure SetYarnCnt(aYarnCnt: Single);
    procedure SetThreadCnt(aCnt: integer);
  protected
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); override;
    procedure OnTimeout(aSender: TObject); override;
    procedure OnWndMsg(msg: TMessage); override;
  public
    constructor Create(aOwner: TComponent); override;
    procedure LotParamAssign(aProdGrpID: integer);
    property Color: TColor read fColor write SetColor;
    property ProdName: string read fProdName write SetProdName;
    property Slip: Single read fSlip write SetSlip;
    property YarnCnt: Single read fYarnCnt write SetYarnCnt;
    property ThreadCnt: integer read fThreadCnt write SetThreadCnt;
  published
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
//------------------------------------------------------------------------------
constructor TLotParameterHandler.Create(aOwner: TComponent);
begin
  inherited Create(aOwner, cLotParameterPipeName);
  fColorValid     := False;
  fProdNameValid  := False;
  fSlipValid      := False;
  fYarnCntValid   := False;
  fThreadCntValid := False;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.SetColor(aColor: TColor);
begin
  fColor      := aColor;
  fColorValid := True;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.SetProdName(aProdName: string);
begin
  fProdName      := aProdName;
  fProdNameValid := True;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.SetSlip(aSlip: Single);
begin
  fSlip      := aSlip;
  fSlipValid := True;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.SetYarnCnt(aYarnCnt: Single);
begin
  fYarnCnt      := aYarnCnt;
  fYarnCntValid := True;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.SetThreadCnt(aCnt: integer);
begin
  fThreadCnt      := aCnt;
  fThreadCntValid := True;
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
begin
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.OnTimeout(aSender: TObject);
begin
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.OnWndMsg(msg: TMessage);
begin
end;
//------------------------------------------------------------------------------
procedure TLotParameterHandler.LotParamAssign(aProdGrpID: integer);
var
  xJob: TJobRec;
begin
  // wss: hier ist es nicht n�tig einen dynamischen Job zu verwenden. -> @xJob wird weitergegeben
  try
    FillChar(xJob, Sizeof(xJob), 0);
    xJob.NetTyp := AssMachine.NetTyp;
    xJob.JobTyp := jtProdGrpUpdate;

    with xJob.ProdGrpUpdate do begin
      ProdGrpID      := aProdGrpID;
      ColorValid     := fColorValid;
      Color          := fColor;
      ProdNameValid  := fProdNameValid;
      ProdName       := fProdName;
      Slip           := Round(fSlip * MMUGlobal.cSlipFactor);
      SlipValid      := fSlipValid;
      YarnCnt        := fYarnCnt; //wss
      YarnCntValid   := fYarnCntValid;
      ThreadCnt      := fThreadCnt;
      ThreadCntValid := fThreadCntValid;
    end;

    NewJob(@xJob);
  except
    on e: Exception do begin
      raise Exception.Create('TLotParameterHandler.Assign failed.' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
end.

