(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: regYMClearerPrint.pas
| Projectpart...: MillMaster
| Subpart.......:
| Process(es)...: -
| Description...:
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date     | Vers.| Vis.| Reason
|-------------------------------------------------------------------------------
| 14.03.2005  0.01  Wss | YMRepClassUnit, SettingsFinderUnit und Komponente entfernt
|=============================================================================*)
unit regYMClearerPrint;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R YMQRUnit.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, DBTables,
  ClassDataSuck,
  YMQRUnit,
  FloorControlX;

//------------------------------------------------------------------------------
type
  TBaseClassDataSuckDBNameProperty = class(TStringProperty)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure GetValues(Proc: TGetStrProc); override;
  end;


procedure MMRegister;
begin
  // Unit YMQRUnit
  RegisterComponents('MillMaster', [TQRQMatrix,
                                TQRCSBasic]);
  // Unit FloorControlX
  RegisterComponents('MillMaster', [TFloorControlX]);

  // property editor for TRepBaseClass
  RegisterPropertyEditor(TypeInfo(String), TBaseClassDataSuck, 'DatabaseName', TBaseClassDataSuckDBNameProperty);
end;

//------------------------------------------------------------------------------
// TYMReportDatabaseName
//------------------------------------------------------------------------------
function TBaseClassDataSuckDBNameProperty.GetAttributes: TPropertyAttributes;
begin
  Result := [paValueList, paSortList, paRevertable];
end;
//------------------------------------------------------------------------------
procedure TBaseClassDataSuckDBNameProperty.GetValues(Proc: TGetStrProc);
var
  i: Integer;
begin
  for i:=0 to Session.DatabaseCount-1 do
    Proc(Session.Databases[i].DatabaseName);
end;
//------------------------------------------------------------------------------
end.
