(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: dmClassDataLayer.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 14.06.2001 1.00 Wss | File created
| 31.10.2001 1.00 Wss | - reading YarnCount into YMSettingsInfoRec added with conversion to YarnUnit
| 04.03.2002 1.00 Wss | Umstellung des YarnCount auf Float
| 20.06.2002 1.01 Nue | Zusaetze wegen Einbau Longterm.
| 17.09.2002 1.06 Nue | Umbau auf ADO
| 27.09.2002 1.02 Wss | - dseWork und dseWork2 f�r Verwendung von qryWorkBeforeOpen angeh�ngt (Event)
                          um Zeitparameter abzuf�llen
                        - Bug bei GetYarnCountInfo() behoben: Bedingung in FillBaseQuery() war nicht
                          richtig f�r die Zeitparameter
                        - Bug bei Query mit der Letzten verwendeten Settings behoben (ORDER BY)
| 27.09.2002       Wss| Umbau ADO
| 03.10.2002       LOK| Umbau ADO2 (dbTables entfernt und TBlobStream erstetzt)
| 03.10.2002       Wss| cUSADateMask nach LoepfeGlobal verschoben
| 29.10.2002       Wss| Lesen der Klassierdaten von Image f�r ADO korrigiert
| 28.11.2002       Wss| DataSet auf ReadOnly und ForwardOnly gesetzt
| 16.01.2003       Wss| Kleinere Korrekturen wegen Mauszeiger umstellung crSQLWait
| 23.01.2003       Wss| Kleinere Korrekturen beim Umschalten des TimeModes
| 11.02.2003       Wss| In GetIDsFromNames wurde nicht nach den Namen gefiltert um die
                        entsprechenden ID's zu holen
| 25.06.2003       Wss| Anpassungen an neue TimeHandling: Shift nur noch �ber Bereich From/To
| 03.07.2003       Wss| Speicherloch: fDM wurde nicht freigegeben in Destroy
| 20.09.2005       Wss| fXMLSettingNames.Duplicated = Accept
|=============================================================================*)
unit ClassDataReader;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, MMUGlobal, DataLayerDef, BaseGlobal,
  LabMasterDef, ADODB, mmADODataSet, mmADOConnection, QualityMatrix{, RepSettingsUnit};

type
  TOnExceptionEvent = procedure (Sender: TObject; aMsg: String) of object;
  //*************************** class TProdGrp *********************************
  TProdGroupInfo = class(TObject)
  private
    fProdID: integer;
    fYMSetID: integer;
    fProdName: string;
    fMachName: string;
    fSpindleFirst: integer;
    fSpindleLast: integer;
    fProdStart: TDateTime;
    fProdEnd: TDateTime;
    fXMLData: Integer;
    fYMSetName: string;
    fYarnCount: Single;
  public
    property ProdID: integer read fProdID write fProdID;
    property ProdName: string read fProdName write fProdName;
    property MachName: string read fMachName write fMachName;
    property SpindleFirst: integer read fSpindleFirst write fSpindleFirst;
    property SpindleLast: integer read fSpindleLast write fSpindleLast;
    property ProdStart: TDateTime read fProdStart write fProdStart;
    property ProdEnd: TDateTime read fProdEnd write fProdEnd;
    property XMLData: Integer read fXMLData write fXMLData;
    property YMSetID: integer read fYMSetID write fYMSetID;
    property YMSetName: string read fYMSetName write fYMSetName;
    property YarnCount: Single read fYarnCount write fYarnCount;
  end;

  TProdGroupList = class(TList)
  private
    function GetItems(Index: Integer): TProdGroupInfo;
    procedure SetItems(Index: Integer; const Value: TProdGroupInfo);
  public
    procedure Clear; override;
    procedure Delete(aIndex: Integer); virtual;
    property Items[Index: Integer]: TProdGroupInfo read GetItems write SetItems; default;
  end;

  TXMLSettingsList = class(TStringList)
  private
    function GetItems(Index: Integer): PXMLSettingsInfoRec;
    procedure SetItems(Index: Integer; const Value: PXMLSettingsInfoRec);
  public
    procedure Clear; override;
    procedure Delete(aIndex: Integer); override;
    property Items[Index: Integer]: PXMLSettingsInfoRec read GetItems write SetItems; default;
  end;

  TClassDataModule = class (TDataModule)
    conDefault: TmmADOConnection;
    dseWork: TmmADODataSet;
    dseTemp: TmmADODataSet;
    dseWork2: TmmADODataSet;
    procedure dsWorkBeforeOpen(DataSet: TDataSet);
  private
    fError: TErrorRec;
    fResultDataSet: TmmADODataSet;
    procedure GetYarnCountInfo(aSetID: Integer; aXMLSettings: PXMLSettingsInfoRec);
    procedure AddWhereToList(aList: TStringList);
  protected
    mFrom: TStringList;
    mGroup: TStringList;
    mMachIDsFromNames: string;
    mMultiTimeRanges: string;
    mOrder: TStringList;
    mParams: TQueryParameters;
    mProdIDsFromNames: string;
    mSelect: TStringList;
    mStyleIDsFromNames: string;
    mWhere: TStringList;
    mYMSetIdsFromNames: string;
    procedure ClearQueryInfo;
    function GetQueryString: string;
    function PrepareQuery: Boolean; virtual;
  public
    constructor Create(aParams: TQueryParameters); reintroduce; virtual;
    destructor Destroy; override;
    function GetBaseSQL(aFrom, aWhere: String): string; virtual;
    function GetLength: Double; virtual;
    function GetProdGroupList(aProdGroupList: TProdGroupList): Boolean;
    function GetXMLSettings(var aXMLSettingsList: TXMLSettingsList): Boolean; overload;
    function GetXMLSettingNames(var aSettingsNameList: TStringList): Boolean; overload;
    function GetXMLSettingsFromID(aID: Integer; var aXMLSettings: TXMLSettingsInfoRec): Boolean;
    function Init: Boolean; virtual;
    function PrepareClassDataQuery(aClassTyp: TClassDataTyp): Boolean; virtual;
    property Error: TErrorRec read fError write fError;
    property ResultDataSet: TmmADODataSet read fResultDataSet write fResultDataSet;
  end;

  TClassDataReader = class (TComponent)
  private
    fClassCut: Array[0..cClassCutFields-1] of Double;
    fClassUncut: Array[0..cClassUncutFields-1] of Double;
    fDM: TClassDataModule;
    fInitialized: Boolean;
    fLen: Double;
    fOnException: TOnExceptionEvent;
    fParams: TQueryParameters;
    fSiroCut: Array[0..cSIROCutFields-1] of Double;
    fSiroUncut: Array[0..cSIROUncutFields-1] of Double;
    fSpliceCut: Array[0..cSpCutFields-1] of Double;
    fSpliceUncut: Array[0..cSpUncutFields-1] of Double;
    fXMLSettings: TXMLSettingsList;
    mAvailableClassData: TClassDataSet;
    mLenReady: Boolean;
    fXMLSettingsReady: Boolean;
    fXMLSettingNames: TStringList;
    fProdGroupList: TProdGroupList;            
    function GetClassCut(Index: Integer): Double;
    function GetClassUncut(Index: Integer): Double;
    function GetClassValue(aClassTyp: TClassDataTyp; aIndex: Integer): Double;
    function GetDM: TClassDataModule;
    function GetLen: Double;
    function GetSiroCut(Index: Integer): Double;
    function GetSiroUncut(Index: Integer): Double;
    function GetSpliceCut(Index: Integer): Double;
    function GetSpliceUncut(Index: Integer): Double;
    function GetXMLSettings: TXMLSettingsList;
    function GetXMLSettingNames: TStringList;
    function GetProdGroupList: TProdGroupList;
    procedure ReadClassData(aClassTyp: TClassDataTyp);
    procedure ReadClassDataInterval(aClassTyp: TClassDataTyp);
    procedure ReadClassDataShift(aClassTyp: TClassDataTyp);
    procedure ReadLenData;
    procedure SetClassCut(Index: Integer; const Value: Double);
    procedure SetClassUncut(Index: Integer; const Value: Double);
    procedure SetParams(const Value: TQueryParameters);
    procedure SetSiroCut(Index: Integer; const Value: Double);
    procedure SetSiroUncut(Index: Integer; const Value: Double);
    procedure SetSpliceCut(Index: Integer; const Value: Double);
    procedure SetSpliceUncut(Index: Integer; const Value: Double);
  protected
    procedure CallOnException(aMsg: String);
    procedure Clear; virtual;
    procedure ParamChanged(Source: TObject); virtual;
    property DM: TClassDataModule read GetDM;
  public
    constructor Create(Owner: TComponent); override;
    destructor Destroy; override;
    procedure FillValuesToMatrix(aDefects, aCuts: Boolean; aMatrix: TQualityMatrix);
    function GetBaseSQL: string;
    function GetXMLSettingsFromID(aID: Integer; var aXMLSettings: TXMLSettingsInfoRec): Boolean;
    function Init: Boolean; virtual;
    function SettingsAreSameHeadClass: Boolean;
    property ClassCut[Index: Integer]: Double read GetClassCut write SetClassCut;
    property Value[aClassTyp: TClassDataTyp; aIndex: Integer]: Double read GetClassValue;
    property ClassUncut[Index: Integer]: Double read GetClassUncut write SetClassUncut;
    property UncutValue[aID: TClassDataTyp; aIndex: Integer]: Double read GetClassValue;
    property Initialized: Boolean read fInitialized write fInitialized;
    property Len: Double read GetLen write fLen;
    property Params: TQueryParameters read fParams write SetParams;
    property SiroCut[Index: Integer]: Double read GetSiroCut write SetSiroCut;
    property SiroUncut[Index: Integer]: Double read GetSiroUncut write SetSiroUncut;
    property SpliceCut[Index: Integer]: Double read GetSpliceCut write SetSpliceCut;
    property SpliceUncut[Index: Integer]: Double read GetSpliceUncut write SetSpliceUncut;
    property XMLSettings: TXMLSettingsList read GetXMLSettings;
    property XMLSettingNames: TStringList read GetXMLSettingNames;
    property ProdGroupList: TProdGroupList read GetProdGroupList;
    property XMLSettingsReady: Boolean read fXMLSettingsReady;
  published
    property OnException: TOnExceptionEvent read fOnException write fOnException;
  end;

//------------------------------------------------------------------------------
var
  ClassDataModule: TClassDataModule;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS,
  LoepfeGlobal,
  DataLayerColDef,
  SettingsReader, XMLDef, XMLSettingsAccess, XMLGlobal, QualityMatrixDef, QualityMatrixBase;

{:------------------------------------------------------------------------------
 TYMSettingsList}
{:-----------------------------------------------------------------------------}
procedure TXMLSettingsList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    Delete(i);
  inherited Clear;
end;

//------------------------------------------------------------------------------
procedure TXMLSettingsList.Delete(aIndex: Integer);
var
  xRec: PXMLSettingsInfoRec;
begin
  if (aIndex >= 0) and (aIndex < Count) then begin
    xRec := PXMLSettingsInfoRec(Objects[aIndex]);
    if Assigned(xRec) then begin
      xRec^.XMLData := '';
      Dispose(xRec);
    end;
    inherited Delete(aIndex);
  end;
end;

//------------------------------------------------------------------------------
function TXMLSettingsList.GetItems(Index: Integer): PXMLSettingsInfoRec;
begin
  Result := PXMLSettingsInfoRec(Objects[Index]);
  if CodeSite.Enabled then
    CodeSite.SendString('GetXMLSettings: ' + Result^.SetName, FormatXML(Result^.XMLData));
end;

//------------------------------------------------------------------------------
procedure TXMLSettingsList.SetItems(Index: Integer; const Value: PXMLSettingsInfoRec);
begin
  Objects[Index] := Pointer(Value);
end;

{:------------------------------------------------------------------------------
 TClassDataModule}
{:-----------------------------------------------------------------------------}
constructor TClassDataModule.Create(aParams: TQueryParameters);
begin
  inherited Create(Nil);
  mFrom := TStringList.Create;
  mGroup := TStringList.Create;
  mOrder := TStringList.Create;
  mSelect := TStringList.Create;
  mWhere := TStringList.Create;
  fResultDataSet := Nil;

  mParams      := aParams;

  mMachIDsFromNames  := '';
  mProdIDsFromNames  := '';
  mStyleIDsFromNames := '';
  mYMSetIdsFromNames := '';
  mMultiTimeRanges   := '';
end;

//------------------------------------------------------------------------------
destructor TClassDataModule.Destroy;
begin
  mFrom.Free;
  mGroup.Free;
  mOrder.Free;
  mSelect.Free;
  mWhere.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TClassDataModule.ClearQueryInfo;
begin
  mSelect.Clear;
  mFrom.Clear;
  mWhere.Clear;
  mGroup.Clear;
  mOrder.Clear;
end;

//------------------------------------------------------------------------------
procedure TClassDataModule.AddWhereToList(aList: TStringList);
begin
  with aList, mParams do begin
    Add(Format(cWhereShiftCalID[TimeMode], [ShiftCalID]));

    if StyleIDs <> '' then Add(Format(cWhereStyleID, [StyleIDs]));
    if mStyleIDsFromNames <> '' then Add(Format(cWhereStyleID, [mStyleIDsFromNames]));

    if ProdIDs <> '' then Add(Format(cWhereProdID, [ProdIDs]));
    if mProdIDsFromNames <> '' then Add(Format(cWhereProdID, [mProdIDsFromNames]));

    if MachIDs <> '' then Add(Format(cWhereMachID, [MachIDs]));
    if mMachIDsFromNames <> '' then Add(Format(cWhereMachID, [mMachIDsFromNames]));

    if (SpindleIDs <> '') and (TimeMode = tmInterval) then Add(Format(cWhereSpindleID, [SpindleIDs]));

    if YMSetIDs <> '' then Add(Format(cWhereYMSetID, [YMSetIDs]));
    if mYMSetIDsFromNames <> '' then Add(Format(cWhereYMSetID, [mYMSetIDsFromNames]));

    // wss Feb 05: Nur bei Interval werden einzelne ID's unterst�tzt -> LabReport, Reinigerassistent
    if (TimeIDs <> '') AND (TimeMode = tmInterval) then
      Add(Format(cWhereMultTimeRange, [mMultiTimeRanges]))
    // ansonsten wenn der Zeitbereich g�ltig ist, dann diesen verwenden
    else if (TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970) then
      Add(cWhereTimeRange[TimeMode]);

//wss 02.2005    else if (UsedInFloor or (TimeMode <> tmInterval)) and ((TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970)) then
//      Add(cWhereTimeRange[TimeMode]);

//    if (TimeIDs <> '') AND (TimeMode <> tmLongtermWeek) then
//      Add(Format(cWhereMultTimeRange, [mMultiTimeRanges]))
//    else if (UsedInFloor or (TimeMode = tmLongtermWeek)) and ((TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970)) then
//      Add(cWhereTimeRange[TimeMode]);
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetBaseSQL(aFrom, aWhere: String): string;
var
  xStrList: TStringList;
begin
EnterMethod('TClassDataModule.GetBaseSQL');
  xStrList := TStringList.Create;
  with xStrList, mParams do
  try
    // add mask for columns
    Text := 'SELECT %s ';

    // add selected view
    if TimeMode = tmShift then
      Add('FROM ' + 'v_production_shift v ' + aFrom)
    else if TimeMode = tmInterval then
      Add('FROM ' + 'v_production_interval v ' + aFrom)
    else //tmLongtermWeek Nue
      Add('FROM ' + 'v_production_week v ' + aFrom);

    // add where part depending of property values
    Add('WHERE ' + aWhere);
    AddWhereToList(xStrList);

    Result := Text;
    CodeSite.SendString('Query', Result);
  finally
    xStrList.Free;
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetLength: Double;
begin
  Result := 0.0;
  if PrepareQuery then begin
    mSelect.Text := 'SUM(c_Len * 1.0) c_Len ';
    with dseWork do
    try
      Close;
      CommandText := GetQueryString;
//      CodeSite.SendStringList('LenQuery', SQL);
      Open;
      while not EOF do begin
        Result := Result + FieldByName('c_Len').AsFloat;
        Next;
      end;
    except
      on e:Exception do
        raise Exception.Create('TClassDataModule.GetLength: ' + e.Message);
    end;
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetQueryString: string;
begin
  if mSelect.Count = 0 then
    mSelect.Text := '*';
  
  Result := Format('SELECT %s FROM %s', [mSelect.CommaText, mFrom.CommaText]);
  if mWhere.Count > 0 then
    Result := Format('%s WHERE %s', [Result, mWhere.Text]);

  if mGroup.Count > 0 then
    Result := Format('%s GROUP BY %s', [Result, mGroup.CommaText]);

  if mOrder.Count > 0 then
    Result := Format('%s ORDER BY %s', [Result, mOrder.CommaText]);

  Result := StringReplace(Result, '"', '', [rfReplaceAll]);
end;

//------------------------------------------------------------------------------
procedure TClassDataModule.GetYarnCountInfo(aSetID: Integer; aXMLSettings: PXMLSettingsInfoRec);
var
  i: Integer;
  //......................................................................
  procedure FillBaseQuery(aFields: String);
  begin
    with dseWork2, mParams do begin
      Close;
      CommandText := Format(GetBaseSQL(', t_prodgroup pg', 'v.c_prod_id = pg.c_prod_id AND v.c_ym_set_id = %d'), [aFields,aSetID]);
    end;
  end;
  //......................................................................
begin
EnterMethod('TClassDataModule.GetYarnCountInfo');
  aXMLSettings^.SingleYarnCount := True;
  with dseWork2 do
  try
    FillBaseQuery('COUNT(DISTINCT pg.c_act_yarncnt) AS YarnCntNumber');
    Open;
    i := FieldByName('YarnCntNumber').AsInteger;
    aXMLSettings^.SingleYarnCount := (i <= 1);

    if i > 0 then begin
      FillBaseQuery('TOP 5 pg.c_act_yarncnt, pg.c_yarncnt_unit');
      CommandText := CommandText + ' ORDER BY c_time DESC';
      Open;

      aXMLSettings^.YarnCount := YarnCountConvert(yuNm, aXMLSettings^.YarnUnit, FieldByName('c_act_yarncnt').AsFloat);
    end else begin
      aXMLSettings^.YarnCount := 0;
      aXMLSettings^.YarnUnit  := yuNone;
    end;
  except
    on e:Exception do
      raise Exception.Create('TClassDataModule.GetYarnCountInfo: ' + e.Message);
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetXMLSettings(var aXMLSettingsList: TXMLSettingsList): Boolean;
var
  xXMLSettingsReader: TXMLSettingsAccess;
  xXMLSettings: PXMLSettingsInfoRec;
  xID: Integer;
  xStr: string;
  xXMLData: String;
begin
EnterMethod('GetXMLSettings');
  Result  := False;
  if PrepareQuery then begin
    xXMLSettingsReader := TXMLSettingsAccess.Create;
    try
      Screen.Cursor := crSQLWait;
      Mouse.CursorPos := Mouse.CursorPos;
      with dseWork do
      try
        Close;
        mSelect.Text := cQRY_YMSettings;
        mFrom.Add(cTAB_YMSettings);
        mWhere.Add(cWhereLinkForYMSettings);
        mOrder.Add(cORD_YMSettings);

        CommandText := GetQueryString;
        CodeSite.SendFmtMsg('YMSettingsQuery: %s', [CommandText]);

        Open;
        // if open ok then create helper for reading YMSettings
        while not EOF do begin
          // get settings info from DB
          xID  := FieldByName('c_ym_set_id').AsInteger;
          // read YM settings through TXMLSettingsAccess
          xXMLSettingsReader.ReadSetting(xID, itXMLSetID, xXMLData, xStr);
          // if no name is defined in table create auto name with ID
          if trim(xStr) = '' then
            xStr := cAutoNameConst + IntToStr(xID);

          // create and add new named XMLSettingsInfo into list
          new(xXMLSettings);

          xXMLSettings^.SetName   := xStr;
          xXMLSettings^.SetID     := xID;
          xXMLSettings^.YarnUnit  := mParams.YarnUnit;
          xXMLSettings^.HeadClass := TSensingHeadClass(FieldByName('c_head_class').AsInteger);
          xXMLSettings^.XMLData   := xXMLData;

          aXMLSettingsList.AddObject(xStr, Pointer(xXMLSettings));
          // check for different yarn count and fill in the last used one
          GetYarnCountInfo(xID, xXMLSettings);

          Next;
        end; // while not EOF

        Result := True;
      except
        on e:Exception do
          raise Exception.Create('TClassDataModule.GetXMLSettings: ' + e.Message);
      end; // with
    finally
      xXMLSettingsReader.Free;
      Screen.Cursor := crDefault; //xCursor;
    end;
  end; // if PrepareQuery
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetXMLSettingNames(var aSettingsNameList: TStringList): Boolean;
var
  xID: Integer;
  xStr: string;
begin
  EnterMethod('GetXMLSettingNames');
  Result  := False;
  aSettingsNameList.Clear;

  if PrepareQuery then
  try
    Screen.Cursor := crSQLWait;
    Mouse.CursorPos := Mouse.CursorPos;
    with dseWork do
    try
      Close;
      mSelect.Text := cQRY_YMSettings;
      mFrom.Add(cTAB_YMSettings);
      mWhere.Add(cWhereLinkForYMSettings);
      mOrder.Add(cORD_YMSettings);

      CommandText := GetQueryString;
      CodeSite.SendFmtMsg('YMSettingsQuery: %s', [CommandText]);

      Open;
      // if open ok then create helper for reading YMSettings
      while not EOF do begin
        // get settings info from DB
        xID  := FieldByName('c_ym_set_id').AsInteger;
        xStr := FieldByName('c_ym_set_name').AsString;
        // if no name is defined in table create auto name with ID
        if trim(xStr) = '' then
          xStr := cAutoNameConst + IntToStr(xID);

        aSettingsNameList.AddObject(xStr, Pointer(xID));
        Next;
      end; // while not EOF
      Result := True;
    except
      on e:Exception do
        raise Exception.Create('TClassDataModule.GetXMLSettingNames: ' + e.Message);
    end; // with
  finally
    Screen.Cursor := crDefault; //xCursor;
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetXMLSettingsFromID(aID: Integer; var aXMLSettings: TXMLSettingsInfoRec): Boolean;
var
  xXMLData: String;
begin
  EnterMethod('GetYMSettingsFromID');
  Result  := False;

  if PrepareQuery then begin
    Screen.Cursor   := crSQLWait;
    Mouse.CursorPos := Mouse.CursorPos;

    with TXMLSettingsAccess.Create do
    try
      ReadSetting(aID, itXMLSetID, xXMLData, aXMLSettings.SetName, aXMLSettings.HeadClass);

      aXMLSettings.XMLData  := xXMLData;
      aXMLSettings.YarnUnit := mParams.YarnUnit;
      GetYarnCountInfo(aID, @aXMLSettings);
      Result := True;
    finally
      Free;
      Screen.Cursor := crDefault; //xCursor;
    end;
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.Init: Boolean;

    //............................................................................
  function CreateWhere(aField, aValues: String): String;
  var
    xStr: String;
    xORStr: String;
  begin
    Result := '';
    xORStr := '';
    while CutStringSep(',', aValues, xStr) do begin
      if Pos('%', xStr) > 0 then
        Result := Format('%s%s(%s LIKE %s)', [Result, xORStr, aField, xStr])
      else
        Result := Format('%s%s(%s = %s)', [Result, xORStr, aField, xStr]);
      xORStr := ' OR ';
    end;
    if Result <> '' then
      Result := Format('AND (%s) ', [Result]);
  end;
  //............................................................................
  function GetIDsFromNames(aIDName, aColName, aWhereNames: String): string;
  var
    xStr: String;
  begin
    Result := '';
    with dseWork, mParams do
    try
      Close;
      CommandText := Format(cQueryGetIDs[TimeMode], [aIDName, aColName]);
      CommandText := CommandText + CreateWhere(aColName, aWhereNames);
      // wss Feb 05: Nur bei Interval werden einzelne ID's unterst�tzt -> LabReport, Reinigerassistent
      if (TimeIDs <> '') AND (TimeMode = tmInterval) then
        CommandText := CommandText + Format(cWhereMultTimeRange, [mMultiTimeRanges])
      // ansonsten wenn der Zeitbereich g�ltig ist, dann diesen verwenden
      else if (TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970) then
        CommandText := CommandText + cWhereTimeRange[TimeMode];

//      else if (UsedInFloor or (TimeMode <> tmInterval)) and ((TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970)) then
//        CommandText := CommandText + cWhereTimeRange[TimeMode];
//
//      if (TimeIDs <> '') AND (TimeMode <> tmLongtermWeek) then
//        CommandText := CommandText + Format(cWhereMultTimeRange, [mMultiTimeRanges])
//      else if (UsedInFloor or (TimeMode = tmLongtermWeek)) and ((TimeFrom > cDateTime1970) and (TimeTo > cDateTime1970)) then
//        CommandText := CommandText + cWhereTimeRange[TimeMode];

      CodeSite.SendFmtMsg('dseWork.CommandText: %s', [CommandText]);

      // now open query and get ID's
      try
        Open;
        repeat
          if not EOF then begin
            xStr := FieldByName(aIDName).AsString;
            Next;
          end else
            xStr := '-1';
          Result := Format('%s, %s', [Result, xStr]);
        until EOF;
        // remove leading comma
        if Result <> '' then
          System.Delete(Result, 1, 1);
      except
        on e:Exception do begin
          CodeSite.SendError(Format('GetIDsFromNames: %s = %s; ' +e.Message, [aIDName, aColName]));
        end;
      end;
    finally
      Close;
    end;
  end;
  //............................................................................
  function ParseTimeIDs(aTimeIDs: String): String;
  //
  //  construct goal in where statement:
  //  ...
  //  and ( (From >= date1 and To < date2) or (From >= date3 and To < date4) or (...) )
  //
  const
    cQRY_Parse =
      // interval
      'select c_interval_start c_time_start, c_interval_end c_next_time_expected ' +
        'from t_interval ' +
        'where c_interval_id in (%s) ' +
        'order by c_interval_start ASC';

//    cQRY_Parse: Array[tmInterval..tmShift] of String = (
//      // interval
//      'select c_interval_start c_time_start, c_interval_end c_next_time_expected ' +
//        'from t_interval ' +
//        'where c_interval_id in (%s) ' +
//        'order by c_interval_start ASC',
//      // shift
//      'SELECT c_shift_start c_time_start, DateAdd(minute, c_shift_length, c_shift_start) c_next_time_expected ' +
//        'FROM t_shift ' +
//        'WHERE c_shiftcal_id = 1 ' +
//        'and c_shift_id in (%s) ' +
//        'order by c_shift_start ASC'
//    );

    cFromToMask = '(v.c_time >= ''%s'' and v.c_time < ''%s'')';
    //..........................................................................
    function GetTimeRange: String;
    var
      xFrom, xTemp: TDateTime;
    begin
      with dseWork do begin
          // save shift start
        xFrom   := FieldByName('c_time_start').AsDateTime;
        xTemp   := xFrom;
        while not EOF do begin
            // this is the next expected shift start
          xTemp := FieldByName('c_next_time_expected').AsDateTime;
            // select next row if any
          Next;
            // if calculated next shift shart differs from expected -> hole detected
          if xTemp < FieldByName('c_time_start').AsDateTime then
            break;
        end; // while
        Result := Format(cFromToMask, [FormatDateTime(cUSADateMask, xFrom), FormatDateTime(cUSADateMask, xTemp)]);
      end;
    end;
    //..........................................................................
  var
    xStrList: TStringList;
  begin
    Result := '';
    xStrList := TStringList.Create;
    with dseWork do
    try
      Close;
      CommandText := Format(cQRY_Parse, [aTimeIDs]);
      Open;
      while not EOF do begin
        xStrList.Add(GetTimeRange);
      end;
    finally
      Close;
    end;
      // now get the TimeRanges in the correct format xxx OR xxx OR ....
    with xStrList do
    try
      if Count > 0 then begin
        Result := Strings[0];
        Delete(0);
        while Count > 0 do begin
          Result := Format('%s OR %s', [Result, Strings[0]]);
          Delete(0);
        end;
      end;
    finally
      Free;
    end;
  end;
    //............................................................................

begin
  with mParams do begin
//    if (TimeIDs <> '') AND (TimeMode <> tmLongtermWeek) then begin
    if (TimeIDs <> '') AND (TimeMode = tmInterval) then begin
      mMultiTimeRanges := ParseTimeIDs(TimeIDs);
    end else begin
      mMultiTimeRanges := '';
    end;

    if StyleNames = '' then
      mStyleIDsFromNames := ''
    else
      mStyleIDsFromNames := GetIDsFromNames('c_style_id', 'c_style_name', StyleNames);

    if ProdNames = '' then
      mProdIDsFromNames := ''
    else
      mProdIDsFromNames := GetIDsFromNames('c_prod_id', 'c_prod_name', ProdNames);

    if MachNames = '' then
      mMachIDsFromNames := ''
    else
      mMachIDsFromNames := GetIDsFromNames('c_machine_id', 'c_machine_name', MachNames);

    if YMSetNames = '' then
      mYMSetIDsFromNames := ''
    else
      mYMSetIDsFromNames := GetIDsFromNames('c_YM_set_id', 'c_YM_set_name', YMSetNames);
  end;
  CodeSite.SendString('IDs from Names', Format('StyleIDs=%s; ProdIDs=%s; MachIDs=%s', [mStyleIDsFromNames, mProdIDsFromNames, mMachIDsFromNames]));
  Result := True;
end;

//------------------------------------------------------------------------------
function TClassDataModule.PrepareClassDataQuery(aClassTyp: TClassDataTyp): Boolean;
begin
  Result := False;
  if PrepareQuery then begin
    // add addition query information for current class values for shift (class, splice, siro)
    if (mParams.TimeMode = tmShift) then begin    //NUE
      mSelect.Text := cSUM_ClassCols[aClassTyp];
      mFrom.Add(cTableForClassCols[aClassTyp]);
      mWhere.Add(cWhereLinkForClassTable[aClassTyp]);
    end
    else if (mParams.TimeMode = tmLongtermWeek) then begin  //NUE
      mSelect.Text := cSUM_ClassCols[aClassTyp];
      mFrom.Add(cTableForLongClassCols[aClassTyp]);         //NUE//NUE
      mWhere.Add(cWhereLinkForClassTable[aClassTyp]);
    end else
      mSelect.Text := cFLD_ClassImage;

    with dseWork do
    try
      Close;
      CommandText := GetQueryString;
      CodeSite.SendFmtMsg('ClassQuery: %s', [CommandText]);
      Open;
      ResultDataSet := dseWork;
      Result := Active;
    except
      on e:Exception do
        raise Exception.Create('TClassDataModule.PrepareClassDataQuery: ' + e.Message);
    end;
  end;
  //  CodeSite.ExitMethod('PrepareClassDataQery');
end;

//------------------------------------------------------------------------------
function TClassDataModule.PrepareQuery: Boolean;
begin
  Result := True;
  ClearQueryInfo;

  with mFrom do begin
    if mParams.TimeMode = tmShift then
      Text := 'v_production_shift v'
    else if mParams.TimeMode = tmInterval then
      Text := 'v_production_interval v'
    else //tmLongtermWeek Nue
     Text :=  'v_production_week v';
  end;

  mWhere.Text := '1 = 1';
  AddWhereToList(mWhere);

  with mGroup do begin
  end;

  with mOrder do begin
  end;
end;

//------------------------------------------------------------------------------
procedure TClassDataModule.dsWorkBeforeOpen(DataSet: TDataSet);
var
  xParam: TParameter;
begin
  with TmmADODataSet(DataSet) do begin
    xParam := Parameters.FindParam('TimeFrom');
    if  xParam <> Nil then begin
      xParam.DataType := ftDateTime;   //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
      xParam.Value := mParams.TimeFrom;
      CodeSite.SendString('TimeFrom', DateTimeToStr(mParams.TimeFrom));
    end;
    xParam := Parameters.FindParam('TimeTo');
    if  xParam <> Nil then begin
      xParam.DataType := ftDateTime;   //NUE: Achtung TmmADOQuery hat ein Problem mit DateTime, wenn dar DataType nicht zuvor auf ftDateTime gesetzt wird!!
      xParam.Value := mParams.TimeTo;
      CodeSite.SendString('TimeTo', DateTimeToStr(mParams.TimeTo));
    end;
  end;
end;

//------------------------------------------------------------------------------
function TClassDataModule.GetProdGroupList(aProdGroupList: TProdGroupList): Boolean;
var
  xXMLSettingsReader: TXMLSettingsAccess;
  xProdInfo: TProdGroupInfo;
begin
EnterMethod('GetProdGroupList');
  Result  := False;
//  aProdGroupList.Clear;
  if PrepareQuery then begin
    xXMLSettingsReader := TXMLSettingsAccess.Create;
    try
      Screen.Cursor   := crSQLWait;
      Mouse.CursorPos := Mouse.CursorPos;
      with dseWork do
      try
        Close;
        mSelect.Text := cQry_ProdGroupInfo;
//        mFrom.Add('v_production_interval');
//        mWhere.Add(cWhereLinkForYMSettings);
//        mOrder.Add(cORD_YMSettings);

        CommandText := GetQueryString;
        CodeSite.SendFmtMsg('Query: %s', [CommandText]);

        Open;
        // if open ok then create helper for reading YMSettings
        while not EOF do begin
          // create and add new named XMLSettingsInfo into list
          xProdInfo := TProdGroupInfo.Create;
          with xProdInfo do begin
            ProdID       := FieldByName('c_prod_id').AsInteger;
            YMSetID      := FieldByName('c_ym_set_id').AsInteger;
            ProdName     := FieldByName('c_prod_name').AsString;
            MachName     := FieldByName('c_machine_name').AsString;
            YMSetName    := FieldByName('c_ym_set_name').AsString;
            SpindleFirst := FieldByName('c_spindle_first').AsInteger;
            SpindleLast  := FieldByName('c_spindle_last').AsInteger;
            ProdStart    := FieldByName('c_prod_start').AsDateTime;
            ProdEnd      := FieldByName('c_prod_end').AsDateTime;
            YarnCount    := FieldByName('c_act_yarncnt').AsFloat;
          end;
          aProdGroupList.Add(xProdInfo);

          Next;
        end; // while not EOF

        Result := True;
      except
        on e:Exception do
          raise Exception.Create('TClassDataModule.GetProdGroupList: ' + e.Message);
      end; // with
    finally
      Screen.Cursor := crDefault; //xCursor;
    end;
  end; // if PrepareQuery
end;

//------------------------------------------------------------------------------
//--- TClassDataReader
//------------------------------------------------------------------------------
constructor TClassDataReader.Create(Owner: TComponent);
begin
  inherited Create(Owner);
  fParams                     := TQueryParameters.Create;
  fXMLSettings                := TXMLSettingsList.Create;
  fProdGroupList              := TProdGroupList.Create;
  fXMLSettingNames            := TStringList.Create;
  fXMLSettingNames.Duplicates := dupAccept;
  fXMLSettingNames.Sorted     := True;
  fInitialized                := False;
  fOnException                := Nil;

  if csDesigning in ComponentState then
    fDM := Nil
  else begin
    with TMMSettingsReader.Instance do
    try
      fParams.LenBase    := Value[cMMUnit];
      fParams.ShiftCalID := Value[cDefaultShiftCalID];
      fParams.YarnUnit   := Value[cYarnCntUnit];
    finally
    end;
    fParams.OnParamChange := ParamChanged;
    fDM := TClassDataModule.Create(Params);
  end;

  Clear;
end;

//------------------------------------------------------------------------------
destructor TClassDataReader.Destroy;
begin
  Clear;
  if Assigned(fDM) then
    fDM.Free;
  fOnException := Nil;
  fParams.Free;
  fXMLSettings.Free;
  fXMLSettingNames.Free;
  fProdGroupList.Free;
  inherited Destroy;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.CallOnException(aMsg: String);
begin
  if Assigned(fOnException) then
    fOnException(Self, aMsg);
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.Clear;
var
  i: Integer;
begin
  CodeSite.SendMsg('TClassDataReader.Clear');
  mAvailableClassData := [];
  mLenReady           := False;
  fXMLSettingsReady   := False;
  fLen                := 0.0;
  fXMLSettings.Clear;
  fXMLSettingNames.Clear;
  fProdGroupList.Clear;

  for i:=0 to High(fClassUncut) do begin
    fClassUncut[i] := 0.0;
    fClassCut[i]   := 0.0;
  end;

  for i:=0 to High(fSpliceUncut) do begin
    fSpliceUncut[i] := 0.0;
    fSpliceCut[i]   := 0.0;
  end;

  for i:=0 to High(fSiroUncut) do begin
    fSiroUncut[i] := 0.0;
    fSiroCut[i]   := 0.0;
  end;

//  for i:=0 to cNumOfClassFields[cdClassUncut] do begin
//    fClassUncut[i] := 0.0;
//    fClassCut[i]   := 0.0;
//  end;
//
//  for i:=1 to cNumOfClassFields[cdSpliceUncut] do begin
//    fSpliceUncut[i] := 0.0;
//    fSpliceCut[i]   := 0.0;
//  end;
//
//  for i:=1 to cNumOfClassFields[cdSiroUncut] do begin
//    fSiroUncut[i] := 0.0;
//    fSiroCut[i]   := 0.0;
//  end;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.FillValuesToMatrix(aDefects, aCuts: Boolean; aMatrix: TQualityMatrix);
var
  i: Integer;
  xCount: Integer;
  xClassData: TClassDataTyp;
begin
  CodeSite.SendMsgEx(csmOrange, 'TClassDataReader.FillValuesToMatrix(');
  aMatrix.ClearValues;

  // fill in data values depending of MatrixTyp and DataView
  xCount := aMatrix.TotFields;
  case aMatrix.MatrixType of
    mtShortLongThin: xClassData := cdClassUncut;
    mtSplice:        xClassData := cdSpliceUncut;
    mtSiro:          xClassData := cdSIROUncut;
  else
    Exit;
  end;

  if aDefects then begin
    for i:=0 to xCount-1 do begin
      aMatrix.SetValue(i, cDefects, Value[xClassData, i]);
//      CodeSite.SendFmtMsg('%d: CDR=%f; MV=%f', [i+1, Value[xClassData, i], aMatrix.GetValue(i, cDefects)]);
//      CodeSite.SendFloat('Field ' + inttostr(i+1), QMatrix.GetValue(i, cDefects));

    end;
  end;

  if aCuts then begin
    inc(xClassData);
    for i:=0 to xCount-1 do
      aMatrix.SetValue(i, cCuts, Value[xClassData, i]);
  end;
  aMatrix.Invalidate;
{
  ClearValues;
  xCount := GetMatrixFieldCount;
  for i:=1 to xCount do begin
    case MatrixTyp of
      mtShortLongThin: begin
          if aDefects then
            fQMatrix.SetValue(i-1, cDefects, aCDR.ClassUncut[i]);
          if aCuts then
            fQMatrix.SetValue(i-1, cCuts,    aCDR.ClassCut[i]);
        end;
      mtSplice: begin
          if aDefects then
            fQMatrix.SetValue(i-1, cDefects, aCDR.SpliceUncut[i]);
          if aCuts then
            fQMatrix.SetValue(i-1, cCuts,    aCDR.SpliceCut[i]);
        end;
      mtSiro: begin
          if aDefects then
            fQMatrix.SetValue(i-1, cDefects, aCDR.SiroUncut[i]);
          if aCuts then
            fQMatrix.SetValue(i-1, cCuts,    aCDR.SiroCut[i]);
        end;
    else
    end;
  end;
{}
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetBaseSQL: string;
begin
  if not fInitialized then
    fInitialized := DM.Init;

  Result := DM.GetBaseSQL('', '1=1');
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetClassCut(Index: Integer): Double;
begin
  Result := GetClassValue(cdClassCut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetClassUncut(Index: Integer): Double;
begin
  Result := GetClassValue(cdClassUncut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetClassValue(aClassTyp: TClassDataTyp; aIndex: Integer): Double;
//var
//  xCursor: TCursor;
begin
//  xCursor := Screen.Cursor;
  if not(aClassTyp in mAvailableClassData) then
  try
    Screen.Cursor   := crSQLWait;
    Mouse.CursorPos := Mouse.CursorPos;
    ReadClassData(aClassTyp);
  finally
    Screen.Cursor := crDefault; //xCursor;
  end;
  
  case aClassTyp of
    cdClassUncut:  Result := fClassUncut[aIndex];
    cdClassCut:    Result := fClassCut[aIndex];
    cdSpliceUncut: Result := fSpliceUncut[aIndex];
    cdSpliceCut:   Result := fSpliceCut[aIndex];
    cdSiroUncut:   Result := fSiroUncut[aIndex];
    cdSiroCut:     Result := fSiroCut[aIndex];
  else
    Result := 0;
  end;
  
  if Params.LenBase <> lbAbsolute then
    Result := CalcValue(Params.LenBase, Result, Len);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetDM: TClassDataModule;
begin
  Result := fDM;
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetLen: Double;
begin
  if not mLenReady then begin
    ReadLenData;
    mLenReady := True;
  end;
  Result := fLen;
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetSiroCut(Index: Integer): Double;
begin
  Result := GetClassValue(cdSIROCut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetSiroUncut(Index: Integer): Double;
begin
  Result := GetClassValue(cdSIROUncut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetSpliceCut(Index: Integer): Double;
begin
  Result := GetClassValue(cdSpliceCut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetSpliceUncut(Index: Integer): Double;
begin
  Result := GetClassValue(cdSpliceUncut, Index);
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetXMLSettings: TXMLSettingsList;
begin
  if not fXMLSettingsReady then begin
    if not Initialized then
      Init;

    try
      fDM.GetXMLSettings(fXMLSettings);
    except
      on e:Exception do
        CallOnException(e.message);
    end;
    fXMLSettingsReady := True;
  end;
  Result := fXMLSettings;
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetXMLSettingNames: TStringList;
begin
  if fXMLSettingNames.Count = 0 then begin
    if not Initialized then
      Init;

    try
      fDM.GetXMLSettingNames(fXMLSettingNames);
    //    Codesite.SendInteger('YMSettingsCount', fYMSettings.Count);
    except
      on e:Exception do
        CallOnException(e.message);
    end;
  end;
  Result := fXMLSettingNames;
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetProdGroupList: TProdGroupList;
begin
  if fProdGroupList.Count = 0 then begin
    if not Initialized then
      Init;

    try
      fDM.GetProdGroupList(fProdGroupList);
    except
      on e:Exception do
        CallOnException(e.message);
    end;
  end;
  Result := fProdGroupList;
end;

//------------------------------------------------------------------------------
function TClassDataReader.GetXMLSettingsFromID(aID: Integer; var aXMLSettings: TXMLSettingsInfoRec): Boolean;
begin
  Result := fDM.GetXMLSettingsFromID(aID, aXMLSettings);
end;

//------------------------------------------------------------------------------
function TClassDataReader.Init: Boolean;
begin
  EnterMethod('TClassDataReader.Init');
  Clear;
  if not fInitialized then
  try
    fInitialized := DM.Init;
  except
    on e:Exception do
      raise Exception.Create('TClassDataReader.Init: ' + e.Message);
  end;
  Result := fInitialized;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.ParamChanged(Source: TObject);
begin
  EnterMethod('ParamChanged');
  if fInitialized then
    Clear;
  fInitialized := False;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.ReadClassData(aClassTyp: TClassDataTyp);
begin
  //  CodeSite.EnterMethod('ReadClassData');
  if not Initialized then
    Init;
  
  if Params.TimeMode in [tmShift,tmLongtermWeek] then begin    //NUE
      // in shift mode: read individual class data only if necessary at call
    ReadClassDataShift(aClassTyp);
    include(mAvailableClassData, aClassTyp);
  end else begin
      // in interval mode: if we get one class data we have ALL data available
    ReadClassDataInterval(aClassTyp);
    mAvailableClassData := [cdClassUncut, cdClassCut, cdSpliceUncut, cdSpliceCut, cdSIROUncut, cdSIROCut];
  end;
  //  CodeSite.ExitMethod('ReadClassData');
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.ReadClassDataInterval(aClassTyp: TClassDataTyp);
var
  i: Integer;
  xClassImageRec: TClassFieldRec;
  xStream: TMemoryStream;
  xData: Variant;
  xBuffer: PByte;
begin
  if fDM.PrepareClassDataQuery(aClassTyp) then begin
    with fDM.ResultDataSet do
    try
      while not EOF do begin
        // read the blob field into record with class data arrays
        xStream := TMemoryStream.Create;
        try
          // Um ein Variant Array zu bekommen, muss �ber das native ADO Objekt abgefragt werden
          xData := Recordset.Fields[cFLD_ClassImage].Value;
          if xData <> NULL then begin
            xBuffer := VarArrayLock(xData);
            try
              xStream.Write(xBuffer^, Recordset.Fields[cFLD_ClassImage].ActualSize);
            finally
              VarArrayUnLock(xData);
            end;
          end;// if xData <> NULL then begin
          xStream.Position := 0;
          xStream.Read(xClassImageRec , sizeof (TClassFieldRec));
        finally
          xStream.Free;
        end;// try finally

        // unpack class data array into ResultData array
        for i:=0 to High(fClassUncut) do begin
          fClassUncut[i] := fClassUncut[i] + xClassImageRec.ClassUncutField[i+1];
          fClassCut[i]   := fClassCut[i]   + xClassImageRec.ClassCutField[i+1];
        end;

        for i:=0 to High(fSpliceUncut) do begin
          fSpliceUncut[i] := fSpliceUncut[i] + xClassImageRec.SpUncutField[i+1];
          fSpliceCut[i]   := fSpliceCut[i]   + xClassImageRec.SpCutField[i+1];
        end;

        for i:=0 to High(fSiroUncut) do begin
          fSiroUncut[i] := fSiroUncut[i] + xClassImageRec.SiroUncutField[i+1];
          fSiroCut[i]   := fSiroCut[i]   + xClassImageRec.SiroCutField[i+1];
        end;

//        for i:=0 to cNumOfClassFields[cdClassUncut] do begin
//          fClassUncut[i] := fClassUncut[i] + xClassImageRec.ClassUncutField[i];
//          fClassCut[i]   := fClassCut[i]   + xClassImageRec.ClassCutField[i];
//        end;
//
//        for i:=1 to cNumOfClassFields[cdSpliceUncut] do begin
//          fSpliceUncut[i] := fSpliceUncut[i] + xClassImageRec.SpUncutField[i];
//          fSpliceCut[i]   := fSpliceCut[i]   + xClassImageRec.SpCutField[i];
//        end;
//
//        for i:=1 to cNumOfClassFields[cdSiroUncut] do begin
//          fSiroUncut[i] := fSiroUncut[i] + xClassImageRec.SiroUncutField[i];
//          fSiroCut[i]   := fSiroCut[i]   + xClassImageRec.SiroCutField[i];
//        end;

        Next;
      end; // while
  //      CodeSite.SendInteger('ReadClassDataInterval: RecordCount', RecordCount);
    except
      on e:Exception do
        raise Exception.Create('TClassDataModule.ReadClassDataInterval: ' + e.Message);
    end; // with mDM
  end; // mDM
  //  CodeSite.ExitMethod('ReadClassDataInterval');
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.ReadClassDataShift(aClassTyp: TClassDataTyp);
var
  i: Integer;
  xField: TField;
  xHighIndex: Integer;
begin
  EnterMethod('ReadClassDataShift');
  //  CodeSite.EnterMethod('ReadClassDataShift');
  try
    if fDM.PrepareClassDataQuery(aClassTyp) then begin
      case aClassTyp of
        cdSpliceUncut, cdSpliceCut: xHighIndex := High(fSpliceUncut);
        cdSIROUncut, cdSIROCut:     xHighIndex := High(fSiroUncut);
      else // cdClassUncut, cdClassCut
        xHighIndex := High(fClassUncut);
      end;

      with fDM.ResultDataSet do
      try
        for i:=0 to xHighIndex do begin
          xField := FieldByName(Format(cClassFieldMask[aClassTyp], [i+1]));
          case aClassTyp of
            cdClassUncut:  fClassUncut[i]  := xField.AsFloat;
            cdClassCut:    fClassCut[i]    := xField.AsFloat;
            cdSpliceUncut: fSpliceUncut[i] := xField.AsFloat;
            cdSpliceCut:   fSpliceCut[i]   := xField.AsFloat;
            cdSIROUncut:   fSiroUncut[i]   := xField.AsFloat;
            cdSIROCut:     fSiroCut[i]     := xField.AsFloat;
          else
          end;
        end; // for
        Close;
      except
        on e:Exception do
          raise Exception.Create('TClassDataReader.ReadClassDataShift: ' + e.Message);
      end;
    end;
  except
    on e:Exception do
      CallOnException(e.Message);
  end;
  //  CodeSite.ExitMethod('ReadClassDataShift');
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.ReadLenData;
begin
  //  CodeSite.EnterMethod('ReadLenData');
  fLen := 0.0;
  if not Initialized then
    Init;
  
  try
    fLen := fDM.GetLength;
  //    Codesite.SendFloat('Len', fLen);
  except
    on e:Exception do
      CallOnException(e.Message);
  end;
  //  CodeSite.ExitMethod('ReadLenData');
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetClassCut(Index: Integer; const Value: Double);
begin
  fClassCut[Index] := Value;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetClassUncut(Index: Integer; const Value: Double);
begin
  fClassUncut[Index] := Value;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetParams(const Value: TQueryParameters);
begin
  if Assigned(Value) then
    fParams.Assign(Value);
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetSiroCut(Index: Integer; const Value: Double);
begin
  fSiroCut[Index] := Value;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetSiroUncut(Index: Integer; const Value: Double);
begin
  fSiroUncut[Index] := Value;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetSpliceCut(Index: Integer; const Value: Double);
begin
  fSpliceCut[Index] := Value;
end;

//------------------------------------------------------------------------------
procedure TClassDataReader.SetSpliceUncut(Index: Integer; const Value: Double);
begin
  fSpliceUncut[Index] := Value;
end;

function TClassDataReader.SettingsAreSameHeadClass: Boolean;
var
  i: Integer;
  xLastHeadClass: TSensingHeadClass;
begin
  Result         := True;
  xLastHeadClass := shcNone;
  i              := XMLSettings.Count-1;
  while (i >= 0) and Result do begin
    if xLastHeadClass = shcNone then xLastHeadClass := XMLSettings[i]^.HeadClass
                                else Result := (xLastHeadClass = XMLSettings[i]^.HeadClass);
    dec(i);
  end;

//  Result := True;
//  i      := 0;
//  while (i < XMLSettings.Count) and Result do begin
//    if i = 0 then xLastHeadClass := XMLSettings[0]^.HeadClass
//             else Result := (xLastHeadClass = XMLSettings[i]^.HeadClass);
//    inc(i);
//  end;
end;

{:------------------------------------------------------------------------------
 TProdGroupList}
{:-----------------------------------------------------------------------------}
procedure TProdGroupList.Clear;
var
  i: Integer;
begin
  for i:=0 to Count-1 do
    Delete(i);
  inherited Clear;
end;

//------------------------------------------------------------------------------
procedure TProdGroupList.Delete(aIndex: Integer);
begin
  if (aIndex >= 0) and (aIndex < Count) then begin
    TObject(Items[aIndex]).Free;
    inherited Delete(aIndex);
  end;
end;

//------------------------------------------------------------------------------
function TProdGroupList.GetItems(Index: Integer): TProdGroupInfo;
begin
  Result := inherited Items[Index];
end;

//------------------------------------------------------------------------------
procedure TProdGroupList.SetItems(Index: Integer; const Value: TProdGroupInfo);
begin
  inherited Items[Index] := Value;
end;


end.









