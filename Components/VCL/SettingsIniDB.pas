(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsIniDB.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Save setting into database like an INI file
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.11.2000  1.00  Wss | Initial Release
| 21.08.2001  1.01  Wss | Select query for delete profile different to load profile -> Init
| 14.02.2002  1.02  Wss | Fokusprobleme wenn aufgerufen aus Floor
| 03.04.2002  1.02  Wss | Kleine Bugfixes
| 27.09.2002  1.02  Wss | Auf TadoDBAccess Klasse umgestellt
| 02.10.2003  1.02  Wss | Handling f�r Profile wenn User in MMAdminGroup erweitert, damit
                          Admin Profile auch f�r normale Benutzer verwendbar sind
| 03.12.2003  1.02  Wss | In LoadDefault wird fProfile = '' gesetzt
|=========================================================================================*)
unit SettingsIniDB;

interface

uses
 SysUtils,WinTypes,WinProcs,Messages,Classes,Graphics,Controls,Forms,
 ExtCtrls,StdCtrls,Buttons,Mask,Tabs,DB,DBCtrls,Wwdbigrd,Wwdbgrid,ComCtrls,
 IvDictio,IvMulti,IvEMulti,mmImageList,ActnList,mmToolBar,mmActionList,
 mmDictionary,mmCommonLib,MemoINI,NwCombo,mmPageControl,mmPanel,ImgList,ToolWin,
 IvMlDlgs,mmButton,BASEFORM,mmTranslator, mmListView,  mmRadioButton,
 mmLabel, mmComboBox, mmStringList, IniFiles, mmBitBtn, adoDBAccess,
 mmCheckBox;

type
  TProfileMode = (pmLoad, pmSave, pmDelete);
//------------------------------------------------------------------------------
  TSettingsIniDB = class;
  TfrmLoadSettings = class;
  TOnBeforeSaveEvent = procedure (Sender: TSettingsIniDB; aOptionalChecked: Boolean) of object;
  TSettingsIniDB = class (TComponent)
  private
    fApplicationName: string;
    fDictionary: TmmDictionary;
    fFormName: string;
    fIni: TMemIniFile;
    fOnBeforeSave: TOnBeforeSaveEvent;
    fProfileName: string;
    fUserName: string;
    function CreateSettingsDialog(aMode: TProfileMode): TfrmLoadSettings;
    function GetUserName: string;
    procedure SetStringName(Index: Integer; const Value: string);
  protected
    function SaveToDB(aProfilename: String; aAsDefault: Boolean): Boolean;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function DeleteSettings: Boolean;
    function LoadDefault: Boolean;
    function LoadSettings(aProfile: String = ''): Boolean;
    procedure ReadProfiles(Strings: TStrings);
    function SaveDefault: Boolean;
    function SaveSettings(aOptionalString: String = ''): Boolean;
    property Dictionary: TmmDictionary read fDictionary write fDictionary;
    property Ini: TMemIniFile read fIni;
    property ProfileName: string read fProfileName;
  published
    property ApplicationName: string index 0  read fApplicationName write SetStringName;
    property FormName: string index 1  read fFormName write SetStringName;
    property OnBeforeSave: TOnBeforeSaveEvent read fOnBeforeSave write fOnBeforeSave;
    property UserName: string index 2  read fUserName write SetStringName;
  end;
  
//------------------------------------------------------------------------------
  TfrmLoadSettings = class (TmmForm)
    acCancel: TAction;
    acHelp: TAction;
    acOK: TAction;
    ActionList: TmmActionList;
    bCancel: TmmButton;
    bHelp: TmmButton;
    bOK: TmmButton;
    cobOptionalCheckbox: TmmCheckBox;
    cobProfilename: TmmComboBox;
    lvProfiles: TmmListView;
    mmTranslator: TmmTranslator;
    paButtons: TmmPanel;
    pnSaveProfile: TmmPanel;
    rbProfileDefault: TmmRadioButton;
    rbProfileName: TmmRadioButton;
    procedure acHelpExecute(Sender: TObject);
    procedure acOKExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure lvProfilesDblClick(Sender: TObject);
  private
    fApplicationName: string;
    fFormName: string;
    fProfileMode: TProfileMode;
    fProfileName: string;
    fSaveAsDefault: Boolean;
    fUserName: string;
    mProfileList: TmmStringList;
    function GetDictionary: TIvDictionary;
    procedure Init;
    procedure SetDictionary(const Value: TIvDictionary);
    procedure SetProfileMode(const Value: TProfileMode);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property ApplicationName: string read fApplicationName write fApplicationName;
    property Dictionary: TIvDictionary read GetDictionary write SetDictionary;
    property FormName: string read fFormName write fFormName;
    property ProfileMode: TProfileMode read fProfileMode write SetProfileMode;
    property ProfileName: string read fProfileName;
    property SaveAsDefault: Boolean read fSaveAsDefault;
    property UserName: string read fUserName write fUserName;
  end;
  
//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  mmCS,
  Dialogs, LoepfeGlobal, MMDialogs,
  mmLib, registry;

//------------------------------------------------------------------------------
resourcestring
  cMsgProfileExists = '(*)Profil existiert bereits. Wollen Sie es ueberschreiben?'; // ivlm

const
  cTempDatabaseName = 'SettingsIniDBName';
  cBoolStrings: Array[Boolean] of String = ('(*)Nein', '(*)Ja'); // ivlm

  cProfileCaption: Array[TProfileMode] of String = (
    '(40)Profil laden',      // ivlm
    '(40)Profil speichern',  // ivlm
    '(40)Profil loeschen'     // ivlm
  );
//------------------------------------------------------------------------------

  cQryOpenProfile =   'select c_UserName, c_ProfileName, c_Default, c_Admin '+
                      'from t_Settings '+
                      'where c_ApplicationName = ''%s'' '+
                      'and c_FormName = ''%s'' '+
                      'and (c_UserName = ''%s'' or c_admin = 1) '+
                      'order by c_ProfileName ';

  cQrySelectForDeleteProfile = 'select c_UserName, c_ProfileName, c_Default '+
                      'from t_Settings '+
                      'where c_ApplicationName = ''%s'' '+
                      'and c_FormName = ''%s'' '+
                      'and c_UserName = ''%s'' '+
                      'and c_default = 0 '+
                      'order by c_ProfileName ';
//------------------------------------------------------------------------------
  cQrySelectDefaultProfile = 'select c_profilename, c_username, c_default, c_admin, c_Text from t_Settings ' +
                             'where c_Default = 1 ' +
                             'and c_ApplicationName = :Appl ' +
                             'and c_FormName = :Form ' +
                             'and (c_UserName = :User or c_Admin = 1)';


  cQryInsertProfile = 'insert into t_Settings (c_UserName, c_ApplicationName, c_FormName, c_ProfileName, c_Default, c_Admin, c_Text) '+
                      'VALUES(:User, :Appl, :Form, :Profile, :Default, :Admin, :Text)';

  cQryClearDefaultProfile = 'update t_Settings  set c_Default = 0 '+
                            'where c_UserName = :User '+
                            'and c_ApplicationName = :Appl '+
                            'and c_FormName = :Form ';
//------------------------------------------------------------------------------
  cWhereProfile = 'where c_UserName = :User ' +
                  'and c_ApplicationName = :Appl ' +
                  'and c_FormName = :Form ' +
                  'and c_ProfileName = :Profile ';

  cWhereProfileOpen = 'where (c_UserName = :User or c_Admin = 1) ' +
                      'and c_ApplicationName = :Appl ' +
                      'and c_FormName = :Form ' +
                      'and c_ProfileName = :Profile ';

  cQryLoadProfile = 'select c_Text from t_Settings ' + cWhereProfileOpen;

  cQryUpdateProfile = 'update t_Settings  set c_Text = :Text ' + cWhereProfile;

  cQryUpdateDefaultProfile = 'update t_Settings  set c_Text = :Text, c_Default = :Default ' + cWhereProfile;

  cQryDeleteProfile = 'delete from t_Settings ' + cWhereProfile;
//------------------------------------------------------------------------------


//:---------------------------------------------------------------------------
//:--- Class: TSettingsIniDB
//:---------------------------------------------------------------------------
constructor TSettingsIniDB.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fIni                  := TMemIniFile.Create('');
  fOnBeforeSave := Nil;
  fProfileName          := '';
  
  if not(csDesigning in ComponentState) then begin
    if Assigned(aOwner) then fFormName := UpperCase(aOwner.ClassName)
                        else fFormName := '';
    fApplicationName := UpperCase(gApplicationName);
    fUserName        := '';
  end else begin
    fApplicationName := '';
    fFormName        := '';
    fUserName        := '';
  end;
end;

//:---------------------------------------------------------------------------
destructor TSettingsIniDB.Destroy;
begin
  FreeAndNil(fIni);
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.CreateSettingsDialog(aMode: TProfileMode): TfrmLoadSettings;
begin
  Result := TfrmLoadSettings.Create(Owner);
  with Result do
  try
    ApplicationName := Self.fApplicationName;
    if Assigned(fDictionary) then
      Dictionary      := Self.fDictionary;
    FormName        := Self.fFormName;
    ProfileMode     := aMode;
    UserName        := GetUserName;
  except
  end;
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.DeleteSettings: Boolean;
begin
  Result := False;
  with CreateSettingsDialog(pmDelete) do
  try
    if ShowModal = mrOK then begin
      with TAdoDBAccess.Create(1, False) do
      try
        if Init then
          with Query[cPrimaryQuery] do begin
            SQL.Text := cQryDeleteProfile;
            ParamByName('User').AsString     := GetUserName;
            ParamByName('Appl').AsString     := Self.fApplicationname;
            ParamByName('Form').AsString     := Self.fFormName;
            ParamByName('Profile').AsString  := ProfileName;
            ExecSQL;
            Result := True;
          end;
      finally
        Free;
      end; //with
    end;
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.GetUserName: string;
begin
  if fUserName = '' then
    Result := UpperCase(GetCurrentMMUser)
  else
    Result := UpperCase(fUserName);
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.LoadDefault: Boolean;
var
  xList: TmmStringList;
begin
  Result := False;
  fProfileName := '';
  xList := TmmStringList.Create;
  with TAdoDBAccess.Create(1, False) do
  try
    if Init then
      with Query[cPrimaryQuery] do begin
        SQL.Text := cQrySelectDefaultProfile;
        ParamByName('Appl').AsString := fApplicationname;
        ParamByName('Form').AsString := fFormname;
        ParamByName('User').AsString := GetUserName;
        Open;
        First;
        // TODO: keine Daten auf DB noch pr�fen ob das so funktioniert
        while not EOF do begin
          if CompareText(FieldByName('c_UserName').AsString, GetUserName) = 0 then begin
            fProfileName := FieldByName('c_ProfileName').AsString;
            xList.Text := FieldByName('c_Text').AsString;
            Result := True;
            Break;
          end;
          Next;
        end;
      end;
  finally
    Free;
    fIni.SetStrings(xList);
    xList.Free;
  end; //with
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.LoadSettings(aProfile: String = ''): Boolean;
var
  xList: TmmStringList;
begin
  Result := False;
  fProfileName := aProfile;
  // wenn kein Parameter angegeben wird, dann normal das Auswahlfenster zeigen
  if fProfileName = '' then
    with CreateSettingsDialog(pmLoad) do
    try
      if ShowModal = mrOK then
        Self.fProfileName := Profilename;
    finally
      Free;
    end;
  
  // nur wenn fProfileName nicht leer ist, dann versuchen das Profil laden
  if fProfileName <> '' then begin
    xList := TmmStringList.Create;
    with TAdoDBAccess.Create(1, False) do
    try
      if Init then
        with Query[cPrimaryQuery] do begin
          SQL.Text := cQryLoadProfile;
          ParamByName('User').AsString := GetUserName;
          ParamByName('Appl').AsString := Self.fApplicationname;
          ParamByName('Form').AsString := Self.fFormname;
          ParamByName('Profile').AsString := fProfileName;
          Open;
          if FindFirst then begin
            xList.Text := FieldByName('c_Text').AsString;
            Result := True;
          end;
        end;
    finally
      Free;
      fIni.SetStrings(xList);
      xList.Free;
    end; //with
  end;
end;

//:---------------------------------------------------------------------------
procedure TSettingsIniDB.ReadProfiles(Strings: TStrings);
begin
  Strings.Clear;
  with TAdoDBAccess.Create(1, False) do
  try
    if Init then
      with Query[cPrimaryQuery] do begin
        SQL.Text := Format(cQryOpenProfile, [fApplicationName, fFormName, GetUserName]);
        CodeSite.SendString('ReadProfiles', SQL.Text);
        Open;
        while not EOF do begin
          Strings.Add(FieldByName('c_profilename').AsString);
          Next;
        end;
      end;
  finally
    Free;
  end; //with
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.SaveDefault: Boolean;
begin
  Result := SaveToDB('DEFAULT', True);
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.SaveSettings(aOptionalString: String = ''): Boolean;
begin
  Result := False;
  with CreateSettingsDialog(pmSave) do
  try
    cobOptionalCheckbox.Caption := aOptionalString;
    cobOptionalCheckbox.Visible := (aOptionalString <> '');
    if ShowModal = mrOK then begin
      Self.fProfileName := ProfileName;
      if Assigned(fOnBeforeSave) then
        fOnBeforeSave(Self, cobOptionalCheckbox.Checked);
      Result := SaveToDB(Self.fProfileName, SaveAsDefault);
    end;
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
function TSettingsIniDB.SaveToDB(aProfilename: String; aAsDefault: Boolean): Boolean;
var
  xStrList: TmmStringList;
  //...........................................................
  function IsAdmin: Boolean;
  var
    xUserName: array[0..100] of Char;
    xSize: DWord;
    xStr: String;
  begin
    Result := False;
    with TRegistry.Create do
    try
      Rootkey := cRegCU;
      if OpenKeyReadOnly(cRegMMSecurityPath) then begin
        if ValueExists(cRegLogonUserGroups) then
          xStr := ReadString(cRegLogonUserGroups)
        else if ValueExists(cRegDefUserGroups) then
          xStr := ReadString(cRegDefUserGroups);
      end;
      Result := AnsiPos('MMAdminGroup', xStr) > 0;
    finally
      Free;
    end;
  end;
  //...........................................................
begin
  Result := False;
  xStrList := TmmStringList.Create;
  fIni.GetStrings(xStrList);
  with TAdoDBAccess.Create(1, False) do
  try
    if Init then
      with Query[cPrimaryQuery] do begin
        // if this is saved as default then set all other records to no default
        if aAsDefault then begin
          // set all settings for current User/Appl/Form as no default
          SQL.Text := cQryClearDefaultProfile;
          ParamByName('User').AsString := GetUserName;
          ParamByName('Appl').AsString := fApplicationname;
          ParamByName('Form').AsString := fFormname;
          try
            ExecSQL;
          finally
            Close;
          end;
        end;
        // Zuerst ein m�gliches vorhandenes Profil mit gleichem Namen l�schen
        SQL.Text := cQryDeleteProfile;
        ParamByName('User').AsString    := GetUserName;
        ParamByName('Appl').AsString    := fApplicationname;
        ParamByName('Form').AsString    := fFormname;
        ParamByName('Profile').AsString := aProfilename;
        ExecSQL;
        // Nun existiert ein gleiches Profil garaniert nicht mehr f�r diesen Benutzer
        SQL.Text := cQryInsertProfile;
        ParamByName('Default').AsBoolean := aAsDefault;
        ParamByName('Admin').AsBoolean   := IsAdmin;

        ParamByName('User').AsString     := GetUserName;
        ParamByName('Appl').AsString     := fApplicationname;
        ParamByName('Form').AsString     := fFormName;
        ParamByName('Profile').AsString  := aProfileName;
        ParamByName('Text').AsString     := xStrList.Text;
        ExecSQL;
        fProfileName := aProfileName;
        Result       := True;
      end; //with
  finally
    Free;
    xStrList.Free;
  end; //TAdoDBAccess.Create(1, False)
end;

//:---------------------------------------------------------------------------
procedure TSettingsIniDB.SetStringName(Index: Integer; const Value: string);
begin
  case Index of
    0: fApplicationName := UpperCase(Value);
    1: fFormName        := UpperCase(Value);
    2: fUserName        := UpperCase(Value);
  else
  end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TfrmLoadSettings
//:---------------------------------------------------------------------------
constructor TfrmLoadSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fApplicationName := '';
  fFormName        := '';
  fProfileName     := '';
  fUserName        := '';
  
  mProfileList := TmmStringList.Create;
end;

//:---------------------------------------------------------------------------
destructor TfrmLoadSettings.Destroy;
begin
  mProfileList.Free;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.acHelpExecute(Sender: TObject);
begin
  Application.HelpCommand(0, HelpContext);
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.acOKExecute(Sender: TObject);
begin
  if fProfileMode = pmSave then begin
    if rbProfileName.Checked then begin
      fSaveAsDefault := False;
      fProfileName   := cobProfilename.Text
    end else begin
      fSaveAsDefault := True;
      fProfileName   := 'DEFAULT';
    end;
  end else begin
    fProfileName := lvProfiles.Selected.Caption;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  cobProfilename.Enabled := rbProfileName.Checked;
  acOK.Enabled := ((ProfileMode = pmSave) and ((Trim(cobProfilename.Text) <> '') or rbProfileDefault.Checked)) OR
                   ((ProfileMode in [pmLoad, pmDelete]) and Assigned(lvProfiles.Selected));
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var
  i: Integer;
begin
  if ModalResult = mrOK then begin
    if fProfileMode = pmSave then begin
      CanClose := True;
      for i:=0 to cobProfilename.Items.Count-1 do begin
        if CompareText(fProfileName, cobProfilename.Items[i]) = 0 then begin
          CanClose := (MMMessageDlg(cMsgProfileExists, mtConfirmation, mbOKCancel, 0, Self) = mrOK);
          Break;
        end;
      end;
    end else
      CanClose := (Trim(fProfileName) <> '');
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.FormShow(Sender: TObject);
begin
  Init;
end;

//:---------------------------------------------------------------------------
function TfrmLoadSettings.GetDictionary: TIvDictionary;
begin
  Result := TmmDictionary(mmTranslator.Dictionary);
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.Init;
var
  xQuery: TNativeAdoQuery;
  xItem: TListItem;
  
  //-----------------------------------------------------------------------
  procedure FillListView(aFilter: String; aExists: Boolean);
  var
    xBool: Boolean;
    xInt: Integer;
  begin
    with xQuery do
    try
      First;
      while not EOF do begin
        xInt := CompareText(FieldByName('c_UserName').AsString, aFilter);
        if ((xInt = 0) and aExists) or ((xInt <> 0) and not aExists) then begin
          xItem := lvProfiles.Items.Add;
          xItem.Caption := FieldByName('c_ProfileName').AsString;
          xItem.SubItems.Add(FieldByName('c_UserName').AsString);
          xBool := FieldByName('c_Default').AsBoolean;
          if Assigned(mmTranslator.Dictionary) then
            xItem.SubItems.Add(mmTranslator.Dictionary.Translate(cBoolStrings[xBool]))
          else
            xItem.SubItems.Add(cBoolStrings[xBool]);
        end;
        Next;
      end; // while
    except
    end;
  end;
  //-----------------------------------------------------------------------
  procedure FillCombobox(aFilter: String);
  begin
    with xQuery do
    try
      First;
      while not EOF do begin
        if CompareText(FieldByName('c_UserName').AsString, aFilter) = 0 then
          cobProfilename.Items.Add(FieldByName('c_ProfileName').AsString);
        Next;
      end; // while
    except
    end;
  end;
  //-----------------------------------------------------------------------
  
begin
  with TAdoDBAccess.Create(1, False) do
  try
    if Init then begin
      xQuery := Query[cPrimaryQuery];
      with xQuery do begin
        if fProfileMode = pmDelete then
          SQL.Text := Format(cQrySelectForDeleteProfile, [fApplicationName, fFormName, fUserName])
        else
          SQL.Text := Format(cQryOpenProfile, [fApplicationName, fFormName, fUserName]);
  
        Open;
        if fProfileMode = pmSave then begin
          FillCombobox(fUsername);
          cobProfilename.Text := fProfileName;
          cobProfilename.SetFocus;
        end else begin
          // Zuerst die Profile vom aktuellen Benutzer
          FillListView(fUsername, True);

          // anschliessend die allgemeinen Profile mit Admin Flag
          if fProfileMode = pmLoad then
            FillListView(fUsername, False);
  
          if lvProfiles.Items.Count > 0 then
            lvProfiles.Items.Item[0].Selected := True;
        end;
      end; // with xQuery
    end; // if Init
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.lvProfilesDblClick(Sender: TObject);
begin
  bOK.Click;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.SetDictionary(const Value: TIvDictionary);
begin
  if (Value <> mmTranslator.Dictionary) and Assigned(Value) then
    mmTranslator.Dictionary := Value;
end;

//:---------------------------------------------------------------------------
procedure TfrmLoadSettings.SetProfileMode(const Value: TProfileMode);
begin
  fProfileMode := Value;
  Caption := Translate(cProfileCaption[fProfileMode]);
  if fProfileMode = pmSave then begin
    lvProfiles.Visible    := False;
    pnSaveProfile.Visible := True;
    pnSaveProfile.Align   := alClient;
  end else begin
    pnSaveProfile.Visible := False;
    lvProfiles.Visible := True;
    lvProfiles.Align   := alClient;
  end;
end;

end.


