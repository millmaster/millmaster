(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: YMRepSettingsFrame.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 00.07.2001  1.00  Wss | Initial Release
| 15.07.2001  1.01  Wss | WarningMsg label added
| 12.02.2002  1.02  Wss | QualityMatrix Komponenten auf TSizingPanel platziert fuer gleiche
                          Groessenverhaeltnise in den versch. Applikationen
| 04.03.2002  1.02  Wss | Seitenverhaeltnis wird nun direkt in Komponente TRepQMatrix behandelt
                          -> SizingPanel nicht mehr noetig
| 04.07.2003  1.02  Wss | Bei OnResize wird das rechte Panel auch mit angepasst, damit die
                          SIRO Matrix auch in der Gr�sse ver�ndert wird.
| 17.02.2004  1.02  Wss | Clear ruft nun Clear der Matrixen auf statt nur ClearValues von mQMatrix
| 20.10.2004  1.02  Wss | Parameter wurde nicht an F-Cluster weitergegeben
| 06.04.2005  1.02  SDo | Func. ShowYMSettingsInfoRec() QMatrix Cluster Kurve sichtbar, wenn FaultCluster sichtbar
                          wss: wieder ausgeklammert und die Visualisierung direkt in CLA gesetzt
| 15.06.2005  1.02  Wss | Wiederholungen aus den EditBoxen entfernt
| 10.01.2008  1.03  Nue | Einbau von VCV.
| 21.01.2008  1.03  Nue | Einbau von Spleissklassierung.
|=========================================================================================*)
unit YMRepSettingsFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  YMParaDef, ExtCtrls, mmPanel, MMUGlobal, YMParaEditBox,
  StdCtrls, mmGroupBox, EditBox, mmSplitter, mmLabel, DataLayerDef,
  SizingPanel, XMLDef, QualityMatrixBase, QualityMatrix, VCLXMLSettingsModel,
  XMLSettingsModel, mmCheckBox;

type
  TFrameMode = (fmChannelData, fmSpliceData, fmFFData, fmSettings);

  {:----------------------------------------------------------------------------
   Base frame for displaying complete YMSettings in one view. Same frame is used in MMLabReport.
   ----------------------------------------------------------------------------}
  TfraRepSettings = class (TFrame)
    laWarningMsg: TmmLabel;
    pnLeft: TmmPanel;
    pnRight: TmmPanel;
    mXMLSettings: TVCLXMLSettingsModel;
    pnBottom: TmmPanel;
    mFaultCluster: TFaultClusterEditBox;
    mSFI: TSFIEditBox;
    mFCluster: TFFClusterEditBox;
    mSplice: TSpliceEditBox;
    mChannel: TChannelEditBox;
    mYarnCount: TYarnCountEditBox;
    mmPanel1: TmmPanel;
    mQMatrix: TQualityMatrix;
    mmPanel2: TmmPanel;
    mFMatrix: TQualityMatrix;
    mRepetition: TRepetitionEditBox;
    mPSettings: TPEditBox;
    mVCV: TVCVEditBox;
    mSpliceQMatrix: TQualityMatrix;
    mCurveSelection: TmmPanel;
    mcbSpliceCurve: TmmCheckBox;
    mcbChannelCurve: TmmCheckBox;
    mcbClusterCurve: TmmCheckBox;
    procedure cb1Click(Sender: TObject);
    procedure cb2Click(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure mcbChannelCurveClick(Sender: TObject);
    procedure mcbClusterCurveClick(Sender: TObject);
    procedure mcbSpliceCurveClick(Sender: TObject);
    procedure mcbSpliceCurveEnter(Sender: TObject);
  private
    fModel: TVCLXMLSettingsModel;
    fWarningMsg: TClearerSettingsWarning;
    mShowSettings: Boolean;
    mFrameMode: TFrameMode;  //Nue:20.01.08
    function GetFrameMode: TFrameMode;
    procedure SetFrameMode(const Value: TFrameMode);
    procedure SetModel(const aValue: TVCLXMLSettingsModel);
    procedure SetWarningMsg(const Value: TClearerSettingsWarning);
    property Model: TVCLXMLSettingsModel read fModel write SetModel;
  public
    constructor Create(aOwner: TComponent); override;
    procedure Clear;
    procedure ShowYMSettingsInfoRec(aSettings: TXMLSettingsInfoRec);
//    procedure ShowYMSettings(aSettings: TYMSettingsRec; aYarnCount: Single; aYarnUnit: TYarnUnit);
//    procedure ShowYMSettingsInfoRec(aSettings: TYMSettingsInfoRec);
    property WarningMsg: TClearerSettingsWarning read fWarningMsg write SetWarningMsg;
  published
    property FrameMode: TFrameMode read GetFrameMode write SetFrameMode;
  end;
  
const
  cFrameModeFieldCount: Array[fmChannelData..fmFFData] of Integer = (
    cClassUncutFields, cSpUncutFields, cSIROUncutFields
  );

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  QualityMatrixDef;

{:------------------------------------------------------------------------------
 TfraRepSettings}
{:-----------------------------------------------------------------------------}
procedure TfraRepSettings.Clear;
begin
  mQMatrix.ClearValues;
  mSpliceQMatrix.ClearValues;   //Nue:20.01.08
  mFMatrix.ClearValues;
end;

{:-----------------------------------------------------------------------------}
constructor TfraRepSettings.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mShowSettings       := False;
end;

procedure TfraRepSettings.cb1Click(Sender: TObject);
begin
  mQMatrix.Visible := mcbChannelCurve.Checked; //Nue:2.7.07
  mSpliceQMatrix.Visible := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbSpliceCurve.Checked := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked;
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked; //Nue:2.7.07

end;

procedure TfraRepSettings.cb2Click(Sender: TObject);
begin
  mQMatrix.Visible := mcbChannelCurve.Checked; //Nue:2.7.07
  mSpliceQMatrix.Visible := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbSpliceCurve.Checked := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked;
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked; //Nue:2.7.07

end;

{:-----------------------------------------------------------------------------}
procedure TfraRepSettings.SetFrameMode(const Value: TFrameMode);
begin
  if mFrameMode <> Value then begin
    mFrameMode := Value;
    SetWarningMsg(fWarningMsg);

    // first clean up the main matrix
    with mQMatrix do begin
      ClearValues;
    end;

//Nue:20.01.08
    // first clean up the main matrix
    with mSpliceQMatrix do begin
      ClearValues;
    end;

    // determine it the settings mode is selected and control the bottom and side panel
    mShowSettings := (mFrameMode = fmSettings);
    pnBottom.Visible := mShowSettings;
    pnRight.Visible  := mShowSettings;
    mCurveSelection.Hide; //Nue:20.01.08

    case mFrameMode of
      fmChannelData: begin
          with mQMatrix do begin
            MatrixType := mtShortLongThin;
            mQMatrix.Show;        //Nue:20.01.08
            mSpliceQMatrix.Hide;  //Nue:20.01.08
          end;
        end;
      fmSpliceData: begin
//Nue:20.01.08
//          with mQMatrix do begin
//            MatrixType := mtSplice;
//          end;
          with mSpliceQMatrix do begin
            MatrixType := mtSplice;
            mQMatrix.Hide;        //Nue:20.01.08
            mSpliceQMatrix.Show;  //Nue:20.01.08
          end;
        end;
      fmFFData: begin
          with mQMatrix do begin
            MatrixType := mtSiro;
            mQMatrix.Show;        //Nue:20.01.08
            mSpliceQMatrix.Hide;  //Nue:20.01.08
          end;
        end;
      fmSettings: begin
          mCurveSelection.Show; //Nue:20.01.08
          with mQMatrix do begin
            MatrixType := mtShortLongThin;
            if mcbChannelCurve.Checked then begin   //Nue:20.01.08
              mQMatrix.Show;        //Nue:20.01.08
              mQMatrix.ClusterVisible := mcbClusterCurve.Checked;  //Nue:20.01.08
              mSpliceQMatrix.Hide;  //Nue:20.01.08
            end
            else begin
              mQMatrix.Hide;        //Nue:20.01.08
              mSpliceQMatrix.Show;  //Nue:20.01.08
            end;
          end;
        end;
    else
    end;
//Nue:20.01.08
//    mQMatrix.CalculateMatrixSize;
//    mQMatrix.Invalidate;
    if mFrameMode=fmSpliceData then begin
      mSpliceQMatrix.CalculateMatrixSize;
      mSpliceQMatrix.Invalidate;
    end
    else begin
      mQMatrix.CalculateMatrixSize;
      mQMatrix.Invalidate;
    end;
    mQMatrix.ClusterVisible := mcbClusterCurve.Checked;  //Nue:20.01.08
//End Nue:20.01.08

  end;
end;

{:-----------------------------------------------------------------------------}
procedure TfraRepSettings.SetWarningMsg(const Value: TClearerSettingsWarning);
begin
  fWarningMsg := Value;
  laWarningMsg.Visible := ((fWarningMsg <> cswNone) and (mFrameMode <> fmSettings)) OR
                           (fWarningMsg = cswManyYarnCount);
  if laWarningMsg.Visible then
    laWarningMsg.Caption := GetWarningMsg(fWarningMsg)
  else
    laWarningMsg.Caption := GetWarningMsg(cswNone);
end;

//:-----------------------------------------------------------------------------
//procedure TfraRepSettings.ShowYMSettings(aSettings: TYMSettingsRec; aYarnCount: Single; aYarnUnit: TYarnUnit);
//begin
//  mQMatrix.PutYMSettings(aSettings);
//  mSiroMatrix.PutYMSettings(aSettings);
//  mSettingsChannel.PutYMSettings(aSettings);
//  mSettingsSplice.PutYMSettings(aSettings);
//  mFFCluster.PutYMSettings(aSettings);
//  mSettingsOthers.PutYMSettings(aSettings);
//  mSettingsOthers.YarnCount := ConvertYarnCountFloatToInt(aYarnCount);
//  mSettingsOthers.YarnUnit  := aYarnUnit;
//end;
//:-----------------------------------------------------------------------------

//procedure TfraRepSettings.ShowYMSettingsInfoRec(aSettings: TYMSettingsInfoRec);
//begin
//  with aSettings  do begin
//    mQMatrix.PutYMSettings(YMSettingsExt.YMSettings);
//    mSiroMatrix.PutYMSettings(YMSettingsExt.YMSettings);
//    mSettingsChannel.PutYMSettings(YMSettingsExt.YMSettings);
//    mSettingsSplice.PutYMSettings(YMSettingsExt.YMSettings);
//    mFFCluster.PutYMSettings(YMSettingsExt.YMSettings);
//    mSettingsOthers.PutYMSettings(YMSettingsExt.YMSettings);
//    mSettingsOthers.YarnCount := ConvertYarnCountFloatToInt(YarnCount);
//    mSettingsOthers.YarnUnit  := YarnUnit;
//  end;
//end;

//:-----------------------------------------------------------------------------
procedure TfraRepSettings.FrameResize(Sender: TObject);
begin
//  pnRight.Width := Round(Self.Width / 2.3);
end;

//:-----------------------------------------------------------------------------
function TfraRepSettings.GetFrameMode: TFrameMode;
begin
  if mFrameMode = fmSettings then
    mCurveSelection.Show //Nue:20.01.08
  else
    mCurveSelection.Hide; //Nue:20.01.08

  Result := mFrameMode;
end;

//:-----------------------------------------------------------------------------
procedure TfraRepSettings.mcbChannelCurveClick(Sender: TObject);
begin
  mQMatrix.Visible := mcbChannelCurve.Checked; //Nue:2.7.07
  mSpliceQMatrix.Visible := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbSpliceCurve.Checked := not(mcbChannelCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := mcbChannelCurve.Checked; //Nue:2.7.07
end;

//:-----------------------------------------------------------------------------
procedure TfraRepSettings.mcbClusterCurveClick(Sender: TObject);
begin
  mQMatrix.ClusterVisible := mcbClusterCurve.Checked;

end;

//:-----------------------------------------------------------------------------
procedure TfraRepSettings.mcbSpliceCurveClick(Sender: TObject);
begin
  mSpliceQMatrix.Visible :=  mcbSpliceCurve.Checked;
  mQMatrix.Visible := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbChannelCurve.Checked := not(mcbSpliceCurve.Checked); //Nue:2.7.07
  mcbClusterCurve.Enabled := not(mcbSpliceCurve.Checked);

end;


//:-----------------------------------------------------------------------------
procedure TfraRepSettings.mcbSpliceCurveEnter(Sender: TObject);
begin
   mcbClusterCurve.Enabled := not(mcbSpliceCurve.Checked); //Nue:2.7.07

end;

//:---------------------------------------------------------------------------
procedure TfraRepSettings.SetModel(const aValue: TVCLXMLSettingsModel);
begin
  if fModel <> aValue then begin
    fModel := aValue;
    mQMatrix.Model      := aValue;
    mSpliceQMatrix.Model      := aValue;   //Nue:20.01.08
    mFMatrix.Model      := aValue;
    mChannel.Model      := aValue;
    mSplice.Model       := aValue;
    mYarnCount.Model    := aValue;
    mFaultCluster.Model := aValue;
    mSFI.Model          := aValue;
    mVCV.Model          := aValue;
    mFCluster.Model     := aValue;
    mPSettings.Model    := aValue;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TfraRepSettings.ShowYMSettingsInfoRec(aSettings: TXMLSettingsInfoRec);
begin
  with aSettings  do begin
    mXMLSettings.xmlAsString := aSettings.XMLData;
    mYarnCount.YarnCount     := YarnCount;
    mYarnCount.YarnUnit      := YarnUnit;
  end;
//  mQMatrix.ClusterVisible :=  mFaultCluster.Enabled;
end;

end.


