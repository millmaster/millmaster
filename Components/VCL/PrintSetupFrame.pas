(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: PrintSetupFrame.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 27.02.2001 1.00 Wss | File created
| 20.03.2002 1.00 Wss | Fehler wenn kein Printer installiert war behoben
| 25.08.2003 1.00 Wss | - Property Routinen Get/Set Exception behandelt, da bei
                          Access Denied die Informationen weder gelesen noch geschrieben
                          werden k�nnen.
                        - Property PrinterOK: Boolean hinzugef�gt wo ausserhalb
                          darauf gepr�ft werden kann, ob der Drucker �berhaupt
                          verf�gbar ist
| 03.06.2004 1.01 SDo | Neu: Ausrichtung & Blck / White Druck
| 11.05.2005 1.01 Wss | BlackWhite bug behoben (negative Logik)
|=============================================================================*)
unit PrintSetupFrame;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, mmButton, IvMlDlgs, mmPrintDialog, mmComboBox, mmGroupBox,
  mmCheckBox, ExtCtrls, mmRadioGroup, mmRadioButton, mmLineLabel, printers;

type
  {:----------------------------------------------------------------------------
   Persistente Klasse fuer das Zusammenfassen und Herausfuehren der Eigenschaften }
  TPrintSetupPersistent = class (TPersistent)
  private
    mPrintDialog: TmmPrintDialog;
    function GetCopies: Integer;
    function GetFromPage: Integer;
    function GetMaxPage: Integer;
    function GetMinPage: Integer;
    function GetOptions: TPrintDialogOptions;
    function GetPrintRange: TPrintRange;
    function GetToPage: Integer;
    procedure SetCopies(Value: Integer);
    procedure SetFromPage(Value: Integer);
    procedure SetMaxPage(Value: Integer);
    procedure SetMinPage(Value: Integer);
    procedure SetOptions(Value: TPrintDialogOptions);
    procedure SetPrintRange(Value: TPrintRange);
    procedure SetToPage(Value: Integer);
  public
    constructor Create(aPrintDialog: TmmPrintDialog); virtual;
  published
    property Copies: Integer read GetCopies write SetCopies;
    property FromPage: Integer read GetFromPage write SetFromPage;
    property MaxPage: Integer read GetMaxPage write SetMaxPage;
    property MinPage: Integer read GetMinPage write SetMinPage;
    property Options: TPrintDialogOptions read GetOptions write SetOptions;
    property PrintRange: TPrintRange read GetPrintRange write SetPrintRange;
    property ToPage: Integer read GetToPage write SetToPage;
  end;

  {:----------------------------------------------------------------------------
   }
  TFramePrintSetup = class (TFrame)
    bPrintProperties: TmmButton;
    cobPrinters: TmmComboBox;
    gbPrinterSetup: TmmGroupBox;
    mPrintDialog: TmmPrintDialog;
    cbBlackWhite: TmmCheckBox;
    mmLineLabel1: TmmLineLabel;
    rbPortrait: TmmRadioButton;
    rbLandscape: TmmRadioButton;
    mmLineLabel2: TmmLineLabel;
    procedure bPrintPropertiesClick(Sender: TObject);
    procedure cobPrintersChange(Sender: TObject);
    procedure cbBlackWhiteClick(Sender: TObject);
    procedure rbPortraitClick(Sender: TObject);
  private
    fOnPrintSetupChanged: TNotifyEvent;
//    fPrinterIndex: Integer;
    fPrintSetup: TPrintSetupPersistent;
    mPortrait: Boolean;
    mPrinterOrientation : TPrinterOrientation;
    function GetCaption: string;
    procedure SetCaption(const Value: string);
    function GetPrinterOK: Boolean;
    function GetPrinterIndex: Integer;
    procedure CheckPrinter;
    function GetBlackWhite: Boolean;
    function GetBlackWhiteEnable: Boolean;
    function GetOrientationEnable: Boolean;
    function GetPortrait: Boolean;
    procedure SetBlackWhite(const Value: Boolean);
    procedure SetBlackWhiteEnable(const Value: Boolean);
    procedure SetOrientationEnable(const Value: Boolean);
    procedure SetPortrait(const Value: Boolean);
    procedure CheckColorPrinter;
    function GetOrientation: TPrinterOrientation;
    procedure SetOrientation(const Value: TPrinterOrientation);
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    property PrinterIndex: Integer read GetPrinterIndex;

    property PrintBlackWhite : Boolean  read GetBlackWhite write SetBlackWhite;
    property BlackWhiteEnable : Boolean  read GetBlackWhiteEnable write SetBlackWhiteEnable;
    property Portrait : Boolean  read GetPortrait write SetPortrait;
    property OrientationEnable : Boolean  read GetOrientationEnable write SetOrientationEnable;
    property Orientation : TPrinterOrientation read GetOrientation write SetOrientation;

  published
    property Caption: string read GetCaption write SetCaption;
    property OnPrintSetupChanged: TNotifyEvent read fOnPrintSetupChanged write fOnPrintSetupChanged;
    property PrinterOK: Boolean read GetPrinterOK;
    property PrintSetup: TPrintSetupPersistent read fPrintSetup write fPrintSetup;
  end;


procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,
  WinSpool;

//------------------------------------------------------------------------------
procedure Register;
begin
  RegisterComponents('Wss', [TFramePrintSetup]);
end;

{:------------------------------------------------------------------------------
 TPrintSetupPersistent}
{:-----------------------------------------------------------------------------}
constructor TPrintSetupPersistent.Create(aPrintDialog: TmmPrintDialog);
begin
  inherited Create;
  mPrintDialog  := aPrintDialog;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetCopies: Integer;
begin
  try
    Result := mPrintDialog.Copies;
  except
    Result := 1;
  end;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetFromPage: Integer;
begin
  try
    Result := mPrintDialog.FromPage;
  except
    Result := 1;
  end;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetMaxPage: Integer;
begin
  try
    Result := mPrintDialog.MaxPage;
  except
    Result := 1;
  end;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetMinPage: Integer;
begin
  try
    Result := mPrintDialog.MinPage;
  except
    Result := 1;
  end;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetOptions: TPrintDialogOptions;
begin
  try
    Result := mPrintDialog.Options;
  except
    Result := [];
  end;
end;
{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetPrintRange: TPrintRange;
begin
  try
    Result := mPrintDialog.PrintRange;
  except
    Result := prAllPages;
  end;
end;

{:-----------------------------------------------------------------------------}
function TPrintSetupPersistent.GetToPage: Integer;
begin
  try
    Result := mPrintDialog.ToPage;
  except
    Result := 1;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetCopies(Value: Integer);
begin
  try
    mPrintDialog.Copies := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetFromPage(Value: Integer);
begin
  try
    mPrintDialog.FromPage := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetMaxPage(Value: Integer);
begin
  try
    mPrintDialog.MaxPage := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetMinPage(Value: Integer);
begin
  try
    mPrintDialog.MinPage := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetOptions(Value: TPrintDialogOptions);
begin
  try
    mPrintDialog.Options := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetPrintRange(Value: TPrintRange);
begin
  try
    mPrintDialog.PrintRange := Value;
  except
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TPrintSetupPersistent.SetToPage(Value: Integer);
begin
  try
    mPrintDialog.ToPage := Value;
  except
  end;
end;

{:------------------------------------------------------------------------------
 TFramePrintSetup}
{:-----------------------------------------------------------------------------}
procedure TFramePrintSetup.bPrintPropertiesClick(Sender: TObject);
begin
  if mPrintDialog.Execute then begin
    cobPrinters.ItemIndex := Printer.PrinterIndex;
    CheckPrinter;
  end;
end;

{:-----------------------------------------------------------------------------}
procedure TFramePrintSetup.cobPrintersChange(Sender: TObject);
var x: integer;
begin
  Printer.PrinterIndex  := cobPrinters.ItemIndex;
  CheckColorPrinter;
  CheckPrinter;
  rbPortrait.OnClick(NIL);
end;

{:-----------------------------------------------------------------------------}
procedure TFramePrintSetup.CheckPrinter;
var
  xHnd: THandle;
begin
  bPrintProperties.Enabled := OpenPrinter(PChar(cobPrinters.Text), xHnd, Nil);
  ClosePrinter(xHnd);

  if Assigned(fOnPrintSetupChanged) then
    fOnPrintSetupChanged(Self);
end;

{:-----------------------------------------------------------------------------}
constructor TFramePrintSetup.Create(aOwner: TComponent);
var
  i: Integer;
begin
  inherited Create(aOwner);

  fOnPrintSetupChanged := Nil;
  fPrintSetup := TPrintSetupPersistent.Create(mPrintDialog);
  if (Printer.Printers.Count > 0) and (ComponentState = []) then begin
    cobPrinters.Enabled := True;
    cobPrinters.Items.Assign(Printer.Printers);
    cobPrinters.ItemIndex := Printer.PrinterIndex;
    CheckPrinter;
  end;

  Portrait := TRUE;
  CheckColorPrinter;
end;

{:-----------------------------------------------------------------------------}
destructor TFramePrintSetup.Destroy;
begin
  FreeAndNil(fPrintSetup);
  inherited Destroy;
end;
{:-----------------------------------------------------------------------------}
function TFramePrintSetup.GetCaption: string;
begin
  Result := gbPrinterSetup.Caption;
end;
{:-----------------------------------------------------------------------------}
function TFramePrintSetup.GetPrinterIndex: Integer;
begin
  Result := Printer.PrinterIndex;
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetPrinterOK: Boolean;
begin
  Result := bPrintProperties.Enabled;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetCaption(const Value: string);
begin
  gbPrinterSetup.Caption := Value;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.cbBlackWhiteClick(Sender: TObject);
begin
  if Assigned(fOnPrintSetupChanged) then
     fOnPrintSetupChanged(Self);
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetBlackWhite: Boolean;
begin
  Result := cbBlackWhite.Checked;
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetBlackWhiteEnable: Boolean;
begin
 Result := cbBlackWhite.Enabled;
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetOrientationEnable: Boolean;
begin
  Result := rbPortrait.Enabled;
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetPortrait: Boolean;
begin
  Result:= mPortrait;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetBlackWhite(const Value: Boolean);
begin
  cbBlackWhite.Checked := Value;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetBlackWhiteEnable(const Value: Boolean);
begin
  cbBlackWhite.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetOrientationEnable(const Value: Boolean);
begin
  rbPortrait.Enabled := Value;
  rbLandscape.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetPortrait(const Value: Boolean);
begin
  rbPortrait.Checked  := Value;
  rbLandscape.Checked := not Value;
  rbPortrait.OnClick(NIL);
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.CheckColorPrinter;
begin
  if GetDeviceCaps(Printer.handle, NUMCOLORS) <= 2 then
     cbBlackWhite.Checked := TRUE
  else
     cbBlackWhite.Checked := FALSE;

  cbBlackWhite.OnClick(self);
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.rbPortraitClick(Sender: TObject);
begin
   try
     if rbPortrait.Checked then begin
        mPortrait := TRUE;
        Printer.Orientation:= poPortrait
     end else begin
        mPortrait := FALSE;
        Printer.Orientation:= poLandscape;
     end;
     mPrinterOrientation := Printer.Orientation;

     if Assigned(fOnPrintSetupChanged) then
        fOnPrintSetupChanged(Self);
  finally
  end;
end;
//------------------------------------------------------------------------------
function TFramePrintSetup.GetOrientation: TPrinterOrientation;
begin
  Result := mPrinterOrientation;
end;
//------------------------------------------------------------------------------
procedure TFramePrintSetup.SetOrientation(
  const Value: TPrinterOrientation);
begin
  mPrinterOrientation := Value;
  if mPrinterOrientation = poPortrait then
     rbPortrait.Checked := TRUE
  else
     rbLandscape.Checked := TRUE;
  rbPortrait.OnClick(NIL);
end;
//------------------------------------------------------------------------------
end.

