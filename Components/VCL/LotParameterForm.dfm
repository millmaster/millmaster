inherited LotParameter: TLotParameter
  Left = 377
  Top = 119
  Anchors = [akRight, akBottom]
  Caption = '(*)Partie Eigenschaften'
  ClientHeight = 383
  ClientWidth = 371
  ParentBiDiMode = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object mmLabel1: TmmLabel [0]
    Left = 8
    Top = 279
    Width = 44
    Height = 13
    Caption = '(*)Name :'
    FocusControl = edPartieName
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmGroupBox1: TmmGroupBox [1]
    Left = 8
    Top = 176
    Width = 353
    Height = 89
    Caption = '(*)Einstellungen'
    TabOrder = 6
    CaptionSpace = True
  end
  object Misc1: TMisc [2]
    Left = 24
    Top = 190
    Width = 320
    Height = 73
    BiDiMode = bdRightToLeftNoAlign
    ParentBiDiMode = False
    TabOrder = 2
    FieldsInLine = True
    RunGrpData = True
  end
  inherited bOK: TmmButton
    Left = 171
    Top = 344
    Action = acOK
  end
  inherited bCancel: TmmButton
    Left = 276
    Top = 344
    Action = acCancel
    Caption = '(*)Abbrechen'
  end
  object edPartieName: TmmMaskEdit
    Left = 8
    Top = 296
    Width = 313
    Height = 21
    MaxLength = 40
    TabOrder = 3
    Visible = True
    OnChange = edPartieNameChange
    AutoLabel.Control = mmLabel1
    AutoLabel.Distance = 4
    AutoLabel.LabelPosition = lpTop
  end
  object mmColorButton1: TmmColorButton
    Left = 328
    Top = 296
    Width = 33
    Height = 21
    AutoLabel.LabelPosition = lpLeft
    Caption = '&Weitere...'
    Color = clBlack
    OnChangeColor = mmColorButton1ChangeColor
    TabOrder = 4
    TabStop = True
    Visible = True
  end
  object mmBitBtn1: TmmBitBtn
    Left = 8
    Top = 344
    Width = 97
    Height = 25
    Action = acSecurity
    Caption = '(*)Zugriffkontrolle'
    TabOrder = 5
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object mmGroupBox2: TmmGroupBox
    Left = 8
    Top = 16
    Width = 353
    Height = 145
    Caption = '(*)Maschinenbereich'
    TabOrder = 7
    CaptionSpace = True
    object mmLabel2: TmmLabel
      Left = 32
      Top = 27
      Width = 130
      Height = 13
      AutoSize = False
      Caption = '(30)Spindelbereich :'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 32
      Top = 50
      Width = 130
      Height = 13
      AutoSize = False
      Caption = '(25)Maschinen Gruppe:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel4: TmmLabel
      Left = 32
      Top = 96
      Width = 130
      Height = 13
      AutoSize = False
      Caption = '(30)Partie Start:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lSpindleRange: TmmLabel
      Left = 184
      Top = 27
      Width = 145
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lMachGrp: TmmLabel
      Left = 184
      Top = 50
      Width = 145
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lStart: TmmLabel
      Left = 184
      Top = 96
      Width = 145
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel5: TmmLabel
      Left = 32
      Top = 73
      Width = 130
      Height = 13
      AutoSize = False
      Caption = '(20)Artikel:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lStyleName: TmmLabel
      Left = 184
      Top = 73
      Width = 145
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel6: TmmLabel
      Left = 32
      Top = 119
      Width = 130
      Height = 13
      AutoSize = False
      Caption = '(30)Partie Ende:'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lStop: TmmLabel
      Left = 184
      Top = 119
      Width = 145
      Height = 13
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
  end
  object mmTranslator1: TmmTranslator
    DictionaryName = 'MainDictionary'
    Left = 72
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Filter'
        0))
  end
  object MMSecurityControl1: TMMSecurityControl
    FormCaption = '(*)Partie Eigenschaften'
    Active = True
    Left = 200
  end
  object LotParameterHandler1: TLotParameterHandler
    Left = 168
  end
  object mmActionList1: TmmActionList
    Left = 136
    object acOK: TAction
      Hint = 'OK'
      OnExecute = acOKExecute
    end
    object acCancel: TAction
      Hint = '(9)Abbrechen'
      OnExecute = acCancelExecute
    end
    object acSecurity: TAction
      Hint = '(*)Zugriffkontrolle'
      OnExecute = acSecurityExecute
    end
  end
  object conDefault: TmmADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETPC1148;Initial Catalog=MM_Win' +
      'ding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=False'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 784
    Top = 40
  end
end
