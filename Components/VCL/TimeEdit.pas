{===============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: TimeEdit.pas
| Projectpart...: Component
| Subpart.......: -
| Process(es)...: -
| Description...: Komponente fuer die Zeiteingabe. Beruecksichtigt automatisch
|                 das 12h oder 24h Zeitformat.
|                 Die Komponete ist von TCustomMaskEdit abgeleitet.
|
|                 Peroperty
|                 TimeInput  : Direkte Zeiteingabe als TDateTime in
|                              die Komponente.
|                 TimeOutput : Ausgabe der eingegebenen Zeit als TDateTime.
|
|                              Weitere Properties stammen von der TMaskEdit
|                              Komponente. 
|
| Info..........: Achtung    : Die globale Variable LongTimeFormat (aus Delphi)
|                              wird temp. veraendert und beim Verlassen der
|                              Komponente wieder in den Orginalzustand gebracht.
|                              Die Func. TimeToStr und StrToTime benuzut die
|                              globale Variable LongTimeFormat.
|                  API       : GetLocaleInfo()

| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4.0.3
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 07.05.1999  1.00  SDO | Component created
|==============================================================================}
unit TimeEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Mask, AutoLabelClass;

type

  TimeFormatEnum = (Long, Short);

  TMMTimeEdit = class(TCustomMaskEdit)
  private
    { Private-Deklarationen }
    FEditTime            : TDateTime;      // EingabeZeit mit Datum & MutationsZeit
    FTimeOutput          : TDateTime;      // AusgabeZeit mit Datum
    fAutoLabel           : TAutoLabel;
    mEditDate            : TDateTime;      // Datum (durchschlaufen)
    FTimeFormat          : TimeFormatEnum; // Zeitformat (lang, kurz)
    mTimeFormat1224      : Byte;           // Zeitformat 12h oder 24h
    mOrgTimeFormat       : String;         // Orginal Zeitformatstring
    mCursorPos           : integer;        // Akt. Cursorposition
    mInputError          : Boolean;        // Eingabefehler
    mChangeValue         : Boolean;        // Eingabe hat gewechselt

    procedure CheckValue;
    function GetVisible: Boolean;
    procedure SetMask;
    procedure SetNewTime;
    procedure SetTimeFormat(const Value: TimeFormatEnum);
    procedure SetTimeInput(const Value: TDateTime);
    procedure SetVisible(Value: Boolean); virtual;

  protected
    { Protected-Deklarationen }
    procedure Change; override;
    procedure DoExit; override;
    procedure GetLocalInformation;
    procedure DoEnter; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyUp(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetEnabled(Value: Boolean); override;
  public
    { Public-Deklarationen }
    constructor Create(aOwner : TComponent); override;
    destructor Destroy; override;
    procedure ValidateEdit; override;
    procedure SetBounds(ALeft, ATop, AWidth, AHeight: Integer); override;

    property TimeInput  : TDateTime read FEditTime write SetTimeInput;
    property TimeOutput : TDateTime read FTimeOutput;

  published
    { Published-Deklarationen }
    property TimeFormat : TimeFormatEnum read FTimeFormat write SetTimeFormat;
    property AutoLabel: TAutoLabel read fAutoLabel write fAutoLabel;
    property Visible: Boolean read GetVisible write SetVisible;
    
    property Anchors;
    property AutoSelect;
    property AutoSize;
    property BiDiMode;
    property BorderStyle;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;

    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

//procedure Register;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

{ TMMTimeEdit }

//------------------------------------------------------------------------------
{
procedure Register;
begin
  RegisterComponents('LOEPFE', [TMMTimeEdit]);
end;
{}
//------------------------------------------------------------------------------
procedure TMMTimeEdit.Change;
begin
  inherited Change;
  // Eingabecheck nur, wenn auch wirklich eine Taste gedrueckt wurde
  if mChangeValue then begin
     CheckValue;
     // Eingabefehler-Behandlung
     if mInputError then begin
        MessageBeep(MB_IconError);
        if mCursorPos >= 0 then
	   SetCursor(mCursorPos-1);
        mInputError := FALSE;
     end;
  end;
  mChangeValue :=FALSE;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Ueberprueft die Eingabezeichen pro Zeitformate und Eingabemaske.
// Die Eingabezeichen sind positionsabhaengig.
// Zeitformat: 12h oder 24h
// Eingabemaske : 'hh:mm', 'hh:mm AMPM', 'hh:mm:ss', 'hh:mm:ss AMPM'
//******************************************************************************
procedure TMMTimeEdit.CheckValue;
var xdezMinSec  : set of '0'..'5';
    xMinSec     : set of '0'..'9';
    xHour24_1   : set of '0'..'2'; // 24h Format 1. Stelle
    xHour12_1   : set of '0'..'1'; // 12h Format 1. Stelle
    xHour24_2   : set of '0'..'3'; // 24h Format 2. Stelle
    xHour12_2   : set of '0'..'2'; // 12h Format 2. Stelle
    xText       : String;
begin

   xdezMinSec := ['0','1','2','3','4','5'];
   xMinSec    := ['0','1','2','3','4','5','6','7','8','9'];
   xHour24_1  := ['0','1','2'];
   xHour12_1  := ['0','1'];
   xHour24_2  := ['0','1','2','3'];
   xHour12_2  := ['0','1','2'];

   // Genereller Teil (hh:mm)
   case mCursorPos of
	1    : begin
		  // 12h Format 1. Stelle
		  if mTimeFormat1224 = 0  then
		     if EditText[mCursorPos] in xHour12_1 then
			mInputError := FALSE;
		  // 24h Format 1. Stelle
		  if mTimeFormat1224 = 1  then
		     if EditText[mCursorPos] in xHour24_1 then
			mInputError := FALSE;
	       end;
	2    : begin
		 // 12h Format 2. Stelle
		 if mTimeFormat1224 = 0  then
		   // 1. Stelle pruefen und Wertebereich fuer 2.Stelle seten
		   case EditText[mCursorPos-1] of
		     '0'     : if EditText[mCursorPos] in xMinSec then
				  mInputError := FALSE;
		     '1','2' : if EditText[mCursorPos] in xHour12_2 then
				  mInputError := FALSE;
		   end;

		 // 24h Format 2. Stelle
		 if mTimeFormat1224 = 1  then
		    // 1. Stelle pruefen und Wertebereich fuer 2.Stelle seten
		    case EditText[mCursorPos-1] of
		      '0','1': if EditText[mCursorPos] in xMinSec then
				  mInputError := FALSE;
		      '2'    : if EditText[mCursorPos] in xHour24_2 then
				  mInputError := FALSE;
		    end;
	       end;
	 3   : begin
		 xText := EditText;
		 xText[mCursorPos] := TimeSeparator;
		 EditText := xText;
		 mInputError := FALSE;
	       end;
	       // 10 tel Min. oder Sec.
	4    : if EditText[mCursorPos] in xdezMinSec then
		  mInputError := FALSE;
	       // 100 tel Min. oder Sec.
	5    : if EditText[mCursorPos] in xMinSec then
		  mInputError := FALSE;
   end;

   // FTimeFormat = LONG (hh:mm:ss)
   if FTimeFormat = LONG then begin
      case mCursorPos of
	6    : begin
		 xText := EditText;
		 xText[mCursorPos] := TimeSeparator;
		 EditText := xText;
		 mInputError := FALSE;
	       end;
	7    : if EditText[mCursorPos] in xdezMinSec then
		  mInputError := FALSE;
	       // 100 tel Min. oder Sec.
	8    : if EditText[mCursorPos] in xMinSec then
		  mInputError := FALSE;
	9    : mInputError := FALSE;
	       // A oder P von AM oder PM String
	10   : if (UpperCase(EditText[mCursorPos]) = 'A') or
		  (UpperCase(EditText[mCursorPos]) = 'P') then
		  mInputError := FALSE;
	       // M von AM oder PM String
	11   : if (UpperCase(EditText[mCursorPos]) = 'M') then
		  mInputError := FALSE;
      end;
   end
   else begin
      // FTimeFormat = SHORT (hh:mm)
      case mCursorPos of
		// A oder P von AM oder PM String
	7    : if (UpperCase(EditText[mCursorPos]) = 'A') or
		  (UpperCase(EditText[mCursorPos]) = 'P') then
		  mInputError := FALSE;
	       // M von AM oder PM String
	8    : if (UpperCase(EditText[mCursorPos]) = 'M') then
		  mInputError := FALSE;
      end;
   end;
end;
//------------------------------------------------------------------------------
constructor TMMTimeEdit.Create(aOwner : TComponent);
begin
  inherited create (aOwner);
  fAutoLabel := TAutoLabel.Create(Self);
  AutoSelect     := FALSE;
  CharCase       := ecNormal;
  mOrgTimeFormat := LongTimeFormat;
  FTimeFormat    := LONG;
  FEditTime      := Now;
  mEditDate      := Date;
  FTimeOutput    := FEditTime;
  EditText       := TimeToStr(FEditTime);
  mCursorPos     := 0;
  mInputError    := FALSE;
  mChangeValue   := FALSE;
end;
//------------------------------------------------------------------------------
destructor TMMTimeEdit.Destroy;
begin
  fAutoLabel.Free;
  LongTimeFormat := mOrgTimeFormat;
  inherited Destroy;               
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.DoEnter;
begin
 inherited DoEnter;
 mInputError:=FALSE;
 SetNewTime;
 SelLength := Length(Text);
 SelText;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.DoExit;
begin
  inherited DoExit;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.GetLocalInformation;
var xbuffer : array[0..20] of char;
begin
  // LOCALE_ITIME        0	= AM / PM 12-hour format, 1 = 24-hour format
  // LOCALE_S1159        String for the AM designator.
  // LOCALE_S2359        String for the PM designator.
  // LOCALE_STIME        Separator
  // LOCALE_STIMEFORMAT  Time formatting strings for this locale.
  //                     The string can consist of a combination of the hour,
  //                     minute, and second format pictures defined in in
  //                     the Hour, Minute, and Second Format Pictures table
  //                     in National Language Support Constants.


  // 12 oder 24 Zeitformat (12h = 0, 24h = 1)
  fillchar(xbuffer, sizeof(xbuffer),'0');
  GetLocaleInfo(LOCALE_USER_DEFAULT,
		LOCALE_ITIME,
		xbuffer,
		sizeof(xbuffer) );
  mTimeFormat1224:= strToInt(xbuffer);
end;
//------------------------------------------------------------------------------
function TMMTimeEdit.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.KeyDown(var Key: Word; Shift: TShiftState);
var xSelStart : Integer;
begin
  GetSel(xSelStart,mCursorPos);
  if key = VK_BACK then
     mChangeValue:=FALSE;
  inherited KeyDown(Key,Shift);
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.KeyPress(var Key: Char);
var xText : STring;
    xSelStart, xSelStop : Integer;
begin
 mChangeValue := TRUE;
 case Ord(Key) of
    VK_BACK : begin // Zeichen loeschen
		xText := EditText;
		xText[mCursorPos] := '_';
		EditText := xText;
		GetSel(xSelStart,xSelStop);
                mChangeValue := FALSE;
                SelLength:=0;
	      end;
    else
       xSelStop   := SelStart;
 end;

 inherited KeyPress(Key);
 mCursorPos   := xSelStop;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.KeyUp(var Key: Word; Shift: TShiftState);
begin
  inherited KeyUp(Key,Shift);
  mInputError  := TRUE;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(aComponent, Operation);
  if (Operation = opRemove) and (aComponent = fAutoLabel.Control) then
    fAutoLabel.Control := Nil;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);
  if Assigned(fAutoLabel) then
    fAutoLabel.SetLabelPos;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetEnabled(Value: Boolean);
begin
  inherited SetEnabled(Value);
  if Assigned(fAutoLabel) then
    fAutoLabel.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetMask;
var x : Byte;
begin
  GetLocalInformation; // 12h oder 24h Format ermitteln

  // Zeitformat wurde waerend der Laufzeit veraendert. -> Format global aendern
  if LongTimeFormat <> mOrgTimeFormat then
     mOrgTimeFormat := LongTimeFormat;

  // Maske setzen (Func. TimeToStr wird immer im 'LongTimeFormat' angezeigt.
  if FTimeFormat = Long then begin
     LongTimeFormat := mOrgTimeFormat;
     if mTimeFormat1224 = 0  then
	// 12h
	EditMask := '!90' + TimeSeparator + '00' + TimeSeparator + '00> LL;2;_'
     else
	//24h
	EditMask := '!90'+ TimeSeparator + '00' + TimeSeparator + '00;1;_';
  end
  else begin
     LongTimeFormat := ShortTimeFormat;
     if mTimeFormat1224 = 0 then
	// 12h
	EditMask := '!90' + TimeSeparator + '00> LL;2;_'
     else
	//24h
	EditMask := '!90'+ TimeSeparator + '00' + ';1;_'
  end;
  // Maske immer auf hh:mm ... setzen
  x:= pos('H' , UpperCase(LongTimeFormat));
  if  LongTimeFormat[x+1] <> 'h' then LongTimeFormat:= 'h' + LongTimeFormat;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetNewTime;
begin
  Clear;
  SetMask;
  EditText := TimeToSTr(FEditTime);
  LongTimeFormat := mOrgTimeFormat;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetTimeFormat(const Value: TimeFormatEnum);
begin
  try
    FTimeFormat := Value;
  except
    MessageBeep(MB_IconError);
    exit;
  end;
  SetNewTime;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetTimeInput(const Value: TDateTime);
begin
  try
    FEditTime := Value;
  except
    MessageBeep(MB_IconError);
    exit;
  end;
  mEditDate := StrToDate(DateToStr(FEditTime));
  SetNewTime;
end;
//------------------------------------------------------------------------------
procedure TMMTimeEdit.SetVisible(Value: Boolean);
begin
  inherited Visible := Value;
  if Assigned(fAutoLabel) then
    fAutoLabel.Visible := Value;
end;
//------------------------------------------------------------------------------

//******************************************************************************
// Finalcheck
//******************************************************************************
procedure TMMTimeEdit.ValidateEdit;
begin
  // Finalcheck
  mCursorPos := 1;
  repeat
    mInputError := TRUE;
    CheckValue;
    inc(mCursorPos);
  until mInputError or (mCursorPos = length(EditText));

  // Retour zum Eingabefeld, wenn n.i.O.
  if mInputError then begin
     SetSel(0, length(EditText) );
     SetFocus;
     mInputError := TRUE;
     exit;
  end;

  LongTimeFormat := mOrgTimeFormat;

  FEditTime:=StrToTime(EditText);
  FTimeOutput:= FEditTime + mEditDate ;
end;
//------------------------------------------------------------------------------

end.
