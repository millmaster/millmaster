object FramePrintSetup: TFramePrintSetup
  Left = 0
  Top = 0
  Width = 318
  Height = 122
  TabOrder = 0
  object gbPrinterSetup: TmmGroupBox
    Left = 0
    Top = 0
    Width = 318
    Height = 122
    Align = alClient
    Caption = '(*)Drucker'
    TabOrder = 0
    CaptionSpace = True
    object mmLineLabel1: TmmLineLabel
      Left = 8
      Top = 50
      Width = 300
      Height = 17
      AutoSize = False
      Caption = '(25)Ausrichtung'
      Layout = tlCenter
      Distance = 2
    end
    object mmLineLabel2: TmmLineLabel
      Left = 8
      Top = 80
      Width = 300
      Height = 17
      AutoSize = False
      FocusControl = cbBlackWhite
      Layout = tlBottom
      Distance = 2
    end
    object cobPrinters: TmmComboBox
      Left = 8
      Top = 20
      Width = 165
      Height = 21
      Anchors = [akLeft, akRight]
      Color = clWindow
      Enabled = False
      ItemHeight = 13
      TabOrder = 0
      Visible = True
      OnChange = cobPrintersChange
      AutoLabel.LabelPosition = lpLeft
      Edit = False
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
    end
    object bPrintProperties: TmmButton
      Left = 202
      Top = 18
      Width = 105
      Height = 25
      Anchors = [akLeft, akRight]
      Caption = '(20)Eigenschaften'
      Enabled = False
      TabOrder = 1
      Visible = True
      OnClick = bPrintPropertiesClick
      AutoLabel.LabelPosition = lpLeft
    end
    object cbBlackWhite: TmmCheckBox
      Left = 8
      Top = 98
      Width = 300
      Height = 17
      Anchors = [akLeft, akRight]
      Caption = '(*)Schwarzweissdruck'
      Color = clBtnFace
      ParentColor = False
      TabOrder = 4
      Visible = True
      OnClick = cbBlackWhiteClick
      AutoLabel.Control = mmLineLabel2
      AutoLabel.Distance = 1
      AutoLabel.LabelPosition = lpTop
    end
    object rbPortrait: TmmRadioButton
      Left = 8
      Top = 70
      Width = 145
      Height = 17
      Caption = '(20)Hochformat'
      Checked = True
      TabOrder = 2
      TabStop = True
      OnClick = rbPortraitClick
    end
    object rbLandscape: TmmRadioButton
      Left = 160
      Top = 70
      Width = 145
      Height = 17
      Caption = '(20)Querformat'
      TabOrder = 3
      OnClick = rbPortraitClick
    end
  end
  object mPrintDialog: TmmPrintDialog
    Left = 240
    Top = 40
  end
end
