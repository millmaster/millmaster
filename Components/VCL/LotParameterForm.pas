(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: LotParameterForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 02.02.2000  1.00  Mg  | Datei erstellt
| 16.02.2000  1.01  Mg  | BroadcastMsg inserted
| 05.05.2000  1.02  Mg  | ThreadCnt inserted
| 29.08.2000  1.03  Mg  | No Broadcast of refresh
| 11.09.2001  1.04  Wss | AfterLanguageChange set index for help file in MMHtmlHelp
| 01.10.2001  1.05  Nue | IsOnlyTemplateSetsAvailable added.
| 18.09.2002  1.06  Nue | Umbau ADO
| 02.10.2002        LOK | Umbau ADO2
| 29.01.2003        Wss | Anpassungen an neue Packages
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 14.04.2003  1.07  Nue | Zulassen von Property-Aenderungen auch bei ftSWSInformatorSBC5
| 26.02.2004  1.08  Nue | Sinnvolle ErrorMsg bei acOK (rsMMNotRunning)
| 22.03.2005  2.00  Nue | Umbau auf XML V.5.0.
|=============================================================================*)
unit LotParameterForm;        
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, StdCtrls, Buttons, mmBitBtn, Mask, mmMaskEdit, mmLabel,
  mmColorButton,MMUGlobal, MMSecurity, IvDictio, IvMulti, IvEMulti,
  mmTranslator, Db, AssignComp, LotParameterHandlerComp,
  ActnList, mmActionList, MiscFrame, mmButton, ExtCtrls, mmGroupBox,BaseGlobal,
  YMParaDef, LoepfeGlobal,
  SettingsReader, ADODB, mmADODataSet, mmADOConnection; //Nue 9.7.01
resourceString
  rsStyle =        '(20)Artikel:';  //ivlm
  rsTemplate =     '(10)Vorlage:';  //ivlm
  rsMMNotRunning = '(*)Partieeigenschaften koennen nur bei laufendem MillMaster System angepasst werden.'; //ivlm
type
  TLotParameter = class(TDialogBottom)
    edPartieName: TmmMaskEdit;
    mmLabel1: TmmLabel;
    mmColorButton1: TmmColorButton;
    mmTranslator1: TmmTranslator;
    MMSecurityControl1: TMMSecurityControl;
    LotParameterHandler1: TLotParameterHandler;
    mmActionList1: TmmActionList;
    acOK: TAction;
    acCancel: TAction;
    Misc1: TMisc;
    mmBitBtn1: TmmBitBtn;
    mmGroupBox1: TmmGroupBox;
    mmGroupBox2: TmmGroupBox;
    mmLabel2: TmmLabel;
    mmLabel3: TmmLabel;
    mmLabel4: TmmLabel;
    lSpindleRange: TmmLabel;
    lMachGrp: TmmLabel;
    lStart: TmmLabel;
    acSecurity: TAction;
    mmLabel5: TmmLabel;
    lStyleName: TmmLabel;
    mmLabel6: TmmLabel;
    lStop: TmmLabel;
    conDefault: TmmADOConnection;
    procedure acOKExecute(Sender: TObject);
    procedure acCancelExecute(Sender: TObject);
    procedure mmColorButton1ChangeColor(Sender: TObject);
    procedure edPartieNameChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mmButton1Click(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    mMiscChanged : boolean;
    fProdGrp : TProdGrp;
    fOrgProdName : string;
    procedure OnMiscValueChanged ( aSender : TUserChanges; aChanged : boolean );
    procedure SetProdGrpID ( aProdGrpID : integer );
    function  GetProdGrpID : integer;
  public
    constructor Create ( aOwner : TComponent ); override;
    property ProdGrpID : integer read GetProdGrpID write SetProdGrpID;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  MMMessages, u_dmAssignment;
//------------------------------------------------------------------------------
  constructor TLotParameter.Create ( aOwner : TComponent );
  begin
    inherited Create ( aOwner );
    mmColorButton1.ColorArray := cMMProdGroupColors;
    acOK.Enabled              := MMSecurityControl1.CanEnabled ( acOK );

    Misc1.YarnCntUnit    := TMMSettingsReader.Instance.Value[cYarnCntUnit];
    Misc1.OnValueChanged := OnMiscValueChanged;
    mMiscChanged         := false;

    fProdGrp := TProdGrp.Create ( dmAssignment.dseQuery );

    //Nue 9.7.01
//    if (NOT(TMMSettingsReader.Instance.IsPackageLabMaster) and NOT (TMMSettingsReader.Instance.IsPackageDispoMaster) or
//         (TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable)) then
    if (NOT TMMSettingsReader.Instance.IsComponentStyle) or TMMSettingsReader.Instance.IsOnlyTemplateSetsAvailable then
        edPartieName.Enabled := True
    else
      case TPartieIndex(TMMSettingsReader.Instance.Value [ cDefaultPartieIndex ]) of
      piArtNameId,piArtNameDate: begin
          edPartieName.Enabled := False;
        end;
      piArtName,piUserInput: begin
          edPartieName.Enabled := True;
        end;
      else (*piNoneInput*)
          edPartieName.Enabled := True;
      end;

//Nue:14.1.02
//    if NOT((TMMSettingsReader.Instance.IsPackageLabMaster) or (TMMSettingsReader.Instance.IsPackageDispoMaster)) then begin
//      mmLabel5.Enabled := False;
//      lStyleName.Enabled := False;
//    end;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.SetProdGrpID ( aProdGrpID : integer );
  begin
    try
      fProdGrp.ProdGrpID := aProdGrpID;
      LotParameterHandler1.AssMachine := (fProdGrp as TMachine);
//      INavChange ( Misc1 ).UpdateComponent ( fProdGrp, nil, nil {???Nue} );
      lSpindleRange.Caption := IntToStr ( fProdGrp.SpindleFirst ) + ' - ' + IntToStr ( fProdGrp.SpindleLast );
      lMachGrp.Caption := IntToStr ( fProdgrp.GrpNr+1 );
      lStart.Caption := DateTimeToStr ( fProdGrp.startTime );
      if fProdGrp.startTime = fProdGrp.stopTime then begin //Noch nicht gestoppt
        lStop.Caption := '-----';
      end
      else begin
        lStop.Caption := DateTimeToStr ( fProdGrp.StopTime);
      end;  

      INavChange ( Misc1 ).UpdateComponent ( fProdGrp, nil, nil {???Nue} );

      if fProdGrp.StyleID<>cDefaultStyleID then begin
        mmLabel5.Caption := cStyle;
        lStyleName.Caption := fProdGrp.StyleName;
      end
      else begin
        mmLabel5.Caption := cTemplate;
        lStyleName.Caption := fProdGrp.YMSetName;
      end;

      fOrgProdName := fProdGrp.Name;
      edPartieName.Text     := fProdGrp.Name;
      mmColorButton1.Color  := fProdGrp.Color;
    except
      on e:Exception do begin
        SystemErrorMsg_ ( 'TLotParameter.SetProdGrpID failed. ' + e.Message );
      end;
    end;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.OnMiscValueChanged ( aSender : TUserChanges; aChanged : boolean );
  begin
    mMiscChanged := true;
  end;
//------------------------------------------------------------------------------
  function TLotParameter.GetProdGrpID : integer;
  begin
    if Assigned ( fProdGrp ) then
      Result := fProdGrp.ProdGrpID
    else
      Result := 0;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.acOKExecute(Sender: TObject);
  var
    xOldCursor: TCursor;
  begin
    try
      if mMiscChanged then begin
        if INavChange ( Misc1 ).CheckUserValue then begin
//Neu
          LotParameterHandler1.Slip    := Misc1.Slip;
          LotParameterHandler1.YarnCnt := Misc1.YarnCnt;
          LotParameterHandler1.ThreadCnt := Misc1.ThreadCnt;

(* Alt         case fProdGrp.FrontType of
            ftSWSInformatorSBC5 : begin
                // Take nothing
              end;
          else
            LotParameterHandler1.Slip    := Misc1.Slip;
            LotParameterHandler1.YarnCnt := Misc1.YarnCnt;
            LotParameterHandler1.ThreadCnt := Misc1.ThreadCnt;
          end;
*)
        end;
        // Send new params to System
        LotParameterHandler1.LotParamAssign ( fProdGrp.ProdGrpID );
        xOldCursor := Screen.Cursor;
        try
          Screen.Cursor := crHourGlass;
          Sleep(5000); //Zuerst muss per StorageHandler die DB nachgefuehrt werden !!!!!!!!!!!!!!!!!!!!!!!!!
        finally
          Screen.Cursor := xOldCursor;
        end;
      end;


{
      if fOrgProdName<>LotParameterHandler1.ProdName then begin
      //@@@@@@@Update List
      end;
{}

    except
      on e:Exception do begin
//        SystemErrorMsg_ ( 'TLotParameter.acOKExecute failed. ' + e.Message );
        SystemErrorMsg_ (rsMMNotRunning);
      end;
    end;
    Close;
    ModalResult := mrOK;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.acCancelExecute(Sender: TObject);
  begin
    Close;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.mmColorButton1ChangeColor(Sender: TObject);
  begin
    LotParameterHandler1.Color := mmColorButton1.Color;
    mMiscChanged := True;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.edPartieNameChange(Sender: TObject);
  begin
    LotParameterHandler1.ProdName := Trim(edPartieName.Text);
    mMiscChanged := True;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.FormClose(Sender: TObject;
  var Action: TCloseAction);
  begin
    // wss, 9.1.2002: this form was called from Floor it has to be closed by itself
    if OLEMode then
      Action := caFree;
    //MMBroadCastMessage ( RegisterWindowMessage ( cMsgAppl ), cMMRefresh );
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.mmButton1Click(Sender: TObject);
  begin
    MMSecurityControl1.Configure;
  end;
//------------------------------------------------------------------------------
  procedure TLotParameter.acSecurityExecute(Sender: TObject);
  begin
    MMSecurityControl1.Configure;
  end;
//------------------------------------------------------------------------------
procedure TLotParameter.FormShow(Sender: TObject);
begin
  mMiscChanged := False;
end;

end.
