(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regYMClearer.pas
| Projectpart...: MillMaster
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages from different units
| Info..........: -
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.06.2005  1.00  Wss | TPExtEditBox und TRepetitionEditBox hinzugefügt
| 10.01.2006  1.01  Nue | TVCVEditBox hinzugefügt
|=========================================================================================*)
unit regYMClearer;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  KeySpinEdit,
  QualityMatrix,
  YMBasicProdParaBox,
  YMSettingsGUI,
  YMParaEditBox,
  //YMParaDBaccessForGUI,  //will no more use SDO
  YMMachConfigBox,
  DialogConfigCode;

procedure MMRegister;
begin
  // Unit QualityMatrix
  RegisterComponents('YMClearer', [TQualityMatrix]);
  // Unit YMBasicProdParaBox
  RegisterComponents('YMClearer', [TGBBasicProdPara]);
  // Unit YMSettingsGUI
  RegisterComponents('YMClearer', [TGUIYMSettings]);
  // Unit KeySpinEdit
  RegisterComponents('YMClearer', [TKeySpinEdit]);
  // Unit YMParaEditBox
  RegisterComponents('YMClearer', [TChannelEditBox,
                                   TSpliceEditBox,
                                   TYarnCountEditBox,
                                   TFaultClusterEditBox,
                                   TSFIEditBox,
                                   TVCVEditBox,
                                   TFFClusterEditBox,
                                   TPEditBox,
                                   TPExtEditBox,
                                   TRepetitionEditBox,
                                   THeadClassEditBox]);
  // Unit YMParaDBaccessForGUI
//NUE1  RegisterComponents('YMClearer', [TYMSettingsForGUI]);
  // Unit YMMachConfigBox
  RegisterComponents('YMClearer', [TGBMachConfigBox]);
  // Unit DialogConfigCode
  RegisterComponents('YMClearer', [TGBConfigCode]);
end;

end.
