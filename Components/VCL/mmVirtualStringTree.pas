(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: mmVirtualStringTree.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows
| Target.system.: Windows
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.03.2003  1.00  Lok | Initial Release
| 02.06.2003        Lok | Methode FindString() hinzugef�gt
| 11.09.2003        Lok | Procedure ForceFocusChanged hinzugef�gt
| 06.01.2004        Lok | Methode 'FilterString' eingef�gt.
|                           Wird ein leerer String �bergeben, dann werden alle Knoten wieder
|                           eingeblendet.
|=========================================================================================*)
unit mmVirtualStringTree;

interface

uses
  VirtualTrees, Controls, classes, sysutils, windows, messages;

type
  TVSTFindOption = (
    foPartial,        // L�sst auch partielle �bereinstimmung zu
    foFirstNode,      // Sucht vom ersten Knoten an
    foDeliveredNode,  // Sucht vom �bergebenen Knoten an (Der Knoten muss als Argument �bergeben werden)
    foFromBegin,      // Die �bereinstimmung muss vom ersten Zeichen her sein
    foCase,           // Die Gross- Klein Schreibung wird ber�cksichtigt
    foSelect,         // Selektiert den gefunden Knoten (die bisherige selektion wird gel�scht)
    foLevel           // Sucht nur im entsprecehnden Level (-1 = Level des aktiven Knotens)
  );// TVSTFindOption
  TVSTFindOptions = set of TVSTFindOption;
const
  cDefaultFindOptions = [foPartial, foFromBegin, foSelect];

type
  TmmVirtualStringTree = class(TVirtualStringTree)
  public
    // Findet einen Knoten mit dem gew�nschten Text im Tree.
    function FindString(aText: string; aCol: integer; aOptions: TVSTFindOptions = cDefaultFindOptions; aLevel: integer = -1; aNode: PVirtualNode = nil): PVirtualNode;
    // Erzwingt ein OnFocusChanged Ereignis (OnFocusChanging wird nicht aufgerufen
    procedure ForceFocusChanged(aNode:PVirtualNode);virtual;
    // Blendet all Knoten aus, bei denen aText nicht im Knotentext enthalten ist.
    procedure FilterString(aText: string; aCol: integer);
  end;

  TPropDataType = (dtNone,
                   dtNumber,
                   dtString,
                   dtEnum
                   ); // TPropDataType

  (*---------------------------------------------------------
    Erm�glicht das Editieren (mmEdit) und ausw�hlen (mmComboBox) von Zellen
  ----------------------------------------------------------*)
  TPropertyEditLink = class(TInterfacedObject, IVTEditLink)
  private
    FControlText: string;
    FUserSelection: variant;
    mColumn: Integer;
    mControlType: TPropDataType;
  protected
    mLink: TWinControl;
    mNode: PVirtualNode;
//    mParent: TForm;
    mTree: TVirtualStringTree;
  public
    destructor Destroy; override;
    function BeginEdit: Boolean; stdcall;
    function CancelEdit: Boolean; stdcall;
    procedure ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
        virtual;
    constructor Create(aControlType: TPropDataType; aControlText: string);
        reintroduce; overload; virtual;
    constructor Create(aControlType: TPropDataType); reintroduce; overload; virtual;
    function EndEdit: Boolean; virtual; stdcall;
    function GetBounds: TRect; stdcall;
    function PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column:
        TColumnIndex): Boolean; virtual; stdcall;
    procedure ProcessMessage(var Message: TMessage); stdcall;
    procedure SetBounds(R: TRect); stdcall;
    property ControlText: string write FControlText;
    property UserSelection: variant read FUserSelection write FUserSelection;
  end;


implementation
uses
  mmMBCS, stdctrls, mmEdit;
{ TmmVirtualStringTree }

(*---------------------------------------------------------
  2.6.2003 --> LOK
  Findet einen Knoten mit dem gew�nschten Text im Tree.
  Die Suche kann durch das Setzen verschiedener Optionen beeinflusst werden.
----------------------------------------------------------*)
function TmmVirtualStringTree.FindString(aText: string; aCol: integer; aOptions: TVSTFindOptions = cDefaultFindOptions; aLevel: integer = -1; aNode: PVirtualNode = nil): PVirtualNode;
var
  xNode: PVirtualNode;
  xNodeTextUnicode: WideString;
begin
  // Initiialisieren
  result := nil;

  // Ausgangspunkt f�r die Suche festlegen
  if foFirstNode in aOptions then
    xNode := GetFirst
  else
    xNode := FocusedNode;

  if foDeliveredNode in aOptions then
    xNode := aNode;

  // Bestimmen des ersten Knotens der dem Level entspricht
  if foLevel in aOptions then begin
    // Aktueller Knoten ...
    if aLevel = -1 then
      aLevel := GetNodeLevel(xNode);

    // ... sonst weiter bis der erste Knoten gefunden ist der dem entsprechenden Level entspricht
    while (assigned(xNode)) do begin
      if GetNodeLevel(xNode)  = cardinal(aLevel) then
        break;
      if GetNodeLevel(xNode) > cardinal(aLevel) then
        xNode := GetVisibleParent(xNode);

      if GetNodeLevel(xNode) < cardinal(aLevel) then
        xNode := GetNext(xNode);
    end;// while (assigned(xNode)) do begin
  end;// if foLevel in aOptions then begin

  // Solange iterieren bis ein Knoten gefunden wird (oder bis zum Ende)
  while (assigned(xNode)) and (not(assigned(result))) do begin
    // Evt. m�ssen die Knoten, bevor sie angesprochen werden initialisiert werden
    if not (vsInitialized in xNode.States) then
      InitNode(xNode);

    xNodeTextUnicode := '';
    // Von jedem Knoten den angezeigten Text holen
    DoGetText(xNode, aCol, ttNormal, xNodeTextUnicode);

    // Text mit der Vorgebe vergleichen (je nach gesetzten Optionen mit versch. Methoden)
    if foPartial in aOptions then begin
      if foCase in aOptions then begin
        // Mit Gr�ssenvergleich
        if foFromBegin in aOptions then begin
          if AnsiPos(aText, xNodeTextUnicode) = 1 then
            result := xNode;
        end else begin
          if AnsiPos(aText, xNodeTextUnicode) > 0 then
            result := xNode;
        end;// if foFromBegin in aOptions then begin
      end else begin
        // Ohne Gr�ssenvergleich
        if foFromBegin in aOptions then begin
          if AnsiPos(Uppercase(aText), UpperCase(xNodeTextUnicode)) = 1 then
            result := xNode;
        end else begin
          if AnsiPos(Uppercase(aText), UpperCase(xNodeTextUnicode)) > 0 then
            result := xNode;
        end;// if foFromBegin in aOptions then begin
      end;
    end else begin
      if foCase in aOptions then begin
        // Mit Gr�ssenvergleich
        if AnsiSameStr(aText, xNodeTextUnicode) then
          result := xNode;
      end else begin
        // Ohne Gr�ssenvergleich
        if AnsiSameText(aText, xNodeTextUnicode) then
          result := xNode;
      end;
    end;// if foPartial in aOptions then begin
    if foLevel in aOptions then
      xNode := GetNextSibling(xNode)
    else
      xNode := GetNext(xNode);
  end;// while (assigned(xNode)) and (not(assigned(result))) do begin

  // evt. gefundenen Text selektieren
  if foSelect in aOptions then begin
    ClearSelection;
    if assigned(result)then begin
      Selected[result] := true;
      ScrollIntoView(result, true);
    end;// if assigned(result)then begin
  end;// if foSelect in aOptions then begin
end;// function TmmVirtualStringTree.FindString(aText: string; aOptions: TVSTFindOptions = cDefaultFindOptions): PVirtualNode;

(*---------------------------------------------------------
  Erzwingt ein OnFocusChanged Ereignis (OnFocusChanging wird nicht aufgerufen
----------------------------------------------------------*)
procedure TmmVirtualStringTree.ForceFocusChanged(aNode: PVirtualNode);
begin
  DoFocusChange(aNode, FocusedColumn);
end;// procedure TmmVirtualStringTree.ForceFocusChange(aNode: PVirtualNode);

(*---------------------------------------------------------
  6.1.2004 --> LOK

  Blendet all Knoten aus, bei denen aText nicht im Knotentext enthalten ist.
  Wird ein leerer String �bergeben, dann werden alle Knoten wieder
  eingeblendet.
----------------------------------------------------------*)
procedure TmmVirtualStringTree.FilterString(aText: string; aCol: integer);
var
  xNode: PVirtualNode;
  xNodeTextUnicode: WideString;
begin
  xNode := GetFirstChild(nil);

  // Durch alle Knoten iterieren
  while (assigned(xNode)) do begin
    // Bei leerem Text werden alle Knoten wieder eingeblendet
    if (aText > '') then begin
      // Evt. m�ssen die Knoten, bevor sie angesprochen werden initialisiert werden
      if not (vsInitialized in xNode.States) then
        InitNode(xNode);

      xNodeTextUnicode := '';
      // Von jedem Knoten den angezeigten Text holen
      DoGetText(xNode, aCol, ttNormal, xNodeTextUnicode);
      // Knoten nur sichtbar, wenn der �bergebene Text irgendwo im Spaltentext vorkommt
      IsVisible[xNode] := (AnsiPos(Uppercase(aText), UpperCase(xNodeTextUnicode)) > 0);
    end else begin
      // Bei leerem Text werden alle Knoten wierder eingeblendet
      IsVisible[xNode] := true;
    end;// if (aText > '') then begin
    xNode := GetNext(xNode);
  end;// while (assigned(xNode)) do begin
end;// procedure TmmVirtualStringTree.FilterString(aText: string; aCol: integer);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TPropertyEditLink.Create(aControlType: TPropDataType; aControlText:
    string);
begin
//  mParent := aParent;
  mControlType := aControlType;
  FControlText := aControlText;
end;// procedure TPropertyEditLink.Create(aInputForm: TfrmRecordInput);

(*---------------------------------------------------------
  Konstruktor
----------------------------------------------------------*)
constructor TPropertyEditLink.Create(aControlType: TPropDataType);
begin
  create(aControlType, '');
end;// procedure TPropertyEditLink.Create(aInputForm: TfrmRecordInput);

(*---------------------------------------------------------
  Destruktor
----------------------------------------------------------*)
destructor TPropertyEditLink.Destroy;
begin
  mLink.Free;
  inherited;
end;// destructor TPropertyEditLink.Destroy;

(*---------------------------------------------------------
  Das Feld soll in der entsprechenden Spalte angezeigt werden
----------------------------------------------------------*)
function TPropertyEditLink.BeginEdit: Boolean;
begin
  Result := True;
  mLink.Show;
  mLink.SetFocus;
end;// function TPropertyEditLink.BeginEdit: Boolean;

(*---------------------------------------------------------
  Der Editor soll mit der Escape Taste verlassen werden. Der Wert wird nicht �bernommen
----------------------------------------------------------*)
function TPropertyEditLink.CancelEdit: Boolean;
begin
  Result := True;
  mLink.Hide;
end;// function TPropertyEditLink.CancelEdit: Boolean;

(*---------------------------------------------------------
  OnKeyDown Handler f�r das Control
----------------------------------------------------------*)
procedure TPropertyEditLink.ControlKeyDown(Sender: TObject; var Key: Word;
    Shift: TShiftState);
begin
  case Key of
    // ESCAPE verl�ssst das Control, ohne den Wert zu �bernehmen
    VK_ESCAPE: begin
      mTree.CancelEditNode;
      mTree.SetFocus;
    end;// VK_ESCAPE: begin

    // �bernimmt den Wert und selektiert den n�chsten Knoten
    VK_DOWN : begin
      mTree.EndEditNode;
      mTree.SetFocus;
      mTree.FocusedNode := mTree.GetNext(mTree.FocusedNode);
    end;// VK_DOWN : begin

    // �bernimmt den Wert und selektiert den vorderen Knoten
    VK_UP: begin
      mTree.EndEditNode;
      mTree.SetFocus;
      mTree.FocusedNode := mTree.GetPrevious(mTree.FocusedNode);
    end;// VK_UP: begin

    // Ubernimmt den Wert
    VK_RETURN: begin
      mTree.EndEditNode;
      mTree.SetFocus;
    end;// VK_ENTER, VK_RETURN: begin
  end;// case Key of

  if Key in [VK_DOWN, VK_UP] then begin
    mTree.ClearSelection;
    mTree.Selected[mTree.FocusedNode] := true;
    Key := 0;
  end;// if Key in [VK_DOWN, VK_UP] then begin
end;// procedure TPropertyEditLink.ControlKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

(*---------------------------------------------------------
  Wird aufgerufen, wenn ein Knoten angeklickt wurde
----------------------------------------------------------*)
function TPropertyEditLink.EndEdit: Boolean;
var
  P: TPoint;
  Dummy: Integer;
begin
  // �berpr�fen, ob auf den editierten Knoten geklickt wurde
  GetCursorPos(P);
  P := mTree.ScreenToClient(P);
  Result := mTree.GetNodeAt(P.X, P.Y, True, Dummy) <> mNode;

  if Result then begin
    // Wert ins DOM eintragen
    if mLink is TCustomEdit then
      UserSelection := TCustomEdit(mLink).Text
    else if mLink is TComboBox then
      UserSelection := TComboBox(mLink).ItemIndex
    else
      raise Exception.Create('TPropertyEditLink.EndEdit: Unbekanntes Control');
    mLink.Hide;
  end;// if Result then begin
end;// function TPropertyEditLink.EndEdit: Boolean;

(*---------------------------------------------------------
  Wird aufgerufen um den Umfang des Controls zu erfragen
----------------------------------------------------------*)
function TPropertyEditLink.GetBounds: TRect;
begin
  Result := mLink.BoundsRect;
end;// function TPropertyEditLink.GetBounds: TRect;

(*---------------------------------------------------------
  Wird aufgerufen, bevor ein Control angezeigt werden soll
----------------------------------------------------------*)
function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node:
    PVirtualNode; Column: TColumnIndex): Boolean;
begin
  // Die internen Paramerter sichern
  mTree := nil;
  if Tree is TVirtualStringTree then
    mTree := TVirtualStringTree(Tree);
  mNode := Node;
  mColumn := Column;

  // ein evt. vorheriges Editfeld freigeben
  FreeAndNil(mLink);

  case mControlType of
    dtNumber, dtString: begin
      // Neues Editfeld erzeugen
      mLink := TMMEdit.Create(nil);
      with mLink as TMMEdit do begin
        Visible := False;
        Parent := Tree;
        Text := FControlText;
        NumMode := (mControlType = dtNumber);
        OnKeyDown := ControlKeyDown;
      end;// with mEdit as TMMEdit do begin
    end;// dtNumber, dtString: begin

    dtEnum: begin
      mLink := TCombobox.Create(nil);
      with mLink as TCombobox do begin
        Visible := False;
        Parent := Tree;
        Style := csDropDownList;
        Items.Text := FControlText;
      end;// with mLink as TmmCombobox do begin
    end;
  end;// case xDataType of
  // Nur weiter, wenn das Tree g�ltig ist
  Result := assigned(mTree);
end;// function TPropertyEditLink.PrepareEdit(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex): Boolean;

(*---------------------------------------------------------
  Wird vom Interface ben�tigt
----------------------------------------------------------*)
procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);
begin
  mLink.WindowProc(Message);
end;// procedure TPropertyEditLink.ProcessMessage(var Message: TMessage);

(*---------------------------------------------------------
  Wird aufgerufen um die Gr�sse des Controls an die Spalte anzupassen
----------------------------------------------------------*)
procedure TPropertyEditLink.SetBounds(R: TRect);
var
  Dummy: Integer;
begin
  // Since we don't want to activate grid extensions in the tree (this would influence how the selection is drawn)
  // we have to set the edit's width explicitly to the width of the column.
  mTree.Header.Columns.GetColumnBounds(mColumn, Dummy, R.Right);
  mLink.BoundsRect := R;
end;// procedure TPropertyEditLink.SetBounds(R: TRect);



end.

