(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: MiscFrame.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 13.12.1999  1.00  Mg  | Datei erstellt
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 18.01.2001  1.11 NueMg| Several changes cause of implementing style in SettingsNav
| 28.02.2002  1.12  Nue | Changings because of yarnCnt-Handling.
| 19.03.2002  1.13  Nue | No longer show slip from machine to assignment GUI.
| 04.04.2002  1.14  Nue | Line enabled: YarnCntUnit := aProdGrp.YarnCntUnit;  //Nue:4.4.02 Before this line was disabled
| 18.09.2002  1.15  Nue | Umbau ADO
| 07.11.2002        LOK | In TMisc.UpdateComponent wird visible auf true gesetzt. F�r Maschinen
|                       | mit Fix Spindle Range kann visible = false sein, da die Daten zuerst von der Maschine
|                       | geladen werden m�ssen und w�hrend dieser Zeit die Settings nicht angezeigt werden d�rfen.
|                       | (siehe 'TAssign.SetSpdRange()')
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
|=============================================================================*)
unit MiscFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, mmPanel, StdCtrls, Mask, mmMaskEdit, mmLabel, AssignComp, MMUGlobal,
  SettingsReader, ComCtrls, mmUpDown, IvDictio, BaseGlobal, LoepfeGlobal, YMParaDef,
  XMLDef, mmEdit;
const
  cFieldYarnCntNotSet =
    '(*)Bitte eine Garn Nummer angeben.'; //ivlm

  cFieldThreadCntNotSet =
    '(*)Bitte eine Fadenzahl angeben.'; //ivlm

  cFieldSlipNotSet =
    '(*)Bitte einen Schlupf angeben.'; //ivlm
type
  TMisc = class(TFrame, INavChange)
    mmPanel1: TmmPanel;
    mmLabel1: TmmLabel;
    mmLabel2: TmmLabel;
    lbYarnCountUnit: TmmLabel;
    mmLabel3: TmmLabel;
    mmUpDown1: TmmUpDown;
    edYarnCnt: TmmEdit;
    edSlip: TmmEdit;
    edThreadCnt: TmmEdit;
    procedure edChange(Sender: TObject);
    procedure edYarnCntKeyPress(Sender: TObject; var Key: Char);
    procedure edSlipKeyPress(Sender: TObject; var Key: Char);
    procedure edExit(Sender: TObject);
    procedure edThreadCntKeyPress(Sender: TObject; var Key: Char);
  private
    fYarnCntUnit: TYarnUnit;
    fSpdRangeList: TSpindleRangeList;
    fOnValueChanged: TOnValueChange;
    fOnFocusChanged: TOnFocusChange;
    fFieldsInLine: boolean;
    fRunGrpData: boolean;
    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    procedure SetYarnCnt(const aYarnCnt: Extended);
    procedure SetYarnCntUnit(aYarnCntUnit: TYarnUnit);
    procedure SetSlip(const aSlip: Extended);
    procedure SetFieldsInLine(aInLine: boolean);
    procedure SetThreadCnt(const aCnt: Integer);
    function GetYarnCnt: Extended;
    function GetSlip: Extended;
    function GetThreadCnt: Integer;
    procedure Check;
    function toFloat(aStr: string): Single;
  protected
//wss    procedure UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    function CheckUserValue: boolean;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    property YarnCnt: Extended read GetYarnCnt write SetYarnCnt;
    property YarnCntUnit: TYarnUnit read fYarnCntUnit write SetYarnCntUnit;
    property Slip: Extended read GetSlip write SetSlip;
    property ThreadCnt: Integer read GetThreadCnt write SetThreadCnt;
    property OnValueChanged: TOnValueChange read fOnValueChanged write fOnValueChanged; // only used in LotParameterForn
  published
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
    property FieldsInLine: boolean read fFieldsInLine write SetFieldsInLine;
    property RunGrpData: boolean read fRunGrpData write fRunGrpData;
  end;
//procedure Register;
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;
{$R *.DFM}
//------------------------------------------------------------------------------
{
  procedure Register;
  begin
    RegisterComponents('Assignments', [TMisc]);
  end;
{}
//------------------------------------------------------------------------------
constructor TMisc.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  edYarnCnt.MinValue   := cYarnCountMin;
  edYarnCnt.MaxValue   := cYarnCountMax;
  edSlip.MinValue      := cMinSlip;
  edSlip.MaxValue      := cMaxSlip;
  edThreadCnt.MinValue := 1;
  edThreadCnt.MaxValue := 9;

  fOnValueChanged := nil;
  fOnFocusChanged := nil;
  fFieldsInLine := False;
  try
    fYarnCntUnit := TMMSettingsReader.Instance.Value[cYarnCntUnit];
    lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit]; //Neu 4.3.2002 Nue
//      fYarnCntUnit := TYarnUnit ( StrToInt ( TMMSettingsReader.Instance.Value[cYarnCntUnit]) );
      //fYarnCntUnit := yuNm;
    edSlip.Text := Format('%1.3f', [1.000]);
  except
    on e: Exception do begin
      SystemErrorMsg_('TMisc.Create failed. ' + e.message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMisc.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then begin
    if Assigned(SpdRangeList) and (AComponent = SpdRangeList) then
      fSpdRangeList := nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TMisc.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if Assigned(fSpdRangeList) then
    fSpdRangeList.add(self);
end;
//------------------------------------------------------------------------------
procedure TMisc.SetYarnCnt(const aYarnCnt: Extended);
begin
  edYarnCnt.AsFloat := aYarnCnt;
//  edYarnCnt.Text := Format('%4.1f', [aYarnCnt]);
end;
//------------------------------------------------------------------------------
procedure TMisc.SetYarnCntUnit(aYarnCntUnit: TYarnUnit);
begin
  fYarnCntUnit := aYarnCntUnit;
  lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit];
end;
//------------------------------------------------------------------------------
procedure TMisc.SetSlip(const aSlip: Extended);
begin
  edSlip.AsFloat := aSlip;
//  edSlip.Text := Format('%1.3f', [aSlip]);
end;
//------------------------------------------------------------------------------
procedure TMisc.SetThreadCnt(const aCnt: Integer);
begin
  edThreadCnt.AsInteger := aCnt;
//  edThreadCnt.Text := IntToStr(aCnt);
end;
//------------------------------------------------------------------------------
procedure TMisc.SetFieldsInLine(aInLine: boolean);
begin
  if aInLine <> fFieldsInLine then begin
    fFieldsInLine := aInLine;
    if fFieldsInLine then begin
      self.Width       := 320;
      self.Height      := 73;
      edYarnCnt.Left   := 80;
      edThreadCnt.Left := 80;
      edThreadCnt.Top  := 40;
      edSlip.Left      := 270;
      edSlip.Top       := edYarnCnt.Top;
    end
    else begin
      self.Width       := 153;
      self.Height      := 94;
      edThreadCnt.Top  := 32;
      edSlip.Left      := 88;
      edSlip.Top       := 64;
      edThreadCnt.Left := 88;
      edYarnCnt.Left   := 88;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMisc.GetYarnCnt: Extended;
begin
//  Check;
  Result := edYarnCnt.AsFloat;
////Old    Result := YarnCountConvert(fYarnCntUnit, yuNm, StrToFloat(edYarnCnt.Text));
//  Result := StrToFloat(edYarnCnt.Text);
end;
//------------------------------------------------------------------------------
function TMisc.GetSlip: Extended;
begin
//  Check;
  Result := edSlip.AsFloat;
//  Result := StrToFloat(edSlip.Text);
end;
//------------------------------------------------------------------------------
function TMisc.GetThreadCnt: Integer;
begin
  Result := edThreadCnt.AsInteger;
//  Result := StrToInt(edThreadCnt.text);
end;
//------------------------------------------------------------------------------
procedure TMisc.Check;
begin
{wss
  if edYarnCnt.Text = '' then
    edYarnCnt.Text := '120';
  if edSlip.Text = '' then
    edSlip.Text := '1.000';
  if toFloat(edSlip.Text) > cMaxSlip then
    edSlip.Text := FloatToStr(cMaxSlip);
  if toFloat(edSlip.Text) < cMinSlip then
    edSlip.Text := FloatToStr(cMinSlip);
  if toFloat(edYarnCnt.Text) < 2 then
    edYarnCnt.Text := '2';
  if toFloat(edYarnCnt.Text) > 3200 then
    edYarnCnt.Text := '3200';
  if StrToInt(edThreadCnt.Text) > 9 then
    edThreadCnt.Text := IntToStr(9);
  if StrToInt(edThreadCnt.Text) < 1 then
    edThreadCnt.Text := IntToStr(1);
{}
end;
//------------------------------------------------------------------------------
function TMisc.toFloat(aStr: string): Single;
begin
  try
    Result := StrToFloat(aStr);
  except
    Result := 1.0;
  end;
end;
//------------------------------------------------------------------------------
procedure TMisc.UpdateComponent(aSpdRange: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//procedure TMisc.UpdateComponent(aSpdRange: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
  //.............................................................
//  procedure InitByOrderPosition(aOrderPosition: TOrderPosition);
//  begin
////      YarnCntUnit := aOrderPosition.YarnCntUnit;
//    YarnCnt := (aOrderPosition as TOrderPosition).YarnCnt; //FloatToStr ( aOrderPosition.YarnCnt );
////      YarnCnt     := YarnCountConvert(aOrderPosition.YarnCntUnit, fYarnCntUnit, (aOrderPosition as TOrderPosition).YarnCnt);//FloatToStr ( aOrderPosition.YarnCnt );
//
//      //Slip added: Nue 21.05.01
//    if aOrderPosition.Slip <> '' then
//      edSlip.Text := aOrderPosition.Slip
//    else if aSpdRange.Slip <> 0 then
//      edSlip.Text := Format('%1.3f', [aSpdRange.Slip / cSlipFactor])
//    else
//      edSlip.Text := Format('%1.3f', [cSlipFactor]);
//
//  end;
  //.............................................................
  procedure InitByProdGrp(aProdGrp: TProdGrp);
  begin
    YarnCntUnit := aProdGrp.YarnCntUnit; //Nue:4.4.02 Before this line was disabled
    YarnCnt     := TProdGrp(aSpdRange).YarnCnt; //FloatToStr ( aProdGrp.YarnCnt );
//wss    YarnCnt := (aSpdRange as TProdGrp).YarnCnt; //FloatToStr ( aProdGrp.YarnCnt );
//      YarnCnt     := YarnCountConvert(aProdGrp.YarnCntUnit, fYarnCntUnit, (aSpdRange as TProdGrp).YarnCnt);

    ThreadCnt := TProdGrp(aSpdRange).getThreadCnt;
//wss    edThreadCnt.Text := Format('%d', [(aSpdRange as TProdGrp).getThreadCnt]);
    edSlip.Text := (aSpdRange as TProdGrp).Slip;
  end;
  //.............................................................
  procedure InitByMiscInfo(aMiscInfo: IMiscInfo);
  begin
//Nue 25.06.01 TTemplate
//      YarnCntUnit := aMiscInfo.getYarnCntUnit;
    if aMiscInfo.getYarnCnt < 0.001 then
      edYarnCnt.Text := ''
    else
      YarnCnt := aMiscInfo.getYarnCnt;
//          YarnCnt := YarnCountConvert(aMiscInfo.getYarnCntUnit, fYarnCntUnit, aMiscInfo.getYarnCnt);

    edThreadCnt.Text := Format('%d', [aMiscInfo.getThreadCnt]);
      //Slip added: Nue 21.05.01
    if aMiscInfo.getSlip = '@' then //Nue 25.06.01 TPreselect
      edSlip.Text := ''
    else if aMiscInfo.getSlip <> '' then
      edSlip.Text := aMiscInfo.getSlip
//Alt bis 19.3.02 Nue: No longer show slip from machine to assignment GUI
//      else if SpdRangeList.AssMachine.Slip<>0 then    //@@Nue darf so zugegriffen werden?
//        edSlip.Text := Format ( '%1.3f',[SpdRangeList.AssMachine.Slip/cSlipFactor] )
    else
      edSlip.Text := Format('%1.3f', [1.000]); //@@Nue 21.06.01 '' ausgeben wenn TPreselect (codieren!!)

  end;
  //.............................................................
  procedure InitByNil;
  begin
    edYarnCnt.Text := '';
    edThreadCnt.Text := '1';
    edSlip.Text := Format('%1.3f', [1.000]);
//      fYarnCntUnit := TYarnUnit ( StrToInt ( TMMSettingsReader.Instance.Value[cYarnCntUnit]) );
    lbYarnCountUnit.Caption := cYarnUnitsStr[fYarnCntUnit];
  end;
  //.............................................................
begin
  try
    try
  //      InitByNil;
      edYarnCnt.Enabled := True;
      edSlip.Enabled := True;
      edThreadCnt.Enabled := False;

      if Assigned(aSpdRange) then begin
        if aSpdRange.MachineType = mtAC338 then begin
          if (aSpdRange is TProdGrp) then
            InitByProdGrp(aSpdRange as TProdGrp)
          else
            InitByNil;
        end
        else begin
          edThreadCnt.Enabled := False;
          if (aSpdRange is TProdGrp) then
            InitByProdGrp(aSpdRange as TProdGrp)
        end;
//wss        case aSpdRange.FrontType of
//          ftInf68K, ftInfPPC: begin
//  //Nue: 23.01.02 Wozu???            edYarnCnt.Enabled := not fRunGrpData;
//  //Nue: 23.01.02 Wozu???            edThreadCnt.Enabled := not fRunGrpData;
//              //edSlip.Enabled := false;
//              if (aSpdRange is TProdGrp) then begin
//                InitByProdGrp(aSpdRange as TProdGrp);
//  {
//              else if ( aSpdRange is TOrderPosition) then
//                InitByOrderPosition ( aSpdRange is TOrderPosition)
//  }
//              end
//              else
//                InitByNil;
//
//            end;
//        else
//          edThreadCnt.Enabled := False;
//          if (aSpdRange is TProdGrp) then
//            InitByProdGrp(aSpdRange as TProdGrp)
//          else begin
//  //Nue alt      InitByMiscInfo ( aMiscInfo );
//  //Nue neu alt             InitByNil;
//  //            edThreadCnt.Text := '1';
//          end;
//        end;
      end;
      if Assigned(aMiscInfo) then begin
        InitByMiscInfo(aMiscInfo);
      end;
    except
      on e: Exception do begin
        SystemErrorMsg_('TMisc.UpdateComponent failed. ' + e.message);
      end;
    end;
  finally
      // LOK (7.11.2002): TMisc wird ausgeblendet, w�hrend die Daten von der Maschine geholt werden.
      // ==> Hier wieder einblenden
    visible := True;
  end;
end;
//------------------------------------------------------------------------------
function TMisc.CheckUserValue: boolean;
begin
  if edYarnCnt.Text = '' then begin
    edYarnCnt.SetFocus;
    InfoMsg(Translate(cFieldYarnCntNotSet));
    Result := False;
  end
  else if edThreadCnt.Text = '' then begin
    edThreadCnt.SetFocus;
    InfoMsg(Translate(cFieldThreadCntNotSet));
    Result := False;
  end
  else if edSlip.Text = '' then begin
    edSlip.SetFocus;
    InfoMsg(Translate(cFieldSlipNotSet));
    Result := False;
  end else
    Result := True;
end;
//------------------------------------------------------------------------------
procedure TMisc.edChange(Sender: TObject);
var
  xItem: TUserChanges;
begin
  if Sender = edYarnCnt then
    xItem := ucYarnCnt
  else if Sender = edThreadCnt then
    xItem := ucThreadCnt
  else if Sender = edSlip then
    xItem := ucSlip
  else
    xItem := ucNone;

  if Assigned(fSpdRangeList) then
    fSpdRangeList.ItemChanged(xItem);

  if Assigned(fOnValueChanged) then
    OnValueChanged(xItem, True);
end;
//------------------------------------------------------------------------------
procedure TMisc.edYarnCntKeyPress(Sender: TObject; var Key: Char);
begin
//  CheckFloatChar(Key);
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------
procedure TMisc.edSlipKeyPress(Sender: TObject; var Key: Char);
begin
//  CheckFloatChar(Key);
  inherited KeyPress(Key);
end;
//------------------------------------------------------------------------------
procedure TMisc.edExit(Sender: TObject);
begin
//  Check;
  if not Assigned(fSpdRangeList) then Exit;
  if Sender = edYarnCnt then
    fSpdRangeList.ItemFocusChanged(ucYarnCnt);

  if Sender = edThreadCnt then
    fSpdRangeList.ItemFocusChanged(ucYarnCnt);

  if Sender = edSlip then
    fSpdRangeList.ItemChanged(ucSlip);
end;
//------------------------------------------------------------------------------
procedure TMisc.edThreadCntKeyPress(Sender: TObject; var Key: Char);
begin
  CheckIntChar(Key);
  inherited KeyPress(key);
end;
//------------------------------------------------------------------------------
end.

