{===========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMHtmlHelp.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Contains the help component and help strings for HTMLHelp file (*.chm)
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 08.05.2000  1.00  Wss | Loaded: check for design flag in ComponentState
| 25.06.2001  1.00  Wss | Helpcontext extended for LabMaster general
| 12.09.2001  1.00  Wss | MMMaConfig added (HelpIndex = 144 - 150
| 02.10.2001  1.00  Wss | DefaultLoepfeIndex set to 3 to have MMHelp-003.chm as default help file
| 26.02.2002  1.00  Wss | AutoConfig von TMMHtmlHelp korrigiert. DefineHelpFile wird nun immer aufgerufen
| 16.07.2002  1.00  Wss | Konstante cRegHelpFilePath nach LoepfeGlobal verschoben
                          mit mindestens aktuellen Pfad mHelpFilePath := '.\'
| 30.08.2002  1.00  Wss | Neue Help-Konstanten hinzugef�gt
| 19.02.2003  1.00  Wss | Help an neue Packages angepasst: Easy, Standard, Pro
| 24.02.2003  1.00  Wss | In Loaded wird nun die Property Variable fHelpMask mit einem
                          Leerstring �berschrieben, damit nicht jede Applikation von Hand
                          ge�ffnet werden muss, um das Property zu l�schen und somit die neuen
                          Help-Dateien zu verwenden
| 25.02.2003  1.00  Wss | Funktion GetHelpContext hinzugef�gt. Nun kann die Liste aus RoboHelp
                          1:1 �bernommen werden (Pfad muss trotzdem noch angepasst werden) und
                          f�r die entsprechenden Forms und Komponenten kann zur Laufzeit per
                            HelpContext := GetHelpContext('Allgemein\MM_LabMaster.htm');
                          das Property HelpContext gesetzt werden. Somit ist man nicth mehr darauf
                          beschr�nkt, dass die Liste eine starre Reihenfolge einhalten muss.
                          -> Alle Applikationen m�ssen jedoch noch angepasst werden...
| 09.09.2003  1.00  Wss | Neue Helpkonstanten f�r Assignments
| 11.09.2003  1.00  Wss | Fehler bei dem Ermitteln ob Hilfe f�r Easy, Std oder Pro ist behoben
                          z.b. wurde in Assignment mmpPro im Property definiert, beim Aufstarten
                          jedoch �ber SettingsReader mmpStandard ausgelesen, sp�ter jedoch wurde
                          dieser Wert wieder mit mmpPro �berschrieben.
| 16.09.2003  1.00  Wss | Index f�r Saal�bersicht hinzugef�gt
| 02.12.2003  1.00  Wss | Haupthilfeindex f�r ClassDesinger hinzugef�gt
| 15.01.2004  1.00  Wss | Weitere Hilfeindexe hinzugef�gt
| 27.07.2004  1.00  Wss | In Loaded den Aufruf DefineHelpFile entfernt da sonst doppelt gemoppelt
| 05.08.2005  1.00  Wss | In Loaded wird auf fAutoConfig gepr�ft und nur wenn True wird �ber SettingsReader
                          Package geholt (wegen MMVisual)
| 30.01.2006  1.00  Wss | TMMHtmlHelp so erweitert, dass sie von ausserhalb parametrisiert werden kann
|==========================================================================================}
unit MMHtmlHelp;

interface

uses
  Classes, Controls, Forms, Windows, LoepfeGlobal, SettingsReader;

//------------------------------------------------------------------------------
const
  cDefaultLoepfeIndex = 3;
  cMMHelpMask         = 'MMHelp-%.3d.chm';
  cMMHelpEasy         = 'MM-Easy-%.3d.chm';
  cMMHelpStandard     = 'MM-Standard-%.3d.chm';
  cMMHelpPro          = 'MM-Pro-%.3d.chm';


  cMMHelpArr: Array[TMMPackage] of String = (
    cMMHelpEasy, cMMHelpStandard, cMMHelpPro
  );

//------------------------------------------------------------------------------
// RoboHELP HTML Edition Report
//------------------------------------------------------------------------------
(**)
const
  cHelpContextCount = 202;
  cHelpContext: Array[1..cHelpContextCount] of String = (
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger
    'File_not_Found.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\globale-fenster-und-funktionen
    'globale-fenster-und-funktionen\DLG_Anzeigeformat.htm',
    'globale-fenster-und-funktionen\DLG_Berichtseinstellung_festlegen.htm',
    'globale-fenster-und-funktionen\DLG_Datenreihensortieren.htm',
    'globale-fenster-und-funktionen\DLG_Drucken.htm',
    'globale-fenster-und-funktionen\DLG_Grafikoptionen.htm',
    'globale-fenster-und-funktionen\DLG_Parameter-fuer-Graphik.htm',
    'globale-fenster-und-funktionen\DLG_Profile-bearbeiten.htm',
    'globale-fenster-und-funktionen\DLG_Tabelleneigenschaften.htm',
    'globale-fenster-und-funktionen\DLG_Zeitraum-auswaehlen.htm',
    'globale-fenster-und-funktionen\GV_Beobachtungszeit-waehlen.htm',
    'globale-fenster-und-funktionen\GV_Grafik_Sortierung_anpassen.htm',
    'globale-fenster-und-funktionen\GV_GrafikParameterEinstellungenaendern.htm',
    'globale-fenster-und-funktionen\GV_Neues-Profil-erstellen.htm',
    'globale-fenster-und-funktionen\GV_Parameter_ein-_ausblenden.htm',
    'globale-fenster-und-funktionen\GV_Profil-als-Standard-speichern.htm',
    'globale-fenster-und-funktionen\GV_Profil-laden.htm',
    'globale-fenster-und-funktionen\GV_Profil-loeschen.htm',
    'globale-fenster-und-funktionen\GV_Profil-speichern.htm',
    'globale-fenster-und-funktionen\GV_Sortierung_in_der_Tabelle_aendern.htm',
    'globale-fenster-und-funktionen\GV_Spalten_ein-_ausblenden_in_der_Tabelle.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Benutzerverwaltung
    'Benutzerverwaltung\BV_Abmelden_vom_MM.htm',
    'Benutzerverwaltung\BV_Anmelden.htm',
    'Benutzerverwaltung\BV_Anmeldung_am_MM.htm',
    'Benutzerverwaltung\BV_Benutzerrechte.htm',
    'Benutzerverwaltung\BV_Benutzerverwaltung.htm',
    'Benutzerverwaltung\BV_Default_Benutzer.htm',
    'Benutzerverwaltung\BV_Grp-aendern.htm',
    'Benutzerverwaltung\BV_Neuer-Benutzer.htm',
    'Benutzerverwaltung\BV_Passwort-aendern.htm',
    'Benutzerverwaltung\BV_Rechte-anpassen.htm',
    'Benutzerverwaltung\BV_Standard_Benutzerkonfiguration.htm',
    'Benutzerverwaltung\BV_Zugriffskontrolle_anpassen.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\WindingMaster\Schichtkalender
    'WindingMaster\Schichtkalender\ShifCal_Standard_Schichten_�bernehmen.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Einstellungen-Schichtkalender.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalenderbereich_bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Kalendernamen_bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtgruppen-erstellen-bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtkalender-drucken.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Schichtmuster-uebernehmen.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_DLG_Standard-Schichten-kopieren.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Gruppenfarbe-veraendern.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Neues-Schichtmuster.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtgruppe_loeschen.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtgruppen-definieren.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender_neu_definieren.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-definition.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtkalender-drucken.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Schichtmuster-bearbeiten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Spezial-Schichten-eintragen.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Standardschichten.htm',
    'WindingMaster\Schichtkalender\SHIFTCAL_Standard-Schichten-definieren.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\WindingMaster\Offlimit
    'WindingMaster\Offlimit\Filter_einstellen.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Fenster_Offline_Offlimit.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Fenster_Online_Offlimit.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Einstellungen.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Offlimit-Ursachen.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Offline-Offlimit-Filter.htm',
    'WindingMaster\Offlimit\OFFL_DLG_Online-Offlimit-Filter-bearbeiten.htm',
    'WindingMaster\Offlimit\OFFL_Einstellungen_anpassen.htm',
    'WindingMaster\Offlimit\OFFL_Grenzwerte.htm',
    'WindingMaster\Offlimit\OFFL_Mittelwert.htm',
    'WindingMaster\Offlimit\OFFL_Offlimit_Zeiten.htm',
    'WindingMaster\Offlimit\OFFL_Offlimit-Analyse.htm',
    'WindingMaster\Offlimit\OFFL_Offlimitursache-definieren.htm',
    'WindingMaster\Offlimit\OFFL_Offline_Offlimit.htm',
    'WindingMaster\Offlimit\OFFL_Online_Offlimit.htm',
    'WindingMaster\Offlimit\OFFL_Spulstellendetail_aufrufen.htm',
    'WindingMaster\Offlimit\OFFL_Ursachenanalyse_durchfuehren.htm',
    'WindingMaster\Offlimit\Offlimit_einstellen.htm',
    'WindingMaster\Offlimit\Offlimit_Ursachen_bearbeiten.htm',
    'WindingMaster\Offlimit\Offlimit_Ursachen_zuweisen.htm',
    'WindingMaster\Offlimit\Ursachenerkennung_definieren.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\WindingMaster\Zuordnung
    'WindingMaster\Zuordnung\ZUO_AC338_Partieerfassung_starten.htm',
    'WindingMaster\Zuordnung\ZUO_AC338_Partieerfassung_stoppen.htm',
    'WindingMaster\Zuordnung\ZUO_Definition_Abgleich_Adjust.htm',
    'WindingMaster\Zuordnung\ZUO_Definition_Maschinenbereich.htm',
    'WindingMaster\Zuordnung\ZUO_Definition_Pilotspulstelle.htm',
    'WindingMaster\Zuordnung\ZUO_Definition_Schlupf.htm',
    'WindingMaster\Zuordnung\ZUO_Dialog_Partie_Eigenschaften.htm',
    'WindingMaster\Zuordnung\ZUO_DLG_Fenster_Zuordnung.htm',
    'WindingMaster\Zuordnung\ZUO_DLG_Reinigereinstellungen_bestaetigen.htm',
    'WindingMaster\Zuordnung\ZUO_DLG_Zusaetzliche_Einstellungen.htm',
    'WindingMaster\Zuordnung\ZUO_Fenster_Vorlagenverwaltung.htm',
    'WindingMaster\Zuordnung\ZUO_Neue_Reinigervorlage_erstellen.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigereinstellung_als_Vorlage_speichern.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigereinstellung_zuordnen.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigereinstellungen_anpassen.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigereinstellungen_ueberpruefen.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigervorlage_loeschen.htm',
    'WindingMaster\Zuordnung\ZUO_Reinigervorlagen.htm',
    'WindingMaster\Zuordnung\ZUO_Vorlage_kopieren.htm',
    'WindingMaster\Zuordnung\ZUO_Zuordnung_stoppen.htm',
    'WindingMaster\Zuordnung\ZUO_ZuordnungAllgemein.htm',
    // neu 9.9.2003
    'WindingMaster\Zuordnung\ZUO_DLG_Settingssource.htm',
    'WindingMaster\Zuordnung\ZUO_DLG_Filter.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\WindingMaster\Spulstellenbericht
    'WindingMaster\Spulstellenbericht\SPST_DLG_Maschinenbericht.htm',
    'WindingMaster\Spulstellenbericht\SPST_DLG_Spulstellenbericht.htm',
    'WindingMaster\Spulstellenbericht\SPST_Spulstellenbericht.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\WindingMaster\Betriebskalender
    'WindingMaster\Betriebskalender\BETRCAL_Betriebskalender_loeschen.htm',
    'WindingMaster\Betriebskalender\BETRKAL_Arbeitsfreie_Tage_eintragen.htm',
    'WindingMaster\Betriebskalender\BETRKAL_Betriebskalender_Drucken.htm',
    'WindingMaster\Betriebskalender\BETRKAL_Fenster_Betriebskalender.htm',
    'WindingMaster\Betriebskalender\BETRKAL_Neuer_Betriebskalender.htm',
    'WindingMaster\Betriebskalender\BETRKAL-definition.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\MMClient
    'MMClient\SYS_DLG_Fenster_MMClient.htm',
    'MMClient\SYS_MMClient.htm',
    'MMClient\SYS_Start-MillMaster.htm',
    'MMClient\SYS_Stop-MillMaster.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\System
    'System\SYS_DLG_Setup_Allgemein.htm',
    'System\SYS_DLG_Setup_Einstellungen.htm',
    'System\SYS_DLG_Setup_MMClient_Einstellungen.htm',
    'System\SYS_DLG_Setup_Versionsverzeichnis.htm',
    'System\SYS_MillMaster_Service.htm',
    'System\SYS_Randbedingungen-definitionen.htm',
    'System\SYS_Setup_BenutzerAdministration.htm',
    'System\SYS_Status.htm',
    'System\SYS_Zeitintervall-Aktualisierung.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Reiniger
    'Reiniger\RE_Einstellungen.htm',
    'Reiniger\RE_Grundeinstellungen.htm',
    'Reiniger\RE_Reinigereinstellungen-Allgemein.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Eventviewer
    'Eventviewer\SYS_Eventviewer.htm',
    'Eventviewer\SYS_Meldungsarten.htm',
    'Eventviewer\SYS_MillMaster_Protokoll.htm',
    'Eventviewer\SYS_Protokoll_speichern.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\LabMaster
    'LabMaster\LM_SFI.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\LabMaster\Style
    'LabMaster\Style\STYLE_Artikel_bearbeiten.htm',
    'LabMaster\Style\STYLE_Artikeldaten_export.htm',
    'LabMaster\Style\STYLE_Artikeldaten_exportieren.htm',
    'LabMaster\Style\STYLE_Artikeldaten_import.htm',
    'LabMaster\Style\STYLE_Artikeldaten_importieren.htm',
    'LabMaster\Style\STYLE_Artikelstamm_bearbeiten.htm',
    'LabMaster\Style\STYLE_Artikelstamm_betrachten.htm',
    'LabMaster\Style\STYLE_Benutzerfelder_definieren.htm',
    'LabMaster\Style\STYLE_Filter_setzen.htm',
    'LabMaster\Style\STYLE_Sortimentstamm.htm',
    'LabMaster\Style\STYLE_Sortimentstamm_bearbeiten.htm',
    // neu 15.01.2004
    'LabMaster\Style\STYLE_Reinigervorlage_dem_Artikel_zuweisen.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\LabMaster\QOfflimit
    'LabMaster\QOfflimit\QOFF_Administrator.htm',
    'LabMaster\QOfflimit\QOFF_Aufgabe_erstellen.htm',
    'LabMaster\QOfflimit\QOFF_Auswertung.htm',
    'LabMaster\QOfflimit\QOFF_generieren.htm',
    'LabMaster\QOfflimit\QOFF_Qualitaetsofflimit.htm',
    'LabMaster\QOfflimit\Q-Offlimit_Auswertung.htm',
    // neu 15.01.2004
    'LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_erfassen.htm',
    'LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_Zuordnen.htm',
    'LabMaster\QOfflimit\QOFF_Fenster_Q-Offlimit_Auswerten.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\LabMaster\LabBericht
    'LabMaster\LabBericht\LAB_Bericht_configurieren.htm',
    'LabMaster\LabBericht\LAB_Bericht_erstellen.htm',
    'LabMaster\LabBericht\LAB_Druckoptionen.htm',
    'LabMaster\LabBericht\LAB_Erweiterte_Einstellungen.htm',
    'LabMaster\LabBericht\LAB_Grafik_bearbeiten.htm',
    'LabMaster\LabBericht\LAB_Klasiermatrix_konfigurieren.htm',
    'LabMaster\LabBericht\LAB_Labor-Bericht.htm',
    'LabMaster\LabBericht\LAB_Tabelle_konfigurieren.htm',
    // neu 15.01.2004
    'LabMaster\LabBericht\LAB_Einstellungen.htm',
    'LabMaster\LabBericht\LAB_Druckoptionen.htm',
    // ClassDesigner
    'LabMaster\ClassDesigner\CLASS_Fenster_Klassendesigner.htm',
    // neu 15.01.2004
    'LabMaster\ClassDesigner\CLASS_Fenster_Klassendesigner.htm',
    'LabMaster\ClassDesigner\CLASS_Neue_Kalkulation.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Remote_Client
    'Remote_Client\RMS_Erste_Hilfe.htm',
    'Remote_Client\RMS_Remote_Client.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Allgemein
    'Allgemein\GV_Fremdfasern-Def.htm',
    'Allgemein\MM_Datenbank_Zugriff.htm',
    'Allgemein\MM_DispoMaster.htm',
    'Allgemein\MM_Easy.htm',
    'Allgemein\MM_Imperfektionen.htm',
    'Allgemein\MM_Kontaktadresse.htm',
    'Allgemein\MM_LabMaster.htm',
    'Allgemein\MM_Langzeitdaten-Haltung.htm',
    'Allgemein\MM_Module.htm',
    'Allgemein\MM_Optionen-WindingMaster.htm',
    'Allgemein\MM_Pro.htm',
    'Allgemein\MM_Produktions-Zeiten.htm',
    'Allgemein\MM_Qualittsdaten.htm',
    'Allgemein\MM_Reiniger-Assistent.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\Machinenkonfiguration
    'Machinenkonfiguration\MACONF_Maschine_aktivieren.htm',
    'Machinenkonfiguration\MACONF_Maschinen_Konfiguration.htm',
    'Machinenkonfiguration\MACONF_Maschinendaten_loeschen.htm',
    'Machinenkonfiguration\MACONF_Neue_Maschine_einfuegen.htm',
    //Folder: G:\HTMLHelp-Manuals\WindingMaster\Ger\CLA
    'CLA\Ausgangsdaten_laden.htm',
    'CLA\Auswertung_drucken.htm',
    'CLA\CLA_Cluster_Einstellung.htm',
    'CLA\CLA_Reinigereinstellungs_Empfehlung.htm',
    'CLA\CLA_SFI_Einstellung.htm',
    'CLA\CLA_Spleiss_empfindliche_Einstellung.htm',
    'CLA\CLA_Spleiss_sehr_empfindliche_Einstellung.htm',
    'CLA\Einstellungen_anpassen.htm',
    'CLA\Fenster_Reiniger_Assistent.htm',
    'CLA\Neue_Serie_erstellen.htm',
    'CLA\Reinigervorlage_speichern.htm',
    'CLA\Reinigung_mit_Reinigervorlage_simulieren.htm',
    'CLA\Reinigung_simulieren.htm',
    // neu 15.01.2004
    'CLA\CLA_Fenster_Reiniger_Assistent.htm',
    'CLA\CLA_Reinigereinstellung_speichern.htm',

    'Floor\FLOOR_STATUS_WHAT.htm'
);

//------------------------------------------------------------------------------
type
  TMMHtmlHelp = class(TComponent)
  private
    fAutoConfig: Boolean;
    fHelpFile: String;
    fLoepfeIndex: Integer;
    fCallerHWnd: HWnd;
    fHelpMask: String;
    fMMHelp: TMMPackage;
    mHelpArray: Array of String;
    mHelpFilePath: String;
    procedure DefineHelpFile;
    procedure SetAutoConfig(const Value: Boolean);
    procedure SetHelpFile(const Value: String);
    procedure SetLoepfeIndex(const Value: Integer);
//    procedure SetHelpMask(const Value: String);
    procedure SetMMHelp(const Value: TMMPackage);
  public
    property CallerHWND: HWnd read fCallerHWnd write fCallerHWnd;
    property HelpFile: String read fHelpFile write SetHelpFile;

    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure InitHelpArray(aArray: Array of String);
    procedure Loaded; override;
    function ShowHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
    procedure ShowHelpContext(aHelpContext: Integer);
  published
    property AutoConfig: Boolean read fAutoConfig write SetAutoConfig default False;
    property HelpMask: String read fHelpMask write fHelpMask;
    property LoepfeIndex: Integer read fLoepfeIndex write SetLoepfeIndex default 1;
    property MMHelp: TMMPackage read fMMHelp write SetMMHelp;
  end;


//------------------------------------------------------------------------------
// PROTOTYPE
//------------------------------------------------------------------------------
function GetHelpContext(aHelpPath: String): Integer;
function HtmlHelp(aCaller: HWnd; aFile: PChar; aCommand: DWord; aData: DWord): HWND; stdcall;

//procedure Register;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units

uses
  
  SysUtils;

//------------------------------------------------------------------------------
const
  cHtmlHelpLib     = 'HHCtrl.ocx';

function HtmlHelp; external cHtmlHelpLib name 'HtmlHelpA';
//------------------------------------------------------------------------------
{
procedure Register;
begin
  RegisterComponents('LOEPFE', [TMMHtmlHelp]);
end;
{}
//------------------------------------------------------------------------------
const
// Commands to pass to HtmlHelp()
  HH_DISPLAY_TOPIC        = $0000;
  HH_HELP_FINDER          = $0000;  // WinHelp equivalent
  HH_DISPLAY_TOC          = $0001;  // not currently implemented
  HH_DISPLAY_INDEX        = $0002;  // not currently implemented
  HH_DISPLAY_SEARCH       = $0003;  // not currently implemented
  HH_SET_WIN_TYPE         = $0004;
  HH_GET_WIN_TYPE         = $0005;
  HH_GET_WIN_HANDLE       = $0006;
  HH_ENUM_INFO_TYPE       = $0007;  // Get Info type name, call repeatedly to enumerate, -1 at end
  HH_SET_INFO_TYPE        = $0008;  // Add Info type to filter.
  HH_SYNC                 = $0009;
  HH_RESERVED1            = $000A;
  HH_RESERVED2            = $000B;
  HH_RESERVED3            = $000C;
  HH_KEYWORD_LOOKUP       = $000D;
  HH_DISPLAY_TEXT_POPUP   = $000E;  // display string resource id or text in a popup window
  HH_HELP_CONTEXT         = $000F;  // display mapped numeric value in dwData
  HH_TP_HELP_CONTEXTMENU  = $0010;  // text popup help, same as WinHelp HELP_CONTEXTMENU
  HH_TP_HELP_WM_HELP      = $0011;  // text popup help, same as WinHelp HELP_WM_HELP
  HH_CLOSE_ALL            = $0012;  // close all windows opened directly or indirectly by the caller
  HH_ALINK_LOOKUP         = $0013;  // ALink version of HH_KEYWORD_LOOKUP
  HH_GET_LAST_ERROR       = $0014;  // not currently implemented // See HHERROR.h
  HH_ENUM_CATEGORY        = $0015;  // Get category name, call repeatedly to enumerate, -1 at end
  HH_ENUM_CATEGORY_IT     = $0016;  // Get category info type members, call repeatedly to enumerate, -1 at end
  HH_RESET_IT_FILTER      = $0017;  // Clear the info type filter of all info types.
  HH_SET_INCLUSIVE_FILTER = $0018;  // set inclusive filtering method for untyped topics to be included in display
  HH_SET_EXCLUSIVE_FILTER = $0019;  // set exclusive filtering method for untyped topics to be excluded from display
  HH_INITIALIZE           = $001C;  // Initializes the help system.
  HH_UNINITIALIZE         = $001D;  // Uninitializes the help system.
  HH_PRETRANSLATEMESSAGE  = $00FD;  // Pumps messages. (NULL, NULL, MSG*).
  HH_SET_GLOBAL_PROPERTY  = $00FC;  // Set a global property. (NULL, NULL, HH_GPROP)

//------------------------------------------------------------------------------
function GetHelpContext(aHelpPath: String): Integer;
var
  i: Integer;
begin
  Result := 1;
  for i:=1 to cHelpContextCount do begin
    if CompareText(aHelpPath, cHelpContext[i]) = 0 then begin
      Result := i;
      Break;
    end;
  end;
end;
//------------------------------------------------------------------------------
// TMMHtmlHelp
//------------------------------------------------------------------------------
constructor TMMHtmlHelp.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);

  fAutoConfig    := False;
  fCallerHWnd    := 0;
  fHelpFile      := '';
  fHelpMask      := '';
  fLoepfeIndex   := 1;
  fMMHelp        := mmpEasy;
  mHelpArray     := nil;
  mHelpFilePath  := '.\';
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.DefineHelpFile;
begin
  if (([csLoading, csDesigning] * ComponentState) = []) then
  try
    fHelpFile := mHelpFilePath + Format(cMMHelpArr[fMMHelp], [fLoepfeIndex]);
    // if HelpFile not available try of default HelpFile (German)
    if not FileExists(fHelpFile) then begin
      fHelpFile := mHelpFilePath + Format(cMMHelpArr[fMMHelp], [cDefaultLoepfeIndex]);
      if not FileExists(fHelpFile) then
        fHelpFile := '';
    end;
  except
    fHelpFile := '';
  end;
end;
//------------------------------------------------------------------------------
destructor TMMHtmlHelp.Destroy;
begin
  Application.OnHelp := Nil;
  SetLength(mHelpArray, 0);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.InitHelpArray(aArray: Array of String);
var
  i: Integer;
begin
  SetLength(mHelpArray, Length(aArray) + 1);
  mHelpArray[0] := ''; // Index 0 entspricht "keine Hilfe verf�gbar"
  for i:=0 to High(aArray) do
    mHelpArray[i+1] := aArray[i];
end;

//------------------------------------------------------------------------------
procedure TMMHtmlHelp.Loaded;
begin
  inherited Loaded;

  if not(csDesigning in ComponentState) then begin
    fCallerHWnd        := Application.Handle;
    Application.OnHelp := ShowHelp;
//    fHelpMask          := ''; // Statt in jeder Applikation das Property von Hand l�schen
    if fAutoConfig then begin
      InitHelpArray(cHelpContext);
      with TMMSettingsReader.Instance do begin
        if IsPackagePro then
         MMHelp := mmpPro
        else if IsPackageStandard then
         MMHelp := mmpStandard
        else
         MMHelp := mmpEasy;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetAutoConfig(const Value: Boolean);
begin
  if Value <> fAutoConfig then begin
    fAutoConfig := Value;
    if fAutoConfig then begin
      mHelpFilePath := GetRegString(cRegLM, cRegMMCommonPath, cRegHelpFilePath, '');
      if mHelpFilePath <> '' then
        mHelpFilePath := IncludeTrailingBackslash(mHelpFilePath);
    end else
      mHelpFilePath := '.\';
    // nun mit diesen Angaben die Helpdatei herausfinden und linken
    DefineHelpFile;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetHelpFile(const Value: String);
begin
  if Value <> fHelpFile then begin
    fHelpFile := Value;
    fAutoConfig := False;
  end;
end;
//------------------------------------------------------------------------------
// 11.09.2003 wss: wird nicht mehr ben�tigt, da nun �ber Easy, Std, Pro Maske definiert ist
//procedure TMMHtmlHelp.SetHelpMask(const Value: String);
//begin
//  if Value <> fHelpMask then begin
//    fHelpMask := Trim(Value);
//    DefineHelpFile;
//  end;
//end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetLoepfeIndex(const Value: Integer);
begin
  if (Value <> fLoepfeIndex) and (Value > 0) then begin
    fLoepfeIndex := Value;
    DefineHelpFile;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.SetMMHelp(const Value: TMMPackage);
begin
  fMMHelp := Value;
  DefineHelpFile;
end;
//------------------------------------------------------------------------------
function TMMHtmlHelp.ShowHelp(Command: Word; Data: Integer; var CallHelp: Boolean): Boolean;
begin
  Result   := True;
  CallHelp := False;
  if fHelpFile <> '' then begin
    if (Data > 0) and (Data <= High(mHelpArray)) then
      HtmlHelp(fCallerHWnd, PChar(fHelpFile), HH_DISPLAY_TOPIC, DWord(mHelpArray[Data]))
    else
      HtmlHelp(fCallerHWnd, PChar(fHelpFile), Command, 0);
  end;

//  Result := True;
//  CallHelp := False;
//  if fHelpFile <> '' then begin
//    if (Data > 0) and (Data <= cHelpContextCount) then
//      HtmlHelp(fCallerHWnd, PChar(fHelpFile), HH_DISPLAY_TOPIC, DWord(cHelpContext[Data]))
//    else
//      HtmlHelp(fCallerHWnd, PChar(fHelpFile), Command, 0);
//  end;
end;
//------------------------------------------------------------------------------
procedure TMMHtmlHelp.ShowHelpContext(aHelpContext: Integer);
var
  xCallHelp: Boolean;
begin
  if aHelpContext = 0 then
    ShowHelp(HH_DISPLAY_TOC, 0, xCallHelp)
  else
    ShowHelp(HH_DISPLAY_TOPIC, aHelpContext, xCallHelp);
end;
//------------------------------------------------------------------------------

end.


