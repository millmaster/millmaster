(*=============================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: pwDBLookupCombo.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 11.04.2001  0.00  PW  | Initial Release
|                       | Show component state Edit/ReadOnly implemented
|============================================================================================*)

unit pwDBLookupCombo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, wwdblook, mmCtrls, LoepfeGlobal;

type
  TpwDBLookupCombo = class(TwwDBLookupCombo)
  private
    fColor: TColor;
    fEnabled: Boolean;
    fReadOnlyColor: TColor;
    fShowMode: TShowMode;
    function GetColor: TColor;
    function GetEnabled: Boolean;
    procedure SetColor(Value: TColor);
    procedure SetEnabled(Value: Boolean);
    procedure SetReadOnlyColor(Value: TColor);
    procedure SetShowMode(Value: TShowMode);
    procedure UpdateProperties;
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Loaded; override;
  published
    property Color: TColor read GetColor write SetColor;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property ReadOnlyColor: TColor read fReadOnlyColor write SetReadOnlyColor;
    property ShowMode: TShowMode read fShowMode write SetShowMode;
  end; //TpwDBLookupCombo

procedure Register;

//-----------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

procedure Register;
begin
  RegisterComponents('mmWorbis', [TpwDBLookupCombo]);
end; //procedure Register
//-----------------------------------------------------------------------------
constructor TpwDBLookupCombo.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fColor         := inherited Color;
  fEnabled       := inherited Enabled;
  fReadOnlyColor := clMMReadOnlyColor;
  fShowMode      := smNormal;
end; //constructor TpwDBLookupCombo.Create
//-----------------------------------------------------------------------------
destructor TpwDBLookupCombo.Destroy;
begin
  inherited Destroy;
end; //destructor TpwDBLookupCombo.Destroy
//-----------------------------------------------------------------------------
function TpwDBLookupCombo.GetColor: TColor;
begin
  Result := fColor;
end; //function TpwDBLookupCombo.GetColor
//-----------------------------------------------------------------------------
function TpwDBLookupCombo.GetEnabled: Boolean;
begin
  Result := fEnabled;
end; //function TpwDBLookupCombo.GetEnabled
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.Loaded;
begin
  inherited Loaded;
  UpdateProperties;
end; //procedure TpwDBLookupCombo.Loaded
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.SetColor(Value: TColor);
begin
  if fColor <> Value then begin
    fColor := Value;
    UpdateProperties;
  end; //if fColor <> Value then begin
end; //procedure TpwDBLookupCombo.SetColor
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.SetEnabled(Value: Boolean);
begin
  if fEnabled <> Value then begin
    fEnabled := Value;
    UpdateProperties;
  end; //if fEnabled <> Value then begin
end; //procedure TpwDBLookupCombo.SetEnabled
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.SetReadOnlyColor(Value: TColor);
begin
  if fReadOnlyColor <> Value then begin
    fReadOnlyColor := Value;
    UpdateProperties;
  end; //if fReadOnlyColor <> Value then begin
end; //procedure TpwDBLookupCombo.SetReadOnlyColor
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.SetShowMode(Value: TShowMode);
begin
  if fShowMode <> Value then begin
    fShowMode := Value;
    UpdateProperties;
  end; //if fShowMode <> Value then begin
end; //procedure TpwDBLookupCombo.SetShowMode
//-----------------------------------------------------------------------------
procedure TpwDBLookupCombo.UpdateProperties;
begin
  if (ComponentState - [csFreeNotification]) = [] then
    if fShowMode = smNormal then begin
      Color := fColor;
      inherited Enabled := fEnabled;
    end //if fShowMode = smNormal then begin
    else begin
      ReadOnly := not fEnabled;
      if fEnabled then inherited Color := fColor
                  else inherited Color := fReadOnlyColor;
      inherited Enabled := True;
    end; //else begin
end; //procedure TpwDBLookupCombo.UpdateProperties

end. //pwDBLookupCombo
