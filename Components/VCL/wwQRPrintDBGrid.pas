(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: wwQRPrintDBGrid.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
| 10.04.2001 1.00 PW  | File created
| 17.04.2001 1.00 Wss | AddColumns: ReplaceString replaced with StringReplace from SysUtils
| 16.05.2001 1.00 Wss | AddColumns: Paramter to call AddLabelToBand for text alignment changed
                                    to take alignment of Grid.TitleAlignment
| 29.10.2001 1.00 Wss | Unterst�tzung f�r Summary implementiert
                        - Property SummaryBand, welches die Daten direkt aus dem wwDBGrid holt
| 30.09.2003 1.00 Wss | Erweiterungen, damit per Events eigene Komponenten statt nur
                        der TQRText und TQRDBText verwendet werden k�nnen. Bei 
                        AddColumns wird f�r's DetailBand der Event OnAddQRComponent
                        aufgerufen, damit von ausserhalb eine eigene QR-Komponente
                        verwendet werden kann. So ist z.B. das F�rben von Spalten,
                        wie sie bei der Artikelliste in MMStyle ersichtlich ist, 
                        m�glich, jedoch muss das setzen und �berwachen der Datenwerte
                        komplett ausserhalb der Komponente erfolgen. 
|=============================================================================*)

unit wwQRPrintDBGrid;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DB, DBGrids, Grids, QuickRpt, Wwdbigrd, Wwdbgrid, QRPrintGrid, 
  QRCtrls, mmQRLabel, mmQRDBText;

type
  TwwQRPrintDBGrid = class (TBaseQRPrintGrid)
  private
    fOnAfterScroll: TAfterScrollEvent;
    fSummaryBand: TDetailBand;
    mAfterScrollProc: TDataSetNotifyEvent;
    function GetGrid: TwwDBGrid;
  protected
    procedure AddColumns; override;
    procedure DoRedirection(aRedirect: Boolean); override;
    function GetColWidth(i: integer): Integer;
    function GetTotalColCount: Integer; override;
    function GetTotalColWidth: Integer; override;
    property Grid: TwwDBGrid read GetGrid;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure LocAfterScroll(DataSet: TDataSet);
  published
    property OnAfterScroll: TAfterScrollEvent read fOnAfterScroll write fOnAfterScroll;
    property SummaryBand: TDetailBand read fSummaryBand write fSummaryBand;
  end;
  

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS;

//:---------------------------------------------------------------------------
//:--- Class: TwwQRPrintDBGrid
//:---------------------------------------------------------------------------
constructor TwwQRPrintDBGrid.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  
  fSummaryBand     := TDetailBand.Create(rbDetail);
  mAfterScrollProc := Nil;
end;

//:---------------------------------------------------------------------------
destructor TwwQRPrintDBGrid.Destroy;
begin
  fSummaryBand.Free;
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TwwQRPrintDBGrid.AddColumns;
var
  i, xPos, xWidth: Integer;
  xDBText: TmmQRDBText;
  xLabel: TmmQRLabel;
  xStr: string;
  xQRPrintable: TQRPrintable;
begin
  xPos := LeftMargin;
  for i:=0 to Grid.FieldCount -1 do begin
    xWidth := Trunc(GetColWidth(i) * mFactor);
    // nur weitermachen, wenn noch gen�gend Platz in der Breite vorhanden ist
    if (xPos + xWidth) <= HeaderBand.Band.Width then begin
      xStr := Grid.Fields[i].DisplayLabel;
      //replace word wrap chars
      // xStr := ReplaceString(xStr,'-~','');
      xStr := StringReplace(xStr, '~', #32, [rfReplaceAll]);
      // Titellabel erstellen und in Verwaltungsliste hinzuf�gen
      mHeaderList.Add(AddQRComponents(HeaderBand, i, xPos, xStr, xWidth, Grid.TitleAlignment, True));
  
      // Datenkomponente behandeln...
      xQRPrintable := Nil;
      if Assigned(OnAddQRComponent) then
        OnAddQRComponent(Self, i, xWidth, xQRPrintable);
  
      // wenn von "aussen" keine eigene Komponente erstellt wurde, dann ein QRDBText erstellen
      if xQRPrintable = Nil then begin
        xQRPrintable := TmmQRDBText.Create(Nil);
        // und die ensprechenden Properties abf�llen
        with TmmQRDBText(xQRPrintable) do begin
          DataSet   := Grid.DataSource.DataSet;
          DataField := Grid.Fields[i].FieldName;
          Alignment := Grid.Fields[i].Alignment;
          AutoSize  := False;
          AutoStretch := DataMultiLine;
          Caption   := xStr;
        end;
      end;
      // hier noch die allgemeinen Properties setzen
      with xQRPrintable do begin
        Left        := xPos;
        Parent      := DetailBand.Band;
        Width       := xWidth;
      end;
      // Vertikal ausrichten innerhalb vom Band
      CalcTopPosition(DetailBand, xQRPrintable);
      // diese Komponente in die Verwaltungsliste aufnehmen
      mDataList.Add(xQRPrintable);
  
      // QRKomponenten f�r das Summary Band
      if Assigned(SummaryBand.Band) and (xQRPrintable is TQRDBText) then begin
        xLabel  := TmmQRLabel.Create(Self);
        with xLabel do begin
          Alignment := Grid.Fields[i].Alignment;
          AutoSize  := False;
          AutoStretch := DataMultiLine;
          Caption   := Grid.Columns[i].FooterValue;
          Left      := xPos;
          Parent    := fSummaryBand.Band;
          Width     := xWidth;
        end;
        // Vertikal ausrichten innerhalb vom Band
        CalcTopPosition(fSummaryBand, xLabel);
      end;
  
      inc(xPos, xWidth + ColDistance);
    end else
      Break;
  end;
end;

//:---------------------------------------------------------------------------
procedure TwwQRPrintDBGrid.DoRedirection(aRedirect: Boolean);
begin
  inherited DoRedirection(aRedirect);
  //:...................................................................
  if aRedirect then begin
    // make sure QuickReports use the same DataSet as the linked DBGrid
    QuickReport.DataSet := Grid.DataSource.DataSet;
    // nun noch den AfterScroll Event umbiegen
    mAfterScrollProc := QuickReport.DataSet.AfterScroll;
    QuickReport.DataSet.AfterScroll := LocAfterScroll;
  end else begin
    // nun noch den AfterScroll Event wieder zur�ckbiegen
    QuickReport.DataSet.AfterScroll := mAfterScrollProc;
    QuickReport.DataSet             := Nil;
  end;
end;

//:---------------------------------------------------------------------------
function TwwQRPrintDBGrid.GetColWidth(i: integer): Integer;
begin
  // indicator column won't be printed !!!!
  if dgIndicator in Grid.Options then inc(i);
  result := Grid.ColWidthsPixels[i];
end;

//:---------------------------------------------------------------------------
function TwwQRPrintDBGrid.GetGrid: TwwDBGrid;
begin
  Result := DataGrid as TwwDBGrid;
end;

//:---------------------------------------------------------------------------
function TwwQRPrintDBGrid.GetTotalColCount: Integer;
begin
  Result := Grid.FieldCount;
end;

//:---------------------------------------------------------------------------
function TwwQRPrintDBGrid.GetTotalColWidth: Integer;
var
  i: Integer;
begin
  Result := 0;
  for i := 0 to Grid.FieldCount -1 do
    Result := Result +GetColWidth(i);
end;

//:---------------------------------------------------------------------------
procedure TwwQRPrintDBGrid.LocAfterScroll(DataSet: TDataSet);
var
  xQRPrintable: TQRPrintable;
  i: Integer;
begin
  if Assigned(mAfterScrollProc) then
    mAfterScrollProc(DataSet);
  
  if Assigned(fOnAfterScroll) then
    for i:=0 to mDataList.Count-1 do begin
      xQRPrintable := TQRPrintable(mDataList.Items[i]);
      if xQRPrintable is TQRDBText then
        fOnAfterScroll(DataSet, TQRDBText(xQRPrintable).DataSet.FieldByName(TQRDBText(xQRPrintable).DataField), xQRPrintable)
      else
        fOnAfterScroll(DataSet, Nil, xQRPrintable);
    end;
end;

end. //wwQRPrintDBGrid
