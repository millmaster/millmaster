(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMSelectNavigatorFrame.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: shows contents from style, lot, machine, shift for navigation
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 01.10.2000  1.00  Wss | Initial Release
| 09.01.2001  1.01  Wss | Reset procedure added
| 11.01.2001  1.02  Wss | Get lowest ShiftCalID from DB in Loaded
| 26.01.2001  1.03  Wss | Functionality:
                          - select Shift/Interval time range
                          - select use of clearer settings
                          - category range are now slideable with a TSplitter
| 14.02.2001  1.04  Wss | - Lot listbox changed to reflect all production groups even if they
                            are produced more than only one.
                          - Filtering for production groups changed to c_prod_id in affect of
                            changing the above line
| 11.02.2002  1.05  Wss | - Ablauf optimiert
                          - in DBVisualBox.SetSelection verbessert: auf ID anstelle von Namen
                          - Property LabelColor hinzugefuegt
| 03.04.2002  1.05  Wss | In SetUseClearerSettings wird auf ComponentState geprueft
| 10.06.2002  1.06  Nue | Erweiterungen wegen Implementation Longterm.
| 03.10.2002  1.07  Nue | Erweiterungen wegen schnellem abf�llen der StringListBoxes.
| 20.10.2002  1.05  Wss | Bei Verwendung von ClearerSettings war es extreom langsam
                          -> Where Anweisung f�r den Link mit c_YM_set_id fehlte in den Queries
| 25.11.2002  1.05  Wss | Beim Umschalten von UseClearerSettings wird die Selektion nicht mehr gel�scht
| 09.01.2003  1.05  Wss | Anpassungen und Korrekturen f�r Lanzeitdatenbank
| 23.01.2003  1.05  Wss | Wenn nichts selektiert wurde in den Listen, dann wird im Titel
                          die Anzahl der vorhandenen Items ausgegeben, damit man einen �berblick hat
                          wieviele Items �berhaupt verf�gbar sind.
| 09.05.2003  1.05  Wss | Enums statt Konstanten verwendet
| 02.06.2003  1.05  Wss | Property Busy geht nun auf das Set mSetThreadRunning
| 24.06.2003  1.05  Wss | - Schicht nicht mehr per Liste sondern auch �ber Kalenderfunktion
                          - Vordefinierte Zeitbereiche f�r Schicht- und Wochenmodus
| 04.07.2003  1.05  Wss | Gui Optik noch etwas verfeinert bei Zeitauswahl
| 21.01.2004  1.05  Wss | beim Umschalten vom UseClearerSettings wird TimeRange nicht mehr immer initialisiert
| 19.02.2004  1.05  Wss | Anpassungen an Loepfeglobales Zahlen/Datum/Zeitformatierungen
| 13.01.2005  1.05  Wss | auf XML-Settings gewechselt
|=========================================================================================*)
unit MMUSBSelectNavigatorFrame;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBVisualBox, mmLabel, ExtCtrls, Grids, mmPanel,
  Db, mmDataSource, mmSplitter, mmStringList,
  LoepfeGlobal, MMUGlobal, Menus, mmPopupMenu,
  wwdbdatetimepicker, ADODB, mmADODataSet, mmADOConnection,
  mmEdit, VCLThread, SyncObjs, mmADOQuery, Buttons, mmSpeedButton,
  mmTimer, mmVCLStringList, mmComboBox, IvDictio, LabMasterDef, mmPaintBox;

//------------------------------------------------------------------------------
resourcestring
  rsLabelStyle           = '(20)Artikel'; //ivlm
  rsLabelLot             = '(20)Partie'; // ivlm
  rsLabelMachine         = '(20)Maschine'; //ivlm
  rsLabelYMSettings      = '(20)Reinigereinstellungen'; //ivlm
  rsLabelInterval        = '(20)Intervalstart'; //ivlm
  rsLabelShift           = '(20)Zeitbereich'; // ivlm '(20)Schichtstart';
  rsLabelWeek            = '(20)Zeitbereich'; // ivlm '(20)Wochenstart';
  rsDataRangeSelected    = '(25)Gewaehlter Zeitbereich'; //ivlm

//------------------------------------------------------------------------------
type
  TFixedTimeRange = (ftrNone,
    ftrCurrentWeek, ftrLast7Days, ftrLastWeek,
    ftrCurrentMonth, ftrLastMonth, ftrLast3Months,
    ftrQuarter1, ftrQuarter2, ftrQuarter3, ftrQuarter4,
    ftrCurrentYear, ftrLastYear
  );
const
  cFixedTimeRangeStr: Array[TFixedTimeRange] of String = (
    '(*)Kein',
    '(*)Aktuelle Woche',   // ivlm
    '(*)Letzte 7 Tagen',   // ivlm
    '(*)Letzte Woche',     // ivlm
    '(*)Aktueller Monat',  // ivlm
    '(*)Letzter Monat',    // ivlm
    '(*)Letzte 3 Monate',  // ivlm
    '(*)1. Quartal',       // ivlm
    '(*)2. Quartal',       // ivlm
    '(*)3. Quartal',       // ivlm
    '(*)4. Quartal',       // ivlm
    '(*)Aktuelles Jahr',   // ivlm
    '(*)Letztes Jahr'     // ivlm
  );

  cShiftFixedTimeRanges: set of TFixedTimeRange = [ftrNone,
    ftrCurrentWeek, ftrLast7Days, ftrLastWeek,
    ftrCurrentMonth, ftrLastMonth, ftrLast3Months,
    ftrQuarter1, ftrQuarter2, ftrQuarter3, ftrQuarter4,
    ftrCurrentYear
  ];

  cWeekFixedTimeRanges:  set of TFixedTimeRange = [ftrNone,
    ftrCurrentMonth, ftrLastMonth, ftrLast3Months,
    ftrQuarter1, ftrQuarter2, ftrQuarter3, ftrQuarter4,
    ftrCurrentYear, ftrLastYear
  ];

type
  (*  Klasse:        TMyDateTimePicker
   *  Vorg�nger:     TwwDBDateTimePicker
   *  Kategorie:     Undefined
   *  Kurzbeschrieb:
   *  Beschreibung:   *)
  TMyDateTimePicker = class(TwwDBDateTimePicker)
  public
    property OnMouseDown;
  end;

  TTimeRangeInfo = record
    FromTime: string;
    ToTime: string;
    Count: Integer;
    Selected: Integer;
  end;

  TSelectionRec = record
    DataSet: TmmADODataSet;
    QueryStr: String;
    Where: String;
    ListBox: TDBVisualStringList; // Nue TWinControl
    Thread: TVCLThread;
    Title: TmmLabel;
    TitleRS: String;
  end;

  (*  Klasse:        TMMUSBSelectNavigator
   *  Vorg�nger:     TFrame
   *  Kategorie:     Undefined
   *  Kurzbeschrieb:
   *  Beschreibung:   *)
  TMMUSBSelectNavigator = class(TFrame)
    dseLot: TmmADODataSet;
    dseMachine: TmmADODataSet;
    dseStyle: TmmADODataSet;
    dseTimeRange: TmmADODataSet;
    dseYMSettings: TmmADODataSet;
    dsLot: TmmDataSource;
    dsMachine: TmmDataSource;
    dsStyle: TmmDataSource;
    dsTimeRange: TmmDataSource;
    dsYMSettings: TmmDataSource;
    laLot: TmmLabel;
    laMachine: TmmLabel;
    laSelectedLot: TmmLabel;
    laSelectedMach: TmmLabel;
    laSelectedStyle: TmmLabel;
    laSelectedTimeRange: TmmLabel;
    laSelectedYMSettings: TmmLabel;
    laStyle: TmmLabel;
    laTimeRange: TmmLabel;
    laYMSettings: TmmLabel;
    pnClearSet: TmmPanel;
    pnLot: TmmPanel;
    pnMachine: TmmPanel;
    pnStyle: TmmPanel;
    pnTimeRange: TmmPanel;
    spClearSet: TmmSplitter;
    spLot: TmmSplitter;
    spMachine: TmmSplitter;
    spTimeRange: TmmSplitter;
    thStyle: TVCLThread;
    thLot: TVCLThread;
    thMachine: TVCLThread;
    thTime: TVCLThread;
    thYMSettings: TVCLThread;
    conStyle: TmmADOConnection;
    pnTimePicker: TmmPanel;
    conLot: TmmADOConnection;
    conMachine: TmmADOConnection;
    conTime: TmmADOConnection;
    conYMSettings: TmmADOConnection;
    conDefault: TmmADOConnection;
    timUpdateFilter: TmmTimer;
    pnLotSelection: TmmPanel;
    lbLot: TDBVisualStringList;
    pnLotFilter: TmmPanel;
    edLotFilter: TmmEdit;
    pnMachineSelection: TmmPanel;
    lbMachine: TDBVisualStringList;
    pnMachineFilter: TmmPanel;
    edMachineFilter: TmmEdit;
    pnStyleSelection: TmmPanel;
    pnStyleFilter: TmmPanel;
    edStyleFilter: TmmEdit;
    lbStyle: TDBVisualStringList;
    pnYMSettingsSelection: TmmPanel;
    lbYMSettings: TDBVisualStringList;
    pnYMSetFilter: TmmPanel;
    edYMSetFilter: TmmEdit;
    pnTimeList: TmmPanel;
    pnTimeFilter: TmmPanel;
    edTimeFilter: TmmEdit;
    lbTime: TDBVisualStringList;
    pnDateSelection: TmmPanel;
    laVon: TmmLabel;
    laBis: TmmLabel;
    tpStart: TwwDBDateTimePicker;
    tpEnd: TwwDBDateTimePicker;
    mColumnNames: TmmVCLStringList;
    mOrderByArr: TmmVCLStringList;
    mWhereIDMaskArr: TmmVCLStringList;
    mmLabel1: TmmLabel;
    bTimeFromShifts: TmmSpeedButton;
    bTimeToShifts: TmmSpeedButton;
    cobFixedTimeSelection: TmmComboBox;
    pmTimeFrom: TmmPopupMenu;
    pmTimeTo: TmmPopupMenu;
    pnTimeRangeBottom: TmmPanel;
    laMaxTimeRange: TmmLabel;
    mTopPaintBox: TmmPaintBox;
    mBottomPaintBox: TmmPaintBox;
    pnTimeRangeInfo: TmmPanel;
    laTimeRangeInfo: TmmLabel;
    laResetTimeRange: TmmLabel;
    procedure OnSelectionClick(Sender: TObject);
    procedure FrameResize(Sender: TObject);
    procedure dseBeforeOpen(DataSet: TDataSet);
    procedure thExecution(Sender: TObject);
    procedure ListBoxBeforeFill(Sender: TObject);
    procedure edFilterChange(Sender: TObject);
    procedure timUpdateFilterTimer(Sender: TObject);
    procedure tpStartEndCalcBoldDay(Sender: TObject; ADate: TDate; Month, Day, Year: Integer; var Accept: Boolean);
    procedure tpStartEndChange(Sender: TObject);
    procedure laResetTimeRangeClick(Sender: TObject);
    procedure tpStartEnter(Sender: TObject);
    procedure bTimeFromToShiftsClick(Sender: TObject);
    procedure cobFixedTimeSelectionChange(Sender: TObject);
    procedure pnTimeRangeResize(Sender: TObject);
    procedure PaintBoxDividerPaint(Sender: TObject);
  private
    FDoResize: Boolean;
    FLabelColor: TColor;
    FOnAfterSelection: TNotifyEvent;
    FOnBeforeFillAllLists: TNotifyEvent;
    FOnAfterFilledAllLists: TNotifyEvent;
    FShowSelection: Boolean;
    fTimeMode: TTimeMode;
    FUseClearerSettings: Boolean;

    mActiveDatePicker: TObject;
    mAvailableDateFrom: TDateTime;
    mAvailableDateTo: TDateTime;
    mCSThreadRunning: TCriticalSection; //CriticalSection for handling mSetThreadRunning
    mCSOpeningDataSet: TCriticalSection;
    mEnableChanges: Boolean;
    mHeight: array[Boolean] of Integer;
    mInitialized: Boolean;
    mMinDate: TDateTime;
    mMaxDate: TDateTime;
    mSelection: array[TSourceSelection] of TSelectionRec;
    mSetThreadRunning: TSourceSelectionSet; //States of threads: is the thread suspended
    mSetOpeningDataSet: TSourceSelectionSet;
    mTimeFromExt: TTime;
    mTimeToExt: TTime;
    procedure AddWhereAndOpenQuery(aSource: TSourceSelection);
    procedure ClearSelection;
    function GetIDString(Index: TSourceSelection): string;
    function GetMultiSelect(Index: TSourceSelection): Boolean;
    function GetNameString(Index: TSourceSelection): string;
    function GetSelCount(aSelection: TSourceSelection): Integer;
    function GetSelectedID(aSelection: TSourceSelection): string;
    function GetSelectedName(aSelection: TSourceSelection): string;
    procedure PrepareBasicQueries(aClearSelection: Boolean);
    procedure ReadDateTimePickerRange(aDataSet: TmmADODataSet; aInitialize: Boolean);
    procedure pmTimeFromToClick(Sender: TObject);
    procedure SetDefaultShiftCal(const Value: Integer);
    procedure SetDoResize(const Value: Boolean);
    procedure SetIDString(Index: TSourceSelection; const Value: string);
    procedure SetLabelColor(const Value: TColor);
    procedure SetMultiSelect(Index: TSourceSelection; const Value: Boolean);
    procedure SetShowSelection(const Value: Boolean);
    procedure SetTimeFrom(const Value: TDateTime);
    procedure SetTimeMode(const Value: TTimeMode);
    procedure SetTimeTo(const Value: TDateTime);
    procedure SetUseClearerSettings(const Value: Boolean);
    procedure UpdateSelectedLabels;
    procedure UpdateShowMode;
    procedure UpdateTitleLabels(aTag: TSourceSelection);
    procedure CloseAllDataSet;
    procedure ChangeOpeningDataSetFlag(aAdd: Boolean; aTag: TSourceSelection);
    procedure ChangeThreadRunningFlag(aAdd: Boolean; aTag: TSourceSelection);
    function GetBusy: Boolean;
    procedure ChangeControlsEnabled(aEnabled: Boolean);
    function EvaluateStartEndOfWeek(aDate: TDate; aStart: Boolean): TDate;
    function GetTimeFrom: TDateTime;
    function GetTimeTo: TDateTime;
    function GetItemsCount(aSelection: TSourceSelection): Integer;
    function GetSelection: String;
    procedure SetSelection(const Value: String);
    procedure ResetTimeRange(aUpdate: Boolean);
    function GetNameFromID(aID: Integer; aSource: TSourceSelection): String;
  public
    FDefaultShiftCal: Integer;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetListValues(aSelection: TSourceSelection; var aStrList: TmmStringList);
    procedure Init;
    procedure Loaded; override;
    procedure Reset;
    procedure ResizePanels;
    procedure StoppFilling;
    property Busy: Boolean read GetBusy;
    property DefaultShiftCal: Integer read FDefaultShiftCal write SetDefaultShiftCal;
    property LotID: string index ssLot read GetIDString write SetIDString;
    property LotName: string index ssLot read GetNameString;
    property MachID: string index ssMach read GetIDString write SetIDString;
    property MachName: string index ssMach read GetNameString;
    property StyleNameFromID[aID: Integer]: String index ssStyle read GetNameFromID; 
    property SelectedID[aSelection: TSourceSelection]: string read GetSelectedID;
    property SelectedName[aSelection: TSourceSelection]: string read GetSelectedName;
    property Selection: String read GetSelection write SetSelection;
    property SelCount[aSelection: TSourceSelection]: Integer read GetSelCount;
    property ItemsCount[aSelection: TSourceSelection]: Integer read GetItemsCount;
    property SettingsName: string index ssYMSet read GetNameString;
    property StyleID: string index ssStyle read GetIDString write SetIDString;
    property StyleName: string index ssStyle read GetNameString;
    property TimeFrom: TDateTime read GetTimeFrom write SetTimeFrom;
    property TimeID: string index ssTime read GetIDString write SetIDString;
    property TimeName: string index ssTime read GetNameString;
    property TimeTo: TDateTime read GetTimeTo write SetTimeTo;
    property YMSetID: string index ssYMSet read GetIDString write SetIDString;
  published
    property DoResize: Boolean read FDoResize write SetDoResize;
    property LabelColor: TColor read FLabelColor write SetLabelColor;
    property MultiSelectClearSet: Boolean index ssYMSet read GetMultiSelect write SetMultiSelect;
    property MultiSelectLot: Boolean index ssLot read GetMultiSelect write SetMultiSelect;
    property MultiSelectMach: Boolean index ssMach read GetMultiSelect write SetMultiSelect;
    property MultiSelectStyle: Boolean index ssStyle read GetMultiSelect write SetMultiSelect;
    property MultiSelectTime: Boolean index ssTime read GetMultiSelect write SetMultiSelect;
    property OnAfterSelection: TNotifyEvent read FOnAfterSelection write FOnAfterSelection;
    property OnBeforeFillAllLists: TNotifyEvent read FOnBeforeFillAllLists write FOnBeforeFillAllLists;
    property OnAfterFilledAllLists: TNotifyEvent read FOnAfterFilledAllLists write FOnAfterFilledAllLists;
    property ShowSelection: Boolean read FShowSelection write SetShowSelection;
    property TimeMode: TTimeMode read fTimeMode write SetTimeMode;
    property UseClearerSettings: Boolean read FUseClearerSettings write SetUseClearerSettings;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS, mmCS,
  SettingsReader;

type
  TYMSettingsInfo = record
    Table: string;
    Where: string;
  end;

const
  cMidnightTime = 1 - 1/(24*60*60); // = 23:59:59
  //-------------------------------------------------------------------------
  // Array with additional table for ClearerSettings, if selected in property UseClearerSettings
  cQryYMSettingsInfo: array[Boolean] of TYMSettingsInfo = (
    (Table: '';                        Where: ''),
    (Table: ', t_xml_ym_settings yms '; Where: 'and v.c_YM_set_id = yms.c_YM_set_id')
    );
  //-------------------------------------------------------------------------
  // Columns: array with default col names for each query, different for Shift and Interval
  cColumnInterval      = 'distinct v.c_time_id, v.c_time ';
  cColumnShift         = 'MIN(v.c_time) TimeStart, MAX(v.c_time) TimeEnd ';
  cColumnLongtermRange = 'MIN(v.c_time) TimeStart, MAX(v.c_time) TimeEnd ';
  //-------------------------------------------------------------------------
  // Where: array with additional WHERE statement
  cWhereInterval      = 'and v.c_time_id in (%s) ';
  cWhereShift         = 'and v.c_time between %s ';
  cWhereLongtermRange = 'and v.c_time_filter between %s ';
  cWhereRangeMask     = ':TimeFrom and :TimeTo ';
  //-------------------------------------------------------------------------
  // Order: array with ORDER BY statement
  cOrderInterval  = 'order by v.c_time DESC ';
  cOrderShift     = ' '; //'order by v.c_time DESC ';
  cOrderTimeRange = ' '; // wenn TimeRange aktiviert ist, dann kein Order By
  //-------------------------------------------------------------------------
  // this 3 queries are the basic query for Interval, Shift or LongtermWeek requests
  // some information are added at runtime depending on properties
  cQryShiftIntFrame: array[TTimeMode] of string = (
    // frame for navigate through interval data
    'select %s ' + // columns
    'from v_production_interval v %s ' + // add table for SetId
    'where v.c_shiftcal_id = %d ' +
    ' %s ', // add where for SetId

    // frame for navigate through shift data
    'select %s ' + // columns
    'from v_production_shift v %s ' +
    'where v.c_shiftcal_id = %d ' +
    ' %s ', // add where for SetId

    // frame for navigate through longterm data (1.06  Nue )
    'select %s ' + // columns
    'from v_production_week v, t_shift sh %s ' +
    'where sh.c_shiftcal_id = %d ' +
    ' %s ' // add where for SetId
    );
  //-------------------------------------------------------------------------

  // Allowed timerange (needed for datetime picker)
  cQryGetMaxTimeRangeShift = 'select MIN(v.c_time) TimeStart, MAX(v.c_time) TimeEnd from v_production_shift v';
  cQryGetMaxTimeRangeWeek  = 'select MIN(v.c_time_filter) TimeStart, MAX(v.c_time_filter) TimeEnd from v_production_week v';

  cQryGetShiftsInDay = 'select c_shift_start from t_shift ' +
                       'where c_shift_start >= ''%s'' and c_shift_start <= ''%s'' ' +
                       'and c_shiftcal_id = 1 ' +
                       'order by c_shift_start';
//------------------------------------------------------------------------------
procedure GetDateTimeFromFixedTimeRange(aFixed: TFixedTimeRange; var aFrom, aTo: TDateTime);
var
  xDay, xMonth, xYear: Word;
  xNow: TDateTime;
begin
  xNow := Now;
  DecodeDate(xNow, xYear, xMonth, xDay);

  case aFixed of
    ftrCurrentWeek: begin
        aFrom := Int(xNow - DayOfWeek(xNow) + 2); // week start with Monday -> + 2
        aTo   := Int(xNow) + cMidnightTime; // -> gestern 23:59:59
      end;
    ftrLast7Days: begin
        aFrom := Int(xNow - 7);             // -> 00:00:00
        aTo   := aFrom + 6 + cMidnightTime; // -> Sonntag 23:59:59
      end;
    ftrLastWeek: begin
        aFrom := Int(xNow - 7 - DayOfWeek(xNow) + 2); // week start with Monday -> + 2
        aTo   := aFrom + 6 + cMidnightTime; // -> Sonntag 23:59:59
      end;
    ftrCurrentMonth: begin
        aFrom := EncodeDate(xYear, xMonth, 1);
        aTo   := EncodeDate(xYear, xMonth, xDay) + cMidnightTime;
      end;
    ftrLastMonth: begin
        DecodeDate(IncMonth(xNow, -1), xYear, xMonth, xDay);
        aFrom := EncodeDate(xYear, xMonth, 1);
        aTo   := EncodeDate(xYear, xMonth, MonthDays[IsLeapYear(xYear), xMonth]) + cMidnightTime;
      end;
    ftrLast3Months: begin
        DecodeDate(IncMonth(xNow, -3), xYear, xMonth, xDay);
        aFrom := EncodeDate(xYear, xMonth, 1);
        DecodeDate(IncMonth(xNow, -1), xYear, xMonth, xDay);
        aTo   := EncodeDate(xYear, xMonth, MonthDays[IsLeapYear(xYear), xMonth]) + cMidnightTime;
      end;
    ftrQuarter1: begin
        aFrom := EncodeDate(xYear, 1, 1);
        aTo   := EncodeDate(xYear, 3, 31) + cMidnightTime;
      end;
    ftrQuarter2: begin
        aFrom := EncodeDate(xYear, 4, 1);
        aTo   := EncodeDate(xYear, 6, 30) + cMidnightTime;
      end;
    ftrQuarter3: begin
        aFrom := EncodeDate(xYear, 7, 1);
        aTo   := EncodeDate(xYear, 9, 30) + cMidnightTime;
      end;
    ftrQuarter4: begin
        aFrom := EncodeDate(xYear, 10, 1);
        aTo   := EncodeDate(xYear, 12, 31) + cMidnightTime;
      end;
    ftrCurrentYear: begin
        aFrom := EncodeDate(xYear, 1, 1);
        aTo   := EncodeDate(xYear, xMonth, xDay) + cMidnightTime;
      end;
    ftrLastYear: begin
        aFrom := EncodeDate(xYear-1, 1, 1);
        aTo   := EncodeDate(xYear-1, 12, 31) + cMidnightTime;
      end;
  else
  end;
end;
//------------------------------------------------------------------------------
constructor TMMUSBSelectNavigator.Create(aOwner: TComponent);
begin
  mInitialized := False;
  inherited Create(aOwner);

  Height := 95;
  laSelectedStyle.Align      := alClient;
  laSelectedLot.Align        := alClient;
  laSelectedMach.Align       := alClient;
  laSelectedTimeRange.Align  := alClient;
  laSelectedYMSettings.Align := alClient;

  fDefaultShiftCal       := -1;
  fDoResize              := False;
  fLabelColor            := clMMReadOnlyColor;
  fOnAfterSelection      := nil;
  fOnBeforeFillAllLists  := nil;
  fOnAfterFilledAllLists := nil;
  fShowSelection         := False;
  fTimeMode              := tmShift;
  fUseClearerSettings := False;
  mEnableChanges      := True;
  mHeight[False]      := Height;
  mHeight[True]       := 95;

  mSetThreadRunning  := [];
  mSetOpeningDataSet := [];
  mCSThreadRunning   := TCriticalSection.Create;
  mCSOpeningDataSet  := TCriticalSection.Create;

  // save query objects in array for automatic access
  with mSelection[ssStyle] do begin
    DataSet := dseStyle;
    ListBox := lbStyle;
    Thread := thStyle;
    Title := laStyle;
    TitleRS := rsLabelStyle;
  end;
  with mSelection[ssLot] do begin
    DataSet := dseLot;
    ListBox := lbLot;
    Thread := thLot;
    Title   := laLot;
    TitleRS   := rsLabelLot;
  end;
  with mSelection[ssMach] do begin
    DataSet := dseMachine;
    ListBox := lbMachine;
    Thread := thMachine;
    Title  := laMachine;
    TitleRS  := rsLabelMachine;
  end;
  with mSelection[ssTime] do begin
    DataSet := dseTimeRange;
    ListBox := lbTime;
    Thread := thTime;
    Title  := laTimeRange;
    TitleRS  := rsLabelShift;
  end;
  with mSelection[ssYMSet] do begin
    DataSet := dseYMSettings;
    ListBox := lbYMSettings;
    Thread := thYMSettings;
    Title := laYMSettings;
    TitleRS := rsLabelYMSettings;
  end;
end; // TMMUSBSelectNavigator.Create cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.StoppFilling;
var
  i: TSourceSelection;
begin
  if mInitialized then begin
    for i:=Low(TSourceSelection) to High(TSourceSelection) do begin
      with mSelection[i] do begin
        ListBox.StoppFilling := True;
        while not mSelection[i].Thread.Suspended do begin
          Sleep(100);
          Application.ProcessMessages; //Ohne diese Zeile gibt es Deadlocks Nue:3.10.02
          CodeSite.SendMsg('Wait.........');
        end;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.CloseAllDataSet;
var
  i: TSourceSelection;
begin
  if mInitialized then begin
    StoppFilling;
    for i:=Low(TSourceSelection) to High(TSourceSelection) do begin
      with mSelection[i] do begin
        DataSet.Close;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.AddWhereAndOpenQuery(aSource: TSourceSelection);
  //............................................................
  procedure PrepareQueries;
  var
    xQIndex: TSourceSelection;
    xWIndex: TSourceSelection;
    xStr: String;
  begin
    // alle Sources durchgehen und ID's holen
    for xQIndex:=Low(TSourceSelection) to High(TSourceSelection) do begin
      mSelection[xQIndex].Where := Trim(GetSelectedId(xQIndex));
    end;

    // alle Sources nochmals durchgehen und Query inkl. Selektion erstellen
    for xQIndex:=Low(TSourceSelection) to High(TSourceSelection) do begin
      // Die aufrufende Source wird nicht ber�cksichtigt, da in diesem ja eine Selektion gemacht wurde
      // Alle: aSource = -1
      if xQIndex <> TSourceSelection(aSource) then begin
        with mSelection[xQIndex] do
        try
          // original Query setzen
          DataSet.Close;
          DataSet.CommandText := QueryStr;
          // nun die entsprechenden Where Anweisungen von den anderen ListBox hinzufuegen
          for xWIndex:=Low(TSourceSelection) to High(TSourceSelection) do begin
            if (xWIndex <> xQIndex) then begin
              xStr := mSelection[xWIndex].Where;
              if (xStr <> '') and ((xQIndex <> ssYMSet) or UseClearerSettings) then begin
                xStr := Format(mWhereIDMaskArr.Strings[Ord(xWIndex)], [xStr]);
                DataSet.CommandText := DataSet.CommandText + xStr;
              end;
            end;
          end;
          // finally add the order by statement
          if (xQIndex <> ssYMSet) or UseClearerSettings then begin
            DataSet.CommandText := DataSet.CommandText + (mOrderByArr.Strings[Ord(xQIndex)]);
//            CodeSite.SendFmtMsg('%s: %s', [Name, DataSet.CommandText]);
          end;
        except
          on e:Exception do
            CodeSite.SendError('AddWhereAndOpenQuery.PrepareQueries: ' + e.Message);
        end; // with
      end; // if xQIndex
    end; // for xQIndex
  end;
  //............................................................
  procedure OpenQuery(aSourceID: TSourceSelection);
  begin
    // Nur die anderen Queries behandeln
    if aSourceID <> aSource then begin
      with mSelection[aSourceID] do
      try
        if (aSourceID <> ssYMSet) or UseClearerSettings then begin
          ListBox.StoppFilling := False;
          Thread.Resume; //F�hrt 'mSelection[xQIndex].DataSet.Open' in thExecution aus
          Sleep(1);
        end;
      except
        on e:Exception do
          CodeSite.SendError('AddWhereAndOpenQuery.OpenQuery: ' + e.Message);
      end; // with
    end; // if xQIndex
  end;
  //............................................................
begin
//  EnterMethod('AddWhereAndOpenQuery ' + IntToStr(aSource));
  // Falls wir hier angelangen, obwohl das OpenDataSet noch nicht leer ist
  // dann Methode wieder verlassen. Sollte eigentlich nicht passieren!!
  mCSOpeningDataSet.Enter;
  try
    if (mSetOpeningDataSet <> []) then begin
      Exit;
    end;
  finally
    mCSOpeningDataSet.Leave;
  end;

  Screen.Cursor := crSQLWait;   // crHourGlass;
  ChangeControlsEnabled(False); // verhindert Mouse/Tastatureingaben

  // OpenSet ist leer, w�hrend abf�llen trotzdem zulassen
  // -> es m�ssen aber die F�llroutinen unterbrochen werden und warten bis sie
  // wieder Ready sind.
  StoppFilling;
  // Queries werden vorbereitet und in den entsprechenden DataSet vorgelegt
  PrepareQueries;
  // zuerst der Min/MaxTimeRange direkt holen, bevor die anderen Threads
  // gestartet werden. Thread darf nicht verwendet werden wegen �berschneidung
  // der Werte in mAvailableDateFrom/mAvailableDateTo
  if (aSource <> ssTime) and (TimeMode <> tmInterval) then begin
    ReadDateTimePickerRange(dseTimeRange, False);
    UpdateTitleLabels(ssTime);
  end else
    OpenQuery(ssTime);

  // nun nur noch die anderen Sources aktivieren, da nun mAvailableDateFrom und
  // mAvailableDateTo gelesen wurden
  OpenQuery(ssStyle);
  OpenQuery(ssMach);
  OpenQuery(ssYMSet);
  OpenQuery(ssLot);
end; // TMMUSBSelectNavigator.AddWhereAndOpenQuery cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ClearSelection;
var
  i: TSourceSelection;
begin
  mEnableChanges := False;
  edStyleFilter.Text := '';
  edLotFilter.Text   := '';
  edMachineFilter.Text := '';
  edTimeFilter.Text    := '';
  edYMSetFilter.Text   := '';
  // Restart von UpdateFilter Timer wird �ber mEnableChanges blockiert
  // daher hier 1x manuel aufrufen
  timUpdateFilter.Restart;
  // clear string id for where statement
  for i:=Low(TSourceSelection) to High(TSourceSelection) do
    mSelection[i].ListBox.SetSelection('');

  if fTimeMode <> tmInterval then begin
    cobFixedTimeSelection.ItemIndex := 0;
    ResetTimeRange(True);
  end;
  mEnableChanges := True;
  // now update queries with no selection
  UpdateSelectedLabels;
end; // TMMUSBSelectNavigator.ClearSelection cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.OnSelectionClick(Sender: TObject);
begin
  if (Sender is TwwDBDateTimePicker) then begin
    cobFixedTimeSelection.ItemIndex := 0;
    if not ((Sender as TwwDBDateTimePicker).DroppedDown) then begin
      // wenn eine Auswahl sich �berkreuzt, dann das entsprechenden Datum
      // automatisch anpassen
      if Sender = tpStart then begin
        if tpStart.Date > tpEnd.Date then
          tpStart.Date := tpEnd.Date;
      end else if Sender = tpEnd then begin
        if tpEnd.Date < tpStart.Date then
          tpEnd.Date := tpStart.Date;
      end;
    end else
      Exit;
  end;

  if Assigned(fOnBeforeFillAllLists) then
    //Signalization that Actions can become disabled
    fOnBeforeFillAllLists(Self);

  AddWhereAndOpenQuery(TSourceSelection((Sender as TControl).Tag));
  // update all labels with selection
  UpdateTitleLabels(TSourceSelection((Sender as TControl).Tag));
  UpdateSelectedLabels;
end; // TMMUSBSelectNavigator.DBVisualBoxClick cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.FrameResize(Sender: TObject);
begin
  mHeight[fShowSelection] := Height;
  if fDoResize then
    ResizePanels;
end; // TMMUSBSelectNavigator.FrameResize cat:Undefined

//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetBusy: Boolean;
begin
//  Result := (mSetOpeningDataSet <> []);
  Result := (mSetThreadRunning <> []);
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetIDString(Index: TSourceSelection): string;
begin
  Result := GetSelectedId(Index);
end; // TMMUSBSelectNavigator.GetIDString cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.GetListValues(aSelection: TSourceSelection; var aStrList: TmmStringList);
var
  xLB: TStringGrid;
begin
  case aSelection of
    ssStyle: xLB := lbStyle;
    ssLot:   xLB := lbLot;
    ssMach:  xLB := lbMachine;
    ssTime:  xLB := lbTime;
    ssYMSet: xLB := lbYMSettings
  else
    Exit;
  end;

  with xLB do begin
    aStrList.BeginUpdate;
    aStrList.Assign(Cols[0]);
    aStrList.EndUpdate;
  end;
end; // TMMUSBSelectNavigator.GetListValues cat:Undefined

//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetMultiSelect(Index: TSourceSelection): Boolean;
begin
  case Index of
    ssStyle: Result := lbStyle.Multiselect;
    ssLot:   Result := lbLot.MultiSelect;
    ssMach:  Result := lbMachine.MultiSelect;
    ssTime:  Result := lbTime.MultiSelect;
    ssYMSet: Result := lbYMSettings.MultiSelect;
  else
    Result := False;
  end;
end; // TMMUSBSelectNavigator.GetMultiSelect cat:Undefined

//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetNameString(Index: TSourceSelection): string;
begin
  case Index of
    ssStyle: Result := lbStyle.Hint;
    ssLot:   Result := lbLot.Hint;
    ssMach:  Result := lbMachine.Hint;
    ssTime:  Result := lbTime.Hint;
    ssYMSet:
      if UseClearerSettings then
        Result := lbYMSettings.Hint
      else
        Result := '';
  else
    Result := '';
  end;
end; // TMMUSBSelectNavigator.GetNameString cat:Undefined

//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetSelectedID(aSelection: TSourceSelection): string;
begin
  if (aSelection = ssTime) and (fTimeMode <> tmInterval) then
    Result := cWhereRangeMask
  else
    Result := mSelection[aSelection].ListBox.KeyCommaText;
end; // TMMUSBSelectNavigator.GetSelectedID cat:Undefined

//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetSelectedName(aSelection: TSourceSelection): string;
begin
  case aSelection of
    ssStyle: Result := lbStyle.Hint;
    ssLot:   Result := lbLot.Hint;
    ssMach:  Result := lbMachine.Hint;
    ssTime:  Result := lbTime.Hint;
    ssYMSet:
      if UseClearerSettings then
        Result := lbYMSettings.Hint
      else
        Result := '';
  else
    Result := '';
  end;
end; // TMMUSBSelectNavigator.GetSelectedName cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.Loaded;
begin
  inherited Loaded;
  ResizePanels;
end; // TMMUSBSelectNavigator.Loaded cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.Init;
begin
  if not (csDesigning in ComponentState) then begin
    // get the first ShiftCalID from DB once
    if fDefaultShiftCal = -1 then begin
      with TmmADODataSet.Create(nil) do try
        Connection := conDefault;
        CommandText := 'SELECT MIN(c_shiftcal_id) DefaultCal FROM t_shiftcal';
        Open;
        if RecordCount > 0 then
          fDefaultShiftCal := FieldByName('DefaultCal').AsInteger
        else
          fDefaultShiftCal := 1;
      finally
        Free;
      end;
    end;

    mInitialized := True;
    PrepareBasicQueries(True);
  end;
end; // TMMUSBSelectNavigator.Init cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.PrepareBasicQueries(aClearSelection: Boolean);
var
  i: TSourceSelection;
begin
  if mInitialized then begin
    for i:=Low(TSourceSelection) to High(TSourceSelection) do begin
      // set databasename property in every query component
      with mSelection[i].DataSet do
        Close;
    end;

//    if aClearSelection then
//      ClearSelection;

    case fTimeMode of
      tmInterval: begin
          lbTime.DataSource := dsTimeRange;
          mColumnNames.Strings[Ord(ssTime)] := cColumnInterval;
          mWhereIDMaskArr.Strings[Ord(ssTime)] := cWhereInterval;
          mOrderByArr.Strings[Ord(ssTime)]  := cOrderInterval;
        end;
      tmShift: begin
          lbTime.DataSource := Nil;
          mColumnNames.Strings[Ord(ssTime)] := cColumnShift;
          mWhereIDMaskArr.Strings[Ord(ssTime)] := cWhereShift;
          mOrderByArr.Strings[Ord(ssTime)]  := cOrderShift;
          ReadDateTimePickerRange(dseTimeRange, aClearSelection);
//          ReadDateTimePickerRange(dseTimeRange, True);
        end;
      tmLongtermWeek: begin
          lbTime.DataSource := Nil;
          mColumnNames.Strings[Ord(ssTime)] := cColumnLongtermRange;
          mWhereIDMaskArr.Strings[Ord(ssTime)] := cWhereLongtermRange;
          mOrderByArr.Strings[Ord(ssTime)]  := cOrderTimeRange;
          ReadDateTimePickerRange(dseTimeRange, aClearSelection);
//          ReadDateTimePickerRange(dseTimeRange, True);
        end;
    else
    end;

    for i:=Low(TSourceSelection) to High(TSourceSelection) do begin
      // Alle Grundqueries haben das gleiche Format
      mSelection[i].QueryStr := Format(cQryShiftIntFrame[fTimeMode], [mColumnNames.Strings[Ord(i)],
        cQryYMSettingsInfo[UseClearerSettings].Table,
        fDefaultShiftCal,
        cQryYMSettingsInfo[UseClearerSettings].Where]);
    end;

    // nach unten verschoben, da bei Initialisierung die Properties TimeFrom/To
    // in ReadDateTimePickerRange ermittelt werden und darum erst hier bekannt sind
    if aClearSelection then
      ClearSelection;

    AddWhereAndOpenQuery(TSourceSelection(-1));
  end;
end; // TMMUSBSelectNavigator.Init cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.dseBeforeOpen(DataSet: TDataSet);
var
  xParam: TParameter;
begin
  with TmmADODataSet(DataSet) do begin
    xParam := Parameters.FindParam('TimeFrom');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := TimeFrom;
    end;
    xParam := Parameters.FindParam('TimeTo');
    if xParam <> nil then begin
      xParam.DataType := ftDateTime;
      xParam.Value    := TimeTo;
    end;
  end;
end; // TMMUSBSelectNavigator.qryBeforeOpen cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.Reset;
begin
  ClearSelection;
  AddWhereAndOpenQuery(TSourceSelection(-1));
end; // TMMUSBSelectNavigator.Reset cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ResizePanels;
var
  xWidth: Integer;
  xCount: Integer;
begin
  xCount := 0;
  if pnStyle.Visible then inc(xCount);
  if pnLot.Visible then inc(xCount);
  if pnMachine.Visible then inc(xCount);
  if pnTimeRange.Visible then inc(xCount);

  if xCount > 0 then begin
    // make the changes visible in design mode
    if fUseClearerSettings then begin
      xWidth := ClientWidth div (xCount + 1);
      pnClearSet.Width := xWidth;
      spClearSet.Width := 5;
    end
    else begin
      xWidth := (ClientWidth - ((xCount - 1) * 5)) div xCount;
      pnClearSet.Width := 0;
      spClearSet.Width := 0;
    end;
    pnTimeRange.Align := alNone;
    if pnStyle.Visible then pnStyle.Width := xWidth;
    if pnLot.Visible then pnLot.Width := xWidth;
    if pnMachine.Visible then pnMachine.Width := xWidth;
    pnTimeRange.Align := alClient;
  end;
end; // TMMUSBSelectNavigator.ResizePanels cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ReadDateTimePickerRange(aDataSet: TmmADODataSet; aInitialize: Boolean);
var
  xTimeFrom, xTimeTo: TDate;
begin
  with aDataSet do
  try
    if aInitialize then begin
      Close;
      if fTimeMode = tmShift then
        CommandText := cQryGetMaxTimeRangeShift
      else
        CommandText := cQryGetMaxTimeRangeWeek;
    end;
    Open;
    if not EOF then
    try
      // Lese die Min und Max m�glichen Zeiten in der Daten vorhanden sind
      // Beim Aufstarten entspricht dies dem vollen Zeitbereich, in welchem
      // Daten vorhanden sind, Ansonsten wird der Bereich durch die Auswahl
      // der anderen Selektionen eingeschr�nkt.
      xTimeFrom := Int(FieldByName('TimeStart').AsDateTime);
      xTimeTo   := Int(FieldByName('TimeEnd').AsDateTime);
      if fTimeMode = tmShift then begin
        mAvailableDateFrom := xTimeFrom;
        mAvailableDateTo   := xTimeTo + cMidnightTime;
      end else begin
        // Min/MaxDate werden jeweils auf Wochenanfang und -ende berechnet
        mAvailableDateFrom := Int(EvaluateStartEndOfWeek(xTimeFrom, True));
        mAvailableDateTo := Int(EvaluateStartEndOfWeek(xTimeTo, False));
      end;

      if aInitialize then begin
        // und den Zeitbereich einschr�nken
        mMinDate := mAvailableDateFrom;
        mMaxDate := mAvailableDateTo;
        // Zeitbereich f�r's erste mal auf 1 Jahr beschr�nken
        if (xTimeTo - xTimeFrom) > 365 then
          xTimeFrom := xTimeTo - 365;
        // nun noch den Zeitbereich in den Komponenten �bernehmen
        TimeFrom := xTimeFrom;
        TimeTo   := xTimeTo;
      end;

      laMaxTimeRange.Caption := Format('%s - %s', [DateToStr(mAvailableDateFrom), DateToStr(mAvailableDateTo)]);
    except
      on e:Exception do
        CodeSite.SendError('ReadDateTimePickerRange: ' + e.Message);
    end;
  finally
    Close;
  end;
end; // TMMUSBSelectNavigator.ReadDateTimePickerRange cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetDefaultShiftCal(const Value: Integer);
begin
  if fDefaultShiftCal <> Value then
    fDefaultShiftCal := Value;
end; // TMMUSBSelectNavigator.SetDefaultShiftCal cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetDoResize(const Value: Boolean);
begin
  if fDoResize <> Value then begin
    fDoResize := Value;
    if fDoResize then
      ResizePanels;
  end;
end; // TMMUSBSelectNavigator.SetDoResize cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetIDString(Index: TSourceSelection; const Value: string);
begin
  case Index of
    ssStyle: lbStyle.KeyCommaText := Value;
    ssLot: lbLot.KeyCommaText := Value;
    ssMach: lbMachine.KeyCommaText := Value;
    ssTime: lbTime.KeyCommaText := Value;
    ssYMSet: lbYMSettings.KeyCommaText := Value;
  else
  end;
  AddWhereAndOpenQuery(Index);
  UpdateSelectedLabels;
end; // TMMUSBSelectNavigator.SetIDString cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetLabelColor(const Value: TColor);
begin
  if Value <> fLabelColor then begin
    fLabelColor := Value;
    laSelectedStyle.Color := fLabelColor;
    laSelectedLot.Color := fLabelColor;
    laSelectedMach.Color := fLabelColor;
    laSelectedTimeRange.Color := fLabelColor;
    laSelectedYMSettings.Color := fLabelColor;
  end;
end; // TMMUSBSelectNavigator.SetLabelColor cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetMultiSelect(Index: TSourceSelection; const Value: Boolean);
begin
  case Index of
    ssStyle: lbStyle.MultiSelect := Value;
    ssLot:   lbLot.MultiSelect := Value;
    ssMach:  lbMachine.MultiSelect := Value;
    ssTime:  lbTime.MultiSelect := Value;
    ssYMSet: lbYMSettings.MultiSelect := Value;
  else
  end;
end; // TMMUSBSelectNavigator.SetMultiSelect cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetShowSelection(const Value: Boolean);
begin
  if Value <> fShowSelection then begin
    fShowSelection := Value;
    UpdateShowMode;
    Height := mHeight[fShowSelection];
  end;
end; // TMMUSBSelectNavigator.SetShowSelection cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetTimeFrom(const Value: TDateTime);
begin
  tpStart.Date := Value;
end; // TMMUSBSelectNavigator.SetTimeFrom cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.UpdateShowMode;
begin
  if not (csDesigning in ComponentState) then begin
    laSelectedStyle.Visible := not fShowSelection;
    laSelectedLot.Visible   := not fShowSelection;
    laSelectedMach.Visible  := not fShowSelection;
    laSelectedYMSettings.Visible := not fShowSelection;
    laSelectedTimeRange.Visible  := not fShowSelection;

    pnStyleSelection.Visible   := fShowSelection;
    pnLotSelection.Visible     := fShowSelection;
    pnMachineSelection.Visible := fShowSelection;
    pnYMSettingsSelection.Visible := fShowSelection;
    // entweder Listbox oder Kalender f�r Zeitauswahl anzeigen lassen
    pnTimeList.Visible        := fShowSelection and (fTimeMode = tmInterval);
    pnTimePicker.Visible      := fShowSelection and (fTimeMode <> tmInterval);
    // Buttons f�r Schichtauswahl nur im Schichtmodus
    bTimeFromShifts.Visible  := (fTimeMode = tmShift);
    bTimeToShifts.Visible    := (fTimeMode = tmShift);
    // Zeitbereichlabel noch anpassen
    laMaxTimeRange.Caption  := Format('%s - %s', [DateToStr(mAvailableDateFrom), DateToStr(mAvailableDateTo)]);
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetTimeMode(const Value: TTimeMode);
var
  xTR: TFixedTimeRange;
  xTRSet: set of TFixedTimeRange;
begin
  fTimeMode := Value;
  // erst alle DataSet schliessen
  CloseAllDataSet;

  case fTimeMode of
    tmInterval: begin
        xTRSet := [];
        mSelection[ssTime].TitleRS := rsLabelInterval;
      end;
    tmShift: begin
        xTRSet := cShiftFixedTimeRanges;
        mSelection[ssTime].TitleRS := rsLabelShift;
        mTimeFromExt := 0.0;
        mTimeToExt   := cMidnightTime;
        if (Self.Height < 200) and fShowSelection then
          Self.Height := 200;
      end;
    tmLongtermWeek: begin
        xTRSet := cWeekFixedTimeRanges;
        mSelection[ssTime].TitleRS := rsLabelWeek;
        mTimeFromExt := 0.0;
        mTimeToExt   := cMidnightTime;
        if (Self.Height < 200) and fShowSelection then
          Self.Height := 200;
      end;
  else
  end;
  // Combobox mit den festen Zeitbereichsauswahlen auff�llen
  cobFixedTimeSelection.Clear;
  for xTR:=Low(TFixedTimeRange) to High(TFixedTimeRange) do begin
    if xTR in xTRSet then
      cobFixedTimeSelection.Items.AddObject(Translate(cFixedTimeRangeStr[xTR]), Pointer(xTR));
  end;
  if cobFixedTimeSelection.Items.Count > 0 then
    cobFixedTimeSelection.ItemIndex := 0;

  UpdateTitleLabels(ssTime);
  UpdateShowMode;
  PrepareBasicQueries(True);
end; // TMMUSBSelectNavigator.SetTimeMode cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetTimeTo(const Value: TDateTime);
begin
  tpEnd.Date := Value;
end; // TMMUSBSelectNavigator.SetTimeTo cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetUseClearerSettings(const Value: Boolean);
var
  xEnabled: Boolean;
begin
  if Value <> fUseClearerSettings then begin
    StoppFilling;
    fUseClearerSettings := Value;
    pnClearSet.Visible := fUseClearerSettings;
    spClearSet.Visible := fUseClearerSettings;
    ResizePanels;

    if mInitialized then
      mSelection[ssYMSet].ListBox.KeyCommaText := '';
    if not (csDesigning in ComponentState) then begin
      xEnabled := Enabled;
      Enabled := False;
      PrepareBasicQueries(False);
      Enabled := xEnabled;
    end;
  end;
end; // TMMUSBSelectNavigator.SetUseClearerSettings cat:Undefined
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.UpdateTitleLabels(aTag: TSourceSelection);
begin
  with mSelection[aTag] do begin
    if ((aTag = ssTime) and (fTimeMode <> tmInterval)) OR (ListBox.SelCount > 0) then begin
      Title.Caption := TitleRS;
    end else
      Title.Caption := Format('%s (#%d)', [TitleRS, ListBox.ItemsCount]);
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.UpdateSelectedLabels;
  //.............................................
  procedure SetSelectedString(aSrcListBox: TDBVisualStringList; aDestLabel: TLabel);
  var
    xStr: string;
  begin
    with aSrcListBox do begin
      // Duplicated ist in der Methode DataText ausgeschalten
      xStr := Trim(DataText('*'));
      aDestLabel.Caption := xStr;
      if xStr = '*' then
        xStr := '';

      Hint := xStr;
      aDestLabel.Hint := xStr;
    end;
  end; // TMMUSBSelectNavigator.SetSelectedString cat:Undefined
  //.............................................
begin
  SetSelectedString(lbStyle,      laSelectedStyle);
  SetSelectedString(lbLot,        laSelectedLot);
  SetSelectedString(lbMachine,    laSelectedMach);
  SetSelectedString(lbYMSettings, laSelectedYMSettings);
  if fTimeMode = tmInterval then
    SetSelectedString(lbTime, laSelectedTimeRange)
  else
    laSelectedTimeRange.Caption := Format('%s - %s', [DateToStr(TimeFrom), DateToStr(TimeTo)]);
end; // TMMUSBSelectNavigator.UpdateSelectedLabels cat:Undefined

//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.thExecution(Sender: TObject);
var
  xTag: TSourceSelection;
begin
  xTag := TSourceSelection((Sender as TVCLThread).Tag);
  with mSelection[xTag].DataSet do
  try
    // Setze Flag f�r diesen Thread als Running. Wird im finally wieder gel�scht
    ChangeThreadRunningFlag(True, xTag);

    // Das Open von diesem DataSet steht noch bevor -> Flag setzen
    // Wird in Event Property BeforeFill von den DBVisualStringList wieder gel�scht
    ChangeOpeningDataSetFlag(True, xTag);

    // Open bewirkt, dass schlussendlich in der DBVisualStringList Komponente
    // die Daten ausgelesen und abgef�llt werden.
    Open;
  finally
    // Dieser Thread ist fertig -> Running Flag aus dem Set l�schen
    ChangeThreadRunningFlag(False, xTag);
  end; //with
end;
//------------------------------------------------------------------------------

destructor TMMUSBSelectNavigator.Destroy;
begin
  while Busy do
    Application.ProcessMessages;

  mCSThreadRunning.Free;
  mCSOpeningDataSet.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ChangeControlsEnabled(aEnabled: Boolean);
begin
  lbStyle.Enabled      := aEnabled;
  lbLot.Enabled        := aEnabled;
  lbMachine.Enabled    := aEnabled;
  lbTime.Enabled       := aEnabled;
  lbYMSettings.Enabled := aEnabled;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ChangeOpeningDataSetFlag(aAdd: Boolean; aTag: TSourceSelection);
begin
  mCSOpeningDataSet.Enter;
  try
    if aAdd then begin
      Include(mSetOpeningDataSet, aTag);
//      CodeSite.SendInteger('ChangeOpeningDataSetFlag Add', Ord(aTag));
    end
    else begin
      Exclude(mSetOpeningDataSet, aTag);
//      CodeSite.SendInteger('ChangeOpeningDataSetFlag Rem', Ord(aTag));
    end;
  finally
    // wenn alle DataSet ge�ffnet wurden, dann rufe das Event Property OnAfterSelection auf
    // und setze den AppStart Mauszeiger
    if mSetOpeningDataSet = [] then begin
      ChangeControlsEnabled(True);
      if Assigned(fOnAfterSelection) then
        fOnAfterSelection(Self);
      Screen.Cursor := crAppStart;
    end;
    mCSOpeningDataSet.Leave;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ChangeThreadRunningFlag(aAdd: Boolean; aTag: TSourceSelection);
begin
  mCSThreadRunning.Enter;
  try
    if aAdd then
      // dieser Thread startet
      Include(mSetThreadRunning, aTag)
    else begin
      // dieser Thread ist fertig
      Exclude(mSetThreadRunning, aTag);
      UpdateTitleLabels(aTag);
//      CodeSite.SendInteger('ChangeThreadRunningFlag', Ord(aTag));
    end;
  finally
    // wenn alle Thread fertig sind, dann rufe das Event Property OnAfterFilledAllLists auf
    // und setze den Default Mauszeiger
    if mSetThreadRunning = [] then begin
      ChangeControlsEnabled(True);
      if Assigned(fOnAfterFilledAllLists) then
        fOnAfterFilledAllLists(Self);
      Screen.Cursor := crDefault;
      Mouse.CursorPos := Mouse.CursorPos;
    end;
    mCSThreadRunning.Leave;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ListBoxBeforeFill(Sender: TObject);
begin
  // Das �ffnen von diesem DataSet ist beendet -> Flag aus Set l�schen
  ChangeOpeningDataSetFlag(False, TSourceSelection(TControl(Sender).Tag));
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.edFilterChange(Sender: TObject);
begin
  if mEnableChanges then
    timUpdateFilter.Restart;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.timUpdateFilterTimer(Sender: TObject);
begin
  timUpdateFilter.Enabled := False;
  lbStyle.Filter      := edStyleFilter.Text;
  lbLot.Filter        := edLotFilter.Text;
  lbMachine.Filter    := edMachineFilter.Text;
  lbTime.Filter       := edTimeFilter.Text;
  lbYMSettings.Filter := edYMSetFilter.Text;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.tpStartEndCalcBoldDay(Sender: TObject;
  ADate: TDate; Month, Day, Year: Integer; var Accept: Boolean);
begin
  Accept := (ADate >= mMinDate) and
            (ADate <= mMaxDate);
  if fTimeMode = tmLongtermWeek then begin
    // DayOfWeek: 1=So, 2=Mo, 3=Di, 4=Mi, 5=Do, 6=Fr, 7=Sa
    if mActiveDatePicker = tpStart then
      Accept := (Accept and (DayOfWeek(ADate) = 2)) OR ((Day = 1) and (ADate >= mMinDate))
    else if mActiveDatePicker = tpEnd then
      Accept := (Accept and (DayOfWeek(ADate) = 1)) OR ((Day = MonthDays[IsLeapYear(Year), Month]) and (ADate <= mMaxDate))
    else
      Accept := False;
  end
  else if fTimeMode = tmLongtermWeek then begin
    if mActiveDatePicker = tpStart then
      Accept := Accept and (ADate >= mMinDate)
    else if mActiveDatePicker = tpEnd then
      Accept := Accept and (ADate <= mMaxDate)
    else
      Accept := False;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.tpStartEndChange(Sender: TObject);
var
  xDay, xMonth, xYear: Word;
begin
  with TwwDBDateTimePicker(Sender) do begin
    DecodeDate(Date, xYear, xMonth, xDay);
    if fTimeMode = tmLongtermWeek then begin
      // Das Start/End Datum nur dann anpassen, wenn Mitten in der Woche gew�hlt wurde
      // Wenn 1. oder Letzter Tag vom Monat selektiert wurde, dann entsprechend behalten
      if (Sender = tpStart) and (xDay <> 1) then
        Date := EvaluateStartEndOfWeek(Date, True)
      else if (Sender = tpEnd) and (xDay <> MonthDays[IsLeapYear(xYear), xMonth]) then
        Date := EvaluateStartEndOfWeek(Date, False);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.EvaluateStartEndOfWeek(aDate: TDate; aStart: Boolean): TDate;
begin
  // ww DoW:    0=Mo, 1=Di, 2=Mi, 3=Do, 4=Fr, 5=Sa, 6=So (Enumeration)
  // DayOfWeek: 1=So, 2=Mo, 3=Di, 4=Mi, 5=Do, 6=Fr, 7=Sa
  // SettingsReader liefert Index basis 0 zur�ck
  //            0=So, 1=Mo, 2=Di, 3=Mi, 4=Do, 5=Fr, 6=Sa
  if aStart then
    Result := aDate - ((DayOfWeek(aDate) - 1 - TMMSettingsReader.Instance.Value[cStartDayOfWeek] + 7) mod 7)
  else
    Result := aDate + ((TMMSettingsReader.Instance.Value[cStartDayOfWeek] - DayOfWeek(aDate) + 7) mod 7);
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.laResetTimeRangeClick(Sender: TObject);
begin
  ResetTimeRange(True);
  OnSelectionClick(tpStart);
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.ResetTimeRange(aUpdate: Boolean);
begin
  mTimeFromExt := 0.0;
  mTimeToExt   := cMidnightTime;
  if aUpdate then begin
    TimeFrom := mMinDate;
    TimeTo   := mMaxDate;
  end;
end;
//------------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.tpStartEnter(Sender: TObject);
begin
  mActiveDatePicker := Sender;
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetTimeFrom: TDateTime;
begin
// mAvailableDateFrom, mAvailableDateTo
  Result := Int(tpStart.Date) + mTimeFromExt;
  if Result < mAvailableDateFrom then
    Result := mAvailableDateFrom
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetTimeTo: TDateTime;
begin
// mAvailableDateFrom, mAvailableDateTo
  Result := Int(tpEnd.Date) + mTimeToExt;
  if Result > (mAvailableDateTo + cMidnightTime)then
    Result := mAvailableDateTo + cMidnightTime;
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetSelCount(aSelection: TSourceSelection): Integer;
begin
  with mSelection[aSelection] do begin
    if ((aSelection = ssTime) and (fTimeMode = tmLongtermWeek)) then begin
      Result := 0;
    end else
      Result := ListBox.SelCount;
  end;
end;
//------------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetItemsCount(aSelection: TSourceSelection): Integer;
begin
  with mSelection[aSelection] do begin
    Result := ListBox.ItemsCount;
{
    if ((xIndex = ssTime) and (fTimeMode = tmLongtermWeek)) then begin
      Result := 0;
    end else
      Result := ListBox.SelCount;
{}
  end;
end;

//:---------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetSelection: String;
begin
  with TIniStringList.Create do
  try
    Values['TimeMode'] := TimeMode;
    Values['StyleID'] := StringReplace(mSelection[ssStyle].ListBox.KeyCommaText, '''', '', [rfReplaceAll]);
    Values['LotID']   := StringReplace(mSelection[ssLot].ListBox.KeyCommaText, '''', '', [rfReplaceAll]);
    Values['MachID']  := StringReplace(mSelection[ssMach].ListBox.KeyCommaText, '''', '', [rfReplaceAll]);
    Values['YMSetID'] := StringReplace(mSelection[ssYMSet].ListBox.KeyCommaText, '''', '', [rfReplaceAll]);
    if TimeMode = tmInterval then
      Values['TimeID'] := StringReplace(mSelection[ssTime].ListBox.KeyCommaText, '''', '', [rfReplaceAll])
    else begin
      Values['FixedTimeRange'] := cobFixedTimeSelection.ItemIndex;
      if cobFixedTimeSelection.ItemIndex = 0 then begin
        Values['TimeFrom'] := mmFloatToStr(TimeFrom);
        Values['TimeTo']   := mmFloatToStr(TimeTo);
//        Values['TimeFrom'] := Double(TimeFrom);
//        Values['TimeTo']   := Double(TimeTo);
      end;
    end;
    Result := CommaText;
    CodeSite.SendString('TMMUSBSelectNavigator.GetSelection', Result);
  finally
    Free;
  end;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.SetSelection(const Value: String);
var
  xDateTime: TDateTime;
begin
  if Value <> '' then begin
    CodeSite.SendString('TMMUSBSelectNavigator.SetSelection', Value);
    with TIniStringList.Create do
    try
      CommaText := Value;
      TimeMode  := ValueDef('TimeMode', TimeMode);
      // now wait until component gets ready
      while Busy do
        Application.ProcessMessages;

      mSelection[ssStyle].ListBox.KeyCommaText := ValueDef('StyleID', '');
      mSelection[ssLot].ListBox.KeyCommaText   := ValueDef('LotID', '');
      mSelection[ssMach].ListBox.KeyCommaText  := ValueDef('MachID', '');
      mSelection[ssYMSet].ListBox.KeyCommaText := ValueDef('YMSetID', '');
      if TimeMode = tmInterval then
        mSelection[ssTime].ListBox.KeyCommaText := ValueDef('TimeID', '')
      else begin
        cobFixedTimeSelection.ItemIndex := ValueDef('FixedTimeRange', 0);
        if cobFixedTimeSelection.ItemIndex = 0 then begin
          xDateTime := mmStrToFloat(ValueDef('TimeFrom', '0'));
          if xDateTime < cDateTime1970 then xDateTime := Int(Now) - 365;
          TimeFrom := xDateTime;
          xDateTime := mmStrToFloat(ValueDef('TimeTo', '0'));
          if xDateTime < cDateTime1970 then xDateTime := Int(Now) + cMidnightTime;
          TimeTo   := xDateTime;
//          TimeFrom := ValueDef('TimeFrom', Int(Now)-365);
//          TimeTo   := ValueDef('TimeTo', Now + cMidnightTime);
        end else
          cobFixedTimeSelectionChange(Nil);
      end;
    finally
      Free;
    end;
    AddWhereAndOpenQuery(TSourceSelection(-1));
    UpdateSelectedLabels;
    // now wait until component gets ready
    while Busy do
      Application.ProcessMessages;
  end;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.bTimeFromToShiftsClick(Sender: TObject);
  //.............................................................
  procedure GetShiftInfo(aDateTimePicker: TwwDBDateTimePicker; aMenu: TPopupMenu);
  var
    xMenu: TMenuItem;
  begin
    aMenu.Items.Clear;
    with dseTimeRange do begin
      Close;
      CommandText := Format(cQryGetShiftsInDay, [FormatDateTime(cUSADateMask, aDateTimePicker.Date),
                                                 FormatDateTime(cUSADateMask, aDateTimePicker.Date + cMidnightTime)]);
//      CodeSite.SendString('GetShiftInfo', CommandText);
      Open;
      while not EOF do
      try
        xMenu := TMenuItem.Create(Self);
        with xMenu do begin
          Tag     := aMenu.Tag;
          OnClick := pmTimeFromToClick;
          Caption := TimeToStr(FieldByName('c_shift_start').AsFloat);
        end;
        aMenu.Items.Add(xMenu);
        Next;
      except
        on e:Exception do
          CodeSite.SendError('GetShiftInfo: ' + e.Message);
      end;
    end;
  end;
  //.............................................................
begin
  with TSpeedButton(Sender) do begin
    if Tag = 0 then
      GetShiftInfo(tpStart, pmTimeFrom)
    else
      GetShiftInfo(tpEnd, pmTimeTo);

    with ClientToScreen(Point(Left, Top)) do
      PopupMenu.Popup(X - Left, Y - Top + Height);
  end;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.pmTimeFromToClick(Sender: TObject);
begin
  with TMenuItem(Sender) do begin
    if Tag = 0 then begin
      mTimeFromExt := StrToTime(Caption);
      OnSelectionClick(tpStart);
    end else begin
      mTimeToExt := StrToTime(Caption);
      OnSelectionClick(tpStart);
    end;
  end;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.cobFixedTimeSelectionChange(Sender: TObject);
var
  xFrom: TDateTime;
  xTo: TDateTime;
begin
  with cobFixedTimeSelection do begin
    if ItemIndex = 0 then begin
      laResetTimeRangeClick(tpStart);
    end else begin
      ResetTimeRange(False);
      GetDateTimeFromFixedTimeRange(TFixedTimeRange(Items.Objects[ItemIndex]), xFrom, xTo);
      TimeFrom := xFrom;
      TimeTo   := xTo;
      OnSelectionClick(cobFixedTimeSelection);
    end;
  end;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.pnTimeRangeResize(Sender: TObject);
begin
  if pnTimePicker.Visible then
    pnTimePicker.Height := pnTimeRange.Height - laTimeRange.Height;
end;
//:---------------------------------------------------------------------------
procedure TMMUSBSelectNavigator.PaintBoxDividerPaint(Sender: TObject);
var
  y: Integer;
begin
  y := 0;
  with TmmPaintBox(Sender) do begin
    if TControl(Sender).Tag = 1 then begin
      Canvas.Pen.Color := clWhite;
      Canvas.Polyline([Point(0,y), Point(Width,y)]);
      inc(y);
    end;
    Canvas.Pen.Color := clDkGray;
    Canvas.Polyline([Point(0,y), Point(Width,y)]);
    inc(y);
    Canvas.Pen.Color := cl3DDkShadow;
    Canvas.Polyline([Point(0,y), Point(Width,y)]);
  end;
end;
//:---------------------------------------------------------------------------
function TMMUSBSelectNavigator.GetNameFromID(aID: Integer; aSource: TSourceSelection): String;
var
  i: Integer;
  xLB: TDBVisualStringList;
begin
  Result := '';
  case aSource of
    ssStyle: xLB := lbStyle;
    ssLot:   xLB := lbLot;
    ssMach:  xLB := lbMachine;
    ssYMSet: xLB := lbYMSettings;
  else
    Exit;
  end;

  for i:=1 to xLB.RowCount-1 do begin
    if Integer(xLB.Objects[0, i]) = aID then begin
      Result := xLB.Cells[0, i];
      Break;
    end;
  end;
end;

end.

