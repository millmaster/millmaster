(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: regMMU.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Registers the components in different pages from different units
| Info..........: -
| Develop.system: Windows 2000
| Target.system.: Windows 2000
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 14.08.2000  1.00  Wss | Initial Release
| 06.02.2003  1.01  Nue | UEditIPAddr added, for IPaddress check in edit field.
|=========================================================================================*)
unit regMMU;

interface
//------------------------------------------------------------------------------
procedure MMRegister;
//------------------------------------------------------------------------------
implementation // 15.07.2002 added mmMBCS to imported units
{$R MMHtmlHelp.dcr}
{$R SettingsIniDB.dcr}
//{$R MMSelectNavigatorFrame.dcr}
{$R QRPrintGrid.dcr}
{$R PrintSetupFrame.dcr}
//{$R ArrowButton.dcr}
{$R mmNumEdit.dcr}
{$R SizingPanel.dcr}

uses
  mmMBCS,

  Classes, DsgnIntf, Controls,
  MMHtmlHelp,
  mmVCLStringList,
  SettingsIniDB,
//  MMSelectNavigatorFrame,
  MMUSBSelectNavigatorFrame,  //Nue:9.7.2002
  QRPrintGrid,
  PrintSetupFrame,
  MachNodeComp,
//  ArrowButton,
  mmNumCtrl,
  mmDbValCb,
  SizingPanel,
  DBStringGrid,
  UEditIPAddr,
  StyleUserFields,
  RepSettingsUnit,
  MMQualityMatrixClass; //SDO 12.01.2004;
//------------------------------------------------------------------------------
procedure MMRegister;
begin
  // Unit MMHtmlHelp
  RegisterComponents('MillMaster', [TMMHtmlHelp]);
  RegisterComponents('MillMaster', [TmmVCLStringList]);
  // Unit SettingsIniDB, StyleUserFields
  RegisterComponents('MillMaster', [TSettingsIniDB, TStyleUserFields]);
  // Unit MMSelectNavigatorFrame
//  RegisterComponents('MillMaster', [TMMSelectNavigator]);
  // Unit MMUSBSelectNavigatorFrame   Nue:9.7.2002
  RegisterComponents('MillMaster', [TMMUSBSelectNavigator]);
  // Unit QRPrintGrid
  RegisterComponents('MillMaster', [TQRPrintDBGrid, TQRPrintStringGrid]);
  // Unit PrintSetupFrame
  RegisterComponents('MillMaster', [TFramePrintSetup]);
  // Unit MachNodeComp
  RegisterComponents('MaConfig', [TAdminMachNode, TBaseEdit, TBaseComboBox, TBaseCheckBox,TBaseEditIPAddr]);
  // Unit ArrowButton
//  RegisterComponents('MillMaster', [TArrowButton]);
  // Unit mmNumCtrl
  RegisterComponents('MillMaster', [TmmNumEdit]);
  // Unit mmDbValCb
  RegisterComponents('MillMaster', [TmmDBValComboBox]);
  // Unit SizingPanel
  RegisterComponents('MillMaster', [TSizingPanel]);
  // Unit DBStringGrid
  RegisterComponents('MillMaster', [TDBStringGrid]);
  // Unit UEditIPAddr
  RegisterComponents('MillMaster', [CEditIPAddr]);
  // Unit RepSettingsUnit
  RegisterComponents('MillMaster', [TRepSettings]);
  // Unit MMQualityMatrixClass
  RegisterComponents('MillMaster', [TMMQualityMatrix]);
end;
//------------------------------------------------------------------------------
end.
