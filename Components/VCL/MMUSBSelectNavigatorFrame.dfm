object MMUSBSelectNavigator: TMMUSBSelectNavigator
  Left = 0
  Top = 0
  Width = 792
  Height = 483
  Color = clBtnShadow
  ParentColor = False
  TabOrder = 0
  OnResize = FrameResize
  object spTimeRange: TmmSplitter
    Left = 460
    Top = 0
    Width = 5
    Height = 483
    Cursor = crHSplit
    AutoSnap = False
    Beveled = True
    MinSize = 10
    ResizeStyle = rsUpdate
  end
  object spClearSet: TmmSplitter
    Left = 637
    Top = 0
    Width = 5
    Height = 483
    Cursor = crHSplit
    Align = alRight
    AutoSnap = False
    Beveled = True
    MinSize = 10
    ResizeStyle = rsUpdate
  end
  object spLot: TmmSplitter
    Left = 150
    Top = 0
    Width = 5
    Height = 483
    Cursor = crHSplit
    AutoSnap = False
    Beveled = True
    MinSize = 10
    ResizeStyle = rsUpdate
  end
  object spMachine: TmmSplitter
    Left = 305
    Top = 0
    Width = 5
    Height = 483
    Cursor = crHSplit
    AutoSnap = False
    Beveled = True
    MinSize = 10
    ResizeStyle = rsUpdate
  end
  object pnStyle: TmmPanel
    Left = 0
    Top = 0
    Width = 150
    Height = 483
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 0
    object laStyle: TmmLabel
      Left = 3
      Top = 3
      Width = 144
      Height = 14
      Align = alTop
      AutoSize = False
      Caption = '(20)Artikel'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelectedStyle: TmmLabel
      Left = 3
      Top = 17
      Width = 144
      Height = 60
      Align = alTop
      AutoSize = False
      Caption = '*'
      Color = clInfoBk
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pnStyleSelection: TmmPanel
      Left = 3
      Top = 77
      Width = 144
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object pnStyleFilter: TmmPanel
        Left = 0
        Top = 382
        Width = 144
        Height = 21
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object edStyleFilter: TmmEdit
          Left = 0
          Top = 0
          Width = 144
          Height = 21
          Hint = '(*)Geben Sie hier ihre Filterwerte ein'
          Anchors = [akLeft, akTop, akRight]
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGrayText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnChange = edFilterChange
          AutoLabel.LabelPosition = lpLeft
          Decimals = -1
          InfoText = '(*)Filterkriterium eingeben'
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
      end
      object lbStyle: TDBVisualStringList
        Left = 0
        Top = 0
        Width = 144
        Height = 382
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 1000
        FixedRows = 0
        Options = [goRangeSelect, goThumbTracking]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        DataField = 'c_style_name'
        DataSource = dsStyle
        DefaultItems = '*'
        KeyField = 'c_style_id'
        OnBeforeFill = ListBoxBeforeFill
        OnSelect = OnSelectionClick
        Sorted = True
        ColWidths = (
          124
          0)
      end
    end
  end
  object pnLot: TmmPanel
    Left = 155
    Top = 0
    Width = 150
    Height = 483
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 1
    object laLot: TmmLabel
      Left = 3
      Top = 3
      Width = 144
      Height = 14
      Align = alTop
      AutoSize = False
      Caption = '(20)Partie'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelectedLot: TmmLabel
      Left = 3
      Top = 17
      Width = 144
      Height = 60
      Align = alTop
      AutoSize = False
      Caption = '*'
      Color = clInfoBk
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pnLotSelection: TmmPanel
      Left = 3
      Top = 77
      Width = 144
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object lbLot: TDBVisualStringList
        Tag = 1
        Left = 0
        Top = 0
        Width = 144
        Height = 382
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 15
        Enabled = False
        FixedCols = 0
        RowCount = 1000
        FixedRows = 0
        GridLineWidth = 0
        Options = [goRangeSelect, goThumbTracking]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        DataField = 'c_prod_name'
        DataSource = dsLot
        DefaultItems = '*'
        KeyField = 'c_prod_id'
        OnBeforeFill = ListBoxBeforeFill
        OnSelect = OnSelectionClick
        ColWidths = (
          124
          0)
      end
      object pnLotFilter: TmmPanel
        Left = 0
        Top = 382
        Width = 144
        Height = 21
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object edLotFilter: TmmEdit
          Left = 0
          Top = 0
          Width = 144
          Height = 21
          Hint = '(*)Geben Sie hier ihre Filterwerte ein'
          Anchors = [akLeft, akTop, akRight]
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGrayText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnChange = edFilterChange
          AutoLabel.LabelPosition = lpLeft
          Decimals = -1
          InfoText = '(*)Filterkriterium eingeben'
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
      end
    end
  end
  object pnMachine: TmmPanel
    Left = 310
    Top = 0
    Width = 150
    Height = 483
    Align = alLeft
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 2
    object laMachine: TmmLabel
      Left = 3
      Top = 3
      Width = 144
      Height = 14
      Align = alTop
      AutoSize = False
      Caption = '(20)Maschine'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelectedMach: TmmLabel
      Left = 3
      Top = 17
      Width = 144
      Height = 60
      Align = alTop
      AutoSize = False
      Caption = '*'
      Color = clInfoBk
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pnMachineSelection: TmmPanel
      Left = 3
      Top = 77
      Width = 144
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object lbMachine: TDBVisualStringList
        Tag = 2
        Left = 0
        Top = 0
        Width = 144
        Height = 382
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 15
        FixedCols = 0
        RowCount = 1000
        FixedRows = 0
        GridLineWidth = 0
        Options = [goRangeSelect, goThumbTracking]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        DataField = 'c_machine_name'
        DataSource = dsMachine
        DefaultItems = '*'
        KeyField = 'c_machine_id'
        OnBeforeFill = ListBoxBeforeFill
        OnSelect = OnSelectionClick
        Sorted = True
        ColWidths = (
          124
          0)
      end
      object pnMachineFilter: TmmPanel
        Left = 0
        Top = 382
        Width = 144
        Height = 21
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object edMachineFilter: TmmEdit
          Left = 0
          Top = 0
          Width = 144
          Height = 21
          Hint = '(*)Geben Sie hier ihre Filterwerte ein'
          Anchors = [akLeft, akTop, akRight]
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGrayText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnChange = edFilterChange
          AutoLabel.LabelPosition = lpLeft
          Decimals = -1
          InfoText = '(*)Filterkriterium eingeben'
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
      end
    end
  end
  object pnTimeRange: TmmPanel
    Left = 465
    Top = 0
    Width = 172
    Height = 483
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 3
    OnResize = pnTimeRangeResize
    object laTimeRange: TmmLabel
      Left = 3
      Top = 3
      Width = 166
      Height = 14
      Align = alTop
      AutoSize = False
      Caption = '0'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelectedTimeRange: TmmLabel
      Left = 3
      Top = 17
      Width = 166
      Height = 60
      Align = alTop
      AutoSize = False
      Caption = '*'
      Color = clInfoBk
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pnTimePicker: TmmPanel
      Left = 3
      Top = 77
      Width = 166
      Height = 236
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      Visible = False
      object pnDateSelection: TmmPanel
        Left = 0
        Top = 0
        Width = 166
        Height = 142
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object laVon: TmmLabel
          Left = 4
          Top = 48
          Width = 41
          Height = 13
          AutoSize = False
          Caption = '(6)Von:'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object laBis: TmmLabel
          Left = 4
          Top = 86
          Width = 41
          Height = 13
          AutoSize = False
          Caption = '(6)Bis:'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mmLabel1: TmmLabel
          Left = 4
          Top = 8
          Width = 145
          Height = 13
          AutoSize = False
          Caption = '(*)Feste Zeitbereiche'
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object bTimeFromShifts: TmmSpeedButton
          Left = 121
          Top = 62
          Width = 21
          Height = 21
          Caption = '...'
          PopupMenu = pmTimeFrom
          Visible = True
          OnClick = bTimeFromToShiftsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object bTimeToShifts: TmmSpeedButton
          Tag = 1
          Left = 121
          Top = 100
          Width = 21
          Height = 21
          Caption = '...'
          PopupMenu = pmTimeTo
          Visible = True
          OnClick = bTimeFromToShiftsClick
          AutoLabel.LabelPosition = lpLeft
        end
        object mTopPaintBox: TmmPaintBox
          Left = 0
          Top = 0
          Width = 166
          Height = 2
          Align = alTop
          Visible = True
          OnPaint = PaintBoxDividerPaint
          AutoLabel.LabelPosition = lpLeft
        end
        object laResetTimeRange: TmmLabel
          Left = 3
          Top = 127
          Width = 143
          Height = 13
          Cursor = crHandPoint
          Alignment = taCenter
          Caption = '(25)Zeitbereich zuruecksetzen'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clMaroon
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsUnderline]
          ParentFont = False
          Visible = True
          OnClick = laResetTimeRangeClick
          AutoLabel.LabelPosition = lpLeft
        end
        object tpStart: TwwDBDateTimePicker
          Tag = 3
          Left = 4
          Top = 62
          Width = 113
          Height = 21
          Hint = '(6)Von:'
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.Options = [mdoDayState, mdoWeekNumbers]
          CalendarAttributes.PopupYearOptions.NumberColumns = 1
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          CalendarAttributes.FirstDayOfWeek = wwdowMonday
          DateFormat = dfLong
          Date = 37417
          Epoch = 1950
          ShowButton = True
          TabOrder = 0
          UnboundDataType = wwDTEdtDate
          DisplayFormat = 'ddd, d. MMM yyyy'
          OnCalcBoldDay = tpStartEndCalcBoldDay
          OnCloseUp = OnSelectionClick
          OnChange = tpStartEndChange
          OnEnter = tpStartEnter
        end
        object tpEnd: TwwDBDateTimePicker
          Tag = 3
          Left = 4
          Top = 100
          Width = 113
          Height = 21
          Hint = '(6)Bis:'
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          CalendarAttributes.Options = [mdoDayState, mdoWeekNumbers]
          CalendarAttributes.PopupYearOptions.NumberColumns = 1
          CalendarAttributes.PopupYearOptions.StartYear = 2000
          CalendarAttributes.FirstDayOfWeek = wwdowMonday
          DateFormat = dfLong
          Date = 37412
          Epoch = 1950
          ShowButton = True
          TabOrder = 1
          UnboundDataType = wwDTEdtDate
          DisplayFormat = 'ddd, d. MMM yyyy'
          OnCalcBoldDay = tpStartEndCalcBoldDay
          OnCloseUp = OnSelectionClick
          OnChange = tpStartEndChange
          OnEnter = tpStartEnter
        end
        object cobFixedTimeSelection: TmmComboBox
          Tag = 3
          Left = 4
          Top = 22
          Width = 141
          Height = 21
          Style = csDropDownList
          Color = clWindow
          ItemHeight = 13
          TabOrder = 2
          Visible = True
          OnChange = cobFixedTimeSelectionChange
          AutoLabel.LabelPosition = lpLeft
          Edit = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
        end
      end
      object pnTimeRangeBottom: TmmPanel
        Left = 0
        Top = 208
        Width = 166
        Height = 28
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object laMaxTimeRange: TmmLabel
          Left = 4
          Top = 6
          Width = 8
          Height = 13
          Caption = '0'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
        object mBottomPaintBox: TmmPaintBox
          Tag = 1
          Left = 0
          Top = 0
          Width = 166
          Height = 3
          Align = alTop
          Color = clBtnFace
          ParentColor = False
          Visible = True
          OnPaint = PaintBoxDividerPaint
          AutoLabel.LabelPosition = lpLeft
        end
      end
      object pnTimeRangeInfo: TmmPanel
        Left = 0
        Top = 192
        Width = 166
        Height = 16
        Align = alBottom
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 2
        object laTimeRangeInfo: TmmLabel
          Left = 4
          Top = 0
          Width = 130
          Height = 13
          Caption = '(25)Verfuegbare Daten'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = True
          AutoLabel.LabelPosition = lpLeft
        end
      end
    end
    object pnTimeList: TmmPanel
      Left = 3
      Top = 313
      Width = 166
      Height = 167
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object pnTimeFilter: TmmPanel
        Left = 0
        Top = 146
        Width = 166
        Height = 21
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object edTimeFilter: TmmEdit
          Left = 0
          Top = 0
          Width = 166
          Height = 21
          Hint = '(*)Geben Sie hier ihre Filterwerte ein'
          Anchors = [akLeft, akTop, akRight]
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGrayText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnChange = edFilterChange
          AutoLabel.LabelPosition = lpLeft
          Decimals = -1
          InfoText = '(*)Filterkriterium eingeben'
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
      end
      object lbTime: TDBVisualStringList
        Tag = 3
        Left = 0
        Top = 0
        Width = 166
        Height = 146
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 15
        Enabled = False
        FixedCols = 0
        RowCount = 1000
        FixedRows = 0
        Options = [goRangeSelect, goThumbTracking]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        DataField = 'c_time'
        DataSource = dsTimeRange
        DefaultItems = '*'
        KeyField = 'c_time_id'
        OnBeforeFill = ListBoxBeforeFill
        OnSelect = OnSelectionClick
        ColWidths = (
          146
          0)
      end
    end
  end
  object pnClearSet: TmmPanel
    Left = 642
    Top = 0
    Width = 150
    Height = 483
    Align = alRight
    BevelOuter = bvNone
    BorderWidth = 3
    TabOrder = 4
    object laYMSettings: TmmLabel
      Left = 3
      Top = 3
      Width = 144
      Height = 14
      Align = alTop
      AutoSize = False
      Caption = '(20)Reinigereinstellungen'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      Layout = tlCenter
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object laSelectedYMSettings: TmmLabel
      Left = 3
      Top = 17
      Width = 144
      Height = 60
      Align = alTop
      AutoSize = False
      Caption = '*'
      Color = clInfoBk
      ParentColor = False
      ParentShowHint = False
      ShowHint = True
      Visible = True
      WordWrap = True
      AutoLabel.LabelPosition = lpLeft
    end
    object pnYMSettingsSelection: TmmPanel
      Left = 3
      Top = 77
      Width = 144
      Height = 403
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object lbYMSettings: TDBVisualStringList
        Tag = 4
        Left = 0
        Top = 0
        Width = 144
        Height = 382
        Align = alClient
        ColCount = 2
        DefaultRowHeight = 15
        Enabled = False
        FixedCols = 0
        RowCount = 1
        FixedRows = 0
        Options = [goRangeSelect, goThumbTracking]
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        DataField = 'c_YM_set_name'
        DataSource = dsYMSettings
        DefaultItems = '*'
        KeyField = 'c_YM_set_id'
        OnBeforeFill = ListBoxBeforeFill
        OnSelect = OnSelectionClick
        ColWidths = (
          140
          0)
      end
      object pnYMSetFilter: TmmPanel
        Left = 0
        Top = 382
        Width = 144
        Height = 21
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        object edYMSetFilter: TmmEdit
          Left = 0
          Top = 0
          Width = 144
          Height = 21
          Hint = '(*)Geben Sie hier ihre Filterwerte ein'
          Anchors = [akLeft, akTop, akRight]
          Color = clWindow
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGrayText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Visible = True
          OnChange = edFilterChange
          AutoLabel.LabelPosition = lpLeft
          Decimals = -1
          InfoText = '(*)Filterkriterium eingeben'
          NumMode = False
          ReadOnlyColor = clInfoBk
          ShowMode = smNormal
          ValidateMode = [vmInput]
        end
      end
    end
  end
  object dsStyle: TmmDataSource
    DataSet = dseStyle
    Left = 32
    Top = 168
  end
  object dsLot: TmmDataSource
    DataSet = dseLot
    Left = 192
    Top = 168
  end
  object dsTimeRange: TmmDataSource
    DataSet = dseTimeRange
    Left = 528
    Top = 400
  end
  object dsMachine: TmmDataSource
    DataSet = dseMachine
    Left = 344
    Top = 168
  end
  object dsYMSettings: TmmDataSource
    DataSet = dseYMSettings
    Left = 712
    Top = 168
  end
  object thStyle: TVCLThread
    AutoSuspend = True
    FreeOnTerminate = False
    OnExecution = thExecution
    Left = 96
    Top = 168
  end
  object thLot: TVCLThread
    Tag = 1
    AutoSuspend = True
    FreeOnTerminate = False
    OnExecution = thExecution
    Left = 251
    Top = 168
  end
  object thMachine: TVCLThread
    Tag = 2
    AutoSuspend = True
    FreeOnTerminate = False
    OnExecution = thExecution
    Left = 406
    Top = 160
  end
  object thTime: TVCLThread
    Tag = 3
    AutoSuspend = True
    FreeOnTerminate = False
    OnExecution = thExecution
    Left = 561
    Top = 400
  end
  object thYMSettings: TVCLThread
    Tag = 4
    AutoSuspend = True
    FreeOnTerminate = False
    OnExecution = thExecution
    Left = 776
    Top = 160
  end
  object dseStyle: TmmADODataSet
    Connection = conStyle
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    ExecuteOptions = [eoAsyncFetchNonBlocking]
    LockType = ltReadOnly
    BeforeOpen = dseBeforeOpen
    CommandText = 
      'select distinct'#13#10'  c_style_id,'#13#10'  c_style_name'#13#10#13#10'from '#13#10'  t_pro' +
      'dgroup pg,'#13#10'  t_dw_prodgroup_shift pgs,'#13#10'  t_shift_fragshift sfs' +
      ','#13#10'  t_shift sh'#13#10#13#10'where pg.c_prod_id = pgs.c_prod_id'#13#10'and pgs.c' +
      '_fragshift_id = sfs.c_fragshift_id'#13#10'and sfs.c_shift_id = sh.c_sh' +
      'ift_id'#13#10'and sh.c_shiftcal_id = %d'#13#10
    CommandTimeout = 60
    Parameters = <>
    Prepared = True
    Left = 32
    Top = 136
  end
  object dseMachine: TmmADODataSet
    Tag = 2
    Connection = conMachine
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    ExecuteOptions = [eoAsyncFetchNonBlocking]
    LockType = ltReadOnly
    BeforeOpen = dseBeforeOpen
    CommandText = 
      'select distinct'#13#10'  c_machine_name,'#13#10'  c_machine_id'#13#10#13#10'from'#13#10'  t_' +
      'prodgroup pg,'#13#10'  t_dw_prodgroup_shift pgs,'#13#10'  t_shift_fragshift ' +
      'sfs,'#13#10'  t_shift sh'#13#10#13#10'where pg.c_prod_id = pgs.c_prod_id'#13#10'and pg' +
      's.c_fragshift_id = sfs.c_fragshift_id'#13#10'and sfs.c_shift_id = sh.c' +
      '_shift_id'#13#10'and sh.c_shiftcal_id = %d'#13#10
    CommandTimeout = 60
    Parameters = <>
    Prepared = True
    Left = 344
    Top = 136
  end
  object dseLot: TmmADODataSet
    Tag = 1
    Connection = conLot
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    ExecuteOptions = [eoAsyncFetchNonBlocking]
    LockType = ltReadOnly
    BeforeOpen = dseBeforeOpen
    CommandText = 
      'select distinct'#13#10'  c_prod_id,'#13#10'  c_prod_name'#13#10#13#10'from '#13#10'  v_produ' +
      'ction_shift'#13#10#13#10'where c_shiftcal_id = 1'#13#10
    CommandTimeout = 60
    Parameters = <>
    Prepared = True
    Left = 184
    Top = 136
  end
  object dseTimeRange: TmmADODataSet
    Tag = 3
    Connection = conTime
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    ExecuteOptions = [eoAsyncFetchNonBlocking]
    LockType = ltReadOnly
    BeforeOpen = dseBeforeOpen
    CommandText = 
      'select distinct'#13#10'  sh.c_shift_id,'#13#10'  sh.c_shift_start'#13#10#13#10'from'#13#10' ' +
      ' t_prodgroup pg,'#13#10'  t_dw_prodgroup_shift pgs,'#13#10'  t_shift_fragshi' +
      'ft sfs,'#13#10'  t_shift sh'#13#10#13#10'where pg.c_prod_id = pgs.c_prod_id'#13#10'and' +
      ' pgs.c_fragshift_id = sfs.c_fragshift_id'#13#10'and sfs.c_shift_id = s' +
      'h.c_shift_id'#13#10'and sh.c_shiftcal_id = 1'#13#10
    CommandTimeout = 60
    Parameters = <>
    Prepared = True
    Left = 528
    Top = 368
  end
  object dseYMSettings: TmmADODataSet
    Tag = 4
    Connection = conYMSettings
    CursorLocation = clUseServer
    CursorType = ctOpenForwardOnly
    ExecuteOptions = [eoAsyncFetchNonBlocking]
    LockType = ltReadOnly
    BeforeOpen = dseBeforeOpen
    CommandText = 
      'select distinct'#13#10'  yms.c_YM_set_id,'#13#10'  yms.c_YM_set_name'#13#10#13#10'from' +
      #13#10'  t_xml_ym_settings yms,'#13#10'  t_prodgroup pg,'#13#10'  t_dw_prodgroup_' +
      'shift pgs,'#13#10'  t_shift_fragshift sfs,'#13#10'  t_shift sh'#13#10#13#10'where yms.' +
      'c_YM_set_id = pg.c_YM_set_id'#13#10'and pg.c_prod_id = pgs.c_prod_id'#13#10 +
      'and pgs.c_fragshift_id = sfs.c_fragshift_id'#13#10'and sfs.c_shift_id ' +
      '= sh.c_shift_id'#13#10'and sh.c_shiftcal_id = 1'#13#10
    CommandTimeout = 60
    Parameters = <>
    Prepared = True
    Left = 712
    Top = 136
  end
  object conStyle: TmmADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP-01;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 32
    Top = 104
  end
  object conLot: TmmADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP-01;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 192
    Top = 104
  end
  object conMachine: TmmADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP-01;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 344
    Top = 104
  end
  object conTime: TmmADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP-01;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 528
    Top = 336
  end
  object conYMSettings: TmmADOConnection
    CommandTimeout = 60
    Connected = True
    ConnectionString = 
      'Provider=SQLOLEDB.1;Password=netpmek32;User ID=MMSystemSQL;Initi' +
      'al Catalog=MM_Winding;Data Source=VM-MMDEVELOP;Use Procedure for' +
      ' Prepare=1;Auto Translate=False;Packet Size=4096;Workstation ID=' +
      'VM-MMDEVELOP-01;Use Encryption for Data=False;Tag with column co' +
      'llation when possible=False'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 712
    Top = 104
  end
  object conDefault: TmmADOConnection
    CommandTimeout = 60
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=VM-MMDEVELOP;Initial Catalog=MM_' +
      'Winding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=Fa' +
      'lse'
    CursorLocation = clUseServer
    LoginPrompt = False
    Mode = cmRead
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 32
    Top = 48
  end
  object timUpdateFilter: TmmTimer
    Enabled = False
    Interval = 300
    OnTimer = timUpdateFilterTimer
    Left = 80
    Top = 48
  end
  object mColumnNames: TmmVCLStringList
    Strings.Strings = (
      'distinct v.c_style_id, v.c_style_name'
      'distinct v.c_prod_id, v.c_prod_name'
      'distinct v.c_machine_id, v.c_machine_name'
      
        '// will be set at runtime depending on TimeMode and UseTimeList ' +
        'property'
      'distinct v.c_YM_set_id, v.c_YM_set_name'
      ' ')
    Left = 203
    Top = 24
  end
  object mOrderByArr: TmmVCLStringList
    Strings.Strings = (
      'order by v.c_style_name'
      'order by v.c_prod_name'
      'order by v.c_machine_name'
      
        '// will be set at runtime depending on TimeMode and UseTimeList ' +
        'property'
      'order by v.c_YM_set_name')
    Left = 267
    Top = 24
  end
  object mWhereIDMaskArr: TmmVCLStringList
    Strings.Strings = (
      'and v.c_style_id in (%s) '
      'and v.c_prod_id in (%s) '
      'and v.c_machine_id in (%s) '
      
        '// will be set at runtime depending on TimeMode and UseTimeList ' +
        'property'
      'and v.c_YM_set_id in (%s) ')
    Left = 235
    Top = 24
  end
  object pmTimeFrom: TmmPopupMenu
    AutoHotkeys = maManual
    AutoPopup = False
    Left = 564
    Top = 133
  end
  object pmTimeTo: TmmPopupMenu
    Tag = 1
    AutoHotkeys = maManual
    AutoPopup = False
    Left = 564
    Top = 173
  end
end
