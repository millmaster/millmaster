(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DBStringGrid.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Shows values from database in a StringGrid
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 05.09.2002  1.00  Wss | Initial Release
| 27.11.2002  1.00  Wss | Diverse Verbesserungen
| 27.03.2003  1.00  Wss | Bug in ScanDataset behoben
| 23.05.2003  1.00  Wss | In MouseDown nur auf LeftMouse reagieren
| 28.11.2003        Lok | Instrumentierung aus TVisualControlDBLink.ActiveChanged entfernt
| 01.12.2003        Wss | TitleAlignment wird pauschal aus normalem Alignment �bernommen (ScanDataset)
| 03.12.2003        Lok | Memory Leak entfernt. Im Destruktor wurde 'ClearAll()' nicht aufgerufen. Dadurch
|                       |   entstand ein Memory Leak da die Instanzen von TColumn nicht freigegeben wurden.
| 05.12.2003        Wss | Anpassungen f�r Summen-Detail Modus: KeyField und ValueField (neu) getrenntes handling
| 14.01.2004        Wss | ClearAll in Destroy wieder entfernt da Problem im ExpandMode
                          -> TColumns werden nun manuell im Destroy freigegeben
| 03.05.2005        Wss | Bugfix: wenn Dataset leer war, wurde dir RowCount = 1 und somit waren keine FixedRows mehr
                          vorhanden
|=========================================================================================*)
unit DBStringGrid;

interface

uses
  SysUtils, Windows, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DB, Grids, mmStringGrid;

type
  TDBStringGrid = class;
  
  IFillVisualControl = interface (IUnknown)
    procedure FillValues(aActive: Boolean);
  end;
  
  TVisualControlDBLink = class (TDataLink)
  private
    mControl: IFillVisualControl;
  protected
    procedure ActiveChanged; override;
  public
    constructor Create(aControl: IFillVisualControl);
  end;
  
  TExpandInfo = class (TPersistent)
  private
    fColor: TColor;
    fDataDone: Boolean;
    fDetailGrid: TDBStringGrid;
    fExpandable: Boolean;
    fExpanded: Boolean;
    fKeyField: string;
    fKeyValueX: Variant;
    fLevel: Integer;
    fSQLWhere: string;
    mOwnerGrid: TDBStringGrid;
    fValueField: String;
    function GetDetailGrid: TDBStringGrid;
    function GetSQLWhere: string;
    procedure SetKeyField(Value: string);
    procedure SetLevel(Value: Integer);
  protected
    procedure AssignTo(Dest: TPersistent); override;
  public
    constructor Create(aOwner: TDBStringGrid);
    destructor Destroy; override;
    function GetExpandedRowCount: Integer;
    procedure ResetExpanded;
    property Color: TColor read fColor write fColor;
    property DataDone: Boolean read fDataDone write fDataDone;
    property DetailGrid: TDBStringGrid read GetDetailGrid;
    property Expandable: Boolean read fExpandable write fExpandable;
    property Expanded: Boolean read fExpanded write fExpanded;
    property ValueField: String read fValueField write fValueField;
    property KeyField: string read fKeyField write SetKeyField;
    property KeyValueX: Variant read fKeyValueX write fKeyValueX;
    property Level: Integer read fLevel write SetLevel;
    property SQLWhere: string read GetSQLWhere write fSQLWhere;
  end;
  
  TColumnChangedEvent = procedure (Sender: TObject; aColIndex: Integer) of object;
  TColumn = class (TPersistent)
  private
    fAlignment: TAlignment;
    fField: TField;
    fFieldName: string;
    fHierarchyColor: TColor;
    fOnColumnChanged: TColumnChangedEvent;
    fTag: Integer;
    fTitleAlignment: TAlignment;
    fWidth: Integer;
    procedure SetTitleAlignment(Value: TAlignment);
    procedure SetWidth(Value: Integer);
//    procedure Update;
  protected
    property OnColumnChanged: TColumnChangedEvent read fOnColumnChanged write fOnColumnChanged;
  public
    constructor Create;
    procedure Assign(aSource: TPersistent); override; 
    property Alignment: TAlignment read fAlignment write fAlignment;
    property Field: TField read fField write fField;
    property FieldName: string read fFieldName write fFieldName;
    property HierarchyColor: TColor read fHierarchyColor write fHierarchyColor;
    property Tag: Integer read fTag write fTag;
    property TitleAlignment: TAlignment read fTitleAlignment write SetTitleAlignment;
    property Width: Integer read fWidth write SetWidth;
  end;
  
  TColumnMoveEvent = procedure (Sender: TObject; aFromIndex, aToIndex: Integer) of object;
  TExpandInfoEvent = procedure (Sender: TObject; aExpandInfo: TExpandInfo) of object;
  TFillDetailGridEvent = procedure (Sender: TObject; aDetailGrid: TDBStringGrid) of object;
  TGetColumnInfo = procedure (Sender: TObject; aColumn: TColumn) of object;
  TDBStringGrid = class (TmmStringGrid, IFillVisualControl)
  private
    fEnableDetails: Boolean;
    fKeepValues: Boolean;
    fKeyField: string;
    fLevel: Integer;
    fOnEvent: TGetColumnInfo;
    fOnFillDetailGrid: TFillDetailGridEvent;
    fOnGetColumnInfo: TGetColumnInfo;
    fOnGetExpandInfo: TExpandInfoEvent;
    fSQLWhere: string;
    fTitleFont: TFont;
    mDataLink: TVisualControlDBLink;
    mDrawBmp: TBitmap;
    procedure FontChanged(Sender: TObject);
    function GetCol: LongInt;
    function GetColumns(aCol: Integer): TColumn;
    function GetData(ACol, ARow: Integer): TObject;
    function GetDataRowCount: Integer;
    function GetDataSource: TDataSource;
    function GetExpandInfo(aRow: Integer): TExpandInfo;
    function GetValue(ACol, ARow: Integer): string;
    function IsDataSetActive: Boolean;
    procedure Proc_OnColumnChanged(Sender: TObject; aColIndex: Integer);
    procedure SetCol(Value: LongInt);
    procedure SetColumns(aCol: Integer; Value: TColumn);
    procedure SetData(ACol, ARow: Integer; Value: TObject);
    procedure SetDataSource(aValue: TDataSource);
    procedure SetEnableDetails(Value: Boolean);
    procedure SetKeyField(Value: string);
    procedure SetTitleFont(Value: TFont);
    procedure SetValue(ACol, ARow: Integer; const Value: string);
  protected
    procedure CollapseData(aRow: Integer);
//    function CreateNewDetailObject: TExpandInfo;
    procedure DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState); override;
    procedure ExpandData(aRow: Integer);
    procedure FillValues(aActive: Boolean);
    procedure InsertRows(aRow, aCount: Integer);
    procedure InternalClear(aStartCol, aStartRow: Integer);
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
    procedure RemoveRows(aRow, aCount: Integer);
    procedure ScanDataset;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AssignTo(Dest: TPersistent); override;
    procedure ClearAll;
    procedure ClearData;
    function Dataset: TDataSet;
    procedure InsertCols(aCol, aCount: Integer);
    procedure Loaded; override;
    procedure PrepareEnableDetails;
    procedure RemoveCols(aCol, aCount: Integer);
    property Col read GetCol write SetCol;
    property Columns[aCol: Integer]: TColumn read GetColumns write SetColumns;
    property Data[ACol, ARow: Integer]: TObject read GetData write SetData;
    property DataRowCount: Integer read GetDataRowCount;
    property ExpandInfo[aRow: Integer]: TExpandInfo read GetExpandInfo;
    property KeyField: string read fKeyField write SetKeyField;
    property Level: Integer read fLevel write fLevel;
    property SQLWhere: string read fSQLWhere write fSQLWhere;
    property Value[ACol, ARow: Integer]: string read GetValue write SetValue;
  published
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property EnableDetails: Boolean read fEnableDetails write SetEnableDetails;
    property KeepValues: Boolean read fKeepValues write fKeepValues;
    property OnEvent: TGetColumnInfo read fOnEvent write fOnEvent;
    property OnFillDetailGrid: TFillDetailGridEvent read fOnFillDetailGrid write fOnFillDetailGrid;
    property OnGetColumnInfo: TGetColumnInfo read fOnGetColumnInfo write fOnGetColumnInfo;
    property OnGetExpandInfo: TExpandInfoEvent read fOnGetExpandInfo write fOnGetExpandInfo;
    property TitleFont: TFont read fTitleFont write SetTitleFont;
  end;
  

procedure Register;

implementation
{$R DBStringGrid.res}
uses
  mmCS, ExtCtrls, LoepfeGlobal;

procedure Register;
begin
  RegisterComponents('Data Controls', [TDBStringGrid]);
end;

//:---------------------------------------------------------------------------
//:--- Class: TVisualControlDBLink
//:**********************************************************************
constructor TVisualControlDBLink.Create(aControl: IFillVisualControl);
begin
  inherited Create;
  mControl := aControl;
end;

//:**********************************************************************
procedure TVisualControlDBLink.ActiveChanged;
begin
  mControl.FillValues(Self.Active);
end;

//:---------------------------------------------------------------------------
//:--- Class: TExpandInfo
//:**********************************************************************
constructor TExpandInfo.Create(aOwner: TDBStringGrid);
begin
  inherited Create;
  
  fColor      := clWindow;
  fDetailGrid := Nil;
  fSQLWhere   := '';
  mOwnerGrid  := aOwner;
end;

//:**********************************************************************
destructor TExpandInfo.Destroy;
begin
  fDetailGrid.Free;
  
  inherited Destroy;
end;

//:**********************************************************************
procedure TExpandInfo.AssignTo(Dest: TPersistent);
var
  xDest: TExpandInfo;
begin
  if Dest is TExpandInfo then begin
    xDest := TExpandInfo(Dest);
    xDest.Color      := fColor;
    xDest.Expandable := fExpandable;
    xDest.Expanded   := fExpanded;
    xDest.KeyField   := fKeyField;
    xDest.Level      := fLevel;
    xDest.SQLWhere   := fSQLWhere;
    xDest.ValueField := fValueField;
  end else
    inherited AssignTo(Dest);
end;

//:**********************************************************************
function TExpandInfo.GetDetailGrid: TDBStringGrid;
begin
  // Das Detailgrid wird erst erstellt, wenn es ben�tigt wird
  if not Assigned(fDetailGrid) then begin
    fDetailGrid := TDBStringGrid.Create(Nil);
    // KeyField von dieser Zeile gilt f�r die Daten des Detailgrid
    // -> den Feldnamen weitergeben, damit in FillValues der KeyValue gelesen werden kann
  //  fDetailGrid.KeyField   := fKeyField;
    // N�chstes DetailGrid sind Daten f�r n�chste Stufe
    fDetailGrid.EnableDetails := True;
    fDetailGrid.KeepValues    := True;
    fDetailGrid.Level         := fLevel + 1;
    // Daten f�r das aktuelles Grid (OwnerGrid) wurden mit dieser SQLWhere Erweiterung geholt
    // Diese SQLWhere Anweisung nach unten weitergeben, damit diese dann weiter
    // erg�nzt werden
    fDetailGrid.SQLWhere      := fSQLWhere;
  end;
  
  Result := fDetailGrid;
end;

//:**********************************************************************
function TExpandInfo.GetExpandedRowCount: Integer;
var
  i: Integer;
  xInfo: TExpandInfo;
begin
  Result := 0;
  if Assigned(fDetailGrid) then begin
    Result := fDetailGrid.DataRowCount;
    for i:=1 to fDetailGrid.RowCount-1 do begin
      xInfo := TExpandInfo(fDetailGrid.Objects[0, i]);
      if Assigned(xInfo) then begin
        if xInfo.Expanded then
          inc(Result, xInfo.GetExpandedRowCount);
      end;
    end;
  end;
end;

//:**********************************************************************
function TExpandInfo.GetSQLWhere: string;
begin
  // Result := '';
  // if Assigned(mOwnerGrid) then
  //   // Es wird angenommen, dass beim OwnerGrid sicher 1 Zeile vorhanden ist, da man sonst
  //   // nicht hier sein k�nnte
  //   Result := mOwnerGrid.ExpandInfo[1].SQLWhereString;
  // // nun noch die eigene Anweisung hinten anh�ngen
  // Result := Result + fSQLWhere;
  Result := fSQLWhere;
end;

//:**********************************************************************
procedure TExpandInfo.ResetExpanded;
var
  i: Integer;
  xInfo: TExpandInfo;
begin
  if Assigned(fDetailGrid) then begin
    for i:=1 to fDetailGrid.RowCount-1 do begin
      xInfo := TExpandInfo(fDetailGrid.Objects[0, i]);
      if Assigned(xInfo) then begin
        xInfo.Expanded := False;
        xInfo.ResetExpanded;
      end;
    end;
  end;
end;

//:**********************************************************************
procedure TExpandInfo.SetKeyField(Value: string);
begin
  fKeyField := Trim(Value);
end;

//:**********************************************************************
procedure TExpandInfo.SetLevel(Value: Integer);
begin
  fLevel := Value;
  
  if Assigned(fDetailGrid) then
    fDetailGrid.Level := fLevel;
end;

//:---------------------------------------------------------------------------
//:--- Class: TColumn
//:**********************************************************************
constructor TColumn.Create;
begin
  inherited Create;
  
  fAlignment       := taLeftJustify;
  fField           := Nil;
  fTag             := 0;
  fOnColumnChanged := Nil;
  fTitleAlignment  := taLeftJustify;
  fWidth           := 0;
  fHierarchyColor  := clNone;
end;

//:**********************************************************************
procedure TColumn.Assign(aSource: TPersistent);
begin
  if aSource is TColumn then begin
    fAlignment      := TColumn(aSource).Alignment;
    fTitleAlignment := TColumn(aSource).TitleAlignment;
    fWidth          := TColumn(aSource).Width;
  end else
    inherited Assign(aSource);
end;

//:**********************************************************************
procedure TColumn.SetTitleAlignment(Value: TAlignment);
begin
  if fTitleAlignment <> Value then
  begin
    fTitleAlignment := Value;
  end;
end;

//:**********************************************************************
procedure TColumn.SetWidth(Value: Integer);
begin
  if fWidth <> Value then
  begin
    fWidth := Value;
  end;
end;

//:**********************************************************************
{
procedure TColumn.Update;
begin
  if Assigned(fOnColumnChanged) then
    fOnColumnChanged(Self, -1);
end;
{}

//:---------------------------------------------------------------------------
//:--- Class: TDBStringGrid
//:**********************************************************************
constructor TDBStringGrid.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  fTitleFont := TFont.Create;
  mDataLink := TVisualControlDBLink.Create(Self);
  mDrawBmp := TBitmap.Create;

  DefaultDrawing   := False;
  DefaultRowHeight := 17;
  ColCount  := 2;
  RowCount  := 2;
  FixedCols := 1;
  FixedRows := 1;
  ColWidths[0] := 10;

  //OnDrawCell := DrawCellLoc;

  //fColumns.OnColumnChanged := Proc_OnColumnChanged;
  fEnableDetails    := False;
  fKeepValues       := False;
  fOnFillDetailGrid := Nil;
  fOnGetExpandInfo  := Nil;
  fSQLWhere         := '';
  fTitleFont.OnChange := FontChanged;
end;

//:**********************************************************************
destructor TDBStringGrid.Destroy;
var
  i: Integer;
begin
//  ClearAll;
  for i:=FixedCols to ColCount-1 do begin
    if Assigned(Objects[i, 0]) then begin
      Objects[i, 0].Free;
      Objects[i, 0] := Nil;
    end;
  end;

  fTitleFont.Free;
  mDataLink.Free;
  mDrawBmp.Free;

  inherited Destroy;
end;

//:**********************************************************************
procedure TDBStringGrid.AssignTo(Dest: TPersistent);
var
  i: Integer;
  xDest: TDBStringGrid;
begin
  if Dest is TDBStringGrid then begin
    xDest := TDBStringGrid(Dest);
    xDest.ColCount := ColCount;
    xDest.RowCount := RowCount;
    for i:=0 to ColCount-1 do
      xDest.Cols[i] := Cols[i];
  end else
    inherited AssignTo(Dest);
end;

//:**********************************************************************
procedure TDBStringGrid.ClearAll;
var
  xCol: Integer;
begin
  for xCol:=0 to ColCount-1 do begin
    if Assigned(Objects[xCol, 0]) then begin
      Objects[xCol, 0].Free;
      Objects[xCol, 0] := Nil;
    end;
  end;

  InternalClear(0, 0);
  //fColumns.Clear;
  ColCount := FixedCols + 1;
  RowCount := FixedRows + 1;
end;

//:**********************************************************************
procedure TDBStringGrid.ClearData;
begin
  InternalClear(FixedCols, FixedRows);
end;

//:**********************************************************************
procedure TDBStringGrid.CollapseData(aRow: Integer);
begin
  // wird nur aufgerufen, wenn auch DetailDaten expanded sind
  // erst m�ssen bei allen Zeilen von den Detaildaten das Expanded Flag zur�cksetzen
  // for i:=aRow+1 to ExpandInfo[aRow].GetExpandedRowCount do
  //   ExpandInfo[i].Expanded := False;
  // anschliessend k�nnen diese Zeilen einfach entfernt werden
  RemoveRows(aRow+1, ExpandInfo[aRow].GetExpandedRowCount);
  ExpandInfo[aRow].ResetExpanded;
end;

//:**********************************************************************
{
function TDBStringGrid.CreateNewDetailObject: TExpandInfo;
begin
end;
{}

//:**********************************************************************
function TDBStringGrid.Dataset: TDataSet;
begin
  Result := Nil;
  if Assigned(mDatalink.DataSource) then
    if Assigned(mDatalink.DataSource.DataSet) then
      Result := mDataLink.DataSource.DataSet;
  
  if Result = Nil then
    Abort;
end;

//:**********************************************************************
procedure TDBStringGrid.DrawCell(ACol, ARow: Longint; ARect: TRect; AState: TGridDrawState);
var
  xInfo: TExpandInfo;
  X, Y: Integer;
  xAlign: TAlignment;
  xColor: TColor;
  
  procedure DrawText(aCanvas: TCanvas; aRect: TRect; aValue: string; aAlign: TAlignment);
  var
    xVertOffset: Integer;
    xHorzOffset: Integer;
  begin
    with aCanvas do begin
      xVertOffset := aRect.Top + (((aRect.Bottom - aRect.Top) - TextHeight(aValue)) div 2);
      xHorzOffset := TextWidth('Mi') div 4; // Yields rougly the width of
                                               // an "average" character.
      case aAlign of
        taLeftJustify: begin
          xHorzOffset := aRect.Left + xHorzOffset;
        end;
  
        taRightJustify: begin
          xHorzOffset := aRect.Left + (aRect.Right - aRect.Left) - TextWidth(aValue) - xHorzOffset;
        end;
  
        taCenter: begin
          xHorzOffset := aRect.Left + (((aRect.Right - aRect.Left) - TextWidth(aValue)) div 2);
        end;
      else
      end;
      TextRect(aRect, xHorzOffset, xVertOffset, aValue);
  
  {
      if fEnableDetails and Assigned(xInfo) then begin
        if (ACol > 0) and (ACol <= xInfo.Level) and (ARow = 0) then begin
          with aRect do begin
            xVertOffset  := (Bottom - Top - 5) div 2;
            Top    := Top + xVertOffset;
            Bottom := Bottom - xVertOffset;
            inc(Left);
            Right  := Left + 8;
          end;
          // Farbiges Rechteck definiert und zeichnen
          Brush.Color := xInfo.Color;
          Pen.Color   := clBlack;
          Rectangle(aRect);
        end;
      end;
  {}
    end; // with Canvas
  end;
  
begin
  xAlign := taLeftJustify;
  xInfo := Nil;
  
  xColor := Color;
  // Defaultaussehen verwenden
  if (csDesigning in ComponentState) or not IsDataSetActive then begin
    if gdFixed in aState then
      xColor := FixedColor;
    Canvas.Brush.Color := xColor;
    Canvas.FillRect(aRect);
  end else begin
    // im ExpandMode Info holen
    if fEnableDetails then
      xInfo := TExpandInfo(Objects[0, aRow]);
  
    // Fixed Cells default aussehen lassen
    if gdFixed in aState then begin
      xColor := FixedColor;
      if (aCol > 0) then begin
        xAlign := Columns[aCol].TitleAlignment;
        Canvas.Font := fTitleFont;
      end;
    end else begin
      // Eigenschaften/Darstellung f�r Datenzellen holen
      if (aCol > 0) then begin
        xAlign := Columns[aCol].Alignment;
        Canvas.Font := Font;
      end;
      if Assigned(xInfo) then begin
        if xInfo.Expandable then
          xColor := xInfo.Color;
      end;
    end;
    Canvas.Brush.Color := xColor;
    Canvas.FillRect(aRect);
    DrawText(Canvas, aRect, Cells[aCol, aRow], xAlign);
    // Nur bei der ersten Spalte Bitmap zeichnen
    if Assigned(xInfo) and (aCol = 0) then begin
    //   with mDrawBmp do begin
    //     if aRow = Row then begin
    //       LoadFromResourceName(HInstance, 'MARKER');
    //       Dormant;
    //       with aRect do begin
    //         X := Left;
    //         Y := Top + ((Bottom - Top - mDrawBmp.Height) div 2);
    //       end;
    //       Self.Canvas.Draw(X, Y, mDrawBmp);
    //     end;
    //   end;
    //   if fEnableDetails then begin
    //     xInfo := TExpandInfo(Objects[0, aRow]);
        if xInfo.Expandable then begin
          with mDrawBmp do begin
            if xInfo.Expanded then
              LoadFromResourceName(HInstance, 'COLLAPS')
            else
              LoadFromResourceName(HInstance, 'EXPAND');
            Dormant;
          end;
          with aRect do begin
            X := Right - mDrawBmp.Width;
            Y := Top + ((Bottom - Top - mDrawBmp.Height) div 2);
          end;
          Canvas.Draw(X, Y, mDrawBmp);
       end;
    end; // if Assigned(xInfo)
  end;
  
  if gdFixed in aState then begin
    Frame3D(Canvas, aRect, clWindow, clBtnShadow, 1);
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.ExpandData(aRow: Integer);
var
  i: Integer;
begin
  // wird nur aufgerufen, wenn Detaildaten Objekt vorhanden ist
  // TODO: Daten von Detaildaten in Grid einf�gen, Flag setzen
  if Assigned(fOnFillDetailGrid) then begin
    with ExpandInfo[aRow] do begin
      if not DataDone then begin
        // KeyField von dieser Zeile gilt f�r die Daten des Detailgrid
        // -> den Feldnamen weitergeben, damit in FillValues der KeyValue gelesen werden kann
  //      DetailGrid.KeyField := KeyField;
        // Die SQLWhere Anweisung nach unten weitergeben, damit diese dann weiter
        // erg�nzt werden
  //      DetailGrid.SQLWhere := SQLWhere;
        // Bei den DetailGrids m�ssen die externen Eventhandlers manuell gesetzt...
        // OnFillDetailGrid wird ben�tigt, weil das Dataset ausserhalb dieser Komponente
        // vorbereitet wird. Es wird tempor�r eine DataSource angeh�ngt und wieder entfernt.
        DetailGrid.OnFillDetailGrid := fOnFillDetailGrid;
        // W�rend dem Abf�llen m�ssen f�r die entsprechenden Datenzeilen wiederum ExpandInfo
        // Werte f�r die n�chste Detailstufe von aussen definiert werden.
        DetailGrid.OnGetExpandInfo  := fOnGetExpandInfo;
        // DataSet wird von aussen definiert, per Open Funktion wird schlussendlich die
        // interne FillValues() Methode aufgerufen
        fOnFillDetailGrid(Self, DetailGrid);
        // ...und wieder gel�scht werden, da diese ja nicht auf der Oberfl�che sichtbar sind
        DetailGrid.OnFillDetailGrid := Nil;
        DetailGrid.OnGetExpandInfo  := Nil;
        // Stellt sicher, dass die Daten nur 1x geholt werden
        DataDone := True;
      end;
      // Platz machen f�r neue Zeilen
      InsertRows(aRow+1, DetailGrid.DataRowCount);
      // und nun die Daten hinein kopieren
      for i:=1 to DetailGrid.RowCount-1 do begin
        Rows[aRow+i] := DetailGrid.Rows[i];
      end;
    end;
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.FillValues(aActive: Boolean);
  
  procedure LocalFillValues;
  var
    i: Integer;
    xCount: Integer;
    xInfo: TExpandInfo;
  begin
    xCount   := FixedRows;
//    RowCount := FixedRows + 1;
    if fEnableDetails and Assigned(fOnGetExpandInfo) then begin
      xInfo := TExpandInfo.Create(Self);
      // Dieses ExpandInfo geh�rt noch zu dieser Stufe/Grid -> Level von Grid �bernehmen
      xInfo.Level := Level;
      // von aussen m�ssen noch diverse Informationen wie
      // KeyField oder Expandable f�r diese Detailstufe gesetzt werden
      // SQLWhere und Level wird bei Expand an das neu erzeugte DetailGrid weitergegeben
      fOnGetExpandInfo(Self, xInfo);
      CodeSite.SendBoolean('Expandable', xInfo.Expandable);
      CodeSite.SendString('KeyField', xInfo.KeyField);
    end else
      xInfo := Nil;

    with Dataset do
    try
      DisableControls;
      while not EOF do
      try
        // f�r jede Datenzeile ein DetailInfo Objekt erstellen
        if Assigned(xInfo) then begin
          // wenn ExpandInfo vorhanden und ein Name f�r KeyField gesetzt wurde,
          // dann pro Zeile den Identifikationswert auslesen, mit SQLWhere von Grid
          // erg�nzen und in ExpandInfo-Objekt speichern.
          if xInfo.KeyField <> '' then begin
            with FieldByName(xInfo.ValueField) do begin
              // f�r jede Zeile die individuelle Where-Anweisung erstellen
              if DataType = ftString then
                xInfo.SQLWhere := fSQLWhere + Format(' AND (%s = ''%s'') ', [xInfo.KeyField, AsString])
              else if DataType in [ftSmallint, ftInteger, ftWord] then
                xInfo.SQLWhere := fSQLWhere + Format(' AND (%s = %d) ', [xInfo.KeyField, AsInteger])
              else if DataType in [ftDate, ftTime, ftDateTime] then
                xInfo.SQLWhere := fSQLWhere + Format(' AND (%s = ''%s'') ', [xInfo.KeyField, FormatDateTime(cUSADateMask, AsDateTime)]);
            end; // with
          end else // if KeyField
            xInfo.SQLWhere := fSQLWhere;

      CodeSite.SendString('LocalFillValues: SQLWhere', xInfo.SQLWhere);

          // nun ein eigenes ExpandInfo Objekt erstellen
          Objects[0, xCount] := TExpandInfo.Create(Self);
          // und Werte zuweisen
          TExpandInfo(Objects[0, xCount]).Assign(xInfo);
        end; // if Assigned(xInfo)

        for i:=1 to ColCount-1 do
          Cells[i, xCount] := FieldByName(Columns[i].FieldName).DisplayText;
        // n�chste Zeile
        inc(xCount);
        // Reserve schon aufgef�llt?
        if RowCount = xCount then
          // Ja -> neue Reserve erstellen
          RowCount := RowCount + 100;
        Next;
      except
        on e:Exception do
          CodeSite.SendError('LocalFillValues: ' + e.Message);
      end; // while
    finally
      // am Schluss noch die �bersch�ssigen Zeilen wieder entfernen
      if xCount > FixedRows then
        RowCount := xCount;
      EnableControls;
      xInfo.Free;
    end;
  end;

begin
  //  EnterMethod('TDBVisualStringList.FillItemList');
  if aActive then begin
  //  ScanDataset;
    ClearAll;
    if (([csDesigning, csDestroying] * ComponentState) = []) then begin
      ScanDataset;
      LocalFillValues;
    end;
  end else if not fKeepValues then
    ClearAll;
  
  // Enabled := aActive;
  // if (([csDesigning, csDestroying] * ComponentState) = []) then
  //   if aActive then begin
  //     xList := TmmStringList.Create;
  //     FillItemListProc(mDataLink.DataSource.DataSet, fDefaultItems, fDataField, fKeyField, xList, fSorted);
  //     Cols[0].Clear;
  //     Cols[1].Clear;
  //     if xList.Count > 0 then begin
  //       RowCount := xList.Count;
  //       Cols[0].Assign(xList);
  //     end else
  //       RowCount := 1;
  // //      CodeSite.SendStringList('xList', xList);
  //     xList.Free;
  //
  //     Row := 0;
  //     if Assigned(fOnAfterFill) then
  //       fOnAfterFill(Self);
  //   end;
end;

//:**********************************************************************
procedure TDBStringGrid.FontChanged(Sender: TObject);
begin
  ParentFont := False;
  //FDesktopFont := False;
  // if fTitelFont.Height <> FFontHeight then
  // begin
  //   Include(FScalingFlags, sfFont);
  //   FFontHeight := Font.Height;
  // end;
  // Perform(CM_FONTCHANGED, 0, 0);
end;

//:**********************************************************************
function TDBStringGrid.GetCol: LongInt;
begin
  Result := inherited Col;
  if fEnableDetails then
    dec(Result);
end;

//:**********************************************************************
function TDBStringGrid.GetColumns(aCol: Integer): TColumn;
begin
  Result := TColumn(Objects[aCol, 0]);
  if Result = Nil then Abort;
end;

//:**********************************************************************
function TDBStringGrid.GetData(ACol, ARow: Integer): TObject;
begin
  if fEnableDetails then
    inc(aCol);
  Result := Objects[ACol, ARow];
end;

//:**********************************************************************
function TDBStringGrid.GetDataRowCount: Integer;
begin
  // ein leeres Grid hat trotzdem eine g�ltige Zelle
  if Trim(Cells[1,1]) = '' then
    Result := 0
  else
    Result := RowCount - FixedRows;
end;

//:**********************************************************************
function TDBStringGrid.GetDataSource: TDataSource;
begin
  Result := mDataLink.DataSource;
end;

//:**********************************************************************
function TDBStringGrid.GetExpandInfo(aRow: Integer): TExpandInfo;
begin
  Result := Nil;
  if fEnableDetails and (aRow > 0) then begin
    Result := TExpandInfo(Objects[0, aRow]);
    if Result = Nil then Abort;
  end;
end;

//:**********************************************************************
function TDBStringGrid.GetValue(ACol, ARow: Integer): string;
begin
  if fEnableDetails then
    inc(aCol);
  Result := Cells[ACol, ARow];
end;

//:**********************************************************************
procedure TDBStringGrid.InsertCols(aCol, aCount: Integer);
var
  i: Integer;
begin
  ColCount := ColCount + aCount;
  for i:=ColCount-1 downto (aCol + aCount) do
    MoveColumn(i - aCount, i);
  // bestehende Spalten die gleiche Breite wieder zuweisen
  // for i:=ColCount-1 downto (aCol + aCount) do
  //   ColWidths[i] := ColWidths[i - aCount];
  // an den neuen Cols noch die Standardbreite zuweisen
  for i:=aCol to aCol + aCount-1 do
    ColWidths[i] := DefaultColWidth;
  
  // for i:=ColCount-1 downto (aCol + aCount) do
  //   Cols[i] := Cols[i - aCount];
  // for i:=ColCount-1 downto (aCol + aCount) do
  //   Colwidths[i] := Colwidths[i - aCount];
  // // an den neuen Cols noch die Standardbreite zuweisen
  // for i:=aCol to aCol + aCount-1 do
  //   ColWidths[i] := DefaultColWidth;
  //
  // if fEnableGraphics then
  // begin
  // for i:=0 to CCount-1 do
  //   for j:=0 to RowCount-1 do
  //    Objects[ColIndex+i,j]:=nil;
  // end;
  // ClearCols(ColIndex,Ccount);
end;

//:**********************************************************************
procedure TDBStringGrid.InsertRows(aRow, aCount: Integer);
var
  i: Integer;
begin
  RowCount := RowCount + aCount;
  for i:=RowCount-1 downto (aRow + aCount) do
    MoveRow(i - aCount, i);
  
  // for i:=ColCount-1 downto (aCol + aCount) do
  //   Cols[i] := Cols[i - aCount];
  // for i:=ColCount-1 downto (aCol + aCount) do
  //   Colwidths[i] := Colwidths[i - aCount];
  // // an den neuen Cols noch die Standardbreite zuweisen
  // for i:=aCol to aCol + aCount-1 do
  //   ColWidths[i] := DefaultColWidth;
  //
  // if fEnableGraphics then
  // begin
  // for i:=0 to CCount-1 do
  //   for j:=0 to RowCount-1 do
  //    Objects[ColIndex+i,j]:=nil;
  // end;
  // ClearCols(ColIndex,Ccount);
end;

//:**********************************************************************
procedure TDBStringGrid.InternalClear(aStartCol, aStartRow: Integer);
var
  xCol, xRow: Integer;
begin
  for xRow:=aStartRow to RowCount-1 do begin
    if fEnableDetails and Assigned(Objects[0, xRow]) then begin
      Objects[0, xRow].Free;
      Objects[0, xRow] := Nil;
    end;
    for xCol:=aStartCol to ColCount-1 do begin
      Cells[xCol, xRow] := '';
    end;
  end;
  inherited RowCount := FixedRows + 1;
end;

//:**********************************************************************
function TDBStringGrid.IsDataSetActive: Boolean;
begin
  Result := False;
  if Assigned(mDatalink.DataSource) then
    if Assigned(mDatalink.DataSource.DataSet) then
      Result := mDatalink.DataSource.DataSet.Active;
end;

//:**********************************************************************
procedure TDBStringGrid.Loaded;
begin
  inherited Loaded;
  
  PrepareEnableDetails;
end;

//:**********************************************************************
{
Bedingungen: 
- Property EnbaleDetails = True 
- OnGetDetailData enh�lt eine g�lte Methode

Wenn auf erste Spalte geklickt wird, dann wird ein Detailobjekt erstellt und die Methode in OnGetDetailData aufgerufen, um es f�llen zu lassen. Anschliessend 
wird das gef�llte Detailobjekt in das Grid eingef�gt (ExpandData).

Wird nochmals auf dieses Feld geklickt, werden die Detaildaten wieder aus dem Grid entfertn (CollapsData).
}
procedure TDBStringGrid.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  xInfo: TExpandInfo;
  xCol, xRow: Integer;
begin
  inherited MouseDown(Button, Shift, X, Y);
  
  if fEnableDetails and (Button = mbLeft) then begin
    MouseToCell(X, Y, xCol, xRow);
    CodeSite.SendFmtMsg('MouseDown: Row=%d, xRow=%d', [Row, xRow]);
    if (xRow > 0) and (xCol = 0) then begin
      Row := xRow;
      xInfo := TExpandInfo(Objects[0, xRow]);
      if Assigned(xInfo) then begin
        if xInfo.Expandable then begin
          xInfo.Expanded := not xInfo.Expanded;
          if xInfo.Expanded then begin
            ExpandData(xRow);
          end else begin
            CollapseData(xRow);
          end; // if Expanded
          Invalidate;
        end; // if Expandable
      end; // if Assigned(xInfo)
    end;
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.PrepareEnableDetails;
begin
  if fEnableDetails then begin
    ColWidths[0] := 20;
  end else begin
    ColWidths[0] := 10;
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.Proc_OnColumnChanged(Sender: TObject; aColIndex: Integer);
var
  i: Integer;
begin
  if aColIndex = -1 then begin
    for i:=1 to ColCount-1 do
      ColWidths[i] := Columns[i].Width;
  end else
    ColWidths[aColIndex] := Columns[aColIndex].Width;
  
  Invalidate;
end;

//:**********************************************************************
procedure TDBStringGrid.RemoveCols(aCol, aCount: Integer);
var
  i: Integer;
begin
  for i:=(aCol + aCount) to ColCount-1 do
    MoveColumn(i, i - aCount);
  
  // for i:=aCol to ColCount-aCount-1 do
  //   ColWidths[i] := ColWidths[i+aCount];
  
  ColCount := ColCount - aCount;
  
  // for i:=aCol to ColCount-1 do
  //   MoveColumn(i + aCount, i);
  // for i:=aCol to ColCount-1 do
  //   ColWidths[i]:=ColWidths[i + aCount];
  //
  // ColCount := ColCount - aCount;
  // for i:=aCol to ColCount-1 do
  //   Cols[i]:=Cols[i+aCount];
  // for i:=aCol to ColCount-1 do
  //   ColWidths[i]:=ColWidths[i + aCount];
  //
  // ColCount := ColCount - aCount;
end;

//:**********************************************************************
procedure TDBStringGrid.RemoveRows(aRow, aCount: Integer);
var
  i: Integer;
begin
  for i:=(aRow + aCount) to RowCount-1 do
    MoveRow(i, i - aCount);
  
  RowCount := RowCount - aCount;
end;

//:**********************************************************************
procedure TDBStringGrid.ScanDataset;
var
  i: Integer;
  xCol: Integer;
  xFirst: Boolean;
begin
  xFirst   := True;
  ColCount := 2;
  xCol     := FixedCols;
  with Dataset.Fields do begin
    for i:=0 to Count-1 do begin
      with Fields[i] do begin
        if Visible then begin
          // ColCount erst ab dem 2. Index erh�hen, da immer erst die erste Zelle gef�llt
          // werden soll
//          if i > 0 then
//          if Cells[xCol, 0] <> '' then
          if xFirst then xFirst   := False
                    else ColCount := ColCount + 1;

          Cells[xCol, 0]   := DisplayLabel;
          Objects[xCol, 0] := TColumn.Create;
          Columns[xCol].Alignment := Alignment;
          Columns[xCol].TitleAlignment := Alignment;
          Columns[xCol].Field     := Fields[i];
          Columns[xCol].FieldName := FieldName;
          Columns[xCol].Width     := Round(DisplayWidth * cAverageCharWidth);
          if Assigned(fOnGetColumnInfo) then
            fOnGetColumnInfo(Self, Columns[xCol]);
          Columns[xCol].OnColumnChanged := Proc_OnColumnChanged;
          inc(xCol);
        end;
      end;
    end; // for i
  end; // with
  
  Proc_OnColumnChanged(Nil, -1);
end;

//:**********************************************************************
procedure TDBStringGrid.SetCol(Value: LongInt);
begin
  // if fEnableDetails then
  //   inc(Value);
  inherited Col := Value;
end;

//:**********************************************************************
procedure TDBStringGrid.SetColumns(aCol: Integer; Value: TColumn);
var
  xColumn: TColumn;
begin
  xColumn := Columns[aCol];
  if Assigned(xColumn) then
    xColumn.Assign(Value);
end;

//:**********************************************************************
procedure TDBStringGrid.SetData(ACol, ARow: Integer; Value: TObject);
begin
  if fEnableDetails then
   inc(aCol);
  Objects[ACol, ARow] := Value;
end;

//:**********************************************************************
procedure TDBStringGrid.SetDataSource(aValue: TDataSource);
begin
  if Assigned(aValue) then begin
//    CodeSite.SendBoolean('datasource', avalue.DataSet.active);
    mDataLink.DataSource := aValue;
  //  ScanDataset;
  end else begin
    mDataLink.DataSource := Nil;
    if not fKeepValues then
      ClearAll;
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.SetEnableDetails(Value: Boolean);
begin
  if fEnableDetails <> Value then begin
    fEnableDetails := Value;
    PrepareEnableDetails;
  //   if Value then begin
  // //     InsertCols(0, 1);
  // //     FixedCols := FixedCols + 1;
  //     ColWidths[0] := 20;
  //     fEnableDetails := Value;
  //   end else begin
  //     fEnableDetails := Value;
  // //     RemoveCols(0, 1);
  //     ColWidths[0] := 10;
  //     // wenn nur noch eine Spalte vorhanden ist, dann wird FixedCols intern automatisch
  //     // auf 0 gesetzt.
  // //     if FixedCols > 0 then
  // //       FixedCols := FixedCols - 1;
  //   end;
  //  Invalidate;
  end;
end;

//:**********************************************************************
procedure TDBStringGrid.SetKeyField(Value: string);
begin
  fKeyField := Trim(Value);
end;

//:**********************************************************************
procedure TDBStringGrid.SetTitleFont(Value: TFont);
begin
  fTitleFont.Assign(Value);
end;

//:**********************************************************************
procedure TDBStringGrid.SetValue(ACol, ARow: Integer; const Value: string);
begin
  if fEnableDetails then
   inc(aCol);
  Cells[ACol, ARow] := Value;
end;

{
Bedingungen:
- Property EnbaleDetails = True
- OnGetDetailData enh�lt eine g�lte Methode

Wenn auf erste Spalte geklickt wird, dann wird ein Detailobjekt erstellt und die Methode in OnGetDetailData aufgerufen, um es
f�llen zu lassen. Anschliessend wird das gef�llte Detailobjekt in das Grid eingef�gt (ExpandData).

Wird nochmals auf dieses Feld geklickt, werden die Detaildaten wieder aus dem Grid entfertn (CollapsData).
}
{
Bedingungen:
- Property EnbaleDetails = True
- OnGetDetailData enh�lt eine g�lte Methode

Wenn auf erste Spalte geklickt wird, dann wird ein Detailobjekt erstellt und die Methode in OnGetDetailData aufgerufen, um es
f�llen zu lassen. Anschliessend wird das gef�llte Detailobjekt in das Grid eingef�gt (ExpandData).

Wird nochmals auf dieses Feld geklickt, werden die Detaildaten wieder aus dem Grid entfertn (CollapsData).
}

end.
