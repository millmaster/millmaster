(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AssignComp.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5.00
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 06.12.1999  1.00  Mg  | Datei erstellt
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 08.05.2000  1.11  Mg  | CheckUserValue in INavigateChanged
| 10.05.2000  1.12  Mg  | Query IsReadyToAssign auf Net Node umgestellt
| 22.05.2000  1.20  Mg  | Changing Model in TSpindleRangeList implemented
| 27.06.2000  1.30  Mg  | TMachine and TProdGrp LengthMode inserted
|                       | cGetProdGrpDetailData : c_LengthMode inserted
| 28.08.2000  1.31  Nue | TMachine.Assign inserted
|             1.32  Mg  | TGetSettingsHandler : MachineID in Message to Initializer
| 12.09.2000  1.33  Mg  | MMGetComputerName inserted
| 20.09.2000  1.34  Mg  | TMachine : OpenQuery and InitProperties inserted
| 09.10.2000  1.35  Mg  | TGetSettingsHandler : Progressbar eingefuegt
| 30.11.2000  1.36  Nue | TMachine modified because of changings in types of Kr (MaConfig)
| 19.12.2000  1.37  SDo | Proc. SystemErrorMsg_, UserErrorMsg, InfoMsg, WarningMsg,
|                       | CheckIntChar, CheckFloatChar ins LoepfeGlobal.pas umgelagert
| 18.01.2001  1.38 NueMg| Several changes cause of implementing style in SettingsNav
| 22.01.2001  1.39 Nue  | cGETSETTINGS_TIMEOUT increased from 10 to 30s because of Murata timeouts
|                       | (With ZE-Version>10.14, the "problem" with Murata should be solved.)
| 27.01.2001  1.39 Wss  | In initialization TMMSettingsReader.Instance.Init remarked to prevent access DB at design time
| 25.03.2001  1.06  Nue | Col. c_efficiency removed from cQueryMachInfoTot
| 03.04.2001  1.07  Nue | Following temporary code deleted (Nue)// @@@@@@@@@@@@@@@ nur fuer Spoerry, bis Clearer Settings Spectra unterstuetzen
|                       |    if ( FrontType = ftSWSInformatorSBC5 ) and ( MachineAttributes.IsSpectra )  then
|                       |    Result := false;
| 16.03.2001  1.08  Nue | Col. c_slip added in cQueryMachInfoTot
| 17.05.2001  1.09  Nue | Col. c_floor_id and c_adapt_or_name deleted in t_machine
| 21.03.2001  1.10  Nue | Slip added for handling in assignments
| 20.06.2001  1.11  Nue | In class TSpindleRangeList different 'if csDesigning in ComponentState then exit;' added.
| 12.07.2001  1.12  Nue | Several changes for release 2.01.01
| 13.09.2001  1.12  Nue | - In TProdGrp.SetProdGrpID NoRunningProdGrp set to false to identify a valid
                    Wss     prodgroup called from Floor
                          - In TSpindleRangeList.SetActSpdRange parameter call to UpdateProdGrpList changed
| 14.01.2002  1.13  Nue | cNoMachUploadDone moved from AssignmentsMainForm to AssignComp
| 01.03.2002  1.13  Wss | YarnCnt in TProdGrp, TOrderPosition aus DB nun auch als Float (kein Faktor 10 mehr)
| 18.03.2002  1.14  Nue | TOrderPosition.SetStyleID: YarnCnt from t_style converted to global YarnCntUnit.
| 25.03.2002  1.15  Nue | cNoMachUploadDone2 modified.
| 02.10.2002  1.16  LOK | Umbau auf ADO.
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 08.11.2002        LOK | Umbau ADO
| 10.12.2002        Nue | UpdateAllComponents called at WM_GETSETTINGS_FAILED in TSpindleRangeList.WndProc
| 22.01.2003  1.17  Nue | m.c_last_confirmed_upload in cQueryMachInfoTot added.
| 14.02.2003        Wss | gMMSetup durch TMMSettingsReader.Instance ersetzt wegen Probleme mit WinXP
| 26.02.2003        Nue | Minor changes in SetProdGrpID. (Message if machine already deleted)
| 03.03.2003        Wss | Get/Set public Methoden f�r Record TMachineYMConfigRec implementiert
                          ACHTUNG: Records d�rfen NIE als Property definiert werden!!!
| 26.08.2003  1.20  Nue | Ueberarbeitung Assignment: Klassen TxxItem von SettingsNavFrame neu jetzt in AssignComp.
| 03.09.2003  1.21  Nue | fComputerName := '.' added.
| 23.01.2004  1.22  Wss | DeallocateHWnd Aufruf in TSpindleRangeList.Destroy hinzugef�gt
| 29.01.2004  1.23  Nue | mGettingSettings hinzugef�gt; Modifikation in GetIsAbleToOverstartMaGroup
| 14.12.2004  2.00  Nue | Umbau auf XML V.5.0.
| 17.03.2005  2.00  Wss | In TSystemPort.Create wird der Thread nur noch zur Laufzeit erzeugt
| 19.07.2005  2.01  Nue | TPreselectItem.SettingsLen eliminated.
| 28.09.2005        Nue | GetSettingsFromMachine added.   (for access on FixSpdRange-machines)
| 01.11.2005        Nue | GettingSettingsFromMachine added.   (for access on FixSpdRange-machines)
| 07.06.2006        Nue | Richtiges Handling von leeren Spindelbereichen.
| 27.11.2006  2.02  Nue | c_use_newer_mapfile und CheckNewestMapfile Handling added.
| 08.02.2007        Nue | TMachine.YMType and handling for differ AC-Machines added.
=====================================================================================*)
unit AssignComp;
                        
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  mmEdit, mmLabel, mmGroupBox, mmList, mmSpeedButton, MMUGlobal,
  mmThread, IPCClass, BaseGlobal, LoepfeGlobal, mmTimer, IvDictio,
  SettingsReader, TypInfo, WaitForm, YMParaDef, YMParaUtils,// YMParaDBAccess,
  mmAdoDataset, XMLDef, xmlMaConfigClasses, VCLXMLSettingsModel;

resourcestring
  cStyle = '(20)Artikel:'; //ivlm
  cTemplate = '(10)Vorlage:'; //ivlm
  cMaGrpNr = '(*)Gruppen Nr:'; //ivlm
  cSettingsNotOK = '(*)Reiniger Einstellungen der Maschine koennen nicht abgefragt werden. Vermutlich ist Millmaster-Basissystem abgestellt!'; //ivlm
  cTimeoutMsg = '(*)Keine Reaktion vom Server System. Bitte wiederholen.'; //ivlm
//Nue: 8.8.01
  cNoMachUploadDone1 = '(*)Der Bauzustand der Maschine wurde noch nicht erfasst!'; //ivlm
  cNoMachUploadDone2 = '(*)Starten sie die MM-Anwendung "Maschinenkonfiguration" und fuehren sie einen "Daten laden - ab Maschinen" durch! '; //ivlm
  cMachineDeleted = '(*)Diese Maschine wurde geloescht! Es sind nur noch Produktionsdaten vorhanden!'; //ivlm

//Nue:6.4.05 von AskAssign �bernommen
  rsNotSameTKforProdGrp = '(*)Der Tastkopf der gewaehlten Partie, Vorlage oder Artikels unterscheidet sich von dem Tastkopf auf der Maschine! Die gewuenschte Partie kann daher nicht gestartet werden!'; //ivlm

  //F�r SettingsNavFRame, ListCobChooser usw.
  cMachine = '(*)Maschine'; // ivlm
  cMachGroup = '(8)Grp. Nr.'; // ivlm
  cSpdRange = '(15)Spulstellen'; // ivlm
  cSpdFirst = '(*)Erste Spulstelle'; //ivlm
  cSpdLast = '(*)Letzte Spulstelle'; //ivlm
  cStyle1 = '(*)Artikel'; // ivlm
  cStart = '(20)Start Mode'; //ivlm
  cState = '(*)Status'; // ivlm
  cTKClass = '(14)TK-Klasse'; // ivlm
  cLotName = '(*)Partie'; // ivlm
  cLotStart = '(30)Partie Start'; // ivlm
  cLotEnd = '(30)Partie Ende'; // ivlm
  cTemplateName = '(*)Vorlage'; // ivlm
  cAssortment = '(*)Sortiment'; // ivlm
  cChangedOnZE = '(*)Durch ZE veraendert'; //ivlm
  cYarnCount = '(*)Garn Nr [%s]'; //ivlm      
  cSlip = '(12)Schlupf'; //ivlm
  cNrOfDatasets = '(*)Anzahl Datensaetze'; //ivlm


const
//NUE1  cGETSETTINGS_TIMEOUT = 30000; //increased from 10 to 30s because of Murata timeouts 22.1.01 Nue
  cGETSETTINGS_TIMEOUT = 30000; //increased from 30s to 40s  21.04.05 Nue

  cAssignmentsPipeName = 'GLB_Ass_';
  ASSIGN_REG_STR = 'Assignments_msg';
  WM_DOASSIGN = 1;
  WM_ASSIGN_DONE = 2;
  WM_ASSIGN_TIMEOUT = 3;
  WM_DOCLOSE = 4;
  WM_CLOSE_DONE = 5;
  WM_CLOSE_TIMEOUT = 6;
  WM_CLOSEMSG_TIMEOUT = 7;
  WM_GETSETTINGS_TIMEOUT = 8;
  WM_GETSETTINGS_DONE = 9;
  WM_GETSETTINGS_FAILED = 10;
  WM_DOGETSETTINGS = 11;
  WM_NOSETTONGS_NEED = 12;
  WM_DOSAVESETTINGS = 13;

//------------------------------------------------------------------------------
  cQrySelMachine =
    'SELECT * FROM t_machine where c_machine_id=:c_machine_id ';
//------------------------------------------------------------------------------
  cQryGetMachineNode =
    'select * from t_net_node n, t_net t, t_front_type f, t_machine m ' +
    'where t.c_net_id=n.c_net_id and n.c_front_type=f.c_front_type and n.c_node_id=m.c_node_id ' +
    'order by m.c_node_id';
//------------------------------------------------------------------------------
  cQrySelMachConfig =
    'SELECT * FROM t_magroup_config where c_magroup_config_id=:c_magroup_config_id ';
//------------------------------------------------------------------------------

type
  //F�r SettingsNavFRame, ListCobChooser usw.
   TSourceTypes = (stTemplate, stOrderPosition, stStyle, stProdGrp, stMaMemory, stHistory);
//..............................................................................

  //F�r AskAssign, DetailInfo usw.
  TAddFields = record
                 Name: string;
                 Value: string;
               end;
  TStyleInfo = record
                 StyleName: string;
                 Fields: Array[1..10] of TAddFields;
               end;


//..............................................................................
  TBaseSpindleRange = class;
  TOrderPosition = class;
//..............................................................................
  IMiscInfo = interface
    function getYarnCnt: Single;
    function getSlip: string;
    function getYarnCntUnit: TYarnUnit;
    function getThreadCnt: Byte;
  end;
//..............................................................................
  INavChange = interface
    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    function CheckUserValue: boolean; // Anweisung an Komponente, die Eingabefelder zu pruefen.
                                      // Result = false wenn zwingende Eingaben noch nicht gemacht wurden
  end;
//..............................................................................
  INavChangeAdd = interface(INavChange)
    function getAsObject: TObject;
  end;
//..............................................................................
  TUserChanges = (ucNone, ucSpindleRange, ucPilotSpindles, ucMaGrpNr,
    ucYarnCnt, ucThreadCnt, ucSlip, ucSettings, ucAddSettings, ucProdUnit);
//..............................................................................
  TChangedItemsByUser = set of TUserChanges;
//..............................................................................
  TOnValueChange = procedure(aItme: TUserChanges; aChanged: boolean) of object;
//..............................................................................
  TOnFocusChange = procedure(aSender: TUserChanges) of object;
//..............................................................................
  TMachine = class(TComponent)
  private
    fQuery: TmmAdoDataset;
    fInopSettings0: DWord;
    fInopSettings1: DWord;
    fFrontType: TFrontType;
    fMachineName: string;
    fMachineID: integer;
    fNumSpindles: integer;
    fNetTyp: TNetTyp;
    fSpeedRamp: integer;
    fSpeed: integer;
    fLengthWindow: integer;
    fLengthMode: integer;
    fFixSpdRanges: boolean;
    fSlip: Integer;
    fMachineConfigA: Word;
    fMachineConfigB: Word;
    fMachineConfigC: Word;
    // = Bauzustand
//NUE1    fMachineYMConfigRec: TMachineYMConfigRec; //Contains: FirstSpd, LastSpd,sensing_head,..
    fNodeID: string;
    fMaOffsetID: Integer;
    fStarting: TDateTime;
    fDataCollection: Integer;
    fOnlineSpeed: Boolean;
//    fEfficiency     :Real;
    fLsInProd: Integer;
    fLsBreak: Integer;
    fLsWaitMat: Integer;
    fLsWairRev: Integer;
    fLsClean: Integer;
    fLsFree: Integer;
    fLsDefine: Integer;
    fLongStopDef: Integer;
    fCutRetries: Integer;
    fCheckLen: Integer;
    FCheckNewestMapfile: Boolean;
    fYMVersion: string;
    fYMOption: TFrontSwOption;
//NUE1    fAWEMachType: TAWEMachType;
    fMachineType: TMachineType;
    fSpindlePos: Integer;
//NUE1    fMachineManuf: string;
//NUE1    fMachineModel: string;
//NUE1    fMTSpindPos: Integer;
    fUploadTimeout: Boolean;
//NUE1    fDefaultNetTypforMachTyp: TNetTyp;
    fIsConnectedToMachine: Boolean;
    fLastConfirmedUpload: TDatetime;
    FMaConfigReader: TMaConfigReader;
    fOverruleMapID: Integer;
    fUploaded: Boolean;
    FYMType: TFrontType;
    function CheckSlip(aSlip: Integer): Integer;
    function GetIsReadyToAssign: boolean; // dito
    function GetIsReadyToClose: boolean;
    function GetIsReadyToGetMachineSettings: boolean;
    function GetIsAbleToGetMachineSettings: boolean;
    function GetIsAbleToClose: boolean;
    function GetIsAbleToOverstartMaGroup: boolean;
    function GetIsOnline: boolean;
    function GetNumOfGrps: integer;
    function GetIsInProduction: Boolean;
    function GetMaConfigReader: TMaConfigReader;
//NUE1    procedure SetMachineYMConfigRec(const aMachineYMConfigRec: TMachineYMConfigRec);
    procedure SetSlip(const aValue: Integer);
//NUE1    property AWEMachType: TAWEMachType read fAWEMachType;
//    property CheckLen: Integer read fCheckLen;
//    property CutRetries: Integer read fCutRetries;
//    function GetMachineYMConfigRec: TMachineYMConfigRec;

//    property FrontType: TFrontType read fFrontType;
//    property InopSettings0: DWord read fInopSettings0;
//    property InopSettings1: DWord read fInopSettings1;
//    property LongStopDef: Integer read fLongStopDef;
//    property LsBreak: Integer read fLsBreak;
//    property LsClean: Integer read fLsClean;
//    property LsDefine: Integer read fLsDefine;
//    property LsFree: Integer read fLsFree;
//    property Efficiency     :Real read fEfficiency write fEfficiency;
//    property LsInProd: Integer read fLsInProd;
//    property LsWairRev: Integer read fLsWairRev;
//    property LsWaitMat: Integer read fLsWaitMat;
//    property MachineConfigA: word read fMachineConfigA;
//    property MachineConfigB: word read fMachineConfigB;
//    property MachineConfigC: word read fMachineConfigC;
//    property YMOption: TFrontSwOption read fYMOption write fYMOption;
//    property YMVersion: string read fYMVersion;
  protected
    procedure SetMachineID(aMachID: integer);
    procedure InitProperties(aMachID: integer);
    procedure OpenQuery(aMachID: integer; aSQLStr: string);
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    procedure Assign(aSource: TPersistent); override;
    property CheckNewestMapfile: Boolean read FCheckNewestMapfile write
        FCheckNewestMapfile;                                    //Nue:27.11.06
    property MachineName: string read fMachineName write fMachineName;
    property NumSpindles: integer read fNumSpindles write fNumSpindles; //Erweitert mit write wegen Easy Maconfig Nue:11.2.03
    property NumOfGrps: integer read GetNumOfGrps;
    property MachineID: integer read fMachineID write SetMachineID;
    property NetTyp: TNetTyp read fNetTyp;
    property LengthWindow: integer read fLengthWindow;
    property LengthMode: integer read fLengthMode;
    property IsReadyToAssign: boolean read GetIsReadyToAssign;
    property IsReadyToGetMachineSettings: boolean read GetIsReadyToGetMachineSettings;
    property IsReadyToClose: boolean read GetIsReadyToClose;
    property IsAbleToGetMachineSettings: boolean read GetIsAbleToGetMachineSettings;
    property IsAbleToClose: boolean read GetIsAbleToClose; // possible to stopp a ProdGrp on this machine
    property IsAbleToOverstartMaGroup: boolean read GetIsAbleToOverstartMaGroup;
    property IsOnline: boolean read GetIsOnline;
    property IsConnectedToMachine: Boolean read fIsConnectedToMachine write fIsConnectedToMachine; //Nue:11.12.02
    property FixSpdRanges: boolean read fFixSpdRanges write fFixSpdRanges;
    property Slip: Integer read fSlip write SetSlip;
//NUE1    property MachineYMConfigRec: TMachineYMConfigRec read fMachineYMConfigRec write SetMachineYMConfigRec;

    property NodeId: string read fNodeId write fNodeId;
    property MaOffsetID: Integer read fMaOffsetID;
    property Starting: TDateTime read fStarting write fStarting;
    property DataCollection: Integer read fDataCollection write fDataCollection;
    property SpindlePos: Integer read fSpindlePos write fSpindlePos;
    property MachineType: TMachineType read fMachineType write fMachineType;
//NUE1    property DefaultNetTypforMachTyp: TNetTyp read fDefaultNetTypforMachTyp default ntNone;
//NUE1    property MachineManuf: string read fMachineManuf;
//NUE1    property MachineModel: string read fMachineModel;
//NUE1    property MTSpindPos: Integer read fMTSpindPos;
    property IsInProduction: Boolean read GetIsInProduction;
    property LastConfirmedUpload: TDatetime read fLastConfirmedUpload write fLastConfirmedUpload;
    property MaConfigReader: TMaConfigReader read GetMaConfigReader;
    property OnlineSpeed: Boolean read fOnlineSpeed write fOnlineSpeed;
    property OverruleMapID: Integer read fOverruleMapID;
    property Speed: integer read fSpeed;
    property SpeedRamp: integer read fSpeedRamp;
    property Uploaded: Boolean read fUploaded default False;
    property UploadTimeout: Boolean read fUploadTimeout write fUploadTimeout;
    property YMType: TFrontType read FYMType;
  published
    property Query: TmmAdoDataset read fQuery write fQuery;
  end;
//..............................................................................
  TBaseSpindleRange = class(TMachine)
  private
    fSpindleFirst: integer;
    fSpindleLast: integer;
    fNoRunningProdGrp: Boolean; //Nue:  27.8.01
  public
    constructor Create(aQuery: TmmAdoDataset); reintroduce; virtual;
    destructor Destroy; override;
    property SpindleFirst: integer read fSpindleFirst;
    property SpindleLast: integer read fSpindleLast;
    property NoRunningProdGrp: Boolean read fNoRunningProdGrp write fNoRunningProdGrp;
  end;
//..............................................................................
  TSpindleRange = class(TBaseSpindleRange)
  private
  public
    procedure Init(aMachineID, aSpindleFirst, aSpindleLast: integer);
  end;
//..............................................................................
  TProdGrp = class(TBaseSpindleRange, IMiscInfo)
  private
    fProdGrpID: integer;
    fPilotSpindles: integer;
    fSlip: string;
    fMachSpeedRamp: integer;
    fMachSpeed: integer;
    fMachLengthWindow: integer;
    fMachLengthMode: integer;
    fYarnCnt: Single;
    fYarnCntUnit: TYarnUnit;
    fGrpNr: integer;
    fYMSetID: integer;
    fOrderPositionID: integer;
    fStyleID: integer;
    fStyleName: string; //New since 13.11.01 Nue
    fYMSetName: string; //New since 13.11.01 Nue
    fTemplateYMSet: Boolean; //New since 13.11.01 Nue
    fThreadCnt: Byte; //New since 19.12.00 Nue
    fName: string;
    fColor: TColor;
    fStartTime: TDateTime;
    fStopTime: TDateTime;
    fState: TProdGrpState;
    procedure SetProdGrpID(aProdGrpID: integer);
  protected
    function GetMachSpeed: integer;
    function GetMachSpeedRamp: integer;
    function GetMachLengthWindow: integer;
  public
    constructor Create(aQuery: TmmAdoDataset); override;
    function getYarnCnt: Single;
    function getSlip: string;
    function getYarnCntUnit: TYarnUnit;
    function getThreadCnt: Byte;
    property ProdGrpID: integer read fProdGrpID write SetProdGrpID;
    property PilotSpindles: integer read fPilotSpindles;
    property MachSpeedRamp: integer read GetMachSpeedRamp;
    property MachSpeed: integer read GetMachSpeed;
    property MachLengthWindow: integer read GetMachLengthWindow;
    property MachLengthMode: integer read fMachLengthMode;
    property Slip: string read fSlip;
    property YarnCnt: Single read fYarnCnt;
    property YarnCntUnit: TYarnUnit read fYarnCntUnit;
    property GrpNr: integer read fGrpNr;
    property YMSetID: integer read fYMSetID;
    property OrderPositionID: integer read fOrderPositionID;
    property StyleID: integer read fStyleID;
    property StyleName: string read fStyleName;
    property YMSetName: string read fYMSetName;
    property TemplateYMSet: Boolean read fTemplateYMSet;
    property Name: string read fName;
    property Color: TColor read fColor; // only availabe if prodgrp is runnning
    property StartTime: TDateTime read fStartTime;
    property StopTime: TDateTime read fStopTime; //Nue:22.01.02
    property State: TProdGrpState read fState; //Nue:22.01.02
  end;
//..............................................................................
  TSimulatorProdGrp = class(TProdGrp)
  private
  public
    property PilotSpindles: integer read fPilotSpindles write fPilotSpindles;
    property SpindleFirst: integer read fSpindleFirst write fSpindleFirst;
    property SpindleLast: integer read fSpindleLast write fSpindleLast;
    property Slip: string read fSlip write fSlip;
//    property YarnCnt      : Single read fYarnCnt write fYarnCnt;
    property YarnCntUnit: TYarnUnit read fYarnCntUnit write fYarnCntUnit;
    property GrpNr: integer read fGrpNr write fGrpNr;
    property YMSetID: integer read fYMSetID write fYMSetID;
    property OrderPositionID: integer read fOrderPositionID write fOrderPositionID;
    property Name: string read fName write fName;
    property Color: TColor read fColor write fColor;
  end;
//..............................................................................
  TGetSettingsHandler = class;
//..............................................................................
  TSpindleRangeList = class(TComponent)
  private
    mComponentList: TmmList;
    mProdGrpList: TStringList;
    mProdGrpCursor: integer;
    mAssWindowMsg: DWord;
    mWindowHandle: HWND;
    mXMLSettingsCollection: PXMLSettingsCollection;
//wss    mMachSettings: PSettingsArr;
    mMiscInfo: IMiscInfo;
    mTKSpindleFirst: Integer; //Nue:21.8.03
    fFirst: boolean;
    fLast: boolean;
    fQuery: TmmAdoDataSet;
    fAssMachine: TMachine;
    fMachineID: integer;
    fSettingsHandler: TGetSettingsHandler;
    fChangedItems: TChangedItemsByUser;
    fOnItemValueChanged: TOnValueChange;
    fOnItemFocusChanged: TOnFocusChange;
    fProdUnit: TObject;
    fCallByMachine: boolean;
    FGettingSettingsFromMachine: Boolean;
    mGettingSettingsFromMachineCaller: HWND;
//    procedure UpdateAllComponents;
    function getSettingsNav: TObject; // sucht den Settings Navigator in der Liste der Komponenten
    procedure SetGettingSettingsFromMachine(const Value: Boolean);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure WndProc(var msg: TMessage);
    procedure SetActSpdRange(aSpdRange: TMachine);
    function GetActSpdRange: TMachine;
    procedure UpdateProdGrpList; overload;
    procedure UpdateProdGrpList(aSpindleFirst: integer); overload;
    procedure SetFirstLastFlags;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function CheckUserValues: boolean; // Ueberprueft die User Eingaben jeder Komponente
    function CheckGrpNr(aGrpNr: integer): boolean; overload;
    function CheckGrpNr(aGrpNr, aFirstSpd: integer; out aNewIndex: Integer):
        boolean; overload;
    procedure add(aComponent: INavChange);
    procedure ItemChanged(aChanged: TUserChanges);
    procedure ItemFocusChanged(aChanged: TUserChanges);
    procedure Up;
    procedure Down;
    property CallByMachine: boolean read fCallByMachine write fCallByMachine;  //Erkennung ob CallByMachine or CallByProdgrp Nue:20.8.03
    property FirstRange: boolean read fFirst;
    property LastRange: boolean read fLast;
    property ActSpdRange: TMachine read GetActSpdRange write SetActSpdRAnge;
    property ChangedItems: TChangedItemsByUser read fChangedItems;
    property OnItemValueChanged: TOnValueChange read fOnItemValueChanged write fOnItemValueChanged;
    property OnItemFocusChanged: TOnFocusChange read fOnItemFocusChanged write fOnItemFocusChanged;
    property ProdUnit: TObject read fProdUnit write fProdUnit;
    procedure GetSettingsFromMachine(aHwnd: HWND);
    procedure UpdateAllComponents;
    procedure UpdateAllComponentsCauseOfProdUnitChange;
    property GettingSettingsFromMachine: Boolean read FGettingSettingsFromMachine
        write SetGettingSettingsFromMachine;
    property SettingsNavObject: TObject read getSettingsNav; //Nue 20.06.01 sucht den Settings Navigator in der Liste der Komponenten
    property ProdGrpList: TStringList read mProdGrpList; //Nue: 7.7.03 Zugriffsproperty wegen Checks in SpindleFieldsFrame
  published
    property Query: TmmAdoDataSet read fQuery write fQuery;
    property AssMachine: TMachine read fAssMachine write fAssMachine;
    property SettingsHandler: TGetSettingsHandler read fSettingsHandler write fSettingsHandler;
  end;
//..............................................................................
  TAssEdit = class(TmmEdit, INavChange)
  private
    fSpdRangeList: TSpindleRangeList;
    procedure SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss    procedure UpdateComponent(aProdGrp: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
    function CheckUserValue: boolean;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
  published
    property SpdRangeList: TSpindleRangeList read fSpdRangeList write SetSpdRangeList;
  end;
//..............................................................................
  TOrderPosition = class(TComponent, IMiscInfo)
  private
    mInitialized: boolean;
    mQuery: TmmAdoDataset;
    fOrderPositionID: integer;
    fOrderID: integer;
    fStyleID: integer;
    fStyleName: string;
    fOrderPositionName: string;
    fMeterToProd: Double;
    fYarnCnt: Single;
    fThreadCnt: Byte;
    fSlip: string;
    fYarnCntUnit: TYarnUnit;
    procedure SetOrderPositionID(aOrderPositionID: integer);
    procedure SetStyleID(aStyleID: integer);
    function GetOrderPositionID: integer;
    function GetOrderID: integer;
    function GetStyleID: integer;
    function GetMeterToProd: Double;
    function GetStyleName: string;
    function GetOrderPositionName: string;
  public
    constructor Create(aQuery: TmmAdoDataset); reintroduce; virtual;
    function getYarnCnt: Single;
    function getSlip: string;
    function getYarnCntUnit: TYarnUnit;
    function getThreadCnt: Byte;
    property OrderPositionID: integer read GetOrderPositionID write SetOrderPositionID;
    property StyleID: integer read GetStyleID write SetStyleID;
    property OrderID: integer read GetOrderID;
    property MeterToProd: Double read GetMeterToProd;
    property StyleName: string read GetStyleName;
    property OrderPositionName: string read GetOrderPositionName;
    property Slip: string read fSlip (*nur temporaer wegen Zugriff in SettingsNav.GetMiscInfo*)  write fSlip;
    property YarnCnt: Single read fYarnCnt (*nur temporaer wegen Zugriff in SettingsNav.GetMiscInfo*)  write fYarnCnt;
    property YarnCntUnit: TYarnUnit read fYarnCntUnit;
  end;
//..............................................................................

//..............................................................................
  TBaseItem = class(TObject)
  private
    fName: string;
  public
    property Name: string read fName write fName;
  end;
//..............................................................................
  TBaseSetIDItem = class(TBaseItem)
  private
    fSetID: integer;
    fColor: TColor;
    fHeadClass: TSensingHeadClass;
  public
    constructor Create(aSetID: integer); virtual;
    property SetID: integer read fSetID;
    property Color: TColor read fColor write fColor;
    property HeadClass: TSensingHeadClass read fHeadClass write fHeadClass; //Nue 9.8.01
  end;
//..............................................................................
  TProdGrpItem = class(TBaseSetIDItem)
  private
    fProdGrpID: integer;
    fOrderPositionID: integer;
    fStyleID: integer;
    fMachineName: string;
    fStyleName: string; //Nue 28.06.01
    fYMSetName: string; //Nue 20.09.01
    fProdStart: string;
    fProdEnd: string;
    fSpindleFirst: string;
    fSpindleLast: string;
    fState: integer;
    fYarnCnt: Single; //Nue 27.03.03
    fSlip: string; //Nue 27.03.03
    fGrpNr: integer;
    fStartMode: integer;  //Nue 27.03.03
//wss    fStyleInfo: TStringList; //Nue:03.06.03

  public
    constructor Create(aSetID: integer; aProdGrpID: integer; aOrderPosID: integer; aStyleID: integer); reintroduce; virtual;
    property ProdGrpID: integer read fProdGrpID;
    property OrderPositionID: integer read fOrderPositionID;
    property StyleID: integer read fStyleID;
//    property StyleInfo: TStringList read fStyleInfo write fStyleInfo;    //Nue:03.06.03
    property MachineName: string read fMachineName write fMachineName;
    property StyleName: string read fStyleName write fStyleName; //Nue 28.06.01
    property YMSetName: string read fYMSetName write fYMSetName; //Nue 20.09.01
    property ProdStart: string read fProdStart write fProdStart;
    property ProdEnd: string read fProdEnd write fProdEnd;
    property SpindleFirst: string read fSpindleFirst write fSpindleFirst;
    property SpindleLast: string read fSpindleLast write fSpindleLast;
    property State: integer read fState write fState;
    property YarnCnt: Single read fYarnCnt write fYarnCnt; //Nue 21.03.03
    property Slip: string read fSlip write fSlip; //Nue 27.03.03
    property GrpNr: integer read fGrpNr write fGrpNr; //Nue 27.03.03
    property StartMode: integer read fStartMode write fStartMode; //Nue 26.05.03
  end;
//..............................................................................
  TPreselectItem = class(TBaseItem)
  private
    fSpindleFirst: string;
    fSpindleLast: string;
    fGrpNr: integer;
    FLengthMode: Byte;
    FLengthWindow: Word;
    FPilotSpindles: Byte;
    FSettings: string;
//    fState: string;
    fState: TGroupState;  //Nue:8.10.06
    fYarnCnt: Single; //Nue 21.06.01
    fYMSetName: string; //Nue 01.10.01
    fSlip: string; //Nue 27.03.03
  public
    constructor Create; reintroduce; virtual;
//NUE1    property Settings: TYMSettingsByteArr read fSettings write fSettings;
{ TODO 1 -oNue -c1 : Hier am 17.5.05 noch Anpassungen machen, damit TXMLSettingsRec statt nur XMLData �bergeben werden (wegen LengthMode, LengthWindow, PilotSpindles?) }
//    property Settings: string read fSettings write fSettings;
    property SpindleFirst: string read fSpindleFirst write fSpindleFirst;
    property SpindleLast: string read fSpindleLast write fSpindleLast;
    property GrpNr: integer read fGrpNr write fGrpNr;
    property LengthMode: Byte read FLengthMode write FLengthMode;
    property LengthWindow: Word read FLengthWindow write FLengthWindow;
    property PilotSpindles: Byte read FPilotSpindles write FPilotSpindles;
    property Settings: string read FSettings write FSettings;
//    property State: string read fState write fState;
    property State: TGroupState read fState write fState; //Nue:8.10.06
    property YarnCnt: Single read fYarnCnt write fYarnCnt; //Nue 21.06.01
    property YMSetName: string read fYMSetName write fYMSetName; //Nue 01.10.01
    property Slip: string read fSlip write fSlip; //Nue 27.03.03
  end;
//..............................................................................
  TStyleItem = class(TBaseSetIDItem)
  private
    fStyleID: integer;
    fYMSetName: string;
    fAssortmentName: string;
    fYarnCnt: Single; //Nue 27.03.03
    fSlip: string;  //Nue 27.03.03
    fStyleInfo: TStringList; //Nue:03.06.03
  public
    constructor Create(aSetID: integer; aStyleID: integer); reintroduce; virtual;
    property StyleID: integer read fStyleID;
    property StyleInfo: TStringList read fStyleInfo write fStyleInfo;    //Nue:03.06.03
    property YMSetName: string read fYMSetName write fYMSetName;
    property AssortmentName: string read fAssortmentName write fAssortmentName;
    property YarnCnt: Single read fYarnCnt write fYarnCnt; //Nue 21.03.03
    property Slip: string read fSlip write fSlip; //Nue 27.03.03
  end;
//..............................................................................
  TOrderPositionItem = class(TBaseSetIDItem)
  private
    fOrderPositionID: integer;
  public
    constructor Create(aSetID: integer; aOrderPositionID: integer); reintroduce; virtual;
    property OrderPositionID: integer read fOrderPositionID;
  end;
//..............................................................................
  TTemplateItem = class(TBaseSetIDItem)
  private
  public
  end;
//..............................................................................




{Deleted001213
  TStyle = class ( TObject )
  private
    mInitialized         : boolean;
    mQuery               : TmmQuery;
    fStyleID             : integer;
    fStyleName           : String;
//    fMeterToProd         : Double;
    procedure SetStyleID ( aStyleID : integer );
    function  GetStyleID: integer;
//    function  GetMeterToProd:Double;
    function  GetStyleName:String;
  public
    constructor Create ( aQuery : TmmQuery );
    property StyleID   : integer read GetStyleID write SetStyleID;
//    property MeterToProd       : Double  read GetMeterToProd;
    property StyleName         : String  read GetStyleName;
  end;
{}
//..............................................................................
  TAfterReceive = procedure(aReceivedMsg: PResponseRec) of object;
  TSystemPortThread = class(TmmThread)
  private
    mReader: TIPCServer;
    fAfterReceive: TAfterReceive;
    fPortName: string;
    procedure SetPortName(aPortName: string);
  protected
    procedure Execute; override;
  public
    constructor Create;
    destructor Destroy; override;
    property AfterReceive: TAfterReceive read fAfterReceive write fAfterReceive;
    property PortName: string read fPortName write SetPortName;
  end;
//..............................................................................
  TProcessMsg = procedure(aReceivedMsg: PResponseRec) of object;
//..............................................................................
  TSystemPort = class(TComponent)
  private
    mThread: TSystemPortThread;
    mWriter: TIPCClient;
    fProcessMsg: TProcessMsg;
    fComputerName: string;
    fPortName: string;
  protected
    procedure AfterReceive(aReceivedMsg: PResponseRec);
  public
    constructor Create(aOwner: TComponent; aPortName: string); reintroduce; virtual;
    destructor Destroy; override;
    procedure NewJob(aJob: PJobRec); virtual;
    property LocalComputerName: string read fComputerName;
    property PortName: string read fPortName;
    property ProcessMsg: TProcessMsg read fProcessMsg write fProcessMsg;
  published
  end;
//..............................................................................
  THandlerState = (asNone, asNoJob, asWaitJobRespons, asJobResponsedOK, asJobResponsedNotOK);
  TBaseHandler = class(TComponent)
  private
    fAssMachine: TMachine;
    FGetWindowHandle: HWND;
    procedure WndProc(var msg: TMessage);
  protected
    mState: THandlerState;
    mWindowHandle: HWND;
    mTimer: TmmTimer;
    mPort: TSystemPort;
    procedure OnTimeout(aSender: TObject); virtual;
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); virtual;
    procedure OnWndMsg(msg: TMessage); virtual; abstract;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure NewJob(aJob: PJobRec);
  public
    constructor Create(aOwner: TComponent); overload; override;
    constructor Create(aOwner: TComponent; aPortName: string); reintroduce; overload;
    destructor Destroy; override;
    property GetWindowHandle: HWND read FGetWindowHandle; //Nue:30.11.05 Gibt den WindowsHandle f�r den BaseHandler bekannt
  published
    property AssMachine: TMachine read fAssMachine write fAssMachine;
  end;
//..............................................................................
  TGetSettingsHandler = class(TBaseHandler)
  private
    FGettingSettingsFromMachine: Boolean;
    mWait: TWait;
    mInitializerHWND: HWND;
    mGettingSettings: Boolean;  //Zeigt an, ob bereits ein Upload aktiv ist Nue:29.1.04
    fSettings: PXMLSettingsCollection;
    fShowWait: boolean;
    fXMLMaConfig: string;
    fXMLSettingsCollection: PXMLSettingsCollection;
    mDebugOutEnabled: Boolean;
    procedure DoGetSettings(aWinMsgTyp: Integer);
    procedure ProcessGetSettingsTimeoutMsg;
    procedure ProcessGetSettingsDoneMsg(aMsgTyp: TResponseMsgTyp);
    function StartGetSaveSettings(aInitializer: HWND; aNoMessage: Boolean;
        aWinMsgTyp: Integer): Boolean;
  protected
    procedure OnTimeout(Sender: TObject); override;
    procedure OnMsgReceived(aReceivedMsg: PResponseRec); override;
    procedure OnWndMsg(msg: TMessage); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function StartGetSettings(aInitializer: HWND; aNoMessage: Boolean = False):
        Boolean;
    function StartSaveSettings(aInitializer: HWND; aNoMessage: Boolean = False):
        Boolean;
    property GettingSettingsFromMachine: Boolean write FGettingSettingsFromMachine;    //Flag um anzuzeigen das ein Upload wegen FixSpdRange im AskAssign ausgel�st wurde!!
    property XMLSettingsCollection: PXMLSettingsCollection read fXMLSettingsCollection;
//    property SettingsArr: PSettingsArr read fSettings;
    property ShowWait: boolean read fShowWait write fShowWait;
    property XMLMaConfig: string read fXMLMaConfig;
  published
  end;

//..............................................................................

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS, MMEventLog, SettingsNavFrame, XMLGlobal;

const
  cGetSettingsPipeName = cAssignmentsPipeName + 'GetSe' + cAddOnChannelName;
//------------------------------------------------------------------------------
  //Gleiches Query wie in YMParaDBAccess.pas
//  cQueryMachInfoTot = //!!!!!letzte 8 Zeilen muessen mit AssignComp.cQueryMachInfo identisch sein (ohne die letzte)!!!!!!
//    'select m.c_node_id, m.c_maoffset_id, m.c_starting, m.c_data_collection, ' +
//    'm.c_online_speed, m.c_ls_in_prod, m.c_ls_break, m.c_ls_wait_mat, m.c_ls_wait_rev, ' +
//    'm.c_ls_clean, m.c_ls_free, m.c_ls_define, m.c_spindle_pos, m.c_longStopDef, m.c_cutRetries, ' +
//    'm.c_checkLen, m.c_inopSettings0, m.c_inopSettings1, m.c_optionCode, m.c_YM_version, m.c_AWE_mach_type, ' +
//    'mt.c_machine_manuf, mt.c_machine_model, mt.c_spind_pos, mt.c_machine_type, mt.c_default_net_id, ' +
//    'm.c_machine_name, m.c_nr_of_spindles, m.c_net_id, ' +
//    'm.c_speedRamp, m.c_fixSpindlerange, m.c_slip, nn.c_front_type, ' +
//    'm.c_opt_speed, m.c_configA, m.c_configB, m.c_configC, ' +
//    'm.c_inopsettings0, m.c_inopsettings1, m.c_last_confirmed_upload ' +
//    'from t_machine m, t_net_node nn, t_machine_type mt ' +
//    'where m.c_node_id = nn.c_node_id ' +
//    'and c_machine_id =:c_machine_id ' +
//    'and m.c_machine_type = mt.c_machine_type ';

//  cQueryMachInfoTot = //!!!!!letzte 8 Zeilen muessen mit AssignComp.cQueryMachInfo identisch sein (ohne die letzte)!!!!!!
//    'select m.c_node_id, m.c_maoffset_id, m.c_starting, m.c_data_collection, m.c_spindle_pos, ' +
//    'mt.c_machine_manuf, mt.c_machine_model, mt.c_spind_pos, mt.c_machine_type, mt.c_default_net_id, ' +
//    'm.c_machine_name, m.c_nr_of_spindles, m.c_net_id, ' +
//    'm.c_fixSpindlerange, m.c_slip, nn.c_front_type, m.c_magroup_config_id, m.c_overrule_mapfile_id ' +
//    'from t_machine m, t_net_node nn, t_machine_type mt ' +
//    'where m.c_node_id = nn.c_node_id ' +
//    'and c_machine_id =:c_machine_id ' +
//    'and m.c_machine_type = mt.c_machine_type ';
//
  cQueryMachInfoTot = //!!!!!letzte 8 Zeilen muessen mit AssignComp.cQueryMachInfo identisch sein (ohne die letzte)!!!!!!
    'select m.c_node_id, m.c_maoffset_id, m.c_starting, m.c_data_collection, m.c_spindle_pos, ' +
    'm.c_machine_name, m.c_nr_of_spindles, m.c_net_id, m.c_machine_type, ' +
    'm.c_fixSpindlerange, m.c_slip, nn.c_front_type, m.c_magroup_config_id, m.c_overrule_mapfile_id, m.c_use_newer_mapfile ' +
    'from t_machine m, t_net_node nn ' +
    'where m.c_node_id = nn.c_node_id ' +
    'and c_machine_id =:c_machine_id ';

//------------------------------------------------------------------------------
  cGetMachineIDQuery =
//alt  'select c_machine_id from t_prodgroup where c_prod_id = :c_prod_id';
  'select p.c_machine_id from t_prodgroup p, t_machine m where (p.c_machine_id=m.c_machine_id) and (p.c_prod_id = :c_prod_id)'; //Nue:25.2.03
//------------------------------------------------------------------------------
  cGetRunProdGrpsOfMachine =
    'select t_p.c_prod_id c_prod_id from t_prodgroup t_p, t_prodgroup_state t_ps ' +
    'where t_p.c_machine_id = :c_machine_id ' +
    'and   t_p.c_prod_id = t_ps.c_prod_id ' +
    'and   t_p.c_ym_set_id <> 0 ' +
    'and   t_p.c_prod_state <> 4 ' + // 4 = stopped prod grps
    'order by t_p.c_spindle_first';
//------------------------------------------------------------------------------
{Alt bis 13.11.01
  cGetProdGrpDetailData =
  'select t_p.c_prod_id, t_p.c_spindle_first, t_p.c_spindle_last, t_p.c_pilotspindles, t_p.c_slip, ' +
  't_p.c_machine_id, t_p.c_YM_set_id, t_p.c_act_yarncnt, t_p.c_yarncnt_unit, t_p.c_machineGroup, ' +
  't_p.c_speed, t_p.c_speedRamp, t_p.c_lengthWindow, t_p.c_order_position_id, t_p.c_prod_name, ' +
  't_p.c_machine_name, t_p.c_prod_start, t_ps.c_color, t_p.c_lengthMode, t_p.c_nr_of_threads, t_p.c_style_id, ' +
  't_p.c_style_name '+  //Nue:13.11.01
  'from t_prodgroup t_p, t_prodgroup_state t_ps ' +
  'where t_p.c_prod_id = :c_prod_id ' +
  'and   t_ps.c_prod_id =* t_p.c_prod_id';
  }
  //------------------------------------------------------------------------------
//Neu ab 13.11.01
  cGetProdGrpDetailData =
    'select t_p.c_prod_id, t_p.c_spindle_first, t_p.c_spindle_last, t_p.c_pilotspindles, t_p.c_slip, ' +
    't_p.c_machine_id, t_p.c_YM_set_id, t_p.c_act_yarncnt, t_p.c_yarncnt_unit, t_p.c_machineGroup, ' +
    't_p.c_speed, t_p.c_speedRamp, t_p.c_lengthWindow, t_p.c_order_position_id, t_p.c_prod_name, ' +
    't_p.c_machine_name, t_p.c_prod_start, t_p.c_prod_end, t_p.c_prod_state, t_ps.c_color, t_p.c_lengthMode, t_p.c_nr_of_threads, t_p.c_style_id, ' +
    't_p.c_style_name, t_y.c_YM_set_name, t_y.c_template_set ' + //Nue:13.11.01
    'from t_prodgroup t_p, t_prodgroup_state t_ps, t_xml_ym_settings t_y ' +
    'where t_p.c_prod_id = :c_prod_id ' +
    'and t_p.c_YM_set_id=t_y.c_YM_set_id ' +
    'and   t_ps.c_prod_id =* t_p.c_prod_id';
//------------------------------------------------------------------------------
  cGetOrderPositionData =
    'select t_op.c_order_position_name, t_op.c_order_id, t_op.c_style_id, ' +
    't_s.c_style_name, t_s.c_slip, t_op.c_m_to_prod, t_s.c_nr_of_threads, t_op.c_act_yarncnt ' +
    'from t_order_position t_op, t_style t_s ' +
    'where t_op.c_order_position_id = :c_order_position_id ' +
    'and   t_op.c_style_id = t_s.c_style_id';
//------------------------------------------------------------------------------
  cGetStyleData =
    'select * from t_style ' +
    'where c_style_id =:c_style_id';
//------------------------------------------------------------------------------
  cQueryMachineOnline =
    'select m.c_machine_id ' +
    'from t_net_node nn, t_machine m ' +
    'where m.c_machine_id = :c_machine_id ' +
    'and   nn.c_node_id = m.c_node_id ' +
    'and   nn.c_node_stat in ( 1 )';
//------------------------------------------------------------------------------
  cQueryMachineInProduction =
    'select c_machine_id ' +
    'from t_prodgroup_state ' +
    'where c_prod_state in(1,3,5,6) ' +
    'and c_machine_id =:c_machine_id ';

//------------------------------------------------------------------------------
{ TProdGrpList }
//------------------------------------------------------------------------------
constructor TSpindleRangeList.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  mComponentList := TmmList.Create;
  mProdGrpList := TStringList.Create;
    // create my own window handle to receive windows messages
  mWindowHandle := AllocateHWnd(WndProc);
  mAssWindowMsg := RegisterWindowMessage(ASSIGN_REG_STR);
  fMachineID := 0;
  fAssMachine := nil;
  fQuery := nil;
  fOnItemValueChanged := nil;
  fOnItemFocusChanged := nil;
  mProdGrpCursor := -1;
  fFirst := True;
  fLast := False;
  fChangedItems := [];
  fProdUnit := nil;
  fCallByMachine := True;
  FGettingSettingsFromMachine := False;
end;
//------------------------------------------------------------------------------
destructor TSpindleRangeList.Destroy;
var
  i: Integer;
begin
  DeallocateHWnd(mWindowHandle);
  mComponentList.Free;

  // Produktionsgruppen wieder freigeben !
  for i := 0 to mProdGrpList.Count - 1 do begin
    if assigned(mProdGrpList.Objects[i]) then
      mProdGrpList.Objects[i].Free;
  end;
  mProdGrpList.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.Up;
begin
  if csDesigning in ComponentState then exit;
  if mProdGrpCursor < (mProdGrpList.Count - 1) then begin
    inc(mProdGrpCursor);
    if mProdGrpCursor = mProdGrpList.Count - 1 then
      fLast := True
    else
      fLast := False;
    if mProdGrpCursor > 0 then
      fFirst := False;
    UpdateAllComponents;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.Down;
begin
  if csDesigning in ComponentState then exit;
  if mProdGrpCursor > 0 then begin
    dec(mProdGrpCursor);
    if mProdGrpCursor = 0 then
      fFirst := True
    else
      fFirst := False;
    if mProdGrpCursor < mProdGrpList.Count - 1 then
      fLast := False;
    UpdateAllComponents;
  end;
end;
//------------------------------------------------------------------------------
function TSpindleRangeList.CheckUserValues: boolean;
var x: integer;
  function p(interf: INavChange): boolean;
  begin
    Result := interf.CheckUserValue;
  end;
begin
  Result := True;
  if csDesigning in ComponentState then exit;
  if Assigned(mComponentList) then
    for x := 0 to mComponentList.Count - 1 do begin
      Result := p(INavChange(mComponentList.Items[x]));
      if not Result then
        Exit;
    end;
end;
//------------------------------------------------------------------------------
function TSpindleRangeList.CheckGrpNr(aGrpNr: integer): boolean;
var x: integer;
begin
  Result := False;
  for x := 0 to mProdGrpList.Count - 1 do begin
    if (mProdGrpList.Objects[x] is TProdGrp) then
      if ((mProdGrpList.Objects[x] as TProdGrp).GrpNr = aGrpNr) then
        Result := True;
  end;
end;
//------------------------------------------------------------------------------
function TSpindleRangeList.CheckGrpNr(aGrpNr, aFirstSpd: integer; out
    aNewIndex: Integer): boolean;
var x: integer;
begin
  Result := False;
  for x := 0 to mProdGrpList.Count - 1 do begin
    if (mProdGrpList.Objects[x] is TBaseSpindleRange) then
      if ((mProdGrpList.Objects[x] as TBaseSpindleRange).SpindleFirst=aFirstSpd) then begin
        aNewIndex := x;
        Result := True;
      end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.add(aComponent: INavChange);
begin
  if Assigned(mComponentList) then
    mComponentList.Add(pointer(aComponent));
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.ItemChanged(aChanged: TUserChanges);
var xObject: TObject;
begin
  if csDesigning in ComponentState then exit;
  xObject := getSettingsNav;
  if (aChanged = ucProdUnit) and Assigned(xObject) then begin
    mMiscInfo := TSettingsNav(xObject).MiscInfo;
    UpdateAllComponentsCauseOfProdUnitChange;
    mMiscInfo := Nil; // wss
  end;
  Include(fChangedItems, aChanged);
  if Assigned(OnItemValueChanged) then
    OnItemValueChanged(aChanged, True);
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.ItemFocusChanged(aChanged: TUserChanges);
begin
  if Assigned(OnItemFocusChanged) then
    OnItemFocusChanged(aChanged);
end;
//------------------------------------------------------------------------------
function TSpindleRangeList.getSettingsNav: TObject;
var x: integer;
  xOwner: TComponent;
begin
  Result := nil;
  xOwner := self.Owner;
  for x := 0 to xOwner.ComponentCount - 1 do begin
    if (xOwner.Components[x] is TSettingsNav) then
      Result := TObject(xOwner.Components[x]);
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation = opRemove then begin
      //@@@mComponentList.Remove ( pointer ( AComponent ) );
    if Assigned(fQuery) and (AComponent = fQuery) then
      fQuery := nil;
    if Assigned(fAssMachine) and (AComponent = fAssMachine) then
      fAssMachine := nil;
    if Assigned(fSettingsHandler) and (AComponent = fSettingsHandler) then
      fSettingsHandler := nil;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.WndProc(var msg: TMessage);
begin
  if csDesigning in ComponentState then exit;
  try
    if msg.Msg = mAssWindowMsg then begin
      CodeSite.SendInteger('TSpindleRangeList.WndProc.WParam', msg.WParam);
      case msg.WParam of
        WM_GETSETTINGS_DONE: begin
            mXMLSettingsCollection := SettingsHandler.XMLSettingsCollection;
//wss            mMachSettings := SettingsHandler.SettingsArr;
            fAssMachine.IsConnectedToMachine := True; //Nue:11.12.02
            UpdateAllComponents;
          end;
        WM_NOSETTONGS_NEED: begin
            UpdateAllComponents;
          end;
        WM_GETSETTINGS_TIMEOUT: begin
            UserErrorMsg(cTimeoutMsg, Owner);

            if GettingSettingsFromMachine then   //Message Ausl�sen (FixSpdRange) um allf�llig sichtbares AskAssign-Window zu canceln. Nue:30.11.05
                SendMessage(mGettingSettingsFromMachineCaller, mAssWindowMsg, WM_GETSETTINGS_TIMEOUT, 0);
          end;
        WM_GETSETTINGS_FAILED: begin

            if GettingSettingsFromMachine then   //Message Ausl�sen (FixSpdRange) um allf�llig sichtbares AskAssign-Window zu canceln. Nue:30.11.05
                SendMessage(mGettingSettingsFromMachineCaller, mAssWindowMsg, WM_GETSETTINGS_TIMEOUT, 0);

//Alt bis 10.12.02              UserErrorMsg (cSettingsNotOK, Owner );
//             mMachSettings := SettingsHandler.SettingsArr;  //Neu
            fAssMachine.IsConnectedToMachine := False; //Nue:11.12.02
            UpdateAllComponents; //Nue:10.12.02
          end;
      else
      end;
      Exit;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TSpindleRangeList.WndProc failed. ' + e.Message);
      Exit;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.SetActSpdRange(aSpdRange: TMachine);
begin
  if csDesigning in ComponentState then exit;
  fMachineID := aSpdRange.MachineID;
  if Assigned(fAssMachine) then
    fAssMachine.MachineID := fMachineID;
  if (aSpdRange is TBaseSpindleRange) then
    if (aSpdRange as TBaseSpindleRange).NoRunningProdGrp then
      UpdateProdGrpList((aSpdRange as TBaseSpindleRange).SpindleFirst)
//        UpdateProdGrpList
    else
      UpdateProdGrpList((aSpdRange as TBaseSpindleRange).SpindleFirst)
  else
    UpdateProdGrpList;

  mXMLSettingsCollection := Nil;
//wss  mMachSettings := nil;

//Ausgeklammert Nue:19.04.05 auf Wunsch Kast
//Wieder eingebaut aber gez�gelt nach TAssign.acAssignExecute Nue:28.9.05
//  if (fAssMachine.IsOnline) and (fAssMachine.FixSpdRanges) then begin
//      // get the Settings and the Spindle Ranges of the Machine
//    if not (SettingsHandler.StartGetSettings(mWindowHandle, True)) then begin
//      SendMessage(mWindowHandle, mAssWindowMsg, WM_NOSETTONGS_NEED, 0); //@@Nue
//    end;
//  end
//  else
    SendMessage(mWindowHandle, mAssWindowMsg, WM_NOSETTONGS_NEED, 0);
end;
//------------------------------------------------------------------------------
function TSpindleRangeList.GetActSpdRange: TMachine;
begin
  Result := Nil;
  if csDesigning in ComponentState then
    Exit;

  if mProdGrpList.Count > 0 then
    Result := mProdGrpList.Objects[mProdGrpCursor] as TMachine;
end;

//------------------------------------------------------------------------------
procedure TSpindleRangeList.GetSettingsFromMachine(aHwnd: HWND);
begin
  // get the Settings and the Spindle Ranges of the Machine

  GettingSettingsFromMachine := True;
  mGettingSettingsFromMachineCaller := aHwnd;

  if not (SettingsHandler.StartGetSettings(mWindowHandle, True)) then begin
    SendMessage(mWindowHandle, mAssWindowMsg, WM_NOSETTONGS_NEED, 0); //@@Nue
    GettingSettingsFromMachine := False;
  end;
end;

//------------------------------------------------------------------------------
procedure TSpindleRangeList.UpdateProdGrpList;
var x: integer;
  xSpdRange: TBaseSpindleRange;

//  procedure Check(aIndex, aSpindleLast: integer);
//  var xNewSpdRange: TSpindleRange;
//    xSpdRange: TBaseSpindleRange;
//    xSpdLast: integer;
//    xCount: integer;
//  begin
//    if aIndex < 0 then begin
//      if mProdGrpList.Count > 0 then begin
//        xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[0]);
////Test        xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[mProdGrpList.Count-1]);
//        xSpdLast := xSpdRange.SpindleFirst - 1;
//      end
//      else begin
//        xSpdLast := aSpindleLast;
//      end;
//      if xSpdLast >= 1 then begin
//        if mProdGrpList.Count > 0 then begin   //Nue: 25.8.03
//          xNewSpdRange := TSpindleRange.Create(fQuery);
//          xNewSpdRange.Init(AssMachine.MachineID, 1, xSpdLast);
//          mProdGrpList.InsertObject(0, '', xNewSpdRange);
//        end
//        else begin
//{ DONE 1 -oNue -cXMLSettings : Auff�llen der TK-Bereiche bei einer leeren MAschine }
//          //Nue: 21.8.03 Auff�llen der TK-Bereiche bei einer leeren MAschine
//
//          with fAssMachine.MaConfigReader do begin
// codesite.sendstring('mProdGrpList.Count= 0: ',FormatXML(fAssMachine.MaConfigReader.DOM));
//            xCount := 0;
//            while (GroupValueDef[xCount,cXPSpindleFromItem,0] <> 0) and (GroupValueDef[xCount,cXPSpindleFromItem,0] <> $FF) do begin
//              xNewSpdRange := TSpindleRange.Create(fQuery);
//              xNewSpdRange.Init(AssMachine.MachineID, GroupValueDef[xCount,cXPSpindleFromItem,1], GroupValueDef[xCount,cXPSpindleToItem,1]);
//              mProdGrpList.InsertObject(xCount, '', xNewSpdRange);
//              if GroupValueDef[xCount,cXPSpindleFromItem,1]=mTKSpindleFirst then
//                mProdGrpCursor := xCount;
//              inc(xCount);
//            end; //while
//          end; //with
//
////NUE1
////          with fAssMachine.MachineYMConfigRec do begin
////            xCount := 1;
////            while (spec[xCount].spindle.start <> 0) and (spec[xCount].spindle.start <> $FF) do begin
////              xNewSpdRange := TSpindleRange.Create(fQuery);
////              xNewSpdRange.Init(AssMachine.MachineID, spec[xCount].spindle.start, spec[xCount].spindle.stop);
////              mProdGrpList.InsertObject(xCount-1, '', xNewSpdRange);
////              if spec[xCount].spindle.start=mTKSpindleFirst then
////                mProdGrpCursor := xCount-1;
////              inc(xCount);
////            end; //while
////          end; //with
//        end; //else
//      end;
//      Exit;
//    end;
//
//    xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[aIndex]);
//    if xSpdRange.SpindleLast < aSpindleLast then begin
//
//      xSpdLast := aSpindleLast;
//
//      if NOT(fAssMachine.MaConfigReader.IsSameSection(xSpdRange.SpindleLast + 1, aSpindleLast)) then begin
//      //Im leeren Bereich hat es verschiedene Sektionen
//         xSpdLast := fAssMachine.MaConfigReader.LastGroupSpindleFromSpindle(xSpdRange.SpindleLast + 1, False);
//         if xSpdLast=0 then
//           xSpdLast := aSpindleLast;
//      end; //if NOT
//
//      xNewSpdRange := TSpindleRange.Create(fQuery);
//      xNewSpdRange.Init(AssMachine.MachineID, xSpdRange.SpindleLast + 1, xSpdLast);
//      mProdGrpList.InsertObject(aIndex + 1, '', xNewSpdRange);
//      if fAssMachine.MaConfigReader.GroupValueDef[aIndex + 1,cXPSpindleFromItem,1]=mTKSpindleFirst then   //Nue:30.05.06
//        mProdGrpCursor := aIndex + 1;  //Nue:30.05.06
////Org      Check(aIndex, xNewSpdRange.SpindleFirst - 1);
//      Check(aIndex+1, AssMachine.NumSpindles);
//    end
//    else begin
//      Check(aIndex - 1, xSpdRange.SpindleFirst - 1);
//    end;
//  end;
//
  procedure Check(aIndex, aSpindleLast: integer);
  var xNewSpdRange: TSpindleRange;
    xSpdRange: TBaseSpindleRange;
    xSpdLast: integer;
    xCount: integer;
    xMaSpdlFrom: Integer;
    xMaSpdlTo: Integer;
    xRunSpdls: set of 1..cMaxSpindeln;
    xCount1: Integer;
    xNewStartGrp: Integer;
    xNewEndGrp: Integer;

//----local local -----------------------------------
  procedure InsertNewGrp(aNewStartGrp: Integer; aNewEndGrp: Integer);
  var
    xSpdLast: Integer;
  begin
    xSpdLast := 0;

    if NOT((fAssMachine.MaConfigReader.FirstGroupSpindleFromSpindle(aNewStartGrp, False)=0) and
       (fAssMachine.MaConfigReader.FirstGroupSpindleFromSpindle(aNewEndGrp, False)=0)) then begin
    //Kein undefinierter Bereich

      while (xSpdLast=0) AND (NOT(fAssMachine.MaConfigReader.IsSameSection(aNewStartGrp, aNewEndGrp))) do begin
      //Im leeren Bereich hat es verschiedene Sektionen
        xSpdLast := fAssMachine.MaConfigReader.LastGroupSpindleFromSpindle(aNewStartGrp, False);
        if xSpdLast=0 then begin
          Inc(aNewStartGrp);
        end
        else begin
          InsertNewGrp(xSpdLast+1, aNewEndGrp);
          aNewEndGrp := xSpdLast;
        end;
      end; //while

      xNewSpdRange := TSpindleRange.Create(fQuery);
      xNewSpdRange.Init(AssMachine.MachineID, aNewStartGrp, aNewEndGrp);
      mProdGrpList.InsertObject(aIndex + 1, '', xNewSpdRange);
      CodeSite.SendFmtMsg('New empty range in mProdGrpList added: %d -%d', [aNewStartGrp, aNewEndGrp]);
  //    if fAssMachine.MaConfigReader.GroupValueDef[aIndex + 1,cXPSpindleFromItem,1]=mTKSpindleFirst then begin  //Nue:30.05.06
      if fAssMachine.MaConfigReader.IsSameSection(aNewStartGrp, mTKSpindleFirst) then begin
        mProdGrpCursor := aIndex + 1;  //Nue:30.05.06
        CodeSite.SendFmtMsg('mProdGrpCursor on range: %d -%d', [aNewStartGrp, aNewEndGrp]);
      end;
      INC(aIndex);
    end;
  end;
//----end local local -----------------------------------

  begin
    if aIndex < 0 then begin
      //Leere Maschine
      if mProdGrpList.Count > 0 then begin
        xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[0]);
//Test        xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[mProdGrpList.Count-1]);
        xSpdLast := xSpdRange.SpindleFirst - 1;
      end
      else begin
        xSpdLast := aSpindleLast;
      end;
      if xSpdLast >= 1 then begin
        if mProdGrpList.Count > 0 then begin   //Nue: 25.8.03
          xNewSpdRange := TSpindleRange.Create(fQuery);
          xNewSpdRange.Init(AssMachine.MachineID, 1, xSpdLast);
          mProdGrpList.InsertObject(0, '', xNewSpdRange);
        end
        else begin
{ DONE 1 -oNue -cXMLSettings : Auff�llen der TK-Bereiche bei einer leeren MAschine }
          //Nue: 21.8.03 Auff�llen der TK-Bereiche bei einer leeren MAschine

          with fAssMachine.MaConfigReader do begin
 codesite.sendstring('mProdGrpList.Count= 0: ',FormatXML(fAssMachine.MaConfigReader.DOM));
            xCount := 0;
            while (GroupValueDef[xCount,cXPSpindleFromItem,0] <> 0) and (GroupValueDef[xCount,cXPSpindleFromItem,0] <> $FF) do begin
              xNewSpdRange := TSpindleRange.Create(fQuery);
              xNewSpdRange.Init(AssMachine.MachineID, GroupValueDef[xCount,cXPSpindleFromItem,1], GroupValueDef[xCount,cXPSpindleToItem,1]);
              mProdGrpList.InsertObject(xCount, '', xNewSpdRange);
//Nue:17.06.08 Abfrage rausgenommen, weil mTKSpindleFirst immer 0 war ????
//              if GroupValueDef[xCount,cXPSpindleFromItem,1]=mTKSpindleFirst then
                mProdGrpCursor := xCount;
              inc(xCount);
            end; //while
          end; //with

//NUE1
//          with fAssMachine.MachineYMConfigRec do begin
//            xCount := 1;
//            while (spec[xCount].spindle.start <> 0) and (spec[xCount].spindle.start <> $FF) do begin
//              xNewSpdRange := TSpindleRange.Create(fQuery);
//              xNewSpdRange.Init(AssMachine.MachineID, spec[xCount].spindle.start, spec[xCount].spindle.stop);
//              mProdGrpList.InsertObject(xCount-1, '', xNewSpdRange);
//              if spec[xCount].spindle.start=mTKSpindleFirst then
//                mProdGrpCursor := xCount-1;
//              inc(xCount);
//            end; //while
//          end; //with
        end; //else
      end;
      Exit;
    end;

    xSpdRange := TBaseSpindleRange(mProdGrpList.Objects[aIndex]);
////    if xSpdRange.SpindleLast < aSpindleLast then begin
    if xSpdRange.SpindleLast <= aSpindleLast then begin

      fAssMachine.MaConfigReader.GetMachSpindleRange(xMaSpdlFrom, xMaSpdlTo);
      //Alle laufenden Spindeln in Set f�llen
      for xCount := 0 to aIndex do begin
        for xCount1 := (mProdGrpList.Objects[xCount] as TBaseSpindleRange).SpindleFirst to (mProdGrpList.Objects[xCount] as TBaseSpindleRange).SpindleLast do begin
          xRunSpdls := xRunSpdls + [xCount1];
        end;
      end;

      if NOT(xRunSpdls=[xMaSpdlFrom..xMaSpdlTo]) then begin
        xNewStartGrp := 0;
        xNewEndGrp := 0;
        for xCount := xMaSpdlFrom to xMaSpdlTo do begin
          if xNewStartGrp=0 then begin
            if not(xCount in xRunSpdls) then begin
              xNewStartGrp := xCount;
              xNewEndGrp := xCount;
            end;
          end
          else begin
            if not(xCount in xRunSpdls) then begin
              xNewEndGrp := xCount;
            end
            else begin
              InsertNewGrp(xNewStartGrp, xNewEndGrp);
              xNewStartGrp := 0;
              xNewEndGrp := 0;
            end;
          end;
        end; //for

        if xNewStartGrp<>0 then
          InsertNewGrp(xNewStartGrp, xNewEndGrp);

      end; //if NOT
    end; //if
  end; //Check

begin
  if csDesigning in ComponentState then exit;
  try
      // clean the list
    for x := 0 to mProdGrpList.Count - 1 do
      mProdGrpList.Objects[x].Free;
    mProdGrpList.Clear;
    mProdGrpCursor := 0;
    if not Assigned(fQuery) then
      raise Exception.Create('No Query assigned.');
      // fill up the new ProdGrps into the List
    with Query do begin
      Close;
      CommandText := cGetRunProdGrpsOfMachine;
      Parameters.ParamByName('c_machine_id').Value := fMachineID;
      Open;
      while (not EOF) do begin // ADO Conform
        xSpdRange := TProdGrp.Create(fQuery);
        (xSpdRange as TProdGrp).ProdGrpID := FieldByName('c_prod_id').AsInteger;
        mProdGrpList.AddObject('', xSpdRange);
        Next;
      end;

        // Now check if there are free spindle ranges, fill them up
      Check(mProdGrpList.Count - 1, AssMachine.NumSpindles);

      fFirst := True;
      fLast := False;
      if mProdGrpList.Count = 1 then begin
        fFirst := True;
        fLast := True;
      end;
      if mProdGrpList.Count = 0 then begin
        fLast := True;
        mProdGrpCursor := -1;
      end;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TSpindleRangeList.UpdateProdGrpList failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.UpdateProdGrpList(aSpindleFirst: integer);
var x: integer;
  xSpdRange: TBaseSpindleRange;
begin
  if csDesigning in ComponentState then exit;
  mTKSpindleFirst := aSpindleFirst;  //Nue:21.8.03
  UpdateProdGrpList;
  for x := 0 to mProdGrpList.Count - 1 do begin
    xSpdRange := (mProdGrpList.Objects[x] as TBaseSpindleRange);
    if xSpdRange.SpindleFirst = aSpindleFirst then
      mProdGrpCursor := x;
  end;
  SetFirstLastFlags;
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.SetFirstLastFlags;
begin
  fFirst := False;
  fLast := False;
  if mProdGrpCursor = 0 then
    fFirst := True;
  if mProdGrpCursor = mProdGrpList.Count - 1 then
    fLast := True;
end;

procedure TSpindleRangeList.SetGettingSettingsFromMachine(const Value: Boolean);
begin
  FGettingSettingsFromMachine := Value;
  fSettingsHandler.GettingSettingsFromMachine := Value;  //Flag, damit im GetSettingsHandler AllXMLValues ein und ausgeschaltet werden k�nnen.
end;

//------------------------------------------------------------------------------
procedure TSpindleRangeList.UpdateAllComponents;
var x: integer;
  procedure p(interf: INavChange);
  begin
    if mProdGrpList.Count > 0 then
      interf.UpdateComponent((mProdGrpList.Objects[mProdGrpCursor] as TBaseSpindleRange), mXMLSettingsCollection, nil)
//wss      interf.UpdateComponent((mProdGrpList.Objects[mProdGrpCursor] as TBaseSpindleRange), mMachSettings, nil)
    else
      interf.UpdateComponent(nil, nil, nil);
  end;
begin
  if csDesigning in ComponentState then exit;
  if Assigned(mComponentList) then
    for x := 0 to mComponentList.Count - 1 do begin
      p(INavChange(mComponentList.Items[x]));
    end;
//    fProdUnit := nil; //Nue 21.12.2000
  fChangedItems := [];
  if Assigned(OnItemValueChanged) then  
    OnItemValueChanged(ucNone, False);
end;
//------------------------------------------------------------------------------
procedure TSpindleRangeList.UpdateAllComponentsCauseOfProdUnitChange;
var x: integer;
  procedure p(interf: INavChange);
  begin
    if mProdGrpList.Count > 0 then
      interf.UpdateComponent(nil, nil, mMiscInfo)
    else
      interf.UpdateComponent(nil, nil, nil);
  end;
begin
  if csDesigning in ComponentState then exit;
  if Assigned(mComponentList) then
    for x := 0 to mComponentList.Count - 1 do begin
      p(INavChange(mComponentList.Items[x]));
    end;
  fProdUnit := nil; //Nue 21.12.2000
end;
//------------------------------------------------------------------------------
{ TAssMachine }
//------------------------------------------------------------------------------
constructor TMachine.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
//  fMachineAttributes    := nil;
  fMachineID            := cUninitializedMachine;
  fMachineName          := '';
  fNumSpindles          := 0;
  fNetTyp               := ntNone;
  fLengthMode           := cDefaultLengthMode;
  fIsConnectedToMachine := True;
//  codesite.SendNumAsHexEx(csmObject, 'Create von TMachine', cardinal(Pointer(self)));
//  fSpeedRamp := 0;
//  fSpeed := 0;
end;
//------------------------------------------------------------------------------
destructor TMachine.Destroy;
begin
//  FreeAndNil(fMachineAttributes);
  FreeAndNil(fMaConfigReader);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TMachine.Assign(aSource: TPersistent);
var xSrc: TMachine;
begin
  if aSource is TMachine then begin
    xSrc   := aSource as TMachine;
    fQuery := xSrc.Query;

    fMachineName    := xSrc.MachineName;
    fMachineID      := xSrc.MachineID;
    fNumSpindles    := xSrc.NumSpindles;
    fNetTyp         := xSrc.NetTyp;
    fLengthWindow   := xSrc.LengthWindow;
    fLengthMode     := xSrc.LengthMode;
    fFixSpdRanges   := xSrc.FixSpdRanges;
    fCheckNewestMapfile := xSrc.CheckNewestMapfile; //Nue:27.11.06
    fSlip           := xSrc.Slip;
    fOverruleMapID  := xSrc.OverruleMapID;
    fNodeID         := xSrc.NodeID;
    fMaOffsetID     := xSrc.MaOffsetID;
    fStarting       := xSrc.Starting;
    fDataCollection := xSrc.DataCollection;
    fSpindlePos     := xSrc.SpindlePos;
    fMachineType    := xSrc.MachineType;
//NUE1    fMachineManuf   := xSrc.MachineManuf;
//NUE1    fMachineModel   := xSrc.MachineModel;
//NUE1    fMTSpindPos     := xSrc.MTSpindPos;
    //DONE wss: f�r diese Properties m�sste noch eine andere L�sung f�r den Zugriff erfolgen,
    //          da diese Infos im XML sind und nicht direkt auf der DB
    fSpeed          := xSrc.Speed;
    fSpeedRamp      := xSrc.SpeedRamp;
    fOnlineSpeed    := xSrc.OnlineSpeed;
    fYMType         := xSrc.YMType;  //Nue: 8.2.07
{
    fSpeed := xSrc.Speed;
    fSpeedRamp := xSrc.SpeedRamp;
    fMachineConfigA := xSrc.MachineConfigA;
    fMachineConfigB := xSrc.MachineConfigB;
    fMachineConfigC := xSrc.MachineConfigC;
    fLsInProd := xSrc.LsInProd;
    fLsBreak := xSrc.LsBreak;
    fLsWaitMat := xSrc.LsWaitMat;
    fLsWairRev := xSrc.LsWairRev;
    fLsClean := xSrc.LsClean;
    fLsFree := xSrc.LsFree;
    fLsDefine := xSrc.LsDefine;
    fInopSettings0 := xSrc.InopSettings0;
    fInopSettings1 := xSrc.InopSettings1;
    fOnlineSpeed := xSrc.OnlineSpeed;
    fLongStopDef := xSrc.LongStopDef;
    fYMVersion := xSrc.YMVersion;
    fAWEMachType := xSrc.AWEMachType;
    fLastConfirmedUpload := xSrc.LastConfirmedUpload; //22.01.03 Nue
    fCutRetries := xSrc.CutRetries;
    fCheckLen := xSrc.CheckLen;
{}
  end
  else
    inherited Assign(aSource);
end;

//------------------------------------------------------------------------------
procedure TMachine.OpenQuery(aMachID: integer; aSQLStr: string);
begin
  try
    if not Assigned(fQuery) then
      raise Exception.Create('Query not assigned.');
    with fQuery do begin
      Close;
      CommandText := aSQLStr;
      Parameters.ParamByName('c_machine_id').value := aMachID;
      Open;
      if EOF then // ADO Conform
        raise Exception.Create('No Machine available with Machine ID = ' + IntToStr(aMachID));
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TMachine.OpenQuery failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachine.InitProperties(aMachID: integer);
var
  xID: Integer;
  xVariant: Variant;
begin
  with fQuery do
  try
    fLengthWindow            := TMMSettingsReader.Instance.Value[cYMLenWindow];
    fYMType                  := ftNone;   //Nue: 8.2.07 TFrontType
    fLengthMode              := cDefaultLengthMode;
    fSpeed                   := cDefaultSpeed;
    fSpeedRamp               := cDefaultSpeedRamp;
    fMachineID               := aMachID;
    fMachineName             := FieldByName('c_machine_name').AsString;
    fNumSpindles             := FieldByName('c_nr_of_spindles').AsInteger;
    fNetTyp                  := TNetTyp(FieldByName('c_net_id').AsInteger);
    fNodeID                  := FieldByName('c_node_id').AsString;
    fMaOffsetID              := FieldByName('c_maoffset_id').AsInteger;
    fFrontType               := TFrontType(FieldByName('c_front_type').AsInteger);
    fFixSpdRanges            := FieldByName('c_fixSpindlerange').AsBoolean;
    fCheckNewestMapfile      := FieldByName('c_use_newer_mapfile').AsBoolean;  //Nue:27.11.06
    fSlip                    := CheckSlip(FieldByName('c_slip').AsInteger);
    fStarting                := FieldByName('c_starting').AsDateTime;
    fDataCollection          := FieldByName('c_data_collection').AsInteger;
    fSpindlePos              := FieldByName('c_spindle_pos').AsInteger;
    fMachineType             := TMachineType(FieldByName('c_machine_type').AsInteger);
//NUE1    fMachineManuf            := FieldByName('c_machine_manuf').AsString;
//NUE1    fMachineModel            := FieldByName('c_machine_model').AsString;
//NUE1    fMTSpindPos              := FieldByName('c_spind_pos').AsInteger;
//NUE1    fDefaultNetTypforMachTyp := TNetTyp(FieldByName('c_default_net_id').AsInteger);
    xVariant                 := FieldByName('c_overrule_mapfile_id').Value;
    if VarIsNull(xVariant) then fOverruleMapID := 0
                           else fOverruleMapID := xVariant;

    //Neu ab V5.00
    fUploaded := False;
    xID       := FieldByName('c_magroup_config_id').AsInteger;
    if xID > 0 then begin
      //Wenn Upload get�tigt wurde -> holen des Datums
      Close;
      CommandText := cQrySelMachConfig;
      Parameters.ParamByName('c_magroup_config_id').value := xID;
      Open;
      if FindFirst then begin
        fUploaded            := True;
        fLastConfirmedUpload := FieldByName('c_date').AsDateTime;
      end;
    end;
{wss: f�r XML nicht mehr ben�tigt
    fMachineAttributes       := TMachineAttributes.Create(fInopSettings0, fInopSettings1);
    fYMOption                := LOW(TFrontSwOption);
    fSpeedRamp           := FieldByName('c_speedRamp').AsInteger;
    fSpeed               := FieldByName('c_opt_speed').AsInteger;
    fMachineConfigA      := FieldByName('c_configA').AsInteger;
    fMachineConfigB      := FieldByName('c_configB').AsInteger;
    fMachineConfigC      := FieldByName('c_configC').AsInteger;
    fInopSettings0       := FieldByName('c_inopsettings0').AsInteger;
    fInopSettings1       := FieldByName('c_inopsettings1').AsInteger;
    fLastConfirmedUpload := FieldByName('c_last_confirmed_upload').AsDateTime; //22.1.03 Nue
    fOnlineSpeed         := FieldByName('c_online_speed').AsBoolean;
    fLsInProd            := FieldByName('c_ls_in_prod').AsInteger;
    fLsBreak             := FieldByName('c_ls_break').AsInteger;
    fLsWaitMat           := FieldByName('c_ls_wait_mat').AsInteger;
    fLsWairRev           := FieldByName('c_ls_wait_rev').AsInteger;
    fLsClean             := FieldByName('c_ls_clean').AsInteger;
    fLsFree              := FieldByName('c_ls_free').AsInteger;
    fLsDefine            := FieldByName('c_ls_define').AsInteger;
    fLongStopDef         := FieldByName('c_longStopDef').AsInteger;
    fCutRetries          := FieldByName('c_cutRetries').AsInteger;
    fCheckLen            := FieldByName('c_checkLen').AsInteger;
    fInopSettings0       := FieldByName('c_inopSettings0').AsInteger;
    fInopSettings1       := FieldByName('c_inopSettings1').AsInteger;
    xStr                 := FieldByName('c_optionCode').AsString;
    for i := ORD(fSwOBasic) to ORD(fSwOEnd) do begin
      if (xStr = cFrontSwOption[TFrontSwOption(i)]) then begin
        fYMOption := TFrontSwOption(i);
        break;
      end;
    end;
    fYMVersion               := FieldByName('c_YM_version').AsString;
    fAWEMachType             := TAWEMachType(FieldByName('c_AWE_mach_type').AsInteger);
{}
  except
    on e: Exception do begin
      raise Exception.Create('TMachine.InitProperties failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMachine.SetMachineID(aMachID: integer);
//var
//  xQuery2: TmmAdoDataSet;
begin
  if aMachID <> fMachineID then begin   // LOK
    try
  //alt      OpenQuery ( aMachID, cQueryMachInfo );
//    codesite.SendNumAsHexEx(csmObject, 'Instanz von TMachine', cardinal(Pointer(self)));

      OpenQuery(aMachID, cQueryMachInfoTot);
      InitProperties(aMachID);
      fUploadTimeout := False;

(*      if not(assigned(FModel)) then begin // LOK
        FModel := TVCLXMLSettingsModel.Create(Self);  //Create Model
        codesite.SendNumAsHexEx(csmCheckPoint, 'Modell TMaschine (SetMachineID)', cardinal(Pointer(FModel)));
      end;// if not(assigned(FModel)) then begin
      FModel.MaConfigReader.MachID := aMachID;   //Zuweisen der MachID, damit im Model der Bauzustand ab der DB abgelegt wird.*)

      MaConfigReader.MachID := aMachID;

      //Initialisieren der Length-Felder
      //Die Werte, mit welchen eine neue Partie gestartet wird, werden aber schlussendlich per SettingsReader ausgelesen.
      //  Diese Werte gelten ja fabrikweit und k�nnen pro Partiestart im Assignment (AddYMSettingsForm) angepasst werden).
      //  Dabei werden diese Werte sogleich f�r die ganze Maschine �bernommen (auf ZE, WSC, usw.)
      //wss: wenn Available=False dann wurden diese Felder in InitProperties initialisiert 
      if MaConfigReader.Available then begin
        fLengthWindow := MaConfigReader.MachValueDef[cXPLengthWindowItem, fLengthWindow];
        fLengthMode   := Ord(GetLengthWindowModeValue(MaConfigReader.MachValueDef[cXPLengthModeItem, GetEnumName(TypeInfo(TLengthWindowMode), fLengthMode)]));

        //Initialisieren der Speed-Felder mit den Werten der ersten Sektion (0)
        //Die Werte, mit welchen eine neue Partie gestartet wird, werden aber schlussendlich vom AddYMSettingsForm ausgelesen.
        //  Diese Werte gelten sektionsweit
        fSpeed        := MaConfigReader.GroupValueDef[0, cXPSpeedItem,     fSpeed];
        fSpeedRamp    := MaConfigReader.GroupValueDef[0, cXPSpeedRampItem, fSpeedRamp];

        fYMType       := GetFrontTypeValue(MaConfigReader.MachValueDef[cXPYMTypeItem, GetEnumName(TypeInfo(TLengthWindowMode), Ord(FYMType))]);  //Nue: 8.2.07 ntNone

//        fLengthWindow := MaConfigReader.MachValueDef[cXPLengthWindowItem, cDefaultLengthWindow];
//        fLengthMode   := Ord(GetLengthWindowModeValue(MaConfigReader.MachValueDef[cXPLengthModeItem,
//                                                      GetEnumName(TypeInfo(TLengthWindowMode), cDefaultLengthMode)]));
//
//        //Initialisieren der Speed-Felder mit den Werten der ersten Sektion (0)
//        //Die Werte, mit welchen eine neue Partie gestartet wird, werden aber schlussendlich vom AddYMSettingsForm ausgelesen.
//        //  Diese Werte gelten sektionsweit
//        fSpeed        := MaConfigReader.GroupValueDef[0, cXPSpeedItem, cDefaultSpeed];
//        fSpeedRamp    := MaConfigReader.GroupValueDef[0, cXPSpeedRampItem, cDefaultSpeedRamp];
      end;

  //NUE1
  //    xQuery2 := TmmAdoDataset.Create(nil);
  //    xQuery2.Assign(Query); //Used to get infos (DB) from already established mmQuery

  //    with TSettingsDBAccess.Create {(xQuery2)} do
  //    try
  //      fMachineYMConfigRec := GetMachineYMConfig(aMachID);
  //    finally
  //      Free;
  ////      FreeAndNil(xQuery2);
  //    end;

    except
      on e: Exception do begin
        raise Exception.Create('TMachine.SetMachineID failed: ' + e.Message);
      end;
    end;
  end;// if aMachID <> fMachineID then begin
end;
//------------------------------------------------------------------------------
function TMachine.GetIsReadyToAssign: boolean;
begin
  Result := GetIsOnline;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsOnline: boolean;
begin
//  Result := True;
  with fQuery do
  try
    Close;
    CommandText := cQueryMachineOnline;
    Parameters.ParamByName('c_machine_id').value := fMachineID;
    Open;
    Result := not EOF; // ADO Conform
    Close;
  except
    on e: Exception do begin
      raise Exception.Create('TAssMachine.GetIsOnline failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsInProduction: boolean;
begin
//  Result := True;
  with Query do
  try
    Close;
    CommandText := cQueryMachineInProduction;
    Parameters.ParamByName('c_machine_id').value := MachineID;
    Open;
    Result := not EOF; // ADO Conform
    Close;
  except
    on e: Exception do begin
      raise Exception.Create('TAssMachine.GetIsInProduction failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
                       
function TMachine.GetNumOfGrps: integer;
begin
  case NetTyp of
    ntWSC: Result := cWSCSpdGroupLimit;
    ntLX:  Result := cLXSpdGroupLimit;
  else
    Result := cZESpdGroupLimit;
  end;
//wss  case AWEMachType of
//    amtAC338: Result := 6;
//  else
//    Result := 12;
//  end;

//Alt bis 4.4.02 Nue    case FrontType of
//      ftSWSInformatorSBC5 : Result := 6;
//    else
//      Result := 12;
//    end;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsReadyToClose: boolean;
begin
  Result := True;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsReadyToGetMachineSettings: boolean;
begin
  Result := GetIsOnline;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsAbleToGetMachineSettings: boolean;
begin
//Alt bis 11.12.02   Result := true;
  Result := fIsConnectedToMachine; //Nue:11.12.02
    {
    case MachineTyp of
      mtAC338, mtAC338Spectra : Result := true
    else
      Result := false;
    end;
    }
end;
//------------------------------------------------------------------------------
function TMachine.GetIsAbleToClose: boolean;
begin
  Result := not(fNetTyp in cNetTypeEthernet);
//  Result := True;
//  case MachineType of
//    mtAC338: Result := False;
//  end;

//Alt bis 4.4.02 Nue   case FrontType of
//      ftSWSInformatorSBC5 : Result := false;
//    end;
end;
//------------------------------------------------------------------------------
function TMachine.GetIsAbleToOverstartMaGroup: boolean;
begin
  Result := True;
//Seit 29.1.04 folgende Zeilen disabled (Nue in Absprache mit Kast)
//  case AWEMachType of
//    amtAC338: begin
//        if not FixSpdRanges then //Nue: This check new since 4.2.02
//          Result := False;
//      end;
//  end;
end;
//------------------------------------------------------------------------------
procedure TMachine.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then begin
    if Assigned(Query) and (AComponent = Query) then
      fQuery := nil;
  end;
end;
//------------------------------------------------------------------------------
{ TBaseSpindleRange }
//------------------------------------------------------------------------------
constructor TBaseSpindleRange.Create(aQuery: TmmAdoDataset);
begin
  inherited Create(nil);
  fQuery := TmmAdoDataset.Create(nil);
  fQuery.Assign(aQuery);
  fNoRunningProdGrp := True;
end;
//------------------------------------------------------------------------------
destructor TBaseSpindleRange.Destroy;
begin
  FreeAndNil(fQuery);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
{ TSpindleRangeList }
//------------------------------------------------------------------------------
procedure TSpindleRange.Init(aMachineID, aSpindleFirst, aSpindleLast: integer);
begin
  try
    MachineID := aMachineID;
    fSpindleFirst := aSpindleFirst;
    fSpindleLast := aSpindleLast;
    if (fSpindleLast < fSpindleFirst) or (fSpindleFirst < 1) then
      raise Exception.Create(Format('SpindleFirst = %d, SpindleLast = %d', [fSpindleFirst, fSpindleLast]));
    if fNumSpindles < fSpindleLast then
      raise Exception.Create(Format('Spindles out of Range : SpindleLast = %d, NumSpindles = %d', [fSpindleLast, fNumSpindles]));
  except
    on e: Exception do begin
      raise Exception.Create('TSpindleRange.Init failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
{ TAssEdit }
//------------------------------------------------------------------------------
procedure TAssEdit.SetSpdRangeList(aSpdRangeList: TSpindleRangeList);
begin
  fSpdRangeList := aSpdRangeList;
  if Assigned(fSpdRangeList) then
    fSpdRangeList.add(self);
end;
//------------------------------------------------------------------------------
procedure TAssEdit.UpdateComponent(aProdGrp: TBaseSpindleRange; aXMLSettingsCollection: PXMLSettingsCollection; aMiscInfo: IMiscInfo);
//wss procedure TAssEdit.UpdateComponent(aProdGrp: TBaseSpindleRange; aMachSettings: PSettingsArr; aMiscInfo: IMiscInfo);
begin
    // Nothing to do
end;
//------------------------------------------------------------------------------
function TAssEdit.CheckUserValue: boolean;
begin
  Result := True;
end;
//------------------------------------------------------------------------------
procedure TAssEdit.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) then begin
    if Assigned(SpdRangeList) and (AComponent = SpdRangeList) then
      fSpdRangeList := nil;
  end;
end;
//------------------------------------------------------------------------------
{ TProdGrp }
//------------------------------------------------------------------------------
constructor TProdGrp.Create(aQuery: TmmAdoDataset);
begin
  inherited Create(aQuery);
  fProdGrpID := 0;
  fMachineID := 0;
  fSpindleFirst := 0;
  fSpindleLast := 0;
  fSlip := '1.000';
  fYarnCnt := 0;
//  fYarnCntUnit := yuNone;
  fYarnCntUnit   := TYarnUnit (TMMSettingsReader.Instance.Value [cYarnCntUnit]);
//Neu    fYarnCntUnit   := TYarnUnit (TMMSettingsReader.Instance.Value [cYarnCntUnit]);
  fGrpNr := 0;
  fYMSetID := 0;
  fMachLengthMode := cDefaultLengthMode;
  fThreadCnt := 1;
end;
//------------------------------------------------------------------------------
function TProdGrp.getYarnCnt: Single;
begin
  Result := fYarnCnt;
end;
//------------------------------------------------------------------------------
function TProdGrp.getSlip: string;
begin
  Result := fSlip;
end;
//------------------------------------------------------------------------------
function TProdGrp.getYarnCntUnit: TYarnUnit;
begin
  Result := fYarnCntUnit;
end;
//------------------------------------------------------------------------------
function TProdGrp.getThreadCnt: Byte;
begin
  Result := fThreadCnt;
end;
//------------------------------------------------------------------------------
procedure TProdGrp.SetProdGrpID(aProdGrpID: integer);
var
  xSlip: Double;
begin
  if aProdGrpID <> fProdGrpID then begin
    fProdGrpID := aProdGrpID;
    fColor := 0;
    with fQuery do
    try
      Close;
      CommandText := cGetMachineIDQuery;
      Parameters.ParamByName('c_prod_id').value := fProdGrpID;
      Open;
//alt          if EOF then  // ADO Conform
//alt            raise Exception.Create ( 'No MachineID available.' );
//alt          MachineID    := FieldByName ( 'c_machine_id' ).AsInteger;

        //Neu: Nue:26.2.03
      if not EOF then // ADO Conform
        MachineID := FieldByName('c_machine_id').AsInteger;
//13.1.04 Nue: Nachfolgende Infomessage sei �berfl�ssig gem�ss Kast
//      else
//        InfoMsg(cMachineDeleted, TWinControl(Owner));

      Close;
      CommandText := cGetProdGrpDetailData;
      Parameters.ParamByName('c_prod_id').value := fProdGrpID;
      Open;
      if EOF then // ADO Conform
        raise Exception.Create('No ProdGrp Detail Data available.');
      fYMSetID := FieldByName('c_YM_set_id').AsInteger;
      fSpindleFirst := FieldByName('c_spindle_first').AsInteger;
      fSpindleLast := FieldByName('c_spindle_last').AsInteger;
      fPilotSpindles := FieldByName('c_pilotspindles').AsInteger;
//TODO wss: WARUM wird hier der Slip als String nochmals ausgelesen obwohl dieser in der Basisklasse TMaschine als Integer vorhanden ist?
      xSlip := FieldByName('c_slip').AsInteger;
      fSlip := Format('%1.3f', [xSlip / cSlipFactor]);
//Nue:7.3.06      fYarnCntUnit := TYarnUnit(FieldByName('c_yarncnt_unit').AsInteger);
// fYarnCntUnit wird im Creator auf die globale einstellung gesetzt.
      fYarnCnt := YarnCountConvert(yuNm, fYarnCntUnit, FieldByName('c_act_yarncnt').AsFloat);
//      fYarnCnt :=  FieldByName('c_act_yarncnt').AsFloat;  //Neu:7.3.06
      fGrpNr := FieldByName('c_machineGroup').AsInteger;
      fMachSpeed := fSpeed;
      fMachSpeedRamp := fSpeedRamp;
      fMachLengthWindow := fLengthWindow;
      fMachLengthMode := fLengthMode;
      fSpeed := FieldByName('c_speed').AsInteger;
      fSpeedRamp := FieldByName('c_speedRamp').AsInteger;
      fLengthWindow := FieldByName('c_lengthWindow').AsInteger;
      fLengthMode := FieldByName('c_lengthMode').AsInteger;
      fOrderPositionID := FieldByName('c_order_position_id').AsInteger;
      fStyleID := FieldByName('c_style_id').AsInteger;
      fStyleName := FieldByName('c_style_name').AsString;
      fYMSetName := FieldByName('c_ym_set_name').AsString;
      if (FieldByName('c_template_set').AsBoolean = False) then
        fTemplateYMSet := False
      else
        fTemplateYMSet := True;
      fName := FieldByName('c_prod_name').AsString;
      fColor := FieldByName('c_color').AsInteger;
      fStartTime := FieldByName('c_prod_start').AsDateTime;
      fStopTime := FieldByName('c_prod_end').AsDateTime;
      fState := TProdGrpState(FieldByName('c_prod_state').AsInteger);
      fThreadCnt := Byte(FieldByName('c_nr_of_threads').AsInteger);
      NoRunningProdGrp := False;
    except
      on e: Exception do begin
        raise Exception.Create('TProdGrp.SetProdGrpID failed. ' + e.Message);
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TProdGrp.GetMachSpeed: integer;
begin
  Result := fMachSpeed
end;
//------------------------------------------------------------------------------
function TProdGrp.GetMachSpeedRamp: integer;
begin
  Result := fMachSpeedRamp;
end;
//------------------------------------------------------------------------------
function TProdGrp.GetMachLengthWindow: integer;
begin
  Result := fMachLengthWindow;
end;
//------------------------------------------------------------------------------
 {  TOrderPosition }
//------------------------------------------------------------------------------
constructor TOrderPosition.Create(aQuery: TmmAdoDataset);
begin
  inherited Create(nil);
  mQuery := aQuery;
  fOrderPositionID := cDefaultOrderPositionID; //0;
  fOrderID := 0;
  fStyleID := 0;
  fYarnCntUnit := TYarnUnit(TMMSettingsReader.Instance.Value[cYarnCntUnit]);
  mInitialized := False;
end;
//------------------------------------------------------------------------------
procedure TOrderPosition.SetOrderPositionID(aOrderPositionID: integer);
begin
  try
    with mQuery do begin
      Close;
      CommandText := cGetOrderPositionData;
      Parameters.ParamByName('c_order_position_id').value := aOrderPositionID;
      Open;
      if EOF then // ADO Conform
        raise Exception.Create('No data of order positon available ID = ' + IntToStr(aOrderPositionID));
      fOrderID := FieldByName('c_order_id').AsInteger;
      fStyleID := FieldByName('c_style_id').AsInteger;
      fStyleName := FieldByName('c_style_name').AsString;
      fOrderPositionName := FieldByName('c_order_position_name').AsString;
      fMeterToProd := FieldByName('c_m_to_prod').AsFloat;
      fOrderPositionID := aOrderPositionID;
      fYarnCnt := FieldByName('c_act_yarncnt').AsFloat;
      fThreadCnt := Byte(FieldByName('c_nr_of_threads').AsInteger);
        //Slip added: Nue 21.05.01
      if (FieldByName('c_slip').AsFloat / cSlipFactor) <> 0 then
        fSlip := Format('%1.3f', [(FieldByName('c_slip').AsFloat / cSlipFactor)])
      else
        fSlip := '';
      mInitialized := True;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TOrderPosition.SetOrderPositionID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TOrderPosition.SetStyleID(aStyleID: integer);
begin
  try
    with mQuery do begin
      Close;
      CommandText := cGetStyleData;
      Parameters.ParamByName('c_style_id').value := aStyleID;
      Open;
      if EOF then // ADO Conform
        raise Exception.Create('No data of style available ID = ' + IntToStr(aStyleID));
      fStyleName := FieldByName('c_style_name').AsString;
      fStyleID := aStyleID;

      fOrderPositionID := cDefaultOrderPositionID; //bis OrderPosition-Handling implementiert wird
//wss: WAS SOLL DAS?!!!!!      fOrderPositionID := aStyleID;
//     In Version 4.2 wurde wenigstens nach dieser Zeile wieder der Default zugewiesen. Aber warum �berhaupt neu setzen wenn immer Default?
      fOrderID := cDefaultOrderID; //bis OrderPosition-Handling implementiert wird
      fOrderPositionName := FieldByName('c_style_name').AsString;

      fMeterToProd := 0;
//Alt bis 18.3.02 Nue        fYarnCnt           := FieldByName ( 'c_yarncnt' ).AsFloat;
      fYarnCnt := YarnCountConvert(yuNm, TMMSettingsReader.Instance.Value[cYarnCntUnit], FieldByName('c_yarncnt').AsFloat);
      fThreadCnt := Byte(FieldByName('c_nr_of_threads').AsInteger);
        //Slip added: Nue 21.05.01
      if (FieldByName('c_slip').AsFloat / cSlipFactor) <> 0 then
        fSlip := Format('%1.3f', [(FieldByName('c_slip').AsFloat / cSlipFactor)])
      else
        fSlip := '';
      mInitialized := True;
    end;
  except
    on e: Exception do begin
      raise Exception.Create('TOrderPosition.SetStyleID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetStyleID: integer;
begin
  if not mInitialized then
    StyleID := cDefaultStyleID;
  Result := fStyleID;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetStyleName: string;
begin
  if not mInitialized then
    fStyleName := cDefaultStyleName;
  Result := fStyleName;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetOrderPositionID: integer;
begin
  if not mInitialized then
    OrderPositionID := cDefaultOrderPositionID;
  Result := fOrderPositionID;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetOrderID: integer;
begin
  if not mInitialized then
    OrderPositionID := cDefaultOrderPositionID;
  Result := fOrderID;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetMeterToProd: Double;
begin
  if not mInitialized then
    fMeterToProd := 0;
  Result := fMeterToProd;
end;
//------------------------------------------------------------------------------
function TOrderPosition.GetOrderPositionName: string;
begin
  if not mInitialized then
    OrderPositionID := cDefaultOrderPositionID;
  Result := fOrderPositionName;
end;
//------------------------------------------------------------------------------
function TOrderPosition.getYarnCnt: Single;
begin
  Result := fYarnCnt;
end;

function TOrderPosition.getSlip: string;
begin
  Result := fSlip;
end;

function TOrderPosition.getYarnCntUnit: TYarnUnit;
begin
  Result := fYarnCntUnit;
end;

function TOrderPosition.getThreadCnt: Byte;
begin
  Result := fThreadCnt;
end;
//------------------------------------------------------------------------------
 {  TStyle }
//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
 { TSystemPortThread }
//------------------------------------------------------------------------------
constructor TSystemPortThread.Create;
begin
  inherited Create(True);
  fAfterReceive := nil;
  mReader := nil;
end;
//------------------------------------------------------------------------------
destructor TSystemPortThread.Destroy;
begin
  Windows.TerminateThread(self.Handle, 0);
  if Assigned(mReader) then
    mReader.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSystemPortThread.SetPortName(aPortName: string);
begin
  try
    fPortName := aPortName;
    if Assigned(mReader) then
      FreeAndNil(mReader);
    mReader := TIPCServer.Create(aPortName, TMMSettingsReader.Instance.Value[SettingsReader.cUserGroup], sizeof(TResponseRec), 60000, 1);
    if not mReader.Init then
      raise Exception.Create('Init of Reader failed. Error : ' + FormatErrorText(mReader.Error));
    Resume;
  except
    on e: Exception do
      raise Exception.Create('TSystemPortThread.SetPortName failed. ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
procedure TSystemPortThread.Execute;
var
  xMsg: PResponseRec;
  xMsgSize: DWord;
  xRead: DWord;
begin
  try
    xMsgSize := SizeOf(TResponseRec);
    xMsg     := AllocMem(xMsgSize);
    while (not Terminated) do begin
      if not mReader.ReadDynamic(PByte(xMsg), xMsgSize, xRead) then
        raise Exception.Create('Read failed. ' + FormatErrorText(mReader.Error));
      if not Assigned(fAfterReceive) then
        raise Exception.Create('Method not assigned.');
      AfterReceive(xMsg);
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TSystemPortThread.Execute failed. ' + e.Message);
      Windows.ExitProcess(0);
    end;
  end;
end;
//------------------------------------------------------------------------------
 { TBaseSystemPort }
//------------------------------------------------------------------------------
constructor TSystemPort.Create(aOwner: TComponent; aPortName: string);
begin
  inherited Create(aOwner);
  if TMMSettingsReader.Instance.IsPackageEasy then
    fComputerName := '.'
  else
    fComputerName := LoepfeGlobal.MMGetComputerName;

  mWriter              := TIPCClient.Create(gMMHost, cChannelNames[ttJobManager]);
  fProcessMsg          := nil;
  fPortName            := aPortName + IntToStr(GetTickCount);
  if not(csDesigning in ComponentState) then begin
    mThread              := TSystemPortThread.Create;
    mThread.AfterReceive := AfterReceive;
    mThread.PortName     := fPortName;
  end else
    mThread := Nil;
end;
//------------------------------------------------------------------------------
destructor TSystemPort.Destroy;
begin
  mThread.Terminate;
  FreeAndNil(mThread);
  FreeAndNil(mWriter);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TSystemPort.NewJob(aJob: PJobRec);
begin
  aJob^.JobLen := GetJobDataSize(aJob);
  if not mWriter.Write(PByte(aJob), aJob^.JobLen) then
    raise Exception.Create('TBaseSystemPort.NewJob failed. ' + FormatErrorText(mWriter.Error));
end;
//------------------------------------------------------------------------------
procedure TSystemPort.AfterReceive(aReceivedMsg: PResponseRec);
begin
  try
    if Assigned(fProcessMsg) then
      fProcessMsg(aReceivedMsg)
    else
      raise Exception.Create('ProcessMsg method not assigned.');
  except
    on e: Exception do begin
      raise Exception.Create('TAssignSystemPort.AfterReceive failed.' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
 { TBaseHandler }
//------------------------------------------------------------------------------

constructor TBaseHandler.Create(aOwner: TComponent; aPortName: string);
begin
  inherited Create(aOwner);
    // create my own window handle to receive windows messages
  mWindowHandle := AllocateHWnd(WndProc);
  FGetWindowHandle := mWindowHandle;
  mPort := TSystemPort.Create(nil, aPortName);
  mPort.ProcessMsg := OnMsgReceived;
  mTimer := TmmTimer.Create(nil);
  mTimer.OnTimer := OnTimeout;
  mTimer.Enabled := False;
  mState := asNoJob;
  fAssMachine := nil;
end;
//------------------------------------------------------------------------------
constructor TBaseHandler.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  fAssMachine := nil;
  FGetWindowHandle := 0;
end;
//------------------------------------------------------------------------------
destructor TBaseHandler.Destroy;
begin
  DeallocateHWnd(mWindowHandle);
  mPort.Free;
  mTimer.free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TBaseHandler.OnTimeout(aSender: TObject);
begin
  mTimer.Enabled := False;
end;
//------------------------------------------------------------------------------
procedure TBaseHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
begin
  mTimer.Enabled := False;
end;
//------------------------------------------------------------------------------
procedure TBaseHandler.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Assigned(fAssMachine) then
    if (Operation = opRemove) and (AComponent = fAssMachine) then
      fAssMachine := nil;
end;
//------------------------------------------------------------------------------
procedure TBaseHandler.NewJob(aJob: PJobRec);
begin
  mPort.NewJob(aJob);
  mTimer.Enabled := True;
  mState         := asWaitJobRespons;
end;
//------------------------------------------------------------------------------
procedure TBaseHandler.WndProc(var msg: TMessage);
begin
  if (msg.Msg = RegisterWindowMessage(ASSIGN_REG_STR)) then begin
    OnWndMsg(msg);
  end
  else begin // Handle all messages with the default handler
    DefWindowProc(mWindowHandle, msg.Msg, msg.wParam, msg.lParam);
  end;
end;
//------------------------------------------------------------------------------
  { TGetSettingsHandler }
//------------------------------------------------------------------------------
constructor TGetSettingsHandler.Create(aOwner: TComponent);
begin
  inherited Create(aOwner, cGetSettingsPipeName);
  mTimer.Interval  := cGETSETTINGS_TIMEOUT;
  mGettingSettings := False;
  fSettings        := nil;
  mInitializerHWND := 0;
  mWait            := nil; // TWait.Create ( nil );
  fShowWait        := False;
  fGettingSettingsFromMachine := False;

  // Zus�tzliche CodeSite Outputs
  mDebugOutEnabled := GetClassDebug(ClassName);

end;
//------------------------------------------------------------------------------
destructor TGetSettingsHandler.Destroy;
begin
  mWait.Free;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.OnTimeout(Sender: TObject);
begin
  inherited;
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_TIMEOUT, 0);
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.OnMsgReceived(aReceivedMsg: PResponseRec);
var
  i: Integer;
begin
  EnterMethod('TGetSettingsHandler.OnMsgReceived');
  inherited;
  if aReceivedMsg.MsgTyp = rmSaveMaConfigOk then begin
    fXMLSettingsCollection := Nil;
    fXMLMaConfig           := StrPas(aReceivedMsg^.XMLMaConfig);
    codesite.SendFmtMsg('rmSaveMaConfigOk received from StorageHandler XMLMaConfig: %s',[fXMLMaConfig]);
  end
  else if aReceivedMsg.MsgTyp = rmGetSettingsOk then begin
    fXMLMaConfig           := '';
    fXMLSettingsCollection := @aReceivedMsg.XMLSettingsCollection;
  end;

  if mDebugOutEnabled then begin
//    for i := 0 to (cZESpdGroupLimit-1) do begin
    for i := 0 to (cMaxGroupMemoryLimit-1) do begin
      if (fXMLSettingsCollection <> Nil) then begin
        with fXMLSettingsCollection.SettingsArr[i] do begin
          codesite.SendFmtMsg('GROUP %d: StyleID: %d, ProdName: %s, YMSetName: %s, LengthWindow: %d',[i,StyleID,StrPas(@ProdName),StrPas(@YMSetName), LengthWindow]);
        end;
      end;
    end; //for
  end; //if mDebugOutEnabled


  // Nun noch den Aufrufer benachrichtigen
  SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_DONE, ORD(aReceivedMsg.MsgTyp));
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.OnWndMsg(msg: TMessage);
begin
  case msg.wParam of
    WM_DOGETSETTINGS: begin
      mGettingSettings := True;
      DoGetSettings(WM_DOGETSETTINGS);
    end;
    WM_DOSAVESETTINGS: begin  //NUE: 13.01.05
      mGettingSettings := True;
      DoGetSettings(WM_DOSAVESETTINGS);
    end;
    WM_GETSETTINGS_DONE: begin
      ProcessGetSettingsDoneMsg(TResponseMsgTyp(msg.lParam));
      mGettingSettings := False;
    end;
    WM_GETSETTINGS_TIMEOUT: begin
      ProcessGetSettingsTimeoutMsg;
      mGettingSettings := False;
    end;
    WM_GETSETTINGS_FAILED: begin
      UserErrorMsg(cSettingsNotOK, Owner); //Nue:26.11.02
      mGettingSettings := False;
    end;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.ProcessGetSettingsDoneMsg(aMsgTyp: TResponseMsgTyp);
begin
  try
    if ShowWait then
      mWait.Close;
    Screen.Cursor := crArrow;
    if mState <> asWaitJobRespons then
      Exit;
    mState := asNoJob;

    case aMsgTyp of
      rmSaveMaConfigNotOk, rmGetSettingsNotOk: begin
          SendMessage(mInitializerHWND, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_FAILED, AssMachine.MachineID);
        end;
      rmSaveMaConfigOk, rmGetSettingsOk: begin
          SendMessage(mInitializerHWND, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_DONE, AssMachine.MachineID);
        end;
    else
      raise Exception.Create('MsgTyp not defined. Type = ' + GetEnumName(TypeInfo(TResponseMsgTyp), Ord(aMsgTyp)));
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TGetSettingsHandler.ProcessGetSettingsDoneMsg failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.ProcessGetSettingsTimeoutMsg;
begin
  if ShowWait then
    mWait.Close;
  Screen.Cursor := crArrow;
  mState := asNoJob;
  SendMessage(mInitializerHWND, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_TIMEOUT, AssMachine.MachineID);
end;
//------------------------------------------------------------------------------
procedure TGetSettingsHandler.DoGetSettings(aWinMsgTyp: Integer);
var
  xJob: TJobRec;
begin
  // es ist kein dynamischer Jobbuffer n�tig. @xJob wird weitergegeben
  try
    if not Assigned(AssMachine) then
      raise Exception.Create('DoGetSettings: AssMachine not assigned.');

    FillChar(xJob, sizeof(xJob), 0);
    // wss: ein GetSettingsAllGroups wird im MsgHandler auf einzelne GetSettings aufgeteilt
    // und der StorageHandler setzt diese Meldungen wieder zusammen und verschickt zum Assignment
    // dies als eine komplete Meldung
    fSettings   := nil;
    if aWinMsgTyp=WM_DOSAVESETTINGS then begin     //Aufruf von MaConfig
      xJob.JobTyp := jtSaveMaConfigToDB;
      xJob.NetTyp := AssMachine.NetTyp;
      xJob.SaveMaConfig.MachineID    := AssMachine.MachineID;
      xJob.SaveMaConfig.UseXMLData   := false;
      xJob.SaveMaConfig.ComputerName := mPort.LocalComputerName;
      xJob.SaveMaConfig.Port         := mPort.PortName;
    end
    else {if aWinMsgTyp=WM_DOGETSETTINGS then} begin  //Aufruf von Assignment
      xJob.JobTyp := jtGetSettingsAllGroups;
      xJob.NetTyp := AssMachine.NetTyp;
      xJob.GetSettingsAllGroups.MachineID    := AssMachine.MachineID;
      xJob.GetSettingsAllGroups.ComputerName := mPort.LocalComputerName;
      xJob.GetSettingsAllGroups.Port         := mPort.PortName;
      xJob.GetSettingsAllGroups.AllXMLValues := not fGettingSettingsFromMachine;
    end;

    NewJob(@xJob);

    if ShowWait then begin
      mWait.ParentWindow := (Owner as TWinControl).Handle;
      mWait.ShowModal;
    end;
  except
    on e: Exception do begin
      mGettingSettings := False;
      SystemErrorMsg_('TGetSettingsHandler.DoGetSettings failed. ' + e.Message, nil, False); //Nue:10.12.02 False added
      SendMessage(mInitializerHWND, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_FAILED, 0);
    end;
  end;
end;

//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
function TGetSettingsHandler.StartGetSaveSettings(aInitializer: HWND;
    aNoMessage: Boolean; aWinMsgTyp: Integer): Boolean;
begin
  Result := True;
  if mGettingSettings then EXIT;  //Cursor im Hourglass; vermutlich bereits ein StartGetSettings running. Nue:29.01.04

//  if (Screen.Cursor=crHourGlass) then EXIT;  //Cursor im Hourglass; vermutlich bereits ein StartGetSettings running. Nue:25.08.03

  if AssMachine.IsReadyToGetMachineSettings then begin
    if mState <> asNoJob then
      raise Exception.Create('TGetSettingsHandler.InitializeGetSettings failed. Job already is in Queue. ');
    mInitializerHWND := aInitializer;
    Screen.Cursor := crHourGlass;
    SendMessage(mWindowHandle, RegisterWindowMessage(ASSIGN_REG_STR), aWinMsgTyp, 0);
  end
  else begin
    if not (aNoMessage) then
      SendMessage(aInitializer, RegisterWindowMessage(ASSIGN_REG_STR), WM_GETSETTINGS_FAILED, 0);
    Result := False;
  end;
end;

//------------------------------------------------------------------------------
function TGetSettingsHandler.StartGetSettings(aInitializer: HWND; aNoMessage:
    Boolean = False): Boolean;
begin
  Result := StartGetSaveSettings(aInitializer, aNoMessage, WM_DOGETSETTINGS);
end;

//------------------------------------------------------------------------------
function TGetSettingsHandler.StartSaveSettings(aInitializer: HWND; aNoMessage:
    Boolean = False): Boolean;
begin
  Result := StartGetSaveSettings(aInitializer, aNoMessage, WM_DOSAVESETTINGS);
end;

//------------------------------------------------------------------------------
function TMachine.CheckSlip(aSlip: Integer): Integer;
begin
  if (aSlip < (cMinSlip * 1000)) or (aSlip > (cMaxSlip * 1000)) then
    Result := 1000
  else
    Result := aSlip;
end;

//------------------------------------------------------------------------------
function TMachine.GetMaConfigReader: TMaConfigReader;
begin
  Result := nil;
(*  if assigned(fModel) then
    result := fModel.MaConfigReader;*)
  if not assigned(FMaConfigReader)then begin
    fMaConfigReader := TMaConfigReader.Create;
    fMaConfigReader.MachID := MachineID;
  end;// if not assigned(FMaConfigReader)then begin
  result := fMaConfigReader;
end;

//------------------------------------------------------------------------------
//NUE1
//procedure TMachine.SetMachineYMConfigRec(const aMachineYMConfigRec: TMachineYMConfigRec);
//begin
//  fMachineYMConfigRec := aMachineYMConfigRec;
//  with aMachineYMConfigRec do begin
//    fLongStopDef := longStopDef;
//    fCutRetries := cutRetries;
//    fCheckLen := checkLen;
//    fSpeedRamp := defaultSpeedRamp;
//    fSpeed := defaultSpeed;
//    fFrontType := frontType;
//    fYMVersion := StrPas(@frontSWVersion);
//    fYMOption := frontSwOption;
//    fMachineType := MachineType;   //Neu statt aWEMachType
////NUE1    fAWEMachType := aWEMachType;
////    fMachineName    := machBez;
//    fInopSettings0 := inoperativePara[0];
//    fInopSettings1 := inoperativePara[1];
//  end;
//end;
//
//------------------------------------------------------------------------------
procedure TMachine.SetSlip(const aValue: Integer);
begin
  fSlip := CheckSlip(aValue);
end;

//------------------------------------------------------------------------------
//function TMachine.GetMachineYMConfigRec: TMachineYMConfigRec;
//begin
//  Result := fMachineYMConfigRec;
//end;
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
  { T..Item }
//------------------------------------------------------------------------------
constructor TBaseSetIDItem.Create(aSetID: integer);
begin
  inherited Create;
  fSetID := aSetID;
  fName := '';
end;
//------------------------------------------------------------------------------
constructor TProdGrpItem.Create(aSetID: integer; aProdGrpID: integer; aOrderPosID: integer; aStyleID: integer);
begin
  inherited Create(aSetID);
  fProdGrpID := aProdGrpID;
  fOrderPositionID := aOrderPosID;
  fStyleID := aStyleID;
end;
//------------------------------------------------------------------------------
constructor TStyleItem.Create(aSetID: integer; aStyleID: integer);
begin
  inherited Create(aSetID);
  fStyleID := aStyleID;
  fAssortmentName := '';
  fYMSetName := '';
end;
//------------------------------------------------------------------------------
constructor TPreselectItem.Create;
begin
  inherited Create;
  fYMSetName := '';
end;

//------------------------------------------------------------------------------
constructor TOrderPositionItem.Create(aSetID: integer; aOrderPositionID: integer);
begin
  inherited Create(aSetID);
  fOrderPositionID := aOrderPositionID;
end;


//------------------------------------------------------------------------------

end.

