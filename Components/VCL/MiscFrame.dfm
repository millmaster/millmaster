object Misc: TMisc
  Left = 0
  Top = 0
  Width = 132
  Height = 94
  TabOrder = 0
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 132
    Height = 94
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object mmLabel1: TmmLabel
      Left = -17
      Top = 12
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Garn Nr:'
      FocusControl = edYarnCnt
      Visible = True
      AutoLabel.Control = lbYarnCountUnit
      AutoLabel.Distance = 48
      AutoLabel.LabelPosition = lpRight
    end
    object mmLabel2: TmmLabel
      Left = -17
      Top = 44
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Schlupf:'
      FocusControl = edSlip
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object lbYarnCountUnit: TmmLabel
      Left = 111
      Top = 12
      Width = 16
      Height = 13
      Caption = 'Nm'
      FocusControl = mmLabel1
      Visible = True
      AutoLabel.Distance = 30
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = -10
      Top = 76
      Width = 80
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = '(12)Fadenzahl:'
      Visible = False
      AutoLabel.LabelPosition = lpLeft
    end
    object mmUpDown1: TmmUpDown
      Left = 90
      Top = 72
      Width = 15
      Height = 21
      Associate = edThreadCnt
      Min = 1
      Max = 9
      Position = 1
      TabOrder = 3
      Visible = False
      Wrap = False
    end
    object edYarnCnt: TmmEdit
      Left = 65
      Top = 8
      Width = 41
      Height = 21
      Hint = '(*)Garnnummer des produzierenden Artikels'
      Color = clWindow
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      Text = '120'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edYarnCntKeyPress
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel1
      AutoLabel.LabelPosition = lpLeft
      Decimals = 1
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      ValidateMode = [vmExit, vmReadWrite]
    end
    object edSlip: TmmEdit
      Left = 65
      Top = 40
      Width = 41
      Height = 21
      Hint = '(*)Schlupf der Maschine'
      Color = clWindow
      MaxLength = 5
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Text = '1.000'
      Visible = True
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edSlipKeyPress
      Alignment = taRightJustify
      AutoLabel.Control = mmLabel2
      AutoLabel.LabelPosition = lpLeft
      Decimals = 3
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      ValidateMode = [vmExit, vmReadWrite]
    end
    object edThreadCnt: TmmEdit
      Left = 73
      Top = 72
      Width = 17
      Height = 21
      Hint = '(*)Anzahl der Faeden im Garn'
      Color = clWindow
      MaxLength = 1
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      Text = '1'
      Visible = False
      OnChange = edChange
      OnExit = edExit
      OnKeyPress = edThreadCntKeyPress
      Alignment = taRightJustify
      AutoLabel.LabelPosition = lpLeft
      Decimals = 0
      NumMode = True
      ReadOnlyColor = clInfoBk
      ShowMode = smNormal
      ValidateMode = [vmExit, vmReadWrite]
    end
  end
end
