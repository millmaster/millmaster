unit YMSettingsControl_TLB;

// ************************************************************************ //
// WARNING                                                                    
// -------                                                                    
// The types declared in this file were generated from data read from a       
// Type Library. If this type library is explicitly or indirectly (via        
// another type library referring to this type library) re-imported, or the   
// 'Refresh' command of the Type Library Editor activated while editing the   
// Type Library, the contents of this file will be regenerated and all        
// manual modifications will be lost.                                         
// ************************************************************************ //

// PASTLWTR : $Revision:   1.88.1.0.1.0  $
// File generated on 30.09.2005 09:30:01 from Type Library described below.

// ************************************************************************ //
// Type Lib: W:\MillMaster\GUI\Floor\YMSettingsControl\YMSettingsControl.tlb (1)
// IID\LCID: {D04E85A1-48D4-11D4-86E5-00105A55E052}\0
// Helpfile: 
// DepndLst: 
//   (1) v2.0 stdole, (C:\WINDOWS\system32\STDOLE2.TLB)
//   (2) v4.0 StdVCL, (C:\WINDOWS\system32\STDVCL40.DLL)
// ************************************************************************ //
{$TYPEDADDRESS OFF} // Unit must be compiled without type-checked pointers. 
interface

uses Windows, ActiveX, Classes, Graphics, OleServer, OleCtrls, StdVCL;

// *********************************************************************//
// GUIDS declared in the TypeLibrary. Following prefixes are used:        
//   Type Libraries     : LIBID_xxxx                                      
//   CoClasses          : CLASS_xxxx                                      
//   DISPInterfaces     : DIID_xxxx                                       
//   Non-DISP interfaces: IID_xxxx                                        
// *********************************************************************//
const
  // TypeLibrary Major and minor versions
  YMSettingsControlMajorVersion = 1;
  YMSettingsControlMinorVersion = 0;

  LIBID_YMSettingsControl: TGUID = '{D04E85A1-48D4-11D4-86E5-00105A55E052}';

  IID_IYMSettingsControl: TGUID = '{37225A91-5276-11D4-86EE-00105A55E052}';
  DIID_IQualityMatrixEvents: TGUID = '{D04E85AB-48D4-11D4-86E5-00105A55E052}';
  IID_IQualityMatrix: TGUID = '{D04E85A9-48D4-11D4-86E5-00105A55E052}';
  DIID_IClearerSettingsEvents: TGUID = '{D04E85B1-48D4-11D4-86E5-00105A55E052}';
  IID_IClearerSettingsBasic: TGUID = '{D04E85B5-48D4-11D4-86E5-00105A55E052}';
  DIID_IClearerSettingsBasicEvents: TGUID = '{D04E85B7-48D4-11D4-86E5-00105A55E052}';
  CLASS_ClearerSettingsBasic: TGUID = '{D04E85B9-48D4-11D4-86E5-00105A55E052}';
  CLASS_QualityMatrix: TGUID = '{D04E85AD-48D4-11D4-86E5-00105A55E052}';
  IID_IXMLSettings: TGUID = '{3A671439-680A-43C2-8167-0509F537EFE0}';
  CLASS_ClearerSettings: TGUID = '{CF538739-8D99-4269-B362-E56243B42E7C}';
type

// *********************************************************************//
// Forward declaration of types defined in TypeLibrary                    
// *********************************************************************//
  IYMSettingsControl = interface;
  IYMSettingsControlDisp = dispinterface;
  IQualityMatrixEvents = dispinterface;
  IQualityMatrix = interface;
  IQualityMatrixDisp = dispinterface;
  IClearerSettingsEvents = dispinterface;
  IClearerSettingsBasic = interface;
  IClearerSettingsBasicDisp = dispinterface;
  IClearerSettingsBasicEvents = dispinterface;
  IXMLSettings = interface;
  IXMLSettingsDisp = dispinterface;

// *********************************************************************//
// Declaration of CoClasses defined in Type Library                       
// (NOTE: Here we map each CoClass to its Default Interface)              
// *********************************************************************//
  ClearerSettingsBasic = IClearerSettingsBasic;
  QualityMatrix = IQualityMatrix;
  ClearerSettings = IXMLSettings;


// *********************************************************************//
// Interface: IYMSettingsControl
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {37225A91-5276-11D4-86EE-00105A55E052}
// *********************************************************************//
  IYMSettingsControl = interface(IDispatch)
    ['{37225A91-5276-11D4-86EE-00105A55E052}']
    procedure UpdateControl; safecall;
  end;

// *********************************************************************//
// DispIntf:  IYMSettingsControlDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {37225A91-5276-11D4-86EE-00105A55E052}
// *********************************************************************//
  IYMSettingsControlDisp = dispinterface
    ['{37225A91-5276-11D4-86EE-00105A55E052}']
    procedure UpdateControl; dispid 100;
  end;

// *********************************************************************//
// DispIntf:  IQualityMatrixEvents
// Flags:     (4096) Dispatchable
// GUID:      {D04E85AB-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IQualityMatrixEvents = dispinterface
    ['{D04E85AB-48D4-11D4-86E5-00105A55E052}']
  end;

// *********************************************************************//
// Interface: IQualityMatrix
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D04E85A9-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IQualityMatrix = interface(IYMSettingsControl)
    ['{D04E85A9-48D4-11D4-86E5-00105A55E052}']
    function  Get_DoubleBuffered: WordBool; safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    function  Get_VisibleDockClientCount: Integer; safecall;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function  Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    function  IsRightToLeft: WordBool; safecall;
    function  UseRightToLeftReading: WordBool; safecall;
    function  UseRightToLeftScrollBar: WordBool; safecall;
    function  Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function  Get_Cursor: Smallint; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    function  Get_DataView: Integer; safecall;
    procedure Set_DataView(Value: Integer); safecall;
    function  Get_MatrixTyp: Integer; safecall;
    procedure Set_MatrixTyp(Value: Integer); safecall;
    function  Get_ChannelVisible: WordBool; safecall;
    procedure Set_ChannelVisible(Value: WordBool); safecall;
    function  Get_ChannelColor: Integer; safecall;
    procedure Set_ChannelColor(Value: Integer); safecall;
    function  Get_ClusterVisible: WordBool; safecall;
    procedure Set_ClusterVisible(Value: WordBool); safecall;
    function  Get_ClusterColor: Integer; safecall;
    procedure Set_ClusterColor(Value: Integer); safecall;
    function  Get_SpliceVisible: WordBool; safecall;
    procedure Set_SpliceVisible(Value: WordBool); safecall;
    function  Get_SpliceColor: Integer; safecall;
    procedure Set_SpliceColor(Value: Integer); safecall;
    function  Get_ActiveVisible: WordBool; safecall;
    procedure Set_ActiveVisible(Value: WordBool); safecall;
    function  Get_ActiveColor: Integer; safecall;
    procedure Set_ActiveColor(Value: Integer); safecall;
    function  Get_DisplayMode: Integer; safecall;
    procedure Set_DisplayMode(Value: Integer); safecall;
    function  Get_InactiveColor: Integer; safecall;
    procedure Set_InactiveColor(Value: Integer); safecall;
    property DoubleBuffered: WordBool read Get_DoubleBuffered write Set_DoubleBuffered;
    property VisibleDockClientCount: Integer read Get_VisibleDockClientCount;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property Cursor: Smallint read Get_Cursor write Set_Cursor;
    property DataView: Integer read Get_DataView write Set_DataView;
    property MatrixTyp: Integer read Get_MatrixTyp write Set_MatrixTyp;
    property ChannelVisible: WordBool read Get_ChannelVisible write Set_ChannelVisible;
    property ChannelColor: Integer read Get_ChannelColor write Set_ChannelColor;
    property ClusterVisible: WordBool read Get_ClusterVisible write Set_ClusterVisible;
    property ClusterColor: Integer read Get_ClusterColor write Set_ClusterColor;
    property SpliceVisible: WordBool read Get_SpliceVisible write Set_SpliceVisible;
    property SpliceColor: Integer read Get_SpliceColor write Set_SpliceColor;
    property ActiveVisible: WordBool read Get_ActiveVisible write Set_ActiveVisible;
    property ActiveColor: Integer read Get_ActiveColor write Set_ActiveColor;
    property DisplayMode: Integer read Get_DisplayMode write Set_DisplayMode;
    property InactiveColor: Integer read Get_InactiveColor write Set_InactiveColor;
  end;

// *********************************************************************//
// DispIntf:  IQualityMatrixDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D04E85A9-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IQualityMatrixDisp = dispinterface
    ['{D04E85A9-48D4-11D4-86E5-00105A55E052}']
    property DoubleBuffered: WordBool dispid 2;
    property VisibleDockClientCount: Integer readonly dispid 3;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; dispid 5;
    property Enabled: WordBool dispid -514;
    function  IsRightToLeft: WordBool; dispid 7;
    function  UseRightToLeftReading: WordBool; dispid 10;
    function  UseRightToLeftScrollBar: WordBool; dispid 11;
    property Visible: WordBool dispid 12;
    property Cursor: Smallint dispid 13;
    property DataView: Integer dispid 4;
    property MatrixTyp: Integer dispid 6;
    property ChannelVisible: WordBool dispid 8;
    property ChannelColor: Integer dispid 9;
    property ClusterVisible: WordBool dispid 14;
    property ClusterColor: Integer dispid 15;
    property SpliceVisible: WordBool dispid 16;
    property SpliceColor: Integer dispid 17;
    property ActiveVisible: WordBool dispid 18;
    property ActiveColor: Integer dispid 19;
    property DisplayMode: Integer dispid 1;
    property InactiveColor: Integer dispid 20;
    procedure UpdateControl; dispid 100;
  end;

// *********************************************************************//
// DispIntf:  IClearerSettingsEvents
// Flags:     (4096) Dispatchable
// GUID:      {D04E85B1-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IClearerSettingsEvents = dispinterface
    ['{D04E85B1-48D4-11D4-86E5-00105A55E052}']
  end;

// *********************************************************************//
// Interface: IClearerSettingsBasic
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D04E85B5-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IClearerSettingsBasic = interface(IYMSettingsControl)
    ['{D04E85B5-48D4-11D4-86E5-00105A55E052}']
    function  Get_DoubleBuffered: WordBool; safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    function  Get_VisibleDockClientCount: Integer; safecall;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function  Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    function  IsRightToLeft: WordBool; safecall;
    function  UseRightToLeftReading: WordBool; safecall;
    function  UseRightToLeftScrollBar: WordBool; safecall;
    function  Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function  Get_Cursor: Smallint; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    property DoubleBuffered: WordBool read Get_DoubleBuffered write Set_DoubleBuffered;
    property VisibleDockClientCount: Integer read Get_VisibleDockClientCount;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property Cursor: Smallint read Get_Cursor write Set_Cursor;
  end;

// *********************************************************************//
// DispIntf:  IClearerSettingsBasicDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {D04E85B5-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IClearerSettingsBasicDisp = dispinterface
    ['{D04E85B5-48D4-11D4-86E5-00105A55E052}']
    property DoubleBuffered: WordBool dispid 2;
    property VisibleDockClientCount: Integer readonly dispid 3;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; dispid 5;
    property Enabled: WordBool dispid -514;
    function  IsRightToLeft: WordBool; dispid 7;
    function  UseRightToLeftReading: WordBool; dispid 10;
    function  UseRightToLeftScrollBar: WordBool; dispid 11;
    property Visible: WordBool dispid 12;
    property Cursor: Smallint dispid 13;
    procedure UpdateControl; dispid 100;
  end;

// *********************************************************************//
// DispIntf:  IClearerSettingsBasicEvents
// Flags:     (4096) Dispatchable
// GUID:      {D04E85B7-48D4-11D4-86E5-00105A55E052}
// *********************************************************************//
  IClearerSettingsBasicEvents = dispinterface
    ['{D04E85B7-48D4-11D4-86E5-00105A55E052}']
  end;

// *********************************************************************//
// Interface: IXMLSettings
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3A671439-680A-43C2-8167-0509F537EFE0}
// *********************************************************************//
  IXMLSettings = interface(IYMSettingsControl)
    ['{3A671439-680A-43C2-8167-0509F537EFE0}']
    function  Get_DoubleBuffered: WordBool; safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    function  Get_VisibleDockClientCount: Integer; safecall;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function  Get_Enabled: WordBool; safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    function  IsRightToLeft: WordBool; safecall;
    function  UseRightToLeftReading: WordBool; safecall;
    function  UseRightToLeftScrollBar: WordBool; safecall;
    function  Get_Visible: WordBool; safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function  Get_Cursor: Smallint; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    function  Get_SettingsGroup: Integer; safecall;
    procedure Set_SettingsGroup(Value: Integer); safecall;
    property DoubleBuffered: WordBool read Get_DoubleBuffered write Set_DoubleBuffered;
    property VisibleDockClientCount: Integer read Get_VisibleDockClientCount;
    property Enabled: WordBool read Get_Enabled write Set_Enabled;
    property Visible: WordBool read Get_Visible write Set_Visible;
    property Cursor: Smallint read Get_Cursor write Set_Cursor;
    property SettingsGroup: Integer read Get_SettingsGroup write Set_SettingsGroup;
  end;

// *********************************************************************//
// DispIntf:  IXMLSettingsDisp
// Flags:     (4416) Dual OleAutomation Dispatchable
// GUID:      {3A671439-680A-43C2-8167-0509F537EFE0}
// *********************************************************************//
  IXMLSettingsDisp = dispinterface
    ['{3A671439-680A-43C2-8167-0509F537EFE0}']
    property DoubleBuffered: WordBool dispid 2;
    property VisibleDockClientCount: Integer readonly dispid 3;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer; dispid 5;
    property Enabled: WordBool dispid -514;
    function  IsRightToLeft: WordBool; dispid 7;
    function  UseRightToLeftReading: WordBool; dispid 10;
    function  UseRightToLeftScrollBar: WordBool; dispid 11;
    property Visible: WordBool dispid 12;
    property Cursor: Smallint dispid 13;
    property SettingsGroup: Integer dispid 1;
    procedure UpdateControl; dispid 100;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TClearerSettingsBasic
// Help String      : FLRCSBasic Control
// Default Interface: IClearerSettingsBasic
// Def. Intf. DISP? : No
// Event   Interface: IClearerSettingsBasicEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TClearerSettingsBasic = class(TOleControl)
  private
    FIntf: IClearerSettingsBasic;
    function  GetControlInterface: IClearerSettingsBasic;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure UpdateControl;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer;
    function  IsRightToLeft: WordBool;
    function  UseRightToLeftReading: WordBool;
    function  UseRightToLeftScrollBar: WordBool;
    property  ControlInterface: IClearerSettingsBasic read GetControlInterface;
    property  DefaultInterface: IClearerSettingsBasic read GetControlInterface;
    property DoubleBuffered: WordBool index 2 read GetWordBoolProp write SetWordBoolProp;
    property VisibleDockClientCount: Integer index 3 read GetIntegerProp;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp;
    property Visible: WordBool index 12 read GetWordBoolProp write SetWordBoolProp;
  published
    property Cursor: Smallint index 13 read GetSmallintProp write SetSmallintProp stored False;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TQualityMatrix
// Help String      : FLRQMatrix Control
// Default Interface: IQualityMatrix
// Def. Intf. DISP? : No
// Event   Interface: IQualityMatrixEvents
// TypeFlags        : (34) CanCreate Control
// *********************************************************************//
  TQualityMatrix = class(TOleControl)
  private
    FIntf: IQualityMatrix;
    function  GetControlInterface: IQualityMatrix;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure UpdateControl;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer;
    function  IsRightToLeft: WordBool;
    function  UseRightToLeftReading: WordBool;
    function  UseRightToLeftScrollBar: WordBool;
    property  ControlInterface: IQualityMatrix read GetControlInterface;
    property  DefaultInterface: IQualityMatrix read GetControlInterface;
    property DoubleBuffered: WordBool index 2 read GetWordBoolProp write SetWordBoolProp;
    property VisibleDockClientCount: Integer index 3 read GetIntegerProp;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp;
    property Visible: WordBool index 12 read GetWordBoolProp write SetWordBoolProp;
  published
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Cursor: Smallint index 13 read GetSmallintProp write SetSmallintProp stored False;
    property DataView: Integer index 4 read GetIntegerProp write SetIntegerProp stored False;
    property MatrixTyp: Integer index 6 read GetIntegerProp write SetIntegerProp stored False;
    property ChannelVisible: WordBool index 8 read GetWordBoolProp write SetWordBoolProp stored False;
    property ChannelColor: Integer index 9 read GetIntegerProp write SetIntegerProp stored False;
    property ClusterVisible: WordBool index 14 read GetWordBoolProp write SetWordBoolProp stored False;
    property ClusterColor: Integer index 15 read GetIntegerProp write SetIntegerProp stored False;
    property SpliceVisible: WordBool index 16 read GetWordBoolProp write SetWordBoolProp stored False;
    property SpliceColor: Integer index 17 read GetIntegerProp write SetIntegerProp stored False;
    property ActiveVisible: WordBool index 18 read GetWordBoolProp write SetWordBoolProp stored False;
    property ActiveColor: Integer index 19 read GetIntegerProp write SetIntegerProp stored False;
    property DisplayMode: Integer index 1 read GetIntegerProp write SetIntegerProp stored False;
    property InactiveColor: Integer index 20 read GetIntegerProp write SetIntegerProp stored False;
  end;


// *********************************************************************//
// OLE Control Proxy class declaration
// Control Name     : TClearerSettings
// Help String      : 
// Default Interface: IXMLSettings
// Def. Intf. DISP? : No
// Event   Interface: 
// TypeFlags        : (2) CanCreate
// *********************************************************************//
  TClearerSettings = class(TOleControl)
  private
    FIntf: IXMLSettings;
    function  GetControlInterface: IXMLSettings;
  protected
    procedure CreateControl;
    procedure InitControlData; override;
  public
    procedure UpdateControl;
    function  DrawTextBiDiModeFlagsReadingOnly: Integer;
    function  IsRightToLeft: WordBool;
    function  UseRightToLeftReading: WordBool;
    function  UseRightToLeftScrollBar: WordBool;
    property  ControlInterface: IXMLSettings read GetControlInterface;
    property  DefaultInterface: IXMLSettings read GetControlInterface;
    property DoubleBuffered: WordBool index 2 read GetWordBoolProp write SetWordBoolProp;
    property VisibleDockClientCount: Integer index 3 read GetIntegerProp;
    property Enabled: WordBool index -514 read GetWordBoolProp write SetWordBoolProp;
    property Visible: WordBool index 12 read GetWordBoolProp write SetWordBoolProp;
  published
    property  TabStop;
    property  Align;
    property  DragCursor;
    property  DragMode;
    property  ParentShowHint;
    property  PopupMenu;
    property  ShowHint;
    property  TabOrder;
    property  OnDragDrop;
    property  OnDragOver;
    property  OnEndDrag;
    property  OnEnter;
    property  OnExit;
    property  OnStartDrag;
    property Cursor: Smallint index 13 read GetSmallintProp write SetSmallintProp stored False;
    property SettingsGroup: Integer index 1 read GetIntegerProp write SetIntegerProp stored False;
  end;

procedure Register;

implementation

uses ComObj;

procedure TClearerSettingsBasic.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{D04E85B9-48D4-11D4-86E5-00105A55E052}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$80040154*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TClearerSettingsBasic.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IClearerSettingsBasic;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TClearerSettingsBasic.GetControlInterface: IClearerSettingsBasic;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TClearerSettingsBasic.UpdateControl;
begin
  DefaultInterface.UpdateControl;
end;

function  TClearerSettingsBasic.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := DefaultInterface.DrawTextBiDiModeFlagsReadingOnly;
end;

function  TClearerSettingsBasic.IsRightToLeft: WordBool;
begin
  Result := DefaultInterface.IsRightToLeft;
end;

function  TClearerSettingsBasic.UseRightToLeftReading: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftReading;
end;

function  TClearerSettingsBasic.UseRightToLeftScrollBar: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftScrollBar;
end;

procedure TQualityMatrix.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{D04E85AD-48D4-11D4-86E5-00105A55E052}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$00000000*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TQualityMatrix.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IQualityMatrix;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TQualityMatrix.GetControlInterface: IQualityMatrix;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TQualityMatrix.UpdateControl;
begin
  DefaultInterface.UpdateControl;
end;

function  TQualityMatrix.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := DefaultInterface.DrawTextBiDiModeFlagsReadingOnly;
end;

function  TQualityMatrix.IsRightToLeft: WordBool;
begin
  Result := DefaultInterface.IsRightToLeft;
end;

function  TQualityMatrix.UseRightToLeftReading: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftReading;
end;

function  TQualityMatrix.UseRightToLeftScrollBar: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftScrollBar;
end;

procedure TClearerSettings.InitControlData;
const
  CControlData: TControlData2 = (
    ClassID: '{CF538739-8D99-4269-B362-E56243B42E7C}';
    EventIID: '';
    EventCount: 0;
    EventDispIDs: nil;
    LicenseKey: nil (*HR:$00000000*);
    Flags: $00000008;
    Version: 401);
begin
  ControlData := @CControlData;
end;

procedure TClearerSettings.CreateControl;

  procedure DoCreate;
  begin
    FIntf := IUnknown(OleObject) as IXMLSettings;
  end;

begin
  if FIntf = nil then DoCreate;
end;

function TClearerSettings.GetControlInterface: IXMLSettings;
begin
  CreateControl;
  Result := FIntf;
end;

procedure TClearerSettings.UpdateControl;
begin
  DefaultInterface.UpdateControl;
end;

function  TClearerSettings.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := DefaultInterface.DrawTextBiDiModeFlagsReadingOnly;
end;

function  TClearerSettings.IsRightToLeft: WordBool;
begin
  Result := DefaultInterface.IsRightToLeft;
end;

function  TClearerSettings.UseRightToLeftReading: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftReading;
end;

function  TClearerSettings.UseRightToLeftScrollBar: WordBool;
begin
  Result := DefaultInterface.UseRightToLeftScrollBar;
end;

procedure Register;
begin
  RegisterComponents('ActiveX',[TClearerSettingsBasic, TQualityMatrix, TClearerSettings]);
end;

end.
