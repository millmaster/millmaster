(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: FLRCSettingsPPG.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 27.07.2006        Wss | Auswahl IPI Limits eingef�gt
| 16.09.2008  1.01  Nue | Auswahl VCV Limits eingef�gt in DFM
|=========================================================================================*)
unit FLRXMLSettingsPPG;

interface

uses SysUtils, Windows, Messages, Classes, Graphics, Controls, StdCtrls,
  ExtCtrls, Forms, ComServ, ComObj, StdVcl, AxCtrls, mmRadioGroup,
  mmRadioButton, mmPanel, IvDictio, IvMulti, IvEMulti, mmTranslator,
  IvAMulti, IvBinDic, mmDictionary;

type
  TXMLSettingsPPG = class(TPropertyPage)
    rgSettingsGroup: TmmRadioGroup;
    mDictionary: TmmDictionary;
    mTranslator: TmmTranslator;
    procedure PropertyPageCreate(Sender: TObject);
  private
  protected
  public
    procedure UpdatePropertyPage; override;
    procedure UpdateObject; override;
  end;

const
  Class_XMLSettingsPPG: TGUID = '{87D03296-CB37-4BBE-81CF-50DD45BCBEF1}';

implementation

{$R *.DFM}

procedure TXMLSettingsPPG.UpdatePropertyPage;
begin
  // Update your controls from OleObject
  rgSettingsGroup.ItemIndex := OleObject.SettingsGroup - 1;
  // Auswahl ist nur 1x m�glich, da sonst beim ver�ndern eine neue Komponente erstellt wird und dies beim Speicher
  // der Floor zu einem Fehler f�hrt!!
  rgSettingsGroup.Enabled   := (rgSettingsGroup.ItemIndex = -1);
end;

procedure TXMLSettingsPPG.UpdateObject;
begin
  // Update OleObject from your controls
  OleObject.SettingsGroup := rgSettingsGroup.ItemIndex + 1;
  // finally update control
  OleObject.UpdateControl;
end;

procedure TXMLSettingsPPG.PropertyPageCreate(Sender: TObject);
begin
  DoubleBuffered := True;
end;

initialization
  TActiveXPropertyPageFactory.Create(
    ComServer,
    TXMLSettingsPPG,
    Class_XMLSettingsPPG);
end.
