library YMSettingsControl;


 // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  mmCS,
  ComServ,
  LoepfeGlobal,
  FLRQMatrixImpl in 'FLRQMatrixImpl.pas' {QualityMatrix: CoClass},
  FLRXMLSettingsImpl in 'FLRXMLSettingsImpl.pas' {ClearerSettingsOld: CoClass},
  FLRCSBasicImpl in 'FLRCSBasicImpl.pas' {ClearerSettingsBasic: CoClass},
  FLRQMatrixPPG in 'FLRQMatrixPPG.pas' {QualityMatrixPPG: TPropertyPage},
  YMSettingsBaseControl in 'YMSettingsBaseControl.pas',
  FloorActiveX in '..\..\..\..\LoepfeShares\Common\FloorActiveX.pas',
  FLRXMLSettingsPPG in 'FLRXMLSettingsPPG.pas' {XMLSettingsPPG: TPropertyPage},
  ClassDataReader in '..\..\..\Components\VCL\ClassDataReader.pas' {ClassDataModule: TDataModule},
  FloorControlX in '..\..\..\..\loepfeshares\common\FloorControlX.pas',
  XMLSettingsAccess in '..\..\..\Common\XMLSettingsAccess.pas',
  MMQualityMatrixClass in '..\..\..\Components\VCL\MMQualityMatrixClass.pas';

{$E dll}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}
{$R 'Version.res'}
begin
{$IFDEF MemCheck}
  MemChk(gApplicationName);
{$ENDIF}
  CodeSite.Enabled := GetRegBoolean(cRegLM, cRegMillMasterPath + '\Debug', 'YMSettingsControl', False);
end.

