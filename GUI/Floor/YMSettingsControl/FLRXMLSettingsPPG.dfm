object XMLSettingsPPG: TXMLSettingsPPG
  Left = 637
  Top = 136
  Width = 198
  Height = 294
  Caption = '(*)Reiniger Einstellungen'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = PropertyPageCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rgSettingsGroup: TmmRadioGroup
    Left = 0
    Top = 0
    Width = 190
    Height = 267
    Align = alClient
    Caption = '(30)Anzeigen'
    ItemIndex = 0
    Items.Strings = (
      '(8)Kanal'
      '(8)Spleiss'
      '(11)Fehlerschwarm'
      '(11)FF-Cluster'
      '(11)SFI'
      '(11)VCV'
      '(11)Garnnummer'
      '(11)P Einstellungen'
      '(15)P Konfiguration'
      '(12)Wiederholung')
    TabOrder = 0
    CaptionSpace = True
  end
  object mDictionary: TmmDictionary
    DictionaryName = 'MainDict'
    AutoConfig = True
    Left = 104
    Top = 16
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDict'
    Left = 144
    Top = 16
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
