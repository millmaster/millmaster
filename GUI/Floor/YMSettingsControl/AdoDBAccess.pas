(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......:
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Enth�lt die Klasse f�r den DB-Zugriff der StoreX Threads �ber ADO.
|
| Info..........: Ersetzt die Klasse TDBAccess
| Develop.system: Windows 2000
| Target.system.: Windows NT / 2000
| Compiler/Tools: Delphi 5.01
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 28.06.2002  1.00  Lok | Datei erstellt
| 11.06.2003        Lok | TAdoDBAccess.Init: Fehlermeldung erweitert (exception)
| 23.07.2003        Lok | TNativeAdoQuery: Property 'AsXML' hinzugef�gt. Damit kann das Recordset
|                       |   als XML-String ausgelesen, oder von einem XML-String gesetzt werden.
| 07.08.2003        Lok | Methode TNativeAdoQuery.Clone eingef�hrt. Damit kann das Recordset kopiert werden.
|                       | Methode TNativeAdoQuery.Filter(aFilterstring: string) eingef�hrt. Wendet einen
|                       |   Filter auf das Recordset an.
|                       | Methode TNativeAdoQuery.Sort(aSortString: string) eingef�hrt.
| 05.09.2003        Lok | Fehler in TAdoDBAccess.Init behoben (Im Fehlerfall wurde ein leerer
|                       |   ConnectionString in das Eventlog geschrieben.
|                       | Diverse Compiler Warnungen behoben
| 14.01.2004        Lok | In der Methode 'function TmmAdoField.IsNull: Boolean' die Abfrage ob das Feld
|                       | g�ltig ist wie folgt ge�ndert:
|                       |   von: result := (mRecordset.Fields[Index].Value = Null)
|                       |   zu : result := ((VarType(mRecordset.Fields[Index].Value) and varTypeMask) = varNull)
| 22.04.2004        Nue | In SendDebugQuery, SetParams, Open und ExecSQL Infos eingebaut f�r EventLog.
| 14.05.2004        Lok | Compiler Hints bereinigt.
|                       |   In SendDebugQuery bei einem Parameterfehler den Namen des Parameters ausgeben. 
| 25.05.2004        Wss | In den SetAsxxx Proceduren einen Kommentar hinzugef�gt da Abbruchbedingung der Rekursion nicht klar war
| 23.06.2004        Lok | In der Methode 'TNativeADOQuery.SetParams' die Fehlermeldung um Parameter nummer und Name erweitert.  
| 09.03.2005        Lok | Fehler in Send DebugQuery bei der Formatierung von Float Parametern behoben.  
| 21.03.2005        Lok | Transaktionsverwaltung in TNativeAdoQuery eingef�gt. Die Transaktionen werden �ber einen Backpointer
|                       |   von TADODBAccess verwaltet. 
|                       | In TADODBAccess Verwaltung f�r verschachtelte Transaktionen implementiert.  
|=========================================================================================*)
(*

! ! ! ! ! !     A C H T U N G     ! ! ! ! ! !

Die Datei 'AdoDbAccess' nur mit Modelmaker 
bearbeiten. Das Modell befindet sich im 
Unterverzeichnis '\ModelMaker'.

*********************************************

Es wird die Klasse 'TDBAccess' simuliert. Der Zugriff auf die einzelnen
Elemente der Klasse 'TAdoDBAccess' erfogt identisch zu der BDE Version.
Es wird das Verhalten der eingesetzten VCL Komponenten simuliert (Parameter, Fields).
Die simulierten Komponenten sind jedoch nicht von TDataSet abgeleitet und k�nnen
deshalb in einer visuellen Umgebung nur beschr�nkt eingesetzt werden.
*)
unit AdoDbAccess;

interface

uses
  SysUtils, Windows, Messages, Classes, ADODB_TLB;

const
  cMaxQueries     = 6;
  cPrimaryQuery   = 0;
  cSecondaryQuery = 1;
  cThirdQuery     = 2;
  cFourthQuery    = 3;

  cConnectionString = 'Provider=SQLOLEDB.1;Data Source=%s;Initial Catalog=%s;Password=%s;User ID=%s;Packet Size=8192';

// Datentypen der Parameter (Kopie aus ADO_TLB.pas)
const
  adEmpty            = $00000000;
  adTinyInt          = $00000010;
  adSmallInt         = $00000002;
  adInteger          = $00000003;
  adBigInt           = $00000014;
  adUnsignedTinyInt  = $00000011;
  adUnsignedSmallInt = $00000012;
  adUnsignedInt      = $00000013;
  adUnsignedBigInt   = $00000015;
  adSingle           = $00000004;
  adDouble           = $00000005;
  adCurrency         = $00000006;
  adDecimal          = $0000000E;
  adNumeric          = $00000083;
  adBoolean          = $0000000B;
  adError            = $0000000A;
  adUserDefined      = $00000084;
  adVariant          = $0000000C;
  adIDispatch        = $00000009;
  adIUnknown         = $0000000D;
  adGUID             = $00000048;
  adDate             = $00000007;
  adDBDate           = $00000085;
  adDBTime           = $00000086;
  adDBTimeStamp      = $00000087;
  adBSTR             = $00000008;
  adChar             = $00000081;
  adVarChar          = $000000C8;
  adLongVarChar      = $000000C9;
  adWChar            = $00000082;
  adVarWChar         = $000000CA;
  adLongVarWChar     = $000000CB;
  adBinary           = $00000080;
  adVarBinary        = $000000CC;
  adLongVarBinary    = $000000CD;
  adChapter          = $00000088;
  adFileTime         = $00000040;
  adPropVariant      = $0000008A;
  adVarNumeric       = $0000008B;
  adArray            = $00002000;


type
  TmmAdoParams = class;
  TNativeAdoQuery = class;
  TAdoDBAccessDB = class;
  TAdoQueryArr = array [0..cMaxQueries - 1] of TNativeAdoQuery;

  // Typ des Parameters:
  // In der Methode TNativeAdoQuery.SetParams muss ein neuer case eingef�hrt werden,
  // ... wenn hier ein neuer Typ eingef�hrt wird
  TFieldType = (ptNone, ptInteger, ptFloat, ptDateTime, ptString, ptBoolean, ptBlob);

  TBlobByteData = Array of Byte;
  TTransIsolation = (tiDirtyRead, tiReadCommitted, tiRepeatableRead);

  // 1 Wird aufgerufen, wenn ein neuer Parameter gefunden wurde 
  TNewParamEvent = procedure (aSender: TObject; aNewParamName: string) of object;
  (*
  TList kann nicht polymorph verwendet werden.
  *)
  TBaseList = class(TPersistent)
  private
    FOwnsObjects: Boolean;
    // 1 Aggregatfunktion 
    function GetCount: Integer;
    // 1 Aggregatfunktion 
    function GetItems(Index: Integer): TObject;
    // 1 Aggregatfunktion 
    procedure SetItems(Index: Integer; aValue: TObject);
  protected
    FList: TList;
  public
    // 1 Konstruktor 
    constructor Create; virtual;
    // 1 Destruktor 
    destructor destroy; override;
    // 1 Aggregatfunktion 
    function Add(Item: Pointer): Integer; virtual;
    // 1 Aggregatfunktion 
    procedure Clear; virtual;
    // 1 Aggregatfunktion 
    procedure Delete(Index: Integer); virtual;
    // 1 Aggregatfunktion 
    procedure Exchange(Index1, Index2: Integer);
    // 1 Aggregatfunktion 
    function IndexOf(Item: TObject): Integer; overload;
    // 1 Anzahl Eintr�ge 
    property Count: Integer read GetCount;
    // 1 Einzelen Eintr�ge 
    property Items[Index: Integer]: TObject read GetItems write SetItems; default;
    // 1 True, wenn die Objekte intern verwaltet werden (freigeben beim L�schen) 
    property OwnsObjects: Boolean read FOwnsObjects write FOwnsObjects;
  end;
  
  (*
  Der Parser benachrichtigt das aufrufende Objekt dar�ber, wenn 
  ein neuer Parameter gefunden wurde. Das aufrufende Objekt kann 
  dann einen TmmAdoParameter mit dem entsprechenden Namen erzeugen.
  Ausserdem muss f�r die Ausf�hrung �ber ADO die Definition des 
  Parameters von der Art der BDE (':Parametername') zu ADO('?') 
  gewandelt werden.
  
  BDE: 'SELECT c_Name FORM t_Adresse WHERE c_ID = :ID'
  ADO: 'SELECT c_Name FORM t_Adresse WHERE c_ID = :?'
  *)
  TAdoParamParser = class(TObject)
  private
    FOnNewParam: TNewParamEvent;
    FReplaceToken: Char;
    FText: String;
    // 1 Setzt den String der zu Parsen ist 
    procedure SetText(Value: String);
  public
    // 1 Konstruktor 
    constructor Create; overload;
    // 1 Destruktor 
    destructor Destroy; override;
    // 1 Benachrichtigt den Aufrufer, dass ein neuer Parameter gefunden wurde 
    procedure DoNewParam(aParamName: string);
    // 1 Parst den String nach Parametern 
    procedure ParseSQL;
    // 1 Wird aufgerufen, wenn der Parser einen neuen Parameter gefunden hat 
    property OnNewParam: TNewParamEvent read FOnNewParam write FOnNewParam;
    // 1 Zeichenfolge mit welcher ein Parameter ersetzt wird. 
    property ReplaceToken: Char read FReplaceToken write FReplaceToken;
    // 1 Zu parsender Text 
    property Text: String read FText write SetText;
  end;
  
  TAdoDBAccess = class(TObject)
  private
    FConnection: _Connection;
    FDataBase: TAdoDBAccessDB;
    FDBError: DWord;
    FDBErrorTxt: String;
    FDBName: String;
    FHostName: String;
    FInitialized: Boolean;
    FPassword: String;
    FQuery: TAdoQueryArr;
    FTransIsolation: TTransIsolation;
    FUserName: String;
    mNotOwnConnection: Boolean;
    mNumQueries: Integer;
    mOutputDebugMsg: Boolean;
    mTransLevel: Integer;
    // 1 Setzt die �bergebene Connection 
    procedure SetConnection(Value: _Connection);
    // 1 Setzt den Isolationslevel der Connection. 
    procedure SetTransIsolation(Value: TTransIsolation);
  public
    // 1 Konstruktor 
    constructor Create(aNumOfQueries : integer; aDBODBC: Boolean = false); overload;
    // 1 Konstruktor 
    constructor Create(aNumOfQueries : integer; aConnection: _Connection); overload;
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    destructor Destroy; override;
    // 1 Schreibt die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Commit;
    // 1 Erzeugt die Connection der Klasse 
    procedure CreateConnection;
    // 1 Erzeugt die Queries 
    procedure CreateQueries(aNumOfQueries: integer);
    function Init: Boolean; overload;
    // 1 True, wenn bereits eine Transaktion ausgef�hrt wird 
    function InTransaction: Boolean;
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Rollback;
    // 1 Setzt den Connectionstring zusammen 
    procedure SetConnectionString;
    // 1 Startet eine neue Transaktion 
    procedure StartTransaction;
    // 1 �ber diese Connection erfolgt der Zugriff auf die Datenbank. 
    property Connection: _Connection read FConnection write SetConnection;
    // 1 Simuliert in Hinsicht auf die Transaktionen ein TDatabase Objekt 
    property DataBase: TAdoDBAccessDB read FDataBase;
    // 1 Datenbank Error 
    property DBError: DWord read FDBError;
    // 1 Fehlermeldungstext 
    property DBErrorTxt: String read FDBErrorTxt;
    // 1 Name der Datenbank auf dem SQL Server 
    property DBName: String read FDBName write FDBName;
    // 1 Name der SQL Server Instanz 
    property HostName: String read FHostName write FHostName;
    // 1 True, wenn der Init durchgef�hrt wurde 
    property Initialized: Boolean read FInitialized write FInitialized;
    // 1 Passwort zum Benutzer 
    property Password: String read FPassword write FPassword;
    // 1 Array mit Queries die benutzt werden k�nnen 
    property Query: TAdoQueryArr read FQuery write FQuery;
    // 1 Setzt den Isolationslevel der Connection (Default tiReadCommited) 
    property TransIsolation: TTransIsolation read FTransIsolation write SetTransIsolation;
    // 1 Benutzername f�r die Verbindung zum Host 
    property UserName: String read FUserName write FUserName;
  end;
  
  (*
  Die Parameter werden in einer Liste vom Typ 'TAdoParams' verwaltet.
  *)
  TmmAdoParameter = class(TPersistent)
  private
    FADOStream: Stream;
    FADOType: Integer;
    FAsBoolean: Boolean;
    FAsDateTime: TDateTime;
    FAsFloat: Double;
    FAsInteger: Integer;
    FAsString: String;
    FName: String;
    FParamType: TFieldType;
    mAdoParams: TmmAdoParams;
    // 1 Gibt den Wert des Parameters als Boolean zur�ck 
    function GetAsBoolean: Boolean;
    // 1 Gibt den Wert des Parameters als TDateTime zur�ck 
    function GetAsDateTime: TDateTime;
    // 1 Gibt den Wert des Parameters als Float zur�ck 
    function GetAsFloat: Double;
    // 1 Gibt den Wert des Parameters als Integer zur�ck 
    function GetAsInteger: Integer;
    // 1 Gibt den Wert des Parameters als String zur�ck 
    function GetAsString: String;
    procedure SetADOType(Value: Integer);
    // 1 Setzt den Wert des Parameters als Boolean 
    procedure SetAsBoolean(const Value: Boolean);
    // 1 Setzt den Wert des Parameters als TDateTime 
    procedure SetAsDateTime(Value: TDateTime);
    // 1 Setzt den Wert des Parameters als Float 
    procedure SetAsFloat(Value: Double);
    // 1 Setzt den Wert des Parameters als Integer 
    procedure SetAsInteger(Value: Integer);
    procedure SetAsString(const Value: String);
  public
    // 1 Neuer Konstruktor 
    constructor Create(aName: string; aParams: TmmAdoParams); overload;
    // 1 Destruktor 
    destructor Destroy; override;
    // 1 Setzt die Daten f�r den entsprechenden Parameter 
    procedure SetBlobData(aBuffer: Pointer; Size: Integer);
    // 1 ADO Stream f�r Blob Felder 
    property ADOStream: Stream read FADOStream write FADOStream;
    // 1 ADO kann manchmal den Parametertyp nicht korrekt erkennen, deshalb muss der Typ bekannt gegeben werden 
    property ADOType: Integer read FADOType write SetADOType;
    // 1 Setzt oder gibt den Wert des Parameters als Boolean 
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    // 1 Setzt oder gibt den Wert des Parameters als TDateTime 
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    // 1 Setzt oder gibt den Wert des Parameters als Float 
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    // 1 Setzt oder gibt den Wert des Parameters als Integer 
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    // 1 Setzt oder gibt den Wert des Parameters als String 
    property AsString: String read GetAsString write SetAsString;
    // 1 Name des Parameters 
    property Name: String read FName;
    // 1 Datentyp des Parameters 
    property ParamType: TFieldType read FParamType;
  end;
  
  TmmAdoParams = class(TBaseList)
  private
    FAdoParamParser: TAdoParamParser;
    FParamCheck: Boolean;
    // 1 Zugriffsmethode auf die aggregierte Liste 
    function GetItems(Index: Integer): TmmAdoParameter;
    // 1 Zugriffsmethode auf die aggregierte Liste 
    procedure SetItems(Index: Integer; Value: TmmAdoParameter);
  public
    // 1 Konstruktor 
    constructor Create; overload; override;
    // 1 Destruktor 
    destructor destroy; override;
    // 1 Erzeugt die Parameterliste 
    function CreateParams(const aText: string; aCreateNew:boolean = true): String;
    // 1 Wird vom Parser aufgerufen, sobald ein neuer Parameter gefunden wurde 
    procedure FoundNewParam(aSender: TObject; aNewParamName:string);
    // 1 Liefert den Index eines Parameters mit dem entsprechenden Namen 
    function IndexOf(aName: string): Integer; overload;
    // 1 Sucht den Parameter in der Auflistung  
    function ParamByName(const aName: string): TmmAdoParameter;
    // 1 Gibt den Parameter mit dem entsprechenden Index zur�ck 
    property Items[Index: Integer]: TmmAdoParameter read GetItems write SetItems; default;
    // 1 True, wenn die Parameter jedes geparst werden sollen 
    property ParamCheck: Boolean read FParamCheck write FParamCheck default true;
  end;
  
  TmmAdoField = class(TPersistent)
  private
    FAsBoolean: Boolean;
    FAsDateTime: TDateTime;
    FAsFloat: Double;
    FAsInteger: Integer;
    FAsString: String;
    FFieldType: TFieldType;
    FIndex: Integer;
    FName: String;
    mRecordset: _Recordset;
    // 1 Gibt den Wert des Parameters als Boolean zur�ck 
    function GetAsBoolean: Boolean;
    // 1 Gibt den Wert des Parameters als TDateTime zur�ck 
    function GetAsDateTime: TDateTime;
    // 1 Gibt den Wert des Parameters als Float zur�ck 
    function GetAsFloat: Double;
    // 1 Gibt den Wert des Parameters als Integer zur�ck 
    function GetAsInteger: Integer;
    // 1 Gibt den Wert des Parameters als String zur�ck 
    function GetAsString: String;
    // 1 Gibt die Position des Feldes in der Auflistung zur�ck 
    function GetFieldNo: Integer;
    // 1 Fragt den Wert des Feldes bei ADO an 
    function GetValue: Variant;
    // 1 Setzt den Wert des Parameters als Boolean 
    procedure SetAsBoolean(const NewValue: Boolean);
    // 1 Setzt den Wert des Parameters als TDateTime 
    procedure SetAsDateTime(NewValue: TDateTime);
    // 1 Setzt den Wert des Parameters als Float 
    procedure SetAsFloat(NewValue: Double);
    // 1 Setzt den Wert des Parameters als Integer 
    procedure SetAsInteger(NewValue: Integer);
    // 1 Setzt den Wert des Parameters als String 
    procedure SetAsString(const NewValue: String);
  public
    // 1 Neuer Konstruktor 
    constructor Create(aName: string; aIndex: integer; aRecordset: _Recordset); overload;
    // 1 Gibt zur�ck ob das Feld einen Wert hat 
    function IsNull: Boolean;
    // 1 Setzt oder gibt den Wert des Parameters als Boolean 
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    // 1 Setzt oder gibt den Wert des Parameters als TDateTime 
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    // 1 Setzt oder gibt den Wert des Parameters als Float 
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    // 1 Setzt oder gibt den Wert des Parameters als Integer 
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    // 1 Setzt oder gibt den Wert des Parameters als String 
    property AsString: String read GetAsString write SetAsString;
    // 1 Position des Feldes in der Auflistung 
    property FieldNo: Integer read GetFieldNo;
    // 1 Datentyp des Feldes 
    property FieldType: TFieldType read FFieldType;
    // 1 Index des Feldes in der Datenmenge 
    property Index: Integer read FIndex;
    // 1 Name des Parameters 
    property Name: String read FName;
    // 1 Eigentlicher Wert des Feldes 
    property Value: Variant read GetValue;
  end;
  
  (*
  Die Felder werden bei ADO angefragt (Nach der Ausf�hrung von SELECT)
  *)
  TmmAdoFields = class(TBaseList)
  private
    // 1 Zugriffsmethode auf die aggregierte Liste 
    function GetItems(Index: Integer): TmmAdoField;
    // 1 Zugriffsmethode auf die aggregierte Liste 
    procedure SetItems(Index: Integer; Value: TmmAdoField);
  public
    // 1 Sucht das Feld in der Auflistung  
    function FieldByName(const aName: string): TmmAdoField;
    // 1 Findet ein Feld anhand des Namens 
    function FieldNo(aName: string): Integer;
    // 1 Liefert den Index eines Feldes mit dem entsprechenden Namen 
    function IndexOf(aName: string): Integer; overload;
    // 1 Gibt den Parameter mit dem entsprechenden Index zur�ck 
    property Items[Index: Integer]: TmmAdoField read GetItems write SetItems; default;
  end;
  
  (*
  Die Klasse stellt auch ein _Recordset zu Verf�gung das f�r 
  SELECT Abfragen eingesetzt wird. Das Recordset kann auch einem
  TADOQuery (ADO Express) oder einem TmmADOQuery zugewiesen werden.
  Die G�ltigkeitsdauer ist allerdings auf die Lebensdauer des 
  TAdoDBAccess Objektes beschr�nkt
  *)
  TNativeAdoQuery = class(TPersistent)
  private
    FTransIsolation: TTransIsolation;
    mDBAccess: TAdoDBAccess;
    mIsIdValid: Boolean;
    mUpdating: Boolean;
    // 1 gibt den Staus der Datenbank Verbindung zur�ck 
    function GetActive: Boolean;
    // 1 Gibt das Recordset als XMLStream zur�ck 
    function GetAsXML: String;
    // 1 Pr�ft ob der Datenzeiger vor dem ersten Datensatz steht 
    function GetBOF: Boolean;
    // 1 Erm�glicht den Zugriff auf das native Command Objekt 
    function GetCommand: _Command;
    // 1 Pr�ft ob der Datenzeiger hinter dem letzten Datensatz steht 
    function GetEOF: Boolean;
    // 1 Delegiert die Verwaltung an die Parameterliste 
    function GetParamCheck: Boolean;
    // 1 Gibt das native Recordset zur�ck 
    function GetRecordset: _Recordset;
    // 1 Extrahiert den SQL-String aus der Stringliste 
    function GetSQL: String;
    // 1 Setzt den Status der Datenbankverbindung 
    procedure SetActive(Value: Boolean);
    // 1 Setzt die Datenmange aus einem XMLStream der zuvor mit 'GetAsXMLString' erzeugt wurde 
    procedure SetAsXML(const Value: String);
    // 1 Zugriffsmethode f�r die Connection 
    procedure SetConnection(Value: _Connection);
    // 1 Delegiert die Verwaltung an die Parameterliste 
    procedure SetParamCheck(Value: Boolean);
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure SetParams;
    // 1 Setzt das native Recordset 
    procedure SetRecordset(Value: _Recordset);
    // 1 Setzt den Isolationslevel der Connection. 
    procedure SetTransIsolation(Value: TTransIsolation);
    procedure SQLChange(Sender: TObject);
  protected
    FCommand: _Command;
    FConnection: _Connection;
    FFields: TmmAdoFields;
    FParams: TmmAdoParams;
    FRecordset: _Recordset;
    FSQL: TStrings;
    // 1 Wird ausgel�st wenn die StringList "SQL" gel�scht wird. 
    procedure OnClearSQL(Sender: TObject); virtual;
  public
    // 1 Konstruktor 
    constructor Create; overload;
    // 1 Konstruktor in dem der Owner mitgegeben wird (wird f�r Transaktionsverwaltung ben�tigt) 
    constructor Create(aOwner: TAdoDBAccess); overload;
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    destructor Destroy; override;
    // 1 Kopiert die Eigenschaften des Source Objekt auf die eigenen Eigenschaften 
    procedure Assign(aSource: TPersistent); override;
    // 1 Kopiert die Daten des Source Datasets 
    procedure Clone(aSource: TNativeADOQuery);
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Close; virtual;
    // 1 Sendet eine Liste der aufgetretenen Fehler an CodeSite 
    function CodeSiteSendError(aErrorMsg:string): String;
    // 1 Schreibt die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Commit;
    // 1 Erzeugt die Fields Auflistung 
    procedure CreateFields;
    function ExecSQL: Integer;
    // 1 Sucht das Feld in der Auflistung  
    function FieldByName(const aName: string): TmmAdoField;
    // 1 Wendet einen Filter auf das zugrunde liegende Recordset an. 
    procedure Filter(aFilterstring: string);
    // 1 Findet ein Feld anhand seines Namens 
    function FindField(aName: string): TmmAdoField;
    // 1 Geht zum ersten Datensatz 
    function FindFirst: Boolean;
    // 1 Geht zum letzten Datensatz 
    function FindLast: Boolean;
    // 1 Geht zum n�chsten Datensatz 
    function FindNext: Boolean;
    // 1 Geht zum vorherigen Datensatz 
    function FindPrior: Boolean;
    // 1 Geht zum ersten Datensatz 
    procedure First;
    // 1 Holt die Daten aus einem BLOB Feld 
    function GetBlobFieldData(aFieldNr: integer; var aBuffer: TBlobByteData): Integer; overload;
    // 1 Holt die Daten aus einem BLOB Feld 
    function GetBlobFieldData(aFieldName: string; var aStream: TMemoryStream): Integer; overload;
    // 1 F�gt einen Datensatz in die Datenbank ein (Urspr�ngliche Funktion) 
    function InsertSQL(aTable, aIDField : String; aMaxIDVal, aMinIDVal : integer): Integer; overload;
    // 1 True, wenn bereits eine Transaktion ausgef�hrt wird 
    function InTransaction: Boolean;
    // 1 Geht zum letzten Datensatz 
    procedure Last;
    // 1 Geht zum n�chsten Datensatz 
    procedure Next;
    // 1 F�hrt ein SQL Query aus (SELECT) 
    function Open: Integer; virtual;
    // 1 Sucht den Parameter in der Auflistung  
    function ParamByName(const aName: string): TmmAdoParameter;
    // 1 Geht zum vorhergehenden Datensatz 
    procedure Prior;
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Rollback;
    function SendDebugQuery: String; virtual;
    // 1 Sendet ein kompletes Recordset an Codesite 
    procedure SendRecordsetToCodeSite(aName: string; aCount: integer);
    // 1 Setzt die Sortierung des Recordsets 
    procedure Sort(aSortString: string);
    // 1 Startet eine neue Transaktion 
    procedure StartTransaction;
    // 1 Versetzt die Datenmenge in den gew�nschten Status 
    property Active: Boolean read GetActive write SetActive;
    // 1 Setzt oder Gibt das Recordset als XMLStream 
    property AsXML: String read GetAsXML write SetAsXML;
    // 1 Pr�ft ob der Datenzeiger vor dem ersten Datensatz steht 
    property BOF: Boolean read GetBOF;
    // 1 Erm�glicht den Zugriff auf das native Command Objekt 
    property Command: _Command read GetCommand;
    // 1 Connection �ber die der Datenbankzugriff erfolgt 
    property Connection: _Connection read FConnection write SetConnection;
    // 1 Pr�ft ob der Datenzeiger hinter dem letzten Datensatz steht 
    property EOF: Boolean read GetEOF;
    // 1 Fragt die Fields Collection des aktuellen Record ab 
    property Fields: TmmAdoFields read FFields;
    // 1 True, wenn jedes mal wenn das Query �ndert die Parameter abgefragt werden sollen 
    property ParamCheck: Boolean read GetParamCheck write SetParamCheck default true;
    // 1 Liste mit den Parametern 
    property Params: TmmAdoParams read FParams;
    // 1 Gibt das zugrunde liegende Recordset zur�ck 
    property Recordset: _Recordset read GetRecordset write SetRecordset;
    // 1 SQL String als Stringliste (wie TQuery) 
    property SQL: TStrings read FSQL write FSQL;
    // 1 Setzt den Isolationslevel der Connection (Default tiReadCommited) 
    property TransIsolation: TTransIsolation read FTransIsolation write SetTransIsolation;
  end;
  
  TDBAccessStringList = class(TStringList)
  private
    FOnClearList: TNotifyEvent;
  public
    // 1 L�scht die Liste und damit auch die Parameter 
    procedure Clear; override;
  published
    property OnClearList: TNotifyEvent read FOnClearList write FOnClearList;
  end;
  
  (*
  Das DB Objekt in der Original DBAccessClass,
  handelt die Transaktionen. Diese werden in der 
  Klasse AdoDBAccess vom internen Connection Objekt
  verwaltet. Um kompatibel zur BDE Vewrsion zu bleiben,
  werden die entsprechenden Aufrufe simuliert.
  Der endg�ltige Zugriff erfolgt dann �ber die
  interne Connection.
  *)
  TAdoDBAccessDB = class(TObject)
  private
    FTransIsolation: TTransIsolation;
    mDBAccess: TAdoDBAccess;
    // 1 Setzt den Isolationslevel der Connection. 
    procedure SetTransIsolation(Value: TTransIsolation);
  public
    // 1 Konstruktor 
    constructor Create(aDBAccess: TAdoDbAccess); overload;
    // 1 Schreibt die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Commit;
    // 1 True, wenn bereits eine Transaktion ausgef�hrt wird 
    function InTransaction: Boolean;
    // 1 Verwirft die �nderungen die w�hrend der Transaktion gemacht wurden 
    procedure Rollback;
    // 1 Startet eine neue Transaktion 
    procedure StartTransaction;
    // 1 Setzt den Isolationslevel der Connection (Default tiReadCommited) 
    property TransIsolation: TTransIsolation read FTransIsolation write SetTransIsolation;
  end;
  


implementation

uses
  mmMBCS,
  MMUGlobal, LoepfeGlobal, mmcs, BaseGlobal, comobj, typinfo;

const
  cr   = #13;
  lf   = #10;
  tab  = #9;
  crlf = cr+lf;



(*
TList kann nicht polymorph verwendet werden.
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TBaseList
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TBaseList.Create 
(*
Erzeugt die interne Liste
*)
constructor TBaseList.Create;
begin
  FList := TList.Create;
  FOwnsObjects := true;
end;{ TBaseList.Create }

//: TBaseList.destroy 
(*
R�umt die interne Liste auf
*)
destructor TBaseList.destroy;
begin
  Clear;
  FList.Free;
  inherited Destroy;
end;{ TBaseList.destroy }

//: TBaseList.Add 
(*
-
*)
function TBaseList.Add(Item: Pointer): Integer;
begin
  result := FList.Add(Item);
end;{ TBaseList.Add }

//: TBaseList.Clear 
(*
-
*)
procedure TBaseList.Clear;
begin
  while count > 0 do begin
    if FOwnsObjects then
      TObject(FList.Items[0]).Free;
    FList.Delete(0);
  end;// while count > 0 do begin
end;{ TBaseList.Clear }

//: TBaseList.Delete 
(*
-
*)
procedure TBaseList.Delete(Index: Integer);
begin
  if (Index >= 0) and (Index < Count) then begin
    if FOwnsObjects then
      Items[Index].Free;
    FList.delete(Index);
  end;// if (Index >= 0) and (Index < Count) then begin
end;{ TBaseList.Delete }

//: TBaseList.Exchange 
(*
-
*)
procedure TBaseList.Exchange(Index1, Index2: Integer);
begin
  FList.Exchange(Index1, Index2);
end;{ TBaseList.Exchange }

//: TBaseList.GetCount 
(*
-
*)
function TBaseList.GetCount: Integer;
begin
  result := FList.Count;
end;{ TBaseList.GetCount }

//: TBaseList.GetItems 
(*
-
*)
function TBaseList.GetItems(Index: Integer): TObject;
begin
  result := TObject(FList.Items[Index]);
end;{ TBaseList.GetItems }

//: TBaseList.IndexOf 
(*
-
*)
function TBaseList.IndexOf(Item: TObject): Integer;
begin
  result := FList.IndexOf(Item);
end;{ TBaseList.IndexOf }

//: TBaseList.SetItems 
(*
-
*)
procedure TBaseList.SetItems(Index: Integer; aValue: TObject);
begin
  FList[Index] := aValue;
end;{ TBaseList.SetItems }

(*
Der Parser benachrichtigt das aufrufende Objekt dar�ber, wenn 
ein neuer Parameter gefunden wurde. Das aufrufende Objekt kann 
dann einen TmmAdoParameter mit dem entsprechenden Namen erzeugen.
Ausserdem muss f�r die Ausf�hrung �ber ADO die Definition des 
Parameters von der Art der BDE (':Parametername') zu ADO('?') 
gewandelt werden.

BDE: 'SELECT c_Name FORM t_Adresse WHERE c_ID = :ID'
ADO: 'SELECT c_Name FORM t_Adresse WHERE c_ID = :?'
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TAdoParamParser
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TAdoParamParser.Create 
(*
-
*)
constructor TAdoParamParser.Create;
begin
  // ADO Parameter werden mit '?' sustituiert
  ReplaceToken := '?';
  {-------------------------------------------------------------------------------}
  inherited Create;
end;{ TAdoParamParser.Create }

//: TAdoParamParser.Destroy 
(*
-
*)
destructor TAdoParamParser.Destroy;
begin
  inherited Destroy;
end;{ TAdoParamParser.Destroy }

//: TAdoParamParser.DoNewParam 
(*
-
*)
procedure TAdoParamParser.DoNewParam(aParamName: string);
begin
  if assigned(FOnNewParam) then
    FOnNewParam(self,aParamName);
end;{ TAdoParamParser.DoNewParam }

//: TAdoParamParser.ParseSQL 
(*
Die Parameter werden in der Form ':ParamName' angeliefert
und m�ssen durch '?' ersetzt werden. Wurde ein Parameter 
gefunden, wird der Aufrufer durch 'DoNewParam' benachrichtigt.

Der Parser ist aus der Unit 'AdoDB.pas' kopiert und an diversen 
Stellen etwas angepasst.

MBCS Zeichen werden nicht beachtet. Dies ist auch nicht n�tig, da 
die Delimiter-Zeichen (cLiteral und cNameDelimiter) sowiso im
ASCII Zeichensatz angesiedelt sind.
*)
procedure TAdoParamParser.ParseSQL;
var
  xValue: PChar;
  xCurPos: PChar;
  xStartPos: PChar;
  xCurChar: Char;
  xLiteral: Boolean;
  xEmbeddedLiteral: Boolean;
  xName: String;
  
  const
    cLiterals      = ['''', '"', '`'];
    cNameDelimiter = [' ', ',', ';', ')', #13, #10];
  
  // Pr�ft ob das aktuelle Zeichen ein Parameter-Trennzeichen ist.
  // Auf jeden Parameter muss ein Zeichen aus cNameDelimiter folgen
  function IsNameDelimiter: Boolean;
  begin
    Result := xCurChar in cNameDelimiter;
  end;// function IsNameDelimiter: Boolean;
  
  // Pr�ft ob ein String beginnt oder endet.
  function IsLiteral: Boolean;
  begin
    Result := xCurChar in cLiterals;
  end;// function IsLiteral: Boolean;
  
  // Entfernt die Anf�hrungs- und Schlusszeichen in einem Parameter
    // (INSERT INTO t_Table (c_ID) VALUES (:"c_ID") wird zu
    // (INSERT INTO t_Table (c_ID) VALUES (:c_ID)
  function StripLiterals(aBuffer: PChar): string;
  var
    xLen: Word;
    xTempBuf: PChar;
  
    procedure StripChar;
    begin
      // Entfernt das vordere Literal
      if xTempBuf^ in cLiterals then
        StrMove(xTempBuf, xTempBuf + 1, xLen - 1);
      // Entfernt das hintere Literal
      if xTempBuf[StrLen(xTempBuf) - 1] in cLiterals then
        xTempBuf[StrLen(xTempBuf) - 1] := #0;
    end;// procedure StripChar;
  begin
    xLen := StrLen(aBuffer) + 1;
    // Platz schaffen f�r String + 1 (0-Byte)
    xTempBuf := AllocMem(xLen);
    Result := '';
    try
      StrCopy(xTempBuf, aBuffer);
      // Entfernt die Literale
      StripChar;
      Result := StrPas(xTempBuf);
    finally
      FreeMem(xTempBuf, xLen);
    end;// try finally
  end;// function StripLiterals(aBuffer: PChar): string;
  
begin
  // Initialisieren
  xValue := PChar(FText);
  
  xCurPos := xValue;
  xLiteral := False;
  xEmbeddedLiteral := False;
  {-------------------------------------------------------------------------------}
  // Parser
  repeat
    // MultiByte Charakter �berspringen
    while (xCurPos^ in LeadBytes) do
      Inc(xCurPos, 2);
    // Aktuelles Zeichen holen
    xCurChar := xCurPos^;
  
    // Pr�fen ob ein Parameter beginnt
    if (xCurChar = ':') and not xLiteral and ((xCurPos + 1)^ <> ':') then begin
      // Erstes Zeichen des Parameters speichern (':')
      xStartPos := xCurPos;
  
      // Bis Zeilenende oder bis zum Trennzeichen suchen
      while (xCurChar <> #0) and (xLiteral or not IsNameDelimiter) do begin
        Inc(xCurPos);
        // MBCS Cahrakter �berspringen
        while (xCurPos^ in LeadBytes) do
          Inc(xCurPos, 2);
        // Aktuelles Zeichen holen
        xCurChar := xCurPos^;
        if IsLiteral then begin
          // In einem String darf nicht nach Parametern gesucht werden
          xLiteral := xLiteral xor true;
          // Wenn direkt nach dem ':' ein Literal steht, dann ist der Parametername...
          // ... in Literale eingefasst (..., :"ParamName", ...)
          if xCurPos = xStartPos + 1 then
            xEmbeddedLiteral := True;
        end;// if IsLiteral then begin
      end;// while (xCurChar <> #0) and (xLiteral or not IsNameDelimiter) do begin
  
      // Den String tempor�r k�rzen um den Parameternamen zu extrahieren
      xCurPos^ := #0;
  
      if xEmbeddedLiteral then begin
        // Literale entfernen, wenn der Parameternamen in Literale eingefasst ist
        // ... z.B: '..., :"ParamName", ...'
        xName := StripLiterals(xStartPos + 1);
        xEmbeddedLiteral := False;
      end else begin
        xName := StrPas(xStartPos + 1);
      end;// if xEmbeddedLiteral then begin
  
      // Aufrufer benachrichtigen, dass ein neuer Parameter gefunden wurde
      DoNewParam(xName);
  
      // String wieder verl�ngern
      xCurPos^ := xCurChar;
  
      // Ersatz Token einsetzen
      xStartPos^ := FReplaceToken;
  
      // Auf n�chstes Zeichen nach dem Token setzen
      Inc(xStartPos);
  
      StrMove(xStartPos, xCurPos, StrLen(xCurPos) + 1);
      // Auf erstes Zeichen nach dem Parameter setzen
      xCurPos := xStartPos;
    end else begin // if (xCurChar = ':') and ...
      if (xCurChar = ':') and not xLiteral and ((xCurPos + 1)^ = ':') then begin
        StrMove(xCurPos, xCurPos + 1, StrLen(xCurPos) + 1)
      end else begin
        if IsLiteral then
          xLiteral := xLiteral xor True;
      end;// if (xCurChar = ':') and ...
    end;// if (xCurChar = ':') and ...
    // N�chstes Zeichen
    Inc(xCurPos);
  // Fertig wenn Stringende
  until xCurChar = #0;
  {-------------------------------------------------------------------------------}
  // Text zur�ck kopieren
  FText := StrPas(xValue);
end;{ TAdoParamParser.ParseSQL }

//: TAdoParamParser.SetText 
(*
Der String wird sofort geparst und mit dem
Ereignis OnNewParam wird der Aufrufer benachrichtigt,
dass ein neuer Parameter gefunden wurde.
*)
procedure TAdoParamParser.SetText(Value: String);
begin
  if FText <> Value then begin
    FText := Value;
    // Ersetzt die Parameter der Form ':ParamName' mit FReplaceToken.
    // Ausserdem wird f�r jeden gefundenen Parameter das Ereignis OnNewParam gefeuert
    ParseSQL;
  end;// if FText <> Value then begin
end;{ TAdoParamParser.SetText }

{-**********************************************************************
************************************************************************
******************
******************  Class:    TAdoDBAccess
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TAdoDBAccess.Create 
(*
Der Konstruktor erzeugt bereits die gew�nschte Anzahl Queries auf 
die dann �ber das Array zugegriffen werden kann.
*)
constructor TAdoDBAccess.Create(aNumOfQueries : integer; aDBODBC: Boolean = false);
begin
  FDataBase    := TAdoDBAccessDB.Create(self);
  FInitialized := false;
  {-------------------------------------------------------------------------------}
  // Erzeugt die zugeh�rige Connection
  if not(mNotOwnConnection) then
    CreateConnection;
  
  // Defaultwert den die BDE gesetzt hat
  TransIsolation := tiReadCommitted;
  
  // Erzeugt die gew�nschte Anzahl Queries
  CreateQueries(aNumOfQueries);
  {-------------------------------------------------------------------------------}
  // Debug Settings aus der Registry lesen
  mOutputDebugMsg := GetClassDebug(ClassName);
end;{ TAdoDBAccess.Create }

//: TAdoDBAccess.Create 
(*
Der Konstruktor erzeugt bereits die gew�nschte Anzahl Queries auf 
die dann �ber das Array zugegriffen werden kann.
*)
constructor TAdoDBAccess.Create(aNumOfQueries : integer; aConnection: _Connection);
begin
  // Connection �bernehmen
  FConnection := aConnection;
  
  // Die Connection muss nicht erzeugt werden
  mNotOwnConnection := true;
  
  // Default Konstruktor
  Create(aNumOfQueries);
end;{ TAdoDBAccess.Create }

//: TAdoDBAccess.Destroy 
(*
Die Datenbankverbindung und die Queries werden freigegeben.
*)
destructor TAdoDBAccess.Destroy;
var
  i: Integer;
begin
  try
    (* Wenn die Connection noch in einer Transaktion ist,
       dann muss ein Rollback durchgef�hrt werden.
       Dies kann vorkommen, wenn der Destruktor im Kontext eines anderen
       Threads aufgerufen wird. *)
    if InTransaction then begin
      Rollback;
    end;
  except
    on e:Exception do begin
      Codesite.SendError('ADODBAccess: ' + e.Message + ' (' + IntToStr(GetCurrentThreadID) +')');
      raise;
    end;// on e:Exception do begin
  end;// try except
  
  // Alle Queries schliessen und freigeben
  for i := 0 to mNumQueries - 1 do begin
    if assigned(FQuery[i]) then begin
      FQuery[i].Close;
    end;
    FreeAndNil(FQuery[i]);
  end;// for i := 0 to mNumQueries do begin
  {-------------------------------------------------------------------------------}
  // Die Connection ist nicht initialisiert, wenn im Konstruktor ein Fehler
  // ... aufgetreten ist.
  if assigned(Connection) then begin
    if not(mNotOwnConnection) then begin
      // Connection schliessen
      if not(Connection.State = adStateClosed) then begin
        FConnection.Close;
      end;// if not(Connection.State = adStateClosed) then begin
    end;// if not(mNotOwnConnection) then begin
  end;// if assigned(Connection) then begin
  FConnection := nil;
  {-------------------------------------------------------------------------------}
  FreeAndNil(FDataBase);
  {-------------------------------------------------------------------------------}
  inherited Destroy;
end;{ TAdoDBAccess.Destroy }

//: TAdoDBAccess.Commit 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccess.Commit;
begin
  if assigned(FConnection) then begin
    (* Bei verschachtelten Transaktionen wird nur die �usserste Transaktion
       best�tigt. Innere Transaktionen werden nicht beachtet. *)
    if mTransLevel = 1 then begin
      FConnection.CommitTrans;
      if mOutputDebugMsg then
        CodeSite.SendMsg('TAdoDBAccess.Commit: Execute Commit Transaction');
    end;// if mTransLevel = 1 then begin
  
    // Wenn eine Transaktion aktiv war, dann den Z�hler wierder zur�cksetzen
    if mTransLevel > 0 then
      dec(mTransLevel);
  end else begin
    raise Exception.Create('TAdoDBAccess.Commit: No Connection assigned');
  end;// if assigned(FConnection) then begin
end;{ TAdoDBAccess.Commit }

//: TAdoDBAccess.CreateConnection 
(*
-
*)
procedure TAdoDBAccess.CreateConnection;
begin
  FConnection := CoConnection.Create;
  
  HostName := gSQLServerName;
  DBName   := gDBName;
  Password := gDBPassword;
  UserName := gDBUserName;
  
  // Initialisieren der Connection
  SetConnectionString;
end;{ TAdoDBAccess.CreateConnection }

//: TAdoDBAccess.CreateQueries 
(*
-
*)
procedure TAdoDBAccess.CreateQueries(aNumOfQueries: integer);
var
  i: Integer;
begin
  if (aNumOfQueries > 0)and(aNumOfQueries <= cMaxQueries) then begin
    mNumQueries := aNumOfQueries;
    // Erzeugen der Queries
    for i := 0 to aNumOfQueries - 1 do
      FQuery[i] := TNativeAdoQuery.Create(self);
  end;// if (aNumOfQueries > 0)and(aNumOfQueries <= cMaxQueries) then begin
end;{ TAdoDBAccess.CreateQueries }

//: TAdoDBAccess.Init 
(*
-
*)
function TAdoDBAccess.Init: Boolean;
var
  i: Integer;
  xConnectionString: String;
  xExceptAddrString: String;
  xExceptAddr: Pointer;
begin
  fDBErrorTxt := '';
  
  Result := False;
  if (mNumQueries > 0) then begin
    if (mNumQueries <= cMaxQueries) then begin
      try
       if not(mNotOwnConnection) then begin
          // Connection string setzt sich aus den Properties DBName, HostName,
          // UserName und Password zusammen
          SetConnectionString;
          // Versucht die Connection zu �ffnen und weist dann allen Queries die
          // offene Connection zu
  
          // Passwort auskommentieren f�r die CodeSite Meldung (auskommentiert 30.6.2003 LOK)
          xConnectionString := StringReplace(FConnection.ConnectionString,Password,'********',[]);
  //        CodeSite.SendString('AdoDbAccess: Connection String before ''Open''', xConnectionString);
  
          // Es wird hier nicht der Firehouse Cursor verwendet, da dann unter Umst�nden
          // der Cursor nicht auf den ersten Datensatz zur�ckgestellt werden kann.$
          // Dieses Verhalten ist nur m�glich wenn CacheSize auf einen Wert gesetzt wird
          // der alle Datens�tze aufnehmen kann und zus�tlich diese auch aufnimmt (ADO).
          FConnection.CursorLocation := adUseClient;
          FConnection.Open(FConnection.ConnectionString,'','',integer(adConnectUnspecified));
  
          // Passwort auskommentieren f�r die CodeSite Meldung (auskommentiert 30.6.2003 LOK)
  //        xConnectionString := StringReplace(FConnection.ConnectionString,Password,'********',[]);
  //        CodeSite.SendString('AdoDbAccess: Connection String After ''Open''', xConnectionString);
        end;// if not(mNotOwnConnection) then begin
  
        for i := 0 to mNumQueries - 1 do
          FQuery[i].Connection := FConnection;
        Result := true;
        FInitialized := true;
      except
        on E:Exception do begin
          fDBError := ERROR_INVALID_FUNCTION;
          xExceptAddr := ExceptAddr;
          xExceptAddrString := 'nil';
          if assigned(xExceptAddr) then
            xExceptAddrString := IntToHex(integer(xExceptAddr), 8);
  
          fDBErrorTxt := 'TAdoDBAccess.Init: ' +
                         E.Message +
                         cCrlf + cCrlf + 'Additional Information: ' + cCrlf +
                         Format('Error Address (hex): %s', [xExceptAddrString]) + cCrlf +
                         'ConnectionString : ' + xConnectionString + cCrlf +
                         Format('Num Queries : %d', [mNumQueries]) + cCrlf +
                         Format('Connection State : %d (0=adStateClosed, 1=adStateOpen, 2=adStateConnecting, 4=adStateExecuting, 8=adStateFetching)',[FConnection.State]);
        end;// on E:Exception do begin
      end;// try except
    end; // if (mNumQueries <= cMaxQueries) then begin
  end; // if (mNumQueries > 0) then begin
end;{ TAdoDBAccess.Init }

//: TAdoDBAccess.InTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
function TAdoDBAccess.InTransaction: Boolean;
begin
  result := mTransLevel > 0;
end;{ TAdoDBAccess.InTransaction }

//: TAdoDBAccess.Rollback 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccess.Rollback;
begin
  if assigned(FConnection) then begin
    if mTransLevel > 0 then begin
      (* aktuelle Transaktion zur�cknehmen.
         Bei verschachtelten Transaktionen ist der Aufrufer daf�r verantwortlich, dass eine Allf�llige
         Exception die zu einem Rollback gef�hrt hat, an den Aufrufer weitergegeben wird und, dass die
         Daten konststent bleiben *)
      FConnection.RollbackTrans;
      // Keine Transaktion mehr aktiv
      mTransLevel := 0;
  
      if mOutputDebugMsg then
        CodeSite.SendMsg('TAdoDBAccess.Rollback: Execute Rollback Transaction');
    end;// if mTransLevel > 0 then begin
  end else begin
    raise Exception.Create('TAdoDBAccess.Rollback: No Connection assigned');
  end;// if assigned(FConnection) then begin
end;{ TAdoDBAccess.Rollback }

//: TAdoDBAccess.SetConnection 
(*
Die Connection muss an alle Queries �bergeben werden.
*)
procedure TAdoDBAccess.SetConnection(Value: _Connection);
var
  i: Integer;
begin
  FConnection := Value;
  for i := 0 to mNumQueries -1 do
    FQuery[i].Connection := FConnection;
end;{ TAdoDBAccess.SetConnection }

//: TAdoDBAccess.SetConnectionString 
(*
Der Connection String setzt sich aus folgenden Properties
zusammen: 
  HostName
  DBName  
  Password
  UserName
*)
procedure TAdoDBAccess.SetConnectionString;
begin
  FConnection.Connectionstring := Format(cDefaultConnectionString,[HostName,DBName,Password,Username]);
end;{ TAdoDBAccess.SetConnectionString }

//: TAdoDBAccess.SetTransIsolation 
(*
Default Level ist 'tiReadCommitted'. Dies entspricht dem 
ADO Wert 'adXactReadCommitted'.
Es werden hier nur die Isolationslevel der BDE unterst�tzt:
  tiDirtyRead
  tiReadCommitted 
  tiRepeatableRead

Nach Dokumentation BDE bedeuten im Falle des SQL Servers die
Werte '  tiDirtyRead' und 'tiReadCommitted' das selbe.

ADO unterst�tztz noch weitere Werte, die hier jedoch nicht implementiert sind.
*)
procedure TAdoDBAccess.SetTransIsolation(Value: TTransIsolation);
begin
  if assigned(FConnection) then begin
    case Value of
      tiDirtyRead,
      tiReadCommitted :
        FConnection.IsolationLevel := adXactReadCommitted;
      tiRepeatableRead:
        FConnection.IsolationLevel := adXactRepeatableRead;
    end;// case Value of
  end;// if assigned(FConnection) then begin
end;{ TAdoDBAccess.SetTransIsolation }

//: TAdoDBAccess.StartTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccess.StartTransaction;
begin
  if assigned(FConnection) then begin
    (* Nur der �usserste Start wird tats�chlich durchgef�hrt.
       Weitere Aufrufe inkrementieren Lediglich einen Z�hler der bei einem Commit wieder
       dekrementiert und bei einem Rollback wieder initialisiert wird. *)
    if mTransLevel = 0 then begin
      FConnection.BeginTrans;
      if mOutputDebugMsg then
        CodeSite.SendMsg('TAdoDBAccess.StartTransaction: Execute Start Transaction');
    end;
    inc(mTransLevel);
  end else begin
    raise Exception.Create('TAdoDBAccess.StartTransaction: No Connection assigned');
  end;// if assigned(FConnection) then begin
end;{ TAdoDBAccess.StartTransaction }

(*
Die Parameter werden in einer Liste vom Typ 'TAdoParams' verwaltet.
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TmmAdoParameter
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TmmAdoParameter.Create 
(*
Der Name des Parameters wird direkt dem Konstruktor �bergeben
*)
constructor TmmAdoParameter.Create(aName: string; aParams: TmmAdoParams);
begin
  Create;
  FName := aName;
  FParamType := ptNone;
  mAdoParams := aParams;
  // Parametertyp nicht zugewiesen
  FAdoType := adEmpty;
  {-------------------------------------------------------------------------------}
  {-------------------------------------------------------------------------------}
end;{ TmmAdoParameter.Create }

//: TmmAdoParameter.Destroy 
(*
-
*)
destructor TmmAdoParameter.Destroy;
begin
  {-------------------------------------------------------------------------------}
  if FADOStream <> nil then begin
    FADOStream.Close;
    FADOStream := nil;
  end;// if FADOStream <> nil then begin
  {-------------------------------------------------------------------------------}
  inherited Destroy;
end;{ TmmAdoParameter.Destroy }

//: TmmAdoParameter.GetAsBoolean 
(*
-
*)
function TmmAdoParameter.GetAsBoolean: Boolean;
begin
  Result := FAsBoolean;
end;{ TmmAdoParameter.GetAsBoolean }

//: TmmAdoParameter.GetAsDateTime 
(*
-
*)
function TmmAdoParameter.GetAsDateTime: TDateTime;
begin
  Result := FAsDateTime;
end;{ TmmAdoParameter.GetAsDateTime }

//: TmmAdoParameter.GetAsFloat 
(*
-
*)
function TmmAdoParameter.GetAsFloat: Double;
begin
  Result := FAsFloat;
end;{ TmmAdoParameter.GetAsFloat }

//: TmmAdoParameter.GetAsInteger 
(*
-
*)
function TmmAdoParameter.GetAsInteger: Integer;
begin
  Result := FAsInteger;
end;{ TmmAdoParameter.GetAsInteger }

//: TmmAdoParameter.GetAsString 
(*
-
*)
function TmmAdoParameter.GetAsString: String;
begin
  Result := FAsString;
end;{ TmmAdoParameter.GetAsString }

//: TmmAdoParameter.SetADOType 
procedure TmmAdoParameter.SetADOType(Value: Integer);
var
  i: Integer;
begin
  FAdoType := Value;
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].ADOType := Value;
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetADOType }

//: TmmAdoParameter.SetAsBoolean 
(*
-
*)
procedure TmmAdoParameter.SetAsBoolean(const Value: Boolean);
var
  i: Integer;
begin
  FAsBoolean := Value;
  FParamType := ptBoolean;
  
  (*                      !!! ACHTUNG !!!
    Beim Hinzuf�gen von neuen Typen darauf achten, dass die Funktion
    SetParams von TNativeAdoQuery entsprechend erg�nzt wird.
  *)
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].AsBoolean := Value;
        // wss: Anstatt ein Break wird hier Q&D der Indexz�hler auf Count gesetzt, damit
        // der Loop abbricht !?!?
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetAsBoolean }

//: TmmAdoParameter.SetAsDateTime 
(*
-
*)
procedure TmmAdoParameter.SetAsDateTime(Value: TDateTime);
var
  i: Integer;
begin
  FAsDateTime := Value;
  FParamType := ptDateTime;
  
  (*                      !!! ACHTUNG !!!
    Beim Hinzuf�gen von neuen Typen darauf achten, dass die Funktion
    SetParams von TNativeAdoQuery entsprechend erg�nzt wird.
  *)
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].AsDateTime := Value;
        // wss: Anstatt ein Break wird hier Q&D der Indexz�hler auf Count gesetzt, damit
        // der Loop abbricht !?!?
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetAsDateTime }

//: TmmAdoParameter.SetAsFloat 
(*
-
*)
procedure TmmAdoParameter.SetAsFloat(Value: Double);
var
  i: Integer;
begin
  FAsFloat := Value;
  FParamType := ptFloat;
  
  (*                      !!! ACHTUNG !!!
    Beim Hinzuf�gen von neuen Typen darauf achten, dass die Funktion
    SetParams von TNativeAdoQuery entsprechend erg�nzt wird.
  *)
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].AsFloat := Value;
        // wss: Anstatt ein Break wird hier Q&D der Indexz�hler auf Count gesetzt, damit
        // der Loop abbricht !?!?
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetAsFloat }

//: TmmAdoParameter.SetAsInteger 
(*
-
*)
procedure TmmAdoParameter.SetAsInteger(Value: Integer);
var
  i: Integer;
begin
  FAsInteger := Value;
  FParamType := ptInteger;
  
  (*                      !!! ACHTUNG !!!
    Beim Hinzuf�gen von neuen Typen darauf achten, dass die Funktion
    SetParams von TNativeAdoQuery entsprechend erg�nzt wird.
  *)
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
  
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].AsInteger := Value;
        // wss: Anstatt ein Break wird hier Q&D der Indexz�hler auf Count gesetzt, damit
        // der Loop abbricht !?!?
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetAsInteger }

//: TmmAdoParameter.SetAsString 
(*
-
*)
procedure TmmAdoParameter.SetAsString(const Value: String);
var
  i: Integer;
begin
  FAsString := Value;
  FParamType := ptString;
  
  (*                      !!! ACHTUNG !!!
    Beim Hinzuf�gen von neuen Typen darauf achten, dass die Funktion
    SetParams von TNativeAdoQuery entsprechend erg�nzt wird.
  *)
  {-------------------------------------------------------------------------------}
  // Beschr�nkt Strings die als Array of Char definiert sind
  // und mit #0 aufgef�llt sind
  FAsString := StrPas(PChar(FAsString));
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].AsString := Value;
        // wss: Anstatt ein Break wird hier Q&D der Indexz�hler auf Count gesetzt, damit
        // der Loop abbricht !?!?
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetAsString }

//: TmmAdoParameter.SetBlobData 
(*
Um die Daten in die Datenbank zu bringen, m�ssen die Daten 
�ber den Parameter gesetzt werden.
Der Stream wird erst erzeugt, wenn er gebraucht wird.
*)
procedure TmmAdoParameter.SetBlobData(aBuffer: Pointer; Size: Integer);
var
  xBuffer: TBlobByteData;
  i: Integer;
  xChar: PByte;
begin
  if FADOStream <> nil then begin
    // Stream besteht bereits und muss nicht neu erzeugt werden
    FADOStream.Position := 0;
    // Setzt das Ende des Strings auf die aktuelle Position
    FADOStream.SetEOS;
  end else begin
    // Stream existiert noch nicht
    FADOStream := CoStream.Create;
    FADOStream.Type_ := adTypeBinary;
    // Stream zum schreiben bereitstellen
    FADOStream.Open(EmptyParam,adModeUnknown,integer(adOpenStreamUnspecified), '', '');
  end;// if FADOStream <> nil then begin
  {-------------------------------------------------------------------------------}
  // Daten umkopieren
  SetLength(xBuffer,Size);
  
  xChar := aBuffer;
  for i := 0 to Size -1  do begin
    xBuffer[i] := xChar^;
    inc(xChar);
  end;
  
  // Daten in den Stream schreiben
  FADOStream.Write(xBuffer);
  
  FADOStream.Position := 0;
  
  FParamType := ptBlob;
  {-------------------------------------------------------------------------------}
  if (mAdoParams<>nil) then begin
    i := 0;
    // Zuerst mal sich selber suchen
    while (mAdoParams.Count > i) and (mAdoParams[i] <> self)do
      inc(i);
  
    // auf n�chsten Parameter zeigen
    inc(i);
    // Ab hier einen Parameter mit gleichem Namen suchen
    while (mAdoParams.Count > i) do begin
      if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
        // Ruft sich rekursiv f�r den gefundenen Parameter auf
        mAdoParams[i].SetBlobData(aBuffer,Size);
        i := mAdoParams.Count;
      end;// if (AnsiCompareText(mAdoParams[i].Name,FName) = 0) then begin
      inc(i);
    end;// while (mAdoParams.Count > i) do begin
  end;// if mAdoParams<>nil then begin
end;{ TmmAdoParameter.SetBlobData }

{-**********************************************************************
************************************************************************
******************
******************  Class:    TmmAdoParams
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TmmAdoParams.Create 
(*
-
*)
constructor TmmAdoParams.Create;
begin
  inherited Create;
  {-------------------------------------------------------------------------------}
  FAdoParamParser := TAdoParamParser.Create;
  {-------------------------------------------------------------------------------}
  ParamCheck := true;
  {-------------------------------------------------------------------------------}
  FAdoParamParser.OnNewParam   := FoundNewParam;
  FAdoParamParser.ReplaceToken := '?';
end;{ TmmAdoParams.Create }

//: TmmAdoParams.destroy 
(*
-
*)
destructor TmmAdoParams.destroy;
begin
  FreeAndNil(FAdoParamParser);
  {-------------------------------------------------------------------------------}
  {-------------------------------------------------------------------------------}
  inherited destroy;
end;{ TmmAdoParams.destroy }

//: TmmAdoParams.CreateParams 
(*
Die Parameter werden von ADO verwaltet. Deshalb muss das 
Query geparst werden und die Parameter der Form ':Name' 
durch die Form '?' ersetzt werden.
F�r jeden gefundenen Parameter wird ein ADO Parameter erzeugt.
*)
function TmmAdoParams.CreateParams(const aText: string; aCreateNew:boolean = true): String;
begin
  // Wenn neue Parameter erzeugt werden sollen, dann die Liste leeren
  if aCreateNew then
    Clear;
  
  // Der Text wird sofort geparst
  FAdoParamParser.Text := aText;
  
  // .. und das Ergebnis steht nachher im Property 'Text'
  result := FAdoParamParser.Text;
end;{ TmmAdoParams.CreateParams }

//: TmmAdoParams.FoundNewParam 
(*
Der neue Parameter wird hier erzeugt.Zu diesem Zweck wird
der Name des Parameters mitgegeben. die Funktion gibt den
Ersatzstring zur�ck. Dieser ist bereits vorbelegt mit '?'.
*)
procedure TmmAdoParams.FoundNewParam(aSender: TObject; aNewParamName:string);
var
  xParameter: TmmAdoParameter;
begin
  // Parameter erzeugen
  xParameter := TmmAdoParameter.Create(aNewParamName,self);
  Add(xParameter);
end;{ TmmAdoParams.FoundNewParam }

//: TmmAdoParams.GetItems 
(*
-
*)
function TmmAdoParams.GetItems(Index: Integer): TmmAdoParameter;
begin
  result := TmmAdoParameter(FList.Items[Index]);
end;{ TmmAdoParams.GetItems }

//: TmmAdoParams.IndexOf 
(*
Ist der Parameter nicht vorhanden wird -1 zur�ckgegeben.
*)
function TmmAdoParams.IndexOf(aName: string): Integer;
var
  i: Integer;
begin
  // Initialisieren
  result := -1;
  
  // Alle Parameter nach dem Namen absuchen
  // Der Vergleich ist case insesitive
  i := 0;
  while (i < Count) and (result = -1) do begin
    if (AnsiCompareText(Items[i].Name,aName) = 0)then begin
      result := i;
    end;// if (AnsiCompareText(Items[i].Name,aName) = 0)then begin
    inc(i);
  end;// while (i < Count) and (result = -1) do begin
end;{ TmmAdoParams.IndexOf }

//: TmmAdoParams.ParamByName 
(*
Ist der Parameter nicht vorhanden, wird eine Exception geworfen.
*)
function TmmAdoParams.ParamByName(const aName: string): TmmAdoParameter;
var
  xIndex: Integer;
begin
  result := nil;
  xIndex := IndexOf(aName);
  if xIndex >= 0 then
    result := Items[xIndex];
  {-------------------------------------------------------------------------------}
  // Exception ausl�sen, wenn ein Parameter nicht gefunden wurde
  if result = nil then
    raise Exception.CreateFmt('Parameter ''%s'' not found',[aName]);
end;{ TmmAdoParams.ParamByName }

//: TmmAdoParams.SetItems 
(*
-
*)
procedure TmmAdoParams.SetItems(Index: Integer; Value: TmmAdoParameter);
begin
  FList[Index] := Value;
end;{ TmmAdoParams.SetItems }

{-**********************************************************************
************************************************************************
******************
******************  Class:    TmmAdoField
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TmmAdoField.Create 
(*
Der Name des Feldes und seine Position wird direkt 
dem Konstruktor �bergeben. So kann das Feld nachher
anhand seines Namens seine Position zur�ckgeben. 
Das Recordset wird gebraucht um die Felder zu finden.
*)
constructor TmmAdoField.Create(aName: string; aIndex: integer; aRecordset: _Recordset);
begin
  Create;
  
  FName      := aName;
  FIndex     := aIndex;
  mRecordset := aRecordset;
end;{ TmmAdoField.Create }

//: TmmAdoField.GetAsBoolean 
(*
Das Value wird direkt von ADO abgeholt ohne Umweg �ber einen 
Variant, da offenbar manchmal ein Problem mit dem Typ 
des Variants besteht.
*)
function TmmAdoField.GetAsBoolean: Boolean;
begin
  result := false;
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then begin
    if VarIsNull(mRecordset.Fields[Index].Value) then
      result := false
    else
      result := mRecordset.Fields[Index].Value;
  end;
end;{ TmmAdoField.GetAsBoolean }

//: TmmAdoField.GetAsDateTime 
(*
Das Value wird direkt von ADO abgeholt ohne Umweg �ber einen 
Variant, da offenbar manchmal ein Problem mit dem Typ 
des Variants besteht.
*)
function TmmAdoField.GetAsDateTime: TDateTime;
begin
  result := 0;
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then begin
    if VarIsNull(mRecordset.Fields[Index].Value) then
      result := 0
    else
      result := mRecordset.Fields[Index].Value;
  end;
end;{ TmmAdoField.GetAsDateTime }

//: TmmAdoField.GetAsFloat 
(*
Das Value wird direkt von ADO abgeholt ohne Umweg �ber einen 
Variant, da offenbar manchmal ein Problem mit dem Typ 
des Variants besteht.
*)
function TmmAdoField.GetAsFloat: Double;
begin
  result := 0;
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then begin
    if VarIsNull(mRecordset.Fields[Index].Value) then
      result := 0
    else
      result := mRecordset.Fields[Index].Value;
  end;
end;{ TmmAdoField.GetAsFloat }

//: TmmAdoField.GetAsInteger 
(*
Das Value wird direkt von ADO abgeholt ohne Umweg �ber einen 
Variant, da offenbar manchmal ein Problem mit dem Typ 
des Variants besteht.
*)
function TmmAdoField.GetAsInteger: Integer;
begin
  result := 0;
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then begin
    if VarIsNull(mRecordset.Fields[Index].Value) then
      result := 0
    else
      result := mRecordset.Fields[Index].Value;
  end;
end;{ TmmAdoField.GetAsInteger }

//: TmmAdoField.GetAsString 
(*
Das Value wird direkt von ADO abgeholt ohne Umweg �ber einen 
Variant, da offenbar manchmal ein Problem mit dem Typ 
des Variants besteht.
*)
function TmmAdoField.GetAsString: String;
begin
  result := '';
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then begin
    if VarIsNull(mRecordset.Fields[Index].Value) then
      result := ''
    else
      result := mRecordset.Fields[Index].Value;
  end;
end;{ TmmAdoField.GetAsString }

//: TmmAdoField.GetFieldNo 
(*
Diese Methode hat die gleiche Funktion wie das Property 'Index'.
*)
function TmmAdoField.GetFieldNo: Integer;
begin
  result := FIndex;
end;{ TmmAdoField.GetFieldNo }

//: TmmAdoField.GetValue 
(*
Jede Zugriffsmethode muss diesen Wert abfragen, damit 
sichergestellt ist, dass immer der aktuelle Wert abgefragt 
wird.
*)
function TmmAdoField.GetValue: Variant;
begin
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then
    result := mRecordset.Fields[Index].Value;
end;{ TmmAdoField.GetValue }

//: TmmAdoField.IsNull 
(*
Die Anfrage wird an ADO delegiert.
Wenn die Datenmenge nicht verf�gbar ist, wird eine 
Exception ausgel�st.
*)
function TmmAdoField.IsNull: Boolean;
begin
  if (assigned(mRecordset))and(mRecordset.State = adStateOpen) then
    result := ((VarType(mRecordset.Fields[Index].Value) and varTypeMask) = varNull)
  else
    raise Exception.Create('Operation not allowed on a closed recordset');
end;{ TmmAdoField.IsNull }

//: TmmAdoField.SetAsBoolean 
(*
-
*)
procedure TmmAdoField.SetAsBoolean(const NewValue: Boolean);
begin
  FAsBoolean := NewValue;
  FFieldType := ptBoolean;
end;{ TmmAdoField.SetAsBoolean }

//: TmmAdoField.SetAsDateTime 
(*
-
*)
procedure TmmAdoField.SetAsDateTime(NewValue: TDateTime);
begin
  FAsDateTime := NewValue;
  FFieldType := ptDateTime;
end;{ TmmAdoField.SetAsDateTime }

//: TmmAdoField.SetAsFloat 
(*
-
*)
procedure TmmAdoField.SetAsFloat(NewValue: Double);
begin
  FAsFloat := NewValue;
  FFieldType := ptFloat;
end;{ TmmAdoField.SetAsFloat }

//: TmmAdoField.SetAsInteger 
(*
-
*)
procedure TmmAdoField.SetAsInteger(NewValue: Integer);
begin
  FAsInteger := NewValue;
  FFieldType := ptInteger;
end;{ TmmAdoField.SetAsInteger }

//: TmmAdoField.SetAsString 
(*
-
*)
procedure TmmAdoField.SetAsString(const NewValue: String);
begin
  FAsString := NewValue;
  FFieldType := ptString;
end;{ TmmAdoField.SetAsString }

(*
Die Felder werden bei ADO angefragt (Nach der Ausf�hrung von SELECT)
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TmmAdoFields
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TmmAdoFields.FieldByName 
(*
Ist das Feld nicht vorhanden, wird eine Exception geworfen.
*)
function TmmAdoFields.FieldByName(const aName: string): TmmAdoField;
var
  xIndex: Integer;
begin
  result := nil;
  xIndex := IndexOf(aName);
  if xIndex >= 0 then
    result := Items[xIndex];
  {-------------------------------------------------------------------------------}
  // Exception ausl�sen, wenn ein Parameter nicht gefunden wurde
  if result = nil then
    raise Exception.CreateFmt('Field ''%s'' not found',[aName]);
end;{ TmmAdoFields.FieldByName }

//: TmmAdoFields.FieldNo 
(*
Die Methode gibt die Position des Feldes in der 
Auflistung zur�ck.
*)
function TmmAdoFields.FieldNo(aName: string): Integer;
begin
  result := IndexOf(aName);
  if result = -1 then
    raise Exception.CreateFMT('Field %s not exists',[aName]);
end;{ TmmAdoFields.FieldNo }

//: TmmAdoFields.GetItems 
(*
-
*)
function TmmAdoFields.GetItems(Index: Integer): TmmAdoField;
begin
  result := TmmAdoField(FList.Items[Index]);
end;{ TmmAdoFields.GetItems }

//: TmmAdoFields.IndexOf 
(*
Ist das Feld nicht vorhanden wird -1 zur�ckgegeben.
*)
function TmmAdoFields.IndexOf(aName: string): Integer;
var
  i: Integer;
begin
  // Initialisieren
  result := -1;
  {-------------------------------------------------------------------------------}
  // Alle Felder nach dem Namen absuchen
  // Der Vergleich ist case insesitive
  i := 0;
  while (i < Count) and (result = -1) do begin
    if (AnsiUpperCase(Items[i].Name) = AnsiUpperCase(aName)) then
      result := i;
  
    inc(i);
  end;// while (i < Count) and (result = -1) do begin
end;{ TmmAdoFields.IndexOf }

//: TmmAdoFields.SetItems 
(*
-
*)
procedure TmmAdoFields.SetItems(Index: Integer; Value: TmmAdoField);
begin
  FList[Index] := Value;
end;{ TmmAdoFields.SetItems }

(*
Die Klasse stellt auch ein _Recordset zu Verf�gung das f�r 
SELECT Abfragen eingesetzt wird. Das Recordset kann auch einem
TADOQuery (ADO Express) oder einem TmmADOQuery zugewiesen werden.
Die G�ltigkeitsdauer ist allerdings auf die Lebensdauer des 
TAdoDBAccess Objektes beschr�nkt
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TNativeAdoQuery
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TNativeAdoQuery.Create 
(*
-
*)
constructor TNativeAdoQuery.Create;
begin
  inherited Create;
  {-------------------------------------------------------------------------------}
  FFields := TmmAdoFields.Create;
  FParams := TmmAdoParams.Create;
  mUpdating := false;
  {-------------------------------------------------------------------------------}
  ParamCheck := true;
  {-------------------------------------------------------------------------------}
  FSQL := TDBAccessStringList.Create;
  // Wird aufgerufen wenn ein "Clear" abgesetzt wird
  // "Clear" wird auch beim setzen eines neuen Textes aufgerufen
  TDBAccessStringList(FSQL).OnClearList := OnClearSQL;
  {-------------------------------------------------------------------------------}
  // Ereignis wird gefeuert, wenn der Text der Stringliste �ndert
  TStringList(FSQL).OnChange := SQLChange;
  
  // Das Recordset- oder Command Objekt wird sofort erzeugt da sonst offenbar ein
  // Problem mit der Hostlink DLL besteht (Floor schliesst nicht mehr)
  //   --> Methode SQLChange
  FCommand := CoCommand.Create;
  FCommand.CommandType := adCmdText;
  FCommand.NamedParameters := false;
  FCommand.Prepared := false;
  
  FRecordset:= CoRecordset.Create;
  // Cursor definieren
  // Es wird nicht der Firehouse Cursor verwendet (Begr�ndung siehe TAdoDBAccess.Init)
  FRecordset.CursorLocation := adUseClient;
  FRecordset.CursorType     := adOpenKeyset;
end;{ TNativeAdoQuery.Create }

//: TNativeAdoQuery.Create 
(*
-
*)
constructor TNativeAdoQuery.Create(aOwner: TAdoDBAccess);
begin
  // merkt sich den "Owner"
  mDBAccess := aOwner;
  // Ruft den richtigen Konstruktor auf
  Create;
end;{ TNativeAdoQuery.Create }

//: TNativeAdoQuery.Destroy 
(*
-
*)
destructor TNativeAdoQuery.Destroy;
begin
  // Recordset vor dem freigeben schliessen
  Close;
  
  // FCommand ist ein COM Objekt --> Freigabe mit nil
  FCommand   := nil;
  {-------------------------------------------------------------------------------}
  FreeAndNil(FFields);
  FreeAndNil(FParams);
  FreeAndNil(FSQL);
  {-------------------------------------------------------------------------------}
  {-------------------------------------------------------------------------------}
  inherited Destroy;
end;{ TNativeAdoQuery.Destroy }

//: TNativeAdoQuery.Assign 
(*
-
*)
procedure TNativeAdoQuery.Assign(aSource: TPersistent);
var
  xSource: TNativeAdoQuery;
begin
  if (aSource is TNativeAdoQuery) then begin
    xSource     := TNativeAdoQuery(aSource);
    Connection := xSource.Connection;
    ParamCheck  := xSource.ParamCheck;
  end else begin
    inherited;
  end;// if (aSource is TNativeAdoQuery) then begin
end;{ TNativeAdoQuery.Assign }

//: TNativeAdoQuery.Clone 
(*
Bein Kopieren werden allf�llige Filter entfernt.
Das Recordset kann jetzt ohne eigne Connection verwendet werden.
*)
procedure TNativeAdoQuery.Clone(aSource: TNativeADOQuery);
begin
  if assigned(aSource) then begin
    Recordset := aSource.Recordset.Clone(LockTypeEnum(adLockUnspecified));
    CreateFields;
  end;// if assigned(aSource) then begin
end;{ TNativeAdoQuery.Clone }

//: TNativeAdoQuery.Close 
(*
Das Recordset kann nur geschlossen werden, wenn die Datenmenge
ge�ffnet ist.
*)
procedure TNativeAdoQuery.Close;
var
  xStart: Cardinal;
  
  const
    cCompleteTimeout = 5000;
  
begin
  if FRecordset <> nil then begin
    case FRecordset.State of
      adStateOpen :
        FRecordset.Close;
  
      adStateConnecting,
      adStateExecuting,
      adStateFetching: begin
        xStart := GetTickCount;
        (* Warten bis die Operation abgeschlossen ist.
           Das Query kann noch am ausf�hren einer Abfrage sein, wenn der
           Destruktor von einem anderen Thread aufgerufen wird.
           In diesem Fall muss die Abfrage zuerst abgeschlossen werden. *)
        while (FRecordset.State <> adStateOpen)
          and ((GetTickCount - xStart) < cCompleteTimeout) do
            sleep(1);
        FRecordset.Close;
      end;// adStateConnecting,adStateExecuting,adStateFetching: begin
    end;// case FRecordset.State of
  end;// if FRecordset <> nil then begin
  {-------------------------------------------------------------------------------}
  // Alle Felder freigeben
  FFields.Clear;
end;{ TNativeAdoQuery.Close }

//: TNativeAdoQuery.CodeSiteSendError 
(*
Das Connection Objekt enth�lt eine Liste mit Errormeldungen.
Es k�nnen in einem Aufruf auch mehrere Fehlermeldungen gemeinsam 
auftereten.
*)
function TNativeAdoQuery.CodeSiteSendError(aErrorMsg:string): String;
var
  i: Integer;
  xMsg: String;
begin
  // Urspr�ngliche Fehlermeldung
  xMsg := aErrorMsg + crlf+ crlf;
  {-------------------------------------------------------------------------------}
  // Fehlermeldungen des Providers auflisten
  with FConnection.Errors do begin
    for i := 0 to Count - 1 do begin
      if i = 0 then
        xMsg := xMsg + '(';
      xMsg := xMsg + '#' + IntToStr(Item[i].Number) + crlf;
      xMsg := xMsg + '    ' + 'Error: ' + Item[i].Description + crlf;
      xMsg := xMsg + '    ' + 'Source: ' + Item[i].Source + crlf;
      xMsg := xMsg + '    ' + 'SQLState: ' + Item[i].SQLState + crlf;
      xMsg := xMsg + '    ' + 'NativeError: ' + IntToStr(Item[i].NativeError);
      if i = Count - 1 then
        xMsg := xMsg + ')'
      else
        xMsg := xMsg + crlf + crlf;
    end;// for i := 0 to Count - 1 do begin
  end;//with FConnection.Errors do begin
  {-------------------------------------------------------------------------------}
  // Zusammngestellte Fehlermeldung senden
  result := xMsg;
  CodeSite.SendError(result);
end;{ TNativeAdoQuery.CodeSiteSendError }

//: TNativeAdoQuery.Commit 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TNativeAdoQuery.Commit;
begin
  if assigned(mDBAccess) then
    mDBAccess.Commit
  else
    raise Exception.Create('TNativeAdoQuery.Commit: No AdoDBAccess Object');
end;{ TNativeAdoQuery.Commit }

//: TNativeAdoQuery.CreateFields 
(*
Die Liste muss bei jedem Open neu erzeugt werden. Die Liste 
wird mit dem ADO Objekt Fileds syncronisiert.
*)
procedure TNativeAdoQuery.CreateFields;
var
  i: Integer;
begin
  // Alle Felder freigeben
  FFields.Clear;
  {-------------------------------------------------------------------------------}
  // Alle Felder abfragen und eine eigene Sammlung erzeugen
  if (assigned(FRecordset))and(FRecordset.State = adStateOpen) then begin
    for i := 0 to FRecordset.Fields.Count - 1 do begin
      FFields.Add(TmmAdoField.Create(FRecordset.Fields[i].Name,i,FRecordset));
    end;// for i := 0 to FRecordset.RecordCount - 1 do begin
  end;// if (assigned(FRecordset))and(...
end;{ TNativeAdoQuery.CreateFields }

//: TNativeAdoQuery.ExecSQL 
(*
Wenn die Datenmenge nicht ge�ffnet werden kann, wird ein Fehler zur�ckgegeben.
*)
function TNativeAdoQuery.ExecSQL: Integer;
var
  xRows: OLEVariant;
begin
  result := 0;
  {-------------------------------------------------------------------------------}
  try
    if Assigned(FCommand) then begin
      // Recordset schliessen, wenn es noch offen ist.
      // Es gibt keinen Fehler, wenn kein Recordset zugewiesen ist.
      Close;
  
      SetParams;
      FCommand.Execute(xRows,EmptyParam,adExecuteNoRecords);
      result := xRows;
    end;// if Assigned(FCommand) then begin
  except
    on e: EOleException do begin
      raise Exception.Create(CodeSiteSendError('ADO ExecSQL Error: '+ e.Message)+SendDebugQuery);
    end;// on e: EOleException do begin
  end;// try except
end;{ TNativeAdoQuery.ExecSQL }

//: TNativeAdoQuery.FieldByName 
(*
Ist das Feld nicht vorhanden, wird eine Exception geworfen.
*)
function TNativeAdoQuery.FieldByName(const aName: string): TmmAdoField;
begin
  result := Fields.FieldByName(aName);
  {-------------------------------------------------------------------------------}
  // Exception ausl�sen, wenn ein Parameter nicht gefunden wurde
  if result = nil then
    raise Exception.CreateFmt('Field ''%s'' not found',[aName]);
end;{ TNativeAdoQuery.FieldByName }

//: TNativeAdoQuery.Filter 
(*
Wird ein leerer String �bergeben, dann wird der 
aktuelle Filter gel�scht.
Nachdem der Filter angewendet wurde, ist der erste Datensatz
der gefilterten Datenmenge der aktuelle Datensatz.
Der Filterstring ist eine g�ltige WHERE Klausel mit einigen Einschr�nkungen:
- Wenn ein Feldname Leerzeichen enth�lt dann muss er in eckige Klammmern eingeschlossen werden.
- Zugelassene Operatoren sind <, >, <=, >=, <>, =, oder LIKE.
- Der Wert muss formatiert sein:
  - Strings sind in Single Quotes eingeschlossen (zB. 'String Wert')
    Wird in einem String ein Single Quote verwendet m�ssen zwei Zeichen eingef�gt werden. 
    Das bedeutet f�r Stringdefinition in Delphi folgendes Konstrukt f�r den String O'Malley 'O''''Malley'.
  - Datumsangaben sind in Rauten eingeschlossen (zB. #8/30/2003#)
  - Als Dezimalpunkt wird der Punkt verwendet.
  - Mit dem Operator LIKE sind nur '*' und '%' erlaubt und m�ssen am Anfang oder am Ende des Strings stehen.
  - AND und OR werden der Reihe nach abgearbeitet. Sie k�nnen in Gruppen geklammert werden.
    Geklammerte Ausr�cke k�nnen nicht gemischt werden. Folgender Ausdruck ist nicht erlaubt:
      (LastName = 'Smith' OR LastName = 'Jones') AND FirstName = 'John'
    Dieser Ausdruck muss wie folgt umgesetzt werden:
      (LastName = 'Smith' AND FirstName = 'John') OR (LastName = 'Jones' AND FirstName = 'John')
*)
procedure TNativeAdoQuery.Filter(aFilterstring: string);
begin
  if assigned (FRecordset) then
    Recordset.Filter := aFilterstring;
end;{ TNativeAdoQuery.Filter }

//: TNativeAdoQuery.FindField 
(*
Wenn das gew�nschte Feld nicht in der Auflistung vorhanden
ist, dann wird nil zur�ckgegeben.
*)
function TNativeAdoQuery.FindField(aName: string): TmmAdoField;
begin
  result := FieldByName(aName);
  if result = nil then
    raise Exception.CreateFMT('Field %s doesn''t extists',[aName]);
end;{ TNativeAdoQuery.FindField }

//: TNativeAdoQuery.FindFirst 
(*
-
*)
function TNativeAdoQuery.FindFirst: Boolean;
begin
  result := false;
  
  try
    if assigned(FRecordset) then begin
      if not(EOF and BOF) then begin
        FRecordset.MoveFirst;
        result := true;
      end;// if not(EOF and BOF) then begin
    end;// if assigned(FRecordset) then begin
  except
    // Bei einem Fehler einfach false zur�ckgeben
    on e:EOleException do;
  end;// try finally
end;{ TNativeAdoQuery.FindFirst }

//: TNativeAdoQuery.FindLast 
(*
-
*)
function TNativeAdoQuery.FindLast: Boolean;
begin
  result := false;
  
  try
    if assigned(FRecordset) then begin
      if not(EOF and BOF) then begin
        FRecordset.MoveLast;
        result := true;
      end;// if not(EOF and BOF) then begin
    end;// if assigned(FRecordset) then begin
  except
    on e:EOleException do;
  //     raise Exception.Create(e.Message);
  end;// try finally
end;{ TNativeAdoQuery.FindLast }

//: TNativeAdoQuery.FindNext 
(*
-
*)
function TNativeAdoQuery.FindNext: Boolean;
begin
  result := false;
  
  try
    if assigned(FRecordset) then begin
      FRecordset.MoveNext;
      result := true;
    end;// if assigned(FRecordset) then begin
  except
    on e:EOleException do;
  //     raise Exception.Create(e.Message);
  end;// try finally
end;{ TNativeAdoQuery.FindNext }

//: TNativeAdoQuery.FindPrior 
(*
-
*)
function TNativeAdoQuery.FindPrior: Boolean;
begin
  result := false;
  
  try
    if assigned(FRecordset) then begin
      FRecordset.MovePrevious;
      result := true;
    end;// if assigned(FRecordset) then begin
  except
    on e:EOleException do;
  //     raise Exception.Create(e.Message);
  end;// try finally
end;{ TNativeAdoQuery.FindPrior }

//: TNativeAdoQuery.First 
(*
-
*)
procedure TNativeAdoQuery.First;
begin
  if assigned(FRecordset) then begin
    if not(EOF and BOF) then
      FRecordset.MoveFirst;
  end;// if assigned(FRecordset) then begin
end;{ TNativeAdoQuery.First }

//: TNativeAdoQuery.GetActive 
(*
-
*)
function TNativeAdoQuery.GetActive: Boolean;
begin
  result := false;
  if assigned(FRecordset) then
    result := (FRecordset.State = adStateOpen);
end;{ TNativeAdoQuery.GetActive }

//: TNativeAdoQuery.GetAsXML 
(*
Die Umwandlung wird mittels einem ADO Stream erledigt
*)
function TNativeAdoQuery.GetAsXML: String;
var
  xStrm: Stream;
begin
  result := '';
  {-------------------------------------------------------------------------------}
  if Active then begin
    // Stream erzeugen
    xStrm := CoStream.Create;
    // Recordset in den Stream schreiben
    FRecordset.Save(xStrm, adPersistXML);
    // Stream wieder auslesen
    result := xStrm.ReadText(xStrm.Size);
  
    // Stream schliessen ...
    xStrm.Close;
    // ... und wieder freigeben
    xStrm := nil;
  end;// if Active then begin
end;{ TNativeAdoQuery.GetAsXML }

//: TNativeAdoQuery.GetBlobFieldData 
(*
Die Daten werden im ByteArray aBuffer abgelegt.
*)
function TNativeAdoQuery.GetBlobFieldData(aFieldNr: integer; var aBuffer: TBlobByteData): Integer;
begin
  if not(assigned(FRecordset)) then
    raise Exception.Create('Recordset not availible');
  {-------------------------------------------------------------------------------}
  // Gr�sse der Daten abfragen
  result := FRecordset.Fields[aFieldNr].ActualSize;
  // Buffer vorbereiten
  SetLength(aBuffer,result);
  
  // Buffer "f�llen"
  aBuffer := TBlobByteData(FRecordset.Fields[aFieldNr].Value);
end;{ TNativeAdoQuery.GetBlobFieldData }

//: TNativeAdoQuery.GetBlobFieldData 
(*
Die Daten werden im ByteArray aBuffer abgelegt.
*)
function TNativeAdoQuery.GetBlobFieldData(aFieldName: string; var aStream: TMemoryStream): Integer;
var
  xFieldNr: Integer;
  xData: Variant;
  xBuffer: PByte;
begin
  if not(assigned(FRecordset)) then
    raise Exception.Create('Recordset not availible');
  {-------------------------------------------------------------------------------}
  xFieldNr := FFields.IndexOf(aFieldName);
  if xFieldNr = -1 then
     raise Exception.CreateFMT('Field doesn''t exists',[aFieldName]);
  
  // Gr�sse der Daten abfragen
  result := FRecordset.Fields[xFieldNr].ActualSize;
  
  if aStream = nil then
     raise Exception.Create('Stream is nil');
  
  // Eventuell abfangen ob alle Daten geschrieben sind
  xData := FRecordset.Fields[xFieldNr].Value;
  
  if xData <> NULL then begin
    xBuffer := VarArrayLock(xData);
    try
      aStream.Write(xBuffer^, result);
    finally
      VarArrayUnLock(xData);
    end;
  end;// if xData <> NULL then begin
end;{ TNativeAdoQuery.GetBlobFieldData }

//: TNativeAdoQuery.GetBOF 
(*
Die Pr�fung wird an ADO delegiert.
*)
function TNativeAdoQuery.GetBOF: Boolean;
begin
  if (assigned(FRecordset))and(FRecordset.State = adStateOpen) then
    result := FRecordset.BOF
  else
    raise Exception.Create('Operation not allowed on closed a recordset');
end;{ TNativeAdoQuery.GetBOF }

//: TNativeAdoQuery.GetCommand 
(*
-
*)
function TNativeAdoQuery.GetCommand: _Command;
begin
  Result := FCommand;
end;{ TNativeAdoQuery.GetCommand }

//: TNativeAdoQuery.GetEOF 
(*
Die Pr�fung wird an ADO delegiert.
*)
function TNativeAdoQuery.GetEOF: Boolean;
begin
  if (assigned(FRecordset))and(FRecordset.State = adStateOpen) then
    result := FRecordset.EOF
  else
    raise Exception.Create('Operation not allowed on closed a recordset');
end;{ TNativeAdoQuery.GetEOF }

//: TNativeAdoQuery.GetParamCheck 
(*
-
*)
function TNativeAdoQuery.GetParamCheck: Boolean;
begin
  result := FParams.ParamCheck;
end;{ TNativeAdoQuery.GetParamCheck }

//: TNativeAdoQuery.GetRecordset 
(*
Erlaubt den Zugriff auf das native Recordset Objekt. 
Das REcordset Objekt ist nur g�ltig wenn ein ExecSQL eine 
Ergebnismenge zur�ckliefert (SELECT Query). Ansonsten l�st 
der Zugriff eine Exception aus.
*)
function TNativeAdoQuery.GetRecordset: _Recordset;
begin
  result := FRecordset;
end;{ TNativeAdoQuery.GetRecordset }

//: TNativeAdoQuery.GetSQL 
(*
Da die Stringliste mit SQL.Text auch Zeilenumbr�che mitliefert, 
m�ssen die crlf's ausgefiltert werden.
*)
function TNativeAdoQuery.GetSQL: String;
begin
  result := StringReplace(SQL.Text,#13#10,' ',[rfReplaceAll]);
end;{ TNativeAdoQuery.GetSQL }

//: TNativeAdoQuery.InsertSQL 
(*
Die Funktion vergibt f�r die �bergebene Spalte einen eindeutigen
Index und f�llt dann den entsprechenden Parameter ab.
Danach wird das Query mit ExecSQL ausgef�hrt.

Diese Version der Funktion ist die langsamste. Es werden f�r jeden Zugriff alle
Datens�tzte in der Zieltabelle gelockt. Dieser Vorgang dauert offenbar l�nger 
desto mehr Datens�tze in der Zieltabelle vorhanden sind.
*)
function TNativeAdoQuery.InsertSQL(aTable, aIDField : String; aMaxIDVal, aMinIDVal : integer): Integer;
var
  xQuery: TNativeAdoQuery;
  xTansActive: Boolean;
  
  function getUniqueID : integer;
   function SearchID ( aCheck, aMax, aMin : integer ):integer;
   var
     xCount1    : integer;
     xCount2    : integer;
     xNewBorder : integer;
   begin
     // Check if aCheck id is already in use
     xQuery.SQL.Text := Format ( 'select %s from %s where %s = %d', [aIDField,aTable,aIDField,aCheck] );
     xQuery.Open;
     if xQuery.EOF then begin // a whole is found
       Result := aCheck;
       xQuery.Close;
     end else begin
       // maybe the table is full
       if aMax = aMin then raise Exception.CreateFmt ( 'No space available in table ' + aTable , [] );
       xNewBorder := Trunc ( ( aMax + aMin ) / 2.0 );
       // Count the top half of records
       xQuery.close;
       xQuery.SQL.Text := Format ( 'select count(*) Num from %s where %s <= %d and %s >= %d',
                                   [aTable,aIDField,aMax,aIDField,xNewBorder]);
       xQuery.Open;
       xCount1 := xQuery.FieldByName ( 'Num' ).AsInteger;
       xQuery.Close;
       // Count the bottom half of records
       xQuery.SQL.Text := Format ( 'select count(*) Num from %s where %s <= %d and %s >= %d',
                                   [aTable,aIDField,xNewBorder,aIDField, aMin] );
       xQuery.Open;
       xCount2 := xQuery.FieldByName ( 'Num' ).AsInteger;
       xQuery.Close;
       if xCount1 = xCount2 then  // the border will be moved up because the trunc function
         Result := SearchID ( xNewBorder, aMax, xNewBorder )
       else
         if xCount1 < xCount2 then
           Result := SearchID ( xNewBorder, aMax, xNewBorder )
         else
           Result := SearchID ( xNewBorder, xNewBorder, aMin );
         end;
   end;
  
  begin
   // get the max value of the id field
   xQuery.close;
   xQuery.SQL.Text := Format ( 'select max ( %s ) %s from %s where %s between (%d) and (%d)',
    [aIDField, aIDField, aTable, aIDField, aMinIDVal, aMaxIDVal] );
   try
     xQuery.open;
     Result := xQuery.FieldByName (aIDField).AsInteger;
     xQuery.close;
   except
     raise;
   end;
   if Result = 0 then begin // no record in table
     Result := aMinIDVal;
     Exit;
   end;
   if Result < aMaxIDVal then begin // the max value is not reached
     inc ( Result );
     Exit;
   end;
   if Result > aMaxIDVal then
     raise Exception.Create('Overflow of ID : ' + aIDField);
  
   // search an available value in the field id of the spezified table
   Result := SearchID ( aMinIDVal, aMaxIDVal, aMinIDVal );
  end;
  
begin
  // cretate a query and assign it with the self object
  xQuery := TNativeAdoQuery.Create;
  try
    xQuery.Assign (self);
  except
    xQuery.Free;
    raise Exception.Create('Assign of TNativeADOQuery failed');
  end;// try except
  {-------------------------------------------------------------------------------}
  try
    // start a transaction for the following steps
    if assigned(FConnection) then begin
      try
        FConnection.BeginTrans;
        xTansActive := true;
      except
        xTansActive := false;
      end;// try except
    end else begin
      raise Exception.Create('no Connection Object present');
    end;// if assigned(FConnection) then begin
  except
    xQuery.Free;
    raise;
  end;// try except
  {-------------------------------------------------------------------------------}
  try
    // lock the table with an update
    xQuery.Close;
    xQuery.SQL.Text := Format ('update %s with ( tablockx holdlock ) set %s = 1 where %s = 1', [ aTable, aIDField, aIDField ]);
    xQuery.ExecSQL;
    // get a unique ID from the spezified table
    Result := getUniqueID;
    // completet the query parameter with the ID and insert the data
    Params.ParamByName (aIDField).AsInteger := Result;
    ExecSQL;
    // commit the transaction
    if assigned(FConnection) then begin
      if xTansActive then begin
        FConnection.CommitTrans;
        xTansActive := false;
      end;// if xTansActive then begin
    end else begin
      raise Exception.Create('no Connection Object present');
    end;// if assigned(FConnection) then begin
    xQuery.Free;
  except
    on e:Exception do begin
      try
        if assigned(FConnection) then begin
          if xTansActive then begin
            FConnection.RollbackTrans;
          end;// if xTansActive then begin
        end else begin
          raise Exception.Create('no Connection Object present');
        end;// if assigned(FConnection) then begin
        xQuery.Free;
      except end;
      raise Exception.Create ( e.Message );
    end;
  end;// try except
end;{ TNativeAdoQuery.InsertSQL }

//: TNativeAdoQuery.InTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
function TNativeAdoQuery.InTransaction: Boolean;
begin
  if assigned(mDBAccess) then
    result := mDBAccess.InTransaction
  else
    raise Exception.Create('TNativeAdoQuery.InTransaction: No AdoDBAccess Object');
end;{ TNativeAdoQuery.InTransaction }

//: TNativeAdoQuery.Last 
(*
-
*)
procedure TNativeAdoQuery.Last;
begin
  if assigned(FRecordset) then begin
    if not(EOF and BOF) then
      FRecordset.MoveLast;
  end;// if assigned(FRecordset) then begin
end;{ TNativeAdoQuery.Last }

//: TNativeAdoQuery.Next 
(*
-
*)
procedure TNativeAdoQuery.Next;
begin
  if assigned(FRecordset) then
    FRecordset.MoveNext;
end;{ TNativeAdoQuery.Next }

//: TNativeAdoQuery.OnClearSQL 
(*
Wenn der Eigenschaft SQL ein neuer Text zugewiesen wird, 
dann wird zuerst der alte Text mit Clear gel�scht. In diesem
Fall muss auch die Parameterliste gel�scht werden.
*)
procedure TNativeAdoQuery.OnClearSQL(Sender: TObject);
begin
  // Wird aufgerufen, wenn der Text von "SQL: TStrings" gel�scht wird
  // Dies geschieht auch, wenn der Eigenschaft "Text" ein neuer String
  // zugewiesen wird
  if not(mUpdating)then
    FParams.Clear;
end;{ TNativeAdoQuery.OnClearSQL }

//: TNativeAdoQuery.Open 
(*
-
*)
function TNativeAdoQuery.Open: Integer;
var
  xRows: OLEVariant;
begin
  result := 0;
  {-------------------------------------------------------------------------------}
  try
    if (assigned(FRecordset))and(assigned(FCommand)) then begin
      SetParams;
      FRecordset.CacheSize := 1;
      FRecordset := FCommand.Execute(xRows,EmptyParam,integer(adOptionUnspecified));
      // Felder erzeugen
      CreateFields;
      // Anzahl records
      result := xRows;
    end;// if (assigned(FRecordset))and(assigned(FCommand)) then begin
  except
    on e: EOleException do begin
      raise Exception.Create(CodeSiteSendError('ADO Open Error: '+ e.Message)+SendDebugQuery);
    end;// on e: EOleException do begin
  end;// try except
end;{ TNativeAdoQuery.Open }

//: TNativeAdoQuery.ParamByName 
(*
Ist der Parameter nicht vorhanden, wird eine Exception geworfen.
*)
function TNativeAdoQuery.ParamByName(const aName: string): TmmAdoParameter;
begin
  result := Params.ParamByName(aName);
  {-------------------------------------------------------------------------------}
  // Exception ausl�sen, wenn ein Parameter nicht gefunden wurde
  if result = nil then
    raise Exception.CreateFmt('Parameter ''%s'' not found',[aName]);
end;{ TNativeAdoQuery.ParamByName }

//: TNativeAdoQuery.Prior 
(*
-
*)
procedure TNativeAdoQuery.Prior;
begin
  if assigned(FRecordset) then
    FRecordset.MovePrevious;
end;{ TNativeAdoQuery.Prior }

//: TNativeAdoQuery.Rollback 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TNativeAdoQuery.Rollback;
begin
  if assigned(mDBAccess) then
    mDBAccess.Rollback
  else
    raise Exception.Create('TNativeAdoQuery.Rollback: No AdoDBAccess Object');
end;{ TNativeAdoQuery.Rollback }

//: TNativeAdoQuery.SendDebugQuery 
(*
Es werden alle Parameter und das Query selbst gesendet.
*)
function TNativeAdoQuery.SendDebugQuery: String;
var
  i: Integer;
begin
  //Query und zugeh�rige Parameter zusammenstellen und zur�ckgeben
  Result := Format('CommandText %s',[GetSQL]);
  for i := 0 to FParams.Count - 1 do begin
    case FParams[i].ParamType of
      ptInteger  : Result := Result+cCRLF+Format('Integer:%d, %s:%d; ',[i,Params[i].Name,Params[i].AsInteger]);
      ptFloat    : Result := Result+cCRLF+Format('Float:%d, %s:%f; ',[i,Params[i].Name,Params[i].AsFloat]);
      ptDateTime : Result := Result+cCRLF+Format('DateTime:%d, %s:%s; ',[i,Params[i].Name,DateToStr(Params[i].AsDateTime)]);
      ptString   : Result := Result+cCRLF+Format('String:%d, %s:%s; ',[i,Params[i].Name,Params[i].AsString]);
      ptBoolean  : Result := Result+cCRLF+Format('Boolean:%d, %s:%d; ',[i,Params[i].Name,Integer(Params[i].AsBoolean)]);
      ptBlob     : Result := Result+cCRLF+Format('Blob:%d, %s:%s; ',[i,Params[i].Name,Params[i].AsString]);
      else
        Result := Result+cCRLF+Format('ParamType %d (%s) not recognized',[ord(Params[i].ParamType), Params[i].Name]);
        Exception.Create(Format('ParamType %d (%s) not recognized',[ord(FParams[i].ParamType), Params[i].Name]));
    end;// case FParams[i].ParamType of
  end;// for i := 0 to FParams.Count - 1 do begin
  
  if CodeSite.Enabled then begin
    CodeSite.AddSeparator;
    CodeSite.SendStringEx(csmError,'CommandText',GetSQL);
    for i := 0 to FParams.Count - 1 do begin
      case FParams[i].ParamType of
        ptInteger  : CodeSite.SendIntegerEx(csmError,Format('%d: %s',[i,FParams[i].Name]),FParams[i].AsInteger);
        ptFloat    : CodeSite.SendFloatEx(csmError,Format('%d: %s',[i,FParams[i].Name]),FParams[i].AsFloat);
        ptDateTime : CodeSite.SendDateTimeEx(csmError,Format('%d: %s',[i,FParams[i].Name]),FParams[i].AsDateTime);
        ptString   : CodeSite.SendStringEx(csmError,Format('%d: %s',[i,FParams[i].Name]),FParams[i].AsString);
        ptBoolean  : CodeSite.SendBooleanEx(csmError,Format('%d: %s',[i,FParams[i].Name]),FParams[i].AsBoolean);
        ptBlob     : CodeSite.SendMsgEx(csmError,Format('%d: %s (Blob)',[i,FParams[i].Name]));
        else
          Exception.Create(Format('ParamType %d (%s) not recognized',[ord(FParams[i].ParamType), Params[i].Name]));
      end;// case FParams[i].ParamType of
    end;// for i := 0 to FParams.Count - 1 do begin
    CodeSite.AddSeparator;
  end;// if CodeSite.Enabled then begin
end;{ TNativeAdoQuery.SendDebugQuery }

//: TNativeAdoQuery.SendRecordsetToCodeSite 
(*
Count = 0 entspricht allen Datens�tzen
*)
procedure TNativeAdoQuery.SendRecordsetToCodeSite(aName: string; aCount: integer);
var
  xIndex: Integer;
  i: Integer;
  xRecord: String;
  xRecordset: String;
begin
  // Initialisieren
  xRecordset := '';
  
  if aCount = 0 then
    aCount := MAXINT;
  {-------------------------------------------------------------------------------}
  // Titelzeile im '.csv' Format (Excel)
  for i := 0 to Fields.Count - 1 do
    xRecordset := xRecordset + ';' + Fields[i].Name;
  if xRecordset > '' then
    system.delete(xRecordset, 1, 1);
  {-------------------------------------------------------------------------------}
  // Gew�nschte Anzahl Datens�tze als '.csv' Format schreiben (Excel)
  FindFirst;
  xIndex := 0;
  
  while not(EOF) and (xIndex < aCount) do begin
    xRecord := '';
    for i := 0 to Fields.Count - 1 do begin
      xRecord := xRecord + ';' + Fields[i].AsString;
    end;// for i := 0 to Fields.Count - 1 do begin
  
    // Bisherige Daten mit dem neuen Datensatz zusammenf�gen
    if xRecord > '' then begin
      system.delete(xRecord, 1, 1);
      xRecordset := xRecordset + #13#10 + xRecord;
    end;// if xRecord > '' then begin
  
    // N�chster Datensatz
    next;
    inc(xIndex);
  end;// while not(EOF) and (xIndex < aCount) do begin
  {-------------------------------------------------------------------------------}
  // Recordset senden
  CodeSite.SendString(aName, xRecordset);
end;{ TNativeAdoQuery.SendRecordsetToCodeSite }

//: TNativeAdoQuery.SetActive 
(*
Der Aufruf von Open und Close, entspricht dem Setzen der Eigenschaft Active.
*)
procedure TNativeAdoQuery.SetActive(Value: Boolean);
begin
  if assigned(FRecordset) then begin
    if Value then
      Open
    else
      Close;
  end;// if assigned(FRecordset) then begin
end;{ TNativeAdoQuery.SetActive }

//: TNativeAdoQuery.SetAsXML 
(*
Die Umwandlung wird mittels einem ADO Stream erledigt
*)
procedure TNativeAdoQuery.SetAsXML(const Value: String);
var
  xStrm: Stream;
begin
  // zuerst einmal das evt. offene Recordset schliessen
  close;
  {-------------------------------------------------------------------------------}
  // Stream erzeugen
  xStrm := COStream.Create;
  xStrm.Open(EmptyParam, adModeUnknown, StreamOpenOptionsEnum(adOpenUnspecified), '', '');
  // XML String in den Stream schreiben
  xStrm.WriteText(Value, stWriteChar);
  
  xStrm.Position := 0;
  {-------------------------------------------------------------------------------}
  try
    if assigned(FRecordset) then begin
      FRecordset.CacheSize := 1;
      // Daten aus dem Stream laden
      FRecordset.Open(xStrm, EmptyParam, CursorTypeEnum(adOpenUnspecified), LockTypeEnum(adLockUnspecified), 0);
      xStrm.Close;
      xStrm := nil;
      // Felder erzeugen
      CreateFields;
    end;// if assigned(FRecordset) then begin
  except
    on e: EOleException do begin
      raise Exception.Create(CodeSiteSendError('ADO Open Error: '+ e.Message));
    end;// on e: EOleException do begin
  end;// try except
end;{ TNativeAdoQuery.SetAsXML }

//: TNativeAdoQuery.SetConnection 
(*
Setzt eine neue Connection. 
*)
procedure TNativeAdoQuery.SetConnection(Value: _Connection);
begin
  FConnection := Value;
  if Assigned(FCommand) then
    FCommand.Set_ActiveConnection(FConnection);
end;{ TNativeAdoQuery.SetConnection }

//: TNativeAdoQuery.SetParamCheck 
(*
-
*)
procedure TNativeAdoQuery.SetParamCheck(Value: Boolean);
begin
  FParams.ParamCheck := Value;
end;{ TNativeAdoQuery.SetParamCheck }

//: TNativeAdoQuery.SetParams 
(*
Die Parameter kommen aus der Liste TParams. Die Parameter
werden aus den TAdoParameter Objekten generiert.
*)
procedure TNativeAdoQuery.SetParams;
var
  i: Integer;
  xCount: Integer;
  xParamNr: Integer;
  xActualParam: Integer;
  xStringLength: Integer;
  
  procedure AppendParam(aIndex: integer; aType: DataTypeEnum; Size: integer = 0);
  var
    xParameter: _Parameter;
  begin
    xParameter := FCommand.CreateParameter(PChar(FParams[aIndex].Name),
                                           aType,
                                           adParamInputOutput,
                                           Size,
                                           NULL);
    FCommand.Parameters.Append(xParameter);
  end;// procedure AppendParam(aIndex: integer; aType: DataTypeEnum; Size: integer = 0);
  
  function GetADOParamCount: Integer;
  begin
    try
      result := FCommand.Parameters.Count;
    except
      // Wenn ADO keine Parameter gefunden hat, wirft obige Anweisung eine Exception
      result := 0;
    end;// try except
  end;// function GetADOParamCount: Integer;
  
begin
  if FParams.Count > 0 then begin
    // Anzahl der Parameter abfragen, die ADO gefunden hat
    xCount := GetADOParamCount;
  
    // Wenn ADO und TAdoDBAccess unterschiedliche Anzahl Parameter gefunden haben
    if xCount <> FParams.Count then begin
      // Dummy Parameter erzeugen, sonst "erinnert" sich ADO
      (* Bsp. :
        select
          datediff( minute, :DataEvent, (
             select min ( c_dataevent_start)
             from v_DataEvents where c_dataevent_start > :DataEvent2 ) )
        Len';
        Wird dieses Query ein zweites mal zugewiesen (zwischendurch war ein anderes
        Query zugewiesen), dann "erinnert" sich ADO, nachdem der erste Parameter von Hand erzeugt wurde,
        pl�tzlich daran, dass es das letzte mal bereits zwei Parameter hatte.
        Der Ablauf ist wie folgt:
          - Das Query wird zugewiesen --> ADO findet keine Parameter
          - Die beiden Parameter werden manuell erzeugt
          - Ein anderes Query wird zugewiesen und ausgef�hrt
          - Das select datediff(...) Query wird erneut zugewiesen --> ADO findet keine Parameter
          - FCommand.Parameters.Count wirft eine Exception
          - Nachdem der erste Parameter erzeugt, und zur Liste hinzugef�gt wurde,
            ist FCommand.Parameters.Count = 3
      *)
      AppendParam(0, adInteger, SizeOf(Integer));
      // Zuerst bei ADO alle Parameter l�schen
      while FCommand.Parameters.Count <> 0 do
        FCommand.Parameters.Delete(0);
  
      xParamNr := 0;
      try
        // Dann alle Parameter neu erzeugen
        for i := 0 to FParams.Count - 1 do begin
          xParamNr := i;
          case FParams[i].ParamType of
            ptInteger  : begin
              AppendParam(i, adInteger, SizeOf(Integer));
            end;// ptInteger  : begin
            ptFloat    : begin
              AppendParam(i, adDouble, SizeOf(extended));
            end;// ptFloat    : begin
            ptDateTime : begin
              AppendParam(i, adDate, 0);
            end;// ptDateTime : begin
            ptString   : begin
              xStringLength := Length(FParams[i].AsString);
              (* Wenn ein String Parameter hinzugef�gt wird, muss die L�nge des
                 Parameters offenbar mindesten 1 sein.
                 Ist dies nicht der Fall kann ADO den Parameter nicht hinzuf�gen.
                 *)
              if xStringLength = 0 then
                 xStringLength := xStringLength + 1;
              AppendParam(i, adChar, xStringLength);
            end;// ptString   : begin
            ptBoolean  : begin
              AppendParam(i, adBoolean, SizeOf(Boolean));
            end;// ptBoolean  : begin
            ptBlob     : begin
              AppendParam(i, adLongVarBinary, FParams[i].ADOStream.Size);
              (* Eigenschaften des Parameters im Nachhinein bearbeiten.
                 Dies ist vorl�ufig nur f�r BLOB Felder notwendig. Der wichtige Parameter
                 ist die Eigenscahft Direction. Offenbar k�nnen BLOB Felder nur mit
                 Input Parameter verwendet werden. Wird die Richtung InOut gew�hlt,
                 dann "schiesst" der SQL Server die Connection ab, ohne dass ADO
                 etwas davon merkt. Die Folge ist die Fehlermeldung des SQL Servers:
                 "Error: 17805, Severity: 20, State: 3. Invalid buffer received from client".
                 *)
              FCommand.Parameters[i].Direction :=    1;
            end;// ptBlob     : begin
          end;// case FParams[i].ParamType of
        end;// for i := 0 to FParams.Count - 1 do begin
      except
        on e: EOleException do begin
          CodeSiteSendError(Format('ADO Error in Param %d (Name = %s - Type = %s) ' + e.Message,
                            [xParamNr,
                            FParams[xParamNr].Name,
                            GetEnumName(TypeInfo(TFieldType),ord(FParams[xParamNr].ParamType))]));
          raise Exception.Create(CodeSiteSendError('ADO AppendParam Error: '+ e.Message) + SendDebugQuery);
        end;// on e: EOleException do begin
      end;// try except
    end;//if FCommand.Parameters.Count <> (FParams.Count + 1) then begin
  end;// if FParams.Count > 0 then begin
  {-------------------------------------------------------------------------------}
  xActualParam := 0;
  try
    // Alle Parameter Komplet setzen
    if FParams.Count > 0 then begin
      // Nur weiter, wenn die Anzahl der Parameter stimmt
      if GetADOParamCount = FParams.Count then begin
        for i := 0 to FParams.Count - 1 do begin
          xActualParam := i;
          // Anhand des Typs entscheiden, wie auf den Parameter zugegriffen wird
          case FParams[i].ParamType of
            ptInteger  : FCommand.Parameters[i].Value := FParams[i].AsInteger;
            ptFloat    : FCommand.Parameters[i].Value := FParams[i].AsFloat;
            ptDateTime : FCommand.Parameters[i].Value := FParams[i].AsDateTime;
            ptString   : FCommand.Parameters[i].Value := FParams[i].AsString;
            ptBoolean  : FCommand.Parameters[i].Value := FParams[i].AsBoolean;
            ptBlob     : FCommand.Parameters[i].Value := FParams[i].ADOStream.Read(integer(adReadAll));
            else
              Exception.Create(Format('ParamType %d not recognized',[ord(FParams[i].ParamType)]));
          end;// case FParams[i].ParamType of
          // Wenn der Parametertyp overrouled wurde, dann diesen Datentyp an ADO senden
          if FParams[i].AdoType <> adEmpty then
            FCommand.Parameters[i].Type_ := FParams[i].AdoType;
        end;// for i := 0 to FParams.Count - 1 do begin
      end else begin
        raise Exception.Create('Parameter Count doesn''t fit with ADO');
      end;// if GetADOParamCount = FParams.Count then begin
    end;// if FParams.Count > 0 then begin
  except
    on e: EOleException do begin
      raise Exception.Create(CodeSiteSendError(Format('ADO SetParams Error: %s (Details: %d = %s)', [e.Message, xActualParam, FParams[xActualParam].Name])) + SendDebugQuery);
    end;// on e: EOleException do begin
  end;// try except
end;{ TNativeAdoQuery.SetParams }

//: TNativeAdoQuery.SetRecordset 
(*
Erlaubt den Zugriff auf das native Recordset Objekt. 
Sollte bereits ein Recordset zugewiesen sein, dann
wird dieses freigegeben.

Das neue Recordset wird nur �bernommen, wenn es nicht 
nil ist.
*)
procedure TNativeAdoQuery.SetRecordset(Value: _Recordset);
begin
  if Value <> nil then begin
    // Sollte bereits ein Recordset zugewiesen sein, dann freigeben
    FRecordset := nil;
    // Recordset zuweisen
    FRecordset := Value;
  end;// if Value <> nil then begin
end;{ TNativeAdoQuery.SetRecordset }

//: TNativeAdoQuery.SetTransIsolation 
(*
Default Level ist 'tiReadCommitted'. Dies entspricht dem 
ADO Wert 'adXactReadCommitted'.
*)
procedure TNativeAdoQuery.SetTransIsolation(Value: TTransIsolation);
begin
  if assigned(mDBAccess) then
    mDBAccess.TransIsolation := Value
  else
    raise Exception.Create('TNativeAdoQuery.SetTransIsolation: No AdoDBAccess Object');
end;{ TNativeAdoQuery.SetTransIsolation }

//: TNativeAdoQuery.Sort 
(*
-
*)
procedure TNativeAdoQuery.Sort(aSortString: string);
begin
  if assigned(FRecordset) then
    FRecordset.Sort := aSortString;
end;{ TNativeAdoQuery.Sort }

//: TNativeAdoQuery.SQLChange 
(*
Die Funktion wird aufgerufen, wenn einzelne Zeilen mit Lines[i] 
ver�ndert werden oder wenn �ber die Eigenschaft Text ge�ndert wird.
*)
procedure TNativeAdoQuery.SQLChange(Sender: TObject);
begin
  // F�r InsertSQL mit Nummern Range ist die aktuelle ID ung�ltig
  mIsIDValid := false;
  
  try
    mUpdating := true;
    Close;
  
    // Ereignis nicht ausl�sen solange in dieser Methode
    TStringList(FSQL).OnChange := nil;
  
    // Parameter erzeugen und im Query gleichzeitig mit '?' ersetzen
    FSQL.Text := FParams.CreateParams(GetSQL,false);
  
    // Connection zuweisen
    if FConnection <> nil then begin
      try
        if FConnection.State <> adStateOpen then
          FConnection.Open(FConnection.ConnectionString,'','',integer(adConnectUnspecified));
      except
        on e:Exception do begin
          codeSite.SendError('TNativeAdoQuery.SQLChange: ' + e.Message);
          raise;
        end;// on e:Exception do begin
      end;// try except
    end;// if FConnection <> nil then begin
    FCommand.CommandText := GetSQL;
  
    // Da ADO die Parameters nicht so fleissig aktualisiert, dies hier erledigen
    try
      FCommand.Parameters.Refresh;
    except
    end;//
  
  finally
    mUpdating := false;
    // Ereignis wieder zuweisen
    TStringList(FSQL).OnChange := SQLChange;
  end;// try finally
end;{ TNativeAdoQuery.SQLChange }

//: TNativeAdoQuery.StartTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TNativeAdoQuery.StartTransaction;
begin
  if assigned(mDBAccess) then
    mDBAccess.StartTransaction
  else
    raise Exception.Create('TNativeAdoQuery.StartTransaction: No AdoDBAccess Object');
end;{ TNativeAdoQuery.StartTransaction }

{-**********************************************************************
************************************************************************
******************
******************  Class:    TDBAccessStringList
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TDBAccessStringList.Clear 
(*
Ruft das Clear von TStringList auf. L�scht aber zus�tzlich 
die vorhandenen Parameter.
*)
procedure TDBAccessStringList.Clear;
begin
  inherited Clear;
  {-------------------------------------------------------------------------------}
  // Zus�tzlich den Event OnClearList aufrufen um dem Query die Gelegenheit zu geben
  // seine Parameterliste zu l�schen
  if assigned(FOnClearList) then
    FOnClearList(self);
end;{ TDBAccessStringList.Clear }

(*
Das DB Objekt in der Original DBAccessClass,
handelt die Transaktionen. Diese werden in der 
Klasse AdoDBAccess vom internen Connection Objekt
verwaltet. Um kompatibel zur BDE Vewrsion zu bleiben,
werden die entsprechenden Aufrufe simuliert.
Der endg�ltige Zugriff erfolgt dann �ber die
interne Connection.
*)
{-**********************************************************************
************************************************************************
******************
******************  Class:    TAdoDBAccessDB
******************  Category: No category
******************
************************************************************************
************************************************************************}
//: TAdoDBAccessDB.Create 
(*
-
*)
constructor TAdoDBAccessDB.Create(aDBAccess: TAdoDbAccess);
begin
  mDBAccess := aDBAccess;
end;{ TAdoDBAccessDB.Create }

//: TAdoDBAccessDB.Commit 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccessDB.Commit;
begin
  if assigned(mDBAccess) then
    mDBAccess.Commit
  else
    raise Exception.Create('TAdoDBAccessDB.Commit: No AdoDBAccess Object');
end;{ TAdoDBAccessDB.Commit }

//: TAdoDBAccessDB.InTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
function TAdoDBAccessDB.InTransaction: Boolean;
begin
  if assigned(mDBAccess) then
    result := mDBAccess.InTransaction
  else
    raise Exception.Create('TAdoDBAccessDB.InTransaction: No AdoDBAccess Object');
end;{ TAdoDBAccessDB.InTransaction }

//: TAdoDBAccessDB.Rollback 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccessDB.Rollback;
begin
  if assigned(mDBAccess) then
    mDBAccess.Rollback
  else
    raise Exception.Create('TAdoDBAccessDB.Rollback: No AdoDBAccess Object');
end;{ TAdoDBAccessDB.Rollback }

//: TAdoDBAccessDB.SetTransIsolation 
(*
Default Level ist 'tiReadCommitted'. Dies entspricht dem 
ADO Wert 'adXactReadCommitted'.
*)
procedure TAdoDBAccessDB.SetTransIsolation(Value: TTransIsolation);
begin
  if assigned(mDBAccess) then
    mDBAccess.TransIsolation := Value
  else
    raise Exception.Create('TAdoDBAccessDB.SetTransIsolation: No AdoDBAccess Object');
end;{ TAdoDBAccessDB.SetTransIsolation }

//: TAdoDBAccessDB.StartTransaction 
(*
Die Variable mTransLevel speichert wieviele verschachtelte
Transaktionen im Moment ausgef�hrt werden. Die Methode
'InTransaction' gibt true zur�ck, sobald dieser Wert > 0 ist.
*)
procedure TAdoDBAccessDB.StartTransaction;
begin
  if assigned(mDBAccess) then
    mDBAccess.StartTransaction
  else
    raise Exception.Create('TAdoDBAccessDB.StartTransaction: No AdoDBAccess Object');
end;{ TAdoDBAccessDB.StartTransaction }


end.
