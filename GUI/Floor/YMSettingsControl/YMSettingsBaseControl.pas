(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: SettingsFinderList.pas
| Projectpart...: MillMaster NT
| Subpart.......: Floor
| Process(es)...: -
| Description...: Contains a list of TSettingFinder objects for AXControls on Detail Report
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.06.2000  1.00  Wss | Initial release
| 15.09.2000  1.00  Wss | In CreateLoepfeControl: SettingsFinder.Loaded has to be called manually
| 20.07.2001  1.00  Wss | SettingsFinder replaced with ClassDataReader
| 24.07.2001  1.00  Wss | Bug fixed for invalid ComponentName: mAcitveWindow was Integer -> neg. values
| 20.12.2001  1.00  Wss | UpdateControl: simulate a resize of the control to force container to call
                          save properties when applaying property page
| 04.11.2002        LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 08.05.2003        Wss | In Initialization kann es sein, dass ADO noch nicht ready ist
                          -> Zugriffe per SettingsReader in Funktion GetClassDataReaderFromList() verschoben
|=========================================================================================*)
unit YMSettingsBaseControl;

interface

uses
  Classes, Windows, FloorActiveX,
  mmDictionary, HostlinkDef, HostlinkShare, YMSettingsControl_TLB,
  mmTranslator, LoepfeGlobal, ClassDataReader, Controls, VCLXMLSettingsModel;

const
  cDataReadyCountdown = 3; // Set_Selection, SetMachineSelection, Set_Filter_
  cYMSettingsDic      = 'YMSettingsDictionary';

type
  PClassDataModelRec = ^TClassDataModelRec;
  TClassDataModelRec = record
    Name: string;
    Count: Integer;
    CDR: TClassDataReader;
    Model: TVCLXMLSettingsModel;
  end;
  
  TYMSettingsActiveXControl = class(TFloorActiveXControl, IYMSettingsControl)
  private
    mActiveWindow: DWord;
    mDataCountdown: Byte;
    mTranslator: TmmTranslator;
    function GetClassDataReader: TClassDataReader;
  protected
    mCDMRec: PClassDataModelRec;
    procedure InitializeControl; override;
    procedure LogException(Sender: TObject; aMsg: String);
    procedure TranslateComponent;
    procedure UpdateControl; safecall;
  public
    destructor Destroy; override;
    procedure CreateLoepfeControl(aInstanceClass: TComponentClass); reintroduce;
    function OpenControl: Boolean; virtual;
    procedure SetFilterSelection; override;
    procedure SetMachineSelection(aMachineSelection: String; aProdID: Integer); override;
    procedure SetTimeSelection(aDateFrom, aDateTo: TDateTime; aDataSelection: TDataSelection); override;
    property ClassDataReader: TClassDataReader read GetClassDataReader;
  end;
  

//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,

  mmCS, SysUtils, TypInfo, IvMulti,
  mmEventLog, mmStringList, mmDatabase, MMUGlobal, SettingsReader;

var
  // this variable are used locally in this module only
  lClassDataReaderList: TmmStringList;
  lDictionary: TmmDictionary;
  lLenBase: TLenBase;
  lShiftCalID: Integer;
//------------------------------------------------------------------------------
function GetClassDataReaderFromList(aName: String): PClassDataModelRec;
var
  i: Integer;
begin
  // Da unter WinNT die ADO Schnittstelle im Initialization Teil noch nicht verf�gbar ist
  // d�rfen diese Werte erst hier �ber den SettingsReader geholt werden
  if lShiftCalID = -1 then begin
    with TMMSettingsReader.Instance do begin
      lLenBase    := Value[cMMUnit];
      lShiftCalID := Value[cDefaultShiftCalID];
    end;
  end;
  // look in list if we have already an entry
  i := lClassDataReaderList.IndexOf(aName);
  if i = -1 then begin
    // noop -> create new one
    CodeSite.SendFmtMsg('GetClassDataReaderFromList: Add new name [%s].', [aName]);
    new(Result);
    with Result^ do begin
      Name  := aName;
      Count := 0;
      Model := TVCLXMLSettingsModel.Create(Nil);
      CDR   := TClassDataReader.Create(Nil);
      CDR.Name := 'ClassDataReader_' + aName;
      CDR.Params.LenBase     := lLenBase;
      CDR.Params.ShiftCalID  := lShiftCalID;
      CDR.Params.UsedInFloor := True;
    end;
    // now add the new ClassDataReader object in list and get index
    i := lClassDataReaderList.AddObject(aName, TObject(Result));
  end;
  // increment reference counter and return the object from the given index
  Result := PClassDataModelRec(lClassDataReaderList.Objects[i]);
  inc(Result^.Count);
end;
//------------------------------------------------------------------------------
procedure RemoveClassDataReaderFromList(aName: String);
var
  i: Integer;
  xCDM: PClassDataModelRec;
begin
  i := lClassDataReaderList.IndexOf(aName);
  if i <> -1 then begin
    xCDM := PClassDataModelRec(lClassDataReaderList.Objects[i]);
    dec(xCDM^.Count);

    if xCDM^.Count <= 0 then begin
      CodeSite.SendFmtMsg('RemoveClassDataReaderFromList: delete name [%s].', [aName]);
      xCDM^.CDR.Free;
      xCDM^.Model.Free;
      Dispose(xCDM);
      lClassDataReaderList.Delete(i);
    end;
  end else
    CodeSite.SendError(Format('RemoveClassDataReaderFromList: Name [%s] not found in list.', [aName]));
end;
//------------------------------------------------------------------------------

//:---------------------------------------------------------------------------
//:--- Class: TYMSettingsActiveXControl
//:---------------------------------------------------------------------------
destructor TYMSettingsActiveXControl.Destroy;
begin
  RemoveClassDataReaderFromList(IntToStr(mActiveWindow));
  mTranslator.Free;

  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.CreateLoepfeControl(aInstanceClass: TComponentClass);
begin
  inherited CreateLoepfeControl(aInstanceClass);
  // now DelphiControl property contains a valid reference to the created object
  mActiveWindow := abs(GetActiveWindow);
  if not Assigned(mCDMRec) then begin
    mCDMRec := GetClassDataReaderFromList(IntToStr(mActiveWindow));
    if not Assigned(mCDMRec^.CDR.OnException) then
      mCDMRec^.CDR.OnException := LogException;
  end;
  // Maus/Tastenaktivit�ten ausschalten
  LoepfeControl.Enabled := False;
end;

//:---------------------------------------------------------------------------
function TYMSettingsActiveXControl.GetClassDataReader: TClassDataReader;
begin
  Result := mCDMRec^.CDR;
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.InitializeControl;
begin
  inherited InitializeControl;
  mCDMRec := Nil;
  
  mDataCountdown := cDataReadyCountdown;
  mTranslator    := TmmTranslator.Create(Nil);
  with mTranslator do begin
    Targets.Add(TIvTargetProperty.Create('', 'Caption', ivttInclude));
    Targets.Add(TIvTargetProperty.Create('', 'Hint', ivttInclude));
    Targets.Add(TIvTargetProperty.Create('', 'Items', ivttInclude));
    Dictionary := lDictionary;
  end;
  
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.LogException(Sender: TObject; aMsg: String);
begin
  aMsg := LoepfeControl.ClassName + ': ' + aMsg;
  CodeSite.SendError(aMsg);
  {$IFOPT D-}
  gEventLog.Write(etError, aMsg);
  {$endif}
end;

//:---------------------------------------------------------------------------
function TYMSettingsActiveXControl.OpenControl: Boolean;
begin
  if mDataCountdown > 0 then
    dec(mDataCountdown);
  
  Result := (mDataCountdown = 0);
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.SetFilterSelection;
var
  i: Integer;
begin
  ClassDataReader.Params.MachNames  := '';
  ClassDataReader.Params.StyleNames := '';
  ClassDataReader.Params.ProdNames  := '';
  ClassDataReader.Params.YMSetNames := '';
  if FilterList.Valid then begin
  //    FDelphiControl.ChangeWarningMsg(False, cswFilterNotSupported);
    for i:=0 to FilterList.Count-1 do begin
      with FilterList.Items[i]^ do begin
        case DataItem of
          diMachine_Name: ClassDataReader.Params.MachNames  := CompareValue;
          diStyle_Name:   ClassDataReader.Params.StyleNames := CompareValue;
          diProd_Name:    ClassDataReader.Params.ProdNames  := CompareValue;
          diYM_set_name:  ClassDataReader.Params.YMSetNames := CompareValue;
        else
        end; // case
      end; // with
    end; // for
  end else begin
  //    FDelphiControl.ChangeWarningMsg(True, cswFilterNotSupported);
  end;
  OpenControl;
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.SetMachineSelection(aMachineSelection: String; aProdID: Integer);
begin
  if aProdID = 0 then begin
    ClassDataReader.Params.ProdIDs := '';
    ClassDataReader.Params.MachIDs := aMachineSelection;
  end else begin
    CodeSite.SendInteger('aProdID', aProdID);
    ClassDataReader.Params.ProdIDs := IntToStr(aProdID);
  end;
  OpenControl;
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.SetTimeSelection(aDateFrom, aDateTo: TDateTime; aDataSelection: TDataSelection);
begin
  CodeSite.SendFmtMsg('TYMSettingsActiveXControl.SetTimeSelection: %s -> %s', [DateTimeToStr(aDateFrom), DateTimeToStr(aDateTo)]);
  if aDataSelection in [dsCurShift..dsCustShiftSel] then
    ClassDataReader.Params.TimeMode := tmShift
  else
    ClassDataReader.Params.TimeMode := tmInterval;

  ClassDataReader.Params.TimeFrom := aDateFrom;
  ClassDataReader.Params.TimeTo   := aDateTo;

  OpenControl;
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.TranslateComponent;
begin
  if Assigned(mTranslator.Dictionary) then
    mTranslator.TranslateSubComponent(LoepfeControl, [ivrThisComponentOnly])
end;

//:---------------------------------------------------------------------------
procedure TYMSettingsActiveXControl.UpdateControl;
var
  xRect: TRect;
begin
  try
    // *** HACK ***
    // simulate a resize of the control to force container to call save properties
    // when applaying property page
    xRect := Control.BoundsRect;
    CodeSite.SendRect('Control.BoundsRect', xRect);
    dec(xRect.Left);
    InPlaceSite.OnPosRectChange(xRect);
    inc(xRect.Left);
    InPlaceSite.OnPosRectChange(xRect);
    // *** HACK ***
  
    OpenControl;
  except
  end;
end;


var
  lCDM: PClassDataModelRec;
initialization
  gEventLog := TEventLogWriter.Create('MillMaster', gMMHost, ssApplication, 'YMSettingsControl.dll: ', False);

  lClassDataReaderList := TmmStringList.Create;

  lDictionary := TmmDictionary.Create(Nil);
  with lDictionary do begin
    DictionaryName := cYMSettingsDic;
    AutoConfig     := True;
    Open;
  end;

  // Da unter WinNT die ADO Schnittstelle im Initialization Teil noch nicht verf�gbar ist
  // d�rfen diese Werte erst sp�ter geholt werden -> GetClassDataReaderFromList()
  lLenBase    := lbAbsolute;
  lShiftCalID := -1;

finalization
  with lClassDataReaderList do
  try
    while Count > 0 do begin
      lCDM := PClassDataModelRec(lClassDataReaderList.Objects[0]);
      lCDM^.CDR.Free;
      lCDM^.Model.Free;
      Dispose(lCDM);
      Delete(0);
    end;
  finally
    Free;
  end;

  lDictionary.Free;
  FreeAndNil(gEventLog);
end.

