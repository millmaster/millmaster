object QualityMatrixPPG: TQualityMatrixPPG
  Left = 355
  Top = 136
  Width = 386
  Height = 273
  ActiveControl = rgMatrixTyp
  Caption = '(*)Klassiermatrix'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = PropertyPageCreate
  PixelsPerInch = 96
  TextHeight = 13
  object rgMatrixTyp: TmmRadioGroup
    Left = 0
    Top = 0
    Width = 378
    Height = 41
    Align = alTop
    Caption = '(30)Matrixtyp'
    Columns = 3
    ItemIndex = 0
    Items.Strings = (
      '(30)Garn'
      '(30)Spleiss'
      '(30)Siro')
    TabOrder = 0
    CaptionSpace = True
  end
  object gbQMatrixSettings: TmmGroupBox
    Left = 0
    Top = 45
    Width = 378
    Height = 201
    Align = alClient
    Caption = '(*)Einstellungen'
    TabOrder = 1
    CaptionSpace = True
    object mmLabel1: TmmLabel
      Left = 8
      Top = 24
      Width = 82
      Height = 13
      Caption = '(*)Reinigerkurven'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel4: TmmLabel
      Left = 8
      Top = 120
      Width = 72
      Height = 13
      Caption = '(*)Klassierfelder'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel2: TmmLabel
      Left = 224
      Top = 24
      Width = 59
      Height = 13
      Caption = '(*)Garnfehler'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel5: TmmLabel
      Left = 224
      Top = 120
      Width = 64
      Height = 13
      Caption = '(*)Klassierung'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object mmLabel3: TmmLabel
      Left = 16
      Top = 144
      Width = 63
      Height = 13
      Caption = '(*)Grundfarbe'
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbChannelCurve: TmmCheckBox
      Left = 16
      Top = 40
      Width = 137
      Height = 21
      Hint = '(*)Kanalkurve ein/ausschalten'
      Caption = '(15)Kanalkurve'
      TabOrder = 0
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbClusterCurve: TmmCheckBox
      Left = 16
      Top = 66
      Width = 137
      Height = 21
      Hint = '(*)Klusterkurve ein/ausschalten'
      Caption = '(15)Fehlerschwarm'
      TabOrder = 1
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbSpliceCurve: TmmCheckBox
      Left = 16
      Top = 92
      Width = 137
      Height = 21
      Hint = '(*)Spleisskurve ein/ausschalten'
      Caption = '(15)Spleisskurve'
      TabOrder = 2
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object cbClassFields: TmmCheckBox
      Left = 16
      Top = 160
      Width = 137
      Height = 21
      Hint = '(*)Klassierfelder ein/ausschalten'
      Caption = '(15)Sichtbar'
      TabOrder = 3
      Visible = True
      AutoLabel.LabelPosition = lpLeft
    end
    object bChannelColor: TmmColorButton
      Left = 152
      Top = 40
      Width = 42
      Height = 21
      AutoLabel.Distance = 8
      AutoLabel.LabelPosition = lpLeft
      Caption = '(10)&Weitere...'
      Color = clRed
      Margin = 4
      TabOrder = 4
      TabStop = True
      Visible = True
    end
    object bClusterColor: TmmColorButton
      Left = 152
      Top = 66
      Width = 42
      Height = 21
      AutoLabel.Distance = 8
      AutoLabel.LabelPosition = lpLeft
      Caption = '(10)&Weitere...'
      Color = clBlue
      Margin = 4
      TabOrder = 5
      TabStop = True
      Visible = True
    end
    object bSpliceColor: TmmColorButton
      Left = 152
      Top = 92
      Width = 42
      Height = 21
      AutoLabel.Distance = 8
      AutoLabel.LabelPosition = lpLeft
      Caption = '(10)&Weitere...'
      Color = clLime
      Margin = 4
      TabOrder = 6
      TabStop = True
      Visible = True
    end
    object bActiveColor: TmmColorButton
      Left = 152
      Top = 160
      Width = 42
      Height = 21
      AutoLabel.Distance = 8
      AutoLabel.LabelPosition = lpLeft
      Caption = '(10)&Weitere...'
      Color = clAqua
      Margin = 4
      TabOrder = 7
      TabStop = True
      Visible = True
    end
    object pnMatrixMode: TmmPanel
      Left = 232
      Top = 136
      Width = 121
      Height = 36
      BevelOuter = bvNone
      TabOrder = 8
      object rbQMCoarse: TmmRadioButton
        Left = 0
        Top = 0
        Width = 113
        Height = 17
        Hint = '(*)Grobe Klassierung anzeigen'
        Caption = '(*)Grob'
        TabOrder = 0
      end
      object rbQMFine: TmmRadioButton
        Left = 0
        Top = 16
        Width = 113
        Height = 17
        Hint = '(*)Feine Klassierung anzeigen'
        Caption = '(*)Fein'
        TabOrder = 1
      end
    end
    object mmPanel1: TmmPanel
      Left = 232
      Top = 40
      Width = 121
      Height = 49
      BevelOuter = bvNone
      TabOrder = 9
      object rbDefects: TmmRadioButton
        Left = 0
        Top = 0
        Width = 113
        Height = 17
        Hint = '(*)Grobe Klassierung anzeigen'
        Caption = '(*)Fehler'
        TabOrder = 0
        OnClick = rbYarnDataClick
      end
      object rbCuts: TmmRadioButton
        Tag = 1
        Left = 0
        Top = 16
        Width = 113
        Height = 17
        Hint = '(*)Feine Klassierung anzeigen'
        Caption = '(*)Schnitte'
        TabOrder = 1
        OnClick = rbYarnDataClick
      end
      object rbDefectCuts: TmmRadioButton
        Tag = 2
        Left = 0
        Top = 32
        Width = 113
        Height = 17
        Hint = '(*)Feine Klassierung anzeigen'
        Caption = '(*)Beide'
        TabOrder = 2
        OnClick = rbYarnDataClick
      end
    end
    object bInactiveColor: TmmColorButton
      Left = 152
      Top = 136
      Width = 42
      Height = 21
      AutoLabel.Distance = 8
      AutoLabel.LabelPosition = lpLeft
      Caption = '(10)&Weitere...'
      Color = clAqua
      Margin = 4
      TabOrder = 10
      TabStop = True
      Visible = True
    end
  end
  object mmPanel2: TmmPanel
    Left = 0
    Top = 41
    Width = 378
    Height = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
  end
  object mmActionList: TmmActionList
    OnUpdate = mmActionListUpdate
    Left = 160
    Top = 32
    object acChannelCurve: TAction
      Caption = '(*)Kanalkurve'
      Hint = '(*)Kanalkurve ein/ausschalten'
    end
    object acClusterCurve: TAction
      Caption = '(*)Klusterkurve'
      Hint = '(*)Klusterkurve ein/ausschalten'
    end
    object acSpliceCurve: TAction
      Caption = '(*)Spleisskurve'
      Hint = '(*)Spleisskurve ein/ausschalten'
    end
    object acClassFields: TAction
      Caption = '(*)Klassierfelder'
      Hint = '(*)Klassierfelder ein/ausschalten'
    end
  end
  object mDictionary: TmmDictionary
    DictionaryName = 'MainDict'
    AutoConfig = True
    Left = 192
    Top = 32
  end
  object mTranslator: TmmTranslator
    DictionaryName = 'MainDict'
    Left = 224
    Top = 32
    TargetsData = (
      1
      3
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0))
  end
end
