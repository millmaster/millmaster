(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FLRCSBasicImpl.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 18.09.2001 1.00 Wss | 
|=============================================================================*)
unit FLRCSBasicImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, FloorActiveX, FloorAXCtrls, YMSettingsControl_TLB,
  HostlinkShare, YMSettingsBaseControl, YMBasicProdParaBox;

type
  TClearerSettingsBasic = class(TYMSettingsActiveXControl, IClearerSettingsBasic)
  private
    FDelphiControl: TGBBasicProdPara;
    FEvents: IClearerSettingsBasicEvents;
  protected
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
  public
    function OpenControl: Boolean; override;
    destructor Destroy; override;
    // these methoses will be overriden from descendant class to get Floor information
{
    procedure SetMachineSelection(aMachineSelection: String; aProdID: Integer); override;
    procedure SetTimeSelection(aDateFrom, aDateTo: TDateTime; aDataSelection: TDataSelection); override;
{}
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  RzCSIntf,
  ComObj, SysUtils;

//------------------------------------------------------------------------------
// TFLRCSBasic
//------------------------------------------------------------------------------
function TClearerSettingsBasic.OpenControl: Boolean;
begin
  Result := inherited OpenControl;
  if Result then
  try
//TODO wss: Basic Settings noch implementieren
//    FDelphiControl.Open;
    CodeSite.SendMsg('TClearerSettingsBasic: Open OK');
  except
    on e:Exception do begin
      CodeSite.SendError('TClearerSettingsBasic: Open failed: ' + e.Message);
//      WriteToEventLog('Open failed: ' + e.Message, 'TClearerSettingsBasic.OpenControl: ', etError, 'MillMaster', 'Wetwss');
    end;
    // the quality matrix frame from Martin does missing somethings
  end;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  {TODO: Define property pages here.  Property pages are defined by calling
    DefinePropertyPage with the class id of the page.  For example,
      DefinePropertyPage(Class_FLRCSBasicPage); }
end;
//------------------------------------------------------------------------------
destructor TclearerSettingsBasic.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IClearerSettingsBasicEvents;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.InitializeControl;
begin
  inherited InitializeControl;
  CreateLoepfeControl(TGBBasicProdPara);
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := FDelphiControl.DrawTextBiDiModeFlagsReadingOnly;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.Get_Cursor: Smallint;
begin
  Result := Smallint(FDelphiControl.Cursor);
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.Get_DoubleBuffered: WordBool;
begin
  Result := FDelphiControl.DoubleBuffered;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.Get_Enabled: WordBool;
begin
  Result := FDelphiControl.Enabled;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.Get_Visible: WordBool;
begin
  Result := FDelphiControl.Visible;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.Get_VisibleDockClientCount: Integer;
begin
  Result := FDelphiControl.VisibleDockClientCount;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.IsRightToLeft: WordBool;
begin
  Result := FDelphiControl.IsRightToLeft;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.UseRightToLeftReading: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftReading;
end;
//------------------------------------------------------------------------------
function TClearerSettingsBasic.UseRightToLeftScrollBar: WordBool;
begin
  Result := FDelphiControl.UseRightToLeftScrollBar;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.Set_Cursor(Value: Smallint);
begin
  FDelphiControl.Cursor := TCursor(Value);
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.Set_DoubleBuffered(Value: WordBool);
begin
  FDelphiControl.DoubleBuffered := Value;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.Set_Enabled(Value: WordBool);
begin
  FDelphiControl.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TClearerSettingsBasic.Set_Visible(Value: WordBool);
begin
  FDelphiControl.Visible := Value;
end;
//------------------------------------------------------------------------------
initialization
//  InitializeFloorObjectFactory(TFLRCSBasic, Class_FLRCSBasic);
end.
