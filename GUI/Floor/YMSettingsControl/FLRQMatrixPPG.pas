(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FLRQMatrixPPG.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 18.09.2001 1.00 Wss | Property MatrixMode changed to use DisplayMode for coarse, fine view
|=============================================================================*)
unit FLRQMatrixPPG;

interface

uses SysUtils, Windows, Messages, Classes, Graphics, Controls, StdCtrls,
  ExtCtrls, Forms, ComServ, ComObj, StdVcl, FloorAxCtrls, ActnList,
  mmActionList, mmCheckBox, mmColorButton, mmGroupBox, mmRadioGroup,
  IvDictio, IvMulti, IvEMulti, mmTranslator, IvAMulti, IvBinDic,
  mmDictionary, mmRadioButton, mmLabel, mmPanel;

type
  TQualityMatrixPPG = class(TPropertyPage)
    rgMatrixTyp: TmmRadioGroup;
    mmActionList: TmmActionList;
    acChannelCurve: TAction;
    acClusterCurve: TAction;
    acSpliceCurve: TAction;
    acClassFields: TAction;
    gbQMatrixSettings: TmmGroupBox;
    mmLabel1: TmmLabel;
    mmLabel4: TmmLabel;
    mmLabel2: TmmLabel;
    mmLabel5: TmmLabel;
    cbChannelCurve: TmmCheckBox;
    cbClusterCurve: TmmCheckBox;
    cbSpliceCurve: TmmCheckBox;
    cbClassFields: TmmCheckBox;
    bChannelColor: TmmColorButton;
    bClusterColor: TmmColorButton;
    bSpliceColor: TmmColorButton;
    bActiveColor: TmmColorButton;
    pnMatrixMode: TmmPanel;
    rbQMCoarse: TmmRadioButton;
    rbQMFine: TmmRadioButton;
    mmPanel1: TmmPanel;
    rbDefects: TmmRadioButton;
    rbCuts: TmmRadioButton;
    rbDefectCuts: TmmRadioButton;
    mDictionary: TmmDictionary;
    mTranslator: TmmTranslator;
    bInactiveColor: TmmColorButton;
    mmLabel3: TmmLabel;
    mmPanel2: TmmPanel;
    procedure mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure rbYarnDataClick(Sender: TObject);
    procedure PropertyPageCreate(Sender: TObject);
  private
    mDataView: Integer;
  protected
  public
    procedure UpdateObject; override;
    procedure UpdatePropertyPage; override;
  end;

const
  Class_QualityMatrixPPG: TGUID = '{D04E85BB-48D4-11D4-86E5-00105A55E052}';

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS;

{$R *.DFM}
//------------------------------------------------------------------------------
procedure TQualityMatrixPPG.mmActionListUpdate(Action: TBasicAction; var Handled: Boolean);
begin
  Handled := True;

  bChannelColor.Enabled := cbChannelCurve.Checked;
  bClusterColor.Enabled := cbClusterCurve.Checked;
  bSpliceColor.Enabled  := cbSpliceCurve.Checked;
  bActiveColor.Enabled  := cbClassFields.Checked;
end;
//------------------------------------------------------------------------------
procedure TQualityMatrixPPG.UpdateObject;
begin
  // Update OleObject from your controls
  OleObject.MatrixTyp  := rgMatrixTyp.ItemIndex;

  OleObject.DataView   := mDataView;

  OleObject.ChannelVisible := cbChannelCurve.Checked;
  OleObject.ChannelColor   := bChannelColor.Color;

  OleObject.ClusterVisible := cbClusterCurve.Checked;
  OleObject.ClusterColor   := bClusterColor.Color;

  OleObject.SpliceVisible  := cbSpliceCurve.Checked;
  OleObject.SpliceColor    := bSpliceColor.Color;

  OleObject.ActiveVisible  := cbClassFields.Checked;
  OleObject.ActiveColor    := bActiveColor.Color;
  OleObject.InactiveColor  := bInactiveColor.Color;

  if rbQMCoarse.Checked then OleObject.DisplayMode := 0
                        else OleObject.DisplayMode := 1;

  // finally update control
  OleObject.UpdateControl;
end;
//------------------------------------------------------------------------------
procedure TQualityMatrixPPG.UpdatePropertyPage;
begin
  // Update your controls from OleObject
  rgMatrixTyp.ItemIndex  := OleObject.MatrixTyp;

  mDataView := OleObject.DataView;
  case mDataView of
    0: rbDefects.Checked    := True;
    1: rbCuts.Checked       := True;
    2: rbDefectCuts.Checked := True;
  else
  end;

  cbChannelCurve.Checked := OleObject.ChannelVisible;
  bChannelColor.Color    := OleObject.ChannelColor;

  cbClusterCurve.Checked := OleObject.ClusterVisible;
  bClusterColor.Color    := OleObject.ClusterColor;

  cbSpliceCurve.Checked  := OleObject.SpliceVisible;
  bSpliceColor.Color     := OleObject.SpliceColor;

  cbClassFields.Checked  := OleObject.ActiveVisible;
  bActiveColor.Color     := OleObject.ActiveColor;
  bInactiveColor.Color   := OleObject.InactiveColor;


  if OleObject.DisplayMode = 0 then rbQMCoarse.Checked := True
                               else rbQMFine.Checked := True;

end;
//------------------------------------------------------------------------------
procedure TQualityMatrixPPG.rbYarnDataClick(Sender: TObject);
begin
  mDataView := (Sender as TRadioButton).Tag;
end;
//------------------------------------------------------------------------------
procedure TQualityMatrixPPG.PropertyPageCreate(Sender: TObject);
begin
  DoubleBuffered := True;
end;

initialization
  TActiveXPropertyPageFactory.Create(
    ComServer,
    TQualityMatrixPPG,
    Class_QualityMatrixPPG);
end.

