(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: FLRCSettingsImpl.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 09.07.2001  1.00  Wss | SFIVisible and FFClusterVisible property added
| 16.06.2006  1.00  Wss | YarnCount Box wird nun manuell in ReadXMLSettings Enabled
//| 27.07.2006        Wss | Handling f�r IPI Parameter eingef�gt
| 15.09.2008  1.01  Nue | Handling f�r VCV Parameter eingef�gt
|=========================================================================================*)
unit FLRXMLSettingsImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, FloorActiveX, FloorAXCtrls, YMSettingsControl_TLB,
  HostlinkShare, YMSettingsBaseControl, YMParaEditBox, EditBox;

type
  TClearerSettings = class(TYMSettingsActiveXControl, IXMLSettings)
  private
    FEvents: IClearerSettingsEvents;
    mSettingsGroup: TSettingsGroup;
    mDoResize: Boolean;
    function GetCSettings: TGroupEditBox;
  protected
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_SettingsGroup: Integer; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    procedure LoadFromStream(const Stream: IStream); override;
    procedure SaveToStream(const Stream: IStream); override;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_SettingsGroup(Value: Integer); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    property CSettings: TGroupEditBox read GetCSettings;
  public
    function OpenControl: Boolean; override;
  published
end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,

  ComObj, SysUtils, FLRXMLSettingsPPG, mmcs, dialogs;

//------------------------------------------------------------------------------
procedure TClearerSettings.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  DefinePropertyPage(Class_XMLSettingsPPG)
end;

//------------------------------------------------------------------------------
function TClearerSettings.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := LoepfeControl.DrawTextBiDiModeFlagsReadingOnly;
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IClearerSettingsEvents;
end;

//------------------------------------------------------------------------------
function TClearerSettings.GetCSettings: TGroupEditBox;
begin
  Result := (LoepfeControl as TGroupEditBox);
end;

//------------------------------------------------------------------------------
function TClearerSettings.Get_Cursor: Smallint;
begin
  Result := Smallint(LoepfeControl.Cursor);
end;

//------------------------------------------------------------------------------
function TClearerSettings.Get_DoubleBuffered: WordBool;
begin
  Result := LoepfeControl.DoubleBuffered;
end;

//------------------------------------------------------------------------------
function TClearerSettings.Get_Enabled: WordBool;
begin
  Result := LoepfeControl.Enabled;
end;

//------------------------------------------------------------------------------
function TClearerSettings.Get_Visible: WordBool;
begin
  Result := LoepfeControl.Visible;
end;

//------------------------------------------------------------------------------
function TClearerSettings.Get_VisibleDockClientCount: Integer;
begin
  Result := LoepfeControl.VisibleDockClientCount;
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.InitializeControl;
begin
  inherited InitializeControl;
  mDoResize      := True;
  mSettingsGroup := sgNone;
end;

//------------------------------------------------------------------------------
function TClearerSettings.IsRightToLeft: WordBool;
begin
  Result := LoepfeControl.IsRightToLeft;
end;

//------------------------------------------------------------------------------
// TFLRCSettings
//------------------------------------------------------------------------------
function TClearerSettings.OpenControl: Boolean;
  //..........................................................
  procedure ReadXMLSettings;
  var
    xDoUpdateXML: Boolean;
  begin
    // Parameter wurden ver�ndert und XMLSettingsReady wird auf False gesetzt
    // Beim 1. Zugriff auf die Liste werden die Settings geholt und das Property auf True gewechselt
    // Beim Zuweisen der Settings in das Model werden alle anderen Komponenten benachrichtigt. Diese m�ssen
    // in der OpenControl Methode aber die Settings nicht nochmals holen. Daher die Abfrage �ber XMLSettingsReady
    with ClassDataReader do begin
      // Die erste aktive Komponente l�sst die Settings lesen und f�gt diese ins Model ein
      // Auch wenn keine oder zuviele Settings vorhanden sind wird das Flag auf True gesetzt
      xDoUpdateXML := not XMLSettingsReady; // Nach auslesen der Settings ist XMLSettingsReady = True

      if XMLSettings.Count = 1 then begin
        // Die YarnCount Box ist die einzige, welche nicht in CheckComponents() sich pauschal
        // per Enabled := True freigeben kann. F�hrt zu Problemen mit Templates in Assignment wegen Coarse/Fine
        // Daher wird das hier �bernommen.
        if mSettingsGroup = sgYarnCount then begin
          CSettings.Enabled := True;
          with TYarnCountEditBox(CSettings) do begin
            YarnCount := ClassDataReader.XMLSettings[0]^.YarnCount;
            YarnUnit  := ClassDataReader.XMLSettings[0]^.YarnUnit;
          end;
        end;
        if xDoUpdateXML then
          // Settings werden 1x am Model �bergeben, welche die Komponenten benachrichtig.
          // Jede Komponente setzt sein Enabled Status anhand des FPattern etc selber.
          CSettings.Model.xmlAsString := XMLSettings[0]^.XMLData;
      end else
        // Settings sind schon gelesen. Wenn mehr als 1 Setting oder kein vorhanden
        // ist dann Komponente "manuell" disablen, da ja keine Benachrichtigung �ber
        // das Model ausgel�st wird.
        CSettings.Enabled := False;
    end;

    if not CSettings.Enabled then
      CSettings.ClearValues;
  end;
  //..........................................................
begin
  if mDoResize then begin
    mDoResize := False;
  end;

  Result := inherited OpenControl;
  if Result then
  try
    if FilterList.Valid then begin
      ReadXMLSettings;
      FloorControl.UpdateVCLControl;
      CodeSite.SendMsg(LoepfeControl.ClassName + ': OpenControl OK');
    end else
      CSettings.ClearValues;
  except
    on e:Exception do
      LogException(Self, 'TClearerSettings: OpenControl failed: ' + e.Message);
  end;
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.Set_Cursor(Value: Smallint);
begin
  LoepfeControl.Cursor := TCursor(Value);
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.Set_DoubleBuffered(Value: WordBool);
begin
  LoepfeControl.DoubleBuffered := Value;
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.Set_Enabled(Value: WordBool);
begin
  LoepfeControl.Enabled := Value;
end;

//------------------------------------------------------------------------------
procedure TClearerSettings.Set_Visible(Value: WordBool);
begin
  LoepfeControl.Visible := Value;
end;

//------------------------------------------------------------------------------
function TClearerSettings.UseRightToLeftReading: WordBool;
begin
  Result := LoepfeControl.UseRightToLeftReading;
end;

//------------------------------------------------------------------------------
function TClearerSettings.UseRightToLeftScrollBar: WordBool;
begin
  Result := LoepfeControl.UseRightToLeftScrollBar;
end;

//------------------------------------------------------------------------------

function TClearerSettings.Get_SettingsGroup: Integer;
begin
  Result := Ord(mSettingsGroup);
end;
//:-----------------------------------------------------------------------------
procedure TClearerSettings.LoadFromStream(const Stream: IStream);
var
  xOleStream: TOleStream;
  xSettingsGroup: Integer;
begin
  xOleStream := TOleStream.Create(Stream);
  try
    // erst eigene Daten lesen...
    xOleStream.ReadBuffer(xSettingsGroup, sizeof(xSettingsGroup));
    if (xSettingsGroup < ord(Low(TSettingsGroup))) or (xSettingsGroup > Ord(High(TSettingsGroup))) then
      xSettingsGroup := ord(sgChannel);

    //...anhand dieser die richtige Komponente instanzieren...
    Set_SettingsGroup(xSettingsGroup);
  finally
    xOleStream.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TClearerSettings.SaveToStream(const Stream: IStream);
var
  xOleStream: TOleStream;
  xWriter: TWriter;
  xSettingsGroup: Integer;
begin
  xOleStream := TOleStream.Create(Stream);
  try
    xWriter := TWriter.Create(xOleStream, 4096);
    try
      xWriter.IgnoreChildren := True;

      // erste eigene Daten schreiben...
      xSettingsGroup := ord(mSettingsGroup);
      xWriter.Write(xSettingsGroup, sizeof(xSettingsGroup));
    finally
      xWriter.Free;
    end;
  finally
    xOleStream.Free;
  end;
end;
//:-----------------------------------------------------------------------------
procedure TClearerSettings.Set_SettingsGroup(Value: Integer);
  //........................................................
  procedure PrepareControl;
  begin
    with TGroupEditBox(LoepfeControl) do begin
      Printable := True;
      Model     := mCDMRec^.Model;
      // Gr�sse des ActiveX Controls von der VCL Komponente �bernehmen
      FloorControl.Height := Height;
      FloorControl.Width  := Width;
    end;
    TranslateComponent;
  end;
  //........................................................
begin
  if mSettingsGroup <> TSettingsGroup(Value) then begin
    mSettingsGroup := TSettingsGroup(Value);

    if Assigned(LoepfeControl) then
      LoepfeControl.Free;

//    FloorControl.DrawTest := True;
    case mSettingsGroup of
      sgSplice:     CreateLoepfeControl(TSpliceEditBox);
      sgCluster:    CreateLoepfeControl(TFaultClusterEditBox);
      sgFCluster:   CreateLoepfeControl(TFFClusterEditBox);
      sgSFI:        CreateLoepfeControl(TSFIEditBox);
      sgVCV:        CreateLoepfeControl(TVCVEditBox);   //Nue:15.09.08
      sgYarnCount:  CreateLoepfeControl(TYarnCountEditBox);
      sgP:          CreateLoepfeControl(TPEditBox);
      sgPExt:       CreateLoepfeControl(TPExtEditBox);
      sgRepetition: CreateLoepfeControl(TRepetitionEditBox);
      //TODO: In PPG Datei die IPI Limits auch noch einf�gen
//      sgIPILimits:  CreateLoepfeControl(TIPIEditBox);
    else // sgChannel
      CreateLoepfeControl(TChannelEditBox);
    end;
    PrepareControl;
  end;
end;

initialization
  InitializeFloorObjectFactory(TClearerSettings, CLASS_ClearerSettings);
end.

