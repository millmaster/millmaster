(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: FLRQMatrixImpl.pas
| Projectpart...:
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0 SP 6
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------
| History:
| Date       Vers Vis | Reason
|-------------------------------------------------------------------------------
|-------------------------------------------------------------------------------
| 18.09.2001 1.00 Wss | Property MatrixMode changed to use DisplayMode for coarse, fine view
| 30.09.2005 1.00 Wss | SLT Matrix erscheint nicht nach initialisierung wenn neu Platziert
                        -> Fake mit zuweisen von Siro/SLT in InitializeControl()
| 26.10.2005 1.00 Wss | - In Initialization wird QMatrix auf FixedSize gesetzt
                        - In PrepareControlSize wird noch QMatrix.CalculateMatrixSize aufgerufen
|=============================================================================*)
unit FLRQMatrixImpl;

interface

uses
  Windows, ActiveX, Classes, Controls, Graphics, Menus, Forms, StdCtrls,
  ComServ, StdVCL, FloorActiveX, FloorAXCtrls, YMSettingsControl_TLB,
  HostlinkShare, LoepfeGlobal, YMSettingsBaseControl, QualityMatrix, //MMQualityMatrixClass,
  MMUGlobal;

type
  TFloorQualityMatrix = class(TYMSettingsActiveXControl, IQualityMatrix)
  private
    fChannelVisible: Boolean;
    fSpliceVisible: Boolean;
    fClusterVisible: Boolean;
    fActiveVisible: Boolean;
    FEvents: IQualityMatrixEvents;
    fDataView: TClassViewTyp;
//    function GetQMatrix: QualityMatrix.TFloorQualityMatrix;
    function GetQMatrix: TQualityMatrix;
  protected
    property QMatrix: TQualityMatrix read GetQMatrix;
    procedure DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage); override;
    procedure EventSinkChanged(const EventSink: IUnknown); override;
    procedure InitializeControl; override;
    function DrawTextBiDiModeFlagsReadingOnly: Integer; safecall;
    function Get_Cursor: Smallint; safecall;
    function Get_DoubleBuffered: WordBool; safecall;
    function Get_Enabled: WordBool; safecall;
    function Get_Visible: WordBool; safecall;
    function Get_VisibleDockClientCount: Integer; safecall;
    function IsRightToLeft: WordBool; safecall;
    function UseRightToLeftReading: WordBool; safecall;
    function UseRightToLeftScrollBar: WordBool; safecall;
    procedure Set_Cursor(Value: Smallint); safecall;
    procedure Set_DoubleBuffered(Value: WordBool); safecall;
    procedure Set_Enabled(Value: WordBool); safecall;
    procedure Set_Visible(Value: WordBool); safecall;
    function Get_ActiveColor: Integer; safecall;
    function Get_ActiveVisible: WordBool; safecall;
    function Get_ChannelColor: Integer; safecall;
    function Get_ChannelVisible: WordBool; safecall;
    function Get_ClusterColor: Integer; safecall;
    function Get_ClusterVisible: WordBool; safecall;
    function Get_DataView: Integer; safecall;
    function Get_DisplayMode: Integer; safecall;
    function Get_MatrixTyp: Integer; safecall;
    function Get_SpliceColor: Integer; safecall;
    function Get_SpliceVisible: WordBool; safecall;
    procedure Set_ActiveColor(Value: Integer); safecall;
    procedure Set_ActiveVisible(Value: WordBool); safecall;
    procedure Set_ChannelColor(Value: Integer); safecall;
    procedure Set_ChannelVisible(Value: WordBool); safecall;
    procedure Set_ClusterColor(Value: Integer); safecall;
    procedure Set_ClusterVisible(Value: WordBool); safecall;
    procedure Set_DataView(Value: Integer); safecall;
    procedure Set_MatrixTyp(Value: Integer); safecall;
    procedure Set_SpliceColor(Value: Integer); safecall;
    procedure Set_SpliceVisible(Value: WordBool); safecall;
    procedure Set_DisplayMode(Value: Integer); safecall;
    function Get_InactiveColor: Integer; safecall;
    procedure LoadFromStream(const Stream: IStream); override;
    procedure SaveToStream(const Stream: IStream); override;
    procedure Set_InactiveColor(Value: Integer); safecall;
    procedure OnPrepareControlSize(Sender: TObject; var aWidth, aHeight: Integer);
  public
    function OpenControl: Boolean; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  mmCS, ComObj, SysUtils,
  QualityMatrixDef, QualityMatrixBase, FLRQMatrixPPG, XMLGlobal;

//------------------------------------------------------------------------------
// TFLRQMatrix
//------------------------------------------------------------------------------
function TFloorQualityMatrix.OpenControl: Boolean;
  //..........................................................
  procedure UpdateSettingsProperties(aVisible: Boolean);
  begin
    with QMatrix do begin
      ActiveVisible  := aVisible and fActiveVisible;
      ChannelVisible := aVisible and fChannelVisible;
      ClusterVisible := aVisible and fClusterVisible;
      SpliceVisible  := aVisible and fSpliceVisible;
    end;
  end;
  //..........................................................
  procedure ReadXMLSettings;
  begin
    // Parameter wurden ver�ndert und XMLSettingsReady wird auf False gesetzt
    // Beim 1. Zugriff auf die Liste werden die Settings geholt und das Property auf True gewechselt
    // Beim Zuweisen der Settings in das Model werden alle anderen Komponenten benachrichtigt. Diese m�ssen
    // in der OpenControl Methode aber die Settings nicht nochmals holen. Daher die Abfrage �ber XMLSettingsReady
    with ClassDataReader do begin
      // Die erste aktive Komponente l�sst die Settings lesen und f�gt diese ins Model ein.
      // Auch wenn keine oder zuviele Settings vorhanden sind wird das Flag auf True gesetzt
      if not XMLSettingsReady then begin
        if XMLSettings.Count = 1 then begin
          QMatrix.Model.xmlAsString := XMLSettings[0]^.XMLData;
          if CodeSite.Enabled then
            CodeSite.SendString('TQualityMatrix.OpenControl: XMLData', FormatXML(XMLSettings[0]^.XMLData));
        end;
      end else begin
      end;
      case XMLSettings.Count of
        0: FloorControl.WarningMsg := GetWarningMsg(cswNoSettings);
        1: FloorControl.WarningMsg := GetWarningMsg(cswNone);
      else
        FloorControl.WarningMsg := GetWarningMsg(cswManySettings);
      end;
      UpdateSettingsProperties(XMLSettings.Count = 1);
    end; // with ClassDataReader
  end;
  //..........................................................
  procedure ReadMatrixData;
  var
    xFillCut, xFillUncut: Boolean;
  begin
    try
      xFillUncut := (fDataView = cvOnlyUncut) or (fDataView = cvCutAndUnCut);
      xFillCut   := (fDataView = cvOnlyCut)   or (fDataView = cvCutAndUnCut);
      ClassDataReader.FillValuesToMatrix(xFillUncut, xFillCut, QMatrix);
    except
      on e:Exception do begin
        raise Exception.Create('ReadMatrixData failed: ' + e.Message);
      end;
    end;  // if Result
  end;
  //..........................................................
var
  i: Integer;
begin
EnterMethod('TQualityMatrix.OpenControl');
  Result := inherited OpenControl;
  if Result then
  try
    if FilterList.Valid then begin
      ReadXMLSettings;
      ReadMatrixData;
      FloorControl.UpdateVCLControl;
      CodeSite.SendMsgEx(csmGreen, 'OpenControl OK');
//      for i:=0 to QMatrix.TotFields-1 do
//        CodeSite.SendFloat('Field ' + inttostr(i+1), QMatrix.GetValue(i, cDefects));
    end else begin
      FloorControl.WarningMsg := GetWarningMsg(cswFilterNotSupported);
      QMatrix.ClearValues;             // Daten l�schen
      UpdateSettingsProperties(False); // Settings ausblenden
    end;
  except
    on e:Exception do begin
      LogException(Self, 'OpenControl failed: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.DefinePropertyPages(DefinePropertyPage: TDefinePropertyPage);
begin
  DefinePropertyPage(Class_QualityMatrixPPG);
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.EventSinkChanged(const EventSink: IUnknown);
begin
  FEvents := EventSink as IQualityMatrixEvents;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.GetQMatrix: TQualityMatrix;
begin
  Result := (LoepfeControl as TQualityMatrix);
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.InitializeControl;
begin
  inherited InitializeControl;
  fActiveVisible  := True;
  fChannelVisible := True;
  fClusterVisible := True;
  fSpliceVisible  := True;
  fDataView       := cvCutAndUncut;

  FloorControl.OnPrepareControlSize := OnPrepareControlSize;
  CreateLoepfeControl(TQualityMatrix);
  QMatrix.FixedSize := True;

  // Fake: zwingt die Matrix irgendwie dazu, sich neu zu zeichnen.
  // Wenn diese 2 Zeilen fehlen, dann bleibt die Matrix leer!?!?
  QMatrix.MatrixType := mtSiro;
  QMatrix.MatrixType := mtShortLongThin;
  
  QMatrix.Model := mCDMRec^.Model;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.DrawTextBiDiModeFlagsReadingOnly: Integer;
begin
  Result := QMatrix.DrawTextBiDiModeFlagsReadingOnly;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_Cursor: Smallint;
begin
  Result := Smallint(QMatrix.Cursor);
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_DoubleBuffered: WordBool;
begin
  Result := QMatrix.DoubleBuffered;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_Enabled: WordBool;
begin
  Result := QMatrix.Enabled;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_Visible: WordBool;
begin
  Result := QMatrix.Visible;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_VisibleDockClientCount: Integer;
begin
  Result := QMatrix.VisibleDockClientCount;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.IsRightToLeft: WordBool;
begin
  Result := QMatrix.IsRightToLeft;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.UseRightToLeftReading: WordBool;
begin
  Result := QMatrix.UseRightToLeftReading;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.UseRightToLeftScrollBar: WordBool;
begin
  Result := QMatrix.UseRightToLeftScrollBar;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_Cursor(Value: Smallint);
begin
  QMatrix.Cursor := TCursor(Value);
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_DoubleBuffered(Value: WordBool);
begin
  QMatrix.DoubleBuffered := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_Enabled(Value: WordBool);
begin
  QMatrix.Enabled := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_Visible(Value: WordBool);
begin
  QMatrix.Visible := Value;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ActiveColor: Integer;
begin
  Result := QMatrix.ActiveColor;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ActiveVisible: WordBool;
begin
  Result := fActiveVisible; //QMatrix.ActiveVisible;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ChannelColor: Integer;
begin
  Result := QMatrix.ChannelColor;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ChannelVisible: WordBool;
begin
  Result := fChannelVisible; //QMatrix.ChannelVisible;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ClusterColor: Integer;
begin
  Result := QMatrix.ClusterColor;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_ClusterVisible: WordBool;
begin
  Result := fClusterVisible; //QMatrix.ChannelVisible;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_DataView: Integer;
begin
  case fDataView of
    cvOnlyUncut: Result := 0;
    cvOnlyCut:   Result := 1;
  else  // cvCutAndUncut, cvCutAndUncutAdded
    Result := 2;
  end;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_DisplayMode: Integer;
begin
  case QMatrix.DisplayMode of
    // Coarse
    dmCalculateSCValues: Result := 0;
    // Fine
    dmValues:            Result := 1;
  else // dmCalculateSCValues
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_MatrixTyp: Integer;
begin
  case QMatrix.MatrixType of
    mtShortLongThin: Result := 0;
    mtSplice:        Result := 1;
    mtSiro:          Result := 2;
  else
    Result := 0;
  end;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_SpliceColor: Integer;
begin
  Result := QMatrix.SpliceColor;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_SpliceVisible: WordBool;
begin
  Result := fSpliceVisible; //QMatrix.SpliceVisible;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ActiveColor(Value: Integer);
begin
  QMatrix.ActiveColor := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ActiveVisible(Value: WordBool);
begin
  fActiveVisible        := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ChannelColor(Value: Integer);
begin
  QMatrix.ChannelColor := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ChannelVisible(Value: WordBool);
begin
  fChannelVisible        := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ClusterColor(Value: Integer);
begin
  QMatrix.ClusterColor := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_ClusterVisible(Value: WordBool);
begin
  fClusterVisible        := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_DataView(Value: Integer);
begin
  case Value of
    0: fDataView := cvOnlyUncut;
    1: fDataView := cvOnlyCut;
  else // 2
    fDataView := cvCutAndUncut;
  end;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_DisplayMode(Value: Integer);
begin
  case Value of
    // Coarse
    0: QMatrix.DisplayMode := dmCalculateSCValues;
    // Fine
    1: QMatrix.DisplayMode := dmValues;
  else
    QMatrix.DisplayMode := dmValues; //dmCalculateSCValues;
  end;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_MatrixTyp(Value: Integer);
begin
  case Value of
    0: QMatrix.MatrixType := mtShortLongThin;
    1: QMatrix.MatrixType := mtSplice;
    2: QMatrix.MatrixType := mtSiro;
  else
    QMatrix.MatrixType := mtShortLongThin;
  end;
  QMatrix.Invalidate;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_SpliceColor(Value: Integer);
begin
  QMatrix.SpliceColor := Value;
end;
//------------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_SpliceVisible(Value: WordBool);
begin
  fSpliceVisible := Value;
end;
//------------------------------------------------------------------------------
function TFloorQualityMatrix.Get_InactiveColor: Integer;
begin
  Result := QMatrix.InactiveColor;
end;

//:-----------------------------------------------------------------------------
procedure TFloorQualityMatrix.LoadFromStream(const Stream: IStream);
var
  xOleStream: TOleStream;
  xInt: Integer;
begin
  xOleStream := TOleStream.Create(Stream);
  try
    // erst eigene Daten lesen...
    xOleStream.ReadBuffer(xInt, sizeof(xInt));
    Set_DataView(xInt);  // setzt die Int-Zahl in die Enumeration um
    xOleStream.ReadBuffer(fActiveVisible,  sizeof(Boolean));
    xOleStream.ReadBuffer(fChannelVisible, sizeof(Boolean));
    xOleStream.ReadBuffer(fClusterVisible, sizeof(Boolean));
    xOleStream.ReadBuffer(fSpliceVisible,  sizeof(Boolean));
    //...und dann die Komponente
    xOleStream.ReadComponent(Control);
  finally
    xOleStream.Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TFloorQualityMatrix.SaveToStream(const Stream: IStream);
var
  xOleStream: TOleStream;
  xWriter: TWriter;
  xInt: Integer;
begin
  xOleStream := TOleStream.Create(Stream);
  try
    xWriter := TWriter.Create(xOleStream, 4096);
    try
      xWriter.IgnoreChildren := True;

      xInt := Get_DataView;
      // erste eigene Daten schreiben...
      xWriter.Write(xInt, sizeof(xInt));
      xWriter.Write(fActiveVisible,  sizeof(Boolean));
      xWriter.Write(fChannelVisible, sizeof(Boolean));
      xWriter.Write(fClusterVisible, sizeof(Boolean));
      xWriter.Write(fSpliceVisible,  sizeof(Boolean));
      //...dann die Komponente
      xWriter.WriteDescendent(Control, nil);
    finally
      xWriter.Free;
    end;
  finally
    xOleStream.Free;
  end;
end;

//:-----------------------------------------------------------------------------
procedure TFloorQualityMatrix.Set_InactiveColor(Value: Integer);
begin
  QMatrix.InactiveColor := Value;
end;
//:-----------------------------------------------------------------------------
procedure TFloorQualityMatrix.OnPrepareControlSize(Sender: TObject; var aWidth, aHeight: Integer);
var
  xFactor: Single;
  xImgHeight: Integer;
begin
  if (QMatrix.Width > 0) and (QMatrix.Height > 0) then begin
    QMatrix.CalculateMatrixSize;
    xFactor := QMatrix.Width / QMatrix.Height;

    xImgHeight := Trunc(aWidth / xFactor);
    if xImgHeight > aHeight then
      aWidth  := Trunc(aHeight * xFactor)
    else
      aHeight := xImgHeight;
  end;
end;
//:-----------------------------------------------------------------------------

initialization
  InitializeFloorObjectFactory(TFloorQualityMatrix, Class_QualityMatrix);
end.

