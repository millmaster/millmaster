{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: Basket.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: DLL for Floor
| Process(es)...: -
| Description...: Receives requests from Floor and fill result in shared memory provided
|                 from Floor
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  0.00  Wss | File created
| 03.04.2000  1.00  Wss | - Changed to support as Hostlink directly
                          - extra function for Trend selection implemented
                          - Database connection is kept alive: no create/free anymore.
| 17.05.2000  1.00  Wss | SFI/D calculation changed to SQR(SFI/SFICnt) * 100 / 145
| 29.05.2000  1.00  Wss | I.e. group = 40-49 -> missing dummy group 50-50
| 11.07.2000  1.00  Wss | Length filter implemented: len < 2000m = no data upload
| 23.08.2000  1.00  Wss | ShowLastEmptyGroup implemented
| 27.10.2000  1.00  Wss | At section request ProdName is added to MaName for DetailReport
| 04.02.2001  2.01  Wss | StyleColor, ShowStyleColor implemented
| 10.04.2001  2.01  Wss | - MaList filtered with c_node_id <> 0
                          - Check for c_data_collection for machine state
| 26.04.2001  2.01  Wss | - GrpData.gdStyleName implemented
| 24.08.2001  2.01  Wss | GetOfflimitValues: marker # added if field c_tOnlXXX is > 0
| 24.09.2001  2.02  Wss | DataItem Template added
| 13.11.2001  2.03  Wss | calculation for SFI/D corrected: SQR -> SQRT
| 12.12.2001  2.03  Wss | DataItem for day, week, month, year, SpdCount, Assortment and StartMode added
| 13.12.2001  2.03  Wss | DataItem YarnCount as text added
| 05.02.2002  2.03  Wss | DataItem SFI added
| 06.02.2002  2.03  Wss | CheckTimeSelection: reset time stamp out of if statement
| 28.02.2002  2.03  Wss | YarnCount nun als Float auf DB
| 09.04.2002  2.04  Wss | AddVirtualGroup: Set Stylename to NotInProduction for LotData
| 16.04.2002  2.04  Wss | For Floor the tWa, tRu, tOp has to be divided with SpindleCount
                          in Interval vew because Floor are showing group data for interval too
| 24.07.2002 2.04 Wss | Bei String Typ wurde die Gr�sse vom Konstantenarray nicht ber�cksichtigt
                        -> korrigiert, �berall, wo String abgef�llt wird vorher GetDIDSize aufrufen
| 16.09.2002 2.50 Wss | Auf ADO umgebaut
| 11.10.2002      LOK | EOF ADO Conform
| 23.10.2002 2.50 Wss | Offlimit tLst String formatiert ausgeben Sekunden -> h:mm
| 04.11.2002      LOK | Zugriff auf TMMSettingsReader nur noch mit TMMSettingsReader.Instance
| 22.11.2002      Wss | - StartMode auch als LotData erm�glicht
                        - bei fehlendem StartMode wird statt Leerstring -> Undefined gesendet
                        - DataItem diOffOutOfProd hinzugef�gt
| 02.12.2002 2.03 Wss | - DataItem diShiftInDay hinzugef�gt
| 22.01.2003 2.03 Wss | In GetMachineOfflimitValues werden Zahlen mit Format() mit 1 Kommastelle ausgegeben
| 12.02.2003 2.03 Wss | Fehlerprotokollierung in Execute Zeitabh�ngig gemacht. 1 Minute, bis weitere
                        Fehler ausgegeben werden. Fehler mit raise weitergeben.
| 30.06.2003      Wss | Offlimit Zeichen auf '*' ge�ndert, da '#' vielfach als 'Anzahl' interpretiert wird
| 12.01.2004      Wss | Unterst�tzung f�r QOfflimit Items entfernt
| 25.06.2004      Wss | In InsertDataItem wird beim String der Wert mit Trim() ausgelesen
| 18.01.2005      Wss | �berlauf bei tRu behoben -> wird nun in Single Typ ausgelesen
| 13.05.2005      Wss | Bug mit 12 Partien behoben. Es wurde immernoch am Schluss bei fehlenden Spindeln
                        eine virtuelle Partie eingef�gt und somit waren 13 Partien drin
                        -> Speicherplatz von n�chster Maschine wurde �berschrieben
| 29.09.2005 1.02 Wss | - Compiler Hint/Warnings bearbeitet
                        - Trend Items erweitert
| 21.09.2006 3.00 Nue | Uebernahme Nue: Neue SubDataItems f�r LotData added.
| 12.02.2008 3.01 Nue | Anpassen f�r V5.04: VCV-Zus�tze eingef�gt.
|=========================================================================================*}
unit Basket;

interface
uses
  Classes, Windows, mmThread, DataModul, HostlinkDef, AdoDBAccess,
  HostlinkShare, MMUGlobal, LoepfeGlobal, XMLDef;

type
  TExMemoryStream = class(TMemoryStream)
  public
    procedure WriteByte(aByte: Byte);
    procedure WriteLong(aLong: DWord);
    procedure WriteString(aStr: String);
    procedure WriteWord(aWord: Word);
  end;
  //.................................................................
  TRequestBasket = class(TObject)
  private
    mDataSelection: TDataSelection;
    mDM: TDMAccess;
    mDestinationOffset: DWord;
    mDIDDef: TDid;
    mDidRequest: PDidReqDescr;
    mDidRetDescrMem:  TExMemoryStream;
    mLastCheckMaState: Longword;
    mLastErrorLogTime: Longword;
    mLenBase: TLenBase;
    mMachNr: Word;
    mNrOfDidRetDescrMem: TExMemoryStream;
    mValueMem: TExMemoryStream;
    mWeekStartDay: Integer;
    mDisplayUnit: Single;
    mYarnCountUnit: TYarnUnit;
    fTrendPeriod: TDataSelection;
{$IFDEF MMLinkDummy}
    mMMLinkDummyItems: Integer;
    mSection: Word;
{$ENDIF}
    procedure CheckMachState;
    function GetAllColumnValuesFromDB(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
    function GetAllKeyValues(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
    function GetGroupData(aSubDid: Integer): Word;
    function GetMachName(aAllMachines: Boolean; aIndex: Word): Word;
    function GetMachState: Word;
    function GetMachStateText: Word;
    function GetMachStopGrp: Word;
    function GetMachineOfflimitValues(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
    function GetProdGroupColor(aProdGrpIndex: Integer): Word;
//    function GetQOfflimitValues(aDid: TDidItems; aMachID: Integer; aQOfflimitList: TQOfflimitReader): Word;
    function GetTrendValues(aTrend, aMachID: Word; aProdID: DWord): Word;
    function InsertDataItem(aDataTyp: TFloorDataTyp; aValue: Variant; aSize: Word = 0): Word;
    function InsertEmptyItem(aDataTyp: TFloorDataTyp): Word;
  protected
    function Assemble(var aMaxSize: DWord): Boolean;
    procedure CleanUp;
    procedure Request;
  public
    constructor Create(aDataModul: TDMAccess); virtual;
    destructor Destroy; override;
    function Execute(aDidRequest: PByte; aMaxSize: DWord): Boolean;
    property TrendPeriod: TDataSelection read fTrendPeriod write fTrendPeriod;
  end;
  //.................................................................

var
  lDebugTrend: Boolean;
  lDebugDID:   Boolean;
  lDebugRequest: Boolean;

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  mmCS, Dialogs, Graphics, SysUtils,
  mmEventLog, mmList, SettingsReader, BaseGlobal;

//******************************************************************************
// TExMemoryStream }
//******************************************************************************
procedure TExMemoryStream.WriteByte(aByte: Byte);
begin
  WriteBuffer(aByte, 1);
end;
//------------------------------------------------------------------------------
procedure TExMemoryStream.WriteLong(aLong: DWord);
begin
  WriteBuffer(aLong, 4);
end;
//------------------------------------------------------------------------------
procedure TExMemoryStream.WriteString(aStr: String);
begin
  WriteBuffer(aStr, Length(aStr)+1);
end;
//------------------------------------------------------------------------------
procedure TExMemoryStream.WriteWord(aWord: Word);
begin
  WriteBuffer(aWord, 2);
end;
//------------------------------------------------------------------------------

//******************************************************************************
// TRequestBasket
//******************************************************************************
function TRequestBasket.Assemble(var aMaxSize: DWord): Boolean;
var
  xByte: PByte;
begin
  Result := False;
  // copy complete description of return baskets in main stream.
  xByte  := mDidRetDescrMem.Memory;
  mNrOfDidRetDescrMem.WriteBuffer(xByte^, mDidRetDescrMem.Size);

  // copy complete data section of return baskets in main stream.
  xByte  := mValueMem.Memory;
  mNrOfDidRetDescrMem.WriteBuffer(xByte^, mValueMem.Size);

  xByte := PByte(DWord(mDidRequest) + mDestinationOffset);
  mNrOfDidRetDescrMem.Position := 0;
  // copy MainSect memory stream to shared memory pointer from Floor only
  // if shared memory is large enough
{$IFDEF MMLinkDummy}
  if mNrOfDidRetDescrMem.Size >= (aMaxSize - mDestinationOffset) then begin
    aMaxSize := mNrOfDidRetDescrMem.Size + mDestinationOffset;
  end else
{$ELSE}
  if mNrOfDidRetDescrMem.Size > LongInt((aMaxSize - mDestinationOffset)) then begin
    gEventLog.Write(etWarning, Format('TRequestBasket.Assemble: Basket size to large: need=%d, actual=%d', [mNrOfDidRetDescrMem.Size, aMaxSize]));
    aMaxSize := DWord(mNrOfDidRetDescrMem.Size) + mDestinationOffset;
  end else
{$ENDIF}
  try
    CodeSite.SendInteger('Return Basket Size:', DWord(mNrOfDidRetDescrMem.Size) + mDestinationOffset);
    mNrOfDidRetDescrMem.ReadBuffer(xByte^, mNrOfDidRetDescrMem.Size);
    Result := True;
  except
    gEventLog.Write(etError, Format('TRequestBasket.Assemble: %s', [GetLastErrorText]));
  end;

  CleanUp;
end;
//------------------------------------------------------------------------------
procedure TRequestBasket.CheckMachState;
var
  xMach: PMachineRec;
  i: Integer;
  xMachNr: Integer;
  xList: TmmList;
  xGrpIndex: Integer;
  xStep: Integer;
  //----------------------------------------------------------------------------
  function AddVirtualGroup(aSpdFirst, aSpdLast: Word): PProdGrpRec;
  begin
    new(Result);
    with Result^ do begin
      ProdID     := 0;
      ProdStart  := 0;
      GrpColor   := cVirtualStyleColor;
      StyleColor := cVirtualStyleColor;
      SpdFirst   := aSpdFirst;
      SpdLast    := aSpdLast;
      StartMode  := 0;  // Undefinded
      GrpName    := cVirtualStyleName;
      StyleName  := cVirtualStyleName;
    end;
  end;
  //----------------------------------------------------------------------------
  function GetProdGroups(aDataSet: TNativeAdoQuery): Integer;
  var
    xPGrp: PProdGrpRec;
    xNextSpdFirst: Word;
    xSpdFirst: Word;
    xMaxVirtualGrps: Integer;
  begin
    // now go through all production groups and find some undefined ranges
    xMaxVirtualGrps := cMaxLotCount;
    xNextSpdFirst := 1;
    with aDataSet do begin
      // FIRST: override machine state with production state
      if FindFirst then begin
        Dec(xMaxVirtualGrps, Recordset.RecordCount);
        while not EOF do  // ADO Conform
        try
          xSpdFirst := FieldByName('c_spindle_first').AsInteger;
          // if SpdFirst is other than expected and Floor has to show empty groups
          if (xSpdFirst <> xNextSpdFirst) and gMachConfig.ShowEmptyProdGroup and (xMaxVirtualGrps > 0) then begin
            // OOPS! a hole is detected -> create virtual production group for Floor
            xPGrp := AddVirtualGroup(xNextSpdFirst, FieldByName('c_spindle_First').AsInteger - 1);
            dec(xMaxVirtualGrps);
          end else begin
            new(xPGrp);
            with xPGrp^ do begin
              ProdID     := FieldByName('c_prod_id').AsInteger;
              ProdStart  := FieldByName('c_prod_start').AsDateTime;
              GrpColor   := FieldByName('GrpColor').AsInteger;
              StyleColor := FieldByName('StyleColor').AsInteger;
              SpdFirst   := xSpdFirst;
              SpdLast    := FieldByName('c_spindle_last').AsInteger;
              StartMode  := FieldByName('c_start_mode').AsInteger;
              GrpName    := FieldByName('c_prod_name').AsString;
              StyleName  := FieldByName('c_style_name').AsString;
            end;
            Next;
          end;
          if Assigned(xPGrp) then begin
            xList.Add(xPGrp);
            xNextSpdFirst := xPGrp^.SpdLast + 1;
          end;
        except
        end;
      end;  // if
    end;  // with

    // now check if Floor has to show empty groups and add hole at end of machine
    if (xNextSpdFirst <= xMach^.NrOfSpindles) and (xMaxVirtualGrps > 0) and
       ((xList.Count = 0) or gMachConfig.ShowEmptyProdGroup) then begin
      xPGrp := AddVirtualGroup(xNextSpdFirst, xMach^.NrOfSpindles);
      xList.Add(xPGrp);
    end;

    Result := xList.Count;
  end;
  //----------------------------------------------------------------------------
begin
{$IFDEF MMLinkDummy}
  for xMachNr:=0 to gMachConfig.NrOfMachines-1 do begin
    xMach := gMachConfig.MachRec[xMachNr];
    xMach^.NrOfProdGrp := 6;
    xStep := 1;
    for i:=1 to xMach^.NrOfProdGrp do begin
      with xMach^.ProdGrp[i] do begin
        ProdID := i + ((xMachNr-1) * 6);
        SpdFirst  := xStep;
        SpdLast   := SpdFirst + 9;
        GrpName   := Format('ProdGrp %d', [i]);
        StyleName := Format('Style %d', [i]);
      end;
      inc(xStep, 10);
    end;
  end;
{$ELSE}
  xList := TmmList.Create;
  try
    //
    // FIRST: check for machine state in t_net_node table
    // c_node_stat from t_net_node: 0,1,2: (nsOffline, nsOnline, nsNotValid)
    // TMachState = (msUndefined, msRunning, msFiller, msOffline, msInSynchronisation);
    //
    if mDM.PrepareMachineQuery then begin
      with mDM.ResultQuery do begin
        xMachNr := 0;
        while not EOF do begin    // ADO Conform
          xMach := gMachConfig.MachRec[xMachNr];
{}
          xMach^.State := TMachState(FieldByName('c_machine_state').AsInteger);
          if (xMach^.State <> msOffline) and (FieldByName('c_data_collection').AsInteger = 0) then
            xMach^.State := msFiller;
{
          if FieldByName('c_node_stat').AsInteger = 0 then
            xMach^.State := msOffline
          else if FieldByName('c_data_collection').AsInteger = 1 then
            xMach^.State := msRunning
          else
            xMach^.State := msFiller;
{}
          Next;
          inc(xMachNr);
        end;
        Close;
      end;
    end;
    //
    // SECOND: check for machine/prodgrp state from t_prodgroup_state
    //
    for xMachNr:=0 to gMachConfig.NrOfMachines-1 do begin
      xMach := gMachConfig.MachRec[xMachNr];
      if mDM.PrepareProdGrpQuery(xMach^.MachID) then begin
        xMach^.NrOfProdGrp := GetProdGroups(mDM.ResultQuery);
        mDM.ResultQuery.Close;
        // initialize variables depending of Left/Right head machine
        if xMach^.HeadLeft then begin
          i := 1;
          xStep := 1;
        end else begin
          i := xMach^.NrOfProdGrp;
          xStep := -1;
        end;

        // copy production groups dependent of Left/Right machine
        for xGrpIndex:=0 to xList.Count-1 do begin
          xMach^.ProdGrp[i] := PProdGrpRec(xList.Items[xGrpIndex])^;
          inc(i, xStep);
        end;

        // all other production group ID set to 0 and clear name
        for i:= xMach^.NrOfProdGrp+1 to cMaxLotCount do begin
          xMach^.ProdGrp[i].ProdID    := 0;
          xMach^.ProdGrp[i].GrpName   := ' ';
          xMach^.ProdGrp[i].StyleName := ' ';
        end;
        // clean up temporarly created production group info list
        while xList.Count > 0 do begin
          Dispose(PProdGrpRec(xList.Items[0]));
          xList.Delete(0);
        end;
      end else  // if mDM.
        CodeSite.SendString('PrepareProdGrpQuery failed', mDM.MMError);
    end;  // for xMachNr
  finally
  end;  // with
  xList.Free;
{$ENDIF}
end;
//------------------------------------------------------------------------------
procedure TRequestBasket.CleanUp;
begin
  mNrOfDidRetDescrMem.Clear;
  mDidRetDescrMem.Clear;
  mValueMem.Clear;
end;
//------------------------------------------------------------------------------
constructor TRequestBasket.Create(aDataModul: TDMAccess);
begin
  inherited Create;
  fTrendPeriod      := dsLastWeek; //dsIntervalData;
  mDataSelection    := dsNone;
  mDM               := aDataModul;
  mLastCheckMaState := 0;
  mLastErrorLogTime := 0;

  // memory map returned baskets
  {** Header section ***********************************************
   | TDidReqDescr: mDidRequest
   |................................................................
   |- mNrOfDidRetDescrMem ----------------------------------------------------
   |
   | Memoryblock with Word: count of returned DataItems for each
   | returned Basket
   |
   | Count: Word
   |
   |-mDidRetDescrMem ------------------------------------------------------
   |
   | Memoryblock with TDidRetDescr: each DataItem is described here
   |
   | Data.Did:    Word (from TDid)
   | Data.SubDid: Word (from TDid)
   | DataPos:     DWord;
   |
   |- mValueMem ----------------------------------------------------
   |
   | Memoryblock with DataItems: all DataItems where written
   | continuously in this section:
   |
   | DataTyp: Byte
   | DataLen: Word
   | Data:    DataLen count of bytes (on strings inclusive #0)
   |
   *****************************************************************}

  mNrOfDidRetDescrMem := TExMemoryStream.Create;
  mDidRetDescrMem     := TExMemoryStream.Create;
  mValueMem           := TExMemoryStream.Create;

{$IFDEF MMLinkDummy}
  mDisplayUnit  := 100000;
  mWeekStartDay := 2;
{$ELSE}
  with TMMSettingsReader.Instance do begin
    Init;
    mWeekStartDay  := Value[cStartDayOfWeek] + 1; // on DB value base at 0 -> ISO = 1
    mYarnCountUnit := Value[cYarnCntUnit];
    mLenBase       := Value[cMMUnit];
    case mLenBase of
      // 100km
      lb100KM: mDisplayUnit := 100000;
      // 1000km
      lb1000KM: mDisplayUnit := 1000000;
      // 100'000yrd
      lb100000Y: mDisplayUnit := 100000 * cYdToM;
      // 1'000'000yrd
      lb1000000Y: mDisplayUnit := 1000000 * cYdToM;
    else // Absolute
      mDisplayUnit := 1;
    end;
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
destructor TRequestBasket.Destroy;
begin
  CleanUp;

  mValueMem.Free;
  mDidRetDescrMem.Free;
  mNrOfDidRetDescrMem.Free;

  // if mDidRequest isn't set to Nil pointer Delphi means the dynamic array contains
  // vaild data and would like to clear but the memory is managed from Floor and Delphi
  // is forbidden to touch this

  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TRequestBasket.Execute(aDidRequest: PByte; aMaxSize: DWord): Boolean;
var
  xMaxSize: DWord;
begin
  // set pointer of dynamic array to shared memory of Floor
  PByte(mDidRequest) := aDidRequest;
  // get some basic information
  mDestinationOffset := mDidRequest^.DLPos;

  xMaxSize := aMaxSize;
  Result := False;
  try
    Request;
    Result := Assemble(xMaxSize);
  except
    on e:Exception do begin
      if (GetTickCount - mLastErrorLogTime) > 60000 then begin
        gEventLog.Write(etError, Format('TRequestBasket.Execute: %s', [e.Message]));
        mLastErrorLogTime := GetTickCount;
      end;
    end;
  end;
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetAllColumnValuesFromDB(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
var
{$IFDEF MMLinkDummy}
  i, j: Integer;
{$ENDIF}
  xFieldName: String;
  xStr: String;
  xInt: Integer;
  xDataTyp: TFloorDataTyp;
  xSingle: Single;
  xValue: Variant;
begin
  Result := 0;
  xFieldName := GetDIDColName(aDid);
  xDataTyp    := GetDidType(aDid);
{$IFDEF MMLinkDummy}
  for i:=1 to mMMLinkDummyItems do begin
    case aDid of
      ditRt: case mMachNr of
               0..3: inc(Result, InsertDataItem(xDataTyp, 70273));
               7:       inc(Result, InsertDataItem(xDataTyp, 1574));
             else
               inc(Result, InsertDataItem(xDataTyp, (LongInt(aDid) * mMachNr) div 8));
             end;
      ditOt: case mMachNr of
               0..3: inc(Result, InsertDataItem(xDataTyp, 75113));
               7:       inc(Result, InsertDataItem(xDataTyp, 638));
             else
               inc(Result, InsertDataItem(xDataTyp, (LongInt(aDid) * mMachNr) div 8));
             end;
      diProd_Name: begin
          if mMachNr = cAllValues then
            mMachNr := 0;
          if mSection = 0 then
            for j:=1 to 6 do
              inc(Result, InsertDataItem(xDataTyp, String(gMachConfig.MachRec[mMachNr].ProdGrp[j].GrpName)))
          else
            inc(Result, InsertDataItem(xDataTyp, String(gMachConfig.MachRec[mMachNr].ProdGrp[mSection].GrpName)));
        end;
      diLen: if i mod 4 = 0 then
               inc(Result, InsertDataItem(xDataTyp, 300))
             else
               inc(Result, InsertDataItem(xDataTyp, 0));
      diCYTot: if i mod 4 = 0 then
                 inc(Result, InsertDataItem(xDataTyp, 18))
               else
                 inc(Result, InsertDataItem(xDataTyp, 0));
      diSpindle_Range:
          inc(Result, InsertDataItem(xDataTyp, GetDIDName(aDid) + IntToStr(i {mod 6{})));
      diDisplayUnit: begin
          if i mod 4 = 0 then begin
            if mDisplayUnit < 2 then // absolute is selected in MMConfiguration
              inc(Result, InsertDataItem(fdtSingle, 300))
            else
              inc(Result, InsertDataItem(fdtSingle, mDisplayUnit));
          end else
            inc(Result, InsertDataItem(fdtSingle, 0));
        end;
    else
      case xDataTyp of
        fdtByte..fdtDWord:
          inc(Result, InsertDataItem(xDataTyp, aDid));

        fdtChar, fdtString:
          inc(Result, InsertDataItem(xDataTyp, GetDIDName(aDid)));

        fdtSingle, fdtDouble:
          inc(Result, InsertDataItem(xDataTyp, Integer(aDid) * 1.0));

        fdtDateTime, fdtOLEDateTime:
          inc(Result, InsertDataItem(xDataTyp, Now));
      else
      end;  //case
    end;
  end;
//  Sleep(100);
{$ELSE}
  if aDataSet.FindFirst and (xFieldName <> '') then begin
    with aDataSet do
      while not EOF do  // ADO Conform
      try
        case aDid of
          ditRt, ditOt, ditWt: begin
              xSingle := FieldByName(xFieldName).AsFloat;
              if mDataSelection = dsIntervalData then
                xSingle := xSingle / (FieldByName('c_spindle_last').AsInteger - FieldByName('c_spindle_first').AsInteger + 1);
              inc(Result, InsertDataItem(xDataTyp, xSingle));
            end;
          diSFI_D:
              inc(Result, InsertDataItem(xDataTyp, CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat)));
          diSFI: begin
              xInt := FieldByName('c_AdjustBaseCnt').AsInteger;
              if xInt > 0 then begin
                xSingle := FieldByName('c_AdjustBase').AsFloat / xInt;
                inc(Result, InsertDataItem(xDataTyp, CalcSFI(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat, xSingle)));
              end else
                inc(Result, InsertEmptyItem(xDataTyp));
            end;
          diCVd:  //Nue:12.02.08
              inc(Result, InsertDataItem(xDataTyp, cVCVFactor*CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat)));
          diProd_End:
              if FieldByName(GetDIDColName(diProd_Start)).AsDateTime = FieldByName(xFieldName).AsDateTime then
                inc(Result, InsertEmptyItem(xDataTyp))
              else
                inc(Result, InsertDataItem(xDataTyp, FieldByName(xFieldName).Value));
          diDisplayUnit: begin
              if mDisplayUnit < 2 then  // absolute is selected in MMConfiguration
                inc(Result, InsertDataItem(xDataTyp, FieldByName('c_Len').Value))
              else
                inc(Result, InsertDataItem(fdtSingle, mDisplayUnit));
            end;
          diSpindle_Range: begin
              xStr := Format('%d - %d', [FieldByName('c_spindle_first').AsInteger, FieldByName('c_spindle_last').AsInteger]);
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diSpindleCount: begin
              xInt := FieldByName('c_spindle_last').AsInteger - FieldByName('c_spindle_first').AsInteger + 1;
              inc(Result, InsertDataItem(xDataTyp, xInt));
            end;
          diYM_set_name: begin
              xStr := FieldByName(xFieldName).AsString;
              if Trim(xStr) = '' then
                xStr := Format(cNoYMNamePrefix, [FieldByName('c_YM_set_id').AsInteger]);
              inc(Result, InsertDataItem(fdtString, xStr, GetDIDSize(aDid)));
            end;
          diDay: begin
              xStr := FormatDateTime('dd', FieldByName('c_Time').AsDateTime);
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diWeek: begin
              xStr := IntToStr(MMWeekNumber(FieldByName('c_Time').AsDateTime, mWeekStartDay));
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diMonth: begin
              xStr := FormatDateTime('mm', FieldByName('c_Time').AsDateTime);
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diYear: begin
              xStr := FormatDateTime('yyyy', FieldByName('c_Time').AsDateTime);
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diStart_Mode: begin
              xStr := cProdGrpStartModeArr[TProdGrpStartMode(FieldByName(xFieldName).AsInteger)];
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
          diAct_yarncnt: begin
              xStr := Format('%.1f', [YarnCountConvert(yuNm, mYarnCountUnit, FieldByName(xFieldName).AsFloat)]);
              inc(Result, InsertDataItem(xDataTyp, xStr, GetDIDSize(aDid)));
            end;
{$IFDEF NewItems}
{$ENDIF}
        else
          with FieldByName(xFieldName) do begin
            if IsNull then xValue := Unassigned
                      else xValue := Value;
          end;
          inc(Result, InsertDataItem(xDataTyp, xValue, GetDIDSize(aDid)));
        end;
        Next;
      except
        on e:Exception do begin
          gEventLog.Write(etError, Format('TRequestBasket.GetAllColumnValuesFromDB: %s', [e.Message]));
        end;
      end;
  end else begin
    inc(Result, InsertEmptyItem(xDataTyp));
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetAllKeyValues(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
var
{$IFDEF MMLinkDummy}
  i, j: Integer;
{$ENDIF}
  xFieldName: String;
  xDataTyp: TFloorDataTyp;
  xStr: String;
begin
  Result := 0;
  xFieldName := GetDIDColName(aDid);
  xDataTyp   := GetDidType(aDid);
{$IFDEF MMLinkDummy}
  for i:=1 to mMMLinkDummyItems do begin
    case aDid of
      diProd_Name: begin
          if mMachNr = cAllValues then
            mMachNr := 0;
          if mSection = 0 then
            for j:=1 to 6 do
              inc(Result, InsertDataItem(xDataTyp, String(gMachConfig.MachRec[mMachNr].ProdGrp[j].GrpName)))
          else
            inc(Result, InsertDataItem(xDataTyp, String(gMachConfig.MachRec[mMachNr].ProdGrp[mSection].GrpName)));
        end;
    else
      case xDataTyp of
        fdtByte..fdtDWord:
          inc(Result, InsertDataItem(xDataTyp, aDid));

        fdtChar, fdtString:
          inc(Result, InsertDataItem(xDataTyp, GetDIDName(aDid)));

        fdtSingle, fdtDouble:
          inc(Result, InsertDataItem(xDataTyp, Integer(aDid) * 1.0));

        fdtDateTime, fdtOLEDateTime:
          inc(Result, InsertDataItem(xDataTyp, Now));
      else
      end;  //case
    end;
  end;
//  Sleep(100);
{$ELSE}
  if aDataSet.FindFirst and (xFieldName <> '') then begin
    with aDataSet do
      while not EOF do  // ADO Conform
      try
        case aDid of
          diSFI_D:
              if FieldByName('c_SFICnt').AsInteger = 0 then
                inc(Result, InsertDataItem(xDataTyp, 0.0))
              else
                inc(Result, InsertDataItem(xDataTyp, SQR(FieldByName('c_SFI').AsFloat / FieldByName('c_SFICnt').AsInteger) * 100 / 145));
          diCVd:  //Nue:26.06.08
              if FieldByName('c_SFICnt').AsInteger = 0 then
                inc(Result, InsertDataItem(xDataTyp, 0.0))
              else
                inc(Result, InsertDataItem(xDataTyp, cVCVFactor*SQR(FieldByName('c_SFI').AsFloat / FieldByName('c_SFICnt').AsInteger) * 100 / 145));
          diProd_End:
              if FieldByName(GetDIDColName(diProd_Start)).AsDateTime = FieldByName(xFieldName).AsDateTime then
                inc(Result, InsertEmptyItem(xDataTyp))
              else
                inc(Result, InsertDataItem(xDataTyp, FieldByName(xFieldName).Value));
          diYM_set_name: begin
              xStr := FieldByName(xFieldName).AsString;
              if Trim(xStr) = '' then
                xStr := Format(cNoYMNamePrefix, [FieldByName('c_YM_set_id').AsInteger]);
              inc(Result, InsertDataItem(fdtString, xStr, GetDIDSize(aDid)));
            end;
        else
          inc(Result, InsertDataItem(xDataTyp, FieldByName(xFieldName).Value, GetDIDSize(aDid)));
        end;  // case
        Next;
      except
        on e:Exception do begin
          gEventLog.Write(etError, Format('TRequestBasket.GetAllKeyValues: %s', [e.Message]));
        end;
      end;
  end else begin
    inc(Result, InsertEmptyItem(xDataTyp));
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetGroupData(aSubDid: Integer): Word;
var
  i: Integer;
  xStr: String;
  xTmp: String;
  xSingle: Single;
begin
  xStr := '';
{$IFDEF MMLinkDummy}
  with gMachConfig.MachRec[mMachNr]^ do begin
    for i:=1 to NrOfProdGrp do begin
      case TLotData(aSubDid) of
        ldLot: begin
            xTmp := ProdGrp[i].GrpName;
          end;
        ldSpdRange: begin
            xTmp := Format('%.02d - %.02d', [ProdGrp[i].SpdFirst, ProdGrp[i].SpdLast]);
          end;
        ldEff: begin
            xTmp := Format('%.1f', [90.0 + (i / 10)]);
          end;
        ldStyle: begin
            xTmp := ProdGrp[i].StyleName;
          end;
        ldStartMode: begin
            xTmp := ProdGrp[i].StartMode;
//StartNue:13.9.06
        ldProdStart: begin
            xTmp := DateTimeToStr(ProdGrp[i].ProdStart);
          end;
        ldCYTot: begin
            xTmp := Format('%.1f', [1234.7]);
          end;
        ldSP: begin
            xTmp := Format('%.1f', [123.1]);
          end;
        ldCSP: begin
            xTmp := Format('%.1f', [12.7]);
          end;
        ldProdKg: begin
            xTmp := Format('%.1f', [567.7]);
          end;
        ldCL: begin
            xTmp := Format('%.1f', [12.7]);
          end;
        ldCN: begin
            xTmp := Format('%.1f', [13.2]);
          end;
        ldCS: begin
            xTmp := Format('%.1f', [17.7]);
          end;
        ldCT: begin
            xTmp := Format('%.1f', [9.2]);
          end;
        ldCF: begin
            xTmp := Format('%.1f', [19.3]);
          end;
        ldINEPSkm: begin
            xTmp := Format('%.1f', [11.7]);
          end;
        ldISMALm: begin
            xTmp := Format('%.1f', [2.4]);
          end;
        ldITHICKkm: begin
            xTmp := Format('%.1f', [17.8]);
          end;
        ldITHINkm: begin
            xTmp := Format('%.1f', [23.7]);
          end;
//End Nue:13.9.06
      else
        xTmp := '';
      end;
      // %-*.*s = placeholder for formating string size as min/max from cMaxGroupData
      xStr := Format('%s %-*.*s', [xStr, cMaxGroupData, cMaxGroupData, xTmp]);
    end;  // for
  end;  // with
{$ELSE}
  with gMachConfig.MachRec[mMachNr]^ do begin
    for i:=1 to NrOfProdGrp do begin
      case TLotData(aSubDid) of
        ldLot: begin
            xTmp := ProdGrp[i].GrpName;
          end;
        ldSpdRange: begin
            xTmp := Format('%.02d - %.02d', [ProdGrp[i].SpdFirst, ProdGrp[i].SpdLast]);
          end;
//        ldEff: begin
//            xSingle := 0;
//            if mDM.PrepareGroupDataQuery(ProdGrp[i].ProdID) then begin
//              with mDM.ResultQuery do
//              try
//                if FindFirst then begin
//                  xSingle := Int(FieldByName('c_wtSpd').AsFloat);
//                  if Round(xSingle) > 0 then
//                    xSingle := (FieldByName('c_rtSpd').AsFloat * 100.0) / xSingle;
//                end;
//              except
//                on e:Exception do begin
//                  CodeSite.SendError('Read ProdGroupQuery: ' + e.Message);
//                end;
//              end;
//            end;
//            xTmp := Format('%.1f', [xSingle]);
//          end;
        ldStyle: begin
            xTmp := ProdGrp[i].StyleName;
          end;
        ldStartMode: begin
            xTmp := cProdGrpStartModeArr[TProdGrpStartMode(ProdGrp[i].StartMode)];
          end;
//StartNue:13.9.06
        ldProdStart: begin
            xTmp := DateTimeToStr(ProdGrp[i].ProdStart);
          end;

        ldCYTot,ldEff,ldSP,ldCSP,ldProdKg,ldCL,ldCN,ldCS,ldCT,ldCF,ldINEPSkm,ldISMALm,ldITHICKkm ,ldITHINkm: begin
            xSingle := 0;
            if mDM.PrepareGroupDataQuery(ProdGrp[i].ProdID) then begin
              with mDM.ResultQuery do
              try
                if FindFirst then begin
                  case TLotData(aSubDid) of
                    ldEff: begin
                        xSingle := Int(FieldByName('c_wtSpd').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := (FieldByName('c_rtSpd').AsFloat * 100.0) / xSingle;
                    end;
                    ldCYTot: begin
                        xSingle := Int(FieldByName('c_CYTot').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                    end;
                    ldSP: begin
                        xSingle := Int(FieldByName('c_SP').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldCSP: begin
                        xSingle := Int(FieldByName('c_CSp').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldProdKg: begin
                        xSingle := Int(FieldByName('c_kg').AsFloat);
                      end;
                    ldCL: begin
                        xSingle := Int(FieldByName('c_CL').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldCN: begin
                        xSingle := Int(FieldByName('c_CN').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldCS: begin
                        xSingle := Int(FieldByName('c_CS').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldCT: begin
                        xSingle := Int(FieldByName('c_CT').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldCF: begin
                        xSingle := Int(FieldByName('c_CSIRO').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := xSingle / mDisplayUnit;
                      end;
                    ldINEPSkm: begin
                        xSingle := Int(FieldByName('c_len').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := (FieldByName('c_INeps').AsFloat * 1000.0) / xSingle;
                      end;
                    ldISMALm: begin
                        xSingle := Int(FieldByName('c_len').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := (FieldByName('c_ISmall').AsFloat) / xSingle;
                      end;
                    ldITHICKkm: begin
                        xSingle := Int(FieldByName('c_len').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := (FieldByName('c_IThick').AsFloat * 1000.0) / xSingle;
                      end;
                    ldITHINkm: begin
                        xSingle := Int(FieldByName('c_len').AsFloat);
                        if Round(xSingle) > 0 then
                          xSingle := (FieldByName('c_IThin').AsFloat * 1000.0) / xSingle;
                      end;
                  end; //case TLotData(aSubDid) of

                end; //FindFirst
              except
                on e:Exception do begin
                  CodeSite.SendError('Read ProdGroupQuery: ' + e.Message);
                end;
              end;
            end;
            xTmp := Format('%.1f', [xSingle]);
          end;
//End Nue:13.9.06
      else
        xTmp := '';
      end; // case
      // %-*.*s = placeholder for formating string size as min/max from cMaxGroupData
      xStr := Format('%s %-*.*s', [xStr, cMaxGroupData-1, cMaxGroupData-1, xTmp]);
    end;  // for
  end;  // with
{$ENDIF}
  CodeSite.SendFmtMsg('Lot: %d, %s', [Length(Trim(xStr)), xStr]);
  Result := InsertDataItem(fdtString, Trim(xStr), GetDIDSize(diLotData));
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetMachName(aAllMachines: Boolean; aIndex: Word): Word;
var
  i: Word;
  xStr: String;
begin
  if aAllMachines then begin
    Result := gMachConfig.NrOfMachines;
    for i:=0 to Result-1 do
      InsertDataItem(fdtString, gMachConfig.MachRec[i]^.Name, GetDIDSize(diMachine_Name));
  end else begin
    Result := 1;
    xStr := gMachConfig.MachRec[mMachNr]^.Name;
    InsertDataItem(fdtString, xStr, GetDIDSize(diMachine_Name));
  end;
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetMachState: Word;
begin
  Result := 1;
  InsertDataItem(fdtWord, gMachConfig.MachRec[mMachNr]^.State);
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetMachStateText: Word;
var
  i: Word;
begin
  with cSubDIDList[slMachineState] do begin
    Result := SubDidCount + 2;

    InsertDataItem(fdtString, Title, GetDIDSize(diDIDStatusTexts));

    i := SubDidCount;
    InsertDataItem(fdtWord, i);

    for i:=0 to SubDidCount-1 do
      InsertDataItem(fdtString, SelInfo[i], 0);
  end;
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetMachStopGrp: Word;
begin
  Result := 1;
  InsertDataItem(fdtByte, 0);
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetProdGroupColor(aProdGrpIndex: Integer): Word;
var
  xColor: TColor;
begin
{$IFDEF MMLinkDummy}
  xColor := cProdGroupPalette[aProdGrpIndex];
{$ELSE}
  with gMachConfig do
    if ShowStyleColor then
      xColor := gMachConfig.MachRec[mMachNr]^.ProdGrp[aProdGrpIndex].StyleColor
    else
      xColor := gMachConfig.MachRec[mMachNr]^.ProdGrp[aProdGrpIndex].GrpColor;
{$ENDIF}
  Result := InsertDataItem(fdtLong, xColor)
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetTrendValues(aTrend, aMachID: Word; aProdID: DWord): Word;
type
  TTrendValue = record
    Value: Single;
    Date: TDateTime;
  end;

var
  i: Integer;
  xCount: Integer;
  xTrend: TTrendItems;
  xStep: Word;
  xData: Array of TTrendValue;
  xStr: String;
  xStandard: Single;
  xTrendCount: Integer;
  xValue: Single;
  xLen: Double;
  xColName: string;
{$IFDEF MMLinkDummy}
  xDateTime: TDateTime;
  xDouble: Double;
  xSingle: Single;
{$endif}
  //..................................................................
//  function CalcField(aSrcField, aLenField: TField; aFactor: Single): Single;
//  begin
//    if aLenField.AsInteger = 0 then
//      Result := 0.0
//    else
//      Result := (aSrcField.AsInteger * aFactor) / aLenField.AsInteger;
//  end;
  //..................................................................
begin
  Result := 2;
  xTrend      := TTrendItems(aTrend-1);
  xColName    := GetTrendColName(xTrend);
  xStandard   := 0;
  xTrendCount := 0;

{$IFDEF MMLinkDummy}
  xDateTime := Now;
  xTrendCount := 50;
  SetLength(xData, xTrendCount);
  xStandard := 100;
  for i:=xTrendCount-1 downto 0 do begin
    if i = 25 then
      xData[i].Value := (i+1) * aTrend * 5
    else
      xData[i].Value := (i+1) * aTrend;
    xData[i].Date  := xDateTime;
    xDateTime := xDateTime - 1;
  end;
{$ELSE}
  with mDM do begin
    if PrepareTrendQuery(aMachID, aProdID, TrendPeriod) then begin
      xCount := 0;
      with ResultQuery do
      try
        if FindFirst then begin
          while not EOF do begin  // ADO Conform
            inc(xCount);
            SetLength(xData, xCount);
            xLen := FieldByName('c_len').AsFloat;
            if xLen >= cMinSpuledLen then begin
              // values are calculated online instead with OnCaldFields at TQuery
{
    tiI2_4, tiI4_8, tiI8_20, tiI20_70,
    tiCYTot, tiCN, tiCS, tiCL, tiCT, tiCSp, tiCCl, tiCFF,
    tiYB, tiWeight, tiCones,
    tiSP, tiRSp, tiEffRSp, tiEffCSp
{}
              case xTrend of
                tiSFI_D:
                    xValue := CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat);

                tiSFI: begin
                    i := FieldByName('c_AdjustBaseCnt').AsInteger;
                    if i > 0 then begin
                      xValue := FieldByName('c_AdjustBase').AsFloat / i;
                      xValue := CalcSFI(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat, xValue);
                    end else
                      xValue := 0.0;
                  end;
                tiCVd:   //Nue:12.02.08
                    xValue := cVCVFactor*CalcSFI_D(FieldByName('c_SFI').AsFloat, FieldByName('c_SFICnt').AsFloat);

                tiEffM:
                    if FieldByName('c_otSpd').AsInteger = 0 then
                      xValue := 0.0
                    else
                      xValue := (FieldByName('c_rtSpd').AsFloat * 100.0) / FieldByName('c_otSpd').AsInteger;

                tiEffP:
                    if FieldByName('c_wtSpd').AsInteger = 0 then
                      xValue := 0.0
                    else
                      xValue := (FieldByName('c_rtSpd').AsFloat * 100.0) / FieldByName('c_wtSpd').AsInteger;

                tiINeps, tiISmall, tiIThick, tiIThin,
                tiI2_4, tiI4_8, tiI8_20, tiI20_70: begin
                    xValue := CalcValue(mLenBase, FieldByName(xColName).AsFloat, xLen, GetTrendFactor(xTrend));
                  end;

                tiEffRSp:
                    if FieldByName('c_Sp').AsInteger = 0 then
                      xValue := 0.0
                    else
                      xValue := (FieldByName('c_RSp').AsFloat * 100) / FieldByName('c_Sp').AsInteger;

                tiEffCSp:
                    if FieldByName('c_SFICnt').AsInteger = 0 then
                      xValue := 0.0
                    else
                      xValue := (FieldByName('c_CSp').AsFloat * 100) / FieldByName('c_Sp').AsInteger;

                tiWeight, tiCones:
                  xValue := FieldByName(xColName).AsFloat;
              else // Die restlichen Items werden fix auf L�ngenbasis berechnet
                xValue := CalcValue(mLenBase, FieldByName(xColName).AsFloat, xLen, 0);
              end;  // case
            end else  // if len > cMinSpuledLen
              xValue := 0.0;

            // now add value to array with DateTime and calculate standard value
            xData[xTrendCount].Value := xValue;
            xData[xTrendCount].Date  := FieldByName('c_Time').AsDateTime;
            xStandard      := xStandard + xData[xTrendCount].Value;
            // next record in dataset
            inc(xTrendCount);
            Next;
          end;
          xStandard := xStandard / xTrendCount;
        end else begin
          xStandard   := 0;
          xTrendCount := 1;
          SetLength(xData, xTrendCount);
          xData[0].Value := 0.0;
          xData[0].Date  := Now;
        end;
      except
        on E:Exception do begin
          if lDebugTrend then
            CodeSite.SendMsg(Format('TRequestBasket.GetTrendValues: %s', [e.Message]));
          gEventLog.Write(etError, Format('TRequestBasket.GetTrendValues: %s', [e.Message]));
        end;
      end;
    end else begin
      if lDebugTrend then
        CodeSite.SendString('PrepareTrendQuery failed', MMError);
    end;

  end;
{$ENDIF}

  // Number of Items
  InsertDataItem(fdtShort, xTrendCount);
  InsertDataItem(fdtSingle, xStandard);  // Actual standard value
  xStep := 1;
  // insert all trend values
  for i:=0 to xTrendCount-1 do begin
    if lDebugTrend then begin
      CodeSite.SendFmtMsg('Trend Item:%s, Value:%.2f, Date:%s ',
                          [cTrendSelection[xTrend],
                           xData[i].Value, DateTimeToStr(xData[i].Date)]);
    end;

    // Trend value
    InsertDataItem(fdtSingle, xData[i].Value);
    // X-Axis value
    InsertDataItem(fdtOLEDateTime, xData[i].Date);
    // Description of step
    xStr := 'Value ' + IntToStr(i);
    InsertDataItem(fdtString, xStr, 0);
    // step number
    InsertDataItem(fdtWord, xStep);

    inc(xStep);
    inc(Result, 4);
  end;

  // clear dynamic created array
  SetLength(xData, 0);
end;
//------------------------------------------------------------------------------
function TRequestBasket.GetMachineOfflimitValues(aDid: TDidItems; aDataSet: TNativeAdoQuery): Word;
var
{$IFDEF MMLinkDummy}
  j: Integer;
{$ENDIF}
  i: Integer;
  xFieldName: String;
  xTimeName: String;
  xDataTyp: TFloorDataTyp;
  xStr: String;
  //.....................................................
  procedure CheckForOfflimit(aValue: String);
  begin
    with aDataSet do begin
      // wenn das Feld f�r TimeZ�hlen > 0 ist, dann ist eine Verletzung da!
      if FieldByName(xTimeName).AsInteger > 0 then
        aValue := '*' + aValue;
      inc(Result, InsertDataItem(xDataTyp, aValue, GetDIDSize(aDid)));
    end;
  end;
  //.....................................................
begin
  Result := 0;
  xFieldName := GetDIDColName(aDid);
  xTimeName  := cMetaData[aDid].ShortText;

  xDataTyp    := GetDidType(aDid);
{$IFDEF MMLinkDummy}
  GetAllColumnValuesFromDB(aDid, Nil);
{$ELSE}
  if aDataSet.FindFirst and (xFieldName <> '') then begin
    with aDataSet do
    try
      while not EOF do begin  // ADO Conform
        case aDid of
          diOffSpindle_id: begin
              inc(Result, InsertDataItem(xDataTyp, FieldByName(xFieldName).Value, GetDIDSize(aDid)));
            end;
          diOffOnlineTime: begin
              inc(Result, InsertDataItem(xDataTyp, FieldByName(xFieldName).Value));
            end;
          diOfftLSt: begin
              // Longstopzeit als Minuten betrachten (auf DB sind es Sekunden)
              i := FieldByName(xFieldName).AsInteger div 60;
              CheckForOfflimit(Format('%d:%.2d', [(i div 60), (i mod 60)]));
            end;
          diOffOutOfProd: begin
              if FieldByName(xFieldName).AsBoolean then xStr := 'X'
                                                   else xStr := ' ';
              inc(Result, InsertDataItem(xDataTyp, xStr));
            end;
        else
          CheckForOfflimit(Format('%.1f', [FieldByName(xFieldName).AsFloat]));
        end;
        Next;
      end;  // while
    except
      on e:Exception do begin
        gEventLog.Write(etError, Format('TRequestBasket.GetAllColumnValuesFromDB: %s', [e.Message]));
      end;
    end;
  end else begin
      inc(Result, InsertDataItem(xDataTyp, '0', GetDIDSize(aDid)));
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
//function TRequestBasket.GetQOfflimitValues(aDid: TDidItems; aMachID: Integer; aQOfflimitList: TQOfflimitReader): Word;
//var
//  i, j: Integer;
//  xFieldName: String;
//  xTimeName: String;
//  xDataTyp: TFloorDataTyp;
//  xStr: String;
//begin
//  Result   := 0;
//  xDataTyp := GetDidType(aDid);
//  with aQOfflimitList do begin
//    for i:=0 to Count-1 do begin
//      with Items[i]^ do begin
//        if MachineID = aMachID then begin
//          case aDid of
//            // Strings
//            diQOffTitle: begin
//                inc(Result, InsertDataItem(xDataTyp, Task, GetDIDSize(aDid)));
//                CodeSite.SendString('QOfflimit.Task', Task);
//              end;
//            diQOffStyle: begin
//                inc(Result, InsertDataItem(xDataTyp, Style, GetDIDSize(aDid)));
//                CodeSite.SendString('QOfflimit.Task', Style);
//              end;
//            diQOffMethod: begin
//                inc(Result, InsertDataItem(xDataTyp, Method, GetDIDSize(aDid)));
//                CodeSite.SendString('QOfflimit.Task', Method);
//              end;
//            diQOffDataItem: begin
//                inc(Result, InsertDataItem(xDataTyp, DataItem, GetDIDSize(aDid)));
//                CodeSite.SendString('QOfflimit.Task', DataItem);
//              end;
//            diQOffMa: begin
//                inc(Result, InsertDataItem(xDataTyp, Machine, GetDIDSize(aDid)));
//                CodeSite.SendString('QOfflimit.Task', Machine);
//              end;
//            // Float
//            diQOffValue:
//              inc(Result, InsertDataItem(xDataTyp, Value));
//            diQOffAverageValue:
//              inc(Result, InsertDataItem(xDataTyp, AverageValue));
//            diQOffLimitValue:
//              inc(Result, InsertDataItem(xDataTyp, LimitValue));
//            diQOffPercentage:
//              inc(Result, InsertDataItem(xDataTyp, Percentage));
//          else
//            inc(Result, InsertEmptyItem(xDataTyp));
//          end; // case
//        end; // if
//      end; // with Items^
//    end; // for
//  end; // with aQOfflimitList
//end;
//------------------------------------------------------------------------------
function TRequestBasket.InsertDataItem(aDataTyp: TFloorDataTyp; aValue: Variant; aSize: Word): Word;
var
  xByte: Byte;
  xShort: SmallInt;
  xWord: Word;
  xLong: LongInt;
  xDWord: DWord;
  xStr: String;
  xOLEDateTime: TDateTime;
  xDateTime: TIDateTime;
  xSingle: Single;
  xDouble: Double;
  xPByte: PByte;
  xSize: Word;
  xRetDescr: TDidRetDescr;
begin
  Result := 0;
  case aDataTyp of
    fdtByte: begin
        xByte := aValue;
        xPByte := @xByte;
      end;
    fdtShort: begin
        xShort := aValue;
        xPByte := @xShort;
      end;
    fdtWord: begin
        xWord := aValue;
        xPByte := @xWord;
      end;
    fdtLong: begin
        xLong := aValue;
        xPByte := @xLong;
      end;
    fdtDWord: begin
        xDWord := aValue;
        xPByte := @xDWord;
      end;
    fdtChar, fdtString: begin
        xStr := Trim(aValue);
        if xStr = '' then xStr := ' '
        else if Length(xStr) > aSize then
          Delete(xStr, aSize, Length(xStr)-aSize+1);
        xPByte := PByte(xStr);
      end;
    fdtSingle: begin
        xSingle := aValue;
        xPByte := @xSingle
      end;
    fdtDouble: begin
        xDouble := aValue;
        xPByte := @xDouble
      end;
    fdtDateTime: begin
        xDateTime := ConvertToIDateTime(aValue);
        xPByte    := @xDateTime;
      end;
    fdtOLEDateTime: begin
        xOLEDateTime := aValue;
        xPByte       := @xOLEDateTime;
      end;
  else
    xPByte := Nil;
  end;  //case

  // Now: fill in data in data block
  if Assigned(xPByte) then begin
    // write DID information into description memory stream
    xRetDescr.Data := mDIDDef;
    xRetDescr.DataPos := mValueMem.Size;
    mDidRetDescrMem.WriteBuffer(xRetDescr, sizeof(xRetDescr));

    // write data typ in data section
    mValueMem.WriteByte(Byte(aDataTyp));
    // get size of data item and ...
    if aDataTyp = fdtString then
      xSize := StrLen(PChar(xPByte))+1
    else
      xSize := cDataItemSize[aDataTyp];
    // ... write data item size in data section
    mValueMem.WriteWord(xSize);
    // write data in data section
    mValueMem.WriteBuffer(xPByte^, xSize);
    // now we have inserted one data item
    Result := 1;
  end;
end;
//------------------------------------------------------------------------------
function TRequestBasket.InsertEmptyItem(aDataTyp: TFloorDataTyp): Word;
begin
  case aDataTyp of
    fdtByte..fdtDWord: begin
        Result := InsertDataItem(aDataTyp, 0);
      end;
    fdtChar, fdtString: begin
        Result := InsertDataItem(aDataTyp, ' ', 0);
      end;
    fdtSingle, fdtDouble, fdtDateTime, fdtOLEDateTime:
        Result := InsertDataItem(aDataTyp, 0.0);
  else
    Result := 0;
  end;  //case
end;
//------------------------------------------------------------------------------
procedure TRequestBasket.Request;
{$IFDEF MMLinkDummy}
const
  cDummyItems: Array[TDataSelection] of Integer = (1,1,1,3,3,21,21,15,30,1,8);
{$ENDIF}
var
  xDidArr: Array of TDid;
  xRetArr: Array of TDidRetDescr;
  xAllValues: Boolean;
  xSize: DWord;
  xSubBasketNr: Word;
  i: Word;
  xRetBasketCount: DWord;
  xDIDCount: Word;
  xDateTime: TDateTime;
  xProdGrpCount: Integer;
  xDidItem: TDidItems;
  xKeyName: String;
  xProdID: DWord;
  xMachID: Word;
  xSection: Word;
begin
  xDidArr := Nil;
  xRetArr := Nil;

  if (GetTickCount - mLastCheckMaState) > cReReadGroupDataTimeOut then begin
    CodeSite.SendMsg('Check TimeSelection and MachState');
    // update the time information in shared memory
    mDM.CheckTimeSelection;
    // now check machine state and production groups
    CheckMachState;
  end;
  // reset time stamp at each request so if a big request for all machines are not interupted with CheckTimeSelection
  mLastCheckMaState := GetTickCount;

  xDIDCount  := 0;
  mMachNr    := mDidRequest^.Machine;
  xAllValues := (mMachNr = cAllValues);
  if not xAllValues then
    dec(mMachNr);  // Indexierung der Maschinen 0-basiert, Floor 1-basiert

//  mMachNr    := mDidRequest^.Machine;
//  xAllValues := (mMachNr = cAllValues);

  if xAllValues or (mMachNr < gMachConfig.NrOfMachines) then begin
    xMachID  := 0;
    xProdID  := 0;
    xSection := 0;
    // check if this is a valid identifier
    if not xAllValues then begin
      xMachID  := gMachConfig.MachRec[mMachNr].MachID;
      xSection := mDidRequest^.SectionNo;
      if (xSection >= 1) and (xSection <= cMaxLotCount) then
        xProdID := gMachConfig.MachRec[mMachNr].ProdGrp[xSection].ProdID;
    end;

    // assign array of TDid to memory pointer of data offset in Basket
    LongInt(xDidArr) := LongInt(mDidRequest) + mDidRequest^.DLPos;

    CodeSite.SendMsg(Format('Basket: MachNo:%d/%d, Count:%d, DLPos:%d', [mMachNr, xSection, mDidRequest^.DidCount, mDidRequest^.DLPos]));

    // count through all subBaskets
    for xSubBasketNr:=0 to mDidRequest^.DidCount - 1 do
    try
      xKeyName := '';
      // remove Key bit ($8000) in DID selection
      Word(xDidItem) := Word(xDIDArr[xSubBasketNr].Did) and not cDIDKeyFlag;
       // check for valid DID range
      if (xDidItem > diUnknown) and (xDidItem <= High(TDidItems)) and
          // and if it isn't a virtual production group
         (not ((xSection <> 0) and (xProdID = 0))) then begin

        // if key flag is set then get column name for key query
        if (Word(xDIDArr[xSubBasketNr].Did) and cDIDKeyFlag) > 0 then begin
          xMachID  := cAllValues;
          xKeyName := GetKeyColNames(xDidItem);
//          xKeyName := GetDIDColName(xDidItem);
        end;{ else if not xAllValues then begin
          xMachID  := gMachConfig.MachRec[mMachNr].MachID;
          xKeyName := '';
        end else
          xMachID  := 0;}

        // save DID Items values for writing in return memory stream
        mDIDDef := xDidArr[xSubBasketNr];
        mDataSelection := TDataSelection(mDIDDef.SubDid);

        if lDebugDID then
          CodeSite.SendMsg(Format('    DID:%s  SubDid:%d', [GetDIDName(xDidItem), Integer(mDataSelection)]));

        // fill in data values for each DID in SubBasket
        case xDidItem of
          diMachine_Name: begin
              inc(xDIDCount, GetMachName(xAllValues, xSection));
            end;
          diDIDStopGroup: begin
              inc(xDIDCount, GetMachStopGrp);
            end;
          diDIDMCStatus: begin
              inc(xDIDCount, GetMachState);
            end;
          diDIDStatusTexts: begin
              inc(xDIDCount, GetMachStateText);
            end;
          diDIDSpdTrend: begin
              inc(xDIDCount, GetTrendValues(mDIDDef.SubDid, xMachID, xProdID));
            end;

          diShiftStart, diShiftEnd: begin
              if xDidItem = diShiftStart then
                xDateTime := gMachConfig.ShiftSelection[mDataSelection]^.TimeFrom
              else
                xDateTime := gMachConfig.ShiftSelection[mDataSelection]^.TimeTo;
              // cut the 23:59:59: Floor converts the value to a string
              CodeSite.SendString('Time', DateTimeToStr(xDateTime));
              if mDataSelection in [dsLastDay, dsLast7Day, dsLastWeek, dsCurMonth, dsLastMonth] then
                xDateTime := Int(xDateTime);
              inc(xDIDCount, InsertDataItem(fdtOLEDateTime, xDateTime));
            end;
          // Number of production groups
          diProdGrpCount: begin
              xProdGrpCount := gMachConfig.MachRec[mMachNr]^.NrOfProdGrp;
              // upload count of production groups
              if mDIDDef.SubDid = 1 then
                inc(xDIDCount, InsertDataItem(fdtLong, xProdGrpCount))
              else // upload color for production groups
                inc(xDidCount, GetProdGroupColor(mDIDDef.SubDid-1));
            end;
          // Machine Offlimit
          diOffSpindle_id..diOffCYTot,diOffOutOfProd: begin
              with mDM do begin
                if PrepareMachineOfflimitQuery(xMachID) then
                  inc(xDidCount, GetMachineOfflimitValues(xDidItem, ResultQuery));
              end;
            end;
          // Groupdata
          diLotData: begin
              inc(xDidCount, GetGroupData(mDIDDef.SubDid - 1));
            end;
          // Quality Offlimit
{13.1.2004, wss removed
          diQOffTitle..diQOffPercentage: begin
              if mDM.PrepareQOfflimit then begin
                inc(xDidCount, GetQOfflimitValues(xDidItem, xMachID, mDM.QOfflimitList));
              end else
                // if somethings goes wrong with query initialization insert at least a empty value
                inc(xDidCount, InsertEmptyItem(GetDidType(xDidItem)));
            end;
{}
        else  // case
          // Fill in values from database in case of mDataSelection: Shift- or Interval data
          with mDM do begin
{$IFDEF MMLinkDummy}
            mSection := xSection;
            if mDataSelection = dsCustShiftSel then
              mMMLinkDummyItems := StrToInt(GetRegString(cRegLM, cRegMillMasterPath + '\Debug', 'HostlinkDummyItems', '10'))
            else
              mMMLinkDummyItems := cDummyItems[mDataSelection];
{$ENDIF}
            if PrepareDataQuery(xMachID, xProdID, mDataSelection, xKeyName) then begin
              if xKeyName = '' then
                inc(xDidCount, GetAllColumnValuesFromDB(xDidItem, ResultQuery))
              else
                inc(xDidCount, GetAllKeyValues(xDidItem, ResultQuery));
            end else
              // if somethings goes wrong with query initialization insert at least a empty value
              inc(xDidCount, InsertEmptyItem(GetDidType(xDidItem)));
          end;
        end;  // case
      end else begin  // if valid xDidItem
        CodeSite.SendWarning(Format('Unknown data item value: %d', [Ord(xDidItem)]));
        inc(xDidCount, InsertEmptyItem(fdtPad));
      end;
    except
      on e:Exception do begin
        raise Exception.Create(Format('TRequestBasket.Request: %s', [e.Message]));
      end;
    end;  // for xSubBasketNr
  end else begin // if AllMachines or valid machine number
    CodeSite.SendWarning('Unknown machine id value' + IntToStr(mMachNr));
    inc(xDIDCount, InsertEmptyItem(fdtPad));
  end;

  PByte(xDidArr) := Nil;     // prevents Delphi to cleanup this dynamic array
  // write count of returned DIDs to MainSection
  mNrOfDidRetDescrMem.WriteWord(xDIDCount);


  // Now the DataPos offset has to be increment with the size of used space from ReturnDIDs
  if mDidRetDescrMem.Size > 0 then begin
    xSize := mDidRetDescrMem.Size;
    xRetArr := mDidRetDescrMem.Memory;
    xRetBasketCount := xSize div SizeOf(TDidRetDescr);
    CodeSite.SendInteger('Hostlink: ReturnBasketCount', xRetBasketCount);
    for i:=0 to xRetBasketCount-1 do begin
      xRetArr[i].DataPos := xRetArr[i].DataPos + xSize;
    end;
  end;
end;
//------------------------------------------------------------------------------
initialization
  // get debug setting from registry
  CodeSite.Enabled := CodeSiteEnabled('Hostlink');
  lDebugTrend      := GetRegBoolean(cRegLM, cRegMMDebug, 'HostlinkTrend', False);
  lDebugDID        := GetRegBoolean(cRegLM, cRegMMDebug, 'HostlinkDID', False);
  lDebugRequest    := GetRegBoolean(cRegLM, cRegMMDebug, 'HostlinkRequest', False);
finalization
end.

