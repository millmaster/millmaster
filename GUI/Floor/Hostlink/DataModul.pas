{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: DataModul.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: DLL for LinkManager
| Process(es)...: -
| Description...: Database connection with queries
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  0.00  Wss | File created
| 11.02.2000  1.00  Wss | Trend query ergaenzt mit %Rsp, %CSp
| 03.04.2000  1.02  Wss | - Changed to support as Hostlink directly
                          - extra function for Trend selection implemented
                          - Database connection is kept alive: no create/free anymore.
| 02.05.2000  1.02  Wss | - table changes implemented for spindle/prodgroup data (SFI, Clusters)
| 19.06.2000  1.02  Wss | Curr/Last month implemented
| 04.07.2000  1.02  Wss | Curr/Last month changed to upload prodgrp summaries instead of details (Floor performance)
| 15.09.2000  1.02  Wss | TrendInterval wasn't constrained with the selected interval time range
| 16.11.2000  1.02  Wss | Problems with missing time information for Cur/LastMonth solved
| 12.03.2001  1.02  Wss | selection of TimeRange: -> from >= time AND to < time (instead to <= time)
| 26.04.2001  2.01  Wss | GrpData.gdStyleName implemented
| 13.11.2001  2.03  Wss | Bob, Cones divided to 100 -> factor on database
| 26.11.2001  2.03  Wss | factor for Bob, Cones removed -> field on database changed to float type
| 13.02.2002  2.03  Wss | PrepareGroupDataQuery: Dataset wird fuer alle Gruppen gelesen um dann mit
                          Filter zu arbeiten -> viel schneller, als wenn PEff fuer jede Gruppe einzeln
                          geholt wird. Timeout zwischen 2 reads: cReReadGroupDataTimeOut (5000)
| 16.07.2002 2.03 Wss | Dataitem LckCl hinzugefuegt
| 16.09.2002 2.50 Wss | Auf ADO umgebaut
| 23.10.2002 2.03 Wss | Auslesen von Maschinenofflimit Daten ist nun auch �ber Filter optimiert
| 22.11.2002 2.03 Wss | - Aktualisieren von MaOfflimit Timeout angepasst (ChangeMode mit cmMaOfflimit)
                        - DataItem diOffOutOfProd hinzugef�gt
| 02.12.2002 2.03 Wss | - DataItem diShiftInDay hinzugef�gt
| 16.12.2002 2.03 Wss | In TDMAccess.CheckTimeSelection: wenn noch keine Daten auf DB, wurde
                        per GetDataField auf ein ung�ltiger Record zugegriffen 
| 12.01.2004      Wss | Unterst�tzung f�r QOfflimit Items entfernt
| 27.01.2005      Wss | Aktuelle Monat wird nun auch mit Now initialisiert
| 02.06.2005 2.03 Wss | SpectraPlus und Zenit Datenelemente hinzugef�gt
| 17.06.2005 2.03 Wss | Trend DataItem Short Cluster war noch falsch
| 29.09.2005 2.04 Wss | Trend Items erweitert
| 18.06.2008 2.05 Nue | VCV-Fields added.
|=========================================================================================*}
unit DataModul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  DB, ADODB, HostlinkShare,
  HostlinkDef, mmVCLStringList, AdoDBAccess, mmADODataSet;

const
  cProdIDJoin  = 'AND c_prod_id = %d';
  cMachineJoin = 'AND c_machine_id = %d';
  // Index f�r direktzugriff der Queries
  cHostlinkMaxQueries = 6;
  cQryData            = 0;
  cQryTemp            = 1;
  cQryTrend           = 2;
  cQryGroupData       = 3;
  cQryProdGrpInfo     = 4;
  cQryMachineOfflimit = 5;


type
  TDMAccess =  class(TDataModule)
    slShiftDetailQry: TmmVCLStringList;
    slIntDataQry: TmmVCLStringList;
    slKeyQry: TmmVCLStringList;
    slTrendShift: TmmVCLStringList;
    slTrendColumns: TmmVCLStringList;
    slTrendInterval: TmmVCLStringList;
    slDataColumns: TmmVCLStringList;
    slShiftSumQry: TmmVCLStringList;
    slMachineInfoQry: TmmVCLStringList;
    slGroupData: TmmVCLStringList;
    slIntervalInfo: TmmVCLStringList;
    slMachineOfflimit: TmmVCLStringList;
    slShiftInfo: TmmVCLStringList;
    slProdGrpInfo: TmmVCLStringList;
    slMachineCount: TmmVCLStringList;
  private
    fMMError: String;
//    fQOfflimitList: TQOfflimitReader;
    FResultQuery: TNativeAdoQuery;
    mDBAccess: TAdoDBAccess;
    mGroupDataTimeStamp: DWord;
    mLastDataSelection: TDataSelection;
    mLastProdID: LongInt;
    mLastShiftCalID: Byte;
    function SetMachineFilter(aDataSet: TNativeAdoQuery; aMachID: Word): Boolean;
    function GetNativeQuery(aIndex: Integer): TNativeAdoQuery;
  protected
    property dseData: TNativeAdoQuery index cQryData read GetNativeQuery;
    property dseGroupData: TNativeAdoQuery index cQryGroupData read GetNativeQuery;
    property dseMachineOfflimit: TNativeAdoQuery index cQryMachineOfflimit read GetNativeQuery;
    property dseProdGrpInfo: TNativeAdoQuery index cQryProdGrpInfo read GetNativeQuery;
    property dseTrend: TNativeAdoQuery index cQryTrend read GetNativeQuery;
    property dseTemp: TNativeAdoQuery index cQryTemp read GetNativeQuery;
  public
    property MMError: String read fMMError;
    property ResultQuery: TNativeAdoQuery read FResultQuery;
//    property QOfflimitList: TQOfflimitReader read fQOfflimitList;

    procedure CheckTimeSelection;
    constructor Create(aOwner: TComponent); override;
    destructor Destroy; override;
    function GetMachineCount: Integer;
    function Init: Boolean;
    function PrepareDataQuery(aMachID: Word; aProdID: LongInt; aDataSelection: TDataSelection; aKeyColumn: String): Boolean;
    function PrepareGroupDataQuery(aProdID: Integer): Boolean;
    function PrepareMachineQuery: Boolean;
    function PrepareMachineOfflimitQuery(aMachID: Word): Boolean;
    function PrepareProdGrpQuery(aMachID: Word): Boolean;
//    function PrepareQOfflimit: Boolean;
    function PrepareTrendQuery(aMachID: Word; aProdID: LongInt; aDataSelection: TDataSelection): Boolean;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R *.DFM}
uses
  mmMBCS,

  mmCS,
  mmEventLog, LoepfeGlobal, MMUGlobal;

const
  cKeyQuery = 'SELECT DISTINCT %s FROM t_prodgroup';
//  cQOQueries: Array[Boolean] of String = (cGetTempOfflimits, cGetActOfflimits);

//------------------------------------------------------------------------------
procedure TDMAccess.CheckTimeSelection;
{$IFDEF MMLinkDummy}
const
  cTimePattern: Array[0..3] of String = (' 22:00:00', ' 06:00:00', ' 14:00:00', ' 22:00:00');
{$ENDIF}
var
  xYear, xMonth, xDay: Word;
  xNow: TDateTime;
  xShiftDate: TDateTime;
  xIntDate: TDateTime;
{$IFDEF MMLinkDummy}
  i: Integer;
  xStr: String;
{$ENDIF}
  //...........................................................
  function GetDataField(aQuery: TNativeAdoQuery; aFieldName: String; aIndex: Integer; aDefault: TDateTime): TDateTime;
  begin
    Result := aDefault;
    with aQuery do
    try
      if Active then First else Open;
      while not EOF and (aIndex > 0) do begin
        dec(aIndex);
        Next;
      end;
      if not EOF then
        Result := FieldByName(aFieldName).AsDateTime;
    except
      on e:Exception do begin
//        fMMError := e.Message;
        raise Exception.Create(Format('TDMAccess.GetDataField (%s): %s', [aFieldName, e.Message]));
//        gEventLog.Write(etError, Format('TDMAccess.GetDataField (%s): %s',
//                                        [aFieldName, fMMError]));
      end;
    end;
  end;
  //...........................................................
begin
  xNow       := Now;

{$IFDEF MMLinkDummy}
  i := Round(Int((Frac(xNow) * 24) / 8));
  xStr := DateToStr(xNow) + cTimePattern[i];
  xShiftDate := StrToDateTime(xStr);
//  xShiftDate := StrToDateTime(DateToStr(xNow) + cTimePattern[i]);

  if xShiftDate > xNow then
    xShiftDate := xShiftDate - 1;

  with gMachConfig do begin
    ShiftSelection[dsCurShift]^.TimeTo := xNow;
    ShiftSelection[dsCurShift]^.TimeFrom := xShiftDate;

    ShiftSelection[dsLastShift]^.TimeTo := xShiftDate;
    ShiftSelection[dsLastShift]^.TimeFrom := xShiftDate - (8 / 24);

    ShiftSelection[dsLast3Shift]^.TimeTo := xShiftDate;
    ShiftSelection[dsLast3Shift]^.TimeFrom := xShiftDate - 1;

    // Last Day
    ShiftSelection[dsLastDay]^.TimeFrom := Int(xNow - 1);                     // -> 00:00:00
    ShiftSelection[dsLastDay]^.TimeTo   := Int(xNow) - (1 / (24 * 60 * 60));  // -> 23:59:59
    // Last 7 Days
    ShiftSelection[dsLast7Day]^.TimeFrom := Int(xNow - 7);                    // -> 00:00:00
    ShiftSelection[dsLast7Day]^.TimeTo   := Int(xNow) - (1 / (24 * 60 * 60)); // -> 23:59:59
    // Last Week     1 = Sunday, 7 = saturday
    xShiftDate := Int(xNow - 7 - DayOfWeek(xNow) + 2); // week start with Monday -> + 2
    ShiftSelection[dsLastWeek]^.TimeFrom := xShiftDate;                            // -> 00:00:00
    ShiftSelection[dsLastWeek]^.TimeTo   := xShiftDate + 7 - (1 / (24 * 60 * 60)); // -> 23:59:59

    ShiftSelection[dsIntervalData]^.TimeTo   := xNow;
    ShiftSelection[dsIntervalData]^.TimeFrom := xNow - (8 / 24);
  end;
{$ELSE}
  dseTemp.SQL.Text := slShiftInfo.Text;
  dseTemp.ParamByName('ShiftCalID').AsInteger := gMachConfig.DefaultShiftCalID;
  xShiftDate := GetDataField(dseTemp, 'c_shift_start', 0, xNow);
  if xShiftDate < 1 then
    xShiftDate := xNow;

  with gMachConfig do begin
    ShiftSelection[dsCurShift]^.TimeTo := xNow;
    ShiftSelection[dsCurMonth]^.TimeTo := xNow;

    // after a shift change, update time information in shared memory
    if xShiftDate <> ShiftSelection[dsCurShift]^.TimeFrom then begin
      // if a new Shift is active there could be other Quality Offlimit results
      // to force of re-read the result list
//      FreeAndNil(fQOfflimitList);
      // performs a re-open of data query because new time range
      mLastDataSelection := dsNone;
      // Current Shift
      ShiftSelection[dsCurShift]^.TimeFrom := xShiftDate;
      // Last Shift
      ShiftSelection[dsLastShift]^.TimeFrom := GetDataField(dseTemp, 'c_shift_start', 1, xNow-1);
      ShiftSelection[dsLastShift]^.TimeTo   := xShiftDate;
      // Last 3 Shift
      ShiftSelection[dsLast3Shift]^.TimeFrom := GetDataField(dseTemp, 'c_shift_start', 3, xNow-1);
      ShiftSelection[dsLast3Shift]^.TimeTo   := xShiftDate;
      // Last Day
      ShiftSelection[dsLastDay]^.TimeFrom := Int(xNow - 1);                     // -> 00:00:00
      ShiftSelection[dsLastDay]^.TimeTo   := Int(xNow) - (1 / (24 * 60 * 60));  // -> 23:59:59
      // Last 7 Days
      ShiftSelection[dsLast7Day]^.TimeFrom := Int(xNow - 7);                    // -> 00:00:00
      ShiftSelection[dsLast7Day]^.TimeTo   := Int(xNow) - (1 / (24 * 60 * 60)); // -> 23:59:59
      // Last Week     1 = Sunday, 7 = saturday
      xShiftDate := Int(xNow - 7 - DayOfWeek(xNow) + 2); // week start with Monday -> + 2
      ShiftSelection[dsLastWeek]^.TimeFrom := xShiftDate;                            // -> 00:00:00
      ShiftSelection[dsLastWeek]^.TimeTo   := xShiftDate + 7 - (1 / (24 * 60 * 60)); // -> 23:59:59

      // Current Month
      DecodeDate(xNow, xYear, xMonth, xDay);
      // starts with first day of monts, TimeTo is set above to Now
      ShiftSelection[dsCurMonth]^.TimeFrom := EncodeDate(xYear, xMonth, 1);

      // Last Month
      xNow := xNow - xDay; // retrieve the last month by subract the current count of days from current date
      DecodeDate(xNow, xYear, xMonth, xDay);
      ShiftSelection[dsLastMonth]^.TimeFrom := EncodeDate(xYear, xMonth, 1);
      xNow := EncodeDate(xYear, xMonth, MonthDays[IsLeapYear(xYear), xMonth]) + EncodeTime(23, 59, 59, 00);
      ShiftSelection[dsLastMonth]^.TimeTo   := xNow;
    end;
    // Interval range
    dseTemp.SQL.Text := Format(slIntervalInfo.Text, [gMachConfig.IntervalTimeRange]);

    xIntDate  := GetDataField(dseTemp, 'c_interval_to', 0, xNow-1);
    if xIntDate < 2 then begin
      mLastDataSelection := dsNone;
      ShiftSelection[dsIntervalData]^.TimeFrom := 2;
      ShiftSelection[dsIntervalData]^.TimeTo   := 3;
    end else if xIntDate <> ShiftSelection[dsIntervalData]^.TimeTo then begin
      // performs a re-open of data query because new time range
      mLastDataSelection := dsNone;
      dseMachineOfflimit.Close;
      ShiftSelection[dsIntervalData]^.TimeFrom := GetDataField(dseTemp, 'c_interval_from', 0, xNow-1);
      ShiftSelection[dsIntervalData]^.TimeTo   := xIntDate;
    end;
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
constructor TDMAccess.Create(aOwner: TComponent);
begin
  EnterMethod('TDMAccess.Create');
  inherited Create(aOwner);

//  fQOfflimitList      := Nil;

//  conDefault.Connected := False;
  mDBAccess            := TAdoDBAccess.Create(cHostlinkMaxQueries, False);
  mGroupDataTimeStamp := 0;
  mLastDataSelection  := TDataSelection(255);
  mLastProdID         := 0;
  mLastShiftCalID     := 255;  // it has to be different on first time

end;
//------------------------------------------------------------------------------
destructor TDMAccess.Destroy;
begin
  CodeSite.SendMsg('TDMAccess.Destroy');
  mDBAccess.Free;
//  conDefault.Connected := False;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TDMAccess.Init: Boolean;
begin
  EnterMethod('TDMAccess.Init');
  Result := False;
  with mDBAccess do
  try
    Result := Init;
    if Result then
      dseMachineOfflimit.SQL.Text := slMachineOfflimit.Text
    else
      fMMError := DBErrorTxt;
  except
    on e:Exception do begin
      WriteToEventLog('Hostlink Init failed: ' + e.Message, 'TDMAccess.Init', etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareDataQuery(aMachID: Word; aProdID: LongInt; aDataSelection: TDataSelection; aKeyColumn: String): Boolean;
var
  xProdIDJoin: String;
begin
{$IFDEF MMLinkDummy}
  Result := True;
  FResultQuery := Nil;
{$ELSE}
  if (aDataSelection <> mLastDataSelection) OR
     (aProdID <> mLastProdID) OR
     ((aDataSelection = dsCustShiftSel) AND gMachConfig.CheckChangeMode(cmCustShift)) OR
     (aKeyColumn <> '') then
  begin
    mLastDataSelection := aDataSelection;
    mLastProdID        := aProdID;

    CodeSite.SendWarning('qryData.Close');
    dseData.Close;
    // prepare query for uploading all available items for current data item (IKey feature)
    if aKeyColumn <> '' then begin
      mLastDataSelection := dsNone;  // performs a new request
      with dseData do begin
        SQL.Text := Format(slKeyQry.Text, [aKeyColumn, aKeyColumn]);
        CodeSite.SendStringList('Key Query', SQL);
      end;
    end else begin
      // if a single production group is requested
      // initialize an additional AND statement in where part of query
      if aProdID = 0 then xProdIDJoin := ''
                     else xProdIDJoin := Format(cProdIDJoin, [aProdID]);

      // Shift selection
      if aDataSelection in [dsCurShift..dsCustShiftSel] then begin
        // keep setting from current selection to distinguish new selections
        if  aDataSelection = dsCustShiftSel then begin
          gMachConfig.ClearChangeMode(cmCustShift);
          mLastShiftCalID := gMachConfig.CustShiftCalID
        end else
          mLastShiftCalID := gMachConfig.DefaultShiftCalID;

        with dseData do begin
          // on mont selection build summaries at production groups to have lower data amount
          if (aDataSelection in [dsCurMonth, dsLastMonth]) OR
             ((aDataSelection = dsCustShiftSel) and not gMachConfig.CustShiftDetail) then
            SQL.Text := Format(slShiftSumQry.Text, [slDataColumns.Text, xProdIDJoin])
          else
            SQL.Text := Format(slShiftDetailQry.Text, [slDataColumns.Text, xProdIDJoin]);

          ParamByName('ShiftCalID').AsInteger := mLastShiftCalID;
          ParamByName('TimeFrom').AsDateTime := gMachConfig.ShiftSelection[aDataSelection]^.TimeFrom;
          ParamByName('TimeTo').AsDateTime   := gMachConfig.ShiftSelection[aDataSelection]^.TimeTo;

//          if aDataSelection in [dsCurMonth, dsLastMonth] then
            CodeSite.SendStringList('DataQuery', SQL);
        end;
      end else begin
        // Interval selection
        with dseData do begin
          SQL.Text := Format(slIntDataQry.Text, [slDataColumns.Text, xProdIDJoin]);
          ParamByName('TimeFrom').AsDateTime := gMachConfig.ShiftSelection[aDataSelection]^.TimeFrom;
          ParamByName('TimeTo').AsDateTime   := gMachConfig.ShiftSelection[aDataSelection]^.TimeTo;
          CodeSite.SendString('TimeFrom', DateTimeToStr(gMachConfig.ShiftSelection[aDataSelection]^.TimeFrom));
          CodeSite.SendString('TimeTo', DateTimeToStr(gMachConfig.ShiftSelection[aDataSelection]^.TimeTo));
          CodeSite.SendStringList('IntQuery', SQL);
        end;
      end;
    end;
  end;

  // Open ResultQuery and set machine filter
  // =======================================
  Result := SetMachineFilter(dseData, aMachID);
{$ENDIF}
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareGroupDataQuery(aProdID: Integer): Boolean;
begin
{$IFDEF MMLinkDummy}
  Result := True;
  FResultQuery := Nil;
{$ELSE}
  // wenn Gruppendaten verlangt und Zeit seit letzter Abfrage abgelaufen dann
  // die Daten neu holen (Timeout momentan 5 Sekunden)
  if (GetTickCount - mGroupDataTimeStamp) > cReReadGroupDataTimeOut then begin
    CodeSite.SendWarning('ReRead GroupData');
    with dseGroupData do begin
      Close;
      SQL.Text := Format(slGroupData.Text, [gMachConfig.IntervalTimeRange]);
    end;
  end;

  // Open ResultQuery and set machine filter
  // =======================================
  FResultQuery := dseGroupData;
  with FResultQuery do
  try
    if Active then First else Open;
    Recordset.Filter := 'c_prod_id = ' + IntToStr(aProdID);
  finally
    Result := Active;
    mGroupDataTimeStamp := GetTickCount;
  end;
{$ENDIF}
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareMachineQuery: Boolean;
begin
  EnterMethod('PrepareMachineQuery');
  with dseTemp do begin
    Close;
    SQL.Text := slMachineInfoQry.Text;
  end;
  Result := SetMachineFilter(dseTemp, cAllValues);
end;
//------------------------------------------------------------------------------
function TDMAccess.GetMachineCount: Integer;
begin
  Result := 0;
  with dseTemp do
  try
    Close;
    SQL.Text := slMachineCount.Text;
    Open;
    if FindFirst then
      Result := FieldByName('MachineCount').AsInteger;
  except
    on e:Exception do
      CodeSite.SendError('GetMachineCount error: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareMachineOfflimitQuery(aMachID: Word): Boolean;
begin
{$IFDEF MMLinkDummy}
  Result := True;
  FResultQuery := Nil;
{$ELSE}
  with dseMachineOfflimit do begin
    if not Active or (cmMaOfflimit in gMachConfig.ChangeMode) then begin
      gMachConfig.ChangeMode := gMachConfig.ChangeMode - [cmMaOfflimit];
      Close;
      CodeSite.SendNote('Redefine adoOfflimit');
      CodeSite.SendStringList('MaOfflimit', SQL);
      ParamByName('OfflimitTime').AsInteger := gMachConfig.OfflimitTime;
    end;
  end;
  // Open ResultQuery and set machine filter
  // =======================================
  Result := SetMachineFilter(dseMachineOfflimit, aMachID);
{$ENDIF}
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareProdGrpQuery(aMachID: Word): Boolean;
begin
  with dseTemp do begin
    Close;
    SQL.Text := slProdGrpInfo.Text;
  end;
  // Open ResultQuery and set machine filter
  // =======================================
  Result := SetMachineFilter(dseTemp, aMachID);
end;
//------------------------------------------------------------------------------
function TDMAccess.PrepareTrendQuery(aMachID: Word; aProdID: LongInt; aDataSelection: TDataSelection): Boolean;
var
  xANDStatement: String;
begin
  with dseTrend do begin
    Close;
    if aProdID <> 0 then xANDStatement := Format(cProdIDJoin, [aProdID])
                    else xANDStatement := Format(cMachineJoin, [aMachID]);

    if aDataSelection = dsIntervalData then begin
      SQL.Text := Format(slTrendInterval.Text, [slTrendColumns.Text, xANDStatement]);
      ParamByName('TimeFrom').AsDateTime := gMachConfig.ShiftSelection[aDataSelection]^.TimeFrom;
      ParamByName('TimeTo').AsDateTime   := gMachConfig.ShiftSelection[aDataSelection]^.TimeTo;
    end else begin
      SQL.Text := Format(slTrendShift.Text, [slTrendColumns.Text, xANDStatement]);
      ParamByName('ShiftCalID').AsInteger := gMachConfig.DefaultShiftCalID;
      ParamByName('TimeFrom').AsDateTime := gMachConfig.ShiftSelection[aDataSelection]^.TimeFrom;
      ParamByName('TimeTo').AsDateTime   := gMachConfig.ShiftSelection[aDataSelection]^.TimeTo;
    end;
    CodeSite.SendString('PrepareTrendQuery', SQL.Text);
  end;
  // Open ResultQuery and set machine filter
  // =======================================
  Result := SetMachineFilter(dseTrend, cAllValues);
end;
//------------------------------------------------------------------------------
function TDMAccess.SetMachineFilter(aDataSet: TNativeAdoQuery; aMachID: Word): Boolean;
begin
  Result := False;
  if Assigned(aDataSet) then begin
    FResultQuery := aDataSet;
    with FResultQuery do
    try
      try
        if Active then First
                  else Open;
        if aMachID = cAllValues then begin
          RecordSet.Filter   := '';
        end else begin
          RecordSet.Filter := 'c_machine_id = ' + IntToStr(aMachID);
        end;
      except
        on E:Exception do begin
          if AnsiPos('Timeout expired', e.Message) > 0 then begin
            if Command.CommandTimeout < 120 then begin
              Command.CommandTimeout := Command.CommandTimeout + 30;
              gEventLog.Write(etWarning, Format('ADO Timeout in hostlink incrased to %d sec', [Command.CommandTimeout]));
              SetMachineFilter(aDataSet, aMachID);
              Exit;
            end;
          end;

          fMMError := e.Message;
          raise;
//          xStr := Format('TDMAccess.SetMachineFilter: %s', [fMMError]);
//          gEventLog.Write(etError, xStr);
//
//          CodeSite.SendError(xStr);
//          CodeSite.SendString('SetMachineFilter.aDataSet', aDataSet.SQL.Text);
        end;
      end;
    finally
      Result := Active;
    end;
  end else
    CodeSite.SendError('TDMAccess.SetMachineFilter: aDataSet not assigned');
end;
//------------------------------------------------------------------------------
//function TDMAccess.PrepareQOfflimit: Boolean;
//var
//  xStr: String;
//begin
//  Result := False;
//  if gMachConfig.CheckChangeMode(cmQOfflimit) then begin
//    gMachConfig.ClearChangeMode(cmQOfflimit);
//    FreeAndNil(fQOfflimitList);
//  end;
//  // If QOfflimitList doesn't exists create and initialize it.
//  // In CheckTimeSelection after shift change this object will be deleted to
//  // force to create a new one
//  if not Assigned(fQOfflimitList) then begin
//    with dseTemp do begin
//      Close;
//      SQL.Text := cQOQueries[gMachConfig.ShowSystemQO];
//    end;
//    if SetMachineFilter(dseTemp, cAllValues) then
//    try
//      // The OfflimitList has to be filled in only on shift change or on first call.
//      // Instance fQOfflimitList will destroyed at shift change for detect of new reading
//      fQOfflimitList := TQOfflimitReader.Create;
//      fQOfflimitList.LoadData(dseTemp);
//    except
//      on E:Exception do begin
//        fMMError := e.Message;
//        xStr := Format('TDMAccess.PrepareQOfflimit: %s', [fMMError]);
//        gEventLog.Write(etError, xStr);
//        CodeSite.SendError(xStr);
//      end;
//    end;
//  end;
//
//  if Assigned(fQOfflimitList) then
//    Result := (fQOfflimitList.Count > 0);
//end;
//------------------------------------------------------------------------------

function TDMAccess.GetNativeQuery(aIndex: Integer): TNativeAdoQuery;
begin
  if (aIndex >= 0) and (aIndex <= cHostlinkMaxQueries) then
    Result := mDBAccess.Query[aIndex]
  else begin
    Result := Nil;
    Abort;
  end;
end;

end.

