{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebr�der LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: Hostlink.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: Dll for Floor
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  1.00  Wss | File created
| 03.04.2000  1.00  Wss | Changed to implement Hostlink.dll complete in Delphi
| 12.01.2004  1.00  Wss | Unterst�tzung f�r QOfflimit Items entfernt
|=========================================================================================*}
library Hostlink;
 // 15.07.2002 added mmMBCS to imported units
{%ToDo 'Hostlink.todo'}

uses
  MemCheck,
  mmMBCS,
  ComServ,
  Windows,
  MMInterface in 'MMInterface.pas',
  DataModul in 'DataModul.pas' {DMAccess: TDataModule},
  Basket in 'Basket.pas',
  HostlinkShare in '..\..\..\Common\HostlinkShare.pas',
  HostlinkDef in '..\..\..\common\HostlinkDef.pas';

exports
  // extra exports for shift selection for trend reports
  CloseConnectionHandle,
  GetConnectionHandle,
  RequestDataUsingHandle,
  SetFilterData,  // has to be exported but it isn't used
  SetPeriodSelection,
  // standard exports for Floor
  CreateLink,
  DestroyLink,
  GetDidSize name 'GetDidSize',
  GetFirstDataItemText,
  GetLinkStatus,
  GetLinkVersion,
  GetNextDataItemText,
  IsIndexedDID,
  RequestDataItem;


{$R *.RES}

{$IFDEF MMLinkDummy}
//  {$R 'VersionDummy.res'}
{$ELSE}
  {$R 'Version.res'}
{$ENDIF}

begin
{$IFDEF MemCheck}
  MemChk('Hostlink');
{$ENDIF}
end.

