{*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MMInterface.pas
| Projectpart...: Millmaster NT Winding
| Subpart.......: DLL for Floor
| Process(es)...: -
| Description...: Exported DLL interface function for Floor
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 11.01.1999  1.00  Wss | File created
| 07.09.1999  1.01  Wss | Changed to new version
| 03.04.2000  1.02  Wss | - Changed to support as Hostlink directly
                          - extra function for Trend selection implemented
                          - Database connection is kept alive: no create/free anymore.
| 09.01.2001  1.02  Wss | - At CreateLink the lowest ShiftCalID is read as default
| 27.03.2001  1.02  Wss | - DefaultShiftCalID changed to read from SettingsReader class
                            -> MMU_Parm: MMConfiguration, DefaultShiftCalID
| 17.07.2001  1.02  Wss | Umgestellt auf MMGlossary.mld Dictionary
| 11.10.2002        LOK | EOF ADO Conform
| 15.10.2004  1.02  Wss | Boolean R�ckgabewerte f�r exportierte Funktionen M�SSEN LongBool sein
| 19.10.2004  1.02  Wss | export Anweisung von den Funktionen entfernt, da dies nicht n�tig ist mit einer exports Deklaration im Projektsource
| 11.02.2005  1.02  Wss | Kleine Anpassung in InitializeHostlink f�r die Verwendung mit reptimob.EXE
| 15.03.2005  1.02  Wss | Mauszeiger wird nicht mehr ver�ndert beim RequestDataItem -> hatte Fehler verursacht (Litauen)
| 16.03.2005  1.02  Wss | Liste lSubDidList wurde in Initialization der Unit erstellt aber in DestroyLink freigegeben
                          -> Autoprint ruft DestroyLink auch auf ohne dass die DLL nicht mehr ben�tigt wird -> Access Violation in IsIndexedDID
                          -> Das Freigeben der Liste in den Finalization Teil verschoben
| 19.04.2005  1.02  Wss | Wenn keine Maschinen vorhanden sind, dann wird eine Dummy Maschine 'Empty Mach' erstellt
| 02.06.2005  1.02  Wss | MinDBVersion auf 5.01.00 infolge SpectraPlus und Zenit Datenelemente
| 29.09.2005  1.02  Wss | Compiler Hint/Warnings bearbeitet
|=========================================================================================*}
unit MMInterface;

interface
uses
  Classes, Windows, HostlinkDef;
//------------------------------------------------------------------------------
// EXTRA DLL EXPORTS for Trend requests only
//------------------------------------------------------------------------------
function CloseConnectionHandle(aHandle: DWord): LongBool; cdecl;
function GetConnectionHandle(aStr: PChar): DWord; cdecl;
function RequestDataUsingHandle(aHandle: DWord; aHWnd: HWnd; aEv: Pointer; aCtlId: Word; aBasketHnd: THandle; aMaxBasketSize: DWord): LongBool; cdecl;
function SetFilterData(aHandle: DWord; pIF: Pointer): LongBool; cdecl;  // Export, but don't implement
procedure SetPeriodSelection(aHandle: DWord; aPeriod: Word); cdecl;
//------------------------------------------------------------------------------
// STANDARD DLL EXPORTS
//------------------------------------------------------------------------------
function CreateLink(aHWnd: HWnd): LongBool; cdecl;
procedure DestroyLink; cdecl;
function GetDidSize(aDID: Word): Word; cdecl;
function GetFirstDataItemText(aDataItemText: PWChar; aLength: Word; aCategory: DWord; aFormat: DWord; aTextType: TItemText): Word; cdecl;
function GetLinkStatus(aLinkStatus: PLinkStatus): LongBool; cdecl;
function GetLinkVersion: Word; cdecl;
function GetNextDataItemText(aPos: Word; aDataItemText: PWChar; aLength: Word; aCategory: DWord; aFormat: DWord; aTextType: TItemText): Word; cdecl;
function IsIndexedDID(aDID: Word; aIndexedDID: PExtraDidSelection): LongBool; cdecl;
function RequestDataItem(aHWnd: HWnd; aEv: Pointer; aCtlId: Word; aBasketHnd: THandle; aMaxBasketSize: DWord): LongBool; cdecl;
//------------------------------------------------------------------------------

implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS, mmCS, mmEventLog,
  MemCheck, Forms, Controls, ActiveX, SyncObjs,
  SysUtils, LoepfeGlobal, Basket, HostlinkShare, DataModul,
  mmDictionary, IPCClass, mmList, SettingsReader, BaseGlobal;

type
  PPeriodInfo = ^TPeriodInfo;
  TPeriodInfo = record
    ShiftSelection: TDataSelection;
    Handle: DWord;
  end;

var
  lDataModul: TDMAccess;
  lDBChecked: Boolean;
  lLastErrorTimeStamp: DWord;
  lLock: TCriticalSection;
  lRequest: TRequestBasket;
  lSubDidList: TmmList;
  lConnectionHandleList: TmmList;
  lCurrentConnectionHandle: DWord;
//------------------------------------------------------------------------------
function InitializeHostlink: Boolean;
var
  i: DWord;
  xMachineCount: Integer;
begin
  Result := Assigned(gMachConfig);

  if not Result then
  try
    EnterMethod('InitializeHostlink');

{$IFNDEF MMLinkDummy}
    with TMMSettingsReader.Instance do begin
      if not DBVersionOK(gMinDBVersion, not lDBChecked) then begin
        if not lDBChecked then begin
          lDBChecked := True;
          gEventLog.Write(etError, Format('InitializeHostlink failed: wrong database version: is=%s, should=%s', [DBVersion, gMinDBVersion]));
        end;
        Exit;
      end;
    end;
{$ENDIF}

    lDataModul := TDMAccess.Create(Nil);
{$IFNDEF MMLinkDummy}
    if not lDataModul.Init then begin
      gEventLog.Write(etError, 'InitializeHostlink failed: ' + lDataModul.MMError);
      FreeAndNil(lDataModul);
      // to prevent cascaded errors in other module which build on the shared memory
      // create a fake one with no machines!
//      gMachConfig := THostlinkShare.Create(0);
      Exit;
    end;
{$ENDIF}
{$IFDEF MMLinkDummy}
    gMachConfig := THostlinkShare.Create(8);
    for i:=0 to gMachConfig.NrOfMachines-1 do begin
      with gMachConfig.MachRec[i]^ do begin
        MachID := i;
        Typ    := 1;
        Name   := 'Machine ' + IntToStr(i);
        NrOfSpindles := 60;
      end;
    end;
    lRequest:= TRequestBasket.Create(lDataModul);
    Result := True;
{$ELSE}
    with lDataModul do
    try
      xMachineCount := GetMachineCount;
      if PrepareMachineQuery then begin
        with ResultQuery do begin
          if FindFirst then begin
            gMachConfig := THostlinkShare.Create(xMachineCount);
            i := 0;
            while not EOF do begin   // ADO Conform
              with gMachConfig.MachRec[i]^ do begin
                MachID       := FieldByName('c_Machine_id').AsInteger;
                Typ          := TMachineType(FieldByName('c_Machine_type').AsInteger);
                // SpindlePos: Bit0: 0=Left, 1=Right; Bit1: 0=Down, 1=Top
                HeadLeft     := (FieldByName('c_spindle_pos').AsInteger AND $0001) = 0;
                Name         := FieldByName(GetDIDColName(diMachine_Name)).AsString;
                NrOfSpindles := FieldByName('c_nr_of_spindles').AsInteger;
                CodeSite.SendFmtMsg('Mach: %s = %d', [Name, MachID]);
              end;
              Next;
              inc(i);
            end;  // while not EOF
          end else begin
            gEventLog.Write(etWarning, 'MMConnectDatabase: no machines are defined on database');
            gMachConfig := THostlinkShare.Create(1);
            with gMachConfig.MachRec[0]^ do begin
              MachID       := cInvalidMachID;
              Typ          := mtGeneric;
              // SpindlePos: Bit0: 0=Left, 1=Right; Bit1: 0=Down, 1=Top
              HeadLeft     := True;
              Name         := 'Empty Mach';
              NrOfSpindles := 1;
            end;
          end; // if RecordCount

          lRequest:= TRequestBasket.Create(lDataModul);
          Result := True;
        end;  // with
      end;  // if PrepareMachineQuery
    except
      on e:Exception do
        gEventLog.Write(etError, 'MMConnectDatabase: ' + e.Message);
    end;  // with lDataModul
{$ENDIF}
  except
    on e:Exception do begin
      gEventLog.Write(etError, 'InitializeHostlink: ' + e.Message);
      // to prevent cascaded errors in other module which build on the shared memory
      // create a fake one with no machines!
      gMachConfig := THostlinkShare.Create(0);
    end;
  end; // if not Result
end;
//------------------------------------------------------------------------------
procedure ConvertSubDIDTable(aDic: TmmDictionary; aSubDID: TSubDIDList);
var
  xPExtra: PExtraDidSelection;
  i: Integer;
  //............................................................
  function ConvertToWideChar(aStr: String; aSize: Integer = 0): WideString;
  var
    xWBuf: Array[0..255] of WideChar;
  begin
    aStr := aDic.Translate(aStr);
    if aSize = 0 then
      aSize := MultiByteToWideChar(CP_ACP, 0, PChar(aStr), -1, Nil, 0);
    MultiByteToWideChar(CP_ACP, 0, PChar(aStr), -1, @xWBuf, aSize);
    Result := xWBuf;
  end;
  //............................................................
begin
  new(xPExtra);

  xPExtra.Title := ConvertToWideChar(cSubDIDList[aSubDID].Title);
  xPExtra.MaxSelection := cSubDIDList[aSubDID].SubDidCount;
  SetLength(xPExtra.SelInfo, xPExtra.MaxSelection);
  for i:=0 to xPExtra.MaxSelection-1 do begin
    xPExtra^.SelInfo[i].Description := ConvertToWideChar(cSubDIDList[aSubDID].SelInfo[i]);
    xPExtra^.SelInfo[i].ExtraSelFn := '';
  end;
  lSubDidList.Add(xPExtra);
end;
//------------------------------------------------------------------------------
function CloseConnectionHandle(aHandle: DWord): LongBool;
var
  i: Integer;
begin
  Result := True;
  for i:=0 to lConnectionHandleList.Count-1 do
    if PPeriodInfo(lConnectionHandleList.Items[i])^.Handle = aHandle then begin
      Dispose(PPeriodInfo(lConnectionHandleList.Items[i]));
      lConnectionHandleList.Delete(i);
      break;
    end;
end;
//------------------------------------------------------------------------------
function GetConnectionHandle(aStr: PChar): DWord;
var
  xPPeriodInfo: PPeriodInfo;
begin
  if not Assigned(lConnectionHandleList) then begin
    lConnectionHandleList := TmmList.Create;
    lCurrentConnectionHandle := 0;
  end;
  inc(lCurrentConnectionHandle);

  new(xPPeriodInfo);

  xPPeriodInfo.ShiftSelection := gMachConfig.DefaultTrendSelection;
  xPPeriodInfo.Handle := lCurrentConnectionHandle;
  lConnectionHandleList.Add(xPPeriodInfo);

  Result := lCurrentConnectionHandle;
end;
//------------------------------------------------------------------------------
function GetBasket(aMemory: PByte; aMaxBasketSize: DWord): Boolean;
var
  i, j: Integer;
  xStr: String;
  xChar: PChar;
begin
  CodeSite.SendFmtMsg('GetBasket: Request start for $%.8p, BasketSize=%d', [aMemory, aMaxBasketSize]);

  // >>> Debug info
  if lDebugRequest then begin
    xChar := PChar(aMemory);
    for i:=1 to 5 do begin
      xStr := Format('%p  ', [PByte(xChar)]);
      for j:=1 to 32 do begin
        xStr := Format('%s%.2x ', [xStr, Byte(xChar^)]);
        inc(xChar);
      end;
      CodeSite.SendMsg(xStr);
    end;
  end;
  // <<< Debug info

  Result := lRequest.Execute(aMemory, aMaxBasketSize);

  // >>> Debug info
  if lDebugRequest then begin
    xChar := PChar(aMemory);
    for i:=1 to 5 do begin
      xStr := Format('%p  ', [PByte(xChar)]);
      for j:=1 to 32 do begin
        xStr := Format('%s%.2x ', [xStr, Byte(xChar^)]);
        inc(xChar);
      end;
      CodeSite.SendMsg(xStr);
    end;
    CodeSite.SendFmtMsg('Request ends for $%.8p', [aMemory]);
  end;
  CodeSite.AddSeparator;
  // <<< Debug info
end;
//------------------------------------------------------------------------------
function RequestDataUsingHandle(aHandle: DWord; aHWnd: HWnd; aEv: Pointer; aCtlId: Word; aBasketHnd: THandle; aMaxBasketSize: DWord): LongBool;
var
  i: Integer;
  xMemory: PByte;
begin
  lLock.Enter;
  EnterMethod('RequestDataUsingHandle');
  try
    Result := InitializeHostlink;
    if not Result then begin
      gEventLog.Write(etWarning, 'RequestDataItem InitializeHostlink failed');
      Exit;
    end;
    // retrieve pointer to memory of requesting structure
    if Assigned(aEv) or (aHWnd <> 0) then
      xMemory := PByte(MapViewOfFile(aBasketHnd, FILE_MAP_WRITE, 0, 0, 0))
    else
      xMemory := PByte(aBasketHnd);

    if Assigned(xMemory) and Assigned(lRequest) then
    try
      for i:=0 to lConnectionHandleList.Count-1 do
        if PPeriodInfo(lConnectionHandleList.Items[i])^.Handle = aHandle then begin
          lRequest.TrendPeriod := TDataSelection(PPeriodInfo(lConnectionHandleList.Items[i])^.ShiftSelection);
          break;
        end;

      Result := GetBasket(xMemory, aMaxBasketSize);
    except
      on e:Exception do begin
        result := False;
        if (GetTickCount - lLastErrorTimeStamp) > 5000 then begin
          gEventLog.Write(etWarning, Format('RequestDataUsingHandle failed. BasketSize=%d, Error: %s', [aMaxBasketSize, e.Message]));
          lLastErrorTimeStamp := GetTickCount;
        end;
      end;
    end;
  finally
    lLock.Leave;
    sleep(1);
  end;
end;
//------------------------------------------------------------------------------
function SetFilterData(aHandle: DWord; pIF: Pointer): LongBool;
begin
  CodeSite.SendMsg('SetFilterData');
//  gEventLog.Write(etWarning, 'SetFilterData');
  // This function is not used. It is just defined for repitmob.dll for recognize
  // the extra functions to be used.
  Result := True;
end;
//------------------------------------------------------------------------------
procedure SetPeriodSelection(aHandle: DWord; aPeriod: Word);
var
  i: Integer;
begin
  for i:=0 to lConnectionHandleList.Count-1 do
    if PPeriodInfo(lConnectionHandleList.Items[i])^.Handle = aHandle then begin
      PPeriodInfo(lConnectionHandleList.Items[i])^.ShiftSelection := TDataSelection(aPeriod);
      break;
    end;
end;
//------------------------------------------------------------------------------
//Auf grund eines Fehlverhaltens in Floor darf in CreateLink KEINE Initialisierung
//des Datenmoduls auf ADO gemacht werden. Um diesen Bug zu umgehen, wird in
//RequestDataItem die Initialisierung einmal vorgenommen. Dave Ellis hat mir
//zugesichert, dass die 1. Basket Anfrage vor jedem Laden von Plugins in der Floor
//geschieht, da ich sonst Probleme bekomme mit dem ShardedMemory in MillMasterPlugin
function CreateLink(aHWnd: HWnd): LongBool;
begin
  CodeSite.SendMsg('CreateLink');
  Result := True;
//    Result := InitializeHostlink;
{
  Result := (lCreateLinkCounter = 0);
  if Result then
    Result := InitializeHostlink
  else
    dec(lCreateLinkCounter);

{}
//  CodeSite.SendBoolean('Result', Result);
end;
//------------------------------------------------------------------------------
procedure DestroyLink;
//var
//  xPExtra: PExtraDidSelection;
begin
  CodeSite.SendMsg('DestroyLink');
//  if Assigned(lSubDidList) then begin
//    // clean up SubDIDConversion list
//    while lSubDidList.Count > 0 do begin
//      xPExtra := PExtraDidSelection(lSubDidList.Items[0]);
//      SetLength(xPExtra^.SelInfo, 0);
//      Dispose(xPExtra);
//      lSubDidList.Delete(0);
//    end;
//    FreeAndNil(LSubDidList);
//  end;

  if Assigned(gMachConfig) then
    FreeAndNil(gMachConfig);

  if Assigned(lRequest) then
    FreeAndNil(lRequest);

  if Assigned(lDataModul) then
  try
    FreeAndNil(lDataModul);
  except
    on e:Exception do
      CodeSite.SendError('lDataModul: ' + e.Message);
  end;
end;
//------------------------------------------------------------------------------
function GetDidSize(aDID: Word): Word;
var
  xDid: TDidItems;
begin
  xDid := TDidItems(aDID);
  if (xDid >= diMachine_Name) and (xDid <= High(TDidItems)) then begin
    if cMetaData[xDid].DataTyp = fdtString then
      Result := cMetaData[xDid].DataSize
    else
      Result := cDataItemSize[cMetaData[xDid].DataTyp];
  end else
    Result := 0;
end;
//------------------------------------------------------------------------------
function GetFirstDataItemText(aDataItemText: PWChar; aLength: Word; aCategory: DWord; aFormat: DWord; aTextType: TItemText): Word;
begin
  Result := GetNextDataItemText(0, aDataItemText, aLength, aCategory, aFormat, aTextType);
end;
//------------------------------------------------------------------------------
function GetLinkStatus(aLinkStatus: PLinkStatus): LongBool;
var
  xStr: String;
begin
  Result := Assigned(aLinkStatus);
  if Result then begin
    aLinkStatus.LinkTyp := cLinkSynchronous;
    aLinkStatus.LinkStats := @gConnectionStats;
    if aLinkStatus.HostNameSz > 0 then begin
      xStr := StrPas(aLinkStatus.HostName);
    end;
  end;
end;
//------------------------------------------------------------------------------
function GetLinkVersion: Word;
begin
  Result := cHostlinkVersion;
end;
//------------------------------------------------------------------------------
function GetNextDataItemText(aPos: Word; aDataItemText: PWChar; aLength: Word; aCategory: DWord; aFormat: DWord; aTextType: TItemText): Word;
var
  xDid: TDidItems;
  xStr: String;
begin
  Result := 0;

  inc(aPos);  // start with next one
  try
    for xDid:=TDidItems(aPos) to High(TDidItems) do begin
         // StatusText and SpdTrend are requested different
      if (xDid <> diDIDStatusTexts) AND
         (xDid <> diDIDSpdTrend) AND
         // check at key requests if Key flag is set OR
         ( ((aFormat = cFormatKeyItems) AND cMetaData[xDid].Key) OR
             // if xDid has the same data type ...
           (Boolean(GetDIDFormat(cMetaData[xDid].DataTyp) and aFormat))) then begin
//             (not cMetaData[xDid].LabM OR (cMetaData[xDid].LabM AND gMachConfig.IsLabMaster)) AND
//             // ... and no DispoMaster package restriction
//             (not cMetaData[xDid].DispoM OR (cMetaData[xDid].DispoM AND gMachConfig.IsDispoMaster)))) then begin

        case aTextType of
          itLongText: begin
              xStr := cMetaData[xDid].LongText;
            end;
          itShortText: begin
              xStr := cMetaData[xDid].ShortText;
            end;
        end;
        MultiByteToWideChar(CP_ACP, 0, PChar(xStr), -1, aDataItemText, aLength);
        Result := Integer(xDid);
        break;
      end;
    end;
  except
    on e:Exception do begin
      gEventLog.Write(etError, 'GetNextDataItemText: ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function IsIndexedDID(aDID: Word; aIndexedDID: PExtraDidSelection): LongBool;
var
  xDid: TDidItems;
  xIndex: Integer;
  xStr: String;
begin
  Result := (TDidItems(aDid) >= diMachine_Name) and (TDidItems(aDid) <= High(TDidItems));
  try
    if Result then begin
      xDid := TDidItems(aDID);
      xStr := GetDIDName(xDid);
      Result := cMetaData[xDid].SubDid <> slNone;
      if Result then begin
        xIndex := ord(cMetaData[xDid].SubDid);
        Result := (xIndex >= 0) and (xIndex < lSubDidList.Count);
        if Result then
          aIndexedDID^ := PExtraDidSelection(lSubDidList.Items[xIndex])^
        else
          gEventLog.Write(etWarning, Format('IsIndexedDID: xIndex out of range: %d (%d), aDID=%d, aIndexedDID=%.8x', [xIndex, lSubDidList.Count, aDID, Integer(aIndexedDID)]));
      end;
    end
    else begin
      xStr := 'Unknown DID';
      gEventLog.Write(etWarning, Format('IsIndexedDID: aDID out of range: %d (%d-%d), aIndexedDID=%.8x', [aDID, ord(diMachine_Name), ord(High(TDidItems)), Integer(aIndexedDID)]));
    end;
  except
    on e:Exception do begin
      if (GetTickCount - lLastErrorTimeStamp) > 2000 then begin
        gEventLog.Write(etWarning, Format('IsIndexedDID failed: aDID=%d, aIndexedDID=%.8x, Error: %s', [aDID, Integer(aIndexedDID), e.Message]));
        lLastErrorTimeStamp := GetTickCount;
      end;
    end;
  end;

//  xDid := TDidItems(aDID);
//  Result := (xDid >= diMachine_Name) and (xDid <= High(TDidItems));
//  if Result then begin
//    Result := cMetaData[xDid].SubDid <> slNone;
//    if Result then begin
//      xIndex := Integer(cMetaData[xDid].SubDid);
//      if xIndex >= 0 then
//        aIndexedDID^ := PExtraDidSelection(lSubDidList.Items[xIndex])^;
//    end;
//  end;
end;
//------------------------------------------------------------------------------
function RequestDataItem(aHWnd: HWnd; aEv: Pointer; aCtlId: Word; aBasketHnd: THandle; aMaxBasketSize: DWord): LongBool;
var
  xMemory: PByte;
begin
  lLock.Enter;
  EnterMethod('RequestDataItem');
  try
    try
      Result := InitializeHostlink;
      if not Result then begin
        gEventLog.Write(etWarning, 'RequestDataItem InitializeHostlink failed');
        Exit;
      end;
      // retrieve pointer to memory of requesting structure
      if Assigned(aEv) or (aHWnd <> 0) then
        xMemory := PByte(MapViewOfFile(aBasketHnd, FILE_MAP_WRITE, 0, 0, 0))
      else
        xMemory := PByte(aBasketHnd);

      if Assigned(xMemory) and Assigned(lRequest) then
        // kein Cursor Handling mehr da dies mit Autoprint eventuell Probleme verursacht
        Result := GetBasket(xMemory, aMaxBasketSize)
      else
        gEventLog.Write(etWarning, Format('RequestDataItem invalid: xMemory=%.8x, lRequest=%.8x', [Integer(xMemory), Integer(lRequest)]));
    except
      on e:Exception do begin
        result := False;
        if (GetTickCount - lLastErrorTimeStamp) > 5000 then begin
          gEventLog.Write(etWarning, Format('RequestDataItem failed. BasketSize=%d, Error: %s', [aMaxBasketSize, e.Message]));
          lLastErrorTimeStamp := GetTickCount;
        end;
      end;
    end;
  finally
    lLock.Leave;
  end;
end;
//------------------------------------------------------------------------------
var
  xDic: TmmDictionary;
  xSDL: TSubDIDList;
  i:    Integer;
  xPExtra: PExtraDidSelection;
initialization
  gMinDBVersion    := '5.01.00';
  lDataModul := Nil;
  lDBChecked := False;
  lRequest   := Nil;


  gEventLog := TEventLogWriter.Create('MillMaster', gMMHost, ssApplication, ExtractFileName(ParamStr(0)) + ' Hostlink.dll: ', False);
  lLock      := TCriticalSection.Create;

  xDic := TmmDictionary.Create(Nil);
  with xDic do
  try
    try
      AutoConfig := True;
      Open;
    except
      on e:Exception do begin
        gEventLog.Write(etError, 'Error opening dictionary MMGlossary.mld: ' + e.Message);
      end;
    end;

    try
      // convert SubDIDList to WideChar strings
      lSubDidList := TmmList.Create;
      for xSDL:=slLot to slLotData do
        ConvertSubDIDTable(xDic, xSDL);

      // translate the last SubDID table only, not convert
      with cSubDIDList[slMachineState] do begin
        Title := Translate(Title);
        for i:=0 to SubDidCount-1 do
          SelInfo[i] := Translate(SelInfo[i]);
      end;
    except
      on e:Exception do begin
        gEventLog.Write(etError, 'Converting SubDIDTable failed: ' + e.Message);
      end;
    end;
  finally
    Free;
  end;
//------------------------------------------------------------------------------
finalization
  if Assigned(lSubDidList) then begin
    // clean up SubDIDConversion list
    while lSubDidList.Count > 0 do begin
      xPExtra := PExtraDidSelection(lSubDidList.Items[0]);
      SetLength(xPExtra^.SelInfo, 0);
      Dispose(xPExtra);
      lSubDidList.Delete(0);
    end;
    FreeAndNil(lSubDidList);
  end;

  FreeAndNil(lLock);
  FreeAndNil(gEventLog);
end.


