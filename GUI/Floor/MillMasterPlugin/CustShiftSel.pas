(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: BaseForm.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...:
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 23.08.2000  1.00  Wss | ShowLastEmptyGroup implemented
| 02.10.2002  1.00  Wss | auf ADO umgestellt
| 20.01.2004  1.00  Wss | - QOfflimit Parameter entfernt
                          - Security eingefügt
|=========================================================================================*)
unit CustShiftSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BASEDIALOGBOTTOM, IvDictio, IvMulti, IvEMulti, mmTranslator, Db,
  mmDataSource, StdCtrls, mmComboBox, DBVisualBox, mmLabel, Buttons, mmBitBtn,
  mmDictionary, mmGroupBox, ExtCtrls, mmPanel, mmCheckBox, mmRadioGroup,
  HostlinkShare, mmButton, ADODB, mmADODataSet, mmADOConnection, NumCtrl,
  ActnList, mmActionList, MMSecurity;

type
  TfrmCustOptions = class(TDialogBottom)
    dsShiftCal: TmmDataSource;
    dsShiftFrom: TmmDataSource;
    dsShiftTo: TmmDataSource;
    mTranslator: TmmTranslator;
    mmPanel1: TmmPanel;
    mmGroupBox2: TmmGroupBox;
    laShiftCal: TmmLabel;
    laShiftFrom: TmmLabel;
    laTrendDefault: TmmLabel;
    laShiftTo: TmmLabel;
    vbShiftCal: TDBVisualBox;
    vbShiftFrom: TDBVisualBox;
    cobTrendDefault: TmmComboBox;
    vbShiftTo: TDBVisualBox;
    mmGroupBox1: TmmGroupBox;
    laOfflimitTime: TmmLabel;
    edOfflimitTime: TNumEdit;
    cbShowDetail: TmmCheckBox;
    gbMisc: TmmGroupBox;
    cbShowEmptyProdGroup: TmmCheckBox;
    cbShowStyleColor: TmmCheckBox;
    conDefault: TmmADOConnection;
    dseShiftTo: TmmADODataSet;
    dseShiftFrom: TmmADODataSet;
    dseShiftCal: TmmADODataSet;
    MMSecurityDB: TMMSecurityDB;
    MMSecurityControl: TMMSecurityControl;
    mmButton1: TmmButton;
    mmActionList: TmmActionList;
    acOK: TAction;
    acSecurity: TAction;
    procedure edOfflimitTimeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure vbShiftCalChange(Sender: TObject);
    procedure vbShiftFromChange(Sender: TObject);
    procedure vbShiftToChange(Sender: TObject);
    procedure cbShowDetailClick(Sender: TObject);
    procedure edOfflimitTimeChange(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
  private
    fShiftCalID: Word;
    fShiftFrom: TDateTime;
    fShiftTo: TDateTime;
    function GetDictionary: TmmDictionary;
    function GetShiftValue(const Index: Integer): TDateTime;
    procedure SetDictionary(const Value: TmmDictionary);
    procedure SetShiftValue(const Index: Integer; const Value: TDateTime);
  public
    mChangeMode: TChangeModeSet;
    constructor Create(aOwner: TComponent); override;
    property Dictionary: TmmDictionary read GetDictionary write SetDictionary;
    property ShiftCalID: Word read fShiftCalID write fShiftCalID;
    property ShiftFrom: TDateTime index 0 read GetShiftValue write SetShiftValue;
    property ShiftTo: TDateTime   index 1 read GetShiftValue write SetShiftValue;
  end;

var
  frmCustOptions: TfrmCustOptions;

implementation // 15.07.2002 added mmMBCS to imported units

uses
  mmMBCS,
  LoepfeGlobal, HostlinkDef;

{$R *.DFM}
//------------------------------------------------------------------------------
constructor TfrmCustOptions.Create(aOwner: TComponent);
var
  i: TDataSelection;
  xStr: String;
begin
  inherited Create(aOwner);

  conDefault.Connected := False;
  mChangeMode := [];
  cobTrendDefault.Clear;
  for i:=dsCurShift to dsIntervalData do begin
    xStr := mTranslator.Dictionary.Translate(cDataSelectionText[i]);
    cobTrendDefault.Items.Add(xStr);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.edOfflimitTimeKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_RETURN then begin
    bOK.SetFocus;
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.FormShow(Sender: TObject);
var
  xDateTime: TDateTime;
  xStr: String;
  xIndex: Integer;
begin
  try
    dseShiftCal.Open;
    // check if ShiftCalID exists in table t_shiftcal
    if dseShiftCal.Locate('c_shiftcal_id', fShiftCalID, [loCaseInsensitive]) then begin
      // if yes show calendar name in combobox and ...
      vbShiftCal.Text  := dseShiftCal.FieldByName('c_shiftcal_name').AsString;
      dseShiftFrom.Open;
      if dseShiftFrom.Locate('c_shift_start', fShiftFrom, [loCaseInsensitive]) then begin
        xStr := DateTimeToStr(fShiftFrom);
        xIndex := vbShiftFrom.Items.IndexOf(xStr);
        if xIndex > -1 then begin
          vbShiftFrom.ItemIndex := xIndex;

          dseShiftTo.Open;
          xStr := DateTimeToStr(fShiftTo);
          xIndex := vbShiftTo.Items.IndexOf(xStr);
          if xIndex > -1 then begin
            vbShiftTo.ItemIndex := xIndex;
          end;
        end;
      end;
    end;
  except
  end;
end;
//------------------------------------------------------------------------------
function TfrmCustOptions.GetDictionary: TmmDictionary;
begin
  Result := TmmDictionary(mTranslator.Dictionary);
end;
//------------------------------------------------------------------------------
function TfrmCustOptions.GetShiftValue(const Index: Integer): TDateTime;
begin
  case Index of
    0: Result := fShiftFrom;
    1: Result := fShiftTo;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.SetDictionary(const Value: TmmDictionary);
begin
  mTranslator.Dictionary := Value;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.SetShiftValue(const Index: Integer; const Value: TDateTime);
begin
  case Index of
    0: fShiftFrom := Value;
    1: fShiftTo   := Value;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.vbShiftCalChange(Sender: TObject);
begin
  mChangeMode := mChangeMode + [cmCustShift];
  try
    with dseShiftCal do begin
      // close and clear VisualBox
      dseShiftTo.Close;
      dseShiftFrom.Close;
      if Locate('c_shiftcal_name', vbShiftCal.Text, [loCaseInsensitive]) then begin
        fShiftCalID := FieldByName('c_shiftcal_id').AsInteger;
        dseShiftFrom.Open;
      end else
        vbShiftCal.Text := '';
    end;
  except
    vbShiftCal.Text := '';
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.vbShiftFromChange(Sender: TObject);
var
  xShiftTo: TDateTime;
begin
  mChangeMode := mChangeMode + [cmCustShift];
  with dseShiftFrom do
  try
    if Locate('c_shift_id', vbShiftFrom.KeyValue, [loCaseInsensitive]) then begin
      fShiftFrom := FieldByName('c_shift_start').AsDateTime;
      if trim(vbShiftTo.Text) = '' then
        fShiftTo := fShiftFrom;
    end else
      vbShiftFrom.Text := '';
  except
    vbShiftFrom.Text := '';
  end;

  dseShiftTo.Close;
  if vbShiftFrom.Text <> '' then begin
    dseShiftTo.Open;
    if fShiftTo < fShiftFrom then
      fShiftTo := fShiftFrom;
    vbShiftTo.Text := DateTimeToStr(fShiftTo);
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.vbShiftToChange(Sender: TObject);
begin
  mChangeMode := mChangeMode + [cmCustShift];
  with dseShiftTo do
  try
    if Locate('c_shift_id', vbShiftTo.KeyValue, [loCaseInsensitive]) then
      fShiftTo := FieldByName('c_shift_start').AsDateTime
    else
      vbShiftTo.Text := '';
  except
    vbShiftTo.Text := '';
  end;
end;
//------------------------------------------------------------------------------
procedure TfrmCustOptions.cbShowDetailClick(Sender: TObject);
begin
  mChangeMode := mChangeMode + [cmCustShift];
end;

procedure TfrmCustOptions.edOfflimitTimeChange(Sender: TObject);
begin
  mChangeMode := mChangeMode + [cmMaOfflimit];
end;

procedure TfrmCustOptions.acSecurityExecute(Sender: TObject);
begin
  MMSecurityControl.Configure;
end;

end.

