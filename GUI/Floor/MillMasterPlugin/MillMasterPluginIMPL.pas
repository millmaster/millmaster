(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: MillMasterPluginIMPL.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Implements the ILoepfePlugin interface and its methodes.
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 02.03.2000  1.01  Wss | Error read menu information if bitmap was empty
| 20.03.2000  1.01  Wss | Floor handling of non modal forms got problems -> work around with
                          temporarly solved by creating a dummy form and show in Initialize
| 29.03.2000  1.01  Wss | Name changed to MillMasterPlugin
| 13.04.2000  1.01  Wss | Languages Spanish, Portuguese, Italian added
| 19.06.2000  1.01  Wss | After closing CustShiftSel dialog update registry
| 23.08.2000  1.00  Wss | ShowLastEmptyGroup implemented
| 02.03.2001  1.00  Wss | Language French implemented and flag added in ToolbarBmp.res
| 23.01.2002  1.00  Wss | Menu MillMaster Help changed to WindingMaster Help
| 10.07.2002  1.00  Wss | - Languages ChineseTrad and ChineseSimpl implemented
                          - Language switch changed to use of GlossareLanguage type
                          - Bitmap added to command Customer Options for use in Toolbar
| 10.07.2003  1.00  Wss | WindingMaster replaced with MillMaster in Help
| 16.09.2003  1.00  Wss | Help per Context f�r Saal�bersicht aufrufen und nicht mehr mit 0
| 17.09.2003  1.00  Wss | T�rkisch hinzugef�gt
| 20.01.2004  1.00  Wss | - QOfflimit Parameter entfernt
                          - gApplicationName auf 'MillMasterPlugin' gesetzt
| 27.01.2004  1.00  Wss | Bitmapname f�r Hilfe angegeben
|=========================================================================================*)
unit MillMasterPluginIMPL;

interface

uses
  ComObj, ActiveX, BARCOPLUGIN_TLB, mmDictionary, IvDictio, mmEventLog, Windows,
  LoepfeLibrary_TLB, StdVcl, FloorAutoObjectIMPL, mmStringList, Graphics,
  Messages, Repitmob_TLB;

type
  TMillMasterPlugin = class(TFloorAutoObject, IMillMasterPlugin, INotify)
  private
    mMessageID: DWord;
    mMessageHWnd: HWnd;
    procedure RefreshFloor;
  protected
    procedure WndProc(var Message: TMessage);
    // INotify interface for refreshing Floor
    function IsIDUsed(vnID: OleVariant; var bstrUsedIn: WideString): WordBool; safecall;
    procedure Change(const bstrUserName: WideString); safecall;
    procedure IsAwake; safecall;
  public
    destructor Destroy; override;
    function ExecuteCommand(aCommandID: Integer; aMachines: String; aProdID: DWord): SYSINT; override;
    procedure Initialize; override;
  end;

implementation // 15.07.2002 added mmMBCS to imported units
{$R Toolbar.res}

uses
  mmMBCS,

  mmCS, MMMessages, BaseGlobal, mmEdit,
  AxCtrls, ComServ, Dialogs, sysutils, Classes, mmRegistry, HostlinkDef, LoepfeGlobal,
  Forms, Controls, MMLogin, CustShiftSel, HostlinkShare, MMHtmlHelp;

type
  TCommandID = (
    ciMMHelp,
    ciFloorHelp,
    ciCustOptions,
    ciLogin,
    ciLangGerman,
    ciLangEnglish,
    ciLangSpanish,
    ciLangPortuguese,
    ciLangItalian,
    ciLangFrench,
    ciLangChineseTrad,
    ciLangChineseSimpl,
    ciLangTurkish
  );

const
  // message strings for dialogbox
  cMsgCloseFloor = '(*)Sie muessen die Floor nun beenden.'; //ivlm

  // menu definition
  cFloorMenuDef: Array[0..13] of TFloorMenuRec = (
    // --- Help: MillMaster ---
    (CommandID: Ord(ciMMHelp); SubMenu: 0; MenuBmpName: 'HELP';
     MenuText: '(*)MillMaster'; MenuHint: '(*)MillMaster Hilfe'; MenuStBar: '(*)MillMaster Hilfe'; //ivlm
     ToolbarText: 'WindingMaster Help';
     MenuGroup: mgMain; MenuTyp: mtHelp; MenuState: msEnabled),

    // --- Loepfe Extra Menu ---
    (CommandID: 0; SubMenu: -1; MenuBmpName: '';
     MenuText: '(*)Extra'; MenuHint: '(*)Extra'; MenuStBar: '(*)Extra'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
    // Login
    (CommandID: Ord(ciLogin); SubMenu: 0; MenuBmpName: 'LOGIN';
     MenuText: '(*)Anmelden'; MenuHint: '(*)Als anderen MillMaster Benutzer anmelden'; MenuStBar: '(*)Benutzer anmelden'; //ivlm
     ToolbarText: 'MillMaster Login';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
    // Customer shift selection
{$IFDEF MMLinkDummy}
    (CommandID: Ord(ciMMHelp); SubMenu: 0; MenuBmpName: '';
     MenuText: '(*)MillMaster'; MenuHint: '(*)MillMaster Hilfe'; MenuStBar: '(*)MillMaster Hilfe'; //ivlm
     ToolbarText: 'WindingMaster Help';
     MenuGroup: mgMachine; MenuTyp: mtMachine; MenuState: msEnabled; MenuAvailable: maAll),
{$ELSE}
    (CommandID: Ord(ciCustOptions); SubMenu: 0; MenuBmpName: 'SETTINGS';
     MenuText: '(*)Kunden Optionen'; MenuHint: '(*)Kundenspezifische Einstellungen'; MenuStBar: '(*)Kunden Optionen'; //ivlm
     ToolbarText: 'Customer options';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
{$ENDIF}
    // Language SubMenu
    (CommandID: 0; SubMenu: 9;
     MenuText: '(*)Sprache'; MenuHint: '(*)Sprache waehlen'; MenuStBar: '(*)Eine Sprache waehlen'; //ivlm
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // German
      (CommandID: Ord(ciLangGerman); SubMenu: 0; MenuBmpName: 'GERMAN';
       MenuText: '(*)Deutsch'; MenuHint: '(*)Deutsch'; MenuStBar: '(*)Deutsch'; //ivlm
       ToolbarText: 'Language German';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // English
      (CommandID: Ord(ciLangEnglish); SubMenu: 0; MenuBmpName: 'ENGLISH';
       MenuText: '(*)Englisch'; MenuHint: '(*)Englisch'; MenuStBar: '(*)Englisch'; //ivlm
       ToolbarText: 'Language English';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Spanish
      (CommandID: Ord(ciLangSpanish); SubMenu: 0; MenuBmpName: 'SPANISH';
       MenuText: '(*)Spanisch'; MenuHint: '(*)Spanisch'; MenuStBar: '(*)Spanisch'; //ivlm
       ToolbarText: 'Language Spanish';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Portuguese
      (CommandID: Ord(ciLangPortuguese); SubMenu: 0; MenuBmpName: 'PORTUGUESE';
       MenuText: '(*)Portugiesisch'; MenuHint: '(*)Portugiesisch'; MenuStBar: '(*)Portugiesisch'; //ivlm
       ToolbarText: 'Language Portuguese';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Italian
      (CommandID: Ord(ciLangItalian); SubMenu: 0; MenuBmpName: 'ITALIAN';
       MenuText: '(*)Italienisch'; MenuHint: '(*)Italienisch'; MenuStBar: '(*)Italienisch'; //ivlm
       ToolbarText: 'Language Italian';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // French
      (CommandID: Ord(ciLangFrench); SubMenu: 0; MenuBmpName: 'FRENCH';
       MenuText: '(*)Franzoesisch'; MenuHint: '(*)Franzoesisch'; MenuStBar: '(*)Franzoesisch'; //ivlm
       ToolbarText: 'Language French';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Turkish
      (CommandID: Ord(ciLangTurkish); SubMenu: 0; MenuBmpName: 'TURKISH';
       MenuText: '(*)Tuerkisch'; MenuHint: '(*)Tuerkisch'; MenuStBar: '(*)Tuerkisch'; //ivlm
       ToolbarText: 'Language Turkish';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Chinese Traditional
      (CommandID: Ord(ciLangChineseTrad); SubMenu: 0; MenuBmpName: 'CHINESE';
       MenuText: '(*)Chinesisch Trad.'; MenuHint: '(*)Chinesisch Trad.'; MenuStBar: '(*)Chinesisch Trad.'; //ivlm
       ToolbarText: 'Language ChineseTrad';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled),
      // Chinese Simplified
      (CommandID: Ord(ciLangChineseSimpl); SubMenu: 0; MenuBmpName: 'CHINESE';
       MenuText: '(*)Chinesisch Simpl.'; MenuHint: '(*)Chinesisch Simpl.'; MenuStBar: '(*)Chinesisch Simpl.'; //ivlm
       ToolbarText: 'Language ChineseSimpl';
       MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled)

{
    // Temp for ClearerAssistant
    ,(CommandID: 0; SubMenu: 0;
     MenuText: '-'; MenuHint: '-'; MenuStBar: '-';
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled)
    ,(CommandID: 0; SubMenu: 0;
     MenuText: 'Clearer Assistant'; MenuHint: 'Clearer Assistant'; MenuStBar: 'Clearer Assistant'; //
     ToolbarText: '';
     MenuGroup: mgMain; MenuTyp: mtLoepfe; MenuState: msEnabled)
{}
  );

//------------------------------------------------------------------------------
// TMMLinkPlugin
//------------------------------------------------------------------------------
procedure TMillMasterPlugin.Change(const bstrUserName: WideString);
begin
  CodeSite.SendString('Change', bstrUserName);
end;
//------------------------------------------------------------------------------
destructor TMillMasterPlugin.Destroy;
begin
  DeallocateHWnd(mMessageHWnd);
  inherited Destroy;
end;
//------------------------------------------------------------------------------
function TMillMasterPlugin.ExecuteCommand(aCommandID: Integer; aMachines: String; aProdID: DWord): SYSINT;
begin
  Result := 0;  // default for non-modal functionality
  case TCommandID(aCommandID) of
    ciMMHelp: begin
        with TMMHtmlHelp.Create(Nil) do
        try
          LoepfeIndex := Dictionary.LoepfeIndex;
          AutoConfig := True;
          Loaded;
          ShowHelpContext(GetHelpContext('Floor\FLOOR_STATUS_WHAT.htm'));
//          ShowHelpContext(0);
        finally
          Free;
        end;
      end;
    ciLangGerman: begin
        Dictionary.GlossaryLanguage := glGerman;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangEnglish: begin
        Dictionary.GlossaryLanguage := glEnglish;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangSpanish: begin
        Dictionary.GlossaryLanguage := glSpanish;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangPortuguese: begin
        Dictionary.GlossaryLanguage := glPortuguese;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangItalian: begin
        Dictionary.GlossaryLanguage := glItalian;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangFrench: begin
        Dictionary.GlossaryLanguage := glFrench;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangTurkish: begin
        Dictionary.GlossaryLanguage := glTurkish;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangChineseTrad: begin
        Dictionary.GlossaryLanguage := glChineseTrad;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciLangChineseSimpl: begin
        Dictionary.GlossaryLanguage := glChineseSimpl;
        MessageDlg(Translate(cMsgCloseFloor), mtInformation, [mbOK], 0);
      end;
    ciCustOptions: begin
        with TfrmCustOptions.Create(Nil) do
        try
          Dictionary := Self.Dictionary;
          ShiftCalID := gMachConfig.CustShiftCalID;
          ShiftFrom  := gMachConfig.ShiftSelection[dsCustShiftSel].TimeFrom;
          ShiftTo    := gMachConfig.ShiftSelection[dsCustShiftSel].TimeTo;
          cobTrendDefault.ItemIndex    := Integer(gMachConfig.DefaultTrendSelection)-1;
          edOfflimitTime.Value         := gMachConfig.OfflimitTime;
          cbShowDetail.Checked         := gMachConfig.CustShiftDetail;
          cbShowEmptyProdGroup.Checked := gMachConfig.ShowEmptyProdGroup;
          cbShowStyleColor.Checked     := gMachConfig.ShowStyleColor;

          mChangeMode := gMachConfig.ChangeMode;
          if ShowModal = mrOK then begin
            gMachConfig.CustShiftCalID  := ShiftCalID;
            gMachConfig.ChangeMode      := mChangeMode;
            gMachConfig.CustShiftDetail := cbShowDetail.Checked;
            if vbShiftFrom.Text <> '' then
              gMachConfig.ShiftSelection[dsCustShiftSel].TimeFrom := ShiftFrom;

            if vbShiftTo.Text <> '' then
              gMachConfig.ShiftSelection[dsCustShiftSel].TimeTo   := ShiftTo;

            gMachConfig.DefaultTrendSelection := TDataSelection(cobTrendDefault.ItemIndex + 1);
            gMachConfig.OfflimitTime          := edOfflimitTime.AsInteger;
            gMachConfig.ShowEmptyProdGroup    := cbShowEmptyProdGroup.Checked;
            gMachConfig.ShowStyleColor        := cbShowStyleColor.Checked;

            // update now registry
            gMachConfig.UpdateRegistry;
//            RefreshFloor;
          end;
        finally
          Free;
        end;
      end;
    ciLogin: begin
{$IFDEF MMLinkPluginTest}
        MMBroadCastMessage(mMessageID, cMMRefresh, 0);
{$ELSE}
        with TMMLogin.Create(Nil) do
        try
          Dictionary := Self.Dictionary;
          if Execute then begin
            NotifyFloor(BPNE_MENUCHANGED, 0);
          end;
        finally
          Free;
        end;
{$ENDIF}
      end;
  else
  end;
end;
//------------------------------------------------------------------------------
procedure TMillMasterPlugin.Initialize;
begin
  EnterMethod('Initialize');
  inherited Initialize;

  // check the languages and define the availability of Chinese menu
  CodeSite.SendMsg('Check for chinese');
  if Dictionary.LoepfeSystemLanguage in [glChineseTrad, glChineseSimpl, glTurkish] then begin
    // Turn off menues other than english and chinsese
    cFloorMenuDef[5].MenuState  := msDisabled; //msHide; // German
    cFloorMenuDef[7].MenuState  := msDisabled; //msHide; // Spanish
    cFloorMenuDef[8].MenuState  := msDisabled; //msHide; // Portuguese
    cFloorMenuDef[9].MenuState  := msDisabled; //msHide; // Italian
    cFloorMenuDef[10].MenuState := msDisabled; //msHide; // French
    if Dictionary.LoepfeSystemLanguage = glTurkish then begin
      cFloorMenuDef[12].MenuState := msDisabled; //msHide; // ChineseTrad
      cFloorMenuDef[13].MenuState := msDisabled; //msHide; // ChineseSimpl
    end else begin
      cFloorMenuDef[11].MenuState := msDisabled; //msHide; // Turkish
    end;
  end else begin
    // Turn off the menu for Chinese because the system doesn't support it
    cFloorMenuDef[11].MenuState := msDisabled; //msHide; // Turkish
    cFloorMenuDef[12].MenuState := msDisabled; //msHide; // ChineseTrad
    cFloorMenuDef[13].MenuState := msDisabled; //msHide; // ChineseSimpl
  end;

  CodeSite.SendMsg('Call of InitMenu()');
  InitMenu(cFloorMenuDef);

  // create window handle for receiving message from MMClient
  mMessageID := RegisterWindowMessage(cMsgAppl);
  mMessageHWnd := AllocateHWnd(WndProc);
end;
//------------------------------------------------------------------------------
procedure TMillMasterPlugin.IsAwake;
begin
//  CodeSite.SendMsg('IsAwake');
end;
//------------------------------------------------------------------------------
function TMillMasterPlugin.IsIDUsed(vnID: OleVariant;
  var bstrUsedIn: WideString): WordBool;
begin
//  CodeSite.SendMsg('IsIDUsed');
  Result := False;
end;
//------------------------------------------------------------------------------
procedure TMillMasterPlugin.RefreshFloor;
var
  xRPIObj: IReportItemSetup;
  xNotifySrc: INotifySrc;
begin
  CodeSite.SendMsg('Refresh Message');
  try
    xRPIObj := CreateOLEObject('BARCO.REPORTITEMSETUP.1') as IReportItemSetup;
    if Assigned(xRPIObj) then begin
      xRPIObj.QueryInterface(IID_INotifySrc, xNotifySrc);
      if Assigned(xNotifySrc) then
      try
        xNotifySrc.AddUser(Self, 'MillMasterPlugin');
        xRPIObj.BeginTransaction(Self);
        CodeSite.SendMsg('Refresh: BeginTransaction');
        sleep(100);
        xRPIObj.EndTransaction(Self);
        CodeSite.SendMsg('Refresh: EndTransaction');
      finally
        xNotifySrc.RemoveUser(Self);
        xNotifySrc := Nil;
      end;
      xRPIObj := Nil;
    end;
  except
    on e:Exception do begin
      WriteToEventLog('Floor refresh failed: ' + e.Message, 'MillMasterPlugin: ', etError);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TMillMasterPlugin.WndProc(var Message: TMessage);
begin
  if (Message.Msg = mMessageID) then begin
    case Message.WParam of
      cMMRefresh: RefreshFloor;
    else
    end;
  end else
    DefaultHandler(Message);
end;
//------------------------------------------------------------------------------

initialization
  gApplicationName := 'CustomerOptions';
  CodeSite.Enabled := CodeSiteEnabled('MillMasterPlugin');
  InitializeFloorPlugin(TMillMasterPlugin, CLASS_MillMasterPlugin);
finalization
end.

