inherited frmCustOptions: TfrmCustOptions
  Left = 669
  Top = 120
  Caption = '(*)Kunden Optionen'
  ClientHeight = 403
  ClientWidth = 388
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  inherited bOK: TmmButton
    Left = 194
    Top = 370
    Action = acOK
    TabOrder = 2
  end
  inherited bCancel: TmmButton
    Left = 294
    Top = 370
  end
  object mmPanel1: TmmPanel
    Left = 0
    Top = 0
    Width = 388
    Height = 361
    Align = alTop
    BevelOuter = bvNone
    BorderWidth = 5
    TabOrder = 0
    object mmGroupBox2: TmmGroupBox
      Left = 5
      Top = 5
      Width = 378
      Height = 171
      Align = alTop
      Caption = '(*)Kundenzeitbereiche'
      TabOrder = 0
      CaptionSpace = True
      object laShiftCal: TmmLabel
        Left = 8
        Top = 17
        Width = 100
        Height = 13
        AutoSize = False
        Caption = '(*)Schicht&kalender'
        Enabled = False
        FocusControl = vbShiftCal
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laShiftFrom: TmmLabel
        Left = 8
        Top = 65
        Width = 100
        Height = 13
        AutoSize = False
        Caption = '(*)Schicht &von'
        Enabled = False
        FocusControl = vbShiftFrom
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laTrendDefault: TmmLabel
        Left = 10
        Top = 120
        Width = 103
        Height = 13
        Caption = '(*)&Trend Einzelansicht'
        FocusControl = cobTrendDefault
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object laShiftTo: TmmLabel
        Left = 176
        Top = 65
        Width = 100
        Height = 13
        AutoSize = False
        Caption = '(*)Schicht &bis'
        Enabled = False
        FocusControl = vbShiftTo
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object vbShiftCal: TDBVisualBox
        Left = 8
        Top = 32
        Width = 145
        Height = 21
        Color = clWindow
        Enabled = False
        ItemHeight = 13
        TabOrder = 0
        Visible = True
        OnChange = vbShiftCalChange
        AutoLabel.Control = laShiftCal
        AutoLabel.LabelPosition = lpTop
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        DataField = 'c_shiftcal_name'
        DataSource = dsShiftCal
        KeyField = 'c_shiftcal_id'
      end
      object vbShiftFrom: TDBVisualBox
        Left = 8
        Top = 80
        Width = 145
        Height = 21
        Color = clWindow
        Enabled = False
        ItemHeight = 13
        TabOrder = 1
        Visible = True
        OnChange = vbShiftFromChange
        AutoLabel.Control = laShiftFrom
        AutoLabel.LabelPosition = lpTop
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        DataField = 'c_shift_start'
        DataSource = dsShiftFrom
        KeyField = 'c_shift_id'
      end
      object cobTrendDefault: TmmComboBox
        Left = 10
        Top = 135
        Width = 145
        Height = 21
        Color = clWindow
        ItemHeight = 13
        TabOrder = 3
        Visible = True
        AutoLabel.Control = laTrendDefault
        AutoLabel.LabelPosition = lpTop
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
      end
      object vbShiftTo: TDBVisualBox
        Left = 176
        Top = 80
        Width = 145
        Height = 21
        Color = clWindow
        Enabled = False
        ItemHeight = 13
        TabOrder = 2
        Visible = True
        OnChange = vbShiftToChange
        AutoLabel.Control = laShiftTo
        AutoLabel.LabelPosition = lpTop
        Edit = True
        ReadOnlyColor = clInfoBk
        ShowMode = smNormal
        DataField = 'c_shift_start'
        DataSource = dsShiftTo
        KeyField = 'c_shift_id'
      end
      object cbShowDetail: TmmCheckBox
        Left = 176
        Top = 32
        Width = 146
        Height = 17
        Caption = '(25)Zeige Schichtdetail'
        TabOrder = 4
        Visible = True
        OnClick = cbShowDetailClick
        AutoLabel.LabelPosition = lpLeft
      end
    end
    object mmGroupBox1: TmmGroupBox
      Left = 5
      Top = 184
      Width = 188
      Height = 81
      Anchors = [akLeft, akTop, akRight]
      Caption = '(*)Offlimit Filterbedingung'
      TabOrder = 1
      CaptionSpace = True
      object laOfflimitTime: TmmLabel
        Left = 10
        Top = 20
        Width = 131
        Height = 13
        Caption = '(30)&Anzeigeunterdrueckung'
        FocusControl = edOfflimitTime
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object edOfflimitTime: TNumEdit
        Left = 10
        Top = 35
        Width = 85
        Height = 21
        Decimals = 2
        Digits = 12
        Masks.PositiveMask = '0 min'
        Max = 99999
        NumericType = ntGeneral
        OnChange = edOfflimitTimeChange
        OnKeyDown = edOfflimitTimeKeyDown
        TabOrder = 0
        UseRounding = True
        Validate = False
        ValidateString = 
          'Wert ist nicht im erlaubten Eingabebereich.'#10#13'Bereich von %s bis ' +
          '%s'
      end
    end
    object gbMisc: TmmGroupBox
      Left = 5
      Top = 278
      Width = 378
      Height = 75
      Anchors = [akLeft, akTop, akRight]
      Caption = '(*)Verschiedenes'
      TabOrder = 2
      CaptionSpace = True
      object cbShowEmptyProdGroup: TmmCheckBox
        Left = 10
        Top = 24
        Width = 215
        Height = 17
        Caption = '(40)Leere Produktionsgruppen anzeigen'
        TabOrder = 0
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
      object cbShowStyleColor: TmmCheckBox
        Left = 8
        Top = 48
        Width = 215
        Height = 17
        Caption = '(40)Zeige Artikelfarben'
        TabOrder = 1
        Visible = True
        AutoLabel.LabelPosition = lpLeft
      end
    end
  end
  object mmButton1: TmmButton
    Left = 8
    Top = 370
    Width = 87
    Height = 25
    Action = acSecurity
    TabOrder = 3
    Visible = True
    AutoLabel.LabelPosition = lpLeft
  end
  object dsShiftCal: TmmDataSource
    DataSet = dseShiftCal
    Left = 72
    Top = 24
  end
  object dsShiftFrom: TmmDataSource
    DataSet = dseShiftFrom
    Left = 72
    Top = 72
  end
  object dsShiftTo: TmmDataSource
    DataSet = dseShiftTo
    Left = 233
    Top = 73
  end
  object mTranslator: TmmTranslator
    Left = 307
    Top = 3
    TargetsData = (
      1
      4
      (
        '*'
        'Hint'
        0)
      (
        '*'
        'Caption'
        0)
      (
        '*'
        'Items'
        0)
      (
        '*'
        'Filter'
        0))
  end
  object conDefault: TmmADOConnection
    ConnectionString = 
      'Provider=SQLOLEDB.1;Data Source=WETWSS2000;Initial Catalog=MM_Wi' +
      'nding;Password=netpmek32;User ID=MMSystemSQL;Auto Translate=Fals' +
      'e'
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    AutoConfig = acAuto
    Left = 341
    Top = 5
  end
  object dseShiftTo: TmmADODataSet
    Connection = conDefault
    CommandText = 
      'SELECT '#13#10'  c_shiftcal_id, '#13#10'  c_shift_id,'#13#10'  c_shift_start'#13#10#13#10'FR' +
      'OM '#13#10'  t_shift'#13#10#13#10'WHERE c_shiftcal_id = :c_shiftcal_id'#13#10'AND c_sh' +
      'ift_start >= :c_shift_start'#13#10'AND c_shift_start <= GetDate()'#13#10#13#10'O' +
      'RDER BY '#13#10'  c_shift_start DESC'#13#10
    DataSource = dsShiftFrom
    Parameters = <
      item
        Name = 'c_shiftcal_id'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end
      item
        Name = 'c_shift_start'
        DataType = ftDateTime
        Precision = 16
        Size = 16
        Value = Null
      end>
    Left = 200
    Top = 72
  end
  object dseShiftFrom: TmmADODataSet
    Connection = conDefault
    CommandText = 
      'SELECT '#13#10'  c_shiftcal_id, '#13#10'  c_shift_id,'#13#10'  c_shift_start'#13#10#13#10'FR' +
      'OM '#13#10'  t_shift'#13#10#13#10'WHERE c_shiftcal_id = :c_shiftcal_id'#13#10'AND c_sh' +
      'ift_start <= GetDate()'#13#10#13#10'ORDER BY '#13#10'  c_shift_start DESC'#13#10
    DataSource = dsShiftCal
    Parameters = <
      item
        Name = 'c_shiftcal_id'
        DataType = ftWord
        Precision = 3
        Size = 1
        Value = Null
      end>
    Left = 40
    Top = 72
  end
  object dseShiftCal: TmmADODataSet
    Connection = conDefault
    CursorType = ctStatic
    CommandText = 'SELECT c_shiftcal_id, c_shiftcal_name'#13#10'FROM t_shiftcal'#13#10
    Parameters = <>
    Left = 40
    Top = 24
  end
  object mmActionList: TmmActionList
    Left = 301
    Top = 310
    object acOK: TAction
      Caption = '(9)OK'
    end
    object acSecurity: TAction
      Caption = '(*)Zugriffkontrolle'
      OnExecute = acSecurityExecute
    end
  end
  object MMSecurityDB: TMMSecurityDB
    Active = True
    CheckApplStart = False
    Left = 232
    Top = 312
  end
  object MMSecurityControl: TMMSecurityControl
    FormCaption = '(*)Kunden Optionen'
    Active = True
    MMSecurityName = 'MMSecurityDB'
    Left = 264
    Top = 312
  end
end
