library MillMasterPlugin;
uses
{$IFDEF MemCheck}
  MemCheck,
{$ENDIF}
  mmMBCS,
  ComServ,
  BASEDialog in '..\..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  mmLogin in '..\..\..\..\LoepfeShares\Common\mmLogin.pas' {FLoginMain},
  HostlinkShare in '..\..\..\Common\HostlinkShare.pas',
  Repitmob_TLB in '..\..\..\..\LoepfeShares\Common\Repitmob_TLB.pas',
  BARCOPICTURELib_TLB in '..\..\..\..\LoepfeShares\Common\BARCOPICTURELib_TLB.pas',
  BARCOPLUGIN_TLB in '..\..\..\..\LoepfeShares\Common\BARCOPLUGIN_TLB.pas',
  LoepfeLibrary_TLB in '..\..\..\..\LoepfeShares\Common\LoepfeLibrary_TLB.pas',
  MillMasterPluginIMPL in 'MillMasterPluginIMPL.pas' {MillMasterPlugin: CoClass},
  FloorAutoObjectIMPL in 'FloorAutoObjectIMPL.pas',
  CustShiftSel in 'CustShiftSel.pas' {frmCustOptions},
  BaseForm in '..\..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm};

{$E dll}

exports
  DllGetClassObject,
  DllCanUnloadNow,
  DllRegisterServer,
  DllUnregisterServer;

{$R *.TLB}

{$R *.RES}
{$R 'Version.res'}

begin
{$IFDEF MemCheck}
  MemChk('MillMasterPlugin');
{$ENDIF}
end.
