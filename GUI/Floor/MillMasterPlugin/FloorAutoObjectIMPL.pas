(*==========================================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------------------
| Filename......: FloorAutoObjectIMPL.pas
| Projectpart...: MillMaster NT
| Subpart.......: -
| Process(es)...: -
| Description...: Implements the IBarcoPlugin interface
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi
|-------------------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------------------
| 28.05.2001  2.01  Wss | - Menu separator supported
                          - Menu Extra moved to  most right position
| 31.05.2001  2.01  Wss | Bug resolved: GetSelectedMachines got the MachIndex instead of MachID
| 11.01.2002  2.03  Wss | Detect closing Floor with Hook functions to close first OLEServers
| 17.07.2002  2.03  Wss | Auf MMGlossary.mld umgestellt
|=========================================================================================*)
unit FloorAutoObjectIMPL;

interface

uses
  ActiveX, ComObj, Graphics, StdVcl, Windows,
  LoepfeLibrary_TLB, BARCOPLUGIN_TLB, mmList, mmDictionary, mmEventLog;

type
  // forward declaration of TBitmap because Windows and Graphics contains a type TBitmap
  TBitmap = class(Graphics.TBitmap);
    
type
  PPluginMenuRec = ^TPluginMenuRec;
  TPluginMenuRec = record
    ServerIndex: Integer;
    CommandIndex: Integer;
    SubMenus: SmallInt;
    MenuBmpName: string;
    MenuText: string;
    MenuHint: string;
    MenuStBar: string;
    ToolbarText: string;
    MenuState: TMenuState;
    MenuGroup: TMenuGroup;
    MenuTyp: TMenuTyp;
    MenuAvailable: TMenuAvailable;
  end;
  

  POLEServerRec = ^TOLEServerRec;
  TOLEServerRec = record
    Name: string;
    Used: Boolean;
    StartIndex: Integer;
    ServerIndex: Integer;
    MenuCount: Integer;
    BitmapCount: Integer;
    Bitmap: TBitmap;
  end;
  

type
  TFloorAutoObjectFactory = class (TAutoObjectFactory)
  public
    constructor Create(ComServer: TComServerObject; AutoClass: TAutoClass; const ClassID: TGUID; Instancing: TClassInstancing; ThreadingModel: TThreadingModel 
            = tmSingle);
    procedure UpdateRegistry(Register: Boolean); override;
  end;
  
  TMenuList = class (TmmList)
  private
    function GetItems(aIndex: Integer): PPluginMenuRec;
  public
    procedure Clear; override;
    property Items[aIndex: Integer]: PPluginMenuRec read GetItems;
  end;
  
  TServerList = class (TmmList)
  private
    function GetItems(aIndex: Integer): POLEServerRec;
  public
    procedure Clear; override;
    property Items[aIndex: Integer]: POLEServerRec read GetItems;
  end;
  
  TFloorAutoObject = class (TAutoObject, IBarcoPlugin, ILoepfePluginNotify)
  private
    fDictionary: TmmDictionary;
    fMenu: TMenuList;
    fParentWndHnd: THandle;
    mCommandID: Integer;
    mEventLog: TEventLogWriter;
    mFloorInterface: IBarcoPluginNotify;
    mMachineIndex: Integer;
    mSectionIndex: Integer;
    mSelectedMachines: string;
    function CheckID(var aID: Integer): Boolean;
    procedure GetAddons;
    function GetMenu(aIndex: Integer): PPluginMenuRec;
    function GetMenuCount: Integer;
    procedure GetMenuInformation(const aIntf: ILoepfePlugin; aServerRec: POLEServerRec);
    function GetSelectedMachines: Boolean;
    procedure MergeBitmap(var aDestBmp: TBitmap; aSrcBmp: TBitmap);
  protected
    procedure ChangeClientStatus(nStatus: CLIENTSTATUS); virtual; safecall;
    procedure ContextHelp(nID: SYSINT; hWndParent: Integer); virtual; safecall;
    function GetCommandStatus(nID: SYSINT): Integer; virtual; safecall;
    function GetCommandString(nID: SYSINT; nFlag: COMMANDSTRING): WideString; virtual; safecall;
    procedure GetPluginMenu(iMenuType: MENUTYPE; var hMenu: Integer); virtual; safecall;
    function GetToolbarBitmap(var pCommandIDs: OleVariant): IDispatch; virtual; safecall;
    procedure InitMenu(aMenu: Array of TFloorMenuRec);
    function InvokeCommand(nID: SYSINT; hWndParent: Integer; const pPluginNotify: IBarcoPluginNotify): SYSINT; virtual; safecall;
    procedure LoepfeNotify(aEvent: TLoepfeNotifyEvent; aParameter: OleVariant); safecall;
    procedure NotifyFloor(aEvent, aCookie: Integer);
    property Dictionary: TmmDictionary read fDictionary write fDictionary;
    property Menu[aIndex: Integer]: PPluginMenuRec read GetMenu;
    property MenuCount: Integer read GetMenuCount;
    property ParentWndHnd: THandle read fParentWndHnd;
  public
    destructor Destroy; override;
    function ExecuteCommand(aCommandID: Integer; aMachines: String; aProdID: DWord): SYSINT; virtual;
    procedure Initialize; override;
    function Transl(aString: String): string;
  end;
  

//------------------------------------------------------------------------------
procedure InitializeFloorPlugin(AutoClass: TAutoClass; const ClassID: TGUID);

implementation

uses
  mmMBCS, mmCS,
  AxCtrls, Classes, ComServ, Controls, Dialogs, Forms, Messages, SysUtils, ivDictio,
  LoepfeGlobal, mmRegistry, mmStringList, HostlinkShare, HostlinkDef;

const
  cFloorExitCommandID = $E141;

var
  lServerList: TServerList;
  lFloorWindowHandle: DWord;
  lMsgHookHandle: DWord;
  lCBTHookHandle: DWord;

//******************************************************************************
// public procedures
//******************************************************************************
procedure InitializeFloorPlugin(AutoClass: TAutoClass; const ClassID: TGUID);
begin
  // set default COM Servername to LOEPFE -> AppName = 'LOEPFE.xxxx'
  ComServ.ComServer.SetServerName('LOEPFE');

  TFloorAutoObjectFactory.Create(
    ComServer,
    AutoClass,
    ClassID,
    ciMultiInstance,
    tmBoth);
//    tmFree);
end;
//------------------------------------------------------------------------------
procedure CloseOLEForms;
var
  i: Integer;
  xIntf: ILoepfePlugin;
  xOldCursor: TCursor;
begin
  EnterMethod('CloseOLEForms');
  xOldCursor    := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  with lServerList do
  try
    CodeSite.SendInteger('ServerCount', Count);
    for i:=Count-1 downto 0 do begin
      with Items[i]^ do begin
        if (Name <> '') and Used then
        try
          try
            Used := False;
            xIntf := CreateOleObject(Name) as ILoepfePlugin;
            xIntf.Close;
          finally
            xIntf := Nil;
            CodeSite.SendString('OLE Server closed: ', Name);
          end;
        except
          on e:Exception do
            CodeSite.SendError('CloseServer: ' + e.Message);
        end;
      end;
    end; // for i
  finally
    Screen.Cursor := xOldCursor;
  end; // with
end;
//------------------------------------------------------------------------------
function MsgProcHookProc(nCode: Integer; wParam: WParam; lParam: LParam): LResult; stdcall;
var
  xStruct: PMsg;
begin
  Result := 0;
  // if code is less than zero, the hook procedure must pass the message to the CallNextHookEx
  if nCode < 0 then
    Result := CallNextHookEx(lMsgHookHandle, nCode, wParam, lParam)
  else
  // if code is HC_ACTION, the hook procedure must process the message
  if (nCode = HC_ACTION) {and (wParam <> 0) }then begin
//    if (nCode >= 0) and (wParam <> 0) then begin
      xStruct := Pointer(lParam);
      with xStruct^ do begin
        if (hwnd = lFloorWindowHandle) and (Message = WM_COMMAND) and (wParam = cFloorExitCommandID) then
          CloseOLEForms;
      end;
//    end;
  end;

//  if nCode < 0 then
//    Result := CallNextHookEx(lMsgHookHandle, nCode, wParam, lParam)
//  else begin
//    Result := 0;
//    if (nCode >= 0) and (wParam <> 0) then begin
//      xStruct := Pointer(lParam);
//      with xStruct^ do begin
//        if (hwnd = lFloorWindowHandle) and (Message = WM_COMMAND) and (wParam = cFloorExitCommandID) then
//          CloseOLEForms;
//      end;
//    end;
//  end;
end;
//------------------------------------------------------------------------------
function CBTProcHookProc(nCode: Integer; wParam: WParam; lParam: LParam): LResult; stdcall;
begin
  Result := 0;
  // if code is less than zero, the hook procedure must pass the message to the CallNextHookEx
  if nCode < 0 then
    Result := CallNextHookEx(lCBTHookHandle, nCode, wParam, lParam)
  else if (nCode = HCBT_SYSCOMMAND) and (GetActiveWindow = lFloorWindowHandle) and (wParam = SC_CLOSE) then
      CloseOLEForms;
end;
//------------------------------------------------------------------------------
function EnumWindowProc(aHWnd: HWND; lParam: LPARAM): Boolean;
var
  xCharArr: Array[0..100] of Char;
begin
  if GetWindowText(aHWnd, xCharArr, 100) > 0 then
    CodeSite.SendString('GetWindowText', StrPas(xCharArr));

  if GetClassName(aHWnd, xCharArr, 100) > 0 then
    CodeSite.SendString('GetClassName', StrPas(xCharArr));

  Result := True;
end;
//------------------------------------------------------------------------------
//:---------------------------------------------------------------------------
//:--- Class: TFloorAutoObject
//:---------------------------------------------------------------------------
destructor TFloorAutoObject.Destroy;
begin
  CodeSite.SendMsg('TFloorAutoObject.Destroy');
  fMenu.Free;
  fDictionary.Free;
  mEventLog.Free;
  
  inherited Destroy;
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.ChangeClientStatus(nStatus: CLIENTSTATUS);
begin
  case nStatus of
    BPCCS_MACHINESELCHANGE: begin
        GetSelectedMachines;
      end;
  else
  end;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.CheckID(var aID: Integer): Boolean;
begin
  // menu index in list starts at 0, menu index in Floor starts at 1
  if aID >= 32768 then dec(aID, 32768)
                  else dec(aID, 1);
  
  Result := (aID >= 0) and (aID < GetMenuCount);
  if not Result then
    raise EAbort.Create('Invalid ID of menu item.');
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.ContextHelp(nID: SYSINT; hWndParent: Integer);
begin
  CodeSite.SendFmtMsg('ContextHelp: ID=%d, WndParent=%d', [nID, hWndParent]);
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.ExecuteCommand(aCommandID: Integer; aMachines: String; aProdID: DWord): SYSINT;
begin
  Result := 0;
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.GetAddons;
var
  xStrings: TmmStringList;
  i: Integer;
  xServerIndex: Integer;
  xSrvRec: POLEServerRec;
  xIntf: ILoepfePlugin;
  xSrvName: string;
  xDisabled: Boolean;
  
  //............................................................................
  function GetBitmap: TBitmap;
  var
    xPointer: Pointer;
    xVar: Variant;
    xSize: Integer;
    xMemStream: TMemoryStream;
  begin
    Result     := Nil;
    xMemStream := Nil;
    try
      try
        xVar := xIntf.GetToolbarBitmap;
        xPointer := VarArrayLock(xVar);
        xSize := VarArrayHighBound(xVar, 1) + 1;
        if xSize > 0 then begin
          xMemStream := TMemoryStream.Create;
          xMemStream.WriteBuffer(xPointer^, xSize);
          xMemStream.Position := 0;
  
          Result := TBitmap.Create;
          Result.LoadFromStream(xMemStream);
        end;
      finally
        FreeAndNil(xMemStream);
        VarArrayUnlock(xVar);
      end;
    except
      on e:Exception do begin
        FreeAndNil(Result);
        mEventLog.Write(etError, Format('MMLinkPlugin: GetBitmap %s failed: %s', [xSrvName, e.Message]));
      end;
    end;
  end;
  //............................................................................
  
begin
  with TmmRegistry.Create do
  try
    RootKey := HKEY_LOCAL_MACHINE;
    if OpenKeyReadOnly(cRegLoepfeAddons) then begin
      xStrings := TmmStringList.Create;
      GetKeyNames(xStrings);
  
  //      CodeSite.SendInteger('MMLinkPlugin: Number of Plugins', xStrings.Count);
      xServerIndex := 1;
  //      xServerIndex := 0;
      for i:=0 to xStrings.Count-1 do begin
        xSrvName := Format('%s\%s', [cRegLoepfeAddons, xStrings.Strings[i]]);
        xDisabled := GetRegBoolean(cRegLM, xSrvName, 'Disabled', False);
        if not xDisabled then begin
            // define some initial values of this OLE Automation
          xSrvName := xStrings.Strings[i];
          try
            xIntf := CreateOLEObject(xSrvName) as ILoepfePlugin;
  //            CodeSite.SendString('MMLinkPlugin: OLEObject created', xSrvName);
          except
            on e:Exception do begin
              xIntf := Nil;
              mEventLog.Write(etError, Format('MMLinkPlugin: CreateOLEObject %s failed: %s', [xSrvName, e.Message]));
            end;
          end;
  
          if Assigned(xIntf) then
          try
            New(xSrvRec);
              // Set the OLE Server Name
            xSrvRec^.Name        := xSrvName;
  //            xSrvRec^.Intf        := Nil;
            xSrvRec^.Used        := False;
            xSrvRec^.ServerIndex := xServerIndex;
            xSrvRec^.StartIndex  := fMenu.Count;
            xSrvRec^.BitmapCount := 0;
              // fill in menu definition
            GetMenuInformation(xIntf, xSrvRec);
            xSrvRec^.Bitmap      := GetBitmap;
  
            lServerList.Add(xSrvRec);
  //            mServerList.Add(xSrvRec);
            inc(xServerIndex);
              //dispose the handle;
            xIntf := Nil;
          except
          end;
        end;  // if not xDiabled
      end;
      xStrings.Free;
  
    end;
  finally
    Free;
  end;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetCommandStatus(nID: SYSINT): Integer;
  
  {
  var
    xOK: Boolean;
  {}
  
begin
  Result := MFS_DISABLED;
  if CheckID(nID) then begin
    case fMenu.Items[nID].MenuState of
      msEnabled:  Result := MFS_ENABLED;
      msDisabled: Result := MFS_DISABLED;
      msHide:     Result := MFS_DISABLED;
    else
      Result := MFS_ENABLED;
    end;
  end;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetCommandString(nID: SYSINT; nFlag: COMMANDSTRING): WideString;
var
  xStr: WideString;
begin
  Result := '';
  if nFlag = BPGCS_PLUGINNAME then
    Result := 'MillMaster'
  else if CheckID(nID) then begin
    case nFlag of
      BPGCS_HELPTEXT: begin
          xStr := Transl(Menu[nID]^.MenuStBar) + #10 + Transl(Menu[nID]^.MenuHint);
          Result := SysAllocString(PWideChar(xStr));
        end;
  
      BPGCS_VERB:
        Result := Menu[nID]^.ToolbarText;
      BPGCS_TITLE,
      BPGCS_STATUSBAR:
        Result := Transl(Menu[nID]^.MenuStBar);
    else
      Result := '';
    end;
  end else
    Result := '';
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetMenu(aIndex: Integer): PPluginMenuRec;
begin
  Result := fMenu.Items[aIndex];
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetMenuCount: Integer;
begin
  Result := fMenu.Count;
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.GetMenuInformation(const aIntf: ILoepfePlugin; aServerRec: POLEServerRec);
var
  i: Integer;
  xVar: OLEVariant;
  xRec: PPluginMenuRec;
begin
  try
    xVar := aIntf.MenuValues;
    aServerRec^.MenuCount := VarArrayHighBound(xVar, 1) + 1;
  //    CodeSite.SendInteger('MMLinkPlugin: MenuCount', aServerRec^.MenuCount);
  except
    on e: Exception do begin
      aServerRec^.MenuCount := 0;
  //      CodeSite.SendString('MMLinkPlugin', e.Message + '  ' + GetLastErrorText);
      mEventLog.Write(etError, Format('MMLinkPlugin: GetMenuInformation for %s failed: %s', [aServerRec^.Name, e.Message]));
    end;
  end;
  
  for i:=0 to aServerRec^.MenuCount-1 do begin
    // create new menu item record
    new(xRec);
    xRec^.CommandIndex := i;
    xRec^.ServerIndex  := aServerRec^.ServerIndex;
    // fill in menu information
    xRec^.SubMenus    := xVar[i, mvSubMenus];
    xRec^.MenuBmpName := xVar[i, mvMenuBmpName];
    xRec^.MenuText     := xVar[i, mvMenuText];
    xRec^.MenuHint     := xVar[i, mvMenuHint];
    xRec^.MenuStBar    := xVar[i, mvMenuStBar];
    xRec^.ToolbarText  := xVar[i, mvToolbarText];
    xRec^.MenuTyp     := xVar[i, mvMenuTyp];
    xRec^.MenuGroup   := xVar[i, mvMenuGroup];
    xRec^.MenuState   := xVar[i, mvMenuState];
    xRec^.MenuAvailable := xVar[i, mvMenuAvailable];
  
    // increment number of supported bitmaps
    if xRec^.MenuBmpName <> '' then
      inc(aServerRec^.BitmapCount);
  
    // add menu item record to menu list
    fMenu.Add(xRec);
  end;
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.GetPluginMenu(iMenuType: MENUTYPE; var hMenu: Integer);
  
    //..........................................................................
  procedure InsertSubMenu(aMenuGroup: TMenuGroup; aMenuTyp: TMenuTyp);
  var
    i, j: Integer;
    xID: DWord;
    xInsHnd: THandle;
    xPopHnd: THandle;
    xLoepfeHnd: THandle;
    xStr: String;
    xPChar: PChar;
  begin
    j := 0;
    xInsHnd    := 0;
    xLoepfeHnd := 0;
    for i:=0 to MenuCount-1 do begin
      with Menu[i]^ do begin
        if (aMenuGroup = MenuGroup) and (MenuTyp = aMenuTyp) then begin
          xStr := Transl(MenuText);
          xPChar := PChar(xStr);
  
  {
            if Dockable then
              xID := i + 32768
            else
              xID := i + 1;
  {}
          xID := i + 1;
  
            // get a handle to insert menu items
          if (j = 0) then begin
            if MenuTyp in [mtFile, mtEdit, mtTrend, mtHelp] then
                // hMenu handle parameter is the main menu -> get menu pos
              xInsHnd := GetSubMenu(hMenu, Ord(MenuTyp))
            else if MenuTyp in [mtFloor, mtGroup, mtMachine] then
                // hMenu handle parameter is the popup menu
              xInsHnd := GetSubMenu(hMenu, 0)
              // it is a new main Menu from LOEPFE: MenuTyp = mtLoepfe
            else if SubMenus = -1 then begin
  //              CodeSite.SendString('Add Mainmenu', xStr);
                // insert the new main menu for LOEPFE before Help
              InsertMenu(hMenu, 4, MF_SEPARATOR or MF_BYPOSITION, 0, '');
              xLoepfeHnd := CreateMenu;
              InsertMenu(hMenu, 5, MF_POPUP or MF_BYPOSITION, xLoepfeHnd, xPChar);
              InsertMenu(hMenu, 6, MF_SEPARATOR or MF_BYPOSITION, 0, '');
                // continue with the next menu item
              Continue;
            end else
                // all other menu item are defined as MenuTyp = mtLoepfe
              xInsHnd := xLoepfeHnd;
  
              // do I have to create a submenu?
            if SubMenus > 0 then begin
              xPopHnd := CreatePopupMenu;
              AppendMenu(xInsHnd, MF_POPUP, xPopHnd, xPChar);
              xInsHnd := xPopHnd;
              j := SubMenus;  // the next Count items will be insert in this
                // continue with the next menu item
              Continue;
            end;
          end;
  
          if (MenuState <> msHide) then begin
            if xStr = '-' then
              AppendMenu(xInsHnd, MF_SEPARATOR, 0, Nil)
            else
              AppendMenu(xInsHnd, MF_STRING, xID, xPChar);
          end;
            // decrement the number of menu items in submenu
          if j > 0 then
            dec(j);
        end;
      end;
    end;  // for
  end;
    //..........................................................................
  
begin
  case iMenuType of
    eMAINMENU: begin
        InsertSubMenu(mgMain, mtFile);
        InsertSubMenu(mgMain, mtEdit);
        InsertSubMenu(mgMain, mtTrend);
        InsertSubMenu(mgMain, mtHelp);
        InsertSubMenu(mgMain, mtLoepfe);
      end;
    eMENUFLOOR: begin
        InsertSubMenu(mgFloor, mtFloor);
      end;
    eMENUGROUP: begin
        InsertSubMenu(mgGroup, mtGroup);
      end;
    eMENUMACHINE: begin
        InsertSubMenu(mgMachine, mtMachine);
      end;
  else
  end;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetSelectedMachines: Boolean;
var
  xPos: Integer;
  xMachSel: IBarcoMachineSelectionList;
//  xMach: Integer;
begin
  Result            := False;
  mSelectedMachines := '';
  mSectionIndex     := 0;
  if Assigned(mFloorInterface) then
  try
    try
      // do type conversion because IBarcoMachineSelectionList is defined in both
      // units: BARCOPLUGIN_TLB and DEXSTD_TLB
      xMachSel := mFloorInterface.MachineSelection as IBarcoMachineSelectionList;
      xPos := 0;
      try
        mSectionIndex := xMachSel.Section.FirstSection[xPos];
      except
      end;

      xPos := 0;
      try
        mMachineIndex := xMachSel.FirstMachine[xPos]-1;  // MM ist 0-basiert
        if gMachConfig.MachRec[mMachineIndex].MachID <> cInvalidMachID then begin
          // Keine Sektion gew�hlt -> Selektierte Maschinen ermitteln
          if mSectionIndex = 0 then begin
            mSelectedMachines := IntToStr(gMachConfig.MachRec[mMachineIndex].MachID);
            // readout next selected machines and build a string
            while xPos <> -1 do
            try
              mMachineIndex := xMachSel.NextMachine[xPos]-1;  // MM ist 0-basiert
              mSelectedMachines := Format('%s, %d', [mSelectedMachines, gMachConfig.MachRec[mMachineIndex].MachID]);
            except 
            end;
          end;
          Result := True;
        end; // if <> cInvalidMachID
      except
      end;
    finally
      PByte(xMachSel) := Nil;
    end;
  except
    // try..except because if Floor doesn't have any machine available
  end;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.GetToolbarBitmap(var pCommandIDs: OleVariant): IDispatch;
  
  type
    PIntArray = ^TIntArray;
    TIntArray = Array[0..0] of Integer;
  var
    xPic: TPicture;
    xBmp: TBitmap;
    i: Integer;
    xIDCount: Integer;
    xData: PIntArray;
  
begin
  Result := Nil;
  // FIRST: count through OLEServerRec to get the count of CommandIDs
  xIDCount := 0;
  for i:=0 to lServerList.Count-1 do begin
    if Assigned(lServerList.Items[i]^.Bitmap) and
       (lServerList.Items[i]^.BitmapCount > 0) then
      inc(xIDCount, lServerList.Items[i]^.BitmapCount);
  end;
  
  // SECOND: create a variant array and define the ID's of the specified menu items
  pCommandIDs := VarArrayCreate([0, xIDCount-1], varInteger);
  xData := VarArrayLock(pCommandIDs);
  xIDCount := 0;
  for i:=0 to fMenu.Count-1 do begin
    if Menu[i]^.MenuBmpName <> '' then begin
      xData^[xIDCount] := i+1;
      inc(xIDCount);
    end;
  end;
  VarArrayUnlock(pCommandIDs);
  
  // THIRD: merge all bitmap to one
  xBmp := TBitmap.Create;
  xBmp.Height := cFloorToolbarBmpHeight;
  for i:=0 to lServerList.Count-1 do begin
    if Assigned(lServerList.Items[i]^.Bitmap) and
       (lServerList.Items[i]^.BitmapCount > 0) then
      MergeBitmap(xBmp, lServerList.Items[i]^.Bitmap);
  end;
  
  // FOURTH: create an OLE Picture
  xPic := TPicture.Create;
  xPic.Bitmap.Assign(xBmp);
  GetOLEPicture(xPic, IPictureDisp(Result));
  
  // FIFTH: clean up
  xPic.Free;
  xBmp.Free;
  CodeSite.AddSeparator;
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.Initialize;
var
  xHnd: THandle;
  xCharArr: Array[0..100] of Char;
begin
  EnterMethod('Initialize');
  inherited Initialize;
  
    // connect to shared memory for machine configuration
  gMachConfig := THostlinkShare.Connect;
  
    // create handle to eventlog
  mEventLog := TEventLogWriter.Create('Application', gMMHost, ssApplication, Self.ClassName + ': ', False);
  
  fDictionary := TmmDictionary.Create(Nil);
  with fDictionary do
  try
    CheckLevel := ivclSystem;
    FloorCheck := True;
    AutoConfig := True;
    Open;
  except
  end;
  
  mCommandID      := 0;
  mFloorInterface := Nil;
  fParentWndHnd   := 0;
  mMachineIndex   := 0;
  mSectionIndex   := 0;
  mSelectedMachines := '';
  
  // read menu definition from each applicaton
  fMenu       := TMenuList.Create;
  
  
  xHnd := GetCurrentProcess;
  xHnd := FindWindow(nil, 'Floor');
  if GetWindowText(xHnd, xCharArr, 100) > 0 then
    CodeSite.SendString('GetWindowText', StrPas(xCharArr));
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.InitMenu(aMenu: Array of TFloorMenuRec);
var
  i: Integer;
  xRec: PPluginMenuRec;
  xSrvRec: POLEServerRec;
  xBmp: TBitmap;
begin
  // the plugin has it's own menu -> fill in an own server record
  New(xSrvRec);
  xSrvRec^.Name := '';
  xSrvRec^.ServerIndex := -1;
  xSrvRec^.StartIndex := 0;
  xSrvRec^.BitmapCount := 0;
  
  // First: Add local menu first so the Extra menu is on the most right position
  xSrvRec^.Bitmap        := TBitmap.Create;;
  xSrvRec^.Bitmap.Height := cFloorToolbarBmpHeight;
  xSrvRec^.MenuCount     := Length(aMenu);
  lServerList.Add(xSrvRec);
  
  // copy the menu items to member variable
  try
    xBmp := TBitmap.Create;
    for i:=0 to xSrvRec^.MenuCount-1 do begin
      new(xRec);
      with xRec^ do begin
        CommandIndex := aMenu[i].CommandID;
        ServerIndex  := xSrvRec^.ServerIndex;
        SubMenus     := aMenu[i].SubMenu;
        MenuBmpName  := aMenu[i].MenuBmpName;
        MenuText     := aMenu[i].MenuText;
        MenuHint     := aMenu[i].MenuHint;
        MenuStBar    := aMenu[i].MenuStBar;
        ToolbarText  := aMenu[i].ToolbarText;
        MenuState    := aMenu[i].MenuState;
        MenuAvailable := aMenu[i].MenuAvailable;
        MenuGroup    := aMenu[i].MenuGroup;
        MenuTyp      := aMenu[i].MenuTyp;
  
        if aMenu[i].MenuBmpName <> '' then
        try
          xBmp.LoadFromResourceName(Hinstance, aMenu[i].MenuBmpName);
          MergeBitmap(xSrvRec^.Bitmap, xBmp);
          MenuBmpName  := aMenu[i].MenuBmpName;
          inc(xSrvRec^.BitmapCount);
        except
          MenuBmpName := '';
        end;
      end;
      fMenu.Add(xRec);
    end;
  finally
    xBmp.Free;
  end;

  // Second: Now get menu information of Loepfe plugins
  GetAddons;
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.InvokeCommand(nID: SYSINT; hWndParent: Integer; const pPluginNotify: IBarcoPluginNotify): SYSINT;
var
  xCharArr: Array[0..100] of Char;
  xIntf: ILoepfePlugin;
  xServerID: Integer;
  xV: Variant;
  i: Integer;
  xList: TStringList;
begin
  Result := 0;
//  CodeSite.SendInteger('hWndParent', hWndParent);
//  if GetWindowText(hWndParent, xCharArr, 100) > 0 then
//    CodeSite.SendString('GetWindowText', StrPas(xCharArr));
//  if GetClassName(hWndParent, xCharArr, 100) > 0 then
//    CodeSite.SendString('GetClassName', StrPas(xCharArr));


  if CheckID(nID) then begin
    Screen.Cursor := crHourGlass;

    // save a instance in member variable to have connection to Floor at anytime
    if not Assigned(mFloorInterface) then begin
      mFloorInterface := pPluginNotify;
  //      mFloorInterface._AddRef;  // <-- will be freed in Destroy() method
    end;

    if GetSelectedMachines then begin
      xServerID         := Menu[nID]^.ServerIndex;
      mCommandID        := Menu[nID]^.CommandIndex;
      fParentWndHnd     := hWndParent;

    //    CodeSite.SendFmtMsg('Plugin: Machines: %s, SectionNr: %d', [mSelectedMachines, mSectionIndex]);
      // Command goes external
      if xServerID <> -1 then begin
        // Initiate Hook functionality at first select of plugin command
        if lFloorWindowHandle = 0 then begin
          lFloorWindowHandle := hWndParent;
          lMsgHookHandle     := SetWindowsHookEx(WH_GETMESSAGE, @MsgProcHookProc, HInstance, 0);
          lCBTHookHandle     := SetWindowsHookEx(WH_CBT,        @CBTProcHookProc, HInstance, 0);
        end;

        with lServerList.Items[xServerID]^ do
        try
          Used := True;
          xIntf := CreateOleObject(Name) as ILoepfePlugin;
          // wenn Sektion = 0 ist, dann sind Maschinen selektiert worden
          if mSelectedMachines = '' then begin
            with gMachConfig.MachRec[mMachineIndex]^ do
              with ProdGrp[mSectionIndex] do begin
                Result := xIntf.Execute(mCommandID, IntToStr(MachID), ProdID, SpdFirst, SpdLast, Self);
              end;
          end
          else begin
            // Selektion durch ganze Maschinen -> in mSelectedMachines hat es die entsprechenden IDs
            Result := xIntf.Execute(mCommandID, mSelectedMachines, 0, 0, 0, Self);
          end;
        except
          on e:Exception do
            WriteToEventLog(Format('CreateOleObject failed: %s', [e.Message]), 'MillMasterPlugin: ', etError);
        end; // with lServerList
        xIntf := Nil;
      end else begin
        // command is internal
        Result := ExecuteCommand(mCommandID, mSelectedMachines, 0);
      end;
    end; // if GetSelectedMachines

      // docu says to notify Floor only if modeless window is used
    if Result <> 0 then
    try
      pPluginNotify.NotifyEvent(BPNE_COMMANDCOMPLETE, xV);
    except
      on e: Exception do
        WriteToEventLog(Format('pPluginNotify.NotifyEvent: %s', [e.Message]), 'MillMasterPlugin: ', etError);
    end;

    mFloorInterface := Nil;
    Screen.Cursor := crDefault;
  end;
end;

//function TFloorAutoObject.InvokeCommand(nID: SYSINT; hWndParent: Integer; const pPluginNotify: IBarcoPluginNotify): SYSINT;
//var
//  xCharArr: Array[0..100] of Char;
//  xIntf: ILoepfePlugin;
//  xServerID: Integer;
//  xV: Variant;
//  i: Integer;
//  xList: TStringList;
//begin
//  Result := 0;
//  CodeSite.SendInteger('hWndParent', hWndParent);
//  
//  if GetWindowText(hWndParent, xCharArr, 100) > 0 then
//    CodeSite.SendString('GetWindowText', StrPas(xCharArr));
//  if GetClassName(hWndParent, xCharArr, 100) > 0 then
//    CodeSite.SendString('GetClassName', StrPas(xCharArr));
//  
//
//  if CheckID(nID) then begin
//    Screen.Cursor := crHourGlass;
//  
//    // save a instance in member variable to have connection to Floor at anytime
//    if not Assigned(mFloorInterface) then begin
//      mFloorInterface := pPluginNotify;
//  //      mFloorInterface._AddRef;  // <-- will be freed in Destroy() method
//    end;
//  
//    xServerID         := Menu[nID]^.ServerIndex;
//    mCommandID        := Menu[nID]^.CommandIndex;
//    fParentWndHnd     := hWndParent;
//    GetSelectedMachines;
//
//  //    CodeSite.SendFmtMsg('Plugin: Machines: %s, SectionNr: %d', [mSelectedMachines, mSectionIndex]);
//    // Command goes external
//    if xServerID <> -1 then begin
//      // Initiate Hook functionality at first select of plugin command
//      if lFloorWindowHandle = 0 then begin
//        lFloorWindowHandle := hWndParent;
//        lMsgHookHandle     := SetWindowsHookEx(WH_GETMESSAGE, @MsgProcHookProc, HInstance, 0);
//        lCBTHookHandle     := SetWindowsHookEx(WH_CBT,        @CBTProcHookProc, HInstance, 0);
//      end;
//
//      with lServerList.Items[xServerID]^ do
//      try
//        Used := True;
//        xIntf := CreateOleObject(Name) as ILoepfePlugin;
//        // wenn Sektion = 0 ist, dann sind Maschinen selektiert worden
//        if mSelectedMachines = '' then begin
//          with gMachConfig.MachRec[mMachineIndex]^ do
//            with ProdGrp[mSectionIndex] do begin
//              Result := xIntf.Execute(mCommandID, IntToStr(MachID), ProdID, SpdFirst, SpdLast, Self);
//            end;
//        end
//        else begin
//          // Selektion durch ganze Maschinen -> in mSelectedMachines hat es die entsprechenden IDs
//          Result := xIntf.Execute(mCommandID, mSelectedMachines, 0, 0, 0, Self);
//        end;
//      except
//        on e:Exception do
//          WriteToEventLog(Format('CreateOleObject failed: %s', [e.Message]), 'MillMasterPlugin: ', etError);
//      end; // with lServerList
//      xIntf := Nil;
//    end else begin
//      // command is internal
//      Result := ExecuteCommand(mCommandID, mSelectedMachines, 0);
//    end;
//  
//      // docu says to notify Floor only if modeless window is used
//    if Result <> 0 then
//    try
//      pPluginNotify.NotifyEvent(BPNE_COMMANDCOMPLETE, xV);
//    except
//      on e: Exception do
//        WriteToEventLog(Format('pPluginNotify.NotifyEvent: %s', [e.Message]), 'MillMasterPlugin: ', etError);
//    end;
//  
//    mFloorInterface := Nil;
//    Screen.Cursor := crDefault;
//  end;
//end;
//
//:---------------------------------------------------------------------------
procedure TFloorAutoObject.LoepfeNotify(aEvent: TLoepfeNotifyEvent; aParameter: OleVariant);
begin

end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.MergeBitmap(var aDestBmp: TBitmap; aSrcBmp: TBitmap);
var
  xWidth: Integer;
begin
  xWidth := aDestBmp.Width;
  aDestBmp.Width := aDestBmp.Width + aSrcBmp.Width;
  aDestBmp.Canvas.Draw(xWidth, 0, aSrcBmp);
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObject.NotifyFloor(aEvent, aCookie: Integer);
begin
  if Assigned(mFloorInterface) then
    mFloorInterface.NotifyEvent(aEvent, aCookie);
end;

//:---------------------------------------------------------------------------
function TFloorAutoObject.Transl(aString: String): string;
begin
  Result := fDictionary.Translate(aString);
end;

//:---------------------------------------------------------------------------
//:--- Class: TFloorAutoObjectFactory
//:---------------------------------------------------------------------------
constructor TFloorAutoObjectFactory.Create(ComServer: TComServerObject; AutoClass: TAutoClass; const ClassID: TGUID; Instancing: TClassInstancing; 
        ThreadingModel: TThreadingModel = tmSingle);
begin
  inherited Create(ComServer, AutoClass, ClassID, Instancing, ThreadingModel);
end;

//:---------------------------------------------------------------------------
procedure TFloorAutoObjectFactory.UpdateRegistry(Register: Boolean);
var
  xKey: string;
begin
  inherited UpdateRegistry(Register);
  
  if (Win32PlatForm = VER_PLATFORM_WIN32_NT) then
    with TmmRegistry.Create do
    try
      RootKey := HKEY_LOCAL_MACHINE;
      xKey := Format('%s\%s.%s', [cRegBarcoAddons, ComServer.ServerName, Self.ClassName]);
      if Register then begin
        OpenKey(xKey, True);
        WriteInteger('Plugin', 1);
        WriteInteger('MenusSupported', $0F);  // enables all menu: Main, Floor, Group, Machine
      end else
        DeleteKey(xKey);
    finally
      Free;
    end;
end;

//:---------------------------------------------------------------------------
//:--- Class: TMenuList
//:---------------------------------------------------------------------------
procedure TMenuList.Clear;
begin
  while Count > 0 do begin
    Dispose(Items[0]);
    Delete(0);
  end;
  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TMenuList.GetItems(aIndex: Integer): PPluginMenuRec;
begin
  Result := inherited Items[aIndex];
end;

//:---------------------------------------------------------------------------
//:--- Class: TServerList
//:---------------------------------------------------------------------------
procedure TServerList.Clear;
begin
  while Count > 0 do begin
    Items[0]^.Bitmap.Free;
    Dispose(Items[0]);
    Delete(0);
  end;
  inherited Clear;
end;

//:---------------------------------------------------------------------------
function TServerList.GetItems(aIndex: Integer): POLEServerRec;
begin
  Result := Inherited Items[aIndex];
end;


//------------------------------------------------------------------------------
initialization
  lServerList := TServerList.Create;

  lFloorWindowHandle := 0;
  lMsgHookHandle := 0;
  lCBTHookHandle := 0;

finalization
  lServerList.Free;

  lFloorWindowHandle := 0;

  if lMsgHookHandle <> 0 then
    UnhookWindowsHookEx(lMsgHookHandle);
  if lCBTHookHandle <> 0 then
    UnhookWindowsHookEx(lCBTHookHandle);
end.

