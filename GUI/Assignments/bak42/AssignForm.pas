(*==============================================================================
| Project.......: L O E P F E 'S   M I L L M A S T E R
| Copyright.....: Gebrueder LOEPFE AG, Kastellstr. 10, CH-8623 Wetzikon
|-------------------------------------------------------------------------------
| Filename......: AssignForm.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 4
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 27.12.1999  1.00  Mg  | Projekt started
| 29.02.2000  1.01  Mg  | FormActivate inserted
| 10.03.2000  1.02  Mg  | Print, and Timer inserted
| 15.03.2000  1.03  Mg  | BroadcastMessage for Floor update inserted
| 22.03.2000  1.04  Mg  | System started ProdGrps not longer supported
| 20.04.2000  1.10  Mg  | Spindelranges inserted
| 08.05.2000  1.11  Mg  | Handling of empty fields inserted
| 22.05.2000  1.20  Mg  | New Changing Modell implemented
| 12.07.2000  1.21  Mg  | MaGrpNr1.AssignedMark inserted
| 29.08.2000  1.22  Mg  | No Broadcast of refresh
| 11.10.2000  1.23  Mg  | No clear of ProdGrpName if Settings changed
| 18.01.2001  1.24 NueMg| Several changes cause of implementing style in SettingsNav
| 12.07.2001  1.25  Nue | Several changes for release 2.01.01
| 13.07.2001  1.26  Wss | Bitmap of bLotProperties changed to the same like in Floor
| 01.10.2001  1.27  Nue | IsOnlyTemplateSetsAvailable added.
| 17.12.2001  1.28  Wss | Icons changed
| 10.01.2002  1.29  Nue | PrintReport added.
| 14.03.2002  1.30  Nue | Add ons in acAssignExecute for correct handling of starting
|                       | a new ProdGrp with modified settings from a before done upload from machine.
| 02.10.2002        LOK | Umbau ADO
| 07.11.2002        LOK | In 'TAssign.SetSpdRange()' wird Settings1.visible auf false gesetzt, da die Settings
|                       | erst angezeigt werden sollten, wenn die richtigen Daten verf�gbar sind. Dies gilt f�r
|                       | Maschinen mit Fix Spindle Range. Wenn die Settings da sind, wird 'TSettings.UpdateComponent' aufgerufen.
|                       | In dieser Methode wird visible wieder true.
|                       | Dasselbe gilt f�r die Komponenten SettingsNav1, MaGrpNr1, SpindleFields1 und Misc1
| 12.03.2003  1.31  Nue | Kosmetische Aenderung ChannelVisibility nicht doppelt setzten in acPrintExecute. (Bug fix!)
| 27.08.2003  1.40  Nue | Ueberarbeitung Assignment.
| 04.02.2004  1.41  Nue | Behandlung Schwarz/weiss f�r Printout von Matrix-Komponenten.
| 14.10.2004  1.40  Wss | Beim Drucken der Settings wurde YarnCount nicht gesetzt
|=============================================================================*)
unit AssignForm;
interface
uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  BaseForm, IvDictio, IvMulti, IvEMulti, mmTranslator,
  SettingsNavFrame, SpindleFieldsFrame, ExtCtrls, mmPanel, ToolWin,
  ComCtrls, mmToolBar, {MiscFrame, MaGrpNrFrame,} SettingsFrame, Buttons,
  mmSpeedButton, AssignHandlerComp, Db, AssignComp,
  ActnList, mmActionList, CloseHandlerComp, MMSecurity,
  MMHtmlHelp, mmTimer, LoepfeGlobal,
  LotParameterForm, BaseGlobal,
  mmAdoConnection,
  MMUGlobal, ImgList, mmImageList, //Nue 11.12.01
  PrintSettingsTemplateForm, ADODB, mmADODataSet, //Nue 8.1.02
  ListCobChooserForm;

resourcestring
  cTitleOfWindow = '(40)Zuordnungen fuer Maschine :'; // ivlm
  cNoYMSettings = '(*)Es sind keine Reiniger Einstellungen zugeordnet!';    // ivlm

//  cRefresh =       '(*)Auf der aktuellen Maschine wurden Einstellungen geaendert. Fenster neu laden?';

type
//..............................................................................
  TAssign = class(TmmForm)
    mmToolBar1: TmmToolBar;
    mmPanel1: TmmPanel;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    mmActionList1: TmmActionList;
    acExit: TAction;
    acPrevProdGrp: TAction;
    acNextProdGrp: TAction;
    acAddSettings: TAction;
    acCopyTemplate: TAction;
    acAssign: TAction;
    bPrevProdGrp: TmmSpeedButton;
    bNextProdGrp: TmmSpeedButton;
    bAssign: TmmSpeedButton;
    CloseHandler1: TCloseHandler;
    acCloseProdGrp: TAction;
    MMSecurityControl1: TMMSecurityControl;
    SpeedButton1: TSpeedButton;
    acSecurity: TAction;
    acHelp: TAction;
    acPrint: TAction;
    ToolButton7: TToolButton;
    mmTimer1: TmmTimer;
    GetSettingsHandler1: TGetSettingsHandler;
    Machine1: TMachine;
    acLotProperties: TAction;
    acSpdRangePilotAndGrp: TAction;
    acSlip: TAction;
    acSettingsAndYarnCnt: TAction;
    acClearerCurves: TAction;
    acEditSettings: TAction;
    mmImageList: TmmImageList;
    bLotProperties: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    bCopyTemplate: TToolButton;
    bAddSettings: TToolButton;
    bEditSettings: TToolButton;
    bClose: TToolButton;
    bExit: TToolButton;
    mmTranslator: TmmTranslator;
    Settings1: TSettings;
    SpindleRangeList1: TSpindleRangeList;
    panelNoSettings: TmmPanel;
    AssignHandler1: TAssignHandler;
    SettingsNav1: TSettingsNav;
//    procedure bShowSettingsClick(Sender: TObject);
//    procedure bSaveAsTemplateClick(Sender: TObject);
    procedure acExitExecute(Sender: TObject);
    procedure acPrevProdGrpExecute(Sender: TObject);
    procedure acNextProdGrpExecute(Sender: TObject);
    procedure mmActionList1Update(Action: TBasicAction;
      var Handled: Boolean);
//    procedure acShowSettingsExecute(Sender: TObject);
    procedure acAddSettingsExecute(Sender: TObject);
    procedure acCopyTemplateExecute(Sender: TObject);
    procedure acAssignExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure acCloseProdGrpExecute(Sender: TObject);
    procedure acSecurityExecute(Sender: TObject);
    procedure acHelpExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure acPrintExecute(Sender: TObject);
    procedure mmTimer1Timer(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure mmToolBar1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
//    procedure acLotPropertiesExecute(Sender: TObject);
    procedure acEditSettingsExecute(Sender: TObject);
  private
    mDataBase: TmmAdoConnection;
    mMessageApplID: Word;
    mMessageDlgOpen: boolean; // Flag to know if a DLG is already open
    fChanged: boolean;
    fSettingsVisible: boolean;
    procedure SetSpdRange(aSpdRange: TMachine);
    function GetSpdRange: TMachine;
    procedure SetSettingsVisible(aVisible: boolean);
    procedure OnValueChanged(aSender: TUserChanges; aChanged: boolean);
    procedure OnFocusChanged(aSender: TUserChanges);
    procedure SetChanged(aChanged: boolean);
    property Changed: boolean read fChanged write SetChanged;
  protected
    procedure WndProc(var msg: TMessage); override;
  public
    constructor Create(aOwner: TComponent; aDataBase: TmmAdoConnection); reintroduce; virtual;
    property SpdRange: TMachine read GetSpdRange write SetSpdRange; // first set machid the set prod id if available
    property SettingsVisible: boolean read fSettingsVisible write SetSettingsVisible;
  end;
//..............................................................................
implementation // 15.07.2002 added mmMBCS to imported units
uses
  mmMBCS,
  Printers, MMMessages, TemplateForm, PrintTemplateForm,
  YMParaDef, u_dmAssignment, //Nue: 10.01.02
  SaveTemplateDlg, SettingsReader; //Nue:16.12.03
{$R *.DFM}
const
  cSettingsHeight = 570;
  cSettingsWidth = 1020;
//------------------------------------------------------------------------------
constructor TAssign.Create(aOwner: TComponent; aDataBase: TmmAdoConnection);
begin
  inherited Create(aOwner);
  HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_DLG_Fenster_Zuordnung.htm');
  acAssign.HelpContext := GetHelpContext('WindingMaster\Zuordnung\ZUO_Reinigereinstellung_zuordnen.htm');

  Width := cSettingsWidth;
  ClientHeight := mmPanel1.Height + mmToolbar1.Height + cSettingsHeight;
  if OLEMode then
    RestoreFormLayout;

  mDataBase := aDataBase;
(*    dseQuery2.Connection          := aDataBase;*)
  fSettingsVisible := True;
  SpindleRangeList1.OnItemValueChanged := OnValueChanged;
  SpindleRangeList1.OnItemFocusChanged := OnFocusChanged;
  Changed := False;
  mMessageApplID := RegisterWindowMessage(cMsgAppl);
  mMessageDlgOpen := False;

//Nue:10.9.01: The following lines moved from mmActionList1Update methode.
(*  Misc1.edSlip.Enabled := MMSecurityControl1.CanEnabled(acSlip);
  SpindleFields1.edFirst.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
  SpindleFields1.edLast.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
  SpindleFields1.edPilot.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
  MaGrpNr1.cobGrpNr.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);*)

//Nue: Settings1.ReadOnly wird neu ueber acEditSettings gehandlet!  19.12.01
//    Settings1.ReadOnly := NOT(MMSecurityControl1.CanEnabled(acSettingsAndYarnCnt));
  Settings1.ReadOnly := True;
  bEditSettings.Enabled := MMSecurityControl1.CanEnabled(acEditSettings);
  Settings1.CurveSelection := MMSecurityControl1.CanEnabled(acClearerCurves);
(*  Misc1.edYarnCnt.Enabled := MMSecurityControl1.CanEnabled(acSettingsAndYarnCnt);*)

end;
//------------------------------------------------------------------------------
procedure TAssign.SetSpdRange(aSpdRange: TMachine);
begin
  try
    //Setzen CallByMachine Nue:20.8.03
    if (aSpdRange is TSpindleRange) or (aSpdRange is TProdGrp)then
      SpindleRangeList1.CallByMachine := False
    else
      SpindleRangeList1.CallByMachine := True;

    SpindleRangeList1.ActSpdRange := aSpdRange;
      //fMachineID              := aMachineID;

    if not (SpindleRangeList1.AssMachine.IsOnline) then //Nue:15.1.02
      bAssign.Enabled := False;

    Caption := cTitleOfWindow + ' ' + Machine1.MachineName;
//      Caption                 := Translate ( cTitleOfWindow ) + Machine1.MachineName;
    Changed := False;
    Settings1.ActMachineYMConfigRec := SpindleRangeList1.AssMachine.MachineYMConfigRec; //Nue 9.7.01
//    Settings1.SetActMachineYMConfigRec(SpindleRangeList1.AssMachine.GetMachineYMConfigRec); //Nue 9.7.01
//      Settings1.ActMachineYMConfigRec := SpindleRangeList1.AssMachine.MachineYMConfigRec;  //Nue 9.7.01
      // LOK (7.11.2002): TSettings ausblenden, wenn die Daten zuerst von der Maschine geholt werden.
      // Settings1 wird in der Methode 'TSettings.UpdateComponent()' wieder angezeigt
    if (aSpdRange.FixSpdRanges) and (aSpdRange.IsOnline) and
       (SpindleRangeList1.AssMachine.IsConnectedToMachine) then begin
      Settings1.visible := False;
      SettingsNav1.visible := False;
(*      MaGrpNr1.visible := False;
      SpindleFields1.visible := False;
      Misc1.Visible := False;*)
    end; // if (aSpdRange.FixSpdRanges)and(aSpdRange.IsOnline) then begin

  except
    on e: Exception do begin
      SystemErrorMsg_('TAssign.SetMachineID failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
function TAssign.GetSpdRange: TMachine;
begin
  Result := SpindleRangeList1.ActSpdRange;
end;
//------------------------------------------------------------------------------
//procedure TAssign.SetSettingsVisible(aVisible: boolean);
//begin
//  if aVisible <> fSettingsVisible then begin
//    fSettingsVisible := aVisible;
//    if fSettingsVisible then begin
//      Settings1.Visible := True;
//      self.ClientHeight := mmPanel1.Height + mmToolbar1.Height + cSettingsHeight;
//    end
//    else begin
//      Settings1.Visible := False;
//      self.ClientHeight := mmPanel1.Height + mmToolbar1.Height;
//    end;
//  end;
//end;
procedure TAssign.SetSettingsVisible(aVisible: boolean);
begin
  if aVisible <> fSettingsVisible then begin
    fSettingsVisible := aVisible;
    if fSettingsVisible then begin
      Settings1.Visible := True;
    end
    else begin
      Settings1.Visible := False;
      panelNoSettings.Caption := cNoYMSettings;
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAssign.OnValueChanged(aSender: TUserChanges; aChanged: boolean);
begin
  Changed := aChanged;
    { V 1.23
    if ( aSender = ucSettings ) and Changed then
      SettingsNav1.ProdGrpName := '';
    }
end;
//------------------------------------------------------------------------------
procedure TAssign.OnFocusChanged(aSender: TUserChanges);
begin
(*  if aSender = ucYarnCnt then
    Settings1.YarnCnt := Misc1.YarnCnt;*)
end;
//------------------------------------------------------------------------------
//procedure TAssign.bShowSettingsClick(Sender: TObject);
//begin
//  SettingsVisible := not SettingsVisible;
//end;
//------------------------------------------------------------------------------
procedure TAssign.WndProc(var msg: TMessage);
var xMachine: TMachine;
begin
  if (msg.msg = RegisterWindowMessage(ASSIGN_REG_STR)) then begin
    case msg.wParam of
      WM_ASSIGN_DONE, WM_CLOSE_DONE: begin
          if AssignHandler1.LastStartedProdGrpID <> 0 then begin

            xMachine := TProdGrp.Create(dmAssignment.dseQuery);
            (xMachine as TProdGrp).ProdGrpID := AssignHandler1.LastStartedProdGrpID;
            SpindleRangeList1.ActSpdRange := xMachine;
            xMachine.Free;
          end
          else begin
            SpindleRangeList1.ActSpdRange := Machine1;
          end;
          Changed := False;
        end;
    else
    end;
  end
  else if (msg.msg = mMessageApplID) then begin
    case msg.wParam of
      cMMRefresh: begin
            { Meldung kommt zu frueh -> neue Settings sind noch nicht auf der DB
            if ( msg.LParam = Machine1.MachineID ) and
                 not AssignHandler1.IsAssignPending and not CloseHandler1.IsClosePending  then begin
              if MessageDlg ( Translate ( cRefresh ), mtConfirmation, [mbYes, mbNo],0 ) = mrYes then begin
                SpdRange := Machine1;
              end;
            end;
            }
        end;
    else
    end;
  end
  else begin // Handle all messages with the default handler
    inherited WndProc(msg);
  end;
end;
//------------------------------------------------------------------------------
procedure TAssign.SetChanged(aChanged: boolean);
begin
  fChanged := aChanged;
  acCopyTemplate.Enabled := MMSecurityControl1.CanEnabled(acCopyTemplate);

    // Markierung fuer ProdGrp
(*  MaGrpNr1.AssignedMark := '';
  if (SpindleRangeList1.ActSpdRange is TProdGrp) and not fChanged then
    MaGrpNr1.AssignedMark := 'A';*)

  if Machine1.IsAbleToClose then
    acCloseProdGrp.Enabled := (not fChanged) and MMSecurityControl1.CanEnabled(acCloseProdGrp)
  else
    acCloseProdGrp.Enabled := False;

  if Assigned(SpindleRangeList1.ActSpdRange) then begin
    if not (SpindleRangeList1.ActSpdRange is TProdGrp) then begin
      acCloseProdGrp.Enabled := False;
    end;
  end;

    //Nue 4.7.01
//  if (SettingsNav1.ActSourceType=stProdGrp) or (SettingsNav1.ActSourceType=stHistory) then
//    acLotProperties.Enabled := True
//  else
//    acLotProperties.Enabled := False;

  acAssign.Enabled := AssignHandler1.IsAssignAllowed(False) and MMSecurityControl1.CanEnabled(acAssign)
                      and (Settings1.Visible);  //Nue:26.8.03
end;
//------------------------------------------------------------------------------
procedure TAssign.acExitExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  Close;
end;
//------------------------------------------------------------------------------
procedure TAssign.acPrevProdGrpExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
(*  SpindleRangeList1.Down;*)
  Changed := False;
end;
//------------------------------------------------------------------------------
procedure TAssign.acNextProdGrpExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
(*  SpindleRangeList1.Up;*)
  Changed := False;
end;
//------------------------------------------------------------------------------
procedure TAssign.mmActionList1Update(Action: TBasicAction;
  var Handled: Boolean);
begin
(*  bPrevProdGrp.Enabled := not SpindleRangeList1.FirstRange;
  bNextProdGrp.Enabled := not SpindleRangeList1.LastRange;*)

//Nue:10.9.01: The following lines moved to the create methode.
{    Misc1.edSlip.Enabled := MMSecurityControl1.CanEnabled(acSlip);
//@@Nue: Hier noch Anpassungen wenn gelb hinterlegte Komponenten von Wss realisiert sind
    SpindleFields1.edFirst.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
    SpindleFields1.edLast.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
    SpindleFields1.edPilot.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);
    MaGrpNr1.cobGrpNr.Enabled := MMSecurityControl1.CanEnabled(acSpdRangePilotAndGrp);

    Settings1.ReadOnly := NOT(MMSecurityControl1.CanEnabled(acSettingsAndYarnCnt));
    Settings1.CurveSelection := MMSecurityControl1.CanEnabled(acClearerCurves);
    Misc1.edYarnCnt.Enabled := MMSecurityCont                                            rol1.CanEnabled(acSettingsAndYarnCnt);
{}
  Handled := True;
end;
//------------------------------------------------------------------------------
//procedure TAssign.acShowSettingsExecute(Sender: TObject);
//begin
//  SettingsVisible := not SettingsVisible;
//end;
//------------------------------------------------------------------------------
procedure TAssign.acAddSettingsExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  Changed := AssignHandler1.EditAddYMSettings;
end;
//------------------------------------------------------------------------------
procedure TAssign.acCopyTemplateExecute(Sender: TObject);
begin
  Settings1.SaveSettingsAsTemplate(0, (SettingsNav1.cobChooser.Items.Objects[SettingsNav1.cobChooser.ItemIndex]) as TBaseItem, tcAssignment);  //2.er Parameter 2.12.03 Nue
//Nue:16.7.03  SettingsNav1.UpdateActList;
end;
//------------------------------------------------------------------------------
procedure TAssign.acAssignExecute(Sender: TObject);
begin
  //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  try
//    SpindleRangeList1.UpdateAllComponents;   //Nue:20.8.03
    //Actuellen SpindleRange zuweisen (nicht �ber UpdateComponent!)
    AssignHandler1.SpdRange := SpdRange; //Nue:18.06.03
    if AssignHandler1.Assign then begin

      //Add on for correct handling of starting a new ProdGrp with modified settings from a before
      if (SettingsNav1.ActSourceType=stMaMemory) then begin
        SettingsNav1.ActSourceType := stProdGrp;
      end;
    end;
  except
    on e: Exception do begin
      SystemErrorMsg_('TAssign.acAssignExecute failed. ' + e.Message);
    end;
  end;
end;
//------------------------------------------------------------------------------
procedure TAssign.FormShow(Sender: TObject);
begin
  Caption := cTitleOfWindow + ' ' + Machine1.MachineName;
  SettingsNav1.DropDownCobChooser := True; //Nue:20.8.03 DropDown wenn Maschine selektiert und ProdGrp running
end;
//------------------------------------------------------------------------------
procedure TAssign.acCloseProdGrpExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  CloseHandler1.SpdRange := TBaseSpindleRange(SpindleRangeList1.ActSpdRange);
  CloseHandler1.CloseProdGrp;
end;
//------------------------------------------------------------------------------
procedure TAssign.acSecurityExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton/TSpeedButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  MMSecurityControl1.Configure;
end;
//------------------------------------------------------------------------------
procedure TAssign.acHelpExecute(Sender: TObject);
begin
    //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  Application.HelpCommand(0, self.HelpContext);
end;
//------------------------------------------------------------------------------
procedure TAssign.FormActivate(Sender: TObject);
begin
  Changed := False;
end;
//------------------------------------------------------------------------------
procedure TAssign.acPrintExecute(Sender: TObject);
var
  xYMSettingsByteArr: TYMSettingsByteArr;
  xLength: Word;
  xOldCursor: TCursor;
  xSettingsNav: TSettingsNav;
  xBaseItem: TBaseItem;
  xPrnFrm: TfrmPrint;
begin
{Alt
   Printer.Orientation := poLandscape;
   Print;
//end Alt {}
//Neu
  with TfrmPrintSettings.Create(self) do try
//    CAConfig := SpdR;
//    ReportInfo := mRepInfo;
    // use this grid as source
    if ShowModal = mrOK then begin
//////      with TfrmPrint.Create(Self) do
      xPrnFrm := TfrmPrint.Create(self);
      with xPrnFrm do try
        mQuickReport.UsePrinterIndex(mPrintSetup.PrinterIndex);

{
      with fCAConfig do begin
        with qrsChannel1 do begin
          ChannelVisible      := ShowChannel;
          YarnCountVisible    := ShowYarnCount;
          FaultClusterVisible := ShowCluster;
        end;
        with qrsChannel2 do begin
          SFIVisible          := ShowSFI;
          FFClusterVisible    := ShowFFCluster;
        end;
        with qrsSplice do begin
          SpliceVisible       := ShowSplice;
        end;

        if ShowClassData then begin
          qrmChannel.QMatrix.Assign(QMChannel);
//          qrmSplice.QMatrix.Assign(QMSplice);
          qrmSiro.QMatrix.Assign(QMFF);
        end;
      end;
{}
        qrmChannel.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//        qrmChannel.BlackWhite := cbBlackWhite.Checked; //Nue:4.2.04
        qrmChannel.BlackWhite := mPrintSetup.PrintBlackWhite; //SDo: 4.6.04

        Settings1.GetScreenSettings(xYMSettingsByteArr, xLength);
        qrmChannel.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrmChannel.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        //Nue: Added 25.11.02
        with qrmChannel do begin
          ChannelVisible := cbChannel.Checked;
          SpliceVisible := cbSplice.Checked;
          ClusterVisible := cbFFCluster.Checked;
        end;

        qrmSiro.QMatrix.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
//        qrmSiro.BlackWhite := cbBlackWhite.Checked; //Nue:4.2.04
        qrmSiro.BlackWhite := mPrintSetup.PrintBlackWhite;//SDo: 4.6.04

        qrsSplice.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrsChannel1.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);
        qrsChannel2.PutYMSettings(PYMSettings(@xYMSettingsByteArr)^);

        with qrsChannel1 do begin
          ChannelVisible := cbChannel.Checked;
          YarnCountVisible := cbYarnCount.Checked;
          FaultClusterVisible := cbCluster.Checked;
        end;
        with qrsChannel2 do begin
          SFIVisible := cbSFI.Checked;
          FFClusterVisible := cbFFCluster.Checked;
        end;
        with qrsSplice do begin
          SpliceVisible := cbSplice.Checked;
        end;

        qcbTemplate.Enabled := False;
        qcbLot.Enabled := False;
        qcbStyle.Enabled := False;
        qcbOrderPos.Enabled := False;
        qcbMachine.Enabled := False;

        xSettingsNav := TSettingsNav(SpindleRangeList1.SettingsNavObject);
        xBaseItem := xSettingsNav.ActCobChooserItem;
        if xSettingsNav.acTemplate.Checked then begin
          qcbTemplate.Enabled := True;
          qlTemplate1.Caption := TTemplateItem(xBaseItem).Name;
          qrsChannel1.YarnCount := 0;
        end;
//        if (xSettingsNav.ActSourceType=stProdGrp) then begin
        //wss: bei Partiehistorie wurde nichts gemacht -> ist auch TProdGrpItem, also auch hier abf�llen
        if xSettingsNav.ActSourceType in [stProdGrp, stHistory] then begin
          qcbLot.Enabled          := True;
          qlLot2.Caption          := TProdGrpItem(xBaseItem).Name;
          qlMachine2.Caption      := TProdGrpItem(xBaseItem).MachineName;
          qlSpindleRange2.Caption := TProdGrpItem(xBaseItem).SpindleFirst + '-' + TProdGrpItem(xBaseItem).SpindleLast;
          qlStyle2.Caption        := TProdGrpItem(xBaseItem).StyleName;
          qlTemplate2.Caption     := TProdGrpItem(xBaseItem).YMSetName;
          qlSlip2.Caption         := TProdGrpItem(xBaseItem).Slip;
          //wss: xBaseItem.YarnCnt entspricht schon der globalen Einheit
          qrsChannel1.YarnUnit    := TMMSettingsReader.Instance.Value[cYarnCntUnit];
          //wss: zuk�nftig kein Faktor mehr
          qrsChannel1.YarnCount   := Trunc(TProdGrpItem(xBaseItem).YarnCnt * 10.0);
//          qlSlip2.Caption := Misc1.edSlip.Text;
//          qrsChannel1.YarnCount := Trunc(StrToFloat(Misc1.edYarnCnt.Text) * 10.0);
        end else
        if xSettingsNav.ActSourceType = stStyle then begin
          qcbStyle.Enabled        := True;
          qlStyle3.Caption        := TStyleItem(xBaseItem).Name;
          qlTemplate3.Caption     := TStyleItem(xBaseItem).YMSetName;
          // TODO wss: Erich, warum wird hier auf TProdGrpItem gecastet und nicht auf TStyleItem?
          qlSlip2.Caption         := TProdGrpItem(xBaseItem).Slip;
          //wss: xBaseItem.YarnCnt entspricht schon der globalen Einheit
          qrsChannel1.YarnUnit    := TMMSettingsReader.Instance.Value[cYarnCntUnit];
          //wss: zuk�nftig kein Faktor mehr
          qrsChannel1.YarnCount   := Trunc(TStyleItem(xBaseItem).YarnCnt * 10.0);
//          qlSlip3.Caption := Misc1.edSlip.Text;
        end else
        if xSettingsNav.ActSourceType = stMaMemory then begin
          qcbMachine.Enabled      := True;
          qlMachine5.Caption      := SpdRange.MachineName;
          qlSpindleRange5.Caption := TPreselectItem(xBaseItem).SpindleFirst + '-' + TPreselectItem(xBaseItem).SpindleLast;
          qlLot5.Caption          := TPreselectItem(xBaseItem).Name;
          qlTemplate5.Caption     := TPreselectItem(xBaseItem).YMSetName;
          //wss: xBaseItem.YarnCnt entspricht schon der globalen Einheit
          qrsChannel1.YarnUnit    := TMMSettingsReader.Instance.Value[cYarnCntUnit];
          //wss: zuk�nftig kein Faktor mehr
          qrsChannel1.YarnCount   := Trunc(TPreselectItem(xBaseItem).YarnCnt * 10.0);
//          qrsChannel1.YarnCount := Trunc(StrToFloat(Misc1.edYarnCnt.Text) * 10.0);
        end;

        xOldCursor := Screen.Cursor;
        with mQuickReport do
        try
          Screen.Cursor := crHourGlass;
          Prepare;
          Print;
        finally
          QRPrinter.Free;
          QRPrinter := nil;
          Screen.Cursor := xOldCursor;
        end;
      finally
        Free;
      end;
    end;
//      mCAConfig := CAConfig;
  finally
    Free;
  end;
{end NEU}
end;
//------------------------------------------------------------------------------
procedure TAssign.mmTimer1Timer(Sender: TObject);
begin
    // Weil nach dem FormActivate noch unerklaerliche change Meldungen eintreffen,
    // wird eine Sekunde gewartet und dann das Changed Property nochmals gesetzt.
  mmTimer1.Enabled := False;
  Changed := False;
end;
//------------------------------------------------------------------------------
procedure TAssign.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //MMBroadCastMessage ( RegisterWindowMessage ( cMsgAppl ), cMMRefresh );
  Action := caFree;
end;
//------------------------------------------------------------------------------
procedure TAssign.mmToolBar1Click(Sender: TObject);
begin
    // Test only
    //SpdRange := Machine1;
end;
//------------------------------------------------------------------------------
procedure TAssign.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    //.....
end;
//------------------------------------------------------------------------------
//procedure TAssign.acLotPropertiesExecute(Sender: TObject); //Nue: 28.06.01
//var
//  x: TProdGrpItem;
//begin
//  //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
//  Self.ActiveControl := nil;
//  if SettingsNav1.rbProdGrp.Checked then
////Alt bis 18.12.01
////    with TLotParameter.Create(Self) do begin
////      ParentWindow := ( Owner as TWinControl ).Handle;
////      xInd := SettingsNav1.cobChooser.ItemIndex;
////      x := TProdGrpItem(SettingsNav1.cobChooser.Items.Objects[SettingsNav1.cobChooser.ItemIndex]);
////      ProdGrpID := x.ProdGrpID;
////      if ShowModal = mrOK then begin
////      end;
////      SettingsNav1.RefreshList;
////    end; //with
//    with TLotSelection.Create(Self) do begin
////      ParentWindow := ( Owner as TWinControl ).Handle;
//      x := TProdGrpItem(SettingsNav1.mExtStringGrid.Objects[SettingsNav1.GridObjectCol ,SettingsNav1.mExtStringGrid.Row]);
//      ProdGrpID := x.ProdGrpID;
////      YarnUnit  := Misc1.YarnCntUnit;
//      if ShowModal = mrOK then begin
//      end;
//      SettingsNav1.RefreshList;
//    end; //with
//end;

procedure TAssign.acEditSettingsExecute(Sender: TObject);
begin
  //Used to take away the focus of the actual control, because TToolButton is not a TWinControl and does not get the focus
  Self.ActiveControl := nil;
  Settings1.ReadOnly := not bEditSettings.Down;
end;

end.

