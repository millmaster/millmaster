(*==============================================================================
|-------------------------------------------------------------------------------
| Filename......: AssignmentsServerIMPL.pas
| Projectpart...: MillMaster NT Spulerei
| Subpart.......: -
| Process(es)...: -
| Description...: Assignment Applikation to start and stopp Produktin Groups
| Info..........: -
| Develop.system: Windows NT 4.0
| Target.system.: Windows NT
| Compiler/Tools: Delphi 5
|-------------------------------------------------------------------------------
| History:
| Date        Vers. Vis.| Reason
|-------------------------------------------------------------------------------
| 27.12.1999  1.00  Mg  | Projekt started
| 11.05.2000  1.10  Mg  | try except inserted
| 19.11.2003  1.10  Wss | Enabling CodeSite mit Registry
|=============================================================================*)
program Assignments;
 // 08.03.2002 added mmMBCS to imported units // 15.07.2002 added mmMBCS to imported units
uses
  MemCheck,
  mmMBCS,
  mmCS,
  Forms,
  LoepfeGlobal,
  BaseGlobal,
  BASEAPPLMAIN in '..\..\..\LoepfeShares\Templates\BASEAPPLMAIN.pas' {BaseApplMainForm},
  BASEDialog in '..\..\..\LoepfeShares\Templates\BASEDIALOG.pas' {mmDialog},
  BASEDialogBottom in '..\..\..\LoepfeShares\Templates\BASEDIALOGBOTTOM.pas' {DialogBottom},
  AssignmentsMainForm in 'AssignmentsMainForm.pas' {ApplMainForm},
  BaseForm in '..\..\..\LoepfeShares\Templates\BASEFORM.pas' {mmForm},
  AssignForm in 'AssignForm.pas' {Assign},
  TKClassSelForm in 'TKClassSelForm.pas' {TKClassSel},
  AddYMSettingsForm in '..\..\Common\AddYMSettingsForm.pas' {AddYMSettings},
  ProdGrpCloseForm in 'ProdGrpCloseForm.pas' {ProdGrpClose},
  Assignments_TLB in 'Assignments_TLB.pas',
  AssignmentsServerIMPL in 'AssignmentsServerIMPL.pas' {AssignmentsServer: CoClass},
  AskCloseForm in '..\..\common\AskCloseForm.pas' {AskClose},
  WaitForm in '..\..\Common\WaitForm.pas' {Wait},
  TemplateForm in '..\..\Common\TemplateForm.pas' {Template},
  SaveAsTemplateForm in '..\..\Common\SaveAsTemplateForm.pas' {SaveAsTemplate},
  PrintSettingsTemplateForm in '..\..\Common\PrintSettingsTemplateForm.pas' {frmPrintSettings},
  PrintTemplateForm in '..\..\Common\PrintTemplateForm.pas' {frmPrint},
  u_dmAssignment in 'u_dmAssignment.pas' {dmAssignment: TDataModule},
  SettingsNavFrame in '..\..\Components\VCL\SettingsNavFrame.pas' {SettingsNav: TFrame},
  FilterCobChooserForm in 'FilterCobChooserForm.pas' {Filter},
  SpindleFieldsFrame in '..\..\Components\VCL\SpindleFieldsFrame.pas' {SpindleFields: TFrame},
  DetailInfoForm in 'DetailInfoForm.pas' {frmDetailInfo},
  AskAssignForm in '..\..\Common\AskAssignForm.pas' {AskAssign},
  ListCobChooserForm in 'ListCobChooserForm.pas' {ListCobChooser},
  SettingsFrame in '..\..\Components\VCL\SettingsFrame.pas' {Settings: TFrame},
  MachineSelForm in 'MachineSelForm.pas' {MachineSel},
  AssignComp in '..\..\Components\VCL\AssignComp.pas',
  AssignHandlerComp in '..\..\Components\VCL\AssignHandlerComp.pas';

{$R *.TLB}

{$R *.RES}
{$R Version.RES}
begin
{$IFDEF MemCheck}
  MemChk('Assignments');
{$ENDIF}
  gApplicationName := 'Assignments';
  CodeSite.Enabled := CodeSiteEnabled;
  Application.Initialize;
  Application.Title := 'Assignments';
  Application.CreateForm(TdmAssignment, dmAssignment);
  Application.CreateForm(TApplMainForm, ApplMainForm);
  Application.Run;

end.

